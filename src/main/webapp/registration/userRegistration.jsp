  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import= "WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" autoFlush="true" %>
<html>
<%--Modifies for wu516_19112013: Redesign of light box registration and FB login also.--%>
<head>    
  <script type="text/javascript">  
  function FocusOnInput(){
   alert('inside set focus functin');   
   document.getElementById("loginemail").focus();    
  }  
  </script> 

</head>
  <%
    String hidenewuserform = (String)request.getAttribute("hidenewuserform");
    String tabFocus = request.getAttribute("tabFocus") != null ? (String)request.getAttribute("tabFocus") : "login";
    CommonUtil util = new CommonUtil();
    String TCVersion = util.versionChanges(GlobalConstants.TERMS_AND_COND_VER);
    
    String nonmywu = request.getParameter("nonmywu") != null ? request.getParameter("nonmywu") : "";
    String pnotes = request.getParameter("pnotes") != null ? request.getParameter("pnotes") : "";
    String downloadurl = request.getParameter("downloadurl") != null ? request.getParameter("downloadurl") : ""; //16-Apr-2014
    String pdfId = request.getParameter("pdfId") != null ? request.getParameter("pdfId") : ""; //16-Apr-2014
    String submitType = request.getParameter("submitType") != null ? request.getParameter("submitType") : ""; //16-Apr-2014
    String pdfName = request.getParameter("pdfName") != null ? request.getParameter("pdfName") : ""; //16-Apr-2014  
    String referrerURL_GA = request.getAttribute("referrerURL_GA") != null ? (String)request.getAttribute("referrerURL_GA") : "";//5_AUG_2014  
    String regLoggingType = request.getParameter("gaLoggingType") != null ? request.getParameter("gaLoggingType") : "";
    String vwcid = request.getParameter("vwcid") != null ? request.getParameter("vwcid") : ""; //16-Apr-2014
    String vwurl = request.getAttribute("vwurl") != null ? request.getAttribute("vwurl").toString() : ""; //16-Apr-2014
    String vwcourseId = request.getAttribute("vwcourseId") != null ? request.getAttribute("vwcourseId").toString() : "";    
    String vwsid = request.getAttribute("vwsid") != null ? request.getAttribute("vwsid").toString() : "";
    String vwnid = request.getAttribute("vwnid") != null ? request.getAttribute("vwnid").toString() : "";
    String vwprice  = request.getAttribute("vwprice") != null ? request.getAttribute("vwprice").toString() : "";
    String vwcname  = request.getAttribute("vwcname") != null ? request.getAttribute("vwcname").toString() : "";
    String vwCollegeId  = request.getAttribute("vwCollegeId") != null ? request.getAttribute("vwCollegeId").toString() : "";
    String vwsewf  = request.getAttribute("vwsewf") != null ? request.getAttribute("vwsewf").toString() : "";	
    String vwpdfcid  = request.getAttribute("vwpdfcid") != null ? request.getAttribute("vwpdfcid").toString() : "";	    
    
    String splashFlag = request.getAttribute("splash-page") != null ? (String)request.getAttribute("splash-page") : "";//9_DEC_2014  
    String nonloggedTm = request.getAttribute("nonloggedTm") != null &&  request.getAttribute("nonloggedTm")!=""? (String)request.getAttribute("nonloggedTm") : "";//9_DEC_2014  
    String lbregistratioVal = request.getAttribute("lbregistration") != null ? (String)request.getAttribute("lbregistration") : ""; 
    String firstNameprep = request.getAttribute("firstNameprep") != null && !("undefined").equals(request.getAttribute("firstNameprep"))  ? (String)request.getAttribute("firstNameprep") : ""; 
    String surNameprep = request.getAttribute("surNameprep") != null && !("undefined").equals(request.getAttribute("surNameprep"))  ? (String)request.getAttribute("surNameprep") : ""; 
    String userNameprep = request.getAttribute("userNameprep") != null &&  !("undefined").equals(request.getAttribute("userNameprep"))? (String)request.getAttribute("userNameprep") : ""; 
    String emailAddressprep = request.getAttribute("emailAddressprep") != null && !("undefined").equals(request.getAttribute("emailAddressprep")) ? (String)request.getAttribute("emailAddressprep") : "";
    String fbUserIdprep = request.getAttribute("fbUserIdprep") != null && !("undefined").equals(request.getAttribute("fbUserIdprep")) ? (String)request.getAttribute("fbUserIdprep") : "";
    
    String socialRegType = ((request.getAttribute("socialRegType") != null && !"".equals(request.getAttribute("socialRegType"))) ? (String)request.getAttribute("socialRegType") : "");
    String surveyForm = request.getAttribute("surveyForm") != null ? (String)request.getAttribute("surveyForm") : "";//Added by Priyaa for 21_JUL_2014_REL for survey auto login 
    String homepageUrl = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName(); 
    String emailDomainJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.email.domain.js");
    String emailDomainLoginJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.email.domain.login.js");
CommonFunction common = new CommonFunction();
    
    String[] yoeArr = common.getYearOfEntryArr();
    String YOE_1 = yoeArr[0];
    String YOE_2 = yoeArr[1];
    String YOE_3 = yoeArr[2];
    String YOE_4 = yoeArr[3];
  %>
<body onload="FocusOnInput()">
  <div class="comLgh" id="lbDivNew">
    <div class="fcls nw">
      <%if(!"".equals(splashFlag) && "true".equals(splashFlag)){%>
          <a class="" onclick="javascript:noSplash()">
             <i class="fa fa-times"></i>
          </a>
      <%}else if(("Y").equals(surveyForm)){%>
           <a class="" href="<%=homepageUrl%>">
             <i class="fa fa-times"></i>
          </a>
      <%}else{%>
             <a class="" onclick="hm()">
               <i class="fa fa-times"></i>
            </a>
          <%}%> 
    </div>   
    
   <%if(("N").equals(hidenewuserform)){%>
    
   <div class="pform nlr bgw profrm2"  id="lbsighuplogin" style="<%="globalnav".equalsIgnoreCase(tabFocus) ? "display:block;" : "display:none;"%>">
      <div class="reg-frm">
           <div class="sgnm pt13 remv_gplus">
             <div class="sgn">
               <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
                 <h1 class="txt_cnr mb22">Sign up with</h1> 
                   <fieldset class="so-btn">
                     <a class="btn1 fbbtn" onclick="loginInfoFacebook('fblogin');" id="ab_fb_nav" title="Login with Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK<i class="fa fa-long-arrow-right"></i></a>                   
                   </fieldset>
                </c:if>
                <p class="qler1 valid_err" id="fblbsighuplogin_error" style="display: none;"></p> <%--Added p tag to fix this bug:39819, on 08_Aug_2017, By Thiyagu G--%>
                </div>
               <div class="sgupm">
                 <p class="comptx">You can also <a title="sign up with email"  id="ab_signup_nav" class="link_blue" onclick="javascript:openSignupSigninSection('lbregistration','');">sign up with email</a></p>
               </div>
               <div class="sgala">
                 <div class="borderbot mb20 mt35"></div>
                 <div class="sgalai">
                   <p class="comptx">Already have an account? <a title="Sign in" id="ab_signin_nav" class="link_blue" onclick="javascript:openSignupSigninSection('lblogin','');">Sign in</a></p>
                 </div>
               </div>
             </div>
      </div>
    </div>
    
    <div class="pform nlr" id="lbbackground"  style="<%="splash".equalsIgnoreCase(tabFocus) ? "display:block;" : "display:none;"%>">
      <div class="reg-frm">
         <c:if test="${empty requestScope.nonloggedTm}">
            <div class="reg-frmt" id="abLightBox">
              <h1 class="txt_cnr sgntx mb20">Why should I sign up?</h1>
              <div class="itepos">
                <div class="icnr">
                  <div class="pig_bs"></div>
                  <span>
                    It's totally
                    <br>
                    free to use!
                  </span>
                </div>
                <div class="icnr">
                  <div class="imac_s"></div>
                  <span>Save course and uni comparisons</span>
                </div>
                <div class="icnr mr0">
                  <div class="coat_s"></div>
                  <span>Relevant and tailored advice</span>
                </div>
              </div>
            </div>
          </c:if>      
          <c:if test="${not empty requestScope.nonloggedTm}">
            <c:if test="${requestScope.nonloggedTm eq 'nonloggedTm'}"> 
              <div class="reg-frmt nlt">
                <div class="time_login">
                  <h1>Get Access to Our Sixth Form Timeline</h1>
                    <div class="time_img">
                       <img src="<%=CommonUtil.getImgPath("/wu-cont/images/timline_login.jpg", 0)%>" alt="Timeline Article" title="Timeline Article">
                    </div>
                    <p class="cnt mb15">If you want exclusive access to our super-helpful timeline (which basically makes choosing a uni as simple as tying shoelaces), all you need to do is register. Go on, it only takes 20 seconds.</p>   
                  </div>
                 </div> 
              <div class="borderbot"></div>
            </c:if>
          </c:if>
          <div class="sgnm mt6 remv_gplus">
            <div class="sgn">
            <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
              <h1 class="txt_cnr mb26">Sign up with</h1>
                <fieldset class="so-btn">
                  <a title="Login with Facebook" onclick="loginInfoFacebook('fblogin');" id="ab_fb_lb" class="btn1 fbbtn"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK<i class="fa fa-long-arrow-right"></i></a>                
                </fieldset>
            </c:if>
            </div>
            <div class="sgupm">
              <p class="comptx">You can also <a onclick="javascript:openSignupSigninSection('lbregistration','');" id="ab_sgnup_lb" class="link_blue" title="sign up with email">sign up with email</a></p>
            </div>
            <div class="sgala">
              <div class="borderbot mb20 mt35"></div>
              <div class="sgalai">
                <p class="comptx">Already have an account? <a onclick="javascript:openSignupSigninSection('lblogin','');" id="ab_sgnin_lb" class="link_blue" title="Sign in">Sign in</a></p>
              </div>
            </div>
          </div>          
      </div>
    </div>
    <%
    String styleclass = "display:none;";
    if("lbregistration".equals(lbregistratioVal)){
     styleclass = "display:block;"; %>
      <input type="hidden" id="fbuserregister" value="fbuserregister"/>   
      <input type="hidden" id="socialRegType" value="<%=socialRegType%>"/>
    <%}%>
    <%
     if(("reg").equals(tabFocus)){
        styleclass = "display:block;"; 
     }
    %>    
    <div class="pform nlr bgw profrm2" id="lbregistration"  style="<%=styleclass%>">    
      <%if("N".equalsIgnoreCase(hidenewuserform)){%>
        <html:form action="newuserregistration.html?method=register" commandName="registerBean">
          <div class="reg-frm">
            <div class="reg-frmt bgw" id="addNonAdvDiv">
              <!--Added By Thiyagu G for 24_Nov_2015 Start-->
              <div id="divNonAdvBlock">
                <div id="nonAdvLogoCont" class="lgo">                  
                  <img id="nonAdvUniLogo" width="105" class="ibdr" src=""/>
                </div>
                <div class="rqinf_rht">
                  <h2>Enter your details</h2>
                  <p>We'll send you all the information you need to know in a handy pack.</p>
                </div> 
              </div>            
              <!--Added By Thiyagu G for 24_Nov_2015 End-->
               <h1 id="signupwithemail" class="mb22">Sign up with email</h1>
               <div class="sgala">                 
                  <div class="sgalai">
                    <p class="comptx">Already have an account? <a title="Sign in" id="regSignIn" class="link_blue" onclick="javascript:openSignupSigninSection('lblogin','');">Sign in</a></p>
                  </div>
                </div>
                  <div class="sgnm pt0 remv_gplus" id="signUpWithBlock">
                    <div class="sgn" style="<%="lbregistration".equalsIgnoreCase(lbregistratioVal) ? "display:none;" : "display:block;"%>">
                     <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
                      <h2 class="txt_cnr mb20">Or sign up with...</h2>
                        <fieldset class="so-btn">
                          <a class="btn1 fbbtn" onclick="loginInfoFacebook('fblogin')" title="Login with Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK<i class="fa fa-long-arrow-right"></i></a>                    
                        </fieldset>
                        </c:if>
                      <p class="qler1 valid_err" id="fblbregistration_error" style="display: none;"></p> <%--Added p tag to fix this bug:39819, on 08_Aug_2017, By Thiyagu G--%>
                      <div class="borderbot mb20 mt35"></div>
                    </div>
                 </div>
               <div class="borderbot mb20 mt35" id="divDivider"></div>
               <!--Added grades and contact fields, By Thiyagu G for 03_Nov_2015.-->
              <div class="sub_cnr">
                <div id="pros-pod">
                  <div id="ql-fm" class="qlf-nw">
                    <div class="clear"></div>
                    <div id="jsFormValidationErrors" class="red"></div>
                    <div class="crm_ql-form base">
                      <div id="step2" class="crm_con" style="display:block;">
                        <h3 class="fnt_lbd fnt20">Your details</h3>
                        <fieldset class="row-fluid mt5">                         
                          <fieldset id="firstName_fieldset" class="w50p fl fst_lg1"> 
                            <div class="frm_lbl">
                              <c:if test="${not empty requestScope.firstNameprep}">
                                <html:input path="firstName"  id="firstName"  autocomplete="off" cssClass="c_txt ql-inpt" maxlength="40" value="<%=firstNameprep%>" onfocus="showTooltip('firstNameYText');" onblur="hideTooltip('firstNameYText');registratonValidations(this.id);" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/>
                              </c:if>
                              <c:if test="${empty requestScope.firstNameprep}">
                                <html:input path="firstName" id="firstName" autocomplete="off" cssClass="c_txt ql-inpt" maxlength="40" onfocus="showTooltip('firstNameYText');" onblur="hideTooltip('firstNameYText');registratonValidations(this.id);" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}" />
                              </c:if>
                              <label for="firstName" class="">First name*</label>
                            </div>
                            <span class="cmp" id="firstNameYText" style="display:none;"> 
                              <div class="hdf5"></div>
                              <div>We'd like this information so we don't call you by the wrong name. That'd be kinda rude.</div>
                              <div class="linear"></div>
                            </span>
                            <p class="qler1" id="firstName_error" style="display:none;"></p>                              
                          </fieldset>
                          <fieldset id="surName_fieldset" class="w50p fr fst_lg1">                              
                              <c:if test="${empty requestScope.surNameprep}">
                                 <html:input path="surName" id="surName" autocomplete="off" cssClass="c_txt ql-inpt" maxlength="40" onblur="registratonValidations(this.id);" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/>
                               </c:if>
                               <c:if test="${not empty requestScope.surNameprep}">
                                 <html:input path="surName" styleId="surName" autocomplete="off" cssClass="c_txt ql-inpt" maxlength="40" onblur="registratonValidations(this.id);" value="<%=surNameprep%>" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/>
                               </c:if>
                               <label for="surName" class="">Last name*</label>
                               <p class="qler1" id="surName_error" style="display:none;"></p>
                          </fieldset>
                        </fieldset>
                        <fieldset class="row-fluid mt5 alst_pos">                         
                          <fieldset id="emailAddress_fieldset" class="w100p fst_lg1">                              
                              <c:if test="${empty requestScope.emailAddressprep}">
                                <html:input path="emailAddress" id="emailAddress" autocomplete="off" maxlength="120" cssClass="c_txt ql-inpt" onfocus="showTooltip('emailAddressYText');" onblur="hideTooltip('emailAddressYText');" onkeydown="hideEmailDropdown(event, 'autoEmailId');"/>
                              </c:if>
                              <c:if test="${not empty requestScope.emailAddressprep}">
                                <html:input path="emailAddress" id="emailAddress" autocomplete="off" maxlength="120" cssClass="c_txt ql-inpt" disabled="true" onfocus="showTooltip('emailAddressYText');" onblur="hideTooltip('emailAddressYText');" onkeydown="hideEmailDropdown(event, 'autoEmailId');" value="<%=emailAddressprep%>" />
                              </c:if>
                              <c:if  test="${not empty requestScope.fbUserIdprep}">
                                <html:hidden path="fbUserId" id="fbUserId" cssClass="ql-inpt" value="<%=fbUserIdprep%>" />
                              </c:if>
                              <label class="" for="emailAddress">Email address*</label>
                              <span class="cmp" id="emailAddressYText" style="display:none;"> 
                                <div class="hdf5"></div>
                                <div>Tell us your email and we'll reward you with...an email.</div>
                                <div class="linear"></div>
                              </span>
                              <p class="qler1" id="registerErrMsg" style="display:none;"></p>
                          </fieldset>
                        </fieldset>
                        <fieldset class="row-fluid mt5">                                                  
                          <fieldset id="password_fieldset" class="w100p fl fst_lg1">                              
                            <c:if test="${empty requestScope.prepopualteUserExists}">
                              <html:password path="password" onblur="registratonValidations(this.id);" id="password" maxlength="20" cssClass="c_txt ql-inpt" cssStyle="display: none;" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/> 
                              <label for="regtextpwd" class="">Your password*</label>
                              <span class="pwd_eye"><a onblur="javascript:showPassword('eyeId', 'password');" href="javascript:showPassword('eyeId', 'password');"><i id="eyeId" class="fa fa-eye" aria-hidden="true"></i></a></span>
                              <input type="text" onfocus="changePassword('regtextpwd','password','password','regtextpwd');" id="regtextpwd" class="c_txt"/> 
                              
                            </c:if>
                            <c:if test="${not empty requestScope.prepopualteUserExists}">
                              <html:password path="password" id="password" maxlength="20" cssClass="c_txt ql-inpt" style="display: none;" onblur="registratonValidations(this.id);" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/> 
                              <label for="regtextpwd" class="">Your password*</label>
                              <span class="pwd_eye"><a onblur="javascript:hidepassword('eyeId', 'password');" href="javascript:showPassword('eyeId', 'password');"><i id="eyeId" class="fa fa-eye" aria-hidden="true"></i></a></span>
                               <input type="text" onfocus="changePassword('regtextpwd','password','password','regtextpwd');" id="regtextpwd" class="c_txt" style="display:none"/>       
                               <input type="hidden" id="prepopualtepassword" value="prepopualtepassword"/>
                                                           
                            </c:if>

                            <p class="qler1" id="password_error" style="display:none;"></p>
                          </fieldset>                                                  
                        </fieldset>
                        
                        <fieldset class="row-fluid">
                          <fieldset class="w50p fl adlog" id="postcodeField">                            
                              <html:input path="postCode" id="postcodeIndex" autocomplete="off" onblur="enquiryValidations(this.id);" cssClass="c_txt ql-inpt adiptxt usr" maxlength="25"/>
                              <label for="postcodeIndex" class="lbco">Postcode <span class="pst_code">(optional)</span></label>
                              <div class="qler1" id="postcodeErrMsg" style="display: none;"></div>
                          </fieldset>
                          <fieldset class="w50p fr"><!--Added onclick function for show and hide tooltip Apr_19_2016-->
                              <a onclick="showAndHideToolTip('postcodeText');" onmouseover="showTooltip('postcodeText');" onmouseout="hideTooltip('postcodeText');" class="color1 fnt_14 fl pt20 pl20 pb20 pst_ttip">Why do we need your postcode?
                              <span id="postcodeText" class="cmp" style="display: none;">
                                <div class="hdf5"></div>
                                <div>We use this information to help assess the reach of our products. This is completely optional.</div>
                                <div class="linear"></div>
                              </span>
                              </a>                              
                          </fieldset>                        
                        </fieldset>
                        
                        <fieldset class="row-fluid mb20 mt25 strt_yr">
                          <fieldset class="w100p">
                            <label class="lbco mb5">WHEN WOULD YOU LIKE TO START?*</label>
                          </fieldset>
                          <fieldset id="yoe_fieldset" class="ql-inp">
                            <span class="ql_rad">                              
                              <html:radiobutton path="yeartoJoinCourse" onmouseout="hideTooltip('yeartoJoinCourse1YText');" onclick="registratonValidations(this.id);" cssClass="check" value="<%=YOE_1%>"  id="yeartoJoinCourse1" /><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse1"><%=YOE_1%></label>
                              <span class="cmp tltip pos_rht" id="yeartoJoinCourse1YText" style="display:none;"> 
                                <div class="hdf5"></div>
                                <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                                <div class="linear"></div>
                              </span>
                            </span>
                            <span class="ql_rad">                              
                              <html:radiobutton path="yeartoJoinCourse" onmouseout="hideTooltip('yeartoJoinCourse2YText');" onclick="registratonValidations(this.id);" cssClass="check" value="<%=YOE_2%>"  id="yeartoJoinCourse2" /><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse2"><%=YOE_2%></label>
                              <span class="cmp tltip" id="yeartoJoinCourse2YText" style="display:none;"> 
                                <div class="hdf5"></div>
                                <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                                <div class="linear"></div>
                              </span>
                            </span>
                            <span class="ql_rad">                              
                              <html:radiobutton path="yeartoJoinCourse" onmouseout="hideTooltip('yeartoJoinCourse3YText');" onclick="registratonValidations(this.id);" cssClass="check" value="<%=YOE_3%>"  id="yeartoJoinCourse3" /><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse3"><%=YOE_3%></label>
                              <span class="cmp tltip" id="yeartoJoinCourse3YText" style="display:none;"> 
                                <div class="hdf5"></div>
                                <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                                <div class="linear"></div>
                              </span>
                            </span>
                            <span class="ql_rad mr0">                              
                              <html:radiobutton path="yeartoJoinCourse" onmouseout="hideTooltip('yeartoJoinCourse4YText');" onclick="registratonValidations(this.id);" cssClass="check" value="<%=YOE_4%>"  id="yeartoJoinCourse4" /><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse4"><%=YOE_4%></label>
                                <span class="cmp tltip pos_lft" id="yeartoJoinCourse4YText" style="display:none;"> 
                                  <div class="hdf5"></div>
                                  <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                                  <div class="linear"></div>
                                </span>
                              </span>
                            <p class="qler1" id="yoe_error" style="display:none;"></p>
                          </fieldset>
                        </fieldset>
                        
                        
                      <div>        
                        <h3 class="fnt_lbd fnt20 mb20 mt20">Stay up to date by email <span class="fnt_lrg gry_txt">(optional)</span></h3>
                        <div class="btn_chk">
                          <span class="chk_btn">
                            <input type="checkbox" value="" class="chkbx1" id="marketingEmailRegFlag" onclick="setRegFlags(this.id);"/>
                            <span class="chk_mark"></span>
                          </span>
                          <p><label for="marketingEmailRegFlag" class="fnt_lbd chkbx_100">Newsletters <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                          Emails from us providing you the latest university news, tips and guides</p>
                        </div>
                        <div class="btn_chk">
                          <span class="chk_btn">
                              <input type="checkbox" value="" class="chkbx1" id="solusEmailRegFlag" onclick="setRegFlags(this.id);"/>
                              <span class="chk_mark"></span>
                          </span>
                          <p><label for="solusEmailRegFlag" class="fnt_lbd chkbx_100"> University updates <span class="cmrk_tclr">(Tick to opt in)</span> </label> <br>
                            <spring:message code="wuni.solus.flag.text"/></p>
                        </div>                        
                        <div class="btn_chk">
                          <span class="chk_btn">
                              <input type="checkbox" value="" class="chkbx1" id="surveyEmailRegFlag" onclick="setRegFlags(this.id);"/>
                              <span class="chk_mark"></span>
                          </span>
                          <p><label for="surveyEmailRegFlag" class="fnt_lbd chkbx_100">Surveys <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                          <spring:message code="wuni.survey.flag.text"/></p>
                        </div>
                        <div class="borderbot fl mt20 mb40"></div>
                      </div>
                      
                        <fieldset class="row-fluid">                         
                          <fieldset class="mt0 rgfm-lt">                                                                                                                                            
                            <span class="chk_btn">
                                <input type="checkbox"  name="tac" value="on" id="termsc" class="chkbx1" >
                                <span class="chk_mark"></span>
                            </span>
                            <label for="termsc" id="pdfSignup">
                              <span class="rem1">I confirm I'm over 13 and agree to the <a title="Whatuni community" class="link_blue" href="javascript:void(0);" onclick="window.open('<%=TCVersion%>','termspopup','width=650,height=400,scrollbars=yes,resizable=yes');">terms and conditions</a> and <a title="privacy notice" class="link_blue" href="javascript:void(0);" onclick="showPrivacyPolicyPopup()">privacy notice</a>, and to become a member of the Whatuni community.<span class="as_trk">*</span> </span>
                            </label>
                            <label for="termsc" id="normalsignup">
                              <span class="rem1">I confirm I'm over 13 and agree to the <a title="Whatuni community" class="link_blue" href="javascript:void(0);" onclick="window.open('<%=TCVersion%>','termspopup','width=650,height=400,scrollbars=yes,resizable=yes');">terms and conditions</a> and <a title="privacy notice" class="link_blue" href="javascript:void(0);" onclick="showPrivacyPolicyPopup()">privacy notice</a>, and to become a member of the Whatuni community.<span class="as_trk">*</span> </span>
                            </label>
                            <p class="qler1" id="termsc_error" style="display:none;"></p>
                          </fieldset>
                          <fieldset class="mt5" style="display: none;">
                            <input type="checkbox" class="check" id="newsLetter" value="Y" name="newsLetter" checked="checked" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}">
                            <span class="rem1">We may send you updates and offers from carefully selected partners, please untick this box if you would prefer not to be sent these emails.</span>
                          </fieldset>
                          <input type="hidden" id="emailValidateFlag" value="true"/>
                          <a onclick="return validateUserRegister()" id="btnLogin" title="Register" class="btn1 blue mt15 w150 fr"><span id="formButtonText">SIGN UP</span>
                            <i class="fa fa-long-arrow-right"></i></a>                          
                        </fieldset>
                         <p id="loadinggifreg" style="display:none;" ><span id="loadgif"><img title="" alt="Loading" src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/indicator1.gif", 0)%>" class="loading"/></span></p>    
                         <input type="hidden" id="nonmywu" value="<%=nonmywu%>"/>
                         <input type="hidden" id="pnotes" value="<%=pnotes%>"/>
                       </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <input type="hidden" id="screenwidth" name="screenwidth" value=""/>
        </html:form>
      <%}%>
    </div>
   <%}%> 
    <div class="pform nlr bgw" id="lblogin"  style="<%=("Y".equalsIgnoreCase(hidenewuserform) || ("login").equals(tabFocus) )? "display:block;" : "display:none;"%>">
      <html:form action="newuserregistration.html?method=login">
        <div class="reg-frm">
          <div class = "reg-frmt bgw">
            <h1 class="mb22">Sign in to Whatuni</h1>
            <fieldset>
              <fieldset class="eml-ads">
                <input type="text" name="loginemail" id="loginemail" onclick="if (this.value == 'Email*') { this.value = '';}" onfocus="clearDefaultLogin(this.id, 'Email*');" onkeydown="hideLoginEmailDropdown(event, 'autoEmailIdLogin');" onblur="setDefaultLogin(this.id, 'Email*');" value="Email*" autocomplete="off" />
                <p class="qler1" id="loginemail_error" style="display:none;"></p>
              </fieldset>
              <fieldset class="pwd">
                <input type="password" name="loginpass" id="loginpass" style="display: none;" maxlength="20" onfocus="clearAutoEmail();" onblur="changePassword('textpwd','loginpass','loginpass','deftextpwd');" onkeypress="javascript:if(event.keyCode==13){return validateUserLogin()}"/>
                <input type="text" onfocus="changePassword('textpwd','loginpass','loginpass','textpwd');" id="textpwd" value="Password*"/>
                <p class="qler1" id="loginpass_error" style="display:none;"></p>
              </fieldset>
            </fieldset>
             <div id="loginerrorMsg" style="display:none;color:red;"> </div>
            <fieldset>
              <fieldset class="rgfm-lt comLghf">
                <fieldset class="mt15">
                  <input type="checkbox" value="Y" id="loginRemember">
                  <label for="loginRemember">
                  <span class="rem1 comLghf">Remember me (Don't use this on a public computer)</span>
                  </label>
                </fieldset>
              </fieldset>
              <fieldset class="rgfm-rt">
                <span class="log-btnc">            
                  <a href="<%=request.getContextPath()%>/forgotPassword.html" class="link_blue fnt_14">Forgot password?</a>
                </span>
                <span class="log-btn mt5">
                   <a onclick="return validateUserLogin()" id="btnLogin" title="Sign in" class="btn1 rgfw">SIGN IN<i class="fa fa-long-arrow-right"></i></a>
                </span>
                  <p id="loadinggif" style="display:none;" ><span id="loadgif"><img title="" alt="Loading" src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/indicator1.gif", 0)%>" class="loading"/></span></p>    
              </fieldset>
            </fieldset>
             <%if(("N").equals(hidenewuserform) && (!(("lbregistration").equals(lbregistratioVal)))){%>
            <div class="borderbot fl mt37 mb24"></div>
            <%}%>
          </div>
           <%if(("N").equals(hidenewuserform)){%>
          <div class="sgnm pt0 pt38 remv_gplus">
            <div class="sgn" style="<%="lbregistration".equalsIgnoreCase(lbregistratioVal) ? "display:none;" : "display:block;"%>">
              <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
                <h2 class="txt_cnr mb20">Or sign in with...</h2>
                  <fieldset class="so-btn">
                    <a class="btn1 fbbtn" onclick="loginInfoFacebook('fblogin');" title="Login with Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK<i class="fa fa-long-arrow-right"></i></a>                
                  </fieldset>
                </c:if>
              <p class="qler1 valid_err" id="fblblogin_error" style="display: none;"></p> <%--Added p tag to fix this bug:39819, on 08_Aug_2017, By Thiyagu G--%>
            </div>
            <div class="sgala">
						        <div class="borderbot mb20 mt40"></div>
              <div class="sgalai">
							         <p class="comptx"><a title="Haven't signed up yet" class="link_blue fnt_16 fnt_lrg" id="loginSignedUpYet" onclick="javascript:openSignupSigninSection('lbregistration','');">Haven't signed up yet</a></p>
              </div>
            </div>
          </div>
          <%}%>
        </div>
      </html:form>
    </div>
     <input type="hidden" id="downloadurl" value="<%=downloadurl%>"/> <%--16-Apr-2014--%>
      <input type="hidden" id="pdfId" value="<%=pdfId%>"/> <%--16-Apr-2014--%>
      <input type="hidden" id="submitType" value="<%=submitType%>"/> <%--16-Apr-2014--%>
      <input type="hidden" id="pdfName" value="<%=pdfName%>"/> <%--16-Apr-2014--%>
      <input type="hidden" id="referrerURL_GA" value="<%=referrerURL_GA%>"/> <%--5_AUG_2014--%>
      <input type="hidden" id="regLoggingType" value="<%=regLoggingType%>"/> <%--5_AUG_2014--%>
      <input type="hidden" id="vwcid" value="<%=vwcid%>"/> <%--3_FEB_2015--%> 
      <input type="hidden" id="vwurl" value="<%=vwurl%>"/> <%--3_FEB_2015--%> 
      <input type="hidden" id="vwsid" value="<%=vwsid%>"/> <%--3_FEB_2015--%> 
      <input type="hidden" id="vwnid" value="<%=vwnid%>"/> <%--3_FEB_2015--%> 
      <input type="hidden" id="vwprice" value="<%=vwprice%>"/> <%--3_FEB_2015--%>  
      <input type="hidden" id="vwcourseId" value="<%=vwcourseId%>"/> <%--3_FEB_2015--%>      
      <input type="hidden" id="vwcname" value="<%=vwcname%>"/> <%--3_FEB_2015--%>   
      <input type="hidden" id="vwpdfcid" value="<%=vwpdfcid%>"/>
      <input type="hidden" id="nonAdvLogoFlag" />
	  <c:if test="${'clearingRegisterPod' eq pageNameParam}">
         <input type="hidden" id="postClearingSrc" value="postClearing-lead-capture"/> <%-- check login from post clearing register pod 28_AUG_2020, by Minu --%>
      </c:if>
  </div>   
  <%--Added auto email domain script for 21_02_2017, By Thiyagu G --%>
  <script type="text/javascript" id="domnScptId">
    var $mdv = jQuery.noConflict();
    $mdv(document).ready(function(e){
      $mdv.getScript("<%=CommonUtil.getJsPath()%>/js/emaildomain/<%=emailDomainJs%>", function(){        
        var arr = ["emailAddress", "hideTooltip('emailAddressYText');", "clearErrorMsg('emailAddress_fieldset','registerErrMsg');"];
        $mdv("#emailAddress").autoEmail(arr);        
    	 });
      $mdv.getScript("<%=CommonUtil.getJsPath()%>/js/emaildomain/<%=emailDomainLoginJs%>", function(){
        var arr1 = ["loginemail", "clearLoginErrorMsg('loginemail_error');"];
        $mdv("#loginemail").autoEmailLogin(arr1);        
    	 });
    });
    updateScreenWidth();
  </script>
  <input type="hidden" id="domainLstId" value='<%=java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.form.email.domain.list")%>' />
</body> 
</html>
