<!DOCTYPE html>
<%--
  * @purpose  This jsp is forgot password page
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 3-Nov-2015 Prabhakaran V.                546     modified old file as part of redesign and functionality changes   546 
  * *************************************************************************************************************************
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="WUI.utilities.GlobalConstants, WUI.utilities.CommonUtil" autoFlush="true" %>

<%String emailDomainJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.email.domain.js");
  String url = GlobalConstants.WHATUNI_SCHEME_NAME+request.getServerName();    
%>

  
  <body>
    <%session.setAttribute("noSplashpopup","true");%>
    <header class="md clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr"><jsp:include page="/jsp/common/wuHeader.jsp" /></div>                
        </div>      
      </div>
    </header>
    <div class="ad_cnr">
      <div class="content-bg">
        <div>
          <div id="content-blk" class="fps sub_cnr">
            <h1><spring:message code="wuni.message.password.message.forgot"/></h1>
            <html:form action="/degrees/sendPassword.html" onsubmit="return validateForgotPassword();" commandName="userLoginBean">
              <fieldset>
                <label for="email">Your email address*</label>
                <html:input path="emailId" maxlength="120" id="firstname" autocomplete="off" placeholder="Please enter your email address" oninput="checkEmail();" onkeydown="hideEmailDropdown(event, 'autoEmailId');" />
                <div id="invalidEmailErrDiv" class="fg_pwd_txt">
                  <%-- Below code is email id not exists error message --%> 
                  <c:if test="${not empty requestScope.messageflag}">
                     <c:if test="${requestScope.messageflag eq 'notfound'}">
                      <p>Sorry, we don't seem to have your email address on file. If you think you might have misspelled it, you could have another <a href="/degrees/forgotPassword.html">try</a>, or <a rel="nofollow" onclick="javaScript:showLightBoxLoginForm('popup-newlogin',650,500,'','','messagePage');">register now </a>if you haven't already done so.
                      </p>   
                    </c:if>
                  </c:if>
                  <%-- End of email id not exists error message --%>
                </div>
                <div id="emailAddress_err" class="errormessage"><html:errors property="invalidEmail"/></div>
                <div id="emailAddress_error" class="errormessage"></div>
              </fieldset>
              <fieldset>
                <label for="email">Are you a human?*</label>
                <div id="QapTcha"></div> <!-- Slider Div -->
                <div id="qapTcha_errorMob" class="errormessage er_msg_mob success1"></div>
                <input type="submit" name="buttext" value="Send password" title="Send password" class="btn1 blue mt5 w150 fr"/>
                <div id="qapTcha_errorDes" class="errormessage"></div>
              </fieldset>              
            </html:form>
            <%-- Success message --%>
            <div id="successMessageDiv">
              <c:if test="${not empty requestScope.messageflag}">
                <c:if test="${requestScope.messageflag eq 'found'}">
                  <div class="success p20">
                    <h6 class="fnt_lbd pb5"><spring:message code="wuni.message.forgot.reset.password.thankyou"/></h6>
                    <p class="txt fnt_lrg m0 p0">
                      <i class="icon-ok mr5"></i>
                      <spring:message code="wuni.message.forgot.success"/>
                      <!-- For local add /degrees/home.html in agr1 -->
                    </p>
                  </div> 
                </c:if> 
                <% if(request.getAttribute("messageflag") != null){request.removeAttribute("messageflag");}%>
              </c:if>
            </div>
            <%-- End of success message --%>
          </div>
        </div>
      </div>
    </div>
    <%--Added auto email domain script for 21_02_2017, By Thiyagu G --%>
  <script type="text/javascript" id="domnScptId">
    var $mdv1 = jQuery.noConflict();
    $mdv1(document).ready(function(e){
      $mdv1.getScript("<%=CommonUtil.getJsPath()%>/js/emaildomain/<%=emailDomainJs%>", function(){        
        var arr = ["firstname", "", ""];
        $mdv1("#firstname").autoEmail(arr);        
    	 });      
    });
  </script>
  <input type="hidden" id="domainLstId" value='<%=java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.form.email.domain.list")%>' />
    <%request.setAttribute("getInsightName","forgotpassword.jsp");%> 
    <jsp:include page="/jsp/common/wuFooter.jsp" />
    <jsp:include page="/registration/include/includeForgotPasswordJS.jsp" />
  </body>
