<%--
  * @purpose  This jsp is reset password page
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 3-Nov-2015 Prabhakaran V.                546     modifid old file as part of redesign and functionality changes   546
  * *************************************************************************************************************************
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>

  <body>
    <header class="md clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr"><jsp:include page="/jsp/common/wuHeader.jsp" /></div>                
        </div>      
      </div>
    </header>
    <div class="ad_cnr">
      <div class="content-bg">
        <div>
          <div id="content-blk" class="fps sub_cnr">
            <h1><spring:message code="wuni.message.password.message.reset" /></h1>
            <html:form action="/degrees/resetPassword.html" onsubmit="return validateResetPassword();">
              <fieldset>
                <label for="email">New password *</label>
                <input type="password" id="password" name="password" maxlength="20" placeholder="Please enter your new password" autocomplete="off" onkeypress="checkPassword();"/>
                <div id="password_error" class="errormessage"></div>
              </fieldset>
              <fieldset>
                <label for="email">Confirm new password *</label>
                <input type="password" id="confirmPassword" name="confirmPassword" maxlength="20" placeholder="Please confirm your password" onkeypress="checkPassword();" autocomplete="off"/>
                <div id="confirmPassword_error" class="errormessage"></div>
              </fieldset>
              <fieldset>
                <label for="email">Are you a human?*</label>
                <div id="QapTcha"></div> <%-- Silder Div --%>
                <div id="qapTcha_errorMob" class="errormessage er_msg_mob success1"></div>
                <input type="submit" name="buttext" value="Reset password" title="Reset password" class="btn1 blue mt5 w150 fr"/>
                <div id="qapTcha_errorDes" class="errormessage"></div>
              </fieldset>
              <fieldset class="fr">
                <input type="hidden" name="method" value="resetSubmit"/>
                <input type="hidden" name="token" value="<%=request.getAttribute("userId")%>"/>
                <input type="hidden" name="caTrackFlag" value="<%=request.getAttribute("caTrackFlag")%>"/>
              </fieldset>
            </html:form>
            <%-- Success message --%>
            <div id="successMessageDiv">
              <c:if test="${not empty requestScope.messageFlag}">
                <div class="success p20">
                  <h6 class="fnt_lbd pb5"><spring:message code="wuni.message.forgot.reset.password.thankyou"/></h6>
                  <p class="txt fnt_lrg m0 p0">
                    <i class="icon-ok mr5"></i>
                    <spring:message code="wuni.message.reset.success"/>
                  </p>  
                </div>
                <c:if test="${not empty sessionScope.userInfoList}">
                   <a class="btn_view_all mt40 fr" href="/degrees/mywhatuni.html">Continue<i class="fa fa-long-arrow-right"></i></a>
                </c:if>
                 <c:if test="${empty sessionScope.userInfoList}">
                  <input type="submit" name="buttext" onclick="javaScript:showLightBoxLoginForm('popup-newlogin',650,500, 'resetPassword');" value="Sign in" styleId="signIn" styleClass="btn1 blue mt20 fr" />
                </c:if>
                <% if(request.getAttribute("messageflag") != null){request.removeAttribute("messageflag");}%>
              </c:if>
            </div>
            <%-- End of success message --%>
          </div>
        </div>
      </div>
    </div>
    <%request.setAttribute("getInsightName","resetPassword.jsp");%>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
    <jsp:include page="/registration/include/includeForgotPasswordJS.jsp" />  
  </body>
