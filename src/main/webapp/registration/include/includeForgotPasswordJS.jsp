<%--
  * @purpose  This jsp is used to include the JS files related slider
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Desc                                                         Rel Ver.
  * 3-Nov-2015 Prabhakaran V.                546    File includes JS file for slider operation..                   546
  * *************************************************************************************************************************
--%>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction" %>
<%String facebookLoginJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.facebook.login.js");%>
<script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery-ui.js"></script>
<script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery.ui.touch-punch-0.2.2.min.js"></script>
<script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/jquery/QapTcha.jquery.js"></script>
<script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=facebookLoginJSName%>"></script>
<script type="text/javascript">
  var $ = jQuery.noConflict();
  var screenWidth = document.documentElement.clientWidth;
  $('#QapTcha').QapTcha({
    disabledSubmit: false,
    autoRevert: true
  });
  jQuery("#Slider").draggable('disable');
</script>

