<%@ page import="WUI.utilities.CommonUtil" autoFlush="true"%>
<!-- Start of code Added for wu_537 24-FEB-2015 by Karthi for Popup of blocked user -->

<div class="comLgh">
 <div class="fcls nw">
   <a class="" onclick="hm()">
     <i class="fa fa-times"></i>
   </a>
  </div>
  <div id="lblogin" class="pform nlr bgw" style="display: block;">
    <form class="cpw" action="/degrees/forgotPassword.html" method="POST">
      <div class="reg-frm">
        <div class="sp_pad cf">
          <div class="sav_pro">
            <div class="sp_title">
              <h1>Oh dear!</h1>  
              <h3 class="sp_h3 rmov_bld">This account has been locked out due to the repeated incorrect login attempts. To unlock your account, click on the reset password button below.</h3>
            </div>
                <fieldset class="si_btn">
                  <span class="log-btn mt5">
                    <input type="submit" class="ok1 m28 can" onclick="hm();" value="Reset Password"/>
                  </span>
                </fieldset>
            </div>
          </div>
        </div>
      </form>
  </div>
</div>
<!--End of code Added for wu_537 24-FEB-2015 by Karthi  -->

