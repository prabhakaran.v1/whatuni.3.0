<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ taglib  uri="http://www.springframework.org/tags" prefix="spring"%>    

<body>
    <!--Modified code for responsive redesign 27_Jan_2015 By S.Indumathi-->
    <div class="tim_lgn">
      <header class="clipart">
        <div class="ad_cnr">
          <div class="content-bg">
            <div id="desktop_hdr">
              <jsp:include page="/jsp/common/wuHeader.jsp" />
            </div>                
          </div>      
        </div>
      </header>    
      <div class="ad_cnr">
        <div id="content-bg">
          <div id="content-left">
            <%
              String userEmail = request.getAttribute("userEmail") != null ? (String)request.getAttribute("userEmail") : "";
            %>                    
            <h2 class="hdr wrt-rev mb-20"><strong><spring:message code="wuni.login.message.head" /></strong></h2>
            <html:form action="/degrees/userLogin.html" cssClass="rform" commandName="userLoginBean">
              <xhtml/>
                                  
                <fieldset class="row-fluid mb15">
                  <fieldset class="">
                    <label class="lbco" for="firstname"><spring:message code="wuni.login.label.email" /> <span class="red">*</span></label>
                  </fieldset>
                  <fieldset id="emailAddress_fieldset" class="w100p sumsg tm_si">
                    <c:if test="${not empty requestScope.userEmail}">
                      <html:input path="userName" maxlength="120" id="firstname" placeholder="Email*" autocomplete="off" cssClass="ql-inpt" value="<%=userEmail%>" readonly="true" style="color: rgb(51, 51, 51);" onkeydown="hideEmailDropdown(event, 'autoEmailId');" />          
                    </c:if>
                    <c:if test="${empty requestScope.userEmail}">
                      <html:input path="userName" maxlength="120" id="firstname" placeholder="Email*" autocomplete="off" cssClass="ql-inpt" onkeydown="hideEmailDropdown(event, 'autoEmailId');"  />          
                    </c:if>
                    <span class="err-msg"> 
                      <c:if test="${not empty invaliduser}">
                        ${invaliduser}
                      </c:if>
                      <html:errors path="userName"></html:errors>
                    </span>
                  </fieldset>
                </fieldset>
                <fieldset class="row-fluid mb15">
                  <fieldset class="w100p">
                    <label for="userName" class="lbco"><spring:message code="wuni.login.label.password" /> <span class="red">*</span></label>
                  </fieldset>
                  <fieldset id="password_fieldset" class="w100p sumsg">
                    <html:password path="password" maxlength="20" id="userName" placeholder="Password*" autocomplete="off" cssClass="ql-inpt" onfocus="this.value=''"/> 
                    <span class="err-msg"> 
                      <c:if test="${not empty invalidpassword}">
                        ${invalidpassword}
                      </c:if>
                      <html:errors path="password"></html:errors>
                    </span>
                  </fieldset>
                </fieldset>
                <p class="cinfo"><a href="<%=request.getContextPath()%>/forgotPassword.html" title="forgotten your password"><spring:message code="wuni.message.login.forgot.password" /></a></p>                                
                <input type="submit" name="buttext" value="Sign in" class="btn1 blue mt5 w150 fr" title="Sign in"/>                            
            
            </html:form>
          </div>
        </div>
      </div>
    </div>
    <!--End-->
    <jsp:include page="/jsp/common/wuFooter.jsp" />
  
  <script type="text/javascript">
  var $ = jQuery.noConflict();
    var arr = ["firstname", ""];
	  $("#firstname").autoEmail(arr);
  </script>
  <input type="hidden" id="domainLstId" value='<%=java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.form.email.domain.list")%>' />
</body>