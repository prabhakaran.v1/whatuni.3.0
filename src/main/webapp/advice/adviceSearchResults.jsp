<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, java.util.ArrayList, WUI.utilities.SessionData, java.util.*, WUI.utilities.CommonFunction" %>
<%
  CommonFunction commonFun = new CommonFunction();
  String totalRecordCount = (request.getAttribute("totalRecordCount")!=null) ? request.getAttribute("totalRecordCount").toString() : "0";  
  int noOfPage = (Integer.parseInt(totalRecordCount)/20);
  if((Integer.parseInt(totalRecordCount)%20) > 0) {
    noOfPage++; 
  }       
  int countDisp = 20;
  String pageNo = (request.getAttribute("page")!=null) ? request.getAttribute("page").toString() : "1";   
  String articleText = "";     
  if(Integer.parseInt(totalRecordCount)==1){articleText="article";}else{articleText="articles";}     
  String pageName = (request.getAttribute("pageName")!=null) ? request.getAttribute("pageName").toString() : "1";
  String keyword = (request.getAttribute("keyword")!=null) ? request.getAttribute("keyword").toString() : "";
  String nextUrl = "";     
  String previousUrl = "";  
  int numofpagesplus = (Integer.parseInt(pageNo)+1);
  int numofpagesminus = (Integer.parseInt(pageNo)-1);
  String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
  String domainSpecificPath = httpStr + java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
  String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
  String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
  String envronmentName = commonFun.getWUSysVarValue("WU_ENV_NAME");
  String adviceJsName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.adviceJsName.js");
  String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
%>
  <%if(noOfPage>1){
     if("1".equals(pageNo)){       
        nextUrl = httpStr + "www.whatuni.com/article-search/?keyword="+keyword+"&page="+numofpagesplus;
     %>
        <link rel="next" href="<%=nextUrl%>" />
     <%}else if(noOfPage == Integer.parseInt(pageNo)){
        previousUrl = httpStr + "www.whatuni.com/article-search/?keyword="+keyword+"&page="+numofpagesminus;
     %>
        <link rel="prev" href="<%=previousUrl%>" />
     <%}else{
        nextUrl = httpStr + "www.whatuni.com/article-search/?keyword="+keyword+"&page="+numofpagesplus;
        previousUrl = httpStr + "www.whatuni.com/article-search/?keyword="+keyword+"&page="+numofpagesminus;
     %>
        <link rel="prev" href="<%=previousUrl%>" />
        <link rel="next" href="<%=nextUrl%>" />
     <%}}
     String indexFollow = "noindex,follow";    
     %>
<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
</header> 
<!--Article Page Starts-->
<div class="article_main pb50 cf">
    <div id="stickyTop" class="">
        <div class="article_land_cnr mb25 pb15">    
            <div class="ad_cnr">
                <div class="content-bg">
                    <div class="sbcr mb-10 pros_brcumb">
                        <div class="bc">
                            <div class="bcrumb_blue"></div>
                        </div>
                    </div>
                    <!-- Uni and study advice -->
                    <section class="asrc p0">
                        <div class="asrcc">
                            <h1>Student advice</h1>
                        </div>
                        <div class="asrcb">
                            <div class="pros_search fl w100p">
                                <form id="articleSearchForm" method="post" action="">
                                    <fieldset class="fl">
                                       <%if(request.getAttribute("keyword") != null) {
                                            %><!--when clicking the keyword clear the text code added by prabha on 03_NOV_2015_REL -->
                                            <input class="fnt_lrg tx_bx" autocomplete="off" name="keyword" id="keyword" value="<%=keyword%>" type="text" onkeypress="return keypressSubmit('1', 'adviceSearch', event);" onclick="if(this.value != null){this.value='';}" onblur="if(this.value ==''){this.value='Enter keyword';this.style.color='#a6a6a6';}" onfocus="if(this.value == 'Enter keyword'){this.value = '';this.style.color='#000';}"/>
                                            <%
                                        }else{%>
                                            <input class="fnt_lrg tx_bx" autocomplete="off" name="keyword" id="keyword" value="Enter keyword" type="text" onkeypress="return keypressSubmit('1', 'adviceSearch', event);" onblur="if(this.value == ''){this.value='Enter keyword';this.style.color='#a6a6a6';}" onfocus="if(this.value == 'Enter keyword'){this.value = '';this.style.color='#000';}"> 
                                        <%}%>
                                        <%if(pageNo!=null && "".equals(pageNo)) {
                                            %>
                                            <input type="hidden" name="page" id="page" value="<%=pageNo%>" />
                                            <input type="hidden" name="pageName" id="pageName" value="<%=pageName%>" />
                                            <%
                                        }else{%>
                                            <input type="hidden" name="page" id="page" value="1" />
                                            <input type="hidden" name="pageName" id="pageName" value="adviceSearch" />
                                        <%}%>                                        
                                        <a class="icon_srch fr" onclick="return adviceSearchSubmit('1','adviceSearch');"><i class="fa fa-search fa-1_5x"></i></a>                                        
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </section>					
                </div>
            </div>
        </div>
    </div>
    <!-- Article Main Heading -->
    <section class="p0">
	<div class="ad_cnr">
            <div class="content-bg">
		<div class="artMain">
                    <section class="asadv p0">
                        <!-- Article Nav -->
                        <div id="stickyAdvice" class="">
                            <jsp:include page="/advice/include/adviceLeftNav.jsp">                            
                                <jsp:param name="menuName" value="Blog" />                                    
                            </jsp:include>
                        </div>
                        <div id="leftNavMobile">
                            <jsp:include page="/advice/include/adviceLeftNavForMobile.jsp" />                            
                        </div>  
                        <div class="art-prod">
                            <div class="art-bl">
                                <div class="art-bl bbtm">
                                  <c:if test="${empty requestScope.searchkeyword}">
                                    <h2>Showing results for '<span class="txt_cap"><%=keyword%></span>' <span class="fnt_14">(<%=totalRecordCount%> <%=articleText%>)</span></h2>
                                  </c:if>
                                  <%--Added article not found message with searched keyword for 11_Aug_2015, By Thiyagu G--%>
                                  <c:if test="${not empty requestScope.searchkeyword and requestScope.searchkeyword eq 'notfound'}">
                                    <h2>We could not find any '<span class="txt_cap"><%=keyword.toUpperCase()%></span>' articles</h2>
                                    <h2>But the following articles are currently trending, maybe you'd like them? Or feel free to search again for a related topic.</h2>
                                  </c:if>
                                </div>
                                <c:if test="${not empty requestScope.adviceSearchResults}">
                                  <section class="sh-res p0">
                                      <ul class="sh-lst subsr">
                                      <c:forEach var="primaryCategoryList" items="${requestScope.adviceSearchResults}" varStatus="i">
                                        <c:set var="checkPostId" value="${primaryCategoryList.postId}"/>
                                          <li>
                                            <div class="shl">
                                              <%String readClassName = "subImg";
                                                  if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId"))){
                                                      readClassName = "subImg read";
                                               }}%>
                                              <div class="<%=readClassName%>">
                                                  <a href="<c:out value="${primaryCategoryList.adviceDetailURL}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>">                            
                                                  <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${primaryCategoryList.imageName}"/>" alt="<c:out value="${primaryCategoryList.postTitle}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>">
                                                  <%if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId"))){%>
                                                  <span class="re-img"></span>
                                                  <%}}%>
                                                  </a>
                                              </div>
                                              <div class="subCnt">
                                                <h3><a href="<c:out value="${primaryCategoryList.adviceDetailURL}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>"><c:out value="${primaryCategoryList.postTitle}"/></a></h3>
                                                <div class="fcont">
                                                  <div class="fcontIc"><i class="fa fa-clock-o pr5"></i><c:out value="${primaryCategoryList.updatedDate}"/></div>
                                                     <%-- <c:if test="${not empty primaryCategoryList.articleViewedCount}">
                                                    <div class="fcontIc"><i class="fa fa-eye pr5 pr5"></i><c:out value="${primaryCategoryList.articleViewedCount}"/></div>
                                                  </c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                                                </div>
                                              </div>                
                                            </div>
                                          </li>                
                                        </c:forEach>
                                      </ul>              
                                      <jsp:include page="/advice/include/articlePagination.jsp">
                                         <jsp:param name="pageno" value="<%=pageNo%>"/>
                                         <jsp:param name="page" value="adviceSearchResults"/>
                                         <jsp:param name="pagelimit"  value="13"/>
                                         <jsp:param name="searchhits" value="<%=totalRecordCount%>"/>
                                         <jsp:param name="recorddisplay" value="20"/>
                                         <jsp:param name="primaryCategory" value=""/>
                                         <jsp:param name="secondaryCategory" value=""/>
                                         <jsp:param name="action" value=""/>    
                                         <jsp:param name="pageTitle" value=""/>
                                      </jsp:include>
                                  </section>
                            </c:if>
                            </div>          
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <div class="flyOut"></div>
</div>
<!--Article Page Ends-->
<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>