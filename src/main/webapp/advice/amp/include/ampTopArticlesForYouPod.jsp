<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator"%>

<c:if test="${not empty requestScope.articlesList}">
 <c:set var="listSize" value="${fn:length(articlesList)}"/>
  <%
    int size = Integer.parseInt(pageContext.getAttribute("listSize").toString());
    String ulClass = "art-lst";
  %>  
  <%-- Article section start--%>
  <section class="artfc p0 mt38 cm_art_pd">
   <h2>Most popular</h2>
    <ul class="<%=ulClass%>">
     <c:forEach var="articlesList" items="${requestScope.articlesList}" varStatus="index">
     <li>
      <div class="art-view">
       <div class="img_ppn">
        <a href="<c:out value="${articlesList.adviceDetailURL}"/>" title="<c:out value="${articlesList.postTitle}"/>">
         <amp-img layout="responsive" class="lazy-load" width="225" height="150" src="<c:out value="${articlesList.imageName}"/>" alt="<c:out value="${articlesList.postTitle}"/>" title="<c:out value="${articlesList.postTitle}"/>"></amp-img>
        </a>
       </div>
       <div class="art-inc">
        <div class="art-desc">
        <c:if test="${not empty articlesList.postShortText}">
          <p class="cmart_txt"><c:out value="${articlesList.postShortText}"/></p>
         </c:if>
         <h3>
          <a href="<c:out value="${articlesList.adviceDetailURL}"/>" title="<c:out value="${articlesList.postTitle}"/>">
									  <c:out value="${articlesList.postTitle}"/>
          </a>
 							 </h3>
						  </div>
					  </div>
				  </div>
			  </li>
     </c:forEach>
		  </ul>
	 </section>
  <%-- Article section end --%>
</c:if>