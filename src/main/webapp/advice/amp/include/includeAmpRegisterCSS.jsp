<%--
  * @purpose  This is AMP register CSS jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 21-Feb-2017 Prabhakaran V.               562     First Draft                                                       562 
  * *************************************************************************************************************************
--%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction"%>
<%
  CommonFunction commonFun = new CommonFunction();
  String envronmentName = commonFun.getWUSysVarValue("WU_ENV_NAME");
  String main_480_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.480.css", null);
  String main_992_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.992.css", null);
  String domainSpecificPath = commonFun.getSchemeName(request)+ CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
%>
<script type="text/javascript" language="javascript">
  var dev = jQuery.noConflict();
  adjustStyle();
  function jqueryWidth() {
    return dev(this).width();
  } 
  function adjustStyle() {
    var width = document.documentElement.clientWidth;
    var path = ""; 
    if (width <= 480) {
	     if (dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }
      <%if(("LIVE").equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}%>
    } else if ((width > 480) && (width <= 992)) {         
	     if (dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }       
      <%if(("LIVE").equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}%>
    } else {
      if (dev("#viewport").length > 0) {
        dev("#viewport").remove();
      }       
      document.getElementById('size-stylesheet').href = "";  
      dev(".hm_srchbx").hide();
    }
  }
  dev(window).on('orientationchange', orientationChangeHandler);
  function orientationChangeHandler(e) {
    setTimeout(function() {
      dev(window).trigger('resize');
    }, 500);
  }
  dev(window).resize(function() {  
    adjustStyle();  
  });
</script>