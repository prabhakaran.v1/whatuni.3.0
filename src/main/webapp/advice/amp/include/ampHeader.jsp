<%@page import="WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
  * @purpose  This is AMP page header jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 21-Feb-2017 Prabhakaran V.               562     First Draft                                                       562 
  * *************************************************************************************************************************
--%>

<%
  CommonFunction commonFun = new CommonFunction();
  String link = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();
  String userProfileImg = "";      
  String mandFieldIsProv= "circle";
%>
<header class="md clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <div id="header" class="hrdr">
		        <button on="tap:sidebar.toggle" class="amp_but hamb-icn"><i class="fa fa-bars amp_sidebar"></i></button> 
          <div class="un_logo"><a href="<%=link%>" title="Whatuni"></a></div>
          <div id="hdrmenu2" class="header-right mt30">
            <div id="mob_nav_rt" class="mob_nav_rt">
              <ul>
              <c:if test="${empty sessionScope.userInfoList}">
                  <li><a href="/register"><i class="fa fa-user"></i></a></li>
                </c:if>
                <c:if test="${not empty sessionScope.userInfoList}">
                  <c:forEach var="logedUserInfo" items="${sessionScope.userInfoList}">
                  <c:if test="${not empty logedUserInfo.userImage}">
                    <c:set var="userImg" value="${logedUserInfo.userImage}"/>
                      <%userProfileImg = pageContext.getAttribute("userImg").toString() ;%>
                    </c:if>
                    <c:if test="${not empty logedUserInfo.statusFlag and logedUserInfo.statusFlag eq 'Y'}">
                        <%mandFieldIsProv = "";%>
                    </c:if>
                  </c:forEach>
                  <li class="inactive"><a href="<%=request.getContextPath()%>/mywhatuni.html" class="noalert"><amp-img width="60" height="60" layout="responsive" src="<%=userProfileImg%>" alt="User profile image"></amp-img><span class="<%=mandFieldIsProv%>"></span></a></li>        
                </c:if>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>