<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  * @purpose  This is AMP page CSS jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 21-Feb-2017 Prabhakaran V.               562     First Draft                                                       562 
  * *************************************************************************************************************************
--%>
 <%-- AMP default styles --%>
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
<noscript>
      <style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style>
    </noscript>
<%-- AMP default styles --%>
<%-- AMP Scripts --%>
<script async src="https://cdn.ampproject.org/v0.js"></script>
   
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>  
    <c:if test="${'N' eq sessionScope.sessionData['cookiePerformanceDisabled']}">
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    </c:if>
<%-- AMP Scripts --%>
<%-- Styles Start Here --%>
<style amp-custom>
*{font-family:Lato-Regular,Arial;margin:0;outline:none;padding:0;text-size-adjust:none;box-sizing:border-box}
body{color:#333;font-size:75%;line-height:140%;margin:0 auto;background:#fff;overflow-x:hidden;font-style:initial}
button,textarea{-webkit-appearance:none}
.ad_cnr{margin:0 auto;width:100%}
.ad_cnr .content-bg{float:left}
.txt_upr{text-transform:uppercase}
.lh_18{line-height:18px}
.lh_24{line-height:24px}
.fnt_12{font-size:12px}
.cl_grylt{color:#707070}
ul{list-style:none outside none}
img{border:none}
a{color:#007adf;cursor:pointer;text-decoration:none}
.fnt_lrg{font-family:Lato-Regular,Arial}
section::after,.content-bg::after{clear:both;content:"";display:table;line-height:0}
@font-face{font-family:'FontAwesome';src:url(https://www.whatuni.com/wu-cont/cssstyles/fonts/fontawesome-webfont.eot?v=4.0.3);src:url(https://www.whatuni.com/wu-cont/cssstyles/fonts/fontawesome-webfont.eot?#iefix&v=4.0.3) format("embedded-opentype"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/fontawesome-webfont.woff?v=4.0.3) format("woff"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/fontawesome-webfont.ttf?v=4.0.3) format("truetype"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/fontawesome-webfont.svg?v=4.0.3#fontawesomeregular) format("svg");font-weight:400;font-style:normal}
@font-face{font-family:'Lato-Regular';src:url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Regular.eot?v=4.0.3);src:url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Regular.eot?#iefix&v=4.0.3) format("embedded-opentype"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Regular.woff?v=4.0.3) format("woff"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Regular.ttf?v=4.0.3) format("truetype"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Regular.svg?v=4.0.3#Lato-Regularregular) format("svg");font-weight:400;font-style:normal}
@font-face{font-family:'Lato-Black';src:url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Black.eot?v=4.0.3);src:url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Black.eot?#iefix&v=4.0.3) format("embedded-opentype"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Black.woff?v=4.0.3) format("woff"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Black.ttf?v=4.0.3) format("truetype"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Black.svg?v=4.0.3#Lato-Blackregular) format("svg");font-weight:400;font-style:normal}
@font-face{font-family:'Lato-Bold';src:url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Bold.eot?v=4.0.3);src:url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Bold.eot?#iefix&v=4.0.3) format("embedded-opentype"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Bold.woff?v=4.0.3) format("woff"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Bold.ttf?v=4.0.3) format("truetype"),url(https://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Bold.svg?v=4.0.3#Lato-Boldregular) format("svg");font-weight:400;font-style:normal}
section{float:left;width:100%;padding:70px 0}
.content-bg{width:100%;margin:0 auto}
.fl{float:left}
.fr{float:right}
.p0{padding:0}
.pr5{padding-right:5px}
.pb15{padding-bottom:15px}
.pb40{padding-bottom:40px}
.mb10{margin-bottom:10px}
.mt20{margin-top:20px}
.mb25{margin-bottom:25px}
.fa{display:inline-block;font-style:normal;font-weight:400;line-height:1;-webkit-font-smoothing:antialiased;font-family:FontAwesome}
.fa-1_5x{font-size:1.5em}
.fa-long-arrow-right:before{content:"\f178"}
.fa-trophy:before{content:"\f091"}
.icon-ok:before{content:"\f00c"}
.fa-check:before{content:"\f00c"}
.fa-remove:before,.fa-close:before,.fa-times:before{content:"\f00d"}
.fa-plus:before{content:"\f067"}
.fa-minus:before{content:"\f068"}
.fa-clock-o:before{content:"\f017"}
.fa-eye:before{content:"\f06e"}
.fa-quote-left:before{content:"\f10d"}
.fa-quote-right:before{content:"\f10e"}
.fa-bars:before{content:"\f0c9"}
.fa-user:before{content:"\f007"}
#tapetext{float:left}
a,a:hover{text-decoration:none}
.footer-links a,.footer-links p{color:#fff;font-family:Lato-Regular,Arial;font-size:14px;line-height:24px}
.footer-links a:hover{text-decoration:underline}
#footer-bg{width:100%;padding:6px 20px;background-color:#00bbfd}
.ftr-cont{margin:0 3%;width:100%;padding-bottom:0;padding-top:6px}
.ad_fbdate{display:inline-block}
.footer-links{background-color:rgba(0,0,0,0);background-image:url(https://images1.whatuni.com/wu-cont/images/what_foo_lgo.png);background-repeat:no-repeat;background-size:auto;background-position:0 13px;clear:both;color:#fff;height:auto;padding:150px 0 20px;text-indent:0;width:auto}
.footer-links h2{margin-bottom:10px;padding-bottom:10px;font-family:Lato-Black,Arial;font-size:24px;color:#fff;font-weight:100;text-align:left}
#footer-info ul{float:left;line-height:24px;padding:0;width:165px}
.clear{clear:both;display:block}
.copy,.copy p,.copy em{font-family:Lato-Regular,Arial}
.copy{color:#abe9fe;margin-bottom:30px;padding-left:0;font-size:14px}
.footer-links a:hover,.copy a{color:#abe9fe}
.sticky_foo{width:100%;height:20px;bottom:0;position:fixed;z-index:996;background:#d4ffca;border-top:1px solid #fff;border-bottom:1px solid #9f9d96;padding:8px 0 10px}
.al_mid{margin:0 auto;display:table}
#tapetext{height:25px;overflow:hidden;color:#333;line-height:25px}
#tapetext strong,#tapetext a{font-family:Lato-Regular,Arial;font-size:13px}
#tapetext a{margin-left:3px}
#tapetext a:hover{text-decoration:underline}
#sky{left:50%;margin-left:464px;position:absolute;top:155px}
.sk2{margin:20px 0}
.cook_plcy{width:180px;position:fixed;bottom:40px;right:0;padding:10px;background-color:#fff;z-index:200;box-shadow:#000 0 0 3px;display:block}
.cook_text{padding:20px;background-color:#d4ffca;color:#5e5e61}
.cook_text p{line-height:24px}
.cook_cls{background-image:url(https://images1.whatuni.com/wu-cont/images/cook_cls.png);height:25px;width:24px;float:right;margin:-15px -15px 0 0}
.cook_text a{font-family:Lato-Bold,Arial;font-size:18px;font-weight:100;color:#00bbfd;line-height:20px}
.cook_text a:hover{text-decoration:underline}
.cook_text p{padding-top:10px;font-family:Lato-Regular,Arial;font-size:14px;font-weight:100}
#mbox{background-color:#fff;height:467px;padding:8px;z-index:99999999}
.bcrumb_blue{padding-bottom:12px;padding-top:14px;font-size:12px;font-family:Lato-regular,Arial}
.bcrumb_blue i{padding-left:17px}
.bcrumb_blue div{display:inline}
.bcrumb_blue .divider{padding:0 6px}
.bcrumb_blue span{font-family:Lato-bold,Arial;color:#5e5e61}
.bcrumb_blue a{font-family:Lato-regular,Arial;color:#00bbfd}
.bcrumb_blue a span{color:#00bbfd;font-family:Lato-regular,Arial}
.bcrumb_blue a:hover span{text-decoration:underline}
.borderbot{border-bottom:1px solid #efefef;width:100%}
iframe[name="google_conversion_frame"]{float:left;margin-top:-13px;font-size:0;height:0;line-height:0;width:0}
.fa-paper-plane::before{content:""}
.fa-comment-o::before{content:""}
.chtbx{width:300px;height:434px;position:fixed;right:0;bottom:0;z-index:111111}
.chtbx ul{width:152px;background:#fff;padding-right:0}
.twtfb{background:url(https://images2.whatuni.com/wu-cont/images/ellie_03.png) center 30px no-repeat #5e5d62;position:absolute;width:240px;left:0;padding:30px}
.chtbx li{float:left;padding-bottom:0}
.cbico{width:50px;padding:12px 0 11px;text-align:center;border-right:1px solid #ccc;background:#5e5d62;cursor:pointer}
.cbico:hover{background:#79797b}
.chtact .cbico,.chtact .cbico:hover{background:#5e5d62;cursor:default}
.chtact .cbico{padding:12px 0}
.cbico .fa{font-size:24px;color:#fff}
.chtbx h3{font-size:24px;margin-bottom:6px;color:#fff;font-family:Lato-Bold,Arial}
.chtbx h4{font-size:17px;padding:0 20px;height:45px;color:#fff;font-family:Lato-Regular,Arial}
.chtbx .cros{padding:7px 1px 0 0}
.chtbx .cros .fa{font-size:20px;color:#fff}
.cbcont{margin-top:112px;text-align:center;line-height:24px}
.chtbx button{display:inline-block;margin-top:-40px;color:#acaca7;background:none;border:0;width:40px;height:40px;float:right;cursor:pointer;text-align:left}
.cbfrm button{margin-top:0}
.cbfrm .iptxt{box-sizing:content-box;font-family:Lato-Regular,Arial}
.chtbx .btn{width:100%}
.cbfrm .iptxt{margin-bottom:5px;width:214px;border:0;padding:11px 13px}
.cbfrm textarea{height:64px}
.cbfrm{margin-top:0}
.chtmin{background:#5e5d62;padding:12px 23px;font-size:18px;color:#fff;width:174px;margin-top:75px;position:fixed;right:0;bottom:40px;cursor:pointer;z-index:11111}
span.sp_cnt{display:inline-block;font-family:Lato-Bold,arial;font-size:18px}
.cbfrm .iptxt{margin-bottom:8px;width:210px;border:0;padding:12px 14px;border-radius:0;font-size:14px;line-height:15px;color:#464646}
.cbfrm textarea{height:56px;min-height:inherit;color:#464646;resize:none;padding:14px}
.chtmin .fa-comment-o{font-size:24px;margin-right:10px}
.cbfrm .btn .fa-paper-plane{display:none}
.vph{color:#fff}
.cbfrm .btn,.cbfrm .btn:hover{background:url(https://images1.whatuni.com/wu-cont/img/btn-arw-mp.png) right center no-repeat #00bbfe;font-weight:700;border-bottom:3px solid #1288a4;font-family:Lato-Regular,Arial}
.cbfrm .btn:hover{background:url(https://images4.whatuni.com/wu-cont/img/btn-arw-mp.png) right center no-repeat #00a8e3}
.cbfrm .btn.fc_bt,.cbfrm .btn.fc_bt:hover{background:url(https://images3.whatuni.com/wu-cont/img/btn-arw-mp.png) right center no-repeat #017ab3;border-bottom:3px solid #006f9d}
.cbfrm span{padding-left:20px;font-family:Lato-Regular,Arial}
.suxs{position:absolute;background:#8bc318;width:210px;padding:10px 15px;border-radius:4px;top:66px;box-shadow:#4c4747 0 0 9px 0;z-index:4}
.suxs .fa{float:left;color:#fff;font-size:14px}
.suxtxt{font-style:normal;font-variant:normal;font-weight:400;font-stretch:normal;font-size:13px;line-height:normal;font-family:Lato-Regular,Arial;color:#fff;padding-left:10px;float:left;width:186px;text-align:left;margin-top:-3px}
.cbcont .suxs{top:258px}
.chtbx .cros{position:absolute;right:0;top:0;margin:8px 12px 0 0;padding:7px 1px 0 0}
.suxs.suxs_fb{top:138px}
.cook_plcy .cook_cls i.fa.fa-times{display:none}
#ajax-div.lb_pload{margin:20px auto 14px;text-align:center}
#ajax-div.lb_pload span{display:block;font-size:14px;color:#707070;margin-bottom:20px;margin-top:8px}
#ajax-div.lb_pload input.button{border:0;padding:8px 15px;border-radius:4px;color:#fff;margin-bottom:20px;cursor:pointer;-webkit-appearance:none;background-color:#00bbfd}
#footer-info ul:nth-child(3){width:185px}
#footer-info ul:nth-child(5){width:125px}
body{color:#707070;overflow-x:hidden;font-style:initial;font-family:Lato-Regular,Arial,Verdana,sans-serif}
.content-bg h3,.content-bg h5{font-weight:100}
.content-bg h1{font-size:40px;line-height:43px}
.content-bg h2{font-size:36px}
.content-bg h3{font-size:22px}
.content-bg h5{font-size:18px}
.pros_brcumb .bcrumb_blue i{padding-left:0}
hgroup{margin-bottom:4px}
.cf::after{clear:both;content:"";display:table;height:0;line-height:0}
.fnt_13{font-size:13px}
.bg_gtrylt{background:#eee}
.bg_white{background:#fff}
.article_main figure{line-height:0}
.article_main h1{font-family:Lato-Regular,arial,verdan,sans-serif;color:#707070;font-size:30px;text-transform:none;text-align:left;line-height:34px;margin:0 0 20px}
.article_main h2,.article_main h3,.article_main h5{line-height:24px;font-family:Lato-Regular,arial,verdan,sans-serif}
.article_main h3{font-family:Lato-Bold,arial,verdan,sans-serif;color:#5e5e61}
.article_main h2{font-size:20px}
.article_main h3{font-size:18px}
.article_main h5{font-size:14px}
.article_main .artCont p{font-family:Lato-Regular,arial,verdan,sans-serif;font-size:16px;line-height:24px;margin-bottom:10px;color:#707070}
.artMain{width:100%;padding:20px;overflow:hidden}
.artMainLft{width:100%}
.artMainLft p{font-family:Lato-Bold,arial,verdan,sans-serif;font-size:18px;line-height:24px;color:#707070;font-style:normal}
.artMainLft a{font-family:Lato-Bold,arial,verdan,sans-serif}
.artSec{width:100%;background:#eee;overflow:hidden}
.artSecInn{width:100%;margin:0 auto;background:#eee}
.aLt{width:100%;float:left;padding:20px}
.aLt .aibr{width:60px;float:left;margin-right:20px}
.aLt .aibr figure img{border-radius:100%;max-width:100%;max-height:60px;float:left}
.aMd{width:100%;float:left}
.aMd.img_ppn{width:100%;position:relative;display:block}
.aMd.img_ppn img{max-width:100%;width:100%}
.aRt{width:186px;float:left;margin:20px 0 20px 20px}
.aRt .socI{position:static;bottom:20px}
.aRt .socI ul{float:left;width:100%}
.aRt .socI ul li{float:left;display:inline-block}
.aRt .socI ul li a{width:35px;display:block;height:33px;background:url(https://images2.whatuni.com/wu-cont/images/soci_icons.png) no-repeat}
.aRt .socI ul li a.fbI{background-position:0 0}
.aRt .socI ul li a.fbI:hover{background-position:0 -36px}
.aRt .socI ul li a.twI{background-position:-35px 0}
.aRt .socI ul li a.twI:hover,.aRt .socI ul li a.twI:active{background-position:-35px -36px}
.aRt .socI ul li a.gI{background-position:-70px 0}
.aRt .socI ul li a.gI:hover,.aRt .socI ul li a.gI:active{background-position:-70px -36px}
.aRt .socI ul li a.atI{background-position:-105px 0;width:34px}
.aRt .socI ul li a.atI:hover,.aRt .socI ul li a.atI:active{background-position:-105px -36px}
.artCont{width:100%;margin:32px auto;position:relative;display:inline-block}
.artCont .artLCo{width:100%;float:left;padding:0 20px}
.artLCo figure.image{padding:30px 30px 20px;border-radius:15px;border:1px solid #e2e2e2;text-align:center;margin:15px 0 30px}
.artLCo figure.image figcaption{margin:4px 0 0;line-height:24px;font-family:Lato-Regular,Arial,Verdana,sans-serif}
.mstPop{width:146px}
.art-lst{float:left;width:100%;padding:0 20px}
.art-lst li{float:left;width:227px;margin:0 19px 0 0;border-bottom:1px solid #e2e2e2}
.art-lst li:last-child{margin-right:0}
.art-view{width:100%;float:none;vertical-align:top;position:relative}
.art-view .img_ppn{position:relative;text-align:center;vertical-align:middle;overflow:hidden;width:25%;height:auto;float:left;padding-bottom:20px}
.art-view .img_ppn img{max-width:100%;display:block}
.art-view .art-inc{overflow:hidden;float:left;width:67%;margin:0 0 0 20px}
.art-view .art-desc{min-height:auto}
.art-view .art-inc h3{width:100%;min-height:auto}
.fcont{width:100%;padding:20px 0 15px;overflow:hidden;font-size:13px;font-family:Lato-Regular,arial,verdan,sans-serif}
.fcont .fcontIc{float:left;margin:0 15px 0 0;font-family:Lato-Regular,arial,verdan,sans-serif}
.box_cnr{width:100%;min-height:200px;float:left;position:relative}
#tickerTape{display:block}
.artLCo img{max-width:100%;width:100%;height:auto}
.mhc figure{display:inline-block;padding:30px 30px 13px;border-radius:15px;border:1px solid #e2e2e2;text-align:center;margin:15px 0 30px;max-width:100%;width:100%}
.mhc figcaption{margin:4px 0 0;line-height:24px;font-family:Lato-Regular,Arial,Verdana,sans-serif}
.mhc figcaption a{color:#00bbfd;font-family:Lato-Regular,Arial,Verdana,sans-serif}
.mhc figcaption a:hover{color:#00bbfd;font-family:Lato-Regular,Arial,Verdana,sans-serif}
h1{font-size:200%}
.artfc h3 a{font-family:Lato-Bold,arial,verdan,sans-serif;font-size:18px;vertical-align:top;color:#00bbfd}
.artfc h3 a:hover{text-decoration:underline}
.artfc .art-lst li{border-bottom:1px solid #e2e2e2;float:left;margin:0 24px 20px 0;width:100%}
.artfc .art-lst li:last-child{margin-right:0}
.artfc h2{font-size:24px;line-height:24px;font-family:Lato-Regular,arial,verdan,sans-serif;margin-bottom:17px;color:#707070;text-align:left;padding:0 20px}
.footer-links a{color:#fff}
input[type="button"]{-webkit-appearance:none}
.article_main .mhc a,.artMainLft a,.article_main .artCont a{color:#00bbfd;font-family:lato-bold,arial,sans-serif}
.article_main .mhc a:hover,.artMainLft a:hover,.article_main .artCont a:hover{color:#00bbfd;text-decoration:underline}
.article_main .mhc strong{font-family:lato-bold,arial,sans-serif;font-weight:400}
.article_main .mhc a{font-family:lato-regular,arial,sans-serif}
.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande",tahoma,verdana,arial,sans-serif;font-size:11px;font-style:normal;font-variant-ligatures:normal;font-variant-caps:normal;font-weight:400;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}
.fb_reset > div{overflow:hidden}
#holder_NRW{overflow:auto;min-height:400px}
.aLt .link_blue{color:#00bbfd}
.clipart,.clipart.bg1{background:#01b6ef;width:100%;float:left;position:relative;z-index:2}
#desktop_ftr{display:block}
.hrdr{display:block;width:100%;height:50px;float:left}
#navbar{clear:left;color:#fff;float:left;font-size:24px;margin-left:0;padding:13px 15px;position:relative;z-index:2;display:block}
#navbar:hover,.navact{background:none repeat scroll 0 0 #686868;color:#fff}
.un_logo{float:none;margin:0 auto;width:71px}
.un_logo a,.un_logo.tst a,.logo-my a,.un_logo.dh a,.un_logo.clr15 a{margin:0;background:url(https://images1.whatuni.com/wu-cont/images/mobile/WhatUni%20Logo_mob2.svg) no-repeat 0 0;background-size:100% 100%;height:50px;width:75px;float:left}
.header-right{float:right;margin-top:0;margin-right:0;width:auto}
.mob_nav_rt ul li a i{font-size:24px;color:#fff}
.mob_nav_rt ul li{display:table-cell;vertical-align:middle;position:relative}
.mob_nav_rt ul li a{display:table-cell;width:50px;height:50px;padding:0 10px;vertical-align:middle;text-align:center}
.hdr_menu{float:left;position:absolute;left:0;top:25px;width:100%;height:auto;margin-top:0;background:#686868;z-index:100000}
amp-sidebar{width:240px;background:#686868}
.hdr_menu > ul > li{width:100%;height:auto;padding:0;clear:both;display:block;border-bottom:1px solid #787878}
.mycmp_menu > ul > li > a{color:#fdfaff;font-size:13px;padding:11px 10px 8px}
.hdr_menu > ul li .chose_inr{width:100%;box-sizing:border-box;background:none repeat scroll 0 0 transparent;border:0 solid #025f87;padding:0 0 10px;position:relative;top:-8px}
.mycmp_menu .fa.fa-plus-circle,.mycmp_menu .fa.fa-minus-circle{float:right;font-size:18px;margin-right:6px;display:block}
.mnite_cnt2 li a{font:14px lato-regular,Arial;color:#fff;padding:20px 0 8px;display:block}
.hdr_menu > ul > li > a,.hdr_menu > ul > li.mfnl_bgcol a{display:block;padding:18px 19px 16px;color:#fdfaff;font-size:13px}
#click_view1,#click_view2{display:none}
button.amp_but.hamb-icn{-webkit-appearance:none;background:#01b6ef;border:0;padding:13px;display:inline-block;float:left}
button.amp_but.hamb-icn i{font-size:24px;color:#fff}
.none_bv{display:none}
.amp-close-image.cls{float:right;margin:14px 14px 0 0;position:relative;z-index:111111}
.amp-close-image.cls .cls_icn i{font-size:24px;color:#fff}
.cmp_count{background:none repeat scroll 0 0 #fefe80;border:0 solid #1bacda;border-radius:5px;color:#5e5e61;display:inline-block;float:right;font:14px Lato-Bold,Arial,Verdana,sans-serif;margin-left:8px;margin-top:-3px;padding:2px 7px}
.hdr_menu > ul > li.mfnl_bgcol{background:#00b261;margin-left:0;height:50px;width:100%}
.article_main h1{font-size:24px;line-height:29px}
.mfnl_bgcol .cmp_count{background:#fff;color:#00b260}
.mstPop{float:left;border-top:1px solid #e2e2e2;width:100%;margin:20px 0 0;padding:20px 20px 0}
ul.rgt-lst li{overflow:hidden;border-bottom:1px solid #efefef;background:#f0f0f0;padding:10px;margin:0 0 15px}
ul.rgt-lst li:last-child{margin-bottom:0}
.artCont .mstPop figure{float:left;margin-right:10px;margin-bottom:0;width:140px;height:auto}
.article_main .artCont p{font-family:"Lato-Regular",arial,verdan,sans-serif;font-size:16px;line-height:24px;margin-bottom:10px;color:#707070}
.btn1{margin:0 auto;padding:12px 15px;border:1px solid #fff;width:100%;border-radius:5px;margin-top:40px;font-family:"Lato-Black",Arial;font-size:14px;color:#fff;font-weight:100;display:block;text-transform:uppercase}
.mstPop p a{font-size:14px}
.article_main .yns{width:100%;float:left;margin-left:0;overflow:hidden}
.mstPop.nxs .btn1.bg_orange{background:#FF8B00;color:#fff;border:0}
.mstPop.nxs{position:relative;background:#eee;margin-bottom:30px;padding:20px;float:left}
.mstPop{width:100%;margin:20px 0 0}
.mstPop h2{color:#707070}
.article_main .btn1{border:1px solid #00BBFD}
.mstPop.nxs .btn1{padding:11px 11px 10px 13px}
.article_main .mhc a,.artMainLft a,.article_main .artCont a{font-family:lato-bold,Arial}
.mstPop.nxs .btn1.bg_orange{float:left;width:100%}
.txt_cnr{text-align:center}
.btn1{clear:left;margin:12px auto 0}
.mstPop.nxs .btn1.blue{background:#00bbfd;color:#fff}
.btn1 .fa{float:right;margin-top:1px}
.artLCo ol,.artLCo ul{margin:20px;}
.artLCo ol li,.artLCo ul li{font-family: lato-regular,arial;font-size:16px;line-height:24px;}
.artLCo ul li{list-style:outside disc;}
.article_main .mhc em,.article_main .mhc a{font-family: lato-regular,arial;}
.mob_nav_rt ul li a img{display: block; border: 2px solid #fff; border-radius: 50%; width: 30px; height: 30px; }
.pdf-download #pdf3 amp-img { width: 16px; float: left; margin-right: 4px; }
.pdf-download { margin-top: 10px; }
.article_main .cmart_txt{font-family:"Lato-bold",Arial;color:#afaeb4;font-size:14px;display:block;width:100%;line-height:20px;}
.article_main .mstPop p {line-height: 18px;width: calc(100% - 152px);float: left;}
.cm_art_pd .art-view .art-inc h3{margin-bottom:11px;}
.art-view .img_ppn{width: 140px;}
.art-view .art-inc {width: calc(100% - 160px);}
.article_main .sall_art {width: 100%;}
.article_main .sall_art .btn1{ margin-bottom: 40px;
color: #00bbfd; width: 240px;}
.article_main .sall_art .btn1:hover{background:#00bbfd;color: #fff;}
/* Footer Section */
.fa-facebook:before{content:"\f09a"}
.fa-twitter:before{content:"\f099"}
.footer_bg{background:#00bbfd}
.footer_bg .ftr_lnks{display:block;float:left;width:100%;background:url(https://www.whatuni.com/wu-cont/images/what_foo_lgo.png) no-repeat 30px 30px;padding:170px 0 30px 30px}
.col_wrp{width:100%;float:left;margin-bottom:22px;padding-right:2%}
.col_wrp h2{font-family:"Lato-Black",Arial;font-size:18px;text-align:left;color:#fff;margin-bottom:15px}
.col_wrp ul li a{font-size:14px;color:#fff;line-height:24px}
.col_wrp ul li a:hover{opacity:0.8;text-decoration:underline}
.col_wrp.col4{width:100%;clear:left;margin:0 0 8px 0;padding-right:0}
.col_wrp.col4 .soc_icns li{float:left}
.col_wrp.col4 .soc_icns li a{display:inline-block;width:40px;height:40px;margin-left:0;margin-right:10px;text-align:center;background:#007ab3;border-radius:50%;line-height:40px;
transition:background 0.2s;outline:0}
.col_wrp.col4 .soc_icns li a:hover{background:#015c86;opacity:1;text-decoration:underline}
.col_wrp.col4 .soc_icns li a i{font-size:21px;line-height:42px}
.col_wrp.col4 .soc_icns li a img {width: 24px;margin: 8px 0 auto;padding-left: 1px;min-width: 24px;top: -8px;left: 9px;}
.col_wrp.col4 .soc_icns li a.gp img{width:26px;margin-top:6px;padding-left:0}
.col_wrp.col4 h2{margin-bottom:22px}
.ftr_lnks .cpy_rht p{float:left;clear:left;font-size:14px;color:#fff;margin-top:16px;padding-right:30px;opacity:0.6}
.ftr_lnks .cpy_rht p a {color: #fff;}
/*.col_wrp.col3 ul li:nth-of-type(1) a {pointer-events: none;} Commented by Prabha for app link is clickable on 08_Aug_17*/
/* Footer Section */
/* covid update changes start */
.covid_ltms {float:left;text-align:center;background:#00B26D;width:100%;padding:16px 10px 16px 10px;font:14px/20px "Lato-Regular",Arial;color:#fff;position:relative;z-index:1}
.covid_ltms .covid_link{color:#fff;font:14px/20px "Lato-Regular",Arial;text-decoration:underline;margin-left:6px}
.covid_ltms .covid_link .linfo{padding-right:5px}
.covid_ltms .covid_link:hover{text-decoration:underline}
.covid_ltms .covid_link img{vertical-align:middle}
/* covid update changes end */
.covid_upt{float:left;width:100%;box-sizing:border-box;border:1px solid #00B26D;margin-bottom:20px;padding:9px 10px 9px 10px;font:14px/20px "Lato-Regular",Arial;background:rgba(0, 178, 109, 0.05)}
.covid_upt .covid_txt{color:#00B26D}
.covid_upt .covid_link{color:#02BBFD;font:14px/20px "Lato-Regular",Arial;text-decoration:underline}
.covid_upt .covid_link .linfo{padding-right:5px}
.covid_upt .covid_link:hover{text-decoration:underline}
.covid_upt .covid_link img,.covid_ltms .covid_link i{vertical-align:middle;display:inline-block;width:16px}
</style>