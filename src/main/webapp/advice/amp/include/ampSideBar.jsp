<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--
  * @purpose  This is AMP page side bar jsp..
  * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 21-Feb-2017 Prabhakaran V.               562     First Draft                                                       562 
  * *************************************************************************************************************************
  * 06.05.2020  Prabhakaran V.               1.1     Renaming open day text to virtual tour                            
--%>
<%@page import="WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants"%>
<%
  CommonFunction common = new CommonFunction();
  String basketCount = (String)request.getSession().getAttribute("basketpodcollegecount");
  String fnChNavCnt = "0";
  String clearingonoff = common.getWUSysVarValue("CLEARING_ON_OFF");
  if(basketCount == null || "0".equalsIgnoreCase(basketCount)) {
    common.getBasketPodContent(request, response);
    basketCount = (String)request.getSession().getAttribute("basketpodcollegecount");
  }
  String curYear = common.getWUSysVarValue("CURRENT_YEAR_AWARDS");  
  String clearingUserType = (String)session.getAttribute("USER_TYPE");
%>
<c:if test="${not empty sessionScope.userInfoList}">
  <c:forEach var="mychUserInfo" items="${sessionScope.userInfoList}">
    <c:if test="${not empty mychUserInfo.mychNavCnt}">
      <c:set var="fnChCnt" value="${mychUserInfo.mychNavCnt}"/>
      <%fnChNavCnt= pageContext.getAttribute("fnChCnt").toString() ;%> 
    </c:if>
  </c:forEach>
</c:if>  
<amp-sidebar id="sidebar" layout="nodisplay"  side="left">
  <span class="amp-close-image cls" on="tap:sidebar.close" role="button" tabindex="0">
    <a href="#" class="cls_icn"><i class="fa fa-times" aria-hidden="true"></i></a>
  </span>
  <nav id="hdr_menu" class="hdr_menu mycmp_menu">
    <ul>
      <%if("CLEARING".equalsIgnoreCase(clearingUserType)){%>
        <%--Changed clearing menu name for 16_May_2017, By Thiyagu G.--%>
        <li class="clr15_hmenu"><a id="clearDivId" class="fnt_lbd" href="<%=GlobalConstants.WU_CONTEXT_PATH%>/university-clearing-<%=GlobalConstants.CLEARING_YEAR%>.html">Clearing <%=GlobalConstants.CLEARING_YEAR%></a></li>
      <%}
      if(!("CLEARING".equalsIgnoreCase(clearingUserType))){%>
        <li><a id="findDivId" class="fnt_lbd" href="/degrees/courses/">Find a course </a></li>
        <li><a class="fnt_lbd" href="<%=GlobalConstants.WU_CONTEXT_PATH%>/find-university/">Find a uni </a></li>
        <li><a class="fnt_lbd" href="<%=GlobalConstants.WU_CONTEXT_PATH%>/prospectus/">Prospectuses</a></li>
      <%}%>
      <li><a class="fnt_lbd" href="/open-days/"><spring:message code="virtual.tour.top.nav"/></a></li><%--1.1 --%>
      <li><a id="comparison_tb" class="fnt_lbd" href="<%=GlobalConstants.WU_CONTEXT_PATH%>/comparison">My Final 5 <span class="cmp_count" id="s_count_nav"><%=basketCount%></span></a></li>
      <li id="reviewTabChg"><a id="dsk_reviews" class="fst fnt_lbd subNavDkLink" href="/university-course-reviews/">Reviews</a></li>
      <li><a class="subNavDkLink" href="/university-course-reviews/">Read reviews</a></li>
      <spring:message code="review.form.url" var="reviewFormUrl"/>
      <li><a class="subNavDkLink" href="${reviewFormUrl}">Write a review</a></li>
      <li><a class="subNavDkLink" href="/student-awards-winners/university-of-the-year/">WUSCA winners <%=curYear%></a></li>
      <li id="adviceTabChg" class="nactv"><a class="fnt_lbd desk_adv_act" id="deskNavTop" href="/advice/">Advice</a></li>
      <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
      <li><a href="${teacherSectionUrl}" class="subNavDkLink" title="Covid-19 Updates"><spring:message code="wuni.covid.label"/></a></li> <%--Added teacher category on 04_Mar_2020, By Sri Sankari R--%>
      <li><a href="/advice/research-and-prep/" class="subNavDkLink" title="Research &amp; Prep">Research &amp; Prep</a></li>
      <li><a href="/advice/accommodation/" class="subNavDkLink" title="Accommodation">Accommodation</a></li>
      <li><a href="/advice/student-life/" class="subNavDkLink" title="Student Life">Student Life</a></li>
      <li><a href="/advice/blog/" class="subNavDkLink" title="Blog">Blog</a></li>
      <li><a href="/advice/news/" class="subNavDkLink" title="News">News</a></li>
      <li><a href="/advice/guides/" class="subNavDkLink" title="Guides">Guides</a></li>
      <li><a href="/advice/parents/" class="subNavDkLink" title="Parents">Parents</a></li> 
      
      <%--<li class="mfnl_bgcol"><a class="fnt_lbd" href="<%=GlobalConstants.WU_CONTEXT_PATH%>/myfinalchoice.html">My Final 5 <span id="mychNavCnt" class="cmp_count"><%=fnChNavCnt%></span></a></li>--%>      
      <li><a class="subNavDkLink" href="/whatuni-mobile-app" target="_blank">App</a></li>      
    </ul>
    <div class="mn_icons nw pst_usr_hde soc_flft">
      <a href="#" class="ic_pl mr0 noalert"></a>
      <a title="WhatUni on Twitter" rel="nofollow" href="http://www.twitter.com/whatuni" target="_blank" class="icon_t noalert"></a>      
      <a title="WhatUni on Facebook" rel="nofollow" href="http://www.facebook.com/whatuni" target="_blank" class="icon_f noalert"></a>
      <div id="socialMediaPod" class="flt_lst"></div>
    </div>
  </nav>
</amp-sidebar>
