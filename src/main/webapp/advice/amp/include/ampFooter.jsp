<%@page import="WUI.utilities.CommonFunction" autoFlush="true"%>
<%--
  * @purpose  This is AMP page side bar jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 21-Feb-2017 Prabhakaran V.               562     First Draft                                                       562 
  * *************************************************************************************************************************
--%>
<% String htmlText = new CommonFunction().getHTMLEditorText("AMPF","0");%>

<%=htmlText%>

