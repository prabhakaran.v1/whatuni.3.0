<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<%--
  * @purpose  This is Similar article pod section for AMP page..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 21-Feb-2017 Prabhakaran V.               562     First Draft                                                       562 
  * *************************************************************************************************************************
--%>

<section class="artfc p0">
  <div class="ad_cnr">
    <div class="content-bg">    
      <div class="clear"></div>
      <div class="borderbot fl mb25"></div>
      <h2>Similar articles</h2>
      <c:set var="listSize"> ${fn:length(requestScope.similarArticleList)} </c:set>
      <c:forEach var="similarList" items="${requestScope.similarArticleList}" varStatus="index">
        <ul class="art-lst">
          <li>
          <%String postText = "";%>
          <c:if test="${not empty similarList.postTitle}">
            <c:set var="postTxt" value="${similarList.postTitle}"/>
          <%postText = pageContext.getAttribute("postTxt").toString().replaceAll("\"", "");%>
          </c:if>
            <div class="art-view">
              <div class="img_ppn">
              <c:if test="${not empty similarList.imageName}">
                  <a href="<c:out value="${similarList.adviceDetailURL}"/>" title="<%=postText%>">
                    <amp-img layout="responsive" class="lazy-load" width="225" height="150" src="<c:out value="${similarList.imageName}"/>" alt="<c:out value="${similarList.mediaAltText}"/>" title="<%=postText%>" ></amp-img>
                  </a> 
                </c:if>
              </div>
              <div class="art-inc">
                <div class="art-desc">
                  <p class="cmart_txt"><c:out value="${similarList.personalisedText}"/></p>
                  <h3><a href="<c:out value="${similarList.adviceDetailURL}"/>" title="<%=postText%>"><c:out value="${similarList.postTitle}"/></a></h3>                  
                </div>                
              </div>              
            </div> 
          </li>           
        </ul> 
      </c:forEach>
      <%--Load the similar and most popular article pod through ajax call, added on 13_Jun_2017, By Thiyagu G.--%>      
      <section class="artfc p0 mt38 cm_art_pd" id="mostArticlesPodButtom"></section>
    </div>
  </div>
</section>