<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
  String commonCssName = CommonUtil.getResourceMessage("wuni.common.css", null);
  String cdScreensCssName = CommonUtil.getResourceMessage("wuni.whatuni.cd.screens.css", null);
  String mainHeaderCSS = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);//Added this css for Header changes by Hema.S on 23_OCT_2018_REL
  String ampPageUrl = (String)request.getAttribute("ampPageUrl");
  String facebookLoginJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.facebook.login.js");
  String commonUserProfileJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.user.profile.js");    
%>
<body>
<div id="newvenue" class="dialog" >
      <div id="newvenueform" style="display: none;" ></div>
      <div id="ajax-div" class="lb_pload" style="display: none;">
        <img src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/wu_load_icon.gif",0)%>" alt="loading image" title="" />
        <span id="txt">Please wait</span>
        <input type="button" onclick="hm('newvenue');" value="Cancel" class="button" />
      </div>
    </div>
    <c:if test="${empty sessionScope.userInfoList}">
      <div id="mbox1" style="display: block; width:100%; height: auto;" class="res_lb amp_silb">
        <span>
          <div id="mbd1">
            <div id="newvenueform1" style="display: block;">
              <div class="comLgh" id="lbDivNew1">
                <div class="fcls nw"><a class="" href="javascript:closeAmpPage();"><i class="fa fa-times"></i></a></div>
                <div class="pform nlr bgw" id="lbsighuplogin" style="display:block;">
                  <div class="reg-frm">
                    <div class="sgnm pt13 remv_gplus">
                      <div class="sgn">
                      <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
                        <h1 class="txt_cnr mb22">Sign up with</h1>
                          <fieldset class="so-btn">
                            <a class="btn1 fbbtn" onclick="loginInfoFacebook('fblogin');" id="ab_fb_nav" title="Login with Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK<i class="fa fa-long-arrow-right"></i></a> 
                          </fieldset>
                        </c:if>
                      </div>
                      <div class="sgupm">
                        <p class="comptx">You can also <a title="sign up with email" id="ab_signup_nav" class="link_blue" onclick="javascript:showLightBoxLoginForm('popup-newlogin','650', '500', 'noncomparereg');">sign up with email</a></p>
                      </div>
                      <div class="sgala">
                        <div class="borderbot mb20 mt35"></div>
                        <div class="sgalai">
                          <p class="comptx">Already have an account? <a title="Sign in" id="ab_signin_nav" class="link_blue" onclick="javascript:showLightBoxLoginForm('popup-newlogin','650', '500', 'loginCb');">Sign in</a></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <input type="hidden" id="isMobileUA" />
                <input type="hidden" id="nonAdvLogoFlag" />
                <input type="hidden" id="referrerURL_GA" value="<%=GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName()%>/register"/>
                <input type="hidden" id="regLoggingType" value="AMP page"/>
                <input type="hidden" id="ampPage" value="true" />
                <input type="hidden" name="jsPathname" id="contextJsPath" value="<%=CommonUtil.getJsPath()%>"/>
                <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
                <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />
              </div>
            </div>
          </div>
        </span>
      </div>
    </c:if>
    <input type="hidden" name="ampPageUrl" id="ampPageUrl" value="<%=ampPageUrl%>" />
    <input type="hidden" name="facebookLoginJSName" id="facebookLoginJSName" value="<%=facebookLoginJSName%>" />
    <input type="hidden" name="commonUserProfileJSName" id="commonUserProfileJSName" value="<%=commonUserProfileJSName%>" />
    <jsp:include page="/jsp/common/includeJS.jsp"/>
    <c:if test="${not empty sessionScope.userInfoList}">
      <script type="text/javascript">
        closeAmpPage();
      </script>
    </c:if>
    <script type="text/javascript">
      initmb();
      dynamicLoadJS('//connect.facebook.net/en_US/all.js');
      dynamicLoadJS(document.getElementById("contextJsPath").value+'/js/<%=facebookLoginJSName%>');
    </script>
  </body>