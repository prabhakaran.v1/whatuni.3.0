<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants, WUI.utilities.SessionData, spring.util.GlobalMethods" autoFlush="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

 <%    
    CommonFunction commonFun = new CommonFunction();
    String metaTitle = (request.getAttribute("metaTitle")!=null) ? request.getAttribute("metaTitle").toString() : "";
    String metaKeywords = (request.getAttribute("metaKeywords")!=null) ? request.getAttribute("metaKeywords").toString() : "";
    String metaDescription = (request.getAttribute("metaDescription")!=null) ? request.getAttribute("metaDescription").toString() : ""; 
    String ogImage = (request.getAttribute("ogImage")!=null) ? request.getAttribute("ogImage").toString() : "";     
    String requestURL = (String)request.getAttribute("currentPageUrl");
    String ptURL = requestURL!=null? requestURL.replace("/degrees/" ,"/") :requestURL;
    String primaryCategory = (request.getAttribute("primaryCategory")!=null) ? request.getAttribute("primaryCategory").toString() : "";
    String articleId = (request.getAttribute("articleId")!=null) ? request.getAttribute("articleId").toString() : "";
    String parentCategory = (request.getAttribute("parentCategory")!=null) ? request.getAttribute("parentCategory").toString() : "";
    String secondaryCategory = (request.getAttribute("secondaryCategory")!=null) ? request.getAttribute("secondaryCategory").toString() : "";    
    String artPCategory = parentCategory.replaceAll("-"," ").toUpperCase();
    String artSCategory = secondaryCategory.replaceAll("-"," ").toUpperCase();
    String domainSpecificPath = commonFun.getSchemeName(request)+ CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
    String className = "artCont";
    String gaCollegeName = "";
    
    String enableUcasGuideRightPod = commonFun.getWUSysVarValue("UCAS_GUIDE_RIGHT_POD");
    new GlobalMethods().setCovid19SessionData(request, response);
    String COVID19_SYSVAR = new SessionData().getData(request, GlobalConstants.COVID19_SYSVAR);
    request.setAttribute("COVID19_SYSVAR", COVID19_SYSVAR);
  %>
<body id="hcbody">
  <jsp:include page="/advice/amp/include/ampHeader.jsp"/>
  <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
  <c:if test="${COVID19_SYSVAR eq 'ON'}">
  <div class="covid_ltms">
    <span class="covid_txt"><spring:message code="wuni.covid.label"/></span>
	<a href="${teacherSectionUrl}" class="covid_link"><span class="linfo">Latest info</span><i class="fa fa-long-arrow-right"></i></a>
  </div>
</c:if>

  <jsp:include page="/advice/amp/include/ampSideBar.jsp"/>
  <%String mappingPath = (request.getAttribute("mappingPath")!=null) ? request.getAttribute("mappingPath").toString() : "";%>
  <div class="article_main cf">
  <c:if test="${not empty requestScope.adviceDetailInfoList}">
      <c:if test="${'N' eq sessionScope.sessionData['cookiePerformanceDisabled']}">
        <%--GA logging code starts--%>
        <amp-analytics type="googleanalytics" id="analytics2">
          <script type="application/json">
            {
              "vars": {
                "account": "<%=GlobalConstants.GA_ACCOUNT%>"
              },
              "triggers": {
                "trackPageviewWithAmpdocUrl": {
                  "on": "visible",
                  "request": "pageview",
                  "vars": {
                    "title": "AMP page",
                    "ampdocUrl": "<%=ptURL%>"
                  }
                }
              }
            }
          </script>
        </amp-analytics>
        <amp-analytics type="googleanalytics" id="analytics2">
          <script type="application/json">
            {
              "vars": {
                "account": "<%=GlobalConstants.INSIGHT_ACCOUNT%>"
              },
              "triggers": {
                "trackPageviewWithAmpdocUrl": {
                  "on": "visible",
                  "request": "pageview",
                  "vars": {
                    "title": "AMP page",
                    "ampdocUrl": "<%=ptURL%>"
                  }
                }
              }
            }
          </script>
        </amp-analytics>
        <%--End of GA logging codes--%>
        </c:if>
        <c:forEach var="adviceDetailInfo" items="${requestScope.adviceDetailInfoList}" varStatus="i">
        <c:if test="${not empty adviceDetailInfo.postId}">
            <c:set var="adviceViewPostId" value="${adviceDetailInfo.postId}"/>
            <%
              request.setAttribute("advice_view", pageContext.getAttribute("adviceViewPostId").toString());
              if(!GenericValidator.isBlankOrNull(enableUcasGuideRightPod) && enableUcasGuideRightPod.contains(pageContext.getAttribute("adviceViewPostId").toString())){
                className = "artCont clr15";
              }
            %>
          </c:if>
          <c:if test="${not empty adviceDetailInfo.postTitle}">
            <c:set var="definePostTitle" value="${adviceDetailInfo.postTitle}"/>
            <section class="bg_white p0">
	             <div class="ad_cnr">
                <div class="content-bg">
                  <div class="artMain">
                    <div class="artMainLft fl">
                      <hgroup>
                        <h5 class="txt_upr cl_grylt">${adviceDetailInfo.subCategoryName}</h5>
                        <h1>${adviceDetailInfo.postTitle}</h1>
                      </hgroup>
                      <p class="fnt_lrg fnt_12 lh_18 cl_grylt">${adviceDetailInfo.firstParagraph}</p>
                      <c:if test="${not empty adviceDetailInfo.pdfId}">
                          <%String domainSchemeName = GlobalConstants.WHATUNI_SCHEME_NAME;%>
                          <div class="pdf-download row-fluid">                                
                            <a id="pdf<c:out value="${adviceDetailInfo.pdfId}"/>" href="<%=domainSchemeName%><c:out value="${adviceDetailInfo.pdfUrl}"/>" target="_blank" class="fnt_lrg ml5 fl fnt_14 link_blue">
                              <amp-img width="16" height="16" layout="responsive" src="<%=CommonUtil.getImgPath("/wu-cont/images/icon_pdf1.jpg",0)%>" alt="PDF Icon" class="pdf_icon"></amp-img> Download as a pdf 
                            </a>
                            <a  id="pdfHidden<c:out value="${adviceDetailInfo.pdfId}"/>" href="<%=domainSchemeName%><c:out value="${adviceDetailInfo.pdfUrl}"/>" target="_blank"></a>
                          </div>
                      </c:if>
                    </div>  
                    <c:if test="${not empty requestScope.sponsorDetailList}">  
                    <c:forEach var="sponsorDetailList" items="${requestScope.sponsorDetailList}" >
                      <c:if test="${not empty sponsorDetailList.sponsorUrl and sponsorDetailList.collegeNameDisplay}">
                           <c:set var="collegeName" value="${sponsorDetailList.collegeNameDisplay}"/>        
                              <%gaCollegeName = commonFun.replaceSpecialCharacter(pageContext.getAttribute("collegeName").toString()).toLowerCase(); %>
                          <div class="artMainRgt fr">
                            <figcaption class="fntfy">In association with</figcaption>                        
                            <figure>
                              <a href="<c:out value="${sponsorDetailList.sponsorUrl}"/>" id="sprAncId" target="_blank"  title="In association with "><amp-img layout="responsive" src="<%=CommonUtil.getImgPath("",0)%><c:out value="${sponsorDetailList.mediaPath}"/>" alt="In association with <c:out value="${sponsorDetailList.collegeNameDisplay}"/>" title="In association with <c:out value="${sponsorDetailList.collegeNameDisplay}"/>" width="97" height="75" ></amp-img></a>
                            </figure>
                          </div>
                          <c:if test="${'N' eq sessionScope.sessionData['cookiePerformanceDisabled']}">
                           <%--Start of GA logging codes--%>
                          <amp-analytics type="googleanalytics" id="analytics2">
                            <script type="application/json">                            
                            {
                              "vars": {
                                "account": "<%=GlobalConstants.GA_ACCOUNT%>"
                              },
                              "triggers": {
                                "trackEvent" : {
                                  "on": "click",
                                  "selector": "#sprAncId",
                                  "request": "event",
                                  "vars": {
                                    "eventCategory": "interaction",
                                    "eventAction": "Webclick Sponsored",                  
                                    "eventLabel": "<%=gaCollegeName%>",
                                    "eventValue": "1" 
                                  }
                                }
                              }
                            }                            
                            </script>
                          </amp-analytics>
                           <%--End of GA logging codes--%>
                           </c:if>
                        </c:if>
                      </c:forEach>
                    </c:if>
                  </div>
                </div>
              </div>
            </section>
            <section class="bg_gtrylt p0">
              <div class="ad_cnr art_pic">
                <div class="content-bg">
                  <div class="artSec">
                    <div class="artSecInn">
                      <section class="p0">
                        <article class="aLt">
                          <div class="aibr">
                          <c:if test="${not empty adviceDetailInfo.authorImageName}">
                              <figure class="pb15">
                              <c:if test="${not empty adviceDetailInfo.authorGoogleAccount}">
                                  <a target="_blank" href="<c:out value="${adviceDetailInfo.authorGoogleAccount}"/>" title="<c:out value="${adviceDetailInfo.authorName}"/>">
                                    <amp-img layout="responsive" width="60" height="60" src="<c:out value="${adviceDetailInfo.authorImageName}"/>" alt="<c:out value="${adviceDetailInfo.authorName}"/>" title="<c:out value="${adviceDetailInfo.authorName}"/>"> </amp-img>
                                  </a>
                                </c:if>
                                <c:if test="${empty adviceDetailInfo.authorGoogleAccount}">
                                <amp-img layout="responsive" width="60" height="60" src="<c:out value="${adviceDetailInfo.authorImageName}"/>" alt="<c:out value="${adviceDetailInfo.authorName}"/>" title="<c:out value="${adviceDetailInfo.authorName}"/>" ></amp-img>                                    
                              </c:if>
                            </figure>
                          </c:if>
                        </div>
                        <div class="divAlt">
                          <div class="fnt_lrg fnt_13 lh_24 cl_grylt fontDe">by
                          <c:if test="${not empty adviceDetailInfo.authorGoogleAccount}">
                              <a class="link_blue fntFL" target="_blank" href="<c:out value="${adviceDetailInfo.authorGoogleAccount}"/>"><c:out value="${adviceDetailInfo.authorName}"/></a>
                            </c:if>
                            <c:if test="${empty adviceDetailInfo.authorGoogleAccount}">
                              <span>${adviceDetailInfo.authorName}</span>
                            </c:if>
                          </div>
                          <c:if test="${not empty adviceDetailInfo.updatedDate}"><div class="fnt_lrg fnt_13 lh_24 cl_grylt divAltF"><i class="fa fa-clock-o pr5"></i>Last Updated: <div class="ad_ludate"><c:out value="${adviceDetailInfo.updatedDate}"/></div></div></c:if>
                          <c:if test="${not empty adviceDetailInfo.createdDate}"><div class="fnt_lrg fnt_13 lh_24 cl_grylt divAltF"><i class="fa fa-clock-o pr5"></i>First Published: <div class="ad_fbdate"><c:out value="${adviceDetailInfo.createdDate}"/></div></div></c:if>
                          <%-- <c:if test="${not empty adviceDetailInfo.readCount}"><div class="fnt_lrg fnt_13 lh_24 cl_grylt divAltF"><i class="fa fa-eye pr5"></i><c:out value="${adviceDetailInfo.readCount}"/></div></c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                        </div>
                      </article>
                      <div class="box_cnr">
                        <article class="aMd img_ppn">
                          <%String mediaPath = ""; String mediaAlt = "";%>
                          <c:if test="${not empty adviceDetailInfo.mediaAltText}">
                            <c:set var="mediaAltTxt" value="${adviceDetailInfo.mediaAltText}"/>
                            <%mediaAlt = pageContext.getAttribute("mediaAltTxt").toString().replaceAll("\"", "");
                            %>
                          </c:if>
                          <c:if test="${not empty adviceDetailInfo.imageName}">
                            <figure>
                            <c:if test="${adviceDetailInfo.mediaType eq 'PICTURE'}">
                                <c:set var="defineImageName" value="${adviceDetailInfo.imageName}"/>
                                <amp-img class="lazy-load" width="595" height="330" layout="responsive" src="<c:out value="${adviceDetailInfo.imageName}"/>" alt="<%=mediaAlt%>" ></amp-img>
                                <%mediaPath = pageContext.getAttribute("defineImageName").toString() ;%>
                              </c:if>
                              <c:if test="${adviceDetailInfo.mediaType eq 'VIDEO'}">
                                <a  id="img_std_choice" style="display:block;">
                                  <i id="img_std_choice_icon" class="fa fa-play-circle-o fa-8x"></i>        
                                  <c:set var="defineVideoImageName" value="${adviceDetailInfo.imageName}"/>
                                  <%mediaPath = pageContext.getAttribute("defineVideoImageName").toString() ;%>
                                  <amp-img id="std_video" class="lazy-load" width="595" height="330" layout="responsive" src="<c:out value="${adviceDetailInfo.imageName}"/>" alt="<%=mediaAlt%>"></amp-img>
                                </a>
                              </c:if>
                            </figure>
                          </c:if>
                        </article>
                        <%--Share icons--%>
                        <article class="aRt">
                          <div class="socI">
                            <ul>
                              <li><a href="https://www.facebook.com/sharer/sharer.php?u=<%=domainSpecificPath%><%=mappingPath%>" class="fbI" title="facebook" target="_blank"></a></li>
                              <li><a href="https://twitter.com/intent/tweet?url=<%=domainSpecificPath%><%=mappingPath%>" class="twI" title="twitter" target="_blank"></a></li>
                              <%-- <li><a href="https://plus.google.com/share?url=<%=domainSpecificPath%><%=mappingPath%>" class="gI" target="_blank"></a></li> --%>
                              <li><a href="https://www.pinterest.com/pin/create/button/?url=<%=domainSpecificPath%><%=mappingPath%>&media=<%=mediaPath%>&description=<%=pageContext.getAttribute("definePostTitle").toString().replaceAll("\"", "")%>" class="atI" target="_blank"></a></li>
                            </ul>
                          </div>
                        </article>
                        <%--End of share icon code--%>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </section>
          </c:if>
          <section class="p0">
            <div class="ad_cnr">
              <div class="content-bg">
                <div class="<%=className%>">
                  <article>
                    <aside class="artLCo mhc">
                    <c:if test="${COVID19_SYSVAR eq 'ON' and primaryCategory eq 'CLEARING'}">
                          
                          <div class="covid_upt">
	                        <span class="covid_txt"><%=new SessionData().getData(request, "COVID19_ARTICLE_TEXT")%></span>
	                        <a href="${teacherSectionUrl}" class="covid_link"><span class="linfo">Latest info</span><i class="fa fa-long-arrow-right"></i></a>
                          </div>
                        </c:if>
                    <c:if test="${not empty adviceDetailInfo.pullQuote}">
                        <div class="pQu">
                          <figure>
                            <amp-img layout="responsive" src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/open_quo.jpg" width="52" height="44" alt="open quote" ></amp-img>
                          </figure>
                          <p class="txt_posi_blo m0 fl"><c:out value="${adviceDetailInfo.pullQuote}"/></p>
                          <figure>
                            <amp-img layout="responsive" src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/close_quo.jpg" width="52" height="43" alt="close quote" ></amp-img>
                          </figure>
                        </div>
                      </c:if>
                      <p>${adviceDetailInfo.lastParagraph}</p> 
                    </aside>
                  </article>
                  <aside class="mstPop fr">
                    <%--Most popular pod --%>
                    <c:if test="${not empty requestScope.mostPopularAdviceList}">
                        <div id="MostPulrPodDiv">
                          <h2 class="pb15">Top articles for you</h2>                                    
                          <ul class="rgt-lst">
                          <c:forEach var="mostPopularAdvice" items="${requestScope.mostPopularAdviceList}" varStatus="i" >
                              <li>
                                <figure>
                                  <a href="<c:out value="${mostPopularAdvice.adviceDetailURL}"/>">
                                  <c:if test="${not empty mostPopularAdvice.imageName}">
                                      <%String imgAlt = "";String imgTitle = "";%>
                                      <c:if test="${not empty mostPopularAdvice.mediaAltText}">
                                        <c:set var="imgAltTxt" value="${mostPopularAdvice.mediaAltText}"/>
                                        <%imgAlt = pageContext.getAttribute("imgAltTxt").toString().replaceAll("\"", "");%>
                                      </c:if>
                                      <c:if test="${not empty mostPopularAdvice.postTitle}">
                                        <c:set var="imgTitleTxt" value="${mostPopularAdvice.postTitle}"/>
                                        <%imgTitle = pageContext.getAttribute("imgTitleTxt").toString().replaceAll("\"", "");%>
                                      </c:if>
                                      <amp-img layout="responsive" width="140" height="92" src="<c:out value="${mostPopularAdvice.imageName}"/>" alt="<%=imgAlt%>" title="<%=imgTitle%>"></amp-img>
                                    </c:if>
                                  </a>
                                </figure>
                                <p><span class="cmart_txt"><c:out value="${mostPopularAdvice.postShortText}"/></span>&nbsp;<a href="<c:out value="${mostPopularAdvice.adviceDetailURL}"/>"><c:out value="${mostPopularAdvice.postTitle}"/></a></p>
                              </li>
                            </c:forEach>
                          </ul>
                        </div>
                    </c:if>
                    <%--End of most popular pod--%>
                    <%--Next step pod start--%>
                    <c:if test="${not empty requestScope.browseSearchUrl}">
                        <div class="yns">
                          <div class="mstPop nxs nobord">
                            <h2 class="txt_cnr">Your next steps</h2>
                            <a title="FIND DEGREES" href="<c:out value="${requestScope.browseSearchUrl}"/>" class="btn1 blue mt23">FIND DEGREES<i class="fa fa-long-arrow-right"></i></a>
                            <a title="MORE GUIDES" class="btn1 bg_orange marBtm" href="/advice/guides/subject-guides/">MORE GUIDES<i class="fa fa-long-arrow-right"></i></a>
                          </div>
                        </div>
                    </c:if>
                    <%--End of next step pod--%>
                  </aside>
                </div>
                <div class="fclear"></div>
              </div>
            </div>
          </section>
          <%--Similar articels code start--%>
          <c:if test="${empty requestScope.browseSearchUrl}">
          <c:if test="${not empty requestScope.similarArticleList}">
                <jsp:include page="/advice/amp/include/ampSimilarArticlePod.jsp"/>
            </c:if>
            <c:if test="${not empty requestScope.articlesList}">
                <jsp:include page="/advice/amp/include/ampTopArticlesForYouPod.jsp"/>
            </c:if>
            <article class="fl sall_art">
             <a class="btn1" href="/advice/">SEE ALL ARTICLES<i class="fa fa-long-arrow-right"></i></a>
            </article>
          </c:if>
          <%--End of similar articles pod--%>          
          <section class="p0"><div class="ad_cnr ad_cour"><div class="content-bg"></div></div></section>    
          <div class="flyOut"></div> 
        </c:forEach>
    </c:if>
  </div>
  <jsp:include page="/advice/amp/include/ampFooter.jsp"/>
</body>
