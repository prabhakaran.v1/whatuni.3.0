<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonFunction, WUI.utilities.CommonUtil" %>

<%    
  CommonFunction commonFun = new CommonFunction();
  String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
  String domainSpecificPath = httpStr + java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
  String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
  String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
  String envronmentName = commonFun.getWUSysVarValue("WU_ENV_NAME");
  String adviceJsName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.adviceJsName.js");
  String totalRecordCount = (request.getAttribute("totalRecordCount")!=null) ? request.getAttribute("totalRecordCount").toString() : "0";    
   int noOfPage = (Integer.parseInt(totalRecordCount)/20);
   if((Integer.parseInt(totalRecordCount)%20) > 0) {
     noOfPage++; 
   }  
   String pageNo = (request.getAttribute("page")!=null) ? request.getAttribute("page").toString() : "1";
   String pageName = (request.getAttribute("pageName")!=null) ? request.getAttribute("pageName").toString() : "1";
   String mappingPath = (request.getAttribute("mappingPath")!=null) ? request.getAttribute("mappingPath").toString() : "";
   String parentCategory = (request.getAttribute("parentCategory")!=null) ? request.getAttribute("parentCategory").toString() : "";
   String artPCategory = parentCategory.replaceAll("-"," ").toUpperCase();
   String primaryCategory = "";
   String secondaryCategory = "";
   String nextUrl = "";     
   String previousUrl = "";
   int numofpagesplus = (Integer.parseInt(pageNo)+1);
   int numofpagesminus = (Integer.parseInt(pageNo)-1);
   String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
   String canonUrl = httpStr + "www.whatuni.com"+mappingPath+"/";
   String pCategoryDisplay = "";
   if("research-and-prep".equals(parentCategory)){
      pCategoryDisplay = "Research & Prep";
   }else if("student-life".equals(parentCategory)){
      pCategoryDisplay = "Student Life";
   }else if("blog".equals(parentCategory)){
      pCategoryDisplay = "Blog";
   }else if("news".equals(parentCategory)){
      pCategoryDisplay = "News";
   }else if("guides".equals(parentCategory)){
      pCategoryDisplay = "Guides";
   }else if("clearing".equals(parentCategory)){
      pCategoryDisplay = "Clearing";
   }else if("parents".equals(parentCategory)){
      pCategoryDisplay = "Parents";
   }else if("accommodation".equals(parentCategory)){
      pCategoryDisplay = "Accommodation";
   }else if("coronavirus".equalsIgnoreCase(parentCategory)){  //Added Teacher category on 04_Mar_2020, By Sri Sankari R
	  String covidLabel = CommonUtil.getResourceMessage("wuni.covid.label", null);
	  pCategoryDisplay = covidLabel;
   }
   pageContext.setAttribute("parentCategory", parentCategory);
%>
<link rel="canonical" href="<%=canonUrl%>"/> 
  <%if(noOfPage>1){
     if("1".equals(pageNo)){       
        nextUrl = httpStr + "www.whatuni.com"+mappingPath+"/?page="+numofpagesplus;
     %>
        <link rel="next" href="<%=nextUrl%>" />
     <%}else if(noOfPage == Integer.parseInt(pageNo)){
        previousUrl = httpStr + "www.whatuni.com"+mappingPath+"/?page="+numofpagesminus;
     %>
        <link rel="prev" href="<%=previousUrl%>" />
     <%}else{
        nextUrl = httpStr + "www.whatuni.com"+mappingPath+"/?page="+numofpagesplus;
        previousUrl = httpStr + "www.whatuni.com"+mappingPath+"/?page="+numofpagesminus;
     %>
        <link rel="prev" href="<%=previousUrl%>" />
        <link rel="next" href="<%=nextUrl%>" />
     <%}}
     String indexFollow = "noindex,follow";
     if(Integer.parseInt(pageNo) == 1){
       indexFollow = "index,follow";
    }%>
<body>  
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
</header>
<!--Article Page Starts-->
<div class="article_main pb50 cf">
    <div id="stickyTop" class="">
        <div class="article_land_cnr mb25 pb15">    
            <div class="ad_cnr">
                <div class="content-bg">
                    <div class="sbcr mb-10 pros_brcumb">
                        <div class="bc">
                            <div class="bcrumb_blue">                                
                                <% String bCrumb = (String)request.getAttribute("adviceLandingBreadCrumb"); 
                                if(bCrumb!=null && !"".equals(bCrumb)){%>
                                <%=bCrumb%>
                                <%}%>
                            </div>
                        </div>
                    </div>
                    <!-- Uni and study advice -->
                    <section class="asrc p0">
                        <div class="asrcc">
                            <h1><%=pCategoryDisplay%></h1>
                        </div>
                        <div class="asrcb">
                            <div class="pros_search fl w100p">
                                <form id="articleSearchForm" method="post" action="">
                                    <fieldset class="fl">
                                        <%if(request.getAttribute("keyword") != null) {
                                                String keyword = request.getAttribute("keyword").toString();
                                                %>
                                                <input class="fnt_lrg tx_bx" autocomplete="off" name="keyword" id="keyword" value="<%=keyword%>" type="text" onkeypress="return keypressSubmit('1', 'primaryCat', event);" /> 
                                                <%
                                            }else{%>
                                                <input class="fnt_lrg tx_bx" autocomplete="off" name="keyword" id="keyword" value="Enter keyword" type="text" onkeypress="return keypressSubmit('1', 'primaryCat', event);" onblur="if(this.value == ''){this.value='Enter keyword';this.style.color='#a6a6a6';}" onfocus="if(this.value == 'Enter keyword'){this.value = '';this.style.color='#000';}"> 
                                            <%}%>
                                            <a class="icon_srch fr" onclick="return adviceSearchSubmit('1','primaryCat');"><i class="fa fa-search fa-1_5x"></i></a>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </section>					
                </div>
            </div>
        </div>
    </div>
    <!-- Article Main Heading -->
    <section class="p0">
	<div class="ad_cnr">
            <div class="content-bg">
		<div class="artMain">
                    <section class="asadv p0">
                        <!-- Article Nav -->                        
                        <div id="stickyAdvice">
                            <jsp:include page="/advice/include/adviceLeftNav.jsp">                            
                                <jsp:param name="menuName" value="Blog" />                                    
                            </jsp:include>
                        </div>
                        <div id="leftNavMobile">
                            <jsp:include page="/advice/include/adviceLeftNavForMobile.jsp" />                            
                        </div>  
                        <div class="art-prod">
                          <!-- covid 19 reg start -->
                          <c:if test="${parentCategory eq 'coronavirus'}">
                           
                           <jsp:include page="/advice/include/covidHubRegistrationPod.jsp"/>
                          </c:if>
                          <!-- covid 19 reg end -->
                            <!-- 1 -->
                            <div class="art-bl">                                
                              <jsp:include page="/advice/include/primaryCategories.jsp">
                                <jsp:param name="subCategory" value="yes" />
                              </jsp:include>
                              <c:if test="${parentCategory eq 'coronavirus' and not empty covidSnippetList}">
                                <div class="art_ltstnws_sect">
                                  <h2><spring:message code="label.latest.news"/></h2>
                                  <jsp:include page="/advice/include/covidLatestNewsPod.jsp"/>
                                </div>
                              </c:if>    
                            </div>           
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <div class="flyOut"></div>
</div>
<!--Article Page Ends-->
<jsp:include page="/jsp/common/wuFooter.jsp" />
<c:if test="${parentCategory eq 'coronavirus'}">
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/advice/wu_covidHubRegistration_06052020.js"> </script>
</c:if>
</body>