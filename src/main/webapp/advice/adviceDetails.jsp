<%@page import="WUI.utilities.CommonUtil, java.util.ArrayList, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, WUI.utilities.SessionData, java.util.*, WUI.utilities.GlobalConstants" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>

<%    
  CommonFunction commonFun = new CommonFunction();
  String metaTitle = (request.getAttribute("metaTitle")!=null) ? request.getAttribute("metaTitle").toString() : "";
  String metaKeywords = (request.getAttribute("metaKeywords")!=null) ? request.getAttribute("metaKeywords").toString() : "";
  String metaDescription = (request.getAttribute("metaDescription")!=null) ? request.getAttribute("metaDescription").toString() : ""; 
  String ogImage = (request.getAttribute("ogImage")!=null) ? request.getAttribute("ogImage").toString() : "";     
  String requestURL = (String)request.getAttribute("currentPageUrl");
  String ptURL = requestURL!=null? requestURL.replace("/degrees/" ,"/") :requestURL;
  String primaryCategory = (request.getAttribute("primaryCategory")!=null) ? request.getAttribute("primaryCategory").toString() : "";
  String articleId = (request.getAttribute("articleId")!=null) ? request.getAttribute("articleId").toString() : "";
  String parentCategory = (request.getAttribute("parentCategory")!=null) ? request.getAttribute("parentCategory").toString() : "";
  String secondaryCategory = (request.getAttribute("secondaryCategory")!=null) ? request.getAttribute("secondaryCategory").toString() : "";    
  String artPCategory = parentCategory.replaceAll("-"," ").toUpperCase();
  String artSCategory = secondaryCategory.replaceAll("-"," ").toUpperCase();
  ResourceBundle rb = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources"); 
  String domainSpecificPath = commonFun.getSchemeName(request)+rb.getString("wuni.whatuni.device.specific.css.path");
  String main_480_ver = rb.getString("wuni.whatuni.main.480.css");
  String main_992_ver = rb.getString("wuni.whatuni.main.992.css");
  String adviceJsName = rb.getString("wuni.adviceJsName.js");  
  String lazyLoadJsName = rb.getString("wuni.lazyLoadJsName.js");
  String lazyLoadJs = rb.getString("wuni.lazyLoadJs.js");  
  String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
  boolean isUCASApplication = false;
  String className = "artCont";
  String enableUcasGuideRightPod = commonFun.getWUSysVarValue("UCAS_GUIDE_RIGHT_POD");   // Yogeswari :: 30.06.2015 :: added for UCAS CALCULATOR task.
  String ampHtmlUrl = ptURL + "?amp=true";
  String facebookLoginJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.facebook.login.js");  
  
%>
<%
    CommonFunction common = new CommonFunction();
    String mappingPath = (request.getAttribute("mappingPath")!=null) ? request.getAttribute("mappingPath").toString() : "";
    String tags = (request.getAttribute("tags")!=null) ? request.getAttribute("tags").toString() : "";
    if(tags!=null && !"".equals(tags)){
        tags = common.replaceHypen(common.replaceURL(tags)).toLowerCase();
%>

<%} %>
<body>
<c:if test="${not empty requestScope.sponsPixelTracking}">
  <c:out value="${requestScope.sponsPixelTracking}" escapeXml="false"/>
</c:if><%--Added pixel tracking for sponsor articles by Hema.S on NOV_20_2018_REL--%>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">        
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
</header>  
<!--Article Page Starts-->
<div class="article_main pb40 cf">
    <!-- Article Main Heading -->
    <c:if test="${not empty requestScope.adviceDetailInfoList}">
      <c:forEach var="adviceDetailInfo" items="${requestScope.adviceDetailInfoList}" varStatus="i">
        <c:set var="adviceViewPostId" value="${adviceDetailInfo.postId}"/>        
        <%
          request.setAttribute("advice_view", pageContext.getAttribute("adviceViewPostId").toString());
          if(!GenericValidator.isBlankOrNull(enableUcasGuideRightPod)){
            if(enableUcasGuideRightPod.contains(pageContext.getAttribute("adviceViewPostId").toString())){
              isUCASApplication = true;
              className = "artCont clr15";
            }
          }
        %>
    <section class="bg_white p0">
	  <div class="ad_cnr">
            <div class="content-bg">
                <div class="artMain">
                    <div class="sbcr mb-10 pros_brcumb">
                        <div class="bc">
                            <div class="bcrumb_blue">
                                <% String bCrumb = (String)request.getAttribute("adviceDetailBreadCrumb"); 
                                if(bCrumb!=null && !"".equals(bCrumb)){%>
                                <%=bCrumb%>
                                <%}%>
                            </div>
                        </div>
                    </div>
                    <div class="artMainLft fl">
                        <hgroup>
                            <h5 class="txt_upr cl_grylt"><c:out value="${adviceDetailInfo.subCategoryName}"/></h5>
                            <h1><c:out value="${adviceDetailInfo.postTitle}"/></h1>
                            <c:set var="definePostTitle" value="${adviceDetailInfo.postTitle}"/>
                        </hgroup>
                        <p class="fnt_lrg fnt_12 lh_18 cl_grylt">${adviceDetailInfo.firstParagraph}</p>
                        <c:if test="${not empty adviceDetailInfo.pdfId}">
                          <%String domainSchemeName = GlobalConstants.WHATUNI_SCHEME_NAME;%>
                              <div class="pdf-download row-fluid">                                
                                <a id="pdf<c:out value="${adviceDetailInfo.pdfId}"/>" href="#" onclick="openPDFDownload('<%=domainSchemeName%><c:out value="${adviceDetailInfo.pdfUrl}"/>','<c:out value="${adviceDetailInfo.pdfId}"/>','<c:out value="${adviceDetailInfo.guideName}"/>');" class="fnt_lrg ml5 fl fnt_14 link_blue">
                                <img width="" height="" src="<%=CommonUtil.getImgPath("/wu-cont/images/icon_pdf1.jpg",0)%>" alt="PDF Icon" class="pdf_icon">Download as a pdf </a>
                                <a  id="pdfHidden<c:out value="${adviceDetailInfo.pdfId}"/>" href="" target="_blank"></a>
                                <input type="hidden" id="userid" value='<%=new SessionData().getData(request, "y")%>' />
                              </div>  
                        </c:if>                    
                    </div>
                      <c:if test="${not empty requestScope.sponsorDetailList}">
                      <c:forEach var="sponsorDetailList" items="${requestScope.sponsorDetailList}">
                      <c:if test="${not empty sponsorDetailList.collegeNameDisplay}">
                           <c:set var="collegeName" value="${sponsorDetailList.collegeNameDisplay}"/>        
                              <%String gaCollegeName = commonFun.replaceSpecialCharacter(pageContext.getAttribute("collegeName").toString()).toLowerCase(); %>  
                              <c:if test="${not empty sponsorDetailList.sponsorUrl}">                       
                            <div class="artMainRgt fr">
                                <figcaption class="fntfy">In association with</figcaption>                        
                                <figure>
                                    <a href="<c:out value="${sponsorDetailList.sponsorUrl}"/>" target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${sponsorDetailList.collegeId}"/>','<c:out value="${sponsorDetailList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" title="In association with "><img src="<%=CommonUtil.getImgPath("",0)%><c:out value="${sponsorDetailList.mediaPath}"/>" alt="In association with <c:out value="${sponsorDetailList.collegeNameDisplay}"/>" title="In association with <c:out value="${sponsorDetailList.collegeNameDisplay}"/>" /></a>
                                </figure>
                            </div>
                          </c:if>
                        </c:if>
                      </c:forEach>
                    </c:if>
                    
                </div>
            </div>
	</div>
    </section>
    <section class="bg_gtrylt p0">
        <div class="ad_cnr art_pic">
            <div class="content-bg">
                <!-- Article Main Image Section -->
                <div class="artSec">
                    <div class="artSecInn">
                        <section class="p0">
                            <article class="aLt">
                                <div class="aibr">
                                <c:if test="${not empty adviceDetailInfo.authorImageName}">
                                <figure class="pb15">
                                <c:if test="${not empty adviceDetailInfo.authorGoogleAccount}">
                                    <!--Included target as blank for opening the link in new tab by Indumathi.S Jan-27-16 -->
                                    <a target="_blank" href="<c:out value="${adviceDetailInfo.authorGoogleAccount}"/>" title="<c:out value="${adviceDetailInfo.authorName}"/>">
                                        <img src="<c:out value="${adviceDetailInfo.authorImageName}"/>" alt="<c:out value="${adviceDetailInfo.authorName}"/>" title="<c:out value="${adviceDetailInfo.authorName}"/>" />
                                    </a>
                                    </c:if>
                                    <c:if test="${empty adviceDetailInfo.authorGoogleAccount}">
                                        <img src="<c:out value="${adviceDetailInfo.authorImageName}"/>" alt="<c:out value="${adviceDetailInfo.authorName}"/>" title="<c:out value="${adviceDetailInfo.authorName}"/>" />                                    
                                    </c:if>
                                </figure>
                                </c:if>
                                </div>
                                <div class="divAlt">
                                    <div class="fnt_lrg fnt_13 lh_24 cl_grylt fontDe">by
                                    <c:if test="${not empty adviceDetailInfo.authorGoogleAccount}">
                                        <a class="link_blue fntFL" target="_blank" href="<c:out value="${adviceDetailInfo.authorGoogleAccount}"/>"><c:out value="${adviceDetailInfo.authorName}"/></a>
                                    </c:if>
                                    <c:if test="${empty adviceDetailInfo.authorGoogleAccount}">
                                        <span><c:out value="${adviceDetailInfo.authorName}"/></span>
                                    </c:if>
                                    </div>
                                    <c:if test="${not empty adviceDetailInfo.updatedDate}"><div class="fnt_lrg fnt_13 lh_24 cl_grylt divAltF"><i class="fa fa-clock-o pr5"></i>Last Updated: <div class="ad_ludate"><c:out value="${adviceDetailInfo.updatedDate}"/></div></div></c:if>
                                    <c:if test="${not empty adviceDetailInfo.createdDate}"><div class="fnt_lrg fnt_13 lh_24 cl_grylt divAltF"><i class="fa fa-clock-o pr5"></i>First Published: <div class="ad_fbdate"><c:out value="${adviceDetailInfo.createdDate}"/></div></div></c:if>
                                    <%-- <c:if test="${not empty adviceDetailInfo.readCount}">
                                    <div class="fnt_lrg fnt_13 lh_24 cl_grylt divAltF"><i class="fa fa-eye pr5"></i><c:out value="${adviceDetailInfo.readCount}"/></div>
                                    </c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                                </div>
                            </article>
                            <div class="box_cnr">
                            <article class="aMd img_ppn">
                                <figure>
                                    <%String mediaPath = "";%>
                                    <c:if test="${adviceDetailInfo.mediaType eq 'PICTURE'}">
                                        <c:set var="defineImageName" value="${adviceDetailInfo.imageName}"/>
                                        <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${adviceDetailInfo.imageName}"/>" alt="<c:out value="${adviceDetailInfo.mediaAltText}"/>" />
                                        <%mediaPath = pageContext.getAttribute("defineImageName").toString();%>
                                    </c:if>
                                    <c:if test="${adviceDetailInfo.mediaType eq 'VIDEO'}">
                                        <a  id="img_std_choice" onclick="showhide('video_emb_std_choice','img_std_choice','<c:out value="${adviceDetailInfo.videoUrl}"/>','100%',330,'<c:out value="${adviceDetailInfo.imageName}"/>')" style="display:block;">
                                          <i id="img_std_choice_icon" class="fa fa-play-circle-o fa-8x"></i>        
                                          <c:set var="defineVideoImageName" value="${adviceDetailInfo.imageName}"/>
                                          <%mediaPath = pageContext.getAttribute("defineVideoImageName").toString();%>
                                          <img id="std_video" class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${adviceDetailInfo.imageName}"/>" alt="<c:out value="${adviceDetailInfo.mediaAltText}"/>">
                                        </a>
                                        <iframe id="video_emb_std_choice" frameborder="0" style="display:none;" allowfullscreen=""></iframe>
                                        
                                        
                                    </c:if>                                   
                                </figure>
                            </article>
                            <article class="aRt">
                                <!--<i class="fa fa-camera fa-1_5x cl_grylt"></i>
                                <p class="fnt_lrg fnt_12 lh_18 cl_grylt accr">These unis tend to be the ones that top the ranking tables each year.</p>
                                <div class="artHt">
                                    <div class="shlst"> 
                                        <a href="javascript:;">
                                            <span style="display:;" class="">Add to MyWhatUni<em></em></span>
                                        </a>
                                    </div>
                                </div>-->
                                <div class="socI">
                                    <ul>
                                        <li><a href="https://www.facebook.com/sharer/sharer.php?u=<%=domainSpecificPath%><%=mappingPath%>" class="fbI" title="facebook" target="_blank"></a></li>
                                        <li><a href="https://twitter.com/intent/tweet?url=<%=domainSpecificPath%><%=mappingPath%>" class="twI" title="twitter" target="_blank"></a></li>
                                        <%-- <li><a href="https://plus.google.com/share?url=<%=domainSpecificPath%><%=mappingPath%>" class="gI" target="_blank"></a></li> --%>
                                        <li><a href="https://www.pinterest.com/pin/create/button/?url=<%=domainSpecificPath%><%=mappingPath%>&media=<%=mediaPath%>&description=<%=(String)pageContext.getAttribute("definePostTitle")%>" class="atI" target="_blank"></a></li>
                                        <!--<li><a href="#" class="smI"></a></li>-->
                                    </ul>
                                    <%--<jsp:include  page="/advice/include/newShareThis.jsp" />--%>
                                </div>
                            </article>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>   
    <!-- Article Content Section -->
   
    <section class="p0">
        <div class="ad_cnr">
            <div class="content-bg">
                <div class="<%=className%>">
                    <article>
                        <%-- ::START:: Yogeswari :: 30.06.2015 :: added for UCAS CALCULATOR task --%>
                        <% if(isUCASApplication){%>
                          <jsp:include page="/advice/include/ucasCalculator.jsp"/>                          
                        <%}%>
                        <%-- ::END:: Yogeswari :: 30.06.2015 :: added for UCAS CALCULATOR task --%>
                        <aside class="artLCo mhc">
                        <c:if test="${COVID19_SYSVAR eq 'ON' and primaryCategory eq 'CLEARING'}">
                          <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
                          <div class="covid_upt">
	                        <span class="covid_txt"><%=new SessionData().getData(request, "COVID19_ARTICLE_TEXT")%></span>
	                        <a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_blu_arw.svg", 0)%>"></a>
                          </div>
                        </c:if>
                        <c:if test="${not empty adviceDetailInfo.pullQuote}">
                            <div class="pQu">
                                <figure>
                                    <img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/open_quo.jpg" width="52" height="44" alt="open quote" />
                                </figure>
                                <p class="txt_posi_blo m0 fl">${adviceDetailInfo.pullQuote}
                                <!--<a href="javascript:;" class="link_blue fnt_14 fnt_lrg lh_24 fl w100p" title="TWEET THIS">TWEET THIS</a>--></p>
                                <figure>
                                    <img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/close_quo.jpg" width="52" height="43" alt="close quote" />
                                </figure>
                            </div>
                            </c:if>
                            <p>${adviceDetailInfo.lastParagraph}</p> 
                            
                            <%--<logic:present name="adviceDetailInfo" property="nextAdviceDetailURL">
                              <logic:notEmpty name="adviceDetailInfo" property="nextAdviceDetailURL">
                              <div class="fr w100p cf">
                                <div class="fr"><span class="fnt_lrg fnt_18 lh_24 cl_grylt fl pr5">NEXT</span><i class="fa fa-angle-right fa-2x fl lh_24 cl_grylt"></i></div>
                                <a href="<bean:write name="adviceDetailInfo" property="nextAdviceDetailURL" filter="false" />" class="fr fnt_lbd fnt_16 lh_24 link_blue w100p txt_rht">
                                    <bean:write name="adviceDetailInfo" property="nextPostTitle" filter="false" />
                                </a>
                              </div>
                              </logic:notEmpty>
                            </logic:present>--%>                            
                        </aside>
                    </article>
                    <aside class="mstPop fr">
                    <c:if test="${empty requestScope.browseSearchUrl}">
                      <%--Commented by Prabha for hide the Trending pod in article page, 16-02.2016
                      <div id="viewportTrendingPodDiv" style="display:none;"><span style="color: #ffffff;">.</span></div>
                      <div id="trendingPodDiv" style="display:none;"></div>--%>
                      <c:if test="${empty requestScope.articlePrvw}">
                          <div id="viewportMostPulrPodDiv"><span style="color: #ffffff;">.</span></div>                        
                      </c:if>
                      <div id="MostPulrPodDiv"></div>
                      <%--End of lazyloadpod 03_NOV_2015--%>
                        </c:if>  
                        <c:if test="${not empty requestScope.browseSearchUrl}">
                            <div class="yns">
                            <div class="mstPop nxs nobord">
                                <h2 class="txt_cnr">Your next steps</h2>
                                <a title="FIND DEGREES" href="<c:out value="${requestScope.browseSearchUrl}"/>" class="btn1 blue mt23">FIND DEGREES<i class="fa fa-long-arrow-right"></i></a>
                                <a title="MORE GUIDES" class="btn1 bg_orange marBtm" href="/advice/guides/subject-guides/">MORE GUIDES<i class="fa fa-long-arrow-right"></i></a>
                            </div>
                            <%request.setAttribute("subjectGuide", "yes");%>
                            <div id="div-gpt-ad-subject_guide-0" class="asggb">
                            <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
                            <script type="text/javascript">
                                 googletag.cmd.push(function() {
                                 googletag.display("div-gpt-ad-subject_guide-0");
                                 });
                            </script>  
                            </c:if>                      
                        </div>
                        </div>
                        </c:if>
                    </aside>
                </div>
                <c:if test="${not empty requestScope.browseSearchUrl}">
                  <div class="artCont">
                    <div class="mstPop nxs artSl mt15 mb30 nobord">
                      <h2 class="fl youSt">Your next steps</h2>
                      <a title="FIND DEGREES" href="<c:out value="${requestScope.browseSearchUrl}"/>" class="btn1 blue mt9 fl">FIND DEGREES<i class="fa fa-long-arrow-right"></i></a>
                      <a title="MORE GUIDES" class="btn1 bg_orange fr" href="/advice/guides/subject-guides/">MORE GUIDES<i class="fa fa-long-arrow-right"></i></a>
                    </div>               
                  </div>
                </c:if>
                <div class="fclear"></div>
            </div>
        </div>
    </section>        
   
   <c:if test="${empty requestScope.browseSearchUrl}">
    <c:if test="${not empty requestScope.similarArticleList}">
        <jsp:include page="/advice/include/similarArticleListPod.jsp"/>
      </c:if>      
     <%--1-SEP-2015 modified for lazyloadpod by priyaa--%>
     
      <c:if test="${primaryCategory ne 'TEACHERS AND ADVISORS'}">
      <div id="viewportspamboxDiv"><span style="color: #ffffff;">.</span></div>
      <div id="spamboxDiv"></div>
      </c:if>
    </c:if>  
    <!-- Other Courses you may like Starts-->	
    <section class="p0">
        <div class="ad_cnr ad_cour">
            <div class="content-bg">                
                <!--<div class="borderbot mt40 fl mb25"></div>-->
                <c:if test="${not empty requestScope.otherCoursesAdviceList}">
                    <jsp:include page="/advice/include/courseYouMightLike.jsp"/>
                </c:if>           
                <!-- Other Courses you may like Starts-->
                <c:if test="${not empty requestScope.univsInCityAdviceList}">
                    <jsp:include page="/advice/include/subjectGuide.jsp"/>
                </c:if>
                <!-- Other Courses you may like Ends-->
            </div>
        </div>
    </section>    
    <!-- Other Courses you may like Ends-->    
    
    <div class="flyOut"></div>    
    </c:forEach>
    </c:if> 
</div>
<!--Article Page Ends-->  
    <jsp:include page="/jsp/common/wuFooter.jsp" />
    <!--articleId and primaryCategoryId for ajax call -->
    <input type="hidden" id="primaryCategory" value="<%=primaryCategory%>"/>
    <input type="hidden" id="articleId" value="<%=articleId%>"/>
    <input type="hidden" id="parentCategory" value="<%=parentCategory%>"/>
    <input type="hidden" id="secondaryCategory" value="<%=secondaryCategory%>"/>
    <%--call this script method to set compare all status for add to compare redesign, 03_Feb_2015 By Thiyagu G--%>
    <script type="text/javascript" language="javascript">
      setCompareAll();
    </script>
    <%--Commented by Prabha for unused js loading on Article detail page on 08_May_2018
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=facebookLoginJSName%>"> </script>--%>
    <script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/player_api.js"></script> <%--Add youtube video auto play api js on 03_Oct_2017, By Thiyagu G--%>
    <script type="text/javascript">
      onYouTubeIframeAPIReady();  
    </script>    
  </body>