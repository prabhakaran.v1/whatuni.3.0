<%@page import="WUI.utilities.CommonUtil, java.util.ArrayList, WUI.utilities.SessionData, WUI.utilities.CommonFunction, java.util.*" %>

<%    
  CommonFunction commonFun = new CommonFunction();
  String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
  ResourceBundle rb = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources"); 
  String domainSpecificPath = httpStr + rb.getString("wuni.whatuni.device.specific.css.path");
  String main_480_ver = rb.getString("wuni.whatuni.main.480.css");
  String main_992_ver = rb.getString("wuni.whatuni.main.992.css");
  String envronmentName = commonFun.getWUSysVarValue("WU_ENV_NAME");
  String adviceJsName = rb.getString("wuni.adviceJsName.js");
  String totalRecordCount = (request.getAttribute("totalRecordCount")!=null) ? request.getAttribute("totalRecordCount").toString() : "0";  
   int noOfPage = (Integer.parseInt(totalRecordCount)/20);
   if((Integer.parseInt(totalRecordCount)%20) > 0) {
     noOfPage++; 
   }  
   String pageNo = (request.getAttribute("page")!=null) ? request.getAttribute("page").toString() : "1";
   String pageName = (request.getAttribute("pageName")!=null) ? request.getAttribute("pageName").toString() : "1";
   String mappingPath = (request.getAttribute("mappingPath")!=null) ? request.getAttribute("mappingPath").toString() : "";
   String parentCategory = (request.getAttribute("parentCategory")!=null) ? request.getAttribute("parentCategory").toString() : "";
   String secondaryCategory = (request.getAttribute("secondaryCategory")!=null) ? request.getAttribute("secondaryCategory").toString() : "";
   String artPCategory = parentCategory.replaceAll("-"," ").toUpperCase();
   String artSCategory = secondaryCategory.replaceAll("-"," ").toUpperCase();
   String primaryCategory = "";     
   String nextUrl = "";     
   String previousUrl = "";
   int numofpagesplus = (Integer.parseInt(pageNo)+1);
   int numofpagesminus = (Integer.parseInt(pageNo)-1);
   
   String canonUrl = httpStr + "www.whatuni.com"+mappingPath+"/";
   String lazyLoadJs = rb.getString("wuni.lazyLoadJs.js");
%>
<link rel="canonical" href="<%=canonUrl%>"/> 
  <%if(noOfPage>1){
     if("1".equals(pageNo)){       
        nextUrl = httpStr + "www.whatuni.com"+mappingPath+"/?page="+numofpagesplus;
     %>
        <link rel="next" href="<%=nextUrl%>" />
     <%}else if(noOfPage == Integer.parseInt(pageNo)){
        previousUrl = httpStr + "www.whatuni.com"+mappingPath+"/?page="+numofpagesminus;
     %>
        <link rel="prev" href="<%=previousUrl%>" />
     <%}else{
        nextUrl = httpStr + "www.whatuni.com"+mappingPath+"/?page="+numofpagesplus;
        previousUrl = httpStr + "www.whatuni.com"+mappingPath+"/?page="+numofpagesminus;
     %>
        <link rel="prev" href="<%=previousUrl%>" />
        <link rel="next" href="<%=nextUrl%>" />
     <%}}
     String indexFollow = "noindex,follow";
     if(Integer.parseInt(pageNo) == 1){
       indexFollow = "index,follow";
    }
     %>
<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
</header>   
<!--Article Page Starts-->
<div class="article_main pb50 cf">
    <div id="stickyTop" class="">
        <div class="article_land_cnr mb25 pb15">    
            <div class="ad_cnr">
                <div class="content-bg">
                    <div class="sbcr mb-10 pros_brcumb">
                        <div class="bc">
                            <div class="bcrumb_blue">
                                <% String bCrumb = (String)request.getAttribute("adviceLandingBreadCrumb"); 
                                if(bCrumb!=null && !"".equals(bCrumb)){%>
                                <%=bCrumb%>
                                <%}%>
                            </div>
                        </div>
                    </div>
                    <!-- Uni and study advice -->
                    <section class="asrc p0">
                        <div class="asrcc">
                            <h1 class="txt_cap"><%=secondaryCategory%></h1>
                        </div>
                        <div class="asrcb">
                            <div class="pros_search fl w100p">
                                <form id="articleSearchForm" method="post" action="">
                                    <fieldset class="fl">
                                        <%if(request.getAttribute("keyword") != null) {
                                                String keyword = request.getAttribute("keyword").toString();
                                                %>
                                                <input class="fnt_lrg tx_bx" autocomplete="off" name="keyword" id="keyword" value="<%=keyword%>" type="text" onkeypress="return keypressSubmit('1', 'secondaryCat', event);" /> 
                                                <%
                                            }else{%>
                                                <input class="fnt_lrg tx_bx" autocomplete="off" name="keyword" id="keyword" value="Enter keyword" type="text" onkeypress="return keypressSubmit('1', 'secondaryCat', event);" onblur="if(this.value == ''){this.value='Enter keyword';this.style.color='#a6a6a6';}" onfocus="if(this.value == 'Enter keyword'){this.value = '';this.style.color='#000';}"> 
                                            <%}%>                                                                                    
                                            <a class="icon_srch fr" onclick="return adviceSearchSubmit('1','secondaryCat');"><i class="fa fa-search fa-1_5x"></i></a>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </section>					
                </div>
            </div>
        </div>
    </div>
    <!-- Article Main Heading -->
    <section class="p0">
	<div class="ad_cnr">
            <div class="content-bg">
		<div class="artMain">
                    <section class="asadv p0">
                        <!-- Article Nav -->                        
                        <div id="stickyAdvice">
                            <jsp:include page="/advice/include/adviceLeftNav.jsp">                            
                                <jsp:param name="menuName" value="Blog" />                                    
                            </jsp:include>
                        </div>
                        <div id="leftNavMobile">
                            <jsp:include page="/advice/include/adviceLeftNavForMobile.jsp" />                            
                        </div>  
                        <div class="art-prod">
                            <!-- 1 -->
                            <div class="art-bl">
                                <jsp:include page="/advice/include/secondaryCategories.jsp">
                                    <jsp:param name="subCategory" value="yes" />
                                </jsp:include>
                            </div>                                                    
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <div class="flyOut"></div>
</div>
<!--Article Page Ends-->   
<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>