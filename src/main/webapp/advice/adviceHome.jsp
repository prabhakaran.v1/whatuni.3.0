<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil, java.util.ArrayList, WUI.utilities.SessionData, java.util.*, WUI.utilities.CommonFunction" %>
<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
</header>  
<!--Article Page Starts-->
<div class="article_main pb50 cf">
    <div id="stickyTop" class="">
        <div class="article_land_cnr mb25 pb15">    
            <div class="ad_cnr">
                <div class="content-bg">
                    <div class="sbcr mb-10 pros_brcumb">
                        <div class="bc">
                            <div class="bcrumb_blue">
                                <% String bCrumb = (String)request.getAttribute("adviceHomeBreadCrumb"); 
                                if(bCrumb!=null && !"".equals(bCrumb)){%>
                                <%=bCrumb%>
                                <%}%>
                            </div>
                        </div>
                    </div>
                    <!-- Uni and study advice -->
                    <section class="asrc p0">
                        <jsp:include page="/advice/include/adviceSearchHeader.jsp">                            
                            <jsp:param name="pageName" value="adviceHome" />                                    
                        </jsp:include>     
                        
                    </section>					
                </div>
            </div>
        </div>
    </div>
    
    <!-- Article Main Heading -->
    <section class="p0">
	<div class="ad_cnr">
            <div class="content-bg">
		<div class="artMain">
                    <section class="asadv p0">
                        <!-- Article Nav -->                        
                        <div id="stickyAdvice" class="">
                            <jsp:include page="/advice/include/adviceLeftNav.jsp">                            
                                <jsp:param name="menuName" value="Blog" />                                    
                            </jsp:include>
                        </div>                        
                        <div id="leftNavMobile">
                            <jsp:include page="/advice/include/adviceLeftNavForMobile.jsp" />                            
                        </div>                        
                        <div class="art-prod">
                           
                            <!-- 1 -->
                            <c:if test="${not empty requestScope.adviceHomeEditorsPickList}">
                            <div class="art-bl">
                            <h2>Editor's pick</h2>
                                <jsp:include page="/advice/include/adviceAllCategories.jsp">                            
                                    <jsp:param name="primaryCategory" value="EditorsPick" />
                                    <jsp:param name="position" value="left" />
                                    <jsp:param name="subCategory" value="no" />
                                </jsp:include>
                            </div>
                            </c:if>
                            <%-- 0 --%> <%--Added teacher category on 04_Mar_2020, By Sri Sankari R--%>
                            <c:if test="${not empty requestScope.adviceHomeTeacherList}">
                            <div class="art-bl">
                            <h2 id="coronavirus"><spring:message code="wuni.covid.label"/></h2>
                                <jsp:include page="/advice/include/adviceAllCategories.jsp">
                                    <jsp:param name="primaryCategory" value="coronavirus" />
                                    <jsp:param name="position" value="left" />
                                    <jsp:param name="subCategory" value="no" />
                                </jsp:include>
                            </div>
                            </c:if>
                            <!-- 2 -->
                            <c:if test="${not empty requestScope.adviceHomeResearchPrepList }">
                            <div class="art-bl">
                            <h2>Research &amp; Prep</h2>
                                <jsp:include page="/advice/include/adviceAllCategories.jsp">                            
                                    <jsp:param name="primaryCategory" value="ResearchPrep" />
                                    <jsp:param name="position" value="right" />
                                    <jsp:param name="subCategory" value="yes" />
                                </jsp:include>
                            </div>
                            </c:if>
                            <!-- 2.1 -->
                            <c:if test="${not empty requestScope.adviceHomeAccommodationList }">
                            <div class="art-bl">
                            <h2>Accommodation</h2>
                                <jsp:include page="/advice/include/adviceAllCategories.jsp">                            
                                    <jsp:param name="primaryCategory" value="Accommodation" />
                                    <jsp:param name="position" value="left" />
                                    <jsp:param name="subCategory" value="no" />
                                </jsp:include>
                            </div> 
                            </c:if>                         
                            <!-- 3 -->
                            <c:if test="${not empty requestScope.adviceHomeStudLifeList}">
                            <div class="art-bl">
                                <h2 id="StudentLife">Student life</h2>
                                <jsp:include page="/advice/include/adviceAllCategories.jsp">
                                    <jsp:param name="primaryCategory" value="StudentLife" />
                                    <jsp:param name="position" value="right" />
                                    <jsp:param name="subCategory" value="yes" />
                                </jsp:include>
                            </div>
                           </c:if>
                            <!-- 4 -->
                            <c:if test="${not empty requestScope.adviceHomeBlogList}">
                            <div class="art-bl">
                                <h2 id="Blog">Blog</h2>
                                <jsp:include page="/advice/include/adviceAllCategories.jsp">
                                    <jsp:param name="primaryCategory" value="Blog" />
                                    <jsp:param name="position" value="left" />
                                    <jsp:param name="subCategory" value="no" />
                                </jsp:include>
                            </div>
                            </c:if>
                            <!-- 5 -->
                            <c:if test="${not empty requestScope.adviceHomeNewsList}">
                            <div class="art-bl">
                                <h2 id="News">News</h2>
                                <jsp:include page="/advice/include/adviceAllCategories.jsp">
                                    <jsp:param name="primaryCategory" value="News" />
                                    <jsp:param name="position" value="right" />
                                    <jsp:param name="subCategory" value="no" />
                                </jsp:include>
                            </div>
                           </c:if>
                            <!-- 6 -->
                            <c:if test="${not empty requestScope.adviceHomeGuidesList}">
                            <div class="art-bl">
                                <h2 id="Guides">Guides</h2>
                                <jsp:include page="/advice/include/adviceAllCategories.jsp">
                                    <jsp:param name="primaryCategory" value="Guides" />
                                    <jsp:param name="position" value="left" />
                                    <jsp:param name="subCategory" value="yes" />
                                </jsp:include>
                            </div>
                            </c:if>
                            <!-- 7 -->
                            <c:if test="${not empty requestScope.adviceHomeClearingList}">
                            <div class="art-bl">
                            <h2 id="Clearing">Clearing</h2>
                                <jsp:include page="/advice/include/adviceAllCategories.jsp">
                                    <jsp:param name="primaryCategory" value="Clearing" />
                                    <jsp:param name="position" value="right" />
                                    <jsp:param name="subCategory" value="no" />
                                </jsp:include>
                            </div>
                            </c:if>
                            <%-- 8 --%> <%--Added parents category on 08_Aug_2017, By Thiyagu G--%>
                            <c:if test="${not empty requestScope.adviceHomeParentsList}">
                            <div class="art-bl">
                            <h2 id="Parents">Parents</h2>
                                <jsp:include page="/advice/include/adviceAllCategories.jsp">
                                    <jsp:param name="primaryCategory" value="Parents" />
                                    <jsp:param name="position" value="left" />
                                    <jsp:param name="subCategory" value="no" />
                                </jsp:include>
                            </div>
                            </c:if>
                            
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <div class="flyOut"></div>
</div>
<!--Article Page Ends--> 
<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>