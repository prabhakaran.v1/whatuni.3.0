<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%String linkHilight = (request.getAttribute("linkHilight")!=null) ? request.getAttribute("linkHilight").toString() : "";%>
<div class="art-nav">
    <nav>
        <ul id="horiz" class="mlst">
            <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
             <%-- Clearing menu showing on Top by Kailash L on 21_JULY_2020 --%> 
             <li><a class="<%="clearing".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/clearing/" title="Clearing">Clearing</a></li>          
            <li><a class="<%="coronavirus".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="${teacherSectionUrl}" title="Covid-19 Updates"><spring:message code="wuni.covid.label"/></a></li> <%-- Added Teacher category on 04_Mar_2020, By Sri Sankari R --%>                                        
            <li>
                <a class="<%="research-and-prep".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/research-and-prep/" title="Research &amp; Prep">Research &amp; Prep</a>
                <ul class="slst">
                    <li><a class="<%="choosing-a-course".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/research-and-prep/choosing-a-course/" title="Choosing a Course">
                    <i class="fa fa-angle-right"></i>Choosing a Course</a></li>
                    <li><a class="<%="choosing-a-uni".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/research-and-prep/choosing-a-uni/" title="Choosing a Uni">
                     <i class="fa fa-angle-right"></i>Choosing a Uni</a></li>
                    <li><a class="<%="open-days".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/research-and-prep/open-days/" title="Open Days">
                     <i class="fa fa-angle-right"></i>Open Days</a></li>
                    <li><a class="<%="applying-to-uni".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/research-and-prep/applying-to-uni/" title="Applying to Uni">
                     <i class="fa fa-angle-right"></i>Applying to Uni</a></li>
                    <li><a class="<%="money".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/research-and-prep/money/" title="Money">
                     <i class="fa fa-angle-right"></i>Money</a></li>                    
                    <li><a class="<%="career-advice".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/research-and-prep/career-advice/" title="Career advice">
                     <i class="fa fa-angle-right"></i>Career advice</a></li>                    
                </ul>
            </li>
            <li><a class="<%="accommodation".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/accommodation/" title="Accommodation">
                     Accommodation</a></li>            
            <li><a class="<%="student-life".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/student-life/" title="Student Life">Student Life</a>
                <ul class="slst">
                    <li><a class="<%="wellbeing".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/wellbeing/" title="Wellbeing">
                     <i class="fa fa-angle-right"></i>Wellbeing</a></li>
                    <li><a class="<%="sixth-form-life".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/student-life/sixth-form-life/" title="Sixth Form Life">
                     <i class="fa fa-angle-right"></i>Sixth Form Life</a></li>                    
                    <li><a class="<%="uni-life".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/student-life/uni-life/" title="Uni Life"> <i class="fa fa-angle-right"></i>Uni Life</a></li>
                </ul>
            </li>
            <li><a class="<%="blog".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/blog/" title="Blog">Blog</a></li>
            <li><a class="<%="news".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/news/" title="News">News</a></li>
            <li><a class="<%="guides".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/guides/" title="Guides">Guides</a>
                <ul class="slst">
                    <li><a class="<%="subject-guides".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/guides/subject-guides/" title="Subject Guides"> <i class="fa fa-angle-right"></i>Subject Guides</a></li>
                    <li><a class="<%="ultimate-guides".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/guides/ultimate-guides/" title="Ultimate Guides"> <i class="fa fa-angle-right"></i>Ultimate Guides</a></li>
                    <li><a class="<%="city-guides".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/guides/city-guides/" title="City Guides"> <i class="fa fa-angle-right"></i>City Guides</a></li>
                    <li><a class="<%="ucas-application-guide".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/guides/ucas-application-guide/" title="UCAS Guides"> <i class="fa fa-angle-right"></i>UCAS Guides</a></li> <%-- Yogeswari :: 30.06.2015 :: added for UCAS CALCULATOR task --%>
                </ul>
            </li>
            <li><a class="<%="parents".equalsIgnoreCase(linkHilight) ? "act" : ""%>" id="researchAndPrep" href="/advice/parents/" title="Parents">Parents</a></li>
        </ul>        
    </nav>    
</div>