<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction" %>

<%  
  CommonFunction commonFun = new CommonFunction();
  String subCategory = request.getParameter("subCategory");
  String totalRecordCount = (String) request.getAttribute("totalRecordCount");  
         totalRecordCount = (!GenericValidator.isBlankOrNull(totalRecordCount) ? totalRecordCount : "0");    
  int noOfPage = (Integer.parseInt(totalRecordCount)/20);
  if((Integer.parseInt(totalRecordCount)%20) > 0) {
    noOfPage++; 
  }  
  String pageNo = (String) request.getAttribute("page");
         pageNo = (!GenericValidator.isBlankOrNull(pageNo) ? pageNo : "1");
  String pageName = (request.getAttribute("pageName")!=null) ? request.getAttribute("pageName").toString() : "1";
  String primaryCategory = (request.getAttribute("parentCategory")!=null) ? request.getAttribute("parentCategory").toString() : "";
  String secondaryCategory = "";
  String sponsorClass="";
%>
<form id="secondaryCatForm" method="post" action="">
  <%if("1".equals(pageNo)){%>
    <c:if test="${not empty requestScope.categoryArticleList}">
      <c:forEach var="primaryCategoryList" items="${requestScope.categoryArticleList}" varStatus="i" end="0">
        <c:set var="checkPostId" value="${primaryCategoryList.postId}"/>
        <c:set var="primaryCat" value="${primaryCategoryList.parentCategoryName}"/>
        <c:set var="secondaryCat" value="${primaryCategoryList.subCategoryName}"/>
            <%//primaryCategory = primaryCat.replaceAll(" ","-").toLowerCase(); 
            secondaryCategory = pageContext.getAttribute("secondaryCat").toString().replaceAll(" ","-").toLowerCase();%>
            <div class="bg_cover">
            <div class="ab-img">
                <%String readClassName = "img_ppn";
                    if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId"))){
                        readClassName = "img_ppn read";
                 }}%>
                <figure class="<%=readClassName%>">
                <a href="<c:out value="${primaryCategoryList.adviceDetailURL}"/>" title="">
                    <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${primaryCategoryList.imageName}"/>" alt="<c:out value="${primaryCategoryList.mediaAltText}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>" />
                    <%if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId"))){%>
                    <span class="re-img"></span>
                    <%}}%>
                </a>
                </figure>
            </div>
            <div class="ab-cont">
                <%if("yes".equals(subCategory)){%><h4><c:out value="${primaryCategoryList.subCategoryNameCapital}"/></h4><%}%>
                <h3 class="pb15"><a href="<c:out value="${primaryCategoryList.adviceDetailURL}" />"><c:out value="${primaryCategoryList.postTitle}"/></a></h3>
                <p><c:out value="${primaryCategoryList.postShortText}"/></p>
                <c:if test="${not empty primaryCategoryList.sponsorUrl and primaryCategoryList.collegeDisplayName}">
                  <c:set var="collegeName" value="${primaryCategoryList.collegeDisplayName}"/>
                    <%String gaCollegeName = commonFun.replaceSpecialCharacter(pageContext.getAttribute("collegeName").toString()).toLowerCase(); %>
                    <div class="clr_slwrap">
                      <div class="clr_splgo">            
                        <a target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${primaryCategoryList.collegeId}"/>','<c:out value="${primaryCategoryList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${primaryCategoryList.sponsorUrl}"/>"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${primaryCategoryList.sponsorLogo}"/>" alt="" title="" /></a>  
                      </div>
                      <div class="clr_asp">
                        <h5 class="clr_stxt">Sponsored by</h5>
                        <h5>
                           <a class="clr_suni" target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${primaryCategoryList.collegeId}"/>','<c:out value="${primaryCategoryList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${primaryCategoryList.sponsorUrl}"/>"><c:out value="${primaryCategoryList.collegeDisplayName}"/></a>
                        </h5>
                      </div>
                    </div>
                </c:if>
                <div class="fcont">
                    <div class="fcontIc"><i class="fa fa-clock-o pr5"></i><c:out value="${primaryCategoryList.updatedDate}"/></div>
                    <%-- <c:if test="${not empty primaryCategoryList.readCount}">
                    <div class="fcontIc"><i class="fa fa-eye pr5"></i><c:out value="${primaryCategoryList.readCount}"/></div>
                    </c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                </div>
            </div>
            </div>
        </c:forEach>      
        <section class="art-ml p0">
        <c:forEach var="primaryCategoryList" items="${requestScope.categoryArticleList}" varStatus="i" begin="1" end="6">
          <c:set var="index" value="${i.index}"/>
            <c:set var="checkPostId1" value="${primaryCategoryList.postId}"/>
            <%if(Integer.parseInt(pageContext.getAttribute("index").toString())==1 || Integer.parseInt(pageContext.getAttribute("index").toString())==4){%>
                 <%sponsorClass="";%>
                 <ul class="art-lst">
                 <c:forEach var="primary1CategoryList" items="${requestScope.categoryArticleList}" begin="${i.index}" end="3">
                       <c:if test="${not empty primary1CategoryList.sponsorUrl}">
                         <%sponsorClass="clsp";%>
                       </c:if>
                     </c:forEach>
                <%}%>
                <li>
                    <div class="art-view">
                        <%String readClassName = "img_ppn";
                            if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId1"))){
                                readClassName = "img_ppn read";
                         }}%>
                        <div class="<%=readClassName%>">
                            <a href="<c:out value="${primaryCategoryList.adviceDetailURL}"/>" title="">
                            <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${primaryCategoryList.imageName}"/>" alt="<c:out value="${primaryCategoryList.mediaAltText}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>" /></a>
                            <%if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId1"))){%>
                            <span class="re-img"></span>
                            <%}}%>
                        </div>
                        <div class="art-inc <%=sponsorClass%>"> 
                            <div class="art-desc">
                            <%if("yes".equals(subCategory)){%><c:if test="${not empty primaryCategoryList.subCategoryNameCapital}"><h4><c:out value="${primaryCategoryList.subCategoryNameCapital}"/></h4></c:if><%}%>
                            <h3><a href="<c:out value="${primaryCategoryList.adviceDetailURL}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>"><c:out value="${primaryCategoryList.postTitle}"/></a></h3>
                            </div>
                            <c:if test="${not empty primaryCategoryList.sponsorUrl and primaryCategoryList.collegeDisplayName}">
                                <c:set var="collegeNameDispId" value="${primaryCategoryList.collegeDisplayName}"/>        
                                <%String gaCollegeName = commonFun.replaceSpecialCharacter(pageContext.getAttribute("collegeNameDispId").toString()).toLowerCase(); %>
                               <div class="clr_slwrap">
                                 <div class="clr_splgo">            
                                   <a target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${primaryCategoryList.collegeId}"/>','<c:out value="${primaryCategoryList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${primaryCategoryList.sponsorUrl}"/>"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${primaryCategoryList.sponsorLogo}"/>" alt="" title="" /></a>  
                                 </div>
                                 <div class="clr_asp">
                                    <h5 class="clr_stxt">Sponsored by</h5>
                                    <h5>
                                     <a class="clr_suni" target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${primaryCategoryList.collegeId}"/>','<c:out value="${primaryCategoryList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${primaryCategoryList.sponsorUrl}"/>"><c:out value="${primaryCategoryList.collegeDisplayName}"/></a>
                                    </h5>
                                 </div>
                               </div>
                            </c:if>
                            <div class="fcont">
                                <div class="fcontIc"><i class="fa fa-clock-o pr5"></i><c:out value="${primaryCategoryList.updatedDate}"/></div>
                                <%-- <c:if test="${not empty primaryCategoryList.readCount}">
                                <div class="fcontIc"><i class="fa fa-eye pr5 pr5"></i><c:out value="${primaryCategoryList.readCount}"/></div>
                                </c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                            </div>
                        </div>
                    </div>
                </li>
                <%if(Integer.parseInt(pageContext.getAttribute("index").toString())==3 || Integer.parseInt(pageContext.getAttribute("index").toString())==6){%>
                    </ul>
                <%}%>
            </c:forEach>
        </section>
</c:if>  
<c:if test="${not empty requestScope.categoryArticleList}">
        <section id="categoryArticleList" style="display:none;" class="sh-res p0">
            <ul class="sh-lst subsr">
			    <%String shclassname = "";%>
			    <c:forEach var="primaryCategoryList" items="${requestScope.categoryArticleList}" varStatus="i" begin="7">
                <c:set var="checkPostIdList" value="${primaryCategoryList.postId}"/>
                    <li>
					 <%shclassname = "";%>
					 <c:if test="${not empty primaryCategoryList.sponsorUrl}">
					   <%shclassname = "clsp"; %>
					</c:if>
                    <div class="shl <%=shclassname%>">     
                        <%String readClassName = "subImg";
                            if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostIdList"))){
                                readClassName = "subImg read";
                         }}%>
                        <div class="<%=readClassName%>">
                            <a href="<c:out value="${primaryCategoryList.adviceDetailURL}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>">                            
                            <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${primaryCategoryList.imageName}"/>" alt="<c:out value="${primaryCategoryList.postTitle}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>">
                            <%if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostIdList"))){%>
                            <span class="re-img"></span>
                            <%}}%>
                            </a>
                        </div>
                        <div class="subCnt">
                            <h3><a href="<c:out value="${primaryCategoryList.adviceDetailURL}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>"><c:out value="${primaryCategoryList.postTitle}"/></a></h3>
                            <c:if test="${not empty primaryCategoryList.sponsorUrl and primaryCategoryList.collegeDisplayName}">
                               <c:set var="collegeNameDisplayId" value="${primaryCategoryList.collegeDisplayName}"/>        
                                <%String gaCollegeName = commonFun.replaceSpecialCharacter((String)pageContext.getAttribute("collegeNameDisplayId")).toLowerCase(); %>
                                   <div class="clr_slwrap">
                                     <div class="clr_splgo">            
                                       <a target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${primaryCategoryList.collegeId}"/>','<c:out value="${primaryCategoryList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${primaryCategoryList.sponsorUrl}"/>"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${primaryCategoryList.sponsorLogo}"/>" alt="" title="" /></a>  
                                     </div>
                                     <div class="clr_asp">
                                        <h5 class="clr_stxt">Sponsored by</h5>
                                        <h5>
                                         <a class="clr_suni" target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${primaryCategoryList.collegeId}"/>','<c:out value="${primaryCategoryList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${primaryCategoryList.sponsorUrl}"/>"><c:out value="${primaryCategoryList.collegeDisplayName}"/></a>
                                        </h5>
                                     </div>
                                   </div>
                             </c:if>
                            <div class="fcont">
                                <div class="fcontIc"><i class="fa fa-clock-o pr5"></i><c:out value="${primaryCategoryList.updatedDate}"/></div>
                                <%-- <c:if test="${not empty primaryCategoryList.readCount}">
                                <div class="fcontIc"><i class="fa fa-eye pr5 pr5"></i><c:out value="${primaryCategoryList.readCount}"/></div>
                                </c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                            </div>
                        </div>                
                      </div>
                    </li>                
                </c:forEach>              
            </ul>              
            <jsp:include page="/advice/include/articlePagination.jsp">
                <jsp:param name="pageno" value="<%=pageNo%>"/>
                <jsp:param name="page" value="secondaryCat"/>
                 <jsp:param name="pagelimit"  value="13"/>
                 <jsp:param name="searchhits" value="<%=totalRecordCount%>"/>
                 <jsp:param name="recorddisplay" value="20"/>
                 <jsp:param name="primaryCategory" value="<%=primaryCategory%>"/>
                 <jsp:param name="secondaryCategory" value="<%=secondaryCategory%>"/>
                 <jsp:param name="action" value=""/>    
                 <jsp:param name="pageTitle" value=""/>
            </jsp:include>
        </section>
        <!-- View More -->
        <c:set var="listSize" value="${fn:length(categoryArticleList)}"/>
        <%int size = Integer.parseInt(pageContext.getAttribute("listSize").toString());
        if(size>7){%>
            <div class="fl w100p">
                <a id="categoryArticleList_More" onclick="adviceShowHide('categoryArticleList','categoryArticleList_More');" class="clear btn1 btn2 mt30" title="VIEW MORE"><i class="fa fa-plus"></i>VIEW MORE</a>
            </div>
        <%}%>
</c:if>
<%}else{%>
<c:if test="${not empty requestScope.categoryArticleList}">
        <section id="categoryArticleList" class="sh-res p0">
            <ul class="sh-lst subsr">
			  <%String sh1classname = "";%>
			  <c:forEach var="primaryCategoryList" items="${requestScope.categoryArticleList}" varStatus="i">
                    <c:set var="checkPostIdPage" value="${primaryCategoryList.postId}"/>
                    <c:set var="primaryCatPage" value="${primaryCategoryList.parentCategoryName}"/>
                    <c:set var="secondaryCatPage" value="${primaryCategoryList.subCategoryName}"/> 
                    <%secondaryCategory = pageContext.getAttribute("secondaryCatPage").toString().replaceAll(" ","-").toLowerCase();%>
                    <li>
                     <%sh1classname = "";%>
                     <c:if test="${not empty primaryCategoryList.sponsorUrl}">
					   <%sh1classname = "clsp"; %>
					</c:if>
                    <div class="shl <%=sh1classname%>">   
                        <div class="subImg">
                            <a href="<c:out value="${primaryCategoryList.adviceDetailURL}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>">                            
                            <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${primaryCategoryList.imageName}"/>" alt="<c:out value="${primaryCategoryList.postTitle}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>">
                            <%if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostIdPage") )){%>
                            <span class="re-img"></span>
                            <%}}%>
                            </a>
                        </div>
                        <div class="subCnt">
                            <h3><a href="<c:out value="${primaryCategoryList.adviceDetailURL}"/>" title="<c:out value="${primaryCategoryList.postTitle}"/>"><c:out value="${primaryCategoryList.postTitle}"/></a></h3>
                            <div class="fcont">
                                <div class="fcontIc"><i class="fa fa-clock-o pr5"></i><c:out value="${primaryCategoryList.updatedDate}"/></div>
                                <%-- <c:if test="${primaryCategoryList.readCount}">
                                <div class="fcontIc"><i class="fa fa-eye pr5 pr5"></i><c:out value="${primaryCategoryList.readCount}"/></div>
                                </c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                            </div>
                        </div>                
                    </div>
                    </li>                
                </c:forEach>             
            </ul>              
            <jsp:include page="/advice/include/articlePagination.jsp">
                <jsp:param name="pageno" value="<%=pageNo%>"/>
                <jsp:param name="page" value="secondaryCat"/>
                 <jsp:param name="pagelimit"  value="13"/>
                 <jsp:param name="searchhits" value="<%=totalRecordCount%>"/>
                 <jsp:param name="recorddisplay" value="20"/>
                 <jsp:param name="primaryCategory" value="<%=primaryCategory%>"/>
                 <jsp:param name="secondaryCategory" value="<%=secondaryCategory%>"/>
                 <jsp:param name="action" value=""/>    
                 <jsp:param name="pageTitle" value=""/>
            </jsp:include>
        </section>
</c:if>
<%}%>
<input type="hidden" name="primaryCategory" id="primaryCategory" value="<%=primaryCategory%>" />
<input type="hidden" name="secondaryCategory" id="secondaryCategory" value="<%=secondaryCategory%>" />
<%if(pageNo != null && "".equals(pageNo)) {%>
    <input type="hidden" name="page" id="page" value="<%=pageNo%>" />
    <input type="hidden" name="pageName" id="pageName" value="<%=pageName%>" />
<%}else{%>
    <input type="hidden" name="page" id="page" value="1" />
    <input type="hidden" name="pageName" id="pageName" value="secondaryCat" />
<%}%>            
 </form> 