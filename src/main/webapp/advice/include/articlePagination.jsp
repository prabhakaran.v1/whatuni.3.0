<%@page import="WUI.utilities.GlobalConstants" %>
<%
 // IN Parameters from JSP:Include
  int pageno =1;           // initial page or page selected   
  int pagelimit =0;       // Display number of links in the page
  int searchhits=00;      // Stores total number of records
  int recorddisplay=0;     // To display number of records in the page
  String pageName = "";           // initial page or page selected 
  String primaryCategory="";  // To Store the url to be attach with hyperlink
  String secondaryCategory="";  // To Store the url to be attach with hyperlink  
  String action=null;  
  String pageTitle = "";  
  //String moreReviews="";
  // To Store the url to be attach with hyperlink
  // END of IN Parameters from JSP:Include
  int numofpages =0;        // local variable Stores total number of pages 
  int pagenum =0;          // local variables
  int lower =0;             // local variables
  int upper =0;             // local variables  
 // Reads the In Parameter from the JSP include action and assign to the respective variable  
 if(request.getParameter("pageno")!=null){
  pageno = Integer.parseInt(request.getParameter("pageno"));
 } 
 if(request.getParameter("page")!=null){
  pageName = request.getParameter("page");
 }
 if(request.getParameter("pagelimit")!=null){
  pagelimit = Integer.parseInt(request.getParameter("pagelimit"));
 }  
 if(request.getParameter("searchhits")!=null){
  searchhits = Integer.parseInt(request.getParameter("searchhits"));
 }  
 if(request.getParameter("recorddisplay")!=null){
  recorddisplay = Integer.parseInt(request.getParameter("recorddisplay"));
 }  
 if(request.getParameter("primaryCategory") != null) {
  primaryCategory = request.getParameter("primaryCategory");
 }  
 if(request.getParameter("secondaryCategory") != null) {
  secondaryCategory = request.getParameter("secondaryCategory");
 }
 if(request.getParameter("action")!=null) {
  action = request.getParameter("action");
 }  
 if(request.getParameter("pageTitle")!=null){
  pageTitle = request.getParameter("pageTitle");
 }  
 String sorturl = (String) request.getAttribute("sortingUrl");
 
// end of assign value to the respective variable  
// Check to see if this is the first page If it is not the first page, show the "prev" link
// Build the link back to this routine. Pass the session, user, search and page number
//<!--If user in results no 10 or more then print this previous page link-->

  String hlink="";
  String ppath=request.getContextPath();
  if(pageTitle !=null && pageTitle.trim().equalsIgnoreCase("csearch")) {
    ppath = ppath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE; 
  } 
  if(pageTitle !=null && pageTitle.trim().equalsIgnoreCase("page")) {
    ppath = ppath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE; 
  }
  ppath=ppath;     
  
  numofpages = (searchhits / recorddisplay);
  if(searchhits%recorddisplay>0) { 
    numofpages++; 
  }  
  pagenum = (int)( (pageno - 1) / pagelimit);
  pagenum =  pagenum + 1;
  lower =  ((pagenum-1)*pagelimit)+1;
  upper = java.lang.Math.min(numofpages, (((pageno - 1) / pagelimit)+1) * pagelimit);
  if((pageno%pagelimit) == 0) {
    lower = pageno;
    upper = java.lang.Math.min(numofpages, pageno+pagelimit);
  } 
  int prevPage = pageno - 1;
  int nextPage = pageno + 1;
  if(searchhits > recorddisplay){
    if(prevPage==0){
        hlink = hlink+ "<div class=\"artPg\"><ul><li class=\"hid-ph\"><a class=\"in_visible\" disabled=\"disabled\"><i class=\"fa fa-angle-left\"></i></a></li><li class=\"vis-ph fl\"><a class=\"in_visible\" disabled=\"disabled\">BACK<i class=\"fa fa-angle-left\"></i></a></li>";
    }else{
        hlink = hlink+ "<div class=\"artPg\"><ul><li class=\"hid-ph\"><a onclick=\"paginationSubmit('"+pageName+"','"+prevPage+"');\"><i class=\"fa fa-angle-left\"></i></a></li><li class=\"vis-ph fl\"><a onclick=\"paginationSubmit('"+pageName+"','"+prevPage+"');\">BACK<i class=\"fa fa-angle-left\"></i></a></li>";
    }
    if(numofpages > 1){
      for(int i=lower; i<=upper; i++) {
        if(i == pageno) {
          hlink =hlink +"<li class=\"hid-ph\"><a class=\"active\" disabled=\"disabled\">"+i+"</a></li>";
        } else {
          hlink =hlink +"<li class=\"hid-ph\"><a onclick=\"paginationSubmit('"+pageName+"','"+i+"');\">"+i+"</a></li>";       
        }
      } 
    }
    // Check to see if this is the last page, If it is not the last page, show the "next" link
    // Build the link back to this routine Pass the session, user, search and page number
    int lastpage = numofpages+1;
    if(nextPage == lastpage) {  
      hlink = hlink+ "<li class=\"hid-ph\"><a class=\"mr0 in_visible\" disabled=\"disabled\"><i class=\"fa fa-angle-right\"></i></a></li><li class=\"vis-ph fr\"><a class=\"mr0 in_visible\" disabled=\"disabled\">NEXT<i class=\"fa fa-angle-right\"></i></a></li></ul></div>";    
    }else{
      hlink = hlink+ "<li class=\"hid-ph\"><a class=\"mr0\" onclick=\"paginationSubmit('"+pageName+"','"+nextPage+"');\"><i class=\"fa fa-angle-right\"></i></a></li><li class=\"vis-ph fr\"><a class=\"mr0\" onclick=\"paginationSubmit('"+pageName+"','"+nextPage+"');\">NEXT<i class=\"fa fa-angle-right\"></i></a></li></ul></div>";
    }  
    if(hlink!=null&&!(hlink.equals(""))) {
      out.println(hlink);  
    }
  }
%>