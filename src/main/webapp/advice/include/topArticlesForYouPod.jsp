<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction"%>
<%--
  * @purpose  This is Top article section jsp..
  * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * ?               ?                         ?       First Draft                                                       ? 
  * *************************************************************************************************************************
  * 06.05.2020  Prabhakaran V.               1.1     Renaming open day text to virtual tour                            
--%>
<c:if test="${not empty requestScope.articlesList}">
        
  <c:set var="listSize" value="${fn:length(articlesList)}"/>
  <%
    int size = Integer.parseInt(pageContext.getAttribute("listSize").toString());
    CommonFunction commonFun = new CommonFunction();
    String ulClass = "art-lst"; 
    String podType = request.getAttribute("podType") !=null && String.valueOf(request.getAttribute("podType")).trim().length()>0 ? String.valueOf(request.getAttribute("podType")) : "Top Pod";  
    String pageName = request.getAttribute("articlePodPageName") != null ? (String)request.getAttribute("articlePodPageName") : "";
    String gaArticleName = "";
  %>
  <%if("OPENDAY_LANDING_PAGE".equalsIgnoreCase(pageName)){%>
  <div class="content-bg">
    <div class="ad_cnr">
  <%-- Article section start--%>
    <h3>Virtual tours and events advice</h3>
    <div class="hm_ldicon art_ldicn" id="articlesPodLoading">
      <img src="<%=CommonUtil.getImgPath("/wu-cont/images/f5_loader.gif", 0)%>">
   </div>
   <%
    if(size > 4){
    ulClass = "art-lst slides";
   %>
    <div class="flexslider2">
      <div style="overflow: hidden;" class="flx_slider" id="artSlider">
   <%}%>
      <div class="oda_container">
        <c:forEach var="articlesList" items="${requestScope.articlesList}" varStatus="index">
          <c:set var="postTitle" value="${articlesList.postTitle}"/>
          <c:set var="index" value="${index.index}"/>
          
     <%
     int index = Integer.parseInt(String.valueOf(pageContext.getAttribute("index")));
     String postTitle = (String)pageContext.getAttribute("postTitle");
     gaArticleName = commonFun.replaceSpecialCharacter(postTitle).toLowerCase(); %>
     <%
      int pageNo = 1;
      if(index > 4 && index < 9){
        pageNo = 2;
      }else if(index > 8){
        pageNo = 3;
      }
     %>   
         <div class="oda_pod">
           <div class="oda_pod_img">
             <a href="${articlesList.postUrl}" onclick="ArticlesGAEventsLogging('Articles', '<%=podType%>', '<%=gaArticleName%>');" title="${articlesList.postTitle}">
               <img src="${articlesList.imageName}" alt="${articlesList.postTitle}" title="${articlesList.postTitle}">
             </a>
          </div>
          <div class="oda_pod_cnt">
            <c:if test="${not empty articlesList.postShortText}">
              <p>${articlesList.postShortText}</p>
            </c:if>
            <h4>
              <a href="${articlesList.postUrl}" onclick="ArticlesGAEventsLogging('Articles', '<%=podType%>', '<%=gaArticleName%>');" title="${articlesList.postTitle}">
                ${articlesList.postTitle}
              </a>
            </h4>
            <span>
              <i class="fal fa-clock"></i>
              ${articlesList.updatedDate}
            </span>
          </div>
        </div>
      </c:forEach>
    </div>   
    <%if(size > 4){%>
  </div>
  </div>
  
    <%}%>
  <%-- Article section end --%>
  <a class="savp" href="/advice/research-and-prep/open-days/" onclick="ArticlesGAEventsLogging('Articles', '<%=podType%>', 'See All');">SEE ALL OPEN DAYS ADVICE<i class="fa fa-long-arrow-right"></i></a>
 </div>
 </div>
 <%}else{%>
     <%-- Article section start--%>
   <h2 id="podHeading">Top articles for you</h2>
   <div class="hm_ldicon art_ldicn" id="articlesPodLoading">
     <img src="<%=CommonUtil.getImgPath("/wu-cont/images/f5_loader.gif", 0)%>">
   </div>
   <%
    if(size > 4){
    ulClass = "art-lst slides";
   %>
    <div class="flexslider2">
    <div style="overflow: hidden;" class="flx_slider" id="artSlider">
   <%}%>
    <ul class="<%=ulClass%>">
    <c:forEach var="articlesList" items="${requestScope.articlesList}" varStatus="index">
     <c:set var="index" value="${index.index}"/>
     <%
     int index = Integer.parseInt(String.valueOf(pageContext.getAttribute("index")));
      int pageNo = 1;
      if(index > 4 && index < 9){
        pageNo = 2;
      }else if(index > 8){
        pageNo = 3;
      }
     %>
     <li>
      <div class="art-view">
       <div class="img_ppn">
        <a href="${articlesList.postUrl}" onclick="ArticlesGAEventsLogging('Articles', '<%=podType%>', '<%=pageNo%>');" title="${articlesList.postTitle}">
         <img src="${articlesList.imageName}" alt="${articlesList.postTitle}" title="${articlesList.postTitle}">
        </a>
       </div>
       <div class="art-inc">
        <div class="art-desc">
         <c:if test="${not empty articlesList.postShortText}">
          <p class="cmart_txt">${articlesList.postShortText}</p>
         </c:if>
         <h3>
          <a href="${articlesList.postUrl}" onclick="ArticlesGAEventsLogging('Articles', '<%=podType%>', '<%=pageNo%>');" title="${articlesList.postTitle}">
									  ${articlesList.postTitle}
          </a>
 							 </h3>
						  </div>
					  </div>
				  </div>
			  </li>
     </c:forEach>
		  </ul>    
    <%if(size > 4){%>
    </div>
    </div>
    <%}%>
  <%-- Article section end --%>
   <article class="fl sall_art">
     <a class="btn1" href="/advice/" onclick="ArticlesGAEventsLogging('Articles', '<%=podType%>', 'See All');">SEE ALL ARTICLES<i class="fa fa-long-arrow-right"></i></a>
   </article>
<%}%>
</c:if>
