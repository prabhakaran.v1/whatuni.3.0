<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator"%>

<%
  int size = 0;  
  int pageNo = 1;
%>
<section class="artfc p0">
  <div class="ad_cnr">
    <div class="content-bg">    
      <div class="clear"></div>
      <div class="borderbot fl mb25"></div>      
      <h2>Similar articles</h2>
        <c:set var="listSize"> ${fn:length(requestScope.similarArticleList)} </c:set>
      <c:forEach var="similarList" items="${requestScope.similarArticleList}" varStatus="index">
        <c:set var="indexIntValue" value="${index.index}"/>
        <%
          size = Integer.parseInt(pageContext.getAttribute("indexIntValue").toString()) + 1;
          if(size==1){%><ul class="art-lst">
        <%}%>                        
          <li>
            <div class="art-view">
              <div class="img_ppn">
                <a href="<c:out value="${similarList.adviceDetailURL}"/>" onclick="ArticlesGAEventsLogging('Articles', 'Similar Pod', '<%=pageNo%>');" title="<c:out value="${similarList.postTitle}"/>">
                  <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif",1)%>" data-src="<c:out value="${similarList.imageName}"/>" alt="<c:out value="${similarList.mediaAltText}"/>" title="<c:out value="${similarList.postTitle}"/>" />
                </a> 
              </div>
              <div class="art-inc">
                <div class="art-desc">
                  <p class="cmart_txt"><c:out value="${similarList.personalisedText}"/></p> <%--Added personalised text on 11_July_2017, By Thiyagu G--%>
                  <h3><a href="<c:out value="${similarList.adviceDetailURL}"/>" onclick="ArticlesGAEventsLogging('Articles', 'Similar Pod', '<%=pageNo%>');" title="<c:out value="${similarList.postTitle}"/>"><c:out value="${similarList.postTitle}"/></a></h3>                  
                </div>
                
              </div>              
            </div>  
          </li>           
          <%if(Integer.parseInt(pageContext.getAttribute("listSize").toString())> 4 && size==4){%></ul><ul class="art-lst"><%}%>
          <%if(size== Integer.parseInt(pageContext.getAttribute("listSize").toString())){%></ul><%}%>    
      </c:forEach>
      <%--Load the similar and most popular article pod through ajax call, added on 13_Jun_2017, By Thiyagu G.--%>      
      <section class="artfc p0 mt38 cm_art_pd" id="mostArticlesPodButtom"></section>      
    </div>
  </div>
</section>