<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:if test="${not empty covidSnippetList}">
    <c:forEach var="covidNews" items="${covidSnippetList}">
      <div class="artltstnws_row">
        <div class="artlstnws_lftcol">
          <p><b>${covidNews.articleDate}</b></p>
          <p class="op50">${covidNews.articleTime}</p>
        </div>
        <div class="artlstnws_rgtcol">
          <p>${covidNews.postText}</p>
        </div>
      </div>
    </c:forEach>
    <div id="resultDiv${pageNo+1}"></div>
    <c:if test="${totalRow > (pageNo*3)}">
      <div class="fl w100p" id="loadMoreDiv${pageNo+1}">
        <a onclick="javascript:covidSnippetAjax(${pageNo + 1})" class="ldmrebtn" title="VIEW MORE"><spring:message code="view.more.btn"/></a>
      </div>
    </c:if> 
</c:if>
  