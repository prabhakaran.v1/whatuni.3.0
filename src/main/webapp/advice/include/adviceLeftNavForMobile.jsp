<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%
    String linkHilight = (request.getAttribute("linkHilight")!=null) ? request.getAttribute("linkHilight").toString() : "";    
    String spanDisplay = linkHilight.replaceAll("-"," ");
    if("".equals(spanDisplay)){spanDisplay="Please select";}
%>
    <div id="menu_item" class="reg_dd">
    <div class="sbxme">
            <span id="qualspanul" class="seldr txt_cap"><%=spanDisplay%></span>
    </div>
    <select class="styled qual" id="qualsel" onchange="setSelectedVal(this, 'qualspanul');" name="qualification">
    <%if("advice".equalsIgnoreCase(linkHilight)){%>
        <option value="#" selected>Please select</option>
    <%}else{%>
      <option value="#">Please select</option>
    <%}%>
    <%if("research-and-prep".equalsIgnoreCase(linkHilight)){%>
        <option value="/advice/research-and-prep/" selected>Research &amp; Prep</option>
    <%}else{%>
      <option value="/advice/research-and-prep/">Research &amp; Prep</option>
    <%}%>
    <%if("choosing-a-course".equalsIgnoreCase(linkHilight)){%>
        <option value="/advice/research-and-prep/choosing-a-course/" selected>&emsp;Choosing a Course</option>
    <%}else{%>
      <option value="/advice/research-and-prep/choosing-a-course/">&emsp;Choosing a Course</option>
    <%}%>
	<%if("choosing-a-uni".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/research-and-prep/choosing-a-uni/" selected>&emsp;Choosing a Uni</option>
    <%}else{%>
      <option value="/advice/research-and-prep/choosing-a-uni/">&emsp;Choosing a Uni</option>
    <%}%>
	<%if("open-days".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/research-and-prep/open-days/" selected>&emsp;Open Days</option>
    <%}else{%>
      <option value="/advice/research-and-prep/open-days/">&emsp;Open Days</option>
    <%}%>
    <%if("applying-to-uni".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/research-and-prep/applying-to-uni/" selected>&emsp;Applying to Uni</option>
    <%}else{%>
      <option value="/advice/research-and-prep/applying-to-uni/">&emsp;Applying to Uni</option>
    <%}%>  
	<%if("money".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/research-and-prep/money/" selected>&emsp;Money</option>
    <%}else{%>
      <option value="/advice/research-and-prep/money/">&emsp;Money</option>
    <%}%>
	<%if("accommodation".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/research-and-prep/accommodation/" selected>&emsp;Accommodation</option>
    <%}else{%>
      <option value="/advice/research-and-prep/accommodation/">&emsp;Accommodation</option>
    <%}%>
	<%if("career-advice".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/research-and-prep/career-advice/" selected>&emsp;Career Advice</option>
    <%}else{%>
      <option value="/advice/research-and-prep/career-advice/">&emsp;Career Advice</option>
    <%}%>
	<%if("revision".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/research-and-prep/revision/" selected>&emsp;Revision</option>
    <%}else{%>
      <option value="/advice/research-and-prep/revision/">&emsp;Revision</option>
    <%}%>
	<%if("student-life".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/student-life/" selected>Student Life</option>
    <%}else{%>
      <option value="/advice/student-life/">Student Life</option>
    <%}%>
	<%if("sixth-form-life".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/student-life/sixth-form-life/" selected>&emsp;Sixth Form Life</option>
    <%}else{%>
      <option value="/advice/student-life/sixth-form-life/">&emsp;Sixth Form Life</option>
    <%}%>
	<%if("freshers".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/student-life/freshers/" selected>&emsp;Freshers</option>
    <%}else{%>
      <option value="/advice/student-life/freshers/">&emsp;Freshers</option>
    <%}%>
	<%if("uni-life".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/student-life/uni-life/" selected>&emsp;Uni Life</option>
    <%}else{%>
      <option value="/advice/student-life/uni-life/">&emsp;Uni Life</option>
    <%}%>
	<%if("blog".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/blog/" selected>Blog</option>
    <%}else{%>
      <option value="/advice/blog/">Blog</option>
    <%}%>
	<%if("news".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/news/" selected>News</option>
    <%}else{%>
      <option value="/advice/news/">News</option>
    <%}%>
	<%if("guides".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/guides/" selected>Guides</option>
    <%}else{%>
      <option value="/advice/guides/">Guides</option>
    <%}%>
	<%if("subject-guides".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/guides/subject-guides/" selected>&emsp;Subject Guides</option>
    <%}else{%>
      <option value="/advice/guides/subject-guides/">&emsp;Subject Guides</option>
    <%}%>
	<%if("ultimate-guides".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/guides/ultimate-guides/" selected>&emsp;Ultimate Guides</option>
    <%}else{%>
      <option value="/advice/guides/ultimate-guides/">&emsp;Ultimate Guides</option>
    <%}%>
	<%if("city-guides".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/guides/city-guides/" selected>&emsp;City Guides</option>
    <%}else{%>
      <option value="/advice/guides/city-guides/">&emsp;City Guides</option>
    <%}%>
	<%if("clearing".equalsIgnoreCase(linkHilight)){%>
      <option value="/advice/clearing/" selected>Clearing</option>
    <%}else{%>
      <option value="/advice/clearing/">Clearing</option>
    <%}%>
    <option value="/advice/parents/" ${linkHilight eq 'parents'? "selected" : ""}>Parents</option>
    <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
    
    <option value="${teacherSectionUrl}" ${linkHilight eq 'coronavirus'? "selected" : ""}><spring:message code="wuni.covid.label"/></option>
    </select>    
</div>