<%
  String ucasArticleLinkHilight = (request.getAttribute("ucasArticleLinkHilight")!=null) ? request.getAttribute("ucasArticleLinkHilight").toString() : "";
%>
<aside class="nav_links fl">
  <div class="sel_deg">
    <span class="sld_deg fnt_lbk">UCAS Guides</span>
    <i class="fa fa-angle-down"></i>
  </div>
  <ul class="clr_anav">
    <li class="<%="ucas-form-advice".equalsIgnoreCase(ucasArticleLinkHilight) ? "act" : ""%>"><a href="/advice/ucas-application-guide/ucas-form-advice/54517/">UCAS Form Advice</a></li>
    <li class="<%="ucas-application-deadlines".equalsIgnoreCase(ucasArticleLinkHilight) ? "act" : ""%>"><a href="/advice/ucas-application-guide/ucas-application-deadlines/54520/"> UCAS Applications </a></li>
    <li class="<%="ucas-tariff-points-table".equalsIgnoreCase(ucasArticleLinkHilight) ? "act" : ""%>"><a href="/advice/ucas-application-guide/ucas-tariff-points-table/54521/">Tarrif Points</a></li>
    <li class="<%="how-to-write-the-perfect-personal-statement".equalsIgnoreCase(ucasArticleLinkHilight) ? "act" : ""%>"><a href="/advice/ultimate-guides/how-to-write-the-perfect-personal-statement/39162/">Personal Statement</a></li>
    <li class="<%="open-days-everything-you-need-know".equalsIgnoreCase(ucasArticleLinkHilight) ? "act" : ""%>"><a href="/advice/ultimate-guides/open-days-everything-you-need-know/37144/">Open Days</a></li>
    <li class="<%="how-to-choose-the-right-university".equalsIgnoreCase(ucasArticleLinkHilight) ? "act" : ""%>"><a href="/advice/ultimate-guides/how-to-choose-the-right-university/37097/">Where to Study:<br>Home or Away?</a></li> 
    <li class="<%="how-to-choose-the-right-university".equalsIgnoreCase(ucasArticleLinkHilight) ? "act" : ""%>"><a onclick="openLightBox('ucas');">UCAS tariff calculator</a></li> 
  </ul>
</aside>
<script type="text/javascript">
	var $ucascal = jQuery.noConflict();
  function jqueryWidth() {
    return dev(this).width();
  }
	$ucascal(function() {		
     $ucascal(".sel_deg").click(function(){
       if (jqueryWidth() <= 992) {
         $ucascal(".clr_anav").slideToggle();
       }  
     }); 		
     $ucascal("ul.clr_anav li a").click(function(){
       if (jqueryWidth() <= 992) {
         $ucascal(".sld_deg").html($ucascal(this).html())
         $ucascal(".clr_anav").slideToggle();
       }
		});
	});
</script>