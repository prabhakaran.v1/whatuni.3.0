<%@page import="WUI.utilities.CommonUtil" %>
<%    
  /*String totalRecordCount = (request.getAttribute("totalRecordCount")!=null) ? request.getAttribute("totalRecordCount").toString() : "0";  
  int noOfPage = (Integer.parseInt(totalRecordCount)/20);
  if((Integer.parseInt(totalRecordCount)%20) > 0) {
    noOfPage++; 
  }*/
  String pageNo = (request.getAttribute("page")!=null) ? request.getAttribute("page").toString() : "1";
  String pageName = (request.getAttribute("pageName")!=null) ? request.getAttribute("pageName").toString() : "1";
  String keyword = (request.getAttribute("keyword")!=null) ? request.getAttribute("keyword").toString() : "1";
%>
<div class="asrcc">
  <h1>Student advice</h1>
</div>
<div class="asrcb">
  <div class="pros_search fl w100p">
    <form id="articleSearchForm" method="post" action="">
      <fieldset class="fl">
        <%if(request.getAttribute("keyword") != null) {%>
            <input class="fnt_lrg tx_bx" autocomplete="off" name="keyword" id="keyword" value="<%=keyword%>" type="text" onkeypress="return keypressSubmit('1', 'adviceSearch', event);" /> 
        <%}else{%>
            <input class="fnt_lrg tx_bx" autocomplete="off" name="keyword" id="keyword" value="Enter keyword" type="text" onkeypress="return keypressSubmit('1', 'adviceSearch', event);" onblur="if(this.value == ''){this.value='Enter keyword';this.style.color='#a6a6a6';}" onfocus="if(this.value == 'Enter keyword'){this.value = '';this.style.color='#000';}" /> 
        <%}if(pageNo!=null && "".equals(pageNo)) {%>
            <input type="hidden" name="page" id="page" value="<%=pageNo%>" />
            <input type="hidden" name="pageName" id="pageName" value="<%=pageName%>" />
        <%}else{%>
            <input type="hidden" name="page" id="page" value="1" />
            <input type="hidden" name="pageName" id="pageName" value="adviceSearch" />
        <%}%>
        <a class="icon_srch fr" onclick="return adviceSearchSubmit('1','adviceSearch');"><i class="fa fa-search fa-1_5x"></i></a>
      </fieldset>
    </form>
  </div>
</div>