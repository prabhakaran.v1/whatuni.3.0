<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction" %>
<%
  CommonFunction commonFun = new CommonFunction();
  String primaryCategory = request.getParameter("primaryCategory");
  String position = request.getParameter("position");
  String subCategory = request.getParameter("subCategory");  
  String adviceHomeList = "";
  if("EditorsPick".equals(primaryCategory)){
    adviceHomeList = "adviceHomeEditorsPickList";
  }else if("ResearchPrep".equals(primaryCategory)){
    adviceHomeList = "adviceHomeResearchPrepList";
  }else if("StudentLife".equals(primaryCategory)){
    adviceHomeList = "adviceHomeStudLifeList";
  }else if("Blog".equals(primaryCategory)){
    adviceHomeList = "adviceHomeBlogList";
  }else if("News".equals(primaryCategory)){
    adviceHomeList = "adviceHomeNewsList";
  }else if("Guides".equals(primaryCategory)){
    adviceHomeList = "adviceHomeGuidesList";
  }else if("Clearing".equals(primaryCategory)){
    adviceHomeList = "adviceHomeClearingList";
  }else if("Parents".equals(primaryCategory)){ //Added parents category on 08_Aug_2017, By Thiyagu G
    adviceHomeList = "adviceHomeParentsList";
  }else if("Accommodation".equals(primaryCategory)){
    adviceHomeList = "adviceHomeAccommodationList";
  }else if("coronavirus".equalsIgnoreCase(primaryCategory)){  //Added Teacher category on 04_Mar_2020, By Sri Sankari R
	    adviceHomeList = "adviceHomeTeacherList";
  }
  ArrayList iterateLst =  new ArrayList();
  if(adviceHomeList != ""){
	  request.setAttribute("adviceHomeList", request.getAttribute(adviceHomeList));
  }
%>
<%--  <c:set var="adviceHomeList" scope="request" value="${adviceHomeList}"/>
 --%>   <c:if test="${not empty requestScope.adviceHomeList}"> 
    <%String sponsorClass= "";%>
      <c:forEach var="editorsPickList" items="${requestScope.adviceHomeList}" varStatus="i" end="0">
         <c:set var="checkPostId" value="${editorsPickList.postId}"/>
            <%if("right".equals(position)){%>
              <div class="box_bg_cnr rht bg_cover">                
                <article class="ab-img fRg">
                    <%String readClassName = "img_ppn";
                    if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId"))){
                        readClassName = "img_ppn read";
                    }}%> 
                    <figure class="<%=readClassName%>">
                    <a href="${editorsPickList.adviceDetailURL}" title="">
                      <c:if test="${not empty editorsPickList.imageName}">
                        <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${editorsPickList.imageName}"/>" alt="<c:out value="${editorsPickList.mediaAltText}"/>" title="<c:out value="${editorsPickList.postTitle}"/>"/>
                      </c:if>
                        <%if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId"))){%>
                        <span class="re-img"></span>
                        <%}}%>
                    </a>
                    </figure>
                </article>
                <article class="ab-cont">
                    <%if("yes".equals(subCategory)){%><h4><c:out value="${editorsPickList.subCategoryName}"/></h4><%}%>
                    <h3 class="pb15"><a href="<c:out value="${editorsPickList.adviceDetailURL}"/>"><c:out value="${editorsPickList.postTitle}"/></a></h3>
                    <p><c:out value="${editorsPickList.postShortText}"/></p>
                    <c:if test="${not empty editorsPickList.sponsorUrl and editorsPickList.collegeDisplayName}">
                      <c:set var="collegeNameId" value="${editorsPickList.collegeDisplayName}"/>        
                           <%String gaCollegeName = commonFun.replaceSpecialCharacter(pageContext.getAttribute("collegeNameId").toString()).toLowerCase(); %> 
                            <div class="clr_slwrap">
                              <div class="clr_splgo">            
                                <a target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${editorsPickList.collegeId}"/>','<c:out value="${editorsPickList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${editorsPickList.sponsorUrl}"/>"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${editorsPickList.sponsorLogo}"/>" alt="" title="" /></a>  
                              </div>
                              <div class="clr_asp">
                                <h5 class="clr_stxt">Sponsored by</h5>
                                <h5>
                                   <a class="clr_suni" target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${editorsPickList.collegeId}"/>','<c:out value="${editorsPickList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${editorsPickList.sponsorUrl}"/>"><c:out value="${editorsPickList.collegeDisplayName}" /></a>
                                </h5>
                              </div>
                            </div>
                    </c:if>
                    <div class="fcont">
                        <div class="fcontIc"><span class="fa fa-clock-o pr5"></span><c:out value="${editorsPickList.updatedDate}"/></div>
                        <%-- <c:if test="${not empty editorsPickList.readCount}">
                          <div class="fcontIc"><span class="fa fa-eye pr5"></span><c:out value="${editorsPickList.readCount}"/></div>
                        </c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                    </div>
                </article>
              </div>
            <% } else if("left".equals(position)) {%>
              <div class="box_bg_cnr lft bg_cover">                
                <article class="ab-img">
                    <%String readClassName = "img_ppn";
                    if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId"))){
                        readClassName = "img_ppn read";
                    }}%>
                    <figure class="<%=readClassName%>">
                        <a href="<c:out value="${editorsPickList.adviceDetailURL}"/>" title="<c:out value="${editorsPickList.postTitle}"/>">
                          <c:if test="${not empty editorsPickList.imageName}">
                            <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${editorsPickList.imageName}"/>" alt="<c:out value="${editorsPickList.mediaAltText}"/>" title="<c:out value="${editorsPickList.postTitle}"/>" />
                        </c:if>
                        <%if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId"))){%>
                            <span class="re-img"></span>
                            <%}}%>
                        </a>
                    </figure>
                </article>
                <article class="ab-cont">
                    <div class="art-desc">
                    <%if("yes".equals(subCategory)){%>
                       <c:if test="${not empty editorsPickList.subCategoryName}">
                         <h4><c:out value="${editorsPickList.subCategoryName}"/></h4>
                      </c:if>
                      <%}%>
                    <h3><a href="<c:out value="${editorsPickList.adviceDetailURL}"/>" title="<c:out value="${editorsPickList.postTitle}"/>"><c:out value="${editorsPickList.postTitle}"/></a></h3>
                    </div>
                     <p>${editorsPickList.postShortText}</p>
                     <c:if test="${not empty editorsPickList.sponsorUrl}">
                     <c:if test="${not empty editorsPickList.collegeDisplayName}">
                      <c:set var="collegeNameDisId" value="${editorsPickList.collegeDisplayName}"/>
                        <%String gaCollegeName = commonFun.replaceSpecialCharacter(pageContext.getAttribute("collegeNameDisId").toString()).toLowerCase(); %> 
                        <div class="clr_slwrap">
                          <div class="clr_splgo">            
                            <a target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${editorsPickList.collegeId}"/>','<c:out value="${editorsPickList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${editorsPickList.sponsorUrl}"/>"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${editorsPickList.sponsorLogo}"/>" alt="" title="" /></a>  
                          </div>
                          <div class="clr_asp">
                            <h5 class="clr_stxt">Sponsored by</h5>
                            <h5>
                               <a class="clr_suni" target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${editorsPickList.collegeId}"/> ','<c:out value="${editorsPickList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${editorsPickList.sponsorUrl}"/>"><c:out value="${editorsPickList.collegeDisplayName}"/></a>
                            </h5>
                          </div>
                        </div>
                        </c:if>
                    </c:if>
                   <div class="fcont">
                     <div class="fcontIc"><span class="fa fa-clock-o pr5"></span><c:out value="${editorsPickList.updatedDate}"/></div>  
                       <%-- <c:if test="${not empty editorsPickList.readCount}">                      
                         <div class="fcontIc"><span class="fa fa-eye pr5"></span><c:out value="${editorsPickList.readCount}"/></div>
                     </c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                   </div>
                </article>
              </div>
            <%}%>
        </c:forEach>     
        <section class="art-ml p0">
            <ul class="art-lst">  
              <c:forEach var="editors1PickList" items="${requestScope.adviceHomeList}" varStatus="i" begin="1" end="3">     
                <c:if test="${not empty editors1PickList.sponsorUrl}"> 
                  <%sponsorClass="clsp";%>
               </c:if>
             </c:forEach>
             <c:forEach var="editorsPickList" items="${requestScope.adviceHomeList}" varStatus="i" begin="1" end="3">
               <c:set var="checkPostId1" value="${editorsPickList.postId}"/>
                <li>
                    <div class="art-view">
                        <%String readClassName = "img_ppn";
                        if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId1"))){
                            readClassName = "img_ppn read";
                        }}%>
                        <div class="<%=readClassName%>">
                            <a href="<c:out value="${editorsPickList.adviceDetailURL}"/>" title="">
                            <c:if test="${not empty editorsPickList.imageName}">
                              <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${editorsPickList.imageName}"/>" alt="<c:out value="${editorsPickList.mediaAltText}"/>" title="<c:out value="${editorsPickList.postTitle}"/>" />
                            </c:if>
                            <%if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId1"))){%>
                            <span class="re-img"></span>
                            <%}}%>
                            </a>
                        </div>
                        <div class="art-inc <%=sponsorClass%>">
                           <div class="art-desc">
                            <%if("yes".equals(subCategory)){%>
                              <c:if test="${not empty editorsPickList.subCategoryName}">
                                <h4><c:out value="${editorsPickList.subCategoryName}"/></h4>
                               </c:if>
                             <%}%>
                            <h3><a href="<c:out value="${editorsPickList.adviceDetailURL}"/>" title="<c:out value="${editorsPickList.postTitle}"/>"><c:out value="${editorsPickList.postTitle}"/></a></h3>                        
                           </div>    
                           <c:if test="${not empty editorsPickList.sponsorUrl and editorsPickList.collegeDisplayName}">
                             <c:set var="collegeNameDispId" value="${editorsPickList.collegeDisplayName}" />
                               <%String gaCollegeName = commonFun.replaceSpecialCharacter(pageContext.getAttribute("collegeNameDispId").toString()).toLowerCase(); %>
                               <div class="clr_slwrap">
                                 <div class="clr_splgo">            
                                   <a target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${editorsPickList.collegeId}"/>','<c:out value="${editorsPickList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${editorsPickList.sponsorUrl}"/>"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${editorsPickList.sponsorLogo}"/>" alt="" title="" /></a>  
                                 </div>
                                 <div class="clr_asp">
                                    <h5 class="clr_stxt">Sponsored by</h5>
                                    <h5>
                                     <a class="clr_suni" target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${editorsPickList.collegeId}"/>','<c:out value="${editorsPickList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${editorsPickList.sponsorUrl}"/>"><c:out value="${editorsPickList.collegeDisplayName}"/></a>
                                    </h5>
                                 </div>
                               </div>
                            </c:if>
                            <div class="fcont">
                              <div class="fcontIc"><i class="fa fa-clock-o pr5"></i><c:out value="${editorsPickList.updatedDate}"/></div>
                                 <%-- <c:if test="${not empty editorsPickList.readCount}">                        
                                    <div class="fcontIc"><i class="fa fa-eye pr5 pr5"></i><c:out value="${editorsPickList.readCount}"/></div>
                                </c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                            </div>
                        </div>
                    </div>
                </li>
            </c:forEach>           
            </ul>
        </section>            
 </c:if>        
<c:if test="${not empty requestScope.adviceHomeList}">
        <section class="sh-res p0">
            <div id="<%=adviceHomeList%>" style="display:none;">
            <ul class="sh-lst">
                <%String rclassname="";%>
                   <c:forEach var="editorsPickList" items="${requestScope.adviceHomeList}" varStatus="i" begin="4">
                     <c:set var="indexList" value="${i.index}"/>
                   <%rclassname="";%>
                 <%if(Integer.parseInt(pageContext.getAttribute("indexList").toString())%2==0){%>
                   <c:forEach var="innerList" items="${adviceHomeList}" varStatus="ind" begin="${i.index}" end="2">
                     <c:if test="${not empty innerList.sponsorUrl}">
                        <%rclassname="clssp";%>
                      </c:if>
                    </c:forEach>
                    <li class="<%=rclassname%>">
                    
                    <div class="shl">
                        <h3><a href="<c:out value="${editorsPickList.adviceDetailURL}"/>" title="<c:out value="${editorsPickList.postTitle}"/>">
                        <c:out value="${editorsPickList.postTitle}"/></a></h3>
                        <c:if test="${not empty editorsPickList.sponsorUrl and editorsPickList.collegeDisplayName}">
                          <c:set var="collegeNameDisplId" value="${editorsPickList.collegeDisplayName}" />
                            <%String gaCollegeName = commonFun.replaceSpecialCharacter(pageContext.getAttribute("collegeNameDisplId").toString()).toLowerCase(); %>
                           <div class="clr_slwrap">
                             <div class="clr_splgo">            
                               <a target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${editorsPickList.collegeId }"/>','<c:out value="${editorsPickList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${editorsPickList.sponsorUrl}"/>"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${editorsPickList.sponsorLogo}"/>" alt="" title="" /></a>  
                             </div>
                             <div class="clr_asp">
                                <h5 class="clr_stxt">Sponsored by</h5>
                                <h5>
                                 <a class="clr_suni" target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${editorsPickList.collegeId}"/>','<c:out value="${editorsPickList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${editorsPickList.sponsorUrl}"/>"><c:out value="${editorsPickList.collegeDisplayName}"/></a>
                                </h5>
                             </div>
                           </div>
                        </c:if>                      
                        <div class="fcont">
                            <div class="fcontIc"><i class="fa fa-clock-o pr5"></i><c:out value="${editorsPickList.updatedDate}"/></div>
                            <%-- <c:if test="${editorsPickList.readCount}">
                              <div class="fcontIc"><i class="fa fa-eye pr5 pr5"></i><c:out value="${editorsPickList.readCount}"/></div>
                            </c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                        </div>                        
                    </div>
                 <%}%>
                 <%if(Integer.parseInt(pageContext.getAttribute("indexList").toString())%2==1){%>        
                    <div class="shr">
                        <h3><a href="<c:out value="${editorsPickList.adviceDetailURL}"/>" title="<c:out value="${editorsPickList.postTitle}"/>">
                        <c:out value="${editorsPickList.postTitle}"/></a></h3>
                        <c:if test="${not empty editorsPickList.sponsorUrl and editorsPickList.collegeDisplayName}">
                          <c:set var="collegeNameDisplayId" value="${editorsPickList.collegeDisplayName}"/>
                            <%String gaCollegeName = commonFun.replaceSpecialCharacter(pageContext.getAttribute("collegeNameDisplayId").toString()).toLowerCase(); %> 
                           <div class="clr_slwrap">
                             <div class="clr_splgo">            
                               <a target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${editorsPickList.collegeId}"/>','<c:out value="${editorsPickList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${editorsPickList.sponsorUrl}"/>"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${editorsPickList.sponsorLogo}"/>" alt="" title="" /></a>  
                             </div>
                             <div class="clr_asp">
                                <h5 class="clr_stxt">Sponsored by</h5>
                                <h5>
                                 <a class="clr_suni" target="_blank" onclick="javascript:cpearticlesponsorurl('<c:out value="${editorsPickList.collegeId}"/>','<c:out value="${editorsPickList.sponsorUrl}"/>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="<c:out value="${editorsPickList.sponsorUrl}"/>"><c:out value="${editorsPickList.collegeDisplayName}"/></a>
                                </h5>
                             </div>
                           </div>
                        </c:if>
                        <div class="fcont">
                            <div class="fcontIc"><i class="fa fa-clock-o pr5"></i><c:out value="${editorsPickList.updatedDate}"/></div>
                            <%-- <c:if test="${not empty editorsPickList.readCount}">
                              <div class="fcontIc"><i class="fa fa-eye pr5 pr5"></i><c:out value="${editorsPickList.readCount}"/></div>
                            </c:if> --%><%--commented for March 2020 rel by Sangeeth.S--%>
                        </div>
                    </div>
                    </li>
                 <%}%>
                </c:forEach>
            </ul>
            </div>
        </section>
        <!-- View More -->
        <c:set var="listSize" value="${fn:length(adviceHomeList)}"/>
        <%int size = Integer.parseInt(pageContext.getAttribute("listSize").toString());
        if(size>4){%>
            <div class="fl w100p">
                <a id="<%=adviceHomeList%>_More" onclick="adviceShowHide('<%=adviceHomeList%>','<%=adviceHomeList%>_More');" class="clear btn1 btn2 mt30" title="VIEW MORE"><i class="fa fa-plus"></i>VIEW MORE</a>
            </div>
        <%}%>
</c:if>  