<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.*" %>
<%String compareAllFlag = ""; %>
<c:if test="${not empty requestScope.univsInCityAdviceList}">
<c:set var="sizeoflist"> ${fn:length(requestScope.univsInCityAdviceList)} </c:set>
<!-- Other Courses you may like Starts-->	
            <%StringBuffer compareallcourses = new StringBuffer();boolean compareflag = false;
            String gaCollegeName = "";
            String compareAllCollegeNames = "";
            int beanSize = 0; //Hided Compare All link if only one course/uni displayed in this pod for the bug:39149, on 08_Aug_2017, By Thiyagu G
            %>
            <section class="nonadvert bg_bluelt fulWd grapm1 mt20">
            <c:forEach var="otherCoursesAdvice" items="${requestScope.univsInCityAdviceList}" varStatus="i" end="0"> 
              <c:set var="index" value="${i.index}"/>             
              <h2 class="fl">Universities near <c:out value="${otherCoursesAdvice.cityName}"/></h2></c:forEach>
              <div class="borderbot1 mdev fl mt25"></div>
              <c:forEach var="otherCoursesAdvice" items="${requestScope.univsInCityAdviceList}" varStatus="i" >
              <c:set var="collegeId" value="${otherCoursesAdvice.collegeId}"/>
              <c:set var="shortListFlag" value="${otherCoursesAdvice.courseInBasket}"/>
              <c:set var="univsInCityAdviceSize"> ${fn:length(requestScope.univsInCityAdviceList)} </c:set>
              <c:if test="${not empty otherCoursesAdvice.collegeNameDisplay}">
              <c:set var="collegeName" value="${otherCoursesAdvice.collegeNameDisplay}"/>
                <%gaCollegeName = new CommonFunction().replaceSpecialCharacter(pageContext.getAttribute("collegeName").toString());%>
              </c:if>
              <%
                beanSize = Integer.parseInt(pageContext.getAttribute("univsInCityAdviceSize").toString()) ;
                compareAllCollegeNames += gaCollegeName; //Adding college names for compare all insights log by Prabha on 29_Mar_2016
                compareallcourses.append(pageContext.getAttribute("collegeId").toString() );
                if(Integer.parseInt(pageContext.getAttribute("sizeoflist").toString()) - 1 != Integer.parseInt(pageContext.getAttribute("index").toString())){
                  compareallcourses.append(",");
                  compareAllCollegeNames += GlobalConstants.SPLIT_CONSTANT; //##SPLIT##
                }
                if(!"TRUE".equalsIgnoreCase(pageContext.getAttribute("shortListFlag").toString())){
                  compareflag = true;
                }        
              %>     
              <c:if test="${not empty otherCoursesAdvice.courseId}">             
            <%compareAllFlag = "OTHER_COURSES";%>
            </c:if>
              <div class="mt20 fl fldev">   
              <c:if test="${otherCoursesAdvice.collegeInBasket eq 'TRUE'}">                 
                <%--Add to compare redesign, 03_Feb_2015 By Thiyagu G--%>
                <div class="ver_cmp">
                  <div  class="cmlst" id="basket_div_<c:out value="${otherCoursesAdvice.collegeId}"/>" style="display:none;"> 
                    <div class="compare">
                    <a onclick='addBasket("<c:out value="${otherCoursesAdvice.collegeId}"/>", "C", this,"basket_div_<c:out value="${otherCoursesAdvice.collegeId}"/>", "basket_pop_div_<c:out value="${otherCoursesAdvice.collegeId}"/>", "", "", "<%=gaCollegeName%>");'>
                      <span class="icon_cmp f5_hrt"></span>
                      <span class="cmp_txt">Compare</span>
                      <span class="loading_icon" id="load_<c:out value="${otherCoursesAdvice.collegeId}"/>" style="position:absolute; z-index:1; display:none;">
                      <img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                      <span class="chk_cmp"><span class="chktxt">Add to comparison<em></em></span></span>
                    </a>                 
                    </div>
                  </div>
                  <div class="cmlst act" id="basket_pop_div_<c:out value="${otherCoursesAdvice.collegeId}"/>"> 
                    <div class="compare">
                    <a class="act" onclick='addBasket("<c:out value="${otherCoursesAdvice.collegeId}"/>", "C", this,"basket_div_<c:out value="${otherCoursesAdvice.collegeId}"/>", "basket_pop_div_<c:out value="${otherCoursesAdvice.collegeId}"/>");'>
                      <span class="icon_cmp f5_hrt hrt_act"></span>
                      <span class="cmp_txt">Compare</span>
                      <span class="loading_icon" id="load1_<c:out value="${otherCoursesAdvice.collegeId}"/>" style="position:absolute; z-index:1; display:none;">
                      <img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                      <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                    </a>                 
                    </div>
                    <div id="defaultpop_<c:out value="${otherCoursesAdvice.collegeId}"/>" class="sta" style="display: block;">
                        <%
                            String userId = new SessionData().getData(request, "y");
                            if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
                          %>
                              <a class="view_more" href="/degrees/comparison">View comparison</a>
                          <%}else{%>
                              <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
                          <%}%>
                      </div>
                    <div id="pop_<c:out value="${otherCoursesAdvice.collegeId}"/>" class="sta" style="display:none;"></div>
                  </div>
                  </div>
                </c:if> 
                <c:if test="${otherCoursesAdvice.collegeInBasket ne 'TRUE'}">
                <div class="ver_cmp">
                  <div class="cmlst" id="basket_div_<c:out value="${otherCoursesAdvice.collegeId}"/>"> 
                    <div class="compare">
                    <a onclick='addBasket("<c:out value="${otherCoursesAdvice.collegeId}"/>", "C", this,"basket_div_<c:out value="${otherCoursesAdvice.collegeId}"/>", "basket_pop_div_<c:out value="${otherCoursesAdvice.collegeId}"/>", "", "", "<%=gaCollegeName%>");'>
                      <span class="icon_cmp f5_hrt"></span>
                      <span class="cmp_txt">Compare</span>
                      <span class="loading_icon" id="load_<c:out value="${otherCoursesAdvice.collegeId}"/>" style="position:absolute; z-index:1; display:none;">
                      <img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                      <span class="chk_cmp"><span class="chktxt">Add to comparison<em></em></span></span>
                    </a>                 
                    </div>
                  </div>
                  <div class="cmlst act" id="basket_pop_div_<c:out value="${otherCoursesAdvice.collegeId}"/>" style="display:none;"> 
                    <div class="compare">
                    <a class="act" onclick='addBasket("<c:out value="${otherCoursesAdvice.collegeId}"/>", "C", this,"basket_div_<c:out value="${otherCoursesAdvice.collegeId}"/>", "basket_pop_div_<c:out value="${otherCoursesAdvice.collegeId}"/>");'>
                    <span class="icon_cmp f5_hrt hrt_act"></span>
                    <span class="cmp_txt">Compare</span>
                    <span class="loading_icon" id="load1_<c:out value="${otherCoursesAdvice.collegeId}"/>" style="position:absolute; z-index:1; display:none;"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                    <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                    </a>
                    </div>    
                    <div id="defaultpop_<c:out value="${otherCoursesAdvice.collegeId}"/>" class="sta" style="display: block;">
                        <%
                            String userId = new SessionData().getData(request, "y");
                            if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
                        %>
                                <a class="view_more" href="/degrees/comparison">View comparison</a>
                            <%}else{%>
                                <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
                            <%}%>
                      </div>
                    <div id="pop_<c:out value="${otherCoursesAdvice.collegeId}"/>" class="sta"></div>
                  </div>
                  </div>
                </c:if>                   
                <%
                  String topClassName = "fl img_ppn mr20 noimage";
                %>
                <c:if test="${otherCoursesAdvice.mediaType eq 'PICTURE'}">
                  <c:if test="${not empty otherCoursesAdvice.thumbImageName}">
                    <%topClassName = "fl img_ppn mr20";%>
                </c:if>
                </c:if>
                <c:if test="${otherCoursesAdvice.mediaType eq 'VIDEO'}">
                <c:if test="${not empty otherCoursesAdvice.videoThumbPath}">
                     <%topClassName = "fl img_ppn mr20";%>
                </c:if>
                </c:if>
                <div  class="<%=topClassName%>">
                <c:if test="${not empty otherCoursesAdvice.subOrderItemId and otherCoursesAdvice.subOrderItemId ne '0'}">
                    <c:if test="${otherCoursesAdvice.mediaType eq 'PICTURE'}">
                      <a href="<c:out value="${otherCoursesAdvice.uniHomeURL}"/>">
                        <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${otherCoursesAdvice.thumbImageName}"/>" title="<c:out value="${otherCoursesAdvice.collegeNameDisplay}"/>" alt="<c:out value="${otherCoursesAdvice.collegeNameDisplay}"/>" />
                      </a>
                    </c:if>
                    <c:if test="${otherCoursesAdvice.mediaType eq 'VIDEO'}">
                      <a href="<c:out value="${otherCoursesAdvice.uniHomeURL}"/>">
                          <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src='<c:out value="${otherCoursesAdvice.videoThumbPath}"/>'  title="<c:out value="${otherCoursesAdvice.collegeNameDisplay}"/>" alt="<c:out value="${otherCoursesAdvice.collegeNameDisplay}"/>" />
                      </a>            
                    </c:if>
                  </c:if>
                </div>
                <div class="div_right div_rgFw fl mb20">
                    <div class="fl">
                        <h2>
                        <%--<logic:empty name="otherCoursesAdvice" property="subOrderItemId">
                            <a class="fnt_lbd fnt_18 lh_28 link_blue" href="<bean:write name="otherCoursesAdvice" property="uniHomeURL"/>">
                             <logic:notEmpty  name="otherCoursesAdvice" property="collegeLogo">
                              <img width="116"
                                src="<%=CommonUtil.getImgPath("",i.intValue())%><bean:write name="otherCoursesAdvice" property="collegeLogo" filter="false" />" 
                                title="<bean:write name="otherCoursesAdvice" property="collegeNameDisplay" filter="false" />">
                              </logic:notEmpty>
                            </a>
                       </logic:empty>--%>
                           <a class="fnt_lbd fnt_18 lh_28 link_blue" href="<c:out value="${otherCoursesAdvice.uniHomeURL}"/>" title="<c:out value="${otherCoursesAdvice.collegeNameDisplay}"/>"><c:out value="${otherCoursesAdvice.collegeNameDisplay}"/></a>
                        </h2>
                        <div class="fnt_lbd fnt_14 lh_24 cl_grylt pos_rel"></div>
                    </div>							
                </div>
                <div class="div_three fr">
                    <div class="fr mt5 mb20">
                        <div class="btns_interaction">
                        <c:if test="${not empty otherCoursesAdvice.subOrderWebsite}">
                              <a rel="nofollow" 
                              target="_blank" 
                              class="fl visit-web mr10" 
                              onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<c:out value="${otherCoursesAdvice.gaCollegeName}"/>', <c:out value="${otherCoursesAdvice.websitePrice}"/>); cpeWebClick(this,'<c:out value="${otherCoursesAdvice.collegeId}"/>','<c:out value="${otherCoursesAdvice.subOrderItemId}"/>','<c:out value="${otherCoursesAdvice.cpeQualificationNetworkId}"/>','<c:out value="${otherCoursesAdvice.subOrderWebsite}"/>');var a='s.tl(';" 
                              href="<c:out value="${otherCoursesAdvice.subOrderWebsite}"/>" 
                              title="Visit <c:out value="${otherCoursesAdvice.collegeNameDisplay}"/> website">Visit website <i class="fa fa-caret-right"></i>
                              </a>
                        </c:if>
            
                         <c:if test="${not empty otherCoursesAdvice.subOrderProspectusWebform}">
                            <a rel="nofollow" 
                                target="_blank"
                                class="last fl get-pros mr0"
                                onclick="GAInteractionEventTracking('prospectuswebform', 'interaction', 'Webclick', '<c:out value="${otherCoursesAdvice.gaCollegeName}"/>', <c:out value="${otherCoursesAdvice.webformPrice}"/>); cpeEmailWebformClick(this,'<c:out value="${otherCoursesAdvice.collegeId}"/>','<c:out value="${otherCoursesAdvice.subOrderItemId}"/>','<c:out value="${otherCoursesAdvice.cpeQualificationNetworkId}"/>','<c:out value="${otherCoursesAdvice.subOrderEmailWebform}"/>'); var a='s.tl(';" 
                                href="<c:out value="${otherCoursesAdvice.subOrderProspectusWebform}"/>" 
                                title="Get <c:out value="${otherCoursesAdvice.collegeNameDisplay}"/> Prospectus">Get prospectus
                                <i class="fa fa-caret-right"></i>
                            </a>
                          </c:if>
                          <c:if test="${not empty otherCoursesAdvice.subOrderProspectus}">
                          <c:if test="${empty otherCoursesAdvice.subOrderProspectusWebform}">
                              <a rel="nofollow"
                                  onclick="GAInteractionEventTracking('prospectusbutton', 'engagement', 'Email-Request', '<c:out value="${otherCoursesAdvice.gaCollegeName}"/>'); return prospectusRedirect('/degrees','<c:out value="${otherCoursesAdvice.collegeId}"/>','','','','<c:out value="${otherCoursesAdvice.subOrderItemId}"/>');"
                                  class="last fl get-pros mr0"
                                  title="Get <c:out value="${otherCoursesAdvice.collegeNameDisplay}"/> Prospectus" >Get prospectus
                                  <i class="fa fa-caret-right"></i>
                              </a>
                          </c:if>
                          </c:if>
                        </div>
                    </div>
                </div>
            </div>
              <div class="borderbot1 mdev mt0 fl"></div>                  
              </c:forEach>
              <%--Add to compare redesign, 03_Feb_2015 By Thiyagu G--%>
              <%String currentSubject="test";
              String noneComp = "display:none;";              
              if(beanSize > 1){
                noneComp = "display:block;";
              }
              if(compareflag){%>
              <div class="hor_cmp" style="<%=noneComp%>">                
                <div class="cmlst" id="compareall">                             
                  <div class="compare">
                    <a onclick="compareAll('<%=compareallcourses%>', '<%=compareAllFlag%>', 'A', '', '<%=compareAllCollegeNames%>')">
                      <span class="icon_cmp f5_hrt"></span>
                      <span class="cmp_txt">Compare all</span>
                      <span class="loading_icon" id="load_icon" style="position:absolute; z-index:1; display:none"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                      <span class="chk_cmp"><span class="chktxt" style="display:;">Add to comparison<em></em></span></span>
                    </a>
                  </div>
                </div>
                <div class="cmlst act" id="uncompareall" style="display:none">
                  <div class="compare">
                    <a class="act" onclick="compareAll('<%=compareallcourses%>', '<%=compareAllFlag%>', 'R');">
                      <span class="icon_cmp f5_hrt hrt_act"></span>
                      <span class="cmp_txt">Compare all</span>
                      <span class="loading_icon" id="load_icon" style="position:absolute; z-index:1; display:none"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                      <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                    </a>
                  </div>
                  <div id="popCourseIds" class="sta"></div>
                  <%--Added code for the bug:28713 on 03_Oct_2017, By Thiyagu G--%>
                  <div id="loadPopCourseIds" class="sta" style="display:none;">
                    <a class="view_more" onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');">View comparison</a>
                  </div>
                </div>
              </div>            
          <%}else{%>
            <div class="hor_cmp" style="<%=noneComp%>">
              <div class="cmlst" id="compareall">
                <div class="compare">
                  <a onclick="compareAll('<%=compareallcourses%>', '<%=compareAllFlag%>', 'A', '', '<%=compareAllCollegeNames%>')">
                    <span class="icon_cmp f5_hrt"></span>
                    <span class="cmp_txt">Compare all</span>
                    <span class="loading_icon" id="load_icon" style="position:absolute; z-index:1; display:none"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                    <span class="chk_cmp"><span class="chktxt" style="display:;">Add to comparison<em></em></span></span>
                  </a>
                </div>
              </div>
              <div class="cmlst act" id="uncompareall" style="display:none">
                <div class="compare">                        
                  <a class="act" onclick="compareAll('<%=compareallcourses%>', '<%=compareAllFlag%>', 'R');">
                    <span class="icon_cmp f5_hrt hrt_act"></span>
                    <span class="cmp_txt">Compare all</span>
                    <span class="loading_icon" id="load_icon" style="position:absolute; z-index:1; display:none"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                    <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                  </a>
                </div> 
                <div id="popCourseIds" class="sta"></div>
                <%--Added code for the bug:28713 on 03_Oct_2017, By Thiyagu G--%>
                <div id="loadPopCourseIds" class="sta" style="display:none;">
                  <a class="view_more" onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');">View comparison</a>
                </div>
              </div>
            </div>                
          <%}%>
          <input type="hidden" id="currentcompare" value=""/> 
          <input type="hidden" id="comparestatus" value=""/>
          <input type="hidden" id="compareallcourses" value="<%=compareallcourses%>"/> <%--Added code for the bug:28713 on 03_Oct_2017, By Thiyagu G--%>
            </section>
</c:if>