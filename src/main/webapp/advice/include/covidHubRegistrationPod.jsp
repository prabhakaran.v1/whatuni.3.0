<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.SessionData, WUI.utilities.CommonFunction, WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>
<%
  String emailDomainJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.email.domain.js");
  CommonUtil util = new CommonUtil();
  String TCVersion = util.versionChanges(GlobalConstants.TERMS_AND_COND_VER);
%>
<c:if test="${empty sessionScope.userInfoList}">
  <div class="fwrp covid_hub_reg">
    <div class="fwrpi">
      <fieldset>
        <fieldset class="fl login_txt">
          <label for="fname">
            <h2 class="mb15">Stay up to date with news from Whatuni</h2> 
			<span id="existEmailError" class="fnt_lrg fnt_16">Receive emails from us providing you with the latest university news and guides, including how COVID-19 is affecting university intakes.</span>
		  </label>
        </fieldset>
      </fieldset>
      <fieldset class="row-fluid mb8 mt12">
        <fieldset class="fcwd fl mr18 w50p">
		  <input id="covid_fname" name="firstName" class="ql-inpt adiptxt usr" type="text" value="" maxlength="40" autocomplete="off" onblur="addAndRemoveOverlapClassName(this);">
		  <label for="fname" class="lbco">First name*</label>
		  <!-- <span class="cmp" id="firstNameYText" style="display: none;"> 
			<div class="hdf5"></div>
			<div>We'd like this information so we don't call you by the wrong name. That'd be kinda rude.</div>
			<div class="linear"></div>
		  </span> -->
		  <div class="qler1" id="firstName_covid_error" style="display: none;"></div>
        </fieldset>
        <fieldset class="fcwd fl w50p">
		  <input id="covid_lname" name="lastName" class="ql-inpt adiptxt usr" type="text" value="" maxlength="40" autocomplete="off" onblur="addAndRemoveOverlapClassName(this);"> 
		  <label for="lname" class="lbco">Last name*</label>
		  <div class="qler1" id="lastName_covid_error" style="display:none;"></div>
        </fieldset>
        <fieldset class="fcwd fl w100p">
          <input id="covid_email" name="emailAddress" class="ql-inpt adiptxt usr" onkeydown="hideEmailDropdown(event, 'autoEmailId');" type="text" value="" maxlength="120" autocomplete="off" onblur="addAndRemoveOverlapClassName(this);">
		  <label for="email" class="lbco" id="emailId_lbl">Email address*</label>
		  <div id="autoEmailId" style="display: none; width: 820px;"></div>
		  <!-- <span id="emailYText" class="cmp rght" style="display: none;">
		    <div class="hdf5"></div>
		    <div>Tell us your email and we'll reward you with...an email.</div>
		    <div class="linear"></div>
		  </span>-->
		  <div class="qler1" id="email_covid_error" style="display:none;"></div>
        </fieldset>
      </fieldset>
      <fieldset>
        <fieldset class="fl login_txt pt15"> 
          <span class="chk_btn">
            <input type="checkbox" id="covid_spamBox" class="chkbx1">
            <span class="chk_mark"></span>
          </span>
          <label for="fname"> 
            <span id="existEmailError" class="fnt_lrg fnt_12">
              I confirm I'm over 13 and agree to the <a href="javascript:void(0);" onclick="window.open('<%=TCVersion%>','termspopup','width=650,height=400,scrollbars=yes,resizable=yes');" title="Whatuni community" class="link_blue fnt_lrg">terms and conditions</a>
              and <a  href="javascript:void(0);" onclick="showPrivacyPolicyPopup()" title="privacy notice" class="link_blue fnt_lrg">privacy notice</a>
              , and agree to become a member of the <%-- <a href="javascript:void(0);" onclick="window.open('<%=TCVersion%>','termspopup','width=650,height=400,scrollbars=yes,resizable=yes');" title="Whatuni community" class="link_blue fnt_lrg">Whatuni community</a> --%>Whatuni community.
              <span class="as_trk">*</span>
            </span>
          </label>
          <p class="qler1" id="checkbox_covid_error" style="display:none;"></p>
        </fieldset>
        <fieldset class="fr wd100 fsubg">
          <p id="covidMsgParam"></p>
          <p id="loadinggif" style="display:none;">
            <span id="loadgif">
              <img title="" alt="Loading" src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/indicator1.gif", 0)%>" class="loading">
            </span>
          </p> 
          <a onclick="covidHubRegistrationBox();" class="btn1 blue w219 mt5" title="GET FREE NEWSLETTERS" aria-label="GET FREE NEWSLETTERS">GET FREE NEWSLETTERS<i class="fa fa-caret-right savTag"></i></a> 
        </fieldset>
      </fieldset>
    </div>
  </div>
  <script type="text/javascript" id="domnScptId">
    var $mdv1 = jQuery.noConflict();
    $mdv1(document).ready(function(e){
      $mdv1.getScript("<%=CommonUtil.getJsPath()%>/js/emaildomain/<%=emailDomainJs%>", function(){        
        var arr = ["covid_email", "", ""];
        $mdv1("#covid_email").autoEmail(arr);        
    	 });      
    });
  </script>
  <input type="hidden" id="domainLstId" value='<%=java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.form.email.domain.list")%>' />
</c:if>