<%@page import="WUI.utilities.CommonUtil, java.util.ArrayList, WUI.utilities.CommonFunction, WUI.utilities.SessionData, java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
</header> 
<!--Article Page Starts-->
<div class="article_main pb40 cf noart_data">
    <!-- Article Content Section -->
    <section class="p0">
        <div class="ad_cnr">
            <div class="content-bg">
                <div class="artCont">   
                    <article>
                        <aside class="artLCo">                                
                                <h1>Sorry No Article 
                                <c:if test="${not empty requestScope.noArticleFlag}">
                                   <c:out value="${requestScope.noArticleFlag}"/>
                                </c:if>
                                Available</h1>
                        </aside>
                    </article>
                    <aside class="mstPop fr">
                    <c:if test="${empty requestScope.browseSearchUrl}">
                    <c:if test="${not empty requestScope.mostPopularAdviceList}">       
                                <h2 class="pb15">Most popular</h2>                                    
                                <ul class="rgt-lst">
                                <c:forEach var="mostPopularAdvice" items="${requestScope.mostPopularAdviceList}" varStatus="i">
                                        <li>
                                            <figure><a href="<c:out value="${mostPopularAdvice.adviceDetailURL}"/>"><img src="<c:out value="${mostPopularAdvice.imageName}"/>" alt="<c:out value="${mostPopularAdvice.mediaAltText}"/>" title="<c:out value="${mostPopularAdvice.postUrl}"/>"></a></figure>
                                            <p><a href="<c:out value="${mostPopularAdvice.adviceDetailURL}"/>"><c:out value="${mostPopularAdvice.postUrl}"/></a></p>
                                        </li>
                                    </c:forEach>
                                </ul>
                        </c:if>
                        <c:if test="${not empty requestScope.trendingPopularAdviceList}">
                                <ul class="rgt-lst mt30">
                                    <h2 class="pb15">Trending</h2>
                                    <c:forEach var="trendingPopularAdvice" items="${requestScope.trendingPopularAdviceList}" varStatus="i">
                                        <li>
                                            <figure><a href="<c:out value="${trendingPopularAdvice.adviceDetailURL}"/>"><img src="<c:out value="${trendingPopularAdvice.imageName}"/>" alt="<c:out value="${trendingPopularAdvice.mediaAltText}"/>" title="<c:out value="${trendingPopularAdvice.postUrl}"/>"></a></figure>
                                            <p><a href="<c:out value="${trendingPopularAdvice.adviceDetailURL}"/>"><c:out value="${trendingPopularAdvice.postUrl}"/></a></p>
                                        </li>
                                    </c:forEach>
                                </ul>
                        </c:if> 
                        </c:if>
                        <c:if test="${not empty requestScope.browseSearchUrl}">
                            <div class="yns">
                            <div class="mstPop nxs">
                                <h2 class="txt_cnr">Your next steps</h2>
                                <a title="FIND DEGREES" href="<c:out value="${requestScope.browseSearchUrl}"/>" class="btn1 blue mt23">FIND DEGREES<i class="fa fa-long-arrow-right"></i></a>
                                <a title="MORE GUIDES" class="btn1 bg_orange marBtm" href="/advice/guides">MORE GUIDES<i class="fa fa-long-arrow-right"></i></a>
                            </div>
                            <div id="div-gpt-ad-subject_guide-0" class="asggb">
                            <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
                            <script type="text/javascript">
                                 googletag.cmd.push(function() {
                                 googletag.display("div-gpt-ad-subject_guide-0");
                                 });
                            </script>       
                            </c:if>                 
                        </div>
                        </div>
                        </c:if>
                    </aside>
                </div>
                <div class="fclear"></div>
            </div>
        </div>
    </section>
    <!-- Other Courses you may like Starts-->	
    <section class="p0">
        <div class="ad_cnr ad_cour">
            <div class="content-bg">                
                <div class="borderbot mt40 fl mb25"></div>
                <c:if test="${not empty requestScope.otherCoursesAdviceList}">
                    <jsp:include page="/advice/include/courseYouMightLike.jsp"/>
                </c:if>               
                <!-- Other Courses you may like Starts-->
                <c:if test="${not empty requestScope.univsInCityAdviceList}">
                    <jsp:include page="/advice/include/subjectGuide.jsp"/>
                </c:if>
                <!-- Other Courses you may like Ends-->
            </div>
        </div>
    </section>    
    <!-- Other Courses you may like Ends-->    
    <div class="flyOut"></div>

</div>
<!--Article Page Ends-->
<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>
