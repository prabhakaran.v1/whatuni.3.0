<%--
 *
 *
   * LAYOUT to define single-middle-layout like below (middle will occupy full width, no left/right columns)
 *
 *  -----------------------
 *  | 	Header            |
 *  -----------------------
 *  |	Middle 		  |
 *  |                     |
 *  |                     |
 *  |   Footer            |
 *  -----------------------
 *
 * @author   Sabapathi S
 * @since    whatuni1.0_20190129 - initial draft
 * @version  1.1
 *
--%>

<!DOCTYPE html>
<html lang="en" dir="ltr">
	
   <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
   <%-- oracle.jsp.util.PublicUtil.setWriterEncoding(out, "UTF-8"); --%>
   <% request.setCharacterEncoding("UTF-8"); %>
   <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
	
   <head>
		<tiles:insertAttribute name="thinngsToincludeInHead"/>
		<tiles:insertAttribute name="miscellaneousTracking1"/>
   </head>
   
   
   <tiles:insertAttribute name="bodyContent"/>
   
</html>
