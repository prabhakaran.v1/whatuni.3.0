<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" %>

<%  CommonFunction common = new CommonFunction();
    String bc_breadCrumb = request.getAttribute("breadCrumb") != null && request.getAttribute("breadCrumb").toString().trim().length() > 0 ? request.getAttribute("breadCrumb").toString() : "";
    String searchPageType = request.getParameter("searchPageType");
           searchPageType = !GenericValidator.isBlankOrNull(searchPageType)?searchPageType:"";
    String paramCollegeName = (String) request.getAttribute("collegeName");
           paramCollegeName = paramCollegeName != null && !paramCollegeName.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(paramCollegeName).trim() : "";         
    String paramCollegeId = (request.getAttribute("collegeId") !=null && !request.getAttribute("collegeId").equals("null") && String.valueOf(request.getAttribute("collegeId")).trim().length()>0 ? String.valueOf(request.getAttribute("collegeId")) : ""); 
    String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");//5_AUG_2014
    String currYear = GlobalConstants.CLEARING_YEAR;
    String nextYear = GlobalConstants.NEXT_YEAR;
    String courseHeader = (String)request.getAttribute("courseHeader") != null ? request.getAttribute("courseHeader").toString() : "";
           courseHeader = courseHeader.replaceAll("Clearing & Adjustment", "");  
    String qString = (String)request.getAttribute("queryStr");
           qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";  
    String matchbrowseQueryString = "";  //30_JUN_2015_REL        
    String searchClearing = (String)request.getAttribute("searchClearing");
    if(("TRUE").equals(searchClearing)){
       matchbrowseQueryString = qString.replace("clearing&","");
       matchbrowseQueryString =  !GenericValidator.isBlankOrNull(matchbrowseQueryString)? ("?nd"+matchbrowseQueryString):"";
    }
    String collegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("collegeNameDisplay") : "";
    //
    String collegeLocation = (String) request.getAttribute("collegeLocation");
    collegeLocation = collegeLocation != null && !collegeLocation.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(collegeLocation).trim() : "";      
    String courseExistsLink = (String)request.getAttribute("courseExistsLink") != null ? request.getAttribute("courseExistsLink").toString() : "";
           courseExistsLink = courseExistsLink.replaceAll("clearing&", "");  
    String entryReqFilter = request.getAttribute("entryReqFilter") != null ? (String)request.getAttribute("entryReqFilter") : "";
    String link = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();
%>
<div class="brcb_lft fl">
                      <div class="bcrb_wrp"> 
                      <%if (bc_breadCrumb != null && bc_breadCrumb.trim().length() > 0){%>
             <%=bc_breadCrumb%>
<%} %>
</div>
<div class="opuni_srch fl_w100">
  <div class="sruni_cnr fl_w100">
    <c:if test="${not empty requestScope.listOfCollegeDetails}">
      <c:forEach var="listOfCollegeDetails" items="${requestScope.listOfCollegeDetails}">
        <div class="unilgo_sec">  
          <div class="lgo_univ fl">
            <c:if test="${not empty listOfCollegeDetails.collegeLogo}">
              <a href='<c:out value="${listOfCollegeDetails.uniHomeURL}" escapeXml="false"/>?clearing'>
                <img width="140px" alt='<c:out value="${listOfCollegeDetails.collegeNameDisplay}" escapeXml="false"/>' src="<%=CommonUtil.getImgPath("",0)%>${listOfCollegeDetails.collegeLogo}"/>
              </a>
            </c:if>
            <c:if test="${empty listOfCollegeDetails.collegeLogo}">
              <a href='<c:out value="${listOfCollegeDetails.uniHomeURL}" escapeXml="false" />'>
                <img width="140px" alt='<c:out value="${listOfCollegeDetails.collegeNameDisplay}" escapeXml="false"/>' src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' />
              </a>
            </c:if>
            </div>
            </div>
            <div class="dec_univ fl">
          <div class="univ_sec fl_w100">
            <div class="lst_univ fl_w100">
              <h3>
                <div class="pruni_hd">
                  <a href='<c:out value="${listOfCollegeDetails.uniHomeURL}" escapeXml="false" />?clearing'>
                    <c:out value="${listOfCollegeDetails.collegeNameDisplay}" escapeXml="false" />
                  </a>
                </div>
              </h3>  
              <div class="crs_tit1"><p><c:out value="${requestScope.courseCount}" escapeXml="false"/> <c:out value="${requestScope.courseHeader}" escapeXml="false"/></p></div>
              <ul class="strat cf">
                <c:if test="${listOfCollegeDetails.collegeStudentRating ne 0}">
                  <li class="mr-15 leag_wrap">
                    <span class="fl mr10">OVERALL RATING
                      <span class="cal"></span>
                    </span>    
                    <span class="rat${listOfCollegeDetails.collegeStudentRating} t_tip">
                      <span class="cmp">
                        <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats"/></div>
                        <div class="line"></div>
                      </span>  
                    </span>
                    <span class="rtx">(${listOfCollegeDetails.actualRating})</span>
                    <a href='<c:out value="${listOfCollegeDetails.uniReviewsURL}" escapeXml="false" />' class="link_blue">
                      <c:if test="${not empty listOfCollegeDetails.reviewCount}">
                        <c:if test="${listOfCollegeDetails.reviewCount ne '0'}">
                          <c:out value="${listOfCollegeDetails.reviewCount}" escapeXml="false"/>
                          <c:if test="${listOfCollegeDetails.reviewCount gt 1 }">reviews</c:if>
                          <c:if test="${listOfCollegeDetails.reviewCount eq 1 }">review</c:if>
                        </c:if>
                      </c:if>
                    </a>
                  </li>
                </c:if>
              </ul>
              <c:if test="${not empty listOfCollegeDetails.nextOpenDay}">
              <%-- <div class="clpr_vopd fl_w100">
                  <a href="<SEO:SEOURL pageTitle="uni-open-days" ><%=paramCollegeName%>#<%=paramCollegeId%>#<%=collegeLocation%></SEO:SEOURL>" title="<%=collegeNameDisplay%> next open day">VIEW ALL OPEN DAYS <i class="fa fa-long-arrow-right"></i></a> 
                </div>
                <div class="cpr_nxtopd fl_w100">
                  <span class="nxtopd">NEXT OPEN DAY: </span>
                  <span class="nxtopd_dte"><i class="fa fa-calendar" aria-hidden="true"></i>
                    ${listOfCollegeDetails.opendateDate}&nbsp;${listOfCollegeDetails.opendateMonth}
                  </span>
                </div> --%>
              </c:if>
            
            </div>
          </div>
        </div>  
      </c:forEach>
    </c:if>
</div>
</div>
</div>
<div class="lst_clbtn fr">
    <div class="fl_lr fl">
      <a href="#">
        <span class="left"><i class="fa fa-filter"></i></span>
        <span class="right">Filter results</span>
      </a>
       </div>
</div>