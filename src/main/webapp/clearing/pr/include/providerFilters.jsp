<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants"%>

<%
  String modDefaultText = "e.g. Module name";
  String moduleDefaultText = "e.g. Module name";
  String q = GenericValidator.isBlankOrNull(request.getParameter("q")) ? "" : (request.getParameter("q"));
  String subject = GenericValidator.isBlankOrNull(request.getParameter("subject")) ? "" : (request.getParameter("subject"));
  String university = GenericValidator.isBlankOrNull(request.getParameter("university")) ? "" : (request.getParameter("university"));
  String assessmentType = GenericValidator.isBlankOrNull(request.getParameter("assessment-type")) ? "" : (request.getParameter("assessment-type"));
  //String module = request.getParameter("module");
   //module = !GenericValidator.isBlankOrNull(module)? module: "";        
  String russellGroup = request.getParameter("russell-group"); 
             russellGroup = !GenericValidator.isBlankOrNull(russellGroup)? russellGroup: "";
  String distance = request.getParameter("distance");
              distance = !GenericValidator.isBlankOrNull(distance)? distance: "10";
  String postCode = request.getParameter("postcode");
              postCode = !GenericValidator.isBlankOrNull(postCode)? postCode:"";
  String empRateMax = request.getParameter("employment-rate-max");
              empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
  String empRateMin = request.getParameter("employment-rate-min");
              empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
  String campusType = request.getParameter("campus-type");
              campusType = !GenericValidator.isBlankOrNull(campusType)? campusType: "";
  String locType = request.getParameter("location-type");
              locType = !GenericValidator.isBlankOrNull(locType)? locType: "";
  String ucasTarrifMax = request.getParameter("ucas-points-max");
              ucasTarrifMax = !GenericValidator.isBlankOrNull(ucasTarrifMax)? ucasTarrifMax: "";
  String ucasTarrifMin = request.getParameter("ucas-points-min");
              ucasTarrifMin = !GenericValidator.isBlankOrNull(ucasTarrifMin)? ucasTarrifMin: "";  
  String smode = (String)request.getParameter("study-mode");
         smode = !GenericValidator.isBlankOrNull(smode)? smode : "";
  String sort = (String)request.getParameter("sort");
         sort = !GenericValidator.isBlankOrNull(sort)? sort : "";
  String jacs = (String)request.getParameter("jacs");//3_Jun_2014
         jacs = !GenericValidator.isBlankOrNull(jacs)? jacs : "";
  String ucasCode = (String)request.getParameter("ucas-code");//3_Jun_2014
         ucasCode = !GenericValidator.isBlankOrNull(ucasCode)? ucasCode : "";
  String srchPrefix = "";
  String srchType = request.getParameter("srchType");
  String scoreValue = !GenericValidator.isBlankOrNull(request.getParameter("score")) ? request.getParameter("score") : "";
  
  
  //String defaultModuleTitle = (String)request.getAttribute("defaultModuleTitle");
         //modDefaultText = !GenericValidator.isBlankOrNull(defaultModuleTitle)? defaultModuleTitle: modDefaultText;  
  String location = GenericValidator.isBlankOrNull(request.getParameter("location")) ? "" : (request.getParameter("location"));

  String entryLevel = GenericValidator.isBlankOrNull(request.getParameter("entry-level"))? "" : (request.getParameter("entry-level"));
  String entryPoints = GenericValidator.isBlankOrNull(request.getParameter("entry-points"))? "" : (request.getParameter("entry-points"));
  //String disbledText = !GenericValidator.isBlankOrNull(defaultModuleTitle)? "":"disabled";
  String qString = (String)request.getAttribute("queryStr");
         qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";  
  String noFollowLink = (!GenericValidator.isBlankOrNull(qString)) ? "nofollow":"";
  String subjectCode = (String)request.getAttribute("paramCategoryCode");
              subjectCode = !GenericValidator.isBlankOrNull(subjectCode)? subjectCode.toUpperCase(): "";
  String studyLevel = (String)request.getAttribute("paramStudyLevelId");  
         //studyLevel = !GenericValidator.isBlankOrNull(studyLevel)? studyLevel.toUpperCase() :"";
  String studyModeId = (String)request.getAttribute("paramstudyModeValue");
         studyModeId = !GenericValidator.isBlankOrNull(studyModeId)? studyModeId : "All study modes";
  String seoFirstPageUrl = (String)request.getAttribute("SEO_FIRST_PAGE_URL");
         seoFirstPageUrl = !GenericValidator.isBlankOrNull("seoFirstPageUrl")? seoFirstPageUrl :"";
  
  if(srchType!=null && !"".equals(srchType)){
    if("clearing".equals(srchType)){
      srchPrefix = "clearing-";
    }
  }
  String applyFlag = !GenericValidator.isBlankOrNull(request.getParameter("applyflag")) ? request.getParameter("applyflag") : "";
  String  goApplyFlagFilterValue =  request.getAttribute("goApplyFlagFilterValue") != null ? ((String)request.getAttribute("goApplyFlagFilterValue")).toLowerCase() : "";  
         
 String entryReqFilter = request.getAttribute("entryReqFilter") != null ? (String)request.getAttribute("entryReqFilter") : "";
   String link = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();
   
%>
<div class="fltr lft fl">
  <%--Added close tag for filter in responsive 03_NOV_2015 By Indumathi S--%>
  <div class="filt_cls">
    <a><span>CLOSE</span>
    <img src="<%=new CommonUtil().getImgPath("/wu-cont/images/cmm_close_icon.svg",0)%>" title="close navigation" alt="close navigation"/>
   </a>
  </div>
  <c:if test="${not empty requestScope.userUCASScore and requestScope.userUCASScore ne '0'}">
    <div class="flt_ucscr fl_w100">
      <div class="ucs_rgt fr">
        <div class="uscr_tit">Your UCAS score</div>
        <div class="ucpt">
          <span class="ucspt">${requestScope.userUCASScore} points</span>
          <span class="ucs_edt"><a href="<%=link%>/degrees/wugo/qualifications/search.html?<%=entryReqFilter%>">Edit</a></span>
        </div>
      </div>
    </div>
  </c:if>
  <div class="fltr_in"> 
      <h2>Filter by:</h2>
     <jsp:include page="/clearing/pr/include/deleteProviderFilter.jsp"/>
     <%-- Go apply flag--%>
     <c:if test="${not empty requestScope.whatuniGOApplyFilterFlag}">
     <div class="sh_hd fl_w100">
  <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('coursetype','coursetypeh1');">
  <h4 id="coursetypeh1" class="rdat actct fl"><span class="fl">COURSE TYPE</span><span id="coursetypeh1_span" class="fr togflt_img tog_min"></span></h4></div>
    <div id="coursetype" class="flt_lt fl_w100" style="display: block;">
      <ul class="sub_lik nw loc_typ crse_type">
         <li><input type="radio" id="allCourseFlag" name="allCourses" value=""/><label for="allCourseFlag" onclick="javascript:multicheckApplyFn('allCourseFlag');">All courses</label></li>
         <li class="rdo_goapp"><input type="radio" id="goApplyFlag" name="allCourses" value="y"/>
           <label for="goApplyFlag" class="gaply_sec" onclick="javascript:goApplyLightBox();">
             <span class="fl rdo_aply">
             <span class="go_txt">GO</span><span class="goaply">APPLY</span>
             </span>
             <span class="fl_w100">courses only</span>
           </label>
       <%--  <span class="hlp tool_tip" onclick="goApplyLightBox();"><i class="fa fa-question-circle"></i>  
         </span> --%>
        </li>
        </ul>
        </div>
        </div>
     </c:if>  
    
  <c:if test="${not empty requestScope.subjectList}">
       <div class="sh_hd fl_w100">
          <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('subject','subjecth1');">
            <h4 id="subjecth1" class="rdat actct fl"><span class="fl">SUBJECT</span><span id="subjecth1_span" class="fr togflt_img tog_min"></span></h4>
          </div>
          <div id="subject" class="flt_lt sub_cnt fl_w100" style="display: block;">
            <ul class="sub_lik " id="subject_ul">
            <c:forEach var="subjectList" items="${requestScope.subjectList}" varStatus="row">
            <c:set var="rowValue" value="${row.index}"/>
            <c:set var="subjectDesc" value="${subjectList.categoryDesc}"/>
            <%
                 String displayNoneOrBlock = "";
                 if((Integer.parseInt(pageContext.getAttribute("rowValue").toString())) == 10 ){%>
                   <li id="subjectmore_li" class="stud"><a onclick="showLiForSubject();" class="u_txtmr fl"><span class="u_txt fl">More</span><span class="mr_plus fl"><img  alt="plus icon" src="<%=CommonUtil.getImgPath("/wu-cont/images/tog_plus.svg",0)%>"></span></a></li>
                 <%}%>
                 <%if((Integer.parseInt(pageContext.getAttribute("rowValue").toString())) >= 10  ){
                   displayNoneOrBlock = "style='display:none;'";
                }%>
            <c:set var="subjectCode" value="<%=subjectCode%>"/>
            <c:if test="${subjectList.categoryCode eq subjectCode}">
                   <li class="stud" <%=displayNoneOrBlock%>><span class="u_txt lcn-act1"><span class="act_bg"></span>${subjectList.categoryDesc}</span><span class="sub_cunt fr">(${subjectList.collegeCount})</span></li>
                 </c:if>
                 <c:if test="${subjectList.categoryCode ne subjectCode}">
                 <c:set var="subjectDesc" value="${subjectList.categoryDesc}"/>
                  <li class="stud" <%=displayNoneOrBlock%>>                    
                    <span class="u_txt">
                      <a rel="<%=noFollowLink%>" onclick="searchEventTracking('pr-filter','<%=srchPrefix%>subject','${subjectDesc.toLowerCase()}');"  href="${subjectList.browseMoneyPageUrl}">${subjectList.categoryDesc} </a>                   
                    </span>
                    <span class="sub_cunt fr">(${subjectList.collegeCount})</span>
                  </li>
                 </c:if>                  
             </c:forEach>
            </ul>
          </div>
       </div>
     </c:if>
     <c:if test="${not empty requestScope.qualList}">
          <div class="sh_hd fl_w100">
            <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('qualList','qualListh1');">
              <h4 id="qualListh1" class="rdat actct fl"><span class="fl">Study level</span><span id="qualListh1_span" class="fr togflt_img tog_plus"></span></h4>
            </div>
            <div id="qualList" class="flt_lt fl_w100" style="display: none;">
              <ul class="sub_lik"> 
              <c:forEach var="qualList" items="${requestScope.qualList}">
                <c:if test="${qualList.refineCode eq requestScope.paramStudyLevelId}">
                      <li class="stud fl-act"><span class="act_bg"></span>${qualList.refineDisplayDesc}</li>
                   </c:if>
                   <c:if test="${qualList.refineCode ne requestScope.paramStudyLevelId}">
                     <li>
                       <span class="u_txt">                
                         <c:set var="qualDesc" value="${qualList.refineDesc}"/> 
                         <a onclick="searchEventTracking('pr-filter','<%=srchPrefix%>study-level','${qualDesc.toLowerCase()}');" href="${qualList.browseMoneyageLocationRefineUrl}" >${qualList.refineDisplayDesc}</a>
                         </span>
                     </li>
                   </c:if>
                 </c:forEach>
               </ul>             
             </div>
          </div>
     </c:if>
     <c:if test="${not empty requestScope.studyModeList}">
         <div class="sh_hd fl_w100">
            <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('studyMode','studyModeh1');">
              <h4 id="studyModeh1" class="rdat actct fl">Study mode <span id="studyModeh1_span" class="fr togflt_img tog_min"></span></h4>
            </div>
            <div id="studyMode" class="flt_lt fl_w100" style="display:block;">
              <ul class="sub_lik">
              <c:forEach var="studyModeList" items="${requestScope.studyModeList}">
                <c:set var="studyModeId" value="<%=studyModeId%>"/>
                <c:if test="${studyModeList.refineDesc eq studyModeId}">  
                    <li class="stud fl-act"><span class="act_bg"></span>${studyModeList.refineDesc}</li>
                   </c:if>
                    <c:if test="${studyModeList.refineDesc ne studyModeId}">              
                    <li>
                      <span class="u_txt">
                      <c:set var="studyModeDesc" value="${studyModeList.refineDesc}"/>
                      <a rel="nofollow" onclick="searchEventTracking('pr-filter','<%=srchPrefix%>study-mode','${studyModeDesc.toLowerCase()}');" href="${studyModeList.filterURL}">${studyModeList.refineDesc}</a>
                      </span>                
                    </li>
                  </c:if>
                </c:forEach>   
              </ul>             
            </div>
         </div>
     </c:if>
     <%--Added below filter Assessment, 28_Aug_2018 By Sabapathi.S--%>
     <%--
     <logic:present name="assessmentList" scope="request">
       <logic:notEmpty name="assessmentList" scope="request">
         <div class="sh_hd">
            <div class="hdn" onclick="javascript:toggleMenu('assessment','assessmenth1');">
              <h4 id="assessmenth1" class="rdat actct">How you're assessed </h4>
            </div>
            <div id="assessment" class="flt_lt" style="display:block;">
              <ul class="sub_lik">   
                <logic:iterate id="assessmentList" name="assessmentList" scope="request">
                  <logic:equal name="assessmentList" property="urlText" value="<%=assessmentType%>">
                    <p class="stud fl-act">
                      <span class="act_bg"></span>
                      <bean:write name="assessmentList" property="assessmentDisplayName" />
                    </p>
                   </logic:equal>
                   <logic:notEqual name="assessmentList" property="urlText" value="<%=assessmentType%>">                      
                    <li>                
                      <bean:define id="assessmentTypeName" name="assessmentList" property="assessmentDisplayName" type="java.lang.String"/>
                      <a rel="nofollow" onclick="searchEventTracking('pr-filter','<%=srchPrefix%>assessment-type','<%=assessmentTypeName.toLowerCase()%>');" href="<bean:write name="assessmentList" property="browseMoneyAssessedRefineUrl" />"><bean:write name="assessmentList" property="assessmentDisplayName" /></a>
                    </li>
                  </logic:notEqual>
                </logic:iterate>
              </ul>             
            </div>
         </div>
        </logic:notEmpty>
     </logic:present>
     --%>
     
      <form:form action="/courses/*/*/*/*/*/csearch" commandName="searchBean" >
        <input type="hidden" name="hidden_q" id="hidden_q" value="<%=q%>" />
        <input type="hidden" name="hidden_subject" id="hidden_subject" value="<%=subject%>" />
        <input type="hidden" name="hidden_university" id="hidden_university" value="<%=university%>" />
        <input type="hidden" name="hidden_russell" id="hidden_russell" value="<%=russellGroup%>" />
        <input type="hidden" name="hidden_dist" id="hidden_dist" value="<%=distance%>" />
        <input type="hidden" name="hidden_postCode" id="hidden_postCode" value="<%=postCode%>" />
        <input type="hidden" name="hidden_empRateMax" id="hidden_empRateMax" value="<%=empRateMax%>" />
        <input type="hidden" name="hidden_empRateMin" id="hidden_empRateMin" value="<%=empRateMin%>" />
        <input type="hidden" name="hidden_location" id="hidden_location" value="<%=location%>" />
        <input type="hidden" name="hidden_locationType" id="hidden_locationType" value="<%=locType%>" />
        <input type="hidden" name="hidden_campusType" id="hidden_campusType" value="<%=campusType%>" />
        <input type="hidden" name="hidden_ucasTarrif_max" id="hidden_ucasTarrif_max" value="<%=ucasTarrifMax%>" />
        <input type="hidden" name="hidden_ucasTarrif_min" id="hidden_ucasTarrif_min" value="<%=ucasTarrifMin%>" />        
        <input type="hidden" name="hidden_queryString" id="hidden_queryString" value="<%=qString%>" />
        <input type="hidden" name="sortingUrl" id="sortingUrl" value="<%=seoFirstPageUrl%>" />
        <input type="hidden" name="hidden_modDefaultText" id="hidden_modDefaultText" value="<%=modDefaultText%>" />
        <input type="hidden" name="hidden_studyMode" id="hidden_studyMode" value="<%=smode%>" />
        <input type="hidden" name="hidden_sortorder" id="hidden_sortorder" value="<%=sort%>" />
        <input type="hidden" name="hidden_jacs" id="hidden_jacs" value="<%=jacs%>" />
        <input type="hidden" name="hidden_ucasCode" id="hidden_ucasCode" value="<%=ucasCode%>" />
        <input type="hidden" name="hidden_entryLevel" id="hidden_entryLevel" value="<%=entryLevel%>" />
        <input type="hidden" name="hidden_entryPoints" id="hidden_entryPoints" value="<%=entryPoints%>" />
        <input type="hidden" name="hidden_assessment" id="hidden_assessment" value="<%=assessmentType%>" />
        <input type="hidden" name="hidden_score" id="hidden_score" value="<%=scoreValue%>"/>
        <input type="hidden" name="hidden_applyFlag" id="hidden_applyFlag" value="<%=goApplyFlagFilterValue%>" />
        <input type="hidden" name="hidden_appFlag" id="hidden_appFlag" value="<%=goApplyFlagFilterValue%>" />
      </form:form>
  </div>
</div>
<script type="text/javascript">
defaultApplyCheckBox("hidden_appFlag");
</script>