<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil, java.util.HashMap,WUI.utilities.CommonFunction, com.wuni.util.seo.SeoUrls"%>

<%
 String locationName = (String)request.getAttribute("location");  
        locationName = !GenericValidator.isBlankOrNull(locationName) ? locationName.trim() :"";   
 String parentCategoryDesc = (String)request.getAttribute("parentCategoryDesc");  
        parentCategoryDesc = !GenericValidator.isBlankOrNull(parentCategoryDesc) ? parentCategoryDesc.trim() :""; 
 String q = GenericValidator.isBlankOrNull(request.getParameter("q")) ? "" : (request.getParameter("q"));
 String subject = GenericValidator.isBlankOrNull(request.getParameter("subject")) ? "" : (request.getParameter("subject"));
 String university = GenericValidator.isBlankOrNull(request.getParameter("university")) ? "" : (request.getParameter("university"));
 String russellGroup = request.getParameter("russell-group"); 
             russellGroup = !GenericValidator.isBlankOrNull(russellGroup)? russellGroup: "";
  String queryStringValue = (String)request.getAttribute("queryStr");   
         queryStringValue = !GenericValidator.isBlankOrNull(queryStringValue) ? queryStringValue.trim() :"";   
  String module = request.getParameter("module");
              module = !GenericValidator.isBlankOrNull(module)? new CommonFunction().replaceHypenWithSpace(module): "";
  String distance = request.getParameter("distance");
              distance = !GenericValidator.isBlankOrNull(distance)? distance: "";
  String postCode = request.getParameter("postcode");
              postCode = !GenericValidator.isBlankOrNull(postCode)? postCode:"";
  String empRateMax = request.getParameter("employment-rate-max");
              empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
  String empRateMin = request.getParameter("employment-rate-min");
              empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
  String campusType = request.getParameter("campus-type");
              campusType = !GenericValidator.isBlankOrNull(campusType)? campusType: "";
  String locType = request.getParameter("location-type");
              locType = !GenericValidator.isBlankOrNull(locType)? locType: "";
  String ucasTarrifMax = request.getParameter("ucas-points-max");
              ucasTarrifMax = !GenericValidator.isBlankOrNull(ucasTarrifMax)? ucasTarrifMax: "";
  String ucasTarrifMin = request.getParameter("ucas-points-min");
              ucasTarrifMin = !GenericValidator.isBlankOrNull(ucasTarrifMin)? ucasTarrifMin: "";
   
   String prefType = request.getParameter("your-pref");
              prefType = !GenericValidator.isBlankOrNull(prefType)? prefType: "";
   
   String applyFlag = request.getParameter("applyflag");
          applyFlag = !GenericValidator.isBlankOrNull(applyFlag)? applyFlag: "";
          
   String assessedType = (String)request.getAttribute("assessmentTypeDisplay");
              assessedType = !GenericValidator.isBlankOrNull(assessedType)? assessedType: "";

  String studyMode = (String)request.getAttribute("paramstudyModeDesc");  
        studyMode = !GenericValidator.isBlankOrNull(studyMode)? studyMode :"";
    
   String studyLevel = (String)request.getAttribute("paramStudyLevelId");  
        studyLevel = !GenericValidator.isBlankOrNull(studyLevel)? studyLevel :"";  
  String collegeId =  (String)request.getAttribute("collegeId");  
        collegeId = !GenericValidator.isBlankOrNull(collegeId)? collegeId :"";       
  String studyLevelName = (String)request.getAttribute("studyLevelName");  
         studyLevelName = !GenericValidator.isBlankOrNull(studyLevelName)? studyLevelName :""; 
  String universityName = (String)request.getAttribute("collegeName");  
         universityName = !GenericValidator.isBlankOrNull(universityName)? universityName :"";
         universityName = university;
  String subjectText = (String)request.getAttribute("subjectText");  
         subjectText = !GenericValidator.isBlankOrNull(subjectText)? subjectText :"";
  String searchText = (String)request.getAttribute("searchText");
        if(!GenericValidator.isBlankOrNull(searchText) && searchText.contains("UCAS code")){
         subjectText = !GenericValidator.isBlankOrNull(searchText)? (searchText.replaceAll("UCAS code ", "").replaceAll("-", " ")) : subjectText;
         }
  String parentCatId = (String)request.getAttribute("parentCatId") != null ? (String)request.getAttribute("parentCatId") : "";
  String parentCat = (String)request.getAttribute("parentCatDesc") != null ? (String)request.getAttribute("parentCatDesc") : "";
  String entryLevel = (String)request.getAttribute("entryLevelValue");
  String filter = "N";
  String seoFirstPageUrl = (String)request.getAttribute("SEO_FIRST_PAGE_URL");
         seoFirstPageUrl = !GenericValidator.isBlankOrNull("seoFirstPageUrl")? seoFirstPageUrl :"";   
  String smode = (String)request.getParameter("smode");
         smode = !GenericValidator.isBlankOrNull(smode)? smode : "";
  String sort = (String)request.getParameter("sort");
         sort = !GenericValidator.isBlankOrNull(sort)? sort : "";
 String collegeNameDisplay = (String)request.getAttribute("collegeNameDisplay");
        collegeNameDisplay = !GenericValidator.isBlankOrNull(collegeNameDisplay)? collegeNameDisplay : "";
        
 String SearchUrl = (String)session.getAttribute("FaqURL");    
 
 String providerDeleteFilter = "";
 
 String paramCategoryCode = (String)request.getAttribute("paramCategoryCode");
        paramCategoryCode = !GenericValidator.isBlankOrNull(paramCategoryCode)? paramCategoryCode : "";
        
 String searchJourneyType = (String)request.getAttribute("searchJourneyType");
        searchJourneyType = !GenericValidator.isBlankOrNull(searchJourneyType)? searchJourneyType : "";//24_JUN_2014
 
 //TODO check
 if(subjectText!=null && !("").equals(subjectText)){
    if(!("").equals(studyLevel)){
       if(GenericValidator.isBlankOrNull(searchJourneyType)){//24_JUN_2014
          providerDeleteFilter = new SeoUrls().SearchURLfromProviderResults(subjectText, paramCategoryCode, locationName, studyLevelName,studyLevel,"/page.html");
      }else{
          providerDeleteFilter = new SeoUrls().SearchURLfromProviderResults(subjectText, paramCategoryCode, locationName, studyLevelName,studyLevel,"/page.html?nd&clearing");//30_JUN_2015_REL
      }
    }else{
        providerDeleteFilter =  new SeoUrls().getBrowseCategoryHomeUrl("M");
    }
 }else{
     if(!("").equals(studyLevel)){
     providerDeleteFilter =  new SeoUrls().getBrowseCategoryHomeUrl(studyLevel);
   }else{
      providerDeleteFilter =  new SeoUrls().getBrowseCategoryHomeUrl("M");
   } 
 }
 String jacsCode = (String)request.getParameter("jacs");//3_Jun_2014
        jacsCode = !GenericValidator.isBlankOrNull(jacsCode)? jacsCode : "";
 String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");//5_AUG_2014
 String qString = (String)request.getAttribute("queryStr");
           qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
 String matchbrowseQueryString = ""; ////30_JUN_2015_REL         
    String searchClearing = (String)request.getAttribute("searchClearing");
    if(("TRUE").equals(searchClearing)){
       matchbrowseQueryString = qString.replace("?clearing","");
       matchbrowseQueryString =  !GenericValidator.isBlankOrNull(matchbrowseQueryString)? ("?"+matchbrowseQueryString):"";
    }
    String courseExistsLink = "";
    if("ON".equalsIgnoreCase(clearingonoff)){      
      courseExistsLink = (String)request.getAttribute("courseExistsLink") != null ? request.getAttribute("courseExistsLink").toString() : "";
           courseExistsLink = courseExistsLink.replace("?clearing&", "?");
    }
    
    String ucasEntryPoints = (String)request.getAttribute("entryPoints");
    ucasEntryPoints = !GenericValidator.isBlankOrNull(ucasEntryPoints) &&  ucasEntryPoints.indexOf(",") > -1 ? ucasEntryPoints.replaceAll(",", " - ") : "";
%>
<div class="sh_hd pad_btm fl_w100">
  <div class="ns_flr" id="filterTop" style = "display:none;">
    <div id ="filterblock" style = "display:none;">
      <h4 id="Filterh1">Filters applied</h4>
    </div>
    <div id="Filter" class="flt_lt" style="display:none;">
     <%if("ON".equalsIgnoreCase(clearingonoff)){
         if(!GenericValidator.isBlankOrNull(searchJourneyType) && ("CLEARING").equals(searchJourneyType)){%>
         <c:if test="${not empty requestScope.courseExistsLink}">
              <%filter = "Y";%>
              <div class="frst ch1"> 
               <a class="tp_box mb10 red" href="<%=courseExistsLink%>"> 
                 <span> Clearing</span>
               </a>
              </div>
           </c:if>
        <%}}%>
        <%--Modified the delete filter functions based on new URL pattern Jan-27-16 Indumathi.S--%>
        <c:if test="${not empty requestScope.entryLevelValue}">
           <%filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="removeGradesFilter();">              
                <span> UCAS score range <span> <%=ucasEntryPoints%> </span></span>              
          </a></div>
        </c:if>
        <c:if test="${not empty requestScope.subjectText}">
           <%filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteProviderFilterParams('subject','');">              
                <span><%=subjectText%></span>              
          </a></div>
        </c:if>
         <c:if test="${empty requestScope.subjectText}">    
           <%
            if(!"".equals(subjectText)){
           filter = "Y";
           %>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteProviderFilterParams('subject','<%=parentCategoryDesc%>');">              
                <span><%=subjectText%></span>              
          </a></div>
          <%}%>
        </c:if>
        <%if(providerDeleteFilter!="" && providerDeleteFilter!=null){  filter = "Y";%>
          <div class="frst ch1"> 
            <a class="tp_box mb10" onclick="deleteProviderUniParams();">              
                <span><%=collegeNameDisplay%></span>              
            </a>
          </div> 
          <%}%>
              
      <%-- <%if(russellGroup!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteProviderFilterParams('russell');"><span>Russell Group</span></a></div>       
       <%}%>
       <%if(campusType!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteProviderFilterParams('campus');"><span>Campus type</span></a></div>       
       <%}%> --%>
       <%if(!((empRateMax == "" || empRateMax == "100"  ) &&  (empRateMin == "0" || empRateMin == "" ))){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteProviderFilterParams('empRate');"><span>Employment Rate</span></a></div>       
       <%}%>
       
       <%if(!((ucasTarrifMax == "" || ucasTarrifMax == "480"  ) &&  (ucasTarrifMin == "0" || ucasTarrifMin == "" ))){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteProviderFilterParams('ucasTariff');"><span>Ucas Tariff</span></a></div>       
       <%}%>
        <%if(module!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteProviderFilterParams('module');"><span>Modules: <%=module%></span></a></div>       
       <%}%>
       
       <%if(studyMode!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteProviderFilterParams('studyMode')"><span><%=studyMode%></span></a></div>       
       <%}%>
       
       <%if(assessedType!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteProviderFilterParams('assessed')"><span><%=assessedType%></span></a></div>       
       <%}%>
       
        <%if(postCode!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteProviderFilterParams('postCode');"><span><%=distance%> miles from <%=postCode%></span></a></div>       
       <%}%>
     <%if(applyFlag!=""){filter = "Y"; String applyFilter = ""; if("Y".equalsIgnoreCase(applyFlag)) { applyFilter = "Yes"; }%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('applyFlag','<%=applyFlag%>');"><span>GO apply</span><span><%=applyFilter%></span></a></div>       
       <%}%>
       <%--End--%>
       <%--
       <%
         if(prefType!=""){
          String prefTypeDesc = "";
          String prefTypeArr[] = prefType.split(",");
          String preffType = "";
          String filetvalue = "";
          HashMap locationMap = (HashMap)request.getAttribute("reviewCatMap");
          for(int i = 0; i<prefTypeArr.length; i++ ){ 
           filetvalue = "";
            preffType = (String)locationMap.get(prefTypeArr[i]); 
              for(int j = 0; j<prefTypeArr.length; j++){
                if(prefTypeArr[i]!=prefTypeArr[j]){
                  if(filetvalue!=""){
                    filetvalue = filetvalue + "," + prefTypeArr[j];
                  }else{
                    filetvalue = prefTypeArr[j];
                  }
               }
              }
          %>              
         <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('pref-type', '<%=filetvalue%>');"><span><%=preffType%></span></a></div>
       <%filter = "Y";}}%>
       --%>
       
    </div>
    <input type="hidden" id="filterDiv" name="filterDiv" value="<%=filter%>"/>
    <input type="hidden" id="queryStringVal" name="queryStringVal" value="<%=queryStringValue%>"/>
    <input type="hidden" name="sortingUrl" id="sortingUrl" value="<%=seoFirstPageUrl%>" />
    <input type="hidden" name="collegeId" id="collegeId" value="<%=collegeId%>" />     
  </div>
</div>

<script type="text/javascript">
showFilterBlock();

</script>

 
 