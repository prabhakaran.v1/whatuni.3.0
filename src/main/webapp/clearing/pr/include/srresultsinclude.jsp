<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.SessionData,org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.GlobalFunction" %>
<%  
    CommonFunction common = new CommonFunction();
    String collegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("collegeNameDisplay") : "";
    //String studyLevelDesc = (String)request.getAttribute("studyLevelDesc");
    String paramCollegeName = (String) request.getAttribute("collegeName");
    paramCollegeName = paramCollegeName != null && !paramCollegeName.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(paramCollegeName).trim() : "";      
    String pros_search_name = request.getAttribute("prospectus_search_name") != null ? request.getAttribute("prospectus_search_name").toString() : "";
    String searchPageType = request.getParameter("searchPageType");
           searchPageType = !GenericValidator.isBlankOrNull(searchPageType)?searchPageType:"";
    String clrText = "";
    String clbtnclassname = "";
    if("clearing".equals(searchPageType)){clrText= "clearing-"; clbtnclassname ="clr_btns";}
    String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");
    String searchhits = request.getParameter("searchhits");
           searchhits = !GenericValidator.isBlankOrNull(searchhits)?searchhits:"";
    String advertiserFlag = (String)request.getAttribute("advertiserFlag");
           advertiserFlag = GenericValidator.isBlankOrNull(advertiserFlag)?"": advertiserFlag.equalsIgnoreCase("y") ? "y" : "n"; 
           String assignCollegeLogo = ""; 
           
   String pageno = request.getAttribute("pageno") != null ? (String) request.getAttribute("pageno") : "1";        
   int courseCount = Integer.parseInt(String.valueOf(request.getAttribute("courseCount")));        
   request.setAttribute("isClearingProfile", "YES");
            
%>
<div class="srlst fl_w100">  
  <div class="sruni_cnr fl_w100">
    <div class="dec_univ fl">
      <div class="univ_sec fl_w100">
      <c:if test="${not empty requestScope.courseList}">
        <div class="slst_cnt fl_w100">
          <c:forEach items="${requestScope.courseList}" var="courseList" varStatus="row">
            <c:set value="${courseList.collegeId}" var="collegeIdDef"/>
            <c:set value="${courseList.collegeName}" var="collegeNameDef"/>
            <c:set var="rowValue" value="${row.index}"/>
            <%String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(pageContext.getAttribute("collegeNameDef").toString());%>
            <div class="blu_cnt fl_w100">
              <div class="sub_lst fl">
                <h4><a href="${courseList.courseDetailUrl}">${courseList.courseTitle}</a></h4>
                <c:if test="${not empty courseList.departmentOrFaculty}"><h5>${courseList.departmentOrFaculty}</h5></c:if>
                <div class="ucas_fld">
                  <c:if test="${not empty courseList.entryRquirements}"><span class="ucas_pt"><c:out value="${courseList.entryRquirements}" escapeXml="false" /></span></c:if>
                  <c:if test="${not empty courseList.showApplyNowButton and courseList.showApplyNowButton eq 'Y'}">
                    <ul class="strat ucsPotsCnt cf fl_w100">
                      <li class="mr-15 leag_wrap">        
                        <span class="t_tip">
                          <span class="UcsPots">UCAS Score seems too high?</span>                        
                          <span class="cmp">
                            <div class="hdf5">The UCAS points for this course might be higher than what you have achieved, but we think it might still be possible for you to enrol on this course, so we'd recommend you call the university if you're interested in this course</div>
                            <div class="line"></div>
                          </span>
                        </span>
                      </li>
                    </ul>
                  </c:if>
                </div>
                <div class="subtn_cnt fl_w100">
                  <div class="vw_crsbtn fl">
                    <a href="${courseList.courseDetailUrl}">VIEW COURSE <i class="fa fa-long-arrow-right"></i></a>
                  </div>
                </div>
              </div>
              <c:if test="${not empty courseList.subOrderItemId and courseList.subOrderItemId gt 0}">
                <c:set value="${courseList.subOrderItemId}" var="subOrderItemId"/>
                <div class="subtn_cnt fr">
                  <c:if test="${not empty courseList.showApplyNowButton and courseList.showApplyNowButton eq 'Y'}">
                    <div class="go_aplybtn fl_w100">
                      <span class="hlp tool_tip"><i class="fa fa-question-circle"></i>
                        <span class="cmp hlp-tt">
                          <div class="hdf5"></div>
                          <div>Apply for this course and get an instant decision online right now.</div> 
                          <div class="line"></div>
                        </span>
                      </span>
                      <a title="GO APPLY ONLINE"
                         onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Apply Now', '<%=gaCollegeName%>');SRApplyNow('<%=pageContext.getAttribute("collegeIdDef").toString()%>', '${courseList.courseId}', '${courseList.opportunityId}', '${courseList.subOrderItemId}');" 
                         class="fl_w100"><span class="go">GO</span>APPLY ONLINE</a>
                    </div>
                  </c:if>
                  <div class="lst_clbtn fl_w100">
                    <div class="btns_interaction fl_w100 clr_btn">
                      <c:if test="${not empty courseList.hotLineNo}">
                        <a class="clr-call fl_w100"
                           id="contact_<%=pageContext.getAttribute("rowValue").toString()%>"
                           onclick="GAInteractionEventTracking('Webclick', 'interaction', 'hotline','<%=gaCollegeName%>');cpeHotlineClearing(this,'<%=pageContext.getAttribute("collegeIdDef").toString()%>','<c:out value="${courseList.subOrderItemId}" escapeXml="false" />','${courseList.networkId}','${courseList.hotLineNo}');changeContactButton('${courseList.hotLineNo}', '${courseList.networkId}', '<%=gaCollegeName%>', '${courseList.subOrderItemId}', 'contact_<%=pageContext.getAttribute("rowValue").toString()%>', '<%=pageContext.getAttribute("collegeIdDef").toString()%>');"
                           title="CALL NOW"><i class="fa fa-phone" aria-hidden="true"></i> CALL NOW</a>
                      </c:if>
                      <c:if test="${not empty courseList.subOrderWebsite}">
                        <a rel="nofollow"
                           target="_blank"
                           onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', '${courseList.websitePrice}');cpeWebClickClearingWithCourse(this,'<%=pageContext.getAttribute("collegeIdDef").toString()%>', '${courseList.courseId}', '${courseList.subOrderItemId}','${courseList.networkId}','${courseList.subOrderWebsite}');" 
                           href="${courseList.subOrderWebsite}" 
                           class="visit-web fl_w100" 
                           title="VISIT WEBSITE">Visit website</a>
                      </c:if>
                    </div>
                  </div>
                </div>
              </c:if>
            </div>
            <%if(((Integer.parseInt(pageContext.getAttribute("rowValue").toString())) == 1 && "1".equalsIgnoreCase(pageno)) || (courseCount == 1)){%>
              <c:if test="${requestScope.wugoFlag eq 'ON'}">
                <jsp:include page="/clearing/watchvideo/showWatchVideoButton.jsp"/>
              </c:if>
            <%}%>
            <jsp:include page="/jsp/search/include/searchDrawAdSlots.jsp"> 
              <jsp:param name="fromPage" value="clearingprovidersearchresults"/>
              <jsp:param name="position" value="${rowValue}"/>
              <jsp:param name="totalResults" value="<%=searchhits%>"/>
              <jsp:param name="showDesktop" value="false"/>
              <jsp:param name="showBanner" value="<%=advertiserFlag%>"/>
            </jsp:include> 
          </c:forEach>
        </div>
      </c:if>
    </div>
  </div>
</div>