<%@ page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil"%>
<%  
  String qString = (String)request.getAttribute("queryStr");
         qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  String pagenochar = null;
  CommonUtil util = new CommonUtil();
  String module = request.getParameter("module");
              module = !GenericValidator.isBlankOrNull(module)? module: "";         
  String russellGroup = request.getParameter("russell-group"); 
             russellGroup = !GenericValidator.isBlankOrNull(russellGroup)? russellGroup: "";
  String distance = request.getParameter("distance");
              distance = !GenericValidator.isBlankOrNull(distance)? distance: "";
  String postCode = request.getParameter("postcode");
              postCode = !GenericValidator.isBlankOrNull(postCode)? postCode:"";
  String empRateMax = request.getParameter("employment-rate-max");
              empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
  String empRateMin = request.getParameter("employment-rate-min");
              empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
  String campusType = request.getParameter("campus-type");
              campusType = !GenericValidator.isBlankOrNull(campusType)? campusType: "";
  String locType = request.getParameter("location-type");
              locType = !GenericValidator.isBlankOrNull(locType)? locType: "";
  String ucasTarrifMax = request.getParameter("ucas-points-max");
              ucasTarrifMax = !GenericValidator.isBlankOrNull(ucasTarrifMax)? ucasTarrifMax: "";
  String ucasTarrifMin = request.getParameter("ucas-points-min");
              ucasTarrifMin = !GenericValidator.isBlankOrNull(ucasTarrifMin)? ucasTarrifMin: "";
  String smode = (String)request.getParameter("study-mode");
         smode = !GenericValidator.isBlankOrNull(smode)? smode : "";
  String seoFirstPageUrl = (String)request.getAttribute("SEO_FIRST_PAGE_URL");
         seoFirstPageUrl = !GenericValidator.isBlankOrNull("seoFirstPageUrl")? seoFirstPageUrl :"";
         seoFirstPageUrl = !GenericValidator.isBlankOrNull(seoFirstPageUrl) && (seoFirstPageUrl.lastIndexOf("&sort=") == -1) ? seoFirstPageUrl.trim()+"&sort=r" : seoFirstPageUrl.trim();
  String searchWhat = ((String)request.getAttribute("searchWhat"));
  searchWhat = !GenericValidator.isBlankOrNull("searchWhat")? searchWhat :"";
  String sortParam = !GenericValidator.isBlankOrNull(request.getParameter("sort"))? request.getParameter("sort") :"r";    
  String sortURL = seoFirstPageUrl;
  
  String studyLevelDesc = (String)request.getAttribute("studyLevelDesc");
  studyLevelDesc = !GenericValidator.isBlankOrNull(studyLevelDesc) ? studyLevelDesc : "";  
  String jacsCode = (String)request.getParameter("jacs");//3_JUN_2014
         jacsCode = !GenericValidator.isBlankOrNull(jacsCode)? jacsCode : "";  
  String clearingPage =  (String)request.getAttribute("searchClearing");
       clearingPage = GenericValidator.isBlankOrNull(clearingPage)?"":clearingPage;     
  String pageNumberParameter = "pageno";
  if(qString.contains(pageNumberParameter)){//Added this for pagination removal in URL by Hema.S on 12_FEB_2019_REL
    pagenochar= qString.substring(qString.indexOf(pageNumberParameter)-1,qString.indexOf(pageNumberParameter)+("pageno=".length()+request.getParameter(pageNumberParameter).length()));
  }
  String finalsortURL = "";
  String newSortParam = "";
  String sortBy = (String)request.getAttribute("sortByValue");
  String sortByValue = "&sort="+sortParam;
  if("A".equalsIgnoreCase(searchWhat) && "r".equalsIgnoreCase(sortParam)){
    sortParam = "ta";
  }
  String basketCount = request.getSession().getAttribute("basketpodcollegecount") != null ? String.valueOf(request.getSession().getAttribute("basketpodcollegecount")) : "0";   
  String srchPrefix = "";
   String srchType = request.getParameter("srchType");
   if(srchType!=null && !"".equals(srchType)){
     if("clearing".equals(srchType)){
      srchPrefix = "clearing-";
     }
   }
String  urlForCourseRankingLower = "";
String  urlForCourseRankingHigher = "";
String urlForEnryRequirements = "";
String urlForEnryRequirementsLower = "";
String urlForaToZAsc = "";
String urlForaToZDes = "";
 if(sortURL.lastIndexOf(sortByValue) != -1 ) {
      
             
      urlForEnryRequirements =  sortURL.replaceAll(sortByValue, "");
      
      urlForEnryRequirementsLower = sortURL.replaceAll(sortByValue, "&sort=enta");
     
      urlForaToZAsc = sortURL.replaceAll(sortByValue, "&sort=ta");
         
      urlForaToZDes = sortURL.replaceAll(sortByValue, "&sort=td");
   }else {
     
      urlForEnryRequirements = sortURL;
      urlForEnryRequirementsLower = sortURL + "&sort=enta";
      urlForaToZAsc = sortURL + "&sort=ta";
      urlForaToZDes = sortURL + "&sort=td";
   }
   String propopulaedText = "TA".equalsIgnoreCase(sortBy) ? "A - Z Ascending" : "TD".equalsIgnoreCase(sortBy) ? "A - Z Descending" :
                             "Enqtry requirements" ; 
   if(!GenericValidator.isBlankOrNull(pagenochar)) {

     urlForEnryRequirements = util.pageNumberEmpty(urlForEnryRequirements,pagenochar);
     urlForEnryRequirementsLower = util.pageNumberEmpty(urlForEnryRequirementsLower,pagenochar);
     urlForaToZAsc = util.pageNumberEmpty(urlForaToZAsc,pagenochar);
     urlForaToZDes = util.pageNumberEmpty(urlForaToZDes,pagenochar);
   }
   String collegeCount = request.getSession().getAttribute("basketpodcollegecount") != null ? String.valueOf(request.getSession().getAttribute("basketpodcollegecount")) : "0";   

%>
<div class="lsthdr fl_w100">
   <div class="ucas_selcnt w-300 fr">
      <div class="srtby_cnt fl_w100"> 
        <div class="ucas_sbox" id="sortByDiv">
          <span class="sbox_txt" id="qualspanu1">SORT BY:</span><span class="srtby_val" id="choosenSortBy"></span>
           <i class="fa fa-angle-down fa-lg"></i>
        </div>
        <ul id="sortByOption" class="srSortingOption" style="display:none;">
          <li><a class="<%="entd".equalsIgnoreCase(sortBy) || GenericValidator.isBlankOrNull(sortBy) ? "act" : ""%>" onclick="searchEventTracking('sort','<%=srchPrefix%>enqtry requirements','clicked');" href="<%=urlForEnryRequirements%>">Entry requirements (High to Low)</a></li>
          <li><a class="<%="enta".equalsIgnoreCase(sortBy) ? "act" : ""%>" onclick="searchEventTracking('sort','<%=srchPrefix%>enqtry requirements','clicked');" href="<%=urlForEnryRequirementsLower%>">Entry requirements (Low to High)</a></li>
          <li><a class="<%="ta".equalsIgnoreCase(sortBy) ? "act" : ""%>" onclick="searchEventTracking('sort','<%=srchPrefix%>a-z ascending','clicked');" href="<%=urlForaToZAsc%>">A - Z Ascending</a></li>
          <li><a class="<%="td".equalsIgnoreCase(sortBy) ? "act" : ""%>" onclick="searchEventTracking('sort','<%=srchPrefix%>a-z descending','clicked');" href="<%=urlForaToZDes%>">A - Z Descending</a></li>
        </ul>
      </div>
  </div>  
</div>     

<script type="text/javascript">
 var jQuery = jQuery.noConflict();

jQuery("#sortByDiv").on('click', function (event) { 
 if(jQuery('#sortByOption').hasClass('actadd')){
   jQuery('#sortByOption').removeClass('actadd');
   jQuery('#sortByOption').hide();
 }else{
  jQuery('#sortByOption').addClass('actadd');
  jQuery('#sortByOption').show();
 }
});

</script>