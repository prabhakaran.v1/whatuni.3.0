<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.SessionData,WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, mobile.util.MobileUtils" autoFlush="true" %>
<%
  CommonFunction commonFun = new CommonFunction();
  String pageno = request.getAttribute("pageno") != null ? (String)request.getAttribute("pageno") : "1";
  String urldata_1 = (String)request.getAttribute("URL_1");
  String urldata_2 = (String)request.getAttribute("URL_2");
  String urlString  = urldata_1 + pageno + urldata_2;
  String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "NULL";
  String searchhits = (String) request.getAttribute("universityCount");
  String paramlocationValue = (request.getAttribute("paramlocationValue") !=null && !request.getAttribute("paramlocationValue").equals("null") && String.valueOf(request.getAttribute("paramlocationValue")).trim().length()>0 ? String.valueOf(request.getAttribute("paramlocationValue")) : ""); 
  String resultExists = (request.getAttribute("resultExists") !=null && !request.getAttribute("resultExists").equals("null") && String.valueOf(request.getAttribute("resultExists")).trim().length()>0 ? String.valueOf(request.getAttribute("resultExists")) : ""); 
  String universityCount = (request.getAttribute("universityCount") !=null && !request.getAttribute("universityCount").equals("null") && String.valueOf(request.getAttribute("universityCount")).trim().length()>0 ? String.valueOf(request.getAttribute("universityCount")) : ""); 
  String seoLocationValue = (!GenericValidator.isBlankOrNull(paramlocationValue)) ? commonFun.replaceHypenWithSpace(paramlocationValue): "";  
  String paramSubjectCode = (request.getAttribute("paramSubjectCode") !=null && !request.getAttribute("paramSubjectCode").equals("null") && String.valueOf(request.getAttribute("paramSubjectCode")).trim().length()>0 ? String.valueOf(request.getAttribute("paramSubjectCode")) : "");  
  String paramStudyLevelId = (request.getAttribute("paramStudyLevelId") !=null && !request.getAttribute("paramStudyLevelId").equals("null") && String.valueOf(request.getAttribute("paramStudyLevelId")).trim().length()>0 ? String.valueOf(request.getAttribute("paramStudyLevelId")) : ""); 
  String noindexfollow = request.getAttribute("noindex") !=null ? "noindex,follow"  : "index,follow";
  String studyModeValue = (String) request.getAttribute("hitbox_study_mode");
  if(studyModeValue == null){studyModeValue = "";}
  CommonUtil comUtil = new CommonUtil();  
  String subjectDesc = request.getAttribute("subjectDesc") != null ? request.getAttribute("subjectDesc").toString() : "";   
  int articleLength = 0;
  if(subjectDesc!=null && subjectDesc!=""){
    articleLength = subjectDesc.length();
  }
  String articleText = subjectDesc;
  if(articleLength > 15){
    articleText = subjectDesc.substring(0,13) + "...";
  }
  String studyLevelDesc = request.getAttribute("studyLevelDesc") != null ? (String)request.getAttribute("studyLevelDesc") : ""; 
  String studeyLevel = studyLevelDesc;
  String studyLevelOnly = studyLevelDesc;
  String studyLevelTab = studyLevelDesc;
  if(studyLevelTab.indexOf("degree")==-1){
        studyLevelTab = studyLevelTab.indexOf("hnd/hnc") > -1 ? studyLevelTab.toUpperCase() :studyLevelTab;
        studyLevelTab = studyLevelTab + " degree";
  }
  if(!GenericValidator.isBlankOrNull(studeyLevel)){  //29-Oct-2013 SEO Release - Start of change            
     if(studeyLevel.indexOf("degree")==-1){
        studeyLevel = studeyLevel.indexOf("hnd/hnc") > -1 ? studeyLevel.toUpperCase() :studeyLevel;
        studeyLevel = studeyLevel + " degrees";
     }else{
        studeyLevel = studeyLevel.replaceFirst("degree", "degrees");
        }              
      } //29-Oct-2013 SEO Release - End of change 
  String first_mod_group_id = (String)request.getAttribute("first_mod_group_id");
         first_mod_group_id = (first_mod_group_id!=null && first_mod_group_id.trim().length() >0) ? first_mod_group_id : "";
  String totalCourseCount = (String) request.getAttribute("totalCourseCount"); 
         totalCourseCount = GenericValidator.isBlankOrNull(totalCourseCount)?"":totalCourseCount;
  String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
  String canonicalLink = httpStr + "www.whatuni.com/degrees" + firstPageSeoUrl;
  int totalCount = 0;
  if(totalCourseCount!=""){
    totalCount =  Integer.parseInt(totalCourseCount);
  }
  studyLevelTab = totalCount > 1? studyLevelTab+"s": studyLevelTab;
  String firstTabText = totalCourseCount + " " + studyLevelTab;
String newsearchJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.newSearchResults.js");

String pgsRelCanonicalLink = (String)request.getAttribute("pgsCanonicalURL");//13_MAY_2014_REL
if(!GenericValidator.isBlankOrNull(paramStudyLevelId)){
   canonicalLink = GenericValidator.isBlankOrNull(pgsRelCanonicalLink)?"":pgsRelCanonicalLink;
 }
String clearingYear = GlobalConstants.CLEARING_YEAR;
 String filter = commonFun.getWUSysVarValue("FILTER");
 String socialBoxJs = CommonUtil.getResourceMessage("wuni.social.box.js", null);
 boolean mobileFlag = new MobileUtils().userAgentCheck(request);
String entryReqFilter = request.getAttribute("entryReqFilter") != null ? (String)request.getAttribute("entryReqFilter") : "";
String link = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();

String queryStringValue = (String)request.getAttribute("queryStr");   
         queryStringValue = !GenericValidator.isBlankOrNull(queryStringValue) ? queryStringValue.trim() :"";
  String universityName = (String)request.getAttribute("collegeName");  
         universityName = !GenericValidator.isBlankOrNull(universityName)? universityName :"";
  String providerID = (String)request.getAttribute("providerID");  
          providerID = !GenericValidator.isBlankOrNull(providerID)? providerID :"";  
  String sortBy = (String)request.getAttribute("sortByValue");   
  String providerSortBy = sortBy;
  if("entd".equalsIgnoreCase(sortBy)) {
    providerSortBy = "";
  }
  String qString = new CommonFunction().formProviderResultFilterParam(request);
         qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
         request.setAttribute("showlightbox", "YES");
%>

<body>
<div class="cmm_ldericn" style="display:none;" id="clrSRPR"><img src="<%=new CommonUtil().getImgPath("/wu-cont/images/hm_ldr.gif",0)%>"></div>
<div class="csr_res">
  <% // header part and need to check css, js files to be moved%>
  <header class="md clipart">
    <div class="ad_cnr">
      <div class="content-bg">
        <div id="desktop_hdr">
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>                
      </div>      
    </div>
  </header>
    
  <%// middle content %>
    
  <div class="container1">
    <% // Clearing Home Redesign Code %>
    <div class="clr_wrap fl">
      <jsp:include page="/clearing/search/include/whatUniGoSearchBreadCrumb.jsp"/>
      <div class="cle_sres fl"> 
        <div class="ad_cnr">
          <div class="content-bg">
            <div class="sr-cont fl_w100">
              <jsp:include page="/clearing/search/include/srFilter.jsp">
                <jsp:param name="PAGE_FROM" value="BROWSE_MONEY_PAGE"/>
                <jsp:param name="URLDATA_2" value="<%=urldata_2%>"/>
                <jsp:param name="searchPageType" value="CLEARING"/>
              </jsp:include> 
              <% // 2. Search results info middle content%>
              <div class="srs_rgt fl">
                <div class="srs_cnt fl_w100">
                  <%-- <c:if test="${requestScope.wugoFlag eq 'ON'}"> --%>
                    <c:if test="${not empty requestScope.userUCASScore and requestScope.userUCASScore eq 0}">
                      <div class="entq_cnt fl_w100">
                        <div class="blucnt fl_w100" id="empty_ucas_score">
                          <%--<div class="lst_clse fr" onclick="hideSRMessage('empty_ucas_score');"><a title="close icon"><img src="<%=new CommonUtil().getImgPath("/wu-cont/images/lbx_clse_icon.svg",0)%>"></a></div>--%>
                          <h4><spring:message code="enter.grade.title" arguments="${subjectDesc}" htmlEscape="false" argumentSeparator=";"/></h4>
                          <p class="fl_w100"><spring:message code="enter.grade.text" arguments="${studyLevelTextForEnterGrade};${subjectDesc}"
                          htmlEscape="false" argumentSeparator=";"/></p>                          
                          <div class="subtn_cnt fl">
                            <div class="vw_crsbtn fl">
                              <a href="<%=link%>/degrees/wugo/qualifications/search.html?<%=entryReqFilter%>" class="ent_qual">ENTER GRADES</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </c:if>
                  <%-- </c:if> --%>
                    
                  <%-- <c:if test="${requestScope.wugoFlag eq 'OFF' and not empty requestScope.userUCASScore}">
                    <c:if test="${requestScope.userUCASScore eq 0}">
                      <div class="entq_cnt fl_w100">
                        <div class="blucnt fl_w100" id="empty_ucas_score">
                          <div class="lst_clse fr" onclick="hideUcasMessage();"><a title="close icon"><img src="<%=new CommonUtil().getImgPath("/wu-cont/images/lbx_clse_icon.svg",0)%>"></a></div>
                          <h4>Get a place now with Whatuni GO!</h4>
                          <p class="fl_w100">Enter your grades to find matching courses and apply online in as little as 10 minutes, getting an instant 'Offer in Principle' from your chosen uni.</p>
                          <div class="subtn_cnt fl">
                            <div class="vw_crsbtn fl">
                              <a href="<%=link%>/degrees/wugo/qualifications/search.html?<%=entryReqFilter%>" class="ent_qual">ENTER QUALIFICATIONS</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </c:if>
                  </c:if> --%>
                  <c:if test="${not empty requestScope.universityCount and requestScope.resultExists}">
                    <c:if test="${requestScope.resultExists eq 'N'}">
                      <div class="lst cf">       
                        <div id="ran-alert" class="ran-alert" style="display:block;">
                          <c:if test="${requestScope.recentFilter eq 'C'}">
                            <div class="entq_cnt fl_w100">
                              <div class="blucnt fl_w100" id="del_uni_fil">
                                <div class="lst_clse fr" onclick="deleteUniFilter();"><a title="Remove uni filter"><img src="<%=new CommonUtil().getImgPath("/wu-cont/images/lbx_clse_icon.svg",0)%>"></a></div>
                                <p class="fl_w100">
                                  The uni or college you're looking for doesn't offer this course – sorry. You could try searching by location instead, or
                                  <a href="<SEO:SEOURL pageTitle="all-provider-results"><%=universityName%>#<%=providerID%>#<%=qString%></SEO:SEOURL>">view all courses at this uni</a>. We've also displayed some similar universities you might like below.
                                </p>
                              </div>
                            </div>
                          </c:if>
                          <c:if test="${requestScope.recentFilter eq 'P'}">
                            <div class="entq_cnt fl_w100">
                              <div class="blucnt fl_w100" id="del_uni_fil">
                                <div class="lst_clse fr" onclick="deleteSearchFilterParams('postCode');"><a title="Remove postcode filter"><img src="<%=new CommonUtil().getImgPath("/wu-cont/images/lbx_clse_icon.svg",0)%>"></a></div>
                                <p class="fl_w100">
                                  We can't find any results for your postcode (sorry)! You can either try looking within a greater distance of your home, or - if you have filters added - try removing some to widen your search.
                                </p>
                              </div>
                            </div>
                          </c:if>
                        </div>
                      </div>
                      <input type="hidden" id="queryStringVal" name="queryStringVal" value="<%=queryStringValue%>"/>              
                    </c:if>
                  </c:if>
                    
                  <c:if test="${not empty requestScope.courseRankFoundFlag and requestScope.courseRankFoundFlag eq 'N'}">
                    <div class="entq_cnt fl_w100">
                      <div class="blucnt fl_w100" id="empty_sub_rank">
                        <div class="lst_clse fr" onclick="hideSRMessage('empty_sub_rank');"><a title="close icon"><img src="<%=new CommonUtil().getImgPath("/wu-cont/images/lbx_clse_icon.svg",0)%>"></a></div>
                        <p class="fl_w100">There are no CompUniGuide Subject Rankings available for this Subject. Please choose one of the alternative ways of sorting your results</p>
                      </div>
                    </div>
                  </c:if>
                    
                  <jsp:include page="/clearing/search/include/newSortBy.jsp">                          
                    <jsp:param name="srchType" value="clearing"/>
                  </jsp:include>
                    
                  <div class="srlst fl_w100">
                    <% // results dsiplay %>
                    <jsp:include page="/clearing/search/include/searchResultsMainContent.jsp">
                      <jsp:param name="studyLevelModified" value="<%=studeyLevel%>"/>
                      <jsp:param name="studyLevelOnly" value="<%=studyLevelOnly%>"/> 
                      <jsp:param name="PAGE_FROM" value="BROWSE_MONEY_PAGE"/>
                      <jsp:param name="searchPageType" value="CLEARING"/>
                    </jsp:include>
                    <div class="pr_pagn">      
                      <% 
                          int numofpages = 0;
                          int noOfrecords = Integer.parseInt(searchhits.replaceAll(",", ""));
                              numofpages =  noOfrecords / 10;
                          if(noOfrecords % 10 > 0) {  numofpages++; }
                          String pageNameh = "/jsp/search/searchredesign/newPagination.jsp";
                          if(numofpages >1 && ("1").equals(pageno)){  
                            pageNameh = "/jsp/search/searchredesign/searchInitialPagination.jsp";
                          }
                      %>
                      <jsp:include page="<%=pageNameh%>">
                        <jsp:param name="pageno" value="<%=pageno%>"/>
                        <jsp:param name="pagelimit"  value="10"/>
                        <jsp:param name="searchhits" value="<%=searchhits%>"/>
                        <jsp:param name="recorddisplay" value="10"/>
                        <jsp:param name="displayurl_1" value="<%=urldata_1%>"/>
                        <jsp:param name="displayurl_2" value="<%=urldata_2%>"/>
                        <jsp:param name="firstPageSeoUrl" value="<%=firstPageSeoUrl%>"/>
                        <jsp:param name="universityCount" value="<%=universityCount%>"/>
                        <jsp:param name="resultExists" value="<%=resultExists%>"/>
                      </jsp:include>
                    </div>
                  </div> 
                </div>    
              </div>   
            </div> 
          </div>
        </div>
        <form:form action="/courses/*-courses/*-courses-*/*/*/*/*/page" commandName="searchBean" >
          <input type="hidden" id="sortingUrl" value="<%=request.getAttribute("sortingUrl")%>" />
          <input type="hidden" id="subjectname" value="<%=request.getAttribute("subjectName")%>" />
          <input type="hidden" id="studyleveldesc" value="<%=request.getAttribute("studyLevelDesc")%>" />
          <input type="hidden" id="paramStudyLevelId" value="<%=request.getAttribute("paramStudyLevelId")%>" />
          <input type="hidden" id="paramSubjectCode" value="<%=request.getAttribute("paramSubjectCode")%>" />
          <input type="hidden" id="paramlocationValue" value="<%=request.getAttribute("paramlocationValue")%>" />
          <input type="hidden" id="dropdownCollegeId" value="uc" />
          <input type="hidden" id="userid" value='<%=new SessionData().getData(request, "y")%>' />
          <input type="hidden" name="contextPath" id="contextPath" value="/degrees" />
          <input type="hidden" id="pageName" value="moneyPage" />
          <input type="hidden" id="searchtype" name="searchType" value="CLEARING_SEARCH"/>
          <input type="hidden" id="requestURLForAjax" value="<%=(String)request.getAttribute("requestURLForAjax")%>"/>
        </form:form>
        <input type="hidden" id="isClearingSRPage" value="Y"/>
        <input type="hidden" id="check_mobile_hidden" value="<%=mobileFlag%>"/>  
      </div>
    </div>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
    <jsp:include page="/jsp/common/socialBox.jsp" />
  </div>
</div> 
</body>