<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page import="org.apache.commons.validator.GenericValidator, mobile.util.MobileUtils,WUI.utilities.SearchUtilities,WUI.utilities.CommonUtil"%>

<%
  String urlString = (String)request.getAttribute("sortingUrl");
  String queryStringValues = (String)request.getAttribute("queryStringValues");
  urlString = !GenericValidator.isBlankOrNull(urlString) && (urlString.lastIndexOf("&sort=") == -1) ? urlString.trim()+"&sort=entd" : urlString.trim();
  String sortBy = (String)request.getAttribute("sortByValue");
  String studyLevel = (String)request.getAttribute("paramStudyLevelId");  
  studyLevel = !GenericValidator.isBlankOrNull(studyLevel)? studyLevel :"";
  String pagenochar = null;
  CommonUtil util = new CommonUtil();
  SearchUtilities searchUtilities = new SearchUtilities();
  String pageNumberParameter = "pageno";
  if(queryStringValues.contains(pageNumberParameter)){//Added this for pagination removal in URL by Hema.S on 12_FEB_2019_REL
    pagenochar= queryStringValues.substring(queryStringValues.indexOf(pageNumberParameter)-1,queryStringValues.indexOf(pageNumberParameter)+("pageno=".length()+request.getParameter(pageNumberParameter).length()));
  }
  //
  //
  String defineMostInfo = "r-or".equalsIgnoreCase(sortBy) ? "Overall rating": "Most info";//Added by Indumathi Nov-03-15
  String urlForMostInfo = "";
  String urlForCourseRankingLower = "";
  String urlForCourseRankingHigher = "";
  String urlForEnryRequirements = "";
  String urlForEnryRequirementsLower = "";
  String urlForEmploymentRate = "";
  String urlR_OR = "";
  String urlR_CLR = "";
  String urlR_AR = "";
  String urlR_CYLR = "";
  String urlR_UFR = "";
  String urlR_CSR = "";
  String urlR_SUR = "";
  String urlR_ECR = "";
  String urlR_JPR = "";
  String urlR_RR = "";  //
  String classNameMI = "";
  String classNameTR = "";
  String classNameER = "";
  String classNameEMR = "";
  String classReviews = "";
  String classNameLIMI = "";
  String classNameLITR = "";
  String classNameLIER = "";
  String classNameLIEMR = "";
  String classLIReviews = "";
  String courseViewUrl = null;
  String substr = null;
  String sortByValue = "&sort="+sortBy;
  String qString = (String)request.getAttribute("queryStr");         
         qString = !GenericValidator.isBlankOrNull(qString) ? "?" + qString.trim() :"";
  String urlForaToZAsc = "";
  String urlForaToZDes = "";
  
   if(urlString.lastIndexOf(sortByValue) != -1 ) {
     urlForCourseRankingLower =  urlString.replaceAll(sortByValue, "&sort=crl");
     urlForCourseRankingHigher =  urlString.replaceAll(sortByValue, "&sort=crh");
     urlForEnryRequirements =  urlString.replaceAll(sortByValue, "");
     urlForEnryRequirementsLower = urlString.replaceAll(sortByValue, "&sort=enta");
     urlForaToZAsc = urlString.replaceAll(sortByValue, "&sort=ta");
     urlForaToZDes = urlString.replaceAll(sortByValue, "&sort=td");
   }else {
	     
      urlForEnryRequirements = urlString;
      urlForEnryRequirementsLower = urlString + "&sort=enta";
      urlForCourseRankingHigher = urlString + "&sort=crh";
      urlForCourseRankingLower = urlString + "&sort=crl";
      urlForaToZAsc = urlString + "&sort=ta";
      urlForaToZDes = urlString + "&sort=td";
   } 
   String propopulaedText = "crh".equalsIgnoreCase(sortBy) || "crl".equalsIgnoreCase(sortBy) ? "CUG Rankings" :
      "TA".equalsIgnoreCase(sortBy) ? "A - Z Ascending" : "TD".equalsIgnoreCase(sortBy) ? "A - Z Descending" :
       "Enqtry requirements" ;
   
   if(!GenericValidator.isBlankOrNull(pagenochar)) {
	 urlForCourseRankingLower = util.pageNumberEmpty(urlForCourseRankingLower,pagenochar);
	 urlForCourseRankingHigher = util.pageNumberEmpty(urlForCourseRankingHigher,pagenochar);
	 urlForEnryRequirements = util.pageNumberEmpty(urlForEnryRequirements,pagenochar);
	 urlForEnryRequirementsLower = util.pageNumberEmpty(urlForEnryRequirementsLower,pagenochar);
	 urlForaToZAsc = util.pageNumberEmpty(urlForaToZAsc, pagenochar);
	 urlForaToZDes = util.pageNumberEmpty(urlForaToZDes, pagenochar);
   }
   String collegeCount = request.getSession().getAttribute("basketpodcollegecount") != null ? String.valueOf(request.getSession().getAttribute("basketpodcollegecount")) : "0";   
   String srchPrefix = "";
   String srchType = request.getParameter("srchType");
   if(srchType!=null && !"".equals(srchType)){
     if("clearing".equals(srchType)){
      srchPrefix = "clearing-";
     }
   }
   boolean mobileFlag = new MobileUtils().userAgentCheck(request);
%>

<div class="lsthdr fl_w100">
  <div class="ucas_selcnt w-300 fr">
    <div class="srtby_cnt fl_w100"> 
      <div class="ucas_sbox" id="sortByDiv">
        <span class="sbox_txt" id="qualspanu1">SORT BY:</span><span class="srtby_val" id="choosenSortBy"></span>
        <i class="fa fa-angle-down fa-lg"></i>
      </div>
      <ul id="sortByOption" class="srSortingOption" style="display:none;">
        <li><a class="<%="entd".equalsIgnoreCase(sortBy) ? "act" : ""%>" onclick="searchEventTracking('sort','<%=srchPrefix%>enqtry requirements','clicked');" href="<%=urlForEnryRequirements%>">Entry requirements (High to Low)</a></li>
        <li><a class="<%="enta".equalsIgnoreCase(sortBy) ? "act" : ""%>" onclick="searchEventTracking('sort','<%=srchPrefix%>enqtry requirements','clicked');" href="<%=urlForEnryRequirementsLower%>">Entry requirements (Low to High)</a></li>
        <li><a class="<%="crl".equalsIgnoreCase(sortBy) ? "act" : ""%>" onclick="searchEventTracking('sort','<%=srchPrefix%>course-ranking','clicked');" href="<%=urlForCourseRankingLower%>">CUG Ranking (Low to High)</a></li>
        <li><a class="<%="crh".equalsIgnoreCase(sortBy) ? "act" : ""%>" onclick="searchEventTracking('sort','<%=srchPrefix%>course-ranking','clicked');" href="<%=urlForCourseRankingHigher%>">CUG Ranking (High to Low)</a></li>
        <li><a class="<%="ta".equalsIgnoreCase(sortBy) ? "act" : ""%>"  onclick="searchEventTracking('sort','<%=srchPrefix%>a-z ascending','clicked');" href="<%=urlForaToZAsc%>">A - Z Ascending</a></li>
        <li><a class="<%="td".equalsIgnoreCase(sortBy) ? "act" : ""%>" onclick="searchEventTracking('sort','<%=srchPrefix%>a-z descending','clicked');" href="<%=urlForaToZDes%>">A - Z Descending</a></li>
      </ul>
    </div>
  </div>  
</div>     

<script type="text/javascript">
 var jQuery = jQuery.noConflict();

 jQuery("#sortByDiv").on('click', function (event) {
   if(jQuery('#sortByOption').hasClass('actadd')){
     jQuery('#sortByOption').removeClass('actadd');
     jQuery('#sortByOption').hide();
   }else{
     jQuery('#sortByOption').addClass('actadd');
     jQuery('#sortByOption').show();
   }
 });
</script>