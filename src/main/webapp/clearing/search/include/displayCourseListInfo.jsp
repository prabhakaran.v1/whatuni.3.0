<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:if test="${not empty requestScope.listOfSearchResults}">
  <% 
     int index = 1;
     int listSize = request.getAttribute("listOfSearchResultsSize") != null ? (Integer)request.getAttribute("listOfSearchResultsSize") : 0 ;
  %>
  <c:forEach items="${requestScope.listOfSearchResults}" var="searchResult" varStatus="row">
    <div class="blu_cnt fl_w100">
      <div class="sub_lst fl">
        <h4>
          <c:if test="${searchResult.isSponsoredCollege eq 'Y'}"> 
            <a onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','${searchResult.collegeId}','${searchResult.courseDetailsPageURL}')" href='${searchResult.courseDetailsPageURL}'>${searchResult.courseTitle}</a>
          </c:if>
          <c:if test="${searchResult.isSponsoredCollege ne 'Y'}">
            <a  href='${searchResult.courseDetailsPageURL}'>${searchResult.courseTitle}</a>
          </c:if>
        </h4>
        <c:if test="${not empty searchResult.departmentOrFaculty}"><h5>${searchResult.departmentOrFaculty}</h5></c:if>
        <div class="ucas_fld">
          <c:if test="${not empty searchResult.courseUcasTariff}"><span class="ucas_pt">${searchResult.courseUcasTariff}</span></c:if>
          <c:if test="${empty searchResult.courseUcasTariff}"><span class="ucas_pt">Please contact university</span></c:if>
          <c:if test="${not empty searchResult.showApplyNowButton and searchResult.showApplyNowButton eq 'Y'}">
            <span class="sl_goap">
              <span class="go">GO</span>
              <span class="apply">APPLY</span>
              <span class="hlp tool_tip"><i class="fa fa-question-circle"></i>
                <span class="cmp hlp-tt">
                  <div class="hdf5"></div>
                  <div>Apply for this course and get an instant decision online right now.</div> 
                  <div class="line"></div>
                </span>
              </span>
            </span>
          </c:if>
        </div>
        <c:if test="${searchResult.showTooltipFlag eq 'Y'}">
          <ul class="strat ucsPotsCnt cf fl_w100">
            <li class="mr-15 leag_wrap">        
              <span class="t_tip">
                <span class="UcsPots">UCAS Score seems too high?</span>                        
                <span class="cmp">
                  <div class="hdf5">The UCAS points for this course might be higher than what you have achieved, but we think it might still be possible for you to enrol on this course, so we'd recommend you call the university if you're interested in this course</div>
                  <div class="line"></div>
                </span>
              </span>
            </li>
          </ul>
        </c:if>
      </div>
      <div class="subtn_cnt fr">
        <div class="vw_crsbtn fr">
          <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
            <a onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','${searchResult.collegeId}','${searchResult.courseDetailsPageURL}')" href="${searchResult.courseDetailsPageURL}">VIEW COURSE <i class="fa fa-long-arrow-right"></i></a>
          </c:if>
          <c:if test="${searchResult.isSponsoredCollege ne 'Y'}">
            <a href="${searchResult.courseDetailsPageURL}">VIEW COURSE <i class="fa fa-long-arrow-right"></i></a>
          </c:if>
        </div>
      </div>
    </div>
    <%index++;%>
  </c:forEach>
</c:if>