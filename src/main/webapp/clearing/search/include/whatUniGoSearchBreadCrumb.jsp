<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ page import="org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants" autoFlush="true"%>
<%

String bc_breadCrumb = request.getAttribute("breadCrumb") != null && request.getAttribute("breadCrumb").toString().trim().length() > 0 ? request.getAttribute("breadCrumb").toString() : "";
String paramSubjectCode = (request.getAttribute("paramSubjectCode") !=null && !request.getAttribute("paramSubjectCode").equals("null") && String.valueOf(request.getAttribute("paramSubjectCode")).trim().length()>0 ? String.valueOf(request.getAttribute("paramSubjectCode")) : "");  
String tmpUcasCode = (request.getAttribute("paramUcasCode") !=null && !request.getAttribute("paramUcasCode").equals("null") && String.valueOf(request.getAttribute("paramUcasCode")).trim().length()>0 ? String.valueOf(request.getAttribute("paramUcasCode")) : "");
String jacsSubjectName = (String)request.getAttribute("jacsSubjectName");
         jacsSubjectName = (!GenericValidator.isBlankOrNull(jacsSubjectName))?jacsSubjectName:"";
String totalCourseCount = (String) request.getAttribute("totalCourseCount"); 
         totalCourseCount = GenericValidator.isBlankOrNull(totalCourseCount)?"":totalCourseCount;
  int totalCount = 0;
  if(totalCourseCount!=""){
   totalCount = Integer.parseInt(totalCourseCount);
  }
  String studyLevelTab = request.getAttribute("studyLevelDesc") != null ? (String)request.getAttribute("studyLevelDesc") : "";  ;
  if(studyLevelTab.indexOf("degree")==-1){
        studyLevelTab = studyLevelTab.indexOf("hnd/hnc") > -1 ? studyLevelTab.toUpperCase() :studyLevelTab;
        studyLevelTab = studyLevelTab + " degree";
  }
String firstTabText = totalCourseCount +  " " + studyLevelTab;

  if(!GenericValidator.isBlankOrNull(firstTabText)){
    int firstTabLength = firstTabText.length();
    if(firstTabLength > 25){
      firstTabText = firstTabText.substring(0,24)+"...";
    }
  }
    
  if(totalCourseCount!=""){
   totalCount = Integer.parseInt(totalCourseCount);
  }
  studyLevelTab = totalCount > 1? studyLevelTab+"s": studyLevelTab;
  firstTabText = totalCount > 1 ? firstTabText + "s" : firstTabText;
  String studyLevelDesc = request.getAttribute("studyLevelDesc") != null ? (String)request.getAttribute("studyLevelDesc") : "";  
  String studeyLevel = studyLevelDesc;
  if(!GenericValidator.isBlankOrNull(studeyLevel)){              
     if(studeyLevel.indexOf("degree")==-1){
        studeyLevel = studeyLevel.indexOf("hnd/hnc") > -1 ? studeyLevel.toUpperCase() :studeyLevel;
        studeyLevel = studeyLevel + " degrees";
     }else{
        studeyLevel = studeyLevel.replaceFirst("degree", "degrees");
        }              
  }
 String searchLocation = (String)request.getAttribute("SEARCH_LOCATION_OR_POSTCODE") != null ? (String)request.getAttribute("SEARCH_LOCATION_OR_POSTCODE") : "";
  if(searchLocation!=null && searchLocation!=""){searchLocation = searchLocation.trim();}  
  String entryReqFilter = request.getAttribute("entryReqFilter") != null ? (String)request.getAttribute("entryReqFilter") : "";
  String link = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();
%>
<div class="bcrmb_hd fl">
  <div class="ad_cnr">
    <div class="content-bg">
      <div class="bcrmb_cnt fl">
       <div class="brcb_lft fl">
         <div class="bcrb_wrp">
        
           <%if (bc_breadCrumb != null && bc_breadCrumb.trim().length() > 0){%>
             <%=bc_breadCrumb%>
           <%}%>
        </div>
        <div class="opuni_srch">
          <h1>
          <c:if test="${not empty requestScope.subjectDesc}">${requestScope.subjectDesc}</c:if> <%=studeyLevel%> in Clearing & Adjustment <c:if test="${not empty requestScope.searchLocation and requestScope.searchLocation ne 'United Kingdom'}"> in ${requestScope.searchLocation}</c:if>
          </h1>
          <p>
            <c:if test="${not empty requestScope.universityCount}">${requestScope.universityCount} <c:if test="${requestScope.universityCount eq 1}">university</c:if><c:if test="${requestScope.universityCount gt 1}">universities</c:if> </c:if> offer <c:if test="${not empty requestScope.totalCourseCount}"><%=firstTabText%></c:if> including <c:if test="${not empty requestScope.subjectDesc}">${requestScope.subjectDesc}</c:if>
          </p>
          </div>
        
        <div class="fl_lr">
          <a href="javascript:void(0);">
          <span class="left"><i class="fa fa-filter"></i></span>
          <span class="right">Filter results</span>
          </a>
        </div>
        </div>
        <c:if test="${not empty requestScope.userUCASScore and requestScope.userUCASScore ne 0}">
          <div class="ucs_rgt fr">
            <div class="uscr_tit">Your UCAS score</div>
            <div class="ucpt">
              <span class="ucspt">${requestScope.userUCASScore} points</span>
              <span class="ucs_edt"><a href="<%=link%>/degrees/wugo/qualifications/search.html?<%=entryReqFilter%>">Edit</a></span>
            </div>
          </div>
        </c:if>
        </div>
    </div>
  </div>
</div>
          