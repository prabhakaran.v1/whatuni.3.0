<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.SessionData,WUI.utilities.GlobalConstants, WUI.utilities.GlobalFunction, mobile.util.MobileUtils" %>
<%String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
  %>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
<% String toolTip = new CommonFunction().getDomainName(request, "N"); 
   String CONTEXT_PATH = GlobalConstants.WU_CONTEXT_PATH;
  String searchPageType = request.getParameter("searchPageType");
         searchPageType = !GenericValidator.isBlankOrNull(searchPageType)?searchPageType:"";
  String clearingPage =  (String)request.getAttribute("searchClearing");
         clearingPage = GenericValidator.isBlankOrNull(clearingPage)?"":"TRUE";
  String searchhits = (String) request.getAttribute("universityCount");
  String urlString = request.getAttribute("urlString") != null ? request.getAttribute("urlString").toString() : "";  
  String pageFrom = request.getParameter("PAGE_FROM");
  String keyword =  "";
  if(("KEWORD_MONEY_PAGE").equals(pageFrom)){   keyword =  (String)request.getAttribute("SEARCH_KEYWORD_OR_SUBJECT");}else{keyword =  (String)request.getAttribute("keywordOrSubject");}  
  String urlSTR = (String) request.getAttribute("URL_STRING");  
  String formatedURL = CONTEXT_PATH + urlSTR+".html";
  String univiewUrl = null;
  String studyLevelModified = request.getParameter("studyLevelModified"); //29_OCT_2013_REL
          studyLevelModified = studyLevelModified != null && studyLevelModified.trim().length()>0 ? studyLevelModified : ""; 
  String studyLevelOnly = request.getParameter("studyLevelOnly"); //29_OCT_2013_REL
          studyLevelOnly = studyLevelOnly != null && studyLevelOnly.trim().length()>0 ? studyLevelOnly : ""; 
  if(studyLevelOnly.indexOf("degree")==-1){
        studyLevelOnly = studyLevelOnly.indexOf("hnd/hnc") > -1 ? studyLevelOnly.toUpperCase() :studyLevelOnly;
        studyLevelOnly = studyLevelOnly + " degree";
  }
  String sortBy = (String)request.getAttribute("sortByValue");   
  String reviewText = "";
  String categoryText = "university of the year";
  if(("r-or").equals(sortBy)){ reviewText = "" ;categoryText="university of the year";}
  if(("r-clr").equals(sortBy)){ reviewText = "Course & Lecturers";categoryText="course & lecturers";}
  if(("r-ar").equals(sortBy)){ reviewText = "Accomodation";categoryText="accommodation";} 
  if(("r-cylr").equals(sortBy)){ reviewText = "City Life";categoryText="city life";}
  if(("r-ufr").equals(sortBy)){ reviewText = "Uni Facilities";categoryText="uni facilities";}
  if(("r-csr").equals(sortBy)){ reviewText = "Clubs and Societies";categoryText="clubs & societies";}
  if(("r-sur").equals(sortBy)){ reviewText = "Student Union";categoryText="student union";} 
  // if(("r-ecr").equals(sortBy)){ reviewText = "Eye Candy" ;} //08_OCT_2014 Added by Amir to remove Eye Candy
  if(("r-jpr").equals(sortBy)){ reviewText = "Job Prospectus";categoryText="job prospects";} 
  if(("r-rr").equals(sortBy)){ reviewText = "RECOMMEND THIS UNI" ;} 
  String providerSortBy = sortBy;
  if(sortBy.contains("r-")||sortBy.equals("r")){providerSortBy="";}
  CommonFunction commonFunction = new CommonFunction();
  String qString = commonFunction.formProviderResultFilterParam(request);
         qString = !GenericValidator.isBlankOrNull(qString)? ("?nd"+qString):"";
  if(providerSortBy!=""){
        qString = qString + (GenericValidator.isBlankOrNull(qString)? ("?nd&sort="+providerSortBy):("&sort="+providerSortBy));
  }
  String keywordorsubjectemail = request.getAttribute("keywordOrSubject") != null ? new CommonFunction().replaceURL(request.getAttribute("keywordOrSubject").toString()) : "0";  
  String uniHomeUrl = "";
  String clbtnclassname = "";
  String sponsporedpageLogging ="";
  String hotlinelogging  = "";
  String innerhotlinelogging = "";
  if("CLEARING".equals(searchPageType)){ clbtnclassname ="clr_btns";}
  String clrText = "";
  String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");
  String imgpath="";
  String capeImg = CommonUtil.getImgPath("/wu-cont/images/capeh.png",0);
  String matchOnImg = CommonUtil.getImgPath("/wu-cont/images/match_yes.png",0);
  String matchOffImg = CommonUtil.getImgPath("/wu-cont/images/match_no.png",0);
  String onePxImg = CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0);
  boolean mobileFlag = new MobileUtils().userAgentCheck(request);
  String reviewCatStr_1 = (String)request.getAttribute("reviewCatStr_1");
  String reviewCatStr_2 = (String)request.getAttribute("reviewCatStr_2");
  String reviewCatStr_3 = (String)request.getAttribute("reviewCatStr_3");
  String pageno = request.getAttribute("pageno") != null ? (String)request.getAttribute("pageno") : "1";
%>

<%-- <jsp:include page="/jsp/search/include/noResultsBar.jsp"/>
<jsp:include page="/jsp/search/include/searchGoogleAdSlots.jsp"/> --%>

<c:if test="${not empty requestScope.listOfSearchResults}">
  <c:forEach items="${requestScope.listOfSearchResults}" var="searchResult" varStatus="row">
     <c:set var="rowValue" value="${row.index}"/>
    <div class="sruni_cnr fl_w100">
      <%String gaCollegeName ="";%>
     
      <c:set value="${searchResult.collegeName}" var="collegeName" />
      <c:set value="${searchResult.collegeId}" var="collegeId" />
      <c:set value="${searchResult.collegeId}" var="hcCollegeId" />
      <%
        gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(pageContext.getAttribute("collegeName").toString());
        gaCollegeName = !GenericValidator.isBlankOrNull(gaCollegeName) ? gaCollegeName.trim() : gaCollegeName;
      %>
      <div class="unilgo_sec">  
        <div class="lgo_univ fl">
          <c:if test="${not empty searchResult.collegeLogoPath}">
            <c:set value="${searchResult.collegeLogoPath}" var="collegeLogoPath" />
            <% imgpath=CommonUtil.getImgPath(pageContext.getAttribute("collegeLogoPath").toString(), (Integer.parseInt(pageContext.getAttribute("rowValue").toString()))); %>
            <%if((Integer.parseInt(pageContext.getAttribute("rowValue").toString()))>=1){%>
              <a href="${searchResult.uniHomeUrl}?clearing" >
                <img src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=imgpath%>" width="105" data-src-mobile="<%=imgpath%>" title="${searchResult.collegeNameDisplay}" class="ibdr" alt="${searchResult.collegeNameDisplay}"/>          
              </a>
            <%}else{%>
              <a href="${searchResult.uniHomeUrl}?clearing" >
                <img src="<%=imgpath%>" data-src-mobile="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" width="105" title="${searchResult.collegeNameDisplay}" class="ibdr" alt="${searchResult.collegeNameDisplay}"/>          
              </a>
            <%}%>
          </c:if>
          <c:if test="${empty searchResult.collegeLogoPath}">
            <%imgpath="";%>
            <a href="${searchResult.uniHomeUrl}?clearing" > <img src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" width="105" class="ibdr" alt="${searchResult.collegeNameDisplay}" title="${searchResult.collegeNameDisplay}"/></a>
          </c:if>
        </div>
      </div>
        
      <div class="dec_univ fl">
        <div class="univ_sec fl_w100">
          <div class="lst_univ fl_w100">
            <h3>
              <%if("CLEARING".equals(searchPageType)){%>
                <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
                  <a href="${searchResult.uniHomeUrl}?clearing" onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','${searchResult.collegeId}','${searchResult.uniHomeUrl}?clearing')">${searchResult.collegeNameDisplay}<i class="fa fa-long-arrow-right"></i></a>
                </c:if>
                <c:if test="${searchResult.isSponsoredCollege ne 'Y'}">
                  <a href="${searchResult.uniHomeUrl}?clearing" >${searchResult.collegeNameDisplay}<i class="fa fa-long-arrow-right"></i></a>
                </c:if>
              <%}else{%>
                <a href="${searchResult.uniHomeUrl}">${searchResult.collegeNameDisplay}<i class="fa fa-long-arrow-right"></i></a>
              <%}%>  
            </h3>
            <div class="crs_tit1">
              <p>
                <c:if test="${not empty searchResult.collegePRUrl}">
                  <a rel="nofollow" href='${searchResult.collegePRUrl}'>${searchResult.numberOfCoursesForThisCollege} <%=keyword%> <%=studyLevelOnly%><c:if test="${searchResult.numberOfCoursesForThisCollege gt 1}">s</c:if></a>
                </c:if>
                <c:if test="${empty searchResult.collegePRUrl}">${searchResult.numberOfCoursesForThisCollege} <%=keyword%> <%=studyLevelOnly%><c:if test="${searchResult.numberOfCoursesForThisCollege gt 1}">s</c:if></c:if>
              </p>
            </div>
            <ul class="strat cf">
              <li class="mr-15 leag_wrap">
                <c:if test="${not empty requestScope.sortOrderCugRankingApplied and not empty searchResult.courseRank}">
                  <span class="fl mr10">CompUniGuide &nbsp; ranking &nbsp;${searchResult.courseRank}${searchResult.ordinalRank}<span class="cal"> </span></span> 
                </c:if>
                <c:if test="${empty requestScope.sortOrderCugRankingApplied and not empty searchResult.overallRating}">
                  <c:if test="${searchResult.overallRating gt 0}">
                    <span class="fl mr10">OVERALL RATING <span class="cal"> </span></span> 
                    <span class="rat${searchResult.overallRating} t_tip">
                      <span class="cmp">
                        <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
                        <div class="line"></div>
                      </span>
                    </span>
                    <span class="rtx">(${searchResult.exactRating})</span>
                  </c:if>
                
                  <% if("CLEARING".equals(searchPageType)){%>
                    <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
                      <a class="link_blue"  onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','${searchResult.collegeId}','${searchResult.uniReviewsURL}')" href="${searchResult.uniReviewsURL}">
                        <c:if test="${not empty searchResult.reviewCount and searchResult.reviewCount ne 0}">
                          ${searchResult.reviewCount}<c:if test="${searchResult.reviewCount gt 1}">reviews</c:if><c:if test="${searchResult.reviewCount eq 1}">review</c:if>   
                        </c:if>
                      </a>  
                    </c:if>
                    <c:if test="${searchResult.isSponsoredCollege ne 'Y'}">
                      <a class="link_blue" href="${searchResult.uniReviewsURL}">
                        <c:if test="${not empty searchResult.reviewCount and searchResult.reviewCount ne 0}">
                          ${searchResult.reviewCount}<c:if test="${searchResult.reviewCount gt 1}">reviews</c:if><c:if test="${searchResult.reviewCount eq 1}">review</c:if>   
                        </c:if>
                      </a>  
                    </c:if>
                  <%}else{%>
                    <a class="link_blue" href="${searchResult.uniReviewsURL}">
                      <c:if test="${not empty searchResult.reviewCount and searchResult.reviewCount ne 0}">
                        ${searchResult.reviewCount}<c:if test="${searchResult.reviewCount gt 1}">reviews</c:if><c:if test="${searchResult.reviewCount eq 1}">review</c:if>   
                      </c:if>
                    </a>  
                  <%}%> 
                </c:if>
                <c:if test="${not empty requestScope.sortOrderCugRankingApplied}">
                  <c:if test="${empty searchResult.courseRank and not empty searchResult.overallRating}">
                    <c:if test="${searchResult.overallRating gt 0}">
                      <span class="fl mr10">OVERALL RATING <span class="cal"> </span></span> 
                      <span class="rat${searchResult.overallRating} t_tip">
                        <span class="cmp">
                          <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats"/></div>
                          <div class="line"></div>
                        </span>
                      </span>
                      <span class="rtx">(${searchResult.exactRating})</span> 
                    </c:if>
                  </c:if>
                  <% if("CLEARING".equals(searchPageType)){%>
                    <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
                      <a class="link_blue"  onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','${searchResult.collegeId}','${searchResult.uniReviewsURL}')" href="${searchResult.uniReviewsURL}">
                        <c:if test="${not empty searchResult.reviewCount and searchResult.reviewCount ne 0}">
                          ${searchResult.reviewCount}
                          <c:if test="${searchResult.reviewCount gt 1}">reviews</c:if>
                          <c:if test="${searchResult.reviewCount eq 1}">review</c:if>
                        </c:if>
                      </a>  
                    </c:if>
                    <c:if test="${searchResult.isSponsoredCollege ne 'Y'}">
                      <a class="link_blue" href="${searchResult.uniReviewsURL}">
                        <c:if test="${not empty searchResult.reviewCount and searchResult.reviewCount ne 0}">
                          ${searchResult.reviewCount}
                          <c:if test="${searchResult.reviewCount gt 1}">reviews</c:if>
                          <c:if test="${searchResult.reviewCount eq 1}">review</c:if>
                        </c:if>
                      </a>
                    </c:if>
                  <%}else{%>
                    <a class="link_blue" href="${searchResult.uniReviewsURL}">
                      <c:if test="${not empty searchResult.reviewCount and searchResult.reviewCount ne 0}">
                        ${searchResult.reviewCount}
                        <c:if test="${searchResult.reviewCount gt 1}">reviews</c:if>
                        <c:if test="${searchResult.reviewCount eq 1}">review</c:if>
                      </c:if>
                    </a>  
                  <%}%>
                  <c:if test="${not empty searchResult.reviewCount and searchResult.reviewCount ne 0}">
                    ${searchResult.reviewCount}
                    <c:if test="${searchResult.reviewCount gt 1}">reviews</c:if>
                    <c:if test="${searchResult.reviewCount eq 1}">review</c:if>
                  </c:if> 
                </c:if>
              </li>
            </ul>
          </div>
          <c:if test="${not empty searchResult.subOrderItemId}">
            <c:if test="${searchResult.subOrderItemId gt 0}"><%request.setAttribute("isClearingProfile", "YES");%></c:if>
          </c:if>
        </div>
        <c:if test="${not empty searchResult.bestMatchCoursesList}">
          <c:set value="${searchResult.bestMatchCoursesList}" var="bestMatchCourseList"/>
          <div class="slst_cnt fl_w100">
            <c:forEach items="${bestMatchCourseList}" var="bestMatchCourse" varStatus="rowOne">
              <c:set var="rowValueOne" value="${rowOne.index}"/>
              <div class="blu_cnt fl_w100">
                <div class="sub_lst fl">
                  <h4>
                    <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
                      <a onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','${searchResult.collegeId}','${bestMatchCourse.courseDetailsPageURL}')" href="${bestMatchCourse.courseDetailsPageURL}">${bestMatchCourse.courseTitle}</a>
                    </c:if>
                    <c:if test="${searchResult.isSponsoredCollege ne 'Y'}">
                      <a  href="${bestMatchCourse.courseDetailsPageURL}">${bestMatchCourse.courseTitle}</a>
                    </c:if>
                  </h4>
                  <c:if test="${not empty bestMatchCourse.departmentOrFaculty}"><h5>${bestMatchCourse.departmentOrFaculty}</h5></c:if>
                  <div class="ucas_fld">
                    <c:if test="${not empty bestMatchCourse.courseUcasTariff}">
                      <span class="ucas_pt">${bestMatchCourse.courseUcasTariff}</span>
                    </c:if>
                    <c:if test="${empty bestMatchCourse.courseUcasTariff}"><span class="ucas_pt">Please contact university</span></c:if>
                  </div>
                  <c:if test="${bestMatchCourse.showTooltipFlag eq 'Y'}">
                    <ul class="strat ucsPotsCnt cf fl_w100">
                      <li class="mr-15 leag_wrap">        
                        <span class="t_tip">
                          <span class="UcsPots">UCAS Score seems too high?</span>                        
                          <span class="cmp">
                            <div class="hdf5">The UCAS points for this course might be higher than what you have achieved, but we think it might still be possible for you to enrol on this course, so we'd recommend you call the university if you're interested in this course</div>
                            <div class="line"></div>
                          </span>
                        </span>
                      </li>
                    </ul>
                  </c:if>
                  <div class="subtn_cnt fl_w100">
                    <div class="vw_crsbtn fl">
                      <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
                        <a onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','${searchResult.collegeId}','${bestMatchCourse.courseDetailsPageURL}')" href="${bestMatchCourse.courseDetailsPageURL}">VIEW COURSE <i class="fa fa-long-arrow-right"></i></a>
                      </c:if>
                      <c:if test="${searchResult.isSponsoredCollege ne 'Y'}">
                        <a  href="${bestMatchCourse.courseDetailsPageURL}">VIEW COURSE <i class="fa fa-long-arrow-right"></i></a>
                      </c:if>
                    </div>
                  </div>
                </div>
                <c:if test="${not empty searchResult.subOrderItemId and searchResult.subOrderItemId gt 0}">
                  <div class="subtn_cnt fr">
                    <c:if test="${not empty bestMatchCourse.showApplyNowButton}">
                      <div class="go_aplybtn fl_w100">
                        <span class="hlp tool_tip"><i class="fa fa-question-circle"></i>
                          <span class="cmp hlp-tt">
                            <div class="hdf5"></div>
                            <div>Apply for this course and get an instant decision online right now.</div> 
                            <div class="line"></div>
                          </span>
                        </span>
                                  
                        <a title="GO APPLY ONLINE" 
                           onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Apply Now', '<%=gaCollegeName%>'); SRApplyNow('<%=pageContext.getAttribute("collegeId").toString()%>', '${bestMatchCourse.courseId}', '${bestMatchCourse.opportunityId}', '${searchResult.subOrderItemId}');" 
                           class="fl_w100"><span class="go">GO</span>APPLY ONLINE</a>
                      </div>
                    </c:if>
                    <c:set value="${searchResult.subOrderItemId}" var="subOrderItemId"/>
                    <div class="lst_clbtn fl_w100">
                      <div class="btns_interaction fl_w100 clr_btn">
                        <c:if test="${not empty searchResult.hotLineNo}">
                          <a title="CALL NOW" 
                             onclick="GAInteractionEventTracking('Webclick', 'interaction', 'hotline','<%=gaCollegeName%>');cpeHotlineClearing(this,'<%=pageContext.getAttribute("collegeId").toString()%>','${searchResult.subOrderItemId}','${searchResult.networkId}','${searchResult.hotLineNo}');changeContactButton('${searchResult.hotLineNo}', '${searchResult.networkId}', '<%=gaCollegeName%>', '${searchResult.subOrderItemId}','contact_<%=pageContext.getAttribute("rowValue").toString()%>_<%=pageContext.getAttribute("rowValueOne").toString()%>', '<%=pageContext.getAttribute("collegeId").toString()%>');" 
                             class="clr-call fl_w100" 
                             id="contact_<%=pageContext.getAttribute("rowValue").toString()%>_<%=pageContext.getAttribute("rowValueOne").toString()%>" ><i class="fa fa-phone" aria-hidden="true"></i> CALL NOW</a>
                        </c:if>
                        <c:if test="${not empty searchResult.subOrderWebsite}">
                          <a rel="nofollow" 
                             target="_blank"
                             onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', '${searchResult.websitePrice}');cpeWebClickClearingWithCourse(this,'<%=pageContext.getAttribute("collegeId").toString()%>', '${bestMatchCourse.courseId}', '${searchResult.subOrderItemId}','${searchResult.networkId}','${searchResult.subOrderWebsite}');" 
                             href="${searchResult.subOrderWebsite}"
                             class="visit-web fl_w100" 
                             title="VISIT WEBSITE">Visit website</a>
                        </c:if>
                      </div>
                    </div>
                  </div>
                </c:if>
              </div>
            </c:forEach>
            <c:if test="${searchResult.numberOfCoursesForThisCollege gt 3 and not empty searchResult.collegePRUrl}">
              <div class="cmre_crse fl_w100" id="prPageURLLink_<%=pageContext.getAttribute("rowValue").toString()%>" style="display:block;">
                <a href='${searchResult.collegePRUrl}' rel="nofollow" class="fl_w100">VIEW All ${searchResult.numberOfCoursesForThisCollege} COURSES<i class="fa fa-long-arrow-right"></i></a>
              </div>
            </c:if>
          </div>
        </c:if>
        <%if((Integer.parseInt(pageContext.getAttribute("rowValue").toString())) == 0 && "1".equalsIgnoreCase(pageno)){%> 
          <jsp:include page="/clearing/watchvideo/showWatchVideoButton.jsp"/>
        <%}%>
      </div>
    </div>
    
      <jsp:include page="/jsp/search/include/searchDrawAdSlots.jsp">
        <jsp:param name="fromPage" value="searchresults"/>
        <jsp:param name="position" value="${rowValue}"/>
        <jsp:param name="totalResults" value="<%=searchhits%>"/>
      </jsp:include>
   </c:forEach>
  </c:if>
 <input type="hidden" id="srcEvent"/>
 <input type="hidden" id="currentcompare" value=""/> 
  <%if("CLEARING".equals(searchPageType)){ clrText = "clearing-";%>
      <input type="hidden" name="comparepage" id="comparepage" value="CLEARING"/>    
 <%}%> 

<input type="hidden" name="isclearingPage" id="isclearingPage" value="<%=clearingPage%>"/>


<script type="text/javascript">  
var opdub = jQuery.noConflict();
opdub(window).scroll(function(){ // scroll event 
    lazyloadetStarts();
});
</script>



<jsp:include page="/search/include/includeGradelb.jsp"/>