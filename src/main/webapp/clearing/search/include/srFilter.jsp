<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil,WUI.utilities.GlobalConstants"%>
<%
  String urlData_2 = request.getParameter("URLDATA_2");
  int indexOfUrl = urlData_2.indexOf("page");  
  //String modDefaultText = "e.g. Module name";
  //String moduleDefaultText = "e.g. Module name";  
  String q = GenericValidator.isBlankOrNull(request.getParameter("q")) ? "" : (request.getParameter("q"));
  String subject = GenericValidator.isBlankOrNull(request.getParameter("subject")) ? "" : (request.getParameter("subject"));  
  String university = GenericValidator.isBlankOrNull(request.getParameter("university")) ? "" : (request.getParameter("university"));
  String module = GenericValidator.isBlankOrNull(request.getParameter("module")) ? "" : (request.getParameter("module"));
  String locType = GenericValidator.isBlankOrNull(request.getParameter("location-type")) ? "" : (request.getParameter("location-type"));
  String location = GenericValidator.isBlankOrNull(request.getParameter("location")) ? "" : (request.getParameter("location"));
  String postCode = GenericValidator.isBlankOrNull(request.getParameter("postcode")) ? "" : (request.getParameter("postcode"));
  String distance = GenericValidator.isBlankOrNull(request.getParameter("distance")) ? "10" : (request.getParameter("distance"));
  String campusType = GenericValidator.isBlankOrNull(request.getParameter("campus-type")) ? "" : (request.getParameter("campus-type"));  
  String studyMode = GenericValidator.isBlankOrNull(request.getParameter("study-mode")) ? "" : (request.getParameter("study-mode"));
  String assessmentType = GenericValidator.isBlankOrNull(request.getParameter("assessment-type")) ? "" : (request.getParameter("assessment-type"));
  String reviewCategoryType = GenericValidator.isBlankOrNull(request.getParameter("your-pref")) ? "" : (request.getParameter("your-pref"));
  String empRateMax = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-max")) ? "100" : (request.getParameter("employment-rate-max"));
  String empRateMin = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-min")) ? "" : (request.getParameter("employment-rate-min"));
  String ucasTarrifMax = GenericValidator.isBlankOrNull(request.getParameter("ucas-points-max")) ? "" : (request.getParameter("ucas-points-max"));
  String ucasTarrifMin = GenericValidator.isBlankOrNull(request.getParameter("ucas-points-min")) ? "" : (request.getParameter("ucas-points-min"));
  String russellGroup = GenericValidator.isBlankOrNull(request.getParameter("russell-group")) ? "" : (request.getParameter("russell-group"));
  String entryLevel = GenericValidator.isBlankOrNull(request.getParameter("entry-level"))? "" : (request.getParameter("entry-level"));
  String entryPoints = GenericValidator.isBlankOrNull(request.getParameter("entry-points"))? "" : (request.getParameter("entry-points"));
  String pageno = GenericValidator.isBlankOrNull(request.getParameter("pageno"))? "" : (request.getParameter("pageno"));
  String sortorder = GenericValidator.isBlankOrNull(request.getParameter("sort"))? "" : (request.getParameter("sort"));
  String ucasCode = !GenericValidator.isBlankOrNull(request.getParameter("ucas-code")) ? (request.getParameter("ucas-code")) : "";
  String rf = !GenericValidator.isBlankOrNull(request.getParameter("rf")) ? (request.getParameter("rf")) : "";
  String awardsURL = "/student-awards-winners/university-of-the-year/";
  String scoreValue = !GenericValidator.isBlankOrNull(request.getParameter("score")) ? request.getParameter("score") : "";
  String applyFlag = !GenericValidator.isBlankOrNull(request.getParameter("applyflag")) ? request.getParameter("applyflag") : "";
  String editUcasLinkUrl = "";
  String pageFrom = request.getParameter("PAGE_FROM");
  String jacs = request.getParameter("jacs");
        jacs = !GenericValidator.isBlankOrNull(jacs)? jacs: "";
  
  String sortBy = (String)request.getAttribute("sortByValue");
         sortBy = !GenericValidator.isBlankOrNull("sortBy")? sortBy :"";
  //String defaultModuleTitle = (String)request.getAttribute("defaultModuleTitle");
         //modDefaultText = !GenericValidator.isBlankOrNull(defaultModuleTitle)? defaultModuleTitle: modDefaultText;
  //String disbledText = !GenericValidator.isBlankOrNull(defaultModuleTitle)? "":"disabled";          
  String qString = (String)request.getAttribute("queryStr");
         qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  String noFollowLink = (!GenericValidator.isBlankOrNull(qString)) ? "nofollow" : "";  
  String subjectCode = (String)request.getAttribute("subjectCode");
         subjectCode = !GenericValidator.isBlankOrNull(subjectCode)? subjectCode: "";
  String campusCode = (String)request.getAttribute("campusCode");
         campusCode = !GenericValidator.isBlankOrNull(campusCode)? campusCode: "";
  String studyModeId = (String)request.getAttribute("paramstudyModeValue");
         studyModeId = !GenericValidator.isBlankOrNull(studyModeId)? studyModeId :"All study modes";
  String assessmentId = request.getParameter("assessment-type");
         assessmentId = !GenericValidator.isBlankOrNull(assessmentId)? assessmentId: "";
  String studyLevel = (String)request.getAttribute("paramStudyLevelId");       
         studyLevel = !GenericValidator.isBlankOrNull(studyLevel)? studyLevel :"";
  String resultExists = (String)request.getAttribute("resultExists");       
         resultExists = !GenericValidator.isBlankOrNull(resultExists)? resultExists :"";
  String searchCount = (String)request.getAttribute("universityCount");       
         searchCount = !GenericValidator.isBlankOrNull(searchCount)? searchCount :"";       
  String russellgrpId = (String)request.getAttribute("russellgrpId");
         russellgrpId = !GenericValidator.isBlankOrNull(russellgrpId)? russellgrpId: "";         
  String searchLocation = (String)request.getAttribute("searchLoc");         
         searchLocation = !GenericValidator.isBlankOrNull(searchLocation) && ("england").equalsIgnoreCase(searchLocation) ? "england" : searchLocation;         
         searchLocation = !GenericValidator.isBlankOrNull(searchLocation) && ("united-kingdom").equalsIgnoreCase(searchLocation) ? "All locations" : searchLocation;  
  String removeModuleParameter = (String)request.getAttribute("removeModuleParameter");  
        if(!GenericValidator.isBlankOrNull(removeModuleParameter) && ("YES").equals(removeModuleParameter)){
          module = "";
        }
  String removePostcodeParameter = (String)request.getAttribute("removePostcodeParameter");  
        if(!GenericValidator.isBlankOrNull(removePostcodeParameter) && ("YES").equals(removePostcodeParameter)){
          postCode = ""; 
          distance = "";
        }
 String searchPageType = request.getParameter("clearing");
        searchPageType = !GenericValidator.isBlankOrNull(searchPageType)?searchPageType:"";
 String searchpagelogtype = "";
 if("CLEARING".equalsIgnoreCase(searchPageType)){searchpagelogtype = "clearing-";}   
 String  goApplyFlagFilterValue =  request.getAttribute("goApplyFlagFilterValue") != null ? ((String)request.getAttribute("goApplyFlagFilterValue")).toLowerCase() : "";  
 String locationDisplay = "display:block;";
 String postCodeDisplay = "display:none;";
 String plusMinForLoc = "min";
 String plusMinForPostcode = "plus";
 String selectedField = "loc";
 if((!GenericValidator.isBlankOrNull(request.getParameter("postcode")) && !GenericValidator.isBlankOrNull(request.getParameter("location")))){
   locationDisplay = "display:block;";
   postCodeDisplay = "display:none;";
   plusMinForLoc = "min";
   plusMinForPostcode = "plus";
   selectedField = "loc";
 }else if(!GenericValidator.isBlankOrNull(request.getParameter("postcode"))){
   locationDisplay = "display:none;";
   postCodeDisplay = "display:block;";
   plusMinForLoc = "plus";
   plusMinForPostcode = "min";
   selectedField = "pc";
 }
 String entryReqFilter = request.getAttribute("entryReqFilter") != null ? (String)request.getAttribute("entryReqFilter") : "";
 String link = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();
%>

<div class="fltr lft fl">
 <%--Added close tag for filter in responsive 03_NOV_2015 By Indumathi S--%>
 <div class="filt_cls" onclick="clrFilterInMob('');">
   <a><span>CLOSE</span>
    <img src="<%=new CommonUtil().getImgPath("/wu-cont/images/cmm_close_icon.svg",0)%>" title="close navigation"/>
   </a>
 </div>
 <c:if test="${not empty requestScope.userUCASScore and requestScope.userUCASScore ne 0}">
   <div class="flt_ucscr fl_w100">
     <div class="ucs_rgt fr">
       <div class="uscr_tit">Your UCAS score</div>
       <div class="ucpt">
         <span class="ucspt">${requestScope.userUCASScore} points</span>
         <span class="ucs_edt"><a href="<%=link%>/degrees/wugo/qualifications/search.html?<%=entryReqFilter%>">Edit</a></span>
       </div>
     </div>
   </div>
 </c:if>

   <div class="fltr_in">
     <h2>Filter by:</h2>
     <jsp:include page="/clearing/search/include/deleteFilter.jsp">   
       <jsp:param name="pageFrom" value="<%=pageFrom%>"/>
       <jsp:param name="searchType" value="<%=searchPageType%>"/>
     </jsp:include>
<!-- Go apply flag-->  
     <c:if test="${not empty requestScope.whatuniGOApplyFilterFlag}">
       <div class="sh_hd fl_w100">
         <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('coursetype','coursetypeh1');">
           <h4 id="coursetypeh1" class="rdat actct fl"><span class="fl">COURSE TYPE</span><span id="coursetypeh1_span" class="fr togflt_img tog_min"></span></h4>
         </div>
         <div id="coursetype" class="flt_lt fl_w100" style="display: block;">
           <ul class="sub_lik nw loc_typ crse_type">
             <li><input type="radio" id="allCourseFlag" name="allCourses" value=""/><label for="allCourseFlag" onclick="javascript:multicheckApplyFn('allCourseFlag');">All courses</label></li>
             <c:if test="${requestScope.wugoFlag eq 'ON'}">
               <li class="rdo_goapp"><input type="radio" id="goApplyFlag" name="allCourses" value="y"/>
                 <label for="goApplyFlag" class="gaply_sec" onclick="javascript:goApplyLightBox();">
             <span class="fl rdo_aply">
             <span class="go_txt">GO</span><span class="goaply">APPLY</span>
             </span>
                   <span class="fl_w100">courses only</span>
                 </label>
               </li>
             </c:if>
           </ul>
         </div>
       </div>
     </c:if>
<%--End of go apply flag--%>
     
     <div class="sh_hd fl_w100">
       <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('univDiv','univh1');">
            <h4 id="univh1" class="rdat actct fl">
              <span class="fl">University</span><span id="univh1_span" class="fr togflt_img tog_min"></span>
            </h4>
       </div>
       <div id="univDiv" class="flt_lt fl_w100" style="display:block;">
         <form class="fl_w100" name="uniNameForm" id="uniNameForm" action="" method="post" onsubmit="return uniSearchInResults1(document.getElementById('uniName'), <%=indexOfUrl%>);">
           <input type="text" name="uniName" id="uniName" value="Enter uni name" autocomplete="off"  onkeydown="clearUniNAME(event,this)"  
             onkeyup="autoCompleteUniForSearchResults(event,this)"   
             onclick="clearUniDefaultTEXT(this)" 
             onblur="setUniDefaultTEXT(this)" 
             class="txt_box nw"/>
           <div id="ajxuni_mob"></div>   
           <input type="hidden" id="uniName_hidden" value="" />
           <input type="hidden" id="uniName_id" value="" />
           <input type="hidden" id="uniName_display" value="" />
           <input type="hidden" id="uniName_url" value="" />
           <input type="hidden" id="uniName_alias" value="" /> 
           <input type="button" name="submit" class="go_ic" onclick="javascript:return uniSearchInResults1(document.getElementById('uniName'), <%=indexOfUrl%>)"/>
         </form>
       </div>
     </div>
     
     <c:if test="${not empty subjectList}">
       <div class="sh_hd fl_w100">
         <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('subject','subjecth1');">
            <h4 id="subjecth1" class="rdat actct fl"><span class="fl">SUBJECT</span><span id="subjecth1_span" class="fr togflt_img tog_min"></span></h4>
         </div>
         <div id="subject" class="flt_lt sub_cnt fl_w100" style="display: block;">
           <ul class="sub_lik " id="subject_ul">
             <c:forEach var="subjectList" items="${requestScope.subjectList}" varStatus="rowSub">
               <c:set var="rowValueSub" value="${rowSub.index}"/>
               <%
                 String displayNoneOrBlock = "";
                 if((Integer.parseInt(pageContext.getAttribute("rowValueSub").toString())) == 10 ){%>
                   <li id="subjectmore_li" class="stud"><a onclick="showLiForSubject();" class="u_txtmr fl"><span class="u_txt fl">More</span><span class="mr_plus fl"><img  alt="plus icon" src="<%=CommonUtil.getImgPath("/wu-cont/images/tog_plus.svg",0)%>"></span></a></li>
                 <%}%>
                 <%if((Integer.parseInt(pageContext.getAttribute("rowValueSub").toString())) >= 10  ){
                   displayNoneOrBlock = "style='display:none;'";
                }%>
               <c:if test="${subjectList.subjectTextKey eq subjectCode}">
                 <li class="stud " <%=displayNoneOrBlock%>><span class="u_txt lcn-act1"><span class="act_bg"></span> <c:out value="${subjectList.categoryDesc}"/></span><span class="sub_cunt fr">(<c:out value="${subjectList.collegeCount}"/>)</span></li>
               </c:if>
               <c:if test="${subjectList.subjectTextKey ne subjectCode}">
                 <c:set var="subjectDesc" value="${subjectList.categoryDesc}"/>
                 <li class="stud" <%=displayNoneOrBlock%>>
                   <span class="u_txt"><%if (pageFrom != null && pageFrom.equals("BROWSE_MONEY_PAGE")){%>             
                     <a onclick="searchEventTracking('filter','<%=searchpagelogtype%>subject','<%=pageContext.getAttribute("subjectDesc").toString().toLowerCase()%>');" href="${subjectList.browseMoneyPageUrl}">${subjectList.categoryDesc}</a><%}else if(pageFrom != null && pageFrom.equals("KEWORD_MONEY_PAGE")){%><a onclick="searchEventTracking('filter','<%=searchpagelogtype%>subject','<%=pageContext.getAttribute("subjectDesc").toString().toLowerCase()%>');" href="${subjectList.browseMoneyPageUrl}">${subjectList.categoryDesc}</a><%}%></span><span class="sub_cunt fr">(${subjectList.collegeCount})
                   </span>
                 </li>

               </c:if>                
             </c:forEach>
           </ul>
         </div>
       </div>
     </c:if>
     <div class="sh_hd fl_w100">
       <div class="hdn fl_w100">
         <h4 class="rdat actct loc_drp fl">
           <div class="ucas_selcnt w-300 fl">
             <div class="ucas_sbox">
               <span class="sbox_txt" id="locationspanu1"><%if("loc".equalsIgnoreCase(selectedField)){%> Location <%}else {%>Post code<%}%></span>
               <i class="fa fa-angle-down fa-lg"></i>
             </div>
             <select name="locationOrPostcode" id="locationOrPostcode" class="ucas_slist" onchange="javascript:locationOrPostCode(this);">
               <option value="1" <%if("loc".equalsIgnoreCase(selectedField)){%> selected="selected" <%}%> >Location</option>
               <option value="2" <%if("pc".equalsIgnoreCase(selectedField)){%> selected="selected" <%}%>>Post code</option>
             </select>
           </div>
           <span class="fr togflt_img tog_min" id="location_span" onclick="javascript:toggleMenuClearing('location','location');" <%if("pc".equalsIgnoreCase(selectedField)){%> style="display:none;" <%}%>></span>
           <span class="fr togflt_img tog_min" id="postcode_span" onclick="javascript:toggleMenuClearing('postcode','postcode');"  <%if("loc".equalsIgnoreCase(selectedField)){%> style="display:none;" <%}%>></span>
         </h4>
       </div>
      
       <c:if test="${not empty requestScope.locationRefineList}">
         <div id="location" class="flt_lt fl_w100" style="<%=locationDisplay%>">
           <%String classnamee = "";%>
             <ul class="sub_lik loc loc_typ">   
             <c:forEach var="locationRefineList" items="${requestScope.locationRefineList}"> 
             <c:if test="${not empty  locationRefineList.regionName}">
               <c:if test="${not empty  locationRefineList.topRegionName}">
               <c:set var="topRegion" value="${locationRefineList.topRegionName}"/>  
               <c:set var="searchLocation" value="<%=searchLocation%>"/>    
                  <c:if test="${locationRefineList.locationTextKey eq searchLocation}">                         
                    <li class="stud fl-act">
                      <span class="act_bg"></span>
                      <c:if test="${locationRefineList.regionName eq topRegion}">
                          <span class="u_txt lcn-act1">${locationRefineList.regionName}</span>
                        </c:if>
                        <c:if test="${locationRefineList.regionName ne topRegion}">
                          <span class="u_txt lcn">${locationRefineList.regionName}</span>
                        </c:if>                    
                    </li>
                   </c:if>
                   <c:if test="${locationRefineList.locationTextKey ne searchLocation}">
                     <c:set var="locDesc" value="${locationRefineList.regionName}"/>
                                
                     <li>
                       <span class="u_txt lcn">
                         <c:if test="${locationRefineList.regionName eq topRegion}">
                           <% if (pageFrom != null && pageFrom.equals("BROWSE_MONEY_PAGE")){  %>
                             <a onclick="searchEventTracking('filter','<%=searchpagelogtype%>location','${locDesc.toLowerCase()}');" href="${locationRefineList.locationURL}">${locationRefineList.regionName}</a>
                           <%}else if(pageFrom != null && pageFrom.equals("KEWORD_MONEY_PAGE")){%>
                             <a onclick="searchEventTracking('filter','<%=searchpagelogtype%>location','${locDesc.toLowerCase()}');" href="${locationRefineList.locationURL}">${locationRefineList.regionName}</a>
                           <%}%>
                         </c:if>
                       </span>
                    <c:if test="${locationRefineList.regionName ne topRegion}">
                        <span class="u_txt">
                        <% if (pageFrom != null && pageFrom.equals("BROWSE_MONEY_PAGE")){  %>
                          <a rel="<%=noFollowLink%>" onclick="searchEventTracking('filter','<%=searchpagelogtype%>location','${locDesc.toLowerCase()}');" href="${locationRefineList.locationURL}"><span class="lcn">${locationRefineList.regionName}</span></a>
                        <%}else if(pageFrom != null && pageFrom.equals("KEWORD_MONEY_PAGE")){%>
                           <a rel="nofollow" onclick="searchEventTracking('filter','<%=searchpagelogtype%>location','${locDesc.toLowerCase()}');" href="${locationRefineList.locationURL}"><span class="lcn">${locationRefineList.regionName}</span></a>
                        <%}%>
                        </span>
                        </c:if>
                    </li>
                  
                   </c:if>
                   </c:if>
                 </c:if>
              </c:forEach> 
            </ul>
          </div>      
        </c:if>  
        <div id="postCodeDiv" class="sh_hd fl_w100" style="<%=postCodeDisplay%>">  
          <div id="postcode" class="flt_lt fl_w100" style="display:none;">
            <div class="slider_lst nw">
              <div id="s1" class="flt_lt srch_dis" style="display:block;">              
                <input id="postCodeText" class="txt_box wd" type="text" onkeydown="clearDefaultText1(this, 'Postcode');enableButton('postcodeButton',this, 'Postcode');"
                       maxlength = "10"
                       onfocus="showButton();" 
                       onclick="clearDefaultText1(this,'Postcode');enableButton('postcodeButton',this, 'Postcode');" 
                       onblur="setDefaultText1(this,'Postcode');enableButton('postcodeButton',this, 'Postcode');hideButton();" 
                       onkeyup="setDefaultText1(this,'Postcode');enableButton('postcodeButton',this, 'Postcode');enableDropDown();"
                       onkeypress="clearDefaultText1(this, 'Postcode');enableButton('postcodeButton', this, 'Postcode');javascript:if(event.keyCode==13){return searchResultsURL('postcode');}enableDropDown();"
                       name="postcode" value="Postcode" tabindex="2" title="Postcode" />
                <input type="button" name="submit" id="postcodeButton" class="go_ic sd_icn" disabled="disabled" onclick="javascript:return searchResultsURL('postcode')"/>      
              </div>
              <div class="mls_ctl nw">
                <label class="mls1">Radius</label>               
              </div> 
              <div class="lft_srch disa_rem" id="pstDpDown">
              <div class="drp_val">
                <span class="dpval_txt" id="divPostcode">10 Miles</span>
                <i class="fa fa-angle-down fa-lg"></i>
              </div>
              <select id="dpPostcode" name="dpPostcode" disabled onchange="setSelectedValue(this,'divPostcode', 'hidden_distance');searchResultsURL('postcode');">
                <option value="10">10 miles</option>
                <option value="25">25 miles</option>
                <option value="50">50 miles</option>
                <option value="100">100 miles</option>
                <option value="150">150 miles</option>
              </select>
            </div>
            <script type="text/javascript">loadPostcodeSlider("<%=distance%>");</script>             
            <input id="hidden_distance" name="hidden_distance" type="hidden" value="<%=distance%>"/> 
          </div>       
          
        </div>
      </div>
   </div>     
     <!-- end location-->
      <c:if test="${not empty requestScope.locationList}">
         <div class="sh_hd fl_w100">
           <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('locationType','locationhTypeh1');">
              <h4 id="locationhTypeh1" class="rdat actct fl"><span class="fl">LOCATION TYPE</span><span id="locationhTypeh1_span" class="fr togflt_img tog_plus"></span></h4>
           </div>
           <div id="locationType" class="flt_lt fl_w100" style="display:none;">
             <ul class="sub_lik nw loc_typ">
               <c:forEach var="locationList" items="${locationList}">  
                 <li><input type="checkbox" onclick="javascript:multicheckfn('<c:out value="${locationList.optionTextKey}"/>')" id="<c:out value="${locationList.optionTextKey}"/>"  name="<c:out value="${locationList.optionTextKey}"/>" value="<c:out value="${locationList.optionTextKey}"/>"/><label><c:out value="${locationList.refineDesc}"/></label></li>
               </c:forEach>
             </ul>
           </div>
         </div>
     </c:if>     
     <c:if test="${not empty campustypeList}">
         <div class="sh_hd fl_w100">
            <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('campus','campush1');">
              <h4 id="campush1" class="rdat actct fl"><span class="fl">CAMPUS TYPE</span><span id="campush1_span" class="fr togflt_img tog_plus"></span></h4>
            </div>
            <div id="campus" class="flt_lt fl_w100" style="display:none;">
               <ul class="sub_lik">
               <c:forEach var="campustypeList" items="${campustypeList}">            
                 <c:set var="campusCode" value="<%=campusCode%>"/>
                 <c:if test="${campustypeList.optionTextKey eq campusCode}">
                   <li class="stud fl-act">
                     <span class="act_bg"></span>
                     <c:out value="${campustypeList.refineDesc}"/>
                   </li>
                 </c:if>
                 <c:if test="${campustypeList.optionTextKey ne campusCode}">
                   <c:set var="campusDesc" value="${campustypeList.refineDesc}"/>
                   <c:set var="searchpagelogtype" value="<%=searchpagelogtype%>"/>
                   <li>
                     <span class="u_txt">
                       <a  rel="nofollow" onclick="searchEventTracking('filter','${searchpagelogtype}campus-type','${campusDesc.toLowerCase()}');" href="${campustypeList.browseMoneyageLocationRefineUrl}">${campustypeList.refineDesc}</a>
                     </span>
                   </li>    
                 </c:if>
               </c:forEach>
                </ul>
            </div>
         </div>
     </c:if>
     <c:if test="${not empty requestScope.qualList}">
          <div class="sh_hd fl_w100">
            <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('qualList','qualListh1');">
              <h4 id="qualListh1" class="rdat actct fl"><span class="fl">Study level</span><span id="qualListh1_span" class="fr togflt_img tog_min"></span></h4>
            </div>
            <div id="qualList" class="flt_lt fl_w100" style="display:block;">
              <ul class="sub_lik">
              <c:forEach var="qualList" items="${qualList}">  
              <c:if test="${qualList.refineCode eq requestScope.paramStudyLevelId}">
                      <li class="stud fl-act"><span class="act_bg"></span><c:out value="${qualList.refineDisplayDesc}"/></li>
                   </c:if>
                    <c:if test="${qualList.refineCode ne requestScope.paramStudyLevelId}">                     
                     <li><span class="u_txt"><c:set var="qualDesc" value="${qualList.refineDesc}"/> <a onclick="searchEventTracking('filter','<%=searchpagelogtype%>study-level','${qualDesc.toLowerCase()}');" href="${qualList.browseMoneyageLocationRefineUrl}" >${qualList.refineDisplayDesc}</a></span></li>
                   </c:if>
                 </c:forEach> 
               </ul>             
             </div>
          </div>
     </c:if>
     <c:if test="${not empty requestScope.studyModeList}">
         <div class="sh_hd fl_w100">
            <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('studyMode','studyModeh1');">
              <h4 id="studyModeh1" class="rdat actct fl"><span class="fl">Study mode</span><span id="studyModeh1_span" class="fr togflt_img tog_min"></span></h4>
            </div>
            <div id="studyMode" class="flt_lt fl_w100" style="display:block;">
              <ul class="sub_lik">   
                <c:forEach var="studyModeList" items="${studyModeList}">
                 <c:set var="studyModeId" value="<%=studyModeId%>"/>
                 <c:if test="${studyModeList.refineDesc eq studyModeId}">    
                    <li class="stud fl-act"><span class="act_bg"></span><c:out value="${studyModeList.refineDesc}"/></li>
                   </c:if>
                   <c:if test="${studyModeList.refineDesc ne studyModeId}">                         
                    <li><span class="u_txt"><c:set var="studyModeDesc" value="${studyModeList.refineDesc}"/><a onclick="searchEventTracking('filter','<%=searchpagelogtype%>study-mode','${studyModeDesc.toLowerCase()}');" href="${studyModeList.browseMoneyageLocationRefineUrl}">${studyModeList.refineDesc}</a></span></li>
                  </c:if>
                </c:forEach>
              </ul>             
            </div>
         </div>
     </c:if>
      <input id="ucasTariff_hidden" type="hidden" value=""/>
      <input id="hidden_ucasMax" type="hidden" value=""/> 
      <input id="hidden_ucasMin" type="hidden" value=""/>
      <c:if test="${not empty requestScope.russelList}">
           <div class="sh_hd fl_w100">
              <div class="hdn fl_w100" onclick="javascript:toggleMenuClearing('russell','russellh1');">
                <h4 id="russellh1" class="rdat actct fl"><span class="fl">Russell group</span><span id="russellh1_span" class="fr togflt_img tog_plus"></span></h4>
              </div>
              <div id="russell" class="flt_lt fl_w100" style="display:none;">
                <ul class="sub_lik">
                  <c:forEach var="russelList" items="${requestScope.russelList}">     
                    <li>
                      <c:set var="russellgrpId" value="<%=russellgrpId%>"/>
                      <c:if test="${russelList.optionTextKey eq russellgrpId}">
                        <p class="stud fl-act">
                          <span class="act_bg"></span>
                          <c:out value="${russelList.refineDesc}"/>
                        </p>
                      </c:if>
                      <c:if test="${russelList.optionTextKey ne russellgrpId}">
                        <c:set var="ruselDesc" value="${russelList.refineDesc}"/>
                           <li>
                           <span class="u_txt"> 
                        <a rel="nofollow" onclick="searchEventTracking('filter','<%=searchpagelogtype%>russell','${ruselDesc.toLowerCase()}');" href="<c:out value="${russelList.browseMoneyageLocationRefineUrl}"/>"><c:out value="${russelList.refineDesc}"/></a>
                       </span>
		       </li>
		      </c:if>
                    
                  </c:forEach>
                </ul>
              </div>
              
           </div> 
    </c:if>      
     <form:form action="/*-courses/search" styleId="searchBean" >
        <input type="hidden" name="hidden_q" id="hidden_q" value="<%=q%>" />
        <input type="hidden" name="hidden_subject" id="hidden_subject" value="<%=subject%>" />
        <input type="hidden" name="hidden_university" id="hidden_university" value="<%=university%>" />
        <input type="hidden" name="hidden_module" id="hidden_module" value="<%=module%>" />
        <input type="hidden" name="hidden_locType" id="hidden_locType" value="<%=locType%>" />
        <input type="hidden" name="hidden_locationType" id="hidden_locationType" value="<%=locType%>" />
        <input type="hidden" name="hidden_location" id="hidden_location" value="<%=location%>" />
        <input type="hidden" name="hidden_postCode" id="hidden_postCode" value="<%=postCode%>" />
        <input type="hidden" name="hidden_dist" id="hidden_dist" value="<%=distance%>" />
        <input type="hidden" name="hidden_campusType" id="hidden_campusType" value="<%=campusType%>" />
        <input type="hidden" name="hidden_studyMode" id="hidden_studyMode" value="<%=studyMode%>" />
        <input type="hidden" name="hidden_empRateMax" id="hidden_empRateMax" value="<%=empRateMax%>" />
        <input type="hidden" name="hidden_empRateMin" id="hidden_empRateMin" value="<%=empRateMin%>" />
        <input type="hidden" name="hidden_ucasTarrif_max" id="hidden_ucasTarrif_max" value="<%=ucasTarrifMax%>" />
        <input type="hidden" name="hidden_ucasTarrif_min" id="hidden_ucasTarrif_min" value="<%=ucasTarrifMin%>" />
        <input type="hidden" name="hidden_russell" id="hidden_russell" value="<%=russellGroup%>" />
        <input type="hidden" name="hidden_entryLevel" id="hidden_entryLevel" value="<%=entryLevel%>" />
        <input type="hidden" name="hidden_entryPoints" id="hidden_entryPoints" value="<%=entryPoints%>" />
        <input type="hidden" name="hidden_sortorder" id="hidden_sortorder" value="<%=sortorder%>" />
        <input type="hidden" name="hidden_queryString" id="hidden_queryString" value="<%=qString%>" />
        <input type="hidden" name="hidden_removeModule" id="hidden_removeModule" value="<%=removeModuleParameter%>" />
        <input type="hidden" name="hidden_jacs" id="hidden_jacs" value="<%=jacs%>" />
        <input type="hidden" name="hidden_pageno" id="hidden_pageno" value="<%=pageno%>" />
        <input type="hidden" name="hidden_ucasCode" id="hidden_ucasCode" value="<%=ucasCode%>" />        
        <input type="hidden" name="hidden_resultExists" id="hidden_resultExists" value="<%=resultExists%>" />
        <input type="hidden" name="hidden_searchCount" id="hidden_searchCount" value="<%=searchCount%>" />        
        <%--Added below hidden filters for your pref, 28_Aug_2018 By Sabapathi.S--%>
        <input type="hidden" name="hidden_preferenceType" id="hidden_preferenceType" value="<%=reviewCategoryType%>" />
        <input type="hidden" name="hidden_prefType" id="hidden_prefType" value="<%=reviewCategoryType%>" />
        <input type="hidden" name="hidden_assessment" id="hidden_assessment" value="<%=assessmentType%>" />
        <input type="hidden" name="hidden_rf" id="hidden_rf" value="<%=rf%>" /> 
        <input type="hidden" name="hidden_score" id="hidden_score" value="<%=scoreValue%>" />
        <input type="hidden" name="hidden_applyFlag" id="hidden_applyFlag" value="<%=goApplyFlagFilterValue%>" />
        <input type="hidden" name="hidden_appFlag" id="hidden_appFlag" value="<%=goApplyFlagFilterValue%>" />       
      </form:form>
  </div>
</div>

<script type="text/javascript">
defaultSelectCheckBox("hidden_locType");
defaultSelectCheckBox("hidden_preferenceType");
defaultApplyCheckBox("hidden_appFlag");
defualtOpenFilter('<%=qString%>');
</script>