<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction" autoFlush="true" %>
<%String videoUrl = new CommonFunction().getWUSysVarValue("WUNI_GO_VIDEO_URL");%>
<div id="clrPlayVideo" style="display:none">
  <div class="rvbx_shdw"></div>
  <div class="lbxm_wrp">  
    <div class="ext-cont ins_entrq cedwo_cnt clrdef_cnt vid_lbox">
      <div class="revcls ext-cls"><a onclick="hideVideoClearing('clrPlayVideo');"><img src="<%=new CommonUtil().getImgPath("/wu-cont/images/lbx_white_close_icon.svg",0)%>" alt="Close"></a></div>
      <div class="vid_cnt" id="clr_play_video"></div>
    </div>
  </div> 
</div>

<script type="text/javascript">
  clearingShowVideo();
  function  clearingShowVideo(){
    jQuery('#loadingImg').show();
    jQuery('#videoid').addClass("rvscrl_hid");
    var filePath = document.getElementById("contextJsPath").value+'/js/video/jwplayer.js';
    if (jQuery('head script[src="' + filePath + '"]').length > 0){
    } else {
      dynamicLoadJS(filePath);
    }     
    setTimeout(function(){      
      jwplayerSetup('clr_play_video', '<%=videoUrl%>','', 590, 380, '<%=videoUrl%>');
      jQuery('#clrPlayVideo').show();
      jQuery('#loadingImg').hide();
    }, 1250);
  }
    
  function hideVideoClearing(id){
   jQuery(".rev_lbox").removeAttr("style");
   jQuery(".rev_lbox").addClass("fadeOut").removeClass("fadeIn");  
   jQuery('#videoid').removeClass("rvscrl_hid");
    jwplayer("clr_play_video").stop();	
	  jwplayer('clr_play_video').remove();
    jQuery('#'+id).hide();
  }
</script>