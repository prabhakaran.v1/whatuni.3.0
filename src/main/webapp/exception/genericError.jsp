<!DOCTYPE html>

<%@ page isErrorPage="true" import="java.io.CharArrayWriter, java.io.PrintWriter, java.io.StringWriter"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%@page import="WUI.utilities.SessionData, WUI.utilities.CommonFunction,WUI.utilities.CommonUtil"%>

<%
   String commonCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.css");
   String homeCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.homepage.css");
   String flexSliderCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.flexslider.css");
   String mainHeaderCSS = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);//Added this css for Header changes by Hema.S on 23_OCT_2018_REL
  
%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>      
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">       
  
  <jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
  
  <%@include  file="/include/htmlTitle.jsp"%>   
   <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=mainHeaderCSS%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=commonCssName%>" media="screen" />  
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=homeCssName%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=flexSliderCssName%>" media="screen" />  
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>    
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>    
  <jsp:include  page="/notfound/include/includeErrorResponsive.jsp"/>      
</head>
<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>    
</header>
<!-- Page Content Starts-->    
<section class="main_cnr pt20 pb40">
<div class="ad_cnr">	
  <div class="content-bg error_404">
    <div class="error_desc">
      <a name="top"></a>
      <%String emailSubject   = "Error in whatuni";
      pageContext.setAttribute("emailSubject",emailSubject);
      %>
      <a name="top"></a>
      <h3 class="fnt_lrg">Oh dear!</h3>
      <p class="fnt_lrg">How embarrassing, something seems to have gone a bit wrong.</p>
      <%
        String errorMessage = ""; 
        String messageBody  = "";
        String whatuniEmail = new CommonFunction().getWUSysVarValue("WHAT_UNI_EMAIL");
        if(exception != null){ 
          StringWriter stringWriter = new StringWriter();
          PrintWriter printWriter = new PrintWriter(stringWriter, true); 
          exception.printStackTrace(printWriter); 
          errorMessage    = stringWriter !=null ? stringWriter.toString() : "";
          messageBody     = stringWriter !=null ? String.valueOf(stringWriter).length()>1800?stringWriter.toString().substring(0,1800) : stringWriter.toString() : "";
          messageBody     = messageBody !=null ? messageBody.replaceAll("at "," at %0D") : "";
          messageBody     = messageBody.replaceAll("\"","'");
          stringWriter.close(); printWriter.close();
        }         
        pageContext.setAttribute("messageBody",messageBody);
      %>
    </div>  
    <jsp:include page="/exception/suggest.jsp">
    <jsp:param name="genericErr" value="true"/>
      <jsp:param name="genericErrCnt" value="<%=messageBody%>"/>
    </jsp:include><br/>
    <section class="pcou">
      <div class="content-bg">
        <jsp:include page="/exception/browsePageError.jsp"/>            
      </div>
    </section>  
    <div style="display:none;">
      <h2><spring:message code="uni.errorpage.title"/></h2>
      <p><spring:message code="uni.errorpage.description" arguments="'support.team@whatuni.com' , ${emailSubject} , ''"/></p>
      <p>
        <span><strong><%=exception%></strong></span>
        <a href="#" onclick="document.getElementById('errorDetails').style.display='block';this.style.display='none';" title="View detailed error">View detailed error</a>
      </p>            
      <div id="errorDetails" style="display:none;">
        <%-- <p><% if (exception != null) { out.println(errorMessage); }%></p> --%>
      </div>
      <p>The following pages may be useful in helping you find the information which you are looking for:</p>
      <p><a href="<%=request.getContextPath()%>/home.html" title="Home page">Home page</a></p>
    </div>
    <div style="display:none;">
      <p>If you wish to contact us then please <a href="mailto:support.team@whatuni.com?subject=${emailSubject}">send us an email</a>.</p>
    </div>                
  </div>
</div>
</section>  
<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>
</html>