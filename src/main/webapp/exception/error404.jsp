<!DOCTYPE html>

<%@page import="WUI.utilities.SessionData, WUI.utilities.CommonFunction, WUI.utilities.CommonUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/html.tld" prefix="html"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>

<%
   String commonCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.css");
   String homeCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.homepage.css");
   String flexSliderCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.flexslider.css");
   String mainHeaderCSS = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);//Added this css for Header changes by Hema.S on 23_OCT_2018_REL
   
%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>   
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">       
  
  <jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
  
  <%@include  file="/include/htmlTitle.jsp"%> 
   <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=mainHeaderCSS%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=commonCssName%>" media="screen" />  
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=homeCssName%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=flexSliderCssName%>" media="screen" />  
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>  
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>    
  <jsp:include  page="/notfound/include/includeErrorResponsive.jsp"/>      
</head>
<body>
<%response.setStatus(404);%>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>    
</header>
<!-- Page Content Starts-->    
<section class="main_cnr pt20 pb40">
<div class="ad_cnr">	
  <div class="content-bg error_404">
    <div class="error_desc"> 
    <c:if test="${empty sessionScope.message}">      
        <h3 class="fnt_lrg">Oh dear!</h3>
        <p class="fnt_lrg">How embarrassing, something seems to have gone a bit wrong.</p>
        <input type="hidden" value="<%=(request.getAttribute("reason_for_404") !=null? request.getAttribute("reason_for_404") : "")%>" size="70" />
        <%session.removeAttribute("reason_for_404"); session.removeAttribute("error_code");%>                                 
      </c:if>       
    </div>
    <jsp:include page="/exception/suggest.jsp" /><br/>
    <section class="pcou">
      <div class="content-bg">
        <jsp:include page="/exception/browsePageError.jsp"/>
      </div>
    </section>  
        <c:if test="${not empty sessionScope.message}"> 
        <div id="errorpage">
          <h2>Session expiry</h2>
          <p>Sorry, but the page you requested could not be displayed for the following reason:</p>
          <ul><li>As a security precaution, you were timed out and the page is no longer available.</li></ul>
          <p><a href="<%=request.getContextPath()%>/home.html" title="Home page">Home page</a></p>
          <%session.removeAttribute("message");%>
        </div> 
    </c:if>      
  </div>
</div>
</section> 
<jsp:include page="/jsp/common/wuFooter.jsp"/>
</body>
</html>