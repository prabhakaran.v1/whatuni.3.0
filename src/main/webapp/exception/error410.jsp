<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%@page import="WUI.utilities.SessionData, WUI.utilities.CommonFunction,WUI.utilities.CommonUtil,WUI.utilities.GlobalFunction" %>

<%
   String commonCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.css");
   String homeCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.homepage.css");
   String flexSliderCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.flexslider.css");
   String mainHeaderCSS = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);//Added this css for Header changes by Hema.S on 23_OCT_2018_REL
   
%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>    
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">       
  
  <jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
  
  <%@include  file="/include/htmlTitle.jsp"%> 
   <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=mainHeaderCSS%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=commonCssName%>" media="screen" />  
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=homeCssName%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=flexSliderCssName%>" media="screen" />  
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>  
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>    
  <jsp:include  page="/notfound/include/includeErrorResponsive.jsp"/>    
  
</head>
<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>    
</header>     
<!-- Page Content Starts-->    
<section class="main_cnr pt20 pb40">
  <div class="ad_cnr">	
    <div class="content-bg error_404">    
      <div class="error_desc">
        <h3 class="fnt_lrg">Oh dear, this page has moved...</h3>
        <p class="fnt_lrg">The page you're looking for has packed up its bags and set sail for pastures new.</p>          
        <input type="hidden" value="<%=(request.getAttribute("reason_for_410") !=null? request.getAttribute("reason_for_410") : "")%>" size="70" />
        <%session.removeAttribute("reason_for_410");session.removeAttribute("error_code");%>         
      </div>  
      <jsp:include page="/exception/suggest.jsp"/><br/>              
      <section class="pcou">
        <div class="content-bg">
          <jsp:include page="/exception/browsePageError.jsp"/>
        </div>
      </section>       
    </div>
  </div>        
</section>
<% //Added course title and college id dimensions for insights for 08_MAR_2016 By Thiyagu G.  
   request.setAttribute("cDimCourseTitle", request.getAttribute("courseTitle410") !=null ? new GlobalFunction().getReplacedString((String)request.getAttribute("courseTitle410")) : "");
   request.setAttribute("cDimCollegeId", request.getAttribute("collegeId410") !=null ? (String)request.getAttribute("collegeId410") : "");  
%>
<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>
</html>