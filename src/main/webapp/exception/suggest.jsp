<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction" %>
<%
  String errorType = request.getParameter("genericErr");  
  String errorContent = request.getParameter("genericErrCnt");  
  String emailSubject = "Error in whatuni";
%>
<section class="ltst_art">
<article class="content-bg">  
  <article class="content-bg">
    <div class="flexslider2">
      <div style="overflow: hidden;" class="flexslider" id="f_true">
        <p class="fnt_lbd">If you're feeling at all frustrated, feel free to take it out on a member of our team</p>
        <h2 class="fnt_lbk">Suggest someone for us to sack...</h2>
        <div class="mt30">
          <ul class="slides" id="per_slides">
            <li>
              <div class="ftv1"><%--Upadted 404 page members by Hema.S on 16.07.2018--%>
                <div><a onclick="javascript:setFiredPerson('Dan');"><img id="Dan" alt="Dan" title="Dan" src="<%=CommonUtil.getImgPath("/wu-cont/images/404/dan.png", 1)%>"></a></div>
                <div class="ftc">
                  <a class="btn1 bg_orange fl mt10" onclick="javascript:setFiredPerson('Dan');">DAN<i class="fa fa-long-arrow-right"></i></a>
                </div>
              </div>
            </li>
            <li>
              <div class="ftv2">
                <div><a onclick="javascript:setFiredPerson('Lizzie');"><img id="Lizzie" alt="Lizzie" title="Lizzie" src="<%=CommonUtil.getImgPath("/wu-cont/images/404/lizzie.png", 1)%>"></a></div>
                <div class="ftc">
                  <a class="btn1 bg_orange fl mt10" onclick="javascript:setFiredPerson('Lizzie');">LIZZIE<i class="fa fa-long-arrow-right"></i></a>
                </div>
              </div>
            </li>
            <li>
              <div class="ftv3">
                <div><a onclick="javascript:setFiredPerson('Eleni');"><img id="Eleni" alt="Eleni" title="Eleni" src="<%=CommonUtil.getImgPath("/wu-cont/images/404/eleni.png", 1)%>"></a></div>
                <div class="ftc">
                  <a class="btn1 bg_orange fl mt10" onclick="javascript:setFiredPerson('Eleni');">Eleni<i class="fa fa-long-arrow-right"></i></a>
                </div>
              </div>
            </li>            
            <li>
              <div class="ftv4">
                <div><a onclick="javascript:setFiredPerson('Monisha');"><img id="Monisha" alt="Monisha" title="Monisha" src="<%=CommonUtil.getImgPath("/wu-cont/images/404/monisha.png", 1)%>"></a></div>
                <div class="ftc">
                  <a class="btn1 bg_orange fl mt10" onclick="javascript:setFiredPerson('Monisha');">MONISHA<i class="fa fa-long-arrow-right"></i></a>
                </div>
              </div>
            </li>                        
          </ul>
        </div>
      </div>
      <div class="ermsg">
        <div id="defErr">
          <h3 class="txt_cnr pt35 w100p fl fnt_lbd">Or, if you're feeling generous</h3>
          <p class="txt_cnr mt5 w100p fl fnt_lbd">Return to the <a href="<%=new CommonFunction().getDomainName(request, "N")%>" class="fnt_lbd blue">homepage</a> and let them all keep their jobs.<%=(errorType!=null && "true".equals(errorType)) ? ", " : "."%>
            <%if(errorType!=null && "true".equals(errorType)){%>
              or give them one final chance by sending them an error <a class="fnt_lbd blue" href="mailto:support.team@whatuni.com?subject=<%=emailSubject%>">email</a> to rectify their mistake!
            <%}%>
          </p>
        </div>
        <div id="MonishaErr" style="display:none">
          <h3 class="txt_cnr pt35 w100p fl fnt_lbd">Unlucky Monisha!</h3>
          <p class="txt_cnr mt5 w100p fl fnt_lbd">That's just too bad, Monisha's gonna spend the rest of the day throwing sass around the office.<br/>Return to the <a href="<%=new CommonFunction().getDomainName(request, "N")%>" class="fnt_lbd blue">homepage</a> and let them all keep their jobs.<%=(errorType!=null && "true".equals(errorType)) ? ", " : "."%>
            <%if(errorType!=null && "true".equals(errorType)){%>
              or give them one final chance by sending them an error <a class="fnt_lbd blue" href="mailto:support.team@whatuni.com?subject=<%=emailSubject%>">email</a> to rectify their mistake!
            <%}%>
          </p>
        </div>
        <div id="DanErr" style="display:none">
          <h3 class="txt_cnr pt35 w100p fl fnt_lbd">Unlucky Dan!</h3>
          <p class="txt_cnr mt5 w100p fl fnt_lbd">Uh-oh poor Dan, he's gonna head home and console himself with chilli con carne and granola. Such is life.<br/>Return to the <a href="<%=new CommonFunction().getDomainName(request, "N")%>" class="fnt_lbd blue">homepage</a> and let them all keep their jobs.<%=(errorType!=null && "true".equals(errorType)) ? ", " : "."%>
            <%if(errorType!=null && "true".equals(errorType)){%>
              or give them one final chance by sending them an error <a class="fnt_lbd blue" href="mailto:support.team@whatuni.com?subject=<%=emailSubject%>">email</a> to rectify their mistake!
            <%}%>
          </p>
        </div>
        <div id="EleniErr" style="display:none">
          <h3 class="txt_cnr pt35 w100p fl fnt_lbd">Unlucky Eleni!</h3>
          <p class="txt_cnr mt5 w100p fl fnt_lbd">She's now throwing a massive tantrum in the office. And we have to deal with it. Thanks for that.<br/>Return to the <a href="<%=new CommonFunction().getDomainName(request, "N")%>" class="fnt_lbd blue">homepage</a> and let them all keep their jobs.<%=(errorType!=null && "true".equals(errorType)) ? ", " : "."%>
            <%if(errorType!=null && "true".equals(errorType)){%>
              or give them one final chance by sending them an error <a class="fnt_lbd blue" href="mailto:support.team@whatuni.com?subject=<%=emailSubject%>">email</a> to rectify their mistake!
            <%}%>
          </p>
        </div>
        <div id="LizzieErr" style="display:none">
          <h3 class="txt_cnr pt35 w100p fl fnt_lbd">Unlucky Lizzie!</h3>
          <p class="txt_cnr mt5 w100p fl fnt_lbd">Lizzie will leave and go pursue a career as a cheerleader. Dreams are made on gym mats!<br/>Return to the <a href="<%=new CommonFunction().getDomainName(request, "N")%>" class="fnt_lbd blue">homepage</a> and let them all keep their jobs.<%=(errorType!=null && "true".equals(errorType)) ? ", " : "."%>
            <%if(errorType!=null && "true".equals(errorType)){%>
              or give them one final chance by sending them an error <a class="fnt_lbd blue" href="mailto:support.team@whatuni.com?subject=<%=emailSubject%>">email</a> to rectify their mistake!
            <%}%>
          </p>
        </div>
      </div>      
    </div>
  </article>  
</article>
</section>