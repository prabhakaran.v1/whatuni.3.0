<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%-- did you mean --%>
<c:if test="${not empty listOfDidYouMeanKeywordDetails}">                      
  <div class="d_mean">
    <strong>Did you mean: </strong>
    <c:forEach var="didYouMeanKeywordDetail" items="${listOfDidYouMeanKeywordDetails}">
      <a rel="nofollow" href="<c:out value="${didYouMeanKeywordDetail.urlForDidYouMeanKeyword}"/>"><c:out value="${didYouMeanKeywordDetail.didYouMeanKeyword}"/></a>&nbsp;&nbsp;
    </c:forEach>                            
  </div> 
</c:if>   