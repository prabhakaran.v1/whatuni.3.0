<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonFunction, WUI.utilities.CommonUtil" %>

<% String subjectname = (String) request.getAttribute("err_subject_name");
              subjectname  = subjectname !=null && !subjectname.equalsIgnoreCase("null") && subjectname.trim().length()>0 ? new CommonUtil().toTitleCase(subjectname) : "";
       String studylevelname = (String) request.getAttribute("err_study_level_name");
              studylevelname  = studylevelname !=null && !studylevelname.equalsIgnoreCase("null") && studylevelname.trim().length()>0 ?  new CommonUtil().toLowerCase(studylevelname)  : "";
       String location = (String) request.getAttribute("err_location");
              location  = location !=null && !location.equalsIgnoreCase("null") && location.trim().length()>0 ? new CommonFunction().replacePlus(location.trim()) : "";
       String providername = (String) request.getAttribute("err_provider_name");
              providername  = providername !=null && !providername.equalsIgnoreCase("null") && providername.trim().length()>0 ? providername.trim() : "";     
       String noindexfollow      =   request.getAttribute("noindex") !=null ? "TRUE"  : "FALSE";   
               noindexfollow      =   request.getAttribute("unihomefrmsearch") !=null ? "noindex,nofollow"  : noindexfollow;
               noindexfollow      =   request.getAttribute("otherindex") !=null ? String.valueOf(request.getAttribute("otherindex"))  : noindexfollow;
      String clSearchBarForNoResults = (String)request.getAttribute("clearingSearchBar");
      String tmpUcasCode = (request.getAttribute("ucasCodepresent") !=null && !request.getAttribute("ucasCodepresent").equals("null") && String.valueOf(request.getAttribute("ucasCodepresent")).trim().length()>0 ? String.valueOf(request.getAttribute("ucasCodepresent")) : "");
      
      String noResults = "";
      //wu415_20120821 Added by Sekhar K for showing other qualification in no results message.
      String otherQualification = "";
      if(clSearchBarForNoResults != null){
      noResults = "Y";
        otherQualification = "Degree or HND/HNC";
        if("degree".equalsIgnoreCase(studylevelname)){
          otherQualification = "HND/HNC or Foundation degree";
        }else if("hnd/hnc".equalsIgnoreCase(studylevelname)){
          otherQualification = "Degrees or Foundation degree";
        }
      }else{
        otherQualification = "HND/HNC or Postgraduate";
        if("hnd/hnc".equalsIgnoreCase(studylevelname)){
          otherQualification = "Degree or Foundation degree";
        }else if("postgraduate".equalsIgnoreCase(studylevelname)){
          otherQualification = "Degrees or HND/HNC";
        }
      }
    %>
 
   
    

  <body id="www-whatuni-com">
    <%-- added for 10th March Release --%>
    <% 
        int stind1 = request.getRequestURI().lastIndexOf("/");
        int len1 = request.getRequestURI().length();
        String page_name = request.getRequestURI().substring(stind1+1,len1);
               page_name = page_name !=null ? page_name.toLowerCase() : page_name;
        //String channel  = "/hc/hotcourses/whatuni/"+page_name;
        String channel  = "whatuni";
        String serverName = request.getServerName() !=null ? request.getServerName().toLowerCase() : "";
        String pageType = request.getParameter("pagemessage");
        
       
     %>  
  
      <!--WEBSIDESTORY CODE HBX1.0 (Universal)--> 
      <jsp:include page="/jsp/common/includeIconImg.jsp"/><%--Icon images added by Prabha on 31_May_2016--%>

<div class="clipart">
 <div id="content-bg">
    <div id="wrapper">
     <jsp:include page="/jsp/common/wuHeader.jsp"/>
  	      <div id="content-blk">
 	          <div id="content-left">
	 	           <div id="reviewslarge">
		               <h2 class="hs-1 uni-info mb-25"><strong>

                     <span class="error_404"><%  if(tmpUcasCode!=""){%><%=tmpUcasCode%> <%}else{%><%=subjectname%> <%=studylevelname%> <%}%> <%if("Y".equals(noResults)){%>clearing<%}%> courses
                      <c:if test="${not empty requestScope.err_provider_name}"> in <%=providername%><c:if test="${not empty requestScope.err_location}"> <%=location%></c:if></c:if>
                      <c:if test="${empty requestScope.err_provider_name}"><c:if test="${not empty requestScope.err_location}"> in <%=location%></c:if></c:if></span>
                  </strong></h2>
          		     <div class="content">
                   <%if("Y".equals(noResults)){%>
                      <p>
                          Sorry, we don't have any Clearing <%=studylevelname%> courses listed for that subject yet. You can try selecting a different study level like <%=otherQualification%> from the drop down menu in the search bar. You could also try broadening your search, or searching for a similar subject. If you can't find the Clearing course you need, you might still be able to find Clearing course information on the UCAS website. Don't give up!                       
                      </p>
                   <%}else{%>
                   <c:if test="${empty requestScope.suggestionWords}">
                       <p>
                         Sorry, we couldn't find any results for your search on <%  if(tmpUcasCode!=""){%><%=tmpUcasCode%> <%}else{%><%=subjectname%> <%=studylevelname%><%}%> courses
                        <c:if test="${not empty requestScope.err_provider_name}"> in <%=providername%><c:if test="${not empty requestScope.err_location}"> <%=location%></c:if></c:if><c:if test="${empty requestScope.err_provider_name}"><c:if test="${not empty requestScope.err_location}"> in <%=location%></c:if></c:if>. If you were looking for a different level of study, like <%=otherQualification%>, you should run your search again with that study level selected.
                        </p>
                        <p>
                         We update all courses regularly, but this is a big decision and we want to help you as much as we can. If you'd like to ask us why you can't find the courses you're looking for, you can <a href="mailto:webeditor@whatuni.com">send our Web Editor an email.</a> 
                        </p>
                        <p>
                        In the meantime, please try a new search or browse through our <a href="<%=request.getContextPath()%>/courses/browse.html" title="course list">course list</a>.
                        </p>
                    </c:if>
                    <c:if test="${not empty requestScope.suggestionWords}">
                       <p>Sorry, we couldn't find any results for your search on <%=subjectname%> <%=studylevelname%> courses <c:if test="${not empty requestScope.err_provider_name}"> in <%=providername%><c:if test="${not empty requestScope.err_location}"> <%=location%></c:if><c:if test="${empty requestScope.err_provider_name}"> in <%=location%></c:if></c:if>. </p>
                       <p>Did you mean;</p>
                        ${requestScope.suggestionWords}
                        <p> We update all courses regularly, after all this is a big decision. With our next update we may have <%=subjectname%> <%=studylevelname%> courses<c:if test="${not empty requestScope.err_provider_name}"> in/at <%=providername%><c:if test="${not empty requestScope.err_location}"> <%=location%></c:if></c:if><c:if test="${empty requestScope.err_provider_name}" > in/at <c:if test="${not empty requestScope.err_location}"><%=location%></c:if></c:if>, so please do return to the site.</p>    

                        <p>However in the meantime, please try again by broadening your choices of <%=subjectname%> <%=studylevelname%> courses by:</p>
                          <ul class="w_search">
                             <li>using our unique <a href="<%=request.getContextPath()%>/searchHome.html" title="university course finder and comparison tool">university course finder and comparison tool</a></li>
                              <li>browsing through our <a href="<SEO:SEOURL pageTitle="unibrowse">none</SEO:SEOURL>" title="college or university list">college or university list</a></li>
                              <li>browsing through our <a href="<%=request.getContextPath()%>/courses/browse.html" title="course list">course list</a></li>
                          </ul> 
                          <p>Please note reviews of <%=subjectname%> <%=studylevelname%> courses 
                            <c:if test="${not empty requestScope.err_provider_name}"> in <%=providername%><c:if test="${not empty requestScope.err_location}"> <%=location%></c:if></c:if>
                            <c:if test="${empty requestScope.err_provider_name}"><c:if test="${not empty requestScope.err_location}"> in <%=location%></c:if></c:if> are the subjective opinions of Whatuni members and not of Whatuni.com</p>
                    </c:if>
                    
                     <%}%>
                     </div>
                  </div>
	       </div>
            </div>
    	 <div id="content-right">
<%--
            <div class="times-blk">
            <h2 class="hc-1 mb-10"><bean:message key="wuni.homepage.pod.head.finduni"/> </h2>
            <jsp:include page="/include/findUniversityReview.jsp" /> 
            </div>
--%>
            <%-- Smilialr [SUBJECT] [QUALIFICATION] courses---%>
            <jsp:include page="/courseJourney/locationSubjectPod.jsp" >   
              <jsp:param name="displayStyle" value="block" />
            </jsp:include>             
        </div>
       </div>
</div>
</div>
<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>
</html>