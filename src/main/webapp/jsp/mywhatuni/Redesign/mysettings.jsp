<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction"%>
<%
  CommonFunction common = new CommonFunction();
  String clearingonoff = common.getWUSysVarValue("CLEARING_ON_OFF"); 
%>

<body>
  <header class="clipart">
    <div class="">
      <div class="content-bg">
        <div id="desktop_hdr">
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>                
      </div>      
    </div>
  </header>
  <input type="hidden" id="functionCallFlag" value="no"/>
  <div class="my_sett cmm_uprof">
    <div class="ad_cnr">
      <div class="content-bg">
        <div class="subCon cf">
          <jsp:include page="/jsp/mywhatuni/Redesign/include/leftMenu.jsp"/>
          <div class="Rht_Cnr">
            <c:if test="${not empty sessionScope.userInfoList}">
           <!-- Need to include user info pod.jsp here -->
           <jsp:include page="/jsp/mywhatuni/Redesign/include/userInfoPod.jsp"/>
            </c:if>
        <%--     <%if("ON".equalsIgnoreCase(clearingonoff)){%>
           <div class="set_pri cf app_redire">
             <div class="cmm_unilst fl_w100">
              <a onclick="checkApplicationPageStatus();">Application Page <i class="fa fa-long-arrow-right"></i></a>
          </div>
          </div>
            <%}%> --%>
            <%if("OFF".equalsIgnoreCase(clearingonoff)){%>
            <c:if test="${not empty requestScope.userConfirmedList}">
            <c:forEach var="userConfirmedList" items="${requestScope.userConfirmedList}">
             <div class="set_pri cf">
    <div class="fl sub_cnr">
      <div class="sett_info Pri">
        <h3 class="info_tit"><span class="Tit">Your application</span> 
          <i class="fa fa-plus-circle fnt_24 color1 fr fa-minus-circle"></i>
          </h3>
        <div class="seton_cont">
          <div class="cmm_unilst fl_w100">
  <div class="fl uni-logo col_lft">
    <a><img src="<%=CommonUtil.getImgPath("",0)%>${userConfirmedList.collegeLogo}>" alt=""></a>
  </div>
    <div class="fl col_rgt">
        <div class="rgt_col_lft">
          <h3 class="un-tit">${userConfirmedList.collegeDisplayName}</h3>
          <h6 class="sub-tit">${userConfirmedList.courseTitle}</h6>
        </div>
        <div class="rgt_col_rgt">
          <div class="fx">
          <div class="fx-bx">
          <h5>Completed Application</h5>
          <h6>${userConfirmedList.userSubmittedDate}</h6>
          </div>              
          </div>
        </div>
    </div>
</div>
        </div>
      </div>
    </div>
  </div>
          </c:forEach>
          </c:if>
            <%}%>
            <jsp:include page="/jsp/mywhatuni/Redesign/include/mailingPreferencesPod.jsp"/>
            <jsp:include page="/jsp/mywhatuni/Redesign/include/privacyPod.jsp"/> 
            <!--Added submit button Jan-27-16 Indumathi.S-->
            <div class="subs_cnr mt30" id="off_postop" style="display: none;"> 
              <div class="content-bg">
                <input type="submit" id="saveDetails" value="Save changes" class="btn_view" onclick="return profileSubmitValidations();">
              </div>
              <span id="off_postop1"></span>
              <span id="off_postop2"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <jsp:include page="/jsp/common/wuFooter.jsp" /> 
  </body>