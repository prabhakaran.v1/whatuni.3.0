<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil" %>
<%
  String catPrivacyHid = request.getAttribute("catPrivacyFlag") != null ? request.getAttribute("catPrivacyFlag").toString() : "";
%>
<div class="set_pri cf" style="display:none;">
  <div class="fl sub_cnr">
    <div class="sett_info Pri">
      <h3 class="info_tit"><span class="Tit">Privacy</span> 
        <i class="fa fa-plus-circle fnt_24 color1 fr fa-minus-circle"></i>
      </h3>
      <div class="seton_cont">
        <div class="sett_cnr">
          <fieldset class="row-fluid row1 mychk_lt">								
            <span class="ms_txtbld">Who can see my activity</span>										
            <fieldset class="pt10 w100p fr">	
              <label class="ms_rem" for="friendsActivity">My school friends and Facebook connections can see my activity on Whatuni</label>
            </fieldset>	
          </fieldset>	
          <fieldset class="mychk_rt">
            <form>
              <div class="controls">
                <input class="slideCheck" id="friendsActivity"  type="checkbox">
                <label for="friendsActivity" class="slideCheckDiv"></label>
              </div>
            </form>
          </fieldset>	
        </div>        
      </div>
      <!--Addeded ON/OFF privacy flag to update the ca track flag-->
      <div class="seton_cont" style="display:none;"> 
        <div class="sett_cnr">
          <fieldset class="row-fluid row1 mychk_lt">								
            <span class="ms_txtbld"></span>										
            <fieldset class="pt10 w100p fr">	
              <label class="ms_rem" for="catActivity">My Career Advisor/Teacher at my school can see my activity on Whatuni</label>
            </fieldset>	
            <fieldset class="pt10 w100p fr" id="remainderMailText">	
              <label class="ms_rem" for="catActivity">Please note, if you would like them to be able to send you helpful reminders about uni research or upcoming deadlines you will also need to have email reminders switched on above</label>
            </fieldset>
          </fieldset>	
          <fieldset class="mychk_rt">
            <form>
              <div class="controls">
                <input class="slideCheck" id="catActivity" type="checkbox" onclick="setCatoolActvityFlag(this);" />
                <label for="catActivity" class="slideCheckDiv"></label>
                <input type="hidden" name="catActivityHidden" id="catActivityHidden" value="<%=catPrivacyHid%>"/>
              </div>
            </form>
          </fieldset>	
        </div>
      </div>      
    </div>	
  </div>
</div>
<jsp:include page="/jsp/mywhatuni/Redesign/include/downloadMyData.jsp"/>

<!-- Email updated from universities -->
<c:if test="${not empty requestScope.userClientDetails}">
<div class="set_pri cf dwn_pdf eenq_uni">
      <div class="fl sub_cnr">
        <div class="sett_info Pri">
          <h3 class="info_tit" id="dlData"><span class="Tit">Email updates from Universities</span> 
            <i class="fa fa-plus-circle fnt_24 color1 fr fa-minus-circle"></i>
          </h3>
          <div class="seton_cont">
            <div class="sett_cnr">
              <fieldset class="row-fluid row1 mychk_lt">
                <fieldset class="w100p fr">
                  <p class="ms_rem" for="friendsActivity">Using our website you have indicated that you would like to receive email updates from the below universities. To update your mailing preference with any of these universities you will need to contact the university directly. Please refer to the university's privacy notice for further instructions.</p>
                </fieldset>
              </fieldset>
              <fieldset class="row-fluid" id="download now">
               <ul class="eeuniv_lst fl_w100">
                <c:forEach var="userClientDetails" items="${requestScope.userClientDetails}">
                 <li>
                   <span class="unme_lst fl_w100" title="${userClientDetails.collegeName}">${userClientDetails.collegeName}</span>
                   <a title="${userClientDetails.collegeName}" target="_blank" href="${userClientDetails.collegeURL}" class="fl">Click here to review privacy notice</a>
                    <span class="unme_lst fl_w100" title="${userClientDetails.userOptDate}">Subscribed on  ${userClientDetails.userOptDate}</span>
                  </li>
                 </c:forEach>                                           
               </ul>       
              </fieldset>
            </div>
          </div>
        </div>
      </div>
    </div>
</c:if>
<!-- Email updated from universities -->

<%
String friendsPrivacyFlag = request.getAttribute("friendsPrivacyFlag") != null ? request.getAttribute("friendsPrivacyFlag").toString() : "N";
String catPrivacyFlag = request.getAttribute("catPrivacyFlag") != null ? request.getAttribute("catPrivacyFlag").toString() : "N";
%>
<script type="text/javascript" language="javascript">
  checkAndEnabledPrivacy('<%=friendsPrivacyFlag%>','friendsActivity');
  checkAndEnabledPrivacy('<%=catPrivacyFlag%>','catActivity');
  enabledTextMessage();
</script>
