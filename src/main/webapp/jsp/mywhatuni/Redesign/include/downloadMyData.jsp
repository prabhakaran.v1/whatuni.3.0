<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="set_pri cf dwn_pdf">
    <div class="fl sub_cnr">
      <div class="sett_info Pri">
        <h3 class="info_tit" id="dlData"><span class="Tit">Download my data</span> 
          <i class="fa fa-plus-circle fnt_24 color1 fr fa-minus-circle"></i>
          </h3>
            <div class="seton_cont">
              <div class="sett_cnr">
                <fieldset class="row-fluid row1 mychk_lt">
                  <fieldset class="w100p fr">
                    <p class="ms_rem" for="friendsActivity">When you register and use our services, we collect certain information from you, like your full name and email address. Check out our <a href="/wu-cont/privacy.htm">Privacy Notice</a> for more details about the information that we collect and retain.</p>
                  </fieldset>
                  <c:if test="${hideDownloadBtnFlag ne 'Y'}">
                  <fieldset class="w100p fr">
                    <p class="ms_rem" for="friendsActivity">You can download a copy from here to access your data and this will only be accessible to you.</p>
                  </fieldset>
                </fieldset>
                 <fieldset class="row-fluid" id="download now"> 
                  <a onclick="GAInteractionEventTracking('download', 'GDPR', 'download my data', 'registered', ''); clickButton()" id="btnLogin" title="Download now" class="btn1 blue mt20 w150 fl">
                  <span id="formButtonText">DOWNLOAD NOW</span></a> 
                </fieldset>
                <fieldset class="row-fluid" id="download button" style="display:none;"> 
                  <a class="btn1 blue mt20 w150 fl grey">
                  <span id="formButtonText">DOWNLOADED</span></a> 
                </fieldset>
                <%--Handled pdf download error by sangeeth.S for FEB_12_19 rel--%>
                <c:if test="${not empty sessionScope.pdfErrorMsg}"> 
                  <c:if test="${sessionScope.pdfErrorMsg eq 'error'}">
                    <fieldset class="row-fluid" id="pdfErrMsg">
                      <p class="err"><spring:message code="wuni.download.pdf.error.msg"/></p>
                       <%                        
                            session.removeAttribute("pdfErrorMsg");                         
                        %>
                    </fieldset>   
                  </c:if>
                </c:if>
                <fieldset class="row-fluid"> 
                  <p class="ms_rem">*Please make sure you have your popup blocker disabled before trying to download the PDF.</p>
                </fieldset>
                </c:if>
                <c:if test="${hideDownloadBtnFlag eq 'Y'}">
                <fieldset class="row-fluid"> 
                  <p class="ms_rem">This functionality is currently unavailable. Please try later</p>
                </fieldset>               
                </c:if>
              </div>
            </div>
          </div>
        </div>
      </div>