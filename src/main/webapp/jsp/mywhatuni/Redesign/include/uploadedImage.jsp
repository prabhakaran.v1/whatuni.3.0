<%@ page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction"%>

<script language="Javascript">
  window.onload = hideLoader;
  //
	jQuery(function($) {
		$('#target').Jcrop({
			onSelect : setCoordinates
		});
	});

	function setCoordinates(c) {
		document.myForm.x.value = c.x;
		document.myForm.y.value = c.y;
		document.myForm.w.value = c.w;
		document.myForm.h.value = c.h;
	};
	function checkCoordinates() {
		if (document.myForm.x.value == "" || document.myForm.y.value == "") {
			alert("Please select a crop region");
			return false;
		} else {
    var x;    var y;
    var w;    var h;
    var orgImagePath;   
    x = document.getElementById("x").value;
    y = document.getElementById("y").value;
    w = document.getElementById("w").value;
    h = document.getElementById("h").value;
    orgImagePath = document.getElementById("orgImagePath").value;              
    var finaUrl = "/degrees/mywhatuni.html?butValue=cropprdImage";
    document.getElementById("myForm").action = finaUrl;
	   document.getElementById("myForm").submit();
		}
	} 
</script>

<%
String imagePath = request.getAttribute("imagePath") !=null ? request.getAttribute("imagePath").toString() : "/wu-cont/images/blank_photo.png";
       imagePath = new CommonFunction().getWUSysVarValue("USER_IMAGE_UPLOAD_PATH") + imagePath;
%>

<body>
<header class="clipart">
  <div class="">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
</header>

<div class="content-bg">
    <div class="crop_cnr my_sett">
      <div class="crop_ph">
        <span class="loading_icon" id="load_icon" style="position:absolute; z-index:1; display:block"><img src="<%=new CommonUtil().getImgPath("", 0)%>/wu-cont/images/ldr.gif"/></span><%-- Yogeswari :: 19.05.2015 :: Added loadder --%>
        <img src="<%=new CommonUtil().getImgPath("", 0)%><%=imagePath%>" id="target" />
      </div>
      <div class="crop_btn">
        <form name="myForm" id="myForm" action="" method="post" onsubmit="return checkCoordinates();" ondblclick="return checkCoordinates();">
          <input type="hidden" name="x" id="x" value=""/>
          <input type="hidden" name="y" id="y" value=""/>
          <input type="hidden" name="w" id="w" value=""/>
          <input type="hidden" name="h" id="h" value=""/>
          <input type="hidden" name="orgImagePath" id="orgImagePath" value="<%=imagePath%>"/>
          <input type="submit" value="Crop Image" class="btn_view"/>
          <a class="btn_view" href="/degrees/mywhatuni.html">Cancel</a>
        </form>
      </div>
    </div>
</div>
<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>