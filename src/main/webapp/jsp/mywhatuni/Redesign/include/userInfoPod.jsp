<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, com.wuni.advertiser.ql.form.QuestionAnswerBean, WUI.utilities.CommonFunction"%>
<%
   CommonFunction common = new CommonFunction();
   StringBuffer selectIds = new StringBuffer();
   String userId = new WUI.utilities.SessionData().getData(request, "y");
   String encrytedUserId = common.getEncryptedOrDecryptedUserId(userId, null);
   String imagePath = request.getAttribute("imagePath") !=null ? request.getAttribute("imagePath").toString() : "/wu-cont/images/blank_photo.png";
   String croppedImage = request.getAttribute("croppedImage") !=null ? request.getAttribute("croppedImage").toString() : "/wu-cont/images/blank_photo.png";
   String[] yoeArr = common.getYearOfEntryArr();
   String YOE_1 = yoeArr[0];
   String YOE_2 = yoeArr[1];
   String YOE_3 = yoeArr[2];
   String YOE_4 = yoeArr[3];
%>
<div class="set_per cf">
 <c:if test="${not empty requestScope.updatesuccess}">
  <div class="pf-su"><i class="icon-ok mr5"></i> Profile Updated</div>
 </c:if>
 <c:if test="${empty requestScope.updatesuccess and not empty requestScope.formUpdate}">
   <div class="pf-su" style="color:#CF2326">Oh Dear, something&rsquo;s missing, please check your details below</div>
 </c:if>
 <div class="fl sub_cnr">
  <div id="jsFormValidationErrors" class="red"></div>   
  <html:form name="mywhatunibean" action="/degrees/mywhatuni.html" onsubmit="return getGradesEntryPoints();" enctype="multipart/form-data" id="mywhatuniId" modelAttribute="mywhatunibean">
   <!--Basic info Starts-->
   <h3 class="info_tit">
		<span class="Tit">Basic info</span>
		  <i class="fa fa-minus-circle fnt_24 color1 fr"></i>
	   </h3>
   <div class="sett_cnr show_off">
    <div class="sett_info Bas">
	  
     <div class="crm_con">
      <div class="row-fluid row1" id="userPic">
       <div class="pic_upd_cnr">
        <span for="yourpic" class="lbco pb10">YOUR PIC </span>         
        <div class="pic_upd">
         <img alt="" id="userPhoto" src="<%=croppedImage%>" class="sett_photo"></img>
         <!--<img alt="" id="userPhoto" src="/degrees<%=croppedImage%>" class="sett_photo"></img>-->
        </div>
       </div>               
       <div class="pic_upl">        
        <span class="btn_view">add an image
          <input type="file" name="userPhotoUpload" accept="image/*" id="userPhotoUpload" class="ch_ph" onchange="uploadUserImageAjax(this);" />          
        </span>
        <span class="loading_icon" id="load_icon" style="position:absolute; z-index:1; display:none"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span><%-- Yogeswari :: 19.05.2015 :: Added loadder --%>
       </div>       
       <div class="err" id="userPicErrMsg" style="display: none;"></div>
      </div>       
      <fieldset class="row-fluid row2">
       <fieldset class="w50p fl" id="userFName">
        <label for="fname" class="lbco">FIRST NAME*</label>
        <html:input path="firstName" id="firstName"
                   onkeypress="clearDefaultProfile(this.id, 'First name');"
                   onclick="clearDefaultProfile(this.id, 'First name');"
                   onblur="profileValidations(this.id); setDefaultProfile(this.id, 'First name');"
                   onkeyup="setUnsavedFlag();"
                   cssClass="ql-inpt" maxlength="40"/>
           <div class="err" id="firstNameErrMsg" style="display: none;">
         <p>
          <html:errors path="firstName"/>
         </p>
        </div>
       </fieldset>
       <fieldset class="w50p fr" id="userLName">
        <label for="lname" class="lbco">LAST NAME*</label>
        <html:input path="lastName" id="lastName" 
                   onkeypress="clearDefaultProfile(this.id, 'Last name');"
                   onclick="clearDefaultProfile(this.id, 'Last name');"
                   onblur="setDefaultProfile(this.id, 'Last name'); updateSuccessMessage(this);"
                   onkeyup="setUnsavedFlag();"
                   cssClass="ql-inpt" maxlength="40"/>
        <div class="err" id="lastNameErrMsg" style="display: none;">
         <p>
          <html:errors path="lastName"/>
         </p>
        </div>
       </fieldset>
      </fieldset>
       
      <fieldset class="row-fluid row3">
       <label for="email" class="lbco">EMAIL*</label>
       <fieldset class="w100p">
        <input type="text" name="emailAddress"
               value='${mywhatunibean.email}'
               class="ql-inpt" disabled="disabled"></input>
       </fieldset>
       <div class="w100p mt10">
        <a rel="nofollow" title="Reset password" class="link_blue fnt_lrg fnt_14" id="changePasswordLink" href="/degrees/resetPassword.html?method=resetPassword&token=<%=encrytedUserId%>">Reset password</a>        
       </div>
      </fieldset>
       
      <fieldset class="row-fluid row4" id="yoeFeilds">
       <label class="lbco">WHEN WOULD YOU LIKE TO START?*</label>
       <fieldset class="ql-inp pt10" id="yoeFeild">
        <span class="ql_rad">
         <html:radiobutton path="yeartoJoinCourse"
                     value="<%=YOE_1%>" onclick="profileValidations(this.id);" 
                     id="yeartoJoinCourse1" cssClass="check"/>
         <label for="yeartoJoinCourse1">
          <%=YOE_1%>
         </label>
        </span>
        <span class="ql_rad">
         <html:radiobutton path="yeartoJoinCourse"
                     value="<%=YOE_2%>" onclick="profileValidations(this.id);" 
                     id="yeartoJoinCourse2" cssClass="check"/>
         <label for="yeartoJoinCourse2">
          <%=YOE_2%>
         </label>
        </span>
        <span class="ql_rad">
         <html:radiobutton path="yeartoJoinCourse"
                     value="<%=YOE_3%>" onclick="profileValidations(this.id);" 
                     id="yeartoJoinCourse3" cssClass="check"/>
         <label for="yeartoJoinCourse3">
          <%=YOE_3%>
         </label>
        </span>
        <span class="ql_rad">
         <html:radiobutton path="yeartoJoinCourse"
                     value="<%=YOE_4%>" onclick="profileValidations(this.id);" 
                     id="yeartoJoinCourse4" cssClass="check"/>
         <label for="yeartoJoinCourse4">
          <%=YOE_4%>
         </label>
        </span>
        <input type="hidden" name="yeartoJoinCourse1" value="<%=YOE_1%>"
               id="yeartoJoinCourse"></input>        
       </fieldset>
       <div class="err" id="yoeErrMsg" style="display: block;"></div>
      </fieldset>      
      <div class="err" id="yeartoJoinCourseErrMsg" style="display: none;">
       <!--<html:errors property="wuni.error.mywhatuni.yoe"/>-->       
      </div>
       
      <fieldset></fieldset>
      <fieldset class="row-fluid row5">
       <fieldset class="w50p fl" style="display:none">
        <label for="date" class="lbco mb8">DATE OF BIRTH </label>
        <fieldset class="w100p fl" id="userDOB">
         <div class="st_daycnr">
          <div class="select s_day fwc">
           <span class="fnt_lbd" id="dateSpan">Date</span>
            <i class="fa fa-angle-down fa-lg"></i>
          </div>
          <html:select path="date" id="dateDOB" 
                       cssClass="s_day"
                       onchange="setSelectedDate(this, 'dateSpan'); profileValidations(this.id);">
           <option value="">Day</option>
           <html:options items="${mywhatunibean.dateList}" itemLabel="label" itemValue="value"/>
          </html:select>
         </div>
         <div class="st_mthcnr">
          <div class="select s_mth fwc"><span class="fnt_lbd" id="monthSpan">Month</span><i class="fa fa-angle-down fa-lg"></i></div>
            <html:select path="month" cssClass="s_mth" id="monthDOB" onchange="setSelectedDate(this, 'monthSpan'); profileValidations(this.id);">
             <option value="">Month</option>
            <html:options items="${mywhatunibean.monthList}" itemLabel="label" itemValue="value"/>
          </html:select>
         </div>
         <div class="st_yearcnr">
          <div class="select s_year fwc"><span class="fnt_lbd" id="yearSpan">Year</span><i class="fa fa-angle-down fa-lg"></i></div>           
            <html:select path="year" cssClass="s_year" id="yearDOB" onchange="setSelectedDate(this, 'yearSpan'); profileValidations(this.id);">
              <option value="">Year</option>
              <html:options items="${mywhatunibean.yearList}" itemLabel="label" itemValue="value"/>
          </html:select>
         </div>
         <div class="err" id="userDobErrMsg" style="display: none;"></div>
        </fieldset>
       </fieldset>
       <fieldset class="w50p fl">
        <label for="nationality1" class="lbco mb8">NATIONALITY</label>
        <fieldset class="w100p fr" id="userNationality">
         <div class="select time fwc"><span class="fnt_lbd" id="natSpan">Nationality</span><i class="fa fa-angle-down fa-lg"></i></div>
         <html:select path="nationality"  cssClass="time" id="nationality" onchange="setSelectedDate(this, 'natSpan'); profileValidations(this.id);">
          <html:option value="">Nationality</html:option>
          <c:forEach var="nationalList" items="${mywhatunibean.nationalityList}" varStatus="i">
           <c:set var="optionId" value="${nationalList.optionId}"/>
           <c:set var="i" value="${i.index}"/>
           <html:option value="${nationalList.optionId}">
            ${nationalList.optionDesc}
           </html:option>
           <%if( Integer.valueOf(String.valueOf(pageContext.getAttribute("i"))) == 4 ){%>
           <optgroup label="---------------------------------"></optgroup>
           <%}%>
          </c:forEach>
         </html:select>
         <div class="err" id="userNationalityErrMsg" style="display: none;"></div>
        </fieldset>
       </fieldset>
      </fieldset>
       
      <fieldset class="row-fluid row6">
       <label class="lbco">STUDY LEVEL</label>
       <fieldset class="ql-inp pt10">
        <span class="ql_rad">
         <html:radiobutton path="userStudyLevel" value="452" onclick="enableUGPG(this);" cssClass="check" id="userStudyLeve11"/>
         <label>Undergraduate</label>
        </span>
        <span class="ql_rad">
         <html:radiobutton path="userStudyLevel" value="453" onclick="enableUGPG(this);" cssClass="check" id="userStudyLevel2"/>
         <label>Postgraduate</label>
        </span>
        <span class="ql_rad">
         <html:radiobutton path="userStudyLevel" value="454" onclick="enableUGPG(this);" cssClass="check" id="userStudyLevel3"/>
         <label>Foundation</label>
        </span>
       </fieldset>
      </fieldset>
     </div>
    </div>    
    <!--Basic info Starts-->
     
    <%--<jsp:include page="/jsp/mywhatuni/Redesign/include/userBasicInfoPod.jsp"/> --%>
     
	<jsp:include page="/jsp/mywhatuni/Redesign/include/educationalInfoPod.jsp"/> 
    <%--Changed "FIRST DEGREE" section to above the "Postal Info" for the bug:30841, on 08_Aug_2017, By Thiyagu G--%>
    <div id="pgdetails">
	 <c:if test="${not empty mywhatunibean.pgQuestionAnswers}">
	  <%
		  String currentGroupId = "";
		  String previousGroupId = "";
		  boolean fieldSetOpen=false;
		  String selectedValue = "";
		  String currentAttId = "";
		  String previousAttId = "";
		  String toogleId1 = null;
		  String toogleId2 = null;
	  %>
	<c:forEach var="pgQuestionAnswers" items="${mywhatunibean.pgQuestionAnswers}" varStatus = "index">
     <c:set var="PGquestionAnswerBean" value="${pgQuestionAnswers}"/>
     <%
        QuestionAnswerBean PGquestionAnswerBean = (QuestionAnswerBean) pageContext.getAttribute("PGquestionAnswerBean");
		String styleDisplay = "";
		String groupId = PGquestionAnswerBean.getGroupId();
		String attributeText = PGquestionAnswerBean.getAttributeText();
		String className = PGquestionAnswerBean.getClassName();
		String defaultValue = PGquestionAnswerBean.getDefaultValue();
		String attributeValue = PGquestionAnswerBean.getAttributeValue();
		String attributeId = PGquestionAnswerBean.getAttributeId();
		String optionFlag = PGquestionAnswerBean.getOptionFlag();
		String formatDesc = PGquestionAnswerBean.getFormatDesc();
		String displayFlag = PGquestionAnswerBean.getDisplayFlag();
		String attributeName = PGquestionAnswerBean.getAttributeName();
		String drpSpanId = "dropsapn_"+attributeName;
		if("DROPDOWN".equalsIgnoreCase(formatDesc)){
		  selectIds.append(attributeName+"##");
		}                    
		if(attributeValue == null){
		  selectedValue = defaultValue;
		}else{
		  selectedValue = attributeValue;
		}       
		if(groupId != null){
		  currentGroupId = groupId;
		}else{
		  currentGroupId = "";
		}    
		currentAttId = attributeId;
		if("N".equalsIgnoreCase(displayFlag)){
		  styleDisplay = "display:none;";
		}
		String onfocus = "";
		String onblur = "";
		/* if(selectedValue == defaultValue){ */
			 onfocus = "clearDefault(this," +"'"+defaultValue+"'"+")";
			 onblur = "setDefault(this," +"'"+defaultValue+"'"+")";
		/* }
		if(selectedValue == attributeValue){
			onfocus = "clearDefault(this," +"'"+attributeValue+"'"+")";
			onblur = "setDefault(this," +"'"+attributeValue+"'"+")";
		} */
		if("MWU_STUDY_LEVEL".equalsIgnoreCase(attributeName)){
		%>
		<c:if test="${pgQuestionAnswers.formatDesc eq 'RADIO'}">
			<fieldset class="ql-inp pt10" id="thi" style="<%=styleDisplay%>">
			<c:if test="${pgQuestionAnswers.attributeName eq 'MWU_STUDY_LEVEL'}">
			  <label>${pgQuestionAnswers.attributeText}</label>
			  <html:hidden path="pgQuestionAnswers[${index.index}].attributeValue" id="MWU_STUDY_LEVEL_PG" value="<%=attributeValue%>"/>
			</c:if>
			
			<c:forEach var="options" items="${pgQuestionAnswers.options}">
			  <c:set var="radioOption1" value="${options.optionId}"/>
			<span class="ql_rad">         
			 <%if(attributeValue!=null && attributeValue.equalsIgnoreCase((String)pageContext.getAttribute("radioOption1"))){%>
				<input type="radio" class="check" checked="checked" value="${options.optionId}" name="attributeValue" onclick="enableAreyou(this);"  />
			  <%}else{%>
				<input type="radio" class="check" value="${options.optionId}" name="attributeValue" onclick="enableAreyou(this);"  />
			  <%}%>
			  <label>${options.optionDesc}</label>
			</span>        
			</c:forEach>
			</fieldset>
		</c:if>
		<%}if("280".equals(currentGroupId)){%>
	<!--First Degree Starts-->         
	
			
	
	 <%if("FIRST DEGREE".equalsIgnoreCase(attributeText)){%>
		<div class="sett_info Fdeg">
     <h3 class="info_tit">${pgQuestionAnswers.attributeText}</h3>
		<div class="crm_con">
      <%}%>
		<%if("FIRST_UNIVERSITY".equalsIgnoreCase(attributeName)){%>
			<fieldset class="row-fluid row1">
       <label class="lbco" for="firstUniId">${pgQuestionAnswers.attributeText}</label>	   
       <fieldset class="w100p fl" style="<%=styleDisplay%>" id="FS_<%=attributeName%>">
        <c:if test="${pgQuestionAnswers.formatDesc eq 'TEXT'}">		
        <html:hidden path="pgQuestionAnswers[${index.index}].attributeValue" id="firstUniId_hidden" value="<%=selectedValue%>"/>
			<%
			  if(attributeValue != null){
				String collegeDisplayName = "";
				collegeDisplayName = new WUI.utilities.CommonFunction().getCollegeDisplayName(attributeValue);   
				selectedValue = collegeDisplayName;
			  }
			%>
			<input type="text" id="firstUniId" class="ql-inpt addr" name="firstUniId" onclick="<%=onfocus%>" onkeypress="<%=onfocus%>" onkeyup="autoCompleteUniNAME(event,this);" onblur="<%=onblur%>" value="<%=selectedValue%>"/>
			</c:if>
       </fieldset>
	   <%}%>
	   <%if(!previousAttId.equalsIgnoreCase(currentAttId)){%>
		  <c:if test="${pgQuestionAnswers.formatDesc eq 'CHECKBOX'}">			
			<c:if test="${pgQuestionAnswers.optionFlag ne 'Y'}">
			  <fieldset class="pt10 rgfm-lt" id="FS_<%=attributeName%>">
				<%if("NON_UK_UNIVERSITY_FLAG".equalsIgnoreCase(attributeName)){%>
				  <html:checkbox path="pgQuestionAnswers[${index.index}].attributeValue" onclick="disableEnableUni();" cssClass="checkBox" id="<%=attributeName%>" value ="Y"/>
				<%}else{%>
				  <html:checkbox path="pgQuestionAnswers[${index.index}].attributeValue" cssClass="checkBox" id="<%=attributeName%>"  value ="Y"/>
				<%}%>
				<label for="pgQuestionAnswers[${index.index}].attributeText" class="rem1">${pgQuestionAnswers.attributeText}</label>
			  </fieldset>
			</c:if>
		</c:if> 
		<%}%>      
		<c:if test="${pgQuestionAnswers.formatDesc eq 'TEXT'}">
		  <%if("NON_UK_UNIVERSITY_NAME".equalsIgnoreCase(attributeName)){%>       
			<fieldset style="margin: 10px 0px 0px; <%=styleDisplay%>" id="FS_<%=attributeName%>">
				<label></label>
			<html:input path="pgQuestionAnswers[${index.index}].attributeValue" id="<%=attributeName%>"   onclick="<%=onfocus%>" onkeypress="<%=onfocus%>" onblur="<%=onblur%>" value="<%=selectedValue%>"/>
			</fieldset>
			</fieldset>
		  <%}%>
      
	  <%if("SUBJECT_STUDIED".equalsIgnoreCase(attributeName)){%>
			<fieldset class="row-fluid row2" style="<%=styleDisplay%>" id="FS_<%=attributeName%>">
			<fieldset class="w100p">
			<label class="lbco">${pgQuestionAnswers.attributeText}</label>
			<html:hidden path="pgQuestionAnswers[${index.index}].attributeValue" id="firstCourseId_display" value="<%=selectedValue%>"/>
			<input type="text" id="firstCourseId" class="ql-inpt addr" name="firstCourseId" onclick="<%=onfocus%>" onkeypress="<%=onfocus%>" onkeyup="autoCompleteUniCourse(event,this);"  onblur="<%=onblur%>" value="<%=selectedValue%>"/>
			</fieldset>	
			</fieldset>				
		  <%}else if("FIRST_DEGREE_COURSE".equalsIgnoreCase(attributeName)){%>		
		   <fieldset class="row-fluid row2" style="<%=styleDisplay%>" id="FS_<%=attributeName%>">
		   	<fieldset class="w100p">
			<label></label>
			<html:hidden path="pgQuestionAnswers[${index.index}].attributeValue" cssClass="ql-inpt" id="firstCourseId_hidden" onclick="<%=onfocus%>" onkeypress="<%=onfocus%>" onblur="<%=onblur%>" value="<%=selectedValue%>"/>
			</fieldset>
			</fieldset>	
		  <%}%>
       </c:if>
      
       <c:if test="${pgQuestionAnswers.formatDesc eq 'DROPDOWN'}">
		<%String dropSpanValue = "setSelectedDate(this," +"'"+drpSpanId+"'"+");";			
			if("YEAR_COMPLETED_EXPECTED".equalsIgnoreCase(attributeName)){
		%>		  
		  <fieldset class="row-fluid row3" style="<%=styleDisplay%>" id="FS_<%=attributeName%>">
			<fieldset class="w50p  fl">
			<label class="lbco fwnl fl w100p mb8">${pgQuestionAnswers.attributeText}</label>
			<fieldset class="w100p fr">
				<div class="select time fwc">
				<span class="fnt_lbd" id="<%=drpSpanId%>" class="fnt_lbd"><%=selectedValue%></span>
				<i class="fa fa-angle-down fa-lg"></i>
				</div>
				<html:select path="pgQuestionAnswers[${index.index}].attributeValue" cssClass="time" id="<%=attributeName%>"  onchange="<%=dropSpanValue%>">
				<option value=""><%=defaultValue%></option>
				<html:options items="${pgQuestionAnswers.options}"  itemLabel="optionDesc" itemValue="optionId"  />     
			  </html:select>
			</fieldset>
			</fieldset>  
		  
			<%}if("CLASSFICATION".equalsIgnoreCase(attributeName)){%>
				<fieldset class="w50p  fr">
				<label class="lbco fwnl fl w100p mb8 ml15">${pgQuestionAnswers.attributeText}</label>
				<fieldset class="w100p fr">
				 <div class="select time fwc">
				  <span class="fnt_lbd" id="<%=drpSpanId%>" class="fnt_lbd"><%=selectedValue%></span>
				  <i class="fa fa-angle-down fa-lg"></i>
				 </div>
				 <html:select path="pgQuestionAnswers[${index.index}].attributeValue" cssClass="time" id="<%=attributeName%>" onchange="<%=dropSpanValue%>">
				<option value=""><%=defaultValue%></option>
				<html:options items="${pgQuestionAnswers.options}"  itemLabel="optionDesc" itemValue="optionId"/>                                    
			  </html:select>
				</fieldset>
			   </fieldset>
			  </fieldset>
			  </div>
			</div>
			<%}%>
		</c:if>    
	 
    <!--First Degree Ends-->
     <%}if("281".equals(currentGroupId)){%>
    <!--Current Employment Starts-->
     
	 <%if("CURRENT EMPLOYMENT STATUS".equalsIgnoreCase(attributeText)){%>
    <div class="sett_info Cemp">
     <h3 class="info_tit pt20">${pgQuestionAnswers.attributeText}</h3>
	 <div class="crm_con">
      <%}
	  if("CURRENT_EMPLOYMENT_ARE_YOU".equalsIgnoreCase(attributeName)){
	  %>
      <fieldset class="row-fluid row1">  
	  		  <label class="lbco">${pgQuestionAnswers.attributeText}</label>
			  <fieldset class="ql-inp pt10">	        
		    <c:if test="${pgQuestionAnswers.formatDesc eq 'RADIO'}">
			<c:if test="${pgQuestionAnswers.attributeName ne 'MWU_STUDY_LEVEL'}">
			  
			  <html:hidden path="pgQuestionAnswers[${index.index}].attributeValue" id="<%=attributeName%>" value="<%=attributeValue%>"/>
			</c:if>
			<c:forEach var="options" items="${pgQuestionAnswers.options}">
			  <c:set var="radioOption" value="${options.optionId}"/>
			<span class="ql_rad">         
			 <%if(attributeValue!=null && attributeValue.equalsIgnoreCase((String)pageContext.getAttribute("radioOption"))){%>
				<input type="radio" class="check" checked="checked" value="${options.optionId}" name="attributeValue" onclick="enableAreyou(this);"  />
			  <%}else{%>
				<input type="radio" class="check" value="${options.optionId}" name="attributeValue" onclick="enableAreyou(this);"  />
			  <%}%>
			  <label>${options.optionDesc}</label>
			</span>        
			</c:forEach>
		</c:if>
       </fieldset>
      	  </fieldset>
		  <%}%>
       <c:if test="${pgQuestionAnswers.formatDesc eq 'DROPDOWN'}">
	   <%String dropSpanValue = "setSelectedDate(this," +"'"+drpSpanId+"'"+");";%>
		<%if("CURR_EMP_INDUSTRY".equalsIgnoreCase(attributeName)){%>
	  <fieldset class="row-fluid row2">	  
       <fieldset class="w50p fl mt5" style="<%=styleDisplay%>" id="FS_<%=attributeName%>">
        <fieldset class="w100p fr">
         <div class="select time fwc">
          <span class="fnt_lbd" id="<%=drpSpanId%>"><%=selectedValue%></span> 
			<i class="fa fa-angle-down fa-lg"></i>          
         </div>  
		 <html:select path="pgQuestionAnswers[${index.index}].attributeValue" cssClass="time" id="<%=attributeName%>"  onchange="<%=dropSpanValue%>">
				<option value=""><%=defaultValue%></option>
				<html:options items="${pgQuestionAnswers.options}" itemLabel="optionDesc" itemValue="optionId"/>                                   
			  </html:select>
        </fieldset>
       </fieldset>
	   <%}if("CURR_EMP_SENIORITY_LEVEL".equalsIgnoreCase(attributeName)){%>
       <fieldset class="w50p fr mt5" style="<%=styleDisplay%>" id="FS_<%=attributeName%>">
        <fieldset class="w100p fl">         
		<div class="select time fwc">
          <span class="fnt_lbd" id="<%=drpSpanId%>"><%=selectedValue%></span> 
			<i class="fa fa-angle-down fa-lg"></i>
         </div>  
		 <html:select path="pgQuestionAnswers[${index.index}].attributeValue" cssClass="time" id="<%=attributeName%>"  onchange="<%=dropSpanValue%>">
				<option value=""><%=defaultValue%></option>
				<html:options items="${pgQuestionAnswers.options}"  itemLabel="optionDesc" itemValue="optionId"/>                                   
			  </html:select>
        </fieldset>
       </fieldset>
      </fieldset>	  
	   </div>
    </div>
	  <%}%>
	  </c:if>
    
     
    <!--Current Employment Ends-->
     <%}if("282".equals(currentGroupId)){%>
    <!--Current Employment Starts-->
     <%if("INTERESTS".equalsIgnoreCase(attributeText)){%>
    <div class="sett_info Inrs">
     <h3 class="info_tit">Interests</h3>      
     <div id="" class="crm_con">
	 <%}
	 String rowFluidRow = "";
	 if("STUDY_MODE".equalsIgnoreCase(attributeName)){rowFluidRow = "row-fluid row1";}
	 if("DEGREE_TYPE".equalsIgnoreCase(attributeName)){rowFluidRow = "row-fluid row2";}
  if(!previousAttId.equalsIgnoreCase(currentAttId)){
	 if("STUDY_MODE".equalsIgnoreCase(attributeName) || "DEGREE_TYPE".equalsIgnoreCase(attributeName)){%>
<c:if test="${pgQuestionAnswers.formatDesc eq 'CHECKBOX'}">
      <fieldset class="<%=rowFluidRow%>">
       <label class="lbco">${pgQuestionAnswers.attributeText}</label>
       <fieldset class="ql-inp pt10">	   	   
	   <c:forEach var="options1" items="${pgQuestionAnswers.options}">
		  <c:set var="selOptionId" value="${options1.optionId}"/>
			<span class="ql_rad">
			  <%if(attributeValue!=null && attributeValue.indexOf("<"+(String)pageContext.getAttribute("selOptionId")+">")!=-1){%>
				<input type="checkbox" class="check" checked="checked" name="CHKB_${pgQuestionAnswers.attributeName}" value="${options1.optionId}" >
			  <%}else{%>
				<input type="checkbox" value="${options1.optionId}" name="CHKB_${pgQuestionAnswers.attributeName}">
			  <%}%>
			  <label for="${options1.optionDesc}">${options1.optionDesc}</label>
			</span>
		  </c:forEach>
       </fieldset>
      </fieldset>
      </c:if>
      <%}}
      if("RESEARCH_INTEREST".equalsIgnoreCase(attributeName)){%>
      <fieldset class="row-fluid row3 course_deatils">
       <label for="email" class="lbco">
        <span class="tip_txt">${pgQuestionAnswers.attributeText}</span>
        <span class="tool_tip">
         <i class="fa fa-question-circle fnt_24">           
          <span class="cmp tl_pos_top">
           <div class="hdf5"></div>
           <div>If you have any specific research interests, then please enter them here..</div>
           <div class="line"></div>
          </span>
           </i>
        </span>
       </label>
       <fieldset class="w100p">
	   <c:if test="${pgQuestionAnswers.formatDesc eq 'TEXT'}">  
		   <html:input path="pgQuestionAnswers[${index.index}].attributeValue" cssClass="ql-inpt" id="<%=attributeName%>"  onclick="<%=onfocus%>" onkeypress="<%=onfocus%>" onblur="<%=onblur%>" value="<%=selectedValue%>"/>
		   </c:if>
       </fieldset>
      </fieldset>
     </div>
    </div>
	<%}%>
    <!--Current Employment Ends-->
	<%}
                    if(currentGroupId != null){
                      previousGroupId = currentGroupId;
                    }else{
                      previousGroupId = "";
                    }
                  %>
                  <%if(!previousAttId.equalsIgnoreCase(currentAttId)){%>
                    <html:hidden path="pgQuestionAnswers[${index.index}].orderAttributeId"/>
                    <html:hidden path="pgQuestionAnswers[${index.index}].attributeId"/>
                    <html:hidden path="pgQuestionAnswers[${index.index}].attributeName"/>
                    <html:hidden path="pgQuestionAnswers[${index.index}].attributeValueId"/>
                    <html:hidden path="pgQuestionAnswers[${index.index}].optionId"/>
                    <html:hidden path="pgQuestionAnswers[${index.index}].attrValueSeq"/>
                    <html:hidden path="pgQuestionAnswers[${index.index}].defaultValue"/>
                    <html:hidden path="pgQuestionAnswers[${index.index}].optionFlag"/>    
                    <html:hidden path="pgQuestionAnswers[${index.index}].formatDesc"/>
                 <%}
                 previousAttId = currentAttId;
                 %>
	
	</c:forEach>
	</c:if>
    </div>
    
   <jsp:include page="/jsp/mywhatuni/Redesign/include/postalInfoPod.jsp"/>
     
	   
     <html:hidden path="email"/>
     <input type="hidden" value="Save changes" name="butvalue">
   </div>
   <!--Added by Indumathi.S Jan-27-16 For mailingPreference and privacy -->
    <input type="hidden" name="receiveNoEmailFlag" id="remainderMailId"/>
    <input type="hidden" name="permitEmail"id="permitEmailId"/>
    <input type="hidden" name="solusEmail" id="solusEmailId"/>
    <input type="hidden" name="markettingEmail" id="newsLettersId"/>
    <input type="hidden" name="surveyMail" id="surveyMailId"/>
    <input type="hidden" name="privacy"  id="privacy"/>
    <input type="hidden" name="catPrivacy"  id="catPrivacy"/>
    <input type="hidden" name="reviewSurvey" id="reviewSurveyHidden"/>
    <!--End-->
    <input type="hidden" id="mail_a_href" name="mail_a_href" value="">
  </html:form>
 </div>
</div>

<script type="text/javaScript">
  setGradesEntryPointsValue();
  setDOBValues();
  var selectebufferIds = "<%=selectIds%>";
  setAttributeDDValue(selectebufferIds);

  function showImagediv(){
    document.getElementById('imgdiv').style.display = 'block';
  }
  function hideImagediv(){
    document.getElementById('imgdiv').style.display = 'none';
    document.getElementById("imgerror").innerHTML='';
  }
  function imageuploadErrorMsg(){
    document.getElementById("imgerror").innerHTML='File size should be less than 2 MB';
  }
  
  function toggleMenu(id1,id2,spanId,divId){
    if(document.getElementById(id1).style.display == 'none'){
      document.getElementById(id1).style.display = 'block'
      document.getElementById(id2).className = 'bsc-info'
    } else {
      document.getElementById(id1).style.display = 'none'
      document.getElementById(id2).className = 'bsc-info act'
    }
  }  
disableNonUKSchool();
autocompleteOff('mywhatuniId');
if(document.getElementById("NON_UK_UNIVERSITY_FLAG")){
  if (document.getElementById("NON_UK_UNIVERSITY_FLAG").checked == true){
    document.getElementById("firstUniId").disabled = true;
    document.getElementById("FS_NON_UK_UNIVERSITY_NAME").style.display = 'block';
  }else{
    document.getElementById("firstUniId").disabled = false;
    document.getElementById("FS_NON_UK_UNIVERSITY_NAME").style.display = 'none';
  }
}

</script>

<c:if test="${not empty requestScope.imageuploaderror}">
 <script type="text/javascript">
 imageuploadErrorMsg();
 document.getElementById("imgdiv").style.display='block';
 </script>
</c:if>