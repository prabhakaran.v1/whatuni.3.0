<%--
  * @purpose:  This jsp is to show the error message using light box.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 27-Jan-2016    Indumathi Selvam          1.0      First draft           wu_548
  * *************************************************************************************************************************
--%>
<%
String pageType = request.getParameter("pageType");
%>
<div class="comLgh">
  <div class="fcls nw">
    <a class="noalert" id="clearHiddenVal" onclick="javascript:hm();"><i class="fa fa-times"></i></a>
  </div>   
  <div class="pform nlr bgw opday f5_ms" id="lblogin">    
    <div class="reg-frm">
      <div class="reg-frmt bgw cmp_pt2">   
        <%if("saveMailSettings".equals(pageType)){%> 
          <h2>Oops..<br/>Are you sure you want to leave without saving your user preferences</h2>  
          <div class="fr f5_msge ms_lbbtn">             
            <a class="edt_fnl5_btn ed_f5_btn msg_f5_clse" href="javascript:saveMailSettings();" >Save my preferences <i class="fa richp fa-long-arrow-right"></i></a>
            <a class="edt_fnl5_btn ed_f5_btn msg_f5_clse" href="javascript:resetSettings();">Leave page now <i class="fa richp fa-long-arrow-right"></i></a>
          </div>
        <%}%>
      </div>       
    </div>  
  </div>  
</div>