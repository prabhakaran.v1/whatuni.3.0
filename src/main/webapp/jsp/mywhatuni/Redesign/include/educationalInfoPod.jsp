<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator" %>
<%@ page import="com.wuni.advertiser.ql.form.QuestionAnswerBean, WUI.utilities.CommonFunction"%>
<%int count = 0, subCount = 0;
  String pageNo = (String)request.getAttribute("pageNo");
  String qualSub = "";
  String qualId = "";
  String offerName = request.getParameter("offerName");
  String action = "";
  if("PROVISIONAL_OFFER".equalsIgnoreCase(offerName)){
    action = "PROV_QUAL_EDIT";  
  }
  String formStatusId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("pageStatus"))) ? (String)request.getAttribute("pageStatus") : "";
  String qualifications = "";
  String userGrade = "";
  String userSubject = "";
  String userSubGrd = "";
  String userTariff = (String)request.getAttribute("userTariff");
  String callTypeFlag = "";
  String qualTCCheckbox = (String)request.getAttribute("qualTCCheckbox");
  String pageName = !GenericValidator.isBlankOrNull((String)request.getAttribute("pageName")) ? (String)request.getAttribute("pageName") : null;
  %>
<!--Education info Starts-->
     
    <div class="sett_info Edu" id="ugdetails">
      <c:if test="${not empty mywhatunibean.ugQuestionAnswers}">      
      
      <h3 class="info_tit pt20">Education info</h3>
      <div class="crm_con" id="">
      <c:forEach var="ugQuestionAnswers" items="${mywhatunibean.ugQuestionAnswers}" varStatus = "index">
     <c:set var="attributeName" value="${ugQuestionAnswers.attributeName}"/>
        <%
                 QuestionAnswerBean UGquestionAnswerBean = (QuestionAnswerBean) pageContext.getAttribute("ugQuestionAnswers");
                      String attributeText = UGquestionAnswerBean.getAttributeText();
                      String attributeValue = UGquestionAnswerBean.getAttributeValue();
                      String attributeId = UGquestionAnswerBean.getAttributeId();
                      String optionFlag = UGquestionAnswerBean.getOptionFlag();
                      String defaultValue = UGquestionAnswerBean.getDefaultValue();
                      String schoolName = "";
                    %>
                
         <c:if test="${'SCHOOL_URN' eq attributeName}"> <!--Change the UK_SCHOOL_ID to SCHOOL_URN for attributeName By Prabha on 04_oct_16-->
 <fieldset class="row-fluid row1 my_schcoll">    
         
          <label for="collegename" class="lbco pred_grad">My School / college</label>
          <fieldset class="w100p fl" id="ukSchoolNameFeild">
          <html:hidden path="ugQuestionAnswers[${index.index}].attributeValue" id="ukSchoolName_hidden" value=""/>
           <%
                          if(attributeValue != null){
                            schoolName = new com.wuni.mywhatuni.controller.MyWhatuniController().getUKSchoolName(attributeValue);
                            schoolName = !GenericValidator.isBlankOrNull(schoolName) ? schoolName : ""; //Added code for empty the school name by Prabha on 22_Nov_2016
                          }
                        %>
             
             <c:if test="${empty ugQuestionAnswers.attributeValue}">
            <input type="text" id="ukSchoolName"
                   onkeyup="autoCompleteUKSchool(event,this)"
                   name="ukSchoolName" value="<%=defaultValue%>"
                   onkeypress="clearDefaultProfile(this.id, '<%=defaultValue%>');"
                   onclick="clearDefault(this,'<%=defaultValue%>');"
                   onblur="setDefault(this,'<%=defaultValue%>'); updateSuccessMessage(this);"
                   class="ql-inpt addr errRd"/>
           </c:if>
           <c:if test="${not empty ugQuestionAnswers.attributeValue}">
            <input type="text" id="ukSchoolName" name="ukSchoolName"
                   value="<%=schoolName%>"
                   onkeyup="autoCompleteUKSchool(event,this);"/>
				   
           </c:if>
          </fieldset>
           
          </fieldset>
		  </c:if>
          
           <c:if test="${'NON_UK_SCHOOL_FLAG' eq attributeName}"> 
		   
           <fieldset class="pt10 rgfm-lt my_schntlst">
             <html:checkbox path="ugQuestionAnswers[${index.index}].attributeValue"
                             onclick="disableNonUKSchool();"
                             cssClass="checkBox" id="nonUKFlag"
                             value="Y"/>
             
              <label class="rem1" for="nonUKFlag">My school is not in the UK /
                                                  not on this list</label>
             
             </fieldset>
           </c:if>
        

           <c:if test="${'NON_UK_SCHOOL_NAME' eq attributeName}">
           <fieldset id="nonUKFS" style="display: none; margin:10px 0 0;">
            <label></label>
            <c:if test="${empty ugQuestionAnswers.attributeValue}">            
             <html:input path="ugQuestionAnswers[${index.index}].attributeValue" id="nonUKSchool"
                        value="Please enter your Non UK School name"
                        onkeypress="clearDefaultProfile(this.id, 'Please enter your Non UK School name');"
                        onclick="clearDefault(this,'Please enter your Non UK School name');"
                        onblur="setDefault(this,'Please enter your Non UK School name');updateSuccessMessage(this);" />
            </c:if>
            <c:if test="${not empty ugQuestionAnswers.attributeValue}">
             <html:input path="ugQuestionAnswers[${index.index}].attributeValue" id="nonUKSchool" />
            </c:if>
           </fieldset>
          </c:if>
         <c:if test="${'MWU_STUDY_LEVEL' eq ugQuestionAnswers.attributeName}">
         <html:hidden path="ugQuestionAnswers[${index.index}].attributeValue" id="${attributeName}" value=""/>
        </c:if>
        <html:hidden path="ugQuestionAnswers[${index.index}].orderAttributeId" />
        <html:hidden path="ugQuestionAnswers[${index.index}].attributeId"  value=""/>
        <html:hidden path="ugQuestionAnswers[${index.index}].attributeName" />
        <html:hidden path="ugQuestionAnswers[${index.index}].attributeValueId"/>
        <html:hidden path="ugQuestionAnswers[${index.index}].optionId" />
        <html:hidden path="ugQuestionAnswers[${index.index}].attrValueSeq" />
        <html:hidden path="ugQuestionAnswers[${index.index}].defaultValue" />
        <html:hidden path="ugQuestionAnswers[${index.index}].optionFlag" />
       </c:forEach>
       </div>
     </c:if>
     
        <!--  -->
        <c:if test="${not empty requestScope.userQualList}">                           
                        <section class="cmm_cnt">
                          <div class="cmm_col_12 binf_cnt fl_w100">
                            <div class="cmm_wrap fl_w100">
                              <div class="cmm_row fl_w100">
                                <div class="fl_w100 basic_inf pers_det cont_det qualif qualedit">
         
            <!-- UCAS -->
            <div class="ucas_midcnt fl_w100">
              <div id="ucas_calc fl_w100">
                <div class="ucas_mid fl_w100">
             <c:forEach var="userQualList" items="${requestScope.userQualList}" varStatus="i" >
             <c:set var="indexIValue" value="${i.index}"/>
          <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue").toString())) + 1;%>
                  <!--Qualification Repeat Field start 1-->
                  <div class="add_qualif fl_w100">
                    <div class="ucas_refld fl_w100">
                      <div class="ucas_row fl_w100">
					  <c:if test="${i.index == 0}">
					  <fieldset id="gradesMsg" class=""> 
			<label class="lbco mb8 pred_grad">PREDICTED GRADES</label>
		</fieldset>
		</c:if>
                         <c:set var="qual_check" value="${userQualList.entry_qual_id}"/>
                          <h3 class="un-tit"> <span class="qedt_md"><a data-filter-type="yourGrades" title="Your grades">Edit</a> </span></h3>
                           <div class="qual_edt qual">
                          <h5 class="cmn-tit txt-upr">Qualifications</h5>
                          <div class="qua_fild">
                            <span>
                              ${userQualList.entry_qual_desc}
                            </span>
                          </div>
                        </div>
                        
                        <c:set var="qualid" value="${userQualList.entry_qual_id}"/>
                        <c:set var="qualDesc" value="${userQualList.entry_qual_desc}"/>
                        <jsp:scriptlet>
                          qualId += pageContext.getAttribute("qualid").toString() + ",";
                          qualSub = "";
                          qualifications += pageContext.getAttribute("qualDesc").toString() + ",";
                         </jsp:scriptlet>
                        <c:if test="${not empty userQualList.qual_subject_list}">                                 
                        <div class="qual_edt sub">
                          <h5 class="cmn-tit txt-upr">SUBJECT(S) AND/OR GRADE(S)</h5>
                            <jsp:scriptlet>userSubGrd = "";</jsp:scriptlet>
                            <c:forEach var="subjectList" items="${userQualList.qual_subject_list}" varStatus="j" >
                              <c:set var="indexJValue" value="${j.index}"/>
                              <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue").toString())) + 1;%>
                              <c:set var="grade" value="${subjectList.entry_grade}"/>
                             
                             <input class="subjectArr" type="hidden" id="sub_<%=pageContext.getAttribute("qualid").toString()%>" name="sub_<%=pageContext.getAttribute("qualid").toString()%>" value="<%=pageContext.getAttribute("qualid").toString()%>" />
                              <input class="gradeArr" type="hidden" id="grde_<%=pageContext.getAttribute("qualid").toString()%><%=subCount%>" name="grde_<%=pageContext.getAttribute("qualid").toString()%>" value="<%=pageContext.getAttribute("qualid").toString()%>" />
                              <input class="qualArr" type="hidden" id="qual_<%=pageContext.getAttribute("qualid").toString()%><%=subCount%>" name="qual_<%=pageContext.getAttribute("qualid").toString()%>" value="<%=pageContext.getAttribute("qualid").toString()%>" />
                              <input class="qualSeqArr" type="hidden" id="qual_<%=pageContext.getAttribute("qualid").toString()%><%=subCount%>" name="qual_<%=pageContext.getAttribute("qualid").toString()%>" value="<%=count%>" />
                            <div class="qua_fild">	
                              <c:set var="user_grade" value="${subjectList.entry_grade}"/>
                              <span><c:if test="${not empty subjectList.entry_subject}"> ${subjectList.entry_subject},</c:if> ${subjectList.entry_grade}
                              </span>
                            </div>
                            </c:forEach>
                          </div>
                      </c:if>
                        </div>
                      </div>
                    </div>
                    <input class="gaQualDesc" type="hidden" id="qual_desc" value="<%=qualifications%>" />
                    <input class="gaUserSub_<%=pageContext.getAttribute("qualid").toString()%>" type="hidden" id="user_Sub" value="<%=userSubject%>" />
                    <input class="gaUserGrd_<%=pageContext.getAttribute("qualid").toString()%>" type="hidden" id="user_grd" value="" />
                    <input class="gaSubGrde" type="hidden" id="gaSubGrde_<%=pageContext.getAttribute("qualid").toString()%>" value="<%=userSubGrd%>" />
                                       
                    <input class="qualSubj" type="hidden" id="qualSub_<%=pageContext.getAttribute("qualid").toString()%>" value="" />
                    
                    <input class="gaQualIds" type="hidden" id="qual_id" value="<%=qualId%>" />
                    
				  </c:forEach>
          <input class="qual" type="hidden" id="qualId" value="<%=qualId%>" />
          <input class="gaUcasPoints" type="hidden" id="gaUcasPoints" value="<%=userTariff%>" />
                  <!--Qualification Repeat Field end 1 -->
                </div>
                <!--Qualification Repeat Field start 2-->
				<div id="scoreDiv" class="fl cmqua mt-40 w-647">
                <!--Your Score Container Start -->
                <c:if test="${not empty requestScope.userTariff}">
                <div class="ucas_btm ctrl_mt scr_mt0">
										<div class="ucas_row row_btm30">
											<div id="innerScoreDiv" class="scr_wrap">
												<div class="scr_lft">
                            <div class="chartbox fl w100p">
                              <h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Your UCAS score</font></font></h2>
                              <div class="ucspt_val">${requestScope.userTariff} points</div>  
                            </div>
												</div>
											</div>
  <div class="err" id="subErrMsg" style="display:none;"></div>
										</div>
									</div>
                  </c:if>
                          
              </div>
            </div>
              </div>
              
          <input type="hidden" id="qualDesc" value="<%=qualifications%>" />
          <input type="hidden" id="oldQualDesc" value="<%=qualifications%>" />
          <input type="hidden" id="newQualDesc" value="<%=qualifications%>" />
         </div>
         </div>
         </div>
         </div>
         </section>          
        </c:if>
         <c:if test="${empty requestScope.userQualList}">       
        <fieldset id="gradesMsg" class="row-fluid row2 qlf-nwfs grads"> 
			<label class="lbco mb8 pred_grad">PREDICTED GRADES</label>
			<h3 class="un-tit"> <span class="qedt_md qe_ntyet fnt_16">You have not yet added any qualifications. <a data-filter-type="yourGrades" data-page-name = "Profile" title="Your grades" href="javascript:void(0)">Click here</a> to get started. </span> </h3>
		</fieldset>     
         </c:if>
        
        <!-- -->         
        
        </div>
     
    <!--Education info Ends-->
    <input type="hidden" id="page-name" name="page-name" value="<%=pageName%>" />