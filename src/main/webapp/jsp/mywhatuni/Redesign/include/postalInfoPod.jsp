<%@ taglib uri="/WEB-INF/tlds/bean.tld" prefix="bean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@page import="WUI.utilities.CommonUtil" %>

<!--Postal Info Starts-->
     
    <div class="sett_info Post Fdeg"> <%--Added Fdeg class to reduceing the empty space to fix the bug:30841, on 08_Aug_2017, By Thiyagu G--%>
     <h3 class="info_tit pt20">Postal info</h3>      
     <div class="crm_con">
      <fieldset class="row-fluid row1">
       <fieldset class="w50p  fl">
        <label for="countryresi" class="lbco fwnl fl w100p mb8">Country of residence</label>
        <fieldset class="w100p fr" id="userCountryOfResidence">
         <div class="select time fwc">
          <span class="fnt_lbd" id="countrySpan">Please Select</span>
            <i class="fa fa-angle-down fa-lg"></i>
         </div>
         <html:select path="countryOfResidence" 
                      cssClass="time" id="countryOfResidence"
                      onchange="setSelectedDate(this, 'countrySpan'); hideandshowAddFinder(this); profileValidations(this.id);">
          <html:option value="">Please Select</html:option>
          <c:forEach var="counttyList" items="${mywhatunibean.counttyOfResList}"  varStatus="i">
           <c:set var="intValue" value="${i.index}"/>
           <c:set var="optionIds" value="${counttyList.optionId}"/>
           <html:option value="${counttyList.optionId}">
            ${counttyList.optionDesc}
           </html:option>
           <%if( Integer.valueOf(pageContext.getAttribute("intValue").toString()) == 4 ){%>
           <optgroup label="-----------------------------------"></optgroup>
           <%}%>
          </c:forEach>
         </html:select>
         <div class="err" id="userCountryOfResidenceErrMsg" style="display: none;"></div>
        </fieldset>
       </fieldset>
       <fieldset id="postcodeField" class="w50p fr" style="display:none;">
        <label for="Postfinder" class="lbco fwnl fl w100p pb8">Postcode finder</label>
        <fieldset class="w100p fl">
         <input type="text" name="ukpostcode" class="ql-inpt addr errRd"
                id="postcode" value="Enter UK postcode"
                onkeypress="clearDefaultProfile(this.id, 'First Name');"
                onclick="clearDefault(this, 'Enter UK postcode');"
                onblur="profileValidations(this.id); setDefault(this, 'Enter UK postcode');"></input>  
         <div class="err" id="postcodeErrMsg" style="display:none;"></div>
        </fieldset>        
       </fieldset>
      </fieldset>
       
	  <fieldset class="row-fluid row1a" id="postcodeSection" style="display: none;">
		<fieldset class="w50p  fl">
			<fieldset class="w100p fr" id="postaddid" style="display:none;">
			   <div class="select time fwc">
				  <span id="postCodeAddSpan" class="fnt_lbd">Please select</span>
				  <i class="fa fa-angle-down fa-lg"></i>      
			   </div>
      <select name="postCodeAddSel" id="postCodAdd" class="time" onchange="setSelectedDate(this, 'postCodeAddSpan'); setPostCodeAddress(this);"></select>
			</fieldset>
	   </fieldset>		
		<fieldset class="w50p  fr">	
			<div class="subs_cnr">				
				<input type="button" class="btn_view" value="Find Address"
                onclick="getPostCodeAdd();" id="addFinder"
                style="display:none"/>
			</div>
		</fieldset>	
	</fieldset>
	   
      <fieldset class="row-fluid row2" id="address_field_1" style="display:none">
       <label for="address1" class="lbco">YOUR ADDRESS</label>
       <fieldset class="w100p fl" id="address1Feild">
        <c:if test="${not empty mywhatunibean.addressLine1}">
         <html:input path="addressLine1" 
                    id="address1" cssClass="ql-inpt addr errRd"
                    onkeypress="clearDefaultProfile(this.id, 'First Name');"
                    onclick="clearDefault(this, 'Address line 1');"
                    onblur="setDefault(this, 'Address line 1'); updateSuccessMessage(this);" maxlength="100"/>
        </c:if>
        <c:if test="${empty mywhatunibean.addressLine1}">
         <html:input path="addressLine1"
                    id="address1" cssClass="ql-inpt addr errRd"
                    value="Address line 1"
                    onkeypress="clearDefaultProfile(this.id, 'First Name');"
                    onclick="clearDefault(this, 'Address line 1');"
                    onblur="setDefault(this, 'Address line 1');updateSuccessMessage(this);" maxlength="100"/>
        </c:if>
        <div class="err" id="address1ErrMsg" style="display:none;"></div>
       </fieldset>
       <fieldset class="w100p fl" id="address2Feild">
       <c:if test="${not empty mywhatunibean.addressLine2}">
         <html:input cssClass="ql-inpt addr errRd" path="addressLine2" id="address2"
                    onkeypress="clearDefaultProfile(this.id, 'First Name');"
                    onclick="clearDefault(this, 'Address line 2');"
                    onblur="setDefault(this, 'Address line 2');updateSuccessMessage(this);" maxlength="100"/>
        </c:if>
        <c:if test="${empty mywhatunibean.addressLine2}">
         <html:input cssClass="ql-inpt addr errRd" path="addressLine2" id="address2"
                    value="Address line 2"
                    onkeypress="clearDefaultProfile(this.id, 'First Name');"
                    onclick="clearDefault(this, 'Address line 2');"
                    onblur="setDefault(this, 'Address line 2');updateSuccessMessage(this);" maxlength="100"/>
        </c:if>
       </fieldset>
      </fieldset>
       
      <fieldset class="row-fluid row3" id="address_field_2" style="display:none">
       <fieldset id="townField" class="w50p fl">
        <label for="town" class="lbco">town / city</label>
          <c:if test="${not empty mywhatunibean.town}">
            <html:input path="town" id="town" cssClass="ql-inpt" onkeypress="clearDefaultProfile(this.id, 'Please enter town/city');" onclick="clearDefault(this, 'Please enter town/city');" onblur="profileValidations(this.id); setDefault(this, 'Please enter town/city');" maxlength="100"/>
          </c:if>
          <c:if test="${empty mywhatunibean.town}">
            <html:input path="town" id="town" cssClass="ql-inpt" value="Please enter town/city" onkeypress="clearDefaultProfile(this.id, 'Please enter town/city');" onclick="clearDefault(this, 'Please enter town/city');" onblur="profileValidations(this.id); setDefault(this, 'Please enter town/city');" maxlength="100"/>
          </c:if>
          <div class="err" id="townErrMsg" style="display: none;">
       </fieldset>
       <fieldset id="postcodeIndexField" class="w50p fr">
        <label for="postcodeIndex" class="lbco">Postcode</label>        
			   <c:if test="${not empty mywhatunibean.postCode}">
				<html:input path="postCode" id="postcodeIndex" cssClass="ql-inpt" onkeypress="clearDefaultProfile(this.id, 'Please enter');" onclick="clearDefault(this, 'Please enter');" onblur="setDefault(this, 'Please enter');" maxlength="25"/>  
			  </c:if>
			  <c:if test="${empty mywhatunibean.postCode}">
				<html:input path="postCode" id="postcodeIndex" cssClass="ql-inpt" value="Please enter" onkeypress="clearDefaultProfile(this.id, 'Please enter');" onclick="clearDefault(this, 'Please enter');" onblur="setDefault(this, 'Please enter');" maxlength="25"/>  
			  </c:if>
        <div class="err" id="postcodeIndexErrMsg" style="display:none;"></div>
       </fieldset>
      </fieldset>
     </div>
    </div>     
    <!--Postal Info Ends-->
    
<!-- <script type="text/javascript">
  var $$q = jQuery.noConflict();
  $$q(document).ready(function(){
    if($$q("#countryOfResidence") != ""){
    	mySettingsShowhideAddressFields();
    }
  });  
</script> -->