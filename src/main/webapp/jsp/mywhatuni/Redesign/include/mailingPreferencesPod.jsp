<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<!--Modified By Indumathi.S for including mailing preferences pod in mysettings Jan-27-16 Rel-->
<%
String receiveNoEmail = request.getAttribute("receiveNoEmail") != null ? request.getAttribute("receiveNoEmail").toString() : "N";
String permitEmail = request.getAttribute("permitEmail") != null ? request.getAttribute("permitEmail").toString() : "N";
String solusEmail = request.getAttribute("solusEmail") != null ? request.getAttribute("solusEmail").toString() : "Y";
String markettingEmail = request.getAttribute("markettingEmail") != null ? request.getAttribute("markettingEmail").toString() : "Y";
String surveyMail = request.getAttribute("surveyMail") != null ? request.getAttribute("surveyMail").toString() : "Y"; //Added survey mail flag in mailing preferences for 31_May_2016, By Thiyagu G.
%>

<div class="set_pri cf">
  <div class="fl sub_cnr">
    <div class="sett_info Pri">
      <h3 class="info_tit"><span class="Tit">Mailing preferences</span> 
        <i class="fa fa-plus-circle fnt_24 color1 fr fa-minus-circle"></i>
      </h3>
      <div class="seton_cont">
        <div class="sett_cnr mschk_cnr email">
          <fieldset class="row-fluid row1 mychk_lt">	
            <span class="ms_txtbld">Newsletters</span>
            <fieldset class="pt10 w100p fr">	
              <label class="ms_rem" for="friendsActivity">Emails from us providing you the latest university news, tips and guides.</label>
            </fieldset>	
          </fieldset>
          <fieldset class="mychk_rt">
            <form>
              <div class="controls">
                <input class="slideCheck hide" id="newsLetters" type="checkbox">
                <label for="newsLetters" class="slideCheckDiv"></label>
              </div>
            </form>
          </fieldset>	
        </div>
        <div class="sett_cnr mschk_cnr email">
          <fieldset class="row-fluid row1 mychk_lt">	
            <span class="ms_txtbld">University updates</span>
            <fieldset class="pt10 w100p fr">	
              <label class="ms_rem" for="friendsActivity"><spring:message code="wuni.solus.flag.text"/>.</label>
            </fieldset>	
          </fieldset>
          <fieldset class="mychk_rt">
            <form>
              <div class="controls">
                <input class="slideCheck hide" id="solusEmail" type="checkbox" >
                <label for="solusEmail" class="slideCheckDiv"></label>
              </div>
            </form>
          </fieldset>	
        </div>
        
        <div class="sett_cnr mschk_cnr email">
          <fieldset class="row-fluid row1 mychk_lt">	
            <span class="ms_txtbld">Reminders</span>
            <fieldset class="pt10 w100p fr">	
              <label class="ms_rem" for="friendsActivity">To remind you about upcoming course start dates, your shortlisted courses and any courses you emailed about.</label>
            </fieldset>	
          </fieldset>
          <fieldset class="mychk_rt">
            <form>
              <div class="controls">
              <input class="slideCheck hide" id="remainderMail" type="checkbox" onclick="enabledTextMessage()">
              <label for="remainderMail" class="slideCheckDiv"></label>
              </div>
            </form>
          </fieldset>	
        </div>
        <!--Added survey mail flag in mailing preferences for 31_May_2016, By Thiyagu G. Start-->
        <div class="sett_cnr mschk_cnr email">
          <fieldset class="row-fluid row1 mychk_lt">	
            <span class="ms_txtbld">Surveys</span>
            <fieldset class="pt10 w100p fr">	
              <label class="ms_rem" for="friendsActivity"><spring:message code="wuni.survey.flag.text"/>.</label>
            </fieldset>	
          </fieldset>
          <fieldset class="mychk_rt">
            <form>
              <div class="controls">
              <input class="slideCheck hide" id="surveyMail" type="checkbox" >
              <label for="surveyMail" class="slideCheckDiv"></label>
              </div>
            </form>
          </fieldset>	
        </div>
        <c:if test="${not empty reviewSurveyFlag}">
          <div class="sett_cnr mschk_cnr email">
            <fieldset class="row-fluid row1 mychk_lt">
              <span class="ms_txtbld"><spring:message code="review.survey.text" /></span>
              <fieldset class="pt10 w100p fr">
                <label class="ms_rem" for="reviewSurvey"><spring:message code="review.survey.label" /></label>
              </fieldset>
            </fieldset>
            <fieldset class="mychk_rt">
              <form>
                <div class="controls">
                  <input class="slideCheck hide" id="reviewSurvey" type="checkbox"> 
                  <label for="reviewSurvey" class="slideCheckDiv"></label>
                </div>
              </form>
            </fieldset>
          </div>
        </c:if>
        <!--Added survey mail flag for 31_May_2016, End-->
        <div class="sett_cnr mschk_cnr" style="display:none;">
          <fieldset class="row-fluid row1 mychk_lt">	
            <span class="ms_txtbld">All email</span>
            <fieldset class="pt10 w100p fr">	
              <label class="ms_rem" for="friendsActivity">&#8216;ON&#8217; for above settings or &#8216;OFF to turn all emails off completely</label>
            </fieldset>	
          </fieldset>
          <fieldset class="mychk_rt">
            <form>
              <div class="controls">
              <input class="slideCheck" id="allMail" type="checkbox" onclick="onOffEmail('allMail')">
              <label for="allMail" class="slideCheckDiv"></label>
              </div>
            </form>
          </fieldset>	
        </div>
      </div>
    </div>	
  </div>
</div>
<%--if("N".equalsIgnoreCase(permitEmail)){ %>
<script type="text/javascript" language="javascript">
  disableFlags();
</script>
<%}--%>
<script type="text/javascript" language="javascript">
  checkAndEnabledPrivacy('<%=receiveNoEmail%>','remainderMail');
  checkAndEnabledPrivacy('<%=permitEmail%>','allMail');
  checkAndEnabledPrivacy('<%=solusEmail%>','solusEmail');
  checkAndEnabledPrivacy('<%=markettingEmail%>','newsLetters');
  checkAndEnabledPrivacy('<%=surveyMail%>','surveyMail'); //Added survey mail flag in mailing preferences for 31_May_2016, By Thiyagu G.
  checkAndEnabledPrivacy('${reviewSurveyFlag}', 'reviewSurvey');
</script>