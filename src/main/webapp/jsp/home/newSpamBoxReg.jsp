<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
  * @purpose  This jsp is don't miss out pod
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 3-Nov-2015 Prabhakaran V.                546     modifid old file as part of redesign..                              546
  * *************************************************************************************************************************
--%>

<c:if test="${empty sessionScope.userInfoList}">
  <section class="p0 mt20 mb15">
    <div class="ad_cnr">
      <div class="content-bg">
        <jsp:include page="/jsp/home/spamBoxReg.jsp"/>
      </div>
    </div>
  </section>
</c:if>
