<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%String socialBoxJs = CommonUtil.getResourceMessage("wuni.social.box.js", null);
  CommonFunction common = new CommonFunction();  
  %>
  
<body>
<c:if test="${not empty clearingOnOffSysvar and clearingOnOffSysvar eq 'ON'}">
  <c:set var="CLEARING_TAB_CLS" value=" clr20_hmsrc"/>
</c:if>
<div class="wu_home logged_in_pod${CLEARING_TAB_CLS}">  
  <header class="clipart z3" id="homeHeadId">
    <div class="">
      <div class="content-bg">
        <div id="desktop_hdr">
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>                
      </div>      
    </div>
  </header>
  <%-- <jsp:include page="/jsp/home/include/heroSlider.jsp"/> --%>
  <jsp:include page="/jsp/home/include/heroImageSearchPod.jsp"/>
  <jsp:include page="/jsp/home/include/recentSearchPod.jsp"/>
  <jsp:include page="/jsp/home/include/recommedPopUniPod.jsp"/>
  <%--Added app marketing section in home page - Commented on 12_Sep_2017, By Thiyagu G--%>
    <jsp:include page="/jsp/home/include/appMarketingSetion.jsp">
      <jsp:param name="fromPage" value="NonLoggedInDefault"/>
    </jsp:include>
  <%--End of app marketing section--%>
  <jsp:include page="/jsp/home/include/atAGlance.jsp"/>
  <jsp:include page="/jsp/home/include/spotLight.jsp"/>
  <jsp:include page="/jsp/home/include/studentChoiceAwards.jsp"/> 
  <section class="ltst_art" id="articlesPod"></section>
  <input type="hidden" id="scrollStop" value="" />
  <%--<jsp:include page="/jsp/home/include/featureSchool.jsp"/>--%>
   <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=socialBoxJs%>"></script> 

  <jsp:include page="/jsp/common/wuFooter.jsp" /> 
  <jsp:include page="/jsp/common/socialBox.jsp" />
</div> 
 
</body> 
