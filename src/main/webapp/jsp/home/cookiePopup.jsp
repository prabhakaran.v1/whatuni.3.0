<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <div class="cookins" id="cookiePopup" style="display:block">
      <div class="ad_cnr">
          <div class="content-bg">
             <div class="ck_cnt">
              <p class="ckie_wrp"><span>We use cookies to ensure the best user experience and to serve tailored advertising. To learn more about our cookies and how to manage them, please visit our <a href="/degrees/cookies.html">Cookie Policy</a></p>
              <div class="top_ckiebtnset">
				<a href="javascript:void(0);" class="ckbubtn" onclick="createHeaderCookie('cookie_splash_flag','N')">I AGREE</a>
				<%--<a href="javascript:void(0);" class="ckbubtn" onclick="createHeaderCookie('cookie_splash_flag','N')">Accept all</a>
				 <a href="javascript:void(0);" class="ckbubtn ckbubtn2" id="changeSettingsLightBox">Manage cookies</a> --%>
			  </div>             
              </div>
          </div>
      </div>
  </div> 
  <%-- 
      <!-- cookies start -->
		<div class="cookins newcki" id="cookiePopup" style="display:none">
	      <div class="ad_cnr">
		    <div class="content-bg">
			  <div class="ck_cnt">
			    <p class="ckie_wrp">
				  <span>
				    We use cookies to ensure the best user experience and to serve tailored advertising. To learn more about our cookies and how to manage them, please visit our Cookie Policy.
				  </span>
				</p>
				<div class="top_ckiebtnset">
				  <a href="javascript:void(0);" class="ckbubtn" onclick="createHeaderCookie('cookie_splash_flag','N')">Accept all</a>
				  <a href="javascript:void(0);" class="ckbubtn ckbubtn2" id="changeSettingsLightBox">Change settings</a> 
				</div>
			  </div>
			</div>
		  </div>
		</div> 
		<!-- cookies end -->
		--%>
        <!-- Cookie settings starts -->
        <div id="newCookieSettings" style="display:none">
          <div class="newckibg"></div>
          <div class="newckiwrap">
            <div class="newckitopwrap">
              <div class="newckitoplft">
                <ul id="cookiesList">
                  <li id="yourPrivacy"><a id="yourPrivacy_a" class="activlnk" href="javascript:void(0)"><span>Your Privacy</span></a></li>
                  <li id="strictlyNecessaryCookies"><a id="strictlyNecessaryCookies_a" href="javascript:void(0)"><span>Strictly Necessary Cookies</span></a></li>
                  <li id="performanceCookies"><a id="performanceCookies_a" href="javascript:void(0)"><span>Performance Cookies</span></a></li>
                  <li id="functionalCookies"><a id="functionalCookies_a" href="javascript:void(0)"><span>Functional Cookies</span></a></li>
                  <li id="targetingCookies"><a id="targetingCookies_a" href="javascript:void(0)"><span>Marketing Cookies</span></a></li>
                </ul>
              </div>        
              <div class="newckitoprgt">
              <!-- Your Privacy starts -->
                <div id="yourPrivacySection" class="newckidetbox">
                  <div class="newckidet_lblcntr">
                    <h4>Your Privacy</h4>    
                  </div>
                  <div class="newckidetrgt_ovrflow">
                    <div class="newckidetinfo">                                
                      <p>We use cookies, which are small text files, to improve your experience on our website and to show you personalised content. Information might be about you, your preferences or your device and is mostly used to make the site work as you expect it to. The information does not usually directly identify you, but it can give you a more personalised web experience. You can accept all cookies or manage cookies individually. Blocking some types of cookies may impact your experience of the site and the services we are able to offer.</p>
                      <p>You can change your cookies preference and bring Manage cookies screen at any time by visiting our Cookies Notice page. PLEASE NOTE inactivating performance or marketing cookies will require clearing your browsing data through your browser page settings.</p>
                      <p>For more detailed information about the cookies we use, or how to clear your browser data see our <a href="/degrees/cookies.html">Cookies Notice</a></p>
                    </div>        
                  </div>                                    
                </div>
                <!-- Your Privacy Ends -->
                <!-- Strictly Necessary cookies starts -->
                <div id="strictlyNecessarySection" class="newckidetbox" style="display:none">
                  <div class="newckidet_lblcntr">
                    <h4>Strictly Necessary Cookies</h4>
                      <div class="alwy_act">Always active</div>
                      <!-- <div class="newckidet_lblchkrgt">
                        <p>
                          <input id="strictCkId" type="checkbox" class="newcki_spngrp_chkbox" checked="checked" disabled="disabled">
                          <label for="chkb0">
                            <span class="newckilbl_spntxt_act_i">Inactive</span>
                            <span class="newckilbl_spntxt_act">Active</span>
                          </label>
                        </p>
                      </div> -->
                    </div>
                    <div class="newckidetrgt_ovrflow">
                      <div class="newckidetinfo">                                
                        <p>These cookies are necessary for the website to function and cannot be switched off in our systems. Strictly Necessary cookies enable core functionality such as security, network management, and accessibility. You can set your browser to block or alert you about these cookies, but some parts of the site will not then work. These cookies do not store any personally identifiable information.</p>
                      </div>        
                    </div>                                    
                  </div>
                  <!-- Strictly Necessary cookies ends -->
                  <!-- Performance Cookies starts -->
                  <div id="performanceSection" class="newckidetbox" style="display:none">
                    <div class="newckidet_lblcntr">
                      <h4>Performance Cookies</h4>
                        <div class="newckidet_lblchkrgt">
                          <p>
                          	<c:if test="${'N' eq sessionScope.sessionData['cookiePerformanceDisabled']}">
                          	  <input id="perfCkId" type="checkbox" checked="checked" class="newcki_spngrp_chkbox">
							</c:if>
							<c:if test="${'N' ne sessionScope.sessionData['cookiePerformanceDisabled']}">
                          	  <input id="perfCkId" type="checkbox" class="newcki_spngrp_chkbox">
							</c:if>                            
                            <label for="perfCkId">
                              <span class="newckilbl_spntxt_act_i">Inactive</span>
                              <span class="newckilbl_spntxt_act">Active</span>
                            </label>
                          </p>
                        </div>
                      </div>
                      <div class="newckidetrgt_ovrflow">
                        <div class="newckidetinfo">                                
                          <p>These cookies allow us to count visits and traffic sources so we can measure and improve the performance of our site. They help us to know which pages are the most and least popular and see how visitors move around the site. The cookies collect information in a way that does not directly identify anyone. If you do not allow these cookies we will not know when you have visited our site, and will not be able to monitor its performance.</p>
                        </div>        
                      </div>                                    
                    </div>
                    <!-- Performance Cookies ends -->
                    <!-- Functional cookies starts -->
                    <div id="functionalSection" class="newckidetbox" style="display:none">
                      <div class="newckidet_lblcntr">
                        <h4>Functional Cookies</h4>
                          <div class="newckidet_lblchkrgt">
                          <p>
                            <c:if test="${'N' eq sessionScope.sessionData['cookieFunctionalDisabled']}">
                          	  <input id="functCkId" type="checkbox" checked="checked" class="newcki_spngrp_chkbox">
							</c:if>
							<c:if test="${'N' ne sessionScope.sessionData['cookieFunctionalDisabled']}">
                          	  <input id="functCkId" type="checkbox" class="newcki_spngrp_chkbox">
							</c:if>                             
                            <label for="functCkId">
                              <span class="newckilbl_spntxt_act_i">Inactive</span>
                              <span class="newckilbl_spntxt_act">Active</span>
                            </label>
                          </p>
                        </div>
                      </div>
                      <div class="newckidetrgt_ovrflow">
                        <div class="newckidetinfo">                                
                          <p>These cookies enable the website to provide enhanced functionality and personalisation. They may be set by us or by third party providers whose services we have added to our pages. They enable basic functions such as seeing recently viewed products or searches. If you do not allow these cookies, then some or all of these services may not function properly.</p>
                        </div>        
                      </div>                                    
                    </div>
                    <!-- Functional cookies ends -->
                    <!-- Targeting Cookies starts -->
                    <div id="targetingSection" class="newckidetbox" style="display:none">
                      <div class="newckidet_lblcntr">
                        <h4>Marketing Cookies</h4>
                          <div class="newckidet_lblchkrgt">
                            <p>
	                          <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
	                            <input id="targetCkId" type="checkbox" checked="checked" class="newcki_spngrp_chkbox">
							  </c:if>
							  <c:if test="${'N' ne sessionScope.sessionData['cookieTargetingCookieDisabled']}">
	                            <input id="targetCkId" type="checkbox" class="newcki_spngrp_chkbox">
							  </c:if>                              
                              <label for="targetCkId">
                                <span class="newckilbl_spntxt_act_i">Inactive</span>
                                <span class="newckilbl_spntxt_act">Active</span>
                              </label>
                            </p>
                          </div>
                        </div>
                        <div class="newckidetrgt_ovrflow">
                          <div class="newckidetinfo">                                
                            <p>These cookies may be set through our site by social media services or our advertising partners. Social media cookies enable you to share our content with your friends and networks. They are capable of tracking your browser across other sites and building up a profile of your interests. If you do not allow these cookies you may not be able to use or see the content sharing tools. Advertising cookies may be used to build a profile of your interests and show you relevant adverts on other sites. They do not store directly personal information, but are based on uniquely identifying your browser and internet device. If you do not allow these cookies, you will still see ads, but they will not be tailored to your interests.If you wish to inactivate these cookies after acceptance, please follow the advice in our cookies notice</p>
                          </div>        
                        </div>                                    
                      </div>
                      <!-- Targeting Cookies ends -->
                    </div>        
                  </div>        
                  <div class="newckibotwrap">
                    <div class="newckitoplft_bot">
                      <ul>
                        <li><a href="/degrees/cookies.html"><span>Cookie Notice</span></a></li>
                      </ul>
                    </div>
                    <div class="newckitoprgt_bot" id="saveCookieButton">
                      <a href="javascript:void(0)" class="savsettngsbtn">Save Settings</a>
                    </div>
                  </div>        
                </div>
             </div>
             <!-- Cookie settings ends -->
        <input type="hidden" id="targetCookieConsent" name="targetCookieConsent" value="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled'] ? '0' : '1'}" />
        <input type="hidden" id="perfomanceCookieConsent" name="perfomanceCookieConsent" value="${'N' eq sessionScope.sessionData['cookiePerformanceDisabled'] ? '0' : '1'}" />
        <input type="hidden" id="functionalCookieConsent" name="functionalCookieConsent" value="${'N' eq sessionScope.sessionData['cookieFunctionalDisabled'] ? '0' : '1'}" />