<%--
  * @purpose  This jsp is don't miss out pod
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 3-Nov-2015 Prabhakaran V.                546     modifid old file as part of redesign and functionality changes   546
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.SessionData, WUI.utilities.CommonFunction, WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>
<%
  String emailDomainJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.email.domain.js");
  CommonUtil util = new CommonUtil();
  String TCVersion = util.versionChanges(GlobalConstants.TERMS_AND_COND_VER);
  CommonFunction common = new CommonFunction();
  String[] yoeArr = common.getYearOfEntryArr();
  String YOE_1 = yoeArr[0];
  String YOE_2 = yoeArr[1];
  String YOE_3 = yoeArr[2];
  String YOE_4 = yoeArr[3];
%>
<c:if test="${empty sessionScope.userInfoList}">
  <div class="fwrp">
    <div class="fwrpi lgnFld chkbx_sel">
      <fieldset>
        <fieldset class="fl login_txt">
          <label for="fname">
            <h2 class="mb15">Don't miss out!</h2>
            <span id="existEmailError" class="fnt_lrg fnt_14">Receive a monthly newsletter packed with useful tips and updates to help you find the right uni.</span>
          </label>
        </fieldset>
      </fieldset>
      <fieldset class="row-fluid mb8 mt12">
        <fieldset class="fcwd fl mr20"> 
          <input type="text"  name="firstName" id="spmFirstName" title="First name" aria-label="First name" onclick="if (this.value == 'First name*') { this.value = ''; }" onfocus="clearDefaultLogin(this.id, 'First name*');" onblur="setDefaultLogin(this.id, 'First name*');" value="First name*" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}" class="ql-inpt mr20"/>
          <p class="err" id="firstName_spmerror" style="display:none;"></p>
        </fieldset>
        <fieldset class="fcwd fl mr20"> 
          <input type="text" name="surName" id="spmSurName" title="Last name" aria-label="Last name" onclick="if (this.value == 'Last name*') { this.value = ''; }" onfocus="clearDefaultLogin(this.id, 'Last name*');" onblur="setDefaultLogin(this.id, 'Last name*');" value="Last name*" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}" class="ql-inpt mr20"/>
          <p class="err" id="surName_spmerror" style="display:none;"></p>
        </fieldset>
        <fieldset class="fcwd fl"> 
          <input type="text" name="emailAddress" id="spmEmailAddress" title="Email address" aria-label="Email address" onclick="if (this.value == 'Email address*') { this.value = ''; }" onfocus="clearDefaultLogin(this.id, 'Email address*');" onblur="setDefaultLogin(this.id, 'Email address*');" onkeydown="hideEmailDropdown(event, 'autoEmailId');" value="Email address*" class="ql-inpt"/>
          <p class="err" id="emailAddress_spmerror" style="display:none;"></p>
          <p class="err" id="registerSpmErrMsg" style="display:none;"></p>
        </fieldset>
      </fieldset>                 
      <fieldset class="row-fluid mb10">
        <h3 class="fnt_lbd mb8 fnt_14">When would you like to start?*</h3>
        <fieldset class="ql-inp">               
          <span class="ql_rad">
            <input type="radio" name="yeartoJoinCourse"  class="fl mr5" value="<%=YOE_1%>"  class="yeartoJoinCourse1" title="<%=YOE_1%>" aria-label="Year <%=YOE_1%>"/>
            <label class="fnt_lrg fnt_14" for="yeartoJoinCourse1"><%=YOE_1%></label>
          </span>               
          <span class="ql_rad">
            <input type="radio" name="yeartoJoinCourse"  class="fl mr5" value="<%=YOE_2%>"  class="yeartoJoinCourse2" title="<%=YOE_2%>" aria-label="Year <%=YOE_2%>"/>
            <label class="fnt_lrg fnt_14" for="yeartoJoinCourse2"><%=YOE_2%></label>
          </span>
          <span class="ql_rad">
            <input type="radio" name="yeartoJoinCourse"  class="fl mr5" value="<%=YOE_3%>"  class="yeartoJoinCourse3" title="<%=YOE_3%>" aria-label="Year <%=YOE_3%>"/>
            <label class="fnt_lrg fnt_14" for="yeartoJoinCourse3"><%=YOE_3%></label>
          </span>
          <span class="ql_rad">
            <input type="radio" name="yeartoJoinCourse"  class="fl mr5" value="<%=YOE_4%>"  class="yeartoJoinCourse4" title="<%=YOE_4%>" aria-label="Year <%=YOE_4%>"/>
            <label class="fnt_lrg fnt_14" for="yeartoJoinCourse4"><%=YOE_4%></label>
          </span>
          <div class="clear"></div>
          <p class="err mt5 fl" id="yoe_spmerror" style="display:none;"> </p>
        </fieldset>
      </fieldset>  
      <fieldset>
        <fieldset class="fl login_txt pt15">
          <span class="chk_btn"><input type="checkbox" id="spamBox" class="chkbx1"><span class="chk_mark"></span></span>
          <label for="fname">
            <span id="existEmailError" class="fnt_lrg fnt_14">I confirm I'm over 13 and agree to the <a href="javascript:void(0);" onclick="window.open('<%=TCVersion%>','termspopup','width=650,height=400,scrollbars=yes,resizable=yes');" title="Whatuni community" class="link_blue fnt_lrg">terms and conditions</a> and <a  href="javascript:void(0);" onclick="showPrivacyPolicyPopup()" title="privacy notice" class="link_blue fnt_lrg">privacy notice</a>, and agree to become a member of the <a href="javascript:void(0);" onclick="window.open('<%=TCVersion%>','termspopup','width=650,height=400,scrollbars=yes,resizable=yes');" title="Whatuni community" class="link_blue fnt_lrg">Whatuni community</a>.<span class="as_trk">*</span></span>
          </label>
          <p class="err" id="sBoxChkErr" style="display:none;"></p>
        </fieldset>            
        <fieldset class="fr wd100 fsubg">
          <p id="msgParam"></p>
          <p id="loadinggif" style="display:none;" ><span id="loadgif"><img title="" alt="Loading" src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/indicator1.gif", 0)%>" class="loading"/></span></p>
          <a onclick="return newSpamBoxReg();" class="btn1 blue w219 mt5" title="GET FREE NEWSLETTERS" aria-label="GET FREE NEWSLETTERS">GET FREE NEWSLETTERS<em class="fa fa-long-arrow-right"></em></a>
          
        </fieldset>
      </fieldset>
    </div>
  </div>
  <%--Added auto email domain script for 21_02_2017, By Thiyagu G --%>
  <script type="text/javascript" id="domnScptId">
    var $mdv1 = jQuery.noConflict();
    $mdv1(document).ready(function(e){
      $mdv1.getScript("<%=CommonUtil.getJsPath()%>/js/emaildomain/<%=emailDomainJs%>", function(){        
        var arr = ["spmEmailAddress", "", ""];
        $mdv1("#spmEmailAddress").autoEmail(arr);        
    	 });      
    });
  </script>
  <input type="hidden" id="domainLstId" value='<%=java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.form.email.domain.list")%>' />
</c:if>
