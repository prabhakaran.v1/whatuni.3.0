<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil" %>
<section class="soc_art">
  <article class="content-bg">
    <article class="soc_pod mr40">
      <h2 class="fnt_lbk">Latest news</h2>
      <div class="soc_ico">
        <ul>
          <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
            <li id="twitterTab" onclick="showhidesocialTabs('twitter')" class="act"><i class="fa fa-twitter fa-1_5x"></i>Twitter</li>
          </c:if>
          <li id="facebookTab" onclick="showhidesocialTabs('facebook')"><i class="fa fa-facebook fa-1_5x"></i>Facebook</li>
          <li id="flickrTab" onclick="showhidesocialTabs('flickr')" class="fl_dts">Flickr</li>
        </ul>
      </div>
      <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
      <div id="twitterLnk" class="soc_cnt">
        <div class="soc_disp">                  
            <a class="twitter-timeline"  data-screen-name="" data-chrome="noheader nofooter transparent" height="400" width="570" data-border-color="#dbdbdb" data-link-color="#40ccfe"  data-cards="hidden" href="https://twitter.com/whatuni"  data-widget-id="392256738243522561">Tweet by @Whatuni</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </div>
      </div>
      </c:if>
      <c:if test="${'N' ne sessionScope.sessionData['cookieTargetingCookieDisabled']}">
        <script type="text/javascript">
          jQuery(document).ready(function() {
            showhidesocialTabs('facebook');
          });
        </script>
      </c:if>
      <div id="facebookLnk" class="soc_cnt" style="display:none;">
        <div class="soc_disp">  
          <div id="fb-root"></div>
          <div id="facebookDiv"></div>
        </div>     
      </div>
       <div id="flickrLnk" class="soc_cnt" style="display:none;">
        <div class="soc_disp">  
            <div id="flickr_badge_wrapper">                
                
            </div>
        </div>     
      </div>      
    </article>
    <c:if test="${not empty requestScope.reviewList}">
      <article class="lat_rev">
        <h2 class="fnt_lbk">Reviews you should read</h2>
        <c:forEach items="${requestScope.reviewList}"  var = "reviewList" varStatus="index">
          <c:set var="indexIntVal" value="${index.index}"/>
          <%if((Integer)pageContext.getAttribute("indexIntVal")==0){%>
            <div class="rev_blk first">
          <%}else{%>
            <div class="rev_blk">
          <%}%>
            <c:if test="${not empty reviewList.courseTitle}"><h4 class="fnt_lbd">${reviewList.courseTitle}</h4></c:if>
            <c:if test="${not empty reviewList.collegename}"><h4 class="fnt_lrg mt5">${reviewList.collegename}</h4></c:if>
            <c:if test="${not empty reviewList.overAllRating}"><div class="rev_return mt10 mb15 fl w100p"><span class="rat${reviewList.overAllRating}"></span></div></c:if>
            <c:if test="${not empty reviewList.reviewTitle}"><h5 class="fnt_lbd mt10">$reviewList.reviewTitle}</h5></c:if>
            <c:if test="${not empty reviewList.overallRatingComment}"><p class="fnt_lrg mt5">${reviewList.overallRatingComment}</p></c:if>
            <a href="${reviewList.reviewDetailUrl}" class="fnt_lbd">READ MORE</a>
          </div>
        </c:forEach>
        <a class="btn1 fl" href="/degrees/university-course-reviews/highest-rated/0/0/1/reviews.html ">SEE ALL REVIEWS<i class="fa fa-long-arrow-right"></i></a>
      </article>
    </c:if>
  </article>
</section>
   