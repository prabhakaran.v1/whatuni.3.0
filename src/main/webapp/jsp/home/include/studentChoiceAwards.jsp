<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil" %>

<%
  String studAwdCurYear = "";      
  String awardsImg = "";
  String tempImg = "";
  if(request.getAttribute("studAwdCurYear")!=null && !"".equals(request.getAttribute("studAwdCurYear"))){
    studAwdCurYear = (String)request.getAttribute("studAwdCurYear");    
    tempImg = CommonUtil.getImgPath("/", 0);
    awardsImg = tempImg + "wu-cont/images/awards/"+ studAwdCurYear + "/video_top.jpg"; //Geeting award year image code added by Prabha on 29_Mar_2016
  } 
  String awardsURL        =  "/student-awards-winners/university-of-the-year/";  
%>

<section class="sc_awards">
  <article class="content-bg">
    <article class="awrd_vdo">
      <h2> <a class="fnt_lbk" href="<%=awardsURL%>">Student Choice Awards</a></h2>
      <c:if test="${not empty requestScope.studentChoiceAwrdVideo}">
        <%--Changed all student choice awards lazyload from img_px to trans_img_px.gif on 16_May_2017, By Thiyagu G.--%>
        <a  id="img_std_choice" onclick="showhide('video_emb_std_choice','img_std_choice','${requestScope.studentChoiceAwrdVideo}',631,353,'<%=awardsImg%>')" style="display:block;">
          <i id="img_std_choice_icon" class="fa fa-play-circle-o fa-8x"></i>        
          <img width="631" id="std_video" height="353" src="<%=CommonUtil.getImgPath("/wu-cont/images/trans_img_px.gif", 1)%>" data-src="<%=awardsImg%>" alt="Students choice awards video">
        </a>
        <iframe id="video_emb_std_choice" width="631" height="0" frameborder="0" style="margin-left:10px;display:none;" allowfullscreen=""></iframe>
      </c:if>
    </article>
    <c:if test="${not empty requestScope.awardsList}">
      <article class="top_uni">
        <h4>Your Top 10 Universities</h4>
        <h5 class="mt10">OVERALL RATING</h5>
        <ol>
         <%int rowid = 0;String className = "str_gr";%>
          <c:forEach items="${requestScope.awardsList}" var="awardsList" varStatus="id">
           <c:set var="id" value="${id.index}"/> 
           <% rowid = Integer.parseInt(String.valueOf(pageContext.getAttribute("id")));
              if(rowid%2 == 0){ className = "str_gr";}else{ className = "";}
           %>
           <li class="<%=className%>"><a href="${awardsList.sponsorURL}">${awardsList.collegeNameDisplay}</a>
             <c:if test="${awardsList.positionId eq 1}"><i class="fa fa-trophy fa-1_4x"></i></c:if></li>
          </c:forEach>
        </ol>
      </article>
    </c:if>
    <article class="content-bg fl">
      <a class="btn1" href="<%=awardsURL%>">See all Categories<i class="fa fa-long-arrow-right"></i></a>
    </article>
  </article>
</section>
   