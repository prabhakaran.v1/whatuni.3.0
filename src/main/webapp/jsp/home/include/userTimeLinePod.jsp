<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<c:set var="pagename3">
  <tiles:getAsString name="pagename3" ignore="true"/>
</c:set>

<%
  int mileCount           = 1;
  int liClassInd          = 0;
  int mileDest            = 0;
  String mileHeading      = "";
  String mileColour       = "";
  String youShouldbeHere  = "";
  String yourAreHere      = "";
  String deadLineStge     = "";
  String liClassName      = "";
  String spnClassName     = "";   
  String youAreActive     = "act";
  String youshouldActive  = "ysh";
  String hrefType         = "";
  String stageStatus      = "";
  String userTimeLineURL  = "";
  String selfTimeLineURL  = "";
  boolean reached         = false;        
  String pagename3 = "";
  
  String loadWhatHaveYouDonePod =  request.getParameter("fromHomePage");
  String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");     
  if(!GenericValidator.isBlankOrNull(pageContext.getAttribute("pagename3").toString())){
	  pagename3 = (String)pageContext.getAttribute("pagename3");
	 // System.out.println("Pagename3 -->" + pagename3);
  }
%>

<c:if test="${pagename3 ne 'browseMoneyPageResults.jsp'}">
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
</c:if>
<!-- Timeline Starts-->
<c:if test="${not empty requestScope.userTimeLinePodList}">
<section class="timeline" id="dskTimeline">
<c:if test="${empty requestScope.userTimeLinePage}">  
  <a href="javascript:hideTimeLinePod()" class="tab_arw noalert" id="hideTimeLine"></a>
</c:if>  
<div class="content-bg" id="timeLineCnt">
  <div class="row-fluid">
    <div class="container">            
    
      <c:if test="${not empty requestScope.userTimeLinePage}">  
        <h2 class="row-fluid txt_cnr fl fnt_lbk">Hi<%=(session.getAttribute("showUserName")!=null && !"".equals(session.getAttribute("showUserName"))) ? " "+session.getAttribute("showUserName") : ""%>, nice to see you again!</h2>        
      </c:if>  
      <article class="tml fl">	                              
        <ul class="bjqs non-tablet" id="timeLineUL">
          <c:forEach var="userTimeLine" items="${requestScope.userTimeLinePodList}" varStatus="index" > 
            <c:set var="indexIntValue" value="${index.index}"/>  
            <c:set var="tLineId" value="${userTimeLine.timeLineId}"/>
            <c:set var="tlDispSeq" value="${userTimeLine.tlDispSeq}"/>
            <%String tLineId =  (String)pageContext.getAttribute("tLineId");
            String tlDispSeq =  (String)pageContext.getAttribute("tlDispSeq");
            mileDest = ((Integer) pageContext.getAttribute("indexIntValue")) +1;
            %>
                                                  
            
            <c:if test="${not empty userTimeLine.tlYouAreHereId}">
              <c:set var="youAreHere" value="${userTimeLine.tlYouAreHereId}"/>
              <% String youAreHere =  (String)pageContext.getAttribute("youAreHere");
                 yourAreHere = youAreHere;
              %>
            </c:if>   
            <c:if test="${not empty userTimeLine.tlYouShouldBeHereId}">
              <c:set var="youShldHere" value="${userTimeLine.tlYouShouldBeHereId}"/>
              <%youShouldbeHere =  (String)pageContext.getAttribute("youShldHere");%>
            </c:if>                                              
            <c:if test="${not empty userTimeLine.tlDeadLineFlag}">
              <c:set var="deadLine" value="${userTimeLine.tlDeadLineFlag}"/>
              <%deadLineStge =  (String)pageContext.getAttribute("deadLine");%>
            </c:if> 
              
              
            <c:if test="${not empty userTimeLine.tlURL}">
              <c:set var="timeLineURL" value="${userTimeLine.tlURL}"/>
              
              <% String timeLineURL = (String)pageContext.getAttribute("timeLineURL");
              userTimeLineURL = timeLineURL.trim();
              
              if(timeLineURL.indexOf("www.whatuni.com") != -1){
                hrefType        = "_self";
                selfTimeLineURL = "#";                    
              }else{
                hrefType        = "_blank";
                selfTimeLineURL = timeLineURL.trim();
              }
              if(timeLineURL.indexOf("www.")==-1){                    
                userTimeLineURL = "";
              }%>
            </c:if>                
            <c:if test="${empty userTimeLine.tlURL}">
              <c:if test="${userTimeLine.tlDeadLineFlag eq 'Y'}">
                <% 
                  hrefType        = "_self";
                  selfTimeLineURL = "#";     
                  userTimeLineURL = "";
                %>                    
              </c:if>
            </c:if>                                                
            <c:if test="${not empty userTimeLine.tlItemStatus}">
              <c:set var="mileStatus"  value="${userTimeLine.tlItemStatus}"/>
              <%
                String mileStatus = (String) pageContext.getAttribute("mileStatus");
                if("completed".equalsIgnoreCase(mileStatus)) {                  
                  mileColour = "bg_orng_lt";
                }else{
                  mileColour = "";
                }
                stageStatus = mileStatus;
              %>
            </c:if>                                                                                 
            <%
              if(liClassInd<6) { 
                liClassName = "left";
              }else if(liClassInd>=6 && liClassInd<=23) {
                liClassName = "";                  
              }else{
                liClassName = "right";
              }                                                     
              if(!"".equals(yourAreHere) && !"".equals(youShouldbeHere) && Integer.parseInt(youShouldbeHere)<Integer.parseInt(yourAreHere) && mileDest==Integer.parseInt(tLineId)) {                    
                youAreActive    = "act_rht";
                youshouldActive = "ysh_lft";
              }else{
                youAreActive    = "act"; 
                youshouldActive = "ysh";
              }                 
              if(!"".equals(yourAreHere) && !"".equals(youShouldbeHere) && yourAreHere.equals(youShouldbeHere) &&  yourAreHere.equals(String.valueOf(mileDest))   && String.valueOf(mileDest).equals(request.getAttribute("userTimeLineLen")) && "completed".equals(stageStatus)){              
                yourAreHere     = "";
                youShouldbeHere = "";
                reached = true;
              }                                                     
              if(tLineId.equals(yourAreHere)) {                    
                liClassName   = (!"".equals(liClassName) ? liClassName + " "+youAreActive : youAreActive);
                spnClassName  = "yrh";
                mileColour    = "bg_orng_dk";
              }else if(tLineId.equals(youShouldbeHere)) {
                liClassName   = (!"".equals(liClassName) ? liClassName + " "+youshouldActive : youshouldActive);
                spnClassName  = "ysh";                
              }else if("Y".equals(deadLineStge)) {
                mileColour    = "bg_red_lt";
              }else{                
                spnClassName  = "yrh";                        
              }                       
            %>                                                                                           
            <c:if test="${userTimeLine.tlItemType eq 'M'}">
              <c:set var="mileStageHead" value="${userTimeLine.tlTitle}"/>
              <% String mileStageHead = (String)pageContext.getAttribute("mileStageHead");
                 mileHeading = mileStageHead;
              %>   
              <li class="tmc fl" id="${userTimeLine.tlTitle}"> 
                <ul class="tmb cc<%=mileCount%>"> 
                  <li class="<%=liClassName%>"> 
                  <c:if test="${userTimeLine.tlItemActionType eq 'I'}">                        
                    <a href="<%=selfTimeLineURL%>" target="<%=hrefType%>" class="<%=mileColour%>" onclick="saveUserTimeLine('<%=tLineId%>','<%=userTimeLineURL%>','<%=hrefType%>')">                          
                  </c:if>
                  <c:if test="${userTimeLine.tlItemActionType ne 'I'}">
                    <a href="${userTimeLine.tlURL}" target="<%=hrefType%>" class="<%=mileColour%>">
                  </c:if>  
                  <span class="<%=spnClassName%>"></span>
                  <div class="ttip">
                    <span class="icon_ttp"></span>
                    <c:if test="${not empty userTimeLine.tlImageURL}">
                      <span class="fl"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="${userTimeLine.tlImageURL}" alt=""></span>
                    </c:if>
                    <c:if test="${not empty userTimeLine.tlDescription}">  
                      <span class="pl20 fl fnt_lbd desc">${userTimeLine.tlDescription}</span>
                    </c:if>  
                  </div>
                  <%if(!"".equals(mileColour) && "bg_orng_dk".equals(mileColour)){%>
                    <span class="title fnt_lbk">${userTimeLine.tlTitle}</span>
                  <%}%>
                  </a>                    
                  </li>                                         
                  <%if(request.getAttribute("userTimeLineLen").equals(String.valueOf(((Integer) pageContext.getAttribute("indexIntValue"))+1))){%> 
                </ul>   
                <p id="withLink" class="tmt">                   
                  <span class="v_line"></span>                   
                  <span class="txt"><%=mileHeading%></span>
                </p>                                
              </li>                    
              <%if(reached){%><span class="congs"></span><%}%><%}%>  
            </c:if>                                           
            <c:if test="${userTimeLine.tlItemType eq 'NS'}">
              <li class="<%=liClassName%>"> 
                <c:if test="${userTimeLine.tlItemActionType eq 'I'}">                                          
                  <a href="<%=selfTimeLineURL%>" target="<%=hrefType%>" class="<%=mileColour%>" onclick="saveUserTimeLine('<%=tLineId%>','<%=userTimeLineURL%>','<%=hrefType%>')">                      
                </c:if>  
                <c:if test="${userTimeLine.tlItemActionType ne 'I'}">
                  <a href="${userTimeLine.tlURL}" target="<%=hrefType%>" class="<%=mileColour%>">
                </c:if>
                <span class="<%=spnClassName%>"></span>
                <div class="ttip">
                  <span class="icon_ttp"></span>
                  <c:if test="${not empty userTimeLine.tlImageURL}">
                    <span class="fl"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="${userTimeLine.tlImageURL}"  alt=""></span>
                  </c:if>  
                  <c:if test="${not empty userTimeLine.tlDescription}">
                    <span class="pl20 fl fnt_lbd desc">${userTimeLine.tlDescription}</span>
                  </c:if>  
                </div>
                <%if(!"".equals(mileColour) && "bg_orng_dk".equals(mileColour)){%>
                  <span class="title fnt_lbk">${userTimeLine.tlTitle}</span>
                <%}%>
                </a>                                                                        
              </li> 
              <c:if test="${userTimeLine.tlItemType ne userTimeLinePodList[index.index + 1].tlItemType}">     
              </ul> 
              <p id="withLink" class="tmt">
                <%if(mileCount==1){%>
                  <span class="v_line first"></span>
                <%}else{%>  
                  <span class="v_line"></span>
                <%}%>
                  <span class="txt"><%=mileHeading%></span>
                </p>
              </li>
              <%mileCount++;%>
              </c:if>     
            </c:if>                                             
            <%liClassInd++;if(liClassInd==30){liClassInd=0;}%> 
          </c:forEach>                
        </ul>                       
      </article>      
    </div>
  </div>  
</div>
<!--Added hidden type to handle mysettings alert Jan-27-16 Indumathi.S-->
<input type="hidden" id="definetLineId" value=""/>
<input type="hidden" id="defineUserTimeLineURL" value=""/>
<input type="hidden" id="defineHrefType" value=""/>
<input type="hidden" id="pageNameTimeLine" value="<%=pagename3%>"/>

 <%if(loadWhatHaveYouDonePod != null && ("yes").equals(loadWhatHaveYouDonePod)){%>  
 <%--
  <jsp:include page="/jsp/home/include/userTimeLinePodResponsive.jsp">
    <jsp:param name="isHomePage" value="yes"/>
  </jsp:include>  
  <script type="text/javascript">showDesktopTimeline();</script>
 --%>
  <c:if test="${not empty requestScope.userTimeLinePage}">      
    <h2 id="respUsrDispName" class="row-fluid txt_cnr fl fnt_lbk usr_name">Hi<%=(session.getAttribute("showUserName")!=null && !"".equals(session.getAttribute("showUserName"))) ? " "+session.getAttribute("showUserName") : ""%></h2>
  </c:if>      
  <jsp:include page="/jsp/home/include/whatHaveYouDone.jsp"/>
 <%}%>
</section>
</c:if>
<!-- Timeline Ends-->


<c:if test="${not empty requestScope.userTimeLinePodList and empty requestScope.userTimeLinePage}">
  <script type="text/javascript">
    viewUserTimeLinePod();    
  </script>
</c:if>


<c:if test="${not empty requestScope.userTimeLinePodList}">
  <c:if test="${requestScope.userTimeLinePage eq 'home'}">    
    <script type="text/javascript">showHomeTimeLineImg();</script>
  </c:if>
</c:if>

