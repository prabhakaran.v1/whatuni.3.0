<%@page import="org.apache.commons.validator.GenericValidator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String gaOnclickMethod = "";
String location ="";
String subject = "";
String university = "";
%> 
<!-- You recently search start -->
<c:if test="${not empty requestScope.recentSearchFilterList}">	        
  <section class="ltst_art" id="recentSearchFilterPod">
		<div class="content-bg">
			<div class="rct_src">
				<div class="rct_src_lft">
					<h4 class="rct_head fl">You recently searched for</h4>
					<c:if test="${not empty requestScope.recentSearchDate}">
					  <div class="srcd_date fl">Searched on ${requestScope.recentSearchDate}</div>
					</c:if>
				</div>
				<div class="rct_src_rit">	
					<div class="yrcsr_tag_pod">
					  <c:forEach var="filterList" items="${requestScope.recentSearchFilterList}">
						  <a her="javascript:void(0);" class="rcsr_btn">${filterList.filterName}</a>
						  <c:if test="${'search-subject' eq filterList.description}">						    
						    <c:set var="gaSubject" value="${filterList.filterName}"/>
						    <%subject = (String)pageContext.getAttribute("gaSubject");%> 
						  </c:if>
						  <c:if test="${'university' eq filterList.description}">						    
						    <c:set var="gaUniversity" value="${filterList.filterName}"/>
						    <%university = (String)pageContext.getAttribute("gaUniversity");
						      university = !GenericValidator.isBlankOrNull(university) ? university.toLowerCase() : "";
						    %> 
						  </c:if>
						  <c:if test="${'location' eq filterList.description}">
						    <c:set var="gaLocation" value="${filterList.filterName}"/>
						    <%location = (String)pageContext.getAttribute("gaLocation");
						      location = !GenericValidator.isBlankOrNull(location) ? location.toUpperCase().trim().replaceAll(" ","-") : "UK";
						    %>						    
						  </c:if>
						</c:forEach>						
					</div>
					<c:if test="${not empty requestScope.recentSearchFilterList}">
					  <%if(!GenericValidator.isBlankOrNull(subject)){
					    location = !GenericValidator.isBlankOrNull(location) ? location : "UK";
					    //gaOnclickMethod = "onclick=\"GANewAnalyticsEventsLogging('Search', '"+subject+"', '"+location+"');\"";
					  }else if(!GenericValidator.isBlankOrNull(university)){gaOnclickMethod = "onclick=\"GANewAnalyticsEventsLogging('University Search', 'Search', '"+university+"')\"";
					  }%>					  					  
					  <div class="rtn_to_src fl"><a href="${requestScope.recentSearchFilterUrl}" <%=gaOnclickMethod%>>RETURN TO SEARCH<span class="blue_arw"></span></a></div>
				  </c:if>				
				</div>
			</div>
		</div>
	</section>	
</c:if>
<!-- You recently search end -->