<%@ page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction"%>

<%
  String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");  
  String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
  String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
  String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
%>  
<script type="text/javascript" language="javascript">  
  var flexslider;
  var dev = jQuery.noConflict();
  dev(document).ready(function(){    
    adjustStyle();      
    loadHomeScript();     
  });
  adjustStyle();
  function jqueryWidth() {
    return dev(this).width();
  }
  function adjustStyle() {
    var width = document.documentElement.clientWidth;    
    if(width <= 480) {    
      if(dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }        
      <%if(("LIVE").equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}%>       
    }else if((width > 480) && (width <= 992)) {      
      if(dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }       
      <%if(("LIVE").equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}%>
    }else{
      if(dev("#viewport").length > 0) {
        dev("#viewport").remove();
      }
      dev(".hm_srchbx").hide();
      document.getElementById('size-stylesheet').href = "";        
    }    
  }
  dev(window).on('orientationchange', orientationChangeHandler);
  function orientationChangeHandler(e) {
    setTimeout(function() {
      dev(window).trigger('resize');
    }, 500);
  }
  dev(window).resize(function() {    
    loadHomeScript();
    adjustStyle();
    setTimeout(function() {
     dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
     dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
     }, 200);
  });  
  function loadHomeScript(){
    var screenWidth = jqueryWidth();    
    if((screenWidth >=320) && (screenWidth <= 992)) {            
      dev('#wdt,#nst').addClass('slides').find('li').css('float', 'left');  
      dev('#wdt,#nst').find('.icn').css('min-height', '79px').css('width', '80px');
      dev('#wdt,#nst').find('.icn span').css('width','80px');
      dev('#wdt,#nst').find('.wyd.bg_wt').css('width','100%').css('height', '132px').css('min-height', '132px');
      dev('#wdt,#nst').find('.wyd.bg_gn').css('width','100%').css('height', '132px').css('min-height', '132px');
      dev('.btn_view').css('float','right').css('margin-top','20px');
      dev('#wdt,#nst').find('.ics_nxt').css('background-position', '-400px -1px');  
      dev('.rec_act_wrap .ract_rht').css('width', '100%').css('width', '-=42px');             
      dev("#myfinalHome").hide();
      dev("#homeFinalChResp").show();      
      if(dev("#dispHomeFinalChoice").val()=="N"){
        dev("#timeLineCnt").hide();       
        dev("#respUsrDispName").show();
      }
    }else{	    
      dev('#wdt,#nst').removeClass('slides').removeAttr("style").find('li').removeAttr("style");
      dev('.rec_act_wrap .ract_rht').removeAttr("style");
      dev('#wdt,#nst').find('.wyd.bg_wt').css('height', 'auto');
      dev('#wdt,#nst').find('.wyd.bg_gn').css('height', 'auto');
      dev('#wdt,#nst').find('.icn').css('min-height', '79px').css('width', '80px');        
      if(dev("#myfinalHome")){  
        if(dev("#dispHomeFinalChoice").val()=="Y"){          
          dev("#myfinalHome").show();
          dev("#homeFinalChResp").hide();            
        }  
      }       
      if(dev("#dispHomeFinalChoice").val()=="N"){
        dev("#timeLineCnt").show();  
        dev("#respUsrDispName").hide();
      }
    }
    if((screenWidth < 462)){
      dev('#wdt,#nst').find('.icn').css('min-height', '206px').css('width', '56px');
      dev('.btn_view').css('float','right').css('margin-top','11px').css('margin-right','19px');
      dev('#wdt,#nst').find('.wyd.bg_wt').css('min-height', '206px');
      dev('#wdt,#nst').find('.wyd.bg_gn').css('min-height', '187px');
      dev('#wdt,#nst').find('.icn span').css('width','52px');      
    }    
    if(flexslider!=undefined) {           
      flexslider.vars.minItems = getWuGridSize();
      flexslider.vars.maxItems = getWuGridSize();
      flexslider.slides.width( flexslider.computedW);
      flexslider.update(flexslider.pagingCount);
      flexslider.setProps();
    }     
  }  
  function showDesktopTimeline(){
    dev('#respTimeline').hide();     
  } 
</script>