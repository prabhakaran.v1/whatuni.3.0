<%@page import="org.apache.commons.validator.GenericValidator"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%
  String placeToBeIncluded = GenericValidator.isBlankOrNull(request.getParameter("PLACE_OF_INCLUSION")) ? "NEED_HELP" : request.getParameter("PLACE_OF_INCLUSION");
  String btnStyle = "";
  String divTopClass = "";
  String textType = "";
  String carosalSpecificText2 = "We know things aren't easy right now, but I can help you find your perfect course and uni.";
  String carosalSpecificText3 = "Would you like some help in your search?";
  String clrCarosalSpecificText3 = "";
  String questionText01Id  = "questionTxt_01";
  String questionText02Id  = "questionTxt_02";
  String footerId = "footerContent";
  String footerNoId = "footerNo";
  String carosalSpecificText4 = "OK I'll take you to the Clearing search now";
  String frm = "";
  String noTextType = "";
  if("CAROUSAL".equals(placeToBeIncluded))  {
    btnStyle = "style='width: 1263px;'";
    divTopClass = "cbhro_sec";
    textType = "_carosal";
    carosalSpecificText2 = "Whether you have no clue, some clue or are all clued up on what you want to study or the career you want, I can help.";
    carosalSpecificText3 = "So, how about we get started...";
    questionText01Id = "questionTxt_01_home_page";
    questionText02Id = "questionTxt_02_home_page";
    footerId = "footerContent_home_page";
    footerNoId = "footer_no_home";
    frm = "CAROSAL";
  }
  CommonFunction common = new CommonFunction();
  String userJourney = common.getClearingUserJourneyFlag(request);
  String question3Style = "display: block;";
  String clrQuestion3Style = "display: none;";
  String question4Style = "display: none;";
  if("CLEARING_JOURNEY".equalsIgnoreCase(userJourney)) { 
    carosalSpecificText2 = "Do you want me to help you find the perfect course in Clearing this year?";
    clrCarosalSpecificText3 = "Ok, come back anytime if you want me to help you find the perfect course.";
    question3Style = "display: none;";
    noTextType = "";
    if("CAROUSAL".equals(placeToBeIncluded))  {
      noTextType = "_carosal"; // for clearing question when clicked no separation of carosal and chatbot promo
    }
  }
  String userName = !GenericValidator.isBlankOrNull((String)session.getAttribute("showUserName")) ? ((String)session.getAttribute("showUserName")) : "";
%>

<div data-page="fav_blank" class="page quiz_page <%=divTopClass%>">
   <div class="page-content" id="quiz-cont">
      <div class="cht_cnt" id="mainSection">
         <!--Chat bot-->
         <div class="cht_bot">
            <div class="cht_ico"></div>
            <div class="cht_msg cht_min"> 
               <%--<img id="image_10" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif",0)%>" alt="" width="40" style="display: none;">--%>
               <%if(!GenericValidator.isBlankOrNull(userName) && !"Anonymous".equalsIgnoreCase(userName)){%>
               <span id="<%=questionText01Id%>" style=""><spring:message code="chatbot.popup.first.message.logged.in.user.with.arugument" arguments="<%=userName%>"/><spring:message code="chatbot.popup.first1.message.logged.in.user"/> </span>
               <%}else{%>
                 <span id="<%=questionText01Id%>" style=""><spring:message code="chatbot.popup.first.message.not.logged.in.user"/></span>
               <%}%>
               <span id="clr_questionTxt_03<%=textType%>" style="<%=clrQuestion3Style%>"><%=clrCarosalSpecificText3%></span> <%--To show the question 3 separately in top in clearing journey after clicking no--%>
            </div>
         </div>
          <div class="cht_bot std_mes" id="<%=questionText02Id%>">
              <div class="cht_ico"></div>
              <div class="cht_msg cht_min"> 
                 <%-- <img id="image_10" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif",0)%>" alt="" width="40" style="display: none;"> --%>
                 <span id="questionTxt_02<%=textType%>" style=""><%=carosalSpecificText2%></span> 
              </div>
          </div> 
          
          <div class="cht_bot std_mes" id="question3<%=noTextType%>" style="<%=question3Style%>">
              <div class="cht_ico"></div>
              <div class="cht_msg cht_min"> 
                 <%-- <img id="image_10" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif",0)%>" alt="" width="40" style="display: none;"> --%>
                 <span id="questionTxt_03<%=textType%>" style=""><%=carosalSpecificText3%></span> 
              </div>
          </div> 
          
          <div class="cht_bot std_mes" id="question4<%=textType%>" style="<%=question4Style%>">
              <div class="cht_ico"></div>
              <div class="cht_msg cht_min "> 
                 <%--<img id="image_10" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif",0)%>" alt="" width="40" style="display: none;">--%>
                 <span id="questionTxt_04<%=textType%>" style=""><%=carosalSpecificText4%></span> 
              </div>
          </div>
          
          <div class="cht_bot std_mes" id="question5<%=textType%>" style="display:none;">
              <div class="cht_ico"></div>
              <div class="cht_msg cht_min cht_loader"> 
                 <img id="loading_image_5<%=textType%>" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif",0)%>" alt="" width="40" style="display: none;">
              </div>
          </div>
          
      </div>
   </div>
   <!-- Footer Section -->
   <div id="<%=footerId%>" class="foot_bar" style="display: block;">
      <div id="yesNoDiv" class="yesNoDiv" style="">
         <ul>
            <%if("CLEARING_JOURNEY".equalsIgnoreCase(userJourney)) { %>
             <li <%=btnStyle%>><a class="btn_act btn_bor" id="<%=footerNoId%>" href="javascript:void(0);" onclick="chatPromoContentDisplay('NO_CHAT_PROMO',event, '<%=frm%>');desktopHide('<%=footerNoId%>');GANewAnalyticsEventsLogging('Chatbot - Clearing', 'Clearing', 'Start');GANewAnalyticsEventsLogging('Chatbot - Clearing', 'Clearing', 'No');" tabindex="-1">No</a></li>
             <li <%=btnStyle%>><a class="btn_act btn_bor" href="javascript:void(0);" onclick="clearingPopupClose('clearing', 'chatbot', '<%=frm%>');">Yes</a></li>
             <%} else {%>
             <li <%=btnStyle%>><a class="btn_act btn_bor" href="javascript:void(0);" onclick="GANewAnalyticsEventsLogging('Chatbot - Clearing', 'Non-Clearing', 'Start');openLightBox('chatbot', 'true');" tabindex="-1">Yes sure</a></li>
             <%}%>
         </ul>
      </div>
   </div>
</div>
<%if("CLEARING_JOURNEY".equalsIgnoreCase(userJourney) && "CAROSAL".equalsIgnoreCase(frm)){%>
<input type="hidden" id="chatBotUserType" value="<%=userJourney%>_<%=frm%>"/>
<%}%>