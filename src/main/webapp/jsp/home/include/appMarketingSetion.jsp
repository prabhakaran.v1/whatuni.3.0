<%@page import="WUI.utilities.CommonFunction, WUI.utilities.CommonUtil, mobile.util.MobileUtils, org.apache.commons.validator.GenericValidator, com.layer.util.SpringConstants" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--
  * @purpose  This is App marketing section on home page..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 27-Jul-2017 Prabhakaran V.               567     First Draft                                                       567 
  * *************************************************************************************************************************
  * 08_Aug_2017 Prabhakaran V.               568      Lazy loading the image                                           568
  * 23_Oct_2018 Sangeeth.S                   582      Text changes
  * 07_May_2019 Hema.S                       589      Added erica image slot in home page 
--%>
<%--Added on 12_Sep_2017, By Thiyagu G--%>
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/mob_app_120319.css" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/eblp_hm_css_191119.css" media="screen" />
<% 
CommonFunction common = new CommonFunction();
CommonUtil util = new CommonUtil();
String userList = "NO";
if(session.getAttribute("userInfoList") != null){
userList = "YES";
}
boolean mobileFlag = new MobileUtils().userAgentCheck(request);
String appStoreURL  =  common.getWUSysVarValue("WU_APP_STORE_URL");
String androidAppFlag = common.getWUSysVarValue("ANDROID_APP_ON_OFF");
String clearingonoff = common.getWUSysVarValue("CLEARING_ON_OFF");
String appStoreURLForAndroid = ("ON".equalsIgnoreCase(androidAppFlag)) ? common.getWUSysVarValue("ANDROID_APP_URL") : "";
String CDNPath = util.getImgPath("", 1);
String fromPage = request.getParameter("fromPage");
String appCategory = "the new Whatuni iOS";
String availableText = "Download our app now!";
String appTxt = "Whether you can't decide on what to study or struggling to pick a location, our app can help.";
String appPromotionText = "";
String imageName = "hm_phone@1x";                
if("ON".equalsIgnoreCase(androidAppFlag)){
  availableText = "Get matched to uni courses";
  appCategory = "the Whatuni";
  appTxt = "Whether you can't decide on what to study or you're struggling to pick a perfect location, our app can help!";
}
appPromotionText = "With "+appCategory+" app it's never been easier to find your future. ";//Added space after full stop for UAT feed back Oct_23_18 rel
if("ON".equalsIgnoreCase(clearingonoff)){
  availableText = "Clearing & Adjustment made easy!"; 
  appPromotionText = "";
  appTxt = "Find thousands of university clearing courses in minutes and directly contact university clearing hotlines on the app.  Everything you need to get through clearing & adjustment on your smartphone.";
  imageName = "clr_hm_phone@1x";
  }
  String imagePath = "/wu-cont/images/thumbnails/"+ imageName + ".png";
  imagePath = CommonUtil.getImgPath(imagePath, 0);
  String ericaPageSlot = common.getWUSysVarValue("ERICA_LANDING_PAGE_HERO_IMAGE");
  pageContext.setAttribute("PRE_CLEARING_URL", SpringConstants.PRE_CLEARING_LANDING_URL);
%>

<c:if test="${COVID19_SYSVAR eq 'ON'}">
  <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
  <section class="covidinfo">
    <article class="content-bg" onclick="window.location.href = '${teacherSectionUrl}';">
      <div onclick="window.location.href = '${teacherSectionUrl}';" class="covidbox_fullclick"></div>
      <article class="covinfo_box" onclick="window.location.href = '${teacherSectionUrl}';">
        <h3>Coronavirus (COVID-19) advice and student support</h3>
        <p>All the latest information from our university and student community</p>
        <a href="${teacherSectionUrl}" class="covid_vmorebtn">VIEW MORE 
        <%if(session.getAttribute("userInfoList") != null || mobileFlag){%>
          <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_wht_arw.svg", 0)%>" alt="arrow image"/>
        <%}else{ %>
          <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_wht_arw.svg", 0)%>" alt="arrow image" />
        <%}%>
        </a>
      </article>
    </article>
  </section>
</c:if>
  <!-- Mobile App Section -->
<%if("OFF".equalsIgnoreCase(ericaPageSlot)){%>
<section class="pp_cnt anv_cnt2">
  <article class="pp_wrap">
    <div class="anv_wrp2">   
      <div class="anv_rgt post_lch">
        <h2 class="counter"><%=availableText%></h2>
        <p class="par_cnt"><%=appPromotionText%><%=appTxt%></p>
        <div class="fm_btn">      
          <a class="btn1" href="/whatuni-mobile-app" onclick="alpGaLogging();" target="_blank">FIND OUT MORE <i class="fa fa-long-arrow-right"></i></a>
          <div class="app_btns">
            <a class="app_str" href="<%=appStoreURL%>" target="_blank" title="Whatuni app download store"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/wu_app_ios_btn.svg",0)%>" alt="Whatuni app download store"/></a>
            <%if("on".equalsIgnoreCase(androidAppFlag)){%>
              <a class="app_str and_btn" href="<%=appStoreURLForAndroid%>" target="_blank" title="Whatuni app download store"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/wu_app_android_btn.svg",0)%>" alt="Whatuni app download store"/></a>
            <%}%>
          </div> 
	</div>
      </div>
      <div class="anv_lft">
        <div class="grap2">
          <%if("NonLoggedInDefault".equals(fromPage)){
            if(session.getAttribute("userInfoList") != null || mobileFlag){%>
              <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=imagePath%>" alt="App marketing image"/>
            <%}else{%>
              <img src="<%=imagePath%>" alt="App marketing image"/>
            <%}
          }else{%>
            <img src="<%=imagePath%>" alt="App marketing image"/>
          <%}%>
	</div>
      </div>
    </div>
  </article>
</section> 
<%}%>
<%if("ON".equalsIgnoreCase(ericaPageSlot)){%>
<section class="pp_cnt anv_cnt2 eblp_prm_sec">
  <article class="pp_wrap">
    <div class="anv_wrp2"> 
      <div class="anv_rgt post_lch">
	<h2 class="counter">Download Your Free A-Level &amp; BTEC Exam Survival Guide</h2>
	<p class="par_cnt">With our free survival guide, you'll be able to breeze through your revision, ace your A-Level or BTEC exams and secure the results you need.</p>
	<div class="fm_btn"> 
	  <a class="btn1" href="${PRE_CLEARING_URL}?campaign=28465&utm_source=onsite&utm_medium=banner&utm_campaign=wuhomepage&utm_term=&utm_content=corporate">More info</a> 
	</div>
      </div>
      <div id="imagePod" class="anv_lft">
	<div class="grap2">
         <%if(session.getAttribute("userInfoList") != null || mobileFlag){%>
          <img id="ericaImage" class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="" alt="Download Ebook" />
	 <%}else{%>
              <img id="ericaImage" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" alt="Download Ebook"/>
            <%}%>
        </div>
      </div>
    </div>
  </article>
</section>
<%}%>
<input type="hidden" id="CDNPath" value="<%=CDNPath%>" /> 
<input type="hidden" id="userList" value="<%=userList%>" />
<script type="text/javascript">
var dev = jQuery.noConflict();
dev(window).on('load',  function(){
  ericaImage();
});
function ericaImage(){
  var width = document.documentElement.clientWidth;
  var CDNPath = document.getElementById("CDNPath").value;
  var userList = document.getElementById("userList").value;
  var ericaImage = CDNPath +"/wu-cont/images/eblp_hprmo_img1.png"; 
  var ericaDevicePath = ericaImage.substring(0, ericaImage.lastIndexOf("."));
  if(width <= 767){ 
   ericaImage = "";
   if(document.getElementById("imagePod")){
     document.getElementById("imagePod").style.display = "none";
   } 
  } 
 if(width >= 768 && width <= 992){
   ericaImage = ericaDevicePath +"_992px"+".png";
   if(document.getElementById("ericaImage")){
     document.getElementById("ericaImage").src = ericaImage;
   }
  }
  else{
    if(document.getElementById("ericaImage")){
     if(userList == "YES"){
       document.getElementById("ericaImage").dataset.src = ericaImage;
     }else if(userList == "NO"){
       document.getElementById("ericaImage").src = ericaImage;
     } 
    } 
  }
}
  dev(window).resize(function() {
  ericaImage();
  });
</script>
