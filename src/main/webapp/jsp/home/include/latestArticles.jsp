<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil" %>

<article class="content-bg">  
  <c:if test="${not empty requestScope.articlesList}">
    <article class="content-bg">
      <div class="flexslider2">
        <div style="overflow: hidden;" class="flexslider" id="f_true">         
          <h2 class="fnt_lbk">Articles you may like...</h2>
            <div class="mt30">
              <ul class="slides">
                <% int articleRowid = 1;%>                
                  <c:forEach items="${requestScope.articlesList}" var="articleList" varStatus="articleId"> 
                     <c:set var="articleId" value="${articleId.index}"/>
                    <%
                      int pageNo = 1;
                      int articleIdIntValue = Integer.valueOf(String.valueOf(pageContext.getAttribute("articleId")));
                      if(articleIdIntValue > 4 && articleIdIntValue < 9){
                        pageNo = 2;
                      }else if(articleIdIntValue > 8){
                        pageNo = 3;
                      }
                     %>
                    <li>
                    <div class="ftv<%=articleRowid%>" id="lvp${articleId}">
                      <div class="img_ppn">
                      <a href="${articleList.postUrl}" onclick="ArticlesGAEventsLogging('Articles', 'Articles you may like', '<%=pageNo%>');">                                         
                          <%--Changed first four articles also to lazy load, by Thiyagu G for 16_May_2017--%>
                          <img id="img${articleId}" alt="${articleList.postTitle}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"  data-src="${articleList.imageName}">
                      </a>
                      </div>
                      <div class="ftc">
                        <c:if test="${not empty articleList.postShortText}">
                          <p class="cmart_txt">${articleList.postShortText}</p>
                        </c:if>
						<h3><a href="${articleList.postUrl}" onclick="ArticlesGAEventsLogging('Articles', 'Articles you may like', '<%=pageNo%>');">${articleList.postTitle}</a></h3>
                      </div>
                    </div>
                    </li>
                    <%articleRowid++;%>
                  </c:forEach>              
              </ul>
            </div> 
          </div>
        </div>
      </article>
      <article class="fl content-bg">
        <a class="btn1" href="/student-centre/student-tips.html" onclick="ArticlesGAEventsLogging('Articles', 'Articles you may like', 'See All');">SEE ALL ARTICLES<i class="fa fa-long-arrow-right"></i></a>
		    </article>
   </c:if>
</article>
