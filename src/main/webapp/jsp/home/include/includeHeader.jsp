<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction"%> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%
CommonFunction common = new CommonFunction();
String homeJsName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.homeJsName.js");
String heroImageJsName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.heroImgJsName.js");
String homeSearchJsName = CommonUtil.getResourceMessage("wuni.topnav.search.popup.js", null);
String clearingHomeJs = CommonUtil.getResourceMessage("wuni.common.clearing.home.js", null);
String gradeFilterUcasJs = CommonUtil.getResourceMessage("wuni.ucas.grader.filter.js", null);
String clearingonoff    =  common.getWUSysVarValue("CLEARING_ON_OFF");   
pageContext.setAttribute("getJsDomainValue", CommonUtil.getJsPath());
pageContext.setAttribute("clearingHomeJsName", clearingHomeJs);
pageContext.setAttribute("clearingonoff", clearingonoff);

%>
<%@include  file="/include/htmlTitle.jsp" %>
<%@include  file="/jsp/common/includeHomeCSS.jsp" %>  
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/home/<%=homeJsName%>"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/heroImage_wu561.js"></script>
<%-- <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery.bxslider.js"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery.bxslider.min.js"></script> --%>
<%--condition for non logged in user(below js will only include for first time and return user in home page, not included for logged in user) --%>
<%-- <c:if test=${requestScope.typeOfUser eq 'NEW_USER' }> --%>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/topNavSearchPopup/<%=homeSearchJsName%>"></script>
<%-- </c:if> --%>
<c:if test="${not empty clearingonoff and clearingonoff eq 'ON'}">
  <script type="text/javascript" language="javascript" src="${getJsDomainValue}/js/clearing/${clearingHomeJsName}"></script>
  <script type="text/javascript" language="javascript" id="gradeFilterUcasJsId" src="<%=CommonUtil.getJsPath()%>/js/clearing/<%=gradeFilterUcasJs%>"></script>
</c:if>