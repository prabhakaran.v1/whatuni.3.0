<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--<logic:notEmpty name="providerFaceBookUrl" scope="request">
  <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2F<%=request.getAttribute("providerFaceBookUrl")%>&amp;width=410&amp;height=440&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=true&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:410px; height:400px;" allowTransparency="true"></iframe>
</logic:notEmpty>
<logic:empty name="providerFaceBookUrl" scope="request">
  <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fwhatuni&amp;width=410&amp;height=440&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=true&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:410px; height:400px;" allowTransparency="true"></iframe>
</logic:empty>--%>
<c:if test="${not empty requestScope.providerFaceBookUrl}">
  <div id="container" style="width:100%;">
    <div class="fb-like-box" data-href="https://www.facebook.com/<%=request.getAttribute("providerFaceBookUrl")%>" data-width="292" data-show-faces="true" data-stream="true" data-header="true"></div>
  </div>
</c:if>
<c:if test="${empty requestScope.providerFaceBookUrl}">
  <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fwhatuni&amp;width=410&amp;height=440&amp;colorscheme=light&amp;show_faces=false&amp;header=false&amp;stream=true&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:410px; height:400px;" allowTransparency="true"></iframe>
</c:if>