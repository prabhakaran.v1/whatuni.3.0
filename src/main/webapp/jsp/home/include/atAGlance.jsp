<%@page import="WUI.utilities.CommonUtil"%>
<%String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
<%--
<section class="at_glance">
  <article class="content-bg">
    <h2 class="fnt_lbk">At a glance</h2>
    <div class="glnc_con">
      <article class="gl_lst one">
        <figure>
        <img class="mt30" width="135" height="92" src="<%=CommonUtil.getImgPath("/wu-cont/images/suitcase.png", 0)%>" alt="I want to be"/>
        </figure>
        <a href="<%=request.getContextPath()%>/courses/">
          <div class="lst_cont">
            <h3>I want to be...</h3>
            <div class="ls_cnt">
            <p>Find the best route to your dream job.</p>
             <p></p>
            <p>Perfect if you have a specific career in mind but don't know how to get there.</p>
            </div>
          </div>
        </a>
        <span class="ls_end"></span>
      </article>
      <article class="gl_lst two">
        <figure>
          <img class="mt30" width="135" height="92" src="<%=CommonUtil.getImgPath("/wu-cont/images/hat.png", 0)%>" alt="What can I do">
        </figure>
        <a href="<%=request.getContextPath()%>/courses/">
          <div class="lst_cont">
            <h3>What can I do?</h3>
            <div class="ls_cnt">
              <p>What did students with the same A-levels as you go on to do?</p>
              <p></p>
              <p>Great inspiration if you're struggling to choose a course!</p>
            </div>
          </div>
        </a>
        <span class="ls_end"></span>
      </article>
      <article class="gl_lst">
        <figure>
          <img class="mt30" width="135" height="92" src="<%=CommonUtil.getImgPath("/wu-cont/images/uni.png", 0)%>" alt="Open Days">
        </figure>
        <a href="/open-days/">
          <div class="lst_cont">
            <h3>Open Days</h3>
            <div class="ls_cnt">
              <p>Find open days and save them to your calendar.</p>
              <p></p>
              <p>This section will help you plan visits to different unis.</p>
            </div>
          </div>
        </a>
        <span class="ls_end"></span>
      </article>
      <article class="gl_lst">
        <figure>
          <img class="mt30" width="135" height="92" src="<%=CommonUtil.getImgPath("/wu-cont/images/books.png", 0)%>" alt="Prospectuses">
        </figure>
        <a href="/degrees/prospectus/">
          <div class="lst_cont">
            <h3>Prospectuses</h3>
            <div class="ls_cnt">
            <p>Get key info about the unis you're interested in</p>
            <p></p>
            <p>Got a uni you like the sound of? Don't forget to order the prospectus!</p>
            </div>
          </div>
        </a>
        <span class="ls_end"></span>
      </article>
      <article class="gl_lst">
        <figure>
          <img class="mt30" width="135" height="92" src="<%=CommonUtil.getImgPath("/wu-cont/images/build.png", 0)%>" alt="Inspiration">
        </figure>
        <a href="/advice/">
          <div class="lst_cont">
            <h3>Inspiration</h3>
            <div class="ls_cnt">
              <p>Read blogs, get advice and watch videos</p>
              <p></p>
              <p>All the information you need in one place (plus some fun stuff too)...</p>
            </div>
           </div>
         </a>
         <span class="ls_end"></span>
      </article>
      </div>
  </article>
</section>
--%>
<script type="text/javascript">  
var opdup = jQuery.noConflict();
opdup(window).scroll(function(){ // scroll event 
    lazyloadetStarts();
});
</script>
 