<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ page import="java.util.*" %>
<div class="content-bg" id="whtHaveYouDonePod">
  <div class="row-fluid">
    <div class="container next_steps mt10">
      <!-- What you have done starts-->      
        <div id="wdt" class="wyd_nt fl">        
          <div class="fl wyd bg_wt">
            <h3 class="fnt_lbd mb20">What you've done</h3>
            <c:if test="${not empty requestScope.whatHaveYouDoneList}"> 
            <c:forEach var="whatHaveYouDoneList" items="${requestScope.whatHaveYouDoneList}" varStatus="index">
              <c:set var="index" value="${index.index}"/>
              <%
              
              int i = Integer.parseInt(String.valueOf(pageContext.getAttribute("index")));%>
              <div class="box fl">
                <div class="icn fl">
                  <span class="fl txt_cnr"><i class="fa fa-check fa-2_5x mt25 mb20"></i></span>
                </div>
                <div class="mdl pt20 pl20 fl">
                  <h6 class="fnt_lrg pb5 txt_upr"><c:out value="${whatHaveYouDoneList.mainText}"/></h6>
                  <h5 class="fnt_lbd">
                     <c:if test="${not empty whatHaveYouDoneList.shortText}">
                      <c:set var="shortTexts" value="${whatHaveYouDoneList.shortText}"/> 
                       <%
                       String shortTexts = (String)pageContext.getAttribute("shortTexts");
                        if(shortTexts.length()>28){
                         shortTexts = shortTexts.substring(0,25)+"...";
                        }                                       
                       %>
                      <%=shortTexts%>
                       <%if(shortTexts.length()>=28){%>
                          <div class="show_title">
                            <h5 class="fnt_lbd"><c:out value="${whatHaveYouDoneList.shortText}" /></h5>
                          </div>
                        <%}%>
                    </c:if>
                    </h5>
                </div>
                <div class="btn_view fr mr20 ml20">
                  <a class="fl fnt_lrg" href="${whatHaveYouDoneList.actionUrl}">View</a>
                </div>
                <c:if test="${not empty requestScope.whathaveudoneSize}">
                   <c:if test="${requestScope.whathaveudoneSize eq 1}">                    
                    <span class="icon_wyd_arrow"></span>
                   </c:if>
                   <c:if  test="${requestScope.whathaveudoneSize gt 1}">
                     <%if(i==1){%>
                      <span class="icon_wyd_arrow"></span>
                      <%}%>
                   </c:if>
                </c:if>
                
              </div>
            </c:forEach>
           </c:if> 
           <c:if test="${empty requestScope.whatHaveYouDoneList}"> 
              <div class="box fl no_info">
                <div class="icn fl">
                <span class="fl txt_cnr"><i class="fa fa-check fa-2_5x mt25 mb20"></i></span>
                </div>
                <div class="mdl pt20 pl20 fl">
                  <h6 class="fnt_lrg pb5 txt_upr">Oh dear, you haven't done anything yet!</h6>
                  <h5 class="fnt_lbd">Here are a few suggestions on where to start</h5>
                </div>
                <div class="btn_view fr mr20 ml20"></div>
                <span class="icon_wyd_arrow"></span>
              </div>           
            </c:if>
          </div>        
        </div>
      <!-- What you have done Ends-->
      <!--Next Steps starts-->
         <% HashMap cssStyleMap = new HashMap();
            cssStyleMap.put("Personal information","personal");
            cssStyleMap.put("Course Search","search");
            cssStyleMap.put("Course compared","compare");
            cssStyleMap.put("Open days","open_days");
            cssStyleMap.put("Prospectus requested","ult_gui");
            cssStyleMap.put("Review read","reviews");
            cssStyleMap.put("Articles viewed","ult_gui");
            cssStyleMap.put("Timelines","ult_gui");           
         %>
     
      <c:if test="${not empty requestScope.nextDoneList}">
        <div id="nst" class="wyd_nt fr">
          <div class="fr wyd bg_gn">
            <h3 class="fnt_lbd mb20">Next steps</h3>
            <c:forEach var="nextDoneList" items="${requestScope.nextDoneList}">  
              <div class="box fl">
                <div class="icn fl"> 
                <c:if test="${not empty nextDoneList.actionName}">
                    <c:set var="actionName" value="${nextDoneList.actionName}"/>
                    <%
                       String cssClassName =  (String)cssStyleMap.get((String)pageContext.getAttribute("actionName"));
                    %>
                    <span class="fl txt_cnr mt20 ics_nxt <%=cssClassName%>"></span>
                  </c:if>
                </div>
                <div class="mdl pt20 pl20 fl">
                  <h6 class="fnt_lrg pb5 txt_upr">${nextDoneList.mainText}</h6>
                  <h5 class="fnt_lbd">
                    <c:if test="${not empty nextDoneList.shortText}">
                      <c:set value="${nextDoneList.shortText}" var="shortText"/>
                       <%
                        String shortText = (String)pageContext.getAttribute("shortText");
                        if(shortText.length()>28){
                         shortText = shortText.substring(0,25)+"...";
                        }                                       
                       %>
                      <%=shortText%>
                    </c:if>
                  </h5>                  
                </div>
                <div class="btn_view fr mr20 ml20">
                  <c:if test="${nextDoneList.actionName eq 'Timelines'}">
                  <c:choose>
                    <c:when test = "${fn:contains(nextDoneList.actionUrl, 'www.whatuni.com')}">
                      <a class="fl fnt_lrg" onclick="saveUserTimeLine('${nextDoneList.timeLineId}','${nextDoneList.actionUrl}','_self')">View</a>
                    </c:when>
                    <c:otherwise>
                      <a class="fl fnt_lrg" href="${nextDoneList.actionUrl}" target="_blank" onclick="saveUserTimeLine('${nextDoneList.timeLineId}','${nextDoneList.actionUrl}','_blank')"  >View</a>
                    </c:otherwise>
                  </c:choose>
                  </c:if>
                   <c:if test="${nextDoneList.actionName ne 'Timelines'}">
                     <a class="fl fnt_lrg" href="${nextDoneList.actionUrl}">View</a>
                  </c:if>
                </div>
              </div>
            </c:forEach>
          </div>
        </div>
      </c:if>
      <!--Next Steps Ends-->
    </div>
  </div>
</div>