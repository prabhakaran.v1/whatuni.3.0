<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil,java.util.ArrayList,WUI.utilities.GlobalFunction" %>

<c:if test="${requestScope.FRIENDS_ACTIVITY_LIST gt 0}">
  <%
    ArrayList friendsList = (ArrayList)request.getAttribute("FRIENDS_ACTIVITY_LIST");
    int friendsListSize = friendsList.size();
    int friendsListOffset = Math.round(((float) friendsListSize)/2);
    GlobalFunction globalFunction = new GlobalFunction();
    String userImagePath = "";
  %>  
  <section class="rcnt_act_cont">
    <div id="ban-div">
    
    <div class="content-bg">
      <h2 class="rec_act_head">Your friends' recent activity</h2>
      <div class="rec_act_lft">
        <c:forEach items="${requestScope.FRIENDS_ACTIVITY_LIST}"  var="activityList"  begin="0" end="<%=friendsListOffset%>">
          <c:set var="rightUserDetails" value="${activityList.imageDetails}"/>
          <%userImagePath = globalFunction.getUserImagePath((String)pageContext.getAttribute("rightUserDetails"));%>
          <div class="rec_act_wrap">
            <div class="ract_lft">
                <img src="<%=userImagePath%>" alt="" title="">
            </div>
            <div class="ract_rht">
              ${activityList.friendActivityText}
            </div>
          </div>
          <%userImagePath = "";%>
        </c:forEach>
       </div>
       <div class="rec_act_rht">
        <c:forEach items="${requestScope.FRIENDS_ACTIVITY_LIST}"  var="activityList"  begin="<%=friendsListOffset%>">
          <c:set var="leftUserDetails" value="${activityList.imageDetails}" />
          <%userImagePath = globalFunction.getUserImagePath((String)pageContext.getAttribute("leftUserDetails"));%>
          <div class="rec_act_wrap">
            <c:if test="${not empty activityList.friendActivityText}">
              <div class="ract_lft">
                <img src="<%=userImagePath%>" alt="" title="">
              </div>
              <div class="ract_rht">
                ${activityList.friendActivityText}
              </div>
            </c:if>
          </div>
          <%userImagePath = "";%>
        </c:forEach>
      </div>
    </div>
    </div>
  </section>  
</c:if>