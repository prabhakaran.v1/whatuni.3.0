<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants, WUI.utilities.GlobalFunction, mobile.util.MobileUtils" %> 
<%

String imgpath = "";
%>
<!-- Recommended for you start -->
<c:if test="${not empty requestScope.popularUniversityList}">
	<section class="soc_art rec_fru">
		<div class="content-bg">
			
			<div class="rct_src_lft w236">
				<h2 class="fnt_lbk rct_hd">Recommended for you...</h2>
				<c:if test="${not empty requestScope.popularRecentList}">
				  <c:forEach var="popularRecentList" items="${requestScope.popularRecentList}">				
				    <div class="sub_head">Popular universities for ${popularRecentList.subjectName}</div>
				    <c:set var="searchedSubject" value="${popularRecentList.subjectName}"/>
				    <c:set var="actionDate" value="${popularRecentList.actionDate}"/>
				  </c:forEach>
				</c:if>
			</div>
			<c:if test="${not empty actionDate}">
			<div class="rct_src_rit pt53">
				<div class="srcd_date">You searched <c:out value="${searchedSubject}"/> on <c:out value="${actionDate}"/></div>
			</div>
			</c:if>			
			<div class="clear"></div>				
			<div class="pop_uni_fy">
			 <c:forEach var="popularUniversityList" items="${requestScope.popularUniversityList}" varStatus="i">	
				<div class="pop_uni">
				<c:if test="${not empty popularUniversityList.logoPath}">
        <c:set var="collegeLogoPath" value="${popularUniversityList.logoPath}"/>
        <c:set var="loop" value="${i.index}"/>
         <% imgpath=CommonUtil.getImgPath((String)pageContext.getAttribute("collegeLogoPath"),Integer.parseInt(String.valueOf(pageContext.getAttribute("loop"))));%>
        </c:if>
					<div class="pop_uni_logo"><a href="${popularUniversityList.uniHomeUrl}"><img class="pro_logo" src="<%=imgpath%>" alt="${popularUniversityList.collegeNameDisplay}" title="${popularUniversityList.collegeNameDisplay}"></a></div>
					<div class="pop_uni_name"><a href="${popularUniversityList.uniHomeUrl}">${popularUniversityList.collegeNameDisplay}</a></div>
					<div class="pop_uni_rat"><span class="ml5 rat rat${popularUniversityList.exactRating}"></span></div>
					<div class="pop_uni_vc"><a href="${popularUniversityList.providerResultsUrl}">VIEW COURSES<span class="blue_arw"></span></a></div>
				</div>
				</c:forEach>				
			</div>
		</div>
	</section>
</c:if>
<!-- Recommended for you start -->