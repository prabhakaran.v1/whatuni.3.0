<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="flickr_badge_wrapper">        
 <%--<logic:notEmpty name="providerFlickrUrl" scope="request">
    <div style="width:800px;height:450px;text-align:center;margin:auto;" ><object width="800" height="450" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"> <param name="flashvars" value="offsite=true&amp;lang=en-us&amp;page_show_url=%2Fphotos%2F<%=request.getAttribute("providerFlickrUrl")%>%2Fshow&amp;page_show_back_url=%2Fphotos%2F<%=request.getAttribute("providerFlickrUrl")%>%2F&amp;user_id=<%=request.getAttribute("providerFlickrUserId")%>" /> <param name="allowFullScreen" value="true" /> <param name="src" value="https://www.flickr.com/apps/slideshow/show.swf?v=71649" /> <embed wmode="transparent" width="800" height="450" type="application/x-shockwave-flash" src="https://www.flickr.com/apps/slideshow/show.swf?v=71649" flashvars="offsite=true&amp;lang=en-us&amp;page_show_url=%2Fphotos%2F<%=request.getAttribute("providerFlickrUrl")%>%2Fshow&amp;page_show_back_url=%2Fphotos%2F<%=request.getAttribute("providerFlickrUrl")%>%2F&amp;user_id=<%=request.getAttribute("providerFlickrUserId")%>" allowFullScreen="true" /> </object></div>
  </logic:notEmpty>
  <logic:empty name="providerFlickrUrl" scope="request">
    <div style="width:800px;height:450px;text-align:center;margin:auto;" ><object width="800" height="450" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"  codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0"> <param name="flashvars" value="offsite=true&amp;lang=en-us&amp;page_show_url=%2Fphotos%2Fwhatuni%2Fshow&amp;page_show_back_url=%2Fphotos%2Fwhatuni%2F&amp;user_id=105341327@N03" /> <param name="allowFullScreen" value="true" /> <param name="src" value="https://www.flickr.com/apps/slideshow/show.swf?v=71649" /> <embed wmode="transparent" width="800" height="450" type="application/x-shockwave-flash" src="https://www.flickr.com/apps/slideshow/show.swf?v=71649" flashvars="offsite=true&amp;lang=en-us&amp;page_show_url=%2Fphotos%2Fwhatuni%2Fshow&amp;page_show_back_url=%2Fphotos%2Fwhatuni%2F&amp;user_id=105341327@N03" allowFullScreen="true" /> </object></div>
  </logic:empty>  --%>
  <c:if test="${not empty requestScope.providerFlickrUrl}">
    <div style="width:800px;height:100%;text-align:center;margin:auto;" ><div style="position: relative; padding-bottom: 101%; height: 0; overflow: hidden;"><iframe id="iframe" src="https://flickrit.com/slideshowholder.php?height=100&size=big&speed=stop&thumbnails=0&transition=0&layoutType=responsive&sort=0&userId=<%=request.getAttribute("providerFlickrUserId")%>" scrolling="no" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe></div></div>
  </c:if>
  <c:if test="${empty requestScope.providerFlickrUrl}">
     <c:if test="${not empty requestScope.homeFlickrURL}">
       <div style="width:400px;height:100%;text-align:center;margin:auto;" ><div style="position: relative; padding-bottom: 101%; height: 0; overflow: hidden;"><iframe align="center" src="https://www.flickr.com/slideShow/index.gne?user_id=whatuni&" frameBorder="0" width="400" scrolling="no" height="500"></iframe></div></div>
     </c:if>
      <c:if test="${empty requestScope.homeFlickrURL}">
       <div style="width:800px;height:100%;text-align:center;margin:auto;" ><div style="position: relative; padding-bottom: 101%; height: 0; overflow: hidden;"><iframe align="center" src="https://www.flickr.com/slideShow/index.gne?user_id=whatuni&" frameBorder="0" width="400" scrolling="no" height="500"></iframe></div></div>
      </c:if>
  </c:if>
</div>

<%--New
<iframe align="center" src="http://www.flickr.com/slideShow/index.gne?user_id=whatuni&" frameBorder="0" width="500" scrolling="no" height="500"></iframe>

Old
<iframe id="iframe" src="https://flickrit.com/slideshowholder.php?height=100&size=big&speed=stop&userId=whatuni&thumbnails=0&transition=0&layoutType=responsive&sort=0" scrolling="no" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe>
<iframe id="iframe" src="https://flickrit.com/slideshowholder.php?height=100&size=big&speed=stop&userId=whatuni&thumbnails=0&transition=0&layoutType=responsive&sort=0" scrolling="no" frameborder="0" style="width:100%; height:100%; position: absolute; top:0; left:0;" ></iframe>--%>