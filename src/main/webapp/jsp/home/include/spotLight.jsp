<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="WUI.utilities.CommonUtil"%>

<% String className = "lft txtcnr" ;%>
<c:if test="${not empty requestScope.spotLightCourseList}">
<section class="spotLight">
  <div class="content-bg">
    <div class="row-fluid">
      <h2 class="txtcnr pb30 fnt_lbk">In the spotlight this week...</h2>    
     
        <c:forEach var="spotLightCourseList" items="${requestScope.spotLightCourseList}" end="2">
          <div class="<%=className%>">
            <div class="lbd">
             <c:if test="${not empty spotLightCourseList.titleUrl}">
               <c:choose>
               <c:when test="${fn:contains(spotLightCourseList.titleUrl, 'www.whatuni.com')}">
                  <a onclick="javascript:cpeSpotLightVideo('${spotLightCourseList.collegeId}','${spotLightCourseList.titleUrl}','Y');">
                    <c:if test="${not empty spotLightCourseList.title}">
                      <h3 class="fnt_lbd">${spotLightCourseList.title}</h3>
                    </c:if>
                  </a>               
               </c:when>
               <c:otherwise>
                   <a href="${spotLightCourseList.titleUrl}"  target="_blank" onclick="javascript:cpeSpotLightVideo('${spotLightCourseList.collegeId}','${spotLightCourseList.titleUrl}');">
                    <c:if test="${not empty spotLightCourseList.title}">
                        <h3 class="fnt_lbd">${spotLightCourseList.title}</h3>
                    </c:if>
                    </a>               
               </c:otherwise>
               </c:choose>
              </c:if>
              
               <c:if test="${not empty spotLightCourseList.subTitleUrl}">
               <c:choose>
                  <c:when test="${fn:contains(spotLightCourseList.subTitleUrl, 'www.whatuni.com')}">
                    <a onclick="javascript:cpeSpotLightVideo('${spotLightCourseList.collegeId}','${spotLightCourseList.subTitleUrl}','Y');">
                      <h6 class="fnt_lbd pt5">
                       <c:if test="${not empty spotLightCourseList.subTitle}">
                        ${spotLightCourseList.subTitle}
                       </c:if>
                      </h6>
                    </a>
                  </c:when>
                  <c:otherwise>
                    <a href="${spotLightCourseList.subTitleUrl}"  target="_blank" onclick="javascript:cpeSpotLightVideo('${spotLightCourseList.collegeId}','${spotLightCourseList.subTitleUrl}');">
                      <h6 class="fnt_lbd pt5">
                        <c:if test="${not empty spotLightCourseList.subTitle}">
                          ${spotLightCourseList.subTitle}
                        </c:if>
                      </h6>
                    </a>
                  </c:otherwise>
                </c:choose>
              </c:if>
            </div>
            <div class="vidcnr">
              <div class="lft_vid">
                <div class="img_ppn">
                  <c:if test="${spotLightCourseList.mediaType  eq 'Video'}">
                      <%--Changed all spotlight lazyload from img_px to trans_img_px.gif on 16_May_2017, By Thiyagu G.--%>
                       <a onclick="javascript:dynamicVideoPlayerJsLoad('${spotLightCourseList.videoUrl}','');cpeSpotLightVideo('${spotLightCourseList.collegeId}','${spotLightCourseList.videoUrl}')">
                         <i class="fa fa-play-circle-o fa-8x"></i>
                         <img width="211" title="Video ${potLightCourseList.title}" alt="Video ${potLightCourseList.title}"   src="<%=CommonUtil.getImgPath("/wu-cont/images/trans_img_px.gif", 1)%>" data-src="${spotLightCourseList.imagePath}">
                       </a>
                     </c:if>
                     <c:if test="${spotLightCourseList.mediaType eq 'Image'}">
                         <c:choose>
             			  <c:when test = "${fn:contains(spotLightCourseList.titleUrl, 'www.whatuni.com')}">
                             <a onclick="javascript:cpeSpotLightVideo('${spotLightCourseList.collegeId}','${spotLightCourseList.titleUrl}','Y');">
                               <img width="211" title="Image ${spotLightCourseList.title}" alt="Image ${spotLightCourseList.title}" src="<%=CommonUtil.getImgPath("/wu-cont/images/trans_img_px.gif", 1)%>" data-src="${spotLightCourseList.imagePath}">
                              </a>
                         </c:when>
                          <c:otherwise>
                             <a href="${spotLightCourseList.titleUrl}" target="_blank" onclick="javascript:cpeSpotLightVideo('${spotLightCourseList.collegeId}','${spotLightCourseList.titleUrl}');">
                               <img width="211" title="Image ${spotLightCourseList.title}" alt="Image ${spotLightCourseList.title}" src="<%=CommonUtil.getImgPath("/wu-cont/images/trans_img_px.gif", 1)%>" data-src="${spotLightCourseList.imagePath}">
                              </a>
                          </c:otherwise>
                         </c:choose> 
                      </c:if>
                </div>
              </div>
              <div class="vidtxt fnt_lrg">
                <p class="fnt_lrg">${spotLightCourseList.description}</p>
                 <c:if test="${not empty spotLightCourseList.callToActionUrl}">
                   <c:choose>
             		<c:when test = "${fn:contains(spotLightCourseList.callToActionUrl, 'www.whatuni.com')}">                
                    <a class="rm fnt_lbd" onclick="javascript:cpeSpotLightVideo('${spotLightCourseList.collegeId}','${spotLightCourseList.callToActionUrl}','Y');"  >
                      <c:if test="${not empty spotLightCourseList.callToActionText}"> 
                       ${spotLightCourseList.callToActionText}
                       <i class="fa fa-long-arrow-right"></i>
                      </c:if>
                    </a>
                   </c:when>
                   <c:otherwise>    
                     <a class="rm fnt_lbd" href="${spotLightCourseList.callToActionUrl}" target="_blank" onclick="javascript:cpeSpotLightVideo('${spotLightCourseList.collegeId}','${spotLightCourseList.callToActionUrl}');">
                        <c:if test="${not empty spotLightCourseList.callToActionText}"> 
                         ${spotLightCourseList.callToActionText}
                         <i class="fa fa-long-arrow-right"></i>
                        </c:if>                       
                     </a>
                  </c:otherwise> 
                </c:choose>
                </c:if>
              </div>
            </div>
          </div>
          <%className = "rht txtcnr";%>
        </c:forEach>

    </div>
  </div>
</section>
<div class="dialog" id="box">
  <div align="center" id="flashbanner"></div>
  <div id="linksdiv" style="display:none;" class="mt30">
     <span class="cros"><strong><a href="javascript:closeHomeVideo()">Close</a></strong></span>
   </div>
</div>
</c:if>