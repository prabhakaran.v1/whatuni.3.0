<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, mobile.util.MobileUtils,java.util.ArrayList"%>
<%
  String studentChoiceAwrdVideo = (String)request.getAttribute("studentChoiceAwrdVideo");
         studentChoiceAwrdVideo = studentChoiceAwrdVideo!=null && studentChoiceAwrdVideo!= "" ? studentChoiceAwrdVideo : "";    
  CommonFunction common = new CommonFunction();  
%>
<c:if test="${not empty requestScope.providerHeroImageList}">
   <c:set var="listSize"  value="${providerHeroImageList}"/>
  <jsp:useBean id="listSize" type="java.util.List"></jsp:useBean>
  <%
    int size = ((ArrayList) pageContext.getAttribute("listSize")).size();
    // If size is 1 the display the slider part directly else show slider after bxslider loaded, 25_Sep_2018 By Sabapathi
    String displayEnabled = "display:none";
    if(size == 1) {
      displayEnabled = "display:block";
    }
  %>  
  <section class="slider_cnt" id="heroslider">    
  <div class="hm_ldicon" id="load-icon">
    <img src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif", 1)%>">
  </div>  
  <div id="load-hero-div" style="display:none;">
  <%if(size == 1){%>
  <div class="bx-wrapper" style="max-width: 100%;">
  <%}%>
    <ul class="bxslider" style="<%=displayEnabled%>">
      <%-- Chatbot block which is included in slider instead of default home image for desktop alone, 25_Sep_2018 By Sabapathi --%>
      <li id="wu_chat_slot" class="carosal">
        <jsp:include page="/jsp/home/include/chatbotPromoContent.jsp">
          <jsp:param name="PLACE_OF_INCLUSION" value="CAROUSAL"/>
        </jsp:include>
      </li>
      <c:forEach var="providerHeroImage" items="${requestScope.providerHeroImageList}"  end="0" varStatus="rowId">
        <li id="wu_hero_slot" class="carosal">
          <c:if test="${not empty providerHeroImage.mediaPath}"> 
            <img id="bxslider${rowId.index}" src="" />
            <c:set var="defaultHeroImg" value="${providerHeroImage.mediaPath}"/>
            <input type="hidden" id="bxSliderImg${rowId.index}" value="<%=CommonUtil.getImgPath((String)pageContext.getAttribute("defaultHeroImg"), 1)%>" />
          </c:if>
          <div class="hi_tcont">
            <div class="hmi_vmid">
              ${providerHeroImage.defaultText}	
            </div>
          </div>          
        </li>
      </c:forEach>
      <c:forEach var="providerHeroImage" items="${requestScope.providerHeroImageList}" begin="1" varStatus="rowId1">
        <li class="carosal">
          <c:set var="collegeName" value="${providerHeroImage.collegeNameDisplay}"/>   
          <%--18_Nov_2014 added hero image stats view by Thiyagu G.--%>
          <c:set var="collegeId" value="${providerHeroImage.collegeId}"/>
          <c:set var="imageUrl" value="${providerHeroImage.imageUrl}" />
          <c:set var="viewImageTitle" value="${providerHeroImage.title}"/>
          <%
            String collegeName = (String)pageContext.getAttribute("collegeName");
            String collegeId = (String)pageContext.getAttribute("collegeId");
            String viewImageTitle = (String)pageContext.getAttribute("viewImageTitle");
            String imageUrl = (String)pageContext.getAttribute("imageUrl");
            String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(collegeName); 
            String viewImageTitletemp = new WUI.utilities.CommonFunction().replaceSpecialCharacter(viewImageTitle);
            String heroImageView = gaCollegeName+"~"+collegeId+"~"+viewImageTitletemp;request.setAttribute("hero_image", heroImageView);%>
          <c:if test="${not empty providerHeroImage.mediaPath}">
             <c:set var="providerSliderImage" value="${providerHeroImage.mediaPath}"/>
            <img id="bxslider${rowId1.index}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=CommonUtil.getImgPath((String)pageContext.getAttribute("providerSliderImage"), 1)%>" /> 
            <input type="hidden" id="bxSliderImg${rowId1.index}" value="<%=CommonUtil.getImgPath((String)pageContext.getAttribute("providerSliderImage"), 1)%>" />
          </c:if>              
          <c:if test="${not empty requestScope.countsList}">
            <c:forEach items="${requestScope.countsList}" var="videoPath">
              <div class="rht btm">
                <%--18_Nov_2014 added hero image stats click by Thiyagu G.--%>
                <%String descImageURL = viewImageTitletemp+": "+imageUrl;%>
                <c:choose>
                 <c:when test = "${fn:contains(providerHeroImage.imageUrl, 'www.whatuni.com')}">
                   <a href="${providerHeroImage.imageUrl}" onclick="cpeHeroImage('${providerHeroImage.collegeId}', '<%=descImageURL%>','Y','HERO_IMAGE_CLICK');var a='s.tl(';searchEventTracking('home-hero-image', 'clicked', '<%=gaCollegeName%>');" class="fnt_lbd"> 
                 </c:when>
                 <c:otherwise>
                   <a target="_blank" href="${providerHeroImage.imageUrl}" onclick="cpeHeroImage('${providerHeroImage.collegeId}', '<%=descImageURL%>','Y','HERO_IMAGE_CLICK');var a='s.tl(';searchEventTracking('home-hero-image', 'clicked', '<%=gaCollegeName%>');" class="fnt_lbd">              
                 </c:otherwise>
                </c:choose>        
                <div class="closer_look"> 
                  <%--Added provider logo for 13_Dec_2016, by Thiyagu G.--%>
                  <c:if test="${not empty providerHeroImage.collegeLogo}">
                    <span class="himg_ctoa"> 
                      <img src="<%=CommonUtil.getImgPath("", 0)%>${providerHeroImage.collegeLogo}" title="${providerHeroImage.collegeName}" alt="${providerHeroImage.collegeName}"/>
                    </span>
                  </c:if>
                  <span class="himg_cnt">
                    <span class="fnt_lbd clr_wte">                
                      <c:if test="${not empty providerHeroImage.title}">
                        ${providerHeroImage.title}
                      </c:if>
                    </span>
                    <span class="sub_title fnt_lbd">
                      <c:if test="${not empty providerHeroImage.subTitle}">
                        ${providerHeroImage.subTitle}
                      </c:if>
                    </span>
                  </span>            
                </div>
                </a>
              </div>
            </c:forEach>
          </c:if>
        </li>
      </c:forEach>
      
    </ul>
    <%if(size == 1){%>
    </div>
    <%}%>
    </div>
  </section>
<%if(size == 1){%>
<script type="text/javascript">
  var sizeCnt=1;  
</script>
<%}if(size > 1){%>
<script type="text/javascript">
  var dev12 = jQuery.noConflict();
  var sizeCnt=2;
  var chatLi;
  var heroLi;
  var slider;
  // Chnaged doc.ready to win.load for full load based on condition (i.e. Mobile = default hero img, Desktop = chatbot block), 25_Sep_2018 By Sabapathi
  // And set the height for the chatbot block also for bxslider adjustment
  dev12(window).on('load',  function(){
    dev12("#load-icon").hide();
    dev12("#load-hero-div").show();
    slider = dev12('.bxslider').show().bxSlider({
      auto: true,
      autoHover: true,
      autoControls: true,
      infiniteLoop: true,
      pause: 8000, // stay 8 secs for each slides (Do not slide away for next untill 8 secs)
      speed: 800,
      onSliderLoad: function() {         
          var width = document.documentElement.clientWidth;
          var img_width=1402;//static width
	         var img_height=420;//static height
	         var variation = img_height/img_width	  
	         var img_dht = Math.round(width * variation);
          if(width <= 480){
            img_dht = img_dht + 91;            
            dev12(".bx-viewport, .cbhro_sec").css("height", img_dht);
          }else if(width > 481 && width <= 768){
            img_dht = img_dht + 114;            
            dev12(".bx-viewport, .cbhro_sec").css("height", img_dht);
          }else if(width == 1024){
            dev12(".bx-viewport, .cbhro_sec").css("height", "307");
          }else if(width >= 1280 && width <= 1366){
            dev12(".bx-viewport, .cbhro_sec").css("height", "404");
          }else if(width >= 1402){
            dev12(".bx-viewport, .cbhro_sec").css("height", img_dht);
            dev12(".bx-wrapper,.bx-viewport ul li").css("width", width);
          }else{
            dev12(".bx-viewport, .cbhro_sec").css("height", "378");
          }
          dev12('.bx-pager-link').click(function () {
              var i = dev12(this).attr('data-slide-index');
              slider.goToSlide(i);
              slider.stopAuto();
              slider.startAuto();
              return false;
          });
      },
      onSlideBefore: function() {lazyloadetStarts();clrChatBotTextMod();},
      onSlideAfter: function() {lazyloadetStarts();clrChatBotTextMod();},
      onSlideNext: function() {lazyloadetStarts();clrChatBotTextMod();},
      onSlidePrev: function() {lazyloadetStarts();clrChatBotTextMod();}       
    });
    dev12("#load-icon").hide();
    dev12("#load-hero-div").show();
    dev12(".bx-wrapper a").click(function(){
      slider.startAuto();
    });
    dev12(".bx-pager-link").click(function(){
      slider.startAuto();
    });
    reloadSlider(); // Reload the slider based on width of the screen
    //
  });
</script>
<%}%>
<script type="text/javascript" language="javascript">
  var dev = jQuery.noConflict();
  dev(document).ready(function(){    
    adjustImagesStyle();
  });  
  dev(document).load(function(){    
    adjustImagesStyle();          
  }); 
  function jqueryWidth() {
    return dev(this).width();
  }
  function adjustImagesStyle() {
    var width = document.documentElement.clientWidth;    
    var srb_hgt = document.documentElement.clientHeight;
    dev(".bxslider li").css("width", width);
    dev(".hm_srchbx").css("height", srb_hgt);
    dev(".srb_wrap").css("height", srb_hgt);
    for(var lp = 0; lp < sizeCnt; lp++){
      if(document.getElementById("bxSliderImg"+lp)){
        var url = document.getElementById("bxSliderImg"+lp).value;  
        var urlString = "";
        if(url != null && url != ""){  
          var spMediaPath = url.substring(0, url.lastIndexOf("."));          
          var dwidth = dev(this).width();     
          var img_width=1402;//static width
          var img_height=420;//static height
          var variation = img_height/img_width;
          var img_dht = Math.round(dwidth * variation);
          if(width <= 480){
            img_dht = img_dht + 91;
            dev(".bx-viewport, .cbhro_sec").css("height", img_dht);
            urlString = spMediaPath +'_480px' + url.substring(url.lastIndexOf("."),url.length);                        
            var bxId = "bxslider"+lp;            
            if(lp == 0 && (width >= 320 && width <= 768)){
             dev("img[id^='"+bxId+"']").attr('src',urlString);
            }else{             
             dev("img[id^='"+bxId+"']").attr('data-src',urlString);
            }
          } else if(width > 481 && width <= 992){              
            img_dht = img_dht + 114;
            dev(".bx-viewport, .cbhro_sec").css("height", img_dht);
            urlString = spMediaPath +'_992px' + url.substring(url.lastIndexOf("."),url.length);
            var bxId = "bxslider"+lp;            
            if(lp == 0){
             dev("img[id^='"+bxId+"']").attr('src',urlString);
            }else{
             dev("img[id^='"+bxId+"']").attr('data-src',urlString);
            }
          }else{
            dev(".hm_srchbx").hide();
           if(width == 1024){
              dev(".bx-viewport, .cbhro_sec").css("height", "307");
            }else if(width >= 1280 && width <= 1366){
              dev(".bx-viewport, .cbhro_sec").css("height", "404");
            }else if(width >= 1402){              
              dev(".bx-viewport, .cbhro_sec").css("height", img_dht);
              dev(".bx-wrapper,.bx-viewport ul li,.bxslider li").css("width", width);
            }else{              
              dev(".bx-viewport, .cbhro_sec").css("height", "378");
            }
            var bxId = "bxslider"+lp;            
            if(lp == 0 && (width >= 320 && width <= 768)){             
             dev("img[id^='"+bxId+"']").attr('src',url);
            }else{            
             dev("img[id^='"+bxId+"']").attr('data-src',url);
            }
          }          
        }        
      }
    }
    if(sizeCnt == 1){
      var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
      setTimeout(function(){
        dev("#load-icon").hide();
        dev("#load-hero-div").show();
      },500);
      dev('#wu_chat_slot').show();
      dev('#wu_hero_slot').show();
      if((deviceWidth >= 320 && deviceWidth <= 768)) {
        dev('#wu_chat_slot').hide();    
      } else {
        dev('#wu_hero_slot').hide();
      }
    } 
  }
  dev(window).on('orientationchange', orientationChangeHandler);
  function orientationChangeHandler(e) {
    setTimeout(function() {
      dev(window).trigger('resize'); 
      adjustImagesStyle();
      reloadImg();
    }, 200);
  }
  dev(window).resize(function() {
    // Reload only if bxslider enabled. Slider got enabled only if more than 1 slides
    if(sizeCnt > 1) {
      reloadSlider();
    }
    adjustImagesStyle(); 
    reloadImg();
    //
  }); 
  
  // Reload the slider based of mobile or desktop
  // Mobile -> attach default hero image and detach chatbot
  // Desktop -> Detach default hero image and attach chatbot
  function reloadSlider() {
    var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
    //console.log('deviceWidth > '+deviceWidth);
    //console.log('slider != null > '+ (slider != null) + ' || is mobile design? > '+(deviceWidth >= 320 && deviceWidth <= 768));
    dev('.carosal').css('visibility', 'visible');
    if(slider != null && (deviceWidth >= 320 && deviceWidth <= 768)) {
      //dev('#wu_chat_slot').remove();
      //dev('#wu_hero_slot').addClass('carosal');
      //dev('#wu_chat_slot').removeClass('carosal');
      chatLi =  dev('#wu_chat_slot').detach();
      if(heroLi) { dev('.bxslider').prepend(heroLi); }
      heroLi = null;
      //console.log('with wu_hero_slot > reloadSlider'+' = chat > '+dev(chatLi).attr('id'));
      slider.reloadSlider();
    } else if(slider != null) {
      //dev('#wu_hero_slot').remove();
      //dev('#wu_chat_slot').addClass('carosal');
      //dev('#wu_hero_slot').removeClass('carosal');
      heroLi =  dev('#wu_hero_slot').detach();
      if(chatLi) { dev('.bxslider').prepend(chatLi); }
      chatLi = null;
      //console.log('with wu_chat_slot > reloadSlider'+' = heroLi > '+dev(heroLi).attr('id'));
      slider.reloadSlider();
    }
  }
  function reloadImg(){ 
    var dwidth = dev(this).width();     
    var img_width=1402;//static width
    var img_height=420;//static height
    var variation = img_height/img_width;
    var img_dht = Math.round(dwidth * variation);    
    if(dwidth <= 480){
      img_dht = img_dht + 91;
      dev(".bx-viewport, .cbhro_sec").css("height", img_dht);
    }else if(dwidth > 481 && dwidth <= 768){
      img_dht = img_dht + 114;      
      dev(".bx-viewport, .cbhro_sec").css("height", img_dht);
    }
  }
</script>
</c:if>
<section class="mcr_cnt">
<article class="content-bg">
	<div class="mcr_wrap">
  <c:if test="${not empty requestScope.countsList}">
    <c:forEach items="${requestScope.countsList}" var ="countsList">
      <div class="mcr_col1">
        <div class="mcr_lft"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/mcr_img1.png", 0)%>" alt="" title="" /></div>
        <div class="mcr_rht"><span>${countsList.userCount}</span> Members</div>
      </div>
      <div class="mcr_col1">
        <div class="mcr_lft"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/mcr_img2.png", 0)%>" alt="" title="" /></div>
        <div class="mcr_rht"><span>${countsList.courseCount}</span> Courses</div>
      </div>
      <div class="mcr_col1">
        <div class="mcr_lft"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/mcr_img3.png", 0)%>" alt="" title="" /></div>
        <div class="mcr_rht"><span>${countsList.reviewCount}</span> Reviews</div>
      </div>
      <div class="mcr_col1">
        <div class="mcr_lft"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/mcr_img4.png", 0)%>" alt="" title="" /></div>
        <div class="mcr_rht"><span>${countsList.futureOpendayCount}</span> Open days</div>
      </div>
    </c:forEach>
  </c:if>
	</div>
</article>
</section>
<section class="heroSlider video" id="heroslider_bluscreen"  style="display:none;">
  <div class="content-bg">
    <div class="row-fluid">
      <c:if test="${empty sessionScope.userInfoList}">
        <div class="lft">
          <a class="btn_orng" onclick="javaScript:showLightBoxLoginForm('popup-newlogin',650,500, 'homepage','','hero-image');">SIGN UP / LOG IN</a><%--8_OCT_2014 Added by Priyaa for handling Register tab focus--%>
        </div>
      </c:if>
      <!-- Hero Right-->
      <div class="rht btm">		
        <article class="awrd_vdo mt35">      		
          <a class="p0 m0 fr close_video" style="display:block;" id="closebutton" onclick = "javascript:hidePlayer();">         
            <img width="30" height="30" alt="video_close" src="<%=CommonUtil.getImgPath("/wu-cont/images/close_video.jpg", 0)%>">        
          </a>	
          <iframe id="video_emb" width="500" height="300" frameborder="0" style="display:none;margin-left:10px;" allowfullscreen=""></iframe>
        </article>
      </div>
    </div>
  </div>
</section>