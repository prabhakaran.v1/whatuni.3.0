<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil"%>

<%--
  * @purpose:  This jsp is trending and most popular pod in ajax call..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 3-Nov-2015    Prabhakaran V.             1.0      First draft           wu_546
  * *************************************************************************************************************************
--%>

<c:if test="${not empty requestScope.trendingPopularAdviceList}">
    <ul class="rgt-lst mt30 trend">
      <h2 class="pb15">Trending</h2>
      <c:forEach var="trendingPopularAdvice" items="${requestScope.trendingPopularAdviceList}" varStatus="i">
        <li>
          <figure><a href="<c:out value="${trendingPopularAdvice.adviceDetailURL}"/>">
          <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${trendingPopularAdvice.imageName}"/>" alt="<c:out value="${trendingPopularAdvice.mediaAltText}"/>" title="<c:out value="${trendingPopularAdvice.postTitle}"/>"></a></figure>
          <p><a href="<c:out value="${trendingPopularAdvice.adviceDetailURL}"/>"><c:out value="${trendingPopularAdvice.postTitle}"/></a></p>
        </li>
      </c:forEach>
    </ul>
</c:if>
<c:if test="${not empty requestScope.mostPopularAdviceList}">
    <%      
      int pageNo = 1;
    %>
    <h2 class="pb15">Top articles for you</h2>                                    
    <ul class="rgt-lst">
    <c:forEach var="mostPopularAdvice" items="${requestScope.mostPopularAdviceList}" varStatus="i">
        <li>
          <figure>
            <a href="<c:out value="${mostPopularAdvice.adviceDetailURL}"/>" onclick="ArticlesGAEventsLogging('Articles', 'Top Pod', '<%=pageNo%>');">
            <img  class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<c:out value="${mostPopularAdvice.imageName}"/>" alt="<c:out value="${mostPopularAdvice.mediaAltText}"/>" title="<c:out value="${mostPopularAdvice.postTitle}"/>"></a>
          </figure>
          <span class="cmart_txt"><c:out value="${mostPopularAdvice.postShortText}"/></span>
          <p><a href="<c:out value="${mostPopularAdvice.adviceDetailURL}"/>" onclick="ArticlesGAEventsLogging('Articles', 'Top Pod', '<%=pageNo%>');"><c:out value="${mostPopularAdvice.postTitle}"/></a></p>
        </li>
      </c:forEach>
    </ul>
</c:if>