<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
  String homeStyleClass = "wu_home";
  String myFinalChExist = "";
  if(request.getAttribute("myFinalChoiceExist")!=null){
    myFinalChExist = (String)request.getAttribute("myFinalChoiceExist");
    if("true".equals(myFinalChExist)){
      homeStyleClass = "wu_home pst_ur_f5";
    }else{
      homeStyleClass = "wu_home pst_ur_f5 pst_f5_emty";
    }
  }
  CommonFunction commonFun = new CommonFunction();
  String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
  String canonicalUrl = httpStr + "www.whatuni.com";
  String socialBoxJs = CommonUtil.getResourceMessage("wuni.social.box.js", null);  
%>


<body>
<div class="<%=homeStyleClass%> logged_in_pod" id="homeFnchDisp">
  <header class="clipart">
    <div class="">
      <div class="content-bg">
        <div id="desktop_hdr">
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>                
      </div>      
    </div>
  </header>            
  <c:if test="${not empty requestScope.myFinalChoiceList}">           
    <section class="spotLight final5" id="homeFinalChResp" style="display:none;">
      <div class="content-bg">
        <div class="hm_txt_wrap">
          <h2>Hi <%=(session.getAttribute("showUserName")!=null && !"".equals(session.getAttribute("showUserName"))) ? " "+session.getAttribute("showUserName") : ""%>, you're right on track...</h2>
          <h3>It's time to select your final 5!</h3>
        </div>
      </div>
      <div class="confirm_cont mt50 edt_wrap_act">        
        <div class="fr">          
          <a class="edt_fnl5_btn ed_f5_btn" title="" href="<%=request.getContextPath()%>/comparison">EDIT MY FINAL 5<i class="fa richp fa-long-arrow-right"></i></a>          
        </div>
      </div>
    </section>      
    <jsp:include page="/jsp/myfinalchoice/include/myFinalChoicePod.jsp">
      <jsp:param name="fromHomePage" value="yes"/>
      <jsp:param name="myFinalChExist" value="<%=myFinalChExist%>"/>
    </jsp:include>        
    <section class="timeline" id="dskTimeline">
      <jsp:include page="/jsp/home/include/whatHaveYouDone.jsp"/>
    </section>
  </c:if>  
  <c:if test="${empty requestScope.myFinalChoiceList}">         
    <jsp:include page="/jsp/home/include/userTimeLinePod.jsp">
      <jsp:param name="fromHomePage" value="yes"/>
    </jsp:include>
  </c:if> 
  <jsp:include page="/jsp/home/include/recentSearchPod.jsp"/>
  <jsp:include page="/jsp/home/include/recommedPopUniPod.jsp"/>
  <%-- <jsp:include page="/jsp/home/include/friendsActivityPod.jsp"/> --%><%-- Yogeswari :: 21:07:2015 :: added for friends activity pod.--%>
   
  <section class="ltst_art" id="articlesPod"></section>
  <%--Added app marketing section in home page--%>  
  <jsp:include page="/jsp/home/include/appMarketingSetion.jsp">
   <jsp:param name="fromPage" value="LoggedIn"/>
  </jsp:include>  
  <%--End of app marketing section--%>
  <input type="hidden" id="scrollStop" value="" />
  <jsp:include page="/jsp/home/include/latestNews.jsp"/>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=socialBoxJs%>"></script>     
  <jsp:include page="/jsp/common/wuFooter.jsp" />    
  <jsp:include page="/jsp/common/socialBox.jsp" />
</div>  
</body>  
</html>
 