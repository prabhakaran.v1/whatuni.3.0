<%--
  * @purpose:  This jsp is for find a courses page..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name              Ver.     Changes desc          Rel Ver.
  * 08-Dec-2020    Keerthana          1.0      First draft           wu_923
  * *************************************************************************************************************************
--%>
<body>
  <header class="md clipart">
    <div class="ad_cnr">
      <div class="content-bg">
        <div id="desktop_hdr">
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>
      </div>
    </div>
  </header>
  <section class="Main_Cnr cr_srch fnd_crse">
    <jsp:include page="/jsp/findacourse/include/courseSearchPod.jsp" />
    <div class="ad_cnr">
      <div class="content-bg">
        <div class="sub_cnr">
          <jsp:include page="/jsp/findacourse/include/popularSubjectAreaPod.jsp" />
           <h2 class="hmc">Help me choose</h2>
          <jsp:include page="/jsp/findacourse/include/iWantToBePod.jsp" />
          <jsp:include page="/jsp/findacourse/include/iWantToBeResultsPod.jsp" />
          <input type="hidden" id="iwtbPodDisplay" value="iwtbEntry" />
          <jsp:include page="/jsp/findacourse/include/whatCourseShouldIDoPod.jsp" />
          <jsp:include page="/jsp/findacourse/include/whatCourseShouldIDoResultsPod.jsp" />
          <jsp:include page="/jsp/findacourse/include/choosingACoursePod.jsp" />
        </div>
      </div>
    </div>
  </section>
  <jsp:include page="/jsp/common/wuFooter.jsp" />
</body>
