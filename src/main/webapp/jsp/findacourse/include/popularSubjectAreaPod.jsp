<%@page import="WUI.utilities.CommonUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<%--
  * @purpose:  This jsp gets included in find a courses page for popular subjects pod
  * Change Log
  * *************************************************************************************************************************
  * Date           Name              Ver.     Changes desc          Rel Ver.
  * 08-Dec-2020    Keerthana          1.0      First draft           wu_923
  * *************************************************************************************************************************
--%>

<%
CommonUtil utilities = new CommonUtil();
%>
<c:set var="mobileFlag" value="<%=utilities.getMobileFlag(request)%>" />
<div class="psa" id="popularSubjectArea">
<c:if test="${not empty popularSubjectList}">
  <h2>Popular subject areas</h2>
  <c:choose>
  <c:when test="${mobileFlag eq 'true'}">
    <div id="popularSubjectList">
      <div class="fu_crs_lst">
        <c:forEach var="popularSubjectList" items="${popularSubjectList}" varStatus="i" step="1" begin="1" end="5">
          <a href="${popularSubjectList.subjectUrl}">${popularSubjectList.subjectName}</a>
        </c:forEach>
      </div>
      <c:set var="listLength" value="${fn:length(popularSubjectList)}"/>
      <c:if test="${listLength gt '5'}">
        <a href="javascript:void(0)" class="shwmre_sub" id="showMoreSubjects" onclick="courseGALogging('Show More', 'subject');"><span class="plus"></span>Show more subjects</a> 
      </c:if>
    </div>
    <div id="moreSubjects" style="display:none">
      <div class="fu_crs_lst">
        <c:forEach var="popularSubjectList" items="${popularSubjectList}" varStatus="i" step="1" begin="1">
          <a href="${popularSubjectList.subjectUrl}">${popularSubjectList.subjectName}</a>
        </c:forEach>
      </div>
      <a href="javascript:void(0)" class="shwmre_sub" id="showLessSubjects"><span class="minus"></span>Show less subjects</a>
    </div>
  </c:when>
  <c:otherwise>
      <div class="fu_crs_lst">
      <c:forEach var="popularSubjectList" items="${popularSubjectList}" step="1" varStatus="i">
        <a href="${popularSubjectList.subjectUrl}" onclick="courseGALogging('${popularSubjectList.subjectName}', 'subject');">${popularSubjectList.subjectName}</a>
      </c:forEach>
    </div>
  </c:otherwise>
  </c:choose>
</c:if>
</div>
