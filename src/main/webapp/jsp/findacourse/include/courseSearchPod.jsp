<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import=" WUI.utilities.CommonUtil, WUI.utilities.SessionData, WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator" %>

<%--
  * @purpose:  This jsp gets included in find a courses page for course search pod.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name              Ver.     Changes desc          Rel Ver.
  * 08-Dec-2020    Keerthana          1.0      First draft           wu_923
  * *************************************************************************************************************************
--%>

<%
  String clearingUserType = (String)session.getAttribute("USER_TYPE");
  String navSearchValue = "";
  String studyLevelId = request.getAttribute("studyLevelId") != null ? String.valueOf(request.getAttribute("studyLevelId")) :"";
  String clearingFlag = request.getAttribute("clearingFlag") != null ? String.valueOf(request.getAttribute("clearingFlag")) :"";
  String studyLevelName = "";
  String levelName= "";
  if("M".equalsIgnoreCase(studyLevelId) && "CLEARING".equalsIgnoreCase(clearingFlag)){studyLevelName = "Clearing degrees";navSearchValue = GlobalConstants.CLEARING_NAV_TXT;}
  
  else if("M".equalsIgnoreCase(studyLevelId)){studyLevelName = "DEGREES";navSearchValue = "Undergraduate"; }
  
  else if("L".equalsIgnoreCase(studyLevelId)){studyLevelName = "Postgraduate degrees";navSearchValue = "Postgraduate";}
  else if("A".equalsIgnoreCase(studyLevelId)){studyLevelName = "Foundation degrees";navSearchValue = "Foundation degree";}
  else if("T".equalsIgnoreCase(studyLevelId)){studyLevelName = "Access and Foundation degrees";navSearchValue = "Access & Foundation";}//Changed the Access and foundation to Access & Foundation Indumathi.S Jan-27-16
  else if("N".equalsIgnoreCase(studyLevelId)){studyLevelName = "HND/HNCs";navSearchValue = "HND/HNC";}

%>
    
 <section class="hrosrch_ui fndcrse_ui fl_w100">
        <div class="ad_cnr">
          <div class="content-bg">
            <div class="hrobanr_ui fl_w100">
              <div class="srch_cnt fl_w100">
                <div class="hrohd_ui fl_w100">
                  <h1>Find a course</h1>
                  <c:if test="${not empty snippetSection}">
                    <h2>${snippetSection}</h2>
                  </c:if>
                </div>
                <div class="srch_ui fl_w100">
                  <div class="srch_box fl_w100">
                    <div id="findACourseId" class="crse_tab tab-content fl_w100 dis_fcs">
                      <div class="crse_srch dis_inblk">
                        <div class="tab_btm">
                          <c:if test="${not empty requestScope.clearingSwitchFlag}">  
                          <c:if test="${requestScope.clearingSwitchFlag eq 'OFF' }">
                           <form id="courseSearchForm" method="post" action="/home.html" onsubmit="javascript:return csSearchSubmit('OFF');">
                            <div class="sr_tab_form">
                              <div class="land_inp_grp sub_inpt fl">
                                <div class="sel_degcnr cr_srch fl">
                                  <div class="sel_deg fl_w100">
                                    <input type="text" name="qualification" id="navQualCS" class="sld_deg fnt_lbk" value="<%=navSearchValue%>" readonly="readonly" onblur="setTimeout('hideBlockCS()',500);" onclick="javascript:toggleQualListCS();" />
                                  </div>
                                  <ul id="navQualListCS" class="drop_deg" style="display: none;">
                                    <li class="fl" onclick="javascript:setQualCS(this);">Undergraduate</li>
                                    <li class="fl" onclick="javascript:setQualCS(this);">HND / HNC</li>            
                                    <li class="fl" onclick="javascript:setQualCS(this);">Foundation degree</li>
                                    <li class="fl" onclick="javascript:setQualCS(this);">Access &amp; foundation</li>
                                    <li class="fl" onclick="javascript:setQualCS(this);">Postgraduate</li>
                                    <%--<li onclick="javascript:setQualCS(this);"> UCAS CODE </li>--%>
                                  </ul>   
                                </div>
                              </div>
                             
                              <div class="land_inp_grp sr_cnty fl" id="locSrchDivId">
                                <div class="sr_country_cnr">
                                  <label for="locSrchId" class="visualhid">Please enter subject or uni</label> 
                                  <input class="inptxt" type="text" autocomplete="off" name="csKeyword" id="navKwdCS" onkeyup="autoCompleteKeywordBrowseCS(event,this);javascript:setCourseAutoCompleteClass();" placeholder="Please enter subject or uni" onfocus="javascript:clearNavSearchTextCS(this);" onclick="javascript:clearNavSearchTextCS(this);" onblur="javascript:setNavSearchText(this);" onkeypress="javascript:if(event.keyCode==13){return csSearchSubmit('OFF');}"/>
                                </div>
                                <input type="hidden" id="csKeyword_hidden" value="" />
                                <input type="hidden" id="csKeyword_id" value="" />
                                <input type="hidden" id="csKeyword_display" value="" />
                                <input type="hidden" id="csKeyword_url" value="" />
                                <input type="hidden" id="csKeyword_alias" value="" />
                                <input type="hidden" id="csKeyword_location" value=""/>   
                              </div>
                            </div>
                            <div class="btn_group fl">
                              <button id="landingSearch" type="button" class="cug_btn_blue cug_btn_prim" onclick="csSearchSubmit('OFF');">
                                <span class="srch"></span> Search
                              </button>
                            </div>
                            <input type="hidden" id="csmatchbrowsenode" value=""/>
                            <input type="hidden" id="selQualCS" value=""/>  
                          </form>
                          </c:if>
                          
                          <c:if test="${requestScope.clearingSwitchFlag eq 'ON' }">
                          <form id="courseSearchForm" method="post" action="/home.html" onsubmit="javascript:return csSearchSubmit('OFF');" commandName="coursejourneybean">
                            <div class="sr_tab_form">
                              <div class="land_inp_grp sub_inpt fl">
                                <div class="sel_degcnr cr_srch fl" >
                                  <div class="sel_deg fl_w100">
                                    <input type="text" name="qualification" id="navQualCS" class="sld_deg fnt_lbk" value="<%=navSearchValue%>" readonly="readonly" onblur="setTimeout('hideBlockCS()',500);" onclick="javascript:toggleQualListCS();" />
                                  </div>
                                  <ul id="navQualListCS" class="drop_deg" style="display: none;">
                                    <li class="fl" onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');">Undergraduate</li>
                                    <li class="fl" onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');">HND / HNC</li>            
                                    <li class="fl" onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');">Foundation degree</li>
                                    <li class="fl" onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');">Access &amp; foundation</li>
                                    <li class="fl" onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');">Postgraduate</li>
                                    <%--<li onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');"> UCAS CODE </li>--%>
                                  </ul>   
                                </div>
                              </div>
                             
                              <div class="land_inp_grp sr_cnty fl" id="locSrchDivId">
                                <div class="sr_country_cnr">
                                  <label for="locSrchId" class="visualhid">Please enter subject or uni</label> 
                                  <input class="inptxt" type="text" autocomplete="off" name="csKeyword" id="navKwdCS" onkeyup="autoCompleteKeywordBrowseCS(event,this);javascript:setCourseAutoCompleteClass();" placeholder="Please enter subject or uni" onfocus="javascript:clearNavSearchTextCS(this);" onclick="javascript:clearNavSearchTextCS(this);" onblur="javascript:setNavSearchText(this);" onkeypress="javascript:if(event.keyCode==13){return csSearchSubmit('OFF');}"/>
                                </div>
                                <input type="hidden" id="csKeyword_hidden" value="" />
                                <input type="hidden" id="csKeyword_id" value="" />
                                <input type="hidden" id="csKeyword_display" value="" />
                                <input type="hidden" id="csKeyword_url" value="" />
                                <input type="hidden" id="csKeyword_alias" value="" />
                                <input type="hidden" id="csKeyword_location" value=""/>   
                              </div>
                            </div>
                            <div class="btn_group fl">
                              <button id="landingSearch" type="button" class="cug_btn_blue cug_btn_prim" onclick="csSearchSubmit('ON');">
                                <span class="srch"></span> Search
                              </button>
                            </div>
                            <input type="hidden" id="csmatchbrowsenode" value=""/>
                            <input type="hidden" id="selQualCS" value=""/>  
                          </form>
                          </c:if>
                          </c:if>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
     </section>     
     