<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import=" WUI.utilities.CommonUtil" %>

<%--
  * @purpose:  This jsp gets included in find a courses page for i want to be results pod.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name              Ver.     Changes desc          Rel Ver.
  * 08-Dec-2020    Keerthana          1.0      First draft           wu_923
  * *************************************************************************************************************************
--%>
  
<%
  String gaAccount = new WUI.utilities.CommonFunction().getWUSysVarValue("WU_GA_ACCOUNT");
%>
<c:if test="${not empty requestScope.iwtbResultsList}"> 
  <div id="iwtbResults" class="cr_row fl srch_pod_inact">
    <div class="cr_icons grad_pic fl"></div>
    <div class="row_cont fr">
      <div class="view">
        <h3>I want to be... </h3>
        <a class="select_all fr fnt_lrg" onclick="podChangeToSearchAgain('iwtbResults', 'iwtbEntry');">SEARCH AGAIN</a>
       </div>
    <p>To continue looking at the uni courses that will lead you into your dream job, click View Degrees now! </p>
    <div class="wdg-cont">
      <c:forEach var="iwtbResults" items="${requestScope.iwtbResultsList}" varStatus="i">
        <div class="cont_ls">
          <div class="ls_lt">
          <c:if test="${ not empty iwtbResults.imagePath}">
       <c:set var = "imagePathDef" value = "${iwtbResults.imagePath}" />
           <%String imgPath = CommonUtil.getImgPath("", 1) + pageContext.getAttribute("imagePathDef");%> 
             <img class="lazy-load cs_img" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src='<%=imgPath%>'/>
           </c:if>           
          </div>
          <div class="ls_rt">
            <div class="ls_rtcnr">
              <h5><span class="tit">${iwtbResults.iwantSearchName}</span><span class="sub_ls"> ${iwtbResults.createdDate}</span></h5>
              <div class="it_lts">
                <div class="cs_rst grd_det">
                  <h6> Then you should study</h6>
                </div>
                <div class="cs_rst">
                  <span class="rst_ic wa_tb">${iwtbResults.jacsSubject}</span>
                </div>
              </div>
              <div class="rs_chrt bar_bt">
                <input type="hidden" id="searchGrades" value="0"/>
                <input type="hidden" id="searchQual" value="0"/>
                <a class="btn1 blue" onclick="ulGAEventTracking('IWTB', 'VD', '${iwtbResults.jacsSubject}', '<%=gaAccount%>'); ulSearchResults('${iwtbResults.jacsSubject}', '${iwtbResults.jacsCode}', '0')">VIEW DEGREES<i class="fa fa-long-arrow-right"></i></a>                
              </div>
            </div>
          </div>
        </div>
        </c:forEach>
    </div>
  </div>
</div>
</c:if>