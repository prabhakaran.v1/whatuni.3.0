<%@page import="WUI.utilities.GlobalConstants,WUI.utilities.CommonUtil" %>

<%--
  * @purpose:  This jsp gets included in find a courses page for what course should i do pod.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name              Ver.     Changes desc          Rel Ver.
  * 08-Dec-2020    Keerthana          1.0      First draft           wu_923
  * *************************************************************************************************************************
--%>

<%
  String prepopulation = (String)request.getAttribute("prepopulatinpage");
  String wcidEntryPod = request.getAttribute("wcidEntryPod") != null ? request.getAttribute("wcidEntryPod").toString() : "empty";
  String wcidEntryPodDisp = "display: block;";
  String wcidPodDisplayFlag = "wcidEntry";
  if("notEmpty".equals(wcidEntryPod)){
    wcidEntryPodDisp = "display: none;";
    wcidPodDisplayFlag = "wcidResults";
  }
%> 
<div id="wcidEntry" style="<%=wcidEntryPodDisp%>" class="cr_row srch_pod_inact iwtb csrch_bg">
  <div class="bxd">
    <div class="line"></div>
    <ul class="icoul fl">
      <li></li>
    </ul>
    <div class="iwb"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/wcs_bubble.svg",0)%>" height="205px" alt=""></div>
  </div>
  <h4>Not sure what to study? <br>Find out what others did with the same grades as you...</h4>
  <div class="bar_bt"> 
    <a class="btn1 bg_orange" onclick="openLightBox('whatcanido');">
    TRY NOW
    <i class="fa fa-long-arrow-right"></i>
    </a>
  </div>
  <input type="hidden" id="wcidPodDisplay" value="<%=wcidPodDisplayFlag%>" />
</div>

