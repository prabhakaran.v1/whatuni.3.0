<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction" %>

<%--
  * @purpose:  This jsp gets included in find a courses page for i want to be pod.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name              Ver.     Changes desc          Rel Ver.
  * 08-Dec-2020    Keerthana          1.0      First draft           wu_923
  * *************************************************************************************************************************
--%>

<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/iwanttobe/iwanttobe_wu578.js"></script>
<%
  String ulType = request.getParameter("ultype");
  String iwtbEntryPod = request.getAttribute("iwtbEntryPod") != null ? request.getAttribute("iwtbEntryPod").toString() : "empty";
  String iwtbEntryPodDisp = "display: block;";
  String iwtbPodDisplayFlag = "iwtbEntry";
  if("notEmpty".equals(iwtbEntryPod)){
    iwtbEntryPodDisp = "display: none;";
    iwtbPodDisplayFlag = "iwtbResults";
  }
%>
<div id="iwtbEntry" class="cr_row srch_pod_inact iwtb iwb_csrch_bg" style="<%=iwtbEntryPodDisp%>">
  <div class="bxd">
    <div class="line"></div>
    <ul class="icoul fl">
    <li></li>
    </ul>
    <div class="iwb"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/iw-bubble.svg",0)%>"  height="205px" alt=""></div>
  </div>
  <h4>Know what you want to be already? <br/>Find out how to get there</h4>
  <div class="bar_bt"> 
    <a class="btn1 bg_orange" onclick="GANewAnalyticsEventsLogging('i want to be','try now','click');openLightBox('iwanttobe');">
    TRY NOW
    <i class="fa fa-long-arrow-right"></i>
    </a>
  </div>
</div>
<input type="hidden" id="iwtbPodDisplay" value="<%=iwtbPodDisplayFlag%>" />
   
 
 






