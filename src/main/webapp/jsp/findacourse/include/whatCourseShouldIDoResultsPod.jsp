<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%--
  * @purpose:  This jsp gets included in find a courses page for what course should i do result pod.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name              Ver.     Changes desc          Rel Ver.
  * 08-Dec-2020    Keerthana          1.0      First draft           wu_923
  * *************************************************************************************************************************
--%>

<%
  String gaAccount = new WUI.utilities.CommonFunction().getWUSysVarValue("WU_GA_ACCOUNT");  
  String searchQual = "";
  String qualDispalyName = "";
%>
<c:if test="${not empty requestScope.wcidResultsList}">
<div id="wcidResults" class="cr_row fl srch_pod_inact">
  <div class="cr_icons brf_pic fl"></div>
  <div class="row_cont fr">
    <div class="view">
      <h3>What course should I do </h3>
      <a class="select_all fr fnt_lrg" onclick="podChangeToSearchAgain('wcidResults', 'wcidEntry');">SEARCH AGAIN</a>
    </div>
    <p class="cr_art">To continue looking at the uni courses other students doing the same subjects as you went on to study, click View Degrees now! </p>
    <div class="wdg-cont">      
      <div class="cont_ls det_tr">
      <c:if test="${not empty requestScope.wcidInputDetailsList}">
        <div class="tp_res">
        <c:forEach var="wcidInputDetails" items="${requestScope.wcidInputDetailsList}" varStatus="i">
            <h5>
            <c:if test="${ not empty wcidInputDetails.qualification}">
            <c:set var = "qual" value = "${wcidInputDetails.qualification}" />
                       <%                
                if("AL".equals(pageContext.getAttribute("qual").toString())){                  
                  searchQual = "a_level";
                  qualDispalyName = "A-level";
                }else if("H".equals(pageContext.getAttribute("qual").toString())){                  
                  searchQual = "sqa_higher";
                  qualDispalyName = "Scottish Highers";
                }else if("AH".equals(pageContext.getAttribute("qual").toString())){                  
                  searchQual = "sqa_adv";
                  qualDispalyName = "Scottish Advanced Highers";
                }else if("TP".equals(pageContext.getAttribute("qual").toString())){                  
                  searchQual = "ucas_points";
                  qualDispalyName = "Tariff Points";
                }else if("IB".equals(pageContext.getAttribute("qual").toString())){                  
                  searchQual = "ucas_points";
                  qualDispalyName = "IB Diploma points";
                }else if("BED".equals(pageContext.getAttribute("qual").toString())){                  
                  searchQual = "ucas_points";
                  qualDispalyName = "BTEC Extended Diploma";
                }else if("BD".equals(pageContext.getAttribute("qual").toString())){                  
                  searchQual = "ucas_points";
                  qualDispalyName = "BTEC Diploma";
                }else if("B9C".equals(pageContext.getAttribute("qual").toString())){                  
                  searchQual = "ucas_points";
                  qualDispalyName = "BTEC 90 Credit Diploma";
                }
           
              %>
              </c:if>
              <span class="tit">Your top results with your <%=qualDispalyName%> of:</span>
              <span class="sub_ls">${wcidInputDetails.createdDate}</span>
            </h5>
              <c:if test="${not empty wcidInputDetails.qualSubDesc1}">
              <div class="cs_rst tr_lt">
                <span class="rst_ic wa_tb">${wcidInputDetails.qualSubDesc1}</span>
              </div>
            </c:if>
              <c:if test="${not empty wcidInputDetails.qualSubDesc2}">
              <div class="cs_rst tr_lt">
                <span class="rst_ic wa_tb">${wcidInputDetails.qualSubDesc2}</span>
              </div>
             </c:if>
             <c:if test="${not empty wcidInputDetails.qualSubDesc3}">
              <div class="cs_rst tr_lt">
                <span class="rst_ic wa_tb">${wcidInputDetails.qualSubDesc3}</span>
              </div>
            </c:if>
             <c:if test="${not empty wcidInputDetails.qualSubDesc4}">
               <div class="cs_rst tr_lt">
                <span class="rst_ic wa_tb">${wcidInputDetails.qualSubDesc4}</span>
              </div>
            </c:if>
               <c:if test="${not empty wcidInputDetails.qualSubDesc5}">
          
              <div class="cs_rst tr_lt">
                <span class="rst_ic wa_tb">${wcidInputDetails.qualSubDesc5}</span>
              </div>
            </c:if>
             <c:if test="${not empty wcidInputDetails.qualSubDesc6}">
              <div class="cs_rst tr_lt">
                <span class="rst_ic wa_tb">${wcidInputDetails.qualSubDesc6}</span>
              </div>
            </c:if>
          </c:forEach>
        </div>
        </c:if>
        <c:forEach var="wcidResults" items="${requestScope.wcidResultsList}" varStatus="i">
        <div class="chrt_cs">
          <div class="ls_chrt">
            <h6>${wcidResults.jacsSubject}</h6>
            <div class="chl_bar">
              <span class="ct-cont">
                <span style="width:${wcidResults.jacsPercentage};" class="bar"></span> 
              </span> 
            </div>
            <div class="chl_val">${wcidResults.jacsPercentage}</div>
          </div>
          <div class="rs_chrt bar_bt pchrt">
            <a class="btn1 blue" onclick="ulGAEventTracking('WCID', 'VD', '${wcidResults.jacsSubject}', '<%=gaAccount%>'); ulSearchResults('${wcidResults.jacsSubject}', '${wcidResults.jacsCode}', '${wcidResults.searchGrades}')">VIEW DEGREES<i class="fa fa-long-arrow-right"></i></a>                      
          </div>
        </div>        
     </c:forEach>       
        <input type="hidden" id="searchQual" value="<%=searchQual%>" />  
      </div>      
    </div>
  </div>
</div>
</c:if>