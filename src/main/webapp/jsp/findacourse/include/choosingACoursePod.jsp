<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants;"%>

<%--
  * @purpose:  This jsp gets included in find a courses page for choosing a course pod.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name              Ver.     Changes desc          Rel Ver.
  * 08-Dec-2020    Keerthana          1.0      First draft           wu_923
  * *************************************************************************************************************************
--%>

<c:if test="${not empty articlePodList}">
<div class="cac">
  <h2>Advice on choosing a course</h2>
  <div class="fu_cac_lst">
      <c:forEach var="articlePodList" items="${articlePodList}" varStatus="i">
        <div class="fu_cac_item">
          <img src="<%=CommonUtil.getImgPath(GlobalConstants.ONE_PIXEL_IMG_PATH, 1)%>" data-src="${articlePodList.mediaPath}" title="article name" alt="article image">
          <div class="sub_txt">${articlePodList.personalizedText}</div>
          <a href="${articlePodList.url}" onclick="courseGALogging('${articlePodList.displayOrder}', 'article');" >${articlePodList.articleTitle}</a>
        </div>
      </c:forEach>
  </div>
  <a class="vaa_btn" id="viewAllArticles" href="/advice/research-and-prep/choosing-a-course/" onclick="courseGALogging('See All', 'article');" >
    VIEW MORE ARTICLES<i class="fa fa-long-arrow-right"></i>
  </a>
</div>
</c:if>
