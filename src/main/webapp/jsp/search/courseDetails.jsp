<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import=" WUI.utilities.CommonUtil, WUI.utilities.SessionData, WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction" %>

<%
  String courseDetailsJsName = CommonUtil.getResourceMessage("wuni.courseDetailsJsName.js", null);
  String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
  //
  String paramCourseId = request.getParameter("w") !=null && request.getParameter("w").trim().length()>0 ? request.getParameter("w") : request.getParameter("cid") !=null && request.getParameter("cid").trim().length()>0 ? request.getParameter("cid") : ""; 
  paramCourseId = paramCourseId!=null && paramCourseId.trim().length()>0 ? "" : String.valueOf(request.getAttribute("seoCourseId"));
  //
  String paramCollegeId = request.getParameter("z") !=null && request.getParameter("z").trim().length()>0 ? request.getParameter("z") : "";
  paramCollegeId = paramCollegeId!=null && paramCollegeId.trim().length()>0 ? "" : String.valueOf(request.getAttribute("seoCollegeId"));
  //
  String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");    
  if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0){ 
  cpeQualificationNetworkId = "2";
  }
  request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);
  //
  String bc_breadCrumb = request.getAttribute("breadCrumb") != null && request.getAttribute("breadCrumb").toString().trim().length() > 0 ? request.getAttribute("breadCrumb").toString() : "";
  request.setAttribute("uniLanding", "YES");
  String noindexfollow = "noindex,follow";
  boolean isClearingCourse = (((String)request.getAttribute("CLEARING_COURSE")) != null && "TRUE".equals((String)request.getAttribute("CLEARING_COURSE"))) ? true : false;  
  if(request.getAttribute("crsDetailIndex")!=null && "true".equals(request.getAttribute("crsDetailIndex"))){
    noindexfollow = "index,follow";
  }
  //modified the condition to avoid the clearing course index for june_5_18 release by sangeeth.s
  if(isClearingCourse){
    noindexfollow = "noindex,follow";
  }
  String clearingUserType = (String)session.getAttribute("USER_TYPE") ;
  String searchValue = "Undergraduate";
  String cdPageClassName = "clr_cd_cnt";
  //
  String evntCourseTitle = request.getAttribute("hitbox_course_title") != null && request.getAttribute("hitbox_course_title").toString().trim().length() > 0 ? request.getAttribute("hitbox_course_title").toString() : "";
  String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null);  
%>
   
 <body>
   <%--<jsp:include page="/clearing/watchvideo/loadWatchVideo.jsp"/>--%>
   <div id="loadingImg" class="cmm_ldericn" style="display:none;"><img alt="wugo-loading-image" src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif",0)%>"></div>
   <c:if test="${not empty requestScope.pixelTracking}">
        ${requestScope.pixelTracking}                       
   </c:if>
 <header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
</header> 
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=courseDetailsJsName%>"></script>
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/DonutChart/prefixfree.min.js"></script>
 <%if(isClearingCourse){cdPageClassName = "clr_prof_cnt clr_cd_cnt clrcd_btn";}%>
 <div class="<%=cdPageClassName%>">
 <div class="couse_details container" id="courseDetailsContainer">  
   <div id="fixedscrolltop" class="top_box_shadow">
     <div class="ad_cnr">
       <div class="content-bg">
         <div class="course_deatils">
           <% String  brClassName = "sbcr mb-10 pros_brcumb";
           if(bc_breadCrumb!=null && ("").equals(bc_breadCrumb)){
             brClassName = "sbcr mb-10 pros_brcumb pt20";
           }%>
             <div class="<%=brClassName%>" id="bcrummbs">
               <jsp:include page="/seopods/breadCrumbs.jsp"/>
             </div>
             <%if(!isClearingCourse){%>
		     <div class="prnt"><a onclick="printCssBtn();"><i class="fa fa-print"></i>Print</a></div><%--Added for print css by Prabha on 27_JAN_2016_REL--%>
             <%}%>
             <c:choose>
               <c:when test="${CLEARING_CD_PAGE eq 'CLEARING_CD_PAGE'}">
                 <jsp:include page="/jsp/clearing/coursedetails/include/headerSection.jsp"/>
               </c:when>
               <c:otherwise>
                <jsp:include page="/jsp/search/includeCdPage/courseDetailStickyNav.jsp"/>
               </c:otherwise>
             </c:choose>            
          </div>
        </div>
      </div>
    </div>
  </div>
   <br style="clear:both;">
   <div class="seperator"></div>
   <div class="ad_cnr">
     <div class="content-bg pt25">
         <jsp:include page="/jsp/common/includeDonutChartScriptJS.jsp"/>
         <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/DonutChart/index.js"> </script>
        <div class="course_deatils" id="ajaxDivContent">
          <jsp:include page="/jsp/search/includeCdPage/podList.jsp"/>      
        </div>
    </div>
   </div>
   <%if(!isClearingCourse){%>
   <section class="p0 top_footer mt20">
     <div class="ad_cnr">
        <div class="content-bg">
          <div id="searchBarClose">
          <div class="fleft">
             <c:if test="${not empty requestScope.collegeNameDisplay}">
                <div class="srch_box cl_grylt fnt_14">
                ${requestScope.collegeNameDisplay}
                </div>
            </c:if>
            <c:if test="${not empty requestScope.courseTitle}">
                   <div class="srch_box cl_grylt fnt_14">
                   ${requestScope.courseTitle}
                   </div>
            </c:if>
          </div>
          <a class="btn1 btn3 fright m0" onclick="javascript:showHideDivText('searchBarClose','searchBarOpen')">
            SEARCH AGAIN
            <i class="fa fa-long-arrow-right"></i>
          </a>
          </div>
          <div id="searchBarOpen" style="display:none">
            <div class="fleft">
             <c:if test="${not empty requestScope.courseTitle}">
                   <div class="search_txt">
                   ${requestScope.courseTitle}
                   </div>
            </c:if>
            </div>
            <a class="btn1 btn3 hvr fright m0 act" onclick="javascript:showHideDivText('searchBarOpen','searchBarClose')">
             SEARCH AGAIN
            <i class="fa fa-close"></i>
            </a>
            <div class="searc_div" id="cdPageSearchBar">
              <%
                boolean isclearing = false;
                String onsubmitFunction = "onsubmit=\"javascript:return navSearchSubmitCD('OFF');\"";
                String action =  "action=\"/home.html\"";
                String onclickFunction = "";
              %>
                          
            <c:if test="${requestScope.clearingSwitchFlag eq 'ON'}">
                <%if(!GenericValidator.isBlankOrNull(clearingUserType) && "CLEARING".equals(clearingUserType)){  searchValue = GlobalConstants.CLEARING_NAV_TXT; }%>
                <%isclearing = true; onsubmitFunction = ""; action=""; onclickFunction = "onclick=\"navSearchSubmitCD('ON');\"";%>
              </c:if>
              <form id="navSearchFormCd" method="post" <%=action%> class="topsearch" <%=onsubmitFunction%> >
                  <div class="ns_con">
                    <div class="ns_dd">
                      <fieldset>
                        
                         <input type="text" name="qualification" id="navQualCd" class="fnt_lbk sbx" value="<%=searchValue%>" readonly="readonly" onblur="setTimeout('hideBlock()',500);" onclick="javascript:toggleQualListCd();" />
                         <ul id="navQualListCd" class="ts-option" style="display: none;">
                           <%-- <%if(isclearing){%> <li onclick="javascript:setQualCd(this);"><%=GlobalConstants.CLEARING_NAV_TXT%></li> <%}%> --%> 
                            <li onclick="javascript:setQualCd(this);">Undergraduate</li>
                            <li onclick="javascript:setQualCd(this);">HND / HNC</li>                            
                            <li onclick="javascript:setQualCd(this);">Foundation degree</li>
                            <li onclick="javascript:setQualCd(this);">Access &amp; foundation</li>
                            <li onclick="javascript:setQualCd(this);">Postgraduate</li>
                            <%--<li onclick="javascript:setQualCd(this);"> UCAS CODE </li>--%>
                         </ul>
                      </fieldset>
                    </div>
                    <div class="ns_dd">
                      <fieldset>
                        <input class="fnt_lrg tx_bx" type="text" autocomplete="off" name="keywordCd" id="navKwdCd" onkeyup="autoCompleteKeywordBrowseCd(event,this)" value="Please enter subject or uni" onfocus="javascript:clearNavSearchTextCd(this);" onclick="javascript:clearNavSearchTextCd(this);" onblur="javascript:setNavSearchTextCd(this);" onkeypress="javascript:if(event.keyCode==13){return navSearchSubmitCd('OFF');}"/>
                        <input type="hidden" id="keywordCd_hidden" value="" />
                        <input type="hidden" id="keywordCd_id" value="" />
                        <input type="hidden" id="keywordCd_display" value="" />
                        <input type="hidden" id="keywordCd_url" value="" />
                        <input type="hidden" id="keywordCd_alias" value="" />
                        <input type="hidden" id="keywordCd_location" value=""/>
                      </fieldset>
                    </div>
                    <div class="ns_dd search">
                      <fieldset>
                        <%if(isclearing){%>
                        <button class="fnt_lbk src_btn_y" type="button" class="fnt_lbk src_btn_y" onclick="navSearchSubmitCD('ON');"><i class="fa fa-search1"></i></button>
                        <%}else{%><button class="fnt_lbk src_btn_y" type="submit" >
                         <i class="fa fa-search1"></i>
                        </button>
                        <%}%>
                       </fieldset>
                    </div>
                  </div>
                   <input type="hidden" id="matchbrowsenodeCd" value=""/>
                   <input type="hidden" id="selQualCd" value=""/>  
              </form>
          </div>
          </div>
         </div> 
       </div>      
   </section>
   <%}%>
 </div>
  <%if(isClearingCourse){%>
    <input type="hidden" name="isClearingCourse" id="isClearingCourse" value="<%=isClearingCourse%>"/>
     <input type="hidden" id="clearingFlag" value="CLEARING"/>
  <%}
  String advNonAdvCheck = request.getAttribute("richProfileSubOrderId") != null && request.getAttribute("richProfileSubOrderId").toString().trim().length() > 0 ? request.getAttribute("richProfileSubOrderId").toString() : "";
  String advNonAdv = "";
  if("".equals(advNonAdvCheck)){
    advNonAdv = "Non-Advertiser";
  }else{
    advNonAdv = "Advertiser";
  }
  %>
   <jsp:include page="/jsp/common/wuFooter.jsp" />  
 <%--  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery-min.js"></script>   
   <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/DonutChart/stickySidebar.js"></script>--%>
   <script type="text/javascript">      
      autocompleteOnScroll();
      loadFirstDelivery();    
      setCompareAll();
   </script>
   <%if(isClearingCourse){%>    
      <script type="text/javascript">
        pageImpressionLog('<%=paramCourseId%>', 'CLCD');
      </script>
    <%}else{%>
      <script type="text/javascript">
        pageImpressionLog('<%=paramCourseId%>', 'CD');
      </script>
    <%}%>
   <%-- ::START:: Yogeswari :: 30.06.2015 :: changed for clearing/course detail page responsive task --%>
   <script type="text/javascript">    
    dev(document).ready(function(){
      dev("#menu_item").click(function(){
        if (jqueryWidth() <= 992) {
          dev(".sidebar_left ul").slideToggle();
        }
      });
      dev("#sidebar ul li a").click(function(){
        if (jqueryWidth() <= 992) {
          dev(".sidebar_left ul").slideToggle();
        }
      });      
    });
   </script>
   <%-- ::END:: Yogeswari :: 30.06.2015 :: changed for clearing/course detail page responsive task --%>
   <script type="text/javascript"> 
     GAInteractionEventTracking('', 'Course details', '<%=advNonAdv%>', '<%=evntCourseTitle%>', '0');     
     checkskiplink();
  </script>
  <input type="hidden" id="switchQualType" value="${switchQualType}" />
  <input type="hidden" id="switchSubName" value="${switchSubName}" />
  <input type="hidden" id="switchclearingPrUrl" value="${switchclearingPrUrl}" />
</body>


