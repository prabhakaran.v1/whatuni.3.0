<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.SessionData,WUI.utilities.GlobalConstants, WUI.utilities.GlobalFunction, mobile.util.MobileUtils, com.wuni.util.seo.SeoUrls" %>
<%String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
<% String toolTip = new CommonFunction().getDomainName(request, "N"); 
   String CONTEXT_PATH = GlobalConstants.WU_CONTEXT_PATH;
  String searchPageType = request.getParameter("searchPageType");
         searchPageType = !GenericValidator.isBlankOrNull(searchPageType)?searchPageType:"";
  String clearingPage =  (String)request.getAttribute("searchClearing");
         clearingPage = GenericValidator.isBlankOrNull(clearingPage)?"":"TRUE";
  String searchhits = (String) request.getAttribute("universityCount");
  String urlString = request.getAttribute("urlString") != null ? request.getAttribute("urlString").toString() : "";  
  String pageFrom = request.getParameter("PAGE_FROM");
  String keyword =  "";
  if(("KEWORD_MONEY_PAGE").equals(pageFrom)){   keyword =  (String)request.getAttribute("SEARCH_KEYWORD_OR_SUBJECT");}else{keyword =  (String)request.getAttribute("keywordOrSubject");}  
  String urlSTR = (String) request.getAttribute("URL_STRING");  
  String formatedURL = CONTEXT_PATH + urlSTR+".html";
  String univiewUrl = null;
  String studyLevelModified = request.getParameter("studyLevelModified"); //29_OCT_2013_REL
          studyLevelModified = studyLevelModified != null && studyLevelModified.trim().length()>0 ? studyLevelModified : ""; 
  String studyLevelOnly = request.getParameter("studyLevelOnly"); //29_OCT_2013_REL
          studyLevelOnly = studyLevelOnly != null && studyLevelOnly.trim().length()>0 ? studyLevelOnly : ""; 
  if(studyLevelOnly.indexOf("degree")==-1){
        studyLevelOnly = studyLevelOnly.indexOf("hnd/hnc") > -1 ? studyLevelOnly.toUpperCase() :studyLevelOnly;
        studyLevelOnly = studyLevelOnly + " degree";
  }
  String sortBy = (String)request.getAttribute("sortByValue");   
  String reviewText = "";
  String categoryText = "university of the year";
  if(("r-or").equals(sortBy)){ reviewText = "" ;categoryText="university of the year";}
  if(("r-clr").equals(sortBy)){ reviewText = "Course & Lecturers";categoryText="course & lecturers";}
  if(("r-ar").equals(sortBy)){ reviewText = "Accomodation";categoryText="accommodation";} 
  if(("r-cylr").equals(sortBy)){ reviewText = "City Life";categoryText="city life";}
  if(("r-ufr").equals(sortBy)){ reviewText = "Uni Facilities";categoryText="uni facilities";}
  if(("r-csr").equals(sortBy)){ reviewText = "Clubs and Societies";categoryText="clubs & societies";}
  if(("r-sur").equals(sortBy)){ reviewText = "Student Union";categoryText="student union";} 
  // if(("r-ecr").equals(sortBy)){ reviewText = "Eye Candy" ;} //08_OCT_2014 Added by Amir to remove Eye Candy
  if(("r-jpr").equals(sortBy)){ reviewText = "Job Prospectus";categoryText="job prospects";} 
  if(("r-rr").equals(sortBy)){ reviewText = "RECOMMEND THIS UNI" ;} 
  String providerSortBy = sortBy;
  if(sortBy.contains("r-")||sortBy.equals("r")){providerSortBy="";}
  CommonFunction commonFunction = new CommonFunction();
  String qString = commonFunction.formProviderResultFilterParam(request);
         qString = !GenericValidator.isBlankOrNull(qString)? ("?nd"+qString):"";
  if(providerSortBy!=""){
        qString = qString + (GenericValidator.isBlankOrNull(qString)? ("?nd&sort="+providerSortBy):("&sort="+providerSortBy));
  }
  String keywordorsubjectemail = request.getAttribute("keywordOrSubject") != null ? new CommonFunction().replaceURL(request.getAttribute("keywordOrSubject").toString()) : "0";  
  String uniHomeUrl = "";
  String clbtnclassname = "";
  String sponsporedpageLogging ="";
  String hotlinelogging  = "";
  String innerhotlinelogging = "";
  if("CLEARING".equals(searchPageType)){ clbtnclassname ="clr_btns";}
  String clrText = "";
  String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");
  String imgpath="";
  String capeImg = CommonUtil.getImgPath("/wu-cont/images/capeh.png",0);
  String matchOnImg = CommonUtil.getImgPath("/wu-cont/images/match_yes.png",0);
  String matchOffImg = CommonUtil.getImgPath("/wu-cont/images/match_no.png",0);
  String onePxImg = CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0);
  boolean mobileFlag = new MobileUtils().userAgentCheck(request);
  String reviewCatStr_1 = (String)request.getAttribute("reviewCatStr_1");
  String reviewCatStr_2 = (String)request.getAttribute("reviewCatStr_2");
  String reviewCatStr_3 = (String)request.getAttribute("reviewCatStr_3");
  SeoUrls seoUrl = new SeoUrls();
%>
<c:set var="capeImg" value="<%=capeImg%>"/>
<c:set var="matchOnImg" value="<%=matchOnImg%>"/>
<c:set var="matchOffImg" value="<%=matchOffImg%>"/>
<c:set var="onePxImg" value="<%=onePxImg%>"/>
<c:set var="CONTEXT_PATH" value="<%=CONTEXT_PATH%>"/>
<c:set var="keywordorsubjectemail" value="<%=keywordorsubjectemail%>"/>
<c:set var="searchPageType" value="<%=searchPageType%>"/>
<c:if test="${searchPageType eq 'CLEARING'}">
	<%clrText = "clearing-";%>
   <input type="hidden" name="comparepage" id="comparepage" value="CLEARING"/>
</c:if>
<jsp:include page="/jsp/search/include/noResultsBar.jsp"/>
<jsp:include page="/jsp/search/include/searchGoogleAdSlots.jsp"/>
<c:if test="${not empty listOfSearchResults}">

<c:forEach var="searchResult" items="${listOfSearchResults}" varStatus="i">
    <%String gaCollegeName ="";%>
    <c:set var="collegeName" value="${searchResult.collegeName}"/>
    <c:set var="collegeId" value="${searchResult.collegeId}"/>
    <c:set var="hcCollegeId" value="${searchResult.collegeId}"/>
     <%
     gaCollegeName = commonFunction.replaceSpecialCharacter((String)pageContext.getAttribute("collegeName"));      
      String institutionId = new GlobalFunction().getInstitutionId((String)pageContext.getAttribute("hcCollegeId"));  
    %>
    <c:set var="gaCollegeName" value="<%=gaCollegeName%>"/>
    <c:if test="${searchPageType eq 'CLEARING'}">
    <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
        <div class="clr_pop_cont">
          <div class="clr_pop_txt">
            <i class="fa fa-star"></i>
            Popular in this subject
          </div>
        </c:if>    
    </c:if>
     
    <div class="lst cf opd_wrp">
      <div id="skipaddnal${i.index}"></div><%--25_MAR_2014_REL--%>
        <%--Add to compare and search results redesign, 03_Feb_2015 By Thiyagu G--%>
        <div class="uni_Cnr">
        <c:if test="${empty searchResult.collegeLogoPath}"><div class="lgo">&nbsp;</div></c:if>
        <c:if test="${not empty searchResult.collegeLogoPath}">
        <c:set var="collegeLogoPath" value="${searchResult.collegeLogoPath}"/>
        <c:set var="loop" value="${i.index}"/>
         <% imgpath=CommonUtil.getImgPath((String)pageContext.getAttribute("collegeLogoPath"),Integer.parseInt(String.valueOf(pageContext.getAttribute("loop"))));%>
        <div class="lgo">
        <c:choose>
           <c:when test="${(i.index) >= 1}">
            <img src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=imgpath%>" width="105" data-src-mobile="<%=imgpath%>" title="${searchResult.collegeNameDisplay}" class="ibdr" alt="${searchResult.collegeNameDisplay}"/>          
           </c:when>
           <c:otherwise>
            <img src="<%=imgpath%>" data-src-mobile="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" width="105" title="${searchResult.collegeNameDisplay}" class="ibdr" alt="${searchResult.collegeNameDisplay}"/>          
         </c:otherwise>
          </c:choose>
            <%--Removed russell group image, 03_Feb_2015 By Thiyagu G--%>
            <%--<logic:notEmpty  name="searchResult" property="russelGroup">
                <logic:equal name="searchResult" property="russelGroup" value="Y"><br/><span class="rs-grp">Russell group</span></logic:equal>
            </logic:notEmpty>--%>
            </div>
        </c:if>
        <c:if test="${empty searchResult.collegeLogoPath}">
          <%imgpath="";%>
        </c:if>       
        <div class="uni_Dec">          
          <div class="w430">
        <h3 class="hed1">
        <c:choose>
        <c:when test="${searchPageType eq 'CLEARING'}">
          <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
              <a href="${searchResult.uniHomeUrl}?clearing" onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','${searchResult.collegeId}','"${searchResult.uniHomeUrl}?clearing')">${searchResult.collegeNameDisplay}</a>
            </c:if>
            <c:if test="${searchResult.isSponsoredCollege ne 'Y'}">      
               <a href="${searchResult.uniHomeUrl}?clearing" >${searchResult.collegeNameDisplay}</a>
            </c:if>
        </c:when>
        <c:otherwise>
          <a onclick="sponsoredListGALogging(${collegeId});gtmproductClick(${i.index}, '', ${collegeId});" href="${searchResult.uniHomeUrl}">${searchResult.collegeNameDisplay}</a>
        </c:otherwise>
        </c:choose>
        </h3>
          <p class="pb-5">
            <c:choose>
        <c:when test="${searchPageType eq 'CLEARING'}">
           <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
                <a rel="nofollow" onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','${searchResult.collegeId}','${searchResult.collegePRUrl}')" href='${searchResult.collegePRUrl}'><strong>${searchResult.numberOfCoursesForThisCollege} <%=keyword%> <%=studyLevelOnly%><c:if test="${searchResult.numberOfCoursesForThisCollege gt 0}">s</c:if></strong></a>
              </c:if>
              <c:if test="${searchResult.isSponsoredCollege ne 'Y'}"> 
                 <a rel="nofollow" href='${searchResult.collegePRUrl}'><strong>${searchResult.numberOfCoursesForThisCollege} <%=keyword%> <%=studyLevelOnly%><c:if test="${searchResult.numberOfCoursesForThisCollege gt 1}">s</c:if></strong></a>     
              </c:if>
              </c:when>
              <c:otherwise>
            <a rel="nofollow" onclick="sponsoredListGALogging(${collegeId});gtmproductClick(${i.index}, 'sr_to_pr', ${collegeId});" href='${searchResult.collegePRUrl}' id="sr_to_pr_${i.index}"><strong><c:out value="${searchResult.numberOfCoursesForThisCollege}"/> <%=keyword%> <%=studyLevelOnly%><c:if test="${searchResult.numberOfCoursesForThisCollege gt 1}">s</c:if></strong></a>
          </c:otherwise>
          </c:choose>
          </p>
       </div>
       <c:if test="${not empty searchResult.overallRating}">
      <c:if test="${searchResult.overallRating gt 0}">  
           <ul class="strat cf">        
              <li class="mr-15 leag_wrap">
              <%if(!mobileFlag){%>
                <span  class="fl mr10">OVERALL RATING  <span class="cal"> </span></span>        
                <span class="rat${searchResult.overallRating} t_tip">
								          <%--Over all rating tool tip added by Prabha on 27_JAN_2015_REL--%>
								          <span class="cmp">
								           <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									          <div class="line"></div>
								         </span>
								         <%--End of rating tool tip code--%>
								       </span>
               
								       <span class="rtx">(${searchResult.exactRating})</span>    
              <%}%> 
               <c:choose>
                 <c:when test="${searchPageType eq 'CLEARING'}">            
                 <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
                       <a class="link_blue"  onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','${searchResult.collegeId}','${searchResult.uniReviewsURL}')" href="${searchResult.uniReviewsURL}">
                    </c:if>
                    <c:if test="${searchResult.isSponsoredCollege ne 'Y'}">
                       <a class="link_blue" href="${searchResult.uniReviewsURL}">
                    </c:if>
                 </c:when>
                 <c:otherwise>
                    <a class="link_blue" href="${searchResult.uniReviewsURL}">
                 </c:otherwise>
                 </c:choose>
                 <c:if test="${not empty searchResult.reviewCount}">
                 <c:if test="${searchResult.reviewCount ne 0}">
                  ${searchResult.reviewCount}
                  <c:if test="${searchResult.reviewCount gt 1}">
                     reviews
                  </c:if>
                  <c:if test="${searchResult.reviewCount eq 1}">  
                     review
                  </c:if>
                  </c:if>
                </c:if>
                </a>
              </li>
           </ul>
        </c:if>
       </c:if>    
        </div>
       </div>                   
     <!-- Data -->
     <div class="srch_cnr">       
     <c:if test="${not empty searchResult.bestMatchCoursesList}">
     <c:set var="bestMatchCourseList" value="${searchResult.bestMatchCoursesList}"/>
    
     <c:forEach var="bestMatchCourse" items="${bestMatchCourseList}" end="1">
          <c:set var="courseId2" value="${bestMatchCourse.courseId}"/>
            <div class="srlogo_lt">
              <div class="logo_cmp cmp-bl">
             <c:if test="${bestMatchCourse.wasThisCourseShortlisted eq 'TRUE'}">
                  <div class="cmlst" id="basket_div_${courseId2}" style="display:none;"> 
                  <c:set var="srcEvntCourseTitle1" value="${bestMatchCourse.courseTitle}"/>                    
                    <div class="compare" onmouseover="addToComparisonHover(${courseId2});" onmouseout="addToComparisonOut(${courseId2});"> <%--17_Mar_2015 - Added to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                    <a onclick='addBasket("${courseId2}", "O", this,"basket_div_${courseId2}", "basket_pop_div_${courseId2}", "", "", "${gaCollegeName}");searchEventTracking("view comparison","<%=clrText%>clicked","<%=commonFunction.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle1"))%>: ${gaCollegeName}");'>
                      <span class="icon_cmp f5_hrt"></span>
                      <span class="cmp_txt">Compare</span>
                      <span id="load_${courseId2}" style="display:none;"></span>
                      <span class="chk_cmp"><span class="chktxt" id="show1_${courseId2}" style="display:none;">Add to comparison<em></em></span></span> <%--17_Mar_2015 - Added reference id to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                    </a>
                    </div>
                  </div>
                  <div class="cmlst act" id="basket_pop_div_${courseId2}"> 
                    <div class="compare">
                    <a class="act" onclick='addBasket("${courseId2}", "O", this,"basket_div_${courseId2}", "basket_pop_div_${courseId2}");searchEventTracking("view comparison","<%=clrText%>removed","<%=commonFunction.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle1"))%>: ${gaCollegeName}");'>
                      <span class="icon_cmp f5_hrt hrt_act"></span>
                      <span class="cmp_txt">Compare</span>
                      <span class="loading_icon" id="load1_${courseId2}" style="display:none;"></span>
                      <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                    </a>
                    </div>
                    <div id="defaultpop_${courseId2}" class="sta" style="display: block;">
                        <%
                            String userId = new SessionData().getData(request, "y");
                        %>
                        <c:set var="userId" value="<%=userId%>"/>
                         <c:choose>
                       <c:when test="${not empty userId}">
                       <c:if test="${userId ne 0}">
                            <a class="view_more" href="${CONTEXT_PATH}/comparison">View comparison</a>
                            </c:if>
                       </c:when>
                       <c:otherwise>
                            <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
                       </c:otherwise>
                       </c:choose>
                      </div>
                    <div id="pop_${courseId2}" class="sta" style="display:none;"></div>
                  </div>
                
                 </c:if>
                <c:if test="${bestMatchCourse.wasThisCourseShortlisted eq 'FALSE'}">
                  <div class="cmlst" id="basket_div_${courseId2}"> 
                  <c:set var="srcEvntCourseTitle2" value="${bestMatchCourse.courseTitle}"/> 
                    <div class="compare" onmouseover="addToComparisonHover(${courseId2});" onmouseout="addToComparisonOut(${courseId2});"> <%--17_Mar_2015 - Added to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                    <a onclick='addBasket("${courseId2}", "O", this,"basket_div_${courseId2}", "basket_pop_div_${courseId2}", "", "", "${gaCollegeName}");searchEventTracking("view comparison","<%=clrText%>clicked","<%=commonFunction.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle2"))%>: ${gaCollegeName}");'>
                      <span class="icon_cmp f5_hrt"></span>
                      <span class="cmp_txt">Compare</span>
                      <span class="loading_icon" id="load_${courseId2}" style="display:none;"></span>
                      <span class="chk_cmp"><span class="chktxt" id="show1_${courseId2}" style="display:none;">Add to comparison<em></em></span></span> <%--17_Mar_2015 - Added reference id to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                    </a>
                    </div>
                  </div>
                  <div class="cmlst act" id="basket_pop_div_${courseId2}" style="display:none;"> 
                    <div class="compare">
                    <a class="act" onclick='addBasket("${courseId2}", "O", this,"basket_div_${courseId2}", "basket_pop_div_${courseId2}");searchEventTracking("view comparison","<%=clrText%>removed","<%=commonFunction.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle2"))%>: ${gaCollegeName}");'>
                      <span class="icon_cmp f5_hrt hrt_act"></span>
                      <span class="cmp_txt">Compare</span>
                      <span class="loading_icon" id="load1_${courseId2}" style="display:none;"></span>
                      <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                    </a>
                    </div>
                    <div id="defaultpop_${courseId2}" class="sta" style="display: block;">
                        <%String userId = new SessionData().getData(request, "y");                 
                        %>
                        <c:set var="userId" value="<%=userId%>"/>
                       <c:choose>
                       <c:when test="${not empty userId}">
                       <c:if test="${userId ne 0}">
                            <a class="view_more" href="${CONTEXT_PATH}/comparison">View comparison</a>
                            </c:if>
                             </c:when>
                        <c:otherwise>
                            <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
                       </c:otherwise>
                       </c:choose>
                      </div>
                    <div id="pop_${courseId2}" class="sta"></div>
                  </div>
                </c:if>
              </div>
            </div>
            <div class="dtls" onmouseover="addToComparisonHover(${courseId2},${i.index});" onmouseout="addToComparisonOut(${courseId2},${i.index});"> <%--17_Mar_2015 - Added to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
            <div class="blu cf">
            <c:if test="${not empty bestMatchCourse.matchPercentage}">
              <div class="mch_scr">
                <a id="perc_${i.index}"><c:out value="${bestMatchCourse.matchPercentage}"/> % match</a>
                
                <div class="mch_on" id="dskTooltip_perc_${i.index}" style="display:none;">
                  <a class="mch_opn" id="mch_opn2">
                    <ul>
                       <li>Matches on:</li>
                       <c:if test="${not empty bestMatchCourse.qualificationMatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.qualificationMatchFlag eq 'Y'}">
                             <img src="${matchOnImg}" alt="Study Level" class="mch_s">
                         </c:if>
                          <c:if test="${bestMatchCourse.qualificationMatchFlag eq 'N'}">
                             <img src="${matchOffImg}" alt="Study Level" class="mch_no">
                         </c:if>
                       </span>
                       Study Level
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.subjectMatchFlag}">
                     <li>
                       <span class="mch_icn">
                        <c:if test="${bestMatchCourse.subjectMatchFlag eq 'Y'}">
                             <img src="${matchOnImg}" alt="Entry Requirements" class="mch_s">
                         </c:if>
                         <c:if test="${bestMatchCourse.subjectMatchFlag eq 'N'}">
                             <img src="${matchOffImg}" alt="Entry Requirements" class="mch_no">
                         </c:if>
                       </span>
                       Subject
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.entryLevelMatchFlag}">
                     <li>
                       <span class="mch_icn">
                         <c:if test="${bestMatchCourse.entryLevelMatchFlag eq 'Y'}">
                         <img src="${matchOnImg}" alt="Entry Requirements" class="mch_s">
                         </c:if>
                          <c:if test="${bestMatchCourse.entryLevelMatchFlag eq 'N'}">
                             <img src="${matchOffImg}" alt="Entry Requirements" class="mch_no">
                         </c:if>
                       </span>
                       Entry Requirements
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.assessedMatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.assessedMatchFlag eq 'Y'}">
                             <img src="${matchOnImg}" alt="assessed" class="mch_s">
                         </c:if>
                         <c:if test="${bestMatchCourse.assessedMatchFlag eq 'N'}">
                        <img src="${matchOffImg}" alt="assessed" class="mch_no">
                        </c:if>
                       </span>
                       How you're assessed
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.locationTypeMatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.locationTypeMatchFlag eq 'Y'}">
                             <img src="${matchOnImg}" alt="Location Type" class="mch_s">
                         </c:if>
                          <c:if test="${bestMatchCourse.locationTypeMatchFlag eq 'N'}">
                          <img src="${matchOffImg}" alt="Location Type" class="mch_no">
                         </c:if>
                       </span>
                       Location Type
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.studyModeMatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.studyModeMatchFlag eq 'Y'}">
                             <img src="${matchOnImg}" alt="Study Mode" class="mch_s">
                         </c:if>
                         <c:if test="${bestMatchCourse.studyModeMatchFlag eq 'N'}">
                          <img src="${matchOffImg}" alt="Study Mode" class="mch_no">
                        </c:if>
                       </span>
                       Study Mode
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.locationMatchFlag}"> 
                     <li>
                       <span class="mch_icn">
                        <c:if test="${bestMatchCourse.locationMatchFlag eq 'Y'}">
                         <img src="${matchOnImg}" alt="Location" class="mch_s">
                         </c:if>
                         <c:if test="${bestMatchCourse.locationMatchFlag eq 'N'}">
                          <img src="${matchOffImg}" alt="Location" class="mch_no">
                         </c:if>
                       </span>
                       Location
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.reviewSub1MatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.reviewSub1MatchFlag eq 'Y'}">
                        <img src="${matchOnImg}" alt="review cat 1" class="mch_s">
                         </c:if>
                          <c:if test="${bestMatchCourse.reviewSub1MatchFlag eq 'N'}">
                          <img src="${matchOffImg}" alt="review cat 1" class="mch_no">
                         </c:if>
                       </span>
                       <c:if test="${not empty requestScope.reviewCatStr_1}">${requestScope.reviewCatStr_1}</c:if>
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.reviewSub2MatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.reviewSub2MatchFlag eq 'Y'}">
                             <img src="${matchOnImg}" alt="review cat 2" class="mch_s">
                         </c:if>
                         <c:if test="${bestMatchCourse.reviewSub2MatchFlag eq 'N'}">
                         <img src="${matchOffImg}" alt="review cat 2" class="mch_no">
                        </c:if>
                       </span>
                       <c:if test="${not empty requestScope.reviewCatStr_2}">${requestScope.reviewCatStr_2}</c:if>
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.reviewSub3MatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.reviewSub3MatchFlag eq 'Y'}">
                       <img src="${matchOnImg}" alt="review cat 3" class="mch_s">
                        </c:if>
                        <c:if test="${bestMatchCourse.reviewSub3MatchFlag eq 'N'}">
                         <img src="${matchOffImg}" alt="review cat 3" class="mch_no">
                        </c:if>
                       </span>
                       <c:if test="${not empty requestScope.reviewCatStr_3}">${requestScope.reviewCatStr_3}</c:if>  
                     </li>
                   </c:if>
                    </ul>
                  </a>
                </div>
              </div>
            
              </c:if>
               <div class="codls">
                 <c:if test="${searchResult.sponsoredListFlag  eq 'SPONSORED_LISTING'}">
                   <div class="feabrnd_tltp">
				     <span class="tooltip_cnr">
					   <span class="blk_txt" id="${searchResult.sponsoredListFlag }" data-id="sponsored_stats_not_logged">SPONSORED LISTING</span> 
					 </span>
				   </div>
				 </c:if>
                 <h2>
                   <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
                     <a onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','<c:out value="${searchResult.collegeId}"/>','<c:out value="${bestMatchCourse.courseDetailsPageURL}"/>')" href="<c:out value="${bestMatchCourse.courseDetailsPageURL}"/>">
                      ${bestMatchCourse.courseTitle}
                     </a>
                    </c:if>
                   <c:if test="${searchResult.isSponsoredCollege ne 'Y'}">
                       <a onclick="sponsoredListGALogging(${collegeId});gtmproductClick(${i.index}, 'sr_to_cd', ${collegeId});" href="<c:out value="${bestMatchCourse.courseDetailsPageURL}"/>" id="sr_to_cd_${i.index}">
                         ${bestMatchCourse.courseTitle}
                       </a>
                   </c:if>
                  </h2>
                  <div class="enreq cf">
                    <div class="blk2 blk5">
                      <%--Added by Indumathi.S Mar-29-2016--%>
                      <c:if test="${not empty bestMatchCourse.departmentOrFaculty}">
                        <c:if test="${not empty bestMatchCourse.spUrl}">
                        <p class="cnl nw"><a href="<c:out value="${bestMatchCourse.spUrl}"/>"><c:out value="${bestMatchCourse.departmentOrFaculty}"/>
                          <img src="${capeImg}" alt="<c:out value="${bestMatchCourse.departmentOrFaculty}"/>"></a></p>
                        </c:if>
                        <c:if test="${empty bestMatchCourse.spUrl}">
                        <p class="cnl"><c:out value="${bestMatchCourse.departmentOrFaculty}"/></p>
                        </c:if>
                      </c:if>
                      <%--End--%>
                    </div>
                    <div class="blk1">
                    <c:if test="${not empty bestMatchCourse.ucasCode}">
                       <span><strong>${bestMatchCourse.ucasCode}</strong>  UCAS code </span>
                     </c:if>
                     <c:if test="${not empty bestMatchCourse.employmentRate}">
                       <span><strong>${bestMatchCourse.employmentRate}%</strong> 
                          <span class="tooltip_cnr">
                             <span class="blk_txt">Employment rate </span>
                             <span class="tool_tip fl">
                                <i class="fa fa-question-circle fnt_24">
                                  <span class="cmp">
                                    <div class="hdf5"></div>
                                    <div><spring:message code="wuni.tooltip.kis.entry.text" /></div> <%--Changed KIS text and year to App.properties for 15_Nov_2016 By Thiyagu G--%>
                                    <div class="line"></div>
                                  </span>
                                </i>
                              </span>
                          </span>                       
                       </span>
                     </c:if>
                     <c:if test="${not empty bestMatchCourse.courseRank}">
                       <span><strong>${bestMatchCourse.courseRank}${bestMatchCourse.ordinalRank}</strong>
                         <span class="tooltip_cnr">
                             <span class="blk_txt">CompUniGuide subject ranking </span>
                             <span class="tool_tip fl">
                                <i class="fa fa-question-circle fnt_24">
                                  <span class="cmp">
                                    <div class="hdf5"></div>
                                    <div> <spring:message code="wuni.tooltip.cug.ranking.text" /></div> <%--Changed ranking tooltip content from property file for 08_MAR_2016 By Thiyagu G--%>
                                    <div class="line"></div>
                                  </span>
                                </i>
                              </span>
                            </span>
                          </span>
                     </c:if>
                    </div>
                    <%if(studyLevelModified != null && studyLevelModified!="" && !studyLevelModified.contains("postgraduate")){%>                
                        <div class="er_rht_pod">
                            <div class="blk1 rt">
                              <span class="tooltip_cnr ttip_cnt">
                                <span class="fad">Entry requirements</span>
                                 <c:if test="${not empty bestMatchCourse.courseUcasTariff}">
                                  <span class="tool_tip fl">
                                  <i class="fa fa-question-circle fnt_24">
                                    <span class="cmp">
                                    <div class="hdf5"></div>
                                    <div><spring:message code="wuni.tooltip.entry.requirements.text"/></div> 
                                    <div><spring:message code="wuni.tooltip.entry.requirements.further.info.text"/></div> 
                                    <div class="line"></div>
                                    </span>
                                  </i> 
                                  </span>
                                  </c:if>   
                              </span>
                               <c:if test="${not empty bestMatchCourse.courseUcasTariff}">
                                ${bestMatchCourse.courseUcasTariff}
                              </c:if>
                              <c:if test="${empty bestMatchCourse.courseUcasTariff}">
                                 <span>Please contact Uni directly</span>
                              </c:if>
                            </div>
                          </div>
                            
                    <%}if(studyLevelModified != null && studyLevelModified!="" && studyLevelModified.contains("postgraduate")){%>
                    <div class="er_rht_pod" style="display:none;">
                    <c:if test="${not empty bestMatchCourse.courseDomesticFees}">
                        <div class="blk1 rt tu_fees">
                          <span class="fad">Tuition fees</span>
                          <span><strong><c:out value="${bestMatchCourse.courseDomesticFees}"/></strong><c:out value="${bestMatchCourse.feeDuration}"/></span>
                        </div>
                      </c:if>
                      <c:if test="${empty bestMatchCourse.courseDomesticFees}">
                      <div class="blk1 rt tu_fees">
                          <span class="fad">Tuition fees</span><br/>
                          <span>Fee not supplied by uni</span>
                      </div>
                     </c:if>   
                     </div>
                   <%}%> 
                  </div>
                  <c:if test="${not empty bestMatchCourse.courseClearingPlaces}">
                    <div class="clav sr">
                      <span>${bestMatchCourse.courseClearingPlaces} clearing place<c:if test="${bestMatchCourse.courseClearingPlaces gt 1}">s</c:if> available</span> <c:if test="${not empty bestMatchCourse.lastUpdated}">(${bestMatchCourse.lastUpdated})</c:if>
                    </div>
                  </c:if>
                  <c:if test="${not empty bestMatchCourse.moduleGroupDetails}">
                     <div class="mdls">
                        <a class="sh pls" id="module<c:out value="${bestMatchCourse.moduleGroupId}"/>1" onclick="loadmoduledata('1', '<c:out value="${bestMatchCourse.moduleGroupId}"/>','<c:out value="${bestMatchCourse.courseDetailsPageURL}"/>')">
                          <em>Show</em>
                         <c:out value="${bestMatchCourse.moduleTitle}"/>
                       </a>
                        <div id="module_data_<c:out value="${bestMatchCourse.moduleGroupId}"/>1" style="display:none;">
                        <c:if test="${not empty bestMatchCourse.moduleDetailList}">
                           <c:forEach var="moduleDetailList" items="${bestMatchCourse.moduleDetailList}">
                                <ul>                           
                                      <li>${moduleDetailList}</li>                           
                                </ul>
                              </c:forEach>
                           </c:if>
                          <a class="vwall" onclick="sponsoredListGALogging(${collegeId});gtmproductClick(${i.index}, 'sr_to_cd_view_modules', ${collegeId});" href="<c:out value="${bestMatchCourse.courseDetailsPageURL}"/>" >View all modules</a>  
                        </div>
                     </div>                    
                  </c:if>
                  <c:if test="${not empty bestMatchCourse.addedToFinalChoice}">
                   <c:if test="${bestMatchCourse.addedToFinalChoice eq 'N'}">
                      <div class="myFnChLbl" style="display:none;">                    
                       ${searchResult.collegeNameDisplay}#${searchResult.collegeId}##${searchResult.courseTitle}#${searchResult.courseId}
                      </div>               
                    </c:if>
                  </c:if>
                 </div>               
                
                <%-- Search Interaction button code for responsive redesign Indumathi.S 03_NOV_2015_REL--%>
                <div class="res_btn abtst_v2">                 
                 <div class="btns <%=clbtnclassname%>" id="intBtn">
                   <!--Added By Thiyagu G for 24_Nov_2015 Start-->
                   <c:if test="${not empty bestMatchCourse.subOrderItemId}">
                   <c:if test="${bestMatchCourse.subOrderItemId eq 0}">                     
                            <div class="sremail">
                          <a rel="nofollow" 
                              target="_blank"
                              class="req-inf"
                              onclick="javaScript:nonAdvPdfDownload('${gaCollegeName}#<c:out value="${bestMatchCourse.webformPrice}"/>#${collegeId}#<c:out value="${bestMatchCourse.subOrderItemId}"/>#<c:out value="${bestMatchCourse.cpeQualificationNetworkId}"/>#<c:out value="${bestMatchCourse.subOrderEmailWebform}"/>#<c:out value="${bestMatchCourse.courseId}"/>#<%=imgpath%>#COURSE#<%=institutionId%>');"
                              title="Email <c:out value="${searchResult.collegeNameDisplay}"/>">
                            Request info<i class="fa fa-caret-right"></i>
                          </a>
                          </div> 
                          </c:if>
                          </c:if>
                          
                          <c:if test="${empty bestMatchCourse.subOrderItemId}">         
                          <div class="sremail">
                          <a rel="nofollow" 
                              target="_blank"
                              class="req-inf"
                              onclick="javaScript:nonAdvPdfDownload('${gaCollegeName}#<c:out value="${bestMatchCourse.webformPrice}"/>#${collegeId}#<c:out value="${bestMatchCourse.subOrderItemId}"/>#<c:out value="${bestMatchCourse.cpeQualificationNetworkId}"/>#<c:out value="${bestMatchCourse.subOrderEmailWebform}"/>#<c:out value="${bestMatchCourse.courseId}"/>#<%=imgpath%>#COURSE#<%=institutionId%>');"
                              title="Email <c:out value="${searchResult.collegeNameDisplay}"/>">
                            Request info<i class="fa fa-caret-right"></i>
                          </a>
                          </div>
                      </c:if>
                       <c:if test="${not empty bestMatchCourse.subOrderItemId}">            
                  <c:if test="${bestMatchCourse.subOrderItemId ne 0}">
                  <c:set var="subOrderId1" value="${bestMatchCourse.subOrderItemId}"/>        
                     <%sponsporedpageLogging = "";%>
                     <c:choose>
                     
                     <c:when test="${searchPageType eq 'CLEARING'}">
                      <c:if test="${clearingonoff eq 'ON'}">
                        <div class="srwebsite">
                        <c:if test="${not empty bestMatchCourse.subOrderWebsite}">
                        <c:set var="visitwebsitelink" value="${bestMatchCourse.subOrderWebsite}"/>  
                            <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">                              
                             <% sponsporedpageLogging = "javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','"+pageContext.getAttribute("collegeId")+"','"+pageContext.getAttribute("visitwebsitelink")+" ')";%>                           
                            </c:if>
                            <a rel="nofollow" 
                                target="_blank" 
                                class="visit-web"
                                id="${collegeId}" 
                                onclick="sponsoredListGALogging(${collegeId});
                                         GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', <c:out value="${bestMatchCourse.websitePrice}"/>); cpeWebClickClearingWithCourse(this,'${collegeId}','${courseId}','<c:out value="${bestMatchCourse.subOrderItemId}"/>','<c:out value="${bestMatchCourse.cpeQualificationNetworkId}"/>','<c:out value="${bestMatchCourse.subOrderWebsite}"/>');<%=sponsporedpageLogging%>;addBasket('${courseId2}', 'O', 'visitweb','basket_div_${courseId2}', 'basket_pop_div_${courseId2}', '', '', '${gaCollegeName}');
                                         gtmproductClick(${i.index}, '', ${collegeId});var a='s.tl(';"  
                                href="${CONTEXT_PATH}/visitwebredirect.html?id=<c:out value="${bestMatchCourse.subOrderItemId}"/>"
                                title="<c:out value="${searchResult.collegeNameDisplay}"/> website"> Visit website
                                <i class="fa fa-caret-right"></i></a>
                          </c:if>
                          <c:if test="${not empty bestMatchCourse.hotLineNo}">
                          <% hotlinelogging = "";%>
                          <c:set var="hotline1" value="${bestMatchCourse.hotLineNo}"/>
                            <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">        
                                <% hotlinelogging =  "javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','"+pageContext.getAttribute("collegeId")+"','"+pageContext.getAttribute("hotline1")+" ')";%>
                                <c:set var="hotlinelogging" value="<%=hotlinelogging%>"/>
                              </c:if>
                            <a class="clr-call mr17" onclick="setTimeout(function(){location.href='tel:<c:out value="${bestMatchCourse.hotLineNo}"/>'},1000); GAInteractionEventTracking('Webclick', 'interaction', 'hotline', '${gaCollegeName}');cpeHotlineClearing(this,'${collegeId}','<c:out value="${bestMatchCourse.subOrderItemId}"/>','<c:out value="${bestMatchCourse.cpeQualificationNetworkId}"/>','<c:out value="${bestMatchCourse.hotLineNo}"/>');${hotlinelogging};"><i class="fa fa-phone"></i><c:out value="${bestMatchCourse.hotLineNo}"/></a>
                          </c:if>
                        </div>
                     </c:if> 
                      </c:when>
                      <c:otherwise>
                    <c:if test="${not empty bestMatchCourse.subOrderEmailWebform}">
                     <div class="sremail"><%--Changed event action webclick to webform by Sangeeth.S for July_3_18 rel--%>
                      <a rel="nofollow" 
                         target="_blank"
                         class="req-inf"
                         onclick="sponsoredListGALogging(${collegeId}); 
                                  GAInteractionEventTracking('emailwebform', 'interaction', 'email webform', '${gaCollegeName}', <c:out value="${bestMatchCourse.webformPrice}"/>); cpeEmailWebformClick(this,'${collegeId}','<c:out value="${bestMatchCourse.subOrderItemId}"/>','<c:out value="${bestMatchCourse.cpeQualificationNetworkId}"/>','<c:out value="${bestMatchCourse.subOrderEmailWebform}"/>','${searchResult.sponsoredListFlag}');
                                  gtmproductClick(${i.index}, '', ${collegeId});var a='s.tl(';" 
                         href="${bestMatchCourse.subOrderEmailWebform}"
                         title="Email <c:out value="${searchResult.collegeNameDisplay}"/>">
                         Request info<i class="fa fa-caret-right"></i>
                      </a>
                     </div> 
                     </c:if>
                     <c:if test="${not empty bestMatchCourse.subOrderEmail}">
                   <c:if test="${empty bestMatchCourse.subOrderEmailWebform}">
                   <c:set var="sponsoredOrderItem" value="${searchResult.sponsoredListFlag  eq 'SPONSORED_LISTING' ? sponsoredOrderItemId : 0}"></c:set>
                       <div class="sremail">
                        <a rel="nofollow"
                           class="req-inf"
                           onclick="sponsoredListGALogging(${collegeId});
                                    GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '${gaCollegeName}');adRollLoggingRequestInfo('${collegeId}',this.href);
                                    gtmproductClick(${i.index}, '', ${collegeId});return false;"                
                            href="<SEO:SEOURL pageTitle="sendcollegemail" ><c:out value="${searchResult.collegeName}"/>#<c:out value="${searchResult.collegeId}"/>#<c:out value="${bestMatchCourse.courseId}"/>#${keywordorsubjectemail}#n#<c:out value="${bestMatchCourse.subOrderItemId}"/></SEO:SEOURL>?sponsoredOrderItemId=${sponsoredOrderItem}"
                           title="Email <c:out value="${searchResult.collegeNameDisplay}"/>" >
                           Request info<i class="fa fa-caret-right"></i>
                        </a><%--13-JAN-2015  Added for adroll marketing--%>   
                        </div>
                    </c:if>
                     </c:if>
                     <c:if test="${not empty bestMatchCourse.subOrderProspectusWebform}">
                      <div class="srprospectus">
                      <a rel="nofollow" 
                         target="_blank"
                         class="get-pros" 
                         onclick="sponsoredListGALogging(${collegeId});
                                  GAInteractionEventTracking('prospectuswebform', 'interaction', 'prospectus webform', '${gaCollegeName}', <c:out value="${bestMatchCourse.webformPrice}"/>); cpeProspectusWebformClick(this,'${collegeId}','<c:out value="${bestMatchCourse.subOrderItemId}"/>','<c:out value="${bestMatchCourse.cpeQualificationNetworkId}"/>','<c:out value="${bestMatchCourse.subOrderProspectusWebform}"/>','${searchResult.sponsoredListFlag}');
                                  gtmproductClick(${i.index}, '', ${collegeId});var a='s.tl(';" 
                         href="${bestMatchCourse.subOrderProspectusWebform}" 
                         title="Get <c:out value="${searchResult.collegeNameDisplay}"/> Prospectus">
                         Get prospectus<i class="fa fa-caret-right"></i></a>
                      </div>   
                     </c:if>
                     <c:if test="${not empty bestMatchCourse.subOrderProspectus}">
                      <c:if test="${empty bestMatchCourse.subOrderProspectusWebform}">
                       <c:set var="sponsoredOrderItem" value="${searchResult.sponsoredListFlag  eq 'SPONSORED_LISTING' ? sponsoredOrderItemId : 0}"></c:set>                      
                       <div class="srprospectus">
                       <a onclick="sponsoredListGALogging(${collegeId});
                                   GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '${gaCollegeName}'); return prospectusRedirect('${CONTEXT_PATH}','${collegeId}','${courseId2}','<c:out value="${requestScope.keywordOrSubject}"/>','','<c:out value="${bestMatchCourse.subOrderItemId}"/>','','','${sponsoredOrderItem }');
                                   gtmproductClick(${i.index}, '', ${collegeId});"
                          class="get-pros" title="Get <c:out value="${searchResult.collegeNameDisplay}"/> Prospectus" >
                          Get prospectus<i class="fa fa-caret-right"></i></a>
                       </div>   
                      </c:if>
                     </c:if>
                     <c:if test="${not empty bestMatchCourse.subOrderWebsite}">
                      <div class="srwebsite">
                     <a rel="nofollow" 
                         target="_blank" 
                         class="visit-web" 
                         id="${collegeId}"
                         onclick="sponsoredListGALogging(${collegeId});
                                  GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', <c:out value="${bestMatchCourse.websitePrice}"/>); cpeWebClickWithCourse(this,'${collegeId}','${courseId2}','${bestMatchCourse.subOrderItemId}','<c:out value="${bestMatchCourse.cpeQualificationNetworkId}"/>','<c:out value="${bestMatchCourse.subOrderWebsite}"/>','${searchResult.sponsoredListFlag}');addBasket('${courseId2}', 'O', 'visitweb','basket_div_${courseId2}', 'basket_pop_div_${courseId2}', '', '', '${gaCollegeName}');
                                  gtmproductClick(${i.index}, '', ${collegeId});var a='s.tl(';" 
                         href="${bestMatchCourse.subOrderWebsite}&courseid=${courseId2}"
                         title="<c:out value="${searchResult.collegeNameDisplay}"/> website">
                         Visit website<i class="fa fa-caret-right"></i></a>
                      </div>   
                     </c:if>
                     <c:if test="${not empty searchResult.totalOpendays}">
                       <c:if test="${searchResult.totalOpendays gt 0}">
                      <c:if test="${not empty searchResult.opendaySuborderItemId}">
                        <c:if test="${searchResult.totalOpendays eq 1}">
                           <div class="sropendays">
                            <a rel="nofollow"
                                class="req-inf sr_opd opd_grn"
                                onclick="sponsoredListGALogging(${collegeId});
                                         GAInteractionEventTracking('visitwebsite', 'interaction', 'Reserve A Place', '${gaCollegeName}');
                                         addOpenDaysForReservePlace('<c:out value="${searchResult.openDate}"/>', '<c:out value="${searchResult.openMonthYear}"/>', '<c:out value="${searchResult.collegeId}"/>', '<c:out value="${searchResult.opendaySuborderItemId}"/>', '<c:out value="${searchResult.networkId}"/>'); 
                                         cpeODReservePlace(this,'<c:out value="${searchResult.collegeId}"/>','<c:out value="${searchResult.opendaySuborderItemId}"/>','<c:out value="${searchResult.networkId}"/>','<c:out value="${searchResult.bookingUrl}"/>','${searchResult.sponsoredListFlag}');
                                         gtmproductClick(${i.index}, '', ${collegeId});
                                         GANewAnalyticsEventsLogging('open-days-type', '${searchResult.eventCategoryNameGa}', '${gaCollegeName}');
                                         adBlockerEnable('${searchResult.bookingUrl}');" 
                                href="javascript:void(0);"
                                title="<spring:message code="book.open.day.button.text"/>" >
                                <spring:message code="book.open.day.button.text"/><i class="fa fa-caret-right"></i>
                             </a>   
                           </div>
                       </c:if>
                       <c:if test="${searchResult.totalOpendays ne 1}">
                         <div class="sropendays">
                           <a rel="nofollow"
                              class="req-inf sr_opd opd_grn"
                              <%-- onclick="sponsoredListGALogging(${collegeId});
                                       GAInteractionEventTracking('visitwebsite', 'engagement', 'Book Open Day Request', '${gaCollegeName}');showOpendayPopup('${collegeId}', '');
                                       gtmproductClick(${i.index}, '', ${collegeId});"  --%>
                              href="<%=seoUrl.constructOpendaysSeoUrl((String)pageContext.getAttribute("collegeName"), (String)pageContext.getAttribute("collegeId"))%>"
                              title="<spring:message code="book.open.day.button.text"/>" >
                              <spring:message code="book.open.day.button.text"/><i class="fa fa-caret-right"></i>
                             </a>   
                           </div>
                       </c:if>
                       </c:if>
                   </c:if>
                     </c:if>   
                     </c:otherwise>
                      </c:choose>
                   </c:if>
                   </c:if>       
              </div> 
              </div>
              <!--End-->
           </div>
           <%--Added by Prabha for showing match on section in SR page by Prabha on 13_Dec_2017--%>
           <c:if test="${not empty bestMatchCourse.matchPercentage}">
             <div class="mch_on" id="mch_perc_${i.index}">
               <div class="mch_cls"><a href="#"><i class="fa fa-times"></i></a></div>
               <div class="mch_pro"><h2><c:out value="${searchResult.collegeNameDisplay}"/></h2><h3><c:out value="${bestMatchCourse.courseTitle}"/></h3></div>
               <div class="mch_sco"><h2><c:out value="${bestMatchCourse.matchPercentage}"/>%</h2><h3>match</h3></div>
               <div class="mch_cls"><a href="#"><i class="fa fa-times"></i></a></div>
               <a href="#" class="mch_opn" id="mch_opn2">
                 <em id="em_${i.index}">show</em>
                 <ul id="matchId_${i.index}">
                   <li>Matches on:</li>
                   <c:if test="${not empty bestMatchCourse.qualificationMatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.qualificationMatchFlag eq 'Y'}">  
                         <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOnImg}" alt="Study Level" class="mch_s">
                           </c:when>
                           <c:otherwise>
                             <img src="${matchOnImg}" alt="Study Level" class="mch_s">
                           </c:otherwise>
                           </c:choose>
                         </c:if>
                         <c:if test="${bestMatchCourse.qualificationMatchFlag eq 'N'}">
                           <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOffImg}" alt="Study Level" class="mch_no">
                           </c:when>
                           <c:otherwise>
                             <img src="${matchOffImg}" alt="Study Level" class="mch_no">
                           </c:otherwise>
                           </c:choose>
                         </c:if>
                       </span>
                       Study Level
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.subjectMatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.subjectMatchFlag eq 'Y'}">
                           <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOnImg}" alt="Entry Requirements" class="mch_s">
                           </c:when>
                           <c:otherwise>
                             <img src="${matchOnImg}" alt="Entry Requirements" class="mch_s">
                           </c:otherwise>
                           </c:choose>
                         </c:if>
                         <c:if test="${bestMatchCourse.subjectMatchFlag eq 'N'}">
                           <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOffImg}" alt="Entry Requirements" class="mch_no">
                           </c:when>
                           <c:otherwise>
                             <img src="${matchOffImg}" alt="Entry Requirements" class="mch_no">
                          </c:otherwise>
                          </c:choose>
                         </c:if>
                       </span>
                       Subject
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.entryLevelMatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.entryLevelMatchFlag eq 'Y'}">
                          <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOnImg}" alt="Entry Requirements" class="mch_s">
                          </c:when>
                          <c:otherwise>
                             <img src="${matchOnImg}" alt="Entry Requirements" class="mch_s">
                          </c:otherwise>
                          </c:choose>
                         </c:if>
                         <c:if test="${bestMatchCourse.entryLevelMatchFlag eq 'N'}">
                           <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOffImg}" alt="Entry Requirements" class="mch_no">
                          </c:when>
                          <c:otherwise>
                             <img src="${matchOffImg}" alt="Entry Requirements" class="mch_no">
                          </c:otherwise>
                          </c:choose>
                         </c:if>
                       </span>
                       Entry Requirements
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.assessedMatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.assessedMatchFlag eq 'Y'}">
                             <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOnImg}" alt="assessed" class="mch_s">
                          </c:when>
                          <c:otherwise>
                             <img src="${matchOnImg}" alt="assessed" class="mch_s">
                         </c:otherwise>
                         </c:choose>
                         </c:if>
                         <c:if test="${bestMatchCourse.assessedMatchFlag eq 'N'}">
                           <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOffImg}" alt="assessed" class="mch_no">
                          </c:when>
                          <c:otherwise>
                              <img src="${matchOffImg}" alt="assessed" class="mch_no">
                           </c:otherwise>
                           </c:choose>
                         </c:if>
                       </span>
                       How you're assessed
                     </li>
                   </c:if>
                    <c:if test="${not empty bestMatchCourse.locationTypeMatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.locationTypeMatchFlag eq 'Y'}">
                           <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOnImg}" alt="Location Type" class="mch_s">
                             </c:when>
                          <c:otherwise>
                             <img src="${matchOnImg}" alt="Location Type" class="mch_s">
                           </c:otherwise>
                           </c:choose>
                         </c:if>
                           <c:if test="${bestMatchCourse.locationTypeMatchFlag eq 'N'}">
                           <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOffImg}" alt="Location Type" class="mch_no">
                             </c:when>
                             <c:otherwise>
                         
                             <img src="${matchOffImg}" alt="Location Type" class="mch_no">
                             </c:otherwise>
                         </c:choose>
                         </c:if>
                       </span>
                       Location Type
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.studyModeMatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.studyModeMatchFlag eq 'Y'}">
                           <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOnImg}" alt="Study Mode" class="mch_s">
                             </c:when>
                           <c:otherwise>
                             <img src="${matchOnImg}" alt="Study Mode" class="mch_s">
                          </c:otherwise>
                          </c:choose>
                         </c:if>
                         <c:if test="${bestMatchCourse.studyModeMatchFlag eq 'N'}">
                            <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOffImg}" alt="Study Mode" class="mch_no">
                             </c:when>
                           <c:otherwise>
                             <img src="${matchOffImg}" alt="Study Mode" class="mch_no">
                           </c:otherwise>
                           </c:choose>
                         </c:if>
                       </span>
                       Study Mode
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.locationMatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.locationMatchFlag eq 'Y'}">
                           <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOnImg}" alt="Location" class="mch_s">
                             </c:when>
                          <c:otherwise>
                             <img src="${matchOnImg}" alt="Location" class="mch_s">
                           </c:otherwise>
                           </c:choose>
                         </c:if>
                         <c:if test="${bestMatchCourse.locationMatchFlag eq 'N'}">
                            <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOffImg}" alt="Location" class="mch_no">
                         </c:when>
                         <c:otherwise>
                             <img src="${matchOffImg}" alt="Location" class="mch_no">
                           </c:otherwise>
                           </c:choose>
                         </c:if>
                       </span>
                       Location
                     </li>
                   </c:if>
                     <c:if test="${not empty bestMatchCourse.reviewSub1MatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.reviewSub1MatchFlag eq 'Y'}">
                              <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOnImg}" alt="review cat 1" class="mch_s">
                           </c:when>
                           <c:otherwise>
                             <img src="${matchOnImg}" alt="review cat 1" class="mch_s">
                          </c:otherwise>
                          </c:choose>
                         </c:if>
                          <c:if test="${bestMatchCourse.reviewSub1MatchFlag eq 'N'}">
                            <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOffImg}" alt="review cat 1" class="mch_no">
                          </c:when>
                          <c:otherwise>
                             <img src="${matchOffImg}" alt="review cat 1" class="mch_no">
                          </c:otherwise>
                          </c:choose>
                         </c:if>
                       </span>
                        <c:if test="${not empty requestScope.reviewCatStr_1}">${requestScope.reviewCatStr_1}</c:if>
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.reviewSub2MatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.reviewSub2MatchFlag eq 'Y'}">
                            <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOnImg}" alt="review cat 2" class="mch_s">
                             </c:when>
                          <c:otherwise>
                             <img src="${matchOnImg}" alt="review cat 2" class="mch_s">
                          </c:otherwise>
                          </c:choose>
                         </c:if>
                         <c:if test="${bestMatchCourse.reviewSub2MatchFlag eq 'N'}">      
                         <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOffImg}" alt="review cat 2" class="mch_no">
                           </c:when>
                           <c:otherwise>
                             <img src="${matchOffImg}" alt="review cat 2" class="mch_no">
                          </c:otherwise>
                          </c:choose>
                         </c:if>
                       </span>
                       <c:if test="${not empty requestScope.reviewCatStr_2}">${requestScope.reviewCatStr_2}</c:if>
                     </li>
                   </c:if>
                   <c:if test="${not empty bestMatchCourse.reviewSub3MatchFlag}">
                     <li>
                       <span class="mch_icn">
                       <c:if test="${bestMatchCourse.reviewSub3MatchFlag eq 'Y'}">
                              <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOnImg}" alt="review cat 3" class="mch_s">
                           </c:when>
                           <c:otherwise>
                             <img src="${matchOnImg}" alt="review cat 3" class="mch_s">
                          </c:otherwise>
                          </c:choose>
                         </c:if>
                         <c:if test="${bestMatchCourse.reviewSub3MatchFlag eq 'N'}">
                          
                            <c:choose>
           <c:when test="${(i.index) >= 1}">
                             <img src="${onePxImg}" data-src="${matchOffImg}" alt="review cat 3" class="mch_no">
                             </c:when>
                          <c:otherwise>
                             <img src="${matchOffImg}" alt="review cat 3" class="mch_no">
                             </c:otherwise>
                          </c:choose>
                         </c:if>
                       </span>
                       <c:if test="${not empty requestScope.reviewCatStr_3}">${requestScope.reviewCatStr_3}</c:if>
                     </li>
                   </c:if>
                 </ul>
               </a>
             </div>
          
           </c:if>   
           <%--End of match on code--%>
           <c:if test="${searchResult.sponsoredListFlag  eq 'SPONSORED_LISTING'}">
               <input type="hidden" id="collegeIdSponsored" value="${searchResult.collegeId}"/>
               <input type="hidden" id="collegeNameSponsored" value="${searchResult.collegeName}"/> 
               <input type="hidden" id="orderItemIdSponsored" value="${sponsoredOrderItemId}"/>         
           </c:if>
          </div>
        </c:forEach>
         
         <%--03_NOV_2015_REL Added for responsive redesign Indumathi.S--%>
         <c:if test="${not empty searchResult.numberOfCoursesForThisCollege}">
        <c:if test="${searchResult.numberOfCoursesForThisCollege gt 1}">
           <c:choose>
           <c:when test="${searchPageType eq 'CLEARING'}">
            <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">     
               <a class="addcou" rel="nofollow" onclick="javascript:advertiserExternalUrlStatsLogging('clearingCategorySponsership','<c:out value="${searchResult.collegeId}"/>','<c:out value="${searchResult.collegePRUrl}"/>')" href="<c:out value="${searchResult.collegePRUrl}"/>">
                <span class="ac_txtlnk">
                  View ${searchResult.additionalCourses} additional <c:if test="${searchResult.additionalCourses eq 1}">course</c:if> <c:if test="${searchResult.additionalCourses gt 1 }">courses</c:if> available
                  <i class="fa fa-angle-right"></i>
                </span>
               </a>    
             </c:if>
             <c:if test="${searchResult.isSponsoredCollege ne 'Y'}">
               <a class="addcou" rel="nofollow" href="<c:out value="${searchResult.collegePRUrl}"/>"> 
                <span class="ac_txtlnk">
                  View ${searchResult.additionalCourses} additional <c:if test="${searchResult.additionalCourses eq 1}">course</c:if> <c:if test="${searchResult.additionalCourses gt 1 }">courses</c:if> available
                  <i class="fa fa-angle-right"></i>
                </span>
               </a>
             </c:if>
             </c:when>
             <c:otherwise>
               <a class="addcou" rel="nofollow" href="${searchResult.collegePRUrl}" onclick="sponsoredListGALogging(${collegeId});gtmproductClick(${i.index}, 'sr_to_pr_courses_available', ${collegeId});">
                <span class="ac_txtlnk">
                  View ${searchResult.additionalCourses} additional <c:if test="${searchResult.additionalCourses eq 1}">course</c:if> <c:if test="${searchResult.additionalCourses gt 1 }">courses</c:if> available
                  <i class="fa fa-angle-right"></i>
                </span>
               </a>
             </c:otherwise>
             </c:choose>
          </c:if>
        </c:if>    
       </c:if>
     </div>
   </div>
   <c:if test="${searchResult.isSponsoredCollege eq 'Y'}">
    </div>
   </c:if>
   <c:set var="position" value="${i.index}"/>
   <jsp:include page="/jsp/search/include/searchDrawAdSlots.jsp">
     <jsp:param name="fromPage" value="searchresults"/>
     <jsp:param name="position" value="${position}"/>
     <jsp:param name="totalResults" value="${requestScope.universityCount}"/>
   </jsp:include>
 </c:forEach>
 <input type="hidden" id="srcEvent"/>
 <input type="hidden" id="currentcompare" value=""/> 
 <c:if test="${searchPageType eq 'CLEARING'}">
 <c:set var="clrText" value="clearing-"/>
  <input type="hidden" name="comparepage" id="comparepage" value="CLEARING"/>    
 </c:if>
    
    
    
    
 
    
</c:if>
<input type="hidden" name="isclearingPage" id="isclearingPage" value="${requestScope.searchClearing}"/>
<script type="text/javascript">  
var opdup = jQuery.noConflict();
opdup(window).scroll(function(){ // scroll event 
    lazyloadetStarts();
});
</script>
<jsp:include page="/search/include/includeGradelb.jsp"/>