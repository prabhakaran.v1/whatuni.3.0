<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil"%>
<%
  String urlData_2 = request.getParameter("URLDATA_2");
  int indexOfUrl = urlData_2.indexOf("page");  
  String modDefaultText = "e.g. Module name";
  String moduleDefaultText = "e.g. Module name";  
  String q = GenericValidator.isBlankOrNull(request.getParameter("q")) ? "" : (request.getParameter("q"));
  String subject = GenericValidator.isBlankOrNull(request.getParameter("subject")) ? "" : (request.getParameter("subject"));  
  String university = GenericValidator.isBlankOrNull(request.getParameter("university")) ? "" : (request.getParameter("university"));
  String module = GenericValidator.isBlankOrNull(request.getParameter("module")) ? "" : (request.getParameter("module"));
  String locType = GenericValidator.isBlankOrNull(request.getParameter("location-type")) ? "" : (request.getParameter("location-type"));
  String location = GenericValidator.isBlankOrNull(request.getParameter("location")) ? "" : (request.getParameter("location"));
  String postCode = GenericValidator.isBlankOrNull(request.getParameter("postcode")) ? "" : (request.getParameter("postcode"));
  String distance = GenericValidator.isBlankOrNull(request.getParameter("distance")) ? "10" : (request.getParameter("distance"));
  String campusType = GenericValidator.isBlankOrNull(request.getParameter("campus-type")) ? "" : (request.getParameter("campus-type"));  
  String studyMode = GenericValidator.isBlankOrNull(request.getParameter("study-mode")) ? "" : (request.getParameter("study-mode"));
  String assessmentType = GenericValidator.isBlankOrNull(request.getParameter("assessment-type")) ? "" : (request.getParameter("assessment-type"));
  String reviewCategoryType = GenericValidator.isBlankOrNull(request.getParameter("your-pref")) ? "" : (request.getParameter("your-pref"));
  String empRateMax = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-max")) ? "100" : (request.getParameter("employment-rate-max"));
  String empRateMin = GenericValidator.isBlankOrNull(request.getParameter("employment-rate-min")) ? "" : (request.getParameter("employment-rate-min"));
  String ucasTarrifMax = GenericValidator.isBlankOrNull(request.getParameter("ucas-points-max")) ? "" : (request.getParameter("ucas-points-max"));
  String ucasTarrifMin = GenericValidator.isBlankOrNull(request.getParameter("ucas-points-min")) ? "" : (request.getParameter("ucas-points-min"));
  String russellGroup = GenericValidator.isBlankOrNull(request.getParameter("russell-group")) ? "" : (request.getParameter("russell-group"));
  String entryLevel = GenericValidator.isBlankOrNull(request.getParameter("entry-level"))? "" : (request.getParameter("entry-level"));
  String entryPoints = GenericValidator.isBlankOrNull(request.getParameter("entry-points"))? "" : (request.getParameter("entry-points"));
  String pageno = GenericValidator.isBlankOrNull(request.getParameter("pageno"))? "" : (request.getParameter("pageno"));
  String sortorder = GenericValidator.isBlankOrNull(request.getParameter("sort"))? "" : (request.getParameter("sort"));
  String ucasCode = !GenericValidator.isBlankOrNull(request.getParameter("ucas-code")) ? (request.getParameter("ucas-code")) : "";
  String rf = !GenericValidator.isBlankOrNull(request.getParameter("rf")) ? (request.getParameter("rf")) : "";
  String scoreValue = !GenericValidator.isBlankOrNull(request.getParameter("score")) ? request.getParameter("score") : "";
  String awardsURL = "/student-awards-winners/university-of-the-year/";
  String pageFrom = request.getParameter("PAGE_FROM");
  String jacs = request.getParameter("jacs");
        jacs = !GenericValidator.isBlankOrNull(jacs)? jacs: "";
  
  String sortBy = (String)request.getAttribute("sortByValue");
         sortBy = !GenericValidator.isBlankOrNull("sortBy")? sortBy :"";
  String defaultModuleTitle = (String)request.getAttribute("defaultModuleTitle");
         modDefaultText = !GenericValidator.isBlankOrNull(defaultModuleTitle)? defaultModuleTitle: modDefaultText;
  String disbledText = !GenericValidator.isBlankOrNull(defaultModuleTitle)? "":"disabled";          
  String qString = (String)request.getAttribute("queryStr");
         qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  String noFollowLink = (!GenericValidator.isBlankOrNull(qString)) ? "nofollow" : "";  
  String subjectCode = (String)request.getAttribute("subjectCode");
         subjectCode = !GenericValidator.isBlankOrNull(subjectCode)? subjectCode: "";
  String campusCode = (String)request.getAttribute("campusCode");
         campusCode = !GenericValidator.isBlankOrNull(campusCode)? campusCode: "";
  String studyModeId = (String)request.getAttribute("paramstudyModeValue");
         studyModeId = !GenericValidator.isBlankOrNull(studyModeId)? studyModeId :"All study modes";
  String assessmentId = request.getParameter("assessment-type");
         assessmentId = !GenericValidator.isBlankOrNull(assessmentId)? assessmentId: "";
  String studyLevel = (String)request.getAttribute("paramStudyLevelId");       
         studyLevel = !GenericValidator.isBlankOrNull(studyLevel)? studyLevel :"";
  String resultExists = (String)request.getAttribute("resultExists");       
         resultExists = !GenericValidator.isBlankOrNull(resultExists)? resultExists :"";
  String searchCount = (String)request.getAttribute("universityCount");       
         searchCount = !GenericValidator.isBlankOrNull(searchCount)? searchCount :"";       
  String russellgrpId = (String)request.getAttribute("russellgrpId");
         russellgrpId = !GenericValidator.isBlankOrNull(russellgrpId)? russellgrpId: "";         
  String searchLocation = (String)request.getAttribute("searchLoc");         
         searchLocation = !GenericValidator.isBlankOrNull(searchLocation) && ("england").equalsIgnoreCase(searchLocation) ? "england" : searchLocation;         
         searchLocation = !GenericValidator.isBlankOrNull(searchLocation) && ("united-kingdom").equalsIgnoreCase(searchLocation) ? "All locations" : searchLocation;  
  String removeModuleParameter = (String)request.getAttribute("removeModuleParameter");  
        if(!GenericValidator.isBlankOrNull(removeModuleParameter) && ("YES").equals(removeModuleParameter)){
          module = "";
        }
  String removePostcodeParameter = (String)request.getAttribute("removePostcodeParameter");  
        if(!GenericValidator.isBlankOrNull(removePostcodeParameter) && ("YES").equals(removePostcodeParameter)){
          postCode = ""; 
          distance = "";
        }
 String searchPageType = request.getParameter("clearing");
        searchPageType = !GenericValidator.isBlankOrNull(searchPageType)?searchPageType:"";
 String searchpagelogtype = "";
        if("CLEARING".equalsIgnoreCase(searchPageType)){searchpagelogtype = "clearing-";}       
%>
<div class="fltr">
 <%--Added close tag for filter in responsive 03_NOV_2015 By Indumathi S--%>
 <div class="filt_cls">
   <a href="#"><span>CLOSE</span><i class="fa fa-close"></i></a>
 </div> 
  <div class="fltr_in">
    <h2>Filter by:</h2>
       <jsp:include page="/jsp/search/include/deleteFilter.jsp">   
         <jsp:param name="pageFrom" value="<%=pageFrom%>"/>
         <jsp:param name="searchType" value="<%=searchPageType%>"/>
       </jsp:include>
    <div class="sh_hd">
        <div class="hdn" onclick="javascript:toggleMenu('univDiv','univh1');">
            <h4 id="univh1" class="rdat actct">University<em>show</em></h4>
        </div>
        <div id="univDiv" class="flt_lt" style="display:block;">
          <form name="uniNameForm" id="uniNameForm" action="" method="post" onsubmit="return uniSearchInResults1(document.getElementById('uniName'), <%=indexOfUrl%>);">
              <input type="text" name="uniName" id="uniName" value="Enter uni name" autocomplete="off"  onkeydown="clearUniNAME(event,this)"  
              onkeyup="autoCompleteUniForSearchResults(event,this)"   
              onclick="clearUniDefaultTEXT(this)" 
              onblur="setUniDefaultTEXT(this)" 
              class="txt_box nw"/> 
              <input type="hidden" id="uniName_hidden" value="" />
              <input type="hidden" id="uniName_id" value="" />
              <input type="hidden" id="uniName_display" value="" />
              <input type="hidden" id="uniName_url" value="" />
              <input type="hidden" id="uniName_alias" value="" /> 
              <input type="button" name="submit" class="go_ic" onclick="javascript:return uniSearchInResults1(document.getElementById('uniName'), <%=indexOfUrl%>)"/>
          </form>
        </div>
     </div>
     <div class="sh_hd">
       <div class="hdn" onclick="javascript:toggleMenu('modules','modulesh1');">
         <h4 id="modulesh1" class="rdat actct">Modules<em>show</em></h4>
       </div>
       <div id="modules" class="flt_lt" style="display: block;">
           <input type="text" name="moduleText" id="moduleText" value="<%=modDefaultText%>" maxlength="100" autocomplete="off"  onkeydown="clearDefaultText1(this, '<%=modDefaultText%>');enableButton('moduleButton', this, '<%=moduleDefaultText%>');"
              onclick="clearDefaultText1(this,'<%=modDefaultText%>');enableButton('moduleButton',this, '<%=moduleDefaultText%>');" 
              onblur="setDefaultText1(this,'<%=modDefaultText%>');enableButton('moduleButton',this, '<%=moduleDefaultText%>');" 
              onkeypress="clearDefaultText1(this, '<%=modDefaultText%>');enableButton('moduleButton', this, '<%=moduleDefaultText%>');javascript:if(event.keyCode==13){return searchResultsURL('modules');}"
              onkeyup = "setDefaultText1(this,'<%=modDefaultText%>');enableButton('moduleButton',this, '<%=moduleDefaultText%>');" 
              class="txt_box nw"/> 
           <input type="button" name="submit" id="moduleButton" disabled="<%=disbledText%>" class="go_ic" onclick="javascript:return searchResultsURL('modules')"/>
       </div>     
     </div>    
     <!--Subject-->
     <c:if test="${not empty subjectList}">
       <div class="sh_hd">
          <div class="hdn" onclick="javascript:toggleMenu('subject','subjecth1');">
            <h4 id="subjecth1" class="rdat actct">Subject <em>show</em></h4>
          </div>
          <div id="subject" class="flt_lt" style="display: block;">
            <ul class="sub_lik num">
<c:forEach var="subjectList" items="${requestScope.subjectList}">
            <c:if test="${subjectList.subjectTextKey eq subjectCode}">
                   <li class="stud fl-act"><span class="act_bg"></span><span class="sbj-act"><c:out value="${subjectList.categoryDesc}"/></span><span class="fr">(<c:out value="${subjectList.collegeCount}"/>)</span></li>
                 </c:if>
                  <c:if test="${subjectList.subjectTextKey ne subjectCode}">
               <c:set var="subjectDesc" value="${subjectList.categoryDesc}"/>             
                    <li><%if (pageFrom != null && pageFrom.equals("BROWSE_MONEY_PAGE")){%><a onclick="searchEventTracking('filter','<%=searchpagelogtype%>subject','${subjectDesc.toLowerCase()}');" href="${subjectList.browseMoneyPageUrl}">${subjectList.categoryDesc}</a><%}else if(pageFrom != null && pageFrom.equals("KEWORD_MONEY_PAGE")){%><a onclick="searchEventTracking('filter','<%=searchpagelogtype%>subject','${subjectDesc}');" href="${subjectList.browseMoneyPageUrl}">${subjectList.categoryDesc}</a><%}%><span class="shde">(${subjectList.collegeCount})</span></li>
                 </c:if>                
             </c:forEach>
            </ul>
          </div>
       </div>
     </c:if> 
     <c:if test="${not empty locationRefineList}">
      <div class="sh_hd">
          <div class="hdn" onclick="javascript:toggleMenu('location','locationh1');">
            <h4 id="locationh1" class="rdat actct">Location <em>show</em></h4>
          </div>
          <div id="location" class="flt_lt" style="display: block;">
             <%String classnamee = "";%>
             <ul class="sub_lik loc"> 
             <c:forEach var="locationRefineList" items="${requestScope.locationRefineList}"> 
             <c:if test="${not empty  locationRefineList.regionName}">
               <c:if test="${not empty  locationRefineList.topRegionName}">
               <c:set var="topRegion" value="${locationRefineList.topRegionName}"/>  
               <c:set var="searchLocation" value="<%=searchLocation%>"/>    
                  <c:if test="${locationRefineList.locationTextKey eq searchLocation}">                         
                    <li class="stud fl-act">
                      <span class="act_bg"></span>
                      <c:if test="${locationRefineList.regionName eq topRegion}">
                          <span class="lcn-act1">${locationRefineList.regionName}</span>
                        </c:if>
                        <c:if test="${locationRefineList.regionName ne topRegion}">
                          <span class="lcn-act">${locationRefineList.regionName}</span>
                        </c:if>                    
                    </li>
                   </c:if>
                   <c:if test="${locationRefineList.locationTextKey ne searchLocation}">
                        <c:set var="locDesc" value="${locationRefineList.regionName}"/>           
                    <li>
                     <c:if test="${locationRefineList.regionName eq topRegion}">
                         <% if (pageFrom != null && pageFrom.equals("BROWSE_MONEY_PAGE")){  %>
                        <a onclick="searchEventTracking('filter','<%=searchpagelogtype%>location','${locDesc.toLowerCase()}');" href="${locationRefineList.locationURL}">${locationRefineList.regionName}</a>
                      <%}else if(pageFrom != null && pageFrom.equals("KEWORD_MONEY_PAGE")){%>
                         <a onclick="searchEventTracking('filter','<%=searchpagelogtype%>location','${locDesc.toLowerCase()}');" href="${locationRefineList.locationURL}">${locationRefineList.regionName}</a>
                      <%}%>
                     </c:if>
                    <c:if test="${locationRefineList.regionName ne topRegion}">
                        <% if (pageFrom != null && pageFrom.equals("BROWSE_MONEY_PAGE")){  %>
                          <a rel="<%=noFollowLink%>" onclick="searchEventTracking('filter','<%=searchpagelogtype%>location','${locDesc.toLowerCase()}');" href="${locationRefineList.locationURL}"><span class="lcn">${locationRefineList.regionName}</span></a>
                        <%}else if(pageFrom != null && pageFrom.equals("KEWORD_MONEY_PAGE")){%>
                           <a rel="nofollow" onclick="searchEventTracking('filter','<%=searchpagelogtype%>location','${locDesc.toLowerCase()}');" href="${locationRefineList.locationURL}"><span class="lcn">${locationRefineList.regionName}</span></a>
                        <%}%>
                      </c:if>
                    </li>
                  
                   </c:if>
                   </c:if>
                 </c:if>
              </c:forEach> 
            </ul>
          </div>
      </div>      
    </c:if>  

    <div class="sh_hd">
        <div class="hdn" onclick="javascript:toggleMenu('postcode','postcodeh1');">
          <h4 id="postcodeh1" class="rdat">Postcode <em>show</em></h4>
        </div>    
        <div id="postcode" class="slist perf_ch" style="display:none;">
          <div class="slider_lst nw">
             <div id="s1" class="flt_lt srch_dis" style="display:block;">              
              <input id="postCodeText" class="txt_box wd" type="text" onkeydown="clearDefaultText1(this, 'Postcode');enableButton('postcodeButton',this, 'Postcode');"
                    maxlength = "10"
                    onfocus="showButton();" 
                    onclick="clearDefaultText1(this,'Postcode');enableButton('postcodeButton',this, 'Postcode');" 
                    onblur="setDefaultText1(this,'Postcode');enableButton('postcodeButton',this, 'Postcode');hideButton();" 
                    onkeyup="setDefaultText1(this,'Postcode');enableButton('postcodeButton',this, 'Postcode');enableDropDown();"
                    onkeypress="clearDefaultText1(this, 'Postcode');enableButton('postcodeButton', this, 'Postcode');javascript:if(event.keyCode==13){return searchResultsURL('postcode');}enableDropDown();"
                    name="postcode" value="Postcode" tabindex="2" title="Postcode" />
              <input type="button" name="submit" id="postcodeButton" class="go_ic sd_icn" disabled="disabled" onclick="javascript:return searchResultsURL('postcode')"/>      
              </div>
             <div class="mls_ctl nw">
               <label class="mls1">Radius</label>               
             </div> 
             <div class="lft_srch disa_rem" id="pstDpDown">
             <div class="drp_val">
                <span class="dpval_txt" id="divPostcode">10 Miles</span>
                <i class="fa fa-angle-down fa-lg"></i>
               </div>
              <select id="dpPostcode" name="dpPostcode" disabled onchange="setSelectedValue(this,'divPostcode', 'hidden_distance');searchResultsURL('postcode');">
                    <option value="10">10 miles</option>
                    <option value="25">25 miles</option>
                    <option value="50">50 miles</option>
                    <option value="100">100 miles</option>
                    <option value="150">150 miles</option>
                 </select>
             </div>
              <%--<div class="ns_dd disa_rem" id="pstDpDown">
                <fieldset>
                  
                  <input type="text" name="qualification" id="pstCodeTxt" class="fnt_lbk sbx" value="10 miles" readonly="true" onclick="hideShowPstDpDown('pstCodeList');">
                  <ul id="pstCodeList" class="ts-option" style="display: none;">
                    <li onclick="javascript:setPstValue($$('dpdown1'));"><a href="javascript:void(0);" class="subNavDkLink" id="dpdown1" >10 miles</a></li>
                    <li onclick="javascript:setPstValue($$('dpdown2'));"><a href="javascript:void(0);" class="subNavDkLink" id="dpdown2" >25 miles</a></li>
                    <li onclick="javascript:setPstValue($$('dpdown3'));"><a href="javascript:void(0);" class="subNavDkLink" id="dpdown3" >50 miles</a></li>
                    <li onclick="javascript:setPstValue($$('dpdown4'));"><a href="javascript:void(0);" class="subNavDkLink" id="dpdown4" >100 miles</a></li>
                    <li onclick="javascript:setPstValue($$('dpdown5'));"><a href="javascript:void(0);" class="subNavDkLink" id="dpdown5" >150 miles</a></li>                
                  </ul>
                </fieldset>
              </div>--%>
             <script type="text/javascript">loadPostcodeSlider("<%=distance%>");</script>             
             <input id="hidden_distance" name="hidden_distance" type="hidden" value="<%=distance%>"/> 
          </div>       
          
        </div>
     </div>
     <c:if test="${not empty locationList}">
         <div class="sh_hd">
            <div class="hdn" onclick="javascript:toggleMenu('locationType','locationhTypeh1');">
              <h4 id="locationhTypeh1" class="rdat">Location type<em>show</em></h4>
            </div>
            <div id="locationType" class="flt_lt" style="display:none;">
               <ul class="sub_lik nw">
               <c:forEach var="locationList" items="${locationList}">  
                    <li><input type="checkbox" onclick="javascript:multicheckfn('<c:out value="${locationList.optionTextKey}"/>')" id="<c:out value="${locationList.optionTextKey}"/>"  name="<c:out value="${locationList.optionTextKey}"/>" value="<c:out value="${locationList.optionTextKey}"/>"/><label><c:out value="${locationList.refineDesc}"/></label></li>
                </c:forEach>
                </ul>
            </div>
         </div>
     </c:if>     
     <c:if test="${not empty campustypeList}">
         <div class="sh_hd">
            <div class="hdn" onclick="javascript:toggleMenu('campus','campush1');">
              <h4 id="campush1" class="rdat">Campus type<em>show</em></h4>
            </div>
            <div id="campus" class="flt_lt" style="display:none;">
               <ul class="sub_lik">
               <c:forEach var="campustypeList" items="${campustypeList}">            
                    <li>
                    <c:set var="campusCode" value="<%=campusCode%>"/>
                    <c:if test="${campustypeList.optionTextKey eq campusCode}">
                           <p class="stud fl-act">
                              <span class="act_bg"></span>
                              <c:out value="${campustypeList.refineDesc}"/>
                           </p>
                         </c:if>
                            <c:if test="${campustypeList.optionTextKey ne campusCode}">
                            <c:set var="campusDesc" value="${campustypeList.refineDesc}"/>
                             <c:set var="searchpagelogtype" value="<%=searchpagelogtype%>"/>
                           <a  rel="nofollow" onclick="searchEventTracking('filter','${searchpagelogtype}campus-type','${campusDesc.toLowerCase()}');" href="${campustypeList.browseMoneyageLocationRefineUrl}">${campustypeList.refineDesc}</a>
                        </c:if>
                    </li>
                </c:forEach>
                </ul>
            </div>
         </div>
     </c:if>
     <c:if test="${not empty qualList}">
          <div class="sh_hd">
            <div class="hdn" onclick="javascript:toggleMenu('qualList','qualListh1');">
              <h4 id="qualListh1" class="rdat actct">Study level <em>show</em></h4>
            </div>
            <div id="qualList" class="flt_lt" style="display:block;">
              <ul class="sub_lik">
              <c:forEach var="qualList" items="${qualList}">  
                   <c:if test="${qualList.refineCode eq requestScope.paramStudyLevelId}">
                      <li class="stud fl-act"><span class="act_bg"></span><c:out value="${qualList.refineDisplayDesc}"/></li>
                   </c:if>
                    <c:if test="${qualList.refineCode ne requestScope.paramStudyLevelId}">                     
                     <li><c:set var="qualDesc" value="${qualList.refineDesc}"/> <a onclick="searchEventTracking('filter','<%=searchpagelogtype%>study-level','${qualDesc.toLowerCase()}');" href="${qualList.browseMoneyageLocationRefineUrl}" >${qualList.refineDisplayDesc}</a></li>
                   </c:if>
                 </c:forEach> 
               </ul>             
             </div>
          </div>
     </c:if>
     <c:if test="${not empty studyModeList}">
         <div class="sh_hd">
            <div class="hdn" onclick="javascript:toggleMenu('studyMode','studyModeh1');">
              <h4 id="studyModeh1" class="rdat actct">Study mode <em>show</em></h4>
            </div>
            <div id="studyMode" class="flt_lt" style="display:block;">
              <ul class="sub_lik">   
                <c:forEach var="studyModeList" items="${studyModeList}">
                 <c:set var="studyModeId" value="<%=studyModeId%>"/>
                 <c:if test="${studyModeList.refineDesc eq studyModeId}">    
                    <li class="stud fl-act"><span class="act_bg"></span><c:out value="${studyModeList.refineDesc}"/></li>
                   </c:if>
                   <c:if test="${studyModeList.refineDesc ne studyModeId}">                         
                    <li><c:set var="studyModeDesc" value="${studyModeList.refineDesc}"/><a onclick="searchEventTracking('filter','<%=searchpagelogtype%>study-mode','${studyModeDesc.toLowerCase()}');" href="${studyModeList.browseMoneyageLocationRefineUrl}">${studyModeList.refineDesc}</a></li>
                  </c:if>
                </c:forEach>
              </ul>             
            </div>
         </div>
     </c:if>
     <div class="sh_hd">
        <div class="hdn" onclick="javascript:toggleMenu('empRate','empRateh1');">
          <h4 id="empRateh1" class="rdat">Employment rate <em>show</em></h4>
        </div>
        <div id="empRate" class="slist perf_ch" style="display:none;">
          <div class="slider_lst">
             <div class="mls_ctl nw1">
                <label class="mls1"></label>
                <%--<input type="text" id="pemp_rate_display" class="mls2" readonly="true"/>
                <label class="mls3">100%</label>--%>
             </div>
             <%--<div class="ns_dd">
                <fieldset>
                  <input type="text" name="qualification" id="empTxt" class="fnt_lbk sbx" value="Thiyagu" readonly="true" onclick="hideShowPstDpDown('empList');">
                  <ul id="empList" class="ts-option" style="display: none;">
                    <li onclick="javascript:setEmpValue($$('empdpdown0'));"><a href="javascript:void(0);" class="subNavDkLink" id="empdpdown0">Choose emp rate</a></li>
                    <li onclick="javascript:setEmpValue($$('empdpdown1'));"><a href="javascript:void(0);" class="subNavDkLink" id="empdpdown1" >50% and above</a></li>
                    <li onclick="javascript:setEmpValue($$('empdpdown2'));"><a href="javascript:void(0);" class="subNavDkLink" id="empdpdown2" >60% and above</a></li>
                    <li onclick="javascript:setEmpValue($$('empdpdown3'));"><a href="javascript:void(0);" class="subNavDkLink" id="empdpdown3" >70% and above</a></li>
                    <li onclick="javascript:setEmpValue($$('empdpdown4'));"><a href="javascript:void(0);" class="subNavDkLink" id="empdpdown4" >80% and above</a></li>
                    <li onclick="javascript:setEmpValue($$('empdpdown5'));"><a href="javascript:void(0);" class="subNavDkLink" id="empdpdown5" >90% and above</a></li>
                    <li onclick="javascript:setEmpValue($$('empdpdown6'));"><a href="javascript:void(0);" class="subNavDkLink" id="empdpdown6" >100%</a></li>
                  </ul>
                </fieldset>
              </div>--%>
             <div class="lft_srch">
              <div class="drp_val">
               <span class="dpval_txt" id="divEmprate">50 above</span>
               <i class="fa fa-angle-down fa-lg"></i>
              </div>
              <select id="dpEmprate" name="dpEmprate" onchange="setSelectedValue(this,'divEmprate', 'hidden_empRateMin');searchResultsURL('empRate');">              
               <option value="0">Choose % rate</option> 
               <option value="50">50% and above</option> 
               <option value="60">60% and above</option> 
               <option value="70">70% and above</option> 
               <option value="80">80% and above</option> 
               <option value="90">90% and above</option> 
               <option value="100">100%</option> 
              </select>
            </div>
            <script type="text/javascript">loadSlider("<%=empRateMin%>","<%=empRateMax%>");</script>            
          </div>
          <input type="button" name="submit" class="updt"  id="empTextBox" style="display:none;" value="Update" onclick="javascript:return searchResultsURL('empRate')"/>
        </div>
      </div>
      
      <input id="ucasTariff_hidden" type="hidden" value=""/>
      <input id="hidden_ucasMax" type="hidden" value=""/> 
      <input id="hidden_ucasMin" type="hidden" value=""/>
      <c:if test="${not empty russelList}">
           <div class="sh_hd">
              <div class="hdn" onclick="javascript:toggleMenu('russell','russellh1');">
                <h4 id="russellh1" class="rdat">Russell Group<em>show</em></h4>
              </div>
              <div id="russell" class="flt_lt" style="display:none;">
                 <ul class="sub_lik">
                 <c:forEach var="russelList" items="${russelList}">     
                      <li>
                      <c:set var="russellgrpId" value="<%=russellgrpId%>"/>
                      <c:if test="${russelList.optionTextKey eq russellgrpId}">
                           <p class="stud fl-act">
                              <span class="act_bg"></span>
                              <c:out value="${russelList.refineDesc}"/>
                           </p>
                         </c:if>
                         <c:if test="${russelList.optionTextKey ne russellgrpId}">
                         <c:set var="ruselDesc" value="${russelList.refineDesc}"/>
                       
                           <a rel="nofollow" onclick="searchEventTracking('filter','<%=searchpagelogtype%>russell','${ruselDesc.toLowerCase()}');" href="<c:out value="${russelList.browseMoneyageLocationRefineUrl}"/>"><c:out value="${russelList.refineDesc}"/></a>
                        </c:if>
                      </li>
                  </c:forEach>
                  </ul>
              </div>
           </div> 
    </c:if>      
    
    <%--Added below filters your pref, 28_Aug_2018 By Sabapathi.S--%>
    
    <%--
    <logic:present name="assesmentTypeList" scope="request">
       <logic:notEmpty name="assesmentTypeList" scope="request">
         <div class="sh_hd">
            <div class="hdn" onclick="javascript:toggleMenu('assessment','assessmenth1');">
              <h4 id="assessmenth1" class="rdat">How you're assessed <em>show</em></h4>
            </div>
            <div id="assessment" class="flt_lt" style="display:none;">
              <ul class="sub_lik">   
                <logic:iterate id="assesmentTypeList" name="assesmentTypeList" scope="request">
                    <logic:equal name="assesmentTypeList" property="urlText" value="<%=assessmentId%>">
                      <li class="stud fl-act"><span class="act_bg"></span><bean:write name="assesmentTypeList" property="assessmentDisplayName" /></li>
                    </logic:equal>
                    <logic:notEqual name="assesmentTypeList" property="urlText" value="<%=assessmentId%>"> 
                      <li><bean:define id="assessmentDesc" name="assesmentTypeList" property="assessmentDisplayName" type="java.lang.String"/><a onclick="searchEventTracking('filter','<%=searchpagelogtype%>assessment-type','<%=assessmentDesc.toLowerCase()%>');" href="<bean:write name="assesmentTypeList" property="browseMoneyAssessedRefineUrl"/>"><bean:write name="assesmentTypeList" property="assessmentDisplayName" /></a></li>
                    </logic:notEqual>
                </logic:iterate>
              </ul>             
            </div>
         </div>
        </logic:notEmpty>
     </logic:present>
     --%>
     <%--
     <c:if test="${not empty reviewCategoryList}">
         <div class="sh_hd pref_filt">
            <div class="hdn" onclick="javascript:toggleMenu('reviewCategory','reviewCategoryh1');">
              <h4 id="reviewCategoryh1" class="rdat">What's most important to you?<em>show</em></h4>
            </div>
            <div id="reviewCategory" class="flt_lt" style="display:none;">
               <div class="pref_txt">Select up to 3 of the following categories and we'll match universities that have ranked highly in those categories in the latest WUSCA ratings. Find out more about the WUSCAs <a target="_blank" href="<%=awardsURL%>">here</a></div>  
               <ul class="sub_lik nw">
               <c:forEach var="reviewCategoryList" items="${reviewCategoryList}">
                    <li><input type="checkbox" onclick="javascript:multiCheckPreffn('${reviewCategoryList.urlText}')" id="<c:out value="${reviewCategoryList.urlText}"/>"  name="${reviewCategoryList.urlText}" value="${reviewCategoryList.urlText}"/><label>${reviewCategoryList.reviewCategoryDisplayName}</label></li>
                </c:forEach>
                </ul>
            </div>
         </div>
    </c:if>
     --%>
     <form:form action="/*-courses/search" styleId="searchBean" >
        <input type="hidden" name="hidden_q" id="hidden_q" value="<%=q%>" />
        <input type="hidden" name="hidden_subject" id="hidden_subject" value="<%=subject%>" />
        <input type="hidden" name="hidden_university" id="hidden_university" value="<%=university%>" />
        <input type="hidden" name="hidden_module" id="hidden_module" value="<%=module%>" />
        <input type="hidden" name="hidden_locType" id="hidden_locType" value="<%=locType%>" />
        <input type="hidden" name="hidden_locationType" id="hidden_locationType" value="<%=locType%>" />
        <input type="hidden" name="hidden_location" id="hidden_location" value="<%=location%>" />
        <input type="hidden" name="hidden_postCode" id="hidden_postCode" value="<%=postCode%>" />
        <input type="hidden" name="hidden_dist" id="hidden_dist" value="<%=distance%>" />
        <input type="hidden" name="hidden_campusType" id="hidden_campusType" value="<%=campusType%>" />
        <input type="hidden" name="hidden_studyMode" id="hidden_studyMode" value="<%=studyMode%>" />
        <input type="hidden" name="hidden_empRateMax" id="hidden_empRateMax" value="<%=empRateMax%>" />
        <input type="hidden" name="hidden_empRateMin" id="hidden_empRateMin" value="<%=empRateMin%>" />
        <input type="hidden" name="hidden_ucasTarrif_max" id="hidden_ucasTarrif_max" value="<%=ucasTarrifMax%>" />
        <input type="hidden" name="hidden_ucasTarrif_min" id="hidden_ucasTarrif_min" value="<%=ucasTarrifMin%>" />
        <input type="hidden" name="hidden_russell" id="hidden_russell" value="<%=russellGroup%>" />
        <input type="hidden" name="hidden_entryLevel" id="hidden_entryLevel" value="<%=entryLevel%>" />
        <input type="hidden" name="hidden_entryPoints" id="hidden_entryPoints" value="<%=entryPoints%>" />
        <input type="hidden" name="hidden_sortorder" id="hidden_sortorder" value="<%=sortorder%>" />
        <input type="hidden" name="hidden_queryString" id="hidden_queryString" value="<%=qString%>" />
        <input type="hidden" name="hidden_modDefaultText" id="hidden_modDefaultText" value="<%=modDefaultText%>" />
        <input type="hidden" name="hidden_removeModule" id="hidden_removeModule" value="<%=removeModuleParameter%>" />
        <input type="hidden" name="hidden_jacs" id="hidden_jacs" value="<%=jacs%>" />
        <input type="hidden" name="hidden_pageno" id="hidden_pageno" value="<%=pageno%>" />
        <input type="hidden" name="hidden_ucasCode" id="hidden_ucasCode" value="<%=ucasCode%>" />        
        <input type="hidden" name="hidden_resultExists" id="hidden_resultExists" value="<%=resultExists%>" />
        <input type="hidden" name="hidden_searchCount" id="hidden_searchCount" value="<%=searchCount%>" />        
        <%--Added below hidden filters for your pref, 28_Aug_2018 By Sabapathi.S--%>
        <%-- <input type="hidden" name="hidden_preferenceType" id="hidden_preferenceType" value="<%=reviewCategoryType%>" />
        <input type="hidden" name="hidden_prefType" id="hidden_prefType" value="<%=reviewCategoryType%>" />--%>
        <input type="hidden" name="hidden_assessment" id="hidden_assessment" value="<%=assessmentType%>" />
        <input type="hidden" name="hidden_rf" id="hidden_rf" value="<%=rf%>" />
        <input type="hidden" name="hidden_score" id="hidden_score" value="<%=scoreValue%>" />        
      </form:form>
  </div>
</div>

<script type="text/javascript">
defaultSelectCheckBox("hidden_locType");
//defaultSelectCheckBox("hidden_preferenceType");
defualtOpenFilter('<%=qString%>');
defaultenablemodulebutton('<%=defaultModuleTitle%>');
</script>