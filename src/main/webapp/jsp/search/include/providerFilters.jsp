<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil"%>

<%
  String modDefaultText = "e.g. Module name";
  String moduleDefaultText = "e.g. Module name";
  String q = GenericValidator.isBlankOrNull(request.getParameter("q")) ? "" : (request.getParameter("q"));
  String subject = GenericValidator.isBlankOrNull(request.getParameter("subject")) ? "" : (request.getParameter("subject"));
  String university = GenericValidator.isBlankOrNull(request.getParameter("university")) ? "" : (request.getParameter("university"));
  String assessmentType = GenericValidator.isBlankOrNull(request.getParameter("assessment-type")) ? "" : (request.getParameter("assessment-type"));
  String module = request.getParameter("module");
              module = !GenericValidator.isBlankOrNull(module)? module: "";        
  String russellGroup = request.getParameter("russell-group"); 
             russellGroup = !GenericValidator.isBlankOrNull(russellGroup)? russellGroup: "";
  String distance = request.getParameter("distance");
              distance = !GenericValidator.isBlankOrNull(distance)? distance: "10";
  String postCode = request.getParameter("postcode");
              postCode = !GenericValidator.isBlankOrNull(postCode)? postCode:"";
  String empRateMax = request.getParameter("employment-rate-max");
              empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
  String empRateMin = request.getParameter("employment-rate-min");
              empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
  String campusType = request.getParameter("campus-type");
              campusType = !GenericValidator.isBlankOrNull(campusType)? campusType: "";
  String locType = request.getParameter("location-type");
              locType = !GenericValidator.isBlankOrNull(locType)? locType: "";
  String ucasTarrifMax = request.getParameter("ucas-points-max");
              ucasTarrifMax = !GenericValidator.isBlankOrNull(ucasTarrifMax)? ucasTarrifMax: "";
  String ucasTarrifMin = request.getParameter("ucas-points-min");
              ucasTarrifMin = !GenericValidator.isBlankOrNull(ucasTarrifMin)? ucasTarrifMin: "";  
  String smode = (String)request.getParameter("study-mode");
         smode = !GenericValidator.isBlankOrNull(smode)? smode : "";
  String sort = (String)request.getParameter("sort");
         sort = !GenericValidator.isBlankOrNull(sort)? sort : "";
  String jacs = (String)request.getParameter("jacs");//3_Jun_2014
         jacs = !GenericValidator.isBlankOrNull(jacs)? jacs : "";
  String ucasCode = (String)request.getParameter("ucas-code");//3_Jun_2014
         ucasCode = !GenericValidator.isBlankOrNull(ucasCode)? ucasCode : "";
  String srchPrefix = "";
  String srchType = request.getParameter("srchType");
  
  String defaultModuleTitle = (String)request.getAttribute("defaultModuleTitle");
         modDefaultText = !GenericValidator.isBlankOrNull(defaultModuleTitle)? defaultModuleTitle: modDefaultText;  
  String location = GenericValidator.isBlankOrNull(request.getParameter("location")) ? "" : (request.getParameter("location"));

  String entryLevel = GenericValidator.isBlankOrNull(request.getParameter("entry-level"))? "" : (request.getParameter("entry-level"));
  String entryPoints = GenericValidator.isBlankOrNull(request.getParameter("entry-points"))? "" : (request.getParameter("entry-points"));
  String disbledText = !GenericValidator.isBlankOrNull(defaultModuleTitle)? "":"disabled";
  String qString = (String)request.getAttribute("queryStr");
         qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";  
  String noFollowLink = (!GenericValidator.isBlankOrNull(qString)) ? "nofollow":"";
  String subjectCode = (String)request.getAttribute("paramCategoryCode");
              subjectCode = !GenericValidator.isBlankOrNull(subjectCode)? subjectCode.toUpperCase(): "";
  String studyLevel = (String)request.getAttribute("paramStudyLevelId");  
         //studyLevel = !GenericValidator.isBlankOrNull(studyLevel)? studyLevel.toUpperCase() :"";
  String studyModeId = (String)request.getAttribute("paramstudyModeValue");
         studyModeId = !GenericValidator.isBlankOrNull(studyModeId)? studyModeId : "All study modes";
  String seoFirstPageUrl = (String)request.getAttribute("SEO_FIRST_PAGE_URL");
         seoFirstPageUrl = !GenericValidator.isBlankOrNull("seoFirstPageUrl")? seoFirstPageUrl :"";
  
  if(srchType!=null && !"".equals(srchType)){
    if("clearing".equals(srchType)){
      srchPrefix = "clearing-";
    }
  }
%>
<div class="fltr">
  <%--Added close tag for filter in responsive 03_NOV_2015 By Indumathi S--%>
  <div class="filt_cls">
    <a href="#"><span>CLOSE</span><i class="fa fa-close"></i></a>
  </div>
  <div class="srt_by">SORT BY:</div>
  <div class="fltr_in"> 
      <h2>Filter by:</h2>
     <jsp:include page="/jsp/search/include/deleteProviderFilter.jsp"/>
     <div class="sh_hd">
       <div class="hdn" onclick="javascript:toggleMenu('modules','modulesh1');">
         <h4 id="modulesh1" class="rdat actct">Modules</h4>
       </div>
       <div id="modules" class="flt_lt" style="">
           <input type="text" name="moduleText" id="moduleText" value="<%=modDefaultText%>" maxlength="100" autocomplete="off"  onkeydown="javascript:clearDefaultText1(this, '<%=modDefaultText%>');enableButton('moduleButton', this, '<%=moduleDefaultText%>');"
              onclick="javascript:clearDefaultText1(this,'<%=modDefaultText%>');enableButton('moduleButton', this, '<%=moduleDefaultText%>');" 
              onblur="javascript:setDefaultText1(this,'<%=modDefaultText%>');enableButton('moduleButton', this, '<%=moduleDefaultText%>');" 
              onkeypress="clearDefaultText1(this, '<%=modDefaultText%>');enableButton('moduleButton', this, '<%=moduleDefaultText%>');javascript:if(event.keyCode==13){return providerResultsURL('modules');}"
              onkeyup = "setDefaultText1(this,'<%=modDefaultText%>');enableButton('moduleButton',this, '<%=moduleDefaultText%>');" 
              class="txt_box nw"/> 
           <input type="button" name="submit" id="moduleButton" disabled="<%=disbledText%>" class="go_ic" onclick="javascript:return providerResultsURL('modules')"/>
       </div>     
     </div>    
   <input type="hidden" name="hidden_module" id="hidden_module" value="<%=module%>" />        
  <c:if test="${not empty requestScope.subjectList}">
       <div class="sh_hd">
          <div class="hdn" onclick="javascript:toggleMenu('subject','subjecth1');">
            <h4 id="subjecth1" class="rdat actct">Subject </h4>
          </div>
          <div id="subject" class="flt_lt" style="display: block;">
            <ul class="sub_lik num">
            <c:forEach var="subjectList" items="${requestScope.subjectList}">
            <c:set var="subjectCode" value="<%=subjectCode%>"/>
            <c:if test="${subjectList.categoryCode eq subjectCode}">
                   <p class="stud fl-act">
                      <span class="act_bg"></span><span class="sbj-act">${subjectList.categoryDesc}</span>
                      <span class="fr">(${subjectList.collegeCount})</span>
                    </p>
                 </c:if>
                 <c:if test="${subjectList.categoryCode ne subjectCode}">
                 <c:set var="subjectDesc" value="${subjectList.categoryDesc}"/>
                  <li>                    
                    <a rel="<%=noFollowLink%>" onclick="searchEventTracking('pr-filter','<%=srchPrefix%>subject','${subjectDesc.toLowerCase()}');"  href="${subjectList.browseMoneyPageUrl}">${subjectList.categoryDesc} </a>                   
                    <span class="shde">(${subjectList.collegeCount})</span>
                  </li>
                 </c:if>                  
             </c:forEach>
            </ul>
          </div>
       </div>
     </c:if>
     <c:if test="${not empty requestScope.qualList}">
          <div class="sh_hd">
            <div class="hdn" onclick="javascript:toggleMenu('qualList','qualListh1');">
              <h4 id="qualListh1" class="rdat">Study level </h4>
            </div>
            <div id="qualList" class="flt_lt" style="display: none;">
              <ul class="sub_lik"> 
              <c:forEach var="qualList" items="${requestScope.qualList}">
                <c:if test="${qualList.refineCode eq requestScope.paramStudyLevelId}">
                      <p class="stud fl-act">
                        <span class="act_bg"></span>
                        ${qualList.refineDisplayDesc}
                      </p>
                   </c:if>
                   <c:if test="${qualList.refineCode ne requestScope.paramStudyLevelId}">
                     <li>                
                         <c:set var="qualDesc" value="${qualList.refineDesc}"/> 
                         <a onclick="searchEventTracking('pr-filter','<%=srchPrefix%>study-level','${qualDesc.toLowerCase()}');" href="${qualList.browseMoneyageLocationRefineUrl}" >${qualList.refineDisplayDesc}</a>
                     </li>
                   </c:if>
                 </c:forEach>
               </ul>             
             </div>
          </div>
     </c:if>
     <c:if test="${not empty requestScope.studyModeList}">
         <div class="sh_hd">
            <div class="hdn" onclick="javascript:toggleMenu('studyMode','studyModeh1');">
              <h4 id="studyModeh1" class="rdat actct">Study mode </h4>
            </div>
            <div id="studyMode" class="flt_lt" style="display:block;">
              <ul class="sub_lik">
              <c:forEach var="studyModeList" items="${requestScope.studyModeList}">
                <c:set var="studyModeId" value="<%=studyModeId%>"/>
                <c:if test="${studyModeList.refineDesc eq studyModeId}">  
                    <p class="stud fl-act">
                      <span class="act_bg"></span>
                      ${studyModeList.refineDesc}
                    </p>
                   </c:if>
                    <c:if test="${studyModeList.refineDesc ne studyModeId}">              
                    <li>                
                      <c:set var="studyModeDesc" value="${studyModeList.refineDesc}"/>
                      <a rel="nofollow" onclick="searchEventTracking('pr-filter','<%=srchPrefix%>study-mode','${studyModeDesc.toLowerCase()}');" href="${studyModeList.filterURL}">${studyModeList.refineDesc}</a>
                    </li>
                  </c:if>
                </c:forEach>   
              </ul>             
            </div>
         </div>
     </c:if>
     <%--Added below filter Assessment, 28_Aug_2018 By Sabapathi.S--%>
     <%--
     <logic:present name="assessmentList" scope="request">
       <logic:notEmpty name="assessmentList" scope="request">
         <div class="sh_hd">
            <div class="hdn" onclick="javascript:toggleMenu('assessment','assessmenth1');">
              <h4 id="assessmenth1" class="rdat actct">How you're assessed </h4>
            </div>
            <div id="assessment" class="flt_lt" style="display:block;">
              <ul class="sub_lik">   
                <logic:iterate id="assessmentList" name="assessmentList" scope="request">
                  <logic:equal name="assessmentList" property="urlText" value="<%=assessmentType%>">
                    <p class="stud fl-act">
                      <span class="act_bg"></span>
                      <bean:write name="assessmentList" property="assessmentDisplayName" />
                    </p>
                   </logic:equal>
                   <logic:notEqual name="assessmentList" property="urlText" value="<%=assessmentType%>">                      
                    <li>                
                      <bean:define id="assessmentTypeName" name="assessmentList" property="assessmentDisplayName" type="java.lang.String"/>
                      <a rel="nofollow" onclick="searchEventTracking('pr-filter','<%=srchPrefix%>assessment-type','<%=assessmentTypeName.toLowerCase()%>');" href="<bean:write name="assessmentList" property="browseMoneyAssessedRefineUrl" />"><bean:write name="assessmentList" property="assessmentDisplayName" /></a>
                    </li>
                  </logic:notEqual>
                </logic:iterate>
              </ul>             
            </div>
         </div>
        </logic:notEmpty>
     </logic:present>
     --%>
     
      <form:form action="/courses/*/*/*/*/*/csearch" commandName="searchBean" >
        <input type="hidden" name="hidden_q" id="hidden_q" value="<%=q%>" />
        <input type="hidden" name="hidden_subject" id="hidden_subject" value="<%=subject%>" />
        <input type="hidden" name="hidden_university" id="hidden_university" value="<%=university%>" />
        <input type="hidden" name="hidden_russell" id="hidden_russell" value="<%=russellGroup%>" />
        <input type="hidden" name="hidden_module" id="hidden_module" value="<%=module%>" />
        <input type="hidden" name="hidden_dist" id="hidden_dist" value="<%=distance%>" />
        <input type="hidden" name="hidden_postCode" id="hidden_postCode" value="<%=postCode%>" />
        <input type="hidden" name="hidden_empRateMax" id="hidden_empRateMax" value="<%=empRateMax%>" />
        <input type="hidden" name="hidden_empRateMin" id="hidden_empRateMin" value="<%=empRateMin%>" />
        <input type="hidden" name="hidden_location" id="hidden_location" value="<%=location%>" />
        <input type="hidden" name="hidden_locationType" id="hidden_locationType" value="<%=locType%>" />
        <input type="hidden" name="hidden_campusType" id="hidden_campusType" value="<%=campusType%>" />
        <input type="hidden" name="hidden_ucasTarrif_max" id="hidden_ucasTarrif_max" value="<%=ucasTarrifMax%>" />
        <input type="hidden" name="hidden_ucasTarrif_min" id="hidden_ucasTarrif_min" value="<%=ucasTarrifMin%>" />        
        <input type="hidden" name="hidden_queryString" id="hidden_queryString" value="<%=qString%>" />
        <input type="hidden" name="sortingUrl" id="sortingUrl" value="<%=seoFirstPageUrl%>" />
        <input type="hidden" name="hidden_modDefaultText" id="hidden_modDefaultText" value="<%=modDefaultText%>" />
        <input type="hidden" name="hidden_studyMode" id="hidden_studyMode" value="<%=smode%>" />
        <input type="hidden" name="hidden_sortorder" id="hidden_sortorder" value="<%=sort%>" />
        <input type="hidden" name="hidden_jacs" id="hidden_jacs" value="<%=jacs%>" />
        <input type="hidden" name="hidden_ucasCode" id="hidden_ucasCode" value="<%=ucasCode%>" />
        <input type="hidden" name="hidden_entryLevel" id="hidden_entryLevel" value="<%=entryLevel%>" />
        <input type="hidden" name="hidden_entryPoints" id="hidden_entryPoints" value="<%=entryPoints%>" />
        <input type="hidden" name="hidden_assessment" id="hidden_assessment" value="<%=assessmentType%>" />
      </form:form>
  </div>
</div>
<script type="text/javascript">
defaultenablemodulebutton('<%=defaultModuleTitle%>');
</script>