<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil, spring.valueobject.articles.ArticleGroupVO" autoFlush="true" %>
<% String subjectDesc = request.getParameter("subjectDesc") != null ? request.getParameter("subjectDesc") : "";   %>
   <div id = "articleTab" class="sr-art" style="display:none;">
     <div class="sart-lt">
        <c:if test="${not empty subjectGuideURL}"><c:out value="${requestScope.subjectGuideURL}"></c:out></c:if>
        <c:if test="${not empty articleList}">
        <c:forEach var="articleList" items="${articleList}" varStatus="i">
       
        <%
         
        //  String articleCategoryName = ((ArticleGroupVO)articleList).getArticleGroupName();          
        //  String articleCategoryUrl = "/student-centre/"+(new CommonUtil().replaceSpaceByHypen(articleCategoryName).toLowerCase())+".html";     
        %>         
         <div id="articleroot${i.index}" class="sart1">         
           <div id="articleDiv${i.index}" class=""  style="display:block;" >
            <h2><c:out value="${articleList.articleHeading}"/></h2>
            <div id="articleCont${i.index}" class="art-cont"><c:out value="${ articleList.articleShortDescription}"/></div>
           </div>  
            <div id="articleDesc${i.index}" class="sart1 nw" style="display:none;"></div>
              <a id="articleLink${i.index}" class="addcou1" onclick="articleShowHide('articleDesc${i.index}', this, ${i.index}, <c:out value="${ articleList.articleId}"/>)" href="javascript:void(0);">
                View more
              <em></em>
            </a>
          </div>
        </c:forEach>
        </c:if>
      </div> 
      <div class="sart-rt">
      <c:if test="${not empty articleList}">        
           <jsp:include page="/jsp/home/spamBoxReg.jsp"/>          
       
      </c:if>
      </div>
  </div>

<c:if test="${not empty articleList}">
    <script type="text/javascript">
      document.getElementById("showspamBox").style.display='block'
    </script>
  </c:if>