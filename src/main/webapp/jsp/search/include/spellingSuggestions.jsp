<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="WUI.utilities.CommonFunction, WUI.utilities.CommonUtil, com.wuni.util.seo.SeoUrls" %>

<% 
  String subjectname = (String) request.getAttribute("err_subject_name");
  subjectname = subjectname !=null && !subjectname.equalsIgnoreCase("null") && subjectname.trim().length()>0 ? new CommonUtil().toTitleCase(subjectname) : "";
  String studylevelid = (String) request.getAttribute("err_study_level_id");
  studylevelid = studylevelid !=null && !studylevelid.equalsIgnoreCase("null") && studylevelid.trim().length()>0 ? studylevelid : "";
  String studylevelname = (String) request.getAttribute("err_study_level_name");
  studylevelname = studylevelname !=null && !studylevelname.equalsIgnoreCase("null") && studylevelname.trim().length()>0 ? new CommonUtil().toLowerCase(studylevelname)  : "";
  String location = (String) request.getAttribute("err_location");         
  location = location !=null && !location.equalsIgnoreCase("null") && location.trim().length()>0 ? new CommonUtil().toTitleCase(new CommonFunction().replaceHypenWithSpace(location.trim())) : "";
  String providername = (String) request.getAttribute("err_provider_name");
  providername = providername !=null && !providername.equalsIgnoreCase("null") && providername.trim().length()>0 ? providername.trim() : "";
  String collegeId = (String) request.getAttribute("collegeId");
  collegeId = collegeId != null && collegeId.trim().length() > 0? collegeId : "0"; 
  String categoryCode = request.getAttribute("categoryCode") != null ? request.getAttribute("categoryCode").toString() : "";  
  String urlString = "/degree-courses/search?subject=";
%>

 <body id="www-whatuni-com">
   <%--Added by Indumathi.S for responsive redesign Jan-27-16 --%>
   <div class="splmstk">
     <header class="clipart">
       <div class="ad_cnr">
         <div class="content-bg">
           <div id="desktop_hdr">
             <jsp:include page="/jsp/common/wuHeader.jsp" />
           </div>                
         </div>      
       </div>
     </header>    
     <div class="ad_cnr">
       <div class="content-bg">
         <div id="content-left">
           <h1>No results for <%=subjectname%> <%=studylevelname%> in <%=providername%> <%=location%></h1>
           <p>We are sorry but we cannot find any courses with the keyword <strong class="cfnt"><%=subjectname%></strong> in the title</p>
           <%-- Spelling suggestion --%>
           <c:if test="${not empty requestScope.listOfSpellingSuggestions}">
               <div class="dlinks"><h2>Did you mean:</h2>
                 <c:forEach var="spellSuggest" items="${requestScope.listOfSpellingSuggestions}">
                   <a href="<c:out value="${spellSuggest.linkToBrowseMoneyPage}" escapeXml="false" />"><c:out value="${spellSuggest.categoryDesc}" escapeXml="false" /></a>
                   <span>|</span>
                 </c:forEach>  
               </div>       
           </c:if>                               
           <div class="clear"><h1>A-Z list of subjects</h1></div>
           <div class="clear-1">Whatuni has over 100,000 higher education courses listed on this website. You will find degrees, access/foundation certificates and HNDs/HNCs covering subjects from Animation to Veterinary Medicine.
           </div>
           <div class="clear-1">
             <a href="<%=request.getContextPath()%>/courses/" class="btn1 blue" title="UK University degree courses">UK University degree courses<i class="fa fa-long-arrow-right"></i></a>
           </div>
         </div>
            
         <div class="sgallery">
           <h2 class="mb-20">Popular subjects</h2>
           <ul>
             <li>
               <a href="<%=urlString%>nursing">
                 <img alt="Nursing" src="<%=CommonUtil.getImgPath("/wu-cont/images/nursing.jpg" , 0)%>">             
                 <div class="nma_hw"><span class="cat_title">
                   <img alt="Nursing" src="<%=CommonUtil.getImgPath("/wu-cont/images/spell-suggest/nursing.svg", 0)%>">							  
                   <span class="spltle">Nursing</span>
                 </span></div>
               </a>
             </li>  
             <li>
               <a href="<%=urlString%>teaching">
                 <img alt="Teaching" src="<%=CommonUtil.getImgPath("/wu-cont/images/teaching.jpg", 0)%>">              
                 <div class="nma_hw"><span class="cat_title">
                   <img alt="Teaching" src="<%=CommonUtil.getImgPath("/wu-cont/images/spell-suggest/teaching.svg", 0)%>">
                   <span class="spltle">Teaching</span>
                 </span></div>
               </a>
             </li>
             <li>
               <a href="<%=urlString%>sports-physiotherapy">
                 <img alt="Physiotherapy" src="<%=CommonUtil.getImgPath("/wu-cont/images/physiotherapy.jpg", 0)%>">             
                 <div class="nma_hw"><span class="cat_title">
                   <img alt="Physiotherapy" src="<%=CommonUtil.getImgPath("/wu-cont/images/spell-suggest/physiotherapy.svg", 0)%>">
                   <span class="spltle">Physiotherapy</span>                
                 </span></div>
               </a>
             </li>
             <li>
               <a href="<%=urlString%>medicine">
                 <img alt="Medicine" src="<%=CommonUtil.getImgPath("/wu-cont/images/medicine.jpg", 0)%>">              
                 <div class="nma_hw"><span class="cat_title">
                   <img alt="Medicine" src="<%=CommonUtil.getImgPath("/wu-cont/images/spell-suggest/medicine.svg", 0)%>">
                   <span class="spltle">Medicine</span>                
                 </span></div>
               </a>
             </li>
             <li>
               <a href="<%=urlString%>architecture">
                 <img alt="Architecture" src="<%=CommonUtil.getImgPath("/wu-cont/images/architecture.jpg", 0)%>">             
                 <div class="nma_hw"><span class="cat_title">
                   <img alt="Architecture" src="<%=CommonUtil.getImgPath("/wu-cont/images/spell-suggest/architecture.svg", 0)%>">
                   <span class="spltle">Architecture</span>						    
                 </span></div>
               </a>
             </li>
             <li>
               <a href="<%=urlString%>sports-studies">
                 <img alt="Sports Studies"  src="<%=CommonUtil.getImgPath("/wu-cont/images/sports-studies.jpg", 0)%>">              
                 <div class="nma_hw"><span class="cat_title">
                   <img alt="Sports Studies"  src="<%=CommonUtil.getImgPath("/wu-cont/images/spell-suggest/sport studies.svg", 0)%>">
                   <span class="spltle">Sports Studies</span>
                 </span></div>
               </a>
             </li>
             <li class="mb0">
               <a href="<%=urlString%>fashion">
                 <img alt="Fashion" src="<%=CommonUtil.getImgPath("/wu-cont/images/fashion.jpg?dfg", 0)%>">    
                 <div class="nma_hw"><span class="cat_title">
						       <img alt="Fashion"  src="<%=CommonUtil.getImgPath("/wu-cont/images/spell-suggest/fashion.svg", 0)%>">
						       <span class="spltle">Fashion</span>
                 </span></div>
               </a>
             </li>
             <li class="mb0">
               <a href="<%=urlString%>earth-sciences">
                 <img alt="Earth Sciences"  src="<%=CommonUtil.getImgPath("/wu-cont/images/earth-science.jpg", 0)%>">
                 <div class="nma_hw"><span class="cat_title">
						       <img alt="Earth Sciences"  src="<%=CommonUtil.getImgPath("/wu-cont/images/spell-suggest/earth studies.svg", 0)%>">
						       <span class="spltle">Earth Sciences</span>
                 </span></div>
               </a>
             </li>
             <li class="mb0">
               <a href="<%=urlString%>computer-science">
                 <img alt="Computer Science" src="<%=CommonUtil.getImgPath("/wu-cont/images/computer-science.jpg", 0)%>">
                 <div class="nma_hw"><span class="cat_title">
                   <img alt="Computer Science" src="<%=CommonUtil.getImgPath("/wu-cont/images/spell-suggest/computer science.svg", 0)%>">
                   <span class="spltle">Computer Science</span>
                 </span></div>
               </a>
             </li>
             <div class="clr"></div>
            </ul>
          </div>
        </div>
      </div>
      <!--End-->
      <%request.setAttribute("insightPageFlag", "yes");%>
    </div>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
  </body>