<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@page import="WUI.utilities.CommonUtil,WUI.utilities.SessionData,org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.GlobalFunction" %>
<%  CommonFunction common = new CommonFunction();
    String collegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("collegeNameDisplay") : "";
    String studyLevelDesc = (String)request.getAttribute("studyLevelDesc");
    String paramCollegeName = (String) request.getAttribute("collegeName");
    paramCollegeName = paramCollegeName != null && !paramCollegeName.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(paramCollegeName).trim() : "";      
    String pros_search_name = request.getAttribute("prospectus_search_name") != null ? request.getAttribute("prospectus_search_name").toString() : "";
    String searchPageType = request.getParameter("searchPageType");
           searchPageType = !GenericValidator.isBlankOrNull(searchPageType)?searchPageType:"";
    String clrText = "";
    String clbtnclassname = "";
    if("clearing".equals(searchPageType)){clrText= "clearing-"; clbtnclassname ="clr_btns";}
    String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");
    String searchhits = request.getParameter("searchhits");
           searchhits = !GenericValidator.isBlankOrNull(searchhits)?searchhits:"";
    String advertiserFlag = (String)request.getAttribute("advertiserFlag");
           advertiserFlag = GenericValidator.isBlankOrNull(advertiserFlag)?"": advertiserFlag.equalsIgnoreCase("y") ? "y" : "n"; 
           String assignCollegeLogo = "";           
%>
<c:if test="${not empty requestScope.listOfCollegeDetails}">
  <c:forEach var="listOfCollegeDetails" items="${requestScope.listOfCollegeDetails}">
       <c:if test="${not empty listOfCollegeDetails.collegeLogo}">
        <c:set var="defineCollegeLogo" value="${listOfCollegeDetails.collegeLogo}"/>
        <%assignCollegeLogo=(String)pageContext.getAttribute("defineCollegeLogo");%>        
      </c:if>
      <c:if test="${empty listOfCollegeDetails.collegeLogo}">
        <%assignCollegeLogo="";%>
      </c:if>
    </c:forEach>
  </c:if>
  <c:if test="${not empty requestScope.courseList}">
    <c:forEach var="courseList" items="${requestScope.courseList}" varStatus="rowId">
     <c:set var="courseId" value="${courseList.courseId}"/>
     <c:set var="tcollegId" value="${courseList.collegeId}"/>
      <%
      String institutionId = new GlobalFunction().getInstitutionId((String)pageContext.getAttribute("tcollegId"));
      %>
      <div class="srch_cnr">
        <div class="srlogo_lt">
          <div class="logo_cmp cmp-bl">	
               <c:if test="${courseList.wasThisCourseShortListed eq 'FALSE'}">                    
                  <div class="cmlst" id="basket_div_${courseId}">
                  <div class="compare" onmouseover="addToComparisonHover(${courseId});" onmouseout="addToComparisonOut(${courseId});"> <%--17_Mar_2015 - Added to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                    <c:set var="srcEvntCourseTitle1" value="${courseList.courseTitle}"/>
                    <a onclick='addBasket("${courseId}", "O", this, "basket_div_${courseId}", "basket_pop_div_${courseId}");searchEventTracking("view comparison","<%=clrText%>clicked","<%=common.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle1"))%>: <%=common.replaceSpecialCharacter(paramCollegeName)%>");'>
                    <span class="icon_cmp f5_hrt"></span>
                    <span class="cmp_txt">COMPARE</span>
                    <span class="loading_icon" id="load_${courseId}" style="display:none;"></span>
                    <span class="chk_cmp"><span class="chktxt" id="show_${courseId}" style="display:none;">Add to comparison<em></em></span></span> <%--17_Mar_2015 - Added reference id to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                    </a>
                  </div>  
                  </div>
                  <div class="cmlst act" id="basket_pop_div_${courseId}" style="display:none;"> 
                  <div class="compare">
                  <a class="act" onclick='addBasket("${courseId}", "O", this,"basket_div_${courseId}", "basket_pop_div_${courseId}");searchEventTracking("view comparison","<%=clrText%>removed","<%=common.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle1"))%>: <%=common.replaceSpecialCharacter(paramCollegeName)%>");'>
                  <span class="icon_cmp f5_hrt hrt_act"></span>
                  <span class="cmp_txt">COMPARE</span>
                  <span class="loading_icon" id="load1_${courseId}" style="display:none;"></span>
                  <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                  </a>
                    </div>
                    <div id="defaultpop_${courseId}" class="sta" style="display: block;">
                      <%
                        String userId = new SessionData().getData(request, "y");
                        if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
                      %>
                        <a class="view_more" href="/degrees/comparison">View comparison</a>
                      <%}else{%>
                         <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
                      <%}%>
                    </div>
                    <div id="pop_${courseId}" class="sta"></div>
                  </div>
                </c:if>
                <c:if test="${courseList.wasThisCourseShortListed eq 'TRUE'}">
                  <div class="cmlst" id="basket_div_${courseId}" style="display:none;">
                    <div class="compare" onmouseover="addToComparisonHover(${courseId});" onmouseout="addToComparisonOut(${courseId});"> <%--17_Mar_2015 - Added to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                    <c:set var="srcEvntCourseTitle2" value="${courseList. courseTitle}"/>
                    <a onclick='addBasket("${courseId}", "O", this, "basket_div_${courseId}", "basket_pop_div_${courseId}");searchEventTracking("view comparison","<%=clrText%>clicked","<%=common.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle2"))%>: <%=common.replaceSpecialCharacter(paramCollegeName)%>");'>
                    <span class="icon_cmp f5_hrt"></span>
                    <span class="cmp_txt">COMPARE</span>
                    <span id="load_${courseId}" style="display:none;"></span>
                    <span class="chk_cmp"><span class="chktxt" id="show1_${courseId}" style="display:none;">Add to comparison<em></em></span></span> <%--17_Mar_2015 - Added reference id to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                    </a>                 
                    </div>
                  </div>
                  <div class="cmlst act" id="basket_pop_div_${courseId}"> 
                    <div class="compare">
                    <a class="act" onclick='addBasket("${courseId}", "O", this,"basket_div_${courseId}", "basket_pop_div_${courseId}");searchEventTracking("view comparison","<%=clrText%>removed","<%=common.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle2"))%>: <%=common.replaceSpecialCharacter(paramCollegeName)%>");'>
                    <span class="icon_cmp f5_hrt hrt_act"></span>
                    <span class="cmp_txt">COMPARE</span>
                    <span class="loading_icon" id="load1_${courseId}" style="display:none;"></span>
                    <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                    </a>
                    </div>
                    <div id="defaultpop_${courseId}" class="sta" style="display: block;">
                      <%
                        String userId = new SessionData().getData(request, "y");
                        if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
                      %>
                          <a class="view_more" href="/degrees/comparison">View comparison</a>
                      <%}else{%>
                          <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
                      <%}%>
                    </div>
                    <div id="pop_${courseId}" class="sta" style="display:none;"></div>
                  </div>
                </c:if>                          
          </div>
        </div>   

        <div class="dtls" onmouseover="addToComparisonHover(${courseId});" onmouseout="addToComparisonOut(${courseId});"> <%--17_Mar_2015 - Added to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
            <div class="blu cf">
              <div class="codls">
                <h2><a href="${courseList.courseDetailUrl}">${courseList.courseTitle}</a></h2>
                <div class="enreq cf">
                  <div class="blk2">
                   <c:if test="${not empty courseList.departmentOrFaculty}">
                     <c:if test="${not empty courseList.spURL}">
                        <p class="cnl nw"><a href="${courseList.spURL}">${courseList.departmentOrFaculty}</a></p>
                        <span><a href="${courseList.spURL}" class="hat"></a></span>
                      </c:if>
                      <c:if test="${empty courseList.spURL}">
                        <p class="cnl">${courseList.departmentOrFaculty}</p>
                      </c:if>
                    </c:if>
                    <p>${courseList.availableStudyModes}</p>
                  </div>
                  <div class="blk1 mr24">
                  <c:if test="${not empty courseList.ucasCode}">
                      <span>
                        <strong>${courseList.ucasCode}</strong> UCAS code
                      </span> 
                    </c:if> 
                    <c:if test="${not empty courseList.employmentRate}">                     
                     <span>
                      <strong>${courseList.employmentRate}%</strong>
                      <span class="tooltip_cnr">
                         <span class="blk_txt">Employment rate </span>
                         <span class="tool_tip fl">
                            <i class="fa fa-question-circle fnt_24">
                              <span class="cmp">
                                <div class="hdf5"></div>
                                <div><spring:message code="wuni.tooltip.kis.entry.text" /></div> <%--Changed KIS text and year to App.properties for 15_Nov_2016 By Thiyagu G--%>
                                <div class="line"></div>
                              </span>
                            </i>
                          </span>
                      </span>
                      </span>
                    </c:if>    
                    <c:if test="${not empty courseList.courseRank}">                       
                      <span>
                        <strong>${courseList.courseRank}${courseList.rankOrdinal}</strong> 
                        <span class="tooltip_cnr">
                         <span class="blk_txt">CompUniGuide subject ranking </span> <%--Changed ranking lable frim Times to CUG for 08_MAR_2016 By Thiyagu G--%>
                         <span class="tool_tip fl">
                            <i class="fa fa-question-circle fnt_24">
                              <span class="cmp">
                                <div class="hdf5"></div>
                                <div> <spring:message code="wuni.tooltip.cug.ranking.text" /></div> <%--Changed ranking tooltip content from property file for 08_MAR_2016 By Thiyagu G--%>
                                <div class="line"></div>
                              </span>
                            </i>
                          </span>
                      </span>
                      </span> 
                    </c:if>       
                  </div>
                  <div class="blk1 rt">
                  <%if(studyLevelDesc != null && studyLevelDesc!="" && !studyLevelDesc.contains("Postgraduate")){%>
                    <span class="tooltip_cnr ttip_cnt">
                      <span class="fad">Entry requirements</span>
                      <c:if test="${courseList.entryRquirements}">
                        <span class="tool_tip fl"> 
                        <i class="fa fa-question-circle fnt_24">
                          <span class="cmp">
                          <div class="hdf5"></div>
                          <div><spring:message code="wuni.tooltip.entry.requirements.text"/></div> 
                          <div><spring:message code="wuni.tooltip.entry.requirements.further.info.text"/></div> 
                          <div class="line"></div>
                          </span>
                        </i>
                        </span>
                      </c:if>
                    </span>
                    <c:if test="${not empty courseList.entryRquirements}">
                      ${courseList.entryRquirements}
                    </c:if>
                     <c:if test="${empty courseList.entryRquirements}">
                      <span>Please contact Uni directly</span>
                    </c:if>
                    <%}else if(studyLevelDesc != null && studyLevelDesc!="" && studyLevelDesc.contains("Postgraduate")){%>
                       <c:if test="${not empty courseList.fees}">
                          <div class="blk1 rt tu_fees">
                              <span class="fad">Tuition fees</span>
                              <span><strong>${courseList.fees}</strong>${courseList.feeDuration}</span>
                          </div>
                        </c:if>
                        <c:if test="${empty courseList.fees}">
                          <div class="blk1 rt tu_fees">
                              <span class="fad">Tuition fees</span><br/>
                              <span>Fee not supplied by uni</span>
                          </div>
                        </c:if>
                    <%}%>
                  </div>
                </div>
                <c:if test="${not empty courseList.moduleGroupDetails}">
                  <div class="mdls">
                    <a class="sh pls" id="module${courseList.moduleGroupId}${rowId.index}" onclick="loadmoduledata('${rowId.index}', '${courseList.moduleGroupId}','${courseList.courseDetailUrl}')">
                      <em>Show</em>
                      ${courseList.moduleTitle}
                    </a>
                    <div id="module_data_${courseList.moduleGroupId}${rowId.index}" style="display:none;">
                      <%--Added module related changes in clearing page for 31_May_2016 release, By Thiyagu G--%>
                      <c:if test="${not empty courseList.moduleDetailList}">
                         <c:forEach var="moduleDetailList" items="${courseList.moduleDetailList}">
                          <ul>                           
                                <li>${moduleDetailList} </li>                           
                          </ul>
                        </c:forEach>
                        <a class="vwall" onclick="gtmproductClick(${i.index}, 'sr_to_cd')" href="${courseList.courseDetailUrl}" >View all modules</a>  
                     </c:if>
                     <%--End of change--%>
                    </div>
                  </div>
                </c:if>
              </div>
              <!--Changed the buttons for responsive redesign 03_Nov_2015 By S.Indumathi-->
            <div class="res_btn abtst_v2">           
              <div class="btns <%=clbtnclassname%>"> 
               <%if("clearing".equals(searchPageType)){%>  
                  <%if("ON".equalsIgnoreCase(clearingonoff)){%> <%-- added for not showing buttons when clearing is off--%>
                 <div class="srwebsite">
                  <c:if test="${not empty courseList.subOrderWebsite}">              
                     <a rel="nofollow" 
                         target="_blank" 
                         class="visit-web" 
                         onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${courseList.gaCollegeName}', ${courseList.websitePrice}); cpeWebClickClearingWithCourse(this,'${courseList.collegeId}','${courseList.courseId}','${courseList.subOrderItemId}','${courseList.networkId}','${courseList.subOrderWebsite}');addBasket('${courseList.courseId}', 'O', 'visitweb','basket_div_${courseList.courseId}', 'basket_pop_div_${courseList.courseId}');" 
                         href="/degrees/visitwebredirect.html?id=${courseList.subOrderItemId}"
                         title="<%=collegeNameDisplay%> website">Visit website      
                         <i class="fa fa-caret-right"></i>
                     </a>
                   </c:if>  
                   <c:if test="${not empty courseList.hotLineNo}">
                      <a title="<%=collegeNameDisplay%> phone no." class="clr-call mr17" onclick="setTimeout(function(){location.href='tel:${courseList.hotLineNo}'},1000); GAInteractionEventTracking('Webclick', 'interaction', 'hotline', '${courseList.gaCollegeName}');cpeHotlineClearing(this,'${courseList.collegeId}','${courseList.subOrderItemId}','${courseList.networkId}','${courseList.hotLineNo}');"><i class="fa fa-phone"></i>${courseList.hotLineNo}</a>
                   </c:if>
                 </div>
                 <c:if test="${not empty courseList.subOrderItemId}">
                  <c:if test="${courseList.subOrderItemId eq 0}">                           
                      <div class="sremail">
                        <a rel="nofollow" 
                            target="_blank"
                            class="req-inf"
                            onclick="javaScript:nonAdvPdfDownload('${courseList.gaCollegeName}#${courseList.webformPrice}#${courseList.collegeId}#${courseList.subOrderItemId}#${courseList.networkId}#${courseList.subOrderEmailWebform}#${courseList.courseId}#<%=CommonUtil.getImgPath("",0)%><%=assignCollegeLogo%>#COURSE#<%=institutionId%>');"
                            title="Email ${courseList.collegeNameDisplay}">
                          Request info<i class="fa fa-caret-right"></i>
                        </a>
                      </div>         
                      </c:if>                     
                    </c:if>
                    <c:if test="${empty courseList.subOrderItemId}">
                                         
                        <div class="sremail">
                        <a rel="nofollow" 
                            target="_blank"
                            class="req-inf"
                            onclick="javaScript:nonAdvPdfDownload('${courseList.gaCollegeName}#${courseList.webformPrice}#${courseList.collegeId}#${courseList.subOrderItemId}#${courseList.networkId}#${courseList.subOrderEmailWebform}#${courseList.courseId}#<%=CommonUtil.getImgPath("",0)%><%=assignCollegeLogo%>#COURSE#<%=institutionId%>');"
                            title="Email ${courseList.collegeNameDisplay}">
                          Request info<i class="fa fa-caret-right"></i>
                        </a>
                        </div>
                    </c:if>
                 <%}%>
               <%}else{%>
                  <c:if test="${not empty courseList.subOrderEmailWebform}">
                    <div class="premail"><%--Changed event action webclick to webform by Sangeeth.S for July_3_18 rel--%>
                    <a rel="nofollow" 
                        target="_blank"
                        class="req-inf"
                        onclick="GAInteractionEventTracking('emailwebform', 'interaction', 'email webform', '${courseList.gaCollegeName}', ${courseList.webformPrice}); cpeEmailWebformClick(this,'${courseList.collegeId}','${courseList.subOrderItemId}','${courseList.networkId}','${courseList.subOrderEmailWebform}');" 
                        href="${courseList.subOrderEmailWebform}" 
                        title="Email ${courseList.collegeNameDisplay}">
                         Request info<i class="fa fa-caret-right"></i>
                    </a>
                    </div>
                  </c:if>
                  <%pros_search_name = request.getAttribute("prospectus_search_name") != null ? common.replaceURL(request.getAttribute("prospectus_search_name").toString()) : "0";%>
                  <c:if test="${empty courseList.subOrderEmailWebform}">
                   <c:if test="${not empty courseList.subOrderEmail}">
                      <div class="premail">
                      <a rel="nofollow"
                          class="req-inf"
                          onclick="GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '${courseList.gaCollegeName}');adRollLoggingRequestInfo('${courseList.collegeId}',this.href);return false;"                
                          href="<SEO:SEOURL pageTitle="sendcollegemail" >${courseList.collegeName}#${courseList.collegeId}#${courseList.courseId}#<%=pros_search_name%>#n#${courseList.subOrderItemId}</SEO:SEOURL>"
                          title="Email ${courseList.collegeNameDisplay}" >
                       Request info<i class="fa fa-caret-right"></i>
                      </a>   <%--13-JAN-2015  modified for adroll marketing--%>
                      </div>
                    </c:if>
                  </c:if>
                  <c:if test="${not empty courseList.subOrderProspectusWebform}">
                    <div class="prprospectus">
                    <a rel="nofollow" 
                        target="_blank" 
                        class="get-pros" 
                        onclick="GAInteractionEventTracking('prospectuswebform', 'interaction', 'prospectus webform', '${courseList.gaCollegeName}', ${courseList.webformPrice}); cpeProspectusWebformClick(this,'${courseList.collegeId}','${courseList.subOrderItemId}','${courseList.networkId}','${courseList.subOrderProspectusWebform}');" 
                        href="${courseList.subOrderProspectusWebform}" 
                        title="Get <%=collegeNameDisplay%> Prospectus">
                         Get prospectus<i class="fa fa-caret-right"></i></a>
                    </div>    
                  </c:if>
                  <c:if test="${empty courseList.subOrderProspectusWebform}">
                  <c:if test="${not empty courseList.subOrderProspectus}">
                      <div class="prprospectus">
                      <a onclick="GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '${courseList.gaCollegeName}'); return prospectusRedirect('/degrees','${courseList.collegeId}','${courseList.courseId}','<%=pros_search_name%>','','${courseList.subOrderItemId}');"
                          class="get-pros" 
                          title="Get <%=collegeNameDisplay%> Prospectus" >
                           Get prospectus<i class="fa fa-caret-right"></i></a>
                      </div>    
                  </c:if>
                  </c:if>    
                  <c:if test="${not empty courseList.subOrderWebsite}">              
                    <div class="prwebsite">                               
                      <a rel="nofollow" 
                          target="_blank" 
                          class="visit-web" 
                          onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${courseList.gaCollegeName}', ${courseList.websitePrice}); cpeWebClick(this,'${courseList.collegeId}','${courseList.subOrderItemId}','${courseList.networkId}','${courseList.subOrderWebsite}');" 
                          href="/degrees/visitwebredirect.html?id=${courseList.subOrderItemId}"
                          title="<%=collegeNameDisplay%> website">                                                                                       
                      Visit website<i class="fa fa-caret-right"></i></a>                             
                    </div>
                  </c:if>        
               <%}%> 
              </div>      
            </div>
            </div>
         </div>
      </div>
      <c:if test="${not empty courseList.addedToFinalChoice}">
       <c:if test="${courseList.addedToFinalChoice eq 'N'}">
            <div class="myFnChLbl" style="display:none;">                    
              ${courseList.collegeNameDisplay}#${courseList.collegeId}##${courseList.courseTitle}#${courseList.courseId}
            </div>               
       </c:if>
      </c:if>
       <!--Included parameter to dispaly add slots 03_Nov_2015 By S.Indumathi-->
       <jsp:include page="/jsp/search/include/searchDrawAdSlots.jsp"> 
         <jsp:param name="fromPage" value="clearingprovidersearchresults"/>
         <jsp:param name="position" value="${rowId.index}"/>
         <jsp:param name="totalResults" value="<%=searchhits%>"/>
         <jsp:param name="showDesktop" value="false"/>
         <jsp:param name="showBanner" value="<%=advertiserFlag%>"/>
       </jsp:include>
    </c:forEach>
  </c:if>