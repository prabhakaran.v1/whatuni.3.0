<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator" %>

<%
  String pageFrom = request.getParameter("PAGE_FROM");   
  String jsFunctionName = "submitSearchLink";
  String searchPageType = request.getParameter("searchPageType");
  searchPageType = !GenericValidator.isBlankOrNull(searchPageType)?searchPageType:"NORMAL_SEARCH";
  String brMoneySearchJs = CommonUtil.getResourceMessage("wuni.br.money.search.js", null);
%>
<% if (pageFrom != null && pageFrom.equals("KEWORD_MONEY_PAGE")){ 
    jsFunctionName = "submitSearchLink";%>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/money_search_wu509.js"></script>
<%} else if (pageFrom != null && pageFrom.equals("BROWSE_MONEY_PAGE")){ 
    jsFunctionName = "submitBrowseLink";%>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=brMoneySearchJs%>"></script>
<%}%>

<%
  String urlString = (String)request.getAttribute("URL_STRING");
  String urlString1 = urlString;
  String urlData_2 = request.getParameter("URLDATA_2");
  //int indexOfUrl = urlData_2.indexOf("page");
  String entryLevel = (String)request.getAttribute("entryLevelValue");
  //entryLevel = entryLevel.trim();
  String studeLevelId = (String)request.getAttribute("paramStudyLevelId");
  String filter = new CommonFunction().getWUSysVarValue("FILTER");

  char astarvaluereset = (String)request.getAttribute("sBoxA*Value") == null ? '0' : ((String)request.getAttribute("sBoxA*Value")).charAt(0);
  char avaluereset = (String)request.getAttribute("sBoxAValue") == null ? '0' : ((String)request.getAttribute("sBoxAValue")).charAt(0);
  char bvaluereset = (String)request.getAttribute("sBoxBValue") == null ? '0' : ((String)request.getAttribute("sBoxBValue")).charAt(0);
  char cvaluereset = (String)request.getAttribute("sBoxCValue") == null ? '0' : ((String)request.getAttribute("sBoxCValue")).charAt(0);
  char dvaluereset = (String)request.getAttribute("sBoxDValue") == null ? '0' : ((String)request.getAttribute("sBoxDValue")).charAt(0);
  char evaluereset = (String)request.getAttribute("sBoxEValue") == null ? '0' : ((String)request.getAttribute("sBoxEValue")).charAt(0);  
  char dstarvaluereset = (String)request.getAttribute("sBoxD*Value") == null ? '0' : ((String)request.getAttribute("sBoxD*Value")).charAt(0);
  char btecdvaluereset = (String)request.getAttribute("sBoxBtecDValue") == null ? '0' : ((String)request.getAttribute("sBoxBtecDValue")).charAt(0);
  char mvaluereset = (String)request.getAttribute("sBoxMValue") == null ? '0' : ((String)request.getAttribute("sBoxMValue")).charAt(0);
  char pvaluereset = (String)request.getAttribute("sBoxPValue") == null ? '0' : ((String)request.getAttribute("sBoxPValue")).charAt(0);  
  String tarifPoints = (String)request.getAttribute("tariffPointValue") == null ? "" : (String)request.getAttribute("tariffPointValue");
  String collegeCount = (String)request.getAttribute("collegeCount");
  String searchCollegeId = request.getAttribute("uniSearchCollegeId") != null ?(String)request.getAttribute("uniSearchCollegeId"):"";
  String searchCollegeName = (String)request.getAttribute("uniSearchCollegeName");
  String entryGradeFilter = (String)request.getAttribute("entryGradeFilter");
  String searchType = (String)request.getAttribute("searchType");
  String fromGradePopup = (String)request.getAttribute("fromGradePopup");
%>
  <input type="hidden" id="searchtype" name="searchType" value="<%=searchPageType%>"/>
  <%if(filter.equalsIgnoreCase("Y")){%>
   <%if(studeLevelId.equalsIgnoreCase("m")){%>
    <%--<%if (pageFrom != null && pageFrom.equals("KEWORD_MONEY_PAGE")){%>
    </div>
    <%}%>--%><%-- Commented By Indumathi.S for Rel Nov-03-15 --%>
    <div class="gr_filt_cont">
							<%--	<img id="hi_img" style="display:none;" class="gr_filt_img" src="<%=CommonUtil.getImgPath("/wu-cont/images/hi_lite.png", 1)%>" width="754" height="122"> --%>
      </div>
    <div class="pys" style="position:relative;">
      <%--<a id="remClass" class="r_filt_clse" onclick="removeGradePopup();">
          <i class="fa fa-times"></i>
        </a>--%>
      <%-- Added grade filter popup code for 11_Aug_2015 By Thiyagu G. --%>      
      <div id="dynamicDiv"></div>
    <form id="filterEntryPoint" method="post" action="" onsubmit="javascript:return customAlertPopup('grade_search', '<%=fromGradePopup%>');" >
        <input type="hidden" id="optionId" name="optionId" value="<%=request.getAttribute("optionId")%>"/>
        <input type="hidden" id="ISEXISTSGRADE" name="ISEXISTSGRADE" value="<%=request.getAttribute("ISEXISTSGRADE")%>"/>
        <input type="hidden" id="updategradesflg" name="updategradesflg" value="NO"/>  
        <input type="hidden" id="loginGradsSave" name="logingGradesSave"/>
        <div class="pys-lt"></div> 
        <div class="pys-rt">
        <div class="hdiv">
             <span class="per_srch">Personalise your search</span> <%-- Added grade filter for responsive view code for 03_NOV_2015 by Prabha --%>
            <span class="fleft"><strong class="fleft">Entry Requirements</strong>
                
            </span>
             <span class="fleft ispn1">
              <span class="help fleft ucs">
                  <span class="calc"></span>
                  <a onclick="openLightBox('ucas');">UCAS tariff calculator</a>
                <%--<div class="help-blk hwid">
                  <div class="help-txt">
                      <p>Minimum UCAS Tariff points or grades you need to apply for this course.</p>
		                    <p><jsp:include page="/help/TariffInformation.jsp" /></p>
                    </div>
                  </div>--%>
             </span>
            </span>
            <span class="fleft ispn2">
                <strong id="pttxt" class="fleft">Your grades/expected grades?</strong>
              
            </span>
        </div>
        <%if(entryLevel != null){%>
           <div class="srch-bar">
            <div class="hdiv hlv">
                <div class="shold">
                    <span id="sBox1" class="ts-select pfs"><%=entryLevel%></span>
                    <select id="selBox1" class="iselect pfs" onchange="changeEntryPoints(this,'<%=entryLevel%>', '<%=astarvaluereset%>', '<%=avaluereset%>', '<%=bvaluereset%>', '<%=cvaluereset%>', '<%=dvaluereset%>', '<%=evaluereset%>', '<%=dstarvaluereset%>', '<%=btecdvaluereset%>', '<%=mvaluereset%>', '<%=pvaluereset%>', '<%=tarifPoints%>'); changeFunc1(this,'sBox1');">
                        <option value="A-levels" <%if(entryLevel.equalsIgnoreCase("A-levels")){%>selected="selected"<%}%> >A-levels</option>
                        <option value="SQA Highers" <%if(entryLevel.equalsIgnoreCase("SQA Highers")){%>selected="selected"<%}%> >SQA Highers</option>
                        <option value="SQA Advanced Highers" <%if(entryLevel.equalsIgnoreCase("SQA Advanced Highers")){%>selected="selected"<%}%> >SQA Advanced Highers</option>
                        <option value="BTEC" <%if(entryLevel.equalsIgnoreCase("BTEC")){%>selected="selected"<%}%> >BTEC</option>
                        <option value="Tariff Points" <%if(entryLevel.equalsIgnoreCase("Tariff Points")){%>selected="selected"<%}%> >Tariff Points</option>                        
                    </select>
                </div>
                <div class="grds">
                    <span id="yr_exp2" class="per_srch yr_exp">Your expected grades?</span>
                    <%if(entryLevel.equalsIgnoreCase("A-levels")){%>
                        <span class="gd" id="A*span">A*</span>
                        <div class="shold" id="A*">
                            <span id="sBoxA*" class="ts-select"><%
                            if(request.getAttribute("sBoxA*Value") != null ){out.println(((String)request.getAttribute("sBoxA*Value")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxA*" class="iselect" onchange="changeFunc(this,'sBoxA*');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxA*Value")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxA*Value")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxA*Value")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxA*Value")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                <option value="4" <%if(((String)request.getAttribute("sBoxA*Value")).charAt(0)=='4'){%>selected="selected"<%}%>>4</option>
                                <option value="5" <%if(((String)request.getAttribute("sBoxA*Value")).charAt(0)=='5'){%>selected="selected"<%}%>>5</option>
                            </select>
                        </div>
                        <span class="gd" id="Aspan">A</span>
                        <div class="shold" id="A">
                            <span id="sBoxA" class="ts-select"><%if(request.getAttribute("sBoxAValue") != null ){out.println(((String)request.getAttribute("sBoxAValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxA" class="iselect" onchange="changeFunc(this,'sBoxA');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                <option value="4" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='4'){%>selected="selected"<%}%>>4</option>
                                <option value="5" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='5'){%>selected="selected"<%}%>>5</option>
                            </select>
                        </div>
                        <span class="gd" id="Bspan">B</span>
                        <div class="shold" id="B">
                            <span id="sBoxB" class="ts-select"><%if(request.getAttribute("sBoxBValue") != null ){out.println(((String)request.getAttribute("sBoxBValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxB" class="iselect" onchange="changeFunc(this,'sBoxB');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                <option value="4" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='4'){%>selected="selected"<%}%>>4</option>
                                <option value="5" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='5'){%>selected="selected"<%}%>>5</option>
                            </select>
                        </div>
                        <span class="gd" id="Cspan">C</span>
                        <div class="shold" id="C">
                            <span id="sBoxC" class="ts-select"><%if(request.getAttribute("sBoxCValue") != null ){out.println(((String)request.getAttribute("sBoxCValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxC" class="iselect" onchange="changeFunc(this,'sBoxC');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                <option value="4" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='4'){%>selected="selected"<%}%>>4</option>
                                <option value="5" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='5'){%>selected="selected"<%}%>>5</option>
                            </select>
                        </div>
                        <span class="gd" id="Dspan">D</span>
                        <div class="shold" id="D">
                            <span id="sBoxD" class="ts-select"><%if(request.getAttribute("sBoxDValue") != null ){out.println(((String)request.getAttribute("sBoxDValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxD" class="iselect" onchange="changeFunc(this,'sBoxD');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                <option value="4" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='4'){%>selected="selected"<%}%>>4</option>
                                <option value="5" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='5'){%>selected="selected"<%}%>>5</option>
                            </select>
                        </div>
                        <span class="gd" id="Espan">E</span> 
                        <div class="shold" id="E">
                            <span id="sBoxE" class="ts-select"><%if(request.getAttribute("sBoxEValue") != null ){out.println(((String)request.getAttribute("sBoxEValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxE" class="iselect" onchange="changeFunc(this,'sBoxE');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxEValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxEValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxEValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxEValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                <option value="4" <%if(((String)request.getAttribute("sBoxEValue")).charAt(0)=='4'){%>selected="selected"<%}%>>4</option>
                                <option value="5" <%if(((String)request.getAttribute("sBoxEValue")).charAt(0)=='5'){%>selected="selected"<%}%>>5</option>
                            </select>
                        </div>                        
                        <span class="gd" id="D*span" style="display:none;">D*</span>
                        <div class="shold" id="D*" style="display:none;">
                            <span id="sBoxD*" class="ts-select">0</span>
                            <select id="selBoxD*" class="iselect" onchange="changeFunc(this,'sBoxD*');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>
                        <span class="gd" id="BtecDspan" style="display:none;">D</span>
                        <div class="shold" id="BtecD" style="display:none;">
                            <span id="sBoxBtecD" class="ts-select">0</span>
                            <select id="selBoxBtecD" class="iselect" onchange="changeFunc(this,'sBoxBtecD');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>
                        <span class="gd" id="Mspan" style="display:none;">M</span>
                        <div class="shold" id="M" style="display:none;">
                            <span id="sBoxM" class="ts-select">0</span>
                            <select id="selBoxM" class="iselect" onchange="changeFunc(this,'sBoxM');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>
                        <span class="gd" id="Pspan" style="display:none;">P</span>
                        <div class="shold" id="P" style="display:none;">
                            <span id="sBoxP" class="ts-select">0</span>
                            <select id="selBoxP" class="iselect" onchange="changeFunc(this,'sBoxP');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>
                        <input type="text" id="tp" class="ts-txt" style="display:none;" autocomplete="off" maxlength="3" onclick="onClickTarif();"/>
                    <%}%>
                    <%if(entryLevel.equalsIgnoreCase("SQA Highers") || entryLevel.equalsIgnoreCase("SQA Advanced Highers")){%>
                        <span class="gd" id="A*span" style="display:none;">A*</span>
                        <div class="shold" id="A*" style="display:none;">
                            <span id="sBoxA*" class="ts-select">0</span>
                            <select id="selBoxA*" class="iselect" onchange="changeFunc(this,'sBoxA*');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select> 
                        </div>
                        <span class="gd" id="Aspan">A</span>
                        <div class="shold" id="A">
                            <span id="sBoxA" class="ts-select"><%
                            if(request.getAttribute("sBoxAValue") != null ){out.println(((String)request.getAttribute("sBoxAValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxA" class="iselect" onchange="changeFunc(this,'sBoxA');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                <option value="4" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='4'){%>selected="selected"<%}%>>4</option>
                                <option value="5" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='5'){%>selected="selected"<%}%>>5</option>
                                <option value="6" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='6'){%>selected="selected"<%}%>>6</option>
                                <option value="7" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='7'){%>selected="selected"<%}%>>7</option>
                                <option value="8" <%if(((String)request.getAttribute("sBoxAValue")).charAt(0)=='8'){%>selected="selected"<%}%>>8</option>
                            </select>
                        </div>
                        <span class="gd" id="Bspan">B</span>
                        <div class="shold" id="B">
                            <span id="sBoxB" class="ts-select"><%if(request.getAttribute("sBoxBValue") != null ){out.println(((String)request.getAttribute("sBoxBValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxB" class="iselect" onchange="changeFunc(this,'sBoxB');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                <option value="4" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='4'){%>selected="selected"<%}%>>4</option>
                                <option value="5" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='5'){%>selected="selected"<%}%>>5</option>
                                <option value="6" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='6'){%>selected="selected"<%}%>>6</option>
                                <option value="7" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='7'){%>selected="selected"<%}%>>7</option>
                                <option value="8" <%if(((String)request.getAttribute("sBoxBValue")).charAt(0)=='8'){%>selected="selected"<%}%>>8</option>
                            </select>
                        </div>
                        <span class="gd" id="Cspan">C</span>
                        <div class="shold" id="C">
                            <span id="sBoxC" class="ts-select"><%if(request.getAttribute("sBoxCValue") != null ){out.println(((String)request.getAttribute("sBoxCValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxC" class="iselect" onchange="changeFunc(this,'sBoxC');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                <option value="4" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='4'){%>selected="selected"<%}%>>4</option>
                                <option value="5" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='5'){%>selected="selected"<%}%>>5</option>
                                <option value="6" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='6'){%>selected="selected"<%}%>>6</option>
                                <option value="7" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='7'){%>selected="selected"<%}%>>7</option>
                                <option value="8" <%if(((String)request.getAttribute("sBoxCValue")).charAt(0)=='8'){%>selected="selected"<%}%>>8</option>
                            </select>
                        </div>
                        <span class="gd" id="Dspan">D</span>
                        <div class="shold" id="D">
                            <span id="sBoxD" class="ts-select"><%if(request.getAttribute("sBoxDValue") != null ){out.println(((String)request.getAttribute("sBoxDValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxD" class="iselect" onchange="changeFunc(this,'sBoxD');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                <option value="4" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='4'){%>selected="selected"<%}%>>4</option>
                                <option value="5" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='5'){%>selected="selected"<%}%>>5</option>
                                <option value="6" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='6'){%>selected="selected"<%}%>>6</option>
                                <option value="7" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='7'){%>selected="selected"<%}%>>7</option>
                                <option value="8" <%if(((String)request.getAttribute("sBoxDValue")).charAt(0)=='8'){%>selected="selected"<%}%>>8</option>
                            </select>
                        </div>
                        <span class="gd" id="Espan" style="display:none;">E</span>
                        <div class="shold" id="E" style="display:none;">
                            <span id="sBoxE" class="ts-select">0</span>
                            <select id="selBoxE" class="iselect" onchange="changeFunc(this,'sBoxD');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>                        
                        <span class="gd" id="D*span" style="display:none;">D*</span>
                        <div class="shold" id="D*" style="display:none;">
                            <span id="sBoxD*" class="ts-select">0</span>
                            <select id="selBoxD*" class="iselect" onchange="changeFunc(this,'sBoxD*');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>
                        <span class="gd" id="BtecDspan" style="display:none;">D</span>
                        <div class="shold" id="BtecD" style="display:none;">
                            <span id="sBoxBtecD" class="ts-select">0</span>
                            <select id="selBoxBtecD" class="iselect" onchange="changeFunc(this,'sBoxBtecD');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>
                        <span class="gd" id="Mspan" style="display:none;">M</span>
                        <div class="shold" id="M" style="display:none;">
                            <span id="sBoxM" class="ts-select">0</span>
                            <select id="selBoxM" class="iselect" onchange="changeFunc(this,'sBoxM');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>
                        <span class="gd" id="Pspan" style="display:none;">P</span>
                        <div class="shold" id="P" style="display:none;">
                            <span id="sBoxP" class="ts-select">0</span>
                            <select id="selBoxP" class="iselect" onchange="changeFunc(this,'sBoxP');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>                        
                        <input type="text" id="tp" class="ts-txt" style="display:none;" autocomplete="off" maxlength="3" onclick="onClickTarif();"/>
                    <%}%>                    
                    <%if(entryLevel.equalsIgnoreCase("BTEC")){%>                    
                        <span class="gd" id="A*span" style="display:none;">A*</span>
                        <div class="shold" id="A*" style="display:none;">
                            <span id="sBoxA*" class="ts-select">0</span>
                            <select id="selBoxA*" class="iselect" onchange="changeFunc(this,'sBoxA*');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <span class="gd" id="Aspan" style="display:none;">A</span>
                        <div class="shold" id="A" style="display:none;">
                            <span id="sBoxA" class="ts-select">0</span>
                            <select id="selBoxA" class="iselect" onchange="changeFunc(this,'sBoxA');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <span class="gd" id="Bspan" style="display:none;">B</span>
                        <div class="shold" id="B" style="display:none;">
                            <span id="sBoxB" class="ts-select">0</span>
                            <select id="selBoxB" class="iselect" onchange="changeFunc(this,'sBoxB');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <span class="gd" id="Cspan" style="display:none;">C</span>
                        <div class="shold" id="C" style="display:none;">
                            <span id="sBoxC" class="ts-select">0</span>
                            <select id="selBoxC" class="iselect" onchange="changeFunc(this,'sBoxC');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <span class="gd" id="Dspan" style="display:none;">D</span>
                        <div class="shold" id="D" style="display:none;">
                            <span id="sBoxD" class="ts-select">0</span>
                            <select id="selBoxD" class="iselect" onchange="changeFunc(this,'sBoxD');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <span class="gd" id="Espan" style="display:none;">E</span> 
                        <div class="shold" id="E" style="display:none;">
                            <span id="sBoxE" class="ts-select">0</span>
                            <select id="selBoxE" class="iselect" onchange="changeFunc(this,'sBoxE');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <span class="gd" id="D*span">D*</span>
                        <div class="shold" id="D*">
                            <span id="sBoxD*" class="ts-select"><%
                            if(request.getAttribute("sBoxD*Value") != null ){out.println(((String)request.getAttribute("sBoxD*Value")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxD*" class="iselect" onchange="changeFunc(this,'sBoxD*');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxD*Value")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxD*Value")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxD*Value")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxD*Value")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                
                            </select>                            
                        </div>
                        <span class="gd" id="BtecDspan">D</span>
                        <div class="shold" id="BtecD">
                            <span id="sBoxBtecD" class="ts-select"><%
                            if(request.getAttribute("sBoxBtecDValue") != null ){out.println(((String)request.getAttribute("sBoxBtecDValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxBtecD" class="iselect" onchange="changeFunc(this,'sBoxBtecD');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxBtecDValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxBtecDValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxBtecDValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxBtecDValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                
                            </select>
                        </div>
                        <span class="gd" id="Mspan">M</span>
                        <div class="shold" id="M"> 
                            <span id="sBoxM" class="ts-select"><%
                            if(request.getAttribute("sBoxMValue") != null ){out.println(((String)request.getAttribute("sBoxMValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxM" class="iselect" onchange="changeFunc(this,'sBoxM');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxMValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxMValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxMValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxMValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>
                                
                            </select>
                        </div>
                        <span class="gd" id="Pspan">P</span>
                        <div class="shold" id="P">
                            <span id="sBoxP" class="ts-select"><%
                            if(request.getAttribute("sBoxPValue") != null ){out.println(((String)request.getAttribute("sBoxPValue")).charAt(0));}else{out.println("0");}%></span>
                            <select id="selBoxP" class="iselect" onchange="changeFunc(this,'sBoxP');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0" <%if(((String)request.getAttribute("sBoxPValue")).charAt(0)=='0'){%>selected="selected"<%}%> >0</option>
                                <option value="1" <%if(((String)request.getAttribute("sBoxPValue")).charAt(0)=='1'){%>selected="selected"<%}%>>1</option>
                                <option value="2" <%if(((String)request.getAttribute("sBoxPValue")).charAt(0)=='2'){%>selected="selected"<%}%>>2</option>
                                <option value="3" <%if(((String)request.getAttribute("sBoxPValue")).charAt(0)=='3'){%>selected="selected"<%}%>>3</option>                                
                            </select>
                        </div>
                        <input type="text" id="tp" class="ts-txt" style="display:none;" maxlength="3" autocomplete="off" onclick="onClickTarif();"/>
                    <%}%>                    
                    <%if(entryLevel.equalsIgnoreCase("Tariff Points")){
                        String tariffPoints = (String)request.getAttribute("tariffPointValue");
                    %>
                        <span class="gd" id="A*span" style="display:none;">A*</span>
                        <div class="shold" id="A*" style="display:none;">
                            <span id="sBoxA*" class="ts-select">0</span>
                            <select id="selBoxA*" class="iselect" onchange="changeFunc(this,'sBoxA*');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <span class="gd" id="Aspan" style="display:none;">A</span>
                        <div class="shold" id="A" style="display:none;">
                            <span id="sBoxA" class="ts-select">0</span>
                            <select id="selBoxA" class="iselect" onchange="changeFunc(this,'sBoxA');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <span class="gd" id="Bspan" style="display:none;">B</span>
                        <div class="shold" id="B" style="display:none;">
                            <span id="sBoxB" class="ts-select">0</span>
                            <select id="selBoxB" class="iselect" onchange="changeFunc(this,'sBoxB');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <span class="gd" id="Cspan" style="display:none;">C</span>
                        <div class="shold" id="C" style="display:none;">
                            <span id="sBoxC" class="ts-select">0</span>
                            <select id="selBoxC" class="iselect" onchange="changeFunc(this,'sBoxC');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <span class="gd" id="Dspan" style="display:none;">D</span>
                        <div class="shold" id="D" style="display:none;">
                            <span id="sBoxD" class="ts-select">0</span>
                            <select id="selBoxD" class="iselect" onchange="changeFunc(this,'sBoxD');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>
                        <span class="gd" id="Espan" style="display:none;">E</span> 
                        <div class="shold" id="E" style="display:none;">
                            <span id="sBoxE" class="ts-select">0</span>
                            <select id="selBoxE" class="iselect" onchange="changeFunc(this,'sBoxE');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                            </select>
                        </div>                        
                        <span class="gd" id="D*span" style="display:none;">D*</span>
                        <div class="shold" id="D*" style="display:none;">
                            <span id="sBoxD*" class="ts-select">0</span>
                            <select id="selBoxD*" class="iselect" onchange="changeFunc(this,'sBoxD*');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>
                        <span class="gd" id="BtecDspan" style="display:none;">D</span>
                        <div class="shold" id="BtecD" style="display:none;">
                            <span id="sBoxBtecD" class="ts-select">0</span>
                            <select id="selBoxBtecD" class="iselect" onchange="changeFunc(this,'sBoxBtecD');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>
                        <span class="gd" id="Mspan" style="display:none;">M</span>
                        <div class="shold" id="M" style="display:none;">
                            <span id="sBoxM" class="ts-select">0</span>
                            <select id="selBoxM" class="iselect" onchange="changeFunc(this,'sBoxM');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>
                        <span class="gd" id="Pspan" style="display:none;">P</span>
                        <div class="shold" id="P" style="display:none;">
                            <span id="sBoxP" class="ts-select">0</span>
                            <select id="selBoxP" class="iselect" onchange="changeFunc(this,'sBoxP');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>                              
                            </select>
                        </div>                        
                        <input type="text" id="tp" class="ts-txt" value="<%=tariffPoints%>" maxlength="3" autocomplete="off" onclick="onClickTarif();"/>
                    <%}%>                    
                    <input type="submit" value="Update" disabled="true" id="upt" class="updt" />
                </div>
            </div>        
          </div>
        <%}else{%>
          <div class="srch-bar">
            <div class="hdiv hlv">
                <div class="shold">
                    <span id="sBox1" class="ts-select pfs">A-levels</span>
                    <select id="selBox1" class="iselect pfs" onchange="changeEntryPoints(this); changeFunc1(this,'sBox1');">
                        <option value="A-levels">A-levels</option>
                        <option value="SQA Highers">SQA Highers</option>
                        <option value="SQA Advanced Highers">SQA Advanced Highers</option>
                        <option value="BTEC">BTEC</option>
                        <option value="Tariff Points">Tariff Points</option>                        
                    </select>
                </div>
                <div class="grds">
                    <span id="yr_exp1" class="per_srch yr_exp">Your expected grades?</span>
                    <span class="gd" id="A*span">A*</span>
                    <div class="shold" id="A*">
                        <span id="sBoxA*" class="ts-select">0</span>
                        <select id="selBoxA*" class="iselect" onchange="changeFunc(this,'sBoxA*');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <span class="gd" id="Aspan">A</span>
                    <div class="shold" id="A">
                        <span id="sBoxA" class="ts-select">0</span>
                        <select id="selBoxA" class="iselect" onchange="changeFunc(this,'sBoxA');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <span class="gd" id="Bspan">B</span>
                    <div class="shold" id="B">
                        <span id="sBoxB" class="ts-select">0</span>
                        <select id="selBoxB" class="iselect" onchange="changeFunc(this,'sBoxB');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <span class="gd" id="Cspan">C</span>
                    <div class="shold" id="C">
                        <span id="sBoxC" class="ts-select">0</span>
                        <select id="selBoxC" class="iselect" onchange="changeFunc(this,'sBoxC');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <span class="gd" id="Dspan">D</span>
                    <div class="shold" id="D">
                        <span id="sBoxD" class="ts-select">0</span>
                        <select id="selBoxD" class="iselect" onchange="changeFunc(this,'sBoxD');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <span class="gd" id="Espan">E</span> 
                    <div class="shold" id="E">
                        <span id="sBoxE" class="ts-select">0</span>
                        <select id="selBoxE" class="iselect" onchange="changeFunc(this,'sBoxE');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>                    
                    <span class="gd" id="D*span" style="display:none;">D*</span> 
                    <div class="shold" id="D*" style="display:none;">
                        <span id="sBoxD*" class="ts-select">0</span>
                        <select id="selBoxD*" class="iselect" onchange="changeFunc(this,'sBoxD*');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>                            
                        </select>
                    </div>
                     <span class="gd" id="BtecDspan" style="display:none;">D</span> 
                    <div class="shold" id="BtecD" style="display:none;">
                        <span id="sBoxBtecD" class="ts-select">0</span>
                        <select id="selBoxBtecD" class="iselect" onchange="changeFunc(this,'sBoxBtecD');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>                            
                        </select>
                    </div>
                     <span class="gd" id="Mspan" style="display:none;">M</span> 
                    <div class="shold" id="M" style="display:none;">
                        <span id="sBoxM" class="ts-select">0</span>
                        <select id="selBoxM" class="iselect" onchange="changeFunc(this,'sBoxM');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>                            
                        </select>
                    </div>
                     <span class="gd" id="Pspan" style="display:none;">P</span> 
                    <div class="shold" id="P" style="display:none;">
                        <span id="sBoxP" class="ts-select">0</span>
                        <select id="selBoxP" class="iselect" onchange="changeFunc(this,'sBoxP');" onkeypress="javascript:if(event.keyCode==13){return returnKeyFormSub()}">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>                            
                        </select>
                    </div>                    
                    <input type="text" id="tp" class="ts-txt" style="display:none;" maxlength="3" autocomplete="off" onclick="onClickTarif();"/>
                    <%--<input type="submit" class="updt" value="update" id="upt"  class="updt" />--%>
                    <input type="submit" value="Update" disabled="true" id="upt" class="updt" />
                </div>
            </div>
          </div> 
        <%}%>
         </div>
         <div id="grdeErr" class="errormessage" style="display:none;"></div>
        </form>
        </div>
    <%}}%>
    
<input type="hidden" name="astarvalue" id="astarvalue" value="<%=astarvaluereset%>"/>
<input type="hidden" name="avalue" id="avalue" value="<%=avaluereset%>" />
<input type="hidden" name="bvalue" id="bvalue" value="<%=bvaluereset%>" />
<input type="hidden" name="cvalue" id="cvalue" value="<%=cvaluereset%>" />
<input type="hidden" name="dvalue" id="dvalue" value="<%=dvaluereset%>" />
<input type="hidden" name="evalue" id="evalue" value="<%=evaluereset%>" />
<input type="hidden" name="dstarvalue" id="dstarvalue" value="<%=dstarvaluereset%>"/>
<input type="hidden" name="btecdvalue" id="btecdvalue" value="<%=btecdvaluereset%>" />
<input type="hidden" name="mvalue" id="mvalue" value="<%=mvaluereset%>" />
<input type="hidden" name="pvalue" id="pvalue" value="<%=pvaluereset%>" />
<input type="hidden" name="tariffPointValue" id="tariffPointValue" value="<%=tarifPoints%>" />
<input type="hidden" name="GAgradeFilter" id="GAgradeFilter" value=""/>
<c:if test="${not empty ISEXISTSGRADE}">
 <c:if test="${ISEXISTSGRADE eq YES}">
   <script type="text/javascript">
    document.getElementById("upt").disabled=false;
    document.getElementById("upt").className='updt_active';   
   </script>
 </c:if>
</c:if>