<%--
  * @purpose  This jsp is grade filter light box page
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 3-Nov-2015 Prabhakaran V.                546     Modify t he grade filter codes for light box view..                546
  * *************************************************************************************************************************
--%>
<div class="comLgh" id="lbDivNew">
  <div class="fcls nw">
    <a onclick="closeGradePopup();">
      <i class="fa fa-times"></i>
    </a>
  </div>
  <div style="display: block;" class="pform nlr bgw opday fl" id="grPerson">
    <div class="reg-frm">
	  <div class="lgn_lbx"><jsp:include page="/jsp/search/include/gradeFilter.jsp"/></div>
    </div>
  </div>
</div>