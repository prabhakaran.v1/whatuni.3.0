<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page import="org.apache.commons.validator.GenericValidator, mobile.util.MobileUtils,WUI.utilities.SearchUtilities,WUI.utilities.CommonUtil"%>

<%
  String urlString = !GenericValidator.isBlankOrNull((String)request.getAttribute("sortingUrl")) ? (String)request.getAttribute("sortingUrl") : null;
  String queryStringValues = (String)request.getAttribute("queryStringValues");
  urlString = !GenericValidator.isBlankOrNull(urlString) && (urlString.lastIndexOf("&sort=") == -1) ? urlString.trim()+"&sort=r" : urlString;
  String sortBy = (String)request.getAttribute("sortByValue");
  String studyLevel = (String)request.getAttribute("paramStudyLevelId");  
  studyLevel = !GenericValidator.isBlankOrNull(studyLevel)? studyLevel :"";
  String pagenochar = null;
  CommonUtil util = new CommonUtil();
  SearchUtilities searchUtilities = new SearchUtilities();
  String pageNumberParameter = "pageno";
  if(!GenericValidator.isBlankOrNull(queryStringValues)){
	    if(queryStringValues.contains(pageNumberParameter)){//Added this for pagination removal in URL by Hema.S on 12_FEB_2019_REL
	      if(queryStringValues.indexOf(pageNumberParameter) == 0){
	        pagenochar= queryStringValues.substring(queryStringValues.indexOf(pageNumberParameter),queryStringValues.indexOf(pageNumberParameter)+("pageno=".length()+(request.getParameter(pageNumberParameter).length()+1)));  	
	      }else{
	        pagenochar= queryStringValues.substring(queryStringValues.indexOf(pageNumberParameter)-1,queryStringValues.indexOf(pageNumberParameter)+("pageno=".length()+request.getParameter(pageNumberParameter).length()));
	      }
	    }
	  }
  //
  //
  String defineMostInfo = "r-or".equalsIgnoreCase(sortBy) ? "Overall rating": "Most info";//Added by Indumathi Nov-03-15
  String urlForMostInfo = "";
  String urlForCourseRanking = "";
  String urlForEnryRequirements = "";  
  String urlForEmploymentRate = "";
  String urlR_OR = "";
  String urlR_CLR = "";
  String urlR_AR = "";
  String urlR_CYLR = "";
  String urlR_UFR = "";
  String urlR_CSR = "";
  String urlR_SUR = "";
  String urlR_ECR = "";
  String urlR_JPR = "";
  String urlR_RR = "";  //
  String classNameMI = "";
  String classNameTR = "";
  String classNameER = "";
  String classNameEMR = "";
  String classReviews = "";
  String classNameLIMI = "";
  String classNameLITR = "";
  String classNameLIER = "";
  String classNameLIEMR = "";
  String classLIReviews = "";
  String courseViewUrl = null;
  String substr = null;
  String sortByValue = "&sort="+sortBy;
  String qString = (String)request.getAttribute("queryStr");         
         qString = !GenericValidator.isBlankOrNull(qString) ? "?" + qString.trim() :"";
  if(!GenericValidator.isBlankOrNull(urlString)){
   if(urlString.lastIndexOf(sortByValue) != -1 ) {
      urlForMostInfo =  urlString.lastIndexOf("&sort=r") != -1 ? urlString.replaceAll(sortByValue, "") : urlString.replaceAll(sortByValue, "");
       classNameMI =  urlString.lastIndexOf("&sort=r") != -1 ? "act nw" : "";
       classNameLIMI =  urlString.lastIndexOf("&sort=r") != -1 ? "mact" : "mstinf";
      
      urlForCourseRanking =  urlString.lastIndexOf("&sort=crh") != -1 ? urlString.replaceAll(sortByValue, "&sort=crl") : urlString.replaceAll(sortByValue, "&sort=crh");
      classNameTR =  urlString.lastIndexOf("&sort=crl") != -1 ? "act1" : "";
      classNameTR =  urlString.lastIndexOf("&sort=crh") != -1 ? "act" : classNameTR;
      classNameLITR =  urlString.lastIndexOf("&sort=crl") != -1 ? "mact" : "cournk";
      classNameLITR =  urlString.lastIndexOf("&sort=crh") != -1 ? "mact" : classNameLITR;
      
      urlForEnryRequirements = urlString.lastIndexOf("&sort=entd") != -1 ? urlString.replaceAll(sortByValue, "&sort=enta") : urlString.replaceAll(sortByValue, "&sort=entd");
      classNameER =  urlString.lastIndexOf("&sort=entd") != -1 ? "act" : "";
      classNameER =  urlString.lastIndexOf("&sort=enta") != -1 ? "act1" : classNameER;
      classNameLIER =  urlString.lastIndexOf("&sort=entd") != -1 ? "mact" : "entreq";
      classNameLIER =  urlString.lastIndexOf("&sort=enta") != -1 ? "mact" : classNameLIER;
        
      urlForEmploymentRate = urlString.lastIndexOf("&sort=empd") != -1 ? urlString.replaceAll(sortByValue, "&sort=empa") : urlString.replaceAll(sortByValue, "&sort=empd");
      classNameEMR =  urlString.lastIndexOf("&sort=empd") != -1 ? "act" : "";
      classNameEMR =  urlString.lastIndexOf("&sort=empa") != -1 ? "act1" : classNameEMR;
      classNameLIEMR =  urlString.lastIndexOf("&sort=empa") != -1 ? "mact" : "entreq";
      classNameLIEMR =  urlString.lastIndexOf("&sort=empd") != -1 ?  "mact" : classNameLIEMR;     
      classReviews = urlString.lastIndexOf("&sort=r-") != -1 ? "sturat nws1": "sturat nws";      
      
      urlR_OR =  urlString.replaceAll(sortByValue, "&sort=r-or");
      urlR_CLR =  urlString.replaceAll(sortByValue, "&sort=r-clr");
      urlR_AR =  urlString.replaceAll(sortByValue, "&sort=r-ar");
      urlR_CYLR = urlString.replaceAll(sortByValue, "&sort=r-cylr");
      urlR_UFR = urlString.replaceAll(sortByValue, "&sort=r-ufr");
      urlR_CSR =  urlString.replaceAll(sortByValue, "&sort=r-csr");
      urlR_SUR =  urlString.replaceAll(sortByValue, "&sort=r-sur");
      urlR_ECR = urlString.replaceAll(sortByValue, "&sort=r-ecr");
      urlR_JPR =  urlString.replaceAll(sortByValue, "&sort=r-jpr");
      urlR_RR =  urlString.replaceAll(sortByValue, "&sort=r-rr"); 
   } }
   /*urlForMostInfo = "/degrees"+ urlForMostInfo ;
   urlForCourseRanking = "/degrees"+ urlForCourseRanking ;
   urlForEnryRequirements = "/degrees"+ urlForEnryRequirements ;
   urlForEmploymentRate = "/degrees"+ urlForEmploymentRate ;
   urlR_OR = "/degrees"+ urlR_OR;
   urlR_CLR = "/degrees"+ urlR_CLR;
   urlR_AR = "/degrees"+ urlR_AR;
   urlR_CYLR = "/degrees"+ urlR_CYLR;
   urlR_UFR = "/degrees"+ urlR_UFR;
   urlR_CSR = "/degrees"+ urlR_CSR;
   urlR_SUR = "/degrees"+ urlR_SUR;
   urlR_ECR = "/degrees"+ urlR_ECR;
   urlR_JPR = "/degrees"+ urlR_JPR;
   urlR_RR = "/degrees"+ urlR_RR;*/
   if(!GenericValidator.isBlankOrNull(pagenochar)) {
     urlForMostInfo = util.pageNumberEmpty(urlForMostInfo,pagenochar);
     urlForCourseRanking = util.pageNumberEmpty(urlForCourseRanking,pagenochar);
     urlForEnryRequirements = util.pageNumberEmpty(urlForEnryRequirements,pagenochar);
     urlForEmploymentRate = util.pageNumberEmpty(urlForEmploymentRate,pagenochar);
     urlR_OR = util.pageNumberEmpty(urlR_OR,pagenochar);
     urlR_CLR = util.pageNumberEmpty(urlR_CLR,pagenochar);
     urlR_AR = util.pageNumberEmpty(urlR_AR,pagenochar);
     urlR_CYLR = util.pageNumberEmpty(urlR_CYLR,pagenochar);
     urlR_UFR = util.pageNumberEmpty(urlR_UFR,pagenochar);
     urlR_CSR = util.pageNumberEmpty(urlR_CSR,pagenochar);
     urlR_SUR = util.pageNumberEmpty(urlR_SUR,pagenochar);
     urlR_ECR = util.pageNumberEmpty(urlR_ECR,pagenochar);
     urlR_JPR = util.pageNumberEmpty(urlR_JPR,pagenochar);
     urlR_RR = util.pageNumberEmpty(urlR_RR,pagenochar);
   }
   String collegeCount = request.getSession().getAttribute("basketpodcollegecount") != null ? String.valueOf(request.getSession().getAttribute("basketpodcollegecount")) : "0";   
   String srchPrefix = "";
   String srchType = request.getParameter("srchType");
   if(srchType!=null && !"".equals(srchType)){
     if("clearing".equals(srchType)){
      srchPrefix = "clearing-";
     }
   }
   boolean mobileFlag = new MobileUtils().userAgentCheck(request);
%>
  <div class="srt">      
      <ul class="cf">
        <li class="sort_by">SORT BY:</li>
         <%if(!mobileFlag){%> 
        <li class="<%=classNameLIMI%>"> <a onclick="searchEventTracking('sort','<%=srchPrefix%>mostinfo','clicked');" href="<%=urlForMostInfo%>" class="<%=classNameMI%>">Most info</a></li>
        <li class="<%=classNameLITR%>"><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>course-ranking','clicked');" href="<%=urlForCourseRanking%>" class="<%=classNameTR%>">CompUniGuide ranking</a> <%--Changed ranking lable from Times to CUG for 08_MAR_2016 By Thiyagu G--%></li>
        <%if(!GenericValidator.isBlankOrNull(studyLevel) &&  !("L".equalsIgnoreCase(studyLevel))){classReviews = urlString.lastIndexOf("/r-") != -1 ? "sturat nw1": "sturat nw"; %>
          <li class="<%=classNameLIER%>"><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>entry-requirements','clicked');" href="<%=urlForEnryRequirements%>" class="<%=classNameER%>">Entry requirements</a></li>
        <%}%>
        <li class="<%=classNameLIEMR%> emprate"><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>employment-rate','clicked');" href="<%=urlForEmploymentRate%>" class="<%=classNameEMR%>">Employment rate</a></li>
       
        <li class="<%=classReviews%>" id="stdrank">       
           <a class="srt1">Student ranking</a>           
            <ul id="abcr11" class="strt">
                <li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>student-rating','overall rating');" href="<%=urlR_OR%>">Overall rating</a></li>
                <li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>student-rating','course & lecturers');" href="<%=urlR_CLR%>">Course & Lecturers</a></li>
                <li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>student-rating','accommodation');" href="<%=urlR_AR%>">Accommodation</a></li>
                <li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>student-rating','city life');" href="<%=urlR_CYLR%>">City Life</a></li>
                <li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>student-rating','uni facilities');" href="<%=urlR_UFR%>">Uni Facilities</a></li>
                <li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>student-rating','clubs & societies');" href="<%=urlR_CSR%>">Clubs & Societies</a></li>
                <li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>student-rating','student union');" href="<%=urlR_SUR%>">Students' Union</a></li>
                <%--<li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>student-rating','eye candy');" href="<%=urlR_ECR%><%=qString%>">Eye Candy</a></li> //08_OCT_2014 Added by Amir to remove Eye Candy--%>
                <li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>student-rating','job prospects');" href="<%=urlR_JPR%>">Job Prospects</a></li>
                <%--<li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>student-rating','recommend this uni');" href="<%=urlR_RR%><%=qString%>">Recommend this Uni</a></li>--%>
            </ul>
        </li>
        <%}else if(mobileFlag){%>
        <%--Added by Indumathi Nov-03-15--%>
        <li class="sturat nw mob_drop" id="mob_sort_by"> 
          <a class="srt1 act"><%=defineMostInfo%></a> 
          <ul id="abcr1" class="strt" style="display:none">
            <li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>mostinfo','clicked');" href="<%=urlForMostInfo%>" class="<%=classNameMI%>">MOST INFO</a></li>
            <li><a rel= "nofollow" onclick="searchEventTracking('sort','<%=srchPrefix%>student-rating','overall rating');" href="<%=urlR_OR%>">OVERALL RATING</a></li>
          </ul>
        </li>
        <%}%>
      </ul>
    </div>
    <%--compare block --%>
      <div id="zerocompare" style='<%="0".equalsIgnoreCase(collegeCount) ? "display:block" : "display:none"%>'>
        <span class="cmprs" onclick="showLightBoxLoginForm('CUSTOM-ALERT', 430,500, 'nouni-basket-alert');">compare</span>
        <p class="unis">
          <a onclick="showLightBoxLoginForm('CUSTOM-ALERT', 430 , 500 , 'nouni-basket-alert');"><span id="srcompcnt1">[<%=collegeCount%>] </span>Unis/courses added</a>
        </p>
      </div>
      