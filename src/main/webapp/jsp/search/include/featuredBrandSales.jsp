<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, mobile.util.MobileUtils"%>
<%
  String featuredSalesImage = CommonUtil.getImgPath("/wu-cont/images/fbrnd_vid_thumb.png",0);
  String featuredPlayIcon = CommonUtil.getImgPath("/wu-cont/images/fbrnd_vid_play_icn.png",0);
  CommonUtil util = new CommonUtil();
  String videoClass = "fiveSecPlay"; //
  boolean mobileFlag = new MobileUtils().userAgentCheck(request);
  String mdevDomainName = GlobalConstants.MDEV_DOMAIN;
  String mtestDomainName = GlobalConstants.MTEST_DOMAIN;
  String wwwDomainName = GlobalConstants.LIVE_DOMAIN;
  pageContext.setAttribute("mdevDomain", mdevDomainName);
  pageContext.setAttribute("mtestDomain", mtestDomainName);
  pageContext.setAttribute("wwwDomain", wwwDomainName);
  String protocolDomainName = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN;
  pageContext.setAttribute("protocolDomain", protocolDomainName);
%>
<c:if test="${not empty requestScope.featuredBrandList}">
  <c:forEach var="featuredList" items="${requestScope.featuredBrandList}" varStatus="i">
    <div class="feabrnd_ui fl_w100">
	  <div class="fbrnd_lft fl">
		<div class="sponlst_hd fl_w100 imob">
		  <h4 class="fl_w100">Featured</h4>
	 	</div>
     	<c:if test="${featuredList.mediaType eq 'IMAGE'}">
	      <div class="fbrnd_vid fl">
			<div class="clr15_vid fl_w100">			 
			<c:set var="imagePath" value="${featuredList.mediaPath}"/>
            <% String imagePath = (String)pageContext.getAttribute("imagePath");
               imagePath = util.getImageUrl(request, imagePath, "_278px", "_369px", "_469px");%>
            <%if(!mobileFlag){%>
               <img class="cl_vid" data-rjs="Y" id="featured_image" src="<%=CommonUtil.getImgPath(imagePath, 1)%>" data-src="<%=CommonUtil.getImgPath(imagePath, 1)%>" alt="Sponsored listing image">
               <%} else { %>
               
               <img class="cl_vid" data-rjs="Y" id="featured_image" src="<%=CommonUtil.getImgPath(imagePath, 1)%>" data-src="<%=CommonUtil.getImgPath(imagePath, 1)%>" alt="Sponsored listing image">
               <%}%>
                             
			</div>
          </div>
		</c:if>
	    <c:if test="${featuredList.mediaType eq 'VIDEO'}">
		  <div class="fbrnd_vid fl">
			<div class="clr15_vid fl_w100">
		      <c:if test="${not empty featuredList.mediaPath}">
		        <c:set var="thumbnailPath" value="${featuredList.thumbnailPath}"/>
                <% String thumbnailPath = (String)pageContext.getAttribute("thumbnailPath");
                   thumbnailPath = util.getImageUrl(request, thumbnailPath, "_278px", "_369px", "_469px"); %>
                   <div class="prf_ldicon cmm_ldericn" id="loadIconflv" style="display: none;"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif", 1)%>"></div>
		        <span class="cl_vcnr" id="featured_video_plyicn" onclick="clickPlayAndPause('featured_video', event)" style="display: none;">
				   <span class="pl_icn"><img class="pl_icn_img" src="<%=featuredPlayIcon%>" alt="play icon"></span>			       
			    </span>      
			    <img class="cl_vid" data-rjs="Y" id="featured_image" src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" alt="Featured Video image">
				<%-- <span class="pl_icn"><img class="pl_icn_img" src="<%=featuredPlayIcon%>" alt="play icon"></span> --%>
				<video preload="none" data-rjs="Y" id="featured_video" class="<%=videoClass%>" width="100%" height="100%" style="display: none;"
				  onclick="clickPlayAndPause('featured_video', event)" data-media-id="${featuredList.mediaId}"
				  controlslist="nodownload" playsinline="">
				  <source src="${featuredList.mediaPath}" type="video/mp4">
			    </video>			   			   
              </c:if>
	   		</div>
	   	  </div>
	    </c:if>
	  </div>
	  <div class="fbrnd_rgt fl">
		<div class="sponlst_hd fl_w100 dsktp">
		  <h4 class="fl_w100">Featured</h4>
		</div>
		<div class="fbuniv_sec fl">
	    <c:if test="${not empty featuredList.headline}">
		  <c:if test="${not empty featuredList.profileHeadlineUrl}">
		    <c:choose>
		      <c:when test="${fn:indexOf(featuredList.profileHeadlineUrl, wwwDomain) ne -1 or fn:indexOf(featuredList.profileHeadlineUrl, mtestDomain) ne -1 or fn:indexOf(featuredList.profileHeadlineUrl, mdevDomain) ne -1}">
     		<h3><a href= "javascript:void(0);" data-href="${featuredList.profileHeadlineUrl}" onclick="featuredDbStatsLogging('${featuredList.collegeId}','','INTERNAL','${featuredList.profileHeadlineUrl}');gtmproductClick(${i.index}, 'sponsor_type_featured_college_name');featuredBrandGaLogging();">${featuredList.headline}</a></h3>
			  </c:when>
			  <c:otherwise>
                <h3><a rel="sponsored" target="_blank" href="${featuredList.profileHeadlineUrl}" onclick="featuredDbStatsLogging('${featuredList.collegeId}','','EXTERNAL','${featuredList.profileHeadlineUrl}');gtmproductClick(${i.index}, 'sponsor_type_featured_college_name');featuredBrandGaLogging();">${featuredList.headline}</a></h3>
              </c:otherwise>
			</c:choose>
          </c:if>
		  <c:if test="${empty featuredList.profileHeadlineUrl}">
			<h3>${featuredList.headline}</h3>
		  </c:if>
		</c:if>
		<c:if test="${not empty featuredList.tagline}">
          <p>${featuredList.tagline}</p>
		</c:if>
		<c:if test="${featuredList.reviewDisplayflag eq 'Y'}">
		 <c:if test="${not empty featuredList.overallRating}">
          <c:if test="${featuredList.overallRating gt 0}">  
          <ul class="strat cf">
            <li class="mr-15 leag_wrap">
			  <span class="fl mr10">OVERALL RATING <span class="cal"> </span></span> 
			    <span class="rat${featuredList.overallRating} t_tip"> 
			     <span class="cmp">
				   <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
                   <div class="line"></div>
				 </span>
				 </span> 
				 <span class="rtx">(${featuredList.overallRatingExact})</span>
			   <c:if test="${not empty featuredList.reviewCount}">
				<a href= "javascript:void(0);" data-href="${protocolDomain}${featuredList.uniReviewUrl}" class="link_blue" onclick="featuredDbStatsLogging('${featuredList.collegeId}','','INTERNAL','${protocolDomain}${featuredList.uniReviewUrl}');gtmproductClick(${i.index}, 'sponsor_type_featured_reviews');featuredBrandGaLogging();">
			      <c:if test="${not empty featuredList.reviewCount}">
				  <c:if test="${featuredList.reviewCount ne 0}">
				    ${featuredList.reviewCount}
                    <c:if test="${featuredList.reviewCount gt 1}">
                      reviews
                    </c:if>
				    <c:if test="${featuredList.reviewCount eq 1}">  
                      review
                    </c:if>
				  </c:if>
                </c:if>
              </a>
               </c:if>
			</li>
		  </ul>
		</c:if>
		</c:if>
		</c:if>
		<div class="fb_uni_vc fl_w100">
	     <c:if test="${not empty featuredList.navigationText}">
		  <c:if test="${not empty featuredList.navigationUrl}">
		    <c:choose>
		      <c:when test="${fn:indexOf(featuredList.navigationUrl, wwwDomain) ne -1 or fn:indexOf(featuredList.navigationUrl, mtestDomain) ne -1 or fn:indexOf(featuredList.navigationUrl, mdevDomain) ne -1}">
				<a href= "javascript:void(0);" data-href="${featuredList.navigationUrl}" onclick="featuredDbStatsLogging('${featuredList.collegeId}','','INTERNAL','${featuredList.navigationUrl}');gtmproductClick(${i.index}, 'sponsor_type_featured_view_courses');featuredBrandGaLogging();">${featuredList.navigationText}<span class="blue_arw"></span></a>
			  </c:when>
			  <c:otherwise>
                <a rel="sponsored" target="_blank" href="${featuredList.navigationUrl}" onclick="featuredDbStatsLogging('${featuredList.collegeId}','','EXTERNAL','${featuredList.navigationUrl}');gtmproductClick(${i.index}, 'sponsor_type_featured_view_courses');featuredBrandGaLogging();">${featuredList.navigationText}<span class="blue_arw"></span></a>
              </c:otherwise>
			</c:choose>
          </c:if>
		  <c:if test="${empty featuredList.navigationUrl}">
			${featuredList.navigationText}
		  </c:if>
		 </c:if>
		</div>
       </div>
	 </div>
    </div>
    <input type="hidden" id="collegeId" value="${featuredList.collegeId}"/>
    <input type="hidden" id="collegeNameGA" value="${featuredList.collegeName}"/>
    <input type="hidden" id="featureOrderItemId" value="${featuredList.orderItemId}" /> 
    <input type="hidden" id="featureCollegeId" value="${featuredList.collegeId}" />
  </c:forEach>
</c:if>