<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction" %>
<%
  String queryStringValue = (String)request.getAttribute("queryStr");   
         queryStringValue = !GenericValidator.isBlankOrNull(queryStringValue) ? queryStringValue.trim() :"";
  String universityName = (String)request.getAttribute("collegeName");  
         universityName = !GenericValidator.isBlankOrNull(universityName)? universityName :"";
  String providerID = (String)request.getAttribute("providerID");  
          providerID = !GenericValidator.isBlankOrNull(providerID)? providerID :"";  
  String sortBy = (String)request.getAttribute("sortByValue");   
  String providerSortBy = sortBy;
  if(sortBy.contains("r-")||sortBy.equals("r")){providerSortBy="";}
  CommonFunction commonFunction = new CommonFunction();
  String qString = commonFunction.formProviderResultFilterParam(request);
         qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  if(providerSortBy!=""){
        qString = qString + (GenericValidator.isBlankOrNull(qString)? ("?sort="+providerSortBy):("&sort="+providerSortBy));
  }
%>
<c:if test="${not empty requestScope.universityCount}">
<c:if test="${not empty requestScope.resultExists}">
   <c:if test="${requestScope.resultExists eq 'N'}">
      <div class="lst cf">       
        <div id="ran-alert" class="ran-alert" style="display:block;">
        <c:if test="${requestScope.recentFilter eq 'C'}">
                <a class="close" onclick="deleteUniFilter();">X</a>          
                The uni or college you're looking for doesn't offer this course – sorry. You could try searching by location instead, or
                <a href="<SEO:SEOURL pageTitle="all-provider-results"><%=universityName%>#<%=providerID%>#<%=qString%></SEO:SEOURL>">view all courses at this uni</a>. We've also displayed some similar universities you might like below.
          </c:if>
          <c:if test="${requestScope.recentFilter eq 'P'}">
                <a class="close" onclick="deleteSearchFilterParams('postCode');">X</a>
                We can't find any results for your postcode (sorry)! You can either try looking within a greater distance of your home, or - if you have filters added - try removing some to widen your search.
          </c:if>
           <c:if test="${requestScope.recentFilter eq 'M'}">
                <a class="close" onclick="deleteSearchFilterParams('module');">X</a>                         
                We hate to break it to you, but we can't find any results for that particular module. Try typing in something different, or - if you have filters added - try removing some to widen your search.
          </c:if>
           <c:if test="${requestScope.recentFilter eq 'G'}">
                <a class="close" onclick="removeGradesFilter();">X</a>                         
                Oh dear - we can't find any courses that fit your criteria. If you have any filters added, try removing some to widen your search.
          </c:if>
        </div>
      </div>      
    </c:if>
    <input type="hidden" id="queryStringVal" name="queryStringVal" value="<%=queryStringValue%>"/>

  </c:if>
</c:if>
<c:if test="${not empty requestScope.courseRankFoundFlag}">
  <c:if test="${requestScope.courseRankFoundFlag eq 'N'}">
     <div class="lst cf">       
      <div id="ran-alert" class="ran-alert" style="display:block;"> <%--Changed ranking lable frim Times to CUG for 08_MAR_2016 By Thiyagu G--%>
        There are no CompUniGuide Subject Rankings available for this Subject. Please choose one of the alternative ways of sorting your results
      </div>
     </div>
  </c:if>
</c:if>
      