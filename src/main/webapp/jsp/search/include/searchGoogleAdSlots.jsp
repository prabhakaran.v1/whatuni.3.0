<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
<script type="text/javascript" src="//partner.googleadservices.com/gampad/google_service.js"></script>
    <script type="text/javascript">      
      GS_googleAddAdSenseService("ca-pub-9150511304937518");
      GS_googleEnableAllServices();
    </script>    --%>
    <%
      String gamKeywordOrSubject = request.getAttribute("gamKeywordOrSubject") != null ? (String) request.getAttribute("gamKeywordOrSubject") : "";
      String gamStudyLevelDesc = (String) session.getAttribute("gamStudyLevelDesc");      
     
      if(gamStudyLevelDesc == null || gamStudyLevelDesc.trim().length() == 0 ){
        gamStudyLevelDesc = (String) request.getAttribute("gamStudyLevelDesc");      
      }
      String entryPointsForGAM = "";
      if((String)session.getAttribute("entryPointsforGAM") != null){
        entryPointsForGAM = (String)session.getAttribute("entryPointsforGAM");  //Added by Sekhar for wu405_13032012 for setting new GAM gargetting key.
      }
      String entryRequirementsForGAM = "";  
      if((String)session.getAttribute("entryRequirementsForGAM") != null){
        entryRequirementsForGAM = (String)session.getAttribute("entryRequirementsForGAM");  //Added for wu_536 3-FEB-2015_REL by karthi for GAM  entry requirements.
      }
    %>
    <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
    <script type="text/javascript">
     googletag.cmd.push(function() {
      <% if(gamKeywordOrSubject != null && gamKeywordOrSubject.trim().length() > 0){ 
         gamKeywordOrSubject = gamKeywordOrSubject.replaceAll("\\+", "-");
      %>
      
      googletag.pubads().setTargeting('keyword', ["<%=gamKeywordOrSubject.toLowerCase()%>"]);
      
      //GA_googleAddAttr("keyword", "<%=gamKeywordOrSubject.toLowerCase()%>");
      <%} if(gamStudyLevelDesc != null && gamStudyLevelDesc.trim().length() > 0){ %>
      googletag.pubads().setTargeting('hcchannels', "<%=gamStudyLevelDesc.toLowerCase()%>");
      //GA_googleAddAttr("hcchannels", "<%=gamStudyLevelDesc.toLowerCase()%>");
      <%} if(entryPointsForGAM != ""){%>
      googletag.pubads().setTargeting('alvlgrades', "<%=entryPointsForGAM.toLowerCase()%>");
      //GA_googleAddAttr("alvlgrades", "<%=entryPointsForGAM.toLowerCase()%>");
      <%} if(entryRequirementsForGAM != ""){%>
      googletag.pubads().setTargeting('Entryreq', "<%=entryRequirementsForGAM%>");
      //GA_googleAddAttr("Entryreq", "<%=entryRequirementsForGAM%>");
      <%}%>
      //googletag.pubads().enableSingleRequest();
	   googletag.pubads().enableLazyLoad({
       fetchMarginPercent: 0,
       renderMarginPercent: 0,
       mobileScaling: 0.0
     });
    googletag.enableServices();
  }); 
    </script>
    </c:if>