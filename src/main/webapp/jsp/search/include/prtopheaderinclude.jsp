<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" %>

<%  CommonFunction common = new CommonFunction();
    String searchPageType = request.getParameter("searchPageType");
           searchPageType = !GenericValidator.isBlankOrNull(searchPageType)?searchPageType:"";
    String paramCollegeName = (String) request.getAttribute("collegeName");
           paramCollegeName = paramCollegeName != null && !paramCollegeName.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(paramCollegeName).trim() : "";         
    String paramCollegeId = (request.getAttribute("collegeId") !=null && !request.getAttribute("collegeId").equals("null") && String.valueOf(request.getAttribute("collegeId")).trim().length()>0 ? String.valueOf(request.getAttribute("collegeId")) : ""); 
    String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");//5_AUG_2014
    String currYear = GlobalConstants.CLEARING_YEAR;
    String nextYear = GlobalConstants.NEXT_YEAR;
    String courseHeader = (String)request.getAttribute("courseHeader") != null ? request.getAttribute("courseHeader").toString() : "";
           courseHeader = courseHeader.replaceAll("Clearing & Adjustment", "");  
    String qString = (String)request.getAttribute("queryStr");
           qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";  
    String matchbrowseQueryString = "";  //30_JUN_2015_REL        
    String searchClearing = (String)request.getAttribute("searchClearing");
    if(("TRUE").equals(searchClearing)){
       matchbrowseQueryString = qString.replace("clearing&","");
       matchbrowseQueryString =  !GenericValidator.isBlankOrNull(matchbrowseQueryString)? ("?nd"+matchbrowseQueryString):"";
    }
    String collegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("collegeNameDisplay") : "";
    //
    String collegeLocation = (String) request.getAttribute("collegeLocation");
    collegeLocation = collegeLocation != null && !collegeLocation.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(collegeLocation).trim() : "";      
    String courseExistsLink = (String)request.getAttribute("courseExistsLink") != null ? request.getAttribute("courseExistsLink").toString() : "";
           courseExistsLink = courseExistsLink.replaceAll("clearing&", "");  
    
%>

  <%if(("clearing").equals(searchPageType)){%>              
			 
 <%}%> 
  <div class="prs">
  <c:if test="${not empty requestScope.listOfCollegeDetails}">
    <c:forEach var="listOfCollegeDetails" items="${requestScope.listOfCollegeDetails}">
          <div class="prs-lt">
          <c:if test="${not empty listOfCollegeDetails.collegeLogo}">
              <div class="pru-lgo"><img width="140px" src="<%=CommonUtil.getImgPath("",0)%>${listOfCollegeDetails.collegeLogo}"/></div>
            </c:if>
            <c:if test="${empty listOfCollegeDetails.collegeLogo}"> 
              <div class="pru-lgo">&nbsp;</div>
            </c:if>
          </div>
          <div class="prs-mdl">
            <div class="pmc">
              <p class="prs-hdr">
						           <%if(("clearing").equals(searchPageType)){%>   
                     <a href="${listOfCollegeDetails.uniHomeURL}?clearing">${listOfCollegeDetails.collegeNameDisplay}</a>
                 <%}else{%>                    
                      <a href="${listOfCollegeDetails.uniHomeURL}">${listOfCollegeDetails.collegeNameDisplay}</a>
                 <%}%>
              </p>
              <p class="sbj-dge">${requestScope.courseHeader}<span> (${requestScope.courseCount} result<c:if test="${requestScope.courseCount gt 1}">s</c:if>)</span></p>
                 
                 <ul class="strat cf">
                 <c:if test="${listOfCollegeDetails.collegeStudentRating ne 0}">
                        <li class="mr-15"><span class="fl mr10">Student rating</span> <span class="rat${listOfCollegeDetails.collegeStudentRating}"></span><span class="rtx">(${listOfCollegeDetails.actualRating})</span>
                          <a href="${listOfCollegeDetails.uniReviewsURL}" class="link_blue">
                          <c:if test="${not empty listOfCollegeDetails.reviewCount}">
                             <c:if test="${listOfCollegeDetails.reviewCount ne 0}">
                                  ${listOfCollegeDetails.reviewCount} 
                                  <c:if test="${listOfCollegeDetails.reviewCount gt 1}">
                                     reviews
                                  </c:if>
                                  <c:if test="${listOfCollegeDetails.reviewCount eq 1}">
                                     review
                                  </c:if>
                               </c:if>
                                </c:if>
                            </a>
                            </li>
                          </c:if>
                          <li class="ovew"><span>Overview</span> <strong>${listOfCollegeDetails.collegeOverview}</strong></li>
                        </ul>                        
                 <%if(("clearing").equals(searchPageType)){%>
                    <p class="snipt">If you're looking for clearing courses at  ${listOfCollegeDetails.collegeNameDisplay}, you're in the right area. You can narrow down your search results using the filters on the left-hand side of the page to help find what you're looking for. When you've seen something you like the look of, make sure you contact the university directly as things can change very quickly during clearing.</p>
                    <%--<p class="cal_txt">(Calls to clearing hotlines are free from UK landlines or the standard network rate from mobile)</p>--%> <%--Added calls free text for clearing on 16_May_2017, By Thiyagu G--%>
                 <%}else{%>
                     <p class="snipt">If you are considering ${listOfCollegeDetails.collegeNameDisplay}, it's a good idea to check out ${listOfCollegeDetails.collegeNameDisplay}'s <a href="<SEO:SEOURL pageTitle="uni-open-days" ><%=paramCollegeName%>#<%=paramCollegeId%>#<%=collegeLocation%></SEO:SEOURL>" title="<%=collegeNameDisplay%>'s open days">open days</a>, and also details of ${listOfCollegeDetails.collegeNameDisplay}'s scholarships and <a href="<SEO:SEOURL pageTitle="uni-specific-reviews" ><%=paramCollegeName%>#<%=paramCollegeId%></SEO:SEOURL>" title="<%=collegeNameDisplay%>'s student reviews">student reviews</a>.</p>
                 <%}%>
                 <%--
                 <logic:notEqual value="0" name="listOfCollegeDetails" property="scholarshipCount">
                    <p class="fwi">
                      <a href="<SEO:SEOURL pageTitle="uni-specific-scholarship" ><%=paramCollegeName%>#<%=paramCollegeId%></SEO:SEOURL>" title="<%=collegeNameDisplay%>'s scholarships"><bean:write name="listOfCollegeDetails" property="scholarshipCount"/> Scholarship<logic:greaterThan value="1" name="listOfCollegeDetails" property="scholarshipCount">s</logic:greaterThan> available</a>
                    </p>
                 </logic:notEqual>
                 --%>
              </div>
            </div>
          <div class="prs-rt">
            <div class="opd1">
            <c:if test="${not empty listOfCollegeDetails.nextOpenDay}">       
                 <div class="nod">Next open day</div>
                  <div class="dec-date mt10">
                     <span class="dec-day">${listOfCollegeDetails.opendateDate}</span>&nbsp;${listOfCollegeDetails.opendateMonth}
                   </div>                   
                 <div class="opc"><a href="<SEO:SEOURL pageTitle="uni-open-days" ><%=paramCollegeName%>#<%=paramCollegeId%>#<%=collegeLocation%></SEO:SEOURL>" title="<%=collegeNameDisplay%> next open day">View all open days</a></div>
              </c:if>
            </div>
           </div>
          </c:forEach>
     </c:if>
  </div>
                 