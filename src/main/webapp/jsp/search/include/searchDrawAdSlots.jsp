<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
 
<%
  int stind1 = request.getRequestURI().lastIndexOf("/");
  int len1 = request.getRequestURI().length();
  String page_name = pageContext.getAttribute("pagename3") != null ? (String)pageContext.getAttribute("pagename3") : request.getRequestURI().substring(stind1+1,len1); 
  if("advanceSearchResults.jsp".equalsIgnoreCase(page_name)){
    page_name = "browseMoneyPageResults.jsp";
  }
   if("advanceProviderResults.jsp".equalsIgnoreCase(page_name)){
    page_name = "courseSearchResult.jsp";
  }
  String google_ad_page_name = page_name !=null ? page_name.replaceAll(".jsp","").toLowerCase() : page_name; 
  int results = Integer.parseInt(request.getParameter("totalResults"));
  int position = Integer.parseInt(request.getParameter("position"));
      position = position + 1 ;
  String adSlotName = !GenericValidator.isBlankOrNull(request.getParameter("adSlotName")) ? request.getParameter("adSlotName") : "topban";
  String fromPage = !GenericValidator.isBlankOrNull(request.getParameter("fromPage")) ? request.getParameter("fromPage") : google_ad_page_name;
  String pageName = "wuni_"+google_ad_page_name;
  String mobPageName = "wuni_"+fromPage;
  String pageNumber = GenericValidator.isBlankOrNull(request.getParameter("pageNumber")) ? "" : request.getParameter("pageNumber");
  String showDesktop = GenericValidator.isBlankOrNull(request.getParameter("showDesktop")) ? "true" : request.getParameter("showDesktop").toLowerCase();
  String showBanner = GenericValidator.isBlankOrNull(request.getParameter("showBanner")) ? "n" : request.getParameter("showBanner").toLowerCase();
  String slot = "slot";
%>
<c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
<%--Modified to get AdSlots in mobile, 03_NOV_2015 By Indumathi S--%>     
<%if(results <=4 ){if(position == results){%> 
  <div class="blst" id="blst_<%=position + pageNumber%>"></div>
  <script type="text/javascript" id="gambanner<%=position + pageNumber%>">  
      if (width <= 480) {
        if("n" == "<%=showBanner%>"){
          googletag.cmd.push(function() {
            var <%=slot +position + pageNumber%> = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/<%=mobPageName%>_mbl_lb_300x50', [300, 50],'blst_<%=position + pageNumber%>').addService(googletag.pubads());
             googletag.pubads().enableLazyLoad({
               fetchMarginPercent: 0,
               renderMarginPercent: 0,
               mobileScaling: 0.0
             });
			googletag.enableServices();
            googletag.display('blst_<%=position + pageNumber%>');
            googletag.pubads().refresh([slot+'<%=position + pageNumber%>']);
          });
        }
      }else{
        if("true" == "<%=showDesktop%>"){
          googletag.cmd.push(function() {
            var <%=slot +position + pageNumber%> = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/<%=pageName%>_<%=adSlotName%>_468x60', [728, 90],'blst_<%=position + pageNumber%>').addService(googletag.pubads());
            googletag.pubads().enableLazyLoad({
               fetchMarginPercent: 0,
               renderMarginPercent: 0,
               mobileScaling: 0.0
             });
			googletag.enableServices();
            googletag.display('blst_<%=position + pageNumber%>');
            googletag.pubads().refresh([<%=slot +position + pageNumber%>]);
          });
        }
      }
    </script>  
<%}}else if(results >4){if(position == 4){%>
  <div class="blst" id="blst_<%=position + pageNumber%>"></div>
  <script type="text/javascript" id="gambanner<%=position + pageNumber%>">  
    if (width <= 480) {
      if("n" == "<%=showBanner%>"){
        googletag.cmd.push(function() {
          var <%=slot +position + pageNumber%> = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/<%=mobPageName%>_mbl_lb_300x50', [300, 50],'blst_<%=position + pageNumber%>').addService(googletag.pubads());
          googletag.pubads().enableLazyLoad({
               fetchMarginPercent: 0,
               renderMarginPercent: 0,
               mobileScaling: 0.0
             });
		  googletag.enableServices();
          googletag.display('blst_<%=position + pageNumber%>');
          googletag.pubads().refresh([<%=slot +position + pageNumber%>]);
        });
      }
    }else{
      if("true" == "<%=showDesktop%>"){
        googletag.cmd.push(function() {
          var <%=slot +position + pageNumber%> = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/<%=pageName%>_<%=adSlotName%>_468x60', [728, 90],'blst_<%=position + pageNumber%>').addService(googletag.pubads());
          googletag.pubads().enableLazyLoad({
               fetchMarginPercent: 0,
               renderMarginPercent: 0,
               mobileScaling: 0.0
             });
		  googletag.enableServices();
          googletag.display('blst_<%=position + pageNumber%>');
          googletag.pubads().refresh([<%=slot +position + pageNumber%>]);
        });
      }
    }
  </script>   
<%}}%>
<%if(position == 8){%>
  <div class="blst" id="blst_<%=position + pageNumber%>"></div>
  <script type="text/javascript" id="gambanner<%=position + pageNumber%>">  
    if (width <= 480) {
      if("n" == "<%=showBanner%>"){
        googletag.cmd.push(function() {
          var <%=slot +position + pageNumber%> = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/<%=mobPageName%>_mbl_lb1_300x50', [300, 50],'blst_<%=position + pageNumber%>').addService(googletag.pubads());
          googletag.pubads().enableLazyLoad({
               fetchMarginPercent: 0,
               renderMarginPercent: 0,
               mobileScaling: 0.0
             });
		  googletag.enableServices();
          googletag.display('blst_<%=position + pageNumber%>');
          googletag.pubads().refresh([<%=slot +position + pageNumber%>]);
        });
      }  
    }else{
      if("true" == "<%=showDesktop%>"){
        googletag.cmd.push(function() {
          var <%=slot +position + pageNumber%> = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/<%=pageName%>_<%=adSlotName%>1_468x60', [728, 90],'blst_<%=position + pageNumber%>').addService(googletag.pubads());
          googletag.pubads().enableLazyLoad({
               fetchMarginPercent: 0,
               renderMarginPercent: 0,
               mobileScaling: 0.0
             });
		  googletag.enableServices();
          googletag.display('blst_<%=position + pageNumber%>');
          googletag.pubads().refresh([<%=slot +position + pageNumber%>]);
        });
      }
    }
  </script>  
<%}%>
</c:if>
