<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%> 
<%@ page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil, java.util.HashMap, WUI.utilities.CommonFunction"%>

<%
  CommonFunction comm = new CommonFunction();
  String fromHerb = (String)request.getAttribute("fromHerb");
  String russellGroup = request.getParameter("russell-group"); 
             russellGroup = !GenericValidator.isBlankOrNull(russellGroup)? russellGroup: "";  
  String module = request.getParameter("module");
              module = !GenericValidator.isBlankOrNull(module)? new CommonFunction().replaceHypenWithSpace(module) : "";
  String distance = request.getParameter("distance");
              distance = !GenericValidator.isBlankOrNull(distance)? distance: "";
  String postCode = request.getParameter("postcode");
              postCode = !GenericValidator.isBlankOrNull(postCode)? postCode:"";
  String empRateMax = request.getParameter("employment-rate-max");
              empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
  String empRateMin = request.getParameter("employment-rate-min");
              empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
  String campusType = request.getParameter("campus-type");
              campusType = !GenericValidator.isBlankOrNull(campusType)? campusType: "";
  String locType = request.getParameter("location-type");
              locType = !GenericValidator.isBlankOrNull(locType)? locType: "";
    String loc = request.getParameter("location");
              loc = !GenericValidator.isBlankOrNull(loc)? loc: "";
  String ucasTarrifMax = request.getParameter("ucas-points-max");
              ucasTarrifMax = !GenericValidator.isBlankOrNull(ucasTarrifMax)? ucasTarrifMax: "";
  String ucasTarrifMin = request.getParameter("ucas-points-min");
              ucasTarrifMin = !GenericValidator.isBlankOrNull(ucasTarrifMin)? ucasTarrifMin: "";  
  
  String prefType = request.getParameter("your-pref");
              prefType = !GenericValidator.isBlankOrNull(prefType)? prefType: "";
              
   String assessedType = (String)request.getAttribute("assessmentTypeDisplay");
              assessedType = !GenericValidator.isBlankOrNull(assessedType)? assessedType: "";   
         
  String pageFrom = request.getParameter("PAGE_FROM");
  String parentCatId = (String)request.getAttribute("parentCatId") != null ? (String)request.getAttribute("parentCatId") : "";
  String parentCat = (String)request.getAttribute("parentCatDesc") != null ? (String)request.getAttribute("parentCatDesc") : "";
         parentCat = comm.replaceHypen(comm.replaceURL(parentCat.toLowerCase()));
  String entryLevel = (String)request.getAttribute("entryLevelValue");
  String filter = "N";
  String jacs = request.getParameter("jacs");
         jacs = !GenericValidator.isBlankOrNull(jacs)? jacs: "";//3_JUN_2014
  String clearingonoff = comm.getWUSysVarValue("CLEARING_ON_OFF");//5_AUG_2014
  String searchType = request.getParameter("searchType");
         searchType = !GenericValidator.isBlankOrNull(searchType)? searchType: "";   
  String locationName = (String)request.getAttribute("searchLoc");
        locationName = !GenericValidator.isBlankOrNull(locationName) ? locationName.trim() :"";  
  String queryStringValue = (String)request.getAttribute("queryStr");   
         queryStringValue = !GenericValidator.isBlankOrNull(queryStringValue) ? queryStringValue.trim() :"";   
  String studyMode = (String)request.getAttribute("studyModeDisplay");  
        studyMode = !GenericValidator.isBlankOrNull(studyMode)? studyMode :"";
  String universityName = (String)request.getAttribute("collegeName");  
         universityName = !GenericValidator.isBlankOrNull(universityName)? universityName :"";
         
  String qString = (String)request.getAttribute("queryStr");
         qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  String clearingPage =  (String)request.getAttribute("searchClearing");//10-JUN-2015_REL
         clearingPage = GenericValidator.isBlankOrNull(clearingPage)?"":"TRUE";
  String deleteclr = qString;
   if("TRUE".equals(clearingPage)){
     deleteclr = deleteclr.replace("?clearing","");
     deleteclr =  !GenericValidator.isBlankOrNull(deleteclr)? ("?"+deleteclr):"";
  }
   String ucasEntryPoints = (String)request.getAttribute("gradeFilterUcasScorePoint");
   ucasEntryPoints = !GenericValidator.isBlankOrNull(ucasEntryPoints) &&  ucasEntryPoints.indexOf(",") > -1 ? ucasEntryPoints.replaceAll(",", " - ") : ""; 
%>

<%--Modified the delete filter functions based on new URL pattern Jan-27-16 Indumathi.S--%>
<div class="sh_hd">
 <div class="ns_flr" id="filterTop" style = "display:none;">
    <div id ="filterblock">
      <h4 id="Filterh1">Filters applied</h4>
    </div>
    <div id="Filter" class="flt_lt" style="display:none;">
      <%if("ON".equalsIgnoreCase(clearingonoff)  && ("CLEARING").equals(searchType)){ //5_AUG_2014
      %>
      <c:if test="${not empty requestScope.cleraringBrowseURL}">
           <% filter = "Y";%>           
             <div class="frst ch1"> <a class="tp_box mb10 red" href="<c:out value="${requestScope.cleraringBrowseURL}"/><%=deleteclr%>" ><span>Clearing</span></a></div>          
      </c:if>
      <%}%>    
      <c:if test="${not empty requestScope.subjectDesc}">
       <c:if test="${not empty requestScope.parentCatId }">
           <% filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('subject','<%=parentCat%>');"><c:if test="${not empty requestScope.subjectDesc }"><span><c:out value="${requestScope.subjectDesc}"/></span></c:if>  </a></div>
         </c:if>
      </c:if>
      <c:if test="${not empty requestScope.searchLocName}">
       <%if("Y".equalsIgnoreCase(fromHerb) && !GenericValidator.isBlankOrNull(loc)){%>
           <% filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('location','<%=locationName%>');"><c:if test="${not empty requestScope.searchLocName }"><span><c:out value="${requestScope.searchLocName}"/></span></c:if></a></div>
       <%}else{%>
       <c:if test="${fn:trim(requestScope.searchLocName) ne 'United Kingdom'}">
       
           <% filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('location','<%=locationName%>');"><c:if test="${not empty requestScope.searchLocName }"><span><c:out value="${requestScope.searchLocName}"/></span></c:if> </a></div>
        </c:if>
        <%}%>
       </c:if>
       <c:if test="${not empty  requestScope.entryGradeFilter}">
           <%filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="removeGradesFilter();"><%--3_JUN_2014--%>              
                <span> <%=entryLevel%> grades <%=request.getAttribute("entryPoints")%></span>              
          </a></div>
       </c:if>
       <%--delete filter ucas score generated through grade filter page --%>
       <c:if test="${not empty  requestScope.gradeFilterUcasScorePoint}">
           <%filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="removeGradesFilter();"><%--3_JUN_2014--%>              
                <span> UCAS score range <span><%=ucasEntryPoints%></span></span>                            
          </a></div>
       </c:if>
       <%--END --%>
       <%if(russellGroup!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('russell');"><span>Russell Group</span></a></div>       
       <%}%>
       
       <%if(assessedType!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('assessed');"><span><%=assessedType%></span></a></div>       
       <%}%>
       
       <%if(campusType!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('campusType');"><span>Campus type</span></a></div>       
       <%}%>
       <%if(!((empRateMax == "" || empRateMax == "100"  ) &&  (empRateMin == "0" || empRateMin == "" ))){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('empRate');"><span>Employment Rate</span></a></div>       
       <%}%>
       
       <%if(!((ucasTarrifMax == "" || ucasTarrifMax == "480"  ) &&  (ucasTarrifMin == "0" || ucasTarrifMin == "" ))){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('ucasTariff');"><span>Ucas Tariff</span></a></div>       
       <%}%>
        <%if(module!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('module');"><span>Modules: <%=module%></span></a></div>       
       <%}%>
       
       <%if(studyMode!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('studyMode');"><span><%=studyMode%></span></a></div>       
       <%}%>
       
        <%if(postCode!=""){filter = "Y";%>
           <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('postCode');"><span><%=distance%> miles from <%=postCode%></span></a></div>       
       <%}%><%--End--%>
        <%
         if(locType!=""){
          String locationTypeDesc = "";
          String locTypeArr[] = locType.split(",");
          String locationType = "";
          String filetvalue = "";
          HashMap locationMap = (HashMap)request.getAttribute("locationMap");
          for(int i = 0; i<locTypeArr.length; i++ ){ 
           filetvalue = "";
            locationType = (String)locationMap.get(locTypeArr[i]); 
              for(int j = 0; j<locTypeArr.length; j++){
                if(locTypeArr[i]!=locTypeArr[j]){
                  if(filetvalue!=""){
                    filetvalue = filetvalue + "," + locTypeArr[j];
                  }else{
                    filetvalue = locTypeArr[j];
                  }
               }
              }
          %>              
         <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('locationType', '<%=filetvalue%>');"><span><%=locationType%></span></a></div>
       <%filter = "Y";}}%>
       
       <%
         if(prefType!=""){
          String prefTypeDesc = "";
          String prefTypeArr[] = prefType.split(",");
          String preffType = "";
          String filetvalue = "";
          HashMap locationMap = (HashMap)request.getAttribute("reviewCatMap");
          if(locationMap != null) {
          for(int i = 0; i<prefTypeArr.length; i++ ){ 
           filetvalue = "";
            preffType = (String)locationMap.get(prefTypeArr[i]); 
              for(int j = 0; j<prefTypeArr.length; j++){
                if(prefTypeArr[i]!=prefTypeArr[j]){
                  if(filetvalue!=""){
                    filetvalue = filetvalue + "," + prefTypeArr[j];
                  }else{
                    filetvalue = prefTypeArr[j];
                  }
               }
              }
          %>              
        <%-- <div class="frst ch1"> <a class="tp_box mb10" onclick="deleteSearchFilterParams('preferenceType', '<%=filetvalue%>');"><span><%=preffType%></span></a></div> --%>
       <%filter = "Y";}}}%>
       
    </div>
</div>
</div>
  <input type="hidden" id="filterDiv" name="filterDiv" value="<%=filter%>"/>
  <input type="hidden" id="queryStringVal" name="queryStringVal" value="<%=queryStringValue%>"/>
  <input type="hidden" id="hidden_jacs" name="hidden_jacs" value="<%=jacs%>"/><%--3_JUN_2014--%>
  
<script type="text/javascript">
showFilterBlock(); 
</script>

 
 