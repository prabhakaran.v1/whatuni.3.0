<%--
  Page name:    seoPaginationProvider.jsp
  Version:      1.0
  Since:        2010-08-10
  Author:       Mohamed Syed
  Owner:        www.hotcourses.com
  Description:  This file will generate the pagination link and return as a string to the calling page.
                href="http://www.whatuni.com/*/*/pageno/*/*.html"
--%>
<%@ page import="org.apache.commons.validator.GenericValidator, com.wuni.util.seo.SeoUrls"%>
<%
  // initial page or page selected
  int pageno = 1;
  // Display number of page numbered links in the page
  int pagelimit = 0;
  // Stores total number of records to the entire synario
  int searchhits = 0;
  // To display number of records in the page
  int recorddisplay = 0;
  // To Store the url to be attach with hyperlink
  String displayurl_1 = "";
  // To Store the url to be attach with hyperlink
  String displayurl_2 = "";
  // to set the SEOURL for first page
  String firstPageSeoUrl = "";
  String universityCount = "";
  String resultExists = "";
  // local temp variables
  int numofpages = 0;
  int pagenum = 0;
  int lower = 0;
  int upper = 0;

  // Reads the In Parameter from the JSP include action and assign to the respective variable
  if(request.getParameter("pageno") != null){
    pageno = Integer.parseInt(request.getParameter("pageno"));
  }
  if(request.getParameter("pagelimit") != null){
    pagelimit = Integer.parseInt(request.getParameter("pagelimit"));
  }
  if(!GenericValidator.isBlankOrNull(request.getParameter("searchhits")) ){
    searchhits = Integer.parseInt(request.getParameter("searchhits").replaceAll(",", ""));
  }
  if(request.getParameter("recorddisplay") != null){
    recorddisplay = Integer.parseInt(request.getParameter("recorddisplay"));
  }
  if(request.getParameter("displayurl_1") != null){
    displayurl_1 = request.getParameter("displayurl_1");
  }
  if(request.getParameter("displayurl_2") != null){
    displayurl_2 = request.getParameter("displayurl_2");
  }
  if(request.getParameter("universityCount") != null){
    universityCount = request.getParameter("universityCount");
  }
  if(request.getParameter("resultExists") != null){
    resultExists = request.getParameter("resultExists");
  }
  if(request.getParameter("firstPageSeoUrl") != null){
    firstPageSeoUrl = request.getParameter("firstPageSeoUrl");
  }  
 String qString = (String)request.getAttribute("queryStr");
         qString = !GenericValidator.isBlankOrNull(qString) ? ("?"+qString):"";
 String tempQString = new SeoUrls().constructUrlParameters(request, "", "", qString, universityCount, resultExists, "n");
  if(searchhits > recorddisplay){
      // variable which will hold dynamically-generated-pagination-related-content.      
      StringBuffer paginationContent = new StringBuffer();   
      
      //some calculation
      numofpages = (searchhits / recorddisplay);
      if(searchhits % recorddisplay > 0) {  numofpages++; }
      pagenum = (int)((pageno - 1) / pagelimit);
      pagenum =  pagenum + 1;
      lower = ((pagenum-1) * pagelimit) + 1;
      upper = java.lang.Math.min(numofpages, (((pageno - 1) / pagelimit) + 1) * pagelimit);
      if((pageno%pagelimit) == 0){
        lower = pageno;
        upper = java.lang.Math.min(numofpages, pageno+pagelimit);
      }      
      //start container ul
      paginationContent.append("<div class=\"artPg bor0\"><ul>");
      //add previous 
      if(pageno > 0){ //Changed and commented based on new pagination design, 24_Feb_2015 By Thiyagu G
        if(pageno == 1){
          paginationContent.append("<li class=\"hid-ph\"><a class='in_visible' disabled=\"disabled\" onclick=\"return false;\" href=\"");
          paginationContent.append(displayurl_1+qString);
          paginationContent.append("\"><i class=\"fa fa-angle-left\"></i></a></li>");          
        }else {
          paginationContent.append("<li class=\"hid-ph\"><a rel='follow' href=\"");
          paginationContent.append(displayurl_1 + tempQString);
          if(pageno!=2){
            paginationContent.append("&pageno=");
            paginationContent.append(String.valueOf(pageno-1));  
          }          
          paginationContent.append("\" class=''><i class=\"fa fa-angle-left\"></i></a></li>");
        }
      }    
    
      //add mid-pages      
      if(numofpages > 1){
        for(int i = lower; i <= upper; i++){
          if(i == 1 && i != pageno){
            paginationContent.append("<li><a class=\"hid-ph\" rel='follow' href=\"");            
            paginationContent.append(displayurl_1 + tempQString);            
            paginationContent.append("\" >");
            paginationContent.append(i);
            paginationContent.append("</a></li>");  
          }else if(i == pageno){
            paginationContent.append("<li class='hid-ph' >"); 
            paginationContent.append("<a class=\"active\" disabled=\"disabled\">");
            paginationContent.append(String.valueOf(pageno)); 
            paginationContent.append("</a>"); 
            paginationContent.append("</li>");
          }else{
            paginationContent.append("<li><a class=\"hid-ph\" rel='follow' href=\"");            
            paginationContent.append(displayurl_1 + tempQString);
            paginationContent.append("&pageno=");
            paginationContent.append(String.valueOf(i));            
            paginationContent.append("\" >");
            paginationContent.append(i);
            paginationContent.append("</a></li>");            
          }
        }
      }    
      //add next 
      if(pageno < numofpages){ //Changed and commented based on new pagination design, 24_Feb_2015 By Thiyagu G
        paginationContent.append("<li class='hid-ph'><a rel='follow' href=\"");        
        paginationContent.append(displayurl_1 + tempQString);
        paginationContent.append("&pageno=");
        paginationContent.append(String.valueOf(pageno+1));
        paginationContent.append("\" class='mr0'><i class=\"fa fa-angle-right\"></i></a></li>");
      }else if(pageno == numofpages){ //Changed and commented based on new pagination design, 24_Feb_2015 By Thiyagu G
        paginationContent.append("<li class='hid-ph'><a rel='follow' disabled=\"disabled\" onclick=\"return false;\" href=\"");        
        paginationContent.append(displayurl_1 + tempQString);        
        paginationContent.append("\" class='in_visible'><i class=\"fa fa-angle-right\"></i></a></li>");
      }
    //close containere ul
    paginationContent.append("</ul></div>");        
    //printing the pagination content.
    out.println(paginationContent.toString());
  }  
%>
