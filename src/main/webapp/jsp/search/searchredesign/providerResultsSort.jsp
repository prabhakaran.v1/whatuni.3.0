<%@ page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil"%>
<%  
  String qString = (String)request.getAttribute("queryStr");
         qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  String module = request.getParameter("module");
              module = !GenericValidator.isBlankOrNull(module)? module: "";         
  String russellGroup = request.getParameter("russell-group"); 
             russellGroup = !GenericValidator.isBlankOrNull(russellGroup)? russellGroup: "";
  String distance = request.getParameter("distance");
              distance = !GenericValidator.isBlankOrNull(distance)? distance: "";
  String postCode = request.getParameter("postcode");
              postCode = !GenericValidator.isBlankOrNull(postCode)? postCode:"";
  String empRateMax = request.getParameter("employment-rate-max");
              empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
  String empRateMin = request.getParameter("employment-rate-min");
              empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
  String campusType = request.getParameter("campus-type");
              campusType = !GenericValidator.isBlankOrNull(campusType)? campusType: "";
  String locType = request.getParameter("location-type");
              locType = !GenericValidator.isBlankOrNull(locType)? locType: "";
  String ucasTarrifMax = request.getParameter("ucas-points-max");
              ucasTarrifMax = !GenericValidator.isBlankOrNull(ucasTarrifMax)? ucasTarrifMax: "";
  String ucasTarrifMin = request.getParameter("ucas-points-min");
              ucasTarrifMin = !GenericValidator.isBlankOrNull(ucasTarrifMin)? ucasTarrifMin: "";
  String smode = (String)request.getParameter("study-mode");
         smode = !GenericValidator.isBlankOrNull(smode)? smode : "";
  String seoFirstPageUrl = (String)request.getAttribute("SEO_FIRST_PAGE_URL");
         seoFirstPageUrl = !GenericValidator.isBlankOrNull("seoFirstPageUrl")? seoFirstPageUrl :"";
         seoFirstPageUrl = !GenericValidator.isBlankOrNull(seoFirstPageUrl) && (seoFirstPageUrl.lastIndexOf("&sort=") == -1) ? seoFirstPageUrl.trim()+"&sort=r" : seoFirstPageUrl.trim();
  String searchWhat = ((String)request.getAttribute("searchWhat"));
  searchWhat = !GenericValidator.isBlankOrNull("searchWhat")? searchWhat :"";
  String sortParam = !GenericValidator.isBlankOrNull(request.getParameter("sort"))? request.getParameter("sort") :"r";    
  String sortURL = seoFirstPageUrl;
  
  String studyLevelDesc = (String)request.getAttribute("studyLevelDesc");
  studyLevelDesc = !GenericValidator.isBlankOrNull(studyLevelDesc) ? studyLevelDesc : "";  
  String jacsCode = (String)request.getParameter("jacs");//3_JUN_2014
         jacsCode = !GenericValidator.isBlankOrNull(jacsCode)? jacsCode : "";  
  String clearingPage =  (String)request.getAttribute("searchClearing");
       clearingPage = GenericValidator.isBlankOrNull(clearingPage)?"":clearingPage;     
  
  String finalsortURL = "";
  String newSortParam = "";
  String sortBy = (String)request.getAttribute("sortByValue");
  String sortByValue = "&sort="+sortParam;
  if("A".equalsIgnoreCase(searchWhat) && "r".equalsIgnoreCase(sortParam)){
    sortParam = "ta";
  }
  String basketCount = request.getSession().getAttribute("basketpodcollegecount") != null ? String.valueOf(request.getSession().getAttribute("basketpodcollegecount")) : "0";   
  String srchPrefix = "";
   String srchType = request.getParameter("srchType");
   if(srchType!=null && !"".equals(srchType)){
     if("clearing".equals(srchType)){
      srchPrefix = "clearing-";
     }
   }
%>
 <%String defineMostInfo = ("r".equalsIgnoreCase(sortParam) || GenericValidator.isBlankOrNull(sortParam)) ? "Most Info" : "A to Z";%>

<div class="lsthdr cf">
  <div class="srt">
    <ul class="cf">
      <li class="sort_by">SORT BY:</li>
      <%
        if("A".equalsIgnoreCase(searchWhat)){
          finalsortURL = "";
          newSortParam = "r";
          finalsortURL += sortURL ;
          //finalsortURL += newSortParam.trim() != "" ? (((sortURL.indexOf("?nd&") > -1) ? "&" : "?nd&") + "sort="+newSortParam) : "";
          finalsortURL =  sortURL.lastIndexOf("&sort=r") != -1 ? sortURL.replaceAll(sortByValue, "") : sortURL.replaceAll(sortByValue, "");
      %>
      <li class="<%="r".equalsIgnoreCase(sortParam) ? "mact" : "mstinf"%>">
        <a onclick="searchEventTracking('pr-sort','<%=srchPrefix%>mostinfo','clicked');" rel="nofollow" class="<%="r".equalsIgnoreCase(sortParam) ? "act nw" : ""%>" href=<%=finalsortURL%>>Most info</a>
      <%}else{
      finalsortURL = sortURL.lastIndexOf("&sort=r") != -1 ? sortURL.replaceAll(sortByValue, "") : sortURL.replaceAll(sortByValue, "");
      %>
      <li class="<%="".equalsIgnoreCase(sortParam) && "Z".equalsIgnoreCase(searchWhat) ? "mact" : "mstinf"%>">
        <a onclick="searchEventTracking('pr-sort','<%=srchPrefix%>mostinfo','clicked');" rel="nofollow" class="<%="r".equalsIgnoreCase(sortParam) && "Z".equalsIgnoreCase(searchWhat) ? "act nw" : ""%>" href=<%=finalsortURL%>>Most info</a>
      <%}%>
      </li>
      <li class="<%=!"".equalsIgnoreCase(sortParam) && ("crh".equalsIgnoreCase(sortParam) || "crl".equalsIgnoreCase(sortParam)) ? "mact" : "cournk"%>">
        <%
          finalsortURL = "";
          newSortParam = "crh".equalsIgnoreCase(sortParam) ? "crl" : "crl".equalsIgnoreCase(sortParam) ? "crh" : "crh";
          finalsortURL += sortURL ;
          //finalsortURL += newSortParam.trim() != "" ? (((sortURL.indexOf("?nd&") > -1) ? "&" : "?nd&") + "sort="+newSortParam) : "";
          finalsortURL =  sortURL.lastIndexOf("&sort=crh") != -1 ? sortURL.replaceAll(sortByValue, "&sort=crl") : sortURL.replaceAll(sortByValue, "&sort=crh");
        %>      
        <a onclick="searchEventTracking('pr-sort','<%=srchPrefix%>course-ranking','clicked');" rel="nofollow" class="<%="crh".equalsIgnoreCase(sortParam) ? "act" : "crl".equalsIgnoreCase(sortParam) ? "act1" : ""%>" href="<%=finalsortURL%>">CompUniGuide ranking</a>
      </li>      
     
   <%if(studyLevelDesc != null && studyLevelDesc!="" && !studyLevelDesc.contains("Postgraduate")){%>
      <li class="<%=!"".equalsIgnoreCase(sortParam) && ("enta".equalsIgnoreCase(sortParam) || "entd".equalsIgnoreCase(sortParam)) ? "mact" : "entreq"%>">
        <%
          finalsortURL = "";
          newSortParam = "enta".equalsIgnoreCase(sortParam) ? "entd" : "entd".equalsIgnoreCase(sortParam) ? "enta" : "entd";
          finalsortURL += sortURL;
          //finalsortURL += newSortParam.trim() != "" ? (((sortURL.indexOf("?nd&") > -1) ? "&" : "?nd&") + "sort="+newSortParam) : "";
          finalsortURL =  sortURL.lastIndexOf("&sort=entd") != -1 ? sortURL.replaceAll(sortByValue, "&sort=enta") : sortURL.replaceAll(sortByValue, "&sort=entd");
        %>
        <a onclick="searchEventTracking('pr-sort','<%=srchPrefix%>entry-requirements','clicked');" rel="nofollow" class="<%="enta".equalsIgnoreCase(sortParam) ? "act1" : "entd".equalsIgnoreCase(sortParam) ? "act" : ""%>" href="<%=finalsortURL%>">Entry requirements</a>
      </li>
    <%}%>  
      <li class="<%=!"".equalsIgnoreCase(sortParam) && ("empa".equalsIgnoreCase(sortParam) || "empd".equalsIgnoreCase(sortParam)) ? "mact" : "emprat"%>">
        <%
          finalsortURL = "";
          newSortParam = "empa".equalsIgnoreCase(sortParam) ? "empd" : "empd".equalsIgnoreCase(sortParam) ? "empa" : "empd";
          finalsortURL += sortURL;   
          //finalsortURL += newSortParam.trim() != "" ? (((sortURL.indexOf("?nd&") > -1) ? "&" : "?nd&") + "sort="+newSortParam) : "";
          finalsortURL =  sortURL.lastIndexOf("&sort=empd") != -1 ? sortURL.replaceAll(sortByValue, "&sort=empa") : sortURL.replaceAll(sortByValue, "&sort=empd");
        %>      
        <a onclick="searchEventTracking('pr-sort','<%=srchPrefix%>employment-rate','clicked');"  rel="nofollow" class="<%="empa".equalsIgnoreCase(sortParam) ? "act1" : "empd".equalsIgnoreCase(sortParam) ? "act" : ""%>" href="<%=finalsortURL%>">Employment rate</a>
      </li>

      <li class="<%=!"".equalsIgnoreCase(sortParam) && ("ta".equalsIgnoreCase(sortParam) || "td".equalsIgnoreCase(sortParam)) ? "mact atoz" : "atoz"%> ">
        <%
          finalsortURL = "";
          newSortParam = "ta".equalsIgnoreCase(sortParam) ? "td" : "td".equalsIgnoreCase(sortParam) ? "ta" : "ta";
          finalsortURL += sortURL;   
          //finalsortURL += newSortParam.trim() != "" ? (((sortURL.indexOf("?nd&") > -1) ? "&" : "?nd&") + "sort="+newSortParam) : "";
          finalsortURL =  sortURL.lastIndexOf("&sort=td") != -1 ? sortURL.replaceAll(sortByValue, "&sort=ta") : sortURL.replaceAll(sortByValue, "&sort=td");
        %>        
        <a onclick="searchEventTracking('pr-sort','<%=srchPrefix%>a-z','clicked');"  rel="nofollow" class="<%="ta".equalsIgnoreCase(sortParam) ? "act" : "td".equalsIgnoreCase(sortParam) ? "act1" : ""%>" href="<%=finalsortURL%>">A-Z</a>
      </li>
      
      <%--Added by Indumathi Nov-03-15--%>
      <li class="sturat nw mob_drop"> 
        <a class="srt1 act" id="mob_sort_by"><%=defineMostInfo%></a> 
        <ul id="abcr1" class="strt">
          <%
              finalsortURL = "";
              newSortParam = "r";
              finalsortURL += sortURL ;
              //finalsortURL += newSortParam.trim() != "" ? (((sortURL.indexOf("?nd&") > -1) ? "&" : "?nd&") + "sort="+newSortParam) : "";
              finalsortURL =  sortURL.lastIndexOf("&sort=r") != -1 ? sortURL.replaceAll(sortByValue, "") : sortURL.replaceAll(sortByValue, "");
          %>
          <li><a onclick="searchEventTracking('pr-sort','<%=srchPrefix%>mostinfo','clicked');" rel="nofollow" class="<%="r".equalsIgnoreCase(sortParam) ? "act nw" : ""%>" href=<%=finalsortURL%> >Most info</a></li>
          <li>
            <%
              finalsortURL = "";
              newSortParam = "ta";
              finalsortURL += sortURL;   
              //finalsortURL += newSortParam.trim() != "" ? (((sortURL.indexOf("?nd&") > -1) ? "&" : "?nd&") + "sort="+newSortParam) : "";
              finalsortURL =  sortURL.lastIndexOf("&sort=ta") != -1 ? sortURL.replaceAll(sortByValue, "&sort=ta") : sortURL.replaceAll(sortByValue, "&sort=ta");
            %>        
            <a onclick="searchEventTracking('pr-sort','<%=srchPrefix%>a-z','clicked');"  rel="nofollow" href="<%=finalsortURL%>">A-Z</a>
          </li>
        </ul>
      </li>        
    </ul>
  </div>
    <%--compare block --%>
      <div id="zerocompare" style='<%="0".equalsIgnoreCase(basketCount) ? "display:block" : "display:none"%>'>
        <span class="cmprs" onclick="showLightBoxLoginForm('CUSTOM-ALERT', 430,500, 'nouni-basket-alert');">compare</span>
        <p class="unis">
          <a onclick="showLightBoxLoginForm('CUSTOM-ALERT', 430 , 500 , 'nouni-basket-alert');"><span id="srcompcnt1">[<%=basketCount%>] </span>Unis/courses added</a>
        </p>
      </div>
    <%--unis added --%>
</div>