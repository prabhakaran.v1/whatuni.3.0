<%@ page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" %>
<%String searchPageType = request.getParameter("searchPageType");
         searchPageType = !GenericValidator.isBlankOrNull(searchPageType)?searchPageType:"";  
%>
<c:if test="${not empty requestScope.listOfUserReviews}">
  <h3 class="tit">Student reviews</h3>
  <c:forEach var="userReviews" items="${requestScope.listOfUserReviews}" varStatus="id">
    <div class="lft_cnr mt0">
      <div class="rlst_row">
        <div class="revlst_lft fl">
          <div class="rlst_wrp fr">
            <div class="rev_prof ${userReviews.userNameInitialColourcode}">
            ${userReviews.userNameInitial}
          </div>
          <div class="rev_name">
            ${userReviews.userName}
          </div>
          <div class="rev_dte">
            ${userReviews.createdDate}
          </div>
        </div>
      </div>
      <div class="revlst_rht fl">
        <div class="rlst_wrap">
          <h2>
            <%if(("clearing").equals(searchPageType)){%>   
            <a href="${userReviews.uniHomeURL}?clearing" class="rev_uni link_blue">
            ${userReviews.collegeNameDisplay}
            </a>
            <%}else{%>
            <a href="${userReviews.uniHomeURL}" class="rev_uni link_blue">
               ${userReviews.collegeNameDisplay}
            </a>
            <%}%>
          </h2>
           <c:if test="${userReviews.courseDeletedFlag eq 'TRUE'}">
            <c:if test="${not empty userReviews.courseName}">
              <h3>
                ${userReviews.courseName}
              </h3>
           </c:if>
          </c:if>
           <c:if test="${userReviews.courseDeletedFlag ne 'TRUE'}">
            <h3>
              <a class="rev_sub link_blue" href="${userReviews.courseDetailsURL}" title="${userReviews.courseName}">
                ${userReviews.courseName}
              </a>
            </h3>
          </c:if>
          <div class="reviw_rating" id="viewLess_${id.index}">
            <div class="rate_new">
              <span class="ov_txt">OVERALL UNIVERSITY RATING</span>
              <span class="ml5 rat${userReviews.overallRating}"></span>   
              <c:if test="${not empty userReviews.overallQuesDesc}">         
                <div class="rw_qus_des">
                  ${userReviews.overallQuesDesc}
                </div>
              </c:if>    
            </div>
            <p class="rev_dec" style="display:block;">
              ${userReviews.overallRatingComments}
            </p>
          </div>
          <c:if test="${not empty userReviews.userMoreReviewaList}">
            <div id="viewMore_${id.index}" style="display:none;">
            <c:set var="reviewCatName" value=""/>
            <c:forEach var="userInnerReviews" items="${userReviews.userMoreReviewaList}">
               <c:if test="${userInnerReviews.questionTitle eq 'Overall Rating'}">
                 <c:if test="${userInnerReviews.rating ne 'Y'}">
                    <div class="reviw_rating">
                      <div class="rate_new">
                        <span class="ov_txt">OVERALL UNIVERSITY RATING</span>
                        <span class="ml5 rat${userInnerReviews.rating}"></span>
                        <c:if test="${not empty userInnerReviews.questionDesc}">
                          <div class="rw_qus_des">
                            ${userInnerReviews.questionDesc}
                          </div>
                        </c:if>
                      </div>
                      <p class="rev_dec">
                        ${userInnerReviews.answer}
                      </p>
                    </div>
                  </c:if>
                </c:if>
                <c:if test="${not empty userInnerReviews.questionTitle}">
                  <c:if test="${userInnerReviews.questionTitle ne 'Overall Rating'}">
                    <c:if test="${userInnerReviews.rating ne 'Y'}">
                      
                      <div class="reviw_rating">
                        <div class="rate_new">
                          <%--Added tooltip for "N/A" in star rating by Prabha on 08.03.16--%>
                          
                           <c:if test="${reviewCatName ne userInnerReviews.questionTitle}">  
                              ${userInnerReviews.questionTitle}
	                          <c:if test="${empty userInnerReviews.rating}">
	                            <span class="t_tip">
	                              N/A
	                              <span class="cmp">
	                                <div class="hdf5">
	                                  <spring:message code="wuni.review.without.stat.tooltip" />
	                                </div>
	                                <div class="line"></div>
	                              </span>
	                            </span>
	                          </c:if>
	                          <c:if test="${not empty userInnerReviews.rating}">
	                            <span class="ml5 rat${userInnerReviews.rating}"></span>
	                          </c:if>
                           </c:if>
                          
                          <c:if test="${not empty userInnerReviews.questionDesc}">                            
                            <div class="rw_qus_des">
                              ${userInnerReviews.questionDesc}
                            </div>
                          </c:if>
                        </div>
                        <c:if test="${not empty userInnerReviews.answer}">
                          <p class="rev_dec">
                            ${userInnerReviews.answer}
                          </p>
                        </c:if>
                        <c:set var="reviewCatName" value="${userInnerReviews.questionTitle}"/>
                        <%--End of tooltip code--%>
                      </div>
                    </c:if>
                  </c:if>
                  <c:if test="${userInnerReviews.rating eq 'Y'}">
                    <div class="rev-reco">
                      <div class="tit6">
                        ${userInnerReviews.questionTitle}
                      </div>
                      <a class="yes">
                        <i class="fa fa-check"></i>
                         ${userInnerReviews.answer}
                      </a>
                    </div>
                  </c:if>
                </c:if>
              </c:forEach>
            </div>
            <div class="rev_mre">
              <a id="viewMoreBtn_${id.index}" style="display:block;" title="View more" onclick="providerReviewsShowHide('viewLess_${id.index}','viewMore_${id.index}', 'viewMoreBtn_${id.index}');">+ View more</a>
            </div>
            <c:if test="${not empty userReviews.reviewId}">
              <input type="hidden" id="hidReviewId_${id.index}" value="${userReviews.reviewId}"/>
            </c:if>
          </c:if>
        </div>
      </div>
    </div>
    </div>
  </c:forEach>
</c:if>