<%@page import="org.apache.commons.validator.GenericValidator,WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
  String referrer = (String)request.getSession().getAttribute("FaqURL");
         referrer = GenericValidator.isBlankOrNull(referrer)? "" : referrer;  
  String subjectDescription = (String)request.getAttribute("subjectDescription");
         subjectDescription = GenericValidator.isBlankOrNull(subjectDescription)? "" : subjectDescription.trim();
  String facebookLoginJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.facebook.login.js");
%>
<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr"><jsp:include page="/jsp/common/wuHeader.jsp" /></div>                
    </div>      
  </div>
</header>
<div id="content-blk" class="ad_cnr">
  <div class="sr faq res_faq">
    <%--(1)-- microformated Breadcrumb --%>
    <jsp:include page="/seopods/breadCrumbs.jsp" />              
    <div class="sr-art">
      <div class="sart-lt">
        <div class="com">
          <div class="com-lt">
            <h1 class="result-head">FAQ</h1>
            <h2 class="fsh">Help using the Search Results</h2>
            <p class="arin mt10">Struggling to make sense of what you're seeing? Can't find what you're looking for? Well fear not. This page should help clear things up...</p>
          </div>
        </div>
        <div id="article1Div" class="sart1">
          <a onclick="toggleMenu('addnal11', this)" id="arrowaddnal11" class="addcou1 act" href="javascript:void(0);">Using the Filters<i class="fa fa-plus-circle fnt_24 fr fa-minus-circle"></i></a>
          <div class="sart1 nw" id="addnal11" style="display: block;">
            <div class="fqim"><%--Image path changes and figcaption code added by Prabha on 03_NOV_2015_REL--%>
              <figure>
                <img src="<%=CommonUtil.getImgPath("", 0)%>/wu-cont/images/res_ylw-bar.jpg" alt="" width="635" height="68" />
                <figcaption>See how easy it is to use our personalised search!</figcaption>
              </figure>
            </div>
            <div class="art-cont">See that column down the left of the page, and the yellow highlighted band at the top that says <b>'Personalise your Search'?</b> Those are the filters. They basically allow you to narrow down your search by selecting extra criteria (if you add in your predicted A-level grades, for instance, the search results will change to show only the courses that you're eligible to apply for).
              <div>
                <%--<img width="183" height="120" alt="" src="http://images2.content-wu.com/wu-cont/images/filter.jpg"/></span><span class="fat-txt">
                One option is to take your lessons in the summer. Not only will it be easier to find the time, but you'll be able to count on longer (and hopefully drier) evenings.  Booking your lessons between lectures and seminars may also be a possibility as driving instructors will usually be able to start your lessons on campus.</span>--><!--Commented by Prabha on 03_NOV_2015_REL--%>
              </div>
            </div>		
          </div>
        </div>
        <div id="article1Div" class="sart1">
          <a onclick="toggleMenu('addnal12', this)" id="arrowaddnal12" class="addcou1" href="javascript:void(0);">What do the different filters actually do?<i class="fa fa-plus-circle fnt_24 fr"></i></a>
          <div class="sart1 nw" id="addnal12" style="display: none;">
            <div class="art-cont"><p>The possibilities are (almost) endless. Starting from the top of the page and working down the left-hand column, here's a quick overview of the different filters you can use to help with your search:</p>
              <p><b>Entry Requirements:</b> We put this one right at the top because &ndash; without wanting to blow our own trumpets &ndash; it's pretty darn great. Enter your predicted grades (either A-levels, SQA Highers, SQA Advanced Highers or Tariff Points), and click the 'Update' button to add the filter. If you want to know how many points each different A-level grade is worth, just hover over the <b>'UCAS Tariff Calculator'. </b></p>
              <p><b>Bonus tip:</b> Make sure you log in at this point and save your grades to your MyWhatuni profile (this will save you from having to enter them again every time do a new search)!</p>
              <p><b>University:</b> Got a specific course at a specific uni in mind? No problem. Start typing the name of the uni into this box and then click the search icon to find it.</p>
              <p><b>Modules:</b> Want to find a computing degree that teaches Java? Or maybe a History course with a module on witches? We've got you covered. Just enter the module you're looking for here and hit the search icon.</p>
              <p><b>Subject:</b> You can choose from more specific subject options here.</p>
              <p><b>Location:</b> Narrow your search to a specific region of the UK.</p>
              <p><b>Distance:</b> Plenty of students opt to go to a uni that's far enough away from home to give them independence, but close enough for the occasional weekend trip back. Bang in your postcode, select your optimum distance away from the parents, and you're good to go.</p>
              <p><b>Location type:</b> Want to go to uni in a big city? Or maybe one by the sea? Select from four location-themed options with this filter.</p>
              <p><b>Campus type:</b> Looking for a campus uni? No problem. You can choose to show either 'Campus' or 'Non-campus' unis only with this one.</p>
              <p><b>Study Level:</b> Choose to show only full-time, sandwich, part-time or distance/online courses.</p>
              <p><b>Employment rate:</b> Worried about getting a job when you graduate? Move the slider to filter the results by the % of employed graduates.</p>
              <p><b>Entry requirements:</b> Move the slider to filter by tariff point entry requirements.</p>
              <p><b>Russell Group:</b> If you're set on going to a Russell Group uni, select 'Yes' in this filter.</p>
            </div>		
          </div>
        </div>
        <div id="article1Div" class="sart1">
          <a onclick="toggleMenu('addnal13', this)" id="arrowaddnal13" class="addcou1" href="javascript:void(0);">Comparing Courses<i class="fa fa-plus-circle fnt_24 fr"></i></a>
          <div class="sart1 nw" id="addnal13" style="display: none;">
            <div class="art-cont">
              <p>Use the hearts to save and compare courses.</p>
              <div><%--Image path changes and figcaption code added by Prabha on 03_NOV_2015_REL--%>
                <figure>
                  <img width="635" alt="faq_search_page" src="<%=CommonUtil.getImgPath("", 0)%>/wu-cont/images/faq_hrtsrch_list.jpg"/>
                </figure>
              </div>
              <p>Once you have saved a few courses, click <b>'View Comparison'</b> and you can see exactly how your courses and unis square up against each other. You can find all your saved items in the <b><a href="<%=GlobalConstants.WU_CONTEXT_PATH%>/comparison" title="My Final 5" target="_blank"> 'My Final 5'</a></b> tab.</p>
            </div>	
            <div><%--Image path changes and figcaption code added by Prabha on 03_NOV_2015_REL--%>
              <figure>
                <img width="635" src="<%=CommonUtil.getImgPath("", 0)%>/wu-cont/images/faq_res_comp.jpg"/>
                <figcaption>Comparing unis and courses have never been easier</figcaption>
              </figure>
            </div>
	      </div>
        </div>
        <div id="article1Div" class="sart1 img_toppos">
          <a onclick="toggleMenu('addnal14', this)" id="arrowaddnal14" class="addcou1" href="javascript:void(0);">Ordering your results<i class="fa fa-plus-circle fnt_24 fr"></i></a>
          <div class="sart1 nw" id="addnal14" style="display: none;">
            <div class="art-cont">So. You've made your search and filtered down your results. But did you know you can also order those results in different ways to make things even easier to rank and compare?
              Across the top of the results, you'll find a grey horizontal bar that says 'sort by':
            </div>	
            <div><%--Image path changes and figcaption code added by Prabha on 03_NOV_2015_REL--%>
              <figure>
                <img width="635" height="27" src="<%=CommonUtil.getImgPath("", 0)%>/wu-cont/images/res-sort-by-bar.jpg"></img>
                <figcaption>Order your universities in a that makes sense to you</figcaption>
              </figure>
            </div>
            <div class="art-cont bns">
              <b class="f14">Bonus Tip...</b>
              <p>Don't forget you can add multiple filters at the same time! So say if you want to search for all Russell Group Unis within 50 miles of where you live and then order the results by employment rate, you can &ndash; just put the Russell Group filter to 'Yes', put the Distance filter to 50 miles from your postcode, and then sort the results by 'Employment Rate'. Easy.</p>
            </div>    
          </div>
        </div> 
      </div>
      <c:if test="${empty sessionScope.userInfoList}">
        <div class="sart-rt"><jsp:include page="/jsp/home/spamBoxReg.jsp"/></div>   
      </c:if>
    </div>
  </div>
</div>
<jsp:include page="/jsp/common/wuFooter.jsp" />
<input type="hidden" name="hdrMenuHide" id="hdrMenuHide" value="READREVIEWS" />
<script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=facebookLoginJSName%>"></script>
</body>