<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.SessionData, WUI.utilities.GlobalConstants, WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.GlobalFunction"%>

<%String newsearchJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.newSearchResults.js");
CommonFunction common = new CommonFunction();
String pageno="1"; 
String exitOpenDayRes = "" , odEvntAction = "false";
    if(session.getAttribute("exitOpRes")!=null && !"".equals(session.getAttribute("exitOpRes"))){
      exitOpenDayRes = (String)session.getAttribute("exitOpRes");
    }    
    if(!"".equals(exitOpenDayRes) && "true".equals(exitOpenDayRes)){
      odEvntAction = "true";
    }
    String paramCollegeId = (request.getAttribute("collegeId") !=null && !request.getAttribute("collegeId").equals("null") && String.valueOf(request.getAttribute("collegeId")).trim().length()>0 ? String.valueOf(request.getAttribute("collegeId")) : ""); 
    String paramCategoryCode = (request.getAttribute("paramCategoryCode") !=null && !request.getAttribute("paramCategoryCode").equals("null") && String.valueOf(request.getAttribute("paramCategoryCode")).trim().length()>0 ? new CommonUtil().toUpperCase(String.valueOf(request.getAttribute("paramCategoryCode"))) : "");  
    String paramStudyLevelId = (request.getAttribute("paramStudyLevelId") !=null && !request.getAttribute("paramStudyLevelId").equals("null") && String.valueOf(request.getAttribute("paramStudyLevelId")).trim().length()>0 ? new CommonUtil().toUpperCase(String.valueOf(request.getAttribute("paramStudyLevelId"))) : ""); 
    //
    String first_mod_group_id = (String)request.getAttribute("first_mod_group_id");
    String noindexfollow = request.getAttribute("meta_robot") !=null ? String.valueOf(request.getAttribute("meta_robot"))  : "noindex,follow";
    String keyword = (request.getAttribute("searchText") !=null && !request.getAttribute("searchText").equals("null") && String.valueOf(request.getAttribute("searchText")).trim().length()>0 ? String.valueOf(request.getAttribute("searchText")) : "");  
    String keywordPhrase = request.getAttribute("searchText") != null ? request.getAttribute("searchText").toString() : "";
    // wu582_20181023 - Sabapathi: Added studymodeId for stats logging
    String studymodeId = request.getAttribute("studymodeId") != null ? request.getAttribute("studymodeId").toString() : "";
    String pros_search_name = request.getAttribute("prospectus_search_name") != null ? request.getAttribute("prospectus_search_name").toString() : "";
    String location = (request.getAttribute("location") !=null && !request.getAttribute("location").equals("null") && String.valueOf(request.getAttribute("location")).trim().length()>0 ? String.valueOf(request.getAttribute("location")) : "");  
    String paramFlag = paramStudyLevelId != null &&  paramStudyLevelId.equalsIgnoreCase("") ? "ALL_COURSE_RESULTS" : "COURSE_RESULTS";
    String pageNameValue = paramStudyLevelId != null &&  paramStudyLevelId.equalsIgnoreCase("") ? "ALL COURSE RESULTS" : "COURSE RESULTS";
    String courseMappingPath = request.getAttribute("courseMappingPath") !=null ? "rsearch.html" : "csearch.html";
    String searchPosition = new WUI.utilities.CookieManager().getCookieValue(request, "sresult_provider_position");
    String studyLevelDesc = (String)request.getAttribute("studyLevelDesc");
    // added for 02nd Feb 2009 Release to show the canonicalURL for the SEO requirement 
    String canonicalURL = (String) request.getAttribute("currentPageUrl");
    canonicalURL  = canonicalURL !=null && canonicalURL.trim().length()>0 ? canonicalURL : "";
    // end of thecoce added  
    String paramCollegeName = (String) request.getAttribute("collegeName");
    paramCollegeName = (!GenericValidator.isBlankOrNull(paramCollegeName) && !paramCollegeName.equalsIgnoreCase("null")) ? paramCollegeName.trim() : "";
    String collegeNameGa = common.replaceSpecialCharacter(paramCollegeName);
    String seoCollegeName =  paramCollegeName !=null && paramCollegeName.trim().length()>0 ? new CommonFunction().replaceHypen(new CommonFunction().replaceURL(paramCollegeName)).replaceAll(" ","-")+"-" : "";
    String seoCollegeNameLower  = new CommonUtil().toLowerCase(seoCollegeName);
    String collegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("collegeNameDisplay") : "";
    String collegeLocation = (String) request.getAttribute("collegeLocation");
    collegeLocation = collegeLocation != null && !collegeLocation.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(collegeLocation).trim() : "";
    Object SEARCH_TYPE = request.getAttribute("SEARCH_TYPE");    
    String courseUrl = "/all-courses/csearch?university="+common.replaceHypen(common.replaceURL(paramCollegeName)).toLowerCase(); //Changed new all courses url pettern, By Thiyagu G for 27_Jan_2016.
    int searchhits = 0;
    if(!GenericValidator.isBlankOrNull((String)(request.getAttribute("courseCount"))) && new CommonUtil().isNumber((String)(request.getAttribute("courseCount")))){	
      searchhits = Integer.parseInt(String.valueOf(request.getAttribute("courseCount"))); 
    }
    String courseHeader = (String)request.getAttribute("courseHeader") != null ? request.getAttribute("courseHeader").toString() : "";
    String currYear = GlobalConstants.CLEARING_YEAR;
    String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");//5_AUG_2014
    String qString = (String)request.getAttribute("queryStr");
           qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
    String clrString = qString;
    String courseExistsLink = "";
    if("ON".equalsIgnoreCase(clearingonoff)){      
      courseExistsLink = (String)request.getAttribute("courseExistsLink") != null ? request.getAttribute("courseExistsLink").toString() : "";
           courseExistsLink = courseExistsLink.replace("?", "?clearing&");
    }
    String clearingPage =  (String)request.getAttribute("searchClearing");
       clearingPage = GenericValidator.isBlankOrNull(clearingPage)?"":clearingPage;    
    String isAdvertiser = request.getAttribute("isAdvertiser") != null && request.getAttribute("isAdvertiser").toString().trim().length() > 0 ? (String) request.getAttribute("isAdvertiser") : "";
    String advNonAdv = "";
    if("TRUE".equals(isAdvertiser)){
      advNonAdv = "Advertiser";
    }else{
      advNonAdv = "Non-Advertiser";
    }
    String evntProviderName = (paramCollegeName != null && paramCollegeName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(common.replaceSpecialCharacter(paramCollegeName))) : "").toLowerCase();
    String advertiserFlag = (String)request.getAttribute("advertiserFlag");
           advertiserFlag = GenericValidator.isBlankOrNull(advertiserFlag)?"":advertiserFlag;    
    //  
    String institutionId = new GlobalFunction().getInstitutionId(paramCollegeId);
    String studyLevel = (request.getAttribute("studyLevel") !=null && !request.getAttribute("studyLevel").equals("null") && String.valueOf(request.getAttribute("studyLevel")).trim().length()>0 ? String.valueOf(request.getAttribute("studyLevel")) : ""); 
    String capeImg = CommonUtil.getImgPath("/wu-cont/images/capeh.png",0);
    //flag for to show canonical url in june_5_18 by Sangeeth.S
    String canonicalUrlFlag = request.getAttribute("CANONICAL_URL") != null ? request.getAttribute("CANONICAL_URL").toString() : "";    
%> 
<c:if test="${not empty pageno}">
  <%pageno=String.valueOf(request.getAttribute("pageno")); %>
</c:if>
  <body>
  <c:if test="${not empty requestScope.pixelTracking}">${requestScope.pixelTracking}</c:if> 
   <!--Changed the buttons for responsive redesign 03_Nov_2015 By S.Indumathi-->
   <div class="sr_resp">
    <header class="clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>
    </header>  
    
    <%--9_DEC_2014 Added by Priyaa for pixel tracking--%>
    <div class="ad_cnr">
      <div id="content-blk">
        <%
        request.setAttribute("uniLanding", "YES");
        String urldata_1=""; 
        String urldata_2="";
        String urldata_3="";
        String cid="0";
        %>
        
        <c:if test="${not empty requestScope.collegeId}"> 
         <%cid = String.valueOf(request.getAttribute("collegeId")); %>
        </c:if>
        
        <%            
        urldata_1 = (String)request.getAttribute("URL_1");   
        urldata_2 = (String)request.getAttribute("URL_2");              
        String urlString = urldata_1+pageno+urldata_2;
        String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "";
        String assignCollegeLogo = "";
        session.setAttribute("prospectus_redirect_url", urlString);
        %> 
            <div class="sr Pro_Res">
              <jsp:include page="/seopods/breadCrumbs.jsp" >
                <jsp:param name="pageName" value="PROVIDER_RESULT_PAGE" />            
              </jsp:include>
              
              <div class="prs">
              <c:if test="${not empty requestScope.listOfCollegeDetails}">
                <c:forEach var="listOfCollegeDetails" items="${requestScope.listOfCollegeDetails}">
                    <div class="prs-lt">
                    <c:if test="${not empty listOfCollegeDetails.collegeLogo}">
                        <div class="pru-lgo"><c:set var="defineCollegeLogo" value="${listOfCollegeDetails.collegeLogo}"/>
                        <% assignCollegeLogo = pageContext.getAttribute("defineCollegeLogo").toString(); %>
                        <img width="140px" src="<%=CommonUtil.getImgPath("",0)%>${listOfCollegeDetails.collegeLogo}" alt="${listOfCollegeDetails.collegeNameDisplay}"/></div>
                      </c:if>
                      <c:if test="${empty listOfCollegeDetails.collegeLogo}">
                        <div class="pru-lgo">&nbsp;</div>
                        <%assignCollegeLogo="";%>
                      </c:if>
                    </div>
                    <div class="prs-mdl">
                      <div class="pmc">
                        <p class="prs-hdr"><a href="${listOfCollegeDetails.uniHomeURL}" onclick="sponsoredListGALogging(${listOfCollegeDetails.collegeId});">${listOfCollegeDetails.collegeNameDisplay}</a></p>
                        <p class="sbj-dge">${requestScope.courseHeader}<span> (${requestScope.courseCount} result<c:if test="${requestScope.courseCount gt 1}">s</c:if>)</span></p>
                       
                        <ul class="strat cf">
                        <c:if test="${listOfCollegeDetails.collegeStudentRating ne 0}">
                            <li class="mr-15"><span class="fl mr10">Student rating</span> <span class="rat${listOfCollegeDetails.collegeStudentRating} t_tip">
														  <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								              <span class="cmp">
									              <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									              <div class="line"></div>
								              </span>
								              <%--End of tool tip code--%>
														</span><span class="rtx">(${listOfCollegeDetails.actualRating})</span>
                            <a href="${listOfCollegeDetails.uniReviewsURL}" class="link_blue">
                            <c:if test="${not empty listOfCollegeDetails.reviewCount}">
                             <c:if test="${listOfCollegeDetails.reviewCount ne 0}">
                               ${listOfCollegeDetails.reviewCount}
                                  <c:if test="${listOfCollegeDetails.reviewCount gt 1}">
                                     reviews
                                  </c:if>
                                  <c:if test="${listOfCollegeDetails.reviewCount eq 1}">
                                     review
                                 </c:if>
                              </c:if>
                                </c:if>
                            </a>
                            </li>
                          </c:if>
                          <li class="ovew"><span>Overview</span> <strong>${listOfCollegeDetails.collegeOverview}</strong></li>
                        </ul>
                        <!--Added by Indumathi.S For Nov-03-15 Rel-->
                        <p class="snipt">Considering attending ${listOfCollegeDetails.collegeNameDisplay}? <c:if test="${not empty listOfCollegeDetails.nextOpenDay}">Check out their <a href="<SEO:SEOURL pageTitle="uni-open-days" ><%=paramCollegeName%>#<%=paramCollegeId%>#<%=collegeLocation%></SEO:SEOURL>" title="<%=collegeNameDisplay%>'s open days">open days</a> to see the campus for yourself and f</c:if><c:if test="${empty listOfCollegeDetails.nextOpenDay}">F</c:if>ind out what their own students say about the place in these <a href="${listOfCollegeDetails.uniReviewsURL}" title="<%=collegeNameDisplay%>'s student reviews">reviews</a>...</p>                        
                        <%--<logic:notEqual value="0" name="listOfCollegeDetails" property="scholarshipCount"><p class="fwi"><a href="<SEO:SEOURL pageTitle="uni-specific-scholarship" ><%=paramCollegeName%>#<%=paramCollegeId%></SEO:SEOURL>" title="<%=collegeNameDisplay%>'s scholarships"><bean:write name="listOfCollegeDetails" property="scholarshipCount"/> Scholarship<logic:greaterThan value="1" name="listOfCollegeDetails" property="scholarshipCount">s</logic:greaterThan> available</a></p></logic:notEqual>--%>
                        <%--Added text for foundation degree search by Prabha on 05_Jul_2017--%>
                        <%if("foundation degree".equalsIgnoreCase(studyLevelDesc)){%>
                          <p class="cal_txt fdtxt_wrp"><span class="fdeg_txt">Sorry if there aren't many results, we are waiting for data from our partner UCAS.</span> </p>
                        <%}%>
                        <%--End of code--%>
                      </div>
                    </div>
                    <%-- <div class="prs-rt">
                      <div class="opd1">
                      <c:if test="${not empty listOfCollegeDetails.nextOpenDay}">                       
                          <div class="nod">
                          Added OpenDayType for 13_Dec_2016, by Thiyagu G.
                          <c:set var="defineOpenDayType" value="${listOfCollegeDetails.openDayType}"></c:set>
                          <%
                            String openDayText = "Next open day";
                            if("M".equalsIgnoreCase(studyLevel) && "PG".equalsIgnoreCase((String)pageContext.getAttribute("defineOpenDayType"))){
                              openDayText = "Next postgraduate open day";
                            }else if("L".equalsIgnoreCase(studyLevel) && "UG".equalsIgnoreCase((String)pageContext.getAttribute("defineOpenDayType"))){
                              openDayText = "Next undergraduate open day";
                            }
                          %>
                          <%=openDayText%></div>
                          <div class="dec-date mt10">
                            <span class="dec-day">${listOfCollegeDetails.opendateDate}</span>&nbsp;${listOfCollegeDetails.opendateMonth}
                          </div>                          
                          <div class="opc"><a href="<SEO:SEOURL pageTitle="uni-open-days" ><%=paramCollegeName%>#<%=paramCollegeId%>#<%=collegeLocation%></SEO:SEOURL>" title="<%=collegeNameDisplay%> next open day">View all open days</a></div>
                        </c:if>
                      </div>
                    </div> --%>
                  </c:forEach>
                </c:if>
              </div>
              <c:if test="${COVID19_SYSVAR eq 'ON'}">
		    <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
		    <div class="sr">
					<div class="covid_upt">
						<span class="covid_txt"><%=new SessionData().getData(request, "COVID19_COURSE_TEXT")%></span>
						<a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_blu_arw.svg", 0)%>"></a>
					</div>
				</div>
		  </c:if>
              <!--To show and hide filter option in responsive view 03_Nov_2015 By S.Indumathi-->
              <div class="fl_lr">
                <a href="#">
                    <span class="left"><i class="fa fa-filter"></i></span>
                    <span class="right">Filter results</span>
                </a>
              </div>
              
              <div class="sr-cont">
                <jsp:include page="/jsp/search/include/providerFilters.jsp"/>
                
                <div class="srs-rt">
                  <jsp:include page="/jsp/search/searchredesign/providerResultsSort.jsp"/>
                  <div class="lst">
                    <c:if test="${not empty requestScope.courseList}">
                     <c:forEach var="courseList" items="${requestScope.courseList}" varStatus="rowId">
                         <c:set var="courseId" value="${courseList.courseId}"/>
                        <div class="srch_cnr">
                    <div class="srlogo_lt">
                        <div class="logo_cmp cmp-bl">
                         <c:if test="${courseList.wasThisCourseShortListed eq 'FALSE'}">                           
                                <div class="cmlst" id="basket_div_${courseId}">
                                <div class="compare" onmouseover="addToComparisonHover(${courseId});" onmouseout="addToComparisonOut(${courseId});"> <%--17_Mar_2015 - Added to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                                  <c:set var="srcEvntCourseTitle1" value="${courseList.courseTitle}"/>
                                  <a onclick='addBasket("${courseId}", "O", this, "basket_div_${courseId}", "basket_pop_div_${courseId}", "", "", "<%=common.replaceSpecialCharacter(paramCollegeName)%>");searchEventTracking("view comparison","clicked","<%=common.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle1"))%>: <%=common.replaceSpecialCharacter(paramCollegeName)%>");'>
                                  <span class="icon_cmp f5_hrt"></span>
                                  <span class="cmp_txt">COMPARE</span>
                                  <span class="loading_icon" id="load_${courseId}" style="display:none;"></span>
                                  <span class="chk_cmp"><span class="chktxt" id="show_${courseId}" style="display:none;">Add to comparison<em></em></span></span> <%--17_Mar_2015 - Added reference id to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                                  </a>
                                </div>  
                                </div>
                                <div class="cmlst act" id="basket_pop_div_${courseId}" style="display:none;"> 
                                <div class="compare">
                                <a class="act" onclick='addBasket("${courseId}", "O", this,"basket_div_${courseId}", "basket_pop_div_${courseId}");searchEventTracking("view comparison","removed","<%=common.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle1"))%>: <%=common.replaceSpecialCharacter(paramCollegeName)%>");'>
                                <span class="icon_cmp f5_hrt hrt_act"></span>
                                <span class="cmp_txt">COMPARE</span>
                                <span class="loading_icon" id="load1_${courseId}" style="display:none;"></span>
                                <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                                </a>
                                  </div>
                                  <div id="defaultpop_${courseId}" class="sta" style="display: block;">
                                    <%
                                        String userId = new SessionData().getData(request, "y");
                                        if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
                                    %>
                                        <a class="view_more" href="/degrees/comparison">View comparison</a>
                                    <%}else{%>
                                        <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
                                    <%}%>
                                  </div>
                                  <div id="pop_${courseId}" class="sta"></div>
                                </div>
                              </c:if>	 
                              <c:if test="${courseList.wasThisCourseShortListed eq 'TRUE'}">
                                <div class="cmlst" id="basket_div_${courseId}" style="display:none;">
                                  <div class="compare" onmouseover="addToComparisonHover(${courseId});" onmouseout="addToComparisonOut(${courseId});"> <%--17_Mar_2015 - Added to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                                  <c:set var="srcEvntCourseTitle2" value="${courseList.courseTitle}"/>      
                                  <a onclick='addBasket("${courseId}", "O", this, "basket_div_${courseId}", "basket_pop_div_${courseId}", "", "", "<%=common.replaceSpecialCharacter(paramCollegeName)%>");searchEventTracking("view comparison","clicked","<%=common.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle2"))%>: <%=common.replaceSpecialCharacter(paramCollegeName)%>");'>
                                  <span class="icon_cmp f5_hrt"></span>
                                  <span class="cmp_txt">COMPARE</span>
                                  <span id="load_${courseId}" style="display:none;"></span>
                                  <span class="chk_cmp"><span class="chktxt" id="show1_${courseId}" style="display:none;">Add to comparison<em></em></span></span> <%--17_Mar_2015 - Added reference id to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                                  </a>                 
                                  </div>
                                </div>
                                <div class="cmlst act" id="basket_pop_div_${courseId}"> 
                                  <div class="compare">
                                  <a class="act" onclick='addBasket("${courseId}", "O", this,"basket_div_${courseId}", "basket_pop_div_${courseId}");searchEventTracking("view comparison","removed","<%=common.replaceSpecialCharacter((String)pageContext.getAttribute("srcEvntCourseTitle2"))%>: <%=common.replaceSpecialCharacter(paramCollegeName)%>");'>
                                  <span class="icon_cmp f5_hrt hrt_act"></span>
                                  <span class="cmp_txt">COMPARE</span>
                                  <span class="loading_icon" id="load1_${courseId}" style="display:none;"></span>
                                  <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                                  </a>
                                  </div>
                                  <div id="defaultpop_${courseId}" class="sta" style="display: block;">
                                    <%
                                        String userId = new SessionData().getData(request, "y");
                                        if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
                                    %>
                                        <a class="view_more" href="/degrees/comparison">View comparison</a>
                                    <%}else{%>
                                        <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
                                    <%}%>
                                  </div>
                                  <div id="pop_${courseId}" class="sta" style="display:none;"></div>
                                </div>
                              </c:if>                            
                        </div>
                     </div>                  
                    <div class="dtls" onmouseover="addToComparisonHover(${courseId});" onmouseout="addToComparisonOut(${courseId});"> <%--17_Mar_2015 - Added to show and hide 'Add to comparision' tool tip in hover and out, by Thiyagu G.--%>
                          <div class="blu cf">
                            <div class="codls">
                            
                              <h2><a onclick="sponsoredListGALogging(${courseList.collegeId});gtmproductClick(${rowId.index}, 'pr_to_cd');" href="${courseList.courseDetailUrl}" id="pr_to_cd_${rowId.index}">${courseList.courseTitle}</a></h2>
                              <div class="enreq cf">
                                <div class="blk2 blk5"><!--Added hat image inside SP link by Indumathi.S Mar_29_2016-->
                                <c:if test="${not empty courseList.departmentOrFaculty}">
                                 <c:if test="${not empty courseList.spURL}">
                                      <p class="cnl nw"><a href="${courseList.spURL}">${courseList.departmentOrFaculty}
                                      <img src="<%=capeImg%>" alt="${courseList.departmentOrFaculty}"></a></p>
                                    </c:if>
                                    <c:if test="${empty courseList.spURL}">
                                      <p class="cnl">${courseList.departmentOrFaculty}</p>
                                    </c:if>
                                  </c:if>
                                  <p>${courseList.availableStudyModes}</p>
                                </div>
                                <div class="blk1 mr24">
                                <c:if test="${not empty courseList.ucasCode}">
                                    <span>
                                      <strong>${courseList.ucasCode}</strong> UCAS code
                                    </span> 
                                  </c:if>
                                  <c:if test="${not empty courseList.employmentRate}">                          
                                   <span>
                                    <strong>${courseList.employmentRate}%</strong>
                                    <span class="tooltip_cnr">
                                       <span class="blk_txt">Employment rate </span>
                                       <span class="tool_tip fl">
                                          <i class="fa fa-question-circle fnt_24">
                                            <span class="cmp">
                                              <div class="hdf5"></div>
                                              <div><spring:message code="wuni.tooltip.kis.entry.text" /></div> <%--Changed KIS text and year to App.properties for 15_Nov_2016 By Thiyagu G--%>
                                              <div class="line"></div>
                                            </span>
                                          </i>
                                        </span>
                                    </span>
                                    </span>
                                  </c:if>  
                                  <c:if test="${not empty courseList.courseRank}">                                  
                                    <span>
                                      <strong>${courseList.courseRank}${courseList.rankOrdinal}</strong> 
                                      <span class="tooltip_cnr">
                                       <span class="blk_txt">CompUniGuide subject ranking </span> <%--Changed ranking lable frim Times to CUG for 08_MAR_2016 By Thiyagu G--%>
                                       <span class="tool_tip fl">
                                          <i class="fa fa-question-circle fnt_24">
                                            <span class="cmp">
                                              <div class="hdf5"></div>
                                              <div> <spring:message code="wuni.tooltip.cug.ranking.text" /></div>
                                              <div class="line"></div>
                                            </span>
                                          </i>
                                        </span>
                                    </span>
                                    </span> 
                                  </c:if> 
                                </div>
                                <div class="blk1 rt">
                                 <%if(studyLevelDesc != null && studyLevelDesc!="" && !studyLevelDesc.contains("Postgraduate")){%>
                                  <span class="tooltip_cnr ttip_cnt">
                                  <span class="fad">Entry requirements</span>
                                  <c:if test="${not empty courseList.entryRquirements}">
                                    <span class="tool_tip fl">
                                      <i class="fa fa-question-circle fnt_24">
                                        <span class="cmp">
                                        <div class="hdf5"></div>                                      
                                        <div><spring:message code="wuni.tooltip.entry.requirements.text" /></div> 
                                        <div><spring:message code="wuni.tooltip.entry.requirements.further.info.text" /></div> 
                                        <div class="line"></div>
                                        </span>
                                      </i>
                                    </span>
                                  </c:if>
                                  </span>
                                  <c:if test="${not empty courseList.entryRquirements}">
                                    ${courseList.entryRquirements}
                                  </c:if>
                                  <c:if test="${empty courseList.entryRquirements}">
                                    <span>Please contact Uni directly</span>
                                    </c:if>
                                  <%}else if(studyLevelDesc != null && studyLevelDesc!="" && studyLevelDesc.contains("Postgraduate")){%>
                                     <c:if test="${not empty courseList.fees}">
                                        <div class="blk1 rt tu_fees" style="display:none;">
                                            <span class="fad">Tuition fees</span>
                                            <span><strong>${courseList.fees}</strong>${courseList.feeDuration}</span>
                                        </div>
                                      </c:if>
                                      <c:if test="${empty courseList.fees}">
                                        <div class="blk1 rt tu_fees" style="display:none;">
                                            <span class="fad">Tuition fees</span><br/>
                                            <span>Fee not supplied by uni</span>
                                        </div>
                                        </c:if>
                                  <%}%>
                                </div>
                              </div>
                              <c:if test="${not empty courseList.moduleGroupDetails}">
                                <div class="mdls">
                                  <a class="sh pls" id="module${courseList.moduleGroupId}${rowId.index}" onclick="loadmoduledata('${rowId.index}', '${courseList.moduleGroupId}','${courseList.courseDetailUrl}')">
                                    <em>Show</em>
                                    ${courseList.moduleTitle}
                                  </a>
                                  <div id="module_data_${courseList.moduleGroupId}${rowId.index}" style="display:none;">
                                    <%--Added by Priyaa for Module related changes for Jan 2016 release--%>
                                    <c:if test="${not empty courseList.moduleDetailList}">
                                       <c:forEach var="moduleDetailList" items="${courseList.moduleDetailList}">
                                          <ul>                           
                                                <li>${moduleDetailList} </li>                           
                                          </ul>
                                        </c:forEach>
                                        <a class="vwall" onclick="sponsoredListGALogging(${courseList.collegeId});gtmproductClick(${rowId.index}, 'pr_to_cd_view_modules');" href="${courseList.courseDetailUrl}" >View all modules</a>  
                                     </c:if>
                                    <%--End of change--%>                                  
                                  </div>
                                </div>
                              </c:if>
                              <c:if test="${not empty courseList.addedToFinalChoice}">
                               <c:if test="${courseList.addedToFinalChoice eq 'N'}">
                                  <div class="myFnChLbl" style="display:none;">                    
                                    ${courseList.collegeNameDisplay}#${courseList.collegeId}##${courseList.courseTitle}#${courseList.courseId}
                                  </div>               
                                </c:if>
                              </c:if>
                            </div>
                            
                          <!-- Search Interaction button code for responsive redesign 03_Nov_2015 By S.Indumathi-->
                           <div class="res_btn abtst_v2"> 
                             <a class="cour_info" href="${courseList.courseDetailUrl}">
                               Course Info <i class="fa fa-long-arrow-right"></i></a>
                               <c:if test="${not empty courseList.subOrderItemId}">
                                <c:if test="${courseList.subOrderItemId ne 0}">
                                  <a class="more_info" id="moreInfo">More Info</a>
                                </c:if>
                            </c:if>
                             
                            
                              
                             <div class="btns" id="intBtn"> 
                               <!--Added By Thiyagu G for 24_Nov_2015 Start-->
                                <c:if test="${not empty courseList.subOrderItemId}">
                                  <c:if test="${courseList.subOrderItemId eq 0}">       
                                  <div class="sremail">
                                    <a rel="nofollow" 
                                    target="_blank"
                                    class="req-inf"
                                    onclick="javaScript:nonAdvPdfDownload('${courseList.gaCollegeName}#${courseList.webformPrice}#${courseList.collegeId}#${courseList.subOrderItemId}#${courseList.networkId}#${courseList.subOrderEmailWebform}#${courseList.courseId}#<%=CommonUtil.getImgPath("",0)%><%=assignCollegeLogo%>#COURSE#<%=institutionId%>');"
                                    title="Email ${courseList.collegeNameDisplay}">
                                  Request info<i class="fa fa-caret-right"></i>
                                </a>
                                </div>                              
                                </c:if>
                                </c:if>                             
                            <!--Added By Thiyagu G for 24_Nov_2015 Start-->
                            <c:if test="${empty courseList.subOrderItemId}">                 
                                <div class="sremail">
                                <a rel="nofollow" 
                                    target="_blank"
                                    class="req-inf"
                                    onclick="javaScript:nonAdvPdfDownload('${courseList.gaCollegeName}#${courseList.webformPrice}#${courseList.collegeId}#${courseList.subOrderItemId}#${courseList.networkId}#${courseList.subOrderEmailWebform}#${courseList.courseId}#<%=CommonUtil.getImgPath("",0)%><%=assignCollegeLogo%>#COURSE#<%=institutionId%>');"
                                    title="Email ${courseList.collegeNameDisplay}">
                                  Request info<i class="fa fa-caret-right"></i>
                                </a>
                                </div>
                            </c:if>
                            <!--Added By Thiyagu G for 24_Nov_2015 End--> 
                            <c:if test="${not empty courseList.subOrderEmailWebform}">
                                <div class="premail">
                                <a rel="nofollow" 
                                    target="_blank"
                                    class="req-inf"
                                    onclick="sponsoredListGALogging(${courseList.collegeId});GAInteractionEventTracking('emailwebform', 'interaction', 'email webform', '${courseList.gaCollegeName}', ${courseList.webformPrice}); cpeEmailWebformClick(this,'${courseList.collegeId}','${courseList.subOrderItemId}','${courseList.networkId}','${courseList.subOrderEmailWebform}');" 
                                    href="${courseList.subOrderEmailWebform}" 
                                    title="Email ${courseList.collegeNameDisplay}">
                                  Request info<i class="fa fa-caret-right"></i>
                                </a>
                                </div>
                              </c:if>
                              <%pros_search_name = request.getAttribute("prospectus_search_name") != null ? common.replaceURL(request.getAttribute("prospectus_search_name").toString()) : "0";%>
                              <c:if test="${empty courseList.subOrderEmailWebform}">
                              <c:if test="${not empty courseList.subOrderEmail}">
                                  <div class="premail">
                                  <a rel="nofollow"
                                      class="req-inf"
                                      onclick="sponsoredListGALogging(${courseList.collegeId});GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '${courseList.gaCollegeName}');adRollLoggingRequestInfo('${courseList.collegeId}',this.href);return false;"                
                                      href="<SEO:SEOURL pageTitle="sendcollegemail" >${courseList.collegeName}#${courseList.collegeId}#${courseList.courseId}#<%=pros_search_name%>#n#${courseList.subOrderItemId}</SEO:SEOURL>"
                                      title="Email ${courseList.collegeNameDisplay}" >
                                    Request info<i class="fa fa-caret-right"></i>
                                  </a>   <%--13-JAN-2015  modified for adroll marketing--%>
                                  </div>
                               </c:if>
                              </c:if>
                              <c:if test="${not empty  courseList.subOrderProspectusWebform}">
                                <div class="prprospectus">
                                <a rel="nofollow" 
                                    target="_blank" 
                                    class="get-pros" 
                                    onclick="sponsoredListGALogging(${courseList.collegeId});GAInteractionEventTracking('prospectuswebform', 'interaction', 'prospectus webform', '${courseList.gaCollegeName}', ${courseList.webformPrice}); cpeProspectusWebformClick(this,'${courseList.collegeId}','${courseList.subOrderItemId}','${courseList.networkId}','${courseList.subOrderProspectusWebform}');" 
                                    href="${courseList.subOrderProspectusWebform}" 
                                    title="Get <%=collegeNameDisplay%> Prospectus">
                                    Get prospectus<i class="fa fa-caret-right"></i></a>
                                </div>    
                              </c:if>
                              <c:if test="${empty courseList.subOrderProspectusWebform}">
                              <c:if test="${not empty courseList.subOrderProspectus}">
                                  <div class="prprospectus">
                                  <a onclick="sponsoredListGALogging(${courseList.collegeId});GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '${courseList.gaCollegeName}'); return prospectusRedirect('/degrees','${courseList.collegeId}','${courseList.courseId}','<%=pros_search_name%>','','${courseList.subOrderItemId}');"
                                      class="get-pros" 
                                      title="Get <%=collegeNameDisplay%> Prospectus" >
                                      Get prospectus<i class="fa fa-caret-right"></i></a>
                                  </div>    
                               </c:if>
                              </c:if>    
                              <c:if test="${not empty courseList.subOrderWebsite}">                  
                                <div class="prwebsite">                               
                                <a rel="nofollow" 
                                    target="_blank" 
                                    class="visit-web"
                                    id="${courseList.collegeId}" 
                                    onclick="sponsoredListGALogging(${courseList.collegeId});GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${courseList.gaCollegeName}', ${courseList.webformPrice}); cpeWebClickWithCourse(this,'${courseList.collegeId}','${courseList.courseId}' ,'${courseList.subOrderItemId}','${courseList.networkId}','${courseList.subOrderWebsite}');addBasket('${courseList.courseId}', 'O', 'visitweb','basket_div_${courseList.courseId}', 'basket_pop_div_${courseList.courseId}', '', '', '<%=common.replaceSpecialCharacter(paramCollegeName)%>');" 
                                    href="${courseList.subOrderWebsite}&courseid=${courseList.courseId}"
                                    title="<%=collegeNameDisplay%> website">                                                                                       
                                  Visit website<i class="fa fa-caret-right"></i></a>                              
                                </div>
                             </c:if>      
                              <c:if test="${not empty requestScope.totalOpendays}">
                                 <c:if test="${not empty requestScope.opendaySuborderItemId}">
                                 <c:if test="${requestScope.totalOpendays gt 0}">
                                  <c:if test="${requestScope.totalOpendays eq 1}">
                                      <div class="propendays">
                                        <a rel="nofollow"
                                           class="req-inf sr_opd opd_grn"
                                           target="_blank"
                                           onclick="sponsoredListGALogging(${courseList.collegeId});
                                                    GAInteractionEventTracking('visitwebsite', 'interaction', 'Reserve A Place', '<%=collegeNameGa%>');
                                                    addOpenDaysForReservePlace('${requestScope.openDate}', '${requestScope.openMonthYear}', '<%=paramCollegeId%>', '${requestScope.opendaySuborderItemId}', '${requestScope.networkIdOD}'); 
                                                    cpeODReservePlace(this,'<%=paramCollegeId%>','${requestScope.opendaySuborderItemId}','${requestScope.networkIdOD}','${requestScope.bookingUrl}');
                                                    GANewAnalyticsEventsLogging('open-days-type', '${eventCategoryNameGa}', '<%=collegeNameGa%>');"
                                           href="${requestScope.bookingUrl}"
                                           title="<spring:message code="book.open.day.button.text"/>" >
                                           <spring:message code="book.open.day.button.text"/><i class="fa fa-caret-right"></i>
                                         </a>   
                                       </div>
                                     </c:if>
                                      <c:if test="${requestScope.totalOpendays ne 1}">
                                       <div class="propendays">
                                         <a rel="nofollow"
                                            class="req-inf sr_opd opd_grn"
                                            <%-- onclick="sponsoredListGALogging(${courseList.collegeId});GAInteractionEventTracking('visitwebsite', 'engagement', 'Book Open Day Request', '<%=common.replaceSpecialCharacter(paramCollegeName)%>');showOpendayPopup('<%=paramCollegeId%>', '');" --%> 
                                            href="<SEO:SEOURL pageTitle="uni-open-days" ><%=paramCollegeName%>#<%=paramCollegeId%>#<%=collegeLocation%></SEO:SEOURL>"
                                            title="<spring:message code="book.open.day.button.text"/>" >
                                            <spring:message code="book.open.day.button.text"/><i class="fa fa-caret-right"></i>
                                          </a>   
                                        </div>
                                    </c:if>
                                   </c:if>
                                </c:if>
                              </c:if>
                            </div> 
                            </div>
                           </div>
                          </div>
                         </div>
                         <!--Added by Indumathi.S For including banners Nov-03-15 Rel-->
                         <jsp:include page="/jsp/search/include/searchDrawAdSlots.jsp">
                           <jsp:param name="position" value="${rowId.index}"/>
                           <jsp:param name="showBanner" value="<%=advertiserFlag%>"/>
                           <jsp:param name="showDesktop" value="false"/>
                           <jsp:param name="fromPage" value="providersearchresults"/>
                           <jsp:param name="totalResults" value="<%=searchhits%>"/>
                         </jsp:include>

                       
                       </c:forEach>
                      </c:if>
                    </div>
                  <%if(searchhits > 10){%> <!--Condition for showing pagination div added by Prabha on 03_NOV_2015_REL-->
                  <%--Changed pagination design based on new mockup, 24_Feb_2015 By Thiyagu G--%>
                  <div class="pr_pagn">
                    <jsp:include page="/jsp/search/searchredesign/newPagination.jsp">
                      <jsp:param name="pageno" value="<%=pageno%>"/>
                      <jsp:param name="pagelimit"  value="10"/>
                      <jsp:param name="searchhits" value="<%=searchhits%>"/>
                      <jsp:param name="recorddisplay" value="10"/>
                      <jsp:param name="displayurl_1" value="<%=urldata_1%>"/>
                      <jsp:param name="displayurl_2" value="<%=urldata_2%>"/>
                      <jsp:param name="firstPageSeoUrl" value="<%=firstPageSeoUrl%>"/>  
                      <jsp:param name="universityCount" value="1"/>
                      <jsp:param name="resultExists" value="Y"/>
                      <jsp:param name="action" value=""/>    
                    </jsp:include>
                  </div>
                  <%}%>
                  <%--Added key stats pod and provider reviews section, 24_Feb_2015 By Thiyagu G--%>
             
                      <div class="pr_key">
                        <div class="rich_profile">
                            <jsp:include page="/jsp/search/includeCdPage/keyStatsInfo.jsp">
                                <jsp:param name="keyStatsViewMore" value="Y" />
                            </jsp:include>
                        </div>
                      </div>
                <c:if test="${not empty requestScope.listOfUserReviews}">
                      <div class="pr_reviews pr_rrev">
                        <jsp:include page="/jsp/search/searchredesign/providerReviewsPod.jsp"/>
                      </div>                  
                  </c:if>
                </div>
              </div>              
            </div>          
        </div>      
      </div>
    </div>
    <input type="hidden" id="currentcompare" value=""/>
    <input type="hidden" id="srcEvent"/>
    <input type="hidden" id="tickerNonRespPage"/>
    <input type="hidden" id="paramStudyLevelId" value="<%=paramStudyLevelId%>"/>
    <input type="hidden" id="exitOpenDayResFlg" value="<%=exitOpenDayRes%>"/>
    <%-- wu582_20181023 - Sabapathi: added studymodeId for stats logging  --%>
    <input type="hidden" id="studymodeId" value="<%=studymodeId%>"/>
     <input type="hidden" id="odEvntAction" value="<%=odEvntAction%>"/>
     <input type="hidden" id="courseTitles" name="courseTitles" value="${all_course_title}" />
     <input type="hidden" id="courseId" name="courseId" value="${all_course_id}" />
     <input type="hidden" id="courseDomesticfee" name="courseDomesticfee" value="${all_course_domestic_fee}" />
     <input type="hidden" id="eCommPageName" name="eCommPageName" value="Provider Results page"/>
     <input type="hidden" id="subjectName" name="subjectName" value="${subjectName}"/>
     <input type="hidden" id="instName" name="instName" value="${instNames}"/>
     <input type="hidden" id="pageNo" name="pageNo" value="<%=pageno%>"/>
     <input type="hidden" id="GTMLDCS" value="<%=request.getAttribute("GTMLDCS")%>" />
     <input type="hidden" id="subjectL1" value="<%=request.getAttribute("subjectL1")%>" />
     <input type="hidden" id="subjectL2" value="<%=request.getAttribute("subjectL2")%>" />
     <input type="hidden" id="gtmJsonDataPR" name="gtmJsonDataPR" />
    <jsp:include page="/jsp/common/wuFooter.jsp" />
     <jsp:include page="/jsp/common/eCommerceTracking.jsp" />
    <input type="hidden" name="isclearingPage" id="isclearingPage" value="<%=clearingPage%>"/>
    <input type="hidden" name="hdrMenuHide" id="hdrMenuHide" value="READREVIEWS" />
  <script type="text/javascript">
  loadFirstDeliveryProvider('<%=first_mod_group_id%>');
  ProviderdefaultOpenFilter('<%=paramStudyLevelId%>');
  providerPageViewLogging('<%=paramCollegeId%>','PROVIDER_RESULTS_LIST'); //9_Dec_14 Added by Amir for Provider logging 
  </script>  
  <script type="text/javascript"> 
     GAInteractionEventTracking('', 'Provider results', '<%=advNonAdv%>', '<%=evntProviderName%>', '0'); 
  </script>  
  </body>