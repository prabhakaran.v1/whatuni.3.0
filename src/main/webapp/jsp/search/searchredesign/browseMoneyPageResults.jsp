<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.SessionData,WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, mobile.util.MobileUtils" autoFlush="true" %>
<c:set var="pageName">
  <tiles:getAsString name="pagename3" ignore="true"/>
</c:set>
<% 
String q =!GenericValidator.isBlankOrNull((String)request.getAttribute("q")) ? (String)request.getAttribute("q") : "";
String university = !GenericValidator.isBlankOrNull((String)request.getAttribute("university")) ? (String)request.getAttribute("university") : "";
String subject = !GenericValidator.isBlankOrNull((String)request.getAttribute("subject")) ? (String)request.getAttribute("subject") : "";
String location = !GenericValidator.isBlankOrNull((String)request.getAttribute("location")) ? (String)request.getAttribute("location") : "";
String userScore = !GenericValidator.isBlankOrNull((String)request.getParameter("score")) ? (String)request.getParameter("score") : "0";

String exitOpenDayRes = "" , odEvntAction = "false";
    if(session.getAttribute("exitOpRes")!=null && !"".equals(session.getAttribute("exitOpRes"))){
      exitOpenDayRes = (String)session.getAttribute("exitOpRes");
    }    
    if(!"".equals(exitOpenDayRes) && "true".equals(exitOpenDayRes)){
      odEvntAction = "true";
    }
  String pageno = request.getAttribute("pageno") != null ? (String)request.getAttribute("pageno") : "1";
  String urldata_1 = (String)request.getAttribute("URL_1");
  String urldata_2 = (String)request.getAttribute("URL_2");
  String urlString  = urldata_1 + pageno + urldata_2;
  String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "NULL";
  String searchhits = (String) request.getAttribute("universityCount");
  String paramlocationValue = (request.getAttribute("paramlocationValue") !=null && !request.getAttribute("paramlocationValue").equals("null") && String.valueOf(request.getAttribute("paramlocationValue")).trim().length()>0 ? String.valueOf(request.getAttribute("paramlocationValue")) : ""); 
  String paramSubjectCode = (request.getAttribute("paramSubjectCode") !=null && !request.getAttribute("paramSubjectCode").equals("null") && String.valueOf(request.getAttribute("paramSubjectCode")).trim().length()>0 ? String.valueOf(request.getAttribute("paramSubjectCode")) : "");  
  String paramStudyLevelId = (request.getAttribute("paramStudyLevelId") !=null && !request.getAttribute("paramStudyLevelId").equals("null") && String.valueOf(request.getAttribute("paramStudyLevelId")).trim().length()>0 ? String.valueOf(request.getAttribute("paramStudyLevelId")) : ""); 
  String universityCount = (request.getAttribute("universityCount") !=null && !request.getAttribute("universityCount").equals("null") && String.valueOf(request.getAttribute("universityCount")).trim().length()>0 ? String.valueOf(request.getAttribute("universityCount")) : ""); 
  String resultExists = (request.getAttribute("resultExists") !=null && !request.getAttribute("resultExists").equals("null") && String.valueOf(request.getAttribute("resultExists")).trim().length()>0 ? String.valueOf(request.getAttribute("resultExists")) : ""); 
  String qualDesc = (request.getAttribute("qualDesc") !=null && !request.getAttribute("qualDesc").equals("null") && String.valueOf(request.getAttribute("qualDesc")).trim().length()>0 ? String.valueOf(request.getAttribute("qualDesc")) : "");   
  String tmpUcasCode = (request.getAttribute("paramUcasCode") !=null && !request.getAttribute("paramUcasCode").equals("null") && String.valueOf(request.getAttribute("paramUcasCode")).trim().length()>0 ? String.valueOf(request.getAttribute("paramUcasCode")) : "");
  String jacsSubjectName = (String)request.getAttribute("jacsSubjectName");
         jacsSubjectName = (!GenericValidator.isBlankOrNull(jacsSubjectName))?jacsSubjectName:"";
  String searchLocation = (String)request.getAttribute("SEARCH_LOCATION_OR_POSTCODE") != null ? (String)request.getAttribute("SEARCH_LOCATION_OR_POSTCODE") : "";
  String noindexfollow = request.getAttribute("noindex") != null ? "TRUE"  : "FALSE";
  if(firstPageSeoUrl.contains("module=")){
    noindexfollow = "TRUE";
  }
  String studyModeValue = (String) request.getAttribute("hitbox_study_mode");
  if(studyModeValue == null){studyModeValue = "";}
  CommonUtil comUtil = new CommonUtil();  
  CommonFunction commonFun = new CommonFunction();
  String subjectDesc = request.getAttribute("subjectDesc") != null ? request.getAttribute("subjectDesc").toString() : "";   
  int articleLength = 0;
  if(subjectDesc!=null && subjectDesc!=""){
    articleLength = subjectDesc.length();
  }
  String articleText = subjectDesc;
  if(articleLength > 15){
    articleText = subjectDesc.substring(0,13) + "...";
  }
  String studyLevelDesc = request.getAttribute("studyLevelDesc") != null ? (String)request.getAttribute("studyLevelDesc") : "";
  //System.out.println("--->"+studyLevelDesc);
  String studeyLevel = studyLevelDesc;
  String studyLevelOnly = studyLevelDesc;
  String studyLevelTab = studyLevelDesc;
  if(studyLevelTab.indexOf("degree")==-1){
        studyLevelTab = studyLevelTab.indexOf("hnd/hnc") > -1 ? studyLevelTab.toUpperCase() :studyLevelTab;
        studyLevelTab = studyLevelTab + " course";
  }else{
	  studyLevelTab = " course";
  }
  if(!GenericValidator.isBlankOrNull(studeyLevel)){  //29-Oct-2013 SEO Release - Start of change            
     if(studeyLevel.indexOf("degree")==-1){
        studeyLevel = studeyLevel.indexOf("hnd/hnc") > -1 ? studeyLevel.toUpperCase() :studeyLevel;
        studeyLevel = studeyLevel + " degrees";
     }else{
        studeyLevel = studeyLevel.replaceFirst("degree", "Degrees");
        }              
      } //29-Oct-2013 SEO Release - End of change 
  String first_mod_group_id = (String)request.getAttribute("first_mod_group_id");
         first_mod_group_id = (first_mod_group_id!=null && first_mod_group_id.trim().length() >0) ? first_mod_group_id : "";
  String totalCourseCount = (String) request.getAttribute("totalCourseCount"); 
         totalCourseCount = GenericValidator.isBlankOrNull(totalCourseCount)?"":totalCourseCount;
  String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
  String canonicalLink = httpStr + "www.whatuni.com/degrees" + firstPageSeoUrl;
  int totalCount = 0;
  if(totalCourseCount!=""){
    totalCount =  Integer.parseInt(totalCourseCount);
  }
  studyLevelTab = totalCount > 1? studyLevelTab+"s": studyLevelTab;
  String firstTabText = totalCourseCount + " " + studyLevelTab;
  String newsearchJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.newSearchResults.js");
  String pgsRelCanonicalLink = (String)request.getAttribute("pgsCanonicalURL");//13_MAY_2014_REL
  if(!GenericValidator.isBlankOrNull(paramStudyLevelId)){
   canonicalLink = GenericValidator.isBlankOrNull(pgsRelCanonicalLink)?"":pgsRelCanonicalLink;
  }
  String clearingYear = GlobalConstants.CLEARING_YEAR;  
  String clearingonoff = commonFun.getWUSysVarValue("CLEARING_ON_OFF");
  String qString = (String)request.getAttribute("queryStr");
        qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  String clrString = qString;
  if("ON".equalsIgnoreCase(clearingonoff)){
   clrString = !GenericValidator.isBlankOrNull(clrString) ? (clrString.replace("?","?clearing&")):"?clearing";  
  }
  String filter = commonFun.getWUSysVarValue("FILTER");
  boolean mobileFlag = new MobileUtils().userAgentCheck(request);
  String socialBoxJs = CommonUtil.getResourceMessage("wuni.social.box.js", null);
  String btrCls = ""; 
%>
<body>
 <%--Modified code for responsive redesign 03_Nov_2015 By S.Indumathi--%>
  <div class="sr_resp">
    <header class="clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
           <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>
    </header>  
    
    <div class="ad_cnr">
      <div class="content-bg" id="content-blk"> 
        <div class="sr">
          <%--(1)-- microformated Breadcrumb --%>
           <%if(!mobileFlag){%>             
            <jsp:include page="/seopods/breadCrumbs.jsp" >
              <jsp:param name="pageName" value="BROWSE_MONEY_PAGE" />            
            </jsp:include>
            <%}%>
            <%if(!"".equals(paramStudyLevelId) && "M".equalsIgnoreCase(paramStudyLevelId)){%>
              <div class="com fltr_mwrap">              
            <%}else{%>
             <div class="com nw">
            <%}%>
	      <div class="com-lt">
                <div>
                <%if("".equals(tmpUcasCode)){%>
                  <h1 class="result-head reshd2">
                <c:if test="${not empty subjectDesc}">${requestScope.subjectDesc}</c:if>
      <%=studeyLevel%> 
      <c:if test="${not empty searchLocation}"><c:if test="${searchLocation ne 'United Kingdom'}">in <%=searchLocation%></c:if></c:if>
                  </h1>
                  <h2 class="result-head respar2">
                  <c:if test="${not empty universityCount}">
                  <c:out value='${requestScope.universityCount}'/>
                  <c:if test="${universityCount eq 1}">university</c:if>
                  <c:if test="${universityCount gt 1 }">universities</c:if>
                  </c:if>offer <c:if test="${not empty totalCourseCount}"><%=firstTabText%></c:if> including
                  <c:if test="${not empty subjectDesc}"><c:out value='${requestScope.subjectDesc}'/></c:if>
                  </h2>
                  
                  <%--Added text for foundation degree search by Prabha on 05_Jul_2017--%>
                  <%if("foundation degrees".equalsIgnoreCase(studeyLevel)){%>
                    <p class="cal_txt fdtxt_wrp"><span class="fdeg_txt">Sorry if there aren't many results, we are waiting for data from our partner UCAS.</span> </p>
                  <%}%>
                  <%--End of code--%>
                  
                   <%}
                    
                    if(!"".equals(tmpUcasCode)){
                    %>    
                    <h1 class="result-head reshd2">
                    <c:if test="${not empty SEARCH_KEYWORD_OR_SUBJECT}"><%if(!"".equals(tmpUcasCode)){%> Courses linked to <%}%><c:out value='${requestScope.SEARCH_KEYWORD_OR_SUBJECT}'/></c:if>
                    <%if("".equals(tmpUcasCode)){%>
                      <%=studeyLevel%> 
                    <%}
                    if(!GenericValidator.isBlankOrNull(searchLocation) && !("United Kingdom").equals(searchLocation.trim())){
                    %> 
                    in <c:out value='${requestScope.SEARCH_LOCATION_OR_POSTCODE}'/>
                    <%}%>
                    </h1>
                   <h2 class="result-head respar2">
                   <c:if test="${not empty universityCount}">
                   <c:out value="${requestScope.universityCount}"/>
                   <c:if test="${universityCount eq 1}">university</c:if>
                   <c:if test="${universityCount gt 1}">universities</c:if>
                   </c:if>
                   offer <c:if test="${not empty totalCourseCount}"><%=firstTabText%></c:if>
                   <%if(paramSubjectCode!= "" ){%>
                       including <c:if test="${not empty SEARCH_KEYWORD_OR_SUBJECT}"><c:out value="${requestScope.SEARCH_KEYWORD_OR_SUBJECT}"/></c:if>
                   <%}else if(paramSubjectCode == "" && tmpUcasCode=="" && jacsSubjectName =="") {%>
                       with <c:if test="${not empty SEARCH_KEYWORD_OR_SUBJECT}"><c:out value="${requestScope.SEARCH_KEYWORD_OR_SUBJECT}"/></c:if> in the course title
                   <%}else if(tmpUcasCode!="" && jacsSubjectName ==""){%> 
                        having courses with this code.
                    <%}else if(jacsSubjectName!=""){%> 
                       related to <c:if test="${not empty SEARCH_KEYWORD_OR_SUBJECT}"><c:out value="${requestScope.SEARCH_KEYWORD_OR_SUBJECT}"/></c:if>
                   <%}%>
                   </h2>
                   <%--Added text for foundation degree search by Prabha on 05_Jul_2017--%>
                   <%if("foundation degrees".equalsIgnoreCase(studeyLevel)){%>
                     <p class="cal_txt fdtxt_wrp" style="display: inline-block; margin-top: 10px;"><span class="fdeg_txt" style="background: #fef3e0; padding: 4px 12px; color: #fc9723; display:inline-block; border-radius: 2px; border: 1px solid #fdead0;">Sorry if there aren't many results, we are waiting for data from our partner UCAS.</span> </p>
                  <%}%>
                  <%--End of code--%>
                   
                    <%}%>
                </div>
                <jsp:include page="/search/include/didYouMeanPod.jsp" /> 
	      </div>
             
             <div class="com-rt grd_entqual">
              <c:if test="${'Y' eq requestScope.userQualificationExistFlag}">
              <%if(!("L".equalsIgnoreCase(paramStudyLevelId))){ %>	
              <a class="fq edt_qualif" href="javascript:void(0);" onclick="openLightBox('gradefilter', 'EDIT_QUAL');"><spring:message code="edit.grades.label"/></a>
              <%-- <a class="fq pers_srch_lbx edt_qualif" href="javascript:void(0);" onclick="openLightBox('gradefilter', 'EDIT_QUAL');"><spring:message code="edit.grades.label"/></a> --%>
              <%} %>
              </c:if>
              <!-- Mobile optimization changes added by Minu on 10_Nov_2020 -->
              <div class="wu-fltrbtnset">
                <div class="wuclrsr_rsltbtnset fltr_mob"> 
                <a class="wuclr_btn btnbrdr btnyugrd ${(not empty requestScope.paramStudyLevelId and 'Y' eq requestScope.userQualificationExistFlag and 'L' ne requestScope.paramStudyLevelId)  ? 'wuclrr_bactv_hvr':''}" title="Enter grades" href="#" id = gradeOption>
                   <c:choose>
                     <c:when test="${not empty requestScope.paramStudyLevelId and 'Y' eq requestScope.userQualificationExistFlag and 'L' ne requestScope.paramStudyLevelId }">
                      <spring:message code="edit.grades.label"/>
                      <img class="wuclrsr_imgicn" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_tick_white.svg",0)%>" alt="white tick Icon">
                     </c:when>
                     <c:otherwise>
                       <spring:message code="enter.grades.label"/>
                     </c:otherwise>
                   </c:choose>
                     <c:if test="${empty sessionScope.USER_UCAS_SCORE and empty gradeFilterUcasScorePoint}">
                       <span class="yugrd_tipbox" id = gradeOptionToolTip>
                         <span class="yugrd_tipcollft">
                         <span class="yugrd_lftarrw"></span>
                         <div class="request-loader1">
                           <span></span>
                         </div>
                         </span>
                         <span class="yugrd_tipcolrgt">
                           <img class="wuclrsr_imgicn" id="closeGradeOption" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_close_grey.svg",0)%>" alt="Close Icon">
                           <h4><spring:message code="blink.prompt.title"/></h4>
                           <p><spring:message code="blink.prompt.subtitle"/></p>
                         </span>
                       </span>
                     </c:if>
                   </a>
                   <a class="wuclr_btn btnbrdr btnchnaccp" id = "filterResults" title="Filter Results" href="javascript:void(0);" data-pagename='${not empty pageName ? pageName : ""}'><spring:message code="filter.results.label"/></a>
                 </div>
               </div>
            </div>
            <%--Showing message for rollup, 28_Aug_2018 By Sabapathi.S--%>
              <c:if test="${noResultFlag eq 'Y'}">
                <div id="noResultDiv" class="sr_suc_msg rslt_grn">
                  <i class="fa fa-times" onclick="closeNoResultDiv();"></i>
                 <h2>No Results found for your selection.</h2>
                 <p>We are displaying results for undergraduate courses.</p>
               </div>
              </c:if>
            </div>
            <jsp:include page="/jsp/search/include/newSearchArticleTab.jsp" >
              <jsp:param name="subjectDesc" value="<%=subjectDesc%>" />
            </jsp:include>
            
          </div>  
          <c:if test="${empty requestScope.featuredBrandList}">
		    <% btrCls = "bdr_btm";%>
		  </c:if>
		  <c:if test="${COVID19_SYSVAR eq 'ON'}">
		    <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
		    <div class="sr">
					<div class="covid_upt">
						<span class="covid_txt"><%=new SessionData().getData(request, "COVID19_COURSE_TEXT")%></span>
						<a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_blu_arw.svg", 0)%>"></a>
					</div>
				</div>
		  </c:if>
            <input type="hidden" id="searchtype" name="searchType" value="NORMAL_SEARCH"/>
               <div class="srres_cnt fl_w100">
                    <!-- Sort by Navigation -->
                    <div class="srtby_ui fl_w100">
                        <div class="srt_by srtby_mob fl <%=btrCls%>">SORT BY:</div>
                        <div class="lsthdr cf <%=btrCls%>">
                         <jsp:include page="/jsp/search/include/newSortBy.jsp"/>
                         </div>
                         </div>
          <%--Added by Jeya for Featured slot for Jan 2020 release --%>
          <!-- Featured Brand Section Start -->
             <jsp:include page="/jsp/search/include/featuredBrandSales.jsp" />
          <!-- Featured Brand Section Start -->
          <div class="sr-cont">
            <%-- Filter--%>
           <jsp:include page="/jsp/search/include/searchFilters.jsp" >
              <jsp:param name="PAGE_FROM" value="BROWSE_MONEY_PAGE"/>
              <jsp:param name="URLDATA_2" value="<%=urldata_2%>"/>
            </jsp:include>
           
              <%--  Enter Qualification --%>
              
                <div class="srs-rt">
              <c:if test="${'Y' ne requestScope.userQualificationExistFlag }">
              	<%if(!("L".equalsIgnoreCase(paramStudyLevelId))){ %>       
	              <div class="entq_cnt fl_w100 grad_none">
	                <div class="blucnt fl_w100" id="empty_ucas_score">	                  
	                   <h4><spring:message code="enter.grade.title" arguments="${subjectDesc}" htmlEscape="false" argumentSeparator=";"/></h4>
	                    <c:set var="studyLvlDesc" value="<%=studeyLevel%>"/>
                        <p class="fl_w100"><spring:message code="enter.grade.text" arguments="${studyLevelTextForEnterGrade};${subjectDesc}"
                        htmlEscape="false" argumentSeparator=";"/></p>	                   
	                    <div class="subtn_cnt fl">
	                      <div class="vw_crsbtn fl">
	                        <a onclick="openLightBox('gradefilter');" class="ent_qual"><spring:message code="enter.grades.label"/></a>
	                      </div>
	                    </div>
	                  </div>
	                </div>
	                <%} %>	               
	             </c:if>       
	                  
            <%--  Enter Qualification --%>

              <%-- Search results --%>
              <jsp:include page="/jsp/search/include/newSearchResults.jsp" >   
                <jsp:param name="studyLevelModified" value="<%=studeyLevel%>"/>
                <jsp:param name="studyLevelOnly" value="<%=studyLevelOnly%>"/> 
                <jsp:param name="PAGE_FROM" value="BROWSE_MONEY_PAGE"/>
              </jsp:include>
          <%--Changed pagination design based on new mockup, 24_Feb_2015 By Thiyagu G--%>
          <div class="pr_pagn">      
            <% 
            int numofpages = 0;
             int noOfrecords = 0;
             if(!GenericValidator.isBlankOrNull(searchhits)){	
            	 noOfrecords =  Integer.parseInt(searchhits.replaceAll(",", ""));
             }
                 numofpages =  noOfrecords / 10;
             if(noOfrecords % 10 > 0) {  numofpages++; }
             String pageNameh = "/jsp/search/searchredesign/newPagination.jsp";
             if(numofpages >1 && ("1").equals(pageno)){  
               pageNameh = "/jsp/search/searchredesign/searchInitialPagination.jsp";
              }%>
            <jsp:include page="<%=pageNameh%>">
              <jsp:param name="pageno" value="<%=pageno%>"/>
              <jsp:param name="pagelimit"  value="10"/>
              <jsp:param name="searchhits" value="<%=searchhits%>"/>
              <jsp:param name="recorddisplay" value="10"/>
              <jsp:param name="displayurl_1" value="<%=urldata_1%>"/>
              <jsp:param name="displayurl_2" value="<%=urldata_2%>"/>
              <jsp:param name="firstPageSeoUrl" value="<%=firstPageSeoUrl%>"/>
              <jsp:param name="universityCount" value="<%=universityCount%>"/>
              <jsp:param name="resultExists" value="<%=resultExists%>"/>
            </jsp:include>
          </div>
            </div>
           </div>
         </div>
       </div>
      <%--(2)-- Search relsult info --%>  
      <form:form action="/*-courses/csearch" commandName="searchBean">
        <input type="hidden" id="sortingUrl" value="<%=request.getAttribute("sortingUrl")%>" />
        <input type="hidden" id="subjectname" value="<%=request.getAttribute("subjectName")%>" />
        <input type="hidden" id="studyleveldesc" value="<%=request.getAttribute("studyLevelDesc")%>" />
        <input type="hidden" id="paramStudyLevelId" value="<%=request.getAttribute("paramStudyLevelId")%>" />
        <input type="hidden" id="paramSubjectCode" value="<%=request.getAttribute("paramSubjectCode")%>" />
        <input type="hidden" id="paramlocationValue" value="<%=request.getAttribute("paramlocationValue")%>" />
        <input type="hidden" id="featuredSubject" value="${subjectCode}"/>
        <input type="hidden" id="dropdownCollegeId" value="uc" />        
        <input type="hidden" id="userid" value='<%=new SessionData().getData(request, "y")%>' />
        <input type="hidden" name="contextPath" id="contextPath" value="/degrees" />
        <input type="hidden" id="pageName" value="moneyPage" />
        <input type="hidden" id="qualDesc" value="<%=qualDesc%>" />
        <form:hidden path="orderId" name="searchBean" id="order_by_id"/>
        <form:hidden path="refineFlag" name="searchBean" id="refineFlag"/>
        <form:hidden path="omnitureFlag" name="searchBean" id="omnitureFlag"/>
		
      </form:form>  
    </div>
<input type="hidden" id="instIds" name="instIds" value="${instIds}" />
        <input type="hidden" id="instNames" name="instNames" value="${instNames}" />
        <input type="hidden" id="qualificationInSR" name="qualification1" value="${qualification1}" />
        <input type="hidden" id="pageNum" value="<%=request.getAttribute("pageno")%>" />
        <input type="hidden" id="eCommPageName" value="Search Results Page" />
        <input type="hidden" id="GTMLDCS" value="<%=request.getAttribute("cDimLDCS")%>" />
        <input type="hidden" id="subjectL1" value="<%=request.getAttribute("subjectL1")%>" />
        <input type="hidden" id="subjectL2" value="<%=request.getAttribute("subjectL2")%>" />
        <input type="hidden" id="gtmJsonDataSR" name="gtmJsonDataSR"/>
        
  <input type="hidden" id="tickerNonRespPage"/>
  <input type="hidden" id="exitOpenDayResFlg" value="<%=exitOpenDayRes%>"/>
      <input type="hidden" id="odEvntAction" value="<%=odEvntAction%>"/>
  <jsp:include page="/jsp/common/wuFooter.jsp" />
  <%-- Added to hide chatbot popup in mobile view on 18-AUG-2020 by Hemalatha.K --%>
  <c:set var="mobileFlag" value="<%=comUtil.getMobileFlag(request) %>" />
  <c:if test="${not mobileFlag}">
    <jsp:include page="/jsp/common/socialBox.jsp" />
  </c:if>
  <jsp:include page="/jsp/common/eCommerceTracking.jsp" />
  <input type="hidden" name="hdrMenuHide" id="hdrMenuHide" value="READREVIEWS" />
  <input type="hidden" id="userid" value='<%=new SessionData().getData(request, "y")%>' />
   <input type="hidden" id="subject" name="subject" value="<%=subject%>"/>
  <input type="hidden" id="q" name="q" value="<%=q%>"/>
  <input type="hidden" id="university" name="university" value="<%=university%>"/> 
  <input type="hidden" id="location" name="location" value="<%=location%>"/>
  <input type="hidden" id="queryStr" name="queryStr" value="<%=qString%>"/>
  <input type="hidden" id= "userScore" value="<%=userScore%>"/>
  <input type="hidden" id= selected_qual name="selected_qual" value="<%=request.getAttribute("selected_qual")%>"/>
  <input type="hidden" id="userUcasPoint" value="${sessionScope.USER_UCAS_SCORE}" />
  <input type="hidden" id="userQualificationExistFlag" value="${requestScope.userQualificationExistFlag}"/>
</div>
</div>
</div>
<script type="text/javascript">
loadFirstDelivery(<%=first_mod_group_id%>);
loadfirstArticle();
</script>
<%--Set cookie to stop grade filter popup within the session for 11_Aug_2015 By Thiyagu G.--%>
<script type="text/javascript">
loadGradePopup();
</script>
</body>
