<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction" %>
<%
 String cssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.searchpage.css");
String searchCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.clearing.searchpage.css");
 String commonCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.css");
 String domainSpecPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
 String mainHeaderCSS = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);//Added this css for Header changes by Hema.S on 23_OCT_2018_REL
 String isWhatuniGoClearingPage = request.getParameter("isWhatuniGoClearingPage");
 String wuniMainHeaderCss = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);
 String wuniMobileStyleCss = CommonUtil.getResourceMessage("wuni.whatuni.mobile.styles.css", null); 
 String clsrMainStylesCss = CommonUtil.getResourceMessage("wuni.clsr.main.styles.css", null); 
%>
<meta http-equiv="content-language" content=" en-gb "/><%--21-Jan-2014--%>
<jsp:include page="/jsp/common/includeIconImg.jsp"/><%--Icon images added by Prabha on 31_May_2016--%>


<% String envName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");   
if("Y".equalsIgnoreCase(isWhatuniGoClearingPage)){ 

if(("LIVE").equals(envName)){ %>
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuniMainHeaderCss%>" media="screen">
	<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=clsrMainStylesCss%>" media="screen">
	<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=wuniMobileStyleCss%>" media="screen">
<%} else if("TEST".equals(envName)){%>
  <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=wuniMainHeaderCss%>" media="screen">
	<link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=clsrMainStylesCss%>" media="screen">
	<link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/mobile/<%=wuniMobileStyleCss%>" media="screen"> 
<%} else if("DEV".equals(envName)){%>
  <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=wuniMainHeaderCss%>" media="screen">
	<link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=clsrMainStylesCss%>" media="screen">
	<link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/mobile/<%=wuniMobileStyleCss%>" media="screen">
<%}

} else {%>
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=cssName%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=searchCssName%>" media="screen" />
<%if(("LIVE").equals(envName)){
 %>
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=mainHeaderCSS%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=commonCssName%>" media="screen" />
<%} else if("TEST".equals(envName)){%>
 <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=mainHeaderCSS%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=commonCssName%>" media="screen" />
<%} else if("DEV".equals(envName)){%>
 <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=mainHeaderCSS%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=commonCssName%>" media="screen" />
<%} }%>
<jsp:include page="/jsp/common/abTesting.jsp"/>
<jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
	<jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>
<%-- Facebook pixel tracking script included in head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/facebookPixelTracking.jsp"/>
