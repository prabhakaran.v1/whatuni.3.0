<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<c:if test="${not empty requestScope.subjectGuidesList}">
  <%
    String studyLevelId = request.getAttribute("studyLevelId") != null ? String.valueOf(request.getAttribute("studyLevelId")) :"";
    String clearingFlag = request.getAttribute("clearingFlag") != null ? String.valueOf(request.getAttribute("clearingFlag")) :"";
    String studyLevelName = "";
    String levelName= "";
    if("M".equalsIgnoreCase(studyLevelId) && "CLEARING".equalsIgnoreCase(clearingFlag)){studyLevelName = "Clearing degrees";}
    else if("M".equalsIgnoreCase(studyLevelId)){studyLevelName = "Undergraduate";}
    else if("L".equalsIgnoreCase(studyLevelId)){studyLevelName = "Postgraduate degrees";}
    else if("A".equalsIgnoreCase(studyLevelId)){studyLevelName = "Foundation degrees";}
    else if("T".equalsIgnoreCase(studyLevelId)){studyLevelName = "Access and Foundation degrees";}
    else if("N".equalsIgnoreCase(studyLevelId)){studyLevelName = "HND/HNCs";}
    String totalCount = request.getAttribute("subGuidTotalCount") != null ? request.getAttribute("subGuidTotalCount").toString() : "0";
    int sgLength=0;
    int sgSecLength=0;
    if(Integer.parseInt(totalCount) > 8){
      sgLength = (Integer.parseInt(totalCount)/3)+1;
    }else{
      sgLength = Integer.parseInt(totalCount);
    }
    sgSecLength = sgLength + sgLength;
  %>
    <div id="sgNormal" class="subgd_div fl"> 
      <%if("M".equalsIgnoreCase(studyLevelId) && !"CLEARING".equalsIgnoreCase(clearingFlag)){%>
        <h3>
          <c:if test="${not empty requestScope.popularSubjectsCnt}">
            <c:if test="${requestScope.popularSubjectsCnt eq 0}">
              Guides to popular subjects
            </c:if>
            <c:if test="${requestScope.popularSubjectsCnt ne 0}">
              Subject guides matching your search
            </c:if>
          </c:if>
        </h3>
      <%}else{%>
        <h3>Guides to popular <%=studyLevelName.toLowerCase()%> subjects</h3>
      <%}%>
      <div class="fl subgd_li">
        <div class="fl subgd_li1 cs_fst_column">
          <ul>
            <c:forEach var="subjectGuides" items="${requestScope.subjectGuidesList}" varStatus="i" end="<%=Integer.valueOf(sgLength)%>">          
              <li><a href="${subjectGuides.subjectGuideUrl}">${subjectGuides.description}</a>
              <span style="width:${subjectGuides.readCountPercent}%;" class="per_clr"/>
              </li>
            </c:forEach>
          </ul>
        </div>
        <div class="fl subgd_li1 subgd_spc cs_fst_column">
          <ul>
            <c:forEach var="subjectGuides" items="${requestScope.subjectGuidesList}" varStatus="i" begin="<%=Integer.valueOf(sgLength)%>" end="<%=Integer.valueOf(sgLength)%>">          
              <li><a href="${subjectGuides.subjectGuideUrl}">${subjectGuides.description}</a>
              <span style="width:${subjectGuides.readCountPercent}%;" class="per_clr"/>
              </li>            
            </c:forEach>
          </ul>
        </div>
        <div class="fl subgd_li1 cs_fst_column">
          <ul>
            <c:forEach var="subjectGuides" items="${requestScope.subjectGuidesList}" varStatus="i" begin="<%=Integer.valueOf(sgSecLength)%>">          
              <li><a href="${subjectGuides.subjectGuideUrl}">${subjectGuides.description}</a>
              <span style="width:${subjectGuides.readCountPercent}%;" class="per_clr"/>
              </li>          
            </c:forEach>
          </ul>
        </div>
      </div>      
    </div>    
</c:if>