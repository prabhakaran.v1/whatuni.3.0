<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import=" WUI.utilities.CommonUtil, WUI.utilities.SessionData, WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator" %>
<%
  String clearingUserType = (String)session.getAttribute("USER_TYPE");
  String navSearchValue = "";
  String studyLevelId = request.getAttribute("studyLevelId") != null ? String.valueOf(request.getAttribute("studyLevelId")) :"";
  String clearingFlag = request.getAttribute("clearingFlag") != null ? String.valueOf(request.getAttribute("clearingFlag")) :"";
  String studyLevelName = "";
  String levelName= "";
  if("M".equalsIgnoreCase(studyLevelId) && "CLEARING".equalsIgnoreCase(clearingFlag)){studyLevelName = "Clearing degrees";navSearchValue = GlobalConstants.CLEARING_NAV_TXT;}
  
  else if("M".equalsIgnoreCase(studyLevelId)){studyLevelName = "DEGREES";navSearchValue = "Undergraduate"; }
  
  else if("L".equalsIgnoreCase(studyLevelId)){studyLevelName = "Postgraduate degrees";navSearchValue = "Postgraduate";}
  else if("A".equalsIgnoreCase(studyLevelId)){studyLevelName = "Foundation degrees";navSearchValue = "Foundation degree";}
  else if("T".equalsIgnoreCase(studyLevelId)){studyLevelName = "Access and Foundation degrees";navSearchValue = "Access & Foundation";}//Changed the Access and foundation to Access & Foundation Indumathi.S Jan-27-16
  else if("N".equalsIgnoreCase(studyLevelId)){studyLevelName = "HND/HNCs";navSearchValue = "HND/HNC";}

%>
<div class="pros_search fl">
  <c:if test="${not empty requestScope.clearingSwitchFlag}">  
  <c:if test="${requestScope.clearingSwitchFlag eq 'OFF' }">
      <form id="courseSearchForm" method="post" action="/home.html" class="topsearch" onsubmit="javascript:return csSearchSubmit('OFF');">
        <div class="sel_degcnr">
          <div class="sel_deg">
            <input type="text" name="qualification" id="navQualCS" class="sld_deg fnt_lbk" value="<%=navSearchValue%>" readonly="readonly" onblur="setTimeout('hideBlockCS()',500);" onclick="javascript:toggleQualListCS();" />
          </div>                   
          <ul id="navQualListCS" class="drop_deg" style="display: none;">
            <li onclick="javascript:setQualCS(this);">Undergraduate</li>
            <li onclick="javascript:setQualCS(this);">HND / HNC</li>            
            <li onclick="javascript:setQualCS(this);">Foundation degree</li>
            <li onclick="javascript:setQualCS(this);">Access &amp; foundation</li>
            <li onclick="javascript:setQualCS(this);">Postgraduate</li>
            <%--<li onclick="javascript:setQualCS(this);"> UCAS CODE </li>--%>
          </ul>                  
        </div>
        <fieldset class="fl">
          <input class="fnt_lrg tx_bx" type="text" autocomplete="off" name="csKeyword" id="navKwdCS" onkeyup="autoCompleteKeywordBrowseCS(event,this);javascript:setCourseAutoCompleteClass();" value="Please enter subject or uni" onfocus="javascript:clearNavSearchTextCS(this);" onclick="javascript:clearNavSearchTextCS(this);" onblur="javascript:setNavSearchText(this);" onkeypress="javascript:if(event.keyCode==13){return csSearchSubmit('OFF');}"/>
          <input type="hidden" id="csKeyword_hidden" value="" />
          <input type="hidden" id="csKeyword_id" value="" />
          <input type="hidden" id="csKeyword_display" value="" />
          <input type="hidden" id="csKeyword_url" value="" />
          <input type="hidden" id="csKeyword_alias" value="" />
          <input type="hidden" id="csKeyword_location" value=""/>                    
          <input type="button" onclick="csSearchSubmit('OFF');" class="icon_srch fr" value=""/>
          <i class="fa fa-search fa-1_5x"></i>                  
        </fieldset>
        <input type="hidden" id="csmatchbrowsenode" value=""/>
        <input type="hidden" id="selQualCS" value=""/>        
   </form>
    </c:if>
    <c:if test="${requestScope.clearingSwitchFlag eq 'ON' }">
      <%//if(!GenericValidator.isBlankOrNull(clearingUserType) && "CLEARING".equals(clearingUserType)){ navSearchValue = "Clearing " +GlobalConstants.CLEARING_YEAR;}
      %>
      <form id="courseSearchForm" method="post" action="/home.html" class="topsearch" onsubmit="javascript:return csSearchSubmit('OFF');" commandName="coursejourneybean">
        <div class="sel_degcnr">
          <div class="sel_deg">
            <input type="text" name="qualification" id="navQualCS" class="sld_deg fnt_lbk" value="<%=navSearchValue%>" readonly="readonly" onblur="setTimeout('hideBlockCS()',500);" onclick="javascript:toggleQualListCS();" />
          </div>
          <ul id="navQualListCS" class="drop_deg" style="display: none;">
           <%-- <li class="fl" id="nav-clearing-2015" onclick="javascript:setQualCS(this);updateSearchBars('desktop','clearing');"><%=GlobalConstants.CLEARING_NAV_TXT%></li> --%>
            <li class="fl" onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');">Undergraduate</li>
            <li class="fl" onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');">HND / HNC</li>            
            <li class="fl" onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');">Foundation degree</li>
            <li class="fl" onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');">Access &amp; foundation</li>
            <li class="fl" onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');">Postgraduate</li>
            <%--<li class="fl" onclick="javascript:setQualCS(this);updateSearchBars('desktop','non-clearing');"> UCAS CODE </li>--%>
          </ul>                  
        </div>
        <fieldset class="fl">
          <input type="text" value="Please enter subject or uni" class="fnt_lrg tx_bx" id="navKwdCS" autocomplete="off" name="csKeyword" onkeyup="autoCompleteKeywordBrowseCS(event,this);javascript:setCourseAutoCompleteClass();" onfocus="javascript:clearNavSearchTextCS(this);" onclick="javascript:clearNavSearchTextCS(this);" onblur="javascript:setNavSearchText(this);" onkeypress="javascript:if(event.keyCode==13){return csSearchSubmit('ON');}"/>
          <input type="hidden" id="csKeyword_hidden" value="" />
          <input type="hidden" id="csKeyword_id" value="" />
          <input type="hidden" id="csKeyword_display" value="" />
          <input type="hidden" id="csKeyword_url" value="" />
          <input type="hidden" id="csKeyword_alias" value="" />
          <input type="hidden" id="csKeyword_location" value=""/>                    
          <input type="button" onclick="csSearchSubmit('ON');" class="icon_srch fr" value=""/>
          <i class="fa fa-search fa-1_5x"></i>                  
        </fieldset>
        <input type="hidden" id="csmatchbrowsenode" value=""/>
        <input type="hidden" id="selQualCS" value=""/>        
       </form>        
    </c:if>
    </c:if>    
</div> 