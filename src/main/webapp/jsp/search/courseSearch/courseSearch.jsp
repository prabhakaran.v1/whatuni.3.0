
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<%@page import=" WUI.utilities.CommonUtil, WUI.utilities.SessionData, WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction" %>

<body>
<header class="md clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
</header>
<section class="Main_Cnr cr_srch">
      <div class="top_scroll_order">
        <div id="scrollfreezediv" class="">
          <div class="ad_cnr">
            <div class="content-bg">
                <div class="sbcr mb-10 pros_brcumb">
                  <div class="bc">
                    <div class="bcrumb_blue">
                    <c:if test="${not empty requestScope.csBreadCrumb}">  
                        ${requestScope.csBreadCrumb}                        
                      </c:if>
                    </div>
                  </div>
                </div>     
                    <h2 class="fnt_lbd">Find a course</h2>          
                  <jsp:include page="/jsp/search/courseSearch/include/courseSearchPod.jsp"/>              
            </div>
          </div>
        </div>
      </div>
      <div class="ad_cnr">
        <div class="content-bg">
          <div class="sub_cnr">          
            <%--Modified by Indumathi.S Feb-16-2016 IWantToBe redesign--%>
            <jsp:include page="/jsp/iwanttobe/iwanttobeWidget.jsp"/>
            
            <jsp:include page="/jsp/search/courseSearch/include/iWantToBeResultsPod.jsp"/>
            
            <jsp:include page="/jsp/search/courseSearch/include/whatCourseShouldIDoPod.jsp"/>
            
            <jsp:include page="/jsp/search/courseSearch/include/whatCourseShouldIDoResultsPod.jsp"/>
            
            <jsp:include page="/jsp/search/courseSearch/include/subjectGuidesPod.jsp"/>
          </div>
        </div>
      </div>
    </section>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
</body>
