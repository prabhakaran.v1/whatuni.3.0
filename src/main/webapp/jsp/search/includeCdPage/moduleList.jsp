<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@page import="WUI.utilities.CommonUtil" %>
<%
  String courseId = request.getParameter("courseId");
%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/ajax_wu564.js"> </script>
<c:if test="${not empty requestScope.moduleList}">
  <h2 class="sub_tit fl fnt_lrg lh_24 pb20 txt_lft">Modules
  <%--Added source tooltip for Modules for 08_MAR_2016, By Thiyagu G.--%>
  <span class="tool_tip" href="#">
  <i class="fa fa-question-circle fnt_24">
  <span class="cmp fnt_lbd">
  <div class="fnt_lbd fnt_14 lh_24">These are the sub-topics that you will study as part of this course</div> 
  <%--Added course type flag and update date by Thiyagu G.S for 08_MAR_2016--%>
  <div> Source: <a href="/degrees/jsp/search/kisdataStatic.jsp" rel="nofollow"><spring:message code="wuni.idp.connect.text"/></a>, <c:if test="${not empty requestScope.courseLastUpdatedDate}">${requestScope.courseLastUpdatedDate}</c:if></div>
  <div class="line"></div>
  </span>
  </i>
  </span>
  </h2>
  <div class="fr right_link ">
    DATA SOURCE:
    <a class="cl_blult" href="/degrees/jsp/search/kisdataStatic.jsp" rel="nofollow"><spring:message code="wuni.idp.connect.text"/></a><%--Changed datacourse text from jsp to App.properties for 30_Jan_2019 By Sangeeth.S--%>
 </div>
  <div class="borderbot fl mb20"></div>
  <div id="div_module_info"></div>
  <%int outCount = 0;%>
    <c:forEach var="moduleList" items="${requestScope.moduleList}" varStatus="rowindex">
    <%outCount++;%>
      <div class="modules_toggle cf bor_btm">
        <h2 class="fl fnt_lrg lh_24 link_blue" id="tc${rowindex.index}" onclick="fetchModuleData('review_${rowindex.index}', ' ${moduleList.moduleGroupId}','<%=courseId%>','${rowindex.index}', this)">
                  ${moduleList.modules}
            <i id="hideShow_content${rowindex.index}" class="fa fa-plus-circle fnt_24 color1 fr mt6"></i>
        </h2>
        <ul id="module_data_${rowindex.index}" class="clear disp_off" style="display:none"></ul>
      </div>
    </c:forEach>
    <input type="hidden" id="outount" value="<%=outCount%>"/>
<input type="hidden" id="modulehideshowevent" value=""/>    
</c:if>

