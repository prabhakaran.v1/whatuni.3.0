<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
  String courseId = (String)request.getAttribute("courseId");
  String moduleindex = (String)request.getAttribute("moduleindex");
         moduleindex = moduleindex!=null && moduleindex.trim().length() >0 ? moduleindex : "0";
%>
<script type="text/javascript">
</script>

<c:if test="${not empty requestScope.moduleDetailList}">
  <c:if test="${not empty requestScope.moduleGroupDesc}"><p>${requestScope.moduleGroupDesc}</p></c:if>
  <%int innerCount = 0;%>
  <c:forEach var="moduleDetailList" items="${requestScope.moduleDetailList}" varStatus="rowindex">
  <c:set var="rowindex" value="${rowindex.index}" />
  <% innerCount++; %>
    <% int rowindex = Integer.valueOf(pageContext.getAttribute("rowindex").toString());%>
  
     <c:if test="${moduleDetailList.moduleDescriptionFlag eq 'Y'}">
      <li  id="inner_div_<%=moduleindex%>_<%=rowindex%>" onclick="loadModuleDetailDescription('${moduleDetailList.moduleId}','${moduleDetailList.moduleGroupId}','<%=courseId%>','<%=rowindex%>')">
        <a class="fn_sl">
           <i id="plmn_${moduleDetailList.moduleGroupId}_<%=rowindex%>" class="fa fa-plus-circle fnt_18 color1 pr15"></i>
           <span>
             ${moduleDetailList.moduleTitle}
             <c:if test="${not empty moduleDetailList.credits}">
             (${moduleDetailList.credits} credits)
             </c:if>
              <c:if test="${not empty moduleDetailList.moduleTypeName}">
                 -
                 ${moduleDetailList.moduleTypeName}
              </c:if>
           </span>
        </a>       
      </li>
      <div id="module_data_detail_${moduleDetailList.moduleGroupId}_<%=rowindex%>" class="swhd" style="display:none"></div>
    </c:if>
    <c:if test="${moduleDetailList.moduleDescriptionFlag ne 'Y'}">
    <li>      
           <span>
             ${moduleDetailList.moduleTitle}
             <c:if test="${not empty moduleDetailList.credits}">
             (${moduleDetailList.credits} credits)</c:if>
             <c:if test="${not empty moduleDetailList.moduleTypeName}">
                -
             ${moduleDetailList.moduleTypeName}
             </c:if>
           </span>        
      </li>
    </c:if>
     
  </c:forEach>
  <input type="hidden" id="innercount" value="<%=innerCount%>"/>
</c:if>