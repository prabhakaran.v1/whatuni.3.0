<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator,WUI.utilities.CommonFunction" %>
 
 <%
  CommonFunction comFn = new CommonFunction();
  String subjectName = (String)request.getAttribute("subjectName");
  String rating = (String)request.getAttribute("rating");
  String reviewCount = (String)request.getAttribute("reviewCount");
  String ratingExact = (String)request.getAttribute("reviewExact");
  String subjectDropDownRating = (String)request.getAttribute("subjectDropDownRating");
  String subjectDropDownReviewCount = (String)request.getAttribute("subjectDropDownReviewCount");
  String subjectReviewExact = (String)request.getAttribute("subjectReviewExact");
  String firstSubjectName = (String)request.getAttribute("firstSubjectName");
  String commonPhraseStlye = "display:block";
  String commonPhraseClassName = "defaultCP";
  CommonUtil commonUtil = new CommonUtil();
  String userNameColorClassName = "rev_blue";
  String collegeId = (String)request.getAttribute("collegeId");
  String collegeName = (String)request.getAttribute("collegeName");
  collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
 %>
  <div class="cd_revrw">   
    <div class="rev_boxmn">
    <c:if test="${not empty requestScope.getReviewList}">
      <c:forEach var="getReviewListDetails" items="${requestScope.getReviewList}"> 
        <div class="rev_box">
         <div class="rlst_row">
          <div class="revlst_lft">
            <div class="rlst_wrp">
             <c:if test="${not empty getReviewListDetails.userNameInitial}">
                <c:set var="userInitialName" value="${getReviewListDetails.userNameInitial}"/>
                <%
                  userNameColorClassName = commonUtil.getReviewUserNameColorCode((String)pageContext.getAttribute("userInitialName"));
                %>
                <div class="rev_prof <%=userNameColorClassName%>">${getReviewListDetails.userNameInitial}</div>
            </c:if>
              <div class="rlst_rht">
                <div class="rev_name">${getReviewListDetails.reviewerName}</div>
                <div class="rev_dte">${getReviewListDetails.reviewDate}</div>
              </div>
            </div>
          </div>
          <div class="revlst_rht">
            <div class="rlst_wrap">
              <h2>
                 <c:if test="${getReviewListDetails.courseDeletedFlag ne 'TRUE'}">
                <a href="${getReviewListDetails.courseDetailsReviewURL}" title="${getReviewListDetails.courseTitle}">
                ${getReviewListDetails.courseTitle}
                </a>
                </c:if>
                <c:if test="${getReviewListDetails.courseDeletedFlag eq 'TRUE' }">
                <span class="del_crse">${getReviewListDetails.courseTitle}</span>
                </c:if>
              </h2>
              <div class="reviw_rating">
                <div class="rate_new"><span class="ml5 rat rat${getReviewListDetails.overAllRating}"></span>
                  <div class="rw_qus_des">${getReviewListDetails.overallRatingComment}...</div>
                </div>
                <div class="rev_mre"><a onclick="ajaxReviewLightBox(${getReviewListDetails.reviewId})">Read more</a></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </c:forEach>
      </c:if>
    </div>
    </div>             
    <div class="clear">
      <a class="bton" href="/university-course-reviews/<%=collegeName%>/<%=collegeId%>">READ ALL REVIEWS <i class="fa richp fa-long-arrow-right"></i></a>
    </div>
    <div class="rev_bdwn">            
      <div class="lrhd_sec">
        <h2 class="sub_tit fnt_lrg fl txt_lft whclr">Review breakdown</h2>
        <fieldset class="fs_col2 fr" id="fieldsetId" onclick="openReviewDropdown()">
          <div class="od_dloc" id="questionSelectedText"><span>Overall rating</span><span><i class="fa fa-angle-down"></i></span></div>
          <div class="opsr_lst" id="reviewDropDown">
            <ul>
              <c:if test="${not empty requestScope.reviewBreakDownList}">
                 <c:forEach var="reviewBreakDownList" items="${requestScope.reviewBreakDownList}" varStatus="index">
                  <li class="liclass" onclick="changeTitle(${index.index});closeDropdown();" id="questionDropDown_${index.index}"><span class="liclass">${reviewBreakDownList.questionTitle}</span></li>
                </c:forEach>
              </c:if>
            </ul>
          </div>
        </fieldset>
      </div>
      <div class="revbar_cnt">
        <div class="revbar_col">
        <c:if test="${not empty requestScope.subjectName}">
            <h2>How <%=subjectName%> students rated:</h2>
         </c:if>
          <c:if test="${empty requestScope.subjectName}">
            <h2>How <%=firstSubjectName%> students rated:</h2>    
         </c:if>
          <h3 id="cdRating" class="rev_srat fnt_lrg fnt_14 lh_24">Student rating <span class="ml5 rat<%=subjectDropDownRating%> t_tip">
          <span class="cmp" style="display:none">
            <div class="hdf5">This is the overall rating calculated by averaging all live reviews for this uni on Whatuni.</div>
            <div class="line"></div>
          </span>
          </span>( <%=subjectReviewExact%>)
          <span>
            <%=subjectDropDownReviewCount%> reviews
          </span>
          </h3>
          <c:if test="${not empty requestScope.getReviewBreakdownSubjectList}">
          <c:forEach var="subjectReviewBreakdownlist" items="${requestScope.getReviewBreakdownSubjectList}" varStatus="index">
           <c:set var="index" value="${index.index}" />
            <%
            int index = Integer.valueOf(pageContext.getAttribute("index").toString());
                if(index == 0){
              %>
             <div class="rvbar_cnt" id="star_<%=index%>" >
               <div class="rvbar_wrp">
                 <span class="stra_txt fl">5 star</span>
                 <span class="stra_bar fl"><span id="subject5StarWidthId" class="fl" style="width:${subjectReviewBreakdownlist.fiveStarPercent}%"></span></span>
                 <span class="stra_per fl" id="subject5StartextId">${subjectReviewBreakdownlist.fiveStarPercent}%</span>
                </div>
                <div class="rvbar_wrp">
                  <span class="stra_txt fl">4 star</span>
                  <span class="stra_bar fl"><span id="subject4StarWidthId" class="fl" style="width:${subjectReviewBreakdownlist.fourStarPercent}%"></span></span>
                  <span class="stra_per fl" id="subject4StartextId">${subjectReviewBreakdownlist.fourStarPercent}%</span>
                </div>
                <div class="rvbar_wrp">
                  <span class="stra_txt fl">3 star</span>
                  <span class="stra_bar fl"><span id="subject3StarWidthId" class="fl" style="width:${subjectReviewBreakdownlist.threeStarPercent}%"></span></span>
                  <span class="stra_per fl" id="subject3StartextId">${subjectReviewBreakdownlist.threeStarPercent}%</span>
                </div>
                <div class="rvbar_wrp">
                  <span class="stra_txt fl">2 star</span>
                  <span class="stra_bar fl"><span id="subject2StarWidthId" class="fl" style="width:${subjectReviewBreakdownlist.twoStarPercent}%"></span></span>
                  <span class="stra_per fl" id="subject2StartextId">${subjectReviewBreakdownlist.twoStarPercent}%</span>
                </div>
                <div class="rvbar_wrp">
                  <span class="stra_txt fl">1 star</span>
                  <span class="stra_bar fl"><span id="subject1StarWidthId" class="fl" style="width:${subjectReviewBreakdownlist.oneStarPercent}%"></span></span>
                  <span class="stra_per fl" id="subject1StartextId">${subjectReviewBreakdownlist.oneStarPercent}%</span>
                </div>
              </div>
              <%}%>
              <input type="hidden" id="subject5Star_${index}" value="${subjectReviewBreakdownlist.fiveStarPercent}"/>  <input type="hidden" id="subject4Star_${index}" value="${subjectReviewBreakdownlist.fourStarPercent}"/>
              <input type="hidden" id="subject3Star_${index}" value="${subjectReviewBreakdownlist.threeStarPercent}"/> <input type="hidden" id="subject2Star_${index}" value="${subjectReviewBreakdownlist.twoStarPercent}"/> <input type="hidden" id="subject1Star_${index}" value="${subjectReviewBreakdownlist.oneStarPercent}"/>
            </c:forEach>
          </c:if>
        </div>
        <div class="revbar_col">
          <h2>How all students rated:</h2>
            <h3 id="cdRating" class="rev_srat fnt_lrg fnt_14 lh_24">Student rating <span class="ml5 rat<%=rating%> t_tip">
              <span class="cmp" style="display:none">
                <div class="hdf5">This is the overall rating calculated by averaging all live reviews for this uni on Whatuni.</div>
                <div class="line"></div>
              </span>
              </span>(<%=ratingExact%>)
              <span>
                 <%=reviewCount%>
                  reviews
                  </span>
                  </h3>
                  
                  <c:if test="${not empty requestScope.reviewBreakDownList}">
                    <c:forEach var="reviewBreakDownListStarPod" items="${requestScope.reviewBreakDownList}" varStatus="index">
                    <c:set var="index" value="${index.index}" />
                     <%
                     int index = Integer.valueOf(pageContext.getAttribute("index").toString());
                     if(index == 0){
                     %>
                    <div class="rvbar_cnt" id="starId_<%=index%>">
                      <div class="rvbar_wrp">
                        <span class="stra_txt fl">5 star</span>
                        <span class="stra_bar fl"><span id="5starWidthId" class="fl" style="width:${reviewBreakDownListStarPod.fiveStarPercent}%"></span></span>
                        <span class="stra_per fl" id="5StarId">${reviewBreakDownListStarPod.fiveStarPercent}%</span>
                      </div>
                      <div class="rvbar_wrp">
                        <span class="stra_txt fl">4 star</span>
                        <span class="stra_bar fl"><span id="4starWidthId" class="fl" style="width:${reviewBreakDownListStarPod.fourStarPercent}%"></span></span>
                        <span class="stra_per fl" id="4StarId">${reviewBreakDownListStarPod.fourStarPercent}%</span>
                      </div>
                      <div class="rvbar_wrp">
                        <span class="stra_txt fl">3 star</span>
                        <span class="stra_bar fl"><span id="3starWidthId" class="fl" style="width:${reviewBreakDownListStarPod.threeStarPercent}%"></span></span>
                        <span class="stra_per fl" id="3StarId">${reviewBreakDownListStarPod.threeStarPercent}%</span>
                      </div>
                      <div class="rvbar_wrp">
                        <span class="stra_txt fl">2 star</span>
                        <span class="stra_bar fl"><span id="2starWidthId" class="fl" style="width:${reviewBreakDownListStarPod.twoStarPercent}%"></span></span>
                        <span class="stra_per fl" id="2StarId">${reviewBreakDownListStarPod.twoStarPercent}%</span>
                      </div>
                      <div class="rvbar_wrp">
                        <span class="stra_txt fl">1 star</span>
                        <span class="stra_bar fl"><span id="1starWidthId" class="fl" style="width:${reviewBreakDownListStarPod.oneStarPercent}%"></span></span>
                        <span class="stra_per fl" id="1StarId">${reviewBreakDownListStarPod.oneStarPercent}%</span>
                      </div>
                    </div>
                    <%}%>
                    <input type="hidden" id="5star_${index}" value="${reviewBreakDownListStarPod.fiveStarPercent}"/>  <input type="hidden" id="4star_${index}" value="${reviewBreakDownListStarPod.fourStarPercent}"/>
                    <input type="hidden" id="3star_${index}" value="${reviewBreakDownListStarPod.threeStarPercent}"/><input type="hidden" id="2star_${index}" value="${reviewBreakDownListStarPod.twoStarPercent}"/> <input type="hidden" id="1star_${index}" value="${reviewBreakDownListStarPod.oneStarPercent}"/>  
                  </c:forEach>
                </c:if>
              </div>
            </div>
              </div>     
  
    
  