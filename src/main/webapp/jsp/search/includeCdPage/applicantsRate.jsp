<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil" %>
<%
String tooltiptext = "Source: <a href=\"/degrees/jsp/search/kisdataStatic.jsp\">UCAS</a>, 2015";
%>

<c:if test="${not empty requestScope.getApplicantsData}">
  <section class="application_rate greybg grapm1 fnt_14">
    <div id="div_application_rate"></div>
     <h1 class="fl fnt_lrg">
        <span class="fnt_lrg fnt_24 lh_24 cl_grylt">Application rate</span>
      
        <span class="tool_tip">
          <i class="fa fa-question-circle fnt_24">
            <span class="cmp fnt_lbd">
              <div class="fnt_lbd fnt_14 lh_24">Application rate</div>
              <div>The percentage of total applicants that actually get offered a place on the course.</div>
              <div><%=tooltiptext%></div>
              <div class="line"></div>
            </span>
          </i>
        </span>
     </h1>
     <div class="fright right_link">
        <%--Added course type flag and update date by Thiyagu G.S for 08_MAR_2016--%>
        DATA SOURCE: <a class="cl_blult" rel="nofollow" href="/degrees/jsp/search/kisdataStatic.jsp">
         <c:if test="${not empty requestScope.courseTypeText}">
           ${requestScope.courseTypeText}
         </c:if>
          </a>
     </div>
     <div class="borderbot1 fl mt25"></div>
     <c:forEach var="getApplicantsData" items="${requestScope.getApplicantsData}"> 
       <c:if test="${not empty getApplicantsData.totalApplicants}">
         <div class="clear fleft mt25 fnt_lbd fnt_30">
            ${getApplicantsData.totalApplicants}
            <span class="fnt_14 fnt_lbd">Total applicants (2015)</span>
         </div>                    
        </c:if>
        <% %>
        <div class="clear app_img mt10 fleft">
           <c:if test="${not empty getApplicantsData.totalApplicantsStickMen}">
             <c:set var="totalApplicantsStickMen" value="${getApplicantsData.totalApplicantsStickMen}"/>
             <% int sizeVal = Integer.valueOf(pageContext.getAttribute("totalApplicantsStickMen").toString());
             for(int i = 0; i<sizeVal ; i++) {%>
             <img src="<%=CommonUtil.getImgPath("/wu-cont/images/application-man-img.png", 0)%>">
             <%}%>
          </c:if>
        </div>
         <c:if test="${not empty getApplicantsData.successfulApplicants}">
            <div class="clear fleft mt25 fnt_lbd fnt_30">
             ${getApplicantsData.successfulApplicants}
            <span class="fnt_14 fnt_lbd">Successful applicants (2015)</span>
         </div>                    
        </c:if>
        <% %>
        <div class="clear app_img mt10 fleft">
          <c:if test="${not empty getApplicantsData.successfulApplicantsStickMen}">
           <c:set var="successfulApplicantsStickMen" value="${getApplicantsData.successfulApplicantsStickMen}"/>
            <% int sizeValue = Integer.valueOf(pageContext.getAttribute("successfulApplicantsStickMen").toString());
            for(int j = 0; j<sizeValue ; j++) {%>
            <img src="<%=CommonUtil.getImgPath("/wu-cont/images/application-man-img1.png", 0)%>">
            <%}%>
          </c:if>
        </div>
        <div class="mt10 fright fnt_lrg">
          <img align="absmiddle" src="<%=CommonUtil.getImgPath("/wu-cont/images/application-man-img2.png", 0)%>">
          = 50
        </div>
        <div class="borderbot1 fleft mt20 mb20"></div>
        <c:if test="${not empty getApplicantsData.successfulApplicantsStickMen}">
         <c:if test="${not empty getApplicantsData.applicantsRate}">
            <div id="doughnutChart_application_rate" class="chart w_160 "></div>  
             <c:set var="applicantsRate" value="${getApplicantsData.applicantsRate}"/>                  
            <input type="hidden" name='appln_rate' id='appln_rate' value="${getApplicantsData.applicantsRate}" />
            <script type="text/javascript" id="script_app_rate_box">                          
             drawCddoughnutchart('appln_rate','doughnutChart_application_rate','#fc7169');
            </script>
            <div class="fleft suucess-rate fnt_lrg fnt_14">
              APPLICANT
              <br>
              SUCCESS RATE
            </div>
          </c:if>
        </c:if>
     </c:forEach>
  </section>
</c:if>

