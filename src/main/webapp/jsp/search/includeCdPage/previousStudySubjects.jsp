<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<%@page import="WUI.utilities.GlobalConstants"%>
<%
String tooltiptext = "Source: <a href=\"/degrees/jsp/search/kisdataStatic.jsp\">HESA</a>, 2018";
%>
<c:if test="${not empty requestScope.previousSubjectList}">
   <section class="previous_study greybg grapm1">
     <div id="div_previous_study"></div>
      <h2 class="sub_tit fnt_lrg fl">
        Previous study
        <span class="tool_tip">
          <i class="fa fa-question-circle fnt_24">
            <span class="cmp fnt_lbd">
              <div class="fnt_lbd fnt_14 lh_24 ">Previous study</div>
              <div>The most common A-levels taken by students who end up studying this subject at uni.</div>
              <div><%=tooltiptext%></div>
              <div class="line"></div>
            </span>
          </i>
        </span>
      </h2>
      <div class="fr right_link">
        DATA SOURCE: <a class="cl_blult" rel="nofollow" href="/degrees/jsp/search/kisdataStatic.jsp">HESA</a>
      </div>
      <div class="borderbot1 fl mt25"></div>
      <c:forEach items="${requestScope.previousSubjectList}" var="previousSubjectList" varStatus="index">
        <c:set var="sizeoflist" value="${fn:length(previousSubjectList.subjectList)}"/>
        <c:set var="indexValue" value="${index.index}"/>
        <%
        int sizeoflist = Integer.parseInt(pageContext.getAttribute("sizeoflist").toString());
        int index = Integer.parseInt(pageContext.getAttribute("indexValue").toString());
        if(sizeoflist>0){%>
         <%String styleValue = "";
            if(index >= 1){
              styleValue = "display:none;" ; 
         %>
         <div class="show_pod mt20">
           <h1 class="fnt_lrg"> 
             ${previousSubjectList.kisSubject}
             <a class="fr" onclick="javascript:showhideDivPreviousStudy('<%=index%>');">
                <i id="arrow_prev_study_<%=index%>" class="fa fa-angle-right fa-lg fa-1_5x"></i>
             </a>
           </h1>
         </div>
       <%}%>  
       <div id="prev_study_<%=index%>"  style="<%=styleValue%>">
        <%  if(index == 0){%>
        <h2 class="sub_tit fnt_lrg fl w100p pt20">${previousSubjectList.kisSubject}</h2>
        <%}%>
        <div class=" fnt_lbd fnt_14 lh_24 fl mt20 mt25">Top 5 A-levels taken by students who study this subject at uni</div>
       
        <c:if test="${not empty previousSubjectList.subjectList}">
          <c:forEach items="${previousSubjectList.subjectList}" var="subjectList">
           <div class="clear fl mt20">
            <div class="div1 mb5 fnt_lrg fnt_14 lh-24 cl_grylt">${subjectList.kisSubject} </div>
            <div class="div2">
              <span class="ct-cont">
              <span class="bar" style="width:${subjectList.heldByPercentage}%;"></span>
              </span>
            </div>
            <div class="div3 fnt_18 fr">${subjectList.heldByPercentage}%</div>
             </div>  
          </c:forEach>
        </c:if>
       </div> 
       <div class="borderbot1 fl mt25"></div>
       <%}%>
      </c:forEach>
   </section>
 </c:if>

