<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil"%>
<%
  String paramCourseId = request.getParameter("w") !=null && request.getParameter("w").trim().length()>0 ? request.getParameter("w") : request.getParameter("cid") !=null && request.getParameter("cid").trim().length()>0 ? request.getParameter("cid") : ""; 
  paramCourseId = paramCourseId!=null && paramCourseId.trim().length()>0 ? "" : String.valueOf(request.getAttribute("seoCourseId"));
  String isClearingCourse = (String)request.getAttribute("CLEARING_COURSE");
  boolean isClearingCourses = (((String)request.getAttribute("CLEARING_COURSE")) != null && "TRUE".equals((String)request.getAttribute("CLEARING_COURSE"))) ? true : false;
  String searchType  = "";
         searchType = (!GenericValidator.isBlankOrNull(isClearingCourse) && "TRUE".equals(isClearingCourse))? "CLEARING_COURSE": "NORMAL_COURSE";
  String opportunityId = (String)request.getAttribute("opportunityId");
  String cdOpportunityId = (String)request.getAttribute("cdOpportunityId");
  String oppId = null;
  if(cdOpportunityId != null ){
    oppId = cdOpportunityId;
  }
  if(opportunityId != null) {
    oppId = opportunityId;
  }
  String applyNowFlag = (String)request.getAttribute("checkApplyFlag");
  request.setAttribute("whatuniGOApplyFilterFlag","N");
  String entryRequirmentPod = (String)request.getAttribute("entryRequirmentPod");       
  pageContext.setAttribute("isClearingCoursesFlag", isClearingCourses);
%>
<aside  class="sidebar_left mr20 fl">
   <div id="sidebar" class="">
   <%-- ::START:: Yogeswari :: 30.06.2015 :: changed for clearing/course detail page responsive task --%>
  <%if(!isClearingCourses){%>
   <div id="leftNavMobile">
    <div class="reg_dd" id="menu_item">
      <div class="sbxme">
        <span class="seldr txt_cap" id="qualspanul">Course info</span>
      </div>
    </div>
    </div>
    <%}%>
    <%if(isClearingCourses){%>
      <c:if test="${requestScope.entryRequirmentPod eq 'Yes'}">
        <div id="leftNavMobile">
    <div class="reg_dd" id="menu_item">
      <div class="sbxme">
        <span class="seldr txt_cap" id="qualspanul">Entry requirements</span>
      </div>
    </div>
    </div>
    </c:if>
     <c:if test="${requestScope.entryRequirmentPod ne 'Yes'}">
      <div id="leftNavMobile">
    <div class="reg_dd" id="menu_item">
      <div class="sbxme">
        <span class="seldr txt_cap" id="qualspanul">Course info</span>
      </div>
    </div>
    </div>
     
     </c:if>
    <%}%>
    <%-- ::END:: Yogeswari :: 30.06.2015 :: changed for clearing/course detail page responsive task --%>
    <ul>
      <c:if test="${not empty requestScope.sideBarList}">
      <c:forEach var="sideBarList" items="${requestScope.sideBarList}"> 
         <c:if test="${sideBarList.value eq 'div_tuition_fees'}">
            <li id="sideTutionTab"><a onclick="javascript:skipLinkSetPositions('${sideBarList.value}')">${sideBarList.label}</a></li>
          </c:if>
          <c:if test="${sideBarList.value ne 'div_tuition_fees'}">
            <li><a onclick="javascript:skipLinkSetPositions('${sideBarList.value}')">${sideBarList.label}</a></li>
          </c:if>
          <%--<li><a href="#<bean:write name="sideBarList" property="value"/>"><bean:write name="sideBarList" property="label"/></a></li>--%>
        </c:forEach>
      </c:if>
      <li><a onclick="javascript:skipLinkSetPositions('div_top_articles')">Top articles for you</a></li>
    </ul>
    </div>
  </aside>  
<div class="right_section fr" id="ajaxDivContent">
<%if(isClearingCourses){%>
  <c:if test="${!isClearingCoursesFlag}">
    <c:if test="${requestScope.wugoFlag eq 'ON'}">
      <jsp:include page="/clearing/watchvideo/showWatchVideoButton.jsp"/>
    </c:if> 
     <jsp:include page="/jsp/search/includeCdPage/entryRequirementUCAS.jsp">
      <jsp:param name="searchtype" value="<%=searchType%>"/>
    </jsp:include> 
  </c:if>
  <jsp:include page="/jsp/search/includeCdPage/courseInfo.jsp"/>            
  
  <jsp:include page="/jsp/search/includeCdPage/keyStats.jsp"/>     
  
  <section class="modules">
     <c:if test="${not empty requestScope.moduleInfo}">
          <div id="div_module_info"></div>
         <h2 class="sub_tit fl fnt_lrg lh_24 pb20 txt_lft">Modules
         <%--Added source tooltip for Modules for 08_MAR_2016, By Thiyagu G.--%>
         <span class="tool_tip" href="#">
          <i class="fa fa-question-circle fnt_24">
          <span class="cmp fnt_lbd">
          <div class="fnt_lbd fnt_14 lh_24">These are the sub-topics that you will study as part of this course</div> 
          <%--Added course type flag and update date by Thiyagu G.S for 08_MAR_2016--%>
          <div> Source: <a href="/degrees/jsp/search/kisdataStatic.jsp" rel="nofollow">UCAS</a>,  <c:if test="${not empty requestScope.courseLastUpdatedDate}">${requestScope.courseLastUpdatedDate}</c:if> </div>
          <div class="line"></div>
          </span>
          </i>
          </span>
         </h2>
         <div class="fr right_link ">
            DATA SOURCE:
            <%--Added course tyoe flag and update date by Thiyagu G.S for 08_MAR_2016--%>
            <a class="cl_blult" href="/degrees/jsp/search/kisdataStatic.jsp" rel="nofollow">UCAS</a>
        </div>
        <div class="borderbot fl mb20"></div>
        <p class="fnt_lrg fnt_14 lh_24 cl_grylt fl">${requestScope.moduleInfo}</p>
      </c:if>
   <jsp:include page="/jsp/search/includeCdPage/moduleList.jsp">
     <jsp:param name="courseId" value="<%=paramCourseId%>"/>
    </jsp:include>
  </section>
  <%}%>
  <%if(!isClearingCourses){%>
 <jsp:include page="/jsp/search/includeCdPage/courseInfo.jsp"/>            
  <jsp:include page="/jsp/search/includeCdPage/keyStats.jsp"/>     
  <section class="modules">
    <c:if test="${not empty requestScope.moduleInfo}">
       <div id="div_module_info"></div>
         <h2 class="sub_tit fl fnt_lrg lh_24 pb20 txt_lft">Modules
         <%--Added source tooltip for Modules for 08_MAR_2016, By Thiyagu G.--%>
         <span class="tool_tip" href="#">
          <i class="fa fa-question-circle fnt_24">
          <span class="cmp fnt_lbd">
          <div class="fnt_lbd fnt_14 lh_24">These are the sub-topics that you will study as part of this course</div> 
          <%--Added course type flag and update date by Thiyagu G.S for 08_MAR_2016--%>
          <div> Source: <a href="/degrees/jsp/search/kisdataStatic.jsp" rel="nofollow">UCAS</a>, <c:if test="${not empty requestScope.courseLastUpdatedDate}"> <c:out value="${requestScope.courseLastUpdatedDate}"/> ${requestScope.courseLastUpdatedDate}</c:if> </div>
          <div class="line"></div>
          </span>
          </i>
          </span>
         </h2>
         <div class="fr right_link ">
            DATA SOURCE:
            <%--Added course tyoe flag and update date by Thiyagu G.S for 08_MAR_2016--%>
            <a class="cl_blult" href="/degrees/jsp/search/kisdataStatic.jsp" rel="nofollow">UCAS</a>
        </div>
        <div class="borderbot fl mb20"></div>
        <p class="fnt_lrg fnt_14 lh_24 cl_grylt fl"><c:out value="${requestScope.moduleInfo}"/></p>
     </c:if>
   <jsp:include page="/jsp/search/includeCdPage/moduleList.jsp">
     <jsp:param name="courseId" value="<%=paramCourseId%>"/>
    </jsp:include>
  </section>
  <jsp:include page="/jsp/search/includeCdPage/entryRequirementUCAS.jsp">
    <jsp:param name="searchtype" value="<%=searchType%>"/>
  </jsp:include>
  <%}%>
  <c:if test="${CLEARING_CD_PAGE eq 'CLEARING_CD_PAGE'}">
     <jsp:include page="/jsp/clearing/coursedetails/include/entryRequirementSection.jsp"/>
  </c:if>
  <%--<jsp:include page="/jsp/search/includeCdPage/howYouAreAssessed.jsp"/>--%> <%--commented for Oct_23_18 rel by sangeeth.s--%>
  <jsp:include page="/jsp/search/includeCdPage/applicantsRate.jsp"/> 
  <jsp:include page="/jsp/search/includeCdPage/previousStudySubjects.jsp"/><%--Uncommented Previous study pod in CD page by Hema.S on 13_FEB_2019_REL--%>
  <jsp:include page="/jsp/search/includeCdPage/feeData.jsp"/> 
  <jsp:include page="/jsp/search/includeCdPage/reviewPod.jsp"/> 
  <c:choose>
    <c:when test="${CLEARING_CD_PAGE eq 'CLEARING_CD_PAGE'}">
      <jsp:include page="/jsp/clearing/coursedetails/include/uniInfoSection.jsp"/>
    </c:when>
    <c:otherwise>
      <jsp:include page="/jsp/search/includeCdPage/uniInfo.jsp"/>  
    </c:otherwise>
  </c:choose>  
  <jsp:include page="/jsp/search/includeCdPage/locationMap.jsp"/>
  <jsp:include page="/jsp/search/includeCdPage/spList.jsp"/>
  <c:if test="${!isClearingCoursesFlag}">
    <c:if test="${not empty requestScope.advertiserFlag}">   
       <c:if test="${requestScope.advertiserFlag eq 'FALSE'}">
         <jsp:include page="/jsp/search/includeCdPage/coursesYouMightLikePod.jsp"> 
           <jsp:param name="searchtype" value="<%=searchType%>"/>
         </jsp:include>
      </c:if>
      <c:if test="${requestScope.advertiserFlag eq 'TRUE'}">
         <jsp:include page="/jsp/search/includeCdPage/popularCourses.jsp"/> 
      </c:if>
    </c:if>
  </c:if>
  <c:if test="${CLEARING_CD_PAGE eq 'CLEARING_CD_PAGE'}">
    <jsp:include page="/jsp/clearing/coursedetails/include/otherCoursesSection.jsp"/>
  </c:if>
  <%--Load the article pod through ajax call, added on 13_Jun_2017, By Thiyagu G.--%>
  <section style="padding:20px 0 0"><div id="div_top_articles"></div></section>
  <section class="artfc p0 mt38 cm_art_pd" id="articlesPod"></section>  
  <input type="hidden" id="scrollStop" value="" />
  <input type="hidden" id="oppId" value="<%=oppId%>" />
  <input type="hidden" id="applyNowFlagAjax" value="<%=applyNowFlag%>"/>
  <c:if test="${CLEARING_CD_PAGE eq 'CLEARING_CD_PAGE'}">
    <input type="hidden" id="scoreLabel_hidden" value="${scoreLabel}"/>
    <input type="hidden" id="scoreLevel_hidden" value="${scoreLevel}"/>
    <input type="hidden" id="scoreLabelTooltip_hidden" value="${scoreLabelTooltip}"/>
    <input type="hidden" id="clearingSkipLinks" value="${CLEARING_CD_PAGE}"/>
  </c:if>
</div>