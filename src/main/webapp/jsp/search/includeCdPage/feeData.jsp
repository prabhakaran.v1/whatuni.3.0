<%--
  * @purpose  This is fees section jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 20-feb-2018   Hema.S                     573      Modified Entire Structure                                        573 
  * 11-Jan-2019   Sangeeth.S                 585      rebranding changes hotcourses to IDP Connect                     585
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants,WUI.utilities.CommonUtil"%>
<%
  String ent_collegeName = (String) request.getAttribute("collegeName");
  String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(ent_collegeName);  
  String wps_collegeId = request.getAttribute("interactionCollegeId") != null && request.getAttribute("interactionCollegeId").toString().trim().length() > 0 ? (String)request.getAttribute("interactionCollegeId") : "0";
  String courseId = (String)request.getAttribute("courseId");
  String subOrderItemId = (String)request.getAttribute("subOrderItemId");
  String websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice(subOrderItemId, "website_price");
  String imagename = null;
%>

<c:if test="${not empty requestScope.getFeesData}">
  <section class="tution_fees">
    <h2 class="sub_tit fnt_lrg fl">Tuition fees</h2>
    <div class="fr right_link ">
      DATA SOURCE:
      <a class="cl_blult" href="/degrees/jsp/search/kisdataStatic.jsp" rel="nofollow"><spring:message code="wuni.data.source.fee.text"/></a><%--Changed datacourse text from jsp to App.properties for 30_Jan_2019 By Sangeeth.S--%>
    </div>
    <div id="div_tuition_fees"></div>
    <div id="fees"></div>
    <div id="tutFees" class="crsiner">
      <div class="feecnty">
        <h3>For students from</h3>
        <div class="fee_nav">
          <div class="fnav_wrp" id="mbleview"></div>
          <div class="scrltab">
            <c:forEach var="getFeesData" items="${requestScope.getFeesData}"> 
               <c:if test="${not empty getFeesData.fees}">
                 <a href="#tab${getFeesData.sequenceNo}">${getFeesData.feesType}</a>
              </c:if>
              <c:if test="${empty getFeesData.fees}">
                <c:if test="${not empty getFeesData.url}">
                   <a href="#tab${getFeesData.sequenceNo}">${getFeesData.feesType}</a>
                 </c:if>
              </c:if>
              <c:if test="${empty getFeesData.fees}">
                <c:if test="${empty getFeesData.url}">
                  <c:if test="${not empty getFeesData.state}">
                     <a href="#tab${getFeesData.sequenceNo}">${getFeesData.feesType}</a>
                  </c:if>
                   <c:if test="${empty getFeesData.state}">
                     <c:if test="${not empty getFeesData.feesDesc}">
                      <a href="#tab${getFeesData.sequenceNo}">${getFeesData.feesType}</a>
                     </c:if>
                   </c:if>
                 </c:if>
               </c:if>
          </c:forEach>                     
        </div>
      </div> 
      <c:forEach var="feesValues" items="${requestScope.getFeesData}">                   
        <div class="crsfee hide" id="tab${feesValues.sequenceNo}">
          <div class="feerw clr">  
            <div class="feerw_lt fl"> 
            <c:if test="${feesValues.state eq 'FREE'}">         
              <span class="erfee bld">FREE</span>
            </c:if>
            <c:if test="${feesValues.state ne 'FREE'}">
               <c:if test="${not empty feesValues.fees}">
                <span class="erfee bld">${feesValues.currency}${feesValues.fees}</span>
                ${feesValues.duration} 
              </c:if>
              <c:if test="${empty feesValues.fees}">
                <c:if test="${not empty feesValues.url}">
                 <p>
                 <c:if test="${not empty feesValues.subOrderItemId}">
                    <c:if test="${feesValues.subOrderItemId gt 0}"> 
                       <a href="${feesValues.url}" target="_blank"                    
                        class="visit-web" 
                        onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=websitePrice%>); cpeWebClickWithCourse(this,'<%=wps_collegeId%>','<%=courseId%>','${feesValues.subOrderItemId}','${feesValues.networkId}','${feesValues.url}'); var a='s.tl(';">
                        <spring:message code="wuni.ucas.new.fees.structure.url.text"/> 
                       </a>
                    </c:if>
                  </c:if>
                  <c:if test="${feesValues.subOrderItemId eq 0}"> 
                     <a href="${feesValues.url}" target="_blank"><spring:message code="wuni.ucas.new.fees.structure.url.text"/></a>
                  </c:if>
                  <c:if test="${empty feesValues.subOrderItemId}">
                    <a href="${feesValues.url}" target="_blank"><spring:message code="wuni.ucas.new.fees.structure.url.text"/></a>
                  </c:if>
                </p>
              </c:if>
            </c:if>
            </c:if>
            <c:if test="${not empty feesValues.fees}">
              <span class="tool_tip"><%--Added ucas text tooltip in CD page By Hema.S on 28-Aug-2018_rel--%>
              <i class="fa fa-question-circle fnt_24"  onmouseover="showTooltip('tooltiptext12');" onmouseout="hideTooltip('tooltiptext12');">
              <span class="cmp fnt_lbd" id="tooltiptext12">
                <div class="fnt_lbd fnt_14 lh_24 "></div>
                <div>
                  <spring:message code="wuni.cd.page.fees.ucas.tooltip.text"/>
                  <br/>
                  <spring:message code="wuni.cd.page.fees.ucas.tooltip.note.text"/>
               </div>
               <div class="line"></div>
              </span>
            </i>
          </span>
          </c:if>
          </div>         
            <div class="feerw_rt fl">
               <c:if test="${not empty feesValues.feesDesc}">
                <p><span class="mi_bld">More info: </span>${feesValues.feesDesc}.</p>                
              </c:if>                 
              <div class="fstu_lnk">
                <c:if test="${not empty feesValues.feesTypeTooltip}">
                  <c:set var="imageName" value="${feesValues.feesTypeTooltip}"/>
                    <%
                    String imageName = (String)pageContext.getAttribute("imageName");
                    if((GlobalConstants.NORTHERN_IRELAND_FEES).equalsIgnoreCase(imageName)) {
                    imageName = "NORTHERN_IRELAND";
                    }
                    imagename = "/wu-cont/images/flag_" + imageName.toLowerCase() + ".png";
                  %>                  
                  <%if(!((GlobalConstants.INTERNATIONAL_FEES).equalsIgnoreCase(imageName) ||(GlobalConstants.CHANNEL_ISLAND_FEES) .equalsIgnoreCase(imageName) || (GlobalConstants.DOMESTIC).equalsIgnoreCase(imageName) || (GlobalConstants.OTHER_UK).equalsIgnoreCase(imageName))){%>
                    <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=CommonUtil.getImgPath("",0)%><%=imagename%>" alt="Students from ${feesValues.feesTypeTooltip}">  
                 <%}%>
                 <a onclick="showAndHideToolTip('tooltiptext${feesValues.sequenceNo}');" onmouseover="showTooltip('tooltiptext${feesValues.sequenceNo}');" onmouseout="hideTooltip('tooltiptext${feesValues.sequenceNo}');" class="helptext">
                 <%if((GlobalConstants.INTERNATIONAL_FEES).equalsIgnoreCase(imageName) ||(GlobalConstants.EU_FEES).equalsIgnoreCase(imageName)){%>
                  ${feesValues.feesTypeTooltip} Students
                 <%}else{%>
                 Students from ${feesValues.feesTypeTooltip}
                 <%}%></a>
               </c:if>
               <span class="tool_tip" >
                 <i class="fa fa-question-circle fnt_24"  onmouseover="showTooltip('tooltiptext${feesValues.sequenceNo}');" onmouseout="hideTooltip('tooltiptext${feesValues.sequenceNo}');">
                   <span class="cmp fnt_lbd" id="tooltiptext${feesValues.sequenceNo}">
                   <div class="fnt_lbd fnt_14 lh_24 ">${feesValues.feesTypeTooltip} Fees</div>
                   <div>
                    <c:if test="${empty requestScope.pgCourse}">
                      <c:if test="${feesValues.feesTypeTooltip eq 'International'}">
                       <spring:message code="wuni.ucas.new.fees.live.line.intl"/> 
                     </c:if>
                     <c:if test="${feesValues.feesTypeTooltip ne 'International'}"> 
                       <c:if test="${feesValues.feesTypeTooltip eq 'EU'}">   
                         <spring:message code="wuni.ucas.new.fees.live.line" arguments="with,the European Union"/>
                       </c:if>
                     <c:if test="${feesValues.feesTypeTooltip ne 'EU'}"> 
                       <c:if test="${not empty feesValues.feesTypeTooltip}">
                          <c:set var="feetypes" value="${feesValues.feesTypeTooltip}"/>
                             <spring:message code="wuni.ucas.new.fees.live.line" arguments="with,${feetypes}"/>
                        </c:if>
                      </c:if>
                    </c:if>
                    <c:if test="${not empty feesValues.state}">
                       <c:set var="feeStateDef" value="${feesValues.state}"/>
                          <c:if test="${feesValues.state ne 'FREE'}">
                            <c:if test="${feesValues.state eq 'set'}">
                               <spring:message code="wuni.ucas.new.fees.set.state"/>
                            </c:if>
                       <c:if test="${feesValues.state ne 'set'}">
                           <spring:message code="wuni.ucas.new.fees.state.line" arguments="with,${feetypes}"/>
                        </c:if>
                      </c:if>
                    </c:if>
                    <c:if test="${feesValues.feesTypeTooltip ne 'Channel Islands'}"> 
                       <c:if test="${feesValues.state ne 'set'}">
                          <spring:message code="wuni.ucas.new.fees.upto.date.line"/>
                       </c:if>
                     </c:if>
                     </c:if>
                     <c:if test="${not empty requestScope.pgCourse}">
                       <c:if test="${feesValues.feesTypeTooltip eq 'International'}">
                          <spring:message code="wuni.ucas.new.pg.fees.intl.text"/>
                    </c:if>
                    <c:if test="${feesValues.feesTypeTooltip eq 'EU'}">
                      <spring:message code="wuni.ucas.new.pg.fees.eu.text"/>
                    </c:if>
                    <c:if test="${feesValues.feesTypeTooltip eq 'Domestic'}">
                      <spring:message code="wuni.ucas.new.pg.fees.domestic.text" arguments="in"/>
                    </c:if>
                    <c:if test="${feesValues.feesTypeTooltip eq 'Other UK'}">
                      <spring:message code="wuni.ucas.new.pg.fees.domestic.text" arguments="not in"/>
                    </c:if>
                    </c:if>
                   </div>
                   <div class="line"></div>
                 </span>
               </i>
             </span>
           </div>   
         </div>  
       </div> 
     </div>        
   </c:forEach>
  </div>
</div>
</section>

</c:if>


