<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<c:if test="${not empty requestScope.keyStatsFlag}">
  <section class="key_stats greybg clr_ks_mt25 clrcnge_keysts">
  <h2 class="sub_tit fl fnt_lrg lh_24">Key stats</h2>
  <div class="fr right_link">
   DATA SOURCE:
   <%--Added course type flag and update date by Thiyagu G.S for 08_MAR_2016--%>
   <a class="cl_blult" rel="nofollow" href="/degrees/jsp/search/kisdataStatic.jsp">UNISTATS / <c:if test="${not empty requestScope.courseTypeText}">${requestScope.courseTypeText} /</c:if> HESA</a>
  </div>
  <div id="div_key_stats"></div>
  <div class="borderbot1 fl mt20 mb20"></div>  

  <jsp:include page="/jsp/search/includeCdPage/rankingAndDuration.jsp"/>
  <c:if test="${not empty requestScope.keyStatsList}">
    <c:forEach var="keyStatsList" items="${requestScope.keyStatsList}" varStatus="index">
    <c:set var="index" value="${index.index}" />
      <%String classNameChart = "fl";
        String styleValue = "";%>
      <%
      int index = Integer.valueOf(pageContext.getAttribute("index").toString());
      if(index >= 1){
        styleValue = "display:none;" ;
      %>
      <div class="show_pod ">
          <h2 class="sub_tit fnt_lrg"> 
            ${keyStatsList.subjectName}
           <a class="fr" onclick="javascript:showhideDivCd('<%=index%>');">
              <i id="arrow_key_stats_<%=index%>" class="fa fa-angle-right fa-lg fa-1_5x"></i>
            </a>
          </h2>
      </div>
      <%}else {%>
       <div class="show_pod mb20">
          <h2 class="sub_tit fnt_lrg data_srce"> 
             ${keyStatsList.subjectName}
            <div class="fr right_link">
              DATA SOURCE: <a class="cl_blult" rel="nofollow" href="/degrees/jsp/search/kisdataStatic.jsp">UNISTATS</a>
            </div>
          </h2>
       </div>    
      <%}%>
      <div id="keyStats_<%=index%>" style="<%=styleValue%>" >
        <c:if test="${not empty keyStatsList.dropOutRate}">
          <div class="<%=classNameChart%>">
            <%classNameChart = "fl pl50";%>
            <h2 class="fnt_lbd txt_cnr lh_24">Drop-out rate
              <span class="tool_tip">
                <i class="fa fa-question-circle fnt_24">
                  <span class="cmp fnt_lbd">
                    <div class="fnt_lbd fnt_14 lh_24">Drop-out rate</div>
                    <%--Added three columns to show the drop-out rate calculation in tooltip By Thiyagu G on 24_jan_2017--%>
                    <div>The percentage of students who didn't finish the course.</div>
                    <div>Calculated by 100% - (% of students continuing course + % of students completed course)</div>
                    <div>100 - ${keyStatsList.courseContinuation} = ${keyStatsList.dropOutRate}% Drop-out rate</div>
                    <div><spring:message code="wuni.tooltip.kis.entry.text" /></div> <%--Changed KIS text and year to App.properties for 15_Nov_2016 By Thiyagu G--%>
                    <div class="line"></div>
                  </span>
                </i>
              </span>
            </h2>
            <div id="doughnutChart_dropOutRate_<%=index%>" class="chart w_160 "></div>
             <c:set var="dropOutRate" value="${keyStatsList.dropOutRate}"/>
              <input type="hidden" name='drop_out_rate_<%=index%>' id='drop_out_rate_<%=index%>' value="${keyStatsList.dropOutRate}" />
            <script type="text/javascript" id="script_drop_out_rate_<%=index%>">                          
             drawCddoughnutchart('drop_out_rate_<%=index%>','doughnutChart_dropOutRate_<%=index%>','#f8716c');
            </script>
          </div>
        </c:if>
        <c:if test="${not empty keyStatsList.twoistooneRatio}">
          <div class="<%=classNameChart%>">
            <%classNameChart = "fl pl50";%>
            <h2 class="fnt_lbd txt_cnr lh_24">2:1 or above
              <span class="tool_tip">
                <i class="fa fa-question-circle fnt_24">
                  <span class="cmp fnt_lbd">
                    <div class="fnt_lbd fnt_14 lh_24">2:1 or above</div>
                    <div>The percentage of students who graduated with a 2:1 degree or better.</div>
                    <div><spring:message code="wuni.tooltip.kis.entry.text" /></div>
                    <div class="line"></div>
                  </span>
                </i>
              </span>
            </h2>
            <div id="doughnutChart_twoistooneRatio_<%=index%>" class="chart w_160 "></div>
             <c:set var="twoistooneRatio" value="${keyStatsList.twoistooneRatio}"/>
             <input type="hidden" name='twoistooneRatio_<%=index%>' id='twoistooneRatio_<%=index%>' value="${keyStatsList.twoistooneRatio}" />
            <script type="text/javascript"  id="script_twoistoone_<%=index%>">                            
               drawCddoughnutchart('twoistooneRatio_<%=index%>','doughnutChart_twoistooneRatio_<%=index%>','#a85264');
            </script>
          </div>
        </c:if>
        <c:if test="${not empty keyStatsList.graduates}">
          <div class="<%=classNameChart%>">
            <%classNameChart = "fl pl50";%>
            <h2 class="fnt_lbd txt_cnr lh_24">Employment rate
              <span class="tool_tip right" href="#">
                <i class="fa fa-question-circle fnt_24">
                  <span class="cmp fnt_lbd">
                    <div class="fnt_lbd fnt_14 lh_24">Employment rate</div>
                    <div>The percentage of students in full-time work 6 months after finishing their course.</div>
                     <div><spring:message code="wuni.tooltip.kis.entry.text" /></div>
                    <div class="line"></div>
                  </span>
                </i>
              </span>
            </h2>
             <div id="doughnutChart_graduates_<%=index%>" class="chart w_160 "></div>
             <c:set var="graduates" value="${keyStatsList.graduates}"/>
             <input type="hidden" name='graduates_<%=index%>' id='graduates_<%=index%>' value="${keyStatsList.graduates}" />
            <script type="text/javascript"  id="script_graduates_<%=index%>">                            
               drawCddoughnutchart('graduates_<%=index%>','doughnutChart_graduates_<%=index%>','#edc756');
            </script>
          </div>
      </c:if>
            </div>
             <c:if test="${not empty keyStatsList.avgSalaryAfterSixMonths}">
            <div class="borderbot1 fl mt20 mb20"></div>
            <div class="grapm1 cf">
              <h2 class="fnt_lbd txt_cnr lh_24 fl pb20" id="AfterMonthsId<%=index%>">Average salary
               <span class="tool_tip" href="#">
                <i class="fa fa-question-circle fnt_24">
                  <span class="cmp fnt_lbd">
                    <div class="fnt_lbd fnt_14 lh_24">Average salary</div>                   
                     <div><spring:message code="wuni.tooltip.kis.entry.text" /></div>
                    <div class="line"></div>
                  </span>
                </i>
              </span>          
              </h2>
              <c:if test="${not empty keyStatsList.avgSalaryAfterFourtyMonths}">
              <fieldset class="set_drop">
                <fieldset class="">
                  <div class="select dis_edu">
                    <span id="qualspan<%=index%>" class="fnt_lbd">After 6 months</span>
                    <i class="fa fa-angle-down fa-lg"></i>
                  </div>
                  <select id="qualsel<%=index%>" class="dis_edu" name="keyStats" onchange="setSelectedVal(this, 'qualspan<%=index%>','<%=index%>');">
                    <option value="After 6 months">After 6 months</option>
                     <option value="After 40 months">After 40 months</option>
                  </select>
              </fieldset>
            </fieldset>
            </c:if>   
            <div id="avgSalaryAfterSixMonthsDiv<%=index%>">
             <c:if test="${not empty keyStatsList.avgSalAfterSixMonthsAtuni}">             
                <div class="clear fl mt20" id="avgSalaryAfterSixMonthsDiv<%=index%>">
                  <div class="div1 mb5 fnt_lrg fnt_14 lh-24 cl_grylt">
                    ${keyStatsList.subjectName} graduate salary at this uni
                  </div>
                  <div class="div2"><span class="ct-cont">
                      <span style="width: ${keyStatsList.percentageSixMonthsAtUni}%;" class="bar bg_yellow"></span></span>
                  </div>
								          <div class="div3 fnt_lbd fnt_18 cl_grylt fl">&pound;${keyStatsList.avgSalAfterSixMonthsAtuni}</div>
                </div>
              </c:if>
              <c:if test="${not empty keyStatsList.avgSalaryAfterSixMonths}">
                   <div class="clear fl mt20" >
                  <div class="div1 mb5 fnt_lrg fnt_14 lh-24 cl_grylt">
                    UK ${keyStatsList.subjectName} graduate salary
                  </div>
                  <div class="div2"><span class="ct-cont">
                    <span style="width: ${keyStatsList.percentageSixMonths}%;" class="bar"></span></span>
                  </div>
								          <div class="div3 fnt_lbd fnt_18 cl_grylt fl">&pound;${keyStatsList.avgSalaryAfterSixMonths}</div>
                </div>
              </c:if>
            </div> 
            <c:if test="${not empty keyStatsList.avgSalaryAfterFourtyMonths}">                     
              <div class="clear fl mt20" id="avgSalaryAfterFourtyMonthsDiv<%=index%>" style="display:none;">
                <div class="div1 mb5 fnt_lrg fnt_14 lh-24 cl_grylt">
                  ${keyStatsList.subjectName} graduate salary at this uni
                </div>
                <div class="div2"><span class="ct-cont">
                  <span style="width: ${keyStatsList.percentage40Months}%;" class="bar"></span></span>
                </div>
								        <div class="div3 fnt_lbd fnt_18 cl_grylt fl">&pound;${keyStatsList.avgSalaryAfterFourtyMonths}</div>
              </div>
            </c:if>
          </div>
        </c:if> 

       <div class="borderbot1 fl mt20 mb20"></div>
    </c:forEach>
</c:if>
  </section>
</c:if>         