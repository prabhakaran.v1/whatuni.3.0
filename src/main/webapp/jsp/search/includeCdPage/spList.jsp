<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil"%>

<c:if test="${not empty requestScope.uniSPList}">
   <section class="department">
   <%--offset and length is changed from 3 to 4 for 13_Dec_2016, by Thiyagu G.--%>
     <h2 class="sub_tit fnt_lrg fl txt_lft w100p mb20">Other academic departments</h2>
      <div id="div_other_departments"></div>
      <c:forEach var="uniSPList" items="${requestScope.uniSPList}" varStatus="moreIndex" begin="0" end="4">
       <div class="mt20 fl">
         <c:if test="${empty uniSPList.imagePath}">
          <div class="fl img_ppn noimage"></div>
         </c:if>
          <c:if test="${not empty uniSPList.imagePath}">        
           <div class="fl img_ppn">
             <a href="${uniSPList.spURL}">
               <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="${uniSPList.imagePath}" alt="${uniSPList.advertName}" title="">
             </a>
           </div>
         </c:if>
         <div class="div_right">
         <c:if test="${not empty uniSPList.advertName}"> 
            <h2>
              <a class="fnt_lbd fnt_18 lh_28 link_blue" href="${uniSPList.spURL}">${uniSPList.advertName}</a>
            </h2>
           </c:if>
            <c:if test="${not empty uniSPList.advertDescShort}">
             <div class="fnt_lrg fnt_14 lh_24 cl_grylt mt5 pos_rel">
                ${uniSPList.advertDescShort}...
                <a class="pos_abs_rb fnt_lbd fnt_14 lh_24 link_blue" href="${uniSPList.spURL}">READ MORE</a>
             </div>
          </c:if>
         </div>
       </div>
     </c:forEach> 
     <c:if test="${requestScope.uniSPListSize gt 4}">                   
       <div class="fl w100p">
         <a class="clear btn1 btn2 mt30" id="spViewMorwLink" onclick="javascript:showMoresLess();"><i class="fa fa-plus" ></i>VIEW MORE</a>
       </div>
     </c:if>
      <div id="spMoreDiv" style="display:none;">
      <c:forEach var="uniSPLists" items="${requestScope.uniSPList}" varStatus="indexs" begin="4">
       <div class="mt20 fl">
        <c:if test="${empty uniSPLists.imagePath}">
          <div class="fl img_ppn noimage"></div>
        </c:if>
        <c:if test="${not empty uniSPLists.imagePath}">        
          <div class="fl img_ppn">
            <a href="${uniSPLists.spURL}">
            <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="${uniSPLists.imagePath}" alt="${uniSPLists.advertName}" title="">
            </a>
           </div>
         </c:if>                  
         <div class="div_right">
          <c:if test="${not empty uniSPLists.advertName}"> 
            <h2>
              <a class="fnt_lbd fnt_18 lh_28 link_blue" href="${uniSPLists.spURL}">${uniSPLists.advertName}</a>
            </h2>
           </c:if>
            <c:if test="${not empty uniSPLists.advertDescShort}">
             <div class="fnt_lrg fnt_14 lh_24 cl_grylt mt5 pos_rel">
                 ${uniSPLists.advertDescShort}...
                <a class="pos_abs_rb fnt_lbd fnt_14 lh_24 link_blue" href="${uniSPLists.spURL}">READ MORE</a>
             </div>
          </c:if>
         </div>
        </div>
     </c:forEach>
    </div>
    <c:if test="${requestScope.uniSPListSize gt 4}"> 
       <div class="fl w100p">
         <a style="display:none;" id="spViewLessLink" class="clear btn1 btn2 mt30" onclick="javascript:showMoresLess();"><i class="fa fa-minus"></i>VIEW LESS</a>
       </div>
     </c:if>
   </section>
   <div class="borderbot mt40 fl"></div>
 </c:if>

