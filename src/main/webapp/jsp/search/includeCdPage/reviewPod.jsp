<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%--
  * @purpose  Review pod in course detail page
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                Rel Ver.
  * 18-DEC-2018    Hema.S                    584      First Draft                                                 584
  * *************************************************************************************************************************
--%>

<%String collegeId = (String)request.getAttribute("collegeId");
  String firstSubjectName = (String)request.getAttribute("firstSubjectName");
   String commonPhraseStlye = "display:block";
  String commonPhraseClassName = "defaultCP";
%>
<c:if test="${not empty requestScope.getReviewList}">
  <section class="reviews">
   <div id="div_student_reviews"></div>
    <div class="rrev_sec nprof rev_cd">
      <div class="reviews late_rev drp_hlght" id="write_review_pod">     
        <div class="lrhd_sec">
          <h2 class="sub_tit fnt_lrg fl txt_lft whclr" id="selectedSubjectDropDown">Latest <%if(firstSubjectName != null){%><%=firstSubjectName%><%}%> reviews</h2>
          <c:if test="${not empty requestScope.courseReviewSubjectList}">
            <fieldset class="fs_col2 fr" id="location" onclick="openDropdown()">
              <div class="od_dloc" id="allSubjectDropDownId"><span><%=firstSubjectName%></span><span><i class="fa fa-angle-down"></i></span></div>
              <div class="opsr_lst" id="subjectDropDown">
                <ul>
                  <c:forEach var="courseReviewSubjectList" items="${requestScope.courseReviewSubjectList}" varStatus="index">
                    <li onclick="subReviewBreakDown('${index.index}','<%=collegeId%>','${courseReviewSubjectList.categoryCode}','${courseReviewSubjectList.subjectName}')"><span><a id="subjectDropDownID_${index.index}">${courseReviewSubjectList.subjectName}</a> </span></li>
                  </c:forEach>
                </ul>
              </div>
            </fieldset>
          </c:if>
        </div>
      <div id="subjectDropDownId">
      <jsp:include page="/jsp/search/includeCdPage/reviewSubjectBreakDown.jsp"/>
      </div>
               <div class="odsrch_cnt rrsrch_cnt rev_men ipro_rvmen">
	      <div class="odsrh_col1 fl revm_lft">
                <label class="rrev_txt">Search reviews that mention</label>
                  <fieldset class="odsrh_fie">
                    <form name="reviewSearchKwdForm" id="reviewSearchKwdForm" method="post" action="/home.html" onsubmit="javascript:return commonPhrasesListURL('reviewSearchKwdForm')">
                     <input type="text" name="reviewSearchKwd" id="reviewSearchKwd" onkeyup="validateSrchIcn('key_icn','reviewSearchKwd')" onkeydown="validateSrchIcn('key_icn','reviewSearchKwd')" autocomplete="off" value="Something specific?" onclick="clearReviewSearchText(this,'Something specific?')" onblur="setReviewSearchText(this,'Something specific?')">
                    </form>
                    <i class="fa fa-search srch_dis" aria-hidden="true" id="key_icn" onclick="javascript:return commonPhrasesListURL('reviewSearchKwdForm')"></i>
                  </fieldset>
              </div>
              <c:if test="${not empty requestScope.commonPhrasesList}">
              <div class="odsrh_col1 fl revm_rht">
                <div class="swrd_cnt"><span>or</span></div>
                  <div class="swrd_cnt">
                    <form name="reviewCommonPhraseKwdForm" id="reviewCommonPhraseKwdForm" method="post" action="/home.html" onsubmit="javascript:return commonPhrasesListURL('reviewCommonPhraseKwdForm')">
                      <ul>
                        <c:forEach var="commonPhraseListId" items="${requestScope.commonPhrasesList}" varStatus="index">
                        <c:set var="index" value="${index.index}" />
                        <%
                        int index = (Integer)pageContext.getAttribute("index");
                        if(index>0){
                         commonPhraseStlye = "display:none";                    
                         commonPhraseClassName = "extraCP";   
                         }%>
                        <li style="<%=commonPhraseStlye%>" class="<%=commonPhraseClassName%>" id="comPhrase_<%=index%>" onclick="commonPhrasesListURL('reviewCommonPhraseKwdForm','',this)" value="${commonPhraseListId.commonPhrases}"><a href="javascript:void(0);" title="${commonPhraseListId.commonPhrases}">${commonPhraseListId.commonPhrases}</a></li>          
                        </c:forEach>
                      </ul>
                    </form>
                  </div>
                  <div class="swrd_cnt">
                    <%if("display:none" == commonPhraseStlye){%>
                    <ul>
                      <li id="comPhraseViewMore" onclick="commonPhraseViewMoreAndLess('comPhraseViewMore')"><a href="javascript:void(0);" title="View more">+ View more</a></li>
                      <li id="comPhraseViewLess" onclick="commonPhraseViewMoreAndLess('comPhraseViewLess')" style="display:none"><a href="javascript:void(0);" title="View less">- View less</a></li>
                    </ul>
                    <%}%>
                 </div>        
               </div>
            </c:if>
            <div class="wr_ch_btn">
            <spring:message code="review.form.url" var="reviewFormUrl"/>
		  <a class="wrt_btn" id="write_rvw_btn" href="${reviewFormUrl}">
		     <spring:message code="write.a.review.btn.name"/><i class="fa fa-long-arrow-right"></i>
		  </a>
		</div>
        </div> 
    </div>
  </div>
  </section><div class="borderbot mt40 fl"></div>
</c:if>
