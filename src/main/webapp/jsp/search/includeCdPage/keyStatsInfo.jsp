<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import=" WUI.utilities.*, java.util.ArrayList" %>

<script type="text/javascript" language="javascript" src="<%=new CommonUtil().getJsPath()%>/js/DonutChart/jquery.js"></script>
<script type="text/javascript" language="javascript" src="<%=new CommonUtil().getJsPath()%>/js/DonutChart/jquery.drawDoughnutChart_1_20200818.js"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/rpDoughnutchart_wu542.js"></script>

<%
    String toolTip = new CommonFunction().getDomainName(request, "N");
    pageContext.setAttribute("toolTip", toolTip);
		String keyStatsViewMore = request.getParameter("keyStatsViewMore")!=null? request.getParameter("keyStatsViewMore"): "";
    String displayNone = "display:block";
    String displayBlock = "display:none";
    if(keyStatsViewMore != null && "Y".equals(keyStatsViewMore)){
        displayNone = "display:none";
        displayBlock = "display:block";
    }    
    ArrayList listOfWuscaRatingInfo = request.getAttribute("listOfWuscaRatingInfo") != null ? (ArrayList)request.getAttribute("listOfWuscaRatingInfo") : null;
    ArrayList uniRankingList = request.getAttribute("uniRankingList") != null ? (ArrayList)request.getAttribute("uniRankingList") : null;
    ArrayList listOfUniStats = request.getAttribute("listOfUniStats") != null ? (ArrayList)request.getAttribute("listOfUniStats") : null;
%>
<%if((listOfWuscaRatingInfo != null && listOfWuscaRatingInfo.size() > 0) || (uniRankingList != null && uniRankingList.size() > 0) 
	    || (listOfUniStats != null && listOfUniStats.size() > 0)){ %>
<div class="clear"></div>
<div class="content-bg course_deatils clear">
  <section class="key_stats kys greybg">    
  <h2 class="sub_tit fl fnt_lrg lh_22">Key Stats</h2>
 
 <%if((listOfWuscaRatingInfo != null && listOfWuscaRatingInfo.size() > 0) || (uniRankingList != null && uniRankingList.size() > 0)){%>  
  <div class="borderbot1 fl mt25 mb20"></div>
  <div class="clear"></div>
  <h2>Uni rankings and ratings</h2>
  <div class="clear fl w100p mt20">
	  <%--Code for WUSCA Rating added by Prabha on 27_JAN_2015_REL--%>
	  <c:if test="${not empty requestScope.listOfWuscaRatingInfo}">
      <%String cuYear3 = new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS");
      pageContext.setAttribute("cuYear3", cuYear3);
      String apostrophe = "'";
      pageContext.setAttribute("apostrophe", apostrophe);
      %>
        <div class="twoC col3 fl">
          <h6>
            <span class="hdf3 fl">WUSCA overall rating</span>
            <span class="tool_tip">  
              <i class="fa fa-question-circle fnt_24">
                <span class="cmp tl_pos_top">     
                  <div class="hdf5"></div>
                  <div><spring:message code="wuni.tooltip.wusca.keystats" arguments="${apostrophe}, ${cuYear3},${toolTip},'' "/><!-- For local add /degrees/home.html in agr1 --></div>
                  <div class="line"></div>
                </span>
              </i>
            </span>  
            <c:forEach var="wuscaRatingInfo" items="${requestScope.listOfWuscaRatingInfo}">                 
           <c:if test="${not empty wuscaRatingInfo.overallRating}">
                <c:set var="rating" value="${wuscaRatingInfo.overallRating}"/>
                <div class="rate_trophy">
                  <span class="rat${rating} mt5"></span> (${wuscaRatingInfo.reviewCount})&nbsp; 
                </div>
              </c:if>   
            </c:forEach>       
          </h6>
        </div>     
		</c:if>
		<%--End of WUSCA Rating codes--%>
		<c:if test="${not empty requestScope.uniRankingList}">
      <c:forEach var="uniRanking" items="${requestScope.uniRankingList}">
      <c:if test="${not empty uniRanking.studentRanking}">
         <c:if test="${not empty uniRanking.totalStudentRanking}">
            <%String cuYear1 = new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS");
              pageContext.setAttribute("cuYear1", cuYear1);
            %>
            <div class="twoC col3 pt15 fl pt23">
              <h6>
                <span class="hdf3 fl">WUSCA student ranking</span>
                <span class="tool_tip">  
                  <i class="fa fa-question-circle fnt_24">
                    <span class="cmp">   
                      <div class="hdf5"></div> <%--Moved the tooltip text to application properties May_31_2016--%>
                      <div><spring:message code="wuni.tooltip.wusca.ratings" arguments="${cuYear1}" /></div>
                      <div class="line"></div>
                    </span>
                  </i>
                </span>     
                <div class="fl w100p">
                  <span class="hdf1">${uniRanking.studentRanking}</span>
                  <span class="hdf1">/</span>
                  <span class="hdf2">${uniRanking.totalStudentRanking}</span>
                </div>
              </h6>
            </div>       
         </c:if>
        </c:if>
        <c:if test="${not empty uniRanking.uniTimesRanking}">
      <div class="twoC col3 pt15 fl pt23">      
        <h6>
          <span class="hdf3 fl">CompUniGuide ranking</span> <%--Changed ranking lable and tooltip content from Times to CUG for 08_MAR_2016 By Thiyagu G--%>
          <span class="tool_tip fl right"> 
              <i class="fa fa-question-circle fnt_24">
                <span class="cmp">     
                  <div class="hdf5">Complete University Guide Ranking</div>
                  <div><spring:message code="wuni.tooltip.cug.ranking.text" /></div>
                  <div class="line"></div>
                </span>
              </i>
            </span>
          <div class="fl w100p">
            <span class="hdf1">${uniRanking.uniTimesRanking}</span>
          </div>
        </h6>  
      </div> 
     </c:if>
   </c:forEach>
    </c:if>
  </div>
  <%}%>
  <c:if test="${not empty requestScope.listOfUniStats}">
    <div id="keyStatsFullView" class="uni_rank mt cf" style="<%=displayNone%>">   
     <div class="borderbot1 fl mt23 mb20"></div>
      <h2><span class="fl tp_title">Student stats</span>      
         <span class="tool_tip fl">  
          <i class="fa fa-question-circle fnt_24">
            <span class="cmp">     
              <div class="hdf5"></div>
              <div><spring:message code="wuni.tooltip.hesa.entry.text" /></div>
              <div class="line"></div>
            </span>
          </i>
        </span>
      </h2>    
       <div class="fr right_link">DATA SOURCE: <a class="link_blue" rel="nofollow" href="/degrees/jsp/search/kisdataStatic.jsp">HESA</a></div>
      <div class="keyca mt10">
        <%--Redesigned keystats pod on 16_May_2017, By Thiyagu G--%>
        <c:forEach var="uniStats" items="${requestScope.listOfUniStats}">      
          <div class="keycaf col3 col_1">
            <c:if test="${not empty uniStats.fullPartTime}">
              <div class="fl keyml">
                <c:set var="full_part" value="${uniStats.fullPartTime}"/>
                <%
                  String fullTime ="", partTime = "", fullTimePer = "", partTimePer = "";
                  String fullPartSplit [] = ((String)pageContext.getAttribute("full_part")).split(":");
                  if(fullPartSplit[0]!=null && !"".equals(fullPartSplit[0])){
                    fullTime = fullPartSplit[0];
                    fullTimePer = fullPartSplit[0] + "%";
                  }              
                   if(fullPartSplit[1]!=null && !"".equals(fullPartSplit[1])){
                    partTime = fullPartSplit[1];
                    partTimePer = fullPartSplit[1] + "%";
                  }  
                %>
                <div class="fl">
                  <div id="fp_doughnut" class="chart w_149"></div>              
                </div>
                <div class="fl circle_desc">
                  <div class="fl w150 ">
                    <div class="circle red fl"></div>
                    <div class="fl keyfw">
                      <span class="hdf1 fl keyfwc"><%=fullTimePer%></span>
                      <span class="hdf3 fl keyfwc">FULL-TIME</span>
                    </div>
                  </div>
                  <div class="fl w150">
                    <div class="circle maroon  fl"></div>
                    <div class="fl keyfw">
                      <span class="hdf1 fl keyfwc"><%=partTimePer%></span>
                      <span class="hdf3 fl keyfwc">PART-TIME</span>
                    </div>
                  </div>
                </div>              
              </div>
              <script type="text/javascript" id="script_how_you_are_assesed">                          
                drawRPdoughnutchart('<%=fullTime%>','<%=partTime%>','fp_doughnut');
              </script>  
            </c:if>          
          </div>  
          <div class="keycas keympl col3 col_2">
            <c:if test="${not empty uniStats.schoolMature}">
              <c:set var="school_leaver" value="${uniStats.schoolMature}"/>
              <%
                String schoolCtn ="", schoolLeave = "", schoolCtnPer = "", schoolLeavePer = "";
                String schoolCtnLeaveSplit [] = ((String)pageContext.getAttribute("school_leaver")).split(":");
                if(schoolCtnLeaveSplit[0]!=null && !"".equals(schoolCtnLeaveSplit[0])){
                  schoolCtn = schoolCtnLeaveSplit[0];
                  schoolCtnPer = schoolCtnLeaveSplit[0] + "%";
                }              
                if(schoolCtnLeaveSplit[1]!=null && !"".equals(schoolCtnLeaveSplit[1])){
                  schoolLeave = schoolCtnLeaveSplit[1];
                  schoolLeavePer = schoolCtnLeaveSplit[1] + "%";
                }  
              %>
              <div class="fl keyml mt28">
                <div class="fl">
                  <div id="school_doughnut" class="chart w_149"></div>                
                </div>
                <div class="fl circle_desc">
                  <div class="fl w150 ">
                    <div class="circle red fl"></div>
                    <div class="fl keyfw">
                      <span class="hdf1 fl keyfwc"><%=schoolCtnPer%></span>
                      <span class="hdf3 fl keyfwc">SCHOOL-LEAVERS</span>
                    </div>                    
                  </div>  
                  <div class="fl w150">
                    <div class="circle maroon  fl"></div>
                    <div class="fl keyfw">
                      <span class="hdf1 fl keyfwc"><%=schoolLeavePer%></span>
                      <span class="hdf3 fl keyfwc">MATURE STUDENTS</span>
                    </div>
                  </div>
                </div>
              </div>             
              <script type="text/javascript" id="script_how_you_are_assesed">                          
                drawRPdoughnutchart('<%=schoolCtn%>','<%=schoolLeave%>','school_doughnut');
              </script>
            </c:if>
          </div>
          <div class="keycat fl col_3">
          <c:if test="${not empty uniStats.ugpg}">
             <c:set var="ug_pg" value="${uniStats.ugpg}"/>
              <%
                String ugStud ="", pgStud = "", ugStudPer = "", pgStudPer = "";
                String upPgSplit [] = ((String)pageContext.getAttribute("ug_pg")).split(":");
                if(upPgSplit[0]!=null && !"".equals(upPgSplit[0])){
                  ugStud = upPgSplit[0];
                  ugStudPer = upPgSplit[0] + "%";
                }              
                if(upPgSplit[1]!=null && !"".equals(upPgSplit[1])){
                  pgStud = upPgSplit[1];
                  pgStudPer = upPgSplit[1] + "%";
                }  
              %>
              <div class="fl mt28">
                <div class="fl">
                  <div id="ugPg_doughnut" class="chart w_149"></div>
                </div>
                <div class="fl circle_desc">
                  <div class="fl w150 ">
                    <div class="circle red fl">	</div>
                    <div class="fl keyfw">
                      <span class="hdf1 fl keyfwc"><%=ugStudPer%></span>
                      <span class="hdf3 fl keyfwc">UNDERGRADUATE</span>
                    </div>
                  </div>
                  <div class="fl w150">
                    <div class="circle maroon  fl">	</div>
                    <div class="fl keyfw">
                      <span class="hdf1 fl keyfwc"><%=pgStudPer%></span>
                      <span class="hdf3 fl keyfwc">POSTGRADUATE</span>
                    </div>
                  </div>
                </div>                
              </div>
              <script type="text/javascript" id="script_how_you_are_assesed">                          
                drawRPdoughnutchart('<%=ugStud%>','<%=pgStud%>','ugPg_doughnut');
              </script>
            </c:if>
          </div>
          <div class="keycat row2 fl mt35">
            <div class="keycatin">    
            <c:if test="${not empty uniStats.jobOrWork}">    
             <c:set var="jobPlaced" value="${uniStats.jobOrWork}"/>
                <%
                  Integer noOfJobPlaced = Integer.parseInt((String)pageContext.getAttribute("jobPlaced"));
                  Integer noOfJobLeft =  100 - noOfJobPlaced;                                
                %>
                <div class="fl lfcke sec">
                  <h2 class="fl w100p"><span class="lh_24 fl tp_title">Graduate prospects</span>
                    <span class="tool_tip fl right" href="#"> 
                      <i class="fa fa-question-circle fnt_24">
                        <span class="cmp">     
                          <div class="hdf5"></div>
                          <div><spring:message code="wuni.tooltip.dlhe.entry.text" /></div>
                          <div class="line"></div>
                        </span>
                      </i>
                    </span>
                    </h2>
                  <p>% IN JOB OR FURTHER STUDY</p>
                  <div class="fl">
                    <div id="job_doughnut" class="chart w_149"></div>
                  </div>
                </div>
                <script type="text/javascript" id="script_how_you_are_assesed">                          
                  drawRPdoughnutchart('<%=noOfJobPlaced%>','<%=noOfJobLeft%>','job_doughnut','plain');
                </script> 
              </c:if>    
              <c:if test="${not empty uniStats.noOfStudents}">
                <div class="fl lfcke">
                 <h2 class="fl w100p"><span class="lh_24 fl tp_title">Student numbers</span>
                    <span class="tool_tip fl right"> 
                      <i class="fa fa-question-circle fnt_24">
                        <span class="cmp">     
                          <div class="hdf5"></div>
                          <div><spring:message code="wuni.tooltip.hesa.entry.text" /></div>
                          <div class="line"></div>
                        </span>
                      </i>
                    </span>
                  </h2>
                  <p class="pb2">TOTAL UNDERGRADUATE STUDENTS</p>
                  <div class="hdf1 fl">${uniStats.noOfStudents}</div>
                  <div class="clear"></div>
                </div>
              </c:if>  
              <c:if test="${not empty requestScope.studyAbroadFlag}">
               <c:if test="${requestScope.studyAbroadFlag eq 'Y'}">
                  <div class="colc fl lfcke">
                    <h6 class="pt20">
                      <span class="hdf3">Is there the option to study abroad at this uni?</span>
                      <div class="fl w100p ">
                        <span class="hdf1 fngr"><i class="fa fa-check"></i> Yes</span>
                      </div>
                    </h6>
                  </div>
                </c:if>
              </c:if>
            </div>
          </div>
        </c:forEach>     
      </div>           
     </div>
     <c:set var="displayBlock" value="<%=displayBlock%>"/>
    <div class="fl w100p" style="${displayBlock}"> <%--Added VIEW MORE button for PR page's key stats pod alone, 24_Feb_2015 By Thiyagu G--%>
      <a onclick="adviceShowHide('keyStatsFullView','keyStatsFullView_More');" class="btn_view" title="VIEW MORE" style="display:block;" id="keyStatsFullView_More"><i class="fa fa-plus"></i>VIEW MORE</a>      
    </div>
  </c:if>   
 </section>
</div>
<%}%>