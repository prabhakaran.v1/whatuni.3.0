<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import=" WUI.utilities.CommonUtil,  com.wuni.util.seo.SeoUrls, WUI.utilities.GlobalConstants" %>

<c:if test="${not empty requestScope.listOfLocationInfo}">
             
<%
        String latitudeStr =  (String)request.getAttribute("latitudeStr"); 
        String longitudeStr =  (String)request.getAttribute("longitudeStr");
        String location = (String)request.getAttribute("cityGudieLocation");
               location = location!= null && location.trim().length()>0 ? location :"";
        String headline = (String)request.getAttribute("cityGudieHeadLine");
               headline = headline!= null && headline.trim().length()>0 ? headline :"";
        String map_url = GlobalConstants.MAP_URL + "&size=640x443&markers=color:red|";    
        String loadNewMapJs = CommonUtil.getResourceMessage("wuni.load.new.map.js", null);
%>

<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=loadNewMapJs%>"></script>
<section class="study_loc p0">
  <h2 class="sub_tit fnt_lrg fl txt_lft w100p mbxhd">Where you'll study</h2>
  <div id="div_location"></div>
  <input type="hidden" id="latitude" value="<%=latitudeStr%>"/>
      <input type="hidden" id="longitude" value="<%=longitudeStr%>"/>
  <div class="fl w100p map_staimg1">
    <c:forEach var="venue" items="${requestScope.listOfLocationInfo}" varStatus="index"> 
      <div id="locationinfoMaphh" style="display:none;">
          <p style="font-weight:bold;">
            <strong>
            <c:if test="${not empty venue.addLine1}"> 
              ${venue.addLine1}
            </c:if>
            <c:if test="${not empty venue.addLine2}"> 
              ${venue.addLine2}
            </c:if>
            <c:if test="${not empty venue.town}"> 
              ${venue.town}
            </c:if>
            <c:if test="${not empty venue.countryState}"> 
              ${venue.countryState}
            </c:if>
              ${venue.postcode}
            </strong><br/>
          </p>
       </div>
        <%--Changed static google map to mapbox for Feb_12_19 rel by Sangeeth.S--%>
       <jsp:include page="/jsp/common/mapbox.jsp" /> 
       
       <input type="hidden" id="mapDivContent" name="mapDivContent" value="0"/><%--Added by Prabha for lazy load map on 27_JAN_2016_REL--%>
            <div class="fl w100p mt35" itemscope itemtype="http://schema.org/EducationalOrganization">
             
            
                <div class ="fl w100p mt10" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                 <div class="fl mob_100p">
                   <h2>
              <span itemprop="name" class="title">${venue.collegeNameDisplay}</span></h2>
                  <span itemprop="streetAddress" class="address">
                       <c:if test="${not empty venue.addLine1}"> 
                          ${venue.addLine1}
                      </c:if>
                      <c:if test="${not empty venue.addLine2}"> 
                          ${venue.addLine2}
                      </c:if>
                    </span>
                     <c:if test="${not empty venue.town}"> 
                      <span itemprop="addressLocality" class="address">
                         ${venue.town}<br/>
                      </span>
                    </c:if>
                      <c:if test="${not empty venue.countryState}"> 
                       <span itemprop="addressRegion" class="address">
                          ${venue.countryState}<br/> 
                        </span>
                    </c:if>   
                    <span itemprop="postalCode" class="address">
                      ${venue.postcode}
                    </span>
                    <span class="address">United Kingdom</span>
                    <c:if test="${CLEARING_CD_PAGE eq 'CLEARING_CD_PAGE'}">
                      <span class="cdp_smlbrdrsprow">
                       <span class="cdp_smlbrdrspr"></span>
                      </span>
                    </c:if>
                    <c:if test="${venue.isInLondon eq 'LONDON'}">
                       <span class="address1"> Nearest tube station: <c:if test="${not empty venue.nearestTubeStation}">${venue.nearestTubeStation}&nbsp;</c:if> ${venue.distanceFromTubeStation} miles away</span>
                       <span class="address1"> Nearest train station: <c:if test="${not empty venue.nearestStationName}">${venue.nearestStationName}&nbsp;</c:if> ${venue.distanceFromNearestStation} miles away</span>
                    </c:if>
                    <c:if test="${venue.isInLondon ne 'LONDON'}">
                        <span class="address1"> Nearest train station: <c:if test="${not empty venue.nearestStationName}">${venue.nearestStationName}&nbsp;</c:if>${venue.distanceFromNearestStation} miles away</span>
                    </c:if>      
                  </div>
                     <c:if test="${venue.cityGuideDisplayFlag eq 'Y'}">
                      <div class="fr think_loc">
                         <p class="fnt_lbd fnt_18 lh_24 cl_grylt">Thinking of studying in ${requestScope.cityGudieLocation}?</p>                       
                         <p class="fnt_lrg fnt_14 lh_24 cl_grylt">Check out our <a class="fnt_lrg fnt_14 lh_24 link_blue" href="${venue.cityGuideUrl}">${requestScope.cityGudieLocation} city guide</a>
                       </p>        
                      </div>
                    </c:if>
                 </div>             
             
              </div>
          </c:forEach>
      </div>     
   </section>
    <div class="borderbot mt40 fl"></div>
</c:if>
