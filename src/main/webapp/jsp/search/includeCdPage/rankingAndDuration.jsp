<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="WUI.utilities.CommonFunction,WUI.utilities.CommonUtil"%>  

<% CommonUtil util = new CommonUtil();%>
<c:if test="${not empty requestScope.uniRankingFnList}">
  <div class="uni_rank cf">
    <h2 class="fnt_lbd lh_24 fl pb20">Uni rankings</h2>
    <%--Commented by Prabha on 29_Mar_2016
    <div class="fr right_link">
      DATA SOURCE: <a class="cl_blult" rel="nofollow" href="/degrees/jsp/search/kisdataStatic.jsp">CompUniGuide</a>
    </div> --%>
    <c:forEach var="uniRankingFnList" items="${requestScope.uniRankingFnList}"> 
      <c:if test="${not empty uniRankingFnList.timesSubRankingList}">
        <fieldset class="set_drop sub_drp_act">
          <fieldset class="cd_sub_uni">
           <c:set var="timesSubRankingListSize" value="${fn:length(uniRankingFnList.timesSubRankingList)}"/>
            <c:if test="${timesSubRankingListSize gt 1}">
            <div class="select dis_edu">
                <span id="subspanul" class="fnt_lbd">
                   <c:forEach var="timesSubRankingList" items="${uniRankingFnList.timesSubRankingList}" end="0"> 
                     <c:if test="${not empty timesSubRankingList.subjectName}">
                        <c:set var="subjectNameTrim" value="${timesSubRankingList.subjectName}"/>
                        <%
                        String subjectNameTrim = (String)pageContext.getAttribute("subjectNameTrim");
                        if(subjectNameTrim.length()>22){ subjectNameTrim = subjectNameTrim.substring(0,22) + "..";}%>
                        <%=subjectNameTrim%>    
                      </c:if>
                   </c:forEach>
                </span>
                  <i class="fa fa-angle-down fa-lg"></i>
            </div>
            <select id="subjectSel" class="dis_edu" name="subjectStats" onchange="setSelectedValTimeSubRanking(this, 'subspanul');">
                <c:forEach var="timesSubRankingList" items="${uniRankingFnList.timesSubRankingList}">
                  <option value="${timesSubRankingList.currentRanking}<sup>${timesSubRankingList.thAndRdValue}</sup>">
                    ${timesSubRankingList.subjectName}
                 </option> 
                 </c:forEach>
            </select>
         </c:if>
        </fieldset>
      </fieldset>  
      </c:if>
      <div class="clear fl mt10 w100p">
        <c:if test="${not empty uniRankingFnList.studentRanking}">
        <%String cuYear2 = new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS");%>
          <div class="col3 fl">
            <h6 class="fnt_14 cl_grylt pt20">
              <span class="fnt_lrg fnt_14 lh_24 fl mr5">WUSCA student ranking</span>
              <span class="tool_tip">
                <i class="fa fa-question-circle fnt_24">
                  <span class="cmp fnt_lbd">
                    <div class="fnt_lbd fnt_14 lh_24">WUSCA student ranking</div>
                    <%--<div>These rankings are drawn from ratings given by past and current students.</div>--%>
                    <div><spring:message code="wuni.tooltip.wusca.ratings" arguments="<%=cuYear2%>" /></div><%--Moved the tooltip text to application properties May_31_2016--%>
                    <div class="line"></div>
                  </span>
                </i>
              </span>
              <div class="fl w100p">
               <c:if test="${not empty uniRankingFnList.studentRanking}"> 
               <%--Added th and nd text in wusca student ranking by Hema.S on 28-Aug-2018_rel--%>
               <c:set var="studentRanking" value="${uniRankingFnList.studentRanking}"/>
                 <%
                 String studentRanking =  (String)pageContext.getAttribute("studentRanking");
                 studentRanking = util.getSuperStringSTNDRDTH(studentRanking);%>
                <span class="fnt_lrg fnt_30 cl_grylt lh_24">${uniRankingFnList.studentRanking}<sup><%=studentRanking%></sup></span>
                <span class="fnt_lrg fnt_30 cl_grylter lh_24">/</span>
                <span class="fnt_lrg fnt_18 lh_24 cl_grylter">${uniRankingFnList.totalStudentRanking}</span>
                </c:if>
              </div>
             </h6>
           </div>
         </c:if>
         <c:if test="${not empty uniRankingFnList.uniTimesRanking}">
           <div class="col3 fl cug_wrap">
             <h6 class="fnt_14 cl_grylt pt20">
               <span class="fnt_lrg fnt_14 lh_24 fl mr5">CompUniGuide ranking</span>
               <span class="tool_tip">
                  <i class="fa fa-question-circle fnt_24">
                    <span class="cmp fnt_lbd">
                      <div class="fnt_lbd fnt_14 lh_24">Complete University Guide Ranking</div> <%--Changed ranking lable and tooltip from Times to CUG for 08_MAR_2016 By Thiyagu G--%>
                      <div><spring:message code="wuni.tooltip.cug.ranking.text" /></div>
                      <div class="line"></div>
                    </span>
                  </i>
                </span>
                <div class="fl w100p">
                   <span class="fnt_lrg fnt_30 cl_grylt lh_24">${uniRankingFnList.uniTimesRanking}<sup>${uniRankingFnList.thAndRdValue}</sup></span>
                </div>
              </h6>
           </div>
         </c:if>
         <c:if test="${not empty uniRankingFnList.timesSubRankingList}">
             <c:forEach var="timesSubRankingList" items="${uniRankingFnList.timesSubRankingList}" end="0">
                <div class="col3 fl cug_wrap">
                 <h6 class="fnt_14 cl_grylt pt20">
                   <span class="fnt_lrg fnt_14 lh_24 fl mr5">CompUniGuide subject ranking</span>  
                   <span class="tool_tip right">
                      <i class="fa fa-question-circle fnt_24">
                        <span class="cmp fnt_lbd">
                          <div class="fnt_lbd fnt_14 lh_24">Complete University Guide Subject Ranking</div> <%--Changed ranking lable and tooltip from Times to CUG for 08_MAR_2016 By Thiyagu G--%>
                          <div><spring:message code="wuni.tooltip.cug.ranking.text" /></div>
                          <div class="line"></div>
                        </span>
                      </i>
                    </span>
                    <div class="fl w100p">
                       <span class="fnt_lrg fnt_30 cl_grylt lh_24" id="currentRankingId">${timesSubRankingList.currentRanking}<sup>${timesSubRankingList.thAndRdValue}</sup></span>
                    </div>
                  </h6>
               </div>                    
           </c:forEach>    
         </c:if>
      </div> 
    </c:forEach>                          
  </div>
  <div class="borderbot1 fl mt20 mb20"></div>
</c:if>
<c:if test="${not empty requestScope.getCourseDurationList}">
  <%--Modified the text and commented the tool tip by Indumathi.S Apr_19_2016--%>
  <div class="uni_rank cf">
    <h2 class="fnt_lbd lh_24 fl w100p pb20 data_srce">Start date and duration
      <%--
      <span class="tool_tip">
        <i class="fa fa-question-circle fnt_24">
        <span class="cmp fnt_lbd">
        <div class="fnt_lbd fnt_14 lh_24">Duration</div>
        <div>How long you will spend studying for your qualification.</div>
        //Added course type flag and update date by Thiyagu G.S for 08_MAR_2016
        <div> Source: <a href="/degrees/jsp/search/kisdataStatic.jsp"><logic:present name="courseTypeText" scope="request"><bean:write name="courseTypeText" scope="request" /></logic:present></a>, <logic:present name="courseLastUpdatedDate" scope="request"><bean:write name="courseLastUpdatedDate" scope="request" /></logic:present></div>
        <div class="line"></div>
        </span>
        </i>
        </span>  
        --%>
        <div class="fr right_link">
         DATA SOURCE:
         <a class="cl_blult" rel="nofollow" href="/degrees/jsp/search/kisdataStatic.jsp"><c:if test="${not empty requestScope.courseTypeText}">${requestScope.courseTypeText}</c:if></a>
        </div>
      </h2>
     <c:forEach var="getCourseDurationList" items="${requestScope.getCourseDurationList}"> 
       <c:if test="${not empty getCourseDurationList.studyModes}">
         <c:if test="${not empty getCourseDurationList.shortStartDate}">
          <div class="col3 fl">
          <h6 class="fnt_14 cl_grylt pt20">
            <span class="fnt_lrg fnt_14 lh_24 fl mr5">Start date</span>
            <div class="fl w100p">
              <div class="fnt_lrg fnt_30 cl_grylt lh_24 show_date">
                ${getCourseDurationList.shortStartDate}
                <c:if test="${not empty getCourseDurationList.startDate}">
                <div class="mdl pl20 fl">
                  <span class="fnt_lrg fnt_16"> ${getCourseDurationList.startDate}</span>
                </div>
                </c:if>
              </div>
            </div>
          </h6>
        </div>
       </c:if>
        <div class="col3 fl col_icr sdd_sp">
          <h6 class="fnt_14 cl_grylt pt20">
            <span class="fnt_lrg fnt_14 lh_24 fl mr5"> ${getCourseDurationList.studyModes}</span>
            <div class="fl w100p">
              <%--<span class="fnt_lrg fnt_30 cl_grylt lh_24"><bean:write name="getCourseDurationList" property="durations"/></span>--%>
              <c:if test="${not empty getCourseDurationList.durations}">
                <c:set var="shortDurationName" value="${getCourseDurationList.durations}"/>
                 <% 
                 String shortDurationName = (String)pageContext.getAttribute("shortDurationName");
                  if(shortDurationName.length()>9){
                   shortDurationName = shortDurationName.substring(0,9) + "...";
                  }
                 %>              
                <div class="fnt_lrg fnt_30 cl_grylt lh_24 show_date">
                 <%=shortDurationName%>      
                  <% if(shortDurationName.length()>9){%>
                  <div class="mdl pl20 fl">
                    <span class="fnt_lrg fnt_16"> ${getCourseDurationList.durations}</span>
                  </div>   
                  <%}%>
                </div>
              </c:if>
            </div> 
          </h6>
        </div>
      </c:if> 
    </c:forEach>
  </div>
  <div class="borderbot1 fl mt30 mb20"></div>
</c:if> 
