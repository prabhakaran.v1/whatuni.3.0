<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${not empty requestScope.listOfTopCourses}">
  <section class="univer_like">
    <div id="div_top_courses"></div>
    <h2 class="sub_tit fnt_lrg txt_lft w100p cl_grylt">Other courses at this uni you might like</h2>
    <div class="mt20 mb35 fl w100p">
      <ul class="fl">
      <c:forEach var="listOfTopCourses" items="${requestScope.listOfTopCourses}" end="5">
      <c:if test="${not empty listOfTopCourses.courseTitle}">
          <li>
             <a href="${listOfTopCourses.courseDetailsPageURL}">${listOfTopCourses.courseTitle}</a>
              <div style="width:${listOfTopCourses.count}%"></div>
          </li>
        </c:if>
       </c:forEach>
      </ul>
      <ul class="fr">
      <c:forEach var="listOfTopCourses" items="${requestScope.listOfTopCourses}" begin="5">
        <c:if test="${not empty listOfTopCourses.courseTitle}">
          <li>
             <a href="${listOfTopCourses.courseDetailsPageURL}">${listOfTopCourses.courseTitle}</a>
              <div></div>
          </li>
        </c:if>
       </c:forEach>
      </ul>
    </div>
  </section>
</c:if>

  
  