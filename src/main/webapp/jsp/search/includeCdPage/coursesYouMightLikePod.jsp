<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="WUI.utilities.*" autoFlush="true" %>
<%
    String searchType = request.getParameter("searchtype");
    String CONTEXT_PATH = GlobalConstants.WU_CONTEXT_PATH;
    String collegeNameDispaly = (String)request.getAttribute("interactionCollegeNameDisplay");
    String courseTitle = (String)request.getAttribute("courseTitle");
    StringBuffer compareallcourses = new StringBuffer();
    boolean compareflag = false;
    String copclassname = "";
    if("CLEARING_COURSE".equalsIgnoreCase(searchType)){ copclassname = "clr15";}
    String compareAllCollegeNames = "";
    String gaCollegeName = "";
    int beanSize = 0; //Hided Compare All link if only one course/uni displayed in this pod for the bug:39149, on 08_Aug_2017, By Thiyagu G
%>
<c:if test="${not empty requestScope.listOfTopCourses}">
<c:set var="sizeoflist" value="${fn:length(requestScope.listOfTopCourses)}"/>
    <section class="nonadvert bg_bluelt grapm1 mt20">
      <div id="div_top_courses"></div>
      <h1 class="fnt_lrg fl">Other courses you might like</h1>
      <div class="borderbot1 fl mt25"></div>
      <c:forEach var="topCourse" items="${requestScope.listOfTopCourses}" varStatus="i">
           <c:set var="i" value="${i.index}"/>
           <c:set var="courseId" value="${topCourse.courseId}"/>
           <c:set var="shortListFlag" value="${topCourse.shortListFlag}"/>
           <c:set var="topCoursesSize" value="${fn:length(requestScope.listOfTopCourses)}"/>
           <c:if test="${not empty topCourse.gaCollegeName}">
           <c:set var="collegeName" value="${topCourse.gaCollegeName}"/>
            <%gaCollegeName = new CommonFunction().replaceSpecialCharacter((String)pageContext.getAttribute("collegeName"));
              compareAllCollegeNames += gaCollegeName; //Added college names for insights log by Prabha on 29_Mar_2016%>
          </c:if>
          <% 
            beanSize = Integer.valueOf(pageContext.getAttribute("topCoursesSize").toString());
              compareallcourses.append((String)pageContext.getAttribute("courseId"));
              if((Integer)pageContext.getAttribute("sizeoflist") - 1 != (Integer)pageContext.getAttribute("i")){
                compareallcourses.append(",");
                compareAllCollegeNames += GlobalConstants.SPLIT_CONSTANT;
              }
            if(!"TRUE".equalsIgnoreCase((String)pageContext.getAttribute("shortListFlag"))){
              compareflag = true;
            }        
          %>
          <div class="mt20 fl">
            <%if("CLEARING_COURSE".equalsIgnoreCase(searchType)){%>
            <input type="hidden" name="comparepage" id="comparepage" value="CLEARING"/>
           <%}%>  
           <%
            String topClassName = "fl img_ppn img_ppn_116 mr20 noimage";
           %>
           <c:if test="${topCourse.mediaType eq 'PICTURE'}">
             <c:if test="${not empty topCourse.mediaThumbPath}">
               <%topClassName = "fl img_ppn img_ppn_116 mr20";%>
             </c:if>
          </c:if>
          <c:if test="${topCourse.mediaType eq 'VIDEO'}">
             <c:if test="${not empty topCourse.videoThumbPath}">
               <%topClassName = "fl img_ppn img_ppn_116 mr20";%>
            </c:if>
          </c:if>
             <div class="<%=topClassName%>">
                 <c:if test="${topCourse.mediaType eq 'PICTURE'}">
                 <a href="${topCourse.uniHomeURL}">
                   <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="${topCourse.mediaThumbPath}" alt="${topCourse.collegeNameDisplay}" title="${topCourse.collegeNameDisplay}">
                  </a>
                </c:if>
                <c:if test="${topCourse.mediaType eq 'VIDEO'}">
                  <a href="${topCourse.uniHomeURL}">
                    <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="${topCourse.mediaThumbPath}" alt="${topCourse.collegeNameDisplay}" title="${topCourse.collegeNameDisplay}" />
                  </a>            
                 </c:if>
             </div>
             <%--Add to compare redesign, 03_Feb_2015 By Thiyagu G--%>
             <%if("NORMAL_COURSE".equalsIgnoreCase(searchType)){%>
             <div class="ver_cmp">
               <c:if test="${topCourse.shortListFlag eq 'TRUE'}">
                  <div class="cmlst" id="basket_div_${topCourse.courseId}" style="display:none;"> 
                    <div class="compare">
                    <a onclick='addBasket("${topCourse.courseId}", "O", this,"basket_div_${topCourse.courseId}", "basket_pop_div_${topCourse.courseId}", "", "", "<%=gaCollegeName%>");'>
                      <span class="icon_cmp f5_hrt"></span>
                      <span class="cmp_txt">Compare</span>
                      <span class="loading_icon" id="load_${topCourse.courseId}" style="display:none;position:absolute;"></span>
                      <span class="chk_cmp"><span class="chktxt">Add to comparison<em></em></span></span>
                    </a>
                    </div>
                  </div>
                  <div class="cmlst act" id="basket_pop_div_${topCourse.courseId}">
                    <div class="compare">
                    <a class="act" onclick='addBasket("${topCourse.courseId}", "O", this,"basket_div_${topCourse.courseId}", "basket_pop_div_${topCourse.courseId}");'>
                      <span class="icon_cmp f5_hrt hrt_act"></span>
                      <span class="cmp_txt">Compare</span>
                      <span class="loading_icon" id="load1_${topCourse.courseId}" style="display:none;position:absolute;"></span>
                      <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                    </a>
                    </div>   
                    <div id="defaultpop_${topCourse.courseId}" class="sta" style="display: block;">
                        <%
                            String userId = new SessionData().getData(request, "y");
                            if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
                        %>
                            <a class="view_more" href="<%=CONTEXT_PATH%>/comparison">View comparison</a>
                        <%}else{%>
                            <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
                        <%}%>
                      </div>
                    <div id="pop_${topCourse.courseId}" class="sta" style="display:none;"></div>
                  </div>
                  <!--<div class="shlst otr"><a class="act" ></a></div>-->
                </c:if>   
                 <c:if test="${topCourse.shortListFlag ne 'TRUE'}">
                  <div class="cmlst" id="basket_div_${topCourse.courseId}"> 
                    <div class="compare">
                    <a onclick='addBasket("${topCourse.courseId}", "O", this,"basket_div_${topCourse.courseId}", "basket_pop_div_${topCourse.courseId}", "", "", "<%=gaCollegeName%>");'>
                      <span class="icon_cmp f5_hrt"></span>
                      <span class="cmp_txt">Compare</span>
                      <span class="loading_icon" id="load_${topCourse.courseId}" style="display:none;position:absolute;"></span>
                      <span class="chk_cmp"><span class="chktxt">Add to comparison<em></em></span></span>
                    </a>
                    </div>
                    
                  </div>
                  <div class="cmlst act" id="basket_pop_div_${topCourse.courseId}" style="display:none;">
                    <div class="compare">
                    <a class="act" onclick='addBasket("${topCourse.courseId}", "O", this,"basket_div_${topCourse.courseId}", "basket_pop_div_${topCourse.courseId}");'>
                      <span class="icon_cmp f5_hrt hrt_act"></span>
                      <span class="cmp_txt">Compare</span>
                      <span class="loading_icon" id="load1_${topCourse.courseId}" style="display:none;position:absolute;"></span>
                      <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
                    </a>
                    </div>
                    <div id="defaultpop_${topCourse.courseId}" class="sta" style="display: block;">
                        <%
                            String userId = new SessionData().getData(request, "y");
                            if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
                        %>
                            <a class="view_more" href="<%=CONTEXT_PATH%>/comparison">View comparison</a>
                        <%}else{%>
                            <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
                        <%}%>
                      </div>
                    <div id="pop_${topCourse.courseId}" class="sta"></div>
                  </div>                     
                </c:if>
               </div>
               <%}%>
             <div class="div_right fl mb20">
               <div class="fl">
                 <h2>
                    <a class="fnt_lbd fnt_18 lh_28 link_blue" href="${topCourse.courseDetailsPageURL}" title="${topCourse.courseTitle}">${topCourse.courseTitle}</a>
                  </h2>
                  <div class="fnt_lbd fnt_14 lh_24 cl_grylt pos_rel"> ${topCourse.collegeNameDisplay}</div>
               </div>
               
             </div>
             <c:if test="${not empty topCourse.subOrderItemId}">
               <c:if test="${topCourse.subOrderItemId ne 0}">
               <div class="div_three fr <%=copclassname%>">
                <div class="fr mt5 mb20">
                  <div class="btns_interaction">
              <%if("NORMAL_COURSE".equalsIgnoreCase(searchType)){%>
                 <c:if test="${not empty topCourse.subOrderWebsite}">
                  <a rel="nofollow" 
                      target="_blank" 
                      class="fl visit-web mr10" 
                      onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${topCourse.gaCollegeName}', ${topCourse.websitePrice}); cpeWebClick(this,'${topCourse.collegeId}','${topCourse.subOrderItemId}','${topCourse.cpeQualificationNetworkId}','${topCourse.subOrderWebsite}');var a='s.tl(';" 
                      href="${topCourse.subOrderWebsite}&courseid=${topCourse.courseId}" 
                      title="Visit ${topCourse.collegeNameDisplay} website">
                      Visit website
                      <i class="fa fa-caret-right"></i>
                  </a>
                </c:if>
                   <c:if test="${not empty topCourse.subOrderProspectusWebform}">
                    <a rel="nofollow" 
                        target="_blank"
                        class="last fl get-pros mr0"
                        onclick="GAInteractionEventTracking('prospectuswebform', 'interaction', 'Webclick', '${topCourse.gaCollegeName}', ${topCourse.webformPrice}); cpeProspectusWebformClick(this,'${topCourse.collegeId}','${topCourse.subOrderItemId}','${topCourse.cpeQualificationNetworkId}','${topCourse.subOrderProspectusWebform}'); var a='s.tl(';" 
                        href="${topCourse.subOrderProspectusWebform}" 
                        title="Get ${topCourse.collegeNameDisplay} Prospectus">
                         Get Prospectus
                      <i class="fa fa-caret-right"></i>
                    </a>
                  </c:if>
                  <c:if test="${not empty topCourse.subOrderProspectus and empty topCourse.subOrderProspectusWebform}">
                      <%--href="<SEO:SEOURL pageTitle="collegeprospectus" ><bean:write name="topCourse" property="collegeName" filter="false"/>#<bean:write name="topCourse" property="collegeId" />#<bean:write name="topCourse" property="courseId" />#0#n#<bean:write name="topCourse" property="subOrderItemId" /></SEO:SEOURL>" --%>
                      <a rel="nofollow"
                          onclick="GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '${topCourse.gaCollegeName}'); return prospectusRedirect('<%=CONTEXT_PATH%>','${topCourse.collegeId}','${topCourse.collegeId}','','','${topCourse.subOrderItemId}');"
                          class="last fl get-pros mr0"
                          title="Get ${topCourse.collegeNameDisplay} Prospectus" >
                           Get Prospectus
                      <i class="fa fa-caret-right"></i>
                      </a>   
                 </c:if>
                 <%}else{%> 
                 <c:if test="${not empty topCourse.hotLineNo}">
                      <a id="contact_<%=(Integer.parseInt(pageContext.getAttribute("i").toString()))%>" class="clr-call" onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'hotline', '${topCourse.gaCollegeName}');cpeHotlineClearing(this,'${topCourse.collegeId}','${topCourse.subOrderItemId}','${topCourse.cpeQualificationNetworkId}','${topCourse.hotLineNo}');changeContactButtonCD('${topCourse.hotLineNo}', '${ topCourse.cpeQualificationNetworkId}', '${topCourse.gaCollegeName}', '${topCourse.subOrderItemId}','contact_<%=(Integer.parseInt(pageContext.getAttribute("i").toString()))%>', '${topCourse.collegeId}');" ><i class="fa fa-phone" aria-hidden="true"></i>  CALL NOW</a> 
                  </c:if>
                  <c:if test="${not empty topCourse.subOrderWebsite}">            
                      <a rel="nofollow" 
                      target="_blank" 
                      class="fl visit-web mr10" 
                      onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${topCourse.gaCollegeName}', ${topCourse.websitePrice}); cpeWebClickClearing(this,'${topCourse.collegeId}','${topCourse.subOrderItemId}','${topCourse.cpeQualificationNetworkId}','${topCourse.subOrderWebsite}');" 
                      href="${topCourse.subOrderWebsite}" 
                      title="Visit ${topCourse.collegeNameDisplay} website">Visit website <i class="fa fa-caret-right"></i></a>
                  </c:if>
                           
              <%}%>
               </div>   
              </div>
              </div>
            </c:if>
            </c:if>
          </div>
          <div class="borderbot1 fl mt20 fl"></div>
        </c:forEach>
        <div class="borderbot1 mt0 fl"></div>
        <%String comparePodClass = "display:block;";
         if(("CLEARING_COURSE".equalsIgnoreCase(searchType))){
           comparePodClass = "display:none;";
         }
        %>
       <div class="mt20 fl" style="<%=comparePodClass%>">
          <%
            String noneComp = "display:none;";            
            if(beanSize > 1){
              noneComp = "display:block;";
            }
          %>
          <div class="hor_cmp" id="comapre_all_notdone" style="<%=noneComp%>">
            <%if(compareflag){%>
                <div class="cmlst" id="compareall"> 
                  <div class="compare">
                    <a onclick="compareAll('<%=compareallcourses%>', 'OTHER_COURSES', 'A', '', '<%=compareAllCollegeNames%>')">
                      <span class="icon_cmp f5_hrt"></span>
                      <span class="cmp_txt">Compare all</span>
                      <span class="loading_icon" id="load_icon" style="display:none"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                    </a>
                  </div>                  
                </div>        
                <div class="cmlst act" id="uncompareall" style="display:none">
                    <div class="compare">
                      <a class="act" onclick="compareAll('<%=compareallcourses%>', 'OTHER_COURSES', 'R');">
                        <span class="icon_cmp f5_hrt hrt_act"></span>
                        <span class="cmp_txt">Compare all</span>
                        <span class="loading_icon" id="load_icon" style="display:none"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                      </a>
                    </div>
                    <div id="popCourseIds" class="sta"></div>
                    <%--Added code for the bug:28713 on 03_Oct_2017, By Thiyagu G--%>
                    <div id="loadPopCourseIds" class="sta" style="display:none;">
                      <a class="view_more" onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');">View comparison</a>
                    </div>
                </div>  
               <%}else{%>    
               
                <div class="cmlst" id="compareall" style="display:none"> 
                  <div class="compare">
                    <a onclick="compareAll('<%=compareallcourses%>', 'OTHER_COURSES', 'A', '', '<%=compareAllCollegeNames%>')">
                      <span class="icon_cmp f5_hrt"></span>
                      <span class="cmp_txt">Compare all</span>
                      <span class="loading_icon" id="load_icon" style="display:none"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                    </a>
                  </div>                  
                </div>            
                <div class="cmlst act" id="uncompareall"> 
                  <div class="compare">
                    <a class="act" onclick="compareAll('<%=compareallcourses%>', 'OTHER_COURSES', 'R');">
                      <span class="icon_cmp f5_hrt hrt_act"></span>
                      <span class="cmp_txt">Compare all</span>
                      <span class="loading_icon" id="load_icon" style="display:none"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></span>
                    </a>
                  </div>
                  <div id="popCourseIds" class="sta"></div>
                  <%--Added code for the bug:28713 on 03_Oct_2017, By Thiyagu G--%>
                  <div id="loadPopCourseIds" class="sta" style="display:none;">
                    <a class="view_more" onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');">View comparison</a>
                  </div>
                </div>  
               <%}%>
          </div>
        <%--<div class="tbox comp_sel"  title="" onclick="compareAll('<%=compareallcourses%>', 'OTHER_COURSES')"></div>--%>
           <%if(compareflag){%> 
             <div  class="fl w580" id="comparealltext" style="<%=noneComp%>">
                <%--<h2  class="fnt_lbd lh_24" id="comparealltext">Compare all</h2>--%>
                <h4 class="fnt_lrg fnt_14 lh_24">Compare <%=courseTitle%> at <%=collegeNameDispaly%> against all the unis listed above.</h4>
             </div>
            <div class="fl w580" id="uncomparealltext" style="display:none">
              <%--<h2 class="fnt_lbd lh_24" id="comparealltext">Uncompare all</h2>--%>
              <h4 class="fnt_lrg fnt_14 lh_24">Uncompare <%=courseTitle%> at <%=collegeNameDispaly%> against all the unis listed above.</h4>					
          </div>
          <div id="load_icon" style="display:none"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"/></div>
        <%}else{%>
            <div  class="fl w580" id="comparealltext" style="<%=noneComp%>">      
              <%--<h2 class="fnt_lbd lh_24" id="comparealltext">Compare all</h2>--%>
              <h4 class="fnt_lrg fnt_14 lh_24">Compare <%=courseTitle%> at <%=collegeNameDispaly%> against all the unis listed above.</h4>
            </div>
            <div class="fl w580" id="uncomparealltext" style="display:none">
              <%--<h2 class="fnt_lbd lh_24" id="comparealltext">Uncompare all</h2>--%>
              <h4 class="fnt_lrg fnt_14 lh_24">Uncompare <%=courseTitle%> at <%=collegeNameDispaly%> against all the unis listed above.</h4>					
            </div>            
          <%}%>
    </div>
    </section>    
    <input type="hidden" id="currentcompare" value=""/>
    <input type="hidden" id="comparestatus" value=""/>
    <input type="hidden" id="compareallcourses" value="<%=compareallcourses%>"/> <%--Added code for the bug:28713 on 03_Oct_2017, By Thiyagu G--%>
</c:if>
  
  