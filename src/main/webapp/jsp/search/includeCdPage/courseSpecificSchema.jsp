<%--
  * @purpose  This is course specific schema tag..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 24-Jan-2017 Prabhakaran V.               561     First Draft                                                       561
  * *************************************************************************************************************************
--%>
<% String schemaTag = request.getParameter("schemaTag");%>
<script type="application/ld+json">
{
<%=schemaTag%>
}
</script>
