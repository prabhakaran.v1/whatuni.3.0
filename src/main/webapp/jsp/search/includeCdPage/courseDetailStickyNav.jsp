<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.SessionData,WUI.utilities.GlobalConstants, WUI.utilities.GlobalFunction,com.wuni.util.seo.SeoUrls, mobile.util.MobileUtils" %>
<%
    String paramCourseId = request.getParameter("w") !=null && request.getParameter("w").trim().length()>0 ? request.getParameter("w") : request.getParameter("cid") !=null && request.getParameter("cid").trim().length()>0 ? request.getParameter("cid") : ""; 
    paramCourseId = paramCourseId!=null && paramCourseId.trim().length()>0 ? "" : String.valueOf(request.getAttribute("seoCourseId"));
    boolean isClearingCourse = (((String)request.getAttribute("CLEARING_COURSE")) != null && "TRUE".equals((String)request.getAttribute("CLEARING_COURSE"))) ? true : false;
    SessionData sessionData = new SessionData();
    String extraClass = " mr20"; 
    String wp_collegeId = request.getAttribute("interactionCollegeId") != null && request.getAttribute("interactionCollegeId").toString().trim().length() > 0 ? (String)request.getAttribute("interactionCollegeId") : "0";
    String wp_collegeName = request.getAttribute("interactionCollegeName") != null && request.getAttribute("interactionCollegeName").toString().trim().length() > 0 ? (String)request.getAttribute("interactionCollegeName") : "0";
    String wp_collegeNameDisplay = request.getAttribute("interactionCollegeNameDisplay") != null && request.getAttribute("interactionCollegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("interactionCollegeNameDisplay") : "";
    String coureId = request.getParameter("courseId") != null && request.getParameter("courseId").trim().length() > 0 ? request.getParameter("courseId") : "0";
    String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(wp_collegeName);      
    if("0".equals(coureId)){
      extraClass = " mr17"; 
    }
    String clearingyear = GlobalConstants.CLEARING_YEAR;
    String courseFlag = "PROVIDER";
    if(!"0".equals(coureId)){courseFlag = "COURSE";}
    String institutionId = wp_collegeId != "0" ? new GlobalFunction().getInstitutionId(wp_collegeId) : ""; 
    String applyNowFlag = (String)request.getAttribute("checkApplyFlag");
    SeoUrls seoUrls = new SeoUrls();
    String opportunityId = (String)request.getAttribute("opportunityId");
    String cdOpportunityId = (String)request.getAttribute("cdOpportunityId");
    String oppId = null;
    if(cdOpportunityId != null ){
      oppId = cdOpportunityId;
    }
    if(opportunityId != null) {
      oppId = opportunityId;
    }
    boolean mobileFlag = new MobileUtils().userAgentCheck(request);
    String applySubOrderItemId = "";
%>
<%if(isClearingCourse){%>

     <c:if test="${not empty requestScope.courseInfoList}">
        <c:forEach var="courseInfoList" items="${requestScope.courseInfoList}"> 
          <h1 id="cdCourseTitle" class="cf fnt_lbd fnt_24 lh_28 cl_grylt txt_lft txt_normal">
             ${courseInfoList.courseTitle} 
          </h1>
           <c:if test="${not empty courseInfoList.departmentOrFaculty}">
              <c:if test="${not empty requestScope.profileType}">
                 <c:if test="${requestScope.profileType eq 'SP'}">
                <h2 id="cdSpName" class="cf" style="display: block;">
                  <a class="fl fnt_lbd fnt_18 lh_28 link_blue txt_upr" href="${courseInfoList.profileURL}">
                    {courseInfoList.departmentOrFaculty}
                  </a>
                </h2>
             </c:if>
            </c:if>
          </c:if>
          <h2 class="mt10 fnt_lbd" id="cdInstitutionName">
            <a class="fnt_lbd fnt_18 lh_28 link_blue" href="${courseInfoList.uniHomeUrl}?clearing">${courseInfoList.collegeNameDisplay}<%--Commented by prabha on 15-DEC-2015 <logic:notEmpty name="courseInfoList" property="collegeLocation">, <bean:write name="courseInfoList" property="collegeLocation"/></logic:notEmpty>--%></a>
                                            
          </h2>                             
          <c:if test="${not empty courseInfoList.overAllRating}">
            <c:if test="${courseInfoList.overAllRating gt 1}"> 
              <h3 id="cdRating" class="fnt_lrg fnt_14 lh_24" style="display: block;">Student rating <span class="ml5 rat${courseInfoList.overAllRating} t_tip"> 
							  <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								<span class="cmp">
									<div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats"/></div>
									<div class="line"></div>
								</span>
								<%--End of tool tip code--%> 
							</span>(${courseInfoList.overAllRatingExact})
				<c:if test="${not empty courseInfoList.reviewCount}">	
			       <c:if test="${courseInfoList.reviewCount gt 0}">		
                     <a class="link_blue" href="${courseInfoList.reviewURL}">
                          ${courseInfoList.reviewCount}
                          <c:if test="${courseInfoList.reviewCount gt 1}"> 
                             reviews
                         </c:if>
                         <c:if test="${courseInfoList.reviewCount eq 1}">
                             review
                         </c:if>
                    </a>
                  </c:if>
                </c:if>
              </h3>
            </c:if>
          </c:if>
          <c:if test="${not empty courseInfoList.addedToFinalChoice}">	
            <c:if test="${courseInfoList.addedToFinalChoice eq 'N'}">
              <div class="myFnChLbl" style="display:none;">
               ${courseInfoList.collegeNameDisplay}#${courseInfoList.collegeId}##${courseInfoList.courseTitle}#${courseInfoList.courseId}
              </div>               
            </c:if>
         </c:if>
        </c:forEach> 
      </c:if>
    <div class="fr mt5 mb20 btn_intr">
        <div class="more_info">      
              <a>Apply</a>
         </div>
         <div class="btns_interaction fl clr_btn" id="buttonCls">
            <div class="int_cl">
                  <a class="" onclick="hm()"><i class="fa fa-times"></i></a>
                </div>
              </div>
              
      <div class="fright">
          <c:if test="${not empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
            <c:forEach var="buttonDetails" items="${requestScope.listOfCollegeInteractionDetailsWithMedia}" varStatus="row"> 
              <c:set var="rowValue" value="${row.index}"/>
              <c:if test="${not empty buttonDetails.subOrderItemId}">
                 <c:if test="${buttonDetails.subOrderItemId gt 0}">
                    <c:set var="subOrderId1" value="${buttonDetails.subOrderItemId}"/>
                    <%
                      applySubOrderItemId = pageContext.getAttribute("subOrderId1").toString();
                      String webformPrice = new WUI.utilities.CommonFunction().getGACPEPrice((String)pageContext.getAttribute("subOrderId1"), "webform_price");
                      String websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice((String)pageContext.getAttribute("subOrderId1"), "website_price");
                      request.setAttribute("richProfileSubOrderId",(String)pageContext.getAttribute("subOrderId1"));
                      request.setAttribute("advWebsitePrice",websitePrice);
                    %>
                       <c:if test="${not empty buttonDetails.subOrderWebsite}">
                        <a rel="nofollow" 
                          target="_blank" 
                          class="visit-web" 
                          onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=websitePrice%>); cpeWebClickClearingWithCourse(this,'<%=wp_collegeId%>','${courseInfoList.courseId}','${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.subOrderWebsite}');var a='s.tl(';" 
                          href="/degrees/visitwebredirect.html?id=${buttonDetails.subOrderItemId}"
                          title="Visit <%=wp_collegeNameDisplay%> website">
                          Visit website
                        </a>
                      </c:if>
                      <c:if test="${not empty buttonDetails.hotLineNo}">
                        <a id="contact_<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())%>" title="${buttonDetails.hotLineNo} " class="clr-call mr17" onclick="setTimeout(function(){location.href='tel:${buttonDetails.hotLineNo}'},1000); GAInteractionEventTracking('Webclick', 'interaction', 'hotline', '<%=gaCollegeName%>');cpeHotlineClearing(this,'<%=wp_collegeId%>','${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.hotLineNo}');" >
                           <i class="fa fa-phone" aria-hidden="true"></i>  CALL NOW
                        </a>
                      </c:if>
                  </c:if>
                </c:if>
              </c:forEach>
              <%if("Y".equalsIgnoreCase(applyNowFlag)){%>
                <a onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Apply Now', '<%=gaCollegeName%>');wugoApplyNow('<%=wp_collegeId%>', '<c:out value="${courseInfoList.courseId}" escapeXml="false" />', '<%=applySubOrderItemId%>')" id="applyNowPod" class="clr-ap-nw" title="GO APPLY ONLINE" ><span>GO</span>Apply ONLINE</a> 
              <%}%>
            </c:if>          
          
      </div>
    </div>
    
    <input type="hidden" name="comparepage" id="comparepage" value="CLEARING"/>
    
<%}else{%>
  <c:if test="${not empty requestScope.courseInfoList}">
    <c:forEach var="courseInfoList" items="${requestScope.courseInfoList}"> 
      <h1 id="cdCourseTitle" class="cf fnt_lbd fnt_24 lh_28 cl_grylt txt_lft txt_normal">
       ${courseInfoList.courseTitle}</h1>
       <c:if test="${not empty courseInfoList.departmentOrFaculty}">
         <c:if test="${not empty requestScope.profileType}">
           <c:if test="${request.profileType eq 'SP'}">
            <h2 class="cf" id="cdSpName">
              <a class="fl fnt_lbd fnt_18 lh_28 link_blue txt_upr" href="${courseInfoList.profileURL}">${courseInfoList.departmentOrFaculty}</a>
            </h2>
          </c:if>
        </c:if>
      </c:if>
      <h2 class="mt10 fnt_lbd" id="cdInstitutionName">
        <a class="fnt_lbd fnt_18 lh_28 link_blue" onclick="sponsoredListGALogging(${interactionCollegeId});" href="${courseInfoList.uniHomeUrl}">${courseInfoList.collegeNameDisplay}<%--Commented by prabha on 15-DEC-2015 <logic:notEmpty name="courseInfoList" property="collegeLocation">, <bean:write name="courseInfoList" property="collegeLocation"/></logic:notEmpty>--%></a>
      </h2>
      <c:if test="${not empty courseInfoList.overAllRating}">
        <c:if test="${courseInfoList.overAllRating gt 1}">
          <h3 id="cdRating" class="fnt_lrg fnt_14 lh_24">Student rating <span class="ml5 rat${courseInfoList.overAllRating} t_tip">
					 <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
					 <span class="cmp">
						 <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats"/></div>
						 <div class="line"></div>
					</span>
					<%--End of tool tip code--%>
					</span>(${courseInfoList.overAllRatingExact})
			<c:if test="${not empty courseInfoList.reviewCount}">
			   <c:if test="${courseInfoList.reviewCount gt 0}">
			      <c:if test="${not empty requestScope.getReviewList}">
                    <a class="link_blue vr_bold" id="rvw_skip_link" data-rvw-ga="${courseInfoList.collegeNameDisplay}">
                      <spring:message code="view.review.link.name"/>
<%--                   ${courseInfoList.reviewCount}
                  <c:if test="${courseInfoList.reviewCount gt 1}">
                  reviews
                  </c:if>
                   <c:if test="${courseInfoList.reviewCount eq 1}">
                  review
                  </c:if>
 --%>                </a>
                </c:if>
             </c:if>
            </c:if>
          </h3>
        </c:if>
      </c:if>
       <c:if test="${not empty courseInfoList.addedToFinalChoice}">
          <c:if test="${courseInfoList.addedToFinalChoice eq 'N'}">
               <div class="myFnChLbl" style="display:none;">                    
                ${courseInfoList.collegeNameDisplay}#${courseInfoList.collegeId}##${courseInfoList.courseTitle}#${courseInfoList.courseId}
              </div>               
            </c:if>
          </c:if>
    </c:forEach>
  </c:if>
          
  <div class="hor_cmp">
  <c:if test="${not empty requestScope.courseInfoList}">
    <c:forEach var="courseInfoList" items="${requestScope.courseInfoList}">
       <c:if test="${not empty courseInfoList.wasThisCourseShortlisted}">
          <c:if test="${courseInfoList.wasThisCourseShortlisted eq 'TRUE'}">
            <div class="cmlst" id="basket_div_${courseInfoList.courseId}" style="display:none;"> 
              <div class="compare">
                <a onclick='addBasket("${courseInfoList.courseId}", "O", this,"basket_div_${courseInfoList.courseId}", "basket_pop_div_${courseInfoList.courseId}", "", "", "<%=gaCollegeName%>");'>
                  <span class="icon_cmp f5_hrt"></span>
                  <span class="cmp_txt">Compare</span>
                  <span class="loading_icon" id="load_${courseInfoList.courseId}" style="display:none;position:absolute;"></span>
                  <span class="chk_cmp"><span class="chktxt">Add to comparison<em></em></span></span>
                </a>
              </div>
            </div>
            <div class="cmlst act" id="basket_pop_div_${courseInfoList.courseId}">
            <div class="compare">
              <a class="act" onclick='addBasket("${courseInfoList.courseId}", "O", this,"basket_div_${courseInfoList.courseId}", "basket_pop_div_${courseInfoList.courseId}");'>
                <span class="icon_cmp f5_hrt hrt_act"></span>
                <span class="cmp_txt">Compare</span>
                <span class="loading_icon" id="load1_${courseInfoList.courseId}" style="display:none;position:absolute;"></span>
                <span class="chk_cmp"><span class="chktxt">Remove from comparison<em></em></span></span>
              </a>
            </div>
            <div id="defaultpop_${courseInfoList.courseId}" class="sta" style="display: block;">
              <%
                String userId = sessionData.getData(request, "y");
                if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
              %>
                <a class="view_more" href="/degrees/comparison">View comparison</a>
              <%}else{%>
                <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
              <%}%>
            </div>
            <div id="pop_${courseInfoList.courseId}" class="sta" style="display:none;"></div>
          </div>
        </c:if>
           <c:if test="${courseInfoList.wasThisCourseShortlisted eq 'FALSE'}">
            <div class="cmlst" id="basket_div_${courseInfoList.courseId}">
              <div class="compare">
                <a onclick='addBasket("${courseInfoList.courseId}", "O", this,"basket_div_${courseInfoList.courseId}", "basket_pop_div_${courseInfoList.courseId}", "", "", "<%=gaCollegeName%>");'>
                  <span class="icon_cmp f5_hrt"></span>
                  <span class="cmp_txt">Compare</span>
                  <span class="loading_icon" id="load_${courseInfoList.courseId}" style="display:none;position:absolute;"></span>
                  <span class="chk_cmp">
                  <span class="chktxt">Add to comparison<em></em></span></span>
                </a>
              </div>
            </div>
            <div class="cmlst act" id="basket_pop_div_${courseInfoList.courseId}" style="display:none;">
              <div class="compare">
                <a class="act" onclick='addBasket("${courseInfoList.courseId}", "O", this,"basket_div_${courseInfoList.courseId}", "basket_pop_div_${courseInfoList.courseId}");'>
                  <span class="icon_cmp f5_hrt hrt_act"></span>
                  <span class="cmp_txt">Compare</span>
                  <span class="loading_icon" id="load1_${courseInfoList.courseId}" style="display:none;position:absolute;"></span>
                  <span class="chk_cmp">
                  <span class="chktxt">Remove from comparison<em></em></span></span>
                </a>
              </div>
              <div id="defaultpop_${courseInfoList.courseId}" class="sta" style="display: block;">
                <%
                  String userId = sessionData.getData(request, "y");
                  if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
                %>
                  <a class="view_more" href="/degrees/comparison">View comparison</a>
                <%}else{%>
                  <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
                <%}%>
              </div>
              <div id="pop_${courseInfoList.courseId}" class="sta"></div>
            </div>
          </c:if>
        </c:if>
      </c:forEach>
    </c:if>
  </div>          
  <!--Modified code for sticky buttons Jan-27-16 Rel Indumathi.S-->        
  <div class="fr mt5 mb20 btn_intr">
    <div class="more_info">
		  <a>More Info</a>
		</div>
    <div class="btns_interaction fl" id="buttonCls">
      <div class="int_cl">
		  	<a class="" onclick="hm()"><i class="fa fa-times"></i></a>
		  </div>
      <jsp:include page="/advertpromotion/interactionbuttons/websiteProspectusFromBeanAndHrefCdPage.jsp" >
        <jsp:param name="FLOAT_STYLE" value="RIGHT" />
        <jsp:param name="courseId" value="<%=paramCourseId%>"/>
        <jsp:param name="FROMPAGE" value="SHOWNONREQINFO" />
      </jsp:include>
    </div>
  </div>
            
<%}%>
<input type="hidden" id="check_mobile_hidden" value="<%=mobileFlag%>"/> 
<input type="hidden" id="applyNowFlag" value="<%=applyNowFlag%>"/>
<input type="hidden" id="oppId" value="<%=oppId%>" />
