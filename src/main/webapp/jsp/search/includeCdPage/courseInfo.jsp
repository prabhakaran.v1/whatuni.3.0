<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.GlobalConstants, WUI.utilities.SessionData, WUI.utilities.CommonUtil"%>
<%
  String paramCourseId = request.getParameter("w") !=null && request.getParameter("w").trim().length()>0 ? request.getParameter("w") : request.getParameter("cid") !=null && request.getParameter("cid").trim().length()>0 ? request.getParameter("cid") : ""; 
  paramCourseId = paramCourseId!=null && paramCourseId.trim().length()>0 ? "" : String.valueOf(request.getAttribute("seoCourseId"));
  String collegeId = String.valueOf(request.getAttribute("seoCollegeId"));
  String cdOpportunityId =  (String)request.getAttribute("cdOpportunityId");
         cdOpportunityId  = cdOpportunityId !=null &&  cdOpportunityId.trim().length()>0 ?  cdOpportunityId : "";
  String cdOpportunityText = request.getAttribute("cdOpportunityText") != null ? (String)request.getAttribute("cdOpportunityText") : "";
  if(cdOpportunityText.length() > 100) {//Added by Indumathi.S Apr_19_2016 
    cdOpportunityText = cdOpportunityText.substring(0,99);
    cdOpportunityText += cdOpportunityText.lastIndexOf("..") == -1 ? ".." : "";
  }
   boolean isClearingCourse = (((String)request.getAttribute("CLEARING_COURSE")) != null && "TRUE".equals((String)request.getAttribute("CLEARING_COURSE"))) ? true : false;
%>
<c:if test="${COVID19_SYSVAR eq 'ON'}">
  <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
  <div class="sr">
  <c:choose>
    <c:when test="${CLEARING_CD_PAGE eq 'CLEARING_CD_PAGE'}">
       <jsp:include page="/jsp/clearing/coursedetails/include/covidSection.jsp"/>
    </c:when>
    <c:otherwise>
    <div class="covid_upt">
	  <span class="covid_txt"><%=new SessionData().getData(request, "COVID19_COURSE_TEXT")%></span>
	  <a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_blu_arw.svg", 0)%>"></a>
    </div>
    </c:otherwise>
    </c:choose>
  </div>
</c:if>
<c:if test="${not empty requestScope.courseInfoList}">
<section class="course_info pt0 pb0 clrcnge_crseinfo">
 <div id="div_course_info"></div>
  <c:forEach var="courseInfoList" items="${requestScope.courseInfoList}"> 
      <h2 class="sub_tit fl fnt_lrg pb10">Course info</h2>
      <c:if test="${not empty courseInfoList.ucasCode}">
        <div class="fr right_link1 cl_grylt">                   
          <span class="fnt_14  fl pr5">UCAS CODE</span>
          <span class="fnt_lbd fnt_18"> ${courseInfoList.ucasCode}</span>
        </div>
      </c:if>
      <div id="shortDesc" class="fl fnt_lrg fnt_14 lh_24 cl_grylt" style="display:block;">
      <c:if test="${not empty courseInfoList.courseSummary}">
          ${courseInfoList.courseSummary}
        <c:if test="${not empty courseInfoList.courseSummaryFull}">...
           <a class="fnt_lbk fnt_14 link_blue" onclick="javascript:showHideDivText('shortDesc','fullDesc');">READ MORE</a>
        </c:if>
       </c:if>
      </div>
      <c:if test="${not empty courseInfoList.courseSummaryFull}">
        <div id="fullDesc" class="fl fnt_lrg fnt_14 lh_24 cl_grylt" style="display:none;"> 
             ${courseInfoList.courseSummaryFull}                   
      </div>
      </c:if>
      <c:if test="${not empty requestScope.oppDetailList}">
        <c:set var="oppDetailListSize" value="${fn:length(requestScope.oppDetailList)}"/>
       <!--  <bean:size id="oppDetailListSize" name="oppDetailList" scope="request"/> -->  
        <c:if test="${oppDetailListSize gt 1}">
        <h3 class="fnt_lbd fnt_18 cl_grylt fl w100p mt20">Different course options</h3>
      <fieldset class="w50p fl mt20 cl_grylt">
        <fieldset class="w100p fl">
        </fieldset>
        <div class="select dis_edu">
          <span id="timeSelected" class="fnt_lbd">
          <c:if test="${not empty requestScope.cdOpportunityText}">
              <%=cdOpportunityText%>
          </c:if>
          </span>
          <i class="fa fa-angle-down fa-lg"></i>
        </div>       
        <select id="time" class="dis_edu" onchange="setSelectedoptionText(this,'timeSelected');oppOnChange('<%=collegeId%>','<%=paramCourseId%>',this);" name="time">        
            <c:forEach var="oppDetailList" items="${requestScope.oppDetailList}">
             <c:set var="currOppId" value="${oppDetailList.opportunityId}"/>
             <c:set var="currOppDesc" value="${oppDetailList.opportunityDesc}"/>
            <%
            String currOppDesc = (String)pageContext.getAttribute("currOppDesc");
            String currOppId = (String)pageContext.getAttribute("currOppId");
             if(currOppDesc.length() > 100) {//Added by Indumathi.S Apr_19_2016
               currOppDesc = currOppDesc.substring(0,99);
               currOppDesc += currOppDesc.lastIndexOf("..") == -1 ? ".." : "";
              }%>
            <option value="${oppDetailList.opportunityId}"<%if(cdOpportunityId.equalsIgnoreCase(currOppId)){%> selected="selected"<%}%> ><%=currOppDesc%></option>        
          </c:forEach>
        </select>        
      </fieldset>
      </c:if>
      <input type="hidden" name="opportunityId" id="opportunityId" value="<%=cdOpportunityId%>" />
     </c:if>  
      </c:forEach> 
    </section>
</c:if>
<%-- Commented the code for JIRA 199
 <c:if test="${not empty requestScope.partTimeMsgFlag}">
  <c:if test="${requestScope.partTimeMsgFlag eq 'Y'}">
    <p class="cal_txt fdtxt_wrp"><span class="fdeg_txt" ><spring:message code="wuni.ucas.part.time.course.message"/></span></p>
  </c:if>
 </c:if>
--%>
 