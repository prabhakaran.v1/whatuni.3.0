<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonFunction"%>

<% String ucasDesc = request.getAttribute("ucasdesc") != null && request.getAttribute("ucasdesc").toString().trim().length() > 0 ? (String)request.getAttribute("ucasdesc") : "";
   String ucasPoints = request.getAttribute("ucastariff") != null && request.getAttribute("ucastariff").toString().trim().length() > 0 ? (String)request.getAttribute("ucastariff") : "";
   String wp_collegeId = request.getAttribute("interactionCollegeId") != null && request.getAttribute("interactionCollegeId").toString().trim().length() > 0 ? (String)request.getAttribute("interactionCollegeId") : "0";
   String wp_collegeNameDisplay = request.getAttribute("interactionCollegeNameDisplay") != null && request.getAttribute("interactionCollegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("interactionCollegeNameDisplay") : "";
   String ucasFlag = (String)request.getAttribute("ucasflag");
   String ent_collegeName = (String) request.getAttribute("collegeName");
   String subOrderItemId = request.getAttribute("subOrderItemId") != null && request.getAttribute("subOrderItemId").toString().trim().length() > 0 ? (String)request.getAttribute("subOrderItemId") : "0";
   CommonFunction commonFunObject = new WUI.utilities.CommonFunction();
   String webformPrice = commonFunObject.getGACPEPrice(subOrderItemId, "webform_price");
   String websitePrice = commonFunObject.getGACPEPrice(subOrderItemId, "website_price");
   String gaCollegeName = commonFunObject.replaceSpecialCharacter(ent_collegeName);   
   String paramCourseId = request.getParameter("w") !=null && request.getParameter("w").trim().length()>0 ? request.getParameter("w") : request.getParameter("cid") !=null && request.getParameter("cid").trim().length()>0 ? request.getParameter("cid") : ""; 
   paramCourseId = paramCourseId!=null && paramCourseId.trim().length()>0 ? "" : String.valueOf(request.getAttribute("seoCourseId"));
   String searchType = request.getParameter("searchtype");
   String entryDesc = (String)request.getAttribute("entryDesc");
    
%>     
<%--Entry requirment structure change by Hema.S on 15.5.2018--%>
<c:if test="${not empty requestScope.ucasCourseDetailList}">
  <section class="clrcnge_entryreq p0">
    <div class="clear pt40">
      <div id="div_entry_req"></div>
      <div id="entryrequirments"></div>
      <h2 class="sub_tit descH fl">
        <span class="fnt_lrg fnt_24 lh_24 cl_grylt">Entry requirements</span>        
        <span class="tool_tip">
          <i class="fa fa-question-circle fnt_24">
            <span class="cmp fnt_lbd">
              <div class="fnt_lbd fnt_14 lh_24">Entry requirements</div>
              <div>The minimum grades (and in some cases the subjects) you'll need to get on to this course.</div>
              <div class="line"></div>
            </span>
          </i>
        </span>
      </h2>
      <div class="fr right_link">
        <%--Added course type flag and update date by Thiyagu G.S for 08_MAR_2016--%>
        DATA SOURCE: <a class="cl_blult" rel="nofollow" href="/degrees/jsp/search/kisdataStatic.jsp"><c:if test="${not empty requestScope.courseTypeText}">${requestScope.courseTypeText}</c:if></a>
      </div>
      <%if(!"".equalsIgnoreCase(ucasPoints)){%>
         <%if(!"".equalsIgnoreCase(ucasDesc) && "Tariff Points".equalsIgnoreCase(ucasDesc)){%>
           <h2 class="fnt_18 clear fnt_lrg lh_24 cl_grylt">(Typical UCAS points <%=ucasPoints%>)</h2>
         <%}else{%>
           <h2 class="fnt_18 clear fnt_lrg lh_24 cl_grylt">Typical grades  <%=ucasPoints%></h2>
      <%}}%>
    </div>
    <table class="ent_req table_view1 fnt_14" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr>
          <th>Choose Exam type</th>
          <th> Grades / Points required</th>
        </tr>
        <tr>                 
          <td>
            <fieldset class="set_drop">
              <div class="select dis_edu">
                <span id="selectedspan" class="fnt_lbd">
                <%=entryDesc%>
                </span>
                <i class="fa fa-angle-down fa-lg"></i>  
              </div>
              <select id="dropdownId" class="dis_edu" name="entryRequiremnts" onchange="setSelectedValEntryReq(this, 'selectedspan');">
                <c:forEach var="entryPointRequirmentsUcas" items="${requestScope.ucasCourseDetailList}" varStatus="index" >
                <c:set var="indexValue" value="${index.index}"/>
                  <option value="${entryPointRequirmentsUcas.entryDesc}" id="${indexValue}">${entryPointRequirmentsUcas.entryDesc}</option>
                </c:forEach>
              </select>
            </fieldset>
          </td>  
          <c:forEach var="entryPointRequirmentsUcas" items="${requestScope.ucasCourseDetailList}" varStatus="i" >   
          <c:set var="indexIValue" value="${i.index}"/>   
            <td id="entryPoints_${indexIValue}" style="display:none" class="entryClass">   
              <span class="ent_grdpts">Grades / Points required </span>
              <c:if test="${entryPointRequirmentsUcas.entryPoints ne 'contact university'}">
                ${entryPointRequirmentsUcas.entryPoints}      
              </c:if>
              <c:if test="${entryPointRequirmentsUcas.entryPoints eq 'contact university'}">
                <c:if test="${not empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
                  <c:if test="${not empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
                    <c:forEach var="buttonDetails" items="${requestScope.listOfCollegeInteractionDetailsWithMedia}">
                      <c:if test="${not empty buttonDetails.subOrderItemId}">                                 
                        <c:if test="${buttonDetails.subOrderItemId gt 0}">
                          <%if(("CLEARING_COURSE").equals(searchType)){%>    
                          <c:if test="${not empty buttonDetails.subOrderWebsite}"> 
                            <spring:message code="wuni.entry.requirments.contact.university.text"/>
                            <a rel="nofollow" 
                              target="_blank" 
                              class="fnt_lrg  link_blue" 
                              onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=websitePrice%>); cpeWebClickClearing(this,'<%=wp_collegeId%>','${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.subOrderWebsite}');" 
                              href="${buttonDetails.subOrderWebsite}" 
                              title="Visit ${buttonDetails.collegeNameDisplay} website">${entryPointRequirmentsUcas.entryPoints} </a> <spring:message code="wuni.entry.requirments.contact.university.update.text"/>
                            </c:if>
                           <%}else{%>
                            <c:if test="${not empty buttonDetails.subOrderEmailWebform}">
                              <spring:message code="wuni.entry.requirments.contact.university.text"/> <a rel="nofollow" 
                                target="_blank" 
                                class="fnt_lrg  link_blue" 
                                onclick="GAInteractionEventTracking('emailwebform', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=webformPrice%>); cpeEmailWebformClick(this,'<%=wp_collegeId%>','${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.subOrderEmailWebform}'); var a='s.tl(';" 
                                href="${buttonDetails.subOrderEmailWebform}" 
                                title="Email <%=wp_collegeNameDisplay%>">${entryPointRequirmentsUcas.entryPoints} </a> <spring:message code="wuni.entry.requirments.contact.university.update.text"/>
                            </c:if>
                            <c:if test="${empty buttonDetails.subOrderEmailWebform}">
                              <spring:message code="wuni.entry.requirments.contact.university.text"/> <a rel="nofollow"
                                class="fnt_lrg  link_blue" 
                                onclick="GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '<%=gaCollegeName%>');"
                                href="<SEO:SEOURL pageTitle="sendcollegemail" ><%=ent_collegeName%>#<%=wp_collegeId%>#<%=paramCourseId%>#0#n#${buttonDetails.subOrderItemId}</SEO:SEOURL>"                           
                                title="Email <%=wp_collegeNameDisplay%>" >${entryPointRequirmentsUcas.entryPoints}</a> <spring:message code="wuni.entry.requirments.contact.university.update.text"/>   
                            </c:if>                                         
                            <%}%>
                          </c:if>
                        </c:if>
                        <c:if test="${empty buttonDetails.subOrderItemId}">
                          <spring:message code="wuni.entry.requirments.contact.university.text"/> ${entryPointRequirmentsUcas.entryPoints} <spring:message code="wuni.entry.requirments.contact.university.update.text"/>
                        </c:if>
                      </c:forEach>
                    </c:if>
                  </c:if>
                  <c:if test="${empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
                    <spring:message code="wuni.entry.requirments.contact.university.text"/> ${entryPointRequirmentsUcas.entryPoints} <spring:message code="wuni.entry.requirments.contact.university.update.text"/>
                  </c:if> 
                </c:if>
             </td>             
           </c:forEach>         
        </tr>
        <tr>    
          <th colspan="2">Details</th>  
        </tr>
        <tr>
          <c:forEach var="entryPointRequirmentsUcas" items="${requestScope.ucasCourseDetailList}" varStatus="j" >
          <c:set var="indexJValue" value="${j.index}"/>
            <td colspan="2" id="additionalInfo_${indexJValue}" style="display:none" class="addtionalInfoClass">  
              <c:if test="${entryPointRequirmentsUcas.additionalInfo ne 'contact university'}"> 
                ${entryPointRequirmentsUcas.additionalInfo}  
              </c:if>
              <c:if test="${entryPointRequirmentsUcas.additionalInfo eq 'contact university'}">
                <c:if test="${not empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
                  <c:if test="${not empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
                    <c:forEach var="buttonDetails" items="${requestScope.listOfCollegeInteractionDetailsWithMedia}" >
                      <c:if test="${not empty buttonDetails.subOrderItemId}">                           
                        <c:if test="${buttonDetails.subOrderItemId gt 0}">
                          <%if(("CLEARING_COURSE").equals(searchType)){%>   
                            <c:if test="${not empty buttonDetails.subOrderWebsite}"> 
                              <spring:message code="wuni.entry.requirments.contact.university.text"/>  <a rel="nofollow" 
                              target="_blank" 
                              class="fnt_lrg  link_blue" 
                              onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=websitePrice%>); cpeWebClickClearing(this,'<%=wp_collegeId%>','${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.subOrderWebsite}');" 
                              href="${buttonDetails.subOrderWebsite}" 
                              title="Visit ${buttonDetails.collegeNameDisplay} website">${entryPointRequirmentsUcas.additionalInfo} </a> <spring:message code="wuni.entry.requirments.contact.university.update.text"/>
                            </c:if>
                          <%}else{%>
                               <c:if test="${not empty buttonDetails.subOrderEmailWebform}">
                                 <spring:message code="wuni.entry.requirments.contact.university.text"/> <a rel="nofollow" 
                                 target="_blank" 
                                 class="fnt_lrg  link_blue" 
                                 onclick="GAInteractionEventTracking('emailwebform', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=webformPrice%>); cpeEmailWebformClick(this,'<%=wp_collegeId%>','${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.subOrderEmailWebform}'); var a='s.tl(';" 
                                 href="${buttonDetails.subOrderEmailWebform}" 
                                 title="Email <%=wp_collegeNameDisplay%>">${entryPointRequirmentsUcas.additionalInfo}</a> <spring:message code="wuni.entry.requirments.contact.university.update.text"/>
                                </c:if>
                                <c:if test="${empty buttonDetails.subOrderEmailWebform}">
                                  <spring:message code="wuni.entry.requirments.contact.university.text"/> <a rel="nofollow"
                                  class="fnt_lrg  link_blue"                           
                                  onclick="GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '<%=gaCollegeName%>');"                
                                  href="<SEO:SEOURL pageTitle="sendcollegemail" ><%=ent_collegeName%>#<%=wp_collegeId%>#<%=paramCourseId%>#0#n#${buttonDetails.subOrderItemId}</SEO:SEOURL>" 
                                  title="Email <%=wp_collegeNameDisplay%>" >${entryPointRequirmentsUcas.additionalInfo}</a> <spring:message code="wuni.entry.requirments.contact.university.update.text"/>
                                </c:if>
                               <%}%>
                             </c:if>
                           </c:if>
                           <c:if test="${empty buttonDetails.subOrderItemId}">
                             <spring:message code="wuni.entry.requirments.contact.university.text"/> ${entryPointRequirmentsUcas.additionalInfo} <spring:message code="wuni.entry.requirments.contact.university.update.text"/>
                           </c:if>
                         </c:forEach>
                       </c:if>
                     </c:if>
                     <c:if test="${empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
                       <spring:message code="wuni.entry.requirments.contact.university.text"/> ${entryPointRequirmentsUcas.additionalInfo} for up to date information.
                     </c:if>
                   </c:if>
                 </td>
               </c:forEach>
             </tr>
           </tbody>
        </table>
     </section>
  <div class="borderbot mt35 fl"></div>
</c:if>