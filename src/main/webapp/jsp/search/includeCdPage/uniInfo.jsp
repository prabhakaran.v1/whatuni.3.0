<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import=" WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" %>
<%
  String ent_collegeName = (String) request.getAttribute("collegeName");
  String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(ent_collegeName);   
  String paramCollegeId = request.getParameter("z") !=null && request.getParameter("z").trim().length()>0 ? request.getParameter("z") : "";
  paramCollegeId = paramCollegeId!=null && paramCollegeId.trim().length()>0 ? "" : String.valueOf(request.getAttribute("seoCollegeId"));
  String courseType = request.getParameter("courseType");
  String isClearingCourse = (String)request.getAttribute("CLEARING_COURSE");
  CommonFunction common = new CommonFunction();
  String secClass = "univer_info";
  String medDiv1cls = "fl mt10 mob_100p";
  String medDiv2cls = "";
  String clrDescls = "";
  String uniInfoDivcls = "uni_info_right fl opd_univinfo";
  String info1clsTit = "fnt_lrg fnt_14 lh_30 cl_grylt";
  String info1clsVal = "fnt_lrg fnt_30 lh_30 cl_grylt mb20";
  String info2clsTit = "fnt_lrg fnt_14 lh_14 cl_grylt";
  String info2clsVal = "fnt_lrg fnt_14 lh_14 cl_grylt";
  if("TRUE".equals(isClearingCourse)){
    secClass = "univer_info clruni_info";
    medDiv1cls = "fl mt10 mob_univer_info";
    medDiv2cls = "rich_uni_info";
    uniInfoDivcls = "uni_info_right fr";
    info1clsTit = "tit";
    info1clsVal = "tval";
    info2clsTit = "tit";
    info2clsVal = "sval";
    clrDescls = "uniinfo_desc";
  }
%> 
<c:if test="${not empty requestScope.uniInfoList}">
<section class="<%=secClass%>">
  <div id="div_uni_info"></div>
  <c:forEach var="uniInfoList" items="${requestScope.uniInfoList}"> 
   <div class="fl w575">
   <%if("TRUE".equals(isClearingCourse)){%>
   <h2 class="fnt_18 lh_28 cl_blult">Clearing at <c:out value="${uniInfoList.collegeNameDisplay}" /></h2> 
   <%}else {%>
     <h2 class="sub_tit fnt_lrg fl txt_lft w100p">Uni info</h2>
    <%}%>
     <c:if test="${not empty uniInfoList.collegeNameDisplay}">
     <%if(!"TRUE".equals(isClearingCourse)){%>
       <h2 class="fnt_18 lh_28 cl_blult pt50">
         <a href="<SEO:SEOURL pageTitle="unilanding">${uniInfoList.collegeName}<spring:message code="wuni.seo.url.split.character" />${uniInfoList.collegeId}</SEO:SEOURL>">${uniInfoList.collegeNameDisplay}<c:if test="${not empty uniInfoList.collegeLocation}">, ${uniInfoList.collegeLocation}</c:if></a>
       </h2>
      <%}%> 
     </c:if>
    </div>
    <c:if test="${not empty uniInfoList.awardImagePath}">
      <div class="fr univ_logo" style="margin-top: 14px;">
      <c:set var="badgeTitle" value="${uniInfoList.awardImagePath}"/>
      <%
       String badgeTitle = (String)pageContext.getAttribute("badgeTitle");
       String badgename[] = badgeTitle.split("##SPLIT##");
       for(int i=0;i<badgename.length;i++){%>        
            <img src="<%=CommonUtil.getImgPath(badgename[i], 0)%>" alt="Award image">                       
      <%}%>     
      </div>  
    </c:if>
    
    <c:if test="${empty requestScope.isRichProfile}">
    <div class="<%=medDiv1cls%>">
     <div class="box_cnr <%=medDiv2cls%>">
        <div class="fl img_ppn_490">
          <c:if test="${uniInfoList.videoUrl eq ''}">
            <c:if test="${uniInfoList.imagePath ne ''}">
            <c:set var="clearingImagePath"  value="${uniInfoList.imagePath}"/>  
            <%
              String deviceClearingimagePath = (pageContext.getAttribute("clearingImagePath").toString().indexOf("/CH/") > -1) ? pageContext.getAttribute("clearingImagePath").toString().replace("." ,"_768px.") : (pageContext.getAttribute("clearingImagePath").toString().indexOf("/rich/") > -1) ? pageContext.getAttribute("clearingImagePath").toString().replace("." ,"_992px.") : pageContext.getAttribute("clearingImagePath").toString();
              String mobileClearingImagePath = (pageContext.getAttribute("clearingImagePath").toString().indexOf("/CH/") > -1) ? pageContext.getAttribute("clearingImagePath").toString().replace("." ,"_320px.") : (pageContext.getAttribute("clearingImagePath").toString().indexOf("/rich/") > -1) ? pageContext.getAttribute("clearingImagePath").toString().replace("." ,"_480px.") : pageContext.getAttribute("clearingImagePath").toString().replace("." ,"_353px.");
            %>
              <c:if test="${uniInfoList.mediaTypeId eq '18'}">
                <img width="490" height="270" alt="${uniInfoList.collegeNameDisplay}" class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=CommonUtil.getImgPath("",0)%><%=deviceClearingimagePath%>" data-src-mobile="<%=CommonUtil.getImgPath("",0)%><%=mobileClearingImagePath%>"/>
              </c:if>
              <c:if test="${uniInfoList.mediaTypeId ne '18'}">
                 <img alt="${uniInfoList.collegeNameDisplay}" class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=CommonUtil.getImgPath("",0)%><%=deviceClearingimagePath%>" data-src-mobile="<%=CommonUtil.getImgPath("",0)%><%=mobileClearingImagePath%>"/>
              </c:if>
            </c:if>
          </c:if>
          <c:if test="${uniInfoList.videoUrl ne ''}">
            <%request.setAttribute("showlightbox","YES");%>
            <div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
              <c:if test="${not empty uniInfoList.videoMetaDuration}">
                <meta itemprop="duration" content="${uniInfoList.videoMetaDuration}" />
              </c:if><meta itemprop="thumbnailUrl" content="${uniInfoList.videoThumbnailUrl}" /><meta itemprop="embedURL" content="${uniInfoList.videoUrl}" />
              <a rel="nofollow" class="vhold" onclick="return cdPageVideoShow('${uniInfoList.videoId}','${uniInfoList.collegeId}','B')" title='${uniInfoList.collegeNameDisplay}'>
                <img width="490" height="270" src="${uniInfoList.videoThumbnailUrl}" alt="${uniInfoList.collegeNameDisplay}" title="" />
                <i id="img_std_choice_icon" class="fa fa-play fa-8x"></i>
              </a>
            </div>
          </c:if>
        </div>   
        <div class="<%=uniInfoDivcls%>">
        <c:if test="${not empty uniInfoList.timesRanking}">
        <%if("TRUE".equals(isClearingCourse)){%><div class="trank"><%}%>
            <div class="<%=info1clsTit%>">CompUniGuide ranking</div> <%--Changed ranking lable from Times to CUG for 08_MAR_2016 By Thiyagu G--%>
            <div class="<%=info1clsVal%>">${uniInfoList.timesRanking}</div>
            <%if("TRUE".equals(isClearingCourse)){%></div><%}%>
          </c:if>
          <c:if test="${not empty uniInfoList.overallRating}">
            <c:if test="${uniInfoList.overallRating gt 0}"> 
            <%if("TRUE".equals(isClearingCourse)){%><div class="srank"><%}%>
             <div class="<%=info2clsTit%>">Student rating</div>
              <div class="mt5">
               <span class="rat${uniInfoList.overallRating} t_tip">
							  <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								<span class="cmp">
								  <div class="hdf5"><<spring:message code="wuni.tooltip.overall.keystats" /></div>
									<div class="line"></div>
								</span>
								<%--End of rating tool tip code--%>
							</span>
              <span class="<%=info2clsVal%>">(${uniInfoList.overallRatingExact})</span>
            </div>
            <%if("TRUE".equals(isClearingCourse)){%></div><%}%>
           </c:if>
          </c:if>
          <%if(!"TRUE".equals(isClearingCourse)){%>
          <c:if test="${not empty uniInfoList.nextOpenDay}">
              <div class="fnt_lrg fnt_14 lh_14 cl_grylt mt20">Next open day</div>
              <div class="fnt_lrg fnt_18 lh_24 cl_blult mt5">
                <i class="fa fa-calendar"></i>
                <a href="${uniInfoList.openDayUrl}">${uniInfoList.nextOpenDay}</a>
              </div>
           </c:if>
           <%}%>
           <%
              String wps_collegeId = request.getAttribute("interactionCollegeId") != null && request.getAttribute("interactionCollegeId").toString().trim().length() > 0 ? (String)request.getAttribute("interactionCollegeId") : "0";
              String wp_collegeName = request.getAttribute("interactionCollegeName") != null && request.getAttribute("interactionCollegeName").toString().trim().length() > 0 ? (String)request.getAttribute("interactionCollegeName") : "0";
              String wps_collegeNameDisplay = request.getAttribute("interactionCollegeNameDisplay") != null && request.getAttribute("interactionCollegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("interactionCollegeNameDisplay") : "";
              String wp_floatStyle = request.getParameter("FLOAT_STYLE") != null && request.getParameter("FLOAT_STYLE").trim().length() > 0 ? request.getParameter("FLOAT_STYLE").trim() : "RIGHT";
              String coureId = request.getParameter("courseId") != null && request.getParameter("courseId").trim().length() > 0 ? request.getParameter("courseId") : "0";
              String gasCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(wp_collegeName);  
              //
            %>
            <c:if test="${not empty uniInfoList.subOrderItemId}">
             <%if("TRUE".equals(isClearingCourse)){%><div class="gepr"><%}%>
              <div class="btns_interaction fl">
                <c:if test="${uniInfoList.subOrderItemId gt 0}"> 
                  <c:set var="subOrderId1" value="${uniInfoList.subOrderItemId}"/>
                <%
                  String subOrderId1 = (String)pageContext.getAttribute("subOrderId1");
                  String webformPrices = new WUI.utilities.CommonFunction().getGACPEPrice(subOrderId1, "webform_price");
                  String websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice(subOrderId1, "website_price");
                %>
                 <%if("TRUE".equals(isClearingCourse)){%>
                    <c:if test="${not empty uniInfoList.subOrderWebsite}">
                        <a rel="nofollow" 
                        target="_blank" 
                        class="get-pros" 
                        href="${uniInfoList.clearingProfileURL}" 
                        title="View <%=wps_collegeNameDisplay%> Profile">View Profile</a>
                    </c:if>  
                
                <%}else{%>
                 <c:if test="${not empty uniInfoList.subOrderProspectusWebform}">
                  <a rel="nofollow" 
                      target="_blank" 
                      class="get-pros" 
                      onclick="GAInteractionEventTracking('prospectuswebform', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=webformPrices%>); cpeProspectusWebformClick(this,'<%=wps_collegeId%>','${uniInfoList.subOrderItemId}','${uniInfoList.cpeQualificationNetworkId}','${uniInfoList.subOrderProspectusWebform}'); var a='s.tl(';" 
                      href="${uniInfoList.subOrderProspectusWebform}" 
                      title="Get <%=wps_collegeNameDisplay%> Prospectus">
                      Get prospectus
                      <i class="fa fa-caret-right"></i>
                   </a>
                </c:if>
                 <c:if test="${not empty uniInfoList.subOrderProspectus}">
                   <c:if test="${not empty uniInfoList.subOrderProspectusWebform}">
                    <a  onclick="GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '<%=gaCollegeName%>'); return prospectusRedirect('/degrees','<%=wps_collegeId%>','<%=coureId%>','','','${uniInfoList.subOrderItemId}');"
                        class="get-pros"
                        title="Get <%=wps_collegeNameDisplay%> Prospectus">
                        Get prospectus
                        <i class="fa fa-caret-right"></i>
                    </a>   
                  </c:if>
                </c:if>              
              <%}%>
             </c:if> 
            </div>
            <%if("TRUE".equals(isClearingCourse)){%></div><%}%>
          </c:if>
         </div>      
     </div>
     <c:if test="${not empty uniInfoList.profileDesc}">
       <div class="fnt_lrg fnt_14 lh_24 cl_grylt mt15 fl <%=clrDescls%>">  
         <%if("TRUE".equals(isClearingCourse)){%>    
         <c:set var="profileText" value="${uniInfoList.profileDesc}"/>        
          <%if(Integer.parseInt(pageContext.getAttribute("profileText").toString()) >= 850) {
           //System.out.println(profileText.length()); 
          String profileText = pageContext.getAttribute("profileText").toString().substring(0,849);
           %>
             <h3><a href="${uniInfoList.clearingProfileURL}" title="View <%=wps_collegeNameDisplay%> Profile">${uniInfoList.collegeNameDisplay}</a></h3>
             <p id="lessDesc"><%=pageContext.getAttribute("profileText").toString()%>...</p>
             <p id="fullDesc" style="display:none">${uniInfoList.profileDesc}</p>
             <p class="cuinf_vmr"><a id="viewMoreUniInfoId" href="${uniInfoList.clearingProfileURL}">+ View more</a></p>
          <%} else { %>
             <p><c:out value="${uniInfoList.profileDesc}"/></p>
           <%}%>          
         <%}%>
         <%if(!("TRUE".equals(isClearingCourse))){%>
           ${uniInfoList.profileDesc}
           <%}%>
        </div>
     </c:if>
    </c:if>
    <c:if test="${not empty requestScope.isRichProfile}">
    <%String cntrClassName = "";%>
      <c:if test="${empty uniInfoList.imagePath}">
         <% cntrClassName = "uni_hght";%>
     </c:if>
    <div class="fl mt10 mob_univer_info">
      <div class="box_cnr rich_uni_info <%=cntrClassName%> ">
        <div class="fl img_ppn_490">
          <c:if test="${uniInfoList.imagePath ne ''}">
              <c:set var="richimagePath" value="${uniInfoList.imagePath}"/>
            <%
              String richimagePath = (String)pageContext.getAttribute("richimagePath");
              String deviceRichimagePath = (richimagePath.indexOf("/CH/") > -1) ? richimagePath.replace("." ,"_768px.") : richimagePath.replace("." ,"_992px.");
              String mobileRichImagePath = (richimagePath.indexOf("/CH/") > -1) ? richimagePath.replace("." ,"_320px.") : richimagePath.replace("." ,"_480px.");
            %>
            <img title="" alt="${uniInfoList.collegeNameDisplay}" class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" 
            data-src="<%=CommonUtil.getImgPath("",0)%><%=deviceRichimagePath%>" data-src-mobile="<%=CommonUtil.getImgPath("",0)%><%=mobileRichImagePath%>">          
          </c:if>
        </div>           
        <div class="uni_info_right fr opd_univinfo">  
          <c:if test="${not empty uniInfoList.timesRanking}">        
            <div class="trank">
              <div class="tit">CompUniGuide ranking</div> <%--Changed ranking lable from Times to CUG for 08_MAR_2016 By Thiyagu G--%>
              <div class="tval">${uniInfoList.timesRanking}</div>
            </div>  
          </c:if>
          <c:if test="${not empty uniInfoList.overallRating}">
            <c:if test="${uniInfoList.overallRating gt 0}"> 
              <div class="srank">
                <div class="tit">Student rating</div>
                <div class="mt5">
                  <span class="rat${uniInfoList.overallRating} t_tip">
									 <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								   <span class="cmp">
									  <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									  <div class="line"></div>
								  </span>
								  <%--End of tool tip code--%>
									</span>
                  <span class="sval">(${uniInfoList.overallRatingExact})</span>
                </div>
              </div>
            </c:if>
          </c:if>
          <c:if test="${not empty uniInfoList.nextOpenDay}">
            <%-- <div class="opday">
              <div class="tit">Next open day</div>
              <div class="odate">
                <i class="fa fa-calendar fl fa-1_4x"></i>
                <a href="${uniInfoList.openDayUrl}">${uniInfoList.nextOpenDay}</a>
              </div>
            </div> --%>
          </c:if>
          <%
            String wps_collegeId = request.getAttribute("interactionCollegeId") != null && request.getAttribute("interactionCollegeId").toString().trim().length() > 0 ? (String)request.getAttribute("interactionCollegeId") : "0";
            String wp_collegeName = request.getAttribute("interactionCollegeName") != null && request.getAttribute("interactionCollegeName").toString().trim().length() > 0 ? (String)request.getAttribute("interactionCollegeName") : "0";
            String wps_collegeNameDisplay = request.getAttribute("interactionCollegeNameDisplay") != null && request.getAttribute("interactionCollegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("interactionCollegeNameDisplay") : "";
            String wp_floatStyle = request.getParameter("FLOAT_STYLE") != null && request.getParameter("FLOAT_STYLE").trim().length() > 0 ? request.getParameter("FLOAT_STYLE").trim() : "RIGHT";
            String coureId = request.getParameter("courseId") != null && request.getParameter("courseId").trim().length() > 0 ? request.getParameter("courseId") : "0";
            String gasCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(wp_collegeName);  
            //
          %>
          <c:if test="${not empty uniInfoList.subOrderItemId}">
            <div class="gepr"> 
              <div class="btns_interaction fl">
                <c:if test="${uniInfoList.subOrderItemId gt 0}"> 
                  <c:set var="subOrderId2" value="${uniInfoList.subOrderItemId}"/>
                  <%
                    String subOrderId2 = (String)pageContext.getAttribute("subOrderId2");
                    String webformPrices = new WUI.utilities.CommonFunction().getGACPEPrice(subOrderId2, "webform_price");
                    String websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice(subOrderId2, "website_price");
                  %>
                  <%if(!"TRUE".equals(isClearingCourse)){%> 
                  <c:if test="${not empty uniInfoList.subOrderProspectusWebform}">
                    <a rel="nofollow" 
                       target="_blank" 
                       class="get-pros" 
                       onclick="GAInteractionEventTracking('prospectuswebform', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=webformPrices%>); cpeProspectusWebformClick(this,'<%=wps_collegeId%>','${uniInfoList.subOrderItemId}','${uniInfoList.cpeQualificationNetworkId}','${uniInfoList.subOrderProspectusWebform}'); var a='s.tl(';" 
                       href="${uniInfoList.subOrderProspectusWebform}" 
                       title="Get <%=wps_collegeNameDisplay%> Prospectus">
                       Get prospectus
                       <i class="fa fa-caret-right"></i>
                    </a>
                  </c:if>
                  <c:if test="${not empty uniInfoList.subOrderProspectus}">
                    <c:if test="${empty uniInfoList.subOrderProspectusWebform}">
                      <a  onclick="GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '<%=gaCollegeName%>'); return prospectusRedirect('/degrees','<%=wps_collegeId%>','<%=coureId%>','','','${uniInfoList.subOrderItemId}');"
                          class="get-pros"
                          title="Get <%=wps_collegeNameDisplay%> Prospectus">
                          Get prospectus
                          <i class="fa fa-caret-right"></i>
                      </a>   
                    </c:if>
                  </c:if>
                  <%}%>
                  <%if("TRUE".equals(isClearingCourse)){%> 
                   <c:if test="${not empty uniInfoList.subOrderWebsite}">
                        <a rel="nofollow" 
                        target="_blank" 
                        class="visit-web"  
                        href="${uniInfoList.uniHomeUrl}" 
                        title="View <%=wps_collegeNameDisplay%> Profile">View Profile<i class="fa fa-caret-right"></i></a>
                    </c:if>
                    <%}%>
                </c:if>
              </div>
            </div>
          </c:if>         
        </div>        
      </div>
      <div class="fnt_lrg fnt_14 lh_24 cl_grylt mt15 fl">
        ${uniInfoList.profileDesc}
      </div>
      </div>
    </c:if>    
  </c:forEach>
 </section>
 <div class="borderbot mt40 fl"></div>
</c:if>