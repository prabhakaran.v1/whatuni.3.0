<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
  String moduleindex = (String)request.getAttribute("moduleindex");
  moduleindex = moduleindex!=null && moduleindex.trim().length() >0 ? moduleindex : "0";
%>
<c:if test="${not empty requestScope.detailDescList}">
  <c:forEach var="detailDescList" items="${requestScope.detailDescList}" varStatus="rowindex">
    <c:if test="${not empty detailDescList.moduleDescription}">
      <c:set var="moduleDescription" value="${detailDescList.moduleDescription}" /> 
       <%
       String moduleDescription = (String)pageContext.getAttribute("moduleDescription");
       if(moduleDescription.length() > 600){%>
          <p class="fnt_lrg fnt_14 lh_24 cl_grylt pl30 w100p fl" id="lesscontent<%=moduleindex%>">${detailDescList.shortModuleDesc}...<a class="fnt_lrg link_blue" onclick="showMoreLess('lesscontent', <%=moduleindex%>);">more</a></p>
        <%}else{%>
          <p class="fnt_lrg fnt_14 lh_24 cl_grylt pl30 w100p fl" id="lesscontent<%=moduleindex%>">${detailDescList.moduleDescription}</p>
        <%}%>
          <p class="fnt_lrg fnt_14 lh_24 cl_grylt pl30 w100p fl" id="morecontent<%=moduleindex%>" style="display:none">${detailDescList.moduleDescription}...<a class="fnt_lrg link_blue" onclick="showMoreLess('morecontent', <%=moduleindex%>);">show less</a></p>        
    </c:if>
  </c:forEach>
</c:if>
