<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/tlds/bean.tld" prefix="bean"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%@page import="org.apache.commons.validator.GenericValidator"%>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<%  
  String url = "";
  if(!GenericValidator.isBlankOrNull(request.getHeader("referer"))){
    url = request.getHeader("referer");
  }

%>
<head>   
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">       
  
  <%@include  file="/jsp/common/includeCSS.jsp"%> 
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>  
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>    
  <jsp:include  page="/notfound/include/includeErrorResponsive.jsp"/>      
</head>
<body>
<div class="kis_data">
<header class="clipart">
  <div class="">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>    
</header>   
<div class="content-bg">
<div id="content-blk" style="padding:20px;">
<h2 class="cd-head">Where does our information come from</h2>
<p><strong>UNISTATS</strong><br />The UNISTATS is published by HEFCE (The Higher Education Funding Council for England) and HESA (The Higher Education Statistics Agency). It provides a set of comparable figures which students, parents and advisors can use to help make their Higher Education decisions. The UNISTATS holds the information which students have said they find most useful when choosing a course. It's comprised of:</p>
<ul class="blklst no_bold">
    <li>Measures of student satisfaction from the National Student Survey (NSS), which is completed by more than 220,000 mainly final year students in the UK each year.</li>
    <li>Destination of Leavers from Higher Education (DLHE) which surveys students who gained a qualification from a university or college, six months after they left.</li>
    <li>Course-specific information directly provided by universities and colleges.</li>
</ul>
<p>Data from the UNISTATS which appears on WhatUni includes: Course and University satisfaction, average graduate salary, number of graduates achieving 2:1 or higher, and number of students in graduate jobs. This data was last updated in October 2019 and Next update is due in October 2020.</p>
<%--<p>The How you are assessed data comes from KIS and was last updated in 2016.</p>--%>
<a rel="nofollow" href="<%=url%>" class="fright"> &lt; Back</a>
<br />
<p><strong>HESA Student Record</strong><br />The Higher Education Statistics Agency (HESA) is responsible for collecting and analysing data on students and Universities in the UK.</p>
<p>WhatUni institution and course pages use student gender, age, and domicile data from the HESA Student Record.</p>
<p>Entry qualification information contained within the student record is used within the <i>What Did Others Do</i> and <i>I Want to Be</i> tools to provide a picture of the most common subjects studied by students prior to starting a degree course. Only data for first degree students is considered for the calculations used within the tools. This data is categorised by subject using the <a href="https://www.hesa.ac.uk/jacs/JACS_PS.htm" target="_blank">JACS Principal Subjects</a>. This list has been slightly modified for use with the tools to improve the user experience.  For example, Pre-Clinical Medicine and Clinical Medicine have been combined into one subject.</p>
<p>All HESA Student Record data displayed on the site is subject to data display thresholds, where a cohort must consist of at least 52.5 students in order to be displayed on the site. If a cohort contains less than 52.5 students, then an 'insufficient data' message will be displayed on the site. For example, if only 20 students study a particular subject at a specific university, then the data will not be shown on the site.
The data currently held on the site relates to students who started their first year in 2016/17 and 2017/18 academic years. This data was last updated on WhatUni in March 2020 and Next update is due in October 2020, which will include data for the 2018/19 academic year.</p>
<a rel="nofollow" href="<%=url%>" class="fright"> &lt; Back</a>
<br />
<p><strong>Destination of Leavers of Higher Education</strong><br />The Destinations of Leavers from Higher Education survey or DLHE is a statistical survey which aims to contact UK and EU domiciled graduates from higher education (HE) programmes six months after qualifying from their HE course. Its aim is to establish what type of employment or further study they were engaged in, and their salary, on one specific day in the survey period. DLHE data is used on WhatUni within the <i>What Did Others Do</i> and <i>I Want to Be</i> tools to present the most common jobs, industries, and salary ranges for graduates studying specific subjects. Only data for first degree students is considered for the calculations used within the tools.</p>
<p>The data currently held on the site relates to students graduating in 2014/15 and 2015/16. This was last updated in December 2017. The next update is due in October 2020.</p>
<p>Jobs and Industries within the DLHE data are categorised according to <a href="https://www.hesa.ac.uk/includes/C12018_resources/SOC2010DLHE.pdf?v=1.6" target="_blank">Standard Occupational Codes</a> and <a href="https://www.hesa.ac.uk/component/content/article?id=574" target="_blank">Standard Industry Codes</a>. For display on the WhatUni tools, some similar jobs and industries have been grouped together in order to improve the user experience.</p>
<p>All jobs and industry DLHE data displayed on the site is subject to data display thresholds, where a cohort must consist of at least 52.5 students in order to be displayed on the site. If a cohort contains less than 52.5 students, then an 'insufficient data' message will be displayed on the site. For example, if only 20 students study a particular subject at a specific university, then the data will not be shown on the site.</p>
<p>The data currently held on the site relates to students graduating in 2014/15 and 2015/16. This was last updated in December 2017. The next update is due in October 2020.</p>
<a rel="nofollow" href="<%=url%>" class="fright"> &lt; Back</a>
<br />
<%--rebranding text change for hotcourses group to IDP Connect for 30_Jan_19 by Sangeeth.S--%>
<p><strong><spring:message code="wuni.idp.connect.text"/></strong><br />UCAS provides the basic course data including entry requirements, study modes, course duration and start dates for UCAS and CUKAS members as well as other Higher Education Institutions. <spring:message code="wuni.idp.connect.text"/> also collects additional undergraduate content information to enhance the user experience of the site. This includes:</p>
<ul class="blklst no_bold">
    <li>more detailed summaries</li>
    <li>course module data</li>
    <li>application information</li>
    <li>entry requirements and course fees.</li>
</ul>
<p>In addition to that, and to make course comparisons easier for potential users, <spring:message code="wuni.idp.connect.text"/> displays 'per year' course fees. This means that where UCAS have provided overall fees, our system automatically converts these into per year fees for display on our sites.</p>
<p>UCAS updates are supplied and loaded on to the site on a weekly basis. <spring:message code="wuni.idp.connect.text"/>-collected higher education data is maintained by each institution and updated as often as needed but at least once a year.</p>
<a rel="nofollow" href="<%=url%>" class="fright"> &lt; Back</a>
<br />
<p><strong>University Reviews</strong><br />Member reviews are written by the users of this website. We hope that reviews by students who have attended the uni will give you more of a flavour of the place you are hoping to spend the next few years. The opinions expressed in the reviews are very much those of individual users of WhatUni, they are not the views of either WhatUni or Hotcourses Ltd.<br /><a rel="nofollow" href="<%=url%>" class="fright"> &lt; Back</a></p>
<br />
<!--Destinations related updates by Prabha on 15_Dec_2016 -->
<p><strong>DfE</strong></p>
<p>Data displayed on the Whatuni Destinations School Comparison page is from the Department for Education collection, Statistics: destinations of key stage 4 and key stage 5 pupils.  It can be found at the following URL: <a href="https://www.gov.uk/government/collections/statistics-destinations" target="_blank">https://www.gov.uk/government/collections/statistics-destinations</a></p>
<p>The data sets cover all schools in England and covers a wide range of destinations. For the purpose of our school comparison page we focus on the columns labelled: UK higher education institution, Oxford or Cambridge, Russell Group.</p>
<p>National averages refer to schools in England.  Data collection for Scotland, Wales, and Northern Ireland is ongoing.</p>
<a rel="nofollow" href="<%=url%>" class="fright"> &lt; Back</a>
<!--End of destinations updates-->
<br />
</div>
</div>
</div>
<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>
</html>

