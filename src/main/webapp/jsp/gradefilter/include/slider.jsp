<%--
  * @purpose  This jsp consists of ucas slider present in SR grade filter popup
  * Change Log
  * ****************************************************************************
  * Date           Name              Ver.     Changes desc         Rel Ver.
  * 21-NOV-2019    Jeyalakshmi.D     596      First Draft          596
  * ****************************************************************************
--%>
<%@page import="org.apache.commons.validator.GenericValidator"%>
<%
  String pageName = !GenericValidator.isBlankOrNull((String)request.getAttribute("pageName")) ? (String)request.getAttribute("pageName") : "";
  String ucaspoint = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasPoint")) ? (String)request.getAttribute("ucasPoint") : "0";
  String score = !GenericValidator.isBlankOrNull((String)request.getAttribute("score")) ? (String)request.getAttribute("score") : "";
  String minScore = "0";
  String maxScore = ucaspoint;
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("score"))) {
    String[] scoreArr = score.split(",");
    minScore = scoreArr[0];
    maxScore = scoreArr[1];
  }
%>

<!-- UCAS end -->
 <%if(!("mywhatuni".equalsIgnoreCase(pageName))){ %> 
<div class="pt-25 qua_cnf">
  <h3 class="un-tit p-0 f-20 pt-0">View courses from <span id="minSliderVal"><%=minScore%></span> to <span id="maxSliderVal"><%=maxScore%></span> points</h3>
  <div class="rngsl_cnt">
    <div class="rng_wrap">
      <input type="text" id="cmm_rngeSlider" name="cmm_rngeSlider" value="" class="irs-hidden-input" readonly="" style="display:none">
      <div class="ucsre_val">Your UCAS score:<span id="ucasPt"> <%=ucaspoint%> </span> points <i id="ucasArrow" style="left:-2%" class="fa fa-long-arrow-right"></i></div>
      </div>
      <div class="fl add_qualtxt rngesl_sub" style="display: block;">
      </div>
   </div>
   <div id="ucasVal" class="ucscr_dis fl_w100" style="display:none"></div>
</div>
<%}%>