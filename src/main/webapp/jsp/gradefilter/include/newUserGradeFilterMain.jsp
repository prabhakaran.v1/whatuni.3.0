<%--
  * @purpose  This is the main jsp loads initial SR grade filter popup
  * Change Log
  * **************************************************************************
  * Date           Name              Ver.     Changes desc         Rel Ver.
  * 21-NOV-2019    Jeyalakshmi.D     596      First Draft          596
  * **************************************************************************
--%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>
<%String loadingImg = GlobalConstants.WU_CONT + "/images/hm_ldr.gif"; %>
<body>
  <div id="loadingImg" class="cmm_ldericn" style="display:none;"><img alt="loading-image" src="<%=CommonUtil.getImgPath(loadingImg, 0)%>"></div>
     <div id="gradeFilterRevLightBox"></div>
    <div id="middleContent">
      <jsp:include page="/jsp/gradefilter/newUserGradeFilterPage.jsp"/>
    </div>
    <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
    <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />
  </body>