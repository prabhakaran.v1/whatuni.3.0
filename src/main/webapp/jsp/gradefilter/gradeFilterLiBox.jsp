<%--
  * @purpose  This jsp loads the content on clicking calculate matching course from SR grade filter popup
  * Change Log
  * ****************************************************************************************************
  * Date           Name              Ver.     Changes desc         Rel Ver.
  * 21-NOV-2019    Jeyalakshmi.D     596      First Draft          596
  * ****************************************************************************************************
--%>
<%@page import= "WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator"%>

<% String valErrorCode = (String)request.getParameter("valerror");
   String qualification = request.getParameter("qualification");
   String qualification1 = request.getParameter("qualification1");
   String qualification2 = request.getParameter("qualification2");
   String qualification3 = request.getParameter("qualification3");
   String courseCount = request.getParameter("coursecount");
   String onClickMethod = "saveAndContinue()";
   String pageName = request.getParameter("pageName");
   String QualSubList = (String)request.getAttribute("QualSubList");
%> 
<div class="rvbx_shdw"></div>
<div class="revcls ext-cls"><a href="javascript:void(0);" onclick="closeLigBox();GANewAnalyticsEventsLogging('Qualification Filter', 'Pop-Up', 'Back|<%=courseCount%>');""><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"> </a></div>
  <div class="lbxm_wrp">  
   <div class="ext-cont svap_cnt cedwo_cnt cmm_vwcrse">
      <div class="rev_lst ext">
       <div class="cedwo_wrap">
         <div class="ced_wgo"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/whatuni_logo.svg" title="Whatuni logo" alt="Whatuni Logo"></div>
         <div class="ced_desc">
            <% onClickMethod = "saveAndContinue()";
             if("1".equals(courseCount)){%>
                <p class="ced_ptop">We've matched <%=courseCount%> course based on your qualification.</p>
             <% }else{ %>
                <p class="ced_ptop">We've matched <%=courseCount%> courses based on your qualifications.</p>
             <% } %>
         </div>    
       </div>
       <div class="btn_cnt insuf">
       <div class="btnsec_row fl_w100">
       <%if(GenericValidator.isBlankOrNull(valErrorCode)) {%>
         <%if(!"0".equalsIgnoreCase(courseCount)) {
           if("1".equals(courseCount)){%>    
             <a onclick="<%=onClickMethod%>;GANewAnalyticsEventsLogging('Qualification Filter', 'Pop-Up', 'View Courses|<%=courseCount%>');" class="fr bton">VIEW COURSE</a>
           <%} else {%>
             <a onclick="<%=onClickMethod%>;GANewAnalyticsEventsLogging('Qualification Filter', 'Pop-Up', 'View Courses|<%=courseCount%>');" class="fr bton">VIEW COURSES</a>
           <%}
        }%>
       <%} else {%>
       <a onclick="<%=onClickMethod%>;" class="fl bton">CLOSE</a>
       <%}%>
       </div>
       </div>
     </div>
   </div>
</div>
<input type="hidden" id="QualSubList" name="QualSubList" value="<%=QualSubList%>"/> 
