<%--
  * @purpose  This jsp loads the prepopulated data for grade filter popup in SR and my settings page.
  * Change Log
  * ***********************************************************************************************
  * Date           Name              Ver.     Changes desc         Rel Ver.
  * 21-NOV-2019    Jeyalakshmi.D     596      First Draft          596
  * ***********************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import= "WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator,WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants"%>

<%
  CommonFunction common = new CommonFunction();
  String wugoApplyNowBtnFlag = common.getWUSysVarValue("WUGO_APPLY_NOW_BUTTON_FLAG");
  int loop = 0;
  int count = 0, subCount = 0, subLen = 0; 
  String qualSub = "";
  String qualId = ""; 
 
  String ajaxActionName = "QUAL_SUB_AJAX";
  String formStatusId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("pageStatus"))) ? (String)request.getAttribute("pageStatus") : "";
  String removeBtnFlag = "display:block";
   String subject = !GenericValidator.isBlankOrNull((String)request.getAttribute("subject")) ? (String)request.getAttribute("subject") : "";
  String selectFlag = "";
  String location = !GenericValidator.isBlankOrNull((String)request.getAttribute("location")) ? (String)request.getAttribute("location") : "";
   String q =!GenericValidator.isBlankOrNull((String)request.getAttribute("q")) ? (String)request.getAttribute("q") : "";
   String university = !GenericValidator.isBlankOrNull((String)request.getAttribute("university")) ? (String)request.getAttribute("university") : "";
   String qualCode = !GenericValidator.isBlankOrNull((String)request.getAttribute("qualCode")) ? (String)request.getAttribute("qualCode") : "";
   String referralUrl = !GenericValidator.isBlankOrNull((String)request.getAttribute("referralUrl")) ? (String)request.getAttribute("referralUrl") : "";
   String userId = (String)request.getAttribute("userId");
   String subjectSessionId = (String)request.getAttribute("subjectSessionId");
   String ucaspoint = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasPoint")) ? (String)request.getAttribute("ucasPoint") : "0";
   String ucasMaxPoints = (String)request.getAttribute("ucasMaxPoints");
   String styleFlag = "display:block"; String styleFlag2 = "display:none"; String styleFlag3 = "display:none"; 
   String removeLink =   CommonUtil.getImgPath("/wu-cont/images/cmm_close_icon.svg",0);
   String score = !GenericValidator.isBlankOrNull((String)request.getAttribute("score")) ? (String)request.getAttribute("score") : "";
   String minScore = "0";
   String maxScore = ucaspoint;
 String qualifications = "";
  String addQualStyle = "display:none";
  String addSubStyle = "display:block";
  String addQualStyle_1 = "display:block";
  int addSubLimit = 6;
  String removeQualStyle = "display:block";
  int userQualListSize = (!GenericValidator.isBlankOrNull((String)request.getAttribute("userQualSize"))) ? Integer.parseInt((String)request.getAttribute("userQualSize")) : 0;
  String addQualId = "level_3_add_qualif";
  String callTypeFlag = "";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("score"))) {
     String[] scoreArr = score.split(",");
     minScore = scoreArr[0];
     maxScore = scoreArr[1];
   }
   String postcode = !GenericValidator.isBlankOrNull((String)request.getAttribute("postCode")) ? (String)request.getAttribute("postCode") : "";
   String studyMode = !GenericValidator.isBlankOrNull((String)request.getAttribute("studyMode")) ? (String)request.getAttribute("studyMode") : "";
   String pageName = !GenericValidator.isBlankOrNull((String)request.getAttribute("pageName")) ? (String)request.getAttribute("pageName") : "";
   String ucasCode = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasCode")) ? (String)request.getAttribute("ucasCode") : "";
   String locationType = !GenericValidator.isBlankOrNull((String)request.getAttribute("locationType")) ? (String)request.getAttribute("locationType") : "";
   String campusType = !GenericValidator.isBlankOrNull((String)request.getAttribute("campusType")) ? (String)request.getAttribute("campusType") : "";
   String russellGroup = !GenericValidator.isBlankOrNull((String)request.getAttribute("russellGroup")) ? (String)request.getAttribute("russellGroup") : "";
   String module = !GenericValidator.isBlankOrNull((String)request.getAttribute("module")) ? (String)request.getAttribute("module") : "";
   String distance = !GenericValidator.isBlankOrNull((String)request.getAttribute("distance")) ? (String)request.getAttribute("distance") : "";
   String empRate = !GenericValidator.isBlankOrNull((String)request.getAttribute("empRate")) ? (String)request.getAttribute("empRate") : null;
   String ucasTariff = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasTariff")) ? (String)request.getAttribute("ucasTariff") : null;
   String jacs = !GenericValidator.isBlankOrNull((String)request.getAttribute("jacs")) ? (String)request.getAttribute("jacs") : "";
   String assessmentTypeFilter = !GenericValidator.isBlankOrNull((String)request.getAttribute("assessmentTypeFilter")) ? (String)request.getAttribute("assessmentTypeFilter") : null;
   String reviewCategoryFilter = !GenericValidator.isBlankOrNull((String)request.getAttribute("reviewCategoryFilter")) ? (String)request.getAttribute("reviewCategoryFilter") : "";
   String refererUrl = request.getHeader("referer");
   String queryString = (String)request.getAttribute("queryStr");
   String rf = !GenericValidator.isBlankOrNull((String)request.getAttribute("rf")) ? ((String)request.getAttribute("rf")) : "";
   String empRateMax = !GenericValidator.isBlankOrNull((String)request.getAttribute("empRateMax")) ? (String)request.getAttribute("empRateMax") : "";
   String empRateMin = !GenericValidator.isBlankOrNull((String)request.getAttribute("empRateMin")) ? (String)request.getAttribute("empRateMin") : "";
   String selectedQualification = !GenericValidator.isBlankOrNull((String)request.getAttribute("selected_qual")) ? (String)request.getAttribute("selected_qual") : "";
   String urlQual = !GenericValidator.isBlankOrNull((String)request.getAttribute("urlQual")) ? (String)request.getAttribute("urlQual") : "";
   String referrer = !GenericValidator.isBlankOrNull((String)request.getAttribute("referrer")) ? (String)request.getAttribute("referrer") : "";
   String srScore = !GenericValidator.isBlankOrNull((String)request.getAttribute("srScore")) ? (String)request.getAttribute("srScore") : "";
   
%>
<div class="rev_lbox grdfltlbx_ui fadeIn" style="z-index: 1; visibility: visible;">
 <div class="rvbx_shdw"></div>
  <div class="revcls" id="revclsId"><a href="javascript:void(0);" id="closeBoxId" onclick="srCloseLightbox('');"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"> </a></div>
   <div class="rvbx_cnt srchbx_ui">
     <div class="rev_lst">
       <div class="rev_cen">
<section class="cmm_cnt">
  <div class="cmm_col_12 binf_cnt srqual_mn">
    <div class="cmm_wrap" id="cmm_wrap_id">
      <div class="cmm_row">
        <div class="fl ots_cnt">
          <h1 "pln-sub-tit">Enter your qualifications</h1>
        </div>
        <form class="fl basic_inf pers_det cont_det qualif" id="gradeForm">
          <div class="ucas_midcnt">
            <div id="ucas_calc">
              <div class="ucas_mid">
               <div class="add_qualif">
	            <div class="ucas_refld">                  
		     <div class="l3q_rw" id="l3q_rw_id">	
              <c:if test="${not empty requestScope.userQualList}">
                <c:forEach var="userQualList" items="${requestScope.userQualList}" varStatus="i" >    
                <c:set var="indexIValue" value="${i.index}" />              
                <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue").toString()));%>
                  <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                    <%addQualId = "level_3_add_qualif";%>
                  </c:if>
                  <c:if test="${userQualList.entry_qual_desc eq 'GCSE'}">
                    <%addQualId = "level_2_add_qualif";%>
                  </c:if>
                 	
		       <div class="adqual_rw" data-id=level_3_add_qualif id="level_3_qual_<%=count%>" style="<%=addQualStyle_1%>">
			<div class="ucas_row">
		     	<h5 class="cmn-tit txt-upr"><c:if test="${userQualList.entry_qual_level eq '2'}">GRADE TYPE</c:if><c:if test="${userQualList.entry_qual_level ne '2'}"> Qualification</c:if> </h5>
			<fieldset class="ucas_fldrw">
			  <div class="remq_cnt">
		          <div class="ucas_selcnt">
			  <fieldset class="ucas_sbox">
			                      <c:set var="qualDesc" value="${userQualList.entry_qual_desc}"/>
                            <jsp:scriptlet> qualifications = pageContext.getAttribute("qualDesc").toString() + ","; </jsp:scriptlet>
                            <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                              <span class="sbox_txt" id="qualspan_<%=count%>">${userQualList.entry_qual_desc}</span>
			    </c:if>
                            <span class="grdfilt_arw"></span>
			  </fieldset>
			                    <c:if test="${userQualList.entry_qual_level ne '2'}">
			  <select name="qualification<%=count%>" onchange="appendQualDiv(this, '', '<%=count%>');" id="qualsel_<%=count%>" class="ucas_slist">
			    <option value="0">Please Select</option>
			                        <c:forEach var="qualList" items="${requestScope.qualificationList}" >
			                          <c:set var="selectQualId" value="${userQualList.entry_qual_id}"/>
                                <jsp:scriptlet>selectFlag = "";</jsp:scriptlet>
                                <c:if test="${qualList.entry_qual_id eq selectQualId}">
                                  <jsp:scriptlet>selectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                </c:if>
                                <c:if test="${qualList.entry_qualification ne 'GCSE'}">
                                  <c:if test="${empty qualList.entry_qual_id}">
                                    <optgroup label="${qualList.entry_qualification}"></optgroup>
                                  </c:if>
                                  <c:if test="${not empty qualList.entry_qual_id}">
                                 <option value="${qualList.entry_qual_id}" <%=selectFlag%>>${qualList.entry_qualification} </option>
                               </c:if>                                   
                              </c:if>
                             </c:forEach>
			  </select>
                          <div id="qual_sub_grd_hidId">
                            <c:forEach var="grdQualifications" items="${requestScope.qualificationList}" >
                              <c:if test="${grdQualifications.entry_qualification ne 'GCSE'}">                              
                                <c:if test="${not empty grdQualifications.entry_qual_id}">
                                  <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                  <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}" name="" value="${grdQualifications.entry_grade}" />
                                </c:if>
                              </c:if>                                
                            </c:forEach>
                           </div>
                          </c:if>
                                 
                      	</div>
                      </div>
                      <c:if test="${userQualList.entry_qual_level ne '2'}">
                        <%-- <%if(count != 0){removeQualStyle = "display:block";}%> --%>
                        <div class="add_fld" style="<%=removeQualStyle%>">
                           <a id="remQual<%=count%>" onclick="removeQualEntryReq('level_3_qual_<%=count%>', '<%=count%>');" class="btn_act1 bck f-14 pt-5">
                             <span class="hd-mob rq_mob" id="span_id">Remove Qualification</span>
                             <input type="hidden" id= "removeQualCount" value="<%=count%>"/>	
                           </a>
                        </div>	
                      </c:if>
		    </fieldset>
		  </div>
                  <div class="subgrd_fld" id="subGrdDiv<%=count%>">
                    <div class="ucas_row grd_row" id="grdrow_<%=count%>">
 <div class="rmv_act">
                    <fieldset class="ucas_fldrw quagrd_tit">
                      <div class="ucas_tbox tx-bx">
                        <h5 class="cmn-tit qual_sub">Subject</h5>
                      </div>
		      <div class="ucas_selcnt rsize_wdth">
		      <c:choose>
				  <c:when test="${userQualList.entry_qual_id ne '19'}">
                    <h5 class="cmn-tit qual_grd">Grade</h5>
                  </c:when>  
				  <c:otherwise>
				    <h5 class="cmn-tit qual_grd">Points</h5>
				  </c:otherwise>
				</c:choose>      
		      </div>
                    </fieldset>
                    <c:forEach var="subjectList" items="${userQualList.qual_subject_list}" varStatus="j" >
                      <c:set var="indexJValue" value="${j.index}"/>
                      <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue").toString()));%>
                       <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
 			<fieldset class="ucas_fldrw">
                          <div class="ucas_tbox w-390 tx-bx">
			    <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="sub_${subjectList.entry_subject_id}" onblur="getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="${subjectList.entry_subject}" class="frm-ele" autocomplete="off">															
                            <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                          <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                        </div>
                      </div>
		      		<div class="ucas_selcnt rsize_wdth">
		      				        
				        <c:choose>
						  <c:when test="${userQualList.entry_qual_id ne '19'}">
						    <div class="ucas_sbox mb-ht w-84" id="1grades1">
		                      <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">${subjectList.entry_grade}</span><span class="grdfilt_arw"></span>
		                    </div>
				            <select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" id="qualGrd_<%=count%>_<%=subCount%>"></select>
		                    <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=count%>_<%=subCount%>','${subjectList.entry_grade}'); </script>
						  </c:when>  
						  <c:otherwise>	          						  
						    <div class="ucas_tbox w-390 tx-bx fl_w100" style="width: 158px;" id="1grades1">						      
						      <input onkeyup="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');" onblur="getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');ucasPtVal(this);" onfocus="clearFields(this)" type="text" maxlength="3" class="frm-ele fl_100" id="span_grde_<%=count%>_<%=subCount%>" value="${subjectList.entry_grade}" autocomplete="off"></input>
						    </div>
						  </c:otherwise>
						</c:choose>                          
                          <c:if test="${userQualList.entry_qual_level ne '2'}">
                           <input class="subjectArr" data-id="level_3" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${userQualList.entry_qual_id}" value="${subjectList.entry_subject_id}" />
                         </c:if>
                         <c:if test="${userQualList.entry_qual_level eq '2'}">
                           <input class="subjectArr" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${userQualList.entry_qual_id}" value="${subjectList.entry_subject_id}" />
                         </c:if>                             
                          <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_${userQualList.entry_qual_id}" value="${subjectList.entry_grade}" />
                          <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${userQualList.entry_qual_id}" value="${userQualList.entry_qual_id}" />
                          <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${userQualList.entry_qual_id}" value="<%=count+1%>" />
                        </div>
                        <%if(subCount == 0){
                           removeBtnFlag = "display:none";
                         }else{
                           removeBtnFlag = "display:block";
                         }
                         if(subCount == 1){
                         %>
                         <script>jq("#remSubRow<%=count%>0").show();</script>
                        <%}%>
			<div class="add_fld"> 
                          <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5">
                            <span class="hd-mob">Remove</span>
                            <span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"/></span>
                          </a>
                         </div>
                       </fieldset>
                     </div>
                   </c:forEach>
                   <%if((subCount + 1) < addSubLimit){
                      addSubStyle = "display:block";
                    }else{
                      addSubStyle = "display:none";
                    }%>
                    <div class="ad-subjt" id="addSubject_<%=count%>" style="<%=addSubStyle%>">
                      <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('${userQualList.entry_qual_id}','countAddSubj_<%=count%>', '<%=count%>');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                    </div>
                   </div>
                  </div>
               
		</div>
		   <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                  <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
              </div>
              <%-- <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                <%if(count == (userQualListSize-1)){addQualStyle = "display:none";}%>                    
		              <div class="add_qualtxt" id="addQualBtn_<%=count%>" style="<%=addQualStyle%>">
                  <p class="aq_hlptxt">Studied more than one qualification? </p>
                  <a href="javascript:void(0);" onclick="addQualEntryReq('<%=count+1%>')" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> Add a qualification</a></div>	
	       </c:if> --%>
               
           </c:forEach>
           </c:if>
           </div>
           
           <c:if test="${not empty requestScope.userQualList}">
             <c:forEach var="userQualList" items="${requestScope.userQualList}" varStatus="i" end="<%=userQualListSize%>">    
                <c:set var="indexIValue" value="${i.index}" />              
                <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue").toString()));%>
                 
                  <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                <%-- <%if(count == (userQualListSize-1)){addQualStyle = "display:none";}%>       --%>              
		              <div class="add_qualtxt" data-id="level_3_add_qualtxt" id="addQualBtn_<%=count%>" style="<%=addQualStyle%>">
                  <p class="aq_hlptxt">Studied more than one qualification? </p>
                  <a href="javascript:void(0);" onclick="addQualEntryReq('<%=count+1%>')" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> Add a qualification</a></div>	
	       </c:if>
                  
                  </c:forEach>
                  </c:if>
          
           </div>
           </div>
         </div>
        </div>
        <jsp:include page="/jsp/gradefilter/include/slider.jsp"/>
        <div class="err_field fl_w100" id="errorDiv" style="display:none">
              <p id="pId"></p>
            </div>
           <%if("mywhatuni".equalsIgnoreCase(pageName)){ %>
			   <div class="btn_cnt btn_cnt_bb0 pt-40">
                 <div class="fr">
                   <a onclick="saveAndContinue();" id="viewButton" class="fr bton myset_dave">SAVE <i class="fa fa-long-arrow-right"></i></a>
				 </div>
			   </div>
             <%}else{ %>
			  <div class="btn_cnt pt-40">
                <div class="fr">
                  <a onclick="getMatchedCourse();" id="viewButton" class="fr bton">CALCULATE MATCHING COURSES <i class="fa fa-long-arrow-right"></i></a>
				</div>
			  </div>
             <%} %>
       </div>
     </form>
   </div>
  </div>
  </div>
</section>
</div>
</div>
</div>
</div>

<input type="hidden" id= "userUcasScore" value="<%=ucaspoint%>"/>
<input type="hidden" id="country_hidden" name="country_hidden" value="<%=location%>"/>
<input type="hidden" id="newQualDesc" value="<%=qualifications%>" />
<input type="hidden" id="subject" name="subject" value="<%=subject%>"/>
<input type="hidden" id="q" name="q" value="<%=q%>"/>
<input type="hidden" id="university" name="university" value="<%=university%>"/>  
<input type="hidden" id="qualCode" name="qualCode" value="<%=qualCode%>"/>  
<input type="hidden" id="referralUrl" name="referralUrl" value="<%=referralUrl%>"/>  
<input type="hidden" id="minSliderVal" name="minSliderVal" value="<%=minScore%>"/>
<input type="hidden" id="maxSliderVal" name="maxSliderVal" value="<%=maxScore%>"/> 
<input type="hidden" id="ucasMaxPoints" name="ucasMaxPoints" value="<%=ucasMaxPoints%>"/>
<input type="hidden" id="removeLinkImg" value="<%=removeLink%>" />
<input type="hidden" id="score" value="<%=score%>"/>
<input type="hidden" id="refresh" value="no">
<input type="hidden" id= "postcode" name="postcode" value="<%=postcode%>"/>
<input type="hidden" id="study-mode" name="study-mode" value="<%=studyMode%>"/>
<input type="hidden" id="campus-type" name="campus-type" value="<%=campusType%>"/>
<input type="hidden" id="page-name" name="page-name" value="<%=pageName%>" />
<input type="hidden" id="ucas-code" name="ucas-code" value="<%=ucasCode%>"/>
<input type="hidden" id="location-type" name="location-type" value="<%=locationType%>"/>
<input type="hidden" id="module" name="module" value="<%=module%>"/>  
<input type="hidden" id="distance" name="distance" value="<%=distance%>"/>  
<input type="hidden" id="empRate" name="empRate" value="<%=empRate%>"/>  
<input type="hidden" id="ucasTariff" name="ucasTariff" value="<%=ucasTariff%>"/>
<input type="hidden" id="jacs" name="jacs" value="<%=jacs%>"/> 
<input type="hidden" id="assessment-type" name="assessment-type" value="<%=assessmentTypeFilter%>"/>
<input type="hidden" id="your-pref" name="your-pref" value="<%=reviewCategoryFilter%>"/>
<input type="hidden" id="your-pref" name="your-pref" value="<%=reviewCategoryFilter%>"/>
<input type="hidden" id="queryString" name="queryString" value="<%=queryString%>"/>
<input type="hidden" id="rf" name="rf" value="<%=rf%>"/>  
<input type="hidden" id="empRateMax" name="empRateMax"  value="<%=empRateMax%>" />
<input type="hidden" id="empRateMin" name="empRateMin"  value="<%=empRateMin%>" />
<input type="hidden" id="russellGroup" name="russellGroup" value="<%=russellGroup%>" />
<input type="hidden" id="selected_qual" name="selected_qual" value="<%=selectedQualification%>"/>
<input type="hidden" id="urlQual" name=urlQual value="<%=urlQual%>"/>
<input type="hidden" id="referrer" name=referrer value="<%=referrer%>"/>
<input type="hidden" id="srScore" name=srScore value="<%=srScore%>"/>

<script type="text/javascript">
jq( document ).ready(function() {
  //appendQualDiv(1, '', '0', 'new_entry_page', 'add_Qual');  
  ucasArrow();
  onloadShowingAddQualBtn();
});
</script>
 



                                   
    