<%--
  * @purpose  This jsp loads the data for grade filter popup in SR and my settings page.
  * Change Log
  * **********************************************************************************
  * Date           Name              Ver.     Changes desc         Rel Ver.
  * 21-NOV-2019    Jeyalakshmi.D     596      First Draft          596
  * **********************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants,WUI.utilities.CommonFunction"%>
<%

  CommonFunction common = new CommonFunction();
  String wugoApplyNowBtnFlag = common.getWUSysVarValue("WUGO_APPLY_NOW_BUTTON_FLAG");
  int loop = 0;
  int count = 0, subCount = 0, subLen = 0; 
  String qualSub = "";
  String qualId = ""; 
  String actionName = (!GenericValidator.isBlankOrNull((String)request.getAttribute("actionName"))) ? (String)request.getAttribute("actionName") : "EDIT";//PROV_QUAL_EDIT
  String reviewFlag = "N";
  String ajaxActionName = "QUAL_SUB_AJAX";  
  String removeBtnFlag = "display:block";
  String subject = !GenericValidator.isBlankOrNull((String)request.getAttribute("subject")) ? (String)request.getAttribute("subject") : "";
  String selectFlag = "";
  String gcseSelectFlag = "";
  String addQualStyle = "display:block";
  String addQualBtnStyle = "display:block";
  String gcseSelOption = ": 9-1";
  String removeLink =   CommonUtil.getImgPath("/wu-cont/images/cmm_close_icon.svg",0);
  String qualifications = "";
  String selectQualId ="";
  String selectQualSubLen = ""; 
  String location = !GenericValidator.isBlankOrNull((String)request.getAttribute("location")) ? (String)request.getAttribute("location") : "";
  String q =!GenericValidator.isBlankOrNull((String)request.getAttribute("q")) ? (String)request.getAttribute("q") : "";
  String university = !GenericValidator.isBlankOrNull((String)request.getAttribute("university")) ? (String)request.getAttribute("university") : "";
  String qualCode = !GenericValidator.isBlankOrNull((String)request.getAttribute("qualCode")) ? (String)request.getAttribute("qualCode") : "";
  String referralUrl = !GenericValidator.isBlankOrNull((String)request.getAttribute("referralUrl")) ? (String)request.getAttribute("referralUrl") : "";
  String userId = (String)request.getAttribute("userId");
  String subjectSessionId = (String)request.getAttribute("subjectSessionId");
  String ucaspoint = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasPoint")) ? (String)request.getAttribute("ucasPoint") : "0";
  String ucasMaxPoints = (String)request.getAttribute("ucasMaxPoints");
  String styleFlag = "display:block"; String styleFlag2 = "display:none"; String styleFlag3 = "display:none"; 
  String score = !GenericValidator.isBlankOrNull((String)request.getAttribute("score")) ? (String)request.getAttribute("score") : "";
  String minScore = "0";
  String maxScore = ucaspoint;
  int addSubLimit = 6;
  String removeQualStyle = "display:none";
  int userQualListSize = (!GenericValidator.isBlankOrNull((String)request.getAttribute("userQualSize"))) ? Integer.parseInt((String)request.getAttribute("userQualSize")) : 0;
  String addQualId = "level_3_add_qualif";
  String callTypeFlag = "";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("score"))) {
     String[] scoreArr = score.split(",");
     minScore = scoreArr[0];
     maxScore = scoreArr[1];
   }
   String postcode = !GenericValidator.isBlankOrNull((String)request.getAttribute("postCode")) ? (String)request.getAttribute("postCode") : "";
   String studyMode = !GenericValidator.isBlankOrNull((String)request.getAttribute("studyMode")) ? (String)request.getAttribute("studyMode") : "";
   String pageName = !GenericValidator.isBlankOrNull((String)request.getAttribute("pageName")) ? (String)request.getAttribute("pageName") : "";
   String ucasCode = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasCode")) ? (String)request.getAttribute("ucasCode") : "";
   String locationType = !GenericValidator.isBlankOrNull((String)request.getAttribute("locationType")) ? (String)request.getAttribute("locationType") : "";
   String campusType = !GenericValidator.isBlankOrNull((String)request.getAttribute("campusType")) ? (String)request.getAttribute("campusType") : "";
   String russellGroup = !GenericValidator.isBlankOrNull((String)request.getAttribute("russellGroup")) ? (String)request.getAttribute("russellGroup") : "";
   String module = !GenericValidator.isBlankOrNull((String)request.getAttribute("module")) ? (String)request.getAttribute("module") : "";
   String distance = !GenericValidator.isBlankOrNull((String)request.getAttribute("distance")) ? (String)request.getAttribute("distance") : "";
   String empRate = !GenericValidator.isBlankOrNull((String)request.getAttribute("empRate")) ? (String)request.getAttribute("empRate") : "";
   String ucasTariff = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasTariff")) ? (String)request.getAttribute("ucasTariff") : null;
   String jacs = !GenericValidator.isBlankOrNull((String)request.getAttribute("jacs")) ? (String)request.getAttribute("jacs") : "";
   String assessmentTypeFilter = !GenericValidator.isBlankOrNull((String)request.getAttribute("assessmentTypeFilter")) ? (String)request.getAttribute("assessmentTypeFilter") : "";
   String reviewCategoryFilter = !GenericValidator.isBlankOrNull((String)request.getAttribute("reviewCategoryFilter")) ? (String)request.getAttribute("reviewCategoryFilter") : "";
   String refererUrl = request.getHeader("referer");
   String rf = !GenericValidator.isBlankOrNull((String)request.getAttribute("rf")) ? ((String)request.getAttribute("rf")) : "";
   String empRateMax = !GenericValidator.isBlankOrNull((String)request.getAttribute("empRateMax")) ? (String)request.getAttribute("empRateMax") : "";
   String empRateMin = !GenericValidator.isBlankOrNull((String)request.getAttribute("empRateMin")) ? (String)request.getAttribute("empRateMin") : "";
   String selectedQualification = !GenericValidator.isBlankOrNull((String)request.getAttribute("selected_qual")) ? (String)request.getAttribute("selected_qual") : "";
   String urlQual = !GenericValidator.isBlankOrNull((String)request.getAttribute("urlQual")) ? (String)request.getAttribute("urlQual") : "";
   String referrer = !GenericValidator.isBlankOrNull((String)request.getAttribute("referrer")) ? (String)request.getAttribute("referrer") : "";
   String srScore = !GenericValidator.isBlankOrNull((String)request.getAttribute("srScore")) ? (String)request.getAttribute("srScore") : "";
    
%>
<div class="rev_lbox grdfltlbx_ui fadeIn" style="z-index: 1; visibility: visible;">
 <div class="rvbx_shdw"></div>
  <div class="revcls" id="revclsId"><a href="javascript:void(0);" id="closeBoxId" onclick="srCloseLightbox('');"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"> </a></div>
   <div class="rvbx_cnt srchbx_ui">
     <div class="rev_lst">
       <div class="rev_cen">
<section class="cmm_cnt">
  <div class="cmm_col_12 binf_cnt srqual_mn">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <div class="fl ots_cnt">
          <h1 "pln-sub-tit">Enter your qualifications</h1>
        </div>
        <!-- Uni List -->
        <!-- Contact details -->
        <c:if test="${not empty requestScope.qualificationList}">
        <form class="fl basic_inf pers_det cont_det qualif" id="gradeForm">
          <!-- UCAS -->
          <div class="ucas_midcnt">
            <div id="ucas_calc">
              <div class="ucas_mid">
                <!--Qualification Repeat Field start 1-->
                
                  <div class="add_qualif">
                  <div class="ucas_refld">
                    <!--<h3 class="un-tit">Level 3</h3>-->
                    <!-- L3 Qualification Row  -->
                    <div class="l3q_rw">
                     <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" end="2">
                       <c:set var="indexIvalue" value="${i.index}"/>
                      <c:if test="${not empty qualList.entry_qual_id}">
                       <c:set var="selectQualId1" value="${qualList.entry_qual_id}"/>
                        <%selectQualId = pageContext.getAttribute("selectQualId1").toString();%>
                     </c:if>
                      <c:if test="${empty qualList.entry_qual_id}">
                        <% selectQualId ="1";%>
                      </c:if>
                      <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIvalue").toString()));if(count > 0){addQualStyle = "display:none";}%>
                      <!-- Add Qualification & Subject 1-->		
                      <div class="adqual_rw" data-id=level_3_add_qualif id="level_3_qual_<%=count%>" style="<%=addQualStyle%>">
                        <div class="ucas_row">
                          <h5 class="cmn-tit txt-upr">Qualification</h5>
                          <fieldset class="ucas_fldrw">
                            <div class="remq_cnt">
                              <div class="ucas_selcnt">
                                <fieldset class="ucas_sbox">
                                  <c:set var="selectQualName" value="${qualList.entry_qualification}"/>
                                  <%String selectQualName = pageContext.getAttribute("selectQualName").toString(); %>
                                  <%if("0".equals(String.valueOf(pageContext.getAttribute("selectQualName").toString()))){if("A - Levels".equalsIgnoreCase(selectQualName)){selectQualName="A Level";}qualifications = selectQualName ;}%>
                                  <span class="sbox_txt" id="qualspan_<%=count%>"><%=selectQualName%></span>
                                  <span class="grdfilt_arw"></span>
                                </fieldset>
                                <select name="qualification1" onchange="appendQualDiv(this, '', '<%=count%>', 'new_entry_page');" id="qualsel_<%=count%>" class="ucas_slist">
                                  <option value="0">Please Select</option>
                                  <c:forEach var="qualListLevel3" items="${requestScope.qualificationList}">
                                   
                                   <jsp:scriptlet>selectFlag = "";if("".equals(selectQualId)){selectQualId="1";}</jsp:scriptlet>
                                   <c:set var="selectQualId" value="<%=selectQualId%>" />
                                   <c:if test="${qualListLevel3.entry_qual_id eq selectQualId}">
                                     <jsp:scriptlet>selectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                   </c:if>
                                   <c:if test="${qualListLevel3.entry_qualification ne 'GCSE'}">
                                     <c:if test="${empty qualListLevel3.entry_qual_id}">
                                       <optgroup label="${qualListLevel3.entry_qualification}"></optgroup>
                                     </c:if>
                                     <c:if test="${not empty qualListLevel3.entry_qual_id}">
                                       <option value="${qualListLevel3.entry_qual_id}" <%=selectFlag%>>${qualListLevel3.entry_qualification} </option>
                                     </c:if>                                    
                                   </c:if>
                                  </c:forEach>
                                </select>
                                <c:forEach var="grdQualifications" items="${requestScope.qualificationList}">
                                 <c:if test="${grdQualifications.entry_qualification ne 'GCSE'}">                              
                                   <c:if test="${not empty grdQualifications.entry_qual_id}">
                                     <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                     <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}" name="" value="${grdQualifications.entry_grade}" />
                                   </c:if>
                                 </c:if>                                
                               </c:forEach>
                              </div>
                            </div>
                            <%if(0!= count){%>
                            <div class="add_fld">
                              <a id="1rSubRow3" onclick="javascript:removeQualification('level_3_qual_<%=count%>', <%=count-1%>, 'addQualBtn_<%=count%>');" class="btn_act1 bck f-14 pt-5">
                                <span class="hd-mob rq_mob">Remove Qualification</span><!--<span class="hd-desk"><i class="fa fa-times"></i></span>-->
                              </a>
                            </div>
							<div class="err" id="err_qualse1" style="display:none;"></div>
                            <%}%>                            
                          </fieldset>
                          
                        </div>
                           <div class="subgrd_fld" id="subGrdDiv<%=count%>">
                        <div class="ucas_row grd_row" id="grdrow_<%=count%>">
                          <div class="rmv_act">
                            <fieldset class="ucas_fldrw quagrd_tit">
                              <div class="ucas_tbox tx-bx">
                                <h5 class="cmn-tit qual_sub">Subject</h5>
                              </div>
                              <div class="ucas_selcnt rsize_wdth">
                                <h5 class="cmn-tit qual_grd">Grade</h5>
                              </div>
                            </fieldset>   
                            <c:if test="${not empty qualList.entry_subject}">  
                            <c:set var="selectQualSubLen1" value="${qualList.entry_subject}"/>
                            <%String selectQualSubLen1 = pageContext.getAttribute("selectQualSubLen1").toString();
                              selectQualSubLen = selectQualSubLen1; %>
                            </c:if>
                            <c:if test="${empty qualList.entry_subject}">
                            <%selectQualSubLen = "3";%>
                            </c:if>
                              
                            <c:forEach var="subjectList" items="${qualificationList}" varStatus="j" end="<%=Integer.parseInt(selectQualSubLen)%>">
                              <c:set var="indexJValue" value="${j.index}"/>
                              <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue").toString()));%>
                              <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
                                <fieldset class="ucas_fldrw">
                                  <div class="ucas_tbox w-390 tx-bx">
                                    <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="subIp_<%=subCount%>" onblur="getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="" class="frm-ele" autocomplete="off">															
                                    <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                                    <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                                    </div>
                                  </div>
                                  <div class="ucas_selcnt rsize_wdth">
                                    <div class="ucas_sbox mb-ht w-84" id="1grades1">
                                      <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">A*</span><span class="grdfilt_arw"></span>
                                    </div>
                                    <select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" id="qualGrd_<%=count%>_<%=subCount%>"></select>
                                    <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=count%>_<%=subCount%>','${subjectList.entry_grade}'); </script>                                    
                                    <input class="subjectArr" data-id="level_3" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_<%=selectQualId%>" value="" />                                
                                    <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_<%=selectQualId%>" value="A*" />
                                    <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_<%=selectQualId%>" value="<%=selectQualId%>" />
                                    <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_<%=selectQualId%>" value="<%=count+1%>" />
                                  </div>
                                  <%if(subCount == 0){
                                    removeBtnFlag = "display:none";
                                  }else{
                                    removeBtnFlag = "display:block";
                                  }
                                  if(subCount == 1){%>                                  
                                    <script>jq("#remSubRow<%=count%>0").show();</script>
                                  <%}%>                                  
                                  <div class="add_fld"> <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"></span></a></div>
                                </fieldset>
                              </div>
                            </c:forEach>                      
                            <div class="ad-subjt" id="addSubject_<%=count%>" style="display:block">
                              <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('<%=selectQualId%>','countAddSubj_<%=count%>', '<%=count%>');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                            </div>                          
                          </div>
                        </div>
                        </div>
                        <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                      </div>
                      <!-- Add Qualification & Subject 1 -->
                      <!-- L3 Qualification Row -->
                      </c:forEach>
                    </div>
                    
                    <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" end="1">
                      <c:set var="indexIValue" value="${i.index}" />
                      <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue").toString()));if(count > 0){addQualBtnStyle = "display:none";}%>
                      <div class="add_qualtxt" id="addQualBtn_<%=count%>" style="<%=addQualBtnStyle%>">
                        <p class="aq_hlptxt">Studied more than one qualification? Select additional below</p>
                        <a href="javascript:void(0);" onclick="addQualification('level_3_qual_<%=count + 1%>', <%=count + 1%>, 'addQualBtn_<%=count%>');" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> Add a qualification</a>
                      </div>
                    </c:forEach>
                    <!-- Tariff Points Container Start -->
                    <!-- Tariff Points Container End -->		
                  </div>
                </div>
                <!--Qualification Repeat Field end 1 -->
               </div>
             </div>
            <jsp:include page="/jsp/gradefilter/include/slider.jsp"/>
            <div class="err_field fl_w100" id="errorDiv" style="display:none">
              <p id="pId"></p>
            </div>
            <%if("mywhatuni".equalsIgnoreCase(pageName)){ %> 
				  <div class="btn_cnt btn_cnt_bb0 pt-40">
                    <div class="fr skpvw_cnt">
                       <a onclick="saveAndContinue();" id="viewButton" class="fr bton myset_dave">SAVE <i class="fa fa-long-arrow-right"></i></a>
					</div>
			      </div>
                <%}else{%>
				  <div class="btn_cnt pt-40">
                    <div class="fr skpvw_cnt">
                      <a onclick="getMatchedCourse();" id="viewButton" class="fr bton">CALCULATE MATCHING COURSES <i class="fa fa-long-arrow-right"></i></a>
                      <a onclick="skipAndViewResults();" class="fl bck skvw_btn" title="Skip and view results">Skip and view results</a>
					</div>
				   </div>
                <%}%>
          </div>
         </form>
        </c:if>
      </div>
    </div>
  </div>
</section>
</div>
</div>
</div>
</div>

<input type="hidden" id= "userUcasScore" value="<%=ucaspoint%>"/>
<input type="hidden" id="country_hidden" name="country_hidden" value="<%=location%>"/>
<input type="hidden" id="newQualDesc" value="<%=qualifications%>" />
<input type="hidden" id="subject" name="subject" value="<%=subject%>"/>
<input type="hidden" id="q" name="q" value="<%=q%>"/>
<input type="hidden" id="university" name="university" value="<%=university%>"/>  
<input type="hidden" id="qualCode" name="qualCode" value="<%=qualCode%>"/>  
<input type="hidden" id="referralUrl" name="referralUrl" value="<%=referralUrl%>"/>  
<input type="hidden" id="minSliderVal" name="minSliderVal" value="<%=minScore%>"/>
<input type="hidden" id="maxSliderVal" name="maxSliderVal" value="<%=maxScore%>"/> 
<input type="hidden" id="ucasMaxPoints" name="ucasMaxPoints" value="<%=ucasMaxPoints%>"/>
<input type="hidden" id="removeLinkImg" value="<%=removeLink%>" />
<input type="hidden" id="score" value="<%=score%>"/>
<input type="hidden" id="refresh" value="no">
<input type="hidden" id= "postcode" name="postcode" value="<%=postcode%>"/>
<input type="hidden" id="study-mode" name="study-mode" value="<%=studyMode%>"/>
<input type="hidden" id="campus-type" name="campus-type" value="<%=campusType%>"/>
<input type="hidden" id="page-name" name="page-name" value="<%=pageName%>" />
<input type="hidden" id="ucas-code" name="ucas-code" value="<%=ucasCode%>"/>
<input type="hidden" id="location-type" name="location-type" value="<%=locationType%>"/>
<input type="hidden" id="module" name="module" value="<%=module%>"/>  
<input type="hidden" id="distance" name="distance" value="<%=distance%>"/>  
<input type="hidden" id="empRate" name="empRate" value="<%=empRate%>"/>  
<input type="hidden" id="ucasTariff" name="ucasTariff" value="<%=ucasTariff%>"/>
<input type="hidden" id="jacs" name="jacs" value="<%=jacs%>"/> 
<input type="hidden" id="assessment-type" name="assessment-type" value="<%=assessmentTypeFilter%>"/>
<input type="hidden" id="your-pref" name="your-pref" value="<%=reviewCategoryFilter%>"/>
<input type="hidden" id="rf" name="rf" value="<%=rf%>"/>  
<input type="hidden" id="empRateMax" name="empRateMax"  value="<%=empRateMax%>" />
<input type="hidden" id="empRateMin" name="empRateMin"  value="<%=empRateMin%>" />
<input type="hidden" id="russellGroup" name="russellGroup" value="<%=russellGroup%>" />
<input type="hidden" id="selected_qual" name="selected_qual" value="<%=selectedQualification%>"/>
<input type="hidden" id="urlQual" name=urlQual value="<%=urlQual%>"/>
<input type="hidden" id="referrer" name=referrer value="<%=referrer%>"/>
<input type="hidden" id="srScore" name=srScore value="<%=srScore%>"/>
<%-- <input type="hidden" id="sortingUrl" name="sortingUrl" value="<%=sortingUrl%>" /> --%>

<script type="text/javascript">
jq( document ).ready(function() {
  appendQualDiv(1, '', '0', 'new_entry_page', 'add_Qual');  
  ucasArrow();
});
</script>
 