<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator" autoFlush="true" %>

<body class="md non_log">
    <div id="ol" style="display: none; position: fixed; top: 0px; left: 0px; z-index: 998; width: 768px; height: 100%;"></div>
    <div id="mbox" style="display: none; position: absolute; z-index: 999;"><span><div id="mbd"></div></span></div>
    <%-- GTM script included under body tag --%>
	<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
		<jsp:param name="PLACE_TO_INCLUDE" value="BODY"/>
	</jsp:include>
	<%-- GTM script included under body tag --%>
    <div class="wu_home cpc_landing cpc_single">
      <div class id="shadowId"></div> 
        <header class="md clipart">
            <div class="large"></div>
            <div class="">
                <div class="content-bg">
                    <div id="desktop_hdr">                                   
                       <jsp:include page="/jsp/cpc/include/singleCPCHeader.jsp" />
                    </div>
                </div>
            </div>
        </header>
        <jsp:include page="/jsp/cpc/include/singleCPCTopContent.jsp" />
        <%-- <c:if test="${not empty courseDetailList}"> --%>
		  <jsp:include page="/jsp/cpc/include/cpcSingleMiddleContent.jsp" />
		<%-- </c:if> --%>
        <p class="bk_to_top" style="display:none" id="back-top">
            <a href="#top"> <img src="<%=CommonUtil.getImgPath("/wu-cont/images/bk_2_top.png", 1)%>" alt="go to top"> </a>
        </p>
        <input type="hidden" id="smartBannerJs" name="smartBannerJs" value="jquery.smartbanner.js">
    </div>
    <input type="hidden" id="pageType" value="SINGLE" />
    <input type="hidden" id="campaign" value="${campaign}" />
    <jsp:include page="/jsp/cpc/include/commonLogging.jsp">
    <jsp:param name="pageType" value="SINGLE"/>
    </jsp:include>
</body>