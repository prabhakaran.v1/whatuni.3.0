<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction,WUI.utilities.GlobalConstants,org.apache.commons.validator.GenericValidator"%>
<%String imgpath=""; 
String gaCollegeName ="";
String courseId = "0";
String CONTEXT_PATH = GlobalConstants.WU_CONTEXT_PATH;
CommonFunction commonFunction = new CommonFunction();
String searchhits = (String) request.getAttribute("courseCount");
String pageno = request.getAttribute("pageno") != null ? (String)request.getAttribute("pageno") : "1";
String recorddisplay = "12";
int numofpages = (!GenericValidator.isBlankOrNull(searchhits))? (Integer.parseInt(searchhits.replaceAll(",", "")) / Integer.parseInt(recorddisplay)) : 0 ;
int pgNo = Integer.parseInt(pageno);
int rcdDisp = 0;
int totCrsCnt = 0;
int fromCourseRecordCount = 0;
int endCourseRecordCount = 0;
if(!GenericValidator.isBlankOrNull(searchhits)){
  if(Integer.parseInt(searchhits) % Integer.parseInt(recorddisplay) > 0) {  numofpages++; }	
  pgNo = Integer.parseInt(pageno);
  rcdDisp = Integer.parseInt(recorddisplay);
  totCrsCnt = Integer.parseInt(searchhits);
  fromCourseRecordCount = (pgNo == 1) ? 1 : ((pgNo*rcdDisp) - (rcdDisp -1));
  endCourseRecordCount = (pgNo*rcdDisp < totCrsCnt) ? pgNo*rcdDisp : totCrsCnt;
}
String urldata_1 = (String)request.getAttribute("URL_1");
String urldata_2 = (String)request.getAttribute("URL_2");
String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "NULL";
String courseCount = (request.getAttribute("courseCount") !=null && !request.getAttribute("courseCount").equals("null") && String.valueOf(request.getAttribute("courseCount")).trim().length()>0 ? String.valueOf(request.getAttribute("courseCount")) : ""); 
String resultExists = (request.getAttribute("resultExists") !=null && !request.getAttribute("resultExists").equals("null") && String.valueOf(request.getAttribute("resultExists")).trim().length()>0 ? String.valueOf(request.getAttribute("resultExists")) : "");

%>
<c:set var="CONTEXT_PATH" value="<%=CONTEXT_PATH%>"/>
<c:choose>
<c:when test="${not empty courseDetailList and courseCount gt 0}">
<div class="cpc_uni_wrap">
	<div class="cpc_uni_pod">				
	<c:if test="${not empty courseDetailList}">
	   <c:forEach var="courseDetailList" items="${courseDetailList}" varStatus="row">
		<div class="pop_uni">			
			<div class="pop_uni_det fl_w100">
			  <div class="cpc_uni_cnt_ht">			
				<div class="pop_uni_name"><a href="${courseDetailList.courseDetailUrl}" target="_blank">${courseDetailList.courseTitle}</a></div>
				<c:if test="${not empty courseDetailList.entryRequirments}">							
					<div class="cpc_crs_enrq fl_w100">Entry requirements</div>
					<div class="cpc_crs_cnt fl_w100">${courseDetailList.entryRequirments}</div>
				</c:if>
			  </div>  
					<c:set var="collegeName" value="${courseDetailList.collegeName}"/>
					<%gaCollegeName = commonFunction.replaceSpecialCharacter((String)pageContext.getAttribute("collegeName"));%>
					  <c:set var="gaCollegeName" value="<%=gaCollegeName%>"/>
					<div class="btns fl_w100" id="intBtn">					  
                       <c:if test="${not empty courseDetailList.subOrderItemId}">            
		                  <c:if test="${courseDetailList.subOrderItemId ne 0}">
		                    <c:set var="subOrderId1" value="${courseDetailList.subOrderItemId}"/>         
		                    <c:if test="${not empty courseDetailList.emailWebForm}">
		                     <div class="sremail">
		                      <a rel="nofollow" 
		                         target="_blank"
		                         class="req-inf"
		                         onclick="GAInteractionEventTracking('emailwebform', 'interaction', 'email webform', '${gaCollegeName}', ${courseDetailList.webformprice}); cpeEmailWebformClick(this,'${courseDetailList.collegeId}','${courseDetailList.subOrderItemId}','${courseDetailList.networkId}','${courseDetailList.emailWebForm}'); var a='s.tl(';" 
		                         href="${courseDetailList.emailWebForm}" 
		                         title="Email ${courseDetailList.collegeNameDisplay}">
		                         Request info<i class="fa fa-caret-right"></i>
		                      </a>
		                     </div> 
		                     </c:if>
		                     <c:if test="${not empty courseDetailList.email}">
			                     <c:if test="${empty courseDetailList.emailWebForm}">
			                       <div class="sremail">
			                        <a rel="nofollow"
			                           target="_blank"
			                           class="req-inf"
			                           onclick="GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '${gaCollegeName}');"                
			                            href="<SEO:SEOURL pageTitle="sendcollegemail" >${courseDetailList.collegeName}#${courseDetailList.collegeId}#${courseDetailList.courseId}#0#n#${courseDetailList.subOrderItemId}</SEO:SEOURL>"
			                           title="Email ${courseDetailList.collegeNameDisplay}" >
			                           Request info<i class="fa fa-caret-right"></i>
			                        </a>
			                        </div>
			                     </c:if>
		                     </c:if>
		                     <c:if test="${not empty courseDetailList.prospectusWebForm}">
		                      <div class="srprospectus">
		                      <a rel="nofollow" 
		                         target="_blank" 
		                         class="get-pros" 
		                         onclick="GAInteractionEventTracking('prospectuswebform', 'interaction', 'prospectus webform', '${gaCollegeName}', ${courseDetailList.webformprice}); cpeProspectusWebformClick(this,'${courseDetailList.collegeId}','${courseDetailList.subOrderItemId}','${courseDetailList.networkId}','${courseDetailList.prospectusWebForm}'); var a='s.tl(';" 
		                         href="${courseDetailList.prospectusWebForm}" 
		                         title="Get ${courseDetailList.collegeNameDisplay} Prospectus">
		                         Get prospectus<i class="fa fa-caret-right"></i></a>
		                      </div>   
		                     </c:if>
		                     <c:if test="${not empty courseDetailList.prospectus}">
		                      <c:if test="${empty courseDetailList.prospectusWebForm}">
		                       <div class="srprospectus">
		                       <a onclick="GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '${gaCollegeName}'); return prospectusRedirect('${CONTEXT_PATH}','${courseDetailList.collegeId}','${courseDetailList.courseId}','','','${courseDetailList.subOrderItemId}');"
		                          class="get-pros" title="Get ${courseDetailList.collegeNameDisplay} Prospectus" >
		                          Get prospectus<i class="fa fa-caret-right"></i></a>
		                       </div>   
		                      </c:if>
		                     </c:if>
		                     <c:if test="${not empty courseDetailList.website}">
		                      <div class="srwebsite">
		                     <a rel="nofollow" 
		                         target="_blank" 
		                         class="visit-web" 
		                         id="crs_${row.index}_${courseDetailList.collegeId}"
		                         onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', ${courseDetailList.websitePrice}); cpeWebClickWithCourse(this,'${courseDetailList.collegeId}','${courseDetailList.courseId}','${courseDetailList.subOrderItemId}','${courseDetailList.networkId}','${courseDetailList.website}');addBasket('${courseDetailList.courseId}', 'O', 'visitweb','basket_div_${courseDetailList.courseId}', 'basket_pop_div_${courseDetailList.courseId}', '', '', '${gaCollegeName}');var a='s.tl(';" 
		                         href="${courseDetailList.website}&courseid=${courseDetailList.courseId}"
		                         title="${courseDetailList.collegeNameDisplay} website">
		                         Visit website<i class="fa fa-caret-right"></i></a>
		                      </div>   
		                     </c:if>
		                   </c:if>
                   		</c:if>    
                   <c:set var="opendayscount" value="${courseDetailList.totalOpenDay}"/>
                    
                     <c:if test="${not empty courseDetailList.totalOpenDay}">
                    
                       <c:if test="${courseDetailList.totalOpenDay gt 0}">
                        <c:if test="${not empty courseDetailList.subOrderItemId}">
	                        <c:if test="${courseDetailList.totalOpenDay eq 1}">
	                           <div class="sropendays">
	                            <a rel="nofollow"
	                                class="req-inf sr_opd"
	                                target="_blank"
	                                onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Reserve A Place', '${gaCollegeName}');
	                                         addOpenDaysForReservePlace('<c:out value="${courseDetailList.openDay}"/>', '<c:out value="${courseDetailList.openMonthYear}"/>', '<c:out value="${courseDetailList.collegeId}"/>', '<c:out value="${courseDetailList.subOrderItemId}"/>', '<c:out value="${courseDetailList.networkId}"/>'); 
	                                         cpeODReservePlace(this,'<c:out value="${courseDetailList.collegeId}"/>','<c:out value="${courseDetailList.subOrderItemId}"/>','<c:out value="${courseDetailList.networkId}"/>','<c:out value="${courseDetailList.bookingURL}"/>');"
	                                href="<c:out value="${courseDetailList.bookingURL}"/>"
	                                title="Book <c:out value="${courseDetailList.collegeNameDisplay}"/> open day" >
	                                VIEW ${courseDetailList.totalOpenDay} OPEN DAY<i class="fa fa-caret-right"></i>
	                             </a>   
	                           </div>
	                       </c:if>
	                       <c:if test="${courseDetailList.totalOpenDay ne 1}">
	                         <div class="sropendays">
	                           <a rel="nofollow"
	                              class="req-inf sr_opd"
	                              onclick="GAInteractionEventTracking('visitwebsite', 'engagement', 'Book Open Day Request', '${gaCollegeName}');showOpendayPopup('${courseDetailList.collegeId}', '');" 
	                              title="Book <c:out value="${courseDetailList.collegeNameDisplay}"/> open day" >
	                              VIEW ${courseDetailList.totalOpenDay} OPEN DAYS<i class="fa fa-caret-right"></i>
	                             </a>   
	                           </div>
	                       </c:if>
                        </c:if> 
                     </c:if>
                     </c:if>
                     <c:if test="${courseDetailList.viewCoursesFlag eq 'Y'}">  
						<div class="viewallcurse">
							<a rel="nofollow" class="viewall_crs"	
							 target="_blank"						  
							 title="View Course" 
							 href="${courseDetailList.courseDetailUrl}">
							VIEW COURSE <i class="fa fa-caret-right"></i>
							</a> 
						</div>
						</c:if>
					  </div>
					
					</div>
				  </div>
				  </c:forEach>
				</c:if>
						
			</div>
		</div>
							
				<div class="pr_pagn">
	                <jsp:include page="/jsp/cpc/include/pagination.jsp">
	                  <jsp:param name="pageno" value="<%=pageno%>"/>
	                  <jsp:param name="pagelimit"  value="10"/>
	                  <jsp:param name="searchhits" value="<%=courseCount%>"/>
	                  <jsp:param name="recorddisplay" value="<%=recorddisplay%>"/>              
	                  <jsp:param name="pageURL" value="<%=firstPageSeoUrl%>"/>
	                  <jsp:param name="orderBy" value=""/>
	                  <jsp:param name="pageTitle" value=""/>
	                </jsp:include> 
                </div>
             <c:if test="${not empty courseCount and courseCount gt 0}">				
				<div class="fl_w100"><div class="ttl_pge"><%=fromCourseRecordCount%>-<%=endCourseRecordCount %> of <%=searchhits%> ${courseCount eq '1' ? 'course' : 'courses' }</div></div>
			</c:if>
</c:when>
<c:otherwise>
<div class="cpc_uni_wrap"><div class="fnt_16 txt_cnr no_rslt mt40">Oops! We couldn't find any courses matching your search criteria. Please change your search options and try again</div></div>
</c:otherwise>
</c:choose>	
<input type="hidden" id="selectedSubjectCode" value="${selectedSubjectCode}"/>
<input type="hidden" id="selectedSubjectName" value="${selectedSubjectName}"/>				
<input type="hidden" id="selectedSortCode" value="${selectedSortCode}"/>
<input type="hidden" id="sortName" value="${sortName}"/>
<input type="hidden" id="courseCountId" value="${courseCount}"/>
<input type="hidden" id="preview" value="${preview}"/>				