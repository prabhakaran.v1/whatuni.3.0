
<%@page import="WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator" %>
<%
 // IN Parameters from JSP:Include
  int pageno =1;           // initial page or page selected   
  int pagelimit =0;       // Display number of links in the page
  int searchhits=00;      // Stores total number of records
  int recorddisplay=0;     // To display number of records in the page
  String pageName = "";           // initial page or page selected   
  String action=null;  
  String pageTitle = "";  
  String pageUrl = "";
  String orderBy = "";
  //String moreReviews="";
  // To Store the url to be attach with hyperlink
  // END of IN Parameters from JSP:Include
  int numofpages =0;        // local variable Stores total number of pages 
  int pagenum =0;          // local variables
  int lower =0;             // local variables
  int upper =0;             // local variables  
 // Reads the In Parameter from the JSP include action and assign to the respective variable  
 if(request.getParameter("pageno")!=null){
  pageno = Integer.parseInt(request.getParameter("pageno"));
 } 
 if(request.getParameter("page")!=null){
  pageName = request.getParameter("page");
 }
 if(request.getParameter("pagelimit")!=null){
  pagelimit = Integer.parseInt(request.getParameter("pagelimit"));
 }  
 if(!GenericValidator.isBlankOrNull(request.getParameter("searchhits"))){
  searchhits = Integer.parseInt(request.getParameter("searchhits"));
 }  
 if(request.getParameter("recorddisplay")!=null){
  recorddisplay = Integer.parseInt(request.getParameter("recorddisplay"));
 }   
 if(request.getParameter("pageTitle")!=null){
  pageTitle = request.getParameter("pageTitle");
 }  
 if(request.getParameter("pageURL")!=null){
  pageUrl = request.getParameter("pageURL");
 }  
 
 if(request.getParameter("orderBy")!=null && !"".equals(request.getParameter("orderBy"))){
  orderBy = "orderby="+request.getParameter("orderBy");
 }
 
 if(request.getParameter("pageno")!=null){   
   if(pageUrl.indexOf("?")!=-1){
     pageUrl = pageUrl + (!"".equals(orderBy) ? "&"+orderBy : "") + "&pageno";     
   }else{
     pageUrl = pageUrl + (!"".equals(orderBy) ? "?"+orderBy+"&" : "?") + "pageno";     
   }   
 }
 //?keyword= &subjectId= &pageNo=1
// end of assign value to the respective variable  
// Check to see if this is the first page If it is not the first page, show the "prev" link
// Build the link back to this routine. Pass the session, user, search and page number
//<!--If user in results no 10 or more then print this previous page link-->

  String hlink="";
  String ppath=request.getContextPath();
  if(pageTitle !=null && pageTitle.trim().equalsIgnoreCase("csearch")) {
    ppath = ppath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE; 
  } 
  if(pageTitle !=null && pageTitle.trim().equalsIgnoreCase("page")) {
    ppath = ppath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE; 
  }
  ppath=ppath;     
  
  numofpages = (searchhits / recorddisplay);
  if(searchhits%recorddisplay>0) { 
    numofpages++; 
  }  
  pagenum = (int)( (pageno - 1) / pagelimit);
  pagenum =  pagenum + 1;
  lower =  ((pagenum-1)*pagelimit)+1;
  upper = java.lang.Math.min(numofpages, (((pageno - 1) / pagelimit)+1) * pagelimit);
  if((pageno%pagelimit) == 0) {
    lower = pageno;
    upper = java.lang.Math.min(numofpages, pageno+pagelimit);
  } 
  int prevPage = pageno - 1;
  int nextPage = pageno + 1;
  //arrow svg icon
  String leftArrowSVG ="<svg width=\"16px\" height=\"16px\" viewBox=\"0 0 16 16\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\r\n" + 
	  		"										<!-- Generator: Sketch 60.1 (88133) - https://sketch.com -->\r\n" + 
	  		"										<title>Previous Page</title>\r\n" + 
	  		"										<desc>Created with Sketch.</desc>\r\n" + 
	  		"										<g id=\"Arrow\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\r\n" + 
	  		"											<g id=\"Button/Large---Left-Icon-Copy-5\" transform=\"translate(-3.000000, -142.000000)\" fill=\"#00BBFD\" fill-rule=\"nonzero\">\r\n" + 
	  		"												<g id=\"Icon/Arrow---Left\" transform=\"translate(3.000000, 142.000000)\">\r\n" + 
	  		"													<polygon id=\"Fill\" points=\"7.13333333 12.4666667 8.06666667 11.5333333 5.2 8.66666667 13.3333333 8.66666667 13.3333333 7.33333333 5.2 7.33333333 8.06666667 4.46666667 7.13333333 3.53333333 2.66666667 8\"></polygon>\r\n" + 
	  		"												</g>\r\n" + 
	  		"											</g>\r\n" + 
	  		"										</g>\r\n" + 
	  		"									</svg>";
	String rightArrowSVG ="<svg width=\"16px\" height=\"16px\" class=\"rotate\" viewBox=\"0 0 16 16\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\r\n" + 
			"										<!-- Generator: Sketch 60.1 (88133) - https://sketch.com -->\r\n" + 
			"										<title>Next Page</title>\r\n" + 
			"										<desc>Created with Sketch.</desc>\r\n" + 
			"										<g id=\"Arrow\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">\r\n" + 
			"											<g id=\"Button/Large---Left-Icon-Copy-5\" transform=\"translate(-3.000000, -142.000000)\" fill=\"#00BBFD\" fill-rule=\"nonzero\">\r\n" + 
			"												<g id=\"Icon/Arrow---Left\" transform=\"translate(3.000000, 142.000000)\">\r\n" + 
			"													<polygon id=\"Fill\" points=\"7.13333333 12.4666667 8.06666667 11.5333333 5.2 8.66666667 13.3333333 8.66666667 13.3333333 7.33333333 5.2 7.33333333 8.06666667 4.46666667 7.13333333 3.53333333 2.66666667 8\"></polygon>\r\n" + 
			"												</g>\r\n" + 
			"											</g>\r\n" + 
			"										</g>\r\n" + 
			"									</svg>"; 		
  //	  		
  if(searchhits > recorddisplay){
    if(prevPage==0){
        hlink = hlink+ "<div class=\"artPg\"><ul><li class=\"hid-ph arw_crcl\"><a class=\"in_visible\" disabled=\"disabled\">"+leftArrowSVG+"</a></li>";
    }else{
        if(prevPage == 1){//Added condition to restrict pageno 1 displaying in url
          String firstPage = "";
          firstPage = (pageUrl.indexOf("&pageno") > -1) ? pageUrl.replace("&pageno","") : (pageUrl.indexOf("?pageno") > -1) ? pageUrl.replace("?pageno","") : pageUrl;          
          hlink = hlink+ "<div class=\"artPg\"><ul><li class=\"hid-ph arw_crcl\"><a onclick=\"subjectAjax('','','','"+prevPage+"')\">"+leftArrowSVG+"</a></li>";
        }else{
          hlink = hlink+ "<div class=\"artPg\"><ul><li class=\"hid-ph arw_crcl\"><a onclick=\"subjectAjax('','','','"+prevPage+"')\">"+leftArrowSVG+"</a></li>";
        }
    }
    if(numofpages > 1){
      for(int i=lower; i<=upper; i++) {
        if(i == pageno) {
          hlink =hlink +"<li class=\"hid-ph active\"><a disabled=\"disabled\">"+i+"</a></li>";
        } else {
          if(i == 1){//Added condition to restrict pageno 1 displaying in url
            String firstPage = "";
            firstPage = (pageUrl.indexOf("&pageno") > -1) ? pageUrl.replace("&pageno","") : (pageUrl.indexOf("?pageno") > -1) ? pageUrl.replace("?pageno","") : pageUrl;          
            hlink =hlink +"<li class=\"hid-ph\"><a onclick=\"subjectAjax('','','','"+i+"')\">"+i+"</a></li>";
          }else{
            hlink =hlink +"<li class=\"hid-ph\"><a onclick=\"subjectAjax('','','','"+i+"')\">"+i+"</a></li>";
          }
                 
        }
      } 
    }
    // Check to see if this is the last page, If it is not the last page, show the "next" link
    // Build the link back to this routine Pass the session, user, search and page number
    int lastpage = numofpages+1;
    if(nextPage == lastpage) {  
      hlink = hlink+ "<li class=\"hid-ph arw_crcl\"><a class=\"mr0 in_visible\" disabled=\"disabled\">"+rightArrowSVG+"</a></li></ul></div>";    
    }else{
      hlink = hlink+ "<li class=\"hid-ph arw_crcl\"><a class=\"mr0\" onclick=\"subjectAjax('','','','"+nextPage+"')\">"+rightArrowSVG+"</a></li></ul></div>";
    }  
    if(hlink!=null&&!(hlink.equals(""))) {
      out.println(hlink);  
    }
  }
%>