<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants"%>

<%String lazyLoadNotDoneFlag = GlobalConstants.LAZY_LOAD_FLAG;
  String cpcPlayIcon = CommonUtil.getImgPath("/wu-cont/images/cpc_play_icon.svg",0);
  CommonUtil util = new CommonUtil(); %>
  
<c:if test="${not empty providerDetailList}">
<c:forEach var="uniDetails" items="${providerDetailList}" varStatus="id">  
<section class="slider_cnt" id="heroslider">
  <div class="hm_ldicon" id="load-icon" style="display:none"> <img src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif", 1)%>"> </div>
  <div class="hrosrch_ui fl_w100">
 	<div class="overly"></div>
      <c:set var="imagePath" value="${uniDetails.heroImagePath}"/>
                <%String imagePath = (String)pageContext.getAttribute("imagePath");
                  imagePath = util.getImageUrl(request, imagePath, GlobalConstants.CPC_HERO_MOB_IMGSIZE, GlobalConstants.CPC_HERO_IMGSIZE, GlobalConstants.CPC_HERO_IMGSIZE);%>
      <div id="homeHeroImgDivId" class="hrobanr_ui fl_w100" data-rjs="Y" data-src="<%=CommonUtil.getImgPath(imagePath, 1)%>" style="background: url('<%=CommonUtil.getImgPath(imagePath, 1)%>') center top no-repeat;" data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>">
        <div class="srch_cnt fl_w100">
           <div class="hrohd_ui fl_w100" data-src="<%=CommonUtil.getImgPath(imagePath, 1)%>">
		     <div class="cpc_uni_logo">
			   <c:set var="collegeLogo" value="${uniDetails.collegeLogo}"/>
               <%String collegeLogo = (String)pageContext.getAttribute("collegeLogo");%>
			   <img height="80" width="80" src="<%=CommonUtil.getImgPath(collegeLogo, 1)%>" class="cpc_uni_img" alt="Provider logo" onclick="ajaxCpcLightBox('${uniDetails.collegeId}','SINGLE');">
			  </div>
              <h1>${uniDetails.collegeName}</h1>
             <c:if test="${uniDetails.overAllRating gt 0}">
              <div class="uni_rat" onclick="ajaxCpcLightBox('${uniDetails.collegeId}','SINGLE');">
				<div class="pop_uni_rat">
				  <span class="rat rat${uniDetails.overAllRating}"></span>
                  <span class="rtx">(${uniDetails.overAllRatingExact})</span> 
                  <c:if test="${not empty uniDetails.reviewCount}">
			     	<a class="link_blue rv_cnt">
			        <c:if test="${not empty uniDetails.reviewCount}">
				      <c:if test="${uniDetails.reviewCount ne 0}">
				        ${uniDetails.reviewCount}
                        <c:if test="${uniDetails.reviewCount gt 1}">
                          reviews
                        </c:if>
				        <c:if test="${uniDetails.reviewCount eq 1}">  
                          review
                        </c:if>
				      </c:if>
                    </c:if>
                    </a>
                  </c:if> 
				</div>
				 </div>
				 </c:if>
			   <div class="fl_w100">
				 <div class="uni_discp">
				   <h2>${uniDetails.subTitle}</h2>
	             </div>
			    <div class="cpc_sngl_vall dsktp">
			      <c:if test="${not empty courseCount and courseCount gt 0}">
			      <a onclick="scrollToViewCourses();"><span class="whit_arw"></span>VIEW COURSES</a>
			      </c:if>
		        </div>
		     </div>
           </div>
         </div>
       </div>
     </div>
</section>
 <div class="cpc_mob_vall imob">
 <c:if test="${not empty courseCount and courseCount gt 0}">	
   <a onclick="scrollToViewCourses();"><span class="blue_arw"></span>VIEW COURSES</a>
 </c:if> 
 </div>
 <section class="ltst_art">
   <div class="content-bg">
   
	 <div class="cpc_sngl_vid_pod ${(uniDetails.editorialMediaType eq 'IMAGE') || (uniDetails.editorialMediaType eq 'VIDEO') ? '' : 'txt_wid' }  ">
	   <div class="cpc_sngl_vid">
        <c:if test="${uniDetails.editorialMediaType eq 'IMAGE'}">
          <c:set var="editorialImagePath" value="${uniDetails.editorialMediaPath}"/>
          <%String editorialImagePath = (String)pageContext.getAttribute("editorialImagePath"); %>
								  <c:choose>
                                    <c:when test="${uniDetails.contentHubMediadFlag eq 'Y'}">
                                      <% editorialImagePath = util.getImageUrl(request, editorialImagePath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>
                                      <img width="100%" height="auto" src="<%=CommonUtil.getImgPath(editorialImagePath, 1)%>"></img>
                                    </c:when>
                                    <c:otherwise>
                                      <% editorialImagePath = util.getImageUrl(request, editorialImagePath, GlobalConstants.CPC_MOB_IMGSIZE, GlobalConstants.CPC_TAB_IMGSIZE, GlobalConstants.CPC_DESKTOP_IMGSIZE); %>
                                      <img width="100%" height="auto" src="<%=CommonUtil.getImgPath(editorialImagePath, 1)%>" data-src="<%=CommonUtil.getImgPath(editorialImagePath, 1)%>" data-rjs="Y"  data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>">
                                    </c:otherwise>
                                  </c:choose>                       
	    </c:if>
		<c:if test="${uniDetails.editorialMediaType eq 'VIDEO'}">
	                <c:if test="${not empty uniDetails.editorialMediaPath}">
		              <c:set var="thumbnailPath" value="${uniDetails.thumbnailPath}"/>
                      <%String thumbnailPath = (String)pageContext.getAttribute("thumbnailPath");%>
                      <div class="vide_icn"><img class="pl_icn_img" src="<%=cpcPlayIcon%>" id="cpc_play_icon" onclick="clickPlayAndPause('cpc_libox_video', event);" alt="play icon">
                      </div>                      
								  <c:choose>
                                    <c:when test="${uniDetails.contentHubMediadFlag eq 'Y'}">
                                      <% thumbnailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>
                                      <img width="100%" id="thumbnailImg" height="auto" src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>"></img>
                                    </c:when>
                                    <c:otherwise>
                                      <% thumbnailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants.CPC_MOB_IMGSIZE, GlobalConstants.CPC_TAB_IMGSIZE, GlobalConstants.CPC_DESKTOP_IMGSIZE); %>
                                      <img width="100%" id="thumbnailImg" height="auto" src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-rjs="Y" data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>">
                                    </c:otherwise>
                                  </c:choose>    
                      
		              <video onclick="clickPlayAndPause('cpc_libox_video', event);" preload="none" id="cpc_libox_video" width="100%" height="100%"
				        data-media-id="${uniDetails.mediaId}" style="display:none"
				        controlslist="nodownload noremoteplayback" disablepictureinpicture="">
				        <source src="${uniDetails.editorialMediaPath}" type="video/mp4">				        
			          </video>
                      </c:if>
	       </c:if>
	     </div>
		 <div class="cpc_txt_wrap">
		   ${uniDetails.editorialContent}
	     </div>	   
	 </div>
   </section>
  </c:forEach>
 </c:if>