<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.GlobalConstants"%>

<%String link = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName(); %>
 <div class="hdnav_ui fl_w100">
                            <div id="header" class="hrdr">
                                <div class="cookins" id="cookiePopup" style="display: none;">
                                    <div class="ad_cnr">
                                        <div class="content-bg">
                                            <div class="ck_cnt">
                                                <p class="ckie_wrp"><span>We use cookies to ensure the best user experience and to serve tailored advertising. To learn more about our cookies and how to manage them, please visit our</span><a href="/degrees/cookies.html">Cookie Policy</a></p> <a href="javascript:void(0);" class="ckbubtn" onclick="createHeaderCookie('cookie_splash_flag','N')">I AGREE</a> </div>
                                        </div>
                                    </div>
                                </div> 
                                  <c:if test="${'Y' eq hamBurgerFlag}">
                                    <a title="Show navigation" class="navbar ui-link" id="navbar">
                                      <i class="fa fa-bars" id="fa_bars"></i>
                                      <span class="mnu_clse"></span>
                                    </a>
                                  </c:if>
                                  <c:if test="${'Y' eq whatuniLogoFlag}">
                                    <div class="un_logo">
                                      <a href="<%=link%>" target="_blank" title="Whatuni home"></a>
                                    </div>
                                  </c:if>
                                <input type="hidden" name="loginFrom" id="loginFrom" value="N">
                                <input type="hidden" name="openDayProviderName" id="openDayProviderName" value="">
                                <input type="hidden" id="hide_Menu" value="N">
                            </div>
                            <div id="hdr_menu" class="navi_mnu fl_w100">
                                <!--Menu list-->
                                <nav class="hdr_menu mycmp_menu">
                                    <ul>
                                        <li><a id="clearDivId" class="fnt_lbd"target="_blank" href="/degrees/courses/"><spring:message code="find.course.menu.in.nav.bar"/></a> </li>
                                        <li><a class="fnt_lbd" target="_blank" href="/degrees/find-university/"><spring:message code="find.uni.menu.in.nav.bar"/></a></li>
                                        <li><a class="fnt_lbd" target="_blank" href="/degrees/prospectus/"><spring:message code="prospectuses.menu.in.nav.bar"/></a></li>
                                        <li><a class="fnt_lbd" target="_blank" href="/open-days/"><spring:message code="open.days.menu.in.nav.bar"/></a></li>
                                        <li id="reviewTabChg">
                                          <a id="click_view1" class="fnt_lbd mob_adv_act noalert subNavDkLink"><spring:message code="reviews.menu.in.nav.bar"/><span id="click_view1_span" class="mnuicn"></span></a>
                                            <div class="urws" id="" style="display:none">
                                                <ul class="mnite_cnt2">
                                                    <li><a class="subNavDkLink" target="_blank" href="/university-course-reviews/"><spring:message code="read.review.sub.menu.in.nav.bar"/></a></li>
                                                    <li><a class="subNavDkLink" target="_blank" href="/university-course-reviews/add-review/"><spring:message code="write.review.sub.menu.in.nav.bar"/></a></li>
                                                    <li><a class="subNavDkLink" target="_blank" href="/student-awards-winners/university-of-the-year/"><spring:message code="wusca.winner.sub.meni.in.nav.bar"/></a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li id="adviceTabChg"><a id="click_view2" class="fnt_lbd mob_adv_act noalert subNavDkLink"><spring:message code="advice.menu.in.nav.bar"/><span id="click_view2_span" class="mnuicn"></span></a>
                                        
                                            <div class="urws" id="" style="display:none">
                                                <ul class="mnite_cnt2">
                                                  <c:forEach var="articlesList" items="${articlesList}">
                                                     <li>
                                                       <a href="${articlesList.blogUrl}" target="_blank" class="subNavDkLink" title="${articlesList.blogCategoryName}">${articlesList.blogCategoryName}</a>
                                                     </li>
                                                      
                                                  </c:forEach>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </nav>
                            </div>

                            <section class="timeline">
                                <a href="javascript:hideNonLoggedTimeline()" class="tab_arw" id="hideTimeLine"></a>
                                <div class="content-bg" id="nonloggedtimeline" onclick="javaScript:showLightBoxLoginForm('popup-newlogin',650,500, 'nonLoggedTimeline','','non-loggedin-timeline');">
                                    <div class="row-fluid">
                                        <div class="container">
                                            <article class="tml fl"></article>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <input type="hidden" name="gaAccount" value="UA-22939628-2" id="gaAccount">
                            <input type="hidden" id="contextJsPath" value="https://js.content-hcs.com/wu-cont">
                            <input type="hidden" id="clearingYear" value="Clearing 2019">
                            <input type="hidden" id="headerClass" value="hrdr">
                            <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/mychoice/myFinalChoice_wu565.js"></script>
                            <script type="text/javascript">
                                var vulnerableKeywords = ["onclick", "`", "{", "}", "~", "|", "^", "http:", "javascript:", "https:", "ftp:", "type='text/javascript'", "type=\"text/javascript\"", "language='javascript'", "language=\"javascript\""];
                                var htmlTags = ["<!DOCTYPE", "<!--", "<acronym", "<abbr", "<address", "<applet", "<area", "<article", "<aside", "<audio", "<a", "<big", "<bdi", "<basefont", "<base", "<bdo", "<blockquote", "<body", "<br", "<button", "<b", "<center", "<canvas", "<caption", "<cite", "<code", "<colgroup", "<col", "<dir", "<datalist", "<dd", "<del", "<details", "<dfn", "<dialog", "<div", "<dl", "<dt", "<embed", "<em", "<frameset", "<frame", "<font", "<fieldset", "<figcaption", "<figure", "<footer", "<form", "<header", "<head", "<h1", "<h2", "<h3", "<h4", "<h5", "<h6", "<hr", "<html", "<iframe", "<img", "<ins", "<input", "<i", "<kbd", "<keygen", "<label", "<legend", "<link", "<li", "<main", "<map", "<mark", "<menuitem", "<menu", "<meta", "<meter", "<noscript", "<noframes", "<nav", "<object", "<ol", "<optgroup", "<option", "<output", "<param", "<\pre", "<progress", "<p", "<q", "<rp", "<rt", "<ruby", "<svg", "<samp", "<script", "<section", "<select", "<small", "<strike", "<source", "<span", "<strong", "<style", "<sub", "<summary", "<sup", "<s", "<table", "<thead", "<tbody", "<tfoot", "<tt", "<td", "<th", "<tr", "<text\area", "<time", "<title", "<track", "<ul", "<u", "<var", "<video", "<wbr", "<\/acronym", "<\/abbr", "<\/address", "<\/applet", "<\/area", "<\/article", "<\/aside", "<\/audio", "<\/a", "<\/big", "<\/bdi", "<\/basefont", "<\/base", "<\/bdo", "<\/blockquote", "<\/body", "<\/br", "<\/button", "<\/b", "<\/center", "<\/canvas", "<\/caption", "<\/cite", "<\/code", "<\/colgroup", "<\/col", "<\/dir", "<\/datalist", "<\/dd", "<\/del", "<\/details", "<\/dfn", "<\/dialog", "<\/div", "<\/dl", "<\/dt", "<\/embed", "<\/em", "<\/frameset", "<\/frame", "<\/font", "<\/fieldset", "<\/figcaption", "<\/figure", "<\/footer", "<\/form", "<\/header", "<\/head", "<\/h1", "<\/h2", "<\/h3", "<\/h4", "<\/h5", "<\/h6", "<\/hr", "<\/html", "<\/iframe", "<\/img", "<\/ins", "<\/input", "<\/i", "<\/kbd", "<\/keygen", "<\/label", "<\/legend", "<\/link", "<\/li", "<\/main", "<\/map", "<\/mark", "<\/menuitem", "<\/menu", "<\/meta", "<\/meter", "<\/noscript", "<\/noframes", "<\/nav", "<\/object", "<\/ol", "<\/optgroup", "<\/option", "<\/output", "<\/param", "<\/pre", "<\/progress", "<\/p", "<\/q", "<\/rp", "<\/rt", "<\/ruby", "<\/svg", "<\/samp", "<\/script", "<\/section", "<\/select", "<\/small", "<\/strike", "<\/source", "<\/span", "<\/strong", "<\/style", "<\/sub", "<\/summary", "<\/sup", "<\/s", "<\/table", "<\/thead", "<\/tbody", "<\/tfoot", "<\/tt", "<\/td", "<\/th", "<\/tr", "<\/textarea", "<\/time", "<\/title", "<\/track", "<\/ul", "<\/u", "<\/var", "<\/video", "<\/wbr"];
                            </script>
                        </div>