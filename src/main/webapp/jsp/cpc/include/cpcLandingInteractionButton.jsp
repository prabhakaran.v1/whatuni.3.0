<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<c:forEach var="searchResult" items="${listOfSearchResults}" varStatus="i">
 <c:if test="${not empty searchResult.bestMatchCoursesList}">
     <c:set var="bestMatchCourseList" value="${searchResult.bestMatchCoursesList}"/>
<c:forEach var="bestMatchCourse" items="${bestMatchCourseList}" end="1">
<div class="btns fl_w100" id="intBtn">
<c:if test="${not empty bestMatchCourse.subOrderItemId}">
                   <c:if test="${bestMatchCourse.subOrderItemId eq 0}">                     
                            <div class="sremail">
                          <a rel="nofollow" 
                              target="_blank"
                              class="req-inf"
                              onclick=""
                              title="Email ${searchResult.collegeNameDisplay}">
                            Request info<i class="fa fa-caret-right"></i>
                          </a>
                          </div> 
                          </c:if>
                          </c:if>
                          
                          <c:if test="${empty bestMatchCourse.subOrderItemId}">         
                          <div class="sremail">
                          <a rel="nofollow" 
                              target="_blank"
                              class="req-inf"
                              onclick=""
                              title="Email ${searchResult.collegeNameDisplay}">
                            Request info<i class="fa fa-caret-right"></i>
                          </a>
                          </div>
                      </c:if>
                       <c:if test="${not empty bestMatchCourse.subOrderItemId}">            
                  <c:if test="${bestMatchCourse.subOrderItemId ne 0}">
                  <c:set var="subOrderId1" value="${bestMatchCourse.subOrderItemId}"/>        
                     
                     <c:choose>
                     
                     <c:when test="${searchPageType eq 'CLEARING'}">
                      <c:if test="${clearingonoff eq 'ON'}">
                        <div class="srwebsite">
                        <c:if test="${not empty bestMatchCourse.subOrderWebsite}">
                        <c:set var="visitwebsitelink" value="${bestMatchCourse.subOrderWebsite}"/>  
                            
                            <a rel="nofollow" 
                                target="_blank" 
                                class="visit-web"
                                id="${collegeId}" 
                                onclick=""  
                                href="${bestMatchCourse.subOrderWebsite}&courseid=${courseId2}"
                                title="${searchResult.collegeNameDisplay} website"> Visit website
                                <i class="fa fa-caret-right"></i></a>
                          </c:if>
                          <c:if test="${not empty bestMatchCourse.hotLineNo}">
                         
                          <c:set var="hotline1" value="${bestMatchCourse.hotLineNo}"/>
                           
                            <a class="clr-call mr17" onclick=""><i class="fa fa-phone"></i>${bestMatchCourse.hotLineNo}</a>
                          </c:if>
                        </div>
                     </c:if> 
                      </c:when>
                      <c:otherwise>
                    <c:if test="${not empty bestMatchCourse.subOrderEmailWebform}">
                     <div class="sremail"><%--Changed event action webclick to webform by Sangeeth.S for July_3_18 rel--%>
                      <a rel="nofollow" 
                         target="_blank"
                         class="req-inf"
                         onclick="GAInteractionEventTracking('emailwebform', 'interaction', 'email webform', '${gaCollegeName}', ${bestMatchCourse.webformPrice}); cpeEmailWebformClick(this,'${collegeId}','${bestMatchCourse.subOrderItemId}','${bestMatchCourse.cpeQualificationNetworkId}','${bestMatchCourse.subOrderEmailWebform}'); var a='s.tl(';" 
                         href="${bestMatchCourse.subOrderEmailWebform}" 
                         title="Email ${searchResult.collegeNameDisplay}">
                         Request info<i class="fa fa-caret-right"></i>
                      </a>
                     </div> 
                     </c:if>
                     <c:if test="${not empty bestMatchCourse.subOrderEmail}">
                   <c:if test="${empty bestMatchCourse.subOrderEmailWebform}">
                       <div class="sremail">
                        <a rel="nofollow"
                           class="req-inf"
                           onclick="GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '${gaCollegeName}');adRollLoggingRequestInfo('${collegeId}',this.href);return false;"                
                            href="<SEO:SEOURL pageTitle="sendcollegemail" >${searchResult.collegeName}#${searchResult.collegeId}#${bestMatchCourse.courseId}#${keywordorsubjectemail}#n#${bestMatchCourse.subOrderItemId}</SEO:SEOURL>"
                           title="Email ${searchResult.collegeNameDisplay}" >
                           Request info<i class="fa fa-caret-right"></i>
                        </a><%--13-JAN-2015  Added for adroll marketing--%>   
                        </div>
                    </c:if>
                     </c:if>
                     <c:if test="${not empty bestMatchCourse.subOrderProspectusWebform}">
                      <div class="srprospectus">
                      <a rel="nofollow" 
                         target="_blank" 
                         class="get-pros" 
                         onclick="GAInteractionEventTracking('prospectuswebform', 'interaction', 'prospectus webform', '${gaCollegeName}', ${bestMatchCourse.webformPrice}); cpeProspectusWebformClick(this,'${collegeId}','${bestMatchCourse.subOrderItemId}','${bestMatchCourse.cpeQualificationNetworkId}','${bestMatchCourse.subOrderProspectusWebform}'); var a='s.tl(';" 
                         href="${bestMatchCourse.subOrderProspectusWebform}" 
                         title="Get ${searchResult.collegeNameDisplay} Prospectus">
                         Get prospectus<i class="fa fa-caret-right"></i></a>
                      </div>   
                     </c:if>
                     <c:if test="${not empty bestMatchCourse.subOrderProspectus}">
                      <c:if test="${empty bestMatchCourse.subOrderProspectusWebform}">
                       <div class="srprospectus">
                       <a onclick="GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '${gaCollegeName}'); return prospectusRedirect('${CONTEXT_PATH}','${collegeId}','${courseId2}','${keywordOrSubject}','','${bestMatchCourse.subOrderItemId}');"
                          class="get-pros" title="Get ${searchResult.collegeNameDisplay} Prospectus" >
                          Get prospectus<i class="fa fa-caret-right"></i></a>
                       </div>   
                      </c:if>
                     </c:if>
                     <c:if test="${not empty bestMatchCourse.subOrderWebsite}">
                      <div class="srwebsite">
                     <a rel="nofollow" 
                         target="_blank" 
                         class="visit-web" 
                         id="${collegeId}"
                         onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', ${bestMatchCourse.websitePrice}); cpeWebClickWithCourse(this,'${collegeId}','${courseId2}','${bestMatchCourse.subOrderItemId}','${bestMatchCourse.cpeQualificationNetworkId}','${bestMatchCourse.subOrderWebsite}');addBasket('${courseId2}', 'O', 'visitweb','basket_div_${courseId2}', 'basket_pop_div_${courseId2}', '', '', '${gaCollegeName}');var a='s.tl(';" 
                         href="${bestMatchCourse.subOrderWebsite}&courseid=${courseId2}"
                         title="${searchResult.collegeNameDisplay} website">
                         Visit website<i class="fa fa-caret-right"></i></a>
                      </div>   
                     </c:if>
                     <c:if test="${not empty searchResult.totalOpendays}">
                       <c:if test="${searchResult.totalOpendays gt 0}">
                      <c:if test="${not empty searchResult.opendaySuborderItemId}">
                        <c:if test="${searchResult.totalOpendays eq 1}">
                           <div class="sropendays">
                            <a rel="nofollow"
                                class="req-inf sr_opd"
                                target="_blank"
                                onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Reserve A Place', '${gaCollegeName}');
                                         addOpenDaysForReservePlace('${searchResult.openDate}', '${searchResult.openMonthYear}', '${searchResult.collegeId}', '${searchResult.opendaySuborderItemId}', '${searchResult.networkId}'); 
                                         cpeODReservePlace(this,'${searchResult.collegeId}','${searchResult.opendaySuborderItemId}','${searchResult.networkId}','${searchResult.bookingUrl}');" 
                                href="${searchResult.bookingUrl}"
                                title="Book ${searchResult.collegeNameDisplay} open day" >
                                OPEN DAYS<i class="fa fa-caret-right"></i>
                             </a>   
                           </div>
                       </c:if>
                       <c:if test="${searchResult.totalOpendays ne 1}">
                         <div class="sropendays">
                           <a rel="nofollow"
                              class="req-inf sr_opd"
                              onclick="GAInteractionEventTracking('visitwebsite', 'engagement', 'Book Open Day Request', '${gaCollegeName}');showOpendayPopup('${collegeId}', '');" 
                              title="Book ${searchResult.collegeNameDisplay} open day" >
                              OPEN DAYS<i class="fa fa-caret-right"></i>
                             </a>   
                           </div>
                       </c:if>
                       </c:if>
                   </c:if>
                     </c:if>   
                     </c:otherwise>
                      </c:choose>
                   </c:if>
                   </c:if>    
								<div class="viewallcurse">
									<a rel="nofollow" class="viewall_crs" onclick="GAInteractionEventTracking('visitwebsite', 'engagement', 'Book Open Day Request', 'Bangor University');showOpendayPopup('3769', '');" title="View XX Courses">
									VIEW XX COURSES<i class="fa fa-caret-right"></i>
									</a> 
								</div>
							</div>
							</c:forEach></c:if></c:forEach>