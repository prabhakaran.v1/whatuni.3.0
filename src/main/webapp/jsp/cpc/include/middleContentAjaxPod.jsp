<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction,WUI.utilities.GlobalConstants,org.apache.commons.validator.GenericValidator"%>
<%String imgpath=""; 
String gaCollegeName ="";
String courseFlag = "PROVIDER";
String courseId = "0";
String CONTEXT_PATH = GlobalConstants.WU_CONTEXT_PATH;
CommonFunction commonFunction = new CommonFunction();
CommonUtil util = new CommonUtil();
String searchhits = (String) request.getAttribute("universityCount");
String pageno = request.getAttribute("pageno") != null ? (String)request.getAttribute("pageno") : "1";
String urldata_1 = (String)request.getAttribute("URL_1");
String urldata_2 = (String)request.getAttribute("URL_2");
String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "NULL";
String universityCount = (request.getAttribute("universityCount") !=null && !request.getAttribute("universityCount").equals("null") && String.valueOf(request.getAttribute("universityCount")).trim().length()>0 ? String.valueOf(request.getAttribute("universityCount")) : ""); 
String resultExists = (request.getAttribute("resultExists") !=null && !request.getAttribute("resultExists").equals("null") && String.valueOf(request.getAttribute("resultExists")).trim().length()>0 ? String.valueOf(request.getAttribute("resultExists")) : ""); 
String recorddisplay = "12";
int pgNo = Integer.parseInt(pageno);
int rcdDisp = 0;
int totCrsCnt = 0;
int fromCourseRecordCount = 0;
int endCourseRecordCount = 0;
if(!GenericValidator.isBlankOrNull(searchhits)){	  
	  pgNo = Integer.parseInt(pageno);
	  rcdDisp = Integer.parseInt(recorddisplay);
	  totCrsCnt = Integer.parseInt(searchhits);
	  fromCourseRecordCount = (pgNo == 1) ? 1 : ((pgNo*rcdDisp) - (rcdDisp -1));
	  endCourseRecordCount = (pgNo*rcdDisp < totCrsCnt) ? pgNo*rcdDisp : totCrsCnt;
	}
  String lazyLoadNotDoneFlag = GlobalConstants.LAZY_LOAD_FLAG;
  String onePixelImgUrl = CommonUtil.getImgPath(GlobalConstants.ONE_PIXEL_IMG_PATH, 1);
%>
<c:set var="CONTEXT_PATH" value="<%=CONTEXT_PATH%>"/>
<c:choose>
<c:when test="${not empty providerDetailsList and universityCount gt 0}">
<div class="cpc_uni_wrap">
					<div class="cpc_uni_pod">
					<c:if test="${not empty providerDetailsList}">
                       <c:forEach var="providerDetailsList" items="${providerDetailsList}" varStatus="i">
						<div class="pop_uni">
							<div class="pop_uni_img fl_w100" id="contentBgId">
								<a class="cl_vcnr" onclick="ajaxCpcLightBox('${providerDetailsList.collegeId}', 'MULTIPLE');">
								<c:if test="${not empty providerDetailsList.thumbnailPath}">
								<c:if test="${providerDetailsList.mediaType eq 'VIDEO'}">
								  <span class="pl_icn"><img class="pl_icn_img" src="<%=CommonUtil.getImgPath("/wu-cont/images/cpc_play_icon.svg",0)%>" alt="play icon"></span>
								  </c:if>
								  <c:set var="thumbnailPath" value="${providerDetailsList.thumbnailPath}"/>
								  <%String thumbnailPath = (String)pageContext.getAttribute("thumbnailPath"); %>
								  <c:choose>
                                    <c:when test="${(providerDetailsList.contentHubMediadFlag eq 'Y' and clearingFlag ne 'Y') or (providerDetailsList.clearingProfileMediaFlag eq 'Y' and clearingFlag eq 'Y')}">
                                      <% thumbnailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants._320PX, GlobalConstants._320PX, GlobalConstants._320PX); %>
                                      <img class="cl_vid" src="<%=onePixelImgUrl%>" data-src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>" alt="${providerDetailsList.collegeNameDisplay}">
                                    </c:when>
                                    <c:otherwise>
                                      <% thumbnailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants.CPC_MOB_IMGSIZE, GlobalConstants.CPC_TAB_IMGSIZE, GlobalConstants.CPC_DESKTOP_IMGSIZE); %>
                                      <img class="cl_vid" src="<%=onePixelImgUrl%>" data-src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-rjs="Y"  data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>" alt="${providerDetailsList.collegeNameDisplay}">
                                    </c:otherwise>
                                  </c:choose>
                                  </c:if>
								</a>
							</div>
							<div class="pop_uni_det fl_w100">
							<div class="pop_uni_logo" id="middleLogoId">
							<a onclick="ajaxCpcLightBox('${providerDetailsList.collegeId}', 'MULTIPLE');">
							<c:if test="${not empty providerDetailsList.collegeLogo}">
        <c:set var="collegeLogoPath" value="${providerDetailsList.collegeLogo}"/>
        <c:set var="loop" value="${i.index}"/>
         <% imgpath=CommonUtil.getImgPath((String)pageContext.getAttribute("collegeLogoPath"),Integer.parseInt(String.valueOf(pageContext.getAttribute("loop"))));%>
							<c:choose>
           <c:when test="${(i.index) >= 1}">
            <img src="<%=onePixelImgUrl%>" data-src="<%=imgpath%>" width="105"  title="${providerDetailsList.collegeNameDisplay}" class="pro_logo" alt="${providerDetailsList.collegeNameDisplay}" data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>"/>          
           </c:when>
           <c:otherwise>
            <img src="<%=onePixelImgUrl%>" data-src="<%=imgpath%>" width="105" title="${providerDetailsList.collegeNameDisplay}" class="ibdr" alt="${providerDetailsList.collegeNameDisplay}" data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>"/>          
         </c:otherwise>
          </c:choose>
					    </c:if>
							</a>
							</div>
                            <c:if test="${clearingFlag eq 'Y'}">
                              <div class="clr20_cpc_lble">IN CLEARING</div>
                            </c:if>
							<div class="cpc_uni_cnt_ht">
							<div class="pop_uni_name"><a href="javascript:void(0);" onclick="ajaxCpcLightBox('${providerDetailsList.collegeId}', 'MULTIPLE');">${providerDetailsList.collegeNameDisplay}</a></div>
							<c:if test="${providerDetailsList.overAllRating gt 0}">  
							<div class="pop_uni_rat fl_w100">
								<span class="rat rat${providerDetailsList.overAllRating} t_tip">
									<span class="cmp">
										<div class="hdf5">This is the overall rating calculated by averaging all live reviews for this uni on Whatuni.</div>
										<div class="line"></div>
									</span>
								</span>
								<span class="rtx">(${providerDetailsList.overAllRatingExact})</span>
								<a class="link_blue rv_cnt" onclick="ajaxCpcLightBox('${providerDetailsList.collegeId}', 'MULTIPLE');">
								<c:if test="${not empty providerDetailsList.reviewCount}">
                                <c:if test="${providerDetailsList.reviewCount ne 0}">
                                 ${providerDetailsList.reviewCount}
                                 <c:if test="${providerDetailsList.reviewCount gt 1}">
                                 reviews
                                 </c:if>
                                 <c:if test="${providerDetailsList.reviewCount eq 1}">  
                                 review
                                 </c:if>
                                 </c:if>
                                 </c:if>
				                 </a>
				               </div>
		                     </c:if>
			                 <c:if test="${providerDetailsList.courseCount ne 0}"><div class="cpc_crs_cnt fl_w100"><c:if test="${providerDetailsList.courseCount eq 1}">${providerDetailsList.courseCount} course</c:if><c:if test="${providerDetailsList.courseCount gt 1}">${providerDetailsList.courseCount} courses</c:if></div></c:if>
			               </div>
						      <%-- <jsp:include page="/jsp/cpc/include/cpcLandingInteractionButton.jsp" /> --%>
						       
               <c:set var="collegeName" value="${providerDetailsList.collegeName}"/>
               <%gaCollegeName = commonFunction.replaceSpecialCharacter((String)pageContext.getAttribute("collegeName"));%>
               <c:set var="gaCollegeName" value="<%=gaCollegeName%>"/>
               <div class="btns fl_w100 ${clearingFlag eq 'Y' ? 'clr_btn_com' : ''}" id="intBtn">
                  <c:choose>
                    <c:when test="${clearingFlag eq 'Y'}">
                      <c:if test="${not empty providerDetailsList.subOrderItemId and providerDetailsList.subOrderItemId gt 0}">
                      <c:if test="${not empty providerDetailsList.hotlineNo}">
                        <div class="call_now">
                          <a rel="nofollow" class="callnow" id="contact_cpclp_${providerDetailsList.collegeId}" onclick="GAInteractionEventTracking('Webclick', 'interaction', 'hotline','${gaCollegeName}');changeContact('${providerDetailsList.hotlineNo}','contact_cpclp_${providerDetailsList.collegeId}');cpeHotlineClearing(this,'${providerDetailsList.collegeId}','${providerDetailsList.subOrderItemId}','${providerDetailsList.networkId}','${providerDetailsList.hotlineNo}','${providerDetailsList.orderItemId}');"> 
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            <spring:message code="call.now.label" />
                          </a>
                       </div>
                     </c:if>
                     <c:if test="${not empty providerDetailsList.website}">
                     <div class="srwebsite">
                     <a rel="nofollow" target="_blank" class="req-inf" id="${providerDetailsList.collegeId}"
                       onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', ${providerDetailsList.websitePrice});cpeWebClickClearing(this,'${providerDetailsList.collegeId}','${providerDetailsList.subOrderItemId}','${providerDetailsList.networkId}','${providerDetailsList.website}');"
                       href="${providerDetailsList.website}"
                       title="${providerDetailsList.collegeNameDisplay} website">
                       <spring:message code="visit.website.label" /></a>
                     </div>   
                     </c:if>
                     </c:if>
                    </c:when>
                    <c:otherwise>
                     <c:if test="${not empty providerDetailsList.subOrderItemId}">
                       <c:if test="${providerDetailsList.subOrderItemId eq 0}">                     
                         <div class="sremail">
                          <a rel="nofollow" target="_blank" class="req-inf"
                           onclick="javaScript:nonAdvPdfDownload('${gaCollegeName}#${providerDetailsList.webformprice}#${providerDetailsList.collegeId}#${providerDetailsList.subOrderItemId}#${providerDetailsList.networkId}#${providerDetailsList.emailWebForm}#<%=courseId%>#<%=imgpath%>#PROVIDER#${providerDetailsList.collegeId}');"
                           title="Email ${providerDetailsList.collegeNameDisplay}">
                           Request info<i class="fa fa-caret-right"></i>
                         </a>
                        </div> 
                      </c:if>
                    </c:if>
                    <c:if test="${empty providerDetailsList.subOrderItemId}">         
                      <div class="sremail">
                        <a rel="nofollow"  target="_blank" class="req-inf"
                          onclick="javaScript:nonAdvPdfDownload('${gaCollegeName}#${providerDetailsList.webformprice}#${providerDetailsList.collegeId}#${providerDetailsList.subOrderItemId}#${providerDetailsList.networkId}#${providerDetailsList.emailWebForm}#<%=courseId%>#<%=imgpath%>#PROVIDER#${providerDetailsList.collegeId}');"
                          title="Email ${providerDetailsList.collegeNameDisplay}">
                          Request info<i class="fa fa-caret-right"></i>
                        </a>
                      </div>
                    </c:if>
                    <c:if test="${not empty providerDetailsList.subOrderItemId}">            
                    <c:if test="${providerDetailsList.subOrderItemId ne 0}">
                    <c:set var="subOrderId1" value="${providerDetailsList.subOrderItemId}"/>         
                    <c:if test="${not empty providerDetailsList.emailWebForm}">
                     <div class="sremail"><%--Changed event action webclick to webform by Sangeeth.S for July_3_18 rel--%>
                      <a rel="nofollow" target="_blank" class="req-inf"
                         onclick="GAInteractionEventTracking('emailwebform', 'interaction', 'email webform', '${gaCollegeName}', '${providerDetailsList.webformprice}'); cpeEmailWebformClick(this,'${providerDetailsList.collegeId}','${providerDetailsList.subOrderItemId}','${providerDetailsList.networkId}','${providerDetailsList.emailWebForm}'); var a='s.tl(';" 
                         href="${providerDetailsList.emailWebForm}" 
                         title="Email ${providerDetailsList.collegeNameDisplay}">
                         Request info<i class="fa fa-caret-right"></i>
                      </a>
                     </div> 
                   </c:if>
                   <c:if test="${not empty providerDetailsList.email}">
                   <c:if test="${empty providerDetailsList.emailWebForm}">
                     <div class="sremail">
                      <a rel="nofollow" target="_blank" class="req-inf"
                         onclick="GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '${gaCollegeName}');"                
                         href="<SEO:SEOURL pageTitle="sendcollegemail" >${providerDetailsList.collegeName}#${providerDetailsList.collegeId}#<%=courseId%>#0#n#${providerDetailsList.subOrderItemId}</SEO:SEOURL>"
                         title="Email ${providerDetailsList.collegeNameDisplay}" >
                         Request info<i class="fa fa-caret-right"></i>
                       </a><%--13-JAN-2015  Added for adroll marketing--%>   
                      </div>
                    </c:if>
                    </c:if>
                    <c:if test="${not empty providerDetailsList.prospectusWebForm}">
                      <div class="srprospectus">
                      <a rel="nofollow" target="_blank" class="get-pros" 
                         onclick="GAInteractionEventTracking('prospectuswebform', 'interaction', 'prospectus webform', '${gaCollegeName}', ${providerDetailsList.webformprice}); cpeProspectusWebformClick(this,'${providerDetailsList.collegeId}','${providerDetailsList.subOrderItemId}','${providerDetailsList.networkId}','${providerDetailsList.prospectusWebForm}'); var a='s.tl(';" 
                         href="${providerDetailsList.prospectusWebForm}" 
                         title="Get ${providerDetailsList.collegeNameDisplay} Prospectus">
                         Get prospectus<i class="fa fa-caret-right"></i></a>
                      </div>   
                     </c:if>
                     <c:if test="${not empty providerDetailsList.prospectus}">
                      <c:if test="${empty providerDetailsList.prospectusWebForm}">
                       <div class="srprospectus">
                       <a onclick="GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '${gaCollegeName}'); return prospectusRedirect('${CONTEXT_PATH}','${providerDetailsList.collegeId}','0','','','${providerDetailsList.subOrderItemId}');"
                          class="get-pros" title="Get ${providerDetailsList.collegeNameDisplay} Prospectus" >
                          Get prospectus<i class="fa fa-caret-right"></i></a>
                       </div>   
                      </c:if>
                     </c:if>
                     <c:if test="${not empty providerDetailsList.website}">
                      <div class="srwebsite">
                     <a rel="nofollow" 
                         target="_blank" 
                         class="visit-web" 
                         id="${providerDetailsList.collegeId}"
                         onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', ${providerDetailsList.websitePrice}); cpeWebClickWithCourse(this,'${providerDetailsList.collegeId}','0','${providerDetailsList.subOrderItemId}','${providerDetailsList.networkId}','${providerDetailsList.website}');var a='s.tl(';" 
                         href="${providerDetailsList.website}"
                         title="${providerDetailsList.collegeNameDisplay} website">
                         Visit website<i class="fa fa-caret-right"></i></a>
                      </div>   
                     </c:if>
                   </c:if>
                   </c:if>    
                   <c:set var="opendayscount" value="${providerDetailsList.openDaysCount}"/>
                   <c:if test="${not empty providerDetailsList.openDaysCount}">
                   <c:if test="${providerDetailsList.openDaysCount gt 0}">
                   <c:if test="${not empty providerDetailsList.openDaySubOrderItemId}">
                   <c:if test="${providerDetailsList.openDaysCount eq 1}">
                     <div class="sropendays">
                       <a rel="nofollow" class="req-inf sr_opd" target="_blank"
                          onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Reserve A Place', '${gaCollegeName}');
                                   addOpenDaysForReservePlace('<c:out value="${providerDetailsList.openDay}"/>', '<c:out value="${providerDetailsList.openMonthYear}"/>', '<c:out value="${providerDetailsList.collegeId}"/>', '<c:out value="${providerDetailsList.openDaySubOrderItemId}"/>', '<c:out value="${providerDetailsList.networkId}"/>'); 
                                   cpeODReservePlace(this,'<c:out value="${providerDetailsList.collegeId}"/>','<c:out value="${providerDetailsList.subOrderItemId}"/>','<c:out value="${providerDetailsList.networkId}"/>','<c:out value="${providerDetailsList.bookingURL}"/>');"
                          href="<c:out value="${providerDetailsList.bookingURL}"/>"
                          title="Book <c:out value="${providerDetailsList.collegeNameDisplay}"/> open day" >
                          VIEW ${providerDetailsList.openDaysCount} OPEN DAY<i class="fa fa-caret-right"></i>
                       </a>   
                    </div>
                 </c:if>
                 <c:if test="${providerDetailsList.openDaysCount ne 1}">
                   <div class="sropendays">
                      <a rel="nofollow"  class="req-inf sr_opd"
                         onclick="GAInteractionEventTracking('visitwebsite', 'engagement', 'Book Open Day Request', '${gaCollegeName}');showOpendayPopup('${providerDetailsList.collegeId}', '');" 
                         title="Book <c:out value="${providerDetailsList.collegeNameDisplay}"/> open day" >
                         VIEW ${providerDetailsList.openDaysCount} OPEN DAYS<i class="fa fa-caret-right"></i>
                      </a>   
                    </div>
                  </c:if>
                </c:if>
              </c:if>
            </c:if>                    
         </c:otherwise>
       </c:choose>
      <c:if test="${not empty providerDetailsList.viewCourseFlag and providerDetailsList.viewCourseFlag eq 'Y'}">
        <c:choose>
          <c:when test="${clearingFlag eq 'Y'}">
            <c:set var="providerResultsURL" value="${fn:replace(providerDetailsList.providerResultsURL,'?','?clearing&')}" />
          </c:when>
          <c:otherwise>
            <c:set var="providerResultsURL" value="${providerDetailsList.providerResultsURL}" />                  
          </c:otherwise>
        </c:choose>
		<div class="viewallcurse">
		  <a rel="nofollow" target="_blank" class="viewall_crs" onclick="" title="View ${providerDetailsList.courseCount}<c:choose><c:when test="${providerDetailsList.courseCount eq 1}"> Course</c:when><c:otherwise> Courses</c:otherwise></c:choose>" href="${providerResultsURL}">
			VIEW ${providerDetailsList.courseCount} <c:choose><c:when test="${providerDetailsList.courseCount eq 1}"> COURSE </c:when><c:otherwise>COURSES </c:otherwise> </c:choose> <i class="fa fa-caret-right"></i>
		  </a> 
		</div>
	  </c:if>
	</div>
  </div>
</div>
</c:forEach>
</c:if>
</div>
</div>
<div class="pr_pagn">
  <jsp:include page="/jsp/cpc/include/pagination.jsp">
    <jsp:param name="pageno" value="<%=pageno%>"/>
    <jsp:param name="pagelimit"  value="10"/>
    <jsp:param name="searchhits" value="<%=universityCount%>"/>
    <jsp:param name="recorddisplay" value="12"/>              
    <jsp:param name="pageURL" value="<%=firstPageSeoUrl%>"/>
    <jsp:param name="orderBy" value=""/>
    <jsp:param name="pageTitle" value=""/>
  </jsp:include>
</div>
<c:if test="${not empty universityCount and universityCount gt 0}">				
  <div class="fl_w100"><div class="ttl_pge"><%=fromCourseRecordCount%>-<%=endCourseRecordCount %> of <%=searchhits%> ${universityCount eq '1' ? 'university' : 'universities' }</div></div>
</c:if>
</c:when>
<c:otherwise>
<div class="cpc_uni_wrap"><div class="fnt_16 txt_cnr no_rslt mt40">Oops! We couldn't find any universities matching your search criteria. Please change your search options and try again</div></div>
</c:otherwise>
</c:choose>
<input type="hidden" id="selectedSubjectCode" value="${selectedSubjectCode}"/>
<input type="hidden" id="selectedSubjectName" value="${selectedSubjectName}"/>
<input type="hidden" id="selectedRegionId" value="${selectedRegionId}"/>
<input type="hidden" id="selectedRegionName" value="${selectedRegionName}"/>
<input type="hidden" id="selectedSortCode" value="${selectedSortCode}"/>
<input type="hidden" id="sortName" value="${sortName}"/>
<input type="hidden" id="uniCountId" value="${universityCount}"/>
<input type="hidden" id="preview" value="${preview}" />