<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" %>
<% String lazyLoadNotDoneFlag = GlobalConstants.LAZY_LOAD_FLAG;
   CommonUtil util = new CommonUtil(); %>
<section class="slider_cnt" id="heroslider">
            <div class="hm_ldicon" id="load-icon" style="display:none"> <img src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif", 1)%>"> </div>
            <div class="hrosrch_ui fl_w100">
            <div class="overly"></div>
            <c:set var="imagePath" value="${mediaPath}"/>
            <%String imagePath = (String)pageContext.getAttribute("imagePath");
              imagePath = util.getImageUrl(request, imagePath, GlobalConstants.CPC_HERO_MOB_IMGSIZE, GlobalConstants.CPC_HERO_IMGSIZE, GlobalConstants.CPC_HERO_IMGSIZE);%>
                
                <div id="homeHeroImgDivId" class="hrobanr_ui fl_w100" data-rjs="Y" data-src="<%=CommonUtil.getImgPath(imagePath, 1)%>" data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>" style="background: url('<%=CommonUtil.getImgPath(imagePath, 1)%>') center top no-repeat;"> 
                    <div class="srch_cnt fl_w100">
                        <div class="hrohd_ui fl_w100" data-src="/wu-cont/images/hero_search_bgimg.jpg">
                            <h1>${title}</h1>
                            <h2>${subTitle}</h2>
                            <input type="hidden" id="heroImgPath" value="${mediaPath}"> 
						</div>
                    </div>
                </div>
            </div>
        </section>
		<section class="ltst_art">
			<div class="content-bg">
				<div class="cpc_txt_wrap">
				${editorialContent}
				</div>
			</div>
		</section>