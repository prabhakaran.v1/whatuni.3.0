<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,java.util.HashMap" autoFlush="true" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<c:set var="pagename3">
  <tiles:getAsString name="pagename3" ignore="true"/>
</c:set>
 
<%String pagename3 = "";
int stind3 = request.getRequestURI().lastIndexOf("/");
int len3 = request.getRequestURI().length();
if(!GenericValidator.isBlankOrNull(pageContext.getAttribute("pagename3").toString())){
	  pagename3 = (String)pageContext.getAttribute("pagename3");
}else{
	  pagename3 = request.getRequestURI().substring(stind3+1,len3); 
}
String exitOpenDayRes = "" , odEvntAction = "false";
if(session.getAttribute("exitOpRes")!=null && !"".equals(session.getAttribute("exitOpRes"))){
  exitOpenDayRes = (String)session.getAttribute("exitOpRes");
}    
if(!"".equals(exitOpenDayRes) && "true".equals(exitOpenDayRes)){
  odEvntAction = "true";
}
String gaCollegeName = (String)request.getAttribute("hitbox_college_name");
gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(gaCollegeName);
gaCollegeName = !GenericValidator.isBlankOrNull(gaCollegeName) ? !gaCollegeName.equals("null") ? gaCollegeName.toLowerCase().trim().replaceAll(" ","-") : "" : "";
String studyLevelDesc = request.getAttribute("studyLevelDesc") != null ? (String)request.getAttribute("studyLevelDesc") : "";    
if(!GenericValidator.isBlankOrNull(studyLevelDesc)){
    //Added for fix duplicate study level by Prabha on 21_Feb_17
    HashMap cDimLevelMap = new HashMap();
    cDimLevelMap.put("degree","Degree");
    cDimLevelMap.put("postgraduate","Postgraduate");
    cDimLevelMap.put("hnd/hnc","HND/HNC");
    cDimLevelMap.put("access and foundation","Access and foundation");
    cDimLevelMap.put("foundation degree","Foundation degree");

    String cDimLevel =  (String)cDimLevelMap.get(studyLevelDesc);
    studyLevelDesc = !GenericValidator.isBlankOrNull(cDimLevel) ? cDimLevel : studyLevelDesc;
}   
%>
<div id="revLightBox"></div>
    <div id="loaderImg" class="cmm_ldericn" style="display:none;"><img alt="loading" src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif",0)%>"></div>
    <%@include  file="/jsp/common/includeJS.jsp" %>    
	<div id="newvenue" class="dialog" >
	    <div id="newvenueform" style="display: none;" ></div>
	     <div id="ajax-div" class="lb_pload" style="display: none;">	     	         
	        <img src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/cmn_loading_1.svg",0)%>" alt="loading image" title="" />	         
	         <span id="txt">Please wait</span>
	         <input id="loading_cancel" type="button" onclick="hm('newvenue');" value="Cancel" class="button"/>
	     </div>
	 </div> 
    <%if(request.getAttribute("showBanner")==null){%>
	  <jsp:include page="/jsp/thirdpartytools/includeCallBanner.jsp"/>
	<%}%>
	<jsp:include page="/jsp/thirdpartytools/include/includeAdmedoDiv.jsp">
	  <jsp:param name="pagename" value="<%=pagename3%>"/>
	</jsp:include>
    <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />  
    <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />
    <input type="hidden" id="exitOpenDayResFlg" value="<%=exitOpenDayRes%>"/>
    <input type="hidden" id="odEvntAction" value="<%=odEvntAction%>"/>
    <input type="hidden" id="gaCollegeName" value="<%=gaCollegeName%>"/>   
    <input type="hidden" id="gaStudyLevelDesc" value="<%=studyLevelDesc%>"/>
    