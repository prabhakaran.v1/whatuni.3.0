<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil"%>
<%String imgpath=""; 
String searchhits = (String) request.getAttribute("universityCount");
String pageno = request.getAttribute("pageno") != null ? (String)request.getAttribute("pageno") : "1";
String urldata_1 = (String)request.getAttribute("URL_1");
String urldata_2 = (String)request.getAttribute("URL_2");
String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "NULL";
String universityCount = (request.getAttribute("universityCount") !=null && !request.getAttribute("universityCount").equals("null") && String.valueOf(request.getAttribute("universityCount")).trim().length()>0 ? String.valueOf(request.getAttribute("universityCount")) : ""); 
String resultExists = (request.getAttribute("resultExists") !=null && !request.getAttribute("resultExists").equals("null") && String.valueOf(request.getAttribute("resultExists")).trim().length()>0 ? String.valueOf(request.getAttribute("resultExists")) : ""); 
%>
<section class="cpc_lnd_wrap" id="middleSectionId">
			<div class="content-bg">
				<c:if test="${not empty universityCount}" ><h3 class="txt_cnr" id="uniCntTxtId">${universityCount} ${universityCount eq '1' ? 'university' : 'universities' }</h3></c:if>
				<div class="cpc_fltr">	
				
					<div class="land_inp_grp sub_inpt fl" id="drp_subject_prt" data-id="filterDivId_prt">
					<c:if test="${not empty filtersHideFlag and filtersHideFlag eq 'Y'}">
					  <c:if test="${not empty subjectFilterList}">
                         <div class="sel_degcnr cr_srch fl">
						  <div class="sel_deg fl_w100">
							<input type="text" name="subjectName" id="subjectName" class="sld_deg fnt_lbk" value="Filter by subject" readonly="readonly" onblur="javascript:hideDropDown();" onclick="javascript:showDropDown('drp_subject');">
						  </div>
                         </div>
                         <div class="opsr_lst" id="drp_subject" style="display:none" data-id="filterDivId">
                          <ul>
                          <li><span><a onclick="subjectAjax('0','','','')">All subjects</a> </span></li>
                          <c:forEach var="subjectFilterList" items="${subjectFilterList}" varStatus="i">
							  <li><span><a onclick="subjectAjax('${subjectFilterList.subjectCode}','','','')">${subjectFilterList.subjectName}</a> </span></li>
                          
                          </c:forEach>
                          </ul>
                          </div>
                          </c:if>
                        </c:if>
                    </div>
                        
                          
                         
					<div class="land_inp_grp sub_inpt fl" id="drp_location_prt" data-id="filterDivId_prt">
					 <c:if test="${not empty filtersHideFlag and filtersHideFlag eq 'Y'}">
					  <c:if test="${not empty locationFilterList}"> 
                         <div class="sel_degcnr cr_srch fl">
						  <div class="sel_deg fl_w100">
							<input type="text" name="locationName" id="locationName" class="sld_deg fnt_lbk" value="Filter by region" readonly="readonly" onclick="javascript:showDropDown('drp_location');">
						  </div>
                         </div>  
                          <div class="opsr_lst" id="drp_location" style="display:none" data-id="filterDivId">
                          <ul>
                          <li><span><a onclick="subjectAjax('','0','','')">All locations</a> </span></li>
                          <c:forEach var="locationFilterList" items="${locationFilterList}" varStatus="i">
                          <li><span><a onclick="subjectAjax('','${locationFilterList.regionId}','','')"> ${locationFilterList.regionName}</a> </span></li>
                           
                          </c:forEach>
                          </ul>
                          </div>
                       </c:if>  
                     </c:if> 
					</div>					
                    <c:if test="${not empty sortHideFlag and sortHideFlag eq 'Y' and not empty universityCount and universityCount gt 1}">
					<div class="fr">
						<div class="odsrh_col2 fl revm_lft">
							<fieldset class="fs_col2 fl" id="drp_sortId_prt" data-id="filterDivId_prt">
								<div class="od_dloc" id="sortId" onclick="showDropDown('drp_sortId');"><span id="sortTextId" class="gray_arw">Most info</span></div>
								<div class="opsr_lst sort" id="drp_sortId" style="display:none" data-id="filterDivId">
									<ul id="reviewSortId">
									    <li><span><a onclick="subjectAjax('','','def','')"> Most info</a></span></li>
										<li><span><a onclick="subjectAjax('','','ta','')"> A - Z</a></span></li>
										<li><span><a onclick="subjectAjax('','','td','')"> Z - A</a></span></li>
										<li><span><a onclick="subjectAjax('','','wh','')"> Whatuni WUSCA Rating High to Low</a></span></li>
										<li><span><a onclick="subjectAjax('','','wl','')"> Whatuni WUSCA Rating Low to High</a></span></li>
									</ul>
								</div>
							</fieldset>
						</div>
					</div>
					</c:if>
				</div>

				<div id="middleID">
				<jsp:include page="/jsp/cpc/include/middleContentAjaxPod.jsp" />
				</div>
			</div>
		</section>
