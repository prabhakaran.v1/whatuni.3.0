<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants,org.apache.commons.validator.GenericValidator,java.util.HashMap, WUI.utilities.SessionData"%>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%--
  * @purpose  This jsp is related to CPC landing pages.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 04-Jan-2020   Jeyalakshmi D.              598     First Draft                                                       584 
  * *************************************************************************************************************************
  --%>
<%
  String cpcPlayIcon = CommonUtil.getImgPath("/wu-cont/images/cpc_play_icon.svg",0);
  int imgCount = 0;
  CommonFunction commonFunction = new CommonFunction();
  CommonUtil util = new CommonUtil();
  String lazyLoadNotDoneFlag = GlobalConstants.LAZY_LOAD_FLAG;
  String gaCollegeName ="";
  String CONTEXT_PATH = GlobalConstants.WU_CONTEXT_PATH;
  String onePixelImgUrl = CommonUtil.getImgPath(GlobalConstants.ONE_PIXEL_IMG_PATH, 1);
  String studyLevelDesc = request.getAttribute("studyLevelDesc") != null ? (String)request.getAttribute("studyLevelDesc") : "";    
  if(!GenericValidator.isBlankOrNull(studyLevelDesc)){
      //Added for fix duplicate study level by Prabha on 21_Feb_17
      HashMap cDimLevelMap = new HashMap();
      cDimLevelMap.put("degree","Degree");
      cDimLevelMap.put("postgraduate","Postgraduate");
      cDimLevelMap.put("hnd/hnc","HND/HNC");
      cDimLevelMap.put("access and foundation","Access and foundation");
      cDimLevelMap.put("foundation degree","Foundation degree");

      String cDimLevel =  (String)cDimLevelMap.get(studyLevelDesc);
      studyLevelDesc = !GenericValidator.isBlankOrNull(cDimLevel) ? cDimLevel : studyLevelDesc;
  }    
  String gaDimCollegeName = (String)request.getAttribute("gaCollegeName");
  gaDimCollegeName = !GenericValidator.isBlankOrNull(gaDimCollegeName) ? !gaDimCollegeName.equals("null") ? gaDimCollegeName.toLowerCase().trim().replaceAll(" ","-") : "" : "";
%>
<c:set var="CONTEXT_PATH" value="<%=CONTEXT_PATH%>"/>
<c:if test="${not empty uniDetailsList}">
  <div class="rvbx_shdw" onclick="closeLigBox();videoPauseOnClose();"></div>
  <div class="revcls">
    <a onclick="closeLigBox();videoPauseOnClose();" class="close">
	  <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
		<defs>
		  <path d="M18.7,5.3 C18.3,4.9 17.7,4.9 17.3,5.3 L12,10.6 L6.7,5.3 C6.3,4.9 5.7,4.9 5.3,5.3 C4.9,5.7 4.9,6.3 5.3,6.7 L10.6,12 L5.3,17.3 C4.9,17.7 4.9,18.3 5.3,18.7 C5.5,18.9 5.7,19 6,19 C6.3,19 6.5,18.9 6.7,18.7 L12,13.4 L17.3,18.7 C17.5,18.9 17.8,19 18,19 C18.2,19 18.5,18.9 18.7,18.7 C19.1,18.3 19.1,17.7 18.7,17.3 L13.4,12 L18.7,6.7 C19.1,6.3 19.1,5.7 18.7,5.3 Z" id="path-1"></path>
		</defs>
	    <g id="Icon/Close" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
		<mask id="mask-2" fill="#ffffff">
		  <use xlink:href="#path-1"></use>
		</mask>
		<g id="Mask" fill-rule="nonzero"></g>
		<g id="Mixin/Fill/Black" mask="url(#mask-2)" fill="#ffffff">
		  <rect id="Rectangle" x="0" y="0" width="24" height="24"></rect>
		</g>
		</g>
	  </svg>
    </a>
  </div>
  <div class="rvbx_cnt">
    <div class="rev_lst lbx_scrl" id="parentDivHeight">
       <c:if test="${not empty imageDetailsList}">
         <div class="vid_sec po_rel" id="div1Height">
         <div class="ltx_ovrly"></div>
           <div class="vid_sec1">
             <div class="vid_sec2" id="videoSec">
              <c:choose>
		       <c:when test="${fn:length(imageDetailsList) gt 1}">
		         <div id="slider" class="slider">
				   <div class="slider-items-container" style="width: 3200px; transform: translate(0px, 0px);">
					 <c:forEach var="imageDetails" items="${imageDetailsList}" varStatus="i">
					 <c:set var="imgPos" value="${i.index}" > </c:set>
                     <%imgCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("imgPos").toString()));%>
					  <c:if test="${imageDetails.mediaType eq 'VIDEO' and i.index == 0}">
	   		            <c:set var="firstVidId" value="${imageDetails.mediaId}"/>
	   		          </c:if>
					   <c:if test="${imageDetails.mediaType eq 'IMAGE'}">
					     <div class="slider-item">
					 	   <c:set var="imagePath" value="${imageDetails.mediaPath}"/>
                           <%String imagePath = (String)pageContext.getAttribute("imagePath"); String srcImgPath = (String)pageContext.getAttribute("imagePath");%>
                            <c:choose>                            
                             <c:when test="${(imageDetails.contentHubFlag eq 'Y' and clearingFlag ne 'Y') or (imageDetails.clearingProfileMediaFlag eq 'Y' and clearingFlag eq 'Y')}">
                               <% srcImgPath = util.getImageUrl(request, imagePath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>                               
                             </c:when>
                             <c:otherwise>
                                <% srcImgPath = util.getImageUrl(request, imagePath, GlobalConstants.CPC_MOB_IMGSIZE, GlobalConstants.CPC_TAB_IMGSIZE, GlobalConstants.CPC_DESKTOP_IMGSIZE); %>                               
                              </c:otherwise>
                           </c:choose>
                              <c:set var="srcImg" value="<%=(imgCount != 0)?onePixelImgUrl:srcImgPath%>"/>
                           <c:choose>                          
                             <c:when test="${(imageDetails.contentHubFlag eq 'Y' and clearingFlag ne 'Y') or (imageDetails.clearingProfileMediaFlag eq 'Y' and clearingFlag eq 'Y')}">
                               <% imagePath = util.getImageUrl(request, imagePath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>
                               <img src="${srcImg}" data-src="<%=CommonUtil.getImgPath(imagePath, 1)%>">
                             </c:when>
                             <c:otherwise>
                                <% imagePath = util.getImageUrl(request, imagePath, GlobalConstants.CPC_MOB_IMGSIZE, GlobalConstants.CPC_TAB_IMGSIZE, GlobalConstants.CPC_DESKTOP_IMGSIZE); %>
                                <img src="${srcImg}" data-src="<%=CommonUtil.getImgPath(imagePath, 1)%>" data-rjs="Y"  data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>">
                              </c:otherwise>
                           </c:choose>
                        </div>
                       </c:if>					
						<c:if test="${imageDetails.mediaType eq 'VIDEO'}">
						<div class="slider-item">
						  <c:set var="thumbnailPath" value="${imageDetails.thumbnailPath}"/>
                          <%String thumbnailPath = (String)pageContext.getAttribute("thumbnailPath"); String srcThumbNailPath = (String)pageContext.getAttribute("thumbnailPath");%>
		                  <div class="vide_icn"><img class="pl_icn_img" src="<%=cpcPlayIcon%>" id="cpc_play_icon_${imageDetails.mediaId}" onclick="clickPlayAndPause('cpc_libox_video_${imageDetails.mediaId}', event, 'thumbnailImg_${imageDetails.mediaId}', 'cpc_play_icon_${imageDetails.mediaId}');" alt="play icon"></div>
		                   <c:choose>		                    
                             <c:when test="${(imageDetails.contentHubFlag eq 'Y' and clearingFlag ne 'Y') or (imageDetails.clearingProfileMediaFlag eq 'Y' and clearingFlag eq 'Y')}">
                               <% srcThumbNailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>                               
                             </c:when>
                             <c:otherwise>
                                <% srcThumbNailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants.CPC_MOB_IMGSIZE, GlobalConstants.CPC_TAB_IMGSIZE, GlobalConstants.CPC_DESKTOP_IMGSIZE); %>                               
                              </c:otherwise>
                           </c:choose>
		                   <c:set var="srcThumbNail" value="<%=(imgCount != 0)?onePixelImgUrl:srcThumbNailPath%>"/>
		                   <c:choose>		                    
                             <c:when test="${(imageDetails.contentHubFlag eq 'Y' and clearingFlag ne 'Y') or (imageDetails.clearingProfileMediaFlag eq 'Y' and clearingFlag eq 'Y')}">
                               <% thumbnailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>
                               <img id="thumbnailImg_${imageDetails.mediaId}" width="100%" height="auto" src="${srcThumbNail}" data-src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" >
                             </c:when>
                             <c:otherwise>
                                <% thumbnailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants.CPC_MOB_IMGSIZE, GlobalConstants.CPC_TAB_IMGSIZE, GlobalConstants.CPC_DESKTOP_IMGSIZE); %>
                                <img id="thumbnailImg_${imageDetails.mediaId}" width="100%" height="auto" src="${srcThumbNail}" data-src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-rjs="Y" data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>">
                              </c:otherwise>
                           </c:choose>		                  
				          <video preload="none" id="cpc_libox_video_${imageDetails.mediaId}" width="100%" height="100%" onplay="lightBoxLogging(this,'${gaCollegeName}');"
				            data-media-id="${imageDetails.mediaId}" controls style="display:none" class="v_wid" 
				            controlslist="nodownload noremoteplayback" disablepictureinpicture="">
				            <source src="${imageDetails.mediaPath}" type="video/mp4">				            
			              </video>
						</div>
						</c:if>						
					 </c:forEach>											
					</div>	
					<div class="controls">
		              <a href="#" class="prev-btn"><span class="crsl_lftarw"></span></a>
			          <a href="#" class="next-btn"><span class="crsl_ritarw"></span></a>
					</div>
					<div class="dots">
					</div>	
                  </div>
                </c:when>
                <c:otherwise>
                 <c:forEach var="imageDetails" items="${imageDetailsList}" varStatus="i">
                   <c:if test="${imageDetails.mediaType eq 'VIDEO' and i.index == 0}">	   		          
	   		         <c:set var="firstVidId" value="${imageDetails.mediaId}"/>
	   		       </c:if>
                  <c:if test="${imageDetails.mediaType eq 'IMAGE'}">
                    <c:set var="imagePath" value="${imageDetails.mediaPath}"/>
                    <%String imagePath = (String)pageContext.getAttribute("imagePath");%>
                    <c:choose>
                      <c:when test="${(imageDetails.contentHubFlag eq 'Y' and clearingFlag ne 'Y') or (imageDetails.clearingProfileMediaFlag eq 'Y' and clearingFlag eq 'Y')}">
                        <% imagePath = util.getImageUrl(request, imagePath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>
                        <img width="100%" height="auto" src="<%=CommonUtil.getImgPath(imagePath, 1)%>"></img>
                      </c:when>
                      <c:otherwise>
                        <% imagePath = util.getImageUrl(request, imagePath, GlobalConstants.CPC_MOB_IMGSIZE, GlobalConstants.CPC_TAB_IMGSIZE, GlobalConstants.CPC_DESKTOP_IMGSIZE); %>
                        <img width="100%" height="auto" src="<%=CommonUtil.getImgPath(imagePath, 1)%>"></img>
                      </c:otherwise>
                    </c:choose>        
			      </c:if>
		          <c:if test="${imageDetails.mediaType eq 'VIDEO'}">
	                <c:if test="${not empty imageDetails.mediaPath}">
		              <c:set var="thumbnailPath" value="${imageDetails.thumbnailPath}"/>
                      <%String thumbnailPath = (String)pageContext.getAttribute("thumbnailPath");%>
                      <div class="vide_icn"><img class="pl_icn_img" src="<%=cpcPlayIcon%>" id="cpc_play_icon_${imageDetails.mediaId}" onclick="clickPlayAndPause('cpc_libox_video_${imageDetails.mediaId}', event, 'thumbnailImg_${imageDetails.mediaId}','cpc_play_icon_${imageDetails.mediaId}');" alt="play icon">
                      </div>
                      <c:choose>
                        <c:when test="${(imageDetails.contentHubFlag eq 'Y' and clearingFlag ne 'Y') or (imageDetails.clearingProfileMediaFlag eq 'Y' and clearingFlag eq 'Y')}">
                          <% thumbnailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>
                          <img id="thumbnailImg_${imageDetails.mediaId}" width="100%" height="auto" src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>">
                        </c:when>
                        <c:otherwise>
                          <% thumbnailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants.CPC_MOB_IMGSIZE, GlobalConstants.CPC_TAB_IMGSIZE, GlobalConstants.CPC_DESKTOP_IMGSIZE); %>
                          <img id="thumbnailImg_${imageDetails.mediaId}" width="100%" height="auto" src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-rjs="Y" data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>">
                         </c:otherwise>
                      </c:choose>                      
		              <video preload="none" id="cpc_libox_video_${imageDetails.mediaId}" width="100%" height="100%" onplay="lightBoxLogging(this,'${gaCollegeName}');"
				        data-media-id="${imageDetails.mediaId}" style="display:none"
				        controlslist="nodownload noremoteplayback" disablepictureinpicture="">
				        <source src="${imageDetails.mediaPath}" type="video/mp4">				        
			          </video>
                      </c:if>
	   		        </c:if>      
	   		        </c:forEach>
	              </c:otherwise>
	            </c:choose>
	            <c:if test="${not empty firstVidId}">
   		          <input type="hidden" id="vidMedId_0" value="${firstVidId}">
   		        </c:if>   		        
	          </div>
	        </div>
	      </div> 
	    </c:if>    
        <div class="bx_size" id="div2Height">
          <div class="vid_sec">
             <div class="vid_sec1">
               <c:forEach var="uniDetails" items="${uniDetailsList}" varStatus="row">  
                 <div class="myic_secc_det">
                  <h2 class="uni_nm">
                  <c:choose>
                    <c:when test="${clearingFlag eq 'Y'}">
                      <c:set var="uniHomeURL" value="${uniDetails.uniHomeURL}clearing/" />
                      <c:set var="providerResultsURL" value="${fn:replace(uniDetails.providerResultsURL,'?','?clearing&')}" />
                    </c:when>
                    <c:otherwise>
                      <c:set var="uniHomeURL" value="${uniDetails.uniHomeURL}" />
                      <c:set var="providerResultsURL" value="${uniDetails.providerResultsURL}" />                  
                    </c:otherwise>
                  </c:choose>                  
                  <c:choose>
		            <c:when test="${uniDetails.urlFormingCode eq 'IP'}"> <a target="_blank" href="${uniHomeURL}">${uniDetails.collegeNameDisplay}</a></c:when>
		            <c:otherwise> <a target="_blank" href="${providerResultsURL}">${uniDetails.collegeNameDisplay}</a></c:otherwise></c:choose></h2>
                      <c:if test="${not empty uniDetails.overAllRating}">
                      <c:if test="${uniDetails.overAllRating gt 0}">  
                        <ul class="strat cf">
                          <li class="mr-15 leag_wrap">
                            <span class="rat${uniDetails.overAllRating} t_tip">
                              <span class="cmp">
                                <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
                                <div class="line"></div>
                              </span>
                            </span>
                            <span class="rtx">(${uniDetails.overAllRatingExact})</span> 
                            <c:if test="${not empty uniDetails.reviewCount}">
				              <a class="link_blue" target="_blank" href="${uniDetails.uniReviewUrl}">
			                    <c:if test="${not empty uniDetails.reviewCount}">
				                  <c:if test="${uniDetails.reviewCount ne 0}">
				                    ${uniDetails.reviewCount}
                                    <c:if test="${uniDetails.reviewCount gt 1}">
                                    reviews
                                    </c:if>
				                    <c:if test="${uniDetails.reviewCount eq 1}">  
                                    review
                                    </c:if>
				                  </c:if>
                                </c:if>
                              </a>
                            </c:if> 
                          </li>
                        </ul>
                     </c:if>
                   </c:if>
                 </div>
                <c:set var="collegeName" value="${uniDetails.collegeName}"/>
                <%gaCollegeName = commonFunction.replaceSpecialCharacter((String)pageContext.getAttribute("collegeName"));%>
                <c:set var="gaCollegeName" value="<%=gaCollegeName%>"/>        
                <c:if test="${not empty uniDetails.subOrderItemId}">   
                  <c:if test="${uniDetails.subOrderItemId ne 0}">
                    <c:set var="buttonLabel" value="${uniDetails.buttonLabel}"/>
                    <div class="btns_interaction fl" id="buttonCls">
                    <div class="fright">
                    <c:if test="${not empty uniDetails.buttonLabel}"> 
                      <c:set var = "buttonArr" value = "${fn:split(uniDetails.buttonLabel, ',')}" />
                    <c:forEach var="labelArr" items="${buttonArr}" varStatus="idx">  
                    
                <%--  <%String buttonLabel = (String)pageContext.getAttribute("buttonLabel");
                 if(!GenericValidator.isBlankOrNull(buttonLabel)){  
                 String buttonArr[] = buttonLabel.split(",");
                   for(int i=0;i<=buttonArr.length-1;i++) { %>   --%>
                  
                  <c:set var="subOrderId1" value="${uniDetails.subOrderItemId}"/>     
                    <%--  <%if(buttonArr[i].equalsIgnoreCase("EMAIL")) {%> --%> 
                    
                    <c:if test="${fn:toUpperCase(labelArr) eq 'HOTLINE'}">
                      <c:if test="${not empty uniDetails.hotlineNo}">
                        <a rel="nofollow" class="get-pros callnow" id="contact_cpclb_${uniDetails.collegeId}" onclick="GAInteractionEventTracking('Webclick', 'interaction', 'hotline','${gaCollegeName}');changeContact('${uniDetails.hotlineNo}','contact_cpclb_${uniDetails.collegeId}');cpeHotlineClearing(this,'${uniDetails.collegeId}','${uniDetails.subOrderItemId}','${uniDetails.networkId}','${uniDetails.hotlineNo}','${uniDetails.orderItemId}');">
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <spring:message code="call.now.label" />
                        </a>
                      </c:if>
                    </c:if> 
                    <c:if test="${fn:toUpperCase(labelArr) eq 'EMAIL'}">   
		              <c:if test="${not empty uniDetails.emailWebForm}">
		                <a rel="nofollow" target="_blank" class="req-inf mr20"
		                  onclick="GAInteractionEventTracking('emailwebform', 'interaction', 'email webform', '${gaCollegeName}', ${uniDetails.webformprice}); cpeEmailWebformClick(this,'${uniDetails.collegeId}','${uniDetails.subOrderItemId}','${uniDetails.networkId}','${uniDetails.emailWebForm}'); var a='s.tl(';" 
		                  href="${uniDetails.emailWebForm}" title="Email ${uniDetails.collegeNameDisplay}">Request info<i class="fa fa-caret-right"></i>
		                </a>
		              </c:if>
		              <c:if test="${not empty uniDetails.email}">
			            <c:if test="${empty uniDetails.emailWebForm}">
			              <a rel="nofollow" target="_blank" class="req-inf mr20"
			                onclick="GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '${gaCollegeName}');"                
			                href="<SEO:SEOURL pageTitle="sendcollegemail" >${uniDetails.collegeName}#${uniDetails.collegeId}#0#0#n#${uniDetails.subOrderItemId}</SEO:SEOURL>"
			                title="Email ${uniDetails.collegeNameDisplay}" >Request info<i class="fa fa-caret-right"></i>
			              </a><%--13-JAN-2015  Added for adroll marketing--%>   
			             </c:if>
		               </c:if> 
		                    <%--  <%} else if(buttonArr[i].equalsIgnoreCase("PROSPECTUS")) { %> --%>
		            </c:if>
		            <c:if test="${fn:toUpperCase(labelArr) eq 'PROSPECTUS'}">   
		              <c:if test="${not empty uniDetails.prospectusWebForm}">
		                <a rel="nofollow" target="_blank" class="get-pros" 
		                  onclick="GAInteractionEventTracking('prospectuswebform', 'interaction', 'prospectus webform', '${gaCollegeName}', ${uniDetails.webformprice}); cpeProspectusWebformClick(this,'${uniDetails.collegeId}','${uniDetails.subOrderItemId}','${uniDetails.networkId}','${uniDetails.prospectusWebForm}'); var a='s.tl(';" 
		                  href="${uniDetails.prospectusWebForm}" 
		                  title="Get ${uniDetails.collegeNameDisplay} Prospectus">Get prospectus<i class="fa fa-caret-right"></i>
		                </a>
		              </c:if>
		              <c:if test="${not empty uniDetails.prospectus}">
		                <c:if test="${empty uniDetails.prospectusWebForm}">
		                  <a onclick="GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '${gaCollegeName}'); return prospectusRedirect('${CONTEXT_PATH}','${uniDetails.collegeId}','','','','${uniDetails.subOrderItemId}');"
		                    class="get-pros" title="Get ${uniDetails.collegeNameDisplay} Prospectus" >Get prospectus<i class="fa fa-caret-right"></i>
		                  </a>
		                </c:if>
		              </c:if>
                       <%-- <%}else if(buttonArr[i].equalsIgnoreCase("WEBSITE")) {  %> --%>
                    </c:if>
                    <c:if test="${fn:toUpperCase(labelArr) eq 'WEBSITE'}">   
                      <c:if test="${not empty uniDetails.website}">
		                <c:choose>
		                  <c:when test="${clearingFlag eq 'Y'}">                   
                            <a rel="nofollow" target="_blank" class="req-inf" id="crs_${row.index}_${uniDetails.collegeId}"
                              onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', ${uniDetails.websitePrice});cpeWebClickClearing(this,'${uniDetails.collegeId}','${uniDetails.subOrderItemId}','${uniDetails.networkId}','${uniDetails.website}');"
                              href="${uniDetails.website}"
                              title="${uniDetails.collegeNameDisplay} website">
                              <spring:message code="visit.website.label" />
                            </a>
                          </c:when>
                          <c:otherwise>
                          <a rel="nofollow" target="_blank" class="visit-web" id="crs_${row.index}_${uniDetails.collegeId}" 
		                    onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', ${uniDetails.websitePrice}); cpeWebClickWithCourse(this,'${uniDetails.collegeId}','','${uniDetails.subOrderItemId}','${uniDetails.networkId}','${uniDetails.website}');var a='s.tl(';" 
		                    href="${uniDetails.website}"
		                    title="${uniDetails.collegeNameDisplay} website">Visit website
		                    <i class="fa fa-caret-right"></i>
		                  </a>
                    </c:otherwise>
		          </c:choose>
		        </c:if>
                       <%-- <%} else if(buttonArr[i].equalsIgnoreCase("OPENDAYS")) { %> --%>
              </c:if>
              <c:if test="${fn:toUpperCase(labelArr) eq 'OPENDAYS'}"> 
                <c:set var="opendayscount" value="${uniDetails.totalOpenDays}"/>
                <c:if test="${not empty uniDetails.totalOpenDays}">
                <c:if test="${uniDetails.totalOpenDays gt 0}">
                <c:if test="${not empty uniDetails.subOrderItemId}">
	                        <%-- <c:if test="${uniDetails.totalOpenDays eq 1}"> --%>
	              <a rel="nofollow" class="req-inf sr_opd" target="_blank"
	                onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Reserve A Place', '${gaCollegeName}');
	                         addOpenDaysForReservePlace('${uniDetails.openDay}', '${uniDetails.openMonthYear}', '${uniDetails.collegeId}', '${uniDetails.subOrderItemId}', '${uniDetails.networkId}'); 
	                         cpeODReservePlace(this,'${uniDetails.collegeId}','${uniDetails.subOrderItemId}','${uniDetails.networkId}','${uniDetails.bookingUrl}');"
	                 href="${uniDetails.bookingUrl}"
	                 title="Book ${uniDetails.collegeNameDisplay} open day" >
	                 VIEW OPEN DAYS<i class="fa fa-caret-right"></i>               
	               </a>   
	                       <%-- </c:if>
	                       <c:if test="${uniDetails.totalOpenDays ne 1}">
	                         
	                           <a rel="nofollow"
	                              class="req-inf sr_opd"
	                              onclick="GAInteractionEventTracking('visitwebsite', 'engagement', 'Book Open Day Request', '${gaCollegeName}');showOpendayPopup('${uniDetails.collegeId}', '');" 
	                              title="Book ${uniDetails.collegeNameDisplay} open day" >
	                              VIEW OPEN DAYS<i class="fa fa-caret-right"></i>
	                             </a>   
	                          
	                       </c:if> --%>
                        </c:if> 
                     </c:if>
                     </c:if>
                       <%-- <%}else if(buttonArr[i].equalsIgnoreCase("COURSES")) { %> --%>
                       </c:if>
                       <c:if test="${fn:toUpperCase(labelArr) eq 'COURSES'}"> 
                        <c:if test="${landingPage eq 'SINGLE'}">
                          <a rel="nofollow" class="viewall_crs" target="_blank" onclick="" title="View ${uniDetails.courseCount}<c:choose><c:when test="${uniDetails.courseCount eq 1}"> Course</c:when><c:otherwise> Courses</c:otherwise></c:choose>" href="${providerResultsURL}">
						  VIEW <c:choose><c:when test="${uniDetails.courseCount eq 1}"> Course</c:when><c:otherwise> Courses</c:otherwise></c:choose><i class="fa fa-caret-right"></i>
						  </a> 
						</c:if>
						<c:if test="${landingPage eq 'MULTIPLE'}">
						  <a rel="nofollow" target="_blank" class="viewall_crs" onclick="" title="View ${uniDetails.courseCount}<c:choose><c:when test="${uniDetails.courseCount eq 1}"> Course</c:when><c:otherwise> Courses</c:otherwise></c:choose>" href="${providerResultsURL}">
						  VIEW ${uniDetails.courseCount} <c:choose><c:when test="${uniDetails.courseCount eq 1}"> Course</c:when><c:otherwise> Courses</c:otherwise></c:choose><i class="fa fa-caret-right"></i>
						  </a> 
						</c:if>
					  <%-- <%} %> --%>
					  </c:if>
                   </c:forEach>
                     <%-- <%}
                     }%> --%>
                     </c:if>
                    </div>
                   </div>
                 </c:if>
               </c:if>
              <c:if test="${not empty reviewDetailsList}">
                <div class="cpc_lrs">
                  <h3>Latest reviews</h3>
                </div>
             </c:if>
             </c:forEach>
           </div>
          <c:if test="${not empty reviewDetailsList}">
            <c:forEach var="reviewDetails" items="${reviewDetailsList}">
              <div class="rev_cen">
                <div class="rlst_row">
                   <div class="revlst_lft fl">
                      <div class="rlst_wrp fr">
                        <%--Start :: Iterating user review --%>
                        <div class="rev_prof ${reviewDetails.userNameInitialColourcode}">
                          ${reviewDetails.userNameInt}
                        </div>
                        <div class="rlst_rht">
                          <div class="rev_name">
                            ${reviewDetails.userName}
                          </div>
                          <div class="rev_dte">
                            ${reviewDetails.createdDate}
                          </div>
                        </div>
                      </div>
                    </div>
                   <div class="revlst_rht fl">
                     <div class="rlst_wrap">
                       <h3><a target="_blank" href="${reviewDetails.uniHomeURL}" class="rev_uni link_blue">
                           ${reviewDetails.collegeNameDisplay}
                           </a>
                       </h3>
                       <c:if test="${reviewDetails.courseDeletedFlag eq 'TRUE'}">
                         <c:if test="${not empty reviewDetails.courseName}">
                         <h3>
                           ${reviewDetails.courseName}
                         </h3>
                         </c:if>
                       </c:if>
                     <c:if test="${reviewDetails.courseDeletedFlag ne 'TRUE'}">
                       <h3>
                         <a class="rev_sub link_blue" target="_blank" href="${reviewDetails.courseDetailsURL}" title="${reviewDetails.courseName}">
                         ${reviewDetails.courseName}
                         </a>
                       </h3>
                     </c:if>
                     <c:if test="${not empty reviewDetails.userMoreReviewaList}">
          <div class="rev_bor">
            <div class="rvlbx_cnt">
               <c:set var="reviewCatName" value=""/>
                <c:forEach items="${reviewDetails.userMoreReviewaList}" var="innerReview">
                  <c:if test="${innerReview.questionTitle eq 'Overall Rating'}">
                    <c:if test="${innerReview.rating ne 'Y'}">
                      <div class="reviw_rating">
                        <div class="rate_new">
                          <span class="ov_txt cat_rat">OVERALL UNIVERSITY RATING</span>
                          <span class="ml5 rat rat${innerReview.rating}"></span>
                          <c:if test="${not empty innerReview.questionDesc}">
                            <div class="rw_qus_des">
                              ${innerReview.questionDesc}
                            </div>
                          </c:if>
                        </div>
                        <p class="rev_dec">
                          ${innerReview.answer}
                        </p>
                      </div>
                    </c:if>
                  </c:if>
                  <c:if test="${not empty innerReview.questionTitle}">
                    <c:if test="${innerReview.questionTitle ne 'Overall Rating'}">
                      <c:if test="${innerReview.rating ne 'Y'}"> 
                        <div class="reviw_rating">
                          <div class="rate_new">
                            
	                        <c:if test="${reviewCatName ne innerReview.questionTitle}">  
	                          <span class="cat_rat">${innerReview.questionTitle}</span>
	                          <c:if test="${empty innerReview.rating}">
	                            <span class="t_tip">
	                              N/A
	                              <span class="cmp">
	                                <div class="hdf5">
	                                  <spring:message code="wuni.review.without.stat.tooltip" />
	                                </div>
	                                <div class="line"></div>
	                              </span>
	                            </span>
	                          </c:if>
	                          <c:if test="${not empty innerReview.rating}">
	                            <span class="ml5 rat rat${innerReview.rating}"></span>
	                          </c:if>
	                        </c:if>
                            
                            <c:if test="${not empty innerReview.questionDesc}">
                              <div class="rw_qus_des">
                                ${innerReview.questionDesc}
                              </div>
                            </c:if>
                          </div>
                          <c:if test="${not empty innerReview.answer}">
                            <p class="rev_dec">
                              ${innerReview.answer}
                            </p>
                          </c:if>
                        </div>
                      </c:if>
                      <c:set var="reviewCatName" value="${innerReview.questionTitle}"/>
                    </c:if>
                    <c:if test="${innerReview.rating eq 'Y'}">
                      <div class="rev-reco">
                        <div class="tit6">
                          <span class="cat_rat">${innerReview.questionTitle}</span>
                        </div>
                        <a class="yes">
                        <i class="fa fa-check"></i>${innerReview.answer}
                        </a>
                      </div>
                    </c:if>
                  </c:if>
                </c:forEach>
              </div>
            </div>	
          </c:if>
      </div>
    </div>
  </div>
</div>
</c:forEach>
</c:if>
</div>
</div>
</div>
<input type="hidden" id="gaCollegeName" value="<%=gaCollegeName%>"/>
<input type="hidden" id="gaDimCollegeName" value="<%=gaDimCollegeName%>"/>
<input type="hidden" id="gaStudyLevelDescLB" value="<%=studyLevelDesc%>"/>
<input type="hidden" id="clearingFlag" value="${clearingFlag}">
<input type="hidden" id="mobileFlag" value="${mobileFlag}">
<input type="hidden" id="loggedInUserId" value="<%=new SessionData().getData(request, "y")%>" />
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/cpc/cpc_slider.js"></script>
<%-- <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp">
<jsp:param name="lightBoxPageType" value="${(landingPage eq 'SINGLE') ? 'single cpc landing lightbox' : (landingPage eq 'MULTIPLE') ? 'multi cpc landing lightbox' : ''}"/>
</jsp:include> --%>
<script id="lbscript">
function onScrollPause(){
	jq(document).ready(function(){
		jq(window).scroll(function(){
		  jq('video').each(function(){			 
			playAndPauseFn(jq(this).attr('id'),'pause');			
		  }); 
		});
	});
}
</script>
</c:if> 