<%@page import="WUI.utilities.CommonUtil" autoFlush="true" %>
<body class="md non_log">
    <div id="ol" style="display: none; position: fixed; top: 0px; left: 0px; z-index: 998; width: 768px; height: 100%;"></div>
    <div id="mbox" style="display: none; position: absolute; z-index: 999;"><span><div id="mbd"></div></span></div>
    <%-- GTM script included under body tag --%>
	<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
		<jsp:param name="PLACE_TO_INCLUDE" value="BODY"/>
	</jsp:include>
	<%-- GTM script included under body tag --%>
    <div class="wu_home cpc_landing ${clearingFlag eq 'Y' ? 'clr_cpc' : ''}">
      <div class id="shadowId"></div> 
        <header class="md clipart">
            <div class="large"></div>
            <div class="">
                <div class="content-bg">
                    <div id="desktop_hdr">                        
                       <jsp:include page="/jsp/cpc/include/multipleCPCHeader.jsp" />
                    </div>
                </div>
            </div>
        </header>
        <jsp:include page="/jsp/cpc/include/multipleCPCTopContent.jsp" />
		<jsp:include page="/jsp/cpc/include/cpcMiddleContent.jsp" />
        <p class="bk_to_top" style="display:none" id="back-top">
            <a href="#top"> <img src="<%=CommonUtil.getImgPath("/wu-cont/images/bk_2_top.png", 1)%>" alt="go to top"> </a>
        </p>
        <input type="hidden" id="smartBannerJs" name="smartBannerJs" value="jquery.smartbanner.js">
    </div>
    <input type="hidden" id="pageType" value="MULTIPLE" />
    <input type="hidden" id="campaign" value="${campaign}" />
    <input type="hidden" id="clearingFlag" value="${clearingFlag}">
    <input type="hidden" id="mobileFlag" value="${mobileFlag}">
    <jsp:include page="/jsp/cpc/include/commonLogging.jsp">
      <jsp:param name="pageType" value="MULTIPLE"/>
    </jsp:include>
</body>

