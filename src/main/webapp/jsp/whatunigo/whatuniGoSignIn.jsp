<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants" %>
 <%    
    String courseId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("COURSE_ID"))) ? (String)request.getAttribute("COURSE_ID") : "";
    String opportunityId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("OPPORTUNITY_ID"))) ? (String)request.getAttribute("OPPORTUNITY_ID") : "";
    String queryString = "&courseId=" + courseId + "&opportunityId=" + opportunityId;
    String browserip = GlobalConstants.WHATUNI_SCHEME_NAME + new CommonUtil().getProperties().getString("review.autocomplete.ip");
    String collegeName = (String)request.getAttribute("hitbox_college_name");
    String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(collegeName);
    String collegeId = (String)request.getAttribute("COLLEGE_ID");
    String refererURL = (request.getAttribute("REFERER_URL") != null) ? (String)request.getAttribute("REFERER_URL") : "";
  %>  
  <body>
    <header class="md clipart cmm_hdr">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
             <jsp:include page="/jsp/common/wuHeader.jsp"></jsp:include>             
          </div>
        </div>
      </div>
    </header>
    <section class="cmm_cnt">
      <%-- Timer Sectio --%>
      <%--<jsp:include page="/jsp/whatunigo/include/timer.jsp"></jsp:include> --%>
      <%-- Timer Sectio --%>
      <div class="cmm_col_12 chse_cnt no_topsec cmm_sgnup cmm_sgnin">
        <div class="cmm_wrap">
          <div class="cmm_row">
            <%-- Uni List --%>
            <jsp:include page="/jsp/whatunigo/include/applyNowCourseDetails.jsp"></jsp:include>
            <%-- Uni List --%>
            <%-- SignUp --%>
            <form class="fl basic_inf pers_det sgn_up sgn_in">
              <div class="pers_lst pt-20">
                <h5 class="cmn-tit">Sign in</h5>
                <fieldset class="fl tx-bx">
                  <!--<input type="text" name="Email" class="frm-ele">-->                  
                  <input type="text" name="email" maxlength="120" value="" onkeydown="hideEmailDropdown(event, 'autoEmailId');" onblur="hideTooltip('emailYText');" onfocus="showTooltip('emailYText');" id="email" autocomplete="off" class="frm-ele">
                  <label for="email" class="lbco">Email*</label>
                  <p class="fail-msg" id="loginemail_error" style="display:none;"></p>                  
                </fieldset>
                <fieldset class="fr tx-bx pwd_feld">
                  <input type="password" maxlength="20" onkeypress="javascript:if(event.keyCode==13){return valCmmtUserLogin()}" name="password" id="password" class="frm-ele">
                  <label for="password" class="lbco">Password*</label>
                  <%--<span class="pwd_eye"><a href="javascript:void(0);" onclick="showPassword('eyeIcon', 'password');" onmousedown="showPass('eyeIcon', 'password');" onmouseup="hidePass('eyeIcon', 'password');"><i id="eyeIcon" class="fa fa-eye" aria-hidden="true"></i></a></span>--%>
                  <span class="pwd_eye"><a href="javascript:void(0);" onclick="showPassword('eyeIcon', 'password');"><i id="eyeIcon" class="fa fa-eye" aria-hidden="true"></i></a></span>
                  <p class="fail-msg" id="loginpass_error" style="display:none;"></p>
                </fieldset>                
              </div>
              <p class="fail-msg fl_w100" id="loginerrorMsg" style="display:none;color:red;"> </p>
              <%-- Remember me --%>
              <div class="fl qua_cnf rem_me">
                <div class="btn_chk">
                  <span class="chk_btn">
                    <input type="checkbox" value="Y" id="loginRemember">
                    <span class="checkmark grey"></span>
                  </span>
                  <p><label for="loginRemember" class="chkbx_100">Remember me (Don't use this on a public computer)</label></p>
                </div>
                <p class="cnfrm f-16" style="display:none">The requirement for this course is at least <span class="dotd">120</span> UCAS points</p>
                <div class="btn_cnt pt-40">
                 <div class="fl">
                    <a href="<%=GlobalConstants.FORGOT_PASSWORD_URL%>" title="Forgot password" class="fl bck">Forgot password</a>
                  </div>            
                  <div class="fr">
                    <a href="javascript:void(0)" onclick="valCmmtUserLogin();" class="fr bton">SIGN IN <i class="fa fa-long-arrow-right"></i></a>
                    <p id="loadinggifreg" class="fr pt-10 clear_rht" style="display:none;" ><span id="loadgif"><img title="" alt="Loading" src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/indicator1.gif", 0)%>" class="loading"/></span></p> 
                  </div>                  
                </div>
              </div>
              <%-- Remember me --%>
              <%-- Social login --%>
              <jsp:include page="/jsp/whatunigo/include/socialMediaLogin.jsp">
                <jsp:param name="PAGE_NAME" value="sign in" />
              </jsp:include> 
              <%-- Social login --%>
    
              <div class="btn_cnt pt-40 hvnt_sup">            
                <div class="fl">
                  <a href="<%=GlobalConstants.CMMT_SIGN_UP_URL + queryString%>" title="Haven't signed up yet" class="fl bck">Haven't signed up yet</a>
                </div>
              </div>
            </form>
            <%-- SignUp --%>
          </div>
        </div>
      </div>  
    </section>
    <%--<input type="hidden" id="refererUrl" name="refererUrl" value="<%=(request.getHeader("referer") !=null ? request.getHeader("referer") : browserip+"/degrees/home.html")%>" />--%>
    <input type="hidden" id="refererUrl" value="<%=refererURL%>"/>
    <input type="hidden" id="collegeName" value="<%=gaCollegeName%>"/>
    <input type="hidden" id="collegeId" value="<%=collegeId%>"/>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
  </body>