<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants"%>
<%
  String regionClassName = "subrw";
  String imagesName = GlobalConstants.WU_CONT + "/images/flag_all_location.svg";
  String loadingImg = GlobalConstants.WU_CONT + "/images/hm_ldr.gif";
%>
<div id="loadingImg" class="cmm_ldericn" style="display:none;"><img alt="wugo-loading-image" src="<%=CommonUtil.getImgPath(loadingImg, 0)%>"></div>
<header class="md clipart cmm_hdr">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>
    </div>
  </div>
</header>
<section class="cmm_cnt ss_cnt">
  <div class="brd_crumb timer_sec">
    <div class="cmm_col_12">
      <div class="cmm_wrap">
        <div class="brc_cnt lft fl">
          <div class="brc_row1">
            <span class="bc_hdtxt">Clearing search</span><span class="bc_stpno">Step 1 of 2</span>
          </div>
          <div class="brc_row2">
            <ul>
              <li class="stp_pro disbld"><a href="#"><span class="bcno">1</span></a></li>
              <li class="disbld"><a href="#"><span class="bcno">2</span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="cmm_col_12 binf_cnt ssrch_cnt">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <form name="" id="courseSearchForm" action="/home.html" class="fl_w100 basic_inf pers_det cont_det sub_srch ssrch_loc" onsubmit="javascript:return courseSearchSubmit(courseName);">
          <div class="pers_lst cof_cnt ecrs_fld pt-30">
            <fieldset class="fr tx-bx psc_cnt"  id="ajaxSrchDivId">
              <div class="fl w-100">    
                <input type="text" name="courseName" id="courseName" value='First, choose a subject or university' autocomplete="off"
                       onkeydown="clearCourseName(event,this)"  
                       onkeyup="autoCompleteCourseKeywordBrowse(event,this);clearPeppleOnKeyUp();"   
                       onclick="clearCourseDefaultTEXT(this)" 
                       onblur="setCourseDefaultTEXT(this)"
                       onfocus="clearCourseDefaultTEXT(this)"
                       class="frm-ele"
                       maxlength="4000"
                       onkeypress="javascript:if(event.keyCode==13){return courseSearchSubmit(courseName)}" />
                <input type="hidden" id="courseName_hidden" value="" />
                <input type="hidden" id="courseName_id" value="" />
                <input type="hidden" id="courseName_url" value="" />
                <input type="hidden" id="courseName_display" value="" />
              </div>
              
              <input type="hidden" id="coursematchbrowsenode" value="">  
            </fieldset>
            <div id="selectedSubjectDiv" class="csres_val fl_w100"></div>
          </div>
          
          <c:if test="${not empty requestScope.REGION_LIST}">
            <div class="pers_lst pop_sub locat fl_w100 csl_disbld csl_opac" id="locationDiv">
              <h4>Second, choose where you want to study (optional)</h4>
              <div class="loc_cntry fl_w100">
                <div class="subcnty clrfx">
                  <%String checked = "";%>
                  <c:forEach items="${requestScope.REGION_LIST}" var="region_list" varStatus="row">
                    <c:set var="rowValue" value="${row.index}"/>
                    <%checked = "checked='checked'";%>
                    <c:if test="${not empty region_list.regionName}">
                      <c:set value="${region_list.regionName}" var="regionNames"/>
                      <%
                          if("England".equalsIgnoreCase(pageContext.getAttribute("regionNames").toString())){
                            regionClassName = "subrw allEngSec";
                          }else{
                            regionClassName = "subrw";
                          }
                      %>
                    </c:if>
                    <c:if test="${not empty region_list.urlText}">
                      <c:set value="${region_list.urlText}" var="imageName"/>
                      <%
                          if(pageContext.getAttribute("imageName").toString() != null) {
                            imagesName = "/wu-cont/images/flag_" + pageContext.getAttribute("imageName").toString().toLowerCase() + ".svg";
                          }
                      %>
                    </c:if>
                    <div class="<%=regionClassName%>">
                      <input type="radio" class="subcountry_radio" name="country" id="country_<%=pageContext.getAttribute("rowValue").toString()%>" value='${region_list.urlText}' onclick="selectSubRegions('country_<%=pageContext.getAttribute("rowValue").toString()%>')">
                      <label for="country_<%=pageContext.getAttribute("rowValue").toString()%>"><span class="locTit" id="${region_list.urlText}">${region_list.regionName}</span><span class="cntryFlag"><img src="<%=CommonUtil.getImgPath(imagesName, 0)%>" alt="country flag" /></span></label>
                      <c:if test="${not empty region_list.subRegionList}">
                        <div class="allEngSub fl_w100" id="sub_region" style="display:none">
                          <div class="subcnty clrfx">
                            <c:forEach items="${region_list.subRegionList}" var="sub_region_list" varStatus="rowReg">
                              <div class="subrw">
                                <input type="radio" class="subcountry_radio" name="subcountry" id="subcountry_<%=pageContext.getAttribute("rowReg").toString()%>" onclick="selectSubRegions('subcountry_<%=pageContext.getAttribute("rowReg").toString()%>')" value='${sub_region_list.urlText}'>
                                <label for="subcountry_<%=pageContext.getAttribute("rowReg").toString()%>"><span class="locTit" id="${sub_region_list.urlText}">${sub_region_list.regionName}</span></label>
                              </div>
                            </c:forEach>
                          </div>
                        </div>
                      </c:if>
                    </div>                    
                  </c:forEach>
                </div>
              </div>
            </div>
          </c:if>
          <input type="hidden" id="country_hidden" name="country_hidden" value=""/>
          <input type="hidden" id="courseName_url" name="courseName_url" value=""/>
          <input type="hidden" id="keyword_hidden" name="keyword_hidden" value=""/>
          <input type="hidden" id="keywordSearchFlag" name="keywordSearchFlag" value=""/>
          <input type="hidden" id="subjectLandingPage" name="subjectLandingPage" value="subjectLandingPage"/>
          <div class="btn_cnt ssVcbtn pt-40 csl_disbld" id="viewCourseDiv">            
            <div class="fr">
              <a  href="javascript:void(0);" title="view course" onclick="viewCourse(courseName, country_hidden);" class="fr bton">Next<i class="fa fa-long-arrow-right"></i></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<jsp:include page="/jsp/common/wuFooter.jsp" />  