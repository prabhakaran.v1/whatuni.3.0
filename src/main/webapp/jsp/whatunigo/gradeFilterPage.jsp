<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import= "WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator"%>

<% int loop = 0;
int count = 0, subCount = 0, subLen = 0; 
  String qualSub = "";
  String qualId = ""; 
 
  String ajaxActionName = "QUAL_SUB_AJAX";
  String formStatusId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("pageStatus"))) ? (String)request.getAttribute("pageStatus") : "";
  String removeBtnFlag = "display:block";
   String subject = !GenericValidator.isBlankOrNull((String)request.getAttribute("subject")) ? (String)request.getAttribute("subject") : "";
  String selectFlag = "";
  String gcseSelectFlag = "";
  String newGCSESelectFlag = "";
  String oldGCSESelectFlag = "";
  String gcseSelOption = ": 9-1";
   String location = !GenericValidator.isBlankOrNull((String)request.getAttribute("location")) ? (String)request.getAttribute("location") : "";
   String q =!GenericValidator.isBlankOrNull((String)request.getAttribute("q")) ? (String)request.getAttribute("q") : "";
   String university = !GenericValidator.isBlankOrNull((String)request.getAttribute("university")) ? (String)request.getAttribute("university") : "";
   String qualCode = !GenericValidator.isBlankOrNull((String)request.getAttribute("qualCode")) ? (String)request.getAttribute("qualCode") : "";
   String referralUrl = !GenericValidator.isBlankOrNull((String)request.getAttribute("referralUrl")) ? (String)request.getAttribute("referralUrl") : "";
   String userId = (String)request.getAttribute("userId");
   String subjectSessionId = (String)request.getAttribute("subjectSessionId");
   String ucaspoint = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasPoint")) ? (String)request.getAttribute("ucasPoint") : "0";
   String ucasMaxPoints = (String)request.getAttribute("ucasMaxPoints");
   String styleFlag = "display:block"; String styleFlag2 = "display:none"; String styleFlag3 = "display:none"; 
   String removeLink =   CommonUtil.getImgPath("/wu-cont/images/cmm_close_icon.svg",0);
   String score = !GenericValidator.isBlankOrNull((String)request.getAttribute("score")) ? (String)request.getAttribute("score") : "";
   String minScore = "0";
   String maxScore = ucaspoint;
 String qualifications = "";
  String addQualStyle = "display:none";
  String addSubStyle = "display:block";
  int addSubLimit = 6;
  String removeQualStyle = "display:none";
  int userQualListSize = (!GenericValidator.isBlankOrNull((String)request.getAttribute("userQualSize"))) ? Integer.parseInt((String)request.getAttribute("userQualSize")) : 0;
  String addQualId = "level_3_add_qualif";
  String callTypeFlag = "";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("score"))) {
     String[] scoreArr = score.split(",");
     minScore = scoreArr[0];
     maxScore = scoreArr[1];
   }
  String gsceFlag = "FALSE";
%>
<section class="cmm_cnt">
  <div class="brd_crumb timer_sec">
    <div class="cmm_col_12">
      <div class="cmm_wrap">
        <div class="brc_cnt lft fl">
          <div class="brc_row1">
            <span class="bc_hdtxt">Qualifications</span><span class="bc_stpno">Step 2 of 2</span>
          </div>
          <div class="brc_row2">
            <ul>
              <li class="stp_cmplt disbld"><a href="javascript:void(0);"><span class="bcno">1</span></a></li>
              <li class="stp_pro disbld"><a href="javascript:void(0);"><span class="bcno">2</span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="cmm_col_12 binf_cnt srqual_mn">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <div class="cmm_unilst qual_cnt">
          <p class="ql_dec">UCAS points is only the first level of requirements. Some courses require specific grade combinations and specific subjects. So make sure you enter all of your qualifications correctly to guarantee the best match.</p>
        </div>
        <form class="fl basic_inf pers_det cont_det qualif slqualif" id="gradeForm">
          <div class="ucas_midcnt">
            <div id="ucas_calc">
              <div class="ucas_mid">
              <c:if test="${not empty requestScope.userQualList}">
                <c:forEach var="userQualList" items="${requestScope.userQualList}" varStatus="i" >
                   <c:set var="indexIValue" value="${i.index}"/>
                  <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue").toString()));%>
                  <c:if test="${userQualList.entry_qual_desc eq 'GCSE'}">
                    <%count = 17;addSubLimit=20;%>
                  </c:if>
                  <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                    <%addQualId = "level_3_add_qualif";%>
                  </c:if>
                  <c:if test="${userQualList.entry_qual_desc eq 'GCSE'}">
                    <%addQualId = "level_2_add_qualif";%>
                  </c:if>
                  <div class="add_qualif" id="add_qualif_<%=count%>" data-id="<%=addQualId%>">
	            <div class="ucas_refld">  
	                 <c:if test="${not empty userQualList.entry_qual_level}">                
		                    <c:if test="${userQualList.entry_qual_level eq '2'}">
                        <h3 class="un-tit">Level 2: GCSE 
                        <a href="javascript:void(0)" class="blu-lnk rltv bsc">
                          <span>Why do we need this?</span>
                          <span id="postcodeText" class="cmp adjst">
                          <div class="hdf5"></div>
                          <div>Some courses require you to have studied and achieved a pass grade for specific GCSE subjects. Others require you to have achieved a pass of a minimum number of GCSE subjects. It's therefore important to tell us all of your qualifications so that we can provide you with the best matching courses.</div>
                          <div><strong>Please Note:</strong> GCSEs are the only Level 2 qualifications used by Whatuni Go to match you with courses. If you have different Level 2 Qualifications, you can skip this stage and submit your search. </div>
                          <div class="line"></div>
                          </span>
                        </a>
                        </h3>
                        <p class="ql_dec">Please enter your GCSE grades below. <b>If you have BOTH numerical (9-1) and alphabetical (A*-G) grades for your GCSEs. Please select GCSE: A*-G and convert your numerical grades as follows: (9 = A*, 7-8 = A, 6 = B, 4-5 = C, 3 = D).</b> </p>
                        </c:if>
                      </c:if>
                      <div class="l3q_rw" id="qual_level_<%=count%>">		
		       <div class="adqual_rw">
			<div class="ucas_row">
		     	<h5 class="cmn-tit txt-upr"><c:if test="${userQualList.entry_qual_level eq '2'}">GRADE TYPE</c:if><c:if test="${userQualList.entry_qual_level ne '2'}"> Qualification</c:if> </h5>
			<fieldset class="ucas_fldrw">
			  <div class="remq_cnt">
		          <div class="ucas_selcnt">
			  <fieldset class="ucas_sbox">
                            <c:set var="qualDesc" value="${userQualList.entry_qual_desc}"/>
                            <jsp:scriptlet> qualifications = pageContext.getAttribute("qualDesc").toString() + ","; </jsp:scriptlet>
                            <c:if test="${userQualList.entry_qual_desc eq 'GCSE'}">
                              <c:if test="${userQualList.gcse_grade_flag eq 'OLD'}">
                                <jsp:scriptlet>gcseSelOption = ": A*-G";</jsp:scriptlet>
                              </c:if>
                                <span class="sbox_txt" id="qualspan_<%=count%>">${userQualList.entry_qual_desc} <%=gcseSelOption%></span>
                            </c:if>
                            <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                              <span class="sbox_txt" id="qualspan_<%=count%>">${userQualList.entry_qual_desc }</span>
			                     </c:if>
                            <i class="fa fa-angle-down fa-lg"></i>
			  </fieldset> 
			                    <c:if test="${userQualList.entry_qual_level ne '2'}">
			  <select name="qualification<%=count%>" onchange="appendQualDiv(this, '', '<%=count%>');" id="qualsel_<%=count%>" class="ucas_slist">
			    <option value="0">Please Select</option>
			                        <c:forEach var="qualList" items="${requestScope.qualificationList}" >
                                <c:set var="selectQualId" value="${userQualList.entry_qual_id}"/>
                                <jsp:scriptlet>selectFlag = "";</jsp:scriptlet>
                                <c:if test="${qualList.entry_qual_id eq selectQualId}">
                                  <jsp:scriptlet>selectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                </c:if>
                                <c:if test="${qualList.entry_qualification ne 'GCSE'}">
                                  <c:if test="${empty qualList.entry_qual_id}">
                                    <optgroup label="${qualList.entry_qualification}"></optgroup>
                                  </c:if>
                                  <c:if test="${not empty qualList.entry_qual_id}">
                                 <option value="${qualList.entry_qual_id}" <%=selectFlag%>>${qualList.entry_qualification} </option>
                               </c:if>                                   
                              </c:if>
                             </c:forEach>
			  </select>
                          <div id="qual_sub_grd_hidId">
                            <c:forEach var="grdQualifications" items="${requestScope.qualificationList}" >
                              <c:if test="${grdQualifications.entry_qualification ne 'GCSE'}">                               
                                <c:if test="${not empty grdQualifications.entry_qual_id}">
                                  <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}" name="qualSubj_${grdQualifications.entry_qual_id }" value="${grdQualifications.entry_subject}>" />
                                  <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}" name="" value="${grdQualifications.entry_grade}" />
                                </c:if>
                              </c:if>                              
                            </c:forEach>
                           </div>
                          </c:if>
                          <c:if test="${userQualList.entry_qual_level eq '2'}">
                            <select name="qualification<%=count%>" onchange="appendQualDiv(this, '', '<%=count%>');" id="qualsel_<%=count%>" class="ucas_slist">
			                        <option value="0">Please Select</option>
			                        <c:forEach var="qualList" items="${requestScope.qualificationList}" >
                                <c:if test="${qualList.entry_qualification eq 'GCSE'}">
                                  <c:set var="selectQualId2" value="${userQualList.gcse_grade_flag}"/>
                                  <jsp:scriptlet>gcseSelectFlag = "";</jsp:scriptlet>
                                  <c:if test="${userQualList.gcse_grade_flag eq 'OLD'}">
                                    <jsp:scriptlet>gcseSelectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                  </c:if>
                                  <option value="${qualList.entry_qual_id}_gcse_old_grade" <%=gcseSelectFlag%>>${qualList.entry_qualification}: A*-G </option>
                                   <jsp:scriptlet>gcseSelectFlag = "";</jsp:scriptlet>
                                   <c:if test="${userQualList.gcse_grade_flag eq 'NEW'}">
                                    <jsp:scriptlet>gcseSelectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                  </c:if>
                                  <option value="${qualList.entry_qual_id}_gcse_new_grade" <%=gcseSelectFlag%>>${qualList.entry_qualification}: 9-1 </option>
                                </c:if>
                              </c:forEach>
			      </select>
			                        <c:forEach var="grdQualifications" items="${requestScope.qualificationList}" >
                                <c:if test="${grdQualifications.entry_qualification eq 'GCSE'}">                                                              
                                  <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}_gcse" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                  <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_new_grade" name="" value="${grdQualifications.entry_grade}" />
                                  <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_old_grade" name="" value="${grdQualifications.entry_old_grade}" />
                                </c:if>
                              </c:forEach>
                            </c:if>            
                      	</div>
                      </div>
                      <c:if test="${userQualList.entry_qual_level ne '2'}">
                        <%if(count != 0){removeQualStyle = "display:block";}%>
                        <div class="add_fld" style="<%=removeQualStyle%>">
                           <a id="remQual<%=count%>" onclick="removeQualEntryReq('add_qualif_<%=count%>', '<%=count%>');" class="btn_act1 bck f-14 pt-5">
                             <span class="hd-mob">Remove Qualification</span>
                           </a>
                        </div>	
                      </c:if>
		    </fieldset>
		  </div>
                  <div class="subgrd_fld" id="subGrdDiv<%=count%>">
                    <div class="ucas_row grd_row" id="grdrow_<%=count%>">
                    <fieldset class="ucas_fldrw quagrd_tit">
                      <div class="ucas_tbox tx-bx">
                        <h5 class="cmn-tit txt-upr qual_sub">Subject</h5>
                      </div>
		      <div class="ucas_selcnt rsize_wdth">
                        <h5 class="cmn-tit txt-upr qual_grd">Grade</h5>
		      </div>
                    </fieldset>
                    <c:forEach var="subjectList" items="${userQualList.qual_subject_list}" varStatus="j" >
                       <c:set var="indexJValue" value="${j.index}"/>
                      <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue").toString()));%>
                       <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
 			<fieldset class="ucas_fldrw">
                          <div class="ucas_tbox w-390 tx-bx">
			    <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="sub_${subjectList.entry_subject_id}" onblur="getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="${ subjectList.entry_subject}" class="frm-ele" autocomplete="off">															
                            <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                          <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                        </div>
                      </div>
		      <div class="ucas_selcnt rsize_wdth">
		        <div class="ucas_sbox mb-ht w-84" id="1grades1">
                           <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">${subjectList.entry_grade}</span><i class="fa fa-angle-down fa-lg"></i>
                         </div>
		         <select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" id="qualGrd_<%=count%>_<%=subCount%>"></select>
                         <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=count%>_<%=subCount%>','${subjectList.entry_grade}'); </script>
                         <c:if test="${userQualList.entry_qual_level ne '2'}">
                           <input class="subjectArr" data-id="level_3" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${userQualList.entry_qual_id}" value="${subjectList.entry_subject_id}" />
                         </c:if>
                         <c:if test="${userQualList.entry_qual_level eq '2'}">
                           <input class="subjectArr" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${userQualList.entry_qual_id}" value="${subjectList.entry_subject_id}" />
                         </c:if>                           
                          <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_${userQualList.entry_qual_id}" value="${subjectList.entry_grade}" />
                          <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${userQualList.entry_qual_id}" value="${userQualList.entry_qual_id}" />
                          <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${userQualList.entry_qual_id}" value="<%=count+1%>" />
                        </div>
                        <%if(subCount == 0){
                           removeBtnFlag = "display:none";
                         }else{
                           removeBtnFlag = "display:block";
                         }
                         if(subCount == 1){
                         %>
                         <script>jq("#remSubRow<%=count%>0").show();</script>
                        <%}%>
			<div class="add_fld"> 
                          <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5">
                            <span class="hd-mob">Remove</span>
                            <span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"/></span>
                          </a>
                         </div>
                       </fieldset>
                     </div>
                   </c:forEach>
                   <%if((subCount + 1) < addSubLimit){
                      addSubStyle = "display:block";
                    }else{
                      addSubStyle = "display:none";
                    }%>
                    <div class="ad-subjt" id="addSubject_<%=count%>" style="<%=addSubStyle%>">
                      <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('${userQualList.entry_qual_id}','countAddSubj_<%=count%>', '<%=count%>');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                    </div>
                   </div>
                  </div>
                  <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                  <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
		</div>
              </div>
              <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                <%if(count == (userQualListSize-1)){addQualStyle = "display:none";}%>                    
		              <div class="add_qualtxt" id="addQualBtn_<%=count%>" style="<%=addQualStyle%>">
                  <p class="aq_hlptxt">Studied more than one qualification? </p>
                  <a href="javascript:void(0);" onclick="addQualEntryReq('<%=count+1%>')" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> ADD A QUALIFICATION</a></div>	
	       </c:if>
               </div>
	     </div>
           </c:forEach>
           </c:if>

         <c:forEach var="userQualList" items="${requestScope.userQualList}" >
           <c:if test="${userQualList.entry_qual_desc eq 'GCSE'}">
             <%gsceFlag = "TRUE"; %>
           </c:if>
         </c:forEach>   
         <%if(!("TRUE".equals(gsceFlag))){%> 
            <!--Qualification Repeat Field start 2-->
              <c:if test="${not empty requestScope.qualificationList}">
                <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" >
                <c:set var="indexIValue" value="${i.index}"/>
                  <c:if test="${qualList.entry_qual_id eq '17'}">
                  <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue").toString()));%>
                  <div class="add_qualif">
                    <div class="ucas_refld">
                      <h3 class="un-tit">
                        Level 2: GCSEs
                        <a href="javascript:void(0)" class="blu-lnk rltv bsc">
                          <span>Why do we need this?</span>
                          <span id="postcodeText" class="cmp adjst">
                            <div class="hdf5"></div>
                          <div>Some courses require you to have studied and achieved a pass grade for specific GCSE subjects. Others require you to have achieved a pass of a minimum number of GCSE subjects. It's therefore important to tell us all of your qualifications so that we can provide you with the best matching courses.</div>
                          <div><strong>Please Note:</strong> GCSEs are the only Level 2 qualifications used by Whatuni Go to match you with courses. If you have different Level 2 Qualifications, you can skip this stage and submit your search. </div>
                            <div class="line"></div>
                          </span>
                        </a>
                      </h3>
                      <p class="ql_dec">Please enter your GCSE grades below. <b> If you have BOTH numerical (9-1) and alphabetical (A*-G) grades for your GCSEs. Please select GCSE: A*-G and convert your numerical grades as follows: (9 = A*, 7-8 = A, 6 = B, 4-5 = C, 3 = D).</b> </p>
                      <!-- L3 Qualification Row  -->
                      <div class="l3q_rw" id="qual_level_<%=count%>">
                        <!-- Add Qualification & Subject 1-->		
                        <!-- Add Qualification & Subject 2-->		
                        <div class="adqual_rw" data-id="level_2_add_qualif">                          
                            <div class="ucas_row">    
                            <h5 class="cmn-tit txt-upr">GRADE TYPE</h5>
                              <fieldset class="ucas_fldrw">
                                <div class="remq_cnt">
                                  <div class="ucas_selcnt">
                                    <fieldset class="ucas_sbox">
                                      <c:set var="selectQualLeve2Name" value="${qualList.entry_qualification}"/>
                                      <% String selectQualLeve2Name = pageContext.getAttribute("selectQualLeve2Name").toString();%>
                                      <span class="sbox_txt" id="qualspan_<%=count%>"><%=selectQualLeve2Name%>: 9-1 </span>
                                      <i class="fa fa-angle-down fa-lg"></i>
                                    </fieldset>
                                     <select name="qualification1" onchange="appendQualDiv(this, '', '<%=count%>', 'new_entry_page');" id="qualsel_<%=count%>" class="ucas_slist">
                                     <option value="0">Please Select</option>
                                       <c:forEach var="qualListLevel2" items="${requestScope.qualificationList}">
                                         <c:if test="${qualListLevel2.entry_qualification eq 'GCSE'}">
                                             <jsp:scriptlet>gcseSelectFlag = "selected=\"selected\"";</jsp:scriptlet>                              
                                           <option value="${qualListLevel2.entry_qual_id}_gcse_new_grade" <%=gcseSelectFlag%>>${qualListLevel2.entry_qualification}: 9-1 </option>                               
                                           <option value="${qualListLevel2.entry_qual_id}_gcse_old_grade" >${qualListLevel2.entry_qualification}: A*-G</option>
                                          </c:if>
                                        </c:forEach>
                                      </select>
                                      <c:forEach var="grdQualifications" items="${requestScope.qualificationList}">
                                         <c:if test="${grdQualifications.entry_qualification eq 'GCSE'}">                                                             
                                           <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}_gcse" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                           <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_new_grade" name="" value="${grdQualifications.entry_grade}" />
                                           <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_old_grade" name="" value="${grdQualifications.entry_old_grade}" />
                                         </c:if>
                                      </c:forEach>
                                   </div>
                                 </div>
                                <%--
                                <div class="add_fld">
                                  <a id="1rSubRow3" onclick="javascript:removeSubRow(1,3);" class="btn_act1 bck f-14 pt-5">
                                    <span class="hd-mob">Remove Qualification</span>
                                  </a>
                                </div>
                                --%>
                              </fieldset>
                              <div class="err" id="qualSubId_error" style="display:none;"></div>
                            </div>                          
                          <div class="subgrd_fld" id="subGrdDiv<%=count%>">
                            <div class="ucas_row grd_row" id="grdrow_<%=count%>">                            
                              <fieldset class="ucas_fldrw quagrd_tit">
                                <div class="ucas_tbox tx-bx">
                                  <h5 class="cmn-tit txt-upr qual_sub">Subject</h5>
                                </div>
                                <div class="ucas_selcnt rsize_wdth">
                                  <h5 class="cmn-tit txt-upr qual_grd">Grade</h5>
                                </div>
                              </fieldset>
                              <c:set var="selectQualLevel2SubLen" value="${qualList.entry_subject}"/>
                              <%int  selectQualLevel2SubLen = Integer.parseInt(pageContext.getAttribute("selectQualLevel2SubLen").toString());
                                subLen = selectQualLevel2SubLen;%> 
                              <c:forEach var="subjectList" items="${qualificationList}" varStatus="j" end="<%=subLen%>">
                                <c:set var="indexJValue" value="${j.index}"/>
                              <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue").toString()));%>
                                <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
                                  <fieldset class="ucas_fldrw">
                                    <div class="ucas_tbox w-390 tx-bx">
                                      <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="subIp_<%=subCount%>" onblur="getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="" class="frm-ele" autocomplete="off">															
                                      <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                                      <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                                      </div>
                                    </div>
                                    <div class="ucas_selcnt rsize_wdth">
                                      <div class="ucas_sbox mb-ht w-84" id="1grades1">
                                        <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">9</span><i class="fa fa-angle-down fa-lg"></i>
                                      </div>
                                      <select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" id="qualGrd_<%=count%>_<%=subCount%>"></select>                                      
                                      <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=count%>_<%=subCount%>','${subjectList.entry_grade}'); </script>
                                      
                                      <input class="subjectArr" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${qualList.entry_qual_id}" value="" />                                
                                      <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_${qualList.entry_qual_id}" value="9" />
                                      <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${qualList.entry_qual_id}" value="${qualList.entry_qual_id}" />
                                      <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${qualList.entry_qual_id}" value="<%=count+1%>" />
                                    </div>
                                    <%if(subCount == 0){
                                      removeBtnFlag = "display:none";
                                    }else{
                                      removeBtnFlag = "display:block";
                                    }
                                    if(subCount == 1){%>                                  
                                      <script>jq("#remSubRow<%=count%>0").show();</script>
                                    <%}%> 
                                    <div class="add_fld"> <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"></span></a> </div>
                                  </fieldset>
                                </div>                                
                              </c:forEach>                        
                              <div class="ad-subjt" id="addSubject_<%=count%>" style="display:block">
                                <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('${qualList.entry_qual_id}','countAddSubj_<%=count%>', '<%=count%>');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                              </div>                            
                          </div>
                        </div>
                        <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <!-- Add Qualification & Subject 2 -->
                      </div>
                    </div>
                    </div>
                  </div>
                </c:if>
                </c:forEach>
                </c:if>
                <!--Qualification Repeat Field end 2 -->
             <%}%>           
	 </div>
         <div class="pt-25 qua_cnf">
           <h3 class="un-tit p-0 f-20 pt-0">View courses from <span id="minSliderVal"><%=minScore%></span> to <span id="maxSliderVal"><%=maxScore%></span> points</h3>
             <div class="rngsl_cnt">
               <div class="rng_wrap">
                 <input type="text" id="cmm_rngeSlider" name="cmm_rngeSlider" value="" class="irs-hidden-input" readonly="" style="display:none">
               <div class="ucsre_val">Your UCAS score:<span id="ucasPt"> <%=ucaspoint%> </span> points <i id="ucasArrow" style="left:-2%" class="fa fa-long-arrow-right"></i></div>
              </div>
              <div class="fl add_qualtxt rngesl_sub" style="display: block;">
              </div>
            </div>
           </div>
           <div class="btn_cnt pt-40">
             <div class="fr">
               <a onclick="getMatchedCourse();" id="viewButton" class="fr bton">CALCULATE MATCHING COURSES <i class="fa fa-long-arrow-right"></i></a>
             </div>
           <div class="fl">
             <a href="/degrees/wugo/subject/search.html" class="fl bck"><i class="fa fa-long-arrow-left"></i> Back</a>
           </div>
         </div>
        </div>
       </div>
     </form>
   </div>
  </div>
 </div>
 <input type="hidden" id= "userUcasScore" value="<%=ucaspoint%>"/>
  <input type="hidden" id="country_hidden" name="country_hidden" value="<%=location%>"/>
  <input type="hidden" id="subject" name="subject" value="<%=subject%>"/>
  <input type="hidden" id="q" name="q" value="<%=q%>"/>
  <input type="hidden" id="university" name="university" value="<%=university%>"/>  
  <input type="hidden" id="qualCode" name="qualCode" value="<%=qualCode%>"/>  
  <input type="hidden" id="referralUrl" name="referralUrl" value="<%=referralUrl%>"/>  
  <input type="hidden" id="minSliderVal" name="minSliderVal" value="<%=minScore%>"/>
  <input type="hidden" id="maxSliderVal" name="maxSliderVal" value="<%=maxScore%>"/> 
  <input type="hidden" id="ucasMaxPoints" name="ucasMaxPoints" value="<%=ucasMaxPoints%>"/>
  <input type="hidden" id="removeLinkImg" value="<%=removeLink%>" />
  <input type="hidden" id="score" value="<%=score%>"/>
</section>
<script type="text/javascript">
  jq( document ).ready(function() {
    ucasArrow();
    onloadShowingAddQualBtn();
  });
</script> 

                                   
    