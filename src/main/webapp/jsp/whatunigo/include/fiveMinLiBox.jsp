<%@page import= "WUI.utilities.CommonUtil"%>
<%String errorMsg = "If you do not complete your application within the next 5 minutes you could lose your place on the course";%>
<div class="rvbx_shdw"></div>
  <div class="lbxm_wrp">
    <div class="ext-cont ins_entrq spec_need">
      <div class="revcls ext-cls"><a href="javascript:void(0);" onclick="closeLigBox();"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"></a></div>
      <div class="rev_lst ext">
        <div>
          <i class="fa fa-exclamation-triangle"></i>
          <h4>5 minutes remaining</h4>
          <p><%=errorMsg%></p>
        </div>
        <div class="btn_cnt insuf">
          <div class="btnsec_row fl_w100">
            <a href="javascript:void(0)" onclick="closeLigBox();" class="fl bton">OK</a>
          </div>
        </div>
      </div>
    </div>
  </div>