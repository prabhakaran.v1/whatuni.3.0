<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${not empty requestScope.articleContent}">
  <c:out value="${requestScope.articleContent}" escapeXml="false" />
</c:if>