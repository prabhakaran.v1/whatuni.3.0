<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<jsp:scriptlet>
  String courseId = (String)request.getAttribute("APPLY_NOW_COURSE_ID");
  String opportunityId = (String)request.getAttribute("APPLY_NOW_OPPORTUNITY_ID");
</jsp:scriptlet>
<section class="cmm_cnt mn_cngts">
  <%-- Timer Section --%>
  <div class="brd_crumb timer_sec">
    <div class="cmm_col_12">
      <div class="cmm_wrap">          
         <jsp:include page="/jsp/whatunigo/include/timer.jsp"></jsp:include> 
      </div>
    </div>
  </div>
  <%-- Timer Sectio --%>
  <div class="cong_cnt">
    <h1>Congratulations!</h1>
  </div>
  <div class="cmm_col_12 chse_cnt cngs_cnt nw_desn congrts">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <!-- Uni List -->
        <div class="cmm_tphd">
          <h1>
            <span class="hd_sp fl">You've got an Offer in Principle</span>
            <a href="javascript:void(0)" class="blu-lnk rltv bsc fl">
              <span class="wts_ths">What is this?</span>
              <span id="postcodeText" class="cmp adjst">
                <div class="hdf5"></div>
                <div>An 'Offer in Principle' means that the details you have given us regarding your grades meet the entry requirements given to us by the university for this course. 
                Once you've accepted your Offer in Principle, the uni will receive your application and will contact you to discuss any next steps and confirm your offer.</div>
                <div class="line"></div>
              </span>
            </a>
          </h1>
          <div class="clr"></div>
        </div>
        <!-- Uni List -->           
        <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/>              
        <!-- Uni List -->
        
        <div class="cmm_shrlst act_quikly">
          <h1>Accept this offer</h1>
          <h6>If you are happy that this course is the right one for you, then your next step is to accept this offer.</h6>
        </div>
        
        <c:if test="${not empty requestScope.APPLICANTION_COUNT}">
          <c:if test="${requestScope.APPLICANTION_COUNT ge '10'}">
            <div class="cmm_shrlst act_quikly">
              <h1>Act Quickly!</h1>
              <h6>${requestScope.APPLICANTION_COUNT} people have applied to this university today. If this course is your final choice, accept the offer now to avoid all the places being filled up.</h6>
            </div>
            </c:if>
        </c:if>
        <div class="btn_cnt pt-30">
          <div class="fr">
            <a href="javascript:void(0);" onclick="callAcceptOffer('<%=opportunityId%>', '<%=courseId%>');" class="fr bton">Accept this offer <i class="fa fa-long-arrow-right"></i></a>
          </div>
          <div class="fl">
              <a href="javascript:void(0);" onclick="callLightBox('BACK_TO_COURSE');" class="fl bck"><i class="fa fa-long-arrow-left"></i> Back to Course</a>
            </div> 
        </div>
      </div>
    </div>
  </div>
</section>