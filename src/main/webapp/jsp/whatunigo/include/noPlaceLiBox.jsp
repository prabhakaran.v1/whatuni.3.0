<%@page import= "WUI.utilities.CommonUtil"%>
<%String errorMsg = request.getParameter("errorMsg");%>
<div class="rvbx_shdw"></div>
  <div class="lbxm_wrp">
    <div class="ext-cont ins_entrq spec_need">
      <div class="revcls ext-cls"><a href="javascript:void(0);" onclick="gotoBackSearch();"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"></a></div>
      <div class="rev_lst ext">
        <div>
          <i class="fa fa-exclamation-triangle"></i>
          <h4>Error!</h4>
          <p><%=errorMsg%></p>
        </div>
        <div class="btn_cnt insuf">
          <div class="btnsec_row fl_w100">
            <a href="javascript:void(0)" onclick="gotoBackSearch();" class="bck">Back to course</a>
          </div>
        </div>
      </div>
    </div>
  </div>
