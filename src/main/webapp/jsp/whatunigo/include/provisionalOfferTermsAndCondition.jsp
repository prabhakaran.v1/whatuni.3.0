<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<section class="cmm_cnt mn_tandc">
  <%-- Timer Sectio --%>
    <div class="brd_crumb timer_sec">
      <div class="cmm_col_12">
        <div class="cmm_wrap">
          <jsp:include page="/jsp/whatunigo/include/timer.jsp"/>
        </div>
      </div>
    </div>
  <%-- Timer Sectio --%>
  <div class="cmm_col_12 chse_cnt nw_desn nw_tandc">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <!-- Uni List -->
        <div class="cmm_tphd">
          <h1>Whatuni Go User Terms and Conditions:</h1>
          <h6 class="sub-tit">Please read these carefully before completing your application</h6>
        </div>
        <div class="cmm_trms">
          <c:out value="${requestScope.staticContent}" escapeXml="false"/> 
        </div>
        <div class="qua_cnf">
          <div class="btn_chk">
            <span class="chk_btn">
            <input type="checkbox" name="provFormTCId" value="N" id="provFormTCId">
            <span class="checkmark grey"></span>
            </span>
            <p><label for="provFormTCId" class="chkbx_100">I confirm I've read and agreed to the terms and conditions.</label></p>
            <p class="qler1" id="provFormTC_error" style="display:none;"></p>
          </div>
        </div>
        <div class="btn_cnt pt-40 btp">
          <div class="fl">
             <a href="javascript:void(0)" onclick="backToProvPage();" class="fl bck"><i class="fa fa-long-arrow-left"></i> Back</a>
          </div>   
          <div class="fr">                
            <a href="javascript:void(0)" onclick="completeProvForm();" class="fr bton cmplt">Complete Application <i class="fa fa-long-arrow-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<input type="hidden" id="action" />