<%@page import= "WUI.utilities.CommonUtil"%>
<div class="rvbx_shdw"></div>
  <div class="lbxm_wrp">
    <div class="ext-cont svap_cnt coelbox">
      <div class="revcls ext-cls"><a onclick="closeLigBox();"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"></a></div>
      <div class="rev_lst ext">
        <div>
          <h4>Save your application</h4>
          <p>Do you want to save your application and finish it off later?</p>
        </div>
        <div class="btn_cnt w-56p">
          <div class="btnsec_row fl_w100">
            <a href="javascript:void(0)" onclick="savePageCall();" class="fl bton">Yes</a>
            <a href="javascript:void(0)" onclick="deleteUserDataCall();"class="bck">No, please delete</a>
          </div>  
        </div>
      </div>
    </div>
  </div>
