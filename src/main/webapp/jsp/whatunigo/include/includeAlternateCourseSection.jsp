<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<section class="cmm_cnt">
  <div class="cmm_col_12 binf_cnt plnbr_wrp">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <c:if test="${not empty requestScope.courseInfoList}" >
          <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp">
            <jsp:param name="className" value="qual_cnt" />      
          </jsp:include>
        </c:if>
        <form class="fl basic_inf pers_det cont_det qualif plnb_rote">
          <div class="plr_hdt">
            <c:if test="${not empty requestScope.USER_NAME}">
              <h3 class="pln-tit">Hi ${requestScope.USER_NAME}</h3>
            </c:if>
            <p class="cnfrm f-16">Unfortunately the qualifications you've entered do not meet the entry requirements of this course.</p>
          </div>
          <div class="ucas_midcnt">
            <div id="ucas_calc">
              <div id="scoreDiv" class="mt-30 w-647">
                <c:if test="${not empty requestScope.USER_UCAS_TARIFF_POINT}">
                  <div class="ucas_btm ctrl_mt scr_mt0">
                    <div class="ucas_row row_btm30">
                      <div id="innerScoreDiv" class="scr_wrap">
                        <div class="scr_lft">
                            <div class="chartbox fl w100p">
                              <h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Your UCAS score</font></font></h2>
                              <div class="ucspt_val">${requestScope.USER_UCAS_TARIFF_POINT} points</div>  
                            </div>
                        </div>
                        <%--
                        <div class="scr_rht">                        
                          <div class="sub_btn">
                            <a onclick="gotoBackSearch();">BACK<i class="fa fa-long-arrow-right"></i></a>
                          </div>
                        </div>--%>
                      </div>
                    </div>
                  </div>
                 </c:if>
                <%---<div class="ucas_btm ctrl_mt scr_mt0">
                  <div class="ucas_row row_btm30">
                    <div id="innerScoreDiv" class="scr_wrap">
                      <div class="scr_lft">                       
                        <div class="chartbox fl w100p">
                          <h2>Your UCAS Score</h2>
                          <c:if test="${not empty requestScope.USER_UCAS_TARIFF_POINT}">
                            <div id="ukIntl_doughnut" class="chart fl"><span>${requestScope.USER_UCAS_TARIFF_POINT} points</span></div>
                            <script type="text/javascript" id="script_how_you_are_assesed"> 
                              drawRPdoughnutchart('200','${requestScope.USER_UCAS_TARIFF_POINT}','ukIntl_doughnut');
                            </script>
                          </c:if>
                        </div>                        
                      </div>                      
                      <div class="scr_rht">
                        <div class="scrtxt_top"><span class="scr_txt">How you scored</span><span class="scr_val">${requestScope.USER_UCAS_TARIFF_POINT}%</span></div>
                        <div class="scr_pbar"><span style="width:${requestScope.USER_UCAS_TARIFF_POINT}%"></span></div>
                        <div class="scr_desc">
                          <c:if test="${not empty requestScope.SHORTAGE_USER_UCAS_TARIFF_POINT}">
                            <p>You are ${requestScope.SHORTAGE_USER_UCAS_TARIFF_POINT} UCAS points short of the course requirements</p>
                          </c:if>
                        </div>
                        <div class="sub_btn"><a onclick="getTariffPointsTableDataAjax('score');">BACK<i class="fa fa-long-arrow-right"></i></a></div>
                      </div>                                          
                    </div>
                  </div>
                </div>--%>  
              </div>
            </div>
          </div>
          <%--
          <div class="otoff_cnt">  
            <c:if test="${not empty requestScope.ALTERNATE_OFFER_INFO_LIST}">
              <div class="plnrt_cnt">
                <div class="fl ots_cnt">
                  <h3 class="pln-sub-tit">Other offers in your shortlist</h3>
                  <p class="cnfrm f-16">You can still confirm your offer from these universities below that you’ve already shortlisted:</p>
                </div>                 
                <div class="qul_rw">                
                    <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp">
                      <jsp:param name="className" value="qual_cnt" />                
                      <jsp:param name="ALTERNATE_OFFER_LIST_NAME" value="ALTERNATE_OFFER_INFO_LIST" />
                    </jsp:include>                            
                </div>
             </div>
           </c:if>           
            <c:if test="${not empty requestScope.ALTERNATE_UNIVERSITY_INFO_LIST}">
              <div class="plnrt_cnt">
                <div class="fl ots_cnt">
                  <h3 class="pln-sub-tit">Alternative universities you might be interested in</h3>
                  <p class="cnfrm f-16">These universities will also accept you with your grades</p>
                </div>              
                <div class="qul_rw">                
                    <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp">
                      <jsp:param name="className" value="qual_cnt" />                
                      <jsp:param name="ALTERNATE_UNIVERSITY_LIST_NAME" value="ALTERNATE_UNIVERSITY_INFO_LIST" />
                    </jsp:include>                          
                </div>        
              </div>              
           </c:if> 
          </div>--%>          
          <div class="btn_cnt pt-40">
			<div class="fl">
			  <%--<a href="javascript:void(0)" onclick="backToProvPage();" class="fl bck"><i class="fa fa-long-arrow-left"></i> Back</a>--%>
			  <a href="javascript:void(0)" onclick="gotoBackSearch();" class="fl bck"><i class="fa fa-long-arrow-left"></i> Back</a>                
            </div>
		  </div>               
        </form>
      </div>
    </div>
  </div>
</section>
<script>revLightBox</script>