<%@page import= "WUI.utilities.CommonUtil"%>
<%
  String errorCode = request.getParameter("errorMsg");
  String heading = "";
  String errorMsg = "";
  if("1".equals(errorCode)){
    heading = "Application complete";
    errorMsg = "It looks like you've already completed your application for this course.";
  }else if("2".equals(errorCode)){
    heading = "Application already in progress";
    errorMsg = "Unfortunately you can only apply for one course at a time. To proceed, please cancel your open application.";
  }else if("3".equals(errorCode)){
    heading = "Application already completed";
    errorMsg = "Unfortunately you can only complete an application for one course through Whatuni Go.";
  }else{
    heading = "Error!";
    errorMsg = errorCode;
  }
%>
<div class="rvbx_shdw"></div>
  <div class="lbxm_wrp">
    <div class="ext-cont ins_entrq">
      <div class="revcls ext-cls"><a href="javascript:void(0);" onclick="gotoBackSearch();"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"></a></div>
      <div class="rev_lst ext">
        <div>
          <i class="fa fa-exclamation-triangle"></i>
          <h4><%=heading%></h4>
          <p><%=errorMsg%></p>
        </div>
        <div class="btn_cnt insuf">
          <div class="btnsec_row fl_w100">
            <a href="/degrees/get-application-page.html?action=APPLICATION-PAGE" class="fl bton bap_proc">Back to application</a>
          </div>
        </div>
      </div>
    </div>
  </div>