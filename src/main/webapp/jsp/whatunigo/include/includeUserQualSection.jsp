<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%
  String sectNo = "4";
  String qualSub = "";
  String qualId = "";
%>
<div class="qualif qualedit">
  <div class="ucas_midcnt">
    <div id="ucas_calc">
      <div class="ucas_mid">
         <c:forEach var="qualificList" items="${requestScope.userQualList}" >
         
            <!--Qualification Repeat Field start 1-->
            <div class="add_qualif">
              <div class="ucas_refld">
                <div class="ucas_row">
                   <c:set var="qual_check" value="${qualificList.entry_qual_id}"/>
                   <h3 class="un-tit">Level <c:if test="${not empty qualificList.entry_qual_level}"> ${qualificList.entry_qual_level}</c:if><span class="qedt_md"><a onclick="editReviewQual('<%=sectNo%>', 'QUAL_EDIT');">Edit</a></span></h3>
                
                  <div class="qual_edt qual">
                    <h5 class="cmn-tit txt-upr">Qualifications</h5>
                    <div class="qua_fild">
                      <span>
                        ${qualificList.entry_qual_desc}
                      </span>
                    </div>
                  </div>
                  <c:set var="qualid" value="${qualificList.entry_qual_id}"/>
                  <jsp:scriptlet>qualId += pageContext.getAttribute("qualid").toString() + ",";qualSub = "";</jsp:scriptlet>
                  <c:if test="${not empty qualificList.qual_subject_list}">
                           
                  <div class="qual_edt sub">
                    <h5 class="cmn-tit txt-upr">Subjects & Grades</h5>
                     <c:forEach var="subjectList" items="${qualificList.qual_subject_list}" >
                       <c:set var="grade" value="${subjectList.entry_grade}"/>
                       <c:set var="subId" value="${subjectList.entry_subject_id}"/>
                       <jsp:scriptlet>qualSub += pageContext.getAttribute("subId").toString() + "##SUB##" + pageContext.getAttribute("grade").toString() + "##GRADE##";</jsp:scriptlet>
                      <div class="qua_fild">
                        <span>${subjectList.entry_subject}, ${ subjectList.entry_grade}
                        </span>
                      </div>
                      </c:forEach>
                    </div>
                </c:if>
                  </div>
                </div>
              </div>
             <%-- <input class="qualSubj" type="hidden" id="qualSub_<%=qualid%>" value="<%=qualSub%>" />                   --%>
         </c:forEach>
         <%--  <input class="qual" type="hidden" id="qualId" value="<%=qualId%>" />--%>
     </div>
   </div>
 </div>                       
 <input type="hidden" id="action"/>
 </div>
 <div class="btn_cnt">
  <div class="fl">
    <div class="btn_cmps fl">
      <div class="reltv fl">
        <label class="cnfrm fl"> 
        
        <c:if test="${requestScope.SECTION_REVIEWED_FLAG eq 'Y'}">
          <input type="checkbox" checked="true" id="secRev_form_4" data-id="secRevBtn" onclick="valUserRevTCBtn('4');">
        </c:if>
        <c:if test="${requestScope.SECTION_REVIEWED_FLAG ne 'Y'}">
          <input type="checkbox" id="secRev_form_4" data-id="secRevBtn" onclick="valUserRevTCBtn('4');">
        </c:if>      
        <span class="checkmark grn blu"></span> <span class="bn bn-grn grn_txt bn-blue"><span class="macomp">Section Reviewed</span><span class="frmcomp">Section Reviewed</span></span> 
       </label>
      </div>
    </div>
  </div>
</div>
<script>enableRevSecBtn('form_4');</script>