<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants" %>
<%int count = 0, subCount = 0; 
  String pageNo = (String)request.getAttribute("pageNo");
  String qualSub = "";
  String qualId = ""; 
  String actionName = (!GenericValidator.isBlankOrNull((String)request.getAttribute("actionName"))) ? (String)request.getAttribute("actionName") : "EDIT";//PROV_QUAL_EDIT
  String reviewFlag = "N";
  if("EDIT".equalsIgnoreCase(actionName)){
    reviewFlag = "Y";
  }
  String ajaxActionName = "QUAL_SUB_AJAX";
  String formStatusId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("pageStatus"))) ? (String)request.getAttribute("pageStatus") : "";
  String removeBtnFlag = "display:block";
  String selectFlag = "";
  String gcseSelectFlag = "";
  String newGCSESelectFlag = "";
  String oldGCSESelectFlag = "";
  String gcseSelOption = ": 9-1";
  String userTariff = (String)request.getAttribute("userTariff");
  String removeLink =   CommonUtil.getImgPath("/wu-cont/images/cmm_close_icon.svg",0);
  String qualifications = "";
  String addQualStyle = "display:none";
  String addSubStyle = "display:block";
  int addSubLimit = 6;
  String removeQualStyle = "display:none";
  int userQualListSize = (!GenericValidator.isBlankOrNull((String)request.getAttribute("userQualSize"))) ? Integer.parseInt((String)request.getAttribute("userQualSize")) : 0;
  String addQualId = "level_3_add_qualif";
  String callTypeFlag = "";
  String qualTCCheckbox = (String)request.getAttribute("qualTCCheckbox");
  boolean showDefaultGCSEFlag = true;
%>
<section class="cmm_cnt">

      <div class="brd_crumb timer_sec">
        <div class="cmm_col_12">
          <div class="cmm_wrap">
            <c:if test="${not empty requestScope.pageName}">
            <div class="brc_cnt lft fl">
              <div class="brc_row1"><span class="bc_hdtxt"><c:if test="${not empty requestScope.pageName}"> ${requestScope.pageName}</c:if> </span><span class="bc_stpno">Step <c:if test="${not empty requestScope.pageNo}"> ${requestScope.pageNo}</c:if> of 4</span>
              </div>
              <jsp:include page="/jsp/whatunigo/include/includeStepsSection.jsp"/>
            </div>
            </c:if>
            <jsp:include page="/jsp/whatunigo/include/timer.jsp"></jsp:include>  
          </div>
        </div>
      </div>

  <div class="cmm_col_12 binf_cnt">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <div class="fl ots_cnt pb-40">
          <h1 class="pln-sub-tit">Review your qualifications</h1>
          <p class="cnfrm f-16">Check to see if your qualifications are correct</p>
        </div>
        <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/>
        
        <c:if test="${not empty requestScope.userQualList}">
        <form class="fl basic_inf pers_det cont_det qualif revqua_ttip">
				  <div class="ucas_midcnt">
            <div id="ucas_calc">
              <div class="ucas_mid">
                <c:forEach var="userQualList" items="${requestScope.userQualList}" varStatus="i" >
                  <c:set var="indexIValue" value="${i.index}"/>
                  <c:set var="qualDes" value="${userQualList.entry_qual_desc}"/>
                  <%String qualDes = pageContext.getAttribute("qualDes").toString();%>
                  <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue")));%>
                  <c:if test="${userQualList.entry_qual_desc eq 'GCSE'}">
                    <%count = 17;addSubLimit=20;showDefaultGCSEFlag = false;%>
                  </c:if>
                  <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                    <%addQualId = "level_3_add_qualif";qualifications += qualDes + ",";%>
                  </c:if>
                  <c:if test="${userQualList.entry_qual_desc eq 'GCSE'}">
                    <%addQualId = "level_2_add_qualif";%>
                  </c:if>
                  <div class="add_qualif" id="add_qualif_<%=count%>" data-id="<%=addQualId%>">
							  <div class="ucas_refld">                  
								  <h3 class="un-tit">Level <c:if test="${not empty userQualList.entry_qual_level}">
                    ${userQualList.entry_qual_level} <c:if test="${userQualList.entry_qual_level eq '2'}">: GCSE </c:if></c:if> 
                    
                       <span class="hlp tool_tip" id="toolTip_<%=count%>"> <i class="fa fa-question-circle"></i>
                          <span class="cmp hlp-tt">
                              <div class="hdf5"></div>
                              <c:if test="${userQualList.entry_qual_level eq '3'}">
                                <div>Level 3 Qualifications are those studied after school and include: A-Levels, AS Levels, SQA Highers, SQA Advanced Highers and BTECs. Please enter all of the Level 3 qualifications you have studied for and the results you achieved.</div>
                              </c:if>
                              <c:if test="${userQualList.entry_qual_level ne '3'}">
                                <div> GCSEs are the qualifications you studied for and achieved at secondary school. Please enter all of the GCSE subjects you took and the results you achieved.</div>
                              </c:if>
                              <div class="line"></div>
                          </span>
                      </span>
                  
                  </h3>
                  
                  <div class="l3q_rw" id="qual_level_<%=count%>">		
									  <div class="adqual_rw">
											<div class="ucas_row">
												<h5 class="cmn-tit txt-upr"><c:if test="${userQualList.entry_qual_level eq '2'}">GRADE TYPE</c:if><c:if test="${userQualList.entry_qual_level ne '2'}"> Qualifications</c:if> </h5>
												<fieldset class="ucas_fldrw">
												<div class="remq_cnt">
													<div class="ucas_selcnt">
														<fieldset class="ucas_sbox">
														  <c:set var="qualDesc" value="${userQualList.entry_qual_desc}"/>
                              <c:if test="${userQualList.entry_qual_desc eq 'GCSE'}">
                                <c:if test="${userQualList.gcse_grade_flag eq 'OLD'}">
                                   <jsp:scriptlet>gcseSelOption = ": A*-G";</jsp:scriptlet>
                                </c:if>
                                <span class="sbox_txt" id="qualspan_<%=count%>">${userQualList.entry_qual_desc} <%=gcseSelOption%></span>
                              </c:if>
                              <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                                <span class="sbox_txt" id="qualspan_<%=count%>">${userQualList.entry_qual_desc}</span>
															</c:if>
                              <i class="fa fa-angle-down fa-lg"></i>
														</fieldset>
														<c:if test="${userQualList.entry_qual_level ne '2'}">
														<select name="qualification<%=count%>" onchange="appendQualDiv(this, '', '<%=count%>');" id="qualsel_<%=count%>" class="ucas_slist">
															 <option value="0">Please Select</option>
															 <c:forEach var="qualList" items="${requestScope.qualificationList}">
                                 <c:set var="selectQualId" value="${userQualList.entry_qual_id}"/>
                                 <jsp:scriptlet>selectFlag = "";</jsp:scriptlet>
                                 <c:if test="${qualList.entry_qual_id eq selectQualId}">
                                   <jsp:scriptlet>selectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                 </c:if>
                                 <c:if test="${qualList.entry_qualification ne 'GCSE'}">
                                    <c:if test="${empty qualList.entry_qual_id}">
                                       <optgroup label="${qualList.entry_qualification}"></optgroup>
                                     </c:if>
                                     <c:if test="${not empty qualList.entry_qual_id}">
                                        <option value="${qualList.entry_qual_id}" <%=selectFlag%>>${qualList.entry_qualification} </option>
                                     </c:if>                                   
                                 </c:if>
                               </c:forEach>
														  </select>
                              <div id="qual_sub_grd_hidId">
                                <c:forEach var="grdQualifications" items="${requestScope.qualificationList}" >
                                   <c:if test="${grdQualifications.entry_qualification ne 'GCSE'}">                            
                                   <c:if test="${not empty grdQualifications.entry_qual_id}">
                                     <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                     <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}" name="" value="${grdQualifications.entry_grade}" />
                                   </c:if>
                                   </c:if>                                 
                                 </c:forEach>
                               </div>
                              </c:if>
                              <c:if test="${userQualList.entry_qual_level eq '2'}">
                               <select name="qualification<%=count%>" onchange="appendQualDiv(this, '', '<%=count%>');" id="qualsel_<%=count%>" class="ucas_slist">
															 <option value="0">Please Select</option>
															 <c:forEach var="qualList" items="${requestScope.qualificationList}">
                                 <c:if test="${qualList.entry_qualification eq 'GCSE'}">
                                   <c:set var="selectQualId2" value="${userQualList.gcse_grade_flag}"/>
                                   <jsp:scriptlet>gcseSelectFlag = "";</jsp:scriptlet>
                                   <c:if test="${userQualList.gcse_grade_flag eq 'NEW'}">
                                     <jsp:scriptlet>gcseSelectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                   </c:if>   
                                   
                                   <option value="${qualList.entry_qual_id}_gcse_new_grade" <%=gcseSelectFlag%>>${qualList.entry_qualification}: 9-1 </option>
                                   <jsp:scriptlet>gcseSelectFlag = "";</jsp:scriptlet>
                                   <c:if test="${userQualList.gcse_grade_flag eq 'OLD'}">
                                     <jsp:scriptlet>gcseSelectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                   </c:if>
                                   <option value="${qualList.entry_qual_id}_gcse_old_grade" <%=gcseSelectFlag%>>${qualList.entry_qualification}: A*-G</option>
                                 </c:if>
                               </c:forEach>
														  </select>
														  <c:forEach var="grdQualifications" items="${requestScope.qualificationList}">
                                 <c:if test="${grdQualifications.entry_qualification eq 'GCSE'}">                                                                 
                                   <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}_gcse" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                   <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_new_grade" name="" value="${grdQualifications.entry_grade}" />
                                   <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_old_grade" name="" value="${grdQualifications.entry_old_grade}" />
                                 </c:if>
                               </c:forEach>
                              </c:if>             
                              
                               
													</div>
                          </div>
                              <%if(count != 0){removeQualStyle = "display:block";}%>
                              <div class="add_fld" style="<%=removeQualStyle%>">
                                <a id="remQual<%=count%>" onclick="removeQualEntryReq('add_qualif_<%=count%>', '<%=count%>');" class="btn_act1 bck f-14 pt-5">
                                  <span class="hd-mob">Remove Qualification</span>
                                </a>
                              </div>													
												</fieldset>
                        <div class="err" id="qualSubId_error" style="display:none;"></div>
											</div>
                
                 <div class="subgrd_fld" id="subGrdDiv<%=count%>">
                      <div class="ucas_row grd_row" id="grdrow_<%=count%>">
                      <fieldset class="ucas_fldrw quagrd_tit">
														<div class="ucas_tbox tx-bx">
                                <h5 class="cmn-tit txt-upr qual_sub">Subject</h5>
														</div>
														<div class="ucas_selcnt rsize_wdth">
                              <h5 class="cmn-tit txt-upr qual_grd">Grade</h5>
														</div>
                          </fieldset>
                          
                      <c:forEach var="subjectList" items="${userQualList.qual_subject_list}" varStatus="j" >
                        <c:set var="indexJValue" value="${j.index}"/>
                          <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue")));%>
                          <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
													<fieldset class="ucas_fldrw">
														<div class="ucas_tbox w-390 tx-bx">
															<input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="sub_${subjectList.entry_subject_id}" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="${subjectList.entry_subject}" class="frm-ele" autocomplete="off">															
                              
                              <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                              <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                              </div>
                                  
														</div>
														<div class="ucas_selcnt rsize_wdth">
														  <div class="ucas_sbox mb-ht w-84" id="1grades1">
                                <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">${subjectList.entry_grade}</span><i class="fa fa-angle-down fa-lg"></i>
                              </div>
															<select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" id="qualGrd_<%=count%>_<%=Integer.parseInt(pageContext.getAttribute("indexJValue").toString())%>"></select>
                              <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=count%>_<%=subCount%>','${subjectList.entry_grade}'); </script>
                              <c:if test="${userQualList.entry_qual_level ne '2'}">
                                <input class="subjectArr" data-id="level_3" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${userQualList.entry_qual_id}" value="${subjectList.entry_subject_id}" />
                              </c:if>
                              <c:if test="${userQualList.entry_qual_level eq '2'}">
                                <input class="subjectArr" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${userQualList.entry_qual_id}" value="${subjectList.entry_subject_id}" />
                              </c:if>                            
                              <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_${userQualList.entry_qual_id}" value="${subjectList.entry_grade}" />
                              <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${userQualList.entry_qual_id}" value="${userQualList.entry_qual_id}" />
                              <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${userQualList.entry_qual_id}" value="<%=count+1%>" />
														</div>
                            <%if(subCount == 0){
                              removeBtnFlag = "display:none";
                            }else{
                              removeBtnFlag = "display:block";
                            }
                            if(subCount == 1){
                            %>
                            <script>jq("#remSubRow<%=count%>0").show();</script>
                            <%}%>
														<div class="add_fld"> 
                              <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5">
                                <span class="hd-mob">Remove</span>
                                <span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"/></span>
                              </a>
                            </div>
													</fieldset>
												</div>
                        </c:forEach>
                        <%if((subCount + 1) < addSubLimit){
                          addSubStyle = "display:block";
                        }else{
                          addSubStyle = "display:none";
                        }%>
                        <div class="ad-subjt" id="addSubject_<%=count%>" style="<%=addSubStyle%>">
                          <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('${userQualList.entry_qual_id}','countAddSubj_<%=count%>', '<%=count%>');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                        </div>
                      </div>
                    </div>
                       <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                       <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                       
									</div>
							
								</div>
								   <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                    <%if(count == (userQualListSize-1)){addQualStyle = "display:none";}%>                    
										<div class="add_qualtxt" id="addQualBtn_<%=count%>" style="<%=addQualStyle%>">
                      <a href="javascript:void(0);" onclick="addQualEntryReq('<%=count+1%>')" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> ADD A QUALIFICATION</a></div>	
                   </c:if>
                   </div>
									</div>
                   </c:forEach>
							
              <!--Qualification Repeat Field start 2-->
              <%if(showDefaultGCSEFlag){%> 
                <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" >
                  <c:set var="indexIValue" value="${i.index}"/>
                  <c:if test="${qualList.entry_qual_id eq '17'}">
                  <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue")));%>
                  <div class="add_qualif" id="add_qualif_<%=count%>" data-id="level_2_add_qualif">
                    <div class="ucas_refld">
                      <h3 class="un-tit">
                        Level 2: GCSE
                        <%--
                        <a href="javascript:void(0)" class="blu-lnk rltv bsc">
                          <span>Why do we need this?</span>
                          <span id="postcodeText" class="cmp adjst">
                            <div class="hdf5"></div>
                            <div>Some courses require you to have studied and achieved a pass grade for specific a GCSE subject, or achieved a pass on a minimum number of GCSEs. It is therefore important to tell us of all your GCSE qualifications so we can provide you with the best matching courses.</div>
                            <div><strong>Please Note:</strong> GCSEs are the only Level 2 qualifications used by Whatuni Go to match you with courses. If you have different Level 2 Qualifications, you can skip this stage and submit your search.</div>
                            <div class="line"></div>
                          </span>
                        </a>
                        --%>
                        <span class="hlp tool_tip"> <i class="fa fa-question-circle"></i>
                          <span class="cmp hlp-tt">
                            <div class="hdf5"></div>
                            <div> GCSEs are the qualifications you studied for and achieved at secondary school. Please enter all of the GCSE subjects you took and the results you achieved.</div>
                            <div class="line"></div>
                          </span>
                        </span>
                      </h3>
                      <%--<p class="ql_dec">Please enter your GCSE grades below. You'll need to select whether your grade scores are letters or numerical. Please refer to your certificate for the correct grade.</p>--%>
                      <!-- L3 Qualification Row  -->
                      <div class="l3q_rw" id="qual_level_<%=count%>">
                        <!-- Add Qualification & Subject 1-->		
                        <!-- Add Qualification & Subject 2-->		
                        <div class="adqual_rw">                          
                            <div class="ucas_row">
                              <h5 class="cmn-tit txt-upr">GRADE TYPE </h5>
                              <fieldset class="ucas_fldrw">
                                <div class="remq_cnt">
                                  <div class="ucas_selcnt">
                                    <fieldset class="ucas_sbox">
                                      <c:set var="selectQualLeve2Name" value="${qualList.entry_qualification}"/>
                                      <%String selectQualLeve2Name = pageContext.getAttribute("selectQualLeve2Name").toString(); %>
                                      <span class="sbox_txt" id="qualspan_<%=count%>"><%=selectQualLeve2Name%>: A*-G</span>
                                      <i class="fa fa-angle-down fa-lg"></i>
                                    </fieldset>
                                     <select name="qualification1" onchange="appendQualDiv(this, '', '<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>', 'new_entry_page');" id="qualsel_<%=count%>" class="ucas_slist">
                                     <option value="0">Please Select</option>
                                       <c:forEach var="qualListLevel2" items="${requestScope.qualificationList}" >
                                         <c:if test="${qualListLevel2.entry_qualification eq 'GCSE'}">
                                             <jsp:scriptlet>gcseSelectFlag = "selected=\"selected\"";</jsp:scriptlet>                              
                                           <option value="${qualListLevel2.entry_qual_id}_gcse_new_grade">${qualListLevel2.entry_qualification}: 9-1 </option>                               
                                           <option value="${qualListLevel2.entry_qual_id}_gcse_old_grade" <%=gcseSelectFlag%>>${qualListLevel2.entry_qualification}: A*-G</option>
                                          </c:if>
                                        </c:forEach>
                                      </select>
                                      <c:forEach var="grdQualifications" items="${requestScope.qualificationList}" >
                                         <c:if test="${grdQualifications.entry_qualification eq 'GCSE'}">                                                                
                                           <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}_gcse" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                           <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_new_grade" name="" value="${grdQualifications.entry_grade}" />
                                           <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_old_grade" name="" value="${grdQualifications.entry_old_grade}" />
                                         </c:if>
                                      </c:forEach>
                                   </div>
                                 </div>
                                <%--
                                <div class="add_fld">
                                  <a id="1rSubRow3" onclick="javascript:removeSubRow(1,3);" class="btn_act1 bck f-14 pt-5">
                                    <span class="hd-mob">Remove Qualification</span>
                                  </a>
                                </div>
                                --%>
                              </fieldset>
                              <div class="err" id="qualSubId_error" style="display:none;"></div>
                            </div>                          
                          <div class="subgrd_fld" id="subGrdDiv<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>">
                            <div class="ucas_row grd_row" id="grdrow_<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>">                            
                              <fieldset class="ucas_fldrw quagrd_tit">
                                <div class="ucas_tbox tx-bx">
                                  <h5 class="cmn-tit txt-upr qual_sub">Subject</h5>
                                </div>
                                <div class="ucas_selcnt rsize_wdth">
                                  <h5 class="cmn-tit txt-upr qual_grd">Grade</h5>
                                </div>
                              </fieldset>
                              <c:set var="selectQualLevel2SubLen" value="${qualList.entry_subject}"/>
                              <%String selectQualLevel2SubLen = pageContext.getAttribute("selectQualLevel2SubLen").toString(); %>
                              <c:forEach var="subjectList" items="${qualificationList}" begin="0" varStatus="j" end="<%=Integer.parseInt(selectQualLevel2SubLen)%>">    
                               <c:set var="indexJValue" value="${j.index }"/>                       
                              <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue").toString()));%>
                                <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
                                  <fieldset class="ucas_fldrw">
                                    <div class="ucas_tbox w-390 tx-bx">
                                      <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="subIp_<%=subCount%>" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="" class="frm-ele" autocomplete="off">															
                                      <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                                      <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                                      </div>
                                    </div>
                                    <div class="ucas_selcnt rsize_wdth">
                                      <div class="ucas_sbox mb-ht w-84" id="1grades1">
                                        <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">A*</span><i class="fa fa-angle-down fa-lg"></i>
                                      </div>
                                      <select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" id="qualGrd_<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>_<%=subCount%>"></select>                                      
                                      <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>_<%=Integer.parseInt(pageContext.getAttribute("indexJValue").toString())%>','${subjectList.entry_grade}'); </script>
                                      
                                      <input class="subjectArr" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${qualList.entry_qual_id}" value="" />                                
                                      <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_${qualList.entry_qual_id}" value="A*" />
                                      <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${qualList.entry_qual_id}" value="${qualList.entry_qual_id}" />
                                      <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${qualList.entry_qual_id}" value="<%=count+1%>" />
                                    </div>
                                    <%if(subCount == 0){
                                      removeBtnFlag = "display:none";
                                    }else{
                                      removeBtnFlag = "display:block";
                                    }
                                    if(subCount == 1){%>                                  
                                      <script>jq("#remSubRow<%=count%>0").show();</script>
                                    <%}%> 
                                    <div class="add_fld"> <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"></span></a> </div>
                                  </fieldset>
                                </div>                                
                              </c:forEach>                       
                              <div class="ad-subjt" id="addSubject_<%=count%>" style="display:block">
                                <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('${qualList.entry_qual_id}','countAddSubj_<%=count%>', '<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                              </div>                            
                          </div>
                        </div>
                        <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <!-- Add Qualification & Subject 2 -->
                      </div>
                    </div>
                    </div>
                  </div>
                  
                  <script>jq( document ).ready(function() {appendQualDiv(jq('#qualsel_'+<%=count%>).get(0), '', '<%=count%>', 'new_entry_page');});</script>
                  </c:if>
                </c:forEach>
              <%}%>  
              <!--Qualification Repeat Field end 2 -->


									
								</div>
								
							<div id="scoreDiv" class="fl cmqua mt-40 w-647">
							<c:if test="${not empty requestScope.userTariff}">
              <div class="ucas_btm ctrl_mt scr_mt0">
										<div class="ucas_row row_btm30">
											<div id="innerScoreDiv" class="scr_wrap">
												<div class="scr_lft">
													
                            <div class="chartbox fl w100p">
                              <h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Your UCAS score</font></font></h2>
                              <div class="ucspt_val" id="ucasPt">${requestScope.userTariff} points</div>  
                            </div>
													
												</div>
											</div>
										</div>
									</div>
                  </c:if>

</div>
							</div>
						
							<div class="pt-25 qua_cnf">
								<h3 class="un-tit p-0 f-20 pt-0">Confirmation</h3>
								<p class="cnfrm f-16">Please review and make sure you have entered your score correctly. If your score is different to what you have entered then <c:if test="${not empty requestScope.COLLEGE_DISPLAY_NAME}">${requestScope.COLLEGE_DISPLAY_NAME}</c:if> is within their rights not to make you a formal offer. <a href="javascript:void(0);" onclick="showTermsConditionPopup();">See full terms &amp; conditions here</a></p>
								<div class="btn_chk">
									<span class="chk_btn">
                   <%if("EDIT".equalsIgnoreCase(actionName)){
                   if("Y".equalsIgnoreCase(qualTCCheckbox)){%>
                      <input onclick="enableMarkComplete('entryReqSecId', '', '<%=pageNo%>');" type="checkbox" name="qualTCId" value="Y" id="qualTCId" checked>
                    <%}else{%>
                      <input onclick="enableMarkComplete('entryReqSecId', '', '<%=pageNo%>');" type="checkbox" name="qualTCId" value="N" id="qualTCId">
                    <%}
                   }else{%>
                  <input onclick="enableMarkComplete('entryReqSecId', '', '<%=pageNo%>');" type="checkbox" name="qualTCId" value="N" id="qualTCId">
                  <%}%>
									<span class="checkmark grey"></span>
									</span>
									<p>
                    <label for="qualTCId" class="chkbx_100" id="labelUcasPt">I confirm that all the information I have given is true and accurate, including details of my qualification information and I have achieved <c:if test="${not empty requestScope.userTariff}">${requestScope.userTariff} UCAS points.</c:if></label>
                  </p>                                                 
								</div>
                <p class="qler1" id="qualTCId_error" style="display:none;"></p>
                <p class="qler1" id="subErrMsg" style="display:none;"></p>
		<input type="hidden" id="oldGrade" value="<%=userTariff%>" />
                  <input type="hidden" id="newGrade" value="<%=userTariff%>" /> 
              </div>
							<div class="btn_cnt pt-40">
								<div class="fr">
                <%if("EDIT".equalsIgnoreCase(actionName)){%>
								<div class="btn_cmps fl">
									<div class="reltv fl">
										<label class="cnfrm fl">            									  
									  <%if("C".equalsIgnoreCase(formStatusId)){callTypeFlag="onload";%>
                      <input id="markAsComFlagId" onclick="passJSONObj('<%=actionName%>', '4', 'N', '', '', 'EDIT_QUAL_GA', 'Y');" type="checkbox" disabled="disabled" checked>
                    <%}else{%>
                      <input id="markAsComFlagId" onclick="passJSONObj('<%=actionName%>', '4', 'N', '', '', 'EDIT_QUAL_GA', 'Y');" type="checkbox" >
                    <%}%>
                    <span class="checkmark grn"></span> <span class="bn bn-grn grn_txt"><span class="macomp">Mark As Complete</span><span class="frmcomp">Completed</span></span> 
									 </label>
								  </div>
								</div>
                <%}%>
								<a href="javascript:void(0);" onclick="passJSONObj('<%=actionName%>', '5', '<%=reviewFlag%>', '', '','EDIT_QUAL_GA' );" class="fr bton">SAVE AND CONTINUE <i class="fa fa-long-arrow-right"></i></a>
                
							</div>
              <%if("PROV_QUAL_UPDATE".equalsIgnoreCase(actionName)){%>
							  
                  <div class="fl">
                    <a href="javascript:void(0)" onclick="backToProvPage();" class="fl bck"><i class="fa fa-long-arrow-left"></i> Back</a>
                  </div>
              <%}else{%>
              <jsp:include page="/jsp/whatunigo/include/includeBackLink.jsp"/>
						  <%}%>
            </div>
						
				</div></form>
        <input type="hidden" id="action" />
        <input type="hidden" id="removeLinkImg" value="<%=removeLink%>" />
        <input type="hidden" id="pageNo" value="<%=pageNo%>" />
        <input type="hidden" id="oldQualDesc" value="<%=qualifications%>" />
        <input type="hidden" id="newQualDesc" value="<%=qualifications%>" />
       </c:if>
          
     <script>
     enableMarkComplete('entryReqSecId','<%=callTypeFlag%>');
     onloadShowingAddQualBtn();
     </script>
  </div>
  </div>
  </div></section>
