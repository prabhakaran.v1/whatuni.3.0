<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import= "com.wuni.util.seo.SeoUrls, WUI.utilities.CommonUtil,  org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants" autoFlush="true" %>
<% 
   SeoUrls seoUrl = new SeoUrls();
   String className = (String)request.getAttribute("className");
   className =  !GenericValidator.isBlankOrNull(className) ? className : "";
   String listName = "courseInfoList";
   String collegeId = "", collegeName = "", courseTitle = "", courseId = "", courseExistFlag = "";
   boolean alternateOfferFlag = false;
   boolean alternateUniversityFlag = false;
   boolean congratsPageFlag = false;
   if(!GenericValidator.isBlankOrNull(request.getParameter("ALTERNATE_OFFER_LIST_NAME"))){
     alternateOfferFlag = true;
     listName = request.getParameter("ALTERNATE_OFFER_LIST_NAME");
   }
   if(!GenericValidator.isBlankOrNull(request.getParameter("ALTERNATE_UNIVERSITY_LIST_NAME"))){
     alternateUniversityFlag = true;
     listName = request.getParameter("ALTERNATE_UNIVERSITY_LIST_NAME");
   }
   if(!GenericValidator.isBlankOrNull(request.getParameter("CONGRATS_PAGE_COURSE_INFO_LIST_NAME"))){
     congratsPageFlag = true;
     listName = request.getParameter("CONGRATS_PAGE_COURSE_INFO_LIST_NAME");
   }
   pageContext.setAttribute("listName", listName);

%>
<c:if test="${not empty requestScope.courseInfoList}">
  <c:forEach items="${requestScope.courseInfoList}" var="courseInfoList">
    <div class="cmm_unilst provlgo_hid <%=className%>">
      <div class="fl uni-logo col_lft">
        <%if(alternateOfferFlag || alternateUniversityFlag){%>        
            <c:set value="${courseInfoList.college_name}" var="collegeNameItr"/>
            <c:set value="${courseInfoList.college_id}" var="collegeIdItr"/>
            <c:set value="${courseInfoList.course_Title}" var="courseTitleItr"/>
            <c:set value="${courseInfoList.course_id}" var="courseIdItr"/>
        <%
          collegeId = pageContext.getAttribute("collegeIdItr").toString();
          collegeName = pageContext.getAttribute("collegeNameItr").toString();
          courseTitle = pageContext.getAttribute("courseTitleItr").toString();
          courseId = pageContext.getAttribute("courseIdItr").toString();          
        %>
        <c:if test="${not empty courseInfoList.college_logo}">
          <a href='<%=seoUrl.construnctUniHomeURL(collegeId,collegeName)%>'><img src="<%=CommonUtil.getImgPath("",0)%>${courseInfoList.college_logo}" alt="${courseInfoList.college_display_name}"></a>
        </c:if>
        <c:if test="${empty courseInfoList.college_logo}">
          <a href='javascript:void(0);'><img src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" alt="${courseInfoList.college_display_name}"></a>
        </c:if>
      <%}else{%>
        <c:if test="${not empty courseInfoList.college_logo}">
          <a href="javascript:void(0);"><img src="<%=CommonUtil.getImgPath("",0)%>${courseInfoList.college_logo}" alt="${courseInfoList.college_display_name}"></a>
        </c:if>
        <c:if test="${empty courseInfoList.college_logo}">
        <a href='javascript:void(0);'><img src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" alt="${courseInfoList.college_display_name}"></a>
        </c:if>
        <%} %>
        <div class="fl col_rgt">
          <%if(alternateOfferFlag || alternateUniversityFlag){%>
            <h3 class="un-tit"><a href='<%=seoUrl.construnctUniHomeURL(collegeId,collegeName)%>'>${courseInfoList.college_display_name}</a></h3>
          <%}else{%>
            <h3 class="un-tit"><a href="javascript:void(0);">${courseInfoList.college_display_name}</a></h3>
          <%}%>
          <%if(alternateOfferFlag || alternateUniversityFlag){%>
            <h6 class="sub-tit"><a href='<%=seoUrl.courseDetailsPageURL(courseTitle,courseId,collegeId)%>'>${courseInfoList.course_Title}</a></h6>
          <%}else{%>
            <h6 class="sub-tit"><a href="javascript:void(0)">${courseInfoList.course_Title}</a></h6>
          <%}%>
          <div class="fx">
            <div class="fx-bx">
              <h5>Study Mode</h5>
              <h6>${courseInfoList.study_mode}</h6>
            </div>
            <div class="fx-bx">
              <h5>Study Duration</h5>
              <h6>${courseInfoList.study_duration}</h6>
            </div>
            <div class="fx-bx">
              <h5>UCAS Tariff Points 
                <span class="hlp tool_tip"><i class="fa fa-question-circle"></i>
                  <span class="cmp hlp-tt">
                    <div class="hdf5"></div>
                    <div>UCAS Tariff points are a way of measuring the value of your post-16 qualifications (not your GCSEs). Your total depends on which qualifications you have studied and the grades you have achieved. Please Note: Some qualifications aren't on the UCAS Tariff system, but this doesn't mean they aren't accepted by universities. So please list all qualifications you have here.</div> 
                    <div class="line"></div>
                  </span>
                </span>
              </h5>
              <h6>${courseInfoList.tariff_points}</h6>
              <%if(alternateOfferFlag){%>
                <%-- button --%>
                <div class="sub_btn blue"><a onclick="navigation('1', '1', 'I','','','','${courseInfoList.course_id}','${courseInfoList.opportunity_id}');">CONFIRM OFFER <i class="fa fa-long-arrow-right"></i></a></div>
                <%-- button --%>
              <%}else if(alternateUniversityFlag){%>
                <%-- button --%>
                <div class="sub_btn"><a href='<%=seoUrl.courseDetailsPageURL(courseTitle,courseId,collegeId)%>' onclick="getTariffPointsTableDataAjax('score');">VIEW COURSE <i class="fa fa-long-arrow-right"></i></a></div>
                <%-- button --%>
              <%}%>
            </div>
          </div>
          <input type="hidden" id="collegeDisplayName" name="collegeDisplayName" value="${requestScope.COLLEGE_DISPLAY_NAME}"/>
          <input type="hidden" id="coureReqTariffPts" name="coureReqTariffPts" value="${requestScope.COURSE_REQ_TARIFF_POINT}"/>
        </div>
      </div> 
  </c:forEach>
</c:if>