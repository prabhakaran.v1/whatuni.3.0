<%@ taglib uri="/WEB-INF/tlds/html.tld" prefix="html"%>
<%@ page contentType="text/html;charset=windows-1252"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants" %>
<%
  String pageName = request.getParameter("PAGE_NAME");
  String fbErrorId = "fblblogin_error";
  if("sign up".equalsIgnoreCase(pageName)){
    fbErrorId = "fblbregistration_error";
  }
  String refererURL = (request.getAttribute("REFERER_URL") != null) ? (String)request.getAttribute("REFERER_URL") : "";
%>
<%-- Social login --%>
<div class="cnf_off">
  <%if(!"sign up".equalsIgnoreCase(pageName)){%>
  <h5 class="cmn-tit">Or <%=pageName%> with...</h5>
  <div class="sgn">
    <div class="so-btn">
      <a class="btn1 fbbtn" onclick="loginInfoFacebook('fblogin');" title="Login with Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK</a>
    <%--  <a class="btn1 gbtn" title="Login with Google"><img class="g_icon" src="images/google_icon.svg" /> GOOGLE</a> --%>
    </div>    
    <p class="qler1 valid_err" id="<%=fbErrorId%>" style="display: none;"></p>    
  </div>
  <%}%>
</div>
<input type="hidden" id="yoeId" value="<%=GlobalConstants.YEAR_OF_ENTRY_1%>"/>
<input type="hidden" value="N" id="marketingEmailRegFlag"/>
<input type="hidden" value="N" id="solusEmailRegFlag"/>
<input type="hidden" value="N" id="surveyEmailRegFlag"/>
<input type="hidden" id="referrerURL_GA" value="<%=refererURL%>"/>
<input type="hidden" id="regLoggingType" value="whatuni-go"/>
<input type="hidden" id="submitType" value="">
<%-- Social login --%>
