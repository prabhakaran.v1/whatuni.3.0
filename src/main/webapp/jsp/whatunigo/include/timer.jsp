<script type="text/javascript">
  var $cmt = jQuery.noConflict();
  $cmt(window).scroll(function(){
    if(document.getElementById("timerStky")){
      var tmrStky = document.getElementById("timerStky");
      var offsetTop = tmrStky.offsetTop;
      var cntPos = offsetTop+102;
      if ($cmt(window).scrollTop() >= cntPos) {
        $cmt('.brc_cnt.rgt').addClass('time_stky');
      } else {
        $cmt('.brc_cnt.rgt').removeClass('time_stky');
      }
    }
  });
  
</script>
<%-- Timer Section --%>

<div class="brc_cnt fr rgt" id="timerStky">
  <div class="brc_row1 fr">
    <span class="bc_hdtxt fr">Time remaining</span><span class="bc_stpno fr" id="timer"></span>
  </div>
</div>