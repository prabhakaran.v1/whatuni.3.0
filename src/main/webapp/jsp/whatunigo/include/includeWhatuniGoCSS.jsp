<%@page import="WUI.utilities.CommonUtil" %>
<meta http-equiv="content-language" content=" en-gb "/>

<%
  String wuniGoHeaderStyleCss = CommonUtil.getResourceMessage("wuni.go.style.header.css", null); //cmm_header_styles_586
  String wuniGoStyleCss = CommonUtil.getResourceMessage("wuni.cmm.main.style.css", null); //cmm_main_styles_586
  String wuniGOMobileStyleCss = CommonUtil.getResourceMessage("wuni.go.mobile.css", null);    //cmm_mobile_styles_586
%>

<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuniGoHeaderStyleCss%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuniGoStyleCss%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=wuniGOMobileStyleCss%>" media="screen" />
<jsp:include page="/jsp/common/includeIconImg.jsp"/>
<!--[if IE 7]>    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/whatuni-screens_ie7hacks-wu552.css" media="screen" />   <![endif]-->
<jsp:include page="/jsp/common/abTesting.jsp"/>

<%if(request.getAttribute("showBanner")==null){%>
  <jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
<%}%>

<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
  <jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>
<%-- Facebook pixel tracking script included in head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/facebookPixelTracking.jsp"/>