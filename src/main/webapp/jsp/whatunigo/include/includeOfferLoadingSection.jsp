<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import= "WUI.utilities.CommonUtil"%>
<%String C_END_TIME_T1 = (String)request.getAttribute("C_END_TIME_T1");%>
<section class="cmm_cnt nw-hgt">
    <div class="cmm_col_12 chse_cnt cngs_cnt nw_desn chkur_app">
      <div class="cmm_wrap">
        <div class="cmm_row">
          <div class="cmm_tphd">
             <h1>Checking your application</h1>
             <div class="scr_pbar" id="myProgress">
               <span class="cya_percen" id="prLabel">0%</span>
               <span id="myBar" class="scr-fill" style="width:0%"></span>
             </div>
             <c:if test="${not empty requestScope.APPLICANTION_COUNT}">
                 <c:if test="${requestScope.APPLICANTION_COUNT ge '10'}">
                   <div class="cmm_apld">
                     <span class="fl usr_icn"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/user_icon.svg"></span>
                     <span class="fl mlft">${requestScope.APPLICANTION_COUNT} other people have applied to this uni in the past 24 hours</span>                   
                   </div>
                 </c:if>
            </c:if>
          </div>        
        </div>
      </div>
    </div> 
</section>
<input type="hidden" value="${requestScope.APPLICANTION_COUNT}" id="applicationCountId"/>
<input type="hidden" id="offerTimer1StatusId"/>
<input type="hidden" id="action"/>
<input type="hidden" id="endTimeT1" name="endTimeT1" value="<%=C_END_TIME_T1%>"/> 
<script>
jq( document ).ready(function() {
  offerLoadingProgressBar();
  setTimerCookie('offer_timer','','0')
  startTimer1('TIMER_2');
});
</script>