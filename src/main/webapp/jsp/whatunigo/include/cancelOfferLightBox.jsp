<%@page import= "WUI.utilities.CommonUtil, mobile.util.MobileUtils"%>

<%String hotline = request.getParameter("hotLine");
String collegeDisplayName = request.getParameter("collegeDisplayName");
boolean mobileFlag = new MobileUtils().userAgentCheck(request);
%>
<div class="rev_lbox" id="cancalOfferlightBox" style="display:block;opacity:1;z-index:11111;visibility:visible">
 <div class="rvbx_shdw"></div>  
  <div class="lbxm_wrp"> 
    <div class="ext-cont svap_cnt">
     <div class="revcls ext-cls"><a onclick="closeLigBox();"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close" /></a></div>
     <div class="rev_lst ext">
       <div>
         <h4>Cancelling with <%=collegeDisplayName%></h4>
         <p>You will need to cancel your offer with the university directly. Whatuni will not contact the university on your behalf. Please contact the university using the number below.</p>
       </div>
       <div class="btn_cnt w-56p">
        <div class="btnsec_row fl_w100">
         <a id="contact_0" class="fl bton tc con_btn"  onclick="changeContactButtonInCancelOffer('<%=hotline%>','contact_0');wugoStatsLogging('CANCAL_OFFER_LIGHTBOX');"><i class="fa fa-phone" aria-hidden="true"></i>Call now</a>
       </div>
       </div>
     </div>
   </div>
  </div> 
 </div>
 <input type="hidden" id="check_mobile_hidden" value="<%=mobileFlag%>"/> 

