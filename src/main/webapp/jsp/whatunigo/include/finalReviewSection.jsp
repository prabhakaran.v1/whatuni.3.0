<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<section class="cmm_cnt">
    <div class="brd_crumb timer_sec">
      <div class="cmm_col_12">
        <div class="cmm_wrap">
          <jsp:include page="/jsp/whatunigo/include/timer.jsp"/>
        </div>
      </div>
    </div>
  <div class="cmm_col_12 chse_cnt no_topsec frev_cnf">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <div class="fl ots_cnt">
		  <h1 class="pln-sub-tit">Confirm your offer</h1>
		</div>
        <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/>
        <c:if test="${not empty requestScope.staticContent}">
          <c:out value="${requestScope.staticContent}" escapeXml="false"/>
        </c:if>
        <div class="btn_cnt pt-40"> 
          <div class="rcnf_btn">           
            <div class="fr rgt">
              <%--<a class="fr bton" onclick="offerSuccess('offerFormSuccess','7','8');">CONFIRM OFFER</a><input type="hidden" name="action"/>--%>
              <a class="fr bton" id="confirmRevBtn" onclick="generateApplication();">CONFIRM OFFER</a>
            </div>
          </div>
          <div class="fl lft">
            <a href="javascript:void(0)" onclick="backLink('4', '4');" class="fl bck"><i class="fa fa-long-arrow-left"></i> Back</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <input type="hidden" id="action"/>
  <input type="hidden" id="htmlId"/>
</section>