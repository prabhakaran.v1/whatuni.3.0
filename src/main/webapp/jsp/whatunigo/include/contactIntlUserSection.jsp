<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%
String hotLineNo = request.getAttribute("hotLine") != null ? (String)request.getAttribute("hotLine") : "";
%>
<section class="cmm_cnt chse_cnt">
    <div class="cmm_col_12 no_topsec">
      <div class="cmm_wrap">
        <div class="cmm_row">
        <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/>
        <c:out value="${requestScope.staticContent}" escapeXml="false"/>
          <div class="btn_cnt pt-40">            
            <div class="fr">
            <c:if test="${not empty requestScope.hotLine}">
              <a id="hotlineTextId" href="javascript:void(0);" onclick="callHotline('<%=hotLineNo%>','hotlineTextId');contactGaLogging();" class="fr bton con_btn"><i class="fa fa-phone" aria-hidden="true"></i> CALL NOW</a>
            </c:if>
            </div>
           <jsp:include page="/jsp/whatunigo/include/includeBackLink.jsp"/>
          </div>
    </div>
    </div>
    </div>
</section>
<script type="text/javascript">
jq( document ).ready(function() {
  stopinterval();
  wugoStatsLogging('INTL');
});
</script>