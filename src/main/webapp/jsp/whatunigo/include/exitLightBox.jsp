<%@page import= "WUI.utilities.CommonUtil"%>
 <div class="rvbx_shdw"></div>  
  <div class="lbxm_wrp"> 
    <div class="ext-cont ins_entrq spec_need">
     <div class="revcls ext-cls"><a href="javascript:void(0);" onclick="closeLigBox();"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"></a></div>
     <div class="rev_lst ext">
       <div>
         <h4>Sure you want to go back?</h4>
         <p>Please note that if you say YES, you will lose your place on this course.</p>
       </div>
       <div class="btn_cnt insuf">
         <div class="btnsec_row fl_w100">
         <a href="javascript:void(0)" onclick="cancelTimer('BACK_TO_COURSE');" class="fl bton">Yes</a>
         <a href="javascript:void(0)" onclick="closeLigBox();" class="fr bton">No</a>
         </div>         
       </div>       
     </div>
   </div>
  </div> 