<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants" %>
<%
  String userTariff = (String)request.getAttribute("userTariff");
  int count = 0, subCount = 0, subLen = 0; 
  String pageNo = (String)request.getAttribute("pageNo");
  String qualSub = "";
  String qualId = ""; 
  String actionName = (!GenericValidator.isBlankOrNull((String)request.getAttribute("actionName"))) ? (String)request.getAttribute("actionName") : "EDIT";//PROV_QUAL_EDIT
  String reviewFlag = "N";
  String ajaxActionName = "QUAL_SUB_AJAX";  
  String removeBtnFlag = "display:block";
  String selectFlag = "";
  String gcseSelectFlag = "";
  String addQualStyle = "display:block";
  String addQualBtnStyle = "display:block";
  String gcseSelOption = ": 9-1";
  String removeLink =   CommonUtil.getImgPath("/wu-cont/images/cmm_close_icon.svg",0);
  String qualifications = "";
%>
<section class="cmm_cnt">
  <div class="cmm_col_12 binf_cnt srqual_mn">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/>
        <!-- Uni List -->
        <div class="cmm_unilst qual_cnt">
          <p class="ql_dec">UCAS points is only the first level of requirements. Some courses require specific grade combinations and specific subjects. So make sure you enter all of your qualifications correctly to guarantee the best match.</p>
        </div>
        <!-- Uni List -->
        <!-- Contact details -->
        <c:if test="${not empty requestScope.qualificationList}">
        <form class="fl basic_inf pers_det cont_det qualif revqua_ttip ">
          <!-- UCAS -->
          <div class="ucas_midcnt">
            <div id="ucas_calc">
              <div class="ucas_mid">
                <!--Qualification Repeat Field start 1-->
                
                  <div class="add_qualif">
                  <div class="ucas_refld">
                    <!--<h3 class="un-tit">Level 3</h3>-->
                    <!-- L3 Qualification Row  -->
                    <div class="l3q_rw">
                     <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" begin="0" end="2">
                      <c:set var="indexValue" value="${i.index}"/>
                      <c:set var="selectQualId" value="${qualList.entry_qual_id}"/>
                      <%String selectQualId = pageContext.getAttribute("selectQualId").toString(); %>
                      <c:set var="selectQualId_1" value="<%=selectQualId%>"/>
                      
                      <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexValue").toString()));if(count > 0){addQualStyle = "display:none";}%>
                      <!-- Add Qualification & Subject 1-->		
                      <div class="adqual_rw" data-id=level_3_add_qualif id="level_3_qual_<%=count%>" style="<%=addQualStyle%>">
                        <div class="ucas_row">
                          <h3 class="un-tit">Level 3                          
                            <span class="hlp tool_tip"> <i class="fa fa-question-circle"></i>
                              <span class="cmp hlp-tt">
                                <div class="hdf5"></div>                                
                                <div>Level 3 Qualifications are those studied after school and include: A-Levels, AS Levels, SQA Highers, SQA Advanced Highers and BTECs. Please enter all of the Level 3 qualifications you have studied for and the results you achieved.</div>     
                                <div class="line"></div>
                              </span>
                            </span>
                          </h3>
                          <h5 class="cmn-tit txt-upr">Qualifications</h5>
                          <fieldset class="ucas_fldrw">
                            <div class="remq_cnt">
                              <div class="ucas_selcnt">
                                <fieldset class="ucas_sbox">
                                  <c:set var="selectQualName" value="${qualList.entry_qualification}"/>
                                  <% String selectQualName = pageContext.getAttribute("selectQualName").toString(); %>
                                  <%if("0".equals(String.valueOf(count))){if("A - Levels".equalsIgnoreCase(selectQualName)){selectQualName="A Level";}}qualifications += selectQualName + ",";%>
                                  <span class="sbox_txt" id="qualspan_<%=count%>"><%=selectQualName%></span>
                                  <i class="fa fa-angle-down fa-lg"></i>
                                </fieldset>
                                <select name="qualification1" onchange="appendQualDiv(this, '', '<%=count%>', 'new_entry_page');" id="qualsel_<%=count%>" class="ucas_slist">
                                  <option value="0">Please Select</option>
                                  
                                  <c:forEach var="qualListLevel3" items="${requestScope.qualificationList}" >
                                   
                                   <jsp:scriptlet>selectFlag = "";if("".equals(selectQualId)){selectQualId="1";}</jsp:scriptlet>
                                   <c:set value="<%=selectQualId%>" var="selectQualId"/>
                                   <c:if test="${qualListLevel3.entry_qual_id eq selectQualId}">
                                     <jsp:scriptlet>selectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                   </c:if>
                                   <c:if test="${qualListLevel3.entry_qualification ne 'GCSE'}">
                                     <c:if test="${empty qualListLevel3.entry_qual_id}">
                                       <optgroup label="${qualListLevel3.entry_qualification}"></optgroup>
                                     </c:if>
                                     <c:if test="${not empty qualListLevel3.entry_qual_id}">
                                       <option value="${qualListLevel3.entry_qual_id}" <%=selectFlag%>>${qualListLevel3.entry_qualification}</option>
                                     </c:if>                                  
                                   </c:if>
                                  </c:forEach>
                                </select>
                                <c:forEach var="grdQualifications" items="${requestScope.qualificationList}" >
                                 <c:if test="${grdQualifications.entry_qualification ne 'GCSE'}">                               
                                   <c:if test="${not empty grdQualifications.entry_qual_id}">
                                     <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                     <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}" name="" value="${grdQualifications.entry_grade}" />
                                   </c:if>
                                 </c:if>                               
                               </c:forEach>
                              </div>
                            </div>
                            <%if(0!= count){%>
                            <div class="add_fld">
                              <a id="1rSubRow3" onclick="javascript:removeQualification('level_3_qual_<%=count%>', <%=count-1%>, 'addQualBtn_<%=count%>');" class="btn_act1 bck f-14 pt-5">
                                <span class="hd-mob">Remove Qualification</span><!--<span class="hd-desk"><i class="fa fa-times"></i></span>-->
                              </a>
                            </div>
                            <%}%>                            
                          </fieldset>
                          <div class="err" id="qualSubId_error" style="display:none;"></div>
                        </div>
                        <div class="subgrd_fld" id="subGrdDiv<%=count%>">
                        <div class="ucas_row grd_row" id="grdrow_<%=count%>">
                        
                            <fieldset class="ucas_fldrw quagrd_tit">
                              <div class="ucas_tbox tx-bx">
                                <h5 class="cmn-tit txt-upr qual_sub">Subject</h5>
                              </div>
                              <div class="ucas_selcnt rsize_wdth">
                                <h5 class="cmn-tit txt-upr qual_grd">Grade</h5>
                              </div>
                            </fieldset>                            
                              <c:set var="selectQualSubLen" value="${qualList.entry_subject}"/>
                              <%String  selectQualSubLen = pageContext.getAttribute("selectQualSubLen").toString();%>
                              <%if(selectQualSubLen!=null && !selectQualSubLen.trim().equals("")){subLen = Integer.parseInt(selectQualSubLen);}else{selectQualSubLen="3";}%> 
                            <c:forEach var="subjectList" items="${requestScope.qualificationList}" begin="0" varStatus="j" end="<%=Integer.parseInt(selectQualSubLen)%>">  
                              <c:set var="indexValue" value="${j.index}"/>
                              <%subCount = Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1;%>
                              <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
                                <fieldset class="ucas_fldrw">
                                  <div class="ucas_tbox w-390 tx-bx">
                                    <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="subIp_<%=subCount%>" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="" class="frm-ele" autocomplete="off">															
                                    <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                                    <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                                    </div>
                                  </div>
                                  <div class="ucas_selcnt rsize_wdth">
                                    <div class="ucas_sbox mb-ht w-84" id="1grades1">
                                      <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">A*</span><i class="fa fa-angle-down fa-lg"></i>
                                    </div>
                                    <select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" id="qualGrd_<%=count%>_<%=subCount%>"></select>
                                    <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=count%>_<%=subCount%>','${subjectList.entry_grade}'); </script>                                    
                                    <input class="subjectArr" data-id="level_3" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_<%=selectQualId%>" value="" />                                
                                    <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_<%=selectQualId%>" value="A*" />
                                    <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_<%=selectQualId%>" value="<%=selectQualId%>" />
                                    <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_<%=selectQualId%>" value="<%=count+1%>" />
                                  </div>
                                  <%if(subCount == 0){
                                    removeBtnFlag = "display:none";
                                  }else{
                                    removeBtnFlag = "display:block";
                                  }
                                  if(subCount == 1){%>                                  
                                    <script>jq("#remSubRow<%=count%>0").show();</script>
                                  <%}%>                                  
                                  <div class="add_fld"> <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"></span></a></div>
                                </fieldset>
                              </div>
                            </c:forEach>                      
                            <div class="ad-subjt" id="addSubject_<%=count%>" style="display:block">
                              <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('<%=selectQualId%>','countAddSubj_<%=count%>', '<%=count%>');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                            </div>                          
                          </div>
                        </div>
                        
                        <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                      </div>
                      </c:forEach>
                      <!-- Add Qualification & Subject 1 -->
                      <!-- L3 Qualification Row -->
                    </div>
                    <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" end="1">
                      <c:set var="indexValue" value="${i.index }"/>
                      <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexValue")));if(count > 0){addQualBtnStyle = "display:none";}%>
                      <div class="add_qualtxt" id="addQualBtn_<%=count%>" style="<%=addQualBtnStyle%>">
                        <p class="aq_hlptxt">Studied more than one qualification? Select additional below</p>
                        <a href="javascript:void(0);" onclick="addQualification('level_3_qual_<%=count + 1%>', <%=count + 1%>, 'addQualBtn_<%=count%>');" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> ADD A QUALIFICATION</a>
                      </div>
                    </c:forEach>
                    <!-- Tariff Points Container Start -->
                    <!-- Tariff Points Container End -->		
                  </div>
                </div>
                <!--Qualification Repeat Field end 1 -->
                
                <!--Qualification Repeat Field start 2-->
                <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" >
                  <c:set var="indexIValue" value="${i.index}"/>
                  <c:if test="${qualList.entry_qual_id eq '17'}">
                  <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue")));%>
                  <div class="add_qualif">
                    <div class="ucas_refld">
                      <h3 class="un-tit">
                        Level 2: GCSE
                        <span class="hlp tool_tip"> <i class="fa fa-question-circle"></i>
                          <span class="cmp hlp-tt">
                            <div class="hdf5"></div>
                            <div> GCSEs are the qualifications you studied for and achieved at secondary school. Please enter all of the GCSE subjects you took and the results you achieved.</div>
                            <div class="line"></div>
                          </span>
                        </span>
                      </h3>
                      <p class="ql_dec">Please enter your GCSE grades below. You'll need to select whether your grade scores are letters or numerical. Please refer to your certificate for the correct grade.</p>
                      <!-- L3 Qualification Row  -->
                      <div class="l3q_rw" id="qual_level_<%=count%>">
                        <!-- Add Qualification & Subject 1-->		
                        <!-- Add Qualification & Subject 2-->		
                        <div class="adqual_rw" data-id="level_2_add_qualif">                          
                            <div class="ucas_row">
                            <h5 class="cmn-tit txt-upr">GRADE TYPE </h5>
                              <fieldset class="ucas_fldrw">
                                <div class="remq_cnt">
                                  <div class="ucas_selcnt">
                                    <fieldset class="ucas_sbox">
                                      <c:set var="selectQualLeve2Name" value="${qualList.entry_qualification}"/>
                                      <%String selectQualLeve2Name = pageContext.getAttribute("selectQualLeve2Name").toString(); %>
                                      <span class="sbox_txt" id="qualspan_<%=count%>"><%=selectQualLeve2Name%>: A*-G</span>
                                      <i class="fa fa-angle-down fa-lg"></i>
                                    </fieldset>
                                     <select name="qualification1" onchange="appendQualDiv(this, '', '<%=count%>', 'new_entry_page');" id="qualsel_<%=count%>" class="ucas_slist">
                                     <option value="0">Please Select</option>
                                       <c:forEach var="qualListLevel2" items="${requestScope.qualificationList}" >
                                         <c:if test="${qualListLevel2.entry_qualification eq 'GCSE'}">
                                             <jsp:scriptlet>gcseSelectFlag = "selected=\"selected\"";</jsp:scriptlet>                              
                                           <option value="${qualListLevel2.entry_qual_id}_gcse_new_grade">${qualListLevel2.entry_qualification}: 9-1 </option>                               
                                           <option value="${qualListLevel2.entry_qual_id}_gcse_old_grade" <%=gcseSelectFlag%>>${qualListLevel2.entry_qualification}: A*-G</option>
                                          </c:if>
                                        </c:forEach>
                                      </select>
                                      <c:forEach var="grdQualifications" items="${requestScope.qualificationList}">
                                         <c:if test="${grdQualifications.entry_qualification eq 'GCSE'}">                                                              
                                           <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}_gcse" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                           <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_new_grade" name="" value="${grdQualifications.entry_grade}" />
                                           <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_old_grade" name="" value="${grdQualifications.entry_old_grade}" />
                                         </c:if>
                                      </c:forEach>
                                   </div>
                                 </div>
                                <%--
                                <div class="add_fld">
                                  <a id="1rSubRow3" onclick="javascript:removeSubRow(1,3);" class="btn_act1 bck f-14 pt-5">
                                    <span class="hd-mob">Remove Qualification</span>
                                  </a>
                                </div>
                                --%>
                              </fieldset>
                              <div class="err" id="qualSubId_error" style="display:none;"></div>
                            </div>                          
                          <div class="subgrd_fld" id="subGrdDiv<%=count%>">
                            <div class="ucas_row grd_row" id="grdrow_<%=count%>">                            
                              <fieldset class="ucas_fldrw quagrd_tit">
                                <div class="ucas_tbox tx-bx">
                                  <h5 class="cmn-tit txt-upr qual_sub">Subject</h5>
                                </div>
                                <div class="ucas_selcnt rsize_wdth">
                                  <h5 class="cmn-tit txt-upr qual_grd">Grade</h5>
                                </div>
                              </fieldset>
                              <c:set var="selectQualLevel2SubLen" value="${qualList.entry_subject}"/>
                              <%String selectQualLevel2SubLen = pageContext.getAttribute("selectQualLevel2SubLen").toString(); %>
                              <%subLen = Integer.parseInt(selectQualLevel2SubLen);%> 
                              <c:forEach var="subjectList" items="${requestScope.qualificationList}" varStatus="j" end="<%=Integer.parseInt(selectQualLevel2SubLen)%>">
                               <c:set var="indexJValue" value="${j.index}"/>
                              <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue")));%>
                                <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
                                  <fieldset class="ucas_fldrw">
                                    <div class="ucas_tbox w-390 tx-bx">
                                      <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="subIp_<%=subCount%>" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="" class="frm-ele" autocomplete="off">															
                                      <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                                      <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                                      </div>
                                    </div>
                                    <div class="ucas_selcnt rsize_wdth">
                                      <div class="ucas_sbox mb-ht w-84" id="1grades1">
                                        <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">A*</span><i class="fa fa-angle-down fa-lg"></i>
                                      </div>
                                      <select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" id="qualGrd_<%=Integer.parseInt(String.valueOf(pageContext.getAttribute("indexValue")))%>_<%=Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue")))%>"></select>                                      
                                      <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=Integer.parseInt(String.valueOf(pageContext.getAttribute("indexValue")))%>_<%=Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue")))%>','${subjectList.entry_grade}'); </script>
                                      
                                      <input class="subjectArr" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${qualList.entry_qual_id}" value="" />                                
                                      <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_${qualList.entry_qual_id}" value="A*" />
                                      <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${qualList.entry_qual_id}" value="${qualList.entry_qual_id}" />
                                      <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${qualList.entry_qual_id}" value="<%=count+1%>" />
                                    </div>
                                    <%if(subCount == 0){
                                      removeBtnFlag = "display:none";
                                    }else{
                                      removeBtnFlag = "display:block";
                                    }
                                    if(subCount == 1){%>                                  
                                      <script>jq("#remSubRow<%=count%>0").show();</script>
                                    <%}%> 
                                    <div class="add_fld"> <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"></span></a> </div>
                                  </fieldset>
                                </div> 
                                </c:forEach>                               
                              <div class="ad-subjt" id="addSubject_<%=count%>" style="display:block">
                                <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('${qualList.entry_qual_id}','countAddSubj_<%=count%>', '<%=Integer.parseInt(String.valueOf(pageContext.getAttribute("indexValue")))%>');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                              </div>                            
                          </div>
                        </div>
                        <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <!-- Add Qualification & Subject 2 -->
                      </div>
                    </div>
                    </div>
                  </div>
                  <script>jq( document ).ready(function() {appendQualDiv(jq('#qualsel_'+<%=count%>).get(0), '', '<%=count%>', 'new_entry_page');});</script>
                  </c:if>
                </c:forEach>
                <!--Qualification Repeat Field end 2 -->
              </div>
              <%--<div class="err" id="subErrMsg" style="display:none;"></div>--%>
            </div>
            <!-- UCAS end -->
            <!-- Terms & Condition -->
            <%--
            <div class="pt-25 qua_cnf">
              <div class="rngsl_cnt">
                <div class="ucscr_dis fl_w100">Your UCAS score is <span id="ucasPt">0 points</span></div>
              </div>
            </div>
            --%>
              <div id="scoreDiv" class="fl cmqua mt-40 w-647">
										<div class="ucas_btm ctrl_mt scr_mt0">
										<div class="ucas_row row_btm30">
											<div id="innerScoreDiv" class="scr_wrap">
												<div class="scr_lft">
                            <div class="chartbox fl w100p">
                              <h2>Your UCAS score</h2>
                              <div class="ucspt_val" id="ucasPt">0 points</div>  
                            </div>
													</div>
											</div>
										</div>
									</div>
								</div>            
            
            
            
            
            <div class="pt-25 qua_cnf">
              <h3 class="un-tit p-0 f-20 pt-0">Confirmation</h3>
              <p class="cnfrm f-16">Please review and make sure you have entered your score correctly. If your score is different to what you have entered <c:if test="${not empty requestScope.COLLEGE_DISPLAY_NAME}">then ${requestScope.COLLEGE_DISPLAY_NAME}</c:if> is within their rights not to make you a formal offer. <a href="javascript:void(0);" onclick="showTermsConditionPopup();">See full terms &amp; conditions here</a></p>
              <div class="btn_chk">
                <span class="chk_btn">
                <input onclick="enableMarkComplete('entryReqSecId');" type="checkbox" name="qualTCId" value="N" id="qualTCId">
                <span class="checkmark grey"></span>
                </span>
                <p><label for="qualTCId" class="chkbx_100" id="labelUcasPt">I confirm that all the information I have given is true and accurate, including details of my qualification information and I have achieved <c:if test="${not empty requestScope.userTariff}">${requestScope.userTariff} UCAS points.</c:if></label></p>
                <%--<p class="qler1" id="qualTCId_error" style="display:none;"></p>--%>                                    
              </div>
                <p class="qler1" id="qualTCId_error" style="display:none;"></p>
                <p class="qler1" id="subErrMsg" style="display:none;"></p> 
                <input type="hidden" id="oldGrade" value="<%=userTariff%>" />
                <input type="hidden" id="newGrade" value="<%=userTariff%>" />
             </div>
            <div class="btn_cnt pt-40">
              <div class="fr">
                <a href="javascript:void(0);" onclick="passJSONObj('<%=actionName%>', '5', '<%=reviewFlag%>', '', '', 'NEW_USER');" class="fr bton">SAVE AND CONTINUE <i class="fa fa-long-arrow-right"></i></a>
              </div>              
            </div>
            <!-- Terms & Condition -->
            <!-- Contact details -->
          </div>
        </form>
        </c:if>
      </div>
    </div>
  </div>
</section>
<input type="hidden" id="newQualDesc" value="<%=qualifications%>" />
<input type="hidden" id="action" />
<input type="hidden" id="removeLinkImg" value="<%=removeLink%>" />
<script>
jq( document ).ready(function() {
 appendQualDiv(1, '', '0', 'new_entry_page', 'add_Qual');  
 setTimerCookie('offer_timer','','0');
 stopinterval();
 startTimer1();
 });
</script>