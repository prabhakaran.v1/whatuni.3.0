<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${not empty requestScope.pageNo}">
  <% String pageNo = (String)request.getAttribute("pageNo");
     int prevPage = Integer.parseInt(pageNo) - 1; 
     String action = request.getParameter("action");   
  %>
  <div class="fl">
 
  <% if("1".equalsIgnoreCase(pageNo)) { %>
    <a href="javascript:void(0)" onclick="backLink('', '');" class="fl bck"><i class="fa fa-long-arrow-left"></i> Back</a>
  <%}  else if("4".equalsIgnoreCase(pageNo) && ("EDIT".equalsIgnoreCase(action))) { %>
      <a href="javascript:void(0)" onclick="backLink('<%=pageNo%>','<%=pageNo%>');" class="fl bck"><i class="fa fa-long-arrow-left"></i> Back</a>
  <% } else { %>
      <a href="javascript:void(0)" onclick="backLink('<%=pageNo%>','<%=prevPage%>');" class="fl bck"><i class="fa fa-long-arrow-left"></i> Back</a>
  <% } %>

  </div>
</c:if>