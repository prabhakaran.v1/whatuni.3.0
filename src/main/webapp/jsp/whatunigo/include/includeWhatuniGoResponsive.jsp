<%@page import="WUI.utilities.CommonUtil,WUI.utilities.GlobalConstants"%>
<%
 String cmmtMobileCSS = CommonUtil.getResourceMessage("wuni.go.mobile.css", null);
 String jQueryJsName = CommonUtil.getResourceMessage("wuni.jquery.js", null);
 String cmmtHeaderCSS = CommonUtil.getResourceMessage("wuni.go.style.header.css", null);
 String cmmtMainCSS = CommonUtil.getResourceMessage("wuni.cmm.main.style.css", null);
%>
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=cmmtHeaderCSS%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=cmmtMainCSS%>" media="screen" />  
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/home/<%=jQueryJsName%>"> </script>    
<script type="text/javascript" language="javascript">
var dev = jQuery.noConflict();
dev(document).ready(function(){
  adjustStyle();
  var arr = ["email", "hideTooltip('emailAddressYText');", "clearErrorMsg('wugoEmailId','registerErrMsg');"];
  dev("#email").autoEmail(arr);
  dev('.frm-ele').blur(function () {
    var x = dev(this).val();
    if (x != "") {
      dev(this).next('label').addClass("top_lb");
    } else {
      dev(this).next('label').removeClass("top_lb");
    }
  });
  dev('.frm-ele').each(function(index) {      
    var xy = dev(this).val();
    if (xy != "") {
      dev(this).next('label').addClass("top_lb");
    } else {
      dev(this).next('label').removeClass("top_lb");
    } 
  });
});
adjustStyle();
function jqueryWidth() {
  return dev(this).width();
} 
function adjustStyle() {    
    var width = document.documentElement.clientWidth;
    var path = ""; 
    //included for dynamic load css for mobile view
    if (width <= 1024) {      
      dev("#size-stylesheet").attr("href", "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=cmmtMobileCSS%>");      
    }
    //     
    dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
    dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
    if (width <= 480) {    
            
    } else if ((width > 480) && (width <= 992)) {      
             
    } else {                
        document.getElementById('size-stylesheet').href = "";  
        //dev(".hm_srchbx").hide();
    }    
}
dev(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
    setTimeout(function() {
        dev(window).trigger('resize');        
    }, 500);
}
dev(window).resize(function() {  
  dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
  dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());   
  adjustStyle();   
});
</script>
<input type="hidden" id="domainLstId" value='<%=CommonUtil.getResourceMessage("wuni.form.email.domain.list", null)%>' />
<input type="hidden" id="cmmtPages" value='<%=GlobalConstants.CMMT_PAGE_NAMES%>' />