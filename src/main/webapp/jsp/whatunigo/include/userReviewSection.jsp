<%@ taglib uri="http://java.sun.com/jstl/core"  prefix="c" %>
<jsp:scriptlet>
  String plusCls = "fa-plus-circle";
  String questionIds = "";
  String checkedFlag = "checked='checked'";
  String hideFlag = "display:block;";
  String clsName = "bn-outln";
  String validateStr = "";
</jsp:scriptlet>  
<section class="cmm_cnt">
   <div class="brd_crumb timer_sec">
          <div class="cmm_col_12">
            <div class="cmm_wrap">
                <div class="brc_cnt lft fl">
                  <div class="brc_row1">
                    <span class="bc_hdtxt">Review</span><span class="bc_stpno">Step 5 of 5</span>
                  </div>
                  <jsp:include page="/jsp/whatunigo/include/includeStepsSection.jsp"/>
                </div>
                 <jsp:include page="/jsp/whatunigo/include/timer.jsp"/> 
            </div>
      </div>
    </div>
    <div class="cmm_col_12 binf_cnt rev_appl">
      <div class="cmm_wrap">
        <div class="cmm_row">
        <div class="fl ots_cnt">
					<h1 class="pln-sub-tit">Time for one last check...</h1>
					<p class="cnfrm f-16">Please make sure everything is correct before submitting.</p>
		    </div>
        
        <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/>
      <c:if test="${not empty requestScope.sectionInfoList}">
        <div class="accordion_container">
      
        <c:forEach var="sectionList" items="${requestScope.sectionInfoList}">
          <c:if test="${'Review' ne sectionList.page_name}">
          <div class="accordion_head" >
           <c:set var="pageId" value="${sectionList.page_id}" />
          <jsp:scriptlet>plusCls = "fa-plus-circle";</jsp:scriptlet>
          <c:if test="${'1' eq sectionList.page_id}">
            <jsp:scriptlet>plusCls = "fa-minus-circle";</jsp:scriptlet>
          </c:if>
            <div class="ahd_row tabsec${sectionList.page_id}">
              <h3 class="acr-tit fl" onclick="reviewSection('${sectionList.page_id}');">${sectionList.page_name}</h3><span class="plusminus" onclick="reviewSection('${sectionList.page_id}');"><i id="i_${sectionList.page_id}" class="fa <%=plusCls%>" aria-hidden="true"></i></span>
              <c:if test="${'1' eq sectionList.page_id}">
              <c:if test="${not empty requestScope.questionAnswerList}">
              <div class="accordion_body" id="section_${sectionList.page_id}">
              <c:forEach var="qusAnsList" items="${requestScope.questionAnswerList}">
                 <jsp:scriptlet>hideFlag = "display:block;";</jsp:scriptlet>
              <c:if test="${'N' eq qusAnsList.default_show_flag}">
                <jsp:scriptlet>hideFlag = "display:none";</jsp:scriptlet>
              </c:if>
              <div class="abdy_ fl_w100" style="<%=hideFlag%>" id="qus_${qusAnsList.page_question_id}">
              <div class="revap_rw fl_w100">
                <h5 class="cmn-tit" id="tit_${qusAnsList.page_question_id}"><span class="cmn-tit1">${qusAnsList.question_title}<c:if test="${'Y' eq qusAnsList.mandatory_flag}">*</c:if></span> <span class="qedt_md"><a href="javascript:void(0)" onclick="editSec('${qusAnsList.page_question_id}');">Edit</a></span></h5>
                <h6 class="cmn-ansr" id="head_${qusAnsList.page_question_id}">${qusAnsList.answer_value}</h6>
                </div>
                 <div class="biedt_sec" id="sect_${qusAnsList.page_question_id}" style="display:none;">
                  <div class="bas_lst pt-40">
                    <h5 class="cmn-tit">${qusAnsList.question_title}<c:if test="${'Y' eq qusAnsList.mandatory_flag}">*</c:if></h5>
                    <div class="blst_rw" id="div_${qusAnsList.page_question_id}" style="<%=hideFlag%>">
                    <c:if test="${'RADIO' eq qusAnsList.format_desc}">
                      
                      <c:forEach var="qusOptions" items="${qusAnsList.question_options}">
                        <c:set var="questionId" value="${qusAnsList.page_question_id}" />
                        <c:set var="format" value="${qusAnsList.format_desc}" />
                        <c:set var="optionId1" value="${qusOptions.option_id}" />
                                
                                <jsp:scriptlet>
                         checkedFlag = ""; clsName = "";
                       </jsp:scriptlet>
                       <c:if test="${'optionId1' eq qusAnsList.answer_option_id}">
                         <jsp:scriptlet>checkedFlag = "checked='checked'"; clsName = " bn-gry active";</jsp:scriptlet>
                       </c:if>
                        
                        <a class="opt_${questionId} bn i-bn bn-outln <%=clsName%>" id="${optionId1}" name="${questionId}" onclick="changeRadio(this.id, '${questionId}');changeBtn('${questionId}', 'REVIEW_PAGE');" ${qusOptions.option_desc} style="<%=hideFlag%>">${qusOptions.option_desc}</a>
                      </c:forEach>
                      <input type="hidden" id="hid_${qusAnsList.page_question_id}" class="${qusAnsList.answer_option_id}" value="${qusAnsList.answer_value}"/>
                    </c:if>
                    
                    
                    <c:if test="${'DROPDOWN' eq qusAnsList.format_desc}">
                       <div class="ucas_selcnt w-300" >
                         <div class="ucas_sbox">
                          <span class="sbox_txt" id="select_${qusAnsList.page_question_id}_span">Please Choose</span>
                          <i class="fa fa-angle-down fa-lg"></i>
                         </div>
                        <select name="${qusAnsList.page_question_id}" id="${qusAnsList.format_desc}_${qusAnsList.page_question_id}" class="ucas_slist" onchange="selectChange('div_${qusAnsList.page_question_id}', '${qusAnsList.page_question_id}');">
                          <option>Please Choose</option>
                          <c:forEach var="qusOptions" items="${qusAnsList.question_options}">
                             <c:set var="selectId" value="${qusOptions.option_id}" />
                             <jsp:scriptlet>checkedFlag = "";</jsp:scriptlet>
                             <c:if test="${selectId eq qusAnsList.answer_option_id}">
                               <jsp:scriptlet>checkedFlag = "selected";</jsp:scriptlet>
                             </c:if>
                             <option value="${qusOptions.option_id}" <%=checkedFlag%>>${qusOptions.option_desc}</option>
                          </c:forEach>
                        </select>
                        <p class="fail-msg" id="${qusAnsList.format_desc}_${qusAnsList.page_question_id}_Err" style="display:none;"></p>
                      </div>
                    </c:if>
                    
              </div>       
                    <p class="fail-msg" id="${qusAnsList.format_desc}_${qusAnsList.page_question_id}_Err" style="display:none;"></p>
                  </div>
                  <div class="qedt_md"><a href="javascript:void(0)" onclick="saveChange('${qusAnsList.page_question_id}', '${qusAnsList.format_desc}', '${sectionList.page_id}');">Confirm changes</a></div>
                </div>
              </div>
               
              </c:forEach>
              
             <div class="btn_cnt">
                <div class="fl">
                  <div class="btn_cmps fl">
                    <div class="reltv fl">
                      <label class="cnfrm fl">
                      <c:if test="${'Y' eq qusAnsList.section_review_flag}">
                        <input type="checkbox" checked="true" id="secRev_form_${sectionList.page_id}" data-id="secRevBtn" onclick="valUserRevTCBtn('${sectionList.page_id}');">
                      </c:if>
                      <c:if test="${'Y' ne qusAnsList.section_review_flag}">
                        <input type="checkbox" id="secRev_form_${sectionList.page_id}" data-id="secRevBtn" onclick="valUserRevTCBtn('${sectionList.page_id}');">
                      </c:if>                      
                      <span class="checkmark grn blu"></span> <span class="bn bn-grn grn_txt bn-blue"><span class="macomp">Section Reviewed</span><span class="frmcomp">Section Reviewed</span></span> 
                     </label>
                    </div>
                  </div>
                </div>
						  </div>
              
             </div>
             </c:if>
             </c:if>
             <c:if test="${'1' eq sectionList.page_id}">
               <div class="accordion_body" id="section_${sectionList.page_id}" style="display:none;"></div>
             </c:if>
             
            </div>
            
          </div>
          </c:if>
          
</c:forEach>

      </div>
     </c:if> 
      <!-- Basic Info -->
      <div class="bas_info">
        <div class="btn_cnt pt-40">
          <div class="fr">
          <input type="hidden" id="action" value=""/>
          <input type="hidden" id="htmlId" value=""/>            
          <input type="hidden" id="sectId" value="1"/>
          <c:if test="${not empty requestScope.pageInfoList}">
            <c:forEach var="pageInfoList" items="${requestScope.pageInfoList}">
              <input type="hidden" class="section_review" id="revSecFlag_${pageInfoList.page_id}" value="${pageInfoList.reviewed_flag}" />
            </c:forEach>
          </c:if>
          <input type="submit" onclick="reviewSubmit('5', '6', '', 'LB');" id="revSubmitFlagId" class="bton disbld" value="CONTINUE" ></input>
          </div>
          <jsp:include page="/jsp/whatunigo/include/includeBackLink.jsp"/>
        </div>
      </div>
    <!-- Basic Info -->
    </div>
    </div>
    </div>
</section>
<script> selectChange('div_5', '5');userRevBtnShow('4');enableRevSecBtn('form_1');changeBtn('4', 'ONLOAD');enableDisTCondiBtn();</script>