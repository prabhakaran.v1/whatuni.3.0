<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.GlobalConstants,WUI.utilities.CommonUtil,com.wuni.util.seo.SeoUrls, WUI.utilities.CommonFunction" %>
<%String userSubmittedDate = (String)request.getAttribute("userSubmittedDate");

 String userStatusCode = (String)request.getAttribute("userStatusCode");
 String pageName = request.getParameter("PAGE_NAME");
String fbErrorId = "fblblogin_error";
  if("sign up".equalsIgnoreCase(pageName)){
    fbErrorId = "fblbregistration_error";
  }
  String timer1EndTime = (String)request.getAttribute("timer1EndTime");
  String timer2EndTime = (String)request.getAttribute("timer2EndTime");
  String statusTitle = null;
  String statusText = "If you want to accept this Offer in Principle, please complete your application within the set time limit. If the course no longer interests you, please cancel it so that we can release it to another student.";
  String statusTextClass="cnf_off ots_cnt";
  if("3".equals(userStatusCode)){
  statusTitle = "Your status: accepted";
  statusText = "Congratulations! You have accepted your Offer in Principle";
   statusTextClass="cnf_off";
  }
  if("1".equals(userStatusCode)){
  statusTitle = "Your status: Application not completed";
  }
  if("2".equals(userStatusCode)){
  statusTitle = "Your status: You've got an Offer in Principle";
  }
  String timer1Status = (String)request.getAttribute("timer1Status");
  String endTimeT1 = (String)request.getAttribute("C_START_TIME_T1");
  String startTimeT1 = (String)request.getAttribute("C_END_TIME_T1");
  String hotLine = (String)request.getAttribute("hotLine");
  String collegeDisplayName = (String)request.getAttribute("COLLEGE_DISPLAY_NAME");
  String collegeName = (String)request.getAttribute("hitbox_college_name");
  String collegeId = (String)request.getAttribute("hitbox_college_id");
  String profilePageURL = new SeoUrls().construnctProfileURL(collegeId,collegeName,GlobalConstants.CLEARING_PAGE_FLAG);
  String APPLY_NOW_COURSE_ID = (String)request.getAttribute("APPLY_NOW_COURSE_ID");
  String APPLY_NOW_OPPORTUNITY_ID = (String)request.getAttribute("APPLY_NOW_OPPORTUNITY_ID");
  String REFERER_URL = (String)request.getAttribute("REFERER_URL");
  String DELETED_COURSE_FLAG = (String)request.getAttribute("DELETED_COURSE_FLAG");
  CommonFunction common = new CommonFunction();
  String[] yoeArr = common.getYearOfEntryArr();
  String YOE_1 = yoeArr[0];
  %>
<%if(!"0".equals(userStatusCode)){%>
  <section id="applicationPage" class="cmm_cnt ebk_suc sta_cnt provlgo_hid">
    <div class="cmm_col_12 chse_cnt cngs_cnt">
      <div class="cmm_wrap">
        <div class="artfc st-title">
          <h2>Your Application</h2>  
          <h3 class="un-tit"><%=statusTitle%>
            <%if("2".equals(userStatusCode)){%>
              <a href="javascript:void(0)" class="blu-lnk rltv bsc"><span>What's this?</span>
                <span id="postcodeText" class="cmp adjst">
                  <div class="hdf5"></div>
                  <div>An 'Offer in Principle' means that the details you have given us regarding your grades meet the entry requirements given to us by the university for this course. 
                  Once you've accepted your Offer in Principle, the uni will receive your application and will contact you to discuss any next steps and confirm your offer.</div>
                  <div class="line"></div>
                </span>
              </a>
            <%}%>
          </h3>
          <div class="<%=statusTextClass%>">
            <p><%=statusText%></p>
            <%if(!"3".equals(userStatusCode)){%>
              <div class="sub_btn">
                <a class="btn1" onclick="continueApp();" href="/degrees/wugo/apply-now.html?courseId=${requestScope.APPLY_NOW_COURSE_ID}&opportunityId=${requestScope.APPLY_NOW_OPPORTUNITY_ID}">COMPLETE APPLICATION<i class="fa fa-long-arrow-right"></i></a>
              </div>
            <%}%>
          </div>            
        </div>
        <div class="cmm_row">
          <c:if test="${not empty requestScope.courseInfoList}">
            <c:forEach items="${requestScope.courseInfoList}" var="courseInfoList">
              <div class="cmm_unilst">
                <div class="fl uni-logo col_lft">
                  <a href="${courseInfoList.profilePageUrl}"><img src="<%=CommonUtil.getImgPath("",0)%>${courseInfoList.college_logo}" alt=""></a>
                </div>
                <%if("3".equals(userStatusCode)){%>
                  <div class="fl col_rgt">
                    <h3 class="un-tit"><a href="${courseInfoList.profilePageUrl}">${courseInfoList.college_display_name}</a></h3>
                    <%if("Y".equalsIgnoreCase(DELETED_COURSE_FLAG)){%>
                      <h6 class="sub-tit">${courseInfoList.course_Title}</h6>
                    <%}else{%>
                      <h6 class="sub-tit"><a href="${courseInfoList.cdPageUrl}">${courseInfoList.course_Title}</a></h6>
                    <%}%>
                    <div class="fx">
                      <div class="fx-bx">
                        <h5>Offer accepted</h5>
                        <h6><%=userSubmittedDate%></h6>
                      </div>
                    </div>
                  </div>
                <%}%>
                <%if(!"3".equals(userStatusCode)){%>
                  <div class="fl col_rgt">
                    <div class="rgt_col_lft">
                      <h3 class="un-tit"><a href="${courseInfoList.profilePageUrl}">${courseInfoList.college_display_name}</a></h3> 
                      <%if("Y".equalsIgnoreCase(DELETED_COURSE_FLAG)){%>
                        <h6 class="sub-tit">${courseInfoList.course_Title}</h6>
                      <%}else{%>
                        <h6 class="sub-tit"><a href="${courseInfoList.cdPageUrl}">${courseInfoList.course_Title}</a></h6>
                      <%}%>
                      <div class="fx">
                        <div class="fx-bx">
                          <h5>Offer accepted</h5>
                          <h6><%=userSubmittedDate%></h6>
                        </div>              
                      </div>
                    </div>
                    <div class="rgt_col_rgt">
                      <div class="fx" id="timerStky">
                        <div class="fx-bx">
                          <h5>Time remaining</h5>
                          <%if("1".equals(userStatusCode)){%>
                            <h6 id="timer"></h6>
                          <%}%>
                          <%if("2".equals(userStatusCode)){%>
                            <h6 id="timer"></h6>
                          <%}%>
                        </div>              
                      </div>
                      <div class="fx cbtn_fx">
                        <div class="fx-bx clr15">
                          <%if("1".equals(userStatusCode)){%>
                            <div class="btns_interaction">
				              <a onclick="cancelTimer('CANCEL_TIMER_1');" class="cbtn" id="cancelBtn">Cancel Offer<i class="fa fa-long-arrow-right"></i> </a>
				            </div>
                          <%}%>
                          <%if("2".equals(userStatusCode)){%>
                            <div class="btns_interaction">
				              <a onclick="cancelTimer('CANCEL_TIMER_2');" class="cbtn" id="cancelBtn">Cancel Offer<i class="fa fa-long-arrow-right"></i> </a>
				            </div>
                          <%}%>
                        </div>              
                      </div>
                    </div>
                  </div>
                <%}%>
              </div> 
            </c:forEach>
          </c:if>
          <%if("3".equals(userStatusCode)){%>    
            <div class="cnf_off"> 
              <h2>Next steps</h2>    
              <p>The <%=collegeDisplayName%> will be in touch with you soon* to discuss your next steps. Share the excitement with your friends!</p>
              <div class="sgn">
                <div class="so-btn">
                  <a class="btn1 fbbtn" onclick="shareFBTweet('FB', 'APP')" title="Share Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK</a>
                  <a class="btn1 twbtn" onclick="shareFBTweet('TWEET', 'APP')" title="Share Twitter"><i class="fa fa-twitter fa-1_5x"></i>TWITTER</a> 
                </div>
                <p class="qler1 valid_err" id="<%=fbErrorId%>" style="display: none;"></p>    
              </div>
              <div class="appbtwn_time fl_w100">
                <p>* If you make your application between 9am-5pm, Mon-Fri, the uni will be in touch within 1 hour. If you make you application outside these times, the uni will contact you on the next working day.</p>
                <p>** Please note that if your offer is cancelled by either you or the university, you will be unable to make another online application via Whatuni Go this year. <a href="javascript:void(0);" onclick="showTermsConditionPopup();">Read our full T&Cs here.</a></p>
              </div>
            </div>
          <%}%>
        </div>
      </div>
    </div>  
    <%if("3".equals(userStatusCode)){%>  
      <jsp:include page="/jsp/whatunigo/include/articleSection.jsp"/>   
      <div class="hst_cnt">
        <div class="hst_row">
          <h2>Having second thoughts?</h2>
          <p><a onclick="callLightBox('0##SPLIT##CANCEL_OFFER');">I want to cancel my offer</a></p>
        </div>
      </div>
    <%}%>
  </section>
  <input type="hidden" id="yoeId" value="<%=YOE_1%>"/>
  <input type="hidden" value="N" id="marketingEmailRegFlag"/>
  <input type="hidden" value="N" id="solusEmailRegFlag"/>
  <input type="hidden" value="N" id="surveyEmailRegFlag"/>
  <input type="hidden" id="referrerURL_GA" value="<%=REFERER_URL%>"/>
  <input type="hidden" id="regLoggingType" value="whatuni-go"/>
  <input type="hidden" id="submitType" value="">
  <input type="hidden" id="offerName" value="APPLICATION_TIMER"/>
  <input type="hidden" id="userStatusCode" value="<%=userStatusCode%>"/>
  <input type="hidden" id="hotLine" value="<%=hotLine%>"/>
  <input type="hidden" id="collegeDisplayName" value="<%=collegeDisplayName%>"/>
  <input type="hidden" id="collegeDispName" value="<%=collegeDisplayName%>"/>
<%}%>
          
        