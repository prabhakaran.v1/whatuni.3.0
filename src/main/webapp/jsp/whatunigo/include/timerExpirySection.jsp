<%@page import= "WUI.utilities.CommonUtil"%>
<div class="rvbx_shdw"></div>
  <div class="lbxm_wrp">
    <div class="ext-cont svap_cnt coelbox">
      <div class="revcls ext-cls"><a href="javascript:void(0);" onclick="gotoBackSearch();"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"></a></div>
      <div class="rev_lst ext">
        <div>
          <h4>You've run out of time</h4>
          <p>Your place on this course has now been released to other students. You'll need to start your application again if you want to apply for this course.</p>
        </div>
        <div class="btn_cnt w-56p">
          <div class="btnsec_row fl_w100">
            <a href="javascript:void(0)" onclick="gotoBackSearch();" class="fr bck">Back to course</a>
          </div>  
        </div>
      </div>
    </div>
  </div>