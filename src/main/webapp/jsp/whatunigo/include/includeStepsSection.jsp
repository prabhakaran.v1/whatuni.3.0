<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="org.apache.commons.validator.GenericValidator"%>
<%String pageNo = (String)request.getAttribute("pageNo");%>
<%
  String pageSts = "";
  String formStatusId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("pageStatus"))) ? (String)request.getAttribute("pageStatus") : "";
  String allFormStatusId = "Y";
%>
<!--

stp_nomrk - orange

orange with grey - stp_pro - #FF8B00

green - stp_cmplt - #00B26D

green with grey border- stp_saved - #00B26D
-->
<c:if test="${not empty requestScope.pageInfoList}">
    <div class="brc_row2">
      <ul id="pageNavULId">
      <c:forEach var="pageInfoList" items="${requestScope.pageInfoList}">
        <c:set var="pageId" value="${pageInfoList.page_id}"/>
        <c:set var="status" value="${pageInfoList.status}"/>
        <c:set var="currentPage" value="${pageInfoList.current_page_flag}"/>
        <%
          pageSts = "";      
          if("I".equalsIgnoreCase((String)pageContext.getAttribute("status"))){
            pageSts = ("Y".equalsIgnoreCase((String)pageContext.getAttribute("currentPage"))) ? "stp_pro" : "stp_nomrk";
            allFormStatusId = "I";
          }else if("C".equalsIgnoreCase((String)pageContext.getAttribute("status"))){
            pageSts = ("Y".equalsIgnoreCase((String)pageContext.getAttribute("currentPage"))) ? "stp_saved" : "stp_cmplt";
          }
          if("Y".equalsIgnoreCase((String)pageContext.getAttribute("currentPage"))){
        %>
        
        <%}%>
        
        <%
           String pageId = pageContext.getAttribute("pageId").toString(); 
           String status = pageContext.getAttribute("status").toString(); 
        %>
        <li class="<%=pageSts%>" id="pageNav_<%=pageId%>">
          <a onclick="navigation('<%=pageNo%>', '${pageInfoList.page_id}', 'top-nav');">
          <span id="step_${pageInfoList.page_id}" class="bcno">${pageInfoList.page_id}</span>
          </a>
          <input type="hidden" id="form_${pageInfoList.page_id}" value="<%=status%>"/>
          <input type="hidden" id="form_${pageInfoList.page_id}_rev" value="${pageInfoList.reviewed_flag}"/>
        </li>
        
      </c:forEach>
      </ul>
    </div>
    <input type="hidden" id="currentPageId" value="<%=pageNo%>"/>
    <input type="hidden" id="formStatusId" value="<%=formStatusId%>"/>    
    <input type="hidden" id="allFormStatusId" value="<%=allFormStatusId%>"/>
</c:if>
<script>markAsComplete('<%=formStatusId%>', 'onload')</script> 