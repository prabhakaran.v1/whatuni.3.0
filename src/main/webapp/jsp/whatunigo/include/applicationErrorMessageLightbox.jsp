<%@page import= "WUI.utilities.CommonUtil"%>
<%String srText = request.getParameter("srText");%>
<div class="rvbx_shdw"></div>
<div class="lbxm_wrp">  
  <div class="ext-cont svap_cnt cedwo_cnt clrdef_cnt appl_redir">
    <div class="revcls ext-cls"> <% if(!"SR_MESSAGE".equalsIgnoreCase(srText)){%><a href="javascript:void(0);" onclick="closeLigBox();"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"> </a><%}%></div>
    <div class="rev_lst ext">
      <% if("SR_MESSAGE".equalsIgnoreCase(srText)){%>
        <div class="cedwo_wrap">
          <div class="ced_wgo"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/wugo_logo.svg" title="Whatuni Go logo" alt="Whatuni GO Logo"></div>
          <div class="ced_desc">
            <p>Here is a list of all GO Apply courses. If the course shows the Go Apply badge then you have met the entry criteria and can apply directly. </p>
            <p>If you see any courses without the GO Apply button, then you have unfortunately not met the exact entry criteria to apply, but we urge you to give the university a call anyway if this is your chosen course.</p>
          </div> 
        </div>
        <div class="btn_cnt insuf">
          <a href="javascript:void(0);" onclick="multicheckApplyFn('goApplyFlag');" class="fl bton">Close</a>
        </div>
      <%} else {%>
        <div class="cedwo_wrap">
          <div class="ced_wgo"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/wugo_logo.svg" title="Whatuni Go logo" alt="Whatuni GO Logo"></div>
          <div class="ced_desc">
            <p>You haven't started any applications yet</p>
            <p>Come back to this page once you have begun an application for your chosen course</p>
          </div> 
        </div>
        <div class="btn_cnt insuf">
          <a href="javascript:void(0);" onclick="closeLigBox();" class="fl bton">Close</a>
        </div>
      <%}%>
    </div>
  </div>
</div>