<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="org.apache.commons.validator.GenericValidator"%>  
<jsp:scriptlet>String readOnly = "";String divCls = "";String checkedFlag = "checked='checked'";
String formStatusId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("pageStatus"))) ? (String)request.getAttribute("pageStatus") : "";
</jsp:scriptlet>
<section class="cmm_cnt">
    <div class="brd_crumb timer_sec">
          <div class="cmm_col_12">
            <div class="cmm_wrap">
                <div class="brc_cnt lft fl">
                  <div class="brc_row1">
                    <span class="bc_hdtxt"><c:if test="${not empty requestScope.pageName}"> ${requestScope.pageName}</c:if> </span><span class="bc_stpno">Step <c:if test="${not empty requestScope.pageNo}"> ${requestScope.pageNo}</c:if> of 4</span>
                  </div>
                  <jsp:include page="/jsp/whatunigo/include/includeStepsSection.jsp"/>
                </div>
                <jsp:include page="/jsp/whatunigo/include/timer.jsp"></jsp:include>  
            </div>
      </div>
    </div>
    <div class="cmm_col_12 binf_cnt">
      <div class="cmm_wrap">
        <div class="cmm_row">
  
         <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/>
          <c:if test="${not empty requestScope.questionAnswerList}">
        <form class="fl basic_inf pers_det cont_det">
        
        
          <c:forEach var="questionAnswerList" items="${requestScope.questionAnswerList}">
          <c:set var="questionId" value="${questionAnswerList.page_question_id}"/>
          <%String questionId = pageContext.getAttribute("questionId").toString(); %>
          <c:set var="format" value="${questionAnswerList.format_desc}"/>
          <jsp:scriptlet>
          divCls = "";
          if("21".equals(questionId)){
            divCls = "fnsec";
          }else if("22".equals(questionId)){
            divCls = "lnsec";
          }else if("20".equals(questionId)){
            divCls = "addrln2";
          }</jsp:scriptlet>
          <c:if test="${questionAnswerList.format_desc eq 'TEXT'}">
          <div id="div_${questionAnswerList.page_question_id}" class="pers_lst pt-20 <%=divCls%>">
            <fieldset class="tx-bx w-100">
            <jsp:scriptlet>readOnly = "";</jsp:scriptlet>
          <c:if test="${questionAnswerList.page_question_id eq '16'}">
            <jsp:scriptlet>readOnly = "readonly";</jsp:scriptlet>
             </c:if>
            <input type="text" id="TEXT_${questionAnswerList.page_question_id}" name="TEXT_${questionAnswerList.page_question_id}" value="${questionAnswerList.answer_value}" onblur="validateFormData(this.id, 'TEXT');contactFormSubmit('', 'uncheck');" onkeyup="contactFormSubmit('', 'uncheck');enOrDisTopNav();" maxlength="${questionAnswerList.max_length}" class="frm-ele" <%=readOnly%>/>
              <label for="TEXT_${questionAnswerList.page_question_id}" class="lbco">${questionAnswerList.question_title}<c:if test="${questionAnswerList.mandatory_flag eq 'Y'}">*</c:if></label>
              <p class="fail-msg" id="TEXT_${questionAnswerList.page_question_id}_Err" style="display:none;"></p>
              <p class="su-msg" id="TEXT_${questionAnswerList.page_question_id}_Sus" style="display:none;"></p>
          </fieldset>
          </div>
          </c:if>
          
          <c:if test="${questionAnswerList.page_question_id eq '17'}">
          <div id="div_${questionAnswerList.page_question_id}" class="pers_lst pnum_cnt pt-20">
        
            <fieldset class="fl tx-bx">
              <input type="text" id="PHONE_${questionAnswerList.page_question_id}" name="PHONE_${questionAnswerList.page_question_id}" value="${questionAnswerList.answer_value}" maxlength="${questionAnswerList.max_length}" class="frm-ele" onblur="validateFormData(this.id, 'TEXT');contactFormSubmit('', 'uncheck');" onkeyup="contactFormSubmit('', 'uncheck');enOrDisTopNav();">
              <label for="PHONE_${questionAnswerList.page_question_id}" class="lbco">${questionAnswerList.question_title}<c:if test="${questionAnswerList.mandatory_flag eq 'Y'}">*</c:if></label>
              <p class="fail-msg" id="PHONE_${questionAnswerList.page_question_id}_Err" style="display:none;"></p>
            </fieldset>
        
            <fieldset class="fr tx-bx mob-mb0">
               <div class="mob_cnt fl" id="div_${questionAnswerList.page_question_id}">
                <div class="ucas_selcnt">
                    <fieldset class="ucas_sbox">
                      <span class="sbox_txt" id="select_${questionAnswerList.page_question_id}_span">Please Choose</span>
                      <i class="fa fa-angle-down fa-lg"></i>
                    </fieldset>
                    <select name="DROPDOWN_${questionAnswerList.page_question_id}" id="DROPDOWN_${questionAnswerList.page_question_id}" class="ucas_slist" onchange="selectChange('div_${questionAnswerList.page_question_id}', '${questionAnswerList.page_question_id}');validateFormData(this.id, 'DROPDOWN');contactFormSubmit('', 'uncheck');enOrDisTopNav();">
                      <option value="">Phone type</option>
                      <c:forEach var="qusOptions" items="${questionAnswerList.question_options}" >
                        <c:set var="selectId" value="${qusOptions.option_id}"/>
                   <jsp:scriptlet>checkedFlag = "";</jsp:scriptlet>
                   <c:if test="${questionAnswerList.answer_option_id eq selectId}">
                     <jsp:scriptlet>checkedFlag = "selected";</jsp:scriptlet>
                   </c:if>
                        <option value="${qusOptions.option_id}" <%=checkedFlag%>>${qusOptions.option_desc}</option>
                      </c:forEach>
                    </select>
                </div>
                <p class="fail-msg" id="DROPDOWN_${questionAnswerList.page_question_id}_Err" style="display:none;"></p>
               </div>
            </fieldset>
            
          </div>
          </c:if>
          
          <c:if test="${questionAnswerList.page_question_id eq '18'}">
          <div id="div_${questionAnswerList.page_question_id}" class="pers_lst cof_cnt pt-30">
            <fieldset class="fl tx-bx mob-mb0">
              <h5 class="cmn-tit txt-upr">${questionAnswerList.question_title}</h5>
              <div class="gen_cnt fl">
              <div class="ucas_selcnt">
                  <fieldset class="ucas_sbox">
                    <span class="sbox_txt" id="select_${questionAnswerList.page_question_id}_span">Select</span>
                    <i class="fa fa-angle-down fa-lg"></i>
                  </fieldset>
                  <select name="DROPDOWN_${questionAnswerList.page_question_id}" id="DROPDOWN_${questionAnswerList.page_question_id}" class="ucas_slist" onchange="selectChange('div_${questionAnswerList.page_question_id}', '${questionAnswerList.page_question_id}');validateFormData(this.id, 'DROPDOWN');contactFormSubmit('', 'uncheck');enOrDisTopNav();changeDefaultVal('div_${questionAnswerList.page_question_id}');">
                    <option value="">Select</option>
                    <c:forEach var="qusOptions" items="${questionAnswerList.question_options}" varStatus="i">
                      <c:set var="indexIValue" value="${i.index}"/>
                      <c:set var="selectid" value="${qusOptions.option_id}"/>
                   <jsp:scriptlet>checkedFlag = "";</jsp:scriptlet>
                   <c:if test="${questionAnswerList.answer_option_id eq selectid}">
                     <jsp:scriptlet>checkedFlag = "selected";</jsp:scriptlet>
                   </c:if>               
                        <option value="${qusOptions.option_id}" <%=checkedFlag%>>${qusOptions.option_desc}</option>
                       <%if(Integer.parseInt(pageContext.getAttribute("indexIValue").toString()) == 4 ){%>
                        <optgroup label="-----------------------------------"></optgroup>
                       <%}%>
                      </c:forEach>
                  </select>
              </div>
              <p class="fail-msg" id="DROPDOWN_${questionAnswerList.page_question_id}_Err" style="display:none;"></p>
              </div>
            </fieldset>
            <%--
            <fieldset class="fr tx-bx psc_cnt">
             <h5 class="cmn-tit txt-upr">POSTCODE FINDER</h5>
              <div class="fl w-100">              
              <input type="text" name="Enter Postcode" class="frm-ele">
              <label class="lbco">Enter postcode</label>
             </div>
              <div class="fr blu-bg"><span class="go_ic"><i class="fa fa-search"></i></span></div>
            </fieldset>--%>

          </div>  
          </c:if>
           
</c:forEach>
<script>
  selectChange('div_17', '17');
  selectChange('div_18', '18');
  //To validate the mark as complete button
  contactFormSubmit('', 'uncheck', 'onload');
  //
</script>
<input type="hidden" id="qusArr" value=""/>

          <div class="btn_cnt pt-40">
            <div class="fr">
								<div class="btn_cmps fl">
									<div class="reltv fl">
										<label class="cnfrm fl">            									  
									  <%if("C".equalsIgnoreCase(formStatusId)){%>
									    <input id="markAsComFlagId" onclick="contactFormSubmit('MARK_AS_COMPLETE');focusErrFld();" type="checkbox" disabled="disabled" checked>
									  <%}else{%>
                      <input id="markAsComFlagId" onclick="contactFormSubmit('MARK_AS_COMPLETE');focusErrFld();" type="checkbox" >
                    <%}%>
                    <span class="checkmark grn"></span> <span class="bn bn-grn grn_txt"><span class="macomp">Mark As Complete</span><span class="frmcomp">Completed</span></span> 
									 </label>
								  </div>
								</div>
              <a href="javascript:void(0)" class="fr bton" onclick="contactFormSubmit('contactFormSubmit');focusErrFld();">SAVE AND CONTINUE <i class="fa fa-long-arrow-right"></i></a>
            </div>
         <jsp:include page="/jsp/whatunigo/include/includeBackLink.jsp"/>
          </div>

    </form>
       </c:if>
         </div>
    </div>
    </div>
</section>