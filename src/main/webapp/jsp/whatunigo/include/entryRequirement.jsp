<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="org.apache.commons.validator.GenericValidator"%>
<%int count = 0, subCount = 0;
  String pageNo = (String)request.getAttribute("pageNo");
  String qualSub = "";
  String qualId = "";
  String offerName = request.getParameter("offerName");
  String action = "";
  if("PROVISIONAL_OFFER".equalsIgnoreCase(offerName)){
    action = "PROV_QUAL_EDIT";  
  }
  String formStatusId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("pageStatus"))) ? (String)request.getAttribute("pageStatus") : "";
  String qualifications = "";
  String userGrade = "";
  String userSubject = "";
  String userSubGrd = "";
  String userTariff = (String)request.getAttribute("userTariff");
  String callTypeFlag = "";
  String qualTCCheckbox = (String)request.getAttribute("qualTCCheckbox");
  %>
<section class="cmm_cnt">
  <%if(!"PROVISIONAL_OFFER".equalsIgnoreCase(offerName)){%>
    <div class="brd_crumb timer_sec">
      <div class="cmm_col_12">
        <div class="cmm_wrap">
          <div class="brc_cnt lft fl">          
            <div class="brc_row1">
              <span class="bc_hdtxt"><c:if test="${not empty requestScope.pageName}"> <c:out value="${requestScope.pageName}" /></c:if> </span><span class="bc_stpno">Step <c:if  test="${not empty requestScope.pageNo}"> <c:out value="${requestScope.pageNo}" /></c:if> of 4</span>
            </div>          
            <jsp:include page="/jsp/whatunigo/include/includeStepsSection.jsp"/>
          </div>
           <jsp:include page="/jsp/whatunigo/include/timer.jsp" />
        </div>
      </div>
    </div>
  <%}%>
  <div class="cmm_col_12 binf_cnt">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <div class="fl ots_cnt pb-40">
          <h1 class="pln-sub-tit">Review your qualifications</h1>
          <p class="cnfrm f-16">Check to see if your qualifications are correct</p>
        </div>
        <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/>
        <c:if test="${not empty requestScope.userQualList}">
          <form class="fl basic_inf pers_det cont_det qualif qualedit">
            <div class="ucas_midcnt">
              <div id="ucas_calc">
                <div class="ucas_mid">
                <c:forEach var="userQualList" items="${requestScope.userQualList}" varStatus="row">
                  <c:set var="index" value="${row.index}" />
                  <%count = Integer.parseInt(pageContext.getAttribute("index").toString());%>
                  <div class="add_qualif">
                    <div class="ucas_refld">
                      <div class="ucas_row">
                        <c:set var="qual_check" value="${userQualList.entry_qual_id}" />
                         <h3 class="un-tit">Level <c:if test="${not empty userQualList.entry_qual_level}"> ${userQualList.entry_qual_level}</c:if><span class="qedt_md"><a onclick="editQual('<%=pageNo%>', '<%=action%>');">Edit</a></span></h3>
                       
                        <div class="qual_edt qual">
                          <h5 class="cmn-tit txt-upr">Qualifications</h5>
                          <div class="qua_fild">
                            <span>
                              <c:out value="${userQualList.entry_qual_desc}"/>
                            </span>
                          </div>
                        </div>
                        
                        <c:set var="qualid" value="${userQualList.entry_qual_id}" />
                        <c:set var="qualDesc" value="${userQualList.entry_qual_desc}" />
                        <jsp:scriptlet> String qualid = pageContext.getAttribute("qualid").toString(); qualId += qualid + ",";qualSub = "";qualifications += pageContext.getAttribute("qualDesc").toString() + ",";</jsp:scriptlet>
                        <c:if test="${not empty userQualList.qual_subject_list}">     
						
                        <div class="qual_edt sub">
                          <h5 class="cmn-tit txt-upr">Subject(s) & Grade(s)</h5>
                            <jsp:scriptlet>userSubGrd = "";</jsp:scriptlet>
                           <c:forEach var="subjectList" items="${userQualList.qual_subject_list}" varStatus="row">
                             <c:set var="subIndex" value="${row.index}" />
                             <%subCount = Integer.parseInt(pageContext.getAttribute("subIndex").toString());%>
                             <c:set var="grade" value="${subjectList.entry_grade}" />
                             <c:set var="subId" value="${subjectList.entry_subject_id}" />
                             <jsp:scriptlet>String subId = pageContext.getAttribute("subId").toString();String grade = pageContext.getAttribute("grade").toString();qualSub += subId + "##SUB##" + grade + "##GRADE##";</jsp:scriptlet>
                             <input class="subjectArr" type="hidden" id="sub_<%=qualid%>" name="sub_<%=qualid%>" value="${subId}" />
                              <input class="gradeArr" type="hidden" id="grde_<%=qualid%><%=subCount%>" name="grde_<%=qualid%>" value="${grade}" />
                              <input class="qualArr" type="hidden" id="qual_<%=qualid%><%=subCount%>" name="qual_<%=qualid%>" value="<%=qualid%>" />
                              <input class="qualSeqArr" type="hidden" id="qual_<%=qualid%><%=subCount%>" name="qual_<%=qualid%>" value="<%=count%>" />
                            <div class="qua_fild">
                              <c:set var="user_subject" value="${subjectList.entry_subject}" />
                              <c:set var="user_grade" value="${subjectList.entry_grade}" />
                              <jsp:scriptlet>String user_grade = pageContext.getAttribute("user_grade").toString();String user_subject = pageContext.getAttribute("user_subject").toString();userGrade += user_grade + "|";userSubject += user_subject + "|";userSubGrd += user_subject + "|" + user_grade + "|";</jsp:scriptlet>
                              <span><c:out value="${subjectList.entry_subject}" />, <c:out value="${subjectList.entry_grade}" />
                              </span>
                            </div>
                            </c:forEach> 
                          </div>
                         </c:if>
                        </div>
                      </div>
                    </div>
                    <input class="gaQualDesc" type="hidden" id="qual_desc" value="<%=qualifications%>" />
                    <input class="gaUserSub_<%=qualid%>" type="hidden" id="user_Sub" value="<%=userSubject%>" />
                    <input class="gaUserGrd_<%=qualid%>" type="hidden" id="user_grd" value="<%=userGrade%>" />
                    <input class="gaSubGrde" type="hidden" id="gaSubGrde_<%=qualid%>" value="<%=userSubGrd%>" />
                                       
                    <input class="qualSubj" type="hidden" id="qualSub_<%=qualid%>" value="<%=qualSub%>" />
                    
                    <input class="gaQualIds" type="hidden" id="qual_id" value="<%=qualId%>" />
                    
				  </c:forEach>
          <input class="qual" type="hidden" id="qualId" value="<%=qualId%>" />
          <input class="gaUcasPoints" type="hidden" id="gaUcasPoints" value="<%=userTariff%>" />
                </div>
				<div id="scoreDiv" class="fl cmqua mt-40 w-647">
                <c:if test="${not empty requestScope.userTariff}">
                <div class="ucas_btm ctrl_mt scr_mt0">
										<div class="ucas_row row_btm30">
											<div id="innerScoreDiv" class="scr_wrap">
												<div class="scr_lft">
                            <div class="chartbox fl w100p">
                              <h2><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Your UCAS score</font></font></h2>
                              <div class="ucspt_val">${requestScope.userTariff} points</div>  
                            </div>
												</div>
											</div>
										</div>
									</div>
                  </c:if>
                <div class="pt-25 qua_cnf">
                  <h3 class="un-tit p-0 f-20 pt-0">Confirmation</h3>
                  <p class="cnfrm f-16">Please review and make sure you have entered your score correctly. If your score is different to what you have entered then the <c:if test="${not empty requestScope.COLLEGE_DISPLAY_NAME}">${requestScope.COLLEGE_DISPLAY_NAME}</c:if> is within their rights not to make you a formal offer. <a href="javascript:void(0);" onclick="showTermsConditionPopup();">See full terms &amp; conditions here</a>.</p>
                  <div class="btn_chk">
                    <span class="chk_btn">
                    <%if("PROVISIONAL_OFFER".equalsIgnoreCase(offerName)){%>
                      <input onclick="enableMarkComplete('entryReqSecId', '', '<%=pageNo%>');" type="checkbox" name="qualTCId" value="N" id="qualTCId">
                    <%}else{
                    if("Y".equalsIgnoreCase(qualTCCheckbox)){%>
                      <input onclick="enableMarkComplete('entryReqSecId', '', '<%=pageNo%>');" type="checkbox" name="qualTCId" value="Y" id="qualTCId" checked>
                    <%}else{%>
                      <input onclick="enableMarkComplete('entryReqSecId', '', '<%=pageNo%>');" type="checkbox" name="qualTCId" value="N" id="qualTCId">
                    <%}}%>
                    <span class="checkmark grey"></span>
                    </span>
                   
                    <p>
                      <label for="qualTCId" class="chkbx_100">I confirm that all the information I have given is true and accurate, including details of my qualification information and I have achieved  <c:if test="${not empty requestScope.userTariff}">${requestScope.userTariff} UCAS points.</c:if></label>
                    </p>
                    <p class="qler1" id="qualTCId_error" style="display:none;"></p>
                    <c:if test="${not empty requestScope.userTariff}"><input type="hidden" id="oldGrade" value="${requestScope.userTariff}" /></c:if>
                  </div>
                </div>
                <div class="btn_cnt pt-40">
                  <div class="fr">                    
                    <%if("PROVISIONAL_OFFER".equalsIgnoreCase(offerName)){%>
                      <a class="fr bton" onclick="passJSONObj('PROV_QUAL_UPDATE', '5', 'N', '', 'NO_VALIDATION');">SAVE AND CONTINUE <i class="fa fa-long-arrow-right"></i></a>
                    <%}else{%>
                      <div class="btn_cmps fl">
                        <div class="reltv fl">
                          <label class="cnfrm fl">                                      
                          <%if("C".equalsIgnoreCase(formStatusId)){callTypeFlag="onload";%>
                            <input id="markAsComFlagId" onclick="saveQual('4', '4', 'C', '', 'Y');" type="checkbox" disabled="disabled" checked>
                          <%}else{%>
                            <input id="markAsComFlagId" onclick="saveQual('4', '4', 'C', '', 'Y');" type="checkbox" >
                          <%}%>
                          <span class="checkmark grn"></span> <span class="bn bn-grn grn_txt"><span class="macomp">Mark As Complete</span><span class="frmcomp">Completed</span></span> 
                          </label>
                        </div>
                      </div>
                      <a class="fr bton " id="contBtn" onclick="saveQual('4', '5', 'I', '');" >SAVE AND CONTINUE <i class="fa fa-long-arrow-right"></i></a>                      
                    <%}%>
                  </div>
                <jsp:include page="/jsp/whatunigo/include/includeBackLink.jsp"/>
                </div>
              </div>
            </div>
              </div>
              
          <input type="hidden" id="qualDesc" value="<%=qualifications%>" />
          <input type="hidden" id="oldQualDesc" value="<%=qualifications%>" />
          <input type="hidden" id="newQualDesc" value="<%=qualifications%>" />
          </form>
          
        </c:if>
        <script>enableMarkComplete('entryReqSecId','<%=callTypeFlag%>');</script>
      </div>
    </div>
  </div>
</section>
<input type="hidden" id="action" />
 <%if("PROVISIONAL_OFFER".equalsIgnoreCase(offerName)){%>
<script>
jq( document ).ready(function() {
 setTimerCookie('offer_timer','','0');
 stopinterval();
 startTimer1();
 });
</script>
<%}else{%>
<script>
jq( document ).ready(function() {
enableDisContiBtn();
 });
</script>
<%}%>