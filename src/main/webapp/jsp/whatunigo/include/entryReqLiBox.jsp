<%@page import= "WUI.utilities.CommonUtil"%>

<jsp:scriptlet> 
  String qualErrorCode = request.getParameter("qualErrorCode");
  String oldGrade = request.getParameter("oldGrade");
  String newGrade = request.getParameter("newGrade");
  String oldQualDesc = request.getParameter("oldQualDesc");
  String newQualDesc = request.getParameter("newQualDesc");
  String offerName = request.getParameter("offerName");
  String pageNo = request.getParameter("pageNo");
  String from = request.getParameter("from");
  String subCount = request.getParameter("subCount");
  String action = "";
  if("PROVISIONAL_OFFER".equalsIgnoreCase(offerName)){
    action = "PROV_QUAL_EDIT";  
  }
  String eventLabel = "";
  String clsNme = "";
  if("INTL_USER".equalsIgnoreCase(qualErrorCode)){
    clsNme = "intusr_lbx";
  }
</jsp:scriptlet>

<div class="rvbx_shdw"></div>
<div class="lbxm_wrp">
  <div class="ext-cont ins_entrq spec_need <%=clsNme%>">
    <div class="revcls ext-cls"><a href="javascript:void(0);" onclick="closeLigBox();"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"></a></div>
    <div class="rev_lst ext">
      <% if("0".equals(qualErrorCode)) { eventLabel = "|||Qualification|";%>
      <div>
        <i class="fa fa-exclamation-triangle"></i>
        <h4>Multiple Qualifications not accepted</h4>
        <p class="ssn_desc">
        <%if("NEW_USER".equalsIgnoreCase(from)){%>
          Unfortunately we are currently unable to match users with multiple qualifications e.g. A-Levels + BTECs with the entry requirements for this course.
        <%}else{%>
          Unfortunately we are currently unable to match users with multiple qualifications e.g. A-Levels + BTECs with the entry requirements for this course.
        <%}%>
         <span class="fbld">Are your new qualifications correct?</span>
        </p>
      </div>
      <div class="btn_cnt insuf">
        <div class="btnsec_row fl_w100">
          <a onclick="getAlternatePage();" class="fl bton">YES</a>
          <a onclick="callEditQual('<%=pageNo%>', '<%=action%>', '<%=from%>');" class="fr bton">NO</a>
        </div>
      </div>
      <%} else if("1".equals(qualErrorCode)) { eventLabel = "Grade||||";%>
      <div>
        <i class="fa fa-exclamation-triangle"></i>
        <h4>Insufficient entry requirements</h4>
        <p> Your tariff points doesn't meet the provider required points </p>
      </div>
      <div class="btn_cnt insuf">
        <div class="btnsec_row fl_w100">
          <a onclick="getAlternatePage();" class="fl bton"><%=newGrade%> Points</a>
        </div>  
        <a onclick="callEditQual('<%=pageNo%>', '<%=action%>', '<%=from%>');" class="bck">Not sure/ I need to enter them again</a>
      </div>
      <%} else if("2".equals(qualErrorCode)) { eventLabel = "||Subject&Grade||";%>
        <div>
           <i class="fa fa-exclamation-triangle"></i>
           <h4>Specific subject needed</h4>
           <p class="ssn_desc">
           <%if("NEW_USER".equalsIgnoreCase(from)){%>
             This course requires you to have studied a specific subject and achieve a certain grade in order to qualify.
           <%}else{%>
             This course requires you to have studied a specific subject and achieve a certain grade in order to qualify.
           <%}%>
             <span class="fbld">Are your new qualifications correct?</span>
           </p>
        </div>
        <div class="btn_cnt insuf">
          <div class="btnsec_row fl_w100">
            <a onclick="getAlternatePage();" class="fl bton">YES</a>
            <a onclick="callEditQual('<%=pageNo%>', '<%=action%>', '<%=from%>');" class="fr bton">NO</a>
          </div>  
        </div>      
      <%} else if("3".equals(qualErrorCode)) { eventLabel = "|||Qualification|";%>
        <div>
           <i class="fa fa-exclamation-triangle"></i>
           <h4>Qualification not accepted</h4>
           <p class="ssn_desc">
             <%if("NEW_USER".equalsIgnoreCase(from)){%>
               Unfortunately we are unable to match <span class="rd"><%=newQualDesc%></span> with the entry requirements of this course.
             <%}else{%>
               Unfortunately we are unable to match <span class="rd"><%=newQualDesc%></span> with the entry requirements of this course.
             <%}%>
             <span class="fbld">Are your qualifications correct?</span>
           </p>
       </div>
        <div class="btn_cnt insuf">
          <div class="btnsec_row fl_w100">
            <a onclick="getAlternatePage();" class="fl bton">YES</a>
            <a onclick="callEditQual('<%=pageNo%>', '<%=action%>', '<%=from%>');" class="fr bton">NO</a>
          </div>  
        </div> 
       <%}else if("4".equals(qualErrorCode)) { eventLabel = "||||Quantity";%>
        <div>
           <i class="fa fa-exclamation-triangle"></i>
           <h4>Entered all your qualifications?</h4>
           <p class="ssn_desc"> 
           <%if("NEW_USER".equalsIgnoreCase(from)){%>
             This course requires you to have studied a minimum of <%=subCount%> subjects to qualify. If you haven't entered all your qualifications, please do so now.
           <%}else{%>
             This course requires you to have studied a minimum of <%=subCount%> subjects to qualify. If you haven't entered all your qualifications, please do so now.
           <%}%>
           
             <span class="fbld">Are your qualifications correct?</span>
           </p>
       </div>
        <div class="btn_cnt insuf">
          <div class="btnsec_row fl_w100">
            <a onclick="getAlternatePage();" class="fl bton">YES</a>
            <a onclick="callEditQual('<%=pageNo%>', '<%=action%>', '<%=from%>');" class="fr bton">NO</a>
          </div>  
        </div> 
       <%}else if("5".equals(qualErrorCode)) { eventLabel = "|||Qualification|";%>
        <div>
           <i class="fa fa-exclamation-triangle"></i>
           <h4>Qualification not accepted</h4>
           <p class="ssn_desc">
             <%if("NEW_USER".equalsIgnoreCase(from)){%>
               Unfortunately we are unable to match <span class="rd">GCSE</span> with the entry requirements of this course.
             <%}else{%>
               Unfortunately we are unable to match <span class="rd">GCSE</span> with the entry requirements of this course.
             <%}%>
             <span class="fbld">Are your qualifications correct?</span>
           </p>
       </div>
        <div class="btn_cnt insuf">
          <div class="btnsec_row fl_w100">
            <a onclick="getAlternatePage();" class="fl bton">YES</a>
            <a onclick="callEditQual('<%=pageNo%>', '<%=action%>', '<%=from%>');" class="fr bton">NO</a>
          </div>  
        </div> 
       <%}else if("KEYWORD_ERROR".equalsIgnoreCase(qualErrorCode) || "AJAX_SEL_ERROR".equalsIgnoreCase(qualErrorCode)){%>          
          <p class="ssn_desc"><span class="rd">
          <%if("KEYWORD_ERROR".equalsIgnoreCase(qualErrorCode)){%>
          Please select a subject from the dropdown list
          <%}else if("AJAX_SEL_ERROR".equalsIgnoreCase(qualErrorCode)){%>
          Please select an option from the dropdown
          <%}%>
          </span></p>
          <div class="btn_cnt insuf">
            <div class="btnsec_row fl_w100">
              <a onclick="closeLigBox();" class="fl bton">Close</a>
            </div>  
          </div> 
       <%}else if("QUAL_VALIDATION".equalsIgnoreCase(qualErrorCode)){%>
          <p class="ssn_desc"><span class="rd">Please enter a valid level 3 qualification</span></p>
          <div class="btn_cnt insuf">
            <div class="btnsec_row fl_w100">
              <a onclick="closeLigBox();" class="fl bton">Close</a>
            </div>  
          </div> 
       <%}else if("SCIENCE_PRACT_ERROR".equalsIgnoreCase(qualErrorCode)){%>
            <p class="ssn_desc"><span class="rd">You need to add an A-Level Science qualification to accompany your Science Practical grade. Please add your A-Level science grade to continue</span></p>
            <div class="btn_cnt insuf">
            <div class="btnsec_row fl_w100">
              <a onclick="closeLigBox();" class="fl bton">Close</a>
            </div>  
          </div> 
          <%}else if("STEP_COMPLETE".equalsIgnoreCase(qualErrorCode)){%>
          <h4>Did you forget something?</h4>
          <p class="ssn_desc"><span class="rd">Please complete each stage and mark it as 'complete' to continue.</span></p>
          <div class="btn_cnt insuf">
            <div class="btnsec_row fl_w100">
              <a onclick="closeLigBox();" class="fl bton">Close</a>
            </div>  
          </div> 
       <%}else if("INTL_USER".equalsIgnoreCase(qualErrorCode)){%>
       <div>
           <i class="fa fa-exclamation-triangle"></i>
           <h4>Are your answers correct?</h4>
           <p class="ssn_desc">Please double check your answers on this page before continuing</p>
       </div>
        <div class="btn_cnt insuf">
          <div class="btnsec_row fl_w100">
            <a onclick="validateBasicInfo('1', '2', 'I', 'LBOX');" class="fl bton">Yes, they're correct</a>
            <a onclick="closeLigBox();" class="fr bton">Edit my answers</a>
          </div>  
        </div> 
       <%}%>
    </div>
  </div>
</div>
<%if(eventLabel != ""){%>
<script type="text/javascript">
  gaEventLog("Apply Now Form", "Entry Requirements Not Met", "<%=eventLabel%>");
</script>
<%}%>