<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="org.apache.commons.validator.GenericValidator" %>
<jsp:scriptlet>
  String questionIds = "";
  String checkedFlag = "checked='checked'";
  String hideFlag = "display:block;";
  String clsName = "bn-outln";
  String validateStr = "";
  //String pageId = (String)request.getAttribute("pageNo");
  String pageNum = "";
  String divCls = "";
  String fildCls = "";
  String qusId1 = "";
</jsp:scriptlet>  
   
 <c:forEach var="qusAnsList" items="${requestScope.questionAnswerList}" >
    <jsp:scriptlet>hideFlag = "display:block;";</jsp:scriptlet>
    <c:if test="${qusAnsList.default_show_flag eq 'N'}">
      <jsp:scriptlet>hideFlag = "display:none";</jsp:scriptlet>
    </c:if>
    <c:set var="questionId" value="${qusAnsList.page_question_id}"/>  
    <c:set var="format" value="${qusAnsList.format_desc}"/>  
          <c:set var="pageNo" value="${qusAnsList.page_id}"/>  
          <c:set var="qusId" value="${qusAnsList.page_question_id}"/>  
             <jsp:scriptlet>
              pageNum = pageContext.getAttribute("pageNo").toString();divCls = "";fildCls = "";
              qusId1 = pageContext.getAttribute("qusId").toString();
      if("9".equals(qusId1)){
        fildCls = "fl tx-bx mob-mb0";
      }else if("10".equals(qusId1)){
        divCls = "pt-20";
      fildCls = "fl tx-bx";
      }else if("11".equals(qusId1)){
        divCls = "pt-20 lnsec";
      fildCls = "fr tx-bx";
      }else if("12".equals(qusId1)){
        divCls = "";
      fildCls = "tx-bx w-100";
      }else if("13".equals(qusId1)){
        divCls = "pt-40 dofb";
      fildCls = "dob_cnt fl";
      }else if("14".equals(qusId1)){
      divCls = "pt-30 gend";
      fildCls = "gen_cnt fl";
    }else if("15".equals(qusId1)){
      divCls = "pt-30 uid";
      fildCls = "tx-bx w-100 mob-mb-10";
    }else if("16".equals(qusId1)){
      divCls = "pt-20";
      fildCls = "tx-bx w-100";
    }else if("18".equals(qusId1)){
      divCls = "cof_cnt pt-30";
      fildCls = "fl tx-bx mob-mb0";
    }else if("17".equals(qusId1)){
      divCls = "pnum_cnt pt-20";
      fildCls = "fr tx-bx mob-mb0";
    }else if("19".equals(qusId1)){
      divCls = "pt-20";
      fildCls = "tx-bx w-100";
    }else if("20".equals(qusId1)){
      divCls = "pt-20";
      fildCls = "tx-bx w-100";
    }else if("21".equals(qusId1)){
      divCls = "pt-20";
      fildCls = "fl tx-bx";
    }else if("22".equals(qusId1)){
      divCls = "pt-20";
      fildCls = "fl tx-bx";
    }
</jsp:scriptlet>
<c:if test="${qusAnsList.page_id eq '1'}">
  <jsp:scriptlet>hideFlag = "display:block;";</jsp:scriptlet>
  <c:if test="${qusAnsList.default_show_flag eq 'N'}">
      <jsp:scriptlet>hideFlag = "display:none";</jsp:scriptlet>
  </c:if>
              <div class="abdy_ fl_w100" style="<%=hideFlag%>" id="qus_${qusAnsList.page_question_id}">
              <div class="revap_rw fl_w100">
                <h5 class="cmn-tit" id="tit_${qusAnsList.page_question_id}"><span class="cmn-tit1">${qusAnsList.question_title}<c:if test="${qusAnsList.mandatory_flag eq 'Y'}">*</c:if></span> <span class="qedt_md"><a href="javascript:void(0)" onclick="editSec('${qusAnsList.page_question_id}');">Edit</a></span></h5>
                <h6 class="cmn-ansr" id="head_${qusAnsList.page_question_id}">${qusAnsList.answer_value}</h6>
                </div>
                 <div class="biedt_sec" id="sect_${qusAnsList.page_question_id }" style="display:none;">
                  <div class="bas_lst pt-40">
                    <h5 class="cmn-tit">${qusAnsList.question_title }<c:if test="${qusAnsList.mandatory_flag eq 'Y'}">*</c:if></h5>
                    <div class="blst_rw" id="div_${qusAnsList.page_question_id }" style="<%=hideFlag%>">
                    <c:if test="${qusAnsList.format_desc eq 'RADIO'}">
                      <c:forEach var="qusOptions" items="${qusAnsList.question_options}">
                        <c:set var="questionId1" value="${qusAnsList.page_question_id}"/>  
                               <c:set var="format1" value="${qusAnsList.format_desc}"/>  
                                <c:set var="optionId1" value="${qusOptions.option_id}"/>  
                                <c:set var="optionId1" value="${qusOptions.option_id}"/>
                                 <%String  optionId1 = pageContext.getAttribute("optionId1").toString();
                                   String  questionId1 = pageContext.getAttribute("questionId1").toString();
                                 %>     
                                  <c:set var="optionId1_1" value="<%=optionId1%>"/>
                                  
                                <jsp:scriptlet>
                         checkedFlag = ""; clsName = "";
                       </jsp:scriptlet>
                       <c:if test="${qusAnsList.answer_option_id eq optionId1_1}">
                         <jsp:scriptlet>checkedFlag = "checked='checked'"; clsName = " bn-gry active";</jsp:scriptlet>
                       </c:if>
                        
                        <a class="opt_<%=questionId1%> bn i-bn bn-outln <%=clsName%>" id="<%=optionId1%>" name="<%=questionId1%>" onclick="changeRadio(this.id, '<%=questionId1%>');changeBtn('<%=questionId1%>', 'REVIEW_PAGE');" ${qusOptions.option_desc } style="<%=hideFlag%>">${qusOptions.option_desc}</a>
                      </c:forEach>
                      <input type="hidden" id="hid_${qusAnsList.page_question_id}" class="${qusAnsList.answer_option_id}" value="${qusAnsList.answer_value}"/>
                    </c:if>
                    
                    <c:if test="${qusAnsList.format_desc eq 'DROPDOWN'}">
                       <div class="ucas_selcnt w-300" >
                         <div class="ucas_sbox">
                            <span class="sbox_txt" id="select_${qusAnsList.page_question_id}_span">Please Choose</span>
                            <i class="fa fa-angle-down fa-lg"></i>
                         </div>
                        <select name="${qusAnsList.page_question_id}" id="${qusAnsList.format_desc }_${qusAnsList.page_question_id }" class="ucas_slist" onchange="selectChange('div_${qusAnsList.page_question_id }', '${qusAnsList.page_question_id }');">
                          <option>Please Choose</option>
                          <c:forEach var="qusOptions" items="${qusAnsList.question_options}" >
                             <c:set var="selectId1" value="${qusOptions.option_id}"/>  
                             <%String selectId1 = pageContext.getAttribute("selectId1").toString(); %>
                             <c:set var="selectId1_1" value="<%=selectId1 %>"/>  
                             <jsp:scriptlet>checkedFlag = "";</jsp:scriptlet>
                             <c:if test="${selectId1_1.answer_option_id eq selectId1_1}">
                               <jsp:scriptlet>checkedFlag = "selected";</jsp:scriptlet>
                             </c:if>
                             <option value="${qusOptions.option_id}" <%=checkedFlag%>>${qusOptions.option_desc}</option>
                          </c:forEach>
                        </select>
                        <p class="fail-msg" id="${qusAnsList.format_desc }_${qusAnsList.page_question_id }_Err" style="display:none;"></p>
                      </div>
                    </c:if>
                    
              </div>       
                    <p class="fail-msg" id="${qusAnsList.format_desc }_${qusAnsList.page_question_id }_Err" style="display:none;"></p>
                  </div>
                  <div class="qedt_md"><a href="javascript:void(0)" onclick="saveChange('${qusAnsList.page_question_id }', '${qusAnsList.format_desc }', '<%=pageNum%>');">Confirm changes</a></div>
                </div>
              </div>
               
      
           <%--   
             <div class="btn_cnt">
                <div class="fl">
                  <div class="btn_cmps fl">
                    <div class="reltv fl">
                      <label class="cnfrm fl">            
                      <input type="checkbox" id="secRev_form_<bean:write name="sectionList" property="page_id"/>" data-id="secRevBtn" onclick="valUserRevTCBtn('<bean:write name="sectionList" property="page_id"/>');">
                      <span class="checkmark grn blu"></span> <span class="bn bn-grn grn_txt bn-blue"><span class="macomp">Section Reviewed</span><span class="frmcomp">Section Reviewed</span></span> 
                     </label>
                    </div>
                  </div>
                </div>
						  </div>
              
       
       --%>
             
</c:if>

<c:if test="${qusAnsList.page_id ne '1'}">
   <%String questionId_1 = pageContext.getAttribute("questionId").toString();%>
   <c:set var="questionId_1" value="<%=questionId_1%>"/>

              <div class="abdy_ fl_w100" id="qus_${qusAnsList.page_question_id }">
                <div class="revap_rw fl_w100">
                <h5 class="cmn-tit" id="tit_${qusAnsList.page_question_id }"><span class="cmn-tit1">${qusAnsList.question_title }<c:if test="${qusAnsList.mandatory_flag eq 'Y'}">*</c:if></span> <span class="qedt_md">
                <c:if test="${qusAnsList.page_question_id ne '16'}">
                  <a href="javascript:void(0)" onclick="editSec('${qusAnsList.page_question_id}');">Edit</a>
                </c:if>
                </span></h5>
                <h6 class="cmn-ansr" id="head_${qusAnsList.page_question_id}">${qusAnsList.answer_value}</h6>
                </div>
                
                <div class="biedt_sec" id="sect_${qusAnsList.page_question_id}" style="display:none;">
                <div class="pers_lst <%=divCls%>">
                    <h5 class="cmn-tit">${qusAnsList.question_title}<c:if test="${qusAnsList.mandatory_flag eq 'Y'}">*</c:if></h5>
                    
                    <c:if test="${qusAnsList.format_desc eq 'TEXT'}">
                    
                      <fieldset class="<%=fildCls%>">
                       <input type="text" id="TEXT_${qusAnsList.page_question_id }" name="TEXT_${qusAnsList.page_question_id}" value="${qusAnsList.answer_value}" class="frm-ele" onblur="validateFormData(this.id, 'TEXT');" maxlength="${qusAnsList.max_length}">
                        <label for="TEXT_${qusAnsList.page_question_id }" class="lbco top_lb">${qusAnsList.question_title }<c:if test="${qusAnsList.mandatory_flag eq 'Y'}">*</c:if></label>
                        <p class="fail-msg" id="TEXT_${qusAnsList.page_question_id }_Err" style="display:none;"></p>
                        <p class="su-msg" id="TEXT_${qusAnsList.page_question_id }>_Sus" style="display:none;"></p>
                      </fieldset>
                 
                
                    </c:if>
                    <c:if test="${qusAnsList.format_desc eq 'DATE'}">
                    <div class="<%=fildCls%>">
                      <div class="ucas_selcnt fl" id="div_DD">
                        <fieldset class="ucas_sbox">
                          <span class="sbox_txt" id="select_DD_span">DD</span>
                          <i class="fa fa-angle-down fa-lg"></i>
                        </fieldset>
                        <select name="DATE_DD" id="DATE_DD" class="ucas_slist" onchange="selectChange('div_DD', 'DD'); validateFormData(this.id, 'DROPDOWN');">
                          <option value="">DD</option>
                          <c:forEach var="dateList" items="${requestScope.dateList}">
                            <option value="${dateList.value }">${dateList.label} </option>
                          </c:forEach>
                        </select>
                    </div>
          
                    <div class="ucas_selcnt fl" id="div_MM">
                        <fieldset class="ucas_sbox">
                          <span class="sbox_txt" id="select_MM_span">MM</span>
                          <i class="fa fa-angle-down fa-lg"></i>
                        </fieldset>
                        <select name="DATE_MM" id="DATE_MM" class="ucas_slist" onchange="selectChange('div_MM', 'MM'); validateFormData(this.id, 'DROPDOWN');">
                          <option value="">MM</option>
                          <c:forEach var="monthList" items="${requestScope.monthList}">
                            <option value="${monthList.value }">${monthList.label } </option>
                          </c:forEach>
                        </select>
                    </div>
                    
                    <div class="ucas_selcnt fl" id="div_YYYY">
                        <fieldset class="ucas_sbox">
                          <span class="sbox_txt" id="select_YYYY_span">YYYY</span>
                          <i class="fa fa-angle-down fa-lg"></i>
                        </fieldset>
                        <select name="DATE_YYYY" id="DATE_YYYY" class="ucas_slist" onchange="selectChange('div_YYYY', 'YYYY'); validateFormData(this.id, 'DROPDOWN');">
                         <option value="">YYYY</option>
                         <c:forEach var="yearList" items="${requestScope.yearList}">
                            <option value="${yearList.value }">${yearList.label } </option>
                          </c:forEach>
                        </select>
                    </div>
           
            </div>
             <p class="fail-msg" id="DATE_Err" style="display:none;"></p>
            <input type="hidden" name="DOD" id="DOB" value="${qusAnsList.answer_value}" />
                    </c:if>
                    
                    <c:if test="${qusAnsList.format_desc eq 'RADIO'}">
                      
                      <c:forEach var="qusOptions" items="${qusAnsList.question_options}" >
                        <c:set var="optionId" value="${qusOptions.option_id}"/>  
                        <%String optionId = pageContext.getAttribute("optionId").toString(); %>
                        <c:set var="optionId_1" value="<%=optionId%>"/>
                        
                        <jsp:scriptlet>
                         checkedFlag = ""; clsName = "bn-outln";
                       </jsp:scriptlet>
                       <c:if test="${qusAnsList.answer_option_id eq optionId_1}">
                         <jsp:scriptlet>checkedFlag = "checked='checked'"; clsName = "bn-gry";</jsp:scriptlet>
                       </c:if>
                        
                        <a class="opt_<%=questionId_1%> bn i-bn bn-outln <%=clsName%>" id="<%=optionId%>" name="<%=questionId_1%>" onclick="changeRadio(this.id, '<%=questionId_1%>');changeBtn('<%=questionId_1%>', 'REVIEW_PAGE');" ${qusOptions.option_desc } style="<%=hideFlag%>">${qusOptions.option_desc }</a>
                      </c:forEach>
                      <input type="hidden" id="hid_${qusAnsList.page_question_id}" class="${qusAnsList.answer_option_id }" value="${qusAnsList.answer_value }"/>
                    </c:if>
                   
                    
                    <c:if test="${qusAnsList.format_desc eq 'DROPDOWN'}"> 
                    
                    <c:if test="${qusAnsList.page_question_id eq '17'}">
                      <fieldset class="fl tx-bx">
                      <input type="text" id="PHONE_${qusAnsList.page_question_id }" maxlength="${qusAnsList.max_length }" name="PHONE_${qusAnsList.page_question_id}" value="${qusAnsList.answer_value}" class="frm-ele" onblur="validateFormData(this.id, 'TEXT');">
                        <label for="PHONE_${qusAnsList.page_question_id }" class="lbco top_lb">${qusAnsList.question_title }<c:if test="${qusAnsList.mandatory_flag eq 'Y'}">*</c:if></label>
                      <p class="fail-msg fl_w100" id="PHONE_${qusAnsList.page_question_id}_Err" style="display:none;"></p>
                      </fieldset>
                    
                       <fieldset class="fr tx-bx mob-mb0">
                           <div class="mob_cnt fl">
                            <div class="ucas_selcnt" id="div_${qusAnsList.page_question_id }" style="<%=hideFlag%>">
                                <fieldset class="ucas_sbox">
                                  <span class="sbox_txt" id="select_${qusAnsList.page_question_id }_span">Phone type</span>
                                  <i class="fa fa-angle-down fa-lg"></i>
                                </fieldset>
                                <select name="DROPDOWN_${qusAnsList.page_question_id }" id="DROPDOWN_${qusAnsList.page_question_id}" class="ucas_slist" onchange="selectChange('div_${qusAnsList.page_question_id }', '${qusAnsList.page_question_id }');">
                                <option>Phone type</option>
                                <c:forEach var="qusOptions" items="${qusAnsList.question_options}">
                                   <c:set var="selctId" value="${qusOptions.option_id}"/>  
                                   <%String selctId = pageContext.getAttribute("selctId").toString(); %>
                                   <c:set var="selctId_1" value="<%=selctId%>"/>
                                   <jsp:scriptlet>checkedFlag = "";</jsp:scriptlet>
                                   <c:if test="${qusAnsList.answer_option_id eq selctId_1}">
                                     <jsp:scriptlet>checkedFlag = "selected";</jsp:scriptlet>
                                   </c:if>
                                   <option value="${qusOptions.option_id }" <%=checkedFlag%>>${qusOptions.option_desc }</option>
                                </c:forEach>
                              </select>
                                <p class="fail-msg fl_w100" id="DROPDOWN_${qusAnsList.page_question_id}_Err" style="display:none;"></p>
                            </div>
                           </div>
                        </fieldset>
                    </c:if>
                    <c:if test="${qusAnsList.page_question_id ne '17'}">
                    <div class="<%=fildCls%>">
                       <div class="ucas_selcnt" id="div_${qusAnsList.page_question_id}" style="<%=hideFlag%>">
                         <div class="ucas_sbox">
                            <span class="sbox_txt" id="select_${qusAnsList.page_question_id}_span">Please Choose</span>
                            <i class="fa fa-angle-down fa-lg"></i>
                          </div>
                        <select name="${qusAnsList.page_question_id}" id="${qusAnsList.format_desc}_${qusAnsList.page_question_id}" class="ucas_slist" onchange="selectChange('div_${qusAnsList.page_question_id}', '${qusAnsList.page_question_id }');">
                          <option>Please Choose</option>
                          <c:forEach var="qusOptions" items="${qusAnsList.question_options}" >
                            <c:set var="selectId" value="${qusOptions.option_id}"/>  
                            <%String selectId =  pageContext.getAttribute("selectId").toString();%>
                            <c:set var="selectId_1" value="<%=selectId%>"/>
                             <jsp:scriptlet>checkedFlag = "";</jsp:scriptlet>
                             <c:if test="${qusAnsList.answer_option_id eq selectId_1}">
                               <jsp:scriptlet>checkedFlag = "selected";</jsp:scriptlet>
                             </c:if>
                             <option value="${qusOptions.option_id }" <%=checkedFlag%>>${qusOptions.option_desc }</option>
                          </c:forEach>
                        </select>
                        <p class="fail-msg fl_w100" id="DROPDOWN_${qusAnsList.page_question_id }_Err" style="display:none;"></p>
                      </div>
                      </div>
                     </c:if>
               </c:if>
        </div>              
                    
                    
                 
                  <div class="qedt_md fl_w100"><a href="javascript:void(0)" onclick="saveChange('${qusAnsList.page_question_id}', '${qusAnsList.format_desc }', '<%=pageNum%>');">Confirm changes</a></div>
                </div>
              </div>
           </c:if>

              </c:forEach>
              <div class="btn_cnt">
                <div class="fl">
                  <div class="btn_cmps fl">
                    <div class="reltv fl">
                      <label class="cnfrm fl">      
                      <c:if test="${qusAnsList.section_review_flag eq 'Y'}">      
                        <input type="checkbox" checked="true" id="secRev_form_<%=pageNum%>" data-id="secRevBtn" onclick="valUserRevTCBtn('<%=pageNum%>');">
                      </c:if>
                      <c:if test="${qusAnsList.section_review_flag ne 'Y'}">
                        <input type="checkbox" id="secRev_form_<%=pageNum%>" data-id="secRevBtn" onclick="valUserRevTCBtn('<%=pageNum%>');">
                      </c:if>                    
                      <span class="checkmark grn blu"></span> <span class="bn bn-grn grn_txt bn-blue"><span class="macomp">Section Reviewed</span><span class="frmcomp">Section Reviewed</span></span> 
                     </label>
                    </div>
                  </div>
                </div>
						  </div>
          <%if("1".equals(pageNum)){%>
            <script>selectChange('div_5', '5');changeBtn('4', 'ONLOAD');selectChange('div_5', '5')</script>
          <%}else if("2".equals(pageNum)){%>
          <script>
            prepopulateDOB('DOB');
            selectChange('div_9', '9');
            selectChange('div_14', '14');
            selectChange('div_MM', 'MM');
            selectChange('div_DD', 'DD');
            selectChange('div_YYYY', 'YYYY');
          </script>
        <%}else if("3".equals(pageNum)){%>
          <script>
            selectChange('div_17', '17');
            selectChange('div_18', '18');
          </script>
        <%}if(!GenericValidator.isBlankOrNull(pageNum)){%>
         <script>enableRevSecBtn('form_<%=pageNum%>');</script>
        <%}%>