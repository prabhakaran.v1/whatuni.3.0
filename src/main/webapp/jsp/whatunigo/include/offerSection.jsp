<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<jsp:scriptlet>
String timerExpCall = (String)request.getAttribute("timerExpCall");
String ERROR_FLAG = (String)request.getAttribute("ERROR_FLAG");
String ERROR_MESSAGE = (String)request.getAttribute("ERROR_MESSAGE");
</jsp:scriptlet>

<%if("Y".equalsIgnoreCase(ERROR_FLAG)){%>

<input type="hidden" id="" value="" />

<script>
errorLiboxCall('ERROR', '<%=ERROR_MESSAGE%>');
</script>

<%}else if(!"Y".equalsIgnoreCase(timerExpCall)){%>
<section class="cmm_cnt">
  <div class="cmm_col_12 chse_cnt no_topsec">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <h1>What an excellent choice!</h1>
        <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/>
        <c:if test="${not empty requestScope.offerText}">
          <c:out value="${requestScope.offerText}" escapeXml="false"/>
        </c:if>
        <input type="hidden" id="qusArr" value=""/>
          <div class="btn_cnt pt-40">            
            <div class="fr">
              <a id="acceptingOfferBtn" onclick="navigation('1', '1');" class="fr bton">PROCEED <i class="fa fa-long-arrow-right"></i></a>
            </div>            
            <div class="fl">
              <a href="javascript:void(0);" onclick="callLightBox('BACK_TO_COURSE');" class="fl bck"><i class="fa fa-long-arrow-left"></i> Back to Course</a>
            </div>            
          </div>
    </div>
    </div>
    </div>
</section>
<%}else{%>
<script type="text/javascript">
timerOneExpiryCall('', 'timer-2');
</script>
<%}%>

<script type="text/javascript">
jq( document ).ready(function() {
  //clearInterval(setIrlId);
  stopinterval();
  startTimer1('TIMER_2');
  //wugoStatsLogging('OFFER_FORM');
 });
</script>