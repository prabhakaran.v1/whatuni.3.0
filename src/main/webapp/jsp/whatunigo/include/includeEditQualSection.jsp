<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants" %>
<%int count = 0, subCount = 0; 
  String pageNo = (String)request.getAttribute("pageNo");
  String qualSub = "";
  String qualId = "";  
  String ajaxActionName = "QUAL_SUB_AJAX";
  String reviewFlag = "N";
  String removeBtnFlag = "display:block";
  String qualSize = (String)request.getAttribute("userQualSize");
  int qulSize = qualSize != null ? Integer.parseInt(qualSize) : 0;
  String qlCls = "";
  String selectFlag = "";
  String gcseSelectFlag = "";
  String newGCSESelectFlag = "";
  String oldGCSESelectFlag = "";
  String gcseSelOption = ": 9-1";
  String userTariff = (String)request.getAttribute("userTariff");
  String removeLink =   CommonUtil.getImgPath("/wu-cont/images/cmm_close_icon.svg",0);
  String addQualStyle = "display:none";
  String addSubStyle = "display:block";
  int addSubLimit = 6;
  String removeQualStyle = "display:none";
  int userQualListSize = (!GenericValidator.isBlankOrNull((String)request.getAttribute("userQualSize"))) ? Integer.parseInt((String)request.getAttribute("userQualSize")) : 0;
  String addQualId = "level_3_add_qualif";
%>
<div class="qualif qualedit">
<div class="ucas_midcnt">
  <div id="ucas_calc">
    <div class="ucas_mid">
    <c:forEach var="userQualList" items="${requestScope.userQualList}" varStatus="i" >
      <c:set var="indexValue" value="${i.index}"/>
      <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexValue").toString())); qlCls = ""; if(count == (qulSize - 1)){ qlCls = "bor_btm0";} %>
                  <c:if test="${userQualList.entry_qual_desc eq 'GCSE'}">
                    <%count = 17;addSubLimit=20;%>
                  </c:if>
                  <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                    <%addQualId = "level_3_add_qualif";%>
                  </c:if>
                  <c:if test="${userQualList.entry_qual_desc eq 'GCSE'}">
                    <%addQualId = "level_2_add_qualif";%>
                  </c:if>
                  <div class="add_qualif" id="add_qualif_<%=count%>" data-id="<%=addQualId%>">
          <div class="ucas_refld">
            
            <h3 class="un-tit">Level <c:if test="${not empty userQualList.entry_qual_level}"> ${userQualList.entry_qual_level}</c:if> </h3>
            <div class="l3q_rw" id="qual_level_<%=count%>">
            
              <div class="adqual_rw">
                <div class="ucas_row">
                  <h5 class="cmn-tit txt-upr">Qualifications</h5>
                  <fieldset class="ucas_fldrw">
                    <div class="remq_cnt">
                      <div class="ucas_selcnt">
                        <fieldset class="ucas_sbox">
                          <c:if test="${userQualList.entry_qual_desc eq 'GCSE'}">
                                <c:if test="${userQualList.gcse_grade_flag eq 'OLD'}">
                                   <jsp:scriptlet>gcseSelOption = ": A*-G";</jsp:scriptlet>
                                </c:if>
                                <span class="sbox_txt" id="qualspan_<%=count%>">${userQualList.entry_qual_desc} <%=gcseSelOption%></span>
                              </c:if>
                              <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                                <span class="sbox_txt" id="qualspan_<%=count%>">${userQualList.entry_qual_desc}</span>
															</c:if>
                          <i class="fa fa-angle-down fa-lg"></i>
                        </fieldset>
                        <c:if test="${userQualList.entry_qual_level ne '2'}">
														<select name="qualification<%=count%>" onchange="appendQualDiv(this, '', '<%=count%>');" id="qualsel_<%=count%>" class="ucas_slist">
                            <option value="0">Please Select</option>
                            <c:forEach var="qualList" items="${requestScope.qualificationList}">
                               <c:set var="selectQualId" value="${userQualList.entry_qual_id}"/>
                               <%String selectQualId = pageContext.getAttribute("selectQualId").toString();%>
                               <c:set var="selectQualId_1" value="<%=selectQualId%>" />
                               <jsp:scriptlet>selectFlag = "";</jsp:scriptlet>
                               <c:if test="${qualList.entry_qual_id eq selectQualId_1}">
                                 <jsp:scriptlet>selectFlag = "selected=\"selected\"";</jsp:scriptlet>
                               </c:if>
                               <c:if test="${qualList.entry_qualification ne 'GCSE'}">
                                     <c:if test="${empty qualList.entry_qual_id}">
                                       <optgroup label="${qualList.entry_qualification}"></optgroup>
                                     </c:if>
                                     <c:if test="${not empty qualList.entry_qual_id}">
                              <option value="${qualList.entry_qual_id }" <%=selectFlag%>>${qualList.entry_qualification }</option>
                                     </c:if>                                  
                              </c:if>
                            </c:forEach>
                          </select>
                          <div id="qual_sub_grd_hidId">
                          <c:forEach var="grdQualifications" items="${requestScope.qualificationList}" varStatus="in" >
                          <c:set var="indexInValue" value="${in.index }"/>
                            <c:if test="${grdQualifications.entry_qualification ne 'GCSE'}">                             
                                   <c:if test="${not empty grdQualifications.entry_qual_id}">
                              <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id }" name="qualSubj_${grdQualifications.entry_qual_id }" value="${grdQualifications.entry_subject }" />
                              <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}" name="" value="${grdQualifications.entry_grade }" />
                                   </c:if>
                            </c:if>                                
                          </c:forEach>
                          </div>
                        </c:if>
                        <c:if test="${userQualList.entry_qual_level eq '2'}">
                               <select name="qualification<%=count%>" onchange="appendQualDiv(this, '', '<%=count%>');" id="qualsel_<%=count%>" class="ucas_slist">
                          <option value="0">Please Select</option>
                          <c:forEach var="qualList" items="${requestScope.qualificationList}" >
                             <c:if test="${qualList.entry_qualification eq 'GCSE'}">
                               <c:set var="selectQualId2" value="${userQualList.gcse_grade_flag}"/>
                               <jsp:scriptlet>gcseSelectFlag = "";</jsp:scriptlet>
                               <c:if test="${userQualList.gcse_grade_flag eq 'NEW'}">
                                 <jsp:scriptlet>gcseSelectFlag = "selected=\"selected\"";</jsp:scriptlet>
                               </c:if>
                               
                               <option value="${qualList.entry_qual_id }_gcse_new_grade" <%=gcseSelectFlag%>>${qualList.entry_qualification }: 9-1 </option>
                               <jsp:scriptlet>gcseSelectFlag = "";</jsp:scriptlet>
                               <c:if test="${userQualList.gcse_grade_flag eq 'OLD'}">
                                 <jsp:scriptlet>gcseSelectFlag = "selected=\"selected\"";</jsp:scriptlet>
                               </c:if>
                               <option value="${qualList.entry_qual_id }_gcse_old_grade" <%=gcseSelectFlag%>>${qualList.entry_qualification }: A*-G</option>
                             </c:if>
                          </c:forEach>
                        </select>
                        <c:forEach var="grdQualifications" items="${requestScope.qualificationList}" varStatus="in" >
                                 <c:if test="${grdQualifications.entry_qualification eq 'GCSE'}">                                                            
                                   <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id }_gcse" name="qualSubj_${grdQualifications.entry_qual_id }" value="${grdQualifications.entry_subject}" />
                                   <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_new_grade" name="" value="${grdQualifications.entry_grade}" />
                                   <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_old_grade" name="" value="${grdQualifications.entry_old_grade}" />
                                 </c:if>
                               </c:forEach>
                              </c:if>
                        
                      </div>
                    </div>
                              <%if(count != 0){removeQualStyle = "display:block";}%>
                              <div class="add_fld" style="<%=removeQualStyle%>">
                                <a id="remQual<%=count%>" onclick="removeQualEntryReq('add_qualif_<%=count%>', '<%=count%>');" class="btn_act1 bck f-14 pt-5">
                                  <span class="hd-mob">Remove Qualification</span>
                                </a>
                              </div>													
													
                  </fieldset>
                  <%--<div class="err" id="qualSubId_error" style="display:none;"></div>--%>                                  
                </div>
                 <div class="subgrd_fld" id="subGrdDiv<%=count%>">
                      <div class="ucas_row grd_row" id="grdrow_<%=count%>">
                      <fieldset class="ucas_fldrw quagrd_tit">
														<div class="ucas_tbox tx-bx">
                                <h5 class="cmn-tit txt-upr qual_sub">Subject</h5>
														</div>
														<div class="ucas_selcnt rsize_wdth">
                              <h5 class="cmn-tit txt-upr qual_grd">Grade</h5>
														</div>
                          </fieldset>
                          <c:forEach var="subjectList" items="${userQualList.qual_subject_list}" varStatus="j" >
                          <c:set var="indexJValue" value="${j.index}"/>
                          <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue").toString()));%>
                          <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
                        <fieldset class="ucas_fldrw">
                          <div class="ucas_tbox w-390 tx-bx">
                            <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="sub_${subjectList.entry_subject_id }" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" 
                              placeholder="${subjectList.entry_subject }" value="${subjectList.entry_subject}" class="frm-ele" autocomplete="off">															
                            <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                            <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                            </div>
                        </div>  
                        <div class="ucas_selcnt rsize_wdth">
                          <div class="ucas_sbox mb-ht w-84" id="1grades1">
                            <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">${subjectList.entry_grade}</span><i class="fa fa-angle-down fa-lg"></i>
                          </div>
                          <select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');" id="qualGrd_<%=count%>_<%=subCount%>"></select>
                          <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=count%>_<%=subCount%>', '${subjectList.entry_grade}'); </script>
                          <c:if test="${userQualList.entry_qual_level ne '2'}">
                            <input class="subjectArr" data-id="level_3" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${userQualList.entry_qual_id}" value="${subjectList.entry_subject_id}" />
                          </c:if>
                          <c:if test="${userQualList.entry_qual_level eq '2'}">
                            <input class="subjectArr" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${userQualList.entry_qual_id}" value="${subjectList.entry_subject_id}" />
                          </c:if>
                          <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_${userQualList.entry_qual_id}" value="${subjectList.entry_grade}" />
                          <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${userQualList.entry_qual_id}" value="${userQualList.entry_qual_id}" />
                          <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${userQualList.entry_qual_id}" value="<%=count+1%>" />
                        </div>
                        <%if(subCount == 0){
                            removeBtnFlag = "display:none";
                          }else{
                            removeBtnFlag = "display:block";
                          }
                          if(subCount == 1){
                          %>
                          <script>jq("#remSubRow<%=count%>0").show();</script>
                          <%}%>
                        <div class="add_fld"> <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"></span></a></div>
                      </fieldset>
                    </div>
                  </c:forEach>
                        <%if((subCount + 1) < addSubLimit){
                          addSubStyle = "display:block";
                        }else{
                          addSubStyle = "display:none";
                        }%>
                        <div class="ad-subjt" id="addSubject_<%=count%>" style="<%=addSubStyle%>">
                    <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('${userQualList.entry_qual_id}','countAddSubj_<%=count%>', '<%=count%>');">
                    <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                  </div>
                </div>
              </div>              
	      <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
              <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
              
            </div>
          </div>
                   <c:if test="${userQualList.entry_qual_desc ne 'GCSE'}">
                    <%if(count == (userQualListSize-1)){addQualStyle = "display:none";}%>                    
										<div class="add_qualtxt" id="addQualBtn_<%=count%>" style="<%=addQualStyle%>">
                      <a href="javascript:void(0);" onclick="addQualEntryReq('<%=count+1%>')" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> ADD A QUALIFICATION</a></div>	
										</div>
                   </c:if>
    </div>
    </c:forEach>
  </div>
  <p class="qler1" id="subErrMsg" style="display:none;"></p>
  <input type="hidden" id="oldGrade" value="<%=userTariff%>" />
  <input type="hidden" id="newGrade" value="<%=userTariff%>" />
</div>
</div>
<div class="qedt_md fl_w100">
   <a href="javascript:void(0)" onclick="passJSONObj('USER_REVIEW_QUAL_UPDATE', '5', '<%=reviewFlag%>', '4');">Confirm changes</a>
</div>   
</div>
 <div class="btn_cnt">
  <div class="fl">
    <div class="btn_cmps fl">
      <div class="reltv fl">
        <label class="cnfrm fl">
        <c:if test="${requestScope.SECTION_REVIEWED_FLAG eq 'Y'}">
          <input type="checkbox" checked="true" id="secRev_form_4" data-id="secRevBtn" onclick="valUserRevTCBtn('4');">
        </c:if>
        <c:if test="${requestScope.SECTION_REVIEWED_FLAG ne 'Y'}">
          <input type="checkbox" id="secRev_form_4" data-id="secRevBtn" onclick="valUserRevTCBtn('4');">
        </c:if>       
        <span class="checkmark grn blu"></span> <span class="bn bn-grn grn_txt bn-blue"><span class="macomp">Section Reviewed</span><span class="frmcomp">Section Reviewed</span></span> 
       </label>
      </div>
    </div>
  </div>
</div>
<script>enableRevSecBtn('form_4');onloadShowingAddQualBtn();</script>
<input type="hidden" id="removeLinkImg" value="<%=removeLink%>" />
<input type="hidden" id="reviewEditQualPage" value="Y" />