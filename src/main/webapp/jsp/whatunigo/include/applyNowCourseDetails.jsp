<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants" %>
<%
  String courseId = !GenericValidator.isBlankOrNull((String)request.getAttribute("COURSE_ID")) ? (String)request.getAttribute("COURSE_ID") : "";
  String opportunityId = !GenericValidator.isBlankOrNull((String)request.getAttribute("OPPORTUNITY_ID")) ? (String)request.getAttribute("OPPORTUNITY_ID") : "";
  String listName = "APPLY_NOW_COURSE_LIST";
  boolean congratsPageFlag = false;
  if(!GenericValidator.isBlankOrNull(request.getParameter("CONGRATS_PAGE_COURSE_INFO_LIST_NAME"))){
     congratsPageFlag = true;
     listName = request.getParameter("CONGRATS_PAGE_COURSE_INFO_LIST_NAME");
   }
  String pageName = request.getParameter("pageName"); 
%>
<c:if test="${not empty requestScope.APPLY_NOW_COURSE_LIST}">
  <c:forEach items="${requestScope.APPLY_NOW_COURSE_LIST}" var="applyNowCourseList">
    <%if("wugoSignUpPage".equalsIgnoreCase(pageName)){%>
      <h1>You'll need to create an account to apply for this course</h1>
      <p class="cnfrm f-16">Once you've registered we'll quickly check your qualifications and then we'll be able to tell you if you're eligible for an Offer in Principle.</p>
    <%}else{%>
      <h1>Let's get started...</h1>
    <%}%>
    <div class="cmm_unilst provlgo_hid">
      <div class="fl uni-logo col_lft">
        <c:if test="${not empty applyNowCourseList.collegeLogoPath}">
          <a href="javascript:void(0);"><img src="<%=CommonUtil.getImgPath("",0)%>${applyNowCourseList.collegeLogoPath}" alt="${applyNowCourseList.collegeNameDisplay}"></a>
        </c:if>
        <c:if test="${empty applyNowCourseList.collegeLogoPath}">
          <a href="javascript:void(0);"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" alt="${applyNowCourseList.collegeNameDisplay}"></a>          
        </c:if>
      </div>
      <div class="fl col_rgt">
        <h3 class="un-tit"><a href="javascript:void(0);">${applyNowCourseList.collegeNameDisplay}</a></h3>
        <h6 class="sub-tit"><a href="javascript:void(0);">${applyNowCourseList.courseTitle}</a></h6>
        
        <div class="fx">
          <c:if test="${not empty applyNowCourseList.studyMode}">
            <div class="fx-bx">
              <h5>Study Mode</h5>
              <h6>${applyNowCourseList.studyMode}</h6>
            </div>
          </c:if>
          <c:if test="${not empty applyNowCourseList.studyDuration}">
            <div class="fx-bx">
              <h5>Study Duration</h5>
              <h6>${applyNowCourseList.studyDuration}</h6>
            </div>
          </c:if>
          <c:if test="${not empty applyNowCourseList.ucasTariffPoints}">
            <div class="fx-bx">
              <h5>UCAS Tariff Points                   
                <span class="hlp tool_tip"><i class="fa fa-question-circle"></i>
                  <span class="cmp hlp-tt">
                    <div class="hdf5"></div>
                    <div><spring:message code="wuni.cmmt.ucas.tariff.tooltip.text"/></div> 
                    <div class="line"></div>
                  </span>
                </span>                  
              </h5>
              <h6>${applyNowCourseList.ucasTariffPoints}</h6>
            </div>
          </c:if>
        </div>       
      </div>
    </div>
  </c:forEach>
  <form:form action="/clearing-match-maker-tool.html"  styleClass="" commandName="wuGoFormBean">
    <c:forEach items="${requestScope.APPLY_NOW_COURSE_LIST}" var="applyNowCourseList">
      <input type="hidden" value='${applyNowCourseList.collegeId}' id ="collegeId" />
       
      <input type="hidden" value='<%=courseId%>' name="courseId" id="courseId" />
      <input type="hidden" value='<%=opportunityId%>' name="opportunityId" id="opportunityId" />
      
      <input type="hidden" value='${applyNowCourseList.collegeLogoPath}' name="collegeLogo" id="collegeLogo" />  
      <input type="hidden" value='${applyNowCourseList.courseTitle}' name="courseTitle" id="courseTitle" />
      <input type="hidden" value='${applyNowCourseList.studyMode}' name="studyMode" id="studyMode" />
      <input type="hidden" value='${applyNowCourseList.studyDuration}' name="studyDuration" id="studyDuration" />  
      <input type="hidden" value='${applyNowCourseList.ucasTariffPoints}' name="tariffPoint" id="tariffPoint" />
      <input type="hidden" value='${applyNowCourseList.collegeNameDisplay}' name="collegeNameDisp" id="collegeNameDisp" />
    </c:forEach>
  </form:form>
</c:if>