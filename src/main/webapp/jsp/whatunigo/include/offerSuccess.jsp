<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>
<%
  String provisionalOfferJsName = CommonUtil.getResourceMessage("wuni.provisional.offer.js", null);
  String whatuniGOJsName = CommonUtil.getResourceMessage("wuni.whatunigo.offer.js", null);
  String browserip = GlobalConstants.WHATUNI_SCHEME_NAME + new CommonUtil().getProperties().getString("review.autocomplete.ip");
  String COLLEGE_DISPLAY_NAME = (String)request.getAttribute("COLLEGE_DISPLAY_NAME");
  String COURSE_NAME = (String)request.getAttribute("COURSE_NAME");
  String collegeName = (String)request.getAttribute("hitbox_college_name");
  String refererURL = (request.getAttribute("REFERER_URL") != null) ? (String)request.getAttribute("REFERER_URL") : "";
  String actionName = (String)request.getAttribute("actionName");
  String collegeId = (String)request.getAttribute("COLLEGE_ID");
%>

    <header class="clipart cmm_hdr">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>  
    </header>
    <div id="middleContent">
      <%if("provisonal-offer-congrats".equalsIgnoreCase(actionName)){%>
        <jsp:include page="/jsp/whatunigo/include/provisionalOfferCongratsSection.jsp"/>      
        <input type="hidden" id="offerName" value="PROVISIONAL_OFFER"/>
        <script>jq( document ).ready(function() {timerMsgFunc('offer_timer', 'TIMER_2');});</script>
      <%}else{%>      
      <section class="cmm_cnt">
        <div class="cong_cnt">
          <h1>Congratulations!</h1>
        </div>
        <div class="cmm_col_12 chse_cnt cngs_cnt">
          <div class="cmm_wrap">
            <div class="cmm_row">
                <!-- Uni List -->
            <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/>
            <!-- Uni List -->
              <div class="cnf_off">
              <c:forEach var="courseInfoList" items="${requestScope.courseInfoList}" >
                <p>Well done! You've now confirmed your application to ${courseInfoList.college_display_name}.</p>
              </c:forEach>
                <p>We will now send your application to the university for review. The university will be in touch with you shortly* after they have received your application to make their own final checks. Please make sure that your phone is charged and switched on.</p>
                <p>The university will either confirm your formal offer or let you know that they cannot make a formal offer. If they do make you a formal offer, start celebrating, you're going to <%=COLLEGE_DISPLAY_NAME%>!</p>
                <p>We wish you the best of luck. Please share your excitement with your friends!</p>
                <p>(* If you make your application between 9am - 5pm, Mon-Fri, the uni will be in touch within 1 hour. If you make you application outside these times, the uni will contact you on the next working day. Read our full <a href="javascript:void(0);" onclick="showTermsConditionPopup();">T&amp;Cs</a>.)</p>
                <div class="sgn">
                  <div class="so-btn">
                    <a class="btn1 fbbtn" onclick="shareFBTweet('FB')" title="Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK</a>
                    <a class="btn1 twbtn" onclick="shareFBTweet('TWEET')" title="Twitter"><i class="fa fa-twitter fa-1_5x"></i>TWITTER</a> 
                  </div>
                  <p class="qler1 valid_err" id="fblbregistration_error" style="display: none;"></p>
                </div>
              </div>
            </div>
          </div>
        </div>
          <c:out value="${requestScope.staticContent}" escapeXml="false"/>  
      </section>
      <input type="hidden" id="offerName" value="ACCEPTING_OFFER"/>
      <%}%>
    </div>
    
    <input type="hidden" id="courseName" value="<%=COURSE_NAME%>" />
    <input type="hidden" id="collegeDispName" value="<%=COLLEGE_DISPLAY_NAME%>" />
    <input type="hidden" id="refererUrl" name="refererUrl" value="<%=refererURL%>" />    
    <input type="hidden" id="applyNowCourseId" name="applyNowCourseId" value="${requestScope.APPLY_NOW_COURSE_ID}"/>
    <input type="hidden" id="applyNowOpportunityId" name="applyNowOpportunityId" value="${requestScope.APPLY_NOW_OPPORTUNITY_ID}"/>    
    <input type="hidden" id="endTimeT1" name="endTimeT1" value="${requestScope.C_END_TIME_T1}"/>        
    <input type="hidden" id="statusT1" name="statusT1" value="N"/>
    <input type="hidden" id="collegeName" value="<%=collegeName%>"/>
    <input type="hidden" id="collegeId" value="<%=collegeId%>"/>
    <jsp:include page="/jsp/common/wuFooter.jsp"/>
