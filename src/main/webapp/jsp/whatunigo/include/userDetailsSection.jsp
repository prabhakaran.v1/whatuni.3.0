<%@ taglib uri="http://java.sun.com/jstl/core"  prefix="c" %>
<%@page import="org.apache.commons.validator.GenericValidator"%>
<jsp:scriptlet>
  String fieldSetCls = "tx-bx w-100 mob-mb-10";
  String divCls = "pers_lst pt-30";
  String questionIds = "";
  String questionId = ""; 
  String format = "";
  String checkedFlag = "checked='checked'";
  String formStatusId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("pageStatus"))) ? (String)request.getAttribute("pageStatus") : "";
</jsp:scriptlet>
<section class="cmm_cnt">
    <div class="brd_crumb timer_sec">
          <div class="cmm_col_12">
            <div class="cmm_wrap">
                <div class="brc_cnt lft fl">
                  <div class="brc_row1">
                    <span class="bc_hdtxt"><c:if test="${not empty requestScope.pageName}"> ${requestScope.pageName}</c:if> </span><span class="bc_stpno">Step <c:if test="${not empty requestScope.pageNo}"> ${requestScope.pageNo}</c:if> of 4</span>
                  </div>
                  <jsp:include page="/jsp/whatunigo/include/includeStepsSection.jsp"/>
                </div>
                 <jsp:include page="/jsp/whatunigo/include/timer.jsp"></jsp:include>  
            </div>
      </div>
    </div>
    <div class="cmm_col_12 binf_cnt">
      <div class="cmm_wrap">
        <div class="cmm_row">

        <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/> 
      <c:if test="${not empty requestScope.questionAnswerList}">
        <form id="userInfo" class="fl basic_inf pers_det">
        <c:forEach var="userInfoQuestionList" items="${requestScope.questionAnswerList}">
          <c:set var="questionId" value="${userInfoQuestionList.page_question_id}" />
          <% questionId = String.valueOf((Integer.parseInt(pageContext.getAttribute("questionId").toString()))); %>
          <c:set var="format" value="${userInfoQuestionList.format_desc}" />
          <% format = pageContext.getAttribute("format").toString(); %>
          <c:if test="${'9' eq userInfoQuestionList.page_question_id}">
         <div id="div_${userInfoQuestionList.page_question_id}" class="pers_lst pt-40 ucas_tit">
            <div class="col-half">
              <div class="ucas_selcnt ">
                <fieldset class="ucas_sbox">
                  <span class="sbox_txt" id="select_${userInfoQuestionList.page_question_id}_span">${userInfoQuestionList.question_title}<c:if test="${'Y' eq userInfoQuestionList.mandatory_flag}">*</c:if></span>
                  <i class="fa fa-angle-down fa-lg"></i>
                </fieldset>
                <select name="DROPDOWN_${userInfoQuestionList.page_question_id}" id="DROPDOWN_${userInfoQuestionList.page_question_id}" class="ucas_slist" onchange="selectChange('div_${userInfoQuestionList.page_question_id}', '${userInfoQuestionList.page_question_id}');validateFormData(this.id, 'DROPDOWN');formSubmit('','uncheck');enOrDisTopNav();">
                 <option value="">Title</option>
                 <c:forEach var="qusOptions" items="${userInfoQuestionList.question_options}">
                   <c:set var="selectId" value="${qusOptions.option_id}" />
                   <jsp:scriptlet>checkedFlag = "";</jsp:scriptlet>
                   <c:if test="${selectId eq userInfoQuestionList.answer_option_id}">
                     <jsp:scriptlet>checkedFlag = "selected";</jsp:scriptlet>
                   </c:if>
                   <option value="${qusOptions.option_id}" <%=checkedFlag%>>${qusOptions.option_desc}</option>
                 </c:forEach>
                </select>
              </div>
              <p class="fail-msg fl_w100" id="DROPDOWN_${userInfoQuestionList.page_question_id}_Err" style="display:none;"></p>
            </div>
          </div>
          </c:if>
          <%--Added Nationality field by D.Jeyalakshmi on 01/Aug/2019 rel--%>          
          <c:if test="${'25' eq userInfoQuestionList.page_question_id}">
          <div id="div_<%=questionId %>" class="pers_lst pt-30 gend nation">
            <h5 class="cmn-tit txt-upr">${userInfoQuestionList.question_title}<c:if test="${'Y' eq userInfoQuestionList.mandatory_flag}">*</c:if>
              <a href="javascript:void(0);" class="blu-lnk rltv bsc"><span>Why we need this?</span>
              <span id="postcodeText" class="cmp adjst">
                <div class="hdf5"></div>
                <div>The university requires this detail in order to process your application.</div>
                <div class="line"></div>
              </span>
              </a>
            </h5>
            <div class="gen_cnt fl">
              <div class="ucas_selcnt">
                 <fieldset class="ucas_sbox">
                   <span class="sbox_txt" id="select_${userInfoQuestionList.page_question_id}_span">Nationality</span>
                   <i class="fa fa-angle-down fa-lg"></i>
                 </fieldset>
                 <select name="DROPDOWN_${userInfoQuestionList.page_question_id}" id="DROPDOWN_${userInfoQuestionList.page_question_id}" class="ucas_slist" onchange="selectChange('div_${userInfoQuestionList.page_question_id}', '${userInfoQuestionList.page_question_id}');validateFormData(this.id, 'DROPDOWN');formSubmit('','uncheck');enOrDisTopNav();">
                   <option value="">Nationality</option>
                   <c:forEach var="qusOptions" items="${userInfoQuestionList.question_options}" varStatus="row">
                     <c:set var="index" value="${row.index}" />
                     <c:set var="selectid1" value="${qusOptions.option_id}" />
                   <jsp:scriptlet>checkedFlag = "";</jsp:scriptlet>
                   <c:if test="${selectid1 eq userInfoQuestionList.answer_option_id}">
                     <jsp:scriptlet>checkedFlag = "selected";</jsp:scriptlet>
                   </c:if>                   
                     <option value="${qusOptions.option_id}" <%=checkedFlag%>>${qusOptions.option_desc}</option>
                     <% if(Integer.parseInt(pageContext.getAttribute("index").toString()) == 4 ){%>
                     <optgroup label="-----------------------------------"></optgroup>
                     <%}%>
                   </c:forEach>
                 </select>
              </div>
             <p class="fail-msg fl_w100" id="DROPDOWN_${userInfoQuestionList.page_question_id}_Err" style="display:none;"></p>
            </div>
          </div>  
          </c:if>
          <c:if test="${'TEXT' eq userInfoQuestionList.format_desc}">
            <jsp:scriptlet>
            divCls = "";
            fieldSetCls = "tx-bx w-100";
            if("10".equals(questionId)){
            divCls = "pt-20 fnsec";
              fieldSetCls = "fl tx-bx";
            }else if("11".equals(questionId)){
              divCls = "pt-20 lnsec";
              fieldSetCls = "fl tx-bx";
            }else if("12".equals(questionId)){
              divCls = "pt-20";
              fieldSetCls = "tx-bx w-100";
            }else if("15".equals(questionId)){
              fieldSetCls = "tx-bx w-100 mob-mb-10";
              divCls = "pt-20 uid";
            }
          </jsp:scriptlet>
           <div class="pers_lst <%=divCls%>">
            <fieldset class="<%=fieldSetCls%>">
              <c:if test="${'15' eq userInfoQuestionList.page_question_id}">
                <input type="text" id="TEXT_${userInfoQuestionList.page_question_id}" name="TEXT_${userInfoQuestionList.page_question_id}" value="${userInfoQuestionList.answer_value}" class="frm-ele" onblur="validateFormData(this.id, 'TEXT');formSubmit('','uncheck');" onkeyup="automaticHyphenAfterThreeDigits('TEXT_${userInfoQuestionList.page_question_id}', '');formSubmit('','uncheck');enOrDisTopNav();" onkeypress="automaticHyphenAfterThreeDigits('TEXT_${userInfoQuestionList.page_question_id}', '');" maxlength="${userInfoQuestionList.max_length}">
              </c:if>
              <c:if test="${'15' ne userInfoQuestionList.page_question_id}">
                <input type="text" id="TEXT_${userInfoQuestionList.page_question_id}" name="TEXT_${userInfoQuestionList.page_question_id}" value="${userInfoQuestionList.answer_value}" class="frm-ele" onblur="validateFormData(this.id, 'TEXT');formSubmit('','uncheck');" onkeyup="formSubmit('','uncheck');enOrDisTopNav();" maxlength="${userInfoQuestionList.max_length}">
              </c:if>
              <label for="TEXT_${userInfoQuestionList.page_question_id}" class="lbco">${userInfoQuestionList.question_title}<c:if test="${'Y' eq userInfoQuestionList.mandatory_flag}">*</c:if></label>
              <p class="fail-msg" id="TEXT_${userInfoQuestionList.page_question_id}_Err" style="display:none;"></p>
              <p class="su-msg" id="TEXT_${userInfoQuestionList.page_question_id}_Sus" style="display:none;"></p>
            </fieldset>
            <c:if test="${'15' eq userInfoQuestionList.page_question_id}">
            <p class="pt-10">This should be a 10 digit number</p>
            </c:if>
          </div>
          </c:if>
          
          <c:if test="${'13' eq userInfoQuestionList.page_question_id}">
          <div class="pers_lst pt-40 dofb">
            <h5 class="cmn-tit txt-upr">${userInfoQuestionList.question_title}<c:if test="${'Y' eq userInfoQuestionList.mandatory_flag}">*</c:if>
            <a href="javascript:void(0);" class="blu-lnk rltv bsc"><span>Why we need this?</span>
              <span id="dobtt" class="cmp adjst">
                <div class="hdf5"></div>
                <div>The university requires this detail in order to process your application.</div>
                <div class="line"></div>
              </span>
            </a>
            </h5>
            <div class="dob_cnt fl">
            
            <div class="ucas_selcnt fl" id="div_DD">
                <fieldset class="ucas_sbox">
                  <span class="sbox_txt" id="select_DD_span">DD</span>
                  <i class="fa fa-angle-down fa-lg"></i>
                </fieldset>
                <select name="DATE_DD" id="DATE_DD" class="ucas_slist" onchange="selectChange('div_DD', 'DD'); validateFormData(this.id, 'DROPDOWN'); formSubmit('','uncheck');enOrDisTopNav();">
                  <option value="">DD</option>
                  <c:forEach var="dateList" items="${requestScope.dateList}">
                    <option value="${dateList.value}">${dateList.label} </option>
                  </c:forEach>
                </select>
            </div>
          
            <div class="ucas_selcnt fl" id="div_MM">
                <fieldset class="ucas_sbox">
                  <span class="sbox_txt" id="select_MM_span">MM</span>
                  <i class="fa fa-angle-down fa-lg"></i>
                </fieldset>
                <select name="DATE_MM" id="DATE_MM" class="ucas_slist" onchange="selectChange('div_MM', 'MM'); validateFormData(this.id, 'DROPDOWN');formSubmit('','uncheck');enOrDisTopNav();">
                  <option value="">MM</option>
                  <c:forEach var="monthList" items="${requestScope.monthList}">
                    <option value="${monthList.value}">${monthList.label} </option>
                  </c:forEach>
                </select>
            </div>
            
            <div class="ucas_selcnt fl" id="div_YYYY">
                <fieldset class="ucas_sbox">
                  <span class="sbox_txt" id="select_YYYY_span">YYYY</span>
                  <i class="fa fa-angle-down fa-lg"></i>
                </fieldset>
                <select name="DATE_YYYY" id="DATE_YYYY" class="ucas_slist" onchange="selectChange('div_YYYY', 'YYYY'); validateFormData(this.id, 'DROPDOWN');formSubmit('','uncheck');enOrDisTopNav();">
                 <option value="">YYYY</option>
                 <c:forEach var="yearList" items="${requestScope.yearList}">
                    <option value="${yearList.value}">${yearList.label} </option>
                  </c:forEach>
                </select>
            </div>
            
            </div>
            <p class="fail-msg fl_w100" id="DATE_Err" style="display:none;"></p>
          </div>
          <jsp:scriptlet>
           questionIds += "DATE:DD,DATE:MM,DATE:YYYY,";
        </jsp:scriptlet>
          <input type="hidden" name="DOD" id="DOB" value="${userInfoQuestionList.answer_value}" />
          </c:if>
          <c:if test="${'14' eq userInfoQuestionList.page_question_id}">
          <div class="pers_lst pt-30 gend">
            <h5 class="cmn-tit txt-upr">${userInfoQuestionList.question_title}<c:if test="${'Y' eq userInfoQuestionList.mandatory_flag}">*</c:if> 
            <a href="javascript:void(0);" class="blu-lnk rltv bsc"><span>Why we need this?</span>
              <span id="gentt" class="cmp adjst">
                <div class="hdf5"></div>
                <div>The university requires this detail in order to process your application.</div>
                <div class="line"></div>
              </span>
            </a>
            </h5>
            
            <div class="gen_cnt fl" id="div_${userInfoQuestionList.page_question_id}">
            <div class="ucas_selcnt fl">
                <fieldset class="ucas_sbox">
                  <span class="sbox_txt" id="select_${userInfoQuestionList.page_question_id}_span">Select</span>
                  <i class="fa fa-angle-down fa-lg"></i>
                </fieldset>
                <select name="DROPDOWN_${userInfoQuestionList.page_question_id}" id="DROPDOWN_${userInfoQuestionList.page_question_id}" class="ucas_slist" onchange="selectChange('div_${userInfoQuestionList.page_question_id}', '${userInfoQuestionList.page_question_id}'); validateFormData(this.id, 'DROPDOWN');formSubmit('','uncheck');enOrDisTopNav();">
                  <option value="">Select</option>
                  <c:forEach var="qusOptions" items="${userInfoQuestionList.question_options}">
                    <c:set var="selectid" value="${qusOptions.option_id}" />
                   <jsp:scriptlet>checkedFlag = "";</jsp:scriptlet>
                   <c:if test="${selectid eq userInfoQuestionList.answer_option_id}">
                     <jsp:scriptlet>checkedFlag = "selected";</jsp:scriptlet>
                   </c:if>
                    <option value="${qusOptions.option_id}" <%=checkedFlag%>>${qusOptions.option_desc}</option>
                  </c:forEach>
                </select>
                
            </div>
            </div>
            <p class="fail-msg fl_w100" id="DROPDOWN_${userInfoQuestionList.page_question_id}_Err" style="display:none;"></p>
          </div>  
          
          </c:if>
          <jsp:scriptlet>
           if(!"13".equals(questionId)){questionIds += format +":" + questionId + ",";}
        </jsp:scriptlet>
     </c:forEach>   
     <input type="hidden" id="qusArr" value="<%=questionIds%>"/>     
          <div class="btn_cnt pt-40">            
            <div class="fr">
              	<div class="btn_cmps fl">
									<div class="reltv fl">
										<label class="cnfrm fl">            									  
									  <%if("C".equalsIgnoreCase(formStatusId)){%>
									    <input id="markAsComFlagId" onclick="formSubmit('MARK_AS_COMPLETE', '', 'MARK_AS_COMPLETE');focusErrFld();" type="checkbox" disabled="disabled" checked>
									  <%}else{%>
                      <input id="markAsComFlagId" onclick="formSubmit('MARK_AS_COMPLETE', '', 'MARK_AS_COMPLETE');focusErrFld();" type="checkbox" >
                    <%}%>
                    <span class="checkmark grn"></span> <span class="bn bn-grn grn_txt"><span class="macomp">Mark As Complete</span><span class="frmcomp">Completed</span></span> 
									 </label>
								  </div>
								</div>
              <a href="javascript:void(0)" onclick="formSubmit('userFormSubmit');focusErrFld();" class="fr bton">SAVE AND CONTINUE <i class="fa fa-long-arrow-right"></i></a>
            </div>
          <jsp:include page="/jsp/whatunigo/include/includeBackLink.jsp"/>
          </div>

    </form>
      
        </c:if>
        <script type="text/javascript">
          prepopulateDOB('DOB');
          selectChange('div_9', '9');
          selectChange('div_14', '14');
          selectChange('div_MM', 'MM');
          selectChange('div_DD', 'DD');
          selectChange('div_YYYY', 'YYYY');
           selectChange('div_25', '25');
          //To validate the mark as complete button
          formSubmit('','uncheck', 'onload');
          //
        </script>
    </div>
    </div>
    </div>
</section>


<script type="text/javascript">
 jq(document).ready (function() {
    automaticHyphenAfterThreeDigits('TEXT_15', 'onLoad');
  });
</script>