<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="org.apache.commons.validator.GenericValidator"%>
<jsp:scriptlet>
  String questionIds = "";
  String checkedFlag = "checked='checked'";
  String hideFlag = "display:block;";
  String clsName = "bn-outln";
  String validateStr = "";
  String formStatusId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("pageStatus"))) ? (String)request.getAttribute("pageStatus") : "";
</jsp:scriptlet>
<section class="cmm_cnt">
    <div class="brd_crumb timer_sec">
          <div class="cmm_col_12">
            <div class="cmm_wrap">
                <div class="brc_cnt lft fl">
                  <div class="brc_row1">
                    <span class="bc_hdtxt">${requestScope.pageName} </span><span class="bc_stpno">Step ${requestScope.pageNo} of 4</span>
                  </div>
                  <jsp:include page="/jsp/whatunigo/include/includeStepsSection.jsp"/>
                </div>
              <jsp:include page="/jsp/whatunigo/include/timer.jsp"/>  
         </div>
      </div>
    </div>
    <div class="cmm_col_12 binf_cnt">
      <div class="cmm_wrap">
        <div class="cmm_row">
  
         <jsp:include page="/jsp/whatunigo/include/uniInfoSection.jsp"/>
  <c:if test="${not empty requestScope.questionAnswerList}">
    
    <form id="basicInfo" class="fl basic_inf">
    <c:forEach var="basicInfoQuestionList" items="${requestScope.questionAnswerList}">
      <c:set var="questionId" value="${basicInfoQuestionList.page_question_id}" />
      <c:set var="format" value="${basicInfoQuestionList.format_desc}" />
      <jsp:scriptlet>hideFlag = "display:block;";</jsp:scriptlet>
        <c:if test="${basicInfoQuestionList.default_show_flag eq 'N'}">
          <jsp:scriptlet>hideFlag = "display:none";</jsp:scriptlet>
        </c:if>
              
      <div id="div_${basicInfoQuestionList.page_question_id}" style="<%=hideFlag%>" class="bas_lst pt-40" data-id="div_${basicInfoQuestionList.page_question_id}">        
        <h5 class="cmn-tit">${basicInfoQuestionList.question_title}<c:if test="${basicInfoQuestionList.mandatory_flag eq 'Y'}">*</c:if>
        <c:if test="${basicInfoQuestionList.page_question_id eq '5'}">
        <span class="hlp tool_tip wto_ttip"><i class="fa fa-question-circle"></i>
                                    <span class="cmp hlp-tt">
                                        <div class="hdf5"></div>
                                        <div>${basicInfoQuestionList.help_text_desc}</div>
                                        <div class="line"></div>
                                    </span>
                                </span>
                                </c:if>
        
        </h5>
        <c:if test="${basicInfoQuestionList.format_desc eq 'RADIO'}">
          <div class="blst_rw" id="div_${basicInfoQuestionList.page_question_id}">
          <c:set var="pageQusId" value="${basicInfoQuestionList.page_question_id}" />
          <c:forEach var="qusOptions" items="${basicInfoQuestionList.question_options}" >
              
               <c:set var="optionId" value="${qusOptions.option_id}"/>
               <jsp:scriptlet>
                 checkedFlag = ""; clsName = "bn-outln";
               </jsp:scriptlet>
               <c:if test="${basicInfoQuestionList.answer_option_id eq optionId}">
                 <jsp:scriptlet>checkedFlag = "checked='checked'"; clsName = "bn-outln bn-gry active";</jsp:scriptlet>
               </c:if>
              <%-- <button type="button"  name="${basicInfoQuestionList.page_question_id}" onclick="changeBtn('${basicInfoQuestionList.page_question_id}');" class="bn i-bn bn-outln">${qusOptions.option_desc}</button>--%>
               <a class="opt_${questionId} bn i-bn <%=clsName%>" id="${optionId}" name="${questionId}" onclick="changeRadio(this.id, '${questionId}');changeBtn('${questionId}');enOrDisTopNav();" ${qusOptions.option_desc} style="<%=hideFlag%>">${qusOptions.option_desc}</a>
               <%--<input type="radio" id="${qusOptions.option_id}" name="${basicInfoQuestionList.page_question_id}" class="bn i-bn bn-outln" onclick="changeBtn('${basicInfoQuestionList.page_question_id}');" value="${qusOptions.option_desc}" <%=checkedFlag%>  />--%>
            </c:forEach>
            <input type="hidden" id="hid_${basicInfoQuestionList.page_question_id}" class="${basicInfoQuestionList.answer_option_id}" value="${basicInfoQuestionList.answer_value}"/>
            <jsp:scriptlet>validateStr += "hid_"+ pageContext.getAttribute("pageQusId").toString() + ",";</jsp:scriptlet>
            <c:if test="${basicInfoQuestionList.page_question_id eq '7'}">
            <span class="blu-lnk rltv bsc"><span>${basicInfoQuestionList.help_text_title}</span>
            <span id="postcodeText" class="cmp adjst">
              <div class="hdf5"></div>
              <div>${basicInfoQuestionList.help_text_desc}</div>
              <div class="line"></div>
            </span>
          </span>
          </c:if>
        </div>
        </c:if>
        <c:if test="${basicInfoQuestionList.format_desc eq 'DROPDOWN'}">
        <div class="ucas_selcnt w-300" id="div_${basicInfoQuestionList.page_question_id}">
          <div class="ucas_sbox">
          <span class="sbox_txt" id="select_${basicInfoQuestionList.page_question_id}_span">Please Choose</span>
          <i class="fa fa-angle-down fa-lg"></i>
          </div>
          <select name="${basicInfoQuestionList.page_question_id}" id="${basicInfoQuestionList.page_question_id}" class="ucas_slist" onchange="selectChange('div_${basicInfoQuestionList.page_question_id}', '${basicInfoQuestionList.page_question_id}');enOrDisTopNav();changeDefaultVal('div_${basicInfoQuestionList.page_question_id}');">
            <option>Please Choose</option>
            <c:forEach var="qusOptions" items="${basicInfoQuestionList.question_options}">
               <c:set var="selectId" value="${qusOptions.option_id}" />
               <jsp:scriptlet>checkedFlag = "";</jsp:scriptlet>
               <c:if test="${basicInfoQuestionList.answer_option_id eq selectId}">
                 <jsp:scriptlet>checkedFlag = "selected";</jsp:scriptlet>
               </c:if>
               <option value="${qusOptions.option_id}" <%=checkedFlag%>>${qusOptions.option_desc}</option>
            </c:forEach>
          </select>
          <input type="hidden" id="hid_${basicInfoQuestionList.page_question_id}" class="${basicInfoQuestionList.answer_option_id}" value="${basicInfoQuestionList.answer_value}"/>
        </div>
        <jsp:scriptlet>
          validateStr += "sel_"+ pageContext.getAttribute("questionId").toString() + ",";
        </jsp:scriptlet>
        </c:if>
        
        </div>        
       </c:forEach>
       <p class="qler1 mb20" id="bsinfoErr" style="display:none;"></p>
       <input type="hidden" id="qusArr" value="<%=questionIds%>"/>
       <input type="hidden" id="validateStr" value="<%=validateStr%>"/>       

       <div class="btn_cnt pt-40">
        <div class="fr">
								<div class="btn_cmps fl">
									<div class="reltv fl">
										<label class="cnfrm fl">
                    <%if("C".equalsIgnoreCase(formStatusId)){%>
                      <%--<script>markAsComplete('C');</script>--%>
									    <input id="markAsComFlagId" onclick="markAsCompBasicInfo('1', '2', 'I');" type="checkbox" disabled="disabled" checked>
									  <%}else{%>
                      <input id="markAsComFlagId" onclick="markAsCompBasicInfo('1', '2', 'I');" type="checkbox" >
                    <%}%>
                    <span class="checkmark grn"></span> <span class="bn bn-grn grn_txt"><span class="macomp">Mark As Complete</span><span class="frmcomp">Completed</span></span> 
									 </label>
								  </div>
								</div>
                <input type="hidden" id="action" value=""/>
          <a id="basicInfoBtn" onclick="validateBasicInfo('1', '2', 'I');" class="fr bton">SAVE AND CONTINUE <i class="fa fa-long-arrow-right"></i></a>
        </div>
         <jsp:include page="/jsp/whatunigo/include/includeBackLink.jsp"/> 
        </div>
      </form>
   
  </c:if>
        <script>preSelect();enableMarkComplete('', 'onload');changeBtn('4');</script>
    </div>
    </div>
    </div>
</section>