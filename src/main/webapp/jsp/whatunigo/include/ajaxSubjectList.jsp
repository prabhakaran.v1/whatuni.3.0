<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${not empty requestScope.ajaxSubjectList}">
  <c:forEach items="${requestScope.ajaxSubjectList}" var="ajaxList">
    <div id="${ajaxList.subject_id}" onclick="setAjaxSubjectVal(this)" class="optionDiv">
        <span class="ajx_sub" id="SUB_${ajaxList.subject_id}">
          <span class="ajx_rt"><span class="ajx_txt">${ajaxList.subject_desc}</span></span>
        </span>
      </div>
  </c:forEach>
</c:if>