<section class="cmm_cnt">
    <div class="cong_cnt"><h1>Congratulations!</h1></div>
    <div class="cmm_col_12 chse_cnt cngs_cnt">
      <div class="cmm_wrap">
        <div class="cmm_row">
        <!-- Uni List -->
         <div class="cmm_unilst">
          <div class="fl uni-logo col_lft">
            <a href="#"><img src="https://mtest.whatuni.com/commimg/myhotcourses/institution/myhc_247531.jpg" alt=""></a>
          </div>
          <div class="fl col_rgt">
            <h3 class="un-tit"><a href="University of Nottingham">University of Nottingham</a></h3>
            <h6 class="sub-tit"><a href="Psychology">Psychology</a></h6>
            <div class="fx">
              <div class="fx-bx">
                <h5>Study mode</h5>
                <h6>Full Time</h6>
              </div>
              <div class="fx-bx">
                <h5>Study duration</h5>
                <h6>3 years</h6>
              </div>
              <div class="fx-bx">
                <h5>UCAS tariff points 
                  <span class="hlp tool_tip"><i class="fa fa-question-circle"></i>
                    <span class="cmp hlp-tt">
                      <div class="hdf5"></div>
                      <div>We are showing the minimum and maximum UCAS points scores that the institution has listed for all qualifications</div> 
                      <div class="line"></div>
                    </span>
                  </span>
                </h5>
                <h6>120</h6>
              </div>
            </div>
          </div>
         </div>  
        <!-- Uni List -->
        <div class="cnf_off">
          <p>You have confirmed your place at the University of Nottingham.</p>
          <p>They will be in touch with you soon to discuss your next steps.</p>
          <p>Share the excitement with your friends!</p>
          <div class="sgn">
            <div class="so-btn">
              <a class="btn1 fbbtn" onclick="loginInfoFacebook('fblogin')" title="Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK</a>
              <a class="btn1 twbtn" onclick="loginInfoFacebook('fblogin')" title="Twitter"><i class="fa fa-twitter fa-1_5x"></i>TWITTER</a> 
            </div>
            <p class="qler1 valid_err" id="fblbregistration_error" style="display: none;"></p> 
          </div>
        </div> 
    </div>
    </div>
    </div>  
<jsp:include page="/jsp/whatunigo/include/articleSection.jsp"/>     

</section>