<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants" %>
 <%        
    String fbUserIdprep = request.getAttribute("fbUserIdprep") != null && !("undefined").equals(request.getAttribute("fbUserIdprep")) ? (String)request.getAttribute("fbUserIdprep") : "";
    String courseId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("COURSE_ID"))) ? (String)request.getAttribute("COURSE_ID") : "";
    String opportunityId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("OPPORTUNITY_ID"))) ? (String)request.getAttribute("OPPORTUNITY_ID") : "";
    String queryString = "&courseId=" + courseId + "&opportunityId=" + opportunityId;
    String collegeName = (String)request.getAttribute("hitbox_college_name");
    String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(collegeName);
    String collegeId = (String)request.getAttribute("COLLEGE_ID");
    String refererURL = (request.getAttribute("REFERER_URL") != null) ? (String)request.getAttribute("REFERER_URL") : "";
  %>
  <body>
    <header class="md clipart cmm_hdr">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
             <jsp:include page="/jsp/common/wuHeader.jsp"></jsp:include>             
          </div>
        </div>
      </div>
    </header>
    <section class="cmm_cnt">
      <%-- Timer Sectio --%>
      <%--<jsp:include page="/jsp/whatunigo/include/timer.jsp"></jsp:include> --%>
      <%-- Timer Sectio --%>
      <div class="cmm_col_12 chse_cnt no_topsec cmm_sgnup cmmsgn_up">
        <div class="cmm_wrap">
          <div class="cmm_row">
            <%-- Uni List --%>
            <jsp:include page="/jsp/whatunigo/include/applyNowCourseDetails.jsp">
              <jsp:param name="pageName" value="wugoSignUpPage"/>
            </jsp:include>
            <%-- Uni List --%>
            
            <%-- SignUp --%>
            <form class="fl basic_inf pers_det sgn_up">
              <div class="fl_w100 qua_cnf alr_hv">
                <div class="sgalai">
                  <p class="comptx"><span class="alrdy_acc">Already have an account?</span> <a href="<%=GlobalConstants.CMMT_SIGN_IN_URL + queryString%>" title="sign in" class="link_blue">sign in</a></p>
                </div>
               </div> 
              <div class="pers_lst pt-20">                
                <fieldset id="firstName_fieldset" class="fl tx-bx">
                  <input type="text" maxlength="40" name="firstName" onkeypress="javascript:if(event.keyCode==13){return valCmmtUserRegister()}" id="firstName" onblur="hideTooltip('firstNameYText');registratonValidations(this.id);" onfocus="showTooltip('firstNameYText');"class="frm-ele">
                  <label for="firstName" class="lbco">First name*</label>
                  <span class="cmp" id="firstNameYText" style="display:none;"> 
                    <div class="hdf5"></div>
                    <div>We'd like this information so we don't call you by the wrong name. That'd be kinda rude.</div>
                    <div class="linear"></div>
                  </span>
                  <p class="fail-msg" id="firstName_error" style="display:none;"></p>
                </fieldset>
                <fieldset id="surName_fieldset" class="fr tx-bx">
                  <input type="text" name="surName" id="surName" onkeypress="javascript:if(event.keyCode==13){return valCmmtUserRegister()}" maxlength="40" onblur="registratonValidations(this.id);" class="frm-ele">
                  <label for="surName" class="lbco">Last name*</label>
                  <p class="fail-msg" id="surName_error" style="display:none;"></p>
                </fieldset>
              </div>
              <div class="pers_lst pt-20">
                <fieldset class="tx-bx w-100" id="emailAddress_fieldset">
                  <%--<input type="text" name="email" class="frm-ele">--%>
                  <input type="text" name="email" maxlength="120" value="" onkeydown="hideEmailDropdown(event, 'autoEmailId');" onblur="hideTooltip('emailAddressYText');" onfocus="showTooltip('emailAddressYText');" id="email" autocomplete="off" class="frm-ele">
                  <c:if test="${not empty requestScope.fbUserIdprep}">
                    <html:hidden property="fbUserId" styleId="fbUserId" styleClass="ql-inpt" value="<%=fbUserIdprep%>" />
                  </c:if>
                  <label for="email" class="lbco">Email*</label>
                  <span class="cmp" id="emailAddressYText" style="display:none;"> 
                    <div class="hdf5"></div>
                    <div>Tell us your email and we'll reward you with...an email.</div>
                    <div class="linear"></div>
                  </span>
                  <p class="fail-msg" id="registerErrMsg" style="display:none;"></p>
                </fieldset>
              </div>
              <div class="pers_lst pt-20">
                <fieldset id="password_fieldset" class="tx-bx w-100 pwd_feld">
                  <input type="password" maxlength="20" onkeypress="javascript:if(event.keyCode==13){return valCmmtUserRegister()}" onblur="registratonValidations(this.id);" name="password" id="password" class="frm-ele">
                  <label for="password" class="lbco">Password*</label>
                  <%--<span class="pwd_eye"><a href="javascript:void(0);" onmousedown="showPass('eyeIcon', 'password');" onmouseup="hidePass('eyeIcon', 'password');"><i id="eyeIcon" class="fa fa-eye" aria-hidden="true"></i></a></span>--%>
                  <span class="pwd_eye"><a href="javascript:void(0);" onclick="showPassword('eyeIcon', 'password');"><i id="eyeIcon" class="fa fa-eye" aria-hidden="true"></i></a></span>
                  <p class="fail-msg" id="password_error" style="display:none;"></p>
                </fieldset>
              </div>              
              <%--
              <div class="pers_lst">
                <h3 class="fnt_lbd fnt20 mb20">Stay up to date by email <span class="fnt_lrg gry_txt">(optional)</span></h3>
                <div class="btn_chk">
                  <span class="chk_btn">
                  <input type="checkbox" value="" class="chkbx1" id="marketingEmailRegFlag" onclick="setRegFlags(this.id);">
                  <span class="chk_mark"></span>
                  </span>
                  <p><label for="marketingEmailRegFlag" class="fnt_lbd chkbx_100">Newsletters <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                  Emails from us providing you the latest university news, tips and guides</p>
                </div>
                <div class="btn_chk">
                  <span class="chk_btn">
                  <input type="checkbox" value="" class="chkbx1" id="solusEmailRegFlag" onclick="setRegFlags(this.id);">
                  <span class="chk_mark"></span>
                  </span>
                  <p><label for="solusEmailRegFlag" class="fnt_lbd chkbx_100"> University updates <span class="cmrk_tclr">(Tick to opt in)</span> </label> <br>
                 <spring:message code="wuni.solus.flag.text"/></p>
                </div>
                <div class="btn_chk">
                <span class="chk_btn">
                <input type="checkbox" value="" class="chkbx1" id="surveyEmailRegFlag" onclick="setRegFlags(this.id);">
                <span class="chk_mark"></span>
                </span>
                <p><label for="surveyEmailRegFlag" class="fnt_lbd chkbx_100">Surveys <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                <spring:message code="wuni.survey.flag.text"/></p>
                </div>                
              </div>  --%>            
              <%-- Social login --%>             
              <jsp:include page="/jsp/whatunigo/include/socialMediaLogin.jsp">
                <jsp:param name="PAGE_NAME" value="sign up" />
              </jsp:include>             
              <%-- Social login --%>
              
              <%-- Checkbox Confirmation --%>
              <div class="fl qua_cnf">
                <div class="btn_chk">
                  <span class="chk_btn">
                  <input type="checkbox" name="termsc" value="N" id="termsc">
                  <span class="checkmark grey"></span>
                  </span>
                  <p><label for="termsc" class="chkbx_100">I confirm I'm over 13 and agree to the <a href="javascript:void(0);" onclick="showTermsConditionPopup();">terms and conditions</a> and <a href="javascript:void(0);" onclick="showPrivacyPolicyPopup();">privacy notice</a>, and to become a member of the Whatuni community</label></p>
                  <p class="qler1" id="termsc_error" style="display:none;"></p>
                </div>
                <p class="cnfrm f-16" style="display:none">The requirement for this course is at least <span class="dotd">120</span> UCAS points</p>
              </div>
              <%-- Checkbox Confirmation --%>
              <%--Hidden inputs--%>
              <input type="hidden" id="emailValidateFlag" value="true"/>
              <%--Hidden inputs--%>
              <div class="btn_cnt pt-40">            
                <div class="fr">
                  <a href="javascript:void(0)" onclick="valCmmtUserRegister();" class="fr bton">SIGN UP <i class="fa fa-long-arrow-right"></i></a>
                </div>
                <p id="loadinggifreg" class="fr pt-10 clear_rht" style="display:none;" ><span id="loadgif"><img title="" alt="Loading" src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/indicator1.gif", 0)%>" class="loading"/></span></p> 
              </div>
              <input type="hidden" id="screenwidth" name="screenwidth" value=""/>
            </form>
            <%-- SignUp --%>
          </div>
        </div>
      </div> 
    </section>
    <%--<input type="hidden" id="refererUrl" name="refererUrl" value="<%=(request.getHeader("referer") !=null ? request.getHeader("referer") : browserip+"/degrees/home.html")%>" />--%>
    <input type="hidden" id="refererUrl" value="<%=refererURL%>"/>
    <input type="hidden" id="collegeName" value="<%=gaCollegeName%>"/>
    <input type="hidden" id="collegeId" value="<%=collegeId%>"/>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
  </body>