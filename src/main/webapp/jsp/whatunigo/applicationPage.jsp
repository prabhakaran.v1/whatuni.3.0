<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>
<%
  String browserip = GlobalConstants.WHATUNI_SCHEME_NAME + new CommonUtil().getProperties().getString("review.autocomplete.ip");
  String facebookLoginJSName = CommonUtil.getResourceMessage("wuni.facebook.login.js", null);
  String collegeName = (String)request.getAttribute("hitbox_college_name");
  String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(collegeName);
  String collegeId = (String)request.getAttribute("hitbox_college_id");
  String userStatusCode = (String)request.getAttribute("userStatusCode");
  String APPLY_NOW_COURSE_ID = (String)request.getAttribute("APPLY_NOW_COURSE_ID");
  String APPLY_NOW_OPPORTUNITY_ID = (String)request.getAttribute("APPLY_NOW_OPPORTUNITY_ID");
  String REFERER_URL = (String)request.getAttribute("REFERER_URL");
  String C_END_TIME_T1 = (String)request.getAttribute("C_END_TIME_T1");
%>  

 <body>
    <div id="loadingImg" class="cmm_ldericn" style="display:none;"><img alt="wugo-loading-image" src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif",0)%>"></div>
    <header class="clipart cmm_hdr">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>  
    </header>
    <script type="text/javascript">
  jq( document ).ready(function() {
  //clearInterval(setIrlId);
  var userStatusCode = jq('#userStatusCode').val();
  stopinterval();
  if(userStatusCode == 1){
  startTimer1('APPLICATION_TIMER_1');
  }
   if(userStatusCode == 2){
   startTimer1('TIMER_2');
    jq("#applicationPage").addClass("sgt_off");
    }
    if(userStatusCode == 0){
    callLightBox('0##SPLIT##APPLICATION_ERROR_MESSAGE_LIGHTBOX');
    }
   });
   function changeContactButtonInCancelOffer(hotlineNumber,anchorId) {
     jq("#" + anchorId).attr("title", hotlineNumber);
     jq("#" + anchorId).html("<i class='fa fa-phone' aria-hidden='true'></"+ "i>"+hotlineNumber);
     var mobileFlag = jq("#check_mobile_hidden").val();
     cancelOfferCallGAlog();
     if(mobileFlag == 'true'){
       setTimeout(function(){
     location.href='tel:'+ hotlineNumber;
    },1000)
     }
   }
</script>
    <div id="middleContent"><jsp:include page="/jsp/whatunigo/include/applicationPageUniInfo.jsp"/></div>
    <input type="hidden" id="offerName" value="ACCEPTING_OFFER"/>
    <%--<input type="hidden" id="refererUrl" name="refererUrl" value="<%=(request.getHeader("referer") !=null ? request.getHeader("referer") : browserip+"/degrees/home.html")%>" />--%>
    <input type="hidden" id="refererUrl" value="<%=REFERER_URL%>"/> 
    <input type="hidden" id="applyNowCourseId" name="applyNowCourseId" value="<%=APPLY_NOW_COURSE_ID%>"/>
    <input type="hidden" id="applyNowOpportunityId" name="applyNowOpportunityId" value="<%=APPLY_NOW_OPPORTUNITY_ID%>"/>
    <input type="hidden" id="collegeId" name="collegeId" value="<%=collegeId%>"/>
   <%-- <input type="hidden" id="userIdT1" name="userIdT1" value="${requestScope.C_USER_ID_T1}"/>
    <input type="hidden" id="courseIdT1" name="courseIdT1" value="${requestScope.C_COURSE_ID_T1}"/>
    <input type="hidden" id="opportunityIdT1" name="opportunityIdT1" value="${requestScope.C_OPPORTUNITY_ID_T1}"/>
    <input type="hidden" id="timerIdT1" name="timerIdT1" value="${requestScope.C_TIMER_ID_T1}"/>
    <input type="hidden" id="startTimeT1" name="startTimeT1" value="${requestScope.C_START_TIME_T1}"/>--%> 
    <input type="hidden" id="endTimeT1" name="endTimeT1" value="<%=C_END_TIME_T1%>"/>    
    <input type="hidden" id="userStatusCode" value="<%=userStatusCode%>"/>
    <input type="hidden" id="statusT1" name="statusT1" value="N"/>
    <input type="hidden" id="collegeName" value="<%=gaCollegeName%>"/>
    <input type="hidden" id="pageNameHidden" value="Application Page"/>
    <jsp:include page="/jsp/common/wuFooter.jsp" />    
</body>
