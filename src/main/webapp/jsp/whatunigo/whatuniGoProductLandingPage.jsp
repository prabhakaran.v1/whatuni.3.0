<%@ page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction" autoFlush="true" %>
<body>
<% 
CommonFunction common = new CommonFunction();
String clearingonoff = common.getWUSysVarValue("CLEARING_ON_OFF");
String videoUrl = common.getWUSysVarValue("WUNI_GO_VIDEO_URL");
%>
<%-- GTM script included under body tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
  <jsp:param name="PLACE_TO_INCLUDE" value="BODY" />
</jsp:include>
<%--Added for cookie policy pop-up by Hema.S on 31.07.2018_rel--%>
<jsp:include page="/jsp/home/cookiePopup.jsp"/>
<div class="cont_show">
  <jsp:include page="/jsp/app/include/includeALPHeader.jsp">
    <jsp:param name="fromPage" value="productPage"/>
  </jsp:include>
  <div id="fullpage" class="sect_wrap inn_cont">            
  <!-- // Third section/2 -->
    <div class="section section1" id="slide1">
      <div class="section-inner">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
              <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6 fl_right img_fld cs_sub_cont">
                  <div class="utu_vid">
                    <iframe src="<%=videoUrl%>" style="position:absolute;width:100%;height:100%;left:0" width="641" height="360" frameborder="0" allowfullscreen></iframe>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-6 col-lg-5 fl_left cs_sub_cont">
                  <div class="jumbotron">
                    <h2>Whatuni Go! Taking the stress out of Clearing</h2>
                    <div class="tinfo">
                      <p>Use our FREE Whatuni Go tool to quickly find and secure the university place that’s perfect for you - all at the touch of a button.</p> 			
                      <p>No calls, no drama. Just your future, a whole lot clearer. </p>
                      <p><a href="/university-clearing/" class="obtn">TRY NOW</a></p>
                    </div>                                            
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- click to down -->
      <div class="hfcr" href="#slide3" moveto=2>
        <div class="hfcr-mo-txt">
          <div class="hfcr-txt">MORE</div>
          <div class="hfcr-txt2">
            <img src="<%=CommonUtil.getImgPath("/wu-cont/images/wug-white-arrw.png",0)%>">
          </div>
        </div>
      </div>
      <!-- click to down -->
    </div>
    <!-- // Third section/2 -->
    <div class="section section2" id="slide2">
      <div class="section-inner">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-5 img_fld fl_left cs_sub_cont animated fadeInDown"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/wugo_lp_ipad1.png",0)%>" class="img-responsive" width="" alt="Search image" /> </div>
                                    <div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 fl_right cs_sub_cont">
                                        <div class="jumbotron"> 
                                            <h2>What is Whatuni GO?</h2>
                                            <p>Whether you’re going through Clearing, Adjustment, or simply making a late entry, you can use Whatuni Go to find the right university course for you.</p>
                                            <p>And the genius bit? You could apply to your favourite course online and get an Offer in Principle from the university right there and then – in as little as 10 minutes!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<!-- click to down -->
				<div class="hfcr" href="#slide4" moveto=3>
				  <div class="hfcr-mo-txt">
					<div class="hfcr-txt">
					  MORE
					</div>
					<div class="hfcr-txt2">
						<img src="<%=CommonUtil.getImgPath("/wu-cont/images/wug-white-arrw.png",0)%>">
					</div>
				  </div>
				</div>
				<!-- click to down -->
            </div>
            <div class="section section3" id="slide3">
                <div class="section-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn ">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 fl_left cs_sub_cont animated">
                                        <div class="jumbotron"> 
                                            <h2>How does Whatuni Go work?</h2>
                                            <p>Simply enter your grades and uni preferences and we’ll show you a selection of matching Clearing courses for you to browse through and compare.</p> 
                                            <p>See the GO APPLY symbol? This means you can apply and receive an offer in principle from your chosen uni online, right there and then. No need to pick up the phone.</p>
                                        </div>                                        
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 fl_right img_fld cs_sub_cont animated"> <img src="<%=CommonUtil.getImgPath("/wu-cont/images/wugo_lp_iphone1.png",0)%>" class="img-responsive" width="" alt="Responsive image"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<!-- click to down -->
				<div class="hfcr" href="#slide5" moveto=4>
				  <div class="hfcr-mo-txt">
					<div class="hfcr-txt">
					  MORE
					</div>
					<div class="hfcr-txt2">
						<img src="<%=CommonUtil.getImgPath("/wu-cont/images/wug-white-arrw.png",0)%>">
					</div>
				  </div>
				</div>
				<!-- click to down -->
            </div>            
            <div class="section section4" id="slide4">
                <div class="section-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 col-lg-5 fl_right cs_sub_cont">
                                        <div class="jumbotron"> 
                                            <h2>Making the right choice for you...</h2>
                                            <p>To help you pick the perfect uni for you, we have both university and course profile pages, giving you full information on course modules, learning facilities, accommodation and much more.</p>
                                            <p>And don’t forget to read our real-life student reviews, to get the rundown on what life’s really like at your chosen university.</p>
                                        </div>                                        
                                    </div>
                                    <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6 fl_left img_fld cs_sub_cont"> <img src="<%=CommonUtil.getImgPath("/wu-cont/images/wugo_lp_laptop.png",0)%>" class="img-responsive" width="" alt="Responsive image"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<!-- click to down -->
				<div class="hfcr" href="#slide6" moveto=5>
				  <div class="hfcr-mo-txt">
					<div class="hfcr-txt">
					  MORE
					</div>
					<div class="hfcr-txt2">
						<img src="<%=CommonUtil.getImgPath("/wu-cont/images/wug-white-arrw.png",0)%>">
					</div>
				  </div>
				</div>
				<!-- click to down -->
            </div>
            <div class="section section5" id="slide5">
                <div class="section-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-5 img_fld fl_right cs_sub_cont"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/wugo_lp_ipad2.png",0)%>" class="img-responsive" width="" alt="A unique profile image"> </div>
                                    <div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 fl_left cs_sub_cont">
                                        <div class="jumbotron"> 
                                            <h2>How do I apply?</h2>
                                            <p>Once you’ve chosen which university and course you want to apply for, simply hit the GO APPLY button and answer some simple questions.  Your offer in principle will then be confirmed.</p>
                                            <p>Yes, it really is that easy. Give it a try and see for yourself...</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<!-- click to down -->
				<div class="hfcr" href="#slide7" moveto=6>
				  <div class="hfcr-mo-txt">
					<div class="hfcr-txt">
					  MORE
					</div>
					<div class="hfcr-txt2">
						<img src="<%=CommonUtil.getImgPath("/wu-cont/images/wug-white-arrw.png",0)%>">
					</div>
				  </div>
				</div>
				<!-- click to down -->
            </div>
            <div class="section section6" id="slide6">
                <div class="section-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6 fl_left img_fld cs_sub_cont"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/wugo_lp_macbook.png",0)%>" class="img-responsive" width="" alt="Shortlist image"> </div>
                                    <div class="col-xs-12 col-sm-8 col-md-5 col-lg-5 fl_right cs_sub_cont">
                                        <div class="jumbotron"> 
                                            <h2>What's an ‘Offer in Principle’?</h2>
                                            <p>It means the details you’ve provided fulfil the university’s entry requirements, and they are happy to accept you as a student in September - subject to their own final checks.</p>
                                            <p>They’ll get in touch with you soon after you’ve applied to discuss your application and confirm your offer and start date. And then? Well, it's time to start celebrating!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<!-- click to down -->
				<div class="hfcr" href="#slide8" moveto=7>
				  <div class="hfcr-mo-txt">
					<div class="hfcr-txt">
					  MORE
					</div>
					<div class="hfcr-txt2">
						<img src="<%=CommonUtil.getImgPath("/wu-cont/images/wug-white-arrw.png",0)%>">
					</div>
				  </div>
				</div>
				<!-- click to down -->
            </div>
            <div class="section section7" id="slide7">
                <div class="section-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 img_fld fl_right cs_sub_cont"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/wugo_lp_iphone2.png",0)%>" class="img-responsive" width="" alt="Responsive image"> </div>
                                    <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7 fl_left cs_sub_cont">
                                        <div class="jumbotron"> 
                                            <h2>How many unis are on Whatuni Go?</h2>
                                            <p>We've teamed up with a hand-picked selection of awesome universities on Whatuni Go 2019, and they have thousands of places that you can apply to online through us.</p>
                                            <p>Can't find your perfect match through Whatuni Go? No worries... you can still use Whatuni's full Clearing service to find your perfect course. You'll just have to go old school and pick up the phone. How very 2018 of you!</p>
                                            <p><a href="/degrees/wugo/subject/search.html" class="obtn">SEARCH</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<!-- click to down -->
				<div class="hfcr" href="#slide9" moveto=8>
				  <div class="hfcr-mo-txt">
					<div class="hfcr-txt">
					  MORE
					</div>
					<div class="hfcr-txt2">
						<img src="<%=CommonUtil.getImgPath("/wu-cont/images/wug-white-arrw.png",0)%>">
					</div>
				  </div>
				</div>
				<!-- click to down -->
            </div>
            <div class="section section8" id="slide8">
                <div class="section-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 img_fld fl_left cs_sub_cont"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/wugo_lp_iphone3.png",0)%>" class="img-responsive" width="" alt="Responsive image"> </div>
                                    <div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 fl_right cs_sub_cont">
                                        <div class="jumbotron"> 
                                            <h2>Whatuni Go FAQs</h2>
                                            <p>Got another question about Whatuni Go? We’ve got all the answers.</p>
                                            <p><a href="https://www.whatuni.com/advice/clearing/whatuni-go-faqs/78268/" class="obtn">SEARCH FAQS</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
              </div>
        <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/common/commonFunctions_wu572.js"></script>
       <script type="text/javascript">  
        var jq = jQuery.noConflict();  
        jq(function() {
		  jq('div[href*=#]').on('click', function(e) {
			e.preventDefault();
			jq.fn.fullpage.moveTo((jq(this).attr('moveto')),0);
		  });
		});  
       </script>
        <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
        <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />  
        <script type="text/javascript">    
          checkcookie();    
       </script>
  </body>