<%@page import="WUI.utilities.CommonUtil" autoFlush="true" %>

<%
  String collegeName = (String)request.getAttribute("hitbox_college_name");
  String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(collegeName);
  String collegeId = (String)request.getAttribute("COLLEGE_ID");
  String refererURL = (request.getAttribute("REFERER_URL") != null) ? (String)request.getAttribute("REFERER_URL") : "";
%>

<body>
    <div id="loadingImg" class="cmm_ldericn" style="display:none;"><img alt="wugo-loading-image" src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif",0)%>"></div>
    <header class="clipart cmm_hdr">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>  
    </header>
    <div id="middleContent">
      <div class="brd_crumb timer_sec">
        <div class="cmm_col_12">
          <div class="cmm_wrap">          
             <jsp:include page="/jsp/whatunigo/include/timer.jsp"></jsp:include> 
          </div>
        </div>
      </div>   
    
      <jsp:include page="/jsp/whatunigo/include/newUserQualificationEntryPage.jsp">
         <jsp:param name="offerName" value="PROVISIONAL_OFFER" />       
      </jsp:include>    
    </div>
    <input type="hidden" id="offerName" value="PROVISIONAL_OFFER"/>
    <input type="hidden" id="refererUrl" value="<%=refererURL%>"/>
    <input type="hidden" id="applyNowCourseId" name="applyNowCourseId" value="${requestScope.APPLY_NOW_COURSE_ID}"/>
    <input type="hidden" id="applyNowOpportunityId" name="applyNowOpportunityId" value="${requestScope.APPLY_NOW_OPPORTUNITY_ID}"/>
    <input type="hidden" id="endTimeT1" name="endTimeT1" value="${requestScope.C_END_TIME_T1}"/>        
    <input type="hidden" id="statusT1" name="statusT1" value="N"/>        
    <input type="hidden" id="collegeName" value="<%=gaCollegeName%>"/>
    <input type="hidden" id="collegeId" value="<%=collegeId%>"/>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
</body> 