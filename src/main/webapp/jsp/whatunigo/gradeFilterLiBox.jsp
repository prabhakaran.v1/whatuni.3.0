<%@page import= "WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator"%>

<% String valErrorCode = (String)request.getParameter("valerror");
   String qualification = request.getParameter("qualification");
   String qualification1 = request.getParameter("qualification1");
   String qualification2 = request.getParameter("qualification2");
   String qualification3 = request.getParameter("qualification3");
   String courseCount = request.getParameter("coursecount");
   String onClickMethod = "saveAndContinue()";
   String pageName = request.getParameter("pageName");
%> 
<div class="rvbx_shdw"></div>
  <div class="lbxm_wrp">  
   <div class="ext-cont svap_cnt cedwo_cnt cmm_vwcrse">
    <div class="revcls ext-cls"><a href="javascript:void(0);" onclick="closeLigBox();"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/lbx_clse_icon.svg" alt="Close"> </a></div>
     <div class="rev_lst ext">
       <div class="cedwo_wrap">
         <div class="ced_wgo"><img src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/wugo_logo.svg" title="Whatuni Go logo" alt="Whatuni GO Logo"></div>
         <div class="ced_desc">
          <%if("2".equalsIgnoreCase(valErrorCode)) {onClickMethod = "saveAndContinue()";%>
         <p>Unfortunately we are currently unable to offer direct applications to Clearing courses for students who have studied <%=qualification%>.</p>
         <p>However don't worry, we can still show you all the info you need to know about the courses available to you, and you can then call the University directly to gain your place on your chosen Clearing course.</p>
         <%} else if("0".equalsIgnoreCase(valErrorCode)) {onClickMethod = "saveAndContinue()";%>
         <p>Unfortunately we are currently unable to offer direct applications to Clearing courses for students who have studied a combination of <%=qualification1%> <%=qualification2%> <%=qualification3%>.</p>
         <p>However don't worry, we can still show you all the info you need to know about the courses available to you, and you can then call the University directly to gain your place on your chosen Clearing course.</p>
         <%} else { onClickMethod = "saveAndValidate()";
             if("1".equals(courseCount)){%>
                <p class="ced_ptop">We've matched <%=courseCount%> course based on your qualifications.</p>
            <% }else if("subjectLandingPage".equalsIgnoreCase(pageName) && "0".equals(courseCount)){ %>
                <p class="ced_ptop">We've matched <%=courseCount%> courses based on your search criteria.</p>
             <% }else{ %>
                <p class="ced_ptop">We've matched <%=courseCount%> courses based on your qualifications.</p>
             <% } %>
         <%}%>
        </div>    
       </div>
       <div class="btn_cnt insuf">
       <div class="btnsec_row fl_w100">
       <%if(GenericValidator.isBlankOrNull(valErrorCode)) {%>
         <%if(!"0".equalsIgnoreCase(courseCount)) {
           if("1".equals(courseCount)){%>    
             <a onclick="<%=onClickMethod%>;" class="fr bton">VIEW COURSE</a>
           <%} else {%>
             <a onclick="<%=onClickMethod%>;" class="fr bton">VIEW COURSES</a>
           <%}
        }%>
       <%} else {%>
       <a onclick="<%=onClickMethod%>;" class="fl bton">CLOSE</a>
       <%}%>
       </div>
       </div>
     </div>
   </div>
</div>