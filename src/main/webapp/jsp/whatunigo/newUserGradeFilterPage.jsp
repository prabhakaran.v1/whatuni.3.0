<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants" %>
<%
 int loop = 0;
  int count = 0, subCount = 0, subLen = 0; 
  String qualSub = "";
  String qualId = ""; 
  String actionName = (!GenericValidator.isBlankOrNull((String)request.getAttribute("actionName"))) ? (String)request.getAttribute("actionName") : "EDIT";//PROV_QUAL_EDIT
  String reviewFlag = "N";
  String ajaxActionName = "QUAL_SUB_AJAX";  
  String removeBtnFlag = "display:block";
   String subject = !GenericValidator.isBlankOrNull((String)request.getAttribute("subject")) ? (String)request.getAttribute("subject") : "";
  String selectFlag = "";
  String gcseSelectFlag = "";
  String addQualStyle = "display:block";
  String addQualBtnStyle = "display:block";
  String gcseSelOption = ": 9-1";
  String removeLink =   CommonUtil.getImgPath("/wu-cont/images/cmm_close_icon.svg",0);
  String qualifications = "";
  String selectQualId ="";
  String selectQualSubLen = "";
 
  String formStatusId = (!GenericValidator.isBlankOrNull((String)request.getAttribute("pageStatus"))) ? (String)request.getAttribute("pageStatus") : "";
  
  String location = !GenericValidator.isBlankOrNull((String)request.getAttribute("location")) ? (String)request.getAttribute("location") : "";
 
  String newGCSESelectFlag = "";
  String oldGCSESelectFlag = "";
  
   String q =!GenericValidator.isBlankOrNull((String)request.getAttribute("q")) ? (String)request.getAttribute("q") : "";
   String university = !GenericValidator.isBlankOrNull((String)request.getAttribute("university")) ? (String)request.getAttribute("university") : "";
   String qualCode = !GenericValidator.isBlankOrNull((String)request.getAttribute("qualCode")) ? (String)request.getAttribute("qualCode") : "";
   String referralUrl = !GenericValidator.isBlankOrNull((String)request.getAttribute("referralUrl")) ? (String)request.getAttribute("referralUrl") : "";
   String userId = (String)request.getAttribute("userId");
   String subjectSessionId = (String)request.getAttribute("subjectSessionId");
   String ucaspoint = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasPoint")) ? (String)request.getAttribute("ucasPoint") : "0";
   String ucasMaxPoints = (String)request.getAttribute("ucasMaxPoints");
   String styleFlag = "display:block"; String styleFlag2 = "display:none"; String styleFlag3 = "display:none"; 
  
   String score = !GenericValidator.isBlankOrNull((String)request.getAttribute("score")) ? (String)request.getAttribute("score") : "";
   String minScore = "0";
   String maxScore = ucaspoint;
 
  int addSubLimit = 6;
  String removeQualStyle = "display:none";
  int userQualListSize = (!GenericValidator.isBlankOrNull((String)request.getAttribute("userQualSize"))) ? Integer.parseInt((String)request.getAttribute("userQualSize")) : 0;
  String addQualId = "level_3_add_qualif";
  String callTypeFlag = "";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("score"))) {
     String[] scoreArr = score.split(",");
     minScore = scoreArr[0];
     maxScore = scoreArr[1];
   }
%>
<section class="cmm_cnt">
  <div class="brd_crumb timer_sec">
    <div class="cmm_col_12">
      <div class="cmm_wrap">
        <div class="brc_cnt lft fl">
          <div class="brc_row1">
            <span class="bc_hdtxt">Qualifications</span><span class="bc_stpno">Step 2 of 2</span>
          </div>
          <div class="brc_row2">
            <ul>
              <li class="stp_cmplt disbld"><a href="javascript:void(0);"><span class="bcno">1</span></a></li>
              <li class="stp_pro disbld"><a href="javascript:void(0);"><span class="bcno">2</span></a></li>
            </ul>
          </div>
        </div>
        <div class="brc_cnt rgt fr">
          <div class="sub_btn"><a onclick="skipAndViewResults();" title="Skip to view all courses">SKIP TO VIEW ALL COURSES <i class="fa fa-long-arrow-right"></i></a></div>  
      </div>
      </div>
    </div>
  </div>
  <div class="cmm_col_12 binf_cnt srqual_mn">
    <div class="cmm_wrap">
      <div class="cmm_row">
        <div class="cmm_unilst qual_cnt">
          <p class="ql_dec">UCAS points is only the first level of requirements. Some courses require specific grade combinations and specific subjects. So make sure you enter all of your qualifications correctly to guarantee the best match.</p>
        </div>
        <!-- Uni List -->
        <!-- Contact details -->
        <c:if test="${not empty requestScope.qualificationList}">
        <form class="fl basic_inf pers_det cont_det qualif slqualif" id="gradeForm">
          <!-- UCAS -->
          <div class="ucas_midcnt">
            <div id="ucas_calc">
              <div class="ucas_mid">
                <!--Qualification Repeat Field start 1-->
                
                  <div class="add_qualif">
                  <div class="ucas_refld">
                    <!--<h3 class="un-tit">Level 3</h3>-->
                    <!-- L3 Qualification Row  -->
                    <div class="l3q_rw">
                     <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" begin="0" end="2">
                      <c:set var="indexIValue" value="${i.index}"/>
                      <c:if test="${not empty qualList.entry_qual_id}">
                      <c:set var="selectQualId1" value="${qualList.entry_qual_id}"/>
                      <%
                        String selectQualId1 = pageContext.getAttribute("selectQualId1").toString(); 
                        selectQualId = selectQualId1;
                      %>
                      </c:if>
                      <c:if test="${empty qualList.entry_qual_id}">
                        <% selectQualId ="1";%>
                      </c:if>
                      <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue")));if(count > 0){addQualStyle = "display:none";}%>
                      <!-- Add Qualification & Subject 1-->		
                      <div class="adqual_rw" data-id=level_3_add_qualif id="level_3_qual_<%=count%>" style="<%=addQualStyle%>">
                        <div class="ucas_row">
                          <h5 class="cmn-tit txt-upr">Qualification</h5>
                          <fieldset class="ucas_fldrw">
                            <div class="remq_cnt">
                              <div class="ucas_selcnt">
                                <fieldset class="ucas_sbox">
                                  <c:set var="selectQualName" value="${qualList.entry_qualification}"/>
                                  <%String selectQualName = pageContext.getAttribute("selectQualName").toString(); %>
                                  <%if("0".equals(String.valueOf(pageContext.getAttribute("indexIValue")))){if("A - Levels".equalsIgnoreCase(selectQualName)){selectQualName="A Level";}qualifications = selectQualName ;}%>
                                  <span class="sbox_txt" id="qualspan_<%=count%>"><%=selectQualName%></span>
                                  <i class="fa fa-angle-down fa-lg"></i>
                                </fieldset>
                                <select name="qualification1" onchange="appendQualDiv(this, '', '<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>', 'new_entry_page');" id="qualsel_<%=count%>" class="ucas_slist">
                                  <option value="0">Please Select</option>
                                  <c:forEach var="qualListLevel3" items="${requestScope.qualificationList}">
                                   <jsp:scriptlet>selectFlag = "";if("".equals(selectQualId)){selectQualId="1";}</jsp:scriptlet>
                                   <c:set value="<%=selectQualId%>" var="selectQualId"/>
                                   <c:if test="${qualListLevel3.entry_qual_id eq selectQualId}">
                                     <jsp:scriptlet>selectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                   </c:if>
                                   <c:if test="${qualListLevel3.entry_qualification ne 'GCSE'}">
                                     <c:if test="${empty qualListLevel3.entry_qual_id}">
                                       <optgroup label="${qualListLevel3.entry_qualification}"></optgroup>
                                     </c:if>
                                     <c:if test="${not empty qualListLevel3.entry_qual_id}">
                                       <option value="${qualListLevel3.entry_qual_id}" <%=selectFlag%>>${qualListLevel3.entry_qualification} </option>
                                     </c:if>                                   
                                   </c:if>
                                  </c:forEach>
                                </select>
                                <c:forEach var="grdQualifications" items="${requestScope.qualificationList}">
                                 <c:if test="${grdQualifications.entry_qualification ne 'GCSE'}">                                
                                   <c:if test="${not empty grdQualifications.entry_qual_id}">
                                     <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                     <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}" name="" value="${grdQualifications.entry_grade}" />
                                   </c:if>
                                 </c:if>                               
                               </c:forEach>
                              </div>
                            </div>
                            <%if(0!= count){%>
                            <div class="add_fld">
                              <a id="1rSubRow3" onclick="javascript:removeQualification('level_3_qual_<%=count%>', <%=count-1%>, 'addQualBtn_<%=count%>');" class="btn_act1 bck f-14 pt-5">
                                <span class="hd-mob">Remove Qualification</span><!--<span class="hd-desk"><i class="fa fa-times"></i></span>-->
                              </a>
                            </div>
                            <%}%>                            
                          </fieldset>
                          <div class="err" id="qualSubId_error" style="display:none;"></div>
                        </div>
                        <div class="subgrd_fld" id="subGrdDiv<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>">
                        <div class="ucas_row grd_row" id="grdrow_<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>">
                        
                            <fieldset class="ucas_fldrw quagrd_tit">
                              <div class="ucas_tbox tx-bx">
                                <h5 class="cmn-tit txt-upr qual_sub">Subject</h5>
                              </div>
                              <div class="ucas_selcnt rsize_wdth">
                                <h5 class="cmn-tit txt-upr qual_grd">Grade</h5>
                              </div>
                            </fieldset>  
                            <c:if test="${not empty qualList.entry_subject}">   
                              <c:set var="selectQualSubLen1" value="${qualList.entry_subject}"/>
                              <%
                                String selectQualSubLen1 = pageContext.getAttribute("selectQualSubLen1").toString();
                                selectQualSubLen = selectQualSubLen1;
                              %>
                            </c:if>
                            <c:if test="${empty qualList.entry_subject}">
                              <%selectQualSubLen = "3";%>
                            </c:if>
                              
                            <c:forEach var="subjectList" items="${qualificationList}" varStatus="j" begin="0" end="<%=Integer.parseInt(selectQualSubLen)%>">
                              <c:set var="indexJValue" value="${j.index}"/>
                              <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue")));%>
                              <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
                                <fieldset class="ucas_fldrw">
                                  <div class="ucas_tbox w-390 tx-bx">
                                    <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="subIp_<%=subCount%>" onblur="getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="" class="frm-ele" autocomplete="off">															
                                    <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                                    <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                                    </div>
                                  </div>
                                  <div class="ucas_selcnt rsize_wdth">
                                    <div class="ucas_sbox mb-ht w-84" id="1grades1">
                                      <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">A*</span><i class="fa fa-angle-down fa-lg"></i>
                                    </div>
                                    <select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" id="qualGrd_<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>_<%=Integer.parseInt(pageContext.getAttribute("indexJValue").toString())%>"></select>
                                    <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=String.valueOf(pageContext.getAttribute("indexIValue"))%>_<%=String.valueOf(pageContext.getAttribute("indexJValue"))%>','${subjectList.entry_grade}');</script>                                    
                                    <input class="subjectArr" data-id="level_3" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_<%=selectQualId%>" value="" />                                
                                    <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_<%=selectQualId%>" value="A*" />
                                    <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_<%=selectQualId%>" value="<%=selectQualId%>" />
                                    <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_<%=selectQualId%>" value="<%=count+1%>" />
                                  </div>
                                  <%if(subCount == 0){
                                    removeBtnFlag = "display:none";
                                  }else{
                                    removeBtnFlag = "display:block";
                                  }
                                  if(subCount == 1){%>                                  
                                    <script>jq("#remSubRow<%=count%>0").show();</script>
                                  <%}%>                                  
                                  <div class="add_fld"> <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"></span></a></div>
                                </fieldset>
                              </div>
                            </c:forEach>                        
                            <div class="ad-subjt" id="addSubject_<%=count%>" style="display:block">
                              <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('<%=selectQualId%>','countAddSubj_<%=count%>', '${indexIValue}');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                            </div>                          
                          </div>
                        </div>
                        
                        <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                      </div>
                      </c:forEach>
                      <!-- Add Qualification & Subject 1 -->
                      <!-- L3 Qualification Row -->
                    </div>
                    <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" end="2">
                      <c:set var="indexIValue" value="${i.index}"/>
                      <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue")));if(count > 0){addQualBtnStyle = "display:none";}%>
                      <div class="add_qualtxt" id="addQualBtn_<%=count%>" style="<%=addQualBtnStyle%>">
                        <p class="aq_hlptxt">Studied more than one qualification? Select additional below</p>
                        <a href="javascript:void(0);" onclick="addQualification('level_3_qual_<%=count + 1%>', <%=count + 1%>, 'addQualBtn_<%=count%>');" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> ADD A QUALIFICATION</a>
                      </div>
                    </c:forEach>
                    <!-- Tariff Points Container Start -->
                    <!-- Tariff Points Container End -->		
                  </div>
                </div>
                <!--Qualification Repeat Field end 1 -->
                
                <!--Qualification Repeat Field start 2-->
                <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" >
                  <c:set var="indexIValue" value="${i.index}"/>
                  <c:if test="${qualList.entry_qual_id eq '17'}">
                  <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue")));%>
                  <div class="add_qualif">
                    <div class="ucas_refld">
                      <h3 class="un-tit">
                        Level 2: GCSEs
                        <a href="javascript:void(0)" class="blu-lnk rltv bsc">
                          <span>Why do we need this?</span>
                          <span id="postcodeText" class="cmp adjst">
                            <div class="hdf5"></div>
                          <div>Some courses require you to have studied and achieved a pass grade for specific GCSE subjects. Others require you to have achieved a pass of a minimum number of GCSE subjects. It's therefore important to tell us all of your qualifications so that we can provide you with the best matching courses.</div>
                          <div><strong>Please Note:</strong> GCSEs are the only Level 2 qualifications used by Whatuni Go to match you with courses. If you have different Level 2 Qualifications, you can skip this stage and submit your search. </div>
                            <div class="line"></div>
                          </span>
                        </a>
                      </h3>
                      <p class="ql_dec">Please enter your GCSE grades below. <b>If you have BOTH numerical (9-1) and alphabetical (A*-G) grades for your GCSEs. Please select GCSE: A*-G and convert your numerical grades as follows: (9 = A*, 7-8 = A, 6 = B, 4-5 = C, 3 = D).</b> </p>
                      <!-- L3 Qualification Row  -->
                      <div class="l3q_rw" id="qual_level_<%=count%>">
                        <!-- Add Qualification & Subject 1-->		
                        <!-- Add Qualification & Subject 2-->		
                        <div class="adqual_rw" data-id="level_2_add_qualif">                          
                            <div class="ucas_row">    
                            <h5 class="cmn-tit txt-upr">GRADE TYPE</h5>
                              <fieldset class="ucas_fldrw">
                                <div class="remq_cnt">
                                  <div class="ucas_selcnt">
                                    <fieldset class="ucas_sbox">
                                      <c:set var="selectQualLeve2Name" value="${qualList.entry_qualification}"/>
                                      <%String selectQualLeve2Name = pageContext.getAttribute("selectQualLeve2Name").toString(); %>
                                      <span class="sbox_txt" id="qualspan_<%=count%>"><%=selectQualLeve2Name%>: A*-G</span>
                                      <i class="fa fa-angle-down fa-lg"></i>
                                    </fieldset>
                                     <select name="qualification1" onchange="appendQualDiv(this, '', '<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>', 'new_entry_page');" id="qualsel_<%=count%>" class="ucas_slist">
                                     <option value="0">Please Select</option>
                                       <c:forEach var="qualListLevel2" items="${requestScope.qualificationList}" >
                                         <c:if test="${qualListLevel2.entry_qualification eq 'GCSE'}">
                                             <jsp:scriptlet>gcseSelectFlag = "selected=\"selected\"";</jsp:scriptlet>                              
                                              <option value="${qualListLevel2.entry_qual_id}_gcse_new_grade" >${qualListLevel2.entry_qualification} : 9-1 </option>                               
                                              <option value="${qualListLevel2.entry_qual_id}_gcse_old_grade" <%=gcseSelectFlag%>>${qualListLevel2.entry_qualification} : A*-G</option>
                                          </c:if>
                                        </c:forEach>
                                      </select>
                                      <c:forEach var="grdQualifications" items="${requestScope.qualificationList}" >
                                         <c:if test="${grdQualifications.entry_qualification eq 'GCSE'}">                                                               
                                           <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}_gcse" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                           <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_new_grade" name="" value="${grdQualifications.entry_grade}" />
                                           <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}_gcse_old_grade" name="" value="${grdQualifications.entry_old_grade}" />
                                         </c:if>
                                      </c:forEach>
                                   </div>
                                 </div>
                                <%--
                                <div class="add_fld">
                                  <a id="1rSubRow3" onclick="javascript:removeSubRow(1,3);" class="btn_act1 bck f-14 pt-5">
                                    <span class="hd-mob">Remove Qualification</span>
                                  </a>
                                </div>
                                --%>
                              </fieldset>
                              <div class="err" id="qualSubId_error" style="display:none;"></div>
                            </div>                          
                          <div class="subgrd_fld" id="subGrdDiv<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>">
                            <div class="ucas_row grd_row" id="grdrow_<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>">                            
                              <fieldset class="ucas_fldrw quagrd_tit">
                                <div class="ucas_tbox tx-bx">
                                  <h5 class="cmn-tit txt-upr qual_sub">Subject</h5>
                                </div>
                                <div class="ucas_selcnt rsize_wdth">
                                  <h5 class="cmn-tit txt-upr qual_grd">Grade</h5>
                                </div>
                              </fieldset>
                              <c:set var="selectQualLevel2SubLen" value="${qualList.entry_subject}"/>
                              <%
                                 String selectQualLevel2SubLen = pageContext.getAttribute("selectQualLevel2SubLen").toString();
                                 subLen = Integer.parseInt((selectQualLevel2SubLen));
                              %> 
                              <c:forEach var="subjectList" items="${requestScope.qualificationList}" varStatus="j" end="<%=subLen%>">
                                 <c:set var="indexJValue" value="${j.index }"/>
                                 <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue")));%>
                                <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
                                  <fieldset class="ucas_fldrw">
                                    <div class="ucas_tbox w-390 tx-bx">
                                      <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="subIp_<%=subCount%>" onblur="getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="" class="frm-ele" autocomplete="off">															
                                      <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                                      <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                                      </div>
                                    </div>
                                    <div class="ucas_selcnt rsize_wdth">
                                      <div class="ucas_sbox mb-ht w-84" id="1grades1">
                                        <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">9</span><i class="fa fa-angle-down fa-lg"></i>
                                      </div>
                                      <select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" id="qualGrd_<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>_<%=Integer.parseInt(pageContext.getAttribute("indexJValue").toString())%>"></select>                                      
                                      <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=String.valueOf(pageContext.getAttribute("indexIValue"))%>_<%=String.valueOf(pageContext.getAttribute("indexJValue"))%>','${subjectList.entry_grade}'); </script>
                                      
                                      <input class="subjectArr" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_${qualList.entry_qual_id}" value="" />                                
                                      <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_${qualList.entry_qual_id}" value="9" />
                                      <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${qualList.entry_qual_id}" value="${qualList.entry_qual_id}" />
                                      <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_${qualList.entry_qual_id}" value="<%=count+1%>" />
                                    </div>
                                    <%if(subCount == 0){
                                      removeBtnFlag = "display:none";
                                    }else{
                                      removeBtnFlag = "display:block";
                                    }
                                    if(subCount == 1){%>                                  
                                      <script>jq("#remSubRow<%=count%>0").show();</script>
                                    <%}%> 
                                    <div class="add_fld"> <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"></span></a> </div>
                                  </fieldset>
                                </div>                                
                              </c:forEach>                      
                              <div class="ad-subjt" id="addSubject_<%=count%>" style="display:block">
                                <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('${qualList.entry_qual_id}','countAddSubj_<%=count%>', '<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                              </div>                            
                          </div>
                        </div>
                        <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <!-- Add Qualification & Subject 2 -->
                      </div>
                    </div>
                    </div>
                  </div>
                  
                  <script>jq( document ).ready(function() {appendQualDiv(jq('#qualsel_'+<%=count%>).get(0), '', '<%=Integer.parseInt(pageContext.getAttribute("indexIValue").toString())%>', 'new_entry_page', 'add_Qual_gcse');});</script>
                  </c:if>
                </c:forEach>
                <!--Qualification Repeat Field end 2 -->
              </div>
              <%--<div class="err" id="subErrMsg" style="display:none;"></div>--%>
            </div>
            <!-- UCAS end -->
                 <div class="pt-25 qua_cnf">
                <h3 class="un-tit p-0 f-20 pt-0">View courses from <span id="minSliderVal"><%=minScore%></span> to <span id="maxSliderVal"><%=maxScore%></span> points</h3>
                <div class="rngsl_cnt">
                  <div class="rng_wrap">
                    <input type="text" id="cmm_rngeSlider" name="cmm_rngeSlider" value="" class="irs-hidden-input" readonly="" style="display:none">
                    <div class="ucsre_val">Your UCAS score:<span id="ucasPt"> <%=ucaspoint%> </span> points <i id="ucasArrow" style="left:-2%" class="fa fa-long-arrow-right"></i></div>
                  </div>
               
                  <div class="fl add_qualtxt rngesl_sub" style="display: block;">
                  </div>
                </div>

              </div>
             <div class="btn_cnt pt-40">
               	<div class="fr skpvw_cnt">
                  <a onclick="getMatchedCourse();" id="viewButton" class="fr bton">CALCULATE MATCHING COURSES <i class="fa fa-long-arrow-right"></i></a>
                  <a onclick="skipAndViewResults();" class="fl bck skvw_btn" title="Skip to view all courses"> Skip to view all courses</a>
                </div>
                <div class="fl">
                  <a href="/degrees/wugo/subject/search.html" class="fl bck" title="Back"><i class="fa fa-long-arrow-left"></i> Back</a>
                  
                </div>
              </div>
           </div>
        </form>
        </c:if>
      </div>
    </div>
  </div>
</section>
 <input type="hidden" id= "userUcasScore" value="<%=ucaspoint%>"/>
 <input type="hidden" id="country_hidden" name="country_hidden" value="<%=location%>"/>
<input type="hidden" id="newQualDesc" value="<%=qualifications%>" />
<input type="hidden" id="subject" name="subject" value="<%=subject%>"/>
  <input type="hidden" id="q" name="q" value="<%=q%>"/>
  <input type="hidden" id="university" name="university" value="<%=university%>"/>  
  <input type="hidden" id="qualCode" name="qualCode" value="<%=qualCode%>"/>  
  <input type="hidden" id="referralUrl" name="referralUrl" value="<%=referralUrl%>"/>  
  <input type="hidden" id="minSliderVal" name="minSliderVal" value="<%=minScore%>"/>
  <input type="hidden" id="maxSliderVal" name="maxSliderVal" value="<%=maxScore%>"/> 
  <input type="hidden" id="ucasMaxPoints" name="ucasMaxPoints" value="<%=ucasMaxPoints%>"/>
  <input type="hidden" id="removeLinkImg" value="<%=removeLink%>" />
  <input type="hidden" id="score" value="<%=score%>"/>
  <input type="hidden" id="refresh" value="no">
<script type="text/javascript">
jq( document ).ready(function() {
 appendQualDiv(1, '', '0', 'new_entry_page', 'add_Qual');  
   ucasArrow();
 });
</script>