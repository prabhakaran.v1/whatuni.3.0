<%@page import="WUI.utilities.CommonUtil" autoFlush="true" %>

<body>
<div id="loadingImg" class="cmm_ldericn" style="display:none;"><img alt="wugo-loading-image" src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif",0)%>"></div>
  <header class="clipart cmm_hdr">
    <div class="ad_cnr">
      <div class="content-bg">
        <div id="desktop_hdr">
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>                
      </div>      
    </div>  
  </header>
  <div id="middleContent">
    <jsp:include page="/jsp/whatunigo/newUserGradeFilterPage.jsp"/>
  </div>
<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>