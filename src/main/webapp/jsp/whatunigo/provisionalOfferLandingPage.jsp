<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>
<%
  String browserip = GlobalConstants.WHATUNI_SCHEME_NAME + new CommonUtil().getProperties().getString("review.autocomplete.ip");
  String collegeName = (String)request.getAttribute("hitbox_college_name");
  String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(collegeName);
  String collegeId = (String)request.getAttribute("COLLEGE_ID");
  String refererURL = (request.getAttribute("REFERER_URL") != null) ? (String)request.getAttribute("REFERER_URL") : "";
  String C_END_TIME_T1 = (String)request.getAttribute("C_END_TIME_T1");
%>
  <body>
    <div id="loadingImg" class="cmm_ldericn" style="display:none;"><img alt="wugo-loading-image" src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif",0)%>"></div>
    <header class="clipart cmm_hdr">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>  
    </header>
    <div id="middleContent">
    <div class="brd_crumb timer_sec">
      <div class="cmm_col_12">
        <div class="cmm_wrap">          
           <jsp:include page="/jsp/whatunigo/include/timer.jsp"></jsp:include> 
        </div>
      </div>
    </div>
      <jsp:include page="/jsp/whatunigo/include/entryRequirement.jsp">
         <jsp:param name="offerName" value="PROVISIONAL_OFFER" />       
      </jsp:include>    
    </div>
    <input type="hidden" id="offerName" value="PROVISIONAL_OFFER"/>
    <%--<input type="hidden" id="refererUrl" name="refererUrl" value="<%=(request.getHeader("referer") !=null ? request.getHeader("referer") : browserip+"/degrees/home.html")%>" />--%>
    <input type="hidden" id="refererUrl" value="<%=refererURL%>"/>
    <input type="hidden" id="applyNowCourseId" name="applyNowCourseId" value="${requestScope.APPLY_NOW_COURSE_ID}"/>
    <input type="hidden" id="applyNowOpportunityId" name="applyNowOpportunityId" value="${requestScope.APPLY_NOW_OPPORTUNITY_ID}"/>
   <%-- <input type="hidden" id="userIdT1" name="userIdT1" value="${requestScope.C_USER_ID_T1}"/>
    <input type="hidden" id="courseIdT1" name="courseIdT1" value="${requestScope.C_COURSE_ID_T1}"/>
    <input type="hidden" id="opportunityIdT1" name="opportunityIdT1" value="${requestScope.C_OPPORTUNITY_ID_T1}"/>
    <input type="hidden" id="timerIdT1" name="timerIdT1" value="${requestScope.C_TIMER_ID_T1}"/>
    <input type="hidden" id="startTimeT1" name="startTimeT1" value="${requestScope.C_START_TIME_T1}"/>    
    --%>
    <input type="hidden" id="endTimeT1" name="endTimeT1" value="<%=C_END_TIME_T1%>"/>        
    <input type="hidden" id="statusT1" name="statusT1" value="N"/>        
    <input type="hidden" id="collegeName" value="<%=gaCollegeName%>"/>
    <input type="hidden" id="collegeId" value="<%=collegeId%>"/>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
  </body>