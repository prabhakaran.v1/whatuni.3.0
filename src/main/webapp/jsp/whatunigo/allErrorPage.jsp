<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator" autoFlush="true" %>

<%
  String browserip = GlobalConstants.WHATUNI_SCHEME_NAME + new CommonUtil().getProperties().getString("review.autocomplete.ip");
  String ERROR_CODE = (String)request.getAttribute("ERROR_CODE");
  String PLCE_AVAIL_FLAG = (String)request.getAttribute("PLCE_AVAIL_FLAG");
  String C_END_TIME_T1 = (String)request.getAttribute("C_END_TIME_T1");
  String timerExpCall = (String)request.getAttribute("timerExpCall");
  String TIMER = (String)request.getAttribute("TIMER");
  String collegeName = (String)request.getAttribute("hitbox_college_name");
  String collegeId = (String)request.getAttribute("COLLEGE_ID");
  String refererURL = (request.getAttribute("REFERER_URL") != null) ? (String)request.getAttribute("REFERER_URL") : "";
%>
  <body>
    <header class="clipart cmm_hdr">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>  
    </header>
    <div id="middleContent" style="height:600px;"></div>
    <input type="hidden" id="offerName" value="ACCEPTING_OFFER"/>
    <input type="hidden" id="refererUrl" name="refererUrl" value="<%=refererURL%>" />
   <%-- <input type="hidden" id="refferUrl" value="${requestScope.REFERER_URL}" />--%>
    <input type="hidden" id="applyNowCourseId" name="applyNowCourseId" value="${requestScope.APPLY_NOW_COURSE_ID}" />
    <input type="hidden" id="applyNowOpportunityId" name="applyNowOpportunityId" value="${requestScope.APPLY_NOW_OPPORTUNITY_ID}" />
    <input type="hidden" id="collegeId" name="collegeId" value="<%=collegeId%>"/>
   <%-- <input type="hidden" id="userIdT1" name="userIdT1" value="${requestScope.C_USER_ID_T1}"/>
    <input type="hidden" id="courseIdT1" name="courseIdT1" value="${requestScope.C_COURSE_ID_T1}" />
    <input type="hidden" id="opportunityIdT1" name="opportunityIdT1" value="${requestScope.C_OPPORTUNITY_ID_T1}" />
    <input type="hidden" id="timerIdT1" name="timerIdT1" value="${requestScope.C_TIMER_ID_T1}" />
    <input type="hidden" id="startTimeT1" name="startTimeT1" value="${requestScope.C_START_TIME_T1}" /> --%> 
    <input type="hidden" id="endTimeT1" name="endTimeT1" value="<%=C_END_TIME_T1%>"/>       
    <input type="hidden" id="statusT1" name="statusT1" value="N"/>       
    <input type="hidden" id="collegeName" value="<%=collegeName%>"/>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
    <script type="text/javascript">
      <%if(!GenericValidator.isBlankOrNull(ERROR_CODE)){%>
        errorLiboxCall('ERROR', '<%=ERROR_CODE%>');
      <%}else if("N".equalsIgnoreCase(PLCE_AVAIL_FLAG)){%>
        errorLiboxCall('NO_PLACE', 'Unfortunately there are no more places available on this course. We advise you to call the university directly, or you can try again later');
      <%}else if("Y".equalsIgnoreCase(timerExpCall)){%>
        timerOneExpiryCall('', '<%=TIMER%>');
      <%}%>
    </script>
  </body>