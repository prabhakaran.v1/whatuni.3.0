<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
 
<%--
  * @purpose  This is included for GAM banner display jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 05-Sep-2017 Prabhakaran V.               568      First Draft                                                       568 
  * *************************************************************************************************************************
--%>
<%
  int stind1 = request.getRequestURI().lastIndexOf("/");
  int len1 = request.getRequestURI().length();
  String page_name = "";
  if(pageContext.getAttribute("pagename3") != null){
	  page_name = (String)pageContext.getAttribute("pagename3");
  }else {
	  page_name = request.getRequestURI().substring(stind1+1,len1);	  
  }
  boolean openDayDatePageName = false;
  if((!GenericValidator.isBlankOrNull(page_name)) && (("overviewpage.jsp").equalsIgnoreCase(page_name))){           
    page_name = "aboutus.jsp";           
  }
  //17_Mar_2015 - added condition to retain existing jsp file name for open day landing pages, by Thiyagu G.
  if((!GenericValidator.isBlankOrNull(page_name)) && (("opendaysLandingPage.jsp").equalsIgnoreCase(page_name) || ("opendayslandingdate.jsp").equalsIgnoreCase(page_name))){           
    page_name = "opendaybrowse.jsp";  
    if(("opendayslandingdate.jsp").equalsIgnoreCase(page_name)){
      openDayDatePageName = true;
    }
  }
  if("advanceSearchResults.jsp".equalsIgnoreCase(page_name)){
    page_name = "browseMoneyPageResults.jsp";
  }
  if("advanceProviderResults.jsp".equalsIgnoreCase(page_name)){
    page_name = "courseSearchResult.jsp";
  }
  //17_MAR_2015 Added by Amir to Retain opendays provider landing page.
  if((!GenericValidator.isBlankOrNull(page_name)) && (("opendaysProviderLanding.jsp").equalsIgnoreCase(page_name))){
    page_name = "viewopendaysnew.jsp";   
  }
  //17_May_2015 Added by Thiyagu G to Retain mywhatuni settings page.
  if((!GenericValidator.isBlankOrNull(page_name)) && (("mysettings.jsp").equalsIgnoreCase(page_name))){
    page_name = "mywhatuni.jsp";   
  } 
  String articleCategoryName = (String)request.getAttribute("articleCategoryName");
  int clearingIndex = -1;
  if(articleCategoryName != null){
    clearingIndex = articleCategoryName.indexOf("clearing");
  }
  String clSearchBarForNoResults = (String)request.getAttribute("clearingSearchBar");
  String noResults = "";
  if(clSearchBarForNoResults != null){
    noResults = "Y";
  }
  if((!GenericValidator.isBlankOrNull(page_name)) && (("coursedetails.jsp").equalsIgnoreCase(page_name))){    
    if("TRUE".equals((String)request.getAttribute("CLEARING_COURSE"))){
      page_name = "clearingCourseDeatils.jsp";           
    }
  }
  //21_July_2015 - added condition to retain existing jsp file name for Find a course pages, by Thiyagu G.
  if((!GenericValidator.isBlankOrNull(page_name)) && (("coursesearch.jsp").equalsIgnoreCase(page_name))){
    page_name = "categoryhome.jsp";   
  }
  if((!GenericValidator.isBlankOrNull(page_name)) && "newProviderHome.jsp".equals(page_name)){
    String newProfileType = (String)request.getAttribute("setProfileType");
    if((!GenericValidator.isBlankOrNull(newProfileType)) && "CLEARING_PROFILE".equals(newProfileType)){       
      page_name = "clearinguniprofile.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "CLEARING_LANDING".equals(newProfileType)){
      page_name = "clearingunilanding.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "NORMAL".equals(newProfileType)){
      page_name = "providerhome.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "SUB_PROFILE".equals(newProfileType)){
      page_name = "subjectprofile.jsp";
    }          
  }
  boolean showNonAdvBanner = true;  
  if(request.getAttribute("nonAdvertFlag")!=null && "false".equals(request.getAttribute("nonAdvertFlag"))){
    showNonAdvBanner = false;
  }
%>
<%
  String google_ad_page_name = page_name !=null ? page_name.replaceAll(".jsp","").toLowerCase() : page_name; 
  //following code added for 16th Feb 2010 Release
  String jspNamesArray [] = new String[]{"ucasDeadlines", "ucasGettingDegree", "ucasHome", "ucasInfoForm", "ucasOpenDays", "ucasPersonalStatement", "ucasPoints", "expandCollegeMap", "uniStreetView"};
  for(int nameloop=0; nameloop<jspNamesArray.length; nameloop++){
    if(google_ad_page_name !=null && google_ad_page_name.equalsIgnoreCase(jspNamesArray[nameloop])){
      google_ad_page_name = jspNamesArray[nameloop];
      break;
    }
  }
  int pageclindexvalue = google_ad_page_name.indexOf("clearing");
%>        
<script type="text/javascript">
	function $$(id) {
		return document.getElementById(id);
	}
	
  var res=parseInt((screen.width),10);
  var height = document.body.clientHeight;
  var footerHeight = "0";
  if($$('footer-bg')){ footerHeight = $$('footer-bg').clientHeight; }
  var bannerHght = height - footerHeight; //Change the height of the banner based on the hight of the center content by Prabha on 28_Jun_2016
  
</script>
<%
  String banclassname = "";
  String pagesTobeHandled = "courseDetails.jsp,adviceHome.jsp,adviceSearchResults.jsp,primaryCategoryLanding.jsp,secondaryCategoryLanding.jsp,opendaybrowse.jsp,openDaySearch.jsp,viewopendaysnew.jsp,studentChoiceResult_2015.jsp,compareBasket.jsp, clearingHome.jsp,clearingCourseDeatils.jsp,providerhome.jsp,uniBrowser.jsp,uniFinderAZ.jsp,adviceDetails.jsp,ProspectusLocation.jsp,categoryhome.jsp,compareBasket.jsp,reviewHome.jsp, opendaySearchResults.jsp,clearingBrowseMoneyPage.jsp,clearingCourseSearchResult.jsp,collegeEmailSuccess.jsp,collegeProspectusSuccess.jsp,prospectusSentSuccess.jsp,downloadProspectusSuccess.jsp,enquiryLimitMessage.jsp";  //Added jsp names to be handled for banner issue 19_Apr_2016 Indumathi.S     
  if((pagesTobeHandled.contains(page_name))){
    if("courseDetails.jsp".equals(page_name) ||("opendaybrowse.jsp".equals(page_name)) || ("opendaySearchResults.jsp".equals(page_name)) || ("viewopendaysnew.jsp".equals(page_name))
       || "adviceHome.jsp".equals(page_name) || "adviceSearchResults.jsp".equals(page_name) || "primaryCategoryLanding.jsp".equals(page_name) || "secondaryCategoryLanding.jsp".equals(page_name) 
       || "compareBasket.jsp".equals(page_name) || "studentChoiceResult_2015.jsp".equals(page_name) || "prospectuslocation.jsp".equalsIgnoreCase(page_name) || "clearingCourseDeatils.jsp".equalsIgnoreCase(page_name) 
       || "providerhome.jsp".equalsIgnoreCase(page_name) || "unibrowser.jsp".equalsIgnoreCase(page_name) || "unifinderaz.jsp".equalsIgnoreCase(page_name) || "categoryhome.jsp".equalsIgnoreCase(page_name)
       || "reviewHome.jsp".equalsIgnoreCase(page_name) || "adviceDetails.jsp".equalsIgnoreCase(page_name) || "collegeEmailSuccess.jsp".equalsIgnoreCase(page_name) || "collegeProspectusSuccess.jsp".equalsIgnoreCase(page_name)
       || "prospectusSentSuccess.jsp".equalsIgnoreCase(page_name) || "downloadProspectusSuccess.jsp".equalsIgnoreCase(page_name) || "enquiryLimitMessage.jsp".equalsIgnoreCase(page_name)){       
      if("studentChoiceResult_2015.jsp".equals(page_name)){
        banclassname = "top:344px;";
      }else if("uniFinderAZ.jsp".equals(page_name)){
        banclassname = "top:500px;";
      }else if("adviceDetails.jsp".equalsIgnoreCase(page_name)){ //Added by Inudmathi.S for adviceDetails page banner issue 19_Apr_2016
        banclassname = "top:900px;";
      } else if("reviewHome.jsp".equalsIgnoreCase(page_name)){
        banclassname = "top:420px;";
      }else if("viewopendaysnew.jsp".equalsIgnoreCase(page_name)){ //Added by Prabha on  openday banner issue 28_Jun_2016
        String opendayAdvertiser = (String)request.getAttribute("opendayAdvertiser");
        //banclassname = "top:390px";
       // if("Y".equalsIgnoreCase(opendayAdvertiser)){banclassname = "top:900px";}
      }else if("opendaySearchResults.jsp".equalsIgnoreCase(page_name)){
        //banclassname = "top:344px";
      }else{
        if(!"opendaybrowse.jsp".equalsIgnoreCase(page_name)){
          banclassname = "top:390px;";
        }
      }        
    }else if ("clearingHome.jsp".equalsIgnoreCase(page_name)){ //Added else if condition for clearing home page banners for 09_Jun_2015, By Thiyagu G. 
      banclassname = "top:770px;";
    }else{
      banclassname = "top:259px;";
    }
    if("opendaybrowse.jsp".equalsIgnoreCase(page_name)){
     // banclassname = "top:800px;";
    }else if("opendaySearchResults.jsp".equalsIgnoreCase(page_name) && "viewopendaysnew.jsp".equalsIgnoreCase(page_name)){
      //banclassname = "top:477px;";
    }else if ("clearingBrowseMoneyPage.jsp".equalsIgnoreCase(page_name)){
      banclassname = "top:559px!important;";
    }else if("clearingcoursesearchresult.jsp".equalsIgnoreCase(page_name)){
      banclassname = "top:627px!important;";
    }
  }
%> 
<div id="sky" style="<%=banclassname%>">   
  <script type="text/javascript">
  function $$(id) {
		return document.getElementById(id);
	}  
  
    var skyobj=$$('sky');
    if(screen.width <= 1024){ 
      if($$('content-bg')){
        $$('content-bg').style.width = '1004px'; 
        $$('content-bg').style.padding = '0';
      }
      if($$('footer')){$$('footer').style.width = '1004px';}          
      if($$('footer')){$$('footer').style.padding = '0';}
      if($$('wrapper')){$$('wrapper').style.padding = '0';}
      if($$('content-cmpr')){$$('content-cmpr').style.padding = '0';}
      //$$('toprightbanner').style.left = '258px'; 16-Apr-2014k
    }
  </script> 
  <%
    if(!"whyStudySearchLanding.jsp".equals(page_name)){
      String subjectGuide = "no";
      if(request.getAttribute("subjectGuide") != null){
        subjectGuide = request.getAttribute("subjectGuide").toString();
      }
      String showBannerFlag = "";
      if(!GenericValidator.isBlankOrNull((String)request.getAttribute("showBannerFlag"))){
        showBannerFlag = request.getAttribute("showBannerFlag").toString();
      }
      if(!"yes".equals(subjectGuide) &&(showNonAdvBanner) || showNonAdvBanner){
        %>
        <div class="sk1" id="sk1">
        <% if("viewopendaysnew.jsp".equalsIgnoreCase(page_name)){%>
           <script type="text/javascript">
             var bannerHgt = bannerHght - 480;
             if(bannerHgt > 600 && res >= 1280){
              <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
              googletag.cmd.push(function() { googletag.display('sk1');});
              skyobj.style.display='block';
              </c:if>
            }
        </script>
        <%}else{%>
          <script type="text/javascript"> 
            if(res >= 1280){
            <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
             googletag.cmd.push(function() { googletag.display('sk1');});
              skyobj.style.display='block';
              </c:if>
            }
          </script>
        <%}%>
          
        </div>
        <%if(!"clearinghome.jsp".equalsIgnoreCase(page_name) && (!openDayDatePageName)){%>
          <div class="sk2" id="sk2">
            <script type="text/javascript">
           
              if(bannerHght>=1700){ 
                if(res >= 1280){ 
                  <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
                  googletag.cmd.push(function() { googletag.display('sk2');});
                  skyobj.style.display='block';
                  </c:if>
                  var jq = jQuery.noConflict();jq(".subCon").addClass('ad_bnr');
                }
              }
            </script>
          </div>
        <%}%>
      <%}
      request.removeAttribute("subjectGuide");
    }
  %>
  <script type="text/javascript">if(res < 1280){skyobj.style.display='none';} </script>
</div>