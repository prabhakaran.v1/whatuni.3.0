<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalFunction, WUI.utilities.CommonUtil, mobile.util.MobileUtils, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants" %>
<%-- Google-Analytics code commented during development     --%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
 

<%  
   /*End of code added*/ 
   int stind1 = request.getRequestURI().lastIndexOf("/");
   int len1 = request.getRequestURI().length();
   String pageName = "";
   if(pageContext.getAttribute("pagename3") != null){
	   pageName = (String)pageContext.getAttribute("pagename3");
   }else{
	   pageName = request.getRequestURI().substring(stind1+1,len1);
   }
          pageName = pageName !=null ? pageName.toLowerCase() : pageName;      
  //
   if((!GenericValidator.isBlankOrNull(pageName)) && (("newUser.jsp").equalsIgnoreCase(pageName)  || ("retUser.jsp").equalsIgnoreCase(pageName) ||("loggedIn.jsp").equalsIgnoreCase(pageName))){
              pageName = "home.jsp";
   } else if ((!GenericValidator.isBlankOrNull(pageName)) && (("degreesubjectguide.jsp").equalsIgnoreCase(pageName))){
      pageName = "categoryhome.jsp";
   }   
   //17_Mar_2015 - added condition to retain existing jsp file name for open day landing pages, by Thiyagu G.
   if ((!GenericValidator.isBlankOrNull(pageName)) && (("opendaysLandingPage.jsp").equalsIgnoreCase(pageName) || ("opendayslandingdate.jsp").equalsIgnoreCase(pageName))){
      pageName = "opendaybrowse.jsp";
   }   
   if(MobileUtils.mobileUserAgentCheck(request) && "mobilelayout.jsp".equals(pageName)){ //08_Oct_2014, condition added for spliting mobile and desktop pages. By Thiyagu G.
    pageName = (String)request.getAttribute("gaPageName");
   }
   if((!GenericValidator.isBlankOrNull(pageName)) && (("richprofilelanding.jsp").equalsIgnoreCase(pageName))){           
    if(request.getAttribute("richProfileType")!=null && "RICH_INST_PROFILE".equalsIgnoreCase((String)request.getAttribute("richProfileType"))){
      pageName = "richuniview.jsp";      
      request.setAttribute("getInsightName","uniview.jsp"); //13_JAN_15 Added by Amir
    }else if(request.getAttribute("richProfileType")!=null && "RICH_SUB_PROFILE".equalsIgnoreCase((String)request.getAttribute("richProfileType"))){
      pageName = "richsubjectprofile.jsp";
      request.setAttribute("getInsightName","subjectprofile.jsp"); //13_JAN_15 Added by Amir
    }
   }  
   
   if((!GenericValidator.isBlankOrNull(pageName)) && (("reviewhome.jsp").equalsIgnoreCase(pageName))){
    pageName = "morereviews.jsp";
    request.setAttribute("getInsightName","morereviews.jsp");
   }   
   
   //03_FEB_15 Added by Amir for Review Redesign
   if((!GenericValidator.isBlankOrNull(pageName)) && (("reviewsearchresults.jsp").equalsIgnoreCase(pageName))){ 
    String srchType = "";
    if(request.getAttribute("reviewSrchType")!=null && !"".equals(request.getAttribute("reviewSrchType"))){
      srchType = (String)request.getAttribute("reviewSrchType");
    }
    if(!"".equals(srchType) && "keyword".equals(srchType)){ 
      pageName = "morereviews.jsp";
      if(request.getAttribute("reviewInsType")!=null && !"".equals(request.getAttribute("reviewInsType"))){
        request.setAttribute("getInsightName","providerreviews.jsp");        
      }else{
        request.setAttribute("getInsightName","morereviews.jsp");
      }  
    }
    if(!"".equals(srchType) && "subject".equals(srchType)){ 
      pageName = "subjectreviews.jsp";
      request.setAttribute("getInsightName","subjectreviews.jsp");
    }
    if(!"".equals(srchType) && "provider".equals(srchType)){ 
      pageName = "providerreviews.jsp";
      request.setAttribute("getInsightName","providerreviews.jsp");
    }   
   }
   
   //03_FEB_15 Added by Amir for Review Redesign
   if((!GenericValidator.isBlankOrNull(pageName)) && (("providerreviewresults.jsp").equalsIgnoreCase(pageName))){
    String srchType = "";
    if(request.getAttribute("reviewSrchType")!=null && !"".equals(request.getAttribute("reviewSrchType"))){
      srchType = (String)request.getAttribute("reviewSrchType");
    }
    if(!"".equals(srchType) && "provider".equals(srchType)){ 
      pageName = "providerreviews.jsp";
      request.setAttribute("getInsightName","providerreviews.jsp");
    }    
   }   
   //17_Mar_2015 - added condition to retain existing jsp file name for open day landing pages, by Thiyagu G.
   if((!GenericValidator.isBlankOrNull(pageName)) && (("opendayslanding.jsp").equalsIgnoreCase(pageName) || ("opendayslandingdate.jsp").equalsIgnoreCase(pageName))){
    pageName = "opendaybrowse.jsp";
   }
   //17_MAR_2015 Added by Amir to Retain opendays provider landing page.
   if((!GenericValidator.isBlankOrNull(pageName)) && (("opendaysProviderlanding.jsp").equalsIgnoreCase(pageName) || ("opendayproviderlandingpage.jsp").equalsIgnoreCase(pageName))){
    pageName = "viewopendaysnew.jsp";
    request.setAttribute("getInsightName",pageName);
   }
   if((!GenericValidator.isBlankOrNull(pageName)) && (("studentchoiceresult_2015.jsp").equalsIgnoreCase(pageName))){
    request.setAttribute("getInsightName","studentawards.jsp");
    String loadyear = request.getAttribute("loadYear") != null ? (String)request.getAttribute("loadYear") : "";//Added Loadyear by Indumathi.S Mar_29_2016
    if(!GenericValidator.isBlankOrNull(loadyear)){
      pageName = "studentchoiceresult_"+loadyear+".jsp";
    }    
   }
   if((!GenericValidator.isBlankOrNull(pageName)) && (("coursesearch.jsp").equalsIgnoreCase(pageName))){
    request.setAttribute("getInsightName","categoryhome.jsp");
   }
   if((!GenericValidator.isBlankOrNull(pageName)) && (("mysettings.jsp").equalsIgnoreCase(pageName))){
    request.setAttribute("getInsightName","mywhatuni.jsp");
   }
   //
   if((!GenericValidator.isBlankOrNull(pageName)) && "newproviderhome.jsp".equals(pageName)){
    String newProfileType = (String)request.getAttribute("setProfileType");
    if((!GenericValidator.isBlankOrNull(newProfileType)) && "CLEARING_PROFILE".equals(newProfileType)){       
      pageName = "clearinguniprofile.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "CLEARING_LANDING".equals(newProfileType)){
      pageName = "clearingunilanding.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "NORMAL".equals(newProfileType)){
      pageName = "uniview.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "SUB_PROFILE".equals(newProfileType)){
      pageName = "subjectprofile.jsp";
    }          
  }  
  String pageType = (String)request.getAttribute("setPageType");
  if("browsemoneypageresults".equals(pageType)){
    pageName = "browsemoneypageresults.jsp";
  }else if("moneypageresults".equals(pageType)){
    pageName = "moneypageresults.jsp";
  }
  //new GlobalFunction().getCustomDimesnionValues(pageName, request);
  String region = (String)request.getAttribute("cDimRegion") != null ? (String)request.getAttribute("cDimRegion") : (String) request.getSession().getAttribute("sc_prob44_location");
  String regionUpper = null;
  if(!GenericValidator.isBlankOrNull(region)){
    regionUpper = region.toUpperCase();
  }
  request.setAttribute("GTMregion",regionUpper);
%>
<script type="text/javascript">     
   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
   <c:set var="googleAnalyticId" value="UA-52581773-4"/>
   <c:choose>
    <c:when test="${'N' ne sessionScope.sessionData['cookiePerformanceDisabled']}">
      window['ga-disable-${googleAnalyticId}'] = true;
    </c:when>
    <c:when test="${'N' eq sessionScope.sessionData['cookiePerformanceDisabled']}">
      window['ga-disable-${googleAnalyticId}'] = false;
    </c:when>
   </c:choose>
  //Insights account changed and removed sampling for 08_MAR_2016, By Thiyagu G.
  ga('create', 'UA-52581773-4', 'auto', {'name': 'newTracker'}); //TODO    fortest TEST: UA-37421000-2 LIVE - UA-52581773-1 changed to UA-52581773-1’4’
  ga('newTracker.set', 'contentGroup1', 'whatuni');
  //ga('create', 'UA-52581773-1', 'auto', {'sampleRate': 80}); 
  //ga('newTracker.require', 'displayfeatures');
  //Changed existing dimentions(dimension4, dimension7, dimension10, dimension11 and dimension16) value for 08_MAR_2016 By Thiyagu G
  <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimPageName"))){%>
     ga('newTracker.set', 'dimension1', '<%=(String)request.getAttribute("cDimPageName")%>');
   <%}%>
     ga('newTracker.set', 'dimension2', 'whatuni');
    <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimCollegeId"))){%>
     ga('newTracker.set', 'dimension3', '<%=(String)request.getAttribute("cDimCollegeId")%>');
   <%}%>
    <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimGranularQualificationType"))){%>
     ga('newTracker.set', 'dimension4', '<%=(String)request.getAttribute("cDimGranularQualificationType")%>');
   <%}%>
    <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimLevel"))){%>
     ga('newTracker.set', 'dimension5', '<%=(String)request.getAttribute("cDimLevel")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimLDCS"))){%>
     ga('newTracker.set', 'dimension6', '<%=(String)request.getAttribute("cDimLDCS")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimJACS"))){%>
     ga('newTracker.set', 'dimension7', '<%=(String)request.getAttribute("cDimJACS")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimCPE2"))){%>
     ga('newTracker.set', 'dimension8', '<%=(String)request.getAttribute("cDimCPE2")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimStudyMode"))){%>
     ga('newTracker.set', 'dimension9', '<%=(String)request.getAttribute("cDimStudyMode")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimQuintile"))){%>
     ga('newTracker.set', 'dimension10', '<%=(String)request.getAttribute("cDimQuintile")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimHECOS"))){%>
     ga('newTracker.set', 'dimension11', '<%=(String)request.getAttribute("cDimHECOS")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimRegion"))){%>
     ga('newTracker.set', 'dimension12', '<%=region%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimCountry"))){%>
     ga('newTracker.set', 'dimension13', '<%=(String)request.getAttribute("cDimCountry")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimCourseTitle"))){%>
     ga('newTracker.set', 'dimension14', '<%=(String)request.getAttribute("cDimCourseTitle")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimUID")) && !"0".equals((String)request.getAttribute("cDimUID"))){%>
     ga('newTracker.set', 'dimension15', '<%=(String)request.getAttribute("cDimUID")%>');
   <%}%>
    <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimIsAdvertiser"))){%>
     ga('newTracker.set', 'dimension16', '<%=(String)request.getAttribute("cDimIsAdvertiser")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimGrade"))){%>
     ga('newTracker.set', 'dimension17', '<%=(String)request.getAttribute("cDimGrade")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimKeyword"))){%>
     ga('newTracker.set', 'dimension18', '<%=(String)request.getAttribute("cDimKeyword")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimTargetYear"))){%>
     ga('newTracker.set', 'dimension19', '<%=(String)request.getAttribute("cDimTargetYear")%>');
   <%}%>
   <%if(!GenericValidator.isBlankOrNull((String)request.getAttribute("cDimSchool"))){%>
     ga('newTracker.set', 'dimension20', '<%=(String)request.getAttribute("cDimSchool")%>');
   <%}%>
   ga('newTracker.send', 'pageview');
  </script>  
<!-- END GOOGLE ANALYTICS CALL --> 
<input type="hidden" id="GAMPagename" value="<%=pageName%>"/>
<% 
//String admedoPages = "browsemoneypageresults.jsp, coursesearchresult.jsp, contenthub.jsp, coursedetails.jsp, providerreviews.jsp, viewopendaysnew.jsp, richuniview.jsp, uniview.jsp, richsubjectprofile.jsp, subjectprofile.jsp, qlbasicform.jsp, collegeemailsuccess.jsp";
String admedoPages = GlobalConstants.SMART_PIXEL_PAGE_NAMES;

if(!GenericValidator.isBlankOrNull(pageName) && (admedoPages.contains(pageName.toLowerCase()))){%>
  <jsp:include page="/jsp/thirdpartytools/include/includeSmartPixel.jsp"/>
<%}%>
  <input type="hidden" id="GTMRegion" value="<%=regionUpper%>" />
  <input type="hidden" id="GTMCpe2" value="<%=request.getAttribute("cDimCPE2")%>" />
  <input type="hidden" id="GTMJacs" value="<%=request.getAttribute("cDimJACS")%>" />
  <input type="hidden" id="GTMKeyword" value="<%=request.getAttribute("cDimKeyword")%>" />