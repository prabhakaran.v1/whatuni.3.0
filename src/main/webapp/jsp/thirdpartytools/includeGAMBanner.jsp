<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
</c:set>

<%--
  * @purpose  This is included for GAM banner display jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 05-Sep-2017 Prabhakaran V.               568      First Draft                                                       568 
  * *************************************************************************************************************************
--%>
<%
  int stind1 = request.getRequestURI().lastIndexOf("/");
  int len1 = request.getRequestURI().length();
  String page_name = pageContext.getAttribute("pagename3") != null ?  (String)pageContext.getAttribute("pagename3") : request.getRequestURI().substring(stind1+1,len1);
  boolean openDayDatePageName = false;
  CommonFunction common = new CommonFunction();
  if((!GenericValidator.isBlankOrNull(page_name)) && (("overviewpage.jsp").equalsIgnoreCase(page_name))){           
    page_name = "aboutus.jsp";           
  }
  //17_Mar_2015 - added condition to retain existing jsp file name for open day landing pages, by Thiyagu G.
  if((!GenericValidator.isBlankOrNull(page_name)) && (("opendaysLandingPage.jsp").equalsIgnoreCase(page_name) || ("opendayslandingdate.jsp").equalsIgnoreCase(page_name))){           
    page_name = "opendaybrowse.jsp";  
    if(("opendayslandingdate.jsp").equalsIgnoreCase(page_name)){
      openDayDatePageName = true;
    }
  }
  if("advanceSearchResults.jsp".equalsIgnoreCase(page_name)){
    page_name = "browseMoneyPageResults.jsp";
  }
  if("advanceProviderResults.jsp".equalsIgnoreCase(page_name)){
    page_name = "courseSearchResult.jsp";
  }
  //17_MAR_2015 Added by Amir to Retain opendays provider landing page.
  if((!GenericValidator.isBlankOrNull(page_name)) && (("opendaysproviderlanding.jsp").equalsIgnoreCase(page_name))){
    page_name = "viewopendaysnew.jsp";   
  }
  //17_May_2015 Added by Thiyagu G to Retain mywhatuni settings page.
  if((!GenericValidator.isBlankOrNull(page_name)) && (("mysettings.jsp").equalsIgnoreCase(page_name))){
    page_name = "mywhatuni.jsp";   
  } 
  String articleCategoryName = (String)request.getAttribute("articleCategoryName");
  int clearingIndex = -1;
  if(articleCategoryName != null){
    clearingIndex = articleCategoryName.indexOf("clearing");
  }
  String clSearchBarForNoResults = (String)request.getAttribute("clearingSearchBar");
  String noResults = "";
  if(clSearchBarForNoResults != null){
    noResults = "Y";
  }
  if((!GenericValidator.isBlankOrNull(page_name)) && (("coursedetails.jsp").equalsIgnoreCase(page_name))){    
    if("TRUE".equals((String)request.getAttribute("CLEARING_COURSE"))){
      page_name = "clearingCourseDeatils.jsp";           
    }
  }
  //21_July_2015 - added condition to retain existing jsp file name for Find a course pages, by Thiyagu G.
  if((!GenericValidator.isBlankOrNull(page_name)) && (("coursesearch.jsp").equalsIgnoreCase(page_name))){
    page_name = "categoryhome.jsp";   
  }
  if((!GenericValidator.isBlankOrNull(page_name)) && "newProviderHome.jsp".equals(page_name)){
    String newProfileType = (String)request.getAttribute("setProfileType");
    if((!GenericValidator.isBlankOrNull(newProfileType)) && "CLEARING_PROFILE".equals(newProfileType)){       
      page_name = "clearinguniprofile.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "CLEARING_LANDING".equals(newProfileType)){
      page_name = "clearingunilanding.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "NORMAL".equals(newProfileType)){
      page_name = "providerhome.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "SUB_PROFILE".equals(newProfileType)){
      page_name = "subjectprofile.jsp";
    }          
  }
  boolean showNonAdvBanner = true;  
  if(request.getAttribute("nonAdvertFlag")!=null && "false".equals(request.getAttribute("nonAdvertFlag"))){
    showNonAdvBanner = false;
  }
%>
<input type="hidden" name="appServerName" id="appServerName" value="<%=request.getAttribute("appServerName")%>" />
<%
  String google_ad_page_name = page_name !=null ? page_name.replaceAll(".jsp","").toLowerCase() : page_name; 
  //following code added for 16th Feb 2010 Release
  String jspNamesArray [] = new String[]{"ucasDeadlines", "ucasGettingDegree", "ucasHome", "ucasInfoForm", "ucasOpenDays", "ucasPersonalStatement", "ucasPoints", "expandCollegeMap", "uniStreetView"};
  for(int nameloop=0; nameloop<jspNamesArray.length; nameloop++){
    if(google_ad_page_name !=null && google_ad_page_name.equalsIgnoreCase(jspNamesArray[nameloop])){
      google_ad_page_name = jspNamesArray[nameloop];
      break;
    }
  }
  if("opendaysearchresults".equalsIgnoreCase(google_ad_page_name)){
    google_ad_page_name = "opendaysearch";
  }
  String tags = !GenericValidator.isBlankOrNull((String)request.getAttribute("tags")) ? common.replaceHypen(common.replaceURL(request.getAttribute("tags").toString())).toLowerCase() : "";
%>        
<jsp:include page="/jsp/thirdpartytools/include/includeGoogleTagJs.jsp" />
<%if(showNonAdvBanner){%>
  <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
  <script type="text/javascript">
    if(screen.width >= 1280){
      googletag.cmd.push(function() {
        googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/wuni_<%=google_ad_page_name%>_skyscrapper_120x600',[160, 600], "sk1").addService(googletag.pubads());
        googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/wuni_isf_<%=google_ad_page_name%>_skyscrapper_120x600',[160, 600], "sk2").addService(googletag.pubads());
        <%if(!GenericValidator.isBlankOrNull(tags)){%>
          googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/wuni_subjectguide_mpuslot_300x250', [300, 250], 'div-gpt-ad-subject_guide-0').addService(googletag.pubads());
          googletag.pubads().setTargeting("keyword","<%=tags%>");
        <%}
        if("uniFinderAZ.jsp".equalsIgnoreCase(page_name)){%>
          if(screen.width <= 480){
            var slot42 = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/wuni_<%=google_ad_page_name%>_mbl_lb_468x60',[300, 50], "blst_42").addService(googletag.pubads());
            var slot82 = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/wuni_<%=google_ad_page_name%>_mbl_lb1_468x60',[300, 50], "blst_82").addService(googletag.pubads());
            var slot43 = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/wuni_<%=google_ad_page_name%>_mbl_lb_468x60',[300, 50], "blst_43").addService(googletag.pubads());
            var slot83 = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/wuni_<%=google_ad_page_name%>_mbl_lb1_468x60',[300, 50], "blst_83").addService(googletag.pubads());
          }else{
            var slot42 = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/wuni_<%=google_ad_page_name%>_lb_468x60',[728, 90], "blst_42").addService(googletag.pubads());
            var slot82 = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/wuni_<%=google_ad_page_name%>_lb1_468x60',[728, 90], "blst_82").addService(googletag.pubads());
            var slot43 = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/wuni_<%=google_ad_page_name%>_lb_468x60',[728, 90], "blst_43").addService(googletag.pubads());
            var slot83 = googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/wuni_<%=google_ad_page_name%>_lb1_468x60',[728, 90], "blst_83").addService(googletag.pubads());
          }
        <%}%>
        googletag.pubads().collapseEmptyDivs();
      }); 
    }
  </script>
  </c:if>
<%}%>
<jsp:include page="/jsp/thirdpartytools/include/createTargetingAttr.jsp" />
<c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
<script type='text/javascript'>
  googletag.cmd.push(function() {
	googletag.pubads().enableLazyLoad({
       fetchMarginPercent: 0,
       renderMarginPercent: 0,
       mobileScaling: 0.0
    });
   googletag.enableServices();
  }); 
</script>
</c:if>
