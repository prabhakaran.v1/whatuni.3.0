
<%@page import="com.itextpdf.text.log.SysoCounter"%>
<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil, java.util.HashMap, WUI.utilities.SessionData, WUI.utilities.GlobalConstants, WUI.utilities.GlobalFunction" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
 
<%-- 
  wu314_20110712_Syed: Google-Analytics code for www.whatuni.com  
--%>

<%-- Google-Analytics code commented during development     --%>
<% 
   String appServerName = new CommonUtil().getDbName();
   CommonUtil commonUtil = new CommonUtil();	  
   if(appServerName !=null && appServerName.trim().length()==0){
    appServerName = "www.whatuni.com";
   } 
   request.setAttribute("appServerName", appServerName);      
   /*End of code added*/ 
   int stind1 = request.getRequestURI().lastIndexOf("/");
   int len1 = request.getRequestURI().length();
   String pageName = "";
   if(pageContext.getAttribute("pagename3") != null){
	   pageName = (String)pageContext.getAttribute("pagename3");
	}else{
	   pageName = request.getRequestURI().substring(stind1+1,len1);
	}
   pageName = pageName !=null ? pageName.toLowerCase() : pageName;
   
   if((!GenericValidator.isBlankOrNull(pageName)) && (("whystudysearchlanding.jsp").equalsIgnoreCase(pageName))){
    pageName = "regionhome.jsp";
    request.setAttribute("getInsightName","regionhome.jsp"); //28_OCT_14 Added by Amir
    request.setAttribute("cDimLevel","Undergraduate");
   }//Modified on 21_NOV_2013 FOR FIXING PAGENAME
   //
   // Added for logging advance search interstitial 23_Aug_2018, By Sabapathi.S
   if((!GenericValidator.isBlankOrNull(pageName)) && (("interstitialsearch.jsp").equalsIgnoreCase(pageName))){
    pageName = "advancesearch.jsp";
   }
   //   
   if((!GenericValidator.isBlankOrNull(pageName)) && (("degreesubjectguide.jsp").equalsIgnoreCase(pageName))){
    pageName = "categoryhome.jsp";
   }//Modified on 21_NOV_2013 FOR FIXING PAGENAME
   
   if((!GenericValidator.isBlankOrNull(pageName)) && (("mywhatuni.jsp").equalsIgnoreCase(pageName))){
    if(request.getAttribute("comparegapagename") != null){
      pageName = (String)request.getAttribute("comparegapagename");
    }
   }//Modified on 25_MAR_2014    
   if((!GenericValidator.isBlankOrNull(pageName)) && (("comparebasket.jsp").equalsIgnoreCase(pageName))){
    if(request.getAttribute("comparegapagename") != null){
      pageName = (String)request.getAttribute("comparegapagename");
    }
   }
   if((!GenericValidator.isBlankOrNull(pageName)) && (("newUser.jsp").equalsIgnoreCase(pageName)  || ("retUser.jsp").equalsIgnoreCase(pageName) ||("loggedIn.jsp").equalsIgnoreCase(pageName))){
    pageName = "home.jsp";
   }
   
   if((!GenericValidator.isBlankOrNull(pageName)) && (("overviewpage.jsp").equalsIgnoreCase(pageName))){           
    pageName = "aboutus.jsp";           
    request.setAttribute("getInsightName","aboutus.jsp"); //28_OCT_14 Added by Amir
   }
   if((!GenericValidator.isBlankOrNull(pageName)) && (("coursedetails.jsp").equalsIgnoreCase(pageName))){    
    if("TRUE".equals((String)request.getAttribute("CLEARING_COURSE"))){
       pageName = "clearingCourseDeatils.jsp";           
    }
   }
   //   
   if((!GenericValidator.isBlankOrNull(pageName)) && (("richprofilelanding.jsp").equalsIgnoreCase(pageName))){           
    if(request.getAttribute("richProfileType")!=null && "RICH_INST_PROFILE".equalsIgnoreCase((String)request.getAttribute("richProfileType"))){
      pageName = "richuniview.jsp";      
      request.setAttribute("getInsightName","uniview.jsp"); //13_JAN_15 Added by Amir
    }else if(request.getAttribute("richProfileType")!=null && "RICH_SUB_PROFILE".equalsIgnoreCase((String)request.getAttribute("richProfileType"))){
      pageName = "richsubjectprofile.jsp";
      request.setAttribute("getInsightName","subjectprofile.jsp"); //13_JAN_15 Added by Amir
    }
  }
  
  if((!GenericValidator.isBlankOrNull(pageName)) && (("reviewhome.jsp").equalsIgnoreCase(pageName))){
    pageName = "morereviews.jsp";
    request.setAttribute("getInsightName","morereviews.jsp");
  }
  //03_FEB_15 Added by Amir for Review Redesign
  if((!GenericValidator.isBlankOrNull(pageName)) && (("reviewsearchresults.jsp").equalsIgnoreCase(pageName))){ 
    String srchType = "";
    if(request.getAttribute("reviewSrchType")!=null && !"".equals(request.getAttribute("reviewSrchType"))){
      srchType = (String)request.getAttribute("reviewSrchType");
    }
    if(!"".equals(srchType) && "keyword".equals(srchType)){ 
      pageName = "morereviews.jsp";
      if(request.getAttribute("reviewInsType")!=null && !"".equals(request.getAttribute("reviewInsType"))){
        request.setAttribute("getInsightName","providerreviews.jsp");        
      }else{
        request.setAttribute("getInsightName","morereviews.jsp");
      }  
    }
    if(!"".equals(srchType) && "subject".equals(srchType)){ 
      pageName = "subjectreviews.jsp";
      request.setAttribute("getInsightName","subjectreviews.jsp");
    }
    if(!"".equals(srchType) && "provider".equals(srchType)){ 
      pageName = "providerreviews.jsp";
      request.setAttribute("getInsightName","providerreviews.jsp");
    }   
  }
  //03_FEB_15 Added by Amir for Review Redesign
  if((!GenericValidator.isBlankOrNull(pageName)) && (("providerreviewresults.jsp").equalsIgnoreCase(pageName))){
    String srchType = "";
    if(request.getAttribute("reviewSrchType")!=null && !"".equals(request.getAttribute("reviewSrchType"))){
      srchType = (String)request.getAttribute("reviewSrchType");
    }
    if(!"".equals(srchType) && "provider".equals(srchType)){ 
      pageName = "providerreviews.jsp";
      request.setAttribute("getInsightName","providerreviews.jsp");
    }    
  }   
  //17_Mar_2015 - added condition to retain existing jsp file name for open day landing pages, by Thiyagu G.
  if((!GenericValidator.isBlankOrNull(pageName)) && (("opendayslandingPage.jsp").equalsIgnoreCase(pageName) || ("opendayslandingdate.jsp").equalsIgnoreCase(pageName))){
    pageName = "opendaybrowse.jsp";
   }
   if(("opendaysearchresults.jsp").equalsIgnoreCase(pageName)){
     pageName = "opendaysearch.jsp";
   }
  //17_MAR_2015 Added by Amir to Retain opendays provider landing page.
  if((!GenericValidator.isBlankOrNull(pageName)) && (("opendaysProviderLanding.jsp").equalsIgnoreCase(pageName) || ("opendayproviderlandingpage.jsp").equalsIgnoreCase(pageName))){
    pageName = "viewopendaysnew.jsp";
    request.setAttribute("getInsightName",pageName);
  }
  if((!GenericValidator.isBlankOrNull(pageName)) && (("studentchoiceresult_2015.jsp").equalsIgnoreCase(pageName))){
    request.setAttribute("getInsightName","studentawards.jsp");
    String loadyear = request.getAttribute("loadYear") != null ? (String)request.getAttribute("loadYear") : "";//Added Loadyear by Indumathi.S Mar_29_2016
    if(!GenericValidator.isBlankOrNull(loadyear)){
      pageName = "studentchoiceresult_"+loadyear+".jsp";
    }    
  }  
  //19_May_2015 - added condition to retain existing jsp file name for mywhatui settings pages, by Thiyagu G.
  if((!GenericValidator.isBlankOrNull(pageName)) && (("mysettings.jsp").equalsIgnoreCase(pageName))){
    pageName = "mywhatuni.jsp";
  }
  
  if((!GenericValidator.isBlankOrNull(pageName)) && ("newproviderhome.jsp".equals(pageName) || "clearingprofile.jsp".equalsIgnoreCase(pageName))){
    String newProfileType = (String)request.getAttribute("setProfileType");
    if((!GenericValidator.isBlankOrNull(newProfileType)) && "CLEARING_PROFILE".equals(newProfileType)){       
      pageName = "clearinguniprofile.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "CLEARING_LANDING".equals(newProfileType)){
      pageName = "clearingunilanding.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "NORMAL".equals(newProfileType)){
      pageName = "uniview.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "SUB_PROFILE".equals(newProfileType)){
      pageName = "subjectprofile.jsp";
    }    
  }
  //21_July_2015 - added condition to retain existing jsp file name for Find a course pages, by Thiyagu G.
  if((!GenericValidator.isBlankOrNull(pageName)) && (("coursesearch.jsp").equalsIgnoreCase(pageName))){
    pageName = "categoryhome.jsp";
  }
  String lightBoxPageType = request.getParameter("lightBoxPageType");
  if(!GenericValidator.isBlankOrNull(lightBoxPageType) && ("single cpc landing lightbox".equalsIgnoreCase(lightBoxPageType) || "multi cpc landing lightbox".equalsIgnoreCase(lightBoxPageType))){
	   pageName = lightBoxPageType;
  }
  String studyLevelDesc = request.getAttribute("studyLevelDesc") != null ? (String)request.getAttribute("studyLevelDesc") : "";    
   
  String gaPageName   = (String) request.getAttribute("GAPageName"); //10_DEC_2013_REL -- // 03_JUNE_2014_REL change this to generic one, if anybody want to ganame different to jsp name, please set value to this attribute.
         gaPageName   = !GenericValidator.isBlankOrNull(gaPageName) ? gaPageName : pageName;
         pageName     = gaPageName;
  String gaCollegeId  = (String) request.getAttribute("hitbox_college_id");
         gaCollegeId  = !GenericValidator.isBlankOrNull(gaCollegeId) ? gaCollegeId : "";
  String gacourseTitle = (String)request.getAttribute("hitbox_course_title");
         gacourseTitle = !GenericValidator.isBlankOrNull(gacourseTitle) ? gacourseTitle : "";
  String gaCollegeName = (String)request.getAttribute("hitbox_college_name");
         gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(gaCollegeName);
         gaCollegeName = !GenericValidator.isBlankOrNull(gaCollegeName) ? !gaCollegeName.equals("null") ? gaCollegeName.toLowerCase().trim().replaceAll(" ","-") : "" : "";
   
   String gakeyword = (String) request.getSession().getAttribute("sc_prob42_keyword");
   String gacategory = (String) request.getSession().getAttribute("sc_prob43_category");
   String galocation = (String) request.getSession().getAttribute("sc_prob44_location"); 
   String ganetworkid = (String) request.getSession().getAttribute("cpeQualificationNetworkId");
   
   String ganetworkName = "";
   if(!GenericValidator.isBlankOrNull(ganetworkid) && "2".equals(ganetworkid)){
      ganetworkName = "ug";
   } else if(!GenericValidator.isBlankOrNull(ganetworkid) && "3".equals(ganetworkid)) {
      ganetworkName = "pg";
   }
   String sc45_coIds = (String)request.getAttribute("sc_prob45_collegeIds");
          sc45_coIds = !GenericValidator.isBlankOrNull(sc45_coIds) ? sc45_coIds : "";
   String sc46_coIds = (String)request.getAttribute("sc_prob46_collegeIds");
          sc46_coIds = !GenericValidator.isBlankOrNull(sc46_coIds) ? sc46_coIds : "";
   String sc47_coIds = (String)request.getAttribute("sc_prob47_collegeIds");
          sc47_coIds = !GenericValidator.isBlankOrNull(sc47_coIds) ? sc47_coIds : "";
   String sc48_coIds = (String)request.getAttribute("sc_prob48_collegeIds");
          sc48_coIds = !GenericValidator.isBlankOrNull(sc48_coIds) ? sc48_coIds : "";
   String sc49_coIds = (String)request.getAttribute("sc_prob49_collegeIds");
          sc49_coIds = !GenericValidator.isBlankOrNull(sc49_coIds) ? sc49_coIds : "";
   String sc50_coIds = (String)request.getAttribute("sc_prob50_collegeIds");
          sc50_coIds = !GenericValidator.isBlankOrNull(sc50_coIds) ? sc50_coIds : "";
   String pageNo = (String) request.getAttribute("pageno");
          pageNo = !GenericValidator.isBlankOrNull(pageNo) ? pageNo : "1";
          
   String pageType = (String)request.getAttribute("setPageType");
   if("browsemoneypageresults".equals(pageType)){
     pageName = "browsemoneypageresults.jsp";
   }else if("moneypageresults".equals(pageType)){
     pageName = "moneypageresults.jsp";
   }
   new GlobalFunction().getCustomDimesnionValues(pageName, request);
   //Added the below logic to set the value for Dimension 6 on 11_July_2017, By Thiyagu G.
   String prPageNames = GlobalConstants.PRV_PAGE_NAMES;
   SessionData sessionData = new SessionData();
   String prvDim = sessionData.getData(request, "prvDimension");
   if(GenericValidator.isBlankOrNull(prvDim)){
     sessionData.addData(request, response, "prvDimension", "No");
   }
   if(prPageNames.contains(pageName) && "No".equals(sessionData.getData(request, "prvDimension"))){
    sessionData.addData(request, response, "prvDimension", "Yes");
   }
   String userId = (String)request.getAttribute("cDimUID");
   String schoolName = (String)request.getAttribute("cDimSchool");
   String sourceType = (String)request.getAttribute("dimSourceType");
   String schoolUrn = (String)request.getAttribute("dimSchoolUrn");
   //
   String quizDimension11 = sessionData.getData(request, "quizDimension11");
   String quizDimension10 = sessionData.getData(request, "quizDimension10");
   //
   String ebookDimension14 = (String)request.getAttribute("ebookDim14");
   String applyFlag = (String)request.getParameter("applyflag");
   String openDayEventType = (String)request.getAttribute("openDayEventType");
   String clearingUserType = (String)session.getAttribute("USER_TYPE") ;
   //
   //
   if((!GenericValidator.isBlankOrNull(pageName)) && ("clearingebooklandingpage.jsp".equals(pageName) || "clearingebooksuccess.jsp".equals(pageName)) ) {
      pageName = "leadcapture";
   }
   if((!GenericValidator.isBlankOrNull(pageName)) && ("whatuniGoSignIn.jsp".equalsIgnoreCase(pageName) || "whatuniGoSignUp.jsp".equalsIgnoreCase(pageName) || "provisionalOfferLandingPage.jsp".equalsIgnoreCase(pageName) || "newEntryReqLandingPage.jsp".equalsIgnoreCase(pageName) || "whatuniGoSignUp.jsp".equalsIgnoreCase(pageName) || "provisionalOfferLandingPage.jsp".equalsIgnoreCase(pageName) || "allErrorPage.jsp".equalsIgnoreCase(pageName))){
     pageName = "applynowform";
   }else if((!GenericValidator.isBlankOrNull(pageName)) && ("whatuniGoLandingPage.jsp".equalsIgnoreCase(pageName))){
     pageName = "acceptofferform";
   }else if((!GenericValidator.isBlankOrNull(pageName)) && ("gradeFilterLandingPage.jsp".equalsIgnoreCase(pageName) || "gradefiltermain.jsp".equalsIgnoreCase(pageName) || "newusergradefiltermain.jsp".equalsIgnoreCase(pageName))) {
     pageName = "gradefilter";
   }else if((!GenericValidator.isBlankOrNull(pageName)) && "applicationpage.jsp".equalsIgnoreCase(pageName)) {
     pageName = "applicationpage";
   } else if((!GenericValidator.isBlankOrNull(pageName)) && "newUserGradeLandingPage.jsp".equalsIgnoreCase(pageName)) {
     pageName = "gradefilter";
   }else if((!GenericValidator.isBlankOrNull(pageName)) && "offerSuccess.jsp".equalsIgnoreCase(pageName)){
      String actionName = (String)request.getAttribute("actionName");
      if("provisonal-offer-congrats".equalsIgnoreCase(actionName)){
        pageName = "applynowform";
      }
   }else if((!GenericValidator.isBlankOrNull(pageName)) && "singleCPCLandingPage.jsp".equalsIgnoreCase(pageName)){
	   pageName = "single cpc landing";
   }else if((!GenericValidator.isBlankOrNull(pageName)) && "multipleCPCLandingPage.jsp".equalsIgnoreCase(pageName)){
	   pageName = "multi cpc landing"; 
   }   
   String featuredCollegeName = (String)request.getAttribute("featuredCollegeName");
   String sponsoredInstName = !GenericValidator.isBlankOrNull((String)request.getAttribute("sponsoredInstName")) ? (String)request.getAttribute("sponsoredInstName") : "";
   String boostedInstName = !GenericValidator.isBlankOrNull((String)request.getAttribute("boostedInstName")) ? (String)request.getAttribute("boostedInstName") : "";
   String sponsored_category = "Sponsored SR";
   String sponsored_type_manual = "Manual";
   String sponsored_type_automatic = "Sponsored";
   String sponsored_type_featured = "Featured";
   String sponsored_type_none = "None";
   String gaSetValue = "";
   String sponsored_list_flag = !GenericValidator.isBlankOrNull((String)request.getAttribute("sponsoredListFlag")) ? (String)request.getAttribute("sponsoredListFlag") : "";
   String manual_boosting_flag = !GenericValidator.isBlankOrNull((String)request.getAttribute("manualBoostingFlag")) ? (String)request.getAttribute("manualBoostingFlag") : "";
   String cpcClearingFlag = request.getAttribute("cpcClearingFlag") != null ? (String)request.getAttribute("cpcClearingFlag") : "";
%>
<%--Changed to new UA Amir 24-Nov-15 release--%>
  <script type="text/javascript">
    var pageLoadedTime = new Date().getTime();
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    <c:set var="googleAnalyticId" value="<%=GlobalConstants.GA_ACCOUNT%>"/>
   <c:choose>
    <c:when test="${'N' ne sessionScope.sessionData['cookiePerformanceDisabled']}">
      window['ga-disable-${googleAnalyticId}'] = true;
    </c:when>
    <c:when test="${'N' eq sessionScope.sessionData['cookiePerformanceDisabled']}">
      window['ga-disable-${googleAnalyticId}'] = false;
    </c:when>
   </c:choose>
   // ga('create', '<%=GlobalConstants.GA_ACCOUNT%>', 'auto');  //TODO CHANGE THIS ID TO LIVE : UA-22939628-2 OR TEST ID : UA-37421000-1    
    ga('create', '<%=GlobalConstants.GA_ACCOUNT%>', 'auto', {'useAmpClientId': true});
    ga('require', 'displayfeatures');            
    
    ga('set', 'dimension1', "<%=pageName%>");
    <%if(!GenericValidator.isBlankOrNull(gacourseTitle)){%>
      ga('set', 'dimension2', "<%=gacourseTitle%>");      
    <%}%>  
    <%if(!GenericValidator.isBlankOrNull(gaCollegeName)){%>
      ga('set', 'dimension3', "<%=gaCollegeName%>");      
    <%}%> 
    <%if((!GenericValidator.isBlankOrNull(pageName)) && (("uniview.jsp").equalsIgnoreCase(pageName) || ("viewopendaysnew.jsp").equalsIgnoreCase(pageName))){
      if(("true").equals((String)request.getAttribute("nonAdvertFlag"))){%>        
        ga('set', 'dimension4', 'NonAdvertiser');      
    <%}else{%>            
        ga('set', 'dimension4', 'Advertiser');
    <%}}%>    
    <%if((!GenericValidator.isBlankOrNull(pageName)) && (("richuniview.jsp").equalsIgnoreCase(pageName) || ("contenthub.jsp").equalsIgnoreCase(pageName) || ("applynowform").equalsIgnoreCase(pageName) 
    		|| ("acceptofferform").equalsIgnoreCase(pageName) || ("applicationpage").equalsIgnoreCase(pageName) || ("clearingProfile.jsp").equalsIgnoreCase(pageName))){%> 
      ga('set', 'dimension4', 'Advertiser');
    <%}%>    
    <% if(!GenericValidator.isBlankOrNull(quizDimension10)) {%>
      ga('set', 'dimension10', "<%=quizDimension10%>");
    <%}%>
    <%if(!GenericValidator.isBlankOrNull(quizDimension11)) {%>
      ga('set', 'dimension11', "<%=quizDimension11%>");
    <%}%>
    <%if(!GenericValidator.isBlankOrNull(ebookDimension14)) {%>
    ga('set', 'dimension14', "<%=ebookDimension14%>");
  <%}%>
   <%if(!GenericValidator.isBlankOrNull(applyFlag) && "Y".equalsIgnoreCase(applyFlag)) {%>
    ga('set', 'dimension14', "WUGO");
  <%}%>
  <%if((!GenericValidator.isBlankOrNull(studyLevelDesc) && "Clearing".equalsIgnoreCase(studyLevelDesc)) || "CLEARING".equalsIgnoreCase(clearingUserType) || ("multi cpc landing".equalsIgnoreCase(pageName) && "Y".equalsIgnoreCase(cpcClearingFlag))) {%>
     ga('set', 'dimension14', "Clearing");
  <%}%>
   
    <%if(!GenericValidator.isBlankOrNull(studyLevelDesc)){
    //Added for fix duplicate study level by Prabha on 21_Feb_17
    HashMap cDimLevelMap = new HashMap();
    cDimLevelMap.put("degree","Degree");
    cDimLevelMap.put("postgraduate","Postgraduate");
    cDimLevelMap.put("hnd/hnc","HND/HNC");
    cDimLevelMap.put("access and foundation","Access and foundation");
    cDimLevelMap.put("foundation degree","Foundation degree");

    String cDimLevel =  (String)cDimLevelMap.get(studyLevelDesc);
    studyLevelDesc = !GenericValidator.isBlankOrNull(cDimLevel) ? cDimLevel : studyLevelDesc;
    //End of duplicate study level
    //Added to log dimension 5 as clearing for June_5th release by Sangeeth.S
    String url = request.getQueryString();
    if(!GenericValidator.isBlankOrNull(url) && url.indexOf("clearing") > -1){
      studyLevelDesc = "Clearing";
    }
    %>
      //console.log("<%=url%>"+"study"+ "<%=studyLevelDesc%>");
      ga('set', 'dimension5', "<%=studyLevelDesc%>");      
    <%}%>
    //Added Dimension 6 on 11_July_2017, By Thiyagu G.
    ga('set', 'dimension6', '<%=sessionData.getData(request, "prvDimension")%>');
    //Added below dimension 7, 8, 9 by Prabha on 08_Aug_17
    <%if(!GenericValidator.isBlankOrNull(userId)){%>      
      ga('set', 'dimension7', '<%=userId%>');
    <%}
    if(!GenericValidator.isBlankOrNull(schoolName) && !GenericValidator.isBlankOrNull(schoolUrn)){%>      
      ga('set', 'dimension8', "<%=schoolName.toLowerCase()%>, School URN:<%=schoolUrn%>");
    <%}
    if(!GenericValidator.isBlankOrNull(sourceType)){%>      
      ga('set', 'dimension9', "<%=sourceType.toLowerCase()%>");
    <%}
    //Added below dimension15 for November_21_19 rel by Sangeeth.S
    String userUcasPoint = (String)request.getAttribute("scoreValue");
    if(GenericValidator.isBlankOrNull(userUcasPoint)){   		
      userUcasPoint = (String)session.getAttribute("USER_UCAS_SCORE");   
    }
    if(!GenericValidator.isBlankOrNull(userUcasPoint)){      
      if(userUcasPoint.indexOf(",") > -1){
        userUcasPoint = userUcasPoint.substring(userUcasPoint.lastIndexOf(",") + 1);
      }%>
      ga('set', 'dimension15', "<%=userUcasPoint%>");
    <%}%>
    //End of adding dimensions code by Prabha on 8_Aug_17
    
    //Added below dimension16 for featured brand, manual boosting and sponsored listing - added by Sujitha for March_03_2020 rel
    <%if(("moneypageresults.jsp".equalsIgnoreCase(pageName)) || ("browsemoneypageresults.jsp".equalsIgnoreCase(pageName))){%>   
      <%if(request.getAttribute("featuredBrandList") != null && request.getAttribute("featuredBrandList") != "" && "MANUAL_BOOSTING".equalsIgnoreCase(manual_boosting_flag) && "SPONSORED_LISTING".equalsIgnoreCase(sponsored_list_flag)){
          gaSetValue = sponsored_type_manual + "|" + sponsored_type_automatic + "|" + sponsored_type_featured;
        }else if(request.getAttribute("featuredBrandList") != null && request.getAttribute("featuredBrandList") != ""){
          if("MANUAL_BOOSTING".equalsIgnoreCase(manual_boosting_flag)){ 
            gaSetValue = sponsored_type_manual + "|" + sponsored_type_featured;
          }else if("SPONSORED_LISTING".equalsIgnoreCase(sponsored_list_flag)){
            gaSetValue = sponsored_type_automatic + "|" + sponsored_type_featured;
          }else{
            gaSetValue = sponsored_type_featured;
          }
        }else if("MANUAL_BOOSTING".equalsIgnoreCase(manual_boosting_flag) && "SPONSORED_LISTING".equalsIgnoreCase(sponsored_list_flag)){
          gaSetValue =  sponsored_type_manual + "|" + sponsored_type_automatic;
        }else if("MANUAL_BOOSTING".equalsIgnoreCase(manual_boosting_flag)){
          gaSetValue =  sponsored_type_manual;
        }else if("SPONSORED_LISTING".equalsIgnoreCase(sponsored_list_flag)){
          gaSetValue = sponsored_type_automatic;
        }else{
  	    gaSetValue = sponsored_type_none;
        }
      %>
      <%if(!GenericValidator.isBlankOrNull(gaSetValue)){%>
        ga('set', 'dimension16', "<%=gaSetValue%>");
      <%}%>   
    <%}%>
    //End of adding dimension16 
    
    //Added bellow dimension 18 for logging open day event type(Physical,virtual, online and combination ) by Sangeeth.S April_23_2020
    <%if(!GenericValidator.isBlankOrNull(openDayEventType)){
      openDayEventType = commonUtil.setOpenDayEventGALabel(openDayEventType);
      openDayEventType = commonUtil.setDimValWithEmptyPipe(openDayEventType);
      %>
      ga('set', 'dimension18', "<%=openDayEventType%>");
    <%}%>
    //
    
    <%if ((!GenericValidator.isBlankOrNull(studyLevelDesc) && "Clearing".equalsIgnoreCase(studyLevelDesc)) || "CLEARING".equalsIgnoreCase(clearingUserType)) {
		if (("browsemoneypageresults.jsp".equalsIgnoreCase(pageName)) || ("clearingcoursesearchresult.jsp".equalsIgnoreCase(pageName)) || ("moneypageresults.jsp".equalsIgnoreCase(pageName))) {
          if (request.getAttribute("matchTypeDim") != null && request.getAttribute("matchTypeDim") != "") {
            gaSetValue = (String) request.getAttribute("matchTypeDim");
		%>
			ga('set', 'dimension19', "<%=gaSetValue%>");
		<%}}
      }%>
    
    ga('send', 'pageview');    
    
    <%if("advicedetails.jsp".equalsIgnoreCase(pageName)){%>      
      setTimeout("ga('send','event', 'adjusted bounce rate', '40_seconds', 'article read')",40000); //Added UA logging for article details page alone for 08_Mar_2016 release, By Thiyagu G.
    <%}%>
    
    <%if(("moneypageresults.jsp".equalsIgnoreCase(pageName)) || ("browsemoneypageresults.jsp".equalsIgnoreCase(pageName))){%>   
      <%if("moneypageresults.jsp".equalsIgnoreCase(pageName)){%>
        <%if(Integer.parseInt(pageNo) <=1  && (!GenericValidator.isBlankOrNull(gakeyword))  && (!GenericValidator.isBlankOrNull(galocation))){%>        
          ga('send', 'event', 'Search' , '<%=gakeyword%>' , '<%=galocation%>' , 1 , {nonInteraction: true}); //Added by Amir for UA logging for 24-NOV-15 release
        <%}%>
      <%}%>
      <%if("browsemoneypageresults.jsp".equalsIgnoreCase(pageName)){%>
       <%if(Integer.parseInt(pageNo) <=1 && (!GenericValidator.isBlankOrNull(gacategory))  && (!GenericValidator.isBlankOrNull(galocation))){%>        
          ga('send', 'event', 'Search' , '<%=gacategory%>' , '<%=galocation%>' , 2 , {nonInteraction: true}); //Added by Amir for UA logging for 24-NOV-15 release
        <%}%>
      <%}%>        
      //Below code is event logging of featured brand, manual boosting and sponsored listing - added by Sujitha for March_03_2020 rel
      <%if(request.getAttribute("featuredBrandList") != null && request.getAttribute("featuredBrandList") != ""){%>
        ga("send", "event", "<%=sponsored_category%>", "<%=sponsored_type_featured%>", "<%=featuredCollegeName%>");
      <%}%>
      <%if("SPONSORED_LISTING".equalsIgnoreCase(sponsored_list_flag)){%>
        ga("send", "event", "<%=sponsored_category%>", "<%=sponsored_type_automatic%>", "<%=sponsoredInstName%>");
      <%}%>
      <%if("MANUAL_BOOSTING".equalsIgnoreCase(manual_boosting_flag)){%>
        ga("send", "event", "<%=sponsored_category%>", "<%=sponsored_type_manual%>", "<%=boostedInstName%>");
      <%}%>
      //End of adding event logging code 
    <%}%>     
    function GAInteractionEventTracking(buttonType, interactonCat, interactionType, collegeName, cpeValue){
      eventTimeTracking();
      var booleanValue = true;
      if(buttonType == 'emailsubmit' || buttonType == 'emailwebform' || buttonType == 'prospectuswebform' || buttonType == 'visitwebsite' ||  buttonType == 'innerlinks' || buttonType == 'applynow' || buttonType == 'kisinner'){
        booleanValue = false;
      }
      var eventCategory = interactonCat;
      var eventAction = interactionType;
      var eventLabel = ((eventCategory != null && eventCategory.trim() == 'Lead Capture') || (buttonType != null && buttonType.trim() == 'leadcapturebutton')) ? collegeName : collegeName.toLowerCase().trim();
      if(eventCategory == "Opt In"){
    	  eventLabel = collegeName;
      }
      var optionVal = 1;
      if(cpeValue != undefined && cpeValue !=""){
        optionVal = cpeValue;
      }
      if(cpeValue == "0"){
        optionVal = cpeValue;
      }
      //TODO CHANGE THIS ID TO LIVE : UA-22939628-2 OR TEST ID : UA-37421000-1
      var eventVal = optionVal;     
      if(eventLabel == 'clearing courses'){
        ga('send', 'event', eventCategory , eventAction , eventLabel, {nonInteraction: booleanValue});
      }
      else{
        ga('send', 'event', eventCategory , eventAction , eventLabel , eventVal , {nonInteraction: booleanValue}); //Added by Amir for UA logging for 24-NOV-15 release
      }      
    }    
    function GARegistrationLogging(eventCategory, eventAction, eventValue,userId){//5_AUG_2014
      //TODO CHANGE THIS ID TO LIVE : UA-22939628-2 OR TEST ID : UA-37421000-1
      eventTimeTracking();
      if(eventAction == 'Email' || eventAction == 'Prospectus' || eventAction == 'Prospectus-dl'){
        if(userId!= "null" && userId != "" && userId!= null ){                   
          ga('send', 'event', eventCategory , eventAction , eventValue); //Added by Amir for UA logging for 24-NOV-15 release
        }
      }else{               
        ga('send', 'event', eventCategory , eventAction , eventValue); //Added by Amir for UA logging for 24-NOV-15 release
      }      
    }
    function GACovid19Logging(){
    	
    	GARegistrationLogging("Covid Banner", "<%=pageName%>", "Latest Info");
    }
    function GANewAnalyticsEventsLogging(eventCategory, eventAction, eventLabel, podType){
      eventTimeTracking();
      if('topnav' == podType){
    	  ga('set', 'dimension1', podType);
      }
      ga('send', 'event', eventCategory , eventAction , eventLabel); //Added by Amir for UA logging for 24-NOV-15 release      
    }
    //Added function for articles event logging for 08_MAR_2016, By Thiyagu G.
    function ArticlesGAEventsLogging(eventCategory, eventAction, eventLabel){
      eventTimeTracking();
      var width = document.documentElement.clientWidth;
      var eLabel = eventLabel;
      <% if(!("categoryhome.jsp".equalsIgnoreCase(pageName))) { %>
      if(eLabel != "See All"){
        if (width <= 480) {
          eLabel = "1";
        } else if ((width > 480) && (width <= 992)) {
          eLabel = "1";
        } else {
          eLabel = eventLabel;
        }
      }
      <% } 
      if("opendaybrowse.jsp".equalsIgnoreCase(pageName)){ //Added by sangeeth.S for July_3_18 rel
        pageName = pageName.substring(0, 1).toUpperCase() + pageName.substring(1);
      }%>
      
      var pagePod = '<%=pageName%>|'+eventAction;
      ga('send', 'event', eventCategory, pagePod, eLabel);      
    }
    //Added condition for Final 5 event logging for 08_MAR_2016, By Thiyagu G.
    function FinalFiveAnalyticsEventsLogging(eventCategory, eventAction, eventLabel){
     // var eventValue = document.getElementById(eventLabel).value;
      //eventLabel = eventValue.substring(1);      
      ga('create', 'UA-52581773-4', 'auto', {'name': 'newTracker'});
      ga('newTracker.send', 'event', eventCategory , eventAction , eventLabel); //Added by Amir for UA logging for 24-NOV-15 release
    }
    //Added for tracking event time triggered for NOV_20_18 rel by Sangeeth.S
    function eventTimeTracking(){
      var eventClickedTime = new Date().getTime();
      var eventClickSecTime = Math.round(Math.abs(eventClickedTime - pageLoadedTime)/1000);      
      if( 0 != eventClickSecTime){
        //console.log("set, dimension13, eventClickSecTime): " + Math.round(eventClickSecTime));
        ga('set', 'dimension13', ''+eventClickSecTime); 
      }      
    }
  </script>      
  <%-- END GOOGLE ANALYTICS CALL --%> 
  