<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  * @purpose  This is included for GAM banner js..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 05-Sep-2017 Prabhakaran V.               568      First Draft                                                       568 
  * *************************************************************************************************************************
--%>
<c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
<script type='text/javascript'>
  var googletag = googletag || {};
  googletag.cmd = googletag.cmd || [];
  (function() {
    var gads = document.createElement('script');
    gads.async = true;
    gads.type = 'text/javascript';
    var useSSL = 'https:' == document.location.protocol;
    gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
    var node = document.getElementsByTagName('script')[0];
    node.parentNode.insertBefore(gads, node);
  })();
</script>
</c:if>