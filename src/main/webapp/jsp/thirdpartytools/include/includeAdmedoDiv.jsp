<%@page import="WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
  * @purpose  This is included for admedo banner jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 05-Sep-2017 Prabhakaran V.               568      First Draft                                                       568 
  * *************************************************************************************************************************
--%>
<% 
String pageName = request.getParameter("pagename");
String admedoPages = GlobalConstants.ADMEDO_PAGE_NAMES;//"browsemoneypageresults.jsp, richprofilelanding.jsp, coursesearchresult.jsp, coursedetails.jsp, providerreviewresults.jsp, newproviderhome.jsp, opendaysprovlanding.jsp, clearinghome.jsp, collegeemailsuccess.jsp";
if(!GenericValidator.isBlankOrNull(pageName) && (admedoPages.contains(pageName.toLowerCase()) || "collegeemailsuccess.jsp".equalsIgnoreCase(pageName.toLowerCase()) || "qlbasicform.jsp".equalsIgnoreCase(pageName.toLowerCase()))){%>
  <div class="sky" id="admedoBanner" style="display: none;"></div>
  <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
    <script type="text/javascript">googletag.cmd.push(function() { googletag.display('admedoBanner');});</script>
  </c:if>
<%}%>