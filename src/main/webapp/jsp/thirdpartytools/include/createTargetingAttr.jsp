<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
</c:set>
<%--
  * @purpose  This is included for targeting and admedo slot defining..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 05-Sep-2017 Prabhakaran V.               568      First Draft                                                       568 
  * *************************************************************************************************************************
--%>
<%
  String gamKeywordOrSubject = request.getAttribute("gamKeywordOrSubject") != null ? (String) request.getAttribute("gamKeywordOrSubject") : "";
  String gamStudyLevelDesc = (String) session.getAttribute("gamStudyLevelDesc");      
     
  if(gamStudyLevelDesc == null || gamStudyLevelDesc.trim().length() == 0 ){
    gamStudyLevelDesc = (String) request.getAttribute("gamStudyLevelDesc");      
  }
  String entryPointsForGAM = "";
  if((String)session.getAttribute("entryPointsforGAM") != null){
    entryPointsForGAM = (String)session.getAttribute("entryPointsforGAM");
  }  
  String entryRequirementsForGAM = "";  
  if((String)session.getAttribute("entryRequirementsForGAM") != null){
    entryRequirementsForGAM = (String)session.getAttribute("entryRequirementsForGAM");
    session.removeAttribute("entryRequirementsForGAM");
  }
  String sectionName = (String)request.getAttribute("sectionName");
  String collegeId = (String)request.getAttribute("collegeId") != null ? (String)request.getAttribute("collegeId") : "";  
  String keyword = "";
   int stind1 = request.getRequestURI().lastIndexOf("/");
  int len1 = request.getRequestURI().length();
  String page_name = pageContext.getAttribute("pagename3") != null ?  (String)pageContext.getAttribute("pagename3") : request.getRequestURI().substring(stind1+1,len1); 
  String admedoPages = GlobalConstants.ADMEDO_PAGE_NAMES;//"browsemoneypageresults.jsp, richprofilelanding.jsp, coursesearchresult.jsp, coursedetails.jsp, providerreviewresults.jsp, newproviderhome.jsp, opendaysprovlanding.jsp, clearinghome.jsp, collegeemailsuccess.jsp";
  String clearingProfile = (String)request.getAttribute("clearingHeader");
  String clearingFlag = (String)request.getAttribute("searchClearing");
  String clearingCourse = (String)request.getAttribute("CLEARING_COURSE");
  String cDimCollegeId = (String)request.getAttribute("cDimCollegeId");
  if("".equals(collegeId)&& ("contenthub.jsp".equalsIgnoreCase(page_name) || "clearingProfile.jsp".equalsIgnoreCase(page_name))){
    collegeId = (String)request.getAttribute("COLLEGE_ID") != null ? (String)request.getAttribute("COLLEGE_ID") : "";
  }
%>
<c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
<script type="text/javascript">
  googletag.cmd.push(function() {
    <%if(admedoPages.contains(page_name.toLowerCase())){%>
      googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/<%=GlobalConstants.ADMEDO_PIXEL%>',[1, 1], "admedoBanner").addService(googletag.pubads());
    <%}else if("collegeemailsuccess.jsp".equalsIgnoreCase(page_name.toLowerCase()) || "qlbasicform.jsp".equalsIgnoreCase(page_name.toLowerCase())){%>
      googletag.defineSlot('/<%=GlobalConstants.NETWORK_CODE%>/<%=GlobalConstants.CONVERSION_PIXEL%>',[1, 1], "admedoBanner").addService(googletag.pubads());
    <%}
    
    if(!GenericValidator.isBlankOrNull(gamKeywordOrSubject)){ 
      gamKeywordOrSubject = gamKeywordOrSubject.replaceAll("\\+", "-");
      gamKeywordOrSubject = gamKeywordOrSubject.replaceAll(" ", "-");
      if(!GenericValidator.isBlankOrNull(gamStudyLevelDesc)){%>
        googletag.pubads().setTargeting('keyword', ["<%=gamKeywordOrSubject.toLowerCase()%>"]);
      <%}
      if(!GenericValidator.isBlankOrNull(gamStudyLevelDesc)){ %>
        googletag.pubads().setTargeting('hcchannels', "<%=gamStudyLevelDesc.toLowerCase()%>");
      <%}
      if(!GenericValidator.isBlankOrNull(entryPointsForGAM)){%>
        googletag.pubads().setTargeting('alvlgrades', "<%=entryPointsForGAM.toLowerCase()%>");
      <%}
      if(!GenericValidator.isBlankOrNull(entryRequirementsForGAM)){%>
        googletag.pubads().setTargeting('Entryreq', "<%=entryRequirementsForGAM%>");
      <%}
    }else{%>
      googletag.pubads().setTargeting('hcchannels', "degree");
    <%}
    if("clearinghome.jsp".equalsIgnoreCase(page_name)){
      sectionName = "Clearing";
    }
    if("coursedetails.jsp".equalsIgnoreCase(page_name)){
      sectionName = "Courses";
      if("TRUE".equalsIgnoreCase(clearingCourse)){
        sectionName = "clearingcourses";
      }
      if(!GenericValidator.isBlankOrNull(cDimCollegeId)){
        collegeId = cDimCollegeId;
      }
      keyword = !GenericValidator.isBlankOrNull((String)request.getAttribute("ldcsSubject")) ? ((String)request.getAttribute("ldcsSubject")).replaceAll(" ", "-").toLowerCase() : "";%>
      googletag.pubads().setTargeting('university', "<%=collegeId%>");
    <%}
    if(!GenericValidator.isBlankOrNull(keyword)){%>
      googletag.pubads().setTargeting('keyword', ["<%=keyword%>"]);
    <%}
    if(("richprofilelanding.jsp".equalsIgnoreCase(page_name) || "newproviderhome.jsp".equalsIgnoreCase(page_name) || "providerreviewresults.jsp".equalsIgnoreCase(page_name) || "opendaysprovlanding.jsp".equalsIgnoreCase(page_name) || "contenthub.jsp".equalsIgnoreCase(page_name) || "clearingProfile.jsp".equalsIgnoreCase(page_name)) && !GenericValidator.isBlankOrNull(collegeId)){
      if("TRUE".equalsIgnoreCase(clearingProfile)){
        sectionName = "Clearing";
      }
    %>
    googletag.pubads().setTargeting('university', "<%=collegeId%>");
    <%}
    
    if("coursesearchresult.jsp".equalsIgnoreCase(page_name) || "clearingCourseSearchResult.jsp".equalsIgnoreCase(page_name)){
      if("TRUE".equalsIgnoreCase(clearingFlag)){
        sectionName = "Clearing";
      }
    %>
      googletag.pubads().setTargeting('university', "<%=collegeId%>");
    <%}
    
    if("collegeemailsuccess.jsp".equalsIgnoreCase(page_name) || "qlbasicform.jsp".equalsIgnoreCase(page_name)){%>
      googletag.pubads().setTargeting('university', "<%=collegeId%>");
    <%}
    if(!GenericValidator.isBlankOrNull(sectionName)){%>
      googletag.pubads().setTargeting('section', "<%=sectionName%>");
    <%}
    %>
    
    googletag.enableServices();
  });
</script>
</c:if>