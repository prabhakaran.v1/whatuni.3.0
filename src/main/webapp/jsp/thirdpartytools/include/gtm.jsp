<%@page import="WUI.utilities.GlobalConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%
 String placeToInclude = (String)request.getParameter("PLACE_TO_INCLUDE");
%>
<c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
<%if("HEAD".equals(placeToInclude)) {%>
  <script type="text/javascript">
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','<%=GlobalConstants.GTM_ACCOUNT%>');
  </script>
<%} else if("BODY".equals(placeToInclude)){%>
   <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<%=GlobalConstants.GTM_ACCOUNT%>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<%}%>
</c:if>