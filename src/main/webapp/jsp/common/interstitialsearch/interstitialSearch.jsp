<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.SessionData" autoFlush="true" %>
<%@page import="com.wuni.util.seo.SeoUrls"%>
<%@page import="com.wuni.util.seo.SeoPageNamesAndFlags"%>
<%@page import="org.apache.commons.validator.GenericValidator"%>
<%@page import="WUI.utilities.SessionData"%>
<%@page import="WUI.utilities.CommonFunction"%>
<%@page import="WUI.utilities.GlobalConstants"%>
<%@page import="WUI.utilities.CommonUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%
String canonicalUrl = GlobalConstants.WHATUNI_SCHEME_NAME+GlobalConstants.WHATUNI_DOMAIN;
String ajaxDynamicListJSName = CommonUtil.getResourceMessage("wuni.ajaxDynamicList.js", null);
String interstitialJSName = CommonUtil.getResourceMessage("wuni.search.interstitial.js", null);
String gradeFilterJsName = CommonUtil.getResourceMessage("wuni.sr.grade.filter.js", null);
String qualification  = request.getParameter("qual");
String refererUrl = request.getHeader("referer");
String awardsURL = "/student-awards-winners/university-of-the-year/";
String clearingOnOffFlag = (String)request.getAttribute("CLEARING_ON_OFF_FLAG");
if(GenericValidator.isBlankOrNull(qualification)) {
  qualification = "degree";
}
request.setAttribute("qualification", qualification);
String qualparentClass = "";
if("ON".equals(clearingOnOffFlag)) {
  qualparentClass = "adjrady";
}
String qualificationCode = GenericValidator.isBlankOrNull((String)request.getAttribute("DEFAULT_QUAL_SELECTED")) ? "" : (String)request.getAttribute("DEFAULT_QUAL_SELECTED");
String qualificationTypeSelected = "Others";
request.setAttribute("qualificationTypeSelected", qualificationTypeSelected);
if("CLEARING".equals(qualificationCode)) {
  qualificationTypeSelected = "Clearing/Adjustment";
  request.setAttribute("qualificationTypeSelected", qualificationTypeSelected);
  qualificationCode = "M";
}
%>
  <body class="bdfix md non_log">
      
      <div id="ol" style="display: none; position: fixed; top: 0px; left: 0px; z-index: 998; width: 1280px; height: 100%;"></div>
      
      <div id="loadingImg" style="display:none; position: absolute; z-index: 3;">
        <div style="top: 0;position: fixed;left: 0;background: rgba(0,0,0,0.5);width: 100%;height: 100%;">
          <img style="position: absolute; top: 50%; left: 50%; margin-top: -32px; margin-left: -32px;" src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/loader.gif",0)%>" alt="loading image">
        </div>
      </div>
      
      <div id="mbox" style="display: none; position: absolute; z-index: 999;">
        <span>
          <div id="mbd"></div>
        </span>
      </div>
      <div class="advsrch advsrch_ui">         
		<div class="shdicn" style="display:none;">
        <div class="flipldr"></div>
      </div>
         
	  <div id="parentAdv" class="advcont">
        <div id="closeHere" class="advcls clrfx">
          <a href="javascript:void(0)" onclick="closeInterstitialPage('<%=refererUrl%>');"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/close.svg",0)%>" alt="close" width="20" height="20"></a>
        </div>
        <div class="advmid">
          <h2>Advanced search</h2>
               			   
			<%-- Start :: Qualification filter --%>
			<c:if test="${not empty requestScope.QUALIFICATION_LIST}">
              <div class="advrw" id="reviewPod">
                <h3 onclick="slideDownOrUp('qualParent', $$('qualToggle'), event)">
                  Qualification*
                  <div class="info-circle tlinfo tooltext">
                    <div class="tlpcnt tooltext">
                      <div class="tooltext">
                        <p class="tooltext">Undergraduate</p>
                          Essentially, a degree. Sometimes called BAs (Bachelor of Arts) or BSc (Bachelor of Science), these are level 6 qualifications that will go into a great deal of depth on a subject, lasting on average 3 years. They are academic in nature.
                      </div>
                      <div class="tooltext">
                        <p class="tooltext">Foundation Degree</p>
                          Still higher education but a step down from an undergraduate degree, a foundation degree will give you grounding in a subject but not go into as much depth. A level 5 qualification that usually lasts a year, these often have a practical element but are largely academic.
                      </div>
                      <div class="tooltext">
                        <p class="tooltext">Postgraduate</p>
                          The next step up from an undergraduate degree, postgraduate degrees enable you to become more specialised through either taught or research methods of learning. There are several types of postgraduate degree including masters, PhDs, PG Certificates, PG Diplomas, and PGCEs. These qualifications are very detailed and last between 1-4 years.
                      </div>
                      <div class="tooltext">
                        <p class="tooltext">Access & foundation</p>
                          Access and foundation courses are for students wishing to study a particular subject at university but require relevant or specific qualifications in order to do so. Access and foundation course are a level 3 qualification which introduces the subject, teaches the essentials and enables students to enter higher education. They usually last around one year.
                      </div>
                      <div class="tooltext">
                        <p class="tooltext">HND/HNC</p>
                          A HND (aka Higher National Diploma) is a level 5 qualification that is usually run at a college, not a university, and aims at getting you ready for the workplace. They take 2 years to complete and are the equivalent to the first 2 years of an academic degree. HNCs (aka Higher National Certificates) are only a year long and are level 4 qualifications. They also aim at getting you ready for work.
                      </div>
                    </div>
                  </div>
                  <span class="fa fa-angle-down" id="qualToggle"></span>
                </h3>
                <div id="qualParent" class="inptsr clrfx qualrady <%=qualparentClass%>">
                  <%String checked = "";%>
                  <c:forEach var="qualificationList" items="${requestScope.QUALIFICATION_LIST}" varStatus="row">
                    <c:set var="indexIntValue" value="${row.index}"/>
                    <input type="hidden" name="codequal_<%=pageContext.getAttribute("indexIntValue").toString()%>" id="codequal_<%=pageContext.getAttribute("indexIntValue").toString()%>" value='${qualificationList.qualCode}'/>
                    <div class="rady">
                      <%checked = "";%>
                      <c:if test="${requestScope.qualificationTypeSelected ne 'Clearing/Adjustment'}">
                        <c:if test="${qualification eq qualificationList.urlText}">
                          <%
                            checked = "checked='checked'";
                          %>
                        </c:if>
                      </c:if>
                      <c:if test="${qualificationTypeSelected eq qualificationList.qualDesc}">
                          <%
                            checked = "checked='checked'";
                          %>
                      </c:if>
                      <input type="radio" name="qual" class="qual_radio" id="qual_<%=pageContext.getAttribute("indexIntValue").toString()%>" <%=checked%> onclick="selectFilters('qual_<%=pageContext.getAttribute("indexIntValue").toString()%>', 'single')" value='${qualificationList.urlText}' >
                      <label for='qual_<%=pageContext.getAttribute("indexIntValue").toString()%>'><span id="labelqual_<%=pageContext.getAttribute("indexIntValue").toString()%>">${qualificationList.qualDesc}</span></label>
                    </div>
                  </c:forEach>
                </div>
              </div>
            </c:if>
            <%-- End :: Qualification filter --%>
        
            <%-- Start :: Subject filter --%>
			<div class="advrw">
              <h3>What do you want to study?*</h3>
              <div class="inptsr" id="subjectL1Div">
                <input type="text" placeholder="Enter subject" class="inptxt" id="interstitialSubjectSearch_l1" autocomplete="off" name="interstitialSubjectSearch_l1" value="${requestScope.sub_name}"
                     onkeyup="autoCompleteInterstitialSubjectSearch(event,this);searchKeyWordWhenSearched(event)"/>
                <input type="hidden" id="interstitialSubjectSearch_l1_id" value="${requestScope.sub_id}">
                <input type="hidden" id="interstitialSubjectSearch_l1_display" value="">
                <input type="hidden" id="interstitialSubjectSearch_l1_code" value="">
                <input type="hidden" id="interstitialSubjectSearch_l1_l2flag" value="">
                <span class="fa fa-search"></span>
              </div>
              <div id="sub_error_msg" style="display:none" class="errmsg">Please re-enter the Subject</div>
              <%-- Added the keyword error msg, 25_Sep_2018 By Sabapathi --%>
              <div id="keyowrd_error_msg" style="display:none" class="errmsg">Please enter the valid subject</div>
              <!-- <div id="subjectL2Div" class="inptsr" style="display:none">
                <div class="selopt">
                  <p class="sublhd">Choose a more specific form of <span id="l1subjectname"></span> (Optional)</p>
                  <div id="l2holder" class="select dis_edu">
                    <span id="interstitialSubjectSearch_l2_label" class="fnt_lbd">Please select</span>
                    <i class="fa fa-angle-down fa-lg"></i>
                  </div>
                  <select id="interstitialSubjectSearch_l2" onchange="manageSubjectLevelTwoDropdown(this)" class="dis_edu"> 
                    <option value="-1" selected="selected">Please select</option> 
                  </select>
                </div>
              </div> -->
            </div> 
	        <%-- End :: Subject filter --%>
			   
			<%-- Start :: Country filter --%>	
			<c:if test="${not empty requestScope.REGION_LIST}">
              <div class="advrw">
                <h3 onclick="slideDownOrUp('countryParent', $$('countryToggle'), event)">Country <span class="fa fa-angle-down" id="countryToggle" ></span></h3>
                <div id="countryParent" >
                  <div class="inptsr clrfx cntyrady">
                    <%String checked = "";%>
                    <c:forEach var="regionList" items="${requestScope.REGION_LIST}" varStatus="row">
                      <c:set var="rowIntValue" value="${row.index}"/>
                        <div class="rady">
                          <%checked = "";%>
                          <c:if test="${(regionList.urlText eq requestScope.loc_name)}">
                            <%
                              checked = "checked='checked'";
                            %>
                          </c:if>
                          <c:if test="${(regionList.regionName eq 'England') and not empty requestScope.loc_name and (regionList.urlText ne requestScope.loc_name)}">
                            <%
                              checked = "checked='checked'";
                            %>
                          </c:if>
                           <input type="radio" class="country_radio" name="country" id="country_<%=pageContext.getAttribute("rowIntValue").toString()%>" onclick="selectFilters('country_<%=pageContext.getAttribute("rowIntValue").toString()%>', 'single')" value='${regionList.urlText}' <%=checked%>>
                           <label for="country_<%=pageContext.getAttribute("rowIntValue").toString()%>"><span id="labelcountry_<%=pageContext.getAttribute("rowIntValue").toString()%>">${regionList.regionName}</span></label>
                        </div>
                     
                      </c:forEach>
                   </div>
                   <c:if test="${not empty requestScope.SUB_REGION_LIST}">
                       <div class="subcnty clrfx" id="sub_region_list" style="display:none;">
                       <c:forEach var="regionList" items="${requestScope.SUB_REGION_LIST}" varStatus="row">
                          <c:set var="rowValue" value="${row.index}"/>
                            <div class="subrw">  
                            <%checked = "";%>                          
                            <c:if test="${regionList.urlText eq requestScope.loc_name or (regionList.urlText eq 'greater-london' and requestScope.loc_name eq 'london')}">
                              <script type="text/javascript">selectFilters('country_1', 'single');</script>
                              <%
                                checked = "checked='checked'";
                              %>
                            </c:if>
                              <input type="radio" class="subcountry_radio" name="subcountry" id="subcountry_<%=pageContext.getAttribute("rowValue").toString()%>" onclick="selectFilters('subcountry_<%=pageContext.getAttribute("rowValue").toString()%>', 'single')" value='${regionList.urlText}' <%=checked%> >
                              <label for="subcountry_<%=pageContext.getAttribute("rowValue").toString()%>"><span id="labelsubcountry_<%=pageContext.getAttribute("rowValue").toString()%>">${regionList.regionName}</span></label>
                            </div>
                          </c:forEach>
                       </div>
                  
                   </c:if>
                </div>
             </div>
       </c:if>
			   <%-- End :: Country filter --%>
			   
			   <%-- Start :: Location type filter --%>
			   <c:if test="${not empty requestScope.LOCATION_TYPE_LIST}">
     
             <div class="advrw">
                <h3  onclick="slideDownOrUp('locationParent', $$('locationToggle'), event)">Location type <span class="fa fa-angle-down" id="locationToggle"></span></h3>
                <div class="chkcont">
                   <p>Select as many as you want</p>
                   <div id="locationParent" class="inptsr clrfx loctyp">
                   <c:forEach var="locationList" items="${requestScope.LOCATION_TYPE_LIST}" varStatus="row">
                     <c:set var="rowInt" value="${row.index}"/>
                        <div class="rady">
                           <input type="checkbox" class="loc_check" name="locationType" id="locationType_<%=pageContext.getAttribute("rowInt").toString()%>" onclick="selectFilters('locationType', 'multiple', this)"  value='${locationList.urlText}' >
                           <label for='locationType_<%=pageContext.getAttribute("rowInt").toString()%>'>
                           <c:set var="optionName" value="${locationList.optionDesc}"/>
                              <jsp:include page="/jsp/common/interstitialsearch/interstitialSearchUtilAVG.jsp" >
                                <jsp:param name="SVG_TYPE" value='<%=pageContext.getAttribute("optionName").toString()%>'/>
                              </jsp:include>
                              <span id="labellocationType_<%=pageContext.getAttribute("rowInt").toString()%>">${locationList.optionDesc}</span>
                           </label>
                           <input type="hidden" name="urlText<%=pageContext.getAttribute("rowInt").toString()%>" id="urlText<%=pageContext.getAttribute("rowInt").toString()%>" value='${locationList.urlText}'/>
                        </div>
                     
                      </c:forEach>
                   </div>
                </div>
             </div>

         </c:if>
			   <%-- End :: Location type filter --%>
			   
			   <%-- Start :: How you study filter --%>
			   <c:if test="${not empty requestScope.STUDY_MODE_LIST}">
               <div class="advrw">
                    <h3 onclick="slideDownOrUp('studymodeParent', $$('studyToggle'), event)">
                       How you study
                       <div class="info-circle tlinfo tooltext">
                          <div class="tlpcnt tlpcnthw tooltext">
                             <div class="tooltext">
                                <p class="tooltext">Full-time</p>
                                Monday-Friday, 9-5, a full-time course will take up most of your time but is the quickest to complete in terms of years.
                             </div>
                             <div class="tooltext">
                                <p class="tooltext">Sandwich</p>
                                A full-time course that has a year doing something else is usually called a sandwich course. This could be spending time in industry or it could mean studying abroad, it depends on the course.
                             </div>
                             <div class="tooltext">
                                <p class="tooltext">Part-time</p>
                                Part-time courses usually take longer to do in terms of years but mean you can do other things around your studies, such as keeping a job.
                             </div>
                             <div class="tooltext">
                                <p class="tooltext">Distance/online</p>
                                Distance and online courses are ones you do largely by yourself, in the comfort of your home. Some include time in college/university, but most are done at a computer, alone.
                             </div>
                          </div>
                       </div>
                       <span class="fa fa-angle-down" id="studyToggle"></span>
                    </h3>
                    <div id="studymodeParent" class="inptsr clrfx hwsty">
                       <%String checked = "";%>
                       <c:forEach var="studymodeList" items="${requestScope.STUDY_MODE_LIST}" varStatus="row">
                     <c:set var="rowsInt" value="${row.index}"/>
                         <div class="rady">
                            <%checked = "";%>
                            <c:if test="${studymodeList.studymodeDesc eq 'All study types'}">
                           
                            <%
                              checked = "checked='checked'";
                            %>
                           
                           </c:if>
                            <input type="radio" class="study_radio" name="studymode" id="studymode_<%=pageContext.getAttribute("rowsInt").toString()%>" onclick="selectFilters('studymode_<%=pageContext.getAttribute("rowsInt").toString()%>', 'single')"  value='${studymodeList.urlText}' <%=checked%>>
                            <label for='studymode_<%=pageContext.getAttribute("rowsInt").toString()%>'><span id="labelstudymode_<%=pageContext.getAttribute("rowsInt").toString()%>">${studymodeList.studymodeDesc}</span></label>
                         </div>
                    
                       </c:forEach>
                    </div>
                 </div>
</c:if>
           <%-- End :: How you study filter --%>
           
           <%-- Start :: Previous qualifications filter for clearing and normal --%>
           <jsp:include page="/jsp/common/interstitialsearch/gradeFilterIteration.jsp" >
            <jsp:param name="type" value='normal'/>
            <jsp:param name="qualCode" value="<%=qualificationTypeSelected%>"/>
           </jsp:include>
           
           <c:if test="${requestScope.qualificationTypeSelected eq 'Clearing/Adjustment'}">
             <jsp:include page="/jsp/common/interstitialsearch/gradeFilterIteration.jsp" >
               <jsp:param name="type" value='clearing'/>
               <jsp:param name="qualCode" value="<%=qualificationTypeSelected%>"/>
             </jsp:include>
           </c:if>
           <%-- End :: Previous qualifications filter for clearing and normal --%>
           
			   
			   <%-- Start :: How you're assessed filter --%>
         <%--
         <logic:present name="ASSESSMENT_TYPE_LIST" scope="request">
             <logic:notEmpty name="ASSESSMENT_TYPE_LIST" scope="request">
               <div class="advrw">
                  <h3 onclick="slideDownOrUp('assessmentParent', $$('assessmentToggle'))">How you're assessed<span id="assessmentToggle" class="fa fa-angle-down" ></span></h3>
                  <div class="chkcont">
                     <p>Are you more of a coursework person or better at exams?</p>
                     <div id="assessmentParent" class="inptsr clrfx loctyp asstyp">
                        <logic:iterate id="assessmentList" name="ASSESSMENT_TYPE_LIST" indexId="row" scope="request">
                          <div class="rady">
                             <input type="radio" class="asessment_radio" name="assessment" id="assessment_<%=row%>" onclick="selectFilters('assessment_<%=row%>', 'single')"  value='<bean:write name="assessmentList" property="urlText" />' >
                             <label for='assessment_<%=row%>'>
                                <bean:define id="assessmentName" name="assessmentList" property="assessmentDisplayName"/>
                                <jsp:include page="/jsp/common/interstitialsearch/interstitialSearchUtilAVG.jsp" >
                                  <jsp:param name="SVG_TYPE" value='<%=assessmentName%>'/>
                                </jsp:include>
                                <span id="labelassessment_<%=row%>"><bean:write name="assessmentList" property="assessmentDisplayName" /></span>
                             </label>
                          </div>
                        </logic:iterate>
                     </div>
                  </div>
               </div>
            </logic:notEmpty>
         </logic:present>
         --%>
			   <%-- End :: How you're assessed filter --%>
			<%--    
			   Start :: What's most important to you? filter
			   <c:if test="${not empty requestScope.REVIEW_CAT_LIST}">
               <div class="advrw">
                  <h3 onclick="slideDownOrUp('reviewParent', $$('reviewToggle'), event)">What's most important to you?
                      <div class="info-circle tlinfo tooltext">
                        <div class="tlpcnt tooltext">
                           <div class="tooltext">
                              Select up to 3 of the following categories and we'll match universities that have ranked highly in those categories in the latest WUSCA ratings. Find out more about the WUSCAs <a target="_blank" class="tooltext" href="<%=awardsURL%>">here</a>
                           </div>
                        </div>
                     </div>
                      <span id="reviewToggle" class="fa fa-angle-down" ></span>
                  </h3>
                  <div class="chkcont">
                     <p>Choose up to 3 options</p>
                     <div id="reviewParent" class="inptsr clrfx loctyp opttyp">
                        <div class="radycont">
                        <c:forEach var="reviewCategoryList" items="${requestScope.REVIEW_CAT_LIST}" varStatus="row">
                            <c:set var="rowsIntValue" value="${row.index}"/>
                             <div class="rady">
                               <input type="checkbox" class="review_check" name="reviewCategory" id="reviewCategory_<%=pageContext.getAttribute("rowsIntValue").toString()%>" onclick="validateReview(this);"  value='${reviewCategoryList.urlText}' >
                               <label for='reviewCategory_<%=pageContext.getAttribute("rowsIntValue").toString()%>'>
                               <c:set var="reviewName" value="${reviewCategoryList.reviewCategoryDisplayName}"/>
                                  <jsp:include page="/jsp/common/interstitialsearch/interstitialSearchUtilAVG.jsp" >
                                    <jsp:param name="SVG_TYPE" value='<%=pageContext.getAttribute("reviewName").toString()%>'/>
                                  </jsp:include>
                                  <span id="labelreviewCategory_<%=pageContext.getAttribute("rowsIntValue").toString()%>">${reviewCategoryList.reviewCategoryDisplayName}</span>
                               </label>
                               <input type="hidden" name="urlText<%=pageContext.getAttribute("rowsIntValue").toString()%>" id="urlText<%=pageContext.getAttribute("rowsIntValue").toString()%>" value='${reviewCategoryList.urlText}'/>
                             </div>
                      </c:forEach>
                        </div>
                     </div>
                  </div>
               </div>
         </c:if>
          --%>
			   <%-- End :: What's most important to you? filter --%>
		</div>	   
     
   </div>
    
  <div id="input_hiddens">
    <input type="hidden" class="url_hiddens_nodef" id="qual_hidden" name="qual_hidden" value="<%=qualification%>"/>
    <input type="hidden" class="url_hiddens_nodef" id="qualType_hidden" name="qualType_hidden" value="<%=qualificationTypeSelected%>"/>
    <input type="hidden" class="url_hiddens_nodef" id="codequal_hidden" name="codequal_hidden" value="<%=qualificationCode%>"/>
    <input type="hidden" class="url_hiddens_nodef" id="interstitialSubjectSearch_l1_url" name="interstitialSubjectSearch_l1_url" value="${requestScope.sub_url_text}"/>
    <input type="hidden" class="url_hiddens_nodef" id="interstitialSubjectSearch_l2_url" name="interstitialSubjectSearch_l2_url" value=""/>
    <%-- Added the keyword hidden, 25_Sep_2018 By Sabapathi --%>
    <input type="hidden" class="url_hiddens_nodef" id="keyword_hidden" name="keyword_hidden" value=""/>
    <input type="hidden" class="url_hiddens_nodef" id="country_hidden" name="country_hidden" value=""/>
    <input type="hidden" class="url_hiddens_nodef" id="locationType_hidden" name="locationType_hidden" value="${requestScope.loc_name}"/>
    <input type="hidden" class="url_hiddens_nodef" id="studymode_hidden" name="studymode_hidden" value=""/>
    <input type="hidden" class="url_hiddens_nodef" id="grade_hidden" name="grade_hidden" value="a-level"/>
    <input type="hidden" class="url_hiddens_nodef" id="grade_applied_hidden" name="grade_hidden" value=""/>
    <input type="hidden" class="url_hiddens_nodef" id="grade_points_hidden" name="grade_points_hidden" value=""/>
    <input type="hidden" class="url_hiddens_nodef" id="grade_points_applied_hidden" name="grade_points_hidden" value=""/>
    <input type="hidden" class="url_hiddens_nodef" id="assessment_hidden" name="assessment_hidden" value=""/>
    <input type="hidden" class="url_hiddens_nodef" id="reviewCategory_hidden" name="reviewCategory_hidden" value=""/>
    <input type="hidden" class="url_hiddens_nodef" id="clearing_score" name="clearing_score" value=""/>
  </div>
		 
   <%-- Start :: Bottom Nav --%>
   <div id="footer" class="ad_fltr_sticky clrfx" style="display:none">
      <div class="advmid_fltr clrfx">
  
         <%-- Start :: Show to remove selected filters if no courses --%>
         <div id="footer_zero_count" class="advmid_fltr_all" style="display:none;">
            <div class="ad_fltr_caution"><i class="fa fa-exclamation-triangle"></i>No results available</div>
            <p>Try removing some filters</p>
            <ul id="filter_ul">
               <li style="display:none" id="qual_fl"><a href="javascript:clearFilters('qual', 'ONE', 'radio');"><span id="qual_fs">Degree </span><i class="fa fa-times"></i></a></li>
               <li style="display:none" id="interstitialSubjectSearch_fl"><a href="javascript:clearFilters('interstitialSubjectSearch', 'ONE', 'text');"><span id="interstitialSubjectSearch_fs"></span><i class="fa fa-times"></i></a></li>
               <li style="display:none" id="qualification_fl"><a href="javascript:clearFilters('qualification', 'ONE', 'select');"><span id="qualification_fs">Qualification </span><i class="fa fa-times"></i></a></li>
               <li style="display:none" id="country_fl"><a href="javascript:clearFilters('country', 'ONE', 'radio');"><span id="country_fs"></span><i class="fa fa-times"></i></a></li>
              <c:if test="${not empty requestScope.LOCATION_TYPE_LIST}">
               
              <c:forEach var="locationList" items="${requestScope.LOCATION_TYPE_LIST}" varStatus="row">
                <c:set var="rowValueInt" value="${row.index}"/>
                  <li style="display:none" id="locationType_<%=pageContext.getAttribute("rowValueInt").toString()%>_fl"><a href="javascript:clearFilters('locationType_<%=pageContext.getAttribute("rowValueInt").toString()%>', 'ONE', 'checkbox');"><span id="locationType_<%=pageContext.getAttribute("rowValueInt").toString()%>_fs">${locationList.optionDesc}</span><i class="fa fa-times"></i></a></li>
                   
              </c:forEach>
               
               
               </c:if>
               <li style="display:none" id="studymode_fl"><a href="javascript:clearFilters('studymode', 'ONE', 'radio');"><span id="studymode_fs"></span><i class="fa fa-times"></i></a></li>
               <li style="display:none" id="grade_fl"><a href="javascript:clearFilters('grade', 'ONE', 'radio');"><span id="grade_fs">A-Levels</span><i class="fa fa-times"></i></a></li>
               <li style="display:none" id="assessment_fl"><a href="javascript:clearFilters('assessment', 'ONE', 'radio');"><span id="assessment_fs"></span><i class="fa fa-times"></i></a></li>
               <c:if test="${not empty requestScope.REVIEW_CAT_LIST}">
           <c:forEach var="reviewCategoryList" items="${requestScope.REVIEW_CAT_LIST}" varStatus="row">
           <c:set var="rowValueInts" value="${row.index}"/>
                   
                    <li style="display:none" id="reviewCategory_<%=pageContext.getAttribute("rowValueInts").toString()%>_fl"><a href="javascript:clearFilters('reviewCategory_<%=pageContext.getAttribute("rowValueInts").toString()%>', 'ONE', 'checkbox');"><span id="reviewCategory_<%=pageContext.getAttribute("rowValueInts").toString()%>_fs">${reviewCategoryList.reviewCategoryDisplayName}</span><i class="fa fa-times"></i></a></li>
              
                   </c:forEach>
          
              
               </c:if>
            </ul>
         </div>
         <%-- End :: Show to remove selected filters if no courses --%>
   
         <%-- Start :: If courses available for filters --%>
         <div id="footer_with_count" class="advmid_fltr_clr" style="display:none">
            <a href="javascript:void(0)" onclick="clearFilters('', 'ALL')" class="ad_fltr_clr">Clear all</a> <a href="javascript:void(0)" onclick="submitResults();" id="filter_submit_button" class="ad_fltr_view wht_btn">View 1,125 courses</a>
         </div>
         <%-- End :: If courses available for filters --%>
      </div>
   </div>
   <%-- End :: Bottom Nav --%>
  
  <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
  <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />  
  </div>
  
  <script type="text/javascript">
    hideCloseIfDesktop();
  </script>
  <c:if test="${not empty requestScope.sub_name}">
    <script type="text/javascript">getCourseCount();</script>
  </c:if>
 </body>
