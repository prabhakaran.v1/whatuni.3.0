<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.SessionData" autoFlush="true" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page import="WUI.utilities.GlobalConstants"%>
<%@page import="WUI.utilities.CommonUtil" %>

<%
  String type = request.getParameter("type");
  String display = "display:none";
  String qualCode = request.getParameter("qualCode");
  String requestName = "GRADE_FILTER";
  String tagIdentifier = "";
  String parentDivId = "others_grade";
  if("Clearing/Adjustment".equals(qualCode) && "clearing".equals(type)) {
    display = "display:block";
  }
  if(!"Clearing/Adjustment".equals(qualCode) && "normal".equals(type)) {
    display = "display:block";
  }
  if("clearing".equals(type)) {
    requestName = "CLEARING_GRADE_FILTER";
    tagIdentifier = "clr";
    parentDivId = "clearing_grade";
  }
  request.setAttribute("requestName", requestName);
%>

<%-- Start :: Previous qualifications filter --%>
   <c:if test="${not empty requestScope.requestName}">
     <div class="advrw parent_div" id="others_grade" style="<%=display%>">
       <h3 onclick="slideDownOrUp('qualificationParent', $$('qualificationToggle'), event)">Qualification<span id="qualificationToggle" class="fa fa-angle-down"></span>
            </h3>
       <jsp:include page="/jsp/common/interstitialsearch/includeGradeFilter.jsp"/>
       <div class="aplybtn">
          <button id="apply_0" onclick="selectFilters('apply_0', 'qualification', '');" disabled="disabled">Apply</button>
       </div>
     </div>
   </c:if>
<%-- End :: Previous qualifications filter --%>