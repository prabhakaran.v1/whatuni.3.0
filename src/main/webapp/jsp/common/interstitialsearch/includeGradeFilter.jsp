<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants,WUI.utilities.CommonFunction"%>
<%
  int loop = 0;
  int count = 0, subCount = 0, subLen = 0; 
  String qualSub = "";
  String qualId = ""; 
  String reviewFlag = "N";
  String ajaxActionName = "QUAL_SUB_AJAX";  
  String removeBtnFlag = "display:block";
  String subject = !GenericValidator.isBlankOrNull((String)request.getAttribute("subject")) ? (String)request.getAttribute("subject") : "";
  String selectFlag = "";
  String gcseSelectFlag = "";
  String addQualStyle = "display:block";
  String addQualBtnStyle = "display:block";
  String gcseSelOption = ": 9-1";
  String removeLink =   CommonUtil.getImgPath("/wu-cont/images/cmm_close_icon.svg",0);
  String qualifications = "";
  String selectQualId ="";
  String selectQualSubLen = ""; 
  String location = !GenericValidator.isBlankOrNull((String)request.getAttribute("location")) ? (String)request.getAttribute("location") : "";
  String q =!GenericValidator.isBlankOrNull((String)request.getAttribute("q")) ? (String)request.getAttribute("q") : "";
  String university = !GenericValidator.isBlankOrNull((String)request.getAttribute("university")) ? (String)request.getAttribute("university") : "";
  String qualCode = !GenericValidator.isBlankOrNull((String)request.getAttribute("qualCode")) ? (String)request.getAttribute("qualCode") : "";
  String referralUrl = !GenericValidator.isBlankOrNull((String)request.getAttribute("referralUrl")) ? (String)request.getAttribute("referralUrl") : "";
  String userId = (String)request.getAttribute("userId");
  String subjectSessionId = (String)request.getAttribute("subjectSessionId");
  String ucaspoint = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasPoint")) ? (String)request.getAttribute("ucasPoint") : "0";
  String ucasMaxPoints = (String)request.getAttribute("ucasMaxPoints");
  String styleFlag = "display:block"; String styleFlag2 = "display:none"; String styleFlag3 = "display:none"; 
  String score = !GenericValidator.isBlankOrNull((String)request.getAttribute("score")) ? (String)request.getAttribute("score") : "";
  String minScore = "0";
  String maxScore = ucaspoint;
  int addSubLimit = 6;
  String removeQualStyle = "display:none";
  String addSubStyle = "display:block";
  int userQualListSize = (!GenericValidator.isBlankOrNull((String)request.getAttribute("userQualSize"))) ? Integer.parseInt((String)request.getAttribute("userQualSize")) : 0;
  String addQualId = "level_3_add_qualif";
  String callTypeFlag = "";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("score"))) {
     String[] scoreArr = score.split(",");
     minScore = scoreArr[0];
     maxScore = scoreArr[1];
   }
   String postcode = !GenericValidator.isBlankOrNull((String)request.getAttribute("postCode")) ? (String)request.getAttribute("postCode") : null;
   String studyMode = !GenericValidator.isBlankOrNull((String)request.getAttribute("studyMode")) ? (String)request.getAttribute("studyMode") : null;
   String pageName = !GenericValidator.isBlankOrNull((String)request.getAttribute("pageName")) ? (String)request.getAttribute("pageName") : null;
   String ucasCode = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasCode")) ? (String)request.getAttribute("ucasCode") : null;
   String locationType = !GenericValidator.isBlankOrNull((String)request.getAttribute("locationType")) ? (String)request.getAttribute("locationType") : null;
   String campusType = !GenericValidator.isBlankOrNull((String)request.getAttribute("campusType")) ? (String)request.getAttribute("campusType") : null;
   String russellGroup = !GenericValidator.isBlankOrNull((String)request.getAttribute("russellGroup")) ? (String)request.getAttribute("russellGroup") : null;
   String module = !GenericValidator.isBlankOrNull((String)request.getAttribute("module")) ? (String)request.getAttribute("module") : null;
   String distance = !GenericValidator.isBlankOrNull((String)request.getAttribute("distance")) ? (String)request.getAttribute("distance") : null;
   String empRate = !GenericValidator.isBlankOrNull((String)request.getAttribute("empRate")) ? (String)request.getAttribute("empRate") : null;
   String ucasTariff = !GenericValidator.isBlankOrNull((String)request.getAttribute("ucasTariff")) ? (String)request.getAttribute("ucasTariff") : null;
   String jacs = !GenericValidator.isBlankOrNull((String)request.getAttribute("jacs")) ? (String)request.getAttribute("jacs") : null;
   String assessmentTypeFilter = !GenericValidator.isBlankOrNull((String)request.getAttribute("assessmentTypeFilter")) ? (String)request.getAttribute("assessmentTypeFilter") : null;
   String reviewCategoryFilter = !GenericValidator.isBlankOrNull((String)request.getAttribute("reviewCategoryFilter")) ? (String)request.getAttribute("reviewCategoryFilter") : null;
%>
<div class="chkcont">
  			<p>What grades will you get/have you got?</p>
     
        <!-- Uni List -->
        <!-- Contact details -->
        <c:if test="${not empty requestScope.qualificationList}">
            <div id="qualificationParent" class="fl basic_inf pers_det cont_det qualif">  
          <!-- UCAS -->
             <div class="ucas_mid">
                <!--Qualification Repeat Field start 1-->
                
                  <div class="add_qualif">
                  <div class="ucas_refld">
                    <!--<h3 class="un-tit">Level 3</h3>-->
                    <!-- L3 Qualification Row  -->
                    <div class="l3q_rw he">
                    <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" end="2">
                      <c:set var="indexIValue" value="${i.index}" />
                      <c:if test="${not empty qualList.entry_qual_id}">
                        <c:set var="selectQualId1" value="${qualList.entry_qual_id}"/>
                      <%selectQualId = pageContext.getAttribute("selectQualId1").toString();%>
                      </c:if>
                      <c:if test="${empty qualList.entry_qual_id}">
                        <% selectQualId ="1";%>
                      </c:if>
                      <%count = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIValue")));if(count > 0){addQualStyle = "display:none";}%>
                      <!-- Add Qualification & Subject 1-->		
                      <div class="adqual_rw" data-id=level_3_add_qualif id="level_3_qual_<%=count%>" style="<%=addQualStyle%>">
                        <div class="ucas_row">
                          <!-- <h5 class="cmn-tit txt-upr">Qualification</h5> -->
                          <fieldset class="ucas_fldrw">
                            <div class="remq_cnt">
                              <div class="ucas_selcnt">
                                <fieldset class="ucas_sbox">
                                  <c:set var="selectQualName" value="${qualList.entry_qualification}"/>
                                  <%String selectQualName = pageContext.getAttribute("selectQualName").toString(); %>
                                  <%if(!"0".equals(String.valueOf(pageContext.getAttribute("selectQualName").toString()))){if("A - Levels".equalsIgnoreCase(selectQualName)){selectQualName = "A Level";}qualifications = selectQualName ;}%>
                                  <span class="sbox_txt" id="qualspan_<%=count%>"><%=selectQualName%></span>
                                  <span class="grdfilt_arw"></span>
                                </fieldset>
                                <select name="qualification1" onchange="appendQualDiv(this, '', '<%=count%>', 'new_entry_page');" id="qualsel_<%=count%>" class="ucas_slist">
                                  <option value="0">Please Select</option>
                                  <c:forEach var="qualListLevel3" items="${requestScope.qualificationList}">
                                   <jsp:scriptlet>selectFlag = "";if("".equals(selectQualId)){selectQualId="1";}</jsp:scriptlet>
                                   <c:set var="selectQualId" value="<%=selectQualId%>"/>
                                   <c:if test="${qualListLevel3.entry_qual_id eq selectQualId}">
                                     <jsp:scriptlet>selectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                   </c:if>
                                   <c:if test="${qualListLevel3.entry_qualification ne 'GCSE'}">
                                     <c:if test="${empty qualListLevel3.entry_qual_id}">
                                       <optgroup label="${qualListLevel3.entry_qualification}"></optgroup>
                                     </c:if>
                                     <c:if test="${not empty qualListLevel3.entry_qual_id}">
                                       <option value="${qualListLevel3.entry_qual_id}" <%=selectFlag%>>${qualListLevel3.entry_qualification}</option>
                                     </c:if>                                 
                                   </c:if>
                                  </c:forEach>
                                </select>
                                <c:forEach var="grdQualifications" items="${requestScope.qualificationList}">
                                  <c:if test="${grdQualifications.entry_qualification ne 'GCSE'}">                                 
                                   <c:if test="${not empty grdQualifications.entry_qual_id}">
                                     <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.entry_qual_id}" name="qualSubj_${grdQualifications.entry_qual_id}" value="${grdQualifications.entry_subject}" />
                                     <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.entry_qual_id}" name="" value="${grdQualifications.entry_grade}" />
                                   </c:if>
                                 </c:if>                               
                               </c:forEach>
                              </div>
                            </div>
                            <%if(count != 0){%>
                             <div class="add_fld">
                              <a id="1rSubRow3" onclick="javascript:removeQualification('level_3_qual_<%=count%>', <%=count-1%>, 'addQualBtn_<%=count%>');" class="btn_act1 bck f-14 pt-5">
                                <span class="hd-mob rq_mob">Remove Qualification</span><!--<span class="hd-desk"><i class="fa fa-times"></i></span>-->
                              </a>
                            </div>
							<div class="err" id="err_qualse1" style="display:none;"></div>
							<%}%>
                            </fieldset>
                            <div class="err_field fl_w100" style="display:none"><p>Please enter the qualification</p></div>
                        </div>                         
                      <div class="subgrd_fld" id="subGrdDiv<%=pageContext.getAttribute("indexIValue").toString()%>">
                        <div class="ucas_row grd_row" id="grdrow_<%=pageContext.getAttribute("indexIValue").toString()%>">
                          <div class="rmv_act">
                            <fieldset class="ucas_fldrw quagrd_tit">
                              <div class="ucas_tbox tx-bx">
                                <h5 class="cmn-tit qual_sub">Subject</h5>
                              </div>
                              <div class="ucas_selcnt rsize_wdth">
                                <h5 class="cmn-tit qual_grd">Grade</h5>
                              </div>
                            </fieldset>   
                            <c:if test="${not empty qualList.entry_subject}">  
                             <c:set var="selectQualSubLen1" value="${qualList.entry_subject}"/>
                            <%String selectQualSubLen1 = pageContext.getAttribute("selectQualSubLen1").toString();
                              selectQualSubLen = selectQualSubLen1; %>
                            </c:if>
                            <c:if test="${empty qualList.entry_subject}">
                            <%selectQualSubLen = "3";%>
                            </c:if>
                              
                            <c:forEach var="subjectList" items="${requestScope.qualificationList}" varStatus="j" end="<%=Integer.parseInt(selectQualSubLen)-1%>">
                            <c:set var="indexJValue" value="${j.index}"/>
                              <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue").toString()));%>
                              <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
                                <fieldset class="ucas_fldrw">
                                  <div class="ucas_tbox w-390 tx-bx">
                                    <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="subIp_<%=subCount%>" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="" class="frm-ele" autocomplete="off">															
                                    <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                                    <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                                    </div>
                                  </div>
                                  <div class="ucas_selcnt rsize_wdth">
                                    <div class="ucas_sbox mb-ht w-84" id="1grades1">
                                      <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">A*</span><span class="grdfilt_arw"></span>
                                    </div>
                                    <select class="ucas_slist qus_slt" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');" id="qualGrd_<%=count%>_<%=subCount%>"></select>
                                    <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=count%>_<%=subCount%>','${subjectList.entry_grade}'); </script>                                    
                                    <input class="subjectArr" data-id="level_3" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_<%=selectQualId%>" value="" />                                
                                    <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_<%=selectQualId%>" value="A*" />
                                    <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_<%=selectQualId%>" value="<%=selectQualId%>" />
                                    <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_<%=selectQualId%>" value="<%=count+1%>" />
                                  </div>
                                  <%if(subCount == 0){
                                    removeBtnFlag = "display:none";
                                  }else{
                                    removeBtnFlag = "display:block";
                                  }
                                  if(subCount == 1){%>                                  
                                    <script>jq("#remSubRow<%=count%>0").show();</script>
                                  <%}%>                                  
                                  <div class="add_fld"> <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"></span></a></div>
                                  <div id="err_msg_<%=count%>_<%=subCount%>" class="err_field fl_w100"></div>
                                </fieldset>
                              </div>
                            </c:forEach>            
                            <div class="ad-subjt" id="addSubject_<%=count%>" style="display:block">
                              <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('<%=selectQualId%>','countAddSubj_<%=count%>', '<%=count%>');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                            </div>                          
                            </div>                          
                          </div>
                        </div>
                        
                        <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                      </div>
                      </c:forEach>
                      <!-- Add Qualification & Subject 1 -->
                      <!-- L3 Qualification Row -->
                    </div>
                    <c:forEach var="qualList" items="${requestScope.qualificationList}" varStatus="i" begin="0" end="1">
                      <c:set var="indexIvalue" value="${i.index}"/>
                      <%count = Integer.parseInt(pageContext.getAttribute("indexIvalue").toString());if(count > 0){addQualBtnStyle = "display:none";}%>
                      <div class="add_qualtxt" id="addQualBtn_<%=count%>" style="<%=addQualBtnStyle%>">
                        <p class="aq_hlptxt">Studied more than one qualification? Select additional below</p>
                        <a href="javascript:void(0);" onclick="addQualification('level_3_qual_<%=count + 1%>', <%=count + 1%>, 'addQualBtn_<%=count%>');" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> Add a qualification</a>
                        
                      </div>
                    </c:forEach>
                    <div id="errorDiv" class="err_field fl_w100" style="display:none"><p id="pId"></p></div>
                    <!-- <div class="add_qualtxt" id="addQual" style="display: block;">
                      <a href="javascript:void(0);" id="apply_0" onclick="selectFilters('', 'qualification', '');" class="fl btn_act2 bn-blu mt-40 apply_now" disabled="disabled"> APPLY</a>
                    </div> -->
                    <!-- Tariff Points Container Start -->
                    <!-- Tariff Points Container End -->		
                 </div>
                 </div>
                 </div>
                 </div>
                 </c:if>
       </div>
    
<input type="hidden" id= "userUcasScore" value="<%=ucaspoint%>"/>
<input type="hidden" id="country_hidden" name="country_hidden" value="<%=location%>"/>
<input type="hidden" id="newQualDesc" value="<%=qualifications%>" />
<input type="hidden" id="subject" name="subject" value="<%=subject%>"/>
<input type="hidden" id="q" name="q" value="<%=q%>"/>
<input type="hidden" id="university" name="university" value="<%=university%>"/>  
<input type="hidden" id="qualCode" name="qualCode" value="<%=qualCode%>"/>  
<input type="hidden" id="referralUrl" name="referralUrl" value="<%=referralUrl%>"/>  
<input type="hidden" id="ucasMaxPoints" name="ucasMaxPoints" value="<%=ucasMaxPoints%>"/>
<input type="hidden" id="removeLinkImg" value="<%=removeLink%>" />
<input type="hidden" id="score" value="<%=score%>"/>
<input type="hidden" id="refresh" value="no">
<input type="hidden" id= "postcode" name="postcode" value="<%=postcode%>"/>
<input type="hidden" id="study-mode" name="study-mode" value="<%=studyMode%>"/>
<input type="hidden" id="page-name" name="page-name" value="<%=pageName%>" />
<input type="hidden" id="ucas-code" name="ucas-code" value="<%=ucasCode%>"/>
<input type="hidden" id="location-type" name="location-type" value="<%=locationType%>"/>
<input type="hidden" id="module" name="module" value="<%=module%>"/>  
<input type="hidden" id="distance" name="distance" value="<%=distance%>"/>  
<input type="hidden" id="empRate" name="empRate" value="<%=empRate%>"/>  
<input type="hidden" id="ucasTariff" name="ucasTariff" value="<%=ucasTariff%>"/>
<input type="hidden" id="jacs" name="jacs" value="<%=jacs%>"/> 
<input type="hidden" id="assessment-type" name="assessment-type" value="<%=assessmentTypeFilter%>"/>
<input type="hidden" id="your-pref" name="your-pref" value="<%=reviewCategoryFilter%>"/>

