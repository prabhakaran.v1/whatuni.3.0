
<%
String svgType = request.getParameter("SVG_TYPE");
%>


<%if("Big city".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
       <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
       <title>big city</title>
       <desc>Created with Sketch.</desc>
       <defs></defs>
       <g id="Filter-icon/big-city/active" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g class="actstat" id="Page-1" transform="translate(4.000000, 5.000000)" stroke="#707070" stroke-width="2">
             <polygon id="Stroke-1" stroke-linecap="round" stroke-linejoin="round" points="0.5 48.5 14.5 48.5 14.5 12 0.5 12"></polygon>
             <polygon id="Stroke-3" stroke-linecap="round" stroke-linejoin="round" points="18.5 48.5 35.5 48.5 35.5 0.5 18.5 0.5"></polygon>
             <polyline id="Stroke-4" stroke-linecap="round" stroke-linejoin="round" points="46.5 22.2993 46.5 9.0003 35.5 9.0003 35.5 48.5003 39.372 48.5003 47.5 48.5003"></polyline>
             <polygon id="Stroke-5" stroke-linecap="round" stroke-linejoin="round" points="39.495 48.5 49.495 48.5 49.495 23 39.495 23"></polygon>
             <polyline id="Stroke-6" stroke-linecap="round" stroke-linejoin="round" points="14.5478 48.5 18.4998 48.5 18.4998 5.5 6.4998 5.5 6.4998 11.335"></polyline>
             <path d="M45.5,8.9211 C45.5,6.4361 43.485,4.4211 41,4.4211 C38.515,4.4211 36.5,6.4361 36.5,8.9211" id="Stroke-7"></path>
             <path d="M41,0.5 L41,4.134" id="Stroke-8" stroke-linecap="round"></path>
             <path d="M4,16 L11,16" id="Stroke-10" stroke-linecap="round"></path>
             <path d="M43,28 L46,28" id="Stroke-11" stroke-linecap="round"></path>
             <path d="M39,13 L39,18.0990195" id="Stroke-14" stroke-linecap="round"></path>
             <path d="M27,6 L27,10" id="Stroke-19" stroke-linecap="round"></path>
             <path d="M27,15 L27,19" id="Stroke-19-Copy" stroke-linecap="round"></path>
             <path d="M27,24 L27,28" id="Stroke-19-Copy-2" stroke-linecap="round"></path>
             <path d="M27,32 L27,36" id="Stroke-19-Copy-3" stroke-linecap="round"></path>
             <path d="M27,40 L27,44" id="Stroke-19-Copy-4" stroke-linecap="round"></path>
             <path d="M23,6 L23,10" id="Stroke-24" stroke-linecap="round"></path>
             <path d="M23,15 L23,19" id="Stroke-24-Copy" stroke-linecap="round"></path>
             <path d="M23,24 L23,28" id="Stroke-24-Copy-2" stroke-linecap="round"></path>
             <path d="M23,32 L23,36" id="Stroke-24-Copy-3" stroke-linecap="round"></path>
             <path d="M23,40 L23,44" id="Stroke-24-Copy-4" stroke-linecap="round"></path>
             <path d="M31,6 L31,10" id="Stroke-25" stroke-linecap="round"></path>
             <path d="M31,15 L31,19" id="Stroke-25-Copy" stroke-linecap="round"></path>
             <path d="M31,24 L31,28" id="Stroke-25-Copy-2" stroke-linecap="round"></path>
             <path d="M31,32 L31,36" id="Stroke-25-Copy-3" stroke-linecap="round"></path>
             <path d="M31,40 L31,44" id="Stroke-25-Copy-4" stroke-linecap="round"></path>
             <path d="M43,13 L43,18" id="Stroke-30" stroke-linecap="round"></path>
             <path d="M43,32 L46,32" id="Stroke-32" stroke-linecap="round"></path>
             <path d="M43,36 L46,36" id="Stroke-33" stroke-linecap="round"></path>
             <path d="M43,40 L46,40" id="Stroke-34" stroke-linecap="round"></path>
             <path d="M43,44 L46,44" id="Stroke-35" stroke-linecap="round"></path>
             <path d="M4,20 L11,20" id="Stroke-36" stroke-linecap="round"></path>
             <path d="M4,24 L11,24" id="Stroke-37" stroke-linecap="round"></path>
             <path d="M4,28 L11,28" id="Stroke-38" stroke-linecap="round"></path>
             <path d="M4,32 L11,32" id="Stroke-39" stroke-linecap="round"></path>
             <path d="M4,36 L11,36" id="Stroke-40" stroke-linecap="round"></path>
             <path d="M4,40 L11,40" id="Stroke-41" stroke-linecap="round"></path>
             <path d="M4,44 L11,44" id="Stroke-42" stroke-linecap="round"></path>
          </g>
       </g>
    </svg>
                              

<%} else if("Countryside".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
       <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
       <title>countryside</title>
       <desc>Created with Sketch.</desc>
       <defs></defs>
       <g id="Filter-icon/countryside" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g class="actstat" id="Group" transform="translate(5.000000, 4.000000)" stroke="#707070" stroke-width="2">
             <path d="M0,49 L48,49" id="Stroke-1" stroke-linecap="round"></path>
             <path d="M12.5,48.7106 L12.5,16.4196" id="Stroke-4" stroke-linecap="round"></path>
             <path d="M37.4253,48.7106 L37.4253,23.9936" id="Stroke-6" stroke-linecap="round"></path>
             <path d="M12.6761,39.1418 C19.0071,39.1418 24.1391,34.0538 24.1391,27.7778 C24.1391,21.5018 19.0071,0.4938 12.6761,0.4938 C6.3451,0.4938 1.2131,21.5018 1.2131,27.7778 C1.2131,34.0538 6.3451,39.1418 12.6761,39.1418 Z" id="Stroke-8"></path>
             <path d="M7.5748,22.7067 L12.4998,27.4937" id="Stroke-11" stroke-linecap="round"></path>
             <path d="M17.5039,19.2461 L12.5789,24.0331" id="Stroke-12" stroke-linecap="round"></path>
             <path d="M17.5039,28.7107 L12.5789,33.4977" id="Stroke-13" stroke-linecap="round"></path>
             <path d="M41.5611,26.2653 L37.4241,30.2863" id="Stroke-14" stroke-linecap="round"></path>
             <path d="M33.2802,30.015 L37.4172,34.036" id="Stroke-15" stroke-linecap="round"></path>
             <path d="M37.5,38.9626 C42.471,38.9626 46.5,34.9326 46.5,29.9626 C46.5,24.9926 42.471,9.4936 37.5,9.4936 C32.529,9.4936 28.5,24.9926 28.5,29.9626 C28.5,34.9326 32.529,38.9626 37.5,38.9626 Z" id="Stroke-16"></path>
          </g>
       </g>
    </svg>
                              
<%} else if("Seaside".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
       <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
       <title>seaside</title>
       <desc>Created with Sketch.</desc>
       <defs></defs>
       <g id="Filter-icon/seaside/active" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g class="actstat" id="Page-1" transform="translate(4.000000, 4.000000)" stroke="#707070" stroke-width="2">
             <path d="M0.5,45.2661 C0.5,47.2211 2.067,48.8051 4,48.8051 C5.933,48.8051 7.5,47.2211 7.5,45.2661" id="Stroke-1" stroke-linecap="round"></path>
             <path d="M7.5,45.2661 C7.5,47.2211 9.067,48.8051 11,48.8051 C12.933,48.8051 14.5,47.2211 14.5,45.2661" id="Stroke-3" stroke-linecap="round"></path>
             <path d="M14.5,45.2661 C14.5,47.2211 16.067,48.8051 18,48.8051 C19.933,48.8051 21.5,47.2211 21.5,45.2661" id="Stroke-5" stroke-linecap="round"></path>
             <path d="M21.5,45.2661 C21.5,47.2211 23.067,48.8051 25,48.8051 C26.933,48.8051 28.5,47.2211 28.5,45.2661" id="Stroke-7" stroke-linecap="round"></path>
             <path d="M28.5,45.2661 C28.5,47.2211 30.067,48.8051 32,48.8051 C33.933,48.8051 35.5,47.2211 35.5,45.2661" id="Stroke-9" stroke-linecap="round"></path>
             <path d="M35.5,45.2661 C35.5,47.2211 37.067,48.8051 39,48.8051 C40.933,48.8051 42.5,47.2211 42.5,45.2661" id="Stroke-11" stroke-linecap="round"></path>
             <path d="M42.5,45.2661 C42.5,47.2211 44.067,48.8051 46,48.8051 C47.933,48.8051 49.5,47.2211 49.5,45.2661" id="Stroke-13" stroke-linecap="round"></path>
             <path d="M22,41.2265 C22,41.2265 29.857,28.2265 49.5,31.2265" id="Stroke-15" stroke-linecap="round"></path>
             <path d="M7,42.2265 C7,42.2265 15,29.2265 26,36.2265" id="Stroke-17" stroke-linecap="round"></path>
             <path d="M42.1457,9.9392 C44.3547,9.9392 46.1457,8.1482 46.1457,5.9392 C46.1457,3.7302 44.3547,1.9392 42.1457,1.9392 C39.9367,1.9392 38.1457,3.7302 38.1457,5.9392 C38.1457,8.1482 39.9367,9.9392 42.1457,9.9392 Z" id="Stroke-19"></path>
             <path d="M27.6671,11.6284 C25.7471,6.3354 20.6741,2.5454 14.6991,2.5454 C7.0771,2.5454 0.8981,8.7014 0.8981,16.2954 C0.8981,17.6204 1.0971,18.8984 1.4481,20.1104 L27.6671,11.6284 Z" id="Stroke-21" stroke-linecap="round" stroke-linejoin="round"></path>
             <path d="M8.8706,17.5139 C8.8706,17.5139 5.8666,6.3139 9.7656,3.5139" id="Stroke-23" stroke-linecap="round"></path>
             <path d="M11,3.0139 C11,3.0139 17,3.0139 20,14.2269" id="Stroke-25"></path>
             <path d="M14.4977,16.2265 L20.0017,34.0765" id="Stroke-28"></path>
             <path d="M9.635,0.5 L10.496,3.014" id="Stroke-29" stroke-linecap="round"></path>
          </g>
       </g>
    </svg>
                              
                              
<%} else if("Town".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
       <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
       <title>town</title>
       <desc>Created with Sketch.</desc>
       <defs></defs>
       <g id="Filter-icon/town" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g class="actstat" id="Group" transform="translate(4.000000, 6.000000)" stroke="#707070" stroke-width="2">
             <polygon id="Stroke-1" points="0.5 45.5 49.5 45.5 49.5 40.5 0.5 40.5"></polygon>
             <polygon id="Stroke-3" points="0.5 5.5 49.5 5.5 49.5 0.5 0.5 0.5"></polygon>
             <polygon id="Stroke-5" points="9.5 19.5 16.5 19.5 16.5 11.5 9.5 11.5"></polygon>
             <polygon id="Stroke-6" points="9.5 34.5 16.5 34.5 16.5 26.5 9.5 26.5"></polygon>
             <polygon id="Stroke-7" points="21.5 19.5 28.5 19.5 28.5 11.5 21.5 11.5"></polygon>
             <polygon id="Stroke-8" points="21.5 40.5 28.5 40.5 28.5 26.496 21.5 26.496"></polygon>
             <polygon id="Stroke-9" points="33.5 19.5 40.5 19.5 40.5 11.5 33.5 11.5"></polygon>
             <polygon id="Stroke-10" points="33.5 34.5 40.5 34.5 40.5 26.5 33.5 26.5"></polygon>
             <path d="M47.5,40.5 L47.5,5.5 M2.5,5.5 L2.5,40.5" id="Stroke-11"></path>
          </g>
       </g>
    </svg>
                              
                              
<%} else if("Exam".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
       <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
       <title>exam</title>
       <desc>Created with Sketch.</desc>
       <defs></defs>
       <g id="Filter-icon/exam" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g class="actstat" id="Page-1" transform="translate(3.000000, 5.000000)" stroke="#707070" stroke-width="2">
             <path d="M6.6425,0.5 C6.6425,0.5 0.4995,0.5 0.4995,6.642 L0.4995,42.357 C0.4995,42.357 0.4995,48.5 6.6425,48.5 L32.4205,48.5 C32.4205,48.5 38.5625,48.5 38.5625,42.357 L38.5625,6.642 C38.5625,6.642 38.5625,0.5 32.4205,0.5 L6.6425,0.5 Z" id="Stroke-1"></path>
             <polygon id="Stroke-3" stroke-linecap="round" stroke-linejoin="round" points="43.5373 7.4978 43.5373 39.4228 47.5003 48.4978 51.5373 39.4228 51.5373 7.4978"></polygon>
             <polyline id="Stroke-6" stroke-linecap="round" stroke-linejoin="round" points="7.2795 12.2183 9.9885 15.2183 14.2795 8.5093"></polyline>
             <polyline id="Stroke-7" stroke-linecap="round" stroke-linejoin="round" points="7.2795 26.2104 9.9885 29.2104 14.2795 22.5014"></polyline>
             <polyline id="Stroke-8" stroke-linecap="round" stroke-linejoin="round" points="7.2795 39.2104 9.9885 42.2104 14.2795 35.5014"></polyline>
             <path d="M43.5629,40.5137 L51.5629,40.5137" id="Stroke-9"></path>
             <path d="M43.5629,13.4978 L51.5629,13.4978" id="Stroke-11"></path>
             <path d="M19.0629,11.9978 L30.0629,11.9978" id="Stroke-14" stroke-linecap="round"></path>
             <path d="M19.0629,25.9978 L30.0629,25.9978" id="Stroke-15" stroke-linecap="round"></path>
             <path d="M19.0629,38.9978 L30.0629,38.9978" id="Stroke-16" stroke-linecap="round"></path>
          </g>
       </g>
    </svg>
                              
<%} else if("Coursework".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
       <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
       <title>coursework</title>
       <desc>Created with Sketch.</desc>
       <defs></defs>
       <g id="Filter-icon/coursework" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g class="actstat" id="Course-icon" transform="translate(11.000000, 0.000000)" stroke="#707070" stroke-width="2">
             <path d="M0.6402,45.9059 C0.6402,43.8229 2.3242,42.1339 4.4012,42.1339 L35.4322,42.1339 L35.4322,0.6399 L0.6402,0.6399 L0.6402,8.1849 L0.6402,42.1339 L0.6402,45.5869" id="Stroke-1" stroke-linecap="round" stroke-linejoin="round"></path>
             <polygon id="Stroke-3" points="8.733 17.342 27.339 17.342 27.339 12.065 8.733 12.065"></polygon>
             <path d="M34.9521,42.0801 L4.4121,42.0801 C2.3461,42.0941 0.6741,43.6951 0.6741,45.6701 C0.6741,47.6461 2.3461,49.2471 4.4121,49.2581 L8.5511,49.2611 M8.7231,45.8881 L8.7231,56.3601 L11.8891,53.1221 L14.9511,56.3601 L14.9511,45.8881 L8.7231,45.8881 Z M15.4811,49.3151 L35.3851,49.3151 L15.4811,49.3151 Z" id="Stroke-4" stroke-linecap="round" stroke-linejoin="round"></path>
          </g>
       </g>
    </svg>
                              
<%} else if("Practical".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
       <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
       <title>practical</title>
       <desc>Created with Sketch.</desc>
       <defs></defs>
       <g id="Filter-icon/practical" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
          <g class="actstat" id="Icon" transform="translate(4.000000, 4.000000)" stroke="#707070" stroke-width="2">
             <path d="M16.1652,28.5788 C16.1652,24.1608 12.5832,20.5788 8.1652,20.5788 C4.8382,20.5788 1.9892,22.6108 0.7832,25.4998 L7.5312,25.4998 L7.5312,25.5058 C7.5532,25.5058 7.5732,25.4998 7.5942,25.4998 C9.2512,25.4998 10.5942,26.8428 10.5942,28.4998 C10.5942,30.1568 9.2512,31.4998 7.5942,31.4998 C7.5732,31.4998 7.5532,31.4948 7.5312,31.4938 L7.5312,31.4998 L0.7342,31.4998 C1.9032,34.4698 4.7802,36.5788 8.1652,36.5788 C12.5832,36.5788 16.1652,32.9968 16.1652,28.5788 Z" id="Stroke-1"></path>
             <path d="M15.7479,31.5788 L21.9779,31.5788" id="Stroke-3"></path>
             <path d="M22.0393,14.0708 L22.0393,46.4798 L22.0453,46.4798 C22.0453,46.5018 22.0393,46.5218 22.0393,46.5428 C22.0393,48.1998 23.3823,49.5428 25.0393,49.5428 C26.6963,49.5428 28.0393,48.1998 28.0393,46.5428 C28.0393,46.5218 28.0333,46.5018 28.0333,46.4798 L28.0393,46.4798 L28.0393,14.0708" id="Stroke-4"></path>
             <polygon id="Stroke-6" points="31.035 11.0002 36.035 11.0002 36.035 4.0002 31.035 4.0002"></polygon>
             <path d="M37.0471,0.5 C37.0471,0.5 36.0471,0.5 36.0471,1.5 L36.0471,13.5 C36.0471,13.5 36.0471,14.5 37.0471,14.5 L41.0471,14.5 C41.0471,14.5 42.0471,14.5 42.0471,13.5 L42.0471,1.5 C42.0471,1.5 42.0471,0.5 41.0471,0.5 L37.0471,0.5 Z" id="Stroke-7"></path>
             <path d="M18.0314,0.5 L7.6244,6 L17.0314,6.031 L17.0314,13.5 C17.0314,13.5 17.0314,14.5 18.0314,14.5 L29.8244,14.5 C29.8244,14.5 30.8244,14.5 30.8244,13.5 L30.8244,1.5 C30.8244,1.5 30.8244,0.5 29.8244,0.5 L18.0314,0.5 Z" id="Stroke-9" stroke-linecap="round" stroke-linejoin="round"></path>
             <path d="M22.0286,25.5788 L15.7476,25.5788" id="Stroke-12"></path>
             <path d="M28.0179,31.5788 L46.0319,31.5788 L46.0319,31.5728 C46.0529,31.5728 46.0729,31.5788 46.0939,31.5788 C47.7509,31.5788 49.0939,30.2358 49.0939,28.5788 C49.0939,26.9218 47.7509,25.5788 46.0939,25.5788 C46.0729,25.5788 46.0529,25.5848 46.0319,25.5848 L46.0319,25.5788 L27.9899,25.5788" id="Stroke-13"></path>
          </g>
       </g>
    </svg>
                              
                              
<%} else if("Accommodation".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
      <title>accommodation</title>
      <desc>Created with Sketch.</desc>
      <defs></defs>
      <g id="Filter-icon/accommodation" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
         <g class="actstat" id="Page-1" transform="translate(5.000000, 5.000000)" stroke="#707070" stroke-width="2">
            <path d="M0,18.3666667 L22.0310811,0.702459166 C22.0310811,0.702459166 23.9920538,-0.868959351 25.9629808,0.688846445 L48.3333333,18.3666667" id="Stroke-5" stroke-linecap="round"></path>
            <path d="M43.5,14.9833333 L43.5,45.7040626 C43.5,47.3666667 41.8191971,47.3666667 41.8191971,47.3666667 L37.7457711,47.3666667 C36.0649682,47.3666667 36.0649682,45.7040626 36.0649682,45.7040626 L36.0649682,32.5412263 C36.0649682,31.2510456 35.0749753,30.2060989 33.8555527,30.2060989 L28.6484252,30.2060989 C27.4290027,30.2060989 26.4406906,31.2510456 26.4406906,32.5412263 L26.4406906,45.7040626 C26.4406906,47.3666667 24.7598876,47.3666667 24.7598876,47.3666667 L5.54746961,47.3666667 C3.86666667,47.3666667 3.86666667,45.7040626 3.86666667,45.7040626 L3.86666667,14.9833333" id="Stroke-9" stroke-linecap="round" stroke-linejoin="round"></path>
            <path d="M3.86666667,14.9833333 L3.86666667,4.53110762 C3.86666667,3.63064343 4.60821143,2.9 5.52368832,2.9 L8.00964502,2.9 C8.9251219,2.9 9.66666667,3.63064343 9.66666667,4.53110762 L9.66666667,10.6333333 L3.86666667,14.9833333 Z" id="Stroke-3"></path>
         </g>
      </g>
   </svg>
                                 
<%} else if("City life".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <title>city life</title>
    <desc>Created with Sketch.</desc>
    <defs></defs>
    <g id="Filter-icon/city-life" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g id="Page-1" transform="translate(3.000000, 5.000000)">
            <path class="actstat" d="M19.3333333,11.9287606 L19.3333333,2.49468677 C19.3333333,2.49468677 19.3333333,0 21.7821804,0 L29.451153,0 C29.451153,0 31.9,0 31.9,2.49468677 L31.9,16.4333333" id="Stroke-1" stroke="#707070" stroke-width="2"></path>
            <polyline class="actstat" id="Stroke-3" stroke="#707070" stroke-width="2" points="18.3666667 12.5666667 22.2333333 12.5666667 22.2333333 47.3666667"></polyline>
            <polyline class="actstat" id="Stroke-5" stroke="#707070" stroke-width="2" points="8.7 23.2 8.7 12.5666667 18.3666667 12.5666667"></polyline>
            <path d="M14.5,46.2042857 C14.5,46.2042857 14.5,46.4 14.2972578,46.4 L1.93333333,46.4 L1.93333333,24.6595431 C1.93333333,24.6595431 1.93333333,22.2333333 4.44666667,22.2333333 L11.9866667,22.2333333 C11.9866667,22.2333333 14.5,22.2333333 14.5,24.6595431 L14.5,46.2042857 Z" id="Stroke-9" class="actstat" stroke="#707070" stroke-width="2"></path>
            <polyline id="Stroke-11" class="actstat" stroke="#707070" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" points="43.5 46.4 30.9333333 46.4 30.9333333 17.5034477 37.1823178 10.6333333 43.5 17.5034477 43.5 29"></polyline>
            <polygon class="actstat" id="Stroke-13" stroke="#707070" stroke-width="2" points="37.7 46.4 50.2666667 46.4 50.2666667 29 37.7 29"></polygon>
            <polygon class="actstat1" id="Fill-15" fill="#707070" points="4.83333333 25.1333333 4.83333333 25.1333333 4.83333333 28.0333333 7.73333333 28.0333333 7.73333333 25.1333333"></polygon>
            <polygon class="actstat1" id="Fill-16" fill="#707070" points="40.6 35.7666667 40.6 35.7666667 40.6 38.6666667 43.5 38.6666667 43.5 35.7666667"></polygon>
            <polygon class="actstat1" id="Fill-17" fill="#707070" points="22.2333333 2.9 22.2333333 2.9 22.2333333 5.8 25.1333333 5.8 25.1333333 2.9"></polygon>
            <polygon class="actstat1" id="Fill-18" fill="#707070" points="26.1 23.2 26.1 23.2 26.1 26.1 29 26.1 29 23.2"></polygon>
            <polygon class="actstat1" id="Fill-19" fill="#707070" points="11.6 15.4666667 11.6 15.4666667 11.6 18.3666667 14.5 18.3666667 14.5 15.4666667"></polygon>
            <polygon class="actstat1" id="Fill-20" fill="#707070" points="16.4333333 21.2666667 16.4333333 21.2666667 16.4333333 24.1666667 19.3333333 24.1666667 19.3333333 21.2666667"></polygon>
            <polygon class="actstat1" id="Fill-21" fill="#707070" points="16.4333333 36.7333333 16.4333333 36.7333333 16.4333333 39.6333333 19.3333333 39.6333333 19.3333333 36.7333333"></polygon>
            <polygon class="actstat1" id="Fill-22" fill="#707070" points="4.83333333 35.7666667 7.73333333 35.7666667 7.73333333 32.8666667 4.83333333 32.8666667"></polygon>
            <polygon class="actstat1" id="Fill-23" fill="#707070" points="33.8333333 21.2666667 36.7333333 21.2666667 36.7333333 18.3666667 33.8333333 18.3666667"></polygon>
            <polygon class="actstat1" id="Fill-24" fill="#707070" points="4.83333333 40.6 7.73333333 40.6 7.73333333 37.7 4.83333333 37.7"></polygon>
            <polygon class="actstat1" id="Fill-25" fill="#707070" points="33.8333333 26.1 36.7333333 26.1 36.7333333 23.2 33.8333333 23.2"></polygon>
            <polygon class="actstat1" id="Fill-26" fill="#707070" points="8.7 28.0333333 11.6 28.0333333 11.6 25.1333333 8.7 25.1333333"></polygon>
            <polygon class="actstat1" id="Fill-27" fill="#707070" points="44.4666667 34.8 47.3666667 34.8 47.3666667 31.9 44.4666667 31.9"></polygon>
            <polygon class="actstat1" id="Fill-28" fill="#707070" points="26.1 5.8 29 5.8 29 2.9 26.1 2.9"></polygon>
            <polygon class="actstat1" id="Fill-29" fill="#707070" points="8.7 31.9 11.6 31.9 11.6 29 8.7 29"></polygon>
            <polygon class="actstat1" id="Fill-30" fill="#707070" points="26.1 43.5 29 43.5 29 40.6 26.1 40.6"></polygon>
            <polygon class="actstat1" id="Fill-31" fill="#707070" points="8.7 40.6 11.6 40.6 11.6 37.7 8.7 37.7"></polygon>
            <polygon class="actstat1" id="Fill-32" fill="#707070" points="37.7 26.1 40.6 26.1 40.6 23.2 37.7 23.2"></polygon>
            <path class="actstat" d="M0,46.4 L52.2,46.4" id="Stroke-33" stroke="#707070" stroke-width="2" stroke-linecap="round"></path>
        </g>
    </g>
</svg>
                                 
<%} else if("Clubs and societies".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
        <title>clubs and societies</title>
        <desc>Created with Sketch.</desc>
        <defs></defs>
        <g id="Filter-icon/practical" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
           <g class="actstat" id="Icon" transform="translate(4.000000, 4.000000)" stroke="#707070" stroke-width="2">
              <path d="M16.1652,28.5788 C16.1652,24.1608 12.5832,20.5788 8.1652,20.5788 C4.8382,20.5788 1.9892,22.6108 0.7832,25.4998 L7.5312,25.4998 L7.5312,25.5058 C7.5532,25.5058 7.5732,25.4998 7.5942,25.4998 C9.2512,25.4998 10.5942,26.8428 10.5942,28.4998 C10.5942,30.1568 9.2512,31.4998 7.5942,31.4998 C7.5732,31.4998 7.5532,31.4948 7.5312,31.4938 L7.5312,31.4998 L0.7342,31.4998 C1.9032,34.4698 4.7802,36.5788 8.1652,36.5788 C12.5832,36.5788 16.1652,32.9968 16.1652,28.5788 Z" id="Stroke-1"></path>
              <path d="M15.7479,31.5788 L21.9779,31.5788" id="Stroke-3"></path>
              <path d="M22.0393,14.0708 L22.0393,46.4798 L22.0453,46.4798 C22.0453,46.5018 22.0393,46.5218 22.0393,46.5428 C22.0393,48.1998 23.3823,49.5428 25.0393,49.5428 C26.6963,49.5428 28.0393,48.1998 28.0393,46.5428 C28.0393,46.5218 28.0333,46.5018 28.0333,46.4798 L28.0393,46.4798 L28.0393,14.0708" id="Stroke-4"></path>
              <polygon id="Stroke-6" points="31.035 11.0002 36.035 11.0002 36.035 4.0002 31.035 4.0002"></polygon>
              <path d="M37.0471,0.5 C37.0471,0.5 36.0471,0.5 36.0471,1.5 L36.0471,13.5 C36.0471,13.5 36.0471,14.5 37.0471,14.5 L41.0471,14.5 C41.0471,14.5 42.0471,14.5 42.0471,13.5 L42.0471,1.5 C42.0471,1.5 42.0471,0.5 41.0471,0.5 L37.0471,0.5 Z" id="Stroke-7"></path>
              <path d="M18.0314,0.5 L7.6244,6 L17.0314,6.031 L17.0314,13.5 C17.0314,13.5 17.0314,14.5 18.0314,14.5 L29.8244,14.5 C29.8244,14.5 30.8244,14.5 30.8244,13.5 L30.8244,1.5 C30.8244,1.5 30.8244,0.5 29.8244,0.5 L18.0314,0.5 Z" id="Stroke-9" stroke-linecap="round" stroke-linejoin="round"></path>
              <path d="M22.0286,25.5788 L15.7476,25.5788" id="Stroke-12"></path>
              <path d="M28.0179,31.5788 L46.0319,31.5788 L46.0319,31.5728 C46.0529,31.5728 46.0729,31.5788 46.0939,31.5788 C47.7509,31.5788 49.0939,30.2358 49.0939,28.5788 C49.0939,26.9218 47.7509,25.5788 46.0939,25.5788 C46.0729,25.5788 46.0529,25.5848 46.0319,25.5848 L46.0319,25.5788 L27.9899,25.5788" id="Stroke-13"></path>
           </g>
        </g>
     </svg>
                                 
<%} else if("Student support".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
        <title>student support</title>
        <desc>Created with Sketch.</desc>
        <defs></defs>
        <g id="Filter-icon/student-support" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
           <g class="actstat" id="Page-1" transform="translate(5.000000, 4.000000)" stroke="#707070" stroke-width="2">
              <path d="M2.06414387,34.1171315 C0.771889199,35.7143663 0,37.7365014 0,39.9373708 C0,45.0971056 4.24621526,49.280301 9.48368139,49.280301 C11.6137987,49.280301 13.5798071,48.5881118 15.1631695,47.4198393" id="Stroke-1"></path>
              <path d="M33.1694334,47.5465381 C34.7494986,48.6357166 36.7057699,49.280301 38.8240992,49.280301 C44.0744469,49.280301 48.3326029,45.3189527 48.3326029,40.4345718 C48.3326029,38.415438 47.6049957,36.5539889 46.3812926,35.0648296" id="Stroke-3"></path>
              <path d="M15.1631695,2.76491904 C13.5833281,1.62319686 11.6287912,0.947698097 9.51213451,0.947698097 C4.25895483,0.947698097 0,5.10815637 0,10.2398461 C0,12.4667296 0.801500725,14.5101941 2.13816241,16.1108676" id="Stroke-5"></path>
              <path d="M46.325495,16.1108676 C47.5829205,14.5172783 48.3326029,12.5108082 48.3326029,10.3313525 C48.3326029,5.14912736 44.0953751,0.947698097 38.8689944,0.947698097 C36.729396,0.947698097 34.7543821,1.65269613 33.1694334,2.84074838" id="Stroke-7"></path>
              <path d="M24.1663015,49.280301 C37.5133498,49.280301 48.3326029,38.4610479 48.3326029,25.1139996 C48.3326029,11.7669513 37.5133498,0.947698097 24.1663015,0.947698097 C10.8192532,0.947698097 0,11.7669513 0,25.1139996 C0,38.4610479 10.8192532,49.280301 24.1663015,49.280301 Z" id="Stroke-9"></path>
              <path d="M48.3326029,25.1139996 C48.3326029,11.7669513 37.5133498,0.947698097 24.1663015,0.947698097 C10.8200587,0.947698097 0,11.7669513 0,25.1139996 C0,38.4602423 10.8200587,49.280301 24.1663015,49.280301 C37.5133498,49.280301 48.3326029,38.4602423 48.3326029,25.1139996 Z M36.0125277,25.5878486 C36.0125277,31.86865 30.9201396,36.9602258 24.6401505,36.9602258 C18.3593491,36.9602258 13.2677734,31.86865 13.2677734,25.5878486 C13.2677734,19.3070472 18.3593491,14.2154714 24.6401505,14.2154714 C30.9201396,14.2154714 36.0125277,19.3070472 36.0125277,25.5878486 Z" id="Stroke-11"></path>
              <path d="M0.966666667,30.9333333 L14.2534086,30.9333333" id="Stroke-13" stroke-linecap="round"></path>
              <path d="M0.966666667,20.3 L14.2534086,20.3" id="Stroke-15" stroke-linecap="round"></path>
              <path d="M34.8,30.9333333 L47.3666667,30.9333333" id="Stroke-17" stroke-linecap="round"></path>
              <path d="M34.8,20.3 L47.3666667,20.3" id="Stroke-19" stroke-linecap="round"></path>
              <path d="M19.3333333,35.7666667 L19.3333333,48.3333333" id="Stroke-21" stroke-linecap="round"></path>
              <path d="M29.9666667,35.7666667 L29.9666667,48.3333333" id="Stroke-23" stroke-linecap="round"></path>
              <path d="M19.3333333,1.93333333 L19.3333333,15.2011067" id="Stroke-25" stroke-linecap="round"></path>
              <path d="M29.9666667,1.93333333 L29.9666667,15.2011067" id="Stroke-27" stroke-linecap="round"></path>
           </g>
        </g>
     </svg>
                                 
                                 
<%} else if("Course and lecturers".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
        <title>course and lecturers</title>
        <desc>Created with Sketch.</desc>
        <defs></defs>
        <g id="Filter-icon/courses" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <path class="actstat1" d="M30.6067288,38.2821411 C30.6846789,38.4141978 30.7332831,38.509572 30.7947261,38.5976098 C32.8471068,41.5496266 34.9049899,44.4988922 36.9527853,47.4545772 C37.1114367,47.6829252 37.2260693,47.7315294 37.4709243,47.5554538 C38.4751053,46.8355616 39.4948762,46.1385958 40.5082277,45.4315424 C40.604519,45.364597 40.6925568,45.2866469 40.8347012,45.1729314 C40.6457868,45.1215761 40.5238177,45.0793913 40.3981805,45.0564648 C37.0041407,44.4145226 35.0572219,40.7068483 36.4392316,37.5310688 C36.9271076,36.4131724 37.7442083,35.6034083 38.7236286,34.914696 C39.6315182,34.2764221 40.5146471,33.6005488 41.4427121,32.9145878 C41.0933121,32.4147899 40.7622534,31.9415869 40.4174388,31.4472914 C37.1187732,33.7463614 33.8714631,36.0087489 30.6067288,38.2821411 M29.0000393,39.0964906 C28.9642739,39.1084123 28.9285086,39.1221682 28.8927432,39.1359241 C28.8872409,39.2542249 28.8771532,39.3725257 28.8771532,39.4908264 C28.8762362,43.3727423 28.8762362,47.253741 28.8771532,51.1356568 C28.8771532,51.6308693 28.8780703,51.6327035 29.3558586,51.6327035 C34.6702225,51.6336205 39.9836693,51.6299523 45.2980331,51.6391229 C45.6382625,51.6391229 45.7611485,51.563924 45.7583974,51.1961828 C45.7418903,48.9200394 45.7510609,46.6420619 45.7492268,44.3650014 C45.7492268,44.2503689 45.7345538,44.1366534 45.7217149,43.9431537 C42.6917481,46.0523922 39.7241413,48.1166947 36.7336079,50.1975043 C34.1410788,46.4751569 31.570559,42.7849067 29.0000393,39.0964906 M37.7442083,20.8570803 C40.8961442,20.9698787 44.0398265,18.3305794 44.0544995,14.591725 C44.0682554,11.1353251 41.2391247,8.33095501 37.7827248,8.30527732 C34.5445852,8.2805167 31.4751848,10.8666265 31.4779359,14.5908079 C31.4806871,18.4488802 34.7179096,20.9552057 37.7442083,20.8570803 M54.6593836,34.5854714 C53.9312378,33.5473593 53.2361062,32.5569343 52.5400575,31.5674263 C50.2116417,28.2522537 47.8786405,24.9407493 45.5575612,21.6209914 C45.3622274,21.341288 45.1568059,21.218402 44.8074059,21.233992 C44.123279,21.264255 43.4354839,21.2220702 42.751357,21.2532503 C42.4734877,21.2660891 42.1708579,21.340371 41.9342563,21.4788471 C40.6696303,22.2170805 39.3114642,22.6150847 37.8533384,22.6306747 C36.2906679,22.6471818 34.8371275,22.2354217 33.4991366,21.4155699 C33.3340658,21.3137762 33.1158055,21.2541673 32.9204716,21.2504991 C31.9043689,21.2349091 30.8882662,21.2321579 29.8721635,21.2523332 C29.6575715,21.2569185 29.4108823,21.3321174 29.2366408,21.4550035 C26.938488,23.0800342 24.6504228,24.7197378 22.3596064,26.3557732 C22.2532274,26.4318892 22.1431802,26.5015858 22.0257965,26.580453 C21.9505975,26.4914981 21.8946569,26.428221 21.8423844,26.3621926 C20.0623706,24.1181463 18.2832738,21.8741 16.5014258,19.6309708 C15.7329294,18.6634723 14.7452556,18.1343285 13.4916343,18.1545039 C11.9968261,18.1783474 10.8816809,18.856972 10.2113099,20.1802898 C9.52626597,21.5292854 9.65740558,22.8581056 10.5460369,24.083298 C11.0339129,24.7564202 11.5722273,25.3937771 12.0894492,26.0467239 C14.8195374,29.490285 17.5496256,32.932929 20.2806309,36.3755729 C20.5915143,36.7671576 20.5960996,36.7680747 21.0161132,36.468196 C23.5160193,34.6826798 26.0140913,32.8962466 28.5139974,31.1107303 C28.614874,31.0382826 28.721253,30.9722543 28.9064991,30.8502852 L28.9064991,37.3238132 C33.1167225,34.4103129 37.2453275,31.5527534 41.4051126,28.6731843 C42.4120447,30.1047153 43.392382,31.4995639 44.3846412,32.9118366 C44.2489163,33.0136303 44.153542,33.0869951 44.0563336,33.1585258 C42.3946205,34.3901376 40.710898,35.5924035 39.0785309,36.8625319 C37.9844781,37.7144808 37.572718,38.9250003 37.7955637,40.2684935 C38.2660155,43.1113801 41.4225367,44.3457431 43.7518696,42.6326747 C46.7194764,40.4500714 49.683415,38.2619658 52.6473536,36.0747772 C53.3058028,35.5878182 53.9624179,35.1008593 54.6593836,34.5854714 M10.822072,17.0164322 C11.3723081,16.8403566 11.8821936,16.6193451 12.4150056,16.5184685 C14.5728482,16.1067084 16.3858763,16.7523188 17.7770566,18.446129 C19.2214264,20.2050505 20.6126067,22.0089079 22.0303818,23.7898388 C22.4311371,24.2942219 22.3357628,24.2226912 22.7878735,23.9026372 C24.7329582,22.5197104 26.6679552,21.1230277 28.6185422,19.7483545 C28.8120419,19.6107955 29.0825747,19.5319283 29.3219274,19.5200065 C29.9647866,19.4888264 30.610397,19.5099188 31.356884,19.5099188 C29.574119,16.8687855 29.2027096,14.1460337 30.4453262,11.2802206 C31.1340384,9.6927894 32.2656907,8.47493346 33.7669182,7.61748218 C36.7941339,5.88607252 40.6467038,6.31067139 43.1695364,8.64092136 C44.6891052,10.0440235 45.5749853,11.7763502 45.7620656,13.8342333 C45.9473118,15.8691898 45.4310069,17.7335732 44.1654638,19.4677341 C44.4341624,19.4851582 44.625828,19.5355965 44.8028206,19.5025823 C45.8345134,19.3136679 46.4296854,19.7676127 47.0184381,20.625064 C50.2483241,25.3240805 53.5543261,29.9708245 56.8346504,34.6359097 C56.904347,34.7358693 56.9712924,34.837663 57.0666667,34.9770561 C56.8970105,35.1054446 56.7374421,35.2301648 56.5742053,35.3493826 C53.689134,37.4787964 50.8058967,39.6109613 47.9116547,41.7284533 C47.6218637,41.9402942 47.52007,42.1603887 47.5209871,42.515291 C47.534743,45.9808615 47.5246553,49.4455149 47.5374941,52.9101683 C47.5384112,53.2568171 47.4522075,53.3641131 47.093637,53.3641131 C40.5916802,53.3540255 34.0906405,53.3540255 27.5886836,53.3641131 C27.2264449,53.3641131 27.1457436,53.2513147 27.1457436,52.9065001 C27.1558312,46.8951705 27.15308,40.8829238 27.15308,34.8706772 L27.15308,34.3708793 C27.1200659,34.3488699 27.0861347,34.3277775 27.0531205,34.304851 C24.8017377,35.9115405 22.5503549,37.519147 20.2622897,39.1524312 C19.8358567,38.6205363 19.4158432,38.1023973 19.0013319,37.579673 C15.7696118,33.5033404 12.5406428,29.4242566 9.30708858,25.3497582 C7.61052722,23.2111738 7.61969782,20.4517397 9.34010275,18.3269112 C9.40062872,18.2517122 9.45381821,18.1700939 9.49416886,18.1150703 C6.73473473,14.561462 3.98630531,11.0252779 1.2424612,7.48725963 C0.881139478,7.02139305 0.930660728,6.50692227 1.34425488,6.18961945 C1.75784903,5.87139956 2.27140274,5.95760322 2.62905622,6.4188845 C5.27385783,9.81934371 7.91682532,13.2225541 10.5588758,16.6257645 C10.6560841,16.7504847 10.7367854,16.8880437 10.822072,17.0164322" id="Fill-1" fill="#707070"></path>
        </g>
    </svg>
                                 
                                 
<%} else if("Giving back".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
        <title>giving back</title>
        <desc>Created with Sketch.</desc>
        <defs></defs>
        <g id="Filter-icon/giving-back" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
           <g id="Page-1" transform="translate(6.000000, 7.000000)">
              <g id="Group" transform="translate(14.500000, 7.000000)">
                 <g id="Group-24">
                    <path d="M8,0 L0,10" id="Fill-21" fill="#FFFFFF"></path>
                    <path class="actstat" d="M8,0 L0,10" id="Stroke-23" stroke="#707070" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
                 </g>
                 <path class="actstat" d="M12,5 C12,5 5.37944284,17.8318622 0,10.9928796" id="Stroke-27" stroke="#707070" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
              </g>
              <path class="actstat" d="M13.0677265,0 C5.23410279,0 0,7.10131337 0,15.0071593 C0,22.4228063 3.81463415,28.831777 8.4777439,33.8904268 C13.1408537,38.9490766 18.7075348,42.698845 22.526176,44.8749235 C22.8236934,45.0416922 23.1763066,45.0416922 23.473824,44.8749235 C27.2924652,42.698845 32.8581446,38.9490766 37.5222561,33.8904268 C42.1853659,28.831777 46,22.4228063 46,15.0071593 C46,7.10131337 40.7648955,0 32.9312718,0 C28.3142422,0 25.2459059,2.59956988 23,6.1148514 C20.7540941,2.59956988 17.6857578,0 13.0677265,0 Z" id="Stroke-3" stroke="#707070" stroke-width="2"></path>
              <g id="Group-8" transform="translate(8.000000, 20.000000)">
                 <path d="M10,0 L0,12" id="Fill-5" fill="#FFFFFF"></path>
                 <path class="actstat" d="M10,0 L0,12" id="Stroke-7" stroke="#707070" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
              </g>
              <g id="Group-12" class="actstat" transform="translate(12.000000, 26.000000)" stroke="#707070" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                 <path d="M0,10 C1.33367795,10 8,0 8,0" id="Stroke-11"></path>
              </g>
              <g id="Group-16" class="actstat" transform="translate(16.000000, 30.000000)" stroke="#707070" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                 <path d="M0,10 C1.21180328,9.79731127 8,0 8,0" id="Stroke-15"></path>
              </g>
              <g id="Group-20" class="actstat" transform="translate(20.000000, 34.000000)" stroke="#707070" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                 <path d="M0,9 C1.40741747,8.84980867 7,0 7,0" id="Stroke-19"></path>
              </g>
              <g id="Group-28" transform="translate(15.000000, 12.000000)"></g>
           </g>
        </g>
     </svg>


<%} else if("Students' union".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
        <title>student union</title>
        <desc>Created with Sketch.</desc>
        <defs></defs>
        <g id="Filter-icon/student-union" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
           <g class="actstat" id="Page-1" transform="translate(5.000000, 7.000000)" stroke="#707070" stroke-width="2">
              <path d="M44.1919676,42.0102625 C44.1919676,42.0102625 43.9958517,44.4666667 41.5028529,44.4666667 L28.090519,44.4666667 C28.090519,44.4666667 25.5975202,44.4666667 25.4005733,42.0102625 L22.2369577,2.45640419 C22.2369577,2.45640419 22.0408418,0 24.5338407,0 L45.0595312,0 C45.0595312,0 47.5525301,0 47.3555832,2.45640419 L44.1919676,42.0102625 Z" id="Stroke-3"></path>
              <path d="M11.5543776,29.9666667 L0,17.4372095 L23.2,17.4 L11.5543776,29.9666667 Z M11.6,29 L11.6,41.5666667 L11.6,29 Z" id="Stroke-7"></path>
              <path d="M3.58742261,43.9289586 C3.58742261,43.9289586 1.98247479,44.4666667 3.67724546,44.4666667 L19.5199616,44.4666667 C19.5199616,44.4666667 21.2147323,44.4666667 19.6080897,43.9364848 L13.1349131,41.8007049 C13.1349131,41.8007049 11.5274232,41.2696868 9.92247535,41.8073949 L3.58742261,43.9289586 Z" id="Stroke-11"></path>
              <path d="M22.2333333,4.83333333 L47.3666667,4.83333333" id="Stroke-13"></path>
           </g>
        </g>
     </svg>

<%} else if("Uni facilities".equalsIgnoreCase(svgType)) { %>

    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
        <title>uni facilities</title>
        <desc>Created with Sketch.</desc>
        <defs></defs>
        <g id="Filter-icon/uni-facilities" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
           <g class="actstat" id="Page-1" transform="translate(4.000000, 10.000000)" stroke="#707070" stroke-width="2">
              <path d="M39.8864475,0 L9.41355246,0 C7.4181337,0 5.8,1.68432262 5.8,3.76196466 L5.8,24.2713687 C5.8,26.3490107 7.4181337,28.0333333 9.41355246,28.0333333 L39.8864475,28.0333333 C41.8818663,28.0333333 43.5,26.3490107 43.5,24.2713687 L43.5,3.76196466 C43.5,1.68432262 41.8818663,0 39.8864475,0 Z" id="Stroke-3"></path>
              <path d="M48.6463416,34.2114182 L45.668303,31.211837 C43.4339385,29.1967337 42.5114485,28.0333333 40.4550644,28.0333333 L8.84473963,28.0333333 C6.78835556,28.0333333 5.8667011,29.1967337 3.63233662,31.211837 L0.6534625,33.9248185 C-0.751997883,35.5529313 0.25237987,37.7 2.30792835,37.7 L46.9918757,37.7 C49.0465886,37.7 50.0526375,35.839531 48.6463416,34.2114182 Z M29,33.8333333 L21.2666667,33.8333333 L21.2666667,32.8728617 L23.0262053,30.9333333 L27.2404613,30.9333333 L28.9686704,32.8728617 L29,33.8333333 Z" id="Stroke-7" stroke-linecap="round" stroke-linejoin="round"></path>
              <path d="M38.8234234,3.86666667 C38.8234234,3.86666667 39.6333333,3.86666667 39.6333333,4.67866667 L39.6333333,23.3546667 C39.6333333,23.3546667 39.6333333,24.1666667 38.8234234,24.1666667 L10.4765766,24.1666667 C10.4765766,24.1666667 9.66666667,24.1666667 9.66666667,23.3546667 L9.66666667,4.67866667 C9.66666667,4.67866667 9.66666667,3.86666667 10.4765766,3.86666667 L38.8234234,3.86666667 Z" id="Stroke-11"></path>
           </g>
        </g>
     </svg>
                                 
<%} else if("Job prospects".equalsIgnoreCase(svgType)) { %>


    <svg width="40px" height="40px" viewBox="0 0 58 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
        <title>job prospects</title>
        <desc>Created with Sketch.</desc>
        <defs></defs>
        <g id="Filter-icon/job-prospects" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
           <g class="actstat" id="Page-1" transform="translate(5.000000, 8.000000)" stroke="#707070" stroke-width="2">
              <path d="M48.3333333,34.5845312 C48.3333333,38.9747455 44.8991655,42.5333333 40.6618784,42.5333333 L7.67307908,42.5333333 C3.43497984,42.5333333 0,38.9747455 0,34.5845312 L0,14.7154688 C0,10.3252545 3.43497984,6.76666667 7.67307908,6.76666667 L40.6618784,6.76666667 C44.8991655,6.76666667 48.3333333,10.3252545 48.3333333,14.7154688 L48.3333333,34.5845312 Z" id="Stroke-3"></path>
              <path d="M15.1633987,7.73333333 L15.1633987,5.16331995 C15.1633987,2.31223561 17.6143387,0 20.6382364,0 L27.6950969,0 C30.7189946,0 33.1699346,2.31223561 33.1699346,5.16331995 L33.1699346,7.4584739" id="Stroke-5"></path>
              <path d="M0,24.1666667 L13.2679739,24.1666667" id="Stroke-7"></path>
              <path d="M14.142449,21.2666667 C14.142449,21.2666667 13.2679739,21.2666667 13.2679739,22.0682145 L13.2679739,27.2317855 C13.2679739,27.2317855 13.2679739,28.0333333 14.142449,28.0333333 L16.1843484,28.0333333 C16.1843484,28.0333333 17.0588235,28.0333333 17.0588235,27.2317855 L17.0588235,22.0682145 C17.0588235,22.0682145 17.0588235,21.2666667 16.1843484,21.2666667 L14.142449,21.2666667 Z" id="Stroke-9"></path>
              <path d="M32.1489849,21.2666667 C32.1489849,21.2666667 31.2745098,21.2666667 31.2745098,22.0682145 L31.2745098,27.2317855 C31.2745098,27.2317855 31.2745098,28.0333333 32.1489849,28.0333333 L34.1908844,28.0333333 C34.1908844,28.0333333 35.0653595,28.0333333 35.0653595,27.2317855 L35.0653595,22.0682145 C35.0653595,22.0682145 35.0653595,21.2666667 34.1908844,21.2666667 L32.1489849,21.2666667 Z" id="Stroke-11"></path>
              <path d="M17.0588235,24.1666667 L31.2745098,24.1666667" id="Stroke-14"></path>
              <path d="M34.1176471,24.1666667 L47.3856209,24.1666667" id="Stroke-15"></path>
           </g>
        </g>
     </svg>
     
<%}%>