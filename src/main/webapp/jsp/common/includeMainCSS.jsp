<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants" %>
<meta http-equiv="content-language" content=" en-gb "/><%--21-Jan-2014--%>
<jsp:include page="/jsp/common/includeIconImg.jsp"/><%--Icon images added by Prabha on 31_May_2016--%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
<%
  String cdScreensCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.cd.screens.css");
String searchCssName = CommonUtil.getResourceMessage("wuni.clearing.searchpage.css", null);
%>

<% String envName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
   String commonCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.css");
   String domainSpecPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
   String mainHeaderCSS = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);//Added this css for Header changes by Hema.S on 23_OCT_2018_REL
   int stind3 = request.getRequestURI().lastIndexOf("/");
  int len3 = request.getRequestURI().length();
  String pagename3 = pageContext.getAttribute("pagename3") != null ?  (String)pageContext.getAttribute("pagename3") : request.getRequestURI().substring(stind3+1,len3);
//Added condition to restrict the inlcude CSS by Sangeeth.S for CMMT release 
  String notInCssPageName =GlobalConstants.CMMT_PAGE_NAMES; //page names not required to include the css. 
 
  if(!(notInCssPageName.contains(pagename3) || "opendaySearchResults.jsp".equalsIgnoreCase(pagename3) || "openDaysLandingPage.jsp".equalsIgnoreCase(pagename3) || "opendaysProviderLanding.jsp".equalsIgnoreCase(pagename3) || "reviewHome.jsp".equalsIgnoreCase(pagename3) || "openDayProviderLandingPage.jsp".equalsIgnoreCase(pagename3))){
  if(("LIVE").equals(envName)){
 %>
 <%--Changed environment based path for main.css too for 01_Sep_2015, By Thiyagu G--%>
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=mainHeaderCSS%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=commonCssName%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=cdScreensCssName%>" media="screen" />
<%} else if("TEST".equals(envName)){%>
 <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=mainHeaderCSS%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=commonCssName%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=cdScreensCssName%>" media="screen" />
<%} else if("DEV".equals(envName)){%>
 <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=mainHeaderCSS%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=commonCssName%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=cdScreensCssName%>" media="screen" />
<%}%>
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=searchCssName%>" media="screen" />
<!--[if IE 7]>    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/whatuni-screens_ie7hacks-wu552.css" media="screen" />   <![endif]-->
 <% 
  if("unigeneralreview.jsp".equalsIgnoreCase(pagename3)){
  %> 
  <script language="javascript" type="text/javascript"> <%--Script provided by Rakesh reg site content secuity --%>
      if (window!= top) top.location.href = location.href;
  </script>
<%}
}%>

<jsp:include page="/jsp/common/abTesting.jsp"/>
<%
String homePageName = "newUser.jsp, retUser.jsp, loggedIn.jsp";
if(!homePageName.contains(pagename3) && request.getAttribute("showBanner")==null){%>
  <jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
<%}%>
<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
	<jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>
<%-- Facebook pixel tracking script included in head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/facebookPixelTracking.jsp"/>