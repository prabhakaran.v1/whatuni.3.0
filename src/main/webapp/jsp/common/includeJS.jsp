<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="WUI.utilities.CommonUtil" %>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
   
 <%int stind1 = request.getRequestURI().lastIndexOf("/");
   int len1 = request.getRequestURI().length();
   String page_name = "";
		   
   if(pageContext.getAttribute("pagename3") != null){
	 page_name = (String)pageContext.getAttribute("pagename3");
   }else{
	 page_name =  request.getRequestURI().substring(stind1+1,len1); 
   }
   String pageNames = "newUser.jsp, retUser.jsp, loggedIn.jsp, opendaysearch.jsp";
   String ajaxDynamicListJSName = CommonUtil.getResourceMessage("wuni.ajaxDynamicList.js", null);
   String commonSrchJSName = CommonUtil.getResourceMessage("wuni.commonSrchJSName.js", null);
   String allJSName = CommonUtil.getResourceMessage("wuni.allJsName.js", null);
   String searchJSName = CommonUtil.getResourceMessage("wuni.searchJSName.js", null);
   String requestProspJsName = CommonUtil.getResourceMessage("wuni.requestProspJsName.js", null);
   String addBasketJs = CommonUtil.getResourceMessage("wuni.add.basket.js", null);  
   String modalJs = CommonUtil.getResourceMessage("wuni.modal.js", null);
   String cpeStatsLogJsName = CommonUtil.getResourceMessage("wuni.cpe.stats.log.js", null);  
   pageContext.setAttribute("PAGE_NAME", page_name);
   %>
<%if(!pageNames.contains(page_name) && "richProfileLanding.jsp".equalsIgnoreCase(page_name)){%>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/javascripts/'/><spring:message code='wuni.interaction.js'/>"></script>
<%}else if(!pageNames.contains(page_name)){%> 
  <c:choose>
    <c:when test="${fn:containsIgnoreCase(PAGE_NAME, 'browseMoneyPageResults.jsp') or fn:containsIgnoreCase(PAGE_NAME, 'courseSearchResult.jsp')}"> 
      <link rel="preload" as="script" href="<wu:jspath source='/js/javascripts/'/><spring:message code='wuni.interaction.js'/>">
    </c:when>
    <c:otherwise>
     <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/javascripts/'/><spring:message code='wuni.interaction.js'/>"></script>
    </c:otherwise>
  </c:choose>
  
<%}%>
<c:choose>
    <c:when test="${fn:containsIgnoreCase(PAGE_NAME, 'browseMoneyPageResults.jsp') or fn:containsIgnoreCase(PAGE_NAME, 'courseSearchResult.jsp')}"> 
      <link rel="preload" as="script" href="<%=CommonUtil.getJsPath()%>/js/<%=cpeStatsLogJsName%>">
    </c:when>
    <c:otherwise>
     <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=cpeStatsLogJsName%>"> </script>
    </c:otherwise>
  </c:choose>


<script type="text/javascript" src="https://apis.google.com/js/client.js" defer="defer"></script>
<%if(!pageNames.contains(page_name) && "richProfileLanding.jsp".equalsIgnoreCase(page_name)){%> 
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/ajax_wu564.js" defer="defer"> </script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/<%=ajaxDynamicListJSName%>" defer="defer"> </script>
<%}else{%>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/ajax_wu564.js" > </script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/<%=ajaxDynamicListJSName%>"> </script>
<%}%>
<c:if test="${'Y' ne cpcClearingFlag}">
<%if(!pageNames.contains(page_name) && !"browseMoneyPageResults.jsp".equals(page_name) && !"clearingBrowseMoneyPage.jsp".equalsIgnoreCase(page_name) && !"richProfileLanding.jsp".equalsIgnoreCase(page_name) && !"newProviderHome.jsp".equalsIgnoreCase(page_name)){%> 
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=addBasketJs%>"> </script>
<%}else if("richProfileLanding.jsp".equalsIgnoreCase(page_name) || "newProviderHome.jsp".equalsIgnoreCase(page_name)){%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=addBasketJs%>" defer="defer"> </script>
<%}%>
</c:if>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.commonJsName.js'/>"></script> 
 <c:if test="${'Y' ne cpcClearingFlag}">
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=allJSName%>"> </script>    
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=commonSrchJSName%>">  </script>
 <%--
<%if(!pageNames.contains(page_name) ){%> 
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/basket.js"> </script>
<%}%>--%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/common_wu551.js"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=searchJSName%>"> </script>
</c:if>
<c:if test="${empty requestScope.showlightbox}"> 
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/videoplayer/<%=modalJs%>"> </script>
</c:if>
<c:if test="${'Y' ne cpcClearingFlag}">
<%if(!pageNames.contains(page_name) && ("browseMoneyPageResults.jsp".equals(page_name) || "richProfileLanding.jsp".equalsIgnoreCase(page_name))){%> 
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=requestProspJsName%>" defer="defer"> </script>
<%}else{%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=requestProspJsName%>"> </script>
<%}%>
</c:if>