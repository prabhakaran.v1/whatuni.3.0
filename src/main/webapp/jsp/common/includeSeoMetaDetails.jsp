<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${not empty META_TITLE}" >
  <title>${META_TITLE}</title>
</c:if>
<c:if test="${not empty META_DESC}" >
  <meta name="description" content="${META_DESC}"/>
</c:if>
<c:choose>
  <c:when test="${not empty META_ROBOTS}">
    <c:set var="META_ROBOT_VALUE" value="${META_ROBOTS}"></c:set>
  </c:when>
  <c:when test="${not empty param.noindexfollow}">
    <c:set var="META_ROBOT_VALUE" value="${param.noindexfollow}"></c:set>
  </c:when>
</c:choose>
<meta name="ROBOTS" content="${META_ROBOT_VALUE}"/>
<meta name="country" content="United Kingdom"/> 
<meta name="content-type" content="text/html; charset=utf-8"/>
