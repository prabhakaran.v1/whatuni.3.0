<%@page import="WUI.utilities.GlobalFunction,com.wuni.util.seo.SeoUrls, WUI.utilities.CommonUtil,WUI.utilities.SessionData, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator,mobile.util.MobileUtils, WUI.utilities.GlobalConstants" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>

<%
  SeoUrls seoUrl = new SeoUrls(); 
  CommonUtil utilities = new CommonUtil();
  String f_x = session.getAttribute("x") != null && session.getAttribute("x").toString().trim().length() > 0 ? session.getAttribute("x").toString().trim() : "16180339"; 
  String f_y = session.getAttribute("y") != null && session.getAttribute("y").toString().trim().length() > 0 ? session.getAttribute("y").toString().trim() : "0";
  CommonFunction commonFn = new CommonFunction();
  String htmlText = commonFn.getHTMLEditorText("F","0");
  String mywhatunipage = (String)request.getAttribute("mywhatunipage");
  int stind3 = request.getRequestURI().lastIndexOf("/");
  int len3 = request.getRequestURI().length();
  String pagename3 = "";
  if(!GenericValidator.isBlankOrNull(pageContext.getAttribute("pagename3").toString())){
	  pagename3 = (String)pageContext.getAttribute("pagename3");
  }else{
	  pagename3 = request.getRequestURI().substring(stind3+1,len3); 
  }
  String homePageName = "newUser.jsp, retUser.jsp, loggedIn.jsp";
  String clearingUserType = "" ;
  String showClearingSplashscreen = "" ;
  String showSplashPopUp = homePageName + ", articleDetails.jsp,opendaySearchResults.jsp,openDaysLandingPage.jsp,opendaysProviderLanding.jsp";
  String clearingPopUpFlag = commonFn.getWUSysVarValue("CLEARING_POP_UP_FLAG");  
  String requestURL = (String)request.getAttribute("REQUEST_URI");
  boolean isClearing = (requestURL != null) ? new GlobalFunction().isClearing(requestURL) : false;
  String noJSlogging = "";
  if(request.getAttribute("noJSLogging")!=null && "false".equals(request.getAttribute("noJSLogging"))){
    noJSlogging = (String)request.getAttribute("noJSLogging");
  }
  String domainSpecPath = commonFn.getSchemeName(request) + CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
  String dynFacebookLoginJSName = CommonUtil.getResourceMessage("wuni.facebook.login.js", null);
  String commonUserProfileJSName = CommonUtil.getResourceMessage("wuni.common.user.profile.js", null);
  String socialBoxJs = CommonUtil.getResourceMessage("wuni.social.box.js", null);
  String emailDomainJsName = CommonUtil.getResourceMessage("wuni.email.domain.js", null);
  String clearingSwitchFlag = !GenericValidator.isBlankOrNull((String)request.getAttribute("clearingSwitchFlag")) ? (String)request.getAttribute("clearingSwitchFlag") : "OFF"; 
  String cpeQualificationNetworkId = !GenericValidator.isBlankOrNull((String)request.getSession().getAttribute("cpeQualificationNetworkId")) ? (String)request.getSession().getAttribute("cpeQualificationNetworkId") : "2";
  String profileType = (String)request.getAttribute("setProfileType");
        cpeQualificationNetworkId = (!GenericValidator.isBlankOrNull(profileType) && "CLEARING_PROFILE".equalsIgnoreCase(profileType)) ? GlobalConstants.CLEARING_NETWORK_ID : cpeQualificationNetworkId;
  String userJourneyFlag = !GenericValidator.isBlankOrNull((String)request.getAttribute("USER_JOURNEY_FLAG")) ? (String)request.getAttribute("USER_JOURNEY_FLAG") : "";// Added for cmmt
  String dynTopNavSrchJSName = CommonUtil.getResourceMessage("wuni.topnav.search.popup.js", null);
  String clearingHomeJs = CommonUtil.getResourceMessage("wuni.common.clearing.home.js", null);
  String gradeFilterUcasJs = CommonUtil.getResourceMessage("wuni.ucas.grader.filter.js", null);
  pageContext.setAttribute("clearingHomeJsName", clearingHomeJs);
 
%>
${bodyContent}
<c:set var="pageName" value="${pagename3}"/>
<jsp:include page="/jsp/clearing/include/gradeFilterPopupLayout.jsp">                          
 <jsp:param name="pageName" value="${pagename3}"/>
</jsp:include>
<div id="revLightBox"></div>
<div id="loaderTopNavImg" class="cmm_ldericn zindx" style="display:none;"><img alt="loading" src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif",0)%>"></div>
<%if("CLEARING".equalsIgnoreCase(userJourneyFlag) && !"3".equals(cpeQualificationNetworkId)){%>
  <%--<input type="hidden" id="clearingFlag" value="CLEARING"/>--%>
  <input type="hidden" id="userJourneyFlagId" value="<%=userJourneyFlag%>" />
  <input type="hidden" name="comparepage" id="comparepage" value="CLEARING"/> 
  <input type="hidden" id="clearingProfileUrlFlag" value="CLEARING_PROFILE_URL"/>
<%}%>

<input type="hidden" id="clearingHomeJsName" value="/js/clearing/${clearingHomeJsName}" />
<input type="hidden" id="gradeFilterClrSrchJS" value="/js/clearing/<%=gradeFilterUcasJs%>" />
<input type="hidden" id="gradeFilterClrSrchJSLoaded" value="0" />
<input type="hidden" id="studyLevelPreHidden" value="${not empty requestScope.studylevel ? requestScope.studylevel : not empty sessionScope.studyLevel ? sessionScope.studyLevel : ''}"/>
<input type="hidden" id="gradePointsPreHidden" value="${not empty requestScope.entrypoints ? requestScope.entrypoints : not empty sessionScope.entryPoints ? sessionScope.entryPoints : ''}"/>
<input type="hidden" id="pointsPreHidden" value="${not empty requestScope.points ? requestScope.points : not empty sessionScope.points ? sessionScope.points : ''}"/>
<input type="hidden" id="topNavSrchJS" value="/js/topNavSearchPopup/<%=dynTopNavSrchJSName%>" />
<input type="hidden" id="emailDomainJsName" value="<%=emailDomainJsName%>" />
<input type="hidden" id="facebookLoginJSName" value="<%=dynFacebookLoginJSName%>" />
<input type="hidden" id="commonUserProfileJSName" value="<%=commonUserProfileJSName%>" />
<input type="hidden" id="loggedInUserId" value="<%=new SessionData().getData(request, "y")%>" />
<input type="hidden" id="contextPath" value="/degrees" />
<input type="hidden" id="domainSpecPath" value="<%=domainSpecPath%>" />
<input type="hidden" id="pdfFromPageURL" />
<input type="hidden" id="isMobileUA" />
<input type="hidden" id="clearingSwitchFlag" value="<%=clearingSwitchFlag%>" />
<input type="hidden" id="networkId" name="networkId" value="<%=cpeQualificationNetworkId%>" />
<input type="hidden" id="boostedInstId" name="boostedInstId" value="${sessionScope.boostedInstId}"/>
<input type="hidden" id="sponsoredInstId" name="sponsoredInstId" value="${sessionScope.sponsoredInstId}"/>
<input type="hidden" id="sponsoredListFlag" value="${sponsoredListFlag}"/>
<input type="hidden" id="manualBoostingFlag" value="${manualBoostingFlag}"/>
<c:if test="${not empty sessionScope.sessionData['LAT_LONG']}">
  <input type="hidden" id="latAndLong" value="${sessionScope.sessionData['LAT_LONG']}">
</c:if>
<%if(!"qlBasicForm.jsp".equalsIgnoreCase(pagename3) && !"qlAdvanceForm.jsp".equalsIgnoreCase(pagename3)){%>
<% if(!("".equals(pagename3)) && homePageName.contains(pagename3)){%><%--26_AUG_2014--%>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<section class="ourpartners">
<article class="content-bg">
<div class="part_pod">
  <div class="par_dis">	
    <ul>
      <%--28_Oct_2014 changed partners url from /partners to /about-us/partners/ by Thiyagu G.--%>
      <li><span class="our_pt fnt_lbk">OUR PARTNERS</span></li>
      <%--<li><a class="dib" href="/about-us/partners"><span class="icloud mt15"></span></a></li>--%>
      <li><a class="dib" href="/about-us/partners"><span class="ucas"></span></a></li>
      <li><a class="dib" href="/about-us/partners"><span class="hesa"></span></a></li>      
      <li><a class="dib" href="/about-us/partners"><span class="nyum"></span></a></li>
    </ul>
  </div>
</div>
</article>
</section>
<%}
  new MobileUtils().userAgentCheck(request);  
  String desktopURL = (String)request.getAttribute("desktopURL");
%>  
<input type="hidden" id="isMobileUserAgent" value="<%=new MobileUtils().userAgentCheck(request)%>"/>
<input type="hidden" id="MobileURL" value='<%=(desktopURL!=null && !"".equals(desktopURL) ? desktopURL : "")%>'/>

<%=htmlText%>    
<c:if test="${not empty sessionScope.userInfoList}">        
  <input type="hidden" id="fnchCompCnt"/>
  <input type="hidden" id="dispHomeFinalChoice" value="<%=request.getAttribute("displayFinalChoicePod")%>"/>
</c:if>

<div class="clear"></div>
<%-- loading JS file --%> 
  <%if(!"CONTENT_HUB".equals(request.getParameter("FROM_PAGE")) && !"clearingEbookLandingPage.jsp".equalsIgnoreCase(pagename3) && !"clearingEbookSuccess.jsp".equalsIgnoreCase(pagename3) && !"opendayproviderlandingpage.jsp".equalsIgnoreCase(pagename3) && !"opendayBookingForm.jsp".equalsIgnoreCase(pagename3)) {%>
    <jsp:include page="/tickertape/tickerTape.jsp" />   <%--16-APR-2014--%>
  <%}%>
<%}%>
<%if(!"CONTENT_HUB".equals(request.getParameter("FROM_PAGE"))) {%>
<%@include  file="/jsp/common/includeJS.jsp" %>
<%}%>
<script type="text/javascript">isItMobile();</script>
<script type="text/javascript">loadMobileSiteURL()</script>
<c:if test="${not empty requestScope.showlightbox}"> 
	 <jsp:include page="/videoReview/videoPopUp.jsp" />
</c:if>
<%-- TODO delete after testing --%>
<div id="newvenue" class="dialog" >
    <div id="newvenueform" style="display: none;" ></div>
     <div id="ajax-div" class="lb_pload" style="display: none;">
         <%--Changed By Sekhar K for wu401
         <img src="/degrees/img/whatuni/circle-thickbox.gif" alt="" title="" />--%>
         <%if("richProfileLanding.jsp".equalsIgnoreCase(pagename3)){%>
           <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/cmn_loading_1.svg", 1)%>" alt="loading image"/>
      <%}else{%>
        <img src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/cmn_loading_1.svg",0)%>" alt="loading image" title="" />
      <%}%>
         
         
         <span id="txt">Please wait</span>
         <input id="loading_cancel" type="button" onclick="hm('newvenue');" value="Cancel" class="button"/>
     </div>
 </div> 
<style type="text/css">
#holder_NRW{
  -webkit-overflow-scrolling: touch;
  overflow:auto;
  min-height:400px;
}
</style>
<%--Added write review widget popup code for 09_Jun_2015, By Thiyagu G --%>
<div style="position:fixed;width:100%;height:100%;background:#000;opacity:.8;top:0;position:fixed;display:none;z-index:9999;" id="fade_NRW"></div>
<a style="position:fixed;top:10px;right:15px;color:#fff;font-weight:bold;display:none;font-size:24px;z-index:99999;" id="close_NRW" onclick="customConfirm();">
<i class="fa fa-times"></i></a>
<div style="position:fixed;margin:50px;background:#fff;top:0;display:block;position:fixed;display:none;z-index:99999;" id="holder_NRW">
  <iframe src="" id="iframe_NRW" frameborder="0" width="100%" height="99%"></iframe>
</div>
<input type="hidden" id="lightBoxName" value=""/>
<%String domainPathWidget = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");%>
<input type="hidden" id="domainPathWidget" value="<%=domainPathWidget%>">
<script type="text/javascript" id="indl">
  if(document.getElementById('inner_div_0_0')){document.getElementById('inner_div_0_0').onclick();}
</script><%--11-Feb-2014_REL--%>
<%-- LOAD GAM--%>
<%if(!homePageName.contains(pagename3) && (!"clearingEbookLandingPage.jsp".equalsIgnoreCase(pagename3)) && (!"clearingEbookSuccess.jsp".equalsIgnoreCase(pagename3))){%><%--26_AUG_2014--%>
<%if(request.getAttribute("showBanner")==null){%>
<jsp:include page="/jsp/thirdpartytools/includeCallBanner.jsp"/>
<%}%>
<jsp:include page="/jsp/thirdpartytools/include/includeAdmedoDiv.jsp">
  <jsp:param name="pagename" value="<%=pagename3%>"/>
</jsp:include>
<%}%>

<c:if test="${empty sessionScope.userInfoList}">
<% //Added for 21_JUL_2015_REL by Priyaa for survey auto login - Start of code
String showAutoLoginLBoxFlag = (String)request.getAttribute("showAutoLoginLBoxFlag") ;
String clearingSurveyResponseFlag = (String)request.getAttribute("clearingSurveyResponseFlag") ;
if(("Y".equals(showAutoLoginLBoxFlag)) && (!GenericValidator.isBlankOrNull(clearingSurveyResponseFlag))){
%>
  <script type="text/javascript">
      setTimeout(function(){javaScript:showLightBoxLoginForm('survey-login',650,500);},300);
  </script>
<%}else if("N".equals(showAutoLoginLBoxFlag)){%>
  <script type="text/javascript">
      setTimeout(function(){javaScript:showLightBoxLoginForm('survey-login',650,500);},300);
  </script>
<%}%>
</c:if>
<%--End of code--%>
<%-- ::START:: Yogeswari :: 09.06.2015 :: for clearing splash screen --%>
<script type="text/javascript">var showClearingPopup = false;</script>
<c:if test="${requestScope.clearingSwitchFlag eq 'ON'}">
<%if("ON".equals(clearingPopUpFlag)){%>
<%
clearingUserType = (String)session.getAttribute("USER_TYPE") ;
showClearingSplashscreen = (String)session.getAttribute("SHOW_CLEARING_SPLASH_SCREEN") ;
if((GenericValidator.isBlankOrNull(clearingUserType) || "NON_CLEARING".equals(clearingUserType)) &&  (GenericValidator.isBlankOrNull(showClearingSplashscreen)) && isClearing == false){
if((session.getAttribute("noSplashpopup")==null && !"true".equals(session.getAttribute("noSplashpopup"))) || showSplashPopUp.contains(pagename3)){
%>
 <%--  <script type="text/javascript">          //Hided clearing splash popup on June_23_20_rel by Sangeeth.S 
    var clrFlag = getCookie('<%=GlobalConstants.CLEARING_USER_TYPE_COOKIE%>');
    if(clrFlag == null || clrFlag == ""){
        setTimeout(function(){showLightBoxLoginForm('clearing-2015',467,500, 'Y','','splash-page')},300);
        showClearingPopup = true;
    }
  </script> --%>
<%}}%>
<%}%>
</c:if>
<%-- ::END:: Yogeswari :: 09.06.2015 :: for clearing splash screen --%>
<%-- LOAD GA --%>
<%String nonresponsivepagenames = "ProspectusLocation.jsp,moreReviews.jsp,subjectreviews.jsp,opendaybrowse.jsp,ViewOpenDaysNew.jsp,openDaysProvLanding.jsp,openDaySearch.jsp,qlBasicForm.jsp,prospectusSentSuccess.jsp,downloadProspectusSentSuccess.jsp";%>
    <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />    

<% 
 String insightPageFlag = "no";
 if(request.getAttribute("insightPageFlag") != null){
   insightPageFlag = request.getAttribute("insightPageFlag").toString();
 }%>
 <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />
 <%--Cookie popup changes added by Prabha on 27_JAN_2016_REL--%>
 <script type="text/javascript">
   <%if(session.getAttribute("userInfoList") == null){%>
     //var userType = getCookie('userType');
     var url = contextPath+"/create-cookies.html?cookieName=userType&processType=get";      
     var ajaxObj = new sack();
     ajaxObj.requestFile = url;	  
     ajaxObj.onCompletion = function(){manageUserTypeResponse(ajaxObj)};	
     ajaxObj.runAJAX();
     function manageUserTypeResponse(ajaxObj){
    		var userType = "NO";
    		    if(ajaxObj.response == "YES"){   
    		    	userType = "YES";
    		    }
    		    if(userType == "NO"){
    		          //setSessionCookie('userType', 'newUser');
    		       	  var url = contextPath+"/create-cookies.html?cookieName=userType&cookieValue=newUser";      
    		             var ajaxObj = new sack();
    		             ajaxObj.requestFile = url;	  
    		             ajaxObj.onCompletion = function(){};	
    		             ajaxObj.runAJAX();
    		        }else if(userType == "YES"){
    		          //setSessionCookie('userType', 'returnUser');
    		       	 var url = contextPath+"/create-cookies.html?cookieName=userType&cookieValue=returnUser";      
    		            var ajaxObj = new sack();
    		            ajaxObj.requestFile = url;	  
    		            ajaxObj.onCompletion = function(){};	
    		            ajaxObj.runAJAX();
    		        }
      };
     
   <%}%>
 </script>
<%--End of cookie popup code--%>
<%
  //FOR STATS LOG Entry with the javascript
  if("".equals(noJSlogging)){   //JS logging will be proceed when noJSlogging is empty 30_JUN_15 by Amir
    if(session.getAttribute("stats_log_id") != null){ 
      String session_log_id = (String) session.getAttribute("stats_log_id");
      String inArray[] = session_log_id.split(",");
   %> 
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/statslog-wu514.js"></script>  
  <%out.println("<script type=\"text/javascript\" language=\"javascript\">");
     //Changed WU scheme as part of SSL work for 08_MAR_2016, By Thiyagu G.
     String domainSchemeName=GlobalConstants.WHATUNI_SCHEME_NAME;
     if(inArray !=null){
       for(int arloop = 0; arloop<inArray.length; arloop++){
   %>
      document.write(unescape("%3Cscript type='text/javascript'%3E var boo %3D callStatLogRequest('jslog','%3Fl%3D<%=inArray[arloop]%>','<%=domainSchemeName%><spring:message code="review.autocomplete.ip"/>')%3B %3C/script%3E"));
   <% }
   } 
   session.removeAttribute("stats_log_id");
   out.println("</script>");   
   }
   }
   %>    
    <script type="text/javascript">var timersLB =0;</script>
   <%String userID = new SessionData().getData(request, "y");%>   
   <%   
   if(session.getAttribute("noSplashpopup")==null && !"true".equals(session.getAttribute("noSplashpopup"))){
   if(session.getAttribute("noSplashWin")==null && (GenericValidator.isBlankOrNull(userID) || "0".equals(userID))){
   %>   
    <script type="text/javascript">
      //setTimeout(function(){showLightBoxLoginForm('splash-login',642,450)},180000);      
      var width = document.documentElement.clientWidth;      
      if(width > 992 && !showClearingPopup){ // Yogeswari :: 09.06.2015 :: changes for clearing splash screen --%>
        //timersLB =  setTimeout(function(){showLightBoxLoginForm('popup-newlogin',650,500, 'Y','','splash-page')},180000); //Hided splash popup for 13_Dec_2016, by Thiyagu G.
      }
    </script>
   <%}} 
    session.removeAttribute("noSplashpopup");
   %> 
   <%--Added for 24_NOV_2015_REL by Indumathi.S For redirect lightbox--%>
   <%
     String hotcrs_source = request.getParameter("utm_source");
     String hotcrs_medium = request.getParameter("utm_medium");
     String hotcrs_campaign = request.getParameter("utm_campaign");
     if(("hotcourses").equalsIgnoreCase(hotcrs_source) && 
         ("redirect").equalsIgnoreCase(hotcrs_medium) && 
         ("hcmigration").equalsIgnoreCase(hotcrs_campaign)) {
   %>  
   <script type="text/javascript">   
     var width = document.documentElement.clientWidth;
     clearTimeout(timersLB);
     timersLB = setTimeout(function(){showLightBoxLoginForm('info-popup',664,500, 'Y','','ugpg_lb')});
     if(width > 992){ 
       //timersLB =  setTimeout(function(){showLightBoxLoginForm('popup-newlogin',650,500, 'Y','','splash-page')},180000); //Hided splash popup for 13_Dec_2016, by Thiyagu G.
     }
   </script>    
   <%}%>
   <%--End--%>
   <%--ADded by priyaa for  webclick comparison ligh box dont show this again button 11-AUG-2015 release --%>
  <input type="hidden" id="dontShowCompSplashHidden" value=""/>
  <script type="text/javascript">
    var compSpalshValue=getJavaCookie("DS_WC_TO_COM", "get"); // Added for bug fix by Hemalatha.K on 17-Dec-2019_REL
    if(compSpalshValue=="TRUE"){
       if($$("dontShowCompSplashHidden")){
        $$("dontShowCompSplashHidden").value="TRUE";
       }
    }else{
      compSpalshValue="";
    }
  </script>
  <%-- FOR STATS LOG Entry with the javascript --%>
  
<%--1024 resolution script start here--%>
<% if(!homePageName.contains(pagename3)){
%><%--26_AUG_2014--%>
<script type="text/javascript">
var widthSky = document.documentElement.clientWidth;
if(widthSky > 992){
    var skypescrapperres = parseInt((screen.width),10);
    if(skypescrapperres<1280){
        if(document.getElementById("content-bg")){
            document.getElementById("content-bg").className="csrn_rn"; 
        }if(document.getElementById("wrapper")){
            document.getElementById("wrapper").className="wsrn_rn";
        }if(document.getElementById("sky")){
            document.getElementById("sky").className="hidden";
        }
        //document.getElementById("toprightbanner").className="top_ban"; 16-Apr-2014
        if(document.getElementById("content-cmpr")){
          document.getElementById("content-cmpr").style.padding="0px";
          document.getElementById("content-cmpr").style.width="100%";
        }
        if(document.getElementById("clrb1")){document.getElementById("clrb1").style.left="15px";}
    }
}
</script>
<%}%>
<script type="text/javascript">
initmb();
</script>
<%--1024 resolution script end here--%>  

<%
String nonmywu = request.getParameter("nonmywu");
if(request.getParameter("nonmywu") != null && "NONMYWU".equalsIgnoreCase(nonmywu)){%>
<script type="text/javascript">
   if(document.getElementById('getuserbasketlogged')){
    document.getElementById('getuserbasketlogged').onclick();
  }</script>
<%}%>
<%--25-Mar-2014_REL--%>  
<script type="text/javascript">
setWrapperWidth();
setTimeout(function(){displayTickerTape()}, 2000);
</script>

<c:if test="${not empty sessionScope.userInfoList}">  
<script type="text/javascript">
  var $fch = jQuery.noConflict();
  setTimeout(function(){hideTickerTape()}, 2005);
  function hideTickerTape(){
    var showFinalFive=getFinalFiveCookie("showFinalFive");        
    if($fch("div").hasClass("myFnChLbl") && (showFinalFive==null && showFinalFive!="Dont")){
      $fch("#tickerTape").css('display','none');
    }  
  }
</script>
</c:if>
<c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 998012114;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/998012114/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</c:if>
<%--28_Oct_2014 added event tracking for view hero image by Thiyagu G.--%>
<script type="text/javascript">
<%//18_Nov_2014 added hero image stats view by Thiyagu G.
if(request.getAttribute("hero_image") != null){
String heroImageDetail[] = (request.getAttribute("hero_image").toString()).split("~");
%>
    window.onload = searchEventTracking("home-hero-image", "view", "<%=heroImageDetail[0]%>");
    window.onload = cpeHeroImage("<%=heroImageDetail[1]%>", "<%=heroImageDetail[2]%>","","HERO_IMAGE_VIEW");
<%request.removeAttribute("hero_image");}%>
</script>

<script type="text/javascript">
<%//09_Dec_2014 added view advice details stats logging by Thiyagu G.
if(request.getAttribute("advice_view") != null){
String adviceDetailView = request.getAttribute("advice_view").toString();
%>    
    window.onload = cpeAdviceDetailPage('<%=adviceDetailView%>','ADVICE_DETAIL_VIEW');
<%request.removeAttribute("hero_image");}%>
</script>

<%-- ::START:: Yogeswari :: 19.05.2015 :: this code for scroll-to-top --%>

<script type="text/javascript">
  scrollToTopReadyFunction();
  scrollToTopResizeFunction();
  var $scroll = jQuery.noConflict();
  $scroll(window).scroll(function() {
    scrollToTopScrollFunction();
  });
</script>
<%--Added code for function not called in ql forms by Prabha on 13_Jun_2017--%>
<%String stopLazyloadCallPages = "qlBasicForm.jsp, meetTheTeam.jsp, teamMember.jsp, nofound.jsp, cookies.jsp, error.jsp,reviewHome.jsp, clearingEbookSuccess.jsp, advanceSearchResults.jsp, provisionalOfferLandingPage.jsp, offerSuccess.jsp, whatuniGoLandingPage.jsp, newEntryReqLandingPage.jsp, allErrorPage.jsp, subjectLandingPage.jsp, gradeFilterLandingPage.jsp,applicationPage.jsp, whatuniGoSignUp.jsp, whatuniGoSignIn.jsp, newUserGradeLandingPage.jsp, courseSearchResult.jsp, forgotPassword.jsp, openDayProviderLandingPage.jsp,clearingCourseSearchResult.jsp, overviewPage.jsp, partnershipPage.jsp, contactUsPage.jsp,contactUsSuccess.jsp";
    if(!stopLazyloadCallPages.contains(pagename3) && !"CONTENT_HUB".equals(request.getParameter("FROM_PAGE"))){%>
<script type="text/javascript">
  $scroll(window).scroll(function() {
    lazyloadetStarts();
  });
</script>
<%}%>
<%--End of code--%>
<%if(!"CONTENT_HUB".equals(request.getParameter("FROM_PAGE"))){%>
<p class="bk_stick bk_to_top" style="display:none" id="back-top">
    <a href="#top">
    <%if("richProfileLanding.jsp".equalsIgnoreCase(pagename3)){%>
        <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=CommonUtil.getImgPath("/wu-cont/images/bk_2_top.png", 1)%>" alt="go to top"/>
      <%}else{%>
        <img src="<%=CommonUtil.getImgPath("/wu-cont/images/bk_2_top.png", 1)%>" alt="go to top"/>
      <%}%>
    </a>
</p>
<%}%>
<%-- ::END:: Yogeswari :: 19.05.2015 :: this code for scroll-to-top --%>
<input type="hidden" id="smartBannerJs" name="smartBannerJs" value="jquery.smartbanner.js"/>

<script type="text/javascript">
 var $scroll = jQuery.noConflict();
  if(document.documentElement.clientWidth < 992){
  dynamicLoadJS(document.getElementById("contextJsPath").value+'/js/jquery/'+document.getElementById("smartBannerJs").value);
  setTimeout(function(){
 
  $scroll(function () {$scroll.smartbanner({});})
  }, 500);
  }
</script>
<%--Added by Indumathi.S for social box Nov-24-15--%>
<% if(isClearing == false && !GlobalConstants.CMMT_PAGE_NAMES.contains(pagename3) && !("moneyPageResults.jsp").equalsIgnoreCase(pagename3) && !("clearingProfile.jsp").equalsIgnoreCase(pagename3)&& !("contenthub.jsp").equalsIgnoreCase(pagename3) && !("browseMoneyPageResults.jsp").equalsIgnoreCase(pagename3) && !("clearingMoneyPageResults.jsp").equalsIgnoreCase(pagename3) && !"clearingBrowseMoneyPage.jsp".equalsIgnoreCase(pagename3) && (!"loggedIn.jsp".equalsIgnoreCase(pagename3)) && (!"retUser.jsp".equalsIgnoreCase(pagename3)) &&(!"newUser.jsp".equalsIgnoreCase(pagename3)) && (!"awards.jsp".equalsIgnoreCase(pagename3)) && (!"providerReviewResults.jsp".equalsIgnoreCase(pagename3)) && (!"advanceSearchResults.jsp".equalsIgnoreCase(pagename3))&& (!"provisionalOfferLandingPage.jsp".equalsIgnoreCase(pagename3)) && (!"offerSuccess.jsp".equalsIgnoreCase(pagename3)) && (!"whatuniGoLandingPage.jsp".equalsIgnoreCase(pagename3))&& (!"newEntryReqLandingPage.jsp".equalsIgnoreCase(pagename3)) && (!"allErrorPage.jsp".equalsIgnoreCase(pagename3)) && (!"clearingEbookLandingPage.jsp".equalsIgnoreCase(pagename3)) && (!"clearingEbookSuccess.jsp".equalsIgnoreCase(pagename3)  && (!"subjectLandingPage.jsp".equalsIgnoreCase(pagename3)) && (!"gradeFilterLandingPage.jsp".equalsIgnoreCase(pagename3))) && (!"applicationPage.jsp".equalsIgnoreCase(pagename3)) && (!"newUserGradeLandingPage.jsp".equalsIgnoreCase(pagename3)) && (!"clearingHome.jsp".equalsIgnoreCase(pagename3))) {%>
<%-- Added to hide chatbot popup in mobile view on 18-AUG-2020 by Hemalatha.K --%>
<c:set var="mobileFlag" value="<%=utilities.getMobileFlag(request) %>" />
<c:if test="${(mobileFlag and (pageName ne 'courseSearchResult.jsp' and pageName ne 'courseDetails.jsp' and pageName ne 'newProviderHome.jsp' and pageName ne 'richProfileLanding.jsp')) or not mobileFlag}">
  <c:choose>
    <c:when test="${fn:containsIgnoreCase(pageName, 'courseSearchResult.jsp')}"> 
      <link rel="preload" as="script" href="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=socialBoxJs%>">
    </c:when>
    <c:otherwise>
      <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=socialBoxJs%>"></script> 
    </c:otherwise>  
  </c:choose>
  <jsp:include page="/jsp/common/socialBox.jsp" />
</c:if>
<%}%>

<script type="text/javascript">
  var $ckie = jQuery.noConflict();
$ckie( document ).ready(function() {
  checkCookie();  
});
  
</script>
