<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
<%String keyword = (String)request.getAttribute("cDimKeyword");

String clearingUserType = (String)session.getAttribute("USER_TYPE") ;
   %>
<c:set var="clearingUserType" value ="<%=clearingUserType%>"/>
<script type="text/javascript">
var GTM_SR_JSON_ARRAY={};
var GTM_PR_JSON_ARRAY={};
<c:if test="${not empty listOfSearchResults}">
  GTM_SR_JSON_ARRAY = {	
	    <c:if test="${clearingUserType eq 'CLEARING'}">
	  	<c:set var="featuredBrandList" value="${featuredBrandListClearing}" />
	    </c:if>
    <c:if test="${not empty featuredBrandList}">
    "featured brand" :[
	   <c:forEach var="featuredBrandList" items="${featuredBrandList}">	
	     {
		   "id" : "${featuredBrandList.collegeId}",
		   "Name" : "${featuredBrandList.collegeName}",
		   "List" : "featured brand",
		   "Brand" : "${featuredBrandList.collegeName}",
		   "Category" : "${GTMregion}",
		   "Variant" : "${qualification1}",
		   "Position" : "${pageno}",
		   "Price" : "1",
		   "Dimension2" :"${featuredBrandList.collegeId}",
		   "Dimension3" :"${featuredBrandList.collegeName}",
		   "Dimension4" : "${qualification1}",
		   "Dimension5" : "${cDimLDCS}",
		   "Dimension6" : "${cDimJACS}",
		   "Dimension7" : "${cDimCPE2}",
		   "Dimension8" : "${GTMregion}",
		   "Dimension9" : "<%=keyword%>",
		   "Dimension10" : "${subjectL1}",
		   "Dimension11" : "${subjectL2}",
		   "Dimension13" : "Featured"
		 }
		</c:forEach>
	  ],
	</c:if>
		
		"SearchResults" :[
<c:forEach var="searchResult" items="${listOfSearchResults}" varStatus="i">
<c:set var="tilePos" value="${i.index}" > </c:set>
<c:set var="last" value="${listOfSearchResults.size()-1}"/>
<jsp:scriptlet>
  int tilePosition = 1;
  int pageNo = request.getAttribute("pageno") != null ? Integer.parseInt((String)request.getAttribute("pageno")) : 1;
  tilePosition = pageContext.getAttribute("tilePos") != null ? (int)pageContext.getAttribute("tilePos") : 0 ;
  tilePosition = ((pageNo-1) * 10) + tilePosition;
  tilePosition = tilePosition + 1;
</jsp:scriptlet>
{
	"id" : "${searchResult.collegeId}",
    "Name" : "${searchResult.collegeName}",
    "List" : "Search Results",
    "Brand" : "${searchResult.collegeName}",
    "Category" : "${GTMregion}",
    "Variant" : "${qualification1}",
    "Position" : "<%=tilePosition%>",
    "Price" : "1",
    "Dimension2" :"${searchResult.collegeId}",
    "Dimension3" :"${searchResult.collegeName}",
    "Dimension4" : "${qualification1}",
    "Dimension5" : "${cDimLDCS}",
    "Dimension6" : "${cDimJACS}",
    "Dimension7" : "${cDimCPE2}",
    "Dimension8" : "${GTMregion}",
    "Dimension9" : "<%=keyword%>",
    "Dimension10" : "${subjectL1}",

    <c:choose>
      <c:when test="${sponsoredInstId eq searchResult.collegeId || boostedInstId eq searchResult.collegeId || ((clearingUserType eq 'CLEARING') and not empty searchResult.matchTypeDim)}">
    	"Dimension11" : "${subjectL2}",
      </c:when>
      <c:otherwise>
        "Dimension11" : "${subjectL2}"
      </c:otherwise>
    </c:choose>

    <c:choose>
    <c:when test="${(sponsoredInstId eq searchResult.collegeId) and ((clearingUserType eq 'CLEARING') and not empty searchResult.matchTypeDim)}">
    "Dimension13" : "Sponsored",
   </c:when>
    <c:when test="${(sponsoredInstId eq searchResult.collegeId)}">
    "Dimension13" : "Sponsored"
   </c:when>
  </c:choose>
  
  <c:choose>
  <c:when test="${(boostedInstId eq searchResult.collegeId) and ((clearingUserType eq 'CLEARING') and not empty searchResult.matchTypeDim)}">
  "Dimension13" : "Manual",
 </c:when>
  <c:when test="${boostedInstId eq searchResult.collegeId}">
  "Dimension13" : "Manual"
 </c:when>
</c:choose>
     <c:if test="${(clearingUserType eq 'CLEARING') and not empty searchResult.matchTypeDim}">
      "Dimension14" : "${searchResult.matchTypeDim}"
    </c:if>
}<c:if test="${i.index ne last}">,</c:if>
</c:forEach>
]};
</c:if>
<c:if test="${not empty courseList}">
GTM_PR_JSON_ARRAY = {
		"ProviderResults" :[
  <c:forEach var="courseLists" items="${requestScope.courseList}" varStatus="loop">
  <c:set var="tilePos" value="${loop.index}" > </c:set>
  <c:set var="last" value="${courseList.size()-1}"/>
  <jsp:scriptlet>
  int tilePosition = 1;
  int pageNo = request.getAttribute("pageno") != null ? Integer.parseInt((String)request.getAttribute("pageno")) : 1;
  tilePosition = pageContext.getAttribute("tilePos") != null ? (int)pageContext.getAttribute("tilePos") : 0 ;
  tilePosition = ((pageNo-1) * 10) + tilePosition;
  tilePosition = tilePosition + 1;
</jsp:scriptlet>
{
	"id" : "${courseLists.courseId}",
    "Name" : "${courseLists.courseTitle}",
    "List" : "Provider Results",
    "Brand" : "${instNames}",
    "Category" : "${GTMregion}",
    "Variant" : "${paramStudyLevelId}",
    "Position" : "<%=tilePosition%>",
    "Price" : "1",
    "Dimension2" : "${courseLists.courseId}",
    "Dimension3" : "${courseLists.courseTitle}",
    "Dimension4" : "${paramStudyLevelId}",
    "Dimension5" : "${GTMLDCS}",
    "Dimension6" : "${cDimJACS}",
    "Dimension7" : "${cDimCPE2}",
    "Dimension8" : "${GTMregion}",
    "Dimension9" : "<%=keyword%>",
    "Dimension10" : "${subjectL1}",
    <c:choose>
    <c:when test="${(clearingUserType eq 'CLEARING') and not empty courseLists.matchTypeDim}">
  	"Dimension11" : "${subjectL2}",
    </c:when>
    <c:otherwise>
      "Dimension11" : "${subjectL2}"
    </c:otherwise>
  </c:choose>
  <c:if test="${(clearingUserType eq 'CLEARING') and not empty courseLists.matchTypeDim}">
    "Dimension14" : "${courseLists.matchTypeDim}"
    </c:if>
}<c:if test="${loop.index ne last}">,</c:if>
</c:forEach>
]};
</c:if>
</script>
</c:if>
       
       