<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="org.apache.commons.validator.GenericValidator"%>
<%@page import="WUI.utilities.CommonFunction"%>
<%@page import="WUI.utilities.GlobalFunction"%>
<%@page import="WUI.utilities.CommonUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%
String qualTxt = "";
String vowTex = "a";
String dispTxt = "";
String clearingUserOrNot = (String)session.getAttribute("USER_TYPE");
String userUcasPoint = (String)session.getAttribute("USER_UCAS_SCORE");
pageContext.setAttribute("userUcasPoint", userUcasPoint);
String requestURL = (String)request.getAttribute("REQUEST_URI");   
boolean isClearing = (requestURL != null) ? new GlobalFunction().isClearing(requestURL) : false;
if(isClearing){
  pageContext.setAttribute("clearingUserOrNot", "CLEARING");
}else{
  pageContext.setAttribute("clearingUserOrNot", "NON_CLEARING");
}
%>


<div class="rvbx_shdw"></div>
<div class="revcls">
	<a href="javascript:void(0);" onclick="closeLigBox();">
	  <img src="<%=CommonUtil.getImgPath("/wu-cont/images/lbx_clse_icon.svg",0)%>"
		alt="Close">
	</a>
</div>
<div class="rvbx_cnt srchbx_ui">
	<div class="rev_lst">
		<div class="rev_cen" data-id-ses="${sessionScope.USER_TYPE}" data-id="${clearingUserOrNot}" >
			
			<div class="srch_ui">
			  <c:if test="${clearingUserOrNot ne 'CLEARING'}">
				<ul class="tabs" id="srchUlId">
				  <c:if test="${not empty clearingOnOffSysvar and clearingOnOffSysvar eq 'ON'}">
				    <li class="tab-link current" data-tab="tab-4">Clearing</li>
				  </c:if>
					<li class="tab-link ${clearingOnOffSysvar ne 'ON' ? 'current' : ''}" data-tab="tab-1">Courses</li>
					<li class="tab-link" data-tab="tab-2">Universities</li>
					<li class="tab-link" data-tab="tab-3">Advice</li>
				</ul>
				<div class="srch_box fl_w100">
				   <c:if test="${not empty clearingOnOffSysvar and clearingOnOffSysvar eq 'ON'}">
					  <jsp:include page="/jsp/clearing/include/clearingTabInNormalHomePage.jsp">
					    <jsp:param value="CLEARING_TAB_IN_INYER_TOP_NAV" name="CLEARING_NAV"/>
					  </jsp:include> 
					</c:if>
					<div id="tab-1" class="crse_tab tab-content ${clearingOnOffSysvar ne 'ON' ? 'fl_w100 current' : ''}">
						<div class="crse_srch dis_inblk">
							<div class="tab_btm">
								<form id="topNavSearchForm" action="/home.html"
									method="post">
									<div class="sr_tab_form">
										<!-- Input start here -->
										<div class="land_inp_grp sub_inpt fl">
											<div class="sel_degcnr cr_srch fl" id="styLvlDivId">
												<div class="sel_deg fl_w100">
													<input type="text" name="qualification" id="navQualTP"
														class="sld_deg fnt_lbk" value="Undergraduate"
														readonly="readonly"														
														onclick="toggleQualListMob(null,'top_nav');">
												</div>
												<c:if test="${not empty requestScope.studyLevelQualList}">
													<ul id="topNavQualList" class="drop_deg"style="display:none;">
													  <c:forEach var="styLvl" items="${requestScope.studyLevelQualList}" varStatus="row">
													    <li class="fl" onclick="setQualTP('dpdwnkntp${row.count}','${styLvl.urlText}');" id="dpdwnkntp${row.count}">${styLvl.qualDesc}</li>
													  </c:forEach>													
													</ul>
													</c:if>
											</div>
										</div>
										<!-- Input End here -->
										<!-- Input Start here -->
										<div class="land_inp_grp sr_subcrse fl">
											<div class="sr_country_cnr" id="subSrchDivId">
												<label for="keywordTpNav" class="visualhid">Enter subject</label> 
												<input type="text" class="inptxt" name="keywordTpNav"
													id="keywordTpNav" placeholder="Enter subject"
													onkeyup="autoCompleteKeywordBrowseTP(event,this);"
													placeholder="Enter subject"													
													onkeydown="clearUniNAME(event,this);"
													autocomplete="off"
													onkeypress="javascript:if(event.keyCode==13){return navSearchSubmit('OFF', 'top_nav_popup');}" />

												<input type="hidden" id="keywordTpNav_hidden" value="" />
								                <input type="hidden" id="keywordTpNav_id" value="" />
								                <input type="hidden" id="keywordTpNav_display" value="" />
								                <input type="hidden" id="keywordTpNav_url" value="" />
								                <input type="hidden" id="keywordTpNav_alias" value="" />
								                <input type="hidden" id="keywordTpNav_location" value=""/>
								                <input type="hidden" id="keywordTpNav_qualification" value=""/>
								                <input type="hidden" id="isClearingSelectedTpNav" value="N"/>
												<input type="hidden" id="matchbrowsenodeTpNav" value="" />
												<input type="hidden" id="selQualTpNav" value=""/>
											</div>
										</div>
										<!-- Input Start here -->
										<!-- Input End here -->
										<div class="land_inp_grp sr_cnty fl" id="locSrchDivId">
											<div class="sr_country_cnr">
												<label for="locSrchId" class="visualhid">Select
													Location</label> <input type="text" class="inptxt"
													id="locSrchId"
													onclick="showHideLocDrpDn('locDrpDnId','show');"
													onfocus="showHideLocDrpDn('locDrpDnId','show');"													
													onkeyup="showHideLocDrpDn('locDrpDnId','hide',);"
													readonly="readonly"
													placeholder="Select Location (optional)">
											</div>
											<input type="hidden" id="locUrlTextTP" value=""/>       
											<!-- location List Start -->
											<div id="locDrpDnId" class="opsr_lst" style="display:none">													
												<c:if test="${not empty requestScope.locationList}">
													<ul id="regionUlist">															
														<c:forEach var="regionList"	items="${requestScope.locationList}" varStatus="row">
														  <c:set var="rowIntValue" value="${row.index}" />
															<li id="${regionList.regionId}" onclick="locSelectFn('${regionList.regionUrlText}','${regionList.regionName}');">																		
															  <span id="${regionList.regionUrlText}"><a href="javascript:void(0);">${regionList.regionName}</a></span>																		
															</li>
														</c:forEach>																
													</ul>	
												</c:if>													
											</div>
											<!-- location List End -->
										</div>
										<div class="err_field fl_w100" id="errMsgSubSrchlbox" style="display:none">
										  <p>Please enter subject</p>
										</div>										
									</div>
									<div class="btn_group fl">
										<button id="landingSearch" type="button"
											class="cug_btn_blue cug_btn_prim" onclick="navSearchSubmit('OFF', 'top_nav_popup');">
											<span class="srch"></span> Search
										</button>
									</div>									
									<input type="hidden" name="locKwdFlg" id="locKwdFlg" value=""/>
									<input type="hidden" name="locDrpdnFlg" id="locDrpdnFlg" value=""/> 
									<input type="hidden" name="subKwdFlg" id="subKwdFlg" value=""/>
								</form>
							</div>
							<div class="advsrc_lnk fl_w100">
								<a href="javascript:void(0);" onclick="openLightBox('interstitial');">Advanced search <i
									class="fa fa-long-arrow-right"></i></a>
							</div>
							<div class="key_tpics fl_w100">
								<div class="keytop_ui fl_w100">
								  <c:if test="${not empty requestScope.recentSearchList}">
									<div class="adv_ui fl">
										<h3>Recent searches</h3>
										<ul>
										  <c:forEach var="recentSrchList" items="${requestScope.recentSearchList}">
										     <c:set var="qualDesc" value="${recentSrchList.qualDesc}" />
										     <c:set var="subDesc" value="${recentSrchList.subjectName}" />
										     <%
										       dispTxt = "You searched for ";
										       qualTxt = "";
										       vowTex = "a "; 
										       qualTxt = (String)pageContext.getAttribute("qualDesc");
										       char vowel = qualTxt.toLowerCase().charAt(0);
										       if( (vowel == 'a')||(vowel == 'e')||(vowel == 'i')||(vowel == 'o')||(vowel == 'u')){
										    	   vowTex = "an ";
										        }
										       dispTxt +=  vowTex + qualTxt + " in " + (String)pageContext.getAttribute("subDesc");
										     %>
										     <c:if test="${not empty recentSrchList.regionName}">
										      <c:set var="locDesc" value="${recentSrchList.regionName}" />
										     <% dispTxt += " in " +(String)pageContext.getAttribute("locDesc");%>
										     </c:if>
											<li class="dsktop" id="course_tab"><a href="${recentSrchList.subjectUrl}" title="<%=dispTxt%>">${recentSrchList.subjectName}</a></li>
											<li class="mble"><a href="${recentSrchList.subjectUrl}" ><%=dispTxt %></a></li>
										  </c:forEach>
										</ul>
									</div>
								  </c:if>
									<div class="recsrch_ui fl">
										<h5 class="fl_w100">Not sure where to start?</h5>
										<div class="recnt_ui fl_w100">
											<div class="resea_col col1 fl">
												<div class="care_strt fl_w100">
													<div class="cst_lft fl">
														<img src="<%=CommonUtil.getImgPath("/wu-cont/images/career_start_icon.svg",0)%>"
															alt="<spring:message code="search.popup.what.career.text"/>">
													</div>
													<div class="cst_rgt fr">
														<h3><spring:message code="search.popup.what.career.text"/></h3>
														<p>Find out what to study to get there.</p>
														<a class="fl" href="javascript:void(0);" onclick="GANewAnalyticsEventsLogging('Top Nav Search','Widget','I want to be','topnav');openLightBox('iwanttobe');">Get started <i
															class="fa fa-long-arrow-right"></i></a>
													</div>
												</div>
											</div>
											<div class="resea_col col2 fl">
												<div class="care_strt fl_w100">
													<div class="cst_lft fl">
														<img src="<%=CommonUtil.getImgPath("/wu-cont/images/deg_study_icon.svg",0)%>"
															alt="<spring:message code="search.popup.what.study.text"/>">
													</div>
													<div class="cst_rgt fr">
														<h3><spring:message code="search.popup.what.study.text"/></h3>
														<p>Get suggestions based on your subjects and grades.</p>
														<a class="fl" href="javascript:void(0);" onclick="GANewAnalyticsEventsLogging('Top Nav Search','Widget','What can I do','topnav');openLightBox('whatcanido');">Get started <i
															class="fa fa-long-arrow-right"></i></a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="tab-2" class="univ_tab tab-content">
						<div class="crse_srch subsrse dis_inblk">
							<div class="tab_btm">
								<form id="uniSrchTpForm" action="/home.html"
									method="post" onsubmit="javascript:return redirectUniHOME(document.getElementById('uniSrchTpId'),'uniSrchTpForm','finduniTp');">
									<div class="sr_tab_form">
										<div class="land_inp_grp sr_cnty fl">
											<div class="sr_country_cnr" id="uniSrchDivId">
												<label for="uniSrchTpId" class="visualhid">Enter university name</label>
												<input type="text" class="inptxt" 
												  name="uniSrchTpId"
													id="uniSrchTpId"												
													onkeyup="autoCompleteUniNAME(event,this,'TOP_NAV_UNI');" 
													onkeydown="clearUniNAME(event,this);"
													onkeypress="javascript:if(event.keyCode==13){redirectUniHOME(this,'uniSrchTpForm','finduniTp');}"
													autocomplete="off"
													placeholder="Enter university name">													
												<input type="hidden" id="uniSrchTpId_hidden" value="">
												<input type="hidden" id="uniSrchTpId_id" value="">
												<input type="hidden" id="uniSrchTpId_display" value="">
												<input type="hidden" id="uniSrchTpId_url" value=""> 
												<input type="hidden" id="uniSrchTpId_alias" value="">
											</div>
										</div>
										<div class="err_field fl_w100" id="errMsgUniSrchlbox" style="display:none">
										  <p>Please select university from dropdown</p>
										</div>
									</div>
									<div class="btn_group fl" style="display: none;">
										<button id="landingSearch" type="button"
											class="cug_btn_blue cug_btn_prim"
											onclick="redirectUniHOME(document.getElementById('uniSrchTpId'),'uniSrchTpForm','finduniTp');">
											<span class="srch"></span> Search
										</button>
									</div>
									
								</form>
							</div>
							<spring:message code="find.uni.url" var="findUniUrl"/>
							<div class="advsrc_lnk fl_w100">
								<a href= "${findUniUrl}" onclick="GANewAnalyticsEventsLogging('University Search', 'View A-Z', 'Clicked', 'topnav');">
								  <spring:message code="view.unis.btn.name"/> <i class="fa fa-long-arrow-right"></i>
							    </a>
							</div>
						</div>

					</div>
					<div id="tab-3" class="adv_tab tab-content">
						<div class="crse_srch subsrse loc_srch dis_inblk">
							<div class="tab_btm">
							  <form id="advSrchFormTp" action="/home.html" method="post">
								<div class="sr_tab_form">
									<div class="land_inp_grp sr_cnty fl">
										<div class="sr_country_cnr" id="advSrchDivId">
											<label for="advSrchId" class="visualhid">Enter keyword</label> 
											<input type="text" name="keyword" class="inptxt"
												id="advSrchTpId" 
												onkeypress="javascript:if(event.keyCode==13){return adviceSearchSubmitTp();}"
												autocomplete="off"
												placeholder="Enter keyword">
											<input type="hidden" name="page" value="1" />
           						<input type="hidden" name="pageName" value="adviceSearch" />	
										</div>
									</div>
									<div class="err_field fl_w100" id="errMsgAdvSrchlbox" style="display:none">
									  <p>Please enter valid keyword</p>
						    		</div>
								</div>
							   
								<div class="btn_group fl">
									<button id="landingSearch" type="button"
										class="cug_btn_blue cug_btn_prim" onclick="adviceSearchSubmitTp();">
										<span class="srch"></span> Search
									</button>
								</div>								
							  </form>
							</div>
							<div class="key_tpics fl_w100">
								<div class="keytop_ui fl_w100">
								  <c:if test="${not empty requestScope.adviceKeyTopicList}">	
									<div class="adv_ui fl_w100">
										<h3>Key topics</h3>
										<ul>
										  <%int endDivCnt = 1;%>											
										  <c:forEach var="keyTopList" items="${requestScope.adviceKeyTopicList}" varStatus="loop">												
											<c:if test="${loop.index % 3 == 0}">
											  <li>
												<div class="adv_col fl_w100">
											</c:if>
												  <a href="${keyTopList.keyTopicUrl}" onclick="GANewAnalyticsEventsLogging('Advice Search', 'Category', '${keyTopList.keytopics}', 'topnav');">${keyTopList.keytopics}</a>														
											<c:if test="${loop.index % 3 == endDivCnt}">	
											    <% endDivCnt = endDivCnt + 1;%>
											    </div>
											  </li>
											</c:if>											
										  </c:forEach>
										</ul>
									</div>
								  </c:if>
									<div class="advsrc_lnk fl_w100">
										<a href="/advice/" onclick="GANewAnalyticsEventsLogging('Advice Search', 'Category', 'View All', 'topnav');">View all <i class="fa fa-long-arrow-right"></i></a>
									</div>
									</c:if> 
									<c:if test="${clearingUserOrNot eq 'CLEARING'}">
									 <jsp:include page="/jsp/clearing/include/clearingCommonHomeSearch.jsp">
									   <jsp:param value="CLEARING_JOURNEY_TOP_NAV" name="CLEARING_NAV"/>
									 </jsp:include>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
