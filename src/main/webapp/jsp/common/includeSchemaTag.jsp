<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.ArrayList,org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, java.time.format.DateTimeFormatter, java.time.LocalDateTime" autoFlush="true"%>
<% 
  String reviewCount = !GenericValidator.isBlankOrNull(request.getParameter("reviewCount")) ? request.getParameter("reviewCount") : "0";
 %>
<c:set var="pageName"><%=request.getParameter("pageName")%></c:set>
<c:choose>
 <c:when test="${(reviewCount ne 0) and (pageName eq 'REVIEW_SCHEMA')}">
<%
  String reviewRating = !GenericValidator.isBlankOrNull(request.getParameter("reviewRating")) ? request.getParameter("reviewRating") : "0";
  String collegeDisplayName = !GenericValidator.isBlankOrNull(request.getParameter("collegeDisplayName")) ? request.getParameter("collegeDisplayName") : "";
%>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "CollegeOrUniversity",
  "name": "<%=collegeDisplayName%>",
  "aggregateRating": {
    "@type": "AggregateRating",
   "ratingValue": "<%=reviewRating%>",
   "reviewCount": "<%=reviewCount%>"
 }
}
</script>
<%--End of profile review schema tag code--%>
<%--SR page breadcrumb schema tag --%>
</c:when>
 <c:when test="${pageName eq 'BREADCRUMB_SCHEMA'}">

  <c:set var="schemaListSize" value="${requestScope.schemaListSize}"/>
    <c:if test="${not empty requestScope.breadcrumbSchemaList}">
      <script type="application/ld+json">
        {
          "@context": "http://schema.org",
          "@type": "BreadcrumbList",
          "itemListElement": [
           <c:forEach var="breadcrumbSchemaLst" items="${requestScope.breadcrumbSchemaList}" varStatus="rowIndex">
             {
               "@type": "ListItem",
               "position": "${breadcrumbSchemaLst.itemId}",
               "item": {
                 "@id": "${breadcrumbSchemaLst.breadcrumbUrl}",
                 "name": "${breadcrumbSchemaLst.bradcrumbText}"
               }
             }
             <c:if test="${rowIndex.index+1 ne requestScope.schemaListSize}">
              ,
             </c:if>  
           </c:forEach>
          ]
        }
      </script>
    </c:if>
  </c:when>  
 <c:when test="${pageName eq 'WEB_PAGE_SCHEMA'}">
<%
  String catAvgRating = !GenericValidator.isBlankOrNull((String)request.getAttribute("categoryavgrating")) ? (String)request.getAttribute("categoryavgrating") : "0";
  if(!"0".equals(catAvgRating)){
    String catTotalReviews = !GenericValidator.isBlankOrNull((String)request.getAttribute("categorytotalreviews")) ? (String)request.getAttribute("categorytotalreviews") : "0";
    String catActualRating = !GenericValidator.isBlankOrNull((String)request.getAttribute("categoryActualRating")) ? (String)request.getAttribute("categoryActualRating") : "0";  
    String locationName = !GenericValidator.isBlankOrNull((String)request.getAttribute("SEARCH_LOCATION_OR_POSTCODE")) ? (String)request.getAttribute("SEARCH_LOCATION_OR_POSTCODE") : "";
    locationName = (!GenericValidator.isBlankOrNull(locationName) && !("United Kingdom").equalsIgnoreCase(locationName.trim())) ? locationName : "UK";
    String subjectDesc = request.getAttribute("subjectDesc") != null ? request.getAttribute("subjectDesc").toString() : ""; 
     String studeyLevel = request.getAttribute("studyLevelDesc") != null ? (String)request.getAttribute("studyLevelDesc") : ""; 
    if(!GenericValidator.isBlankOrNull(studeyLevel)){          
     if(studeyLevel.indexOf("degree")==-1){
        studeyLevel = studeyLevel.indexOf("hnd/hnc") > -1 ? studeyLevel.toUpperCase() :studeyLevel;
        studeyLevel = studeyLevel + " degrees";
     }else{
        studeyLevel = studeyLevel.replaceFirst("degree", "degrees");
        }              
      }
  }
  %><%-- Commented on 19_04_17 by Prabha
  <script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebPage",
   "mainEntity":{
          "@type": "Course",
          "name": "<%=subjectDesc%> <%=studeyLevel%>",
          "description": "<%=subjectDesc%> <%=studeyLevel%> <%=locationName%>"
          },
          "aggregateRating": {
            "@type": "AggregateRating",
            "ratingValue": "<%=catActualRating%>",
            "reviewCount": "<%=catTotalReviews%>"
          } }       
</script>
--%>
</c:when>
<c:when test="${(pageName eq 'UNI_RICH_PROFILE_SCHEMA') or (pageName eq 'UNI_PROFILE_SCHEMA')}">
<% //Added else if block to add schema tag for uni profile page on 16_May_2017, By Thiyagu G
  String collegeNameDisplay = !GenericValidator.isBlankOrNull((String)request.getAttribute("collegeNameDisplay")) ? (String)request.getAttribute("collegeNameDisplay") : "";    
  String reviewCounts = !GenericValidator.isBlankOrNull((String)request.getAttribute("reviewCount")) ? (String)request.getAttribute("reviewCount") : "0";
  String overallRating = !GenericValidator.isBlankOrNull((String)request.getAttribute("overallRating")) ? (String)request.getAttribute("overallRating") : "";
%>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "CollegeOrUniversity",
  "name": "${collegeNameDisplay}",
  "address": {
      <c:if test="${not empty requestScope.locationInfoList}">        
        <c:forEach var="venue" items="${requestScope.locationInfoList}" varStatus="rowIndex">
          "@type": "PostalAddress",
          "addressLocality": "${venue.town}",
          "addressRegion": "${venue.countryState}",
          "postalCode": "${venue.postcode}",
          "streetAddress": "${venue.addLine1} <c:if test="${not empty venue.addLine2}">, ${venue.addLine2}</c:if>"
          </c:forEach>
      </c:if>
  },
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": "<%=overallRating%>",
    "reviewCount": "<%=reviewCounts%>"
  }
} 
</script>
</c:when>
<c:when test="${(pageName eq 'RICH_PROFILE_OPENDAY_SCHEMA') or (pageName eq 'PROVIDER_RESULTS_OPENDAY_SCHEMA')}">
<% //Added else if block to add schema tag for uni profile page on 16_May_2017, By Thiyagu G  
  String domainPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
  DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
  LocalDateTime now = LocalDateTime.now();
  String currentDate = dtf.format(now); 
%>

  <script type="application/ld+json">
      <c:if test="${not empty requestScope.listOfOpendayInfo}">
        <c:set var="listSize"  value="${requestScope.listOfOpendayInfo}"/>
        <jsp:useBean id="listSize" type="java.util.List"/>
         <%int size =  ((ArrayList)pageContext.getAttribute("listSize")).size();%>
         <c:set var="size" value="<%=size%>"/>
        [
        <c:forEach var="openDaysListInfo" items="${requestScope.listOfOpendayInfo}" varStatus="rowIndex">
		  {
            "@context": "http://schema.org",
            "@type": "EducationEvent",
            <c:if test="${not empty openDaysListInfo.headline}">
            "name": "${openDaysListInfo.headline}",
			</c:if> 
            <c:if test="${empty openDaysListInfo.headline}">
			"name": "${openDaysListInfo.collegeNameDisplay} virtual open day",
            </c:if> 
            "description": "${openDaysListInfo.opendayDesc}", 
            "offers": {
              "@type": "Offer",
              "price": "0",
              "priceCurrency": "GBP"
            },
			<c:if test="${not empty openDaysListInfo.stOpenDayStartDate}">
            "startDate": "${openDaysListInfo.stOpenDayStartDate}",
			</c:if>
			<c:if test="${empty openDaysListInfo.stOpenDayStartDate}">
            "startDate": "<%=currentDate%>",
			</c:if>    
            "endDate" : "${openDaysListInfo.stOpenDayEndDate}",  
            "url": "<%=domainPath%>${openDaysListInfo.opendaysProviderURL}",
            "location": {
              "@type" : "Place",
              "name" : "${openDaysListInfo.collegeNameDisplay}",  
              "address": "${openDaysListInfo.opendayVenue}"  
            }
          }
            <c:if test="${rowIndex.index+1 ne size}">
              ,
             </c:if> 
        </c:forEach>
        ]
      </c:if>
  </script>
</c:when>
<c:when test="${pageName eq 'HOME_PAGE_SCHEMA'}">

  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "name": "Whatuni",
      "url": "https://www.whatuni.com",
      "contactPoint": [{"@type": "ContactPoint",
        "telephone": "+4420-7384-6000",
        "contactType": "customer support"
      }],
      "logo": " https://www.whatuni.com/wu-cont/images/logo_print.png",
      "sameAs": ["https://www.facebook.com/whatuni",
        "https://twitter.com/whatuni",
        "https://www.instagram.com/whatuni"
      ]
    }
  </script>
  <script data-schema="WebPage" type="application/ld+json">
    {"@id":"https://www.whatuni.com","potentialAction":{"@type":"ViewAction","target":"android-app://com.hotcourse.group.wuapp/https:www.whatuni.com"},"@type":"WebPage","@context":"http://schema.org"}
  </script>

</c:when>
<%--End of SR page breadcrumb schema tag--%>
<c:when test="${pageName eq 'ARTICLE_DETAIL_PAGE_SCHEMA'}">
<c:if test="${not empty adviceDetailInfoList}">
      <c:forEach var="adviceDetailInfo" items="${requestScope.adviceDetailInfoList}" varStatus="i">
  <script type='application/ld+json'>
	{
    "@context":"http://schema.org",
	"@type":"Article",
	"headline":"${adviceDetailInfo.postTitle}",
	"url":"${param.adviceDetailPageUrl}",	
    "thumbnailUrl":"${fn:replace(adviceDetailInfo.imageName, ".jpg", "_225px.jpg")}",
	"image":"${adviceDetailInfo.imageName}",	
	"dateCreated":"${adviceDetailInfo.createdDate}",
	"datePublished":"${adviceDetailInfo.createdDate}",
    <c:if test="${not empty adviceDetailInfo.updatedDate}">
	"dateModified":"${adviceDetailInfo.updatedDate}",
    </c:if>
    <c:if test="${empty adviceDetailInfo.updatedDate}">
	"dateModified":"${adviceDetailInfo.createdDate}",
    </c:if>
	"creator":{"@type":"Person","name":"${adviceDetailInfo.authorName}"},
	"author":{"@type":"Person","name":"${adviceDetailInfo.authorName}"},
	"publisher":{"@type":"Organization","name":"Whatuni",
	"logo":{"@type":"http://schema.org/ImageObject",
	"url":"${param.logoUrl}"}},
	"mainEntityOfPage":{"@type":"WebPage",
	"@id":"${param.adviceDetailPageUrl}"},
	"keywords":[""]
	}
  </script>
  </c:forEach>
  </c:if>
</c:when>
<c:when test="${pageName eq 'OPEN_DAY_PROVIDER_PAGE_SCHEMA'}">
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "BreadcrumbList",
"itemListElement": [
{
"@type": "ListItem",
"position": "1",
"item": {
"@id": "${param.od_home_url}",
"name": "${param.od_home_name}"
}
},
{
"@type": "ListItem",
"position": "2",
"item": {
"@id": "${param.od_prov_url}",
"name": "${param.od_prov_name}"
}
}
]
}
</script>
</c:when>
</c:choose>