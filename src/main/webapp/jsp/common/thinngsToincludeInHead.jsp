<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction"%>
<%
CommonFunction common = new CommonFunction();
String newGradeFilterCSS = CommonUtil.getResourceMessage("wuni.whatuni.grade.filter.css",null);
String gradeFilterUcasJs = CommonUtil.getResourceMessage("wuni.ucas.grader.filter.js", null);
String clearingonoff    =  common.getWUSysVarValue("CLEARING_ON_OFF");
pageContext.setAttribute("clearingonoff", clearingonoff);
%>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
</c:set>

<c:choose>
<c:when test="${pagename3 eq 'newUser.jsp' or pagename3 eq 'loggedIn.jsp'}">
<jsp:include page="/jsp/common/headjsps/homeheadjsp.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'mysettings.jsp' or pagename3 eq 'uploadedImage.jsp'}">
<jsp:include page="/jsp/common/headjsps/myprofilehead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'courseSearch.jsp'}">
<jsp:include page="/jsp/common/headjsps/findCourseHeader.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'clearingHome.jsp'}">
<jsp:include page="/jsp/common/headjsps/clearingHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'appLandingPage.jsp' or pagename3 eq 'appFaqPage.jsp' or  pagename3 eq 'appContactUsPage.jsp'}">
<jsp:include page="/jsp/common/headjsps/mobileAppHead.jsp"/>
</c:when> 
<c:when test="${pagename3 eq 'browseMoneyPageResults.jsp'}">
<jsp:include page="/jsp/common/headjsps/searchResultsHead.jsp"/>
</c:when>
<%-- <c:when test="${pagename3 eq 'clearingBrowseMoneyPages.jsp'}"> --%>
<%-- <jsp:include page="/jsp/common/headjsps/clearingSearchResultsHead.jsp"/> --%>
<%-- </c:when> --%>
<c:when test="${pagename3 eq 'overviewPage.jsp'}">
<jsp:include page="/jsp/common/headjsps/aboutUsHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'meetTheTeam.jsp'}">
<jsp:include page="/jsp/common/headjsps/meetTheTeamHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'teamMember.jsp'}">
<jsp:include page="/jsp/common/headjsps/teamMemberHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'partnershipPage.jsp'}">
<jsp:include page="/jsp/common/headjsps/partnershipHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'contactUsPage.jsp' or pagename3 eq 'contactUsSuccess.jsp'}">
<jsp:include page="/jsp/common/headjsps/contactUsHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'uniBrowser.jsp' or pagename3 eq 'uniFinderAZ.jsp'}">
<jsp:include page="/jsp/common/headjsps/findUniversityHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'newStudentAwards.jsp'}">
<jsp:include page="/jsp/common/headjsps/newStudentAwardsHeader.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'studentChoiceResult_2015.jsp'}">
<jsp:include page="/jsp/common/headjsps/studentChoiceResult_2015Header.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'studChoiceHome.jsp'}">
<jsp:include page="/jsp/common/headjsps/studChoiceHomeHeader.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'ucascalculator.jsp'}">
<jsp:include page="/jsp/common/headjsps/ucasCalculatorHead.jsp"/>
</c:when>
<%-- <c:when test="${pagename3 eq 'clearingCourseSearchResult.jsp'}"> --%>
<%-- <jsp:include page="/jsp/common/headjsps/clearingProviderResultsHead.jsp"/> --%>
<%-- </c:when> --%>
<c:when test="${pagename3 eq 'courseSearchResult.jsp'}">
<jsp:include page="/jsp/common/headjsps/providerResultsHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'faqs.jsp'}">
  <jsp:include page="/jsp/common/headjsps/faqsHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'adviceHome.jsp'}">
  <jsp:include page="/jsp/common/headjsps/adviceHomeHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'adviceSearchResults.jsp'}">
  <jsp:include page="/jsp/common/headjsps/adviceSearchResultsHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'primaryCategoryLanding.jsp'}">
  <jsp:include page="/jsp/common/headjsps/primaryCategoryLandingHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'secondaryCategoryLanding.jsp'}">
  <jsp:include page="/jsp/common/headjsps/secondaryCategoryLandingHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'adviceDetails.jsp'}">
  <jsp:include page="/jsp/common/headjsps/adviceDetailsHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'noPreviewAdvice.jsp'}">
  <jsp:include page="/jsp/common/headjsps/noPreviewAdviceHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'adviceDetailsAmpPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/adviceDetailsAmpPageHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'ampRegisterPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/ampRegisterPageHead.jsp"/>
</c:when>

<c:when test="${pagename3 eq 'reviewHome.jsp'}">
<jsp:include page="/jsp/common/headjsps/universityCourseReviewHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'iwanttobeSearch.jsp'}">
  <jsp:include page="/jsp/common/headjsps/IWantToBeSearchHead.jsp"/>
</c:when>

<c:when test="${pagename3 eq 'courseDetails.jsp'}">
  <jsp:include page="/jsp/common/headjsps/CourseDetailsHeader.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'openDaysLandingPage.jsp'}">
<jsp:include page="/jsp/common/headjsps/openDaysLandingPageHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'opendaySearchResults.jsp'}">
<jsp:include page="/jsp/common/headjsps/openDaysSearchPageHead.jsp"/>
</c:when>

<c:when test="${pagename3 eq 'whatcanidoSearch.jsp'}">
  <jsp:include page="/jsp/common/headjsps/WhatCanIDoWidgetHeader.jsp"/>
</c:when>

<c:when test="${pagename3 eq 'opendaysProviderLanding.jsp'}">
<jsp:include page="/jsp/common/headjsps/openDaysProviderPageHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'contenthub.jsp'}">
  <jsp:include page="/jsp/common/headjsps/contentHubHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'newProviderHome.jsp'}">
  <jsp:include page="/jsp/common/headjsps/newProviderHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'richProfileLanding.jsp'}">
  <jsp:include page="/jsp/common/headjsps/richProfileLandingHead.jsp"/>
</c:when>

<c:when test="${pagename3 eq 'addWidgetReview.jsp'}">
  <jsp:include page="/jsp/common/headjsps/AddWidgetReviewHead.jsp"/>
</c:when>

<c:when test="${pagename3 eq 'interstitialSearch.jsp'}">
  <jsp:include page="/jsp/common/headjsps/interstitialSearchHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'chatbot.jsp'}">
<jsp:include page="/jsp/common/headjsps/chatbotHead.jsp"/>
</c:when>

<c:when test="${pagename3 eq 'ourBirthdayPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/ourBirthdayPageHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'visitWebSuccess.jsp'}">
<jsp:include page="/jsp/common/headjsps/visitWebsiteHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'qlBasicForm.jsp'}">
  <jsp:include page="/jsp/common/headjsps/enquiryDpHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'enquiryLimitMessage.jsp' or pagename3 eq 'collegeEmailSuccess.jsp' or pagename3 eq 'collegeProspectusSuccess.jsp' or pagename3 eq 'downloadProspectusSuccess.jsp'}">
<jsp:include page="/jsp/common/headjsps/enquiryLimitMessageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'loginPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/userLoginHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'forgotPassword.jsp'}">
  <jsp:include page="/jsp/common/headjsps/forgotPasswordHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'ProspectusLocation.jsp'}">
  <jsp:include page="/jsp/common/headjsps/ProspectusLocationHeader.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'myProspectusLocation.jsp'}">
  <jsp:include page="/jsp/common/headjsps/BulkProspectusHeader.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'prospectusErrorMessage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/prospectusErrorMessageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'prospectusRedirection.jsp'}">
  <jsp:include page="/jsp/common/headjsps/prospectusRedirectionHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'messagePage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/messagePageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'prospectusSentSuccess.jsp'}">
  <jsp:include page="/jsp/common/headjsps/prospectusSentSuccessHead.jsp"></jsp:include>
</c:when>

<c:when test="${pagename3 eq 'compareBasket.jsp'}">
  <jsp:include page="/jsp/common/headjsps/compareBasketHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'resetPassword.jsp'}">
  <jsp:include page="/jsp/common/headjsps/resetPasswordHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'nofound.jsp'}">
<jsp:include page="/jsp/common/headjsps/notFoundHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'cookies.jsp'}">
  <jsp:include page="/jsp/common/headjsps/cookiesHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'clearingEbookLandingPage.jsp' or pagename3 eq 'clearingEbookSuccess.jsp'}">
  <jsp:include page="/jsp/common/headjsps/clearingEbookLandingPageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'subjectLandingPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/subjectLandingHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'newEntryReqLandingPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/newEntryReqLandingPageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'newUserGradeLandingPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/newUserGradeLandingPageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'gradeFilterLandingPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/gradeFilterLandingPageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'whatuniGoSignUp.jsp' or pagename3 eq 'whatuniGoSignIn.jsp'}">
  <jsp:include page="/jsp/common/headjsps/whatuniGoSignUpHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'whatuniGoLandingPage.jsp' or pagename3 eq 'provisionalOfferLandingPage.jsp' or pagename3 eq 'allErrorPage.jsp' or pagename3 eq 'applicationPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/whatuniGoLandingPageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'whatuniGoProductLandingPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/whatuniGoProductLandingPageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'offerSuccess.jsp'}">
  <jsp:include page="/jsp/common/headjsps/offerSuccessHead.jsp"></jsp:include>
</c:when>

<c:when test="${pagename3 eq 'rssArticleFeed.jsp'}">
  <jsp:include page="/jsp/common/headjsps/rssArticleFeedHead.jsp"></jsp:include>
</c:when>

<c:when test="${pagename3 eq 'gradeFilterMain.jsp'}">
  <jsp:include page="/jsp/common/headjsps/gradeFilterMainHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'newUserGradeFilterMain.jsp'}">
  <jsp:include page="/jsp/common/headjsps/newUserGradeFilterMainHead.jsp"></jsp:include>
</c:when>

<c:when test="${pagename3 eq 'spellingSuggestions.jsp'}">
  <jsp:include page="/jsp/common/headjsps/spellingSuggestionsHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'multipleCPCLandingPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/multipleCPCLandingPageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'singleCPCLandingPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/singleCPCLandingPageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'openDayProviderLandingPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/openDayProviderLandingPageHead.jsp"></jsp:include>
</c:when>
<c:when test="${pagename3 eq 'clearingProfile.jsp'}">
  <jsp:include page="/jsp/common/headjsps/clearingProfileHead.jsp"/>
</c:when>

<c:when test="${pagename3 eq 'clearingBrowseMoneyPage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/searchResultsHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'clearingCourseSearchResult.jsp'}">
  <jsp:include page="/jsp/common/headjsps/clearingProviderResultsHead.jsp"/>
</c:when>
<c:when test="${pagename3 eq 'unSubscribePage.jsp'}">
  <jsp:include page="/jsp/common/headjsps/unSubscribePageHead.jsp"/>
</c:when>
</c:choose>
<c:if test="${clearingonoff eq 'ON' and (pagename3 ne 'multipleCPCLandingPage.jsp' and pagename3 ne 'singleCPCLandingPage.jsp' and 'true' ne param.amp)}">
<link rel="stylesheet" type="text/css" id="gradeFilterCssId" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=newGradeFilterCSS%>" media="screen">
<c:if test = "${pagename3 ne 'clearingBrowseMoneyPage.jsp' and pagename3 ne 'clearingCourseSearchResult.jsp'}">
<script type="text/javascript" language="javascript" id="slimScrollJsId" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery.slimscroll.min.js"></script>
<c:if test="${pagename3 eq 'courseSearchResult.jsp'}">
<script type="text/javascript" language="javascript" id="gradeFilterUcasJsId" src="<%=CommonUtil.getJsPath()%>/js/clearing/<%=gradeFilterUcasJs%>"></script>
</c:if>
</c:if>
</c:if>