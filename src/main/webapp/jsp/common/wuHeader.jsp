
<%@page import="WUI.utilities.GlobalFunction, WUI.utilities.CookieManager, com.wuni.util.seo.SeoUrls, WUI.utilities.CommonUtil,WUI.utilities.CommonFunction,WUI.utilities.SessionData, WUI.utilities.GlobalConstants,org.apache.commons.validator.GenericValidator, spring.util.GlobalMethods, com.layer.util.SpringConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
<%    
  String activeMenu       =   "";
  String classNameHtml5   =   "";
  //String activecls        =   "class=nactv";
  String activecls        =   "class=nav_act";
  String pagename3        =   "";
  String switchPages = "browseMoneyPageResults.jsp,courseSearchResult.jsp,clearingBrowseMoneyPage.jsp,clearingCourseSearchResult.jsp,clearingSearchResults.jsp,clearingProviderResults.jsp,courseDetails.jsp,contenthub.jsp,newProviderHome.jsp,richProfileLanding.jsp,clearingProfile.jsp";
  if(!GenericValidator.isBlankOrNull(pageContext.getAttribute("pagename3").toString())){
	  pagename3 = (String)pageContext.getAttribute("pagename3");
  }else{
	  pagename3 = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1,request.getRequestURI().length());
  }
  request.setAttribute("banPageName", pagename3);
  CommonFunction common = new CommonFunction();
  String clearingonoff    =  common.getWUSysVarValue("CLEARING_ON_OFF");   
  String wuLogoClass      =  common.getWUSysVarValue("WU_HOME_PAGE_LOGO");   
  String currYear         =  common.getWUSysVarValue("CURRENT_YEAR_AWARDS");  
  String preClearingOnOff =  common.getWUSysVarValue("PRE_CLEARING_ON_OFF"); 
  String postClearingOnOff =  common.getWUSysVarValue("POST_CLEARING_ON_OFF"); 
  String awardsURL        =  "/student-awards-winners/university-of-the-year/"; //Removed 2015 URL condition check for 21_Mar_2017, By Thiyagu G.
  String enableAwardsLink =   new CommonFunction().getWUSysVarValue("ENABLE_STUDENT_AWARD_LINKS");  
  String basketCount      =  (String)request.getSession().getAttribute("basketpodcollegecount");
  String html5PageNames   =  "newUser.jsp, retUser.jsp, loggedIn.jsp, schoolVisitsPage.jsp, schoolVisitsSuccess.jsp, orderDownloadPage.jsp, orderDownloadSuccess.jsp";        
  String lname            =  request.getServerName();
  String user_Id          =  new SessionData().getData(request,"y");  
  String hideHeader       =  "";  
  String fnChNavCnt       = "0";
  String hideNavSrchHeaderPages = GlobalConstants.HIDE_NAV_SRCH_PAGES + "provisionalOfferLandingPage.jsp" + "gradeFilterLandingPage.jsp" + "newUserGradeLandingPage.jsp";
  String hideHeaderPages  = GlobalConstants.HIDE_HEADER_PAGES + "provisionalOfferLandingPage.jsp" + "gradeFilterLandingPage.jsp" + "newUserGradeLandingPage.jsp";
  String hideTimeLinePages = GlobalConstants.HIDE_TIMELINE_PAGES + "provisionalOfferLandingPage.jsp" + "gradeFilterLandingPage.jsp" + "newUserGradeLandingPage.jsp";
  String hideMobileSrchPages = GlobalConstants.HIDE_MOBILE_SRCH_PAGES + "provisionalOfferLandingPage.jsp" + "gradeFilterLandingPage.jsp" + "newUserGradeLandingPage.jsp";
  String clearingUserType = (String)session.getAttribute("USER_TYPE") ;
  
  if(session.getAttribute("userInfoList") == null){
  new WUI.utilities.CommonUtil().autoLogin(request, response);
  }
  if(hideHeaderPages.contains(pagename3) || "qlBasicForm.jsp".equalsIgnoreCase(pagename3) || "whatuniGoLandingPage.jsp".equalsIgnoreCase(pagename3) || "offerSuccess.jsp".equalsIgnoreCase(pagename3) || "subjectLandingPage.jsp".equalsIgnoreCase(pagename3) || "gradeFilterLandingPage.jsp".equalsIgnoreCase(pagename3) || "applicationPage.jsp".equalsIgnoreCase(pagename3) || "newUserGradeLandingPage.jsp".equalsIgnoreCase(pagename3)){
    hideHeader = "display:none;";    
    request.setAttribute("hidMenu","Y");
  }else{
    request.setAttribute("hidMenu","N");
  }
  if(clearingonoff!=null && !"".equals(clearingonoff)) {
    request.setAttribute("clearingSwitchFlag",clearingonoff);
  }
  if(request.getAttribute("curActiveMenu")!=null && !"".equals(request.getAttribute("curActiveMenu"))) {
    activeMenu = (String)request.getAttribute("curActiveMenu");
  }
  if(html5PageNames.contains(pagename3)) {
    classNameHtml5  = "hl5";
  }
  if(basketCount == null || "0".equalsIgnoreCase(basketCount)) {
    new CommonFunction().getBasketPodContent(request, response);
    basketCount = (String)request.getSession().getAttribute("basketpodcollegecount");
  }
  String link = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();
  String activeClClass        =   "class=nactv clr15_hmenu";
  String clearingPopUpFlag = new CommonFunction().getWUSysVarValue("CLEARING_POP_UP_FLAG");
  String headerClass = "hrdr";
  String myFinalChoiceJs = CommonUtil.getResourceMessage("wuni.my.final.choice.js", null);
  String clearingHomeJs = CommonUtil.getResourceMessage("wuni.common.clearing.home.js", null);
  boolean clUserFlag = false;
  String HIDE_LOGIN = (String)request.getAttribute("HIDE_LOGIN");
  String navClass = "navbar ui-link";
  if("Y".equalsIgnoreCase(HIDE_LOGIN)){
    navClass = "navbar_dis";
  }
  String wugoApplyNowBtnFlag = common.getWUSysVarValue("WUGO_APPLY_NOW_BUTTON_FLAG");
  new GlobalMethods().setCovid19SessionData(request, response);
  String COVID19_SYSVAR = new SessionData().getData(request, GlobalConstants.COVID19_SYSVAR);
  request.setAttribute("COVID19_SYSVAR", COVID19_SYSVAR);
  pageContext.setAttribute("PRE_CLEARING_URL", SpringConstants.PRE_CLEARING_LANDING_URL);
  pageContext.setAttribute("PRE_CLEARING_SYSVAR", preClearingOnOff);
  pageContext.setAttribute("CLEARING_ON_OFF_SYSVAR", clearingonoff);
  pageContext.setAttribute("PAGE_NAME", pagename3);
  pageContext.setAttribute("getJsDomainValue", CommonUtil.getJsPath());
  pageContext.setAttribute("clearingHomeJsName", clearingHomeJs);
  pageContext.setAttribute("clearingonoff", clearingonoff);
%>
<%-- ::START:: Yogeswari :: 09.06.2015 :: for clearing splash screen --%>
<c:if test="${requestScope.clearingSwitchFlag eq  'ON'}">
<%-- <%if("ON".equals(clearingPopUpFlag)){%> --%>
<%
    String requestURL = (String)request.getAttribute("REQUEST_URI");   
    boolean isClearing = (requestURL != null) ? new GlobalFunction().isClearing(requestURL) : false;
    String clearingSession = (String)session.getAttribute("USER_TYPE");
    //if (clearingSession == null || "".equals(clearingSession)) {  //commented for setting user type only based on accessing url by sangeeth.s in June_23_20 rel
      String clearingCookie = new CookieManager().getCookieValue(request,GlobalConstants.CLEARING_USER_TYPE_COOKIE);
      //if (clearingCookie != null && clearingCookie.trim().length() > 0) {
        if (isClearing) {
          session.setAttribute("USER_TYPE", GlobalConstants.CLEARING_USER_TYPE_VAL);
          Cookie cookiee = new CookieManager().createCookie(request, GlobalConstants.CLEARING_USER_TYPE_COOKIE, GlobalConstants.CLEARING_USER_TYPE_VAL);
          response.addCookie(cookiee);
        }else {
          session.setAttribute("USER_TYPE", GlobalConstants.NON_CLEARING_USER_TYPE_VAL);
          Cookie cookiee = new CookieManager().createCookie(request, GlobalConstants.CLEARING_USER_TYPE_COOKIE, GlobalConstants.NON_CLEARING_USER_TYPE_VAL);
          response.addCookie(cookiee);
        }
      /* } else {
        if(isClearing) {         
          session.setAttribute("USER_TYPE", GlobalConstants.CLEARING_USER_TYPE_VAL);
          Cookie cookiee = new CookieManager().createCookie(request, GlobalConstants.CLEARING_USER_TYPE_COOKIE, GlobalConstants.CLEARING_USER_TYPE_VAL);
        }        
      } */
    //}
    clearingUserType = (String)session.getAttribute("USER_TYPE") ;
    //if("CLEARING".equals(clearingUserType)){headerClass = headerClass + " clr15";}
    if("CLEARING".equalsIgnoreCase(clearingUserType)){
	      headerClass = headerClass + " leav_clr";
	      clUserFlag = true;
	    }else{
	      headerClass = headerClass + " join_clr";
	      clUserFlag = false;
	    }
%>
<%-- <%}else{
   headerClass = headerClass + " join_clr";
}%> --%>
</c:if>

<%-- GTM script included under body tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
	<jsp:param name="PLACE_TO_INCLUDE" value="BODY"/>
</jsp:include>
<%-- GTM script included under body tag --%>
<spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
<spring:message code="review.form.url" var="reviewFormUrl"/>
<c:if test="${not empty sessionScope.userInfoList}">
  <c:forEach  var="mychUserInfo" items="${sessionScope.userInfoList}">
       <c:if test="${not empty mychUserInfo.mychNavCnt}">
        <c:set var="fnChCnt" value="${mychUserInfo.mychNavCnt}"></c:set>
        <%String fnChCnt = pageContext.getAttribute("fnChCnt").toString();  %>
        <%fnChNavCnt=fnChCnt;%> 
    </c:if>
</c:forEach>

  

</c:if>    
<div class="hdnav_ui fl_w100">
<%-- ::END:: Yogeswari :: 09.06.2015 :: for clearing splash screen --%>
<div id="header" class="<%=headerClass%>">   
 <jsp:include page="/jsp/home/cookiePopup.jsp"/><%--Added header cookie policy pop-up by Hema.S on 31.07.2018_rel--%>
 <%-- Handled condition to hide the hamburger for pre clearing pages in mobile view by Sujitha V o 02_JUNE_2020 --%>
 <c:if test="${!fn:containsIgnoreCase(PAGE_NAME, 'clearingebooklandingpage.jsp') and !fn:containsIgnoreCase(PAGE_NAME, 'clearingebooksuccess.jsp')}"> 
   <a title="Show navigation" class="<%=navClass%>" id="navbar" ><i class="fa fa-bars"></i><span class="mnu_clse"></span></a>
 </c:if> 
  <c:if test="${not empty requestScope.clearingSwitchFlag}">     
    <c:if test="${requestScope.clearingSwitchFlag eq 'ON'}">
      <%-- <div class="un_logo clr15 <%=classNameHtml5%>"><a href="<%=link%>" title="Whatuni home"></a></div> --%>
      <div class="un_logo <%=classNameHtml5%>"><a href="<%=link%>" title="Whatuni home"></a></div>
    </c:if>    
    <c:if test="${requestScope.clearingSwitchFlag eq 'OFF'}">
      <c:if test="${not empty requestScope.mywhatunipage}">
        <c:if test="${requestScope.mywhatunipage eq 'mywhatunipage'}">
          <div class="logo-my"><a href="<%=link%>" title="Whatuni home"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/mywt-logo.png", 0)%>" border="0" alt="Whatuni home"></a></div>  
        </c:if>
      </c:if>
      <c:if test="${empty requestScope.mywhatunipage}">
        <%if(pagename3!=null && ("home.jsp").equals(pagename3)){%>
          <div class="<%=wuLogoClass%>" itemscope itemtype="http://schema.org/Organization">  <a itemprop="url" href="<%=link%>" title="Whatuni"><img alt="Whatuni Search" itemprop="logo" src="<%=CommonUtil.getImgPath("/wu-cont/images/what_lgo.png",0)%>"/></a></div>
        <%}else{%>
          <div class="<%=wuLogoClass%>"><a href="<%=link%>" title="Whatuni"></a></div>
        <%}%>  
      </c:if>
    </c:if>    
  </c:if>
  <input type="hidden" name="loginFrom" id="loginFrom" value="N"/>
  <input type="hidden" name="openDayProviderName" id="openDayProviderName" value=""/>       
  <input type="hidden" id="hide_Menu" value="<%=request.getAttribute("hidMenu")%>"/>
  <%if( !hideNavSrchHeaderPages.contains(pagename3) && !"qlBasicForm.jsp".equalsIgnoreCase(pagename3) && !"qlAdvanceForm.jsp".equalsIgnoreCase(pagename3) && !"offerSuccess.jsp".equalsIgnoreCase(pagename3)  && !"whatuniGoLandingPage.jsp".equalsIgnoreCase(pagename3) && !"subjectLandingPage.jsp".equalsIgnoreCase(pagename3) && !"gradeFilterLandingPage.jsp".equalsIgnoreCase(pagename3) && !"applicationPage.jsp".equalsIgnoreCase(pagename3) && !"newUserGradeLandingPage.jsp".equalsIgnoreCase(pagename3) && !"clearingebooklandingpage.jsp".equalsIgnoreCase(pagename3) && !"clearingebooksuccess.jsp".equalsIgnoreCase(pagename3)){%>
    <jsp:include page="/jsp/common/wuNavSrchHeader.jsp"/>
  <%}%>              
  <%if( hideNavSrchHeaderPages.contains(pagename3) || "qlBasicForm.jsp".equalsIgnoreCase(pagename3) || "qlAdvanceForm.jsp".equalsIgnoreCase(pagename3) || "offerSuccess.jsp".equalsIgnoreCase(pagename3) || "whatuniGoLandingPage.jsp".equalsIgnoreCase(pagename3) || "subjectLandingPage.jsp".equalsIgnoreCase(pagename3) || "applicationPage.jsp".equalsIgnoreCase(pagename3) || "clearingebooklandingpage.jsp".equalsIgnoreCase(pagename3) || "clearingebooksuccess.jsp".equalsIgnoreCase(pagename3)){%>
    <div class="header-right mt40 enq_profile"> 
      <jsp:include page="/jsp/common/loginDropDown.jsp">
        <jsp:param name="collegeCount" value="<%=basketCount%>"/>
        <jsp:param name="userId" value="<%=user_Id%>"/>
      </jsp:include>
    </div>
  <%}%>
</div>
<div id="hdr_menu" class="navi_mnu fl_w100">
<%if(!"qlAdvanceForm.jsp".equalsIgnoreCase(pagename3) && !"clearingebooklandingpage.jsp".equalsIgnoreCase(pagename3) && !"clearingebooksuccess.jsp".equalsIgnoreCase(pagename3)){%>  
    <!--Menu list-->    
    <nav class="hdr_menu mycmp_menu" style="<%=hideHeader%>">    
      <ul>
        <%-- ::START:: Yogeswari :: 09.06.2015 :: for clearing splash screen --%>
        <%String tabId = "clearDivId";%>
        <%if("ON".equalsIgnoreCase(clearingonoff)){
          String clMenu = "CLEARING_"+GlobalConstants.CLEARING_YEAR;
          String clURL = "/degrees/university-clearing-"+GlobalConstants.CLEARING_YEAR+".html";
        %>

          <%-- changed clearing menu name in nav for clearing by Kailash L on 23_JUNE_2020 --%>     
          <li <%=(!"".equals(activeMenu) && clMenu.equals(activeMenu))? activecls :""%>><a id="<%=tabId%>" oncontextmenu="updateUserTypeSession('','clearing');" onclick="javascript:updateUserTypeSession('','clearing');" class="fnt_lbd" href="<%=clURL%>"><spring:message code="clearing.2020.label"/></a></li>
        <%tabId = "findDivId"; }%>
        <%-- ::END:: Yogeswari :: 09.06.2015 :: for nav bar changes. --%>
        <%-- Added pre clearing sysvar by Sujitha V on 02_JUNE_2020 --%>
        <c:if test="${CLEARING_ON_OFF_SYSVAR eq 'OFF'}">
         <c:if test="${PRE_CLEARING_SYSVAR eq 'ON'}">
          <li><a class="fnt_lbd" href="${PRE_CLEARING_URL}"><spring:message code="clearing.2020.label"/></a></li>
         </c:if>
        </c:if>
        
        <%-- Removed clearing condition to display all nav menu for clearing by Kailash L on 23_JUNE_2020 --%>
        <li <%=(!"".equals(activeMenu) && "findcrs".equals(activeMenu))? activecls :""%>><a id="<%=tabId%>" class="fnt_lbd" onclick="" href="<%=request.getContextPath()%>/courses/">Find a course </a>
        </li>
        <li <%=(!"".equals(activeMenu) && "finduni".equals(activeMenu))? activecls :""%>><a class="fnt_lbd" href="<%=new SeoUrls().getInstitutionFinderURL()%>" onclick="">Find a uni </a></li>
        <li <%=(!"".equals(activeMenu) && "prospectus".equals(activeMenu))? activecls :""%>><a class="fnt_lbd" href="<%=request.getContextPath()%>/prospectus/">Prospectuses</a></li>
        <li <%=(!"".equals(activeMenu) && "opendays".equals(activeMenu))? activecls :""%>><a class="fnt_lbd" href="/open-days/"><spring:message code="virtual.tour.top.nav"/></a></li>     
        
        <%--Added subNavDkLink classname for all the submenu for tab chnages Jan-24-2017--%>
        <li id="reviewTabChg" <%=(!"".equals(activeMenu) && "reviews".equals(activeMenu))? activecls :""%> >
        <a id="dsk_reviews" class="fst fnt_lbd subNavDkLink" href="/university-course-reviews/">Reviews<i class="fa fa-plus-circle fa-1"></i></a>
        <a id="click_view1" class="fst fnt_lbd noalert subNavDkLink" onclick="" style="display:none;">Reviews<span id="click_view1_span" class="mnuicn"></span></a>                       
          <div class="chose_inr urws" id="dsk_rev_submenu">
            <ul class="mnite_cnt2">              
              <li><a class="subNavDkLink" href="/university-course-reviews/">Read reviews</a></li>              
              <li><a class="subNavDkLink" href="${reviewFormUrl}">Write a review</a></li>
              <%if("ON".equalsIgnoreCase(enableAwardsLink)){%><li><a class="subNavDkLink" href="/awards">Student Choice Awards</a></li><%}%>  
              <li><a class="subNavDkLink" href="<%=awardsURL%>">WUSCA winners <%=currYear%></a></li><!--Added by Indumathi.S Mar-29-16 URL restructuring for current year-->
            </ul>
          </div>
        </li>        
        <li id="adviceTabChg" <%=(!"".equals(activeMenu) && "advice".equals(activeMenu))? activecls :""%>><a class="fnt_lbd desk_adv_act" id="deskNavTop" onclick="" href="/advice/">Advice</a>
          <a id="click_view2" class="fnt_lbd mob_adv_act noalert subNavDkLink">Advice<span id="click_view2_span" class="mnuicn"></span></a>
          <div class="chose_inr urws" id="dsk_adv_submenu">
            <ul class="mnite_cnt2">              
              <%-- Clearing menu showing on Top by Kailash L on 21_JULY_2020 --%> 
              <li><a id="clearAdv" class="subNavDkLink" href="/advice/clearing/" title="Clearing">Clearing</a></li>
              <li><a class="subNavDkLink" href="${teacherSectionUrl}" title="Covid-19 Updates"><spring:message code="wuni.covid.label"/></a></li> <%-- Added Teacher category on 04_Mar_2020, By Sri Sankari R --%>
              <li class="mob_adv_act subNavDkLink" ><a href="/advice/" title="Advice">Advice Home</a></li>
              <li><a href="/advice/research-and-prep/" class="subNavDkLink" title="Research &amp; Prep">Research &amp; Prep</a></li>
              <li><a href="/advice/accommodation/" class="subNavDkLink" title="Accommodation">Accommodation</a></li>
              <li><a href="/advice/student-life/" class="subNavDkLink" title="Student Life">Student Life</a></li>
              <li><a href="/advice/blog/" class="subNavDkLink" title="Blog">Blog</a></li>
              <li><a href="/advice/news/" class="subNavDkLink" title="News">News</a></li>
              <li><a href="/advice/guides/" class="subNavDkLink" title="Guides">Guides</a></li>
              <li><a class="subNavDkLink" href="/advice/parents/" title="Parents">Parents</a></li>             
            </ul>
          </div>
        </li>      
        <%if("ON".equalsIgnoreCase(clearingonoff) && "ON".equalsIgnoreCase(wugoApplyNowBtnFlag)){%>  
        <%if("ON".equalsIgnoreCase(clearingonoff)){%>
        <li><a class="fnt_lbd" href="/degrees/whatuni-go-product-page.html" target="_blank">Whatuni GO</a></li> 
        <%}else{%>
        <li><a class="fnt_lbd" href="/whatuni-mobile-app" onclick="alpGaLogging('TOP_NAV');" target="_blank">App</a></li>
        <%}%>
         <%if("ON".equalsIgnoreCase(clearingonoff) && clUserFlag){%>
        <li><a class="fnt_lbd" onclick="checkApplicationPageStatus();">Applications</a></li> 
        <%}
        }%>        
      </ul>      
    </nav>    
  <%}%>    
</div>
<jsp:include page="/jsp/clearing/clearingSwitch.jsp">
  <jsp:param name="podType" value="covid_dsk"/>  
</jsp:include>

<%if( !hideNavSrchHeaderPages.contains(pagename3) && !(switchPages.indexOf(pagename3) > -1 && ("ON".equalsIgnoreCase(clearingonoff) || "ON".equalsIgnoreCase(postClearingOnOff))) && !"qlBasicForm.jsp".equalsIgnoreCase(pagename3) && !"qlAdvanceForm.jsp".equalsIgnoreCase(pagename3) && !"offerSuccess.jsp".equalsIgnoreCase(pagename3)  && !"whatuniGoLandingPage.jsp".equalsIgnoreCase(pagename3) && !"subjectLandingPage.jsp".equalsIgnoreCase(pagename3) && !"gradeFilterLandingPage.jsp".equalsIgnoreCase(pagename3) && !"applicationPage.jsp".equalsIgnoreCase(pagename3) && !"newUserGradeLandingPage.jsp".equalsIgnoreCase(pagename3) && !"clearingebooklandingpage.jsp".equalsIgnoreCase(pagename3) && !"clearingebooksuccess.jsp".equalsIgnoreCase(pagename3)){%>
<c:choose>
  <c:when test="${banPageName eq 'openDaysLandingPage.jsp'}">
  <c:if test="${COVID19_SYSVAR eq 'ON' and parentCategory ne 'coronavirus'}">
  <div class="covid_ltms covid_dsk">
    <div class="covid_txcnt">
    <span class="covid_txt"><spring:message code="wuni.covid.label"/></span>
	<a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_wht_arw.svg", 0)%>"></a>
	</div>
  </div>
</c:if>
  </c:when>
  <c:otherwise>
  <c:if test="${COVID19_SYSVAR eq 'ON' and parentCategory ne 'coronavirus'}">
  <div class="covid_ltms covid_dsk">
    <span class="covid_txt"><spring:message code="wuni.covid.label"/></span>
	<a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_wht_arw.svg", 0)%>"></a>
  </div>
</c:if>
  </c:otherwise>
</c:choose>


<%}if( !hideTimeLinePages.contains(pagename3) && !"clearingProfile.jsp".equalsIgnoreCase(pagename3) &&!"contenthub.jsp".equalsIgnoreCase(pagename3) && !"offerSuccess.jsp".equalsIgnoreCase(pagename3) &&  !"whatuniGoLandingPage.jsp".equalsIgnoreCase(pagename3) && !"subjectLandingPage.jsp".equalsIgnoreCase(pagename3) && !"applicationPage.jsp".equalsIgnoreCase(pagename3)){%>
  <jsp:include page="/jsp/common/wuIncludeUserTimeLinePod.jsp"/>
<%}%>
<%if(!hideMobileSrchPages.contains(pagename3)){%>
<jsp:include page="/jsp/common/includeMobileSearchPod.jsp"/>

<%}%>
<%if( !hideNavSrchHeaderPages.contains(pagename3) && !(switchPages.indexOf(pagename3) > -1 && ("ON".equalsIgnoreCase(clearingonoff) || "ON".equalsIgnoreCase(postClearingOnOff))) && !"qlBasicForm.jsp".equalsIgnoreCase(pagename3) && !"qlAdvanceForm.jsp".equalsIgnoreCase(pagename3) && !"offerSuccess.jsp".equalsIgnoreCase(pagename3) && !"whatuniGoLandingPage.jsp".equalsIgnoreCase(pagename3) && !"subjectLandingPage.jsp".equalsIgnoreCase(pagename3) && !"gradeFilterLandingPage.jsp".equalsIgnoreCase(pagename3) && !"applicationPage.jsp".equalsIgnoreCase(pagename3) && !"newUserGradeLandingPage.jsp".equalsIgnoreCase(pagename3) && !"clearingebooklandingpage.jsp".equalsIgnoreCase(pagename3) && !"clearingebooksuccess.jsp".equalsIgnoreCase(pagename3)){%>
<c:choose>
<c:when test="${banPageName eq 'openDaysLandingPage.jsp'}">
<c:if test="${COVID19_SYSVAR eq 'ON' and parentCategory ne 'coronavirus'}">
  <div class="covid_ltms covid_mob">
    <div class="covid_txcnt">
    <span class="covid_txt"><spring:message code="wuni.covid.label"/></span>
	<a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_wht_arw.svg", 0)%>"></a>
	</div>
  </div>
</c:if>
</c:when>
<c:otherwise>
<c:if test="${COVID19_SYSVAR eq 'ON' and parentCategory ne 'coronavirus'}">
  <div class="covid_ltms covid_mob">
  
    <span class="covid_txt"><spring:message code="wuni.covid.label"/></span>
	<a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_wht_arw.svg", 0)%>"></a>
	
  </div>
</c:if>
</c:otherwise>
</c:choose>
<%} %>
<jsp:include page="/jsp/clearing/clearingSwitch.jsp"> 
  <jsp:param name="podType" value="covid_mob"/>  
</jsp:include>

<input type="hidden" name="gaAccount" value="UA-22939628-2" id="gaAccount"/>
<input type="hidden" id="contextJsPath" value="<%=CommonUtil.getJsPath()%>" />
<input type="hidden" id="clearingYear" value="Clearing <%=GlobalConstants.CLEARING_YEAR%>" />
<input type="hidden" id="headerClass" value="<%=headerClass%>" />
<input type="hidden" id="clearingonoff" value="${clearingonoff}" />
<c:if test="${not empty POST_CLEARING_ON_OFF and POST_CLEARING_ON_OFF eq 'ON' }">
<input type="hidden" id="pagePathGALog" value="${pagePathGALog}" />
</c:if>
<c:if test="${CLEARING_ON_OFF_SYSVAR eq 'ON'}"> <script type="text/javascript" language="javascript" src="${getJsDomainValue}/js/common/loadash.min.js"></script></c:if>
 <c:choose>
  <c:when test="${fn:containsIgnoreCase(PAGE_NAME, 'browseMoneyPageResults.jsp') or fn:containsIgnoreCase(PAGE_NAME, 'courseSearchResult.jsp')}"> 
    <link rel="preload" as="script" href="<%=CommonUtil.getJsPath()%>/js/home/wu_switchHeader_20200721.js">
    <link rel="preload" as="script" href="<%=CommonUtil.getJsPath()%>/js/mychoice/<%=myFinalChoiceJs%>">
  </c:when>
  <c:otherwise>
   <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/home/wu_switchHeader_20200721.js"></script>
   <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/mychoice/<%=myFinalChoiceJs%>"></script>
  </c:otherwise>
</c:choose>

<script type="text/javascript">
  var vulnerableKeywords = ["onclick","`","{","}","~","|","^","http:","javascript:","https:","ftp:","type='text/javascript'","type=\"text/javascript\"","language='javascript'","language=\"javascript\""];
  var htmlTags = ["<!DOCTYPE","<!--","<acronym","<abbr","<address","<applet","<area","<article","<aside","<audio","<a","<big","<bdi","<basefont","<base","<bdo","<blockquote","<body","<br","<button","<b","<center","<canvas","<caption","<cite","<code","<colgroup","<col","<dir","<datalist","<dd","<del","<details","<dfn","<dialog","<div","<dl","<dt","<embed","<em","<frameset","<frame","<font","<fieldset","<figcaption","<figure","<footer","<form","<header","<head","<h1","<h2","<h3","<h4","<h5","<h6","<hr","<html","<iframe","<img","<ins","<input","<i","<kbd","<keygen","<label","<legend","<link","<li","<main","<map","<mark","<menuitem","<menu","<meta","<meter","<noscript","<noframes","<nav","<object","<ol","<optgroup","<option","<output","<param","<\pre","<progress","<p","<q","<rp","<rt","<ruby","<svg","<samp","<script","<section","<select","<small","<strike","<source","<span","<strong","<style","<sub","<summary","<sup","<s","<table","<thead","<tbody","<tfoot","<tt","<td","<th","<tr","<text\area","<time","<title","<track","<ul","<u","<var","<video","<wbr","<\/acronym","<\/abbr","<\/address","<\/applet","<\/area","<\/article","<\/aside","<\/audio","<\/a","<\/big","<\/bdi","<\/basefont","<\/base","<\/bdo","<\/blockquote","<\/body","<\/br","<\/button","<\/b","<\/center","<\/canvas","<\/caption","<\/cite","<\/code","<\/colgroup","<\/col","<\/dir","<\/datalist","<\/dd","<\/del","<\/details","<\/dfn","<\/dialog","<\/div","<\/dl","<\/dt","<\/embed","<\/em","<\/frameset","<\/frame","<\/font","<\/fieldset","<\/figcaption","<\/figure","<\/footer","<\/form","<\/header","<\/head","<\/h1","<\/h2","<\/h3","<\/h4","<\/h5","<\/h6","<\/hr","<\/html","<\/iframe","<\/img","<\/ins","<\/input","<\/i","<\/kbd","<\/keygen","<\/label","<\/legend", "<\/link","<\/li","<\/main","<\/map","<\/mark","<\/menuitem","<\/menu","<\/meta","<\/meter","<\/noscript","<\/noframes","<\/nav","<\/object","<\/ol","<\/optgroup","<\/option","<\/output","<\/param","<\/pre","<\/progress","<\/p","<\/q","<\/rp","<\/rt","<\/ruby","<\/svg","<\/samp","<\/script","<\/section","<\/select","<\/small","<\/strike","<\/source","<\/span","<\/strong","<\/style","<\/sub","<\/summary","<\/sup","<\/s","<\/table","<\/thead","<\/tbody","<\/tfoot","<\/tt","<\/td","<\/th","<\/tr","<\/textarea","<\/time","<\/title","<\/track","<\/ul","<\/u","<\/var","<\/video","<\/wbr"];
</script>
</div>