<%@page import="WUI.utilities.CommonFunction,WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<c:set var="pagename">
  <tiles:getAsString name="pagename" ignore="true"/>
</c:set>

<%
  String collegeCount   = (String)request.getParameter("collegeCount");
  String userId         = (String)request.getParameter("userId");
  String userName       = new CommonFunction().getUserName(userId,  request);
         userName       = GenericValidator.isBlankOrNull(userName)? "Hi, Nice to see you again!":userName;    
  String userProfileImg = "";      
  String mandFieldIsProv= "circle"; 
  CommonUtil util = new CommonUtil();
  String TCVersion = util.versionChanges(GlobalConstants.TERMS_AND_COND_VER);
  String PRVersion = util.versionChanges(GlobalConstants.PRIVACY_VER);
  String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
  String HIDE_LOGIN = (String)request.getAttribute("HIDE_LOGIN");
  String userDispName = (String)request.getAttribute("userDispName");
  String fullName = (String)request.getAttribute("fullName");
  String logoutUrl = "javascript:void(0);";
  String shortListCls = "";
  String userImgCls = "";
  boolean userProfImgFlag = false;
  if(!"0".equals(collegeCount)){
    shortListCls = "short_lst";
  }
%>
<%-- To display the Login Box --%>
<%if(!"Y".equalsIgnoreCase(HIDE_LOGIN)){%> 
<c:if test="${empty sessionScope.userInfoList}">
  <div id="topProfIconId" class="fr w160 signup mt5 <%=shortListCls%>">
    <div class="fr">
    <ul>
      <li class="user_prfnoimg">
      <c:choose>      
        <c:when test="${fn:containsIgnoreCase(pagename, 'searchResultsLandingPage.jsp')}">
        <a rel="nofollow" href="javascript:void(0);" title="Sign up / Log in" class="fnt_lrg login loged_in" id="loglink">
		  <span class="sup_txt fnt_lrg">Sign up / Log in</span>
		  <span class="sgnup_ui"></span>
		</a>
        </c:when>
        <c:otherwise>
		<a rel="nofollow" href="javascript:void(0);" title="Sign up / Log in" class="fnt_lrg login loged_in" id="loglink" onclick="javaScript:showLightBoxLoginForm('popup-newlogin',650,500, 'Y','','global-nav');">
		  <span class="sup_txt fnt_lrg">Sign up / Log in</span>
		  <span class="sgnup_ui"></span>
		</a>
		</c:otherwise>
		</c:choose>
      </li>
      <li>
        <a title="Unis compared" href="javascript:void(0);" onclick="javaScript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="fnt_lrg login sht_lst" id="loglink"> 
          <span class="f5sel_ui"></span>
          <span class="cnt_ui fnt_lbd ml5 mr10" id="s_count"><%=collegeCount%></span>
        </a>
        <div class="drop_menu fl" id="loginDropDown" style="display:none;">
		      <ul><li class="fl bor_rad5"><a class="fl first bor_rad5"><span class="tip"></span><span class="unis_comp fl fnt_lrg">Unis compared</span><span id="s_count_drop_down" class="count fnt_lbd fr"><%=collegeCount%></span></a></li></ul>
		    </div>       
      </li>
    </ul>                  
    </div>    
  </div>   
</c:if>
<c:if test="${not empty sessionScope.userInfoList}">
  <c:forEach var="logedUserInfo" items="${sessionScope.userInfoList}">
  	<c:if test="${not empty logedUserInfo.userImage}">
  	  <c:set var="userImg" value="${logedUserInfo.userImage}"/>
      <jsp:useBean id="userImg"  type="java.lang.String"/>
      <%userProfileImg = userImg;%>
    </c:if>
    <c:if test="${not empty logedUserInfo.statusFlag and logedUserInfo.statusFlag eq 'Y'}">
        <%mandFieldIsProv = "";%>
   </c:if>     
  </c:forEach>
  <%if((!"".equals(userProfileImg)) && !(userProfileImg.indexOf("wu_ptu_icon.png") > -1)){
    userImgCls = "usrpro_act";
    userProfImgFlag = true;
  }%>
  <div id="topProfIconId" class="fr w160 signup mt5 logd_in <%=shortListCls%> <%=userImgCls%>">
    <div class="fr">
     <ul>
       <li class="user_prfnoimg" onclick="showUserMenu('mob_menu_act');">
         <a href="javascript:void(0);" class="fnt_lrg login loged_in" id="loglink">           
           <span class="sgnup_ui"></span>
           <%if(!"".equals(mandFieldIsProv)){%>
             <span class="cnt_ui fnt_lbd ml5 mr10" id="s_count_circle"></span>
           <%}%> 
         </a>
       </li>
       <%if(userProfImgFlag){%>
       <li class="user_prfimg" onclick="showUserMenu('mob_menu_act');">
         <a href="javascript:void(0);" class="noalert"><img src="<%=userProfileImg%>" alt="User profile image"/>
         <%if(!"".equals(mandFieldIsProv)){%>
             <span class="cnt_ui fnt_lbd ml5 mr10" id="s_count_circle"></span>
           <%}%>
         </a>         	
       </li>            
       <%}%>                            
       <li>
         <a title="Unis compared" href="<%=request.getContextPath()%>/comparison" title="" class="fnt_lrg login sht_lst" id="loglink"> 
           <span class="f5sel_ui"></span><span class="cnt_ui fnt_lbd ml5 mr10" id="s_count"><%=collegeCount%></span>
         </a>
        <div id="mob_menu_act" class="mob_rht_menu" style="display: none;"> <span class="tip"></span>
					<ul>
					  <%if(!"".equals(mandFieldIsProv)){%>
					    <li><a href="<%=request.getContextPath()%>/mywhatuni.html">My Profile<span class="com_prof">Complete your profile</span></a></li>
					  <%}else{%>
					    <li><a href="<%=request.getContextPath()%>/mywhatuni.html">My Profile</a></li>
					  <%}%>             
					  <li><a id="loglink" rel="nofollow" href="<%=request.getContextPath()%>/userLogin.html?e=logout">Logout</a></li>
					</ul>
	    	</div>                    
       </li>
     </ul>
    </div>
  </div>
  <input type="hidden" id="loginStaus" value="true"/>    
  <!-- <input type="hidden" id="s_count"/> -->
  <input type="hidden" id="s_count_drop_down"/>
</c:if>
<%}else{%>

<c:if test="${ not empty sessionScope.userInfoList }">
  <%logoutUrl = request.getContextPath()+ "/usrlogout.html";%>
</c:if>
<div class="acc_set_cont cmm_usrnmcnt">
  <%if("Y".equalsIgnoreCase(HIDE_LOGIN) && !GenericValidator.isBlankOrNull(userDispName)){%>
    <div class="acc_set_pod set_bor cmm_user">
    <ul>
      <li> <a href="javascript:void(0);" class="noalert"><span class="unm_log">Logged in</span> <span class="unm_len" title="<%=fullName%>"><%=userDispName%></span> </a>
      <c:if test="${not empty sessionScope.userInfoList}">
      <div id="mob_menu_act" class="mob_rht_menu">
          <span class="tip"></span>
            <ul>
              <li><a href="/degrees/mywhatuni.html">VIEW PROFILE</a></li>
              <li><a id="loglink" rel="nofollow" href="<%=request.getContextPath()%>/usrlogout.html">Logout</a></li>
            </ul>
          </div>
      </c:if>
      </li>
      </ul>
  </div>
    <%}%>
  </div>  



 <div id="mob_nav_rt" class="mob_nav_rt cmm_mobusr">
    <ul>
     <%-- <li class="inactive"><a href="<%=request.getContextPath()%>/courses/"><i class="fa fa-search"></i></a></li>      --%>
     <li class="inactive"><!-- hide y -->
      <%if("Y".equalsIgnoreCase(HIDE_LOGIN) && !GenericValidator.isBlankOrNull(userDispName)){%>
     <a href="javascript:void(0);" class="noalert"><span class="unm_len" title="<%=userDispName%>"><%=userDispName%></span> </a>
     <%}%> 
     <div id="mob_menu_act" class="mob_rht_menu">
          <span class="tip"></span>
          <ul>
            <li><a href="javascript:void(0);"><div class="profvwlk"><%=fullName%></div></a></li>
            <c:if test="${ not empty sessionScope.userInfoList }">
            <li><a href="<%=request.getContextPath()%>/mywhatuni.html"><i class="fa fa-cog"></i> Settings</a></li>
           <li><a href="<%=request.getContextPath()%>/usrlogout.html">Logout</a></li>
           </c:if>
          </ul>
        </div>    
      </li>      
    </ul>				
  </div>	

<%}%>   
  <input type="hidden" id="TCVersion" value="<%=TCVersion%>"/>
  <input type="hidden" id="PRVersion" value="<%=PRVersion%>"/>