<%--
  * @purpose:  This jsp is included for responsive view..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * *************************************************************************************************************************
--%>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction,org.apache.commons.validator.GenericValidator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="environmentName" value="${sessionScope.sessionData['WU_ENV_NAME']}"/>
<%
  String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
  String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
  String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
  String flag = request.getParameter("flag");
         flag = (GenericValidator.isBlankOrNull(flag)) ? "" : flag;   
  String prPageFlag = request.getParameter("prPageFlag");
         prPageFlag = (GenericValidator.isBlankOrNull(prPageFlag)) ? "" : prPageFlag;
  String pageName = (String)request.getAttribute("pageName");
  String environmentName = (String)pageContext.getAttribute("environmentName");
%>

<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
  
<script type="text/javascript" language="javascript">
    function $$D(id){return document.getElementById(id)}
    var dev = jQuery.noConflict();
    dev(document).ready(function(){
      adjustStyle();      
    });
    adjustStyle();    
    function jqueryWidth() {
      return dev(this).width();
    } 
    var width = document.documentElement.clientWidth;
    function adjustStyle() {
      var path = ""; 
      var screenWidth = document.documentElement.clientWidth;      
      dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
      dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
      if (screenWidth <= 480) {
        <%if("findUni".equalsIgnoreCase(flag)){%>
          dev('.op_mth_slider ul').attr("id", "select_cont");
          if (dev('#ajax_listOfOptions').attr('class') != 'opd_ajax' && dev('#ajax_listOfOptions').attr('class') != 'ajx_cnt' && dev('#ajax_listOfOptions').attr('class') != 'ajx_cnt ajx_res') { //Added to fix ajax alignment by Sangeeth.S
              dev('#ajax_listOfOptions').attr("class", "ajax_mob");
          }
        <%}%> 
        if (dev("#viewport").length == 0) {
          dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }        
        
      <%if(("LIVE").equals(environmentName)){%>
          $$D('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("TEST".equals(environmentName)){%>
          $$D('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("DEV".equals(environmentName)){%>
          $$D('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}%>        
      } else if ((screenWidth > 480) && (screenWidth <= 992)) {
          dev('.gen_srchbox').css('width', '100%').css('width', '-=18px');
          if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
          }
          <%if(("LIVE").equals(environmentName)){%>
            $$D('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
          <%}else if("TEST".equals(environmentName)){%>
            $$D('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
          <%}else if("DEV".equals(environmentName)){%>
            $$D('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
          <%}%>
          <%if("findUni".equalsIgnoreCase(flag)){%>
            dev('#mbox').attr("class", "mob_res_lb");
          <%}%>
          
          <%-- <%if("spreDesign".equalsIgnoreCase(flag)){%> 
            
            if($$D('ajax_listOfOptions')){
             // blockNone("ajax_listOfOptions","none");
            } 
          <%}%>    
          --%>          
 
      } else {
          if (dev("#viewport").length > 0) {
            dev("#viewport").remove();
          }
          dev(".hm_srchbx").hide();
          <%if("findUni".equalsIgnoreCase(flag)){%>
            if (dev('#ajax_listOfOptions').attr('class') != 'opd_ajax') {
                dev('#ajax_listOfOptions').removeAttr("class");
            }
            dev("#mobilefixedscrolltop").removeAttr("style").removeClass("sticky_top_nav");
            dev('.op_mth_slider ul').removeAttr("id");
            dev('.op_mth_slider ul li').css('width', '77px');
          <%}%> 
          $$D('size-stylesheet').href = "";
        }
        <%if("spreDesign".equalsIgnoreCase(flag) && (!("prPageFlag".equalsIgnoreCase(prPageFlag)))){%>  
          if(screenWidth <= 992){
            dev('.gr_filt_cont').css({'display':'none'});
          }
        <%}%>        
    }    
    dev(window).on('orientationchange', orientationChangeHandler);
    function orientationChangeHandler(e) {
      setTimeout(function() {
        dev(window).trigger('resize');     
      }, 500);
    }
    dev(window).resize(function() {
      var screenWidth = jqueryWidth();      
      dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
      dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
      <%if("findUni".equalsIgnoreCase(flag)){%>
        if($$D("odLocMap")){
          mapLoad();
        }        
        if (screenWidth <= 480){  //Mobile view 
          dev('.op_mth_slider ul').css({"left": "0px", "display": "none"});
          dev("#fixedscrolltop").removeAttr("style").removeClass("sticky_top_nav");
        }
        else if((screenWidth > 480) && (screenWidth <= 992)){  //Tablet view 
          dev('.gen_srchbox').css('width', '100%').css('width', '-=18px');
          dev("#fixedscrolltop").removeAttr("style").removeClass("sticky_top_nav"); //Removing sticky top
          dev('.op_mth_slider ul').css({"left": "0px", "display": "none"});          
        }
        else{  //Desktop view 
          /*For sticky top*/        
          dev('.gen_srchbox').removeAttr("style");
          dev("#mobilefixedscrolltop").removeAttr("style").removeClass("sticky_top_nav");
          dev('.op_mth_slider ul').css("display", "block");
          dev('.op_mth_slider ul li').css("height", "39px");
          openDayOrientation();
          dev('#mbox').removeClass("mob_res_lb");
          if (dev("#ol").is(':visible')) {
            var posleft = (screenWidth - 650)/2;
            dev('#mbox').css({ left :posleft+'px'});
          }          
        }
     <%}else{%> 
       if(screenWidth > 992){
         <%if("spreDesign".equalsIgnoreCase(flag)){%> // sr page orientation code           
           dev("#mbox.pers_srch").css("display", "none");
           if($$D('grPerson')){            
            hm(); // Closing the grade filter popup
           }
         <%}else{%> // Slider orientation code
           dev(".sucess #Slider").css("left","186px");
         <%}%>         
       }       
     <%}%>
     
    <%--  <%if("spreDesign".equalsIgnoreCase(flag)){%> 
       if($$D('ajax_listOfOptions')){
         //blockNone("ajax_listOfOptions","none");
       }
     <%}%>  --%>
     
     adjustStyle();
     
     <%-- <%if("spreDesign".equalsIgnoreCase(flag) && (!("prPageFlag".equalsIgnoreCase(prPageFlag))) && (!"CLEARING_SEARCH_RESULTS".equalsIgnoreCase(pageName))){%> 
       loadGradePopup(); // Grade filter script
     <%}%> --%>
     
   });
</script>
