<%@page import="WUI.utilities.CommonUtil" %>
<meta name="apple-itunes-app" content="app-id=1267341390"/>

<meta name="google-play-app" content="app-id=com.hotcourses.group.wuapp">
<link rel="shortcut icon" href="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/icons/wuicon.ico" type="x-icon" />
<link rel="apple-touch-icon" sizes="152x152" href="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/icons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="144x144" href="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/icons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="114x114" href="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/icons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="72x72" href="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/icons/apple-touch-icon-72x72.png">