
<%@page import="WUI.utilities.SessionData, WUI.utilities.GlobalConstants" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="pagename3">
   <tiles:getAsString name="pagename3" ignore="true"/>
</c:set>
<%
   String noJSlogging = "";
   if(request.getAttribute("noJSLogging")!=null && "false".equals(request.getAttribute("noJSLogging"))){
     noJSlogging = (String)request.getAttribute("noJSLogging");
   }   
%>
   
<jsp:include page="/jsp/clearing/include/gradeFilterPopupLayout.jsp">
   <jsp:param name="pageName" value="${pagename3}"/>
</jsp:include>

<div id="revLightBox"></div>
<div id="loaderTopNavImg" class="cmm_ldericn zindx" style="display:none;"><img alt="loading" src="${hm_ldr}"></div>

<c:if test="${not empty footerSection}">
${footerSection}
</c:if>

<div class="clear"></div>

<jsp:include page="/jsp/common/tickerTapePod.jsp" />

<div id="newvenue" class="dialog" >
   <div id="newvenueform" style="display: none;" ></div>
   <div id="ajax-div" class="lb_pload" style="display: none;">
      <img src="${cmn_loading_1 }" alt="loading image" title="" />
      <span id="txt">Please wait</span>
      <input id="loading_cancel" type="button" onclick="hm('newvenue');" value="Cancel" class="button"/>
   </div>
</div>

<style type="text/css">
   #holder_NRW{
   -webkit-overflow-scrolling: touch;
   overflow:auto;
   min-height:400px;
   }
</style>

<div style="position:fixed;width:100%;height:100%;background:#000;opacity:.8;top:0;position:fixed;display:none;z-index:9999;" id="fade_NRW"></div>
<a style="position:fixed;top:10px;right:15px;color:#fff;font-weight:bold;display:none;font-size:24px;z-index:99999;" id="close_NRW" onclick="customConfirm();">
<i class="fa fa-times"></i></a>
<div style="position:fixed;margin:50px;background:#fff;top:0;display:block;position:fixed;display:none;z-index:99999;" id="holder_NRW">
   <iframe src="" id="iframe_NRW" frameborder="0" width="100%" height="99%"></iframe>
</div>

<jsp:include page="/jsp/thirdpartytools/includeCallBanner.jsp"/>

<jsp:include page="/jsp/thirdpartytools/include/includeAdmedoDiv.jsp">
   <jsp:param name="pagename" value="${pagename3}"/>
</jsp:include>

<jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />

<jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />

<%   //FOR STATS LOG Entry with the javascript
   if("".equals(noJSlogging)){   //JS logging will be proceed when noJSlogging is empty 30_JUN_15 by Amir
     if(session.getAttribute("stats_log_id") != null){ 
       String session_log_id = (String) session.getAttribute("stats_log_id");
       String inArray[] = session_log_id.split(",");
%> 
<script type="text/javascript" language="javascript" src="${contextJsPath}/js/statslog-wu514.js"></script>  
<%out.println("<script type=\"text/javascript\" language=\"javascript\">");
   //Changed WU scheme as part of SSL work for 08_MAR_2016, By Thiyagu G.
   String domainSchemeName=GlobalConstants.WHATUNI_SCHEME_NAME;
   if(inArray !=null){
     for(int arloop = 0; arloop<inArray.length; arloop++){
%>
document.write(unescape("%3Cscript type='text/javascript'%3E var boo %3D callStatLogRequest('jslog','%3Fl%3D<%=inArray[arloop]%>','<%=domainSchemeName%>
<spring:message code="review.autocomplete.ip"/>
')%3B %3C/script%3E"));
<%}
   } 
      session.removeAttribute("stats_log_id");
      out.println("</script>");   
     }
   }
%>    
   
<p class="bk_stick bk_to_top" style="display:none" id="back-top">
   <a href="#top">
   <img src="${bk_2_top }" alt="go to top"/>
   </a>
</p>

<input type="hidden" id="smartBannerJs" name="smartBannerJs" value="jquery.smartbanner.js"/>
<c:if test="${not empty sessionScope.sessionData['LAT_LONG']}">
   <input type="hidden" id="latAndLong" value="${sessionScope.sessionData['LAT_LONG']}">
</c:if>
<input type="hidden" id="clearingHomeJsName" value="/js/clearing/<spring:message code='wuni.common.clearing.home.js'/>" />
<input type="hidden" id="gradeFilterClrSrchJS" value="/js/clearing/<spring:message code='wuni.ucas.grader.filter.js'/>" />
<input type="hidden" id="gradeFilterClrSrchJSLoaded" value="0" />
<input type="hidden" id="topNavSrchJS" value="/js/topNavSearchPopup/<spring:message code='wuni.topnav.search.popup.js'/>" />
<input type="hidden" id="emailDomainJsName" value="<spring:message code='wuni.email.domain.js'/>" />
<input type="hidden" id="facebookLoginJSName" value="<spring:message code='wuni.facebook.login.js'/>" />
<input type="hidden" id="commonUserProfileJSName" value="<spring:message code='wuni.common.user.profile.js'/>" />
<input type="hidden" id="loggedInUserId" value="<%=new SessionData().getData(request, "y")%>" />
<input type="hidden" id="contextPath" value="/degrees" />
<input type="hidden" id="domainSpecPath" value="${whatuniDomain}" />
<input type="hidden" id="pdfFromPageURL" />
<input type="hidden" id="isMobileUA" />
<input type="hidden" id="networkId" name="networkId" value="${networkId }" />
<input type="hidden" id="boostedInstId" name="boostedInstId" value="${sessionScope.boostedInstId}"/>
<input type="hidden" id="sponsoredInstId" name="sponsoredInstId" value="${sessionScope.sponsoredInstId}"/>
<input type="hidden" id="sponsoredListFlag" value="${sponsoredListFlag}"/>
<input type="hidden" id="manualBoostingFlag" value="${manualBoostingFlag}"/>
<input type="hidden" id="manualBoostingFlag" value="${manualBoostingFlag}"/>
<input type="hidden" id="isMobileUserAgent" value="${mobileFlag }"/>
<input type="hidden" id="domainPathWidget" value="${whatuniDomain}">
<input type="hidden" id="lightBoxName" value=""/>
<input type="hidden" id="MobileURL" value=""/>
