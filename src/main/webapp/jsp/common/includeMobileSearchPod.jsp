<%@page import="WUI.utilities.CommonFunction,WUI.utilities.SessionData, WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<c:set var="pagename3">
   <tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
 
 
<%
  String userId         =  new SessionData().getData(request,"y");
  String collegeCount   =  (String)request.getSession().getAttribute("basketpodcollegecount");
  String get_pagename   =  "";
  if(pageContext.getAttribute("pagename3") != null){
	  get_pagename = (String)pageContext.getAttribute("pagename3");
  }else{
	  get_pagename = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1,request.getRequestURI().length());    
  }
  if(collegeCount == null || "0".equalsIgnoreCase(collegeCount)) {
    new CommonFunction().getBasketPodContent(request, response);
    collegeCount = (String)request.getSession().getAttribute("basketpodcollegecount");
  }
  String clearingUserType = (String)session.getAttribute("USER_TYPE") ;   
  String onclickTopNavMethod = "showComSearchPopup()";
  String navDefualtText = GlobalConstants.SEARCH_POD_PLACEHOLDER_TEXT;
  pageContext.setAttribute("PAGE_NAME", get_pagename);
%>
<section class="hms_cnt hdhms_cnt">
  
  <c:if test="${not empty requestScope.clearingSwitchFlag and requestScope.clearingSwitchFlag eq 'ON'}">
    <%if(!GenericValidator.isBlankOrNull(clearingUserType) && "CLEARING".equals(clearingUserType)){onclickTopNavMethod = "callSubLandPage()";navDefualtText="Enter subject or uni";}%>
  </c:if>
  <%-- Handled condition to hide the search bar for pre clearing pages in mobile view by Sujitha V o 02_JUNE_2020 --%>
  <%-- Handled condition to hide the search bar for clearing page in mobile view by Kailash L  23_JUNE_2020 --%>
  <c:if test="${!fn:containsIgnoreCase(PAGE_NAME, 'clearingebooklandingpage.jsp') and !fn:containsIgnoreCase(PAGE_NAME, 'clearingebooksuccess.jsp') and !fn:containsIgnoreCase(PAGE_NAME, 'clearingHome.jsp')}"> 
    <div class="hms_wrap" onclick="<%=onclickTopNavMethod%>">
      <input class="hms_inp" type="text" placeholder="<%=navDefualtText%>" />
      <span class="hms_fa"><i class="fa fa-search" aria-hidden="true"></i></span>
    </div>
  </c:if>
</section>
<div class="hm_srchbx" id="mobSearch" style="display:none;">
  <div class="srb_wrap">
    <div class="ser_cls" onclick="showComSearchPopup();">
      <a class="" href="javascript:void(0);"><i class="fa fa-times"></i></a>
    </div>
    <div class="srch_cnt">
      <c:if test="${not empty requestScope.clearingSwitchFlag}">  
        <c:if test="${requestScope.clearingSwitchFlag eq 'OFF'}">
          <form id="navMobSearchForm">
					<div class="hms_cnt">
	          <div class="hms_wrap" onclick="showComSearchPopup()">
	            <input class="hms_inp" type="text" placeholder="<%=navDefualtText%>" onclick="showComSearchPopup()" onfocus="showComSearchPopup()" readonly="readonly"> 
	            <span class="hms_fa"><i class="fa fa-search" aria-hidden="true"></i></span> 
	          </div>
	        </div>			
        </form>
      </c:if>
	 <c:if test="${requestScope.clearingSwitchFlag eq 'ON'}">		
		<form id="navMobSearchForm" method="post" class="topsearch" > 
			<div class="hms_cnt">
	          <div class="hms_wrap" onclick="callSubLandPage()">
	            <input class="hms_inp" type="text" placeholder="Enter subject or uni" onclick="callSubLandPage()" onfocus="callSubLandPage()" readonly="readonly"> 
	            <span class="hms_fa"><i class="fa fa-search" aria-hidden="true"></i></span> 
	          </div>
	        </div>
			
		</form>
	  </c:if>  
	  <input type="hidden" id="matchbrowsenodemob" value=""/>
	  <input type="hidden" id="selMobQual" value=""/>
    </c:if>
    </div>
    <div class="sb_pdeg" id="subGuides">
      <jsp:include page="/jsp/search/courseSearch/include/subjectGuidesPod.jsp"/>
	</div>
  </div>
</div>