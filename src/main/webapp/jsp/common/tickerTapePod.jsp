<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page import="WUI.utilities.CommonUtil,java.util.ArrayList" %>
<%
   ArrayList listOfTickerTapes = (ArrayList)request.getAttribute("listOfTickerTapes");
   if(listOfTickerTapes == null || listOfTickerTapes.size() < 1){
     listOfTickerTapes = new CommonUtil().getTickerTapes();    
     request.setAttribute("listOfTickerTapes",listOfTickerTapes);
   }
%>
<c:if test="${not empty requestScope.listOfTickerTapes}">
   <div class="sticky_foo" id="tickerTape" style="display:none;">
      <div class="al_mid">
         <div onmouseout="if(!annst) annst = setTimeout('announcementScroll()', anndelay);" onmouseover="if(!anncount) {clearTimeout(annst);annst = 0}">
            <ul>
               <span id="tapetext">
                  <ul>
                     <c:forEach var="tickerTape" items="${requestScope.listOfTickerTapes}"  varStatus="i" >
                        <c:set var="i" value="${i.index}"/>
                        <li><strong> ANNOUNCEMENTS: </strong><a rel="nofollow" target="_blank" id="tickerLink<%=String.valueOf(pageContext.getAttribute("i"))%>"
                           onclick="advertiserExternalUrlStatsLogging('tickertape','${tickerTape.tickerCollegeId}','${tickerTape.tickerExternalUrl}','<%=session%>');"
                           href="${tickerTape.tickerExternalUrl}" >
                           ${tickerTape.tickerDescription}</a>  
                        </li>
                     </c:forEach>
                  </ul>
               </span>
            </ul>
         </div>
      </div>
   </div>
</c:if>