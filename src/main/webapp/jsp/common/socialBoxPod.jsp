<%@page import=" WUI.utilities.CommonUtil,WUI.utilities.CommonFunction,org.apache.commons.validator.GenericValidator" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
 
<%--
  * @purpose:  This jsp is to display the social box..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                  Rel Ver.
  * 24-Nov-2015    Indumathi S               1.0      First draft                   wu_547
  * 13-Dec-2017    Indumathi.S               1.1      Removed facebook pod          wu_571
  * 06-jun-2018    Hema.S                    1.2      Added clearing courses button wu_577
  * *************************************************************************************************************************
--%>
<%//To hide Social Box on URL redirection 
  CommonFunction common = new CommonFunction();
  String userJourney = common.getClearingUserJourneyFlag(request);
  // Get page name to show chatbot sticky based on conditions
  int stind1 = request.getRequestURI().lastIndexOf("/");
  int len1 = request.getRequestURI().length();
  String pageName = "";
  if(pageContext.getAttribute("pagename3") != null){
	  pageName = (String)pageContext.getAttribute("pagename3");
  }else{
	  pageName = request.getRequestURI().substring(stind1+1,len1);
  }
  pageName = pageName !=null ? pageName.toLowerCase() : pageName;
  //
  if((("newUser.jsp").equalsIgnoreCase(pageName)  || ("retUser.jsp").equalsIgnoreCase(pageName) ||("loggedIn.jsp").equalsIgnoreCase(pageName))){
    pageName = "home.jsp";
  }
  //   
   if((!GenericValidator.isBlankOrNull(pageName)) && (("richprofilelanding.jsp").equalsIgnoreCase(pageName))){           
    if(request.getAttribute("richProfileType")!=null && "RICH_INST_PROFILE".equalsIgnoreCase((String)request.getAttribute("richProfileType"))){
      pageName = "richuniview.jsp";      
      request.setAttribute("getInsightName","uniview.jsp"); //13_JAN_15 Added by Amir
    }else if(request.getAttribute("richProfileType")!=null && "RICH_SUB_PROFILE".equalsIgnoreCase((String)request.getAttribute("richProfileType"))){
      pageName = "richsubjectprofile.jsp";
      request.setAttribute("getInsightName","subjectprofile.jsp"); //13_JAN_15 Added by Amir
    }
  }
  //
  if((!GenericValidator.isBlankOrNull(pageName)) && "newproviderhome.jsp".equals(pageName)){
    String newProfileType = (String)request.getAttribute("setProfileType");
    if((!GenericValidator.isBlankOrNull(newProfileType)) && "CLEARING_PROFILE".equals(newProfileType)){       
      pageName = "clearinguniprofile.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "CLEARING_LANDING".equals(newProfileType)){
      pageName = "clearingunilanding.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "NORMAL".equals(newProfileType)){
      pageName = "uniview.jsp";
    }else if((!GenericValidator.isBlankOrNull(newProfileType)) && "SUB_PROFILE".equals(newProfileType)){
      pageName = "subjectprofile.jsp";
    }    
  }
  // If session is not already, Apply the script method (which updates the session vaalue)
  // Else get session value and display chatbot based on session value
  String createSession = "";
  String isChatbotCookieClosed = (String)request.getSession().getAttribute("chatbot_promo_disabled");
  if(GenericValidator.isBlankOrNull(isChatbotCookieClosed)) {
    createSession = "updateSessionForChatbotDisbaled()";
  }
%>
<%-- Chatbot sticky added, 25_Sep_2018 By Sabapathi --%>
<%if(!"qlbasicform.jsp".equalsIgnoreCase(pageName)) {%>
  <div id="needHelpDiv" class="cbot_cnt">
     <div class="cbs_cnt cbot_min">
        <div id="need_help_text" class="cbs_rgt" onclick="chatPromoContentDisplay('OPEN_CHAT_PROMO', event, '', '<%=userJourney%>');"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/cbttip_arw.png",0)%>" alt="chatbot tooltip arrow" class="cbtip_arw"> <a class="cb_max" href="javascript:void(0);" tabindex="-1">Need some help?</a>
           <a href="javascript:void(0);" onclick="chatPromoContentDisplay('CLOSE_NEED_HELP', event);" class="cbtip_cls" tabindex="-1"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/cbttip_clse.svg",1)%>" alt="Chatbot minimize close"></a>
        </div>
        <div class="cbs_lft" onclick="chatPromoContentDisplay('OPEN_CHAT_PROMO', event, '', '<%=userJourney%>');">
           <a class="cb_max1" href="javascript:void(0);" tabindex="-1"></a>
        </div>
     </div>
     <div id="chatbot_promo" class="cbot_max" style="display:none">
        <div class="cbot_hd">
           <div class="cbs_cnt">
              <div class="cbs_lft">
                 <a href="javascript:void(0);" tabindex="-1"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/cbott_avatar@2x.png",1)%>" alt="Need some help"></a>
              </div>
              <div class="cbs_rgt">
                 <a tabindex="-1">Luna</a>
                 <div class="cb_stat"><span class="act"></span> <span>Online</span></div>
                 <a href="javascript:void(0);" onclick="chatPromoContentDisplay('CLOSE_CHAT_PROMO', event);<%=createSession%>" class="cbtip_cls" tabindex="-1"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/cbttip_clse.svg",1)%>" alt="Chatbot minimize close"></a>
              </div>
           </div>
        </div>
        <jsp:include page="/jsp/home/include/chatbotPromoContent.jsp">
            <jsp:param name="placeOfInclusion" value="NEED_HELP"/>
        </jsp:include>
     </div>
     <input type="hidden" id="isChatbotCookieClosed" name="isChatbotCookieClosed" value="<%=isChatbotCookieClosed%>"/>
  </div>
  
  <script type="text/javascript">
    var $ = jQuery.noConflict();
    $(window).on('load orientationchange', function (e) {
        setTimeout(function() {
          mobileChatBotPromotion("<%=pageName%>");
        }, 200);
    });
    
  </script>
  <%if("Y".equalsIgnoreCase(isChatbotCookieClosed)) {%>
    <script type="text/javascript">
      chatPromoContentDisplay('CLOSE_CHAT_PROMO');
    </script>
  <%}%>
<%}%>
