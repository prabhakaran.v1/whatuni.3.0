<%@page import="WUI.utilities.GlobalConstants" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
  String latitudeStr =  (String)request.getAttribute("latitudeStr");   
  String longitudeStr =  (String)request.getAttribute("longitudeStr");   
%>

<c:if test="${not empty requestScope.latitudeStr}">
  <input type="hidden" id="mbApiKey" value="<%=GlobalConstants.MAPBOX_API_KEY%>"/>
  <input type="hidden" id="mbScpt" value="<%=GlobalConstants.MAPBOX_SCRIPT%>"/>
  <input type="hidden" id="mbCss" value="<%=GlobalConstants.MAPBOX_CSS%>"/>
  <input type="hidden" id="mbStyle" value="<%=GlobalConstants.MAPBOX_STYLE%>"/><%--Added by Sangeeth.s for FEB_12_19_REL for mapbox--%>
</c:if>

<%--Changed static google map to mapbox for Feb_12_19 rel by Sangeeth.S--%>
<c:if test="${not empty requestScope.latitudeStr and not empty requestScope.longitudeStr}">
  <div class="mapbox_cnt">
    <div id="map_canvas"></div>
    <div id="map_div" class="mbx_canv"></div>
    <a id='viewGleMap' class='mbxbtn' href="<%=GlobalConstants.MAP_SEARCH_URL + latitudeStr%>,<%=longitudeStr%>" target="_blank"><%=GlobalConstants.VIEW_GOOGLE_MAP%></a>
  </div>
</c:if>

