<%@page import="WUI.utilities.CommonFunction,WUI.utilities.SessionData, WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
 

<%
  String userId         =  new SessionData().getData(request,"y");
  String collegeCount   =  (String)request.getSession().getAttribute("basketpodcollegecount");
  String get_pagename   =  "";
  if(pageContext.getAttribute("pagename3") != null){
	  get_pagename = (String)pageContext.getAttribute("pagename3");
  }	else {
	  get_pagename = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1,request.getRequestURI().length());    
  }
  
  if(collegeCount == null || "0".equalsIgnoreCase(collegeCount)) {
    new CommonFunction().getBasketPodContent(request, response);
    collegeCount = (String)request.getSession().getAttribute("basketpodcollegecount");
  }
  String clearingUserType = (String)session.getAttribute("USER_TYPE") ;  
  String onclickTopNavMethod = "showComSearchPopup()";
  String navDefualtText = GlobalConstants.SEARCH_POD_PLACEHOLDER_TEXT;
  pageContext.setAttribute("PAGE_NAME", get_pagename);
%>
<%if("qlAdvanceForm.jsp".equalsIgnoreCase(get_pagename)){%>
  <div id="hdrmenu1" class="header-right mt40 enq_profile"> 
    <jsp:include page="/jsp/common/loginDropDown.jsp">
      <jsp:param name="collegeCount" value="<%=collegeCount%>"/>
      <jsp:param name="userId" value="<%=userId%>"/>
    </jsp:include>
  </div>
<%}%>        

<div id="hdrmenu2" class="header-right mt30">
  <%if(!"newUser.jsp".equalsIgnoreCase(get_pagename)){ %>
  <c:if test="${not empty requestScope.clearingSwitchFlag}">  
    <c:if test="${requestScope.clearingSwitchFlag eq 'OFF'}">
      <form id="navSearchForm" class="topsearch">
        <div class="hms_cnt">
          <div class="hms_wrap" onclick="showComSearchPopup()">
            <input class="hms_inp" type="text" placeholder="<%=navDefualtText%>" onclick="showComSearchPopup()" onfocus="showComSearchPopup()" readonly="readonly"> 
            <span class="hms_fa"><i class="fa fa-search" aria-hidden="true"></i></span> 
          </div>
        </div>
        
        <input type="hidden" id="matchbrowsenode" value=""/>
        <input type="hidden" id="selQual" value=""/>        
      </form>  
    </c:if>
    <c:if test="${requestScope.clearingSwitchFlag eq 'ON'}">
    <%-- Handled condition to hide the search bar for clearing page by Kailash L  23_JUNE_2020 --%>
    <c:if test="${!fn:containsIgnoreCase(PAGE_NAME, 'clearingHome.jsp')}"> 
     <%if(!GenericValidator.isBlankOrNull(clearingUserType) && "CLEARING".equals(clearingUserType)){onclickTopNavMethod = "callSubLandPage()";navDefualtText="Enter subject or uni";}%> 
      <form id="navSearchForm" method="post" class="topsearch" onclick="<%= onclickTopNavMethod%>"> 
        <div class="hms_cnt">
          <div class="hms_wrap" onclick="<%= onclickTopNavMethod%>">
            <input class="hms_inp" type="text" placeholder="<%= navDefualtText%>" onclick="<%= onclickTopNavMethod%>" onfocus="<%= onclickTopNavMethod%>" readonly="readonly"> 
            <span class="hms_fa"><i class="fa fa-search" aria-hidden="true"></i></span> 
          </div>
        </div>        
        <input type="hidden" id="matchbrowsenode" value=""/>
        <input type="hidden" id="selQual" value=""/>        
      </form>
      </c:if> 
    </c:if>      
  </c:if>
  <%}%>    
  <jsp:include page="/jsp/common/loginDropDown.jsp">
    <jsp:param name="collegeCount" value="<%=collegeCount%>"/>
    <jsp:param name="userId" value="<%=userId%>"/>
  </jsp:include>  
</div>

    
