<%@ page import="WUI.utilities.CommonUtil" %>

<%--
  * @purpose:  This jsp used to load angular webapp in iframe.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name              Ver.     Changes desc          Rel Ver.
  * 08-Dec-2020    Hemalatha.K       1.0      First draft           
  * *************************************************************************************************************************
--%>

<div id="loaderImg" class="cmm_ldericn zindx">
  <img alt="loading" src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif", 0)%>">
</div>
<iframe id="iframeId" src="" frameborder="0" scrolling="no"></iframe>
