
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.SessionData, spring.util.GlobalMethods"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="pagename3">
   <tiles:getAsString name="pagename3" ignore="true"/>
</c:set>
<c:set var="clearingOnOff" value="${sessionScope.sessionData['CLEARING_ON_OFF']}"/>
<c:set var="currentYearAwards" value="${sessionScope.sessionData['CURRENT_YEAR_AWARDS']}"/>
<c:set var="enableAwardsLink" value="${sessionScope.sessionData['ENABLE_STUDENT_AWARD_LINKS']}"/>
<c:set var="COVID19_SYSVAR" value="${sessionScope.sessionData['COVID19_ON_OFF']}" scope="request"/>
<c:set var="preClearingOnOff" value="${sessionScope.sessionData['PRE_CLEARING_ON_OFF']}"/>
<c:set var="wuLogoClass" value="${sessionScope.sessionData['WU_HOME_PAGE_LOGO']}"/>
<spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
<spring:message code="review.form.url" var="reviewFormUrl"/>
<%    
   if(session.getAttribute("userInfoList") == null){
   new WUI.utilities.CommonUtil().autoLogin(request, response); //DB call
   }
   new GlobalMethods().setCovid19SessionData(request, response); //DB call
%>

<div class="hdnav_ui fl_w100">
   <div id="header" class="hrdr join_clr">
      <jsp:include page="/jsp/home/cookiePopup.jsp"/>
      <a title="Show navigation" class="navbar ui-link" id="navbar" ><i class="fa fa-bars"></i><span class="mnu_clse"></span></a>
      <div class="${wuLogoClass}"><a href="${whatuniDomain}" title="Whatuni"></a></div>
      
      <jsp:include page="/jsp/common/topNavSrchHeader.jsp"/>
      
   </div>
   <div id="hdr_menu" class="navi_mnu fl_w100">
      <nav class="hdr_menu mycmp_menu">
         <ul>
            <c:if test="${clearingOnOff eq 'ON'}">
               <li>
                  <a id="clearDivId" oncontextmenu="updateUserTypeSession('','clearing');" onclick="javascript:updateUserTypeSession('','clearing');" class="fnt_lbd" href="/degrees/university-clearing-${clearingYear}.html">
                     <spring:message code="clearing.2020.label"/>
                  </a>
               </li>
            </c:if>
            <c:if test="${clearingOnOff eq 'OFF'}">
               <c:if test="${preClearingOnOff eq 'ON'}">
                  <li>
                     <a class="fnt_lbd" href="/clearing-guide">
                        <spring:message code="clearing.2020.label"/>
                     </a>
                  </li>
               </c:if>
            </c:if>
            <li><a id="findDivId" class="fnt_lbd" onclick="" href="/degrees/courses/">Find a course </a>
            </li>
            <li><a class="fnt_lbd" href="/degrees/find-university/" onclick="">Find a uni </a></li>
            <li><a class="fnt_lbd" href="/degrees/prospectus/">Prospectuses</a></li>
            <li>
               <a class="fnt_lbd" href="/open-days/">
                  <spring:message code="virtual.tour.top.nav"/>
               </a>
            </li>
            <li id="reviewTabChg">
               <a id="dsk_reviews" class="fst fnt_lbd subNavDkLink" href="/university-course-reviews/">Reviews<i class="fa fa-plus-circle fa-1"></i></a>
               <a id="click_view1" class="fst fnt_lbd noalert subNavDkLink" onclick="" style="display:none;">Reviews<span id="click_view1_span" class="mnuicn"></span></a>                       
               <div class="chose_inr urws" id="dsk_rev_submenu">
                  <ul class="mnite_cnt2">
                     <li><a class="subNavDkLink" href="/university-course-reviews/">Read reviews</a></li>
                     <li><a class="subNavDkLink" href="${reviewFormUrl}">Write a review</a></li>
                     <c:if test="${enableAwardsLink eq 'ON'}">
                        <li><a class="subNavDkLink" href="/awards">Student Choice Awards</a></li>
                     </c:if>
                     <li><a class="subNavDkLink" href="/student-awards-winners/university-of-the-year/">WUSCA winners ${currentYearAwards}</a></li>
                  </ul>
               </div>
            </li>
            <li id="adviceTabChg">
               <a class="fnt_lbd desk_adv_act" id="deskNavTop" onclick="" href="/advice/">Advice</a>
               <a id="click_view2" class="fnt_lbd mob_adv_act noalert subNavDkLink">Advice<span id="click_view2_span" class="mnuicn"></span></a>
               <div class="chose_inr urws" id="dsk_adv_submenu">
                  <ul class="mnite_cnt2">
                     <li><a id="clearAdv" class="subNavDkLink" href="/advice/clearing/" title="Clearing">Clearing</a></li>
                     <li><a class="subNavDkLink" href="${teacherSectionUrl}" title="COVID-19 Updates">COVID-19 Updates</a></li>
                     <li class="mob_adv_act subNavDkLink" ><a href="/advice/" title="Advice">Advice Home</a></li>
                     <li><a href="/advice/research-and-prep/" class="subNavDkLink" title="Research &amp; Prep">Research &amp; Prep</a></li>
                     <li><a href="/advice/accommodation/" class="subNavDkLink" title="Accommodation">Accommodation</a></li>
                     <li><a href="/advice/student-life/" class="subNavDkLink" title="Student Life">Student Life</a></li>
                     <li><a href="/advice/blog/" class="subNavDkLink" title="Blog">Blog</a></li>
                     <li><a href="/advice/news/" class="subNavDkLink" title="News">News</a></li>
                     <li><a href="/advice/guides/" class="subNavDkLink" title="Guides">Guides</a></li>
                     <li><a class="subNavDkLink" href="/advice/parents/" title="Parents">Parents</a></li>
                  </ul>
               </div>
            </li>
         </ul>
      </nav>
   </div>
   <jsp:include page="/jsp/clearing/clearingSwitchPod.jsp">
      <jsp:param name="podType" value="covid_dsk"/>
   </jsp:include>
   <c:if test="${COVID19_SYSVAR eq 'ON' and parentCategory ne 'coronavirus'}">
      <div class="covid_ltms covid_dsk">
         <span class="covid_txt">University course intakes may be affected by Coronavirus (COVID-19)</span>
         <a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="${covid_wht_arw }"></a>
      </div>
   </c:if>
   
   <jsp:include page="/jsp/common/wuIncludeUserTimeLinePod.jsp"/>
   
   <jsp:include page="/jsp/common/mobileSearchPod.jsp"/>
   
   <c:if test="${COVID19_SYSVAR eq 'ON' and parentCategory ne 'coronavirus'}">
      <div class="covid_ltms covid_mob">
         <span class="covid_txt">University course intakes may be affected by Coronavirus (COVID-19)</span>
         <a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="${covid_wht_arw }"></a>
      </div>
   </c:if>
   <jsp:include page="/jsp/clearing/clearingSwitchPod.jsp">
      <jsp:param name="podType" value="covid_mob"/>
   </jsp:include>
   <input type="hidden" name="gaAccount" value="UA-22939628-2" id="gaAccount"/>
   <input type="hidden" id="contextJsPath" value="${contextJsPath }" />
   <input type="hidden" id="clearingYear" value="Clearing ${clearingYear }" />
   <input type="hidden" id="clearingonoff" value="${clearingOnOff}" />
   <input type="hidden" name="loginFrom" id="loginFrom" value="N"/>
   <input type="hidden" id="hide_Menu" value="N"/>
</div>