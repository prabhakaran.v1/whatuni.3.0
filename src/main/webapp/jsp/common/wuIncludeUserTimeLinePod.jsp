<%@ page import="WUI.utilities.CommonUtil"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
 
<%
  String getTimeLinePagename   = "";
  if(pageContext.getAttribute("pagename3") != null){
    getTimeLinePagename = (String)pageContext.getAttribute("pagename3");
  }else{
    getTimeLinePagename = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1,request.getRequestURI().length());          
  }
%>
<c:if test="${not empty requestScope.userTimeLinePage}">  
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/wu_userTimeLinePod_310320.js"></script>
</c:if>

<c:if test="${empty requestScope.userTimeLinePage}">         
  <c:if test="${not empty requestScope.userTimeLinePodList}">    
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/wu_userTimeLinePod_310320.js"></script>     
    <jsp:include page="/jsp/home/include/userTimeLinePod.jsp"/>  
    <%-- 
    <logic:notEmpty name="showResponsiveTimeLine" scope="request">
      <jsp:include page="/jsp/home/include/userTimeLinePodResponsive.jsp"/>
    </logic:notEmpty>      
    --%>
  </c:if>
  <%if(!("qlBasicForm.jsp".equalsIgnoreCase(getTimeLinePagename) || "qlAdvanceForm.jsp".equalsIgnoreCase(getTimeLinePagename) || "offerSuccess.jsp".equalsIgnoreCase(getTimeLinePagename) || "whatuniGoLandingPage.jsp".equalsIgnoreCase(getTimeLinePagename) || "subjectLandingPage.jsp".equalsIgnoreCase(getTimeLinePagename) || "gradeFilterLandingPage.jsp".equalsIgnoreCase(getTimeLinePagename) || "applicationPage.jsp".equalsIgnoreCase(getTimeLinePagename)) || "newUserGradeLandingPage.jsp".equalsIgnoreCase(getTimeLinePagename)){%>
    <c:if  test="${empty sessionScope.userInfoList}">
      <script type="text/javascript">  
        loadNonLoggedTimeline();
        function hideNonLoggedTimeline() {
          var $ntlh = jQuery.noConflict();
          jQuery.noConflict();      
          var className = $ntlh('.clipart').attr('class');
          if(className=="md clipart"){     
            $ntlh('body').removeClass("md non_log").addClass("sl non_log");
            $ntlh(".clipart").removeClass(className).addClass("sl clipart");   
            $ntlh("#nonloggedtimeline").slideUp(1000);      
            $ntlh('#nonloggedtimeline').hide();  
          }else if(className=="sl clipart"){
            $ntlh("#nonloggedtimeline").slideDown(1000);
            $ntlh('body').removeClass("sl non_log").addClass("md non_log");
            $ntlh(".clipart").removeClass(className).addClass("md clipart");   
            $ntlh('#nonloggedtimeline').show();  
          }        
        }    
        function loadNonLoggedTimeline() {
          var $nlg = jQuery.noConflict();
          jQuery.noConflict(); 
          var className = $nlg('.clipart').attr('class');
          $nlg(".clipart").prepend("<div class=\"large\"></div>");
          $nlg('body').addClass("md non_log");  
          $nlg(".clipart").removeClass(className).addClass("md clipart");
          <c:if  test="${'newUser.jsp' eq pagename3}">
           $nlg("#homeHeadId").addClass("z3");
          </c:if>
        }
      </script>
      <section class="timeline">
        <a href="javascript:hideNonLoggedTimeline()" class="tab_arw" id="hideTimeLine"></a>
        <div class="content-bg" id="nonloggedtimeline" onclick="javaScript:showLightBoxLoginForm('popup-newlogin',650,500, 'nonLoggedTimeline','','non-loggedin-timeline');">
          <div class="row-fluid">
            <div class="container">
              <article class="tml fl"></article>
            </div>
          </div>
        </div>
      </section>        
    </c:if>    
  <%}%>
</c:if>