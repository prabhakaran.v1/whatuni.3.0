<%--
  * @purpose:  This jsp is included to add basket js.
  * Change Log
  * *************************************************************************************************************************
  * Date             Name                      Ver.     Changes desc          Rel Ver.
  * 13_June_2015     Thiyagu G                 1.0      First draft           wu_565
  * *************************************************************************************************************************
--%>
<%@page import="WUI.utilities.CommonUtil" %>
<%String addBasketJs = CommonUtil.getResourceMessage("wuni.add.basket.js", null);%>
<script type="text/javascript" language="javascript">
    function $$D(id){return document.getElementById(id)}    
    function dynamicJSLoad(src){//Added to load JS file dynamically on 13_Jun_2017, By Thiyagu G.
      var script = document.createElement("script");
      script.type = "text/javascript";  
      script.src = src;
      if (typeof script != "undefined"){
        document.getElementsByTagName("head")[0].appendChild(script);
      }
    }
    var bas = jQuery.noConflict();
    bas(document).ready(function(){      
      adjustBasketJSLoad();
    });    
    adjustBasketJSLoad();
    function jqueryWidth() {
      return bas(this).width();
    }
    function adjustBasketJSLoad(){
      var screenWidth = document.documentElement.clientWidth;
      var filePath = '';
      var conPath = ($$D("contextJsPath") && $$D("contextJsPath").value != null) ? $$D("contextJsPath").value : "/degrees";
      if (screenWidth <= 480) {
        filePath = '<%=CommonUtil.getJsPath()%>/js/wu_addbaskets_171219.js';
        var filePath1 = '<%=CommonUtil.getJsPath()%>/js/<%=addBasketJs%>';
        bas('script').each(function() {    
            if (this.src.indexOf(filePath1) > -1) {
              this.parentNode.removeChild( this );
            }
        });        
      } else if ((screenWidth > 480) && (screenWidth <= 992)) {
        filePath = '<%=CommonUtil.getJsPath()%>/js/<%=addBasketJs%>';
        var filePath1 = '<%=CommonUtil.getJsPath()%>/js/wu_addbaskets_171219.js';
        bas('script').each(function() {    
            if (this.src.indexOf(filePath1) > -1) {
              this.parentNode.removeChild( this );
            }
        });
      } else {
        filePath = '<%=CommonUtil.getJsPath()%>/js/<%=addBasketJs%>';
        var filePath1 = '<%=CommonUtil.getJsPath()%>/js/wu_addbaskets_171219.js';
        bas('script').each(function() {    
            if (this.src.indexOf(filePath1) > -1) {
              this.parentNode.removeChild( this );
            }
        });
      }      
      if (bas('head script[src="' + filePath + '"]').length > 0){        
      }else{
        if(filePath != '' && !(filePath.indexOf("/degrees/js/wu_addbasket_") > -1)){          
          dynamicJSLoad(filePath);
          var fPath = '/degrees/js/<%=addBasketJs%>';
          bas('script').each(function() {    
              if (this.src.indexOf(fPath) > -1) {
                this.parentNode.removeChild( this );
              }
          });
        }        
      }
    }
    bas(window).on('orientationchange', orientationChangeHandlerBasket);
    function orientationChangeHandlerBasket(e) {
      setTimeout(function() {
        bas(window).trigger('resize');  
        adjustBasketJSLoad();
      }, 500);
    }
    bas(window).resize(function() {});
</script>