<%@page import="WUI.utilities.CommonUtil, com.wuni.util.seo.SeoPageNamesAndFlags, WUI.utilities.GlobalConstants, com.layer.util.SpringConstants"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%
String emailDomainJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.email.domain.js");
String facebookLoginJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.facebook.login.js");
String commonUserProfileJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.user.profile.js");
String canonicalUrl = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + SpringConstants.PRE_CLEARING_LANDING_URL;
String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
String wuniMainHeaderCss = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);
String wuniMobileStyleCss = CommonUtil.getResourceMessage("wuni.whatuni.mobile.styles.css", null); 
String wuniCmmMainStyleCss = CommonUtil.getResourceMessage("wuni.cmm.main.style.css", null);

%>

<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<jsp:include page="/jsp/common/includeIconImg.jsp"/>
<jsp:include page="/include/seoTitle.jsp">
  <jsp:param name="collegeId" value="0"/>
  <jsp:param name="pageName" value="<%=SeoPageNamesAndFlags.LEAD_CAPTURE_NAME%>" /> 
  <jsp:param name="paramFlag" value="<%=SeoPageNamesAndFlags.LEAD_CAPTURE_FLAG%>"/>  
  <jsp:param name="noindexfollow" value="noindex,follow"/>
</jsp:include>
<link rel="canonical" href="<%=canonicalUrl%>"/>
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuniMainHeaderCss%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuniCmmMainStyleCss%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=wuniMobileStyleCss%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/ebook_mobile_styles_31032020.css" media="screen" />
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<script type="text/javascript" language="javascript" src="//connect.facebook.net/en_US/all.js"> </script>
<script type="text/javascript">var dev = jQuery.noConflict();</script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=facebookLoginJSName%>"> </script>  
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=commonUserProfileJSName%>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/emaildomain/<%=emailDomainJs%>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
<jsp:include page="/jsp/common/abTesting.jsp"/>
<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
  <jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>
