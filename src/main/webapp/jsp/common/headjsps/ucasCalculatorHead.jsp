  <%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants" %>  
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
    <%@include  file="/jsp/ucascalculator/include/includeUcasCSS.jsp"%>
    <link id="size-stylesheet" rel="stylesheet" type="text/css" media="screen"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,700italic" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <%
      String widgetDrawDoughnutJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.widget.draw.doughnut.js");
      String ajaxDynamicListJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.ajaxDynamicList.js");
      String cpeStatsLogJsName = CommonUtil.getResourceMessage("wuni.cpe.stats.log.js", null);  
      
    %>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/DonutChart/<%=widgetDrawDoughnutJSName%>"></script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/ucascalculator/wu_ucascalculator_20210119.js"></script>   
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/ajax_wu564.js"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/<%=ajaxDynamicListJSName%>"> </script>  
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=cpeStatsLogJsName%>"> </script>
    <%@include file="/jsp/ucascalculator/include/includeUcasCalcResponsive.jsp" %>
    <%
      String enName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");   
      String domainPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
      String logoPath="";
      if(("LIVE").equals(enName)){
        logoPath = CommonUtil.getCSSPath()+"/images/widget/wu_widlgo.png";
      } else if("TEST".equals(enName)){
        logoPath = domainPath+"/wu-cont/images/widget/wu_widlgo.png";
      } else if("DEV".equals(enName)){
        logoPath = domainPath+"/wu-cont/images/widget/wu_widlgo.png";
      }
      String yearSelected = GlobalConstants.UCAS_LOAD_YEAR_VALUE;
    %>  
  <link rel="canonical" href="<%=domainPath%>/degrees/ucas-calculator.html"/>
  <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="0"/>
      <jsp:param name="pageName" value="UCAS TARIFF POINTS CALCULATOR"/>
      <jsp:param name="paramFlag" value="UCAS_TARIFF_POINTS_CALCULATOR" />
      <jsp:param name="studyLevelId" value="M" />      
      <jsp:param name="noindexfollow" value="index,follow" />  
  </jsp:include>