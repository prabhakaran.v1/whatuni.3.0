<%@page import="WUI.utilities.CommonFunction, WUI.utilities.CommonUtil,WUI.utilities.GlobalFunction,org.apache.commons.validator.GenericValidator" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%
 String commonCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.css");
    String homeCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.homepage.css");
    String mainHeaderCSS = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);//Added this css for Header changes by Hema.S on 23_OCT_2018_REL
    String flexSliderCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.flexslider.css");
    
 %>
  <title>Not Found</title>
  <meta name="ROBOTS" content="noindex,follow"/> 
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">  
   
  <%--@include  file="/jsp/common/includeMainCSS.jsp"--%> 
  <jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
   <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=mainHeaderCSS%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=commonCssName%>" media="screen" />  
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=homeCssName%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=flexSliderCssName%>" media="screen" />
  
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>  
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>    
  <jsp:include  page="/notfound/include/includeErrorResponsive.jsp"/>     
