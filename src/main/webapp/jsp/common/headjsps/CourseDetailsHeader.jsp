<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%@page import=" WUI.utilities.CommonUtil, WUI.utilities.SessionData, WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction" %>

<%
  String domainSpecificPath = new CommonFunction().getSchemeName(request)+ CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
  String main_480_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.480.css", null);
  String main_992_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.992.css", null);
  String courseDetailsJsName = CommonUtil.getResourceMessage("wuni.courseDetailsJsName.js", null);
  String gradeFilterUcasJs = CommonUtil.getResourceMessage("wuni.ucas.grader.filter.js", null);
  String printCssName = CommonUtil.getResourceMessage("wuni.whatuni.print.css", null);
  String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
  String clearingonoff    =  new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");    
  pageContext.setAttribute("clearingonoff", clearingonoff);
  //
  String paramCourseId = request.getParameter("w") !=null && request.getParameter("w").trim().length()>0 ? request.getParameter("w") : request.getParameter("cid") !=null && request.getParameter("cid").trim().length()>0 ? request.getParameter("cid") : ""; 
  paramCourseId = paramCourseId!=null && paramCourseId.trim().length()>0 ? "" : String.valueOf(request.getAttribute("seoCourseId"));
  //
  String paramCollegeId = request.getParameter("z") !=null && request.getParameter("z").trim().length()>0 ? request.getParameter("z") : "";
  paramCollegeId = paramCollegeId!=null && paramCollegeId.trim().length()>0 ? "" : String.valueOf(request.getAttribute("seoCollegeId"));
  //
  String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");    
  if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0){ 
  cpeQualificationNetworkId = "2";
  }
  request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);
  //
  String bc_breadCrumb = request.getAttribute("breadCrumb") != null && request.getAttribute("breadCrumb").toString().trim().length() > 0 ? request.getAttribute("breadCrumb").toString() : "";
  request.setAttribute("uniLanding", "YES");
  String noindexfollow = "noindex,follow";
  boolean isClearingCourse = (((String)request.getAttribute("CLEARING_COURSE")) != null && "TRUE".equals((String)request.getAttribute("CLEARING_COURSE"))) ? true : false;  
  if(request.getAttribute("crsDetailIndex")!=null && "true".equals(request.getAttribute("crsDetailIndex"))){
    noindexfollow = "index,follow";
  }
  //modified the condition to avoid the clearing course index for june_5_18 release by sangeeth.s
  if(isClearingCourse){
    noindexfollow = "noindex,follow";
  }
  String clearingUserType = (String)session.getAttribute("USER_TYPE") ;
  String searchValue = "Degrees";
  String cdPageClassName = "clr_cd_cnt";
  //
  String evntCourseTitle = request.getAttribute("hitbox_course_title") != null && request.getAttribute("hitbox_course_title").toString().trim().length() > 0 ? request.getAttribute("hitbox_course_title").toString() : "";
  String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null); 
  String loadNewMapJs = CommonUtil.getResourceMessage("wuni.load.new.map.js", null);  //Added by Sangeeth.S for mapbox in FEB_12_19 rl
  
%>

<meta name="format-detection" content="telephone=no"/>
    <%if(isClearingCourse){%>
     <jsp:include page="/include/seoTitle.jsp">
        <jsp:param name="collegeId" value="<%=paramCollegeId%>"/>
        <jsp:param name="courseId" value="<%=paramCourseId%>"/>       
        <jsp:param name="paramFlag" value="CLEARING COURSE DETAILS"/>
        <jsp:param name="pageName" value="CLEARING COURSE DETAILS"/>
        <jsp:param name="noindexfollow" value="<%=noindexfollow%>"/>
    </jsp:include> 
    <%}else{%>
      <jsp:include page="/include/seoTitle.jsp">
        <jsp:param name="collegeId" value="<%=paramCollegeId%>"/>
        <jsp:param name="courseId" value="<%=paramCourseId%>"/>       
        <jsp:param name="paramFlag" value="COURSE_DETAIL"/>
        <jsp:param name="pageName" value="COURSE DETAILS"/>
        <jsp:param name="noindexfollow" value="<%=noindexfollow%>"/>
    </jsp:include> 
    <%}%>
    <%if(!isClearingCourse){%>
      <c:if test="${not empty requestScope.canonicalUrl}"><%--Added by Sangeeth.S for 25_SEP_18 rel--%>
         <link rel="canonical" href="${requestScope.canonicalUrl}"/>
      </c:if>
     <%}%>
    <jsp:include page="/jsp/common/includeMainCSS.jsp" />  
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
    <%String reviewlightboxJs = CommonUtil.getResourceMessage("wuni.reviewlightbox.js", null);%>       
   <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/review/<%=reviewlightboxJs%>"></script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=loadNewMapJs%>"></script>
		<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=printCssName%>">
    <%-- ::START:: Yogeswari :: 30.06.2015 :: changed for clearing/course detail page responsive task --%>
    <!--Added script for displaying sticky buttons Jan-27-16-->
    <c:if test="${not empty clearingonoff and clearingonoff eq 'ON'}">
      <script type="text/javascript" language="javascript" id="gradeFilterUcasJsId" src="<%=CommonUtil.getJsPath()%>/js/clearing/<%=gradeFilterUcasJs%>"></script>
    </c:if>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
    <script type="text/javascript" language="javascript">
      var flexslider;
      var dev = jQuery.noConflict();
      dev(document).ready(function(){
       adjustStyle();
      });
      
      adjustStyle();
      function jqueryWidth() {
        return dev(this).width();
      } 
  
      function adjustStyle() {
        var width = document.documentElement.clientWidth;
        var path = ""; 
        if (width <= 480) {
          if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }        
        <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}%>         
        } else if ((width > 480) && (width <= 992)) {
          if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
          }
          <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
          <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
          <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
          <%}%>
        } else {
          if (dev("#viewport").length > 0) {
            dev("#viewport").remove();
          }   
          dev(".btn_cntrl .more_info").css("display","none");
          document.getElementById('size-stylesheet').href = "";
          dev(".hm_srchbx").hide();
        }
    }
    dev(window).on('orientationchange', orientationChangeHandler);
    function orientationChangeHandler(e) {
      setTimeout(function() {
        dev(window).trigger('resize');
      }, 500);
    }
    dev(window).resize(function() {
      adjustStyle();
      var width = document.documentElement.clientWidth;
      if (!dev("#addTofnchTickerList").is(":visible")){
       if(dev("#cookie-lightbox") && !dev("#cookie-lightbox").is(":visible")){
        stickyButton(); 
       }
      } 
      if(width > 992){dev(".btn_intr .more_info").removeAttr("style");}
      if(flexslider!=undefined) {           
        flexslider.vars.minItems = getArticleWuGridSize();
        flexslider.vars.maxItems = getArticleWuGridSize();
        flexslider.slides.width( flexslider.computedW);
        flexslider.update(flexslider.pagingCount);
        flexslider.setProps();
      }
    });
    function jqueryWidth() {
      return dev(this).width();
    }
    //Added by Indumathi.S for Sticky button Jan-27-16
dev(document).ready(function(){
  dev(".btn_intr .more_info").click(function(){
   dev(".btn_cntrl .btns_interaction").removeClass("btn_vis");
   dev(".btn_cntrl .more_info").css("display","none");
   if(checkChildDiv("buttonCls")){
    dev("#socialBoxMin").addClass("intr_sb_pos1");
    dev("#needHelpDiv").addClass("intr_sb_pos1"); // For chatbot sticky 25-09-2018
   }
  });
  dev(".int_cl").click(function(){
   dev('.btn_intr').addClass('btn_cntrl');
   dev(".btn_intr .btns_interaction").addClass("btn_vis");
   dev(".btn_cntrl .more_info").css("display","block");
   dev("#socialBoxMin").removeClass("intr_sb_pos1"); 
   dev("#needHelpDiv").removeClass("intr_sb_pos1"); // For chatbot sticky 25-09-2018
  });
  dev(".f5_clse_btn").click(function(){
    stickyButton();
  }); 
  dev(".cook_cls").click(function(){
   stickyButton();   
  });
  if (!dev("#addTofnchTickerList").is(":visible")){
   if(dev("#cookie-lightbox") && !dev("#cookie-lightbox").is(":visible")){
    stickyButton(); 
   }
  } 
});
function stickyButton() {
 var width = document.documentElement.clientWidth;
 var divPosition;
 if ((width >= 320) && (width <= 567)) {
   divPosition = 279;
 }
 if ((width >= 568) && (width <= 992)) {
   divPosition = 200; 
 }
 if ((width >= 320) && (width <= 992)) {
  dev(window).scroll(function(){  
   width = document.documentElement.clientWidth;
   if (dev(this).scrollTop() > divPosition) {  
    var windowTop = dev(window).scrollTop(); 
    dev('.btn_intr').addClass('btn_cntrl');
    dev(".btn_intr .more_info").css("display","block");
    dev(".btn_intr .btns_interaction").addClass("btn_vis");
    dev("#socialBoxMin").addClass("intr_sb_pos");
    dev("#socialBoxMin").removeClass("intr_sb_pos1");
    // For chatbot sticky 25-09-2018
    dev("#needHelpDiv").addClass("intr_sb_pos");
    dev("#needHelpDiv").removeClass("intr_sb_pos1");
     if(width > 992){
      dev(".btn_intr .more_info").removeAttr("style");
      dev(".btn_intr .more_info").css("display","none");
     }
   }else {
    dev('.btn_intr').removeClass('btn_cntrl');
    dev(".btn_intr .more_info").removeAttr("style");
    dev(".btn_intr .more_info").css("display","none");
    dev(".btn_intr .btns_interaction").addClass("btn_vis");
    dev("#socialBoxMin").removeClass("intr_sb_pos");
    dev("#socialBoxMin").removeClass("intr_sb_pos1");
    // For chatbot sticky 25-09-2018
    dev("#needHelpDiv").removeClass("intr_sb_pos");
    dev("#needHelpDiv").removeClass("intr_sb_pos1");
   }
  });
 }
}
  </script>
  <%-- ::END:: Yogeswari :: 30.06.2015 :: changed for clearing/course detail page responsive task --%>
  <%--Course specific schema code added by Prabha on 24_Jan_2017--%>
  <%if(!GenericValidator.isBlankOrNull(paramCourseId) && !GenericValidator.isBlankOrNull(paramCollegeId)){
    //Added clearing flag for course title changes on 16_May_2017, By Thiyagu G.
    String clearingFlag = "N";
    if(isClearingCourse){clearingFlag = "Y";}
    String schema = new CommonFunction().getCourseSchemaList(paramCourseId, paramCollegeId, clearingFlag);
    if(!GenericValidator.isBlankOrNull(schema)){
  %>
  <jsp:include page="/jsp/search/includeCdPage/courseSpecificSchema.jsp">
    <jsp:param name="schemaTag" value="<%=schema%>"/>
  </jsp:include>
  <%}
  }%>
  <%--End of course specific schema--%>