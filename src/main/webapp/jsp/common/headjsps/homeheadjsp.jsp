  <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction"%>

<%String socialBoxJs = CommonUtil.getResourceMessage("wuni.social.box.js", null);
  %>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">  
  <% // logged scenario need to  hide %>
  	<%String commonVideoJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.commonVideoJsName.js");%>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/videoplayer/<%=commonVideoJSName%>"></script>    
  
  <jsp:include page="/jsp/home/include/includeHeader.jsp"/>
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>    
  <jsp:include  page="/jsp/home/include/includeHomePageResponsive.jsp"/>
  <link rel="canonical" href="<%=new CommonFunction().getSchemeName(request)%>www.whatuni.com"/> 
  <jsp:include  page="/jsp/common/includeSchemaTag.jsp">
    <jsp:param name="pageName" value="HOME_PAGE_SCHEMA"/>
  </jsp:include>
      
