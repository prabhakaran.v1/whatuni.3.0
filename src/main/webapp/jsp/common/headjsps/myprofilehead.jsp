<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction"%>
<%@include  file="/include/htmlTitle.jsp" %>
<%@ include file="/jsp/common/includeMainCSS.jsp"%>        
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set> 
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<%if("uploadedImage.jsp".equalsIgnoreCase(pagename3)){%>
  <script src="<wu:jspath source='/js/cropping/'/><spring:message code='wuni.jquery.Jcrop.js'/>"></script>
  <link rel="stylesheet" href="<%=CommonUtil.getJsPath()%>/cssstyles/jquery.Jcrop.min.css" type="text/css" />
<%}
      String myWhatuniJsName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.myWhatuniJsName.js");
      String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
      String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
      String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
      String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
      String srGradeFilterJsName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.sr.gradefilter.js");
      if(!GenericValidator.isBlankOrNull(pageContext.getAttribute("pagename3").toString())){
    	  pagename3 = (String)pageContext.getAttribute("pagename3");
      }else{
    	  pagename3 = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/")+1,request.getRequestURI().length());
      }
    %>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=myWhatuniJsName%>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=srGradeFilterJsName%>"> </script>
<script type="text/javascript" language="javascript">
var dev = jQuery.noConflict();
dev(document).ready(function(){
adjustStyle();
});
adjustStyle();
function jqueryWidth() {
  return dev(this).width();
} 
function adjustStyle() {
    var width = document.documentElement.clientWidth;
    var path = ""; 
    if (width <= 480) {
	    if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }        
        <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}%>         
    } else if ((width > 480) && (width <= 992)) {         
	   if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }       
       <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}%>
       
    } else {
        if (dev("#viewport").length > 0) {
            dev("#viewport").remove();
        }       
        document.getElementById('size-stylesheet').href = "";        
        dev(".hm_srchbx").hide();
    }
}
dev(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
    setTimeout(function() {
        dev(window).trigger('resize');
    }, 500);
}
dev(window).resize(function() { 
  adjustStyle();  
});
function jqueryWidth() {
    return dev(this).width();
}

</script><!--Added script for sticky submit button Jan-21-16 Indumathi.S-->