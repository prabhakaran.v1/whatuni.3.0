<%@page import="com.wuni.util.seo.SeoUrls"%>
<%@page import="WUI.utilities.GlobalConstants"%>
<%@page import="com.wuni.util.seo.SeoPageNamesAndFlags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
String collegeId = request.getAttribute("COLLEGE_ID") != null ? (String)request.getAttribute("COLLEGE_ID") : "";
String requestUrl = request.getAttribute("REQUEST_URL") != null ? (String)request.getAttribute("REQUEST_URL") : "";
String collegeName = request.getAttribute("COLLEGE_NAME") != null ? (String)request.getAttribute("COLLEGE_NAME") : "";
String canonicalUrl = GlobalConstants.WHATUNI_SCHEME_NAME+GlobalConstants.WHATUNI_DOMAIN+ new SeoUrls().construnctUniHomeURL(collegeId, collegeName);
String enquiryExistFlag = request.getAttribute("enquiryExistFlag") != null ? (String)request.getAttribute("enquiryExistFlag") : "";
String exitOpenDayRes = "" , odEvntAction = "false";
    if(session.getAttribute("exitOpRes")!=null && !"".equals(session.getAttribute("exitOpRes"))){
      exitOpenDayRes = (String)session.getAttribute("exitOpRes");
    }    
    if(!"".equals(exitOpenDayRes) && "true".equals(exitOpenDayRes)){
      odEvntAction = "true";
    }
    String seoPageName = SeoPageNamesAndFlags.UNI_LANDING_PAGE_NAME;
    String seoParamFlag = SeoPageNamesAndFlags.UNI_LANDING_PAGE_FLAG;
    String clearingFlag = request.getAttribute("USER_JOURNEY_FLAG") != null ? (String)request.getAttribute("USER_JOURNEY_FLAG") : "";   
      if("CLEARING".equalsIgnoreCase(clearingFlag)){
        seoPageName = "CLEARING PROFILE";
        seoParamFlag = "CLEARING PROFILE";
      }
%>

    <meta content="text/html;charset=utf-8" http-equiv="Content-Type"></meta>
    <meta content="utf-8" http-equiv="encoding"></meta>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"></meta>
    <meta id="viewport" name="viewport"
          content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;"></meta>
    <jsp:include page="/jsp/common/includeIconImg.jsp"/>
    <jsp:include page="/jsp/common/includeSeoMetaDetails.jsp">
      <jsp:param value="index,follow" name="noindexfollow"/>
    </jsp:include> 
    <link rel="canonical" href="<%=canonicalUrl%>"/>
    <jsp:include page="/jsp/thirdpartytools/include/includeGoogleTagJs.jsp"/>
    <jsp:include page="/jsp/thirdpartytools/include/createTargetingAttr.jsp"/>
    <jsp:include page="/contenthub/include/includeCSS.jsp"/>
    <jsp:include page="/contenthub/include/includeJS.jsp"/>
    <jsp:include page="/jsp/common/includeSchemaTag.jsp">
      <jsp:param name="pageName" value="UNI_RICH_PROFILE_SCHEMA"/>
    </jsp:include>
    <c:if test="${not empty requestScope.listOfOpendayInfo}">
      <jsp:include page="/jsp/common/includeSchemaTag.jsp">
        <jsp:param name="pageName" value="RICH_PROFILE_OPENDAY_SCHEMA"/>
      </jsp:include>
    </c:if>