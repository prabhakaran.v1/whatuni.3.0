<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,WUI.utilities.CommonFunction"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>

  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<%
  String urlmapping = request.getAttribute("urlmapping")!=null ? request.getAttribute("urlmapping").toString() : ""; 
  String odStatusPos = request.getAttribute("odStatusPos")!=null ? request.getAttribute("odStatusPos").toString() : "0";
  String totalPos = request.getAttribute("totalPos")!=null ? request.getAttribute("totalPos").toString() : "0";
  CommonUtil util = new CommonUtil();
  CommonFunction common = new CommonFunction();
  String eVName = common.getWUSysVarValue("WU_ENV_NAME");
  String httpStr = common.getSchemeName(request);
  String canonUrl = httpStr + "www.whatuni.com"+urlmapping+"/";
  String totalRecordCount = (String)request.getAttribute("totalRecordCount");
       totalRecordCount = (GenericValidator.isBlankOrNull(totalRecordCount))? "":totalRecordCount;
       String collegeName = (String)request.getAttribute("odcollegeName");  
       collegeName = (!GenericValidator.isBlankOrNull(collegeName))? collegeName: "";
  String location = request.getParameter("location");
         location = !GenericValidator.isBlankOrNull(location)?  util.toTitleCase(common.replaceHypenWithSpace(location)) : ""; 
  String collegeId = (String)request.getParameter("collegeid");  
         collegeId = (!GenericValidator.isBlankOrNull(collegeId))? collegeId: "0";
  String date = (String)request.getParameter("date");  
         date = (!GenericValidator.isBlankOrNull(date))? date: "";
  String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
  String openDayJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.opendaysearch.js");
  String openDayMainCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.styles.css", null);//getting css name dynamically by Sangeeth.S for 31_Jul_18 rel
  String openDayHeaderCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);  
  String domSpecificPath = common.getSchemeName(request)+ CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
  String openDayLandingJsName = CommonUtil.getResourceMessage("wuni.openday.landing.js", null);//getting Js name dynamically by Sangeeth.S for 20_Nov_18 rel
  
%>
  <%--Meta detail section start--%>
  <jsp:include page="/include/seoTitle.jsp">
    <jsp:param name="collegeId" value="<%=collegeId%>"/>
    <jsp:param name="locationCountyId" value="<%=location%>"/>
    <jsp:param name="paramFlag" value="WUNI_OPEN_DAYS"/>
    <jsp:param name="pageName" value="WU OPEN DAYS SEARCH"/>
    <jsp:param name="entityText" value="<%=date%>"/>
    <jsp:param name="noindexfollow" value="noindex,follow"/>
  </jsp:include>
  <%--Meta detail section end--%>
  <%
  if(!GenericValidator.isBlankOrNull(location)){
    if("south east england".equalsIgnoreCase(location.trim())){
      location = "the South East of England";
    }else if("south west england".equalsIgnoreCase(location.trim())){
      location = "the South West of England";
    }else if("eastern england".equalsIgnoreCase(location.trim())){
      location = "the East of England";
    }else if("northern england".equalsIgnoreCase(location.trim())){
      location = "the North of England";
    }
  }
  %>
  <%--Included css, js and images section--%>
  <jsp:include page="/jsp/common/includeMainCSS.jsp"/>   
  <link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=openDayHeaderCssName%>" media="screen">
  <link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=openDayMainCssName%>" media="screen">   
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>  
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
  <%@include  file="/jsp/openday/include/includeOpenDayResponsive.jsp" %>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/openday/<%=openDayJSName%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/openday/<%=openDayLandingJsName%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
  <%--End of including css, js and images section--%>
