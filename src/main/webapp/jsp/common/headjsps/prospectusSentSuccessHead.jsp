 <!--Changed new responsive design for 03-Nov-2015, by Thiyagu G
        Changed static js name to dynamic and created object creation common for old codes by Sangeeth.S Jul_3_18 rel
    -->
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
    <%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
    <%@include  file="/include/htmlTitle.jsp" %>   
    <%@include  file="/jsp/common/includeMainCSS.jsp" %>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>     
    
    <%
    CommonFunction comFun = new CommonFunction();   
    String envronmentName = comFun.getWUSysVarValue("WU_ENV_NAME");
    String domainSpecificPath = comFun.getSchemeName(request)+CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
    String main_480_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.480.css", null);
    String main_992_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.992.css", null);
    
    String poCourseId="0";
    String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null);
    
    %>
    
<script type="text/javascript" language="javascript">
  var dev = jQuery.noConflict();
  dev(document).ready(function(){
    adjustStyle();
  });
  adjustStyle();
  function loadLazyJS(){
    if(!document.getElementById('lazyload')){
      var file=document.createElement('script');
      file.setAttribute("type","text/javascript");
      file.setAttribute("id", "lazyload");
      file.setAttribute("src", "<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>");
      document.getElementsByTagName("head")[0].appendChild(file);
    }
  }
  function jqueryWidth() {
    return dev(this).width();
  } 
  function adjustStyle() {
    var width = document.documentElement.clientWidth;
    var path = "";     
    if (width <= 480) {
    loadLazyJS();
      if (dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }
      <%if(("LIVE").equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}%>
      dev('.img_ppn img').addClass('lazy-load');
    } else if ((width > 480) && (width <= 992)) { 
    loadLazyJS();
      if (dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      } 
      <%if(("LIVE").equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}%>
      dev('#prospectusKwd').css('width', '100%').css('width', '-=18px');
      dev('.img_ppn img').addClass('lazy-load');      
    }else {
    loadLazyJS();    
      if (dev("#viewport").length > 0) {
        dev("#viewport").remove();
      } 
      document.getElementById('size-stylesheet').href = ""; 
      dev('.img_ppn img').removeAttr('class');
      dev(".hm_srchbx").hide();
    }
  }
  dev(window).on('orientationchange', orientationChangeHandler);
  function orientationChangeHandler(e) {
    setTimeout(function() {
      dev(window).trigger('resize');
    }, 500);
  }
  dev(window).resize(function() {    
    var screenWidth = jqueryWidth();
    var currentElement;
    if(screenWidth <= 480){  //Phone view
      setTimeout("dev('.slides').attr('style', 'marginLeft:0px;');",1000);
      dev('.c1').add('.c2').add('.c3').add('.c4').removeAttr('style');
    }else if ((screenWidth > 480) && (screenWidth <= 992)){  //Tablet view
      dev('#articleSearchForm').find('.tx_bx').css('width', '100%').css('width', '-=40px');
      dev('.gen_srchbox').css('width', '100%').css('width', '-=18px');
      dev('#prospectusKwd').css('width', '100%').css('width', '-=18px');
      setTimeout("dev('.slides').attr('style', 'marginLeft:0px;');",1100);
      if(navigator.userAgent.indexOf("Firefox") == -1){
        dev('.c1').add('.c2').add('.c3').add('.c4').attr("style", "display:block;");
        setTimeout('dev(".c1").add(".c2").add(".c3").add(".c4").attr("style", "display:table-cell;");',500);
      }
      
    }else if(screenWidth > 992){  //Desktop view
      dev('#articleSearchForm').find('.tx_bx').removeAttr("style");
      dev('.gen_srchbox').removeAttr("style");
      dev('.pros_search').find('.tx_bx').removeAttr("style");
      dev('.slides li').css({'width':'980px', 'float':'left', 'display': 'list-item'});
    }  
    adjustStyle();    
  if(document.documentElement.clientWidth <= 992){    
    var cnt = dev("div[id^='slider_']").length;    
    for(var i = 1; i <= cnt; i++){    
      dev('#mslider_'+i).html(dev('#slider_'+i).html());
      dev('#mslider_'+i).find('.slides').removeAttr("style").find('li').removeAttr("style");      
    }
    dev("div[id^='slider_']").css("display", "none");
    dev("a[id^='slideratag_']").css("display", "none");
    dev("a[id^='slideratag1_']").css("display", "none");
    dev("a[id^='selectAll_slider_']").css("display", "none");    
    dev("div[id^='mslider_']").css("width", "100%");
    dev('#mslider_1,#mslider_2').css("display","block");
    dev("i[id^='hideShow_mslider_']").removeClass('fa fa-minus-circle').addClass('fa fa-plus-circle');
    dev('#hideShow_mslider_1,#hideShow_mslider_2').removeClass('fa fa-plus-circle').addClass('fa fa-minus-circle');    
    dev('#selectAll_mslider_1,#selectAll_mslider_2').css("display", "block");
    dev("a[id^='mslideratag_']").css("display", "block");
    dev("a[id^='mslideratag1_']").css("display", "block");     
    dev("#hdr_menu").css("display", "none");
  }else{
    dev("div[id^='mslider_']").css("display", "none");
    dev("a[id^='mslideratag_']").css("display", "none");
    dev("a[id^='mslideratag1_']").css("display", "none");
    dev("a[id^='selectAll_mslider_']").css("display", "none");    
    dev('#slider_1,#slider_2').css("display","block");
    dev('#selectAll_slider_1,#selectAll_slider_2').css("display","block");
    dev('#hideShow_slider_1,#hideShow_slider_2').removeClass('fa fa-plus-circle').addClass('fa fa-minus-circle');    
    dev('#slider_2,#slider_2').css("display", "block");
    dev("a[id^='slideratag_']").css("display", "block");
    dev("a[id^='slideratag1_']").css("display", "block");    
    dev("#hdr_menu").css("display", "block");
    dev('.slides').css({"width" : "6900px", "margin-left" : "0px;"});
  }    
  });
  function jqueryWidth() {
    return dev(this).width();
  }
  
</script>