<%@page import=" WUI.utilities.CommonUtil, java.util.*, mobile.util.MobileUtils, org.apache.commons.validator.GenericValidator, WUI.profile.bean.ProfileBean" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,700italic' rel='stylesheet' type='text/css'>      
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/cb_styles_191119.css" media="screen" />
<%    
  ArrayList subQuestionDetails = request.getAttribute("subQuestionDetails") != null ? (ArrayList)request.getAttribute("subQuestionDetails") : null;
  boolean mobileFlag = new MobileUtils().userAgentCheck(request);
  String refererUrl = request.getHeader("referer");
  String jsPath = CommonUtil.getJsPath();
  String fromPlace = GenericValidator.isBlankOrNull(request.getParameter("from")) ? "false" : request.getParameter("from");
  String hideFooterIfPrepopulating = "style='display:block'";
  
%>
<script type="text/javascript" language="javascript" src="<%=jsPath%>/js/autoComplete/ajax_wu564.js"></script>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>
<script type="text/javascript" language="javascript" src="<%=jsPath%>/js/chatbot/wu_chatbot_040220.js"></script>
