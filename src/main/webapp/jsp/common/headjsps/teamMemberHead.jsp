<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
<%@ page import="WUI.utilities.GlobalConstants"%>
<c:set var="pagename3">
  <tiles:getAsString name="pagename3" ignore="true" />
</c:set>

  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">  
   
  <%@include  file="/jsp/common/includeMainCSS.jsp"%> 
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>  
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>  
  <jsp:include  page="/help/aboutus/includeStaticCntResponsive.jsp"/>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.staticpage.js'/>"></script>
  <%
    String memberName = "", getMetaTitle = "", canonicalUrl = "";
    if(request.getAttribute("dispTeamMember")!=null){
      memberName = (String)request.getAttribute("dispTeamMember");
    }    
    if(request.getAttribute("setParamFlag")!=null){
      getMetaTitle = (String)request.getAttribute("setParamFlag");
    }
    canonicalUrl = new CommonFunction().getSchemeName(request) + GlobalConstants.WHATUNI_DOMAIN +"/team/"+memberName+"/";
  %>
  <link rel="canonical" href="<%=canonicalUrl%>"/> 
  <jsp:include page="/include/seoTitle.jsp">    
    <jsp:param name="paramFlag" value="<%=getMetaTitle%>"/>
    <jsp:param name="pageName" value="<%=getMetaTitle%>"/>          
    <jsp:param name="collegeId" value="0"/>
    <jsp:param name="noindexfollow" value="noindex,follow"/>
  </jsp:include>          