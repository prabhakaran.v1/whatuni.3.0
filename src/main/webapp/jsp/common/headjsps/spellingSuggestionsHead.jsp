<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="WUI.utilities.CommonFunction, WUI.utilities.CommonUtil, com.wuni.util.seo.SeoUrls" %>

<% 
  String subjectname = (String) request.getAttribute("err_subject_name");
  subjectname = subjectname !=null && !subjectname.equalsIgnoreCase("null") && subjectname.trim().length()>0 ? new CommonUtil().toTitleCase(subjectname) : "";
  String studylevelid = (String) request.getAttribute("err_study_level_id");
  studylevelid = studylevelid !=null && !studylevelid.equalsIgnoreCase("null") && studylevelid.trim().length()>0 ? studylevelid : "";
  String location = (String) request.getAttribute("err_location");         
  location = location !=null && !location.equalsIgnoreCase("null") && location.trim().length()>0 ? new CommonUtil().toTitleCase(new CommonFunction().replaceHypenWithSpace(location.trim())) : "";
  String collegeId = (String) request.getAttribute("collegeId");
  collegeId = collegeId != null && collegeId.trim().length() > 0? collegeId : "0";        
  String noindexfollow = request.getAttribute("noindex") !=null ? "TRUE"  : "FALSE";   
  noindexfollow = request.getAttribute("unihomefrmsearch") !=null ? "noindex,nofollow" : noindexfollow;
  noindexfollow = request.getAttribute("otherindex") !=null ? String.valueOf(request.getAttribute("otherindex")) : noindexfollow;
  String categoryCode = request.getAttribute("categoryCode") != null ? request.getAttribute("categoryCode").toString() : "";    
%>

  <c:if test="${not empty requestScope.providerResult}">
    <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="<%=collegeId%>"/>
      <jsp:param name="categoryCode" value="<%=categoryCode%>"/>
      <jsp:param name="studyLevelId" value="<%=studylevelid%>"/>
      <jsp:param name="searchKeyword" value="<%=subjectname%>"/>
      <jsp:param name="paramFlag" value="COURSE_RESULTS_NOT_FOUND"/>
      <jsp:param name="pageName" value="COURSE RESULTS NOT FOUND"/>
      <jsp:param name="noindexfollow" value="<%=noindexfollow%>"/>
      <jsp:param name="locationCountyId" value="<%=location%>"/>
    </jsp:include> 
  </c:if>
  <c:if test="${empty requestScope.providerResult}">
    <jsp:include page="/include/htmlTitle.jsp">   
      <jsp:param name="indexFollowFlag" value="noindex,follow" />
    </jsp:include>
  </c:if>
  <jsp:include page="/jsp/common/includeCSS.jsp"></jsp:include>
  <jsp:include page="/jsp/common/includeResponsive.jsp">
    <jsp:param name="flag" value="spellingSuggestions"/>
  </jsp:include>