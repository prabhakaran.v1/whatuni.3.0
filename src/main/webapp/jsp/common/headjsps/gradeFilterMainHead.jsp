<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator" autoFlush="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%
String ionRangeSliderCss = CommonUtil.getResourceMessage("wuni.sr.ion.range.slider.css", null);
String gradeFilterJsName = CommonUtil.getResourceMessage("wuni.sr.grade.filter.js", null);

 String subjSessionId = (String)session.getAttribute("subjectSessionId");
 String srGradeHeaderStyleCss = CommonUtil.getResourceMessage("wuni.sr.gradefilter.style.header.css", null); //cmm_header_styles_586
 String srGradeMobileStyleCss = CommonUtil.getResourceMessage("wuni.sr.gradefilter.style.mobile.css", null);    //cmm_mobile_styles_586
 
%>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
    
    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=ionRangeSliderCss%>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=srGradeHeaderStyleCss%>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=srGradeMobileStyleCss%>" media="screen" />
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=gradeFilterJsName%>"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/ion.rangeSlider.min.js"> </script>
    <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="0"/>
      <jsp:param name="pageName" value="QUALIFICATION LANDING" /> 
      <jsp:param name="paramFlag" value="QUALIFICATION_LANDING" />      
      <jsp:param name="noindexfollow" value="noindex,follow"/>
    </jsp:include>