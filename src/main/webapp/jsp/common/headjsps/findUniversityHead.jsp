<%@page import="WUI.utilities.GlobalConstants"%>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
    <%
     String noindexfollow = "noindex,follow";
     if(GenericValidator.isBlankOrNull(request.getQueryString())){
       noindexfollow = "index,follow";
     }
     String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null);
     String findUniSearchJs = CommonUtil.getResourceMessage("wuni.find.uni.search.js", null);
     String opendaysJs = CommonUtil.getResourceMessage("wuni.opendays.js", null);
     String jqueryImagemapsterJs = CommonUtil.getResourceMessage("wuni.jquery.imagemapster.js", null);
    %>
    <link rel="canonical" href="<%=GlobalConstants.WHATUNI_SCHEME_NAME%><%=request.getServerName()%><SEO:SEOURL pageTitle="unibrowse">none</SEO:SEOURL>"/>    
    <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="0"/>
      <jsp:param name="pageName" value="COLLEGE BROWSE"/>
      <jsp:param name="paramFlag" value="UNI_LANDING" /> 
      <jsp:param name="noindexfollow" value="<%=noindexfollow%>" />
    </jsp:include>
    <%@include  file="/jsp/common/includeMainCSS.jsp"%> 
    <jsp:include page="/jsp/common/includeResponsive.jsp">
       <jsp:param name="flag" value="findUni"/>
    </jsp:include>
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=findUniSearchJs%>"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/openday/<%=jqueryImagemapsterJs%>"></script>   
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/openday/<%=opendaysJs%>"></script>