 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%
 String collegeId =  (String) request.getAttribute("collegeId");
 collegeId =  collegeId != null && collegeId.trim().length() > 0? collegeId : "0"; 
 String categoryCode=request.getAttribute("categoryCode") != null ? request.getAttribute("categoryCode").toString() : "";
 String studylevelid = (String) request.getAttribute("err_study_level_id");
 studylevelid  = studylevelid !=null && !studylevelid.equalsIgnoreCase("null") && studylevelid.trim().length()>0 ? studylevelid : "";
 String subjectname = (String) request.getAttribute("err_subject_name");
 subjectname  = subjectname !=null && !subjectname.equalsIgnoreCase("null") && subjectname.trim().length()>0 ? new CommonUtil().toTitleCase(subjectname) : "";
 String location = (String) request.getAttribute("err_location");
 location  = location !=null && !location.equalsIgnoreCase("null") && location.trim().length()>0 ? new CommonFunction().replacePlus(location.trim()) : "";
 %>
 <c:if test="${not empty requestScope.providerResult}">
      <jsp:include page="/include/seoTitle.jsp">
          <jsp:param name="collegeId" value="<%=collegeId%>"/>
          <jsp:param name="categoryCode" value="<%=categoryCode%>"/>
          <jsp:param name="studyLevelId" value="<%=studylevelid%>"/>
          <jsp:param name="searchKeyword" value="<%=subjectname%>"/>
            <jsp:param name="paramFlag" value="COURSE_RESULTS_NOT_FOUND"/>
          <jsp:param name="pageName" value="COURSE RESULTS NOT FOUND"/>
          <jsp:param name="noindexfollow" value="noindex,follow"/>
           <jsp:param name="locationCountyId" value="<%=location%>"/>
      </jsp:include> 
  </c:if>
  <c:if test="${empty requestScope.providerResult}">
      <jsp:include page="/include/htmlTitle.jsp">   
        <jsp:param name="indexFollowFlag" value="index,follow" />
      </jsp:include>
  </c:if>
    <%-- @include  file="/include/commonHeaderContent.jsp" %>
    <%@include  file="/include/includeJSCSS.jsp" --%>
    <%@include  file="/jsp/common/includeCSS.jsp" %>