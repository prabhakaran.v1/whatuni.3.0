<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil, java.util.ResourceBundle"%>
<%String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css"); 
  String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
  String main_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.cd.screens.css");
  String commonCssName = CommonUtil.getResourceMessage("wuni.common.css", null);
  String mainHeaderCSS = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);  
  ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
  String retinaJs = bundle.getString("wuni.retina.js");
  String cpcLandingPageJs = CommonUtil.getResourceMessage("wuni.cpc.landing.js", null);
%>
<meta http-equiv="content-language" content=" en-gb "/>
<jsp:include page="/jsp/common/includeIconImg.jsp"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">    
    <c:choose>
      <c:when test="${clearingFlag eq 'Y'}">
      <c:set var="seoPageName" value="CLEARING MULTIPLE CLIENT CPC LANDING PAGE"/>
      <c:set var="seoParamFlag" value="CLEARING_MULTIPLE_CLIENT_CPC_LANDING_PAGE"/>
      </c:when>
      <c:otherwise>
      <c:set var="seoPageName" value="MULTIPLE CLIENT CPC LANDING PAGE"/>
      <c:set var="seoParamFlag" value="MULTIPLE_CLIENT_CPC_LANDING_PAGE"/>
      </c:otherwise>
    </c:choose>
    <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="0"/>
      <jsp:param name="pageName" value="${seoPageName}" /> 
      <jsp:param name="paramFlag" value="${seoParamFlag}" />      
      <jsp:param name="noindexfollow" value="noindex,follow"/>
    </jsp:include>
    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=mainHeaderCSS%>" media="screen">
    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=commonCssName%>" media="screen">    
    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=main_ver%>" media="screen">    
    <!-- End Facebook Pixel Code -->
    <jsp:include page="/jsp/common/includeResponsive.jsp"></jsp:include>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/cpc/<%=cpcLandingPageJs%>"> </script>    
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/cpc/wu_cpcHeader_040220.js"></script>   
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=retinaJs%>"></script> 
    <jsp:include page="/jsp/common/abTesting.jsp"/>
<jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
	<jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>
<%-- Facebook pixel tracking script included in head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/facebookPixelTracking.jsp"/>
<%--Breadcrumb schema added by Prabha on 21.Mar.17--%>
  <jsp:include page="/jsp/common/includeSchemaTag.jsp">
    <jsp:param name="pageName" value="BREADCRUMB_SCHEMA"/>
  </jsp:include>
  <jsp:include page="/jsp/common/includeSchemaTag.jsp">
    <jsp:param name="pageName" value="WEB_PAGE_SCHEMA"/>
  </jsp:include>
