<%@page import="WUI.utilities.CommonUtil, mobile.util.MobileUtils, WUI.utilities.CommonFunction"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%
  CommonFunction common = new CommonFunction();
  String selectedBasketId = (String)request.getAttribute("selectedBasketId");
  String assocTypeCode = (String)request.getAttribute("assocTypeCode");
  String cookieBasketId = common.checkCookieStatus(request);  
  String collegeCount = request.getAttribute("basketpodcollegecount") != null ? String.valueOf(request.getAttribute("basketpodcollegecount")) : "0";
  int cookieBasketCount = Integer.parseInt(collegeCount);
  String userId = new WUI.utilities.SessionData().getData(request, "y");
  int basketNo = 1;
  String compareSuggestionsListSize = request.getAttribute("compareSuggestionsListSize")!=null ? (String)request.getAttribute("compareSuggestionsListSize") : "0";//Added by Indumathi.S 28_Jun_2016
  String viewedBasketName = (String)request.getAttribute("comparisonName");
  String domainSpecPath = common.getSchemeName(request)+CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
  String mobileFlag = "desktop";
  boolean isMobileFlag = new MobileUtils().userAgentCheck(request);
  String flexSliderMinJs = CommonUtil.getResourceMessage("wuni.flex.slider.min.js", null);
  String myFinalFiveJs = CommonUtil.getResourceMessage("wuni.my.final.five.js", null);
  String flexSliderCss = CommonUtil.getResourceMessage("wuni.compare.flexslider.css", null);
  String doughnutChartJs = CommonUtil.getResourceMessage("wuni.compare.doughnut.chart.js", null);
  String basketJs = CommonUtil.getResourceMessage("wuni.basket.js", null);

%>

<jsp:include page="/include/seoTitle.jsp">
  <jsp:param name="collegeId" value="0"/>
  <jsp:param name="pageName" value="FINAL 5 PAGE" /> 
  <jsp:param name="paramFlag" value="FINAL 5 PAGE"/>      
</jsp:include> 
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta name="format-detection" content="telephone=no" />
<meta content="utf-8" http-equiv="encoding">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<jsp:include page="/jsp/common/includeMainCSS.jsp"/> 
<input type="hidden" id="mobileFlag" value="<%=mobileFlag%>" name="mobileFlag"/>

<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
<%if(!(isMobileFlag)){%>
  <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=flexSliderCss%>" media="screen" id="flexSliderCSS"/>
<%}%>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<%if(!(isMobileFlag)){%>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/jquery/<%=flexSliderMinJs%>"></script>
<%}%>
<script type="text/javascript">
  var compareWith = document.documentElement.clientWidth;
  if(compareWith <= 480){
	  document.getElementById('mobileFlag').value = "mobile";
	}else if((compareWith > 480) && (compareWith <= 992)){
	  document.getElementById('mobileFlag').value = "ipad";
	}else{
	  document.getElementById('mobileFlag').value = "desktop";
	}
</script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/mychoice/<%=myFinalFiveJs%>"></script>
<%@include  file="/jsp/basket/include/includeBasketResponsive.jsp" %>