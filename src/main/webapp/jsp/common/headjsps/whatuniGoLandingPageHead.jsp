<%@ taglib uri="http://java.sun.com/jstl/core"  prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
<%@page import="WUI.utilities.CommonUtil" autoFlush="true" %>
<%
  String provisionalOfferJsName = CommonUtil.getResourceMessage("wuni.provisional.offer.js", null);
  String whatuniGOJsName = CommonUtil.getResourceMessage("wuni.whatunigo.offer.js", null);
  String pageName = (String) request.getAttribute("pagename3");
  String paramFlag = "";
  String paramName = "";
  
%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<jsp:include page="/jsp/whatunigo/include/includeWhatuniGoCSS.jsp">
	<jsp:param name="fromPage" value="accept-offer" />
</jsp:include>
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/whatunigo/<%=whatuniGOJsName%>"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/whatunigo/<%=provisionalOfferJsName%>"></script>
<c:set var="pageName" value="<%=pageName%>" />
<c:if test="${pageName eq 'whatuniGoLandingPage.jsp' or pageName eq 'allErrorPage.jsp'}">
  <%paramName = "WUGO ACCEPTING OFFER JOURNEY"; paramFlag = "WUGO_ACCEPTING_OFFER_JOURNEY"; %>
</c:if>
<c:if test="${pageName eq 'provisionalOfferLandingPage.jsp'}">
  <%paramName = "WUGO PROVISIONAL OFFER JOURNEY"; paramFlag = "WUGO_PROVISIONAL_OFFER_JOURNEY"; %>
</c:if>
<c:if test="${pageName eq 'applicationPage.jsp'}">
  <%paramName = "APPLICATION PAGE"; paramFlag = "APPLICATION_PAGE"; %>
</c:if>

<jsp:include page="/include/seoTitle.jsp">
	<jsp:param name="collegeId" value="0" />
	<jsp:param name="pageName" value="<%=paramName%>" />
	<jsp:param name="paramFlag" value="<%=paramFlag%>" />      
	<jsp:param name="noindexfollow" value="noindex,follow" />
</jsp:include>