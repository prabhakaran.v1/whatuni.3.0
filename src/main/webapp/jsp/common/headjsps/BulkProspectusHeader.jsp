<%@ page import="java.util.*, WUI.utilities.CommonFunction, WUI.utilities.CommonUtil" autoFlush="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
  <head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
    <meta content="utf-8" http-equiv="encoding" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;" />
    <%  String metaRobots = request.getAttribute("metarobots") != null ? String.valueOf(request.getAttribute("metarobots")) : "noindex,follow"; //21-Jan-2014 Release
        String urlString  = request.getAttribute("loginUrl")  != null ?  String.valueOf(request.getAttribute("loginUrl")) : "/home.html";
        String pageno	          =   (String) (request.getAttribute("pageno") !=null ? request.getAttribute("pageno") : "1");
        String totalCount        =   request.getAttribute("allProsCount")!= null ? request.getAttribute("allProsCount").toString() : "1";
        String url = (String)request.getAttribute("urlString");
        String studyLevel = "";        
        String networkId = (String)request.getAttribute("netWorkId");
        String urlArray[] = url.split("/");
        String uClass = "actl";
        String pgClass = "nacr"; 
        String pageName = "PROSPECTUS BROWSE LOCATION";  
               studyLevel = (String)request.getAttribute("studyLevel");
        String queryString = (String)request.getAttribute("modqueryStr");
               queryString = queryString!= null & queryString != "" ? queryString:"";
        String logCollegeId = request.getParameter("collegeid");        
        //
        String queryStringPros = (String)request.getAttribute("modqueryStrWO");
               queryStringPros = queryStringPros!= null & queryStringPros != "" ? queryStringPros:"";
        //
        //String prospectusURL = "/degrees/prospectus/";
        String prospectusURL = "/degrees"+url+".html";        
        String fullQueryStr = (String)request.getAttribute("queryStr");
        fullQueryStr = fullQueryStr!=null && fullQueryStr !="" ? "?"+fullQueryStr: "";
        String sort = request.getAttribute("SORT") == null || ((String)request.getAttribute("SORT")).trim().length() == 0 ? "" :  "&sort="+((String)request.getAttribute("SORT")).trim();
        url = prospectusURL+"?mypage=my_prospectus"+sort;
        int sliderCount = 0;
        //'asc_times_ranking
        String universitySortStr = (fullQueryStr.contains("sort=asc_college") || fullQueryStr.contains("sort=desc_college")) ? (fullQueryStr.contains("sort=asc_college") ? fullQueryStr.replace("asc_college","desc_college").replace("pageno="+pageno ,"pageno=1") : fullQueryStr.replace("desc_college","asc_college").replace("pageno="+pageno ,"pageno=1")) : (queryString!="" ?   (queryString + "&mypage=my_prospectus&sort=asc_college&pageno=1"): "?mypage=my_prospectus&sort=asc_college&pageno=1"); // Yogeswari :: 19.05.2015 :: for adding sort type when pagination.
        String universitySortClass = (fullQueryStr.contains("sort=asc_college") || fullQueryStr.contains("sort=desc_college")) ? (fullQueryStr.contains("sort=asc_college") ? "fa fa-angle-up fa-1_4x fl" : "fa fa-angle-down fa-1_4x fl") : "fa fa-angle-up fa-1_4x fl";
        String ratinSortStr = (fullQueryStr.contains("sort=asc_student_rating") || fullQueryStr.contains("sort=desc_student_rating")) ? (fullQueryStr.contains("sort=asc_student_rating") ? fullQueryStr.replace("asc_student_rating","desc_student_rating").replace("pageno="+pageno ,"pageno=1") : fullQueryStr.replace("desc_student_rating","asc_student_rating").replace("pageno="+pageno ,"pageno=1")) : (queryString!="" ?   (queryString + "&mypage=my_prospectus&sort=asc_student_rating&pageno=1"): "?mypage=my_prospectus&sort=asc_student_rating&pageno=1"); // Yogeswari :: 19.05.2015 :: for adding sort type when pagination.
        String ratinSortStrClass = (fullQueryStr.contains("sort=asc_student_rating") || fullQueryStr.contains("sort=desc_student_rating")) ? (fullQueryStr.contains("sort=asc_student_rating") ? "fa fa-angle-up fa-1_4x fl" : "fa fa-angle-down fa-1_4x fl") : "fa fa-angle-up fa-1_4x fl";
        
         if(studyLevel.equalsIgnoreCase("M")){
            pageName = "PROSPECTUS BROWSE LOCATION";
         }else if(studyLevel.equalsIgnoreCase("L")){               
            pageName = "PG PROSPECTUS BROWSE LOCATION";
         }
        String canonicalLink = "";
        CommonFunction commonFun = new CommonFunction();
        String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
         if(fullQueryStr!="" && fullQueryStr != null){
           canonicalLink = httpStr + "www.whatuni.com/degrees/prospectus/";
           metaRobots = "noindex,follow";
        }else{
           metaRobots = "index,follow";
        }
        String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null);
        String prospectusJs =CommonUtil.getResourceMessage("wuni.prospectusJsName.js", null);
        CommonFunction common = new CommonFunction();
        String gaCollegeName = "";    
        
    %>
     <jsp:include page="/include/seoTitle.jsp" >
         <jsp:param name="collegeId" value="0"/>
         <jsp:param name="pageName" value="<%=pageName%>"/>
         <jsp:param name="paramFlag" value="PROSPECTUS_BROWSE_LOCATION"/>
         <jsp:param name="noindexfollow" value="<%=metaRobots%>"/>
     </jsp:include>
      <link rel="canonical" href="<%=canonicalLink%>"/> 
     <%@include  file="/jsp/common/includeMainCSS.jsp" %>     
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.modernizr.js'/>"></script> 
      <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=prospectusJs%>"></script>       
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
     <%
String domainSpecificPath = new CommonFunction().getSchemeName(request)+CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
String main_480_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.480.css", null);
String main_992_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.992.css", null);
String envronmentName = commonFun.getWUSysVarValue("WU_ENV_NAME");%>

<script type="text/javascript" language="javascript">
  var dev = jQuery.noConflict();
  dev(document).ready(function(){
    adjustStyle();
  });
  adjustStyle();
  function loadLazyJS(){
    if(!document.getElementById('lazyload')){
      var file=document.createElement('script');
      file.setAttribute("type","text/javascript");
      file.setAttribute("id", "lazyload");
      file.setAttribute("src", "<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>");
      document.getElementsByTagName("head")[0].appendChild(file);
    }
  }
  function jqueryWidth() {
    return dev(this).width();
  } 
  function adjustStyle() {
    var width = document.documentElement.clientWidth;
    var path = "";     
    if (width <= 480) {
    loadLazyJS();
      if (dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }
      <%if(("LIVE").equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}%>
      dev('.img_ppn img').addClass('lazy-load');
    } else if ((width > 480) && (width <= 992)) { 
    loadLazyJS();
      if (dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      } 
      <%if(("LIVE").equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}%>
      //dev('#prospectusKwd').css('width', '100%').css('width', '-=18px');
      dev('.img_ppn img').addClass('lazy-load');      
    }else {
    loadLazyJS();    
      if (dev("#viewport").length > 0) {
        dev("#viewport").remove();
      } 
      document.getElementById('size-stylesheet').href = ""; 
      dev('.img_ppn img').removeAttr('class');
      dev(".hm_srchbx").hide();
    }
  }
  dev(window).on('orientationchange', orientationChangeHandler);
  function orientationChangeHandler(e) {
    setTimeout(function() {
      dev(window).trigger('resize');
    }, 500);
  }
  dev(window).resize(function() {    
    var screenWidth = jqueryWidth();    
    adjustStyle();
  });
  function jqueryWidth() {
    return dev(this).width();
  }
</script>     
     
   </head>