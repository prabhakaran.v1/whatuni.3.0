<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.SessionData, WUI.utilities.GlobalConstants, WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.GlobalFunction, java.util.ResourceBundle"%>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
 
<%
ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
String newsearchJSName = bundle.getString("wuni.newSearchResults.js");
String ecommerceTrackingJsName = bundle.getString("wuni.ecommerce.tracking.js");
String pageno="1"; 
String exitOpenDayRes = "" , odEvntAction = "false";
    if(session.getAttribute("exitOpRes")!=null && !"".equals(session.getAttribute("exitOpRes"))){
      exitOpenDayRes = (String)session.getAttribute("exitOpRes");
    }    
    if(!"".equals(exitOpenDayRes) && "true".equals(exitOpenDayRes)){
      odEvntAction = "true";
    }

%>
<c:if test="${requestScope.pageno ne ''}">
  <%pageno=String.valueOf(request.getAttribute("pageno")); %>
</c:if>
<% 
    String paramCollegeId = (request.getAttribute("collegeId") !=null && !request.getAttribute("collegeId").equals("null") && String.valueOf(request.getAttribute("collegeId")).trim().length()>0 ? String.valueOf(request.getAttribute("collegeId")) : ""); 
    String paramCategoryCode = (request.getAttribute("paramCategoryCode") !=null && !request.getAttribute("paramCategoryCode").equals("null") && String.valueOf(request.getAttribute("paramCategoryCode")).trim().length()>0 ? new CommonUtil().toUpperCase(String.valueOf(request.getAttribute("paramCategoryCode"))) : "");  
    String paramStudyLevelId = (request.getAttribute("paramStudyLevelId") !=null && !request.getAttribute("paramStudyLevelId").equals("null") && String.valueOf(request.getAttribute("paramStudyLevelId")).trim().length()>0 ? new CommonUtil().toUpperCase(String.valueOf(request.getAttribute("paramStudyLevelId"))) : ""); 
    //
    String first_mod_group_id = (String)request.getAttribute("first_mod_group_id");
    String noindexfollow = request.getAttribute("meta_robot") !=null ? String.valueOf(request.getAttribute("meta_robot"))  : "noindex,follow";
    String keyword = (request.getAttribute("searchText") !=null && !request.getAttribute("searchText").equals("null") && String.valueOf(request.getAttribute("searchText")).trim().length()>0 ? String.valueOf(request.getAttribute("searchText")) : "");  
    String keywordPhrase = request.getAttribute("searchText") != null ? request.getAttribute("searchText").toString() : "";
    // wu582_20181023 - Sabapathi: Added studymodeId for stats logging
    String studymodeId = request.getAttribute("studymodeId") != null ? request.getAttribute("studymodeId").toString() : "";
    String pros_search_name = request.getAttribute("prospectus_search_name") != null ? request.getAttribute("prospectus_search_name").toString() : "";
    String location = (request.getAttribute("location") !=null && !request.getAttribute("location").equals("null") && String.valueOf(request.getAttribute("location")).trim().length()>0 ? String.valueOf(request.getAttribute("location")) : "");  
    String paramFlag = paramStudyLevelId != null &&  paramStudyLevelId.equalsIgnoreCase("") ? "ALL_COURSE_RESULTS" : "COURSE_RESULTS";
    String pageNameValue = paramStudyLevelId != null &&  paramStudyLevelId.equalsIgnoreCase("") ? "ALL COURSE RESULTS" : "COURSE RESULTS";
    String courseMappingPath = request.getAttribute("courseMappingPath") !=null ? "rsearch.html" : "csearch.html";
    String searchPosition = new WUI.utilities.CookieManager().getCookieValue(request, "sresult_provider_position");
    String studyLevelDesc = (String)request.getAttribute("studyLevelDesc");
    // added for 02nd Feb 2009 Release to show the canonicalURL for the SEO requirement 
    String canonicalURL = (String) request.getAttribute("currentPageUrl");
    canonicalURL  = canonicalURL !=null && canonicalURL.trim().length()>0 ? canonicalURL : "";
    // end of thecoce added  
    String paramCollegeName = (String) request.getAttribute("collegeName");
    paramCollegeName = (!GenericValidator.isBlankOrNull(paramCollegeName) && !paramCollegeName.equalsIgnoreCase("null")) ? paramCollegeName.trim() : "";
    String seoCollegeName =  paramCollegeName !=null && paramCollegeName.trim().length()>0 ? new CommonFunction().replaceHypen(new CommonFunction().replaceURL(paramCollegeName)).replaceAll(" ","-")+"-" : "";
    String seoCollegeNameLower  = new CommonUtil().toLowerCase(seoCollegeName);
    String collegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("collegeNameDisplay") : "";
    String collegeLocation = (String) request.getAttribute("collegeLocation");
    collegeLocation = collegeLocation != null && !collegeLocation.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(collegeLocation).trim() : "";
    Object SEARCH_TYPE = request.getAttribute("SEARCH_TYPE");
    CommonFunction common = new CommonFunction();
    String courseUrl = "/all-courses/csearch?university="+common.replaceHypen(common.replaceURL(paramCollegeName)).toLowerCase(); //Changed new all courses url pettern, By Thiyagu G for 27_Jan_2016.
    int searchhits = 0;
    if(!GenericValidator.isBlankOrNull((String)(request.getAttribute("courseCount"))) && new CommonUtil().isNumber((String)(request.getAttribute("courseCount")))){	
      searchhits = Integer.parseInt(String.valueOf(request.getAttribute("courseCount"))); 
    }
    String courseHeader = (String)request.getAttribute("courseHeader") != null ? request.getAttribute("courseHeader").toString() : "";
    String currYear = GlobalConstants.CLEARING_YEAR;
    String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");//5_AUG_2014
    String qString = (String)request.getAttribute("queryStr");
           qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
    String clrString = qString;
    String courseExistsLink = "";
    if("ON".equalsIgnoreCase(clearingonoff)){      
      courseExistsLink = (String)request.getAttribute("courseExistsLink") != null ? request.getAttribute("courseExistsLink").toString() : "";
           courseExistsLink = courseExistsLink.replace("?", "?clearing&");
    }
    String clearingPage =  (String)request.getAttribute("searchClearing");
       clearingPage = GenericValidator.isBlankOrNull(clearingPage)?"":clearingPage;    
    String isAdvertiser = request.getAttribute("isAdvertiser") != null && request.getAttribute("isAdvertiser").toString().trim().length() > 0 ? (String) request.getAttribute("isAdvertiser") : "";
    String advNonAdv = "";
    if("TRUE".equals(isAdvertiser)){
      advNonAdv = "Advertiser";
    }else{
      advNonAdv = "Non-Advertiser";
    }
    String evntProviderName = (paramCollegeName != null && paramCollegeName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(common.replaceSpecialCharacter(paramCollegeName))) : "").toLowerCase();
    String advertiserFlag = (String)request.getAttribute("advertiserFlag");
           advertiserFlag = GenericValidator.isBlankOrNull(advertiserFlag)?"":advertiserFlag;    
    //  
    String institutionId = new GlobalFunction().getInstitutionId(paramCollegeId);
    String studyLevel = (request.getAttribute("studyLevel") !=null && !request.getAttribute("studyLevel").equals("null") && String.valueOf(request.getAttribute("studyLevel")).trim().length()>0 ? String.valueOf(request.getAttribute("studyLevel")) : ""); 
    String capeImg = CommonUtil.getImgPath("/wu-cont/images/capeh.png",0);
    //flag for to show canonical url in june_5_18 by Sangeeth.S
    String canonicalUrlFlag = request.getAttribute("CANONICAL_URL") != null ? request.getAttribute("CANONICAL_URL").toString() : "";
%>
  <c:if test="${'N' ne sessionScope.sessionData['cookieTargetingCookieDisabled']}">
    <%ecommerceTrackingJsName = CommonUtil.getResourceMessage("wuni.ecommerce.tracking.disabled.js", null);%>   
  </c:if>
     <jsp:include page="/jsp/common/includeSeoMetaDetails.jsp">
       <jsp:param value="<%= noindexfollow%>" name="noindexfollow"/>
     </jsp:include>    
     <jsp:include page="/jsp/search/searchredesign/includeSearchCSS.jsp"/>
     <jsp:include page="/jsp/common/includeResponsive.jsp">                                
        <jsp:param name="flag" value="spreDesign"/>
        <jsp:param name="prPageFlag" value="prPageFlag"/>
      </jsp:include>
    <input type="hidden" id="jsPathHidden" value="<%=CommonUtil.getJsPath()%>/js/"/>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/wu_breakingJsInPreLoad_20210119.js"></script>
    <link rel="preload" as="script" href="<%=CommonUtil.getJsPath()%>/js/<%=newsearchJSName%>"> 
    <%-- <link rel="preload" as="script" href="<%=CommonUtil.getJsPath()%>/js/<%=ecommerceTrackingJsName%>"> --%>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=ecommerceTrackingJsName%>"></script>
    <!--Modified the condition for self referencing canonical url for clearing in june_5_18 by Sangeeth.S -->
    <% if((SEARCH_TYPE != null && SEARCH_TYPE.toString().equals("BROWSE_SEARCH")) || "CANONICAL_URL".equalsIgnoreCase(canonicalUrlFlag)) { %>
    <link rel="canonical" href="<%=canonicalURL%>"/>       
    <% } %>    
    <!--Added by Indumathi.S Nov-03-15 Rel For including banners-->
    <jsp:include page="/jsp/search/include/searchGoogleAdSlots.jsp"/>
    <%--Added schema tag for the open day event in PR page for Oct_20_18 rel by Sangeeth.S--%>
    <c:if test="${not empty requestScope.listOfOpendayInfo}">
      <jsp:include page="/jsp/common/includeSchemaTag.jsp">
        <jsp:param name="pageName" value="PROVIDER_RESULTS_OPENDAY_SCHEMA"/>
      </jsp:include>
    </c:if>
