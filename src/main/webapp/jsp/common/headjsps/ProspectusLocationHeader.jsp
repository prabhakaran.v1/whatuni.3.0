<%@ page import="java.util.*, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants,WUI.utilities.CommonUtil" autoFlush="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
  <head>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
    <meta content="utf-8" http-equiv="encoding" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;" />
    <%  String metaRobots = request.getAttribute("metarobots") != null ? String.valueOf(request.getAttribute("metarobots")) : "noindex,follow"; //21-Jan-2014 Release
        String urlString  = request.getAttribute("loginUrl")  != null ?  String.valueOf(request.getAttribute("loginUrl")) : "/home.html";
        String url = (String)request.getAttribute("urlString");
        String url1 = "";
        String url2 = "";
        String studyLevelId = "";
        String studyLevel = "";
        String pageno = (String)request.getAttribute("pageNo");
        String networkId = (String)request.getAttribute("netWorkId");
        String urlArray[] = url.split("/");
        String uClass = "actl";
        String pgClass = "nacr";
        String pageName = "PROSPECTUS BROWSE LOCATION";  
               studyLevel = (String)request.getAttribute("studyLevel");
        String queryString = (String)request.getAttribute("modqueryStr");
               queryString = queryString!= null & queryString != "" ? queryString:"";
        String logCollegeId = request.getParameter("collegeid");
        
        //
        String queryStringPros = (String)request.getAttribute("modqueryStrWO");
               queryStringPros = queryStringPros!= null & queryStringPros != "" ? queryStringPros:"";
        //
        String prospectusURL = "/degrees/prospectus/";
        String prosepectusSortType =  ("L").equalsIgnoreCase(studyLevel)?"level=L":"level=M";
        String prosCateLvlTracking = ("M".equalsIgnoreCase(studyLevel)  ? "university-prospectuses-ug" : "university-prospectuses-pg");        
        //
        String fullQueryStr = (String)request.getAttribute("queryStr");
        fullQueryStr = fullQueryStr!=null && fullQueryStr !="" ? "?"+fullQueryStr: "";
        //
        String UGTabUrl = prospectusURL +  (queryStringPros!="" ?   queryStringPros + "&level=M" : "?level=M") ;
        String PGTabUrl = prospectusURL + (queryStringPros!="" ? queryStringPros + "&level=L" : "?level=L") ;
        int sliderCount = 0;
        
        String universitySortStr = (fullQueryStr.contains("sort=asc_college") || fullQueryStr.contains("sort=desc_college")) ? (fullQueryStr.contains("sort=asc_college") ? fullQueryStr.replace("asc_college","desc_college") : fullQueryStr.replace("desc_college","asc_college")) : (queryString!="" ?   (queryString + "&sort=asc_college" ): "?sort=asc_college") ;
        String universitySortClass = (fullQueryStr.contains("sort=asc_college") || fullQueryStr.contains("sort=desc_college")) ? (fullQueryStr.contains("sort=asc_college") ? "fa fa-angle-up fa-1_4x fl" : "fa fa-angle-down fa-1_4x fl") : "fa fa-angle-up fa-1_4x fl";
        String ratinSortStr = (fullQueryStr.contains("sort=asc_student_rating") || fullQueryStr.contains("sort=desc_student_rating")) ? (fullQueryStr.contains("sort=asc_student_rating") ? fullQueryStr.replace("asc_student_rating","desc_student_rating") : fullQueryStr.replace("desc_student_rating","asc_student_rating")) : (queryString!="" ?   (queryString + "&sort=asc_student_rating" ): "?sort=asc_student_rating") ;
        String ratinSortStrClass = (fullQueryStr.contains("sort=asc_student_rating") || fullQueryStr.contains("sort=desc_student_rating")) ? (fullQueryStr.contains("sort=asc_student_rating") ? "fa fa-angle-up fa-1_4x fl" : "fa fa-angle-down fa-1_4x fl") : "fa fa-angle-up fa-1_4x fl";
        String timesRankingSortStr = (fullQueryStr.contains("sort=asc_times_ranking") || fullQueryStr.contains("sort=desc_times_ranking")) ? (fullQueryStr.contains("sort=asc_times_ranking") ? fullQueryStr.replace("sort=asc_times_ranking","sort=desc_times_ranking") : fullQueryStr.replace("sort=desc_times_ranking","sort=asc_times_ranking")) : (queryString!="" ?   (queryString + "&sort=asc_times_ranking" ): "?sort=asc_times_ranking") ;
        String timesRankingClass = (fullQueryStr.contains("sort=asc_times_ranking") || fullQueryStr.contains("sort=desc_times_ranking")) ? (fullQueryStr.contains("sort=asc_times_ranking") ? "fa fa-angle-up fa-1_4x fl" : "fa fa-angle-down fa-1_4x fl") : "fa fa-angle-up fa-1_4x fl";
             if(studyLevel.equalsIgnoreCase("M")){
                pageName = "PROSPECTUS BROWSE LOCATION";
             }else if(studyLevel.equalsIgnoreCase("L")){               
                pageName = "PG PROSPECTUS BROWSE LOCATION";
             }
        String httpStr = new CommonFunction().getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
        String canonicalLink = httpStr + "www.whatuni.com/degrees/prospectus/";        
        if(fullQueryStr!="" && fullQueryStr != null){
           //canonicalLink = httpStr + "www.whatuni.com/degrees/prospectus/";
           metaRobots = "noindex,follow";
        }else{
           metaRobots = "index,follow";
        }
        //
        String flexSliderCssName = CommonUtil.getResourceMessage("wuni.flexslider.css", null);
        String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null);
        String prospectusJs = CommonUtil.getResourceMessage("wuni.prospectusJsName.js", null);
        String collegeNameDisplay = (request.getAttribute("collegeDispName") != null) ? (String)request.getAttribute("collegeDispName") : "";
        
    %><%--16-Apr-2014--%>
     <jsp:include page="/include/seoTitle.jsp" >
         <jsp:param name="collegeId" value="0"/>
         <jsp:param name="pageName" value="<%=pageName%>"/>
         <jsp:param name="paramFlag" value="PROSPECTUS_BROWSE_LOCATION"/>
         <jsp:param name="noindexfollow" value="<%=metaRobots%>"/>
     </jsp:include>
      <link rel="canonical" href="<%=canonicalLink%>"/> 
     <%@include  file="/jsp/common/includeMainCSS.jsp" %>
     <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=flexSliderCssName%>" media="screen" />
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
    <script type="text/javascript">
     function loadLazyJS(){
       if(!document.getElementById('lazyload')){
         var file=document.createElement('script');
         file.setAttribute("type","text/javascript");
         file.setAttribute("id", "lazyload");
         file.setAttribute("src", "<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>");
         document.getElementsByTagName("head")[0].appendChild(file);
       }
     }
   </script> 
    <!--<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/home/jquery.flexslider-min.js"> </script>-->
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery.flexslider-2.2.min.js"></script>
      <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.modernizr.js'/>"></script>       
      <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=prospectusJs%>"></script> 
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
     <%@include  file="/prospectus/include/includeProspectusResponsive.jsp" %>
     <%--<jsp:include  page="/jsp/home/include/includeHeaderResponsive.jsp"/>--%>
   </head>
