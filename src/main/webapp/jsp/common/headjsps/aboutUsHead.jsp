<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>

  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">  
  <%@include  file="/jsp/common/includeMainCSS.jsp"%> 
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>  
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>  
  <jsp:include  page="/help/aboutus/includeStaticCntResponsive.jsp"/>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.staticpage.js'/>"></script>

  <%@include  file="/include/htmlTitle.jsp"%>
  <c:if test="${not empty requestScope.canonicalUrl}"><%--Added by Sangeeth.S for 25_SEP_18 rel--%>       
    <link rel="canonical" href="${requestScope.canonicalUrl}"/>
  </c:if>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/javascripts/'/><spring:message code='wuni.aboutus.js'/>"> </script>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.homeJsName.js'/>"></script>
