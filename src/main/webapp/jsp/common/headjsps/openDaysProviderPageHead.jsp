<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, WUI.utilities.GlobalFunction, org.apache.commons.validator.GenericValidator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
  <%
    CommonFunction commonFun = new CommonFunction();
    String enVName = commonFun.getWUSysVarValue("WU_ENV_NAME");
    String domainSpecificsPath = commonFun.getSchemeName(request)+ CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
    String gaProviderCollegeName = "";
    String indexfollow = "index,follow";
    String collegeId = "", canonUrl = "";
    if(request.getAttribute("collegeId")!=null && !"".equals(request.getAttribute("collegeId"))){
      collegeId = (String)request.getAttribute("collegeId");
    }
    if(request.getAttribute("provCanonicalUrl")!=null && !"".equals(request.getAttribute("provCanonicalUrl"))){
      canonUrl = (String)request.getAttribute("provCanonicalUrl");
    }
    String exitOpenDayRes = "" , odEvntAction = "false";
    if(session.getAttribute("exitOpRes")!=null && !"".equals(session.getAttribute("exitOpRes"))){
      exitOpenDayRes = (String)session.getAttribute("exitOpRes");
    }    
    if(!"".equals(exitOpenDayRes) && "true".equals(exitOpenDayRes)){
      odEvntAction = "true";
    }
    if(request.getAttribute("noindex")!=null && "true".equals(request.getAttribute("noindex"))){
      indexfollow = "noindex,follow";
    }
     String gaCollegeName = "";
     String dateClassName = "ucod_lftyr fl";
     String websitePrice = (String)request.getAttribute("websitePrice") != "" ? (String)request.getAttribute("websitePrice") : "";
      String openMonthYear = (String)request.getAttribute("openMonthYear") != "" ? (String)request.getAttribute("openMonthYear") : "";
    String orderItemId = (String)request.getAttribute("orderItemId") != "" ? (String)request.getAttribute("orderItemId") : "";
    String networkId = (String)request.getAttribute("networkId") != "" ? (String)request.getAttribute("networkId") : "";
    String bookingUrl = (String)request.getAttribute("bookingUrl") != "" ? (String)request.getAttribute("bookingUrl") : "";
  String openDayMonthDesc = "", stTimeDesc = "", edTimeDesc = "";
  String collegeName = (String)request.getAttribute("interactionCollegeName");
  if(request.getAttribute("provCanonicalUrl")!=null && !"".equals(request.getAttribute("provCanonicalUrl"))){
    canonUrl = (String)request.getAttribute("provCanonicalUrl");
  }
  String openDaySaveCal = "";
   String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
  String providerLogo = !GenericValidator.isBlankOrNull((String)request.getAttribute("providerLogo")) ? CommonUtil.getImgPath("",0) + (String)request.getAttribute("providerLogo") : "";
  String totalRecordCount = (String)request.getAttribute("totalRecordCount");  
  String openDayMonthYearDesc = (String)request.getAttribute("openDayMonthYearDesc") != "" ? (String)request.getAttribute("openDayMonthYearDesc") : "";
  String openDayMainCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.styles.css", null);//getting css name dynamically by Sangeeth.S for 31_Jul_18 rel
  String openDayHeaderCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);  
  String openDayLandingJsName = CommonUtil.getResourceMessage("wuni.openday.landing.js", null);//getting Js name dynamically by Sangeeth.S for 20_Nov_18 rel
  
%>  
  <jsp:include page="/jsp/common/includeMainCSS.jsp"/> 
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>   
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=openDayMainCssName%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=openDayHeaderCssName%>" media="screen" />
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>   
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
  <%@include  file="/jsp/openday/include/includeOpenDayResponsive.jsp" %>
  <%if(!"".equals(canonUrl)){%>
    <link rel="canonical" href="<%=canonUrl%>"/>
  <%}%>
  <jsp:include page="/include/seoTitle.jsp">
    <jsp:param name="collegeId" value="<%=collegeId%>" />    
    <jsp:param name="pageName" value="WU OPEN DAYS PROVIDER PAGE" /> 
    <jsp:param name="paramFlag" value="WUNI_OPEN_DAYS" />      
    <jsp:param name="noindexfollow" value="<%=indexfollow%>"/>
</jsp:include>