<%@ page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<jsp:include page="/jsp/common/includeIconImg.jsp"/>
<title>Whatuni is turning 10 years old! Can you believe it?!</title> 
<meta name="description" content="In the last ten years Whatuni has helped A LOT of students find their right university. Have a look at some of the crazy stats we've racked up along the way."/> 
<meta name="ROBOTS" content="noindex, follow"/>   
<meta name="content-type" content="text/html; charset=utf-8"/>  
<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>   
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<link href="<%=CommonUtil.getCSSPath()%>/10th_birthday/css/anniv_styles.css" rel="stylesheet" type="text/css" media="screen"> 
