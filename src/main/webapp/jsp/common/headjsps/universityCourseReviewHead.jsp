<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
    <%
      String indexfollow = "index,follow";
      String seoPageName = "WUNI READ REVIEW PAGE";
      String canonUrl = "";  
      String previousUrl ="", nextUrl ="", pageURL = "", orderByUrl = "", orderBy =""; 
      String collegeDisplayName = !GenericValidator.isBlankOrNull((String)request.getAttribute("interactionCollegeNameDisplay")) ? (String)request.getAttribute("interactionCollegeNameDisplay") : "";
      String reviewRating = !GenericValidator.isBlankOrNull((String)request.getAttribute("reviewRating")) ? (String)request.getAttribute("reviewRating") : "0";
      String reviewCount = !GenericValidator.isBlankOrNull((String)request.getAttribute("reviewCnt")) ? (String)request.getAttribute("reviewCnt") : "0";
      String overAllRating = !GenericValidator.isBlankOrNull((String)request.getAttribute("overAllRating")) ? (String)request.getAttribute("overAllRating") : "0";
      String whatuniHeaderStyleCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);  
      String whatuniMainStyleCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.styles.css", null);
      String totalRecordCount = (request.getAttribute("totalRecordCount")!=null) ? request.getAttribute("totalRecordCount").toString() : "0";  
      String pageNo = !GenericValidator.isBlankOrNull((String)request.getAttribute("page")) ? (String)request.getAttribute("page") : "1";
      int pageVal = Integer.parseInt(pageNo);
      int noOfPage = (Integer.parseInt(totalRecordCount)/5);
      if((Integer.parseInt(totalRecordCount)%5) > 0) {
        noOfPage++; 
      }
      boolean provRevPageFlag = false;
      boolean SearchRevPageFlag = false;
      
      String collegeId = "0", refineSubjectId = "", refineKeyword = "";
      if(request.getAttribute("collegeId")!=null && !"".equals(request.getAttribute("collegeId"))){
        collegeId = (String)request.getAttribute("collegeId");
      }  
      if(request.getAttribute("refSubjectId")!=null && !"".equals(request.getAttribute("refSubjectId"))){
        refineSubjectId = (String)request.getAttribute("refSubjectId");
      }    
      if(request.getAttribute("reviewSearchKeyword")!=null && !"".equals(request.getAttribute("reviewSearchKeyword"))){
        refineKeyword = (String)request.getAttribute("reviewSearchKeyword");
      }      
      //START :: condition for the page specific seo
      if(request.getAttribute("reviewCanonicalUrl")!=null && !"".equals(request.getAttribute("reviewCanonicalUrl"))){
        pageURL = (String)request.getAttribute("reviewCanonicalUrl");
        canonUrl = pageURL; 
      }
      if(request.getAttribute("tmpOrderBy")!=null && !"".equals(request.getAttribute("tmpOrderBy"))){          
        orderBy = (String)request.getAttribute("tmpOrderBy");
        orderByUrl = "orderby="+orderBy;
        indexfollow = "noindex,follow";
      }
      if(request.getAttribute("provSrchesult")!=null && "true".equals(request.getAttribute("provSrchesult"))){
        provRevPageFlag = true;
        seoPageName = "WUNI PROVIDER REVIEW PAGE";      
      }else if(request.getAttribute("searchResult")!=null && "true".equals(request.getAttribute("searchResult"))){
        SearchRevPageFlag = false;
        indexfollow = "noindex,follow";
        seoPageName = "WUNI REVIEW SEARCH PAGE";
      }      
      if(pageVal>1){
        indexfollow = "noindex,follow";
        previousUrl = pageURL + (!"".equals(orderByUrl) ? "?"+orderByUrl+"&" : "?") + "pageno="+ String.valueOf(pageVal-1);
        nextUrl     = pageURL + (!"".equals(orderByUrl) ? "?"+orderByUrl+"&" : "?") + "pageno="+ String.valueOf(pageVal+1);
      }
      if(pageVal==noOfPage){
        previousUrl = pageURL + (!"".equals(orderByUrl) ? "?"+orderByUrl+"&" : "?") + "pageno="+ String.valueOf(pageVal-1);
        nextUrl     = "";
      }
      //END :: condition for the page specific seo
      
    %>    
    <%@include  file="/jsp/common/includeMainCSS.jsp"%>
    <link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=whatuniHeaderStyleCssName%>" media="screen">
    <link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=whatuniMainStyleCssName%>" media="screen">   
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/> 
    <%if(!GenericValidator.isBlankOrNull(canonUrl) && !SearchRevPageFlag){%>
      <link rel="canonical" href="<%=canonUrl%>"/>
    <%}%> 
    <%if(!"".equals(previousUrl)){%>
      <link rel="prev" href="<%=previousUrl%>" />
    <%}%>
    <%if(!"".equals(nextUrl)){%>
      <link rel="next" href="<%=nextUrl%>" />
    <%}%>
    <%--START :: SEO functionality for the review page--%>
    <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="<%=collegeId%>" />    
      <jsp:param name="pageName" value="<%=seoPageName%>" /> 
      <jsp:param name="paramFlag" value="COURSE_RESULTS" />      
      <jsp:param name="categoryCode" value="<%=refineSubjectId%>"/>  
      <jsp:param name="searchKeyword" value="<%=refineKeyword%>"/>
      <jsp:param name="noindexfollow" value="<%=indexfollow%>"/>
    </jsp:include>
    <%--END :: SEO functionality for the review page--%>    
    <%--START :: schema tag gor the provider review result page--%>
    <%if(provRevPageFlag){%>
      <jsp:include page="/jsp/common/includeSchemaTag.jsp">
        <jsp:param name="pageName" value="REVIEW_SCHEMA"/>
        <jsp:param name="collegeDisplayName" value="<%=collegeDisplayName%>"/>
        <jsp:param name="reviewCount" value="<%=reviewCount%>"/>
        <jsp:param name="reviewRating" value="<%=reviewRating%>"/>
      </jsp:include>
    <%}%>
    <%--END :: schema tag gor the provider review result page--%>        
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>    
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/review/'/><spring:message code='wuni.review.js'/>"></script>
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>    
    <jsp:include page="/jsp/review/include/includeReviewResponsive.jsp"/>