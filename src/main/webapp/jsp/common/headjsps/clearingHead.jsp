<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants,java.util.*"%>
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
    <meta name="format-detection" content="telephone=no"/>
    <%
      CommonFunction commonFun = new CommonFunction();
      request.setAttribute("showlightbox","true");
      ResourceBundle rb = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources"); 
      String clsrMainStylesCss = CommonUtil.getResourceMessage("wuni.clsr.main.styles.css", null);
      String wuniMainHeaderCss = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);
      String wuniMobileStyleCss = CommonUtil.getResourceMessage("wuni.whatuni.mobile.styles.css", null);
      String clearingHomeJs = CommonUtil.getResourceMessage("wuni.common.clearing.home.js", null);
      String gradeFilterUcasJs = CommonUtil.getResourceMessage("wuni.ucas.grader.filter.js", null);
      String lazyLoadJs = rb.getString("wuni.lazyLoadJs.js");  
      String homeJsName = CommonUtil.getResourceMessage("wuni.homeJsName.js", null);
      String envronmentName = commonFun.getWUSysVarValue("WU_ENV_NAME");
      String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
      String canonicalUrl = httpStr + "www.whatuni.com/university-clearing/"; //canonical url change for jun_5_2018 release
      pageContext.setAttribute("clearingHomeJsName", clearingHomeJs);
      pageContext.setAttribute("getJsDomainValue", CommonUtil.getJsPath());
      
    %>
    <link rel="canonical" href="<%=canonicalUrl%>"/> 
      <jsp:include page="/include/seoTitle.jsp">
          <jsp:param name="collegeId" value="0"/>
          <jsp:param name="pageName" value="WU CLEARING HOME"/>
          <jsp:param name="paramFlag" value="WU CLEARING HOME" />  
          <jsp:param name="noindexfollow" value="index,follow" />  
      </jsp:include> 
   
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuniMainHeaderCss%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=clsrMainStylesCss%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=wuniMobileStyleCss%>" media="screen">

<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>    
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script> 
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/home/<%=homeJsName%>"></script> 
<script type="text/javascript" language="javascript" src="${getJsDomainValue}/js/clearing/${clearingHomeJsName}"></script>  
<c:if test="${'ON' eq sessionScope.wuClearingOnOff}"> 
<script type="text/javascript" language="javascript" id="gradeFilterUcasJsId" src="<%=CommonUtil.getJsPath()%>/js/clearing/<%=gradeFilterUcasJs%>"></script>
</c:if>
<jsp:include page="/jsp/common/includeIconImg.jsp"/> 

<jsp:include page="/jsp/common/abTesting.jsp"/>
<%if(request.getAttribute("showBanner")==null){%>
  <jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
<%}%>
<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
  <jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>
<%-- Facebook pixel tracking script included in head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/facebookPixelTracking.jsp"/>


<script type="text/javascript" language="javascript">
  var dev = jQuery.noConflict();
  dev(document).ready(function(){
  adjustStyle();
  });
  adjustStyle();
  function jqueryWidth() {
    return dev(this).width();
  } 
  function adjustStyle() {
      var width = document.documentElement.clientWidth;
      var path = ""; 
      if (width <= 480) {
      if(document.getElementById("lightBoxResp")){
          document.getElementById("lightBoxResp").value = "mobile";
        }
	    if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }
        if (dev("#ol").is(':visible')){
          var posleft = posLeft()+((pageWidth()-590)/2)-12;
          posleft = (posleft < 0) ? 0 : posleft;
          dev('#mbox').addClass("mob_vid_res").css({ left :posleft+'px'});
          dev('#flashbanner').addClass("mob_vid_flb");
        }
    } else if ((width > 480) && (width <= 992)) {  
    if(document.getElementById("lightBoxResp")){
          document.getElementById("lightBoxResp").value = "mobile";
        }
	   if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }       
        if (dev("#ol").is(':visible')){
          var posleft = posLeft()+((pageWidth()-590)/2)-12;
          posleft = (posleft < 0) ? 0 : posleft;
          dev('#mbox').addClass("mob_vid_res").css({ left :posleft+'px'});
          dev('#flashbanner').addClass("mob_vid_flb");
        }

    } else {
      if(document.getElementById("lightBoxResp")){
          document.getElementById("lightBoxResp").value = "";
        }
        if (dev("#viewport").length > 0) {
            dev("#viewport").remove();
        }  
        if (dev("#ol").is(':visible')) {
            var posleft = posLeft()+((pageWidth()-590)/2)-12;
            dev('#mbox').removeClass("mob_vid_res").css({ left :posleft+'px'});
            dev('#flashbanner').removeClass("mob_vid_flb");
          }
        document.getElementById('size-stylesheet').href = "";
        dev(".hm_srchbx").hide();
    }
}
dev(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
    setTimeout(function() {
        dev(window).trigger('resize');
    }, 500);
}
dev(window).resize(function() {
  var screenWidth = jqueryWidth();  
      if (screenWidth <= 480) {
        if (dev("#ol").is(':visible')){
          var posleft = posLeft()+((pageWidth()-590)/2)-12;
          posleft = (posleft < 0) ? 0 : posleft;
          dev('#mbox').addClass("mob_vid_res").css({ left :posleft+'px'});
          dev('#flashbanner').addClass("mob_vid_flb");
        }
      }
      if ((screenWidth > 480) && (screenWidth <= 992)) {      //Tablet || Mobile view        
        if (dev("#ol").is(':visible')){
          var posleft = posLeft()+((pageWidth()-590)/2)-12;
          posleft = (posleft < 0) ? 0 : posleft;
          dev('#mbox').addClass("mob_vid_res").css({ left :posleft+'px'});
          dev('#flashbanner').addClass("mob_vid_flb");
        }
        //document.getElementById("mblOrient").value = "true";
      }else{        
        if (dev("#ol").is(':visible')) {
          var posleft = posLeft()+((pageWidth()-590)/2)-12;
          dev('#mbox').removeClass("mob_vid_res").css({ left :posleft+'px'});
          dev('#flashbanner').removeClass("mob_vid_flb");
        }
        //document.getElementById("mblOrient").value = "false";        
      }
  adjustStyle();  
});
function jqueryWidth() {
    return dev(this).width();
}
</script>