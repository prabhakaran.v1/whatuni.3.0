<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 

<title>Subscribe</title>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta name="description" content="Marketing Module">
<meta http-equiv="expires" content="">
<meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no"/>
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<jsp:include page="/jsp/common/includeIconImg.jsp"/>
<jsp:include page="/jsp/common/abTesting.jsp"/>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/unsubscirbe/'/><spring:message code='wuni.unsubscribe.js'/>"> </script>
<link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/wu_unsubscribe.css" type="text/css">