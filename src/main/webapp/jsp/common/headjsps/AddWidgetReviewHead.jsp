<%@page import="WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants, WUI.utilities.SessionData"%>
<%@page import="WUI.utilities.CommonUtil"%>
<%@page import="java.util.ResourceBundle"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<meta charset="UTF-8">
<%
  CommonFunction commonFun = new CommonFunction();
  String campaignId = request.getParameter("campaign") != null ? request.getParameter("campaign") : "";
  String ambassadorId = request.getParameter("amb") != null ? request.getParameter("amb") : "null";
  String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.  
  ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
  String widgetDomainPathHid = bundle.getString("wuni.whatuni.device.specific.css.path");
  String domainSpecificPath = httpStr + widgetDomainPathHid;  
  String cssPath = httpStr;
  String jsPath = httpStr;
  String imgPath = httpStr;
  String domainName = request.getServerName();
  if(domainName.indexOf("mtest.whatuni.com") > -1 || domainName.indexOf("mdev.whatuni.com") > -1) {
    cssPath += domainName;
    jsPath += domainName;
    imgPath += domainName;
  } else {
    cssPath += bundle.getString("wuni.cssdomain");
    jsPath += bundle.getString("wuni.jsdomain");
    String objPath=Integer.toString(1); 
    Object[] obj ={objPath}; 
    imgPath += CommonUtil.getResourceMessage("wuni.imagedomain", obj);
  }
  CommonUtil util = new CommonUtil();
  String WTCVersion = util.versionChanges(GlobalConstants.TERMS_AND_COND_VER);
  String WPRVersion = util.versionChanges(GlobalConstants.PRIVACY_VER);  
  String userId = ("null".equalsIgnoreCase(ambassadorId))? new SessionData().getData(request, "y") : "0";  
%>
 <link rel="canonical" href="<%=domainSpecificPath%>/university-course-reviews/add-review/"/>
 <jsp:include page="/include/seoTitle.jsp">
  <jsp:param name="collegeId" value="0"/>
  <jsp:param name="pageName" value="WU ADD REVIEW PAGE"/>
  <jsp:param name="paramFlag" value="WU_ADD_REVIEW_PAGE" />  
  <jsp:param name="noindexfollow" value="index,follow" />  
 </jsp:include>
 <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
 <link href='https://fonts.googleapis.com/css?family=Lato:400,700,700italic' rel='stylesheet' type='text/css'>
 <link rel="stylesheet" href="<%=cssPath%>/widget-cont/wu/css/font-awesome.min_221019.css">
 <%-- <link rel="stylesheet" href="<%=cssPath%>/widget-cont/wu/css/style.css">
 <link rel="stylesheet" type="text/css" href="<%=cssPath%>/widget-cont/wu/css/default.css">--%>
 <link rel="stylesheet" type="text/css" href="<%=cssPath%>/widget-cont/wu/css/rev_style_221019.css">
 <link rel="stylesheet" type="text/css" href="<%=cssPath%>/widget-cont/wu/css/rev_default_221019.css">
 <jsp:include page="/jsp/common/abTesting.jsp"/>
 <%-- GTM script included under head tag --%>
 <jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
	<jsp:param name="PLACE_TO_INCLUDE" value="HEAD"/>
 </jsp:include>
 <%-- GTM script included under head tag --%>
 <script type="text/javascript" src="<%=jsPath%>/widget-cont/wu/script/jquery.min.js"></script>
 <script type="text/javascript" src="https://connect.facebook.net/en_US/all.js"></script>
 <script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
   })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  <c:set var="googleAnalyticId" value="<%=GlobalConstants.GA_ACCOUNT%>"/>
   <c:choose>
    <c:when test="${'N' ne sessionScope.sessionData['cookiePerformanceDisabled']}">
      window['ga-disable-${googleAnalyticId}'] = true;
    </c:when>
    <c:when test="${'N' eq sessionScope.sessionData['cookiePerformanceDisabled']}">
      window['ga-disable-${googleAnalyticId}'] = false;
    </c:when>
  </c:choose>
   ga('create', 'UA-22939628-2', 'auto'); //TODO fortest TEST: UA-37421000-2 LIVE - UA-22939628-2 
   ga('require', 'displayfeatures');
   ga('set', 'dimension1', 'write a review');
   //ga('set', 'dimension2', 'whatuni'); //commented for the AUG_28_rel by Sangeeth.S
   ga('send', 'pageview');
 </script>
