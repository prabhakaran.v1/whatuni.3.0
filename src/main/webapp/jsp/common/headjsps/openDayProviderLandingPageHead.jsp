<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
  String indexfollow = "index,follow";
  String collegeId = "", canonUrl = "";
  String OPEN_DAY_HOME_URL = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + GlobalConstants.SEO_PATH_OPEN_DAY_PAGE;
%>

<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<jsp:include page="/jsp/openday/include/includeOpendayJSCSS.jsp">
  <jsp:param value="OPEN_DAY_PROVIDER_LANDING_PAGE" name="PAGE_NAME"/>
</jsp:include>
<%@include  file="/jsp/openday/include/includeOpenDayResponsive.jsp" %>
<c:if test="${not empty provCanonicalUrl}">
<jsp:include page="/jsp/common/includeSchemaTag.jsp">
  <jsp:param name="pageName" value="OPEN_DAY_PROVIDER_PAGE_SCHEMA"/>      
  <jsp:param name="od_home_url" value="<%=OPEN_DAY_HOME_URL%>"/>
  <jsp:param name="od_home_name" value="Open Days"/>
  <jsp:param name="od_prov_url" value="${provCanonicalUrl}"/>
  <jsp:param name="od_prov_name" value="Open Days at ${collegeNameDisplay}"/>
</jsp:include>
<link rel="canonical" href="${provCanonicalUrl}" />
</c:if>
