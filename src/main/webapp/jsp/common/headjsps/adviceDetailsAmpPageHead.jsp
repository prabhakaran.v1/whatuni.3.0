<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants" autoFlush="true"%>

<meta charset="utf-8">
  <%    
    CommonFunction commonFun = new CommonFunction();
    String metaTitle = (request.getAttribute("metaTitle")!=null) ? request.getAttribute("metaTitle").toString() : "";
    String metaKeywords = (request.getAttribute("metaKeywords")!=null) ? request.getAttribute("metaKeywords").toString() : "";
    String metaDescription = (request.getAttribute("metaDescription")!=null) ? request.getAttribute("metaDescription").toString() : ""; 
    String ogImage = (request.getAttribute("ogImage")!=null) ? request.getAttribute("ogImage").toString() : "";     
    String requestURL = (String)request.getAttribute("currentPageUrl");
    String ptURL = requestURL!=null? requestURL.replace("/degrees/" ,"/") :requestURL;
    String primaryCategory = (request.getAttribute("primaryCategory")!=null) ? request.getAttribute("primaryCategory").toString() : "";
    String articleId = (request.getAttribute("articleId")!=null) ? request.getAttribute("articleId").toString() : "";
    String parentCategory = (request.getAttribute("parentCategory")!=null) ? request.getAttribute("parentCategory").toString() : "";
    String secondaryCategory = (request.getAttribute("secondaryCategory")!=null) ? request.getAttribute("secondaryCategory").toString() : "";    
    String artPCategory = parentCategory.replaceAll("-"," ").toUpperCase();
    String artSCategory = secondaryCategory.replaceAll("-"," ").toUpperCase();
    String domainSpecificPath = commonFun.getSchemeName(request)+ CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
    String className = "artCont";
    String gaCollegeName = "";
    
    String enableUcasGuideRightPod = commonFun.getWUSysVarValue("UCAS_GUIDE_RIGHT_POD");
    if(!"".equals(metaTitle) && !"".equals(metaDescription)){%>
      <title><%=metaTitle%></title> 
      <meta name="description" content="<%=metaDescription%>"/> 
      <meta name="keywords" content="<%=metaDescription%>"/>
    <%}%>
    <!-- Commented this second DB call for SEO on 2021_JAN_19 rel by Sujitha V  -->
    <%-- <%}else{%>
      <jsp:include page="/include/seoTitle.jsp">
        <jsp:param name="collegeId" value="0"/>
        <jsp:param name="pageName" value="WHATUNI ARTICLE DETAIL PAGE"/> 
        <jsp:param name="paramFlag" value="WU_ARTICLE_PAGES"/>  
        <jsp:param name="articleId" value="<%=articleId%>"/>
        <jsp:param name="artParentName" value="<%=artPCategory%>"/>
        <jsp:param name="artSubName" value="<%=artSCategory%>"/>
      </jsp:include>
    <%}%> --%>
  <meta name="apple-itunes-app" content="app-id=1267341390"/>
  <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1" />
  <meta name="amp-google-client-id-api" content="googleanalytics">
  <link rel="canonical" href="<%=ptURL%>" />
  <link rel="shortcut icon" href="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/icons/wuicon.ico" type="image/x-icon" />
  <jsp:include page="/advice/amp/include/includeAmpCSS.jsp"/>