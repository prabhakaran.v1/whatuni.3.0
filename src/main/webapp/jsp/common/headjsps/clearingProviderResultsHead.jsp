<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.SessionData, WUI.utilities.GlobalConstants, WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, mobile.util.MobileUtils, java.util.ResourceBundle"%>


  <%String pageno="1"; %>
  <meta name="format-detection" content="telephone=no"/>
  <c:if test="${requestScope.pageno ne ''}">
  <%pageno=String.valueOf(request.getAttribute("pageno")); %>
  </c:if>
   <%String newsearchJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.newSearchResults.js");%>
  <% 
  ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
  String ecommerceTrackingJsName = bundle.getString("wuni.ecommerce.tracking.js");
  String clearingProviderJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.clearing.provider.js");
  String gradeFilterUcasJs = CommonUtil.getResourceMessage("wuni.ucas.grader.filter.js", null);
  String clearingSearchJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.clearing.search.js");
  String paramCollegeId = (request.getAttribute("collegeId") !=null && !request.getAttribute("collegeId").equals("null") && String.valueOf(request.getAttribute("collegeId")).trim().length()>0 ? String.valueOf(request.getAttribute("collegeId")) : ""); 
  String paramCategoryCode = (request.getAttribute("paramCategoryCode") !=null && !request.getAttribute("paramCategoryCode").equals("null") && String.valueOf(request.getAttribute("paramCategoryCode")).trim().length()>0 ? new CommonUtil().toUpperCase(String.valueOf(request.getAttribute("paramCategoryCode"))) : "");  
  String paramStudyLevelId = (request.getAttribute("paramStudyLevelId") !=null && !request.getAttribute("paramStudyLevelId").equals("null") && String.valueOf(request.getAttribute("paramStudyLevelId")).trim().length()>0 ? new CommonUtil().toUpperCase(String.valueOf(request.getAttribute("paramStudyLevelId"))) : ""); 
  //
  String first_mod_group_id = (String)request.getAttribute("first_mod_group_id");
  String noindexfollow = request.getAttribute("meta_robot") !=null ? String.valueOf(request.getAttribute("meta_robot"))  : "noindex,follow";
  
  String keyword = (request.getAttribute("searchText") !=null && !request.getAttribute("searchText").equals("null") && String.valueOf(request.getAttribute("searchText")).trim().length()>0 ? String.valueOf(request.getAttribute("searchText")) : "");  
  String keywordPhrase = request.getAttribute("searchText") != null ? request.getAttribute("searchText").toString() : "";
  // wu582_20181023 - Sabapathi: Added studymodeId for stats logging
  String studymodeId = request.getAttribute("studymodeId") != null ? request.getAttribute("studymodeId").toString() : "";
  String pros_search_name = request.getAttribute("prospectus_search_name") != null ? request.getAttribute("prospectus_search_name").toString() : "";
  String location = (request.getAttribute("location") !=null && !request.getAttribute("location").equals("null") && String.valueOf(request.getAttribute("location")).trim().length()>0 ? String.valueOf(request.getAttribute("location")) : "");  
  String paramFlag = paramStudyLevelId != null &&  paramStudyLevelId.equalsIgnoreCase("") ? "ALL_COURSE_RESULTS" : "COURSE_RESULTS";
  String pageNameValue = paramStudyLevelId != null &&  paramStudyLevelId.equalsIgnoreCase("") ? "ALL COURSE RESULTS" : "COURSE RESULTS";  
  String courseMappingPath = request.getAttribute("courseMappingPath") !=null ? "rsearch.html" : "csearch.html";
  String searchPosition = new WUI.utilities.CookieManager().getCookieValue(request, "sresult_provider_position");
  String studyLevelDesc = (String)request.getAttribute("studyLevelDesc");
  // added for 02nd Feb 2009 Release to show the canonicalURL for the SEO requirement 
  String canonicalURL = (String) request.getAttribute("clrCanonicalURL");
  canonicalURL  = canonicalURL !=null && canonicalURL.trim().length()>0 ? canonicalURL : "";

  // end of thecoce added  
  String paramCollegeName = (String) request.getAttribute("collegeName");
  paramCollegeName = paramCollegeName != null && !paramCollegeName.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(paramCollegeName).trim() : "";          
  
  String seoCollegeName =  paramCollegeName !=null && paramCollegeName.trim().length()>0 ? new CommonFunction().replaceHypen(new CommonFunction().replaceURL(paramCollegeName)).replaceAll(" ","-")+"-" : "";
  String seoCollegeNameLower  = new CommonUtil().toLowerCase(seoCollegeName);
  
  String collegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("collegeNameDisplay") : "";
  //
  String collegeLocation = (String) request.getAttribute("collegeLocation");
  collegeLocation = collegeLocation != null && !collegeLocation.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(collegeLocation).trim() : "";       
  //
  Object SEARCH_TYPE = request.getAttribute("SEARCH_TYPE");
  //    
  CommonFunction common = new CommonFunction();
  String courseUrl = "/all-courses/csearch?university=" + common.replaceHypen(common.replaceURL(paramCollegeName)).toLowerCase(); //Changed new all courses url pettern, By Thiyagu G for 27_Jan_2016.
  String newUniViewJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.uniview.js");//3_Jun_2014
  int searchhits = !GenericValidator.isBlankOrNull(String.valueOf(request.getAttribute("courseCount"))) ? Integer.parseInt(String.valueOf(request.getAttribute("courseCount"))) : 0; 
  String nextYear = GlobalConstants.NEXT_YEAR;
  String courseHeader = (String)request.getAttribute("courseHeader") != null ? request.getAttribute("courseHeader").toString() : "";
         courseHeader = courseHeader.replaceAll("Clearing", "");  
  String uniHomeUrl = "";
  String qString = (String)request.getAttribute("queryStr");
         qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  String clearingPage =  (String)request.getAttribute("searchClearing");
       clearingPage = GenericValidator.isBlankOrNull(clearingPage)?"":clearingPage;
  String isAdvertiser = request.getAttribute("isAdvertiser") != null && request.getAttribute("isAdvertiser").toString().trim().length() > 0 ? (String) request.getAttribute("isAdvertiser") : "";
  String advNonAdv = "";
  if("TRUE".equals(isAdvertiser)){
    advNonAdv = "Advertiser";
  }else{
    advNonAdv = "Non-Advertiser";
  }
  String evntProviderName = (paramCollegeName != null && paramCollegeName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(common.replaceSpecialCharacter(paramCollegeName))) : "").toLowerCase();
  //flag for to show canonical url in june_5_18 by Sangeeth.S
    String canonicalUrlFlag = request.getAttribute("CANONICAL_URL") != null ? request.getAttribute("CANONICAL_URL").toString() : "";
    boolean mobileFlag = new MobileUtils().userAgentCheck(request);
    String entryReqFilter = request.getAttribute("entryReqFilter") != null ? (String)request.getAttribute("entryReqFilter") : "";
    String link = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();
    request.setAttribute("showlightbox", "YES");
  %> 
  <c:if test="${'N' ne sessionScope.sessionData['cookieTargetingCookieDisabled']}">
    <%ecommerceTrackingJsName = CommonUtil.getResourceMessage("wuni.ecommerce.tracking.disabled.js", null);%>   
  </c:if>
  <c:if test="${empty requestScope.courseMappingPath}">
  <jsp:include page="/include/seoTitle.jsp">
  <jsp:param name="collegeId" value="<%=paramCollegeId%>"/>
  <jsp:param name="categoryCode" value="<%=paramCategoryCode%>"/>
  <jsp:param name="studyLevelId" value="<%=paramStudyLevelId%>"/>
  <jsp:param name="paramFlag" value="CLEARING PROVIDER RESULTS"/>
  <jsp:param name="pageName" value="CLEARING PROVIDER RESULTS"/>
  <jsp:param name="noindexfollow" value="<%=noindexfollow%>"/>
  <jsp:param name="searchKeyword" value="<%=keyword%>"/>
  <jsp:param name="locationCountyId" value="<%=location%>"/>
  <jsp:param name="pageNo" value="<%=pageno%>"/>
  </jsp:include> 
  </c:if>
  <jsp:include page="/jsp/search/searchredesign/includeSearchCSS.jsp" >
    <%-- <jsp:param name="isWhatuniGoClearingPage" value="Y"/> --%>
  </jsp:include>
  <jsp:include page="/jsp/common/includeResponsive.jsp">                                
    <jsp:param name="flag" value="spreDesign"/>
    <jsp:param name="prPageFlag" value="prPageFlag"/>
  </jsp:include>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=newsearchJSName%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=newUniViewJSName%>"></script>
 <script type="text/javascript" language="javascript" id="slimScrollJsId" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery.slimscroll.min.js"></script>
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/clearing/<%=clearingProviderJSName%>"></script>
  <script type="text/javascript" language="javascript" id="gradeFilterUcasJsId" src="<%=CommonUtil.getJsPath()%>/js/clearing/<%=gradeFilterUcasJs%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=ecommerceTrackingJsName%>"></script>
  <% if(!GenericValidator.isBlankOrNull(canonicalURL) && "index,follow".equalsIgnoreCase(noindexfollow) && (SEARCH_TYPE != null && SEARCH_TYPE.toString().equals("BROWSE_SEARCH"))  || "CANONICAL_URL".equalsIgnoreCase(canonicalUrlFlag)) { %>
  <link rel="canonical" href="<%=canonicalURL%>"/>       
  <% } %>  
  <%--Added schema tag for the open day event in PR page for Oct_20_18 rel by Sangeeth.S--%>
  <c:if test="${not empty requestScope.listOfOpendayInfo}">
    <jsp:include page="/jsp/common/includeSchemaTag.jsp">
      <jsp:param name="pageName" value="PROVIDER_RESULTS_OPENDAY_SCHEMA"/>
    </jsp:include>
  </c:if>
  <script type="text/javascript">
    var jQuery = jQuery.noConflict();
      jQuery(document).ready(function(){
        jQuery(".bcrmb_cnt .fl_lr a,.cle_sres .filt_cls a").click(function(){
           jQuery(".csr_res").toggleClass("csr_res_opn").toggleClass("csr_res_mov");
           jQuery("html").toggleClass("scrlpge_dis");
        }); 
      });
      
      
      jQuery(document).on('click touchstart', function (event) {
        if (jQuery(event.target).closest("#sortByDiv").length === 0 && jQuery(event.target).closest("#sortByOption").length === 0 ) {	
          jQuery("#sortByOption").hide(); 
          jQuery('#sortByOption').removeClass('actadd');
        }
      });
      
  </script>
