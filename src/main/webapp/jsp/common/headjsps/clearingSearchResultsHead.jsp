<%@page import="WUI.utilities.CommonFunction,WUI.utilities.CommonUtil,WUI.utilities.GlobalConstants, WUI.utilities.SessionData, org.apache.commons.validator.GenericValidator" autoFlush="true" %>

<%
  CommonFunction commonFun = new CommonFunction();
  String pageno = request.getAttribute("pageno") != null ? (String)request.getAttribute("pageno") : "1";
  String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "NULL";
  String paramlocationValue = (request.getAttribute("paramlocationValue") !=null && !request.getAttribute("paramlocationValue").equals("null") && String.valueOf(request.getAttribute("paramlocationValue")).trim().length()>0 ? String.valueOf(request.getAttribute("paramlocationValue")) : ""); 
  String seoLocationValue = (!GenericValidator.isBlankOrNull(paramlocationValue)) ? commonFun.replaceHypenWithSpace(paramlocationValue): "";  
  String paramSubjectCode = (request.getAttribute("paramSubjectCode") !=null && !request.getAttribute("paramSubjectCode").equals("null") && String.valueOf(request.getAttribute("paramSubjectCode")).trim().length()>0 ? String.valueOf(request.getAttribute("paramSubjectCode")) : "");  
  String paramStudyLevelId = (request.getAttribute("paramStudyLevelId") !=null && !request.getAttribute("paramStudyLevelId").equals("null") && String.valueOf(request.getAttribute("paramStudyLevelId")).trim().length()>0 ? String.valueOf(request.getAttribute("paramStudyLevelId")) : ""); 
  String noindexfollow = request.getAttribute("noindex") !=null ? "noindex,follow"  : "index,follow";
  String subjectDesc = request.getAttribute("subjectDesc") != null ? request.getAttribute("subjectDesc").toString() : "";   
  String newsearchJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.newSearchResults.js");
  String clearingSearchJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.clearing.search.js");
  String gradeFilterUcasJs = CommonUtil.getResourceMessage("wuni.ucas.grader.filter.js", null);
  String socialBoxJs = CommonUtil.getResourceMessage("wuni.social.box.js", null);
  String httpStr = commonFun.getSchemeName(request); 
  String canonicalLink = httpStr + "www.whatuni.com/degrees" + firstPageSeoUrl;
%>
 <meta name="format-detection" content="telephone=no"/>
 <%--<jsp:include page="/jsp/search/searchredesign/includeSearchCSS.jsp"/> --%>  
  <jsp:include page="/include/seoTitle.jsp">
    <jsp:param name="collegeId" value="0"/>
    <jsp:param name="locationCountyId" value="<%=seoLocationValue%>"/>
    <jsp:param name="categoryCode" value="<%=paramSubjectCode%>"/>
    <jsp:param name="studyLevelId" value="<%=paramStudyLevelId%>"/>
    <jsp:param name="entityText" value="1"/>
    <jsp:param name="paramFlag" value="CLEARING COLLEGE RESULTS"/>
    <jsp:param name="pageName" value="CLEARING COLLEGE RESULTS"/>
    <jsp:param name="noindexfollow" value="<%=noindexfollow%>"/>
    <jsp:param name="searchKeyword" value="<%=subjectDesc%>"/>
    <jsp:param name="pageNo" value="<%=pageno%>"/>            
  </jsp:include> 
  <jsp:include page="/jsp/common/includeResponsive.jsp">                                
    <jsp:param name="flag" value="spreDesign"/>
    <jsp:param name="isWhatuniGoClearingPage" value="Y"/>
  </jsp:include>
  <jsp:include page="/jsp/common/includeBasket.jsp" />
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=newsearchJSName%>"></script>
 <script type="text/javascript" language="javascript" id="slimScrollJsId" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery.slimscroll.min.js"></script>
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/clearing/<%=clearingSearchJSName%>"></script>
 <script type="text/javascript" language="javascript" id="gradeFilterUcasJsId" src="<%=CommonUtil.getJsPath()%>/js/clearing/<%=gradeFilterUcasJs%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=socialBoxJs%>"></script> 
 <%if(!GenericValidator.isBlankOrNull(canonicalLink)){%><%--13_MAY_2014_REL--%>
  <link rel="canonical" href="<%=canonicalLink%>"/> 
  <%}%>
  <%--Breadcrumb schema added by Prabha on 21.Mar.17--%>
  <jsp:include page="/jsp/common/includeSchemaTag.jsp">
    <jsp:param name="pageName" value="BREADCRUMB_SCHEMA"/>
  </jsp:include>
  <jsp:include page="/jsp/common/includeSchemaTag.jsp">
    <jsp:param name="pageName" value="WEB_PAGE_SCHEMA"/>
  </jsp:include>
  <%--End of breadcrumb schema code--%>
  <jsp:include page="/jsp/search/searchredesign/includeSearchCSS.jsp" >
    <jsp:param name="isWhatuniGoClearingPage" value="Y"/>
  </jsp:include>
  
  <script type="text/javascript">
    var jQuery = jQuery.noConflict();
      jQuery(document).ready(function(){
        jQuery(".bcrmb_cnt .fl_lr a,.cle_sres .filt_cls a").click(function(){
           jQuery(".csr_res").toggleClass("csr_res_opn").toggleClass("csr_res_mov");
           jQuery("html").toggleClass("scrlpge_dis");
        });
      });
     
       jQuery(document).on('click touchstart', function (event) {
        if (jQuery(event.target).closest("#sortByDiv").length === 0 && jQuery(event.target).closest("#sortByOption").length === 0 ) {		
          jQuery("#sortByOption").hide(); 
          jQuery('#sortByOption').removeClass('actadd');
        }
      });
  </script>
