<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.SessionData,WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, mobile.util.MobileUtils, java.util.ResourceBundle" autoFlush="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<% 
ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
String pageName = request.getAttribute("pageName") != null ? (String)request.getAttribute("pageName") : null;
String q =!GenericValidator.isBlankOrNull((String)request.getAttribute("q")) ? (String)request.getAttribute("q") : "";
String university = !GenericValidator.isBlankOrNull((String)request.getAttribute("university")) ? (String)request.getAttribute("university") : "";
String subject = !GenericValidator.isBlankOrNull((String)request.getAttribute("subject")) ? (String)request.getAttribute("subject") : "";
String location = !GenericValidator.isBlankOrNull((String)request.getAttribute("location")) ? (String)request.getAttribute("location") : "";
String userScore = !GenericValidator.isBlankOrNull((String)request.getParameter("score")) ? (String)request.getParameter("score") : "0";

String exitOpenDayRes = "" , odEvntAction = "false";
    if(session.getAttribute("exitOpRes")!=null && !"".equals(session.getAttribute("exitOpRes"))){
      exitOpenDayRes = (String)session.getAttribute("exitOpRes");
    }    
    if(!"".equals(exitOpenDayRes) && "true".equals(exitOpenDayRes)){
      odEvntAction = "true";
    }
  String pageno = request.getAttribute("pageno") != null ? (String)request.getAttribute("pageno") : "1";
  String urldata_1 = (String)request.getAttribute("URL_1");
  String urldata_2 = (String)request.getAttribute("URL_2");
  String urlString  = urldata_1 + pageno + urldata_2;
  String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "NULL";
  String searchhits = (String) request.getAttribute("universityCount");
  String paramlocationValue = (request.getAttribute("paramlocationValue") !=null && !request.getAttribute("paramlocationValue").equals("null") && String.valueOf(request.getAttribute("paramlocationValue")).trim().length()>0 ? String.valueOf(request.getAttribute("paramlocationValue")) : ""); 
  String paramSubjectCode = (request.getAttribute("paramSubjectCode") !=null && !request.getAttribute("paramSubjectCode").equals("null") && String.valueOf(request.getAttribute("paramSubjectCode")).trim().length()>0 ? String.valueOf(request.getAttribute("paramSubjectCode")) : "");  
  String paramStudyLevelId = (request.getAttribute("paramStudyLevelId") !=null && !request.getAttribute("paramStudyLevelId").equals("null") && String.valueOf(request.getAttribute("paramStudyLevelId")).trim().length()>0 ? String.valueOf(request.getAttribute("paramStudyLevelId")) : ""); 
  String universityCount = (request.getAttribute("universityCount") !=null && !request.getAttribute("universityCount").equals("null") && String.valueOf(request.getAttribute("universityCount")).trim().length()>0 ? String.valueOf(request.getAttribute("universityCount")) : ""); 
  String resultExists = (request.getAttribute("resultExists") !=null && !request.getAttribute("resultExists").equals("null") && String.valueOf(request.getAttribute("resultExists")).trim().length()>0 ? String.valueOf(request.getAttribute("resultExists")) : ""); 
  String qualDesc = (request.getAttribute("qualDesc") !=null && !request.getAttribute("qualDesc").equals("null") && String.valueOf(request.getAttribute("qualDesc")).trim().length()>0 ? String.valueOf(request.getAttribute("qualDesc")) : "");   
  String tmpUcasCode = (request.getAttribute("paramUcasCode") !=null && !request.getAttribute("paramUcasCode").equals("null") && String.valueOf(request.getAttribute("paramUcasCode")).trim().length()>0 ? String.valueOf(request.getAttribute("paramUcasCode")) : "");
  String jacsSubjectName = (String)request.getAttribute("jacsSubjectName");
         jacsSubjectName = (!GenericValidator.isBlankOrNull(jacsSubjectName))?jacsSubjectName:"";
  String searchLocation = (String)request.getAttribute("SEARCH_LOCATION_OR_POSTCODE") != null ? (String)request.getAttribute("SEARCH_LOCATION_OR_POSTCODE") : "";
  String noindexfollow = null;
  if("CLEARING_SEARCH_RESULTS".equals(pageName)) {
	  noindexfollow = request.getAttribute("meta_robot") !=null ? String.valueOf(request.getAttribute("meta_robot"))  : "noindex,follow";
  } else {
	  noindexfollow = request.getAttribute("noindex") != null ? "TRUE"  : "FALSE";
  }
  if(firstPageSeoUrl.contains("module=")){
    noindexfollow = "TRUE";
  }
  String studyModeValue = (String) request.getAttribute("hitbox_study_mode");
  if(studyModeValue == null){studyModeValue = "";}
  CommonUtil comUtil = new CommonUtil();  
  CommonFunction commonFun = new CommonFunction();
  String subjectDesc = "";
  if("CLEARING_SEARCH_RESULTS".equals(pageName)) {
	  subjectDesc = request.getAttribute("paramSubjectDesc") != null ? request.getAttribute("paramSubjectDesc").toString() : ""; 
  } else {
	  subjectDesc = request.getAttribute("subjectDesc") != null ? request.getAttribute("subjectDesc").toString() : "";   
  }
  int articleLength = 0;
  if(subjectDesc!=null && subjectDesc!=""){
    articleLength = subjectDesc.length();
  }
  String articleText = subjectDesc;
  if(articleLength > 15){
    articleText = subjectDesc.substring(0,13) + "...";
  }
  String studyLevelDesc = request.getAttribute("studyLevelDesc") != null ? (String)request.getAttribute("studyLevelDesc") : ""; 
  String studeyLevel = studyLevelDesc;
  String studyLevelOnly = studyLevelDesc;
  String studyLevelTab = studyLevelDesc;
  if(studyLevelTab.indexOf("degree")==-1){
        studyLevelTab = studyLevelTab.indexOf("hnd/hnc") > -1 ? studyLevelTab.toUpperCase() :studyLevelTab;
        studyLevelTab = studyLevelTab + " course";
  }else{
	  studyLevelTab = " course";
  }
  if(!GenericValidator.isBlankOrNull(studeyLevel)){  //29-Oct-2013 SEO Release - Start of change            
     if(studeyLevel.indexOf("degree")==-1){
        studeyLevel = studeyLevel.indexOf("hnd/hnc") > -1 ? studeyLevel.toUpperCase() :studeyLevel;
        studeyLevel = studeyLevel + " degrees";
     }else{
        studeyLevel = studeyLevel.replaceFirst("degree", "Degrees");
        }              
      } //29-Oct-2013 SEO Release - End of change 
  String first_mod_group_id = (String)request.getAttribute("first_mod_group_id");
         first_mod_group_id = (first_mod_group_id!=null && first_mod_group_id.trim().length() >0) ? first_mod_group_id : "";
  String totalCourseCount = (String) request.getAttribute("totalCourseCount"); 
         totalCourseCount = GenericValidator.isBlankOrNull(totalCourseCount)?"":totalCourseCount;
  String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
  String canonicalLink = httpStr + "www.whatuni.com/degrees" + firstPageSeoUrl;
  int totalCount = 0;
  if(totalCourseCount!=""){
    totalCount =  Integer.parseInt(totalCourseCount);
  }
  studyLevelTab = totalCount > 1? studyLevelTab+"s": studyLevelTab;
  String firstTabText = totalCourseCount + " " + studyLevelTab;
  String newsearchJSName = bundle.getString("wuni.newSearchResults.js");
  String clearingsearchJSName = bundle.getString("wuni.clearing.search.js");
  String gradeFilterUcasJs = CommonUtil.getResourceMessage("wuni.ucas.grader.filter.js", null);
  String lazyLoadJs = bundle.getString("wuni.lazyLoadJs.js");
  String pgsRelCanonicalLink = (String)request.getAttribute("pgsCanonicalURL");//13_MAY_2014_REL
  if(!GenericValidator.isBlankOrNull(paramStudyLevelId)){
   canonicalLink = GenericValidator.isBlankOrNull(pgsRelCanonicalLink)?"":pgsRelCanonicalLink;
  }
  String clearingYear = GlobalConstants.CLEARING_YEAR;  
  String clearingonoff = commonFun.getWUSysVarValue("CLEARING_ON_OFF");
  String qString = (String)request.getAttribute("queryStr");
        qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  String clrString = qString;
  if("ON".equalsIgnoreCase(clearingonoff)){
   clrString = !GenericValidator.isBlankOrNull(clrString) ? (clrString.replace("?","?clearing&")):"?clearing";  
  }
  String filter = commonFun.getWUSysVarValue("FILTER");
  boolean mobileFlag = new MobileUtils().userAgentCheck(request);
  String socialBoxJs = CommonUtil.getResourceMessage("wuni.social.box.js", null);
  String ecommerceTrackingJsName = bundle.getString("wuni.ecommerce.tracking.js");
  String featuredBrandJsName = bundle.getString("wuni.sr.featuredbrand.js");
  if("CLEARING_SEARCH_RESULTS".equals(pageName) && "index,follow".equalsIgnoreCase(noindexfollow)) {
    canonicalLink = httpStr + "www.whatuni.com" + firstPageSeoUrl; 
  }
  if("true".equalsIgnoreCase(noindexfollow)){
	pageContext.setAttribute("META_ROBOTS_NOINDEX_FOLLOW", "noindex,follow");  
  }
%>
<c:if test="${'N' ne sessionScope.sessionData['cookieTargetingCookieDisabled']}">
  <%ecommerceTrackingJsName = CommonUtil.getResourceMessage("wuni.ecommerce.tracking.disabled.js", null);%>   
</c:if>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
 <jsp:include page="/jsp/search/searchredesign/includeSearchCSS.jsp"/>
 <input type="hidden" name="gaAccount" value="UA-22939628-2" id="gaAccount"/>
 <input type="hidden" id="jsPathHidden" value="<%=CommonUtil.getJsPath()%>/js/"/>
 
 <jsp:include page="/jsp/common/includeResponsive.jsp">                                
    <jsp:param name="flag" value="spreDesign"/>
  </jsp:include>
 
  <jsp:include page="/jsp/common/includeBasket.jsp" />
  <c:if test="${pageName ne 'CLEARING_SEARCH_RESULTS'}">
   <link rel="preload" as="script" href="<%=CommonUtil.getJsPath()%>/js/<%=newsearchJSName%>">
   <%-- <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=newsearchJSName%>"></script> --%>
 </c:if>
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/wu_breakingJsInPreLoad_20210119.js"></script> 
 <c:if test="${pageName eq 'CLEARING_SEARCH_RESULTS'}">
 <script type="text/javascript" language="javascript" id="slimScrollJsId" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery.slimscroll.min.js"></script>
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/clearing/<%=clearingsearchJSName%>"></script>
 <script type="text/javascript" language="javascript" id="gradeFilterUcasJsId" src="<%=CommonUtil.getJsPath()%>/js/clearing/<%=gradeFilterUcasJs%>"></script>
 </c:if>
 <%-- <link rel="preload" as="script" href="<%=CommonUtil.getJsPath()%>/js/<%=ecommerceTrackingJsName%>"> --%>
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=ecommerceTrackingJsName%>"></script> 
 <link rel="preload" as="script" href="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=socialBoxJs%>">
 <%-- <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=ecommerceTrackingJsName%>"></script>  --%>
 <%-- <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=socialBoxJs%>"></script>   --%>
 <c:if test="${not empty requestScope.featuredBrandList and pageName ne 'CLEARING_SEARCH_RESULTS'}">
   <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=featuredBrandJsName%>"></script>
 </c:if>
 <c:if test="${pageName eq 'CLEARING_SEARCH_RESULTS'}">
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
 </c:if> 
 <c:choose>
    <c:when test="${pageName eq 'CLEARING_SEARCH_RESULTS'}">    
      <c:set var="paramFlag" value="CLEARING COLLEGE RESULTS"/>
      <c:set var="pageName" value="CLEARING COLLEGE RESULTS"/>
    </c:when>
    <c:otherwise>
      <c:set var="paramFlag" value="SEARCH_RESULTS"/>
      <c:set var="pageName" value="COLLEGE RESULTS"/>    
    </c:otherwise>
  </c:choose>
   
  <c:choose>
   <c:when test="${pageName ne 'COLLEGE RESULTS'}">
    <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="0"/>
      <jsp:param name="locationCountyId" value="<%=paramlocationValue%>"/>
      <jsp:param name="categoryCode" value="<%=paramSubjectCode%>"/>
      <jsp:param name="studyLevelId" value="<%=paramStudyLevelId%>"/>
      <jsp:param name="entityText" value="1"/>
      <jsp:param name="paramFlag" value="${paramFlag}"/>
      <jsp:param name="pageName" value="${pageName}"/> 
      <jsp:param name="noindexfollow" value="<%=noindexfollow%>"/>
      <jsp:param name="searchKeyword" value="<%=subjectDesc%>"/>
      <jsp:param name="pageNo" value="<%=pageno%>"/>
      <jsp:param name="totalProviderCount" value="${universityCount}"/>
      <jsp:param name="totalCourseCount" value="${totalCourseCount}"/>            
    </jsp:include> 
    </c:when>
    <c:otherwise>
      <c:if test="${not empty META_TITLE}" >
        <title>${META_TITLE}</title>
      </c:if>
      <c:if test="${not empty META_DESC}" >
        <meta name="description" content="${META_DESC}"/>
      </c:if>
      <%-- <c:if test="${not empty META_KEYWORDS}" >
        <meta name="keywords" content="${META_KEYWORDS}"/>
      </c:if> --%>
      <c:choose>
        <c:when test="${not empty META_ROBOTS}" >
          <c:set var="META_ROBOTS_VALUE" value="${META_ROBOTS}"/>         
        </c:when>
        <c:when test="${not empty META_ROBOTS_NOINDEX_FOLLOW}" >
          <c:set var="META_ROBOTS_VALUE" value="${META_ROBOTS_NOINDEX_FOLLOW}"/>  
        </c:when>
        <c:otherwise>
          <c:set var="META_ROBOTS_VALUE" value="index,follow"/> 
        </c:otherwise>
      </c:choose>
        <meta name="ROBOTS" content="${META_ROBOTS_VALUE}"/>
      <meta name="country" content="United Kingdom"/> 
      <meta name="content-type" content="text/html; charset=utf-8"/>
    </c:otherwise>  
  </c:choose>
 <%if(!GenericValidator.isBlankOrNull(canonicalLink)){%><%--13_MAY_2014_REL--%>
  <link rel="canonical" href="<%=canonicalLink%>"/> 
  <%}%>
  <%--Breadcrumb schema added by Prabha on 21.Mar.17--%>
  <jsp:include page="/jsp/common/includeSchemaTag.jsp">
    <jsp:param name="pageName" value="BREADCRUMB_SCHEMA"/>
  </jsp:include>
  <jsp:include page="/jsp/common/includeSchemaTag.jsp">
    <jsp:param name="pageName" value="WEB_PAGE_SCHEMA"/>
  </jsp:include>
