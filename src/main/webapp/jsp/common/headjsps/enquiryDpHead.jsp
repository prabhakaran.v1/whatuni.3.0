  <jsp:include page="/include/htmlTitle.jsp">
    <jsp:param name="indexFollowFlag" value="noindex,follow"/>
  </jsp:include>
  <%@include  file="/jsp/common/includeMainCSS.jsp" %>
  
  
  <%
  
  String emailDomainJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.email.domain.js");
  String facebookLoginJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.facebook.login.js");
  String commonUserProfileJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.user.profile.js");
  
  %>
  
   <jsp:include page="/jsp/common/includeResponsive.jsp"/>
  <script type="text/javascript" language="javascript" src="//connect.facebook.net/en_US/all.js"> </script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=facebookLoginJSName%>"> </script>  
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=commonUserProfileJSName%>"> </script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/PostcodeValidate.js"> </script>   
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/modal-message.js"> </script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/emaildomain/<%=emailDomainJs%>"> </script>