<%@page import="WUI.utilities.CommonUtil, java.util.ArrayList, WUI.utilities.CommonFunction, WUI.utilities.SessionData, java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta http-equiv="content-language" content=" en-gb "/>
<meta content="utf-8" http-equiv="encoding">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">

<%@include  file="/jsp/common/includeMainCSS.jsp" %>
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
<%String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
     String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
    String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
    String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
    String adviceJsName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.adviceJsName.js");
      String lazyLoadJsName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJsName.js");
  String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
  
    %>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<script type="text/javascript" language="javascript">
var dev = jQuery.noConflict();
adjustStyle();
function jqueryWidth() {
  return dev(this).width();
} 
function adjustStyle() {
    var width = document.documentElement.clientWidth;
    var path = ""; 
    dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
    dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
    if (width <= 480) {
	    if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }
        <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}%>
    } else if ((width > 480) && (width <= 992)) {         
	   if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }       
       <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}%>
    } else {
        if (dev("#viewport").length > 0) {
            dev("#viewport").remove();
        }       
        document.getElementById('size-stylesheet').href = "";  
        dev(".hm_srchbx").hide();
    }
}
dev(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
    setTimeout(function() {
        $(window).trigger('resize');
    }, 500);
}
dev(window).resize(function() {
  adjustStyle();  
});
</script>
<%--<jsp:include  page="/jsp/home/include/includeHeaderResponsive.jsp"/>--%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJsName%>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=adviceJsName%>"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/downloadpdf_wu564.js"></script>
<%
    CommonFunction common = new CommonFunction();
    String mappingPath = (request.getAttribute("mappingPath")!=null) ? request.getAttribute("mappingPath").toString() : "";
    String tags = (request.getAttribute("tags")!=null) ? request.getAttribute("tags").toString() : "";
    if(tags!=null || !"".equals(tags)){
        tags = common.replaceHypen(common.replaceURL(tags)).toLowerCase();    
    }
    
%>
<c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
<script type="text/javascript">
    var googletag = googletag || {};
    googletag.cmd = googletag.cmd || [];
    (function() {
       var gads = document.createElement("script");
           gads.async = true;
           gads.type = "text/javascript";
      var useSSL = "https:" == document.location.protocol;
      gads.src = (useSSL ? "https:" : "http:") + "//www.googletagservices.com/tag/js/gpt.js";
      var node =document.getElementsByTagName("script")[0];
          node.parentNode.insertBefore(gads, node);
    })();
</script>
<script>
  googletag.cmd.push(function() {
  googletag.defineSlot('/1029355/wuni_subjectguide_mpuslot_300x250', [300, 250], 'div-gpt-ad-subject_guide-0')
  .addService(googletag.pubads());
  googletag.pubads().setTargeting("keyword","<%=tags%>");
googletag.pubads().collapseEmptyDivs();
  googletag.pubads().enableSingleRequest();
  googletag.enableServices();
  });
</script>
</c:if>
