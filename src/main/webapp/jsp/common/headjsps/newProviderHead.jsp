  <%@page import="java.util.ResourceBundle"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
<meta name="format-detection" content="telephone=no"/>
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">  
  <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
  <%
    String robotValue       =  "index,follow";
    String showBanner       =  "";
    String providerId       = (String)request.getAttribute("interactionCollegeId");
    String newUniPageName   = (String)request.getAttribute("newUniPageName");
    String newUniParamFlag  = (String)request.getAttribute("newUniParamFlag");    
    String mainSecStyle     = "rich_profile clr_prof_cnt nor_intr_btn";    
    String canonicalUrl = request.getAttribute("canonicalUrl")!=null ? request.getAttribute("canonicalUrl").toString().trim() : "";
    if(request.getAttribute("highlightTab")!=null && "CLEARING".equals((String)request.getAttribute("highlightTab"))){    
      mainSecStyle = "rich_profile clr_prof_cnt";
    }
    String nonAdvertFlag = request.getAttribute("nonAdvertFlag")!=null ? (String)request.getAttribute("nonAdvertFlag") : "";
    if(request.getAttribute("nonAdvertFlag")!=null && "true".equals((String)request.getAttribute("nonAdvertFlag"))){    
      showBanner = "ad_cnr";
    } 
    String spMyhcProfileId = "";
    String profileType = (String)request.getAttribute("setProfileType");
    if("SUB_PROFILE".equalsIgnoreCase(profileType)){
      spMyhcProfileId = (String)request.getAttribute("spMyhcProfileId");//Added by Indumathi NOV-03-15 for sp profile
    }
    ResourceBundle rb = ResourceBundle.getBundle("com.resources.ApplicationResources");
    String heroImageJsName = rb.getString("wuni.heroImgJsName.js");//Added hero image js Indumathi.S Apr_19_2016
    String reviewlightboxJs = CommonUtil.getResourceMessage("wuni.reviewlightbox.js", null);
    String uniProfileLandingJS = CommonUtil.getResourceMessage("wuni.uni.profile.landing.js", null);//changed dynaic for Feb_12_19 rel by Sangeeth.S
    String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
    //Added by Indumathi.S For pageview logging July-26-2016
   int stind1 = request.getRequestURI().lastIndexOf("/");
   int len1 = request.getRequestURI().length();
   String pageName = pageContext.getAttribute("pagename3") != null ? (String)pageContext.getAttribute("pagename3") : request.getRequestURI().substring(stind1+1,len1);
   pageName = pageName !=null ? pageName.toLowerCase() : pageName;
   String gaCollegeName = (String)request.getAttribute("hitbox_college_name");
         gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(gaCollegeName);
         gaCollegeName = !GenericValidator.isBlankOrNull(gaCollegeName) ? !gaCollegeName.equals("null") ? gaCollegeName.toLowerCase().replaceAll(" ","-") : "" : "";
  if((!GenericValidator.isBlankOrNull(pageName)) && "newproviderhome.jsp".equals(pageName)){
    String newProfileType = (String)request.getAttribute("setProfileType");
    if((!GenericValidator.isBlankOrNull(newProfileType)) && "NORMAL".equals(newProfileType)){
      pageName = "uniview.jsp";
    } 
  }
  
  %>  
  
  <%@include  file="/jsp/common/includeMainCSS.jsp"%> 
  <jsp:include page="/jsp/search/include/searchGoogleAdSlots.jsp"/>
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>        
  <jsp:include  page="/jsp/newproviderhome/include/switchProfileResponsive.jsp"/>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=heroImageJsName%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/review/<%=reviewlightboxJs%>"></script>
  <c:if test="${not empty requestScope.newUniPageName}">
    <jsp:include page="/jsp/common/includeSeoMetaDetails.jsp">
      <jsp:param value="<%= robotValue%>" name="noindexfollow"/>
    </jsp:include>  
  </c:if>
  <%if(!GenericValidator.isBlankOrNull(canonicalUrl)){%>
    <link rel="canonical" href="<%=canonicalUrl%>"/>
  <%}%>
  <c:if test="${empty requestScope.newUniPageName}">
    <jsp:include page="/include/htmlTitle.jsp">
      <jsp:param name="indexFollowFlag" value ="noindex, nofollow"/>
    </jsp:include>
  </c:if>
  <%--Added schema tag on 16_May_2017, By Thiyagu G--%>
  <%if(!GenericValidator.isBlankOrNull(profileType) && ("NORMAL".equals(profileType) || "CLEARING_LANDING".equals(profileType) || "CLEARING_PROFILE".equals(profileType))){%>
  <jsp:include page="/jsp/common/includeSchemaTag.jsp">
    <jsp:param name="pageName" value="UNI_PROFILE_SCHEMA"/>
  </jsp:include>
  <%}%>