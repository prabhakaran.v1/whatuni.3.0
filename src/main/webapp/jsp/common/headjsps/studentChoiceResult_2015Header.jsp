  <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">  
  <%
    String indexfollow = "index,follow"; 
    String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
    String canonicalUrl = domainSpecificPath + "/student-awards-winners/university-of-the-year/";
    String loadYear = request.getAttribute("loadYear") != null ? (String)request.getAttribute("loadYear") : "";
    String currentAwardYear = request.getAttribute("currentAwardYear") != null ? (String)request.getAttribute("currentAwardYear") : "";
    if(!GenericValidator.isBlankOrNull(loadYear) && Integer.parseInt(loadYear) < Integer.parseInt(currentAwardYear)){
      canonicalUrl = domainSpecificPath + "/degrees/student-awards-winners/"+loadYear+".html";
    }
    String categoryName = request.getAttribute("categorName") != null && currentAwardYear.equals(loadYear) ? (String)request.getAttribute("categorName") : "";
    String pageName = currentAwardYear.equals(loadYear) ? "WHATUNI STUDENT AWARDS" : "WHATUNI AWARDS PAGE";
    String paramFlag = currentAwardYear.equals(loadYear) ? "WHATUNI STUDENT AWARDS" : "WU_HCSTUFF";
    String awardYear = loadYear;
    String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
    //Added uni details for retaining uni when we do the category search, By Thiyagu G for 21_March_2017.
    String uniId = request.getAttribute("uniId") != null ? (String)request.getAttribute("uniId") : "";
    String uniNameDisplay = request.getAttribute("uniNameDisplay") != null ? (String)request.getAttribute("uniNameDisplay") : "";
    String uniNameDisp = !GenericValidator.isBlankOrNull((String)request.getAttribute("uniNameDisplay")) ? (String)request.getAttribute("uniNameDisplay") : "Enter university name";
    String studChoiceAwardsJsName = CommonUtil.getResourceMessage("wuni.student.choice.awards.js", null);//getting Js name dynamically by Sangeeth.S for 20_Nov_18 rel
    
  %>  
  <%@include  file="/jsp/common/includeMainCSS.jsp"%> 
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>  
  
  <jsp:include  page="/jsp/studentawards/include/includeStudChoiceResponsive.jsp"/>   
  <link rel="canonical" href="<%=canonicalUrl%>"/>
  <jsp:include page="/include/seoTitle.jsp">    
    <jsp:param name="collegeId" value="0"/>
    <jsp:param name="pageName" value="<%=pageName%>" />   
    <jsp:param name="paramFlag" value="<%=paramFlag%>"/> 
    <jsp:param name="noindexfollow" value="<%=indexfollow%>"/>
    <jsp:param name="awardYear" value="<%=awardYear%>" />   
    <jsp:param name="categoryName" value="<%=categoryName%>"/> 
  </jsp:include>