<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<%
  String wuniGoHeaderStyleCss = CommonUtil.getResourceMessage("wuni.go.style.header.css", null); //cmm_header_styles_586
  String wuniGoStyleCss = CommonUtil.getResourceMessage("wuni.cmm.main.style.css", null); //cmm_main_styles_586
  String wuniGOMobileStyleCss = CommonUtil.getResourceMessage("wuni.go.mobile.css", null);    //cmm_mobile_styles_586
  String clrSubLandingJsName = CommonUtil.getResourceMessage("wuni.clearing.subject.landing.js", null);  
  
%>

<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuniGoHeaderStyleCss%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuniGoStyleCss%>" media="screen" />
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=wuniGOMobileStyleCss%>" media="screen" />

<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/whatunigo/<%=clrSubLandingJsName%>"> </script>   

<jsp:include page="/jsp/common/includeIconImg.jsp"/>

<jsp:include page="/jsp/common/abTesting.jsp"/>
<%if(request.getAttribute("showBanner")==null){%>
  <jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
<%}%>
<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
  <jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>
<%-- Facebook pixel tracking script included in head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/facebookPixelTracking.jsp"/>
<jsp:include page="/include/seoTitle.jsp"> 
  <jsp:param name="collegeId" value="0"/>
  <jsp:param name="pageName" value="CLEARING SUBJECT LANDING" /> 
  <jsp:param name="paramFlag" value="CLEARING_SUBJECT_LANDING" />      
  <jsp:param name="noindexfollow" value="noindex,nofollow"/>
</jsp:include>