<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">

<c:set var="pagename3">
  <tiles:getAsString name="pagename3" ignore="true"/>
</c:set>

<jsp:include page="/jsp/common/includeIconImg.jsp"/>
<c:choose>
  <c:when test="${pagename3 eq 'appLandingPage.jsp'}">
    <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="0"/>
      <jsp:param name="pageName" value="WHATUNI APP LANDING PAGE" /> 
      <jsp:param name="paramFlag" value="WHATUNI APP LANDING PAGE"/>
    </jsp:include>      
  </c:when>
  <c:when test="${pagename3 eq 'appFaqPage.jsp'}">
    <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="0"/>
      <jsp:param name="pageName" value="WHATUNI APP FAQ PAGE" /> 
      <jsp:param name="paramFlag" value="WHATUNI APP FAQ PAGE"/>      
    </jsp:include>
  </c:when>
  <c:when test ="${pagename3 eq 'appContactUsPage.jsp'}">
    <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="0"/>
      <jsp:param name="paramFlag" value="STUDENT_AWARD"/>
      <jsp:param name="pageName" value="ABOUT US CONTACT US"/>
      <jsp:param name="noindexfollow" value="noindex,follow"/>
    </jsp:include>
  </c:when>
</c:choose>

<c:choose>
  <c:when test="${pagename3 eq 'appLandingPage.jsp'}">
    <jsp:include page="/jsp/app/include/includeAppLandingCSS.jsp">
      <jsp:param name="pageName" value="ALP"/>
    </jsp:include>
  </c:when>
  <c:when test="${pagename3 eq 'appFaqPage.jsp'}">
    <jsp:include page="/jsp/app/include/includeAppLandingCSS.jsp">
      <jsp:param name="pageName" value="FAQ"/>
    </jsp:include>
  </c:when>
  <c:when test ="${pagename3 eq 'appContactUsPage.jsp'}">
    <jsp:include page="/jsp/app/include/includeAppLandingCSS.jsp">
      <jsp:param name="pageName" value="CONTACTUS"/>
    </jsp:include>
  </c:when>
</c:choose>
