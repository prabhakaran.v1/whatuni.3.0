<%@page import="WUI.utilities.CommonUtil" autoFlush="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%
String ionRangeSliderCss = CommonUtil.getResourceMessage("wuni.ion.range.slider.css", null);
String gradeFilterJsName = CommonUtil.getResourceMessage("wuni.grader.filter.js", null);

%>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<jsp:include page="/jsp/whatunigo/include/includeWhatuniGoCSS.jsp"/>
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=ionRangeSliderCss%>" media="screen" />
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/whatunigo/<%=gradeFilterJsName%>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/ion.rangeSlider.min.js"> </script>
<jsp:include page="/include/seoTitle.jsp">
   <jsp:param name="collegeId" value="0"/>
   <jsp:param name="pageName" value="CLEARING QUALIFICATION LANDING" /> 
   <jsp:param name="paramFlag" value="CLEARING_QUALIFICATION_LANDING" />      
   <jsp:param name="noindexfollow" value="noindex,follow"/>
</jsp:include>
