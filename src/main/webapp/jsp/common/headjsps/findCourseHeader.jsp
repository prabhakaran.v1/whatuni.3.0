
<%@page import=" WUI.utilities.CommonUtil, WUI.utilities.SessionData, WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
  <%
  String titleStudyLevelId = request.getAttribute("studyLevelId") != null ? String.valueOf(request.getAttribute("studyLevelId")) :"";  
  %>
  <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="0"/>
      <jsp:param name="studyLevelId" value="<%=titleStudyLevelId%>"/>
      <jsp:param name="pageName" value="COURSE BROWSE CATEGORY"/>
      <jsp:param name="paramFlag" value="J_SUBJECT_LIST"/>  
  </jsp:include>
  <%-- Added canonical url by Sujitha V on 02_JUNE_2020 --%>
  <c:if test="${not empty coureseSearchCanonicalUrl}">
    <link rel="canonical" href="${coureseSearchCanonicalUrl}" />
  </c:if>
<%@include  file="/jsp/common/includeMainCSS.jsp" %>
<%String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
String courseSearchJs = CommonUtil.getResourceMessage("wuni.course.search.js", null);

%>
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=courseSearchJs%>"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/ultimatesearch_wu552.js"> </script>
<script type="text/javascript" language="javascript">
var dev = jQuery.noConflict();
dev(document).ready(function(){
adjustStyle();
});
adjustStyle();
function jqueryWidth() {
  return dev(this).width();
} 
function adjustStyle() {
    var width = document.documentElement.clientWidth;
    var path = ""; 
    dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
    dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
    if (width <= 480) {
	    if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }        
        <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}%>   
    } else if ((width > 480) && (width <= 992)) {         
	   if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }       
       <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}%>  
    } else {
        dev(".hm_srchbx").hide();
        if (dev("#viewport").length > 0) {
            dev("#viewport").remove();
        }
        document.getElementById('size-stylesheet').href = "";         
    }
      dev('#iwtbEntry,#iwtbResults,#wcidEntry,#wcidResults').css("display", "none");
      var iwtbPod = document.getElementById("iwtbPodDisplay") ? document.getElementById("iwtbPodDisplay").value : "";
      var wcidPod = document.getElementById("wcidPodDisplay") ? document.getElementById("wcidPodDisplay").value : "";
      if(iwtbPod != ""){
          dev('#'+iwtbPod).css("display", "block");
      }
      if(wcidPod != ""){
      dev('#'+wcidPod).css("display", "block");
      } 
}
dev(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
    setTimeout(function() {
        dev(window).trigger('resize');
    }, 500);
}
dev(window).resize(function() {
  var screenWidth = jqueryWidth();
  dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
  dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
  if (screenWidth > 992) {
    dev('#scrollfreezediv').addClass('pros_fixed');
    dev('#scrollfreezediv').css({ position: 'fixed', top: 0 });
  }else{
    dev('#scrollfreezediv').removeClass('pros_fixed');
    dev('#scrollfreezediv').css('position','static'); 
  }
    dev('#iwtbEntry,#iwtbResults,#wcidEntry,#wcidResults').css("display", "none");
    var iwtbPod = document.getElementById("iwtbPodDisplay") ? document.getElementById("iwtbPodDisplay").value : "";
    var wcidPod = document.getElementById("wcidPodDisplay") ? document.getElementById("wcidPodDisplay").value : "";    
    dev('#'+iwtbPod).css("display", "block");
    dev('#'+wcidPod).css("display", "block");

  adjustStyle();  
});
function jqueryWidth() {
    return dev(this).width();
}
</script>
