<%@page import="mobile.util.MobileUtils"%>
<%@page import="WUI.utilities.GlobalConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
<%
  String richSecLen = "", overviewImgPath = "", myhcProfileId = "", noindexfollow ="", seoPageName ="", gaCollegeName ="", dispCollegeName = "", highTab = ""; 
  String collegeId =  (String)request.getAttribute("collegeId");
  seoPageName      =  (String)request.getAttribute("seoPageName");
  seoPageName      =  "COLLEGE LANDING PAGE";
  dispCollegeName  =  (String)request.getAttribute("collegeNameDisplay");
  CommonFunction common = new CommonFunction();
  gaCollegeName    = common.replaceSpecialCharacter(dispCollegeName);
  String basketCollegeName = request.getAttribute("interactionCollegeName") != null && request.getAttribute("interactionCollegeName").toString().trim().length() > 0 ? common.replaceSpecialCharacter((String)request.getAttribute("interactionCollegeName")) : "0";
  String heroImageJsName = CommonUtil.getResourceMessage("wuni.heroImgJsName.js", null);
  String domainSpecificPath = common.getSchemeName(request)+ CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
  String main_480_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.480.css", null);
  String main_992_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.992.css", null);
  String envronmentName = common.getWUSysVarValue("WU_ENV_NAME");
  String canonicalUrl = request.getAttribute("canonicalUrl")!=null ? request.getAttribute("canonicalUrl").toString().trim() : "";
  if(request.getAttribute("richProfileType")!=null && "RICH_INST_PROFILE".equalsIgnoreCase((String)request.getAttribute("richProfileType"))){
    noindexfollow = "index,follow";
  }else{
    noindexfollow = "noindex,follow";
  }
  String clearingyear   = GlobalConstants.CLEARING_YEAR;
  String clearingonoff  = common.getWUSysVarValue("CLEARING_ON_OFF");   
  String requestURL = (String)request.getAttribute("REQUEST_URI");
         requestURL = requestURL.indexOf("/degrees") > -1 ? requestURL.replace("/degrees", "") : requestURL;
         requestURL = requestURL.indexOf(".html") > -1 ? requestURL.replace(".html", "/") : requestURL;
  String profileType = (String)request.getAttribute("richProfileType");
  String ugTabStyleClass = "op_loct";  
  if(!GenericValidator.isBlankOrNull(clearingonoff) && "ON".equals(clearingonoff)){  
    ugTabStyleClass = "op_loct";
  }  
  if(request.getAttribute("highlightTab")!=null && !"".equals(request.getAttribute("highlightTab"))){
    highTab = (String)request.getAttribute("highlightTab");    
  } 
  String spMyhcProfileId = (String)request.getAttribute("spMyhcProfileId");//Added by Indumathi NOV-03-15 for sp rich profile
	String commonVideoJSName = CommonUtil.getResourceMessage("wuni.commonVideoJsName.js", null);
  //Added by Indumathi.S For unique content changes May_31_2016
  String tabFlag = request.getAttribute("tabFlag") != null ? (String)request.getAttribute("tabFlag") : "false";
  String courseExistsUGFlag = request.getAttribute("courseExistsUGFlag") != null ? (String)request.getAttribute("courseExistsUGFlag") : "N";
  String courseExistsPGFlag = request.getAttribute("courseExistsPGFlag") != null ? (String)request.getAttribute("courseExistsPGFlag") : "N";
  String profileId = request.getAttribute("profileId") != null ? (String)request.getAttribute("profileId") : "";
  String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null);
  //End
  
  //Added by Indumathi.S For pageview logging July-26-2016
   int stind1 = request.getRequestURI().lastIndexOf("/");
   int len1 = request.getRequestURI().length();
   String pageName = pageContext.getAttribute("pagename3") != null ?  (String)pageContext.getAttribute("pagename3") : request.getRequestURI().substring(stind1+1,len1);
   String reviewlightboxJs = CommonUtil.getResourceMessage("wuni.reviewlightbox.js", null);
   String richProfileJS = CommonUtil.getResourceMessage("wuni.rich.profile.js", null);//changed js reference dynamic by Sangeeth.S for FEB_12_19 rel
   pageName = pageName !=null ? pageName.toLowerCase() : pageName;
  if((!GenericValidator.isBlankOrNull(pageName)) && (("richprofilelanding.jsp").equalsIgnoreCase(pageName))){           
    if(request.getAttribute("richProfileType")!=null && "RICH_INST_PROFILE".equalsIgnoreCase((String)request.getAttribute("richProfileType"))){
      pageName = "richuniview.jsp";      
    }
  }
  String gaPageCollegeName = (String)request.getAttribute("hitbox_college_name");
         gaPageCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(gaPageCollegeName);
         gaPageCollegeName = !GenericValidator.isBlankOrNull(gaPageCollegeName) ? !gaPageCollegeName.equals("null") ? gaPageCollegeName.toLowerCase().replaceAll(" ","-").trim() : "" : "";  
  boolean mobileFlag = new MobileUtils().userAgentCheck(request);
  String userJourney = (String)request.getAttribute("USER_JOURNEY_FLAG"); //Added for cmmt
  String mainSecStyle     = "rich_profile";  
  String seoParamFlag = "UNI_LANDING";
  String clrItrBtnCls = "";
  String mobileMenuText = "More Info";  
  if("CLEARING".equalsIgnoreCase(userJourney)){
      mainSecStyle = "rich_profile clr_prof_cnt clr_nw_bt clrrich_prof";
      seoPageName = "CLEARING PROFILE";
      seoParamFlag = "CLEARING PROFILE";
      noindexfollow = "index,follow";
      canonicalUrl = canonicalUrl.replace("?clearing","");
      clrItrBtnCls = "clr_btn";
      mobileMenuText = "Apply";
  }
  
%> 
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
  <%@include  file="/jsp/common/includeMainCSS.jsp" %> 
  
  <jsp:include page="/jsp/thirdpartytools/include/includeGoogleTagJs.jsp" />
  <jsp:include page="/jsp/thirdpartytools/include/createTargetingAttr.jsp" /> 
  <jsp:include page="/jsp/common/includeSeoMetaDetails.jsp">
    <jsp:param value="<%= noindexfollow%>" name="noindexfollow"/>
  </jsp:include> 
  <%if(!GenericValidator.isBlankOrNull(canonicalUrl)){%>
    <link rel="canonical" href="<%=canonicalUrl%>"/>
  <%}%>
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>  
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=richProfileJS%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/review/<%=reviewlightboxJs%>"></script>
 <script type="text/javascript" language="javascript">
    var dev = jQuery.noConflict();
    dev(document).ready(function(){
      adjustStyle();
    });
    adjustStyle();
    function jqueryWidth() {
      return dev(this).width();
    } 
    function adjustStyle() {
      var width = document.documentElement.clientWidth;
      var path = ""; 
      if (width <= 480) {
        if(document.getElementById("lightBoxResp")){
          document.getElementById("lightBoxResp").value = "mobile";
        }
        if (dev("#viewport").length == 0) {
          dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }
        <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}%>
      } else if ((width > 480) && (width <= 992)) { 
        if(document.getElementById("lightBoxResp")){
          document.getElementById("lightBoxResp").value = "mobile";
        }
        if (dev("#viewport").length == 0) {
          dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        } 
        <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}%>
        dev('#articleSearchForm').find('.tx_bx').css('width', '100%').css('width', '-=40px');
      } else {
        if(document.getElementById("lightBoxResp")){
          document.getElementById("lightBoxResp").value = "";
        }
        if (dev("#viewport").length > 0) {
          dev("#viewport").remove();
        } 
        document.getElementById('size-stylesheet').href = ""; 
      }
    }
    dev(window).on('orientationchange', orientationChangeHandler);
    function orientationChangeHandler(e) {
      setTimeout(function() {
        dev(window).trigger('resize');
        }, 500);
        //vary the size of the rico video section based on the screem width by Prabha on 31_May_2016
        if($$D('richVideoPod') && $$D('richVideoPod').style.display == "block" && document.documentElement.clientWidth <= 1024){
          dev("#flashbanner1,#flashbanner1 video").css({'width':document.documentElement.clientWidth+'px'});
        }
        //End of the video width resize code
      }
      //Trigger the facebook js in orientation
      if(document.getElementById("facebookTab")){
        if(document.getElementById("facebookTab").className == "act"){
          setTimeout(function(){dev("#facebookTab").trigger( "click" );},1000); 
        }
      }
      //End of facebook trigger code
      dev(window).resize(function() {
      mobileSpecificImg("h_image8");
      mobileSpecificImage("h_image2"); //Added lazy loading code for rich video image by Prabha on 13_06_17
      showScrollBar();//Added by Indumathi.S For including scroll bar in profile pod May_31_2016
      var screenWidth = jqueryWidth();
       
      if ((screenWidth > 480) && (screenWidth <= 992)) {      //Tablet || Mobile view
        dev('#articleSearchForm').find('.tx_bx').css('width', '100%').css('width', '-=40px');
        dev("#stickyTop").removeAttr("style").removeClass("sticky_top_nav");
        if (dev("#ol").is(':visible')){
          var posleft = posLeft()+((pageWidth()-590)/2)-12;
          posleft = (posleft < 0) ? 0 : posleft;
          dev('#mbox').addClass("mob_vid_res").css({ left :posleft+'px'});
          dev('#flashbanner').addClass("mob_vid_flb");
        }
        document.getElementById("mblOrient").value = "true";
      }else{
        if(screenWidth <= 480){                             //Mobile view
          dev('#mbox').addClass("mob_vid_res").css({ left :'0px'});
          dev('#flashbanner').addClass("mob_vid_flb");
          document.getElementById("mblOrient").value = "true";
        }else{
          var windowTop = dev(window).scrollTop();         //Desktop view 
          if(311 < windowTop){
            dev('#stickyTop').addClass('sticky_top_nav');        
            dev('#stickyTop').css({ position: 'fixed', top: 0 });            
          }else{
            dev('#stickyTop').removeClass('sticky_top_nav');
            dev('#stickyTop').css('position','static');         
          } 
          if (dev("#ol").is(':visible')) {
            var posleft = posLeft()+((pageWidth()-590)/2)-12;
            dev('#mbox').removeClass("mob_vid_res").css({ left :posleft+'px'});
            dev('#flashbanner').removeClass("mob_vid_flb");
          }
          document.getElementById("mblOrient").value = "false";
        }
        dev('#articleSearchForm').find('.tx_bx').removeAttr("style");
      }
      adjustStyle(); 
      if (!dev("#addTofnchTickerList").is(":visible")){
       if(dev("#cookie-lightbox") && !dev("#cookie-lightbox").is(":visible")){
        stickyButton(); 
       }
      }
      var width = document.documentElement.clientWidth;
      if(width > 992){dev(".btn_intr .more_info").removeAttr("style");}
      });
    function jqueryWidth() {
      return dev(this).width();
    }
  </script>
  <%--<jsp:include  page="/jsp/home/include/includeHeaderResponsive.jsp"/>--%>
  <%--Added schema tag for page and open days section on 16_May_2017, By Thiyagu G--%>
  <%if(!GenericValidator.isBlankOrNull(profileType) && !"RICH_SUB_PROFILE".equals(profileType)){%>
  <jsp:include page="/jsp/common/includeSchemaTag.jsp">
    <jsp:param name="pageName" value="UNI_RICH_PROFILE_SCHEMA"/>
  </jsp:include>
  <c:if test="${not empty requestScope.listOfOpendayInfo}">
    <jsp:include page="/jsp/common/includeSchemaTag.jsp">
      <jsp:param name="pageName" value="RICH_PROFILE_OPENDAY_SCHEMA"/>
    </jsp:include>
  </c:if>
  <%}%>