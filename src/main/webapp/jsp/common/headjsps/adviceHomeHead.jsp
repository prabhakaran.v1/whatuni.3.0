<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<jsp:include page="/jsp/common/includeSeoMetaDetails.jsp"/>
<%@include  file="/jsp/common/includeMainCSS.jsp" %>
<%
  String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
  String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
  String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
  String adviceJsName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.adviceJsName.js");
  String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
  String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
  
%>
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>

<script type="text/javascript" language="javascript">
var dev = jQuery.noConflict();
dev(document).ready(function(){
adjustStyle();
});
adjustStyle();
function jqueryWidth() {
  return dev(this).width();
} 
function adjustStyle() {
  var width = document.documentElement.clientWidth;
  var path = ""; 
  dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
  dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
  if (width <= 480) {
    if (dev("#viewport").length == 0) {
      dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
    }        
    <%if(("LIVE").equals(envronmentName)){%>
      document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
    <%}else if("TEST".equals(envronmentName)){%>
      document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
    <%}else if("DEV".equals(envronmentName)){%>
      document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
    <%}%>         
  } else if ((width > 480) && (width <= 992)) {         
    if (dev("#viewport").length == 0) {
      dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
    }       
    <%if(("LIVE").equals(envronmentName)){%>
      document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
    <%}else if("TEST".equals(envronmentName)){%>
       document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
    <%}else if("DEV".equals(envronmentName)){%>
       document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
    <%}%>
    dev('#articleSearchForm').find('.tx_bx').css('width', '100%').css('width', '-=18px');
  } else {
    if (dev("#viewport").length > 0) {
      dev("#viewport").remove();
    }       
    document.getElementById('size-stylesheet').href = "";        
    dev('#articleSearchForm').find('.tx_bx').removeAttr("style");
    dev(".hm_srchbx").hide();
  }
}
dev(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
  setTimeout(function() {
  dev(window).trigger('resize');
}, 500);
}
dev(window).resize(function() {
  var screenWidth = jqueryWidth();
  dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
    dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
  if ((screenWidth > 480) && (screenWidth <= 992)) {
    dev('#articleSearchForm').find('.tx_bx').css('width', '100%').css('width', '-=18px');
  }else{
    dev('#articleSearchForm').find('.tx_bx').removeAttr("style");
  }
  adjustStyle();  
});
function jqueryWidth() {
  return dev(this).width();
}
</script>
<%--<jsp:include  page="/jsp/home/include/includeHeaderResponsive.jsp"/>--%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=adviceJsName%>"></script>
