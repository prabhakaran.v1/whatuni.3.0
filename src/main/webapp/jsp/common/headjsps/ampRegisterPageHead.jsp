<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no"/>
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
    <jsp:include page="/jsp/common/includeIconImg.jsp"/>
    <jsp:include page="/include/seoTitle.jsp">
      <jsp:param name="collegeId" value="0"/>
      <jsp:param name="pageName" value="AMP_REGISTRATION_PAGE" /> 
      <jsp:param name="paramFlag" value="AMP_REGISTRATION_PAGE"/>      
    </jsp:include>
    <%
      String commonCssName = CommonUtil.getResourceMessage("wuni.common.css", null);
      String cdScreensCssName = CommonUtil.getResourceMessage("wuni.whatuni.cd.screens.css", null);
      String mainHeaderCSS = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);//Added this css for Header changes by Hema.S on 23_OCT_2018_REL
      String ampPageUrl = (String)request.getAttribute("ampPageUrl");
      String facebookLoginJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.facebook.login.js");
      String commonUserProfileJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.user.profile.js");    
     
    %>
    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=mainHeaderCSS%>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=commonCssName%>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=cdScreensCssName%>" media="screen" />
    <!--[if IE 7]> <link rel="stylesheet" type="text/css" href="https://css.whatuni.com/wu-cont/cssstyles/whatuni-screens_ie7hacks-wu552.css" media="screen" /> <![endif]-->
    <jsp:include page="/jsp/common/abTesting.jsp"/>
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
    <jsp:include page="/advice/amp/include/includeAmpRegisterCSS.jsp"/>