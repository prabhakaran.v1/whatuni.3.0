<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants, WUI.utilities.SessionData,WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, mobile.util.MobileUtils, java.util.ResourceBundle" autoFlush="true" %>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction,org.apache.commons.validator.GenericValidator"%>
<% 
ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css"); 
String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");  
  String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "NULL";    
  CommonUtil comUtil = new CommonUtil();  
  CommonFunction commonFun = new CommonFunction(); 
  String httpStr = commonFun.getSchemeName(request); 
  String canonicalLink = httpStr + "www.whatuni.com/degrees" + firstPageSeoUrl;	    
 String commonCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.css");
 String domainSpecPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
 String mainHeaderCSS = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);  
 String main_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.cd.screens.css");
 String retinaJs = CommonUtil.getResourceMessage("wuni.retina.js", null);
 String cpcLandingPageJs = CommonUtil.getResourceMessage("wuni.cpc.landing.js", null);
%>
<meta http-equiv="content-language" content=" en-gb "/>
<jsp:include page="/jsp/common/includeIconImg.jsp"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">    
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=mainHeaderCSS%>" media="screen">
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=commonCssName%>" media="screen">    
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=main_ver%>" media="screen">        
  
 <jsp:include page="/jsp/common/includeResponsive.jsp"></jsp:include> 
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/cpc/<%=cpcLandingPageJs%>"> </script>
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/cpc/wu_cpcHeader_040220.js"></script>
 <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=retinaJs%>"></script>    
<jsp:include page="/jsp/common/abTesting.jsp"/>
<jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
	<jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>
<%-- Facebook pixel tracking script included in head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/facebookPixelTracking.jsp"/>

  <jsp:include page="/include/seoTitle.jsp">
    <jsp:param name="collegeId" value="0"/>            
    <jsp:param name="pageName" value="SINGLE CLIENT CPC LANDING PAGE"/>
    <jsp:param name="paramFlag" value="SINGLE_CLIENT_CPC_LANDING_PAGE"/> 
    <jsp:param name="noindexfollow" value="noindex,follow"/>            
  </jsp:include>   
 <%-- <%if(!GenericValidator.isBlankOrNull(canonicalLink)){%>
  <link rel="canonical" href="<%=canonicalLink%>"/> 
  <%}%>  --%> 
  <jsp:include page="/jsp/common/includeSchemaTag.jsp">
    <jsp:param name="pageName" value="BREADCRUMB_SCHEMA"/>
  </jsp:include>
  <jsp:include page="/jsp/common/includeSchemaTag.jsp">
    <jsp:param name="pageName" value="WEB_PAGE_SCHEMA"/>
  </jsp:include>
