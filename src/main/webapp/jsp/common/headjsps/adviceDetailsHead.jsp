<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, java.util.*, WUI.utilities.GlobalConstants" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no"/>
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<%    
  CommonFunction commonFun = new CommonFunction();
  String metaTitle = (request.getAttribute("metaTitle")!=null) ? request.getAttribute("metaTitle").toString() : "";
  String metaKeywords = (request.getAttribute("metaKeywords")!=null) ? request.getAttribute("metaKeywords").toString() : "";
  String metaDescription = (request.getAttribute("metaDescription")!=null) ? request.getAttribute("metaDescription").toString() : ""; 
  String ogImage = (request.getAttribute("ogImage")!=null) ? request.getAttribute("ogImage").toString() : "";     
  String requestURL = (String)request.getAttribute("currentPageUrl");
  String ptURL = requestURL!=null? requestURL.replace("/degrees/" ,"/") :requestURL;
  String primaryCategory = (request.getAttribute("primaryCategory")!=null) ? request.getAttribute("primaryCategory").toString() : "";
  String articleId = (request.getAttribute("articleId")!=null) ? request.getAttribute("articleId").toString() : "";
  String parentCategory = (request.getAttribute("parentCategory")!=null) ? request.getAttribute("parentCategory").toString() : "";
  String secondaryCategory = (request.getAttribute("secondaryCategory")!=null) ? request.getAttribute("secondaryCategory").toString() : "";    
  String artPCategory = parentCategory.replaceAll("-"," ").toUpperCase();
  String artSCategory = secondaryCategory.replaceAll("-"," ").toUpperCase();
  ResourceBundle rb = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources"); 
  String domainSpecificPath = commonFun.getSchemeName(request)+rb.getString("wuni.whatuni.device.specific.css.path");
  String main_480_ver = rb.getString("wuni.whatuni.main.480.css");
  String main_992_ver = rb.getString("wuni.whatuni.main.992.css");
  String adviceJsName = rb.getString("wuni.adviceJsName.js");  
  String lazyLoadJsName = rb.getString("wuni.lazyLoadJsName.js");
  String lazyLoadJs = rb.getString("wuni.lazyLoadJs.js");  
  String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
  boolean isUCASApplication = false;
  String className = "artCont";
  String enableUcasGuideRightPod = commonFun.getWUSysVarValue("UCAS_GUIDE_RIGHT_POD");   // Yogeswari :: 30.06.2015 :: added for UCAS CALCULATOR task.
  String ampHtmlUrl = ptURL + "?amp=true";
  String facebookLoginJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.facebook.login.js");    
  if(!"".equals(metaTitle) && !"".equals(metaDescription)){%>
    <title><%=metaTitle%></title> 
    <meta name="description" content="<%=metaDescription%>"/> 
    <meta name="keywords" content="<%=metaDescription%>"/> 
  <%}%>
  <!-- Commented this second DB call for SEO on 2021_JAN_19 rel by Sujitha V  -->
  <%-- <%}else{%>
      <jsp:include page="/include/seoTitle.jsp">
        <jsp:param name="collegeId" value="0"/>
        <jsp:param name="pageName" value="WHATUNI ARTICLE DETAIL PAGE"/> 
        <jsp:param name="paramFlag" value="WU_ARTICLE_PAGES"/>  
        <jsp:param name="articleId" value="<%=articleId%>"/>
        <jsp:param name="artParentName" value="<%=artPCategory%>"/>
        <jsp:param name="artSubName" value="<%=artSCategory%>"/>
      </jsp:include>
  <%}%> --%>
<link rel="canonical" href="<%=ptURL%>" />
<meta name="ROBOTS" content="index,follow"/>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
      <meta property="fb:app_id" content="374120612681083" />
      <meta property="fb:admins" content="27327779286"/>            
      <meta property="og:title" content="<%=metaTitle%>" /> 
      <meta property="og:type" content="article" /> 
      <meta property="og:description" content="<%=metaDescription%>" /> 
      <meta property="og:image" content="<%=ogImage%>" /> 
      <meta property="og:url" content="<%=ptURL%>" /> 
      <meta name="twitter:card" value="summary" content="summary" /> 
      <meta name="twitter:creator" value="@whatuni" content="@whatuni" /> 
      <meta name="twitter:url" value="<%=ptURL%>" content="<%=ptURL%>" /> 
     <meta name="twitter:title" value="<%=metaTitle%>" content="<%=metaTitle%>" /> 
     <meta name="twitter:description" value="<%=metaDescription%>" content="<%=metaDescription%>" /> 
<meta name="twitter:image" content="<%=CommonUtil.getImgPath("/wu-cont/images/logo_print.png",0)%>" />
<link rel="amphtml" href="<%=ampHtmlUrl%>" />
<%@include  file="/jsp/common/includeMainCSS.jsp" %>
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>

<script type="text/javascript" language="javascript">
var dev = jQuery.noConflict();
adjustStyle();
function jqueryWidth() {
  return dev(this).width();
} 
function adjustStyle() {
    var width = document.documentElement.clientWidth;
    var path = ""; 
    dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
    dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
    if (width <= 480) {
	    if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }
        <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}%>
    } else if ((width > 480) && (width <= 992)) {         
	   if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }       
       <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}%>
    } else {
      dev(".hm_srchbx").hide();
      if (dev("#viewport").length > 0) {
          dev("#viewport").remove();
      }       
      document.getElementById('size-stylesheet').href = "";        
    }
}
dev(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
    setTimeout(function() {
        dev(window).trigger('resize');
    }, 500);
}
dev(window).resize(function() {  
  adjustStyle();  
});
</script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJsName%>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=adviceJsName%>"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/downloadpdf_wu564.js"></script>
<%--Added schema tag for the article detail page in Mar_03_20 rel by Sangeeth.S--%>
  <c:if test="${not empty adviceDetailInfoList}">
    <jsp:include page="/jsp/common/includeSchemaTag.jsp">
      <jsp:param name="pageName" value="ARTICLE_DETAIL_PAGE_SCHEMA"/>
      <jsp:param name="adviceDetailPageUrl" value="<%=ptURL%>"/>
      <jsp:param name="logoUrl" value="<%=CommonUtil.getImgPath(GlobalConstants.WU_LOGO_URL,0)%>"/>
    </jsp:include>
  </c:if>
<%-- Commented by Prabha for unused js loading on Article detail page on 08_May_2018
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>--%>
<%--<jsp:include  page="/jsp/home/include/includeHeaderResponsive.jsp"/>--%>