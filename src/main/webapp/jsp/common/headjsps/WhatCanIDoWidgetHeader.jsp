
  <%@page import="WUI.utilities.CommonUtil"%>
<%@page import="java.util.ResourceBundle"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
  <link href='https://fonts.googleapis.com/css?family=Lato:400,700,700italic' rel='stylesheet' type='text/css'>
  <jsp:include page="/jsp/common/includeIconImg.jsp"/><%--Icon images added by Prabha on 31_May_2016--%>
  <jsp:include page="/include/seoTitle.jsp">
    <jsp:param name="collegeId" value="0"/>
    <jsp:param name="pageName" value="WHAT CAN I DO WIDGET" /> 
    <jsp:param name="paramFlag" value="WHAT_CAN_I_DO_WIDGET"/>
    <jsp:param name="noindexfollow" value="index,follow" />
  </jsp:include>
  <jsp:include page="/jsp/whatcanido/include/includeWCIDWigetCSS.jsp"/>
    
<%ResourceBundle rb = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources");  
  String ajaxDynamicListJSName = rb.getString("wuni.ajaxDynamicList.js");
  String widgetDoughnutJSName = rb.getString("wuni.widget.doughnut.chart.js");
  String widgetDrawDoughnutJSName = rb.getString("wuni.widget.draw.doughnut.js");
  String lazyLoadJs = rb.getString("wuni.lazyLoadJs.js");
  
%>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/<%=ajaxDynamicListJSName%>"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/ajax_wu564.js"></script>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/whatcanido/whatcanido_wu552.js"></script>
