<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, java.util.ArrayList, WUI.utilities.SessionData, java.util.*, WUI.utilities.CommonFunction" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
  
<%
  CommonFunction commonFun = new CommonFunction();
  String totalRecordCount = (request.getAttribute("totalRecordCount")!=null) ? request.getAttribute("totalRecordCount").toString() : "0";  
  int noOfPage = (Integer.parseInt(totalRecordCount)/20);
  if((Integer.parseInt(totalRecordCount)%20) > 0) {
    noOfPage++; 
  }       
  int countDisp = 20;
  String pageNo = (request.getAttribute("page")!=null) ? request.getAttribute("page").toString() : "1";   
  String articleText = "";     
  if(Integer.parseInt(totalRecordCount)==1){articleText="article";}else{articleText="articles";}     
  String pageName = (request.getAttribute("pageName")!=null) ? request.getAttribute("pageName").toString() : "1";
  String keyword = (request.getAttribute("keyword")!=null) ? request.getAttribute("keyword").toString() : "";
  String nextUrl = "";     
  String previousUrl = "";  
  int numofpagesplus = (Integer.parseInt(pageNo)+1);
  int numofpagesminus = (Integer.parseInt(pageNo)-1);
  String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
  String domainSpecificPath = httpStr + java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
  String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
  String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
  String envronmentName = commonFun.getWUSysVarValue("WU_ENV_NAME");
  String adviceJsName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.adviceJsName.js");
  String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");

%>
  <%if(noOfPage>1){
     if("1".equals(pageNo)){       
        nextUrl = httpStr + "www.whatuni.com/article-search/?keyword="+keyword+"&page="+numofpagesplus;
     %>
        <link rel="next" href="<%=nextUrl%>" />
     <%}else if(noOfPage == Integer.parseInt(pageNo)){
        previousUrl = httpStr + "www.whatuni.com/article-search/?keyword="+keyword+"&page="+numofpagesminus;
     %>
        <link rel="prev" href="<%=previousUrl%>" />
     <%}else{
        nextUrl = httpStr + "www.whatuni.com/article-search/?keyword="+keyword+"&page="+numofpagesplus;
        previousUrl = httpStr + "www.whatuni.com/article-search/?keyword="+keyword+"&page="+numofpagesminus;
     %>
        <link rel="prev" href="<%=previousUrl%>" />
        <link rel="next" href="<%=nextUrl%>" />
     <%}}
     %>
     <jsp:include page="/jsp/common/includeSeoMetaDetails.jsp"/>
     
<%@include  file="/jsp/common/includeMainCSS.jsp" %>
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<script type="text/javascript" language="javascript">
var dev = jQuery.noConflict();
adjustStyle();
function jqueryWidth() {
  return dev(this).width();
} 
function adjustStyle() {
    var width = document.documentElement.clientWidth;
    var path = ""; 
    dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
    dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
    if (width <= 480) {
	    if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }
        <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
        <%}%>        
    } else if ((width > 480) && (width <= 992)) {         
	   if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }        
       <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
        <%}%>
       dev('#articleSearchForm').find('.tx_bx').css('width', '100%').css('width', '-=18px');
    } else {
        if (dev("#viewport").length > 0) {
            dev("#viewport").remove();
        }       
        document.getElementById('size-stylesheet').href = "";
        dev(".hm_srchbx").hide();
    }
}
dev(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
    setTimeout(function() {
        $(window).trigger('resize');
    }, 500);
}
dev(window).resize(function() {
  var screenWidth = jqueryWidth();
  dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
  dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
  if ((screenWidth > 480) && (screenWidth <= 992)) {
    dev('#articleSearchForm').find('.tx_bx').css('width', '100%').css('width', '-=18px');
  }else{
    dev('#articleSearchForm').find('.tx_bx').removeAttr("style");
  }
  adjustStyle();  
});
function jqueryWidth() {
    return dev(this).width();
}
</script>
<%--<jsp:include  page="/jsp/home/include/includeHeaderResponsive.jsp"/>--%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=adviceJsName%>"></script>