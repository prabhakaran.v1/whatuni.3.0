<%@page import="org.apache.commons.validator.GenericValidator,WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants"%>
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">

<jsp:include page="/jsp/search/searchredesign/includeSearchCSS.jsp"/>
<%@include file="/jsp/common/includeResponsive.jsp"%>
<jsp:include page="/include/seoTitle.jsp">  
  <jsp:param name="paramFlag" value="WU_HCSTUFF"/>
  <jsp:param name="pageName" value="WUNI FAQ PAGE"/>
  <jsp:param name="collegeId" value="0"/>
  <jsp:param name="noindexfollow" value=""/>            
</jsp:include>
<script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/faq.js"></script>
<link rel="canonical" href="<%=GlobalConstants.WHATUNI_SCHEME_NAME%><%=request.getServerName()%>/faqs.html"/> 