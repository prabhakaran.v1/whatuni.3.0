<%@page import="WUI.utilities.SessionData, WUI.utilities.GlobalConstants,WUI.utilities.CommonFunction, WUI.utilities.CommonUtil" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%  
  String httpStr = new CommonFunction().getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
  String currentPageUrl = httpStr + "www.whatuni.com/student-awards-winners/2014.html";
  String resultPageFlag = request.getAttribute("resultPageFlag") != null ? (String)request.getAttribute("resultPageFlag") : "";
  String indexFollow = "inValidYear".equalsIgnoreCase(resultPageFlag) ? "noindex, follow" : "index, follow";//Added by Indumathi.S 29_Mar_2016 WUSCA changes
  
%>  
      <title>Best UK University's 2014 | UK University Rankings 2014</title> 
      <meta name="description" content="Looking to find the best University? Check out our 2014 uni rankings by students. These Rankings are for students by students"/> 
      <meta name="ROBOTS" content="<%=indexFollow%>"/><!--To get data from back end-->
      <link rel="canonical" href="<%=currentPageUrl%>" />
			<%String commonVideoJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.commonVideoJsName.js");%>
			<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
			<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/videoplayer/<%=commonVideoJSName%>"></script>    
       <%@include  file="/jsp/common/includeCSS.jsp" %>