<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%
  String provisionalOfferJsName = CommonUtil.getResourceMessage("wuni.provisional.offer.js", null);
  String whatuniGOJsName = CommonUtil.getResourceMessage("wuni.whatunigo.offer.js", null);
  String browserip = GlobalConstants.WHATUNI_SCHEME_NAME + new CommonUtil().getProperties().getString("review.autocomplete.ip");
  String COLLEGE_DISPLAY_NAME = (String)request.getAttribute("COLLEGE_DISPLAY_NAME");
  String COURSE_NAME = (String)request.getAttribute("COURSE_NAME");
  String collegeName = (String)request.getAttribute("hitbox_college_name");
  String refererURL = (request.getAttribute("REFERER_URL") != null) ? (String)request.getAttribute("REFERER_URL") : "";
  String actionName = (String)request.getAttribute("actionName");
  String collegeId = (String)request.getAttribute("COLLEGE_ID");
  
%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">  
    <jsp:include page="/jsp/whatunigo/include/includeWhatuniGoCSS.jsp">
      <jsp:param name="fromPage" value="accept-offer"/>
    </jsp:include>
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/whatunigo/<%=whatuniGOJsName%>"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/whatunigo/<%=provisionalOfferJsName%>"> </script>
     <%if("provisonal-offer-congrats".equalsIgnoreCase(actionName)){%>
       <jsp:include page="/include/seoTitle.jsp">
        <jsp:param name="collegeId" value="0"/>
        <jsp:param name="pageName" value="WUGO PROVISIONAL OFFER JOURNEY" /> 
        <jsp:param name="paramFlag" value="WUGO_PROVISIONAL_OFFER_JOURNEY" />      
        <jsp:param name="noindexfollow" value="noindex,follow"/>
      </jsp:include>
     <%}else{%>
       <jsp:include page="/include/seoTitle.jsp">   
        <jsp:param name="collegeId" value="0"/>
        <jsp:param name="pageName" value="WUGO ACCEPTING OFFER JOURNEY" /> 
        <jsp:param name="paramFlag" value="WUGO_ACCEPTING_OFFER_JOURNEY" />      
        <jsp:param name="noindexfollow" value="noindex,follow"/>
      </jsp:include>
    <%}%>