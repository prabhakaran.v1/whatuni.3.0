    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
    <title>Best UK University's 2015 | UK University Rankings 2015</title> 
    <meta name="description" content="Looking to find the best University? Check out our 2015 uni rankings by students. These Rankings are for students by students"/> 
    <meta name="ROBOTS" content="index,follow"/>
    <%      
      String httpStr = new CommonFunction().getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
      String canonicalUrl = httpStr + "www.whatuni.com/student-awards-winners/2015.html";
    %>
    <link rel="canonical" href="<%=canonicalUrl%>"/>
    <%String commonVideoJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.commonVideoJsName.js");
    
    %>
		<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
			<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/videoplayer/<%=commonVideoJSName%>"></script>    
    <%@include  file="/jsp/common/includeCSS.jsp"%>