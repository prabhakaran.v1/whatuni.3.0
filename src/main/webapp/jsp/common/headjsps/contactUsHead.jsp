<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %> 
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<c:set var="pagename3">
  <tiles:getAsString name="pagename3" ignore="true" />
</c:set>
  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">  
   
  <%@include  file="/jsp/common/includeMainCSS.jsp"%> 
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>  
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>  
  <jsp:include  page="/help/aboutus/includeStaticCntResponsive.jsp"/>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.staticpage.js'/>"></script>
  <c:if test="${pagename3 eq 'contactUsPage.jsp'}">
    <jsp:include page="/include/seoTitle.jsp">   
      <jsp:param name="collegeId" value="0"/>
      <jsp:param name="paramFlag" value="STUDENT_AWARD"/>
      <jsp:param name="pageName" value="ABOUT US CONTACT US"/>
      <jsp:param name="noindexfollow" value="noindex,follow"/>
    </jsp:include> 
  </c:if>  