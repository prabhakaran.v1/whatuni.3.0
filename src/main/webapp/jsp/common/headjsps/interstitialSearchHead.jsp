<%@page import="WUI.utilities.SessionData" autoFlush="true" %>
<%@page import="com.wuni.util.seo.SeoUrls"%>
<%@page import="com.wuni.util.seo.SeoPageNamesAndFlags"%>
<%@page import="org.apache.commons.validator.GenericValidator"%>
<%@page import="WUI.utilities.SessionData"%>
<%@page import="WUI.utilities.CommonFunction"%>
<%@page import="WUI.utilities.GlobalConstants"%>
<%@page import="WUI.utilities.CommonUtil" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%
String canonicalUrl = GlobalConstants.WHATUNI_SCHEME_NAME+GlobalConstants.WHATUNI_DOMAIN;
String ajaxDynamicListJSName = CommonUtil.getResourceMessage("wuni.ajaxDynamicList.js", null);
String interstitialJSName = CommonUtil.getResourceMessage("wuni.search.interstitial.js", null);
String gradeFilterJsName = CommonUtil.getResourceMessage("wuni.sr.grade.filter.js", null);
String qualification  = request.getParameter("qual");
request.setAttribute("qualification", qualification);
String refererUrl = request.getHeader("referer");
String awardsURL = "/student-awards-winners/university-of-the-year/";
String clearingOnOffFlag = (String)request.getAttribute("CLEARING_ON_OFF_FLAG");
if(GenericValidator.isBlankOrNull(qualification)) {
  qualification = "degree";
}
String qualparentClass = "";
if("ON".equals(clearingOnOffFlag)) {
  qualparentClass = "adjrady";
}
String qualificationCode = GenericValidator.isBlankOrNull((String)request.getAttribute("DEFAULT_QUAL_SELECTED")) ? "" : (String)request.getAttribute("DEFAULT_QUAL_SELECTED");
String qualificationTypeSelected = "Others";
request.setAttribute("qualificationTypeSelected", qualificationTypeSelected);
if("CLEARING".equals(qualificationCode)) {
  qualificationTypeSelected = "Clearing/Adjustment";
  request.setAttribute("qualificationTypeSelected", qualificationTypeSelected);
  qualificationCode = "M";
}

%>
      <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
      <meta content="utf-8" http-equiv="encoding">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
      <jsp:include page="/jsp/common/includeIconImg.jsp"/>
      <%--
      <jsp:include page="/jsp/common/includeIconImg.jsp"/>
      <jsp:include page="/include/seoTitle.jsp">
        <jsp:param name="collegeId" value="<%=collegeId%>"/>    
        <jsp:param name="pageName" value="<%=SeoPageNamesAndFlags.UNI_LANDING_PAGE_NAME%>" /> 
        <jsp:param name="paramFlag" value="<%=SeoPageNamesAndFlags.UNI_LANDING_PAGE_FLAG%>"/>  
        <jsp:param name="noindexfollow" value="index,follow"/>
      </jsp:include>
      --%>
      <link rel="canonical" href="<%=canonicalUrl%>"/>
      <link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/advsrch_20210119.css" />
      <link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/wu_advsrch_grdflt_04022020.css">
      <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>
      <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/ajax_wu564.js" defer="defer"> </script>
      <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/<%=ajaxDynamicListJSName%>" defer="defer"> </script>
      <script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/interstitial/<%=interstitialJSName%>"></script>
      <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=gradeFilterJsName%>"> </script>
      <!--[if IE 7]> 
      <!--[if IE 7]> 
      <link rel="stylesheet" type="text/css" href="https://css.content-hcs.com/wu-cont/cssstyles/whatuni-screens_ie7hacks-wu552.css" media="screen" />
      <![endif]-->
