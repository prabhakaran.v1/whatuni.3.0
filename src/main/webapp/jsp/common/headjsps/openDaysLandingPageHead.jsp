<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%--
  * @purpose  This is open day home page head jsp..
  * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * ?               ?                         ?       First Draft                                                       ? 
  * *************************************************************************************************************************
  * 06.05.2020  Prabhakaran V.               1.1     Removed/Updated canonical url                                                              
--%>

  <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
  <meta content="utf-8" http-equiv="encoding">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">
<%
  CommonFunction common = new CommonFunction();
  String envronmentName = common.getWUSysVarValue("WU_ENV_NAME");
  String urlmapping = (request.getAttribute("urlmapping") != null) ? request.getAttribute("urlmapping").toString() : "";  
  String searchCharacter = (request.getAttribute("searchCharacter") != null) ? request.getAttribute("searchCharacter").toString() : "n";
  String indexfollow = searchCharacter !=null && searchCharacter.equalsIgnoreCase("y") ? "noindex,follow" : "index,follow";  
  String qString = request.getQueryString();
        qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  if(!GenericValidator.isBlankOrNull(qString)){
    indexfollow = "noindex,follow";
  }
  String httpStr = new CommonFunction().getSchemeName(request); 
  String canonUrl = httpStr + GlobalConstants.WHATUNI_DOMAIN + GlobalConstants.SEO_PATH_OPEN_DAY_PAGE; //1.1
  String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null);  
  String gaCollegeName = "";
  String opendaysJs = CommonUtil.getResourceMessage("wuni.opendays.js", null);
  String openDayMainCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.styles.css", null);//getting css name dynamically by Sangeeth.S for 31_Jul_18 rel
  String openDayHeaderCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);
  String openDayMobileCSS = CommonUtil.getResourceMessage("wuni.whatuni.mobile.styles.css", null);
  String domainSpecificPath = common.getSchemeName(request)+ CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
  
%>
<%--Modified Entire opendays landing page strcuture by Hema.S on 11.06.2018--%>
  <jsp:include page="/include/seoTitle.jsp">
    <jsp:param name="collegeId" value="0"/>
    <jsp:param name="pageName" value="WU OPEN DAYS HOME PAGE"/>
    <jsp:param name="paramFlag" value="WUNI_OPEN_DAYS" />  
    <jsp:param name="entityText" value="<%=searchCharacter%>" />  
    <jsp:param name="noindexfollow" value="<%=indexfollow%>" />  
  </jsp:include> 
  <%if(!"".equals(canonUrl)){%>
    <link rel="canonical" href="<%=canonUrl%>"/>
  <%}%>   
 
  <jsp:include page="/jsp/common/includeMainCSS.jsp"/> 
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=openDayHeaderCssName%>" media="screen" />
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=openDayMainCssName%>" media="screen" />  
  <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/openday/<%=opendaysJs%>"></script>
  <script type="text/javascript" language="javascript">
    var dev = jQuery.noConflict();
    dev(document).ready(function(){
      adjustStyle();
    });
    function adjustStyle() {
      var width = document.documentElement.clientWidth;
      var path = ""; 
      if (width <= 1024) {
        <%if(("LIVE").equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=openDayMobileCSS%>";
        <%}else if("TEST".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=openDayMobileCSS%>";
        <%}else if("DEV".equals(envronmentName)){%>
            document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=openDayMobileCSS%>";
        <%}%>
      }   
    }
    dev(window).on('orientationchange', orientationChangeHandler);
    function orientationChangeHandler(e) {
      setTimeout(function() {
        dev(window).trigger('resize');
      }, 500);
    }
    dev(window).resize(function() {  
      adjustStyle();  
    });
  </script>
