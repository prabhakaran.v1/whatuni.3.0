<%@ taglib uri="http://java.sun.com/jstl/core"  prefix="c" %>
<%@page import="WUI.utilities.CommonUtil" %>
 <%
    String emailDomainJs = CommonUtil.getResourceMessage("wuni.email.domain.js", null);
    String facebookLoginJSName = CommonUtil.getResourceMessage("wuni.facebook.login.js", null);
    String commonUserProfileJSName = CommonUtil.getResourceMessage("wuni.common.user.profile.js", null);        
    String provisionalOfferJsName = CommonUtil.getResourceMessage("wuni.provisional.offer.js", null);
    String pageName = (String) request.getAttribute("pagename3");
    
    String paramName = "";
    String paramFlag = "";
  %>  
<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">     
    <jsp:include page="/include/htmlTitle.jsp">
      <jsp:param name="indexFollowFlag" value="noindex,follow"/>
    </jsp:include>
    <%@include  file="/jsp/common/includeMainCSS.jsp"%>
    <jsp:include page="/jsp/whatunigo/include/includeWhatuniGoResponsive.jsp"/>
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>
    <script type="text/javascript" language="javascript" src="//connect.facebook.net/en_US/all.js"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=facebookLoginJSName%>"> </script>  
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=commonUserProfileJSName%>"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/PostcodeValidate.js"> </script>   
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/modal-message.js"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/emaildomain/<%=emailDomainJs%>"> </script>               
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/whatunigo/<%=provisionalOfferJsName%>"> </script> 
     <c:set var="pageName" value="<%=pageName%>" />
     <c:if test="${pageName eq 'whatuniGoSignUp.jsp'}">
       <% paramName = "WUGO SIGNUP";
          paramFlag =  "WUGO_SIGNUP";
         %>
     </c:if>
     <c:if test="${pageName eq 'whatuniGoSignIn.jsp'}">
       <% paramName = "WUGO SIGNIN";
          paramFlag =  "WUGO_SIGNIN";
         %>
     </c:if>
    <jsp:include page="/include/seoTitle.jsp"> 
        <jsp:param name="collegeId" value="0"/>
        <jsp:param name="pageName" value="<%=paramName%>" /> 
        <jsp:param name="paramFlag" value="<%=paramFlag %>" />       
        <jsp:param name="noindexfollow" value="noindex,follow"/>
    </jsp:include>