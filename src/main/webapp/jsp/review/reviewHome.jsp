<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import=" WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator" %>
    <%
      String indexfollow = "index,follow";
      String seoPageName = "WUNI READ REVIEW PAGE";
      String canonUrl = "";  
      String previousUrl ="", nextUrl ="", pageURL = "", orderByUrl = "", orderBy =""; 
      String collegeDisplayName = !GenericValidator.isBlankOrNull((String)request.getAttribute("interactionCollegeNameDisplay")) ? (String)request.getAttribute("interactionCollegeNameDisplay") : "";
      String reviewRating = !GenericValidator.isBlankOrNull((String)request.getAttribute("reviewRating")) ? (String)request.getAttribute("reviewRating") : "0";
      String reviewCount = !GenericValidator.isBlankOrNull((String)request.getAttribute("reviewCnt")) ? (String)request.getAttribute("reviewCnt") : "0";
      String overAllRating = !GenericValidator.isBlankOrNull((String)request.getAttribute("overAllRating")) ? (String)request.getAttribute("overAllRating") : "0";
      String reviewsJSName = CommonUtil.getResourceMessage("wuni.review.js", null);      
      String whatuniHeaderStyleCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);  
      String whatuniMainStyleCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.styles.css", null);
      String totalRecordCount = (request.getAttribute("totalRecordCount")!=null) ? request.getAttribute("totalRecordCount").toString() : "0";  
      String pageNo =((request.getAttribute("page")!=null) ? request.getAttribute("page").toString() : "1");
      int pageVal = Integer.parseInt(pageNo);
      int noOfPage = (Integer.parseInt(totalRecordCount)/5);
      if((Integer.parseInt(totalRecordCount)%5) > 0) {
        noOfPage++; 
      }
      boolean provRevPageFlag = false;
      boolean SearchRevPageFlag = false;
      
      String collegeId = "0", refineSubjectId = "", refineKeyword = "";
      if(request.getAttribute("collegeId")!=null && !"".equals(request.getAttribute("collegeId"))){
        collegeId = (String)request.getAttribute("collegeId");
      }  
      if(request.getAttribute("refSubjectId")!=null && !"".equals(request.getAttribute("refSubjectId"))){
        refineSubjectId = (String)request.getAttribute("refSubjectId");
      }    
      if(request.getAttribute("reviewSearchKeyword")!=null && !"".equals(request.getAttribute("reviewSearchKeyword"))){
        refineKeyword = (String)request.getAttribute("reviewSearchKeyword");
      }      
      //START :: condition for the page specific seo
      if(request.getAttribute("reviewCurUrl")!=null && !"".equals(request.getAttribute("reviewCurUrl"))){
        pageURL = (String)request.getAttribute("reviewCurUrl");
        canonUrl = pageURL; 
      }
      if(request.getAttribute("tmpOrderBy")!=null && !"".equals(request.getAttribute("tmpOrderBy"))){          
        orderBy = (String)request.getAttribute("tmpOrderBy");
        orderByUrl = "orderby="+orderBy;
        indexfollow = "noindex,follow";
      }
      if(request.getAttribute("provSrchesult")!=null && "true".equals(request.getAttribute("provSrchesult"))){
        provRevPageFlag = true;
        seoPageName = "WUNI PROVIDER REVIEW PAGE";      
      }else if(request.getAttribute("searchResult")!=null && "true".equals(request.getAttribute("searchResult"))){
        SearchRevPageFlag = true;
        indexfollow = "noindex,follow";
        seoPageName = "WUNI REVIEW SEARCH PAGE";
      }      
      if(pageVal>1){
        indexfollow = "noindex,follow";
        previousUrl = pageURL + (!"".equals(orderByUrl) ? "?"+orderByUrl+"&" : "?") + "pageno="+ String.valueOf(pageVal-1);
        nextUrl     = pageURL + (!"".equals(orderByUrl) ? "?"+orderByUrl+"&" : "?") + "pageno="+ String.valueOf(pageVal+1);
      }
      if(pageVal==noOfPage){
        previousUrl = pageURL + (!"".equals(orderByUrl) ? "?"+orderByUrl+"&" : "?") + "pageno="+ String.valueOf(pageVal-1);
        nextUrl     = "";
      }
      //END :: condition for the page specific seo
    %>    

  <body class="odbnr_sr">
    <%if(provRevPageFlag){%>
      <c:if test="${not empty requestScope.pixelTracking}">${requestScope.pixelTracking}</c:if>
    <%}%>
    <%--START :: Header section--%>
    <header class="md clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>
    </header>
    <%--END :: Header section--%>    
    <div class="container1 wrt_ch">
      <div class="opd_sr fl read_rew">        
        <%--START :: Review Header section--%>  
        <jsp:include page="/jsp/review/include/includeReviewHeader.jsp">
          <jsp:param name="reviewRatingExact" value="<%=reviewRating%>"/>
          <jsp:param name="overAllRating"  value="<%=overAllRating%>"/>
        </jsp:include>
        <%--END :: Review Header section--%>        
        <%--START :: Review Body section--%>  
        <div class="opd_res">
          <div class="ad_cnr">
            <div class="content-bg">               
              <jsp:include page="/jsp/review/include/includeReviewSearchFilter.jsp"/>              
              <jsp:include page="/jsp/review/include/includeReviewResults.jsp"/>                        
            </div>
          </div>
        </div>       
        <%--END :: Review Body section--%>        
      </div>
      <c:if test="${not empty requestScope.collegeId}">
        <input type="hidden" id="providerId" value="${requestScope.collegeId}"/>
        <input type="hidden" id="providerUrl" value="${requestScope.providerUrl}"/>  
      </c:if>  
      <c:if test="${not empty requestScope.reviewCurUrl}">
        <input type="hidden" id="curReviewUrl" value="${requestScope.reviewCurUrl}"/>
      </c:if>
      <jsp:include page="/jsp/common/wuFooter.jsp" />
    </div>
  </body>