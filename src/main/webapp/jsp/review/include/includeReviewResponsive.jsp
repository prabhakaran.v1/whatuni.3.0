
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction"%>
<%
String whatuniMobilestyleCSSName = CommonUtil.getResourceMessage("wuni.whatuni.mobile.styles.css", null);
String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");%>
<script type="text/javascript" language="javascript">
var dev = jQuery.noConflict();
dev(document).ready(function(){
adjustStyle();
});
adjustStyle();
function jqueryWidth() {
  return dev(this).width();
} 
function adjustStyle() {    
    var width = document.documentElement.clientWidth;
    var path = ""; 
    //included for dynamic load css for mobile view
    if (width <= 1024) {
      <%if(("LIVE").equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=whatuniMobilestyleCSSName%>";
      <%}else if("TEST".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=whatuniMobilestyleCSSName%>";
      <%}else if("DEV".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=whatuniMobilestyleCSSName%>";
      <%}%>
    }
    //     
    dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
    dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
    if (width <= 480) {    
      if(dev('#ajax_listOfOptions').attr('class') != 'opd_ajax'){
        dev('#ajax_listOfOptions').attr("class", "ajax_mob");
      }      
    } else if ((width > 480) && (width <= 992)) {      
      if(dev('#ajax_listOfOptions').attr('class') != 'opd_ajax'){
        dev('#ajax_listOfOptions').attr("class", "ajax_mob");
      }       
    } else {
       
        if(dev('#ajax_listOfOptions').attr('class') != 'opd_ajax'){
          dev('#ajax_listOfOptions').removeAttr("class");
        }        
        document.getElementById('size-stylesheet').href = "";  
        dev(".hm_srchbx").hide();
    }    
}
dev(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
    setTimeout(function() {
        dev(window).trigger('resize');        
    }, 500);
}
dev(window).resize(function() {  
  dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
  dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());   
  adjustStyle();   
});
</script>
