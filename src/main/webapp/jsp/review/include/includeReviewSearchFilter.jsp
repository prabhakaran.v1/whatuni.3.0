<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="org.apache.commons.validator.GenericValidator,WUI.utilities.GlobalConstants"%>
<%
  String collegeDisplayName = "Enter uni name";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("collegeDispName"))){
    collegeDisplayName = (String)request.getAttribute("collegeDispName");
  }
  String subjectDisplayName = "Enter subject";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("subjectDisplayName"))){
    subjectDisplayName = (String)request.getAttribute("subjectDisplayName");
  }
  String refineSubjectId = "";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("refSubjectId"))){
    refineSubjectId = (String)request.getAttribute("refSubjectId");
  }
  String studyLevelDisplayName = "All";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("reviewSearchStudyLevel"))){
    studyLevelDisplayName = (String)request.getAttribute("reviewSearchStudyLevel");
  }
  String keywordDisplayName = "Something specific?";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("reviewSearchKeyword"))){
    keywordDisplayName = (String)request.getAttribute("reviewSearchKeyword");
  }
  String refOrderBy = "";  
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("orderBy"))){
    refOrderBy = (String)request.getAttribute("orderBy");
  }  
  String textHighlight = "Latest reviews";
  String latSortStyle = "display:block",oldSortStyle = "display:block",highSortStyle = "display:block",lowSortStyle = "display:block";  
  if("HIGHEST_RATED".equalsIgnoreCase(refOrderBy)){
     textHighlight = "Highest rated";
     highSortStyle = "display:none";
  }else if("LOWEST_RATED".equalsIgnoreCase(refOrderBy)){
     textHighlight = "Lowest rated";
     lowSortStyle = "display:none";
  }else if("DATE_DESC".equalsIgnoreCase(refOrderBy)){
     textHighlight = "Latest reviews";
     latSortStyle = "display:none";
  }else if("DATE_ASC".equalsIgnoreCase(refOrderBy)){   
     textHighlight = "Oldest reviews";
     oldSortStyle = "display:none";
  }else{
     latSortStyle = "display:none";
  }
  //
  String commonPhraseStlye = "display:block";
  String commonPhraseClassName = "defaultCP";
  //
%>
 
<div class="ods_sticky">  
  <%--START :: Review Filter section--%>   
  <div class="odsrch_cnt rrsrch_cnt wrt_row">
    <div class="odsrh_col1 fl">
      <label class="rrev_txt">Study level</label>
      <fieldset class="odsrh_fie">
      <form name="qualificationForm" id="qualificationForm" method="post" action="/home.html" onsubmit="javascript:return searchMoreReviewListURL('qualificationForm')">
      <div class="land_inp_grp sub_inpt fl">
        <div class="sel_degcnr cr_srch fl" id="styLvlDivId">
          <div class="sel_deg fl_w100">
            <input type="text" name="navQualTP" id="navQualTP" class="sld_deg fnt_lbk" value="<%=studyLevelDisplayName%>" readonly="readonly" onclick="toggleQualListMob(null,'top_nav');"> 
          </div>
          <ul id="topNavQualList" class="drop_deg" style="display: none;">
          <li class="fl" onclick="setQualTP('All');searchMoreReviewListURL('qualificationForm');" id="dpdwnkntp1">All</li>
            <li class="fl" onclick="setQualTP('Undergraduate');searchMoreReviewListURL('qualificationForm');" id="dpdwnkntp2">Undergraduate</li>	
            <li class="fl" onclick="setQualTP('Postgraduate');searchMoreReviewListURL('qualificationForm');" id="dpdwnkntp3">Postgraduate</li>	
          </ul>
                  </div>
      </div>
      </form>
      </fieldset>
    </div>						
    <div class="odsrh_col1 fl">
      <label class="rrev_txt">Search university</label>
      <fieldset class="odsrh_fie">
        <form name="uniNameForm" id="uniNameForm" method="post" action="/home.html" onsubmit="javascript:return searchMoreReviewListURL('uniNameForm')">
          <input type="text" name="revUniName" id="revUniName" value="<%=collegeDisplayName%>" autocomplete="off" class="txt_box nw"
           onkeydown="clearUniNAME(event,this);validateSrchIcn('uni_icn','revUniName');" 
           onkeyup="autoCompleteUniNAME(event,this,'REVIEW_PAGE')"  
           onclick="clearReviewSearchText(this,'<%=collegeDisplayName%>')"
           onblur="setReviewSearchText(this,'<%=collegeDisplayName%>')"
           onkeypress="javascript:if(event.keyCode==13){return searchMoreReviewListURL('uniNameForm')}" />
          <input type="hidden" id="revUniName_hidden"/>       
          <input type="hidden" id="revUniName_id"/>
          <input type="hidden" id="revUniName_display"/>
          <input type="hidden" id="revUniName_url"/>
        </form>
        <i class="fa fa-search srch_dis" id="uni_icn" aria-hidden="true" onclick="javascript:return searchMoreReviewListURL('uniNameForm')"></i>
      </fieldset>
    </div>
    <div class="odsrh_col1 fl">
      <label class="rrev_txt">Subject name</label>
      <fieldset class="odsrh_fie">
        <form name="subNameForm" id="subNameForm" method="post" action="/home.html" onsubmit="javascript:return searchMoreReviewListURL('subNameForm')">
          <input type="text" name="revSubjectId" id="revSubjectId" value="<%=subjectDisplayName%>" autocomplete="off" class="txt_box nw"
           onkeydown="clearUniNAME(event,this);validateSrchIcn('sub_icn','revSubjectId');" 
           onkeyup="autoCompleteSubjectName(event,this)" 
           onclick="clearReviewSearchText(this,'<%=subjectDisplayName%>')"
           onblur="setReviewSearchText(this,'<%=subjectDisplayName%>')"
           onkeypress="javascript:if(event.keyCode==13){return searchMoreReviewListURL('subNameForm')}" />
          <input type="hidden" id="revSubjectId_hidden" value="<%=refineSubjectId%>"/>
        </form>
        <i class="fa fa-search srch_dis" id="sub_icn" aria-hidden="true" onclick="javascript:return searchMoreReviewListURL('subNameForm')"></i>
      </fieldset>
    </div>
  </div>  
  <div class="odsrch_cnt rrsrch_cnt rev_men">
    <div class="odsrh_col1 fl revm_lft">
      <label class="rrev_txt">Reviews that mention</label>
      <fieldset class="odsrh_fie">
        <form name="reviewSearchKwdForm" id="reviewSearchKwdForm" method="post" action="/home.html" onsubmit="javascript:return searchMoreReviewListURL('reviewSearchKwdForm')">
          <input type="text" name="reviewSearchKwd" onkeyup="validateSrchIcn('key_icn','reviewSearchKwd');" onkeydown="validateSrchIcn('key_icn','reviewSearchKwd')" id="reviewSearchKwd" value="<%=keywordDisplayName%>" autocomplete="off" onclick="clearReviewSearchText(this,'<%=keywordDisplayName%>');" onblur="setReviewSearchText(this,'<%=keywordDisplayName%>')" maxlength="200"/>
        </form>
        <i class="fa fa-search srch_dis" id="key_icn" aria-hidden="true" onclick="javascript:return searchMoreReviewListURL('reviewSearchKwdForm')"></i>
      </fieldset>
    </div>    
    <c:if test="${not empty requestScope.commonPhrasesList}">
      <div class="odsrh_col1 fl revm_rht">
        <div class="swrd_cnt"><span>or</span></div>
        <div class="swrd_cnt">
          <form name="reviewCommonPhraseKwdForm" id="reviewCommonPhraseKwdForm" method="post" action="/home.html" onsubmit="javascript:return searchMoreReviewListURL('reviewCommonPhraseKwdForm')">
            <ul>
              <c:forEach items="${requestScope.commonPhrasesList}" var="commonPhrasesListId" varStatus="index">
              <c:set var="indexValue" value="${index.index}"></c:set>
                <%if(Integer.parseInt(pageContext.getAttribute("indexValue").toString())>2){
                    commonPhraseStlye = "display:none";                    
                    commonPhraseClassName = "extraCP";   
                  }
                %>
                <li style="<%=commonPhraseStlye%>" class="<%=commonPhraseClassName%>" id="comPhrase_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>" onclick="searchMoreReviewListURL('reviewCommonPhraseKwdForm','',this)" value="${commonPhrasesListId.commonPhrases}"><a href="javascript:void(0);" title="${commonPhrasesListId.commonPhrases}">${commonPhrasesListId.commonPhrases}</a></li>                            
              </c:forEach>
            </ul>
          </form>
        </div>
        
        <div class="swrd_cnt">
          <%if("display:none" == commonPhraseStlye){%>
            <ul>
              <li id="comPhraseViewMore" onclick="commonPhraseViewMoreAndLess('comPhraseViewMore')"><a href="javascript:void(0);" title="View more">+ View more</a></li>
              <li id="comPhraseViewLess" onclick="commonPhraseViewMoreAndLess('comPhraseViewLess')" style="display:none"><a href="javascript:void(0);" title="View less">- View less</a></li>
            </ul>
          <%}%>
        </div>        
      </div>
    </c:if>
  </div>
  <%--END :: Review Filter section--%>  
  <%--START :: no result section - Need to move to constants once the no results text is confirmsed --%>  
  <c:if test="${empty requestScope.reviewSearchList}">
    <div class="op_nores">
      <h2 class="ud_tit">Sorry, we can't find any reviews with the selected filters you entered!<br>Try again with a different filters and you may have more luck.</h2> 
      <div class="sr_calbtn2"><a class="sr_pd" href="<%=GlobalConstants.SEO_PATH_LIST_REVIEW%>">START NEW SEARCH<i class="fa fa-long-arrow-right"></i></a></div>
    </div>
  </c:if>  
  <%--END :: no result section--%>  
  <%--START :: sort Filter section--%> 
  <c:if test="${not empty requestScope.reviewSearchList}">
      <div class="odsrch_cnt rrsrch_cnt late_rev">
        <div class="odsrh_col2 fl revm_lft">
          <fieldset class="fs_col2 fl" id="location">
           <div class="srt_lst fl"><label>Sort:</label></div>
            <div class="od_dloc" id="sortId"><span id="sortTextId"><%=textHighlight%></span><span><i class="fa fa-caret-down"></i></span></div>
            <div class="opsr_lst" id="drp_sortId">
              <ul id="reviewSortId">
                <li id="DteHigh" value="DteHigh" style="<%=latSortStyle%>" onclick="setDropDownValue(this,'sortTextId');sortSearchReviewListURL(this);"><span><a href="javascript:void(0);"> Latest reviews</a></span></li>
                <li id="Dtelow" value="Dtelow" style="<%=oldSortStyle%>" onclick="setDropDownValue(this,'sortTextId');sortSearchReviewListURL(this);"><span><a href="javascript:void(0);"> Oldest reviews</a></span></li>
                <li id="high" value="high" style="<%=highSortStyle%>" onclick="setDropDownValue(this,'sortTextId');sortSearchReviewListURL(this);"><span><a href="javascript:void(0);"> Highest rated</a></span></li>
                <li id="low" value="low" style="<%=lowSortStyle%>" onclick="setDropDownValue(this,'sortTextId');sortSearchReviewListURL(this);"><span><a href="javascript:void(0);"> Lowest rated</a></span></li>
              </ul>
            </div>
          </fieldset>
        </div>
      </div>
  </c:if>
  <%--END :: sort Filter section--%>
</div>