<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO"%>
<%@page import="WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants, WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator" %>
<%
  CommonUtil commonUtil = new CommonUtil();
  String displaySecStyle = "", viewMoreButStyle = "", viewLessButStyle = "";
  String reviewId = "";
  String reviewCatNameDisp = "";
  String userNameColorClassName = "rev_blue";
  String questionTitle = "Having said what you have said would you recommend your uni?";
  pageContext.setAttribute("questionTitle", questionTitle);
  String categoryNames = ",Job Prospects,Course and Lecturers,Clubs and Societies,Students' Union,City Life,Giving Back,";
         categoryNames = categoryNames.toLowerCase();
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("reviewId"))) {
    reviewId = (String)request.getAttribute("reviewId");
  }
  String previousUrl ="", nextUrl ="", orderByUrl = "";
  String totalRecordCount = (request.getAttribute("totalRecordCount")!=null) ? request.getAttribute("totalRecordCount").toString() : "0";  
  int noOfPage = (Integer.parseInt(totalRecordCount)/5);
  if((Integer.parseInt(totalRecordCount)%5) > 0) {
    noOfPage++; 
  }            
  String pageNo =((request.getAttribute("page")!=null) ? request.getAttribute("page").toString() : "1");
  int pageVal = Integer.parseInt(pageNo);
  String pageURL = "", orderBy ="";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("reviewCurUrl"))){
    pageURL = (String)request.getAttribute("reviewCurUrl");
  }        
  String collegeId = "0";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("collegeId"))){
    collegeId = (String)request.getAttribute("collegeId");
  }      
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("tmpOrderBy"))){
    orderBy = (String)request.getAttribute("tmpOrderBy");
    orderByUrl = "&orderby="+orderBy;
  }     
  if(pageVal>1){
    previousUrl = pageURL + orderByUrl + "&pageno="+ String.valueOf(pageVal-1);
    nextUrl     = pageURL + orderByUrl + "&pageno="+ String.valueOf(pageVal+1);
  }
  if(pageVal==noOfPage){
    previousUrl = pageURL + orderByUrl + "&pageno="+ String.valueOf(pageVal-1);
    nextUrl     = "";
  }
  String awardYear = new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS");
  String onePixelImgUrl = CommonUtil.getImgPath(GlobalConstants.ONE_PIXEL_IMG_PATH, 1);
  String studentAwardLogoUrl = CommonUtil.getImgPath("/wu-cont/images/awards/"+awardYear+"/wusca_rev_logo.png", 0);
%>
<div class="rev_lst">
<c:if test="${not empty requestScope.reviewSearchList}">
    <div class="rev_cen">
      <c:forEach var="reviewResList" items="${requestScope.reviewSearchList}" varStatus="index">  
      <c:set var="indexValue" value="${index.index}"></c:set>    
          <%  
            if(Integer.parseInt(pageContext.getAttribute("indexValue").toString())==0){
              if(!"".equals(reviewId)){
                displaySecStyle  = "display:block;";
                viewMoreButStyle = "display:none;";
                viewLessButStyle = "display:block;";                                              
              }else{
                displaySecStyle  = "display:none;";
                viewMoreButStyle = "display:block;";
                viewLessButStyle = "display:none;";
              }
             }else{
                displaySecStyle  = "display:none;";
                viewMoreButStyle = "display:block;";
                viewLessButStyle = "display:none;";
             }      
          %>  
            <%--START :: USER REVIEW DETAILS--%>
             <div class="rlst_row">
                <div class="revlst_lft fl">
                  <div class="rlst_wrp fr">
                    <c:if test="${not empty reviewResList.userName}">
                        <c:set var="userName" value="${reviewResList.userName}"/> 
                        <%
                          userNameColorClassName = commonUtil.getReviewUserNameColorCode(pageContext.getAttribute("userName").toString());
                        %>
                        <div class="rev_prof <%=userNameColorClassName%>">${reviewResList.userNameIntial}</div>
                    </c:if>
                    <c:if test="${not empty reviewResList.userName}">
                        <div class="rev_name">${reviewResList.userName}</div>
                    </c:if>
                    <c:if test="${not empty reviewResList.createdDate}">
                        <div class="rev_dte">${reviewResList.createdDate}</div>
                    </c:if>  
                  </div>                
                </div>  
                <div class="revlst_rht fl">
                  <div class="rlst_wrap">
                    <c:if test="${not empty reviewResList.collegeDisplayName}">
                        <h2><a href="${reviewResList.uniHomeURL}" onclick="viewProfileGaLogging('${reviewResList.collegeDisplayName}')" title="${reviewResList.collegeDisplayName}">${reviewResList.collegeDisplayName}</a></h2>
                    </c:if>
                    <c:if test="${not empty reviewResList.courseName}">
                        <c:if test="${reviewResList.courseDeletedFlag eq 'TRUE'}">
                          <h3>${reviewResList.courseName}</h3>
                        </c:if>
                        <c:if test="${reviewResList.courseDeletedFlag eq 'FALSE'}">
                          <h3><a href="<SEO:SEOURL pageTitle="coursedetail" >${reviewResList.seoStudyLevelText }<spring:message code="wuni.seo.url.split.character" />${reviewResList.courseName}<spring:message code="wuni.seo.url.split.character" />${reviewResList.collegeName}<spring:message code="wuni.seo.url.split.character" />${reviewResList.courseId }<spring:message code="wuni.seo.url.split.character" />${reviewResList.collegeId}</SEO:SEOURL>" title="${reviewResList.courseName}">${reviewResList.courseName}</a></h3>
                        </c:if>                        
                    </c:if>  
                    <div class="reviw_rating">
                      <div class="rate_new"><span class="cat_rat">OVERALL UNIVERSITY RATING</span> <span class="ml5 rat rat${reviewResList.overAllRating}"></span> 
                        <c:if test="${not empty reviewResList.overallQuesDesc}">
                            <div class="rw_qus_des">${reviewResList.overallQuesDesc}</div>
                        </c:if>
                        <c:if test="${not empty reviewResList.overallRatingComment}">
                              <p class="rev_dec">${reviewResList.overallRatingComment}</p>
                        </c:if>  
                      </div>
                      <c:if test="${not empty reviewResList.viewMoreRes}">
                          <div id="viewMore_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" style="<%=displaySecStyle%>">                          
                            <c:set var="reviewCatName" value=""/>
                            <c:forEach var="viewMoreList" items="${reviewResList.viewMoreRes}">
                              <c:if test="${not empty viewMoreList.reviewQuestion}">
                                <c:if test="${viewMoreList.reviewQuestion ne questionTitle}">
                                   
                                   <c:if test="${viewMoreList.reviewQuestion ne 'Overall Rating'}">
                                      <div class="rate_new">                                          
                                        
                                         <c:if test="${reviewCatName ne viewMoreList.reviewQuestion}">  
                                           	<span class="cat_rat">${viewMoreList.reviewQuestion}</span>                                                                                  
	                                        <c:if test="${empty viewMoreList.overallRating}"> 
	                                          <span class="t_tip non_app">
	                                             N/A
	                                            <span class="cmp">
	                                              <div class="hdf5"><spring:message code="wuni.review.without.stat.tooltip" /></div>
	                                              <div class="line"></div>
	                                            </span>
	                                          </span>
	                                        </c:if>
	                                        <c:if test="${not empty viewMoreList.overallRating}">
	                                          <span class="ml5 rat rat${viewMoreList.overallRating}"></span>
	                                        </c:if>
                                         </c:if>                                          
                                        <c:if test="${not empty viewMoreList.questionDesc and not empty viewMoreList.reviewAnswer}">
                                            <div class="rw_qus_des">${viewMoreList.questionDesc}</div>
                                        </c:if>  
                                        <c:if test="${not empty viewMoreList.reviewAnswer}">
                                          <p class="rev_dec">${viewMoreList.reviewAnswer}</p>
                                        </c:if>
                                        <c:set var="reviewCatName" value="${viewMoreList.reviewQuestion}"/>
                                      </div>
                                   </c:if>
                                </c:if>
                                <c:if test="${viewMoreList.reviewQuestion eq questionTitle and viewMoreList.reviewAnswer eq 'Yes'}">
                                    <div class="rate_new rev_yes">  
                                      <div class="rw_qus_des">${viewMoreList.reviewQuestion}</div>
                                      <p class="rev_dec"><i class="fa fa-check"></i>Yes</p>
                                    </div>
                                </c:if>
                              </c:if>
                           </c:forEach>
                          </div>
                      </c:if>
                      <div class="rev_mre">
                        <a id="view-more_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" style="<%=viewMoreButStyle%>" title="View more" href="javascript:void(0)" onclick ="viewMoreResult('view_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>','${reviewResList.collegeId}');">+ View more</a>
                        <a id="view-less_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" style="<%=viewLessButStyle%>" title="View less" href="javascript:void(0)" onclick ="viewMoreResult('view_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>','${reviewResList.collegeId}');">- View less</a>
                        <c:if test="${not empty reviewResList.reviewId}">
                          <input type="hidden" id="hidReviewId_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" value="${reviewResList.reviewId}"/>
                        </c:if> 
                      </div>
                      <div class="wr_btn2_wrp" style="${not empty collegeDispName ? 'display: none': 'display: block'}">
                        <a class="wr_btn2" onclick="viewAllReviewsGaLogging('${reviewResList.collegeDisplayName}', '${reviewResList.collegeId}');">
                          <spring:message code="view.all.reviews.btn.name" /><i class="fa fa-long-arrow-right"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div> 
              </div>
            <%--START :: USER REVIEW DETAILS--%>           
          </c:forEach>  
            <!-- Pagination -->
              <div class="pr_pagn">
                <jsp:include page="/reviewRedesign/include/reviewPagination.jsp">
                  <jsp:param name="pageno" value="<%=pageNo%>"/>
                  <jsp:param name="pagelimit"  value="10"/>
                  <jsp:param name="searchhits" value="<%=totalRecordCount%>"/>
                  <jsp:param name="recorddisplay" value="10"/>              
                  <jsp:param name="pageURL" value="<%=pageURL%>"/>
                  <jsp:param name="orderBy" value="<%=orderBy%>"/>
                  <jsp:param name="pageTitle" value=""/>
                </jsp:include>
              </div>
            <!-- Pagination -->    
    </div>
  
</c:if>
 <%--START :: WUSCA Link section--%>        
  <div class="wusca_win fl" id="awardPod">
    <div class="wusca_cnt">
      <div class="wusca_lft fl"><img id="awardPodImg" src="<%=onePixelImgUrl%>" data-src="<%=studentAwardLogoUrl%>" alt="wusca logo"></div>
      <div class="wusca_rgt fl">
        <h3>Whatuni Student Choice awards</h3>
        <div class="sr_btn_cont fl">
          <div class="res_place"><a class="savTag" href="<%=GlobalConstants.AWARDS_PAGE_URL%>" title="View <%=awardYear%> winners">View <%=awardYear%> winners<i class="fa fa-long-arrow-right"></i></a></div>
        </div>
      </div>
    </div>
  </div>        
  <%--END :: WUSCA Link section--%>  
</div>