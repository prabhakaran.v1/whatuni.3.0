<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%--
  * @purpose  This is profile review light box jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 18-Dec-2018   Jeyalakshmi D.              584     First Draft                                                       584 
  * *************************************************************************************************************************
--%>

<c:if test="${not empty requestScope.listOfUserReviews}">
<c:forEach var="userReviews" items="${requestScope.listOfUserReviews}" varStatus="id">
<c:set var="idValue" value="${id.index}"/>
<div class="rvbx_shdw"></div>
<div class="revcls"><a href="javascript:void(0);"><i class="fa fa-times"></i></a></div>
  <div class="rvbx_cnt">
    <div class="rev_lst">
      <div class="rev_cen">
        <div class="rlst_row">
          <div class="revlst_lft fl">
            <div class="rlst_wrp fr">
              <%--Start :: Iterating user review --%>
              <div class="rev_prof ${userReviews.userNameInitialColourcode}">
                ${userReviews.userNameInitial}
              </div>
              <div class="rlst_rht">
                <div class="rev_name">
                  ${userReviews.userName}
                </div>
                <div class="rev_dte">
                  ${userReviews.createdDate}
                </div>
              </div>
            </div>
          </div>
          <div class="revlst_rht fl">
            <div class="rlst_wrap">
              <h2><a href="${userReviews.uniHomeURL}" class="rev_uni link_blue">
                  ${userReviews.collegeNameDisplay}
                  </a>
              </h2>
              <c:if test="${userReviews.courseDeletedFlag eq 'TRUE'}">
                <c:if test="${not empty userReviews.courseName}">
                  <h3>
                    ${userReviews.courseName}
                  </h3>
                </c:if>
                </c:if>
                <c:if test="${userReviews.courseDeletedFlag ne 'TRUE'}">
                  <h3>
                    <a class="rev_sub link_blue" href="${userReviews.courseDetailsURL}" title="${userReviews.courseName}">
                      ${userReviews.courseName}
                    </a>
                  </h3>
                </c:if>
                <c:if test="${not empty userReviews.userMoreReviewaList}">
                  <div class="rev_bor">
                    <div class="lbx_scrl">
                      <div class="rvlbx_cnt">   
                      <c:set var="reviewCatName" value=""/>
                        <c:forEach items="${userReviews.userMoreReviewaList}" var="userReview">
                          <c:if test="${userReview.questionTitle eq 'Overall Rating'}">
                            <c:if test="${userReview.rating ne 'Y'}">
                              <div class="reviw_rating">
                                <div class="rate_new">
                                  <span class="ov_txt cat_rat">OVERALL UNIVERSITY RATING</span>
                                  <span class="ml5 rat rat${userReview.rating}"></span>
                                  <c:if test="${not empty userReview.questionDesc}">
                                    <div class="rw_qus_des">
                                      ${userReview.questionDesc}
                                    </div>
                                  </c:if>
                                </div>
                                <p class="rev_dec">
                                  ${userReview.answer}
                                </p>
                              </div>
                            </c:if>
                          </c:if>
                          <c:if test="${not empty userReview.questionTitle}">
                            <c:if test="${userReview.questionTitle ne 'Overall Rating'}">
                              <c:if test="${userReview.rating ne 'Y'}">
                                
                                <div class="reviw_rating">
                                  <div class="rate_new">
                                
                                     <c:if test="${reviewCatName ne userReview.questionTitle}">
                                       <span class="cat_rat">${userReview.questionTitle}</span>                                       
                                       <c:if test="${empty userReview.rating}">
                                         <span class="t_tip">
                                          N/A
                                         <span class="cmp">
                                         <div class="hdf5">
                                           <spring:message code="wuni.review.without.stat.tooltip" />
                                         </div>
                                         <div class="line"></div>
                                         </span>
                                         </span>
                                       </c:if>
                                       <c:if test="${not empty userReview.rating}">
                                         <span class="ml5 rat rat${userReview.rating}"></span>
                                       </c:if>
                                     </c:if>
                                    
                                    <c:if test="${not empty userReview.questionDesc}">
                                      <div class="rw_qus_des">
                                        ${userReview.questionDesc}
                                      </div>
                                    </c:if>
                                  </div>
                                  <c:if test="${not empty userReview.answer}">
                                    <p class="rev_dec">
                                      ${userReview.answer}
                                    </p>
                                  </c:if>
                                  <c:set var="reviewCatName" value="${userReview.questionTitle}"/>
                                </div>
                              </c:if>
                            </c:if>
                            <c:if test="${userReview.rating eq 'Y'}">
                              <div class="rev-reco">
                                <div class="tit6">
                                  <span class="cat_rat">${userReview.questionTitle}</span>
                                </div>
                                <a class="yes">
                                  <i class="fa fa-check"></i>${userReview.answer}
                                </a>
                              </div>
                            </c:if>
                          </c:if>
                        </c:forEach>
                      </div>
                    </div>
                  </div>
                </c:if>
             </div>
           </div>
        </div>
      </div>
    </div>
  </div>
</c:forEach>
</c:if>