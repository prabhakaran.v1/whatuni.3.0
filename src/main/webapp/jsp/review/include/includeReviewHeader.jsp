
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="org.apache.commons.validator.GenericValidator" %>
<%
  String headLine = "";
  String collegeDisplayName = "";
  String subjectDisplayName = "";
  String studyLevelDisplayName = "";
  String keywordDisplayName = "";
  String totalRecordCount = "0";
  String totalReviewCountDisplay = "0";
  String reviewRatingExact = !GenericValidator.isBlankOrNull(request.getParameter("reviewRatingExact")) ? request.getParameter("reviewRatingExact") : "0";
  String overAllRating = !GenericValidator.isBlankOrNull(request.getParameter("overAllRating")) ? request.getParameter("overAllRating") : "0";
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("collegeDispName"))){
    collegeDisplayName = (String)request.getAttribute("collegeDispName");
  }  
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("subjectDisplayName"))){
    subjectDisplayName = (String)request.getAttribute("subjectDisplayName");
  }  
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("reviewSearchStudyLevel"))){
	  studyLevelDisplayName = (String)request.getAttribute("reviewSearchStudyLevel");
	  }
  if(!GenericValidator.isBlankOrNull((String)request.getAttribute("reviewSearchKeyword"))){
    keywordDisplayName = (String)request.getAttribute("reviewSearchKeyword");
  }
  //condition for the headline text display
  if(request.getAttribute("totalRecordCount")!=null && !"".equals(request.getAttribute("totalRecordCount"))) {
    totalRecordCount = (request.getAttribute("totalRecordCount")!=null) ? (String)request.getAttribute("totalRecordCount"): "0";
    totalReviewCountDisplay = (request.getAttribute("totalReviewCountDisplay")!=null) ? (String)request.getAttribute("totalReviewCountDisplay"): "0";
    String reviewText = " reviews";
    if("1".equals(totalRecordCount.trim())){reviewText = " review";}
    if(collegeDisplayName != "" && subjectDisplayName == "" && keywordDisplayName == ""){
      headLine =  totalReviewCountDisplay +" " + collegeDisplayName + reviewText;
    }else if(collegeDisplayName == "" && subjectDisplayName !="" && keywordDisplayName == ""){
      headLine =  totalReviewCountDisplay +" " + subjectDisplayName + reviewText;
    }else if(collegeDisplayName == "" && subjectDisplayName =="" && keywordDisplayName != ""){
      headLine =  totalReviewCountDisplay +" of" + reviewText + " mentioning \"" + keywordDisplayName + "\"";
    }else if(collegeDisplayName == "" && subjectDisplayName == "" && keywordDisplayName == ""){
      headLine =  totalReviewCountDisplay + " University" + reviewText;  
    }else{
      headLine =  totalReviewCountDisplay + reviewText; 
    }
  }
  //
%>

<%--START :: Review Header section--%>
<div class="bcrmb_hd fl">
  <div class="ad_cnr">
    <div class="content-bg">                
      
      <%--START :: Bread crumb section--%>
      <jsp:include page="/seopods/breadCrumbs.jsp">
         <jsp:param name="pageName" value="REVIEW_PAGE"/>
      </jsp:include>   
      <%--END :: Bread crumb section--%>
      
      <%--START :: Review count and Applied filter details section--%>
      <div class="opuni_srch">
        <h1>          
          <%=headLine%>
        </h1>
        <%if(!"0".equals(overAllRating)){%>
          <div class="rate_new rev_tooltip">Student rating <span class="ml5 rat<%=overAllRating%> t_tip">
          <span class="cmp">
            <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
            <div class="line"></div>
          </span>
          </span>(<%=reviewRatingExact%>)</div>
         <%}%> 
        <ul>
        <c:if test="${not empty requestScope.reviewSearchStudyLevel}">
              <li onclick="removeFilterParams('navQualTP');"><span>${requestScope.reviewSearchStudyLevel} </span><span><i class="fa fa-times"></i></span></li>
          </c:if>
          <c:if test="${not empty requestScope.collegeDispName}">
              <li onclick="removeFilterParams('revUniName');"><span>${requestScope.collegeDispName} </span><span><i class="fa fa-times"></i></span></li>
          </c:if>
          <c:if test="${not empty requestScope.subjectDisplayName}">
              <li onclick="removeFilterParams('revSubjectId');"><span>${requestScope.subjectDisplayName} </span><span><i class="fa fa-times"></i></span></li>
          </c:if>  
          <c:if test="${not empty requestScope.reviewSearchKeyword}">
              <li onclick="removeFilterParams('reviewSearchKwd');"><span>${requestScope.reviewSearchKeyword} </span><span><i class="fa fa-times"></i></span></li>
          </c:if>            
        </ul>
      </div>
      <%--END :: Review count and Applied filter details section--%> 
      <div class="wr_btn_wp">
        <spring:message code="review.form.url" var="reviewFormUrl"/>
        <a class="wr_btn" id="write_rvw_btn" href="${reviewFormUrl}">
          <spring:message code="write.a.review.btn.name"/><i class="fa fa-long-arrow-right"></i>
        </a>
      </div>           
    </div>
  </div>
</div>
<%--END :: Review Header section--%>