<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import= "WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
String link = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();
%>
<body>
<div id="wrap">
    <div id="hdr">
      <div class="lg_hc"><a href="<%=link%>" title="Whatuni home"><img class="wu_lowdt" src="<%=CommonUtil.getImgPath("",0)%>/wu-cont/images/whatuni_logo.svg" alt="Find UK courses - part time courses, undergraduate, postgraduate courses"></a></div>
    </div>
    <!--END OF HEADER-->
    <div class="clear"></div>
    <!--unsubscribe-->
    <div class="unsb">
      <div class="uTxt"><b><spring:message code="mailing.preference.title"/></b></div>
      <div class="uCnt">
        <p><spring:message code="mailing.preference.text"/></p>
        <form name="recruit" action="/degrees/save-user-preference.html" method="POST" modelAttribute="unSubcribeForm">
          <c:if test="${saveFlag eq 'Y'}">          
            <p style="font-family:verdana;font-size:10pt;color:red; font-weight: bold; text-align: left; padding:0px 5px 0px 5px;"><spring:message code="mailing.success.text"/></p>          
          </c:if> 
          <%-- <p>E-mail address:<input type="text" size="40" maxlength="50" name="p_email" value="" class="main"></p> --%>
          
          <div class="perf">
            <fieldset class="int " id="market_field">
              <div class="eml_perf">
                <fieldset class="perf_txt">
                  <p class="bld"><spring:message code="newsletters.text"/></p>
                  <label><spring:message code="newsletters.label"/></label>
                </fieldset>
                <fieldset class="mychk_rt">
                  <div class="controls">
                    <c:if test="${newsLetterFlag eq 'Y'}">
                      <input class="slideCheck" onclick="onOFFEmail('newsLetters', 'editorEmail')" id="newsLetters" type="checkbox">
                    </c:if>
                    <c:if test="${newsLetterFlag ne 'Y'}">
                      <input class="slideCheck" onclick="onOFFEmail('newsLetters', 'editorEmail')" id="newsLetters" type="checkbox" checked>
                    </c:if>
                    <label for="newsLetters" class="slideCheckDiv"></label>
                  </div>
                </fieldset>
              </div>
            </fieldset>
            <fieldset class="int " id="solus_field">
              <div class="eml_perf">
                <fieldset class="perf_txt">
                  <p class="bld"><spring:message code="university.text"/></p>
                  <label><spring:message code="university.label"/></label>
                </fieldset>
                <fieldset class="mychk_rt">
                  <div class="controls">
                  <c:if test="${solusFlag eq 'Y'}">
                    <input class="slideCheck" onclick="onOFFEmail('solusEmail', 'patnerEmail')" id="solusEmail" type="checkbox">
                  </c:if>
                  <c:if test="${solusFlag ne 'Y'}">
                    <input class="slideCheck" onclick="onOFFEmail('solusEmail', 'patnerEmail')" id="solusEmail" type="checkbox" checked>
                  </c:if>
                    <label for="solusEmail" class="slideCheckDiv"></label>
                  </div>
                </fieldset>
              </div>
            </fieldset>
            <fieldset class="int " id="remind_field">
              <div class="eml_perf">
                <fieldset class="perf_txt">
                  <p class="bld"><spring:message code="reminders.text"/></p>
                  <label><spring:message code="reminders.label"/></label>
                </fieldset>
                <fieldset class="mychk_rt">
                  <div class="controls">
                  <c:if test="${remainderFlag eq 'Y'}">
                    <input class="slideCheck" onclick="onOFFEmail('remainderMail', 'remainEmail')" id="remainderMail" type="checkbox">
                  </c:if>
                  <c:if test="${remainderFlag ne 'Y'}">
                    <input class="slideCheck" onclick="onOFFEmail('remainderMail', 'remainEmail')" id="remainderMail" type="checkbox" checked>
                  </c:if>                   
                    <label for="remainderMail" class="slideCheckDiv"></label>
                  </div>
                </fieldset>
              </div>
            </fieldset>            
            <fieldset class="int  " id="survey_field" style="display : block">
              <div class="eml_perf">
                <fieldset class="perf_txt">
                  <p class="bld"><spring:message code="surveys.text"/></p>
                  <label><spring:message code="surveys.label"/></label>
                </fieldset>
                <fieldset class="mychk_rt">
                  <div class="controls">
                  <c:if test="${surveyFlag eq 'Y'}">
                    <input class="slideCheck" onclick="onOFFEmail('survey', 'surveyFlag')" id="survey" type="checkbox">
                  </c:if>
                  <c:if test="${surveyFlag ne 'Y'}">
                    <input class="slideCheck" onclick="onOFFEmail('survey', 'surveyFlag')" id="survey" type="checkbox" checked>
                  </c:if>
                    <label for="survey" class="slideCheckDiv"></label>
                  </div>
                </fieldset>
              </div>
            </fieldset>
            <c:if test="${not empty reviewSurveyFlag}">
              <fieldset class="int  " id="review_survey_field" style="display: block">
                <div class="eml_perf">
                  <fieldset class="perf_txt">
                    <p class="bld"><spring:message code="review.survey.text" /></p>
                    <label><spring:message code="review.survey.label" /></label>
                  </fieldset>
                  <fieldset class="mychk_rt">
                    <div class="controls">
                      <c:if test="${reviewSurveyFlag eq 'Y'}">
                        <input class="slideCheck" onclick="onOFFEmail('reviewSurvey', 'reviewSurveyFlag')" id="reviewSurvey" type="checkbox">
                      </c:if>
                      <c:if test="${reviewSurveyFlag ne 'Y'}">
                        <input class="slideCheck" onclick="onOFFEmail('reviewSurvey', 'reviewSurveyFlag')" id="reviewSurvey" type="checkbox" checked>
                      </c:if>
                      <label for="reviewSurvey" class="slideCheckDiv"></label>
                    </div>
                  </fieldset>
                </div>
              </fieldset>
            </c:if>
            <input type="hidden" name="encryptUserId" value="${encryptedUserId}">    
            <input type="hidden" name="newsLetters" id="editorEmail" value="${newsLetterFlag}"> 
            <input type="hidden" name="solusEmail" id="patnerEmail" value="${solusFlag}"> 
            <input type="hidden" name="remainderMail" id="remainEmail" value="${remainderFlag}"> 
            <input type="hidden" name="survey" id="surveyFlag" value="${surveyFlag}">         
            <input type="hidden" name="reviewSurveyFlag" id="reviewSurveyFlag" value="${reviewSurveyFlag}">
            <input type="submit" border="0" alt="Unsubscribe" value="Save changes &raquo;&raquo;" class="uBtn">
            <p><spring:message code="please.note.text"/></b> </p>
        </form>
        </div>
        <div class="uTxt"></div>
      </div>
      <!--unsubscribe-->        
    </div>
<jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
<jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />
</body>
