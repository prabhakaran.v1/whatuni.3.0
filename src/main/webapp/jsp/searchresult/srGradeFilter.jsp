<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>  
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% 
 CommonUtil util = new CommonUtil();
 CommonFunction common = new CommonFunction(); 
%>

<c:choose>
 <c:when test="${not empty requestScope.studyLevel and not empty requestScope.entryPoints and fn:length(fn:split(requestScope.studyLevel,',')) gt 0}">
  <c:set var="lengthVal" value="${fn:length(fn:split(requestScope.studyLevel,','))}"/>
  <c:if test="${empty addQualFlag}">
    <div class="fltr_hd stc_hd" id="fltr_hd_id">
	   <h2>Recalculate your UCAS points</h2>
	</div>
    <div class="custm_scrl ajax_ul">
  </c:if>
   
  <c:forTokens var="studyLevelList" items="${requestScope.studyLevel}" varStatus="indexLoop" delims=",">  
    <c:set var="indexId" value="${indexLoop.index}"/>
    <%
    String addQualFlag = (String)request.getAttribute("addQualFlag");
    int indexIdVal = Integer.valueOf(pageContext.getAttribute("indexId").toString());
    if("Y".equalsIgnoreCase(addQualFlag)){
      indexIdVal += 1;
    }
    double totalPoints = 0; 
    double hePts= 0;
    %>  
    <c:set var="indexIdVal" value="<%=indexIdVal%>"/>
    <c:forEach var="gradeFilterList" items="${requestScope.gradeFilterList}" varStatus="index">  
      <c:if test="${fn:toLowerCase(gradeFilterList.qualificationUrl) eq fn:toLowerCase(studyLevelList)}">
        <div class="fltr_wrp ${indexIdVal gt 0 ? 'fltr_plus' : ''}" id="additional_qual_${indexIdVal}">
          <h3>${indexIdVal eq '0' ? 'Qualification' : indexIdVal eq '1' ? 'Second qualification' : 'Third qualification'}</h3>
          <!-- start -->
          <div class="fltr_box ">
            <div class="fltr_dd">
              <ul class="drp_val" data-grade-option-open="${indexIdVal}">
                <li>
                  <span id="qual_prepopulate_${indexIdVal}">${gradeFilterList.qualification}</span>
                </li>  
              </ul>
              <div id="ajax_listOfOptions_grade${indexIdVal}">              
              <ul class="cstm_drp ajax_ul dsp_none" id="grade_option_${indexIdVal}" style="display: none;">
                <c:set var="gradeOptions" value="${gradeFilterList.gradeOptions}"/>
	            <c:set var="parentQualification" value="${gradeFilterList.parentQualification}"/>
	            <c:set var="qualification" value="${gradeFilterList.qualification}"/>
	            <c:set var="qualificationUrl" value="${gradeFilterList.qualificationUrl}"/>
	            <c:set var="qualId" value="${gradeFilterList.qualId}"/>
	            <c:set var="maxPoint" value="${gradeFilterList.maxPoint}"/>
	            <c:set var="maxTotalPoint" value="${gradeFilterList.maxTotalPoint}"/>
	            <c:set var="gradeOptionflag" value="${gradeFilterList.gradeOptionflag}"/>
	            <c:forEach var="gradeFilterListInner" items="${requestScope.gradeFilterList}" varStatus="indexInner">
	              <c:if test="${indexInner.index eq 0}">	              
	              </c:if>   
	                <c:choose>
	                  <c:when test="${gradeFilterListInner.qualification eq gradeFilterListInner.parentQualification and empty gradeFilterListInner.qualId}">
	                    <c:if test="${indexInner.index ne 0}">
	                      <li class="line_sp"></li>
						</c:if>
	                    <li class="ql_dsp">
	                      <a href="javascript:void(0);">${gradeFilterListInner.qualification}</a>
	                    </li>
	                  </c:when>
	                  <c:otherwise>
	                    <li class="ajaxlist ${fn:toLowerCase(gradeFilterListInner.qualificationUrl) eq fn:toLowerCase(studyLevelList) ? 'drp_act' : ''}">
	                       <a href="javascript:void(0);" data-filter-show="yourGrades" data-load-qual-id-value="${gradeFilterListInner.qualId}" data-load-qual-value="${gradeFilterListInner.qualification}" data-load-qual-order-option="${indexIdVal}">${gradeFilterListInner.qualification}</a>
	                    </li>
	                  </c:otherwise>
	                </c:choose>      
	            </c:forEach>            
              </ul>
              
              </div>
            </div>
            
            <c:if test="${addQualFlag eq 'Y' or indexIdVal gt 0}">
                <a href="javascript:void(0);" data-grade-remove-option="${indexIdVal}" class="lvl_cls"><img src='<%=CommonUtil.getImgPath("/wu-cont/images/clr_fltr_close_blu.svg",0)%>' alt="close icon" width="12" height="12"></a>
              </c:if>
              
            <div class="fltr_levls" id="grade_filter_${indexIdVal}">
              <c:forTokens var="gradeOptionsVal" delims="," items="${gradeOptions}" varStatus="indexInner">
                <c:if test="${gradeOptionflag eq 'Y' and (qualId ne '20' and qualId ne '19')}">
                  <c:set var="entryPointsArr">${fn:split(requestScope.entryPoints,',')[indexId]}</c:set> 
                  <c:forTokens var="entryPointsArrVal" items="${entryPointsArr}" delims="-">                                   
                    <c:if test="${fn:endsWith(fn:trim(fn:toLowerCase(entryPointsArrVal)), fn:toLowerCase(fn:split(gradeOptionsVal,'~')[0]))}">
                      <c:set var="totalPoints">${fn:substring(fn:trim(entryPointsArrVal), 0, fn:indexOf(fn:trim(fn:toLowerCase(entryPointsArrVal)), fn:toLowerCase((fn:split(gradeOptionsVal,'~')[0]))))}</c:set>                               
                      <c:set var="qualPoints" value="${fn:split(gradeOptionsVal,'~')[1]}"/>
                      <% totalPoints = totalPoints +  Integer.valueOf(pageContext.getAttribute("qualPoints").toString())*Integer.valueOf(pageContext.getAttribute("totalPoints").toString()) ;%>
                    </c:if>
                  </c:forTokens>
                  <div class="grd_minmax ${totalPoints == 0 ? '' : 'grd_drk'}" id="grd_prnt_id_${indexIdVal}_${indexInner.index}">
                    <p id="grade_name_${indexIdVal}_${indexInner.index}">${fn:split(gradeOptionsVal,'~')[0]}</p>
                    <div class="grd_val">
                      <a class="pls" href="javascript:void(0);" data-grade-option-flag="Y" data-grade-total-max-point="${maxTotalPoint}" data-grade-max-point="${maxPoint}" data-grade-index-type="-1" data-grade-value="${fn:split(gradeOptionsVal,'~')[0]}" data-grade-index="${indexInner.index}" data-load-qual-order-option="${indexIdVal}" data-grade-points="${fn:split(gradeOptionsVal,'~')[1]}"><%-- <img src="${staticDataPreLoader.getImgPath()}/minus.svg" height="12" width="12" alt="plus"> --%><i class="icon-Minus"></i></a>
                      <span id="point_${indexIdVal}_${indexInner.index}">
                        ${totalPoints}
                      </span>
                      <a class="mns" href="javascript:void(0);" data-grade-option-flag="Y" data-grade-total-max-point="${maxTotalPoint}" data-grade-max-point="${maxPoint}" data-grade-index-type="1" data-grade-value="${fn:split(gradeOptionsVal,'~')[0]}" data-grade-index="${indexInner.index}" data-load-qual-order-option="${indexIdVal}" data-grade-points="${fn:split(gradeOptionsVal,'~')[1]}"><%-- <img src="${staticDataPreLoader.getImgPath()}/plus_blue.svg" height="12" width="12" alt="minus"> --%><i class="icon-add"></i></a>
                    </div>
                  </div>    
                </c:if>
                <c:if test="${qualId eq '19'}">	             
	             <!-- ucas_points start -->	             
	             <c:set var="ucasPoints" value="${fn:split(requestScope.entryPoints,',')[indexId]}"/>
	             <% totalPoints = totalPoints + Integer.valueOf(pageContext.getAttribute("ucasPoints").toString());%>
				<div class="fltrr_dip_cnr">					
					<div class="ucas_points">
						<input type="text" maxlength="3" data-id="ucas_text_id" id="ucas_${indexIdVal}" placeholder="Enter UCAS points" value="${ucasPoints}" class="frm-ele" autocomplete="off">
					</div>					
				</div>
				<!-- ucas_points start -->
	           </c:if>
	           <c:if test="${qualId eq '20'}">
	           <c:if test="${indexInner.index eq 0}">
	           <!-- diploma start -->
					<div class="fltrr_dip_cnr">
				</c:if>	
						<!-- ${fn:split(gradeOptionsVal,'~')[0]}_credit_${filterHitCount} start -->
						<div class="fltrr_dip">
							<div class="dip_lft">
								<h3>${fn:split(gradeOptionsVal,'~')[0] eq 'D' ? 'Distinction' : fn:split(gradeOptionsVal,'~')[0] eq 'M' ? 'Merit' : 'Pass'}</h3>
							</div>
							<div class="dip_rit">
								<div class="fltr_dd">
									<ul class="drp_val" id="drp_val" data-grade-option-open="${fn:split(gradeOptionsVal,'~')[0]}_credit_${indexIdVal}" data-grade-option-point="${fn:split(gradeOptionsVal,'~')[1]}">
										<li>
										   <c:set var="entryPointsArr" value="${fn:split(requestScope.entryPoints,',')[indexId]}"/>
										   <c:set var="entryPointsArrVal" value="${fn:split(entryPointsArr,'-')[indexInner.index]}"/>
										   <c:set var="entryPointsArrValCredit" value="${fn:replace(entryPointsArrVal, fn:substring(entryPointsArrVal, 0,1), '')}"/>                                             
											<fmt:parseNumber var="entryPointsArrValCreditInt" type="number" value="${entryPointsArrValCredit}" />
											<c:set var="entryPointsArrValPointWithOutRound" value="${entryPointsArrValCreditInt * fn:split(gradeOptionsVal,'~')[1]}"/>
											<%-- <fmt:formatNumber var="entryPointsArrValCreditPtInt" value="${entryPointsArrValCreditInt * fn:split(gradeOptionsVal,'~')[1]}" type="number" pattern="#"/> --%>
											<c:set var="entryPointsArrValPoint" value='entryPointsArrValCreditPtInt'/>
											<% hePts = hePts + (Double.parseDouble(pageContext.getAttribute("entryPointsArrValPointWithOutRound").toString()));%>
											<% //totalPoints = totalPoints + (Integer.valueOf(pageContext.getAttribute("entryPointsArrValPoint").toString()));%>
											<span id="qual_prepopulate_${fn:split(gradeOptionsVal,'~')[0]}_credit_${indexIdVal}_span"> ${entryPointsArrValCredit} credits</span>
										</li>
									</ul>
									<div id="ajax_listOfOptions_grade${fn:split(gradeOptionsVal,'~')[0]}_credit_${indexIdVal}">
										
											<ul class="cstm_drp ajax_ul dsp_none" id="grade_option_${fn:split(gradeOptionsVal,'~')[0]}_credit_${indexIdVal}" style="display: none">
											 
											  <c:forEach var = "i" begin = "0" step="3" end = "${entryPointsArrValCreditInt}">
											    <li class="ajaxlist" data-credit-index="${indexIdVal}" data-grade-option-flag="D" data-load-hedip-id-value="${fn:split(gradeOptionsVal,'~')[1]}" data-load-hedip-id="${fn:split(gradeOptionsVal,'~')[0]}" data-load-credit-order-option="${fn:split(gradeOptionsVal,'~')[0]}_credit_${indexIdVal}" value='<fmt:formatNumber value="${i * fn:split(gradeOptionsVal,'~')[1]}" type="number" pattern="#"/>'>
											      <a href="javascript:void(0);">${i} credits</a>
											    </li>
											  </c:forEach>
											</ul>											
									</div>
								</div>
							</div>
						</div>
											
					<c:if test="${indexInner.last}">
					</div>
					<% totalPoints = Math.round(hePts);%>
					
					</c:if>
	           </c:if>    
                <c:if test="${gradeOptionflag ne 'Y' and (qualId ne '20' and qualId ne '19')}">
                  <c:set var="entryPointsArrVal">${fn:split(requestScope.entryPoints,',')[indexId]}</c:set> 
                  <c:if test="${indexInner.index eq 0}">
                    <div class="grd_chips mt10">
                  </c:if>
                  <c:choose>
                    <c:when test="${fn:trim(fn:toLowerCase(entryPointsArrVal)) eq fn:toLowerCase(fn:split(gradeOptionsVal,'~')[0])}">
                      <c:set var="qualPoints" value="${fn:split(gradeOptionsVal,'~')[1]}"/>
                      <% totalPoints = Integer.valueOf(pageContext.getAttribute("qualPoints").toString()); %>
                      <a href="javascript:void(0);" id="grade_name_${filterHitCount}_${indexInner.index}" class="slct_chip" data-grade-option-flag="N" data-grade-total-max-point="${maxTotalPoint}" data-grade-max-point="${maxPoint}" data-grade-index-type="C" data-grade-value="${fn:split(gradeOptionsVal,'~')[0]}" data-grade-index="${indexInner.index}" data-load-qual-order-option="${indexIdVal}" data-grade-points="${fn:split(gradeOptionsVal,'~')[1]}">${fn:split(gradeOptionsVal,'~')[0]}</a>
                    </c:when>
                    <c:otherwise>
                      <a href="javascript:void(0);" id="grade_name_${filterHitCount}_${indexInner.index}" data-grade-option-flag="N" data-grade-total-max-point="${maxTotalPoint}" data-grade-max-point="${maxPoint}" data-grade-index-type="C" data-grade-value="${fn:split(gradeOptionsVal,'~')[0]}" data-grade-index="${indexInner.index}" data-load-qual-order-option="${indexIdVal}" data-grade-points="${fn:split(gradeOptionsVal,'~')[1]}">${fn:split(gradeOptionsVal,'~')[0]}</a>
                    </c:otherwise>
                  </c:choose> 
                  <c:if test="${indexInner.last}">
                    </div> 
                  </c:if>
                </c:if>
                <c:if test="${indexInner.last}">
                  <input type="hidden" id="total_points_${indexIdVal}" value="<%=totalPoints%>"/>
                  <input type="hidden" id="grade-option-flag-${indexIdVal}" value="${gradeOptionflag eq 'Y' ? 'Y' : qualId eq '20' ? 'D' : qualId eq '19' ? 'T' : 'N'}"/>
	              <input type="hidden" id="grade_qual_${indexIdVal}" value="${qualification}"/>
	              <input type="hidden" id="grade_qual_url_${indexIdVal}" value="${qualificationUrl}"/>
	              <input type="hidden" id="heCredits_${indexIdVal}" value=""/>
	              <input type="hidden" id="qualId_${indexIdVal}" value="${qualId}"/>
                </c:if>
              </c:forTokens> 
            </div>   
            
          </div> 
          <!-- End -->
        </div>
      </c:if>
    </c:forEach>       
        
  </c:forTokens>            
        
     
          
  <c:if test="${empty addQualFlag}">
    <div class="add_qual dsp_none" id="additional_qual">
      <h3>Have more qualifications?</h3>
      <p> 
       <a href="javascript:void(0);" id="additional_qual_in">
        <span><img src='<%=CommonUtil.getImgPath("/wu-cont/images/clr_fltr_plus.svg",0)%>' width="12" height="12" alt="add icon"><%-- <i class="icon-add"></i> --%></span>
        ADD QUALIFICATION
       </a>
      </p>
    </div>
    </div>   
  </c:if>
        
  </c:when>
  
  <c:when test="${not empty requestScope.gradeFilterList and (empty qualId or addQualFlag eq 'Y')}">      
      <c:if test="${empty addQualFlag}">
        <div class="fltr_hd stc_hd" id="fltr_hd_id">
        	<h2>Calculate your UCAS points</h2>
		</div>
        <div class="custm_scrl ajax_ul">
      </c:if>
        <div class="fltr_wrp ${filterHitCount gt 0 ? 'fltr_plus' : ''}" id="additional_qual_${filterHitCount}">
            <h3>${filterHitCount eq '0' ? 'Qualification' : filterHitCount eq '1' ? 'Second qualification' : 'Third qualification'}</h3>
			
			<div class="fltr_box">
				<div class="fltr_dd">
					<ul class="drp_val" id="drp_val" data-grade-option-open="${filterHitCount}">
						<li>
							<span id="qual_prepopulate_${filterHitCount}">Please select</span>
						</li>
					</ul>
					<div id="ajax_listOfOptions_grade${filterHitCount}">
						
							<ul class="cstm_drp ajax_ul dsp_none" id="grade_option_${filterHitCount}" style="display: none">
								<c:forEach var="gradeFilterList" items="${requestScope.gradeFilterList}" varStatus="index">
				                   <c:if test="${index.index eq 1}">
				                      <c:set var="gradeOptions" value="${gradeFilterList.gradeOptions}"/>
				                      <c:set var="spanTitle" value="${gradeFilterList.qualification}"/>
				                      <c:set var="parentQualification" value="${gradeFilterList.parentQualification}"/>
				                      <c:set var="qualification" value="${gradeFilterList.qualification}"/>
				                      <c:set var="qualificationUrl" value="${gradeFilterList.qualificationUrl}"/>
				                      <c:set var="qualId" value="${gradeFilterList.qualId}"/>
				                      <c:set var="maxPoint" value="${gradeFilterList.maxPoint}"/>
					                  <c:set var="maxTotalPoint" value="${gradeFilterList.maxTotalPoint}"/>
			                  		  <c:set var="gradeOptionflag" value="${gradeOptionflag}"/>	                  
				                    </c:if>
									<c:if test="${index.index eq 0}">
									</c:if>
									<c:choose>
			                      <c:when test="${gradeFilterList.qualification eq gradeFilterList.parentQualification and empty gradeFilterList.qualId}">
			                        <c:if test="${index.index ne 0}">
			                          <li class="line_sp"></li>
									</c:if>
			                        <li class="ql_dsp">
			                          <a href="javascript:void(0);">${gradeFilterList.qualification}</a>
			                        </li>
			                      </c:when>
			                      <c:otherwise>
			                         <li class="ajaxlist">
			                         <a href="javascript:void(0);" data-filter-show="yourGrades" data-load-qual-id-value="${gradeFilterList.qualId}" data-load-qual-value="${gradeFilterList.qualification}" data-load-qual-order-option="${filterHitCount}">${gradeFilterList.qualification}</a>
			                         </li>
			                      </c:otherwise>
			                    </c:choose>								
								</c:forEach> 
							</ul>						
					</div>
				</div>
				<c:if test="${addQualFlag eq 'Y'}">
                <a href="javascript:void(0);" data-grade-remove-option="${filterHitCount}" class="lvl_cls"><img src='<%=CommonUtil.getImgPath("/wu-cont/images/clr_fltr_close_blu.svg",0)%>' alt="close icon" width="12" height="12"></a>
              </c:if>
              <div class="fltr_levls" id="grade_filter_${filterHitCount}">
                <input type="hidden" id="total_points_${filterHitCount}" value="0"/>
                <input type="hidden" id="grade-option-flag-${filterHitCount}" value="${gradeOptionflag eq 'Y' ? 'Y' : qualId eq '20' ? 'D' : qualId eq '19' ? 'T' : 'N'}"/>
	            <input type="hidden" id="grade_qual_${filterHitCount}" value="Please select"/>
	            <input type="hidden" id="grade_qual_url_${filterHitCount}" value="${qualificationUrl}"/>
	            <input type="hidden" id="heCredits" value=""/>
	            <%-- <input type='hidden' id="heCrdPoint_${filterHitCount}" value="0">
                <input type='hidden' id="heReqPoint_${filterHitCount}" value="45">			
			    <input type='hidden' id="heRemainPoint_${filterHitCount}" value="45">
			    <input type='hidden' id="dummy" value="0"> -Added for the alignment  --%> 
               </div>
             </div>
            
				
			</div>
		<%-- </div> --%>          

          <c:if test="${empty addQualFlag}">
          <div class="add_qual" id="additional_qual">
            <h3>Have more qualifications?</h3>
            <p> 
              <a href="javascript:void(0);" id="additional_qual_in">
                <span><img src='<%=CommonUtil.getImgPath("/wu-cont/images/clr_fltr_plus.svg",0)%>' width="12" height="12" alt="add icon"><%-- <i class="icon-add"></i> --%></span>
                ADD QUALIFICATION
              </a>
            </p>
          </div>
        </div>   
          </c:if>
      
  </c:when> 
   <c:when test="${not empty requestScope.gradeFilterList}">
	<c:forEach var="gradeFilterList" items="${requestScope.gradeFilterList}"> 
	 <c:forTokens var="gradeOptionsVal" delims="," items="${gradeFilterList.gradeOptions}" varStatus="index">   
    
            <c:if test="${index.first}">
                <c:set var="gradeOptions" value="${gradeFilterList.gradeOptions}"/>
                 <c:set var="spanTitle" value="${gradeFilterList.qualification}"/>
                 <c:set var="parentQualification" value="${gradeFilterList.parentQualification}"/>
                 <c:set var="qualification" value="${gradeFilterList.qualification}"/>
                 <c:set var="qualificationUrl" value="${gradeFilterList.qualificationUrl}"/>
                 <c:set var="qualId" value="${gradeFilterList.qualId}"/>
                 <c:set var="maxPoint" value="${gradeFilterList.maxPoint}"/>
              <c:set var="maxTotalPoint" value="${gradeFilterList.maxTotalPoint}"/>
              <c:set var="gradeOptionflag" value="${gradeFilterList.gradeOptionflag}"/>
            </c:if>            
            <c:choose>
	            <c:when test="${gradeOptionflag eq 'Y'}">
	              <div class="grd_minmax" id="grd_prnt_id_${filterHitCount}_${index.index}">
                    <p id="grade_name_${filterHitCount}_${index.index}">${fn:split(gradeOptionsVal,'~')[0]}</p>
                    <div class="grd_val">
                        <a class="pls" href="javascript:void(0);" data-grade-option-flag="Y" data-grade-total-max-point="${maxTotalPoint}" data-grade-max-point="${maxPoint}" data-grade-index-type="-1" data-grade-value="${fn:split(gradeOptionsVal,'~')[0]}" data-grade-index="${index.index}" data-load-qual-order-option="${filterHitCount}" data-grade-points="${fn:split(gradeOptionsVal,'~')[1]}"><i class="icon-Minus"></i></a>
                        <span id="point_${filterHitCount}_${index.index}">0</span>
                        <a class="mns" href="javascript:void(0);" data-grade-option-flag="Y" data-grade-total-max-point="${maxTotalPoint}" data-grade-max-point="${maxPoint}" data-grade-index-type="1" data-grade-value="${fn:split(gradeOptionsVal,'~')[0]}" data-grade-index="${index.index}" data-load-qual-order-option="${filterHitCount}" data-grade-points="${fn:split(gradeOptionsVal,'~')[1]}"><i class="icon-add"></i></a>
                    </div>
	             </div>
	           </c:when>
	           <c:when test="${qualId eq '19'}">
	             <!-- ucas_points start -->
				<div class="fltrr_dip_cnr">					
					<div class="ucas_points">
						<input type="text" maxlength="3" data-id="ucas_text_id" id="ucas_${filterHitCount}" placeholder="Enter UCAS points" value="" class="frm-ele" autocomplete="off">
					</div>					
				</div>
				<!-- ucas_points start -->
	           </c:when>
	           <c:when test="${qualId eq '20'}">
	           <c:if test="${index.index eq 0}">
	           <!-- diploma start -->
					<div class="fltrr_dip_cnr">
				</c:if>	
						<!-- ${fn:split(gradeOptionsVal,'~')[0]}_credit_${filterHitCount} start -->
						<div class="fltrr_dip">
							<div class="dip_lft">								
								<h3>${fn:split(gradeOptionsVal,'~')[0] eq 'D' ? 'Distinction' : fn:split(gradeOptionsVal,'~')[0] eq 'M' ? 'Merit' : 'Pass'}</h3>
							</div>
							<div class="dip_rit">
								<div class="fltr_dd">
									<ul class="drp_val" id="drp_val" data-grade-option-open="${fn:split(gradeOptionsVal,'~')[0]}_credit_${filterHitCount}" data-grade-option-point="${fn:split(gradeOptionsVal,'~')[1]}">
										<li>
											<span id="qual_prepopulate_${fn:split(gradeOptionsVal,'~')[0]}_credit_${filterHitCount}_span">0 credits</span>
										</li>
									</ul>
									<div id="ajax_listOfOptions_grade${fn:split(gradeOptionsVal,'~')[0]}_credit_${filterHitCount}">
										
											<ul class="cstm_drp ajax_ul dsp_none" id="grade_option_${fn:split(gradeOptionsVal,'~')[0]}_credit_${filterHitCount}" style="display: none;">
											  <c:forEach var = "i" begin = "0" step="3" end = "45">
											    <li class="ajaxlist" data-credit-index="${filterHitCount}" data-grade-option-flag="D" data-load-hedip-id-value="${fn:split(gradeOptionsVal,'~')[1]}" data-load-hedip-id="${fn:split(gradeOptionsVal,'~')[0]}" data-load-credit-order-option="${fn:split(gradeOptionsVal,'~')[0]}_credit_${filterHitCount}" value='<fmt:formatNumber value="${i * fn:split(gradeOptionsVal,'~')[1]}" type="number" pattern="#"/>'>
											      <a href="javascript:void(0);">${i} credits</a>
											    </li>
											  </c:forEach>
											</ul>
											
									</div>
								</div>
							</div>
						</div>
						<!-- ${fn:split(gradeOptionsVal,'~')[0]}_credit_${filterHitCount} end -->						
					<c:if test="${index.last}">
					</div>
					<!-- diploma start -->
					</c:if>
	           </c:when>    
	           <c:otherwise>
	             <c:if test="${index.index eq 0}">
	               <div class="grd_chips mt10">
	             </c:if>
	                   <a href="javascript:void(0);" id="grade_name_${filterHitCount}_${index.index}" data-grade-option-flag="N" data-grade-total-max-point="${maxTotalPoint}" data-grade-max-point="${maxPoint}" data-grade-index-type="C" data-grade-value="${fn:split(gradeOptionsVal,'~')[0]}" data-grade-index="${index.index}" data-load-qual-order-option="${filterHitCount}" data-grade-points="${fn:split(gradeOptionsVal,'~')[1]}">${fn:split(gradeOptionsVal,'~')[0]}</a>
	             <c:if test="${index.last}">	              
	               </div>
	             </c:if>
	           </c:otherwise>
         	</c:choose>
           <c:if test="${index.first}">
             <input type="hidden" id="grade-option-flag-${filterHitCount}" value="${gradeOptionflag eq 'Y' ? 'Y' : qualId eq '20' ? 'D' : qualId eq '19' ? 'T' : 'N'}"/>
	         <input type="hidden" id="grade_qual_${filterHitCount}" value="${qualification}"/>
	         <input type="hidden" id="grade_qual_url_${filterHitCount}" value="${qualificationUrl}"/>
            <input type="hidden" id="total_points_${filterHitCount}" value="0"/>
            <%-- <input type='hidden' id="heCrdPoint_${filterHitCount}" value="0">
              <input type='hidden' id="heReqPoint_${filterHitCount}" value="45">			
	          <input type='hidden' id="heRemainPoint_${filterHitCount}" value="45">
	          <input type='hidden' id="dummy" value="0"> -Added for the alignment  --%>          
           </c:if>           
      </c:forTokens>
    </c:forEach>
  </c:when>
  </c:choose>


   