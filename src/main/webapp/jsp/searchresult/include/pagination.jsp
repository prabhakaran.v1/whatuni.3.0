<div class="wuclrsr_pgnrow">
 <div class="pr_pagn">
   <jsp:include page="${paginationPageName eq 'newPagination' ? '/jsp/search/searchredesign/newPagination.jsp' : '/jsp/search/searchredesign/searchInitialPagination.jsp'}">
      <jsp:param name="pageno" value="${pageNo}"/>
      <jsp:param name="pagelimit"  value="10"/>
      <jsp:param name="searchhits" value="${recordCount}"/>
      <jsp:param name="recorddisplay" value="10"/>
      <jsp:param name="displayurl_1" value="${URL_1}"/>
      <jsp:param name="displayurl_2" value="${URL_2}"/>
      <jsp:param name="firstPageSeoUrl" value="${seoFirstPageUrl}"/>
      <jsp:param name="universityCount" value="${recordCount}"/>
      <jsp:param name="resultExists" value="${resultExists}"/>
   </jsp:include>
 </div>
</div>