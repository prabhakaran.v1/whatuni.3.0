<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="sidenav" id="sidenav">
  <a href="javascript:void(0)" class="closebtn"><img src="${closeButton}" alt="close icon" width="14" height="14"></a>
  <div class="fltr_cntr">
	<div id="courseFilter" style="display: none;">
	  <jsp:include page="/jsp/searchresult/include/courseFilter.jsp"/>
	</div>
	<div id="locationFilter" style="display: none;">
      <jsp:include page="/jsp/searchresult/include/locationFilter.jsp" />
    </div>
	<div id="universityFilter" style="display: none;">
	  <jsp:include page="/jsp/searchresult/include/universityFilter.jsp" />
	</div>
  </div>
  <div class="btm_pod" id="bottom_button" style="display: none;">
    <div class="btm_btns mt0"> 
	  <span class="clear" id="clear"><a href="javascript:void(0);"><spring:message code="clear.text"/> </a></span>
	  <span class="aply_btn" id="apply"><a href="javascript:void(0);"><spring:message code="apply.text"/> </a></span>
	</div>
  </div>
  <input type="hidden" id="filterOpenType" value="" />
  
  <input type="hidden" id="universityHidden" value="${university}" />
   <input type="hidden" id="qualificationHidden" value="${studylevel}" />
   <input type="hidden" id="subjectHidden" value="${subject}" />
   <input type="hidden" id="keywordHidden" value="${searchKeyword}" />
   <input type="hidden" id="campusTypeHidden" value="${campusType}" />
    <input type="hidden" id="employmentRateHidden" value="${empRate}" />

     <input type="hidden" id="russellHidden" value="${russellFlag}" />
     <input type="hidden" id="studyModeHidden" value="${studyMode}" />
  <input type="hidden" id="regionHidden" value="${region}" />
  <input type="hidden" id="locationTypeHidden" value="${locationType}"/>
  <input type="hidden" id="scoreHidden" value="${scoreValue}" />
  <input type="hidden" id="pageNoHidden" value="${pageNo}" />
  
   <input type="hidden" id="universityHiddenUrl" value="${university}" />
   <input type="hidden" id="qualificationHiddenUrl" value="${studylevel}" />
    <input type="hidden" id="subjectHiddenUrl" value="${subject}" />
     <input type="hidden" id="keywordHiddenUrl" value="${searchKeyword}" />
      <input type="hidden" id="campusTypeHiddenUrl" value="${campusType}" />
     <input type="hidden" id="employmentRateHiddenUrl" value="${empRate}" />
  <input type="hidden" id="russellHiddenUrl" value="${russellFlag}" />
  <input type="hidden" id="studyModeHiddenUrl" value="${studyMode}" />
  <input type="hidden" id="regionHiddenUrl" value="${region}" />
  <input type="hidden" id="locationTypeHiddenUrl" value="${locationType}"/>
  <input type="hidden" id="scoreHiddenUrl" value="${scoreValue}" />
  <input type="hidden" id="pageNoHiddenUrl" value="${pageNo}" />
</div>
	