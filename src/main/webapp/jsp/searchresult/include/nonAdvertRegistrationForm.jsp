 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
 <%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@page import= "WUI.utilities.CommonUtil" autoFlush="true" %>
 
<div class="rvbx_shdw"></div>
  <div class="revcls"><a href="javascript:void(0);"><i class="fa fa-times"></i></a></div>
  <div class="rvbx_cnt">
    <div class="rev_lst">
      <div class="rev_cen">
        <div class="rlst_row">
          <div class="revlst_rht fl">
            <div class="rlst_wrap">
              <div class="rev_bor">
                <div class="lbx_scrl">
                  <div class="rvlbx_cnt">

                    <c:if test="${not empty tabFocus}">
                      <c:set var="tabFocus_val" value="${tabFocus}"/>
                    </c:if>
                    <c:if test="${not empty lbregistration}">
                      <c:set var="lbregistration_val" value="${lbregistration}"/>
                    </c:if>
                    <c:if test="${not empty hidenewuserform}">
                       <c:set var="hidenewuserform_val" value="${hidenewuserform}"/>
                    </c:if>

                    <c:if test="${fn:containsIgnoreCase(hidenewuserform_val, 'N')}">
                      <div class="pform nlr bgw profrm2"  id="lbsighuplogin" style="${fn:containsIgnoreCase(tabFocus_val, 'globalnav') ? 'display:block;' : 'display:none;'}">
                        <div class="reg-frm">
                          <div class="sgnm pt13 remv_gplus">
                            <div class="sgn">
                              <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
                                <h1 class="txt_cnr mb22">Sign up with</h1> 
                                <fieldset class="so-btn">
                                  <a class="btn1 fbbtn" onclick="loginInfoFacebook('fblogin');" id="ab_fb_nav" title="Login with Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK<i class="fa fa-long-arrow-right"></i></a>                   
                                </fieldset>
                              </c:if>
                              <p class="qler1 valid_err" id="fblbsighuplogin_error" style="display: none;"></p> 
                            </div>
                            <div class="sgupm">
                              <p class="comptx">You can also <a title="sign up with email"  id="ab_signup_nav" class="link_blue" onclick="javascript:newSignInSignUpBox('lbregistration','');">sign up with email</a></p>
                            </div>
                            <div class="sgala">
                              <div class="borderbot mb20 mt35"></div>
                              <div class="sgalai">
                                <p class="comptx">Already have an account? <a title="Sign in" id="ab_signin_nav" class="link_blue" onclick="javascript:newSignInSignUpBox('lblogin','');">Sign in</a></p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
    
                      <div class="pform nlr" id="lbbackground"  style="${fn:containsIgnoreCase(tabFocus_val, 'splash') ? 'display:block;' : 'display:none;'}">
                        <div class="reg-frm">
                          <c:if test="${empty requestScope.nonloggedTm}">
                            <div class="reg-frmt" id="abLightBox">
                              <h1 class="txt_cnr sgntx mb20">Why should I sign up?</h1>
                              <div class="itepos">
                                <div class="icnr">
                                  <div class="pig_bs"></div>
                                  <span>
                                    It's totally <br> free to use!
                                  </span>
                                </div>
                                <div class="icnr">
                                  <div class="imac_s"></div>
                                  <span>Save course and uni comparisons</span>
                                </div>
                                <div class="icnr mr0">
                                  <div class="coat_s"></div>
                                  <span>Relevant and tailored advice</span>
                                </div>
                              </div>
                            </div>
                          </c:if>      
                          <c:if test="${not empty requestScope.nonloggedTm}">
                            <c:if test="${requestScope.nonloggedTm eq 'nonloggedTm'}"> 
                              <div class="reg-frmt nlt">
                                <div class="time_login">
                                  <h1>Get Access to Our Sixth Form Timeline</h1>
                                  <div class="time_img">
                                    <img src="${timeline_login}" alt="Timeline Article" title="Timeline Article">
                                  </div>
                                  <p class="cnt mb15">If you want exclusive access to our super-helpful timeline (which basically makes choosing a uni as simple as tying shoelaces), all you need to do is register. Go on, it only takes 20 seconds.</p>   
                                </div>
                              </div> 
                              <div class="borderbot"></div>
                            </c:if>
                          </c:if>
                          <div class="sgnm mt6 remv_gplus">
                            <div class="sgn">
                              <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
                                <h1 class="txt_cnr mb26">Sign up with</h1>
                                <fieldset class="so-btn">
                                  <a title="Login with Facebook" onclick="loginInfoFacebook('fblogin');" id="ab_fb_lb" class="btn1 fbbtn"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK<i class="fa fa-long-arrow-right"></i></a>                
                                </fieldset>
                              </c:if>
                            </div>
                            <div class="sgupm">
                              <p class="comptx">You can also <a onclick="javascript:newSignInSignUpBox('lbregistration','');" id="ab_sgnup_lb" class="link_blue" title="sign up with email">sign up with email</a></p>
                            </div>
                            <div class="sgala">
                              <div class="borderbot mb20 mt35"></div>
                              <div class="sgalai">
                                <p class="comptx">Already have an account? <a onclick="javascript:newSignInSignUpBox('lblogin','');" id="ab_sgnin_lb" class="link_blue" title="Sign in">Sign in</a></p>
                              </div>
                            </div>
                          </div>          
                        </div>
                      </div>

                      <c:choose>
                        <c:when test="${fn:containsIgnoreCase(lbregistration_val, 'lbregistration') or fn:containsIgnoreCase(tabFocus_val, 'reg')}">
                          <c:set var="cssStyle" value="display:block;"/>
                        </c:when>
                        <c:otherwise>
                          <c:set var="cssStyle" value="display:none;"/>
                        </c:otherwise>
                      </c:choose>

                      <div class="pform nlr bgw profrm2" id="lbregistration"  style="${cssStyle}">    
                        <html:form action="newuserregistration.html?method=register" commandName="registerBean">
                          <div class="reg-frm">
                            <div class="reg-frmt bgw rqinf_cnt" id="addNonAdvDiv">
                              <div id="divNonAdvBlock">
                                <div id="nonAdvLogoCont" class="lgo">                  
                                  <img id="nonAdvUniLogo" width="105" class="ibdr" src=""/>
                                </div>
                                <div class="rqinf_rht">
                                  <h2>Enter your details</h2>
                                  <p>We'll send you all the information you need to know in a handy pack.</p>
                                </div> 
                              </div>            
                              <h1 id="signupwithemail" class="mb22">Sign up with email</h1>
                              <div class="sgala">                 
                                <div class="sgalai">
                                  <p class="comptx">Already have an account? <a title="Sign in" id="regSignIn" class="link_blue" onclick="javascript:newSignInSignUpBox('lblogin','');">Sign in</a></p>
                                </div>
                             </div> 
                             <div class="sgnm pt0 remv_gplus" id="signUpWithBlock">
                               <div class="sgn" style="${fn:containsIgnoreCase(lbregistration_val, 'lbregistration') ? 'display:none' : 'display:block'}">
                                 <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
                                   <h2 class="txt_cnr mb20">Or sign up with...</h2>
                                   <fieldset class="so-btn">
                                     <a class="btn1 fbbtn" onclick="loginInfoFacebook('fblogin')" title="Login with Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK<i class="fa fa-long-arrow-right"></i></a>                    
                                   </fieldset>
                                 </c:if>
                                 <p class="qler1 valid_err" id="fblbregistration_error" style="display: none;"></p> 
                                 <div class="borderbot mb20 mt35"></div>
                               </div>
                             </div> 
                             <div class="borderbot mb20 mt35" id="divDivider"></div>
               
                             <div class="sub_cnr">
                               <div id="pros-pod">
                                 <div id="ql-fm" class="qlf-nw">
                                   <div class="clear"></div>
                                   <div id="jsFormValidationErrors" class="red"></div>
                                   <div class="crm_ql-form base">
                                     <div id="step2" class="crm_con" style="display:block;">
                                       <h3 class="fnt_lbd fnt20">Your details</h3>
                                       <fieldset class="row-fluid mt5">                         
                                         <fieldset id="firstName_fieldset" class="w50p fl fst_lg1"> 
                                           <div class="frm_lbl">
                                             <c:if test="${not empty requestScope.firstNameprep and firstNameprep ne 'undefined'}">
                                               <html:input path="firstName"  id="firstName"  autocomplete="off" cssClass="c_txt ql-inpt" maxlength="40" value="${firstNameprep}" onfocus="showTooltip('firstNameYText');" onblur="hideTooltip('firstNameYText');registratonValidations(this.id);" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/>
                                             </c:if> 
                                             <c:if test="${empty requestScope.firstNameprep}">
                                               <html:input path="firstName" id="firstName" autocomplete="off" cssClass="c_txt ql-inpt" maxlength="40" onfocus="showTooltip('firstNameYText');" onblur="hideTooltip('firstNameYText');registratonValidations(this.id);" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}" />
                                             </c:if>
                                             <label for="firstName" class="">First name*</label>
                                           </div>
                                           <span class="cmp" id="firstNameYText" style="display:none;"> 
                                             <div class="hdf5"></div>
                                             <div>We'd like this information so we don't call you by the wrong name. That'd be kinda rude.</div>
                                             <div class="linear"></div>
                                           </span>
                                           <p class="qler1" id="firstName_error" style="display:none;"></p>                              
                                         </fieldset>
                                         <fieldset id="surName_fieldset" class="w50p fr fst_lg1">                              
                                           <c:if test="${empty requestScope.surNameprep}">
                                             <html:input path="surName" id="surName" autocomplete="off" cssClass="c_txt ql-inpt" maxlength="40" onblur="registratonValidations(this.id);" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/>
                                           </c:if> 
                                           <c:if test="${not empty requestScope.surNameprep and surNameprep ne 'undefined'}">
                                             <html:input path="surName" styleId="surName" autocomplete="off" cssClass="c_txt ql-inpt" maxlength="40" onblur="registratonValidations(this.id);" value="${surNameprep}" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/>
                                           </c:if> 
                                           <label for="surName" class="">Last name*</label>
                                           <p class="qler1" id="surName_error" style="display:none;"></p>
                                         </fieldset>
                                       </fieldset>
                                       <fieldset class="row-fluid mt5 alst_pos">                         
                                         <fieldset id="emailAddress_fieldset" class="w100p fst_lg1">                              
                                           <c:if test="${empty requestScope.emailAddressprep}">
                                             <html:input path="emailAddress" id="emailAddress" autocomplete="off" maxlength="120" cssClass="c_txt ql-inpt" onfocus="showTooltip('emailAddressYText');" onblur="hideTooltip('emailAddressYText');" onkeydown="hideEmailDropdown(event, 'autoEmailId');"/>
                                           </c:if>
                                           <c:if test="${not empty requestScope.emailAddressprep and emailAddressprep ne 'undefined'}">
                                             <html:input path="emailAddress" id="emailAddress" autocomplete="off" maxlength="120" cssClass="c_txt ql-inpt" disabled="true" onfocus="showTooltip('emailAddressYText');" onblur="hideTooltip('emailAddressYText');" onkeydown="hideEmailDropdown(event, 'autoEmailId');" value="${emailAddressprep}" />
                                           </c:if>
                                           <c:if  test="${not empty requestScope.fbUserIdprep and fbUserIdprep ne 'undefined'}">
                                             <html:hidden path="fbUserId" id="fbUserId" cssClass="ql-inpt" value="${fbUserIdprep}" />
                                           </c:if> 
                                           <label class="" for="emailAddress">Email address*</label>
                                           <span class="cmp" id="emailAddressYText" style="display:none;"> 
                                             <div class="hdf5"></div>
                                             <div>Tell us your email and we'll reward you with...an email.</div>
                                             <div class="linear"></div>
                                           </span>
                                           <p class="qler1" id="registerErrMsg" style="display:none;"></p>
                                         </fieldset>
                                       </fieldset>
                                       <fieldset class="row-fluid mt5">                                                  
                                         <fieldset id="password_fieldset" class="w100p fl fst_lg1">                              
                                           <c:if test="${empty requestScope.prepopualteUserExists}">
                                             <html:password path="password" onblur="registratonValidations(this.id);" id="password" maxlength="20" cssClass="c_txt ql-inpt" cssStyle="display: none;" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/> 
                                             <label for="regtextpwd" class="">Your password*</label>
                                             <span class="pwd_eye"><a onblur="javascript:showPassword('eyeId', 'password');" href="javascript:showPassword('eyeId', 'password');"><i id="eyeId" class="fa fa-eye" aria-hidden="true"></i></a></span>
                                             <input type="text" onfocus="changePassword('regtextpwd','password','password','regtextpwd');" id="regtextpwd" class="c_txt"/> 
                                           </c:if>
                                           <c:if test="${not empty requestScope.prepopualteUserExists}">
                                             <html:password path="password" id="password" maxlength="20" cssClass="c_txt ql-inpt" style="display: none;" onblur="registratonValidations(this.id);" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/> 
                                             <label for="regtextpwd" class="">Your password*</label>
                                             <span class="pwd_eye"><a onblur="javascript:hidepassword('eyeId', 'password');" href="javascript:showPassword('eyeId', 'password');"><i id="eyeId" class="fa fa-eye" aria-hidden="true"></i></a></span>
                                             <input type="text" onfocus="changePassword('regtextpwd','password','password','regtextpwd');" id="regtextpwd" class="c_txt" style="display:none"/>       
                                             <input type="hidden" id="prepopualtepassword" value="prepopualtepassword"/>
                                           </c:if>
                                           <p class="qler1" id="password_error" style="display:none;"></p>
                                         </fieldset>                                                  
                                       </fieldset>
                        
                                       <fieldset class="row-fluid">
                                         <fieldset class="w50p fl adlog" id="postcodeField">                            
                                         <html:input path="postCode" id="postcodeIndex" autocomplete="off" onblur="enquiryValidations(this.id);" cssClass="c_txt ql-inpt adiptxt usr" maxlength="25"/>
                                         <label for="postcodeIndex" class="lbco">Postcode <span class="pst_code">(optional)</span></label>
                                         <div class="qler1" id="postcodeErrMsg" style="display: none;"></div>
                                       </fieldset>
                                       <fieldset class="w50p fr">
                                         <a onclick="showAndHideToolTip('postcodeText');" onmouseover="showTooltip('postcodeText');" onmouseout="hideTooltip('postcodeText');" class="color1 fnt_14 fl pt20 pl20 pb20 pst_ttip">Why do we need your postcode?
                                           <span id="postcodeText" class="cmp" style="display: none;">
                                             <div class="hdf5"></div>
                                             <div>We use this information to help assess the reach of our products. This is completely optional.</div>
                                             <div class="linear"></div>
                                           </span>
                                         </a>                              
                                       </fieldset>                        
                                     </fieldset>
                        
                                     <fieldset class="row-fluid mb20 mt25 strt_yr">
                                       <fieldset class="w100p">
                                         <label class="lbco mb5">WHEN WOULD YOU LIKE TO START?*</label>
                                       </fieldset>
                                       <fieldset id="yoe_fieldset" class="ql-inp">
                                         <span class="ql_rad">                              
                                           <html:radiobutton path="yeartoJoinCourse" onmouseout="hideTooltip('yeartoJoinCourse1YText');" onclick="registratonValidations(this.id);" cssClass="check" value="${YOE_0_newForm}"  id="yeartoJoinCourse1" /><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse1">${YOE_0_newForm}</label>
                                           <span class="cmp tltip pos_rht" id="yeartoJoinCourse1YText" style="display:none;"> 
                                             <div class="hdf5"></div>
                                             <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                                             <div class="linear"></div>
                                           </span>
                                         </span>
                                        <span class="ql_rad">                              
                                          <html:radiobutton path="yeartoJoinCourse" onmouseout="hideTooltip('yeartoJoinCourse2YText');" onclick="registratonValidations(this.id);" cssClass="check" value="${YOE_1_newForm}"  id="yeartoJoinCourse2" /><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse2">${YOE_1_newForm}</label>
                                          <span class="cmp tltip" id="yeartoJoinCourse2YText" style="display:none;"> 
                                            <div class="hdf5"></div>
                                            <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                                            <div class="linear"></div>
                                          </span>
                                        </span>
                                        <span class="ql_rad">                              
                                          <html:radiobutton path="yeartoJoinCourse" onmouseout="hideTooltip('yeartoJoinCourse3YText');" onclick="registratonValidations(this.id);" cssClass="check" value="${YOE_2_newForm}"  id="yeartoJoinCourse3" /><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse3">${YOE_2_newForm}</label>
                                          <span class="cmp tltip" id="yeartoJoinCourse3YText" style="display:none;"> 
                                            <div class="hdf5"></div>
                                            <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                                            <div class="linear"></div>
                                          </span>
                                        </span>
                                        <span class="ql_rad mr0">                              
                                          <html:radiobutton path="yeartoJoinCourse" onmouseout="hideTooltip('yeartoJoinCourse4YText');" onclick="registratonValidations(this.id);" cssClass="check" value="${YOE_3_newForm}"  id="yeartoJoinCourse4" /><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse4">${YOE_3_newForm}</label>
                                          <span class="cmp tltip pos_lft" id="yeartoJoinCourse4YText" style="display:none;"> 
                                            <div class="hdf5"></div>
                                            <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                                            <div class="linear"></div>
                                          </span>
                                        </span>
                                        <p class="qler1" id="yoe_error" style="display:none;"></p>
                                      </fieldset>
                                    </fieldset>
                        
                                    <div>        
                                      <h3 class="fnt_lbd fnt20 mb20 mt20">Stay up to date by email <span class="fnt_lrg gry_txt">(optional)</span></h3>
                                      <div class="btn_chk">
                                        <span class="chk_btn">
                                          <input type="checkbox" value="" class="chkbx1" id="marketingEmailRegFlag" onclick="setRegFlags(this.id);"/>
                                          <span class="chk_mark"></span>
                                        </span>
                                        <p><label for="marketingEmailRegFlag" class="fnt_lbd chkbx_100">Newsletters <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                                           Emails from us providing you the latest university news, tips and guides
                                        </p>
                                      </div>
                                      <div class="btn_chk">
                                        <span class="chk_btn">
                                          <input type="checkbox" value="" class="chkbx1" id="solusEmailRegFlag" onclick="setRegFlags(this.id);"/>
                                          <span class="chk_mark"></span>
                                        </span>
                                       <p><label for="solusEmailRegFlag" class="fnt_lbd chkbx_100"> University updates <span class="cmrk_tclr">(Tick to opt in)</span> </label> <br>
                                          <spring:message code="wuni.solus.flag.text"/>
                                       </p>
                                     </div>                        
                                     <div class="btn_chk">
                                       <span class="chk_btn">
                                         <input type="checkbox" value="" class="chkbx1" id="surveyEmailRegFlag" onclick="setRegFlags(this.id);"/>
                                         <span class="chk_mark"></span>
                                       </span>
                                       <p><label for="surveyEmailRegFlag" class="fnt_lbd chkbx_100">Surveys <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                                          <spring:message code="wuni.survey.flag.text"/>
                                       </p>
                                     </div>
                                     <div class="borderbot fl mt20 mb40"></div>
                                   </div>
                      
                                   <fieldset class="row-fluid">                         
                                     <fieldset class="mt0 rgfm-lt">                                                                                                                                            
                                       <span class="chk_btn">
                                         <input type="checkbox"  name="tac" value="on" id="termsc" class="chkbx1" >
                                         <span class="chk_mark"></span>
                                       </span>
                                       <label for="termsc" id="pdfSignup">
                                         <span class="rem1">I confirm I'm over 13 and agree to the <a title="Whatuni community" class="link_blue" href="javascript:void(0);" onclick="window.open('${TCVersion}','termspopup','width=650,height=400,scrollbars=yes,resizable=yes');">terms and conditions</a> and <a title="privacy notice" class="link_blue" href="javascript:void(0);" onclick="showPrivacyPolicyPopup()">privacy notice</a>, and to become a member of the Whatuni community.<span class="as_trk">*</span> </span>
                                       </label>
                                       <label for="termsc" id="normalsignup">
                                         <span class="rem1">I confirm I'm over 13 and agree to the <a title="Whatuni community" class="link_blue" href="javascript:void(0);" onclick="window.open('${TCVersion}','termspopup','width=650,height=400,scrollbars=yes,resizable=yes');">terms and conditions</a> and <a title="privacy notice" class="link_blue" href="javascript:void(0);" onclick="showPrivacyPolicyPopup()">privacy notice</a>, and to become a member of the Whatuni community.<span class="as_trk">*</span> </span>
                                       </label>
                                       <p class="qler1" id="termsc_error" style="display:none;"></p>
                                     </fieldset>
                                     <fieldset class="mt5" style="display: none;">
                                       <input type="checkbox" class="check" id="newsLetter" value="Y" name="newsLetter" checked="checked" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}">
                                       <span class="rem1">We may send you updates and offers from carefully selected partners, please untick this box if you would prefer not to be sent these emails.</span>
                                     </fieldset>
                                     <input type="hidden" id="emailValidateFlag" value="true"/>
                                     <a onclick="return validateUserRegister()" id="btnLogin" title="Register" class="btn1 blue mt15 w150 fr"><span id="formButtonText">SIGN UP</span>
                                        <i class="fa fa-long-arrow-right"></i>
                                     </a>                          
                                   </fieldset>
                                   <p id="loadinggifreg" style="display:none;" ><span id="loadgif"><img title="" alt="Loading" src="${indicatorgif}" class="loading"/></span></p>    
                                   <input type="hidden" id="nonmywu" value="${nonmywu}"/>
                                   <input type="hidden" id="pnotes" value=" ${pnotes}"/>
                                 </div>
                               </div>
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>
                     <input type="hidden" id="screenwidth" name="screenwidth" value=""/>
                   </html:form>
                   <c:if test="${fn:containsIgnoreCase(lbregistration_val, 'lbregistration')}">
                     <input type="hidden" id="fbuserregister" value="fbuserregister"/>   
                     <input type="hidden" id="socialRegType" value="${not empty socialRegType ? socialRegType : ''}"/>
                   </c:if>       
                 </div>
               </c:if>
        
               <div class="pform nlr bgw" id="lblogin"  style="${fn:containsIgnoreCase(hidenewuserform_val, 'Y') or fn:containsIgnoreCase(tabFocus_val, 'login') ? 'display:block;' : 'display:none;'}">
                 <html:form action="newuserregistration.html?method=login">
                   <div class="reg-frm">
                     <div class = "reg-frmt bgw">
                       <h1 class="mb22">Sign in to Whatuni</h1>
                       <fieldset>
                         <fieldset class="eml-ads">
                           <input type="text" name="loginemail" id="loginemail" onclick="if (this.value == 'Email*') { this.value = '';}" onfocus="clearDefaultLogin(this.id, 'Email*');" onkeydown="hideLoginEmailDropdown(event, 'autoEmailIdLogin');" onblur="setDefaultLogin(this.id, 'Email*');" value="Email*" autocomplete="off" />
                           <p class="qler1" id="loginemail_error" style="display:none;"></p>
                         </fieldset>
                         <fieldset class="pwd">
                           <input type="password" name="loginpass" id="loginpass" style="display: none;" maxlength="20" onfocus="clearAutoEmail();" onblur="changePassword('textpwd','loginpass','loginpass','deftextpwd');" onkeypress="javascript:if(event.keyCode==13){return validateUserLogin()}"/>
                           <input type="text" onfocus="changePassword('textpwd','loginpass','loginpass','textpwd');" id="textpwd" value="Password*"/>
                           <p class="qler1" id="loginpass_error" style="display:none;"></p>
                         </fieldset>
                       </fieldset>
                       <div id="loginerrorMsg" style="display:none;color:red;"> </div>
                       <fieldset>
                         <fieldset class="rgfm-lt comLghf">
                           <fieldset class="mt15">
                             <input type="checkbox" value="Y" id="loginRemember">
                             <label for="loginRemember">
                               <span class="rem1 comLghf">Remember me (Don't use this on a public computer)</span>
                             </label>
                           </fieldset>
                         </fieldset>
                         <fieldset class="rgfm-rt">
                           <span class="log-btnc">            
                             <a href="/degrees/forgotPassword.html" class="link_blue fnt_14">Forgot password?</a>
                           </span>
                           <span class="log-btn mt5">
                             <a onclick="return validateUserLogin()" id="btnLogin" title="Sign in" class="btn1 rgfw">SIGN IN<i class="fa fa-long-arrow-right"></i></a>
                           </span>
                           <p id="loadinggif" style="display:none;" ><span id="loadgif"><img title="" alt="Loading" src="${indicatorgif}" class="loading"/></span></p>    
                         </fieldset>
                       </fieldset>
                       <c:if test="${fn:containsIgnoreCase(hidenewuserform_val, 'N') and !fn:containsIgnoreCase(lbregistration_val, 'lbregistration')}">
                         <div class="borderbot fl mt37 mb24"></div>
                       </c:if>
                     </div>
                     <c:if test="${fn:containsIgnoreCase(hidenewuserform_val, 'N')}">
                       <div class="sgnm pt0 pt38 remv_gplus">
                         <div class="sgn" style="${fn:containsIgnoreCase(lbregistration_val, 'lbregistration') ? 'display:none;' : 'display:block;'}">
                           <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
                             <h2 class="txt_cnr mb20">Or sign in with...</h2>
                             <fieldset class="so-btn">
                               <a class="btn1 fbbtn" onclick="loginInfoFacebook('fblogin');" title="Login with Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK<i class="fa fa-long-arrow-right"></i></a>                
                             </fieldset>
                           </c:if>
                           <p class="qler1 valid_err" id="fblblogin_error" style="display: none;"></p> 
                         </div>
                         <div class="sgala">
						   <div class="borderbot mb20 mt40"></div>
                           <div class="sgalai">
							 <p class="comptx">
							   <a title="Haven't signed up yet" class="link_blue fnt_16 fnt_lrg" id="loginSignedUpYet" onclick="javascript:newSignInSignUpBox('lbregistration','');">Haven't signed up yet</a>
							 </p>
                           </div>
                         </div>
                       </div>
                     </c:if>
                   </div>
                 </html:form>
               </div>
    
               <input type="hidden" id="downloadurl" value="${downloadurl}"/> 
               <input type="hidden" id="pdfId" value="${pdfId}>"/> 
               <input type="hidden" id="submitType" value="${submitType}"/> 
               <input type="hidden" id="pdfName" value="${pdfName}"/> 
               <input type="hidden" id="referrerURL_GA" value="${referrerURL_GA}"/>
               <input type="hidden" id="regLoggingType" value="${regLoggingType}"/> 
               <input type="hidden" id="vwcid" value="${vwcid}"/> 
               <input type="hidden" id="vwurl" value="${vwurl}"/> 
               <input type="hidden" id="vwsid" value="${vwsid}"/>  
               <input type="hidden" id="vwnid" value="${vwnid}"/> 
               <input type="hidden" id="vwprice" value="${vwprice}"/> 
               <input type="hidden" id="vwcourseId" value="${vwcourseId}"/>      
               <input type="hidden" id="vwcname" value="${vwcname}"/> 
               <input type="hidden" id="vwpdfcid" value="${vwpdfcid}"/>
               <input type="hidden" id="nonAdvLogoFlag" />
	           <c:if test="${'clearingRegisterPod' eq pageNameParam}">
                 <input type="hidden" id="postClearingSrc" value="postClearing-lead-capture"/> 
               </c:if> 
               <input type="hidden" id="domainLstId" value="${emailDomainList}" />    
               <script type="text/javascript" id="domnScptId">
                 var $mdv = jQuery.noConflict();
                 $mdv(document).ready(function(e){
                   $mdv.getScript('<c:out value='${emailDomainJs}'/>', function(){        
                     var arr = ["emailAddress", "hideTooltip('emailAddressYText');", "clearErrorMsg('emailAddress_fieldset','registerErrMsg');"];
                     $mdv("#emailAddress").autoEmail(arr);        
    	           });
                   $mdv.getScript('<c:out value='${emailDomainLoginJs}'/>', function(){
                     var arr1 = ["loginemail", "clearLoginErrorMsg('loginemail_error');"];
                     $mdv("#loginemail").autoEmailLogin(arr1);        
    	           });
                });
                updateScreenWidth();
              </script>  
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<%-- <script type="text/javascript" id="domnScptId">
    var $mdv = jQuery.noConflict();
    $mdv(document).ready(function(e){
      $mdv.getScript("<%=CommonUtil.getJsPath()%>/js/emaildomain/wu_emailDomain_20200818.js", function(){        
        var arr = ["emailAddress", "hideTooltip('emailAddressYText');", "clearErrorMsg('emailAddress_fieldset','registerErrMsg');"];
        $mdv("#emailAddress").autoEmail(arr);        
    	 });
      $mdv.getScript("<%=CommonUtil.getJsPath()%>/js/emaildomain/emailDomainLogin.js", function(){
        var arr1 = ["loginemail", "clearLoginErrorMsg('loginemail_error');"];
        $mdv("#loginemail").autoEmailLogin(arr1);        
    	 });
    });
    updateScreenWidth();
  </script> --%>
