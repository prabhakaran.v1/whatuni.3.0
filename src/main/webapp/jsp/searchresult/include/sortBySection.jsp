<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="wuclrsr_srtby fl_w100">
  <div class="srt">
    <ul class="cf">
      <li class="sturat nw">
        <a class="srt1"><spring:message code="sort.by.text"/><img class="wuclrsr_imgicn" src="${domainPathImg}clr20_icn_dropdown_grey.svg" alt="Plus Icon"> </a>
        <ul id="sortBy" class="strt">
          <li class="${fn:toLowerCase(sortByValue) eq 'r' ? 'active' : ''}">

            <a rel="nofollow" data-sort-show="mostInfo" data-mostInfo-ga-value="mostinfo" data-mostInfo-value="R">
              <spring:message code="sort.by.most.info"/>
            </a>
          </li>
          <li class="${((fn:toLowerCase(sortByValue) eq 'crh') or (fn:toLowerCase(sortByValue)) eq 'crl') ? 'active' : ''}">

            <a rel="nofollow" data-sort-show="compUniGuideranking" data-compUniGuideranking-ga-value="course-ranking" data-compUniGuideranking-value="${sortByValue eq 'CRH' ? 'crl' : 'crh'}">
              <spring:message code="sort.by.comp.uni.guide.ranking"/>
            </a>
          </li>
          <c:if test="${not empty qualCode and qualCode ne 'L'}">
          <li class="${((fn:toLowerCase(sortByValue) eq 'enta') or (fn:toLowerCase(sortByValue)) eq 'entd') ? 'active' : ''}">

            <a rel="nofollow" data-sort-show="entryRequirements" data-entryRequirements-ga-value="entry-requirements" data-entryRequirements-value="${sortByValue eq 'ENTD' ? 'enta' : 'entd'}">
              <spring:message code="sort.by.entry.requirements"/>
            </a>
          </li>
          </c:if>
          <li class="${((fn:toLowerCase(sortByValue) eq 'empa') or (fn:toLowerCase(sortByValue)) eq 'empd') ? 'active' : ''}">

            <a rel="nofollow" data-sort-show="employmentRate" data-employmentRate-ga-value="employment-rate" data-employmentRate-value="${sortByValue eq 'EMPD' ? 'empa' : 'empd'}">
              <spring:message code="sort.by.employment.rate"/>
            </a>
          </li>
          <li class="${fn:toLowerCase(sortByValue) eq 'r-or' ? 'active' : ''}">

            <a rel="nofollow" data-sort-show="overallRating" data-overallRating-ga-value="student-rating" data-overallRating-value="r-or">
              <spring:message code="sort.by.student.ranking"/>
            </a>
          </li>
          <%-- <li class="${fn:toLowerCase(sortByValue) eq 'r-clr' ? 'active' : ''}">
            <a rel="nofollow" data-sort-show="courseLecturers" data-courseLecturers-value="r-clr">
              <spring:message code="sort.by.course.lecturers"/>
            </a>
          </li>
          <li class="${fn:toLowerCase(sortByValue) eq 'r-ar' ? 'active' : ''}">
            <a rel="nofollow" data-sort-show="accommodation" data-accommodation-value="r-ar">
              <spring:message code="sort.by.accommodation"/>
            </a>
          </li>
          <li class="${fn:toLowerCase(sortByValue) eq 'r-cylr' ? 'active' : ''}">
            <a rel="nofollow" data-sort-show="cityLife" data-cityLife-value="r-cylr">
              <spring:message code="sort.by.city.life"/>
            </a>
          </li>
          <li class="${fn:toLowerCase(sortByValue) eq 'r-ufr' ? 'active' : ''}">
            <a rel="nofollow" data-sort-show="uniFacilities" data-uniFacilities-value="r-ufr">
              <spring:message code="sort.by.uni.facilities"/>
            </a>
          </li>
          <li class="${fn:toLowerCase(sortByValue) eq 'r-csr' ? 'active' : ''}">
            <a rel="nofollow" data-sort-show="clubsSocieties" data-clubsSocieties-value="r-csr">
              <spring:message code="sort.by.clubs.societies"/>
            </a>
          </li>
          <li class="${fn:toLowerCase(sortByValue) eq 'r-sur' ? 'active' : ''}">
            <a rel="nofollow" data-sort-show="studentsUnion" data-studentsUnion-value="r-sur">
              <spring:message code="sort.by.students.union"/>
            </a>
          </li>
          <li class="${fn:toLowerCase(sortByValue) eq 'r-jpr' ? 'active' : ''}">
            <a rel="nofollow" data-sort-show="jobProspects" data-jobProspects-value="r-jpr">
              <spring:message code="sort.by.job.prospects"/>
            </a>
          </li> --%>
        </ul>
      </li>
    </ul>
  </div>
  <input type="hidden" id="sortHidden" value="${sortByValue}" />
</div>