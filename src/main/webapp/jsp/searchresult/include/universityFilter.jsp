<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="fltr_cntr">
			<div class="fltr_hd stc_hd"><h2><spring:message code="title.text.university"/></h2></div>
			<div class="fltr_wrp">
				<h3><spring:message code="sub.title.text.university.name"/> </h3>
				<div class="fltr_box">
					<div class="fltr_levls" id="universityDiv">
						<div class="flrt_src">
							<div class="uni_inpt">
								<input type="text" id="ajaxUniversityName" placeholder="University name" autocomplete="off" name="ajaxUniversityName">
								<a href="javascript:void(0);" id="uni_srch_icon"> <span class="srch"></span></a>
							</div>							
							<div id="ajax_listOfOptions_university"></div>
						</div>
					</div>
				</div>
			</div>
			
           <c:if test="${not empty locationTypeList}">
			 <div class="fltr_wrp">
				<h3><spring:message code="sub.title.text.campus.type"/></h3>
				<div class="fltr_box">
					<div class="fltr_levls" id="campusTypeDiv">
						<div class="grd_chips">
						  <c:forEach items="${locationTypeList}" var="campusType" varStatus="ct">
						    <c:if test="${campusType.optionType eq 'CAMPUS_BASED_UNIV'}">
						      <a href="javascript:void(0);" data-filter-show="campusType" data-campusType-value="${campusType.optionTextKey}" class="${campusType.selectedFlag eq 'Y' ? 'slct_chip' : ''}" data-campusType-ga-value="campus-type,${fn:toLowerCase(campusType.optionDesc)}">${campusType.optionDesc} universities</a>
						    </c:if>
					      </c:forEach>
						</div>
					</div>
				</div>
			</div>
		  </c:if>
			
	    <div class="fltr_wrp">
		  <h3><spring:message code="sub.title.text.employment.rate"/></h3>
		  <div class="fltr_box ">
			<div class="fltr_levls">
			  <div class="grd_chips" id="employmentRateDiv">
				<a href="javascript:void(0);" data-filter-show="employmentRate" data-employmentRate-value="50" class="${empRate eq '50' ? 'slct_chip' : ''}" data-employmentRate-ga-value="emprate-slider,50-100">50% and above</a>			
				<a href="javascript:void(0);" data-filter-show="employmentRate" data-employmentRate-value="60" class="${empRate eq '60' ? 'slct_chip' : ''}" data-employmentRate-ga-value="emprate-slider,60-100">60% and above</a>
		        <a href="javascript:void(0);" data-filter-show="employmentRate" data-employmentRate-value="70" class="${empRate eq '70' ? 'slct_chip' : ''}" data-employmentRate-ga-value="emprate-slider,70-100">70% and above</a>
			    <a href="javascript:void(0);" data-filter-show="employmentRate" data-employmentRate-value="80" class="${empRate eq '80' ? 'slct_chip' : ''}" data-employmentRate-ga-value="emprate-slider,80-100">80% and above</a>
			    <a href="javascript:void(0);" data-filter-show="employmentRate" data-employmentRate-value="90" class="${empRate eq '90' ? 'slct_chip' : ''}" data-employmentRate-ga-value="emprate-slider,90-100">90% and above</a>
			    <a href="javascript:void(0);" data-filter-show="employmentRate" data-employmentRate-value="100" class="${empRate eq '100' ? 'slct_chip' : ''}" data-employmentRate-ga-value="emprate-slider,100-100">100%</a>
			  </div>
			</div>
		  </div>
		</div>
			<div class="fltr_wrp">
				<div class="fltr_swt">
					<div class="fltr_rit">
						<h3><spring:message code="sub.title.text.russell.group"/></h3>
						<p class="subtxt"><spring:message code="russell.group.text"/></p>
					</div>
					<div class="fltr_lft swt" id="russellDiv">
						<label class="switch">
						  <input type="checkbox" id="russell" class="${selectedRussellGroup eq 'Yes' ? 'slct_chip' : ''}" data-filter-show="russell" data-russell-value="" ${selectedRussellGroup eq 'Yes' ? 'checked' : ''}>
						  <span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>