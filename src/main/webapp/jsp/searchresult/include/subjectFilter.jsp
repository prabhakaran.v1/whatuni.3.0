<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<!--subject carousel starts -->
<c:if test="${not empty subjectList}">
  <div class="cata-sub-wrap" id="subjetFilter">
    <div class="cata-sub-nav">
      <div class="nav-prev arrow" style="display: none;"><span><img src="${subArwLeft}" alt="left arrow"></span></div>
      <ul>
        <li>
          <h4>Related subjects:</h4>
        </li>
        <c:forEach var="subjectList" items="${subjectList}" varStatus="i">
          <c:set var="index" value="${i.index}"/>
          <c:if test="${subjectList.subjectTextKey eq subject}">
            <li><a class="active" id="subject_${index}">${subjectList.categoryDesc} (${subjectList.collegeCount})</a></li>
          </c:if>
          <c:if test="${subjectList.subjectTextKey ne subject}">

            <li><a id="subject_${index}" data-categoryType="filter" data-filterType="related-subjects" data-filtersApplied="${subjectList.categoryDesc.toLowerCase()}" href="${subjectList.browseMoneyPageUrl}">${subjectList.categoryDesc} (${subjectList.collegeCount})</a></li>
          </c:if>
        </c:forEach>
      </ul>
      <div class="nav-next arrow" style="display: block;"><span><img src="${subArwRight}" alt="left arrow"></span></div>
    </div>
  </div>
</c:if>
<!-- subject carousel ends -->