<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="fltr_hd stc_hd">
   <h2><spring:message code="title.text.course"/> </h2>
</div>
<div class="slim-scroll ajax_ul">
   <c:if test="${not empty qualificationList}">
      <div class="fltr_wrp ">
         <h3><spring:message code="title.text.qualification"/></h3>
         <p class="subtxt"><spring:message code="sub.title.text.choose"/></p>
         <div class="fltr_box">
            <div class="fltr_levls">
               <div class="grd_chips" id="qualificationDiv">
                  <c:forEach items="${qualificationList}" var="qualification" varStatus="ql">

                     <a href="javascript:void(0);" id="qualification" data-filter-show="qualification" data-qualification-value="${qualification.refineTextKey}" class="${qualification.selectedFlag eq 'Y' ? 'slct_chip' : ''}" data-qualification-ga-value="study-level,${fn:toLowerCase(qualification.refineDesc)}" data-qualification-subject-name-url="${qualification.browseCatTextKey}">${qualification.refineDisplayDesc}</a>
                  </c:forEach>
               </div>
            </div>
         </div>
      </div>
   </c:if>
   <c:if test="${not empty studyModeList}">
      <div class="fltr_wrp ">
         <h3><spring:message code="title.text.course.type"/> </h3>
         <p class="subtxt"><spring:message code="sub.title.text.choose"/></p>
         <div class="fltr_box">
            <div class="fltr_levls">
               <div class="grd_chips" id="studyModeDiv">
                  <c:forEach items="${studyModeList}" var="studyMode" varStatus="sm">
                     <a href="javascript:void(0);" data-filter-show="studyMode" data-studyMode-value="${studyMode.refineTextKey}" class="${studyMode.selectedFlag eq 'Y' ? 'slct_chip' : ''}" data-studyMode-ga-value="study-mode,${fn:toLowerCase(studyMode.refineDesc)}">${studyMode.refineDesc}</a>
                  </c:forEach>
               </div>
            </div>
         </div>
      </div>
   </c:if>
</div>