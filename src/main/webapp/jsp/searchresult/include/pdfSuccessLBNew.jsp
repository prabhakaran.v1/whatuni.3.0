<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="/WEB-INF/tlds/html.tld" prefix="html"%>
<%-- <%
  String flag = request.getParameter("flag");  
  String fromPage = request.getParameter("fromPage");  
  %> --%>
  <div class="rvbx_shdw"></div>
  <div class="revcls"><a href="javascript:void(0);"><i class="fa fa-times"></i></a></div>
  <div class="rvbx_cnt">
    <div class="rev_lst">
      <div class="rev_cen">
        <div class="rlst_row">
          <div class="revlst_rht fl">
            <div class="rlst_wrap">
              <div class="rev_bor">
                <!-- <div class="lbx_scrl">  -->
                  <div class="rvlbx_cnt">
<div class="comLgh">
 <%-- <div class="fcls nw">
   <a class="" onclick="closePdfSuccess('<%=fromPage%>');">
     <i class="fa fa-times"></i>
   </a>
  </div> --%>
  <div id="lblogin" class="pform nlr bgw smlbx main_success fl" style="display: block;">    
    <div class="reg-frm">
      <div class="lgn_lbx">
        <div class="success p20">
          <h6 class="fnt_lbd pb5">Huzzah!</h6>
          <p class="txt fnt_lrg m0 p0">
            <i class="icon-ok mr5"></i>
            Your PDF is downloading.
          </p>
        </div>
        <div class="dnld pt30 pb30">
          <h4 class="fnt_lrg">Having problems downloading?</h4>
          <p>Make sure your pop-up blocker is disabled</p>
        </div>
        <fieldset class="si_btn">
            <span class="log-btn mt5">
              <input type="submit" id="closePdfSuccessLB" class="ok1 m28 can" value="Close"/>
            </span>
          </fieldset>
      </div>
    </div>
  </div>
</div>

</div>
<!-- </div> -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>