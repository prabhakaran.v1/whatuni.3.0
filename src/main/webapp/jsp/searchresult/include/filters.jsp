<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="wuclrsr_rsltbtnset" id="filterOptionBlock">
 <a class="wuclr_btn btnbrdr btnyugrd ${not empty sessionScope.USER_UCAS_SCORE or not empty scoreValue ? 'btnbg':'' }" id="srgradeOption" data-filter-type="yourGrades" title="Your grades" href="javascript:void(0)">
      YOUR GRADES 
      <c:choose>
         <c:when test="${not empty sessionScope.USER_UCAS_SCORE or not empty scoreValue}">
            <img class="wuclrsr_imgicn" src="${whiteTickIcon}" alt="white tick Icon">
         </c:when>
         <c:otherwise>
            <img class="wuclrsr_imgicn wuclrsr_imgicn_hovr" src="${whitePlusIcon}" alt="whilte Plus Icon"/>
            <img class="wuclrsr_imgicn wuclrsr_imgicn_nrml" src="${bluePlusIcon}" alt="blue Plus Icon"/>
         </c:otherwise>
      </c:choose>
      <c:if test="${empty sessionScope.USER_UCAS_SCORE and empty scoreValue}">
         <span class="yugrd_tipbox" id="gradeOptionToolTip">
            <span class="yugrd_tipcollft">
               <span class="yugrd_lftarrw"></span>
               <span class="request-loader1">
                <span></span>
               </span>
            </span>
            <span class="yugrd_tipcolrgt">
               <img class="wuclrsr_imgicn" id="closeGradeOption" src="${closeGreyIcon}" alt="Close Icon">
               <h4>Add your grades</h4>
               <p>Find out your chance of being accepted onto a course</p>
            </span>
         </span>
      </c:if>
   </a>

  <c:if test="${not empty qualificationList and not empty studyModeList}">
      <a class="wuclr_btn btnbrdr btnyugrd ${not empty selectedQual or not empty selectedStudyMode ? 'btnbg': '' }" title="Course" data-filter-option="courseFilter">
         <spring:message code="title.text.course"/> 
         <c:choose>
            <c:when test="${not empty selectedQual or not empty selectedStudyMode}">
               <img class="wuclrsr_imgicn" src="${whiteTickIcon}" alt="white tick Icon">
            </c:when>
            <c:otherwise>
               <img class="wuclrsr_imgicn wuclrsr_imgicn_hovr" src="${whitePlusIcon}" alt="whilte Plus Icon"/>
               <img class="wuclrsr_imgicn wuclrsr_imgicn_nrml" src="${bluePlusIcon}" alt="blue Plus Icon"/>
            </c:otherwise>
         </c:choose>
      </a>
   </c:if>
   <c:if test="${not empty locationList and not empty locationTypeList}">
      <a class="wuclr_btn btnbrdr btnyugrd ${not empty selectedRegion or not empty selectedLocType ? 'btnbg': '' }" title="Location" data-filter-option="locationFilter">
         <spring:message code="title.text.location"/> 
         <c:choose>
            <c:when test="${not empty selectedRegion or not empty selectedLocType}">
               <img class="wuclrsr_imgicn" src="${whiteTickIcon}" alt="white tick Icon">
            </c:when>
            <c:otherwise>
               <img class="wuclrsr_imgicn wuclrsr_imgicn_hovr" src="${whitePlusIcon}" alt="whilte Plus Icon"/>
               <img class="wuclrsr_imgicn wuclrsr_imgicn_nrml" src="${bluePlusIcon}" alt="blue Plus Icon"/>
            </c:otherwise>
         </c:choose>
      </a>
   </c:if> 

   <a class="wuclr_btn btnbrdr btnyugrd ${not empty selectedRussellGroup or not empty selectedCampusType or not empty empRate ? 'btnbg': '' }" title="University" data-filter-option="universityFilter">
      <spring:message code="title.text.university"/> 
      <c:choose>
         <c:when test="${not empty selectedRussellGroup or not empty selectedCampusType or not empty empRate}">
            <img class="wuclrsr_imgicn" src="${whiteTickIcon}" alt="white tick Icon">
         </c:when>
         <c:otherwise>
            <img class="wuclrsr_imgicn wuclrsr_imgicn_hovr" src="${whitePlusIcon}" alt="whilte Plus Icon"/>
            <img class="wuclrsr_imgicn wuclrsr_imgicn_nrml" src="${bluePlusIcon}" alt="blue Plus Icon"/>
         </c:otherwise>
      </c:choose>
   </a>

   <c:if test="${not empty scoreValue or not empty selectedRussellGroup or not empty selectedStudyMode or not empty selectedRegion or not empty selectedLocType or not empty selectedCampusType or not empty empRate}">
      <a class="wuclr_lnk lnkclr" title="Clear all" data-filter-option="clearAllFilter"><img class="wuclrsr_imgicn" src="${closeGreyIcon}" alt="Close Icon"><spring:message code="clear.all.text"/></a>
   </c:if>
</div>