<%Long startJspBuild = new Long(System.currentTimeMillis());%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.SessionData" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


  <% 
    String collegeId = request.getAttribute("collegeId") != null && request.getAttribute("collegeId").toString().trim().length() > 0 ? (String) request.getAttribute("collegeId") : "";
    String collegeName = request.getAttribute("collegeName") != null && request.getAttribute("collegeName").toString().trim().length() > 0 ? (String) request.getAttribute("collegeName") : "";
    collegeName = collegeName  !=null && !collegeName.equalsIgnoreCase("null") ? collegeName:  "";    
    String collegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("collegeNameDisplay") : "";
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");    
    if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0){ 
      cpeQualificationNetworkId = "2";
    }request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);
    String subOrderItemId = request.getAttribute("subOrderItemId") != null && request.getAttribute("subOrderItemId").toString().trim().length() > 0 ? (String) request.getAttribute("subOrderItemId") : "";  
    String courseId = request.getAttribute("courseId") != null && request.getAttribute("courseId").toString().trim().length() > 0 ? (String) request.getAttribute("courseId") : "";
    String courseName = (String) request.getAttribute("courseName");
           courseName = !GenericValidator.isBlankOrNull(courseName)? courseName : "";
    String profileType = (String) request.getAttribute("profileType");
           profileType = !GenericValidator.isBlankOrNull(profileType)? profileType : "";
    String getProspectusFlag = request.getAttribute("getProspectusFlag") != null && request.getAttribute("getProspectusFlag").toString().length() > 0 ? (String)request.getAttribute("getProspectusFlag") : "N";
    String cpeValue = new WUI.utilities.CommonFunction().getGACPEPrice(subOrderItemId, "email");
    String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(collegeName);
    String token = ""+session.getAttribute("_synchronizerToken");  
    String enquiryExistFlag = !GenericValidator.isBlankOrNull((String)request.getAttribute("enquiryExistFlag")) ? (String)request.getAttribute("enquiryExistFlag") : "N";
  %>

    <%
    String emailDefaultText = "";
    if(GenericValidator.isBlankOrNull(courseId) || "0".equalsIgnoreCase(courseId)){
      emailDefaultText = "Hello, \n\nI read about " + collegeNameDisplay + " on Whatuni.com, and would like to request more information about the institution."; 
    }else{
      emailDefaultText = "Hello, \n\nI read about the " + courseName + " offered by "+collegeNameDisplay+" on Whatuni.com and would like to request more information about the course."; 
    }
    if(request.getSession().getAttribute("usermessage") != null && !"".equals((String)request.getSession().getAttribute("usermessage"))){
      emailDefaultText = (String)request.getSession().getAttribute("usermessage");
    } 
    String typeofEnquiryText = "";
    String buttonValue = "";
    %>



<html:form action="/degrees/send-college-email.html" id="oneqlform" commandName="qlformbean">  
<tags:token/>
<div class="rvbx_shdw"></div>
  <div class="revcls"><a href="javascript:void(0);"><i class="fa fa-times"></i></a></div>
  <div class="rvbx_cnt">
    <div class="rev_lst">
      <div class="rev_cen">
        <div class="rlst_row">
          <div class="revlst_rht fl">
            <div class="rlst_wrap">
              <div class="rev_bor">
                <div class="lbx_scrl"> 
                  <div class="rvlbx_cnt">
<div id="fpcs" class="onc">
  <div class="finc onc-cont">	
    <!-- <div class="fcls nw">
      <a onclick="hm();" class="cls"></a>
    </div> -->		    
    <div class="line">
      <h1 class="htxt">We are sending the following info to:</h1>
      <h1 class="htxt1"><%=collegeNameDisplay%></h1>
    </div>
    <c:if test="${not empty qlformbean.firstName}">
      <div class="txtc">
        <span class="wtu">First name:</span> <span class="wkbs"> ${qlformbean.firstName}</span>
      </div>
    </c:if>
    <c:if test="${not empty qlformbean.lastName}">
      <div class="txtc">
        <span class="wtu">Last name:</span> <span class="wkbs"> ${qlformbean.lastName} </span>
      </div>
    </c:if>
    <c:if test="${not empty qlformbean.address1}">
      <div class="txtc">
        <span class="wtu">Address:</span> <span class="wkbs"> 
          ${qlformbean.address1}
          <c:if test="${not empty qlformbean.address2}">, ${qlformbean.address2}</c:if>
          <c:if test="${not empty qlformbean.town}">, ${qlformbean.town}</c:if>
          <c:if test="${not empty qlformbean.postCode}">, ${qlformbean.postCode}</c:if>
        </span>
      </div>
    </c:if>
    <c:if test="${not empty qlformbean.nationalityDesc}">
      <div class="txtc">
        <span class="wtu">Nationality:</span> <span class="wkbs"> ${qlformbean.nationalityDesc}</span>
      </div>
    </c:if>
    <c:if test="${not empty qlformbean.countryOfResDesc}">
      <div class="txtc">
        <span class="wtu">Country of residence:</span> <span class="wkbs"> ${qlformbean.countryOfResDesc}</span>
      </div>
    </c:if>
    <c:if test="${not empty qlformbean.dateOfBirth}">
      <div class="txtc" style="display: none;">
        <span class="wtu">Date of birth:</span> <span class="wkbs"> ${qlformbean.dateOfBirth}</span>
      </div>
    </c:if>
    <c:if test="${not empty qlformbean.predictedGrades}">
      <div class="txtc">
        <span class="wtu">Predicted grades:</span> <span class="wkbs"> ${qlformbean.predictedGrades}</span>
      </div>
    </c:if>
    <div class="txtc">
      <span class="wtu"><a id="changemyinfo" onclick="changeMyInfo('${qlformbean.typeOfEnquiry}');">Change my info</a></span>
    </div>
    <c:if test="${qlformbean.typeOfEnquiry eq 'send-college-email'}">
      <%typeofEnquiryText = "Don't show this again, I'm happy for this data to be sent each time I make an enquiry"; buttonValue = "One-click enquiry";%>
    </c:if>
    <c:if test="${qlformbean.typeOfEnquiry eq 'order-prospectus'}">
      <%typeofEnquiryText = "Don't show this again, I'm happy for this data to be sent each time I order a prospectus"; buttonValue = "One-click order";%>
    </c:if>
    <c:if test="${qlformbean.typeOfEnquiry eq 'download-prospectus'}">
      <%typeofEnquiryText = "Don't show this again, I'm happy for this data to be sent each time I download a prospectus"; buttonValue = "One-click download";%>
    </c:if>

    <div class="ochk">
      <html:checkbox path="oneClickCheckBox" id="oneClickCheckBox" value="Y"/><label><%=typeofEnquiryText%></label>
      <c:if test="${qlformbean.newsLetter eq 'Y'}">
        <div style="display:none;"><html:checkbox path="newsLetter" id="newsLetter" value="Y" /><label>We'd like to send you information about universities and courses you might be interested in. Please check this box to if you would like to register with the Hotcourses Ltd mailing list. (We know your info's important to you, so we never pass it to a third party).</label></div>
      </c:if>
      <c:if test="${qlformbean.newsLetter ne 'Y'}">
        <div style="display:none;"><html:checkbox path="newsLetter" id="newsLetter" value="Y"/><label>We'd like to send you information about universities and courses you might be interested in, so we'd like to register you on the WhatUni mailing list. Please untick this box if you would prefer not to be sent emails by Hotcourses Ltd, the publisher of WhatUni. (We know your info's important to you, so we never pass it to a third party).</label></div>
      </c:if>
    </div>
    <div class="onc-btn"><span class="log-btn"><input type="button" name="oneclickButton" value="<%=buttonValue%>" id="btnLogin" title="" onclick="oneclickBoxSubmit('${qlformbean.typeOfEnquiry}', '<%=gaCollegeName%>', <%=cpeValue%>);"></span></div>
  
  </div>
</div>

</div>
<!-- </div> -->
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<input type="hidden" name="originalURL" id="or_url"/>
<input type="hidden" name="consentFlag" id="consentFlag" value="${consentFlagOneClick}"/>
<c:if test="${consentFlagOneClick eq 'Y'}">
  <input type="hidden" name="consentFlagCheck" id="consentFlagCheck" value="YES"/>
</c:if>
<html:hidden id="cpeQualificationNetworkId" path="cpeQualificationNetworkId" value="<%=cpeQualificationNetworkId%>" />
<html:hidden path="collegeId" id="collegeId"/>  
<html:hidden path="courseId" id="courseId"/>
<html:hidden path="subOrderItemId" id="subOrderItemId"/>
<html:hidden path="qlFlag" styleId="qlFlag"/>
<html:hidden path="subject" styleId="subject"/>
<html:hidden path="typeOfEnquiry" id="typeOfEnquiry"/>      
<html:hidden path="requestUrl"/>
<html:hidden path="refferalUrl"/>
<html:hidden path="postButtonFlag"/>
<html:hidden path="studyLevelId" id="studyLevelId"/>
<html:hidden path="postEnquiry" id="PostEnqiryFlag"/>
<input type="hidden" name="checboxflag" id="checboxflag"/>
<input type="hidden" name="newsflag" id="newsflag"/>
<input type="hidden" name="umessage" id="onemessage"/>
<input type="hidden" name="oneClick" value="Proceed" />
<input type="hidden" name="onclickLoop" value="Y" />
<input type="hidden" name="_synchronizerToken" value="<%=token%>" id="tokenId"/>
<input type="hidden" id="OlEnquiryExistFlag" value="<%=enquiryExistFlag%>"/>
<c:if test="${not empty requestScope.profileType}">
<html:hidden path="profileType" id="profileType"/>
</c:if>
<input type='hidden' id="omnitureCollegeId" value="<%=collegeId%>" />
<input type="hidden" name="hidden_user_id" id="hidden_user_id" value='<%=new SessionData().getData(request,"y")%>'/>
<input type="hidden" name="robots" value="nofollow" />
<%-- wu582_20181023 - Sabapathi: added screenwidth for stats logging  --%>
<input type="hidden" id="screenwidth" name="screenwidth" value=""/>
<c:if test="${qlformbean.typeOfEnquiry ne 'send-college-email'}">
  <html:hidden path="message" value="<%=emailDefaultText%>"/> 
</c:if>
</html:form>   
<script type="text/javascript" id="updateScreenWidth">
  // wu582_20181023 - Sabapathi: update screenwidth in hidden
  updateScreenWidth();
</script>
