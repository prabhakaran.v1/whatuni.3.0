<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.*" autoFlush="true" %>
<%--
  * @purpose  This jsp is add webclicks to visit website lightbox
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 11-AUG-2015 Priyaa parthasarathy         544     new file
  * *************************************************************************************************************************
  * 29-MAR-2016 Prabhakaran V.                551    Added college name param for add basket js method..                   
--%>
<%String headingText = "uni" ;
  String gaCollegeName = ""; //College name for insight log
  %>
<c:if test="${not empty requestScope.basketList}">
<div class="rvbx_shdw"></div>
  <div class="revcls"><a href="javascript:void(0);"><i class="fa fa-times"></i></a></div>
  <div class="rvbx_cnt">
    <div class="rev_lst">
      <div class="rev_cen">
        <div class="rlst_row">
          <div class="revlst_rht fl">
            <div class="rlst_wrap">
              <div class="rev_bor">
                <!-- <div class="lbx_scrl"> -->
                  <div class="rvlbx_cnt">
    
    <div id="lblogin" class="pform nlr bgw opday fl" style="display: block;">
      <div class="reg-frm">
        <div class="lgn_lbx">
         <c:forEach var="basketList" items="${requestScope.basketList}">
           <c:if test="${not empty basketList.collegeName}">
             <c:set var="collegeNameDef" value="${basketList.collegeName}"/>
             <%gaCollegeName = new CommonFunction().replaceSpecialCharacter(pageContext.getAttribute("collegeNameDef").toString());%>
           </c:if>
           <c:if test="${basketList.associationType eq 'O'}">
             <% headingText = "course";%>
          </c:if>
          <div class="lhdr"><h2>We've added this <%=headingText%> to your shortlist </h2></div>
          <div class="fl cmp_div">
             <div class="cmp_cr">
                <div class="cmp_rw">
                  <div class="cmp_lgo_lft">
                    <c:if test="${not empty basketList.collegeLogo_1}">
                      <img src="<%=CommonUtil.getImgPath("", 0)%>${basketList.collegeLogo_1}">
                    </c:if>
                  </div>                
                  <div class="cmp_rt">
                    <div class="ver_cmp">
                     <c:if test="${basketList.shortListFlag eq 'TRUE'}">
                     <c:if test="${basketList.associationType eq 'C'}">
                          <div id="lb_basket_div_${basketList.collegeId}" class="cmlst act" style="display: block;">
                            <div class="compare">
                              <a onclick='addBasket("${basketList.collegeId}", "C", "lb_basket_div_${basketList.collegeId}","basket_div_${basketList.collegeId}", "basket_pop_div_${basketList.collegeId}","","fromlighbox", "<%=gaCollegeName%>");'>
                                <span class="icon_cmp f5_hrt"></span>
                                <span class="cmp_txt">Compare</span>
                                <span style="display: none; position: absolute;" id="load_${basketList.collegeId}" class="loading_icon"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif", 0)%>"></span>
                                <span class="chk_cmp"><span class="chktxt">Add to comparison<em></em></span></span>
                              </a>
                            </div>
                          </div>
                          <div style="display: none;" class="cmlst" id="lb_basket_pop_div_${basketList.collegeId}">
                            <div class="compare">
                              <a onclick='addBasket("${basketList.collegeId}", "C", "lb_basket_pop_div_${basketList.collegeId}","basket_div_${basketList.collegeId}", "basket_pop_div_${basketList.collegeId}","","fromlighbox", "<%=gaCollegeName%>");'>
                                <span class="icon_cmp f5_hrt"></span>
                                <span class="cmp_txt">Compare</span>
                                <span class="loading_icon" id="load_${basketList.collegeId}" style="display: none; position: absolute;">
                                <img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif", 0)%>"></span>
                                <span class="chk_cmp"><span class="chktxt">Add to comparison<em></em></span></span>
                              </a>
                            </div>
                          </div>
                        </c:if>
                        <c:if test="${basketList.associationType eq 'O'}">
                          <div id="lb_basket_div_${basketList.courseid}" class="cmlst act" style="display: block;">
                            <div class="compare">
                              <a onclick='addBasket("${basketList.courseid}", "O", "lb_basket_div_${basketList.courseid}","basket_div_${basketList.courseid}", "basket_pop_div_${basketList.courseid}","","fromlighbox", "<%=gaCollegeName%>");'>
                                <span class="icon_cmp f5_hrt"></span>
                                <span class="cmp_txt">Compare</span>
                                <span style="display: none; position: absolute;" id="load_${basketList.courseid}" class="loading_icon"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif", 0)%>"></span>
                                <span class="chk_cmp"><span class="chktxt">Add to comparison<em></em></span></span>
                              </a>
                            </div>
                          </div>
                          <div style="display: none;" class="cmlst" id="lb_basket_pop_div_${basketList.courseid}">
                            <div class="compare">
                              <a onclick='addBasket("${basketList.courseid}", "O", "lb_basket_pop_div_${basketList.courseid}","basket_div_${basketList.courseid}", "basket_pop_div_${basketList.courseid}","","fromlighbox", "<%=gaCollegeName%>");'>
                                <span class="icon_cmp f5_hrt"></span>
                                <span class="cmp_txt">Compare</span>
                                <span class="loading_icon" id="load_${basketList.courseid}" style="display: none; position: absolute;">
                                <img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif", 0)%>"></span>
                                <span class="chk_cmp"><span class="chktxt">Add to comparison<em></em></span></span>
                              </a>
                            </div>
                          </div>
                        </c:if>
                    </c:if>
                  </div>
                </div>
                  <div class="cmp_lt">
                    <span  id="compStatustxtColourLb" class="grntxt"><span class="fa fa-check" id="compStatusimageLb"></span><span id="compStatusTextLb">ADDED</span></span>
                    <p class="c_uni">${basketList.coursetitle}</p>
                    <p class="c_nm">${basketList.collegeNameDisplay}</p>
                  </div>                
              </div>           
            </div>
            <div class="vw_cmp_txt">
              <div class="grn_btn">
                <c:if test="${not empty sessionScope.userInfoList}">
                  <a title="VIEW COMPARISON" class="btn1"  href="/degrees/comparison">VIEW COMPARISON<i class="fa fa-long-arrow-right"></i></a>
                </c:if>
                <c:if test="${empty sessionScope.userInfoList}">
                  <a title="VIEW COMPARISON" class="btn1" onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'Webclick-comparison');">VIEW COMPARISON<i class="fa fa-long-arrow-right"></i></a>
                </c:if>
              </div>
              <div class="grn_btn_lnk"><a title="Don't show this again" onclick="javascript:dontShowCompLB();">Don't show this again</a></div>
              </div>
            </div>       
           </c:forEach>
        </div>
      </div>
    </div>
  
 <!-- </div> -->
 </div>
 </div>
 </div>
 </div>
 </div>
 </div>
 </div>
 </div>
</c:if>
