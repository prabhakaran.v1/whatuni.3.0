<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="pagename3">
   <tiles:getAsString name="pagename3" ignore="true"/>
</c:set>
<!-- Whatuni sr page head starts -->
<div class="wuclrsr_rslthead">
   <div class="ad_cnr">
      <div class="content-bg" id="content-blk">
         <div class="sr">
            <c:if test="${not mobileFlag}">
               <jsp:include page="/seopods/breadCrumbs.jsp" >
                  <jsp:param name="pageName" value="BROWSE_MONEY_PAGE" />
               </jsp:include>
            </c:if>

            <div class="com" id="headerSectionDiv">
               <div class="com-lt">
                  <div>
                     <h1 class="result-head reshd2">
                        <c:if test="${not empty subjectNameDisplay}">${subjectNameDisplay}</c:if>
                        ${studyLevelDescription}
                        <c:if test="${not empty selectedRegion and selectedRegion ne 'United kingdom'}"> in ${selectedRegion}</c:if>
                     </h1>
                     <h2 class="result-head respar2">
                        <span class="reshd_txt">
                           <c:if test="${not empty recordCount}">${recordCount}</c:if>
                           ${recordCount eq 1 ? 'university' : 'universities'} offer
                           <c:if test="${not empty totalCourseCount}"> ${totalCourseCount}</c:if>
                           ${studyLevelDesc eq 'degree' ? '' : studyLevelDesc eq 'hnd/hnc' ? fn:toUpperCase(studyLevelDesc) : studyLevelDesc}
                           ${totalCourseCount eq 1 ? 'course' : 'courses'} including 
                           <c:if test="${not empty subjectNameDisplay}">${subjectNameDisplay}</c:if>
                        </span>
                        <span class="nwsrch">

                           <a href="javascript:void(0)" onclick="showComSearchPopup()" id="topNavAnchorTagId" data-topNav-ga-value="${fn:toLowerCase(pagename3)},${fn:toLowerCase(keywordOrSubject)}">
                              <span class="nsrch_icn"><i class="fa fa-search" aria-hidden="true"></i></span>
                              <spring:message code="new.search.text"/>
                           </a>
                        </span>
                     </h2>
                     <c:if test="${studyLevelDescription eq 'foundation degrees'} ">
                        <p class="cal_txt fdtxt_wrp"><span class="fdeg_txt">Sorry if there aren't many results, we are waiting for data from our partner UCAS.</span> </p>
                     </c:if>
                  </div>
               </div>
            </div>
            <div id="articleTab" class="sr-art" style="display:none;">
               <div class="sart-lt">
               </div>
               <div class="sart-rt">
               </div>
            </div>
            <!-- search results filters pod -->
            <jsp:include page="/jsp/searchresult/include/filters.jsp" />
            <!--End of search result filters section -->
         </div>
      </div>
   </div>
</div>
<!-- Whatuni sr page head ends -->