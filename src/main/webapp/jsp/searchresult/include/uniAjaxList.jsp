<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:if test="${not empty uniAjaxList}">
		<ul id="ajaxUniDropDown" class="cstm_drp ajax_ul" style="overflow: hidden; width: auto; display: none;">
			<c:forEach var="uniAjaxList" items="${uniAjaxList}">
				<li id="uniDropDown_${uniAjaxList.collegeId}" data-filter-show="university" data-university-value="${fn:toLowerCase(uniAjaxList.collegeTextKey)}" 
				    data-university-ga-value="${fn:toLowerCase(uniAjaxList.collegeNameDisplay)}">
				  <a onclick="javascript:void(0);"> ${uniAjaxList.collegeNameDisplay} </a>
				</li>
			</c:forEach>
		</ul>
</c:if>