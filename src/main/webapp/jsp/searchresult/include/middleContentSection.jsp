<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="/jsp/search/include/searchGoogleAdSlots.jsp"/>

<c:if test="${not empty listOfSearchResults}">
   <c:forEach var="searchResultsList" items="${listOfSearchResults}" varStatus="i">
      <div class="wuclrsr_colrow">
         <div class="wuclrsr_uniinf">
            <div class="wuclrsr_unittldet">
               <div class="wuclrsr_unittl_lft">
                  <c:if test="${not empty searchResultsList.sponsoredListFlag and searchResultsList.sponsoredListFlag eq 'SPONSORED_LISTING'}">
                     <span class="wuclrsr_spnstip">
                     <span class="tooltip_cnr ttip_cnt">
                     <span class="fad" id='${searchResultsList.sponsoredListFlag}'>Sponsored</span>
                     </span>                 
                     </span>
                  </c:if>
                  <c:if test="${not empty searchResultsList.uniHomeUrl}">
                     <h3>
                        <a data-uni-link 
                           data-collegeId="${searchResultsList.collegeId}" 
                           data-index="${i.index}"  
                           href="${searchResultsList.uniHomeUrl}">
                        ${searchResultsList.collegeNameDisplay}
                        </a>
                     </h3>
                  </c:if>
                  <p>
                     <a data-pr-link 
                        data-collegeId="${searchResultsList.collegeId}" 
                        data-index="${i.index}" 
                        data-productClick-page="sr_to_pr"  
                        href="${searchResultsList.collegePRUrl}" class="wuclrsr_crscnt">
                     ${searchResultsList.numberOfCoursesForThisCollege} ${subjectNameDisplay} degree${(searchResultsList.numberOfCoursesForThisCollege gt 1) ? 's' : ''}
                     </a>
                  </p>
               </div>
               <div class="wuclrsr_unittl_rgt">
                  <div class="wuclrsr_lgo">
                     <img src="${domainPath}${searchResultsList.collegeLogoPath}" width="64" title="${searchResultsList.collegeNameDisplay}" class="ibdr" alt="${searchResultsList.collegeNameDisplay}">                         
                     <!-- <c:choose>
                        <c:when test="${(i.index) >= 1}">
                          <img src="${img_px_gif}" data-src="${domainPath}${searchResultsList.collegeLogoPath}" width="64" title="${searchResultsList.collegeNameDisplay}" class="ibdr" alt="${searchResultsList.collegeNameDisplay}">
                        </c:when>
                        <c:otherwise>
                          <img src="${domainPath}${searchResultsList.collegeLogoPath}" width="64" title="${searchResultsList.collegeNameDisplay}" class="ibdr" alt="${searchResultsList.collegeNameDisplay}">                         
                        </c:otherwise>
                        </c:choose> -->
                  </div>
               </div>
            </div>
            <div class="wuclrsr_unirvwdet">
               <c:if test="${not empty searchResultsList.overallRating and searchResultsList.overallRating ge 1}">
                  <ul class="strat cf">
                     <li>
                        <span class="fl mr10">OVERALL RATING </span>
                        <span class="rat${searchResultsList.overallRating} t_tip">
                           <span class="cmp">
                              <div class="hdf5">
                                 <spring:message code="wuni.tooltip.overall.keystats" />
                              </div>
                              <div class="line"></div>
                           </span>
                        </span>
                        <span class="rtx">(${searchResultsList.exactRating})</span>
                        <c:if test="${not empty searchResultsList.reviewCount and searchResultsList.reviewCount ge 1}">
                           <a class="link_blue" href="${searchResultsList.uniReviewsURL}">
                           ${searchResultsList.reviewCount} review${(searchResultsList.reviewCount gt 1) ? 's' : ''}
                           </a>
                        </c:if>
                     </li>
                  </ul>
               </c:if>
            </div>
            <!-- Course List -->
            <c:if test="${not empty searchResultsList.bestMatchCoursesList}">
               <c:forEach var="courseSpecificList" items="${searchResultsList.bestMatchCoursesList}" varStatus="j">
                  <ul class="empcom_cnt">
                     <c:if test="${not empty courseSpecificList.employmentRate}">
                        <li>
                           <div class="com_guid">
                              <div class="emp_rate">
                                 Employment rate: ${courseSpecificList.employmentRate}% 
                                 <span class="tooltip_cnr">
                                    <span class="tool_tip fl">
                                       <i class="fa fa-question-circle fnt_24">
                                          <span class="cmp">
                                             <div class="hdf5"></div>
                                             <div>
                                                <spring:message code="wuni.tooltip.kis.entry.text" />
                                             </div>
                                             <div class="line"></div>
                                          </span>
                                       </i>
                                    </span>
                                 </span>
                              </div>
                           </div>
                        </li>
                     </c:if>
                     <c:if test="${not empty courseSpecificList.courseRank}">
                        
                        <li>
                           <div class="com_guid">
                              <div class="emp_rate">
                                 CompUniGuide: ${courseSpecificList.courseRank}
                                 <span class="tooltip_cnr">
                                    <span class="tool_tip fl">
                                       <i class="fa fa-question-circle fnt_24">
                                          <span class="cmp">
                                             <div class="hdf5"></div>
                                             <div>
                                                <spring:message code="wuni.tooltip.cug.ranking.text" />
                                             </div>
                                             <div class="line"></div>
                                          </span>
                                       </i>
                                    </span>
                                 </span>
                              </div>
                           </div>
                        </li>
                     </c:if>
                  </ul>
                  <div class="wuclrsr_crslstrow">
                     <c:set var="courseId" value="${courseSpecificList.courseId}"/>
                     <h4>
                        <a data-cd-link 
                           data-collegeId="${searchResultsList.collegeId}" 
                           data-index="${i.index}" 
                           data-productClick-page="sr_to_cd"  
                           href="${courseSpecificList.courseDetailsPageURL}">
                        ${courseSpecificList.courseTitle}
                        </a>
                     </h4>
                     <!-- Comparison Section -->
                     <div class="hor_cmp">
                        <c:if test="${courseSpecificList.wasThisCourseShortlisted eq 'TRUE'}">
                           <div class="cmlst" id="basket_div_${courseId}" style="display:none;">
                              <div class="compare">
                                 <a data-addcompare="add-comparison"
                                    data-courseId="${courseId}"

                                    data-collegeName="${searchResultsList.collegeNameSpecialCharRemove}"
                                    data-courseTitle="${courseSpecificList.courseTitleSpecialCharRemove}">                             
                                    <span class="icon_cmp f5_hrt"></span> 
                                    <span  id="load_${courseId}" style="display:none;"></span>
                                    <span class="chk_cmp">
                                       <span class="chktxt">
                                          <spring:message code="add.to.comparison"/>
                                          <em></em>
                                       </span>
                                    </span>
                                 </a>
                              </div>
                           </div>
                           <div class="cmlst act" id="basket_pop_div_${courseId}">
                              <div class="compare">
                                 <a class="act"
                                    data-removecompare="remove-comparison"
                                    data-courseId="${courseId}"

                                    data-collegeName="${searchResultsList.collegeNameSpecialCharRemove}"
                                    data-courseTitle="${courseSpecificList.courseTitleSpecialCharRemove}">
                                    <span class="icon_cmp f5_hrt hrt_act"></span> 
                                    <span class="loading_icon"></span> 
                                    <span class="chk_cmp">
                                       <span class="chktxt">
                                          <spring:message code="remove.from.comparison"/>
                                          <em></em>
                                       </span>
                                    </span>
                                 </a>
                              </div>
                              <div class="sta" id="defaultpop_${courseId}" style="display: block;">
                                 <c:choose>
                                    <c:when test="${not empty userId and userId ne 0}">
                                       <a class="view_more" href="${contextPath}/comparison">
                                          <spring:message code="view.comparison"/>
                                       </a>
                                    </c:when>
                                    <c:otherwise>
                                       <a data-viewcompare="view-comparison" class="view_more"><spring:message code="view.comparison"/></a>
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <div class="sta" id="pop_${courseId}" style="display:none;"></div>
                           </div>
                        </c:if>
                        <c:if test="${courseSpecificList.wasThisCourseShortlisted eq 'FALSE'}">
                           <div class="cmlst" id="basket_div_${courseId}">
                              <div class="compare">
                                 <a data-addcompare="add-comparison"
                                    data-courseId="${courseId}"

                                    data-collegeName="${searchResultsList.collegeNameSpecialCharRemove}"
                                    data-courseTitle="${courseSpecificList.courseTitleSpecialCharRemove}">
                                    <span class="icon_cmp f5_hrt"></span> 
                                    <span class="loading_icon" id="load_${courseId}" style="display:none;"></span>
                                    <span class="chk_cmp">
                                       <span class="chktxt">
                                          <spring:message code="add.to.comparison"/>
                                          <em></em>
                                       </span>
                                    </span>
                                 </a>
                              </div>
                           </div>
                           <div class="cmlst act" id="basket_pop_div_${courseId}" style="display:none;">
                              <div class="compare">
                                 <a data-removecompare="remove-comparison"
                                    data-courseId="${courseId}"

                                    data-collegeName="${searchResultsList.collegeNameSpecialCharRemove}"
                                    data-courseTitle="${courseSpecificList.courseTitleSpecialCharRemove}"
                                    class="act">
                                    <span class="icon_cmp f5_hrt hrt_act"></span> 
                                    <span class="loading_icon"></span> 
                                    <span class="chk_cmp">
                                       <span class="chktxt">
                                          <spring:message code="remove.from.comparison"/>
                                          <em></em>
                                       </span>
                                    </span>
                                 </a>
                              </div>
                              <div class="sta" id="defaultpop_${courseId}" style="display: block;">
                                 <c:choose>
                                    <c:when test="${not empty userId and userId ne 0}">
                                       <a class="view_more" href="${contextPath}/comparison">
                                          <spring:message code="view.comparison"/>
                                       </a>
                                    </c:when>
                                    <c:otherwise>
                                       <a data-viewcompare="view-comparison"
                                          class="view_more"><spring:message code="view.comparison"/></a>
                                    </c:otherwise>
                                 </c:choose>
                              </div>
                              <div class="sta" id="pop_${courseId}"></div>
                           </div>
                        </c:if>
                     </div>
                     <!-- Comparison Section -->
                     <div class="wuclrsr_crslstmta">
                        <div class="wuclrsr_crsmtboxrow">
                           <ul class="wuclrsr_ucsinfcol">
                              <c:if test="${not empty courseSpecificList.courseUcasTariff}">
                                 <li class="wuclrsr_ucspnt">
                                    <span>
                                       <spring:message code="ucas.points"/>
                                    </span>
                                    <span>${courseSpecificList.courseUcasTariff}</span>
                                 </li>
                              </c:if>
                              <c:if test="${not empty courseSpecificList.ucasCode}">
                                 <li class="wuclrsr_ucscde">
                                    <span>
                                       <spring:message code="ucas.code"/>
                                    </span>
                                    <span>${courseSpecificList.ucasCode}</span>
                                 </li>
                              </c:if>
                           </ul>
                           <ul class="wuclrsr_crsboxbtnset">
                              <!-- No Advertiser Request Info -->
                              <c:if test="${not empty courseSpecificList.subOrderItemId}">
                                 <c:if test="${courseSpecificList.subOrderItemId eq 0}">
                                    <li class="wuclrsr_vstbtn req_info">
                                       <a rel="nofollow"

                                          data-nonadv-link data-nonadvpdfdownload="${searchResultsList.collegeNameSpecialCharRemove}#${courseSpecificList.webformPrice}#${searchResultsList.collegeId}#${courseSpecificList.subOrderItemId}#${courseSpecificList.cpeQualificationNetworkId}#${courseSpecificList.subOrderEmailWebform}#${courseSpecificList.courseId}#${domainPath}${searchResultsList.collegeLogoPath}#COURSE#${searchResultsList.providerId}"
                                          target="_blank" 
                                          title="Email ${searchResultsList.collegeNameDisplay}">
                                          <spring:message code="request.info.label" />
                                       </a>
                                    </li>
                                 </c:if>
                              </c:if>
                              <c:if test="${empty courseSpecificList.subOrderItemId}">
                                 <li class="wuclrsr_vstbtn req_info">
                                    <a rel="nofollow"

                                       data-nonadv-link data-nonadvpdfdownload="${searchResultsList.collegeNameSpecialCharRemove}#${courseSpecificList.webformPrice}#${searchResultsList.collegeId}#${courseSpecificList.subOrderItemId}#${courseSpecificList.cpeQualificationNetworkId}#${courseSpecificList.subOrderEmailWebform}#${courseSpecificList.courseId}#${domainPath}${searchResultsList.collegeLogoPath}#COURSE#${searchResultsList.providerId}"
                                       target="_blank" 
                                       title="Email ${searchResultsList.collegeNameDisplay}">
                                       <spring:message code="request.info.label" />
                                    </a>
                                 </li>
                              </c:if>
                              <!-- End of No Advertiser Request Info -->
                              <!-- Advertiser CTA starts -->
                              <c:if test="${not empty courseSpecificList.subOrderItemId and courseSpecificList.subOrderItemId ne 0}">
                                 <!-- Visit Website CTA starts --> 
                                 <c:if test="${not empty courseSpecificList.subOrderWebsite}">
                                    <li class="wuclrsr_vstbtn vis_web">
                                       <a target="_blank"
                                          data-visitweb-link="visit_web"
                                          data-collegeId="${searchResultsList.collegeId}" 
                                          data-index="${i.index}" 
                                          data-buttonType="visitwebsite"
                                          data-eventCategory="interaction"
                                          data-eventAction="Webclick"

                                          data-collegeName="${searchResultsList.collegeNameSpecialCharRemove}"   
                                          data-courseId="${courseId}"
                                          data-subOrderItemId="${courseSpecificList.subOrderItemId}"
                                          data-networkId ="${courseSpecificList.cpeQualificationNetworkId}"
                                          data-suborderWebsite="${courseSpecificList.subOrderWebsite}"
                                          data-sponsoredListFlag="${searchResultsList.sponsoredListFlag}"
                                          id="${searchResultsList.collegeId}"        
                                          href="${courseSpecificList.subOrderWebsite}&courseid=${courseId}"
                                          title="${searchResultsList.collegeNameDisplay} website">
                                          <spring:message code="visit.website.label" />
                                       </a>
                                    </li>
                                 </c:if>
                                 <!-- Visit Website CTA ends -->
                                 <!-- Get Prospectus CTA starts -->
                                 <c:choose>
                                    <c:when test="${not empty courseSpecificList.subOrderProspectusWebform}">
                                       <li class="wuclrsr_vstbtn get_pros">
                                          <a rel="nofollow"
                                             target="_blank"
                                             data-prospectuswebform-link="get_prospectus_webform"
                                             data-collegeId="${searchResultsList.collegeId}"
                                             data-index="${i.index}" 
                                             data-buttonType="prospectuswebform"
                                             data-eventCategory="interaction"
                                             data-eventAction="prospectus webform"
                                             data-collegeName="${searchResultsList.collegeNameSpecialCharRemove}"
                                             data-webPrice="${courseSpecificList.webformPrice}" 
                                             data-subOrderItemId="${courseSpecificList.subOrderItemId}"
                                             data-networkId ="${courseSpecificList.cpeQualificationNetworkId}"
                                             data-subOrderProspectusWebform="${courseSpecificList.subOrderProspectusWebform}"
                                             data-sponsoredListFlag="${searchResultsList.sponsoredListFlag}"
                                             href="${courseSpecificList.subOrderProspectusWebform}" 
                                             title="Get ${searchResultsList.collegeNameDisplay} Prospectus">
                                             <spring:message code="get.prospectus.label" />
                                          </a>
                                       </li>
                                    </c:when>
                                    <c:when test="${not empty courseSpecificList.subOrderProspectus and empty courseSpecificList.subOrderProspectusWebform}">
                                       <c:set var="sponsoredOrderItem" value="${searchResultsList.sponsoredListFlag  eq 'SPONSORED_LISTING' ? sponsoredOrderItemId : 0}"></c:set>
                                       <li class="wuclrsr_vstbtn get_pros">
                                          <a data-prospectsform-link="get_prospectus_form"
                                             data-collegeId="${searchResultsList.collegeId}"
                                             data-index="${i.index}" 
                                             data-buttonType="prospectusbutton"
                                             data-eventCategory="engagement"
                                             data-eventAction="Prospectus-Request"
                                             data-collegeName="${searchResultsList.collegeNameSpecialCharRemove}"
                                             data-courseId="${courseId}"

                                             data-subjectOrKeyword="${keywordOrSubject}"
                                             data-subOrderItemId="${courseSpecificList.subOrderItemId}"
                                             data-sponsoredOrderItem="${sponsoredOrderItem}"
                                             title="Get ${searchResultsList.collegeNameDisplay} Prospectus">
                                             <spring:message code="get.prospectus.label" />
                                          </a>
                                       </li>
                                    </c:when>
                                 </c:choose>
                                 <!-- Get Prospectus CTA ends -->
                                 <!-- Request info CTA starts -->
                                 <c:choose>
                                    <c:when test="${not empty courseSpecificList.subOrderEmailWebform}">
                                       <li class="wuclrsr_vstbtn req_info">
                                          <a rel="nofollow" 
                                             target="_blank"
                                             data-requestinfowebform-link="request_info_webform"
                                             data-collegeId="${searchResultsList.collegeId}"
                                             data-index="${i.index}" 
                                             data-buttonType="emailwebform"
                                             data-eventCategory="interaction"
                                             data-eventAction="email webform"
                                             data-collegeName="${searchResultsList.collegeNameSpecialCharRemove}"
                                             data-webPrice="${courseSpecificList.webformPrice}"
                                             data-subOrderItemId="${courseSpecificList.subOrderItemId}"
                                             data-networkId ="${courseSpecificList.cpeQualificationNetworkId}"
                                             data-subOrderEmailWebForm="${courseSpecificList.subOrderEmailWebform}"
                                             data-sponsoredListFlag="${searchResultsList.sponsoredListFlag}"
                                             href="${courseSpecificList.subOrderEmailWebform}"
                                             title="Email ${searchResultsList.collegeNameDisplay}">
                                             <spring:message code="request.info.label" />
                                          </a>
                                       </li>
                                    </c:when>
                                    <c:when test="${not empty courseSpecificList.subOrderEmail and empty courseSpecificList.subOrderEmailWebform}">
                                       <c:set var="sponsoredOrderItem" value="${searchResultsList.sponsoredListFlag  eq 'SPONSORED_LISTING' ? sponsoredOrderItemId : 0}"/>
                                       <li class="wuclrsr_vstbtn req_info">
                                          <a rel="nofollow"
                                             data-requestinfoform-link="request_info_form"
                                             data-collegeId="${searchResultsList.collegeId}"
                                             data-index="${i.index}" 
                                             data-buttonType="emailbutton"
                                             data-eventCategory="engagement"
                                             data-eventAction="Email-Request"
                                             data-collegeName="${searchResultsList.collegeNameSpecialCharRemove}"
                                             href="<SEO:SEOURL pageTitle="sendcollegemail">
                                             ${searchResultsList.collegeName}#${searchResultsList.collegeId}#${courseSpecificList.courseId}#${keywordOrSubject}#n#${courseSpecificList.subOrderItemId}</SEO:SEOURL>?sponsoredOrderItemId=${sponsoredOrderItem}"         
                                             title="Email ${searchResultsList.collegeNameDisplay}">
                                             <spring:message code="request.info.label" />
                                          </a>
                                       </li>
                                    </c:when>
                                 </c:choose>
                                 <!-- Request info CTA ends -->
                                 <!-- Open day CTA starts -->
                                 <c:if test="${not empty searchResultsList.opendaySuborderItemId }">
                                    <c:if test="${not empty searchResultsList.totalOpendays and searchResultsList.totalOpendays gt 0}">
                                       <c:choose>
                                          <c:when test="${searchResultsList.totalOpendays eq 1 }">
                                             <li class="wuclrsr_vstbtn book_eve">
                                                <a rel="nofollow"
                                                   data-opendaywebform-link="open_day_webform"
                                                   data-collegeId="${searchResultsList.collegeId}"
                                                   data-index="${i.index}" 
                                                   data-buttonType="visitwebsite"
                                                   data-eventCategory="interaction"
                                                   data-eventAction="Reserve A Place"
                                                   data-collegeName="${searchResultsList.collegeNameSpecialCharRemove}"
                                                   data-opendayDate="${searchResultsList.openDate}"
                                                   data-opendayMonthYear="${searchResultsList.openMonthYear}"
                                                   data-subOrderItemId="${searchResultsList.opendaySuborderItemId}"
                                                   data-networkId="${searchResultsList.networkId}"
                                                   data-opendayBookingUrl="${searchResultsList.bookingUrl}"
                                                   data-sponsoredListFlag="${searchResultsList.sponsoredListFlag}"
                                                   data-eventCatoryNameGA="${searchResultsList.eventCategoryNameGa}"
                                                   href="javascript:void(0);"
                                                   title="<spring:message code="book.open.day.button.text"/>
                                                   ">
                                                   <spring:message code="book.open.day.button.text"/>
                                                </a>
                                             </li>
                                          </c:when>
                                          <c:when test="${searchResultsList.totalOpendays ne 1}">
                                             <li class="wuclrsr_vstbtn book_eve">
                                                <a rel="nofollow"
                                                   href="${searchResultsList.opendayUrl}"
                                                   title="<spring:message code="book.open.day.button.text"/>
                                                   ">
                                                   <spring:message code="book.open.day.button.text"/>
                                                </a>
                                             </li>
                                          </c:when>
                                       </c:choose>
                                    </c:if>
                                 </c:if>
                                 <!-- Open day CTA ends -->
                              </c:if>
                              <!-- Advertiser CTA Ends -->
                           </ul>
                        </div>
                     </div>
                  </div>
                  <c:if test="${searchResultsList.sponsoredListFlag  eq 'SPONSORED_LISTING'}">
                     <input type="hidden" id="collegeIdSponsored" value="${searchResultsList.collegeId}"/>
                     <input type="hidden" id="collegeNameSponsored" value="${searchResultsList.collegeName}"/> 
                     <input type="hidden" id="orderItemIdSponsored" value="${sponsoredOrderItemId}"/>         
                  </c:if>
               </c:forEach>
            </c:if>
            <!-- Course List -->
            <c:if test="${not empty searchResultsList.numberOfCoursesForThisCollege and searchResultsList.numberOfCoursesForThisCollege gt 1}">
               <div class="wuclrsr_vwallrellnk">
                  <a rel="nofollow" 
                     data-viewallpr-link 
                     data-collegeId="${searchResultsList.collegeId}" 
                     data-index="${i.index}" 
                     data-productClick-page="sr_to_pr_courses_available"
                     href="${searchResultsList.collegePRUrl}">
                     <spring:message code="view.all.courses" arguments="${searchResultsList.numberOfCoursesForThisCollege -1};${searchResultsList.numberOfCoursesForThisCollege gt 2 ? 's' : ''}" htmlEscape="false" argumentSeparator=";"/>
                     <img src="${domainPathImg}covid_blu_arw.svg">
                  </a>
               </div>
            </c:if>
         </div>
      </div>
       <c:set var="position" value="${i.index}"/>
	     <jsp:include page="/jsp/search/include/searchDrawAdSlots.jsp">
	     <jsp:param name="fromPage" value="searchresults"/>
	     <jsp:param name="position" value="${position}"/>
	     <jsp:param name="totalResults" value="${recordCount}"/>
	   </jsp:include>
   </c:forEach>
</c:if>