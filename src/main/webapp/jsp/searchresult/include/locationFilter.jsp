<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="fltr_hd stc_hd">
   <h2><spring:message code="title.text.location"/></h2>
</div>
<div class="slim-scroll ajax_ul">
   <c:if test="${not empty locationList}">
      <div class="fltr_wrp">
         <h3><spring:message code="sub.title.text.study.region"/> </h3>
         <p class="subtxt"><spring:message code="sub.title.text.choose"/></p>
         <div class="fltr_box ">
            <div class="fltr_levls">
               <div class="grd_chips" id="regionDiv">
                  <c:forEach items="${locationList}" var="locationList" varStatus="rl">

                     <a href="javascript:void(0);" data-filter-show="region" data-region-value="${locationList.regionTextKey}" class="${locationList.selectedFlag eq 'Y' ? 'slct_chip' : ''}" data-region-ga-value="location,${fn:toLowerCase(locationList.regionName)}">${locationList.regionName}</a>
                  </c:forEach>
               </div>
            </div>
         </div>
      </div>
   </c:if>
   <c:if test="${not empty locationTypeList}">
      <div class="fltr_wrp">
         <h3><spring:message code="sub.title.text.location.type"/> </h3>
         <p class="subtxt"><spring:message code="sub.title.text.multi.choose"/> </p>
         <div class="fltr_box">
            <div class="fltr_levls">
               <div class="grd_chips" id="locationTypeDiv">
                  <c:forEach items="${locationTypeList}" var="locationType" varStatus="lt">

                    <c:if test="${locationType.optionType eq 'UNIV_LOC_TYPE'}">
                      <a href="javascript:void(0);" data-filter-show="locationType" data-locationType-value="${locationType.optionTextKey}" class="${locationType.selectedFlag eq 'Y' ? 'slct_chip' : ''}" data-locationType-ga-value="location-type,${fn:toLowerCase(locationType.optionTextKey)}">${locationType.optionDesc}</a>				
                    </c:if>
                  </c:forEach>
               </div>
            </div>
         </div>
      </div>
   </c:if>
</div>