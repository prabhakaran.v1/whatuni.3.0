<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib  uri="http://www.springframework.org/tags" prefix="spring"%>

<%-- GTM script included under body tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
	<jsp:param name="PLACE_TO_INCLUDE" value="BODY"/>
</jsp:include>
<%-- GTM script included under body tag --%>

<!--Start of filter pop up -->
<div class="clr_fltr" id="clr_fltr" style="display: none;">
   <div class="overlay"></div>
   <jsp:include page="/jsp/searchresult/include/filterPopup.jsp" />
</div>
<!--End of filter pop up -->
<div class="sr_resp srredes">
   <!-- Header starts here -->
   <header class="md clipart">
      <div class="large"></div>
      <div class="ad_cnr">
         <div class="content-bg">
            <div id="desktop_hdr">
               <jsp:include page="/jsp/common/header.jsp" />
            </div>
         </div>
      </div>
   </header>
   <!-- Header ends here -->
   <jsp:include page="/jsp/searchresult/include/headerSection.jsp"/>
   <!-- Whatuni sr page center starts -->
   <div class="ad_cnr wuclrsr_cntrblk">
      <div class="content-bg" id="content-blk">
         <div class="sr">
            <jsp:include page="/jsp/searchresult/include/subjectFilter.jsp"/>
            <c:if test="${not empty featuredBrandList}">
              <div class="wuclrsr_featrbox srres_cnt fl_w100">
               <jsp:include page="/jsp/search/include/featuredBrandSales.jsp" />
              </div>
            </c:if>
            <jsp:include page="/jsp/searchresult/include/sortBySection.jsp"/>
            <jsp:include page="/jsp/searchresult/include/middleContentSection.jsp"/>
            <jsp:include page="/jsp/searchresult/include/pagination.jsp"/>
            <jsp:include page="/jsp/searchresult/include/contentSnippetSection.jsp"/>
         </div>
      </div>
   </div>
   <!-- Whatuni sr page ends -->      
   <!-- Footer starts here -->
   <jsp:include page="/jsp/common/footer.jsp" />
      <!-- SR chatbot starts here -->
   <c:if test="${not mobileFlag}">
     <jsp:include page="/jsp/common/socialBoxPod.jsp" />
   </c:if>
    <!-- SR chatbot ends here -->
   <jsp:include page="/jsp/common/eCommerceTracking.jsp" />
   <!-- Footer ends here -->
   
   <input type="hidden" id="exitOpenDayResFlg" value="${exitOpenDayRes}"/>
   <input type="hidden" id="odEvntAction" value="${odEvntAction}"/>
   <input type="hidden" id="paramSubjectCode" value="${paramSubjectCode}"/>
   <input type="hidden" id="paramStudyLevelId" value="${paramStudyLevelId}"/>  
   <input type="hidden" id="paramlocationValue" value="${paramlocationValue}"/>
   <input type="hidden" id="featuredSubject" value="${subject}"/>
   <input type="hidden" id="subjectname" value="${subject}"/>
   <input type="hidden" id="subject" value="${subject}"/> 
   <input type="hidden" id="studyleveldesc" value="${studyLevelDescription}"/>
   <input type="hidden" id="pageName" value="searchResults"/>
   <input type="hidden" id="subjectL1" value="${subjectL1}"/>
   <input type="hidden" id="subjectL2" value="${subjectL2}"/>
   <input type="hidden" id="eCommPageName" value="Search Results Page"/>
   <input type="hidden" id="GTMLDCS" value="${cDimLDCS}"/>
   <input type="hidden" id="instIds" name="instIds" value="${instIds}"/>
   <input type="hidden" id="instNames" name="instNames" value="${instNames}"/>
   <input type="hidden" id="location" name="location" value="${location}"/>
   <input type="hidden" id="qualificationInSR" name="qualification1" value="${qualification1}"/>
   <input type="hidden" id="gtmJsonDataSR" name="gtmJsonDataSR"/>
   <input type="hidden" id= selected_qual name="selected_qual" value="${selected_qual}"/>
   <input type="hidden" id="pageNum" value="${pageNo}" />
   <input type="hidden" id="filterCSS" value="/cssstyles/<spring:message code='wuni.whatuni.grade.filter.css'/>" />
   <input type="hidden" id="slimScrollJs" value="/js/jquery/<spring:message code='wuni.slimscroll.js'/>" />
   
</div>
