<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0">
<meta http-equiv="content-language" content=" en-gb "/>
<jsp:include page="/jsp/common/iconImage.jsp"/>

<link rel="canonical" href="${canonicalUrl }"/> 

<jsp:include page="/jsp/common/abTracking.jsp"/>
<jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>

<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
   <jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>

<%-- Facebook pixel tracking script included in head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/facebookPixelTracking.jsp"/>

<jsp:include page="/jsp/common/responsive.jsp">
<%--    <jsp:param name="flag" value="spreDesign"/>
 --%>
</jsp:include>

<jsp:include page="/jsp/common/includeBasket.jsp" />

<jsp:include page="/jsp/common/includeSchemaTag.jsp">
   <jsp:param name="pageName" value="BREADCRUMB_SCHEMA"/>
</jsp:include>
<jsp:include page="/jsp/common/includeSchemaTag.jsp">
   <jsp:param name="pageName" value="WEB_PAGE_SCHEMA"/>
</jsp:include>
