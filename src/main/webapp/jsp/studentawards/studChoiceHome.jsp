 <%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction" %> 
  <body id="www-whatuni-com">
  <header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
</header> 
    <div class="clipart">
      <div id="content-bg">
        <div id="wrapper"> 
          <div  class="sca">
            <h2 class="hs-1 loc-ins mb-20"><strong>UK University Rankings : Whatuni Student Choice Awards</strong></h2>
            <div class="scat video">
              <span>            
                <img width="150" src="<%=CommonUtil.getImgPath("/wu-cont/images/",0)%>awards/2015/awd_stuchoice1.png" alt="Whatuni Student Choice Awards" title="Whatuni Student Choice Awards">
              </span>
              <p>                                
                <div id="courseSummary" class="desc fl mr20 no_vid">
                  <p><strong>Steady on there!</strong></p>                            
                  <p>The results of the 2015 Whatuni Student Choice Awards haven't been published yet, but fear not - if you check back at 07:00am on the 24th April 2015, you'll find exactly what you need...</p>
                </div>                                                                             
              </p>
              <%--
                <logic:present name="mediaPath" scope="request">
                  <div class="fr vid_tn" style="cursor: pointer;" onclick="javascript:dynamicVideoPlayerJsLoad('<bean:write name="mediaPath" scope="request" filter="false"/>','<%=CommonUtil.getImgPath("/wu-cont/images/awards.jpg", 0)%>');">                    
                    <a class="icon_play"><i class="fa fa-youtube-play fa-3_5x "></i></a>                    
                  </div>
                </logic:present>          
              --%>
            </div>
            <div class="dialog" id="box">
              <div align="center" id="flashbanner"></div>
              <div id="linksdiv" class="mt30" style="display:none;">
                <span class="cros"><strong><a href="javascript:closeHomeVideo()">Close</a></strong></span>
              </div>
            </div>                                       
          </div>
        </div>
      </div>
    </div>
    <jsp:include page="/jsp/common/wuFooter.jsp" />    
  </body>

