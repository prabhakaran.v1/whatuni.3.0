<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page
	import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator"%>

<%
	String rnkEqual = "fa fa-minus";
	String rnkLesser = "fa icon-caret-down";
	String rnkGreater = "fa icon-caret-up";
	String parentDivId = "studChoiceRes";
	String totalCount = (String) request
			.getAttribute("studChoiceResCount");
	String curPageNo = request.getAttribute("pageno") != null
			? (String) request.getAttribute("pageno")
			: "1";
	int totalPages = 0;
	int results = 0;
	if (request.getAttribute("ajaxResAppend") != null
			&& !"".equals(request.getAttribute("ajaxResAppend"))) {
		parentDivId = "";
	}
	//Added by Indumathi.S for Mar-08-16 REL
	String previousYear = request.getAttribute("previousYear") != null
			? (String) request.getAttribute("previousYear")
			: "2014";
	String loadYear = request.getAttribute("loadYear") != null
			? (String) request.getAttribute("loadYear")
			: "";
	String awardsReview = "";
	if ("2014".equals(loadYear)) {
		awardsReview = "<p>The " + loadYear
				+ " results are based on over 18,000+ reviews.</p>";
	} else if ("2015".equals(loadYear)) {
		awardsReview = "<p>The "
				+ loadYear
				+ " results are based on over 20,000 ratings and reviews written between 10th April"
				+ previousYear + " and 6th March " + loadYear + ".</p>";
	}
	String curYear = new CommonFunction()
			.getWUSysVarValue("CURRENT_YEAR_AWARDS");
	String awardsURL = "/student-awards-winners/" + loadYear + ".html";
	if (curYear.equals(loadYear)) {
		awardsURL = "/student-awards-winners/university-of-the-year/";
	}
	String categorName = request.getAttribute("noResultCatName") != null
			? (String) request.getAttribute("noResultCatName")
			: "University of the year";
   //Added by Sangeeth.S for March 12 release to display the interaction buttons on hover in desktop
   String c9DivStyle = "";
   if(!(request.getHeader("User-Agent").indexOf("Mobile") != -1)) {
    c9DivStyle = "visibility:hidden";
   }
   String categoryName = !GenericValidator.isBlankOrNull((String)request.getAttribute("categorName")) ? (String)request.getAttribute("categorName") : "";
%>
<c:if test="${not empty requestScope.studChoiceAwardsList}">
	<div class="list_view" id="<%=parentDivId%>">
		<c:if test="${empty requestScope.ajaxResAppend}">
			<div class="row_tit">
				<div class="c5">Rank</div>
				<div class="c6">
					<%
						if (!"".equals(previousYear)) {
					%>
					<%=previousYear%>
					rank
					<%
						} else {
					%>
					Last Year
					<%
						}
					%>
				</div>
				<div class="c7">Institution</div>
				<%
					if (!"2012".equals(loadYear)) {
				%>
				<div class="c8 course_deatils">
					<span class="scr_ttip">Score</span>
					<%
						if ("2017".equals(loadYear)) {
					%>
					<span class="tool_tip right"> <i
						class="fa fa-question-circle fnt_24"> <span
							class="cmp fnt_lbd">
								<div class="fnt_lbd fnt_14 lh_24">Scores are an aggregate
									of student rankings taken from reviews submitted between 11
									March 2016 and 28 Feb 2017. We only display two decimal places
									for the purpose of saving space on the page, however for
									calculating the rankings we go down to five decimal places.</div>
								<div class="line"></div>
						</span>
					</i>
					</span>

					<%
						} else if ("2018".equals(loadYear)) {
					%>
					<span class="tool_tip right"> <i
						class="fa fa-question-circle fnt_24"> <span
							class="cmp fnt_lbd">
								<div class="fnt_lbd fnt_14 lh_24">Scores are an aggregate
									of student rankings taken from reviews submitted between 3
									March 2017 and 28 Feb 2018. We only display two decimal places
									in order to save space on the page, however for the purposes of
									calculating the rankings we go down to five decimal places.</div>
									<div class="line"></div>
                  </span>
                </i>
              </span>
            <%}else if("2019".equals(loadYear)){
              String dateRange = "1 March 2018 and 28 Feb 2019";
               if("Independent Higher Education".equalsIgnoreCase(categoryName) || "Further Education College".equalsIgnoreCase(categoryName)){
                 dateRange = "14 March 2018 and 28 Feb 2019";
               }
              %>
               <span class="tool_tip right">
                <i class="fa fa-question-circle fnt_24">
                  <span class="cmp fnt_lbd">
                    <div class="fnt_lbd fnt_14 lh_24">Scores are an aggregate of student rankings taken from reviews submitted between <%=dateRange%>. We only display two decimal places in order to save space on the page, however for the purposes of calculating the rankings we go down to five decimal places.</div>
								<div class="line"></div>
						</span>
					</i>
					</span>
					<%
						}else if("2020".equals(loadYear)){
              String dateRange = "1 March 2019 and 29 Feb 2020";
              
             %>
              <span class="tool_tip right">
               <i class="fa fa-question-circle fnt_24">
                 <span class="cmp fnt_lbd">
                   <div class="fnt_lbd fnt_14 lh_24">Scores are an aggregate of student rankings taken from reviews submitted between <%=dateRange%>. We only display two decimal places in order to save space on the page, however for the purposes of calculating the rankings we go down to five decimal places.</div>
								<div class="line"></div>
						</span>
					</i>
					</span>
					<%
						}
					%>
				</div>
				<%
					}
				%>
			</div>
		</c:if>
		<c:if test="${not empty requestScope.stdchAjaxSrch}">
			<div class="row_tit">
				<div class="c5">Rank</div>
				<div class="c6">
					<%
						if (!"".equals(previousYear)) {
					%>
					<%=previousYear%>
					rank
					<%
						} else {
					%>
					Last Year
					<%
						}
					%>
				</div>
				<div class="c7">Institution</div>
				<%
					if (!"2012".equals(loadYear)) {
				%>
				<div class="c8 course_deatils">
					<span class="scr_ttip">Score</span>
					<%
						if ("2017".equals(loadYear)) {
					%>
					<span class="tool_tip right"> <i
						class="fa fa-question-circle fnt_24"> <span
							class="cmp fnt_lbd">
								<div class="fnt_lbd fnt_14 lh_24">Scores are an aggregate
									of student rankings taken from reviews submitted between 11
									March 2016 and 28 Feb 2017. We only display two decimal places
									for the purpose of saving space on the page, however for
									calculating the rankings we go down to five decimal places.</div>
								<div class="line"></div>
						</span>
					</i>
					</span>

					<%
						} else if ("2018".equals(loadYear)) {
					%>
					<span class="tool_tip right"> <i
						class="fa fa-question-circle fnt_24"> <span
							class="cmp fnt_lbd">
								<div class="fnt_lbd fnt_14 lh_24">Scores are an aggregate
									of student rankings taken from reviews submitted between 3
									March 2017 and 28 Feb 2018. We only display two decimal places
									in order to save space on the page, however for the purposes of
									calculating the rankings we go down to five decimal places.</div>
									<div class="line"></div>
                  </span>
                </i>
              </span>            
            <%}else if("2019".equals(loadYear)){
               String dateRange = "1 March 2018 and 28 Feb 2019";
               if("Independent Higher Education".equalsIgnoreCase(categoryName) || "Further Education College".equalsIgnoreCase(categoryName)){
                 dateRange = "14 March 2018 and 28 Feb 2019";
               }
               %>               
               <span class="tool_tip right">
                <i class="fa fa-question-circle fnt_24">
                  <span class="cmp fnt_lbd">
                    <div class="fnt_lbd fnt_14 lh_24">Scores are an aggregate of student rankings taken from reviews submitted between <%=dateRange%>. We only display two decimal places in order to save space on the page, however for the purposes of calculating the rankings we go down to five decimal places.</div>
									 <div class="line"></div>
                  </span>
                </i>
              </span>
            <%}else if("2019".equals(loadYear)){
              String dateRange = "1 March 2018 and 28 Feb 2019";
               if("Independent Higher Education".equalsIgnoreCase(categoryName) || "Further Education College".equalsIgnoreCase(categoryName)){
                 dateRange = "14 March 2018 and 28 Feb 2019";
               }
              %>
               <span class="tool_tip right">
                <i class="fa fa-question-circle fnt_24">
                  <span class="cmp fnt_lbd">
                    <div class="fnt_lbd fnt_14 lh_24">Scores are an aggregate of student rankings taken from reviews submitted between <%=dateRange%>. We only display two decimal places in order to save space on the page, however for the purposes of calculating the rankings we go down to five decimal places.</div>
								<div class="line"></div>
						</span>
					</i>
					</span>
					<%
						}else if("2020".equals(loadYear)){
              String dateRange = "1 March 2019 and 29 Feb 2020";
              
             %>
              <span class="tool_tip right">
               <i class="fa fa-question-circle fnt_24">
                 <span class="cmp fnt_lbd">
                   <div class="fnt_lbd fnt_14 lh_24">Scores are an aggregate of student rankings taken from reviews submitted between <%=dateRange%>. We only display two decimal places in order to save space on the page, however for the purposes of calculating the rankings we go down to five decimal places.</div>
								<div class="line"></div>
						</span>
					</i>
					</span>
					<%
						}
					%>
				</div>
				<%
					}
				%>
				<c:if test="${not empty requestScope.removeFilter}">
					<a class="awd_reset" href="<%=awardsURL%>"><i
						class="fa fa-times"></i></a>
				</c:if>
			</div>
		</c:if>
		<div class="rep_list intr_btn_style">
			<c:forEach var="studAwardsRes"
				items="${requestScope.studChoiceAwardsList}" varStatus="index">
				<c:set var="indexIntValue" value="${index.index}" />
				<%
					if (Integer.parseInt(pageContext.getAttribute(
									"indexIntValue").toString()) == 0) {
				%>
				<div class="row-fluid first">
					<%
						} else {
					%>
					<div class="row-fluid">
						<%
							if (Integer.parseInt(pageContext.getAttribute(
												"indexIntValue").toString()) + 1 == 6) {
						%><span
							id="loadAjaxDiv_<%=curPageNo%>"></span>
						<%
							}
						%>
						<%
							}
						%>
						<div class="c5">
							<%--Added Tweet this by Indumathi.S Mar_26_2016--%>
							<c:if test="${not empty studAwardsRes.curYear}">
								<%
									if (curYear.equals(loadYear)) {
								%>
								<input type="hidden" id="collegeName${studAwardsRes.curYear}"
									value="${studAwardsRes.collegeDisplayName}" />
								<div class="st_rank tw_ttip">
									<a href="javascript:void(0);"
										onclick="showAndHideTweet('tweetThis${studAwardsRes.curYear}');"
										onmouseover="showTweetThis('tweetThis${studAwardsRes.curYear}');"
										onmouseout="hideTweetThis('tweetThis${studAwardsRes.curYear}');">
										<span class="rktxt">${studAwardsRes.curYear}</span><sup>${studAwardsRes.curYearSubScript}</sup>
										<c:if test="${not empty studAwardsRes.rankStatus}">
											<c:if test="${'equal' eq studAwardsRes.rankStatus}">
												<i class="<%=rnkEqual%>"></i>
											</c:if>
											<c:if test="${'lesser' eq studAwardsRes.rankStatus}">
												<i class="<%=rnkLesser%>"></i>
											</c:if>
											<c:if test="${'greater' eq studAwardsRes.rankStatus}">
												<i class="<%=rnkGreater%>"></i>
											</c:if>
										</c:if>
									</a>
									<div class="chk_cmp"
										onmouseover="showTweetThis('tweetThis${studAwardsRes.curYear}');"
										onmouseout="hideTweetThis('tweetThis${studAwardsRes.curYear}');">
										<a style="display: none;"
											id="tweetThis${studAwardsRes.curYear}" class="chktxt"
											target="_blank"
											onclick="tweetThisUrlFnc('collegeName${studAwardsRes.curYear}',this);">
											Tweet this?<em></em>
										</a>
									</div>
								</div>
								<%
									} else {
								%>
								<div class="st_rank">
									<span class="rktxt">${studAwardsRes.curYear}</span><sup>${studAwardsRes.curYearSubScript}</sup>
									<c:if test="${not empty studAwardsRes.rankStatus}">
										<c:if test="${'equal' eq studAwardsRes.rankStatus}">
											<i class="<%=rnkEqual%>"></i>
										</c:if>
										<c:if test="${'lesser' eq studAwardsRes.rankStatus}">
											<i class="<%=rnkLesser%>"></i>
										</c:if>
										<c:if test="${'greater' eq studAwardsRes.rankStatus}">
											<i class="<%=rnkGreater%>"></i>
										</c:if>

									</c:if>
								</div>
								<%
									}
								%>
							</c:if>
							<!--End-->
						</div>
						<div class="c6">
							<c:if test="${not empty studAwardsRes.lastYear}">
								<div class="st_ltyr">
									<div class="st_ltcnr">
										<span class="ltxt">${studAwardsRes.lastYear}</span><sup>${studAwardsRes.lastYearSubScript}</sup>
									</div>
								</div>
							</c:if>
							<c:if test="${empty studAwardsRes.lastYear}">
								<span class="ltxt">N/A</span>
							</c:if>
						</div>
						<div class="c7">
							<c:if test="${not empty studAwardsRes.collegeLogo}">
								<div class="img_ppn">
									<a href="${studAwardsRes.uniHomeURL}" class="div1"
										title="${studAwardsRes.collegeDisplayName}"> <img
										class="awd_logo  lazy-load"
										src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"
										data-src="${studAwardsRes.collegeLogo}"
										title="${studAwardsRes.collegeDisplayName}"
										alt="${studAwardsRes.collegeDisplayName}">
									</a>
								</div>
							</c:if>
						</div>
						<div class="c8">
							<div class="rat_sum cf bar_rat">
								<div class="rat_bar">
									<a href="${studAwardsRes.uniHomeURL}" class="div1"
										title="${studAwardsRes.collegeDisplayName}">${studAwardsRes.collegeDisplayName}</a>
									<c:if test="${not empty studAwardsRes.rating}">
										<div class="div2">
											<span class="ct-cont"><span class="bar"
												style="width:${studAwardsRes.ratingPercent};"></span></span>
										</div>
										<div class="div3">${studAwardsRes.rating}</div>
									</c:if>
								</div>
							</div>
						</div>
						<div class="c9" style="<%=c9DivStyle%>">
							<c:if test="${not empty studAwardsRes.subOrderItemId}">
								<div class="btns_interaction">
									<c:if test="${studAwardsRes.subOrderItemId ne 0}">
										<c:set var="collegeName" value="${studAwardsRes.collegeName}" />
										<%
											String webformPrice = new WUI.utilities.CommonFunction()
																	.getGACPEPrice((String) pageContext
																			.getAttribute("subOrderItem"),
																			"webform_price");
															String websitePrice = new WUI.utilities.CommonFunction()
																	.getGACPEPrice((String) pageContext
																			.getAttribute("subOrderItem"),
																			"website_price");
															String gaCollegeName = new CommonFunction()
																	.replaceSpecialCharacter((String) pageContext
																			.getAttribute("collegeName"));
										%>

										<c:if test="${not empty studAwardsRes.subOrderEmailWebform}">
											<%--Changed event action webclick to webform by Sangeeth.S for July_3_18 rel--%>
											<a rel="nofollow" target="_blank" class="req-inf"
												onclick="GAInteractionEventTracking('emailwebform', 'interaction', 'email webform', '<%=gaCollegeName%>', <%=webformPrice%>); cpeEmailWebformClick(this,'${studAwardsRes.collegeId}','${studAwardsRes.subOrderItemId}','${studAwardsRes.networkId}','${studAwardsRes.subOrderEmailWebform}'); var a='s.tl(';"
												href="${studAwardsRes.subOrderEmailWebform}"
												title="Email${studAwardsRes.collegeDisplayName}">Request
												info<i class="fa fa-caret-right"></i>
											</a>
										</c:if>
										<c:if test="${not empty studAwardsRes.subOrderEmail}">
											<c:if test="${empty studAwardsRes.subOrderEmailWebform}">
												<a rel="nofollow"
													onclick="GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '<%=gaCollegeName%>');adRollLoggingRequestInfo('${studAwardsRes.collegeId}',this.href);return false;"
													class="req-inf"
													href="<SEO:SEOURL pageTitle="sendcollegemail" >${studAwardsRes.collegeName}#${studAwardsRes.collegeId}#0#0#n#${studAwardsRes.subOrderItemId}</SEO:SEOURL>"
													title="Email ${studAwardsRes.collegeDisplayName}">Request
													info<i class="fa fa-caret-right"></i>
												</a>
											</c:if>
										</c:if>
										<c:if
											test="${not empty studAwardsRes.subOrderProspectusWebform}">
											<a rel="nofollow" target="_blank" class="get-pros"
												onclick="GAInteractionEventTracking('prospectuswebform', 'interaction', 'prospectus webform', '<%=gaCollegeName%>', <%=webformPrice%>); cpeProspectusWebformClick(this,'${studAwardsRes.collegeId}','${studAwardsRes.subOrderItemId}','${studAwardsRes.networkId}','${studAwardsRes.subOrderProspectusWebform}'); var a='s.tl(';"
												href="${studAwardsRes.subOrderProspectusWebform}"
												title="Get ${studAwardsRes.collegeDisplayName} Prospectus">Get
												prospectus<i class="fa fa-caret-right"></i>
											</a>
										</c:if>
										<c:if
											test="${not empty studAwardsRes.subOrderProspectus and empty studAwardsRes.subOrderProspectusWebform}">
											<a
												onclick="GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '<%=gaCollegeName%>'); return prospectusRedirect('/degrees','${studAwardsRes.collegeId}','0','','','${studAwardsRes.subOrderItemId}');"
												class="get-pros"
												title="Get ${studAwardsRes.collegeDisplayName} Prospectus">Get
												prospectus<i class="fa fa-caret-right"></i>
											</a>

										</c:if>
									</c:if>
								</div>
							</c:if>
						</div>
					</div>
			</c:forEach>
		</div>
		<input type="hidden" id="curPageNo" value="<%=curPageNo%>" />
		<%
			if (totalCount != null && !"".equals(totalCount)) {
					results = Integer.parseInt(totalCount);
					totalPages = results / 10;
					if (results % 10 > 0) {
						totalPages++;
					}
					for (int i = 2; i <= totalPages; i++) {
		%>
		<div id="next_results_<%=i%>"></div>
		<%
			}
				}
		%>
		<input type="hidden" id="totPageNo" value="<%=totalPages%>" />
		<%
			if (totalPages > 1) {
		%>
		<div id="loader_text" class="ldr-con txt_cnr" style="display: none;">Loading
			more results..</div>
		<script type="text/javascript">loadAjaxPaginationRes();</script>
		<div class="ldr" id="loader" style="display: none;"></div>
		<%
			}
				String catCount = "ten";

				if ("2017".equals(loadYear) || "2018".equals(loadYear)) {
					catCount = "twelve";
				} else if ("2014".equals(loadYear) || "2012".equals(loadYear)) {
					catCount = "nine";
				}
		%>
		<c:if test="${ empty requestScope.ajaxResAppend}">
			<div class="awd_info_txt">
				<%--Changed text for 21_March_2017, By Thiyagu G--%>
				<p>The key difference between the Whatuni Student Choice Awards
					and other university rankings tables is that it's solely driven by
					student opinion.</p>
				<p>There is no cost for universities to enter and there is no
					judging panel.</p>
				<%
					if ("2018".equals(loadYear)) {
				%>
				<p>Existing students score universities across various aspects
					of their experience to help future students find the right uni, and
					to help the universities continually improve.</p>
				<%
					} else {
				%>
				<p>
					Students give their university an overall rating of one to five; we
					average those results to create overall ratings for each university
					across
					<%=catCount%>
					categories of student experiences.
				</p>
				<p>Existing students write reviews of their university
					experiences to help future students find the right uni, and to help
					the universities continually improve.</p>
				<%
					}
				%>
				<%=awardsReview%>
			</div>
			<%
				if (!GenericValidator.isBlankOrNull(previousYear)) {
			%>
			<div class="sr_calbtn3">
				<a class="sr_pd"
					href="/student-awards-winners/<%=previousYear%>.html">See the <%=previousYear%>
					results here <i class="fa fa-long-arrow-right"></i></a>
			</div>
			<%
				}
			%>
		</c:if>
		<c:if test="${not empty requestScope.stdchAjaxSrch}">
			<div class="awd_info_txt">
				<%--Changed text for 21_March_2017, By Thiyagu G--%>
				<p>The key difference between the Whatuni Student Choice Awards
					and other university rankings tables is that it's solely driven by
					student opinion.</p>
				<p>There is no cost for universities to enter and there is no
					judging panel.</p>
				<%
					if ("2018".equals(loadYear)) {
				%>
				<p>Existing students score universities across various aspects
					of their experience to help future students find the right uni, and
					to help the universities continually improve.</p>
				<%
					} else {
				%>
				<p>
					Students give their university an overall rating of one to five; we
					average those results to create overall ratings for each university
					across
					<%=catCount%>
					categories of student experiences.
				</p>
				<p>Existing students write reviews of their university
					experiences to help future students find the right uni, and to help
					the universities continually improve.</p>
				<%
					}
				%>
				<%=awardsReview%>
			</div>
			<%
				if (!GenericValidator.isBlankOrNull(previousYear)) {
			%>
			<div class="sr_calbtn3">
				<a class="sr_pd"
					href="/student-awards-winners/<%=previousYear%>.html">See the <%=previousYear%>
					results here <i class="fa fa-long-arrow-right"></i></a>
			</div>
			<%
				}
			%>
		</c:if>
	</div>
	<c:if test="${not empty requestScope.sponserBy}">
		<input type="hidden" id="cateSponserImg"
			value="<%=CommonUtil.getImgPath("/wu-cont/images/",0)%>${requestScope.sponserBy}" />
		<input type="hidden" id="cateSponserTxt"
			value="${requestScope.sponserBy}" />
	</c:if>
	<c:if test="${not empty requestScope.sponserURL}">
		<input type="hidden" id="cateSponserURL"
			value="${requestScope.sponserURL}" />
	</c:if>
</c:if>
<c:if test="${empty requestScope.studChoiceAwardsList}">
	<c:if test="${not empty requestScope.stChUniHome}">
		<div class="list_view" id="<%=parentDivId%>">
			<c:if test="${empty requestScope.intlSchErr}">
				<div class="Nores">
					<c:if test="${not empty requestScope.backToResults}">
						<a class="bk_btn"
							href="javascript:loadStudCateResult('${requestScope.backToResults}')">BACK
							TO RESULTS<i class="fa fa-long-arrow-right"></i>
						</a>
					</c:if>
					<%
						if ("2018".equalsIgnoreCase(loadYear)) {
										if ("International".equalsIgnoreCase(categorName)
												|| "Postgraduate"
														.equalsIgnoreCase(categorName)) {
					%>
					<h2 class="ud_tit">
						The uni you're searching for didn't qualify for the
						<%=categorName%>
						category. Only universities with enough reviews written by
						<%=categorName%>
						students are included. Don't worry though - you can read what
						students think about <a class="link_blue"
							href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
						by visiting their profile...
					</h2>
					<%
						} else if ("Further Education College"
												.equalsIgnoreCase(categorName)) {
					%>
					<h2 class="ud_tit">
						This category is for further education colleges that have higher
						education provision. Have a look at the other categories to see
						how students rated <a class="link_blue"
							href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
					</h2>
					<%
						} else if ("Independent Higher Education"
												.equalsIgnoreCase(categorName)) {
					%>
					<h2 class="ud_tit">
						This category is for independent higher education providers. Have
						a look at the other categories to see how students rated <a
							class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
					</h2>
					<%
						} else {
					%>
					<h2 class="ud_tit">
						The uni you're searching for didn't qualify for this category.
						Only universities with 150 or more reviews are included. Don't
						worry though - you can read what students think about <a
							class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
						by visiting their profile...
					</h2>
					<%
						}
									}
						
						else if ("2020".equalsIgnoreCase(loadYear)) {
									if ("International".equalsIgnoreCase(categorName)
											|| "Postgraduate"
													.equalsIgnoreCase(categorName)) {
				%>
				<h2 class="ud_tit">
					Oh dear, the uni you're searching for didn't qualify for our
						<%=loadYear%>
						rankings (only universities with 50 or more reviews made the
						grade). Don't worry though, you can read what students think
						about <a class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
						by visiting their profile...
				</h2>
				<%
					}else if ("Student Union".equalsIgnoreCase(categorName)) {
%>
<h2 class="ud_tit">
					Oh dear, the uni you're searching for didn't qualify for our
		<%=loadYear%>
		rankings in Students' Union. Don't worry though, you can read what students think
		about <a class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
		by visiting their profile...
</h2>

<%}else if ("Accommodation".equalsIgnoreCase(categorName)) {%>
	<h2 class="ud_tit awd_info_txt spotit2">
	<span class="fnt_18 txt_bold">The uni you're searching for didn't qualify for our <%=loadYear%> rankings in Accommodation.</span><br/>
	 Whatuni follows a strict verification process with each institution having to reach a preset threshold. Unfortunately due to an internal data loss, 
a small number of institutions were unable to meet the requirements in <%=loadYear%>. You can read what students think about
<a class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a> by visiting their profile.
</h2>

<%
} else {
				%>
				<h2 class="ud_tit">
					Oh dear, the uni you're searching for didn't qualify for our
		<%=loadYear%>
		rankings in <%=categorName%>. Don't worry though, you can read what students think
		about <a class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
		by visiting their profile...
				</h2>
				<%
					}
								}
						else {
					%>
					<h2 class="ud_tit">
						Oh dear, the uni you're searching for didn't qualify for our
						<%=loadYear%>
						rankings (only universities with 100 or more reviews made the
						grade). Don't worry, though - you can read what students think
						about <a class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
						by visiting their profile...
					</h2>
					<%
						}
					%>
					<div class="sr_calbtn2">
						<a class="sr_pd"
							href="<%=request.getAttribute("stChReviewHome")%>">READ
							REVIEWS<i class="fa fa-long-arrow-right"></i>
						</a>
					</div>
				</div>
			</c:if>
			<c:if test="${not empty requestScope.intlSchErr}">
				<div class="Nores">
					<c:if test="${not empty requestScope.backToResults}">
						<a class="bk_btn"
							href="javascript:loadStudCateResult('${requestScope.backToResults}')">BACK
							TO RESULTS<i class="fa fa-long-arrow-right"></i>
						</a>
					</c:if>
					<%
						if ("2018".equalsIgnoreCase(loadYear)) {
										if ("International".equalsIgnoreCase(categorName)
												|| "Postgraduate"
														.equalsIgnoreCase(categorName)) {
					%>
					<h2 class="ud_tit">
						The uni you're searching for didn't qualify for the
						<%=categorName%>
						category. Only universities with enough reviews written by
						<%=categorName%>
						students are included. Don't worry though - you can read what
						students think about <a class="link_blue"
							href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
						by visiting their profile...
					</h2>
					<%
						} else if ("Further Education College"
												.equalsIgnoreCase(categorName)) {
					%>
					<h2 class="ud_tit">
						This category is for further education colleges that have higher
						education provision. Have a look at the other categories to see
						how students rated <a class="link_blue"
							href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
					</h2>
					<%
						} else if ("Independent Higher Education"
												.equalsIgnoreCase(categorName)) {
					%>
					<h2 class="ud_tit">
						This category is for independent higher education providers. Have
						a look at the other categories to see how students rated <a
							class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
					</h2>
					<%
						} else {
					%>
					<h2 class="ud_tit">
						The uni you're searching for didn't qualify for this category.
						Only universities with 150 or more reviews are included. Don't
						worry though - you can read what students think about <a
							class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
						by visiting their profile...
					</h2>
					<%
						}
									}else if ("2020".equalsIgnoreCase(loadYear)) {
										if ("International".equalsIgnoreCase(categorName)
												|| "Postgraduate"
														.equalsIgnoreCase(categorName)) {
					%>
					<h2 class="ud_tit">
						Oh dear, the uni you're searching for didn't qualify for our
						<%=loadYear%>
						rankings (only universities with 50 or more reviews made the
						grade). Don't worry though, you can read what students think
						about <a class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
						by visiting their profile...
					</h2>
	<%
		} else if ("Student Union".equalsIgnoreCase(categorName)) {
%>
<h2 class="ud_tit">
Oh dear, the uni you're searching for didn't qualify for our
						<%=loadYear%>
						rankings in Students' Union. Don't worry though, you can read what students think
						about <a class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
						by visiting their profile...
</h2>
<%}else if ("Accommodation".equalsIgnoreCase(categorName)) {
%>
<h2 class="ud_tit awd_info_txt spotit2">
  <span class="fnt_18 txt_bold">The uni you're searching for didn't qualify for our <%=loadYear%> rankings in Accommodation.</span><br/>
   Whatuni follows a strict verification process with each institution having to reach a preset threshold. Unfortunately due to an internal data loss, 
a small number of institutions were unable to meet the requirements in <%=loadYear%>. You can read what students think about
<a class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a> by visiting their profile.
</h2>
<%
} else {
					%>
					<h2 class="ud_tit">
						Oh dear, the uni you're searching for didn't qualify for our
						<%=loadYear%>
						rankings in <%=categorName%>. Don't worry though, you can read what students think
						about <a class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
						by visiting their profile...
					</h2>
					<%
						}
									} else {
					%>
					<h2 class="ud_tit">
						Oh dear, the uni you're searching for didn't qualify for the
						International category only universities with 100 or more reviews
						written by international students made the grade. Don't worry,
						though - you can read what students think about <a
							class="link_blue" href="${requestScope.stChUniHomeURL}">${requestScope.stChUniHome}</a>
						by visiting their profile...
					</h2>
					<%
						}
					%>


					<div class="sr_calbtn2">
						<a class="sr_pd"
							href="<%=request.getAttribute("stChReviewHome")%>">READ
							REVIEWS<i class="fa fa-long-arrow-right"></i>
						</a>
					</div>
				</div>
			</c:if>
		</div>
	</c:if>
	<input id="shwsky" type="hidden" value="nosky" />
</c:if>

