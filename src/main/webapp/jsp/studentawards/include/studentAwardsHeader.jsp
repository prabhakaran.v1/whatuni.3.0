<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.GlobalConstants"%>
<%String loadYear = request.getAttribute("loadYear") != null ? (String)request.getAttribute("loadYear") : "";
String currentAwardYear = request.getAttribute("currentAwardYear") != null ? (String)request.getAttribute("currentAwardYear") : "";
String categorName = request.getAttribute("categorName") != null ? (String)request.getAttribute("categorName") : "University of the year";%>
<div class="ad_cnr">
  <div class="content-bg">
    <div class="subCon cf">      
      <!-- year,category&sponsor div opening-->
      <div class="Awd_tpcnr awd_yoe">
        <span style="display:none;" id="studchLoadResgif" class="loading_icon"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>"></span>
      
        <div class="awd_catcnr">
        <c:if test="${not empty requestScope.awardYearList}">
        <fieldset class="w100p fl">
         <c:if test="${not empty requestScope.awardYearList}">	     
              <label for="yearOfEntryId" class="lbco">YEAR</label>
              <div class="select time fwc">
                <span class="fnt_lbd" id="yearSpan"><%=loadYear%></span><i class="fa fa-angle-down fa-lg"></i>
              </div>
              <fmt:parseNumber var = "loadYearNum" type = "number" value = "${currentAwardYear}" />
              <select id="yearOfEntryId" class="time" onchange="loadStudentYear(this.value);">  
                <c:forEach var="awardYearList" items="${requestScope.awardYearList}" varStatus="index"> 
                <c:if test="${loadYear eq awardYearList.currentYear}">
                  <option value="${awardYearList.currentYear}" selected="selected">${awardYearList.currentYear}</option>
                </c:if>
                <fmt:parseNumber var = "currYearNum" type = "number" value = "${awardYearList.currentYear}" />
                 <c:if test="${loadYear ne awardYearList.currentYear and loadYearNum >= currYearNum}">
                  <option value="${awardYearList.currentYear}" >${awardYearList.currentYear}</option>
                </c:if>
                </c:forEach>
              </select>  
            </c:if>   
          </fieldset>		
          </c:if>
          <input type="hidden" id="loadYear" value="<%=loadYear%>"/>
          <!--Modified by Indumathi.S Mar_29_16-->
          <input type="hidden" name="currentAwardYear" id="currentAwardYear" value="<%=currentAwardYear%>"/>
          <fieldset class="w100p fl">
               <c:if test="${not empty requestScope.studChoiceCateList}">
              <label for="categoryId" class="lbco">CATEGORY</label>
              <div class="select time fwc">
                <span class="fnt_lbd" id="awdcatSpan"><%=categorName%></span><i class="fa fa-angle-down fa-lg"></i>
              </div>              
               <select id="categoryId" class="time" onchange="loadStudCateResult();">
               <c:forEach var="studChoiceCateList" items="${requestScope.studChoiceCateList}">   
               <c:if test="${categorName eq  studChoiceCateList.questionTitle}">
                  <option value="${studChoiceCateList.questionId}" selected="selected"> ${studChoiceCateList.questionTitle}</option>
                </c:if>
                <c:if test="${categorName ne  studChoiceCateList.questionTitle}">
                  <option value="${studChoiceCateList.questionId}" > ${studChoiceCateList.questionTitle}</option>
                </c:if>
                </c:forEach>
              </select>    
            </c:if> 
          </fieldset>																								
        </div>  
        <c:if test="${ not empty requestScope.sponserBy}">           
          <div class="spon_logo_cnr" id="sponserDiv">
            <%if("2018".equalsIgnoreCase(loadYear) && ("Independent Higher Education".equalsIgnoreCase(categorName) || "Further Education College".equalsIgnoreCase(categorName))){%>
              <span  class="spon_txt">In partnership with...</span>
            <%}else{%>
              <span  class="spon_txt">Sponsored by..</span>                    
            <%}%>
            <c:if test="${ not empty requestScope.sponserURL}">  
              <a id="cateSpoUrl" rel=nofollow href="${requestScope.sponserURL}" class="spon_logo" target="_blank">
                <img id="cateSpoImg" src="<%=CommonUtil.getImgPath("/wu-cont/images/",0)%>${requestScope.sponserBy}"  alt="${requestScope.sponserURL}"/>
              </a> 
            </c:if>  
            <c:if test="${ empty requestScope.sponserURL}">                 
              <a rel=nofollow href="#" class="spon_logo" target="_blank" id="cateSpoUrl">
                <img id="cateSpoImg" src="<%=CommonUtil.getImgPath("/wu-cont/images/",0)%>${requestScope.sponserBy}" alt="${requestScope.sponserURL}"/>            
              </a>  
            </c:if>                     
          </div>          
        </c:if>  
            <c:if test="${ empty requestScope.sponserBy}">  
          <div class="spon_logo_cnr" id="sponserDiv" style="display:none;">
          
            <span  class="spon_txt">Sponsored by..</span>                    
            <c:if test="${ empty requestScope.sponserURL}">
              <a rel=nofollow href="#" class="spon_logo" target="_blank" id="cateSpoUrl"><img id="cateSpoImg" src=""/></a>
            </c:if>                    
          </div>
        </c:if>      
      </div>            
      <!--year,category&sponsor div ending-->                  
      <jsp:include page="/jsp/studentawards/include/studentAwardsResults.jsp"/>      
    </div>
  </div>
</div>