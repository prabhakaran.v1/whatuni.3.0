<%@ page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction"%>

<%
  String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");  
  String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
  String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
  String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
%>  

<script type="text/javascript" language="javascript">
  
  var dev = jQuery.noConflict();
  dev(document).ready(function(){
    adjustStyle();
  });
  adjustStyle();
  
  function jqueryWidth() {
    return dev(this).width();
  } 
  
  function adjustStyle() {
    var width = document.documentElement.clientWidth;
    dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
    dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
    var path = ""; 
    if(width <= 480) {    
      if(dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }        
      <%if(("LIVE").equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
      document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}%>
      dev(".course_deatils").hide();
      dev(".tool_tip right").hide();
    }else if((width > 480) && (width <= 992)) {      
      if(dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }       
      <%if(("LIVE").equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
      document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}%>
      dev(".course_deatils").show();      
      dev(".tool_tip right").hide();
      dev(".St_Awd .c8.course_deatils").css("display", "block"); 
      dev(".scr_ttip").css("float", "right");      
    }else{
      if(dev("#viewport").length > 0) {
        dev("#viewport").remove();
      }
      document.getElementById('size-stylesheet').href = "";   
      dev(".hm_srchbx").hide();
      dev(".course_deatils").show();
      dev(".tool_tip right").show();
    }
  }
  dev(window).on('orientationchange', orientationChangeHandler);
  
function orientationChangeHandler(e) {
    setTimeout(function() {
        dev(window).trigger('resize');
    }, 500);
}
dev(window).resize(function() {
  dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
  dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
  var screenWidth = jqueryWidth();
  if (screenWidth <= 480){  //Mobile view 
    
  }
  else if((screenWidth > 480) && (screenWidth <= 992)){  //Tablet view 
    
  }
  else{  //Desktop view 
    /*For sticky top*/
    var windowTop = dev(window).scrollTop();         
    if(150 < windowTop){
      dev('#fixedscrolltop').addClass('sticky_top_nav');        
      dev('#fixedscrolltop').css({ position: 'fixed', top: 0 });            
    }else{
      dev('#fixedscrolltop').removeClass('sticky_top_nav');
      dev('#fixedscrolltop').css('position','static');         
    }
    /*For sticky top*/
    
  }
  adjustStyle();  
});
function jqueryWidth() {
    return dev(this).width();
}
</script>
