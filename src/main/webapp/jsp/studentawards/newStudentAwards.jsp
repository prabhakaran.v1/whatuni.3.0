<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.SessionData, WUI.utilities.GlobalConstants,WUI.utilities.CommonFunction, WUI.utilities.CommonUtil" %>
<%  
  String httpStr = new CommonFunction().getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
  String currentPageUrl = httpStr + "www.whatuni.com/student-awards-winners/2014.html";
  String resultPageFlag = request.getAttribute("resultPageFlag") != null ? (String)request.getAttribute("resultPageFlag") : "";
  String indexFollow = "inValidYear".equalsIgnoreCase(resultPageFlag) ? "noindex, follow" : "index, follow";//Added by Indumathi.S 29_Mar_2016 WUSCA changes
%>
    <% //String awardImage[] = new String[]{ "OVERALL_10.png","STUD-UNION_10.png","CLUBS-SOCIETIES_10.png","ACCOMMODATION_10.png", "UNI-FACILITY_10.png", "COURSE-LECTURER_10.png", "EYE-CANDY_10.png", "CITY-LIFE_10.png","JOB-PROSPECTS_10.png"}; 
       //String awardImage[] = new String[]{ "o_rating_5","s_union_5","c_society_5","accom_5", "u_facility_5", "c_lecturer_5", "eye_candy_5", "city_life_5","j_pros_5"};
       //String awardImage[] = new String[]{ "o_rating_10","s_union_10","c_society_10","accom_10", "u_facility_10", "c_lecturer_10", "eye_candy_10", "city_life_10","j_pros_10"};
       String awardImage[] = new String[]{ "astr","ajob","acal","astu", "aacm", "auf", "acl", "acas","aint"};
       // String awardImage[] = new String[]{ "o_rating_20","s_union_20","c_society_20","accom_20", "u_facility_20", "c_lecturer_20", "eye_candy_20", "city_life_20","j_pros_20"}; 
    %>
    <body id="www-whatuni-com">
    <header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
</header> 
    <div class="clipart">
    <div id="content-bg">
    <div id="wrapper">
        <div  class="sca">
            <h2 class="hs-1 loc-ins mb-20"><strong>UK University Rankings : Whatuni Student Choice Awards</strong></h2>
              <div class="scat video">
                <span>
                  <img width="150" alt="whatuni awards" src="<%=CommonUtil.getImgPath("/wu-cont/images/awards.jpg", 0)%>">  
                </span>
                <p>
                  <!--<p><strong>Welcome to the results of the Whatuni Student Choice Awards 2014!</strong> The rankings below are based on the 18,000+ reviews we 
                  received in the months prior to our (incredibly glamorous) May 2014 Awards ceremony (Clive Anderson was there and everything). 
                  The awards go to the unis that have been rated the highest for each category by current and past students, so you can be sure they're based on 
                  personal experience (you can trust them, in other words)...</p>-->      
                  
                  <div id="subTextSummary" class="desc fl mr20"><p ><strong>Welcome to the results of the Whatuni Student Choice Awards 2014!</strong> The rankings below are based on the 18,000+ reviews we 
                  received in the months prior to our (incredibly glamorous) May 2014 Awards ceremony...<br /><a onclick="javaScript:showReadmoreSummary();">view more</a></p>
                  <p><a title="Back to the 2015 results" href="/student-awards-winners/2015.html">Back to the 2015 results</a></p>
                  </div>
                     
                  <div id="courseSummary" style="display:none;" class="desc fl mr20"><p><strong>Welcome to the results of the Whatuni Student Choice Awards 2014!</strong> The rankings below are based on the 18,000+ reviews we 
                  received in the months prior to our (incredibly glamorous) May 2014 Awards ceremony (Clive Anderson was there and everything). 
                  The awards go to the unis that have been rated the highest for each category by current and past students, so you can be sure they're based on 
                  personal experience (you can trust them, in other words).<br /><a onclick="javaScript:showReadmoreSummary();">hide</a></p>
                  <p><a title="Back to the 2015 results" href="/student-awards-winners/2015.html">Back to the 2015 results</a></p>
                  </div>
                  
                  
                </p>
                <c:if test="${not empty requestScope.mediaPath}">
                <div class="fr vid_tn" style="cursor: pointer;" onclick="javascript:dynamicVideoPlayerJsLoad('${requestScope.mediaPath}','<%=CommonUtil.getImgPath("/wu-cont/images/awards.jpg", 0)%>');">                    
                    <a class="icon_play"><i class="fa fa-youtube-play fa-3_5x "></i></a>                    
                </div>
                </c:if>
  <%--               <!--<div class="fr">
                    <logic:present name="mediaPath" scope="request">
                        <a onclick="javascript:dynamicVideoPlayerJsLoad('<bean:write name="mediaPath" scope="request" filter="false"/>','<%=CommonUtil.getImgPath("/wu-cont/images/awards.jpg", 0)%>');">
                            <img width="250" height="150" alt="whatuni awards" src="<%=CommonUtil.getImgPath("/wu-cont/images/awards.jpg", 0)%>">
                        </a>
                    </logic:present>
                </div> --%>
              </div>
              <div class="dialog" id="box">
                  <div align="center" id="flashbanner"></div>
                  <div id="linksdiv" class="mt30" style="display:none;">
                     <span class="cros"><strong><a href="javascript:closeHomeVideo()">Close</a></strong></span>
                   </div>
                </div>
              <div class="scam">
              <c:if test="${not empty requestScope.studAwardList}">
                  <div class="scm-tp">
                  <%int count=0;%>
                    <c:forEach var="studAwardList" items="${requestScope.studAwardList}" varStatus="index">    
                    <c:set var="indexIntValue" value="${index.index}"/>      
                     <%  String className="";
                         if(Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIntValue"))) % 10 == 0) {  
                         if(count % 3 == 0){if(Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIntValue"))) !=0){
                          if((count) == 3 ){
                            out.print("</div><div class='scm-mdl'>");
                          }else{
                            out.print("</div><div class='scm-btm'>");
                          }
                          
                         }}   
                         
                         count++;%> 
                    <div class="scam fst">
                      <div class="saft">
                        <div class="saft lt">
                          <span class="<%=awardImage[count-1]%>"></span>
                        </div>
                        <div class="saft rt"><span>${studAwardList.questionTitle}</span></div>
                      </div>
                      
                      <div class="safm">
                        <ol>
                       <%}%>
                       <c:if test="${not empty studAwardList.collegeName}">
                         <c:set var = "collegeName" value = "${studAwardList.collegeName}" />                     
                         <li><a href="<SEO:SEOURL pageTitle="unilanding" ><%=pageContext.getAttribute("collegeName")%><spring:message code="wuni.seo.url.split.character" />${studAwardList.collegeId}</SEO:SEOURL>"  title="View ${studAwardList.collegeDisplayName} ${studAwardList.questionTitle} reviews" >${studAwardList.collegeDisplayName}</a></li>
                          </c:if>
                         <% if(Integer.parseInt(String.valueOf(Integer.parseInt(pageContext.getAttribute("indexIntValue").toString())+1)) %10==0){
                            if(Integer.parseInt(String.valueOf(pageContext.getAttribute("indexIntValue"))) !=0){
                         %>   
                              </ol></div>
                              <div class="safl"><c:if test="${not empty studAwardList.sponsorBy}"><p>sponsored by</p>
                              <p><c:if test = "${not empty studAwardList.sponsorURL}">
                                     <a target="_blank" href="${studAwardList.sponsorURL}"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/", Integer.parseInt((String)pageContext.getAttribute("indexIntValue")))%>${studAwardList.sponsorBy}" alt=""/></a>
                                 </c:if>
                                 <c:if test = "${empty studAwardList.sponsorURL}">
                                     <img src="<%=CommonUtil.getImgPath("/wu-cont/images/", Integer.parseInt(pageContext.getAttribute("indexIntValue").toString())) %>${studAwardList.sponsorBy}" alt=""/>
                                 </c:if>
                               </p></c:if></div>
                           </div>
                          <%}}%> 
                  </c:forEach>
                  </div>
                </c:if>
                
                <%--<div class="slyw"><a href="/student-awards-winners/2012.html">See last year's winners</a></div>--%>
              </div>
              
               
            </div>
            
            
        <%-- <div id="footer">
           <jsp:include page="/include/wuniFooter.jsp" />
       </div> --%>
    </div>
    </div>
    </div>
    </div>
    <jsp:include page="/jsp/common/wuFooter.jsp" />  
    <input type="hidden" name="hdrMenuHide" id="hdrMenuHide" value="READREVIEWS" />
  </body>

