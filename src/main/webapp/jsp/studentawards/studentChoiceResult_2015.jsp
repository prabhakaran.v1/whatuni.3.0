<%@page import="WUI.utilities.CommonFunction"%>
<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator"%>  
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

  <%
    String indexfollow = "index,follow"; 
    String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
    String canonicalUrl = domainSpecificPath + "/student-awards-winners/university-of-the-year/";
    String loadYear = request.getAttribute("loadYear") != null ? (String)request.getAttribute("loadYear") : "";
    String currentAwardYear = request.getAttribute("currentAwardYear") != null ? (String)request.getAttribute("currentAwardYear") : "";
    if(!GenericValidator.isBlankOrNull(loadYear) && Integer.parseInt(loadYear) < Integer.parseInt(currentAwardYear)){
      canonicalUrl = domainSpecificPath + "/degrees/student-awards-winners/"+loadYear+".html";
    }
    String categoryName = request.getAttribute("categorName") != null && currentAwardYear.equals(loadYear) ? (String)request.getAttribute("categorName") : "";
    String pageName = currentAwardYear.equals(loadYear) ? "WHATUNI STUDENT AWARDS" : "WHATUNI AWARDS PAGE";
    String paramFlag = currentAwardYear.equals(loadYear) ? "WHATUNI STUDENT AWARDS" : "WU_HCSTUFF";
    String awardYear = loadYear;
    String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
    //Added uni details for retaining uni when we do the category search, By Thiyagu G for 21_March_2017.
    String uniId = request.getAttribute("uniId") != null ? (String)request.getAttribute("uniId") : "";
    String uniNameDisplay = request.getAttribute("uniNameDisplay") != null ? (String)request.getAttribute("uniNameDisplay") : "";
    String uniNameDisp = !GenericValidator.isBlankOrNull((String)request.getAttribute("uniNameDisplay")) ? (String)request.getAttribute("uniNameDisplay") : "Enter university name";
    String studChoiceAwardsJsName = CommonUtil.getResourceMessage("wuni.student.choice.awards.js", null);
    if("2020".equals(currentAwardYear)){studChoiceAwardsJsName = CommonUtil.getResourceMessage("wuni.student.choice.2020.awards.js", null);}%>
<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
  <input type="hidden" name="currAwardYear" id="currAwardYear" value="<%=currentAwardYear%>" />
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=studChoiceAwardsJsName%>"></script>          
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>      
</header>
<div class="St_Awd">
<section class="hero_pod p0" id="heroslider"> 
  <div class="fix_top_bor"> 
    <div id="fixedscrolltop1">
      <div class="ad_cnr">
        <div class="content-bg">
          <div class="w100p cf wsa_wrc">            
            <div class="St_Awd_top">
              <div class="sbcr mb-10 pros_brcumb" id="bcrummbs">								 
                <div class="bc">                  
                  <jsp:include page="/seopods/breadCrumbs.jsp"/>                    
                </div>                
              </div>              	
              <h1 class="cf tit2">Whatuni Student Choice Awards ${loadYear}</h1>	              
            </div>
            <div class="srch_open">
              <div class="filt_srch">                
                <fieldset class="fr srch_box">                  
                  <input type="text" name="uniName" id="uniName" value="<%=uniNameDisp%>" autocomplete="off" class="fnt_lrg gen_srchbox"
                    onkeydown="clearUniNAME(event,this);" 
                    onkeyup="autoCompleteResUniNAME(event,this)"  
                    onclick="clearUniDefaultTEXT(this)" 
                    onblur="setSchUniDefaultTEXT('Enter university name','uniName')"
                    onkeypress="javascript:if(event.keyCode==13){loadUniAwardResult();}"/>                  
                  <form:form id="wuscaUniForm" method="post" action="" commandName = "StudentAwardsBean" >
                    <input type="hidden"  id="uniName_hidden"/>
                    <input type="hidden" name="collegeId" id="uniName_id" value="<%=uniId%>" />
                    <input type="hidden" name="collegeDisplayName" id="uniName_display" value="<%=uniNameDisplay%>" />
                    <input type="hidden" id="uniName_alias"/>
                    <input type="hidden" id="uniName_url"/>
                    <input type="button" class="gen_icon_srch fr" onclick="loadUniAwardResult();"/>
                  </form:form>
                </fieldset>
              </div>
            </div>
            <%if(!GenericValidator.isBlankOrNull(loadYear) && Integer.parseInt(loadYear) == 2017){%>
            <div class="awd_info_txt">
                <p>The WUSCAs are our annual celebration of student satisfaction in higher education. The 26,000 reviews we receive each year represent the most comprehensive student voice in the country. Choose a university and a category below to find out how they did last year.</p> 
            </div>
            <%} else if(!GenericValidator.isBlankOrNull(loadYear) && Integer.parseInt(loadYear) == 2018){%>
            <div class="awd_info_txt">
                <p>The WUSCAs are our annual celebration of student satisfaction in higher education. The 36,000+ reviews we received towards this year's awards showcase the student voice and help you make informed decisions. Choose a university and a category below to find out how they did.</p> 
            </div>
            <%}else if(!GenericValidator.isBlankOrNull(loadYear) && Integer.parseInt(loadYear) == 2019){%>
            <div class="awd_info_txt">
                <p>The WUSCAs are our annual celebration of the best universities and higher education institutions in the UK. Every single award has been voted for by the people that matter most: real students, through the 41,000+ student reviews we've collected this year. Select a category below to find out who won what...</p> 
            </div>
            <%}else if(!GenericValidator.isBlankOrNull(loadYear) && Integer.parseInt(loadYear) == 2020){%>
            <div class="awd_info_txt">
                <p>The WUSCAs are our annual celebration of the best universities and higher education institutions in the UK. Every single award has been voted for by the people that matter most: real students, through the 41,000+ student reviews we've collected this year. Select a category below to find out who won what...</p> 
            </div>
            <%}%>
          </div>	           
        </div>
      </div>
    </div>
  </div> 
</section>		
<jsp:include page="/jsp/studentawards/include/studentAwardsHeader.jsp"/> 
</div>
<jsp:include page="/jsp/common/wuFooter.jsp" />
<input type="hidden" name="hdrMenuHide" id="hdrMenuHide" value="READREVIEWS" />
</body>
