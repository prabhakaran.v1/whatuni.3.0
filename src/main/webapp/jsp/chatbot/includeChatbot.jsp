<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import=" WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, java.util.*, mobile.util.MobileUtils, WUI.utilities.GlobalConstants" %>
<% 
  ArrayList subQuestionDetails = request.getAttribute("subQuestionDetails") != null ? (ArrayList)request.getAttribute("subQuestionDetails") : null;
  ArrayList answerOptionsList = request.getAttribute("answerOptionsList") != null ? (ArrayList)request.getAttribute("answerOptionsList") : null;
  ArrayList previousQualList = request.getAttribute("previousQualList") != null ? (ArrayList)request.getAttribute("previousQualList") : null;
  int subQuestionDetailSize = subQuestionDetails != null ? subQuestionDetails.size() : 0;
  boolean mobileFlag = new MobileUtils().userAgentCheck(request);
  String quizSeparator = GlobalConstants.QUIZ_SEPARATOR;
  String quizQuestionSep = GlobalConstants.QUIZ_QUESTION_SEP;
  String quizAnswerSep = GlobalConstants.QUIZ_ANSWER_SEP;
  String quizPrevQualSep = GlobalConstants.QUIZ_PREV_QUAL_SEP;
  String quizMobileSkipSep = GlobalConstants.QUIZ_MOBILE_SKIP_SEP;
  //newly added for grade filter change Nov 21st rel Jeya
  int loop = 0;
  int count = 0, subCount = 0, subLen = 0; 
  String qualSub = "";
  String qualId = ""; 
  String ajaxActionName = "QUAL_SUB_AJAX";  
  String removeBtnFlag = "display:block";
  String selectFlag = "";
  String gcseSelectFlag = "";
  String addQualStyle = "display:block";
  String addQualBtnStyle = "display:block";
  String gcseSelOption = ": 9-1";
  String removeLink = CommonUtil.getImgPath("/wu-cont/images/ch_minus_blue.svg",0);
  String whitePlusImg = CommonUtil.getImgPath("/wu-cont/images/ch_white_plus.svg",0);
  String qualifications = "";
  String selectQualId ="";
  String selectQualSubLen = "";
   String qualCode = !GenericValidator.isBlankOrNull((String)request.getAttribute("qualCode")) ? (String)request.getAttribute("qualCode") : "";
   String referralUrl = !GenericValidator.isBlankOrNull((String)request.getAttribute("referralUrl")) ? (String)request.getAttribute("referralUrl") : "";
   String userId = (String)request.getAttribute("userId");
   String ucasMaxPoints = (String)request.getAttribute("ucasMaxPoints");
   String styleFlag = "display:block"; String styleFlag2 = "display:none"; String styleFlag3 = "display:none"; 
   int addSubLimit = 6;
   String removeQualStyle = "display:none";
   int userQualListSize = (!GenericValidator.isBlankOrNull((String)request.getAttribute("userQualSize"))) ? Integer.parseInt((String)request.getAttribute("userQualSize")) : 0;
   String addQualId = "level_3_add_qualif";
   String callTypeFlag = "";
 %>

<c:if test="${not empty requestScope.questionDetailsList}">   
  <c:forEach var="questionDetail" items="${requestScope.questionDetailsList}" varStatus="index">  
  
    <c:if test="${not empty requestScope.subQuestionDetails}">                                      
     <c:forEach var="subQuestionDetail" items="${requestScope.subQuestionDetails}" varStatus="subIndex"> 
        <c:set var="subQuestionDetailsSize" value="${requestScope.subQuestionDetails.size()-1}"/>                     
            <%-- ::START:: wu583_20181120 - Sabapathi: added class name based on conditions  --%>
            <%
            String noBgImgClass = "";
            String addStyleBg = "";
            %>
            <c:if test="${subIndex.index gt 0}">
            <c:if test="${subIndex.index eq subQuestionDetailsSize}">
            <c:if test="${questionDetail.filterName eq 'Filter remove'}">
                  <c:if test="${questionDetail.answerDisplayStyle eq 'FILTERS_TO_REMOVE'}">
                            <%
                              noBgImgClass = "no_bgimg";
                              addStyleBg = "style='background-image: none'";
                            %>
                    </c:if>
                </c:if>
             </c:if>
            </c:if>
            <%-- ::END:: wu583_20181120 - Sabapathi: added class name based on conditions  --%>
            <div class="cht_bot <%=noBgImgClass%>" <%=addStyleBg%>>
              <div class="cht_ico"></div>
              <c:if test="${subIndex.index eq 0}">
              <c:if test="${questionDetail.answerType ne 'AJAX'}">
                  <c:if test="${questionDetail.filterName ne 'Region'}">
                    <div class="cht_msg cht_min" >
                      <img id="image_${questionDetail.questionId}${subIndex.index}" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif", 0)%>" alt="" width="40"/>
                      <span id="questionTxt_${questionDetail.questionId}${subIndex.index}" style="display:none">${subQuestionDetail.questionText}</span>
                    </div>
                  </c:if>
                  <c:if test="${questionDetail.filterName eq 'Region'}">
                    <c:if test="${questionDetail.answerDisplayStyle eq 'SELECT_BOX'}">                
                      <div class="cht_msg cht_un chs_reg" >
                        <img id="image_${questionDetail.questionId}${subIndex.index}" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif", 0)%>" alt="" width="40"/>                        
                        <div onclick="displaySubLocation();" id="questionTxt_${questionDetail.questionId}${subIndex.index}" style="display:none" class="cho_reg">Choose region <i class="fa fa-angle-down fa-1_4x fr"></i></div>
                      </div>
                    </c:if>         
                  </c:if>
                </c:if>
               </c:if>
               <c:if test="${subIndex.index gt 0}">
                
                  <%-- ::START:: wu583_20181120 - Sabapathi: added class name based on conditions  --%>
                  <%String className = "cht_un cht_sub";%>
                  <c:if test="${questionDetail.filterName eq 'Filter remove'}">
                    <c:if test="${questionDetail.answerDisplayStyle eq 'FILTERS_TO_REMOVE'}">
                            <%className = "";%>
                        </c:if>
                  </c:if>     
                  <%-- ::END:: wu583_20181120 - Sabapathi: added class name based on conditions  --%>
                  
                  <div class="cht_msg cht_min <%=className%>" id="chtMsg_${questionDetail.questionId}${subIndex.index}">	
                    <img id="image_${questionDetail.questionId}${subIndex.index}" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif", 0)%>" alt="" width="40"/>																					
                    <span id="questionTxt_${questionDetail.questionId}${subIndex.index}" style="display:none">${subQuestionDetail.questionText}</span>					
                    <c:if test="${subIndex.index eq subQuestionDetailsSize}">
                    <c:if test="${questionDetail.answerType eq 'AJAX'}">
                      <input class="subjectInpAjx" style="display:none" type="text" id="searchsubject_${questionDetail.questionId}${subIndex.index}" onkeyup="getCourseList(this.id);" placeholder="${requestScope.optionText}" />				
                    </c:if>
                    <%-- ::START:: wu583_20181120 - Sabapathi: added filters remove block  --%>
                    <c:if test="${questionDetail.answerType eq 'SINGLE_SELECTION'}">
                     <c:if test="${questionDetail.filterName eq 'Filter remove'}">                
                      <c:if test="${questionDetail.answerDisplayStyle eq 'FILTERS_TO_REMOVE'}">
                             <div class="cbf_row cbf_fld" style="display:none" id="question_${questionDetail.questionId}_q">
                                <ul class="cbf_col">
                                <c:forEach var="answerOption" items="${requestScope.answerOptionsList}" varStatus="answerIndex">
                                     <li>
                                      <%String optionTextString = "";%>
                                      <c:if test="${not empty answerOption.optionText}">
                                      <c:set var="optionTextStringSplit" value="${answerOption.optionText}"/>
                                        <% optionTextString = ((String)pageContext.getAttribute("optionTextStringSplit")).split("##SPLIT##")[0];%>
                                      </c:if>
                                      <span class="cbf_cou"><a href="javascript:getQuizDetails('${answerOption.nextQuestionId}', '${answerOption.optionValue}', '${answerOption.optionText}', '${questionDetail.questionId}', '', 'removeFilter')"><%=optionTextString%> <img src="<%=CommonUtil.getImgPath("/wu-cont/images/cbf_img1.png", 1)%>" title="<%=optionTextString%>"></a></span>
                                      <span class="cbf_scunt">
                                      <c:if test="${not empty answerOption.optionValue}">
                                         <c:if test="${answerOption.optionValue eq 1}">
                                            Shows ${answerOption.optionValue} course
                                         </c:if>
                                         <c:if test="${answerOption.optionValue ne 1}">
                                            Shows ${answerOption.optionValue} courses
                                          </c:if>
                                        </c:if>
                                      </span>
                                    </li>
                                  </c:forEach>
                                </ul>
                             </div>
                        </c:if>
                     </c:if>    
                    </c:if>
                    <%-- ::END:: wu583_20181120 - Sabapathi: added filters remove block  --%>
                   </c:if>
                  </div>                   
               </c:if>
               
               <!--Subject search ajax-->	
               <c:if test="${subIndex.index eq 0}">
               
                 <c:if test="${questionDetail.answerType eq 'AJAX'}">
                   <c:if test="${questionDetail.filterName ne 'JOB_OR_INDUSTRY'}">
                     <div class="cht_msg cht_min cht_un cht_sub" >
                       <img id="image_${questionDetail.questionId}${subIndex.index}" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif", 0)%>" alt="" width="40"/>
                       <span id="questionTxt_${questionDetail.questionId}${subIndex.index}" style="display:none" >${subQuestionDetail.questionText}</span>								
                       <c:if test="${subIndex.index eq subQuestionDetailsSize}">
                         <input class="subjectInpAjx" style="display:none" type="text" id="searchsubject_${questionDetail.questionId}${subIndex.index}" onkeyup="getCourseList(this.id);" placeholder="${requestScope.optionText}" />
                       </c:if>
                     </div>  
                   </c:if>
                 </c:if> 
               </c:if>
                
               <c:if test="${subIndex.index eq subQuestionDetailsSize}">
               <c:if test="${questionDetail.answerType eq 'AJAX'}">
                 <c:if test="${questionDetail.filterName ne 'JOB_OR_INDUSTRY'}">
                     <div class="cht_sub" id="searchResultList">									
                     </div>
                  </c:if>
                 </c:if>
               </c:if>

               <!--Job or Industry search ajax-->	
               	<c:if test="${subIndex.index eq 0}">
                  <c:if test="${questionDetail.answerType eq 'AJAX'}">
                  <c:if test="${questionDetail.filterName eq 'JOB_OR_INDUSTRY'}"> 
                     <div class="cht_msg cht_min cht_un cht_sub" >
                       <img id="image_${questionDetail.questionId}${subIndex.index}" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif", 0)%>" alt="" width="40"/>
                       <span id="questionTxt_${questionDetail.questionId}${subIndex.index}" style="display:none" >${subQuestionDetail.questionText}</span>								
                       <c:if test="${subIndex.index eq subQuestionDetailsSize}">
                         <input class="subjectInpAjx" style="display:none" type="text" id="searchsubject_${questionDetail.questionId}${subIndex.index}" onkeyup="getJobOrIndustryList(this.id);" placeholder="${requestScope.optionText}" />
                      </c:if>
                     </div>
                   </c:if>
                 </c:if>
              </c:if>
               <c:if test="${subIndex.index eq subQuestionDetailsSize}">
                <c:if test="${questionDetail.answerType eq 'AJAX'}">
                 <c:if test="${questionDetail.filterName eq 'JOB_OR_INDUSTRY'}">
                     <div class="cht_sub" id="jobOrIndustryResultList" >									
                     </div>
                     </c:if>
                 </c:if>
               </c:if>
              
              <!--select list-->
              <c:if test="${subIndex.index eq subQuestionDetailsSize}">
              
              <c:if test="${questionDetail.answerType eq 'SINGLE_SELECTION'}">
               
                <c:if test="${questionDetail.answerDisplayStyle ne 'FULL_BUTTON'}">
                
                   <c:if test="${questionDetail.answerDisplayStyle ne 'BUTTONS'}">
                   
                      <c:if test="${not empty requestScope.answerOptionsList}">
                     
                       <c:if test="${questionDetail.filterName ne 'Filter remove'}">
                      
                           <c:if test="${questionDetail.filterName ne 'Region'}">
                          
                            <div class="grd_un" style="display:none" id="question_${questionDetail.questionId}_q">
                           
                              <c:forEach var="answerOption" items="${requestScope.answerOptionsList}" varStatus="answerIndex">
                             
                              <c:set var="answerOptionsListSize" value="${requestScope.answerOptionsList.size()-1}"/>
                             
                               <c:if test="${answerOption.nextQuestionId ne 0}">
                              
                                  <div class="deg_sel">  
                                    <c:if test="${questionDetail.answerDisplayStyle ne 'LIST_WITH_IMAGE'}">
                                   
                                    <c:if test="${questionDetail.filterName eq 'Qualification'}">
                                   
                                        <label id="qualSingleSel_${answerIndex.index}" onclick="selectQualification(this.id, 'qualification');">
                                        
                                          <input type="radio" name="qualification" value="${answerOption.optionValue}|${answerOption.optionText}|${answerOption.nextQuestionId}"/>
                                         
                                          <c:choose>
                                          <c:when test="${answerIndex.index eq 0}">
                                                 <span class="tp_rn">${answerOption.optionText}</span>
                                          </c:when>
                                          <c:otherwise>
                                          <c:choose>
                                          <c:when test="${answerIndex.index eq answerOptionsListSize}">
                                            <span class="bt_rn">${answerOption.optionText}</span>            
                                          </c:when>
                                          <c:otherwise>
                                           <span>${answerOption.optionText}</span>
                                          </c:otherwise>
                                          </c:choose>
                                          </c:otherwise>
                                          </c:choose>
                                        </label>
                                      </c:if>
                                    </c:if>
                                    <c:if test="${questionDetail.answerDisplayStyle eq 'LIST_WITH_IMAGE'}">
                                            
                                      <label id="qualSingleSelLoc_${answerIndex.index}" onclick="selectQualification(this.id, 'location');">
                                       
                                        <c:choose>
                                        <c:when test="${answerIndex.index eq 0}">
                                       
                                        <input type="radio" name="location" value="${answerOption.optionValue}|${answerOption.optionText}|${answerOption.nextQuestionId}" id="location_${answerIndex.index}" checked="checked"/>
                                        </c:when>
                                        <c:otherwise>
                                        
                                        <input type="radio" name="location" value="${answerOption.optionValue}|${answerOption.optionText}|${answerOption.nextQuestionId}" id="location_${answerIndex.index}" />
                                        </c:otherwise>
                                        </c:choose>
                                      
                                        <c:set var="imageName" value="${answerOption.imageName}"/>
                                        <c:choose>
                                        <c:when test="${answerIndex.index eq 0}">   
                                         
                                          <span class="tp_rn"><img src="<%=CommonUtil.getImgPath("/wu-cont/img/icons/"+(String)pageContext.getAttribute("imageName")+".png", 0)%>" class="cn_flg" alt="UK">${answerOption.optionText}</span>
                                        </c:when>
                                        <c:otherwise>
                                        <c:choose>
                         
                                        <c:when test="${answerIndex.index eq answerOptionsListSize}">
                                     
                                          <span class="bt_rn"><img src="<%=CommonUtil.getImgPath("/wu-cont/img/icons/"+(String)pageContext.getAttribute("imageName")+".png", 0)%>" class="cn_flg" alt="UK">${answerOption.optionText}</span>            
                                       </c:when>
                                       <c:otherwise>
                                       
                                           <span><img src="<%=CommonUtil.getImgPath("/wu-cont/img/icons/"+(String)pageContext.getAttribute("imageName")+".png", 0)%>" class="cn_flg" alt="UK">${answerOption.optionText}</span>
                                       </c:otherwise>
                                       </c:choose>
                                        </c:otherwise>
                                        </c:choose>                                         
                                      </label>
                                    </c:if>
                                <c:if test="${questionDetail.answerDisplayStyle ne 'LIST_WITH_IMAGE'}">
                              
                                   <c:if test="${questionDetail.filterName ne 'Qualification'}">                                  
                                        <label id="qualSingleSel_${answerIndex.index}" onclick="selectQualification('single_select');">
                                          <input type="radio" name="radioOpt" value="${answerOption.optionText}|${answerOption.optionValue}|${answerOption.nextQuestionId}"/>
                                         <c:choose>
                                         <c:when test="${answerIndex.index eq 0}">
                                            <span class="tp_rn">${answerOption.optionText}</span>
                                         </c:when>
                                         <c:otherwise>
                                         <c:choose>
                                         <c:when test="${answerIndex.index eq answerOptionsListSize}">
                                            <span class="bt_rn">${answerOption.optionText}</span>            
                                          </c:when>
                                          <c:otherwise>
                                           <span>${answerOption.optionText}</span>
                                          </c:otherwise>
                                          </c:choose>
                                          </c:otherwise>
                                          </c:choose>
                                        </label>
                                         </c:if>                                                                      
                                    </c:if>
                                   
                                  </div>  
                                </c:if>
                              </c:forEach>
                            </div>
                         </c:if>
                          </c:if>
                          <c:if test="${questionDetail.filterName eq 'Region'}">
                          <c:if test="${questionDetail.answerDisplayStyle eq 'SELECT_BOX'}">                       
                              <div class="cht_sub sub_region" style="display:none" id="question_${questionDetail.questionId}_q">	
                                <ul>
                                  <li id="qualSingleSel_i" onclick="selectQualification(this.id, 'region', 'ENGLAND');">All England</li>                                       
                                  <c:forEach var="answerOption" items="${requestScope.answerOptionsList}" varStatus="answerIndex">
                                  <c:if test="${answerOption.nextQuestionId ne 0}">                                   
                                      <li id="qualSingleSel_${answerIndex.index}" onclick="selectQualification(this.id, 'region', '${answerOption.optionValue}');">${answerOption.optionText}</li>
                                    </c:if>
                                  </c:forEach>
                                </ul>
                              </div>
                            </c:if>    
                          </c:if>
                      </c:if>
                    </c:if>
                  </c:if>
                </c:if>
                
                </div>
                  <c:if test="${questionDetail.answerType eq 'DROPDOWN'}">
  
   <c:forEach var="qualList" items="${requestScope.previousQualList}" varStatus="i" begin="0" end="0">
        <img id="image_${questionDetail.questionId}1" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif", 0)%>" alt="" width="40"/>              
                      <c:if test="${not empty qualList.qualId}">
                      <c:set var="selectQualId1" value="${qualList.qualId}"/>
                      <%
                        String selectQualId1 = pageContext.getAttribute("selectQualId1").toString(); 
                        selectQualId = selectQualId1;
                      %>
                      </c:if>
                      <c:if test="${empty qualList.qualId}">
                        <% selectQualId ="1";%>
                      </c:if>
                      <c:if test="${questionDetail.filterName eq 'First qualification'}">
                      <%count = 0;%>
                      </c:if>
                       <c:if test="${questionDetail.filterName eq 'Second qualification'}">
                      <%count = 1;%>
                      </c:if>
                       <c:if test="${questionDetail.filterName eq 'Third qualification'}">
                      <%count = 2;%>
                      </c:if>
  
<div class="cht_bot cht_snd cb_grde" id="question_${questionDetail.questionId}1" style="display:none">
                           <div class="grde">
                                <div class="grades">
   <div class="adqual_rw" data-id=level_3_add_qualif id="level_3_qual_<%=count%>">
                        <div class="ucas_row">
                         <fieldset class="ucas_fldrw">
                            <div class="remq_cnt">
                              <div class="ucas_selcnt">
                                <fieldset class="ucas_sbox">
                                  <c:set var="selectQualName" value="${qualList.qualification}"/>
                                  <%String selectQualName = pageContext.getAttribute("selectQualName").toString(); %>
                                  <%if("0".equals(String.valueOf(count))){if("A - Levels".equalsIgnoreCase(selectQualName)){selectQualName="A Level";}qualifications = selectQualName ;}%>
                                  <span class="sbox_txt" id="qualspan_<%=count%>"><%=selectQualName%></span>
                                  <span class="grdfilt_arw"></span>
                                </fieldset>
                                <select name="qualification1" onchange="appendQualDiv(this, '', '<%=count%>', 'new_entry_page');" id="qualsel_<%=count%>" class="ucas_slist qus_slt">
                                  <option value="0">Please Select</option>
                                  <c:forEach var="qualListLevel3" items="${requestScope.previousQualList}">
                                   <jsp:scriptlet>selectFlag = "";if("".equals(selectQualId)){selectQualId="1";}</jsp:scriptlet>
                                   <c:set value="<%=selectQualId%>" var="selectQualId"/>
                                   <c:if test="${qualListLevel3.qualId eq selectQualId}">
                                     <jsp:scriptlet>selectFlag = "selected=\"selected\"";</jsp:scriptlet>
                                   </c:if>
                                   <c:if test="${qualListLevel3.qualification ne 'GCSE'}">
                                     <c:if test="${empty qualListLevel3.qualId}">
                                       <optgroup label="${qualListLevel3.qualification}"></optgroup>
                                     </c:if>
                                     <c:if test="${not empty qualListLevel3.qualId}">
                                       <option value="${qualListLevel3.qualId}" <%=selectFlag%>>${qualListLevel3.qualification} </option>
                                     </c:if>                                   
                                   </c:if>
                                  </c:forEach>
                                </select>
                                <c:forEach var="grdQualifications" items="${requestScope.previousQualList}">
                                 <c:if test="${grdQualifications.qualification ne 'GCSE'}">                                
                                   <c:if test="${not empty grdQualifications.qualId}">
                                     <input type="hidden" id="qualSubj_<%=count%>_${grdQualifications.qualId}" name="qualSubj_${grdQualifications.qualId}" value="${grdQualifications.noOfsubjects}" />
                                     <input type="hidden" id="qualGrde_<%=count%>_${grdQualifications.qualId}" name="" value="${grdQualifications.gradeStr}" />
                                   </c:if>
                                 </c:if>                               
                               </c:forEach>
                              </div>
                            </div>
                                             
                          </fieldset>
                        	  
                        </div>
                        <div class="subgrd_fld" id="subGrdDiv<%=count%>">
                        <div class="ucas_row grd_row" id="grdrow_<%=count%>">
                         
                            <c:if test="${not empty qualList.noOfsubjects}">   
                              <c:set var="selectQualSubLen1" value="${qualList.noOfsubjects}"/>
                              <%
                                String selectQualSubLen1 = pageContext.getAttribute("selectQualSubLen1").toString();
                                selectQualSubLen = selectQualSubLen1;
                              %>
                            </c:if>
                            <c:if test="${empty qualList.noOfsubjects}">
                              <%selectQualSubLen = "3";%>
                            </c:if>
                              
                            <c:forEach var="subjectList" items="${previousQualList}" varStatus="j" begin="0" end="<%=Integer.parseInt(selectQualSubLen) - 1%>">
                              <c:set var="indexJValue" value="${j.index}"/>
                              <%subCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("indexJValue")));%>
                              <div class="rmv_act" id="subSec_<%=count%>_<%=subCount%>">
                                <fieldset class="ucas_fldrw">
                                  <div class="ucas_tbox w-390 tx-bx">
                                    <input type="text" data-id="qual_sub_id" id="qualSub_<%=count%>_<%=subCount%>" name="subIp_<%=subCount%>" onkeyup="cmmtQualSubjectList(this, 'qualSub_<%=count%>_<%=subCount%>', '<%=ajaxActionName%>', 'ajaxdiv_<%=count%>_<%=subCount%>');" placeholder="Enter subject" value="" class="frm-ele" autocomplete="off">															
                                    <input type="hidden" id="qualSubHid_<%=count%>_<%=subCount%>" name="qualSubHid1" />
                                    <div id="ajaxdiv_<%=count%>_<%=subCount%>" data-id="wugoSubAjx" class="ajaxdiv">
                                    </div>
                                  </div>
                                  <div class="ucas_selcnt rsize_wdth">
                                    <div class="ucas_sbox mb-ht w-84" id="1grades1">
                                      <span class="sbox_txt" id="span_grde_<%=count%>_<%=subCount%>">A*</span><span class="grdfilt_arw"></span>
                                    </div>
                                    <select class="ucas_slist" onchange="setGradeVal(this, 'grde_<%=count%>_<%=subCount%>');getUcasPoints('sub_<%=count%>_<%=subCount%>', 'GRADE');" id="qualGrd_<%=count%>_<%=Integer.parseInt(pageContext.getAttribute("indexJValue").toString())%>"></select>
                                    <script>appendGradeDropdown('<%=count%>', 'qualGrd_<%=count%>_<%=String.valueOf(pageContext.getAttribute("indexJValue"))%>','${subjectList.gradeStr}');</script>                                    
                                    <input class="subjectArr" data-id="level_3" type="hidden" id="sub_<%=count%>_<%=subCount%>" name="sub_<%=selectQualId%>" value="" />                                
                                    <input class="gradeArr" type="hidden" id="grde_<%=count%>_<%=subCount%>" name="grde_<%=selectQualId%>" value="A*" />
                                    <input class="qualArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_<%=selectQualId%>" value="<%=selectQualId%>" />
                                    <input class="qualSeqArr" type="hidden" id="qual_<%=count%>_<%=subCount%>" name="qual_<%=selectQualId%>" value="<%=count+1%>" />
                                  </div>
                                  <%if(subCount == 0){
                                    removeBtnFlag = "display:none";
                                  }else{
                                    removeBtnFlag = "display:block";
                                  }
                                  if(subCount == 1){%>                                  
                                    <script>$("#remSubRow<%=count%>0").show();</script>
                                  <%}%>                                  
                                  <div class="add_fld"> <a style="<%=removeBtnFlag%>" id="remSubRow<%=count%><%=subCount%>" onclick="removeSubject('subSec_<%=count%>_<%=subCount%>','<%=count%>');" class="btn_act1 bck f-14 pt-5"><span class="hd-desk"><img class="aq_img" src="<%=removeLink%>" title="Remove subject &amp; grade"></span></a></div>
                                  
                                </fieldset>
                              </div>
                            </c:forEach>                        
                            <div class="ad-subjt" id="addSubject_<%=count%>" style="display:block">
                              <a class="btn_act bck fnrm" id="subAdd<%=count%>" onclick="addSubject('<%=selectQualId%>','countAddSubj_<%=count%>', '<%=count%>');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>
                            </div>                          
                          </div>
                        </div>
                        
                        <input type="hidden" class="total_countAddSubj_<%=count%>" id= "total_countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                        <input type="hidden" class="countAddSubj_<%=count%>" id= "countAddSubj_<%=count%>" value="<%=subCount + 1%>"/>
                      </div>
                      </div>
                      </div></div></c:forEach>
                      </c:if>
                   </c:if>                        
    <div class="cht_bot ">

              <!--multi select-->
              <c:if test="${subIndex.index eq subQuestionDetailsSize}">
             <c:if test="${questionDetail.answerType eq 'MULTI_SELECTION'}">
              <c:if test="${not empty requestScope.answerOptionsList}">
                   <c:if test="${questionDetail.answerDisplayStyle ne 'GRADE_BOX'}">                             
                        <div class="grd_un" style="display:none" id="question_${questionDetail.questionId}_q">                  
                          <c:forEach var="answerOption" items="${requestScope.answerOptionsList}" varStatus="answerIndex">
                            <c:if test="${answerOption.optionText ne 'MULTIPLE'}">                                         
                              <div class="deg_sel">
                                <label onclick="selectQualification(this.id,'locType');" id="qualSingleSel_${answerIndex.index}">
                                  <input type="checkbox" name="locationType" value="${answerOption.optionText}|${answerOption.optionValue}|${fn:escapeXml(answerOption.commentAgainstAnswer)}" id="locationType_${answerIndex.index}"/>
                                  <c:choose>
                                  <c:when test="${answerIndex.index eq 0}">
                                    <span class="tp_rn">${answerOption.optionText}</span>
                                  </c:when>
                                  <c:otherwise>
                                  <c:set var="answerOptionsListSize2" value="${requestScope.answerOptionsList.size()-2}"/>
                                  <c:choose>
                                  <c:when test="${answerIndex.index eq answerOptionsListSize2}">
                                    <span class="bt_rn">${answerOption.optionText}</span>            
                                  </c:when>
                                  <c:otherwise>
                                    <span>${answerOption.optionText}</span>
                                  </c:otherwise>
                                  </c:choose>
                                  </c:otherwise>
                                  </c:choose>
                                </label>
                              </div>	
                            </c:if>
                          </c:forEach>
                          <c:forEach var="answerOption" items="${requestScope.answerOptionsList}" varStatus="answerIndex">
                          <c:if test="${answerOption.optionText eq 'MULTIPLE'}">
                            <c:if test="${not empty answerOption.commentAgainstAnswer}">                                      
                                <input type="hidden" id="commentForLocType" value="${answerOption.commentAgainstAnswer}">
                              </c:if>   
                            </c:if>
                          </c:forEach>
                        </div>                          
                     </c:if>
                  </c:if> 
                </c:if>
              </c:if>
           </div>
             
            <!--Previous qualification-->
            <c:if test="${subIndex.index eq subQuestionDetailsSize}">
             <c:if test="${questionDetail.answerDisplayStyle eq 'GRADE_BOX'}">                                                       
                <div class="cht_bot cht_snd cb_grde" style="display:none" id="question_${questionDetail.questionId}_q">
                  <div class="grde">
                    <div class="grades">
                      <ul id="dynamicQualList">                                                                        
                      </ul>										
                    </div>                         																													
                  </div>
                </div>
              </c:if>
            </c:if>                                                       
            <%=quizSeparator%>
        </c:forEach>              
      </c:if>
       
       <c:if test="${questionDetail.answerDisplayStyle eq 'BUTTONS'}">                                                                                         
     <c:if test="${not empty requestScope.answerOptionsList}">
            <div  id="yesNoDiv" class="yesNoDiv" style="display:none">
              <ul>      
              <c:forEach var="answerOption" items="${requestScope.answerOptionsList}" varStatus="answerIndex">        
                  <li><a class="btn_act btn_bor" href="javascript:void(0);" onclick="getQuizDetails('${answerOption.nextQuestionId}', '${answerOption.optionValue}', '${fn:escapeXml(answerOption.optionText)}', '${answerOption.nextQuestionId}', '', 'yesNo');" >${answerOption.optionText}</a></li> 
               </c:forEach>                                   
              </ul>
            </div>   
          </c:if>
      </c:if>  
      
    <c:if test="${questionDetail.answerDisplayStyle eq 'ADD_QUAL_BUTTONS'}">                                                                                         
     <c:if test="${not empty requestScope.answerOptionsList}">
            <div  id="addQualButton" class="addQualButton" style="display:none">
              <ul>     
               <c:if test="${not empty requestScope.skipExit}">
                      <li>
                        <a classs="Cbot_skpvw" href="javascript:void(0);" id="skipView_${questionDetail.questionId}" onclick="getQuizDetails('${questionDetail.skipExit}','skpExt','','${questionDetail.questionId}');">
                            <c:if test="${not empty requestScope.courseCount}">
                              <c:if test="${requestScope.courseCount eq 1}">
                                  Skip and view ${requestScope.courseCount} result
                                </c:if>
                                <c:if test="${requestScope.courseCount ne 1}">
                                 Skip and view ${requestScope.courseCount} results
                                </c:if>
                            </c:if>
            
                        </a>
                      </li>   
                    </c:if>   
               <c:forEach var="answerOption" items="${requestScope.answerOptionsList}" varStatus="answerIndex">   
                  <c:if test="${answerOption.nextQuestionId eq '41' or answerOption.nextQuestionId eq '42'}">     
                   <li><a class="btn_act ad-qus cur-disabled" id="selectAns_${answerOption.nextQuestionId}" href="javascript:void(0);" onclick="getQuizDetails('${answerOption.nextQuestionId}', '${answerOption.optionValue}', '${fn:escapeXml(answerOption.optionText)}', '${answerOption.nextQuestionId}', '', 'yesNo');" ><span class="fl qus-txt">${answerOption.optionText}</span><span class="fr"><img class="aq_img" src="<%=whitePlusImg%>" alt="Add a qualification"></span></a></li>
                  </c:if>
                   <c:if test="${answerOption.nextQuestionId eq '12'}"> 
                   <c:if test="${questionDetail.filterName eq 'First qualification'}"> 
                     <li><a class="fl btn_act cur-disabled" id="selectAns_${answerOption.nextQuestionId}" href="javascript:void(0);" onclick="getQuizDetails('${answerOption.nextQuestionId}', '${answerOption.optionValue}', '${fn:escapeXml(answerOption.optionText)}', '${answerOption.nextQuestionId}', '', 'yesNo');" >${answerOption.optionText}</a></li>
                   </c:if>
                   <c:if test="${questionDetail.filterName eq 'Second qualification'}"> 
                     <li><a class="fl btn_act" id="selectAns_${answerOption.nextQuestionId}" href="javascript:void(0);" onclick="getQuizDetails('${answerOption.nextQuestionId}', '${answerOption.optionValue}', '${fn:escapeXml(answerOption.optionText)}', '${answerOption.nextQuestionId}', '', 'yesNo');" >${answerOption.optionText}</a></li>
                   </c:if>
                   <c:if test="${questionDetail.filterName eq 'Third qualification'}"> 
                     <li><a class="fl btn_act" id="selectAns_${answerOption.nextQuestionId}" href="javascript:void(0);" onclick="getQuizDetails('${answerOption.nextQuestionId}', '${answerOption.optionValue}', '${fn:escapeXml(answerOption.optionText)}', '${answerOption.nextQuestionId}', '', 'yesNo');" >${answerOption.optionText}</a></li>
                   </c:if>
                   </c:if> 
               </c:forEach>  
                                              
              </ul>
            </div>   
          </c:if>
      </c:if>  
           
      <c:if test="${questionDetail.ctaButtonText eq 'Done'}">
       <c:if test="${not empty requestScope.answerOptionsList}">
           <c:if test="${not empty requestScope.skipToNext}"> 
              <div id="skipQuesDiv" class="skipQuesDiv" style="display:none">
                <ul>
                  <%if(!mobileFlag){%>
                  <c:if test="${not empty requestScope.skipToNext}">
                       <li><a href="javascript:void(0);" onclick="getQuizDetails('${questionDetail.skipToNext}','skpNxt','','${questionDetail.questionId}')">Skip to next question</a></li> 
                    </c:if>
                    <c:if test="${not empty requestScope.skipExit}">
                      <li>
                        <a href="javascript:void(0);" id="skipView_${questionDetail.questionId}" onclick="getQuizDetails('${questionDetail.skipExit}','skpExt','','${questionDetail.questionId}');">
                            <%-- ::START:: wu583_20181120 - Sabapathi: added this block to display course count with text  --%>
                          
                            <c:if test="${not empty requestScope.courseCount}">
                              <c:if test="${requestScope.courseCount eq 1}">
                         
                                  Skip and view ${requestScope.courseCount} result
                                </c:if>
                                <c:if test="${requestScope.courseCount ne 1}">
                                
                                  Skip and view ${requestScope.courseCount} results
                                </c:if>
                            </c:if>
                            <%-- ::END:: wu583_20181120 - Sabapathi: added this block to display course count with text --%>
                        </a>
                      </li>   
                    </c:if>
                  <%} else {%>
                  <c:if test="${not empty requestScope.skipToNext}">
                       <li><a href="javascript:void(0);" onclick="skipQuestionFn();" >Skip question</a></li> 
                    </c:if>
                  <%}%>
                  <li><a href="javascript:void(0);" id="selectAns" class="btn_act cbbtn_dis" onclick="getQuizDetails('done','','','${questionDetail.questionId}');" >Done</a></li>
                </ul>
              </div>                  
          
          </c:if>
          </c:if>
          <c:if test="${empty requestScope.skipToNext}">
            <div id="skipQuesDiv" class="skipQuesDiv skpvw_dis" style="display:none">
              <ul>               
                <li><a href="javascript:void(0);" id="selectAns" class="btn_act cbbtn_dis" onclick="getQuizDetails('done','','','${questionDetail.questionId}');" >Done</a></li>
              </ul>
            </div>  
         </c:if>
     </c:if>    
         
      <c:if test="${questionDetail.answerDisplayStyle eq 'FULL_BUTTON'}">
      <c:if test="${not empty requestScope.answerOptionsList}">
            <div id="fullButton" style="display:none">
              <ul>
              <c:forEach var="answerOption" items="${requestScope.answerOptionsList}" varStatus="answerIndex">               
                  <li><a class="btn_act chk_res" href="javascript:void(0);" onclick="checkYourResults('${answerOption.nextQuestionId}');">${answerOption.optionText}</a></li> 
               </c:forEach>                               
              </ul>
            </div>   
        </c:if>
      </c:if>                       
         
         <c:if test="${questionDetail.filterName eq 'Grade'}">
        <div class="cht_ftr cbot_err" style="display:none" id="gradeSaveDiv"> 
          <div id="previousQualErrMsg" class="err_msg pq_err" style="display:none">
            <a href="#" class="err_cls" id="closeErrMsg" onclick="closeErrMsgFunc('previousQualErrMsg','quiz');">
              <img src="<%=CommonUtil.getImgPath("/wu-cont/img/icons/ic_grey_x.png", 0)%>" class="cls_ico" alt="" >
            </a>				
            <div id="pqerrmsg">
            </div>
          </div>			
          <div class="grd_sve">
            <ul><li><a id="previousQualSavBtn" href="#" onclick="getQuizDetails('done','','',${questionDetail.questionId});" class="btn_act cbbtn_dis">Save</a></li></ul>
          </div>									 						                   
        </div> 	
      </c:if>                         
        
      <%=quizQuestionSep%>${questionDetail.questionId}
      <%=quizQuestionSep%>${questionDetail.answerType}
      <%=quizQuestionSep%>${questionDetail.filterName}
      <%=quizQuestionSep%>${questionDetail.answerDisplayStyle}
      <%=quizQuestionSep%>${questionDetail.ctaButtonText} 
      <%=quizQuestionSep%>${requestScope.nextQuestionId}
      <%=quizQuestionSep%><%=subQuestionDetailSize%>
      <%=quizQuestionSep%>${questionDetail.progressPercent} 
      <%=quizQuestionSep%>${requestScope.courseCount} 
      <%=quizQuestionSep%>${questionDetail.questionName} 
      <%=quizQuestionSep%>${requestScope.keywordSearchSubject}
      <c:if test="${not empty requestScope.multipleNextQuestionId}">
           <%=quizQuestionSep%>${requestScope.multipleNextQuestionId}
       </c:if>
       <c:if test="${not empty requestScope.previousQualification}">
          <%=quizQuestionSep%>${requestScope.previousQualification} 
          <%=quizQuestionSep%>${requestScope.gradeStr}
          <%=quizQuestionSep%>${requestScope.gradeLevel}
        </c:if>              
   </c:forEach>
    </c:if>
<%=quizAnswerSep%>
<c:if test="${not empty requestScope.answerOptionsList}">
    <div id="searchAnswerOpt" style="display:none" >  
    <c:forEach var="answerOption" items="${requestScope.answerOptionsList}" varStatus="answerIndex">
        <c:if test="${not empty answerOption.commentAgainstAnswer}">
          <div id="answerOpt_${answerIndex.index}">${answerOption.commentAgainstAnswer}</div>
        </c:if>
      </c:forEach>  
      <input type="hidden" id="answerOptLength" value="<%=answerOptionsList.size()%>"/>
    </div>  
   
   <c:forEach var="questionDetail" items="${requestScope.questionDetailsList}" varStatus="index">  
      <c:if test="${questionDetail.filterName eq 'Qualification'}">
      <c:if test="${questionDetail.answerType eq 'SINGLE_SELECTION'}">
                                                             
        <div id="qualHidden" style="display:none">
        <c:forEach var="answerOption" items="${requestScope.answerOptionsList}" varStatus="answerIndex">                         
            <input type="hidden" id="qualHidden_${answerIndex.index}" value="${answerOption.optionText}|${answerOption.optionValue}|${answerOption.nextQuestionId}"/>
          </c:forEach>
        </div>
     </c:if>
     </c:if>
     </c:forEach>
</c:if>

<%=quizPrevQualSep%>
<c:if test="${not empty requestScope.previousQualList}">
    <div class="cht_bot cht_snd">
      <div class="grd_un grd_sec" id="question_previous_qual">
        <div class="deg_sel"> 
        <c:forEach var="qual" items="${requestScope.previousQualList}" varStatus="answerIndex">                                                                              
            <label id="qualSingleSel_${answerIndex.index}" onclick="selectSubjectGrade(this.id, 'grade');">
              <input type="radio" name="grade" value='${qual.gradeStr}'/>
              <c:choose>
              <c:when test="${answerIndex.index eq 0}">
                <span class="tp_rn">${qual.gradeStr}</span>
              </c:when>
              <c:otherwise>
              <c:set var="previousQualListSize" value="${requestScope.previousQualList.size()-1}"/>
              <c:choose>
              <c:when test="${answerIndex.index eq previousQualListSize}">
                <span class="bt_rn">${qual.gradeStr}</span>            
             </c:when>
             <c:otherwise>
               <span>${qual.gradeStr}</span>
              </c:otherwise>
              </c:choose>
              </c:otherwise>
              </c:choose>
            </label>                                                       
          </c:forEach>
          <c:if test="${not empty requestScope.tempQualGrades}">
              <input type="hidden" id="tempQualGrades1" value='${requestScope.tempQualGrades}'/>
          </c:if>
        </div>
      </div> 
    </div>
  </c:if> 
 

<%=quizMobileSkipSep%>
<%if(mobileFlag){%>
<c:if test="${not empty requestScope.skipExit}">
    <div id="skipAndViewResults" class="skpvw_res" style="display:none"> 								 
      <ul>        
        <li class="skp_cls">
          <a href="#" onclick="closeSkipDiv('skipAndViewResults');"><img src="<%=CommonUtil.getImgPath("/wu-cont/img/icons/Line.png", 0)%>"></a>
        </li> 
        <li>
          <a href="javascript:void(0);" onclick="getQuizDetails('${requestScope.skipToNext}','skpNxt','','getQuestionId')">Skip to next question</a>
        </li>
        <li>
          <a href="javascript:void(0);" onclick="getQuizDetails('${requestScope.skipExit}','skpExt','','getQuestionId')">
              <%-- ::START:: wu583_20181120 - Sabapathi: added this block to display course count with text  --%> 
         
              <c:if test="${not empty requestScope.courseCount}">
             
                 <c:if test="${requestScope.courseCount eq 1}">
                    Skip and view ${requestScope.courseCount} result
                  </c:if>
                  <c:if test="${requestScope.courseCount ne 1}">
                    Skip and view ${requestScope.courseCount} results
                  </c:if>
                </c:if>
              
              <%-- ::END:: wu583_20181120 - Sabapathi: added this block to display course count with text  --%>
          </a>
        </li>
      </ul>               
    </div> 
  </c:if>
<%}%>

<%=GlobalConstants.QUIZ_SELECTED_URL_SEP%>
<c:if test="${not empty requestScope.selectedFilterUrl}">${requestScope.selectedFilterUrl}</c:if>    