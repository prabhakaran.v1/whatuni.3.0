<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<c:if test="${not empty requestScope.qualSubjectAndCourse}">
    <div class="cht_sub" id="search_ul">
      <ul>
      <c:forEach var="course" items="${requestScope.qualSubjectAndCourse}" varStatus="courseIndex">
          <li id="li_${courseIndex.index}" onclick="saveSubject(this.id);">${course.description}
          </li>
        </c:forEach>				
      </ul>
    </div>
  <c:forEach var="course" items="${requestScope.qualSubjectAndCourse}" varStatus="courseIndex">
    <c:if test="${not empty course.searchCode}">
            <input type="hidden" id="course_list_${courseIndex.index}" value="${course.description}|${course.searchId}|${course.searchCode}"/>								   
      </c:if>
      <c:if test="${empty course.searchCode}">   
          <input type="hidden" id="course_list_${courseIndex.index}" value="${course.description}|${course.searchId}|"/>								         
       </c:if>
     </c:forEach>
</c:if>