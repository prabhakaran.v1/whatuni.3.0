<%--
  * @purpose:  'Quiz' widget search page..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 28-Nov-2017    Indumathi S               1.0      First draft           wu_571
  * *************************************************************************************************************************
--%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import=" WUI.utilities.CommonUtil, java.util.*, mobile.util.MobileUtils, org.apache.commons.validator.GenericValidator, WUI.profile.bean.ProfileBean, WUI.utilities.CommonFunction" %>

<%    
  ArrayList subQuestionDetails = request.getAttribute("subQuestionDetails") != null ? (ArrayList)request.getAttribute("subQuestionDetails") : null;
  boolean mobileFlag = new MobileUtils().userAgentCheck(request);
  String refererUrl = request.getHeader("referer");
  String jsPath = CommonUtil.getJsPath();
  String fromPlace = GenericValidator.isBlankOrNull(request.getParameter("from")) ? "false" : request.getParameter("from");
  String hideFooterIfPrepopulating = "style='display:block'";
  String removeLink =   CommonUtil.getImgPath("/wu-cont/images/ch_minus_blue.svg",0);
%>

<body>
  <c:if test="${not empty requestScope.questionDetailsList}">
    <c:forEach var="questionDetail" items="${requestScope.questionDetailsList}" varStatus="index">
        <div class="pages" id="chatbotWidget"> 
          <div class="cbot_cls" onclick="doGAlogOnExit();closeChatBox('<%=refererUrl%>');updateSessionForChatbotDisbaled();"><a href="javascript:void(0);"><i class="fa fa-times"></i></a></div>
          
          <div data-page="fav_blank" class="page quiz_page ch_ques">            
            <div class="navbar">       
              <div class="navbar-inner">
                <div class="center" >
                  <div class="pr_bar">
                    <div id="percentProgressBar" style="width:${questionDetail.progressPercent}%" class="pr_grn"></div>
                  </div>                 
                </div>
              </div>
            </div>
            
            <div class="page-content" id="quiz-cont">
              <div class="cht_cnt chbt_no" id="mainSection">
                
                <%if("true".equalsIgnoreCase(fromPlace)) {
                    String userNameText = "Hi there";
                    if(session.getAttribute("userInfoList") != null) {
                        ArrayList userInfoList = (ArrayList)session.getAttribute("userInfoList");
                        ProfileBean bean = null;
                        if(userInfoList != null && userInfoList.size() > 0) {
                          bean = (ProfileBean)userInfoList.get(0);
                          userNameText = "Hi "+bean.getForeName();
                        }
                    }
                    hideFooterIfPrepopulating = "style='display:none'";
                %>                  
                  
                  <!--Chat bot-->
                  <div class="cht_bot">
                      <div class="cht_ico"></div>
                      <div class="cht_msg cht_min">
                          <span id="questionTxt_10" style=""><%=userNameText%>, <spring:message code="chatbot.popup.first1.message.logged.in.user"/> <spring:message code="chatbot.popup.second.message"/> <spring:message code="chatbot.iframe.first.message"/>
              </span>
                      </div>
                  </div>
                  <input type="hidden" id="questionListSize" value="1">
                  <div class="cht_usr" id="answer_1">Yes</div>
                  <%--
                  <div class="cht_bot">
                      <div class="cht_ico"></div>
                      <div class="cht_msg cht_min">
                          <span id="questionTxt_130" style="">Great! Do you know what subject you want to study?</span>
                      </div>
                      <!--Subject search ajax-->
                      <!--Job or Industry search ajax-->
                      <!--select list-->
                      <!--multi select-->
                  </div>
                  --%>
                  
                <%} else {%>
                
                  <!--Chat bot-->  
                  <c:if test="${not empty requestScope.subQuestionDetails}">
                      <c:forEach var="subQuestionDetail" items="${requestScope.subQuestionDetails}" varStatus="subIndex">                        
                        <div class="cht_bot">
                          <div class="cht_ico"></div>
                          <c:choose>
                          <c:when test="${subIndex.index eq 0}">
                            <div class="cht_msg cht_min">
                              <img id="image_${questionDetail.questionId}${subIndex.index}" src="<%=CommonUtil.getImgPath("/wu-cont/images/cht_loader.gif", 0)%>" alt="" width="40">
                              <span id="questionTxt_${questionDetail.questionId}${subIndex.index}" style="display:none">${subQuestionDetail.questionText}</span>				                          
                            </div>
                          </c:when>
                          <c:otherwise>
                            <div class="cht_msg cht_min" id="questionTxt_${questionDetail.questionId}${subIndex.index}" style="display:none">
                              ${subQuestionDetail.questionText}${subIndex.index}																	
                            </div>
                         </c:otherwise>
                           </c:choose>
                        </div>  
                      </c:forEach>
                      <input type="hidden" id="questionListSize" value="<%=subQuestionDetails.size()%>"/>
                </c:if>              
                <%}%>
              </div>
            </div>
              
            <!-- Footer Section -->           
            <div  id="footerContent" class="foot_bar" <%=hideFooterIfPrepopulating%>>
              <div id="yesNoDiv" class="yesNoDiv">
                  <ul>
                  <c:if test="${questionDetail.answerDisplayStyle eq 'BUTTONS'}">
                     <c:if test="${not empty requestScope.answerOptionsList}">
                       <c:forEach var="answerOption" items="${requestScope.answerOptionsList}" varstatus="answerIndex">
                            <li><a class="btn_act btn_bor" href="javascript:void(0);" onclick="getQuizDetails('${answerOption.nextQuestionId}', '${answerOption.optionValue}', '${fn:escapeXml(answerOption.optionText)}', '${questionDetail.questionId}', '', 'yesNo');" >${fn:escapeXml(answerOption.optionText)}</a></li> 
                         </c:forEach>
                         </c:if>
                    </c:if>               
                  </ul>
              </div>        
            </div>           
          </div>
          <div style="display:none" id="answerOptionHidden">            
          </div>
         
          <form:form commandName="chatbotBean" action="/chatbot-widget">
            <form:hidden path="qualificationCode" id="qualification"/>
            <form:hidden path="categoryCode" id="categoryCode"/>
            <form:hidden path="previousQual" cssClass="entry-level" id="previousQual" />	
            <form:hidden path="previousQualGrades" id="previousQualGrades"/>
            <form:hidden path="qualTypeId" id="qualTypeId"/>	
            <form:hidden path="jacsCode" id="jacsCode"/>
            <form:hidden path="subject1_id" id="subject1"/> 
            <form:hidden path="subject2_id" id="subject2"/> 
            <form:hidden path="subject3_id" id="subject3"/> 
            <form:hidden path="subject4_id" id="subject4"/> 
            <form:hidden path="subject5_id" id="subject5"/>
            <form:hidden path="subject6_id" id="subject6"/>
            <form:hidden path="subj1_tariff_points" id="grade1"/>
            <form:hidden path="subj2_tariff_points" id="grade2"/>
            <form:hidden path="subj3_tariff_points" id="grade3"/>
            <form:hidden path="subj4_tariff_points" id="grade4"/>
            <form:hidden path="subj5_tariff_points" id="grade5"/>
            <form:hidden path="subj6_tariff_points" id="grade6"/> 
            <form:hidden path="questionName" id="questionName"/>            
            <form:hidden path="timeDeplay" id="timeDeplay"/>
            <form:hidden path="questionId" id="questionId"/>	
            <form:hidden path="nextQuestionId" id="nextQuestionId"/>
            <form:hidden path="answer" id="answer"/>
            <form:hidden path="selectedFilterUrl" id="selectedFilterUrl"/> 
          </form:form>
          
          <input type="hidden" id="searchCategoryId" value=""/>
          <input type="hidden" id="keywordSearch" value=""/>          
          <input type="hidden" id="qualificationName" value=""/>
          <input type="hidden" id="locationType" class="location-type" value=""/>
          <input type="hidden" id="locationTypeName" value=""/>
          <input type="hidden" id="locationTypeNameSearch" value=""/>
          <input type="hidden" id="region" class="location" value=""/>
          <input type="hidden" id="regionFlag" value=""/>	
          
          <input type="hidden" id="assessmentType" class="assessment-type" value=""/>
          <input type="hidden" id="assessmentTypeId" value=""/>
          <input type="hidden" id="studyMode" class="study-mode" value=""/>
          <input type="hidden" id="studyModeTextName" value=""/>
          <input type="hidden" id="reviewCategory" class="your-pref" value=""/>
          <input type="hidden" id="reviewCategoryName" value=""/>
          <input type="hidden" id="multipleNextQuestionId" value=""/>
         
          <input type="hidden" id="jobOrIndustryName" value=""/>		              
          <input type="hidden" id="filterName" value=""/>
          <input type="hidden" id="valueToBeShown" value=""/>
          <input type="hidden" id="filterFlag" value=""/>
          <input type="hidden" name="gradeCount" id="gradeCount" value=""/>
          <input type="hidden" name="pqSelectedGrades" id="pqSelectedGrades" class="entry-points" value=""/>
          <input type="hidden" name="numberOfGrades" id="numberOfGrades" value="1"/>				              
          <input type="hidden" name="pqSelToDisplay" id="pqSelToDisplay" value=""/>	
          <input type="hidden" id="previousQualSelected" value=""/>
        
          <input type="hidden" id="subjectQualName" value=""/>       
          <input type="hidden" id="tempQualGrades" value=""/>
          <input type="hidden" id="questionLength" value=""/>
          <input type="hidden" id="subjectQualKeyVal" value=""/> 
          <input type="hidden" id="TAKE_QUIZ" value=""/>
          <input type="hidden" id="searchCategoryCode" value=""/>
          <input type="hidden" id="courseCount" value=""/>
          <input type="hidden" id="mobileFlag" value="<%=mobileFlag%>"/>
          <input type="hidden" id="refererUrl" value="<%=refererUrl%>"/>
          <%-- Added below hiddens 1) From where navigated (Page), 2) How long user navigated to chatbot before exit 25_Sep_2018 By Sabapathi --%>
          <input type="hidden" id="fromPlace" name="fromPlace" value="<%=fromPlace%>"/>
          <input type="hidden" id="ga_Log_on_force_exit" name="ga_Log_on_force_exit" value=""/>
          <input type="hidden" id="removeLinkImg" value="<%=removeLink%>" />
        </div>                
            
        <script type="text/javascript" language="javascript">                          
          getIndexQuizDetails();                
        </script>
      </c:forEach>
  </c:if>
  
  <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
  <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" /> 
  <div id="loadingImg" style="display:none; position: absolute; z-index: 3;">
   <div style="top: 0;position: fixed;left: 0;background: rgba(0,0,0,0.5);width: 100%;height: 100%;">
    <img style="position: absolute; top: 50%; left: 50%; margin-top: -32px; margin-left: -32px;" src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/loader.gif",0)%>" alt="loading image"/>
    </div>
  </div>
</body>