<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<!--Job or Industry list -->
<c:if test="${not empty requestScope.jobIndustryAjaxList}">
  <div class="cht_sub" id="search_ul">
    <ul>
    <c:forEach var="job" items="${requestScope.jobIndustryAjaxList}" varStatus="jobIndex">
      <li id="li_${jobIndex.index}" onclick="saveJobOrIndustry(this.id);">${job.jobOrIndustryName}</li>	
    </c:forEach>			
    </ul>
  </div>
  <c:forEach var="job" items="${requestScope.jobIndustryAjaxList}" varStatus="jobIndex">
    <input type="hidden" id="job_list_${jobIndex.index}" value="${job.jobOrIndustryName}|${job.jobOrIndustryId}|${job.jobOrIndustryFlag}"/>								
  </c:forEach>
</c:if>