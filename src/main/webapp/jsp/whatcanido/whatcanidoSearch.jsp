<%--
  * @purpose:  'what can i do' widget search page..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 08-Mar-2016    Indumathi Selvam          1.0      First draft           wu_550
  * *************************************************************************************************************************
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@page import=" WUI.utilities.CommonUtil, java.util.*" %>
<%ResourceBundle rb = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources");  
  String ajaxDynamicListJSName = rb.getString("wuni.ajaxDynamicList.js");
  String widgetDoughnutJSName = rb.getString("wuni.widget.doughnut.chart.js");
  String widgetDrawDoughnutJSName = rb.getString("wuni.widget.draw.doughnut.js");
  String lazyLoadJs = rb.getString("wuni.lazyLoadJs.js");
  String cpeStatsLogJsName = CommonUtil.getResourceMessage("wuni.cpe.stats.log.js", null); 
%>

<body>
<%
 String qualificationCode = request.getAttribute("qualificationCode") != null ? (String)request.getAttribute("qualificationCode") : "";
 String qualificationTypeIdStr = request.getAttribute("qualificationTypeId") != null ? (String)request.getAttribute("qualificationTypeId") : "";
%>
<div id="whatcanidoWidget">
  <div id="whatcanidoHome">
    <div class="wcs_cont">
      <div class="wrapp wcs_ido">
        <a class="wcs_clse" id="close_NRW" onclick="closeWidgetLightBox();">   
        <i class="fa fa-times"></i></a>
        
        <div class="container1">
          <div id="typeform" class="wd_frm">
            <form>
              <div class="hdr fl mb0 b_div">
                <div class="lft fl"></div>
                <div class="mdc m0 fr">
                  <img src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/wcs_bubble.svg", 0)%>" class="fl bub" height="200px" alt="">
                  <div class="wcs_hdtit">
                    <div>Not sure which course to choose?</div>
                    <p>Enter your subjects and grades below and we'll give you suggested courses suitable for you.</p>
                  </div>
                </div>
              </div>
              <div class="hdr fl m0 mb_spce">
                <div class="lft fl"><p>1 <i class="fa fa-long-arrow-right"></i></p></div>
                <div class="mdc m0 fr">
                  <label class="w100p" >Your qualification</label>
                  <fieldset class="ucas_fldrw">
                    <div class="ucas_selcnt">
                      <div class="ucas_sbox">
                        <span class="sbox_txt" id="qualSpan"><%=qualificationCode%></span>
                        <i class="fa fa-angle-down fa-lg"></i>
                      </div>
                      <c:if test="${not empty requestScope.qualificationList}">
                          <select class="ucas_slist" id="qualsel" onchange="setSelectedVal(this, 'qualSpan');onChageQualGrades(this);" >	
                          <c:forEach var="qualifications" items="${requestScope.qualificationList}" varStatus="index">
                            
                              <option value="${qualifications.qualificationId}|${qualifications.qualificationTypeId}|${qualifications.qualification}">${qualifications.qualification}</option>     
       
                            </c:forEach>
                          </select>
                          <c:forEach var="grdQualifications" items="${requestScope.qualificationList}" varStatus="index">
                        <c:if test="${not empty grdQualifications.qualificationId}">
                           
                              <input type="hidden" id="${grdQualifications.qualificationId}" name="${grdQualifications.qualificationId}" value="${grdQualifications.qualificationGrades}" />
                              <input type="hidden" id="${grdQualifications.qualificationId}NoOfSub" value="${grdQualifications.defaultNoOfSubjects}"/>

                     </c:if>
                          </c:forEach>
 
                      </c:if>
                      <input type="hidden" id="qualTypeIdStr" value="<%=qualificationTypeIdStr%>">
                    </div>
                  </fieldset>
                </div>
              </div>
              <div class="hdr fl m0 wcs_hdr">
                <div class="lft fl"><p>2 <i class="fa fa-long-arrow-right"></i></p></div>
                <div class="mdc m0 fr">
                  <div class="ucas_row grd_row" id="clearTarrif">
                    <div class="wcs_subgrd">
                      <label class="wcs_sub">Your subjects</label>
                      <label class="wcs_grd">Your grades</label>
                    </div>
                    <!--Repeat Field start -->
                    <fieldset class="ucas_fldrw">
                      <div class="ucas_tbox">
                        <input type="text" id="wcidSub1" name="wcidSub1" onkeypress="return keypressSubmit(this, event);" onkeyup="wcidQualSubjectList(event, this);" placeholder="Enter subject here..." autocomplete="off" />
                        <input type="hidden" id="wcidSub1_hidden" name="wcidSub1_hidden" />                  
                      </div>
                      <div class="ucas_selcnt rsize_wdth">
                        <div class="ucas_sbox">
                          <span class="sbox_txt" id="gradeSpan1">A*</span>
                          <i class="fa fa-angle-down fa-lg"></i>
                        </div>
                        <select class="ucas_slist" name="setOptVal" id="setOptVal1" onchange="setSelectedVal(this, 'gradeSpan1');">										              
                          <c:if test="${not empty requestScope.qualGradeArr}">
              
                        <c:forEach var="qualGradeArr" items="${requestScope.qualGradeArr}" varStatus="index">
                            
                                <option value="${qualGradeArr.defaultGrades}">${qualGradeArr.defaultGrades}</option>
                            
                              </c:forEach>
                
                        
                          </c:if>
                        </select>
                      </div>
                    </fieldset>
                    <!--Repeat Field end -->
                    <!--Repeat Field start -->
                    <fieldset class="ucas_fldrw">
                      <div class="ucas_tbox">
                       <input type="text" id="wcidSub2" name="wcidSub2" onkeypress="return keypressSubmit(this, event);" onkeyup="wcidQualSubjectList(event, this);" placeholder="Enter subject here..." autocomplete="off" />
                       <input type="hidden" id="wcidSub2_hidden" name="wcidSub2_hidden" />
                      </div>
                      <div class="ucas_selcnt rsize_wdth">
                        <div class="ucas_sbox">
                          <span class="sbox_txt" id="gradeSpan2">A*</span>
                          <i class="fa fa-angle-down fa-lg"></i>
                        </div>
                        <select class="ucas_slist" name="setOptVal" id="setOptVal2" onchange="setSelectedVal(this, 'gradeSpan2');">										
                         <c:if test="${not empty requestScope.qualGradeArr}">
              
                     <c:forEach var="qualGradeArr" items="${requestScope.qualGradeArr}" varStatus="index">
                             
                                <option value="${qualGradeArr.defaultGrades}">${qualGradeArr.defaultGrades}</option>

                              </c:forEach>
                       
                    
                          </c:if>
                        </select>
                      </div>	
                    </fieldset>
                    <!--Repeat Field end -->
                    <!--Repeat Field start -->
                    <fieldset class="ucas_fldrw">
                      <div class="ucas_tbox">
                       <input type="text" id="wcidSub3" name="wcidSub3" onkeypress="return keypressSubmit(this, event);" onkeyup="wcidQualSubjectList(event, this);" placeholder="Enter subject here..." autocomplete="off" />
                       <input type="hidden" id="wcidSub3_hidden" name="wcidSub3_hidden" />
                      </div>
                      <div class="ucas_selcnt rsize_wdth">
                        <div class="ucas_sbox">
                          <span class="sbox_txt" id="gradeSpan3">A*</span>
                          <i class="fa fa-angle-down fa-lg"></i>
                        </div>
                        <select class="ucas_slist" name="setOptVal" id="setOptVal3" onchange="setSelectedVal(this, 'gradeSpan3');">										
                            <c:if test="${not empty requestScope.qualGradeArr}">
              
                     <c:forEach var="qualGradeArr" items="${requestScope.qualGradeArr}" varStatus="index">
                             
                                <option value="${qualGradeArr.defaultGrades}">${qualGradeArr.defaultGrades}</option>

                              </c:forEach>
                       
                    
                          </c:if>
                        </select>
                      </div>									
                    </fieldset>
                    <!--Repeat Field end -->
                  </div>
                  <div class="add_fld add_qualtxt"><a href="javascript:void(0);" id="addSubject"><i class="fa fa-plus"></i> Add subject</a></div>
                  <div id="gradesError" class="wcs_err" style="display:none"></div>
                  <div id="subjectError" class="wcs_err" style="display:none"></div>
                  </div>
                </div>
              <div class="hdr fl m0 sres_mb">
                <div class="lft fl"></div>
                <div class="mdc m0 fr" id="wcidSubmit">
                  <a href="javascript:void(0);" onclick="submitQualification();" class="btn_com bl_bg fl cwhite stp1">SEE YOUR RESULTS <i class="fa fa-long-arrow-right pdl20"></i></a>
                </div>          
              </div>
            </form>
          </div>
        </div>   
      </div>
    </div>
  </div>
  <div id="whatcanidoResults"></div>
  <div id="unicourseDiv"></div>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/DonutChart/<%=widgetDoughnutJSName%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/DonutChart/<%=widgetDrawDoughnutJSName%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=cpeStatsLogJsName%>"> </script>
  <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
  <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />
  <div class="wcs_load_icon" id="loadingIcon" style="display:none;">
    <img src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/wcs_load_icon.gif", 0)%>">
		<div class="load_txt">Loading... please wait</div>
  </div>
</div>
</body>


          