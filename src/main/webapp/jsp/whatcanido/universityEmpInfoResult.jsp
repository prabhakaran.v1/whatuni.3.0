<%--
  * @purpose:  jsp for showing uni results in what course i do widget..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 08-Mar-2016    Prabhakaran V.            1.0      First draft           wu_550
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="WUI.utilities.CommonUtil,  WUI.utilities.GlobalConstants"%>
<%String logId = request.getAttribute("logId") != null ? (String)request.getAttribute("logId") : "";
  String jacsCode = request.getAttribute("jacsCode") != null ? (String)request.getAttribute("jacsCode") : "";
  String tariffPoint = request.getAttribute("tariffPoint") != null ? (String)request.getAttribute("tariffPoint") : "";
  String entryPoints = request.getAttribute("entryPoints") != null ? (String)request.getAttribute("entryPoints") : "";
  String entryLevel = request.getAttribute("entryLevel") != null ? (String)request.getAttribute("entryLevel") : "";
  String tempImgPath = CommonUtil.getImgPath("/", 0);
  String jacsSubject = request.getAttribute("jacsSubject") != null ? (String)request.getAttribute("jacsSubject") : "";
  String employeeInfoFlag = request.getAttribute("employeeInfoFlag") != null ? (String)request.getAttribute("employeeInfoFlag") : "";

  String imgManPath =  tempImgPath + "wu-cont/images/widget/";
%>
<div class="wcs_cont">
  <div class="wrapp wcs_ido" style="margin-bottom:0;">
    <a class="wcs_clse" id="close_NRW" onclick="closeWidgetLightBox();"><i class="fa fa-times"></i></a>
    <div id="typeform" class="pag4">
      <div class="hdr hnew">
        <h1>Job and employment info by uni</h1>
        <div class="h2hdr w100p fl pdt15">
          <div class="wcs_emtst"><h2 class="fl">For <%=jacsSubject%> graduates</h2></div>
          <a onclick="goToPreviousPage();" class="btn_com ico fr">Search again</a>
						    <a onclick="goToBackPage();" class="btn_com ico fr wcs_sts_back">Back</a>
        </div>
      </div>
      <div class="gbg w100p fl wcs_srchbx">
<!--         Below code is University ajax auto complete -->
     	 	<div class="perf_srch">
          <span class="src_tbox">
            <input type="text" style="color: rgb(166, 166, 166);" name="uniName" id="uniName" class="uniip" onkeyup="uniList(event, this);" onkeypress="keypressUniSearchSubmit(event);" autocomplete="off" placeholder="Find a specific uni" />
          </span>
          <input type="hidden" id="uniName_hidden">
          <input type="hidden" id="uniName_ukprn">
          <input type="hidden" id="uniName_display">
          <input type="hidden" name="logId" id="logId" value="<%=logId%>"/>
          <input type="hidden" name="entryPoints" id="entryPoints" value="<%=entryPoints%>"/>
          <input type="hidden" name="entryLevel" id="entryLevel" value="<%=entryLevel%>"/> 
          <input type="hidden" name="jacsCode" id="jacsCode" value="<%=jacsCode%>"/>
          <input type="hidden" name="tariffPoint" id="tariffPoint" value="<%=tariffPoint%>"/>
          <span class="src_btn">
            <input type="button" class="uniclr" id="uniClear" onclick="getCollegeSearch('UNLOAD');" style="display:none;"/>
            <button id="uniLoad" onclick="getCollegeSearch('UNILOAD');" ><i class="fa fa-search"></i></button>
          </span>
          <div class="yrsb1" style="display:none; color:red;" id="errordiv"></div>
		      </div>
      </div>
<!--       End of the ajax code -->
      <div id="sortUniPod">
        <%if("Y".equalsIgnoreCase(employeeInfoFlag)){%>
          <jsp:include page="/jsp/whatcanido/include/whatcanidoSortingPod.jsp" />
        <%}%>
      </div>
      <div id="sortingPod" style="display:none;"></div><!-- Div for getting ajax response while pagination and sorting -->
    </div>
<!--     Below code is average stats pod -->
    <div class="ftr_pod w100p fl">
    <c:if test="${not empty requestScope.averageStatsList}">
 <c:forEach var="averageStatsList" items="${requestScope.averageStatsList}" varStatus="rowIndex">
     <c:set var="intIndexValue" value="${rowIndex.index}"/>
          <%String empDataNotAvail = "";
          String salDataNotAvail = "";%>
          <c:if test="${empty averageStatsList.empRate}">
            <%empDataNotAvail = "TRUE";%>
           </c:if>
           <c:if test="${empty averageStatsList.salaryRange}">
            <%salDataNotAvail = "TRUE";%>
          </c:if>
        <!--   Below code is Employement rate -->
          <%if(!("TRUE".equalsIgnoreCase(empDataNotAvail) && "TRUE".equalsIgnoreCase(salDataNotAvail))){%>
            <div class="ftr">
              <div class="btn_ftr"><a class="cwhite btn_com fl" id="btn_uk" onclick="showUKpod();">UK Average</a></div>  
              <div class="wbg w100p fl pd30" id="tgldiv">
                <a><img class="ico_ftr" src='<%=CommonUtil.getImgPath("/wu-cont/images/widget/stats_icon.svg", 1)%>' alt="stats"/></a>
                <div class="w80p fr ex_cntr">
                  <div class="cont_hide " id="ratePod">
                    <h3 class="w100p cwhite"><%=jacsSubject%> stats</h3>
                    <c:if test="${not empty averageStatsList.empRate}">
                    <c:set var="subStickManDef" value="${averageStatsList.empRate}"/>
                      <%String empRateVal = pageContext.getAttribute("subStickManDef").toString();
                      if("INSUFFICIENT DATA".equalsIgnoreCase(empRateVal) || "DATA NOT AVAILABLE".equalsIgnoreCase(empRateVal)){%>
                        <h4 class="w100p pdt30 fnt17 cwhite fl">Graduate employment and further study <span class="cr">${averageUKStatsList.empRate}</span></h4>
                      <%}else{%>
                      <c:if test="${not empty averageStatsList.empRateColor}">
                 
                          <h4 class="w100p pdt30 fnt17 cwhite fl">Graduate employment and further study 
                          <span class="cr">
                            <span class="${averageStatsList.empRateColor} pdt30">${averageStatsList.empRate} out of 10</span> &nbsp;
                            <span class="scr_tltip " onclick="showToolTip('toolTipSub2<%=pageContext.getAttribute("intIndexValue").toString()%>')" onmouseover="showToolTip('toolTipSub2<%=pageContext.getAttribute("intIndexValue").toString()%>')" onmouseout="hideToolTip('toolTipSub2<%=pageContext.getAttribute("intIndexValue").toString()%>')">
                              <i class="fa fa-question-circle fr fnt18" id="toolTipDiv3"></i> 
                              <span class="scr_tip" id="toolTipSub2<%=pageContext.getAttribute("intIndexValue").toString()%>">
                                Approximately ${averageStatsList.empRate} out of 10 graduates who studied <%=jacsSubject%> at a UK university are employed, due to start work or continued into further study within 6 months of graduating. <br></br><%=GlobalConstants.HESA_SOURCE%>                                
                              </span>
                            </span>
                          </span>
                        </h4>
                     <!--    Code for employement rate sticky man -->
                        <c:set var="stickManColor" value="${averageStatsList.empRateColor}"/>
                        <%	
                        String imgPath = imgManPath + pageContext.getAttribute("stickManColor").toString() + "_ico.png";
                        
                        float figSize = Float.parseFloat(empRateVal);%>
                        <ul class="rat_ico">
                          <%for(int i = 1; i<figSize ; i++) {%>
                            <li><img src="<%=imgPath%>" alt="full stickMan"/></li>
                          <%}
                          if(empRateVal.indexOf(".5") >= 0){
                            imgPath = imgManPath + pageContext.getAttribute("stickManColor").toString() + "_gry.png";%>
                            <li><img src="<%=imgPath%>" alt="half stickMan"></li>
                          <%}
                          float remain = 10.0f - figSize;
                          for(int lp = 1; lp < remain; lp++){%>
                            <li><img src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/gry_icon.png", 0)%>" alt="half stickMan"></li>
                          <%}%>
                        </ul>
                     <!--    End of sticky man code
         -->
                      </c:if>
                    <%}%>
                  </c:if>
                 <!--  End of employement rate code
                  Code for salary range -->
                  <c:if test="${not empty averageStatsList.salaryRange}">
                 <c:set var="subSalaryRangeDef" value="${averageStatsList.salaryRange}"/>
                    
                    <%if("INSUFFICIENT DATA".equalsIgnoreCase(pageContext.getAttribute("subSalaryRangeDef").toString()) || "DATA NOT AVAILABLE".equalsIgnoreCase(pageContext.getAttribute("subSalaryRangeDef").toString())){%>
                      <h4 class="w100p pdt30 fnt18 cwhite fl">Average salary 
                        <span class="cr">
                          ${averageStatsList.averageStatsList}
                        </span>
                      </h4>
                    <%}else{%>
                      <h4 class="w100p pdt30 fnt18 cwhite fl">Average salary 
                        <span class="${averageStatsList.salaryRangeColor} fr">${averageStatsList.salaryRange}
                          <span class="scr_tltip " onclick="showToolTip('toolTipSub1<%=pageContext.getAttribute("intIndexValue").toString()%>')" onmouseover="showToolTip('toolTipSub1<%=pageContext.getAttribute("intIndexValue").toString()%>')" onmouseout="hideToolTip('toolTipSub1<%=pageContext.getAttribute("intIndexValue").toString()%>')">
                            <i class="fa fa-question-circle fr fnt18" id="toolTipDiv4"></i>
                            <span class="scr_tip" id="toolTipSub1<%=pageContext.getAttribute("intIndexValue").toString()%>">
                              Students who studied <%=jacsSubject%> at a UK university typically earn ${averageStatsList.salaryRange} six months after graduating. <br></br><%=GlobalConstants.HESA_SOURCE%>
                            </span>
                          </span>
                        </span>
                      </h4>
                    <!--   Code for caluating the salary range -->
                      <c:set var="avgSalaryBarRanges" value="${averageStatsList.salaryBarWidth}"/>
                      
                      <%String barDifff = pageContext.getAttribute("avgSalaryBarRanges").toString();
                   
                      String barDiff[] = barDifff.split("-");
                      String barLeft = "0";
                      float barWidth = 0.0f;
                      if(barDiff.length == 2){
                        barLeft = barDiff[0];
                        barWidth = Float.parseFloat(barDiff[1]) - Float.parseFloat(barLeft);
                        if(barWidth == 0){
                          barWidth = 1.0f;
                          if(Float.parseFloat(barLeft) < 1.0f){
                            barLeft = "1";
                          }
                        }
                      }%>
                    <!--   End of the bar range calculation code -->
                      <div class="pbar fl"><div class="p${averageStatsList.salaryRangeColor} fl" style="width:<%=barWidth%>%; margin-left:<%=barLeft%>%;"></div></div>
                      <p class="lmh w100p fl">
                        <span class="fl">Low</span>
                        <span>Medium</span>
                        <span class="fr">High</span>
                      </p>
                    <%}%>
          
                  </c:if>
                 <!--  End of salary range code
                  Code for job and industry donut chart -->
                  <div class="expnd_rw expnd_btn">
							             <a class="btn_com fr mt20 bl_bg cwhite" onclick="hideShowAvgPod('jobIndustryStats');">See what they do now</a>
						        
						            </div>
                </div>
                <div class="cont_hide" id="jobIndustryStats" style="display:none;">
                  <h3 class="w100p cwhite">Overall industry stats</h3>
                  <div class="expnd_rw">
						              <div class="exp_cnt" id="exp_cnt">
						                <ul>
								                <li><a id="intLink" class="act" onclick="hideAvgStats('INDUSTRY');">INDUSTRIES</a></li>
                        <li><a id="jobLink" onclick="hideAvgStats('JOB');">JOBS</a></li>
							               </ul>
                   <!--    Code for industry stats -->
                      <c:if test="${not empty requestScope.industryInfoList}">
                  
                  	        
                          <div class="dnut_cnr" id="avgDnut_cnr1">
                            <div class="chartbox fl w100p">
                            <c:forEach var="industryStatsList" items="${requestScope.industryInfoList}" varStatus="index">
                            <c:set var="indeIntValue" value="${index.index}"/>
                                <div class="dnt_pod">
                                  <div id="doughnutChart_graduates_stats-1<%= pageContext.getAttribute("indeIntValue").toString()%>" class="chart fl">
                                     <span>${industryStatsList.industryDescription}</span>
                                  </div>
                                  <input type="hidden" name='graduates_<%=pageContext.getAttribute("indeIntValue").toString()%>' id='graduates_stats-1<%=pageContext.getAttribute("indeIntValue").toString()%>' value="${industryStatsList.industryWisePerncentage}%" />
                                  <script type="text/javascript"  id="script_graduates_stats-1<%=pageContext.getAttribute("indeIntValue").toString()%>">                            
                                    drawSubjectdoughnutchart('graduates_stats-1<%=pageContext.getAttribute("indeIntValue").toString()%>','doughnutChart_graduates_stats-1<%=pageContext.getAttribute("indeIntValue").toString()%>', '${industryStatsList.industryColour}');
                                  </script>
                                </div>
                               
                              </c:forEach>
                            </div>
                          </div>
                   
               
                      </c:if>
                    <!--   End of industry stats code
                      Code for job stats -->
                      <c:if test="${not empty  requestScope.jobInfoList}">    
                                     
                          <div class="dnut_cnr" id="avgDnut_cnr2" style="display:none;">
                            <div class="chartbox fl w100p">
                            <c:forEach var="jobStatsList" items="${requestScope.jobInfoList}" varStatus="index">
                               <c:set var="indIntValue" value="${index.index}"/>
                                <div class="dnt_pod">
                                  <div id="doughnutChart_graduates_stats-2<%=pageContext.getAttribute("indIntValue").toString()%>" class="chart fl">
                                    <span>${jobStatsList.jobDescription}</span>
                                  </div>
                       
                                  <input type="hidden" name='graduates_<%=pageContext.getAttribute("indIntValue").toString()%>' id='graduates_stats-2<%=pageContext.getAttribute("indIntValue").toString()%>' value="${jobStatsList.jobWisePercentage}%" />
                                  <script type="text/javascript"  id="script_graduates_stats-2<%=pageContext.getAttribute("indIntValue").toString()%>">                            
                                    drawSubjectdoughnutchart('graduates_stats-2<%=pageContext.getAttribute("indIntValue").toString()%>','doughnutChart_graduates_stats-2<%=pageContext.getAttribute("indIntValue").toString()%>', '${jobStatsList.jobColour}');
                                  </script>
                                </div>
      
                              </c:forEach>
                            </div>
                          </div>
                 
                      
                      </c:if><!-- 
                      End of job stats code
                     --></div>
                  </div> 
                  <div class="expnd_rw expnd_btn">
						              <a class="btn_com fr mt20 bl_bg cwhite ico" onclick="hideShowAvgPod('ratePod');">Back</a>
				              </div>
                </div>
              <!--   End of the donut chart code -->
              </div>
            </div>
          </div>
        <%}%>
        </c:forEach>
      </c:if>
    </div>
  <!--   End of average stats pod -->
  </div>
</div>
