<%--
  * @purpose:  jsp for showing search results in what can i do..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 08-Mar-2016    Indumathi Selvam          1.0      First draft           wu_550
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="WUI.utilities.GlobalConstants,org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction" %>

<%
  String logId = request.getAttribute("logId") != null ? (String)request.getAttribute("logId") : "";
  String gaAccount = new WUI.utilities.CommonFunction().getWUSysVarValue("WU_GA_ACCOUNT");
  String messageCode = request.getAttribute("messageCode") != null ? (String)request.getAttribute("messageCode") : "0";
  String messagePercentage = request.getAttribute("messagePercent") != null ? (String)request.getAttribute("messagePercent") : "";
  String qualDispalyName = request.getAttribute("qualification") != null ? (String)request.getAttribute("qualification") : "";  
  String entryLevel = request.getAttribute("entryLevel") != null ? (String)request.getAttribute("entryLevel") : ""; 
%>
<div class="wcs_cont">
  <div class="wrapp wcs_ido">
    <a class="wcs_clse" id="close_NRW" onclick="closeWidgetLightBox();">
    <i class="fa fa-times"></i></a>
    <div class="container1">
      <div id="typeform" class="pag2">
        <div class="hdr">
          <h1>Your matches </h1>
           <div class="h2hdr w100p fl pdt15">
           <c:if test="${not empty requestScope.gradesSubjectList}">
      
            
                <div class="wcs_emtst">
                  <h2 class="fl">
                    ${requestScope.gradesSubjectList}
                  </h2>
                </div>
             
      
            </c:if>
            <a onclick="goToPreviousPage();" class="btn_com ico fr">Search again</a>
          </div>
          <c:if test="${not empty requestScope.resultsFlag}">
          <c:if test="${requestScope.resultsFlag eq 'S'}">
  <c:if test="${not empty requestScope.jacsSubjectsList}">
           <c:if test="${not empty requestScope.jacsSubjectsList}">
               
                  <p class="not_eno_txt">
                  <c:if test="${requestScope.returnCodeTwoHeadingText}">
                   
                     Not enough students studied <span>${requestScope.returnCodeTwoHeadingText}</span>. 
                   <c:if test="${requestScope.returnCodeTwoCloseMatch}"> Our closest match is for students who studied <span>${requestScope.returnCodeTwoCloseMatch}</span>.</c:if></c:if>
                  </p>
                
                  </c:if>
             
        </c:if>
            </c:if>
            <c:if test="${requestScope.resultsFlag eq 'N'}">
           
              <p class="not_eno_txt">
                We're sorry to say it, but in the last two years there haven't been enough people who have gone to uni with 
                <span>${requestScope.returnCodeTwoHeadingText}</span> 
                for us to provide any meaningful results. You're clearly a special and unique individual - try changing your grades to find a match or, alternatively, you could try entering fewer subjects.
                </p>
     
            </c:if>
      
          </c:if>
          <c:if test="${not empty requestScope.resultsFlag}">
    <c:if test="${requestScope.resultsFlag eq 'F'}">
            
					    <div class="wcs_permtch"><i class="fa fa-check"></i> PRODUCED A PERFECT MATCH</div>
     
            </c:if> 
 
         </c:if> 
        </div>
        <!--Section-->
        <c:if test="${not empty requestScope.jacsSubjectsList}">
        
       <c:forEach var="jacsSubjectsList" items="${requestScope.jacsSubjectsList}" varStatus="rowIndex">
          <c:set var="indexIntValue" value="${rowIndex.index}"/>
              <div class="wbg w100p fl pd30">
                <h3 class="w100p">${jacsSubjectsList.jacsSubject} <span class="wcs_cousc">(${jacsSubjectsList.jacsQualification})</span> 
                 <span class="ttip_wcs">
                 <span class="scr_tltip fr" id="toolTipDiv" onclick="showToolTip('toolTip<%=pageContext.getAttribute("indexIntValue").toString()%>')" onmouseover="showToolTip('toolTip<%=pageContext.getAttribute("indexIntValue").toString()%>')" onmouseout="hideToolTip('toolTip<%=pageContext.getAttribute("indexIntValue").toString()%>')"><i class="fa fa-question-circle fr" id="toolTipDiv1"></i>
                    <span class="scr_tip" id="toolTip<%=pageContext.getAttribute("indexIntValue").toString()%>" style="display:none"><strong>
                     <c:if test="${not empty jacsSubjectsList.jacsPercentage}">
                      
                        ${jacsSubjectsList.jacsPercentage}
                      
                      </c:if>
                      </strong> of students who studied 
                      <c:if test="${not empty requestScope.subjectsNameList}">
                  
                      ${requestScope.subjectsNameList}
                     
                      </c:if>
                      at sixth form, went on to study 
                       <c:if test="${not empty jacsSubjectsList.jacsSubject}">
                     
                        ${jacsSubjectsList.jacsSubject}
                      </c:if> at university.</span>
                    </span> 
                 <span class="fr ${jacsSubjectsList.wcidColor}">${jacsSubjectsList.jacsPercentage}</span>
                </span>
                </h3>
                <!--Progress bar-->
                <div class="pbar fl">
                  <div class="p${jacsSubjectsList.wcidColor} fl" style="width:${jacsSubjectsList.jacsPercentage}"></div>
                </div>
                <!--Progress bar-->
                <c:if test="${not empty jacsSubjectsList.jacsPercentage}">
                <c:set var="jacsCode" value="${jacsSubjectsList.jacsCode}"/>
                 
                  <a onclick="getJobInfo('${jacsSubjectsList.jacsCode}', '<%=logId%>', '<%=qualDispalyName%>', '${jacsSubjectsList.searchGrades}', '${jacsSubjectsList.equivalentTarriffPoints}');" class="btn_com fl mt20">Job info</a>
                  <a onclick="ulGAEventTracking('WCID', 'VD', '${jacsSubjectsList.jacsSubject}', '<%=gaAccount%>'); ulSearchResults('${jacsSubjectsList.jacsSubject}', '${jacsSubjectsList.jacsCode}', '${jacsSubjectsList.searchGrades}', '<%=entryLevel%>')" class="btn_com fr mt20 bl_bg cwhite">View degrees</a>
              
                </c:if>
              </div> 
          
            </c:forEach>
             <%if("2".equalsIgnoreCase(messageCode)){%>
              <div class="wbg w100p fl pd30 wcs_oth_sub">
                 The remaining <%=messagePercentage%>% of students studied a variety of other subjects.
              </div>
            <%}%>
       
        
        </c:if>
      </div>
    </div>
  </div>
</div>	

 