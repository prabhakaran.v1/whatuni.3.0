<%--
  * @purpose:  jsp for showing job industry donut chart in what course i do widget..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 08-Mar-2016    Prabhakaran V.            1.0      First draft           wu_550
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%String collegeId = request.getAttribute("collegeId") != null ? (String)request.getAttribute("collegeId") : "";%>
<ul>
<c:if test="${not empty requestScope.industryStatsList}">
      <li><a id="industryLinks<%=collegeId%>" href="javascript:void(0);" class="act" onclick="hideJobIndustry('INDUSTRY', '<%=collegeId%>');">INDUSTRIES</a></li>
  </c:if>
  <c:if test="${not empty requestScope.jobStatsList}">
      <li><a id="jobLinks<%=collegeId%>" href="javascript:void(0);" class="" onclick="hideJobIndustry('JOB', '<%=collegeId%>');">JOBS</a></li>
  </c:if>
</ul>
<c:if test="${not empty requestScope.industryStatsList}">

    <div class="dnut_cnr" id="dnut_cnr_1_<%=collegeId%>">
      <h4 class="w100p pdt30 fnt18 fl">Top 3 industries </h4>
      <div class="chartbox fl w100p">
      <c:forEach var="industryStatsList" items="${requestScope.industryStatsList}" varStatus="index">
      <c:set var="indexIntValue" value="${index.index}"/>
        <div class="dnt_pod">
          <div id="doughnutChart_graduates_<%=collegeId%><%=pageContext.getAttribute("indexIntValue").toString()%>" class="chart fl">
            <span>${industryStatsList.industryDescription}</span>
          </div>
          <input type="hidden" name='graduates_<%=pageContext.getAttribute("indexIntValue").toString()%>' id='graduates_<%=collegeId%><%=pageContext.getAttribute("indexIntValue").toString()%>' value="${industryStatsList.industryWisePerncentage}%" />
          <script type="text/javascript"  id="script_graduates_<%=collegeId%>_chart-1-<%=pageContext.getAttribute("indexIntValue").toString()%>">                            
            drawSubjectdoughnutchart('graduates_<%=collegeId%><%=pageContext.getAttribute("indexIntValue").toString()%>','doughnutChart_graduates_<%=collegeId%><%=pageContext.getAttribute("indexIntValue").toString()%>', '${industryStatsList.industryColour}');
          </script>
          </div>
        </c:forEach>
      </div>
    </div>

</c:if>
<c:if test="${not empty requestScope.jobStatsList}">
    <div class="dnut_cnr" id="dnut_cnr_2_<%=collegeId%>" style="display:none;">
      <h4 class="w100p pdt30 fnt18 fl">Top 3 jobs </h4>
      <div class="chartbox fl w100p">
      <c:forEach var="jobStatsList" items="${requestScope.jobStatsList}" varStatus="index">
      <c:set var="indIntvalue" value="${index.index}"/>
        <div class="dnt_pod">
          <div id="doughnutChart_graduates_<%=collegeId%>-chart-2<%=pageContext.getAttribute("indIntvalue").toString()%>" class="chart fl">
            <span>${jobStatsList.jobDescription}</span>
          </div>
          <input type="hidden" name='graduates_<%=pageContext.getAttribute("indIntvalue").toString()%>' id='graduates_<%=collegeId%>-chart-2<%=pageContext.getAttribute("indIntvalue").toString()%>' value="${jobStatsList.jobWisePercentage}%" />
          <script type="text/javascript"  id="script_graduates_<%=collegeId%>_chart-2-<%=pageContext.getAttribute("indIntvalue").toString()%>">                            
            drawSubjectdoughnutchart('graduates_<%=collegeId%>-chart-2<%=pageContext.getAttribute("indIntvalue").toString()%>','doughnutChart_graduates_<%=collegeId%>-chart-2<%=pageContext.getAttribute("indIntvalue").toString()%>', '${jobStatsList.jobColour}');
          </script>
          </div>
      
        </c:forEach>
      </div>
    </div>

</c:if>
