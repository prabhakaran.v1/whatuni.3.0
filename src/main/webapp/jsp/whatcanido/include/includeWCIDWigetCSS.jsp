<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction" %>

<% String envName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
   String widgetCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.whatcanido.widget_main.css");
   String domainSpecPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
  if(("LIVE").equals(envName)){
 %>
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=widgetCssName%>" media="screen" />
<%} else if("TEST".equals(envName)){%>
  <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=widgetCssName%>" media="screen" />
<%} else if("DEV".equals(envName)){%>
  <link rel="stylesheet" type="text/css" href="<%=domainSpecPath%>/wu-cont/cssstyles/<%=widgetCssName%>" media="screen" />
<%}%>