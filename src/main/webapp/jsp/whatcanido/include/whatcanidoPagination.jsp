<%--
  * @purpose:  jsp include for what course i do widget result pagination..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 08-Mar-2016    Prabhakaran V.            1.0      First draft           wu_550
  * *************************************************************************************************************************
--%>
<%
  int pageno = 1;
  int pagelimit = 0;
  int searchhits = 0;
  int recorddisplay = 0;
  int numofpages = 0;
  int pagenum = 0;
  int lower = 0;
  int upper = 0;
  String sortingOrder = null;
  String sortingType = null;
  // Reads the In Parameter from the JSP include action and assign to the respective variable
  if(request.getParameter("pageno") != null){
    pageno = Integer.parseInt(request.getParameter("pageno"));
  }
  if(request.getParameter("pagelimit") != null){
    pagelimit = Integer.parseInt(request.getParameter("pagelimit"));
  }
  if(request.getParameter("searchhits") != null){
    searchhits = Integer.parseInt(request.getParameter("searchhits").replaceAll(",", ""));
  }
  if(request.getParameter("recorddisplay") != null){
    recorddisplay = Integer.parseInt(request.getParameter("recorddisplay"));
  }
  if(request.getParameter("sortingOrder") != null){
    sortingOrder = request.getParameter("sortingOrder");
  }
  if(request.getParameter("sortingType") != null){
    sortingType = request.getParameter("sortingType");
  }
  String sortingFlag = "F";
 //displaying pagination only if(searchhits > recorddisplay)
 if(searchhits > recorddisplay){
 // variable which will hold dynamically-generated-pagination-related-content.      
 StringBuffer paginationContent = new StringBuffer();   
 //some calculation
 numofpages = (searchhits / recorddisplay);
 if(searchhits % recorddisplay > 0) {  numofpages++; }
 pagenum = (int)((pageno - 1) / pagelimit);
 pagenum =  pagenum + 1;
 lower = ((pagenum-1) * pagelimit) + 1;
 upper = java.lang.Math.min(numofpages, (((pageno - 1) / pagelimit) + 1) * pagelimit);
 if((pageno%pagelimit) == 0){
   lower = pageno;
   upper = java.lang.Math.min(numofpages, pageno+pagelimit);
 }
 paginationContent.append("<div class='h2hdr w100p fl pdt10 mb60'>");
 if(pageno > 1){
     paginationContent.append("<a rel='follow' onclick=\"getSortingResult(");
     paginationContent.append(String.valueOf(pageno-1)+", '"+sortingType+"', '"+sortingOrder+"', '"+sortingFlag+"'");
     paginationContent.append(")\" class='btn_com ico fl' title='Previous'>Previous</a>");          
 }    
 if(pageno < numofpages){
   paginationContent.append("<a rel='follow' onclick=\"getSortingResult(");
   paginationContent.append(String.valueOf(pageno+1)+",'"+sortingType+"', '"+sortingOrder+"', '"+sortingFlag+"'");
   paginationContent.append(")\" class='btn_com ico1 fr' title='Next page'>Next</a>");
 }
 paginationContent.append("</div>");
 out.println(paginationContent.toString());
}  
%>
