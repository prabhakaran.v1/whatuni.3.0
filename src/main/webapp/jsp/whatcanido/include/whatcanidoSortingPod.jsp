<%-- 
  * @purpose:  jsp for showing uni results in what course i do widget..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 08-Mar-2016    Prabhakaran V.            1.0      First draft           wu_550
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.wuni.util.seo.SeoUrls, WUI.utilities.CommonUtil,  WUI.utilities.GlobalConstants"%>
<%String jacsCode = request.getAttribute("jacsCode") != null ? (String)request.getAttribute("jacsCode") : "";
  String entryPoints = request.getAttribute("entryPoints") != null ? (String)request.getAttribute("entryPoints") : "";
  String entryLevel = request.getAttribute("entryLevel") != null ? (String)request.getAttribute("entryLevel") : "";
  String ukprn = request.getAttribute("ukprn") != null ? (String)request.getAttribute("ukprn") : "";
  String collegeId = request.getAttribute("collegeId") != null ? (String)request.getAttribute("collegeId") : "";
  String sortingOrder = (String)request.getAttribute("sortingOrder") != null ? (String)request.getAttribute("sortingOrder") : "D";
  String sortingType = (String)request.getAttribute("sortingType") != null ? (String)request.getAttribute("sortingType") : "EMP_RATE";
  String totalCount = request.getAttribute("totalCount") != null ? (String)request.getAttribute("totalCount") : "";
  String tempImgPath = CommonUtil.getImgPath("/", 0);
  String pageNo = request.getAttribute("pageNo") != null ? (String)request.getAttribute("pageNo") : "1";
  String jacsSubject = request.getAttribute("jacsSubject") != null ? (String)request.getAttribute("jacsSubject") : "";
  String gaAccount = new WUI.utilities.CommonFunction().getWUSysVarValue("WU_GA_ACCOUNT");
  String recorddisplay = "20";
  String pagelimit = "10";
  String url = GlobalConstants.WHATUNI_SCHEME_NAME+request.getServerName();
  String salClass = ""; //Adding class for selected sort by which is salary
  String empClass = ""; //Adding class for selected sort by which is emp rate
  String salAscending = ""; //Adding class for asending & descending arrow for salary
  String empAscending = ""; //Adding class for asending & descending arrow for emp rate
  String salarySort = "";
  String empSort = "";
  if("SALARY".equalsIgnoreCase(sortingType)){
    salClass = "active";
    salarySort = sortingOrder;
    if("A".equalsIgnoreCase(sortingOrder)){
      salAscending = "up";
    }else{
      salAscending = "down";
    }
  }
  if("EMP_RATE".equalsIgnoreCase(sortingType)){
    empClass = "active";
    empSort = sortingOrder;
    if("A".equalsIgnoreCase(sortingOrder)){
      empAscending = "up";
    }else{
      empAscending = "down";
    }
  }
  String imgManPath =  tempImgPath + "wu-cont/images/widget/";
%>
<div class="gbg w100p fl">
  <p class="fnt16">Sort by</p>
  <a id="salaryTab" onclick="getSortResult('SALARY', '<%=salarySort%>', '<%=collegeId%>', '<%=ukprn%>');" class="btn_com fr <%=salClass%>">Salary<i class="fa fa-angle-<%=salAscending%> fr pdi"></i></a>
  <a id="empRateTab" onclick="getSortResult('EMP_RATE', '<%=empSort%>', '<%=collegeId%>', '<%=ukprn%>');" class="btn_com fr <%=empClass%>">Employment rate<i class="fa fa-angle-<%=empAscending%> fr pdi"></i></a>
</div>
<c:if test="${not empty requestScope.uniInfoList}">
 <c:forEach var="uniInfoList" items="${requestScope.uniInfoList}" varStatus="rowIndex">
 <c:set var="indexIntValue" value="${rowIndex.index}"/>
  <c:if test="${not empty uniInfoList.collegeNameDisplay}">            
        <div class="wbg w100p fl pd30">
        <c:if test="${not empty uniInfoList.subjectGuideURL}">
       
            <a onclick="openNewWindow('<%=url%>${uniInfoList.subjectGuideURL}')" >
            <c:if test="${not empty uniInfoList.imagePath}">
            <c:set var="imgLogo" value ="${uniInfoList.imagePath}"/>
             
                <%String logo =  tempImgPath + pageContext.getAttribute("imgLogo").toString();
                if(Integer.parseInt(pageContext.getAttribute("indexIntValue").toString()) <= 3){%>
                  <img src="<%=logo%>" alt="${uniInfoList.collegeNameDisplay}"/>
                <%}else{%>
                  <img class="lazy-load" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src="<%=logo%>" alt="${uniInfoList.collegeNameDisplay}"/>
                <%}%>
              </c:if>
            </a>
  
          </c:if>
          <div class="w80p fr">
            <h3 class="w100p">${uniInfoList.collegeNameDisplay}</h3>
        <!--     Below code is Employment rate -->
            <c:if test="${not empty uniInfoList.empRate}">
    <c:if test="${not empty uniInfoList.empRateColor}">
             
              <c:set var="empRateDef" value="${uniInfoList.empRate}"/>
         <!--        Employment rate tooltip text changed for 24_Jan_2017, By Thiyagu G -->
                <%if("INSUFFICIENT DATA".equalsIgnoreCase(pageContext.getAttribute("empRateDef").toString()) || "DATA NOT AVAILABLE".equalsIgnoreCase(pageContext.getAttribute("empRateDef").toString())){%>
                  <h4 class="w100p pdt30 fnt18 pr">Graduate employment and further study <span class="fr">${uniInfoList.empRate}</span></h4>
                <%}else{%>
                  <h4 class="w100p pdt20 fnt18"> Graduate employment and further study 
                    <span class="cr">
                      <span class="${uniInfoList.empRateColor}">${uniInfoList.empRate} out of 10</span> 
                      <span class="scr_tltip" onclick="showToolTip('toolTipSub4<%=pageContext.getAttribute("indexIntValue").toString()%>')" onmouseover="showToolTip('toolTipSub4<%=pageContext.getAttribute("indexIntValue").toString()%>')" onmouseout="hideToolTip('toolTipSub4<%=pageContext.getAttribute("indexIntValue").toString()%>')">
                        <i class="fa fa-question-circle fr fnt18" id="toolTipDiv5"></i>
                        <span class="scr_tip" id="toolTipSub4<%=pageContext.getAttribute("indexIntValue").toString()%>">
                          ${uniInfoList.empRate} out of 10 graduates who studied at ${uniInfoList.collegeNameDisplay} are employed, due to start work or continued into further study within 6 months of graduating. <br></br><%=GlobalConstants.HESA_SOURCE%>
                        </span>                     
                      </span>
                    </span>
                  </h4>
                 <!--  Code for drawing employement rate sticky man -->
                  <c:set var="stickManDef" value="${uniInfoList.empRate}"/>
                  <c:set var="stickManColorDef" value="${uniInfoList.empRateColor}"/>
                  <%String imgPath = imgManPath + pageContext.getAttribute("stickManColorDef").toString() + "_ico.png";
                  float figSize = Float.parseFloat(pageContext.getAttribute("stickManDef").toString());
                  float remain = 10.0f - figSize;
                  if(Integer.parseInt(pageContext.getAttribute("indexIntValue").toString()) <= 3){%>
                    <ul class="rat_ico">
                      <%for(int i = 1; i<=figSize ; i++) {%>
                        <li><img src="<%=imgPath%>" alt="full stickMan"/></li>
                      <%}
                      if(pageContext.getAttribute("stickManDef").toString().indexOf(".5") >= 0){
                        imgPath = imgManPath + pageContext.getAttribute("stickManColorDef").toString() + "_gry.png";%>
                        <li><img src="<%=imgPath%>" alt="half stickMan"></li>
                      <%}
                      for(int lp = 1; lp < remain; lp++){%>
                        <li><img src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/gry_icon.png", 0)%>" alt="empty stickMan"></li>
                      <%}%>
                    </ul>
                  <%}else{%>
                    <ul class="rat_ico">
                      <%for(int i = 1; i<=figSize ; i++) {%>
                        <li><img class="lazy-load" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src="<%=imgPath%>" alt="full stickMan"/></li>
                      <%}
                      if(pageContext.getAttribute("stickManDef").toString().indexOf(".5") >= 0){
                        imgPath = imgManPath + pageContext.getAttribute("stickManColorDef").toString() + "_gry.png";%>
                        <li><img class="lazy-load" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src="<%=imgPath%>" alt="half stickMan"></li>
                      <%}
                      for(int lp = 1; lp < remain; lp++){%>
                        <li><img class="lazy-load" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/gry_icon.png", 0)%>" alt="half stickMan"></li>
                      <%}%>
                    </ul>
                  <%}%>
                <%}%>
             <!--    End of sticky man code -->
       
</c:if>
            </c:if>
          <!--   End of emplement rate code
            Below code salary range -->
            <c:if test="${not empty uniInfoList.salaryRange}">
         <c:set var="salaryRangeDef" value="${uniInfoList.salaryRange}"/>
              
              <%if("INSUFFICIENT DATA".equalsIgnoreCase(pageContext.getAttribute("salaryRangeDef").toString()) || "DATA NOT AVAILABLE".equalsIgnoreCase(pageContext.getAttribute("salaryRangeDef").toString())){%>
                <h4 class="w100p pdt30 fnt18 pr fl">Average salary <span class="fr">${uniInfoList.salaryRange}</span></h4>
              <%}else{%>
             <!--  Code for calcuating the salary bar range -->
                <c:set var="salaryBarRanges" value="${uniInfoList.salaryBarWidth}"/>
                <%String barDifff = pageContext.getAttribute("salaryBarRanges").toString();
                String barDiff[] = barDifff.split("-");
                float barWidth = 0.0f;
                String barLeft = "0";
                if(barDiff.length == 2){
                  barLeft = barDiff[0];
                  barWidth = Float.parseFloat(barDiff[1]) - Float.parseFloat(barLeft);
                  if(barWidth == 0){
                    barWidth = 1.0f;
                    if(Float.parseFloat(barLeft) < 1.0f){
                      barLeft = "1";
                    }
                  }
                }%>
               <!--  End of salary bar calculation -->
                <h4 class="w100p fl pdt20 fnt18">Average salary 
                  <span class="cr">
                    <span class="${uniInfoList.salaryRangeColor}">${uniInfoList.salaryRange}</span>
                    <span class="scr_tltip" onclick="showToolTip('toolTipSub3<%=pageContext.getAttribute("indexIntValue").toString()%>')" onmouseover="showToolTip('toolTipSub3<%=pageContext.getAttribute("indexIntValue").toString()%>')" onmouseout="hideToolTip('toolTipSub3<%=pageContext.getAttribute("indexIntValue").toString()%>')">
                      <i class="fa fa-question-circle fr fnt18" id="toolTipDiv2"></i>
                      <span class="scr_tip" id="toolTipSub3<%=pageContext.getAttribute("indexIntValue").toString()%>">
                        Graduates who studied <%=jacsSubject%> at ${uniInfoList.collegeNameDisplay} typically earn ${uniInfoList.salaryRange} six months after graduation. <br></br><%=GlobalConstants.HESA_SOURCE%>
                      </span> 
                    </span>
                  </span>
                </h4>
                <div class="pbar fl"><div class="p${uniInfoList.salaryRangeColor} fl" style="width:<%=barWidth%>%; margin-left:<%=barLeft%>%;"></div></div>
                <p class="lmh w100p fl">
                  <span class="fl">Low</span>
                  <span>Medium</span>
                  <span class="fr">High</span>
                </p>
              <%}%>
            </c:if>
           <!--  End of salary code
            Below code is donut chat and view degree button -->
            <div class="expnd_rw">
            <c:if test="${not empty uniInfoList.providerViewDegreeFlag}">
             
                <c:set var="url1" value="${uniInfoList.providerViewDegreeFlag}"/>
                <c:if test="${uniInfoList.providerViewDegreeFlag eq 'Y'}">
                 <c:set var="uniUrl" value="${uniInfoList.subjectGuideURL}"/>
                  <c:if test="${not empty uniInfoList.collegeNameForUrl}">
                 
                    <c:set var="collegeNameUrl" value="${uniInfoList.collegeNameForUrl}"/>
                    <a onclick="ulGAEventTracking('WCID', 'VD', '<%=jacsSubject%>', '<%=gaAccount%>');openNewWindow('<%=new SeoUrls().getJacsProviderURL(jacsCode, pageContext.getAttribute("collegeNameUrl").toString(), entryLevel, entryPoints)%>')" class="btn_com fr mt20 bl_bg cwhite">View degrees</a>
   
                  </c:if>
              
                </c:if>
                <c:if test="${uniInfoList.providerViewDegreeFlag eq 'G'}">
         
                  <div class="srge fright"><div class="srge-rt"><p>Degrees available with higher entry requirements</p></div></div>

                </c:if>
           
              </c:if>
              <c:if test="${not empty uniInfoList.jobIndustryFlag}">
                  <c:if test="${uniInfoList.jobIndustryFlag eq 'Y'}">
                
                  <a class="expnd_lnk" id="expnd_lnk<%=pageContext.getAttribute("indexIntValue").toString()%>" onclick="getJobIndustry('${uniInfoList.collegeId}', '${uniInfoList.ukprn}', '<%=pageContext.getAttribute("indexIntValue").toString()%>');"><i class="fa fa-angle-up"></i> Graduate destinations at this uni</a>
              	   <div class="exp_cnt" id="exp_cnt<%=pageContext.getAttribute("indexIntValue").toString()%>" style="display:none;"></div>
            
</c:if>
              </c:if>
            </div>
				      </div>
        </div>
      </c:if>
    <!--   End of the donut chart code -->
  
    </c:forEach>
  <!--   Below code is what pagination -->
    <div class="h2hdr w100p fl pdt10">
      <jsp:include page="/jsp/whatcanido/include/whatcanidoPagination.jsp">
        <jsp:param name="pagelimit" value="<%=pagelimit%>"/>
        <jsp:param name="pageno" value="<%=pageNo%>"/>
        <jsp:param name="sortingOrder" value="<%=sortingOrder%>"/>
        <jsp:param name="searchhits" value="<%=totalCount%>"/>
        <jsp:param name="sortingType" value="<%=sortingType%>"/>
        <jsp:param name="recorddisplay" value="<%=recorddisplay%>"/>
      </jsp:include>
    </div>
  <!--   End of pagination code -->

</c:if>
  