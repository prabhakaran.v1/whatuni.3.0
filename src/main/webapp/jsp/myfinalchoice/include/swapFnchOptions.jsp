<%@page import="WUI.utilities.CommonUtil"%>

<div class="swap_cont" style="display:none;" id="shwSwapOpt">
<div class="swp_wrap">
  <label>Swap</label>
  <span class="swp_fstch">
    <span id="frmSwpDisp">First choice</span>
    <select class="swp_slt_lst" id="swapFrom" onchange="showSwapPosText('swapFrom','frmSwpDisp');" onclick="hideUserActionPod();">
      <option value="1">First choice</option>
      <option value="2">Second choice</option>
      <option value="3">Third choice</option>
      <option value="4">Fourth choice</option>
      <option value="5">Fifth choice</option>
    </select>    
  </span>
</div>
<div class="swp_wrap">
  <label>With</label>
  <span class="swp_fstch">
    <span id="toSwpDisp">First choice</span>
    <select class="swp_slt_lst" id="swapTo" onchange="showSwapPosText('swapTo','toSwpDisp')" onclick="hideUserActionPod();">
      <option value="1">First choice</option>
      <option value="2">Second choice</option>
      <option value="3">Third choice</option>
      <option value="4">Fourth choice</option>
      <option value="5">Fifth choice</option>
    </select>    
	<span style="display:none;" id="swapLoaderGif" class="load_icon"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/f5_ldr.gif",0)%>" alt="loading icon"></span>
  </span>
</div>
<span id="swapStatus" class="f5_swp_succ" style="display:none;">swapped succesfully</span>
<div class="swp_conf_btn">  
  <a href="javascript:positionSwap();">REORDER</a>
</div>
</div>