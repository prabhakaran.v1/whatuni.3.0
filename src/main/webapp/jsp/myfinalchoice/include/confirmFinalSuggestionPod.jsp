<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
  String dispConfirmButStyle ="display:none;", dispFinalChEditStyle = "display:block;", prefixButId = "", confrmDivStyle="confirm_cont mt50";  
  String isRespBut = request.getParameter("respBut");
  if(isRespBut!=null && "true".equals(isRespBut)){
    prefixButId    = "inBut";  
    confrmDivStyle = "confirm_cont mt50 mob_conf_cont";
  }  
%>
<c:if test="${not empty requestScope.hideConfirmBut}"> 
  <c:if test="${requestScope.hideConfirmBut eq 'true'}">
    <%
      dispConfirmButStyle  = "display:block;";
      dispFinalChEditStyle = "display:none;";
    %>
  </c:if>
  <div class="<%=confrmDivStyle%>">
    <div class="fl">
      <div class="hd">Are you happy with your 5 choices?</div>
      <div class="mt10 fnt_14 dnt_wry">(Don't worry you can always change your mind later on).</div>    
    </div>
    <div class="fr">          
      <a class="edt_fnl5_btn" title="CONFIRM MY FINAL 5" href="javascript:confirmFinalChSlot()" id="<%=prefixButId%>dispConfirmButStyle" style="<%=dispConfirmButStyle%>">CONFIRM MY FINAL 5<i class="fa richp fa-long-arrow-right"></i></a>                  
      <a class="edt_fnl5_btn" title="EDIT MY FINAL 5" href="javascript:editMyFinalchoiceSlot()" id="<%=prefixButId%>dispFinalChEditStyle" style="<%=dispFinalChEditStyle%>">EDIT MY FINAL 5<i class="fa richp fa-long-arrow-right"></i></a>      
    </div>
  </div>
</c:if>