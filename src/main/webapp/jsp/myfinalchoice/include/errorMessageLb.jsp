<%
  String pageType = request.getParameter("pageType");  
  String okButStyle = "display:inline-block;";
  if(request.getParameter("fromFnchHome")!=null){
    okButStyle = "display:none;";
  }
%>
<div class="comLgh">
  <div class="fcls nw">
    <%if("confirmslots".equals(pageType)){%>
       <a class="noalert" onclick="javascript:hm();"><i id="close" class="fa fa-times"></i></a>     
    <%}else{%>
       <%if("crsExist".equals(pageType)){%> 
        <a class="noalert" onclick="javascript:refreshFinalChoice();"><i class="fa fa-times"></i></a>
       <%}else if("fiveslots".equals(pageType)){%> 
        <a class="noalert" onclick="javascript:hideSugOpt('show');"><i class="fa fa-times"></i></a>
       <%}else if("exitCnfrm".equals(pageType)){%> 
        <a id="close1" class="noalert" onclick="javascript:hm();"><i id="close" class="fa fa-times"></i></a>
       <%}else{%>
        <a class="noalert" onclick="javascript:closeFnch();"><i class="fa fa-times"></i></a>
       <%}%> 
    <%}%>
  </div>   
  <div class="pform nlr bgw opday f5_ms" id="lblogin">    
    <div class="reg-frm">
      <div class="reg-frmt bgw cmp_pt2">   
        <%if("fiveslots".equals(pageType)){%>
            <h1>Oops..<br/> you have already added your 5 choices.</h1>
            <div class="fr f5_msge">    
              <a class="edt_fnl5_btn ed_f5_btn msg_f5_ok" id="fnchOkBtn" style="<%=okButStyle%>" title="OK" href="javascript:hm();">OK<i class="fa richp fa-long-arrow-right"></i></a>
            </div>    
        <%}else if("zeroslots".equals(pageType)){%> 
            <h1>Steady on there..<br/> you need to add some choices before you confirm.</h1>            
            <div class="fr f5_msge">    <a class="edt_fnl5_btn ed_f5_btn msg_f5_clse" onclick="javascript:closeFnch();">CONTINUE <i class="fa richp fa-long-arrow-right"></i></a></div>
         <%}else if("confirmslots".equals(pageType)){%>
            <h2>Confirmed successfully!!</h2>  
            <div class="fr f5_msge">              
              <a class="edt_fnl5_btn ed_f5_btn msg_f5_clse" title="CONTINUE" href="/degrees/comparison">CONTINUE<i class="fa richp fa-long-arrow-right"></i></a>
            </div>                         
         <%}else if("stopEmptyslot".equals(pageType)){%>   
            <h2>Oops..<br/>You were trying to swap with Empty slot!</h2>  
            <div class="fr f5_msge">             
              <a class="edt_fnl5_btn ed_f5_btn msg_f5_clse" href="javascript:hm();">CONTINUE <i class="fa richp fa-long-arrow-right"></i></a>
            </div> 
         <%}else if("stopSameslot".equals(pageType)){%>   
            <h2>Oops..<br/>You need to select two separate slots to swap!</h2>  
            <div class="fr f5_msge">             
              <a class="edt_fnl5_btn ed_f5_btn msg_f5_clse" href="javascript:hm();">CONTINUE <i class="fa richp fa-long-arrow-right"></i></a>
            </div> 
         <%}else if("crsExist".equals(pageType)){%>   
            <h2>Oops..<br/>You have already saved this course!</h2>  
            <div class="fr f5_msge">             
              <a class="edt_fnl5_btn ed_f5_btn msg_f5_clse" href="javascript:refreshFinalChoice();">CONTINUE <i class="fa richp fa-long-arrow-right"></i></a>
            </div>
         <%}else if("uniExist".equals(pageType)){%>   
            <h2>Oops..<br/>You have already added this university to comparison basket</h2>  
            <div class="fr f5_msge">             
              <a class="edt_fnl5_btn ed_f5_btn msg_f5_clse" href="javascript:reorderFinalFive('C');">CONTINUE <i class="fa richp fa-long-arrow-right"></i></a>
            </div>
         <%}else if("exitCnfrm".equals(pageType)){%>
            <h1>Save your Final 5 choices</h1>
            <h2>(Don't worry, you can always change your mind later on)</h2>
            <div class="fr f5_msge">             
              <a class="edt_fnl5_btn ed_f5_btn msg_f5_clse f5_cond" href="javascript:reorderFinalFive('C');">Save <i class="fa richp fa-long-arrow-right"></i></a>
              <a class="edt_fnl5_btn ed_f5_btn msg_f5_clse f5_cond" href="javascript:hm();">Edit <i class="fa richp fa-long-arrow-right"></i></a>
            </div>
         <%}%>
      </div>        
    </div>       
  </div>  
</div>  

