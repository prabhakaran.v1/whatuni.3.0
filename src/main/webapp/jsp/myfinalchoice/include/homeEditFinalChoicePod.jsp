<%
  String confrmDivStyle = "confirm_cont mt50 edt_wrap_act";  
  String isRespBut = request.getParameter("respBut");  
  if(isRespBut!=null && "true".equals(isRespBut)){
    confrmDivStyle = "confirm_cont mt50 mob_conf_cont";
  }
  String isLightBox = request.getParameter("isLightBox");
  String newFinalFiveUrl = request.getContextPath() + "/comparison";
%>

<%if(isLightBox==null){%>
<div class="<%=confrmDivStyle%>" id="mychEditLightbox">  
  <div class="fr">              
    <a class="edt_fnl5_btn ed_f5_btn" title="EDIT MY FINAL 5" href="<%=newFinalFiveUrl%>">EDIT MY FINAL 5<i class="fa richp fa-long-arrow-right"></i></a>          
  </div>
</div>
<div class="<%=confrmDivStyle%>" id="cnfrmEditLightbox" style="display:none;">  
  <div class="fr">              
    <a class="edt_fnl5_btn ed_f5_btn" title="CONFIRM MY FINAL 5" href="<%=newFinalFiveUrl%>">CONFIRM MY FINAL 5<i class="fa richp fa-long-arrow-right"></i></a>
  </div>
</div>
<%}else{%>
<div class="<%=confrmDivStyle%>" id="mychEditLightbox">  
  <div class="fr">              
    <a class="edt_fnl5_btn ed_f5_btn" title="CONFIRM MY FINAL 5" href="<%=newFinalFiveUrl%>">CONFIRM MY FINAL 5<i class="fa richp fa-long-arrow-right"></i></a>          
  </div>
</div>
<%}%>