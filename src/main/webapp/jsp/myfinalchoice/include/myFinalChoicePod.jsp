<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%
  String hideBtn = (String)request.getAttribute("hideConfirmBut");
  String isFromHomePage  = "myfinalChoiceHome", choiceContClass = "choice_container",showHomeLbl = "";
  String isHomePage      = request.getParameter("fromHomePage");
  String myFinalChExist  = request.getParameter("myFinalChExist");
  String isLightBox      = request.getParameter("isLightBox");
         showHomeLbl     = request.getParameter("showHomeLbl");         
  if(request.getAttribute("myFnChHomePage")!=null && "true".equals(request.getAttribute("myFnChHomePage"))){
    isFromHomePage = "myfinalHome";
    choiceContClass = "choice_container edt_wrap_inact";
  }
  String userId = new WUI.utilities.SessionData().getData(request, "y");
  String uName = new WUI.utilities.CommonFunction().getUserFirstLastName(userId, request); 
  String newFinalFiveUrl = request.getContextPath() + "/comparison";
%>
<c:if test="${not empty requestScope.myFinalChoiceList}">
<section class="spotLight final5" id="<%=isFromHomePage%>">
  <div class="content-bg">
    <%--Print button added by Prabha on 29_Mar_2016--%>
    <%if("false".equalsIgnoreCase(hideBtn) && !"yes".equalsIgnoreCase(isHomePage)){%> <%--Condition is added by Prabha for hiding the button in home page--%>
      <div class="prnt" id="printBtn"><a onclick="printFinal5();"><i class="fa fa-print"></i>Print</a></div>
    <%}%>
    <%--End of print final 5 code--%>
    <div class="hm_txt_wrap">	
      <c:if test="${not empty requestScope.myFnChHomePage}">
        <h2 id="nameMychTag">Hi <%=(session.getAttribute("showUserName")!=null && !"".equals(session.getAttribute("showUserName"))) ? " "+session.getAttribute("showUserName") : ""%>, you're right on track...</h2>
        <h3>It's time to select your final 5!</h3>
      </c:if>
      <%if(showHomeLbl!=null && !"".equals(showHomeLbl)){%>
        <h2 id="nameMychTag">Hi <%=(session.getAttribute("showUserName")!=null && !"".equals(session.getAttribute("showUserName"))) ? " "+session.getAttribute("showUserName") : ""%>, you're right on track...</h2>
        <h3>It's time to select your final 5!</h3>
      <%}%> 
      <%if(showHomeLbl==null){%>
        <c:if test="${empty requestScope.myFnChHomePage}">
          <h2>It's time to select your final 5!</h2>
        </c:if>
      <%}%>	  
    </div>  
    <div class="hm_txt_wrap full_name">
					 <h2><%=uName%>'s final uni choices</h2>
				</div>
    <div class="row-fluid">       
      <div id="choice_container" class="<%=choiceContClass%>" onclick="location.href='<%=newFinalFiveUrl%>'">     
        <jsp:include page="/jsp/myfinalchoice/include/finalchoiceSlotPod.jsp"/>
      </div>
      <div class="car_adv_txt"><p>Don't forget to show this to your careers advisor/head of sixth form so they know what you want to study and where.</p></div>
      <%if(isHomePage == null && isLightBox == null){%>
        <jsp:include page="/jsp/myfinalchoice/include/swapFnchOptions.jsp"/>              
      <%}%>  
      <!--Confirm/Edit Button-->
      <%if((isHomePage != null && ("yes").equals(isHomePage)) || (isLightBox != null && ("true").equals(isLightBox))){%>              
        <%if(isLightBox != null && ("true").equals(isLightBox)){%>          
          <jsp:include page="/jsp/myfinalchoice/include/homeEditFinalChoicePod.jsp">          
            <jsp:param name="isLightBox" value="<%=isLightBox%>"/>          
          </jsp:include>          
        <%}else{%>  
          <jsp:include page="/jsp/myfinalchoice/include/homeEditFinalChoicePod.jsp"/>
        <%}%>
      <%}else{%>  
        <jsp:include page="/jsp/myfinalchoice/include/confirmFinalSuggestionPod.jsp"/>
      <%}%>      
      
      <!--Final Choice Suggestion pod below-->
      <%if(isHomePage == null && isLightBox == null){%>
        <div id="finalChSugestPod">
          <jsp:include page="/jsp/myfinalchoice/include/otherUniCourseSuggestionPod.jsp"/>
        </div>
      <%}%>		      
    </div>
  </div>
  <input type="hidden" id="myFinalchVal"/>
  <input type="hidden" id="myFnchPos"/>
  <input type="hidden" id="editUniCrsVal"/>
  <input type="hidden" id="mblOrient"/>
  <input type="hidden" id="exitFnchCnrfm" value="false"/>
  <input type="hidden" id="fnch_a_href"/>
  <%if(isLightBox!=null && "true".equals(isLightBox)){%>
    <input type="hidden" id="finalChLightBox" value="<%=isLightBox%>"/>
  <%}%>	    
</section>
<%if((isHomePage != null && ("yes").equals(isHomePage)) || (isLightBox != null && ("true").equals(isLightBox))){%>
  <script type="text/javascript">changeSlotStyle();</script>
<%}%>  
<script type="text/javascript">checkConfirm5();</script>
</c:if>