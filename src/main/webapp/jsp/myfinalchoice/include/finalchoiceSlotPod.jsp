<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%

String spanSlotClass = "", savedChPosStyle = "display:none;", editChPosStyle = "display:none;", crsStyleClass = "vbox", dispEditButStyle = "", dispDropStyle = "", 
 confrmLockStyle = "",dispSlotPosClr = "Br_lne bg_white", dispUniLogo = "", dispColName = "", dispUniLogoStyle = "" , dispColNameStyle = "";	  
 
  String myFinalChExist  = request.getParameter("myFinalChExist"); 
 
  if((myFinalChExist!=null && "false".equals(myFinalChExist)) || (request.getAttribute("myFinalChoiceExist")!=null && "false".equals(request.getAttribute("myFinalChoiceExist")))){
    dispDropStyle = "display:none;";
  } 
  String crsCollegeIds="";
  String newFinalFiveUrl = request.getContextPath() + "/comparison";
%>

<c:if test="${not empty requestScope.myFinalChoiceList}">
  <ul class="drag_container"> 
    <c:forEach var="myChoiceList" items="${requestScope..myFinalChoiceList}" varStatus="index">
      <c:if test="${empty myChoiceList.finalChoiceId}">
        <li class="drag_box">
          <div class="Heading_top">
            <a href="#" class="slft">                                        
              <c:if test="${not empty myChoiceList.dispSlotPositionStyle}">
                <i class="${myChoiceList.dispSlotPositionStyle}"></i>                      
                <%spanSlotClass="";%>
              </c:if>                      
              <c:if test="${empty myChoiceList.dispSlotPositionStyle}">
                <%spanSlotClass="ttof";%>
              </c:if>                                        
              <span class="<%=spanSlotClass%>">${myChoiceList.dispSlotPositionNo}</span>
            </a>								 
          </div>
          <div class="fnl5_drag" id="fnchSlotPos_${index.index+1}">
          <div class="fnl5_step1">
            <div class="Br_lne" id="finalDefPos_${index.index+1}">
              <div class="cn">
                <div class="f5button"><a class="btn4 add" href="<%=newFinalFiveUrl%>"><i class="fa fa-plus"></i>Add New</a></div>
                <input type="hidden" id="choicePosStatus_${index.index+1}" value="N">
              </div>                                        
            </div>                  
            <div id="finalChPos_${index.index+1}" style="display:none;">	
              <div class="Br_lne bg_white" id="slotClr_${index.index+1}">
                <a href="<%=newFinalFiveUrl%>" class="Li_Cl"><i class="fa fa-times fa-lg"></i></a>
                <div class="cn">
                  <div id="finalUniSlot_${index.index+1}" style="display:none;">
                    <h2>university</h2> 
                    <input type="hidden" id="finalChoiceId_${index.index+1}"/>
                    <div class="txt_area1">
                      <textarea class="vbox" autocomplete="off" onkeydown="clearFcUniNAME(event,this,'Enter uni')" onkeyup="autoCompleteFnchUniName(event,this)"
                        onclick="clearFcDefaultText(this,'addFcUni','${index.index+1}')" onblur="setFcDefaultText(this,'Enter uni')" 
                        onkeypress="javascript:if(event.keyCode==13){return redirectFnChUniHOME(this,'homeSearchBean')}" 
                        name="uniName${index.index+1}" id="uniName${index.index+1}">Enter uni</textarea>
                      <input type="hidden" id="uniName${index.index+1}_hidden">							  
                    </div>                              
                    <span class="c_im focus" id="uniLogoShw_${index.index+1}" style="display:none;"><img src="" id="uniLogo_${index.index+1}" alt="<%=dispColName%>" title="<%=dispColName%>"></span>
                    <span class="c_im focus" id="uniTxtShw_${index.index+1}" style="display:none;"><p class="par" id="uniTitle_${index.index+1}"></p></span>							
                  </div>
                  <div id="finalCrsSlot_${index.index+1}" style="display:none;">
                    <div class="crse_txtarea">
					<h2>course</h2>
                    <div class="txt_area1">							 
                      <textarea class="vbox" autocomplete="off" onkeydown="clearFcUniNAME(event,this,'Enter course');" onkeyup="autoCompleteCourseNAME(event,this,'uniName${index.index+1}');clearusrCnt();" 
                        onclick="clearFcDefaultText(this,'saveFcUni','${index.index+1}')" onblur="setFcDefaultText(this,'Enter course')" 
                        onkeypress="javascript:if(event.keyCode==13){return redirectFnChUniHOME(this,'homeSearchBean');}else if(event.keyCode==8 || event.keyCode==46){clearusrCnt('${index.index+1}');}" 
                        name="crsName${index.index+1}" id="crsName${index.index+1}" onselect="setMyfnchPos('${index.index+1}');">${myChoiceList.courseTitle}</textarea>
                      <input type="hidden" id="crsName${index.index+1}_hidden" value="${myChoiceList.courseId}">
					  <input type="hidden" id="crsIdExist_${index.index+1}" value="false"/>
                    </div>
					</div>
                  </div>
                  <div class="f5button" id="usrPenAct_${index.index+1}" style="display:none;">						  
                    <%--<a onclick="usrStatusActionPod('${index.index+1}','fnchUsrActSlot_${index.index+1}')" style="display:none;" class="count_box" id="usrPenCnt_${index.index+1}"><bean:write name="myChoiceList" property="inCompActionCnt"/></a>     
                    <div class="f5_actions" style="display:none;" id="fnchUsrActSlot_${index.index+1}"></div>                            --%>
                  </div>                            
                </div>                        
              </div>
            </div>
            </div>
          </div>                                               				
        </li>              
      </c:if>                                           
      
      <c:if test="${not empty myChoiceList.finalChoiceId}">
        <c:if test="${myChoiceList.slotPositionStatus eq 'E'}">
          <c:if test="${not empty myChoiceList.courseId}">
            <%crsStyleClass = "vbox focus";%>	
          </c:if>
        </c:if>
        <c:if test="${empty myChoiceList.courseId}">
          <%crsStyleClass = "vbox";%>	
        </c:if>
        <c:if test="${myChoiceList.slotPositionStatus eq 'C'}">
          <%
            savedChPosStyle  = "display:block;";
            editChPosStyle   = "display:none;";                    
            dispEditButStyle = "display:none;"; 
            confrmLockStyle  = "display:block;";
            dispSlotPosClr   = "Br_lne bg_blue";
          %>	
        </c:if>                
        <c:if test="${myChoiceList.slotPositionStatus eq 'E'}">
          <%
            savedChPosStyle = "display:none;";
            editChPosStyle  = "display:block;";
            confrmLockStyle = "display:none;";
            dispEditButStyle = "display:block;";
            dispSlotPosClr   = "Br_lne bg_white";
          %>	
        </c:if>			
        <li class="drag_box">
          <div class="Heading_top">
            <a href="#" class="slft">
              <c:if test="${not empty myChoiceList.dispSlotPositionStyle}">
                <i class="${myChoiceList.dispSlotPositionStyle}"></i>
                <%spanSlotClass="";%>
              </c:if>                      
              <c:if test="${empty myChoiceList.dispSlotPositionStyle}">
                <%spanSlotClass="ttof";%>
              </c:if>                                        
              <span class="<%=spanSlotClass%>">${myChoiceList.dispSlotPositionNo}</span>
            </a>								 
            <%--<a class="srgt" href="#" style="<%=confrmLockStyle%>" id="cnfButPosId_${index.index+1}"><i class="fa fa-lock fa-2x"></i></a>--%>
          </div>                                     
          <div class="fnl5_drag" id="fnchSlotPos_${index.index+1}">
          <div class="fnl5_step1">
            <div class="Br_lne" id="finalDefPos_${index.index+1}" style="display:none;">
              <div class="cn">                
                <div class="f5button"><a class="btn4 add" href="javascript:switchUniChoice(${index.index+1});"><i class="fa fa-plus"></i>Add New</a></div>
              </div>                    
            </div>                  
            <div id="finalChPos_${index.index+1}">
              <div class="<%=dispSlotPosClr%>" id="slotClr_${index.index+1}">
                <a class="Li_Cl" style="<%=dispEditButStyle%>" id="cancelSlotCh_${index.index+1}"><i class="fa fa-times fa-lg"></i></a>
                <div class="cn">                                                  
                  <input type="hidden" id="finalChoiceId_${index.index+1}" value="${myChoiceList.finalChoiceId}"/>
                  <c:if test="${not empty myChoiceList.collegeLogo}">	
                    <c:set var="shwUniLogo" value="${myChoiceList.collegeLogo}"/>		
                    <%dispUniLogo = (String) pageContext.getAttribute("shwUniLogo");%>
                  </c:if>											                 
                  <c:if test="${not empty myChoiceList.collegeDispName}">
                    <c:set var="shwcolDispName" value="${myChoiceList.collegeDispName}"/>  
                    <%
                      dispColName = (String)pageContext.getAttribute("shwcolDispName");
                      dispColName = (dispColName.length()>55 ? dispColName.substring(0,52) + "...": dispColName);
                    %>								  
                  </c:if>
                  <%
                    if(!"".equals(dispUniLogo)){
                      dispUniLogoStyle = "display:block;";
                      dispColNameStyle = "display:none;";
                    }else{
                      dispUniLogoStyle = "display:none;";
                      dispColNameStyle = "display:block;";
                    }
                  %>
                  <c:if test="${not empty myChoiceList.slotPositionStatus}">                            
                    <div id="savedChPos_${index.index+1}" style="<%=savedChPosStyle%>"> 	
                      <h2>university</h2>                             
                      <span class="c_im" style="<%=dispUniLogoStyle%>"><img src="<%=dispUniLogo%>" alt="<%=dispColName%>" title="<%=dispColName%>"></span>                
                      <span class="c_im" style="<%=dispColNameStyle%>"><p class="par"><%=dispColName%></p></span>
					  
					  <div class="crse_txtarea">
                      <c:if test="${not empty myChoiceList.courseId}">
                        <c:set var="crsTitle" value="${myChoiceList.courseTitle}"/>
                        <% String crsTitle = (String)pageContext.getAttribute("crsTitle"); %>
                        <h2>course</h2><p class="par" id="crsTitle_${index.index+1}"><%=(crsTitle.length()>43 ? crsTitle.substring(0,39) + "...": crsTitle)%></p>  
                      </c:if>						  
                      <c:if test="${empty myChoiceList.courseId}">
						<h2>&nbsp;&nbsp;&nbsp;</h2>
                        <p class="par">&nbsp;</p>
                      </c:if>
					  </div>
					  
                      <div class="f5button">                            
                       <%-- <logic:notEmpty name="myChoiceList" property="inCompActionCnt">
                          <a onclick="usrStatusActionPod('${index.index+1}','svdFnchUsrActSlot_${index.index+1}')" id="svdUsrPenCnt_${index.index+1}" class="count_box">
                          <bean:write name="myChoiceList" property="inCompActionCnt"/></a>
                          <div class="f5_actions" style="display:none;" id="svdFnchUsrActSlot_${index.index+1}"></div>
                        </logic:notEmpty>  																										--%>
                      </div>
                    </div>                        
                    <div id="finalUniSlot_${index.index+1}" style="<%=editChPosStyle%>">	
                      <h2>university</h2>
                      <div class="txt_area1">
                        <textarea style="display:none;" class="vbox focus" autocomplete="off" onkeydown="clearFcUniNAME(event,this,'Enter uni')" 
                          onkeyup="autoCompleteFnchUniName(event,this)" onclick="clearFcDefaultText(this,'addFcUni','${index.index+1}')" 
                          onblur="setFcDefaultText(this,'Enter uni')" onkeypress="javascript:if(event.keyCode==13){return redirectFnChUniHOME(this,'homeSearchBean')}" 	
                          name="uniName${index.index+1}" id="uniName${index.index+1}">${myChoiceList.collegeDispName}</textarea>
                        <input type="hidden" id="uniName${index.index+1}_hidden" value="${myChoiceList.collegeId}"/>
						<%--Added condition for Final 5 event logging for 08_MAR_2016, By Thiyagu G.--%>
						<c:set var="crsCollegeId" value="${myChoiceList.collegeId}"/>
						<%crsCollegeIds=crsCollegeIds+","+ pageContext.getAttribute("crsCollegeId");%>
                      </div>					
                      <span class="c_im focus" id="uniLogoShw_${index.index+1}" style="<%=dispUniLogoStyle%>"><img src="<%=dispUniLogo%>" id="uniLogo_${index.index+1}" alt="<%=dispColName%>" title="<%=dispColName%>"></span>   
                      <span class="c_im focus" id="uniTxtShw_${index.index+1}" style="<%=dispColNameStyle%>"><p class="par" id="uniTitle_${index.index+1}"><%=dispColName%></p></span>
                    </div>
                    <div id="finalCrsSlot_${index.index+1}" style="<%=editChPosStyle%>">
                      <div class="crse_txtarea">
					  <h2>course</h2>
                      <div class="txt_area1">
                        <textarea class="<%=crsStyleClass%>" autocomplete="off" onkeydown="clearFcUniNAME(event,this,'Enter course');clearusrCnt();"
                          onkeyup="autoCompleteCourseNAME(event,this,'uniName${index.index+1}');" 
                          onclick="clearFcDefaultText(this,'saveFcUni','${index.index+1}')" onblur="setFcDefaultText(this,'Enter course')" 
                          onkeypress="javascript:if(event.keyCode==13){return redirectFnChUniHOME(this,'homeSearchBean');}else if(event.keyCode==8 || event.keyCode==46){clearusrCnt('${index.index+1}');}" 
                          name="crsName${index.index+1}" id="crsName${index.index+1}" onselect="setMyfnchPos('${index.index+1}');">${myChoiceList.courseTitle}</textarea>
                        <input type="hidden" id="crsName${index.index+1}_hidden" value="${myChoiceList.courseId}">
						<c:if test="${not empty myChoiceList.courseId}">
						<input type="hidden" id="crsIdExist_${index.index+1}" value="true"/>
						</c:if>
                      </div>
					  </div>
                    </div>
                    <div class="f5button" id="usrPenAct_${index.index+1}" style="<%=editChPosStyle%>">                          
                      <%--<a onclick="usrStatusActionPod('${index.index+1}','fnchUsrActSlot_${index.index+1}')" style="<%=editChPosStyle%>" class="count_box" id="usrPenCnt_${index.index+1}"><bean:write name="myChoiceList" property="inCompActionCnt"/></a>
                      <div class="f5_actions" style="display:none;" id="fnchUsrActSlot_${index.index+1}"></div>							  --%>
                    </div>   
                    <input type="hidden" id="choicePosStatus_${index.index+1}" value="${myChoiceList.slotPositionStatus}">	
                  </c:if>                       
                </div>                        
              </div>
            </div>
            </div>
          </div>				
        </li>              
        <%dispUniLogo = "";%>
      </c:if>                                         
    </c:forEach>
	<%--Added condition for Final 5 event logging for 08_MAR_2016, By Thiyagu G.--%>
    <li class="drag_box">              
      <jsp:include page="/jsp/myfinalchoice/include/confirmFinalSuggestionPod.jsp">
        <jsp:param name="respBut" value="true" /> 
      </jsp:include>              
    </li>
  </ul>
  <input type="hidden" id="confirmedUniIds" value="<%=crsCollegeIds%>">
</c:if>
