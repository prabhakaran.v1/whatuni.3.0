<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil"%> 
<%
  String hidCollegeId = "", hidCourseId = "", hidCrsTitle = "", hidCollegeName = "";
%>

<c:if test="${not empty requestScope.mySuggestionList}">
  <div class="choice_container oth_uni_pod">  
	<span style="display:none;" id="sugChoicegif" class="load_icon"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/f5_ldr.gif",0)%>"></span>
    <div class="hd fl mb20">Other unis / courses you may want to add</div>
    <ul class="drag_container" id="dropdiv">
      <c:forEach var="suggestList" items="${requestScope.mySuggestionList}" varStatus="index">      
        <li class="drag_box">
          <div class="fnl5_drag">
            <div class="fnl5_step1">
              <div class="Br_lne bg_white">                
                <div class="cn">                
                  <c:if test="${not empty suggestList.collegeId}">
                    <c:set var="hid_CollegeId" value="${suggestList.collegeId}"/>
                    <c:set var="shortcollegeName" value="${suggestList.collegeDispName}"/>                    
                    <%hidCollegeId   =  (String) pageContext.getAttribute("hid_CollegeId");%>
                    <%hidCollegeName = (String) pageContext.getAttribute("shortcollegeName");%>
                  </c:if>               
                  
                  <c:if test="${not empty suggestList.courseId}">
                    <c:set var="hid_CourseId"  value="${suggestList.courseId}"/>
                    <%hidCourseId = (String) pageContext.getAttribute("hid_CourseId");%>
                  </c:if>                                     
                  
                  <c:if test="${not empty suggestList.collegeLogo}">
                    <h2>university</h2>
                    <span class="c_im"><img id="sugUniLogo_${index.index + 1}" src="${suggestList.collegeLogo}" alt="<%=hidCollegeName%>" title="<%=hidCollegeName%>"></span>
                  </c:if>
                  
                  <c:if test="${not empty suggestList.collegeLogo}">
                    <h2>university</h2>
                    <%hidCollegeName = (hidCollegeName.length()>67 ? hidCollegeName.substring(0,59) + "...": hidCollegeName);%>
                    <span class="c_im"><p class="par"><%=hidCollegeName%></p></span>
                  </c:if>
                  <c:if test="${not empty suggestList.courseId}">
                    <c:set id="crsTitle" value="${suggestList.courseTitle}"/>
                    <% String crsTitle = ""; 
                       crsTitle = (String) pageContext.getAttribute("crsTitle");
                       hidCrsTitle = (crsTitle.length()>43 ? crsTitle.substring(0,39) + "...": crsTitle);%>
                    <div class="dpar">
                      <h2>course</h2>
                      <p class="par"><%=hidCrsTitle%></p>
                    </div>  
                  </c:if>                                                        
                  <c:if test="${empty suggestList.courseId}">
                    <div class="dpar">
                      <h2>&nbsp;&nbsp;&nbsp;</h2>
                      <p class="par">&nbsp;</p>
                    </div>  
                  </c:if>                  
                  <input type="hidden" id="drpColId_${index.index + 1}" value="<%=hidCollegeId%>"/>
                  <input type="hidden" id="drpCrsId_${index.index + 1}" value="<%=hidCourseId%>"/>
                  <div class="f5button f5_add_btn" id="addSugBtn_${index.index + 1}">
                    <a class="btn4 add" href="javascript:addSugtoFnchSlot('${index.index + 1}');"><i class="fa fa-plus"></i>Add</a>
                  </div>                       
                  <%
                    hidCollegeId = "";
                    hidCourseId  = "";
                    hidCrsTitle  = "";                    
                  %>
                </div>
              </div>
            </div>
          </div>
        </li>
      </c:forEach>
    </ul>
  </div>    
</c:if>



