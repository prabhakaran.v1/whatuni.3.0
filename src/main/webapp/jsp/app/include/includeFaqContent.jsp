<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" %>
<%--
  * @purpose  This is App FAQ Page (ALP) signup form.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                              Rel Ver.
  * 08-Aug-2017    Thiyagu G.                567      First Draft                                               567 
  * *************************************************************************************************************************
--%>
<%
  String wuLink = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();  
  CommonUtil util = new CommonUtil();
  String TCVersion = util.versionChanges(GlobalConstants.TERMS_AND_COND_VER);
  String PRVersion = util.versionChanges(GlobalConstants.PRIVACY_VER);
%>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-4">
                    <!-- Panel section 1 -->
                    <div class="panel_lft">
                        <h3>Basic - So you don't have to be</h3>
                        <ul class="nav nav-pills nav-stacked">
                            <li role="presentation"><a href="#navlnk1">What is the Whatuni App?</a></li>
                            <li role="presentation"><a href="#navlnk2">Why build an app?</a></li>
                            <li role="presentation"><a href="#navlnk3">What does the App Do?</a></li>
                            <li role="presentation"><a href="#navlnk4">How will the app know my location?</a></li>
                        </ul>
                    </div>
                    <!-- Panel section 1 -->
                    <!-- Panel section 2 -->
                    <div class="panel_lft">
                        <h3>Matching - Not just for dating apps</h3>
                        <ul class="nav nav-pills nav-stacked">
                            <li role="presentation"><a href="#navlnk5">What is the matching facility?</a></li>
                            <li role="presentation"><a href="#navlnk6">How can I get better matches?</a></li>
                        </ul>
                    </div>
                    <!-- Panel section 2 -->
                    <!-- Panel section 3 -->
                    <div class="panel_lft">
                        <h3>Profiles and privacy - It's all in the details</h3>
                        <ul class="nav nav-pills nav-stacked">
                            <li role="presentation"><a href="#navlnk7">How much does it cost?</a></li>
                            <li role="presentation"><a href="#navlnk8">Why do I have register?</a></li>
                            <li role="presentation"><a href="#navlnk9">Should I opt in for push notifications?</a></li>
                        </ul>
                    </div>
                    <!-- Panel section 3 -->
                    <!-- Panel section 4 -->
                    <div class="panel_lft">
                        <h3>Troubleshooting - can we fix it? Yes we can!</h3>
                        <ul class="nav nav-pills nav-stacked">
                            <li role="presentation"><a href="#navlnk10">Where can I download the app?</a></li>
                            <li role="presentation"><a href="#navlnk11">The app doesn't work?</a></li>
                            <li role="presentation"><a href="#navlnk12">App keeps crashing?</a></li>
                            <li role="presentation"><a href="#navlnk13">I'm already at uni, how do I upload a review?</a></li>
                            <li role="presentation"><a href="#navlnk14">I've sent an email request to a uni I like, but I haven't heard back?</a></li>
                            <li role="presentation"><a href="#navlnk15">How do I submit feedback?</a></li>
                            <li role="presentation"><a href="#navlnk16">How can I delete my account?</a></li>
                        </ul>
                    </div>
                    <!-- Panel section 4 -->
                </div>
                <div class="col-xs-12 col-sm-offset-1 col-sm-7 col-md-offset-1 col-md-8 col-lg-offset-1 col-lg-7">
                   <div class="panel_rgt">
                      <h2>Basic - So you don't have to be</h2>
                      <h3 id="navlnk1">What is the Whatuni App?</h3>
                       <p><a href="<%=wuLink%>" title="Whatuni" target="_blank">Whatuni</a> is a research site, dedicated to helping you find your perfect uni and course. Our uniqueness is that we're totally student-led: every year we collect more than 26,000 reviews from university students to help ensure you're making the right decision on everything, from courses and city life to accommodation.</p>
                       <p>We've decided to build an app that lets you access the information we have on our website, but in bitesize, snackable chunks to make your search easier than ever before.</p>
                       <h3 id="navlnk2">Why build an app?</h3>
                       <p>There's a lot of reasons that led us to build this app but two that really stand out. First of all, over half our users are accessing Whatuni on their smartphones at any one time. So, it just makes sense to make that mobile experience slicker and easier for students by creating an app. </p>
                       <p>Second of all, it takes time to choose the right course at the right uni.  Having an app at your fingertips gives you a direct access point to your research anytime, anywhere and at your own convenience. The app will save your searches and all your favourites, so you don't need to make any concrete decisions until the time is right for you. Or at least until that UCAS deadline hits!</p>
                       <h3 id="navlnk3">What does the app do?</h3>
                       <h4>Helps you explore</h4>
                       <p>Explore 1000s of different subjects - with everything from Criminology to Molecular Biology, be it Undergraduate, Foundation or HND/HNC - we got you covered.</p>
                       <h4>Leads you on a discovery</h4>
                       <p>Discover comprehensive profiles on universities and Higher Ed colleges across the UK. With all the important details, from drop-out rates and job prospects to tuition fees and the cost of a pint, we arm you with the facts - unbiased and complete with student reviews - so you can make the right choice.</p>
                       <p>You can also order a prospectus, drop the uni an email, or sign up for an open day, all at zero cost and commitment-free.</p>
                       <h4>Saves what you love</h4>
                       <p>Love what you see? Save what you love, using the 'heart' icon to bundle all your faves to your profile. Once you're good and ready, you can whittle down your choices to your Final 5.</p>                       
                       <h3 id="navlnk4">How will the app know my location?</h3>
                       <p>The Whatuni app will let you know about upcoming open days and uni events in your area. Huzzah! For this to be possible, you have to enable access to your location on your device. If you didn't enable location services first time round, don't worry: you can just get it done in your Settings:</p>
                       <p> Settings > Privacy > Location Services (switch to 'on') > Whatuni > (Allow Location Access) Always</p>
                       <h2>Matching - Not just for dating apps</h2>
                       <h3 id="navlnk5">What is the matching facility?</h3>
                       <p>Whatuni's app has an awesome matchmaker in the form of our virtual assistant "Luna", who will help narrow your search to courses and institutions that match your preferences. From beachside locations and bustling cities to courses that are more practical than exam-based, the app will seamlessly produce results according to your best match.</p>
                       <h3 id="navlnk6">How can I get better matches?</h3>
                       <p>Not getting the matches you want? Our matching works according to your profile preferences; to get the best matches, take the time to talk to our virtual assistant, Luna.  It only takes a few moments but it'll make your research sooooooooo much easier.  If you skipped through Luna's quiz first time round, no worries - you can access it via the profile screen:</p>
                       <p>Whatuni > Profile > Take Our Quiz</p>
                       <h2 class="our_fp">Profiles and privacy - It's all in the details</h2>
                       <p>Our full privacy policy can be found <a href="<%=PRVersion%>">here</a>.</p>
                       <h3 id="navlnk7">How much does it cost?</h3>
                       <p>It is 100% free to download and access all the content.</p>
                       <p>There are no in-app purchases and you will not be charged for any actions you take on the app, for example ordering uni prospectuses. Hurray!</p>
                       
                       <h3 id="navlnk8">Why do I have to register?</h3>
                       <p>To have the full experience of the Whatuni app, we ask you to register through Facebook login or using an email address. This is just so you can save your searches and match preferences, as well as having the option to share your research with your careers advisor.</p> 
                       <p>Any personal data that you provide us with when you use the app will be used in accordance with our <a href="<%=PRVersion%>">Privacy Policy</a> and <a href="<%=TCVersion%>">Terms and Conditions</a>.</p>

                       <h3 id="navlnk9">Should I opt in for push notifications?</h3>
                       <p>We definitely recommend opting in for push, as the notifications act as a timeline to keep your research on track, from UCAS application deadlines to clearing info, push will keep you in the loop.</p>
                       <p>If you didn't get round to enabling push notifications when you signed up, don't worry, you can just enable it via your settings:</p>
                       <p>Settings > Notifications > Whatuni > Allow notifications </p>
                       
                       <h2>Troubleshooting - can we fix it? Yes we can!</h2>
                       <h3 id="navlnk10">Where can I download the app?</h3>
                       <p>You can download the app directly from iTunes and/or Google Playstore.<%-- Though the app is not yet available for Android users, watch this space as we'll be release our Google Play version in only a few weeks' time! For more info, get in touch at <a href="mailto:android@whatuni.com">android@whatuni.com.</a>--%></p>
                       <p>The Whatuni app gives a comprehensive list of universities in the UK only. For those looking to study abroad, check out our international site: <a href="https://www.hotcoursesabroad.com/" target="_blank">Hotcourses Abroad.</a></p>

                       <h3 id="navlnk11">The app doesn't work?</h3>
                       <p>Please check which operating system your device is running on.</p>
                       <p>On iOS the WU app runs on iOS9 up and later.  Please upgrade your operating software if you are using iOS9 or below.</p>
                       <p>On Android the Whatuni app runs on 5.0 or later.<%--The app doesn't currently run on Android, but you can register your interest for the Android release here: <a href="mailto:android@whatuni.com">android@whatuni.com.</a>--%></p>

                       <h3 id="navlnk12">App keeps crashing?</h3>
                       <p>This may be very temporary so don't despair, just force quit the app and try again at a later time. In the mean-time, make sure you have the latest version of the app and that your phone's operating system is up-to-date.</p>
                       <p>If the issue persists, try reinstalling the app. To reinstall, simply delete the app from your phone and download Whatuni again on the app store. As long as you don't delete your account, this won't delete your saved and favourite searches.</p>

                       <h3 id="navlnk13">I'm already at uni, how do I upload a review?</h3>
                       <p>We salute you for submitting a review to help our students make the right choices and help us recognise those unis that are doing a great job (and those who are doing not-so-great!). Unfortunately, we cannot accept reviews via the app (yet!), so please continue to upload your university reviews via the <a href="/university-course-reviews/">desktop/mobile site</a>. Uni reviews left on the app store will not be included on our sites so please avoid doing this.</p>

                       <h3 id="navlnk14">I've sent an email request to a uni I like, but I haven't heard anything back? </h3>
                       <p>Don't worry, this is not uncommon. Universities are often inundated with requests and some unis are hotter on their comms than others. Though unfortunately we cannot be responsible for the responsiveness of our unis, if you still haven't heard anything back within 2 weeks, drop us a message <a href="mailto:feedback@whatuni.com">feedback@whatuni.com</a> and we'll chase this up for you.</p>

                       <h3 id="navlnk15">How do I submit feedback?</h3>
                       <p>We really appreciate the feedback you've given us to date; please keep your ideas flowing. We take on board all comments and suggestions so we can keep on developing and improving our service.  Our app is built for you, and if there's a functionality you feel is missing or you'd like us to include, then wing your suggestions our way, to <a href="mailto:feedback@whatuni.com">feedback@whatuni.com.</a></p>
                       <p>Likewise, if you're enjoying the app, do please spread the word by giving us a review on the app store. App store reviews are really important to us so big thank you to all those who have contributed so far.</p>

                       <h3 id="navlnk16">How can I delete my account?</h3>
                       <p>Why would you wanna do that?! Though we're sad to see you go, if you'd like to delete your account, simply drop us an email at <a href="mailto:delete@whatuni.com">delete@whatuni.com</a> and we'll remove your account.</p>
                       <p>If there's something we did wrong, please do <a href="mailto:feedback@whatuni.com">let us know</a> so we can do better. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
