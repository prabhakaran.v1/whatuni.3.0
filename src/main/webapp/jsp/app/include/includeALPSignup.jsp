

<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" %>


<% 
CommonFunction common = new CommonFunction();
String appStoreURL  =  common.getWUSysVarValue("WU_APP_STORE_URL");
String androidAppFlag = common.getWUSysVarValue("ANDROID_APP_ON_OFF");
String clearingonoff = common.getWUSysVarValue("CLEARING_ON_OFF");
String imageName = "Page_1_phone@1x";
String appStoreURLForAndroid = ("ON".equalsIgnoreCase(androidAppFlag)) ? common.getWUSysVarValue("ANDROID_APP_URL") : "";
if("ON".equalsIgnoreCase(clearingonoff)){
  imageName = "clr_page_1_phone@1x";
  }
  String imagePath = "/wu-cont/images/thumbnails/"+ imageName + ".png";
%>
<div class="section" id="slide1">
  <div class="section-inner">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn ">
          <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 fl_left cs_sub_cont animated">
              <div class="jumbotron"> 
                <img src="<%=CommonUtil.getImgPath("/wu-cont/images/icons/app_icon@1x.png",0)%>" class="img-responsive" alt="Responsive image"/>
                <h2>Connecting you to your university</h2>
              </div>
              <div class="row form_cont">
                <div class="col-lg-11">
                  <div class="row form_cont">
                    <p class="dwapp_btn"><a href="<%=appStoreURL%>" target="_blank" title="Whatuni app download store" role="button"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/wu_app_ios_btn.svg",0)%>" alt="Whatuni app download store"/></a>
                    <%if("ON".equalsIgnoreCase(androidAppFlag)){%>
                      <a class="andapp_btn" href="<%=appStoreURLForAndroid%>" target="_blank" title="Whatuni app download store" role="button"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/wu_app_android_btn.svg",0)%>" alt="Whatuni app download store"/></a>
                    <%}%>
                    </p>                    
                  </div>
                </div>
              </div>              
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 fl_right img_fld cs_sub_cont animated">
              <img src="<%=CommonUtil.getImgPath("",0)%><%=imagePath%>" class="img-responsive" width="" alt="Responsive image">
              <%if("ON".equalsIgnoreCase(clearingonoff)){%>
                <img src="<%=CommonUtil.getImgPath("/wu-cont/images/thumbnails/clr_am_handrawn_text.svg",0)%>" class="img-responsive clr_arwtext" alt="Responsive image"/>
              <%}%>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>