<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" %>


<%--
  * @purpose  This is App FAQ Page (ALP) signup form.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                              Rel Ver.
  * 08-Aug-2017    Thiyagu G.                567      First Draft                                               567 
  * *************************************************************************************************************************
--%>

<%CommonUtil util = new CommonUtil();
    String TCVersion = util.versionChanges(GlobalConstants.TERMS_AND_COND_VER);%>
<div class="section fp-section fp-table" id="slide1">
                <div class="fp-tableCell" id="get_hght">
				<div class="section-inner">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-9 col-md-9 col-lg-9 mid_algn ">
								<div class="row">
									<div class="col-xs-12 col-sm-9 col-md-7 col-lg-6 cs_sub_cont fl_none">
										<div class="jumbotron"><h2>Contact us</h2></div>
										<div class="row form_cont">
											<div class="col-lg-12">
												<div class="row form_cont">
													<form class="form-horizontal" action="" method="post" id="appcontactus" name="appcontactus">
														<div class="form-group form-group-lg">
															<div class="col-sm-6 col-lg-6">
																<input type="text" class="form-control" aria-label="First name" id="firstName" name="firstName" placeholder="First name*" autocomplete="off" maxlength="40" onkeyup="submitAppContactUsPage();" onkeypress="javascript:if(event.keyCode==13){return submitAppContactUsPage('SUBMIT')}">
                  <p class="err" id="firstName_error" style="display:none;"></p>
															</div>
															<div class="col-sm-6 col-lg-6">
																<input type="text" class="form-control" aria-label="Last name" id="lastName" name="lastName" placeholder="Last name*" autocomplete="off" maxlength="40" onkeyup="submitAppContactUsPage();" onkeypress="javascript:if(event.keyCode==13){return submitAppContactUsPage('SUBMIT')}">
                <p class="err" id="lastName_error" style="display:none;"></p>
															</div>
															<div class="col-lg-12">
																<input type="text" class="form-control" aria-label="Email address" id="emailAddress" name="emailAddress" placeholder="Email address*" autocomplete="off" maxlength="50" onkeyup="submitAppContactUsPage();" onkeypress="javascript:if(event.keyCode==13){return submitAppContactUsPage('SUBMIT')}">
                <p style="display: none;" id="emailAddress_error" class="err"></p>
															</div>
                 <div class="col-lg-12">
																<textarea class="form-control" rows="6" id="message" name="message" maxlength="4000" placeholder="Please enter a brief description of your message*" onkeyup="submitAppContactUsPage();" onkeypress="javascript:if(event.keyCode==13){return submitAppContactUsPage('SUBMIT')}"></textarea>
                <p style="display:none;" id="message_error" class="err"></p>
															</div>
               <p style="float:left;width:100%;margin:14px 0 0;">By clicking on Send Email, you are agreeing to our <a href="<%=TCVersion%>" target="_blank" style="color:#fff;text-decoration:underline">Terms and Conditions</a>.</p>
														</div>
														<p><a id="sendMailLink" class="btn btn-primary btn-lg btn_dis" onclick="submitAppContactUsPage('SUBMIT');" role="button">SEND EMAIL</a>
                <a id="lodingImg" class="btn btn-primary btn-lg spin_load" role="button" style="display:none"><img class="" src="<%=CommonUtil.getImgPath("/wu-cont/images/icons/spin_load.svg",0)%>" atl="Signup loading icon" ></a></p>
													</form>
              <div class="well well-lg" id="emailSuccessMsg" style="display:none;"></div>
              <p id="emailSuccessBtn" style="display:none;"><a class="btn btn-primary btn-lg btn-success" role="button"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/icons/tick_icon.png",0)%>" atl="sucess icon" /></a></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
   </div>
			</div>
<script type="text/javascript">
  var vulnerableKeywords = ["onclick","`","{","}","~","|","^","http:","javascript:","https:","ftp:","type='text/javascript'","type=\"text/javascript\"","language='javascript'","language=\"javascript\""];
  var htmlTags = ["<!DOCTYPE","<!--","<acronym","<abbr","<address","<applet","<area","<article","<aside","<audio","<a","<big","<bdi","<basefont","<base","<bdo","<blockquote","<body","<br","<button","<b","<center","<canvas","<caption","<cite","<code","<colgroup","<col","<dir","<datalist","<dd","<del","<details","<dfn","<dialog","<div","<dl","<dt","<embed","<em","<frameset","<frame","<font","<fieldset","<figcaption","<figure","<footer","<form","<header","<head","<h1","<h2","<h3","<h4","<h5","<h6","<hr","<html","<iframe","<img","<ins","<input","<i","<kbd","<keygen","<label","<legend","<link","<li","<main","<map","<mark","<menuitem","<menu","<meta","<meter","<noscript","<noframes","<nav","<object","<ol","<optgroup","<option","<output","<param","<\pre","<progress","<p","<q","<rp","<rt","<ruby","<svg","<samp","<script","<section","<select","<small","<strike","<source","<span","<strong","<style","<sub","<summary","<sup","<s","<table","<thead","<tbody","<tfoot","<tt","<td","<th","<tr","<text\area","<time","<title","<track","<ul","<u","<var","<video","<wbr","<\/acronym","<\/abbr","<\/address","<\/applet","<\/area","<\/article","<\/aside","<\/audio","<\/a","<\/big","<\/bdi","<\/basefont","<\/base","<\/bdo","<\/blockquote","<\/body","<\/br","<\/button","<\/b","<\/center","<\/canvas","<\/caption","<\/cite","<\/code","<\/colgroup","<\/col","<\/dir","<\/datalist","<\/dd","<\/del","<\/details","<\/dfn","<\/dialog","<\/div","<\/dl","<\/dt","<\/embed","<\/em","<\/frameset","<\/frame","<\/font","<\/fieldset","<\/figcaption","<\/figure","<\/footer","<\/form","<\/header","<\/head","<\/h1","<\/h2","<\/h3","<\/h4","<\/h5","<\/h6","<\/hr","<\/html","<\/iframe","<\/img","<\/ins","<\/input","<\/i","<\/kbd","<\/keygen","<\/label","<\/legend", "<\/link","<\/li","<\/main","<\/map","<\/mark","<\/menuitem","<\/menu","<\/meta","<\/meter","<\/noscript","<\/noframes","<\/nav","<\/object","<\/ol","<\/optgroup","<\/option","<\/output","<\/param","<\/pre","<\/progress","<\/p","<\/q","<\/rp","<\/rt","<\/ruby","<\/svg","<\/samp","<\/script","<\/section","<\/select","<\/small","<\/strike","<\/source","<\/span","<\/strong","<\/style","<\/sub","<\/summary","<\/sup","<\/s","<\/table","<\/thead","<\/tbody","<\/tfoot","<\/tt","<\/td","<\/th","<\/tr","<\/textarea","<\/time","<\/title","<\/track","<\/ul","<\/u","<\/var","<\/video","<\/wbr"];
</script>