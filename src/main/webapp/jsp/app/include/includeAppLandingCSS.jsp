<%@page import="WUI.utilities.CommonUtil" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%
  String ajaxDynamicListJSName = CommonUtil.getResourceMessage("wuni.ajaxDynamicList.js", null);
  String appFaqPageJSName = CommonUtil.getResourceMessage("wuni.app.faq.page.js", null);
  String pageName = request.getParameter("pageName"); //Added to identify the calling page's name on 05_Sep_2017, By Thiyagu G
  String appLandingPageJsName = CommonUtil.getResourceMessage("wuni.app.landing.js", null);//getting js name from property file for 31_July_18 rel by Sangeeth.S
//Getting Css names from property file for 24_Sep_2019 release by Sujitha V
  String wuappBootstrapMinCss = CommonUtil.getResourceMessage("wuapp_bootstrap.min", null);
  String wuappMainStyleCss = CommonUtil.getResourceMessage("wuapp_main_style", null);
  String wugoMainStyleCss = CommonUtil.getResourceMessage("wugo_main_style", null);
  String wugoBootstrapMinCss = CommonUtil.getResourceMessage("wugo_bootstrap.min", null);
  
if("ALP".equals(pageName)){%>
<link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuappBootstrapMinCss%>">
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuappMainStyleCss%>" />
<%}else {%>
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wugoBootstrapMinCss%>">
  <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wugoMainStyleCss%>" />
<%}%>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<%if("ALP".equals(pageName) || "productPage".equals(pageName)){%>
  <script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/app/jquery.fullPage.js"></script>
  <script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/app/<%=appLandingPageJsName%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/<%=ajaxDynamicListJSName%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/ajax_wu564.js"></script>
<%}else if("FAQ".equals(pageName)){%>
  <script type="text/javascript" src='<%=CommonUtil.getJsPath()%>/js/jquery/jquery.scrollto.js'></script>
  <script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/app/<%=appFaqPageJSName%>"></script>
<%}else if("CONTACTUS".equals(pageName)){%>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/autoComplete/ajax_wu564.js"></script>
  <script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/app/appContactUs.js"></script>
<%}%>
<jsp:include page="/jsp/common/abTesting.jsp"/>
<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
	<jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>
<%-- Facebook pixel tracking script included in head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/facebookPixelTracking.jsp"/>