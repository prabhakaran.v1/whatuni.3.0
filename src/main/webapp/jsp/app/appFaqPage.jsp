<%@ page import="WUI.utilities.CommonUtil" autoFlush="true" %>

<body class="ctus_scrl">
  <%-- GTM script included under body tag --%>

  <jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
    <jsp:param name="PLACE_TO_INCLUDE" value="BODY" />
  </jsp:include>
  <div id="top" class="faq_cont">
    <div class="cont_show faq_wrap">
      <jsp:include page="/jsp/app/include/includeALPHeader.jsp">
        <jsp:param name="fromPage" value="faq"/>
      </jsp:include>
      <div id="fullpage" class="inn_cont">
        <h2>Frequently Asked Questions</h2>		      			     
      </div>
    </div>
    <jsp:include page="/jsp/app/include/includeFaqContent.jsp"/>      
  </div>
  <div class="bk_to_top" style="display: none;" id="back-top">
    <a href="#top"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/bk_2_top.png",0)%>" alt="go to top"></a>
  </div>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/common/commonFunctions_wu572.js"></script>
  <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
  <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />
</body>