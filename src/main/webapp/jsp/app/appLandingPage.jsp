<%@ page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction" autoFlush="true" %>

<body>
 <% 
CommonFunction common = new CommonFunction();
String appStoreURL  =  common.getWUSysVarValue("WU_APP_STORE_URL");
String androidAppFlag = common.getWUSysVarValue("ANDROID_APP_ON_OFF");
String appStoreURLForAndroid = ("ON".equalsIgnoreCase(androidAppFlag)) ? common.getWUSysVarValue("ANDROID_APP_URL") : "";
String appText = "Whatuni for iPhone";
String clearingonoff = common.getWUSysVarValue("CLEARING_ON_OFF");
String url = "https://www.youtube.com/embed/N70Txq1Fip4?rel=1&wmode=1paque&enablejsapi=1;showinfo=1;controls=1";
if("ON".equalsIgnoreCase(clearingonoff)){
  url = "https://www.youtube.com/embed/xg4qUYnqOaE?rel=1&wmode=1paque&enablejsapi=1;showinfo=1;controls=1";
}
if("ON".equalsIgnoreCase(androidAppFlag)){
  appText = "the Whatuni App";  
}
%>
<%-- GTM script included under body tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
  <jsp:param name="PLACE_TO_INCLUDE" value="BODY" />
</jsp:include>
<%--Added for cookie policy pop-up by Hema.S on 31.07.2018_rel--%>
<jsp:include page="/jsp/home/cookiePopup.jsp"/>
<div class="cont_show">
  <jsp:include page="/jsp/app/include/includeALPHeader.jsp">
    <jsp:param name="fromPage" value="applanding"/>
  </jsp:include>
  <%if("ON".equalsIgnoreCase(clearingonoff)){%>
    <div id="fullpage" class="inn_cont clapp_txt">
  <%}%>
    <%if(!("ON".equalsIgnoreCase(clearingonoff))){%>
      <div id="fullpage" class="inn_cont">
    <%}%>
        <jsp:include page="/jsp/app/include/includeALPSignup.jsp"/>
    <!-- // Third section/2 -->
        <div class="section" id="slide2">
          <div class="section-inner">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                  <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 fl_right img_fld cs_sub_cont">
                      <div class="utu_vid">
                        <iframe src="<%=url%>" style="position:absolute;width:100%;height:100%;left:0" width="641" height="360" frameborder="0" allowfullscreen></iframe>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5 fl_left cs_sub_cont">
                      <div class="jumbotron"> <img src="<%=CommonUtil.getImgPath("/wu-cont/images/icons/video.svg",0)%>" class="img-responsive" alt="Responsive image">
                        <h2>At a glance</h2>
                        <p>Introducing <%=appText%>. All the course and university research functionality of our website, at your fingertips.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- // Third section/2 -->
        <div class="section" id="slide3">
          <div class="section-inner">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                  <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 img_fld fl_left cs_sub_cont"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/thumbnails/Page_2_phone@1x.png",0)%>" class="img-responsive" width="" alt="Search image" /> </div>
                      <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7 fl_right cs_sub_cont">
                        <div class="jumbotron">
                          <img src="<%=CommonUtil.getImgPath("/wu-cont/images/icons/search.svg",0)%>" class="img-responsive" alt="Course search image" />
                          <h2>Search universities and courses</h2>
                          <p>Save time and use our app to quickly find universities and courses according to your interests, qualifications and location.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="section" id="slide4">
            <div class="section-inner">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                    <div class="row">
                      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 fl_right img_fld cs_sub_cont"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/thumbnails/Page_3_phone@1x.png",0)%>" class="img-responsive" width="" alt="Quiz mobile image"> </div>
                      <div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 fl_left cs_sub_cont">
                        <div class="jumbotron"> 
                          <img src="<%=CommonUtil.getImgPath("/wu-cont/images/icons/quiz.svg",0)%>" class="img-responsive" alt="Quiz image">
                          <h2>Figure out what you want to study</h2>
                          <p>Totally confused? Chat with our virtual assistant who will match you up with suitable courses and universities.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="section" id="slide5">
            <div class="section-inner">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                    <div class="row">
                      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 img_fld fl_left cs_sub_cont"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/thumbnails/Page_4_phone@1x.png", 0)%>" class="img-responsive" width="" alt="A unique profile image"> </div>
                        <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7 fl_right cs_sub_cont">
                          <div class="jumbotron"> 
                           <img src="<%=CommonUtil.getImgPath("/wu-cont/images/icons/course_details.svg", 0)%>" class="img-responsive" alt="A unique viewpoint image">
                           <h2>A unique viewpoint</h2>
                           <p>Check out university and course information that you won't find anywhere else, including reviews from students who've already attended that university.</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="section" id="slide6">
              <div class="section-inner">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                      <div class="row">
                         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 fl_right img_fld cs_sub_cont"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/thumbnails/Page_5_phone@1x.png", 0)%>" class="img-responsive" width="" alt="Shortlist image"> </div>
                           <div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 fl_left cs_sub_cont">
                             <div class="jumbotron"> 
                               <img src="<%=CommonUtil.getImgPath("/wu-cont/images/icons/shortlist.svg", 0)%>" class="img-responsive" alt="Shortlist favourites image">
                               <h2>Shortlist favourites</h2>
                               <p>Narrow down your options and make picking the final 5 courses for your UCAS application a lot easier and faster.</p>
                             </div>
                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
               <div class="section" id="slide7">
                 <div class="section-inner">
                   <div class="container">
                     <div class="row">
                       <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                         <div class="row">
                           <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4  img_fld fl_left cs_sub_cont"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/thumbnails/Page_6_phone@1x.png", 0)%>" class="img-responsive" width="" alt="Responsive image"> </div>
                            <div class="col-xs-12 col-sm-8 col-md-7 col-lg-7 fl_right cs_sub_cont">
                              <div class="jumbotron"> 
                                <img src="<%=CommonUtil.getImgPath("/wu-cont/images/icons/enquiry.svg", 0)%>" class="img-responsive" alt="Ask Questions image">
                                <h2>Ask Questions</h2>
                                <p>Get as much information as you need as easily as possible by emailing the university or requesting a prospectus.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="section" id="slide8">
                  <div class="section-inner">
                    <div class="container">
                      <div class="row">
                        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-11 mid_algn">
                          <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 img_fld fl_right cs_sub_cont"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/thumbnails/Page_7_phone@1x.png", 0)%>" class="img-responsive" width="" alt="Responsive image"> </div>
                              <div class="col-xs-12 col-sm-8 col-md-7 col-lg-6 fl_left cs_sub_cont">
                                <div class="jumbotron"> 
                                  <img src="<%=CommonUtil.getImgPath("/wu-cont/images/icons/notifications.svg", 0)%>" class="img-responsive" alt="Responsive image">
                                  <h2>Never miss a deadline</h2>
                                  <p>Get push notifications and helpful reminders so that you don't miss out on information that affects you.</p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/common/commonFunctions_wu572.js"></script>
        <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
        <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />  
        <script type="text/javascript">    
          checkcookie();
       </script>
     </body>