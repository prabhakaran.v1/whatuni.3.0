<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.*"%>
<c:if test="${not empty requestScope.COLLEGE_IN_BASKET}">
<%String basketcid = (String) request.getAttribute("COLLEGEID_IN_INTERACTION");%>
<%String collegeName = request.getParameter("basketCollegeName") != null ? request.getParameter("basketCollegeName") : "";
String fromPage = request.getParameter("fromPage") != null ? request.getParameter("fromPage") : "";
  String gaInteractionCompare = "";
  String richProfileExist = request.getParameter("richProfile");
  String extraClass = "", extraText ="";
  if(richProfileExist!=null && "true".equals(richProfileExist)){
    extraClass  = "position:absolute;";
    extraText   = "<em></em>";
  }
  if("content-hub".equals(fromPage)) {
    gaInteractionCompare = "GAInteractionEventTracking(\"compare\", \"Compare\", \"Heart Icon\", \""+collegeName+"\");";
  }
  String CONTEXT_PATH = GlobalConstants.WU_CONTEXT_PATH; %>

<% if("content-hub".equals(fromPage)) {%>
  
  <c:if test="${requestScope.COLLEGE_IN_BASKET eq 'FALSE'}"> 
    
    <div class="com_sec" id="basket_div_<%=basketcid%>">
        <div onclick='<%=gaInteractionCompare%>addBasket("<%=basketcid%>", "C", this,"basket_div_<%=basketcid%>", "basket_pop_div_<%=basketcid%>", "", "", "<%=collegeName%>")' class="cmp_hov"><i class="fa fa-heart-o" aria-hidden="true"></i> COMPARE
        <span class="chktxt">Add to comparison<em></em></span>
        </div>
        <span class="loading_icon" id="load_<%=basketcid%>" style="display:none;<%=extraClass%>">
          <img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>">
        </span>
    </div>
    
    <div class="com_sec act" id="basket_pop_div_<%=basketcid%>" style="display:none;">
        <div onclick='addBasket("<%=basketcid%>", "C", this,"basket_div_<%=basketcid%>", "basket_pop_div_<%=basketcid%>");' class="cmp_hov act">
            <i class="fa fa-heart" aria-hidden="true"></i> COMPARE
        </div>
        <span class="loading_icon" id="load_<%=basketcid%>" style="display:none;<%=extraClass%>">
          <img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>">
        </span>
        <div id="defaultpop_<%=basketcid%>" class="ch_vcmp sta" style="display: none;">
          <%
          String userId = new SessionData().getData(request, "y");
          if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
          %>
              <a class="view_more" href="<%=CONTEXT_PATH%>/comparison">View comparison</a>
          <%}else{%>
              <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
          <%}%>
        </div>
        <div id="pop_<%=basketcid%>" class="ch_vcmp"></div>
    </div>
    
    
  </c:if>
  
  <c:if test="${requestScope.COLLEGE_IN_BASKET ne 'FALSE'}">
    
    <div class="com_sec" id="basket_div_<%=basketcid%>" style="display:none;">
        <div onclick='<%=gaInteractionCompare%>addBasket("<%=basketcid%>", "C", this,"basket_div_<%=basketcid%>", "basket_pop_div_<%=basketcid%>", "", "", "<%=collegeName%>")' class="cmp_hov"><i class="fa fa-heart-o" aria-hidden="true"></i> COMPARE
        <span class="chktxt">Add to comparison<%=extraText%><em></em></span>
        </div>
        <span class="loading_icon" id="load1_<%=basketcid%>" style="display:none;<%=extraClass%>">
          <img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>">
        </span>
    </div>
    
    <div class="com_sec act" id="basket_pop_div_<%=basketcid%>" >
        <div onclick='<%=gaInteractionCompare%>addBasket("<%=basketcid%>", "C", this,"basket_div_<%=basketcid%>", "basket_pop_div_<%=basketcid%>", "", "", "<%=collegeName%>")' class="cmp_hov act">
          <i class="fa fa-heart" aria-hidden="true"></i> COMPARE
        </div>
        <span class="loading_icon" id="load1_<%=basketcid%>" style="display:none;<%=extraClass%>">
          <img src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",0)%>">
        </span>
        <div id="defaultpop_<%=basketcid%>" class="ch_vcmp sta" style="display: block;">
          <%
          String userId = new SessionData().getData(request, "y");
          if(userId!=null && !"0".equals(userId) && !"".equals(userId)){
          %>
              <a class="view_more" href="<%=CONTEXT_PATH%>/comparison">View comparison</a>
          <%}else{%>
              <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
          <%}%>
        </div>
        <div id="pop_<%=basketcid%>" class="ch_vcmp" style="display:none;"></div>
    </div>
    
  </c:if>
  
<%} else {%>

  <%--Add to compare redesign, 03_Feb_2015 By Thiyagu G--%>
  <div class="hor_cmp">
    <c:if test="${requestScope.COLLEGE_IN_BASKET eq 'FALSE'}"> 
      <div class="cmlst" id="basket_div_<%=basketcid%>"> 
        <div class="compare">
        <a onclick='addBasket("<%=basketcid%>", "C", this,"basket_div_<%=basketcid%>", "basket_pop_div_<%=basketcid%>", "", "", "<%=collegeName%>")'>
          <span class="icon_cmp f5_hrt"></span>
          <span class="cmp_txt">Compare</span>
          <span class="loading_icon" id="load_<%=basketcid%>" style="display:none;<%=extraClass%>"></span>
          <span class="chk_cmp"><span class="chktxt">Add to comparison<%=extraText%><em></em></span></span>
        </a>        
        </div>
      </div>
  
      <div class="cmlst act" id="basket_pop_div_<%=basketcid%>" style="display:none;"> 
        <div class="compare">
        <a class="act" onclick='addBasket("<%=basketcid%>", "C", this,"basket_div_<%=basketcid%>", "basket_pop_div_<%=basketcid%>");'>
          <span class="icon_cmp f5_hrt hrt_act"></span>
          <span class="cmp_txt">Compare</span>
          <span class="loading_icon" id="load1_<%=basketcid%>" style="display:none;<%=extraClass%>"></span>
          <span class="chk_cmp"><span class="chktxt">Remove from comparison<%=extraText%><em></em></span></span>
        </a>      
        </div>
        <div id="defaultpop_<%=basketcid%>" class="sta" style="display: block;">
            <%
                String userId = new SessionData().getData(request, "y");
                if(userId!=null && !"0".equals(userId) && !"".equals(userId) ){
            %>
                <a class="view_more" href="<%=CONTEXT_PATH%>/comparison">View comparison</a>
            <%}else{%>
                <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
            <%}%>
          </div>
        <div id="pop_<%=basketcid%>" class="sta"></div>
      </div>
    </c:if>  
    <c:if test="${requestScope.COLLEGE_IN_BASKET ne 'FALSE'}">
      <div class="cmlst" id="basket_div_<%=basketcid%>" style="display:none;"> 
        <div class="compare">
        <a onclick='addBasket("<%=basketcid%>", "C", this,"basket_div_<%=basketcid%>", "basket_pop_div_<%=basketcid%>", "", "", "<%=collegeName%>");'>
          <span class="icon_cmp f5_hrt"></span>
          <span class="cmp_txt">Compare</span>
          <span class="loading_icon" id="load_<%=basketcid%>" style="display:none;<%=extraClass%>"></span>
          <span class="chk_cmp"><span class="chktxt">Add to comparison<%=extraText%><em></em></span></span>
        </a>
        </div>        
      </div>
      <div class="cmlst act" id="basket_pop_div_<%=basketcid%>"> 
        <div class="compare">
        <a class="act" onclick='addBasket("<%=basketcid%>", "C", this,"basket_div_<%=basketcid%>", "basket_pop_div_<%=basketcid%>");'>
          <span class="icon_cmp f5_hrt hrt_act"></span>
          <span class="cmp_txt">Compare</span>
          <span class="loading_icon" id="load1_<%=basketcid%>" style="display:none;<%=extraClass%>"></span>
          <span class="chk_cmp"><span class="chktxt">Remove from comparison<%=extraText%><em></em></span></span>
        </a>        
        </div>
        <div id="defaultpop_<%=basketcid%>" class="sta" style="display: block;">
            <%
                String userId = new SessionData().getData(request, "y");
                if(userId!=null && !"0".equals(userId) && !"".equals(userId)){
            %>
                <a class="view_more" href="<%=CONTEXT_PATH%>/comparison">View comparison</a>
            <%}else{%>
                <a onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');" class="view_more">View comparison</a>
            <%}%>
          </div>
        <div id="pop_<%=basketcid%>" class="sta" style="display:none;"></div>
      </div>
    </c:if>  
  </div>
<%}%>
</c:if>