<%Long startJspBuild = new Long(System.currentTimeMillis());%>
<%@ page import="WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction, WUI.utilities.CommonUtil, java.util.List,org.apache.commons.validator.GenericValidator" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>

     
      <%
      String token = ""+session.getAttribute("_synchronizerToken");  
      request.setAttribute("hitbox_eventtag", GlobalConstants.EMAIL_EVENT);
      String collegeId   =  (String) request.getAttribute("collegeId"); 
      String GA_newUserRegister   =  (String) request.getAttribute("GA_newUserRegister");//5_AUG_2014
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");    
    if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0){ 
      cpeQualificationNetworkId = "2";
    }request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);
    String subOrderItemId = (String)request.getAttribute("subOrderItemId");   
    

    String tmp_collegeId = request.getAttribute("collegeId") != null && request.getAttribute("collegeId").toString().trim().length() > 0 ? (String)request.getAttribute("collegeId") : "0";
    String tmp_collegeName = request.getAttribute("collegeName") != null && request.getAttribute("collegeName").toString().trim().length() > 0 ? (String)request.getAttribute("collegeName") : "";    
    CommonFunction common = new CommonFunction();
    String gaCollegeName1 = common.replaceURL(tmp_collegeName);
    com.wuni.util.seo.SeoUrls seoUrl = new com.wuni.util.seo.SeoUrls();
    String uniUrl = (seoUrl.construnctUniHomeURL(tmp_collegeId, tmp_collegeName)).toLowerCase();            
    String tmp_collegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? " "+(String)request.getAttribute("collegeNameDisplay") : "";    
    String tmp_courseId = request.getAttribute("courseId") != null && request.getAttribute("courseId").toString().trim().length() > 0 ? (String)request.getAttribute("courseId") : "0";
    
    String subOrderWebsite = (String)request.getAttribute("subOrderWebsite");
    request.setAttribute("hitbox_college_id", collegeId);
    request.setAttribute("hitbox_college_name", tmp_collegeName);
    int listSize = 0;
    String newUserFlag = (String)request.getAttribute("newUserFlag");
    String courseName = request.getAttribute("courseName") != null && request.getAttribute("courseName").toString().trim().length() > 0 ? (String)request.getAttribute("courseName") : "";    
    String postenquirysuggestionJsName = CommonUtil.getResourceMessage("wuni.post.enquiry.suggestion.js", null);
    %>

  <!--Changed new responsive design for 03-Nov-2015, by Thiyagu G-->
<body id="www-whatuni-com" onLoad="waitPreloadPage()"> 
<c:if test="${not empty requestScope.pixelTracking}">${requestScope.pixelTracking}</c:if> <!--18_NOV_2014 Added by Amir for pixel tracking in email success page-->
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">    
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>
    </div>  
  </div>  
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=postenquirysuggestionJsName%>"></script>
</header>

<section class="main_cnr pt40 pb40">
  <div class="ad_cnr">
    <div id="sub_container" class="content-bg">
      <div class="sub_cnr abt">
        <div class="fr rht_sec main_success">
          <div class="success p20">
            <h6 class="fnt_lbd pb5">Bish, Bash, Bosh. Done.</h6>
            <p class="txt fnt_lrg m0 p0">
              <i class="icon-ok mr5"></i>
              Your email has been successfully sent to following uni.     
              <%--Post to facebook code added by Prabha on 31_05_16--%>
              <a class="btn1 fbbtn suc_fbbtn" onclick="openFbPopUpFb('<%=uniUrl%>');">Post to facebook
                <i class="fa fa-facebook fa-1_5x"></i>
                <i class="fa fa-long-arrow-right"></i>
              </a>
              <%--End of post to facebook code--%>
         </div> 
         <div class="pros_scroll">
            <c:if test="${not empty requestScope.prospectusSentCollegeNames}">
              <ul class="uni_menu">
               <c:forEach var="prospectusSentCollegeNames" items="${requestScope.prospectusSentCollegeNames}">
                <li><a>
                  <span class="fl uni fnt_lrg">${prospectusSentCollegeNames.collegeDisplayName}</span>
                  <input type="hidden" id="prospectusDispCollegeName" name="prospectusDispCollegeName" value="${prospectusSentCollegeNames.collegeDisplayName}"/>
                  <i class="fa icon-ok fa-1_5x fr"></i>
                  </a>
                </li>
                </c:forEach>
              </ul> 
            </c:if>
          </div> 
          <input type="hidden" id="prospectusCourseName" name="prospectusCourseName" value="<%=courseName%>"/>
          <input type="hidden" id="prospectusGaCollegeName" name="prospectusGaCollegeName" value="<%=gaCollegeName1%>"/>
        <%-- Commented on 20_Nov_2018 by Prabha
        <logic:present name="postEnquiryList" scope="request" >
         
            <form action="/degrees/send-post-enquiry.html" method="post" id="postenqiry" name="postenquiryform">            
              <input type="hidden" name="postSubject" value="<%=(String)request.getAttribute("postsubject")%>"/>
              <input type="hidden" name="postStudyLevelId" value="<%=(String)request.getAttribute("poststudyLevelId")%>"/>
              <input type="hidden" id="insDimYOE" name="postYearofcourse" value="<%=(String)request.getAttribute("postyearofcourse")%>"/>    
              <input type="hidden" id="insDimQualType" value="<%=(String)request.getAttribute("cDimQualificationType")%>"/>
              <input type="hidden" id="insDimLevelType" value="<%=(String)request.getAttribute("cDimLevel")%>"/>
              <input type="hidden" id="insDimUID" value="<%=(String)request.getAttribute("cDimUID")%>"/>            
              <input type="hidden" name="_synchronizerToken" value="<%=token%>"/> --%>
              <%-- wu582_20181023 - Sabapathi: added screenwidth for stats logging  --%>
	       <%--<input type="hidden" id="screenwidth" name="screenwidth" value=""/>    
         
             <div class="list_cnr fl cf pb40">
                <h3 class="fnt_lrg">                  
                  Other students have also enquired to:
                  <a id="sr_selectAll" onclick="javascript:selectAllForDld('SELECT','postenqiry')" class="select_all fr fnt_lrg">SELECT ALL</a>
                  <a id="sr_DeSelectAll" onclick="javascript:selectAllForDld('DESELECT','postenqiry')" class="select_all fr fnt_lrg" style="display:none;"  >DESELECT ALL</a>
                </h3>
                <div class="list_view fl">
                  <logic:iterate id="postEnquiryList" name="postEnquiryList" scope="request" indexId="i" type="com.wuni.util.valueobject.ql.PostEnquiryVO">        
                     <bean:define id="postCollegeId" name="postEnquiryList" property="collegeId" type="java.lang.String"/> 
                     <bean:define id="postCollegeDisplayName" name="postEnquiryList" property="collegeDisplayName"/>
                     <bean:define id="postCollegeName" name="postEnquiryList" property="collegeName" type="java.lang.String"/> 
                     <%String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(postCollegeName);%>
                     <div id="dpRow_<%=i%>" class="row" onclick="javascript:addDLProspectus('<%=i%>');">
                       <div class="c1">
                         <div class="img_ppn">
                           <img alt="<bean:write name="postEnquiryList" property="collegeDisplayName"/>" title="<bean:write name="postEnquiryList" property="collegeDisplayName"/>" src="<bean:write name="postEnquiryList" property="collegeLogo"/>">
                         </div>
                       </div>
                       <div class="c2 fnt_lbd">
                         <span class="fnt_lbd">
                         <logic:present name="postEnquiryList" property="courseTitle">
                           <bean:write name="postEnquiryList" property="courseTitle"/><br/> 
                         </logic:present>
                         <bean:write name="postEnquiryList" property="collegeDisplayName"/> </span>
                         <input type="hidden" name="emailcheck" value="<%=postCollegeId%>#<bean:write name="postEnquiryList" property="subOrderItemId"/>#<bean:write name="postEnquiryList" property="courseId"/>#<%=postCollegeDisplayName%>#<%=postCollegeName%>#<bean:write name="postEnquiryList" property="emailPrice"/>" id="hidden_dpRow_<%=i.intValue()%>" />                         
                       </div>
                       <a onclick="javascript:addDLProspectus('<%=i%>');">
                         <i id="addRemoveIcon_<%=i%>" class="fa fa-plus fa-1_5x" onclick="javascript:addDLProspectus('<%=i%>');"></i>
                       </a>
                     </div>         
                   </logic:iterate>
                 </div>             
                <div class="fr w100p mt20">
                  <a class="btn1 bg_orange fr mt10" onclick="postEmail(this);" >
                      REQUEST INFO
                     <i class="fa fa-long-arrow-right"></i>
                  </a>
                </div> 
              </div>   
              <div class="fr fr mt20">
                <span class="fnt_lbd fnt_14">Return to</span>
                <a class="link_blue fnt_lbd fnt_14" href="<%=new CommonFunction().getDomainName(request, "N")%>">homepage</a>
              </div>
            </form>
          </logic:present>
         --%>
         
        </div>
      </div>
    </div>
  </div>
</section>
<jsp:include page="/jsp/common/wuFooter.jsp" />
<c:if test="${not empty sessionScope.postEnquiryList}">
  <script type="text/javascript">
  insightIntLogging("<%=collegeId%>","post enquiry");
  </script>
</c:if>
<script type="text/javascript">
function postEmail(obj){
  idArr = new Array;
  var j = 0;
  var myform = document.getElementById("postenqiry");
  var inputs = myform.getElementsByClassName("act row"); 
  if(inputs.length == 0){
   alert("Please select to enquiry");
   return false;
  }
  for(var i = 0; i < inputs.length; i++){
         idArr[j] = inputs[i].id;
         j = j+1;
  } 
  var reminputs = myform.getElementsByClassName("row");  
  for(var k = 0; k < reminputs.length; k++){
    var idd = reminputs[k].id;
    if($$D(idd).className == "row"){    
      var element = document.getElementById("hidden_"+idd);
          element.parentNode.removeChild(element);
    }
  }
  for (i=0;i<idArr.length;i++) {
    if(document.getElementById(idArr[i]) != null){   
      var checkValue = document.getElementById("hidden_"+idArr[i]).value;      
        var splitValue = checkValue.split("#");
        var collegeId = splitValue[0];
        var subOrderItemId = splitValue[1];        
        var collegeName = splitValue[4];        
        var emaiPrice = splitValue[5];        
      if (i==idArr.length-1) {           
          ga('send', 'pageview', {'hitCallback': function(){document.postenquiryform.submit();}}); //Added by Amir for UA logging for 24-NOV-15 release
          GAInteractionEventTracking('emailsubmit', 'interaction', 'Email', collegeName, Number(emaiPrice));          
          insightIntLogging(collegeId,'email submitted');
        
      }
    }
  }  
}
// wu582_20181023 - Sabapathi: update screenwidth in hidden
updateScreenWidth();
</script>
<%if(!GenericValidator.isBlankOrNull(newUserFlag) && "Y".equals(newUserFlag)){%>
<script type="text/javascript">
  GARegistrationLogging('register', "Email", '<%=gaCollegeName1%>','<%=GA_newUserRegister%>');
</script>
<%}%>
</body>