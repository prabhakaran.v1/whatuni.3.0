<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonUtil, java.util.List, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator" %>
<%
int listSize = 0;   
String token = ""+session.getAttribute("_synchronizerToken");  
String collegeId   =  (String) request.getAttribute("collegeId"); 
String dwnBtnText = "";
String dwnBtnFlag = "";
String poCourseId="";
%>
<%-- Commented on 20_Nov_2018 by Prabha
<logic:present name="postEnquiryList" scope="request" >

  <form action="/degrees/send-mywhatuni-postenquiry.html" method="post" id="postprospecuts" name="postprospectusform">            
    <input type="hidden" name="mywhatunitype" value="MYPROSPECTUS"/>
    <input type="hidden" id="insDimYOE" name="postYearofcourse" value="<%=(String)request.getAttribute("postyearofcourse")%>"/>    
    <input type="hidden" id="insDimQualType" value="<%=(String)request.getAttribute("cDimQualificationType")%>"/>
    <input type="hidden" id="insDimLevelType" value="<%=(String)request.getAttribute("cDimLevel")%>"/>
    <input type="hidden" id="insDimUID" value="<%=(String)request.getAttribute("cDimUID")%>"/>            
    <input type="hidden" name="_synchronizerToken" value="<%=token%>"/>--%>
    <%-- wu582_20181023 - Sabapathi: added screenwidth for stats logging  --%>
  <%--  <input type="hidden" id="screenwidth" name="screenwidth" value=""/>
    <div class="list_cnr fl cf pb40">
      <h3 class="fnt_lrg">
        Other students also downloaded these... 
        <a id="sr_selectAll" onclick="javascript:selectAllForDld('SELECT','postprospecuts')" class="select_all fr fnt_lrg">SELECT ALL</a>
        <a id="sr_DeSelectAll" onclick="javascript:selectAllForDld('DESELECT','postprospecuts')" class="select_all fr fnt_lrg" style="display:none;"  >DESELECT ALL</a>
      </h3>
      <div class="list_view fl">
        <logic:iterate id="postEnquiryList" name="postEnquiryList" scope="request" indexId="i" type="com.wuni.util.valueobject.ql.PostEnquiryVO">
           <bean:define id="postCollegeId" name="postEnquiryList" property="collegeId" type="java.lang.String"/>
           <bean:define id="postCollegeDisplayName" name="postEnquiryList" property="collegeDisplayName"/>
           <bean:define id="postCollegeName" name="postEnquiryList" property="collegeName" type="java.lang.String"/>
           <bean:define id="postInteractionType" name="postEnquiryList" property="interactionType" type="java.lang.String"/>
           <logic:notEmpty name="postEnquiryList" property="courseId">
            <bean:define id="postCourseId" name="postEnquiryList" property="courseId" type="java.lang.String"/>
            <%poCourseId=postCourseId;%>
           </logic:notEmpty>
           <logic:empty name="postEnquiryList" property="courseId">
           <%poCourseId="0";%>
           </logic:empty>
           <%String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(postCollegeName);dwnBtnFlag=dwnBtnFlag+postInteractionType;%>
           <div id="dpRow_<%=i%>" class="row" onclick="javascript:addDLProspectus('<%=i%>');">
             <div class="c1">
               <div class="img_ppn">
                 <img alt="<bean:write name="postEnquiryList" property="collegeDisplayName"/>" title="<bean:write name="postEnquiryList" property="collegeDisplayName"/>" src="<bean:write name="postEnquiryList" property="collegeLogo"/>">
               </div>
             </div>
             <div class="c2 fnt_lbd">
               <span class="fnt_lbd"> <bean:write name="postEnquiryList" property="collegeDisplayName"/> </span>
               <input type="hidden" name="emailcheck" value="<%=postCollegeId%>#<bean:write name="postEnquiryList" property="subOrderItemId"/>#<bean:write name="postEnquiryList" property="cpeQualificationNetworkId"/>#<bean:write name="postEnquiryList" property="collegeDisplayName"/>#<bean:write name="postEnquiryList" property="dpFlag"/>#<bean:write name="postEnquiryList" property="dpURL"/>#<%=postCollegeName%>#<bean:write name="postEnquiryList" property="emailPrice"/>#<%=poCourseId%>" id="hidden_dpRow_<%=i.intValue()%>" />
             </div>
             <a onclick="javascript:addDLProspectus('<%=i%>');">
               <i id="addRemoveIcon_<%=i%>" class="fa fa-plus fa-1_5x" onclick="javascript:addDLProspectus('<%=i%>');"></i>
             </a>
           </div>         
         </logic:iterate>
       </div>             
      <div class="fr w100p mt20">
        <a class="btn1 bg_orange fr mt10" onclick="postProspectus(this);" >
          <%
            if(dwnBtnFlag.contains("RP") && dwnBtnFlag.contains("DP")){
              dwnBtnText = "GET/DOWNLOAD PROSPECTUS";
            }else if(dwnBtnFlag.contains("RP") && !dwnBtnFlag.contains("DP")){
              dwnBtnText = "GET PROSPECTUS";
            }else if(dwnBtnFlag.contains("DP") && !dwnBtnFlag.contains("RP")){
              dwnBtnText = "DOWNLOAD PROSPECTUS";
            }
          %>
            <%=dwnBtnText%>
           <i class="fa fa-long-arrow-right"></i>
        </a>
      </div> 
    </div>    
    <div class="fr fr mt20">
      <span class="fnt_lbd fnt_14">Return to</span>
      <a class="link_blue fnt_lbd fnt_14" href="<%=new CommonFunction().getDomainName(request, "N")%>">homepage</a>
    </div>
  </form>
</logic:present>--%>
<c:if test="${not empty sessionScope.postEnquiryList}">
<script type="text/javascript">    
  //insightIntLogging("<%=collegeId%>","post enquiry");
</script>
</c:if>

<script type="text/javascript">
function postProspectus(obj){
  idArr = new Array;
  var j = 0;
  var myform = document.getElementById("postprospecuts");
  var inputs = myform.getElementsByClassName("act row"); 
  if(inputs.length == 0){
   alert("Please select to download");
   return false;
  }
  for(var i = 0; i < inputs.length; i++){
         idArr[j] = inputs[i].id;
         j = j+1;
  } 
  var reminputs = myform.getElementsByClassName("row");  
  for(var k = 0; k < reminputs.length; k++){
    var idd = reminputs[k].id;
    if($$D(idd).className == "row"){    
      var element = document.getElementById("hidden_"+idd);
          element.parentNode.removeChild(element);
    }
  }  
  for (i=0;i<idArr.length;i++) {    
    if(document.getElementById(idArr[i]) != null){       
      var checkValue = document.getElementById("hidden_"+idArr[i]).value;
        var splitValue = checkValue.split("#");
        var collegeId = splitValue[0];
        var subOrderItemId = splitValue[1];
        var networkId = splitValue[2];
        var collegeName = splitValue[6];
        var dpFlag = splitValue[4];
        var emaiPrice = splitValue[7];
        var dpURL = "";
        //Changed event action prospectus-dl to webform by Sangeeth.S for July_3_18 rel
        if (i==idArr.length-1) { 
          ga('send', 'pageview', {'hitCallback': function(){document.postprospectusform.submit();}}); //Added by Amir for UA logging for 24-NOV-15 release
        if(dpFlag == 'Y'){
          GAInteractionEventTracking('dpsubmit', 'interaction', 'prospectus webform', collegeName, Number(emaiPrice));
          dpURL = splitValue[5];
          window.open(dpURL, '_blank');
          insightIntLogging(collegeId,'download prospectus submitted');
        }else{          
          GAInteractionEventTracking('prospectussubmit', 'interaction', 'Prospectus', collegeName, Number(emaiPrice));
          insightIntLogging(collegeId,'prospectus submitted');
        }
      }else{        
        if(dpFlag == 'Y'){          
          GAInteractionEventTracking('dpsubmit', 'interaction', 'prospectus webform', collegeName, Number(emaiPrice));
          dpURL = splitValue[5];
          window.open(dpURL, '_blank');
          insightIntLogging(collegeId,'download prospectus submitted');
        }else{          
          GAInteractionEventTracking('prospectussubmit', 'interaction', 'Prospectus', collegeName, Number(emaiPrice));
          insightIntLogging(collegeId,'prospectus submitted');
        }
      }
    }
  }  
}
// wu582_20181023 - Sabapathi: update screenwidth in hidden
var $ = jQuery.noConflict();
$(document).ready(function(){
  updateScreenWidth();
});
</script>
