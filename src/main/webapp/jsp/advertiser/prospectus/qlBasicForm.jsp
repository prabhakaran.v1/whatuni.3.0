<%Long startJspBuild = new Long(System.currentTimeMillis());%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.SessionData, WUI.utilities.CommonFunction" %>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<html>

 <head>
      <jsp:include page="/include/htmlTitle.jsp">
        <jsp:param name="indexFollowFlag" value="noindex,follow"/>
      </jsp:include>
     <%@include  file="/jsp/common/includeMainCSS.jsp" %>
     <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
    <link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>     
<%
String nonmywu = request.getParameter("nonmywu") != null ? request.getParameter("nonmywu") : "";
    String pnotes = request.getParameter("pnotes") != null ? request.getParameter("pnotes") : "";
    String submitType = request.getParameter("submitType") != null ? request.getParameter("submitType") : "";
    String referrerURL_GA = request.getAttribute("refferalUrl") != null ? (String)request.getAttribute("refferalUrl") : "";
    String vwcid = request.getParameter("vwcid") != null ? request.getParameter("vwcid") : "";
    String vwurl = request.getParameter("vwurl") != null ? request.getParameter("vwurl") : "";
    String vwcourseId = request.getParameter("vwcourseId") != null ? request.getParameter("vwcourseId") : "";    
    String vwsid = request.getParameter("vwsid") != null ? request.getParameter("vwsid") : "";
    String vwnid = request.getParameter("vwnid") != null ? request.getParameter("vwnid") : "";
    String vwprice  = request.getParameter("vwprice") != null ? request.getParameter("vwprice") : "";
    String vwcname  = request.getParameter("vwcname") != null ? request.getParameter("vwcname") : "";
    String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
    String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
    String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
    String facebookLoginJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.facebook.login.js");
    String commonUserProfileJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.user.profile.js");    
    String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
    String enquiryExistFlag = (String)request.getAttribute("enquiryExistFlag");%>
<script type="text/javascript" language="javascript">
  var dev = jQuery.noConflict();
  dev(document).ready(function(){
    adjustStyle();
  });
  adjustStyle();
  
  function jqueryWidth() {
    return dev(this).width();
  } 
  function adjustStyle() {
    var width = document.documentElement.clientWidth;
    var path = "";   
    dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
    dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
    if (width <= 480) {    
      if (dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }
      <%if(("LIVE").equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}%> 
      dev("#hdr_menu").css("display", "none");
      dev("#hdrmenu2").css("display", "block");
    } else if ((width > 480) && (width <= 992)) {     
      if (dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      } 
      <%if(("LIVE").equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}%>      
      dev("#hdr_menu").css("display", "none");
      dev("#hdrmenu2").css("display", "block");
    }else {    
      if (dev("#viewport").length > 0) {
        dev("#viewport").remove();
      } 
      document.getElementById('size-stylesheet').href = "";       
      dev("#hdr_menu").css("display", "none");
      dev("#hdrmenu2").css("display", "none");
      dev(".hm_srchbx").hide();
    }
  }
  dev(window).on('orientationchange', orientationChangeHandler);
  function orientationChangeHandler(e) {
    setTimeout(function() {
      dev(window).trigger('resize');
    }, 500);
  }
  dev(window).resize(function() {    
    var screenWidth = jqueryWidth();
    var currentElement;
//    if(screenWidth <= 480){  //Phone view
//      dev("#hdr_menu").css("display", "none");
//      dev("#hdrmenu2").css("display", "block");
//    }else if ((screenWidth > 480) && (screenWidth <= 992)){  //Tablet view
//      dev("#hdr_menu").css("display", "none");
//      dev("#hdrmenu2").css("display", "block");
//    }else if(screenWidth > 992){  //Desktop view
//      dev("#hdr_menu").css("display", "none");
//      dev("#hdrmenu2").css("display", "none");
//    }  
    adjustStyle();
  });
  function jqueryWidth() {
    return dev(this).width();
  }  
</script>
  <%
    String profileType = (String) request.getAttribute("profileType");
           profileType = !GenericValidator.isBlankOrNull(profileType)? profileType : "";
    String widgetId = new CommonFunction().getWUSysVarValue("WU_EMAIL_WIDGETID");    
  %>    
    <script type="text/javascript" language="javascript" src="//connect.facebook.net/en_US/all.js"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=facebookLoginJSName%>"> </script>    
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=commonUserProfileJSName%>"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/modal-message.js"> </script>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/PostcodeValidate.js"> </script>
    
    <%
    String emailDefaultText = "";
    String postEnquiryFlag = request.getAttribute("postEnquiryRequest") != null && request.getAttribute("postEnquiryRequest").toString().trim().length() > 0 ? (String) request.getAttribute("postEnquiryRequest") : "";
    %>


<body id="www-whatuni-com" data-twttr-rendered="true">
<div class="lbox_gry lbox_pros">
  <header class="clipart bg1">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" >          
        </jsp:include>
      </div>
    </div>
    </header>
    
  <section class="lbox_cont">
		<section class="lbox_wrap qlfrm nw_frm remv_gplus cl_csnt">
    <div id="content-bg" >
      <div class="lb_reqpros">
        <h2 class=" fnt_lbd">Request prospectuses</h2>
        <c:if test="${empty sessionScope.userInfoList}" >
          <div class="enq_signin">Already have an account?<a class="sgntx" title="Sign in">Sign in </a>
              <jsp:include page="/jsp/advertiser/include/AlreadyRegisteredPod.jsp" />  
          </div>
        </c:if>
      </div>      
      <div id="content-blk">        
        <aside class="lbx_rht">
          <div id="content-right" class="bprl">
           <c:if test="${not empty requestScope.porspectusList}">
            <div id="prospectusPopDiv">
              <jsp:include page="/prospectus/interactionProspectusPod.jsp"/>
             </div>
           </c:if>
        </div>   
        </aside>
        <article class="lbx_lft lft_wid remv_gplus">
          <div id="content-left" class="fl sub_cnr">
          <div class="lb_reqpros">
            <h2 class=" fnt_lbd">Request prospectuses</h2>            
            	<%--Sign in concertina code added by Prabha on 15-DEC-2015--%>
             <c:if test="${empty sessionScope.userInfoList}" >
               <div class="enq_signin">Already have an account?<a class="sgntx" title="Sign in">Sign in </a>
                    <jsp:include page="/jsp/advertiser/include/AlreadyRegisteredPod.jsp" />  
               </div>
             </c:if>
             <%--End of Sign in concertina code--%>            
          </div>
          <div id="pros-pod">
            <div id="ql-fm" class="qlf-nw">
              <div class="clear"></div>
              <div id="jsFormValidationErrors" class="red"></div>
              <html:form action="/degrees/orderprospectus/send-college-email.html"  cssClass="qlform nw_frm"  id="qlform" commandName="qlformbean">
                  <tags:token/>
                  <jsp:include page="/jsp/advertiser/prospectus/qlbasicYourDetails.jsp"/>                  
               
                <html:hidden path="qlFlag" id="qlFlag"/>          
                 <html:hidden path="typeOfEnquiry" id="typeOfEnquiry"/>  
                 <html:hidden path="bulkProspectusType" id="bulkProspectusType"/> 
                 <html:hidden path="requestUrl" id="requestUrl"/>
                 <html:hidden path="refferalUrl" id="refferalUrl"/>
                 <html:hidden path="postButtonFlag"/>
                 <input type="hidden" name="btSubmit" value="ProsProceed" />
                 <html:hidden path="autoEmailFlag" id="autoEmailFlag"/>
                 <input type="hidden" name="widgetId" id="widgetId" value="<%=widgetId%>"/>
                 <c:if test="${not empty requestScope.profileType}">
                   <html:hidden path="profileType" id="profileType"/>
                 </c:if>        
                 <input type="hidden" name="hidden_user_id" id="hidden_user_id" value='<%=new SessionData().getData(request,"y")%>'/>
                 <input type="hidden" name="robots" value="nofollow" />
                 
                 <input type="hidden" id="downloadurl" value=""/>
                  <input type="hidden" id="nonmywu" value="<%=nonmywu%>"/>
                  <input type="hidden" id="pnotes" value="<%=pnotes%>"/>
                  <input type="hidden" id="submitType" value="<%=submitType%>"/>
                  <input type="hidden" id="pdfName" value=""/>
                  <input type="hidden" id="pdfId" value=""/>
                  <input type="hidden" id="referrerURL_GA" value="<%=referrerURL_GA%>"/>
                  <input type="hidden" id="regLoggingType" value="global-nav"/>
                  <input type="hidden" id="vwcid" value="<%=vwcid%>"/>
                  <input type="hidden" id="vwurl" value="<%=vwurl%>"/>
                  <input type="hidden" id="vwsid" value="<%=vwsid%>"/>
                  <input type="hidden" id="vwnid" value="<%=vwnid%>"/>
                  <input type="hidden" id="vwprice" value="<%=vwprice%>"/>
                  <input type="hidden" id="vwcourseId" value="<%=vwcourseId%>"/>
                  <input type="hidden" id="vwcname" value="<%=vwcname%>"/>
                  <input type="hidden" id="emailAlreadyExistsflag" value=""/>
                  <input type="hidden" id="networkFlag" value="0" />
                  <input type="hidden" id="enquiryExistFlag" value="<%=enquiryExistFlag%>"/>
                  <%-- wu582_20181023 - Sabapathi: added screenwidth for stats logging  --%>
		  <input type="hidden" id="screenwidth" name="screenwidth" value=""/>
              </html:form>
            </div>
          </div>
        </div>
        </article>
        <br clear="all">
      </div>    
    </div>
  </section>
  </section>
  
  <jsp:include page="/jsp/common/wuFooter.jsp" /> 
</div>
 <%if(session.getAttribute("userInfoList") == null){%>
   <script type="text/javascript">
     loadEmailCookies();
     //disableUKresidentCollegeEmail();
    </script>  
 <%}%>   
<%-- DONT ALTER THIS: mainly used to track time taken details --%>

<script type="text/javascript">
function setHeaderDisplayNone(){
  document.getElementById("hdr_menu").style.display = "none";
  document.getElementById("hdrmenu2").style.display = "none";
}
<c:if test="${not empty sessionScope.userInfoList}">
<c:if test="${not empty sessionScope.usermessage}">
  if(document.getElementById('step1') != null){
    document.getElementById('step1').style.display = 'none';
  }
  document.getElementById('step2').style.display = 'block';
</c:if>
</c:if>
</script>   
<script type="text/javascript">
 setEventListeners();
</script>
<script type="text/javascript">
  var dev = jQuery.noConflict();
    dev(document).ready(function () {
    dev("#fname").focus();
    dev('.adiptxt').blur(function () {
      var x = dev(this).val();
      if (x != "") {
        dev(this).next('label').addClass("top");
      } else {
        dev(this).next('label').removeClass("top");
      }
    });    
    dev('.adiptxt').each(function(index) {      
      var xy = dev(this).val();
      if (xy != "") {
        dev(this).next('label').addClass("top");
      } else {
        dev(this).next('label').removeClass("top");
      } 
    });
  });
  // wu582_20181023 - Sabapathi: update screenwidth in hidden
  updateScreenWidth();
</script>
</body>
</html>