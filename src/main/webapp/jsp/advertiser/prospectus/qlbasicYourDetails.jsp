<%Long startJspBuild = new Long(System.currentTimeMillis());%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,com.wuni.advertiser.ql.QLCommon,WUI.utilities.SessionData, WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>  
  <% 
    String profileType = (String) request.getAttribute("profileType");
           profileType = !GenericValidator.isBlankOrNull(profileType)? profileType : "";
  
    String enquiryType = ((com.wuni.advertiser.ql.form.QLFormBean)request.getAttribute("qlformbean")).getTypeOfEnquiry(); 
    String flag = new QLCommon().getStudyLevelFlagFromProspectus(request);
    String emailDomainJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.email.domain.js");
    CommonFunction common = new CommonFunction();
    String[] yoeArr = common.getYearOfEntryArr();
    String YOE_1 = yoeArr[0];
    String YOE_2 = yoeArr[1];
    String YOE_3 = yoeArr[2];
    String YOE_4 = yoeArr[3];
    
    String yearOfEntry1 = YOE_1;
  %>

<div class="crm_ql-form base">  
  <div id="step2" class="crm_con" style="display:block;">
    <!--Redesign the layout for 19_Apr_2016, By Thiyagu G-->
    <h3 class="fnt_lrg fnt_24 mb20 mt20">Your details</h3>
    <fieldset class="row-fluid">      
       <fieldset class="w50p fl adlog" id="divfname"> 
         <c:if test="${empty qlformbean.firstName}">
           <html:input  path="firstName" id="fname" autocomplete="off" maxlength="40" cssClass="ql-inpt adiptxt usr" value="" onfocus="showTooltip('firstNameYText');" onblur="hideTooltip('firstNameYText');enquiryValidations(this.id);" />
           <label class="lbco" for="fname">First name*</label>
         </c:if>
         <c:if test="${not empty qlformbean.firstName}">
           <html:input  path="firstName" id="fname" autocomplete="off" maxlength="40" cssClass="ql-inpt adiptxt usr" onfocus="showTooltip('firstNameYText');" onblur="hideTooltip('firstNameYText');enquiryValidations(this.id);" />
           <label class="lbco" for="fname">First name*</label>
         </c:if>
         <span class="cmp" id="firstNameYText" style="display:none;"> 
           <div class="hdf5"></div>
           <div>We'd like this information so we don't call you by the wrong name. That'd be kinda rude.</div>
           <div class="linear"></div>
         </span>
         <div class="qler1" id="errorfname" style="display:none;"></div>
      </fieldset>
      <fieldset class="w50p fr adlog" id="divlname">
        <c:if test="${empty qlformbean.firstName}">
           <html:input path="lastName" id="lname" autocomplete="off" maxlength="40" cssClass="ql-inpt adiptxt usr" value="" onblur="enquiryValidations(this.id);" />
           <label class="lbco" for="lname">Last name*</label>
        </c:if>
        <c:if test="${not empty qlformbean.firstName}">
           <html:input  path="lastName" id="lname" autocomplete="off" maxlength="40" cssClass="ql-inpt adiptxt usr" onblur="enquiryValidations(this.id);" />
           <label class="lbco" for="lname">Last name*</label>
        </c:if>
       <div class="qler1" id="errorlname" style="display:none;"></div>               
      </fieldset>
    </fieldset>
    <fieldset class="row-fluid indx_pos alst_pos">          
      <fieldset id="divEmail" class="w100p adlog">
        <c:if test="${not empty requestScope.isExistsBasicFormEmail}"> 
          <html:input  path="emailAddress" id="email" maxlength="120" cssClass="ql-inpt adiptxt usr" readonly="true" onkeydown="hideEmailDropdown(event, 'autoEmailId'); return false" onfocus="this.blur();"/>
          <label class="lbco" for="email">Email address*</label>
        </c:if>
      <c:if test="${empty requestScope.isExistsBasicFormEmail}">
        <c:if test="${empty qlformbean.emailAddress}">
          <html:input path="emailAddress" id="email" autocomplete="off" maxlength="120" cssClass="ql-inpt adiptxt usr" value="" onfocus="showTooltip('emailYText');" onblur="hideTooltip('emailYText');" onkeydown="hideEmailDropdown(event, 'autoEmailId');"/>
          <label class="lbco" for="email">Email address*</label>
        </c:if>
        <c:if test="${not empty qlformbean.emailAddress}">
          <html:input path="emailAddress" id="email" autocomplete="off" maxlength="120" cssClass="ql-inpt adiptxt usr" onfocus="showTooltip('emailYText');" onblur="hideTooltip('emailYText');" onkeydown="hideEmailDropdown(event, 'autoEmailId');"/>
          <label class="lbco" for="email">Email address*</label>
        </c:if>
      </c:if>
      <span id="emailYText" class="cmp rght" style="display: none;">
          <div class="hdf5"></div>
          <div>Tell us your email and we'll reward you with...an email.</div>
          <div class="linear"></div>
       </span>
      <div class="qler1" id="errorEmail" style="display:none;"></div>
       <c:if test="${not empty requestScope.errEmailExists}">
         <div class="qler1" id="errorEmailExist"><html:errors path="emailAddress"/></div>
       </c:if>
    </fieldset>
  </fieldset>
  <fieldset class="row-fluid indx_pos">          
      <fieldset id="divPassword" class="w100p adlog">
      <c:if test="${empty requestScope.isExistsBasicFormEmail}">
        <c:if test="${empty qlformbean.emailAddress}">
          <html:password path="password" id="password" maxlength="20" cssClass="ql-inpt adiptxt usr " autocomplete="off" onblur="enquiryValidations(this.id);" />
          <label for="password" class="lbco" id="emailId_lbl">Password*</label> 
          <span class="pwd_eye"><a onblur="javascript:hideprospectuspassword('password');" href="javascript:showProspectusPassword('password');" onclick="showProspectusPassword('password');"><i id="eyeId" onclick="showProspectusPassword('password');" class="fa fa-eye" aria-hidden="true"></i></a></span>
          <div class="qler1" id="errorPassword" style="display:none;"></div>
        </c:if>
        <c:if test="${not empty qlformbean.emailAddress}">
        <c:if test="${not empty requestScope.errEmailExists}">
          <html:password path="password" id="password" maxlength="20" cssClass="ql-inpt adiptxt usr " autocomplete="off" onblur="enquiryValidations(this.id);" />
          <label for="password" class="lbco" id="emailId_lbl">Password*</label> 
          <span class="pwd_eye"><a onblur="javascript:hideprospectuspassword('password');" href="javascript:showProspectusPassword('password');" onclick="showProspectusPassword('password');"><i id="eyeId" onclick="showProspectusPassword('password');" class="fa fa-eye" aria-hidden="true"></i></a></span>
          <div class="qler1" id="errorPassword" style="display:none;"></div>
        </c:if>
        </c:if>
      </c:if>
      </fieldset>
      </fieldset>
      
  
  <fieldset class="row-fluid mt20 mb22 lbx_mbt">
      <fieldset class="w100p mb10 mt10"><label class="lbco">WHEN WOULD YOU LIKE TO START? *</label></fieldset>
      <fieldset class="ql-inp" id="yoeFeild"><%--8_OCT_2014_REL modified by Priyaa for year of entry--%>
        <span class="ql_rad" id="yyoe1" onmouseout="hideTooltip('yyoe1Text')">
          <html:radiobutton path="yeartoJoinCourse" cssClass="check" value="<%=YOE_1%>" id="yeartoJoinCourse1" onclick="javascript:showDoNotGrades(this);enquiryValidations(this.id);" /><label for="yeartoJoinCourse1"><%=YOE_1%></label>
          <span id="yyoe1Text" class="cmp tltip pos_rht" style="display: none;">
            <div class="hdf5"></div>
            <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
            <div class="linear"></div>
          </span>
        </span>
        <span class="ql_rad" id="yyoe2" onmouseout="hideTooltip('yyoe2Text')">
          <html:radiobutton path="yeartoJoinCourse" cssClass="check" value="<%=YOE_2%>" id="yeartoJoinCourse2" onclick="javascript:showDoNotGrades(this);enquiryValidations(this.id);"/><label for="yeartoJoinCourse2"><%=YOE_2%></label>
          <span id="yyoe2Text" class="cmp tltip pos_rht" style="display: none;">
            <div class="hdf5"></div>
            <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
            <div class="linear"></div>
         </span>
        </span>
        <span class="ql_rad" id="yyoe3" onmouseout="hideTooltip('yyoe3Text')">
          <html:radiobutton path="yeartoJoinCourse" cssClass="check" value="<%=YOE_3%>" id="yeartoJoinCourse3" onclick="javascript:showDoNotGrades(this);enquiryValidations(this.id);"/><label for="yeartoJoinCourse3"><%=YOE_3%></label>
          <span id="yyoe3Text" class="cmp tltip pos_rht" style="display: none;">
              <div class="hdf5"></div>
              <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
              <div class="linear"></div>
           </span>
        </span> 
        <span class="ql_rad mr0" id="yyoe4" onmouseout="hideTooltip('yyoe4Text')">
          <html:radiobutton path="yeartoJoinCourse" cssClass="check" value="<%=YOE_4%>" id="yeartoJoinCourse4" onclick="javascript:showDoNotGrades(this);enquiryValidations(this.id);"/><label for="yeartoJoinCourse4"><%=YOE_4%></label>
          <span id="yyoe4Text" class="cmp tltip pos_rht" style="display: none;">
              <div class="hdf5"></div>
              <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
              <div class="linear"></div>
           </span>
        </span>
        <div class="qler1" id="yoeErrMsg" style="display:none;">Which year you would like to start uni?</div>
        <%--  <html:hidden path="yeartoJoinCourse" id="yeartoJoinCourse"/>--%>
    </fieldset>
  </fieldset>
    
  <h3 class="fnt_lrg fnt_24 cont_h3txt">Contact details</h3>
  <fieldset class="row-fluid mt20" id="countryOfResfieldset">
      <fieldset class="w50p fl">
        <fieldset>
          <label class="lbco mb5" for="cor">COUNTRY OF RESIDENCE*</label>
        </fieldset>
        <fieldset id="divCountryOfRes" class="mb0">
        <div class="select time">
         <span class="fnt_lbd" id="countryOfResidence">Country of residence</span>
         <i class="fa fa-angle-down fa-lg"></i>
        </div>
        <html:select path="prosCountryOfRes" id="countryOfResidenceSel" cssClass="time" onchange="setDropdownSelectedValue(this,'countryOfResidence');hideandshowAddFinderReg(this);enquiryValidations(this.id);">
          <html:option value="">Country of residence</html:option>
          <c:forEach var="countryList" items="${qlformbean.prospectusCountryOfRes}" varStatus="i">
              <html:option value="${countryList.nationalityValue}">
                ${countryList.nationalityDesc}
              </html:option>
             <c:if test="${i.index eq 4}">
             <optgroup label="-----------------------------------"></optgroup>
             </c:if>
           </c:forEach>
         </html:select>          
         <div id="errorCountry" class="qler1" style="display: none;"></div>
         </fieldset>
      </fieldset>
      <fieldset class="w50p fr mb0" id="postcodeField" style="display:none;">
        <fieldset>
          <label for="posco" class="lbco mb5">POSTCODE FINDER</label>
        </fieldset>
        <fieldset>
          <input type="text" value="" placeholder="Enter UK postcode" id="postcode" onkeypress="javascript:if(event.keyCode==13){getPostCodeAddReg('reg');}" onblur="enquiryValidations(this.id);hideTooltip('ypstcode');" autocomplete="off" class="fnt_lrg tx_bx"  onfocus="showTooltip('ypstcode');"/>                              
          <span class="src_btn">
            <input type="button" class="icon_srch fr" onclick="getPostCodeAddReg('reg');" id="addFinder" style="display:none"><i class="fa fa-search" onclick="getPostCodeAddReg('reg');"></i></input>
          </span>
          <span id="ypstcode" class="cmp pst_cd_fd" style="display: none;">
            <div class="hdf5"></div>
            <div>We need your address so you can receive print prospectuses if you request them. We won't pop round or anything.</div>
            <div class="linear"></div>
          </span>
        </fieldset>
        <div class="qler1" id="postcodeErrMsg" style="display: none;"></div>
      </fieldset>
    </fieldset>
    
   <div id="addressFields" style="display:none;">
    <fieldset class="row-fluid mb20" id="postcodeSection" style="display: none;">                          
      <fieldset class="w100p fl" id="postaddid" style="display:none;">
        <div class="select time fwc n_slct">
          <span id="postCodeAddSpan" class="fnt_lbd">Please select</span>
          <i class="fa fa-angle-down fa-lg"></i>      
        </div>
        <select name="postCodeAddSel" id="postCodAdd" class="time" onchange="setDropdownSelectedValue(this, 'postCodeAddSpan'); setPostCodeAddressProspectus(this);"></select>
      </fieldset>
    </fieldset>
    <fieldset class="w100p fl" id="divAddress1">      
      <fieldset class="w100p fl adlog"> 
        <c:if test="${not empty qlformbean.address1}">
          <html:input path="address1" id="address1" autocomplete="off" cssClass="ql-inpt adiptxt usr addr errRd" maxlength="100" />
          <label class="lbco" id="lblAddress1" for="address1">Address line 1*</label>
        </c:if>
        <c:if test="${empty qlformbean.address1}">
          <html:input path="address1" id="address1" autocomplete="off" cssClass="ql-inpt adiptxt usr addr errRd" value="" maxlength="100" />
          <label class="lbco" id="lblAddress1" for="address1">Address line 1*</label>
        </c:if>                 
        <div id="errorAddress1" class="qler1" style="display:none;"></div>
      </fieldset>
    </fieldset>
    <fieldset class="w100p fl" >
      <fieldset class="w100p fl adlog">    
       <c:if test="${not empty qlformbean.address2}">
          <html:input path="address2" id="address2" autocomplete="off" cssClass="ql-inpt adiptxt usr addr errRd" maxlength="100" />
          <label class="lbco" id="lblAddress2" for="address1">Address line 2</label>
        </c:if>
        <c:if test="${empty qlformbean.address2}">
          <html:input path="address2" id="address2" autocomplete="off" cssClass="ql-inpt adiptxt usr addr errRd" value="" maxlength="100" />
          <label class="lbco" id="lblAddress2" for="address2">Address line 2</label>
        </c:if>
      </fieldset>
    </fieldset>
    <fieldset class="w100p fl mb20 ">
      <fieldset class="w50p fl adlog" id="divTown">        
        <c:if test="${not empty qlformbean.town}">
          <html:input path="town" id="town" autocomplete="off" cssClass="ql-inpt adiptxt usr" maxlength="100" />          
          <label class="lbco" id="lblTown" for="divTown">Town / city*</label>
        </c:if>
        <c:if test="${empty qlformbean.town}">
          <html:input path="town" id="town" autocomplete="off" cssClass="ql-inpt adiptxt usr" value="" maxlength="100" />
          <label class="lbco" id="lblTown" for="divTown">Town / city*</label>
        </c:if>
         <div id="errorTown" class="qler1" style="display:none;"></div>
      </fieldset>
      <fieldset id="postcodeIndexField" class="w50p fr adlog">        
        <c:if test="${qlformbean.prosnonukresident eq 'No'}">
          <c:if test="${not empty qlformbean.postCode}">
            <html:input path="postCode" onblur="postCodeValCheck(this.id)" id="postcodeIndex" autocomplete="off" cssClass="ql-inpt adiptxt usr disb" maxlength="25" /> 
            <label class="lbco" id="lblPostcodeIndex" for="postcodeIndex">Postcode*</label>
          </c:if>
          <c:if test="${empty qlformbean.postCode}">
            <html:input path="postCode" onblur="postCodeValCheck(this.id)" id="postcodeIndex" autocomplete="off" cssClass="ql-inpt adiptxt usr disb" value="" maxlength="25" />
            <label class="lbco" id="lblPostcodeIndex" for="postcodeIndex">Postcode*</label>
          </c:if>
        </c:if>
        <c:if test="${qlformbean.prosnonukresident ne 'No'}">
          <c:if test="${not empty qlformbean.postCode}">
            <html:input path="postCode" onblur="postCodeValCheck(this.id)" id="postcodeIndex" autocomplete="off" cssClass="ql-inpt adiptxt usr" maxlength="25" /> 
            <label class="lbco" id="lblPostcodeIndex" for="postcodeIndex">Postcode*</label>
          </c:if>
         <c:if test="${empty qlformbean.postCode}">
            <html:input path="postCode" onblur="postCodeValCheck(this.id)" id="postcodeIndex" autocomplete="off" cssClass="ql-inpt adiptxt usr" value="" maxlength="25" /> 
            <label class="lbco" id="lblPostcodeIndex" for="postcodeIndex">Postcode*</label>
          </c:if>
        </c:if>      
        <!--  
        <logic:messagesPresent>
        </logic:messagesPresent> 
        -->
        
        
         <div class="err"><html:errors property="postCode"/></div>
         
        <div class="err" id="errorPostcodeIndex" style="display:none;"></div>
      </fieldset>   
   </fieldset>
   </div>
   <%--Added this for showing consent flag details By Hema.S on 17_12_2019_REL --%>
   <!-- Data sharing pod start -->
   <c:if test="${not empty requestScope.bulkProspectusClientleadConsentList}"> 
   <div class="sgnup_chkbx1 date_shr" id="consentPodDiv">
     <h3 class="fnt_lbd fnt20 mb20 mt20">University E-Newsletter Sign Up <span class="fnt_lrg gry_txt">(optional)</span></h3>
     <p class="rem1 pb18">
     You can now opt in to receive e-newsletters from your favourite universities on Whatuni. Please read the university privacy notice before subscribing. You will need to contact the University directly if you wish to update your mailing preference.
     </p>
       <c:forEach var="bulkProspectusClientleadConsentList" items="${requestScope.bulkProspectusClientleadConsentList}">
       <div class="btn_chk mb10" id="consentflagdiv_${bulkProspectusClientleadConsentList.collegeId}">
	     <span class="chk_btn">
	      <html:checkbox id="consentFlag_${bulkProspectusClientleadConsentList.collegeId}" path="consentFlag" value="${bulkProspectusClientleadConsentList.collegeId}"/>
		   <span class="chk_mark"></span>
		 </span>
		 <p><label for="marketingEmailFlag1" class="fnt_lbd chkbx_100">${bulkProspectusClientleadConsentList.collegeDisplayName}</label> <br>
           ${bulkProspectusClientleadConsentList.collegeConsentText}
      </div>
      <!-- <input type="hidden" id="removedUni"> -->
      <html:hidden path="removedUni" id="removedUni"/>
      </c:forEach>
     <div class="borderbot fl mt0 mb30"></div>
    </div>
    <html:hidden path="consentFlagCheck" value="YES"/>
    <html:hidden id="collegeIdLists" path="collegeIdLists" value="${requestScope.collegeIdValues}"/>
    </c:if>
    <!-- Data sharing pod end -->
   
   <div class="sgnup_chkbx1">
    <h3 class="fnt_lbd fnt20 mb20 mt20">Stay up to date by email <span class="fnt_lrg gry_txt">(optional)</span></h3>
													<div class="btn_chk">
														<span class="chk_btn">
							  <c:if test="${qlformbean.marketingEmail eq 'Y'}">							
                              <input type="checkbox" name="marketingEmail" id="marketingEmailFlag" checked="checked"/>
                              </c:if>
                             <c:if test="${qlformbean.marketingEmail eq 'N'}">	
                                <input type="checkbox" name="marketingEmail"  id="marketingEmailFlag"/>
                             </c:if>
                              <span class="chk_mark"></span>
                            </span>
														<p><label for="marketingEmailFlag" class="fnt_lbd chkbx_100">Newsletters <span class="cmrk_tclr">(Tick to opt in)</span> </label> <br>
														Emails from us providing you the latest university news, tips and guides</p>
                          </div>
													<div class="btn_chk">
														<span class="chk_btn">
							<c:if test="${qlformbean.solusEmail eq 'Y'}">							
                              <input type="checkbox" name="solusEmail" id="solusEmailFlag" checked="checked"/>    
                            </c:if>
                              <c:if test="${qlformbean.solusEmail eq 'N'}">
                              <input type="checkbox" name="solusEmail" id="solusEmailFlag"/>    
                              </c:if>
                              <span class="chk_mark"></span>
                            </span>
														<p> <label for="solusEmailFlag" class="fnt_lbd chkbx_100"> University updates <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                            <spring:message code="wuni.solus.flag.text"/></p>
                          </div>
													<div class="btn_chk">
														<span class="chk_btn">
							  <c:if test="${qlformbean.surveyEmail eq 'Y'}">							
                              <input type="checkbox" name="surveyEmail" id="surveyEmailFlag" checked="checked"/>
                              </c:if>
                              <c:if test="${qlformbean.surveyEmail eq 'N'}">
                                <input type="checkbox" name="surveyEmail" id="surveyEmailFlag"/>
                              </c:if>
                              <span class="chk_mark"></span>
                            </span>
														<p><label for="surveyEmailFlag" class="fnt_lbd chkbx_100">Surveys <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                            <spring:message code="wuni.survey.flag.text"/></p>
                          </div>
												<div class="borderbot fl mt20 mb40"></div>
                      </div>
                                                                                  
                      
   
   
    <fieldset class="row-fluid mt25 mb15" style="display:none;">
      <fieldset class="pb18"><label class="lbco">ARE YOU A UK RESIDENT?</label></fieldset>
      <fieldset class="w50p fl">
        <span class="ql_rad">
          <html:radiobutton path="prosnonukresident" onclick="disableProsUKresident()" cssClass="fl mr5" value="Yes"  id="nonukresident" /><label class="fnt_lrg fnt_14">Yes</label>         
        </span>
        <span class="ql_rad">
          <html:radiobutton path="prosnonukresident" onclick="disableProsUKresident()" cssClass="fl mr5" value="No"  id="nonukresident" /><label class="fnt_lrg fnt_14">No</label>         
        </span>
      </fieldset>
   </fieldset>   
   
    <fieldset class="row-fluid blk_pros1">
    
    
    
    
    
     <!--08_Oct_2014 - Removed below news letter text. by Thiyagu G.-->
    <div class="ql-optout mt0" style="display:none;">
      <html:checkbox path="newsLetter" id="newsLetter" cssClass="check" value="Y"/>
      <span class="new_lbl" for="mess1">We'd like to send you information about universities and courses you might be interested in, so we'd like to register you on the WhatUni mailing list. Please untick this box if you would prefer not to be sent emails by Hotcourses Ltd, the publisher of WhatUni. (We know your info's important to you, so we never pass it to a third party).</span> 
    </div>
    
                   
                          <fieldset class="mt0 rgfm-lt">                            
                              <span class="chk_btn">
                                <input type="checkbox" value="" class="chkbx1" id="enquiryFormProceed"/>
                                <span class="chk_mark"></span>
                              </span>
                              <label for="enquiryFormProceed" id="normalsignup">
																               <span class="rem1">I confirm I'm over 13 and agree to the <a href="javascript:void(0);" onclick="showTermsConditionPopup()" class="link_blue" title="terms and conditions">terms and conditions</a> and <a href="javascript:void(0);" onclick="showPrivacyPolicyPopup()" class="link_blue" title="privacy notice">privacy notice</a>, and to become a member of the Whatuni community.<span class="as_trk">*</span> </span>
                              </label>  
                               <p class="qler1" id="termsc_error" style="display:none;"></p>
                            </fieldset>
 

        <div id="basketEmptyErr" class="err" style="display:none;"></div>
        <%
      String orderProspetusOnclick = "return validateBulkProspectusIntrctnmAndLogOmniture(this,'COLLEGE_PROSPECTUS','','');";
    %>
      
    <a class="btn1 bg_orange fr" id="getProspectus" onclick="<%=orderProspetusOnclick%>">Get prospectus
      <i class="fa fa-long-arrow-right"></i>
   </a>

    </fieldset>
    
   <%-- <p class="mt10 fnt_lrg">By clicking 'Proceed' you agree that the universities can contact you directly.</p>--%>
    
</div>

<input type="hidden" id="pgFlag" name="pgFlag" value="<%=flag%>"/>

</div>
<%if("Y".equalsIgnoreCase((String)request.getAttribute("errEmailExists"))){%>
  <script type="text/javascript">
  dev("#divEmail").addClass("qlerr");
  //setErrorAndSuccessMsg("divEmail", "w100p adlog qlerr", "errorEmailExist", "");
   dev("#errorEmailExist").addClass("qler1");
  </script>
  <%}%>
  <%--Added auto email domain script for 21_02_2017, By Thiyagu G --%>
  <script type="text/javascript" id="domnScptId">
    var $mdv1 = jQuery.noConflict();
    $mdv1(document).ready(function(e){
      $mdv1.getScript("<%=CommonUtil.getJsPath()%>/js/emaildomain/<%=emailDomainJs%>", function(){        
        var arr = ["email", "hideTooltip('emailYText');", "clearErrorMsg('divEmail','errorEmail');"];
        $mdv1("#email").autoEmail(arr);        
    	 });      
    });
  </script>
  <input type="hidden" id="domainLstId" value='<%=java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.form.email.domain.list")%>' />

<script type="text/javascript">
//
function checkExistEmail(obj){
    var email = obj.value;
    if(email == 'Enter email address'){
      return false;
    }
    var url= "/degrees/commonaction.html";
    var ajax=new sack();
    ajax.requestFile = url;
    
    ajax.setVar("emailAddress", email);
    ajax.setVar("actionType", "checkEmail");
    
    ajax.onCompletion = function(){ showResponseFun(ajax, email); };	
    ajax.runAJAX();
}

function showResponseFun(ajax, textVal){
  var response = ajax.response;
  if(response.trim() == "EXIST"){
    document.getElementById("existEmailError").innerHTML = "The email address " + textVal + " is already registered with us, please login. This is to protect your personal data";
    document.getElementById("existEmailError").className = "err";
    return false;
  }
}
function clearDefaultEnquiry(obj, text){  
  if(document.getElementById(obj).value == text){
    document.getElementById(obj).value = "";
  }
}
function setDefaultEnquiry(obj, text){  
  if(document.getElementById(obj).value == ""){
    document.getElementById(obj).value = text;
  }
}
if (document.getElementById('nationality1') != null){
  var el = document.getElementById('nationality1');
  var text = el.options[el.selectedIndex].text;
  document.getElementById('natSpan').innerHTML = text;
}

/*if (document.getElementById('countryOfResidenceSel') != null){
  var el = document.getElementById('countryOfResidenceSel');
  var text = el.options[el.selectedIndex].text;
  document.getElementById('countryOfResidence').innerHTML = text;
  document.getElementById('addressFields').style.display = "block";  
} */

function setSelectedValue(obj, spanid){
    var dobid = obj.id
    var el = document.getElementById(dobid);
    var text = el.options[el.selectedIndex].text;
    document.getElementById(spanid).innerHTML = text;
  }
function checkChanges(){

}
</script>

<%if(!("Y").equals(flag)){
String userId = new SessionData().getData(request, "y");
if ("0".equals(userId) || userId == null) { 
%>

<%}%>
<script type="text/javascript">
//setMultiSetvalueEnquiryForm();  
showDoNotGradesDefault();
//showDOBPrePopulate();
if (document.getElementById('grade_210') != null){
  var el = document.getElementById('grade_210');
  var text = el.options[el.selectedIndex].text;
  document.getElementById('gradeSelected').innerHTML = text;
}

function showDoNotGradesDefault(){
  if(document.getElementById('yeartoJoinCourse') != null){
  var yeartoJoinCourse = document.getElementById('yeartoJoinCourse').value;
  if(document.getElementById('checkFieldId-grade')){
    if(yeartoJoinCourse == "<%=yearOfEntry1%>"){
      document.getElementById('checkFieldId-grade').style.display = "none";
    }
  }
  }
}

function showDoNotGrades(obj){
  var yoeId = obj.id;
  var yeartoJoinCourse = document.getElementById(yoeId).value;
  if(document.getElementById('checkFieldId-grade')){
    if(yeartoJoinCourse == "<%=yearOfEntry1%>"){
      document.getElementById('checkFieldId-grade').style.display = "none";
    } else{
      document.getElementById('checkFieldId-grade').style.display = "block";
    }
  }
} 
function showDOBPrePopulate(){
  var elDate = $$D("dateDOB");  
  var elMonth = $$D("monthDOB");
  var elYear = $$D("yearDOB");  
  $$("dateSpan").innerHTML = elDate.options[elDate.selectedIndex].text;
  $$("monthSpan").innerHTML = elMonth.options[elMonth.selectedIndex].text;
  $$("yearSpan").innerHTML = elYear.options[elYear.selectedIndex].text;  
}
</script>
<%}%>
<c:if test="${empty requestScope.isExistsBasicFormEmail}">
<c:if test="${empty qlformbean.emailAddress}">
<script type="text/javascript">
  var $ad = jQuery.noConflict();
    $ad(document).ready(function(e){
      showhideAddressFields();
  });
</script>
</c:if>
</c:if>

<!-- <script type="text/javascript">
  var $$q = jQuery.noConflict();
  $$q(document).ready(function(){
    if($$q("#countryOfResidenceSel") != ""){
    	prospectusShowhideAddressFields();
    }
  });  
</script> -->