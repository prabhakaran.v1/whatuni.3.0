<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>

<c:if test="${not empty requestScope.courseInfoList}">
  <c:forEach var="courseInfo" items="${requestScope.courseInfoList}">
    <div class="logcon"><img src="${courseInfo.collegeLogPath}" alt="${courseInfo.collegeNameDisplay}"></div>    
    <h4 class="pt30">${courseInfo.courseTitle}</h4>
    <h5>${courseInfo.collegeNameDisplay}</h5>
    <c:if test="${empty requestScope.emptyFees}">
    <h6>Tuition Fees</h6>
    <div id="tutFees" class="crsiner">
      <div class="feecnty">      
        <div class="fee_nav">
          <div class="fnav_wrp" id="mbleview"></div>
          <div class="scrltab">
          <c:if test="${empty requestScope.domesticFeesEmp}">
            <a href="#tab1">Domestic</a>
          </c:if>
          <c:if test="${empty requestScope.otherUKFeesEmp}">
            <a href="#tab2">Other UK</a>
          </c:if>
          <c:if test="${empty requestScope.englandFeesEmp}">
            <a href="#tab3">England</a>
          </c:if>
          <c:if test="${empty requestScope.englandFeesEmp}">
            <a href="#tab4">Scotland</a>
          </c:if>
          <c:if test="${empty requestScope.walesFeesEmp}">
            <a href="#tab5">Wales</a>
          </c:if>
          <c:if test="${empty requestScope.northIreFeesEmp}">
            <a href="#tab6">Northern Ireland</a>
          </c:if>
          <c:if test="${empty requestScope.euFeesEmp}">     
            <a href="#tab7">EU</a>
          </c:if>
          <c:if test="${empty requestScope.intelFeesEmp}">
            <a href="#tab8">Rest of World</a>
          </c:if>
        </div>
      </div>    
      <c:if test="${empty requestScope.domesticFeesEmp}">
        <div class="crsfee active" id="tab1">
          <div class="feerw clr">
            <div class="feerw_lt fl">
              <span class="erfee bld"><c:out value="${courseInfo.domesticFees}" escapeXml="false" /></span> 
            </div>
          </div> 
         </div>
       </c:if>
       <c:if test="${empty requestScope.otherUKFeesEmp}">  
         <div class="crsfee hide" id="tab2">
           <div class="feerw clr">
             <div class="feerw_lt fl">
               <span class="erfee bld"><c:out value="${courseInfo.otherUKFees}" escapeXml="false" /></span> 
             </div>
            </div> 
           </div>
         </c:if>
         <c:if test="${empty requestScope.englandFeesEmp}">
           <div class="crsfee hide" id="tab3">
             <div class="feerw clr">
               <div class="feerw_lt fl">
                 <span class="erfee bld"><c:out value="${courseInfo.engFees}" escapeXml="false" /></span> 
               </div>
             </div> 
           </div>
         </c:if>
         <c:if test="${empty requestScope.scotFeesEmp}">
           <div class="crsfee hide" id="tab4">
             <div class="feerw clr">
               <div class="feerw_lt fl">
                 <span class="erfee bld"><c:out value="${courseInfo.scotFees}" escapeXml="false" /></span>
               </div>
             </div> 
           </div>
         </c:if>
         <c:if test="${empty requestScope.walesFeesEmp}">
           <div class="crsfee hide" id="tab5">
             <div class="feerw clr">
               <div class="feerw_lt fl">
                 <span class="erfee bld"><c:out value="${courseInfo.walesFees}" escapeXml="false" /></span>
               </div>
             </div> 
           </div>
         </c:if>
         <c:if test="${empty requestScope.northIreFeesEmp}">
         <div class="crsfee hide" id="tab6">
           <div class="feerw clr">
             <div class="feerw_lt fl">
               <span class="erfee bld"><c:out value="${courseInfo.northIreFees}" escapeXml="false" /></span>
             </div>
           </div> 
         </div>
         </c:if>
         <c:if test="${empty requestScope.euFeesEmp}">
           <div class="crsfee hide" id="tab7">
             <div class="feerw clr">
               <div class="feerw_lt fl">
                 <span class="erfee bld"><c:out value="${courseInfo.euFees}" escapeXml="false" /></span>
               </div>
             </div> 
           </div>
         </c:if>
         <c:if test="${empty requestScope.intelFeesEmp}">
           <div class="crsfee hide" id="tab8">
             <div class="feerw clr">
               <div class="feerw_lt fl">
                 <span class="erfee bld"><c:out value="${courseInfo.intlFees}" escapeXml="false" /></span>
               </div>
             </div>
           </div>
         </c:if>
       </div>
      </div>
    </c:if>
    <h6>Course info</h6>
    <c:if test="${not empty courseInfo.entryPoints}">
      <div class="tf_rw1">
        <p class="tf_subt">Entry requirements:</p>
        <p class="tf_val">${courseInfo.entryPoints}</p>
      </div>
    </c:if>
    <c:if test="${not empty courseInfo.description}">
      <div class="tf_rw1">
        <p class="tf_subt">Study mode:</p>
        <p class="tf_val">${courseInfo.description}</p>
      </div>
    </c:if>
    <c:if test="${not empty courseInfo.durationDesc}">
      <div class="tf_rw1">
        <p class="tf_subt">Study duration:</p>
        <p class="tf_val">${courseInfo.durationDesc}</p>
      </div>
    </c:if>    
    <c:if test="${not empty courseInfo.shortText}">
      <p>
        <div class="tf_rw1">
          <p class="tf_subt">Start date:</p>
          <p class="tf_val">
            ${courseInfo.shortText}</p>
        </div>
      </p>
    </c:if>      
  </c:forEach> 
</c:if>
<c:if test="${not empty requestScope.collegeInfoList}">
  <c:forEach var="collegeInfo" items="${requestScope.collegeInfoList}"> 
    <div class="logcon"> <img src="${collegeInfo.collegeLogPath}" alt="${collegeInfo.collegeNameDisplay}"></div> <br>     
      <h5>${collegeInfo.collegeNameDisplay}</h5>
      <c:if test="${not empty collegeInfo.overallRatingExact}">
        <c:if test="${collegeInfo.overallRatingExact eq 0}">
        <span class="rat${collegeInfo.overallRating} t_tip">
        <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
        <span class="cmp">
          <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
          <div class="line"></div>
        </span>
        <%--End of rating tool tip code--%>
      </span>
      (${collegeInfo.overallRatingExact})
      </c:if>
    </c:if>   
    <c:if test="${not empty collegeInfo.timesRanking}">
      <p><strong>CompUniGuide ranking:</strong> <%--Changed ranking lable from Times to CUG for 08_MAR_2016 By Thiyagu G--%>
      <br>${collegeInfo.timesRanking}</p>
    </c:if>
    <c:if test="${not empty collegeInfo.positionId}">
      <p><strong>Whatuni student rating:</strong>
      <br>${collegeInfo.positionId}/${collegeInfo.totalCnt}</p>
    </c:if>
  </c:forEach>
</c:if>
