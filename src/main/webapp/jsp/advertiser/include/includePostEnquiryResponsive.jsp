<%--
  * @purpose: This jsp is included for post enquiry responsive view.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 3-Nov-2015     Thiyagu G                 1.0      First draft           wu_546
  * *************************************************************************************************************************
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction,org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction,java.util.*"%>
<%
  ResourceBundle rb = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources"); 
  String domainSpecificPath = new CommonFunction().getSchemeName(request)+rb.getString("wuni.whatuni.device.specific.css.path");
  String main_480_ver = rb.getString("wuni.whatuni.main.480.css");
  String main_992_ver = rb.getString("wuni.whatuni.main.992.css");
  String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
  String lazyLoadJs = rb.getString("wuni.lazyLoadJs.js");
  
%>
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen"/>  
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>    
<script type="text/javascript" language="javascript">
    function $$D(id){return document.getElementById(id)}
    var dev = jQuery.noConflict();
    dev(document).ready(function(){
      adjustStyle();
    });
    adjustStyle();
    function loadLazyJS(){
    if(!document.getElementById('lazyload')){
      var file=document.createElement('script');
      file.setAttribute("type","text/javascript");
      file.setAttribute("id", "lazyload");
      file.setAttribute("src", "<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>");
      document.getElementsByTagName("head")[0].appendChild(file);
    }
  }
    function jqueryWidth() {
      return dev(this).width();
    }     
    function adjustStyle() {
      var path = ""; 
      var screenWidth = document.documentElement.clientWidth;
      if (screenWidth <= 480) {
        loadLazyJS();
        if (dev("#viewport").length == 0) {
          dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
        }        
      <%if(("LIVE").equals(envronmentName)){%>
          $$D('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
          $$D('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
          $$D('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}%>      
      } else if ((screenWidth > 480) && (screenWidth <= 992)) {  
      loadLazyJS();
          if (dev("#viewport").length == 0) {
            dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
          }
          <%if(("LIVE").equals(envronmentName)){%>
            $$D('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
          <%}else if("TEST".equals(envronmentName)){%>
            $$D('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
          <%}else if("DEV".equals(envronmentName)){%>
            $$D('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
          <%}%>         
      } else {
        loadLazyJS();
          if (dev("#viewport").length > 0) {
            dev("#viewport").remove();
          }          
          $$D('size-stylesheet').href = "";
        }
        dev(".hm_srchbx").hide();
    }
    dev(window).on('orientationchange', orientationChangeHandler);
    function orientationChangeHandler(e) {
      setTimeout(function() {
        dev(window).trigger('resize');
      }, 500);
    }
    dev(window).resize(function() {
      adjustStyle();  
    });
</script>