<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,com.wuni.advertiser.ql.QLCommon, WUI.utilities.SessionData, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants" %>

  <%      
    CommonFunction common = new CommonFunction();
    String collegeId = GenericValidator.isBlankOrNull((String) request.getAttribute("collegeId"))?  "":(String) request.getAttribute("collegeId");
    String collegeName = GenericValidator.isBlankOrNull((String) request.getAttribute("collegeName")) ? "":(String) request.getAttribute("collegeName");
    collegeName = !"null".equalsIgnoreCase(collegeName) ? collegeName:  "";    
    String collegeNameDisplay = GenericValidator.isBlankOrNull((String) request.getAttribute("collegeNameDisplay")) ? "":(String) request.getAttribute("collegeNameDisplay");
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");    
    cpeQualificationNetworkId = GenericValidator.isBlankOrNull((String) request.getAttribute("collegeName")) ? "2": cpeQualificationNetworkId;    
    request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);
    String subOrderItemId = GenericValidator.isBlankOrNull((String) request.getAttribute("subOrderItemId")) ? "":(String) request.getAttribute("subOrderItemId");
    String courseId = GenericValidator.isBlankOrNull((String) request.getAttribute("courseId")) ? "":(String) request.getAttribute("courseId");
    String courseName = GenericValidator.isBlankOrNull((String) request.getAttribute("courseName")) ? "":(String) request.getAttribute("courseName");
    String profileType = GenericValidator.isBlankOrNull((String) request.getAttribute("profileType")) ? "":(String) request.getAttribute("profileType");
    String getProspectusFlag = GenericValidator.isBlankOrNull((String) request.getAttribute("getProspectusFlag")) ? "N":(String) request.getAttribute("getProspectusFlag");
    String cpeValue = common.getGACPEPrice(subOrderItemId, "email");
    String widgetId = common.getWUSysVarValue("WU_EMAIL_WIDGETID");
    String gaCollegeName = common.replaceSpecialCharacter(collegeName);
    String flag = new QLCommon().getStudyLevelFlagFromProspectus(request);
    
    String dim12Val = request.getParameter("prevpagedim12") != null ? request.getParameter("prevpagedim12") : "";    
    String nonmywu = request.getParameter("nonmywu") != null ? request.getParameter("nonmywu") : "";
    String pnotes = request.getParameter("pnotes") != null ? request.getParameter("pnotes") : "";
    String submitType = request.getParameter("submitType") != null ? request.getParameter("submitType") : "";
    String referrerURL_GA = request.getAttribute("refferalUrl") != null ? (String)request.getAttribute("refferalUrl") : "";
    String vwcid = request.getParameter("vwcid") != null ? request.getParameter("vwcid") : "";
    String vwurl = request.getParameter("vwurl") != null ? request.getParameter("vwurl") : "";
    String vwcourseId = request.getParameter("vwcourseId") != null ? request.getParameter("vwcourseId") : "";    
    String vwsid = request.getParameter("vwsid") != null ? request.getParameter("vwsid") : "";
    String vwnid = request.getParameter("vwnid") != null ? request.getParameter("vwnid") : "";
    String vwprice  = request.getParameter("vwprice") != null ? request.getParameter("vwprice") : "";
    String vwcname  = request.getParameter("vwcname") != null ? request.getParameter("vwcname") : "";
    String netFlag = "3";
    
    String enquiryExistFlag = (String)request.getAttribute("enquiryExistFlag");
    
    String[] yoeArr = common.getYearOfEntryArr();
    String YOE_1 = yoeArr[0];
    String YOE_2 = yoeArr[1];
    String YOE_3 = yoeArr[2];
    String YOE_4 = yoeArr[3];
  %>
 
  <%
   String emailDefaultText = "";
   if(request.getAttribute("opendate") == null){
     if(GenericValidator.isBlankOrNull(courseId) || "0".equalsIgnoreCase(courseId)){
       emailDefaultText = "Hello, \nI read about " + collegeNameDisplay + " on Whatuni.com, and would like to request more information about the institution."; 
     }else{
       emailDefaultText = "Hello, \nI read about the " + courseName + " offered by "+collegeNameDisplay+" on Whatuni.com and would like to request more information about the course."; 
     }
   }  
   String postEnquiryFlag = GenericValidator.isBlankOrNull((String) request.getAttribute("postEnquiryRequest"))?  "":(String) request.getAttribute("postEnquiryRequest");
   String sendMailDisplay = "display:block;";
   String sendMailBtnDisplay = "display:none;";
   String postOneClickCheckBox = "";
   %>

<body id="www-whatuni-com" data-twttr-rendered="true">
  <div class="lbox_gry">
    <header class="clipart bg1">
        <div class="content-bg">
           <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" >
              <jsp:param name="headername" value="basket" />
              <jsp:param name="subnavname" value="C" />
            </jsp:include>
           </div>
       </div>
    </header>
    <!--Redesign the layout for 19_Apr_2016, By Thiyagu G-->
    <section class="lbox_cont">
    <section class="lbox_wrap qlfrm cl_csnt">
      <div id="content-bg">        
          <div id="content-blk">
            <aside class="lbx_rht">
              <div id="content-right">
                <jsp:include page="/jsp/advertiser/include/collegeAndCourseInfoPod.jsp" />
              </div>
            </aside>
            <!--Added college and course info pod, By Thiyagu G for 03_Nov_2015.-->
            <article class="lbx_lft lft_wid remv_gplus">
              <div id="content-left" class="fl sub_cnr">
                <h2 class="fnt_lbd">
                   <c:if test="${qlformbean.typeOfEnquiry eq 'send-college-email'}">Request info</c:if>
                   <c:if test="${qlformbean.typeOfEnquiry eq 'order-prospectus'}">Request prospectus</c:if>
                   <c:if test="${qlformbean.typeOfEnquiry eq 'download-prospectus'}">Download prospectus</c:if>
                </h2>                
                <%--Sign in concertina code added by Prabha on 15-DEC-2015--%>
                <%String uId = new SessionData().getData(request, "y");
                if(GenericValidator.isBlankOrNull(uId) || "0".equals(uId)){%>                  
                  <div class="enq_signin">Already have an account? <a class="sgntx" title="Sign in">Sign in <!--<i class="fa fa-plus-circle fnt_24 color1 fr mt6"></i>--></a>
                    <%if(uId==null || "0".equals(uId) || "".equals(uId) ){%>
                      <jsp:include page="/jsp/advertiser/include/AlreadyRegisteredPod.jsp" />  
                    <%}%>  
                  </div>
                <%}%>
                <%--End of Sign in concertina  code--%>                
                
                <c:if test="${not empty requestScope.courseInfoList}">
                  <c:forEach var="mobCourseInfo" items="${requestScope.courseInfoList}">
                    <div class="uni_coltxt">				
                      <h4 class="pt30">${mobCourseInfo.courseTitle}</h4> 
                      <h5>${mobCourseInfo.collegeNameDisplay}</h5>
                    </div>
                  </c:forEach>
                </c:if>
                <div id="pros-pod">
                  <div id="ql-fm" class="qlf-nw">
                    <div class="clear"></div>
                    <div class="red" id="jsFormValidationErrors"></div>         
                    <html:form action="/degrees/send-college-email.html" cssClass="qlform nw_frm" id="qlform" commandName="qlformbean">
					<tags:token/>			
                  <div class="crm_ql-form base">                    
                    <div class="crm_con">
                      <c:if test="${qlformbean.typeOfEnquiry eq 'send-college-email'}">
                        <h3 class="fnt_lrg fnt_24 mb20 mt20">Your enquiry</h3>
                        <fieldset class="row-fluid mb15">                        
                          <fieldset class="w100p fl mb0" id="divfumessage">
                            <c:if  test="${empty qlformbean.message}">
                              <textarea name="message" id="umessage" onblur="javascript:limitCharCountEnquiry(this,4000,'charCount');" onfocus="javascript:limitCharCountEnquiry(this,4000,'charCount');" onkeyup="javascript:limitCharCountEnquiry(this,4000,'charCount');" onkeydown="javascript:limitCharCountEnquiry(this,4000,'charCount');" ><%=emailDefaultText %></textarea>
                            </c:if>
                            <c:if  test="${not empty qlformbean.message}">
                              <textarea name="message" id="umessage" onblur="javascript:limitCharCountEnquiry(this,4000,'charCount');" onfocus="javascript:limitCharCountEnquiry(this,4000,'charCount');" onkeyup="javascript:limitCharCountEnquiry(this,4000,'charCount');" onkeydown="javascript:limitCharCountEnquiry(this,4000,'charCount');" >${qlformbean.message}</textarea> 
                            </c:if>
                            <div class="qler1" id="errorumessage" style="display:none;"></div>
                            <c:if test="${not empty requestScope.errTextArea}">
                              <div class="qler1" id="errorumessage1"><html:errors path="message"/></div>
                            </c:if>
                          </fieldset>
                        </fieldset>
                      </c:if>
                      <c:if test="${qlformbean.typeOfEnquiry eq 'send-college-email'}">
                        <c:if test="${qlformbean.oneClickMandatory eq 'Y'}">
                          <c:if test="${not empty qlformbean.oneClickCheckBox}">
                            <c:set var="postOneClickCheckBoxId" value="${qlformbean.oneClickCheckBox}"/>
                            <%postOneClickCheckBox = (String) pageContext.getAttribute("postOneClickCheckBoxId");%>
                          </c:if>                          
                          <%if(GenericValidator.isBlankOrNull(postOneClickCheckBox) || "Y".equals(postOneClickCheckBox)){
                              sendMailDisplay = "display:none;";sendMailBtnDisplay = "display:block;";  
                            }%>
                        </c:if>
                      </c:if>
                      <div id="sendCollegeEmail" style="<%=sendMailDisplay%>">
                      <h3 class="fnt_lrg fnt_24 mb20 mt20">Your details</h3>      
                      <c:set var="enquiryFormParam" value="${qlformbean.typeOfEnquiry eq 'send-college-email' ? 'enquiryForm' : ''}" />   
                      <fieldset class="row-fluid">                        
                        <fieldset class="w50p fl adlog" id="divfname">
                           <html:input  path="firstName" id="fname" autocomplete="off" maxlength="40" cssClass="ql-inpt adiptxt usr" onfocus="showTooltip('firstNameYText');" onblur="hideTooltip('firstNameYText');enquiryValidations(this.id, '${enquiryFormParam}');"/>
                           <label for="fname" class="lbco">First name*</label>
                           <span class="cmp" id="firstNameYText" style="display:none;"> 
                             <div class="hdf5"></div>
                             <div>We'd like this information so we don't call you by the wrong name. That'd be kinda rude.</div>
                             <div class="linear"></div>
                           </span>
                           <div class="qler1" id="errorfname" style="display:none;"></div>
                        </fieldset>
                        <fieldset class="w50p fr adlog" id="divlname">
                           <html:input  path="lastName" id="lname" autocomplete="off" maxlength="40" cssClass="ql-inpt adiptxt usr" onblur="enquiryValidations(this.id, '${enquiryFormParam}');"/>    
                           <label for="lname" class="lbco">Last name*</label>
                           <div class="qler1" id="errorlname" style="display:none;"></div>
                        </fieldset>
                      </fieldset>                      
                      <fieldset class="row-fluid indx_pos alst_pos">                       
                       <fieldset id="divEmail" class="w100p adlog">
                         <c:if test="${not empty requestScope.isExistsBasicFormEmail}"> 
                           <html:input path="emailAddress" id="email" maxlength="120" cssClass="ql-inpt adiptxt usr disb" readonly="true" onkeydown="hideEmailDropdown(event, 'autoEmailId'); return false" onfocus="this.blur();"/>
                           <label for="email" class="lbco" id="emailId_lbl">Email address*</label>
                         </c:if>
                          <c:if test="${empty requestScope.isExistsBasicFormEmail}"> 
                            <html:input path="emailAddress" id="email" maxlength="120" cssClass="ql-inpt adiptxt usr" autocomplete="off" onfocus="showTooltip('emailYText')" onblur="hideTooltip('emailYText');" onkeydown="hideEmailDropdown(event, 'autoEmailId');" />
                            <label for="email" class="lbco" id="emailId_lbl">Email address*</label>
                            <span id="emailYText" class="cmp rght" style="display: none;">
                            <div class="hdf5"></div>
                            <div>Tell us your email and we'll reward you with...an email.</div>
                            <div class="linear"></div>
                            </span>
                          </c:if>
                          <div class="qler1" id="errorEmail" style="display:none;"></div>
                          <c:if test="${not empty requestScope.errEmailExists}">
                              <div class="qler1" id="errorEmailExist"><html:errors path="emailAddress"/></div>
                            </c:if>
                        </fieldset>
                      </fieldset>
                    <c:if test="${empty requestScope.isExistsBasicFormEmail}">    
                      <fieldset class="row-fluid indx_pos">                       
                       <fieldset id="divPassword" class="w100p adlog">
                         <html:password path="password"  id="password" maxlength="20" cssClass="ql-inpt adiptxt usr " autocomplete="off" onblur="enquiryValidations(this.id);" />
                           <label for="password" class="lbco" id="emailId_lbl">Password*</label> 
                           <span class="pwd_eye"><a onblur="javascript:hideprospectuspassword('password');" href="javascript:showProspectusPassword('password');"><i id="eyeId" class="fa fa-eye" aria-hidden="true"></i></a></span>
                          <div class="qler1" id="errorPassword" style="display:none;"></div>
                        
                        </fieldset>
                      </fieldset>
                      </c:if>
                      <c:if test="${qlformbean.typeOfEnquiry eq 'send-college-email'}">
                        <fieldset class="row-fluid chttip_pos">
                          <fieldset class="w50p fl adlog" id="postcodeField">                            
                              <html:input path="postCode" id="postcodeIndex" autocomplete="off" onblur="enquiryValidations(this.id,'enquiryForm');" cssClass="ql-inpt adiptxt usr" maxlength="8"/>
                              <label for="postcodeIndex" class="lbco">Postcode<span class="pst_code">(optional)</span></label>
                              <div class="qler1" id="postcodeErrMsg" style="display: none;"></div>
                              <c:if test="${not empty requestScope.errPostCodeArea}">
                                <div class="qler1" id="postcodeErrMsg1"><html:errors path="postCodeMessage"/></div>
                              </c:if>
                          </fieldset>
                          <fieldset class="w50p fr"><!--Added onclick function for show and hide tooltip Apr_19_2016-->
                              <a onclick="showAndHideToolTip('postcodeText');" onmouseover="showTooltip('postcodeText');" onmouseout="hideTooltip('postcodeText');" class="color1 fnt_14 fl pt20 pl20 pb20 pst_ttip">Why do we need your postcode?
                              <span id="postcodeText" class="cmp" style="display: none;">
                                <div class="hdf5"></div>
                                <div>We use this information to help assess the reach of our products. This is completely optional.</div>
                                <div class="linear"></div>
                              </span>
                              </a>                              
                          </fieldset>                        
                        </fieldset>
                                               
                        <input type="text" class="disp_off" id="beeTrap" autocomplete="off" />                     
                       
                      </c:if>
                                         
                      <fieldset class="row-fluid mt20 mb22 lbx_mbt">
                        <fieldset class="w100p mb10 mt10">
                          <label class="lbco">WHEN WOULD YOU LIKE TO START?*</label>
                        </fieldset>
                        <fieldset class="ql-inp" id="yoeFeild">
                         <span class="ql_rad" id="yyoe1" onmouseout="hideTooltip('yyoe1Text')">
                            <html:radiobutton path="yeartoJoinCourse" cssClass="check" value="<%=YOE_1%>" id="yeartoJoinCourse1" onclick="enquiryValidations(this.id);" /><label for="yeartoJoinCourse1"><%=YOE_1%></label>
                            <span id="yyoe1Text" class="cmp tltip pos_rht" style="display: none;">
                              <div class="hdf5"></div>
                              <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                              <div class="linear"></div>
                           </span>
                          </span>
                          <span class="ql_rad" id="yyoe2" onmouseout="hideTooltip('yyoe2Text')">
                            <html:radiobutton path="yeartoJoinCourse" cssClass="check" value="<%=YOE_2%>" id="yeartoJoinCourse2" onclick="enquiryValidations(this.id);" /><label for="yeartoJoinCourse2"><%=YOE_2%></label>
                              <span id="yyoe2Text" class="cmp tltip pos_rht" style="display: none;">
                              <div class="hdf5"></div>
                              <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                              <div class="linear"></div>
                           </span>
                         </span>
                          <span class="ql_rad" id="yyoe3" onmouseout="hideTooltip('yyoe3Text')">
                            <html:radiobutton path="yeartoJoinCourse" cssClass="check" value="<%=YOE_3%>" id="yeartoJoinCourse3" onclick="enquiryValidations(this.id);" /><label for="yeartoJoinCourse3"><%=YOE_3%></label>
                              <span id="yyoe3Text" class="cmp tltip pos_rht" style="display: none;">
                              <div class="hdf5"></div>
                              <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                              <div class="linear"></div>
                           </span>
                          </span> 
                          <span class="ql_rad mr0" id="yyoe4" onmouseout="hideTooltip('yyoe4Text')">
                            <html:radiobutton path="yeartoJoinCourse" cssClass="check" value="<%=YOE_4%>" id="yeartoJoinCourse4" onclick="enquiryValidations(this.id);" /><label for="yeartoJoinCourse4"><%=YOE_4%></label>
                              <span id="yyoe4Text" class="cmp tltip pos_rht" style="display: none;">
                              <div class="hdf5"></div>
                              <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
                              <div class="linear"></div>
                           </span>
                          </span>
                          <div class="qler1" id="yoeErrMsg" style="display:none;">Which year you would like to start uni?</div>                        
                        </fieldset>
                      </fieldset>
                      
                      <c:if test="${qlformbean.typeOfEnquiry  ne 'send-college-email'}">
                      <h3 class="fnt_lrg fnt_24 cont_h3txt">Contact details</h3>
                      <fieldset class="row-fluid mt20" id="divCountry">
                        <fieldset class="w50p fl">
                          <fieldset>
                          <label class="lbco mb5" for="cor">COUNTRY OF RESIDENCE*</label>
                          </fieldset>
                          <fieldset id="divCountryOfRes" class="mb0">
                             
                                <div class="select time">
                                  <span class="fnt_lbd" id="countrySpan">Country of residence</span>
                                  <i class="fa fa-angle-down fa-lg"></i>
                                </div>  
                               <html:select path="countryOfResidence" id="countryOfResidence" cssClass="time" onchange="setDropdownSelectedValue(this, 'countrySpan'); hideandshowAddFinderReg(this);enquiryValidations(this.id);">
                                  <html:option value="">Country of residence</html:option>
                                 
                                  <c:forEach var="countryList" items="${qlformbean.prospectusCountryOfRes}" varStatus="i">
                                      <html:option value="${countryList.nationalityValue}">
                                        ${countryList.nationalityDesc}
                                      </html:option>
                                     <c:if test="${i.index eq 4}">
                                     <optgroup label="-----------------------------------"></optgroup>
                                     </c:if>
                                   </c:forEach>
                                   
                                 </html:select>
                                                      
                            <div id="errorCountry" class="qler1" style="display: none;"></div>
                          </fieldset>
                        </fieldset>
                        <fieldset class="w50p fr mb0" id="postcodeField" style="display:none;">
                            <fieldset>
                              <label for="posco" class="lbco mb5">POSTCODE FINDER*</label>
                            </fieldset>
                            <fieldset>
                              <input type="text" value="" placeholder="Enter UK postcode" id="postcode" maxlength="25" onkeypress="javascript:if(event.keyCode==13){getPostCodeAddReg('reg')}" onblur="enquiryValidations(this.id);hideTooltip('ypstcode');" autocomplete="off" class="fnt_lrg tx_bx" onfocus="showTooltip('ypstcode');"/>
                              <span class="src_btn">
                                <input type="button" class="icon_srch fr" onclick="getPostCodeAddReg('reg');" id="addFinder" style="display:none"><i class="fa fa-search" onclick="getPostCodeAddReg('reg');"></i></input>
                              </span>
                              <span id="ypstcode" class="cmp pst_cd_fd" style="display: none;">
                                <div class="hdf5"></div>
                                <div>We need your address so you can receive print prospectuses if you request them. We won't pop round or anything.</div>
                                <div class="linear"></div>
                              </span>
                            </fieldset>
                          <div class="qler1" id="postcodeErrMsg" style="display: none;"></div>
                        </fieldset>
                      </fieldset>   
                      <div id="addressFields" style="display:none;">
                        <fieldset class="row-fluid mb20" id="postcodeSection" style="display: none;">                          
                          <fieldset class="w100p fl" id="postaddid" style="display:none;">
                            <div class="select time fwc">
                              <span id="postCodeAddSpan" class="fnt_lbd">Please select</span>
                              <i class="fa fa-angle-down fa-lg"></i>      
                            </div>
                            <select name="postCodeAddSel" id="postCodAdd" class="time" onchange="setDropdownSelectedValue(this, 'postCodeAddSpan'); setPostCodeAddressEnq(this);"></select>
                          </fieldset>
                        </fieldset>                        
                        <fieldset class="w100p fl" id="divAddress1">                    
                          <fieldset class="w100p fl adlog">
                            <html:input path="address1" id="address1" autocomplete="off" cssClass="ql-inpt adiptxt usr addr errRd" maxlength="100"/>
                            <label class="lbco" for="address1" id="lblAddress1">Address line 1*</label>
                          </fieldset>
                          <div id="errorAddress1" class="qler1" style="display:none;"></div>
                        </fieldset>
                        <fieldset class="w100p fl">
                          <fieldset class="w100p fl adlog">
                            <html:input path="address2" id="address2" autocomplete="off" cssClass="ql-inpt adiptxt usr addr errRd" maxlength="100"/>
                            <label class="lbco" for="address2" id="lblAddress2">Address line 2</label>
                          </fieldset>
                        </fieldset>
                        <fieldset class="w100p fl mb20" >
                          <fieldset class="w50p fl adlog" id="divTown">                            
                            <html:input path="town" id="town" autocomplete="off" cssClass="ql-inpt adiptxt usr" maxlength="100"/>
                            <label class="lbco" for="town" id="lblTown">Town / city*</label>
                            <div id="errorTown" class="qler1" style="display:none;"></div>
                          </fieldset>
                          <fieldset class="w50p fr adlog" id="postcodeIndexField">                            
                            <html:input path="postCode" id="postcodeIndex" autocomplete="off" cssClass="ql-inpt adiptxt usr" onblur="postCodeValCheck(this.id)" maxlength="25"/>
                            <label class="lbco" for="postcodeIndex" id="lblPostcodeIndex">Postcode*</label>
                            <div id="errorPostcodeIndex" class="qler1" style="display:none;"></div>
                          </fieldset>
                        </fieldset>                  
                      </div>
                      </c:if>
                      </div>
                      <%--Added this for showing consent flag details By Hema.S on 17_12_2019_REL --%>
                      <!-- Data sharing start -->
                      <c:if test="${not empty requestScope.consentFlag}"> 
                          <div class="sgnup_chkbx1 date_shr">
                            <h3 class="fnt_lbd fnt20 mb20 mt20">University E-Newsletter Sign Up <span class="fnt_lrg gry_txt">(optional)</span></h3>
                            <p class="rem1 pb18">
                            You can now opt in to receive e-newsletters from your favourite universities on Whatuni. Please read the university privacy notice before subscribing. You will need to contact the University directly if you wish to update your mailing preference.
                            </p>
                            <div class="btn_chk"> <span class="chk_btn">
                            <html:checkbox path="consentFlag" id="dataShareFlag" value="ON"/>
                            <span class="chk_mark"></span> </span>
                            <p>
                              <label for="dataShareFlag" class="chkbx_100">${requestScope.consentText}</label>
                            </p>
                          </div>
                         <div class="borderbot fl mt10 mb30"></div>
                        </div>
                       <html:hidden path="consentFlagCheck" value="YES"/>
                      </c:if>
                      
                      <!-- Data sharing end -->
                      <div id="sendCollegeEmails" style="<%=sendMailDisplay%>">
                      <!--Indumathi-->                                                                 
                      <div class="sgnup_chkbx1">        
                        <h3 class="fnt_lbd fnt20 mb20 mt20">Stay up to date by email <span class="fnt_lrg gry_txt">(optional)</span></h3>
													<div class="btn_chk">
														<span class="chk_btn">
                              <c:if test="${qlformbean.marketingEmail eq 'Y'}">
                              <input type="checkbox" name="marketingEmail" id="marketingEmailFlag" checked="checked"/>
                              </c:if>
                              <c:if test="${qlformbean.marketingEmail eq 'N'}">
                                <input type="checkbox" name="marketingEmail" id="marketingEmailFlag"/>
                              </c:if>
                              <span class="chk_mark"></span>
                            </span>
														<p><label for="marketingEmailFlag" class="fnt_lbd chkbx_100">Newsletters <span class="cmrk_tclr">(Tick to opt in)</span> </label> <br>
														Emails from us providing you the latest university news, tips and guides</p>
                          </div>
													<div class="btn_chk">
														<span class="chk_btn">
							<c:if test="${qlformbean.solusEmail eq 'Y'}"> 							
                              <input type="checkbox" name="solusEmail" id="solusEmailFlag" checked="checked"/>    
                              </c:if>
                              <c:if test="${qlformbean.solusEmail eq 'N'}"> 
                              <input type="checkbox" name="solusEmail" id="solusEmailFlag"/>    
                              </c:if>
                              <span class="chk_mark"></span>
                            </span>
														<p> <label for="solusEmailFlag" class="fnt_lbd chkbx_100"> University updates <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                            <spring:message code="wuni.solus.flag.text"/></p>
                          </div>
													<div class="btn_chk">
														<span class="chk_btn">
							  <c:if test="${qlformbean.surveyEmail eq 'Y'}"> 							 
                              <input type="checkbox" name="surveyEmail" id="surveyEmailFlag" checked="checked"/>
                              </c:if>
                              <c:if test="${qlformbean.surveyEmail eq 'N'}">
                                <input type="checkbox" name="surveyEmail" id="surveyEmailFlag"/>
                              </c:if>
                              <span class="chk_mark"></span>
                            </span>
														<p><label for="surveyEmailFlag" class="fnt_lbd chkbx_100">Surveys <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                            <spring:message code="wuni.survey.flag.text"/></p>
                          </div>
												<div class="borderbot fl mt20 mb40"></div>
                      </div>
                                                                                  
                      <fieldset class="row-fluid get_pros1">                          
                          <fieldset class="mt0 rgfm-lt">                            
                              <span class="chk_btn">
                                <input type="checkbox" value="" class="chkbx1" id="enquiryFormProceed"/>
                                <span class="chk_mark"></span>
                              </span>
                              <label for="enquiryFormProceed" id="normalsignup">
																               <span class="rem1">I confirm I'm over 13 and agree to the <a href="javascript:void(0);" onclick="showTermsConditionPopup()" class="link_blue" title="terms and conditions">terms and conditions</a> and <a href="javascript:void(0);" onclick="showPrivacyPolicyPopup()" class="link_blue" title="privacy notice">privacy notice</a>, and to become a member of the Whatuni community.<span class="as_trk">*</span> </span>
                              </label>  
                               <p class="qler1" id="termsc_error" style="display:none;"></p>
                               <p class="qler1" id="beeTrapError" style="display:none;"></p>
                            </fieldset>

                          <%String onclick = "";%>
                           <c:if test="${qlformbean.typeOfEnquiry  eq 'order-prospectus'}">
                            <%onclick = "hideSubmitButton('getProspectus'); return validateInteractionFormAndLogOmniture(this,'COLLEGE_PROSPECTUS','"+gaCollegeName+"',"+cpeValue+");";%>
                           
                              <a onclick="<%=onclick%>" id="getProspectus" class="btn1 bg_orange fr">Get prospectus <i class="fa fa-long-arrow-right"></i></a>
                         
                          </c:if>
                          <c:if test="${qlformbean.typeOfEnquiry  eq 'send-college-email'}">
                            <%onclick = "hideSubmitButton('sendEmail'); return validateInteractionFormAndLogOmniture(this,'COLLEGE_EMAIL', '"+gaCollegeName+"',"+ cpeValue+");";%>
                             <a onclick="<%=onclick%>" id="sendEmail" class="btn1 bg_orange fr rqinfo_blue">REQUEST INFO<i class="fa fa-long-arrow-right"></i></a>
                       </c:if>
                          <c:if test="${qlformbean.typeOfEnquiry  eq 'download-prospectus'}">
                            <%onclick = "hideSubmitButton('downloadProspectus'); return validateInteractionFormAndLogOmniture(this,'DOWNLOAD_PROSPECTUS','"+gaCollegeName+"',"+cpeValue+");";%>
                            <fieldset class="row-fluid">
                              <a onclick="<%=onclick%>" id="downloadProspectus" class="btn1 bg_orange fr">Proceed <i class="fa fa-long-arrow-right"></i></a>
                            </fieldset>
                          </c:if>
                        </fieldset>
                      </div>
                      <div id="sendCollegeEmailBtn" style="<%=sendMailBtnDisplay%>">
                        <fieldset class="">
                        <c:if test="${qlformbean.typeOfEnquiry  eq 'send-college-email'}">
                        <fieldset class="mt0 rgfm-lt">
                            <span class="chk_btn">
                                <input type="checkbox" value="" class="chkbx1" id="enquiryFormProceed1"/>
                                <span class="chk_mark"></span>
                              </span>
                              <label for="enquiryFormProceed1" id="normalsignup">
																                <span class="rem1">I confirm I'm over 13 and agree to the <a href="javascript:void(0);" onclick="showTermsConditionPopup()" class="link_blue" title="terms and conditions">terms and conditions</a> and <a href="javascript:void(0);" onclick="showPrivacyPolicyPopup()" class="link_blue" title="privacy notice">privacy notice</a>, and to become a member of the Whatuni community.<span class="as_trk">*</span> </span>
                              </label> 
                               <p class="qler1" id="termsc_error1" style="display:none;"></p>
                          
                          </fieldset>
                            <a onclick="prospectusRedirect('/degrees', '<%=collegeId%>', '<%=courseId%>', '${qlformbean.subject}', '<%=postEnquiryFlag%>', '${qlformbean.subOrderItemId}', 'EMAIL','adRoll','${qlformbean.sponsoredOrderItemId}');" id="sendEmail" class="btn1 bg_orange fr rqinfo_blue">REQUEST INFO<i class="fa fa-long-arrow-right"></i></a>
                        </c:if>    
                        </fieldset>
                      </div>
                    </div>
                  </div>
                  <html:hidden id="cpeQualificationNetworkId" path="cpeQualificationNetworkId" value="<%=cpeQualificationNetworkId%>" />
                  <html:hidden path="collegeId" id="collegeId"/>  
                  <html:hidden path="courseId" id="courseId"/>
                  <html:hidden path="subOrderItemId" id="subOrderItemId"/>
                  <html:hidden path="typeOfEnquiry" id="typeOfEnquiry"/>
                  <c:if test="${qlformbean.typeOfEnquiry  ne 'send-college-email'}">
                     <html:hidden path="message" value="<%=emailDefaultText%>"/> 
                  </c:if>
                  <c:if test="${not empty requestScope.profileType}">
                    <html:hidden path="profileType" id="profileType"/>
                  </c:if>
                  <html:hidden path="qlFlag" id="qlFlag"/>
                  <html:hidden path="subject" id="subject"/>                  
                  <html:hidden path="requestUrl" id="requestUrl"/>
                  <html:hidden path="refferalUrl" id="refferalUrl"/>
                  <html:hidden path="postButtonFlag"/>
                  <html:hidden path="studyLevelId" id="studyLevelId"/>   
                  <html:hidden path="sponsoredOrderItemId" id="sponsoredOrderItemId"/>               
                  <input type='hidden' id="omnitureCollegeId" value="<%=collegeId%>" />
                  <input type="hidden" name="btSubmit" value="Proceed" />
                  <html:hidden path="autoEmailFlag" id="autoEmailFlag"/>
                  <input type="hidden" name="widgetId" id="widgetId" value="<%=widgetId%>"/>
                  <input type="hidden" id="pgFlag" name="pgFlag" value="<%=flag%>"/>                  
                  <input type="hidden" id="downloadurl" value=""/>
                  <input type="hidden" id="nonmywu" value="<%=nonmywu%>"/>
                  <input type="hidden" id="pnotes" value="<%=pnotes%>"/>
                  <input type="hidden" id="submitType" value="<%=submitType%>"/>
                  <input type="hidden" id="pdfName" value=""/>
                  <input type="hidden" id="pdfId" value=""/>
                  <input type="hidden" id="referrerURL_GA" value="<%=referrerURL_GA%>"/>
                  <input type="hidden" id="regLoggingType" value="global-nav"/>
                  <input type="hidden" id="vwcid" value="<%=vwcid%>"/>
                  <input type="hidden" id="vwurl" value="<%=vwurl%>"/>
                  <input type="hidden" id="vwsid" value="<%=vwsid%>"/>
                  <input type="hidden" id="vwnid" value="<%=vwnid%>"/>
                  <input type="hidden" id="vwprice" value="<%=vwprice%>"/>
                  <input type="hidden" id="vwcourseId" value="<%=vwcourseId%>"/>
                  <input type="hidden" id="vwcname" value="<%=vwcname%>"/>
                  <input type="hidden" id="emailAlreadyExistsflag" value=""/>  
                  <input type="hidden" id="networkFlag" value="<%=netFlag%>" />
                  <input type="hidden" id="dim12Val" value="<%=dim12Val%>" />
                  <input type="hidden" id="enquiryExistFlag" value="<%=enquiryExistFlag%>"/>
                  <%-- wu582_20181023 - Sabapathi: added screenwidth for stats logging  --%>
		          <input type="hidden" id="screenwidth" name="screenwidth" value=""/>
                </html:form>
              </div>
            </div>
            </div>
            </article>            
          </div>
      </div>
    </section>
  </section>
  <%--Added by Indumathi.S For auto email dropdown Feb-21-2017 --%>
  <script type="text/javascript">
  var $ = jQuery.noConflict();
    var arr = ["email", "hideTooltip('emailYText');", "addFltCls('email', 'emailId_lbl');", "clearErrorMsg('divEmail','errorEmail');"];
	  $("#email").autoEmail(arr);
  </script>
  <input type="hidden" id="domainLstId" value='<%=java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.form.email.domain.list")%>' />
  <jsp:include page="/jsp/common/wuFooter.jsp" /> 
  </div>
  <%if("Y".equalsIgnoreCase((String)request.getAttribute("errEmailExists"))){%>
  <script type="text/javascript">
  dev("#divEmail").addClass("qlerr");
  //setErrorAndSuccessMsg("divEmail", "w100p adlog qlerr", "errorEmailExist", "");
   dev("#errorEmailExist").addClass("qler1");
  </script>
  <%}%>
 <%if(session.getAttribute("userInfoList") == null){%>
   <script type="text/javascript">
     loadEmailCookies();
    </script>  
<%}
  String userId = new SessionData().getData(request, "y");
  if ("0".equals(userId) || userId == null) {   
 %>
 <script type="text/javascript">
   setGradesValueFromCookies();
 </script>
 <%}%>
<script type="text/javascript">  
  var dev = jQuery.noConflict();
  setMultiSetvalueEnquiryForm();
  setDOBValues();
  showhideAddressFields();
  function limitCharCountEnquiry(limitField, limitNum, updateThisElement) {
    var limitFieldLength = limitField.value.length;
    var letters = /^[\-a-zA-Z0-9\s\.\,\:\[\]\`\;\~\''\""\-\@\#\?\%\/\=\&\^\{\}\<\>\$\*\|\!\(\)\-\+\_\\]*$/;
    if ( limitFieldLength > limitNum) {
      limitField.value = limitField.value.substring(0, limitNum);
    }
    limitFieldLength = trimString(limitField.value).length;
    if(!letters.test(limitField.value)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
    	showErrorMsg('errorumessage', 'Please double check your message and remove any special characters', 'divfumessage');
    }else if(limitFieldLength < 50){
    	showErrorMsg('errorumessage', "Please be as descriptive as possible with your request - min. 50 characters", 'divfumessage');
    } else{
    	blockNone("errorumessage","none");
    	dev("#divfumessage").removeClass("qlerr")
    }
    return false;
  }
  function hideandshowdiv(){
    document.getElementById('step1').style.display = 'none';
    document.getElementById('step2').style.display = 'block';
  }
</script> 
 <script type="text/javascript">
   setEventListeners();
 </script>
 <script type="text/javascript">
  var dev = jQuery.noConflict();
    dev(document).ready(function () {
    dev("#umessage").focus();
    dev('.adiptxt').blur(function () {
      var x = dev(this).val();
      if (x != "") {
        dev(this).next('label').addClass("top");
      } else {
        dev(this).next('label').removeClass("top");
      }
    });
    dev('.adiptxt').each(function(index) {      
      var xy = dev(this).val();
      if (xy != "") {
        dev(this).next('label').addClass("top");
      } else {
        dev(this).next('label').removeClass("top");
      } 
    });
  });
  // wu582_20181023 - Sabapathi: update screenwidth in hidden
  updateScreenWidth();
 </script>
</body>