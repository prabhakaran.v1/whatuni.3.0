<%Long startJspBuild = new Long(System.currentTimeMillis());%>

<%@ page import="WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction, WUI.utilities.CommonUtil, java.util.List" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>


      <%
      request.setAttribute("hitbox_eventtag", GlobalConstants.EMAIL_EVENT);
      String collegeId   =  (String) request.getAttribute("collegeId"); 
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");    
    if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0){ 
      cpeQualificationNetworkId = "2";
    }request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);
    String subOrderItemId = (String)request.getAttribute("subOrderItemId");   
    String tmp_collegeId = request.getAttribute("collegeId") != null && request.getAttribute("collegeId").toString().trim().length() > 0 ? (String)request.getAttribute("collegeId") : "0";
    String tmp_collegeName = request.getAttribute("collegeName") != null && request.getAttribute("collegeName").toString().trim().length() > 0 ? " "+(String)request.getAttribute("collegeName") : "";    
    com.wuni.util.seo.SeoUrls seoUrl = new com.wuni.util.seo.SeoUrls();
    String uniUrl = (seoUrl.construnctUniHomeURL(tmp_collegeId, tmp_collegeName)).toLowerCase();            
    String tmp_collegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? " "+(String)request.getAttribute("collegeNameDisplay") : "";    
    String tmp_courseId = request.getAttribute("courseId") != null && request.getAttribute("courseId").toString().trim().length() > 0 ? (String)request.getAttribute("courseId") : "0";
    
    String subOrderWebsite = (String)request.getAttribute("subOrderWebsite");
    request.setAttribute("hitbox_college_id", collegeId);
    request.setAttribute("hitbox_college_name", tmp_collegeName);
    String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(tmp_collegeName); 
    String websitePrice = request.getAttribute("websitePrice") != null && request.getAttribute("websitePrice").toString().trim().length() > 0 ? (String)request.getAttribute("websitePrice") : "0";        
    String enquriyCollegeLimit = request.getAttribute("enquriyCollegeLimit") != null && request.getAttribute("enquriyCollegeLimit").toString().trim().length() > 0 ? (String)request.getAttribute("enquriyCollegeLimit") : "";        
    String enquriyCourseLimit = request.getAttribute("enquriyCourseLimit") != null && request.getAttribute("enquriyCourseLimit").toString().trim().length() > 0 ? (String)request.getAttribute("enquriyCourseLimit") : "";        
    String postenquirysuggestionJsName = CommonUtil.getResourceMessage("wuni.post.enquiry.suggestion.js", null);
%>

  <!--Changed new responsive design for 03-Nov-2015, by Thiyagu G-->
  <body id="www-whatuni-com" onLoad="waitPreloadPage()"> 
    <header class="clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">    
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>
        </div>  
      </div>          
      <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=postenquirysuggestionJsName%>"></script>
    </header>
    <section class="main_cnr pt40 pb40">
      <div class="ad_cnr">
        <div id="sub_container" class="content-bg">
          <div class="sub_cnr abt">
            <div class="fr rht_sec main_success">
              <div class="success p20">
                <h6 class="fnt_lbd pb5">Boom</h6>              
                <%if("Y".equalsIgnoreCase(enquriyCollegeLimit)){%>
                <p class="txt fnt_lrg m0 p0">You've exceeded the number of times you can contact this provider through Whatuni. Soz.</p>
                <%}else if("Y".equalsIgnoreCase(enquriyCourseLimit)){%>
                <p class="txt fnt_lrg m0 p0">You've already enquired about this course...</p>
                <%}%>
              </div> 
              <c:if test="${not empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
                  <c:forEach var="buttonDetails" items="${requestScope.listOfCollegeInteractionDetailsWithMedia}">
                    <c:if test="${not empty buttonDetails.subOrderItemId}">
                      <c:if test="${buttonDetails.subOrderItemId gt 0}">
                        <div class="fleft">
                          <c:if test="${not empty buttonDetails.subOrderWebsite}">
                            <a rel="nofollow" 
                            target="_blank" 
                            class="visit-web" 
                            onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=websitePrice%>); cpeWebClick(this,'<%=tmp_collegeId%>','${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.subOrderWebsite}');" 
                            href="${buttonDetails.subOrderWebsite}" 
                            title="Visit <%=tmp_collegeNameDisplay%> website"></a>
                          </c:if>
                        </div>
                      </c:if>
                    </c:if>
                  </c:forEach>
              </c:if>           
            </div>
          </div>
        </div>
      </div>
    </section>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
  </body>
</html>