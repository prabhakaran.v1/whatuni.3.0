<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
 <c:set var="clearingOnOff" value="${sessionScope.sessionData['CLEARING_ON_OFF']}"/>
<%
CommonFunction common = new CommonFunction();
String pagename3        =   "";  
String requestURL = (String)request.getAttribute("REQUEST_URI");
if(!GenericValidator.isBlankOrNull(requestURL)){
requestURL = requestURL.indexOf("/degrees") > -1 ? requestURL.replace("/degrees", "") : requestURL;
requestURL = requestURL.indexOf(".html") > -1 ? requestURL.replace(".html", "/") : requestURL;   
}
if(!GenericValidator.isBlankOrNull(pageContext.getAttribute("pagename3").toString())){
  pagename3 = (String)pageContext.getAttribute("pagename3");
}
String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId"); 
String pageNames = "browseMoneyPageResults.jsp,courseSearchResult.jsp,clearingBrowseMoneyPage.jsp,clearingCourseSearchResult.jsp,clearingSearchResults.jsp,clearingProviderResults.jsp,courseDetails.jsp,contenthub.jsp,newProviderHome.jsp,richProfileLanding.jsp,clearingProfile.jsp"; 
String profilePageNames = "contenthub.jsp,newProviderHome.jsp,richProfileLanding.jsp,clearingProfile.jsp";
String clearingUserType = (String)session.getAttribute("USER_TYPE") ;
String showSwitchFlag = (String)request.getAttribute("SHOW_SWITCH_FLAG"); 
String clearingonoff = (String)pageContext.getAttribute("clearingOnOff");
%>
<!-- Clearing switch starts -->
<%if("ON".equalsIgnoreCase(clearingonoff) && pagename3 != "" && pageNames.indexOf(pagename3) > -1 && !GlobalConstants.PG_NETWORK_ID.equals(cpeQualificationNetworkId) && "Y".equalsIgnoreCase(showSwitchFlag)){%>
<div class="wuclrsr_swtchrow ${param.podType}" data-sw-type="${sessionScope.USER_TYPE}">
    <div class="wuclrsr_swtchbox">
        <%if("CLEARING".equalsIgnoreCase(clearingUserType)){ %>
        <span class="wuclrsr_swtchtxt">Not looking for clearing courses?</span>
        <a href="javascript:void(0)" onclick="switchUrl('${pagename3}','Off');updateUserTypeSession('','non-clearing');" class="wuclrsr_swtchlink">
            <span class="linfo">Switch</span>
            <img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_wht_arw.svg",0)%>">
        </a>
        <%}else{%>
        <span class="wuclrsr_swtchtxt">Looking for Clearing courses?</span>
        <a href="javascript:void(0)" onclick="switchUrl('${pagename3}','On');updateUserTypeSession('','clearing');" class="wuclrsr_swtchlink">
            <span class="linfo">Switch</span>
            <img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_wht_arw.svg",0)%>">
        </a>
        <%}%>
    </div>                                
</div>
<%}%>
<!-- Clearing switch ends -->
<!-- Clearing switch off update of profile page study level drop down starts  by vedha Sep 30-->
<c:if test="${not empty POST_CLEARING_ON_OFF and POST_CLEARING_ON_OFF eq 'ON' }">
<c:if test = "${not empty studyLevelList}">
<%if(pagename3 != "" && profilePageNames.indexOf(pagename3) > -1){ %>
<div class="clrswth ${param.podType}">
	<div class="clrswth_chbox"> 
	<c:if test="${highlightTab ne 'Clearing 2020'}">
	    <div class="clrswth_txt">I'm interested in 
			<ul class="dd_main">
				<li class="submenu">							
					 <a href="javascript:void(0)" class="clrswth_link">${highlightTab}</a>
						<ul class="dd_mnu">
						  <c:forEach var="studyLevelList" items ="${studyLevelList}">
						    <c:set var = "studyLevel" value="${studyLevelList.studyMode eq 'Undergraduate'? 'UG' : studyLevelList.studyMode eq 'Postgraduate'?'PG':'CL'}" ></c:set>
							<c:choose>
							  <c:when test="${studyLevelList.studyMode eq highlightTab}">
							    <li><a href="javascript:void(0)">${studyLevelList.studyMode}</a></li>
							  </c:when>
							  <c:otherwise>
								 <li><a onclick="javascript:switchProfileURL('${studyLevel}');">${studyLevelList.studyMode}</a></li>
							  </c:otherwise>
							</c:choose>
						  </c:forEach>
						</ul>
					</li>
				  </ul>courses</div>
				</c:if>		
				<c:if test="${highlightTab eq 'Clearing 2020'}">
				 	<div class="clrswth_txt">Clearing 2020 has closed now					
					  <a onclick="javascript:switchProfileURL('UG');" class="clrswth_arw">GET 2021 INFO <i class="fa fa-long-arrow-right"></i></a>
					</div>
				</c:if>
		</div>
</div>
<%}%>
<input type="hidden" id = "studyLevelGA" value="${highlightTab}"/>
</c:if>
</c:if> 