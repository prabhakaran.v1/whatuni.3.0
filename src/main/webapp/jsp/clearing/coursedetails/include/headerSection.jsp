<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil"%>   

<%
String paramCourseId = request.getParameter("w") !=null && request.getParameter("w").trim().length()>0 ? request.getParameter("w") : request.getParameter("cid") !=null && request.getParameter("cid").trim().length()>0 ? request.getParameter("cid") : ""; 
paramCourseId = paramCourseId!=null && paramCourseId.trim().length()>0 ? "" : String.valueOf(request.getAttribute("seoCourseId"));
pageContext.setAttribute("clr_courseId", paramCourseId);
%>
<%-- <div class="sbcr mb-10 pros_brcumb pt20" id="bcrummbs">
</div> --%>

<div class="com wuclrpr_hdinfrow wucdpg_hdrow">
  <c:if test="${not empty courseInfoList}">
    <c:forEach var="clr_courseInfoList" items="${courseInfoList}"> 
      <div class="com-lt">
        <div>
          <h4 class="wuclr_ttlbl">In Clearing</h4>
          <h1 class="result-head reshd2">
            <c:if test="${not empty clr_courseInfoList.courseTitle}">${clr_courseInfoList.courseTitle}</c:if>
          </h1>
          <h2 class="result-head respar2">
            <c:if test="${not empty clr_courseInfoList.uniHomeUrl}">
              <a href="${clr_courseInfoList.uniHomeUrl}" onclick="sponsoredListGALogging('${clr_courseInfoList.collegeId}');"><c:if test="${not empty clr_courseInfoList.collegeNameDisplay}">${clr_courseInfoList.collegeNameDisplay}</c:if></a>
           </c:if>
         </h2>
       </div>
       <c:if test="${not empty clr_courseInfoList.overAllRating}">
         <c:if test="${clr_courseInfoList.overAllRating gt 1}">
           <div class="wuclrsr_unirvwdet">
             <ul class="strat cf">
               <li>
                 <span class="rat${clr_courseInfoList.overAllRating} t_tip">
                   <span class="cmp">
                     <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats"/></div>
                     <div class="line"></div>
                   </span>
                 </span>
                 <span class="rtx">(${clr_courseInfoList.overAllRatingExact})</span>
                   <c:if test="${clr_courseInfoList.reviewCount gt 0}">
                     <c:if test="${not empty requestScope.getReviewList}">
                       <a class="link_blue vr_bold" id="rvw_skip_link" data-rvw-ga="${clr_courseInfoList.collegeNameDisplay}" title="Reviews at ${clr_courseInfoList.collegeNameDisplay}">
                         <spring:message code="view.review.link.name"/>
                         <%-- ${clr_courseInfoList.reviewCount}
                         <c:if test="${clr_courseInfoList.reviewCount gt 1}">reviews</c:if>
                         <c:if test="${clr_courseInfoList.reviewCount eq 1}">review</c:if> --%>
                      </a>
                    </c:if>
                  </c:if>
                </li>                                        
             </ul>
           </div>
         </c:if>
       </c:if>
     </div>
     <div class="com-rt">
       <div class="wuclrpr_inslogo">
         <a href="${clr_courseInfoList.uniHomeUrl}">
           <img  src="${domainPathWithoutImgSrc}/${collegeLogo}" class="ibdr" width="77" alt="${clr_courseInfoList.collegeNameDisplay}">  
         </a>       
       </div>
     </div>  
    </c:forEach>
  </c:if>
  <div class="com wuclrpr_hdbtnset">
    <div class="wuclrsr_unirvwdet">
       <c:if test ="${not empty scoreLabel}">
         <div class="strat cf">
           <div class="coacptbtn">          
             <span class="coabtntxt">Chance of acceptance:</span>
              <div class="wuclrsr_crsmtrtng">
                <c:if test="${not empty scoreLevel}">
                  <c:set var="indicatorClass" value="${scoreLevel eq 'LIKELY' ? 'txtgrncol' : scoreLevel eq 'POSSIBLE' ? 'txtgrncol' : 'txtorngcol'}"/>
                </c:if>
                <div class="wuclrsr_crsmtrtnglft ${indicatorClass}" id="scoreColorIndicator_divId" data-id-indicator="${indicatorClass}">
                  <span class="wuclrsr_crsmtrtngtxt" id="scoreLabel_spanId">${scoreLabel}</span>
                  <span class="wuclrsr_crsmtrtngbrdrset">
                    <span class="wuclrsr_crsmtrtngbrd${scoreLevel ne 'LIKELY' ? ' brdrgry' : ''}" id="scoreColor_spanId1"></span>                  
                    <span class="wuclrsr_crsmtrtngbrd${scoreLevel eq 'STRETCH' ? ' brdrgry' : ''}" id="scoreColor_spanId2"></span>  
                    <span class="wuclrsr_crsmtrtngbrd"></span>
                    <span class="wuclrsr_crsmtrtngbrd"></span>
                    <span class="wuclrsr_crsmtrtngbrd"></span>
                  </span>
                </div>
                <c:if test="${not empty scoreLabelTooltip}">
                  <div class="wuclrsr_crsmtrtngrtg">
                    <span class="tooltip_cnr ttip_cnt">
                      <span class="tool_tip fl">
                        <img class="wuclrsr_imgicn" src="${domainPathWithImgSrc}clr20_icn_info_circle.svg" alt="Info Icon">
                        <span class="cmp">
                          <div class="hdf5"></div>
                          <div id="scoreToolTip_divId">
                            ${scoreLabelTooltip}
                            <!-- We are showing the minimum and maximum UCAS points scores that the institution has listed for all
                                 qualifications.<br><br>However the lowest and highest scores required for this course may vary by
                                  qualification. -->
                           </div>
                           <div>
                             <br><!-- Further information can be found by clicking the course title on this page and viewing the
                                detailed entry requirements by qualification, but we also recommend you contact the institution
                                directly. -->
                           </div>
                           <div class="line"></div>
                         </span>
                       </span>
                     </span>
                   </div>
                 </c:if>
               </div>
             </div>
           </div>
         </c:if>
            
         <c:if test="${not empty listOfCollegeInteractionDetailsWithMedia}">
           <c:forEach var="buttonDetails" items="${requestScope.listOfCollegeInteractionDetailsWithMedia}" varStatus="row"> 
             <ul class="wuclrsr_crsboxbtnset${empty buttonDetails.hotLineNo ? ' vwonlybtn' : ''}">           
               <li class="wuclrsr_vstbtn">
                 <c:set var="rowValue" value="${row.index}"/>
                 <c:if test="${not empty buttonDetails.subOrderItemId}">
                   <c:if test="${buttonDetails.subOrderItemId gt 0}">
                     <c:set var="subOrderId1" value="${buttonDetails.subOrderItemId}"/>
                     <%
                       String webformPrice = new WUI.utilities.CommonFunction().getGACPEPrice((String)pageContext.getAttribute("subOrderId1"), "webform_price");
                       String websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice((String)pageContext.getAttribute("subOrderId1"), "website_price");
                       request.setAttribute("richProfileSubOrderId",(String)pageContext.getAttribute("subOrderId1"));
                       request.setAttribute("advWebsitePrice",websitePrice);
                     %>
                     <c:if test="${not empty buttonDetails.subOrderWebsite}">
                       <a rel="nofollow" 
                          target="_blank" 
                          onclick="sponsoredListGALogging('${interactionCollegeId}');
                                   GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', <%=websitePrice%>); 
                                   cpeWebClickClearingWithCourse(this,'${interactionCollegeId}','${clr_courseId}','${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.subOrderWebsite}');var a='s.tl(';" 
                          href="${buttonDetails.subOrderWebsite}&courseid=${clr_courseId}"
                          title="Visit ${interactionCollegeNameDisplay} website">
                          <spring:message code="visit.website.label" />
                        </a>
                      </c:if>                                       
                    </c:if>
                  </c:if>
                </li>
                <c:if test="${not empty buttonDetails.hotLineNo}">
                  <li class="wuclrsr_calbtn">
                    <a id="contact_"  
                       onclick="sponsoredListGALogging('${interactionCollegeId}');
                                changeContact('${buttonDetails.hotLineNo}','contact_'); 
                                GAInteractionEventTracking('Webclick', 'interaction', 'hotline', '${gaCollegeName}');
                                cpeHotlineClearingForCdPage(this,'${interactionCollegeId}','${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.hotLineNo}', '', '${clr_courseId}');" >
                       <i class="fa fa-phone" aria-hidden="true"></i> 
                       <spring:message code="call.now.label" />
                     </a>
                   </li>
                 </c:if>
               </ul>        
             </c:forEach>          
           </c:if>
        </div>
   </div>  
 </div>
 <c:if test="${not empty opportunityId}">
   <c:set var="oppId" value="${opportunityId}"/>
 </c:if>
  <c:if test="${not empty cdOpportunityId}">
   <c:set var="oppId" value="${cdOpportunityId}"/>
 </c:if>
<input type="hidden" id="check_mobile_hidden" value="${mobileFlag}"/> 
<input type="hidden" id="applyNowFlag" value="${checkApplyFlag}"/>
<input type="hidden" id="oppId" value="${oppId}" />


