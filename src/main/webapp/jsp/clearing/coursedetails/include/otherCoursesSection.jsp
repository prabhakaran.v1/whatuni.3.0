<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${not empty viewAllCourses}">
  <section class="univer_like">
    <div id="div_top_courses"></div>
      <div class="cdp_vwalllnk">
        <a class="btnlnk" href="${viewAllCourses}">View all similar courses at this uni<i class="fa fa-long-arrow-right"></i></a>
      </div>
  </section>
</c:if>