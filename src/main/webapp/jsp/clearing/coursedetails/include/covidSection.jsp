<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${not empty hybridFlag and not empty compressedFlag}">
  <c:if test="${hybridFlag ne 'N' or compressedFlag ne 'N'}">
    <div class="covid_upt crdpgcvd_inf">
      <h3>How Covid-19 will affect this course</h3>
      <c:if test="${not empty startDate}">
        <ul class="dteinful">
          <li>Starts online</li>
          <li>Start date: ${startDate}</li>
        </ul>
      </c:if>
      <span class="covid_txt">
        <c:choose>
          <c:when test="${hybridFlag eq 'Y' and compressedFlag eq 'N'}">
            This course will start online and may then continue on campus depending on the latest coronavirus policies.
          </c:when>
          <c:when test="${hybridFlag eq 'N' and compressedFlag eq 'Y'}">
            This course will start on-campus. The duration of the course has been shortened because of the coronavirus pandemic.
          </c:when>
          <c:when test="${hybridFlag eq 'Y' and compressedFlag eq 'Y'}">
            This course will start online and may then continue on campus depending on the latest coronavirus policies. The duration of the course has been shortened because of the pandemic.
          </c:when>
        </c:choose>
      </span>
    </div>
  </c:if>
</c:if>
