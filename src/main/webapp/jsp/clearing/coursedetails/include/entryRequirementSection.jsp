<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<section class="clrcnge_entryreq p0 mb25">
  <div class="clear pt40">
    <div id="div_entry_req"></div>
    <div id="entryrequirments"></div>
    <h2 class="sub_tit descH fl">
      <span class="fnt_lrg fnt_24 lh_24 cl_grylt">Entry requirements</span> 
      <span class="tool_tip cdp_ettlptp"> 
        <img class="wuclrsr_imgicn" src="${domainPathWithImgSrc}clr20_icn_info_circle.svg" alt="Info Icon"> 
        <span class="cmp fnt_lbd">
          <div class="fnt_lbd fnt_14 lh_24">Entry requirements</div>
          <div>The minimum grades (and in some cases the subjects) you'll need to get on to this course.</div>
          <div class="line"></div>
        </span>
      </span>
    </h2>
    <div class="fr right_link">
      DATA SOURCE: <a class="cl_blult" rel="nofollow" href="/degrees/jsp/search/kisdataStatic.jsp">UCAS</a>
    </div>
  </div>
 
  <c:if test="${not empty minQuuivalentPoints }">
    <div class="cdpentr_detrow">
      <h4>Guide UCAS points</h4>
      <div class="cdpentr_detcol cdp_blubrdr">
        <h3>${minQuuivalentPoints} UCAS points</h3>
        <p>This score is for guidance only - you should contact the university to find out their accepted grades for this course.</p>
      </div>
    </div>
  </c:if>
   
  <div class="cdpentr_detrow">
   <c:if test ="${not empty scoreLabel}">
    <c:if test="${not empty scoreLabelTooltip}">
      <h4 class="wuclrsr_crsmtrtng">
        Your chance of acceptance
        <div class="wuclrsr_crsmtrtngrtg">
          <span class="tooltip_cnr ttip_cnt"> 
            <span class="tool_tip fl"> 
              <img class="wuclrsr_imgicn" src="${domainPathWithImgSrc}clr20_icn_info_circle.svg" alt="Info Icon"> 
              <span class="cmp">
                <div class="hdf5"></div>
                <div>
                  ${scoreLabelTooltip}
                  <!-- We are showing the minimum and maximum UCAS points scores that the institution has listed for all qualifications. -->
                  <br><br>
                  <!-- However the lowest and highest scores required for this course may vary by qualification. -->
                </div>
                <div>
                  <br><!-- Further information can be found by clicking the course title on this page and viewing the detailed entry requirements by qualification, but we also recommend you contact the institution directly. -->
                </div>
                <div class="line"></div>
              </span>
            </span>
          </span>
        </div>
      </h4>
    </c:if>
    </c:if>
    <c:if test="${not empty userScore and userScore gt 0}">     
      <div class="cdpentr_detcol cdp_blubrdr cdp_yucpnt">
        <div class="cdp_yucpntlft">
          <h4>Your UCAS points</h4>
          <h3>${userScore} UCAS points</h3>
          <c:if test ="${not empty scoreLabel}">
          <p>
            <c:choose>
              <c:when test="${scoreLevel eq 'LIKELY'}">
                Your UCAS points are higher than what's published.
              </c:when>
              <c:when test="${scoreLevel eq 'POSSIBLE'}">
                Your UCAS points exactly match what's published.
              </c:when>
              <c:when test="${scoreLevel eq 'STRETCH'}">
                Your UCAS points are lower than what's published but there may still be a chance that you're accepted for this course.
              </c:when>              
            </c:choose>
          </p>
          </c:if>
        </div>
        <c:if test ="${not empty scoreLabel}">
        <div class="cdp_yucpntrgt">
          <div class="wuclrsr_crsmtrtng">
            <c:if test="${not empty scoreLevel}">
              <c:set var="indicatorClass" value="${scoreLevel eq 'LIKELY' ? 'txtgrncol' : scoreLevel eq 'POSSIBLE' ? 'txtgrncol' : 'txtorngcol'}"/>
             </c:if>            
              <div class="wuclrsr_crsmtrtnglft ${indicatorClass}">
                <span class="wuclrsr_crsmtrtngtxt">${scoreLabel}</span>                 
                <span class="wuclrsr_crsmtrtngbrdrset"> 
                  <span class="wuclrsr_crsmtrtngbrd${scoreLevel ne 'LIKELY' ? ' brdrgry' : ''}"></span>                  
                  <span class="wuclrsr_crsmtrtngbrd${scoreLevel eq 'STRETCH' ? ' brdrgry' : ''}"></span>                  
                  <span class="wuclrsr_crsmtrtngbrd"></span> 
                  <span class="wuclrsr_crsmtrtngbrd"></span> 
                  <span class="wuclrsr_crsmtrtngbrd"></span>
                </span>
              </div>
          </div>
        </div>    
        </c:if>   
      </div>
      <div class="cdp_recalupntrow">
        <a href="javascript:void(0);" onclick="getUcasCalculator('cdpod')">
          <span class="cdp_reclc_imgicn"></span>
          Recalculate my UCAS points
        </a>
      </div>
    </c:if>
    <c:if test="${empty userScore or userScore le 0}">
      <div class="cdpentr_detcol cdp_redbrdr">
       <h4 class="cdp_redtxtcaps">You haven't added your grades</h4>
       <p>This will show you how likely you are to be accepted for a course.</p>
       <p><a href="javascript:void(0);" onclick="getUcasCalculator('cdpod')" class="cdp_blu_entgrdbtn">Enter Grades</a></p>
      </div>
    </c:if>
  </div>
</section>