<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import=" WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants"%>

<c:if test="${not empty uniInfoList}">
  <section class="univer_info">
    <div id="div_uni_info"></div>
    <c:forEach var="clr_uniInfoList" items="${uniInfoList}"> 
      <div class="fl w575">
        <h2 class="sub_tit fnt_lrg fl txt_lft w100p">Uni info</h2>
      </div>
      <div class="fl mt20 mob_univer_info">
        <div class="box_cnr rich_uni_info ">
          <div class="fl img_ppn_490 cdp_uniimgbox">             
           
             <c:set var="clearingImagePath"  value="${clr_uniInfoList.mediaPath}"/>  
             <c:set var="thumbnailPath" value="${clr_uniInfoList.thumbnailPath}"/>
             <%
               String deviceClearingimagePath =  pageContext.getAttribute("clearingImagePath").toString();
               String mobileClearingImagePath =  pageContext.getAttribute("clearingImagePath").toString();
               String thumbnailPath = pageContext.getAttribute("thumbnailPath").toString(); 
               pageContext.setAttribute("deviceClearingimagePath", deviceClearingimagePath);
               pageContext.setAttribute("mobileClearingImagePath", mobileClearingImagePath);             
               pageContext.setAttribute("thumbnailPath", thumbnailPath);
              %>   
            <c:if test="${clr_uniInfoList.mediaTypeId eq '66'}">                  
              <img class="lazy-load"  alt="${clr_uniInfoList.collegeNameDisplay}" 
                   src="<%=CommonUtil.getImgPath(GlobalConstants.ONE_PIXEL_IMG_PATH, 1)%>"
                   data-src="${domainPathWithoutImgSrc}/${deviceClearingimagePath}"
                   data-src-mobile="${domainPathWithoutImgSrc}/${mobileClearingImagePath}" />
            </c:if>
            <c:if test="${clr_uniInfoList.mediaTypeId eq '67'}">                  
              <img class="lazy-load" id="thumbnailImg_${clr_uniInfoList.mediaId}" 
                   width="100%" height="auto" src="<%=CommonUtil.getImgPath(GlobalConstants.ONE_PIXEL_IMG_PATH, 1)%>" 
                   alt="${clr_uniInfoList.collegeNameDisplay}"
                   data-src="${domainPathWithoutImgSrc}/${thumbnailPath}" 
                   data-src-mobile="${domainPathWithoutImgSrc}/${thumbnailPath}">
            </c:if>
                                  
           <%--  <c:if test="${clr_uniInfoList.mediaType eq 'VIDEO'}">
            
            <c:set var="thumbnailPath" value="${clr_uniInfoList.thumbnailPath}"/>
            <%
              String thumbnailPath = (String)pageContext.getAttribute("thumbnailPath"); 
              String videoPlayIcon = CommonUtil.getImgPath("/wu-cont/images/cpc_play_icon.svg",0);
              pageContext.setAttribute("thumbnailPath", thumbnailPath);
              pageContext.setAttribute("videoPlayIcon", videoPlayIcon);
            %>
            <div class="vide_icn">
		       <img class="pl_icn_img" src="${videoPlayIcon}" id="clr_uniInfor_play_icon_${clr_uniInfoList.mediaId}" onclick="clickPlayAndPause('openday_gallery_video_${gallerySectionList.mediaId}', event, 'thumbnailImg_${gallerySectionList.mediaId}', 'openday_play_icon_${gallerySectionList.mediaId}');" alt="play icon">
		    </div>
              <img class="lazy-load" id="thumbnailImg_${clr_uniInfoList.mediaId}" 
                   width="100%" height="auto" src="<%=CommonUtil.getImgPath(GlobalConstants.ONE_PIXEL_IMG_PATH, 1)%>" 
                   alt="${clr_uniInfoList.collegeNameDisplay}"
                   data-src="${domainPathWithoutImgSrc}/${thumbnailPath}" 
                   data-src-mobile="${domainPathWithoutImgSrc}/${thumbnailPath}">
                 
            <video preload="none" 
		           id="clearing_uni_info_video_${clr_uniInfoList.mediaId}" 
		           width="100%" height="100%" 
		           onclick="return cdPageVideoShow('${clr_uniInfoList.videoId}','${clr_uniInfoList.collegeId}','B') 
		           controls style="display:none" 
		           class="vhold"  
		           controlslist="nodownload noremoteplayback" 
		           disablepictureinpicture="">
		           <source src="${clr_uniInfoList.mediaPath}" type="video/mp4">
		     </video> 
            </c:if> --%>

          
           <%-- <c:if test="${empty clr_uniInfoList.videoUrl}">
              <c:if test="${not empty clr_uniInfoList.imagePath}">
                <c:set var="clearingImagePath"  value="${clr_uniInfoList.imagePath}"/>  
                <%
                  String deviceClearingimagePath = (pageContext.getAttribute("clearingImagePath").toString().indexOf("/CH/") > -1) ? pageContext.getAttribute("clearingImagePath").toString().replace("." ,"_768px.") : (pageContext.getAttribute("clearingImagePath").toString().indexOf("/rich/") > -1) ? pageContext.getAttribute("clearingImagePath").toString().replace("." ,"_992px.") : pageContext.getAttribute("clearingImagePath").toString();
                  String mobileClearingImagePath = (pageContext.getAttribute("clearingImagePath").toString().indexOf("/CH/") > -1) ? pageContext.getAttribute("clearingImagePath").toString().replace("." ,"_320px.") : (pageContext.getAttribute("clearingImagePath").toString().indexOf("/rich/") > -1) ? pageContext.getAttribute("clearingImagePath").toString().replace("." ,"_480px.") : pageContext.getAttribute("clearingImagePath").toString().replace("." ,"_353px.");
                  pageContext.setAttribute("deviceClearingimagePath", deviceClearingimagePath);
                  pageContext.setAttribute("mobileClearingImagePath", mobileClearingImagePath);
                %>
      
                <c:if test="${clr_uniInfoList.mediaTypeId eq '18'}">
                  <img width="490" height="270" alt="${clr_uniInfoList.collegeNameDisplay}" class="lazy-load" src="<%=CommonUtil.getImgPath(GlobalConstants.ONE_PIXEL_IMG_PATH, 1)%>" data-src="${domainPathWithoutImgSrc}/${deviceClearingimagePath}" data-src-mobile="${domainPathWithoutImgSrc}/${mobileClearingImagePath}"/>
                </c:if>
                <c:if test="${clr_uniInfoList.mediaTypeId ne '18'}">
                  <img alt="${clr_uniInfoList.collegeNameDisplay}" class="lazy-load" src="<%=CommonUtil.getImgPath(GlobalConstants.ONE_PIXEL_IMG_PATH, 1)%>" data-src="${domainPathWithoutImgSrc}/${deviceClearingimagePath}" data-src-mobile="${domainPathWithoutImgSrc}/${mobileClearingImagePath}"/>
                </c:if>
              </c:if>
            </c:if>
            <c:if test="${not empty clr_uniInfoList.videoUrl }">
              <%request.setAttribute("showlightbox","YES");%>
              <div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
                <c:if test="${not empty clr_uniInfoList.videoMetaDuration}">
                  <meta itemprop="duration" content="${clr_uniInfoList.videoMetaDuration}" />
                </c:if>
                <meta itemprop="thumbnailUrl" content="${clr_uniInfoList.videoThumbnailUrl}" />
                <meta itemprop="embedURL" content="${clr_uniInfoList.videoUrl}" />
                <a rel="nofollow" class="vhold" onclick="return cdPageVideoShow('${clr_uniInfoList.videoId}','${clr_uniInfoList.collegeId}','B')" title='${clr_uniInfoList.collegeNameDisplay}'>
                  <img width="490" height="270" src="${clr_uniInfoList.videoThumbnailUrl}" alt="${clr_uniInfoList.collegeNameDisplay}"/>
                  <i id="img_std_choice_icon" class="fa fa-play fa-8x"></i>
                </a>
              </div>
            </c:if> --%>
          </div>  
      
          <div class="uni_info_right fr opd_univinfo cdp_uniinfbox">
            <div class="wuclrpr_inslogo">
              <c:if test="${not empty collegeLogo}">
                <a href="${clr_uniInfoList.clearingProfileURL}">
                  <img src="${domainPathWithoutImgSrc}${collegeLogo}"
                       data-src-mobile="${domainPathWithoutImgSrc}<%=GlobalConstants.ONE_PIXEL_IMG_PATH%>"
                       width="77" class="ibdr"
                       alt="${clr_uniInfoList.collegeNameDisplay}">
                </a>
              </c:if>
            </div>
            <div class="cdp_uniinfboxrgt">
              <div class="cdp_uniinfdelft">
                <div class="wuclrsr_unittldet">
                  <div class="wuclrsr_unittl_lft">
                    <c:if test="${not empty  clr_uniInfoList.collegeNameDisplay}">
                      <h3>
                        <a href="${clr_uniInfoList.clearingProfileURL}">${clr_uniInfoList.collegeNameDisplay}</a>
                      </h3>
                    </c:if>
                  </div>
                </div>
                <div class="wuclrsr_unirvwdet">
                  <ul class="strat cf">
                    <c:set var="tick_green" value="clr20_icn_tick_green.svg"/>
                    <c:set var="cross_red" value="content_hub/icons/clr_ac_sr_no.svg"/>
                    <c:if test="${clr_uniInfoList.accommodationFlag eq 'Y' and clr_uniInfoList.scholarshipsFlag eq 'Y'}">
                      <li>
                        <img class="wuclrsr_imgicn" src="${domainPathWithImgSrc}${tick_green}" alt="Tick Icon"> 
                        <spring:message code="accommodation.available.label"/>
                      </li>
                      <li>
                        <img class="wuclrsr_imgicn" src="${domainPathWithImgSrc}${tick_green}" alt="Tick Icon"> 
                        <spring:message code="scholarships.available.label"/>
                      </li>
                    </c:if>
                    <c:if test="${clr_uniInfoList.accommodationFlag eq 'Y' and clr_uniInfoList.scholarshipsFlag eq 'N'}">
                      <li>
                        <img class="wuclrsr_imgicn" src="${domainPathWithImgSrc}${tick_green}" alt="Tick Icon"> 
                        <spring:message code="accommodation.available.label"/>
                      </li>
                      <li>
                       <img class="wuclrsr_imgicn" src="${domainPathWithImgSrc}${cross_red}" alt="Cross Icon"> 
                       <spring:message code="scholarships.available.label"/>
                      </li>
                    </c:if>
                    <c:if test="${clr_uniInfoList.accommodationFlag eq 'N' and clr_uniInfoList.scholarshipsFlag eq 'Y'}">
                      <li>
                        <img class="wuclrsr_imgicn" src="${domainPathWithImgSrc}${cross_red}" alt="Cross Icon"> 
                        <spring:message code="accommodation.available.label"/>
                     </li>
                     <li>
                       <img class="wuclrsr_imgicn" src="${domainPathWithImgSrc}${tick_green}" alt="Tick Icon"> 
                       <spring:message code="scholarships.available.label"/>
                     </li>
                   </c:if>
                   <c:if test="${clr_uniInfoList.accommodationFlag eq 'N' and clr_uniInfoList.scholarshipsFlag eq 'N'}">
                     <li>
                       <img class="wuclrsr_imgicn" src="${domainPathWithImgSrc}${cross_red}" alt="Cross Icon"> 
                       <spring:message code="accommodation.available.label"/>
                     </li>
                     <li>
                       <img class="wuclrsr_imgicn" src="${domainPathWithImgSrc}${cross_red}" alt="cross Icon"> 
                       <spring:message code="scholarships.available.label"/>
                     </li>
                   </c:if>
                 </ul>
               </div>
             </div>
             <c:if test="${not empty clr_uniInfoList.awardImagePath}">
               <div class="cdp_uniinfdergt">
                 <img src="${domainPathWithoutImgSrc}/${clr_uniInfoList.awardImagePath}" alt="award image">
               </div>
             </c:if>
           </div>
         </div>
       </div>
     </div>
   </c:forEach>
 </section>
</c:if>
