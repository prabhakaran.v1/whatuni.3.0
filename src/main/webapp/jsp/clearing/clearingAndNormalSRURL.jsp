<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<%@page import="WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator"%>
<%
  String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");
  String clearingPage =  (String)request.getAttribute("searchClearing");//10-JUN-2015_REL
   clearingPage = GenericValidator.isBlankOrNull(clearingPage)?"":"TRUE";
  if("ON".equalsIgnoreCase(clearingonoff)){
  String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
  if("2".equalsIgnoreCase(cpeQualificationNetworkId)){
    String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "NULL";
    String clerNormalSRUrl = "";
    String substr = "";  
    String appendString = "For " + GlobalConstants.NEXT_YEAR + " entry";
    /*if(GenericValidator.isBlankOrNull(clearingPage)){//10-JUN-2015_REL
      substr = firstPageSeoUrl.substring(0,firstPageSeoUrl.lastIndexOf("/page.html"));
      clerNormalSRUrl ="/degrees"+substr+"/page.html";     
    }else{//10-JUN-2015_REL
      substr = firstPageSeoUrl.substring(0,firstPageSeoUrl.lastIndexOf("/page.html"));
      clerNormalSRUrl ="/degrees"+substr+"/page.html";
    }*/
    clerNormalSRUrl = firstPageSeoUrl;
    String qString = (String)request.getAttribute("queryStr");
           qString = !GenericValidator.isBlankOrNull(qString)? (qString.replace("clearing&","")):"";
           qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
           //clerNormalSRUrl = clerNormalSRUrl + qString;
  /*if("TRUE".equals(clearingPage)){
     qString = qString.replace("?clearing","");
     qString =  !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  }else{
   qString = !GenericValidator.isBlankOrNull(qString)?  (qString.replace("?","?clearing")):"?clearing";  
  }*/
%>
 <span class="clr_ent_link">
 <c:if test="${not empty requestScope.cleraringBrowseURL}">
    <c:if test="${not empty requestScope.subjectDesc}">
      <a href="<%=request.getAttribute("cleraringBrowseURL")%><%=qString%>">  <%=appendString%> ${requestScope.subjectDesc} degrees<i class="fa fa-long-arrow-right"></i></a>
    </c:if>
    <%--<logic:present name="SEARCH_KEYWORD_OR_SUBJECT" scope="request">  
      <a href="<%=request.getAttribute("cleraringBrowseURL")%><%=qString%>">  <%=appendString%> <bean:write name="SEARCH_KEYWORD_OR_SUBJECT" scope="request"/> degrees<i class="fa fa-long-arrow-right"></i></a>
    </logic:present>--%>
  </c:if>
  <c:if test="${empty requestScope.cleraringBrowseURL}">
  <c:if test="${not empty requestScope.SEARCH_KEYWORD_OR_SUBJECT}">
   <c:if test="${requestScope.NORMAL_BROWSE eq 'Y'}">
        <a href="<%=clerNormalSRUrl%>"><%=appendString%> ${requestScope.SEARCH_KEYWORD_OR_SUBJECT} degrees <i class="fa fa-long-arrow-right"></i></a>
      </c:if>
    </c:if>
    <c:if test="${not empty requestScope.subjectDesc}">
    <c:if test="${requestScope.NORMAL_BROWSE eq 'Y'}">
        <a href="<%=clerNormalSRUrl%>"><%=appendString%> ${requestScope.subjectDesc} degrees <i class="fa fa-long-arrow-right"></i></a>
     </c:if>
    </c:if>
  </c:if>
 </span>
<%}}%>
