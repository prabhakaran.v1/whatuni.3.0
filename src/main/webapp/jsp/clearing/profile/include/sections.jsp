<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
 String noMediaClass = "";
 CommonUtil util = new CommonUtil();
 CommonFunction common = new CommonFunction();
 String overviewTrans = "";
%>

<c:if test="${not empty requestScope.SECTIONS_LIST}">
    <c:forEach var="sectionsList" items="${requestScope.SECTIONS_LIST}" varStatus="row" >
    <c:set var="rowValue" value="${row.index}"/>
      <c:set var="imgPath" value="${sectionsList.imagePath}"/>
      <div class="clear"></div>      
      <section class="cont_fluid row4 rw<%=(Integer.parseInt(pageContext.getAttribute("rowValue").toString())) + 3%>" id="con-for-nav-<%=(Integer.parseInt(pageContext.getAttribute("rowValue").toString())) + 1%>">
        <div class="csticky-holder">
            <c:if test="${empty sectionsList.imagePath}">
              <%noMediaClass = "no_media";%>
            </c:if>
            <%if(Integer.parseInt(pageContext.getAttribute("rowValue").toString()) == 0) {
              overviewTrans = "trans";
            } else {
              overviewTrans = "";
            }%>
              <article class="clft_cnt1 csticky <%=overviewTrans%> <%=noMediaClass%>" id="article_sticky_<%=pageContext.getAttribute("rowValue").toString()%>">              
                <article class="col_lft">
                    <c:if test="${sectionsList.mediaTypeId ne '29'}">
                      <div id="imageSectionHolder_<%=pageContext.getAttribute("rowValue").toString()%>"  class="cont_rat">
                         <c:if test="${not empty sectionsList.imagePath}">
                           <div class="ovr_vw"><img class="sections" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"                           
                             data-src='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._580PX)%>'
                             data-src-ipad='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._307PX)%>'
                             data-src-tab='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._580PX)%>'
                             data-src-mobile='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._320PX)%>'
                             data-img-load-type='lazyLoad'
                             data-one-pix="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"
                             data-media-name="${sectionsList.mediaName}"
                             alt="${sectionsList.sectionNameDisplay}">
                           </div>
                         </c:if>
                         <div class="ovr_hdr">   
                          <c:set var="sectionName" value="${sectionsList.sectionNameDisplay}"/>
                           <c:set var="sectionNameOrg" value="${sectionsList.sectionName}"/>
                          <%String sectionName = (String)pageContext.getAttribute("sectionName");
                          String sectionNameOrg = (String)pageContext.getAttribute("sectionNameOrg");
                          if(!"Clearing".equalsIgnoreCase(sectionName) && !"What's New".equalsIgnoreCase(sectionNameOrg) && !"Clearing USPs".equalsIgnoreCase(sectionNameOrg) && !"What Happens Next?".equalsIgnoreCase(sectionNameOrg) && !"Our Clearing Guide".equalsIgnoreCase(sectionNameOrg) && !"Contact Details".equalsIgnoreCase(sectionNameOrg) && !"COVID 19".equalsIgnoreCase(sectionNameOrg)){
                          if("Students' Union".equalsIgnoreCase(sectionName)){
                            sectionName = "Student Union";
                            }
                            String uniImagePath = "/wu-cont/images/content_hub/icons/"+ common.replaceHypen(common.replaceURL(sectionNameOrg.toLowerCase())) +".svg";%>
                           <img src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" 
                             data-src='<%=CommonUtil.getImgPath(uniImagePath,0)%>'
                             data-src-ipad='<%=CommonUtil.getImgPath(uniImagePath,0)%>'
                             data-src-tab='<%=CommonUtil.getImgPath(uniImagePath,0)%>'
                             data-src-mobile='<%=CommonUtil.getImgPath(uniImagePath,0)%>'
                             alt="${sectionsList.sectionNameDisplay}">
                             <%} %>
                          <h1>${sectionsList.sectionNameDisplay}</h1>
                        </div>
                        <div class="ovr_lay"></div>
                      </div>
                     </c:if>
                     <c:if test="${sectionsList.mediaTypeId eq '29'}">
                        <div id="videoSectionHolder_<%=pageContext.getAttribute("rowValue").toString()%>" class="vid-content facilities">
                          <c:set var="sectionImgPath" value="${sectionsList.thumbnailPath}"/>
                          <%String sectionImgPath = (String)pageContext.getAttribute("sectionImgPath"); %>
                          <div id="bg_video_section_<%=pageContext.getAttribute("rowValue").toString()%>" class="vid_bgg">
                             <%--<span class="vid_icon" onclick="playAndPause('video_section_<%=row%>')" style="display:block" id="sectPlayIconId<%=row%>"></span>--%>
                             <div class="play-icon" onclick="playAndPause('video_section_<%=pageContext.getAttribute("rowValue").toString()%>', event, 'overlay_div')" id="sectPlayIconId<%=pageContext.getAttribute("rowValue").toString()%>"> <img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/play-icon.png",0)%>" alt="section image play icon" style="display: block;"> </div>
                             <img src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"                           
                             data-src='<%=util.contentHubDeviceSpecificSectionPath(sectionImgPath,GlobalConstants._580PX)%>'
                             data-src-ipad='<%=util.contentHubDeviceSpecificSectionPath(sectionImgPath,GlobalConstants._307PX)%>'
                             data-src-tab='<%=util.contentHubDeviceSpecificSectionPath(sectionImgPath,GlobalConstants._580PX)%>'
                             data-src-mobile='<%=util.contentHubDeviceSpecificSectionPath(sectionImgPath,GlobalConstants._320PX)%>'
                             data-img-load-type='lazyLoad'
                             data-one-pix="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"
                             alt="${sectionsList.sectionNameDisplay}">
                          </div>
                          <div class="ovr_hdr" onclick="playAndPause('video_section_<%=pageContext.getAttribute("rowValue").toString()%>', event, 'overlay_div')">    
                          <c:set var="sectionNameOne" value="${sectionsList.sectionNameDisplay}"/>
                          <c:set var="sectionNameOneOrg" value="${sectionsList.sectionName}"/> 
                          <%String  sectionNameOne = (String)pageContext.getAttribute("sectionNameOne");
                          String sectionNameOneOrg = (String)pageContext.getAttribute("sectionNameOneOrg");
                          String uniImagePath = "/wu-cont/images/content_hub/icons/"+ common.replaceHypen(common.replaceURL(sectionNameOneOrg.toLowerCase())) +".svg";%>
                          <%if(!"Clearing".equalsIgnoreCase(sectionNameOne) && !"What's New".equalsIgnoreCase(sectionNameOneOrg) && !"Clearing USPs".equalsIgnoreCase(sectionNameOneOrg) && !"What Happens Next?".equalsIgnoreCase(sectionNameOneOrg) && !"Our Clearing Guide".equalsIgnoreCase(sectionNameOneOrg) && !"Contact Details".equalsIgnoreCase(sectionNameOneOrg) && !"COVID 19".equalsIgnoreCase(sectionNameOneOrg)){%>
                            <img src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" 
                             data-src='<%=CommonUtil.getImgPath(uniImagePath,0)%>'
                             data-src-ipad='<%=CommonUtil.getImgPath(uniImagePath,0)%>'
                             data-src-tab='<%=CommonUtil.getImgPath(uniImagePath,0)%>'
                             data-src-mobile='<%=CommonUtil.getImgPath(uniImagePath,0)%>'
                             alt="${sectionsList.sectionNameDisplay}">
                             <%} %>
                            <h1>${sectionsList.sectionNameDisplay}</h1>
                          </div>
                          <c:if test="${not empty sectionsList.imagePath}">
                            <div id="wrapper_video_section_<%=pageContext.getAttribute("rowValue").toString()%>" class="home-header__player" style="display:none">
                                <video preload="none" onclick="playAndPause('video_section_<%=pageContext.getAttribute("rowValue").toString()%>',event, '')" onpause="updateVideoMapOnPlayPause(this, 'pause', event);hideShowIconOnClickPlayAndPauseVideo('video_section_<%=pageContext.getAttribute("rowValue").toString()%>', 'pause')" onplay="updateVideoMapOnPlayPause(this, 'play', event);hideShowIconOnClickPlayAndPauseVideo('video_section_<%=pageContext.getAttribute("rowValue").toString()%>', 'play')" id="video_section_<%=pageContext.getAttribute("rowValue").toString()%>" class="sectionpod" controlslist="nofullscreen nodownload" controls="" playsinline="" data-media-name="${sectionsList.mediaName}" data-media-id="${sectionsList.mediaId}">
                                  <source src="${sectionsList.imagePath}" type="video/mp4">
                                </video>
                            </div>
                            <input type="hidden" id="<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>_sectSubOrderItemId_${sectionsList.mediaId}" name="<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>_sectSubOrderItemId_${sectionsList.mediaId}" value="${sectionsList.subOrderItemId}"/>
                          </c:if>  
                        </div>
                      </c:if>
                      
                      
                      
                </article>
            </article>
            <article class="col_rgt">
                <div class="trnk_cnt">
                <c:if test="${not empty sectionsList.awardImage}">
                <c:set var="awardImage" value="${sectionsList.awardImage}"/>
                <div class="rnk_awrd">
                 <img src="<%=CommonUtil.getImgPath((String)pageContext.getAttribute("awardImage"),0)%>" alt="award image">        
                </div>
                </c:if>
                    <div class="line-div"></div>
                    <c:if test="${sectionsList.sectionNameDisplay ne 'Clearing USPs'}">
                    <c:if test="${not empty sectionsList.wuscaRank}">
                    <div class="wrnk_ttip">
                      <c:set var="sectionNames" value="${sectionsList.sectionNameDisplay}"/>
                     <%String sectionNames = (String)pageContext.getAttribute("sectionNames");
                     if(sectionNames.equalsIgnoreCase("Overview")) {
                      sectionNames = "overall";
                       } %>
                          <div class="ovw_rnk">
                              <div class="ornk_txt">WUSCA ${sectionsList.sectionNameDisplay} ranking 
                                  <span class="ch_ttip" onclick="showAndHideToolTip('wuscatooltiptext<%=pageContext.getAttribute("rowValue").toString()%>')" onmouseover="showTooltip('wuscatooltiptext<%=pageContext.getAttribute("rowValue").toString()%>')" onmouseout="hideTooltip('wuscatooltiptext<%=pageContext.getAttribute("rowValue").toString()%>')">
                                      <span class="ct_hov"></span>
                                      <span class="bul_ttip" id="wuscatooltiptext<%=pageContext.getAttribute("rowValue").toString()%>"><spring:message code="wuni.contenthub.wusca.rating.review" arguments="<%=sectionNames%>"/>
                                          <span class="ttip_arw"></span>
                                      </span>
                                  </span>
                              </div>
                              <div class="ornk_rnge"><span>${sectionsList.wuscaRank}/</span>${sectionsList.wuscaOverall}</div>
                          </div>
                          </div>
                      </c:if>
                    <div class="ar_cnt">
                    <c:if test="${not empty sectionsList.description}">
                       ${sectionsList.description}
                    </c:if>                  
                    </div>
                    </c:if>
                    <c:if test="${sectionsList.sectionNameDisplay eq 'Clearing USPs'}">
                       <c:set var = "descPoints" value = "${fn:split(sectionsList.description, '###')}" />
	                   <c:forEach var="descPoint" items="${descPoints}" varStatus="idx">  
	                    <div class="clr_ups">
					   	  <div class="clr_ups_lft">${idx.index + 1}</div>
						  <div class="clr_ups_rit">${descPoint}</div>
					    </div>                   
	                   </c:forEach>					
                    </c:if>
                    <div class="line-div1"></div>                   
                </div>
            </article>
        </div>
        <input type="hidden" id="sectionProfileId_<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" name="sectionProfileId_<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" value="${sectionsList.profileId}"/>
        <input type="hidden" id="sectMyhcProfileId_<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" name="sectMyhcProfileId_<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" value="${sectionsList.myhcProfileId}"/>
        <input type="hidden" id="sectSubOrderItemId_<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" name="sectSubOrderItemId_<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" value="${sectionsList.subOrderItemId}"/>
        <c:if test="${not empty sectionsList.sectionName}">
        <c:set var="sectionNameGA" value="${sectionsList.sectionName}"/> 
        <%String sectionNameGA = (String)pageContext.getAttribute("sectionNameGA");
        if("Students' Union".equalsIgnoreCase(sectionNameGA)){
          sectionNameGA = "Student Union";
         }%>
        <input type="hidden" id="sectionName_<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" name="sectionName_<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" value="<%=sectionNameGA%>"/>
        </c:if>
      </section>
    </c:forEach>
</c:if>