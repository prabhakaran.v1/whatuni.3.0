<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, com.wuni.util.seo.SeoUrls,  WUI.utilities.GlobalConstants" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%
CommonFunction common = new CommonFunction();
String collegeName = request.getAttribute("COLLEGE_NAME") != null && request.getAttribute("COLLEGE_NAME").toString().trim().length() > 0 ? common.replaceSpecialCharacter((String)request.getAttribute("COLLEGE_NAME")).trim() : "0";
String wp_collegeId = request.getAttribute("COLLEGE_ID") != null && request.getAttribute("COLLEGE_ID").toString().trim().length() > 0 ? (String)request.getAttribute("COLLEGE_ID") : "0";
String providerOpendayUrl = new SeoUrls().constructOpendaysSeoUrl(collegeName, wp_collegeId);
CommonUtil util = new CommonUtil();
%>


<c:if test="${not empty requestScope.GALLERY_LIST}">
        <!-- More Image Gallery -->
      <c:if test="${requestScope.GALLERY_FLAG eq 'Y'}">
      <div class="clear"></div>
        <section class="gallery-section rw23" id="con-for-nav-36">                            
               <div class="image-gallery">
                <div class="gal_head">
                   <h2>Life at ${requestScope.COLLEGE_NAME_DISPLAY}</h2>
                   <p id="mediaName"></p>
                   <c:if test="${not empty requestScope.opendayListSize}">
                     <c:forEach var="openDayList" items="${requestScope.OPEN_DAY_LIST}" varStatus="row" end="0">
                        <c:if test="${requestScope.opendayListSize gt 1}">                          
                          <a class="tkvr_btn" href="<%=providerOpendayUrl%>">TAKE VIRTUAL TOUR</a>
                        </c:if>
                        <c:if test="${requestScope.opendayListSize eq 1}">
                          <a class="tkvr_btn" target="_blank" href="${openDayList.bookingUrl}" onclick="GANewAnalyticsEventsLogging('open-days-type', '${eventCategoryNameGa}', '<%=collegeName%>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Reserve A Place', '${openDayList.collegeDispName}');addOpenDaysForReservePlace('<c:out value="${openDayList.openday}" escapeXml="false" />', '<c:out value="${openDayList.monthYear}" escapeXml="false" />', '<c:out value="${openDayList.collegeId}" escapeXml="false" />', '${openDayList.suborderItemId}', '${openDayList.networkId}'); cpeODReservePlace(this,'${openDayList.collegeId}','${openDayList.suborderItemId}','${openDayList.networkId}','${openDayList.bookingUrl}');">TAKE VIRTUAL TOUR</a>
                        </c:if>
                     </c:forEach> 
                   </c:if>
                   
                 </div>
                <div id="main" role="main">
                    <div class="slider-main">
                        <div id="slider" class="flexslider">
                            <ul class="slides" id="sliderUlId">
                                  <c:forEach var="galleryList" items="${requestScope.GALLERY_LIST}" varStatus="row" begin="1">
                                  <c:set var="rowValue" value="${row.index}"/>
                                    <c:set var="imgPath" value="${galleryList.mediaPath}"/>
                                      <li>
                                         <p id="mediaName_<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())%>" style="display:none">
                                           <%-- <c:if test="${not empty galleryList.mediaName}">
                                              ${galleryList.mediaName}
                                           </c:if> --%>
                                         </p>
                                        <c:if test="${galleryList.mediaTypeId ne 67}">
                                          <div class="overlay"></div>
                                          <img  id="imageGallery<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"                                           
                                          data-src="${galleryList.mediaPath}"                                          
                                          data-src-ipad='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._768PX)%>'
                                          data-src-tab='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._768PX)%>'
                                          data-src-mobile='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._320PX)%>'
                                          data-media-name="${galleryList.mediaName}"
                                          data-img-load-type='lazyLoad'
                                          data-one-pix="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"
                                          class="Gallery image"  alt="Life at ${requestScope.COLLEGE_NAME_DISPLAY}">
                                        </c:if>
                                        <c:if test="${galleryList.mediaTypeId eq 67}">                         
                                          <c:set var="imgPathGallery" value="${galleryList.thumbNailPath}"/>
                                          <div id="bg_vid-slider<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())%>" class="vid_bgg">
                                             <span class="vid_icon" onclick="playAndPause('vid-slider<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())%>',event, 'overlay_div')" style="display:block" id="playIconId<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())%>"></span>
                                             <img src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>'
                                             data-src='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPathGallery"),GlobalConstants._768PX)%>'
                                             data-src-ipad='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPathGallery"),GlobalConstants._768PX)%>'
                                             data-src-mobile='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPathGallery"),GlobalConstants._320PX)%>'
                                             data-img-load-type='lazyLoad'
                                             data-one-pix="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"
                                             class="Gallery image"/>
                                          </div>                                          
                                          <video preload="none" style="display:none" onclick="playAndPause('vid-slider<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())%>',event, '')" onpause="updateVideoMapOnPlayPause(this, 'pause', event);hideAndShowPlayIcon('playIconId<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())%>', 'pause');" onplay="updateVideoMapOnPlayPause(this, 'play', event);hideAndShowPlayIcon('playIconId<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())%>', 'play')" id="vid-slider<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())%>" loop="" controlslist="nofullscreen nodownload" class="flexpod" controls="false" playsinline="" data-media-name="${galleryList.mediaName}">
                                              <source src="${galleryList.mediaPath}" type="video/mp4">
                                          </video>
                                        </c:if>
                                      </li>
                                  </c:forEach>
                                
                            </ul>
                        </div>
                        <div id="carousel" class="flexslider">
                            <ul class="slides">
                                 <c:forEach var="galleryList" items="${requestScope.GALLERY_LIST}" varStatus="row" begin="1">
                                   <c:set var="img_Path" value="${galleryList.thumbNailPath}"/> 
                                   <c:set var="rowValue" value="${row.index}"/>
                                    <li>
                                      <c:if test="${galleryList.mediaTypeId eq 67}">                          
                                        <span class="vid_icon slider-vid<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())%>"></span>
                                      </c:if>    
                                      <img src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" 
                                      data-src="<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("img_Path"),GlobalConstants._170PX)%>"                                                                                 
                                      data-src-ipad='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("img_Path"),GlobalConstants._170PX)%>'
                                      data-src-tab='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("img_Path"),GlobalConstants._170PX)%>'
                                      data-src-mobile='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("img_Path"),GlobalConstants._170PX)%>'
                                      data-img-load-type='lazyLoad'
                                      data-one-pix="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"
                                      data-media-name="${galleryList.mediaName}"
                                      class="Carousal image"  alt="Life at ${requestScope.COLLEGE_NAME_DISPLAY}">
                                    </li>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
              </div>  
            <input type="hidden" id="sectionName_36" name="sectionName_36" value="GALLERY"/>
        </section>
        <script type="text/javascript">
          formGallerySlider();
        </script>
       </c:if>
  </c:if>
