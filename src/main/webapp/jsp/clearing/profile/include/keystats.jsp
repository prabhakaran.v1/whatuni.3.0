<%@page import="WUI.utilities.CommonUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
CommonUtil util = new CommonUtil();
String statsValue1Position = "";
String posOrdinal = "";
%>
<c:if test="${requestScope.ALL_STATS_EMPTY_FLAG eq 'N'}">        
  <c:if test="${not empty requestScope.UNI_STATS_LIST}">
      <div class="clear"></div>
      <section class="cont_fluid rw16" id="con-for-nav-31">
        <article class="key_stats">
          <div class="keystats-cont">
            <c:if test="${requestScope.KEY_STATS_EMPTY_FLAG eq 'N'}">
              <c:forEach var="uniStatsList" items="${requestScope.UNI_STATS_LIST}" end="3"  varStatus="row">
              <c:set var="rowValue" value="${row.index}"/>
               <%String statsDesc = (1==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "WUSCA ranking" :(2==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "CUG ranking" : (3==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "Undergraduate students" : (4==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "TEF score" : "";
                 String className = "rat_val";
                 statsValue1Position = "";
               %>                                      
                 <%if(1==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1){%>
                 <h2>Key stats</h2>
                   <ul class="clr_ac_sr">
						<li class="${ACCOMODATION_AVAIL_FLAG eq 'Y' ? 'yes' : 'no' }">Accommodation available</li>
						<li class="${SCHOLARSHIP_AVAIL_FLAG eq 'Y' ? 'yes' : 'no' }">Scholarships available</li>
					</ul>
                  <div class="stats-wrap clr">
                 <%}%> 
                 
                    <div class="stats-lists fl fadeInUp wow" data-wow-delay=".4s">
                      <c:if test="${uniStatsList.customizedFlag eq 'N'}">
                        <div id="key_sts1">
                          <c:if test="${not empty uniStatsList.statsValue1}">
                            <c:set var="statsValue1Pos" value="${uniStatsList.statsValue1}"/>                             
                            <h4><%=statsDesc%> 
                            <%if((1==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1)|| (3==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1)){%>
                              <span class="ch_ttip lft">
                            <%}
                            statsValue1Position = (String)pageContext.getAttribute("statsValue1Pos");
                            if ((2==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) || (4==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1)){ %>                         
                            <span class="ch_ttip">
                             <%}%>                  
                            <span class="ct_hov"></span>
                                <span class="bul_ttip">
                                <%if(statsDesc.equalsIgnoreCase("WUSCA ranking")) {%>
                                  <spring:message code="wuni.contenthub.wusca.rating.review" arguments="overall"/>
                                <%}
                                if(statsDesc.equalsIgnoreCase("CUG ranking")) {%>
                                  <spring:message code="wuni.tooltip.cug.ranking.text"/>
                                <%}
                                if(statsDesc.equalsIgnoreCase("Undergraduate students")) {%>
                                  <spring:message code="wuni.tooltip.hesa.entry.text" />
                                <%}
                                 if(statsDesc.equalsIgnoreCase("TEF score")) {%>
                                  <spring:message code="wuni.contenthub.keystats.tef.score"/>
                                <%}%>
                                    <span class="ttip_arw"></span>
                                </span>
                            </span>
                          </h4>
                          </c:if>
                          <div class="rating">
                            <%if(4==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1){%>
                              <c:if test="${not empty uniStatsList.statsValue1}">
                                <c:set var="tefLogoColor" value="${uniStatsList.statsValue1}"/>
                                <h5><span class="medium"><img src='<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/TEF_"+(String)pageContext.getAttribute("tefLogoColor")+"_logo.png",0)%>' alt="tefLogoColor"></span></h5>
                              </c:if>
                            <%}else{
                               posOrdinal = (1==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1||2==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? util.getSuperStringSTNDRDTH(statsValue1Position) : "NaN";
                               if((1==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1||2==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) && !"NaN".equalsIgnoreCase(posOrdinal)){%>                                                           
                                <h5>${uniStatsList.statsValue1}<%=posOrdinal%>                               
                             <%}else{%>
                                <h5>${uniStatsList.statsValue1}
                             <%}
                               if(1==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1||2==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1){%>
                                <c:if test="${not empty uniStatsList.statsValue2}">
                                  <span class="slash">/</span>
                                </c:if>  
                              <%}
                                if(3==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1){ className="medium"; %>
                                  <br>
                              <%}%>                                
                                <c:if test="${not empty uniStatsList.statsValue2}"><span class="<%=className%>">${uniStatsList.statsValue2}</span></c:if></h5>                              
                            <%}%>
                          </div>
                        </div>
                      </c:if>
                      <c:if test="${uniStatsList.customizedFlag eq 'Y'}">
                        <div id="key_sts1">
                          <c:if test="${not empty uniStatsList.statsValue1}">
                            <div class="cus_data">
                              <div class="cd_hd">${uniStatsList.statsDescription}</div>
                              <div class="cd_val">${uniStatsList.statsValue1}</div>
                            </div>
                          </c:if>
                        </div>
                      </c:if>                        
                    </div>
                                  
                  <%if(4==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1){%>                                        
                    </div>
                  <%}%> 
               </c:forEach>
             </c:if>
             <c:if test="${requestScope.STUDENTS_STATS_EMPTY_FLAG eq 'N'}">
               <c:forEach var="uniStatsList" items="${requestScope.UNI_STATS_LIST}" begin="4"  varStatus="row">
               <c:set var="rowValue" value="${row.index}"/>
                 <%String stats1Desc = (5==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "Full-time" :(6==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "School leavers" : (7==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "Undergraduate" : (8==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "UK" : "";%>
                 <%String stats2Desc = (5==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "Part-time" :(6==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "Mature students" : (7==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "Postgraduate" : (8==Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1) ? "International" : "";%>
                 <%if(5 == Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1){%>
                  <div class="line"><span></span></div>
                  <div class="ksts_rat">
                    
                      <ul>
                     <%}%>     
                        <li>
                          <c:if test="${uniStatsList.customizedFlag eq 'N'}">
                          <c:if test="${not empty uniStatsList.statsValue1 and not empty uniStatsList.statsValue2}">
                            <div class="ksts_txt"><span class="fl"><%=stats1Desc%></span><span class="fr"><%=stats2Desc%></span></div>
                            <div class="ksts_val"><span class="fl">${uniStatsList.statsValue1}%</span><span class="fr">${uniStatsList.statsValue2}%</span></div>
                            <div class="ksts_bar">
                                <div class="prg_bar"><span style="width:${uniStatsList.statsValue1}%"></span></div>
                            </div>
                          </c:if>
                          </c:if>
                          <c:if test="${uniStatsList.customizedFlag eq 'Y'}">
                            <c:if test="${not empty uniStatsList.statsValue1}">                     
                              <div class="cus_data">
                                <div class="cd_hd">${uniStatsList.statsDescription}</div>
                                <div class="cd_val">${uniStatsList.statsValue1}</div>
                              </div>                                                   
                            </c:if>
                          </c:if>
                        </li>                  
                 
                <%if(8 == Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1){%>                       
                    </ul>
                  </div>
                <%}%>      
              </c:forEach>
            </c:if>
          </div>
        </article>
        <input type="hidden" id="sectionName_31" name="sectionName_31" value="KEY STATS"/>
      </section>
  </c:if>
</c:if>