<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import=" WUI.utilities.CommonUtil" %>

<% CommonUtil commonUtil = new CommonUtil();
 String userNameColorClassName = "rev_blue";
%> 
<div class="rev_boxmn">
  <c:if test="${empty requestScope.latestSubjectAjaxReview}">
    <c:if test="${not empty requestScope.LATEST_UNI_REVIEW_LIST}">
      <c:forEach var="latestReviews" items="${requestScope.LATEST_UNI_REVIEW_LIST}"  varStatus="cnt">
        <div class="rev_box">
          <div class="rlst_row">
            <div class="revlst_lft">
              <div class="rlst_wrp">
                <c:if test="${not empty latestReviews.userNameInitial}">
                  <c:if test="${not empty latestReviews.userNameInitial}">
                    <c:set var="userName" value="${latestReviews.userNameInitial}"/> 
                    <%
                      String userName = pageContext.getAttribute("userName").toString();
                      userNameColorClassName = commonUtil.getReviewUserNameColorCode(userName);
                    %>
                    <div class="rev_prof <%=userNameColorClassName%>">${latestReviews.userNameInitial}</div>
                  </c:if>
                </c:if>
                <div class="rlst_rht">
                  <div class="rev_name">${latestReviews.userName}</div>
                  <div class="rev_dte">${latestReviews.reviewedDate}</div>
                </div>
              </div>
            </div>
            <div class="revlst_rht">
              <div class="rlst_wrap">
                <h2>
                <c:if test="${latestReviews.courseDeletedFlag ne 'TRUE'}">
                <a href="${latestReviews.courseDetailsPageURL}" title="${latestReviews.courseTitle}">
                ${latestReviews.courseTitle}
                </a>
                </c:if>
                 <c:if test="${latestReviews.courseDeletedFlag eq 'TRUE'}">
                <span class="del_crse">${latestReviews.courseTitle}</span>
                </c:if>
                </h2>
                <div class="reviw_rating">
                  <div class="rate_new">
                    <div class="stud_review">
                      <div class="ovlr_cnt">
                        <div class="rat_wrp">
                          <c:set var="roundRatingValue" value="${latestReviews.overAllRating}"/>
                          <%
                          String roundRatingValue = pageContext.getAttribute("roundRatingValue").toString();
                          int roundVlaue = Integer.parseInt(roundRatingValue);
                          for(int i = 0; i<5;i++) {
                            if(i<roundVlaue){%>
                              <i class="fa fa-star fa-2" aria-hidden="true"></i>
                            <%} else { %>
                                  <i class="fa fa-star-o fa-2" aria-hidden="true"></i> 
                                <%}
                           }%>
                          </div>
                        </div>
                      </div>
                    <div class="rw_qus_des">${latestReviews.reviewText}...</div>
                  </div>
                  <div class="rev_mre"><a onclick="ajaxReviewLightBox(${latestReviews.reviewId})">Read more</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </c:forEach>
    </c:if>
  </c:if>
  <c:if test="${not empty requestScope.latestSubjectAjaxReview}">
    <c:if test="${requestScope.latestSubjectAjaxReview}">
      <c:forEach var="latestSubjectReviews" items="${requestScope.latestSubjectAjaxReview}" varStatus="cnt">
        <div class="rev_box">
          <div class="rlst_row">
            <div class="revlst_lft">
              <div class="rlst_wrp">
                <c:if test="${not empty latestSubjectReviews.userNameInitial}">
                    <c:set var="userInitialName" value="${latestSubjectReviews.userNameInitial}"/> 
                    <%
                      String userInitialName = pageContext.getAttribute("userInitialName").toString();
                      userNameColorClassName = commonUtil.getReviewUserNameColorCode(userInitialName);
                    %>
                    <div class="rev_prof <%=userNameColorClassName%>">${latestSubjectReviews.userNameInitial}</div>
                </c:if>
                <div class="rlst_rht">
                  <div class="rev_name">${latestSubjectReviews.reviewerName}</div>
                  <div class="rev_dte">${latestSubjectReviews.reviewDate}</div>
                </div>
              </div>
            </div>
            <div class="revlst_rht">
              <div class="rlst_wrap">
                <h2>
                <c:if test="${latestSubjectReviews.courseDeletedFlag ne 'TRUE'}">
                  <a href="${latestSubjectReviews.courseDetailsReviewURL}" title="${latestSubjectReviews.courseName}">
                   ${latestSubjectReviews.courseName}
                  </a>
                </c:if>
                <c:if test="${latestSubjectReviews.courseDeletedFlag eq 'TRUE'}">
                  <span class="del_crse">${latestSubjectReviews.courseName}</span>
                </c:if>
               </h2>
                <div class="reviw_rating">
                  <div class="rate_new">
                    <div class="stud_review">
                      <div class="ovlr_cnt">
                        <div class="rat_wrp">
                          <c:set var="ratingValue" value="${latestSubjectReviews.reviewRatings}"/>
                          <%
                          String ratingValue = pageContext.getAttribute("ratingValue").toString();
                          int ratingValues = Integer.parseInt(ratingValue);
                          for(int i = 0; i<5;i++) {
                            if(i<ratingValues){%>
                              <i class="fa fa-star fa-2" aria-hidden="true"></i>
                            <%} else { %>
                                  <i class="fa fa-star-o fa-2" aria-hidden="true"></i> 
                               <%}
                          }%>
                        </div>
                      </div>
                    </div>
                    <div class="rw_qus_des">${latestSubjectReviews.reviewDesc}...</div>
                  </div>
                  <div class="rev_mre"><a onclick="ajaxReviewLightBox(${latestSubjectReviews.reviewId})">Read more</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </c:forEach>
    </c:if>
  </c:if>
</div>