<%@page import="WUI.utilities.CommonUtil" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%
  String emailDomainJsName = CommonUtil.getResourceMessage("wuni.email.domain.js", null);
  String facebookLoginJSName = CommonUtil.getResourceMessage("wuni.facebook.login.js", null);
  String clearingProfileJsName = CommonUtil.getResourceMessage("wuni.cleaing.profile.js", null);
  //String jqueryJsName = CommonUtil.getResourceMessage("wuni.jquery.1.8.js", null);
  String waypointJsName = CommonUtil.getResourceMessage("wuni.waypoint.js", null);
  String flexsliderJsName = CommonUtil.getResourceMessage("wuni.jquery.flexslider.js", null);
  String sliderTabsJsName = CommonUtil.getResourceMessage("wuni.slider.tabs.js", null);
  String jqueryEasingJsName = CommonUtil.getResourceMessage("wuni.jquery.easing.js", null);  
 %>
<%-- <script src="<%=CommonUtil.getJsPath()%>/js/content_hub/<%=jqueryJsName%>"></script> --%>
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<script src="<%=CommonUtil.getJsPath()%>/js/jquery/<%=waypointJsName%>"></script>
<script src="<%=CommonUtil.getJsPath()%>/js/content_hub/<%=flexsliderJsName%>"></script>
<script src="<%=CommonUtil.getJsPath()%>/js/content_hub/<%=sliderTabsJsName%>"></script>
<script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/content_hub/<%=jqueryEasingJsName%>"></script>
<%-- <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/DonutChart/jquery.drawDoughnutChart_1_wu541.js"></script> --%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/emaildomain/<%=emailDomainJsName%>"> </script>
<jsp:include page="/jsp/common/includeJS.jsp" />
<script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/clearing/profile/<%=clearingProfileJsName%>"></script>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=facebookLoginJSName%>"> </script> 
