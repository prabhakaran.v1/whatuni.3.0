<%@page import="WUI.utilities.CommonUtil" %>
<%
String wuniHeaderStylesCss = CommonUtil.getResourceMessage("wuni.header.styles.css", null);
String wuniMainDomesticCss = CommonUtil.getResourceMessage("wuni.main.domestic.css", null);
String searchCssName = CommonUtil.getResourceMessage("wuni.clearing.searchpage.css", null);
%>
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
<link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuniHeaderStylesCss%>" media="screen">
<link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=wuniMainDomesticCss%>" media="screen">
<link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/<%=searchCssName%>" media="screen" />
<link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/ch_flexslider.css" />
<link rel="stylesheet" href="<%=CommonUtil.getCSSPath()%>/cssstyles/jquery.sliderTabs.min.css" />
<jsp:include page="/jsp/common/abTesting.jsp"/>
<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
	<jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>
<%-- Facebook pixel tracking script included in head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/facebookPixelTracking.jsp"/>
