<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="WUI.utilities.CommonUtil" %>
   <div class="rvbx_shdw"></div>
   <div class="revcls">
      <a href="javascript:void(0);" onclick="reviewLightBox()" class="close">
        <img src='<%=CommonUtil.getImgPath("/wu-cont/images/clrsf_wht_clse_icon.svg",0)%>' alt="close_icon">
      </a>
   </div>
   <div class="rvbx_cnt">
     <div class="clr_sf">
		<h3 class="clr_sf_hd"><spring:message code="clearing.ended.label"/></h3>
		<p class="clr_sf_txt"><spring:message code="clearing.ended.inyear.text"/></p>
		<a class="clr_sf_btn" onclick="javascript:switchProfileURL('UG');GANewAnalyticsEventsLogging('Clearing Lightbox','In Year','Clicked')"><spring:message code="view.intake.button"/></a>
	</div>
   </div>