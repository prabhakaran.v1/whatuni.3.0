<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.SessionData, WUI.utilities.GlobalConstants, WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction"%>

  <%String pageno="1"; %>
  <meta name="format-detection" content="telephone=no"/>
  <c:if test="${requestScope.pageno ne ''}">
  <%pageno=String.valueOf(request.getAttribute("pageno")); %>
  </c:if>
   <%String newsearchJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.newSearchResults.js");%>
  <% 
 
  String paramCollegeId = (request.getAttribute("collegeId") !=null && !request.getAttribute("collegeId").equals("null") && String.valueOf(request.getAttribute("collegeId")).trim().length()>0 ? String.valueOf(request.getAttribute("collegeId")) : ""); 
  String paramCategoryCode = (request.getAttribute("paramCategoryCode") !=null && !request.getAttribute("paramCategoryCode").equals("null") && String.valueOf(request.getAttribute("paramCategoryCode")).trim().length()>0 ? new CommonUtil().toUpperCase(String.valueOf(request.getAttribute("paramCategoryCode"))) : "");  
  String paramStudyLevelId = (request.getAttribute("paramStudyLevelId") !=null && !request.getAttribute("paramStudyLevelId").equals("null") && String.valueOf(request.getAttribute("paramStudyLevelId")).trim().length()>0 ? new CommonUtil().toUpperCase(String.valueOf(request.getAttribute("paramStudyLevelId"))) : ""); 
  //
  String first_mod_group_id = (String)request.getAttribute("first_mod_group_id");
  String noindexfollow = request.getAttribute("meta_robot") !=null ? String.valueOf(request.getAttribute("meta_robot"))  : "noindex,follow";
  
  String keyword = (request.getAttribute("searchText") !=null && !request.getAttribute("searchText").equals("null") && String.valueOf(request.getAttribute("searchText")).trim().length()>0 ? String.valueOf(request.getAttribute("searchText")) : "");  
  String keywordPhrase = request.getAttribute("searchText") != null ? request.getAttribute("searchText").toString() : "";
  // wu582_20181023 - Sabapathi: Added studymodeId for stats logging
  String studymodeId = request.getAttribute("studymodeId") != null ? request.getAttribute("studymodeId").toString() : "";
  String pros_search_name = request.getAttribute("prospectus_search_name") != null ? request.getAttribute("prospectus_search_name").toString() : "";
  String location = (request.getAttribute("location") !=null && !request.getAttribute("location").equals("null") && String.valueOf(request.getAttribute("location")).trim().length()>0 ? String.valueOf(request.getAttribute("location")) : "");  
  String paramFlag = paramStudyLevelId != null &&  paramStudyLevelId.equalsIgnoreCase("") ? "ALL_COURSE_RESULTS" : "COURSE_RESULTS";
  String pageNameValue = paramStudyLevelId != null &&  paramStudyLevelId.equalsIgnoreCase("") ? "ALL COURSE RESULTS" : "COURSE RESULTS";  
  String courseMappingPath = request.getAttribute("courseMappingPath") !=null ? "rsearch.html" : "csearch.html";
  String searchPosition = new WUI.utilities.CookieManager().getCookieValue(request, "sresult_provider_position");
  String studyLevelDesc = (String)request.getAttribute("studyLevelDesc");
  // added for 02nd Feb 2009 Release to show the canonicalURL for the SEO requirement 
  String canonicalURL = (String) request.getAttribute("searchUrl");
  canonicalURL  = canonicalURL !=null && canonicalURL.trim().length()>0 ? GlobalConstants.WHATUNI_SCHEME_NAME+GlobalConstants.WHATUNI_DOMAIN+canonicalURL : "";

  // end of thecoce added  
  String paramCollegeName = (String) request.getAttribute("collegeName");
  paramCollegeName = paramCollegeName != null && !paramCollegeName.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(paramCollegeName).trim() : "";          
  
  String seoCollegeName =  paramCollegeName !=null && paramCollegeName.trim().length()>0 ? new CommonFunction().replaceHypen(new CommonFunction().replaceURL(paramCollegeName)).replaceAll(" ","-")+"-" : "";
  String seoCollegeNameLower  = new CommonUtil().toLowerCase(seoCollegeName);
  
  String collegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("collegeNameDisplay") : "";
  //
  String collegeLocation = (String) request.getAttribute("collegeLocation");
  collegeLocation = collegeLocation != null && !collegeLocation.equalsIgnoreCase("null") ? new CommonUtil().toTitleCase(collegeLocation).trim() : "";       
  //
  Object SEARCH_TYPE = request.getAttribute("SEARCH_TYPE");
  //    
  CommonFunction common = new CommonFunction();
  String courseUrl = "/all-courses/csearch?university=" + common.replaceHypen(common.replaceURL(paramCollegeName)).toLowerCase(); //Changed new all courses url pettern, By Thiyagu G for 27_Jan_2016.
  String newUniViewJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.uniview.js");//3_Jun_2014
  int searchhits = 0;
  if(!GenericValidator.isBlankOrNull((String)(request.getAttribute("courseCount"))) && new CommonUtil().isNumber((String)(request.getAttribute("courseCount")))){	
    searchhits = Integer.parseInt(String.valueOf(request.getAttribute("courseCount"))); 
  }
  String nextYear = GlobalConstants.NEXT_YEAR;
  String courseHeader = (String)request.getAttribute("courseHeader") != null ? request.getAttribute("courseHeader").toString() : "";
         courseHeader = courseHeader.replaceAll("Clearing", "");  
  String uniHomeUrl = "";
  String qString = (String)request.getAttribute("queryStr");
         qString = !GenericValidator.isBlankOrNull(qString)? ("?"+qString):"";
  String clearingPage =  (String)request.getAttribute("searchClearing");
       clearingPage = GenericValidator.isBlankOrNull(clearingPage)?"":clearingPage;
  String isAdvertiser = request.getAttribute("isAdvertiser") != null && request.getAttribute("isAdvertiser").toString().trim().length() > 0 ? (String) request.getAttribute("isAdvertiser") : "";
  String advNonAdv = "";
  if("TRUE".equals(isAdvertiser)){
    advNonAdv = "Advertiser";
  }else{
    advNonAdv = "Non-Advertiser";
  }
  String evntProviderName = (paramCollegeName != null && paramCollegeName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(common.replaceSpecialCharacter(paramCollegeName))) : "").toLowerCase();
  //flag for to show canonical url in june_5_18 by Sangeeth.S
    String canonicalUrlFlag = request.getAttribute("CANONICAL_URL") != null ? request.getAttribute("CANONICAL_URL").toString() : "";
    %>
    
<body>
  <!--Modified code for responsive redesign 03_Nov_2015 By S.Indumathi-->
  <div class="sr_resp clr_sr">
    <header class="clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>
    </header>  
    
    <div class="ad_cnr">
      <div id="content-blk">
        <%
        request.setAttribute("uniLanding", "YES");
        String urldata_1=""; 
        String urldata_2="";
        String urldata_3="";
        String cid="0";
        %>
         <c:if test="${not empty requestScope.collegeId}">
         <%cid = String.valueOf(request.getAttribute("collegeId")); %>
       </c:if>
        <%            
        urldata_1 = (String)request.getAttribute("URL_1");   
        urldata_2 = (String)request.getAttribute("URL_2");              
        String urlString = urldata_1+pageno+urldata_2;
        String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "";        
        session.setAttribute("prospectus_redirect_url", urlString);
        %> 
        <div class="sr Pro_Res">
          <jsp:include page="/seopods/breadCrumbs.jsp" >
            <jsp:param name="pageName" value="CLEARING_PROVIDER_RESULT_PAGE" />            
          </jsp:include>
          <jsp:include page="/jsp/search/include/prtopheaderinclude.jsp" >
            <jsp:param name="searchPageType" value = "clearing"/>
          </jsp:include>
          <div class="fl_lr">
            <a href="#">
              <span class="left"><i class="fa fa-filter"></i></span>
              <span class="right">Filter results</span>
            </a>
          </div>
          <div class="sr-cont">
            <jsp:include page="/jsp/search/include/providerFilters.jsp">                
              <jsp:param name="srchType" value="clearing"/>
            </jsp:include>
            <div class="srs-rt">
              <jsp:include page="/jsp/search/searchredesign/providerResultsSort.jsp">
                <jsp:param name="srchType" value="clearing"/>
              </jsp:include>
              <div class="lst ">  
                <!--Added searchhits param for showing banners Indumathi.S Nov-03-15 Rel-->
                <jsp:include page="/jsp/search/include/srresultsinclude.jsp">                    
                  <jsp:param name="searchPageType" value="clearing"/>
                  <jsp:param name="searchhits" value="<%=searchhits%>"/>
                </jsp:include>
              </div>                  
              <div class="pr_pagn">
                <jsp:include page="/jsp/search/searchredesign/newPagination.jsp">
                  <jsp:param name="pageno" value="<%=pageno%>"/>
                  <jsp:param name="pagelimit"  value="10"/>
                  <jsp:param name="searchhits" value="<%=searchhits%>"/>
                  <jsp:param name="recorddisplay" value="10"/>
                  <jsp:param name="displayurl_1" value="<%=urldata_1%>"/>
                  <jsp:param name="displayurl_2" value="<%=urldata_2%>"/>
                  <jsp:param name="firstPageSeoUrl" value="<%=firstPageSeoUrl%>"/>
                  <jsp:param name="universityCount" value="1"/>
                  <jsp:param name="resultExists" value="Y"/>
                  <jsp:param name="action" value=""/>    
                </jsp:include>
              </div>
              <c:if test="${not empty requestScope.listOfUniStats}"> 
                <div class="pr_key">
                  <div class="rich_profile">
                    <jsp:include page="/jsp/search/includeCdPage/keyStatsInfo.jsp">
                      <jsp:param name="keyStatsViewMore" value="Y" />
                    </jsp:include>
                  </div>
                </div>
              </c:if>   
              <c:if test="${not empty requestScope.listOfUserReviews}"> 
                <div class="pr_reviews">
                  <jsp:include page="/jsp/search/searchredesign/providerReviewsPod.jsp">
                    <jsp:param name="searchPageType" value = "clearing"/>
                  </jsp:include> 
                </div>                  
              </c:if>
            </div>  
          </div>
        </div>              
      </div>      
    </div>
    
    <input type="hidden" id="currentcompare" value=""/>
    <input type="hidden" name="comparepage" id="comparepage" value="CLEARING"/> 
    <input type="hidden" id="srcEvent"/>
    <input type="hidden" id="searchtype" name="searchType" value="CLEARING_SEARCH"/>
    <input type="hidden" name="isclearingPage" id="isclearingPage" value="<%=clearingPage%>"/>
    <%-- wu582_20181023 - Sabapathi: added studymodeId for stats logging  --%>
    <input type="hidden" id="studymodeId" value="<%=studymodeId%>"/>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
  </div>
  <input type="hidden" name="hdrMenuHide" id="hdrMenuHide" value="READREVIEWS" />
  <script type="text/javascript">
  loadFirstDeliveryProvider('<%=first_mod_group_id%>');
  ProviderdefaultOpenFilter('<%=paramStudyLevelId%>');
  </script>
  <script type="text/javascript"> 
     GAInteractionEventTracking('', 'Provider results', '<%=advNonAdv%>', '<%=evntProviderName%>', '0'); 
  </script>  
  </body>                      