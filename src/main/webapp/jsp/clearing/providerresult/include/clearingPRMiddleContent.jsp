<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<c:if test="${not empty providerResultsList}">
   <c:forEach var="providerResultsList" items="${providerResultsList}" varStatus="i">
      <c:set var="index" value="${i.index}"/>
      <div class="wuclrpr_colrow">
         <div class="wuclrsr_uniinf">
            <div class="wuclrsr_crslstrow">
               <h4><a href="${domainPath}${providerResultsList.courseDetailPageUrl}" onclick="sponsoredListGALogging('${providerResultsList.collegeId}');gtmproductClick(${index}, 'pr_to_cd');" id="pr_to_cd_${index}">${providerResultsList.courseTitle}</a></h4>
               <div class="wuclrsr_crslstmta">
                  <c:if test="${not empty providerResultsList.scoreLabel}">
                     <c:set var="indicatorClass" value="${providerResultsList.scoreLevel eq 'LIKELY' ? 'txtgrncol' : providerResultsList.scoreLevel eq 'STRETCH' ? 'txtorngcol' : 'txtgrncol' }"></c:set>
                     <div class="wuclrsr_crsmtrtng">
                        <div class="wuclrsr_crsmtrtnglft ${indicatorClass }">
                           <span class="wuclrsr_crsmtrtngtxt">${providerResultsList.scoreLabel}</span>
                           <span class="wuclrsr_crsmtrtngbrdrset">
                           <span class="wuclrsr_crsmtrtngbrd ${providerResultsList.scoreLevel ne 'LIKELY' ? 'brdrgry' : ''}"></span>
                           <span class="wuclrsr_crsmtrtngbrd ${providerResultsList.scoreLevel eq 'STRETCH' ? 'brdrgry' : ''}"></span>
                           <span class="wuclrsr_crsmtrtngbrd"></span>
                           <span class="wuclrsr_crsmtrtngbrd"></span>
                           <span class="wuclrsr_crsmtrtngbrd"></span>
                           </span>
                        </div>
                        <div class="wuclrsr_crsmtrtngrtg">
                           <span class="tooltip_cnr ttip_cnt">
                              <span class="tool_tip fl">
                                 <img class="wuclrsr_imgicn" src="${domainPathImg }clr20_icn_info_circle.svg" alt="Info Icon">
                                 <span class="cmp">
                                    <div class="hdf5"></div>
                                    <div> ${providerResultsList.scoreLabelTooltip}</div>
                                    <div class="line"></div>
                                 </span>
                              </span>
                           </span>
                        </div>
                     </div>
                  </c:if>
                  <div class="wuclrsr_crsmtboxrow">
                     <ul class="wuclrsr_ucsinfcol">
                        <c:if test="${not empty providerResultsList.entryReqPoints}">
                           <li class="wuclrsr_ucspnt">
                              <span>UCAS points</span>
                              <span>${providerResultsList.entryReqPoints}</span>
                           </li>
                        </c:if>
                        <c:if test="${not empty providerResultsList.ucasCode}">
                           <li class="wuclrsr_ucscde">
                              <span>UCAS code</span>
                              <span>${providerResultsList.ucasCode}</span>
                           </li>
                        </c:if>
                        <li class="wuclrsr_nxtsdte">
                           <c:if test="${not empty providerResultsList.ucasCode}">
                              <span class="wuclrsr_nxtsdtelft">
                              <span>Start date</span>
                              <span>${providerResultsList.startDate}</span>
                              </span>
                           </c:if>
                           <c:if test="${not empty providerResultsList.tagLine}">
                              <span class="wuclrsr_nxtsdtergt">
                              <span>${providerResultsList.tagLine}</span>
                              </span>  
                           </c:if>
                        </li>
                     </ul>
                     <ul class="wuclrsr_crsboxbtnset ${empty hotLine ? 'vwonlybtn' : ''}">
                        <c:if test="${not empty website}">
                           <li class="wuclrsr_vstbtn">
                              <a href="${website}&courseid=${providerResultsList.courseId}" target="_blank" onclick="sponsoredListGALogging('${providerResultsList.collegeId}');matchTypeGALogging('${providerResultsList.courseId}');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', '');cpeWebClickClearingWithCourse(this,'${providerResultsList.collegeId}', '${providerResultsList.courseId}', '${providerResultsList.suborderItemId}','${networkId}','${website}','');">
                                 <spring:message code="visit.website.label" />
                              </a>
                           </li>
                        </c:if>
                        <c:if test="${not empty hotLine}">
                           <li class="wuclrsr_calbtn">
                              <a id="contact_${index}" onclick="sponsoredListGALogging('${providerResultsList.collegeId}');matchTypeGALogging('${providerResultsList.courseId}');GAInteractionEventTracking('Webclick', 'interaction', 'hotline','${gaCollegeName}');cpeHotlineClearing(this,'${providerResultsList.collegeId}','${providerResultsList.suborderItemId}','${networkId}','${hotLine}','');changeContact('${hotLine}','contact_${index}');">
                                 <i class="fa fa-phone" aria-hidden="true"></i>
                                 <spring:message code="call.now.label" />
                              </a>
                           </li>
                        </c:if>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </c:forEach>
   <input type="hidden" id="matchTypeDim" value="${matchTypeDim}" />
   <input type="hidden" id="courseMatchTypeMap" value="${courseMatchTypeMap}" />
   <input type="hidden" id="uniMatchTypeMap" value="${uniMatchTypeMap}" />
   <div class="wuclrsr_pgnrow">
      <div class="pr_pagn">
         <jsp:include page="${paginationPageName eq 'newPagination' ? '/jsp/search/searchredesign/newPagination.jsp' : '/jsp/search/searchredesign/searchInitialPagination.jsp'}">
            <jsp:param name="pageno" value="${pageNo}"/>
            <jsp:param name="pagelimit"  value="10"/>
            <jsp:param name="searchhits" value="${recordCount}"/>
            <jsp:param name="recorddisplay" value="10"/>
            <jsp:param name="displayurl_1" value="${URL_1}"/>
            <jsp:param name="displayurl_2" value="${URL_2}"/>
            <jsp:param name="firstPageSeoUrl" value="${firstPageSeoUrl}"/>
            <jsp:param name="universityCount" value="${recordCount}"/>
            <jsp:param name="resultExists" value=""/>
         </jsp:include>
      </div>
   </div>
</c:if>