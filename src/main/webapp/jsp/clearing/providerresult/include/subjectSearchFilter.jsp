<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="WUI.utilities.CommonUtil"%>

			<div class="fltr_hd stc_hd"><h2>Subject</h2></div>

			<div class="fltr_wrp">
				<h3>Choose a subject</h3>
				<div class="fltr_box">
					<div class="fltr_levls">
						<div class="flrt_src">
							<div class="uni_inpt">
								<input type="text" id="subjectAjax" placeholder="Subject name" autocomplete="off" name="subject">
								<a href="javascript:void(0);" id="sub_srch_icon"> <span class="srch"></span></a>
							</div>							
							<div id="ajax_listOfOptions_subject">
							<c:if test="${not empty subjectAjaxList}">
									<ul id="ajaxSubjectDropDown" class="cstm_drp ajax_ul" style="overflow: hidden; width: auto; display: none;">
										<c:forEach var="subjectAjaxList" items="${subjectAjaxList}" varStatus="sub">
											<c:set var="index" value="${sub.index}" />
											<li id="subjectDropDown_${index}" data-filter-show="subject" data-subject-ga-value="${fn:toLowerCase(subjectAjaxList.subject)}" 
												data-filter-type="A" data-subject-id="${index}"
												data-qual-value="${subjectAjaxList.categoryCode}"
												data-subject-value="${subjectAjaxList.subjectTextKey}">
												<a href="javascript:void(0);"> ${subjectAjaxList.subject} </a>
											</li>
										</c:forEach>
									</ul>
									<!-- <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 5px; height: 106.667px;"></div>
	                            	<div class="slimScrollRail" onclick="clickScroll(this,event);" style="width: 7px; height: 100%; position: absolute; top: 0px; display: block; border-radius: 5px; background: rgb(226, 226, 226); z-index: 90; right: 6px;"></div> -->


							</c:if>
						</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="fltr_wrp">
				<div class="fltr_box">
					<div class="fltr_levls">
						<div class="grd_chips" id="subjectDiv">
						<c:forEach var="subjectAjaxList" items="${subjectAjaxList}" varStatus="sub">
							<c:set var="index" value="${sub.index}" />
							<a href="javascript:void(0);" class="disabled" id="subject_${index}"
								data-filter-show="subject" data-filter-type="B" data-subject-id="${index}"
								data-subject-value="${subjectAjaxList.subjectTextKey}"
								style="display: none;"> ${subjectAjaxList.subject} </a>
						</c:forEach>
					</div>
					</div>
				</div>
			</div>					

