<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="WUI.utilities.CommonUtil" %>
<div class="wuclrsr_rslthead">
   <div class="ad_cnr">
      <div class="content-bg" id="content-blk">
         <div class="sr">
            <jsp:include page="/seopods/breadCrumbs.jsp">
               <jsp:param name="pageName" value="CLEARING_PROVIDER_RESULTS_PAGE"/>
            </jsp:include>
            <div class="com wuclrpr_hdinfrow">
               <c:if test="${not empty listOfcollegeDetails}">
                  <c:forEach var="collegeDetails" items="${listOfcollegeDetails}">
                     <div class="com-lt">
                        <div>
                           <h4 class="wuclr_ttlbl">
                              <spring:message code="clearing.text.label" />
                           </h4>
                           <h1 class="result-head reshd2"><c:if test="${not empty searchSubject}">${searchSubject} degrees at </c:if> <c:if test="${not empty searchPhrase}">${searchPhrase} degrees at </c:if><a href="${domainPath}${collegeDetails.uniHomeUrl}" onclick="sponsoredListGALogging('${collegeDetails.collegeId}');">${collegeDetails.collegeDisplayName}</a></h1>
                           <h2 class="result-head respar2">                              
                              <c:choose>
                           <c:when test="${(not empty searchSubject or not empty searchPhrase) and not empty collegeDetails.collegeDisplayName}">
                           <c:if test="${not empty courseCount}">There are ${courseCount} ${searchPhrase}${searchSubject} degree ${courseCount eq 1 ? 'course' : courseCount gt 1 ? 'courses' : ''} in clearing at ${collegeDetails.collegeDisplayName}.</c:if> For more information about their clearing course intakes you can visit their <a href="${domainPath}${collegeDetails.uniHomeUrl}">${collegeDetails.collegeDisplayName} clearing profile.</a>
                           </c:when> 
                           <c:when test="${not empty collegeDetails.collegeDisplayName}">
                             <c:if test="${not empty courseCount}">There are ${courseCount} clearing degree ${courseCount eq 1 ? 'course' : courseCount gt 1 ? 'courses' : ''} available at ${collegeDetails.collegeDisplayName}. Find which clearing course is the best for you.</c:if>
                           </c:when>                           
                           </c:choose>
                           </h2>
                        </div>
                        <div class="wuclrsr_unirvwdet">
                           <ul class="strat cf">
                              <li>
                        <c:if test="${not empty collegeDetails.actualRating and collegeDetails.actualRating ge 1}">
                                 <span class="rat${collegeDetails.actualRating} t_tip">
                                    <span class="cmp">
                                       <div class="hdf5">
                                          <spring:message code="wuni.tooltip.overall.keystats" />
                                       </div>
                                       <div class="line"></div>
                                    </span>
                                 </span>
                                 <span class="rtx">(${collegeDetails.overAllRating})</span>
                                    <c:if test="${not empty collegeDetails.reviewCount and collegeDetails.reviewCount ge 1}">
                                 <a class="link_blue" href="${collegeDetails.uniReviewUrl}">
                                       ${collegeDetails.reviewCount} 
                                       ${(collegeDetails.reviewCount gt 1) ? 'reviews' : (collegeDetails.reviewCount eq 1) ? 'review' : ''}
                                 </a>
                                    </c:if>
                        </c:if>
                              </li>
                           </ul>
                        </div>
                     </div>
                     <div class="com-rt">
                        <c:if test="${not empty collegeDetails.collegeLogo}">
                           <div class="wuclrpr_inslogo">
                              <img src="<%=CommonUtil.getImgPath("",0)%>${collegeDetails.collegeLogo}" width="77" title="${collegeDetails.collegeDisplayName}" class="ibdr" alt="${collegeDetails.collegeDisplayName}">         
                           </div>
                        </c:if>
                     </div>
                     <div class="com wuclrpr_hdbtnset">
                        <div class="wuclrsr_unirvwdet">
                           <ul class="strat cf">
                              <c:if test="${(not empty collegeDetails.accommodationAvailable) and ('Y' eq collegeDetails.accommodationAvailable)}">
                                 <li>
                                    <i class="fa fa-check" aria-hidden="true"></i> 
                                    <spring:message code="accommodation.available.label" />
                                 </li>
                              </c:if>
                              <c:if test="${(not empty collegeDetails.scholarshipAvailable) and ('Y' eq collegeDetails.scholarshipAvailable)}">
                                 <li>
                                    <i class="fa fa-check" aria-hidden="true"></i> 
                                    <spring:message code="scholarships.available.label" />
                                 </li>
                              </c:if>
                           </ul>
                           <ul class="wuclrsr_crsboxbtnset ${empty collegeDetails.hotLineNo ? 'vwonlybtn' : ''}">
                              <c:if test="${not empty collegeDetails.website}">
                                 <li class="wuclrsr_vstbtn">
                                    <a href="${collegeDetails.website}" onclick="sponsoredListGALogging('${collegeDetails.collegeId}');matchTypeGALogging('${collegeDetails.collegeId}');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', '');cpeWebClickClearingWithCourse(this,'${collegeDetails.collegeId}', '', '${collegeDetails.suborderItemId}','${collegeDetails.networkId}','${collegeDetails.website}','${collegeDetails.orderItemId }');" target="_blank">
                                       <spring:message code="visit.website.label" />
                                    </a>
                                 </li>
                              </c:if>
                              <c:if test="${not empty collegeDetails.hotLineNo}">
                                 <li class="wuclrsr_calbtn">
                                    <a id="contact_" onclick="sponsoredListGALogging('${collegeDetails.collegeId}');matchTypeGALogging('${collegeDetails.collegeId}');GAInteractionEventTracking('Webclick', 'interaction', 'hotline','${gaCollegeName}');changeContact('${collegeDetails.hotLineNo}','contact_');cpeHotlineClearing(this,'${collegeDetails.collegeId}','${collegeDetails.suborderItemId}','${collegeDetails.networkId}','${collegeDetails.hotLineNo}','${collegeDetails.orderItemId }');">
                                       <i class="fa fa-phone" aria-hidden="true"></i>
                                       <spring:message code="call.now.label" />
                                    </a>
                                 </li>
                              </c:if>
                           </ul>
                        </div>
                     </div>
                  </c:forEach>
               </c:if>
            </div>
            <div id="articleTab" class="sr-art" style="display:none;">
               <div class="sart-lt">
               </div>
               <div class="sart-rt">
               </div>
            </div>
            <%--clearing search results filters pod --%>
            <jsp:include page="/jsp/clearing/providerresult/include/filters.jsp" />
            <%--End of clearing search result filters section --%>
         </div>
      </div>
   </div>
</div>