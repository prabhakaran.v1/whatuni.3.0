<%@page import="WUI.utilities.CommonUtil, java.util.ArrayList, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, WUI.utilities.SessionData, java.util.*, WUI.utilities.GlobalConstants" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>
<body>
   <c:if test="${not empty pixelTrackingCode}">${pixelTrackingCode}</c:if>
   <%--Start of filter pop up --%>
   <div class="clr_fltr" id="clr_fltr" style="display: none;">
      <div class="overlay"></div>
      <jsp:include page="/jsp/clearing/include/filterPopup.jsp" />
   </div>
   <%--End of clearing filter pop up --%>
   <div class="sr_resp">
      <!-- Header starts here -->
      <header class="md clipart">
         <div class="large"></div>
         <div class="ad_cnr">
            <div class="content-bg">
               <div id="desktop_hdr">
                  <jsp:include page="/jsp/common/wuHeader.jsp" />
               </div>
            </div>
         </div>
      </header>
      <!-- Header ends here -->
      <!-- Center starts here -->
      <!-- Whatuni clearing pr page head starts -->
      <jsp:include page="/jsp/clearing/providerresult/include/headerSection.jsp" />
      <!-- Whatuni clearing pr page head ends -->       
      <!-- Whatuni clearing pr page center starts --> 
      <div class="ad_cnr">
         <div class="content-bg" id="content-blk">
            <div class="sr">
               <c:if test="${COVID19_SYSVAR eq 'ON'}">
                  <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
                  <div class="covid_upt">
                     <span class="covid_txt"><%=new SessionData().getData(request, "COVID19_COURSE_TEXT")%></span>
                     <a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_blu_arw.svg", 0)%>"></a>
                  </div>
               </c:if>
               <div class="wuclrsr_srtby fl_w100">
                  <div class="srt">
                     <ul class="cf">
                        <li class="sturat nw">
                           <a class="srt1">Sort by <img class="wuclrsr_imgicn" src="${domainPathImg}clr20_icn_dropdown_grey.svg" alt="Plus Icon"> </a>
                           <ul id="sortBy" class="strt">
                              <c:if test="${not empty subjectText}">
                                 <li class="${fn:toUpperCase(sortBy) eq 'R'? 'active' : ''}"><a rel="nofollow" data-sort-show="mostInfo" data-mostInfo-value="r" onclick="searchEventTracking('pr-sort','mostinfo','clicked');">Most info</a></li>
                              </c:if>
                              <li class="${fn:toUpperCase(sortBy) eq 'ENTD' ? 'active' : ''}"><a rel="nofollow" data-sort-show="UCASD" data-UCASD-value="entd" onclick="searchEventTracking('pr-sort','entry-requirements-high-to-low','clicked');">Entry requirements (high to low)</a></li>
                              <li class="${fn:toUpperCase(sortBy) eq 'ENTA' ? 'active' : ''}"><a rel="nofollow" data-sort-show="UCASA" data-UCASA-value="enta" onclick="searchEventTracking('pr-sort','entry-requirements-low-to-high','clicked');">Entry requirements (low to high)</a></li>
                              <li class="${fn:toUpperCase(sortBy) eq 'TA' ? 'active' : ''}"><a rel="nofollow" data-sort-show="ascending" data-ascending-value="ta" onclick="searchEventTracking('pr-sort','a-z-ascending','clicked');">A-Z ascending</a></li>
                              <li class="${fn:toUpperCase(sortBy) eq 'TD' ? 'active' : ''}"><a rel="nofollow" data-sort-show="descending" data-descending-value="td" onclick="searchEventTracking('pr-sort','a-z-descending','clicked');">A-Z descending</a></li>
                           </ul>
                        </li>
                     </ul>
                  </div>
                  <input type="hidden" id="sortHidden" value="${sortBy}" />
               </div>
               <jsp:include page="/jsp/clearing/providerresult/include/clearingPRMiddleContent.jsp" />
            </div>
         </div>
      </div>
      <!-- Whatuni clearing pr page center ends --> 
      <!-- Center ends here -->
      <!-- Footer starts here -->
      <input type="hidden" id="check_mobile_hidden" value="${mobileFlag}">
      <input type="hidden" id="eCommPageName" name="eCommPageName" value="Provider Results page"/>
      <input type="hidden" id="courseTitles" name="courseTitles" value="${allCourseTitle}" />
      <input type="hidden" id="courseId" name="courseId" value="${allCourseId}" />
      <input type="hidden" id="subjectName" name="subjectName" value="${subjectName}"/>
      <input type="hidden" id="instName" name="instName" value="${instNames}"/>
      <input type="hidden" id="GTMLDCS" value="<%=request.getAttribute("GTMLDCS")%>" />
      <input type="hidden" id="subjectL1" value="${subjectL1}" />
      <input type="hidden" id="gtmJsonDataPR" name="gtmJsonDataPR" />
      <input type="hidden" id="pageNo" name="pageNo" value="${pageno}"/>
      <input type="hidden" id="userType" name="userType" value="${sessionScope.USER_TYPE}" />
      <jsp:include page="/jsp/common/wuFooter.jsp" />
      <jsp:include page="/jsp/common/eCommerceTracking.jsp" />
      <!-- Footer ends here -->
   </div>
</body>