<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil,  WUI.utilities.GlobalConstants"%>
<%String loadingImg = GlobalConstants.WU_CONT + "/images/hm_ldr.gif"; %>
<body>
  <div id="ebookLoadingImg" class="cmm_ldericn" style="display:none;"><img alt="loading-image" src="<%=CommonUtil.getImgPath(loadingImg, 0)%>"></div>
  <header class="md clipart">
    <div class="ad_cnr">
      <div class="content-bg">
        <div id="desktop_hdr">
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>                
      </div>      
    </div>
  </header>
  <section class="cmm_cnt eblp_cntr">
	  <%-- E book landing start --%>
	  <div class="eblp_hero">
      <img id="heroPodImg" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" 
                           data-src="<%=CommonUtil.getImgPath("/wu-cont/images/WUEBLP_hero_img2_new.jpg",0)%>"
                           data-src-tab="<%=CommonUtil.getImgPath("/wu-cont/images/WUEBLP_hero_img2_new_992.jpg",0)%>"
                           data-src-ipad="<%=CommonUtil.getImgPath("/wu-cont/images/WUEBLP_hero_img2_new_992.jpg",0)%>"
                           data-src-mobile="<%=CommonUtil.getImgPath("/wu-cont/images/WUEBLP_hero_img2_new_480.jpg",0)%>"
                           alt="E Book Landing hero image" />
	  </div>
    <div class="cmm_col_12">
     <c:if test="${not empty sessionScope.userInfoList}">
        <jsp:include page="/jsp/clearing/include/clearingEbookDownload.jsp">
          <jsp:param name="from_page" value="ebook_landing"/>
        </jsp:include>
      </c:if> 
	    <h2 class="eb_title fl_w100"> Download Your Free Copy of the Ultimate Clearing Preparation Guide </h2>
		  <p class="eb_disc fl_w100">Our Ultimate Clearing Preparation Guide - which you can download completely FREE today. It has everything you need to know about your options for study in 2020-21, from applying to university to navigating the Clearing process online. Here's what's included:</p>
		  <div class="eb_thumb">
			  <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" 
                               data-src="<%=CommonUtil.getImgPath("/wu-cont/images/WUEBLP_thumb_img2.jpg",0)%>" 
                               data-src-mobile="<%=CommonUtil.getImgPath("/wu-cont/images/WUEBLP_thumb_img2_350.jpg",0)%>" 
                               title="E Book thumbnail image" alt="E Book thumbnail image">
		  </div>
		  <ul class="eb_lst">
			<li>
			 <h3>Make Online Count</h3>
			  <p>With top tips and real-life advice from fellow students and universities on getting organised and staying focused during lockdown, from how to make the most of virtual tours to how to ensure you've chosen the right course, we have everything you need to get you ahead of your online decision-making.</p>
			</li>
			<li>
		      <h3>In This Together</h3>
			  <p>When it all gets a bit too much to handle, we've got helpful tips from fellow students and medical experts for managing your stress, staying connected, and keeping your physical and mental well-being in check during this stressful time.</p>
			</li>
			<li>
		      <h3>Clearing Covered</h3>
			  <p>What to do before and after Results Day, and complete with practical advice on best practice for contacting universities, our preparation guide removes the heavy-lifting so you have everything you need, all in one place.</p>
			</li>	  
		  </ul>
   </div>
   <%-- E book landing end --%>
   
   <%-- Timer Section --%>
   <c:if test="${empty sessionScope.userInfoList}">
     <div class="brd_crumb timer_sec">
       <div class="cmm_col_12">
         <div class="cmm_wrap">
           <div id="regForm" class="cmm_row">
             <%-- Uni List --%>
             <h1>Sign up to download your FREE guide now...</h1>
             <div id="clearingLandingRegDiv">
              <jsp:include page="/jsp/clearing/include/clearingEbookRegistration.jsp"/>
             </div>
           </div>
        </div>
     </div>
    </div>
   </c:if>
     
   <%-- Timer Sectio --%>
   
   <%-- About whatuni start --%>
   <c:if test="${not empty aboutWhatuniHtmlContents}">
     ${aboutWhatuniHtmlContents}
   </c:if>
   <%-- About whatuni end --%>
   
  </section>
  <input type="hidden" id="pageIdentifier" name="pageIdentifier" value="lead-capture" />
  <jsp:include page="/jsp/common/wuFooter.jsp">
    <jsp:param name="FROM_PAGE" value="LEAD_CAPTURE" />
  </jsp:include>
  <div class="ebdwn_sec">
	  <div class="content-bg">
	    <div class="ebdwn_lft fl" title="download your ebook"> 
	 	    Download Your FREE eBook
		  </div>
		  <div class="ebdwn_rht fr">
		    <a class="req_info" id="requestInfoId" href="javascript:scrollToRegDiv();GAInteractionEventTracking('downloadbutton', 'Lead Capture', 'Download Now', 'Clicked');">Download now</a>
	    </div>
	   </div>
  </div>
  <script type="text/javascript">
    jQuery(document).ready(function() {
      lazyLoadHeroImage();
    });
    //pageViewFacebook();
  </script>
</body>
