<%@page import="WUI.utilities.CommonFunction, WUI.utilities.CommonUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<% 
  CommonFunction common = new CommonFunction();
  String appStoreURL  =  common.getWUSysVarValue("WU_APP_STORE_URL");
  String androidAppFlag = common.getWUSysVarValue("ANDROID_APP_ON_OFF"); 
  String appStoreURLForAndroid = ("ON".equalsIgnoreCase(androidAppFlag)) ? common.getWUSysVarValue("ANDROID_APP_URL") : "";
%>
<body>
  <header class="md clipart">
    <div class="ad_cnr">
      <div class="content-bg">
        <div id="desktop_hdr">
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>                
      </div>      
    </div>
  </header>
  <section class="cmm_cnt ebksuc_cnt">
    <%--    <div class="cong_cnt"><h1>Congratulations!</h1></div>--%>
    <div class="cmm_col_12 chse_cnt cngs_cnt ebk_suc">
      <div class="cmm_wrap">
        <div id="regForm" class="cmm_row">
          <jsp:include page="/jsp/clearing/include/clearingEbookDownload.jsp"/>
         </div>
       </div>
       <%-- App section --%>
       <%-- App promo page start --%>
       <div class="get_app_suc">
         <h3>The Whatuni App: Connecting you to your university</h3>
         <p class="od_descr">Use our app to quickly find universities and courses that are perfect for you. Tailor your search according to your interests and qualifications and read thousands of student reviews to help you narrow down your choices. Plus, get helpful reminders so that you don't miss out on information that affects you.</p>
         <div class="app_btns">
          <a class="app_str and_btn" href="<%=appStoreURL%>" target="_blank" title="Whatuni app download store"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/wu_app_ios_btn.svg",0)%>" alt="Whatuni app download store"></a>
          <a class="app_str" href="<%=appStoreURLForAndroid%>" target="_blank" title="Whatuni app download store"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/wu_app_android_btn.svg",0)%>" alt="Whatuni app download store"></a>
         </div>
       </div>
       <%-- App promo page end --%>
       <%-- Article Section --%>
       <c:if test="${not empty requestScope.CLEARING_LANDING_ARICLE_LIST}">
         <div class="artc_cnt">
           <div class="artc_row">
             <div class="artfc cm_art_pd">
               <h2>Want More Uni Advice? Check these out... </h2>
               <ul class="art-lst">
                 <c:forEach var="clearingArticleList" items="${requestScope.CLEARING_LANDING_ARICLE_LIST}">
                   <li>
                     <div class="art-view">
                       <div class="img_ppn">
                         <a href="${clearingArticleList.adviceDetailURL}" title="">
                           <c:if test="${not empty clearingArticleList.imageName}">
                             <img src="${clearingArticleList.imageName}" alt="${clearingArticleList.mediaAltText}" title="${clearingArticleList.postTitle}" />
                           </c:if>
                         </a>                                              
                       </div>
                       <div class="art-inc">
                         <div class="art-desc">
                           <c:if test="${not empty clearingArticleList.subCategoryName}">
                             <p class="cmart_txt">${clearingArticleList.subCategoryName}</p>
                           </c:if>
                           <h3>
                             <a href="${clearingArticleList.adviceDetailURL}" title="${clearingArticleList.postTitle}">${clearingArticleList.postTitle}</a>
                           </h3>
                         </div>
                       </div>
                     </div>
                   </li>
                 </c:forEach>
               </ul>
             </div>
           </div>
        </div>
     </c:if>
     <%-- Article Section --%>
  </section>
  <jsp:include page="/jsp/common/wuFooter.jsp">
     <jsp:param name="FROM_PAGE" value="LEAD_CAPTURE" />
  </jsp:include> 
  <script type="text/javascript">
    //pageViewFacebook();
  </script>
</body>