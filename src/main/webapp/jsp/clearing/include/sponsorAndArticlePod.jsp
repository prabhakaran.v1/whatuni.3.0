<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction" %>
<%  
   CommonFunction commonFun = new CommonFunction();
   String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");%>
<!-- Most popular uni Starts-->
<!--Changed the webclick to webform at line 118 BY Sangeeth.S for Jul_3_18 rel-->
<!-- Article Building starts-->
<c:if test="${not empty requestScope.listOfArticleTeaser}">
   <div class="clr_adv fl_w100">
      <div class="ad_cnr">
         <div class="content-bg">
            <div class="artc_cnt">
               <div class="artc_row">
                  <div class="artfc cm_art_pd">
                     <h2>Clearing advice</h2>
                     <p>Confused about Clearing? We've got you covered...</p>
                     <ul class="art-lst">
                        <%String sponsorClass = "";%>
                        <c:forEach var="articleTeasers" items="${requestScope.listOfArticleTeaser}" varStatus="i">
                           <c:if test="${not empty articleTeasers.sponsorUrl}"> 
                              <%sponsorClass = "clsp";%>
                           </c:if>
                        </c:forEach>
                        <c:forEach var="articleTeasers" items="${requestScope.listOfArticleTeaser}" varStatus="i">
                           <c:set var="checkPostId" value="${articleTeasers.postId}"></c:set>
                           <li>
                              <div class="art-view">
                                 <%String readClassName = "img_ppn";
                                    if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId"))){
                                        readClassName = "img_ppn read";
                                    }}%>
                                 <div class="<%=readClassName%>">
                                    <a href="${articleTeasers.adviceDetailURL}" 
                                       title="${articleTeasers.postTitle}">
                                       <c:if test="${not empty articleTeasers.imageName}">
                                          <img class="lazy-load" 
                                             src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="${articleTeasers.imageName}"
                                             alt="${articleTeasers.mediaAltText}"
                                             title="${articleTeasers.postTitle}"></img>
                                       </c:if>
                                       <%if(session.getAttribute("viewedPostIdsList")!=null){if(session.getAttribute("viewedPostIdsList").toString().contains((String)pageContext.getAttribute("checkPostId"))){%>
                                       <span class="re-img"></span>
                                       <%}}%>
                                    </a>
                                 </div>
                                 <div class="art-inc <%=sponsorClass%>">
                                    <div class="art-desc">
                                       <!-- <p class="cmart_txt">More on Clearing</p> -->
                                       <h4>${articleTeasers.subCategoryNameCapital}</h4>
                                       <h3>
                                          <a href="${articleTeasers.adviceDetailURL}"
                                             title="${articleTeasers.postTitle}">
                                          ${articleTeasers.postTitle}</a>
                                       </h3>
                                    </div>
                                    <c:if test="${not empty articleTeasers.sponsorUrl and not empty articleTeasers.collegeDisplayName}">
                                       <%--Added ga tracking for the June_5 release by Sangeeth.S--%>
                                       <c:set var="collegeNameId" value="${articleTeasers.collegeDisplayName}"></c:set>
                                       <%String gaCollegeName = commonFun.replaceSpecialCharacter((String)pageContext.getAttribute("collegeNameId")).toLowerCase();%>
                                       <div class="clr_slwrap">
                                          <div class="clr_splgo">            
                                             <a target="_blank" onclick="javascript:cpearticlesponsorurl('${articleTeasers.collegeId}','${articleTeasers.sponsorUrl}');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="${articleTeasers.sponsorUrl}"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="${articleTeasers.sponsorLogo}" alt="" title="" />							  </a>  
                                          </div>
                                          <div class="clr_asp">
                                             <h5 class="clr_stxt">Sponsored by</h5>
                                             <h5>
                                                <a class="clr_suni" target="_blank" onclick="javascript:cpearticlesponsorurl('${articleTeasers.collegeId}','${articleTeasers.sponsorUrl}');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick Sponsored', '<%=gaCollegeName%>');" href="${articleTeasers.sponsorUrl}">${articleTeasers.collegeDisplayName}</a>
                                             </h5>
                                          </div>
                                       </div>
                                    </c:if>
                                 </div>
                              </div>
                           </li>
                        </c:forEach>
                     </ul>
                     <article class="fl sall_art">
                        <a class="btn1" href="/advice/clearing/">
                        VIEW ALL ARTICLES
                        <i class="fa fa-long-arrow-right"></i>
                        </a>
                     </article>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</c:if>
<!-- Article Building Ends-->