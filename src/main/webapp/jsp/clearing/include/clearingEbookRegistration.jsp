<!DOCTYPE HTML >
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@page import= "WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" %>

    
<%
  String hidenewuserform = (String)request.getAttribute("hidenewuserform");
  String tabFocus = request.getAttribute("tabFocus") != null ? (String)request.getAttribute("tabFocus") : "login";
  CommonUtil util = new CommonUtil();
  String TCVersion = util.versionChanges(GlobalConstants.TERMS_AND_COND_VER);
  String nonmywu = request.getParameter("nonmywu") != null ? request.getParameter("nonmywu") : "";
  String pnotes = request.getParameter("pnotes") != null ? request.getParameter("pnotes") : "";
  String downloadurl = request.getParameter("downloadurl") != null ? request.getParameter("downloadurl") : ""; //16-Apr-2014
  String pdfId = request.getParameter("pdfId") != null ? request.getParameter("pdfId") : ""; //16-Apr-2014
  String submitType = request.getParameter("submitType") != null ? request.getParameter("submitType") : ""; //16-Apr-2014
  String pdfName = request.getParameter("pdfName") != null ? request.getParameter("pdfName") : ""; //16-Apr-2014  
  String referrerURL_GA = request.getAttribute("referrerURL_GA") != null ? (String)request.getAttribute("referrerURL_GA") : "";//5_AUG_2014  
  String regLoggingType = request.getParameter("gaLoggingType") != null ? request.getParameter("gaLoggingType") : "";
  String vwcid = request.getParameter("vwcid") != null ? request.getParameter("vwcid") : ""; //16-Apr-2014
  String vwurl = request.getAttribute("vwurl") != null ? request.getAttribute("vwurl").toString() : ""; //16-Apr-2014
  String vwcourseId = request.getAttribute("vwcourseId") != null ? request.getAttribute("vwcourseId").toString() : "";    
  String vwsid = request.getAttribute("vwsid") != null ? request.getAttribute("vwsid").toString() : "";
  String vwnid = request.getAttribute("vwnid") != null ? request.getAttribute("vwnid").toString() : "";
  String vwprice  = request.getAttribute("vwprice") != null ? request.getAttribute("vwprice").toString() : "";
  String vwcname  = request.getAttribute("vwcname") != null ? request.getAttribute("vwcname").toString() : "";
  String vwCollegeId  = request.getAttribute("vwCollegeId") != null ? request.getAttribute("vwCollegeId").toString() : "";
  String vwsewf  = request.getAttribute("vwsewf") != null ? request.getAttribute("vwsewf").toString() : "";	
  String vwpdfcid  = request.getAttribute("vwpdfcid") != null ? request.getAttribute("vwpdfcid").toString() : "";	    
    
  String splashFlag = request.getAttribute("splash-page") != null ? (String)request.getAttribute("splash-page") : "";//9_DEC_2014  
  String nonloggedTm = request.getAttribute("nonloggedTm") != null &&  request.getAttribute("nonloggedTm")!=""? (String)request.getAttribute("nonloggedTm") : "";//9_DEC_2014  
  String lbregistratioVal = request.getAttribute("lbregistration") != null ? (String)request.getAttribute("lbregistration") : ""; 
  String firstNameprep = request.getAttribute("firstNameprep") != null && !("undefined").equals(request.getAttribute("firstNameprep"))  ? (String)request.getAttribute("firstNameprep") : ""; 
  String surNameprep = request.getAttribute("surNameprep") != null && !("undefined").equals(request.getAttribute("surNameprep"))  ? (String)request.getAttribute("surNameprep") : ""; 
  String userNameprep = request.getAttribute("userNameprep") != null &&  !("undefined").equals(request.getAttribute("userNameprep"))? (String)request.getAttribute("userNameprep") : ""; 
  String emailAddressprep = request.getAttribute("emailAddressprep") != null && !("undefined").equals(request.getAttribute("emailAddressprep")) ? (String)request.getAttribute("emailAddressprep") : "";
  String fbUserIdprep = request.getAttribute("fbUserIdprep") != null && !("undefined").equals(request.getAttribute("fbUserIdprep")) ? (String)request.getAttribute("fbUserIdprep") : "";
    
  String socialRegType = ((request.getAttribute("socialRegType") != null && !"".equals(request.getAttribute("socialRegType"))) ? (String)request.getAttribute("socialRegType") : "");
  String surveyForm = request.getAttribute("surveyForm") != null ? (String)request.getAttribute("surveyForm") : "";//Added by Priyaa for 21_JUL_2014_REL for survey auto login 
  String homepageUrl = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName(); 
  String emailDomainJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.email.domain.js");
  String emailDomainLoginJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.email.domain.login.js");
  CommonFunction common = new CommonFunction();
  String[] yoeArr = common.getYearOfEntryArr();
  String YOE_1 = yoeArr[0];
  String YOE_2 = yoeArr[1];
  String YOE_3 = yoeArr[2];
  String YOE_4 = yoeArr[3];
  pageContext.setAttribute("yoe1", YOE_1);
  pageContext.setAttribute("yoe2", YOE_2);
  pageContext.setAttribute("yoe3", YOE_3);
  pageContext.setAttribute("yoe4", YOE_4);
%>
  
<html:form action="newuserregistration.html?method=register" commandName="registerBean" class="fl basic_inf pers_det sgn_up">
          
  <%-- Uni List --%>
  <%-- SignUp --%>
  <div class="fl qua_cnf">
    <div class="sgalai">
      <p class="comptx"><span class="alrdy_acc">Already have an account?</span> <a title="Sign in" id="signInLink" class="link_blue">sign in</a></p>
      <%-- Social login --%>
      <div id="loginFieldPod" style="display:none" class="cnf_off ebsin">
        <div class="sgn">
          <div class="so-btn">
          <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
            <a class="btn1 fbbtn" onclick="loginInfoFacebook('fblogin', 'fb_clearing_landing');" title="Login with Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK</a>
           </c:if>
            <a class="btn1 btn_view_all mt5 w150 mr0" title="LOGIN" onclick="clearingLandingUserPageFlag('SIGN_EMAIL');checkUserAgent();">YOUR EMAIL<i class="fa fa-long-arrow-right"></i></a>
          </div>
          <p class="qler1 valid_err" id="fblbregistration_error" style="display: none;"></p> 
          <input type="hidden" name="middlePageSigninFlag" id="middlePageSigninFlag" value="N"/>
        </div>
      </div>
      <%-- Social login --%>
    </div>		
  </div>
  <%-- Checkbox Confirmation --%>
  <%-- Social login --%>
  <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
  <div class="cnf_off">
    <h5 class="cmn-tit">Or sign up with...</h5>
    <div class="sgn">
      <div class="so-btn">
        <a class="btn1 fbbtn" onclick="loginInfoFacebook('fblogin', 'fb_clearing_landing')" title="Login with Facebook"><i class="fa fa-facebook fa-1_5x"></i>FACEBOOK</a>                    
        <%--<a class="btn1 gobtn mr0" title="Login with Google" href="javascript:loginWithGooglePlus();" style="display:none;"><i class="fa fa-google-plus fa-1_5x"></i>GOOGLE<i class="fa fa-long-arrow-right"></i></a>--%>
      </div>
      <p class="qler1 valid_err" id="fblbregistration_error" style="display: none;"></p> <%--Added p tag to fix this bug:39819, on 08_Aug_2017, By Thiyagu G--%>
    </div>
  </div>
</c:if>
  <%-- Social login --%>
  <div class="pers_lst pt-20">
    <h5 class="cmn-tit">Your details</h5>
  <fieldset id="firstName_fieldset" class="fl tx-bx"> 
      <c:if test="${not empty requestScope.firstNameprep}">
        <html:input path="firstName" id="firstName" autocomplete="off" class="frm-ele" maxlength="40" value="<%=firstNameprep%>" onfocus="showTooltip('firstNameYText');" onblur="labelTopOnBlur(this);hideTooltip('firstNameYText');ebookRegistrationValidations(this.id);" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/>
      </c:if>
      <c:if test="${empty requestScope.firstNameprep}">
        <html:input path="firstName" id="firstName" autocomplete="off" class="frm-ele" maxlength="40" onfocus="showTooltip('firstNameYText');" onblur="labelTopOnBlur(this);hideTooltip('firstNameYText');ebookRegistrationValidations(this.id);" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}" />
      </c:if>
      <label class="lbco">First name*</label>
      <span class="cmp" id="firstNameYText" style="display:none;"> 
        <div class="hdf5"></div>
        <div>We'd like this information so we don't call you by the wrong name. That'd be kinda rude.</div>
        <div class="linear"></div>
      </span>
      <p class="fail-msg" id="firstName_error" style="display:none;"></p>                              
    </fieldset>
    <fieldset id="surName_fieldset" class="fr tx-bx">  
      <c:if test="${empty requestScope.surNameprep}">                            
        <html:input path="surName" id="surName" autocomplete="off" class="frm-ele" maxlength="40" onblur="labelTopOnBlur(this);ebookRegistrationValidations(this.id);" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/>
      </c:if>
      <c:if test="${not empty requestScope.surNameprep}">
        <html:input path="surName" id="surName" autocomplete="off" class="frm-ele" maxlength="40" onblur="labelTopOnBlur(this);ebookRegistrationValidations(this.id);" value="<%=surNameprep%>" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/>
      </c:if>
      <label class="lbco">Last name*</label> 
      <span class="cmp rght" id="surNameYText" style="display:none;"> 
        <div class="hdf5"></div>
        <div>We'd like this information so we don't call you by the wrong name. That'd be kinda rude.</div>
        <div class="linear"></div>
      </span>
      <p class="fail-msg" id="surName_error" style="display:none;"></p>
    </fieldset>
  </div>
  <div class="pers_lst pt-20">
    <fieldset class="tx-bx w-100" id="emailAddress_fieldset">  
      <c:if test="${empty requestScope.emailAddressprep}"> 
        <html:input path="emailAddress" id="emailAddress" autocomplete="off" maxlength="120" class="frm-ele" onfocus="showTooltip('emailAddressYText');" onblur="labelTopOnBlur(this);hideTooltip('emailAddressYText');" onkeydown="hideEmailDropdown(event, 'autoEmailId');"/>
      </c:if>
      <c:if test="${not empty requestScope.emailAddressprep}">
        <html:input path="emailAddress" id="emailAddress" autocomplete="off" maxlength="120" class="frm-ele" disabled="true" onfocus="showTooltip('emailAddressYText');" onblur="labelTopOnBlur(this);hideTooltip('emailAddressYText');" onkeydown="hideEmailDropdown(event, 'autoEmailId');" value="<%=emailAddressprep%>" />
      </c:if>
      <c:if test="${not empty requestScope.fbUserIdprep}">
        <html:hidden path="fbUserId" id="fbUserId" class="frm-ele" value="<%=fbUserIdprep%>" />
      </c:if>
      <label class="lbco">Email*</label>
      <span class="cmp" id="emailAddressYText" style="display:none;"> 
        <div class="hdf5"></div>
        <div>Tell us your email and we'll reward you with...an email.</div>
        <div class="linear"></div>
      </span>
      <%--<p class="qler1" id="emailAddress_error" style="display:none;"></p>--%>
      <p class="fail-msg" id="ebookEmailAddress_error" style="display:none;"></p>
    </fieldset>
  </div>
  <div class="pers_lst pt-20">                                                  
    <fieldset id="password_fieldset" class="tx-bx w-100 pwd_feld">                         
      <html:password path="password" onblur="labelTopOnBlur(this);ebookRegistrationValidations(this.id);" id="password" maxlength="20" class="frm-ele" style="display: none;" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}"/> 
      <label class="lbco">Password*</label>
      <span class="pwd_eye"><a onblur="javascript:showPassword('eyeId', 'password');" href="javascript:showPassword('eyeId', 'password');"><i id="eyeId" class="fa fa-eye" aria-hidden="true"></i></a></span>
      <input type="text" onfocus="changePassword('regtextpwd','password','password','regtextpwd');" id="regtextpwd" class="frm-ele"/>
      <p class="fail-msg" id="password_error" style="display:none;"></p>
    </fieldset>                                                  
  </div>
                        
  <div class="pers_lst pt-20 cnf_off"> 
    <h5 class="cmn-tit">When would you like to start?</h5>
    <fieldset class="row-fluid mb10 mt25 strt_yr">
	    <fieldset id="yoe_fieldset" class="ql-inp">
        <%String yoe1Checked = ""; %>
        <c:if test="${yearofEntry eq yoe1}">
          <%yoe1Checked = "checked=\"checked\"";%>
        </c:if>
        <span class="ql_rad">  
          <input type="radio" onmouseout="hideTooltip('yeartoJoinCourse1YText');" onmouseover="showTooltip('yeartoJoinCourse1YText');" onclick="ebookRegistrationValidations(this.id);" class="check" value="<%=YOE_1%>" id="yeartoJoinCourse1" name="yearToJoinCourse" <%=yoe1Checked%>><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse1"><%=YOE_1%></label>
          <span class="cmp tltip pos_rht" id="yeartoJoinCourse1YText" style="display:none;"> 
            <div class="hdf5"></div>
            <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
            <div class="linear"></div>
          </span>
        </span>
        <%String yoe2Checked = "";%>
        <c:if test="${yearofEntry eq yoe2}">
          <%yoe2Checked = "checked=\"checked\"";%>
        </c:if>
        <span class="ql_rad">                              
          <input type="radio" onmouseout="hideTooltip('yeartoJoinCourse2YText');" onmouseover="showTooltip('yeartoJoinCourse2YText');" onclick="ebookRegistrationValidations(this.id);" class="check" value="<%=YOE_2%>"  id="yeartoJoinCourse2" name="yearToJoinCourse" <%=yoe2Checked%>/><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse2"><%=YOE_2%></label>
          <span class="cmp tltip" id="yeartoJoinCourse2YText" style="display:none;"> 
            <div class="hdf5"></div>
            <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
            <div class="linear"></div>
          </span>
        </span>
        <%String yoe3Checked = "";%>
        <c:if test="${yearofEntry eq yoe3}">
          <%yoe3Checked = "checked=\"checked\"";%>
        </c:if>
        <span class="ql_rad">                              
          <input type="radio" onmouseout="hideTooltip('yeartoJoinCourse3YText');" onmouseover="showTooltip('yeartoJoinCourse3YText');" onclick="ebookRegistrationValidations(this.id);" class="check" value="<%=YOE_3%>"  id="yeartoJoinCourse3" name="yearToJoinCourse" <%=yoe3Checked%>/><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse3"><%=YOE_3%></label>
          <span class="cmp tltip" id="yeartoJoinCourse3YText" style="display:none;"> 
            <div class="hdf5"></div>
            <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
            <div class="linear"></div>
          </span>
        </span>
        <%String yoe4Checked = "";%>
        <c:if test="${yearofEntry eq yoe4}">
          <%yoe4Checked = "checked=\"checked\"";%>
        </c:if>
        <span class="ql_rad mr0">                              
          <input type="radio" onmouseout="hideTooltip('yeartoJoinCourse4YText');" onmouseover="showTooltip('yeartoJoinCourse4YText');" onclick="ebookRegistrationValidations(this.id);" class="check" value="<%=YOE_4%>"  id="yeartoJoinCourse4" name="yearToJoinCourse" <%=yoe4Checked%>/><label class="w100 fnt_lrg fl pl5" for="yeartoJoinCourse4"><%=YOE_4%></label>
          <span class="cmp tltip pos_lft" id="yeartoJoinCourse4YText" style="display:none;"> 
            <div class="hdf5"></div>
            <div>We don't want to give you information for the wrong year. That'd be pretty useless.</div>
            <div class="linear"></div>
          </span>
        </span>
        <p class="fail-msg" id="yoe_error" style="display:none;"></p>
      </fieldset>
    </fieldset>  
    <div class="borderbot fl mt20 mb40"></div>
  </div>
                        
  <div class="pers_lst">        
    <h3 class="fnt_lbd fnt20 mb20 mt20">Stay up to date by email <span class="fnt_lrg gry_txt">(optional)</span></h3>
    <div class="btn_chk">
      <span class="chk_btn">
        <input type="checkbox" value="" class="chkbx1" id="marketingEmailRegFlag" onclick="setRegFlags(this.id);"/>
        <span class="chk_mark"></span>
      </span>
      <p><label for="marketingEmailRegFlag" class="fnt_lbd chkbx_100">Newsletters <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
        Emails from us providing you the latest university news, tips and guides
      </p>
    </div>
    <div class="btn_chk">
      <span class="chk_btn">
        <input type="checkbox" value="" class="chkbx1" id="solusEmailRegFlag" onclick="setRegFlags(this.id);"/>
        <span class="chk_mark"></span>
      </span>
      <p><label for="solusEmailRegFlag" class="fnt_lbd chkbx_100"> University updates <span class="cmrk_tclr">(Tick to opt in)</span> </label> <br>
        <spring:message code="wuni.solus.flag.text"/>
      </p>
    </div>                        
    <div class="btn_chk">
      <span class="chk_btn">
       <input type="checkbox" value="" class="chkbx1" id="surveyEmailRegFlag" onclick="setRegFlags(this.id);"/>
       <span class="chk_mark"></span>
      </span>
      <p><label for="surveyEmailRegFlag" class="fnt_lbd chkbx_100">Surveys <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
         <spring:message code="wuni.survey.flag.text"/>
      </p>
    </div>
    <div class="borderbot fl mt20 mb40"></div>
  </div>
  <div class="pers_lst mt20">
    <fieldset class="row-fluid">                         
      <fieldset class="mt0 rgfm-lt">                                                                                                                                            
        <span class="chk_btn">
          <input type="checkbox"  name="tac" value="on" id="termsc" class="chkbx1" >
          <span class="chk_mark"></span>
        </span>
        <label for="termsc" id="pdfSignup" style="display: none;">
          <span class="rem1">I confirm I'm over 13 and agree to the <a title="Whatuni community" class="link_blue" href="javascript:void(0);" onclick="window.open('<%=TCVersion%>','termspopup','width=650,height=400,scrollbars=yes,resizable=yes');">terms and conditions</a> and <a title="privacy notice" class="link_blue" href="javascript:void(0);" onclick="showPrivacyPolicyPopup()">privacy notice</a>, and to become a member of the Whatuni community.<span class="as_trk">*</span> </span>
        </label>
        <label for="termsc" id="normalsignup" style="display: block;">
          <span class="rem1">I confirm I'm over 13 and agree to the <a title="Whatuni community" class="link_blue" href="javascript:void(0);" onclick="window.open('<%=TCVersion%>','termspopup','width=650,height=400,scrollbars=yes,resizable=yes');">terms and conditions</a> and <a title="privacy notice" class="link_blue" href="javascript:void(0);" onclick="showPrivacyPolicyPopup()">privacy notice</a>, and to become a member of the Whatuni community.<span class="as_trk">*</span> </span>
        </label>
        <p class="fail-msg" id="termsc_error" style="display:none;"></p>
      </fieldset>
      <fieldset class="mt5" style="display: none;">
        <input type="checkbox" class="check" id="newsLetter" value="Y" name="newsLetter" checked="checked" onkeypress="javascript:if(event.keyCode==13){return validateUserRegister()}">
        <span class="rem1">We may send you updates and offers from carefully selected partners, please untick this box if you would prefer not to be sent these emails.</span>
      </fieldset>
      <input type="hidden" id="emailValidateFlag" value="true"/>
      <a onclick="return validateUserRegister();" id="btnLogin" title="Register" class="btn1 blue mt15 w150 fr"><span id="formButtonText">SIGN UP</span>
        <i class="fa fa-long-arrow-right"></i>
      </a>
      <p id="loadinggifreg" class="fr pt-10 clear_rht" style="display:none;" ><span id="loadgif"><img title="" alt="Loading" src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/indicator1.gif", 0)%>" class="loading"/></span></p>
    </fieldset>
  </div>
</html:form>
<input type="hidden" id="screenwidth" name="screenwidth" value=""/>
<input type="hidden" id="downloadurl" value="<%=downloadurl%>"/> <%--16-Apr-2014--%>
<input type="hidden" id="pdfId" value="<%=pdfId%>"/> <%--16-Apr-2014--%>
<input type="hidden" id="submitType" value="<%=submitType%>"/> <%--16-Apr-2014--%>
<input type="hidden" id="pdfName" value="<%=pdfName%>"/> <%--16-Apr-2014--%>
<input type="hidden" id="referrerURL_GA" value="<%=referrerURL_GA%>"/> <%--5_AUG_2014--%>
<input type="hidden" id="regLoggingType" value="lead-capture"/> <%--5_AUG_2014--%>
<input type="hidden" id="vwcid" value="<%=vwcid%>"/> <%--3_FEB_2015--%> 
<input type="hidden" id="vwurl" value="<%=vwurl%>"/> <%--3_FEB_2015--%> 
<input type="hidden" id="vwsid" value="<%=vwsid%>"/> <%--3_FEB_2015--%> 
<input type="hidden" id="vwnid" value="<%=vwnid%>"/> <%--3_FEB_2015--%> 
<input type="hidden" id="vwprice" value="<%=vwprice%>"/> <%--3_FEB_2015--%>  
<input type="hidden" id="vwcourseId" value="<%=vwcourseId%>"/> <%--3_FEB_2015--%>      
<input type="hidden" id="vwcname" value="<%=vwcname%>"/> <%--3_FEB_2015--%>   
<input type="hidden" id="vwpdfcid" value="<%=vwpdfcid%>"/>
<input type="hidden" id="nonAdvLogoFlag" />
      
<script type="text/javascript" id="domnScptId">
  jQuery(document).ready(function(e){        
    var arr = ["emailAddress", "hideTooltip('emailAddressYText');", "clearErrorMsg('emailAddress_fieldset','ebookEmailAddress_error');"];
    jQuery("#emailAddress").autoEmail(arr);        
    updateScreenWidth();
  });
</script>
<input type="hidden" id="domainLstId" value='<%=java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.form.email.domain.list")%>' />
 