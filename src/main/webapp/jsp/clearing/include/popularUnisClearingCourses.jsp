<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, mobile.util.MobileUtils" %>
<%
  CommonFunction comFun = new CommonFunction();
  String gaCollegeName = "";
  boolean mobileFlag = new MobileUtils().userAgentCheck(request);
  %>
<c:if test="${not empty requestScope.listOfFeaturedProviders}">
  <div class="clr_popcnr" id="featuredUni" data-id="stats_not_logged">
    <h3 class="clr_ptit">Featured universities</h3>
    <%--<p class="cal_txt">(Calls free from UK landlines or standard network rate from mobile)</p>--%> <%--Added calls free text for clearing on 16_May_2017, By Thiyagu G--%>
    <div class="clr_plogo">
      <ul id="ulFeaturedUni">
        <c:forEach var="featuredProviders" items="${requestScope.listOfFeaturedProviders}" varStatus="row">
          <c:set var="rowValue" value="${row.index}"/>
          <c:set var="collegeName" value="${featuredProviders.collegeName}" />
          <li>
            <%--Added GA log for featured providers for 3_Jul_2018, By Sangeeth.S--%>
            <% gaCollegeName = "";
              gaCollegeName = comFun.replaceSpecialCharacter(pageContext.getAttribute("collegeName").toString());%>
            <a href='${featuredProviders.url}'
              target="_blank"
              onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>');advertiserExternalUrlStatsLogging('clearingFeaturedProviders','${featuredProviders.collegeId}','${featuredProviders.url}','${featuredProviders.orderItemId }');">
            <img width="140" class="lazy-load clr_hlogo"
              src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src='${featuredProviders.domainMediaPath}'
              title='${featuredProviders.collegeDisplayName}'
              alt='${featuredProviders.collegeDisplayName}'></img>
            </a>
            <%--Added hotline numbers for featured providers for 16_May_2017, By Thiyagu G.--%>
            <div class="sr_opd_mc1 clr15">
              <div class="btns_interaction">
                <c:if test="${not empty featuredProviders.hotline}">
                  <a class="cbtn" id="contact_<%=(Integer.parseInt(pageContext.getAttribute("rowValue").toString()))%>"
                  title="${featuredProviders.hotline}"
                  onclick="GAInteractionEventTracking('Webclick', 'interaction', 'hotline', '<%=comFun.replaceSpecialCharacter((String)pageContext.getAttribute("collegeName"))%>');
                  cpeFeaturedHotlineClearing(this,'${featuredProviders.collegeId}','','<c:out value="${featuredProviders.hotline}" escapeXml="false" />','${featuredProviders.orderItemId }');
                  changeContact('${featuredProviders.hotline}','<%=comFun.replaceSpecialCharacter((String)pageContext.getAttribute("collegeName"))%>','contact_<%=(Integer.parseInt(pageContext.getAttribute("rowValue").toString()))%>', '${featuredProviders.collegeId}');">
                  <i class="fa fa-phone pr5"></i>CALL NOW
                  </a>
                </c:if>
              </div>
            </div>
          </li>
        </c:forEach>
      </ul>
    </div>
  </div>
  <input type="hidden" id="check_mobile_hidden" value="<%=mobileFlag%>"/> 
</c:if>