<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>      

<c:if test="${not empty uniAjaxList}">
   <c:forEach var="uniAjaxList" items="${uniAjaxList}" varStatus="i">
      <c:set var="index" value="${i.index}"/>
      <div id="uni_${i.index}" <c:if test="${empty uniAjaxList.sdMessage}"> onclick="ajaxDrpDownSelectUni('${uniAjaxList.url }');" class="optionDiv" </c:if>  <c:if test="${not empty uniAjaxList.sdMessage}"> class="optionDiv dsbl" </c:if> >
         <span class="ajx_sub">
         <span id="browseCategoryId_${index}" style="display:none">${uniAjaxList.url }</span> 
         <span class="ajx_rt">
         <span class="ajx_txt" id="university_${i.index}"> ${uniAjaxList.description} </span>
         </span>
         </span>
         <c:if test="${not empty uniAjaxList.sdMessage}">
         <span class="nocrse">${uniAjaxList.sdMessage }</span>
         </c:if>
      </div>
   </c:forEach>
</c:if>