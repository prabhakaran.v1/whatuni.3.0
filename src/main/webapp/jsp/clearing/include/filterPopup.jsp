<%@page import="WUI.utilities.CommonUtil"%>
<div class="sidenav" id="sidenav">
  <a href="javascript:void(0)" class="closebtn"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/clr_fltr_close_gry.svg", 0)%>" alt="close icon" width="14" height="14"></a>
  <div class="fltr_cntr">
    <div id="locationFilter" style="display: none;">
      <jsp:include page="/jsp/clearing/searchresult/include/locationFilter.jsp" />
    </div>
	
	<div id="courseFilter" style="display: none;">
	  <jsp:include page="/jsp/clearing/include/courseFilter.jsp"/>
	</div>
	<div id="acceptanceFilter" style="display: none;">
	  <jsp:include page="/jsp/clearing/include/acceptanceFilter.jsp" />
	</div>
	<div id="universityFilter" style="display: none;">
	  <jsp:include page="/jsp/clearing/searchresult/include/uniSearchFilter.jsp" />
	</div>
	<div id="subjectFilter" style="display: none;">
	  <%-- <jsp:include page="/jsp/clearing/providerresult/include/subjectSearchFilter.jsp" /> --%>
	</div>
  </div>
  <div class="btm_pod" id="bottom_button" style="display: none;">
    <div class="btm_btns mt0"> 
	  <span class="clear" id="clear"><a href="javascript:void(0);">Clear</a></span>
	  <span class="aply_btn" id="apply"><a href="javascript:void(0);">APPLY</a></span>
	</div>
  </div>
  <input type="hidden" id="filterOpenType" value="" />
  <input type="hidden" id="regionHidden" value="${region}" />
  <input type="hidden" id="studyModeHidden" value="${studyMode}" />
  <input type="hidden" id="qualificationHidden" value="${qual}" />
  <input type="hidden" id="hybridHidden" value="${hybridFlag}" />
  <input type="hidden" id="locationTypeHidden" value="${locationType}"/>
  <input type="hidden" id="pageUrl" value="${pageUrl}" />
  <input type="hidden" id="subjectHidden" value="${subject}" />
  <input type="hidden" id="universityHidden" value="${university}" />
  <input type="hidden" id="keywordHidden" value="${searchKeyword}" />
  <input type="hidden" id="campusHidden" value="${campusType}" />
  <input type="hidden" id="scholarshipHidden" value="${scholarshipFlag}" />
  <input type="hidden" id="accommodationHidden" value="${accommodationFlag}" />
  <input type="hidden" id="scoreHidden" value="${scoreValue}" />
  <input type="hidden" id="acceptanceHidden" value="${acceptanceFlag}" />
  <input type="hidden" id="collegeIdHidden" value="${collegeId}" />
  <input type="hidden" id="qualCodeHidden" value="${qualCode}" />
  <input type="hidden" id="queryStr" value="${queryStr}" />  
  <input type="hidden" id="pageNoHidden" value="${pageNo}" />
  
    <input type="hidden" id="regionHiddenUrl" value="${region}" />
  <input type="hidden" id="studyModeHiddenUrl" value="${studyMode}" />
  <input type="hidden" id="qualificationHiddenUrl" value="${qual}" />
  <input type="hidden" id="hybridHiddenUrl" value="${hybridFlag}" />
  <input type="hidden" id="locationTypeHiddenUrl" value="${locationType}"/>
  <input type="hidden" id="subjectHiddenUrl" value="${subject}" />
  <input type="hidden" id="universityHiddenUrl" value="${university}" />
  <input type="hidden" id="keywordHiddenUrl" value="${searchKeyword}" />
  <input type="hidden" id="campusHiddenUrl" value="${campusType}" />
  <input type="hidden" id="scholarshipHiddenUrl" value="${scholarshipFlag}" />
  <input type="hidden" id="accommodationHiddenUrl" value="${accommodationFlag}" />
  <input type="hidden" id="scoreHiddenUrl" value="${scoreValue}" />
  <input type="hidden" id="acceptanceHiddenUrl" value="${acceptanceFlag}" />
  <input type="hidden" id="collegeIdHiddenUrl" value="${collegeId}" />
  <input type="hidden" id="qualCodeHiddenUrl" value="${qualCode}" />
  <input type="hidden" id="pageNoHiddenUrl" value="${pageNo}" />
</div>
	