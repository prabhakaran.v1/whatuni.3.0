<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${not empty requestScope.faqPodList}">
   <div class="clr_faq grey_bg fl_w100" id="faqs">
      <div class="ad_cnr">
         <div class="content-bg">
            <div class="accord_cnt">
               <h3 class="clr_ptit">FAQs</h3>
               <!-- Accordion Container -->
               <div class="accordion_container fl_w100">
                  <c:forEach var="faqPodList" items="${requestScope.faqPodList}" varStatus="row">
                     <c:set var="rowValue" value="${row.index}"/>
                     <div class="accordion_head fl_w100">
                        <div class="ahd_row fl_w100">
                           <h3 class="acr-tit fl"  onclick="getAnswer(${rowValue});">${faqPodList.questionTitle}</h3>
                           <%if((Integer.parseInt(pageContext.getAttribute("rowValue").toString())) == 0){%>
                           <span class="plusminus" onclick="getAnswer(0);">
                           <i id="icon_${rowValue}" class="fa fa-minus-circle" aria-hidden="true"></i></span>
                           <div id="answer_0" class="accordion_body fl_w100" style="display: block;">
                              <c:out value="${faqPodList.answer}" escapeXml="false" />
                           </div>
                           <%}else {%>
                           <span class="plusminus" onclick="getAnswer(${rowValue});">
                           <i id="icon_${rowValue}" class="fa fa-plus-circle" aria-hidden="true"></i></span>
                           <div id="answer_${rowValue}" class="accordion_body fl_w100"  style="display: none;">
                              <c:out value="${faqPodList.answer}" escapeXml="false" />
                              <%}%>
                           </div>
                        </div>
                     </div>
                  </c:forEach>
               </div>
            </div>
         </div>
      </div>
   </div>
</c:if>