<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="page_name">
  <tiles:getAsString name="pagename3" ignore="true"/>
</c:set>
 <!-- POST Clearing register pod 26-08-2020, S.Minu -->
<div class="clr_hro grd_hd_bg">
	<div class="ad_cnr">
		<div class="content-bg">
			<div class="clr_early">
				<h1 class="hd"><spring:message code="title.text.postclearing" /></h1>				
				<c:choose>
			    	<%-- <c:when test="${empty userId or '0' eq userId}"> --%>
			    	<c:when test="${empty sessionScope.userInfoList}">    
			    	    <p class="desc"><spring:message code="subtitle.text.postclearing" /></p>
				    	<a href="javascript:void(0);" id="postClearingRegister" class="clr_reg_btn" data-pagename="${not empty page_name ? page_name : ''}"><spring:message code="register.button.postclearing" /></a>
				    </c:when>
				    <c:otherwise>
				        <p class="desc"><spring:message code="loggedin.use.rmsg.postclearing" /></p>
				    </c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
</div>