<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% 
String qualTxt = "";
String vowTex = "a";
String dispTxt = "";
String userUcasPoint = (String)session.getAttribute("USER_UCAS_SCORE");
pageContext.setAttribute("userUcasPoint", userUcasPoint);
String pageName = (String)request.getParameter("CLEARING_NAV");
String paramName = "CLEARING_JOURNEY_TOP_NAV".equalsIgnoreCase(pageName) ? "CLEARING_JOURNEY_TOP_NAV" : "CLEARING_JOURNEY_HOME_PAGE";
pageContext.setAttribute("paramName", paramName);
%>   
<ul class="tabs" id="srchUlId">
   <li class="tab-link current" data-tab="tab-1">Clearing courses</li>
   <li class="tab-link" data-tab="tab-2">Universities</li>
</ul>
<div class="srch_box fl_w100">
   <div id="tab-1" class="crse_tab tab-content fl_w100 current">
      <div class="crse_srch dis_inblk">
         <div class="tab_btm">
            <div class="sr_tab_form">
               <div class="land_inp_grp sr_subcrse fl" id="subjectDivId">
                  <div class="sr_country_cnr" id="clearing_subSrchDivId">
                     <label for="keywordTpNav" class="visualhid">Subject</label>
                     <input type="text" class="inptxt" name="topClearingSubjectName" id="topClearingSubjectName" placeholder="Subject" onkeyup="ajaxList(event,'topClearingSubjectName','ajax_listOfOptions_clearing','subjectDivId')" autocomplete="off">                                          
                     <input type="hidden" id="subjectName_hidden" value="">
                     <input type="hidden" id="subjectName_urlHidden" value="">
                  </div>
               </div>
               <div class="land_inp_grp sr_subcrse fl" id="clearing_locSrchDivId">
                  <div class="sr_country_cnr" id="locationDivId">
                     <label for="keywordTpNav" class="visualhid">Location (optional)</label>
                     <input type="text" class="inptxt clr_arw" 	id="clearing_locSrchId" onclick="clearingShowHideLocDrpDn('clearing_locDrpDnId');"
                        readonly="readonly"
                        placeholder="Location (optional)">                                          
                  </div>
                  <!-- location List Start -->
                  <div id="clearing_locDrpDnId" class="opsr_lst" style="display:none">
                     <c:if test="${not empty locationList}">
                        <ul id="clearing_regionUlist">
                           <c:forEach var="locationList" items="${locationList}" varStatus="i">
                              <c:set var="index" value="${i.index}"/>
                              <li id="${locationList.regionId }" onclick="ajaxDrpDownSelect('clearing_locSrchId', '${locationList.regionName }','${locationList.regionUrlText }');">																		
                                 <span id="${locationList.regionUrlText }"><a href="javascript:void(0);">${locationList.regionName }</a></span>																		
                              </li>
                           </c:forEach>
                        </ul>
                     </c:if>
                  </div>
                  <input type="hidden" id="locationUrl_hidden" value="">
                  <!-- location List End -->
               </div>
               <div class="land_inp_grp sr_subcrse fl">
                  <div class="sr_country_cnr" id="ucasDivId">
                     <label for="keywordTpNav" class="visualhid">UCAS points (optional)</label>
                     <input type="text" class="inptxt" name="ucasPoint" id="ucasPoint" placeholder="UCAS points (optional)" value="${not empty userUcasPoint ? userUcasPoint : ''}"  maxlength="3" autocomplete="off"">
                  </div>
               </div>
               <div class="err_field fl_w100" id="clearing_errMsgSubSrchlbox" style="display:none">
                  <p></p>
               </div>
            </div>
            <div class="advsrc_lnk fl_w100 mob"><span class="ucas_src_wrap"><span class="ucas_src">Don't know your UCAS points?</span><a href="javascript:void(0);" onclick="getUcasCalculator('${paramName eq 'CLEARING_JOURNEY_TOP_NAV' ? 'topNav': 'homePod'}')">Use our calculator</a></span> </div>
            <div class="btn_group fl">
               <button id="clearing_landingSearch" type="button" class="cug_btn_blue cug_btn_prim" onclick="clearingSRUrl();"> <span class="srch"></span> Search </button>
            </div>
         </div>
         <div class="advsrc_lnk fl_w100 dsk"><span class="ucas_src_wrap"><span class="ucas_src">Don't know your UCAS points?</span> <a href="javascript:void(0);" onclick="getUcasCalculator('${paramName eq 'CLEARING_JOURNEY_TOP_NAV' ? 'topNav': 'homePod'}')">Use our calculator</a></span> </div>
      </div>
   <c:if test="${paramName eq 'CLEARING_JOURNEY_TOP_NAV'}">
  <div class="key_tpics fl_w100">
  <c:if test="${not empty requestScope.clearingRecentSearchList}">
	<div class="keytop_ui fl_w100">
		<div class="adv_ui fl">
			<h3>Recent searches</h3>
			<ul>
			  <c:forEach var="recentSrchList" items="${requestScope.clearingRecentSearchList}">
			     <c:set var="qualDesc" value="${recentSrchList.qualDesc}" />
			     <c:set var="subDesc" value="${recentSrchList.subjectName}" />
			     <%
			       dispTxt = "You searched for ";
			       qualTxt = "";
			       vowTex = "a "; 
			       qualTxt = (String)pageContext.getAttribute("qualDesc");
			       char vowel = qualTxt.toLowerCase().charAt(0);
			       if( (vowel == 'a')||(vowel == 'e')||(vowel == 'i')||(vowel == 'o')||(vowel == 'u')){
			    	   vowTex = "an ";
			        }
			       dispTxt +=  vowTex + qualTxt + " in " + (String)pageContext.getAttribute("subDesc");
			     %>
			     <c:if test="${not empty recentSrchList.regionName}">
			      <c:set var="locDesc" value="${recentSrchList.regionName}" />
			     <% dispTxt += " in " +(String)pageContext.getAttribute("locDesc");%>
			     </c:if>
				<li class="dsktop"><a href="${recentSrchList.subjectUrl}" title="<%=dispTxt%>">${recentSrchList.subjectName}</a></li>
				<li class="mble"><a href="${recentSrchList.subjectUrl}" ><%=dispTxt %></a></li>
			  </c:forEach>
			</ul>
		</div>
		</div>
	  </c:if>
	</div>
	</c:if>
   </div>
   <div id="tab-2" class="univ_tab tab-content">
      <div class="crse_srch subsrse dis_inblk">
         <div class="tab_btm">
            <div class="sr_tab_form">
               <div class="land_inp_grp sr_cnty fl" id="uniDivId">
                  <div class="sr_country_cnr" id="uniSrchDivId">
                     <label for="uniSrchTpId" class="visualhid">Enter university name</label>
                     <input type="text" class="inptxt" name="uniSearchId" id="uniSearchId" onkeyup="ajaxList(event,'uniSearchId','ajax_listOfOptions_clearing','uniDivId')" placeholder="Enter university name" autocomplete="off">
                  </div>
               </div>
               <div class="err_field fl_w100" id="errMsgUniSrchlbox" style="display:none">
                  <p>Please select university from dropdown</p>
               </div>
            </div>
         </div>
         <spring:message code="find.uni.url" var="findUniUrl"/>
         <div class="advsrc_lnk fl_w100"> 
           <a href="${findUniUrl}">
             <spring:message code="view.unis.btn.name"/> <i class="fa fa-long-arrow-right"></i>
           </a> 
         </div>
      </div>
   </div>
</div>