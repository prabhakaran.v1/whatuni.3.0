<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<div class="fl_w100 ots_cnt" id="regForm">
  <h1 class="pln-sub-tit">Success!</h1>
  <p class="cnfrm f-16">
    <c:choose>
      <c:when test="${param.from_page eq 'ebook_landing'}">
        <spring:message code="pre.clearing.landing.page.success.msg"/>
      </c:when>
      <c:otherwise>
        <spring:message code="pre.clearing.landing.page.success.msg"/> <spring:message code="pre.clearing.success.page.success.msg"/>
      </c:otherwise>
   </c:choose>
  </p>
  <div class="sub_btn">
    <a onclick="GAInteractionEventTracking('downloadguidebutton', 'Lead Capture', 'Download Guide', 'Clicked');getEbookPdf();">DOWNLOAD<i class="fa fa-long-arrow-right"></i></a>
  </div>
</div>