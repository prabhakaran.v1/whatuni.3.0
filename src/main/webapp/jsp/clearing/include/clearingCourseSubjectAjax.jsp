<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>      
<div id="KW" class=""><span class="ajx_tit"><a id="keyWordSearch" href="">KEYWORD SEARCH FOR <span class="bld" id="freeText"></span></a></span>
</div>

<c:if test="${not empty subjectAjaxList}">
   <c:forEach var="subjectAjaxList" items="${subjectAjaxList}" varStatus="i">
      <c:set var="index" value="${i.index}"/>
      <div id="sub_${i.index}" onclick="ajaxDrpDownSelect('topClearingSubjectName', '${subjectAjaxList.description}','${subjectAjaxList.url}')" class="optionDiv">
         <span class="ajx_sub">
         <span id="categoryCode_${index}" style="display:none">${subjectAjaxList.url}</span> 
         <span class="ajx_rt">
         <span class="ajx_txt" id="subject_${i.index}"> ${subjectAjaxList.description} </span>
         </span>
         </span>
      </div>
   </c:forEach>
   <input type="hidden" id="keywordmatchBrowseCatId" value="${keywordmatchBrowseCatId}"/>   
</c:if>