<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="WUI.utilities.CommonUtil"%>
 <c:if test="${not empty scoreValue or not empty userUcasPoint}">
   <div class="fltr_hd stc_hd">
      <h2>Chance of acceptance</h2>
      <div class="new">NEW!</div>
      <p class="subtxt3">With the grades you have entered, show courses where the chance of acceptance is...</p>
   </div>
   <div class="slim-scroll ajax_ul">
      <!-- 1 pod end  -->
      <div class="fltr_wrp" id="acceptanceDiv">
         <h3 class="coa_ttl">Choose one or more</h3>
			<div class="fltr_box">
				<div class="fltr_tick">
					<div class="fltr_lft">
						<label class="coa_cont"> 
						<input type="checkbox" id="acceptance" data-filter-show="acceptance" data-acceptance-value="stretch"> 
						<span class="checkmark"></span>
						</label>
					</div>
					<div class="fltr_rit">
						<div class="wuclrsr_crsmtrtng">
							<div class="wuclrsr_crsmtrtnglft txtredcol">
								<span class="wuclrsr_crsmtrtngtxt">POSSIBLE</span> 
								<span class="wuclrsr_crsmtrtngbrdrset"> 
								<span class="wuclrsr_crsmtrtngbrd brdrgry"></span> 
								<span class="wuclrsr_crsmtrtngbrd brdrgry"></span> 
								<span class="wuclrsr_crsmtrtngbrd"></span> 
								<span class="wuclrsr_crsmtrtngbrd"></span> 
								<span class="wuclrsr_crsmtrtngbrd"></span>
								</span>
							</div>
						</div>
						<p class="subtxt">Include the courses that you don't meet the required UCAS points for, but you might still have a chance to be accepted onto.</p>
					</div>
				</div>
			</div>
			<div class="fltr_box">
				<div class="fltr_tick">
					<div class="fltr_lft">
						<label class="coa_cont"> 
						<input type="checkbox" id="acceptance" data-filter-show="acceptance" data-acceptance-value="possible"> 
						<span class="checkmark"></span>
						</label>
					</div>
					<div class="fltr_rit">
						<div class="wuclrsr_crsmtrtng">
							<div class="wuclrsr_crsmtrtnglft txtgrncol">
								<span class="wuclrsr_crsmtrtngtxt">LIKELY</span> 
								<span class="wuclrsr_crsmtrtngbrdrset"> 
								<span class="wuclrsr_crsmtrtngbrd brdrgry"></span> 
								<span class="wuclrsr_crsmtrtngbrd"></span> 
								<span class="wuclrsr_crsmtrtngbrd"></span> 
								<span class="wuclrsr_crsmtrtngbrd"></span> 
								<span class="wuclrsr_crsmtrtngbrd"></span>
								</span>
							</div>
						</div>
						<p class="subtxt">Include the courses you have a good chance to be accepted onto.</p>
					</div>
				</div>
			</div>
			<div class="fltr_box">
				<div class="fltr_tick">
					<div class="fltr_lft">
						<label class="coa_cont"> 
						<input type="checkbox" id="acceptance" data-filter-show="acceptance" data-acceptance-value="likely"> 
						<span class="checkmark"></span>
						</label>
					</div>
					<div class="fltr_rit">
						<div class="wuclrsr_crsmtrtng">
							<div class="wuclrsr_crsmtrtnglft txtgrncol">
								<span class="wuclrsr_crsmtrtngtxt">VERY LIKELY</span> 
								<span class="wuclrsr_crsmtrtngbrdrset"> 
								<span class="wuclrsr_crsmtrtngbrd"></span> 
								<span class="wuclrsr_crsmtrtngbrd"></span> 
								<span class="wuclrsr_crsmtrtngbrd"></span> 
								<span class="wuclrsr_crsmtrtngbrd"></span> 
								<span class="wuclrsr_crsmtrtngbrd"></span>
								</span>
							</div>
						</div>
						<p class="subtxt">Include the courses you have a very strong chance to be accepted onto.</p>
					</div>
				</div>
			</div>
      </div>
   </div>
</c:if>