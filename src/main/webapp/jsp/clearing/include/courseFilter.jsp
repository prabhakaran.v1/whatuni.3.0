<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="WUI.utilities.CommonUtil"%>
	  <div class="fltr_hd stc_hd"><h2>Course</h2></div>
	  <div class="slim-scroll ajax_ul">
	   <div class="fltr_wrp ">
		  <h3>Start-online courses</h3><div class="new">NEW!</div>
		  <p class="subtxt2">Include courses that start online then continue on-campus?</p>
		  <div class="fltr_box">
			<div class="fltr_levls">
			  <div class="grd_chips" id="hybridDiv">
				<a href="javascript:void(0);" data-filter-show="hybrid" data-hybrid-value="y"  class="${param.hybrid eq 'y' ? 'slct_chip' : ''}" data-hybrid-ga-value="start-online,Yes">Yes</a>
				<a href="javascript:void(0);" data-filter-show="hybrid" data-hybrid-value="n"  class="${param.hybrid eq 'n' ? 'slct_chip' : ''}" data-hybrid-ga-value="start-online,No">No</a>
			  </div>
			</div>
		  </div>
		</div>
		<c:if test="${not empty qualificationList}">
		<div class="fltr_wrp ">
		  <h3>Qualification</h3>
		  <p class="subtxt">Choose one</p>
		  <div class="fltr_box">
			<div class="fltr_levls">
			  <div class="grd_chips" id="qualificationDiv">
			    <c:forEach items="${qualificationList}" var="qualification" varStatus="ql">
				<a href="javascript:void(0);" id="qualification" data-filter-show="qualification" data-qualification-value="${qualification.qualTextKey}" class="${qualification.selectedFlag eq 'Y' ? 'slct_chip' : ''}" data-qualification-ga-value="study-level,${fn:toLowerCase(qualification.qualDisplayDesc)}" data-qualification-subject-name-url="${qualification.browseCatTextKey}">${qualification.qualDisplayDesc}</a>
				</c:forEach>
			  </div>
			</div>
		  </div>
		</div>
		</c:if>
		<c:if test="${not empty studyModeList}">  
	    <div class="fltr_wrp ">
		  <h3>Course type</h3>
		  <p class="subtxt">Choose one</p>
		  <div class="fltr_box">
		    <div class="fltr_levls">
			  <div class="grd_chips" id="studyModeDiv">
			    <c:forEach items="${studyModeList}" var="course" varStatus="sm">
			      <a href="javascript:void(0);" data-filter-show="studyMode" data-studyMode-value="${course.studyModeTextKey}" class="${course.selectedFlag eq 'Y' ? 'slct_chip' : ''}" data-studyMode-ga-value="study-mode,${fn:toLowerCase(course.displayStudyModeDesc)}">${course.displayStudyModeDesc}</a>
			    </c:forEach>
			  </div>
			</div>
		  </div>
		</div>
		</c:if>
	  </div>
