<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants,org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<% 
 CommonUtil util = new CommonUtil();
 CommonFunction common = new CommonFunction(); 
 String pageName = !GenericValidator.isBlankOrNull((String)request.getAttribute("pageName")) ? (String)request.getAttribute("pageName") : "";
%>
<!-- sidenav start -->
	<div class="clr_fltr" id="filterSlider" style="display:none">
		<div class="overlay"></div>
		<div class="sidenav" id="sidenavGf">
		  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><img src='<%=CommonUtil.getImgPath("/wu-cont/images/clr_fltr_close_gry.svg",0)%>' alt="close icon" width="14" height="14"></a>
    	  <div class="fltr_cntr" id="yourGradesBlock"></div>
    	</div>    	
    	<div class="btm_pod dsp_none" id="bottom_button_gf" style="display:none">
    	    <p class="err" id="errorMsgGrd" style="display:none"></p>
			<h3 id="ucasPoints" class="">YOUR UCAS POINTS <span id="ucasPoints_span">0</span></h3> 
			<div class="btm_btns"> 
				<span class="clear" id="clearGf">
					<a href="javascript:void(0);">Clear</a>
				</span><%if("mywhatuni".equalsIgnoreCase(pageName)){ %> 
				<span class="aply_btn" id="applyGf">
					<a href="javascript:void(0);" id = "myWhatuni" data-urlCheck = "myWhatuni">SAVE</a>
				</span>
				<%} else { %>
				  <span class="aply_btn" id="applyGf">
					<a href="javascript:void(0);">APPLY</a>
				  </span>
				<%}%>
			</div>
		</div>
	</div>
	<input type="hidden" id="filterOpenType">
	<input type="hidden" id="filterHitCount" value="0">
	<input type="hidden" id="studyLevelPreHidden" value="">
	<input type="hidden" id="filterPodName" value="">
	<input type="hidden" id="callPodName" value="">
	<input type="hidden" id="sesUcasPt" value="${sessionScope.USER_UCAS_SCORE}">
	<input type="hidden" id="callUserType" value="${sessionScope.USER_TYPE}">
	<input type="hidden" id="pageNameGf" value="${param.pageName}">
	<!-- sidenav end -->