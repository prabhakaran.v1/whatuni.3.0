<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonFunction,WUI.utilities.CommonUtil,WUI.utilities.GlobalConstants, WUI.utilities.SessionData, org.apache.commons.validator.GenericValidator" autoFlush="true" %>

<%
  CommonFunction commonFun = new CommonFunction();
  String pageno = request.getAttribute("pageno") != null ? (String)request.getAttribute("pageno") : "1";
  String urldata_1 = (String)request.getAttribute("URL_1");
  String urldata_2 = (String)request.getAttribute("URL_2");
  String urlString  = urldata_1 + pageno + urldata_2;
  String firstPageSeoUrl = request.getAttribute("SEO_FIRST_PAGE_URL") != null ? (String)request.getAttribute("SEO_FIRST_PAGE_URL") : "NULL";
  String searchhits = (String) request.getAttribute("universityCount");
  String paramlocationValue = (request.getAttribute("paramlocationValue") !=null && !request.getAttribute("paramlocationValue").equals("null") && String.valueOf(request.getAttribute("paramlocationValue")).trim().length()>0 ? String.valueOf(request.getAttribute("paramlocationValue")) : ""); 
  String resultExists = (request.getAttribute("resultExists") !=null && !request.getAttribute("resultExists").equals("null") && String.valueOf(request.getAttribute("resultExists")).trim().length()>0 ? String.valueOf(request.getAttribute("resultExists")) : ""); 
  String universityCount = (request.getAttribute("universityCount") !=null && !request.getAttribute("universityCount").equals("null") && String.valueOf(request.getAttribute("universityCount")).trim().length()>0 ? String.valueOf(request.getAttribute("universityCount")) : ""); 
  String seoLocationValue = (!GenericValidator.isBlankOrNull(paramlocationValue)) ? commonFun.replaceHypenWithSpace(paramlocationValue): "";  
  String paramSubjectCode = (request.getAttribute("paramSubjectCode") !=null && !request.getAttribute("paramSubjectCode").equals("null") && String.valueOf(request.getAttribute("paramSubjectCode")).trim().length()>0 ? String.valueOf(request.getAttribute("paramSubjectCode")) : "");  
  String paramStudyLevelId = (request.getAttribute("paramStudyLevelId") !=null && !request.getAttribute("paramStudyLevelId").equals("null") && String.valueOf(request.getAttribute("paramStudyLevelId")).trim().length()>0 ? String.valueOf(request.getAttribute("paramStudyLevelId")) : ""); 
  String noindexfollow = request.getAttribute("noindex") !=null ? "noindex,follow"  : "index,follow";
  String studyModeValue = (String) request.getAttribute("hitbox_study_mode");
  if(studyModeValue == null){studyModeValue = "";}
  CommonUtil comUtil = new CommonUtil();  
  String subjectDesc = request.getAttribute("subjectDesc") != null ? request.getAttribute("subjectDesc").toString() : "";   
  int articleLength = 0;
  if(subjectDesc!=null && subjectDesc!=""){
    articleLength = subjectDesc.length();
  }
  String articleText = subjectDesc;
  if(articleLength > 15){
    articleText = subjectDesc.substring(0,13) + "...";
  }
  String studyLevelDesc = request.getAttribute("studyLevelDesc") != null ? (String)request.getAttribute("studyLevelDesc") : ""; 
  String studeyLevel = studyLevelDesc;
  String studyLevelOnly = studyLevelDesc;
  String studyLevelTab = studyLevelDesc;
  if(studyLevelTab.indexOf("degree")==-1){
        studyLevelTab = studyLevelTab.indexOf("hnd/hnc") > -1 ? studyLevelTab.toUpperCase() :studyLevelTab;
        studyLevelTab = studyLevelTab + " degree";
  }
  if(!GenericValidator.isBlankOrNull(studeyLevel)){  //29-Oct-2013 SEO Release - Start of change            
     if(studeyLevel.indexOf("degree")==-1){
        studeyLevel = studeyLevel.indexOf("hnd/hnc") > -1 ? studeyLevel.toUpperCase() :studeyLevel;
        studeyLevel = studeyLevel + " degrees";
     }else{
        studeyLevel = studeyLevel.replaceFirst("degree", "degrees");
        }              
      } //29-Oct-2013 SEO Release - End of change 
  String first_mod_group_id = (String)request.getAttribute("first_mod_group_id");
         first_mod_group_id = (first_mod_group_id!=null && first_mod_group_id.trim().length() >0) ? first_mod_group_id : "";
  String totalCourseCount = (String) request.getAttribute("totalCourseCount"); 
         totalCourseCount = GenericValidator.isBlankOrNull(totalCourseCount)?"":totalCourseCount;
  String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
  String canonicalLink = httpStr + "www.whatuni.com/degrees" + firstPageSeoUrl;
  int totalCount = 0;
  if(totalCourseCount!=""){
    totalCount =  Integer.parseInt(totalCourseCount);
  }
  studyLevelTab = totalCount > 1? studyLevelTab+"s": studyLevelTab;
  String firstTabText = totalCourseCount + " " + studyLevelTab;
String newsearchJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.newSearchResults.js");

String pgsRelCanonicalLink = (String)request.getAttribute("pgsCanonicalURL");//13_MAY_2014_REL
if(!GenericValidator.isBlankOrNull(paramStudyLevelId)){
   canonicalLink = GenericValidator.isBlankOrNull(pgsRelCanonicalLink)?"":pgsRelCanonicalLink;
 }
String clearingYear = GlobalConstants.CLEARING_YEAR;
 String filter = commonFun.getWUSysVarValue("FILTER");
 String socialBoxJs = CommonUtil.getResourceMessage("wuni.social.box.js", null);
%>

<body>
  <!--Modified code for responsive redesign 03_Nov_2015 By S.Indumathi-->
  <div class="sr_resp clr_sr">
    <header class="clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>
    </header>
    <div class="ad_cnr">
      <div id="content-blk" class="content-bg">
        <div class="sr">
          <jsp:include page="/seopods/breadCrumbs.jsp" >
            <jsp:param name="pageName" value="CLEARING_BROWSE_MONEY_PAGE" />            
          </jsp:include>

          <%if(!"".equals(paramStudyLevelId) && "M".equalsIgnoreCase(paramStudyLevelId)){%>
             <div class="com">              
          <%}else{%>
           <div class="com nw">
          <%}%>
          <div class="com-lt">
            <div>
            <%-- <h1 class="result-head">
              <strong>
              <c:if test="${not empty requestScope.subjectDesc}">${requestScope.subjectDesc}</c:if><%=studeyLevel%> <div class="clr_fnt_clr">in Clearing & Adjustment </div><c:if test="${not empty requestScope.searchLocation}"><c:if test="${requestScope.searchLocation ne 'United Kingdom'}"> in ${requestScope.searchLocation}</c:if></c:if></strong>
              <span> 
              <c:if test="${not empty requestScope.universityCount}">${requestScope.universityCount} <c:if test="${requestScope.universityCount eq 1}">university</c:if><c:if test="${requestScope.universityCount gt 1}">universities</c:if></c:if> offer <c:if test="${not empty requestScope.totalCourseCount }"> <%=firstTabText%> </c:if>  including  <c:if test="${not empty requestScope.subjectDesc }">${requestScope.subjectDesc}</c:if></span></h1> --%>
              <%--<p class="cal_txt">(Calls to clearing hotlines are free from UK landlines or the standard network rate from mobile)</p>--%> <%--Added calls free text for clearing on 16_May_2017, By Thiyagu G--%>
              
              <%--<span class="dmtxt">Find <logic:present name="subjectDesc" scope="request"><bean:write name="subjectDesc" scope="request"/></logic:present> Clearing courses for 2014 below. Check fees, entry requirements and the number of places available, 
                  compare the courses, stats and rankings, then grab the Clearing hotline numbers and hit the phone!.</span>--%>
                  
                <h1 class="result-head reshd2"><c:if test="${not empty requestScope.subjectDesc}">${requestScope.subjectDesc}</c:if><%=studeyLevel%> <div class="clr_fnt_clr">in Clearing & Adjustment </div><c:if test="${not empty requestScope.searchLocation}"><c:if test="${requestScope.searchLocation ne 'United Kingdom'}"> in ${requestScope.searchLocation}</c:if></c:if></h1>
                <h2 class="result-head respar2"><c:if test="${not empty requestScope.universityCount}">${requestScope.universityCount} <c:if test="${requestScope.universityCount eq 1}">university</c:if><c:if test="${requestScope.universityCount gt 1}">universities</c:if></c:if> offer <c:if test="${not empty requestScope.totalCourseCount }"> <%=firstTabText%> </c:if>  including  <c:if test="${not empty requestScope.subjectDesc }">${requestScope.subjectDesc}</c:if></h2> 
                <jsp:include page="/jsp/clearing/clearingAndNormalSRURL.jsp"/> 
            </div>
            <jsp:include page="/search/include/didYouMeanPod.jsp" /> 
           </div>  
          <div class="com-rt">
            <%if(filter.equalsIgnoreCase("Y")){%>
               <%if(paramStudyLevelId.equalsIgnoreCase("m")){%>
               <a class="fq pers_srch_lbx" href="#" onclick="javascript:showPopupGradeFilter('popup-grade',650,500, '<%=paramStudyLevelId%>', '<%=urldata_2%>');">PERSONALISE YOUR SEARCH<i class="fa fa-long-arrow-right"></i></a><!-- Script for responsive view added by Prabha on 03_Nov_2015 -->
              <%}}%>
              <div class="fl_lr">
                <a href="#">
                  <span class="left"><i class="fa fa-filter"></i></span>
                  <span class="right">Filter results</span>
                </a>
              </div> 
            </div>          
          </div> 
         </div>
                <!-- Grasdes-->
                 <jsp:include page="/jsp/search/include/gradeFilter.jsp" >
                      <jsp:param name="PAGE_FROM" value="BROWSE_MONEY_PAGE"/>
                      <jsp:param name="URLDATA_2" value="<%=urldata_2%>"/>
                      <jsp:param name="searchPageType" value="CLEARING_SEARCH"/>
                  </jsp:include>
        
                <div class="sr-cont">
                <!-- Filter-->
                 <jsp:include page="/jsp/search/include/searchFilters.jsp" >
                     <jsp:param name="PAGE_FROM" value="BROWSE_MONEY_PAGE"/>
                     <jsp:param name="URLDATA_2" value="<%=urldata_2%>"/>
                     <jsp:param name="searchPageType" value="CLEARING"/>
                  </jsp:include>
                <!-- -->
                <div class="srs-rt">
                  <div class="lsthdr cf">
                    <jsp:include page="/jsp/search/include/newSortBy.jsp">                          
                      <jsp:param name="srchType" value="clearing"/>
                    </jsp:include>
                  </div>
                  
                  <jsp:include page="/jsp/search/include/newSearchResults.jsp">   
                    <jsp:param name="studyLevelModified" value="<%=studeyLevel%>"/>
                    <jsp:param name="studyLevelOnly" value="<%=studyLevelOnly%>"/> 
                    <jsp:param name="PAGE_FROM" value="BROWSE_MONEY_PAGE"/>
                     <jsp:param name="searchPageType" value="CLEARING"/>
                  </jsp:include>
              <div class="pr_pagn">      
              <% 
                 int numofpages = 0;
                 int noOfrecords = Integer.parseInt(searchhits.replaceAll(",", ""));
                     numofpages =  noOfrecords / 10;
                 if(noOfrecords % 10 > 0) {  numofpages++; }
                 String pageNameh = "/jsp/search/searchredesign/newPagination.jsp";
                 if(numofpages >1 && ("1").equals(pageno)){  
                   pageNameh = "/jsp/search/searchredesign/searchInitialPagination.jsp";
                  }%>
                    <jsp:include page="<%=pageNameh%>">
                    <jsp:param name="pageno" value="<%=pageno%>"/>
                    <jsp:param name="pagelimit"  value="10"/>
                    <jsp:param name="searchhits" value="<%=searchhits%>"/>
                    <jsp:param name="recorddisplay" value="10"/>
                    <jsp:param name="displayurl_1" value="<%=urldata_1%>"/>
                    <jsp:param name="displayurl_2" value="<%=urldata_2%>"/>
                    <jsp:param name="firstPageSeoUrl" value="<%=firstPageSeoUrl%>"/>
                    <jsp:param name="universityCount" value="<%=universityCount%>"/>
                    <jsp:param name="resultExists" value="<%=resultExists%>"/>
                  </jsp:include>
                </div>
                 
                </div>
              </div>
            </div>
          <%--(2)-- Search relsult info --%>  
          <form:form action="/courses/*-courses/*-courses-*/*/*/*/*/page" commandName="searchBean" >
            <input type="hidden" id="sortingUrl" value="<%=request.getAttribute("sortingUrl")%>" />
            <input type="hidden" id="subjectname" value="<%=request.getAttribute("subjectName")%>" />
            <input type="hidden" id="studyleveldesc" value="<%=request.getAttribute("studyLevelDesc")%>" />
            <input type="hidden" id="paramStudyLevelId" value="<%=request.getAttribute("paramStudyLevelId")%>" />
            <input type="hidden" id="paramSubjectCode" value="<%=request.getAttribute("paramSubjectCode")%>" />
            <input type="hidden" id="paramlocationValue" value="<%=request.getAttribute("paramlocationValue")%>" />
            <input type="hidden" id="dropdownCollegeId" value="uc" />
            <input type="hidden" id="userid" value='<%=new SessionData().getData(request, "y")%>' />
            <input type="hidden" name="contextPath" id="contextPath" value="/degrees" />
            <input type="hidden" id="pageName" value="moneyPage" />
            <input type="hidden" id="searchtype" name="searchType" value="CLEARING_SEARCH"/>
          </form:form>>   
         </div>   
    <jsp:include page="/jsp/common/wuFooter.jsp" />
    <jsp:include page="/jsp/common/socialBox.jsp" />
    <input type="hidden" name="hdrMenuHide" id="hdrMenuHide" value="READREVIEWS" />
    <input type="hidden" id="userid" value='<%=new SessionData().getData(request, "y")%>' />
</div>  
<script type="text/javascript">
loadFirstDelivery(<%=first_mod_group_id%>);
loadfirstArticle();
</script>
<!--Set cookie to stop grade filter popup within the session for 11_Aug_2015 By Thiyagu G.-->
<script type="text/javascript">
loadGradePopup();
</script>
</body>
