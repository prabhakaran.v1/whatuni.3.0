<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants,java.util.*"%>

<body>
   <div id="loadingImg" class="cmm_ldericn" style="display:none;"><img alt="wugo-loading-image" src="${loadingImgPath}"></div>
   <header class="clipart">
      <div class="ad_cnr">
         <div class="content-bg">
            <div id="desktop_hdr">
               <jsp:include page="/jsp/common/wuHeader.jsp" />
            </div>
         </div>
      </div>
   </header>
   <div class="container1 clr2020">
      <div class="clr_land fl_w100">
         <!--Hide Clearing search pod when post clearing ON-->
       <c:choose>
       <c:when test="${'ON' eq sessionScope.postClearingOnOff and 'OFF' eq sessionScope.wuClearingOnOff }">
         <jsp:include page="/jsp/clearing/include/postClearingRegisterPod.jsp" />
       </c:when>
       <c:otherwise>
         <jsp:include page="/jsp/clearing/include/clearingHomeSearch.jsp" />
       </c:otherwise>
        </c:choose>
        <c:if test="${'ON' eq sessionScope.wuClearingOnOff }">
           <div class="clr_hmid clr_funi grey_bg fl_w100">
             <div class="ad_cnr">
               <div class="content-bg">
                 <!-- Clearing popular Starts-->
                  <jsp:include page="/jsp/clearing/include/popularUnisClearingCourses.jsp" />
                  <!-- Clearing popular Ends -->
                </div>
             </div>
           </div>
         </c:if>
         <jsp:include page="/jsp/clearing/include/faqSection.jsp" />
      </div>
      <!-- Sponsor and Article Building Starts-->     
      <jsp:include page="/jsp/clearing/include/sponsorAndArticlePod.jsp" />
      <!-- Sponsor and Article Building Ends-->    
   </div>
   <%--Changed WU scheme as part of SSL work for 08_MAR_2016, By Thiyagu G.--%>        
   <%String browserip = GlobalConstants.WHATUNI_SCHEME_NAME + new CommonUtil().getProperties().getString("review.autocomplete.ip");%>
   <input type="hidden" id="refererUrl" name="refererUrl" value="<%=(request.getHeader("referer") !=null ? request.getHeader("referer") : browserip+"/degrees/home.html")%>" />
   <input type="hidden" id="contextPath" name="contextPath" value="/degrees" />
   <input type="hidden" id="extraPod" value="false"/>
   <input type="hidden" id="mblOrient"/>
   <input type="hidden" id="lightBoxResp" value=""/>
   <input type="hidden" id="clearingFlag" value="CLEARING"/>
   <input type="hidden" id="clearingHome" value="CLEARINGHOME"/>
   <input type="hidden" id="collegeIdValues" value="${collegeIdValues}"/>
   <input type="hidden" id="collegeNameValues" value="${collegeNameValues}"/>   
   <input type="hidden" id="orderItemIdHomeFeatured" value="${orderItemId}"/>   
   <jsp:include page="/jsp/common/wuFooter.jsp"/>
   <%-- ::START:: wu582_20181023 - Sabapathi: added script for stats logging on page load  --%>
   <script type="text/javascript">
      pageImpressionLog("", "CLHM");
   </script>
   <%-- ::END:: wu582_20181023 - Sabapathi: added script for stats logging on page load  --%>
</body>