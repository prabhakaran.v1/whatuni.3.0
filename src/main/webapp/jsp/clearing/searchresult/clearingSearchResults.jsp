<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%@page import="WUI.utilities.CommonUtil, WUI.utilities.SessionData"%>  

<body>
   <%--Start of filter pop up --%>
   <div class="clr_fltr" id="clr_fltr" style="display: none;">
      <div class="overlay"></div>
      <jsp:include page="/jsp/clearing/include/filterPopup.jsp" />
   </div>
   <%--End of clearing filter pop up --%>
   <div class="sr_resp">
      <!-- Header starts here -->
      <header class="md clipart">
         <div class="large"></div>
         <div class="ad_cnr wuclrsr_cntrblk">
            <div class="content-bg">
               <div id="desktop_hdr">
                  <jsp:include page="/jsp/common/wuHeader.jsp" />
               </div>
            </div>
         </div>
      </header>
      <!-- Header ends here -->
      <jsp:include page="/jsp/clearing/searchresult/include/headerSection.jsp" />
      <div class="ad_cnr">
         <div class="content-bg mh732" id="content-blk">
            <div class="sr">
      <c:if test="${COVID19_SYSVAR eq 'ON'}">
        <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
          <div class="covid_upt">
	        <span class="covid_txt"><%=new SessionData().getData(request, "COVID19_COURSE_TEXT")%></span>
	        <a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_blu_arw.svg", 0)%>"></a>
          </div>
      </c:if>
               <jsp:include page="/jsp/clearing/searchresult/include/featureUniPod.jsp" />
               <div class="wuclrsr_srtby fl_w100">
                  <div class="srt">
                     <ul class="cf">
                        <li class="sturat nw">
                           <a class="srt1">Sort by <img class="wuclrsr_imgicn" src="${domainPathImg}clr20_icn_dropdown_grey.svg" alt="Plus Icon"> </a>
                           <ul id="sortBy" class="strt">
                              <li class="${fn:toUpperCase(sortBy) eq 'R' ? 'active' : ''}"><a rel="nofollow" data-sort-show="mostInfo" data-mostInfo-value="r" onclick="searchEventTracking('sort','mostinfo','clicked');">Most info</a></li>
                              <li class="${fn:toUpperCase(sortBy) eq 'ENTD' ? 'active' : ''}"><a rel="nofollow" data-sort-show="UCASD" data-UCASD-value="entd" onclick="searchEventTracking('sort','ucas-points-high-to-low','clicked');">UCAS Points (high to low)</a></li>
                              <li class="${fn:toUpperCase(sortBy) eq 'ENTA' ? 'active' : ''}"><a rel="nofollow" data-sort-show="UCASA" data-UCASA-value="enta" onclick="searchEventTracking('sort','ucas-points-low-to-high','clicked');">UCAS Points (low to high)</a></li>
                              <li class="${fn:toUpperCase(sortBy) eq 'URH' ? 'active' : ''}"><a rel="nofollow" data-sort-show="uniRank" data-uniRank-value="urh" onclick="searchEventTracking('sort','cug-university-ranking','clicked');">University League Table (Highest First)</a></li>
                              <li class="${fn:toUpperCase(sortBy) eq 'CRH' ? 'active' : ''}"><a rel="nofollow" data-sort-show="subRank" data-subRank-value="crh" onclick="searchEventTracking('sort','cug-subject-ranking','clicked');">Subject League Table (Highest First)</a></li>
                              <li class="${fn:toUpperCase(sortBy) eq 'R-OR' ? 'active' : ''}"><a rel="nofollow" data-sort-show="rating" data-rating-value="r-or" onclick="searchEventTracking('sort','student-rating','clicked');">Student rating</a></li>
                           </ul>
                        </li>
                     </ul>
                  </div>
                  <input type="hidden" id="sortHidden" value="${sortBy}" />
               </div>
               <jsp:include page="/jsp/clearing/searchresult/include/clearingSRMiddleContent.jsp" />
            </div>
         </div>
      </div>
      <input type="hidden" id="check_mobile_hidden" value="${mobileFlag}">
      <input type="hidden" id="instIds" name="instIds" value="${instIds}" />
      <input type="hidden" id="instNames" name="instNames" value="${instNames}" />
      <input type="hidden" id="qualificationInSR" name="qualification1" value="${qualification1}" />
      <input type="hidden" id="pageNum" value="<%=request.getAttribute("pageno")%>" />
      <input type="hidden" id="eCommPageName" value="Search Results Page" />
      <input type="hidden" id="GTMLDCS" value="${cDimLDCS}" />
      <input type="hidden" id="subjectL1" value="${subjectL1}" />
      <input type="hidden" id="gtmJsonDataSR" name="gtmJsonDataSR"/>
      <input type="hidden" id="userType" name="userType" value="${sessionScope.USER_TYPE}" />
      <input type="hidden" id="browseCatCodeFeatured" value="${browseCatCode}" />
      <input type="hidden" id="subjectFeatured" value="${subject}" /> 
      <input type="hidden" id="qualificationFeatured" value="${qualification1}" />
      <jsp:include page="/jsp/common/wuFooter.jsp" />
      <jsp:include page="/jsp/common/eCommerceTracking.jsp" />
      <%-- <jsp:include page="/jsp/clearing/include/gradeFilterPopupLayout.jsp">                          
         <jsp:param name="pageName" value="clearingSearchResults.jsp"/>
         </jsp:include> --%>
   </div>
</body>