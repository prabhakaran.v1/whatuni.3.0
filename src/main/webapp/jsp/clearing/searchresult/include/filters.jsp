<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil"%>
<% 
   String userUcasPoint = (String)session.getAttribute("USER_UCAS_SCORE");
   pageContext.setAttribute("userUcasPoint", userUcasPoint);
   %> 
<div class="wuclrsr_rsltbtnset" id="filterOptionBlock">
   <a class="wuclr_btn btnbrdr btnyugrd ${not empty userUcasPoint or not empty scoreValue ? 'btnbg':'' }" id="gradeOption" data-filter-option="yourGrades" title="Your grades" href="javascript:void(0)">
      YOUR GRADES 
      <c:choose>
         <c:when test="${not empty userUcasPoint or not empty scoreValue}">
            <img class="wuclrsr_imgicn" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_tick_white.svg",0)%>" alt="white tick Icon">
         </c:when>
         <c:otherwise>
            <img class="wuclrsr_imgicn wuclrsr_imgicn_hovr" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_white.svg",0)%>" alt="whilte Plus Icon"/>
            <img class="wuclrsr_imgicn wuclrsr_imgicn_nrml" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_blue.svg",0)%>" alt="blue Plus Icon"/>
         </c:otherwise>
      </c:choose>
      <c:if test="${empty userUcasPoint and empty scoreValue}">
         <span class="yugrd_tipbox" id="gradeOptionToolTip">
            <span class="yugrd_tipcollft">
               <span class="yugrd_lftarrw"></span>
               <span class="request-loader1">
                <span></span>
               </span>
            </span>
            <span class="yugrd_tipcolrgt">
               <img class="wuclrsr_imgicn" id="closeGradeOption" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_close_grey.svg",0)%>" alt="Close Icon">
               <h4>Add your grades</h4>
               <p>Find out your chance of being accepted onto a course</p>
            </span>
         </span>
      </c:if>
   </a>
   <c:if test="${not empty qualificationList}">
      <a class="wuclr_btn btnbrdr btnyugrd ${not empty selectedQual ? 'btnbg': '' }" title="Course" data-filter-option="courseFilter">
         Course 
         <c:choose>
            <c:when test="${not empty selectedQual}">
               <img class="wuclrsr_imgicn" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_tick_white.svg",0)%>" alt="white tick Icon">
            </c:when>
            <c:otherwise>
               <img class="wuclrsr_imgicn wuclrsr_imgicn_hovr" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_white.svg",0)%>" alt="whilte Plus Icon"/>
               <img class="wuclrsr_imgicn wuclrsr_imgicn_nrml" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_blue.svg",0)%>" alt="blue Plus Icon"/>
            </c:otherwise>
         </c:choose>
      </a>
   </c:if>
   <c:if test="${not empty regionList or not empty uniLocationTypeList}">
      <a class="wuclr_btn btnbrdr btnyugrd ${not empty selectedRegion or not empty selectedLocType ? 'btnbg': '' }" title="Location" data-filter-option="locationFilter">
         Location 
         <c:choose>
            <c:when test="${not empty selectedRegion or not empty selectedLocType}">
               <img class="wuclrsr_imgicn" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_tick_white.svg",0)%>" alt="white tick Icon">
            </c:when>
            <c:otherwise>
               <img class="wuclrsr_imgicn wuclrsr_imgicn_hovr" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_white.svg",0)%>" alt="whilte Plus Icon"/>
               <img class="wuclrsr_imgicn wuclrsr_imgicn_nrml" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_blue.svg",0)%>" alt="blue Plus Icon"/>
            </c:otherwise>
         </c:choose>
      </a>
   </c:if>
   <a class="wuclr_btn btnbrdr btnyugrd ${accommodationFlag eq 'Y' or accommodationFlag eq 'y' or scholarshipFlag eq 'y' or scholarshipFlag eq 'Y' or not empty campusType ? 'btnbg': '' }" title="University" data-filter-option="universityFilter">
      University 
      <c:choose>
         <c:when test="${accommodationFlag eq 'Y' or scholarshipFlag eq 'Y' or accommodationFlag eq 'y' or scholarshipFlag eq 'y' or not empty campusType}">
            <img class="wuclrsr_imgicn" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_tick_white.svg",0)%>" alt="white tick Icon">
         </c:when>
         <c:otherwise>
            <img class="wuclrsr_imgicn wuclrsr_imgicn_hovr" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_white.svg",0)%>" alt="whilte Plus Icon"/>
            <img class="wuclrsr_imgicn wuclrsr_imgicn_nrml" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_blue.svg",0)%>" alt="blue Plus Icon"/>
         </c:otherwise>
      </c:choose>
   </a>
   <a class="wuclr_btn btnbrdr btnchnaccp ${not empty acceptanceFlag ? 'btnbg': '' }" title="Chance of acceptance" id="chanceOfAcceptanceId" data-filter-option="acceptanceFilter">
      Chance of acceptance 
      <c:choose>
         <c:when test="${not empty acceptanceFlag}">
            <img class="wuclrsr_imgicn" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_tick_white.svg",0)%>" alt="white tick Icon">
         </c:when>
         <c:otherwise>
            <img class="wuclrsr_imgicn wuclrsr_imgicn_hovr" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_white.svg",0)%>" alt="whilte Plus Icon"/>
            <img class="wuclrsr_imgicn wuclrsr_imgicn_nrml" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_plus_blue.svg",0)%>" alt="blue Plus Icon"/>
         </c:otherwise>
      </c:choose>
      <c:if test="${empty scoreValue}">
         <span class="yugrd_tipbox" id="chanceofAccepatnceTooltip">
            <span class="yugrd_tipcollft">
               <span class="yugrd_lftarrw"></span>
               <span class="request-loader2">
                <span></span>  
               </span>
            </span>
            <span class="yugrd_tipcolrgt">
               <img class="wuclrsr_imgicn" id="closeChanceOfAcceptance" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_close_grey.svg",0)%>" alt="Close Icon">
               <h4>Add your grades</h4>
               <p>It'll show how likely you are to be accepted for a course.</p>
               <p><span class="btn_addgrds" data-filter-option="yourGrades">Add your grades</span></p>
            </span>
         </span>
      </c:if>
   </a>
   <c:if test="${not empty acceptanceFlag or not empty selectedStudyMode or not empty selectedRegion or not empty selectedLocType or accommodationFlag eq 'y' or scholarshipFlag eq 'y' or accommodationFlag eq 'Y' or scholarshipFlag eq 'Y' or not empty campusType or not empty scoreValue}">
      <a class="wuclr_lnk lnkclr" title="Clear all" data-filter-option="clearAllFilter"><img class="wuclrsr_imgicn" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr20_icn_close_grey.svg",0)%>" alt="Close Icon"> Clear all</a>
   </c:if>
   <input type="hidden" id="userUcasPoint" value="${userUcasPoint}" />
   <input type="hidden" id="scoreValue" value="${scoreValue}" />
   
</div>