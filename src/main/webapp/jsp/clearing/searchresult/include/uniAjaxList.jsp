<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:if test="${not empty uniAjaxList}">

		<ul id="ajaxUniDropDown" class="cstm_drp ajax_ul" style="overflow: hidden; width: auto; display: none;">
			<c:forEach var="uniAjaxList" items="${uniAjaxList}">
				<li id="uniDropDown_${uniAjaxList.collegeId}" data-filter-show="university" data-university-value="${fn:toLowerCase(uniAjaxList.providerTextKey)}" data-university-ga-value="${fn:toLowerCase(uniAjaxList.description)}">
				  <a onclick="javascript:void(0);"> ${uniAjaxList.description} </a>
				</li>
			</c:forEach>
		</ul>
		<!-- <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 5px; height: 106.667px;"></div>
		<div class="slimScrollRail" onclick="clickScroll(this,event);" style="width: 7px; height: 100%; position: absolute; top: 0px; display: block; border-radius: 5px; background: rgb(226, 226, 226); z-index: 90; right: 6px;"></div> -->
</c:if>