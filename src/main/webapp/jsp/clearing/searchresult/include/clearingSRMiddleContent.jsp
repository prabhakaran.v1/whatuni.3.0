<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<jsp:include page="/jsp/search/include/searchGoogleAdSlots.jsp"/>
<c:if test="${not empty searchResultsList}">
   <c:forEach var="searchResultsList" items="${searchResultsList}" varStatus="i">
      <c:set var="index" value="${i.index}"/>
      <div class="wuclrsr_colrow">
         <div class="wuclrsr_uniinf">
            <div class="wuclrsr_unittldet">
               <div class="wuclrsr_unittl_lft">
                  <c:if test="${not empty searchResultsList.stdAdvertType and searchResultsList.stdAdvertType eq 'SPONSORED_LISTING'}">
                     <span class="wuclrsr_spnstip">
                     <span class="tooltip_cnr ttip_cnt">
                     <span class="fad" id="${searchResultsList.stdAdvertType}" data-id="stats_not_logged">SPONSORED</span>
                     </span>
                     </span>
                  </c:if>
                  <h3><a href="${domainPath}${searchResultsList.uniProfileUrl}" onclick="sponsoredListGALogging('${searchResultsList.collegeId}');gtmproductClick(${index}, '', '${searchResultsList.collegeId}');">${searchResultsList.collegeDisplayName}</a></h3>
                  <p><a href="${domainPath}${searchResultsList.collegePRUrl}" class="wuclrsr_crscnt" onclick="sponsoredListGALogging('${searchResultsList.collegeId}');gtmproductClick(${index}, 'sr_to_pr', '${searchResultsList.collegeId}');">${searchResultsList.hits} ${dispSubjectName} degrees</a></p>
               </div>
               <div class="wuclrsr_unittl_rgt">
                  <div class="wuclrsr_lgo">
                     <c:choose>
                        <c:when test="${(i.index) >= 1}">
                           <img src="${img_px_gif}" data-src="${domainPath}${searchResultsList.collegeLogoPath}" width="64" title="${searchResultsList.collegeDisplayName}" class="ibdr" alt="${searchResultsList.collegeDisplayName}">
                        </c:when>
                        <c:otherwise>
                           <img src="${domainPath}${searchResultsList.collegeLogoPath}" width="64" title="${searchResultsList.collegeDisplayName}" class="ibdr" alt="${searchResultsList.collegeDisplayName}">                         
                        </c:otherwise>
                     </c:choose>
                  </div>
               </div>
            </div>
            <div class="wuclrsr_unirvwdet">
               <ul class="strat cf">
                  <c:if test="${not empty searchResultsList.rating and searchResultsList.rating ge 1}">
                     <li>
                        <span class="rat${searchResultsList.rating} t_tip">
                           <span class="cmp">
                              <div class="hdf5">This is the overall rating calculated by averaging all live reviews for this uni on
                                 Whatuni.
                              </div>
                              <div class="line"></div>
                           </span>
                        </span>
                        <span class="rtx">(${searchResultsList.exactRating})</span>
                        <c:if test="${not empty searchResultsList.reviewCount and searchResultsList.reviewCount ge 1}">
                           <a class="link_blue" href="${domainPath}${searchResultsList.reviewUrl}">
                           ${searchResultsList.reviewCount}
                           ${(searchResultsList.reviewCount gt 1) ? 'reviews' : (searchResultsList.reviewCount eq 1) ? 'review' : ''}
                           </a>
                        </c:if>
                     </li>
                  </c:if>
                  <c:if test="${searchResultsList.accommodationAvailable eq 'Y'}">
                     <li>
                        <img class="wuclrsr_imgicn" src="${domainPathImg}clr20_icn_tick_green.svg" alt="Tick Icon"> 
                        <spring:message code="accommodation.available.label" />
                     </li>
                  </c:if>
                  <c:if test="${searchResultsList.scholarshipAvailable eq 'Y'}">
                     <li>
                        <img class="wuclrsr_imgicn" src="${domainPathImg}clr20_icn_tick_green.svg" alt="Tick Icon"> 
                        <spring:message code="scholarships.available.label" />
                     </li>
                  </c:if>
               </ul>
            </div>
            <c:if test="${not empty searchResultsList.courseDetailsList}">
               <c:forEach var="courseDetailsList" items="${searchResultsList.courseDetailsList}" varStatus="j">
                  <c:set var="innerIndex" value="${j.index}"/>
                  <c:set var="indicatorClass" value="${courseDetailsList.scoreLevel eq 'LIKELY' ? 'txtgrncol' : courseDetailsList.scoreLevel eq 'STRETCH' ? 'txtorngcol' : 'txtgrncol' }"></c:set>
                  <div class="wuclrsr_crslstrow">
                     <h4><a href="${domainPath}${courseDetailsList.courseDetailPageUrl}" onclick="sponsoredListGALogging('${searchResultsList.collegeId}');gtmproductClick(${index}, 'sr_to_cd', '${searchResultsList.collegeId}');" id="sr_to_cd_${index}">${courseDetailsList.courseTitle}</a></h4>
                     <div class="wuclrsr_crslstmta">
                        <c:if test="${not empty courseDetailsList.scoreLabel}">
                           <div class="wuclrsr_crsmtrtng">
                              <div class="wuclrsr_crsmtrtnglft ${indicatorClass}">
                                 <span class="wuclrsr_crsmtrtngtxt">${courseDetailsList.scoreLabel}</span>
                                 <span class="wuclrsr_crsmtrtngbrdrset">
                                 <span class="wuclrsr_crsmtrtngbrd ${courseDetailsList.scoreLevel ne 'LIKELY' ? 'brdrgry' : ''}"></span>
                                 <span class="wuclrsr_crsmtrtngbrd ${courseDetailsList.scoreLevel eq 'STRETCH' ? 'brdrgry' : ''}"></span>
                                 <span class="wuclrsr_crsmtrtngbrd"></span>
                                 <span class="wuclrsr_crsmtrtngbrd"></span>
                                 <span class="wuclrsr_crsmtrtngbrd"></span>
                                 </span>
                              </div>
                              <div class="wuclrsr_crsmtrtngrtg">
                                 <span class="tooltip_cnr ttip_cnt">
                                    <span class="tool_tip fl">
                                       <img class="wuclrsr_imgicn" src="${domainPathImg}clr20_icn_info_circle.svg" alt="Info Icon">
                                       <span class="cmp">
                                          <div class="hdf5"></div>
                                          <div>${courseDetailsList.scoreTooltip}</div>
                                          <div class="line"></div>
                                       </span>
                                    </span>
                                 </span>
                              </div>
                           </div>
                        </c:if>
                        <div class="wuclrsr_crsmtboxrow">
                           <ul class="wuclrsr_ucsinfcol">
                              <c:if test="${not empty courseDetailsList.entryPoints}">
                                 <li class="wuclrsr_ucspnt">
                                    <span>UCAS points</span>
                                    <span>${courseDetailsList.entryPoints}</span>
                                 </li>
                              </c:if>
                              <c:if test="${not empty courseDetailsList.ucasCode}">
                                 <li class="wuclrsr_ucscde">
                                    <span>UCAS code</span>
                                    <span>${courseDetailsList.ucasCode}</span>
                                 </li>
                              </c:if>
                              <li class="wuclrsr_nxtsdte">
                                 <c:if test="${not empty courseDetailsList.startDate}">
                                    <span class="wuclrsr_nxtsdtelft">
                                    <span>Start date</span>
                                    <span>${courseDetailsList.startDate}</span>
                                    </span>
                                 </c:if>
                                 <c:if test="${not empty courseDetailsList.tagLine}">
                                    <span class="wuclrsr_nxtsdtergt">
                                    <span>${courseDetailsList.tagLine}</span>
                                    </span> 
                                 </c:if>
                              </li>
                           </ul>
                           <ul class="wuclrsr_crsboxbtnset ${empty courseDetailsList.hotLineNo ? 'vwonlybtn' : ''}">
                              <c:if test="${not empty courseDetailsList.website}">
                                 <li class="wuclrsr_vstbtn">
                                    <c:choose>
                                       <c:when test="${not empty searchResultsList.stdAdvertType and searchResultsList.stdAdvertType eq 'SPONSORED_LISTING'}">
                                          <a href="${courseDetailsList.website}&courseid=${courseDetailsList.courseId}" target="_blank" onclick="sponsoredListGALogging('${searchResultsList.collegeId}');gtmproductClick(${index}, '', '${searchResultsList.collegeId}');matchTypeGALogging('${courseDetailsList.courseId}');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${searchResultsList.gaCollegeName}', '');cpeWebClickClearingSponsored(this,'${searchResultsList.collegeId}', '', '','${courseDetailsList.networkId}','${courseDetailsList.website}','${courseDetailsList.orderItemId}');">
                                             <spring:message code="visit.website.label" />
                                          </a>
                                       </c:when>
                                       <c:when test="${not empty searchResultsList.stdAdvertType and searchResultsList.stdAdvertType eq 'MANUAL_BOOSTING'}">
                                          <a href="${courseDetailsList.website}&courseid=${courseDetailsList.courseId}" target="_blank" onclick="sponsoredListGALogging('${searchResultsList.collegeId}');gtmproductClick(${index}, '', '${searchResultsList.collegeId}');matchTypeGALogging('${courseDetailsList.courseId}');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${searchResultsList.gaCollegeName}', '');cpeWebClickClearingWithCourse(this,'${searchResultsList.collegeId}', '${courseDetailsList.courseId}', '${courseDetailsList.subOrderItemId}','${courseDetailsList.networkId}','${courseDetailsList.website}','');">
                                             <spring:message code="visit.website.label" />
                                          </a>
                                       </c:when>
                                       <c:otherwise>
                                          <a href="${courseDetailsList.website}&courseid=${courseDetailsList.courseId}" target="_blank" onclick="sponsoredListGALogging('${searchResultsList.collegeId}');gtmproductClick(${index}, '', '${searchResultsList.collegeId}');matchTypeGALogging('${courseDetailsList.courseId}');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${searchResultsList.gaCollegeName}', '');cpeWebClickClearingWithCourse(this,'${searchResultsList.collegeId}', '${courseDetailsList.courseId}', '${courseDetailsList.subOrderItemId}','${courseDetailsList.networkId}','${courseDetailsList.website}','');">
                                             <spring:message code="visit.website.label" />
                                          </a>
                                       </c:otherwise>
                                    </c:choose>
                                 </li>
                              </c:if>
                              <c:if test="${not empty courseDetailsList.hotLineNo}">
                                 <li class="wuclrsr_calbtn">
                                    <c:choose>
                                       <c:when test="${not empty searchResultsList.stdAdvertType and searchResultsList.stdAdvertType eq 'SPONSORED_LISTING'}">
                                          <a id="contact_${index}${innerIndex}" onclick="sponsoredListGALogging('${searchResultsList.collegeId}');gtmproductClick(${index}, '', '${searchResultsList.collegeId}');matchTypeGALogging('${courseDetailsList.courseId}');GAInteractionEventTracking('Webclick', 'interaction', 'hotline','${searchResultsList.gaCollegeName}');cpeHotlineClearingSponsored(this,'${searchResultsList.collegeId}','','${courseDetailsList.networkId}','${courseDetailsList.hotLineNo}','${courseDetailsList.orderItemId}');changeContact('${courseDetailsList.hotLineNo}','contact_${index}${innerIndex}');" >
                                             <i class="fa fa-phone" aria-hidden="true"></i>
                                             <spring:message code="call.now.label" />
                                          </a>
                                       </c:when>
                                       <c:when test="${not empty searchResultsList.stdAdvertType and searchResultsList.stdAdvertType eq 'MANUAL_BOOSTING'}">
                                          <a id="contact_${index}${innerIndex}" onclick="sponsoredListGALogging('${searchResultsList.collegeId}');gtmproductClick(${index}, '', '${searchResultsList.collegeId}');matchTypeGALogging('${courseDetailsList.courseId}');GAInteractionEventTracking('Webclick', 'interaction', 'hotline','${searchResultsList.gaCollegeName}');cpeHotlineClearing(this,'${searchResultsList.collegeId}','${courseDetailsList.subOrderItemId}','${courseDetailsList.networkId}','${courseDetailsList.hotLineNo}','');changeContact('${courseDetailsList.hotLineNo}','contact_${index}${innerIndex}');" >
                                             <i class="fa fa-phone" aria-hidden="true"></i>
                                             <spring:message code="call.now.label" />
                                          </a>
                                       </c:when>
                                       <c:otherwise>
                                          <a id="contact_${index}${innerIndex}" onclick="sponsoredListGALogging('${searchResultsList.collegeId}');gtmproductClick(${index}, '', '${searchResultsList.collegeId}');matchTypeGALogging('${courseDetailsList.courseId}');GAInteractionEventTracking('Webclick', 'interaction', 'hotline','${searchResultsList.gaCollegeName}');cpeHotlineClearing(this,'${searchResultsList.collegeId}','${courseDetailsList.subOrderItemId}','${courseDetailsList.networkId}','${courseDetailsList.hotLineNo}','');changeContact('${courseDetailsList.hotLineNo}','contact_${index}${innerIndex}');" >
                                             <i class="fa fa-phone" aria-hidden="true"></i>
                                             <spring:message code="call.now.label" />
                                          </a>
                                       </c:otherwise>
                                    </c:choose>
                                 </li>
                              </c:if>
                           </ul>
                        </div>
                     </div>
                     <c:if test="${not empty searchResultsList.stdAdvertType and searchResultsList.stdAdvertType eq 'SPONSORED_LISTING' and innerIndex eq 0}">
                        <input type="hidden" id="collegeIdSponsored" value="${searchResultsList.collegeId}"/>
                        <input type="hidden" id="collegeNameSponsored" value="${searchResultsList.collegeName}"/> 
                        <input type="hidden" id="subOrderItemIdSponsored" value="${courseDetailsList.subOrderItemId}"/> 
                        <input type="hidden" id="orderItemIdSponsored" value="${courseDetailsList.orderItemId}"/>         
                     </c:if>
                  </div>
               </c:forEach>
            </c:if>
            <c:if test="${searchResultsList.hits gt 3}">
               <div class="wuclrsr_vwallrellnk">
                  <a href="${domainPath}${searchResultsList.collegePRUrl}" onclick="sponsoredListGALogging('${searchResultsList.collegeId}');gtmproductClick('${index}', 'sr_to_pr_courses_available', '${searchResultsList.collegeId}');">View all ${searchResultsList.hits} related courses <img src="${domainPathImg}covid_blu_arw.svg"></a>               
               </div>
            </c:if>
         </div>
      </div>
      <c:set var="position" value="${i.index}"/>
      <jsp:include page="/jsp/search/include/searchDrawAdSlots.jsp">
         <jsp:param name="fromPage" value="searchresults"/>
         <jsp:param name="position" value="${position}"/>
         <jsp:param name="totalResults" value="${universityCount}"/>
      </jsp:include>
   </c:forEach>
   <input type="hidden" id="matchTypeDim" value="${matchTypeDim}" />
   <input type="hidden" id="uniMatchTypeMap" value="${uniMatchTypeMap}" />
   <input type="hidden" id="courseMatchTypeMap" value="${courseMatchTypeMap}" />
   <div class="wuclrsr_pgnrow">
      <div class="pr_pagn">
         <jsp:include page="${paginationPageName eq 'newPagination' ? '/jsp/search/searchredesign/newPagination.jsp' : '/jsp/search/searchredesign/searchInitialPagination.jsp'}">
            <jsp:param name="pageno" value="${pageNo}"/>
            <jsp:param name="pagelimit"  value="10"/>
            <jsp:param name="searchhits" value="${recordCount}"/>
            <jsp:param name="recorddisplay" value="10"/>
            <jsp:param name="displayurl_1" value="${URL_1}"/>
            <jsp:param name="displayurl_2" value="${URL_2}"/>
            <jsp:param name="firstPageSeoUrl" value="${firstPageSeoUrl}"/>
            <jsp:param name="universityCount" value="${recordCount}"/>
            <jsp:param name="resultExists" value="${resultExists}"/>
         </jsp:include>
      </div>
   </div>
</c:if>