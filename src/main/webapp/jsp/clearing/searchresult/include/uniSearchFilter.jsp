<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil"%>
<div class="fltr_cntr">
			<div class="fltr_hd stc_hd"><h2>University</h2></div>

			<div class="fltr_wrp">
				<h3>University name</h3>
				<div class="fltr_box">
					<div class="fltr_levls" id="universityDiv">
						<div class="flrt_src">
							<div class="uni_inpt">
								<input type="text" id="ajaxUniversityName" placeholder="University name" autocomplete="off" name="ajaxUniversityName">
								<a href="javascript:void(0);" id="uni_srch_icon"> <span class="srch"></span></a>
							</div>							
							<div id="ajax_listOfOptions_university"></div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="fltr_wrp">
				<h3>Campus type</h3>
				<div class="fltr_box">
					<div class="fltr_levls" id="campusDiv">
						<div class="grd_chips">
							<a href="javascript:void(0);" data-filter-show="campus" data-campus-value="campus" class="${campusType eq 'campus' ? 'slct_chip' : ''}" data-campus-ga-value="campus-type,campus">Campus universities</a>
							<a href="javascript:void(0);" data-filter-show="campus"  data-campus-value="non-campus" class="${campusType eq 'non-campus' ? 'slct_chip' : ''}" data-campus-ga-value="campus-type,non-campus">Non-Campus universities</a>
						</div>
					</div>
				</div>
			</div>
			
			<div class="fltr_wrp">
				<div class="fltr_swt">
					<div class="fltr_rit">
						<h3>Scholarships</h3>
						<p class="subtxt">Show only universities offering scholarships for students who go through Clearing.</p>
					</div>
					<div class="fltr_lft swt" id="scholarshipDiv">
						<label class="switch">
						  <input type="checkbox" id="scholarship" class="${scholarshipFlag eq 'y' ? 'slct_chip' : ''}" data-filter-show="scholarship" data-scholarship-value="" ${scholarshipFlag eq 'y' ? 'checked' : ''}>
						  <span class="slider round"></span>
						</label>
					</div>
				</div>
				<div class="fltr_swt">
					<div class="fltr_rit">
						<h3>Accommodation</h3>
						<p class="subtxt">Show only universities with accommodation available for Clearing students.</p>
					</div>
					<div class="fltr_lft swt" id="accommodationDiv">
						<label class="switch">
						  <input type="checkbox" id="accommodation" class="${accommodationFlag eq 'y' ? 'slct_chip' : ''}" data-filter-show="accommodation" data-accommodation-value="" ${accommodationFlag eq 'y' ? 'checked' : ''}>
						  <span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
		</div>