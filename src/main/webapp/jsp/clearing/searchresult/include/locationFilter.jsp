<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="fltr_hd stc_hd"><h2>Location</h2></div>
  <div class="slim-scroll ajax_ul">
	    <c:if test="${not empty regionList}">
	    <div class="fltr_wrp">
		  <h3>Region</h3>
		  <p class="subtxt">Choose one</p>
		  <div class="fltr_box ">
			<div class="fltr_levls">
			  <div class="grd_chips" id="regionDiv">
			    <c:forEach items="${regionList}" var="region" varStatus="rl">
				<a href="javascript:void(0);" data-filter-show="region" data-region-value="${region.regionTextKey}" class="${region.selectedFlag eq 'Y' ? 'slct_chip' : ''}" data-region-ga-value="location,${fn:toLowerCase(region.regionName)}">${region.regionName}</a>
				</c:forEach>
			  </div>
			</div>
		  </div>
		</div>
	  </c:if>
		<c:if test="${not empty uniLocationTypeList}">
		<div class="fltr_wrp">
		  <h3>Location type</h3>
		  <p class="subtxt">Choose one or more</p>
		  <div class="fltr_box">
			<div class="fltr_levls">
			  <div class="grd_chips" id="locationTypeDiv">
			    <c:forEach items="${uniLocationTypeList}" var="locType" varStatus="lt">
			      <a href="javascript:void(0);" data-filter-show="locationType" data-locationType-value="${locType.locTypeTextKey}" class="${locType.selectedFlag eq 'Y' ? 'slct_chip' : ''}" data-locationType-ga-value="location-type,${fn:toLowerCase(locType.locTypeDesc)}">${locType.locTypeDesc}</a>				
			    </c:forEach>
			  </div>
			</div>
		  </div>
		</div>
		</c:if>
      </div>
 