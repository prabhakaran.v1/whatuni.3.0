<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.SessionData,WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, mobile.util.MobileUtils" autoFlush="true" %>

<!-- Whatuni clearing sr page head starts -->
        <div class="wuclrsr_rslthead">
            <div class="ad_cnr">
                <div class="content-bg" id="content-blk">
                    <div class="sr">
                        <jsp:include page="/seopods/breadCrumbs.jsp">
     						   <jsp:param name="pageName" value="CLEARING_SEARCH_RESULTS_PAGE"/>
   						   </jsp:include>
                        <div class="com">
                            <div class="com-lt">
                                <div>
                                    <h4 class="wuclr_ttlbl"><spring:message code="clearing.text.label" /></h4>
                                    <h1 class="result-head reshd2"><c:if test="${not empty dispSubjectName}">${dispSubjectName}</c:if> ${studyLevelDesc} <c:if test="${not empty selectedRegion and selectedRegion ne 'United kingdom'}"> in ${selectedRegion}</c:if></h1>
                                    <h2 class="result-head respar2">
                                    <c:if test="${not empty totalCourseCount}">
                                    ${totalCourseCount} degree ${totalCourseCount eq 1 ? 'course' : totalCourseCount gt 1 ? 'courses' : ''} in clearing featuring <c:if test="${not empty dispSubjectName}">${dispSubjectName}</c:if>. <c:if test="${not empty dispSubjectName}">Find which ${dispSubjectName} clearing course is the best for you.</c:if>                              
                                    </c:if>
                                    </h2>
                                </div>
                            </div>                            
                        </div>
                        <div id="articleTab" class="sr-art" style="display:none;">
                            <div class="sart-lt">
                            </div>
                            <div class="sart-rt">
                            </div>
                        </div>
                        
                        <%--clearing search results filters pod --%>
                        <jsp:include page="/jsp/clearing/searchresult/include/filters.jsp" />
                        <%--End of clearing search result filters section --%>
                        
                    </div>                    
                </div>                
            </div>
        </div>        