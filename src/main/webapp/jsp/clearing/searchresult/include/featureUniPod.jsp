<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="WUI.utilities.CommonUtil"%>
<c:if test="${not empty featuredBrandListClearing}">
   <c:forEach var="featuredBrandList" items="${featuredBrandListClearing}" varStatus="i">
      <div class="wuclrsr_featrbox srres_cnt fl_w100" id="featured_clearing" data-id="stats_not_logged_featured">
         <div class="feabrnd_ui fl_w100">
            <div class="fbrnd_lft fl">
               <div class="sponlst_hd fl_w100 imob">
                  <h4 class="fl_w100">Featured</h4>
               </div>
               <c:if test="${featuredBrandList.mediaType eq 'IMAGE'}">
                  <div class="fbrnd_vid fl">
                     <div class="clr15_vid fl_w100">
                        <img class="cl_vid" data-rjs="Y" id="featured_image_clearing" src="${imagePathFeatured }" data-src="${imagePathFeatured }"
                           alt="Sponsored listing image">
                     </div>
                  </div>
               </c:if>
               <c:if test="${featuredBrandList.mediaType eq 'VIDEO'}">
                  <div class="fbrnd_vid fl" id ="featured_videoId" data-id="stats_not_logged_featured_video">
                     <div class="clr15_vid fl_w100">
                        <c:if test="${not empty featuredBrandList.mediaPath}">
                           <div class="prf_ldicon cmm_ldericn" id="loadIconflv" style="display: none;"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif", 1)%>"></div>
                           <span class="cl_vcnr" id="featured_video_plyicn" onclick="clickPlayAndPauseVideo('featured_video_clearing', event)" style="display: none;">
                           <span class="pl_icn"><img class="pl_icn_img" src="${featuredPlayIcon }" alt="play icon"></span>			       
                           </span>      
                           <img class="cl_vid" data-rjs="Y" id="featured_image_clearing" src="${thumbnailPath }" data-src="${thumbnailPath }" alt="Featured Video image">
                           <video preload="none" data-rjs="Y" id="featured_video_clearing" class="${videoClass }" width="100%" height="100%" style="display: none;"
                              onclick="clickPlayAndPauseVideo('featured_video_clearing', event)" data-media-id="${featuredBrandList.mediaId}"
                              controlslist="nodownload" playsinline="">
                              <source src="${featuredBrandList.mediaPath}" type="video/mp4">
                           </video>
                        </c:if>
                     </div>
                  </div>
               </c:if>
            </div>
            <div class="fbrnd_rgt fl">
               <div class="sponlst_hd fl_w100 dsktp">
                  <h4 class="fl_w100">Featured</h4>
               </div>
               <div class="fbuniv_sec fl">
                  <div class="wuclr_fbrndfullhd">
                     <div class="wuclr_fbrndhdlft">
                        <c:if test="${not empty featuredBrandList.headline}">
                           <c:if test="${not empty featuredBrandList.profileHeadlineUrl}">
                              <c:choose>
                                 <c:when test="${fn:indexOf(featuredBrandList.profileHeadlineUrl, wwwDomain) ne -1 or fn:indexOf(featuredBrandList.profileHeadlineUrl, mtestDomain) ne -1 or fn:indexOf(featuredBrandList.profileHeadlineUrl, mdevDomain) ne -1}">
                                    <h3><a href= "javascript:void(0);" data-href="${featuredBrandList.profileHeadlineUrl}" onclick="featuredClearingDbStatsLogging('${featuredBrandList.collegeId}','','INTERNAL','${featuredBrandList.profileHeadlineUrl}','${featuredBrandList.orderItemId}');gtmproductClick(${i.index}, 'sponsor_type_featured_college_name');featuredBrandGaLogging();">${featuredBrandList.headline}</a></h3>
                                 </c:when>
                                 <c:otherwise>
                                    <h3><a rel="sponsored" target="_blank" href="${featuredBrandList.profileHeadlineUrl}" onclick="featuredClearingDbStatsLogging('${featuredBrandList.collegeId}','','EXTERNAL','${featuredBrandList.profileHeadlineUrl}','${featuredBrandList.orderItemId}');gtmproductClick(${i.index}, 'sponsor_type_featured_college_name');featuredBrandGaLogging();">${featuredBrandList.headline}</a></h3>
                                 </c:otherwise>
                              </c:choose>
                           </c:if>
                           <c:if test="${empty featuredBrandList.profileHeadlineUrl}">
                              <h3>${featuredBrandList.headline}</h3>
                           </c:if>
                        </c:if>
                        <c:if test="${not empty featuredBrandList.tagline}">
                           <p>${featuredBrandList.tagline}</p>
                        </c:if>
                        <c:if test="${featuredBrandList.reviewDisplayflag eq 'Y'}">
                           <c:if test="${not empty featuredBrandList.overallRating}">
                              <c:if test="${featuredBrandList.overallRating gt 0}">
                                 <ul class="strat cf">
                                    <li class="mr-15 leag_wrap">
                                       <span class="fl mr10">OVERALL RATING <span class="cal"> </span></span> 
                                       <span class="rat${featuredBrandList.overallRating} t_tip">
                                          <span class="cmp">
                                             <div class="hdf5">
                                                <spring:message code="wuni.tooltip.overall.keystats" />
                                             </div>
                                             <div class="line"></div>
                                          </span>
                                       </span>
                                       <span class="rtx">(${featuredBrandList.overallRatingExact})</span>
                                       <c:if test="${not empty featuredBrandList.reviewCount}">
                                          <a href= "javascript:void(0);" data-href ="${domainPath}${reviewUrlFeatured}" class="link_blue" onclick="gtmproductClick(${i.index}, 'sponsor_type_featured_reviews');featuredBrandGaLogging();featuredClearingDbStatsLogging('${featuredBrandList.collegeId}','','INTERNAL','${domainPath}${reviewUrlFeatured}','${featuredBrandList.orderItemId}')">
                                             <c:if test="${not empty featuredBrandList.reviewCount}">
                                                <c:if test="${featuredBrandList.reviewCount ne 0}">
                                                   ${featuredBrandList.reviewCount}
                                                   <c:if test="${featuredBrandList.reviewCount gt 1}">
                                                      reviews
                                                   </c:if>
                                                   <c:if test="${featuredBrandList.reviewCount eq 1}">  
                                                      review
                                                   </c:if>
                                                </c:if>
                                             </c:if>
                                          </a>
                                       </c:if>
                                    </li>
                                 </ul>
                              </c:if>
                           </c:if>
                        </c:if>
                     </div>
                     <div class="wuclr_fbrndhdrgt">
                        <div class="wuclr_fbrndhdrgtlgo">
                           <img src="${domainPath}${featuredBrandList.collegeLogoPath}" width="64" title="${featuredBrandList.collegeName}" class="ibdr" alt="${featuredBrandList.collegeName}"> 
                        </div>
                     </div>
                  </div>
                  <ul class="wuclrsr_crsboxbtnset">
                     <c:if test="${not empty featuredBrandList.navigationUrl}">
                        <li class="wuclrsr_vstbtn"><a href="${featuredBrandList.navigationUrl}" target="_blank" onclick="matchTypeGALogging('${featuredBrandList.collegeId}');gtmproductClick(${i.index}, 'clearing_featured_visit_website');featuredBrandGaLogging();featuredClearingDbStatsLogging('${featuredBrandList.collegeId}','','CLEARING_FEATURED_WESITE','${featuredBrandList.navigationUrl}','${featuredBrandList.orderItemId}');">Visit website</a></li>
                     </c:if>
                     <c:if test="${not empty featuredBrandList.hotlineNumber}">
                        <li class="wuclrsr_calbtn"><a id="contactFeatured" onclick="matchTypeGALogging('${featuredBrandList.collegeId}');gtmproductClick(${i.index}, 'clearing_featured_hotline');featuredBrandGaLogging();featuredClearingDbStatsLogging('${featuredBrandList.collegeId}','','CLEARING_FEATURED_HOTLINE','${featuredBrandList.hotlineNumber}','${featuredBrandList.orderItemId}');changeContact('${featuredBrandList.hotlineNumber}','contactFeatured')"><i class="fa fa-phone" aria-hidden="true"></i>Call now</a></li>
                     </c:if>
                  </ul>
               </div>
            </div>
         </div>
         <input type="hidden" id="collegeIdFeatured" value="${featuredBrandList.collegeId}"/>
         <input type="hidden" id="collegeNameFeatured" value="${featuredBrandList.collegeName}"/>
         <input type="hidden" id="networkIdFeatured" value="${featuredBrandList.networkId}"/>
         <input type="hidden" id="gaCollegeNameFeatured" value="${gaCollegeNameFeatured}"/>
         <input type="hidden" id="orderItemIdFeatured" value="${featuredBrandList.orderItemId}"/>  
      </div>
   </c:forEach>
</c:if>