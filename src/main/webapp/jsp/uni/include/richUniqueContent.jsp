<%--
  * @purpose:  jsp for showing unique content..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 31-May-2016    Indumathi.S               1.0      First draft           wu_553
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, mobile.util.MobileUtils"%>
<%CommonFunction common = new CommonFunction();
  String toolTip = common.getDomainName(request, "N");
  String richSecLen = "", overviewImgPath = "", myhcProfileId = "";
  String rpLandingImages = "Academic-Strengths,Accommodation,City-Life,Clubs-and-Societies,Course-and-Lecturers,Entry-Requirements,Facilities,Fees,international,Job-Prospects,Overview-Unique,Overview,ScholarshipsBursaries,Student-Support,Student-Union,Uni of the year,uni-info";
  boolean mobileFlag = new MobileUtils().userAgentCheck(request); //Added code for checking device or not by Prabha on 24_Jan_2017_REL
  String onePxImg = CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1);
  String loadingImg = CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif", 1);
%>

<section class="p0">
<div class="content-bg uni_fwdth uni_toppos">
  <div class="prorim">
  <c:if test="${not empty requestScope.collegeOverViewList}">
      <div class="uni_hdcnt">
        <div class="uniimg_head" id="uniimg_head_mob_id">
        <%if(mobileFlag){%>
          <img class="lazy-load" src="<%=onePxImg%>" data-src="<%=CommonUtil.getImgPath("/wu-cont/images/uni-info.svg", 1)%>" alt="Uni info"/>
        <%}else{%>
          <img src="<%=CommonUtil.getImgPath("/wu-cont/images/uni-info.svg", 1)%>" alt="Uni info"/>
        <%}%>
        <span class="ucnt_hdtxt">Uni info</span></div>
          <c:forEach var="providerMediaList" items="${requestScope.collegeOverViewList}" varStatus="index">
          <c:set var="indexValue" value="${index.index}"/>
            <c:if test="${not empty providerMediaList.originalSectionName}">
              <c:set var="sectionNameMob" value="${providerMediaList.originalSectionName}"/>
              <c:set var="profileSectionNameMob" value="${providerMediaList.selectedSectionName}"/>
              <div class="uniimg_head" id="uniimg_head_mob_id<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" style="display:none;" title="<%=(String)pageContext.getAttribute("profileSectionNameMob")%>">
                <%String uniImagePathMob = "/wu-cont/images/"+ common.replaceHypen(common.replaceURL((String)pageContext.getAttribute("sectionNameMob"))) +".svg";  
                  if (rpLandingImages.contains(common.replaceHypen(common.replaceURL((String)pageContext.getAttribute("sectionNameMob"))))) { %>
                  <img class="lazy-load" src="<%=onePxImg%>" data-src="<%=CommonUtil.getImgPath(uniImagePathMob, 1)%>" title="<%=(String)pageContext.getAttribute("profileSectionNameMob")%>" alt="<%=(String)pageContext.getAttribute("profileSectionNameMob")%>"/>
                <%}
                %><span class="ucnt_hdtxt"><%=(String)pageContext.getAttribute("profileSectionNameMob")%></span>
                </div>
            </c:if>
            <div class="uniimg_head unicnt_head" id="wuscaRat_mob<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" style="display:none;">
              <c:if test="${not empty providerMediaList.profileRating}">
                <c:set var="profileRatingMob" value="${providerMediaList.profileRating}"/>
                <c:set var="profileRoundRatingMob" value="${providerMediaList.profileRoundRating}"/>
                <c:if test="${not empty providerMediaList.selectedSectionName}">
                  <c:set var="sectionNameDisplayMob" value="${providerMediaList.originalSectionName}"/>
                  <%
                    int indexValue = Integer.parseInt(pageContext.getAttribute("indexValue").toString());
                    String profileRoundRatingMob = (String)pageContext.getAttribute("profileRoundRatingMob");
                    String profileRatingMob = (String)pageContext.getAttribute("profileRatingMob");
                    String sectionNameDisplayMob = (String)pageContext.getAttribute("sectionNameDisplayMob");
                  %>
                  <jsp:include page="/jsp/uni/richprofile/include/wuscaUniRatting.jsp">
                    <jsp:param name="indexVal" value="<%=indexValue+1%>"/>
                    <jsp:param name="profileRoundRating" value="<%=profileRoundRatingMob%>"/>
                    <jsp:param name="profileRating" value="<%=profileRatingMob%>"/>
                    <jsp:param name="sectionNameDisplay" value="<%=sectionNameDisplayMob%>"/>
                    <jsp:param name="wuscaToolTip" value="wuscaToolTipMob"/>
                  </jsp:include>
                </c:if>
              </c:if>
              <span class="uni_bbtn"><a href="javascript:backToIntro();"><i class="fa fa-times"></i> BACK</a></span>
          </div>
        </c:forEach>
      </div>
      
      <div class="pro_image fl"> 
         <div class="uniimg_head" id="uniimg_head_id">
            <img class="lazy-load" src="<%=onePxImg%>" data-src="<%=CommonUtil.getImgPath("/wu-cont/images/uni-info.svg", 1)%>" alt="Uni info"/><span class="ucnt_hdtxt">Uni info</span>
         </div>
        <c:forEach var="providerMediaList" items="${requestScope.collegeOverViewList}" varStatus="index">
        <c:set var="indexValue" value="${index.index}"/>
          <c:if test="${not empty providerMediaList.originalSectionName}">
            <c:set var="sectionName" value="${providerMediaList.originalSectionName}"/>
            <c:set var="profileSectionName" value="${providerMediaList.selectedSectionName}"/>
             <div class="uniimg_head" id="uniimg_head_id<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" style="display:none;" title="<%=(String)pageContext.getAttribute("profileSectionName")%>">
             <%String uniImagePath = "/wu-cont/images/"+ common.replaceHypen(common.replaceURL((String)pageContext.getAttribute("sectionName"))) +".svg";
               if (rpLandingImages.contains(common.replaceHypen(common.replaceURL((String)pageContext.getAttribute("sectionName"))))) { %>
                <img class="lazy-load" src="<%=onePxImg%>" data-src="<%=CommonUtil.getImgPath(uniImagePath, 1)%>" title="<%=(String)pageContext.getAttribute("profileSectionName")%>" alt="<%=(String)pageContext.getAttribute("profileSectionName")%>"/>
               <%}
               %><span class="ucnt_hdtxt" id="profileSectionSpan<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>"><%=(String)pageContext.getAttribute("profileSectionName")%></span>
               <%String profileSectionName = (String)pageContext.getAttribute("profileSectionName");
               if(profileSectionName.length() > 26) { profileSectionName = profileSectionName.substring(0,25) + "..."; %>
               <input type="hidden" id="profileSectionDesktop<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" value="<%=(String)pageContext.getAttribute("profileSectionName")%>">
               <%}%>
               </div>
          </c:if>
        </c:forEach>
        <div class="proSli">
          <ul id="richSecCnt">              
            <c:forEach var="providerMediaList" items="${requestScope.collegeOverViewList}" varStatus="index">  
            <c:set var="indexValue" value="${index.index}"/>          
              <%if(Integer.parseInt(pageContext.getAttribute("indexValue").toString())==0){%>
                <c:if test="${not empty providerMediaList.profileImagePath}">
                  <c:set var="overviewImg" value="${providerMediaList.profileImagePath}"/>
                  <%overviewImgPath = (String)pageContext.getAttribute("overviewImg");%>  
                </c:if>
                <c:if test="${not empty providerMediaList.myhcProfileId}">
                  <c:set var="myhcProfId" value="${providerMediaList.myhcProfileId}"/>
                  <%myhcProfileId = (String)pageContext.getAttribute("myhcProfId");%>  
                </c:if>
              <%}%>             
              <li id="richImg<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>">                                  
                <%if(Integer.parseInt(pageContext.getAttribute("indexValue").toString())==0){%>              
                  <c:if test="${not empty providerMediaList.profileImagePath}">    
                    <c:if test="${providerMediaList.mediaType eq 'Image'}" >
                      <%if(mobileFlag){%>
                        <img class="lazy-load" src="<%=onePxImg%>" alt="${providerMediaList.selectedSectionName}" data-src="${providerMediaList.profileImagePath}"/>
                      <%}else{%>
                        <img class="lazy-load" src="${providerMediaList.profileImagePath}" alt="${providerMediaList.selectedSectionName}" data-src="${providerMediaList.profileImagePath}"/>
                      <%}%>
                    </c:if>
                    <c:if test="${providerMediaList.mediaType eq 'Video'}" >
                      <%request.setAttribute("showlightbox","YES");%>
                      <%if(mobileFlag){%>
                        <img class="lazy-load" src="<%=onePxImg%>" alt="${providerMediaList.selectedSectionName}" data-src="${providerMediaList.profileImagePath}"/>
                      <%}else{%>
                        <img class="lazy-load" src="${providerMediaList.profileImagePath}" alt="${providerMediaList.selectedSectionName}" data-src="${providerMediaList.profileImagePath}"/>
                      <%}%>
                      <span id="provie<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>" class="provie plyIcon" onclick="javascript:return showVideo('${providerMediaList.videoId}','${providerMediaList.collegeId}','B', '<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>', '${providerMediaList.subOrderItemId}');"><i class="fa fa-play-circle-o"></i></span>
                      <div class="prf_ldicon" id="loadIcon<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>" style="display: none;"><img  class="lazy-load" src="<%=onePxImg%>" data-src="<%=loadingImg%>"></div>
                    </c:if>
                  </c:if>              
                <%}else{%>                                          
                  <c:if test="${not empty providerMediaList.profileImagePath}">                  
                    <c:if test="${providerMediaList.mediaType eq 'Image'}" >
                      <img class="lazy-load" src="<%=onePxImg%>" data-src="${providerMediaList.profileImagePath}" alt="${providerMediaList.selectedSectionName}"/>                    
                    </c:if>                  
                    <c:if test="${providerMediaList.mediaType eq 'Video'}" >
                      <%request.setAttribute("showlightbox","YES");%>
                      <img class="lazy-load" src="<%=onePxImg%>" data-src="${providerMediaList.profileImagePath}" alt="${providerMediaList.selectedSectionName}"/>                    
                      <span id="provie<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>" class="provie plyIcon" onclick="javascript:return showVideo('${providerMediaList.videoId}','${providerMediaList.collegeId}','B', '<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>', '${providerMediaList.subOrderItemId}');"><i class="fa fa-play-circle-o"></i></span>                    
                      <div class="prf_ldicon" id="loadIcon<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>" style="display: none;"><img  class="lazy-load" src="<%=onePxImg%>" data-src="<%=loadingImg%>"></div>
                    </c:if>
                  </c:if>                                                                                                  
                  <c:if test="${empty providerMediaList.profileImagePath}">
                    <img class="lazy-load" src="<%=onePxImg%>" data-src="<%=overviewImgPath%>" alt="${providerMediaList.selectedSectionName}"/>
                  </c:if>                                                                                                          
                <%}%>
              </li>                                                        
              <input type="hidden" id="secTypeId_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" value="${providerMediaList.selectedSectionName}"/>
              <input type="hidden" id="profSec_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" value="${providerMediaList.selectedSectionName}"/>
              <input type="hidden" id="richSecMyhcProfId_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" value="${providerMediaList.myhcProfileId}"/>
              <input type="hidden" id="richSecProfId_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" value="${providerMediaList.profileId}"/>          
            </c:forEach>                 
            <input type="hidden" id="myhcProfileId" value="<%=myhcProfileId%>"/>
          </ul>
          <span class="proin" id="prev_lnk"><a class="lftar" href="javascript:sliderPlay('prev');"><i class="fa fa-angle-left"></i></a></span>
          <span class="proin"><a class="rgar" href="javascript:sliderPlay('next');"><i class="fa fa-angle-right"></i></a></span>    
        </div>  
      </div>  
      <c:set var="richImgLen" value="${fn:length(requestScope.collegeOverViewList)}"/>                 
      <%richSecLen=pageContext.getAttribute("richImgLen").toString();%>
      <input type="hidden" id="curPofSec" value=""/>
  </c:if>
  
  <c:if test="${not empty requestScope.showProfileTab}">
    <div class="pro_info fl">
      <c:if test="${not empty requestScope.collegeOverViewList}">
          <c:forEach var="providerMediaList" items="${requestScope.collegeOverViewList}" varStatus="index">
           <c:set var="indexValue" value="${index.index}"/>  
            <div class="uniimg_head unicnt_head" id="wuscaRat<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" style="display:none;">
              <c:if test="${not empty providerMediaList.profileRating}">
                <c:set var="profileRating" value="${providerMediaList.profileRating}"/>
                <c:set var="profileRoundRating" value="${providerMediaList.profileRoundRating}"/>
                <c:if test="${not empty providerMediaList.selectedSectionName}">
                  <c:set var="sectionNameDisplay" value="${providerMediaList.originalSectionName}"/>
                  <%
                    int indexValue = Integer.parseInt(pageContext.getAttribute("indexValue").toString());
                    String profileRating = (String)pageContext.getAttribute("profileRating");
                    String profileRoundRating = (String)pageContext.getAttribute("profileRoundRating"); 
                    String sectionNameDisplay = (String)pageContext.getAttribute("sectionNameDisplay");
                  %>
                  <jsp:include page="/jsp/uni/richprofile/include/wuscaUniRatting.jsp">
                    <jsp:param name="indexVal" value="<%=indexValue+1%>"/>
                    <jsp:param name="profileRating" value="<%=profileRating%>"/>
                    <jsp:param name="profileRoundRating" value="<%=profileRoundRating%>"/>
                    <jsp:param name="sectionNameDisplay" value="<%=sectionNameDisplay%>"/>
                    <jsp:param name="wuscaToolTip" value="wuscaToolTip"/>
                  </jsp:include>
                 
                </c:if>
              </c:if>
              <span class="uni_bbtn"><a href="javascript:backToIntro();"><i class="fa fa-times"></i> BACK</a></span>
            </div>
          </c:forEach>
      </c:if>
        <div id="content-2" class="pro_infoco fl content mCustomScrollbar light" data-mcs-theme="minimal-dark">
          <div id="richProIntroSec">
            <ul class="cag-list">
              <c:forEach var="sectionTabs" items="${requestScope.showProfileTab}" varStatus="index">
              <c:set var="indexValue" value="${index.index}"/>  
                <c:set var="selectedSectionName" value="${sectionTabs.buttonLabel}"/>
                <%String selectedSectionName = (String)pageContext.getAttribute("selectedSectionName");
                selectedSectionName = selectedSectionName.replaceAll("'","");
                 if("Students Union".equalsIgnoreCase(selectedSectionName)){
                   selectedSectionName = "Student Union";
                 }
                %>
                <li><a href="javascript:showContent('<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>')" onclick="GAEventTrackingRichIP('Webclick', 'Profile Content', '<%=selectedSectionName%>');" ><i class="fa fa-plus-circle"></i><span class="link_blue txt">${sectionTabs.buttonLabel}</span></a></li>
              </c:forEach>
            </ul>
            <c:if test="${not empty requestScope.wuscaWinLogoList}">
             <div class="ui_stuch">
                <c:forEach var="wuscaWinLogo" items="${requestScope.wuscaWinLogoList}">
                  <p class="fr stuch">
                  <%if(mobileFlag){%>
                    <img class="lazy-load" src="<%=onePxImg%>" data-src="${wuscaWinLogo.awardImage}" alt="WUSCA winner logo"/>
                  <%}else{%>
                    <img src="${wuscaWinLogo.awardImage}" alt="WUSCA winner logo"/>
                  <%}%>
                  </p>
                </c:forEach>
              </div>
            </c:if>
          </div>
          <c:if test="${not empty requestScope.collegeOverViewList}">
            <div id="rpProfileSec" style="display:none;">
              <c:forEach var="profileSection" items="${requestScope.collegeOverViewList}" varStatus="index">  
              <c:set var="indexValue" value="${index.index}"/>                                     
                <div id="proSec<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" style="display:none;">        
                  <div id="crd_desc" class="mxHg "><br>
                    <div class="clear"></div>
                    ${profileSection.profileDescription}                      
                  </div>              
                </div>  
              </c:forEach>                           
            </div>
          </c:if>
        </div>
        <span class="uni_shadow"></span>
      </div>  
    
      <div class="rolC rwetm rwet pb5 uni_prvnxt">
        <div class="rolCL fr sli_clrchg">
          <span id="sec_prev_lnk" class="sec_prev_lnk"><a href="javascript:sliderPlay('prev');"><div class="unibtn_md"><i class="fa fa-long-arrow-left fl mr10"></i>PREV</div></a></span>
          <a class="fr" href="javascript:sliderPlay('next');"><div class="unibtn_md"><i class="fa fa-long-arrow-right fr mr10"></i>NEXT</div></a>
        </div>
      </div>
		
    </c:if>    
    <input type="hidden" id="feaVideoLen" value="<%=richSecLen%>"/>
    <input type="hidden" id="rpCurPos" value=""/>
    <input type="hidden" id="rpPrePos" value=""/>
    <input type="hidden" id="rpNexPos" value=""/>            
    <script type="text/javascript">richProfileSlider("1","<%=richSecLen%>","first");</script>
  </div>
</div>    
</section>  