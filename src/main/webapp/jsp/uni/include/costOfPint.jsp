<%--
  * @purpose:  This jsp is to display a Cost of Pint details..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 24-Nov-2015    Indumathi Selvam          1.0      First draft           wu_547
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil" %>

<c:if test="${not empty requestScope.costOfPint}">
    <c:forEach var="costOfPint" items="${requestScope.costOfPint}"> 
     <div class="content-bg">
      <section class="key_stats greybg cospin clr_ks_mt25">
        <h2 class="sub_tit fl fnt_lrg lh_24">Cost of living</h2>
        <div class="fr right_link">
          DATA SOURCE:
          <spring:message code="wuni.tooltip.costofpint.entry.text" /> <%--Changed datacourse text from jsp to App.properties for 24_Jan_2016 By Thiyagu G--%>          
        </div>
        <div id="div_key_stats"></div>
        <div class="borderbot1 fl mt25 mb20"></div>
              
        <div class="glico">
          <c:if test="${not empty costOfPint.whatuniCostPint}">
              <h2 class="fnt_lbd txt_cnr lh_24 fl pb20" id="AfterMonthsId0">Cost of a pint
                <span class="tool_tip"> 
                  <i class="fa fa-question-circle fnt_24">
                    <span class="cmp"><div class="hdf5"></div><div>This is the average cost of a pint <c:if test="${not empty requestScope.cityGudieLocation}">in ${requestScope.cityGudieLocation}</c:if>.</div><div><spring:message code="wuni.tooltip.costofliving.entry.text" /></div><div class="line"></div></span>
                  </i>
                </span> 
              </h2>  
              
              <c:if test="${not empty requestScope.whatuniCostPint}">
                <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src='<%=CommonUtil.getImgPath("/wu-cont/images/gl-ico.png",0)%>' alt="Cost of pint full">
                <div class="gllft">
                  <div class="div3 cl_grylt">&pound;${requestScope.whatuniCostPint}</div>
                </div>
              </c:if>
              <c:if test="${empty requestScope.whatuniCostPint}">
                <c:if test="${not empty requestScope.whatuniCostPintOne and not empty requestScope.whatuniCostPintTwo}">
                    <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src='<%=CommonUtil.getImgPath("/wu-cont/images/glicon1.png",0)%>' alt="Cost of pint half">
                     <div class="gllft">
                       <div class="div3 cl_grylt">&pound;${requestScope.whatuniCostPintTwo}</div>
                       <div class="range1"><span class="wi10"></span> <span class="hein"></span><span class="wi10"></span></div>
                       <div class="div3 cl_grylt">&pound;${requestScope.whatuniCostPintOne}</div>
                     </div>
                </c:if>
              </c:if>  
          </c:if>
        </div>

        <div class="grapm1 fr cf">
          <c:if test="${not empty costOfPint.accommodationUpper}">
            <c:if test="${costOfPint.accommodationUpper ne 0}">
              <h2 class="fnt_lbd txt_cnr lh_24 fl pb20" id="AfterMonthsId0">Accommodation costs
                <span class="tool_tip"> 
                  <i class="fa fa-question-circle fnt_24">
                    <span class="cmp"> 
                      <div class="hdf5"></div> 
                      <div>This is the average annual cost for a full time student at this uni.</div>
                      <div>
                        <c:if test="${not empty costOfPint.myhcAccommodationToolTipFlag}">
                          <c:if test="${costOfPint.myhcAccommodationToolTipFlag eq 'Y'}">                        
                            <spring:message code="wuni.tooltip.costofliving.entry.text" />
                          </c:if>
                          <c:if test="${costOfPint.myhcAccommodationToolTipFlag ne 'Y'}">                        
                            <spring:message code="wuni.tooltip.kiscostofliving.entry.text" />
                          </c:if>
                        </c:if>
                      </div>
                      <div class="line"></div>
                    </span>
                  </i>
                </span> 
              </h2>
            </c:if>
          </c:if>
          <div id="avgSalaryAfterSixMonthsDiv0"> 
           <c:if test="${not empty costOfPint.accommodationUpper}">
             <c:if test="${costOfPint.accommodationUpper ne 0}">
               <div class="clear fl w100p" id="avgSalaryAfterSixMonthsDiv0">
                 <i class="fa fa-home fl"></i>
                  <div class="div2">
                    <span class="ct-cont">
                      <c:set var="accLower" value="${costOfPint.accommodationLowerBarLimit}"/>
                      <c:set var="accUpper" value="${costOfPint.accommodationUpperBarLimit}"/>
                      <%
                        String barWidth = String.valueOf(Integer.parseInt(pageContext.getAttribute("accUpper").toString()) - Integer.parseInt(pageContext.getAttribute("accLower").toString()));
                               if("0".equals(barWidth)){
                                barWidth = "0.2";
                               } 
                      %>
                      <span style='width: <%=barWidth%>%;left: <%=(String)pageContext.getAttribute("accLower")%>%;' class="bar">
                        <div class="cstrn">                      
                          <c:if test="${costOfPint.accommodationLower ne 0}">
                            <div class="div3 fnt_lbd fnt_18 cl_grylt fl lr1 acc_cst">&pound;${costOfPint.accommodationLower}</div>
                          </c:if>                      
                          <div class="div3 fnt_lbd fnt_18 cl_grylt fl lr1">&pound;${costOfPint.accommodationUpper}</div>
                        </div>
                      </span>
                    </span>
                  </div>
                 <i class="fa fa-home fr"></i>
               </div>
             </c:if>
            </c:if>
            
             <c:if test="${not empty costOfPint.livingCost}">
              <div class="clear fl mt30 w100p">
                <h2 class="fnt_lbd txt_cnr lh_24 fl pt20 pb10" id="AfterMonthsId0">Living costs
                  <span class="tool_tip"> 
                    <i class="fa fa-question-circle fnt_24">
                      <span class="cmp"> 
                        <div class="hdf5"></div> 
                        <div>This is the average annual cost of living for a student studying in the town where this uni is based.</div>
                        <div><spring:message code="wuni.tooltip.costofliving.entry.text" /></div> <%--Changed datacourse text from jsp to App.properties for 24_Jan_2016 By Thiyagu G--%>   
                        <div class="line"></div>
                      </span>
                    </i>
                  </span> 
                </h2>
                <i class="fa fa-database fl"></i>
                <div class="div2">
                  <span class="ct-cont">
                    <span style='width: ${costOfPint.livingBarLimit}%;' class="bar bg_brn">
                      <div class="cstrn">
                        <div class="div3 fnt_lbd fnt_18 cl_grylt fl lr1">&pound;${costOfPint.livingCost}</div>
                      </div>
                    </span>
                  </span>
                </div>
                <i class="fa fa-database fr"></i>
              </div>
            </c:if>
          </div> 
        </div>  
      </section>
      </div>
    </c:forEach>
</c:if>