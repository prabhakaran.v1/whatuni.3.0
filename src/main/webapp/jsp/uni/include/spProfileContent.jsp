<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<%
  String richSecLen = "", overviewImgPath = "", myhcProfileId = "";
%>

<section class="p0">
<div class="content-bg uni_toppos">
  <div class="prorim">
    <c:if test="${not empty requestScope.collegeOverViewList}">
      <div class="pro_image fl">                              
        <div class="proSli">
          <ul id="richSecCnt">              
            <c:forEach var="providerMediaList" items="${requestScope.collegeOverViewList}" varStatus="index"> 
            <c:set var="indexValue" value="${index.index}"/>           
              <%if(Integer.parseInt(pageContext.getAttribute("indexValue").toString())==0){%>
                <c:set var="overviewImg" value="${providerMediaList.profileImagePath}"/>
                <c:set var="myhcProfId" value="${providerMediaList.myhcProfileId}"/>
                <%
                  overviewImgPath = (String)pageContext.getAttribute("overviewImg");
                  myhcProfileId = pageContext.getAttribute("myhcProfId").toString();
                %>  
              <%}%>             
              <li id="richImg<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>">                                  
                <%if(Integer.parseInt(pageContext.getAttribute("indexValue").toString())==0){%>              
                  <c:if test="${not empty providerMediaList.profileImagePath}">    
                    <c:if test="${providerMediaList.mediaType eq 'Image'}">
                      <img class="lazy-load" src="${providerMediaList.profileImagePath}" alt="${providerMediaList.selectedSectionName}" data-src="${providerMediaList.profileImagePath}"/>
                    </c:if>
                     <c:if test="${providerMediaList.mediaType eq 'Video'}">
                      <%request.setAttribute("showlightbox","YES");%>
                      <img class="lazy-load" src="${providerMediaList.profileImagePath}" alt="${providerMediaList.selectedSectionName}" data-src="${providerMediaList.profileImagePath}"/>                                        
                      <span class="provie" onclick="javascript:return showVideo('${providerMediaList.videoId}','${providerMediaList.collegeId}','B');"><i class="fa fa-play-circle-o"></i></span>
                    </c:if>
                  </c:if>              
                <%}else{%>                                          
                  <c:if test="${not empty providerMediaList.profileImagePath}">                  
                    <c:if test="${providerMediaList.mediaType eq 'Image'}">
                      <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${providerMediaList.profileImagePath}" alt="${providerMediaList.selectedSectionName}"/>                    
                    </c:if>                  
                    <c:if test="${providerMediaList.mediaType eq 'Video'}">
                      <%request.setAttribute("showlightbox","YES");%>
                      <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${providerMediaList.profileImagePath}" alt="${providerMediaList.selectedSectionName}"/>                    
                      <span class="provie" onclick="javascript:return showVideo('${providerMediaList.videoId}','${providerMediaList.collegeId}','B');"><i class="fa fa-play-circle-o"></i></span>                    
                    </c:if>
                  </c:if>                                                                                                  
                  <c:if test="${empty providerMediaList.profileImagePath}">
                    <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="<%=overviewImgPath%>" alt="${providerMediaList.selectedSectionName}"/>
                  </c:if>                                                                                                          
                <%}%>
              </li>                                                        
              <input type="hidden" id="richSecProfId_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" value="${providerMediaList.profileId}"/>
              <input type="hidden" id="profSec_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" value="${providerMediaList.selectedSectionName}"/>
            </c:forEach>                 
            <input type="hidden" id="myhcProfileId" value="<%=myhcProfileId%>"/>
          </ul>
          <span class="proin" id="prev_lnk"><a class="lftar" href="javascript:sliderPlay('prev');"><i class="fa fa-angle-left"></i></a></span>
          <span class="proin"><a class="rgar" href="javascript:sliderPlay('next');"><i class="fa fa-angle-right"></i></a></span>    
        </div>  
      </div>
      <c:set var="richImgLen" value="${fn:length(requestScope.collegeOverViewList)}"/>                    
      <%richSecLen=pageContext.getAttribute("richImgLen").toString();%>
      <input type="hidden" id="curPofSec" value=""/>
    </c:if>
    <c:if test="${not empty requestScope.showProfileTab}">
      <div class="pro_info fl">
        <div class="pro_infoco fl">
          <div id="richProIntroSec">
            <h2 class="fnt_24">Uni info</h2>
            <ul class="cag-list">
              <c:forEach var="sectionTabs" items="${requestScope.showProfileTab}" varStatus="index">
              <c:set var="indexValue" value="${index.index}"/> 
                <li>
                  <a href="javascript:showContent('<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>')"><i class="fa fa-plus-circle"></i><span class="link_blue txt">${sectionTabs.buttonLabel}</span></a>                    
                </li>
              </c:forEach>
            </ul>
            <c:if test="${not empty requestScope.wuscaWinLogoList}">
              <c:forEach var="wuscaWinLogo" items="${requestScope.wuscaWinLogoList}"> 
                <p class="fr stuch"><img class="lazy-load" src="${wuscaWinLogo.awardImage}" data-src="${wuscaWinLogo.awardImage}" alt="WUSCA winner logo"/></p>
              </c:forEach>
            </c:if>
          </div>
          <jsp:include page="/jsp/uni/richprofile/include/profileSections.jsp"/>        
        </div>
      </div>          
    </c:if>    
    <input type="hidden" id="feaVideoLen" value="<%=richSecLen%>"/>
    <input type="hidden" id="rpCurPos" value=""/>
    <input type="hidden" id="rpPrePos" value=""/>
    <input type="hidden" id="rpNexPos" value=""/>            
    <script type="text/javascript">richProfileSlider("1","<%=richSecLen%>","first");</script>
  </div>
</div>    
</section>  