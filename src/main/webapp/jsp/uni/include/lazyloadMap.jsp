<%--
  * @purpose  This jsp is used to lazy load map script...
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Desc                                                         Rel Ver.
  * 27-JAN-2016 Prabhakaran V.               548    Files include map loading script function call..                 548
  * *************************************************************************************************************************
--%>
<%
  String latitudeStr =  request.getParameter("latitudeStr");   
  String longitudeStr =  request.getParameter("longitudeStr");  
%>
<script type="text/javascript" id="script_google_map">lboxNewMapInitialize('<%=latitudeStr%>','<%=longitudeStr%>')</script>