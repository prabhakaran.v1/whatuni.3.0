<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction,WUI.utilities.GlobalConstants,org.apache.commons.validator.GenericValidator, mobile.util.MobileUtils, WUI.utilities.SessionData" autoFlush="true"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>

<%
  String richSecLen = "", overviewImgPath = "", myhcProfileId = "", noindexfollow ="", seoPageName ="", gaCollegeName ="", dispCollegeName = "", highTab = ""; 
  String collegeId =  (String)request.getAttribute("collegeId");
  seoPageName      =  (String)request.getAttribute("seoPageName");
  seoPageName      =  "COLLEGE LANDING PAGE";
  dispCollegeName  =  (String)request.getAttribute("collegeNameDisplay");
  CommonFunction common = new CommonFunction();
  gaCollegeName    = common.replaceSpecialCharacter(dispCollegeName);
  String basketCollegeName = request.getAttribute("interactionCollegeName") != null && request.getAttribute("interactionCollegeName").toString().trim().length() > 0 ? common.replaceSpecialCharacter((String)request.getAttribute("interactionCollegeName")) : "0";
  String heroImageJsName = CommonUtil.getResourceMessage("wuni.heroImgJsName.js", null);
  String domainSpecificPath = common.getSchemeName(request)+ CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
  String main_480_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.480.css", null);
  String main_992_ver = CommonUtil.getResourceMessage("wuni.whatuni.main.992.css", null);
  String envronmentName = common.getWUSysVarValue("WU_ENV_NAME");
  String canonicalUrl = request.getAttribute("canonicalUrl")!=null ? request.getAttribute("canonicalUrl").toString().trim() : "";
  if(request.getAttribute("richProfileType")!=null && "RICH_INST_PROFILE".equalsIgnoreCase((String)request.getAttribute("richProfileType"))){
    noindexfollow = "index,follow";
  }else{
    noindexfollow = "noindex,follow";
  }
  String clearingyear   = GlobalConstants.CLEARING_YEAR;
  String clearingonoff  = common.getWUSysVarValue("CLEARING_ON_OFF");   
  String requestURL = (String)request.getAttribute("REQUEST_URI");
         requestURL = requestURL.indexOf("/degrees") > -1 ? requestURL.replace("/degrees", "") : requestURL;
         requestURL = requestURL.indexOf(".html") > -1 ? requestURL.replace(".html", "/") : requestURL;
  String profileType = (String)request.getAttribute("richProfileType");
  String ugTabStyleClass = "op_loct";  
  if(!GenericValidator.isBlankOrNull(clearingonoff) && "ON".equals(clearingonoff)){  
    ugTabStyleClass = "op_loct";
  }  
  if(request.getAttribute("highlightTab")!=null && !"".equals(request.getAttribute("highlightTab"))){
    highTab = (String)request.getAttribute("highlightTab");    
  } 
  String spMyhcProfileId = (String)request.getAttribute("spMyhcProfileId");//Added by Indumathi NOV-03-15 for sp rich profile
	String commonVideoJSName = CommonUtil.getResourceMessage("wuni.commonVideoJsName.js", null);
  //Added by Indumathi.S For unique content changes May_31_2016
  String profileId = request.getAttribute("profileId") != null ? (String)request.getAttribute("profileId") : "";
  String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null);
  //End
  
  //Added by Indumathi.S For pageview logging July-26-2016
   int stind1 = request.getRequestURI().lastIndexOf("/");
   int len1 = request.getRequestURI().length();
   String pageName = pageContext.getAttribute("pagename3") != null ?  (String)pageContext.getAttribute("pagename3") : request.getRequestURI().substring(stind1+1,len1);
   String reviewlightboxJs = CommonUtil.getResourceMessage("wuni.reviewlightbox.js", null);
   String richProfileJS = CommonUtil.getResourceMessage("wuni.rich.profile.js", null);//changed js reference dynamic by Sangeeth.S for FEB_12_19 rel
   pageName = pageName !=null ? pageName.toLowerCase() : pageName;
  if((!GenericValidator.isBlankOrNull(pageName)) && (("richprofilelanding.jsp").equalsIgnoreCase(pageName))){           
    if(request.getAttribute("richProfileType")!=null && "RICH_INST_PROFILE".equalsIgnoreCase((String)request.getAttribute("richProfileType"))){
      pageName = "richuniview.jsp";      
    }
  }
  String gaPageCollegeName = (String)request.getAttribute("hitbox_college_name");
         gaPageCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(gaPageCollegeName);
         gaPageCollegeName = !GenericValidator.isBlankOrNull(gaPageCollegeName) ? !gaPageCollegeName.equals("null") ? gaPageCollegeName.toLowerCase().replaceAll(" ","-").trim() : "" : "";  
  String userJourney = (String)request.getAttribute("USER_JOURNEY_FLAG"); //Added for cmmt
  String mainSecStyle     = "rich_profile";  
  String seoParamFlag = "UNI_LANDING";
  String clrItrBtnCls = "";
  String mobileMenuText = "More Info";  
  if("CLEARING".equalsIgnoreCase(userJourney)){
      mainSecStyle = "rich_profile clr_prof_cnt clr_nw_bt clrrich_prof";
      seoPageName = "CLEARING PROFILE";
      seoParamFlag = "CLEARING PROFILE";
      noindexfollow = "index,follow";
      canonicalUrl = canonicalUrl.replace("?clearing","");
      clrItrBtnCls = "clr_btn";
      mobileMenuText = "Apply";
  }
%>
<body onload="adjustStyle();">
<input type="hidden" name="pageName" id="pageName" value="<%=pageName%>">
<input type="hidden" name="gaPageCollegeName" id="gaPageCollegeName" value="<%=gaPageCollegeName%>">
<c:if test="${not empty requestScope.pixelTracking}">${requestScope.pixelTracking}</c:if>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>        
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/videoplayer/<%=commonVideoJSName%>" defer="defer"></script>    
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=heroImageJsName%>"></script>
</header>
<div class="<%=mainSecStyle%>">  
<section id="heroslider" class="heroSlider p0">  
  <div id="stickyTop" class="">
    <div class="course_deatils hrbgi">
      <div class="content-bg">
        <c:if test = "${not empty requestScope.mobileFlag and !requestScope.mobileFlag}">
         <jsp:include page="/seopods/breadCrumbs.jsp">
            <jsp:param name="reviewProfilePage" value="st_fix pros_brcumb"/>
          </jsp:include><%--Added breadcrumb Indumathi.S Feb-16-2016--%>
        </c:if>
        <%String tabClass = (!"RICH_SUB_PROFILE".equals(profileType)) ? "clr_lft_cnr prof_tab" : "clr_lft_cnr";%>
        <div class="<%=tabClass%>"> 
          <%if("RICH_SUB_PROFILE".equals(profileType)){%>
             <input type="hidden" id="spMyhcProfileId" value="<%=spMyhcProfileId%>"/><%--Added by Indumathi NOV-03-15 for sp rich profile--%>
          <%}%>          
          <div id="fixedscrolltop">          
            <c:if test="${not empty requestScope.interactionCollegeNameDisplay}">
              <h1 class="cf">${requestScope.interactionCollegeNameDisplay}</h1>
            </c:if>
            <c:if test="${not empty requestScope.spdeptName}">
              <h2>${requestScope.spdeptName}</h2>
            </c:if>  
            <c:if test="${not empty requestScope.listOfUniOverallReviewsInfo}">  
              <c:forEach var="uniReviewsInfo" items="${requestScope.listOfUniOverallReviewsInfo}">
                                              
                <c:if test="${not empty uniReviewsInfo.reviewCount}">    
                  <c:set var="reviewCnt" value="${uniReviewsInfo.reviewCount}"/>    
                  <%request.setAttribute("richProvReviewCnt",(String)pageContext.getAttribute("reviewCnt"));%>              
                  <c:if test="${not empty uniReviewsInfo.overallRating}">
                    <c:set var="rating" value="${uniReviewsInfo.overallRating}"/>
                   
										<h3 class="fnt_lrg fnt_14 lh_24">Student rating <span class="ml5 rat<%=(String)pageContext.getAttribute("rating")%> t_tip ttip_pctrl">
										<%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
										 <span class="cmp">
										   <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
											 <div class="line"></div>
										 </span>
										 <%--End of tool tip code--%>
									 </span>
                      (${requestScope.averageReviewRating})
											<%--Modified by Indumathi.S Mar-29-2016 review URL restructuring--%>			 
                      <c:if test="${not empty uniReviewsInfo.reviewCount}">
                        <c:if test="${uniReviewsInfo.reviewCount gt 0}">
                          <c:if test="${not empty requestScope.listOfLatestReviews}">
                            <a class="link_blue nowc vr_bold" id="rvw_skip_link" data-rvw-ga="${uniReviewsInfo.collegeNameDisplay}"  title="Reviews at ${uniReviewsInfo.collegeNameDisplay}"><spring:message code="view.review.link.name"/></a>
                          </c:if>
                        </c:if>
                      </c:if>                      
                    </h3>
                  </c:if>                
                </c:if>                
              </c:forEach>   
            </c:if>
            <%--Changed include file palce for Add to comparision, 03_Feb_2015 by Thiyagu G--%>
            <%--Added to hide compare heart in cmmt journey--%>                          
            <%if(!("clearing".equalsIgnoreCase(userJourney) && "RICH_INST_PROFILE".equalsIgnoreCase(profileType))){%>
            <div class="comp_sel fl">
              <c:if test="${not empty requestScope.collegeOverViewList}">
                <jsp:include page="/jsp/advertiser/ip/include/headShortlist.jsp">
                  <jsp:param name="richProfile" value="true"/>
                  <jsp:param name="basketCollegeName" value="<%=basketCollegeName%>"/>
                </jsp:include>
              </c:if> 
            </div>
            <%}%>
            <c:if test="${not empty requestScope.listOfCollegeInteractionDetailsWithMedia}">            
            <div class="fr mtmin btn_intr"><%--Modified code for sticky buttons Jan-27-16 Rel Indumathi.S--%>
              <div class="more_info">
                <a><%=mobileMenuText%></a>
              </div>
                <div class="btns_interaction fl <%=clrItrBtnCls%>" id="buttonCls">
                  <div class="int_cl">
                    <a class="" onclick="hm()"><i class="fa fa-times"></i></a>
                  </div>               
                  <jsp:include page="/advertpromotion/interactionbuttons/websiteProspectusFromBeanAndHrefCdPage.jsp" >
                    <jsp:param name="FLOAT_STYLE" value="LEFT" />
                    <jsp:param name="FROMPAGE" value="SHOWNONREQINFO" />
                  </jsp:include>               
                </div>
                </div>  
              </c:if>  
              <c:if test="${empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
                <c:if test="${not empty requestScope.CLEARING_COURSE_EXIST_FLAG}">
                  <c:if test="${requestScope.CLEARING_COURSE_EXIST_FLAG eq 'Y'}">
                  <div class="fr mtmin btn_intr">
                    <div class="more_info">
                      <a><%=mobileMenuText%></a>                          
            </div> 
            <div class="btns_interaction fl <%=clrItrBtnCls%>" id="buttonCls"> 
                      <div class="int_cl">
                        <a class="" onclick="hm()"><i class="fa fa-times"></i></a>
                      </div>   
                      <div class="">
                        <jsp:include page="/advertpromotion/interactionbuttons/websiteProspectusFromBeanAndHrefCdPage.jsp" >
                        <jsp:param name="FLOAT_STYLE" value="LEFT" />
                        <jsp:param name="FROMPAGE" value="RICH_PROFILE_PAGE" />
                      </jsp:include> 
                      </div>             
                    </div>
                   </div>
               </c:if>
             </c:if>
            </c:if>               
          </div>            
        </div>
      </div>
    </div> 
  </div>  
 
  <c:if test="${COVID19_SYSVAR eq 'ON'}">
    <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
    <div class="content-bg mt25">
      <div class="covid_upt mb25">
	    <span class="covid_txt"><%=new SessionData().getData(request, "COVID19_COURSE_TEXT")%></span>
	    <a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_blu_arw.svg", 0)%>"></a>
      </div>
    </div>
  </c:if>
      
  <div class="hero_cnr">
     <%--Modified the code by Indumathi.S for device specific images on Jan-24-17 Rel --%>  
    <c:if test="${not empty requestScope.richProfileImgPath}">       
    <!--  Modified the code for device specific image lazyloading by Minu rel 08-Dec-2020 -->
     <c:choose>
     <c:when test="${not empty requestScope.mobileFlag and !requestScope.mobileFlag}">
       <img id = "rchImage" class="clr_hero_img" src = '${requestScope.richProfileImgPath}'>
     </c:when>
     <c:otherwise>
       <img id = "h_image8" class="clr_hero_img" src = '<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-img ="${requestScope.richProfileImgPath}" data-src="${requestScope.richProfileImgPath}" data-src-mobile = "${requestScope.richProfileImgPath}" >
     </c:otherwise>
     </c:choose>
      <input type="hidden" id="heroImgPath" value="${requestScope.richProfileImgPath}" />
    </c:if> 
    <%--Added by Indumathi.S for Jan-27-16--%>
    <c:if test="${not empty requestScope.courseExistsFlag and requestScope.courseExistsFlag ne 'NO_COURSE'}">
        <div class="content-bg">
          <a class="rp btn1 btn3 w210" href="<%=request.getAttribute("courseUrl")%>">VIEW ALL COURSES<i class="fa fa-long-arrow-right"></i></a>
          <div class="rch_btn_bor"></div>
        </div>
    </c:if>
    <%--End--%>
  </div>
</section>
<%--Separated the code rich subject profile and rich institution profile May_31_2016--%>
<%if("RICH_SUB_PROFILE".equals(profileType)){%>
  <jsp:include page="/jsp/uni/include/spProfileContent.jsp"/>
<%} else {%>
  <jsp:include page="/jsp/uni/include/richUniqueContent.jsp"/>
<%}%>
<%--End May_31_2016--%>

<%--Key Stats Starts--%>
<%--<logic:notEmpty name="listOfUniStats" scope="request">--%>
  <div class="clear"></div>
  <jsp:include page="/jsp/uni/richprofile/include/keyStatsInfo.jsp"/>
<%--</logic:notEmpty>--%>
<%--Key Stats Ends--%>
<%--Reviews Starts--%>
<c:if test="${not empty requestScope.listOfLatestReviews}">
  <jsp:include page="/jsp/uni/richprofile/include/latestReviewPod.jsp"/>
</c:if>
<%--Reviews Ends--%>
<jsp:include page="/jsp/uni/richprofile/include/openDayScholarshipsPod.jsp"/>
<%--Hide location flag code added by Prabha on 31_May_16--%>
<%if("N".equalsIgnoreCase((String)request.getAttribute("hideLocationFlag"))){%>
  <c:if test="${not empty requestScope.locationInfoList}">
    <jsp:include page="/jsp/uni/richprofile/include/richProfileLocationMap.jsp"/>
  </c:if>
<%}%>
<%--End of hide location flag changes--%>
<%--Added by Indumathi.S for Cost of pint Nov-24-15--%>
<c:if test="${not empty requestScope.costOfPint}">
  <jsp:include page="/jsp/uni/include/costOfPint.jsp"/>
</c:if>
<%--End--%>

<c:if test="${not empty requestScope.uniSPList}">
  <jsp:include page="/jsp/uni/richprofile/include/richProfileSpList.jsp"/>
</c:if>  
<%--Modified the code by Indumathi.S May_31_2016--%>
<c:if test="${not empty requestScope.richProfileVidUrlPath and not empty requestScope.ColgSubOrderItemId}">
    <section class="reviewscnt p0">    
    <%--Added lazy loading code for rich video image by Prabha on 13_06_17--%>
      <div id="h_image2" data-src="${requestScope.richProfileVidTmbPath}" style="background:url('<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>') no-repeat center center;background-size:cover !important; height:371px;">
      <%--End of code--%>
        <div id="richVideoLink" class="vidpl">
          <a onclick="javascript:richVideoJsLoad('${requestScope.richProfileVidUrlPath}','${requestScope.richProfileVidTmbPath}');richProfileVideoStatsLog('<%=collegeId%>','${requestScope.ColgSubOrderItemId}','<%=session.getAttribute("cpeQualificationNetworkId")%>','<%=profileId%>','RICH_PROFILE_SECVIDEO');searchEventTracking('richvideo','clicked','<%=gaCollegeName%>')"><i class="fa fa-play-circle-o"></i></a>
        </div>  
      </div>
      <%-- Full width video container added by Prabha on 31_May_2016--%>
      <div id="richVideoPod" class="rp_vdcnt rp_vdinr">
        <div class="vd_inner">
	         <div class="rpvd_clse"><a onclick="closeRichVideo();" class="#"><i class="fa fa-times"></i></a></div>
        <div align="center" id="flashbanner1"></div>
        </div>
      </div>    
      <%-- End of the full width video container code--%>    
    </section>      
</c:if>
<%--End--%>
<%if(!GenericValidator.isBlankOrNull(profileType) && !"RICH_SUB_PROFILE".equals(profileType)){%>
<%--Load the article pod through ajax call, added on 13_Jun_2017, By Thiyagu G.--%>
<section class="comn_art p0 mb40">
		<div class="content-bg">
    <section class="artfc p0 mt38 cm_art_pd" id="articlesPod"></section>
  </div>
  <input type="hidden" id="scrollStop" value="" />
</section>
<%}%>
<jsp:include page="/jsp/uni/richprofile/include/socialList.jsp"/>
<%--Added by priyaa for Final 5 - 21-JUL-2015_REL - Start of change--%>
<c:if test="${not empty requestScope.addedToFinalChoice and requestScope.addedToFinalChoice eq 'N'}">
    <div class="myFnChLbl" style="display:none;">                    
      ${requestScope.collegeNameDisplay}#${requestScope.collegeId}
    </div>               
</c:if>
<%--end of change--%>
 <%--Changed WU scheme as part of SSL work for 08_MAR_2016, By Thiyagu G.--%>
 <%String browserip = GlobalConstants.WHATUNI_SCHEME_NAME + new CommonUtil().getProperties().getString("review.autocomplete.ip");%>
<input type="hidden" id="refererUrl" name="refererUrl" value="<%=(request.getHeader("referer") !=null ? request.getHeader("referer") : browserip+"/degrees/home.html")%>" />
<input type="hidden" id="contextPath" name="contextPath" value="/degrees" />
<input type="hidden" id="collegeId" value="<%=request.getAttribute("collegeId")%>"/>
<input type="hidden" id="networkId" value="<%=session.getAttribute("cpeQualificationNetworkId")%>"/>
<input type="hidden" id="richProfileType" value="<%=request.getAttribute("richProfileType")%>"/>
<input type="hidden" id="extraPod" value="false"/>
<input type="hidden" id="mblOrient"/>
</div>
<input type="hidden" id="switchProvUrl" value="<%=requestURL%>"/><%-- Yogeswari :: 30.06.2015 :: changed for Profile tabs on existing profile page task --%>
<input type = "hidden" id = "mobileFlag" value = "${requestScope.mobileFlag}"/>
  <input type="hidden" id="lightBoxResp" value=""/>
  <%request.setAttribute("noJSLogging","false");%>
  <jsp:include page="/jsp/common/wuFooter.jsp" />
  <%-- ::START:: wu582_20181023 - Sabapathi: added script for stats logging on page load  --%>
  <script type="text/javascript">
    var $ = jQuery.noConflict();
    $(document).ready(function() {
      richProfSecStatsLog('logOnLoad');
    });
  </script>
  <%-- ::END:: wu582_20181023 - Sabapathi: added script for stats logging on page load  --%>
</body>