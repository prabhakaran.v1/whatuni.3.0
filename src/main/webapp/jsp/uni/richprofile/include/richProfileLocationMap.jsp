<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import=" WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" %>

<%
  String latitudeStr =  (String)request.getAttribute("latitudeStr");   
  String longitudeStr =  (String)request.getAttribute("longitudeStr");  
  String map_url = GlobalConstants.MAP_URL + "&size=540x384&scale=2&markers=color:red|";
  String loadNewMapJs = CommonUtil.getResourceMessage("wuni.load.new.map.js", null);
%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=loadNewMapJs%>"></script>
<section class="p0 mt30">
  <div class="course_deatils">    
    <div class="content-bg">
      <input type="hidden" id="latitude" value="<%=latitudeStr%>"/>
      <input type="hidden" id="longitude" value="<%=longitudeStr%>"/>
      <h2 class="fnt_lrg lh_26 fnt_24 mb12 bor_rom mbxhd">Where you'll study<span id="act_map"><i class="fa fa-plus-circle color1 pr0"></i></span></h2>    
    </div>
    <div class="study_loc ric_loc mt0">
      <div class="fl w100p">
        <c:forEach var="venue" items="${requestScope.locationInfoList}"> <%--Removed itemprop, itemscope and itemtype on 16_May_2017, By Thyagu G--%>
          <div id="locationinfoMaphh" style="display:none;">
            <p style="font-weight:bold;">
              <strong>
                <c:if test="${not empty venue.addLine1}">
                  ${venue.addLine1}, 
                </c:if>
                 <c:if test="${not empty venue.addLine2}">
                  ${venue.addLine2}, 
                </c:if>
                 <c:if test="${not empty venue.town}">
                  ${venue.town}, 
                </c:if>
                 <c:if test="${not empty venue.countryState}">
                  ${venue.countryState}, 
                </c:if>
                ${venue.postcode}                         
              </strong><br/>
            </p>
          </div>
          <div class="bghgim map_vis map_staimg">
          <%--Changed static google map to mapbox for Feb_12_19 rel by Sangeeth.S--%>
            <jsp:include page="/jsp/common/mapbox.jsp" />                               
          </div>
					<input type="hidden" id="mapDivContent" name="mapDivContent" value="0"/><%--Added by Prabha for lazy load map on 27_JAN_2016_REL--%>
          <div class="content-bg">
          <div class="fl thinkcon">
            <div>
              <span><h2>${venue.collegeNameDisplay}</h2></span>
              <span class="address">
                <c:if test="${not empty venue.addLine1}">
                  <p>${venue.addLine1}</p>
                </c:if>
                 <c:if test="${not empty venue.addLine2}">
                  <p>${venue.addLine2}</p>
                </c:if>
              </span>
              <c:if test="${not empty venue.town}">
                <span class="address">
                  <p>${venue.town}</p>
                </span>
              </c:if>
              <c:if test="${not empty venue.countryState}">
                <span class="address">
                  <p>${venue.countryState}</p>
                </span>
              </c:if>  
              <span class="address">
                <p>${venue.postcode}</p>
              </span>
              <%--Added condition for hiding the text for non-uk providers by prabha on 04_OCT_2017--%>
              <c:if test="${venue.isInLondon ne 'NONUK'}">
                <p>United Kingdom</p>
              </c:if>
              <%--End of code--%>
              <%--Hiding train/tube station for non uk providers by Prabha on 04_oct_2017--%>
              <c:if test="${venue.isInLondon eq 'LONDON'}">                           
                <c:if test="${not empty venue.nearestTubeStation}">
                  <p class="mt10"><span class="fnt_lbd">Nearest tube station: </span>${venue.nearestTubeStation}&nbsp; ${venue.distanceFromTubeStation} miles away</p>
                </c:if>  
                <c:if test="${not empty venue.nearestStationName}">
                  <p class="mt10"><span class="fnt_lbd">Nearest train station: </span>${venue.nearestStationName}&nbsp; ${venue.distanceFromNearestStation} miles away</p>
                </c:if>  
              </c:if>
              <c:if test="${venue.isInLondon ne 'LONDON'}"> 
                <c:if test="${not empty venue.nearestStationName}">
                  <p class="mt10"><span class="fnt_lbd">Nearest train station: </span>${venue.nearestStationName}&nbsp; ${venue.distanceFromNearestStation} miles away</p>
                </c:if>
              </c:if>                     
            </div>
          </div>  
          <c:if test="${venue.cityGuideDisplayFlag eq 'Y'}">
            <div class="fr think_loc">          
              <h2>Thinking of studying in ${venue.location}?</h2>
              <p class="mt30">Check out our</p> 
              <p><a href="${venue.cityGuideUrl}" class="link_blue fnt_lbd">${venue.location} city guide</a></p>
            </div>                
          </c:if>
          <div class="borderbot mt35 fl"></div>
        </div>          
      </c:forEach>                    
    </div>
  </div>    
 </div>
</section>