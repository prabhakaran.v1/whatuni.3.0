<%--
  * @purpose:  jsp for showing wusca rattings..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 31-May-2016    Indumathi.S               1.0      First draft           wu_553
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction"%>

<%String indexVal = !GenericValidator.isBlankOrNull(request.getParameter("indexVal")) ? request.getParameter("indexVal") : "";
String profileRoundRating = !GenericValidator.isBlankOrNull(request.getParameter("profileRoundRating")) ? request.getParameter("profileRoundRating") : "";
String sectionNameDisplay = !GenericValidator.isBlankOrNull(request.getParameter("sectionNameDisplay")) ? request.getParameter("sectionNameDisplay") : "";
String profileRating = !GenericValidator.isBlankOrNull(request.getParameter("profileRating")) ? request.getParameter("profileRating") : "";
String wuscaToolTip = !GenericValidator.isBlankOrNull(request.getParameter("wuscaToolTip")) ? request.getParameter("wuscaToolTip") : "";
String curYear1 = new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS");
pageContext.setAttribute("curYear1", curYear1);
%>
 
<%if(!GenericValidator.isBlankOrNull(request.getParameter("profileRoundRating")) && !GenericValidator.isBlankOrNull(request.getParameter("sectionNameDisplay"))) {%>
    <div class="mxHg calc_ctrl">
      <% String toolTipSectionName = "University of the year";
      if(!GenericValidator.isBlankOrNull(sectionNameDisplay)){
        if(sectionNameDisplay.contains("Overview")) {%>
          <span class="wusca_txt">OVERALL SCORE</span>
        <%} else {   
        if("Student union".equalsIgnoreCase(sectionNameDisplay)){
        sectionNameDisplay = "Students' union";
         }
        %>
          <span class="wusca_txt"><%=sectionNameDisplay%> rating</span>
      <% toolTipSectionName = sectionNameDisplay;
      pageContext.setAttribute("toolTipSectionName", toolTipSectionName);
     
      }}%>
      <div class="rate_trophy" onclick="showAndHideToolTip('<%=wuscaToolTip%><%=indexVal%>');" onmouseover="showToolTip('<%=wuscaToolTip%><%=indexVal%>');" onmouseout="hideToolTip('<%=wuscaToolTip%><%=indexVal%>');">
        <div class="rp_ttip_ctrl">
          <span class="mt5 rt_txt">
            <%String wuscaClass = "";
              for(int i=1; i<=5; i++) {
                wuscaClass = (i <= Integer.parseInt(profileRoundRating)) ? "fa fa-trophy" : "fa fa-trophy trop_gry";%>
                <i class="<%=wuscaClass%>"></i>
            <%}%>
          </span>
        <div class="cmp hdf5" style="display:none" id="<%=wuscaToolTip%><%=indexVal%>">This is this uni's WUSCA ${curYear1} ranking for the ${toolTipSectionName} category. See the full league table <a href="/student-awards-winners/university-of-the-year/">here</a></div>
          (<%=profileRating%>) 
        </div>
      </div>
    </div>
  <%}%>
