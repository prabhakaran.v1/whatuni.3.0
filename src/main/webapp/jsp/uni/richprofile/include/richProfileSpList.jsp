<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil"%>

<section class="department deim mb40 p0 course_deatils">
  <div class="content-bg course_deatils">  
    <h2 class="fnt_lrg fnt_24 lh_26 gryC">Other academic departments</h2>
    <c:forEach var="uniSPList" items="${requestScope.uniSPList}" begin="0" end="3" varStatus="moreIndex">
      <div class="mt20 fl">
        <c:if test="${not empty uniSPList.imagePath}">
          <div class="fl img_ppn">
            <a href="${uniSPList.spURL}"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="${uniSPList.imagePath}" alt="${uniSPList.advertName}"></a>
          </div>
        </c:if>
        <c:if test="${empty uniSPList.imagePath}">
          <div class="fl img_ppn noimage"></div>
        </c:if>        
        <div class="div_right">        
          <c:if test="${not empty uniSPList.advertName}">
            <h2><a class="link_blue" href="${uniSPList.spURL}">${uniSPList.advertName}</a></h2>
          </c:if>
          <c:if test="${not empty uniSPList.advertDescShort}">
            <p class="mt5 pos_rel">
              ${uniSPList.advertDescShort}...
              <a class="pos_abs_rb hdf4 link_blue" href="${uniSPList.spURL}">READ MORE</a>
            </p>
          </c:if>
        </div>
      </div>
    </c:forEach>              
    <c:if test="${requestScope.uniSPListSize gt 4}">
      <div class="fl w100p">
        <a class="clear btn1 btn2 mt30 w195" id="spViewMorwLink" onclick="javascript:showMoresLess();"><i class="fa fa-plus" ></i>VIEW MORE</a>
      </div>
    </c:if>
    <div id="spMoreDiv" style="display:none;">
      <c:forEach var="uniSPLists" items="${requestScope.uniSPList}" begin="4" varStatus="indexs">    
        <div class="mt20 fl">
          <c:if test="${not empty uniSPLists.imagePath}">
            <div class="fl img_ppn">
              <a href="${uniSPLists.spURL}"><img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="${uniSPLists.imagePath}" alt="${uniSPLists.advertName}"></a>
            </div>
          </c:if> 
          <c:if test="${empty uniSPLists.imagePath}">
            <div class="fl img_ppn noimage"></div>
          </c:if>                  
          <div class="div_right">          
            <c:if test="${not empty uniSPLists.advertName}">
              <h2><a class="link_blue" href="${uniSPLists.spURL}">${uniSPLists.advertName}</a></h2>
            </c:if>
            <c:if test="${not empty uniSPLists.advertDescShort}">
              <p class="mt5 pos_rel">
                ${uniSPLists.advertDescShort}...
                <a class="pos_abs_rb hdf4 link_blue" href="${uniSPLists.spURL}">READ MORE</a>
              </p>
            </c:if>
          </div>
        </div>
      </c:forEach>
    </div>
    <c:if test="${requestScope.uniSPListSize gt 4}">
      <div class="fl w100p">
        <a style="display:none;" id="spViewLessLink" class="clear btn1 btn2 mt30" onclick="javascript:showMoresLess();"><i class="fa fa-minus"></i>VIEW LESS</a>
      </div>
    </c:if>
  </div>
</section> 