<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.wuni.util.seo.SeoUrls"%>


<%
  String scholarShipViewAllUrl = "";
  String collegeId = "", collegeName="" , openExtraClass= "";  
  if(request.getAttribute("collegeId")!=null && !"".equals(request.getAttribute("collegeId"))){
    collegeId = (String)request.getAttribute("collegeId");
  }
  if(request.getAttribute("collegeName")!=null && !"".equals(request.getAttribute("collegeName"))){
    collegeName = (String)request.getAttribute("collegeName");
  }
  scholarShipViewAllUrl = new SeoUrls().constructUniSpecificScholarshipUrl(collegeId,collegeName);
%>

<section class="p0 mt35">
<div class="content-bg course_deatils">
  <div class="pfos">
    <c:if test="${not empty requestScope.listOfOpendayInfo}">
      <div class="pfopd fl non_schol">
        <div class="pftc">
          <h2 class="mb25 fnt_24"><spring:message code="open.day.pod.title.text"/></h2>
          <c:forEach var="openDaysList" items="${requestScope.listOfOpendayInfo}" varStatus="index">
          <c:set var="indexValue" value="${index.index}"/>  
            <%if(Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1==2){openExtraClass ="mt23";}%>
            <div class="pftci">
              <c:set var="collNameDip" value="${openDaysList.collegeName}"/>
              <%-- <h2 class="<%=openExtraClass%>"><a href="<SEO:SEOURL pageTitle="uni-open-days" ><%=(String)pageContext.getAttribute("collNameDip")%>#${openDaysList.collegeId}#${openDaysList.collegeLocation}</SEO:SEOURL>"  class="link_blue"  title="${openDaysList.opendayDate}"><i class="fa fa-calendar"></i>${openDaysList.opendayDate}</a></h2> --%>              
              <h2 class="<%=openExtraClass%>">
                <a href="<SEO:SEOURL pageTitle="uni-open-days" ><%=(String)pageContext.getAttribute("collNameDip")%>#${openDaysList.collegeId}#${openDaysList.collegeLocation}</SEO:SEOURL>"  class="link_blue"  title="${openDaysList.opendayDate}">
                  <c:if test="${openDaysList.eventCategoryId eq '6'}">
                   <spring:message code="virtual.tour.title"/>
                  </c:if>
                  <c:if test="${not empty openDaysList.opendayDate and openDaysList.eventCategoryId eq '7'}">
                   <spring:message code="online.event.title" arguments="${openDaysList.opendayDate}"/>
                  </c:if>
                  <c:if test="${not empty openDaysList.opendayDate and openDaysList.eventCategoryId eq '5'}">
                   <i class="fa fa-calendar"></i>${openDaysList.opendayDate}
                  </c:if>
                </a>
              </h2>   
              <p class="fnt_lbd">${openDaysList.headline}</p>
              <spring:message code="event.type.mode.label" var="eventMode"/>
              <p>${openDaysList.eventCategoryId eq '5' ? openDaysList.opendayVenue : eventMode}</p>
              <%-- <c:if test="${not empty openDaysList.opendayDesc}">                
                <c:set var="openDayCntDesc" value="${openDaysList.opendayDesc}"/>
                <% //Added read more link Indumathi.S Feb-16-2016
                String openDayCntDesc = (String)pageContext.getAttribute("openDayCntDesc");
                if(openDayCntDesc.length()>120){ %>
                  <p><%=openDayCntDesc.substring(0,119)%>...<a href="<SEO:SEOURL pageTitle="uni-open-days" ><%=(String)pageContext.getAttribute("collNameDip")%>#${openDaysList.collegeId}#${openDaysList.collegeLocation}</SEO:SEOURL>">Read more</a>
                <%}else{%>
                   <p><%=openDayCntDesc%></p>
                <%}%>
              </c:if> --%>
            </div>
          </c:forEach>                    
        </div>
        <c:forEach var="openDaysList" items="${requestScope.listOfOpendayInfo}" varStatus="index" end="0">
         <c:set var="collName" value="${openDaysList.collegeName}"/>
         <spring:message code="view.open.day.button.text" var="viewODBtnLabel"/>
         <div><a href="<SEO:SEOURL pageTitle="uni-open-days" ><%=(String)pageContext.getAttribute("collName")%>#${openDaysList.collegeId}#${openDaysList.collegeLocation}</SEO:SEOURL>" class="btn1 btn3 mt30 w190 fxwd" title="${viewODBtnLabel}">${viewODBtnLabel}<i class="fa fa-long-arrow-right"></i></a></div>
        </c:forEach>
      </div>
    </c:if>
  </div>  
  <div class="borderbot mt40 fl"></div>
</div>
</section>