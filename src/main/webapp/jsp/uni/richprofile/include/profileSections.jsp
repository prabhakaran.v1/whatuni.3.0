<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO"%>
<%@page import=" WUI.utilities.CommonUtil" %>

<c:if test="${not empty requestScope.collegeOverViewList}">
  <div id="rpProfileSec" style="display:none;">
    <c:forEach var="profileSection" items="${requestScope.collegeOverViewList}" varStatus="index">   
    <c:set var="indexValue" value="${index.index}"/>                                     
      <div id="proSec<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" style="display:none;">        
        <div id="crd_desc" class="mxHg ">
          <h2 class="sub_tit fl fnt_lrg lh_22">${profileSection.selectedSectionName}</h2>
          <div class="clear"></div>
          ${profileSection.profileDescription}                        
        </div>              
      </div>  
      <input type="hidden" id="richSecMyhcProfId_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>" value="${profileSection.myhcProfileId}"/>
    </c:forEach>                           
    <%--Slider back link is change by Prabha on 27-JAN-2016--%>
		<div class="rolC rwetm rwet pb5">
      <div class="rolCL">
        <a href="javascript:backToIntro();"><span data-hover="MAIN SECTION" class="rolB">MAIN SECTION</span></a>
      </div>
			<div class="rolCL fr sli_clrchg">
        <span id="sec_prev_lnk" style="display: inline;"><a href="javascript:sliderPlay('prev');"><i class="fa fa-long-arrow-left fl mr10"></i><span data-hover="PREVIOUS">PREVIOUS</span></a></span>
				<a class="fr" href="javascript:sliderPlay('next');"><i class="fa fa-long-arrow-right fr mr10"></i><span data-hover="NEXT">NEXT</span></a>
      </div>
		</div>  
		<%--End of the Slider back code--%>
  </div>
</c:if>
