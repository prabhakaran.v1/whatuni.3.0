<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import=" WUI.utilities.CommonUtil"%>

<% CommonUtil commonUtil = new CommonUtil();
 String userNameColorClassName = "rev_blue";
%>
<div class="rev_boxmn">
  <c:if test="${empty requestScope.latestSubjectAjaxReview}">
  <c:if test="${not empty requestScope.listOfLatestReviews}">
    <c:forEach var="latestReviews" items="${requestScope.listOfLatestReviews}" varStatus="cnt">
              <div class="rev_box">
                <div class="rlst_row">
                  <div class="revlst_lft">
                    <div class="rlst_wrp">
                      <c:if test="${not empty latestReviews.userNameInitial}">
                        <c:set var="userName" value="${latestReviews.userNameInitial}"/> 
                        <%
                          userNameColorClassName = commonUtil.getReviewUserNameColorCode((String)pageContext.getAttribute("userName"));
                        %>
                        <div class="rev_prof <%=userNameColorClassName%>">${latestReviews.userNameInitial}</div>
                    </c:if>
                      <div class="rlst_rht">
                        <div class="rev_name">${latestReviews.reviewerName}</div>
                        <div class="rev_dte">${latestReviews.reviewDate}</div>
                      </div>
                    </div>
                  </div>
                  <div class="revlst_rht">
                    <div class="rlst_wrap">
                      <h2>
                         <c:if test="${latestReviews.courseDeletedFlag ne 'TRUE'}">
                           <a href="${latestReviews.courseDetailsReviewURL}" title="${latestReviews.courseName}">
                             ${latestReviews.courseName}
                           </a>
                          </c:if>
                         <c:if test="${latestReviews.courseDeletedFlag eq 'TRUE'}">
                           <span class="del_crse">${latestReviews.courseName}</span>
                         </c:if>
                      </h2>
                      <div class="reviw_rating">
                        <div class="rate_new"><span class="ml5 rat rat${latestReviews.reviewRatings}"></span>
                          <div class="rw_qus_des">${latestReviews.reviewDesc}...</div>
                        </div>
                        <div class="rev_mre"><a onclick="ajaxReviewLightBox(${latestReviews.reviewId})">Read more</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
               </c:forEach>
               </c:if>
               </c:if>
                <c:if test="${not empty requestScope.latestSubjectAjaxReview}">
               <c:forEach var="latestSubjectReviews" items="${requestScope.latestSubjectAjaxReview}" varStatus="cnt">
              <div class="rev_box">
                <div class="rlst_row">
                  <div class="revlst_lft">
                    <div class="rlst_wrp">
                     <c:if test="${not empty latestSubjectReviews.userNameInitial}">
                        <c:set var="userInitialName" value="${latestSubjectReviews.userNameInitial}"/> 
                        <%
                          userNameColorClassName = commonUtil.getReviewUserNameColorCode((String)pageContext.getAttribute("userInitialName"));
                        %>
                        <div class="rev_prof <%=userNameColorClassName%>">${latestSubjectReviews.userNameInitial}</div>
                    </c:if>
                      <div class="rlst_rht">
                        <div class="rev_name">${latestSubjectReviews.reviewerName}</div>
                        <div class="rev_dte">${latestSubjectReviews.reviewDate}</div>
                      </div>
                    </div>
                  </div>
                  <div class="revlst_rht">
                    <div class="rlst_wrap">
                      <h2>
                        <c:if test="${latestSubjectReviews.courseDeletedFlag ne 'TRUE'}">
                          <a href="${latestSubjectReviews.courseDetailsReviewURL}" title="${latestSubjectReviews.courseName}">
                            ${latestSubjectRseviews.courseName}
                          </a>
                        </c:if>
                        <c:if test="${latestSubjectReviews.courseDeletedFlag eq 'TRUE'}">
                          <span class="del_crse">${latestSubjectReviews.courseName}</span>
                        </c:if>
                      </h2>
                      <div class="reviw_rating">
                        <div class="rate_new"><span class="ml5 rat rat${latestSubjectReviews.reviewRatings}"></span>
                          <div class="rw_qus_des">${latestSubjectReviews.reviewDesc}...</div>
                        </div>
                        <div class="rev_mre"><a onclick="ajaxReviewLightBox(${latestSubjectReviews.reviewId})">Read more</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
               </c:forEach>
                </c:if>
            </div>