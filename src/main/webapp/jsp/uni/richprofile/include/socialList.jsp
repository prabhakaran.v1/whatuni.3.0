<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction" %>
<%String instagramName = request.getAttribute("instagramURL") != null ? (String)request.getAttribute("instagramURL") : "";
String instagram_SYS_VAR = new CommonFunction().getWUSysVarValue("INSTAGRAM_SYSVAR");%>
<section class="soc_art p0 rich_profile">
  <div class="content-bg ">
    <article class="soc_pod pt37 course_deatils">
      <h2 class="sub_tit fnt_lrg lh_26">Latest news</h2>
      <div class="soc_ico">
        <ul>
          <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
            <li id="twitterTab" onclick="showhidesocialTabs('twitter')" class="act"><i class="fa fa-twitter fa-1_5x"></i>Twitter</li>
          </c:if>
          <li id="facebookTab" onclick="loadFBJS();"><i class="fa fa-facebook fa-1_5x"></i>Facebook</li>
          
          <%if("ON".equalsIgnoreCase(instagram_SYS_VAR)){
          if(!GenericValidator.isBlankOrNull(instagramName)){%>
            <li id="instagramTab" onclick="showhidesocialTabs('instagram');"><i class="fa fa-instagram"></i>Instagram</li> <%--Added instagram tab by Prabha on 31_May_16--%>
          <%}
          }%>
        </ul>
      </div> 
      <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">          
      <div id="twitterLnk" class="soc_cnt mb30">
        <div class="soc_disp">                  
          <c:if test="${not empty requestScope.providerTwitterName}">
            <a class="twitter-timeline" data-screen-name="${requestScope.providerTwitterName}" data-chrome="noheader nofooter transparent" height="400" width="400" data-border-color="#dbdbdb" data-link-color="#40ccfe"  data-cards="hidden" href="${requestScope.providerTwitterUrl}"  data-widget-id="392256738243522561">Tweet by @<%=request.getAttribute("collegeNameDisplay")%></a>
          </c:if>
          <c:if test="${empty requestScope.providerTwitterName}">
            <a class="twitter-timeline" data-screen-name="" data-chrome="noheader nofooter transparent" height="400" width="400" data-border-color="#dbdbdb" data-link-color="#40ccfe"  data-cards="hidden" href="https://twitter.com/whatuni"  data-widget-id="392256738243522561">Tweet by @Whatuni</a>
          </c:if>  
        </div>
      </div>
      </c:if>
      <div id="facebookLnk" class="soc_cnt mb30" style="display:none;">
        <div class="soc_disp">  
          <div id="fb-root"></div>
          <div id="facebookDiv" class="richfbs"></div>
        </div>     
      </div>
      <div id="instagramLnk" class="soc_cnt mb30" style="display:none;">
        <div class="soc_disp">  
          <div id="instagramCntDiv"></div>
        </div>     
      </div>            
    </article>
  </div>
</section>
<c:if test="${not empty requestScope.providerFaceBookUrl}">
  <input type="hidden" id="provFbUrl" value="${requestScope.providerFaceBookUrl}"/>    
</c:if>
<%--Added instagram related hidden inputs by Prabha on 31_May_16--%>
<input type="hidden" name="instaId" id="instaId" value=""/>
<input type="hidden" name="instaName" id="instaName" value="<%=instagramName%>"/>
<input type="hidden" name="TagName" id="TagName" value="<%=request.getAttribute("collegeNameDisplay")%>"/>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/common/socialPodJs.js"></script>
<c:if test="${'N' ne sessionScope.sessionData['cookieTargetingCookieDisabled']}">
  <script type="text/javascript">
    jQuery(document).ready(function() {
      loadFBJS();
    });
  </script>
</c:if>