<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%
  String newUniReviewUrl = request.getAttribute("newUniReviewUrl") != null ? request.getAttribute("newUniReviewUrl").toString():"";
  String questionTitle = (String)request.getAttribute("questionTitle");
  String collegeId = (String)request.getAttribute("collegeId");
  String commonPhraseStlye = "display:block";
  String commonPhraseClassName = "defaultCP";
  String rating = (String)request.getAttribute("rating");
  String reviewExact = (String)request.getAttribute("reviewExact");
  String reviewCount = (String)request.getAttribute("reviewCount");
%>
<c:if test="${not empty requestScope.reviewBreakdownlist}">
  <div class="rev_bdwn">
    <div class="lrhd_sec">
      <h2 class="sub_tit fnt_lrg fl txt_lft whclr">Review breakdown</h2>
      <fieldset class="fs_col2 fr" id="fieldsetId" onclick="openReviewDropdown()">    
        <div class="od_dloc" id="question1"><span title="<%=questionTitle%>"><%=questionTitle%></span><span><i class="fa fa-angle-down"></i></span></div>
        <div class="opsr_lst" id="reviewDropDown">
          <ul>       
            <c:forEach var="reviewBreakdownlist" items="${requestScope.reviewBreakdownlist}" varStatus="index">  
            <c:set var="indexValue" value="${index.index}"/>
              <li class="liclass" onclick="changeQuestion(<%=pageContext.getAttribute("indexValue").toString()%>);closeDropdown()" id="dropdownid_<%=pageContext.getAttribute("indexValue").toString()%>"><span><a class="liclass"> ${reviewBreakdownlist.questionTitle}</a> </span></li>
            </c:forEach>
          </ul>
        </div>        
      </fieldset>
    </div>
    <h3 id="cdRating" class="rev_srat fnt_lrg fnt_14 lh_24">Student rating <span class="ml5 rat<%=rating%> t_tip">
    <span class="cmp" style="display:none">
      <div class="hdf5">This is the overall rating calculated by averaging all live reviews for this uni on Whatuni.</div>
      <div class="line"></div>
    </span>
    </span>( <%=reviewExact%> )
    <span>
    <%=reviewCount%> reviews
    </span>
    </h3>
    <c:forEach var="reviewBreakdownlist1" items="${requestScope.reviewBreakdownlist}" varStatus="index"> 
    <c:set var="indexValue" value="${index.index}"/>
      <%
      //String starStyle = "display:none";
      if(Integer.parseInt(pageContext.getAttribute("indexValue").toString()) == 0){
        //starStyle = "display:block";
      %>
      <div class="rvbar_cnt" id="star_<%=pageContext.getAttribute("indexValue").toString()%>"> 
        <div class="rvbar_wrp">    
          <span class="stra_txt fl">5 star</span>
          <span class="stra_bar fl"><span id="5starWidthId" class="fl" style="width:${reviewBreakdownlist1.fiveStarPercent}%"></span></span>
          <span class="stra_per fl" id="5StarId">${reviewBreakdownlist1.fiveStarPercent}%</span>
        </div>
        <div class="rvbar_wrp">
          <span class="stra_txt fl">4 star</span>
          <span class="stra_bar fl"><span id="4starWidthId" class="fl" style="width:${reviewBreakdownlist1.fourStarPercent}%"></span></span>
          <span class="stra_per fl" id="4StarId">${reviewBreakdownlist1.fourStarPercent}%</span>
        </div>
        <div class="rvbar_wrp">
          <span class="stra_txt fl">3 star</span>
          <span class="stra_bar fl"><span id="3starWidthId" class="fl" style="width:${reviewBreakdownlist1.threeStarPercent}%"></span></span>
          <span class="stra_per fl" id="3StarId">${reviewBreakdownlist1.threeStarPercent}%</span>
        </div>
        <div class="rvbar_wrp">
          <span class="stra_txt fl">2 star</span>
          <span class="stra_bar fl"><span id="2starWidthId" class="fl" style="width:${reviewBreakdownlist1.twoStarPercent}%"></span></span>
          <span class="stra_per fl" id="2StarId">${reviewBreakdownlist1.twoStarPercent}%</span>
        </div>
        <div class="rvbar_wrp">
          <span class="stra_txt fl">1 star</span>
          <span class="stra_bar fl"><span id="1starWidthId" class="fl" style="width:${reviewBreakdownlist1.oneStarPercent}%"></span></span>
          <span class="stra_per fl" id="1StarId">${reviewBreakdownlist1.oneStarPercent}%</span>
        </div>              
      </div>
      <%}%>
      <input type="hidden" id="5star_${indexValue}" value="${reviewBreakdownlist1.fiveStarPercent}"/><input type="hidden" id="4star_${indexValue}" value="${reviewBreakdownlist1.fourStarPercent}"/>  
      <input type="hidden" id="3star_${indexValue}" value="${reviewBreakdownlist1.threeStarPercent}"/> <input type="hidden" id="2star_${indexValue}" value="${reviewBreakdownlist1.twoStarPercent}"/><input type="hidden" id="1star_${indexValue}" value="${reviewBreakdownlist1.oneStarPercent}"/>
    </c:forEach>
    <div class="odsrch_cnt rrsrch_cnt rev_men ipro_rvmen">
      <div class="odsrh_col1 fl revm_lft">
        <label class="rrev_txt">Search reviews that mention</label>
        <fieldset class="odsrh_fie">
          <form name="reviewSearchKwdForm" id="reviewSearchKwdForm" method="post" action="/home.html" onsubmit="javascript:return commonPhrasesListProfilePageURL('reviewSearchKwdForm')">
            <input type="text" name="reviewSearchKwd" id="reviewSearchKwd" onkeyup="validateSrchIcn('key_icn','reviewSearchKwd')" onkeydown="validateSrchIcn('key_icn','reviewSearchKwd')" autocomplete="off" value="Something specific?" onclick="clearReviewSearchText(this,'Something specific?')" onblur="setReviewSearchText(this,'Something specific?')">
          </form>
          <i class="fa fa-search srch_dis" id="key_icn" aria-hidden="true" onclick="javascript:return commonPhrasesListProfilePageURL('reviewSearchKwdForm')"></i>
        </fieldset>
      </div>
      <c:if test="${not empty requestScope.commonPhrasesList}">
          <div class="odsrh_col1 fl revm_rht">
            <div class="swrd_cnt"><span>or</span></div>
            <div class="swrd_cnt">
              <form name="reviewCommonPhraseKwdForm" id="reviewCommonPhraseKwdForm" method="post" action="/home.html" onsubmit="javascript:return commonPhrasesListProfilePageURL('reviewCommonPhraseKwdForm')">
                <ul>
                <c:forEach items="${requestScope.commonPhrasesList}" var="commonPhraseListId"  varStatus="index">
                <c:set var="indexValue" value="${index.index}"/>  
                  <%if(Integer.parseInt(pageContext.getAttribute("indexValue").toString()) > 2){
                      commonPhraseStlye = "display:none";                    
                      commonPhraseClassName = "extraCP";   
                  }%>
                  <li style="<%=commonPhraseStlye%>" class="<%=commonPhraseClassName%>" id="comPhrase_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>" onclick="commonPhrasesListProfilePageURL('reviewCommonPhraseKwdForm','',this)" value="${commonPhraseListId.commonPhrases}"><a href="javascript:void(0);" title="${commonPhraseListId.commonPhrases}">${commonPhraseListId.commonPhrases}</a></li>          
                  </c:forEach>
                  </ul>
                </form>
              </div>
              <div class="swrd_cnt">
                <%if("display:none" == commonPhraseStlye){%>
                  <ul>
                    <li id="comPhraseViewMore" onclick="commonPhraseViewMoreAndLess('comPhraseViewMore')"><a href="javascript:void(0);" title="View more">+ View more</a></li>
                    <li id="comPhraseViewLess" onclick="commonPhraseViewMoreAndLess('comPhraseViewLess')" style="display:none"><a href="javascript:void(0);" title="View less">- View less</a></li>
                  </ul>
                <%}%>
              </div>        
            </div>
        </c:if>
      </div>
       <div class="wr_ch_btn">
       <spring:message code="review.form.url" var="reviewFormUrl"/>
		  <a class="wrt_btn" id="write_rvw_btn" href="${reviewFormUrl}">
		     <spring:message code="write.a.review.btn.name"/><i class="fa fa-long-arrow-right"></i>
		  </a>
		</div>
    </div>
  </c:if>