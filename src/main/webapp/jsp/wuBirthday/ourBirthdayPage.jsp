<%@ page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" autoFlush="true" %>

<body>
  <section class="pp_cnt anv_cnt1">
    <article class="pp_wrap">
      <div class="anv_wrp1">
        <div class="anv_lt graph wow fadeInUp"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_01.png",0)%>" alt="whatuni" /></div>
        <div class="ac3_1">
          <p class="graph wow zoomIn"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/wu_anni_logo_01.png", 0)%>" alt="Whatuni" /></p>
          <p class="par_cnt graph wow zoomIn">Whatuni is 10 years old! Here's a few facts and stats before we blow out the candles...</p>
        </div>
        <div class="anv_rt graph wow fadeInUp"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_02.png", 0)%>" alt="Ballon" /></div>
      </div>
      <div class="grap2 lgo">
        <p class="cak_ldy wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;"><img class="graph wow fadeInUp" src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_05.png", 0)%>" alt="Cake" /></p>
      </div>
    </article>
  </section>
  <section class="pp_cnt anv_cnt2">
    <article class="pp_wrap">
      <h2 class="graph wow zoomIn">The Students Who Use Whatuni</h2>
      <div class="anv_wrp2">
        <div class="anv_lft graph wow bounceInLeft"><p class="grap2"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_07.png", 0)%>" alt="Desktop" /></p></div>
        <div class="anv_rgt graph wow bounceInRight">
          <h2>359,033</h2>
          <p class="par_cnt ">students currently signed up to receive our newsletters</p>
        </div>
      </div>
      <div class="anv_wrp2 ctrl1">
        <div class="anv_lft frht graph wow bounceInRight"><p class="grap2"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_08.png", 0)%>" alt="Student" /></p></div>
        <div class="anv_rgt graph wow bounceInLeft">
          <h2>6.1 Million</h2>
          <p class="par_cnt">students visit the site each year</p>
        </div>
      </div>
      <div class="anv_wrp2 ctrl2">
        <div class="anv_lft graph wow bounceInLeft"><p class="grap2"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_09.png", 0)%>" alt="Stars" /></p></div>
        <div class="anv_rgt graph wow bounceInRight">
          <h2>92,165</h2>
          <p class="par_cnt">university reviews from current students. These help prospective students decide, and provide feedback to universities so they can improve.</p>
        </div>
      </div>
    </article>
  </section>
  <section class="pp_cnt anv_cnt2 anv_cnt3">
    <article class="pp_wrap">
      <h2 class="graph wow zoomIn">The Whatuni Team</h2>
      <div class="ac3_1">
        <div class="twt_pos">
          <div class="anv_lt graph wow rotateIn"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_10a.png", 0)%>" alt="World" /></div>
          <img class="graph wow zoomIn" src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_10.png", 0)%>" alt="Satillite"/>
          <div class="anv_lb graph wow fadeInLeft"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_10b.png", 0)%>" alt="Plan" /></div>
        </div>
        <h2 class="graph wow fadeInUp">25,000</h2>
        <p class="par_cnt graph wow fadeInUp">miles travelled every single year to collect reviews. That's the distance round the world with 99 miles to spare!</p>
      </div>           
      <div class="anv_wrp2 ctrl1 ctrl2">
        <div class="anv_rgt">
          <h2>51,730</h2>
          <p class="par_cnt ">students inspired to start their university journey after our school presentations</p>
        </div>
        <div class="anv_lft"><p class="grap2 " ><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_11.png", 0)%>" alt="Class room" /></p></div>
      </div>
    </article>
  </section> 
  <section class="pp_cnt anv_cnt4">&nbsp;</section>
  <section class="pp_cnt anv_cnt2 anv_cnt5">
    <article class="pp_wrap">
      <h2 class="graph wow zoomIn">Whatuni By Numbers</h2>
      <div class="wbn_row">         
        <div class="anv_wrp2 col1 graph wow zoomIn">
          <div class="anv_lft"><p class="grap2"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_12.png", 0)%>" alt="Provider" /></p></div>
          <div class="anv_rgt">
            <h2>593</h2>
            <p class="par_cnt ">providers to choose from</p>
          </div>
        </div>
        <div class="anv_wrp2 col3 graph wow zoomIn">
          <div class="anv_lft"><p class="grap2"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_13.png", 0)%>" alt="Student research" /></p></div>
          <div class="anv_rgt">
            <h2>67,033</h2>
            <p class="par_cnt ">courses listed for students to research</p>
          </div>
        </div>
        <%--
          <div class="anv_wrp2 col3 graph wow zoomIn" >
          <div class="anv_lft"><p class="grap2 "><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_14.png", 0)%>" alt="registration" /></p></div>
          <div class="anv_rgt">
          <h2>267,107</h2>
          <p class="par_cnt ">registrations over the past 5 years</p>
          </div>
          </div>
        --%>                          
      </div>
      <div class="anv_wrp2 ctrl1">
        <div class="anv_lft frht anv_lft5 graph wow bounceInRight"><p class="grap2 "><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_15.png?", 0)%>" alt="Course search" /></p></div>
        <div class="anv_rgt graph wow bounceInLeft">
          <h2>1,754,513</h2>
          <p class="par_cnt ">web clicks from profiles over the last 3 years by students searching for specific courses and universities</p>
        </div>
      </div>
    </article>
  </section>
  <section class="pp_cnt anv_cnt2 anv_cnt3 anv_cnt6">
    <article class="pp_wrap">
      <div class="ac3_1">
        <p class="graph wow fadeInDown"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_16.png", 0)%>" alt="Enquiry"/></p>
        <h2 class="graph wow fadeInUp">180,000</h2>
        <p class="par_cnt graph wow fadeInUp">prospectuses delivered right to students' doorsteps</p>
      </div>           
      <div class="ac3_1">
        <p class="graph wow fadeInUp"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_17.png", 0)%>" alt="Openday" /></p>
        <h2 class="graph wow fadeInUp">19,477</h2>
        <p class="par_cnt graph wow fadeInUp">open days saved to students' profiles over their last four years</p>
      </div>
    </article>
  </section>
  <section class="pp_cnt anv_cnt2 anv_cnt5 anv_cnt7">
    <article class="pp_wrap mob">
      <div class="pw_cnt7a graph wow fadeInUp"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_19a.png", 0)%>" alt="rockect" /></div>
    </article>
    <article class="pp_wrap desk">
      <div class="pw_cnt7a graph wow fadeInUp"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_19.png", 0)%>" alt="Rockect" /></div>
      <div class="pw_cnt7"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_18.png", 0)%>" alt="cloud"/></div>
    </article>
  </section>
  <section class="pp_cnt anv_cnt2 anv_cnt5 anv_cnt8"> 
    <article class="pp_wrap">
      <div class="wbn_row">         
        <div class="anv_wrp2 col1 graph wow zoomIn">
          <div class="anv_lft"><p class="grap2 "><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_20.png", 0)%>" alt="2014 awards" /></p></div>
        </div>
        <div class="anv_wrp2 col2 graph wow zoomIn">
          <div class="anv_lft"><p class="grap2"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_21.png", 0)%>" alt="2015 awards" /></p></div>
        </div>
        <div class="anv_wrp2 col3 graph wow zoomIn">
          <div class="anv_lft"><p class="grap2"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_22.png", 0)%>" alt="2016 awards" /></p></div>
        </div>                          
      </div>
      <h2 class="graph wow zoomIn">3 amazing #WUSCAs and counting</h2>
    </article>
  </section>
  <article class="anv_cnt8a">
    <article class="pp_wrap"><p class="graph"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_22a.png", 0)%>" alt="Snow"/></p></article>
  </article>
  <section class="pp_cnt anv_cnt3 anv_cnt6 anv_cnt9">
    <article class="pp_wrap">
      <div class="ac3_1">
        <p class="par_cnt graph wow fadeInUp">Thanks for being a part of the Whatuni journey!</br> Here's to the next 10 years!</p>
        <p class="graph wow bounceInRight"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_27.svg", 0)%>" width="515" alt="your awesome"/></p>
      </div>           
      <div class="ac3_1">
        <div class="sgn_fbtn graph wow fadeInUp">
          <nav class="pp_nav"><ul><li><a href="<%=GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName()%>">VISIT WEBSITE</a></li></ul></nav>
        </div>
        <p class="graph wow zoomIn"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_28.png", 0)%>" alt="Whatuni logo" /></p>
      </div>
    </article>
    <article class="anv_cnt10">
      <article class="pp_wrap"><p class="graph"><img src="<%=CommonUtil.getImgPath("/wu-cont/10th_birthday/images/anni_img_29.png", 0)%>" alt="Hills" /></p></article>
    </article>
  </section>
  <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
  <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/wow.min.js"></script>
  <script type="text/javascript">
    var $j = jQuery.noConflict();
    $j(document).ready(function(){
      if ( window.innerWidth >= 320){
    	  new WOW().init(); 
      }
    });
  </script> 
</body>