<body>
  <header class="clipart">
    <div class="ad_cnr">
      <div class="content-bg">
        <div id="desktop_hdr">
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>
      </div>
    </div>
  </header>
  <section class="main_cnr abts pt40 pb40">
    <div class="ad_cnr">
      <div id="sub_container" class="content-bg">
        <div class="sub_cnr abt">
          <%-- Left Menu Starts --%>
          <jsp:include page="/help/aboutus/aboutUsLeftMenu.jsp">
            <jsp:param name="pagename" value="partners" />
          </jsp:include>
          <%-- Left Menu Ends --%>
          <%-- Right Menu Starts --%>
          <jsp:include page="/jsp/common/includeWebApp.jsp" />
          <%-- Right Menu Ends --%>
        </div>
      </div>
    </div>
  </section>
  <jsp:include page="/jsp/common/wuFooter.jsp" />
</body>