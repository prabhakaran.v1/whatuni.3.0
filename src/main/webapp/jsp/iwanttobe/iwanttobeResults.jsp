<%--
  * @purpose:  jsp for showing search results in want can i do..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 16-Feb-2016    Indumathi Selvam          1.0      First draft           wu_549
  * *************************************************************************************************************************
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.wuni.util.seo.SeoUrls, WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants,org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction" %>

<% String joborindustryname = request.getAttribute("jobOrIndustryName") != null ? (String)request.getAttribute("jobOrIndustryName") : "";
String joborindustryText = request.getAttribute("joborindustryText") != null ? (String)request.getAttribute("joborindustryText") : "";
String ultimateSearchId = request.getAttribute("ultimateSearchId") != null ? (String)request.getAttribute("ultimateSearchId") : "";
String returCode = request.getAttribute("returnCode") != null ? (String)request.getAttribute("returnCode") : "0";
String remainingPercent = request.getAttribute("remainingPercent") != null ? (String)request.getAttribute("remainingPercent") : "";
String gaAccount = new WUI.utilities.CommonFunction().getWUSysVarValue("WU_GA_ACCOUNT");//Added by Indumathi.S for GA logging
%>
<div class="wrapp">
  <i class="fa fa-times" onclick="closeWidgetLightBox();"></i>
  <div class="container1">
    <div id="typeform" class="pag2">
      <div class="hdr fl">
        <h1>Your results for <%=joborindustryname%></h1>
        <c:if test="${not empty requestScope.searchResultsList}">

            <div class="h2hdr w100p fl pdt15">
              <h2 class="fl">Most people who want to work <%=joborindustryText%> head to university and study one of the courses below</h2>
              <a onclick="goToPreviousPage();" class="btn_com ico fr">Search again</a>
            </div>

        </c:if>
      </div>
      <c:if test="${not empty requestScope.searchResultsList}">
      <c:forEach var="searchResultsList" items="${requestScope.searchResultsList}" varStatus="rowIndex">
  <c:set var="intValue" value="${rowIndex.index}"/>
              <div class="wbg w100p fl pd30" id="results<%=pageContext.getAttribute("intValue").toString()%>">
                <h3 class="w100p">
                <c:if test="${not empty searchResultsList.jacsSubject}">
                    ${searchResultsList.jacsSubject}
  
                  </c:if>
                  <span class="scr_tltip fr" id="toolTipDiv" onclick="showToolTip('toolTip<%=pageContext.getAttribute("intValue").toString()%>')" onmouseover="showToolTip('toolTip<%=pageContext.getAttribute("intValue").toString()%>')" onmouseout="hideToolTip('toolTip<%=pageContext.getAttribute("intValue").toString()%>')"><i class="fa fa-question-circle fr" id="toolTipDiv1"></i>
                    <span class="scr_tip" id="toolTip<%=pageContext.getAttribute("intValue").toString()%>" style="display:none"><strong>
                      <c:if test="${not empty searchResultsList.jacsPercentage}">
                        ${searchResultsList.jacsPercentage}
                      </c:if>
                      </strong> of 
                      <c:if test="${not empty searchResultsList.jacsJobOrIndTooltipText}">
                      ${searchResultsList.jacsJobOrIndTooltipText}
                       </c:if>studied <c:if test="${not empty searchResultsList.jacsSubject}">
                        ${searchResultsList.jacsSubject}
                      </c:if> at university. <br></br><%=GlobalConstants.HESA_SOURCE%></span>
                    </span> 
                    <span class="fr ${searchResultsList.color}"><c:if test="${not empty searchResultsList. jacsPercentage}">${searchResultsList.jacsPercentage}</c:if></span>
                  </h3>
                 
                  <div class="pbar fl">
                    <div class="p${searchResultsList.color} fl" style="width:${searchResultsList.jacsPercentage}"></div>
                  </div>
                 
                  <a onclick="getSubjectStats('SUBJECT-STATS', '${searchResultsList.jacsCode}', '${searchResultsList.jacsSubject}', '<%=ultimateSearchId%>');" class="btn_com fl mt20">More info</a>
                 <c:if test="${not empty searchResultsList.jacsPercentage}">
<c:set var="jacsCode" value="${searchResultsList.jacsCode}"/>
                   
                    <!--Added ulGAEventTracking for view degrees by Indumathi.S 19_Apr_16 Rel-->
                    <a onclick="ulGAEventTracking('VD', '${searchResultsList.jacsSubject}', '<%=gaAccount%>'); openNewWindow('<%=new SeoUrls().constructIWantToBeSearchUrl((String)(pageContext.getAttribute("jacsCode")))%>');" class="btn_com fr mt20 bl_bg cwhite">View degrees</a>
                  </c:if>
                </div>
              </c:forEach>
              <div class="iws-dtl nw">
                <span class="iws-fst">
                <%if("3".equalsIgnoreCase(returCode)){%>
                  The remaining <%=remainingPercent%>% of students studied a variety of subjects, none of which produced enough <%=joborindustryname%> to be statistically significant.
                <%}else if("2".equalsIgnoreCase(returCode)){%>
                  The remaining <%=remainingPercent%>% of students studied a variety of subjects.
                <%}%>
                </span>
              </div>  
              <input type="hidden" id="ultimateSearchId" name="ultimateSearchId" value="<%=ultimateSearchId%>"/>
          </c:if>
          <c:if test="${empty requestScope.searchResultsList}">
          <%
          String jobOrIndustryFlag = (String)request.getAttribute("jobOrIndustryFlag");
          String jobOrIndustry = "";
          if("J".equalsIgnoreCase(jobOrIndustryFlag)){ jobOrIndustry = "job"; } 
          if("I".equalsIgnoreCase(jobOrIndustryFlag)){ jobOrIndustry = "industry"; }
          %>
          <div class="hdr fl mb m0">
            <span class="jname">Oh dear!</span>
            <p>We're sorry to say it, but not enough students are working in that <%=jobOrIndustry%> 6 months after graduating for us to provide any meaningful results! Either you have picked something a bit obscure or you are setting your sights very high. Try again with a different <%=jobOrIndustry%> and see if you have better luck.</p>
          </div>
          <div class="h2hdr w100p fl pdt15">
            <a onclick="goToPreviousPage();" class="btn_com ico fr">Search again</a>
          </div>
        </c:if>
      </div>
   </div>
</div>

