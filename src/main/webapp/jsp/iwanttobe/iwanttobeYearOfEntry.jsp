<%--
  * @purpose:  jsp to get the year of entry for what can i do..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 16-Feb-2016    Indumathi Selvam          1.0      First draft           wu_549
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%String schoolType = (String)request.getAttribute("schoolType");
  String checkSchoolType = (String)request.getAttribute("checkSchoolType");
%>
<c:if test="${not empty requestScope.yearOfEntryList}">
    <div class="hdr fl m0">
      <div class="lft fl"><p>3 <i class="fa fa-long-arrow-right"></i></p></div>
      <div class="mdc m0 fr">
        <label class="w100p fl" >What year are you in?</label>
        <div class="w100p fl radiv">
        <c:forEach var="yearOfEntryList" items="${requestScope.yearOfEntryList}" varStatus="rowIndex">  
     <c:set var="rowIntIndex" value="${rowIndex.index}" />
            <%String yearClass = "fl";
            if((int)pageContext.getAttribute("rowIntIndex")%2 != 0){ yearClass = "fr";}%>
            <span class="<%=yearClass%>">
              <input onblur="" type="radio" name="radio11" value="${yearOfEntryList.yearOfEntry}"/>
              <label class="fl" id="lbl_${yearOfEntryList.yearOfEntry}">
              <c:if test="${not empty yearOfEntryList.inWhatYear}">
                ${yearOfEntryList.inWhatYear}
              </c:if>
              </label>
            </span>
          </c:forEach>
          <div class="fl pdt10 w100p red" id="errordiv2"></div>
        </div>
      </div>
    </div>
    <input type="hidden" id="schoolType" value="<%=schoolType%>"/>
    <input type="hidden" id="checkSchoolType" value="<%=checkSchoolType%>"/>
    <input type="hidden" id="yearOfEntryVal" value=""/>
</c:if>

