<%--
  * @purpose:  'want can i do' widget search page..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 16-Feb-2016    Indumathi Selvam          1.0      First draft           wu_549
  * *************************************************************************************************************************
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import=" WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator" %>
<%  String ajaxDynamicListJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.ajaxDynamicList.js");
  String iwanttobeJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.iwanttobe.widget.page.js");
  String widgetDoughnutJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.widget.doughnut.chart.js");
  String widgetDrawDoughnutJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.widget.draw.doughnut.js");
  String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
%>
<body>
<div id="iwanttobeWidget">
  <div class="wrapp" id="iwanttobesearch">
    <i class="fa fa-times" onclick="closeWidgetLightBox();"></i>
    <div class="container1"> 
      <div id="typeform" class="wd_frm">
        <form>
          <div class="hdr fl mb0 b_div">
            <div class="lft fl"></div>
            <div class="mdc m0 fr">
              <img src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/iw-bubble.svg", 0)%>" class="fl bub" alt="">
            </div>
          </div>
          <div class="hdr fl m0">
            <div class="lft fl"><p>1 <i class="fa fa-long-arrow-right"></i></p></div>
            <div class="mdc m0 fr">
              <label class="w100p fl" >What do you want to do when you get older?</label>
               
              <input type="text" id="jobOrIndustry" value="" name="jobOrIndustry" autocomplete="off" placeholder="Enter job or industry" onkeyup="chooseJobOrIndustry(event, this);" onkeypress="return iwanttobeKeypressSubmit(this, event);" maxlength="4000"/>
              <input type="hidden" id="jobOrIndustry_id" name="jobOrIndustry_id" value=""/>
              <input type="hidden" id="jobOrIndustry_flag" name="jobOrIndustry_flag" value=""/>
              <input type="hidden" id="jobOrIndustry_display" name="jobOrIndustry_display" value=""/>
              <div class="pdt10 red" id="errordiv"></div>
            </div>
          </div>
          <div class="hdr fl m0">
            <div class="lft fl"><p>2 <i class="fa fa-long-arrow-right"></i></p></div>
            <div class="mdc m0 fr">
              <label class="w100p fl" >Which situation best describes you?</label>
              <div class="w100p fl radiv">
                <span class="fl" onclick="setCheckedVal('S');">
                  <input type="radio" name="radio1" value="S" id="school"/>
                  <label class="fl scol">At secondary school<span></span></label>
                </span>
                <span class="fr" onclick="setCheckedVal('C');">
                  <input type="radio" name="radio1" value="C" id="college"/>
                  <label class="fl scol scol1">At sixth form / college <span></span></label>
                </span>
              </div>
              <div class="fl pdt10 w100p red" id="errordiv1"></div>
            </div>
          </div>
          <div id="schoolOrCollegeYear">
          </div>
          <div class="hdr fl m0">
            <div class="lft fl"></div>
            <div class="mdc m0 fr">
              <a href="javascript:void(0);" onclick="submitSearchResults();" class="btn_com bl_bg fl cwhite stp1">SEE YOUR RESULTS <i class="fa fa-long-arrow-right pdl20"></i></a>
            </div>        
          </div>
        </form>
      </div>
    </div>     
  </div>
  
  <div id="iwanttoberesults"></div>
  <div id="subjectStatsDiv" style="display:none;"></div>
  <div id="subjectResultDiv" style="display:none;"></div>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/DonutChart/<%=widgetDoughnutJSName%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/DonutChart/<%=widgetDrawDoughnutJSName%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script>
  <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
  <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />
  <div id="loadingImg" style="display:none;">
   <div style="top: 0;position: fixed;left: 0;background: rgba(0,0,0,0.5);width: 100%;height: 100%;">
    <img style="position: absolute; top: 50%; left: 50%; margin-top: -32px; margin-left: -32px;" src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/loader.gif",0)%>" alt="loading image"/>
    </div>
  </div>
</div>
</body>