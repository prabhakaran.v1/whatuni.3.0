<%-- 
  * @purpose:  jsp for showing subject results in i want to be widget..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 16-Feb-2016    Prabhakaran V.            1.0      First draft           wu_549
  * *************************************************************************************************************************
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.wuni.util.seo.SeoUrls, WUI.utilities.CommonUtil,  WUI.utilities.GlobalConstants"%>
<%String jacsCode = request.getAttribute("jacsCode") != null ? (String)request.getAttribute("jacsCode") : "";
  String sortByHow = request.getAttribute("sortByHow") != null ? (String)request.getAttribute("sortByHow") : "";
  String salSortByHow = request.getAttribute("salSortByHow") != null ? (String)request.getAttribute("salSortByHow") : "";
  String empSortByHow = request.getAttribute("empSortByHow") != null ? (String)request.getAttribute("empSortByHow") : "";
  String ultimateSearchId = request.getAttribute("ultimateSearchId") != null ? (String)request.getAttribute("ultimateSearchId") : "";
  String totalCount = request.getAttribute("totalCount") != null ? (String)request.getAttribute("totalCount") : "";
  String sortByWhich = request.getAttribute("sortByWhich") != null ? (String)request.getAttribute("sortByWhich") : "GRADUATE_EMPLOYED_PERCENT";
  String tempImgPath = CommonUtil.getImgPath("/", 0);
  String pageNo = request.getAttribute("pageNo") != null ? (String)request.getAttribute("pageNo") : "1";
  String jacsSubject = request.getAttribute("jacsSubject") != null ? (String)request.getAttribute("jacsSubject") : "";
  String salaryTooltip = request.getAttribute("salaryTooltip") != null ? (String)request.getAttribute("salaryTooltip") : "";
  String empTooltip = request.getAttribute("empTooltip") != null ? (String)request.getAttribute("empTooltip") : "";
  String url = GlobalConstants.WHATUNI_SCHEME_NAME+request.getServerName();
  String salClass = ""; //Adding class for selected sort by which is salary
  String empClass = ""; //Adding class for selected sort by which is emp rate
  String salAscending = ""; //Adding class for asending & descending arrow for salary
  String empAscending = ""; //Adding class for asending & descending arrow for emp rate
  if("SALARY".equalsIgnoreCase(sortByWhich)){
   salClass = "active";
   if("A".equalsIgnoreCase(sortByHow)){
     salAscending = "up";
   }else{
     salAscending = "down";
   }
  }
  if("GRADUATE_EMPLOYED_PERCENT".equalsIgnoreCase(sortByWhich)){
    empClass = "active";
    if("A".equalsIgnoreCase(sortByHow)){
     empAscending = "up";
   }else{
     empAscending = "down";
   }
  }
  String imgManPath =  tempImgPath + "wu-cont/images/widget/";
  String gaAccount = new WUI.utilities.CommonFunction().getWUSysVarValue("WU_GA_ACCOUNT");//Added by Indumathi.S for GA logging
%>
<c:if test="${not empty requestScope.subjectResultsList }">
  <div class="wrapp">
    <i class="fa fa-times" onclick="closeWidgetLightBox();"></i>
    <div class="container1">
      <div id="typeform" class="pag4">
        <div class="hdr hnew"><h1>Where should you study?</h1>
          <div class="iwtb_back">
            <a onclick="goToPreviousPage();" class="btn_com ico fr">Search again</a>
            <a onclick="goTobackUniStats();" class="btn_com ico fr wcs_sts_back">Back</a>
          </div>
        </div>
        <div class="gbg w100p fl">
          <p class="fnt16">Sort by</p>
          <a id="salaryTab" onclick="subjectResultNav('1','<%=jacsCode%>', 'SALARY', <%=ultimateSearchId%>, '<%=salSortByHow%>', '', '<%=sortByHow%>');" class="btn_com fr <%=salClass%>">Salary<i class="fa fa-angle-<%=salAscending%> fr pdi"></i></a>
          <a id="empRateTab" onclick="subjectResultNav('1', '<%=jacsCode%>', 'GRADUATE_EMPLOYED_PERCENT', <%=ultimateSearchId%>, '', '<%=empSortByHow%>', '<%=sortByHow%>');" class="btn_com fr <%=empClass%>">Employment rate<i class="fa fa-angle-<%=empAscending%> fr pdi"></i></a>
        </div>
        <div id="subjectResultPod">
         <c:forEach var="subjectResultsList" items="${requestScope.subjectResultsList}" varStatus="rowIndex"> 
<c:set var="indexIntValue" value="${rowIndex.index}"/>
<c:if test="${not empty subjectResultsList.collegeNameDisplay}">
              <div class="wbg w100p fl pd30">
                <a class="bxz" onclick="openNewWindow('<%=url%>${subjectResultsList.providerViewDegreeURL}')"> 
                <c:if test="${not empty subjectResultsList.imagePath}">
                    <c:set var="imgLogo" value="${subjectResultsList.imagePath}"/>
                    <%String logo =  tempImgPath + pageContext.getAttribute("imgLogo").toString();%>
                    <%if(Integer.parseInt(pageContext.getAttribute("indexIntValue").toString()) <= 3){ %>
                    <img src="<%=logo%>" alt="${subjectResultsList.collegeNameDisplay}"/>
                    <%}else{%>
                    <img class="lazy-load" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src="<%=logo%>" alt="${subjectResultsList.collegeNameDisplay}"/>
                 <%}%>
                  </c:if> 
                </a>
                <div class="w80p fr">
                  <h3 class="w100p">${subjectResultsList.collegeNameDisplay}</h3>
                  <c:if test="${not empty subjectResultsList.stickManFig}">
                  <c:if test="${not empty subjectResultsList.stickManColor}">
              <c:set var="empRateDef" value="${subjectResultsList.stickManFig}"/>

                      <%if("INSUFFICIENT DATA".equalsIgnoreCase(pageContext.getAttribute("empRateDef").toString()) || "DATA NOT AVAILABLE".equalsIgnoreCase(pageContext.getAttribute("empRateDef").toString())){%>
                        <h4 class="w100p pdt30 fnt18 pr">Employment rate 
                          <span class="fr">${subjectResultsList.stickManFig} </span>
                          
                        </h4>
                      <%}else{%>
                        <h4 class="w100p pdt20 fnt18">Employment rate 
                          <span class="cr">
                            <span class="${subjectResultsList.stickManColor}">${subjectResultsList.stickManFig} out of 10</span> 
                              <span class="scr_tltip" onclick="showToolTip('toolTipSub4<%=Integer.valueOf(pageContext.getAttribute("indexIntValue").toString())%>')" onmouseover="showToolTip('toolTipSub4<%=Integer.valueOf(pageContext.getAttribute("indexIntValue").toString())%>')" onmouseout="hideToolTip('toolTipSub4<%=Integer.valueOf(pageContext.getAttribute("indexIntValue").toString())%>')">
                                <i class="fa fa-question-circle fr fnt18" id="toolTipDiv5"></i>
                                
                
                                <span class="scr_tip" id="toolTipSub4<%=pageContext.getAttribute("indexIntValue").toString()%>">
                                Approximately ${subjectResultsList.stickManFig} out of 10 people who studied <%=jacsSubject%> at ${subjectResultsList.collegeNameDisplay} <%=empTooltip%>. <br></br><%=GlobalConstants.HESA_SOURCE%>
                                </span>                     
                              </span>
                            </span>
                          </h4>
                          <c:set var="stickManDef" value="${subjectResultsList.stickManFig}"/>
                          <c:set var="stickManColorDef" value="${subjectResultsList.stickManColor}"/>
                         <%String imgPath = imgManPath + pageContext.getAttribute("stickManColorDef").toString() + "_ico.png";
                         float figSize = Float.valueOf((pageContext.getAttribute("stickManDef").toString()));
                         float remain = 10.0f - figSize;
                         %>
                         <%if(Integer.parseInt(pageContext.getAttribute("indexIntValue").toString()) <= 3){%>
                         <ul class="rat_ico">
                         <%for(int i = 1; i<=figSize ; i++) {%>
                           <li><img src="<%=imgPath%>" alt="full stickMan"/></li>
                         <%}
                         if(pageContext.getAttribute("stickManDef").toString().indexOf(".5") >= 0){
                           imgPath = imgManPath +  pageContext.getAttribute("stickManColorDef").toString() + "_gry.png";%>
                           <li><img src="<%=imgPath%>" alt="half stickMan"></li>
                         <%}
                         
                         for(int lp = 1; lp < remain; lp++){%>
                           <li><img src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/gry_icon.png", 0)%>" alt="half stickMan"></li>
                         <%}%>
                         </ul>
                         <%}else{%>
                         <ul class="rat_ico">
                         <%for(int i = 1; i<=figSize ; i++) {%>
                           <li><img class="lazy-load" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src="<%=imgPath%>" alt="full stickMan"/></li>
                         <%}
                         if(pageContext.getAttribute("stickManDef").toString().indexOf(".5") >= 0){
                           imgPath = imgManPath +  pageContext.getAttribute("stickManColorDef").toString() + "_gry.png";%>
                           <li><img class="lazy-load" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src="<%=imgPath%>" alt="half stickMan"></li>
                         <%}
                         for(int lp = 1; lp < remain; lp++){%>
                           <li><img class="lazy-load" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/gry_icon.png", 0)%>" alt="half stickMan"></li>
                         <%}%>
                         </ul>
                         <%}%>
                         <h5 class="w100p fl fnt16">
                         <%=jacsSubject%> graduates who <%=empTooltip%></h5>
                       <%}%>
                     </c:if>
                   </c:if>
                   <c:if test="${not empty subjectResultsList.salaryRange}">
                  <c:set var="salaryRangeDef" value="${subjectResultsList.salaryRange}"/>
                     <%if("INSUFFICIENT DATA".equalsIgnoreCase(pageContext.getAttribute("salaryRangeDef").toString()) || "DATA NOT AVAILABLE".equalsIgnoreCase(pageContext.getAttribute("salaryRangeDef").toString())){%>
                       <h4 class="w100p pdt30 fnt18 pr fl">Average salary 
                         <span class="fr">${subjectResultsList.salaryRange}</span>
                       </h4>
                     <%}else{%>
                     <c:set var="salaryBarChart" value="${subjectResultsList.salaryRangeColor}"/>
                     <c:set var="salaryBarRanges" value="${subjectResultsList.salaryDisplayColor}"/>
                       <%String barDifff = pageContext.getAttribute("salaryBarRanges").toString();
                       String barDiff[] = barDifff.split("-");
                       float barWidth = 0.0f;
                       String barLeft = "0";
                       if(barDiff.length == 2){
                         barLeft = barDiff[0];
                         barWidth = Float.parseFloat(barDiff[1]) - Float.parseFloat(barLeft);
                         if(barWidth == 0){
                           barWidth = 1.0f;
                           barLeft = "1";
                         }
                       }%>
                       <h4 class="w100p fl pdt20 fnt18">Average salary 
                       <span class="cr">
                         <span class="${subjectResultsList.salaryRangeColor}">${subjectResultsList.salaryRange}</span>
                         <span class="scr_tltip" onclick="showToolTip('toolTipSub3<%=pageContext.getAttribute("indexIntValue").toString()%>')" onmouseover="showToolTip('toolTipSub3<%=pageContext.getAttribute("indexIntValue").toString()%>')" onmouseout="hideToolTip('toolTipSub3<%=pageContext.getAttribute("indexIntValue").toString()%>')">
                           <i class="fa fa-question-circle fr fnt18" id="toolTipDiv2"></i>
                           <span class="scr_tip" id="toolTipSub3<%=pageContext.getAttribute("indexIntValue").toString()%>">
                           Salaries 6 months after graduation for <%=salaryTooltip%> who studied <%=jacsSubject%> at ${subjectResultsList.collegeNameDisplay}. <br></br><%=GlobalConstants.HESA_SOURCE%>
                           </span> 
                        </span>
                      </span>
                    </h4>
                    <div class="pbar fl"><div class="p${subjectResultsList.salaryRangeColor} fl" style="width:<%=barWidth%>%; margin-left:<%=barLeft%>%;"></div></div>
                    <p class="lmh w100p fl">
                      <span class="fl">Low</span>
                      <span>Medium</span>
                      <span class="fr">High</span>
                    </p>
                <%}%>
              </c:if>
              <c:if test="${not empty subjectResultsList.providerViewDegreeFlag}">
              <c:if test="${subjectResultsList.providerViewDegreeFlag eq 'Y' }">
<c:set var="uniUrl" value="${subjectResultsList.subjectGuideURL}"/>
                  <!--Added ulGAEventTracking for view degrees by Indumathi.S 19_Apr_16 Rel-->
                  <a onclick="ulGAEventTracking('VD', '<%=jacsSubject%>', '<%=gaAccount%>'); openNewWindow('<%=new SeoUrls().constructIWantToBeUniSearchUrl(jacsCode, pageContext.getAttribute("uniUrl").toString())%>')" class="btn_com fr mt20 bl_bg cwhite">View degrees</a>

                </c:if>
              </c:if>
            </div>
          </div>
        </c:if>
      </c:forEach>    
    </div>
    <div class="h2hdr w100p fl pdt10">
      <jsp:include page="/jsp/iwanttobe/include/iwanttobePagination.jsp">
        <jsp:param name="pagelimit" value="10"/>
        <jsp:param name="pageno" value="<%=pageNo%>"/>
        <jsp:param name="sortByWhich" value="<%=sortByWhich%>"/>
        <jsp:param name="jacsCode" value="<%=jacsCode%>"/>
        <jsp:param name="searchhits" value="<%=totalCount%>"/>
        <jsp:param name="salSortByHow" value="<%=salSortByHow%>"/>
        <jsp:param name="empSortByHow" value="<%=empSortByHow%>"/>
        <jsp:param name="ultimateSearchId" value="<%=ultimateSearchId%>"/>
        <jsp:param name="sortByHow" value="<%=sortByHow%>"/>
        <jsp:param name="recorddisplay" value="20"/>
      </jsp:include>
    </div>
  </div>
</div>
</div>
</c:if>
<div class="ftr_pod w100p fl">
<c:if test="${not empty requestScope.subjectStatsList}">
<c:forEach var="subjectStatsList" items="${requestScope.subjectStatsList}" varStatus="rowIndex">
<c:set var="indexIntValue" value="${rowIndex.index}"/>
  <%String empDataNotAvail = "";
    String salDataNotAvail = "";%>
    <c:if test="${empty subjectStatsList.stickManFig}">
    <%empDataNotAvail = "TRUE";%>
  </c:if>
  <c:if test="${empty subjectStatsList.salaryRange}">
    <%salDataNotAvail = "TRUE";%>
  </c:if>
  <%if(!("TRUE".equalsIgnoreCase(empDataNotAvail) && "TRUE".equalsIgnoreCase(salDataNotAvail))){%>
  <div class="ftr">
    <div class="btn_ftr"><a class="cwhite btn_com fl" id="btn_uk" onclick="showUKStats();">UK average stats</a></div>  
    
    <div class="wbg w100p fl pd30" id="tgldiv">
      <img src='<%=CommonUtil.getImgPath("/wu-cont/images/widget/briefcase.svg", 1)%>' width="60px" height="60px" alt="briefcase"/>
      <div class="w80p fr">
        <h3 class="w100p cwhite"><%=jacsSubject%> stats (UK average)</h3>
        <c:if test="${not empty subjectStatsList.stickManFig}">
        <c:set var="subStickManDef" value="${subjectStatsList.stickManFig}"/>
          <%String empRateVal = pageContext.getAttribute("subStickManDef").toString();
          if("INSUFFICIENT DATA".equalsIgnoreCase(empRateVal) || "DATA NOT AVAILABLE".equalsIgnoreCase(empRateVal)){%>
          <h4 class="w100p pdt30 fnt18 cwhite fl">Employment rate 
            <span class="cr">${subjectStatsList.stickManFig}</span>
          </h4>
         <%}else{%>
         <c:if test="${not empty subjectStatsList. stickManColor}">
           <h4 class="w100p pdt30 fnt18 cwhite fl">Employment rate 
             <span class="cr">
               <span class="${subjectStatsList.stickManColor} pdt30">${subjectStatsList.stickManFig} out of 10</span> &nbsp;
               <span class="scr_tltip " onclick="showToolTip('toolTipSub2<%=pageContext.getAttribute("indexIntValue").toString()%>')" onmouseover="showToolTip('toolTipSub2<%=pageContext.getAttribute("indexIntValue").toString()%>')" onmouseout="hideToolTip('toolTipSub2<%=pageContext.getAttribute("indexIntValue").toString()%>')">
                 <i class="fa fa-question-circle fr fnt18" id="toolTipDiv3"></i> 
                 <%String empAvgRateTooltip = "";%>
                 <c:if test="${not empty subjectStatsList.empTooltipTxt}">
                  <c:set var="empRateTooltipTxt" value="${subjectStatsList.empTooltipTxt}"/>
                 <% empAvgRateTooltip = pageContext.getAttribute("empRateTooltipTxt").toString();%>
                  </c:if>
                 <span class="scr_tip" id="toolTipSub2<%=pageContext.getAttribute("indexIntValue").toString()%>">
                 Approximately ${subjectStatsList.stickManFig} out of 10 people who studied <%=jacsSubject%> at University ${subjectStatsList.empTooltipTxt}. <br></br><%=GlobalConstants.HESA_SOURCE%>
                 </span>
               </span>
             </span>
           </h4>
           <c:set var="stickManColor" value="${subjectStatsList.stickManColor}"/>
           <%String imgPath = imgManPath + pageContext.getAttribute("stickManColor").toString() + "_ico.png";
             float figSize = Float.parseFloat(empRateVal);%>
              <ul class="rat_ico">
                <%for(int i = 1; i<figSize ; i++) {%>
                  <li><img src="<%=imgPath%>" alt="full stickMan"/></li>
                <%}
                if(empRateVal.indexOf(".5") >= 0){
                  imgPath = imgManPath + pageContext.getAttribute("stickManColor").toString() + "_gry.png";%>
                  <li><img src="<%=imgPath%>" alt="half stickMan"></li>
                <%}
                float remain = 10.0f - figSize;
                for(int lp = 1; lp < remain; lp++){%>
                  <li><img src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/gry_icon.png", 0)%>" alt="half stickMan"></li>
                <%}%>
              </ul>
              <h5 class="w100p fnt16 cwhite fl">
              <%=jacsSubject%> graduates who ${subjectStatsList.empTooltipTxt} .</h5>
           </c:if>
            <%}%>
          </c:if>
          <c:if test="${not empty subjectStatsList.salaryRange}">
   <c:set var="subSalaryRangeDef" value="${subjectStatsList.salaryRange}"/>

          <%String salaryRangeVal = pageContext.getAttribute("subSalaryRangeDef").toString();
          if("INSUFFICIENT DATA".equalsIgnoreCase(salaryRangeVal) || "DATA NOT AVAILABLE".equalsIgnoreCase(salaryRangeVal)){%>
          <h4 class="w100p pdt30 fnt18 cwhite fl">Average salary 
              <span class="cr">
                ${subjectStatsList.salaryRange}
              </span>
              </h4>
              <%}else{%>
            <h4 class="w100p pdt30 fnt18 cwhite fl">Average salary <span class="${subjectStatsList.salaryRangeColor} fr">${subjectStatsList.salaryRange}
             <span class="scr_tltip " onclick="showToolTip('toolTipSub1<%=pageContext.getAttribute("intIndexValue")%>')" onmouseover="showToolTip('toolTipSub1<%=pageContext.getAttribute("intIndexValue")%>')" onmouseout="hideToolTip('toolTipSub1<%=pageContext.getAttribute("intIndexValue")%>')">
             <i class="fa fa-question-circle fr fnt18" id="toolTipDiv4"></i>
             <span class="scr_tip" id="toolTipSub1<%=pageContext.getAttribute("intIndexValue")%>">
             Salaries 6 months after graduation for ${subjectStatsList.salaryTooltipTxt} who studied <%=jacsSubject%> at University. <br></br><%=GlobalConstants.HESA_SOURCE%>
             </span>
            </span>
            </span></h4>
          <c:set var="avgSalaryBarRanges" value="${subjectStatsList.salaryDisplayColor}"/>
            <%String barDifff= pageContext.getAttribute("avgSalaryBarRanges").toString();
            String barDiff[] = barDifff.split("-");
              String barLeft = "0";
              float barWidth = 0.0f;
              if(barDiff.length == 2){
                barLeft = barDiff[0];
                barWidth = Float.parseFloat(barDiff[1]) - Float.parseFloat(barLeft);
                if(barWidth == 0){
                barWidth = 1.0f;
                barLeft = "1";
                }
              }%>
            <div class="pbar fl"><div class="p${subjectStatsList.salaryRangeColor} fl" style="width:<%=barWidth%>%; margin-left:<%=barLeft%>%;"></div></div>
            <p class="lmh w100p fl">
              <span class="fl">Low</span>
              <span>Medium</span>
              <span class="fr">High</span>
            </p>
            <h5 class="w100p fnt16 fl cwhite">Average UK Salary for ${subjectStatsList.salaryTooltipTxt} after 6 months.</h5>
            <%}%>
  
          </c:if>
        </div>
      </div>
   
  </div>
  <%}%>
 </c:forEach>
    </c:if>
 </div>  