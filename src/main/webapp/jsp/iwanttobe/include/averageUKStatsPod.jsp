<%@ taglib uri="/WEB-INF/tlds/html.tld" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="com.wuni.util.seo.SeoUrls, WUI.utilities.CommonUtil"%>
<%String jacsSubject = (String)request.getAttribute("jacsSubject");
  String jacsCode = (String)request.getAttribute("jacsCode");
  String ultimateSearchId = (String)request.getAttribute("ultimateSearchId");
  String tempImgPath = CommonUtil.getImgPath("/wu-cont/images/widget/", 0);%>
  <c:if test="${requestScope.subjectStatsList}">
<c:forEach var="subjectStatsList" items="${requestScope.subjectStatsList}">

    <div class="wbg w100p fl pd30" id="tgldiv">
      <a href="<%=new SeoUrls().constructIWantToBeSearchUrl(jacsCode)%>"><img src="iwanttobe/tempCss/img/med-icon.png" alt="" /></a>
    <div class="w80p fr">
    <h3 class="w100p cwhite"><%=jacsSubject%> stats</h3>
    <!--Employment rate-->
    <c:if test="${not empty subjectStatsList.stickManFig}">
<c:set var="stickManDef" value="${subjectStatsList.stickManFig}"/>
         
          <%String empRateVal = (String)pageContext.getAttribute("stickManDef");
          if("INSUFFICIENT DATA".equalsIgnoreCase(empRateVal) || "DATA NOT AVAILABLE".equalsIgnoreCase(empRateVal)){%>
          <h4 class="w100p pdt30 fnt18 pr">Employment rate 
              <span class="cr">
              ${subjectStatsList.stickManFig}
               
              </span>
              </h4>
          <%}else{
          %>
          <c:if test="${not empty subjectStatsList.stickManColor}">

            <h4 class="w100p pdt30 fnt18 pr">Employment rate 
              <span class="cr">
                <span class="${subjectStatsList.stickManColor} pdt30">${subjectStatsList.stickManFig} out of 10</span> &nbsp;
                <span class="scr_tltip " onmouseover="showToolTip('toolTip<%=(String)pageContext.getAttribute("intIndexValue")%>')" onmouseout="hideToolTip('toolTip<%=(String)pageContext.getAttribute("intIndexValue")%>')">
                  <i class="fa fa-question-circle fr fnt18"></i> 
                  <span class="scr_tip">${subjectStatsList.empTooltipTxt}</span>
                </span>
              </span>
            </h4>
            <c:set var="stickManColor" value="${subjectStatsList.stickManColor}"/>
            <%String stichManVal = String.valueOf((String)pageContext.getAttribute("stickManDef"));
              String stickManImg = String.valueOf((String)pageContext.getAttribute("stickManColor"));
              String imgPath = tempImgPath + stickManImg + "_ico.png";
              float figSize = Float.parseFloat(stichManVal);%>
              <ul class="rat_ico">
                <%for(int i = 1; i<figSize ; i++) {%>
                  <li><img src="<%=imgPath%>" alt="full stickMan"/></li>
                <%}
                if(stichManVal.indexOf(".5") >= 0){
                  imgPath = tempImgPath + stickManImg + "_gry.png";%>
                  <li><img src="<%=imgPath%>" alt="half stickMan"></li>
                <%}
                float remain = 10.0f - figSize;
                for(int lp = 1; lp < remain; lp++){%>
                  <li><img src="<%=CommonUtil.getImgPath("/wu-cont/images/gry_icon.png", 0)%>" alt="half stickMan"></li>
                <%}%>
              </ul>
              <h5 class="w100p fnt16 fl">supply data by Luke</h5>
              <!--Employment rate-->

            </c:if>
            <%}%>

          </c:if>
          <!--Average salary-->
          <c:if test="${not empty subjectStatsList. salaryRange}">
       <c:set var="salaryRangeDef" value="${subjectStatsList.salaryRange}"/>
          <%String salaryRangeVal = (String)pageContext.getAttribute("salaryRangeDef");
          if("INSUFFICIENT DATA".equalsIgnoreCase(salaryRangeVal) || "DATA NOT AVAILABLE".equalsIgnoreCase(salaryRangeVal)){%>
          <h4 class="w100p pdt30 fnt18 pr">Average salary 
              <span class="cr">
                ${subjectStatsList.salaryRange}
              </span>
              </h4>
              <%}else{%>
                       <h4 class="w100p pdt30 fnt18 fl">Average salary <span class="${subjectStatsList.salaryRangeColor} fr">${subjectStatsList.salaryRange}</span></h4>
            <!--Progress bar-->
            <c:set var="salaryBaClr" value="${subjectStatsList.salaryRangeColor}"/>
            <%String salaryBarChart  = String.valueOf((String)pageContext.getAttribute("salaryBaClr"));
              String percentage = "";
              if(salaryBarChart == "red"){
               percentage = "1%";
              }else if(salaryBarChart == "org"){
              percentage = "33%";
              }else if(salaryBarChart == "grn"){
              percentage = "66%";
              }%>
            <div class="pbar fl"><div class="p${subjectStatsList.salaryRangeColor} fl" style="width:33%; margin-left:<%=percentage%>;"></div></div>
            <p class="lmh w100p fl">
              <span class="fl">Low</span>
              <span>Medium</span>
              <span class="fr">High</span>
            </p>
            <!--Progress bar-->
            <h5 class="w100p fnt16 fl">supplied by Luke</h5>
            <!--Average salary-->
            <%}%>

          </c:if>
        </div>
      </div>
      <!--Section-->
    
    </c:forEach>

    </c:if>