<%--
  * @purpose:  jsp include for i want to be widget subject result pagination..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 16-Feb-2016    Prabhakaran V.            1.0      First draft           wu_549
  * *************************************************************************************************************************
--%>
<%
  int pageno = 1;
  int pagelimit = 0;
  int searchhits = 0;
  int recorddisplay = 0;
  int numofpages = 0;
  int pagenum = 0;
  int lower = 0;
  int upper = 0;
  String sortByWhich = null;
  String salSortByHow = null;
  String empSortByHow = null;
  String jacsCode = null;
  String ultimateSearchId = null;
  String sortByHow = null;
  // Reads the In Parameter from the JSP include action and assign to the respective variable
  if(request.getParameter("pageno") != null){
    pageno = Integer.parseInt(request.getParameter("pageno"));
  }
  if(request.getParameter("sortByHow") != null){
    sortByHow = request.getParameter("sortByHow");
  }
  if(request.getParameter("pagelimit") != null){
    pagelimit = Integer.parseInt(request.getParameter("pagelimit"));
  }
  if(request.getParameter("searchhits") != null){
    searchhits = Integer.parseInt(request.getParameter("searchhits").replaceAll(",", ""));
  }
  if(request.getParameter("recorddisplay") != null){
    recorddisplay = Integer.parseInt(request.getParameter("recorddisplay"));
  }
  if(request.getParameter("sortByWhich") != null){
    sortByWhich = request.getParameter("sortByWhich");
  }
  if(request.getParameter("salSortByHow") != null){
    salSortByHow = request.getParameter("salSortByHow");
  }
  if(request.getParameter("empSortByHow") != null){
    empSortByHow = request.getParameter("empSortByHow");
  }
  if(request.getParameter("ultimateSearchId") != null){
    ultimateSearchId = request.getParameter("ultimateSearchId");
  }
  if(request.getParameter("jacsCode") != null){
    jacsCode = request.getParameter("jacsCode");
  }
  if("SALARY".equalsIgnoreCase(sortByWhich)){
    salSortByHow = sortByHow;
  }else{
    empSortByHow = sortByHow;
  }
  String paginationFlag = "YES";
 //displaying pagination only if(searchhits > recorddisplay)
 if(searchhits > recorddisplay){
 // variable which will hold dynamically-generated-pagination-related-content.      
 StringBuffer paginationContent = new StringBuffer();   
 //some calculation
 numofpages = (searchhits / recorddisplay);
 if(searchhits % recorddisplay > 0) {  numofpages++; }
 pagenum = (int)((pageno - 1) / pagelimit);
 pagenum =  pagenum + 1;
 lower = ((pagenum-1) * pagelimit) + 1;
 upper = java.lang.Math.min(numofpages, (((pageno - 1) / pagelimit) + 1) * pagelimit);
 if((pageno%pagelimit) == 0){
   lower = pageno;
   upper = java.lang.Math.min(numofpages, pageno+pagelimit);
 }
 paginationContent.append("<div class='h2hdr w100p fl pdt10 mb60'>");
 if(pageno > 1){
     paginationContent.append("<a rel='follow' onclick=\"subjectResultPagination(");
     paginationContent.append(String.valueOf(pageno-1)+", '"+jacsCode+"', '"+sortByWhich+"','"+ultimateSearchId+"', '"+salSortByHow+"','"+empSortByHow+"','"+paginationFlag+"'");
     paginationContent.append(")\" class='btn_com ico fl' title='Previous'>Previous</a>");          
 }    
 if(pageno < numofpages){
   paginationContent.append("<a rel='follow' onclick=\"subjectResultPagination(");
   paginationContent.append(String.valueOf(pageno+1)+",'"+jacsCode+"', '"+sortByWhich+"','"+ultimateSearchId+"', '"+salSortByHow+"','"+empSortByHow+"','"+paginationFlag+"'");
   paginationContent.append(")\" class='btn_com ico1 fr' title='Next page'>Next</a>");
 }
 paginationContent.append("</div>");
 out.println(paginationContent.toString());
}  
%>
