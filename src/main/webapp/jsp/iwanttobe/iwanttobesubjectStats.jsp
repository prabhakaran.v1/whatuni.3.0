<%--
  * @purpose:  jsp for showing subject stats results in i want to be widget..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 16-Feb-2016    Prabhakaran V.            1.0      First draft           wu_549
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="com.wuni.util.seo.SeoUrls, WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants"%>
<%String jacsSubject = (String)request.getAttribute("jacsSubject");
  String jacsCode = (String)request.getAttribute("jacsCode");
  String ultimateSearchId = (String)request.getAttribute("ultimateSearchId");
  String tempImgPath = CommonUtil.getImgPath("/wu-cont/images/widget/", 0);
  String gaAccount = new WUI.utilities.CommonFunction().getWUSysVarValue("WU_GA_ACCOUNT");//Added by Indumathi.S for GA logging
  %>
  <c:if test="${not empty requestScope.subjectStatsList}">
   <c:forEach var="subjectStatsList" items="${requestScope.subjectStatsList}" varStatus="rowIndex"> 
    <c:set var="rowIntIndex" value="${rowIndex.index}" /> 
  <div class="wrapp">
  <i class="fa fa-times" onclick="closeWidgetLightBox();"></i>
  <div class="container1">
    <div id="typeform" class="pag3">
      <div class="hdr fl">
        <img src='<%=CommonUtil.getImgPath("/wu-cont/images/widget/briefcase.svg", 1)%>' width="60px" height="60px" alt="briefcase"/>
        <div class="mdc fr">
          <h1><%=jacsSubject%>${subjectStatsList.jobIndustryDefinition}</h1>
         
            <div class="h2hdr fl pdt15">
            <c:if test="${not empty subjectStatsList.jacsDefinition}">

              <h2 class="fl w100p">${subjectStatsList.jacsDefinition}</h2>
  
              </c:if>
              <!--Added ulGAEventTracking for view degrees by Indumathi.S 19_Apr_16 Rel-->
              <a onclick="ulGAEventTracking('VD', '<%=jacsSubject%>', '<%=gaAccount%>'); openNewWindow('<%=new SeoUrls().constructIWantToBeSearchUrl(jacsCode)%>')" class="btn_com bl_bg cwhite fl mt25">View degrees</a>
              <div class="iwtb_back">
              <a onclick="goToPreviousPage();" class="btn_com ico fr mt25">Search again</a>
              <a onclick="goToBack();" class="btn_com ico fr wcs_sts_back">Back</a>
              </div>
            </div>
          
        </div>
      </div>
     <div class="wbg w100p fl pd30">
        <h2 class="wbh2">Subject stats</h2>
        <c:if test="${not empty subjectStatsList.entryRequirements}">

          
          <h4 class="w100p fnt18 pdt25">Typical entry requirements</h4>
          ${subjectStatsList.entryRequirements}
        </c:if>
        <c:if test="${not empty subjectStatsList.stickManFig}">
        <c:set var="stickManDef" value="${subjectStatsList.stickManFig}" />
          <%String empRateVal = pageContext.getAttribute("stickManDef").toString();
          if("INSUFFICIENT DATA".equalsIgnoreCase(empRateVal) || "DATA NOT AVAILABLE".equalsIgnoreCase(empRateVal)){%>
          <h4 class="w100p pdt30 fnt18 pr">Employment rate 
              <span class="fr">
               ${subjectStatsList.stickManFig}
              </span>
              </h4>
          <%}else{
          %>
          <c:if test="${not empty subjectStatsList.stickManColor}">
            <h4 id="empStats<%=(int)pageContext.getAttribute("rowIntIndex")%>" class="w100p pdt30 fnt18 pr">Employment rate 
              <span class="cr">
                <span class="${subjectStatsList.stickManColor} pdt30">${subjectStatsList.stickManFig} out of 10</span> &nbsp;
                <span class="scr_tltip " onclick="showToolTip('toolTipStats<%=(int)pageContext.getAttribute("rowIntIndex")%>')" onmouseover="showToolTip('toolTipStats<%=(int)pageContext.getAttribute("rowIntIndex")%>')" onmouseout="hideToolTip('toolTipStats<%=(int)pageContext.getAttribute("rowIntIndex")%>')" >
                  <i class="fa fa-question-circle fr fnt18" id="toolTipDiv5"></i>
                  <span class="scr_tip" id="toolTipStats<%=(int)pageContext.getAttribute("rowIntIndex")%>">
                    Approximately ${subjectStatsList.stickManFig} out of 10 people who studied <%=jacsSubject%> at University ${subjectStatsList.empTooltipTxt}. <br></br><%=GlobalConstants.HESA_SOURCE%>
                  </span>
                </span>
              </span>
            </h4>
            <c:set var="stickManColor" value="${subjectStatsList.stickManColor}"/>
            <% 
              String imgPath = tempImgPath + (String)pageContext.getAttribute("stickManColor") + "_ico.png";
              float figSize = Float.parseFloat(empRateVal);%>
              <ul class="rat_ico">
                <%for(int i = 1; i<=figSize ; i++) {%>
                  <li><img class="lazy-load" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src="<%=imgPath%>" alt="full stickMan"/></li>
                <%}
                if(empRateVal.indexOf(".5") >= 0){
                  imgPath = tempImgPath + (String)pageContext.getAttribute("stickManColor") + "_gry.png";%>
                  <li><img class="lazy-load" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src="<%=imgPath%>" alt="half stickMan"></li>
                <%}
                float remain = 10.0f - figSize;
                for(int lp = 1; lp < remain; lp++){%>
                  <li><img class="lazy-load" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src="<%=CommonUtil.getImgPath("/wu-cont/images/widget/gry_icon.png", 0)%>" alt="half stickMan"></li>
                <%}%>
              </ul>
              
              <h5 class="w100p fnt16 fl">
              <%=jacsSubject%> graduates who ${subjectStatsList.empTooltipTxt}</h5>
             </c:if>
            <%}%>
          </c:if>
          <c:if test="${ not empty subjectStatsList.salaryRange}">
          <c:set var="salaryRangeDef" value="${subjectStatsList.salaryRange}"/>
          <%String salaryRangeVal = (String)pageContext.getAttribute("salaryRangeDef");
          if("INSUFFICIENT DATA".equalsIgnoreCase(salaryRangeVal) || "DATA NOT AVAILABLE".equalsIgnoreCase(salaryRangeVal)){%>
          <h4 class="w100p pdt30 fnt18 pr">Average salary 
              <span class="fr">
                ${subjectStatsList.salaryRange}
              </span>
              </h4>
              <%}else{%>
            <h4 class="w100p pdt30 fnt18 fl">Average salary <span class="${subjectStatsList.salaryRangeColor} fr">${subjectStatsList.salaryRange}</span></h4>
            <c:set var="salaryBaClr" value="${subjectStatsList.salaryRangeColor}"/>
<c:set var="salaryBarRanges" value="${subjectStatsList.salaryDisplayColor}"/>
            <%String barDifff = (String)pageContext.getAttribute("salaryBarRanges");
            String barDiff[] = barDifff.split("-");
              String barLeft = "0";
              float barWidth = 0.0f;
              if(barDiff.length == 2){
                barLeft = barDiff[0];
                barWidth = Float.parseFloat(barDiff[1]) - Float.parseFloat(barLeft);
                if(barWidth == 0){
                barWidth = 1.0f;
                barLeft = "1";
                }
              }%>
            <div class="pbar fl"><div class="p${subjectStatsList.salaryRangeColor} fl" style="width:<%=barWidth%>%; margin-left:<%=barLeft%>%;"></div></div>
            <p class="lmh w100p fl">
              <span class="fl">Low</span>
              <span>Medium</span>
              <span class="fr">High</span>
            </p>
            <h5 class="w100p fnt16 fl"><spring:message code="wuni.widget.text.salary.rate" arguments="<%=jacsSubject%>"/></h5>
            <%}%>
          </c:if>
          <c:if test="${not empty subjectStatsList.qualSubjectList}">
              <h4 class="w100p pdt30 fnt18 fl">Top 3 A-levels  </h4>
              <h5 class="w100p fnt16 fl">Taken by <%=jacsSubject%> students   </h5>
              <div class="chartbox fl w100p">
               <c:forEach var="qualSubjectList" items="${subjectStatsList.qualSubjectList}" varStatus="index"> 
                <c:set var="indexIntValue" value="${index.index}" />
                <div id="doughnutChart_graduates_<%=pageContext.getAttribute("indexIntValue")%>" class="chart fl">
                  <span>${qualSubjectList.qualSubjectName}</span>
                </div>
                <input type="hidden" name='graduates_<%=pageContext.getAttribute("indexIntValue")%>' id='graduates_<%=pageContext.getAttribute("indexIntValue")%>' value="${qualSubjectList.qualSubPercentage}%" />
                <script type="text/javascript"  id="script_graduates_<%=pageContext.getAttribute("indexIntValue")%>">                            
                  drawSubjectdoughnutchart('graduates_<%=pageContext.getAttribute("indexIntValue")%>','doughnutChart_graduates_<%=pageContext.getAttribute("indexIntValue")%>', '${qualSubjectList.imagePath}');
                </script>
             </c:forEach>
            </div>
        </c:if>
        <div class="dsktp">
          <h4 class="fnt18 fl pdt30">Where should you study? </h4>
          <a onclick="getSubjectResult('SUBJECT-RESULT', '1', '<%=jacsCode%>', '<%=ultimateSearchId%>', 'GRADUATE_EMPLOYED_PERCENT', '', 'D');" class="btn_com fr mt20 bl_bg cwhite">View unis</a>
        </div>
      </div>
    </div>
    <div class="mobtm">
      <h4 class="fnt18 fl">Where should you study? </h4>
      <a onclick="getSubjectResult('SUBJECT-RESULT', '1', '<%=jacsCode%>', '<%=ultimateSearchId%>', 'GRADUATE_EMPLOYED_PERCENT', '', 'D');" class="btn_com fr bl_bg cwhite">View unis</a>
    </div>
  </div>
  </div>
  </c:forEach>
  <input type="hidden" id="jacsCode" name="jacsCode" value="<%=jacsCode%>"/>
  <input type="hidden" id="jacsSubject" name="jacsSubject" value="<%=jacsSubject%>"/>
  <input type="hidden" id="ultimateSearchId" name="ultimateSearchId" value="<%=ultimateSearchId%>"/>
</c:if>