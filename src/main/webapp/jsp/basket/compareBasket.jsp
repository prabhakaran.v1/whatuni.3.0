<%@page import="WUI.utilities.CommonUtil, mobile.util.MobileUtils, WUI.utilities.CommonFunction"%>

<%
  CommonFunction common = new CommonFunction();
  String selectedBasketId = (String)request.getAttribute("selectedBasketId");
  String assocTypeCode = (String)request.getAttribute("assocTypeCode");
  String cookieBasketId = common.checkCookieStatus(request);  
  String collegeCount = request.getAttribute("basketpodcollegecount") != null ? String.valueOf(request.getAttribute("basketpodcollegecount")) : "0";
  int cookieBasketCount = Integer.parseInt(collegeCount);
  String userId = new WUI.utilities.SessionData().getData(request, "y");
  int basketNo = 1;
  String compareSuggestionsListSize = request.getAttribute("compareSuggestionsListSize")!=null ? (String)request.getAttribute("compareSuggestionsListSize") : "0";//Added by Indumathi.S 28_Jun_2016
  String viewedBasketName = (String)request.getAttribute("comparisonName");
  String domainSpecPath = common.getSchemeName(request)+CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
  String mobileFlag = "desktop";
  boolean isMobileFlag = new MobileUtils().userAgentCheck(request);
  String flexSliderMinJs = CommonUtil.getResourceMessage("wuni.flex.slider.min.js", null);
  String myFinalFiveJs = CommonUtil.getResourceMessage("wuni.my.final.five.js", null);
  String flexSliderCss = CommonUtil.getResourceMessage("wuni.compare.flexslider.css", null);
  String doughnutChartJs = CommonUtil.getResourceMessage("wuni.compare.doughnut.chart.js", null);
  String basketJs = CommonUtil.getResourceMessage("wuni.basket.js", null);
  
%>

<body>    
  <header class="clipart">
    <div class="ad_cnr">
      <div class="content-bg">
        <div id="desktop_hdr">
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>               
      </div>      
    </div>
    <%if(!(isMobileFlag)){%>
      <script type="text/javascript" language="javascript" src="<%=new CommonUtil().getJsPath()%>/js/DonutChart/jquery.js"></script>
      <script type="text/javascript" language="javascript" src="<%=new CommonUtil().getJsPath()%>/js/DonutChart/<%=doughnutChartJs%>"></script>
    <%}%>
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/mywhatuni/<%=basketJs%>"></script>
  </header>      
  <div class="my_comp fnl5">
    <section id="heroslider" class="hero_pod p0">
      <div class="fix_top_bor">
        <div id="fixedscrolltop" class="">
          <div class="ad_cnr">
            <div class="content-bg">
              <div class="bc_left f5">
                <div id="bcrummbs" class="sbcr mb-10 pros_brcumb" style="display: block;">
                  <div class="bc">
                    <jsp:include page="/seopods/breadCrumbs.jsp"/>
                  </div>
                </div>
                <h1 class="cf tit2 fnt_lbd">My Final 5</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <div class="ad_cnr" id="compareTable">
      <jsp:include page="/jsp/basket/include/compareTable.jsp"/>
    </div>
    <section class="oml_cnt">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="otherUni" class="choice_container oth_uni_pod">
            <div class="hd fl mb20">Other universities you might like <span class="pre_txt">Based on previous behaviour</span></div>
            <div id="otherUniLoading" style="display:inline-block;"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/f5_loader.gif", 0)%>"></div>
          </div>
          <div id="otherCourse" class="choice_container oth_uni_pod">
            <div class="hd fl mb20">Other courses you might like <span class="pre_txt">Based on previous behaviour</span></div>
            <div id="otherCourseLoading" style="display:inline-block;"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/f5_loader.gif", 0)%>"></div>
          </div>
        </div>
      </div>
    </section>
    <div id="friendsActivityPod"></div>
    <input type="hidden" id="scrollContentHidden" value="" />
  </div>
  <jsp:include page="/jsp/common/wuFooter.jsp" />
  <input type="hidden" name="viewedBasketName" id="viewedBasketName" value="<%=viewedBasketName%>"/>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery.contentcarousel.js"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/jquery/jquery.easing.1.3.js"></script>        
  <jsp:include page="/jsp/basket/include/compareBasketResponsiveScript.jsp" />
  <input type="hidden" name="compareSuggestionsListSize" id="compareSuggestionsListSize" value="<%=compareSuggestionsListSize%>"/>
  <input type="hidden" id="flexSliderCSS" value="<%=flexSliderCss%>"/>
  <input type="hidden" id="refresh" value="no">
  <script type="text/javascript">setTimeout(function(){changeBtnStyle();}, 50);</script>
</body>
