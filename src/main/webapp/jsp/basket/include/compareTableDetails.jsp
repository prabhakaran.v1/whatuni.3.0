<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page import="WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants, mobile.util.MobileUtils" %>

<%
  String sessionBasketId = (String)request.getAttribute("sessionBasketId");
  String assocTypeCode = (String)request.getAttribute("assocTypeCode");
  String newBasketCount = (String)request.getAttribute("newBasketCount");
  String basketCount = (String)request.getAttribute("basketpodcollegecount");   
  String defaultFactorsSize = (String)request.getAttribute("defaultFactorsSize");
  String className = "ca-container";
  String clearingUserType = (String)session.getAttribute("USER_TYPE") ;
  boolean clearingFlag = false;
  String clearingClassName = "";
  CommonFunction common = new CommonFunction();
  String clearingonoff = common.getWUSysVarValue("CLEARING_ON_OFF");
  String actionFlag = !GenericValidator.isBlankOrNull((String)request.getAttribute("actionFlag")) ? (String)request.getAttribute("actionFlag") : "";
  
  String reorderClass = "";
  String reorderAction = "E";
  String swapStyle = "display:none;";
  String swapDivCls = "";
  if("E".equalsIgnoreCase(actionFlag)){
    reorderClass = "swp_sec";
    swapStyle = "display:block;";
    reorderAction = "C";
  }
  String reorderButon = "N";
  int choicePos = 0;
  int finalFive = 0;
  String comparisonListSze = null;
  String gaCollegeIds = "";
  String clearingCls = "";
  String choiceStrCls = "";
  String swapArrCls = "";
  boolean isMobileFlag = new MobileUtils().userAgentCheck(request);
  int uniSplitConst = 25;
%>

<div class="cmp_rht" id="rightContainer">
  <c:if test="${empty requestScope.comparisonList}"> <%className = className + " blnk_ste_cont";%> </c:if>
  <div id="ca-container" class="<%=className%>">                   
    <div class="ca-wrapper">
      <c:if test="${not empty requestScope.comparisonList}">
        <c:set var="columnsize"> ${fn:length(requestScope.comparisonList)} </c:set>
        <c:forEach var="comparisonList" items="${requestScope.comparisonList}" varStatus="compareId">
          <c:set var="compareIdIndex" value="${compareId.index}"/>
          <%clearingClassName="";uniSplitConst = 25;%> 
          <c:if test="${comparisonList.clearingFlag eq 'C'}">
            <%clearingClassName="clr15";%>            
          </c:if>
          <%if(!("E".equalsIgnoreCase(actionFlag))){swapStyle = "display:none;"; swapDivCls = "delcou_pos";}%>
          <c:if test="${not empty comparisonList.finalFiveFlag}">
             <c:set var="choiceCountFlag" value="${comparisonList.finalFiveFlag}"/>
            <%reorderButon = pageContext.getAttribute("choiceCountFlag").toString();%>
         </c:if>
          <c:if test="${empty comparisonList.choicePosition}">
            <%reorderClass = "";swapArrCls = "";%>
          </c:if>
          <div id="carouselEle_<%=pageContext.getAttribute("compareIdIndex").toString()%>" class="ca-item cmp_col_sec <%=reorderClass%> <%=clearingClassName%>">
            <%choicePos = 0;%>
            <c:if test="${not empty comparisonList.finalChoiceCount}">
              <c:set var="finalFiveCount" value="${comparisonList.finalChoiceCount}"/>
              <%finalFive = Integer.parseInt( pageContext.getAttribute("finalFiveCount").toString());%>
            </c:if>
            <c:if test="${not empty comparisonList.choicePosition}">
              <%swapArrCls = "swparw_bg";%>
            </c:if>
            <div class="swap_cnt <%=swapArrCls%> <%=swapDivCls%>" style="<%=swapStyle%>">
              <c:if test="${not empty comparisonList.choicePosition}">
                <c:set var="choicePosition" value="${comparisonList.choicePosition}"/>
                <c:set var="collegeIds" value="${comparisonList.collegeId}"/>
                <% 
                  String swapClassName = "";
                  choicePos = Integer.parseInt(pageContext.getAttribute("choicePosition").toString());
                  if(finalFive == choicePos){swapClassName = "swp_dact";}
                %>
                <%--Added for reorder arrow--%>
                <%if(choicePos > 0 && choicePos < 6 && "E".equalsIgnoreCase(actionFlag)){%>
						            <div class="swap_wrp">
                    <div class="swap_lft"><a href="javaScript:reorderFinalFive('REORDER', '<%=choicePos%>', 'LEFT');"><i class="fa fa-angle-left" aria-hidden="true"></i></a></div>
                    <div class="swap_rht <%=swapClassName%>"><a href="javaScript:reorderFinalFive('REORDER', '<%=choicePos%>', 'RIGHT');"><i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
                  </div>
                <%}
                gaCollegeIds += pageContext.getAttribute("collegeIds").toString()  + ",";
                %>
						   </c:if>
              <%--End of reorder arrow code--%>
            </div>
            <c:if test="${comparisonList.courseDeletedFlag eq 'Y'}">
            <c:if test="${comparisonList.clearingFlag eq 'C'}">
                <div class="ct_clrcou delcou_pos"><span class="txt">This course was in clearing <%=GlobalConstants.CLEARING_YEAR%> and is no longer available</span></div>
              </c:if>
              <c:if test="${comparisonList.clearingFlag ne 'C'}">
                <div class="ct_clrcou delcou_pos"><span class="txt">This course is no longer available</span></div>
              </c:if>
            </c:if>
          <ul id="cmp_row_ht">
            <li id="uni_fstchld<%=pageContext.getAttribute("compareIdIndex").toString()%>">
              <%if(choicePos < 1){%>
                <span class="cmp_list_close">
                  <a  onclick="deleteClearingComparison('${comparisonList.assocId}', '${comparisonList.associateTypeCode}', '${comparisonList.basketId}','','','${comparisonList.collegeId}','${comparisonList.clearingFlag}')">
                    <i class="fa fa-times "></i>
                  </a>
                </span>
              <%}%>
              <%--Added choice by Prabha on 16_May_2017--%>
               <div class="Heading_top">
               <c:if test="${not empty comparisonList.choicePosition}">
                    <%choiceStrCls = "";if(choicePos > 2 && choicePos <= 5){choiceStrCls = "cho_pos";}%>
								            <a class="slft <%=choiceStrCls%>" style="display:block;">
                      <%if(choicePos == 1){%>
                        <i class="fa fa-star"></i>
                      <%}else{%>
                        <i class="fa fa-star-o"></i>
                      <%}%>
                      <span class="">${comparisonList.posText} choice</span>
								            </a>	
                </c:if>
                </div>
                <%--End of choice structure--%>
                <%--Added for final 5/remove button--%>
                <div class="btn_add_final desk_f5">
                  <%if(choicePos > 0){%>
							             <a class="add_final af5_rmv" id="addFinal_<%=pageContext.getAttribute("compareIdIndex").toString()%>" onclick="javascript:addRemoveChoice('REMOVE','<%=pageContext.getAttribute("compareIdIndex").toString()%>');">REMOVE</a>
                  <%}else{%>
                  <c:if test="${comparisonList.courseDeletedFlag ne 'Y' and comparisonList.clearingFlag ne 'C'}">
                    <a class="add_final" id="addFinal_${comparisonList.collegeId}" onclick="javascript:addRemoveChoice('SAVE','<%=pageContext.getAttribute("compareIdIndex").toString()%>');">ADD TO FINAL 5</a>
                  </c:if>
                  <%}%>
                </div>
                <%--End of button structure--%>
                <%--Provider logo--%>                
                <%if(!((isMobileFlag))){%>
                <div class="a5_lgo">
                  <span class="c_im">
                    <a href="${comparisonList.uniHomeURL}" title="${comparisonList.collegeDisplayName}">
                      <img src="${comparisonList.collegeLog}" alt="${comparisonList.collegeDisplayName}" title="${comparisonList.collegeDisplayName}"/>
                    </a>
                  </span>
                </div>
                <%}%>
                <%--End of provider logo--%>
                <input type="hidden" id="collegeId_<%=pageContext.getAttribute("compareIdIndex").toString()%>_hidden" value="${comparisonList.collegeId}"/>
                <input type="hidden" id="finalChoiceId_<%=pageContext.getAttribute("compareIdIndex").toString()%>_hidden" value="${comparisonList.finalChoiceId}"/>
                <input type="hidden" id="courseId_<%=pageContext.getAttribute("compareIdIndex").toString()%>_hidden" value="${comparisonList.courseId}"/>
                <input type="hidden" id="basketContentId_<%=pageContext.getAttribute("compareIdIndex").toString()%>_hidden" value="${comparisonList.basketContentId}"/>
                <input type="hidden" id="collegeName_<%=pageContext.getAttribute("compareIdIndex").toString()%>" value="${comparisonList.collegeDisplayName}"/>
              <div class="uni_lst_txt" id="uni_txt<%=pageContext.getAttribute("compareIdIndex").toString()%>">
              <c:if test="${comparisonList.clearingFlag eq 'C'}">
                <%uniSplitConst = 17;%>
              </c:if>
              <c:if test="${not empty comparisonList.collegeDisplayName}">
                 <c:set var="splitCollegeName" value="${comparisonList.collegeDisplayName}"/>
                 <%if(!(isMobileFlag) && pageContext.getAttribute("splitCollegeName").toString().length()>uniSplitConst){%>
                 
                 <% String originalCollegeName = pageContext.getAttribute("splitCollegeName").toString();
                  
                    originalCollegeName = originalCollegeName.substring(0,uniSplitConst)+"...";%>
                    <a class="tool_tip cmp_nmhide" href="${comparisonList.uniHomeURL}" title="${comparisonList.collegeDisplayName}"><%=originalCollegeName%></a>                       
                    <%}else{%>
                      <a  class="tool_tip cmp_nmhide" href="${comparisonList.uniHomeURL}" title="${comparisonList.collegeDisplayName}"><%=pageContext.getAttribute("splitCollegeName").toString()%></a>
                    <%}%>
                     <a class="cmp_univ_fn" href="${comparisonList.uniHomeURL}" title="${comparisonList.collegeDisplayName}">${comparisonList.collegeDisplayName}</a>
                </c:if>
                <c:if test="${comparisonList.clearingFlag eq 'C'}">  
                  <%clearingCls = "clr15";%>
                  <span class="clr_brd_clr">Clearing</span>
                </c:if>
               </div> 
               
               <div class="crse_txtarea" id="course_txt<%=pageContext.getAttribute("compareIdIndex").toString()%>">
                  <c:if test="${not empty comparisonList.courseTitle}">
                     <c:set var="splitCourseName" value="${comparisonList.courseTitle}"/> 
                     <% String originalCourseName = pageContext.getAttribute("splitCourseName").toString();                       
                       if(originalCourseName.length()>25 && (!(isMobileFlag))){ originalCourseName = originalCourseName.substring(0,25)+"...";%>
                       <%--Added course name field --%>
                       <c:if test="${not empty comparisonList.courseUrl}">
                         <p class="par" id="courseTitle_<%=pageContext.getAttribute("compareIdIndex").toString()%>"><a href="${comparisonList.courseUrl}" title="${comparisonList.courseTitle}"><%=originalCourseName%></a></p>
								                 <a href="javaScript:void(0);" onclick="editCourse('REMOVE_COURSE', '<%=pageContext.getAttribute("compareIdIndex").toString()%>');" id="courseId_<%=pageContext.getAttribute("compareIdIndex").toString()%>_href"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                         <%----%>
                         <textarea class="vbox" autocomplete="off" id="courseName_<%=pageContext.getAttribute("compareIdIndex").toString()%>" onclick="clearCourseTitle(this, '<%=pageContext.getAttribute("compareIdIndex").toString()%>');" onblur="javascript:setCourseNameTxt(this, '<%=pageContext.getAttribute("compareIdIndex").toString()%>');" onkeyup="javascript:autoCompleteFinalFiveCourse(event, this, '<%=pageContext.getAttribute("compareIdIndex").toString()%>');"  name="courseName_<%=pageContext.getAttribute("compareIdIndex").toString()%>" style="display:none;">Enter course</textarea>
                         <input type="hidden" id="courseName_hid_<%=pageContext.getAttribute("compareIdIndex").toString()%>_hidden" value="${comparisonList.courseId}"/>
                         <input type="hidden" id="courseName_<%=pageContext.getAttribute("compareIdIndex").toString()%>_hidden" value=""/>
                         <%----%>
							         </c:if>
							         <c:if test="${empty comparisonList.courseDeletedFlag}">
                         <a class="tool_tip cmp_nmhide" title="${comparisonList.courseTitle}"><%=originalCourseName%></a>
                       </c:if>
                       <%--End of course name field--%>
                       <%}else{%>
                       <c:if test="${not empty comparisonList.courseUrl}">
                         <p class="par" id="courseTitle_<%=pageContext.getAttribute("compareIdIndex").toString()%>"><a href="${comparisonList.courseUrl}" title="${comparisonList.courseTitle}"> <%=originalCourseName%></a></p>
								                 <a href="javaScript:void(0);" onclick="editCourse('REMOVE_COURSE', '<%=pageContext.getAttribute("compareIdIndex").toString()%>');" id="courseId_<%=pageContext.getAttribute("compareIdIndex").toString()%>_href"><i class="fa fa-pencil" aria-hidden="true"></i></a>								
                         <%----%>
                         <textarea class="vbox" autocomplete="off" id="courseName_<%=pageContext.getAttribute("compareIdIndex").toString()%>" onclick="clearCourseTitle(this, '<%=pageContext.getAttribute("compareIdIndex").toString()%>');" onblur="javascript:setCourseNameTxt(this, '<%=pageContext.getAttribute("compareIdIndex").toString()%>');" onkeyup="javascript:autoCompleteFinalFiveCourse(event, this, '<%=pageContext.getAttribute("compareIdIndex").toString()%>');"  name="courseName_<%=pageContext.getAttribute("compareIdIndex").toString()%>" style="display:none;">Enter course</textarea>
                         <input type="hidden" id="courseName_<%=pageContext.getAttribute("compareIdIndex").toString()%>_hidden" value="${comparisonList.courseId}"/>
                         <%----%>
							         </c:if>
							         <c:if test="${empty comparisonList.courseUrl}">
                         <a class="tool_tip cmp_nmhide"><%=originalCourseName%> </a>
                       </c:if>
                      
                    <%}%>
                   
                  </c:if>
                  <c:if test="${empty comparisonList.courseTitle and comparisonList.courseDeletedFlag ne 'Y'}">
                    <textarea class="vbox" autocomplete="off" id="courseName_<%=pageContext.getAttribute("compareIdIndex").toString()%>" onclick="clearCourseTitle(this, '<%=pageContext.getAttribute("compareIdIndex").toString()%>');" onblur="javascript:setCourseSearchText(this, '<%=pageContext.getAttribute("compareIdIndex").toString()%>');" onkeyup="javascript:autoCompleteFinalFiveCourse(event, this, '<%=pageContext.getAttribute("compareIdIndex").toString()%>');"  name="courseName_<%=pageContext.getAttribute("compareIdIndex").toString()%>">Enter course</textarea>
                    <input type="hidden" id="courseName_<%=pageContext.getAttribute("compareIdIndex").toString()%>_hidden" value=""/>
								  </c:if>
                </div>
               
               <div class="btn_add_final desk_f5 mob_f5">
                 <%if(choicePos > 0){%>
                   <a class="add_final af5_rmv" id="addFinal_<%=pageContext.getAttribute("compareIdIndex").toString()%>" onclick="javascript:addRemoveChoice('REMOVE','<%=pageContext.getAttribute("compareIdIndex").toString()%>');">REMOVE</a>
                 <%}else{%>
                 <c:if test="${comparisonList.courseDeletedFlag ne 'Y' and comparisonList.clearingFlag ne 'C'}">
                       <a class="add_final" id="addFinal_<%=pageContext.getAttribute("compareIdIndex").toString()%>" onclick="javascript:addRemoveChoice('SAVE','<%=pageContext.getAttribute("compareIdIndex").toString()%>');">ADD TO FINAL 5</a>
                   </c:if>  
                 <%}%>
               </div> 
             </li>
             <c:forEach var="comparetableValuesList" items="${comparisonList.compareTableDataList}" varStatus="rowIndex">
               <c:set var="rowIndexSet" value="${rowIndex.index}"/>
              <li id="rht_fld<%=pageContext.getAttribute("compareIdIndex").toString()%>_<%=pageContext.getAttribute("rowIndexSet").toString()%>">      
                <%String chartClassname = "";%>
                <c:if test="${comparetableValuesList.visualTypeValue eq 'Donut'}">
                  <%chartClassname  = "";%>
                </c:if>
                <span class="lft_lbl_hid">${comparetableValuesList.factorName}</span>
                <input type="hidden" id="fact_name_hid_<%=pageContext.getAttribute("compareIdIndex").toString()%>_<%=pageContext.getAttribute("rowIndexSet").toString()%>" name="comparetableValuesList"   value="${comparetableValuesList.factorName}"/>
                <span id="donut<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>" class="rht_lst_val <%=chartClassname%>">
                <c:if test="${comparetableValuesList.visualTypeValue eq 'Text'}">
                  <c:if test="${comparetableValuesList.columnvalue ne 'N/A'}">
                    ${comparetableValuesList.columnvalue}
                  </c:if>
                  <c:if test="${comparetableValuesList.columnvalue eq 'N/A'}">
                    <span class="rht_lst_val dummy_desk_text tool_tip cmp_nmhide" style="display:block" id="text<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>"> 
                       ${comparetableValuesList.columnvalue}
                       <span class="cmp tl_pos_top1 cour_cmp">
                        <div class="hdf5"></div>
                        <div><c:if test="${comparetableValuesList.factorName eq 'Whatuni student rating'}">This uni doesn't have any stars because we're awaiting more reviews in order to provide an accurate rating</c:if> <c:if test="${comparetableValuesList.factorName ne 'Whatuni student rating'}"> <spring:message code="wuni.compare.na.tooltip" /></c:if></div>
                        <div class="line cour_line"></div>
                       </span>
                     </span>
                  </c:if>
                </c:if>
                <c:if test="${comparetableValuesList.visualTypeValue eq 'Bar'}">
                  <c:if test="${comparetableValuesList.columnvalue ne 'N/A'}">
                    <span class="rht_lst_val dummy_desk_text" style="display:block" id="text<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>"> 
                      ${comparetableValuesList.columnvalue}
                    </span>
                  </c:if>
                  <c:if test="${comparetableValuesList.columnvalue eq 'N/A'}">
                  <span class="rht_lst_val dummy_desk_text tool_tip cmp_nmhide" style="display:block" id="text<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>"> 
                           ${comparetableValuesList.columnvalue}
                           <span class="cmp tl_pos_top1 cour_cmp">
                             <div class="hdf5"></div>
                             <div><c:if test="${comparetableValuesList.factorName eq 'Whatuni student rating'}">This uni doesn't have any stars because we're awaiting more reviews in order to provide an accurate rating</c:if> <c:if test="${comparetableValuesList.factorName ne 'Whatuni student rating'}"> <spring:message code="wuni.compare.na.tooltip"/></c:if></div>
                             <div class="line cour_line"></div>
                           </span>
                         </span>
                  </c:if>
                  
                  <div class="vert_bar_cont" id="bar<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>" style="display:none;">
                  <c:if test="${not empty comparetableValuesList.columnvalue}">                  
                    <c:if test="${comparetableValuesList.columnvalue ne 'N/A'}">
                        <c:set var="barChartValue" value="${comparetableValuesList.chartprctValue}"/>
                          <%String barChartVal = pageContext.getAttribute("barChartValue").toString();
                            barChartVal = barChartVal.replaceAll("<STRONG>", "");
                            barChartVal = barChartVal.replaceAll("</STRONG>", ""); 
                          %>
                            <div class="vert_bar" style="height:<%=barChartVal%>px;"> </div>
                            <c:if test="${comparetableValuesList.factorName ne 'UCAS points needed'}">
                              <span class="rht_lst_val dummy_chart_text">${comparetableValuesList.columnvalue}</span>
                            </c:if>
                            <c:if test="${comparetableValuesList.factorName eq 'UCAS points needed'}">
                             <c:set var="ucasBarChart" value="${comparetableValuesList.columnvalue}"/>
                             <%String ucasSplitString = pageContext.getAttribute("ucasBarChart").toString().replace("<STRONG>","").replace("</STRONG>","");
                              if(ucasSplitString.length()>12){
                               ucasSplitString = ucasSplitString.substring(0,12)+"...";%>                              
                                <a class="tool_tip cmp_nmhide" href="${comparisonList.uniHomeURL}"><%=ucasSplitString%> 
                                  <span class="cmp tl_pos_top1 cour_cmp">
                                    <div class="hdf5"></div>
                                    <div>${comparetableValuesList.columnvalue}</div>
                                    <div class="line cour_line"></div>
                                  </span>                    
                                </a>  
                             <%}else{%> 
                               <span class="rht_lst_val dummy_chart_text">${comparetableValuesList.columnvalue}</span>
                             <%}%>
                            </c:if>
                        </c:if>
                        <c:if test="${comparetableValuesList.columnvalue eq 'N/A'}">
                          <span class="rht_lst_val dummy_chart_text tool_tip cmp_nmhide">
                            ${comparetableValuesList.columnvalue}
                            <span class="cmp tl_pos_top1 cour_cmp">
                              <div class="hdf5"></div>
                              <div><c:if test="${comparetableValuesList.factorName eq 'Whatuni student rating'}">This uni doesn't have any stars because we're awaiting more reviews in order to provide an accurate rating</c:if> <c:if test="${comparetableValuesList.factorName ne 'Whatuni student rating'}"> <spring:message code="wuni.compare.na.tooltip"/></c:if></div>
                              <div class="line cour_line"></div>
                            </span>
                            </span>
                        </c:if>                      
                    </c:if>
                  </div>
                </c:if>
                <c:if test="${comparetableValuesList.visualTypeValue eq 'Donut'}">
                   <%String spanDonutText = "";%>
                   <c:if test="${not empty comparetableValuesList.columnvalue}">
                   <c:if test="${comparetableValuesList.columnvalue ne 'N/A'}">
                      <c:set var="spandonutvalue" value="${comparetableValuesList.columnvalue}"/>
                      <%String spanvalueArr[] = pageContext.getAttribute("spandonutvalue").toString().split(":");spanDonutText = pageContext.getAttribute("spandonutvalue").toString(); if(spanvalueArr.length==3){%>
                      <c:if test="${comparetableValuesList.factorName eq 'How you&#39;re assessed'}">
                                <% spanDonutText = "<span class= \"ht_cntrl\"><span class=\"rht_lst_val\">Coursework "+spanvalueArr[0]+"%</span>";
                                   spanDonutText = spanDonutText +   "<span class=\"rht_lst_val\">Exam "+ spanvalueArr[1]+"%</span>";
                                   spanDonutText = spanDonutText +   "<span class=\"rht_lst_val\">Practical work "+ spanvalueArr[2]+"%</span></span>";%>
                      </c:if>
                      <%}%>
                      <span class="rht_lst_val dummy_desk_text" style="display:block" id="text<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>"><%=spanDonutText%></span>
                      </c:if>
                      <c:if test="${comparetableValuesList.columnvalue eq 'N/A'}">
                  <span class="rht_lst_val dummy_desk_text tool_tip cmp_nmhide" style="display:block" id="text<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>"> 
                           ${comparetableValuesList.columnvalue}
                           <span class="cmp tl_pos_top1 cour_cmp">
                             <div class="hdf5"></div>
                             <div><c:if test="${comparetableValuesList.factorName eq 'Whatuni student rating'}">This uni doesn't have any stars because we're awaiting more reviews in order to provide an accurate rating</c:if> <c:if test="${comparetableValuesList.factorName ne 'Whatuni student rating'}"> <spring:message code="wuni.compare.na.tooltip"/></c:if></div>
                             <div class="line cour_line"></div>
                           </span>
                         </span>
                  </c:if>
                   </c:if>
                   <div class="rich_profile donut_ht" id="donutchart<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>" style="display:none;">
                    <c:if test="${not empty comparetableValuesList.columnvalue}">
                      <c:if test="${comparetableValuesList.columnvalue ne 'N/A'}">
                          <c:set var="donutvalue" value="${comparetableValuesList.columnvalue}"/>
                          <c:set var="factorNameValue" value="${comparetableValuesList.factorName}"/>
                          <%String factorNameVal = ""; %>
                           <c:if test="${comparetableValuesList.factorName eq 'How you&#39;re assessed'}">
                            <% 
                            pageContext.setAttribute("factorNameValue", "Course work:Exam:Practical work");%>
                           </c:if>
                            <%
                              String valueArr[] = pageContext.getAttribute("donutvalue").toString().split(":");
                              String factorNameArr[] = pageContext.getAttribute("factorNameValue").toString().split(":"); 
                              String dispDonutText = pageContext.getAttribute("donutvalue").toString();
                            %>
                          <input type="hidden" name="factorName0_<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>" id="factorName0_<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>" value="<%=valueArr[0]%>"/>
                          <input type="hidden" name="factorName1_<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>" id="factorName1_<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>" value="<%=valueArr[1]%>"/>
                         
                           <%if(valueArr.length ==3){%>
                             <input type="hidden" name="factorName2_<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>" id="factorName2_<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>" value="<%=valueArr[2]%>"/>
                             <%}%>
                            <div class="key_stats">
                              <div class="fl keyml">
                                <div class="fl">
                                  <div id="fp_doughnut_<%=pageContext.getAttribute("rowIndexSet").toString()%>_<%=pageContext.getAttribute("compareIdIndex").toString()%>" class="chart w_149"></div>
                                </div>
                                <div class="fl circle_desc">
                                  <div class="fl w150 ">
                                    <div class="circle red fl"></div>
                                    <div class="fl keyfw"><span class="hdf3 fl keyfwc"><%=factorNameArr[0]%></span>
                                    </div>
                                  </div>
                                  <div class="fl w150">
                                    <div class="circle maroon  fl"></div>
                                    <div class="fl keyfw">
                                      <span class="hdf3 fl keyfwc"><%=factorNameArr[1]%></span>
                                    </div>
                                  </div>
                                  <c:if test="${comparetableValuesList.factorName eq 'How you&#39;re assessed'}">
                                    <div class="fl w150 donut_mt10">
                                      <div class="circle yellow  fl"></div>
                                      <div class="fl keyfw">
                                        <span class="hdf3 fl keyfwc"><%=factorNameArr[2]%></span>
                                      </div>
                                    </div>
                                  </c:if>
                                </div>
                              </div> 
                            </div>
                        </c:if>
                        <c:if test="${comparetableValuesList.columnvalue eq 'N/A'}">
                         <span class="rht_lst_val dummy_desk_text tool_tip cmp_nmhide" style="display:block" id="text<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>"> 
                           ${comparetableValuesList.columnvalue}
                           <span class="cmp tl_pos_top1 cour_cmp">
                             <div class="hdf5"></div>
                             <div><c:if test="${comparetableValuesList.factorName eq 'Whatuni student rating'}">This uni doesn't have any stars because we're awaiting more reviews in order to provide an accurate rating</c:if> <c:if test="${comparetableValuesList.factorName ne 'Whatuni student rating'}"> <spring:message code="wuni.compare.na.tooltip"/></c:if></div>
                             <div class="line cour_line"></div>
                           </span>
                         </span>
                        </c:if>
                      </c:if>
                      <c:if test="${empty comparetableValuesList.columnvalue}">
                    <span class="rht_lst_val dummy_chart_text tool_tip cmp_nmhide" style="display:block" id="empty<%=pageContext.getAttribute("compareIdIndex").toString()%><%=pageContext.getAttribute("rowIndexSet").toString()%>"> 
                      ${comparetableValuesList.columnvalue}
                      <span class="cmp tl_pos_top1 cour_cmp">
                        <div class="hdf5"></div>
                        <div><c:if test="${comparetableValuesList.factorName eq 'Whatuni student rating'}">This uni doesn't have any stars because we're awaiting more reviews in order to provide an accurate rating</c:if> <c:if test="${comparetableValuesList.factorName ne 'Whatuni student rating'}"> <spring:message code="wuni.compare.na.tooltip"/></c:if></div>
                        <div class="line cour_line"></div>
                      </span>
                    </span>
                  </c:if>
                  </div>
                </c:if>
              </span>
              </li>
            </c:forEach>
            
            </ul>
        </div>
      </c:forEach>
      <%comparisonListSze = String.valueOf(pageContext.getAttribute("columnsize").toString());%>    </c:if>
  </div>
  <input type="hidden" name="comparisonListSize" id="comparisonListSize" value="<%=comparisonListSze%>"/>
  <input type="hidden" name="cmpbasketCount" id="cmpbasketCount" value="<%=basketCount%>"/>
  <input type="hidden" name="sessionBasketId" id="sessionBasketId" value="<%=sessionBasketId%>"/>
  <input type="hidden" name="assocTypeCode" id="assocTypeCode" value="<%=assocTypeCode%>"/>    
  <input type="hidden" name="defaultFactorsSize" id="defaultFactorsSize" value="<%=defaultFactorsSize%>">
  <input type="hidden" name="curPos" id="curPos" value="">
  <input type="hidden" id="reorderButon" value="<%=reorderButon%>"/>
  <input type="hidden" id="reorderAction" value="<%=reorderAction%>"/>
  <input type="hidden" id="gaCollegeIds" value="<%=gaCollegeIds%>"/>
  <input type="hidden" id="leaveFromUrl" value=""/>
  <input type="hidden" id="clrCls" value="<%=clearingCls%>"/>
</div>

</div>
