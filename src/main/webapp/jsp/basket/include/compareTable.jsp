<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form"  prefix="form" %> 
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants" %>
<%String currentBasketId = (String)request.getAttribute("selectedBasketId"); 
  String assocTypeCode = (String)request.getAttribute("assocTypeCode");
  
  String comparetabclassname = "";
  if("O".equals(assocTypeCode)){
     comparetabclassname = "lft_catlbl";
  }
    String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");
    String clearingUserType = (String)session.getAttribute("USER_TYPE") ;
    String searchValue = "Undergraduate";
    boolean clearingFlag = false;
    String onsubmitFunction = "compSearchSubmit('OFF')";
    String formSubmit = "";
    String actionValue = "";
    String defaultFactorsSize = (String)request.getAttribute("defaultFactorsSize");
    String sessionBasketId = (String)request.getAttribute("sessionBasketId");
    String searchType = (String)session.getAttribute("USER_TYPE");
%>      
<div class="content-bg">
  <c:if test="${not empty requestScope.actionFlag}">
    <c:if test="${requestScope.actionFlag eq 'E' }">
      <div id="topAlertTxt" class="f5edi_conf">
        <div class="success f5edt_txt">
          <p class="txt fnt_lrg">Finish re-ordering your Final 5 selection by clicking on the <b>Confirm changes</b> below</p>
        </div>
      </div>
    </c:if>
  </c:if>
  
  <div id="subCont" class="subCon cf">
    <div class="cmp_wrap" id="compareContainersection" style="display:inline-block;">
      <c:if test="${not empty requestScope.comparisonList}">
        <jsp:include page="/jsp/basket/include/compareContainer.jsp"/>
      </c:if>
    </div>
    <%String blankStateCls = "display:none;";%>
    <c:if test="${empty requestScope.comparisonList}">
      <%blankStateCls = "display:block;";%>
    </c:if>
    <div id="blankStateId" class="no_cuniv" style="<%=blankStateCls%>">
      <%if("ON".equals(clearingonoff)){%>
        <%if(!GenericValidator.isBlankOrNull(clearingUserType) && "CLEARING".equals(clearingUserType)){ searchValue = GlobalConstants.CLEARING_NAV_TXT;}%>
          <%clearingFlag = true; onsubmitFunction = "compSearchSubmit('ON')"; formSubmit = ""; actionValue ="";%>
        <%}else{%>
          <%clearingFlag = false; onsubmitFunction = "compSearchSubmit('OFF')"; formSubmit = "compSearchSubmit('OFF')"; actionValue ="action=\"/home.html\"";%>
      <%}%>
      <div class="cmp_blnk_st">
        <div class="srch_img"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/blnkst_srch_img.png", 0)%>" alt="Blank state search icon"></div>
        <div class="blnk_st_srch" id="compare_bas_pos">Looks like you haven't saved <br> anything yet!</div>
        <div class="blnk_ste_tle">WHY NOT SEARCH TO GET STARTED?</div> 
             
        <form id="compSearchForm" method="post" <%=actionValue%> onsubmit="return <%=onsubmitFunction%>" >
          <div class="filt_srch">
            <div class="ns_dd">
              <fieldset>                
                <input type="text" name="qualification" id="compQual" class="fnt_lbk sbx" value="<%=searchValue%>" readonly="readonly" onblur="setTimeout('hideBlock()',500);" onclick="javascript:toggleCompQualList();" />
                <ul id="compQualList" class="ts-option" style="display: none;">
                  <%--<%if(clearingFlag){%><li onclick="javascript:setCompQual(this);"><%=GlobalConstants.CLEARING_NAV_TXT%></li><%}%>--%>
                  <li onclick="javascript:setCompQual(this);">Undergraduate</li>
                  <li onclick="javascript:setCompQual(this);">HND / HNC</li>
                  <li onclick="javascript:setCompQual(this);">Foundation degree</li>
                  <li onclick="javascript:setCompQual(this);">Access &amp; foundation</li>                  
                  <li onclick="javascript:setCompQual(this);">Postgraduate</li>
                  <%--<li onclick="javascript:setCompQual(this);">UCAS CODE</li>--%>
                </ul>
              </fieldset>
            </div>
            <fieldset class="fr srch_box">
              <input type="text" onkeypress="javascript:if(event.keyCode==13){return <%=onsubmitFunction%>;}" maxlength="4000" class="fnt_lrg gen_srchbox" onblur="setCompUniDefaultTEXT(this)" onfocus="clearCompUniDefaultTEXT(this);" onclick="clearCompUniDefaultTEXT(this)" onkeyup="autoCompleteCompKeywordBrowse(event,this)" onkeydown="clearUniNAME(event,this)" autocomplete="off" value="Enter uni name or subject" id="compKwd" name="compKeyword">
              <input type="hidden" value="" id="compKeyword_hidden">
              <input type="hidden" value="" id="compkeyword_id">
              <input type="hidden" value="" id="compKeyword_display">
              <input type="hidden" value="" id="compKeyword_url">
              <input type="hidden" value="" id="compKeyword_alias">
              <input type="hidden" value="" id="compKeyword_location">
              <%if(clearingFlag){%>
                <input type="button" onclick="compSearchSubmit('ON');"  value="" class="gen_icon_srch fr">
              <%}else{%>
                <input type="submit" value="" class="gen_icon_srch fr">
              <%}%>
              <input type="hidden" value="" id="compKeywordbrowse">
              <input type="hidden" id="compSelQual" value=""/>
            </fieldset>
          </div>
        </form>
      </div>
    </div>
    <input type="hidden" name="assocTypeCode" id="assocTypeCode" value="<%=assocTypeCode%>"/> 
    <input type="hidden" name="defaultFactorsSize" id="defaultFactorsSize" value="<%=defaultFactorsSize%>">
    <input type="hidden" name="selectedBasketId" id="selectedBasketId" value="<%=currentBasketId%>"/>
    <input type="hidden" name="sessionBasketId" id="sessionBasketId" value="<%=sessionBasketId%>"/>
    
  </div>
</div>