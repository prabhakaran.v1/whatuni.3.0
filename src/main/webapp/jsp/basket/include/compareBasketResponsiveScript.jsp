<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator" %>
<%String selectedBasketId = (String)request.getAttribute("selectedBasketId");
  String assocTypeCode = (String)request.getAttribute("assocTypeCode");
  String cookieBasketId = new WUI.utilities.CommonFunction().checkCookieStatus(request);  
  String collegeCount = request.getAttribute("basketpodcollegecount") != null ? String.valueOf(request.getAttribute("basketpodcollegecount")) : "0";
  int cookieBasketCount = Integer.parseInt(collegeCount);
  String userId = new WUI.utilities.SessionData().getData(request, "y");
  int basketNo = 1;

  String viewedBasketName = (String)request.getAttribute("comparisonName");
%>



      <script type="text/javascript">
        laodonload();
        var dev= jQuery.noConflict();       
      
        dev('#ca-container').height(2+(dev('#cmp_lft_inr').outerHeight(true)));  
        dev('#ca-container').contentcarousel({infinite:false});      
       //
       if(dev('#reorderButon').val() == 'Y'){
    dev('#reorderBtn').show();
  }else if(dev('#reorderButon').val() == 'N'){
    dev('#reorderBtn').hide();
  }
  if(dev('#reorderAction').val() == 'E'){
    dev('#reorderDiv').show();
    dev('#confirmDiv').hide();
  }else if(dev('#reorderAction').val() == 'C'){
    dev('#reorderDiv').hide();
    dev('#confirmDiv').show();
    
    
  }
       //
//REsponsivr
function setHeight(noOfElements, noOfColumn, noOfSet){
  for(var set = 0; set < noOfSet; set++){
    for(var i = 0; i < noOfElements; i++){
     var ele = "rht_fld"+set+"_"+i;
       dev('#'+ele).css("height","auto");
       dev('#'+ele).css("line-height","24px");      
    }
  }
  var width = document.documentElement.clientWidth;
  if (width <= 480) {
    for(var set = 0; set < noOfSet; set++){
      for(var i = 0; i < noOfElements; i = i+noOfColumn){
       var height1 = 0;
       var height2 = 0;
       var height3 = 0;
       var height4 = 0;
        dev('.vert_bar_cont').css('display','none');
        dev('.rich_profile donut_ht').css('display','none');
        height1 = dev('#rht_fld'+set+"_"+i).height();
        height2 = dev('#rht_fld'+set+"_"+(i+1)).height();
        if(height1 < height2){
            height1 = height2;
        }
        dev('#rht_fld'+set+"_"+i).height((height1));
        dev('#rht_fld'+set+"_"+(i+1)).height((height1));
        dev('#rht_fld'+(set+1)+"_"+i).height((height1));
        dev('#rht_fld'+(set+1)+"_"+(i+1)).height((height1));
    }
   }
   dev('.vert_bar_cont').css('display','none');
   dev('.rich_profile donut_ht').css('display','none');
 }else if(width > 480 && width <= 992){
    for(var set = 0; set < noOfSet; set++){
      for(var i = 0; i < noOfElements; i = i+noOfColumn){
       var height1 = 0;
       var height2 = 0;
       var height3 = 0;
       var height4 = 0;
        dev('.vert_bar_cont').css('display','none');
        dev('.rich_profile donut_ht').css('display','none');
        height1 = dev('#rht_fld'+set+"_"+i).height();
        height2 = dev('#rht_fld'+set+"_"+(i+1)).height();
        height3 = dev('#rht_fld'+(set+1)+"_"+i).height();
        height4 = dev('#rht_fld'+(set+1)+"_"+(i+1)).height();
        
        if(height1 < height2){
          height1 = height2;
        }
        if(height3<height4){
          height3 = height4;
        }
        if(height1<height3){
          height1=height3;
        }      
        dev('#rht_fld'+set+"_"+i).height((height1));
        dev('#rht_fld'+set+"_"+(i+1)).height((height1));
        dev('#rht_fld'+(set+1)+"_"+i).height((height1));
        dev('#rht_fld'+(set+1)+"_"+(i+1)).height((height1));
        }
      }
    dev('.vert_bar_cont').css('display','none');
    dev('.rich_profile donut_ht').css('display','none');
    dev('.vert_bar_cont').find('#cmp_row_ht li').css('height','auto');
    }  
  }  
  dev(document).ready(function(){
   setDefaultHeightComp();
  });   
  function setDefaultHeightComp(){
    var comparisonListSize = 0;
    if(document.getElementById("comparisonListSize")){
      comparisonListSize = document.getElementById("comparisonListSize").value;
    }
    var defaultFactorsSize = document.getElementById("defaultFactorsSize").value;  
    //dev(window).trigger('resize');
    resizeFn();
    dev("#cmp_ad_link").click(function(){
      dev(this).next(".cmp_add_more").toggleClass("uni_edt_vw");	
    });
    var width = document.documentElement.clientWidth;
    if (width <= 992) {    
      if(comparisonListSize>0){
        setHeight(defaultFactorsSize, 2, comparisonListSize);
      }
      dev('.cmp_col_sec').css("left","0px");
    }
  }
  
  function resizeFn(){
  adjustStyle();
  var width = document.documentElement.clientWidth;
  if (width <= 992) { 
    dev('.lft_drpchrt_ht').removeClass('lft_drpchrt_ht').addClass('chart_dummy');
    dev('.lft_drpdnt_ht').removeClass('lft_drpdnt_ht').addClass('dochart_dummy');
    dev('.dummy_desk_text').css('display','block');
    dev('.rich_profile donut_ht').css('display','none');
  }else{
   if(document.getElementById("comparisonListSize")){
      comparisonListSize = document.getElementById("comparisonListSize").value;
    }
  var item =    dev("span[id^='left_icon']");

  for(var i=0;i<item.length;i++){
    var cnames = dev(item[i]).attr("class");
  
    if (cnames.indexOf('chart_dummy') > -1) {
      var ht = document.getElementById('left_icon'+i).style.height;     
      dev('#lft_fld'+i).css("height",ht);
      for(j=0;j<comparisonListSize;j++){
        dev('#rft_fld'+j+i).css("height",dev(item[i]).height());
      }
    }
  }
    dev('.chart_dummy').addClass('lft_drpchrt_ht').removeClass('chart_dummy').find('.vert_bar_cont').css('display','table-cell'); 
    dev('.dochart_dummy').addClass('lft_drpdnt_ht').removeClass('dochart_dummy').find('.rich_profile donut_ht').css('display','table-cell');
    dev('.lft_drpchrt_ht').find('.dummy_desk_text').css('display','none');
    dev('.lft_drpdnt_ht').find('.dummy_desk_text').css('display','none');
    dev('.dummy_left_label').css('height','24px');
  }  
  resizeCells();
  changeBtnStyle(); 
}
    
 function resizeCells(){
    var comparisonListSize = 0;
    if(document.getElementById("comparisonListSize")){
      comparisonListSize = document.getElementById("comparisonListSize").value;
    }
    if(comparisonListSize>0){
      var defaultFactorsSize = document.getElementById("defaultFactorsSize").value;  
      var screenWidth = jqueryWidth();
      for(var set = 0; set < comparisonListSize; set++){
        for(var i = 0; i < defaultFactorsSize; i++){
          dev('#rht_fld'+set+"_"+i).css("height", "auto");}
        }
        if ((screenWidth > 480) && (screenWidth <= 992)) {        
         setHeight(defaultFactorsSize, 2, comparisonListSize);
         dev('.cmp_col_sec').css("left","0");
        }else{
        if (screenWidth <= 480) {     
          setHeight(defaultFactorsSize, 2, comparisonListSize);
          dev('.cmp_col_sec').css("left","0");         
        }else{
          var $el = dev('.ca-container'),
          $wrapper		= $el.find('div.ca-wrapper'),
          $items			= $wrapper.children('div.ca-item'),
          cache			= {};
          cache.itemW			= $items.width();
          // save the number of total items
          cache.totalItems	= $items.length;
          $items.each(function(i) {
          dev('#carouselEle'+i).css({
          position	: 'absolute',
          left		: i * cache.itemW + 'px'
          });
        });
        var elements = dev("li[id^='rht_fld']");
        
        if (elements.length > 0) {           
          for (var i = 0; i < elements.length; i++) {
            var ele = elements[i].id;
            dev('#'+ele).css("height","auto");
            dev('#'+ele).css("line-height","24px");
          }
        }
        var factElements =  dev("li[id^='lft_fld']");
        if (factElements.length > 0) {           
          for (var i = 0; i < factElements.length; i++) {
             var ele = factElements[i].id;
             dev('#'+ele).css("height","auto");
             dev('#'+ele).css("line-height","24px");
          }
        }
       setDesktopView(comparisonListSize);     
       dev('#ca-container').height(2+(dev('#cmp_lft_inr').outerHeight(true))); 
       dev('#ca-container').contentcarousel({infinite:false});
       dev('#ca-container').height(2+(dev('#cmp_lft_inr').outerHeight(true)));
       //
       if(dev('#reorderButon').val() == 'Y'){
    dev('#reorderBtn').show();
  }else if(dev('#reorderButon').val() == 'N'){
    dev('#reorderBtn').hide();
  }
  if(dev('#reorderAction').val() == 'E'){
    dev('#reorderDiv').show();
    dev('#confirmDiv').hide();
  }else if(dev('#reorderAction').val() == 'C'){
    dev('#reorderDiv').hide();
    dev('#confirmDiv').show();
  }
       //
         }    
        }
      }
    }
  </script>

