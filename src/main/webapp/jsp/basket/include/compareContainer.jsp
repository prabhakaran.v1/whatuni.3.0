<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="org.apache.commons.validator.GenericValidator" %>
<%
  String arrowClassName = "display:block;";
  String actionFlag = !GenericValidator.isBlankOrNull((String)request.getAttribute("actionFlag")) ? (String)request.getAttribute("actionFlag") : "";
  String reorderButon = "N";
%>      

<div class="cmp_lft">
  <ul id="cmp_lft_inr" class="cmp_lft_inr">
    <li class=""></li>
    <c:if test="${not empty requestScope.defaultFactorsList}">
      <%String jsname="";%>
      <c:forEach var="defaultFactorsList" items="${requestScope.defaultFactorsList}" varStatus="leftIndex">
        <c:set var="leftIndexSet" value="${leftIndex.index}"/>
      <c:if test="${defaultFactorsList.visualType eq 'Text'}">
        <li>
      </c:if>
      <c:if test="${defaultFactorsList.visualType ne 'Text'}">
        <li onclick="javascript:onhideandShow('<%=pageContext.getAttribute("leftIndexSet").toString()%>', '${defaultFactorsList.visualType}')">
      </c:if>
      <span class="cmp_left_icn" id="left_icon<%=pageContext.getAttribute("leftIndexSet").toString()%>" >
        <%arrowClassName = "display:block;";%>
        <c:if test="${defaultFactorsList.visualType eq 'Text'}">
          <%arrowClassName = "display:none;";%>
        </c:if>
        <i id="factorArrow<%=pageContext.getAttribute("leftIndexSet").toString()%>" class="fa fa-chevron-right" style="<%=arrowClassName%>"></i>             
      </span>
      <span id="lft_fld<%=pageContext.getAttribute("leftIndexSet").toString()%>" class="cmp_left_label dummy_left_label" style="height: 24px;">
        <div class="tool_tip" >                
          <span class="cmp_bor_btm">${defaultFactorsList.factorName}</span>
          <c:if test="${not empty defaultFactorsList.helpText}">
              <span class="cmp tl_pos_top">
                <div class="hdf5"></div>
                <div class="tltip_hd">
                  <c:if test="${defaultFactorsList.factorName eq 'CompUniGuide Ranking'}">
                    Complete University Guide Ranking
                  </c:if>
                  <c:if test="${defaultFactorsList.factorName ne 'CompUniGuide Ranking'}">
                    ${defaultFactorsList.factorName}
                  </c:if>
                </div>
                <div>${defaultFactorsList.helpText}</div>
                <div class="line"></div>
              </span>
            </c:if>
          </div>
        </span>  
      </li>
      </c:forEach>
    </c:if>
  </ul>
</div>

<%--Comparison table--%>
<div class="cmp_rht" id="compareRightPod">
  <jsp:include page="/jsp/basket/include/compareTableDetails.jsp"/>
</div>
<%--End of comparson table--%>
<%--Add more fields section--%>
<div id="cmpad_cnt" class="cmpad_cnt">
<c:if test="${not empty requestScope.addFactorsList}">
    <div class="cmp_ad_link">
      <a id="cmp_ad_link" onclick="openPopup('addMoreLink');" >+ Add more fields</a>
      <div class="cmp_add_more" id="addMoreLink" style="display:none;">
        <span class="am_arrow"></span>
        <span class="cmp_edt_txt">Add / edit comparison fields</span>
        <span class="cmp_list_close">
          <a href="javaScript:void(0);" onclick="closePoppup('addMoreLink');"> <i class="fa fa-times "></i></a>
        </span>
        <div class="pros_scroll">
          <ul class="uni_menu">
            <%String classname="";%>
            <c:forEach var="addFactorsList" items="${requestScope.addFactorsList}" varStatus="factorId">
              <%classname = "";%>
              <c:if test="${addFactorsList.factorStatus eq 'Y'}">
                <%classname = "cancel";%>
              </c:if>
              <li id="factors${addFactorsList.factorId}" class="<%=classname%>">
                <a onclick="javascript:changeClass('${addFactorsList.factorId}');">
                  <span class="fl uni fnt_lrg">${addFactorsList.factorName}</span>
                  <c:if test="${addFactorsList.factorStatus eq 'Y'}">
                    <i id="factorStatus${addFactorsList.factorId}" class="fa fa-check fa-1_5x fr"></i>
                  </c:if>
                  <c:if test="${addFactorsList.factorStatus eq 'N'}">
                    <i id="factorStatus${addFactorsList.factorId}" class="fa fa-plus fa-1_5x fr"></i>
                  </c:if>
                </a>
              </li>
            </c:forEach>
          </ul>
        </div>
        <div class="uni_edit_wrap">
          <div class="uni_edtvw_btn">
            <span class="uev_save">
              <button type="submit" value="Save" onclick="javascript:addAdditionalFactors();" title="Apply changes">Apply changes</button>
            </span>
          </div>
        </div>
      </div>
    </div>
  </c:if>
  <%--End of add more fields--%>
  <%--Reorder/confirm button --%>
  <div id="reorderBtn" class="del_cmp sr_calbtn3a">
    <div id="reorderDiv" class="reord_cnt">
				  <a class="rord_f5" onclick="javascript:reorderFinalFive('E');" title="Reorder final 5"> 
        <div class="rord_btn" style="width: 105px;margin: 0 auto;"> Reorder Final 5</div>
				  </a>
		  </div>
    <div id="confirmDiv" class="reord_cnt" style="display:none;">
				  <a class="rord_f5 f5_conf" onclick="javascript:reorderFinalFive('C');" title="Confirm changes"> 
        <div class="rord_btn"><i class="fa fa-check fa-1_2x fr"></i> Confirm changes</div>
				  </a>
		  </div>
  </div>
</div>
<%--End of reorder/confirm button --%>

         
         

   