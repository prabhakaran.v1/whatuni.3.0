<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.*, org.apache.commons.validator.GenericValidator, mobile.util.MobileUtils" autoFlush="true" %>

<%
  String assocTypeCode = !GenericValidator.isBlankOrNull((String)request.getAttribute("associateType")) ? (String)request.getAttribute("associateType") : "";
  String titleText = "universities";
  String slideId = "uniPod";
  if("COURSE".equalsIgnoreCase(assocTypeCode)){titleText ="courses";slideId = "coursePod";}
  String gaCollegeName = "";
  String sliderId = !GenericValidator.isBlankOrNull((String)request.getAttribute("sliderId")) ? (String)request.getAttribute("sliderId") : "1";
  CommonFunction common = new CommonFunction();
  boolean isMobileFlag = new MobileUtils().userAgentCheck(request);
%>

<c:if test="${not empty requestScope.suggestedComparisonList}">

  <div class="hd fl mb20">Other <%=titleText%> you might like <span class="pre_txt">Based on previous behaviour</span></div>
  <div id="slider_<%=sliderId%>" class="flex_slider">
    <ul class="slides drag_container othuni_pod" id="<%=slideId%>">
    <c:forEach var="suggestedComparison" items="${requestScope.suggestedComparisonList}" varStatus="i">
      <li class="drag_box">
        <div class="fnl5_drag">
          <div class="fnl5_step1">
            <div class="Br_lne bg_white">
              <div class="cn">
                <span class="c_im"><a href="${suggestedComparison.uniHomeURL}"> <img id="sugUniLogo_1" src="${suggestedComparison.collegeLog}" alt="${suggestedComparison.collegeDisplayName}" title="${suggestedComparison.collegeDisplayName}"></img></a></span>
						       <div class="unicour_lst">	
						          <c:if test="${not empty suggestedComparison.collegeDisplayName}">
						            <c:set var="collegeName" value="${suggestedComparison.collegeDisplayName}"/>
                    <%
                      String collegeNames = pageContext.getAttribute("collegeName").toString();
                      gaCollegeName = common.replaceSpecialCharacter(collegeNames);
                      if(collegeNames.length() > 81 && (!(isMobileFlag))){
                    	  collegeNames  = collegeNames.substring(0, 79)+"...";
                      }
                    %>
                    <div class="dpar uni_lst">
							               <p class="par"><a href="${suggestedComparison.uniHomeURL}" title="${suggestedComparison.collegeDisplayName}"><%=collegeNames%></a></p>
							             </div>
                  </c:if>
                  <c:if test="${not empty suggestedComparison.courseTitle}">
                    <c:set var="courseName" value="${suggestedComparison.courseTitle}"/>
                    <%
                     String courseNames = pageContext.getAttribute("courseName").toString();
                     if(courseNames.length() > 69 && (!(isMobileFlag))){
                    	 courseNames = courseNames.substring(0, 67)+"...";
                    }%>
                    <div class="dpar cour_lst">
                      <p class="par"><a href="${suggestedComparison.courseUrl}" title="${suggestedComparison.courseTitle}"> <%=courseNames%></a></p>
                    </div>
                  </c:if>
						          </div>	
							         <%if("COLLEGE".equalsIgnoreCase(assocTypeCode)){%>
                  <div class="f5button f5_add_btn" id="basket_div_${suggestedComparison.collegeId}">
							             <a class="btn4 add" href="javaScript:void(0);" onclick='addBasket("${suggestedComparison.collegeId}", "C", this,"basket_div_${suggestedComparison.collegeId}", "basket_pop_div_${suggestedComparison.collegeId}","Y", "", "<%=gaCollegeName%>");'>
                      <div class="cer1"><span class="fa fa-heart"></span>COMPARE</div>
                    </a>
                  </div>
                  <%}else{%>
                    <div class="f5button f5_add_btn" id="basket_div_${suggestedComparison.courseId}">
                      <a class="btn4 add" href="javaScript:void(0);" onclick='addBasket("${suggestedComparison.courseId}", "O", this,"basket_div_${suggestedComparison.courseId}", "basket_pop_div_${suggestedComparison.courseId}","Y", "", "<%=gaCollegeName%>");'>
                        <div class="cer1"><span class="fa fa-heart"></span>COMPARE</div>
                      </a>
                  </div>
                <%}%>
                <%if("COLLEGE".equalsIgnoreCase(assocTypeCode)){%>
                  <div class="f5button f5_add_btn" id="basket_pop_div_${suggestedComparison.collegeId}" style="display:none;">
							             <a class="btn4 add act" href="javaScript:void(0);" onclick='addBasket("${suggestedComparison.collegeId}", "C", this,"basket_div_${suggestedComparison.collegeId}", "basket_pop_div_${suggestedComparison.collegeId}","Y");'>
                      <div class="cer1"><span class="fa fa-heart hrt_act"></span>COMPARE</div>
                    </a>
                  </div>
                <%}else{%>
                  <div class="f5button f5_add_btn" id="basket_pop_div_${suggestedComparison.courseId}" style="display:none;">
                    <a class="btn4 add act" href="javaScript:void(0);" onclick='addBasket("${suggestedComparison.courseId}", "O", this,"basket_div_${suggestedComparison.courseId}", "basket_pop_div_${suggestedComparison.courseId}","Y");'>
                      <div class="cer1"><span class="fa fa-heart hrt_act"></span>COMPARE</div>
                    </a>
                  </div>
                <%}%>        
              </div>
            </div>
          </div>
        </div>
      </li>
    </c:forEach>
  </ul>
</div>
<div class="f5_vwmre" id="<%=slideId%>_more" style="display:none;"><a href="javaScript:void(0);" onclick="viewMoreLess('more', '<%=slideId%>');"><span>+</span> view more</a></div>
<div class="f5_vwmre" id="<%=slideId%>_less" style="display:none;"><a href="javaScript:void(0);" onclick="viewMoreLess('less', '<%=slideId%>');"><span>-</span> view less</a></div>
</c:if>