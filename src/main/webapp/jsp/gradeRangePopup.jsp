<!-- popup starts below -->
<%
  String flag = request.getParameter("flag");
  String message = request.getParameter("message");
  String popMsg = flag;
  String h1Tag = "Woah, steady on there!";
  String closeButton = "Change Grades";
  String contentHeadClass = "line line1 m12 line2";
  boolean shwBgClr = true;
  String bgClass = ""; 
  if("A-LEVEL".equalsIgnoreCase(flag)){
    popMsg = "Woah, steady on there! You can only enter a maximum of six grades (and if you really are enough of a genius to be doing more than six, just enter your top ones).";
  } else if("SQA Highers".equalsIgnoreCase(flag)){
    popMsg = "Woah, steady on there! You can only enter a maximum of ten grades (and if you really are enough of a genius to be doing more than ten, just enter your top ones).";
  }else if("UCAS TARRIF".equalsIgnoreCase(flag)){
    popMsg = "Woah, steady on there! You can only enter a maximum of 999 tariff points.";
  }else if("BTEC".equalsIgnoreCase(flag)){ //28_Oct_2014 added validation message for BTEC by Thiyagu G.
    popMsg = "Woah, steady on there! You can only enter a maximum of three grades (and if you really are enough of a genius to be doing more than three, just enter your top ones).";
  }else if("BACC".equalsIgnoreCase(flag)){
    popMsg = "Whoah, Steady there you can only enter a maximum score of 99.";
  }else if("COMAPRE-BASKET".equalsIgnoreCase(flag)){
    popMsg = "Sorry to break it to you, but you can't compare more than 12 unis and courses at once. Please remove any currently saved items so you can compare more!";
    h1Tag = "WOAH, THERE!";
    closeButton = "OK";
    shwBgClr = false;
  }else if("OPENDAY-SUCESS-ALERT".equalsIgnoreCase(flag)){
    contentHeadClass = "line line1 m12";
    String[] messageArray = message.split("##SPLIT##");
    h1Tag = "OPEN DAY ADDED TO CALENDAR";
    popMsg = "<strong style=\"color:#0099CC;\"  >"+messageArray[0] +"</strong><br>";
    if(!"NODESC".equalsIgnoreCase(messageArray[2])){
      popMsg = popMsg + messageArray[2] + " : " + messageArray[1];
    }else{
      popMsg = popMsg +  messageArray[1];
    }
    
    closeButton = "View calendar";
  }else if("OPENDAY-EXIST-ALERT".equalsIgnoreCase(flag)){
    h1Tag = "OPEN DAY";
    popMsg = "This opendays already added to your MyWhatuni calendar";
    closeButton = "Close";
  }
  if(shwBgClr){
    bgClass = "wkbs";
  }else{
     bgClass = ""; 
  }
%>
<div id="fpcs">
  <div class="finc">	 
    <a class="cls" onclick="hm();"></a>
    <div class="<%=contentHeadClass%>">
      <%if("OPENDAY-SUCESS-ALERT".equalsIgnoreCase(flag)){%>
        <span class="tck"></span>
      <%}%>
      <h1 class="htxt f22"><%=h1Tag%></h1>      
    </div>    
    <div class="txtc m21 w250" style="width:385px;">
      <span class="<%=bgClass%>"><%=popMsg%></span>  
    </div>
    <%if("OPENDAY-SUCESS-ALERT".equalsIgnoreCase(flag)){%>
      <span class="log-btn1">
        <a class="ok1 m28 mb0" href="/degrees/mywhatuni.html?tabname=OD"><%=closeButton%></a>
      </span>  
    <%}else{%>
      <a class="ok1 m28 mb0" onclick="hm();"><%=closeButton%></a>
    <%}%>
  </div>
</div>
<!-- popup ends above -->