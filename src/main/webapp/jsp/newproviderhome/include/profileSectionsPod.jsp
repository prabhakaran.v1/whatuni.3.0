<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>

<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator"%> 

<%String toolTip = new CommonFunction().getDomainName(request, "N");
  String overviewImgPath = "", myhcProfileId = "", profileStyleSec = "", ProfSecLen = "", newProviderType = "";  
  String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");   
  String collegeId = (String) request.getAttribute("interactionCollegeId");
  String overviewSectionProfileId = null;
  if(request.getAttribute("newProviderType")!=null){
    newProviderType = (String)request.getAttribute("newProviderType");
  }  
  String profileType = request.getAttribute("setProfileType") != null ? (String)request.getAttribute("setProfileType") : "";//Added by Indumathi.S For Unique content in institution profile
  String ugContentFlag = request.getAttribute("ugContentFlag") != null ? (String)request.getAttribute("ugContentFlag") : "";
  String clearingTab = request.getAttribute("highlightTab") != null ? (String)request.getAttribute("highlightTab") : "";
  String loadingImg = CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif", 1);
  String onePxImg = CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1);
%>

<c:if test="${not empty requestScope.collegeOverViewList}">
<section class="clr_prof_ptb1">
  <article class="content-bg">
    <section class="modules clr_modules">
      <div class="borderbot fl mb0"></div>
      <div id="div_module_info"></div>
      <c:forEach var="profileSection" items="${requestScope.collegeOverViewList}" varStatus="index">
      <c:set var="indexValue" value="${index.index}"/>  
        <%if(Integer.parseInt(pageContext.getAttribute("indexValue").toString())==0){%>
          <c:if test="${not empty profileSection.profileImagePath}">
          <c:set var="overviewImg" value="${profileSection.profileImagePath}"/>
          <c:set var="myhcProfId" value="${profileSection.myhcProfileId}"/>
          <%
            overviewImgPath = (String)pageContext.getAttribute("overviewImg");
            myhcProfileId = (String)pageContext.getAttribute("myhcProfId");               
          %>  
          </c:if>
        <%}else{            
            profileStyleSec = "display:none;";            
        }%>                     
        <div class="modules_toggle cf bor_btm" id="profSecPos_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>">  <!--Added GA event track Indumathi.S July-26-16-->      
          <c:set var="collegeNameDisplay" value="${profileSection.collegeNameDisplay}"/>
          <input type="hidden" name="collegeNameHidden" id="collegeNameHidden" value="<%=(String)pageContext.getAttribute("collegeNameDisplay")%>"/>
          <c:set var="selectedSectionName" value="${profileSection.selectedSectionName}"/>
          <% String selectedSectionName = pageContext.getAttribute("selectedSectionName").toString();
             selectedSectionName = selectedSectionName.replaceAll("'","");
           if("Students Union".equalsIgnoreCase((String)pageContext.getAttribute("selectedSectionName"))){
        	   selectedSectionName = "Student Union";
           }
          %>
	  <%-- ::START:: wu582_20181023 - Sabapathi: get the section name for on page load profile logging  --%>
          <c:if test="${not empty profileSection.profileId}">
            <c:set var="profileSectionIdForLogging" value="${profileSection.profileId}"/>
            <%if(Integer.parseInt(pageContext.getAttribute("indexValue").toString()) == 0) {
              overviewSectionProfileId = (String)pageContext.getAttribute("profileSectionIdForLogging");
            }%>
          </c:if>
	  <%-- ::END:: wu582_20181023 - Sabapathi: get the section name for on page load profile logging  --%>
          <h2 class="fl fnt_lrg lh_24 link_blue" 
              onclick="viewProfileSecCnt('<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>','${profileSection.profileId}','<%=newProviderType%>','${profileSection.myhcProfileId}');GAEventTrackingNormalIP('Webclick', 'Profile Content','<%=selectedSectionName%>','profSecId_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>');">
            ${profileSection.selectedSectionName}
            <%if(Integer.parseInt(pageContext.getAttribute("indexValue").toString())==0){%>
              <i class="fa fa-minus-circle fnt_24 color1 fr mt6" id="profSecId_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>"></i>
            <%}else{%>
              <i class="fa fa-plus-circle fnt_24 color1 fr mt6"  id="profSecId_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>"></i>
            <%}%>            
          </h2>          
          <c:if test="${not empty profileSection.profileDescription}">
            <div class="clr_box_cnr" style="<%=profileStyleSec%>" id="profSecCnt_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>">
            <c:if test="${not empty profileSection.profileImagePath}">
              <div class="clr_drdn_vd">                                    
                <%if(Integer.parseInt(pageContext.getAttribute("indexValue").toString())==0){%>              
                  <c:if test="${not empty profileSection.profileImagePath}">    
                    <c:if test="${profileSection.mediaType eq 'Image'}">
                      <img src="${profileSection.profileImagePath}" alt="${profileSection.selectedSectionName}"/>
                    </c:if>
                    <c:if test="${profileSection.mediaType eq 'Video'}">
                      <%request.setAttribute("showlightbox","YES");%>
                      <img src="${profileSection.profileImagePath}" alt="${profileSection.selectedSectionName}"/>                                        
                      <span id="provie<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>" class="provie plyIcon" onclick="javascript:return showVideo('${profileSection.videoId}','${profileSection.collegeId}','B', '<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>','${profileSection.subOrderItemId}');"><i class="fa fa-play-circle-o"></i></span>
                      <div class="prf_ldicon" id="loadIcon<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>" style="display: none;"><img  class="lazy-load" src="<%=onePxImg%>" data-src="<%=loadingImg%>"></div>
                    </c:if>
                  </c:if>              
                <%}else{%>                                          
                  <c:if test="${not empty profileSection.profileImagePath}">                  
                    <c:if test="${profileSection.mediaType eq 'Image'}">
                      <img class="lazy-load" src="<%=onePxImg%>" data-src="${profileSection.profileImagePath}" alt="${profileSection.selectedSectionName}"/>                    
                    </c:if>                  
                    <c:if test="${profileSection.mediaType eq 'Video'}">
                      <%request.setAttribute("showlightbox","YES");%>
                      <img class="lazy-load" src="<%=onePxImg%>" data-src="${profileSection.profileImagePath}" alt="${profileSection.selectedSectionName}"/>                    
                      <span id="provie<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>" class="provie plyIcon" onclick="javascript:return showVideo('${profileSection.videoId}','${profileSection.collegeId}','B', '<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>','${profileSection.subOrderItemId}');"><i class="fa fa-play-circle-o" style="display:none;" id="vidPlayId_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>"></i></span>                    
                      <div class="prf_ldicon" id="loadIcon<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>" style="display: none;"><img  class="lazy-load" src="<%=onePxImg%>" data-src="<%=loadingImg%>"></div>
                    </c:if>
                  </c:if>                                                                                                  
                  <c:if test="${empty profileSection.profileImagePath}">
                    <img class="lazy-load" src="<%=onePxImg%>" data-src="<%=overviewImgPath%>" alt="${profileSection.selectedSectionName}"/>
                  </c:if>                                                                                                          
                <%}%>
              </div> 
            </c:if>
            
              <div class="clr_drdn_cnt">
                <!--Added by Indumathi.S For Unique content in institution profile May_31_2016-->
                <%if(!"SUB_PROFILE".equalsIgnoreCase(profileType) && !"CLEARING".equalsIgnoreCase(clearingTab)){
                  String curYear2 = new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS");
                  pageContext.setAttribute("curYear2", curYear2);
                %>
                  <c:if test="${not empty profileSection.profileRating}">
                    <c:set var="profileRoundRating" value="${profileSection.profileRoundRating}"/>
                    <c:if test="${not empty profileSection.selectedSectionName}">
                      <c:set var="sectionNameDisplay" value="${profileSection.selectedSectionName}"/>                  
                      <% String toolTipSectionName = "University of the year";
                      String sectionNameDisplay = (String)pageContext.getAttribute("sectionNameDisplay");
                       if(sectionNameDisplay.contains("Overview")) {%>
                          <p>Overall rating</p>
                        <%} else {%>
                          <p><%=sectionNameDisplay%> rating</p>
                        <%toolTipSectionName = sectionNameDisplay;
                        pageContext.setAttribute("toolTipSectionName", toolTipSectionName);
                        }%>
                      <div class="rate_trophy" onclick="showAndHideToolTip('wuscaToolTip<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>');" onmouseover="showToolTip('wuscaToolTip<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>');" onmouseout="hideToolTip('wuscaToolTip<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>');">
                        <div class="rp_ttip_ctrl">
                        <span class="mt5 rt_txt">
                        <%String wuscaClass = "";
                        for(int i=1; i<=5; i++) {
                          wuscaClass = (i <= Integer.parseInt(pageContext.getAttribute("profileRoundRating").toString())) ? "fa fa-trophy" : "fa fa-trophy trop_gry";%>
                          <i class="<%=wuscaClass%>"></i>
                        <%}%>                       
                        </span>
                        <div class="cmp hdf5" style="display:none" id="wuscaToolTip<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())+1%>">This is this uni's WUSCA ${curYear2} ranking for the ${toolTipSectionName} category. See the full league table <a href="/student-awards-winners/university-of-the-year/">here</a></div>
                        (${profileSection.profileRating})&nbsp; 
                        </div>
                      </div>
                     </c:if>  
                  </c:if>
                <%}%>
                <!--End May_31_2016-->
                ${profileSection.profileDescription}   
              </div>
            </div>             
          </c:if>            
        </div>  
        <input type="hidden" id="secTypeId_<%=Integer.parseInt(pageContext.getAttribute("indexValue").toString())%>" value="${profileSection.selectedSectionName}"/>
      </c:forEach>
      <input type="hidden" id="newProviderTypeId" value="<%=newProviderType%>"/>
    </section>
  </article>
</section>
<c:set var="profileSecLen" value="${fn:length(requestScope.collegeOverViewList)}"/>        
<%ProfSecLen=pageContext.getAttribute("profileSecLen").toString();%>
<input type="hidden" id="ProfSecLen" value="<%=ProfSecLen%>"/>
<input type="hidden" id="myhcProfileId" value="<%=myhcProfileId%>"/>
<input type="hidden" id="collegeId" value="<%=collegeId%>"/>
<input type="hidden" name="cpeQualNetworkId" id="cpeQualNetworkId" value="<%=cpeQualificationNetworkId%>">
</c:if>
<%-- ::START:: wu582_20181023 - Sabapathi: added script for stats logging on page load  --%>
<%if(!GenericValidator.isBlankOrNull(overviewSectionProfileId)) {%>
  <script type="text/javascript">
    var $ = jQuery.noConflict();
    $(document).ready(function() {
      profileSecStatsLogging('<%=overviewSectionProfileId%>', '<%=newProviderType%>');
    });
  </script>
<%}%>
<%-- ::END:: wu582_20181023 - Sabapathi: added script for stats logging on page load  --%>
<%if(("Y").equalsIgnoreCase(ugContentFlag)) {%>
<input type="hidden" id="ugContentFlag" value="<%=ugContentFlag%>"/>
<%}%>