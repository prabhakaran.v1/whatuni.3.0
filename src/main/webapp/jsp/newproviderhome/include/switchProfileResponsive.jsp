<%@ page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction"%>

<%
  String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");  
  String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
  String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
  String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
%>  
<script type="text/javascript" language="javascript">
  
  var dev = jQuery.noConflict();
  dev(document).ready(function(){
    adjustStyle();
    switchProfileToResponsive();
  });
  adjustStyle();
  
  function jqueryWidth() {
    return dev(this).width();
  } 
  
  function adjustStyle() {
    var width = document.documentElement.clientWidth;
    var path = ""; 
    if(width <= 480) {    
      if(dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }        
      <%if(("LIVE").equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
      <%}%>           
    }else if((width > 480) && (width <= 992)) {      
      if(dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }       
      <%if(("LIVE").equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}%>     
    }else{
      if(dev("#viewport").length > 0) {
        dev("#viewport").remove();
      }
      document.getElementById('size-stylesheet').href = ""; 
      dev(".hm_srchbx").hide();
    }
  }
  dev(window).on('orientationchange', orientationChangeHandler);
  
function orientationChangeHandler(e) {
    setTimeout(function() {
        dev(window).trigger('resize');
    }, 500);
}
dev(window).resize(function() {
  switchProfileToResponsive(); 
  adjustStyle();
  var width = document.documentElement.clientWidth;
  if (!dev("#addTofnchTickerList").is(":visible")){
   if(dev("#cookie-lightbox") && !dev("#cookie-lightbox").is(":visible")){
    stickyButton(); 
   }
  }
  if(width > 992){dev(".btn_intr .more_info").removeAttr("style");}
});


function switchProfileToResponsive() {
  var screenWidth = jqueryWidth();
  if((screenWidth > 480) && (screenWidth <= 992)) { //Tablet || Mobile view   
    dev("#stickyTop").removeAttr("style").removeClass("sticky_top_nav");    
    if(document.getElementById("changeAwardStyle")){    
      if(document.getElementById("changeAwardStyle").value=="true"){
        var lftClassName = dev('#awdLftCnt').attr('class');
        var rhtClassName = dev('#awdRhtCnt').attr('class');
        dev("#awdLftCnt").removeClass(lftClassName).addClass("lft_cnr st_awd_tv");
        dev("#awdRhtCnt").removeClass(rhtClassName).addClass("rht_cnr st_awd_cnr");
      }
    }
  }else{
    if(screenWidth <= 480){  //Mobile view            
    }else{ //Desktop view   
      var windowTop = dev(window).scrollTop(); 
      if(150 < windowTop){
        dev('#stickyTop').addClass('sticky_top_nav');        
        dev('#stickyTop').css({ position: 'fixed', top: 0 });            
      }else{
        dev('#stickyTop').removeClass('sticky_top_nav');
        dev('#stickyTop').css('position','static');         
      }       
    }    
    if(document.getElementById("changeAwardStyle")){    
      if(document.getElementById("changeAwardStyle").value=="true"){
        var lftClassName = dev('#awdLftCnt').attr('class');
        var rhtClassName = dev('#awdRhtCnt').attr('class');
        dev("#awdLftCnt").removeClass(lftClassName).addClass("lft_cnr");
        dev("#awdRhtCnt").removeClass(rhtClassName).addClass("rht_cnr");
      }
    }
  }
}

//Added by Indumathi.S for Sticky button Jan-27-16
dev(document).ready(function(){
  dev(".btn_intr .more_info").click(function(){
   dev(".btn_cntrl .btns_interaction").removeClass("btn_vis");
   dev(".btn_cntrl .more_info").css("display","none");
   if(checkChildDiv("buttonCls")){
    dev("#socialBoxMin").addClass("intr_sb_pos1"); 
    dev("#needHelpDiv").addClass("intr_sb_pos1"); // For chatbot sticky 25-09-2018
   }
  });
  dev(".int_cl").click(function(){
   dev('.btn_intr').addClass('btn_cntrl');
   dev(".btn_intr .btns_interaction").addClass("btn_vis");
   dev(".btn_cntrl .more_info").css("display","block");
   dev("#socialBoxMin").removeClass("intr_sb_pos1"); 
   dev("#needHelpDiv").removeClass("intr_sb_pos1"); // For chatbot sticky 25-09-2018
  });
  dev(".f5_clse_btn").click(function(){
    stickyButton();
  }); 
  dev(".cook_cls").click(function(){
   stickyButton();   
  });
  if (!dev("#addTofnchTickerList").is(":visible")){
   if(dev("#cookie-lightbox") && !dev("#cookie-lightbox").is(":visible")){
    stickyButton(); 
   }
  } 
});
function stickyButton() {
 var width = document.documentElement.clientWidth;
 var divPosition;
 if ((width >= 320) && (width <= 567)) {
   divPosition = 279;
 }
 if ((width >= 568) && (width <= 992)) {
   divPosition = 200; 
 }
 if ((width >= 320) && (width <= 992)) {
  dev(window).scroll(function(){  
   width = document.documentElement.clientWidth;
   if (dev(this).scrollTop() > divPosition) {  
    var windowTop = dev(window).scrollTop(); 
    dev('.btn_intr').addClass('btn_cntrl');
    dev(".btn_intr .more_info").css("display","block");
    dev(".btn_intr .btns_interaction").addClass("btn_vis");
    dev("#socialBoxMin").addClass("intr_sb_pos");
    dev("#socialBoxMin").removeClass("intr_sb_pos1");
    // For chatbot sticky 25-09-2018
    dev("#needHelpDiv").addClass("intr_sb_pos");
    dev("#needHelpDiv").removeClass("intr_sb_pos1");
    if(width > 992){
     dev(".btn_intr .more_info").removeAttr("style");
     dev(".btn_intr .more_info").css("display","none");
    }
   }else {
    dev('.btn_intr').removeClass('btn_cntrl');
    dev(".btn_intr .more_info").removeAttr("style");
    dev(".btn_intr .more_info").css("display","none");
    dev(".btn_intr .btns_interaction").addClass("btn_vis");
    dev("#socialBoxMin").removeClass("intr_sb_pos");
    dev("#socialBoxMin").removeClass("intr_sb_pos1");
    // For chatbot sticky 25-09-2018
    dev("#needHelpDiv").removeClass("intr_sb_pos");
    dev("#needHelpDiv").removeClass("intr_sb_pos1");
   }
  });
 }
}
function jqueryWidth() {
    return dev(this).width();
}
</script>
