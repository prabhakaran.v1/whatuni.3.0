<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tlds/html.tld" prefix="html"%>

<%@page import="WUI.utilities.CommonUtil,WUI.utilities.GlobalConstants,WUI.utilities.CommonFunction,org.apache.commons.validator.GenericValidator, com.wuni.util.seo.SeoUrls"%>

<%
  CommonFunction common = new CommonFunction();
  String collegeId          = (String) request.getAttribute("interactionCollegeId");
  String collegeName        = (String) request.getAttribute("interactionCollegeName");
  String collegeNameDisplay = (String) request.getAttribute("interactionCollegeNameDisplay");
  String gaCollegeName      = common.replaceSpecialCharacter(collegeName);
  String clearingyear       = GlobalConstants.CLEARING_YEAR;
  String clearingonoff      = common.getWUSysVarValue("CLEARING_ON_OFF");
  String clearingprovUrl    = "";
  String stickyTopId        = "stickyTop";
  String ugTabStyleClass    = "op_loct";
  String highTab            = ""; 
  String showBanner         = "";
  String nonAdvFlag         = "false";
  boolean iteractionBtnExist = true;
  String clrCourseFlag      = request.getAttribute("CLEARING_COURSE_EXIST_FLAG") != null ? (String)request.getAttribute("CLEARING_COURSE_EXIST_FLAG") : "";
  if(request.getAttribute("clearingprovUrl")!=null && !"".equals(request.getAttribute("clearingprovUrl"))){
    clearingprovUrl = (String)request.getAttribute("clearingprovUrl");
  }
  if(request.getAttribute("interactionExist")!=null && "false".equals(request.getAttribute("interactionExist"))){
    stickyTopId = "stickyTopId1";
    iteractionBtnExist = false;
  }
  if(iteractionBtnExist || "Y".equalsIgnoreCase(clrCourseFlag)){
    stickyTopId = "stickyTop";
  }
  if(request.getAttribute("highlightTab")!=null && !"".equals(request.getAttribute("highlightTab"))){
    highTab = (String)request.getAttribute("highlightTab");    
  } 
  if(!GenericValidator.isBlankOrNull(clearingonoff) && "ON".equals(clearingonoff)){  
    ugTabStyleClass = "op_loct";
  }
  if(request.getAttribute("nonAdvertFlag")!=null && "true".equals((String)request.getAttribute("nonAdvertFlag"))){    
    showBanner = "ad_cnr";
    nonAdvFlag = (String)request.getAttribute("nonAdvertFlag");
  }
  String clearingURL = request.getAttribute("courseUrl") != null ? (String)request.getAttribute("courseUrl") : "";
  String collegeName1 = common.replaceURL(common.getCollegeName(collegeId, request)).toLowerCase();
  clearingURL = new SeoUrls().constructCourseUrl("all", "CLEARING", collegeName1); //Forming all clearing courses url for clearing page by Prabha on 11_Jul_17
  String tabFlag = request.getAttribute("tabFlag") != null ? (String)request.getAttribute("tabFlag") : "false";
  String profileType = !GenericValidator.isBlankOrNull((String)request.getAttribute("setProfileType")) ? (String)request.getAttribute("setProfileType") : "";
  String userJourney = !GenericValidator.isBlankOrNull((String)request.getAttribute("USER_JOURNEY_FLAG")) ? (String)request.getAttribute("USER_JOURNEY_FLAG") : ""; //Added for cmmt
%>

<section id="heroslider" class="heroSlider p0">
<div id="<%=stickyTopId%>" class="">
  <div class="course_deatils hrbgi">
    <div class="<%=showBanner%>">
    <div class="content-bg">
      <jsp:include page="/seopods/breadCrumbs.jsp">
        <jsp:param name="reviewProfilePage" value="st_fix pros_brcumb"/>
      </jsp:include><%--Added breadcrumb Indumathi.S Feb-16-2016--%>
      <div class="clr_lft_cnr prof_tab">                
        <div id="fixedscrolltop" class="clr_univ_wrap">
          <c:if test="${not empty requestScope.uniInfoList}">
            <c:forEach var="uniReviewsInfo" items="${requestScope.uniInfoList}">                                              
              <c:if test="${not empty requestScope.clearingSwitchFlag}">
                <c:if test="${requestScope.clearingSwitchFlag eq 'ON'}">              
                  <c:if test="${not empty requestScope.highlightTab}">                
                    <c:if test="${requestScope.highlightTab eq 'CLEARING'}">  
                                                   
                    </c:if>
                  </c:if>
                </c:if>
              </c:if>
              <h1 class="cf">${uniReviewsInfo.collegeDisplayName}</h1>                
              <c:if test="${not empty requestScope.spdeptName}">
                <h2>${requestScope.spdeptName}</h2>
              </c:if>
              <c:if test="${not empty uniReviewsInfo.reviewCount}">                                       
                <c:if test="${not empty uniReviewsInfo.overAllRating}">
                  <c:set var="rating" value="${uniReviewsInfo.overAllRating}"/>
                    <c:if test="${uniReviewsInfo.reviewCount ne 0}">
                      <h3 class="fnt_lrg fnt_14 lh_24">Student rating <span class="ml5 rat<%=(String)pageContext.getAttribute("rating")%> t_tip ttip_pctrl">
											 <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
											  <span class="cmp">
                          <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
											    <div class="line"></div>
												</span>
												<%--End of tool tip code--%>
											</span> (${uniReviewsInfo.overallRatingExact})                      
                      
                      <c:if test="${not empty uniReviewsInfo.reviewCount}">
                        <c:if test="${uniReviewsInfo.reviewCount gt 0}">
                          <c:if test="${not empty requestScope.reviewSubjectList}">
                            <a class="link_blue nowc vr_bold" id="rvw_skip_link" data-rvw-ga="${uniReviewsInfo.collegeDisplayName}" title="Reviews at ${uniReviewsInfo.collegeDisplayName}"><spring:message code="view.review.link.name"/></a>
                          </c:if>
                        </c:if>
                      </c:if>                                           
                    </c:if>
                  </h3>
                </c:if>                
              </c:if>                        
          <%if("CLEARING".equals(highTab)){%>
            <%--<p class="cal_txt">(Calls free from UK landlines or standard network rate from mobile)</p>--%>
          <%}%>
          <%if(!("clearing".equalsIgnoreCase(userJourney) && "NORMAL".equalsIgnoreCase(profileType))){%>
          <c:if test="${not empty requestScope.COLLEGE_IN_BASKET}">
            <%--Added college name for add comparision insights log by Prabha on 29_Mar_2016--%>
            <c:if test="${not empty uniReviewsInfo.collegeDisplayName}">
              <c:set var="collegeNameDef" value="${uniReviewsInfo.collegeDisplayName}"/>
              <%String basketCollegeName = new CommonFunction().replaceSpecialCharacter((String)pageContext.getAttribute("collegeNameDef"));%>
            <jsp:include page="/jsp/advertiser/ip/include/headShortlist.jsp">
              <jsp:param name="richProfile" value="true"/>
                <jsp:param name="basketCollegeName" value="<%=basketCollegeName%>"/>
            </jsp:include>    
          </c:if>               
            <%--End of insights log--%>
          </c:if>
          <%}%>
          </c:forEach>
           </c:if>
          <c:if test="${not empty requestScope.clearingHeader and requestScope.clearingHeader eq 'FALSE'}">
              <div class="fr mtmin">
                <c:if test="${not empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
                  <div class="fr mt5 mb20 btn_intr btn_othuni"><!--Modified code for sticky buttons Jan-27-16 Rel Indumathi.S-->
                    <div class="more_info">
                      <a>More Info</a>
                    </div>
                    <div class="btns_interaction fl" id="buttonCls">
                      <div class="int_cl">
                        <a class="" onclick="hm()"><i class="fa fa-times"></i></a>
                      </div> 
                      <jsp:include page="/advertpromotion/interactionbuttons/websiteProspectusFromBeanAndHrefCdPage.jsp" >
                        <jsp:param name="FLOAT_STYLE" value="LEFT" />  
                        <jsp:param name="FROMPAGE" value="SHOWNONREQINFO" />
                        <jsp:param name="NONADVFLAG" value="<%=nonAdvFlag%>" />  
                        <jsp:param name="OTHERUNIFLAG" value="true"/>
                      </jsp:include> 
                    </div>  
                    <!--End-->
                  </div>
                </c:if>    
              </div>
          </c:if>
        </div>
       <c:if test="${not empty requestScope.clearingHeader}">
          <c:if test="${requestScope.clearingHeader eq 'TRUE'}">        
            <div class="clr_rht_cnr">                          
              <!--<div class="clr_stamp fr">
                <img alt="Whatuni <%=clearingyear%> clearing logo" title="<%=clearingyear%> University Clearing Courses" src="<%=CommonUtil.getImgPath("/wu-cont/images/clr_stamp.png", 0)%>">
              </div>  -->     
              <%if(iteractionBtnExist || "Y".equalsIgnoreCase(clrCourseFlag)){%>
              <div class="fr mtmin btn_intr">
                <div class="more_info">
                    <a>Apply</a>
                  </div>
                <div class="btns_interaction fr clr_btn" id="buttonCls">
                  <div class="int_cl">
                    <a class="" onclick="hm()"><i class="fa fa-times"></i></a>
                  </div>
                  <div class="">
                    <c:if test="${not empty requestScope.CLEARING_COURSE_EXIST_FLAG}">
                      <c:if test="${requestScope.CLEARING_COURSE_EXIST_FLAG eq 'Y'}">
                        <a class="clr-view-all-courses" href="<%=clearingURL%>" title="CLEARING COURSES">CLEARING COURSES</a>
                      </c:if>
                    </c:if>     
              <c:if test="${not empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
                <c:forEach var="interationButtons" items="${requestScope.listOfCollegeInteractionDetailsWithMedia}">
                  <c:if test="${not empty interationButtons.subOrderItemId}">
                    <c:if test="${interationButtons.subOrderItemId gt 0}">
                      <c:set var="subOrderItemId" value="${interationButtons.subOrderItemId}" />
                      <%
                        String  websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice((String)pageContext.getAttribute("subOrderItemId"), "website_price");
                        request.setAttribute("isClearingProfile", "YES");
                      %>
                            <c:if test="${not empty interationButtons.subOrderWebsite}">
                              <a rel="nofollow" target="_blank" class="visit-web mr10" onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=websitePrice%>); cpeWebClickClearing(this,'<%=collegeId%>','${interationButtons.subOrderItemId}','${interationButtons.cpeQualificationNetworkId}','${interationButtons.subOrderWebsite}');" href="${interationButtons.subOrderWebsite}" title="Visit <%=collegeNameDisplay%> website">Visit website
                              </a>
                            </c:if>                      
                            <c:if test="${not empty interationButtons.hotLineNo}">                          
                               <a id="hotlineTextId" title="Call now <%=collegeNameDisplay%>" class="clr-call" onclick="callHotline('${interationButtons.hotLineNo}','hotlineTextId');; GAInteractionEventTracking('Webclick', 'interaction', 'hotline', '<%=gaCollegeName%>');cpeHotlineClearing(this,'<%=collegeId%>','${interationButtons.subOrderItemId}','${interationButtons.cpeQualificationNetworkId}','${interationButtons.hotLineNo}');">
                                <i class="fa fa-phone"></i> CALL NOW
                              </a>
                            </c:if>
                    </c:if>
                  </c:if>
                </c:forEach>
              </c:if>
              </div>  
              </div>
             </div>
             <%}%>
            </div>
          </c:if>                    
      </c:if>
      </div>                                  
    </div>
    </div>
  </div>
</div>
 <!--Added Hero image by Indumathi.S for Apr-19-16-->
<c:if test="${not empty requestScope.clearingSwitchFlag}">
  <c:if test="${requestScope.clearingSwitchFlag eq 'ON'}">
      <c:if test="${not empty requestScope.profileImgPat}">
          <div class="hero_cnr">
          <div id="h_image5" class="clr_hero_img" style="background:url('${requestScope.profileImgPath}') no-repeat center center;background-size:cover !important; height:508px;"></div>
          <input type="hidden" id="heroImgPath" value="${requestScope.profileImgPath}"/>
            <c:if test="${not empty requestScope.CLEARING_COURSE_EXIST_FLAG}">
              <c:if test="${requestScope.CLEARING_COURSE_EXIST_FLAG eq 'Y'}">
                <div class="content-bg">
                  <a class="rp btn1 btn3 w210" href="<%=clearingURL%>">ALL CLEARING COURSES<i class="fa fa-long-arrow-right"></i></a>
                  <div class="rch_btn_bor"></div>
                </div>
              </c:if>
            </c:if>     
            </div>
      </c:if>
  </c:if>
</c:if>
 <!--End-->
</section>
<%--Added by priyaa for Final 5 - 21-JUL-2015_REL - Start of change--%>
<c:if test="${not empty requestScope.addedToFinalChoice}">
  <c:if test="${requestScope.addedToFinalChoice eq 'N'}">
    <div class="myFnChLbl" style="display:none;">                    
      ${requestScope.interactionCollegeNameDisplay}#${requestScope.interactionCollegeId}
    </div>               
  </c:if>
</c:if>
<%--End of change--%>