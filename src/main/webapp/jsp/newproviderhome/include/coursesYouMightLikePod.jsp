<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn"%>
<%@page import="WUI.utilities.*" autoFlush="true"%>
<%
	String searchType = request.getParameter("searchtype");
	String CONTEXT_PATH = GlobalConstants.WU_CONTEXT_PATH;
	String collegeNameDispaly = (String) request
			.getAttribute("interactionCollegeNameDisplay");
	String courseTitle = (String) request.getAttribute("courseTitle");
	StringBuffer compareallcourses = new StringBuffer();
	boolean compareflag = false;
	String copclassname = "";
	String gaCollegeName = ""; //CollegeName for insights log
	String compareAllCollegeNames = ""; //All college names for insights log
	if ("CLEARING_COURSE".equalsIgnoreCase(searchType)) {
		copclassname = "clr15";
	}
	int beanSize = 0; //Hided Compare All link if only one course/uni displayed in this pod for the bug:39149, on 08_Aug_2017, By Thiyagu G
%>
<c:if test="${not empty requestScope.listOfTopCourses}">
	<c:set var="sizeoflist" value="${fn:length(requestScope.listOfTopCourses)}"/>
	<section class="nonadvert bg_bluelt fulWd grapm1 mt20">
		<div id="div_top_courses"></div>
		<h1 class="fnt_lrg fl">Other universities you might like</h1>
		<div class="borderbot1 fl mt25"></div>
		<c:forEach var="topCourse" items="${requestScope.listOfTopCourses}"	varStatus="i">
		<c:set var="iValue" value="${i.index}"/>
			<c:if test="${not empty topCourse.shortListFlag}">
				<c:set var="college_Id" value="${topCourse.collegeId}"/>
				<c:set var="shortListFlag" value="${topCourse.shortListFlag}"/>
				<c:set var="topCourseSize" value="${fn:length(requestScope.listOfTopCourses)}"/>
				<c:if test="${not empty topCourse.collegeNameDisplay}">
					<c:set var="collegeName" value="${topCourse.collegeNameDisplay}"/>
					<%
						gaCollegeName = new CommonFunction()
												.replaceSpecialCharacter((String)pageContext.getAttribute("collegeName"));
					%>
				</c:if>
				<%
					beanSize = Integer.parseInt(pageContext.getAttribute("topCourseSize").toString());
								compareallcourses.append((String)pageContext.getAttribute("college_Id"));
								compareAllCollegeNames += gaCollegeName; //Added college names for insights log by Prabha on 29_Mar_2016
								if ( Integer.parseInt(pageContext.getAttribute("sizeoflist").toString())- 1 != Integer.parseInt(pageContext.getAttribute("iValue").toString()) ) {
									compareallcourses.append(",");
									compareAllCollegeNames += GlobalConstants.SPLIT_CONSTANT; //##SPLIT##
								}
								if (!"TRUE".equalsIgnoreCase((String)pageContext.getAttribute("shortListFlag"))) {
									compareflag = true;
								}
				%>
			</c:if>
			<div class="mt20 fl fldev">
				<%
					if ("CLEARING_COURSE".equalsIgnoreCase(searchType)) {
				%>
				<input type="hidden" name="comparepage" id="comparepage"
					value="CLEARING" />
				<%
					}
				%>
				<%
					String topClassName = "fl img_ppn img_ppn_116 mr20 noimage";
				%>
				<c:if test="${topCourse.mediaType eq 'PICTURE'}">
					<c:if test="${not empty topCourse.mediaThumbPath}">
						<%
							topClassName = "fl img_ppn img_ppn_116 mr20";
						%>
					</c:if>
				</c:if>
				<c:if test="${topCourse.mediaType eq 'VIDEO'}">
					<c:if test="${not empty topCourse.videoThumbPath}">
						<%
							topClassName = "fl img_ppn img_ppn_116 mr20";
						%>
					</c:if>
				</c:if>
				<div class="<%=topClassName%>">
					<c:if test="${topCourse.mediaType eq 'PICTURE'}">
						<a href="${topCourse.uniHomeURL}">
							<img
							src="${topCourse.mediaThumbPath}"
							alt="${topCourse.collegeNameDisplay}"
							title="${topCourse.collegeNameDisplay}">
						</a>
					</c:if>
					<c:if test="${topCourse.mediaType eq 'VIDEO'}">
						<a href="${topCourse.uniHomeURL}">
							<img
							src='${topCourse.videoThumbPath}'
							alt="${topCourse.collegeNameDisplay}"
							title="${topCourse.collegeNameDisplay}" />
						</a>
					</c:if>
				</div>
				<%if(!"CLEARING PROFILE".equalsIgnoreCase(searchType)){%> 
				<div class="ver_cmp">
					<c:if test="${topCourse.shortListFlag eq 'TRUE'}">
						<div class="cmlst"
							id="basket_div_${topCourse.collegeId}"
							style="display: none;">
							<div class="compare">
								<a
									onclick='addBasket("${topCourse.collegeId}", "C", this,"basket_div_${topCourse.collegeId}", "basket_pop_div_${topCourse.collegeId}", "", "", "<%=gaCollegeName%>");'>
									<span class="icon_cmp f5_hrt"></span> <span class="cmp_txt">Compare</span>
									<span class="loading_icon"
									id="load_${topCourse.collegeId}"
									style="display: none; position: absolute;"></span> <span
									class="chk_cmp"><span class="chktxt">Add to
											comparison<em></em>
									</span></span>
								</a>
							</div>
						</div>
						<div class="cmlst act"
							id="basket_pop_div_${topCourse.collegeId}">
							<div class="compare">
								<a class="act"
									onclick='addBasket("${topCourse.collegeId}", "C", this,"basket_div_${topCourse.collegeId}", "basket_pop_div_${topCourse.collegeId}");'>
									<span class="icon_cmp f5_hrt hrt_act"></span> <span
									class="cmp_txt">Compare</span> <span class="loading_icon"
									id="load1_${topCourse.collegeId}"
									style="display: none; position: absolute;"></span> <span
									class="chk_cmp"><span class="chktxt">Remove from
											comparison<em></em>
									</span></span>
								</a>
							</div>
							<div
								id="defaultpop_${topCourse.collegeId}"
								class="sta" style="display: block;">
								<%
									String userId = new SessionData().getData(request, "y");
												if (userId != null && !"0".equals(userId)
														&& !"".equals(userId)) {
								%>
								<a class="view_more" href="<%=CONTEXT_PATH%>/comparison">View
									comparison</a>
								<%
									} else {
								%>
								<a
									onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');"
									class="view_more">View comparison</a>
								<%
									}
								%>
							</div>
							<div id="pop_${topCourse.collegeId}"
								class="sta" style="display: none;"></div>
						</div>
					</c:if>
					<c:if test="${topCourse.shortListFlag ne 'TRUE'}">
						<div class="cmlst"
							id="basket_div_${topCourse.collegeId}">
							<div class="compare">
								<a
									onclick='addBasket("${topCourse.collegeId}", "C", this,"basket_div_${topCourse.collegeId}", "basket_pop_div_${topCourse.collegeId}", "", "", "<%=gaCollegeName%>");'>
									<span class="icon_cmp f5_hrt"></span> <span class="cmp_txt">Compare</span>
									<span class="loading_icon"
									id="load_${topCourse.collegeId}"
									style="display: none; position: absolute;"></span> <span
									class="chk_cmp"><span class="chktxt">Add to
											comparison<em></em>
									</span></span>
								</a>
							</div>
						</div>
						<div class="cmlst act"
							id="basket_pop_div_${topCourse.collegeId}"
							style="display: none;">
							<div class="compare">
								<a class="act"
									onclick='addBasket("${topCourse.collegeId}", "C", this,"basket_div_${topCourse.collegeId}", "basket_pop_div_${topCourse.collegeId}");'>
									<span class="icon_cmp f5_hrt hrt_act"></span> <span
									class="cmp_txt">Compare</span> <span class="loading_icon"
									id="load1_${topCourse.collegeId}"
									style="display: none; position: absolute;"></span> <span
									class="chk_cmp"><span class="chktxt">Remove from
											comparison<em></em>
									</span></span>
								</a>
							</div>
							<div
								id="defaultpop_${topCourse.collegeId}"
								class="sta" style="display: block;">
								<%
									String userId = new SessionData().getData(request, "y");
												if (userId != null && !"0".equals(userId)
														&& !"".equals(userId)) {
								%>
								<a class="view_more" href="<%=CONTEXT_PATH%>/comparison">View
									comparison</a>
								<%
									} else {
								%>
								<a
									onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');"
									class="view_more">View comparison</a>
								<%
									}
								%>
							</div>
							<div id="pop_${topCourse.collegeId}"
								class="sta"></div>
						</div>
					</c:if>
				</div>
				<%}%>
				<div class="div_right fl mb20">
					<div class="fl">
						<h2>
							<a class="fnt_lbd fnt_18 lh_28 link_blue"
								href="${topCourse.uniHomeURL}"
								title="${topCourse.collegeNameDisplay}">${topCourse.collegeNameDisplay}
							</a>
						</h2>
					</div>
				</div>
				<c:if test="${not empty topCourse.subOrderItemId}">
					<c:if test="${topCourse.subOrderItemId ne 0}">
						<div class="div_three fr <%=copclassname%>">
							<div class="fr mt5 mb20">
								<div class="btns_interaction">
									<%if(!"CLEARING PROFILE".equalsIgnoreCase(searchType)){%>
									<c:if test="${not empty topCourse.subOrderWebsite}">
										<a rel="nofollow" target="_blank" class="fl visit-web mr10"
											onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${topCourse.gaCollegeName}', ${topCourse.websitePrice}); cpeWebClick(this,'${topCourse.collegeId}','${topCourse.subOrderItemId}','${topCourse.cpeQualificationNetworkId}','${topCourse.subOrderWebsite}');var a='s.tl(';"
											href="${topCourse.subOrderWebsite}&courseid=${topCourse.courseId}"
											title="Visit ${topCourse.collegeNameDisplay} website">
											Visit website <i class="fa fa-caret-right"></i>
										</a>
									</c:if>
									<c:if test="${not empty topCourse.subOrderProspectusWebform}">
										<a rel="nofollow" target="_blank" class="last fl get-pros mr0"
											onclick="GAInteractionEventTracking('prospectuswebform', 'interaction', 'Webclick', '${topCourse.gaCollegeName}', ${topCourse.webformPrice}); cpeProspectusWebformClick(this,'${topCourse.collegeId}','${topCourse.subOrderItemId}','${topCourse.cpeQualificationNetworkId}','${topCourse.subOrderProspectusWebform}'); var a='s.tl(';"
											href="${topCourse.subOrderProspectusWebform}"
											title="Get ${topCourse.collegeNameDisplay} Prospectus">
											Get Prospectus <i class="fa fa-caret-right"></i>
										</a>
									</c:if>
									<c:if test="${not empty topCourse.subOrderProspectus and empty topCourse.subOrderProspectusWebform}">
											<a rel="nofollow"
												onclick="GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '${topCourse.gaCollegeName}'); return prospectusRedirect('<%=CONTEXT_PATH%>','${topCourse.collegeId}','${topCourse.courseId}','','','${topCourse.subOrderItemId}');"
												class="last fl get-pros mr0"
												title="Get ${topCourse.collegeNameDisplay} Prospectus">
												Get Prospectus <i class="fa fa-caret-right"></i>
											</a>
									</c:if>
									<%}%>  
									<%--}else{--%>
									<%--
                      <logic:notEmpty name="topCourse" property="subOrderWebsite" >
                        <a rel="nofollow" 
                          target="_blank" 
                          class="fl visit-web mr10" 
                          onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<bean:write name="topCourse" property="gaCollegeName"/>', <bean:write name="topCourse" property="websitePrice" />); cpeWebClickClearing(this,'<bean:write name="topCourse" property="collegeId" />','<bean:write name="topCourse" property="subOrderItemId" />','<bean:write name="topCourse" property="cpeQualificationNetworkId" />','<bean:write name="topCourse" property="subOrderWebsite" />');" 
                          href="<bean:write name="topCourse" property="subOrderWebsite" />" 
                          title="Visit <bean:write name="topCourse" property="collegeNameDisplay"/> website">Visit website <i class="fa fa-caret-right"></i>
                        </a>
                      </logic:notEmpty>
                      <logic:notEmpty name="topCourse" property="hotLineNo">
                        <a class="last fl cbtn mr0" onclick="setTimeout(function(){location.href='tel:<bean:write name="topCourse" property="hotLineNo" />'},1000); GAInteractionEventTracking('visitwebsite', 'interaction', 'hotline', '<bean:write name="topCourse" property="gaCollegeName"/>');cpeHotlineClearing(this,'<bean:write name="topCourse" property="collegeId" />','<bean:write name="topCourse" property="subOrderItemId" />','<bean:write name="topCourse" property="cpeQualificationNetworkId" />','<bean:write name="topCourse" property="hotLineNo" />');" ><i class="fa fa-phone"></i> <bean:write name="topCourse" property="hotLineNo"/></a> 
                      </logic:notEmpty>              
                    --%>
									<%--}--%>
								</div>
							</div>
						</div>
					</c:if>
				</c:if>
			</div>
			<div class="borderbot1 fl mt0"></div>
		</c:forEach>
		<%
			String noneComp = "display:none;";
				if (beanSize > 1) {
					noneComp = "display:block;";
				}
		%>
		<div class="mt20 fl" style="<%=noneComp%>">
			<div class="hor_cmp" id="comapre_all_notdone">
			<%if(!"CLEARING PROFILE".equalsIgnoreCase(searchType)){%> 
				<%
					if (compareflag) {
				%>
				<div class="cmlst" id="compareall">
					<div class="compare">
						<a
							onclick="compareAll('<%=compareallcourses%>','', 'A', '', '<%=compareAllCollegeNames%>');">
							<span class="icon_cmp f5_hrt"></span> <span class="cmp_txt">Compare
								all</span> <span class="loading_icon" id="load_icon"
							style="display: none"><img
								src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",
							0)%>" /></span>
						</a>
					</div>
				</div>
				<div class="cmlst act" id="uncompareall" style="display: none">
					<div class="compare">
						<a class="act"
							onclick="compareAll('<%=compareallcourses%>','', 'R');"> <span
							class="icon_cmp f5_hrt hrt_act"></span> <span class="cmp_txt">Compare
								all</span> <span class="loading_icon" id="load_icon"
							style="display: none"><img
								src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",
							0)%>" /></span>
						</a>
					</div>
					<div id="popCourseIds" class="sta"></div>
					<%--Added code for the bug:28713 on 03_Oct_2017, By Thiyagu G--%>
					<div id="loadPopCourseIds" class="sta" style="display: none;">
						<a class="view_more"
							onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');">View
							comparison</a>
					</div>
				</div>
				<%
					} else {
				%>
				<div class="cmlst" id="compareall" style="display: none">
					<div class="compare">
						<a
							onclick="compareAll('<%=compareallcourses%>','', 'A', '', '<%=compareAllCollegeNames%>');">
							<span class="icon_cmp f5_hrt"></span> <span class="cmp_txt">Compare
								all</span> <span class="loading_icon" id="load_icon"
							style="display: none"><img
								src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",
							0)%>" /></span>
						</a>
					</div>
				</div>
				<div class="cmlst act" id="uncompareall">
					<div class="compare">
						<a class="act"
							onclick="compareAll('<%=compareallcourses%>','', 'R');"> <span
							class="icon_cmp f5_hrt hrt_act"></span> <span class="cmp_txt">Compare
								all</span> <span class="loading_icon" id="load_icon"
							style="display: none"><img
								src="<%=CommonUtil.getImgPath("/wu-cont/images/ldr.gif",
							0)%>" /></span>
						</a>
					</div>
					<div id="popCourseIds" class="sta"></div>
					<%--Added code for the bug:28713 on 03_Oct_2017, By Thiyagu G--%>
					<div id="loadPopCourseIds" class="sta" style="display: none;">
						<a class="view_more"
							onclick="javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');">View
							comparison</a>
					</div>
				</div>
				<%
					}
				 }
				%> 
   		</div>
		</div>
	</section>
	<input type="hidden" id="currentcompare" value="" />
	<input type="hidden" id="comparestatus" value="" />
	 <%if(!"CLEARING PROFILE".equalsIgnoreCase(searchType)){%> 
	   <input type="hidden" id="compareallcourses" value="<%=compareallcourses%>" />
	<%}%> 
	<%--Added code for the bug:28713 on 03_Oct_2017, By Thiyagu G--%>
</c:if>