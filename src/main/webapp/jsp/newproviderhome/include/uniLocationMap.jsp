<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import=" WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" %>

<%
  String latitudeStr =  (String)request.getAttribute("latitudeStr");   
  String longitudeStr =  (String)request.getAttribute("longitudeStr");  
  String map_url = GlobalConstants.MAP_URL + "&size=490x270&markers=color:red|";
  String loadNewMapJs = CommonUtil.getResourceMessage("wuni.load.new.map.js", null);
%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=loadNewMapJs%>"></script>

<section class="clr_prof_ptb2">
<article class="content-bg">        
  <section class="study_loc p0">
    <input type="hidden" id="latitude" value="<%=latitudeStr%>"/>
    <input type="hidden" id="longitude" value="<%=longitudeStr%>"/>
    <h2 class="sub_tit fnt_lrg fl txt_lft w100p mbxhd">Where you'll study</h2>    
    <div id="div_location"></div>                      
    <div class="fl w100p map_staimg2">
      <c:forEach var="venue" items="${requestScope.locationInfoList}">
        <div id="locationinfoMaphh" style="display:none;">
          <p style="font-weight:bold;">
            <strong>
              <c:if test="${not empty venue.addLine1}">
                ${venue.addLine1}, 
              </c:if>
              <c:if test="${not empty venue.addLine2}">
                ${venue.addLine2}, 
              </c:if>
             <c:if test="${not empty venue.town}">
                ${venue.town}, 
              </c:if>
              <c:if test="${not empty venue.countryState}">
                ${venue.countryState}, 
              </c:if>   
               ${venue.postcode}                         
            </strong><br/>
          </p>
        </div>
        <%--Changed static google map to mapbox for Feb_12_19 rel by Sangeeth.S--%>        
        <jsp:include page="/jsp/common/mapbox.jsp" />
        
        <input type="hidden" id="mapDivContent" name="mapDivContent" value="0"/><%--Added by Prabha for lazy load map on 27_JAN_2016_REL--%>
        <div class="fl w100p mt35" id="uniVenueCnt"> <%--Removed itemprop, itemscope and itemtype on 16_May_2017, By Thyagu G--%>
          <div class="fl w100p mt10">
            <div class="fl gm_add_mob">                      
              <span><h2>${venue.collegeNameDisplay}</h2></span>
              <span class="address">
                <c:if test="${not empty venue.addLine1}">
                  ${venue.addLine1}
                </c:if>
                <c:if test="${not empty venue.addLine2}">
                  ${venue.addLine2}
                </c:if>
              </span>
              <c:if test="${not empty venue.town}">
                <span class="address">
                  ${venue.town}
                </span>
              </c:if>
              <c:if test="${not empty venue.countryState}">
                <span class="address">
                  ${venue.countryState}
                </span>
              </c:if>   
              <span class="address">
                ${venue.postcode}
              </span>
              <c:if test="${venue.isInLondon ne 'NONUK'}"><span class="address">United Kingdom</span></c:if> <%--Added condition for hiding loacation for non uk providers by Prabha on 4_oct_2017--%>
              <%--Hiding train station for non uk providers by Prabha on 04_oct_2017--%>
              <c:if test="${venue.isInLondon eq 'LONDON'}">
                <c:if test="${not empty venue.nearestTubeStation}">
                  <span class="address1"><span class="ns_fnt_bld">Nearest tube station: </span>${venue.nearestTubeStation}&nbsp; ${venue.distanceFromTubeStation} miles away</span>
                </c:if>
                <c:if test="${not empty venue.nearestStationName}"> 
                  <span class="address1"><span class="ns_fnt_bld">Nearest train station: </span>${venue.nearestStationName}&nbsp; ${venue.distanceFromNearestStation} miles away</span>
                </c:if>
              </c:if>
              <c:if test="${venue.isInLondon ne 'LONDON'}">
                <c:if test="${not empty venue.nearestStationName}">
                  <span class="address1"><span class="ns_fnt_bld">Nearest train station: </span>${venue.nearestStationName}&nbsp; ${venue.distanceFromNearestStation} miles away</span>
                </c:if>
              </c:if>                                 
            </div>                                
            <c:if test="${venue.cityGuideDisplayFlag eq 'Y'}">
              <div class="fr think_loc">          
                <p class="fnt_lbd fnt_18 lh_24 cl_grylt">Thinking of studying in ${venue.location}?</p>
                <p class="fnt_lrg fnt_14 lh_24 cl_grylt">Check out our <a href="${venue.cityGuideUrl}" class="fnt_lrg fnt_14 lh_24 link_blue">
                  ${venue.location} city guide</a>
                </p>
              </div>                
            </c:if>                      
          </div>  
          <script type="text/javascript">changeMapHeight();</script>
        </div> 
      </c:forEach>                    
    </div>  
  </section>  
</article> 
</section>