<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import=" WUI.utilities.CommonFunction"%>

<%
  String reviewSecStyle = "";
  CommonFunction comFn = new CommonFunction();
  String collegeId = (String)request.getAttribute("collegeId");
  String rating = (String)request.getAttribute("rating");
  String collegeName = (String)request.getAttribute("collegeName");
  collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
%>
<c:if test="${not empty requestScope.reviewSubjectList}">
  <section class="p0 mt35 clr_rw_mob">    
    <div class="content-bg course_deatils rrew_mob">
          <div class="rrev_sec nprof" id="write_review_pod">
            <div class="reviews late_rev drp_hlght">
            <div class="lrhd_sec">
            <h2 class="sub_tit fnt_lrg fl txt_lft whclr" id="reviewTitle">Latest reviews</h2>
                  <c:if test="${not empty requestScope.reviewSubjectList}">
                <fieldset class="fs_col2 fr" id="location" onclick="openDropdown()">
                <div class="od_dloc" id="selectedsubject"><span>All Subjects</span><span><i class="fa fa-angle-down"></i></span></div>
                  <div class="opsr_lst" id="subjectDropDown">
                  <ul>
                    <li><span> <a onclick="showSubjectReview('all','<%=collegeId%>','')">All Subjects</a></span></li>
                    <c:forEach var="reviewSubjectList" items="${requestScope.reviewSubjectList}" varStatus="index"> 
                    <c:set var="indexValue" value="${index.index}"/>
                      <li onclick="showSubjectReview('<%=pageContext.getAttribute("indexValue").toString()%>','<%=collegeId%>','${reviewSubjectList.categoryCode}')"><span><a id="subjectdropdownid_<%=pageContext.getAttribute("indexValue").toString()%>"> ${reviewSubjectList.subjectName}</a> </span></li>
                    </c:forEach>
                  </ul>
                  </div>
                </fieldset> 
                </c:if>
            </div>
             <div id="subjectReviewDetails">
             <jsp:include page="/jsp/uni/richprofile/include/reviewDetailsAjaxPod.jsp"/>
           </div>
            <div class="clear">
              <a class="bton" href="/university-course-reviews/<%=collegeName%>/<%=collegeId%>">READ ALL REVIEWS <i class="fa richp fa-long-arrow-right"></i></a>
            </div>
           
           <jsp:include page="/jsp/uni/richprofile/include/reviewBreakDown.jsp"/>  
          </div>
          </div>
        </div>
  </section>
 </c:if>


