<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction, com.wuni.util.seo.SeoUrls, WUI.utilities.SessionData"%>  

<%
  String studRanking = "", totStudRanking = "", quoteStyle = "txt_posi_blo m0 fl", changeAwardStyle = "", awdImgStyle = "awd_uniyr";
  if(request.getAttribute("awardExist")!=null && !"".equals(request.getAttribute("awardExist"))){
    awdImgStyle = "awd_uniyr txt_rht1";
  }
  String collegeId          = (String) request.getAttribute("interactionCollegeId");
  String nonclearingURL = (String)request.getAttribute("courseUrl");
         nonclearingURL =  !GenericValidator.isBlankOrNull(nonclearingURL)? nonclearingURL.replace("clearing&", ""):"";
  String profileType = request.getAttribute("setProfileType") != null ? (String)request.getAttribute("setProfileType") : "";//Added by Indumathi.S For Unique content in institution profile
  String appendNextYear = GlobalConstants.NEXT_YEAR;
  CommonFunction common = new CommonFunction();
  String collegeName1 = common.replaceURL(common.getCollegeName(collegeId, request)).toLowerCase();
  String clearingURL = new SeoUrls().constructCourseUrl("all", "CLEARING", collegeName1); //Forming clearing view all url by Prabha on 11_Jul_17
%>
<section class="clr_prof_ptb">
<c:if test="${not empty requestScope.showUniInfopod}">
  <div class="content-bg course_deatils clear">
    <c:if test="${not empty requestScope.USER_JOURNEY_FLAG}">
      <c:if test="${requestScope.USER_JOURNEY_FLAG eq 'CLEARING'}">
        <c:if test="${requestScope.CLEARING_COURSE_EXIST_FLAG ne 'Y'}" >
          <c:if test="${not empty requestScope.nonAdvertFlag}">
            <c:if test="${requestScope.nonAdvertFlag eq 'true'}">
              <div class="clr_hlt_avtxt"><h2>This uni does not have any Clearing courses available at the moment on Whatuni.com</h2></div>      
            </c:if>
            <c:if test="${requestScope.nonAdvertFlag eq 'false'}">
              <div class="clr_hlt_avtxt"><h2>This university is not currently displaying their clearing courses on Whatuni.com. We recommend you contact the university directly if you're interested in finding out more about their clearing options.</h2></div>      
            </c:if>
          </c:if>
        </c:if>
      </c:if>
    </c:if>
    <%String tabClass = (!"SUB_PROFILE".equalsIgnoreCase(profileType)) ? "key_stats kys greybg pQu_cnt pQu_info" : "key_stats kys greybg pQu_cnt";%>
    <section class="<%=tabClass%>"> <!--Added class name for tab redesign May_31_2016-->
      <c:if test="${not empty requestScope.uniInfoList}">
        <c:if test="${not empty requestScope.provQuickInfo}" >
          <h2 class="sub_tit fl fnt_lrg lh_22">Quick info</h2>
          <div class="borderbot1 fl mt25 mb20"></div>   
        </c:if>  
          <div class="pQu_cnr mb20">          
            <c:forEach var="profInfoList" items="${requestScope.uniInfoList}">
              
              <c:if test="${not empty profInfoList.awardImage}">
                <c:set var="awdImgLen" value="${profInfoList.awardImage}"/>   
                <%
                  String awdImgLen = (String)pageContext.getAttribute("awdImgLen");
                  String award_imge_len[]= awdImgLen.split("##SPLIT##");                
                  if(award_imge_len.length==2){
                    quoteStyle = "txt_posi_blo m0 fl st_awd_logo2";
                    changeAwardStyle = "true";
                  }else if(award_imge_len.length==3){
                    quoteStyle = "txt_posi_blo m0 fl st_awd_logo";
                    changeAwardStyle = "true";
                  }
                %> 
              </c:if>  
              
              <c:if test="${not empty profInfoList.pullQuotes}">        
                <div id="awdLftCnt" class="lft_cnr">	
                  <div class="pQu_top">
                    <figure class="fl"><i class="fa fa-quote-left"></i></figure>
                      <p class="<%=quoteStyle%>">${profInfoList.pullQuotes}</p>
                    <figure class="fl"><i class="fa fa-quote-right"></i></figure>
                  </div>
                </div>
              </c:if>
              
              <c:if test="${not empty profInfoList.awardImage}">
                <c:set var="awdImg" value="${profInfoList.awardImage}"/>                
                <div id="awdRhtCnt" class="rht_cnr">			
                  <%String awdImg = (String)pageContext.getAttribute("awdImg");
                  String award_imge[]= awdImg.split("##SPLIT##"); 
                    if(award_imge.length>0){
                       if(award_imge.length > 1){awdImgStyle = "awd_uniyr txt_rht1 bad_hor18";}
                  %>
                      <div class="<%=awdImgStyle%>">
                        <%for(int i =0; i<award_imge.length;i++){%>
                          <img src="<%=CommonUtil.getImgPath("",i+1)%><%=award_imge[i]%>" alt="Award image">
                        <%}%>
                      </div>		
                  <%}%>
                </div>
              </c:if>
            </c:forEach>
          </div>                        
      </c:if>                                         
      
      <c:if test="${COVID19_SYSVAR eq 'ON'}">
        <spring:message code="advice.teacher.url" var="teacherSectionUrl"/>
        <div class="sr">
          <div class="covid_upt">
	        <span class="covid_txt"><%=new SessionData().getData(request, "COVID19_COURSE_TEXT")%></span>
	        <a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_blu_arw.svg", 0)%>"></a>
          </div>
        </div>
      </c:if>

      <c:if test="${not empty requestScope.uniRankingList}">
        <%--No pullQuotes means hide the div code added by Prabha on 31_may_2016--%>
        <c:if test="${not empty requestScope.provQuickInfo}">
          <div class="borderbot1 fl mt25 mb20"></div>
          <div class="clear"></div>
        </c:if>
        <%--End of the code--%>
        <h2>Uni rankings and ratings</h2>
      </c:if>  
      <div class="clear fl w100p mt20">        
        <c:if test="${not empty requestScope.uniRankingList}">
          <c:forEach var="uniRankList" items="${requestScope.uniRankingList}">
            <c:if test="${not empty uniRankList.studentRanking}">
              <c:set var="studRank" value="${uniRankList.studentRanking}"/>
              <%studRanking = (String)pageContext.getAttribute("studRank");%>  
            </c:if>
            <c:if test="${not empty uniRankList.totalStudentRanking}">
              <c:set var="totStudRank" value="${uniRankList.studentRanking}"/>
              <%totStudRanking = (String)pageContext.getAttribute("totStudRank");%>
            </c:if>
            <%if(!"".equals(studRanking) || !"".equals(totStudRanking)){
            String cuYear = new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS"); 
            pageContext.setAttribute("cuYear", cuYear);
            %>
              <div class="twoC col3 pt15 fl pt23">              
                <h6>
                  <span class="hdf3 fl">WUSCA student ranking</span>
                  <span class="tool_tip"> 
                    <i class="fa fa-question-circle fnt_24">
                      <span class="cmp"> <%--Moved the tooltip text to application properties May_31_2016--%>
                        <div class="hdf5"></div><div><spring:message code="wuni.tooltip.wusca.ratings" arguments="${cuYear}" /></div><div class="line"></div>
                      </span>
                    </i>
                  </span> 
                  <div class="fl w100p">
                    <c:if test="${not empty uniRankList.studentRanking}">
                      <span class="hdf1">${uniRankList.studentRanking}</span>
                    </c:if>
                    <c:if test="${not empty uniRankList.totalStudentRanking}">
                      <span class="hdf1">/</span>
                      <span class="hdf2">${uniRankList.totalStudentRanking}</span>
                    </c:if>  
                  </div>
                </h6>              
              </div>
            <%}%>
            <c:if test="${not empty uniRankList.uniTimesRanking}">
              <div class="twoC col3 pt15 fl pt23 clr_rnk_wd">                                                                 
                <h6>
                  <span class="hdf3 fl">CompUniGuide ranking</span> <%--Changed ranking lable frim Times to CUG for 08_MAR_2016 By Thiyagu G--%>
                  <span class="tool_tip fl"> 
                    <i class="fa fa-question-circle fnt_24">
                      <span class="cmp">
                        <div class="hdf5">Complete University Guide Ranking</div><div><spring:message code="wuni.tooltip.cug.ranking.text" /></div><div class="line"></div> <%--Changed ranking tooltip content from property file for 08_MAR_2016 By Thiyagu G--%>
                      </span>
                    </i>
                  </span>
                  <div class="fl w100p">
                    <span class="hdf1">${uniRankList.uniTimesRanking}</span>
                  </div>
                </h6> 
              </div>         
            </c:if>
          </c:forEach>
        </c:if>                              
        
        <div class="txt_cnr">
          <!--<logic:notEmpty name="clearingHeader" scope="request">
            <logic:equal value="TRUE" name="clearingHeader" scope="request">
              <logic:notEqual name="courseExistsFlag" scope="request" value="Y">
                <div class="clr_strt_txt">View courses starting in <%=appendNextYear%></div>
              </logic:notEqual>
            </logic:equal>
          </logic:notEmpty>-->          
          <spring:message code="view.open.day.button.text" var="viewODBtnLabel"/>
          <c:if test="${not empty requestScope.openDaysUrlExist}">
            <a title="${viewODBtnLabel}" class="btn1 btn3 mt30 w190 clrmr15" href="${requestScope.openDaysUrlExist}">${viewODBtnLabel}<i class="fa fa-long-arrow-right"></i></a>
          </c:if>
          <c:if test="${not empty requestScope.courseUrl}" >
          <!--Added by Indumathi.S for Jan-27-16-->
          <c:if test="${not empty requestScope.clearingHeader}">
              <c:if test="${requestScope.clearingHeader ne 'TRUE'}">
                <c:if test="${not empty requestScope.courseExistsFlag and requestScope.courseExistsFlag ne 'NO_COURSE'}">
                      <a title="VIEW ALL COURSES" class="btn1 btn3 mt30 w190" href="${requestScope.courseUrl}">VIEW ALL COURSES<i class="fa fa-long-arrow-right"></i></a>
                </c:if>
              </c:if>
              
              <c:if test="${requestScope.clearingHeader eq 'TRUE' and requestScope.courseExistsFlag eq 'Y'}">
               <!-- <logic:notEqual name="courseExistsFlag" scope="request" value="Y">
                  <a title="VIEW ALL COURSES" class="btn1 btn3 mt30 w190" href="<%=nonclearingURL%>">VIEW ALL COURSES<i class="fa fa-long-arrow-right"></i></a>
                </logic:notEqual> -->
                      <a title="VIEW ALL COURSES" class="btn1 btn3 mt30 w190" href="<%=clearingURL%>">VIEW ALL COURSES<i class="fa fa-long-arrow-right"></i></a>
              </c:if>
            </c:if> 
          </c:if>  
        </div>
      </div>
    </section>
  </div>
  <input type="hidden" id="changeAwardStyle" value="<%=changeAwardStyle%>"/>
</c:if>  
</section>