<%@page import="java.util.ResourceBundle"%>
<%@page import="WUI.utilities.CommonUtil"%>
<%@page import="org.apache.commons.validator.GenericValidator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>
  <%
    String robotValue       =  "index,follow";
    String showBanner       =  "";
    String providerId       = (String)request.getAttribute("interactionCollegeId");
    String newUniPageName   = (String)request.getAttribute("newUniPageName");
    String newUniParamFlag  = (String)request.getAttribute("newUniParamFlag");    
    String mainSecStyle     = "rich_profile clr_prof_cnt nor_intr_btn";    
    String canonicalUrl = request.getAttribute("canonicalUrl")!=null ? request.getAttribute("canonicalUrl").toString().trim() : "";
    if(request.getAttribute("highlightTab")!=null && "CLEARING".equals((String)request.getAttribute("highlightTab"))){    
      mainSecStyle = "rich_profile clr_prof_cnt";
    }
    if("CLEARING".equalsIgnoreCase((String)request.getAttribute("USER_JOURNEY_FLAG"))){
      mainSecStyle = "rich_profile clr_prof_cnt clr_nw_bt clrnor_prof";
      newUniPageName = "CLEARING PROFILE";
      newUniParamFlag = "CLEARING PROFILE";
      robotValue       =  "index,follow";
      canonicalUrl = canonicalUrl.replace("?clearing","");
    }
    String nonAdvertFlag = request.getAttribute("nonAdvertFlag")!=null ? (String)request.getAttribute("nonAdvertFlag") : "";
    if(request.getAttribute("nonAdvertFlag")!=null && "true".equals((String)request.getAttribute("nonAdvertFlag"))){    
      showBanner = "ad_cnr";
    } 
    String spMyhcProfileId = "";
    String profileType = (String)request.getAttribute("setProfileType");
    if("SUB_PROFILE".equalsIgnoreCase(profileType)){
      spMyhcProfileId = (String)request.getAttribute("spMyhcProfileId");//Added by Indumathi NOV-03-15 for sp profile
    }
    ResourceBundle rb = ResourceBundle.getBundle("com.resources.ApplicationResources");
    String heroImageJsName = rb.getString("wuni.heroImgJsName.js");//Added hero image js Indumathi.S Apr_19_2016
    String reviewlightboxJs = CommonUtil.getResourceMessage("wuni.reviewlightbox.js", null);
    String uniProfileLandingJS = CommonUtil.getResourceMessage("wuni.uni.profile.landing.js", null);//changed dynaic for Feb_12_19 rel by Sangeeth.S
    String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
    //Added by Indumathi.S For pageview logging July-26-2016
   int stind1 = request.getRequestURI().lastIndexOf("/");
   int len1 = request.getRequestURI().length();
   String pageName = pageContext.getAttribute("pagename3") != null ? (String)pageContext.getAttribute("pagename3") : request.getRequestURI().substring(stind1+1,len1);
   pageName = pageName !=null ? pageName.toLowerCase() : pageName;
   String gaCollegeName = (String)request.getAttribute("hitbox_college_name");
         gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(gaCollegeName);
         gaCollegeName = !GenericValidator.isBlankOrNull(gaCollegeName) ? !gaCollegeName.equals("null") ? gaCollegeName.toLowerCase().replaceAll(" ","-") : "" : "";
  if((!GenericValidator.isBlankOrNull(pageName)) && "newproviderhome.jsp".equals(pageName)){
    String newProfileType = (String)request.getAttribute("setProfileType");
    if((!GenericValidator.isBlankOrNull(newProfileType)) && "NORMAL".equals(newProfileType)){
      pageName = "uniview.jsp";
    } 
  }
  %>  
<body>
<input type="hidden" name="pageName" id="pageName" value="<%=pageName%>">
<input type="hidden" name="gaPageCollegeName" id="gaPageCollegeName" value="<%=gaCollegeName%>">
<input type="hidden" name="nonAdvertFlag" id="nonAdvertFlag" value="<%=nonAdvertFlag%>">
<input type="hidden" name="profileType" id="profileType" value="<%=profileType%>">
<c:if test="${not empty requestScope.pixelTracking}">${requestScope.pixelTracking}</c:if>
<header class="clipart">
  <div class="<%=showBanner%>">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>  
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=uniProfileLandingJS%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=lazyLoadJs%>"> </script> 
</header>
<div class="<%=mainSecStyle%>">
  <jsp:include page="/jsp/newproviderhome/include/profileHeader.jsp"/>  
  <div class="clear"></div>
  <div class="<%=showBanner%>">
    <jsp:include page="/jsp/newproviderhome/include/uniInfoPod.jsp"/>      
    <jsp:include page="/jsp/newproviderhome/include/profileSectionsPod.jsp"/>     
    <c:if test="${not empty requestScope.listOfUniStats}">  
      <jsp:include page="/jsp/newproviderhome/include/uniKeyStatsInfo.jsp"/>
    </c:if>    
    <jsp:include page="/jsp/newproviderhome/include/latestReviewPod.jsp"/>       
    <%--<jsp:include page="/jsp/newproviderhome/include/scholarshipsPod.jsp"/>--%>
    <c:if test="${not empty requestScope.locationInfoList}">
      <jsp:include page="/jsp/newproviderhome/include/uniLocationMap.jsp"/>
    </c:if>  
    
    <!--Added by Indumathi.S for Cost of pint Nov-24-15-->
    <c:if test="${not empty requestScope.costOfPint}">
        <jsp:include page="/jsp/uni/include/costOfPint.jsp"/>
    </c:if>
    <!--End-->
    
    <c:if test="${not empty requestScope.uniSPList}">
      <jsp:include page="/jsp/uni/richprofile/include/richProfileSpList.jsp"/>
    </c:if> 
    <c:if test="${not empty requestScope.listOfTopCourses}">
      <section class="content-bg clr_prof_ptb3">
        <jsp:include page="/jsp/newproviderhome/include/coursesYouMightLikePod.jsp"> 
          <jsp:param name="searchtype" value="<%=newUniPageName%>"/> 
        </jsp:include>                
      </section>
    </c:if>
    
    <%if(!GenericValidator.isBlankOrNull(profileType) && ("NORMAL".equals(profileType) || "CLEARING_LANDING".equals(profileType) || "CLEARING_PROFILE".equals(profileType))){%>
      <div class="content-bg">
        <section class="artfc p0 mt38 cm_art_pd" id="articlesPod"></section>  
        <input type="hidden" id="scrollStop" value="" />
      </div>
    <%}%>
    <input type="hidden" id="extraPod" value="false"/>    
    <c:if test="${not empty requestScope.clearingHeader and requestScope.clearingHeader eq 'TRUE'}">
      <input type="hidden" name="comparepage" id="comparepage" value="CLEARING"/> 
      <input type="hidden" id="clearingFlag" value="CLEARING_PROF"/>
    </c:if>
    <c:if test="${not empty requestScope.switchProvUrl}">
      <input type="hidden" id="switchProvUrl" value="<%=request.getAttribute("switchProvUrl")%>"/>
    </c:if>
  </div>
</div>
<input type="hidden" id="spMyhcProfileId" value="<%=spMyhcProfileId%>"/><!--Added by Indumathi NOV-03-15 for sp rich profile-->
<%request.setAttribute("noJSLogging","false");%>

<!--Added by Indumathi.S NOV-03-15-->
<%if(request.getAttribute("nonAdvertFlag")!=null && "true".equals((String)request.getAttribute("nonAdvertFlag"))){ %>
   <div class="blst">  
    <script type="text/javascript"> 
      var width = document.documentElement.clientWidth;
      if (width <= 767) {
        GA_googleAddSlot("ca-pub-9150511304937518", "wuni_providerhome_lb_300x50");
        GA_googleFetchAds(); 
        GA_googleFillSlot("wuni_providerhome_lb_300x50");
      }
    </script> 
   </div> 
<%}%> 

<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>