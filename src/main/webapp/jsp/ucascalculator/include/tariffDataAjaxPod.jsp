


<!--Tariff Points Container Start -->
<div class="ucas_btm" id="tariffPod" style="display:none;">
 <div class="row_btm30">
  <div class="ucbk_lnk"><a onclick="goBackToScore();"><i class="fa fa-long-arrow-left"></i> Back</a></div>
  <div class="ucas_row">
   <h2>Tariff points table</h2>
   <label>Qualification</label>
   <fieldset class="ucas_fldrw">
    <div class="ucas_selcnt">
     <div class="ucas_sbox">
      <span class="sbox_txt" id="tfqualspan">A Level</span>
      <i class="fa fa-angle-down fa-lg"></i>
     </div>
     <select name="tfqualification" onchange="setSelectedVal(this, 'tfqualspan');getTariffPointsTableDataAjax('tariff');" id="tfqualse1ect" class="ucas_slist">      
     </select>
    </div>
   </fieldset>
  </div>
	 <!--Tariff Points Table start -->
  <div id="tariffDiv"></div>
	</div>	
  <!-- Tariff Points Table End -->
</div>
<!--Tariff Points Container End -->