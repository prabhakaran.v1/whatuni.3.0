<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
String tfqualVal = request.getAttribute("tfqualVal") != null ? (String)request.getAttribute("tfqualVal") : "";
%><!--Added by Indumathi.S FEB-16-16-->

<c:if test="${not empty requestScope.traiffPointsList}">
	 <div class="resptbl">
			<table cellspacing="0" cellpadding="0" border="0" class="ctbl">
				<thead>
				<tr>
		   <th>Grade</th>
		   <th id="qualTH">UCAS Tariff points</th>		   
			 </tr>
 			</thead>
				<tbody>
				<c:forEach var="traiffPointsLt" items="${requestScope.traiffPointsList}">    
				 <tr>
	 				<td data-title="Grade"><span>${traiffPointsLt.grade}</span></td>
						<td data-title="UCAS Tariff points"><span>${traiffPointsLt.tariffPoints}</span></td>						
					</tr>
    </c:forEach>
				</tbody>
			</table>		
	 </div>
</c:if>
