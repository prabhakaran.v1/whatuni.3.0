<%@page import="WUI.utilities.CommonFunction" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%String clearingonoff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");%>
<c:if test="${not empty requestScope.ucasPoints}">
<c:set var="ucasPointsVal" value="${requestScope.ucasPoints}"/>
    <!--Perfect srch pod Start -->
    <div class="ucas_btm ctrl_mt">
      <div class="ucas_row row_btm30">
        <h2>Now find the perfect course with your score</h2>
        <div class="perf_wrap">	
          <div class="perf_pts"><a onclick="delUcasPointsForSR();" id="fcUcas">${ucasPointsVal} points <i class="fa fa-times"></i></a><span>&nbsp;</span></div>
          <input type="hidden" id="fcUcasPoints" value="${ucasPointsVal}" />
          <div class="perf_srch"><span class="src_tbox">
            <input type="text" placeholder="Please enter your subject" autocomplete="off" name="ucasKeyword" id="ucasKwd" onkeyup="autoCompleteUcasKeywordBrowse(event,this)" onkeypress="javascript:if(event.keyCode==13){return ucasSearchSubmit();}" /></span><span class="src_btn">            
            <input type="hidden" id="ucasKeyword_hidden" value="" />
            <input type="hidden" id="ucasKeyword_id" value="" />
            <input type="hidden" id="ucasKeyword_display" value="" />
            <input type="hidden" id="ucasKeyword_url" value="" />            
            <input type="hidden" id="ucasmatchbrowsenode" value="">
            <input type="button" onclick="ucasSearchSubmit('<%=clearingonoff%>');"/><i class="fa fa-search" onclick="ucasSearchSubmit();"></i></span></div>
          </div>	
        </div>
      </div>
<!--Perfect srch pod end -->
</c:if>