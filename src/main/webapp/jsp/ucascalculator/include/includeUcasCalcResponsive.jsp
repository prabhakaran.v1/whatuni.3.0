<%--Added review responsive for 01-Sep-2015 By Thiyagu G --%>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction"%>
<%String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
String fromPageName = request.getParameter("fromPageName") != null ? request.getParameter("fromPageName") : "";
%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/commonresponsive.js"></script>
<script type="text/javascript" language="javascript">  
  var dev = jQuery.noConflict();
  dev(document).ready(function(){
   adjustStyle();
  });
  adjustStyle();
  function jqueryWidth() {
    return dev(this).width();
  }	
  function adjustStyle() {
    var width = document.documentElement.clientWidth;
    var path = "";     
    if (width <= 480) {
      if (dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }       
    } else if ((width > 480) && (width <= 992)) {
      if (dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      } 
    }else {
      if (dev("#viewport").length > 0) {
        dev("#viewport").remove();
      }      
    }
  }
  dev(window).on('orientationchange', orientationChangeHandler);
  function orientationChangeHandler(e) {
    setTimeout(function() {
      dev(window).trigger('resize');
    }, 500);
  }
  dev(window).resize(function() {        
    adjustStyle();
    var screenWidth = jqueryWidth();    
  });
  function jqueryWidth() {
    return dev(this).width();
  }  
</script>