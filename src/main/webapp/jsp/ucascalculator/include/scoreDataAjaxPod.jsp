<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${not empty requestScope.ucasPoints}">
<c:set var ="ucasPointsValue" value="${requestScope.ucasPoints}"/>
<c:set var ="ucasScorePercent" value="${requestScope.ucasScore}"/>
<c:set var ="ucasScorePcnt1" value="${requestScope.ucasScorePercnt1}"/>
<c:set var ="ucasScorePcnt2" value="${requestScope.ucasScorePercnt2}"/> 
  <!--Your Score Container Start -->
  <div class="ucas_btm ctrl_mt scr_mt0">
   <div class="ucas_row row_btm30">
    <h2>Your Score</h2>
    <div id="innerScoreDiv" class="scr_wrap">	
     <div class="scr_lft">
      <!--Chart-->
      <input type="hidden" id="userUcasPoints" name="userUcasPoints" value="${ucasPointsValue}" />
      <input type="hidden" id="userUcasScorePrcnt" name="userUcasScorePrcnt" value="${ucasScorePercent}" />
      <div class="chartbox fl w100p">
       <div id="ukIntl_doughnut" class="chart fl"><span>POINTS</span></div>
         <script type="text/javascript" id="script_how_you_are_assesed"> 
           drawUCASdoughnutchart('${ucasScorePcnt1}','${ucasScorePcnt2}','ukIntl_doughnut');
         </script>
       </div>
       <!--Chart-->
      </div>
      <div class="scr_rht">
       <div class="scrtxt_top"><span class="scr_txt">With your score</span><span class="scr_val">${ucasScorePercent}%</span></div>
       <div class="scr_pbar"><span style="width:${ucasScorePercent}%"></span></div>
       <div class="scr_desc"><p>You could qualify for a place on approximately ${ucasScorePercent}% of all UK courses.</p></div>
       <div class="sub_btn"><a onclick="getTariffPointsTableDataAjax('score');">SEE FULL UCAS POINTS TABLE <i class="fa fa-long-arrow-right"></i></a></div>
      </div>
     </div>	
    </div>
   </div>
   <!--Your Score Container end -->
</c:if> 
