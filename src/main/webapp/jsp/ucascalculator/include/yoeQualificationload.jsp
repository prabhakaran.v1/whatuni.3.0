<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, java.util.*" %>

<%String sectionHeading = ""; String nextSectionHeading="";%>
<c:if test="${not empty requestScope.qualificationList}">
<div id="qualOptions">
  <option value="0">Please Select</option>
   <c:forEach var="qualifications" items="${requestScope.qualificationList}" varStatus="index"> 
     <c:set var="indexIntValue" value="${index.index}"/>        
      <c:set var="qualListSize" value="${qualificationList}"/>     
      <c:if test="${not empty qualifications.parentQualification}">  
            <c:set var="secHeading" value="${qualifications.parentQualification}"/> 
      <%sectionHeading = pageContext.getAttribute("secHeading").toString();%>
    </c:if>
    <%if((Integer.parseInt(pageContext.getAttribute("indexIntValue").toString())==0)) {%>
        <optgroup label="<%=sectionHeading%>">
    <%}%> 
    <c:if test="${not empty qualifications.qualId}">
     <c:if test="${qualifications.qualId eq 1}">
        <option value="${qualifications.qualId}" selected="selected">${qualifications.qualification}</option>
      </c:if>
      <c:if test="${qualifications.qualId ne 1}">
        <option value="${qualifications.qualId}">${qualifications.qualification}</option>
      </c:if>     
    </c:if>
    <c:if test="${qualifications.parentQualification ne qualificationList[indexIntValue + 1].parentQualification }">
       <c:set var="nextSecHeading" value="${qualificationList[indexIntValue + 1].parentQualification}"/>   
      <%nextSectionHeading = (String)pageContext.getAttribute("nextSecHeading");%>
      <%if(nextSectionHeading != null ){%>
        </optgroup>
        <optgroup id = "${qualifications.qualId}" label="<%=nextSectionHeading%>" style="display:block;">
      <%}%>  
      <%ArrayList listSize = (ArrayList)request.getAttribute("qualificationList");
      int size = listSize.size();
        if(Integer.parseInt(pageContext.getAttribute("indexIntValue").toString())==size){%>
          </optgroup>
      <%}%>
      </c:if>
  </c:forEach>
  </div>
  <div id="grdHiddens">  
  <c:forEach var="grdQualifications" items="${requestScope.qualificationList}" varStatus="index1"> 
  <c:if test="${not empty grdQualifications.qualId}">
      <input type="hidden" id="${grdQualifications.qualId}" name="${grdQualifications.qualId}" value="${grdQualifications.gradeStr}" />
      <input type="hidden" id="grdsCnt_${grdQualifications.qualId}" name="grdsCnt_${grdQualifications.qualId}" value="${grdQualifications.noOfSubjects}" />
     </c:if>
    </c:forEach>
  </div>
  </c:if>
<c:if test="${not empty requestScope.yoeFlag}">
  <input type="hidden" id="yoeFlag" value="${requestScope.yoeFlag}" />
</c:if>