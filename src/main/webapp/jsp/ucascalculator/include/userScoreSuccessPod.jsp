<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test = "${not empty requestScope.ucasPoints}">
<c:set var="ucasPointsValue" value="${requestScope.ucasPoints}" />
<c:set var="ucasScorePrcntValue" value="${requestScope.ucasScorePrcnt}" />
<!--Your Score success Pod Start -->
  <div class="ucas_btm ctrl_mt">
	<div class="ucas_row row_btm30">
		<h2>You scored ${ucasPointsValue} points!</h2>
	<div class="scr_wrap">	
		<div class="scr_rht ful_wdth pro_size">
			<div class="scrtxt_top"><span class="scr_txt">With your score</span>
			<span class="scr_val">${ucasScorePrcntValue}% 
			<!--<span class="scr_tltip"><i class="fa fa-question-circle"> </i>
				<span class="scr_tip">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</span>
			</span>-->
			</span>
			</div>
			<div class="scr_pbar"><span style="width:${ucasScorePrcntValue}%"></span></div>
			<div class="scr_desc"><p>You could qualify for a place on approximately ${ucasScorePrcntValue}% of all UK courses.</p></div>
			<div class="scr_ttip">
				<h4>This score should act as guidance only.</h4>
				<p>Having the minimum required UCAS points does not guarantee you
acceptance onto the course. Unis often have specific entry requirements for
their courses, so make sure you contact the uni directly to find out what
they are.</p>
			</div>
		</div>
	</div>	
	</div>
  </div>
 <!--Your Score success Pod end -->
 </c:if>