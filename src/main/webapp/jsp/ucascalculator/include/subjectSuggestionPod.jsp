<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
  String selectdSub = "Please select";
%>
<c:if test="${not empty requestScope.subjectList}">
<!--Suggestion Container Start -->
  <div class="ucas_btm ctrl_mt">
	<div class="ucas_row">
	   <div class="row_btm">	
		<h2>You could study</h2>
		<h3>Suggestion based on your score and previous subjects</h3>
		<fieldset class="ucas_fldrw">
			<div class="suc_shwrow">
			<div class="suc_reslbl"><label>Showing results for</label></div>
			<div class="ucas_selcnt">
			<c:if test="${not empty requestScope.subjectList}">		
			<c:forEach var="subLt" items="${requestScope.subjectList}" end ="1"> 		  
			<c:set var="subjectDsc" value="${subLt.subjectDesc}"/>
       <%selectdSub=(String)pageContext.getAttribute("subjectDsc");%>
    </c:forEach>
   </c:if>
    <div class="ucas_sbox">
					<span class="sbox_txt" id="showingRes"><%=selectdSub%></span>
					<span class="grdfilt_arw"></span>
				</div>
    <select name="subjectsList" class="ucas_slist" id="subjectsList" onchange="setSelectedVal(this, 'showingRes'); youCouldStudySubmit('resultsDp');">      
      <c:forEach var="subjectLt" items="${requestScope.subjectList}" varStatus ="index"> 
      <c:set var="indexIntValue" value="${index.index}"/>
      <c:set var ="subjId" value="${subjectLt.subjectId}"/>
       <%if(Integer.parseInt(pageContext.getAttribute("indexIntValue").toString())==0){%>
        <option value="${subjId}" selected="selected">${subjectLt.subjectDesc}</option>
       <%}else{%>
        <option value="${subjId}">${subjectLt.subjectDesc}</option>
        <%}%>
      </c:forEach>
    </select>
			</div>
			</div>
		</fieldset>
		</div>  
  <div id="resultsDpwn">  
    <%@include file="/jsp/ucascalculator/include/subjectSearchResultsPod.jsp" %> 
  </div>	
	</div>
</div>
</c:if>
 <!--Suggestion Container end -->