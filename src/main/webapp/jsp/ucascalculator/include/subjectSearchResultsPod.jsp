<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonFunction" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%  
  String keywordorsubjectemail = request.getAttribute("keywordOrSubject") != null ? new CommonFunction().replaceURL(request.getAttribute("keywordOrSubject").toString()) : "0";  
  String hotlinelogging  = "";
  String gaCollegeName ="";  
%>
<!-- Suggestion subject pod repeat start-->
<c:if test="${not empty requestScope.searchResultsList}">
<c:forEach var="searchResults" items="${requestScope.searchResultsList}"> 
	<div class="row_blr">
		<div class="scr_wrap">	
			<div class="scr_lft">
			<c:if test="${not empty searchResults.providerImage}">
    <a onclick="openNewWindow('${searchResults.collegeNameUrl}');"> 
      <img src="${searchResults.providerImage}"  title="${searchResults.collegeNameDisplay}" class="ibdr lazy-loaded" alt="${searchResults.collegeNameDisplay}">
    </a>
    </c:if>
    <c:set var="collegeName" value="${searchResults.collegeName}"/>
 <c:set var="collegeId" value="${searchResults.collegeId}"/>
    <%gaCollegeName = new CommonFunction().replaceSpecialCharacter((String)pageContext.getAttribute("collegeName"));%>    
   </div>    
   <c:forEach var="bestMatchCourse" items="${searchResults.bestMatchCoursesList}" end="1"> 
			<div class="scr_rht">
				<div class="scrtxt_top">
     <span class="scr_txt">
      <a onclick="openNewWindow('${bestMatchCourse.courseDetailUrl}');" class="cour_name">${bestMatchCourse.courseTitle}</a>      
				  <a onclick="openNewWindow('${searchResults.collegeNameUrl}');" class="univ_name">${searchResults.collegeNameDisplay}</a>
     </span>
    </div>
				<div class="scr_desc"><p>Entry Requirements   
				<c:if test="${ not empty bestMatchCourse.courseUcasTariff}">    
         ${bestMatchCourse.courseUcasTariff}
         </c:if>
         <c:if test="${empty bestMatchCourse.courseUcasTariff}">
         <span>Please contact Uni directly</span>
      </c:if>
      </p></div>
				<div class="sub_btn">
      <a onclick="openNewWindow('${bestMatchCourse.courseDetailUrl}');">VIEW COURSE <i class="fa fa-long-arrow-right"></i></a>
      <c:if test="${not empty bestMatchCourse.subOrderItemId}">
      <c:if test="${bestMatchCourse.subOrderItemId ne 0}">
<c:set var="subOrderId1" value="${bestMatchCourse.subOrderItemId}"/>
<c:if test="${not empty bestMatchCourse.subOrderEmailWebform}">        
          <%--Changed event action webclick to webform by Sangeeth.S for July_3_18 rel--%>
          <a rel="nofollow" 
             target="_blank"
             class="req_inf"
             onclick="GAInteractionEventTracking('emailwebform', 'interaction', 'email webform', '<%=gaCollegeName%>', ${bestMatchCourse.webformPrice}); cpeEmailWebformClick(this,'<%=pageContext.getAttribute("collegeId")%>','${bestMatchCourse.subOrderItemId}','${bestMatchCourse.cpeQualificationNetworkId}','${bestMatchCourse.subOrderEmailWebform}'); var a='s.tl(';" 
             href="${bestMatchCourse.subOrderEmailWebform}" 
             title="Email ${searchResults.collegeNameDisplay}">
             REQUEST INFO <i class="fa fa-caret-right"></i>
          </a>         
         </c:if>
         <c:if test="${not empty bestMatchCourse.subOrderEmail}">
 <c:if test="${empty bestMatchCourse.subOrderEmailWebform}">
         
            <a rel="nofollow"
               target="_blank"
               class="req_inf"
               onclick="GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '<%=gaCollegeName%>');openNewWindow('<SEO:SEOURL pageTitle="sendcollegemail" >${searchResults.collegeName}#${searchResults.collegeId}#${bestMatchCourse.courseId}#<%=keywordorsubjectemail%>#n#${bestMatchCourse.subOrderItemId}</SEO:SEOURL>');return false;"
               title="Email ${searchResults.collegeNameDisplay}" >
               REQUEST INFO <i class="fa fa-caret-right"></i>
            </a>         
          </c:if>

         </c:if>         
   
        </c:if>
       </c:if>   
    </div>
			</div>
   </c:forEach> 
		</div>	
	</div>

  </c:forEach>
</c:if>
<c:if test="${empty requestScope.searchResultsList}">  
    <div class="row_btm">
      <h3>No results found for this subject.</h3>  
    </div>      
</c:if>
	<!-- Suggestion subject pod repeat end-->	