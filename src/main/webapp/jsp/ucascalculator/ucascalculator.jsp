<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants" %>
    <%
      String enName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");   
      String domainPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
      String logoPath="";
      if(("LIVE").equals(enName)){
        logoPath = CommonUtil.getCSSPath()+"/images/widget/wu_widlgo.png";
      } else if("TEST".equals(enName)){
        logoPath = domainPath+"/wu-cont/images/widget/wu_widlgo.png";
      } else if("DEV".equals(enName)){
        logoPath = domainPath+"/wu-cont/images/widget/wu_widlgo.png";
      }
      String yearSelected = GlobalConstants.UCAS_LOAD_YEAR_VALUE;
    %> 
<body>
   <div class="ucas_cnt ucas_page">
    <div class="ucas_wrap">
     <a class="ucas_cls" id="ucasCls" onclick="ucasClose();"><i class="fa fa-times"></i></a>
     <!-- UCAS Calculator middle container start-->
      <div class="ucas_midcnt">
       
        <div class="ucas_hd">
       <img src="<%=logoPath%>" alt="Whatuni UCAS calulator logo"/>  
       <h2>UCAS Calculator</h2>
       <p>Calculate your UCAS points score and find the best and most relevant courses for you.</p>
      </div>       
        <div id="ucas_calc">
        <div class="ucas_mid ">
       <div class="ucas_row">
		      <div class="row_tb">
		       <fieldset class="ucas_fldrw">
			       <div class="ucas_selcnt">
            <div id="yoeCrsMsg"></div>
            <input type="hidden" id="yoesel" name="yoesel" value="<%=yearSelected%>">
            <input type="hidden" id="qualsTotCnt" name="qualsTotCnt" value="1" />
            <div id="dynQualList" style="display:none;"><jsp:include page="/jsp/ucascalculator/include/yoeQualificationload.jsp"/></div>            
			       </div>
		       </fieldset>
		      </div>
    <!--     Added display none for UCAS tariff link by Prabha on 28_03_2018 -->
        <div class="row_tb ucas_rowtb" style="display:none;">
          <a onclick="openNewWindow('https://www.ucas.com/ucas/undergraduate/getting-started/entry-requirements/tariff/calculator');">See the 2017 UCAS tariff here</a>
        </div>
	      </div>
       <!--Qualification Repeat Field start -->	
	      <div class="add_qualif">
	       <div class="ucas_refld">
	        <div class="ucas_row">
		        <label>Qualification</label>
		        <fieldset class="ucas_fldrw">
			        <div class="ucas_selcnt">
				        <div class="ucas_sbox">
					        <span class="sbox_txt" id="qualspanu1">A Level</span>
					        <span class="grdfilt_arw"></span>
				        </div>
				        <select name="qualification1" onchange="setSelectedVal(this, 'qualspanu1');onChageQualGrades(this);" id="qualse1" class="ucas_slist" >
				          
                </select>
            <input type="hidden" id="qualSubCnt1" value="3">
            <input type="hidden" id="subCnt1" value="3">
            <input type="hidden" id="grdParam1" name="grdParam1" />
            <input type="hidden" id="subParam1" name="subParam1" />
			        </div>
          <div class="err" id="err_qualse1" style="display:none;"></div>
		        </fieldset>
	        </div>
	        <div class="ucas_row grd_row" id="grdrow1">
		        <label>Grade</label>
		        <!--Repeat Field start -->
          <div class="rmv_act" id="1rmvActDiv1">
		        <fieldset class="ucas_fldrw">
			        <div class="ucas_selcnt rsize_wdth">
				        <div class="ucas_sbox" id="1grades1">
					        <span class="sbox_txt ucas_dis" id="1gradeSpan1">A*</span>
					        <span class="grdfilt_arw"></span>
				        </div>
				        <select class="ucas_slist" onchange="setSelectedVal(this, '1gradeSpan1');getUcasPointsAndScore('qualse1');" id="1selGrade1" >				
             <option value="A*">A*</option>
             <option value="A">A</option>
             <option value="B">B</option>
             <option value="C">C</option>
             <option value="D">D</option>
             <option value="E">E</option>
            </select>
            <input type="hidden" id="1grdHid1" name="1grdHid1" />
            <input type="hidden" id="1subHid1" name="1subHid1" />
            <input type="text" id="1ucas1" name="1ucas1" style="display:none;" />
           </div>
			        <div class="ucas_tbox">
  			       <input type="text" id="1qualSub1" name="1qualSub1" onkeyup="ucasQualSubjectList(event, this, 'qualse1');" onblur="getUcasPointsAndScore('qualse1');" placeholder="Enter subject here..." autocomplete="off" class="ucas_dis"  />
            <input type="hidden" id="1qualSub1_hidden" name="1qualSub1_hidden" />
			        </div>
           <div class="add_fld"><a href="javascript:void(0);" class="btn_act" id="subAdd1" style="display:none;"><i class="fa fa-plus"></i></a></div>
			       </fieldset>
          </div>
			       <!--Repeat Field end -->
			       <!--Repeat Field start -->
          <div id="staticSubject">
          <div class="rmv_act" id="1rmvActDiv2">
			       <fieldset class="ucas_fldrw">
			        <div class="ucas_selcnt rsize_wdth">
			         <div class="ucas_sbox" id="1grades2">
			          <span class="sbox_txt ucas_dis" id="1gradeSpan2">A*</span>
			          <span class="grdfilt_arw"></span>
			         </div>
			         <select class="ucas_slist" onchange="setSelectedVal(this, '1gradeSpan2');getUcasPointsAndScore('qualse1');" id="1selGrade2" >				
			          <option value="A*">A*</option>
             <option value="A">A</option>
             <option value="B">B</option>
             <option value="C">C</option>
             <option value="D">D</option>
             <option value="E">E</option>			          
			         </select>
			         <input type="hidden" id="1grdHid2" name="1grdHid2" />
			         <input type="hidden" id="1subHid2" name="1subHid2" />
			         <input type="text" id="1ucas2" name="1ucas2" style="display:none;" />
			        </div>
			        <div class="ucas_tbox">
			         <input type="text" id="1qualSub2" name="1qualSub2" onkeyup="ucasQualSubjectList(event, this, 'qualse1');" onblur="getUcasPointsAndScore('qualse1');" placeholder="Enter subject here..." autocomplete="off" class="ucas_dis"  />
			         <input type="hidden" id="1qualSub2_hidden" name="1qualSub2_hidden" />
			        </div>           
			       </fieldset>
          </div>
			       <!--Repeat Field end -->
			       <!--Repeat Field start -->
          <div class="rmv_act" id="1rmvActDiv3">
			       <fieldset class="ucas_fldrw">
			        <div class="ucas_selcnt rsize_wdth">
			         <div class="ucas_sbox" id="grades3">
			          <span class="sbox_txt ucas_dis" id="1gradeSpan3">A*</span>
			          <span class="grdfilt_arw"></span>
			         </div>
			         <select class="ucas_slist" onchange="setSelectedVal(this, '1gradeSpan3');getUcasPointsAndScore('qualse1');" id="1selGrade3" >				
			          <option value="A*">A*</option>
             <option value="A">A</option>
             <option value="B">B</option>
             <option value="C">C</option>
             <option value="D">D</option>
             <option value="E">E</option>
			         </select>
			         <input type="hidden" id="1grdHid3" name="1grdHid3" />
			         <input type="hidden" id="1subHid3" name="1subHid3" />
			         <input type="text" id="1ucas3" name="1ucas3" style="display:none;" />
			        </div>
			        <div class="ucas_tbox">
			         <input type="text" id="1qualSub3" name="1qualSub3" onkeyup="ucasQualSubjectList(event, this, 'qualse1');" onblur="getUcasPointsAndScore('qualse1');" placeholder="Enter subject here..." autocomplete="off" class="ucas_dis"  />
			         <input type="hidden" id="1qualSub3_hidden" name="1qualSub3_hidden" />
			        </div>
			       </fieldset>
          </div>
          </div>
			       <!--Repeat Field end -->
			      </div>
			     </div>
			    </div>
       <!--Qualification Repeat Field end -->
        <div class="add_fld add_qualtxt" id="addQual" style="display: none;"><a href="javascript:void(0);" class="btn_act2"><i class="fa fa-plus"></i>Add Qualification</a></div>
        <div class="err" id="subErrMsg" style="display:none;"></div>
	       <div class="sub_btn"><a onclick="submitUcasClacSubject('submit');">VIEW MY SCORE <i class="fa fa-long-arrow-right"></i></a></div>
       </div>
        <!-- Tariff Points Container Start -->      
       
        <div id="scoreDiv">
          <div class="ucas_btm ctrl_mt scr_mt0">
          <div class="ucas_row row_btm30">
           <h2>Your Score</h2>
           <div id="innerScoreDiv" class="scr_wrap">	
            <div class="scr_lft">
             <!--Chart-->
             <input type="hidden" id="userUcasPoints" name="userUcasPoints" value="0" />   
             <input type="hidden" id="userUcasScorePrcnt" name="userUcasScorePrcnt" value="0" />
             <div class="chartbox fl w100p">
              <div id="ukIntl_doughnut" class="chart fl"><span>POINTS</span></div>
                <script type="text/javascript" id="script_how_you_are_assesed"> 
                  drawUCASdoughnutchart('1','99','ukIntl_doughnut');
                </script>
              </div>
              <!--Chart-->
             </div>
             <div class="scr_rht">
              <div class="scrtxt_top"><span class="scr_txt">With your score</span><span class="scr_val">0%</span></div>
              <div class="scr_pbar"><span style="width:0%"></span></div>
              <div class="scr_desc"><p>You could qualify for a place on approximately 0% of all UK courses.</p></div>
              <div class="sub_btn"><a onclick="getTariffPointsTableDataAjax('score');">SEE FULL UCAS POINTS TABLE <i class="fa fa-long-arrow-right"></i></a></div>
             </div>
            </div>	
           </div>
          </div>
          <!--Your Score Container end -->
        </div>
        <%@include file="/jsp/ucascalculator/include/tariffDataAjaxPod.jsp" %>
       </div>       
       <div id="ucas_calc_sucs" style="display:none;">     
       </div>
      </div>
      <!-- UCAS Calculator middle container end-->
     </div>
    </div>
    <jsp:include page="/jsp/thirdpartytools/googleAnalytics.jsp" />
    <jsp:include page="/jsp/thirdpartytools/gaCustomDimensions.jsp" />
  </body>
