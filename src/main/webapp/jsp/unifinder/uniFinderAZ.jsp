<%--
  * @purpose:  This jsp is displayed when the user clicks on AZ view..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 3-Sep-2015     Indumathi Selvam          1.0      First draft           wu_546
  * *************************************************************************************************************************
--%>
<%@ page import="WUI.utilities.CommonUtil,org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
 <%
  String searchTxt = (String) request.getAttribute("searchTxt");
         searchTxt = (GenericValidator.isBlankOrNull(searchTxt)) ? "" : searchTxt;
  String region = (String) request.getAttribute("region");
         region = GenericValidator.isBlankOrNull(region) ? "" : "&region=" + region + "&flag=y";//flag is used to differntiate direct and ajax flow
  String noindexfollow = "noindex,follow";
  if(GenericValidator.isBlankOrNull(request.getQueryString())){
    noindexfollow = "index,follow";
  }
  String highlight = "";
  String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null);
  String findUniSearchJs = CommonUtil.getResourceMessage("wuni.find.uni.search.js", null);
  String opendaysJs = CommonUtil.getResourceMessage("wuni.opendays.js", null);
%>
  <body>    
    <header class="clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp"/>
          </div>                
        </div>      
      </div>
    </header>    
    <div class="open_day funi">
      <section id="heroslider" class="hero_pod p0">
        <div class="fix_top_bor">
          <div id="fixedscrolltop" class="">
            <div class="ad_cnr">
              <div class="content-bg">
                <div id="bcrummbs" class="sbcr mb-10 pros_brcumb">          
                  <jsp:include page="/seopods/breadCrumbs.jsp"/>                       
                </div>
                <h1 class="cf tit2 fnt_lbd">University finder</h1>
                <jsp:include page="/jsp/unifinder/include/uniFinderSearchBar.jsp">
                  <jsp:param name="view" value="AZ"/>
                </jsp:include>
                
                <c:if test="${not empty requestScope.alphabetList}">
                    <div class="op_mth_cnr" id="mobilefixedscrolltop">
                      <div class="select_box_month">
                        <span class="sld_mth">A </span>
                        <i class="fa fa-angle-down"></i>
                      </div>
                      <div class="atz op_mth_slider" id="atz">
                        <ul>   
                          <c:forEach var="alphabetList" items="${requestScope.alphabetList}" varStatus="id">
                            <c:if test="${alphabetList.value eq searchTxt }">
                              <%highlight = "active";%> 
                            </c:if>
                            <c:if test="${alphabetList.value ne searchTxt }">
                              <%highlight = "";%>   
                            </c:if>
                            <li id="li_${alphabetList.label}" name="li_${id}" class="<%=highlight%>">
                              <a id="${alphabetList.label}" name="${alphabetList.label}" href="javascript:void(0);" onclick="getUniBrowserList(this, '${alphabetList.label}')">${alphabetList.label}</a>                    
                            </li>
                          </c:forEach>
                        </ul>
                      </div>
                    </div>
                </c:if>	
              </div>  
            </div>
          </div>
          <div class="gen_hero_cnr"></div>
        </div>
      </section>
      <jsp:include page="/jsp/search/include/searchGoogleAdSlots.jsp"/>
      <div class="ad_cnr">
        <div class="content-bg">
          <div class="subCon cf">
            <div class="op_map_cnr" id="op_map_cnr">
              <div class="op_upcom">
                <div class="ucod_cont" id="ucod_cont">
                  <c:if test="${not empty requestScope.displayRegionName}">
                    <div id="displayRegion" class="tit6 fnd_uni_hd">Unis in ${requestScope.displayRegionName}</div>
                  </c:if>
                  <div id="uniList">
                    <jsp:include page="/jsp/unifinder/include/uniFinderAZListPod.jsp"/> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <input type="hidden" id="searchTxt" value="${searchTxt}"/> 
    <input type="hidden" value="<%=region%>" id="region"/>
    <jsp:include page="/jsp/common/wuFooter.jsp" /> 
    <input id="reloadValue" type="hidden" name="reloadValue" value="" />
  </body>

