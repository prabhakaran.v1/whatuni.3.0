<%--
  * @purpose:  This jsp is displayed when the user clicks on Find a uni link..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 3-Sep-2015     Indumathi Selvam          1.0      First draft           wu_546
  * *************************************************************************************************************************
--%>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" %>
  <body>
    <header class="clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>
    </header>     
    <div class="open_day mpv">
      <section id="heroslider" class="hero_pod p0">
        <div class="fix_top_bor">
          <div id="fixedscrolltop1">
            <div class="ad_cnr">
              <div class="content-bg">
                <div id="bcrummbs" class="sbcr mb-10 pros_brcumb">          
                  <jsp:include page="/seopods/breadCrumbs.jsp"/>                       
                </div>
                <h1 class="cf tit2 fnt_lbd">University finder</h1>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="ad_cnr">
        <div class="content-bg">
          <div class="subCon cf">
            <jsp:include page="/jsp/unifinder/include/uniFinderSearchBar.jsp">
              <jsp:param name="view" value="M"/>
            </jsp:include>
            <div class="op_map_cnr">
              <jsp:include page="/jsp/openday/include/opendaysLandingLocation.jsp">
                <jsp:param name="regionUniSearch" value="regionUniSearch"/>
              </jsp:include>
            </div>
          </div>
        </div>
      </div>
    </div>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
  </body>