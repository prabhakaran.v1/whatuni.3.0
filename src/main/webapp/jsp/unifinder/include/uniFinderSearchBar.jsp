<%--
  * @purpose:  This jsp is included for uniBrowser and uniFinder to search based on university..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 3-Sep-2015     Indumathi Selvam          1.0      First draft           wu_546
  * *************************************************************************************************************************
--%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%String pageVisible = (String)request.getParameter("view");
         pageVisible = GenericValidator.isBlankOrNull(pageVisible)?"M":pageVisible;
  pageContext.setAttribute("pageVisible", pageVisible);
%>     
<section class="srch_open">
  <div class="filt_srch">
    <form name="homepagebean" id="homeSearchBean" method="post" action="<%=request.getContextPath()%>/home.html" onsubmit="javascript:return redirectUniHOME(document.getElementById('uniName'),'homeSearchBean','finduni');"> 
      <fieldset class="fr srch_box">
        <label for="reviewSearchKwd" class="tit6 ttu">Know which uni you're looking for?</label>
        <input type="text" name="uniName" id="uniName" class="fnt_lrg gen_srchbox" value='Enter uni name' autocomplete="off"
               onkeydown="clearUniNAME(event,this)" 
               onkeyup="autoCompleteUniNAME(event,this,'finduni');javascript:setUniAutoCompleteClass(this);"  
               onclick="clearUniDefaultTEXT(this)" 
               onblur="setUniDefaultTEXT(this)" maxlength="4000"
               onkeypress="javascript:if(event.keyCode==13){return redirectUniHOME(this,'homeSearchBean')}" />
        <input type="hidden" id="uniName_hidden" value="" />
        <input type="hidden" id="uniName_id" value="" />
        <input type="hidden" id="uniName_display" value="" />
        <input type="hidden" id="uniName_url" value="" /> 
        <input type="hidden" id="uniName_alias" value="" />
        <input type="submit" value="" class="gen_icon_srch fr" />  
        <span class="autocompletepos" id="uniIndicator" style="display:none;"><img src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/indicator.gif", 0)%>" alt="" /></span>
      </fieldset>
    </form>
  </div>
  <div class="view_by">
    <h6 class="tit6">VIEW BY</h6>
    <spring:message code="find.uni.url" var="findUniUrl"/>
    <spring:message code="map.view.url" var="mapViewUrl"/>
    <div class="view_cnr">
      <form name="locAndDateForm" id="locAndDateForm" method="post">
        <ul>
          <li class="op_loct">
            <a id="locLand" href="${mapViewUrl}" class="${'M' eq pageVisible ? 'act' : ''}"><i class="fa fa-map-marker"></i>
              <span class="txt"><spring:message code="map.view.btn.name"/></span>
            </a>
          </li>
          <li class="op_dte">
            <a id="dateLand" href="${findUniUrl}" class="${'AZ' eq pageVisible ? 'act' : ''}">
              <span class="txt"><spring:message code="a.z.btn.name"/></span>
            </a>
          </li>
        </ul>
      </form>
    </div>
  </div>
</section>