<%--
  * @purpose:  This jsp is to show the error message using light box.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc          Rel Ver.
  * 3-Sep-2015     Indumathi Selvam          1.0      First draft           wu_546
  * *************************************************************************************************************************
--%>
<%
String pageType = request.getParameter("pageType");
%>
<div class="comLgh">
  <div class="fcls nw">
     <a class="" onclick="javascript:hm();"><i class="fa fa-times"></i></a>
  </div>   
  <div class="pform nlr bgw opday f5_ms" id="lblogin">    
    <div class="reg-frm">
      <div class="reg-frmt bgw cmp_pt2">   
        <%if("emptyUni".equals(pageType)){%> 
          <h2>Oops..<br/> Please select a university from the dropdown</h2>  
          <div class="fr f5_msge">             
            <a class="edt_fnl5_btn ed_f5_btn msg_f5_clse" href="javascript:hm();">CONTINUE <i class="fa richp fa-long-arrow-right"></i></a>
          </div>
        <%}%>
      </div>        
    </div>       
  </div>  
</div>  

