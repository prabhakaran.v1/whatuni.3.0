<%--
  * @purpose:  This jsp is to get the university list and included in uniFinderAZ page..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc     Rel Ver.
  * 3-Sep-2015     Indumathi Selvam          1.0      First draft      wu_546
  * *************************************************************************************************************************
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator, WUI.utilities.CommonFunction" %>
<%
  String totalCount = request.getAttribute("totalRecordCount") != null ? (String)request.getAttribute("totalRecordCount") : "";  
  String curPageNo = request.getAttribute("pageNo") != null ? (String)request.getAttribute("pageNo") : "1";
  int totalPages = 0;
  int results = 0; 
	String toolTip = new CommonFunction().getDomainName(request, "N");
%>
<c:if test="${not empty requestScope.uniBroswerList}">
    <c:forEach var="uniBroswerList" items="${requestScope.uniBroswerList}" varStatus="rowIndex">
    <c:set var="rowIndexValue" value="${rowIndex.index}"/> 
      <div class="ucod_Cnr" id="con-for-a">
        <%if(Integer.parseInt(pageContext.getAttribute("rowIndexValue").toString())+1==6){%><span id="loadAjaxDiv_<%=curPageNo%>"></span><%}%>     
        <div class="ucod_wrap">
          <div class="lft_pod">
            <c:if test="${not empty uniBroswerList.collegeLogo}"> 
              <a href="${uniBroswerList.collegeNameUrl}">
                <img class="pro_logo lazy-load" alt="${uniBroswerList.collegeNameDisplay}" title ="${uniBroswerList.collegeNameDisplay}" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' data-src='<%=CommonUtil.getImgPath("", Integer.parseInt(pageContext.getAttribute("rowIndexValue").toString()))%>${uniBroswerList.collegeLogo}'/>
              </a>
            </c:if>
          </div>
          <div class="rht_pod">
            <h2><a href="${uniBroswerList.collegeNameUrl}">${uniBroswerList.collegeNameDisplay}</a></h2>
            <c:if test="${not empty uniBroswerList.overallRating and uniBroswerList.overallRating gt 0 }">
                <span class="rhtod_text"><span>Overall rating</span> <span class="rat${uniBroswerList.overallRating} t_tip">
								          <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								          <span class="cmp">
								            <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									           <div class="line"></div>
								          </span>
								          <%--End of rating tool tip code--%>
							         </span>
                  <c:if test="${not empty uniBroswerList.reviewCount and uniBroswerList.reviewCount ne 0 }">
                     <a href="${uniBroswerList.reviewURL}">
                       ${uniBroswerList.reviewCount}
                       review<c:if test="${uniBroswerList.reviewCount gt 1}">s</c:if>                      
                     </a>
                  </c:if>
                </span>  
            </c:if>
          </div>
                    
          <c:if test="${not empty uniBroswerList.currentRank}">
            <%String cuYear1 = new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS");
            pageContext.setAttribute("cuYear1", cuYear1);
            String apostrophe = "'";
            pageContext.setAttribute("apostrophe", apostrophe);
            %>
            <div class="leag_sec trophy">
              <span class="fa fa-trophy t_tip">
							         <%--Trophy tooltip added by Prabha on 27_JAN_2016_REL--%>
                
               
									           <span class="cmp">
								              <div class="hdf5"><spring:message code="wuni.tooltip.wusca.keystats" arguments="${apostrophe},${cuYear1},${toolTip},''"/></div>
									             <div class="line"></div>
									           </span>
									       
                  ${uniBroswerList.currentRank}
                
								        <%--End of trophy tool tip code--%>
             
              <span class="lg_text">/</span>
              <span class="lg_text">${uniBroswerList.totalRank}</span>
               </span>
            </div>
          </c:if>
          
          <div class="sr_calbtn sr_calbtn1">
            <a title="${uniBroswerList.collegeNameDisplay} profile" href="${uniBroswerList.collegeNameUrl}" class="sr_pd">View profile<i class="fa fa-long-arrow-right"></i></a>
          </div>
        </div> 
      </div>
      <jsp:include page="/jsp/search/include/searchDrawAdSlots.jsp">  
        <jsp:param name="adSlotName" value="lb"/>
        <jsp:param name="position" value="${rowIndexValue}"/>
        <jsp:param name="pageNumber" value="<%=curPageNo%>"/>
        <jsp:param name="totalResults" value="<%=totalCount%>"/>
      </jsp:include>
    </c:forEach>

    <input type="hidden" id="curPageNo" value="<%=curPageNo%>" /> 
    <%if(!GenericValidator.isBlankOrNull(totalCount)) {
        results = Integer.parseInt(totalCount);
        totalPages   =  results/10;
        totalPages += (results % 10 > 0) ? 1 : 0;
        int i= Integer.parseInt(curPageNo) + 1; 
        if(i<=totalPages){%>           
          <div id="next_results_<%=i%>"></div>
       <%}  
      }%>
    <input type="hidden" id="totPageNo" value="<%=totalPages%>"/>      
    <%if(totalPages>1){%>  
      <div id="loader_text" class="ldr-con txt_cnr" style="display:none;">Loading more results..</div>
      <script type="text/javascript">loadAjaxPaginationRes();</script>
      <div class="ldr" id="loader" style="display:none;">
        <img src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>' />
      </div>
    <%}%>
</c:if>

<c:if test="${empty requestScope.uniBroswerList and not empty requestScope.searchTxt}">
    Sorry we don't have any unis listed under ${requestScope.searchTxt}
</c:if>