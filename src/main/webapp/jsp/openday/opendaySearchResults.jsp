<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator,WUI.utilities.CommonFunction"%>

<%
  String urlmapping = request.getAttribute("urlmapping")!=null ? request.getAttribute("urlmapping").toString() : ""; 
  String odStatusPos = request.getAttribute("odStatusPos")!=null ? request.getAttribute("odStatusPos").toString() : "0";
  String totalPos = request.getAttribute("totalPos")!=null ? request.getAttribute("totalPos").toString() : "0";
  CommonUtil util = new CommonUtil();
  CommonFunction common = new CommonFunction();
  String eVName = common.getWUSysVarValue("WU_ENV_NAME");
  String httpStr = common.getSchemeName(request);
  String canonUrl = httpStr + "www.whatuni.com"+urlmapping+"/";
  String totalRecordCount = (String)request.getAttribute("totalRecordCount");
       totalRecordCount = (GenericValidator.isBlankOrNull(totalRecordCount))? "":totalRecordCount;
       String collegeName = (String)request.getAttribute("odcollegeName");  
       collegeName = (!GenericValidator.isBlankOrNull(collegeName))? collegeName: "";
  String location = request.getParameter("location");
         location = !GenericValidator.isBlankOrNull(location)?  util.toTitleCase(common.replaceHypenWithSpace(location)) : ""; 
  String collegeId = (String)request.getParameter("collegeid");  
         collegeId = (!GenericValidator.isBlankOrNull(collegeId))? collegeId: "0";
  String date = (String)request.getParameter("date");  
         date = (!GenericValidator.isBlankOrNull(date))? date: "";
  String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
  String openDayJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.opendaysearch.js");
  String openDayMainCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.styles.css", null);//getting css name dynamically by Sangeeth.S for 31_Jul_18 rel
  String openDayHeaderCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);  
  String domSpecificPath = common.getSchemeName(request)+ CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
  String openDayLandingJsName = CommonUtil.getResourceMessage("wuni.openday.landing.js", null);//getting Js name dynamically by Sangeeth.S for 20_Nov_18 rel
%>

<body class="odbnr_sr">
  <%--Header pod strat--%>
  <header class="md clipart">
    <div class="ad_cnr">
      <div class="content-bg">
        <div id="desktop_hdr">
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>                
      </div>      
    </div>
  </header> 
  <%--End of header section--%>
  <div class="container1">
    <div class="opd_sr fl">
      <div class="bcrmb_hd fl">
        <div class="ad_cnr">
          <div class="content-bg">
            <%--Breadcrumb section--%>
	    <jsp:include page="/seopods/breadCrumbs.jsp">
              <jsp:param name="pageName" value="OPEN_DAY_SEARCH"/>
            </jsp:include>
            <%--Breadcrumb section--%>
	    <div class="opuni_srch">
              <%--Top heading section--%>
              <c:if test="${empty requestScope.noResultsFlag}">
                <h1>
                  <%=totalRecordCount%> <%if(GenericValidator.isBlankOrNull(collegeName)){%> <spring:message code="uni.vitual.h1.text" arguments="U"/><c:if test="${requestScope.totalOpenDaysCount gt 1}">s</c:if><%}else{%>Open day<c:if test="${requestScope.totalOpenDaysCount gt 1}">s</c:if><%}%> 
                  <%if(!GenericValidator.isBlankOrNull((collegeName))){%>
                    for <%=collegeName%>
                  <%}if(!GenericValidator.isBlankOrNull((location))){%>
                    in  <%=location%>
                  <%}%>
                </h1>
              </c:if>
              <c:if test="${not empty requestScope.noResultsFlag}"><h1><%=totalRecordCount%> <spring:message code="uni.vitual.h1.text" arguments="U"/>s</h1></c:if>
              <%--Top heading section--%>
              <%--Delete fitler section--%>
	      <ul>
	        <c:if test="${not empty requestScope.odcollegeName}">
                    <li onclick="deleteFilterParams('college');"><span>${requestScope.odcollegeName}</span><span><i class="fa fa-times"></i></span></li>
                </c:if>
                <c:if test="${not empty requestScope.uniOpenDaysMnthList}">
                 <c:forEach var="openDaysMonthList" items="${requestScope.uniOpenDaysMnthList}">
                  <c:if test="${openDaysMonthList.openDayStatus eq 'Y'}">
	              <li onclick="deleteFilterParams('date', '${openDaysMonthList.openDayMonthYearDesc}');"><span>${openDaysMonthList.openDayMonthYear}</span><span><i class="fa fa-times"></i></span></li>
                    </c:if>
                  </c:forEach>
                </c:if>
                <c:if test="${not empty requestScope.selectedLocation}">
                    <li onclick="deleteFilterParams('location');"><span>${requestScope.selectedLocation}</span><span><i class="fa fa-times"></i></span></li>
                </c:if>
                <c:if test="${not empty requestScope.selectedStudyLevel}">
                    <li onclick="deleteFilterParams('qualification');"><span>${requestScope.selectedStudyLevel}</span><span><i class="fa fa-times"></i></span></li>
                </c:if>
	      </ul>
              <%--Delete fitler section--%>
	    </div>
	  </div>
        </div>
      </div>
      <%--Filter and center pod section--%>
      <div class="opd_res">
        <div class="ad_cnr">
          <div class="content-bg">
            <div class="ods_sticky">
              <jsp:include page="/jsp/openday/include/includeOpendaySearchFilter.jsp"/>
            </div>
            <jsp:include page="/jsp/openday/include/includeInteractionPod.jsp"/>
	  </div>
        </div>
        <%--Filter and center pod section--%>
      </div>
    </div>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
  </div>

<input type="hidden" id="totalPos" value="<%=totalPos%>"/>
<input type="hidden" id="odStatusPos" value="<%=odStatusPos%>"/>

</body>