<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.GlobalConstants" %>

<c:if test="${not empty locationDetailsList}">
<div class="opd_res">
  <div class="ad_cnr">
    <div class="content-bg">    
      <div class="odres_lst mt${empty contentSectionList and empty gallerySectionList ? '10' : '0'} map" id="location_id" data-id="ga_not_logged">
        <h3>Where you'll study</h3>
       <%-- Open days details pod1 --%>
        <c:forEach var="venueList" items="${locationDetailsList}">
        <div class="opd_uni_map">
         <c:if test="${not empty venueList.latitude and not empty venueList.longitude}"> 
          <div class="opdmap_lft">
            <c:if test="${not empty venueList.latitude}"><c:set var="latitude" value="${venueList.latitude}"/></c:if>
            <c:if test="${not empty venueList.longitude}"><c:set var="longitude" value="${venueList.longitude}"/></c:if>
            <a href="<%=GlobalConstants.MAP_SEARCH_URL%>${latitude}, ${longitude}" target="_blank" onclick="openDayProviderLandingGALogging('open_day_map', '');">
              <img alt='Mapbox map' src='https://api.mapbox.com/styles/v1/mapbox/streets-v10/static/pin-l+3bb2d0(${longitude},${latitude})/${longitude},${latitude},13,0/600x600?access_token=pk.eyJ1IjoiaG90Y291cnNlc2ludGwiLCJhIjoiY2s2MjFkeHlxMDhwMDN0cXd2cTlqb3dlZiJ9.L-TXEMvZMFKb5WfkuFfMEA'>
            </a>
           <%-- <img src="images/opd_map_smpl.png"> --%>
          </div>
         </c:if> 
	      <div class="opdmap_rit">
	        <div class="opd_uni_dtls">
	          <h4><c:if test="${not empty venueList.collegeNameDisplay}">${venueList.collegeNameDisplay}</c:if></h4>
		      <p><c:if test="${not empty venueList.addressLineOne}">${venueList.addressLineOne}</c:if></p>
		      <p><c:if test="${not empty venueList.addreeslineTwo}">${venueList.addreeslineTwo}</c:if></p> 
		      <p><c:if test="${not empty venueList.town}">${venueList.town}, City of</c:if></p>
		      <p><c:if test="${not empty venueList.postcode}">${venueList.postcode}</c:if></p>
		      <p>United Kingdom</p>
	        </div>
	  
            <div class="opd_uni_trst">	          
	          <c:if test="${venueList.isInLondon eq 'LONDON'}">
	            <c:if test="${not empty venueList.nearestTubeStation}">
		          <span class="bld_txt">Nearest tube station: </span>
		          <span class="nrl_txt">
		            ${venueList.nearestTubeStation}&nbsp; ${venueList.distanceFromTubeStation} miles away
		          </span>
		        </c:if>
		        <c:if test="${not empty venueList.nearestTrainStation}">
                  <span class="bld_txt">Nearest train station: </span>
                  <span class="nrl_txt">
                    ${venueList.nearestTrainStation}&nbsp; ${venueList.distanceFromTrainStation} miles away
                  </span>
		        </c:if>
		      </c:if>
		      <c:if test="${venueList.isInLondon ne 'LONDON'}">
		        <c:if test="${not empty venueList.nearestTrainStation}">
		          <span class="bld_txt">Nearest train station: </span>
		          <span class="nrl_txt">
		            ${venueList.nearestTrainStation}&nbsp; ${venueList.distanceFromTrainStation} miles away
		          </span>
		        </c:if>
		      </c:if>
	        </div>
							
			<c:if test="${venueList.cityGuideFlag eq 'Y'}">		
            <div class="opd_uni_tksd">
	          <h5>Thinking of studying in ${venueList.cityGuideLocation}?</h5>
		      <p>Check out our</p>
	          <a href="${venueList.cityGuideUrl}" onclick="openDayProviderLandingGALogging('open_day_city_guide', '');">${venueList.cityGuideLocation} city guide</a>
	        </div>
	        </c:if>
	      </div>
        </div>
        </c:forEach>
      <%-- Open days details pod1 --%>
      </div>    
    </div>
  </div>
</div>
</c:if>