<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction,WUI.utilities.GlobalConstants,com.layer.util.SpringConstants"%>

<%
  String videoPlayIcon = CommonUtil.getImgPath("/wu-cont/images/cpc_play_icon.svg",0);
  CommonUtil util = new CommonUtil();
  String lazyLoadNotDoneFlag = GlobalConstants.LAZY_LOAD_FLAG;
  String onePixelImgUrl = CommonUtil.getImgPath(GlobalConstants.ONE_PIXEL_IMG_PATH, 1);
  int imageCount = 0;
%>

<c:if test="${not empty gallerySectionList}">
  <div class="opd_res">
    <div class="ad_cnr">
      <div class="content-bg">     
        <div class="odres_lst glry" id="gallery_id" data-id="ga_not_logged">
          <%-- <h3>About ${collegeNameDisplayGallery}</h3> --%>
          <%-- Open days details pod1 --%>
          <c:choose>
		    <c:when test="${fn:length(gallerySectionList) gt 1}">
              <div class="fl_w100">
            </c:when>
            <c:otherwise>
              <div class="fl_w100 slider-item po_rel">
            </c:otherwise>
           </c:choose>
            <c:choose>
		      <c:when test="${fn:length(gallerySectionList) gt 1}">
               <!-- slider start -->
                <div class='slider' id="open_day_gallery_slider">
	              <div class='slider-items-container'>
	                <c:forEach var="gallerySectionList" items="${gallerySectionList}" varStatus="i">
	                  <c:set var="imgPositions" value="${i.index}" > </c:set>
                      <%imageCount = Integer.parseInt(String.valueOf(pageContext.getAttribute("imgPositions").toString()));%> 
	                  <c:if test="${gallerySectionList.mediaType eq 'VIDEO'}">
	                    <div class='slider-item'>
	                      <c:set var="thumbnailPath" value="${gallerySectionList.thumbnailPath}"/>
                          <%
                            String thumbnailPath = (String)pageContext.getAttribute("thumbnailPath"); 
                            String thumbNailPathSrc = (String)pageContext.getAttribute("thumbnailPath");
                          %>
		                  <div class="vide_icn">
		                    <img class="pl_icn_img" src="<%=videoPlayIcon%>" id="openday_play_icon_${gallerySectionList.mediaId}" onclick="clickPlayAndPause('openday_gallery_video_${gallerySectionList.mediaId}', event, 'thumbnailImg_${gallerySectionList.mediaId}', 'openday_play_icon_${gallerySectionList.mediaId}');" alt="play icon">
		                  </div>
		              
		                  <c:choose>		                    
                            <c:when test="${gallerySectionList.contentHubFlag eq 'Y'}">
                              <% thumbNailPathSrc = util.getImageUrl(request, thumbnailPath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>                               
                            </c:when>
                            <c:otherwise>
                              <% thumbNailPathSrc = util.getImageUrl(request, thumbnailPath, SpringConstants.OPENDAY_MOB_IMGSIZE, SpringConstants.OPENDAY_TAB_IMGSIZE, SpringConstants.OPENDAY_DESKTOP_IMGSIZE); %>                               
                            </c:otherwise>
                          </c:choose>
		                  <c:set var="thumbNailSrc" value="<%=(imageCount != 0)?onePixelImgUrl:thumbNailPathSrc%>"/>
	                      <c:choose>		                    
                            <c:when test="${gallerySectionList.contentHubFlag eq 'Y'}">
                              <% thumbnailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>
                              <img id="thumbnailImg_${gallerySectionList.mediaId}" width="100%" height="auto" src="${thumbNailSrc}" data-src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" alt="open day gallery">
                            </c:when>
                            <c:otherwise>
                              <% thumbnailPath = util.getImageUrl(request, thumbnailPath, SpringConstants.OPENDAY_MOB_IMGSIZE, SpringConstants.OPENDAY_TAB_IMGSIZE, SpringConstants.OPENDAY_DESKTOP_IMGSIZE); %>
                              <img id="thumbnailImg_${gallerySectionList.mediaId}" width="100%" height="auto" src="${thumbNailSrc}" data-src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-rjs="Y" data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>" alt="open day gallery">
                            </c:otherwise>
                          </c:choose>	
	              
		                  <video preload="none" data-media-name="${gallerySectionList.mediaName}" 
		                         id="openday_gallery_video_${gallerySectionList.mediaId}" 
		                         width="100%" height="100%" 
		                         onplay="videoPlayedDurationPercentageForOpenday(this);" 
		                         controls style="display:none" 
		                         class="v_wid"  
		                         controlslist="nodownload noremoteplayback" 
		                         disablepictureinpicture="">
		                        <source src="${gallerySectionList.mediaPath}" type="video/mp4">
		                  </video> 
		                </div>
		              </c:if>
  
		              <c:if test="${gallerySectionList.mediaType eq 'IMAGE'}">  
		                <div class='slider-item'>
		                  <c:set var="imagePath" value="${gallerySectionList.mediaPath}"/>
                          <%
                            String imagePath = (String)pageContext.getAttribute("imagePath"); 
                             String imageSrcPath = (String)pageContext.getAttribute("imagePath");
                           %>
                           <c:choose>                            
                             <c:when test="${gallerySectionList.contentHubFlag eq 'Y'}">
                               <% imageSrcPath = util.getImageUrl(request, imagePath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>                               
                             </c:when>
                             <c:otherwise>
                               <% imageSrcPath = util.getImageUrl(request, imagePath, SpringConstants.OPENDAY_MOB_IMGSIZE, SpringConstants.OPENDAY_TAB_IMGSIZE, SpringConstants.OPENDAY_DESKTOP_IMGSIZE); %>                               
                             </c:otherwise>
                           </c:choose>
                           <c:set var="imageSrc" value="<%=(imageCount != 0)?onePixelImgUrl:imageSrcPath%>"/>                  	         
		                   <c:choose>                          
                             <c:when test="${gallerySectionList.contentHubFlag eq 'Y'}">
                               <% imagePath = util.getImageUrl(request, imagePath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>
                               <img src="${imageSrc}" data-src="<%=CommonUtil.getImgPath(imagePath, 1)%>" alt="open day gallery section">
                             </c:when>
                             <c:otherwise>
                               <% imagePath = util.getImageUrl(request, imagePath, SpringConstants.OPENDAY_MOB_IMGSIZE, SpringConstants.OPENDAY_TAB_IMGSIZE, SpringConstants.OPENDAY_DESKTOP_IMGSIZE); %>
                               <img src="${imageSrc}" data-src="<%=CommonUtil.getImgPath(imagePath, 1)%>" data-rjs="Y"  data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>" alt="open day gallery section">
                             </c:otherwise>
                           </c:choose>
                         </div>
		               </c:if>
		             </c:forEach>									
                   </div>	
	               <div class='controls'>
	                  <a href="#" class='prev-btn'><span class="crsl_lftarw"></span></a>
		              <a href="#" class='next-btn'><span class="crsl_ritarw"></span></a>
	              </div>
	              <div class='dots'></div>	
	            </div>	
	             <!-- slider end -->
	          </c:when>
	      
	          <c:otherwise>
                <c:forEach var="gallerySectionList" items="${gallerySectionList}" varStatus="i">
                  <c:if test="${gallerySectionList.mediaType eq 'IMAGE'}">
                	<c:set var="imagePath" value="${gallerySectionList.mediaPath}"/>
                    <%String imagePath = (String)pageContext.getAttribute("imagePath");%>
                    <c:choose>
                      <c:when test="${gallerySectionList.contentHubFlag eq 'Y'}">
                        <% imagePath = util.getImageUrl(request, imagePath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>
                        <img width="100%" height="auto" src="<%=CommonUtil.getImgPath(imagePath, 1)%>" alt="open day gallery"></img>
                      </c:when>
                      <c:otherwise>
                        <% imagePath = util.getImageUrl(request, imagePath, SpringConstants.OPENDAY_MOB_IMGSIZE, SpringConstants.OPENDAY_TAB_IMGSIZE, SpringConstants.OPENDAY_DESKTOP_IMGSIZE); %>
                        <img width="100%" height="auto" src="<%=CommonUtil.getImgPath(imagePath, 1)%>" alt="open day gallery"></img>
                      </c:otherwise>
                    </c:choose>        
			      </c:if>
		          <c:if test="${gallerySectionList.mediaType eq 'VIDEO'}">
	                <c:if test="${not empty gallerySectionList.mediaPath}">
		              <c:set var="thumbnailPath" value="${gallerySectionList.thumbnailPath}"/>
                      <%String thumbnailPath = (String)pageContext.getAttribute("thumbnailPath");%>
                      <div class="vide_icn">
                        <img class="pl_icn_img" src="<%=videoPlayIcon%>" id="openday_play_icon_${gallerySectionList.mediaId}" onclick="clickPlayAndPause('openday_gallery_video_${gallerySectionList.mediaId}', event, 'thumbnailImg_${gallerySectionList.mediaId}','openday_play_icon_${gallerySectionList.mediaId}');" alt="play icon">
                      </div>
                      <c:choose>
                        <c:when test="${gallerySectionList.contentHubFlag eq 'Y'}">
                          <% thumbnailPath = util.getImageUrl(request, thumbnailPath, GlobalConstants._320PX, GlobalConstants._768PX, GlobalConstants._768PX); %>
                          <img  id="thumbnailImg_${gallerySectionList.mediaId}" width="100%" height="auto" src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" alt="open day gallery">
                        </c:when>
                        <c:otherwise>
                          <% thumbnailPath = util.getImageUrl(request, thumbnailPath, SpringConstants.OPENDAY_MOB_IMGSIZE, SpringConstants.OPENDAY_TAB_IMGSIZE, SpringConstants.OPENDAY_DESKTOP_IMGSIZE); %>
                          <img id="thumbnailImg_${gallerySectionList.mediaId}" width="100%" height="auto" src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-src="<%=CommonUtil.getImgPath(thumbnailPath, 1)%>" data-rjs="Y" data-lazyLoad-flag="<%=lazyLoadNotDoneFlag%>" alt="open day gallery">
                        </c:otherwise>
                      </c:choose>       
                           
                      <video preload="none" data-media-name="${gallerySectionList.mediaName}" 
		                     id="openday_gallery_video_${gallerySectionList.mediaId}" 
		                     width="100%" height="100%" 
		                     onplay="videoPlayedDurationPercentageForOpenday(this);" 
		                     controls style="display:none" 
		                     class=""  
		                     controlslist="nodownload noremoteplayback" 
		                     disablepictureinpicture="">
		               <source src="${gallerySectionList.mediaPath}" type="video/mp4">
		            </video>                 
                  </c:if>
	   		    </c:if>        
	   		  </c:forEach>
	        </c:otherwise>
	      </c:choose>
        </div>
       <%-- Open days details pod1 --%>
        <div class="pr_pagn"> </div>
      </div>
    </div>
  </div>
</div>
</c:if>