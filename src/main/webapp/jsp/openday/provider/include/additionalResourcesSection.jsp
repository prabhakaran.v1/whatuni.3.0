<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>      
<%@page import="WUI.utilities.CommonUtil"%>

<c:if test="${not empty additionResourcesList}">
<div class="opd_res">
  <div class="ad_cnr">
    <div class="content-bg">
      <div class="odres_lst mt0 add_rs" id="addition_res_id" data-id="ga_not_logged">
        <h3>Additional Resources</h3>
        <p class="minfo">Find out more about the university you're visiting</p>
        <!-- Open days details pod1 -->
        <div class="opd_uni_adrs">
          <c:forEach var="additionResourcesList" items="${additionResourcesList}">
	      <a href="${additionResourcesList.pdfPath}" class="pdf_lnk" target="_blank" onclick="openDayProviderLandingGALogging('open_day_additional_res', '${additionResourcesList.pdfName}');">
	        <div class="add_rs_pdf">
	          <div class="pdf_icn"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/opdtwnt_pdf.svg", 0)%>" alt="open day pdf"></div>
		      <div class="pdf_txt">${additionResourcesList.pdfName}</div>
	        </div>
	      </a>
	      </c:forEach>
        </div>
      <%-- Open days details pod1 --%>
       <div class="pr_pagn"> </div>
     </div>
   </div>
  </div>
</div>
</c:if>
