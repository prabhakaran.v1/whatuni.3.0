<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<c:if test="${not empty testimonialSectionList}">
  <div class="opd_res">
    <div class="ad_cnr">
      <div class="content-bg">
        <div class="odres_lst mt0 tsm" id="testimonials_id" data-id="ga_not_logged">
          <h3>Testimonials</h3>
		  <div class="opd_uni_tsm">
		    <c:forEach var="testimonialSectionList" items="${testimonialSectionList}" varStatus="i">
		      <div class="opdtsm_${i.index}">
			    <span class="qutn"></span>
				<p class="itlc">${testimonialSectionList.sectionValue}</p>
				<p class="rvnm">${testimonialSectionList.updatedDate}, ${testimonialSectionList.title}</p>
			  </div>
			</c:forEach>
		  </div>
          <div class="pr_pagn"> </div>
        </div>
      </div>
    </div>
  </div>
</c:if>
