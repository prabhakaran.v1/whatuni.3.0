<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>      
<div class="opd_res bx_sdw">
  <div class="ad_cnr">
    <div class="content-bg">
      <div class="odres_lst oprdo opdres_bkbtnlst">
        <div class="opdbkevnt_tabhead">
          <h3>Select  an ${sessionScope.cpeQualificationNetworkId eq "2" ? "undergraduate" : " postgraduate"} event</h3>
          <ul id="switchTab" class="opdBookTabLst">
            <li id="undergraduate" class='${sessionScope.cpeQualificationNetworkId eq "2" ? "opdBookTabActv" : ""}'><a href="javascript:void(0);">Undergraduate</a></li>
            <li id="postgraduate" class='${sessionScope.cpeQualificationNetworkId eq "3" ? "opdBookTabActv" : ""}' ><a href="javascript:void(0);">Postgraduate</a></li>
          </ul>
        </div>
        <div class="opd_evt_tlt dskp">
          <ul>
            <li>Date</li>
            <li>Location</li>
          </ul>
        </div>
        <!-- Open days details pod1 -->
        <jsp:include page="/jsp/openday/provider/include/openDayEventPod.jsp"/>
        <!-- Open days details pod1 -->
        <c:if test="${moreOpenDayFlag eq 'Y'}">
          <div class="fl_w100"><a href="javascript:void(0);" onclick="ajaxOpenDayLightBox(${collegeId},${totalOpenDayCount},${sessionScope.cpeQualificationNetworkId});openDayProviderLandingGALogging('view_more_open_day');" class="view_all_btn">VIEW ${totalOpenDayCount-3} MORE EVENTS</a></div>
        </c:if>
      </div>
    </div>
  </div>
</div>