<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil"%>   

<div class="bcrmb_hd fl">
  <div class="ad_cnr">
     <div class="content-bg">
       <jsp:include page="/seopods/breadCrumbs.jsp">
          <jsp:param name="pageName" value="OPEN_DAY_PROVIDER_LANDING_PAGE"/>
       </jsp:include>
       
       <c:if test="${not empty uniInforList}">
       <c:forEach var="uniInfoHeaderSectionList" items="${requestScope.uniInforList}">
       <div class="opuni_srch det_res">
       <div class="dr_col1 fl">
          <div class="drc_lft fl"> 
            <a href="${uniInfoHeaderSectionList.collegeLandingUrl}" onclick="openDayProviderLandingGALogging('institution_click_header')">
              <img class="pro_logo" src="<%=CommonUtil.getImgPath("",0)%>${uniInfoHeaderSectionList.collegeLogo}" alt="${uniInfoHeaderSectionList.collegeNameDisplay}"> 
            </a>
          </div>
          <div class="drc_rht fl">
            <h1>
              <a href="${uniInfoHeaderSectionList.collegeLandingUrl}" alt="${uniInfoHeaderSectionList.collegeNameDisplay}"></a>
                 Virtual tours and events at ${uniInfoHeaderSectionList.collegeNameDisplay}
            </h1>
            <c:if test="${uniInfoHeaderSectionList.reviewCount ne 0 and not empty uniInfoHeaderSectionList.reviewCount}">
            <div class="rate_new">
              <span class="rat${uniInfoHeaderSectionList.overallRating} t_tip">
	            <span class="cmp" >
	              <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
	              <div class="line"></div>
	            </span> 
              </span>
              <span class="op_rate_nw">(${uniInfoHeaderSectionList.overallRatingExact})</span>
              <a class="link_blue nowc" href="${uniInfoHeaderSectionList.uniReviewsURL}" title="Reviews at ${uniInfoHeaderSectionList.collegeNameDisplay}" onclick="openDayProviderLandingGALogging('institution_review_header')">
                 ${uniInfoHeaderSectionList.reviewCount} 
                 <c:if test="${uniInfoHeaderSectionList.reviewCount eq 1}"> review</c:if>
                 <c:if test="${uniInfoHeaderSectionList.reviewCount gt 1}"> reviews</c:if>
               </a>
            </div>  
            </c:if>                     
          </div>
        </div>
      </div>
      </c:forEach>
      </c:if>
    </div>
  </div>
</div>
