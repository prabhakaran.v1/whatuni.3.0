<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonFunction, WUI.utilities.GlobalFunction"%>

<%
String gaCollegeName = "";
CommonFunction common = new CommonFunction();
GlobalFunction globalFn = new GlobalFunction();
%>

<c:if test="${not empty bookingFormDetailsList}">
  <c:forEach var="bookingFormDetailsList" items="${bookingFormDetailsList}">
   <c:if test="${not empty bookingFormDetailsList.subOrderItemId}">
    <c:if test="${not empty bookingFormDetailsList.bookingFormFlag}">
      <div class="opd_btm_sticky">
        <div class="opd_btm">
           <div class="opd_btm_txt"><!-- <p>TEXT?</p> --></div>
	      <div class="opd_btm_btn">
	    <%--<c:choose>
	        <c:when test="${bookingFormFlag eq 'Y'}">
	          <a href="javascript:void(0);" onclick="openDayBookingFormUrl();openDayProviderLandingGALogging('book_open_day_btn', '');">BOOK OPEN DAY</a>
	        </c:when>
	        <c:otherwise>
	          <c:if test="${not empty bookingUrl}"><a href="${bookingUrl}" target="_blank">BOOK OPEN DAY</a></c:if>
	        </c:otherwise>
	      </c:choose>--%>
	    
	        <c:if test="${bookingFormDetailsList.bookingFormFlag eq 'N'}">
	          <c:if test="${not empty bookingFormDetailsList.bookingUrl}">
	            <c:choose>
	              <c:when test="${bookingFormDetailsList.eventCategoryId eq 6}">
	                <c:set var="gaEventAction" value="Virtual"/>
	              </c:when>
	              <c:when test="${bookingFormDetailsList.eventCategoryId eq 7}">
	                <c:set var="gaEventAction" value="Online"/>
	              </c:when>
	              <c:otherwise>
	                <c:set var="gaEventAction" value="Physical"/>
	              </c:otherwise>
	            </c:choose>	     
	            <c:if test="${not empty bookingFormDetailsList.collegeName}">
             <c:set var="collegeName" value="${bookingFormDetailsList.collegeName}"/>
              <%
                gaCollegeName= common.replaceSpecialCharacter(globalFn.getReplacedString((String)pageContext.getAttribute("collegeName")));
                pageContext.setAttribute("gaCollegeName", gaCollegeName.trim());
              %>
           
             </c:if>      
	            <a href="${bookingFormDetailsList.bookingUrl}" 
	               onclick="GAEventTracking('open-days-type', '${gaEventAction}', '${gaCollegeName}');
	                        GAEventTracking('open-days', 'reserve-place', '${gaCollegeName}','1');
	                        GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${gaCollegeName}', ${bookingFormDetailsList.websitePrice}); 
	                        addOpenDaysForReservePlace('${bookingFormDetailsList.openDay}', '${bookingFormDetailsList.openMonth}', '${bookingFormDetailsList.collegeId}', '${bookingFormDetailsList.subOrderItemId}', '${bookingFormDetailsList.networkId}');
	                        cpeODReservePlace(this,'${bookingFormDetailsList.collegeId}','${bookingFormDetailsList.subOrderItemId}','${bookingFormDetailsList.networkId}','${bookingFormDetailsList.bookingUrl}');" 
	                target="_blank">
	                <spring:message code="book.open.day.button.text"/>
	            </a>
	          </c:if>
	        </c:if>  
	      </div>
        </div>
      </div>
    </c:if>
    </c:if>
  </c:forEach>
</c:if>