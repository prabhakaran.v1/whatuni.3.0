<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>   
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>    
<div class="cntr">
   <c:if test="${not empty uniOpenDaysList}">
      <c:forEach var="uniOpenDaysList" items="${uniOpenDaysList}" varStatus="i" >
         <c:set var="index" value="${i.index}"/>
         <c:if test="${not empty param.collegeId}">
            <c:set var="lightBox" value="_lightBox"/>
         </c:if>
         <c:if test="${not empty uniOpenDaysList.qualType}">
            <c:set var="qualiticationType" value="${fn:split(uniOpenDaysList.qualTypeGA,' ')}" />
         </c:if>
         <label for="opt${index}" class="radio ${uniOpenDaysList.selectedFlag eq 'Y' ? 'active' : ''}" id="eventLabelId_${index}" data-id="${index}${lightBox}" data-lightBoxId="select_open_day${lightBox}" data-gaEventTime="${uniOpenDaysList.startDate}/${uniOpenDaysList.startTime}" data-qualType="${qualiticationType[0]}">
            <input type="radio" ${uniOpenDaysList.selectedFlag eq 'Y' ? 'checked' : ''} name="openDayEvents" value="${uniOpenDaysList.eventItemId}" id="openDayEvent_${index}${lightBox}" class="hidden"/>
            <span class="label"></span>
            <ul>
               <c:if test="${not empty uniOpenDaysList.startDate}">
                  <li>
                     <p class="mob fnt14">Date</p>
                     <p>
                        ${uniOpenDaysList.startDate} 
                        <c:if test="${not empty uniOpenDaysList.startTime}">at ${uniOpenDaysList.startTime}</c:if>
                     </p>
                  </li>
               </c:if>
               <c:if test="${not empty uniOpenDaysList.venue}">
                  <li>
                     <p class="mob fnt14">Location</p>
                     <p>${uniOpenDaysList.venue}</p>
                  </li>
               </c:if>
               <c:if test="${not empty uniOpenDaysList.subOrderItemId}">
                  <c:if test="${not empty uniOpenDaysList.bookingFormFlag}">
                     <c:choose>
                        <c:when test="${uniOpenDaysList.eventCategoryId eq 6}">
                           <c:set var="gaEventAction" value="Virtual"/>
                        </c:when>
                        <c:when test="${uniOpenDaysList.eventCategoryId eq 7}">
                           <c:set var="gaEventAction" value="Online"/>
                        </c:when>
                        <c:otherwise>
                           <c:set var="gaEventAction" value="Physical"/>
                        </c:otherwise>
                     </c:choose>
                     <c:if test="${uniOpenDaysList.bookingFormFlag eq 'N'}">
                        <c:if test="${not empty uniOpenDaysList.bookingUrl}">
                           <li class="evntBookBtn">
                              <a href="javascript:void(0);" data-url="${uniOpenDaysList.bookingUrl}" data-eventAction="${gaEventAction}" data-collegeName="${gaCollegeName}" data-websitePrice="${uniOpenDaysList.websitePrice}" id="bookEvent_${index}"
                                 data-openDay="${uniOpenDaysList.openDay}" data-openMonth="${uniOpenDaysList.openMonth}" data-collegeId="${uniOpenDaysList.collegeId}" data-subOrderItemId="${uniOpenDaysList.subOrderItemId}" data-networkId="${uniOpenDaysList.networkId}"
                                 class="evntBookBtnLnk ${uniOpenDaysList.selectedFlag ne 'Y'  ? 'bkopnday_disabld' : ''}"><spring:message code="book.open.day.button.text" /></a>
                           </li>
                        </c:if>
                     </c:if>
                  </c:if>
               </c:if>
            </ul>
         </label>
         <%-- <c:if test="${index eq 0}">
            <script type="text/javascript">
               var jq = jQuery.noConflict();
               jq(window).load(function() {
                 openDayProviderLandingGALogging('select_open_day', '', '${uniOpenDaysList.startDate}/${uniOpenDaysList.startTime}', '${qualiticationType[0]}');
               });
            </script> 
            </c:if> --%>
         <c:if test="${uniOpenDaysList.selectedFlag eq 'Y'}">
            <input type="hidden"  value="${uniOpenDaysList.startDate}/${uniOpenDaysList.startTime}" id="gaEventTime"/>
            <input type="hidden"  value="${qualiticationType[0]}" id="gaQualType"/>    
         </c:if>
      </c:forEach>
   </c:if>
   <c:if test="${empty uniOpenDaysList}">
      <label for="opt1" class="radio no_evntlbl">
         <p>No ${sessionScope.cpeQualificationNetworkId eq '2' ? 'undergraduate' : 'postgraduate'} open days available at this university</p>
      </label>
   </c:if>
</div>