<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<c:if test="${not empty contentSectionList}">
  <div class="opd_res">
    <div class="ad_cnr">
      <div class="content-bg">
        <div class="odres_lst mt0 lplst mrgt56p" id="content_sec_parent_id">
          <h3>About this event<c:if test="${empty selectedOpenDate}">:</c:if>
            <c:if test="${not empty selectedOpenDate}">
             <span class="headEvntSelDte">(Selected - ${selectedOpenDate})</span>
            </c:if>
            <c:if test="${empty selectedOpenDate}">
             <span class="headEvntSelDte">Virtual event</span>
            </c:if>
          </h3>
          <c:forEach var="contentSectionList" items="${contentSectionList}" varStatus="i">
            <div class="opd_ctils">
              <div class="opd_lft_ctils">
                <c:if test="${not empty contentSectionList.sectionName}">
                  <c:set var="contentSectionIcon" value="${fn:replace(fn:toLowerCase(contentSectionList.sectionName), ' ', '_')}"/>
                </c:if>
                <div class="opd_ctils_icn ${contentSectionIcon}"></div>
              </div>
              <div class="opd_rit_ctils" id="content_section_${i.index}" data-id="ga_not_logged" data-value="${contentSectionList.sectionName}">
                <h3>${contentSectionList.sectionName}</h3>
                <ul>
                  <c:if test="${not empty contentSectionList.contentSectionBulletValuesList}">
                    <c:forEach var="contentSectionBulletValuesList" items="${contentSectionList.contentSectionBulletValuesList}">
                      <li>${contentSectionBulletValuesList.sectionValue}</li>
                    </c:forEach>
                  </c:if>
                </ul>
              </div>
            </div>
          </c:forEach>
        </div>
      </div>
    </div>
  </div>
</c:if>