<%-- light box start --%>
<div class="rvbx_shdw"></div>
<div class="revcls">
  <a href="javascript:void(0);" id="openDayCloseButton" onclick="closeLigBox('opendayLightBox')" class="close">
    <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <defs>
        <path d="M18.7,5.3 C18.3,4.9 17.7,4.9 17.3,5.3 L12,10.6 L6.7,5.3 C6.3,4.9 5.7,4.9 5.3,5.3 C4.9,5.7 4.9,6.3 5.3,6.7 L10.6,12 L5.3,17.3 C4.9,17.7 4.9,18.3 5.3,18.7 C5.5,18.9 5.7,19 6,19 C6.3,19 6.5,18.9 6.7,18.7 L12,13.4 L17.3,18.7 C17.5,18.9 17.8,19 18,19 C18.2,19 18.5,18.9 18.7,18.7 C19.1,18.3 19.1,17.7 18.7,17.3 L13.4,12 L18.7,6.7 C19.1,6.3 19.1,5.7 18.7,5.3 Z" id="path-1"></path>
      </defs>
      <g id="Icon/Close" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <mask id="mask-2" fill="#ffffff">
          <use xlink:href="#path-1"></use>
        </mask>
        <g id="Mask" fill-rule="nonzero"></g>
        <g id="Mixin/Fill/Black" mask="url(#mask-2)" fill="#ffffff">
          <rect id="Rectangle" x="0" y="0" width="24" height="24"></rect>
        </g>
      </g>
    </svg>
  </a>
</div>
<div class="rvbx_cnt">
  <div class="rev_lst">
    <div class="fix_hd po_rel">
      <h3>${displayOpenDayCount} Virtual tours and events</h3>
      <%-- <p>Choose an open day</p>--%>
    </div>
    <div class="lbx_scrl bx_size">
      <!-- radio start -->
      <div class="opd_res">
        <div class="ad_cnr">
          <div class="content-bg">
            <div class="odres_lst lx_wrap">
              <div class="opd_evt_tlt dskp">
                <ul>
                  <li>When</li>
                  <li>Event</li>
                  <li>Location</li>
                </ul>
              </div>
              <jsp:include page="/jsp/openday/provider/include/openDayEventLightBox.jsp">
                <jsp:param value="${collegeId}" name="collegeId"/>
              </jsp:include>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="sltbtn_wrap">
      <div class="sltbtn_cnt">
        <a href="javascript:void(0);" id="selectButton" onclick="openDayPageUrl();" class="disab_btn slct_btn">SELECT</a>
      </div>
    </div>
  </div>
</div>
<%-- light box end --%>