<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>       
<div class="cntr">
   <c:if test="${not empty uniOpenDaysList}">
      <c:forEach var="uniOpenDaysList" items="${uniOpenDaysList}" varStatus="i" >
         <c:set var="index" value="${i.index}"/>
         <c:if test="${not empty param.collegeId}">
            <c:set var="lightBox" value="_lightBox"/>
         </c:if>
         <c:if test="${not empty uniOpenDaysList.qualType}">
            <c:set var="qualiticationType" value="${fn:split(uniOpenDaysList.qualTypeGA,' ')}" />
         </c:if>
         <label for="opt${index}" class="radio" id="eventLabelId_${index}${lightBox}" data-id="${index}${lightBox}" data-lightBoxId="select_open_day${lightBox}" data-gaEventTime="${uniOpenDaysList.startDate}/${uniOpenDaysList.startTime}" data-qualType="${qualiticationType[0]}">
            <input type="radio" name="openDayEvents" value="${uniOpenDaysList.eventItemId}" id="openDayEvent_${index}${lightBox}" class="hidden"/>
            <span class="label"></span>
            <ul>
               <c:if test="${not empty uniOpenDaysList.startDate}">
                  <li>
                     <p class="mob fnt14">When</p>
                     <p>
                        ${uniOpenDaysList.startDate} 
                        <c:if test="${not empty uniOpenDaysList.startTime}">at ${uniOpenDaysList.startTime}</c:if>
                     </p>
                  </li>
               </c:if>
               <c:if test="${not empty uniOpenDaysList.qualType}">
                  <li>
                     <p class="mob fnt14">Event</p>
                     <p>${uniOpenDaysList.qualType}</p>
                  </li>
               </c:if>
               <c:if test="${not empty uniOpenDaysList.venue}">
                  <li>
                     <p class="mob fnt14">Location</p>
                     <p>${uniOpenDaysList.venue}</p>
                  </li>
               </c:if>
            </ul>
         </label>
      </c:forEach>
      <c:if test="${not empty param.collegeId}">
         <input type="hidden"  value="" id="openDayEventsEventTime_lightBox"/>
      </c:if>
   </c:if>
</div>