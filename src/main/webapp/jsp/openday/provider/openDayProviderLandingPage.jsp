<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil"%>
<body class="odbnr_pl">
 <div id="loaderTopNavImg" class="cmm_ldericn zindx" style="display:none;"><img alt="loading" src="<%=CommonUtil.getImgPath("/wu-cont/images/hm_ldr.gif",1)%>"></div>  
  <div class="opd_lpnw">
    <header class="clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>
        </div>
      </div>
    </header>
    <%-- Opendays Redesign Code --%>
    <div class="opd_det fl no_virt">
      <jsp:include page="/jsp/openday/provider/include/openDayHeaderSection.jsp"/>
      <jsp:include page="/jsp/openday/provider/include/selectOpenDayPod.jsp"/>
      <jsp:include page="/jsp/openday/provider/include/contentSection.jsp"/>
      <jsp:include page="/jsp/openday/provider/include/gallerySection.jsp"/>
      <jsp:include page="/jsp/openday/provider/include/locationPod.jsp"/>
      <jsp:include page="/jsp/openday/provider/include/testimonialsSection.jsp"/>
      <jsp:include page="/jsp/openday/provider/include/additionalResourcesSection.jsp"/>
    </div>
    <jsp:include page="/jsp/openday/provider/include/stickyPod.jsp"/>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
    <input type="hidden" id ="collegeId" name="collegeId" value ="${collegeId}"/>
    <input type="hidden" id ="collegeName" name="collegeName" value="${collegeName}"/>
    <input type="hidden" id ="collegeNameGA" name="collegeNameGA" value="${hitbox_college_name}"/>
    <input type="hidden" id ="openDayProviderLandingURL" value="${openDayProviderLandingURL}"/>
    <input type="hidden" id ="userId" name="userId" value="${cDimUID}"/>
    <input type="hidden" id ="eventTime" name="eventTime" value="${eventTime}"/>
    <input type="hidden" id ="selectedEventId" name="selectedEventId"/>
    <!-- <script type="text/javascript">
      var jq = jQuery.noConflict();
      jq(window).scroll(function() {
        opendayProviderLandinOnScrollEvent();  	
      });
      </script> -->
  </div>
  <c:if test="${not empty collegeId}">
    <script type="text/javascript">providerPageViewLogging('${collegeId}','PROVIDER_OPENDAYS_VIEW');</script> 
  </c:if>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/cpc/cpc_slider.js"></script>
</body>