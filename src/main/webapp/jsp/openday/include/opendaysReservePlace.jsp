<%@page import="WUI.utilities.CommonUtil"%> 
<div class="comLgh" id="lbDivNew">
  <div class="fcls nw">
    <a class="" onclick="javascript:closeOpenDaydRes()"><i class="fa fa-times"></i></a>
  </div>   
  <div class="pform nlr bgw opday" id="lblogin">    
    <div class="reg-frm">
      <div class="reg-frmt bgw cf">
        <h1 class="mb0">Woah, there!</h1>
        <h2 class="brbt" >Can't find the open day you're looking for?</h2>
        <div class="borderbot fl  mb22"></div>
        <p class="comptx1 mb15 ">Tell which unis you're interested in and we'll mail you when an open day comes up...</p>
        <div class="w100p ">
          <div class="eml-ads fl">              
            <input type="text" name="uniName" id="uniName" value="Enter uni name" autocomplete="off" 
              onkeydown="clearUniNAME(event,this);" 
              onkeyup="autoCompleteResUniNAME(event,this)"  
              onclick="clearUniDefaultTEXT(this)" 
              onblur="setUniDefaultTEXT(this)"
              onkeypress="javascript:if(event.keyCode==13){return redirectUniHOME(this,'homeSearchBean')}"/>
              <input type="hidden" id="uniName_hidden"/>       
              <input type="hidden" id="uniName_id"/>
              <input type="hidden" id="uniName_display"/>
              <input type="hidden" id="uniName_url"/>
          </div>
          <div class="fl w62 ml4">
            <a title="ADD" class="btn_view" href="javascript:addOpenDayProv();"><i id="view_1" class="fa fa-plus"></i>ADD</a>
          </div>
        </div>
        <div class="w100p fl ">
          <h2 class="mt25">Let me know for open days for...</h2>
        </div>
        <div class="Lht_Box" id="noUni">
          <div class="no_univ">No unis selected yet	</div>	
        </div>   
        <div class="Lht_Box" id="reserveUniList" style="display:none">
          <div class="pros_scroll">
            <ul class="uni_menu" id="addReserveUniList"></ul>
          </div>
        </div>   
        <fieldset class="w100p fl">
          <div class="borderbot fl mt30 mb10"></div>
          <fieldset class="rgfm-rt">
            <span class="log-btn ">
              <a class="btn1 rgfw" title="NEXT" id="btnLogin" onclick="javaScript:loadSocialMediaLogin();">NEXT<i class="fa fa-long-arrow-right"></i></a>
            </span>
            <p style="display:none;" id="loadinggif"><span id="loadgif"><img class="loading" src="<%=CommonUtil.getImgPath("/wu-cont/img/whatuni/indicator1.gif",0)%>" alt="Loading" title=""></span></p>
          </fieldset>
        </fieldset>  
      </div>
    </div>        
  </div>       
</div>   
<input type="hidden" id="hidStyleId"/>
<input type="hidden" id="resOpenDay" value="true"/>
<!--<input type="hidden" id="resUniIds"/>-->