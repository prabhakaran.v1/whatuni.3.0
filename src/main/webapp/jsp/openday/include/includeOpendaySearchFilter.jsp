<%@page import="org.apache.commons.validator.GenericValidator"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%
   String openDayUrl = (String)request.getAttribute("openDayUrl");
   String qualificationId = request.getParameter("level");
          qualificationId = !GenericValidator.isBlankOrNull(qualificationId)? qualificationId: "";
   String filter = "N";
   String collegeid = request.getParameter("collegeid");
          collegeid = !GenericValidator.isBlankOrNull(collegeid)? collegeid: "";
   String location = request.getParameter("location");
          location = !GenericValidator.isBlankOrNull(location)? location: "";
   String uniDefaultText = "Enter uni name";
   String collegeName = (String)request.getAttribute("odcollegeName");
          uniDefaultText = (GenericValidator.isBlankOrNull(collegeName)) ?  uniDefaultText : collegeName;
   String pageNo = request.getParameter("pageNo");
          pageNo = !GenericValidator.isBlankOrNull(pageNo)? pageNo: "";
   String keyword = request.getParameter("keyword");
          keyword = (!GenericValidator.isBlankOrNull(keyword) && !("Please enter subject or uni").equals(keyword))? keyword: "";
   String date = (String)request.getAttribute("curMonth");
          date = !(GenericValidator.isBlankOrNull(date)) ?  date : "";
   String monthSearchFlag = request.getParameter("flag");
          monthSearchFlag = !(GenericValidator.isBlankOrNull(monthSearchFlag)) ?  monthSearchFlag : "";
   String selectedLocation = !GenericValidator.isBlankOrNull((String)request.getAttribute("selectedLocation")) ? (String)request.getAttribute("selectedLocation") : "All UK";
   String selectedStudyLevel = !GenericValidator.isBlankOrNull((String)request.getAttribute("selectedStudyLevel")) ? (String)request.getAttribute("selectedStudyLevel") : "All study levels";
%>

<div class="odsrch_cnt">
  <%--College ajax section--%>
  <div class="odsrh_col1 fl">
    <fieldset class="odsrh_fie">
      <form name="uniNameForm" id="uniNameForm" method="post" action="/home.html" onsubmit="javascript:return uniOpenDayResults(document.getElementById('uniName'))">
        <input type="text" name="uniName" id="uniName" value="<%=uniDefaultText%>" autocomplete="off"  onkeydown="clearUniNAME(event,this)"  
            onkeyup="autoCompleteUniForSearchResults(event,this)"   
            onclick="clearDefaultText1(this,'<%=uniDefaultText%>')" 
            onblur="setDefaultText1(this,'<%=uniDefaultText%>')" 
            class="txt_box nw"
            onkeypress="javascript:if(event.keyCode==13){return uniOpenDayResults(document.getElementById('uniName'))}" />
        <input type="hidden" id="uniName_hidden" value="" />
        <input type="hidden" id="uniName_id" value="" />
        <input type="hidden" id="uniName_display" value="" />
        <input type="hidden" id="uniName_url" value="" />
        <input type="hidden" id="uniName_alias" value="" /> 
        <input type="button" name="btnSubmit" class="go_ic" onclick="javascript:return uniOpenDayResults(document.getElementById('uniName'))" style="display:none;"/>
      </form>
      <i class="fa fa-search" aria-hidden="true" onclick="javascript:return uniOpenDayResults(document.getElementById('uniName'))"></i>
    </fieldset>
  </div>
  <%--College ajax section end--%>
  
  <div class="odsrh_col2 fl">
    <%--Month dropdown section--%>
    <c:if test="${not empty requestScope.uniOpenDaysMnthList}">
      <fieldset class="fs_col1 fl" id="month">
        <div class="od_dloc"><span>When</span><span><i class="fa fa-angle-down"></i></span></div>
        <div class="opsr_lst" id="drp_month">
          <ul>
            <c:forEach var="uniMnthList" items="${requestScope.uniOpenDaysMnthList}" varStatus="index">
             <c:if test="${uniMnthList.openDayStatus eq 'Y'}">
                <li class="ckbx_act" onclick="deleteFilterParams('date', '${uniMnthList.openDayMonthYearDesc}');"><input type="checkbox" id="${uniMnthList.openDayMonthYearDesc}" checked="checked" value="${uniMnthList.openDayMonthYearDesc}" class="osr_ckbx " ><span>${uniMnthList.openDayMonthYear}</span></li>
              </c:if>
              <c:if test="${uniMnthList.openDayStatus ne 'Y'}">
                <li onclick="multicheckfn('${uniMnthList.openDayMonthYearDesc}');"><input type="checkbox" id="${uniMnthList.openDayMonthYearDesc}" value="${uniMnthList.openDayMonthYearDesc}" class="osr_ckbx" ><span>${uniMnthList.openDayMonthYear}</span></li>
              </c:if>
            </c:forEach>
          </ul>
        </div>
      </fieldset>
    </c:if>
    <%--Month dropdown section--%>
    <%--Location filter--%>
    <c:if test="${not empty requestScope.locationList}">
      <fieldset class="fs_col2 fl" id="location">
        <div class="od_dloc"><span>Location</span><span><i class="fa fa-angle-down"></i></span></div>
        <div class="opsr_lst" id="drp_location">
	  <ul>
	  <c:forEach var="locationList" items="${requestScope.locationList}" varStatus="index">
            <c:set var="selectedLocation" value="<%=selectedLocation%>"/>
             <c:if test="${locationList.refineDesc eq selectedLocation}">
                <li class="act"><span>${locationList.refineDesc} </span></li>
              </c:if>
              <c:if test="${locationList.refineDesc ne selectedLocation}">
                <li><span><a href="${locationList.openDayURL}"> ${locationList.refineDesc}</a> </span></li>
	      </c:if>
	    </c:forEach>
	  </ul>
	</div>
      </fieldset>
    </c:if>
    <%--Location filter end--%>
    <%--Study level filter--%>
    <c:if test="${not empty requestScope.qualificationList}">
      <fieldset class="fs_col3 fl">
        <div class="od_dloc" id="studyLevel"><span>Study level</span><span><i class="fa fa-angle-down"></i></span></div>
        <div class="opsr_lst" id="drp_studyLevel">
          <ul>
          <c:forEach var="qualificationList" items="${requestScope.qualificationList}">
             <c:set var="selectedStudyLevel" value="<%=selectedStudyLevel%>"/>
             <c:if test="${qualificationList.refineDesc eq selectedStudyLevel}">
                <li class="act"><span>${qualificationList.refineDesc}</span></li>		   
              </c:if>
              <c:if test="${qualificationList.refineDesc ne selectedStudyLevel}">
                <li><span><a href="${qualificationList.openDayURL}">${qualificationList.refineDesc}</a></span></li>		   
             </c:if>
            </c:forEach>
          </ul>
        </div>
      </fieldset>
    </c:if>
    <%--Study level filter end--%>  
  </div>
</div>
  
 <input type="hidden" name="openDayUrl" id="openDayUrl" value="<%=openDayUrl%>" />
 <input type="hidden" name="hidden_odQualification" id="hidden_odQualification" value="<%=qualificationId%>" />   
 <input type="hidden" name="hidden_collegeid" id="hidden_collegeid" value="<%=collegeid%>" />   
 <input type="hidden" name="hidden_location" id="hidden_location" value="<%=location%>" />   
 <input type="hidden" name="pageNo" id="pageNo" value="<%=pageNo%>" />   
 <input type="hidden" name="hidden_keyword" id="hidden_keyword" value="<%=keyword%>" />   
 <input type="hidden" name="hidden_date" id="hidden_date" value="<%=date%>" />   
 <input type="hidden" name="hidden_dt" id="hidden_dt" value="<%=date%>" />   
 <input type="hidden" name="hidden_monthSearchFlag" id="hidden_monthSearchFlag" value="<%=monthSearchFlag%>" />  
 <script type="text/javascript">
  showFilterBlock('<%=filter%>');
 </script>