<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalFunction"%>
<%--
  * @purpose  This is open day center pod jsp..
  * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * ?               ?                         ?       First Draft                                                       ? 
  * *************************************************************************************************************************
  * 06.05.2020  Prabhakaran V.               1.1     Tile change - Structure level change                            
--%>
<%
String gaCollegeName = "";
String totalOpenDayCount = (String)request.getAttribute("totalOpenDaysCount");
       totalOpenDayCount =  GenericValidator.isBlankOrNull(totalOpenDayCount)?"":totalOpenDayCount;
String keyword = request.getParameter("keyword");
       keyword = (!GenericValidator.isBlankOrNull(keyword) && !("Please enter subject or uni").equals(keyword))? keyword: "";
String search_keyword = (String)request.getAttribute("search_keyword");
       search_keyword = (!GenericValidator.isBlankOrNull(search_keyword) && !("Please enter subject or uni").equals(search_keyword))? search_keyword: "";
CommonUtil util = new CommonUtil();
CommonFunction common = new CommonFunction();
GlobalFunction globalFn = new GlobalFunction();
String location = request.getParameter("location");
       location = !GenericValidator.isBlankOrNull(location)?  util.toTitleCase(common.replaceHypenWithSpace(location)) : ""; 
       if(!GenericValidator.isBlankOrNull(location)){
       if("south east england".equalsIgnoreCase(location.trim())){
        location = "the South East of England";
       }else if("south west england".equalsIgnoreCase(location.trim())){
        location = "the South West of England";
       }else if("eastern england".equalsIgnoreCase(location.trim())){
        location = "the East of England";
       }else if("northern england".equalsIgnoreCase(location.trim())){
        location = "the North of England";
       }
       }
String collegeName = (String)request.getAttribute("odcollegeName");  
       collegeName = (!GenericValidator.isBlankOrNull(collegeName))? collegeName: "";
String totalRecordCount = (String)request.getAttribute("totalRecordCount");
       totalRecordCount = (GenericValidator.isBlankOrNull(totalRecordCount))? "":totalRecordCount;
String empRateMax = request.getParameter("e-rt-max");
       empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
String empRateMin = request.getParameter("e-rt-min");
       empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
String exitOpenDayRes = "" , odEvntAction = "false";
      if(session.getAttribute("exitOpRes")!=null && !"".equals(session.getAttribute("exitOpRes"))){
        exitOpenDayRes = (String)session.getAttribute("exitOpRes");
      } 
      if(!"".equals(exitOpenDayRes) && "true".equals(exitOpenDayRes)){
      odEvntAction = "true";
    }
String date = (String)request.getAttribute("curMonth");
       date = !(GenericValidator.isBlankOrNull(date)) ?  date : "";
String monthSearchFlag = request.getParameter("flag");
       monthSearchFlag = !(GenericValidator.isBlankOrNull(monthSearchFlag)) ?  monthSearchFlag : "";
String futureOpenDayExists = (String)request.getAttribute("futureOpenDayExists");
       futureOpenDayExists = !(GenericValidator.isBlankOrNull(futureOpenDayExists)) ?  futureOpenDayExists : "";
String noResultsText = "";
boolean errFlag = false;
int stind3 = request.getRequestURI().lastIndexOf("/");
int len3 = request.getRequestURI().length();
String pagenameOd = request.getRequestURI().substring(stind3+1,len3);
String monthsText = (String)request.getAttribute("highlightMonth");
       monthsText = !GenericValidator.isBlankOrNull(monthsText)? (" in " + monthsText): " on the dates you entered";
if(!GenericValidator.isBlankOrNull(collegeName)){
  if(((!GenericValidator.isBlankOrNull(empRateMax))) &&  (!GenericValidator.isBlankOrNull(empRateMin))){
    noResultsText = "Sorry, we can't find any events!";
    noResultsText = noResultsText + "</br>";
    noResultsText =  noResultsText + "Please adjust your filters and try again.";
  }else if((!GenericValidator.isBlankOrNull(date))){
    noResultsText = "Sorry, we can't find any events!";
    noResultsText = noResultsText + "</br>";
    if("opendaySearchResults.jsp".equals(pagenameOd)){
      noResultsText =  noResultsText + "Try again with different details and you may have more luck.";
    }else{
      noResultsText =  noResultsText + "Try again with different details and you may have more luck.";
    }
  }else{
    noResultsText = "Sorry, we can't find any events!";
    noResultsText = noResultsText + "</br>";
    if("opendaySearchResults.jsp".equals(pagenameOd)){
      noResultsText =  noResultsText + "Try again with different details and you may have more luck.";
    }else{
      noResultsText =  noResultsText + "Try again with different details and you may have more luck.";
    }
  }
}else if(!GenericValidator.isBlankOrNull(location)){
  if((!GenericValidator.isBlankOrNull(empRateMax)) &&  (!GenericValidator.isBlankOrNull(empRateMin))){
    noResultsText = "Sorry, we can't find any events!";
    noResultsText = noResultsText + "</br>";
    noResultsText =  noResultsText + "Try again with different details and you may have more luck.";
  }else if((!GenericValidator.isBlankOrNull(date))){
    noResultsText = "Sorry, we can't find any events!";
    noResultsText = noResultsText + "</br>";
    noResultsText =  noResultsText + "Try again with different details and you may have more luck.";
  }else{
    noResultsText = "Sorry, we can't find any events!";
    noResultsText = noResultsText + "</br>";
    noResultsText =  noResultsText + "Try again with different details and you may have more luck.";
  }
}else if(!GenericValidator.isBlankOrNull(keyword)){
  if((!GenericValidator.isBlankOrNull(empRateMax)) &&  (!GenericValidator.isBlankOrNull(empRateMin))){
    noResultsText = "Sorry, we can't find any open days matching "+ keyword +" for universities with the employment rate you selected!";
    noResultsText = noResultsText + "</br>";
    noResultsText =  noResultsText + "Try again with a different employment rate or location and you may have more luck.";
  }else if((!GenericValidator.isBlankOrNull(date))){
    noResultsText = "Sorry, we can't find any open days matching "+ keyword + monthsText + "!";
    noResultsText = noResultsText + "</br>";
    noResultsText =  noResultsText + "Try again with a different month or location and you may have more luck.";
  }else{
    noResultsText = "Sorry, we can't find any open days matching "+ keyword;
    noResultsText = noResultsText + "</br>";
    noResultsText =  noResultsText + "Try again with a different month or location and you may have more luck.";
  }
} else if((!GenericValidator.isBlankOrNull(date))){
   noResultsText = "Sorry, we can't find any events!";
    noResultsText = noResultsText + "</br>";
    noResultsText =  noResultsText + "Try again with different details and you may have more luck.";
}
String weekEndFlag = "";
String openDayEventType = "";
boolean virtualGaFlag = true;
boolean onlineGaFlag = true;
boolean physicalGaFlag = true;
%>

<div class="odres_lst">
  <c:if test="${not empty requestScope.noResultsFlag}">
    <div class="op_nores">
      <c:if test="${not empty requestScope.odcollegeName}">
        <c:if test="${empty requestScope.futureOpenDayExists}">
          <h2 class="ud_tit">Sorry the <%=collegeName%> haven't provided us with their open days yet</h2>
        </c:if>
        <c:if test="${not empty requestScope.futureOpenDayExists}">
          <h2 class="ud_tit"><%=noResultsText%></h2>
          <%errFlag = true;%>
        </c:if>
        <c:if test="${empty requestScope.odcollegeName}">
          <h2 class="ud_tit"><%=noResultsText%></h2>   
          <%errFlag = true;%>
        </c:if>
      </c:if> 
      <c:if test="${empty requestScope.odcollegeName}">
        <h2 class="ud_tit"><%=noResultsText%></h2>  
        <%errFlag = true;%>
      </c:if>
      <%if(errFlag){%>
        <div class="sr_calbtn2"><a class="sr_pd" href="/open-days/">START NEW SEARCH<i class="fa fa-long-arrow-right"></i></a></div>
      <%}%>
    </div>
    
    <c:if test="${not empty requestScope.odcollegeName}">
      <c:if test="${not empty requestScope.opendayAdvertiser}">
        <c:if test="${requestScope.opendayAdvertiser eq 'Y'}">
          <h3 class="ods_tit">Other events at <%=collegeName%></h3>
        </c:if>             
      </c:if>
      <c:if test="${empty requestScope.opendayAdvertiser}">
        <h3 class="ods_tit">Open days at unis like <%=collegeName%></h3>
      </c:if>
    </c:if>
    
    <c:if test="${empty requestScope.odcollegeName}">
      <h3 class="ods_tit">Upcoming open days</h3>
    </c:if>
  </c:if>
  
  <c:if test="${not empty requestScope.openDaySearchResultsList}">
    <c:forEach var="openDaySrList" items="${requestScope.openDaySearchResultsList}" varStatus="index">
      <div class="odres_wrp vrtopd_wrp">
        <c:choose>
          <c:when test="${openDaySrList.openDate eq 'Virtual tours'}">
            <div class="odres_tit vrtopd_hd"><h2>Virtual tours</h2></div>
          </c:when>
          <c:otherwise>
            <div class="odres_tit"><span></span><span>${openDaySrList.openDate}</span><span></span></div>
          </c:otherwise>
        </c:choose>
        <c:if test="${not empty openDaySrList.openDaysSRList}">
          <c:forEach var="uniOpenDaysList" items="${openDaySrList.openDaysSRList}" varStatus="idx">
            <c:if test="${not empty uniOpenDaysList.collegeDisplayName}">
              <c:set var="uniDisplayName" value="${uniOpenDaysList.collegeDisplayName}"/>
              <%
                gaCollegeName= common.replaceSpecialCharacter(globalFn.getReplacedString((String)pageContext.getAttribute("uniDisplayName")));
              %>
            </c:if>    
            <div class="ucod_Cnr">
              <input type="hidden" id="" value="${uniOpenDaysList.providerUrl}"/>
              <c:if test="${not empty openDaySrList.openDaysSRList}">
	            <div class="ucod_wrap fl">
	              <div class="lft_pod">
		            <a href="${uniOpenDaysList.providerUrl}">
		              <c:choose>
		                <c:when test="${idx.index lt 2}">
                          <img class="pro_logo" src="<%=CommonUtil.getImgPath("",0)%>${uniOpenDaysList.collegeLogo}" alt="${uniOpenDaysList.collegeDisplayName}" title="${uniOpenDaysList.collegeDisplayName}"/>
                        </c:when>
                        <c:otherwise>
                          <img class="pro_logo" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif",1)%>" data-src="<%=CommonUtil.getImgPath("",0)%>${uniOpenDaysList.collegeLogo}" alt="${uniOpenDaysList.collegeDisplayName}" title="${uniOpenDaysList.collegeDisplayName}"/>
                        </c:otherwise>
		              </c:choose>
                    </a>
                  </div>
		          
		          <div class="rht_pod rhtsr_pod fl">
		            <h2><a href="${uniOpenDaysList.providerUrl}">${uniOpenDaysList.collegeDisplayName}</a></h2>
		            <p class="od_descr">${uniOpenDaysList.qualification}</p>
		            <p class="od_loc"><c:if test="${uniOpenDaysList.eventCategoryId eq '5'}"><i class="fa fa-map-marker" aria-hidden="true"></i></c:if>${uniOpenDaysList.venue} <c:if test="${not empty uniOpenDaysList.eventDate}">, ${uniOpenDaysList.eventDate}</c:if></p>
		          </div> 
		          
		          <c:choose>
		            <c:when test="${uniOpenDaysList.eventCategoryId eq '5'}">
                      <c:set var="opendayType" value="Physical"  />
					  <%
					    if(physicalGaFlag){
					      openDayEventType = openDayEventType+  "Physical|";
					      physicalGaFlag = false;
					    }
					  %> 	                   
                    </c:when>                   
                    <c:when test="${uniOpenDaysList.eventCategoryId eq '6' }">
                      <c:set var="opendayType" value="Virtual"  />
                      <%
                        if(virtualGaFlag){
					      openDayEventType = openDayEventType+  "Virtual|";
					      virtualGaFlag = false;
					    }
                      %>
                    </c:when>
                    <c:otherwise>
                      <c:set var="opendayType" value="Online"  />
                      <%
                        if(onlineGaFlag){
					      openDayEventType = openDayEventType+  "Online|";
					      onlineGaFlag = false;
					    } 
					  %>
                    </c:otherwise>
                  </c:choose>
		          <c:set var="saveCalCls" value="opdbt_btm"/>
		          <c:if test="${uniOpenDaysList.pastDateFlag eq 'N'}">
                    <div class="sr_btn_cont fl">
                      <div class="sav_cal ${opendayType}" id="openDay_${uniOpenDaysList.openDayEventId}">
                        <c:if test="${uniOpenDaysList.eventCategoryId ne '6' }">
                          <c:set var="saveCalCls" value=""/>
                          <c:if test="${not empty sessionScope.userInfoList}">
                            <c:if test="${uniOpenDaysList.openDayExistsFlag ne 'TRUE'}">
		                      <a class="savTag" id="calBtn_${uniOpenDaysList.openDayEventId}" onclick="javascript:addOpendaystocalendar('${uniOpenDaysList.openDate}', '${uniOpenDaysList.openMonthYear}', '${uniOpenDaysList.collegeId}', '${uniOpenDaysList.openDayEventId}', '','${uniOpenDaysList.subOrderItemId}','${uniOpenDaysList.networkQualificationId}', '${uniOpenDaysList.openDayEventId}', '', '${uniOpenDaysList.websitePrice}', '${uniOpenDaysList.bookingUrl}', '${opendayType}')"><i class="far fa-calendar-alt savTag"></i>Save to calendar</a>
                            </c:if>
                            <c:if test="${uniOpenDaysList.openDayExistsFlag eq 'TRUE'}">
		                      <a href="#" class="saved"><i class="far fa-calendar-alt"></i>Saved to calendar</a>
                            </c:if>
                          </c:if>
                          <c:if test="${empty sessionScope.userInfoList}">
                            <a class="savTag" id="calBtn_${uniOpenDaysList.openDayEventId}" onclick="javaScript:showLightBoxLoginForm('popup-newlogin',650,500,'','','add-open-days','<%=gaCollegeName%>#${uniOpenDaysList.openDayEventId}#${uniOpenDaysList.collegeId}#${uniOpenDaysList.openDate}#${uniOpenDaysList.openMonthYear}#${uniOpenDaysList.subOrderItemId}#${uniOpenDaysList.networkQualificationId}#${uniOpenDaysList.websitePrice}#${uniOpenDaysList.bookingUrl}#${opendayType}');"><i class="far fa-calendar-alt savTag"></i>Save to calendar</a>
                          </c:if>
                          <a class="saved" id="calBtnAct_${uniOpenDaysList.openDayEventId}"  style="display:none;"><i class="far fa-calendar-alt"></i>Saved to calendar</a>
                        </c:if>
                      </div>
                      <input type="hidden" id="collegeId_${uniOpenDaysList.openDayEventId}" name="collegeId_${uniOpenDaysList.openDayEventId}" value="${uniOpenDaysList.collegeId}">
                      <c:if test="${not empty uniOpenDaysList.subOrderItemId}">
                        <c:if test="${not empty uniOpenDaysList.bookingUrl}">
                          <div class="book_opd ${saveCalCls}"><a class="savTag" rel="nofollow" target="_blank"  href="${uniOpenDaysList.bookingUrl}" onclick="GAEventTracking('open-days-type', '${opendayType}', '<%=gaCollegeName%>'); GAEventTracking('open-days', 'reserve-place', '<%=gaCollegeName%>','1');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', ${uniOpenDaysList.websitePrice}); addOpenDaysForReservePlace('${uniOpenDaysList.openDate}', '${uniOpenDaysList.openMonthYear}', '${uniOpenDaysList.collegeId}', '${uniOpenDaysList.subOrderItemId}', '${uniOpenDaysList.networkQualificationId}'); cpeODReservePlace(this,'${uniOpenDaysList.collegeId}','${uniOpenDaysList.subOrderItemId}','${uniOpenDaysList.networkQualificationId}','${uniOpenDaysList.bookingUrl}');var a='s.tl(';" title="<spring:message code="book.open.day.button.text" />" ><spring:message code="book.open.day.button.text" /> <i class="fa fa-caret-right savTag"></i></a></div>
                        </c:if>
                      </c:if>
                    </div>
                  </c:if>
                  
	            </div>
              </c:if>
	        </div>
          </c:forEach>
        </c:if>
      </div>
    </c:forEach>
    
    <c:set var="openDayEventType" value="<%=openDayEventType %>" scope="request" />
  </c:if>
  
  <%-- Pagination --%>
  <c:if test="${empty requestScope.noResultsFlag}">	
    <%    
      String pageURL = (String)request.getAttribute("openDayPaginationURL"); 
      String pageNo = request.getParameter("pageno"); 
      pageNo = (GenericValidator.isBlankOrNull(pageNo))? "1":pageNo;
    %>
    <div class="pr_pagn">
      <jsp:include page="/reviewRedesign/include/reviewPagination.jsp">
        <jsp:param name="pageno" value="<%=pageNo%>"/>            
        <jsp:param name="pagelimit"  value="10"/>
        <jsp:param name="searchhits" value="<%=totalRecordCount%>"/>
        <jsp:param name="recorddisplay" value="10"/>              
        <jsp:param name="pageURL" value="<%=pageURL%>"/>
        <jsp:param name="orderBy" value=""/>
        <jsp:param name="pageTitle" value=""/>
      </jsp:include>
    </div>
  </c:if>	 
</div>
<input type="hidden" name="openDaySProviderName" id="openDaySProviderName" value=""/>
<input type="hidden" name="nonAdvProviderName" id="opendayProviderName" value="<%=collegeName%>"/>
<input type="hidden" name="openDayEventTypeDim" id="openDayEventTypeDim" value="<%=openDayEventType %>"/>
<input type="hidden" name="openDaySEventId" id="openDaySEventId" value=""/>
<input type="hidden" name="openDaySCollegeId" id="openDaySCollegeId" value=""/>
<input type="hidden" name="openDaySDateDay" id="openDaySDateDay" value=""/>
<input type="hidden" name="openDaySDateMonthYear" id="openDaySDateMonthYear" value=""/>
<input type="hidden" name="openDaySubOrderItemId" id="openDaySubOrderItemId" value=""/>
<input type="hidden" name="openDayCpeQualId" id="openDayCpeQualId" value=""/>
<input type="hidden" name="openDayCpeQualId" id="openDayCpeQualId" value=""/>
<input type="hidden" name="openDayBookingUrl" id="openDayBookingUrl" value=""/>
<input type="hidden" name="openDayWebPrice" id="openDayWebPrice" value=""/>
<input type="hidden" name="opendayType" id="opendayType" value=""/>
<input type="hidden" name="fullOpenDayUrlwithparam" id="fullOpenDayUrlwithparam" value="<%=(String)request.getAttribute("fullOpenDayUrlwithparam")%>"> 
    
<c:if test="${empty requestScope.defUniOpenDaysList}">
  <input type="hidden" id="resUniIds"/>
  <input type="hidden" id="delResUniIds"/>
  <input type="hidden" id="resOpenDaysExist"/>
  <input type="hidden" id="exitOpenDayResFlg" value="<%=exitOpenDayRes%>"/>
  <input type="hidden" id="odEvntAction" value="<%=odEvntAction%>"/>
</c:if>
