<%@page import="com.wuni.util.MSUnescaper, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalFunction, WUI.utilities.GlobalConstants"%>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--
  * @purpose  This is Save to open day light box jsp..
  * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * ?               ?                         ?       First Draft                                                       ? 
  * *************************************************************************************************************************
  * 06.05.2020  Prabhakaran V.               1.1     Tile change - Book event button change                            
--%>
<%
  String flag = request.getParameter("flag");
  String message = request.getParameter("message");
  String fromlogin = request.getParameter("from");
  CommonFunction common = new CommonFunction();
  if(fromlogin==null || "".equals(fromlogin)){
    fromlogin = "fromlogin";
  }
  String popMsg = flag;
  String h1Tag = "Woah, steady on there!";
  String closeButton = "";
  String contentHeadClass = "line line1 m12 line2";
  
  String collegeName = "";
  String openDayDate = "";
  String url = "";
  String gaCollegeName = "";
  String collegeNameDisp = "";
  String openDate = "";
  String bookingUrl = "";
  String subOrderItemId = "";
  String qualificationId = "";
  String webPrice = "";
  String collegeId = "";
  String openMonthYear = "";
  String opendayType = "";
   
  if("OPENDAY-SUCESS-ALERT".equalsIgnoreCase(flag)){
    String[] messageArray = message.split("#");
    collegeName = messageArray[0];
    
    openDate =  messageArray.length > 4 ? messageArray[4] : "";
    bookingUrl = messageArray.length > 9 ? messageArray[9] : "";
    webPrice = messageArray.length > 8 ? messageArray[8] : "";
    subOrderItemId = messageArray.length > 6 ? messageArray[6] : "";
    qualificationId = messageArray.length > 7 ? messageArray[7] : "";
    openMonthYear = messageArray.length > 5 ? messageArray[5] : "";
    collegeId = messageArray.length > 3 ? messageArray[3] : "";
    opendayType = messageArray.length > 10 ? messageArray[10] : "";
    
    h1Tag = "<i class=\"fa fa-check\"></i>   EVENT ADDED TO CALENDAR";
    popMsg = "<h3 class=\"tit\">"+ messageArray[0]+"</h3><p class=\"info\"> ";
    collegeNameDisp = messageArray[0];
    if(messageArray.length>2){
      gaCollegeName = common.getCollegeTitleFunction(messageArray[3]);
      if(!GenericValidator.isBlankOrNull(gaCollegeName)){url = "/open-days/" + common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(gaCollegeName))).toLowerCase() +"/" + messageArray[3];gaCollegeName = gaCollegeName.trim();}
      if( !"NODESC".equalsIgnoreCase(messageArray[2])){
        popMsg = popMsg + messageArray[2] + " : " + messageArray[1];
        openDayDate = messageArray[1];
      }
    }else{
      popMsg = popMsg +  messageArray[1];
    }
    popMsg = popMsg + "</p>";        
    popMsg = new MSUnescaper().unescape(popMsg);    
  }else if("OPENDAY-EXIST-ALERT".equalsIgnoreCase(flag)){
    h1Tag = "EVENT ALREADY ADDED TO YOUR CALENDAR";
    popMsg = "<p class=\"info\">This event is already added to your MyWhatuni calendar</p>";
  }
  String referrer = request.getHeader("referer");
  String viewCalUrl ="/open-days/#futureOpenDaysDiv";
  String openDayUrl = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + GlobalConstants.SEO_PATH_OPEN_DAY_PAGE;
  if(openDayUrl.equalsIgnoreCase(referrer)){
    viewCalUrl = "/open-days/?div#futureOpenDaysDiv";
  }
 
 if("OPENDAY-EXIST-ALERT".equalsIgnoreCase(flag)){
%>
  <div class="comLgh">
    <div id="mbd">
      <div class="fcls nw">
        <a class="go_visit  " onclick="closeOpenDay('<%=fromlogin%>')">
          <i class="fa fa-times"></i>
        </a>
      </div>  
      
      <div class="pform nlr bgw opday" style="display: block;">
        <div class="reg-frm">
          <div class="sp_pad cf">
            <div class="sav_pro">
              <div class="sp_title"><h2><%=h1Tag%></h2></div>             
              <div class="save_pro_cnr no_bg"><div class="sus_uni"><%=popMsg%></div></div>        
              
              <div class="fl w100p txt_cnr">
                <a id="btnLogin" class="btn1 rgfw" href="<%=viewCalUrl%>" title="view calendar">View Calendar</a>
                <%-- ::START:: Yogeswari :: 19.05.2015 :: Added back to open days link for doing luke's feedback. --%>
                <a id="btnOpenDays" class="btn1 rgfw ml20" href="/open-days/" title="view calendar">Back to open days</a>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<%}else{%>
  <div class="comLgh">
    <div id="mbd">
      <div class="fcls nw"><a class="go_visit  " onclick="closeOpenDay('<%=fromlogin%>')"><i class="fa fa-times"></i></a></div>  
      <div class="pform nlr bgw opday vir_opd" style="display: block;">
        <div class="reg-frm">
          <div class="sp_pad cf">
            <div class="sav_pro">
              <div class="sp_title">
                <%if("OPENDAY-SUCESS-ALERT".equalsIgnoreCase(flag)){%>
                  <input type="hidden" id="opendaySaveAjaxCollegeName" name="opendaySaveAjaxCollegeName" value="<%=collegeName%>"/>
                  <h2><%=h1Tag%></h2>
                  <p class="ods_des"><%=collegeNameDisp%> 's open day saved as a reminder</p>
                  <a class="odsbtn" href="<%=viewCalUrl%>" title="view calendar">VIEW CALENDAR <i class="fa fa-long-arrow-right"></i></a>
                  <p class="ods_des lft">(<span>Please note:</span> you have only saved the open day to your calendar to confirm attendance, please contact the university directly)</p>
                  <a class="btn1 odsbtn1" onclick="opendayFb('<%=openDayDate%>', '<%=url%>', '<%=common.replaceSpecialCharacter(new GlobalFunction().getReplacedString(gaCollegeName))%>');"><i class="fa fa-facebook fa-1_5x"></i> SHARE ON FACEBOOK <i class="fa fa-long-arrow-right"></i></a>
                <%}%>
              </div>
           
              <%if(!GenericValidator.isBlankOrNull(subOrderItemId) && !GenericValidator.isBlankOrNull(bookingUrl)){%>
                <div class="fl w100p txt_cnr dw_resbt">
                  <%--  <p>Do you want to reserve a place on this open day?</p>--%>
                  <a rel="nofollow" class="btn1 rgfw bkopd_btn" target="_blank"  href="<%=bookingUrl%>" onclick="GAEventTracking('open-days-type', '<%=opendayType%>', '<%=gaCollegeName%>');GAEventTracking('open-days', 'reserve-place', '<%=gaCollegeName%>','1');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', '<%=webPrice%>'); addOpenDaysForReservePlace('<%=openDate%>', '<%=openMonthYear%>', '<%=collegeId%>', '<%=subOrderItemId%>', '<%=qualificationId%>'); cpeODReservePlace(this,'<%=collegeId%>','<%=subOrderItemId%>','<%=qualificationId%>','<%=bookingUrl%>');var a='s.tl(';" title="<spring:message code="book.open.day.button.text" />" > <spring:message code="book.open.day.button.text" /> <i class="fa fa-caret-right"></i></a>
                </div>
              <%}%>
           
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>      
<%}%>