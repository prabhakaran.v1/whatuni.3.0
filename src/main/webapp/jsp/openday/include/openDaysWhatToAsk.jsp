<div class="content-bg">
  <h3>What to ask on a Virtual open day</h3>
  <div class="wta_od">
    <ul>
      <li>What is included in the accommodation price?</li>
      <li>How many contact hours does my course have?</li>
      <li>Are there any grants or bursaries available to me?</li>
      <li>What is the Student's Union like?</li>
      <li>Where is the nearest doctors to my accommodation?</li>
      <li>What facilities are available for my course specifically?</li>
      <li>Is there any support if I have any concerns / worries?</li>
      <li>How far is the city centre away from me, will I have to travel?</li>
      <li>What sports / societies does the uni have to offer?</li>
      <li>What is the percentage ratio to exams / coursework?</li>
    </ul>
  </div>
</div>