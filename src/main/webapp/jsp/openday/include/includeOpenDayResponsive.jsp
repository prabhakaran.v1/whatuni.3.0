<%--Added for wu_539 07-APR-2015 by Karthi for OpenDays Responsive--%>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction"%>
<%
String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");
String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
String openDayMobileCSS = CommonUtil.getResourceMessage("wuni.whatuni.mobile.styles.css", null);
String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");%>
<script type="text/javascript" language="javascript">
var flexslider;
var dev = jQuery.noConflict();
dev(document).ready(function(){
mobileSpecificImage('hopd_img');
adjustStyle();
});
adjustStyle();
function jqueryWidth() {
  return dev(this).width();
} 
function adjustStyle() {    
    var width = document.documentElement.clientWidth;
    var path = ""; 
    //included for dynamic load css for mobile view
    if (width <= 1024) {
      <%if(("LIVE").equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=openDayMobileCSS%>";
      <%}else if("TEST".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=openDayMobileCSS%>";
      <%}else if("DEV".equals(envronmentName)){%>
          document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=openDayMobileCSS%>";
      <%}%>
    }
     //     
    dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
    dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
    if (width <= 480) {
    dev('.op_mth_slider ul').attr("id", "select_cont");
    if(dev('#ajax_listOfOptions').attr('class') != 'opd_ajax'){
      dev('#ajax_listOfOptions').attr("class", "ajax_mob");
    }
	    
      //dev('#mbox').attr("class", "mob_res_lb");
    } else if ((width > 480) && (width <= 992)) {  
    dev('.op_mth_slider ul').attr("id", "select_cont");
    if(dev('#ajax_listOfOptions').attr('class') != 'opd_ajax'){
      dev('#ajax_listOfOptions').attr("class", "ajax_mob");
    }
    dev('.gen_srchbox').css('width', '100%').css('width', '-=18px');
	   
      //dev('#mbox').attr("class", "mob_res_lb");
    } else {
       
        if(dev('#ajax_listOfOptions').attr('class') != 'opd_ajax'){
          dev('#ajax_listOfOptions').removeAttr("class");
        }
        dev('.op_mth_slider ul').removeAttr("id");
        dev('.op_mth_slider ul li').css('width', '77px');
        document.getElementById('size-stylesheet').href = "";  
        dev(".hm_srchbx").hide();
    }
    if(document.getElementById("odLocMap")){
      mapLoad();
    }
}
dev(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
    setTimeout(function() {
        dev(window).trigger('resize');        
    }, 500);
}
dev(window).resize(function() {  
  dev('#autoEmailId').css('width', dev('#'+dev('#autoEmailId').closest('fieldset').find("input").attr('id')).outerWidth());
  dev('#autoEmailIdLogin').css('width', dev('#'+dev('#autoEmailIdLogin').closest('fieldset').find("input").attr('id')).outerWidth());
  mobileSpecificImage('hopd_img');
  if(document.getElementById("odLocMap")){
    mapLoad();
  }
  var screenWidth = jqueryWidth();
  if (screenWidth <= 480){  //Mobile view 
    dev('.op_mth_slider ul').css({"left": "0px", "display": "none"});
  }
  else if((screenWidth > 480) && (screenWidth <= 992)){  //Tablet view 
    dev('.gen_srchbox').css('width', '100%').css('width', '-=18px');
    dev("#fixedscrolltop").removeAttr("style").removeClass("sticky_top_nav"); //Removing sticky top
    dev('.op_mth_slider ul').css({"left": "0px", "display": "none"});
  }
  else{  //Desktop view 
    /*For sticky top*/
    var windowTop = dev(window).scrollTop();         
    if(150 < windowTop){
      dev('#fixedscrolltop').addClass('sticky_top_nav');        
      dev('#fixedscrolltop').css({ position: 'fixed', top: 0 });            
    }else{
      dev('#fixedscrolltop').removeClass('sticky_top_nav');
      dev('#fixedscrolltop').css('position','static');         
    }
    /*For sticky top*/
    dev('.gen_srchbox').removeAttr("style");
    dev('.op_mth_slider ul').css("display", "block");
    dev('.op_mth_slider ul li').css("height", "39px");
    openDayOrientation();
    //dev('#mbox').removeClass("mob_res_lb");
    if (dev("#ol").is(':visible')) {
      var posleft = (screenWidth - 650)/2;
      dev('#mbox').css({ left :posleft+'px'});
    }
  }
  adjustStyle(); 
  if(flexslider!=undefined) {           
      flexslider.vars.minItems = getArticleWuGridSize();
      flexslider.vars.maxItems = getArticleWuGridSize();
      flexslider.slides.width( flexslider.computedW);
      flexslider.update(flexslider.pagingCount);
      flexslider.setProps();
    }
});
function mobileSpecificImage(id){
 if($$D(id)) {
  var devWidth = dev(window).width() || document.documentElement.clientWidth;   
	var url = $$D(id).getAttribute('data-src');		
  var urlString = 'url("' + url + '")';
  var imageIndex =  url.lastIndexOf("/") + 1;
	var hrImg = url.substring(imageIndex).split('.');
  var orgUrl = url.substring(0, imageIndex) + hrImg[0];
	if(devWidth < 480){
	  urlString = 'url("'+ orgUrl +'_480px.' + hrImg[1] + '")';
	} else if(devWidth > 481 && devWidth < 992){	  	
    urlString = 'url("'+ orgUrl +'_992px.' + hrImg[1] + '")';
	}
  $$D(id).style.backgroundImage = urlString;
 }
}
</script>
