<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction, WUI.utilities.GlobalFunction" %>
<%--
  * @purpose  This is my past/saved open day section in open day home page jsp..
  * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * ?               ?                         ?       First Draft                                                       ? 
  * *************************************************************************************************************************
  * 06.05.2020  Prabhakaran V.               1.1     Tile change - structure level change                            
--%>
<%boolean showfurtureviewmore = false;
  String moreViewClass = "display:flex";
  String moreViewClasses = "display:flex";
  String classNameForViewMore = "ucod_Cnr";
  String classNameForHaveBeen = "ucod_Cnr";
  String divClassName = "ucod_lftyr fl";
  String divHaveBeenPod = "ucod_lftyr fl";
  String gaCollegeName = "";
  CommonFunction commonFun = new CommonFunction();
%>

<c:if test="${not empty requestScope.myOpenDaysFutureList}">
  <div class="opd_sdcnt" id="amGngtoPodDivId">
    <div class="odres_wrp vrtopd_wrp">
      <div class="odres_tit">
        <h2>Events I'm going to...</h2>
      </div>
      <c:forEach var="myOpenDaysFutureList" items="${requestScope.myOpenDaysFutureList}" varStatus="index">
        <c:set var="openDayFutureDay" value="${myOpenDaysFutureList.openDay}"/>          
    	  <c:if test="${index.index gt 2}">
    	  <%
            moreViewClass="display:none";
            classNameForViewMore="ucod_Cnr moreViewInfo";
          %>
        </c:if>            
        <div class="<%=classNameForViewMore%>" style="<%=moreViewClass%>" id="goingtopodid">
          <div class="ucod_wrap fl">
            <div class="lft_pod">
              <c:if test="${not empty myOpenDaysFutureList.collegeLogo}">
                <a href="${myOpenDaysFutureList.collegeLandingUrl}">
                  <img class="pro_logo"
                   src = "" 
                   data-src="<%=CommonUtil.getImgPath("",0)%>${myOpenDaysFutureList.collegeLogo}"
                   alt="${myOpenDaysFutureList.collegeDisplayName}" 
                   title="${myOpenDaysFutureList.collegeDisplayName}"></img>
                </a>
              </c:if>
            </div>            
            
            <c:if test="${not empty myOpenDaysFutureList.openDaysSRList}">
              <c:forEach var="uniOpenDaysList" items="${myOpenDaysFutureList.openDaysSRList}" varStatus="ind" end="3">
                <c:choose>
                  <c:when test="${uniOpenDaysList.eventCategoryId eq '5'}">
                    <c:set var="opendayType" value="Physical"  />
                  </c:when>
                  <c:when test="${uniOpenDaysList.eventCategoryId eq '6' }">
                    <c:set var="opendayType" value="Virtual"  />
                  </c:when>
                  <c:otherwise>
                    <c:set var="opendayType" value="Online"  />
                  </c:otherwise>
                </c:choose>
                
                <div class="rht_pod rhtsr_pod fl">
                  <input type="hidden" value="${myOpenDaysFutureList.collegeLandingUrl}"/>
                  <h2><span><a href="${myOpenDaysFutureList.collegeLandingUrl}">${myOpenDaysFutureList.collegeDisplayName}</a></span></h2>
                  <p class="od_descr">${uniOpenDaysList.qualification}</p>
                  <p class="od_loc">
                    <c:if test="${uniOpenDaysList.eventCategoryId eq '5'}"><i class="fa fa-map-marker" aria-hidden="true"></i></c:if>
                    ${uniOpenDaysList.town} <c:if test="${not empty uniOpenDaysList.startTime}"> - </c:if> <span>${uniOpenDaysList.startTime} <c:if test="${not empty uniOpenDaysList.endTime}"> - ${uniOpenDaysList.endTime}</c:if><c:if test="${not empty uniOpenDaysList.eventDate}">, ${uniOpenDaysList.eventDate}</c:if></span>
                  </p>
                </div>
                
                <div class="sr_btn_cont fl">                              
                  <div class="sav_cal" id="openDay_${uniOpenDaysList.openDayEventId}">
                    <a class="red_act savTag" id="remove${uniOpenDaysList.openDayEventId}" onclick="javascript:removeOpendays('${uniOpenDaysList.openDayEventId}','goingtopodid');">
                     REMOVE
                     <i class="far fa-calendar-minus savTag"></i>
                    </a>
                  </div>   
                  
                  <c:if test="${not empty uniOpenDaysList.subOrderItemId}">                    
                    <div class="book_opd">
                      <a class="savTag" target="_blank" href="${uniOpenDaysList.bookingUrl}" onclick="GAEventTracking('open-days-type', '${opendayType}', '${myOpenDaysFutureList.collegeName}');GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '${myOpenDaysFutureList.collegeName}', <c:if test="${not empty uniOpenDaysList.websitePrice}">${uniOpenDaysList.websitePrice}</c:if>); addOpenDaysForReservePlace('${myOpenDaysFutureList.openDat}', '${myOpenDaysFutureList.openMonthYear}', '${myOpenDaysFutureList.collegeId}', '${uniOpenDaysList.subOrderItemId}', '${uniOpenDaysList.networkQualificationId}');cpeODReservePlace(this,'${myOpenDaysFutureList.collegeId}','${uniOpenDaysList.subOrderItemId}','${uniOpenDaysList.networkQualificationId}','${uniOpenDaysList.bookingUrl}');var a='s.tl(';GAEventTracking('open-days', 'reserve-place', '${myOpenDaysFutureList.collegeName}','1');closeOpenDaydRes();" rel="nofollow"><spring:message code="book.open.day.button.text" /><i class="fa fa-caret-right savTag"></i></a>
                    </div>            
                  </c:if>
                </div>              
              </c:forEach>
            </c:if>
          </div>            
        </div>   
      </c:forEach> 
    </div>

    <c:if test ="${not empty myOpenDaysFutureList}">
      <c:forEach var="myOpenDaysFutureList" items="${requestScope.myOpenDaysFutureList}" varStatus="ind">
        <c:if test="${ind.index eq 3}">
          <div class="all_opday" id="morebtn"><a href="javascript:void(0)" onclick="viewMoreFod();">+ &nbsp;<span>VIEW MORE</span></a></div>
          <div class="all_opday" id="lessbtn" style="display:none"><a href="javascript:void(0)" onclick="viewLessFod('amGngtoPodDivId');">- &nbsp;<span>VIEW LESS</span></a></div>
        </c:if>
      </c:forEach>
    </c:if>
  
  </div>
</c:if>
 
<c:if test="${not empty requestScope.myOpenDaysPastList}">
  <div class="opd_sdcnt opd_past"  id="haveBeenId">
    <div class="odres_wrp vrtopd_wrp">
      <div class="odres_tit">
        <h2>Events I've been to...</h2>
      </div>
      <c:forEach var="myOpenDaysPastList" items="${requestScope.myOpenDaysPastList}" varStatus="index">     
        <c:set var="openDayFutureDayId" value="${myOpenDaysPastList.openDay}"/>   
        <c:if test="${index.index gt 2}">
          <% 
            moreViewClasses="display:none";
            classNameForHaveBeen="ucod_Cnr moreViewInfohavebeenpod";
          %>
        </c:if>
        
        <div class="<%=classNameForHaveBeen%>" style="<%=moreViewClasses%>" id="havebeen">
          <div class="ucod_wrap fl">
            <div class="lft_pod">
              <c:if test="${not empty myOpenDaysPastList.collegeLogo}">
                <a href="${myOpenDaysPastList.collegeLandingUrl}">
                  <img class="pro_logo"
                   src = "" 
                   data-src="<%=CommonUtil.getImgPath("",0)%>${myOpenDaysPastList.collegeLogo}"
                   alt="${myOpenDaysPastList.collegeDisplayName}" 
                   title="${myOpenDaysPastList.collegeDisplayName}"></img>
                </a>
              </c:if>
            </div>
              
            <c:if test="${not empty myOpenDaysPastList.openDaysSRList}">        
              <c:forEach var="uniOpenDaysList" items="${myOpenDaysPastList.openDaysSRList}" varStatus="ind" end="3">
                <c:if test="${not empty myOpenDaysPastList.collegeDisplayName}">
                  <c:set var="collegeName" value="${myOpenDaysPastList.collegeDisplayName}"/>        
                  <%gaCollegeName = commonFun.replaceSpecialCharacter((String)pageContext.getAttribute("collegeName")).toLowerCase(); %>
                </c:if>
                
                <div class="rht_pod rhtsr_pod fl">
                  <input type="hidden" value="${myOpenDaysPastList.collegeLandingUrl}"/>
                  <h2><span><a href="${myOpenDaysPastList.collegeLandingUrl}">${myOpenDaysPastList.collegeDisplayName}</a></span></h2>
                  <p class="od_descr">${uniOpenDaysList.qualification}</p>
                  <p class="od_loc">
                    <c:if test="${uniOpenDaysList.eventCategoryId eq '5'}"><i class="fa fa-map-marker" aria-hidden="true"></i></c:if>
                    ${uniOpenDaysList.town} <c:if test="${not empty uniOpenDaysList.startTime}"> - </c:if><span>${uniOpenDaysList.startTime} <c:if test="${not empty uniOpenDaysList.endTime}"> - </c:if>${uniOpenDaysList.endTime} <c:if test="${not empty myOpenDaysPastList.eventDate}">, ${myOpenDaysPastList.eventDate}</c:if></span>
                  </p>
                </div>
                  
                <div class="sr_btn_cont fl">                              
                  <div class="sav_cal" id="openDay_${uniOpenDaysList.openDayEventId}">
                    <a class="red_act savTag" id="remove${uniOpenDaysList.openDayEventId}" onclick="javascript:removeOpendays('${uniOpenDaysList.openDayEventId}' , 'havebeen');">
                      REMOVE
                      <i class="far fa-calendar-minus savTag"></i>
                    </a>
                  </div>  
                  <c:if test="${myOpenDaysPastList.basketFlag eq 'FALSE'}">                              
                    <div class="res_place" id="basket_div_${myOpenDaysPastList.collegeId}">
                      <a class="savTag" href="javascript:void(0);" onclick='addBasket("${myOpenDaysPastList.collegeId}", "C", this,"basket_div_${myOpenDaysPastList.collegeId}", "basket_pop_div_${myOpenDaysPastList.collegeId}","", "", "<%=gaCollegeName%>");' rel="nofollow">ADD TO SHORTLIST</a>
                    </div>
                    <div class="res_place" id="basket_pop_div_${myOpenDaysPastList.collegeId}" style="display:none;">
                      <a href="/degrees/comparison" rel="nofollow">ADDED TO SHORTLIST</a>
                    </div>
                  </c:if> 
                  <c:if test="${myOpenDaysPastList.basketFlag eq 'TRUE'}">
                    <div class="res_place" id="basket_pop_div_${myOpenDaysPastList.collegeId}">
                      <a href="/degrees/comparison" rel="nofollow">ADDED TO SHORTLIST</a>
                    </div>
                  </c:if>
                </div>
              </c:forEach>            
            </c:if>          
          </div>            
        </div>   
      </c:forEach> 
    </div>

    <c:if test="${not empty myOpenDaysPastList}">
      <c:forEach var="myOpenDaysPastList" items="${requestScope.myOpenDaysPastList}" varStatus="ind">
        <c:if test="${ind.index eq 3}">
          <div class="all_opday" id="morebtnhavebeen"><a href="javascript:void(0)" onclick="viewMorePod();">+ &nbsp;<span>VIEW MORE</span></a></div>
          <div class="all_opday" id="lessbtnhavebeen" style="display:none"><a href="javascript:void(0)" onclick="viewLessPod('haveBeenId');">- &nbsp;<span>VIEW LESS</span></a></div>
        </c:if>
      </c:forEach>
    </c:if>
  </div>
</c:if>