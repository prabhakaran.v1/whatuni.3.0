<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, WUI.utilities.GlobalFunction" %>
<%--
  * @purpose  This is Feature open day section jsp..
  * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * ?               ?                         ?       First Draft                                                       ? 
  * *************************************************************************************************************************
  * 06.05.2020  Prabhakaran V.               1.1     Tile change - structure level change                            
--%>
<%
   String gaCollegeName = "";
   String opdDateHeading = "",nextOpdDateHeading="";
   String openDayMonthDesc = "", stTimeDesc = "", edTimeDesc = "", canonUrl="";
   String exitOpenDayRes = "" , odEvntAction = "false";
   if(session.getAttribute("exitOpRes")!=null && !"".equals(session.getAttribute("exitOpRes"))){
     exitOpenDayRes = (String)session.getAttribute("exitOpRes");
   }    
   if(!"".equals(exitOpenDayRes) && "true".equals(exitOpenDayRes)){
     odEvntAction = "true";
   }
%>

<c:if test="${not empty requestScope.openDaysUpCommingList }">
  <div class="opdwte_top"><svg id="Layer_2" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 523"><defs><style>.cls-2{fill:#fff;}</style></defs><title>lp_rec_grdient</title><polygon class="cls-2" points="0 523 0 0 1920 0 0 523"/></svg></div>
  <div class="ad_cnr">
    <div class="content-bg">
      <div class="odres_lst">
        <div>
          <div id="futureOpendayPod"></div>
          <jsp:include page="/jsp/openday/include/openDaysFutureDaysPod.jsp"/>
        </div>
        
        <div class="odres_wrp vrtopd_wrp" id="upcomingOpenDaysDiv">
          <div class="odres_tit">
            <h2><spring:message code="feature.pod.h2.text"/></h2>
          </div>
          <c:forEach var="openDaysUpCommingList1" items="${requestScope.openDaysUpCommingList}">
            <c:set var="uniDisplayName" value="${openDaysUpCommingList1.collegeNameDisplay}"/>
            <%gaCollegeName= new CommonFunction().replaceSpecialCharacter(new GlobalFunction().getReplacedString((String)pageContext.getAttribute("uniDisplayName")));%>
            
            <div class="ucod_Cnr">
              <input type="hidden" value="${openDaysUpCommingList1.opendaysProviderURL}"/>
              <div class="ucod_wrap fl">
                <div class="lft_pod">
                  <c:if test="${not empty openDaysUpCommingList1.collegeLogo}">
                    <c:set var="logoPath" value="${openDaysUpCommingList1.collegeLogo}"/>
                    <a href="${openDaysUpCommingList1.opendaysProviderURL}"> 
                      <img class="pro_logo" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="<%=CommonUtil.getImgPath("",0)%>${logoPath}" alt="${openDaysUpCommingList1.collegeNameDisplay}">  
                    </a>
                  </c:if>
                </div>
                
                <c:choose>
                  <c:when test="${openDaysUpCommingList1.eventCategoryId eq '5'}">
                    <c:set var="opendayType" value="Physical"  />
                  </c:when>
                  <c:when test="${openDaysUpCommingList1.eventCategoryId eq '6' }">
                    <c:set var="opendayType" value="Virtual"  />
                  </c:when>
                  <c:otherwise>
                    <c:set var="opendayType" value="Online"  />
                  </c:otherwise>
                </c:choose>
                <c:set var="saveCalCls" value="opdbt_btm"/>
                <div class="rht_pod rhtsr_pod fl">
                  <h2><span><a href="${openDaysUpCommingList1.opendaysProviderURL}"> ${openDaysUpCommingList1.collegeNameDisplay}</a></span></h2>
                  <p class="od_descr">${openDaysUpCommingList1.qualType}</p>
                  <p class="od_loc">
                    <c:if test="${openDaysUpCommingList1.eventCategoryId eq '5'}"><i class="fa fa-map-marker" aria-hidden="true"></i></c:if>
                    ${openDaysUpCommingList1.opendayVenue} <c:if test="${not empty openDaysUpCommingList1.startTime}"> - </c:if><span>${openDaysUpCommingList1.startTime} <c:if test="${not empty openDaysUpCommingList1.endTime}"> - ${openDaysUpCommingList1.endTime}</c:if><c:if test="${not empty openDaysUpCommingList1.eventDate}">, ${openDaysUpCommingList1.eventDate}</c:if></span>
                  </p>
                </div>
                <div class="sr_btn_cont fl">    
                  <c:if test="${openDaysUpCommingList1.eventCategoryId ne '6'}">  
                    <c:set var="saveCalCls" value=""/>         
                    <div class="sav_cal" id="openDay_${openDaysUpCommingList1.eventId}">
                      <c:if test="${not empty sessionScope.userInfoList}">
                        <c:if test="${openDaysUpCommingList1.opendayExist ne 'TRUE'}">
                          <a class="savTag" title="Add ${openDaysUpCommingList1.collegeNameDisplay} open day to calendar" id="calBtn_${openDaysUpCommingList1.eventId}" onclick="javascript:addOpendaystocalendar('${openDaysUpCommingList1.openDate}', '${openDaysUpCommingList1.openMonthYear}', '${openDaysUpCommingList1.collegeId}', '${openDaysUpCommingList1.eventId}', '', '${openDaysUpCommingList1.orderItemId}', '${openDaysUpCommingList1.networkId}', '${openDaysUpCommingList1.eventId}', '', '${openDaysUpCommingList1.websitePrice}', '${openDaysUpCommingList1.bookingUrl}', '${opendayType}')">
                            Save to calendar<i class="far fa-calendar-alt savTag"></i>
                          </a>                                  
                        </c:if>
                        <c:if test="${openDaysUpCommingList1.opendayExist eq 'TRUE'}">  
                          <a class="saved"><i class="far fa-calendar-alt fa-calendar-check"></i>Saved to calendar</a>
                        </c:if>
                      </c:if>
                      <c:if test="${empty sessionScope.userInfoList}">
                        <a class="savTag" title="Add ${openDaysUpCommingList1.collegeNameDisplay} open day to calendar" id="calBtn_${openDaysUpCommingList1.eventId}" href="javascript:showLightBoxLoginForm('popup-newlogin',650,500,'','','add-open-days','<%=gaCollegeName%>#${openDaysUpCommingList1.eventId}#${openDaysUpCommingList1.collegeId}#${openDaysUpCommingList1.openDate}#${openDaysUpCommingList1.openMonthYear}#${openDaysUpCommingList1.orderItemId}#${openDaysUpCommingList1.qualId}#${openDaysUpCommingList1.websitePrice}#${openDaysUpCommingList1.bookingUrl}#${opendayType}');">Save to calendar<i class="far fa-calendar-alt savTag"></i></a>                                   
                      </c:if>
                      <a id="calBtnAct_${openDaysUpCommingList1.eventId}"  style="display:none;" class="saved"><i class="far fa-calendar-alt fa-calendar-check"></i>Saved to calendar</a>
                    </div>
                  </c:if>
                  
                  <c:if test="${not empty openDaysUpCommingList1.orderItemId}">
                    <c:if test="${not empty openDaysUpCommingList1.bookingUrl}">        
                      <div class="book_opd ${saveCalCls}">
                        <a class="savTag" target="_blank" href="${openDaysUpCommingList1.bookingUrl}" onclick="GAEventTracking('open-days-type', '${opendayType}', '<%=gaCollegeName%>'); GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', ${openDaysUpCommingList1.websitePrice}); addOpenDaysForReservePlace('${openDaysUpCommingList1.openDate}', '${openDaysUpCommingList1.openMonthYear}', '${openDaysUpCommingList1.collegeId}', '${openDaysUpCommingList1.orderItemId}', '${openDaysUpCommingList1.networkId}');cpeODReservePlace(this,'${openDaysUpCommingList1.collegeId}','${openDaysUpCommingList1.orderItemId}','${openDaysUpCommingList1.networkId}','${openDaysUpCommingList1.bookingUrl}');var a='s.tl(';GAEventTracking('open-days', 'reserve-place', '<%=gaCollegeName%>','1');closeOpenDaydRes();" rel="nofollow"><spring:message code="book.open.day.button.text" /><i class="fa fa-caret-right savTag"></i></a>
                      </div>
                    </c:if>      
                  </c:if>
                  
                </div>
              </div>
            </div>
          </c:forEach>
        </div>        
        <div class="all_opday"><a  onclick="GAInteractionEventTracking('view all', 'Open Day Search', 'View All', 'view all', '');" href="/open-days/search/"><spring:message code="view.all.vitual.button.text"/> <i class="fa fa-long-arrow-right"></i></a></div>
      </div> 
    </div>
  </div>
    
  <div class="opdwte_btm">
    <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1920 523"><defs><style>.cls-1{fill:#f1f0f0;}</style></defs><polygon class="cls-1" points="1920 0 1920 523 0 523 1920 0"/></svg>
  </div>
  <input type="hidden" id="openDaySProviderName"/>
  <input type="hidden" id="openDaySEventId"/>
  <input type="hidden" id="openDaySCollegeId"/>
  <input type="hidden" id="openDaySDateDay"/>
  <input type="hidden" id="openDaySDateMonthYear"/>
  <input type="hidden" id="openDaySubOrderItemId"/>
  <input type="hidden" id="openDayCpeQualId"/>  
  <input type="hidden" id="fullOpenDayUrlwithparam" value="/open-days/">
  <input type="hidden" id="opendayproviderName" value=""/>    
  <input type="hidden" name="openDayBookingUrl" id="openDayBookingUrl" value=""/>
  <input type="hidden" name="openDayWebPrice" id="openDayWebPrice" value=""/>
  <input type="hidden" name="opendayType" id="opendayType" value=""/>
  <input type="hidden" id="resUniIds"/>
  <input type="hidden" id="delResUniIds"/>
  <input type="hidden" id="resOpenDaysExist"/>
  <input type="hidden" id="exitOpenDayResFlg" value="<%=exitOpenDayRes%>"/>
  <input type="hidden" id="odEvntAction" value="<%=odEvntAction%>"/>
</c:if>