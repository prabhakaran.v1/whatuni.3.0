<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%--Added ajax university search in opan days landing page by Hema.S on 11.06.2018--%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, WUI.utilities.GlobalFunction, org.apache.commons.validator.GenericValidator"%>
<h1>Virtual tours and events</h1>
<section class="hms_cnt">
  <div class="hms_wrap" id="ajaxSrchDivId">
    <form id="opdSearchForm" name="opdSearchForm" method="post" action="/home.html" onsubmit="javascript:return opendaysSearchSubmit(uniOpenDayName);">
      <input type="text" name="uniOpenDayName" id="uniOpenDayName" value='Enter uni name' autocomplete="off"
        onkeydown="clearUniNAME(event,this)"  
        onkeyup="autoCompleteOpendaysKeywordBrowse(event,this)"   
        onclick="clearOpdUniDefaultTEXT(this)" 
        onblur="setOpdUniDefaultTEXT(this)" 
        class="hms_inp"
        maxlength="4000"
        onkeypress="javascript:if(event.keyCode==13){return opendaysSearchSubmit(uniOpenDayName)}" />
      <input type="hidden" id="uniOpenDayName_hidden" value="" />
      <input type="hidden" id="uniOpenDayName_id" value="" />
      <input type="hidden" id="uniOpenDayName_display" value="" />
      <input type="hidden" id="uniOpenDayName_url" value="" />
      <input type="hidden" id="uniOpenDayName_alias" value="" />   
      <input type="hidden" id="uniOpenDayName_location" value=""/>
      <span class="hms_fa" onclick="onclickfunction();"><i class="fa fa-search" aria-hidden="true"></i></span>
      <input type="hidden" id="opdmatchbrowsenode" value="">            
    </form>
  </div>
  <div id="ajaxresults" style="display:none"></div>
</section>
<div class=hro_opdte><a onclick="GAInteractionEventTracking('view all', 'Open Day Search', 'View All', 'view all', '');" href="/open-days/search/"><spring:message code="view.all.vitual.button.text"/> <i class="fa fa-long-arrow-right"></i></a></div>
<script type="text/javascript">
  function onclickfunction(){
    opendaysSearchSubmit(uniOpenDayName);
    var ajaxSelectedName = document.getElementById("uniOpenDayName_display").value;   
    if( ajaxSelectedName != ""){
      ajaxSelectedName = replceSpecialcharacter(ajaxSelectedName);
      GAInteractionEventTracking('view all', 'Open Day Search', 'Ajax',ajaxSelectedName, '');
    }
  }
</script>