<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>

<%
String pageName = (String)request.getParameter("PAGE_NAME");
String paramName = "BOOKING_FORM".equalsIgnoreCase(pageName) ? "WU OPEN DAYS BOOKING FORM" : "WU OPEN DAYS PROVIDER PAGE";
String paramFlag = "BOOKING_FORM".equalsIgnoreCase(pageName) ? "OPEN_DAY_BOOKING_FORM" : "OPEN_DAY_LANDING_PAGE";
String noindexfollow = "BOOKING_FORM".equalsIgnoreCase(pageName) ? "noindex,follow" : "index,follow";

%>

<meta http-equiv="content-language" content=" en-gb "/>
<link rel="stylesheet" type="text/css" href="<wu:csspath source='/cssstyles/'/><spring:message code='wuni.whatuni.main.header.css'/>" media="screen" />
<link rel="stylesheet" type="text/css" href="<wu:csspath source='/cssstyles/'/><spring:message code='wuni.whatuni.main.styles.css'/>" media="screen" />
<link id="size-stylesheet" rel="stylesheet" href="" type="text/css" media="screen" />
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
<c:if test="${param.PAGE_NAME eq 'BOOKING_FORM'}">  
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/openday/common.js'/>"></script>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/openday/form/booking_form.js'/>"> </script>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.facebook.login.js'/>"></script>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/emaildomain/'/><spring:message code='wuni.email.domain.js'/>"></script>
</c:if>
<c:if test="${param.PAGE_NAME eq 'OPEN_DAY_PROVIDER_LANDING_PAGE'}">
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/openday/'/><spring:message code='wuni.openday.landing.js'/>"></script>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/openday/provider/'/><spring:message code='open.day.provider.landing.page'/>"></script>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.retina.js'/>"></script>
  <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.cpe.stats.log.js'/>"> </script>
</c:if>

<jsp:include page="/jsp/common/includeIconImg.jsp"/>
<!--[if IE 7]>    <link rel="stylesheet" type="text/css" href="<%=CommonUtil.getCSSPath()%>/cssstyles/whatuni-screens_ie7hacks-wu552.css" media="screen" />   <![endif]-->
<jsp:include page="/jsp/common/abTesting.jsp"/>

<%if(request.getAttribute("showBanner")==null){%>
  <jsp:include page="/jsp/thirdpartytools/includeGAMBanner.jsp"/>
<%}%>

<%-- GTM script included under head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
  <jsp:param name="PLACE_TO_INCLUDE" value="HEAD" />
</jsp:include>
<%-- Facebook pixel tracking script included in head tag --%>
<jsp:include page="/jsp/thirdpartytools/include/facebookPixelTracking.jsp"/>
<jsp:include page="/jsp/common/includeSeoMetaDetails.jsp">
  <jsp:param value="<%= noindexfollow%>" name="noindexfollow"/>
</jsp:include>