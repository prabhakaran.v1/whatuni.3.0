<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator, mobile.util.MobileUtils, WUI.utilities.SessionData" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%String imageSrc = CommonUtil.getImgPath("/wu-cont/images/opd_heroimg.jpg", 1); 
%>

<body id="landingBody" class="odbnr_lp">
<header id="opnHead" class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp"/>        
      </div>    
    </div>
  </div>
</header> 
<div class="container1">
<div class="opd_lp fl no_virt">
  <div id="opnHro" class="hero_cnt">
    <img id="openDayHeroImage" src="<%=imageSrc%>" data-src="<%=CommonUtil.getImgPath("/wu-cont/images/opd_heroimg.jpg", 1)%>"  alt="opendays hero image" title="" class="hro_img">
      <div class="hro_grd">
        <div class="gry_grd"></div>
      </div>
      <div class="ad_cnr">
        <div class="content-bg">
          <div class="hro_srch">
            <jsp:include page="/jsp/openday/include/openDaysKeywordSearchPod.jsp"/>
            
<%--             <c:if test="${COVID19_SYSVAR eq 'ON'}"> --%>
<%--               <spring:message code="advice.teacher.url" var="teacherSectionUrl"/> --%>
<!--               <div class="covid_opd"> -->
<!--                 <div class="covid_upt"> -->
<!--                   <div class="covid_gbg"> -->
<%--                     <span class="covid_txt"><%=new SessionData().getData(request, "COVID19_OPENDAY_TEXT") %></span> --%>
<%--                     <a href="${teacherSectionUrl}" onclick="GACovid19Logging();" class="covid_link"><span class="linfo">Latest info</span><img src="<%=CommonUtil.getImgPath("/wu-cont/images/covid_blu_arw.svg", 0)%>"></a> --%>
<!--                   </div>   -->
<!--                 </div> -->
<!--               </div> -->
<%--             </c:if> --%>
            
          </div>
        </div>
      </div>
    </div>
    <div class="opd_res no_virt" id="futureOpenDaysDiv">
      <jsp:include page="/jsp/openday/include/openDaysUpcommingPod.jsp"/>
    </div>
    <div class="wtask_cnt">
    <div class="ad_cnr">
      <jsp:include page="/jsp/openday/include/openDaysWhatToAsk.jsp"/>
      </div>
    </div>
    <div class="odadv_cnt" id="articlesPod">
      <input type="hidden" id="scrollStop" value="" />
    </div>
  </div>
  <jsp:include page="/jsp/common/wuFooter.jsp" />
  </div>
</body>
