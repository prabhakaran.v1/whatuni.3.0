<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, WUI.utilities.GlobalFunction, org.apache.commons.validator.GenericValidator"%>
<%
    CommonFunction commonFun = new CommonFunction();
    String enVName = commonFun.getWUSysVarValue("WU_ENV_NAME");
    String domainSpecificsPath = commonFun.getSchemeName(request)+ CommonUtil.getResourceMessage("wuni.whatuni.device.specific.css.path", null);
    String gaProviderCollegeName = "";
    String indexfollow = "index,follow";
    String collegeId = "", canonUrl = "";
    if(request.getAttribute("collegeId")!=null && !"".equals(request.getAttribute("collegeId"))){
      collegeId = (String)request.getAttribute("collegeId");
    }
    if(request.getAttribute("provCanonicalUrl")!=null && !"".equals(request.getAttribute("provCanonicalUrl"))){
      canonUrl = (String)request.getAttribute("provCanonicalUrl");
    }
    String exitOpenDayRes = "" , odEvntAction = "false";
    if(session.getAttribute("exitOpRes")!=null && !"".equals(session.getAttribute("exitOpRes"))){
      exitOpenDayRes = (String)session.getAttribute("exitOpRes");
    }    
    if(!"".equals(exitOpenDayRes) && "true".equals(exitOpenDayRes)){
      odEvntAction = "true";
    }
    if(request.getAttribute("noindex")!=null && "true".equals(request.getAttribute("noindex"))){
      indexfollow = "noindex,follow";
    }
     String gaCollegeName = "";
     String dateClassName = "ucod_lftyr fl";
     String websitePrice = (String)request.getAttribute("websitePrice") != "" ? (String)request.getAttribute("websitePrice") : "";
      String openMonthYear = (String)request.getAttribute("openMonthYear") != "" ? (String)request.getAttribute("openMonthYear") : "";
    String orderItemId = (String)request.getAttribute("orderItemId") != "" ? (String)request.getAttribute("orderItemId") : "";
    String networkId = (String)request.getAttribute("networkId") != "" ? (String)request.getAttribute("networkId") : "";
    String bookingUrl = (String)request.getAttribute("bookingUrl") != "" ? (String)request.getAttribute("bookingUrl") : "";
  String openDayMonthDesc = "", stTimeDesc = "", edTimeDesc = "";
  String collegeName = (String)request.getAttribute("interactionCollegeName");
  if(request.getAttribute("provCanonicalUrl")!=null && !"".equals(request.getAttribute("provCanonicalUrl"))){
    canonUrl = (String)request.getAttribute("provCanonicalUrl");
  }
  String openDaySaveCal = "";
   String lazyLoadJs = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.lazyLoadJs.js");
  String providerLogo = !GenericValidator.isBlankOrNull((String)request.getAttribute("providerLogo")) ? CommonUtil.getImgPath("",0) + (String)request.getAttribute("providerLogo") : "";
  String totalRecordCount = (String)request.getAttribute("totalRecordCount");  
  String openDayMonthYearDesc = (String)request.getAttribute("openDayMonthYearDesc") != "" ? (String)request.getAttribute("openDayMonthYearDesc") : "";
  String openDayMainCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.styles.css", null);//getting css name dynamically by Sangeeth.S for 31_Jul_18 rel
  String openDayHeaderCssName = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);  
  String openDayLandingJsName = CommonUtil.getResourceMessage("wuni.openday.landing.js", null);//getting Js name dynamically by Sangeeth.S for 20_Nov_18 rel
  %>  

<body class="odbnr_pl">
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/openday/<%=openDayLandingJsName%>"></script>      
</header>
<!-- Opendays Redesign Code -->
<c:if test="${not empty requestScope.uniOpenDaysInfoList}">
<div class="opd_det fl no_virt">
  <div class="bcrmb_hd fl">
  <div class="ad_cnr">
    <div class="content-bg">    
                  <jsp:include page="/seopods/breadCrumbs.jsp">
                    <jsp:param name="pageName" value="OPEN_DAY_PROFILE"/>
                  </jsp:include>
                
      <c:if test="${not empty requestScope.uniInfoList}">
       
      <div class="opuni_srch det_res">
      <c:forEach var="uniList" items="${requestScope.uniInfoList}">
        <div class="dr_col1 fl">      
          <div class="drc_lft fl">            
             <img class="pro_logo" src="<%=CommonUtil.getImgPath("",0)%>${uniList.mediaPath}" alt="${uniList.collegeNameDisplay}">  
          </div>     
          <div class="drc_rht fl">
            <h1><a href="${uniList.collegeLandingUrl}" alt="${uniList.collegeNameDisplay}"> ${uniList.collegeNameDisplay}</a></h1>
            <c:if test="${uniList.reviewCount ne 0}">
            <div class="rate_new"><span class="opd_srat">Student rating</span><span class="ml5 rat${uniList.overAllRating} t_tip">
            <span class="cmp" >
             <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
             <div class="line"></div>
            </span>
          </span> (${uniList.overAllRatingExact})
          <a class="link_blue nowc" href="${uniList.uniReviewsURL}" title="Reviews at ${uniList.collegeNameDisplay}">
            ${uniList.reviewCount}<c:if test="${uniList.reviewCount eq 1}"> review</c:if><c:if test="${uniList.reviewCount gt 1}"> reviews</c:if>
          </a>
        </div>
        </c:if>
        <div class="hor_cmp">
        <c:if test="${not empty uniList.collegeInBasket}">
          <c:set var="uniInBasket" value="${uniList.collegeInBasket}"/>
               <c:if test="${not empty uniList.collegeNameDisplay}">
                 <c:set var="providerCollegeName" value="${uniList.collegeNameDisplay}"/>
                       <%gaProviderCollegeName = commonFun.replaceSpecialCharacter((String)pageContext.getAttribute("providerCollegeName")).toLowerCase(); %>
                </c:if>             
                <%request.setAttribute("COLLEGE_IN_BASKET", (String)pageContext.getAttribute("uniInBasket"));%>
                <jsp:include page="/jsp/advertiser/ip/include/headShortlist.jsp">
                  <jsp:param name="richProfile" value="true"/>
                  <jsp:param name="basketCollegeName" value="<%=gaProviderCollegeName%>"/>
                </jsp:include>                
            </c:if>
        </div>
      </div>
    </div>
   </c:forEach>
    <c:if test="${not empty requestScope.recentOpendayList}">
    <c:forEach var="recentOpendayList" items="${requestScope.recentOpendayList}">
     <c:set var="uniDisplayName" value="${recentOpendayList.collegeName}"/>
             <%gaCollegeName= commonFun.replaceSpecialCharacter(new GlobalFunction().getReplacedString((String)pageContext.getAttribute("uniDisplayName")));%>
    <c:if test="${not empty recentOpendayList.orderItemId}">
    <c:if test="${not empty recentOpendayList.bookingUrl}">
    <div class="dr_col2 fl">
      <div class="drc2_cnt fl">
        <div class="drc2_dte">${recentOpendayList.openDate}</div>
        <div class="drc2_btn">
          <div class="sr_btn_cont fl">
            <div class="res_place">
            <c:if test="${not empty recentOpendayList.orderItemId}">
              <a target="_blank" href="${recentOpendayList.bookingUrl}" onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', ${recentOpendayList.websitePrice}); addOpenDaysForReservePlace('${recentOpendayList.openDay}', '${recentOpendayList.openMonth}', '<%=gaCollegeName%>', '${recentOpendayList.orderItemId}', '${recentOpendayList.networkId}');cpeODReservePlace(this,'${recentOpendayList.collegeId}','${recentOpendayList.orderItemId}','${recentOpendayList.networkId}','${recentOpendayList.bookingUrl}');var a='s.tl(';GAEventTracking('open-days', 'reserve-place', '<%=gaCollegeName%>','1');closeOpenDaydRes();" rel="nofollow">Reserve a place<i class="fa fa-caret-right"></i></a>
            </c:if>
            </div>
          </div>
        </div>
      </div>
    </div>  
    </c:if>
    </c:if>
    </c:forEach>
    </c:if>
  </div></c:if>
  </div>
  </div>
  </div>
  
  
  <!-- -->
  <div class="opd_res">
  <div class="ad_cnr">
    <div class="content-bg">
    <div class="odres_lst">
    <h3>Next open days</h3>
    <c:forEach var="openDaysInfoList" items="${requestScope.uniOpenDaysInfoList}">     
        <!-- Open days details pod1 -->
        <div class="odres_wrp">
          <div class="odres_tit">
            <span></span>
            <span>${openDaysInfoList.openMonthYear}</span>
            <span></span>
          </div>
        <!-- UCAS Pod 1 -->
        <c:if test="${not empty openDaysInfoList.openDaysProviderResultList}">
        <c:forEach var="openDaysInfoListInnerCursor" items="${openDaysInfoList.openDaysProviderResultList}">
          <c:set var="collegeGAName" value="${openDaysInfoListInnerCursor.collegeName}"/>  
            <div itemprop="event" itemscope itemtype="http://schema.org/EducationEvent">  
                 <%gaCollegeName= commonFun.replaceSpecialCharacter(new GlobalFunction().getReplacedString((String)pageContext.getAttribute("collegeGAName")));%>
              <c:if test="${not empty openDaysInfoListInnerCursor.weekendFlag}">
               <c:set var="weekendFlag" value="${openDaysInfoListInnerCursor.weekendFlag}"/>     
               <%dateClassName ="";
               if("Y".equalsIgnoreCase((String)pageContext.getAttribute("weekendFlag"))){
                             dateClassName = "sat";
                           }
                           %>
              </c:if>
              <div class="ucod_Cnr">
                <div class="ucod_lftyr fl <%=dateClassName%>">
                  <div class="odres_wek">${openDaysInfoListInnerCursor.openDay}</div>
                  <div class="odres_dte">${openDaysInfoListInnerCursor.openDate}</div>
                  <div class="odres_mon">${openDaysInfoListInnerCursor.openMonth}</div>
                  <div itemprop="name" style="display:none;">${openDaysInfoListInnerCursor.headline}</div>
                  <div itemprop="startDate" style="display:none;">${openDaysInfoListInnerCursor.stDateTimeStamp}</div>
                  <div itemprop="endDate" style="display:none;">${openDaysInfoListInnerCursor.edDateTimeStamp}</div>
                  <div itemprop="description" style="display:none;">${openDaysInfoListInnerCursor.eventDesc}</div>
                </div>
                <div class="ucod_wrap fl">
                  <div class="lft_pod">
                  <c:if test="${not empty openDaysInfoListInnerCursor.logoPath}">
                    <c:set var="collegeLogoPath" value="${openDaysInfoListInnerCursor.logoPath}"/>
                    <a itemprop="url" href="<%=domainSpecificsPath%>${openDaysInfoListInnerCursor.opendaysProviderURL}"> 
                        <img class="pro_logo" src="<%=CommonUtil.getImgPath("",0)%>${openDaysInfoListInnerCursor.logoPath}"  alt="${openDaysInfoListInnerCursor.collegeName}" title="${openDaysInfoListInnerCursor.collegeName}">  
                    </a>
                   </c:if>
                  </div>
                  <div class="rht_pod rhtsr_pod fl">
                    <div class="opdet_wrp fl" itemprop="location" itemscope itemtype="http://schema.org/Place">
                      <h2 itemprop="name">${openDaysInfoListInnerCursor.collegeNameDisplay}</h2>
                      <p class="od_descr">${openDaysInfoListInnerCursor.qualDesc}</p>
                      <p class="od_loc" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><i class="fa fa-map-marker" aria-hidden="true"></i><span itemprop="addressLocality">${openDaysInfoListInnerCursor.opendayVenue}</span><c:if test="${not empty openDaysInfoListInnerCursor.startTime}"> - </c:if><span>${openDaysInfoListInnerCursor.startTime} <c:if test="${not empty openDaysInfoListInnerCursor.endTime}"> - ${openDaysInfoListInnerCursor.endTime}</c:if></span></p>
                      <p class="odlst_des fl">${openDaysInfoListInnerCursor.eventDesc}</p>
                    </div>
                  </div>
                  <div class="sr_btn_cont fl">
                              <div class="sav_cal" id="openDay_${openDaysInfoListInnerCursor.eventId}">
                              <c:if test="${not empty sessionScope.userInfoList}">
                                <c:if test="${openDaysInfoListInnerCursor.opendayExist ne 'TRUE'}">
                                        <a title="Add ${openDaysInfoListInnerCursor.collegeNameDisplay} open day to calendar" id="calBtn_${openDaysInfoListInnerCursor.eventId}" onclick="javascript:addOpendaystocalendar('${openDaysInfoListInnerCursor.openDate}', '${openDaysInfoList.openMonthYear}', '${openDaysInfoListInnerCursor.collegeId}', '${openDaysInfoListInnerCursor.eventId}', '', '${openDaysInfoListInnerCursor.orderItemId}', '${openDaysInfoListInnerCursor.networkId}', '${openDaysInfoListInnerCursor.eventId}', '', '${openDaysInfoListInnerCursor.websitePrice}', '${openDaysInfoListInnerCursor.bookingUrl}')">
                                          Save to calendar<i class="far fa-calendar-alt"></i>
                                        </a>                             
                                      </c:if>
                                      <c:if test="${openDaysInfoListInnerCursor.opendayExist eq 'TRUE'}">
                                        <a class="saved"><i class="far fa-calendar-alt fa-calendar-check"></i>Saved to calendar</a>
                                      </c:if>
                                    </c:if>
                                    <c:if test="${empty sessionScope.userInfoList}">
                                      <a title="Add ${openDaysInfoListInnerCursor.collegeNameDisplay} open day to calendar" id="calBtn_${openDaysInfoListInnerCursor.eventId}" href="javascript:showLightBoxLoginForm('popup-newlogin',650,500,'','','add-open-days','<%=gaCollegeName%>#${openDaysInfoListInnerCursor.eventId}#${openDaysInfoListInnerCursor.collegeId}#${openDaysInfoListInnerCursor.openDate}#${openDaysInfoList.openMonthYear}#${openDaysInfoListInnerCursor.orderItemId}#${openDaysInfoListInnerCursor.qualId}#${openDaysInfoListInnerCursor.websitePrice}#${openDaysInfoListInnerCursor.bookingUrl}');">Save to calendar<i class="far fa-calendar-alt"></i></a>                                   
                                    </c:if>
                                    <a id="calBtnAct_${openDaysInfoListInnerCursor.eventId}"  style="display:none;" class="saved"><i class="far fa-calendar-alt fa-calendar-check"></i>Saved to calendar</a>
                            </div>
                            <c:if test="${not empty openDaysInfoListInnerCursor.orderItemId}">
                                   <c:if test="${not empty openDaysInfoListInnerCursor.bookingUrl}">       
                            <div class="res_place">
                            <a target="_blank" href="${openDaysInfoListInnerCursor.bookingUrl}" onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', ${openDaysInfoListInnerCursor.websitePrice}); addOpenDaysForReservePlace('${openDaysInfoListInnerCursor.openDate}', '${openDaysInfoList.openMonthYear}', '${openDaysInfoListInnerCursor.collegeId}', '${openDaysInfoListInnerCursor.orderItemId}', '${openDaysInfoListInnerCursor.networkId}');cpeODReservePlace(this,'${openDaysInfoListInnerCursor.collegeId}','${openDaysInfoListInnerCursor.orderItemId}','${openDaysInfoListInnerCursor.networkId}','${openDaysInfoListInnerCursor.bookingUrl}');var a='s.tl(';GAEventTracking('open-days', 'reserve-place', '<%=gaCollegeName%>','1');closeOpenDaydRes();" rel="nofollow">Reserve a place<i class="fa fa-caret-right"></i></a>
                            </div>
                            </c:if>
                            </c:if>
                   </div>
                 </div>
               </div>
             </div>
          </c:forEach>    
        </c:if>
       </div>      
    </c:forEach>
    <c:if test="${empty requestScope.noResultsFlag}">
        <%    
          String pageURL = (String)request.getAttribute("pageUrl") != "" ? (String)request.getAttribute("pageUrl") : "";
          String pageNo = request.getParameter("pageno") != null ? request.getParameter("pageno") : "1"; 
        %>
        <div class="pr_pagn">
        <jsp:include page="/reviewRedesign/include/reviewPagination.jsp">
          <jsp:param name="pageno" value="<%=pageNo%>"/>   
           <jsp:param name="pagelimit"  value="10"/>
          <jsp:param name="searchhits" value="<%=totalRecordCount%>"/>
          <jsp:param name="recorddisplay" value="10"/>              
          <jsp:param name="pageURL" value="<%=pageURL%>"/>
          <jsp:param name="orderBy" value=""/>
          <jsp:param name="pageTitle" value=""/>
        </jsp:include>
        </div>
   </c:if>
    </div>
    
    </div>
   </div>
   </div>
  <!-- -->
  </div>
  <c:if test="${not empty requestScope.newOdReg}">
  <input type="hidden" id="newOdReg" value="${requestScope.newOdReg}"/>  
  <input type="hidden" id="odColName" value="${requestScope.collegeDispName}"/>
</c:if> 
<c:if test="${not empty requestScope.odEventId}">
  <script type="text/javascript">triggerAddToCalendar('${requestScope.odEventId}')</script>
</c:if>
<c:if test="${not empty requestScope.odResUniIds}">
  <script type="text/javascript">triggerResUniAjax('${requestScope.odResUniIds}')</script>
</c:if>

<input type="hidden" id="openDaySProviderName"/>
  <input type="hidden" id="openDaySEventId"/>
  <input type="hidden" id="openDaySCollegeId"/>
  <input type="hidden" id="openDaySDateDay"/>
  <input type="hidden" id="openDaySDateMonthYear"/>
  <input type="hidden" id="openDaySubOrderItemId"/>
  <input type="hidden" id="openDayCpeQualId"/>  
  <input type="hidden" id="fullOpenDayUrlwithparam" value="<%=(String)request.getAttribute("fullOpenDayUrlwithparam")%>">
  <input type="hidden" id="opendayproviderName" value="<%=collegeName%>"/>
  <input type="hidden" id="opendayHeading" value="<%=openDaySaveCal%>"/>
    <input type="hidden" name="openDayBookingUrl" id="openDayBookingUrl" value=""/>
    <input type="hidden" name="openDayWebPrice" id="openDayWebPrice" value=""/>
    <input type="hidden" name="opendayType" id="opendayType" value=""/>
   
     <jsp:include page="/jsp/common/wuFooter.jsp" />
     <c:set var="collegeId" value="<%=collegeId%>"/>
     <c:if test="${not empty collegeId}">
  <script type="text/javascript">providerPageViewLogging('<%=collegeId%>','PROVIDER_OPENDAYS_VIEW');</script> 
</c:if>
</c:if>
</body>
