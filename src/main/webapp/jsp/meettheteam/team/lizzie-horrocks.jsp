<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Lizzie Horrocks"><span class="Lizzie h128"></span> <span class="blue">Lizzie Horrocks</span>Marketing & Outreach Officer</a>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Lizzie (also known as Taylor Swift in the office) began her role as Marketing and Outreach officer in September last year following her Linguistics degree at Queen Mary University. She presents in schools, colleges and sixth forms across the UK, speaking to students about how to find the right course and university using both the Whatuni website and app. Famous for her incessant use of puns and the founder of "UCASay that again", she also writes articles for the advice section of the site, further assisting students with their independent research.</p>
      <p>Outside the office, Lizzie enjoys brunching, running, reading and yoga.</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->