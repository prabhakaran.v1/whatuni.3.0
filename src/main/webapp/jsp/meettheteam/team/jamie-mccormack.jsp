<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Jamie McCormack"><span class="Jamie h128"></span> <span class="blue">Jamie McCormack</span>Field Marketing Executive</a>
        <span class="mtsoc">
        <a href="https://www.linkedin.com/in/jamie-mccormack-94b836101?trk=nav_responsive_tab_profile_pic" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
        <!--<a href="https://twitter.com/MiaT_O" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>--> 
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>As a member of the Outreach team, Jamie's job involves travelling the UK in search of WUSCA-winning academic institutions, talking to students about their experiences. As a History and French graduate from the University of Birmingham, Jamie jumps at the chance to use his linguistic skills on campus, chatting to French students and the occasional professor with varying degrees of success.</p>      
      <p>Jamie also attends careers events in schools, aiming to help as many students as possible through the stress/excitement/concern/joy inspired by the university application process.</p>
      <p>A man who lives for sport, he regularly plays Rugby and Cricket to a high standard. He keeps the office amused with his excellent Scottish and Australian accents, and his weaker attempts at Welsh are a particular highlight. Bringing a touch of flamboyance, a live stream of whatever pops into his head and a healthy dose of Celtic fiddle music to the table, Jamie is a hugely valued member of the WhatUni crew.</p>      
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->


