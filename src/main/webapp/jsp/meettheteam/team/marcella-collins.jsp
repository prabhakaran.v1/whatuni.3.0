<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Marcella Collins"><span class="Marcella h128"></span> <span class="blue">Marcella Collins</span>Managing Director UK</a>        
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Marcella joined the Hotcourses Group in 2009 and now oversees our Higher Education websites that include <a href="https://www.whatuni.com">www.whatuni.com</a>, <a href="http://www.thecompleteuniversityguide.co.uk" target="_blank">www.thecompleteuniversityguide.co.uk</a> and <a href="http://www.postgraduatesearch.com" target="_blank">www.postgraduatesearch.com</a>.</p>
      <p>After studying Music at Sheffield Marcella decided to move to the 'Big Smoke' to start her career in advertising. After a few years working in mobile marketing and then for an advertising agency she found the perfect role working with universities and colleges at Hotcourses and hasn't looked back since.</p>
      <p>She understands that finding the right university can be a bit overwhelming, and works across all the teams to offer students the best tools and advice to guide them through the process to allow them to choose a university that will give them a brilliant university experience.</p>
      <p>In her spare time Marcella can be found playing rugby for Saracens or walking her two lovely Miniature Schnauzers, Muffin & Tinker. </p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->