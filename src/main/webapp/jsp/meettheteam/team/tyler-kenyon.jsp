<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Tyler Kenyon"><span class="Tyler h128"></span> <span class="blue">Tyler Kenyon</span>Senior Marketing Manager</a>
        <span class="mtsoc">
          <a href="https://uk.linkedin.com/pub/tyler-kenyon/9/5a0/bbb/" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
          <a href="https://twitter.com/hey_tk" title="Twitter" target="_blank"><i class="fa fa-twitter"></i> </a>          
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Tyler joined the team at Whatuni in September 2014.  Before that he has a somewhat dodgy history that no one is actually too sure about.   He claims to have been in movies, worked in public relations, and even tried publishing (there is little evidence to support any of that).  He also has a funny accent, having grown up in Canada, lived in the US for a couple years, then arrived in London by way of some hot sandy places.</p>      
      <p>His degree is in Political Science, with a postgrad in journalism, and he is a connoisseur of single malt whiskies from the Speyside region.  Combining the two will result in utter boredom.  You've been warned.</p>
      <p>In any event, he works on everything from social media and search engine optimisation, to email marketing and partnerships.  So if you're getting too many emails from him, feel free to return the favour with the below contact details.  Or if you'd like to make his day, just send him a tweet saying hello, or a ridiculous meme (he is addicted to memes).  Why are they so funny?!</p>      
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->
