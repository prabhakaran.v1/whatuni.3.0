<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Jade Whittaker"><span class="Jade h128"></span> <span class="blue">Jade Whittaker</span>Project Co-Ordinator</a>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Jade manages the annual review generation project, coordinating a team who travel up and down the country to collect genuine and insightful student reviews. She's also involved in the event planning for the Whatuni Student Choice Awards, which celebrates universities and all they can do for students.</p>      
      <p>Jade's personal interests include trampoline boxing and putting bow ties on her cat.</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->