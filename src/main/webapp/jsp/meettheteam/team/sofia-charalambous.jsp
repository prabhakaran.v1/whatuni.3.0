<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Sofia Charalambous"><span class="Sofia h128"></span> <span class="blue">Sofia Charalambous</span>Field Marketing Executive</a>
        <span class="mtsoc">
          <a href="https://www.linkedin.com/in/sofia-charalambous-2a2691116/" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>When she's not out and about giving presentations to sixth formers, collecting reviews or attending career fairs to help students with their university research, Sofia can be found enthusiastically decorating the office whiteboard and finding the perfect filter for the Whatuni Snapchat channel.</p>
      <p>Sofia is a newly converted veggie who struggles daily to avoid her former favourite non-veggie foods. Before joining the outreach team, she escaped from busy London and moved to the Bournemouth seaside to attend University.</p>      
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->