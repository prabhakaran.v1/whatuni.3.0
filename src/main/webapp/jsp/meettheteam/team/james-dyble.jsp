<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="James Dyble"><span class="James h128"></span> <span class="blue">James Dyble</span>Outreach Marketing Executive</a>        
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Recently back from his self-proclaimed 'quarter life crisis' travelling around Australasia without managing to get eaten by the wildlife, James has joined as part of the Outreach team at Whatuni. Sharing his wisdom on universities and the magical world of Whatuni to sixth-formers at careers events and presentations in a school near you. He's also responsible for student review collection on universities across the country, getting the real deal on what students think of their uni. With his favourite review being on one student's hatred for green wallpaper at one universities facilities.</p>
      <p>Prior to this chapter of his life, this Norfolk boy made the long old journey out of the countryside to the Midlands in order to study Chemistry at the University of Nottingham. With an industrial placement year getting friendly with the Geordies in Newcastle. James can be regularly found doing a right mix activities across the country in his spare time; from giving tchoukball a go to getting painted into some random creature or cutting shapes somewhere.</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->