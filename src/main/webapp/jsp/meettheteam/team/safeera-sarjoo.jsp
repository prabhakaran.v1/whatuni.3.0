<%--
  * @purpose  This is included for meet the team member content jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 17-DEC-2019 Sangeeth.S               	 597     First Draft                                                       597 
  * *************************************************************************************************************************
--%>	
<div class="fr rht_sec over_view">  
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Safeera Sarjoo"><span class="Safeera h128"></span> <span class="blue">Safeera Sarjoo</span> Editor</a>       
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>An avid storyteller, Safeera studied Journalism at Kingston University, London where she developed her love for writing and even landed an interview with Kim Kardashian!</p>
	  <p>As Editor at Whatuni, she splits her time writing advice for prospective students, leading the social media channels to inspire people and generally helping to bring Whatuni's vision to life. This includes co-running the Whatuni Student Advisory Board and working collaboratively with universities.</p> 
	  <p>In her spare time Safeera can be found planning her next holiday (most likely to New York), on radio and at events championing diversity and marginalised communities, or simply curled up in a coffee shop lost in a book.</p>      
    </div>
  </div>  
</div>
