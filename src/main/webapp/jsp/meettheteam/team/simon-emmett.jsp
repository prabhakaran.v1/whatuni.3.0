<%@page import="WUI.utilities.CommonFunction" %>
<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Simon Emmett"><span class="simon h128"></span> <span class="blue">Simon Emmett</span>CEO</a>
        <span class="mtsoc">
          <a href="https://uk.linkedin.com/pub/simon-emmett/14/1b/b84" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>          
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">  
      <!--URL restructure - Added Uni Home URL for 08_Mar_2016 By Thiyagu G.-->
      <p>Simon joined the Hotcourses group in 2000, shortly after the launch of the internet.  After graduating in Economics from the <a href="<%=new CommonFunction().getSchemeName(request)%>www.whatuni.com/university-profile/university-of-warwick/3771/" title="University of Warwick">University of Warwick</a> he travelled the world for a year.  Upon his return to the UK he applied for a telesales role at Hotcourses in London because he couldn't afford his train ticket back to his home town of Blackpool, the Vegas of the North.  Almost 14 years on he's still working at Hotcourses - we just can't get rid of him.  Simon has worked throughout and managed the UK sales teams and then set up the US sales operation in Boston.</p>
      <p>In 2011 he was given responsibility to balance the books while delivering the very best content, user experience, marketing and partnerships.  As MD of Hotcourses UK the websites he's now responsible for include Whatuni, hotcourses.com and postgraduatesearch.com - they receive over 25 million visits each year.</p>
      <p>Like the rest of the Whatuni team he's passionate about providing market leading products to help students find the right college or university for them. He's proud to have the support of a dynamic and creative team as well as key education sector experts on the Whatuni advisory group. This group includes Professor Michael Arthur, representatives from the Higher Education Academy, HELOA, HEERA and highly experienced careers advisors.</p>
      <p>Simon has been a supporter of the <a target="_blank" href="http://www.hotcoursesfoundation.org/" title="www.HotcoursesFoundation.org">www.HotcoursesFoundation.org</a> and has visited the projects several times to see the amazing work and support they offer.  Unfortunately he was less than popular when he arranged for his sister to volunteer at the Hotcourses school in Kitui, Kenya - only for her to contract Malaria after two months.</p>
      <p>In his spare time he loves to cook seafood dishes, climb mountains (hills) and is an FA qualified coach.  He also loves to travel and is desperate to make it to 50 countries visited next year.</p>                                                     
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->
