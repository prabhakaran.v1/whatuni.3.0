<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Danielle Lapham"><span class="Danielle h128"></span> <span class="blue">Danielle Lapham</span>Search Engine Marketer</a>
        <span class="mtsoc">
          <a href="https://www.linkedin.com/in/dannilapham" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
          <a href="https://www.twitter.com/labouchezla" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>          
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Dan's had a long journey to get to Whatuni, having gained several years' experience working in all sorts of weird and wonderful places, from demolition sites to all-you-can eat buffets, from being an accounts assistant to a Cher tribute act, scatting to jazz and singing in a Thai rock band; it took a lot of terrible songs and a few trial balances for Dan to figure out that her niche was actually digital. If she could turn back time, she'd have ditched the Cher thing.</p>
      <p>As one of newest recruits in the HE digital marketing team Dan has responsibility for injecting mobile first thinking into everything we do. She likes naps, Crossfit and hot sauce on everything.</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->