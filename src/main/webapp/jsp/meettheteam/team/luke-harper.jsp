<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Luke Harper"><span class="Luke h128"></span> <span class="blue">Luke Harper</span>Product Manager</a>
        <span class="mtsoc">
          <a href="http://uk.linkedin.com/in/lukeharperux" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
          <a href="https://twitter.com/LukeHarper1990" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>          
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>After joining the company in May 2013 as part of the Whatuni Marketing team, Luke now has more of a focus on the website and its development. This involves working closely with the data and insights team to ensure the Whatuni product is fit for purpose and contains the most relevant and up to date information for students, so the site can go over and above its competitors in the Higher Education sector.  Luke is also responsible for all functional and aesthetic changes to Whatuni.com, and he works closely with the design and web development teams to keep the site up to date and easy to use for students.</p>
      <p>If Luke isn't listening to Fleetwood Mac at his desk whilst writing fancy colour coded functional requirements, he's likely to be doing some masterful sketching of new page layouts which will no doubt give our talented designers and developers a headache.</p>
      <p>In his spare time you'll often find him trying his hand at playing rugby or cheering on his favourite football team (Tottenham Hotspur). Otherwise he's likely to be cooking up something tasty in the kitchen (most likely an authentic Indian Curry).</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->
