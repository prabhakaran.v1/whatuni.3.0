<%--
  * @purpose  This is included for meet the team member content jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 17-DEC-2019 Sangeeth.S               	 597     First Draft                                                       597 
  * *************************************************************************************************************************
--%>	
<div class="fr rht_sec over_view">  
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Carmen Molina"><span class="Carmen h128"></span> <span class="blue">Carmen Molina</span>Senior Marketing Executive</a>       
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Carmen joined IDP Connect in February 2019. She is responsible of the implementation of the
				HE marketing strategy, ensuring that lots of people hear about the great work we do here at
				Whatuni in helping students find the right universities for them.</p>
			<p>Outside the office, Carmen enjoys spending time with friends, doing sport, going to the theatre,
			reading and travelling.</p>	      
    </div>
  </div>  
</div>