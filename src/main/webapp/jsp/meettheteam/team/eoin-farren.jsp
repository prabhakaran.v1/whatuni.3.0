<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Eoin Farren"><span class="Eoin h128"></span> <span class="blue">Eoin Farren</span>Account Manager</a>
        <span class="mtsoc">
          <a href="https://uk.linkedin.com/in/eoin-farren-b37a915a" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Eoin joined Hotcourses as an Account Manager in July 2016, after spending three years in the SaaS sector. He looks after a diverse portfolio of higher education providers across the UK, and enjoys working closely with these clients to match students up with the right university.</p>
      <p>After graduating with a degree in Journalism from Dublin City University, he embarked on a brief but eventful career as a local news reporter, before crossing the Irish Sea to set up home in South West London.</p>
      <p>When not at Hotcourses, Eoin can usually be found trying out the many excellent restaurants Brixton has to offer, catching up on news/current affairs, or enjoying the very occasional jog around Brockwell Park.</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->