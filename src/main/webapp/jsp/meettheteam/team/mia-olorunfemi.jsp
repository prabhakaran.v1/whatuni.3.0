<%@page import="WUI.utilities.CommonFunction" %>
<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Mia Olorunfemi"><span class="Mia h128"></span> <span class="blue">Mia Olorunfemi</span>Outreach Manager</a>
        <span class="mtsoc">
          <a href="http://uk.linkedin.com/pub/mia-olorunfemi/3a/2b0/41b" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
          <a href="https://twitter.com/MiaT_O" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>          
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">    
      <!--URL restructure - Added Uni Home URL for 08_Mar_2016 By Thiyagu G.-->
      <p>Mia has had a pretty extensive journey through higher education. Having completed an undergraduate degree in English at <a href="<%=new CommonFunction().getSchemeName(request)%>www.whatuni.com/university-profile/goldsmiths-university-of-london/859/">Goldsmiths University</a> and a masters in Publishing Studies at <a href="<%=new CommonFunction().getSchemeName(request)%>www.whatuni.com/university-profile/city-university/7287/">City University</a> She's full of advice on the range of options that are available to get through the university application and funding process, arranging to have a presence at school careers and higher education events to do just this.</p>      
      <p>She very much believes in the value of promoting learning, and the paths to building a fulfilling future. Her experience in organising education events and building links with students and academic staff, have made her inclined to constantly think about improving services to a desired audience, and reaching out to them engagingly. A great outlet for this is also her involvement in the organisation of the <a href="<%=new CommonFunction().getSchemeName(request)%>www.whatuni.com/awards">Whatuni Student Choice Awards</a> and building links with partners.</p>
      <p>She finds too much entertainment from watching Channel 4 panel shows and celebrity news, but will get completely swept into the world of a good book.</p>      
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->
