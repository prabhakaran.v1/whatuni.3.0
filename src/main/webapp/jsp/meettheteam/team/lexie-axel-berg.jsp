<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Lexie Axel-Berg"><span class="Lexie h128"></span> <span class="blue">Lexie Axel-Berg</span>Insights Manager</a>        
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Lexie joined Hotcourses as a fresh faced Leeds graduate back in 2012. Progressing up the ranks she is now responsible for managing key University accounts, and supporting Camilla in her role running the HE sales team.</p>
      <p>She has extensive experience delivering campaigns for a wide range of clients in the HE sector, from Russell Group universities to specialist higher education providers. When in the office her days are spend analysing campaign data, and recommending new products for her clients to incorporate into their marketing plans. A couple of days a week she can be found on a train travelling around the country to visit clients and as a result she now has a fantastic knowledge of the best campus coffee shops at every University.</p>
      <p>Having grown up in Bristol, she heads back to the South West as often as possible, but when in London Lexie can often be found at the nearest local food market or attempting to learn a new skill from a course she's signed up to on Hotcourses. Having completed the London marathon for the Hotcourses Foundation in 2014, she has put her running shoes aside (for now) to pursue her dodgeball career in anticipation of a call-up for the Olympics (2020).</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->
