<%--
  * @purpose  This is included for meet the team member content jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 17-DEC-2019 Sangeeth.S               	 597     First Draft                                                       597 
  * *************************************************************************************************************************
--%>	
<div class="fr rht_sec over_view">  
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Michelle Roberts"><span class="Michelle h128"></span> <span class="blue">Michelle Roberts</span> UK Editor</a>               
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Michelle is the UK Editor at IDP Connect, overseeing content creation on Whatuni and Postgraduate Search. She is responsible for ensuring the content on our website and social media channels is unique, engaging, and helpful to students in making important life decisions.</p>
	  <p>After studying for her degree in Liverpool, Michelle made her way to London where she spent many years writing on lifestyle and personal finance. She has a passion for helping people improve their lives and is excited about helping students choose the right path for them.</p>
	  <p>In her spare time Michelle loves running, having completed three half marathons. She also loves baking, reading, and is a dab hand at DIY.</p>      
    </div>
  </div>  
</div>
