<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Ellie Back"><span class="Ellie h128"></span> <span class="blue">Ellie Back</span>Marketing Manager</a>
       <span class="mtsoc">
         <a href="https://www.linkedin.com/in/ellie-back-1ba91a147/" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>          
        <!--<a href="http://uk.linkedin.com/pub/mia-olorunfemi/3a/2b0/41b" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>-->
        <!--<a href="https://twitter.com/MiaT_O" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a> -->
       </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Ellie manages the Whatuni outreach team, who work in schools and with teachers to help sixth-formers through the university application process. She also oversees review generation for the Whatuni Student Choice Awards, as well as implementing our HE marketing strategy.</p>      
      <p>Outside of work, Ellie can be found in her North London Book Club (first rule about Book Club - <i> always</i>  talk about Book Club) or preaching the joys of Quorn chicken nuggets.</p>      
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->

