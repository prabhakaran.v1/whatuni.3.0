<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Nikki Jell"><span class="Nikki h128"></span> <span class="blue">Nikki Jell</span>Education Liaison Officer</a>
        <span class="mtsoc">
          <a href="https://www.linkedin.com/in/nikki-jell-a1a73083/" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Nikki joined the Whatuni team as Education Liaison Officer following a varied career including 5 years teaching teenagers the intricacies of Business and Psychology. Nikki continues to be passionate about supporting students in achieving their ambitions and is glad to be able to continue to do this in her current role.</p>
      <p>Having never previously ventured further north than Watford, Nikki can normally be found travelling the length and breadth of the country speaking to teachers, careers advisors and students about how <a href="https://www.whatuni.com" title="Whatuni">Whatuni</a> and <a href="https://www.whatuni.com/destinations/" title="Whatuni Destinations" target="_blank">Whatuni Destinations</a> can help them with their university research.  Nikki continues to work with the team to make sure that Whatuni destinations has all the features teachers and careers leaders need to help their students find the right university and course for them.</p>
      <p>As a self-confessed sun worshipper, Nikki's never happier than when topping up her tan on a beach and indulging in her love for watersports. When the British weather allows you're even likely to see her paddleboarding her way down the Thames.</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->