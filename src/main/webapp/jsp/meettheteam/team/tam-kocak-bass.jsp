<div class="fr rht_sec over_view">
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Tam Kocak-Bass"><span class="Tam h128"></span> <span class="blue">Tam Kocak-Bass</span>Marketing and Campaigns Manager, UK</a>       
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Tam is responsible for planning marketing activity at Whatuni. Tam's job is both to ensure that as many people as possible know what a great site Whatuni is. And more importantly, to help students find the right university courses for them.</p>
	  <p>After Studying his BA Hons degree in Social Sciences at University of Sunderland, Tam made his way to London where he's worked in marketing and campaign management.</p>
	  <p>Tam has many interests outside of work. These include Jujitsu, musical theatre and learning the piano.</p>      
    </div>
  </div>  
</div>