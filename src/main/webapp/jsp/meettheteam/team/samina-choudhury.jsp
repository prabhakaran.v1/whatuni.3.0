<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Samina Choudhury"><span class="Samina h128"></span> <span class="blue">Samina Choudhury</span>Account Manager</a>                
      </li>          
    </ul>                            
    <div class="pr-con">                  
      <p>Samina joined the Whatuni team in August 2015 from UCAS Media, where she worked with UK and international universities recruiting UK students. She is very passionate about the ever-changing education sector and deeply enjoys working in the knowledge that she is having a positive influence on prospective students higher education.</p>
      <p>Samina works with university clients across the UK managing their activity on Whatuni to attract new students. She also works with commercial clients looking to offer students' graduate opportunities after their degree.</p>
      <p>Samina can be found in the gym most evenings on her quest to get beach ready and also loves trying out new dining experiences.</p>      
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->