<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Eleni Cashell"><span class="Eleni h128"></span> <span class="blue">Eleni Cashell</span>UK Editor</a>
        <span class="mtsoc">
          <a href="https://www.linkedin.com/pub/eleni-cashell/13/a9b/34b" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
          <a href="https://twitter.com/elenicashell" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>          
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>A self-proclaimed geek and proud Welshie, Eleni studied both her undergraduate and postgraduate degree in Wales. Studying Media and English at Swansea University and Magazine Journalism at Cardiff University, she was never far away from home...until she moved 146 miles away from it eight years ago in order to pursue her writing dreams.</p>      
      <p>She's now the UK Editor at Whatuni and spends most of her time writing advice for prospective students, looking after social media channels and generally shaping the HE content strategy. She also recruits and runs the Whatuni Student Advisory board, ensuring they're having their say on both the Whatuni site and the sector at large.</p>
      <p>In her spare time Eleni enjoys singing in a choir and forcing her colleagues to come and see her,speaking at conferences and seeing as many musicals as her budget can handle.</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->
