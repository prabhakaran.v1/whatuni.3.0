<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Amandeep Chhokar"><span class="Amandeep h128"></span> <span class="blue">Amandeep Chhokar</span>Client Partner Director</a>        
        <span class="mtsoc">
          <a href="https://www.linkedin.com/in/amandeep-chhokar-93693b72/" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>         
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">                  
      <p>Amandeep has been a part of the Whatuni team since 2014 after graduating from Brunel. She has a massive interest in the digital media world, as well as online advertising.</p>
      <%--rebranding text change for hotcourses group to IDP Connect for 30_Jan_19 by Sangeeth.S--%>
      <p>Amandeep works with different universities across the UK on a strategic level, understanding their core recruitment
      objectives and how these can be pushed via the <spring:message code="wuni.idp.connect.text"/> sites, helping both universities and students find the right fit for each other.</p>
      <p>In her spare time, Amandeep loves to dance and take part in competitions and just a good old trip to the cinema to watch any movie!</p>
      <p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->
