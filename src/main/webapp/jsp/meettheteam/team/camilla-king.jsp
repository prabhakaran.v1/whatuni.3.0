<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Camilla King"><span class="Camilla h128"></span> <span class="blue">Camilla King</span>Head of Client Partnerships, UK</a>
      <span class="mtsoc">
         <a href="https://www.linkedin.com/in/camilla-tyminski-49508220/" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>          
       </span>
      </li>          
    </ul>                            
    <div class="pr-con">
    <%--rebranding text change for hotcourses group to IDP Connect for 30_Jan_19 by Sangeeth.S--%>
      <p>Camilla joined <spring:message code="wuni.idp.connect.text"/> in 2011, predominantly helping universities attract the right students for them through solutions across our market leading UK websites. Now heading up the UK team, she is responsible for implementing and driving the UK commercial strategy, with overall responsibility for developing strategic partnerships with Higher Ed providers.</p>
      <p>Camilla spends much of her personal time looking after her 2 young children, as well as playing Netball - GA (glory hunter) of course.</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->