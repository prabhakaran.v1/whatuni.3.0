<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Jon Montgomerie"><span class="Jon h128"></span> <span class="blue">Jon Montgomerie</span>Account Manager</a>                
        <span class="mtsoc">
          <a href="https://www.linkedin.com/in/jon-montgomerie-241008100?trk=hp-identity-name" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
        </span>  
      </li>          
    </ul>                            
    <div class="pr-con">                  
      <p>Jon came to us back in 2015 after graduating with a degree in English Language from University of Reading in 2014. Prior to this his experience stemmed primarily from behind the bar at his local pub from which he departed, leaving his position as manager, though he has lent himself to various musical and charitable projects too.</p>
      <p>If he's not on the phone, he's (loudly) typing away at his laptop putting together stats and figures, or planning his next trip to a university around the UK; anywhere from Portsmouth to Belfast!</p>
      <p>In his spare time Jon is a keen 5-a-side football player and fan of Birmingham City (he's whole heartedly committed to the mundaneness of supporting a mid-table Championship side!).</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->