<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Billy Picard"><span class="Billy h128"></span> <span class="blue">Billy Picard</span>Outreach Manager</a>
        <span class="mtsoc">
          <a href="https://www.linkedin.com/profile/view?id=68171515&trk=spm_pic" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
          <a href="http://www.twitter.com/bilton_keynes" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>          
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Always open to new experiences, Billy recently returned from a tea-fuelled trip that took him through South America and ultimately to living for a year in Australia. Now he's swapped his backpack for a briefcase, and relishes the moments he is able to advise those wanting to go to university, which he does through his management of Whatuni's presence at higher education fairs and careers events. Billy leads the outreach team in their visits to schools and colleges across the UK, where they give impartial advice to students who are thinking about university, but don't know where to go.</p>      
      <p>He's always felt at home on a stage, but now he can no longer hit the high-notes, he's happy to give presentations to prospective students, and maybe even get them to laugh once in a while. After mentoring students throughout his secondary school career, he is now keen to help again with advice on university courses.</p>
      <p>In his spare time, you'll find him at the cinema or with his head buried in a good book. If you can't find him there, check in any number of little restaurants as he loves writing reviews on TripAdvisor (but he's mainly in it for the food).</p>      
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->
