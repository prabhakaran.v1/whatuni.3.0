<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Monisha Sooba"><span class="Monisha h128"></span> <span class="blue">Monisha Sooba</span>Marketing & Outreach Officer</a>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Monisha has been part of the Whatuni team since September 2018, after graduating from the University of Birmingham with a sociology degree.</p>      
      <p>Monisha spends the majority of her time visiting sixth forms and colleges around the country, giving presentations to students that aim to help them through the university application process. When she is in the office she's creating content on the Whatuni Snapchat channel, writing articles and preaching about how the small things in life (giving yourself a pedicure) can really improve your overall wellbeing.</p>      
      <p>In her spare time Monisha enjoys baking new cakes, travelling to new countries and watching Back to the Future. She doesn't enjoy going for runs but does this because she knows this will help her achieve her overall goal of being able to run for a bus and catch it.</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->
