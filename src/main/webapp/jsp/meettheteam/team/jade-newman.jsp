<div class="fr rht_sec over_view">
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Jade Newman"><span class="Jadenewman h128"></span> <span class="blue">Jade Newman</span>Content Executive</a>       
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Jade's role is to create exciting and engaging content for Whatuni's social media channels, helping to inform and advise students across the country on their educational future. She also writes content for the Whatuni website giving useful information to prospective students.</p>
	  <p>In Jade's spare time, she enjoys writing on her beauty and lifestyle blog and creating content for its Instagram page.</p>      
    </div>
  </div>  
</div>