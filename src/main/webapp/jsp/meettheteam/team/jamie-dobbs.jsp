<div class="fr rht_sec over_view">
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Jamie Dobbs"><span class="Jamie h128"></span> <span class="blue">Jamie Dobbs</span>Content Executive</a>       
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Having started with the review collection team, where Jamie travelled thousands of miles collecting 42,000 reviews and talking to real students about their university experiences, his role in the company is now within the marketing team, where he creates articles and designs content for our social media.</p>
	  <p>Jamie has three loves: sport, music and psychology. When he's not knee deep in either of the three, his spare time is filled with being around mates or reading.</p>      
    </div>
  </div>  
</div>