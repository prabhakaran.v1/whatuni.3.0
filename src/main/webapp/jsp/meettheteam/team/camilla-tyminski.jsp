<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Camilla Tyminski"><span class="Camilla h128"></span> <span class="blue">Camilla Tyminski</span>Higher Education Portfolio Director</a>
        <span class="mtsoc">
          <a href="https://www.linkedin.com/profile/view?id=71833649&trk=spm_pic" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>                   
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Camilla joined Hotcourses in 2011, after 2.5 years of painful telesales. She started off as an Account Manager covering both the FE and HE sector and is now responsible for the Higher Education Sales team.</p>      
      <p>Upon joining Hotcourses, Camilla's geography skills were abysmal, but now she has a huge amount of experience with Universities from all over the UK, stretching all the way from Plymouth to Aberdeen. On a day to day basis she speaks with Universities on their student recruitment campaigns, helping them with strategies in order to attract the students they are after. She also spends a lot of time in spreadsheets and looking at client stats (something she secretly loves doing).</p>
      <p>Camilla spends much of her personal time looking after her 2 young children. They keep her extremely busy, so coming in to the office doesn't always seem like work. Her ideal job would be a taste tester (she's a real foodie), but until that opportunity arises, you will find her at Hotcourses.</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->
