<%--
  * @purpose  This is included for meet the team member content jsp..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                    Rel Ver.
  * 17-DEC-2019 Sangeeth.S               	 597     First Draft                                                       597 
  * *************************************************************************************************************************
--%>
<div class="fr rht_sec over_view">
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="javasrcipt:void(0);" title="Margaret Wajnkaim"><span class="Malgorata h128"></span> <span class="blue">Margaret Wajnkaim</span>Project Coordinator</a>       
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>Margaret's role is to support teams with exciting projects throughout the year. This includes 
      managing the delivery of Whatuni's annual review collection, where our team travels the length and breadth of the UK collecting thousands of student reviews about their university experience.</p>
	  <p>In her spare time, Margaret enjoys being creative, whether it is attending art classes or writing 
	  content for the Whatuni website.</p>      
    </div>
  </div>  
</div>