<!-- Right Menu Starts-->	
<div class="fr rht_sec over_view">
  <!--Meet the team starts-->
  <h3 class="fnt_lrg pb35">Meet the team</h3>
  <div class="mteam promt">
    <ul>
      <li>
        <a href="#" title="Vivian Duong"><span class="Vivian h128"></span> <span class="blue">Vivian Duong</span>Product Manager</a>
        <span class="mtsoc">
          <a href="https://www.linkedin.com/in/vivian-duong-81225b43" title="Linkedin" target="_blank"><i class="fa fa-linkedin-square"></i></a>
          <a href="https://twitter.com/Duong_Vivian" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a>          
        </span>
      </li>          
    </ul>                            
    <div class="pr-con">            
      <p>With an insatiable appetite for learning and obsession in how digital is changing the way people interact with everyday tasks, Vivian has found her sweet spot working in the tech and education space (insert venn diagram here).</p>
      <p>Vivian understands that deciding your career and what educational path to take on can be an exciting yet overwhelming task; which is why she's on a mission to develop websites, tools and articles to support your educational journey towards brilliance. This process can involve anything from finding inspiration from the latest podcasts and technologies, sketching page layouts, to going on a post-it note brainstorm frenzy. Her aim is to make Whatuni  as informative, inspiring and user-friendly as possible, so if you've got any feedback, feel free to message Viv your ideas!</p>
      <p>A little more on the hyperactive end of the spectrum, you're likely to find Vivian pulling out awkward dance moves, breaking out in karaoke or gravitating towards anything glittery and kawaii.</p>
    </div>
  </div>
  <!--Meet the team End-->
</div>
<!-- Right Menu Ends-->