<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="WUI.utilities.CommonUtil" %>
<%String allJSName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.allJsName.js");
%>
<%@include  file="/include/includeCSS.jsp" %>  
<script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.commonJsName.js'/>"></script>   
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=allJSName%>"> </script>        
