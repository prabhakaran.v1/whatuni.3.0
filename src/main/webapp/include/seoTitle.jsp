<%@ taglib uri="/WEB-INF/tlds/whatuni.tld" prefix="SEOTITLE" %>
<%@page import="WUI.utilities.SessionData, WUI.utilities.GlobalConstants, org.apache.commons.validator.GenericValidator" autoFlush="true" %>
<%--Added additional parameter award year for WUSCA changes Indumathi.S Mar_29_2016--%>
<% 
  String collegeId = request.getParameter("collegeId");
  String sessionId = new SessionData().getData(request,"x");
  String affliateId = GlobalConstants.WHATUNI_AFFILATE_ID;
  String pageName = !GenericValidator.isBlankOrNull(request.getParameter("pageName")) ? request.getParameter("pageName") : "";
  String paramFlag = !GenericValidator.isBlankOrNull(request.getParameter("paramFlag")) ? request.getParameter("paramFlag") : "";
  String courseId = !GenericValidator.isBlankOrNull(request.getParameter("courseId")) ? request.getParameter("courseId") : "0";
  String studyLevelId = !GenericValidator.isBlankOrNull(request.getParameter("studyLevelId")) ? request.getParameter("studyLevelId") : "0";
  String courseSubjectId = !GenericValidator.isBlankOrNull(request.getParameter("courseSubjectId")) ? request.getParameter("courseSubjectId") : "0";
  String locationCountyId = !GenericValidator.isBlankOrNull(request.getParameter("locationCountyId")) ? request.getParameter("locationCountyId") : "";
  String categoryCode = !GenericValidator.isBlankOrNull(request.getParameter("categoryCode")) ? request.getParameter("categoryCode") : "";
  String entityText = !GenericValidator.isBlankOrNull(request.getParameter("entityText")) ? request.getParameter("entityText") : "";
  String noindexfollow = request.getParameter("noindexfollow") !=null && !request.getParameter("noindexfollow").equalsIgnoreCase("null") && request.getParameter("noindexfollow").trim().length()>0 ? request.getParameter("noindexfollow") : "";
  String searchKeyword = request.getParameter("searchKeyword") !=null && !request.getParameter("searchKeyword").equalsIgnoreCase("null") && request.getParameter("searchKeyword").trim().length()>0 ? request.getParameter("searchKeyword") : "";
  String scholarshipId = request.getParameter("scholarshipId") !=null && !request.getParameter("scholarshipId").equalsIgnoreCase("null") && request.getParameter("scholarshipId").trim().length()>0 ? request.getParameter("scholarshipId") : "";
  String pageNo = !GenericValidator.isBlankOrNull(request.getParameter("pageNo")) ? request.getParameter("pageNo") : "";
  String metaDesc = request.getParameter("metaDesc") !=null && !request.getParameter("metaDesc").equalsIgnoreCase("null") && request.getParameter("metaDesc").trim().length()>0 ? request.getParameter("metaDesc") : "";
  String articleId = !GenericValidator.isBlankOrNull(request.getParameter("articleId")) ? request.getParameter("articleId") : "";
  String artParentName = !GenericValidator.isBlankOrNull(request.getParameter("artParentName")) ? request.getParameter("artParentName") : "";
  String artSubName = !GenericValidator.isBlankOrNull(request.getParameter("artSubName")) ? request.getParameter("artSubName") : "";
  String awardYear = !GenericValidator.isBlankOrNull(request.getParameter("awardYear")) ? request.getParameter("awardYear") : "";//Added by Indumathi.S Mar_29_2016
  String categoryName = !GenericValidator.isBlankOrNull(request.getParameter("categoryName")) ? request.getParameter("categoryName") : "";
%>
<SEOTITLE:seo sessionId="<%=sessionId%>" collegeId="<%=collegeId%>" courseId="<%=courseId%>" affliateId="<%=affliateId%>" pageName="<%=pageName%>" paramFlag="<%=paramFlag%>" studyLevelId="<%=studyLevelId%>" courseSubjectId="<%=courseSubjectId%>" categoryCode="<%=categoryCode%>" locationCountyId="<%=locationCountyId%>" entityText="<%=entityText%>" noIndexFollow="<%=noindexfollow%>" searchKeyword="<%=searchKeyword%>" scholarshipId="<%=scholarshipId%>" pageNo="<%=pageNo%>" metaDesc="<%=metaDesc%>" articleId="<%=articleId%>" artParentName="<%=artParentName%>" artSubName="<%=artSubName%>" awardYear="<%=awardYear%>" categoryName="<%=categoryName%>" 
  totalProviderCount="${param.totalProviderCount}" totalCourseCount="${param.totalCourseCount}"/>
<meta name="content-type" content="text/html; charset=utf-8"/> 