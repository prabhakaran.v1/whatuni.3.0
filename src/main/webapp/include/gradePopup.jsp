<!-- popup starts below -->
<div id="fpcs">
  <div class="finc">	    
    <a class="cls" href="javascript:entryPointFilterSubmit(false);"></a>
    <div class="line line1 m12 line2">
      <h1 class="htxt f22">Save yourself SOME TIME!</h1>      
    </div>    
    <div class="txtc m21 w250">
      <span class="wkbs db">Organise your university search by storing </span>
      <span class="wkbs db pdT5">your grades on your <span class="wtu">MyWhatUni</span> profile.</span>
    </div>
    <a class="btn-joinfree m28 mb0" href="javascript:entryPointFilterSubmit(true);"></a>
  </div>
</div>
<!-- popup ends above -->