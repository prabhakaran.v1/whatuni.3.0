<%@ taglib uri="/WEB-INF/tlds/common-title.tld" prefix="commonTitle" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<c:set var="pagename3">
   <tiles:getAsString name="pagename3" ignore="true"/>
 </c:set>

<%
  String indexFollowFlag = request.getParameter("indexFollowFlag") != null && request.getParameter("indexFollowFlag").trim().length()>0 ? request.getParameter("indexFollowFlag") : "";
%>
<c:choose>
  <c:when test="${pagename3 eq 'home.jsp' || pagename3 eq 'newUser.jsp'  || pagename3 eq 'retUser.jsp'  || pagename3 eq 'loggedIn.jsp' }">
    <commonTitle:title affliateId="220703" indexFollowFlag="<%=indexFollowFlag%>" homepageFlag="Y" pagename3="${pagename3}"/>
  </c:when>
  <c:otherwise>
    <commonTitle:title affliateId="220703" indexFollowFlag="<%=indexFollowFlag%>" pagename3="${pagename3}" />
  </c:otherwise>
</c:choose>
