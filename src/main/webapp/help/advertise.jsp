<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="WUI.utilities.GlobalConstants,WUI.utilities.CommonUtil"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
   <head>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"> </script>
     <%@include  file="/include/htmlTitle.jsp" %>   
     <%--    <%@include  file="/include/commonHeaderContent.jsp" %>
       <%@include  file="/include/includeJSCSS.jsp" %>
       --%>
         <%@include  file="/jsp/common/includeCSS.jsp" %> 
   </head>
   <body>
   <% String urlString  = request.getQueryString()!=null && request.getQueryString().trim().length()>0 ? request.getQueryString().replaceAll("&","&amp;") : "";%>
         <div class="clipart">
          <div id="content-bg">
   <div id="wrapper">
   <%--
                    <jsp:include page="/include/header.jsp" >
                        <jsp:param name="headername" value="review_rating" />
                    </jsp:include>
                  --%>
                <jsp:include page="/jsp/common/wuHeader.jsp" />
                  <%--
                  <div id="container">
                      <div id="leftcolumn">
                      --%>
                 <div id="content-blk">
                  <div id="content-left">

            <%-- <jsp:include page="/include/loggedPod.jsp" >
                 <jsp:param name="urlname" value="advertise" />
                 <jsp:param name="querystringname" value="<%=urlString%>" />
                 <jsp:param name="showlogin" value="yes" />
             </jsp:include>
       </div>      
       <div id="maincolumn">
          <div id="contentcolumn">        --%>
             <div id="aboutus">
                <h1 class="hs-1 uni-info  mb-20"><strong>Advertise On Whatuni</strong></h1>
                <p><%=GlobalConstants.WHATUNI_TEXT%> offers a wide variety of advertising options such as banner ads; school, college, departmental or university profiles, interactive e-mail links and more.</p>
                <p>To find out more about these options, please e-mail one of the contacts below and an account manager will contact you soon afterwards.</p>
                <p><strong>UK and European enquiries: </strong><a class="sblue" href="mailto:<%=GlobalConstants.SALES_EMAIL%>"><%=GlobalConstants.SALES_EMAIL%></a></p><%--email domain changed by Sangeeth as part of rebranding changes 30_JAN_19 REL--%>
              </div>
          </div>
       </div>
     <%--     </div>  
                      <div id="footer">
                       <jsp:include page="/include/wuniFooter.jsp" />
	                        </div>   	--%>
              </div>
        </div>
    </div>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
 </body>
</html>
