<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil"%>  

<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>  
</header>
<!-- cookie button start -->
<%-- <div class="content-bg">
  <div class="cke_wp">
    <a href="javascript:void(0);" id="changeSettingsLightBox" class="cke_st_btn">MANAGE COOKIES</a>
  </div>
</div> --%>
<!-- cookie button end -->
<c:if test="${not empty requestScope.cookieText}">
  ${requestScope.cookieText}
</c:if>

<jsp:include page="/jsp/common/wuFooter.jsp" />
<input type="hidden" name="hdrMenuHide" id="hdrMenuHide" value="READREVIEWS" />
</body>
