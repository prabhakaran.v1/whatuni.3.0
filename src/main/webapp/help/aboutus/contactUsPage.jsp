<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.SessionData, WUI.utilities.GlobalConstants"%>  
<%@ taglib uri="http://www.springframework.org/tags/form"  prefix="form" %>
  <%  
    CommonUtil util = new CommonUtil();
    String TCVersion = util.versionChanges(GlobalConstants.TERMS_AND_COND_VER);
    String latitudeStr =  (String)request.getAttribute("latitudeStr"); 
    String longitudeStr =  (String)request.getAttribute("longitudeStr");
    String loadNewMapJs = CommonUtil.getResourceMessage("wuni.load.new.map.js", null);
  %>
<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>  
  <%String aboutUsJsName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.aboutus.js");%>  
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/javascripts/<%=aboutUsJsName%>"></script>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=loadNewMapJs%>"></script>
</header>
<section class="main_cnr abts pt40 pb40">
<div class="ad_cnr">
  <div id="sub_container" class="content-bg">
    <div class="sub_cnr abt">
      <!-- Left Menu Starts-->
      <jsp:include page="/help/aboutus/aboutUsLeftMenu.jsp">
        <jsp:param name="pagename" value="contactus" />
      </jsp:include> 
      <!-- Left Menu Ends-->
      <!-- Right Menu Starts-->
      <div class="fr rht_sec">
        <h3 class="fnt_lrg pb30">Contact us</h3>
        <div class="right_tab">	
          <form:form action="" method="post" id="aboutcontactus" name="aboutcontactus" commandName="aboutusBean">
            <!--Contact us Starts-->
            <fieldset class="">
              <fieldset class="row-fluid">
                <fieldset class="w50p fl">							
                  <!--<input type="text" id="firstName" name="firstName" value="First name*" autocomplete="off" maxlength="40" onclick="clearDefault(this, 'First name*');" onkeypress="clearDefault(this, 'First name*');" onblur="setDefault(this, 'First name*');" />-->
                  <form:input type="text" id="firstName" name="firstName" path="firstName" placeholder="First name*" autocomplete="off" maxlength="40"/> 
                  <p class="err" id="firstName_error" style="display:none;"></p>
                </fieldset>
                <fieldset class="w50p fr">
                  <!--<input type="text" id="lastName" name="lastName" value="Last name*" autocomplete="off" maxlength="40" onclick="clearDefault(this, 'Last name*');" onkeypress="clearDefault(this, 'Last name*');" onblur="setDefault(this, 'Last name*');" />-->
                  <form:input type="text" id="lastName" name="lastName" path="lastName" placeholder="Last name*" autocomplete="off" maxlength="40"/>
                  <p class="err" id="lastName_error" style="display:none;"></p>
                </fieldset>
              </fieldset>
              <fieldset class="row-fluid">
                <fieldset class="w100p fl">
                  <!--<input type="text" id="emailAddress" name="emailAddress" value="Email address*" autocomplete="off" maxlength="50" onclick="clearDefault(this, 'Email address*');" onkeypress="clearDefault(this, 'Email address*');" onblur="setDefault(this, 'Email address*');" />-->
                  <form:input type="text" id="emailAddress" name="emailAddress" path="emailAddress" placeholder="Email address*" autocomplete="off" maxlength="50"/>
                  <p style="display: none;" id="emailAddress_error" class="err"></p>
                </fieldset>
              </fieldset>
              <fieldset class="row-fulid mb10">		
                <fieldset class="w100p fr">
                  <!--<textarea rows="4" cols="77" class="fnt_lrg fl" id="message" name="message" maxlength="4000" onclick="clearDefault(this, 'Please enter a brief description of your message*');" onkeypress="clearDefault(this, 'Please enter a brief description of your message*');" onblur="setDefault(this, 'Please enter a brief description of your message*');">Please enter a brief description of your message*</textarea>-->
                  <form:textarea rows="4" cols="77" class="fnt_lrg fl" id="message" name="message" path="message" maxlength="4000" placeholder="Please enter a brief description of your message*"></form:textarea>
                </fieldset>
                <p style="display:none;" id="message_error" class="err"></p>
              </fieldset>
              <p style="float:left;width:100%;margin:-5px 0 0;">By clicking on Send Email, you are agreeing to our <a href="<%=TCVersion%>" target="_blank" style="text-decoration:underline">Terms and Conditions</a>.</p>
            </fieldset>	
            <!--Contact us Ends-->
            <fieldset class="row-fulid fr mt5">
               <input type="hidden" id="latitude" value="<%=latitudeStr%>"/>
              <input type="hidden" id="longitude" value="<%=longitudeStr%>"/>
              <input type="hidden" id="userid" value='<%=new SessionData().getData(request, "y")%>'/>
              <a onclick="submitContactUsPage();" class="btn1 blue m0 w180">Send Email<i class="fa fa-long-arrow-right"></i></a> 
            </fieldset>              
            <div class="map mt40 fl w100p">
              <div id="locationinfoMaphh" class="fl w180p">WhatUni <br>
              Bedford House <br>
              69-79 Fulham High Street <br>
              London <br>
              SW6 3JW
            </div>
            <div class="fr"><%--Changed static google map to mapbox for Feb_12_19 rel by Sangeeth.S--%>
              <jsp:include page="/jsp/common/mapbox.jsp" /> 
              <input type="hidden" id="mapDivContent" name="mapDivContent" value="0"/>
            </div>
          </div> 
        </form:form>            
      </div>
    </div>        
    <!-- Right Menu Ends-->
  </div>
</div>
</div>
</section>
<jsp:include page="/jsp/common/wuFooter.jsp" />
<input type="hidden" name="hdrMenuHide" id="hdrMenuHide" value="READREVIEWS" />
</body>