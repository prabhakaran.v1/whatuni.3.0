<%@page import="WUI.utilities.CommonUtil"%>  
<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>  
</header>
<section class="main_cnr abts pt40 pb40">
<div class="ad_cnr">
  <div id="sub_container" class="content-bg">
    <div class="sub_cnr abt">
      <!-- Left Menu Starts-->
      <jsp:include page="/help/aboutus/aboutUsLeftMenu.jsp">
        <jsp:param name="pagename" value="contactus" />
      </jsp:include>
      <!-- Left Menu Ends-->
      <!-- Right Menu Starts-->
      <div class="fr rht_sec">
        <h3 class="fnt_lrg pb40">Contact us</h3>
        <div class="fr rht_sec">
          <div class="success p20">
            <h6 class="fnt_lbd pb5">Thanks,</h6>
            <p class="txt fnt_lrg m0 p0"><i class="icon-ok mr5"></i> Your message has been received, we'll be in touch with you shortly</p>
          </div>
        </div>
      </div>		
      <!-- Right Menu Ends-->
    </div>
  </div>
</div>
</section>                    
<jsp:include page="/jsp/common/wuFooter.jsp" />
<input type="hidden" name="hdrMenuHide" id="hdrMenuHide" value="READREVIEWS" />
</body>