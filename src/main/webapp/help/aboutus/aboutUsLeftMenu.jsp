<%
  String setAboutusType = "Overview";
  String pagename = request.getParameter("pagename");
  if(request.getAttribute("setAboutusType")!=null){
    setAboutusType = (String)request.getAttribute("setAboutusType");
  }
  if("insightsPod".equalsIgnoreCase(pagename)){
    setAboutusType = "Student Insights";
  }
%>
<!-- Left Menu Starts-->
<div class="fl lft_menu mr40">
  <div class="v_menu op_mth_cnr">
    <h6 class="fnt_lrg pt5">ABOUT US</h6>
    <div class="select_box_month">
      <span class="sld_mth"><%=setAboutusType%></span>
      <i class="fa fa-angle-down"></i>
    </div>
    <div class="op_mth_slider">
      <ul>
        <%if("overview".equals(pagename)){%>
          <li>
            <a href="/about-us" class="fnt_lbd act" id="overview">Overview</a>
          </li>
        <%}else{%>
          <li>
            <a href="/about-us" class="fnt_lbd" id="overview">Overview</a>
          </li>
        <%}if("team".equals(pagename)){%>
          <li>
            <a href="/team" class="fnt_lbd act">Our people</a>
          </li>
        <%}else{%>
          <li>
            <a href="/team" class="fnt_lbd">Our people</a>
          </li>  
        <%}%>
        <%--
        <%if("schoolvisits".equals(pagename)){%>
          <li>
            <!--28_Oct_2014 changed school visits url from aboutus/schoolvisits to about-us/school-visits by Thiyagu G.-->
            <a href="/about-us/school-visits" class="fnt_lbd act">School visits</a>
          </li>
        <%}else{%>
          <li>
            <a href="/about-us/school-visits" class="fnt_lbd">School visits</a>
          </li>
        <%}%>
        --%>
        <li><a href="/destinations/" target="_blank" class="fnt_lbd">Career Advisors</a></li> <%--Added destinations link by Prabha on 06_oct_16--%>
        <%--
        <%if("orderdownload".equals(pagename)){%>
        <!--28_Oct_2014 changed order download url from aboutus/orderdownload to about-us/order-download by Thiyagu G.-->
          <li>
            <a href="/about-us/order-download" class="fnt_lbd act">Order / Downloads</a>
          </li>
        <%}else{%>
          <li>
            <a href="/about-us/order-download" class="fnt_lbd">Order / Downloads</a>
          </li>
        <%}
        if("workforus".equals(pagename)){%>
          <li>
            <!--28_Oct_2014 changed work for us url from aboutus/workforus to about-us/work-for-us by Thiyagu G.-->
            <a href="/about-us/work-for-us" class="fnt_lbd act" id="workforus">Work for us</a>
          </li>
        <%}else{%>
          <li>
            <a href="/about-us/work-for-us" class="fnt_lbd" id="workforus">Work for us</a>
          </li>
        <%}%>
        --%>
        <%if("partners".equals(pagename)){%>
          <li>
            <%--28_Oct_2014 changed partners url from /partners to /about-us/partners/ by Thiyagu G.--%>
            <a href="/about-us/partners" class="fnt_lbd act" id="partners">Partners</a>
          </li>
        <%}else{%>
          <li>
            <a href="/about-us/partners" class="fnt_lbd" id="partners">Partners</a>
          </li>
        <%}
        if("contactus".equals(pagename)){%>
          <li>
            <!--28_Oct_2014 changed contact us url from /aboutus/contactus to /about-us/contact-us/ by Thiyagu G.-->
            <a href="/about-us/contact-us" class="fnt_lbd act" id="contactus">Contact us</a>
          </li>
        <%}else{%>
          <li>
            <a href="/about-us/contact-us" class="fnt_lbd" id="contactus">Contact us</a>
          </li>
        <%}%>        
      </ul>
    </div>
  </div>
</div>
<!-- Left Menu Ends-->