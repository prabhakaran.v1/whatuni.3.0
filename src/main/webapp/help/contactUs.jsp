<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page import="WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants" %>

<%
  String whatuniEmail = new CommonFunction().getWUSysVarValue("WHAT_UNI_EMAIL");
%>

<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <%@include  file="/include/htmlTitle.jsp" %>   
    <%@include  file="/include/commonHeaderContent.jsp" %>
    <%@include  file="/include/includeJSCSS.jsp" %>
</head>
   
<body id="www-whatuni-com">

    <div id="wrapper">
    <div id="container">
    <div id="maincolumn">
    <div id="contentcolumn">
      <div id="contactus">
        
        <h2>Contact details</h2>
        <p>If you would like to find out more about Whatuni.com please get in touch.</p>
        
        <h4>Members queries:</h4>        
        <p>If you are a member of Whatuni.com please contact <a href="mailto:<%=whatuniEmail%>" title="mailto:<%=whatuniEmail%>">The Whatuni Team</a>.</p>
        
        <h4>Media advertising:</h4>
        <p>If you would like to find out more about our media opportunities please contact <a href="mailto:<%=GlobalConstants.SALES_EMAIL%>" title="mailto:<%=GlobalConstants.SALES_EMAIL%>"><%=GlobalConstants.SALES_EMAIL%></a> or call 0208 600 1202.</p><%--email domain changed by Sangeeth as part of rebranding changes 30_JAN_19 REL--%>        
        
        <h4>PR opportunities:</h4>
        <p>If you have a great story Whatuni members should know about please email <a href="mailto:<%=whatuniEmail%>" title="mailto:<%=whatuniEmail%>">The Whatuni Team</a>.</p>              
        
        <h4>Technical queries:</h4>
        <p>If you are having any technical issues please contact <a href="mailto:<%=whatuniEmail%>" title="mailto:<%=whatuniEmail%>">The Whatuni Team</a>.</p>                
        
        <h4>You can also write to us:</h4>
        <p>Whatuni.com<br />First Floor,<br />Bedford House,<br />Fulham Green,<br />69-79 Fulham High Street,<br />London SW6 3JW<br /><br />Telephone: 0207 384 6000<br />Reg number: 2471319<br />VAT Reg. No: GB 603 0202 19</p>  
        
        <p><a href="javascript:window.close();" title="Close window">Close window</a></p>
        
      </div>
    </div>
    </div>
    </div>
    </div>
    
</body>
</html>

