<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonFunction" %>

<c:if test="${not empty requestScope.locationSubCategoryList}">
<% 
  String displayStyle = request.getParameter("displayStyle"); 
  displayStyle = displayStyle !=null && displayStyle.equalsIgnoreCase("block") ? "display:block" : "display:none"; 
  String heading = request.getParameter("POD_HEADING");
%>
    <div class="top-crs-blk">
        <%if(heading != null && heading.trim().length() > 0){%>
          <h2 class="hc-1"><strong><a onclick="hideExpandDiv('locationsubjectfrm')"><%=heading%></a></strong></h2>
        <%}else{%>
          <h2 class="hc-1"><a onclick="hideExpandDiv('locationsubjectfrm')">Similar <c:if test="${not empty requestScope.categoryName}">${requestScope.categoryName}</c:if> <c:if test="${not empty request.subjectDesc}"> ${request.subjectDesc} </c:if>   <c:if test="${not empty requestScope.studyLevelDesc}"> ${requestScope.studyLevelDesc} </c:if> courses</a></h2>
        <%}%>
        <ul id="locationsubjectfrm" class="top-crs-list" style="<%=displayStyle%>">
            <c:forEach var="subCategoryList" items="${requestScope.locationSubCategoryList}">
                <% String categoryUrl = "";%>
                <c:if test="${not empty requestScope.categoryUrl}"> 
                <% 
                  categoryUrl = request.getAttribute("categoryUrl").toString();
                %> </c:if>
                <c:set var="subId" value="${subCategoryList.subjectId}"/>
                <c:set var="subName" value="${subCategoryList.seoSubjectName}"/>
                <%  
                String subName = (String)pageContext.getAttribute("subName");
                String subId = (String)pageContext.getAttribute("subId");
                categoryUrl = categoryUrl.replaceAll("##catname##", subName.toString()); categoryUrl = categoryUrl.replaceAll("##catcode##", subId.toString()); %>
                <c:if test="${subCategoryList.parentCategoryId eq 'ROOT'}">
                   <c:if test="${subCategoryList.categoryCodeLength gt 1}"> 
                        <c:if test="${requestScope.titleSubjectCode ne subCategoryList.subjectId}">
                            <li><a href="<%=request.getContextPath()+categoryUrl%>"  title="${subCategoryList.seoSubjectName}">${subCategoryList.seoSubjectName}</a></li>
                        </c:if>
                        <c:if test="${requestScope.titleSubjectCode eq subCategoryList.subjectId}">
                           <li><b>${subCategoryList.seoSubjectName}</b></li>
                        </c:if>
                    </c:if>
                 </c:if>   
                <c:if test="${subCategoryList.parentCategoryId ne 'ROOT'}">
                    <c:if test="${requestScope.titleSubjectCode ne subCategoryList.subjectId}">
                        <li><a href="<%=request.getContextPath()+categoryUrl%>"  title="${subCategoryList.seoSubjectName}">${subCategoryList.seoSubjectName}</a></li>
                     </c:if>
                     <c:if test="${requestScope.titleSubjectCode eq subCategoryList.subjectId}">
                        <li><b>${subCategoryList.seoSubjectName}</b></li>
                     </c:if>
                 </c:if>                    
            </c:forEach>
        </ul>
    </div>
</c:if>