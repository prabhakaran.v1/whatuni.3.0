<%@ page contentType="text/html;charset=windows-1252" import="java.util.*,javax.swing.*" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
<%@ taglib  uri="http://www.springframework.org/tags" prefix="spring"%> 
<%@ page import="WUI.utilities.SessionData" %>

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <title><spring:message code="wuni.message.review.loginredirect.title" /></title>
  </head>
  
  <body id="www-whatuni-com">
    <%
        String fromUrl ="";
        String additionalFromUrl ="";
        String queryString  =   "";
        String flag   =   session.getAttribute("queryString") !=null ? String.valueOf(session.getAttribute("queryString")) :"";
        if(session.getAttribute("courseSearch") != null) { 
            session.removeAttribute("courseSearch");
        } else {
          if(!"NO".equalsIgnoreCase(flag)){
            queryString  =   "?x="+ new SessionData().getData(request,"x")+"&y="+new SessionData().getData(request,"y");
          }
        }
        fromUrl             =   session.getAttribute("fromUrl") !=null ? String.valueOf(session.getAttribute("fromUrl"))+queryString : "/home.html"; //23.05.2008
        if(fromUrl != null && fromUrl.indexOf("/unsubscribe.html") > -1){
        	fromUrl = fromUrl.replace(queryString,""); 
        	flag = "NO";
          } 
        additionalFromUrl   =   session.getAttribute("additionalFromUrl") !=null ? String.valueOf(session.getAttribute("additionalFromUrl")) :"";
        if(additionalFromUrl != null && !additionalFromUrl.equals("")){
           if(fromUrl !=null && !fromUrl.equalsIgnoreCase("/home.html")){
               fromUrl = fromUrl + additionalFromUrl;
           } else {
              fromUrl = fromUrl; 
           }  
        }    
        if(session.getAttribute("fromUrl") != null) session.removeAttribute("fromUrl");
        if(session.getAttribute("additionalFromUrl") != null) session.removeAttribute("additionalFromUrl");
    %>
    <%if("NO".equalsIgnoreCase(flag)){%>
      <script language="JavaScript" type="text/javascript">
        var fromURL = "/degrees"+'<%=fromUrl%>';
         document.location.href = fromURL;
      </script>
    <%}else{%>
    <html:form action="/degrees"+"<%=fromUrl%>">
      <script language="JavaScript" type="text/javascript">
         document.forms[0].submit();
      </script>
    </html:form>
 </body>
</html>
<%}%>
 