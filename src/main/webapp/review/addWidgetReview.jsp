

<%@page import="WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants, WUI.utilities.SessionData, org.apache.commons.validator.GenericValidator"%>
<%@page import="WUI.utilities.CommonUtil"%>
<%@page import="java.util.ResourceBundle"%>
<%
  CommonFunction commonFun = new CommonFunction();
  String campaignId = request.getParameter("campaign") != null ? request.getParameter("campaign") : "";
  String ambassadorId = request.getParameter("amb") != null ? request.getParameter("amb") : "null";
  String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.  
  ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
  String widgetDomainPathHid = bundle.getString("wuni.whatuni.device.specific.css.path");
  String domainSpecificPath = httpStr + widgetDomainPathHid;  
  String cssPath = httpStr;
  String jsPath = httpStr;
  String imgPath = httpStr;
  String domainName = request.getServerName();
  if(domainName.indexOf("mtest.whatuni.com") > -1 || domainName.indexOf("mdev.whatuni.com") > -1) {
    cssPath += domainName;
    jsPath += domainName;
    imgPath += domainName;
  } else {
    cssPath += bundle.getString("wuni.cssdomain");
    jsPath += bundle.getString("wuni.jsdomain");
    String objPath=Integer.toString(1); 
    Object[] obj ={objPath}; 
    imgPath += CommonUtil.getResourceMessage("wuni.imagedomain", obj);
  }
  CommonUtil util = new CommonUtil();
  SessionData sessionData = new SessionData();
  String WTCVersion = util.versionChanges(GlobalConstants.TERMS_AND_COND_VER);
  String WPRVersion = util.versionChanges(GlobalConstants.PRIVACY_VER);  
  String userId = ("null".equalsIgnoreCase(ambassadorId))? new SessionData().getData(request, "y") : "0";
  //wu_300320 - Sangeeth.S: Added lat and long for stats logging
  String[] latAndLong = sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG).split(GlobalConstants.SPLIT_CONSTANT);
  String latitude = (latAndLong.length > 0 && !GenericValidator.isBlankOrNull(latAndLong[0])) ? latAndLong[0].trim() : "";
  String longitude = (latAndLong.length > 1 && !GenericValidator.isBlankOrNull(latAndLong[1])) ? latAndLong[1].trim() : "";  
%>

<body class="default repeat proportion branding font-family-source-sans-pro dark banner-top form-ready">
 <script type="text/javascript">
  var vulnerableKeywords = ["onclick","`","{","}","~","|","^","http:","javascript:","https:","ftp:","type='text/javascript'","type=\"text/javascript\"","language='javascript'","language=\"javascript\""];
  var htmlTags = ["<!DOCTYPE", "<!--", "<acronym", "<abbr", "<address", "<applet", "<area", "<article", "<aside", "<audio", "<a", "<big", "<bdi", "<basefont", "<base", "<bdo", "<blockquote", "<body", "<br", "<button", "<b", "<center", "<canvas", "<caption", "<cite", "<code", "<colgroup", "<col", "<dir", "<datalist", "<dd", "<del", "<details", "<dfn", "<dialog", "<div", "<dl", "<dt", "<embed", "<em", "<frameset", "<frame", "<font", "<fieldset", "<figcaption", "<figure", "<footer", "<form", "<header", "<head", "<h1", "<h2", "<h3", "<h4", "<h5", "<h6", "<hr", "<html", "<iframe", "<img", "<ins", "<input", "<i", "<kbd", "<keygen", "<label", "<legend", "<link", "<li", "<main", "<map", "<mark", "<menuitem", "<menu", "<meta", "<meter", "<noscript", "<noframes", "<nav", "<object", "<ol", "<optgroup", "<option", "<output", "<param", "<pre", "<progress", "<p", "<q","<rp", "<rt", "<ruby", "<svg", "<samp", "<script", "<section", "<select", "<small", "<strike", "<source", "<span", "<strong", "<style", "<sub", "<summary", "<sup", "<s", "<table", "<thead", "<tbody", "<tfoot", "<tt", "<td", "<th", "<tr", "<textarea", "<time", "<title", "<track", "<ul", "<u", "<var", "<video", "<wbr", "<\/acronym", "<\/abbr", "<\/address", "<\/applet", "<\/area", "<\/article", "<\/aside", "<\/audio","<\/a", "<\/big", "<\/bdi", "<\/basefont", "<\/base", "<\/bdo", "<\/blockquote", "<\/body", "<\/br", "<\/button", "<\/b", "<\/center", "<\/canvas", "<\/caption", "<\/cite", "<\/code", "<\/colgroup", "<\/col", "<\/dir", "<\/datalist", "<\/dd", "<\/del", "<\/details", "<\/dfn", "<\/dialog", "<\/div", "<\/dl", "<\/dt","<\/embed", "<\/em", "<\/frameset", "<\/frame", "<\/font", "<\/fieldset", "<\/figcaption", "<\/figure", "<\/footer", "<\/form", "<\/header", "<\/head", "<\/h1", "<\/h2", "<\/h3", "<\/h4", "<\/h5", "<\/h6", "<\/hr", "<\/html","<\/iframe", "<\/img", "<\/ins", "<\/input", "<\/i", "<\/kbd", "<\/keygen", "<\/label", "<\/legend", "<\/link", "<\/li", "<\/main", "<\/map", "<\/mark", "<\/menuitem", "<\/menu", "<\/meta", "<\/meter", "<\/noscript", "<\/noframes", "<\/nav", "<\/object", "<\/ol", "<\/optgroup", "<\/option", "<\/output", "<\/param", "<\/pre", "<\/progress", "<\/p", "<\/q", "<\/rp", "<\/rt", "<\/ruby", "<\/svg", "<\/samp", "<\/script", "<\/section", "<\/select", "<\/small", "<\/strike", "<\/source", "<\/span", "<\/strong", "<\/style", "<\/sub", "<\/summary", "<\/sup","<\/s", "<\/table", "<\/thead", "<\/tbody", "<\/tfoot", "<\/tt", "<\/td", "<\/th", "<\/tr", "<\/textarea", "<\/time", "<\/title", "<\/track", "<\/ul", "<\/u", "<\/var", "<\/video", "<\/wbr"];
 </script>
 <%-- GTM script included under body tag --%>
 <jsp:include page="/jsp/thirdpartytools/include/gtm.jsp">
	<jsp:param name="PLACE_TO_INCLUDE" value="BODY"/>
 </jsp:include>
 <%-- Pgs pod starts here--%>
 <form action="#">
  <div class="wrapp">
   <div id="loader_AMW" style="display:none;text-align:center;margin:378px 84px 200px;"><img alt="loading" src="<%=imgPath%>/widget-cont/wu/img/712.GIF"></div>
   <div class="container1" id="reviewCont_AMW">
    <!-- Back Arrow Added by Sangeeth.S for Aug_28_rel-->
    <div class="pgr_bck" onclick="navigatePages('down');" id="backButton_AMW" style="display: none;"> <i class="fa fa-long-arrow-right"></i> Back</div>
    <!-- Back Arrow -->
    <div id="homePage_AMW" style="display:block"></div>
    <div id="accommodation_AMW" style="display:none"></div>
    <div id="0_rating_AMW" style="display:none"></div><%--accmmodation--%> 
    <div id="1_rating_AMW" style="display:none"></div><%--findingjob--%>
    <div id="2_rating_AMW" style="display:none"></div><%-- courselec--%>
    <div id="3_rating_AMW" style="display:none"></div><%-- studentsUnion--%>
    <div id="4_rating_AMW" style="display:none"></div><%-- facilities--%>
    <div id="5_rating_AMW" style="display:none"></div><%-- citylife--%>
    <div id="6_rating_AMW" style="display:none"></div><%-- support--%>
    <div id="7_rating_AMW" style="display:none"></div><%-- support--%>
    <%--<div id="8_rating_AMW" style="display:none"></div> givingback commented for the AUG_28_18 rel by Sangeeth.S--%>
    <div id="8_rating_AMW" style="display:none"></div><%-- overallexperience--%>
    <div id="registration_AMW" style="display:none;"></div>      
    <div id="success_AMW" style="display:none"></div>
    <%-- footer section start--%>
    <div class="footer" style="display:none;">
    <!--Progress Bar block-->
    <div class="prg_bar">
     <%-- ::START:: dom_20160216 - Suganya: added new back button 
     <span class="pgr_bck" onclick="navigatePages('down');" id="backButton_AMW" style="display:none"> < Back</span>--%>
     <%-- ::END:: dom_20160216 - Suganya: added new back button --%>
     <div class="pgr_bg">
      <div class="grn_bg" id="grnBgId_AMW"></div>
     </div>
     <span id="percent_AMW">0% complete</span>
     </div>
     <!--Progress Bar block-->
     <!--Arrow button block-->
     <div class="btndiv">
      <div class="nav-buttons">
       <div class="button-wrapper up" id ="downarrow_AMW" onclick="navigatePages('down');">
        <div class="arr_btn nav hover-effect enabled"><span></span><i class="fa fa-angle-up"></i><span class="up"></span> </div>
       </div>
       <div class="button-wrapper down" id ="uparrow_AMW" onclick="navigatePages('up');">
        <div class="arr_btn nav enabled hover-effect"><span></span><i class="fa fa-angle-down"></i><span class="down"></span> </div>
       </div>
      </div>
     </div>
     <!--Arrow button block-->
    </div>
    <%-- footer section end--%>
      <%-- help pod start here--%>
       <div class="pod1" id="whyReview_AMW" style="display:none;">
                <a href="javascript:void(0);" onclick="document.getElementById('whyReview_AMW').style.display='block';" class="cls_btn"><i class="fa fa-times"></i></a>
                <h1>Why review?</h1>
                <div class="line"></div>
                <ul>
                    <li>
                        <div class="circle"><img src="<%=imgPath%>/widget-cont/wu/img/img1.png" alt="Whatuni">
                        <%--<div class="circle"><img src="http://www.whatuni.com/widget-cont/wu/img/img1.png" alt="Whatuni">--%>
                        </div>
                        <span>Help future students</span>
                    </li>
                    <li>
                        <div class="circle"><img src="<%=imgPath%>/widget-cont/wu/img/img2.png" alt="Whatuni">
                        <%--<div class="circle"><img src="http://www.whatuni.com/widget-cont/wu/img/img2.png" alt="Whatuni">--%>
                        </div>
                        <span>Help your uni win</span>
                    </li>
                    <li>
                        <div class="circle"><img src="<%=imgPath%>/widget-cont/wu/img/img3.png" alt="Whatuni">
                        <%--<div class="circle"><img src="http://www.whatuni.com/widget-cont/wu/img/img3.png" alt="Whatuni">--%>
                        </div>
                        <span>Maybe win a &#xa3;250 food shop</span>
                    </li>
                </ul>
                <div class="wrev_tmscon"><a href="https://www.whatuni.com/wu-cont/terms-and-conditions/student-reviews/" target="_blank">Terms and Conditions <i class="fa fa-long-arrow-right"></i></a></div>
            </div>
        <%-- help pod end here--%>
   </div>
   <div id="ambCont_AMW" class="container1" style="display:none;"></div>
  </div>
  <%-- Pgs pod end here--%>
  <input type="hidden" id="ids_AMW" value="<%=widgetDomainPathHid%>/220703/9139"/>
  <input type="hidden" id="dimensions_AMW" value="500/670"/>
  <input type="hidden" id="options_AMW" value="2"/>
  <input type="hidden" id="rating_AMW" value="all"/>
  <input type="hidden" id="courseId_AMW" value=""/>
  <input type="hidden" id="collegeId_AMW" value=""/>
  <input type="hidden" id="collegeType_AMW" value=""/>
  <input type="hidden" id="WTCVersion" value="<%=WTCVersion%>"/>
  <input type="hidden" id="WPRVersion" value="<%=WPRVersion%>"/>
  <input type="hidden" id="reviewCount_AMW" value="5/150"/>
  <input type="hidden" id="clgspec_AMW" value="N"/>
  <input type="hidden" id="acc_Required" value="Y"/>
  <input type="hidden" id="next_SEC" value="N"/>
  <input type="hidden" id="page_SEC" value="6"/>
  <input type="hidden" id="campaignId_AMW" value="<%=campaignId%>"/>
  <input type="hidden" id="ambassadorId_AMW" value="<%=ambassadorId%>" />
  <input type="hidden" id="userId" value="<%=userId%>" />
  <input type="hidden" id="lat" value="<%=latitude%>" />
  <input type="hidden" id="lon" value="<%=longitude%>" />
 </form>
<script type="text/javascript" src="<%=jsPath%>/widget-cont/wu/script/newReviewUtil_310320.js"></script>
<script type="text/javascript">
$(window).on("load", function(){getAmbassadorStatus();});
</script>
</body>