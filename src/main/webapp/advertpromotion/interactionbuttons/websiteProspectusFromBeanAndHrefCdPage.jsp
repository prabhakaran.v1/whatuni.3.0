<%--
  -- to display WEBSITE & REQUEST_PROSPECTUS buttons with HREF format.
  -- @author: Mohamed syed.  
  -- @Since: wu305_20110322
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="org.apache.commons.validator.GenericValidator, WUI.utilities.GlobalFunction"%>
<%
  String wp_collegeId = request.getAttribute("interactionCollegeId") != null && request.getAttribute("interactionCollegeId").toString().trim().length() > 0 ? (String)request.getAttribute("interactionCollegeId") : "0";
  String wp_collegeName = request.getAttribute("interactionCollegeName") != null && request.getAttribute("interactionCollegeName").toString().trim().length() > 0 ? (String)request.getAttribute("interactionCollegeName") : "0";
  String wp_collegeNameDisplay = request.getAttribute("interactionCollegeNameDisplay") != null && request.getAttribute("interactionCollegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("interactionCollegeNameDisplay") : "";
  String wp_floatStyle = request.getParameter("FLOAT_STYLE") != null && request.getParameter("FLOAT_STYLE").trim().length() > 0 ? request.getParameter("FLOAT_STYLE").trim() : "RIGHT";
  String coureId = request.getParameter("courseId") != null && request.getParameter("courseId").trim().length() > 0 ? request.getParameter("courseId") : "0";
  String fromPage = request.getParameter("FROMPAGE") != null && request.getParameter("FROMPAGE").trim().length() > 0 ? request.getParameter("FROMPAGE") : "";
  String nonAdvFlag = request.getParameter("NONADVFLAG") != null && request.getParameter("NONADVFLAG").trim().length() > 0 ? request.getParameter("NONADVFLAG") : "";  
  //  
  String institutionId = wp_collegeId != "0" ? new GlobalFunction().getInstitutionId(wp_collegeId) : "";  
  
  String extraClass = " mr20"; 
  String wp_floatStyleClass = "";
  if(wp_floatStyle.trim().equals("CENTER")){
    wp_floatStyleClass = "center_txt cta_btns vg_btn";    
  }else if(wp_floatStyle.trim().equals("RIGHT")){
    wp_floatStyleClass = "fright";    
  }else if(wp_floatStyle.trim().equals("LEFT")){
    wp_floatStyleClass = "";    
  }
  String gaCollegeName = new WUI.utilities.CommonFunction().replaceSpecialCharacter(wp_collegeName);      
  if("0".equals(coureId)){
   extraClass = " mr17"; 
  }
  String webformPrice="";String websitePrice="";
  String courseFlag = "PROVIDER";
  if(!"0".equals(coureId)){courseFlag = "COURSE";}
  String otherUniLikeFlag = request.getParameter("OTHERUNIFLAG") != null && request.getParameter("OTHERUNIFLAG").trim().length() > 0 ? request.getParameter("OTHERUNIFLAG") : "";  
  //  
%>
<c:if test="${not empty requestScope.listOfCollegeInteractionDetailsWithMedia}">
  <c:forEach var="buttonDetails" items="${requestScope.listOfCollegeInteractionDetailsWithMedia}"> 
    <c:if test="${not empty buttonDetails.subOrderItemId}">
     <c:if test="${buttonDetails.subOrderItemId gt 0}"> 
        <c:set var="subOrderId1" value="${buttonDetails.subOrderItemId}"/>
        <%
        String subOrderId1 = (String)pageContext.getAttribute("subOrderId1");
        webformPrice = new WUI.utilities.CommonFunction().getGACPEPrice(subOrderId1, "webform_price");
        websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice(subOrderId1, "website_price");
        request.setAttribute("richProfileSubOrderId",subOrderId1);
        request.setAttribute("advWebsitePrice",websitePrice);
        %>
      <div class="<%=wp_floatStyleClass%>">    
        <c:if test="${not empty buttonDetails.subOrderWebsite}">
        <%if("0".equals(coureId)){%>
          <a rel="nofollow" 
              target="_blank" 
              class="visit-web<%=extraClass%>" 
              onclick="sponsoredListGALogging(${interactionCollegeId});GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=websitePrice%>); cpeWebClickWithCourse(this,'<%=wp_collegeId%>',<%=coureId%>,'${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.subOrderWebsite}');addBasket('<%=wp_collegeId%>', 'C', 'visitweb','basket_div_<%=wp_collegeId%>', 'basket_pop_div_<%=wp_collegeId%>', '', '', '<%=gaCollegeName%>');var a='s.tl(';" 
              href="${buttonDetails.subOrderWebsite}"
              title="Visit <%=wp_collegeNameDisplay%> website">
              Visit website
              <i class="fa fa-caret-right"></i>
              </a>
            <%}else{%>
               <a rel="nofollow" 
              target="_blank" 
              class="visit-web<%=extraClass%>" 
              onclick="sponsoredListGALogging(${interactionCollegeId});GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=gaCollegeName%>', <%=websitePrice%>); cpeWebClickWithCourse(this,'<%=wp_collegeId%>',<%=coureId%>,'${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.subOrderWebsite}');addBasket('<%=coureId%>', 'O', 'visitweb','basket_div_<%=coureId%>', 'basket_pop_div_<%=coureId%>', '', '', '<%=gaCollegeName%>');var a='s.tl(';" 
              href="${buttonDetails.subOrderWebsite}&courseid=<%=coureId%>"
              title="Visit <%=wp_collegeNameDisplay%> website">
              Visit website
              <i class="fa fa-caret-right"></i>
              </a>
            <%}%>
        </c:if>
        <c:if test="${not empty buttonDetails.subOrderEmailWebform}">
            <a rel="nofollow" 
                target="_blank"
                class="req-inf<%=extraClass%>"
                onclick="sponsoredListGALogging(${interactionCollegeId});GAInteractionEventTracking('emailwebform', 'interaction', 'email webform', '<%=gaCollegeName%>', <%=webformPrice%>); cpeEmailWebformClick(this,'<%=wp_collegeId%>','${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.subOrderEmailWebform}');var a='s.tl(';" 
                href="${buttonDetails.subOrderEmailWebform}" 
                title="Email <%=wp_collegeNameDisplay%>">
                Request info
                <i class="fa fa-caret-right"></i>
            </a>
          </c:if>
          <c:if test="${not empty buttonDetails.subOrderEmail}">
            <c:if test="${empty buttonDetails.subOrderEmailWebform}">
              <a rel="nofollow"
                onclick="sponsoredListGALogging(${interactionCollegeId});GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '<%=gaCollegeName%>');adRollLoggingRequestInfo('<%=wp_collegeId%>',this.href);return false;"
                class="req-inf<%=extraClass%>" 
                href="<SEO:SEOURL pageTitle="sendcollegemail" ><%=wp_collegeName%>#<%=wp_collegeId%>#<%=coureId%>#0#n#${buttonDetails.subOrderItemId}</SEO:SEOURL>" 
                title="Email <%=wp_collegeNameDisplay%>" >
                Request info
                <i class="fa fa-caret-right"></i>
            </a> <%--13-JAN-2015  Added for adroll marketing--%>  
          </c:if>
        </c:if>
        <c:if test="${not empty buttonDetails.subOrderProspectusWebform}">
          <a rel="nofollow" 
              target="_blank" 
              class="get-pros" 
              onclick="sponsoredListGALogging(${interactionCollegeId});GAInteractionEventTracking('prospectuswebform', 'interaction', 'prospectus webform', '<%=gaCollegeName%>', <%=webformPrice%>); cpeProspectusWebformClick(this,'<%=wp_collegeId%>','${buttonDetails.subOrderItemId}','${buttonDetails.cpeQualificationNetworkId}','${buttonDetails.subOrderProspectusWebform}');var a='s.tl(';" 
              href="${buttonDetails.subOrderProspectusWebform}" 
              title="Get <%=wp_collegeNameDisplay%> Prospectus">
              Get prospectus
              <i class="fa fa-caret-right"></i>
              </a>
         </c:if>
         <c:if test="${not empty buttonDetails.subOrderProspectus}">
            <c:if test="${empty buttonDetails.subOrderProspectusWebform}">
            
            <a  onclick="sponsoredListGALogging(${interactionCollegeId});GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '<%=gaCollegeName%>'); return prospectusRedirect('/degrees','<%=wp_collegeId%>','<%=coureId%>','','','${buttonDetails.subOrderItemId}');"
                class="get-pros"
                title="Get <%=wp_collegeNameDisplay%> Prospectus">
                Get prospectus
                <i class="fa fa-caret-right"></i>
            </a>   
           </c:if>
        </c:if>   
      </div>
    </c:if> 
    <%if(!GenericValidator.isBlankOrNull(fromPage) && "SHOWNONREQINFO".equals(fromPage) || "true".equals(nonAdvFlag)){%>
    <c:if test="${buttonDetails.subOrderItemId eq 0}">
      <a rel="nofollow" 
          target="_blank"
          class="req-inf<%=extraClass%>"
          onclick="javaScript:nonAdvPdfDownload('<%=gaCollegeName%>#<%=webformPrice%>#<%=wp_collegeId%>#${buttonDetails.subOrderItemId}#${buttonDetails.cpeQualificationNetworkId}#${buttonDetails.subOrderEmailWebform}#<%=coureId%>#${requestScope.collegeLogo}#<%=courseFlag%>#<%=institutionId%>');"
          title="Request info <%=wp_collegeNameDisplay%>">
          Request info
          <i class="fa fa-caret-right"></i>
      </a>
    </c:if>    
    <%}%>
  </c:if> 
  <%if((!GenericValidator.isBlankOrNull(fromPage) && "SHOWNONREQINFO".equals(fromPage)) || "true".equals(nonAdvFlag)){%>
  <c:if test="${empty buttonDetails.subOrderItemId}">
    <!--Added by Indumathi.S May_31_2016-->
    <%if("true".equalsIgnoreCase(otherUniLikeFlag)) {%>
      <c:if test="${not empty requestScope.listOfTopCourses}">
        <a rel="nofollow" class="req-inf mr17 uni_like" onclick="javaScript:showOtherAcdDep();" title="Other Unis Like This">
          OTHER UNIS LIKE THIS <i class="fa fa-caret-right"></i>
        </a>
      </c:if> 
    <%}%>
    <!--End-->
    <a rel="nofollow" 
          target="_blank"
          class="req-inf<%=extraClass%>"
          onclick="javaScript:nonAdvPdfDownload('<%=gaCollegeName%>#<%=webformPrice%>#<%=wp_collegeId%>#${buttonDetails.subOrderItemId}#${buttonDetails.cpeQualificationNetworkId}#${buttonDetails.subOrderEmailWebform}#<%=coureId%>#${requestScope.collegeLogo}#<%=courseFlag%>#<%=institutionId%>');"
          title="Request info <%=wp_collegeNameDisplay%>">
          Request info
          <i class="fa fa-caret-right"></i>
    </a>
  </c:if> 
  <%}%>
</c:forEach>
</c:if>

