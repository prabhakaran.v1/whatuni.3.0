<%@page import="WUI.utilities.CommonFunction" %>
<%
  String detfaultBreadCrumb = "<div> "+
                              "<a href=\"" + new CommonFunction().getSchemeName(request) +"www.whatuni.com\"> " +
                              "<span><img class=\"hicon\" alt=\"Whatuni\" title=\"Whatuni\" src=\""+new WUI.utilities.CommonUtil().getImgPath("/wu-cont/images/home_icon.png",0)+"\"></span></a></div>";
  
  String opendaySearchBreadCrumb = "<div> "+
                                   "<a href=\"" + new CommonFunction().getSchemeName(request) +"www.whatuni.com\"> " +
                                   "<span><i class=\"fa fa-home\"></i></span></a>" ;
  String opendayProviderCollegeName = (String)request.getAttribute("collegeNameBreadCrumb");
  String opendayProviderLandingPage = "<div class=\"od_bcrb\">" + 
                                      "<a href=\"" + new CommonFunction().getSchemeName(request) + "www.whatuni.com\">" +
                                      "<span><i class=\"fa fa-home fa-1_5x\"></i><span style=\"display:none;\">Home</span></span></a></div>" +
                                      "<div class=\"divider\">/</div>" + 
                                      "<div class=\"od_bcrb\">" + 
                                      "<a href=\"" + new CommonFunction().getSchemeName(request) + "www.whatuni.com/open-days/\"> <span>Open Days</span></a></div>"+
                                      "<div class=\"divider\">/</div><span>" + opendayProviderCollegeName + "</span>";

  String bc_pageName = request.getParameter("pageName") != null && request.getParameter("pageName").trim().length() > 0 ? request.getParameter("pageName") : "";
  String bc_breadCrumb = request.getAttribute("breadCrumb") != null && request.getAttribute("breadCrumb").toString().trim().length() > 0 ? request.getAttribute("breadCrumb").toString() : "";
  String bc_qualification = request.getParameter("qualification") != null && request.getParameter("qualification").trim().length() > 0 ? request.getParameter("qualification") : "";

  if(bc_pageName.equals("CATEGORY_HOME")){
    bc_breadCrumb = detfaultBreadCrumb + "<div class=\"bpos\"> &#47; </div><span class=\"bpos\">" + bc_qualification+"</span>"; 
  }else if(bc_pageName.equals("OPEN_DAY_PROVIDER_LANDING_PAGE")){
	bc_breadCrumb = opendayProviderLandingPage;
  }else if(bc_pageName.equals("OPEN_DAY_SEARCH")){
  }
  
  String bc_holdingDivClass = "bc";
  if(bc_pageName.equals("BROWSE_MONEY_PAGE") || bc_pageName.equals("CLEARING_SEARCH_RESULTS_PAGE") || bc_pageName.equals("CLEARING_PROVIDER_RESULTS_PAGE")){
   bc_holdingDivClass = "mbc";
  }else if(bc_pageName.equals("OPEN_DAY_SEARCH") || bc_pageName.equals("OPEN_DAY_PROFILE") || bc_pageName.equals("OPEN_DAY_PROVIDER_LANDING_PAGE")){
    bc_holdingDivClass = "bcrmb_cnt";
  } 
  String reviewProfilePage = request.getParameter("reviewProfilePage") != null && request.getParameter("reviewProfilePage").toString().trim().length() > 0 ? request.getParameter("reviewProfilePage").toString() : "";
if(bc_pageName.equals("OPEN_DAY_SEARCH") || bc_pageName.equals("OPEN_DAY_PROFILE") || bc_pageName.equals("REVIEW_PAGE") ||  bc_pageName.equals("OPEN_DAY_PROVIDER_LANDING_PAGE")){
%>

<div class="bcrmb_cnt">
  <div class="bcrb_wrp">
    
      <%=bc_breadCrumb%>
  
  </div>
  </div>

<% }else if (bc_breadCrumb != null && bc_breadCrumb.trim().length() > 0){%>
  <div class="<%=bc_holdingDivClass%>">
  <div class="bcrumb_blue <%=reviewProfilePage%>">
    
      <%=bc_breadCrumb%>
  
  </div>
  </div>
  <%--=bc_extraSpaceDiv--%>
<%}%>