<%@page import ="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" %>
<%
  String showVideoJsName = CommonUtil.getResourceMessage("wuni.showVideoJsName.js", null);
  String modalJs = CommonUtil.getResourceMessage("wuni.modal.js", null);
%>
<%--<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/videoplayer.js"> </script>--%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/videoplayer/<%=modalJs%>"> </script>
<%--<script type="text/javascript" src="<%=CommonUtil.getJsPath()%>/js/limelightvideoplayer/swfobject.js"></script>--%>
<%--<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/videoplayer/jwplayer.js"></script>--%>
<script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=showVideoJsName%>"> </script>

<%
 /* 0 collegeid     1 college_name     2 college_name_seo  3 review_title   4  review_desc      5  v_category 
    6 length        7 no_hit           8. overall_rating   9 V / B U        10  Video Play URL  11 Video thumb   
    12. college_profile  13. subject_profile_count   14. email_pur_flag    15.prospectus_pur_flag  16.prospectus_pur_flag   
    17.website_pur_flag     18.all_course_count  19.pg_course_count  20.degree_course_count   21.acc_found_course_cnt   22.foundation_course_cnt
    23.hnd_hnc_course_cnt   24.profile_video     25.subject_video    26.college_in_basket */
    String contextpath = request.getContextPath();
    //Changed WU scheme as part of SSL work for 08_MAR_2016, By Thiyagu G.
    String browserip = GlobalConstants.WHATUNI_SCHEME_NAME + new CommonUtil().getProperties().getString("review.autocomplete.ip");

%>         
 <input type="hidden" id="refererUrl" name="refererUrl" value="<%=(request.getHeader("referer") !=null ? request.getHeader("referer") : browserip+"/degrees/home.html")%>" />
  <input type="hidden" id="contextPath" name="contextPath" value="<%=contextpath%>" />
  <input type="hidden" id="jsPath" name="jsPath" value="<%=CommonUtil.getJsPath()%>" />
  
<div class="dialog" id="box">
   <div align="center" id="flashbanner"></div>
   <div id="linksdiv"></div>
</div>
