<%@ page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction"%>

<%
  String domainSpecificPath = new CommonFunction().getSchemeName(request)+java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.device.specific.css.path");  
  String main_480_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.480.css");
  String main_992_ver = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.whatuni.main.992.css");
  String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");
%>  

<script type="text/javascript" language="javascript">
  
var dev = jQuery.noConflict();
dev(document).ready(function(){
  adjustStyle();  
});
adjustStyle();  

function jqueryWidth() {
  return dev(this).width();
}   

function adjustStyle() {
  var width = document.documentElement.clientWidth;
  var path = ""; 
  if(width <= 480) {    
    dev('.op_mth_slider ul').attr("id", "select_cont");
    if(dev('#ajax_listOfOptions').attr('class') != 'opd_ajax'){
      dev('#ajax_listOfOptions').attr("class", "ajax_mob");
    }
    if(dev("#viewport").length == 0) {
      dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
    }        
    <%if(("LIVE").equals(envronmentName)){%>
      document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_480_ver%>";
    <%}else if("TEST".equals(envronmentName)){%>
      document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
    <%}else if("DEV".equals(envronmentName)){%>
      document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_480_ver%>";
    <%}%>                 
    }else if((width > 480) && (width <= 992)) {      
    dev('.op_mth_slider ul').attr("id", "select_cont");
    if(dev('#ajax_listOfOptions').attr('class') != 'opd_ajax'){
      dev('#ajax_listOfOptions').attr("class", "ajax_mob");
    }
    dev('.gen_srchbox').css('width', '100%').css('width', '-=18px');
      if(dev("#viewport").length == 0) {
        dev("head").append('<meta id="viewport" name="viewport" content="width=device-width; initial-scale=1.0; user-scalable=no; maximum-scale=1.0;">');
      }       
      <%if(("LIVE").equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=CommonUtil.getCSSPath()%>/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("TEST".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}else if("DEV".equals(envronmentName)){%>
        document.getElementById('size-stylesheet').href = "<%=domainSpecificPath%>/wu-cont/cssstyles/mobile/<%=main_992_ver%>";
      <%}%>            
    }else{
      if(dev("#viewport").length > 0) {
        dev("#viewport").remove();      
      }
        if(dev('#ajax_listOfOptions').attr('class') != 'opd_ajax'){
          dev('#ajax_listOfOptions').removeAttr("class");
        }
        dev('.op_mth_slider ul').removeAttr("id");
        dev('.op_mth_slider ul li').css('width', '77px');
      document.getElementById('size-stylesheet').href = "";
      dev(".hm_srchbx").hide();
    }
  }
  dev(window).on('orientationchange', orientationChangeHandler);  
  
  function orientationChangeHandler(e) {
    setTimeout(function() {
      dev(window).trigger('resize');
    }, 500);
  }      
  dev(window).resize(function() {      
    adjustStyle();  
  });  
</script>