<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@page import="WUI.utilities.CommonFunction, WUI.utilities.CommonUtil,WUI.utilities.GlobalFunction,org.apache.commons.validator.GenericValidator" %>

<% 
    String subjectname = (String) request.getAttribute("err_subject_name");
    subjectname  = subjectname !=null && !subjectname.equalsIgnoreCase("null") && subjectname.trim().length()>0 ? new CommonUtil().toTitleCase(subjectname) : "";
    subjectname = subjectname.replaceAll("\\+"," ");
    String studylevelid = (String) request.getAttribute("err_study_level_id");       
    studylevelid  = studylevelid !=null && !studylevelid.equalsIgnoreCase("null") && studylevelid.trim().length()>0 ? studylevelid : "";
    String studylevelname = (String) request.getAttribute("err_study_level_name");       
    studylevelname  = studylevelname !=null && !studylevelname.equalsIgnoreCase("null") && studylevelname.trim().length()>0 ?  new CommonUtil().toLowerCase(studylevelname)  : "";
    studylevelname = studylevelname.replaceAll("\\+"," ");
    studylevelname = studylevelname.replaceAll("\\_","-");
    String location = (String) request.getAttribute("err_location");       
    location  = location !=null && !location.equalsIgnoreCase("null") && location.trim().length()>0 ? new CommonFunction().replacePlus(location.trim()) : "";
    String providername = (String) request.getAttribute("err_provider_name");       
    providername  = providername !=null && !providername.equalsIgnoreCase("null") && providername.trim().length()>0 ? providername.trim() : "";
    String collegeId =  (String) request.getAttribute("collegeId");       
    collegeId =  collegeId != null && collegeId.trim().length() > 0? collegeId : "0";          
    String noindexfollow      =   request.getAttribute("noindex") !=null ? "TRUE"  : "FALSE";   
    noindexfollow      =   request.getAttribute("unihomefrmsearch") !=null ? "noindex,nofollow"  : noindexfollow;
    noindexfollow      =   request.getAttribute("otherindex") !=null ? String.valueOf(request.getAttribute("otherindex"))  : noindexfollow;      
    String categoryCode=request.getAttribute("categoryCode") != null ? request.getAttribute("categoryCode").toString() : "";      
    String clSearchBarForNoResults = (String)request.getAttribute("clearingSearchBar");      
    String tmpUcasCode = (request.getAttribute("ucasCodepresent") !=null && !request.getAttribute("ucasCodepresent").equals("null") && String.valueOf(request.getAttribute("ucasCodepresent")).trim().length()>0 ? String.valueOf(request.getAttribute("ucasCodepresent")) : "");
    tmpUcasCode = tmpUcasCode.replaceAll("\\+"," ");  
    String articleKeyword = (String) request.getAttribute("articleKeyword");       
    articleKeyword  = articleKeyword !=null && !articleKeyword.equalsIgnoreCase("null") && articleKeyword.trim().length()>0 ? articleKeyword.trim() : "";
    String noResults = "";
    //wu415_20120821 Added by Sekhar K for showing other qualification in no results message.
    String otherQualification = "";
    if(clSearchBarForNoResults != null){
        noResults = "Y";
        otherQualification = "Degree or HND/HNC";
        if("degree".equalsIgnoreCase(studylevelname)){
            otherQualification = "HND/HNC or Foundation degree";
        }else if("hnd/hnc".equalsIgnoreCase(studylevelname)){
            otherQualification = "Degrees or Foundation degree";
        }
    }else{
        otherQualification = "HND/HNC or Postgraduate";
        if("hnd/hnc".equalsIgnoreCase(studylevelname)){
            otherQualification = "Degree or Foundation degree";
        }else if("postgraduate".equalsIgnoreCase(studylevelname)){
            otherQualification = "Degrees or HND/HNC";
        }
    }                
    if(collegeId!=null && !"".equals(collegeId) && !"0".equals(collegeId)){
      request.setAttribute("cDimCollegeId",collegeId);
    }
    if(studylevelid!=null && !"".equals(studylevelid)){
      new GlobalFunction().getStudyLevelDesc(studylevelid,request);
    }
    if(subjectname!=null && !"".equals(subjectname)){      
      request.setAttribute("cDimKeyword", new GlobalFunction().getReplacedString(subjectname.trim().toLowerCase()).replaceAll("-"," ")); //Removed single and double quotes and replaced hyphen to fix this bug:28921, on 08_Aug_2017, By Thiyagu G
    }
    if(location!=null && !"".equals(location) && !"UNITED KINGDOM".equalsIgnoreCase(location)){
      request.setAttribute("cDimCountry", "UK");
    }
    request.setAttribute("dimCategoryCode", GenericValidator.isBlankOrNull(categoryCode)? "": new GlobalFunction().getReplacedString(categoryCode.trim().toUpperCase()));      
    String studyModeDisp = request.getAttribute("studyModeDisplay")!=null ? (String)request.getAttribute("studyModeDisplay") : "";
    if(request.getAttribute("studyModeDisplay")!=null && !"".equals(request.getAttribute("studyModeDisplay"))){
      request.setAttribute("cDimStudyMode", studyModeDisp.toLowerCase());
    }    
    
    int stind1 = request.getRequestURI().lastIndexOf("/");
    int len1 = request.getRequestURI().length();
    String page_name = request.getRequestURI().substring(stind1+1,len1);
    page_name = page_name !=null ? page_name.toLowerCase() : page_name;
    String channel  = "whatuni";
    String serverName = request.getServerName() !=null ? request.getServerName().toLowerCase() : "";
    String pageType = request.getParameter("pagemessage");
    String commonCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.common.css");
    String homeCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.homepage.css");
    String mainHeaderCSS = CommonUtil.getResourceMessage("wuni.whatuni.main.header.css", null);//Added this css for Header changes by Hema.S on 23_OCT_2018_REL
    String flexSliderCssName = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.flexslider.css");
%>

<body>
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>                
    </div>      
  </div>    
</header>
<!-- Page Content Starts-->    
<section class="main_cnr pt20 pb40">
<div class="ad_cnr">	
  <div class="content-bg error_404">
    <%if(articleKeyword != null && !"".equals(articleKeyword)){%>
      <div class="error_desc">            
        <h3 class="fnt_lrg">No results for <c:if test="${not empty requestScope.err_subject_name}"> <%=articleKeyword%></c:if></h3>
        <p class="fnt_lrg">Sorry, we couldn't find any results for that search!</p>
      </div>	
      <jsp:include page="/exception/suggest.jsp"/>
    <%}else{%>
      <div class="error_desc">            
        <h3 class="fnt_lrg">No results for <%if("Y".equals(noResults)){%>clearing<%}%> <c:if test="${not empty requestScope.err_subject_name}"> <%=subjectname%></c:if></h3>
        <%if("Y".equals(noResults)){%>                      
          <p class="fnt_lrg">Sorry, we couldn't find any results for that search!</p>
        <%}else{%>
          <c:if test="${empty requestScope.suggestionWords }"><p class="fnt_lrg">Sorry, we couldn't find any results for that search!</p></c:if>
          <c:if test="${not empty requestScope.suggestionWords}">
            <p class="fnt_lrg">Sorry, we couldn't find any results for that search!</p>
            <p>Did you mean;</p>
            ${requestScope.suggestionWords}
            <p> We update all courses regularly, after all this is a big decision. With our next update we may have  <%=studylevelname%> courses<logic:present name="err_provider_name" scope="request"> in/at <%=providername%><logic:present name="err_location" scope="request"> <%=location%></logic:present></logic:present><logic:notPresent name="err_provider_name" scope="request"> in/at <logic:present name="err_location" scope="request"><%=location%></logic:present></logic:notPresent>, so please do return to the site.</p>    
            <p>However in the meantime, please try again by broadening your choices of <%=studylevelname%> courses by:</p>
            <ul class="w_search">
              <li>using our unique <a href="<%=request.getContextPath()%>/searchHome.html" title="university course finder and comparison tool">university course finder and comparison tool</a></li>
              <li>browsing through our <a href="<SEO:SEOURL pageTitle="unibrowse">none</SEO:SEOURL>" title="college or university list">college or university list</a></li>
              <li>browsing through our <a href="<%=request.getContextPath()%>/courses/browse.html" title="course list">course list</a></li>
            </ul> 
            <p>Please note reviews of <%=studylevelname%> courses 
            
            <c:if test="${not empty requestScope.err_provider_name }"> in <%=providername%><c:if test="${not empty requestScope.err_location}"> <%=location%></c:if></c:if>
            <c:if test="${empty requestScope.err_provider_name }"><c:if test="${not empty requestScope.err_location}"> in <%=location%></c:if></c:if> are the subjective opinions of Whatuni members and not of Whatuni.com</p>
          </c:if>
        <%}%>                                    
      </div>	            
      <!--28_Oct_2014 added fired persons image by Thiyagu G.-->  
      <jsp:include page="/exception/suggest.jsp"/>
      <c:if test="${not empty requestScope.lOneLevelHtmlContent}">
          <section class="pcou">
            <div class="content-bg">
              <div class="popular_courses">
                <h5 class="fnt_lbd">Popular Courses</h5>
                ${requestScope.lOneLevelHtmlContent}
              </div>
            </div> 
          </section>
         </c:if>     
    <%}%>
  </div>
</div>
</section>    
<jsp:include page="/jsp/common/wuFooter.jsp" />
</body>