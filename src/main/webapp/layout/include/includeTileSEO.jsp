<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<c:set var="pagename3">
   <tiles:getAsString name="pagename3" ignore="true"/>
</c:set>
<c:choose>
   <c:when test="${fn:containsIgnoreCase(pagename3, 'browseMoneyPageResults.jsp') and not empty seoMetaDetailsSRPage}">
      <c:forEach var="seoMetaDetailsSRPage" items="${seoMetaDetailsSRPage}" varStatus="i">
         <c:if test="${not empty seoMetaDetailsSRPage.metaTitle}" >
            <title>${seoMetaDetailsSRPage.metaTitle}</title>
         </c:if>
         <c:if test="${not empty seoMetaDetailsSRPage.metaDesc}" >
            <meta name="description" content="${seoMetaDetailsSRPage.metaDesc}"/>
         </c:if>
         <c:choose>
            <c:when test="${not empty seoMetaDetailsSRPage.metaRobots}" >
               <c:set var="META_ROBOTS_VALUE" value="${seoMetaDetailsSRPage.metaRobots}"/>
            </c:when>
            <c:when test="${not empty META_ROBOTS_NOINDEX_FOLLOW}" >
               <c:set var="META_ROBOTS_VALUE" value="${META_ROBOTS_NOINDEX_FOLLOW}"/>
            </c:when>
            <c:otherwise>
               <c:set var="META_ROBOTS_VALUE" value="index,follow"/>
            </c:otherwise>
         </c:choose>
         <meta name="ROBOTS" content="${META_ROBOTS_VALUE}"/>
         <meta name="country" content="United Kingdom"/>
         <meta name="content-type" content="text/html; charset=utf-8"/>
      </c:forEach>
   </c:when>
</c:choose>