<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>

<c:set var="pagename3">
  <tiles:getAsString name="pagename3" ignore="true"/>
</c:set>

<c:choose>
  <c:when test="${fn:containsIgnoreCase(pagename3, 'browseMoneyPageResults.jsp')}">   
  
    <link rel="stylesheet" type="text/css" href="<wu:csspath source='/cssstyles/'/><spring:message code='wuni.searchpage.css'/>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<wu:csspath source='/cssstyles/'/><spring:message code='wuni.whatuni.main.header.css'/>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<wu:csspath source='/cssstyles/'/><spring:message code='wuni.common.css'/>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<wu:csspath source='/cssstyles/'/><spring:message code='wuni.clearing.searchpage.css'/>" media="screen" /> 
<%--     <link rel="stylesheet" type="text/css" href="<wu:csspath source='/cssstyles/'/><spring:message code='wuni.whatuni.grade.filter.css'/>" media="screen" />   
 --%>    <link rel="stylesheet" type="text/css" href="<wu:csspath source='/cssstyles/'/><spring:message code='wuni.whatuni.smart.banner.css'/>" media="screen" />       
    <!-- Js --> 
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.jquery.js'/>"></script>    
    <c:choose>
      <c:when test="${'N' ne sessionScope.sessionData['cookieTargetingCookieDisabled']}">
        <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.ecommerce.tracking.disabled.js'/>"></script> 
      </c:when>
      <c:otherwise>
        <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.ecommerce.tracking.js'/>"></script> 
      </c:otherwise>
    </c:choose> 
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/ticker.js'/>" defer="defer"></script> 
  </c:when>
</c:choose>
