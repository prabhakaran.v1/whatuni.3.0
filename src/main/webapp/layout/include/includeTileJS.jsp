<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix = "wu" uri = "/WEB-INF/tlds/wutags.tld" %>

<c:set var="pagename3">
<tiles:getAsString name="pagename3" ignore="true"/>
</c:set>

<c:choose>
  <c:when test="${fn:containsIgnoreCase(pagename3, 'browseMoneyPageResults.jsp')}">
    <script type="text/javascript" src="https://apis.google.com/js/client.js" defer="defer"></script>
    <c:if test="${'N' eq sessionScope.sessionData['cookieTargetingCookieDisabled']}">
	  <script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 998012114;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/998012114/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
	  </noscript>
	</c:if>       
	<c:if test="${not empty featuredBrandList}">
      <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.sr.featuredbrand.js'/>"></script>
    </c:if>  
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/searchresult/'/><spring:message code='wuni.searchresult.pageload.js'/>"></script>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/searchresult/'/><spring:message code='wuni.searchresult.js'/>"></script> 
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.header.footer.js'/>"></script>  
<%--     <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/jquery/'/><spring:message code='wuni.slimscroll.js'/>"></script>
 --%>    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/javascripts/'/><spring:message code='wuni.interaction.js'/>"></script>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/javascripts/'/><spring:message code='wuni.social.box.js'/>"></script>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/home/'/><spring:message code='wuni.switch.header.js'/>"></script>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.commonSrchJSName.js'/>"></script>   
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/videoplayer/'/><spring:message code='wuni.modal.js'/>"></script>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/autoComplete/ajax_wu564.js'/>"></script>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/autoComplete/'/><spring:message code='wuni.ajaxDynamicList.js'/>"> </script>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.commonJsName.js'/>"></script>
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.allJsName.js'/>"> </script>       
    <script type="text/javascript" language="javascript" src="<wu:jspath source='/js/'/><spring:message code='wuni.sr.gradefilter.js'/>"></script>
  </c:when>
</c:choose>