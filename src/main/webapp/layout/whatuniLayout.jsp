<!DOCTYPE html>
<html lang="en" dir="ltr">
	
   <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
   <%-- oracle.jsp.util.PublicUtil.setWriterEncoding(out, "UTF-8"); --%>
   <% request.setCharacterEncoding("UTF-8"); %>
   <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
	
   <head>
        <tiles:insertAttribute name="thinngsToincludeForSEO"/>
		<tiles:insertAttribute name="thinngsToincludeForCSS"/>
		<tiles:insertAttribute name="includeAboveHead"/>
   </head>
   <body>
   <tiles:insertAttribute name="includeAboveBodyContent"/>
   <tiles:insertAttribute name="bodyContent"/>
   <tiles:insertAttribute name="thinngsToincludeForJS"/>
   </body>
</html>