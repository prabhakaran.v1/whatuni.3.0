<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%--
 *
 *
   * LAYOUT to define single-middle-layout like below (middle will occupy full width, no left/right columns)
 *
 *  -----------------------
 *  | 	Header            |
 *  -----------------------
 *  |	Middle 		      |
 *  |                     |
 *  |                     |
 *  |   Footer            |
 *  -----------------------
 *
 * @author   Sabapathi S
 * @since    whatuni1.0_20190129 - initial draft
 * @version  1.1
 *
--%>
<c:set var="pagename3">
	<tiles:getAsString name="pagename3" ignore="true" />
</c:set>
<%
  String pageName = (String) pageContext.getAttribute("pagename3");
  String htmlId = "";
  if("clearingCourseSearchPage".equalsIgnoreCase(pageName) || "clearingBrowseMoneyPage.jsp".equalsIgnoreCase(pageName) || "courseDetails.jsp".equalsIgnoreCase(pageName)){
     htmlId = "videoid";
  }
  request.setAttribute("pagename3", pageName);
%>

<!DOCTYPE html>
<c:choose>
 <c:when test="${'true' eq param.amp}">
   <html amp lang="en" dir="ltr" id="<%=htmlId%>">
 </c:when>
 <c:otherwise>
   <html lang="en" dir="ltr" id="<%=htmlId%>">
 </c:otherwise>
</c:choose>
	
   <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
   <%-- oracle.jsp.util.PublicUtil.setWriterEncoding(out, "UTF-8"); --%>
   <% request.setCharacterEncoding("UTF-8"); %>
   
	
   <head>
		<tiles:insertAttribute name="thinngsToincludeInHead"/>
		<tiles:insertAttribute name="miscellaneousTracking1"/>
   </head>
   
   
   <tiles:insertAttribute name="bodyContent"/>
   
</html>
