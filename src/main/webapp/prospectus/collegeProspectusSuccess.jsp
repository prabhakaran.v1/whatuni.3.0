<%Long startJspBuild = new Long(System.currentTimeMillis());%>
<%@page import="WUI.utilities.SessionData, java.util.List, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants, WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator" autoFlush="true" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

  <%
    String collegeId   =  (String) request.getAttribute("collegeId"); 
    String GA_newUserRegister   =  (String) request.getAttribute("GA_newUserRegister");//5_AUG_2014     
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");    
    if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0){ 
    cpeQualificationNetworkId = "2";
    }request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);
    String tmp_collegeId = request.getAttribute("collegeId") != null && request.getAttribute("collegeId").toString().trim().length() > 0 ? (String)request.getAttribute("collegeId") : "0";
    String tmp_collegeName = request.getAttribute("collegeName") != null && request.getAttribute("collegeName").toString().trim().length() > 0 ? (String)request.getAttribute("collegeName") : ""; //Remove space in ga college name by Prabha on 28_Jun_2016
    String gaCollegeName = new  CommonFunction().replaceURL(tmp_collegeName);
    com.wuni.util.seo.SeoUrls seoUrl = new com.wuni.util.seo.SeoUrls();
    String uniUrl = (seoUrl.construnctUniHomeURL(tmp_collegeId, tmp_collegeName)).toLowerCase();
    request.setAttribute("hitbox_eventtag", GlobalConstants.REQUEST_PROSPECTUS_EVENT);
    request.setAttribute("hitbox_college_id", collegeId);
    request.setAttribute("hitbox_college_name", tmp_collegeName);    
    int listSize = 0;   
    String newUserFlag = (String)request.getAttribute("newUserFlag");
    String showCollegeDisplayName = "";
    String postenquirysuggestionJsName = CommonUtil.getResourceMessage("wuni.post.enquiry.suggestion.js", null);
   %>

<!--Changed new responsive design for 03-Nov-2015, by Thiyagu G-->
<body>  
  <c:if test="${not empty requestScope.pixelTracking}">${requestScope.pixelTracking}</c:if> <!--18_NOV_2014 Added by Amir for pixel tracking in email success page-->
  <% String urlString  = request.getQueryString()!=null ? request.getQueryString() : "";%>
  <header class="clipart">
    <div class="ad_cnr">
      <div class="content-bg">
        <div id="desktop_hdr">    
          <jsp:include page="/jsp/common/wuHeader.jsp" />
        </div>
      </div>  
    </div>    
    <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=postenquirysuggestionJsName%>"></script>
  </header>  
  <section class="main_cnr pt40 pb40">
  <div class="ad_cnr">
    <div id="sub_container" class="content-bg">
      <div class="sub_cnr abt">
        <div class="fr rht_sec main_success">
          <div class="success p20">
            <c:if test="${not empty requestScope.prospectusSentCollegeNames}">
              <c:forEach var="prospectusSentCollegeNames" items="${requestScope.prospectusSentCollegeNames}">
                <c:set var="collegeDisplayNameId" value="${prospectusSentCollegeNames.collegeDisplayName}"/>
                <%showCollegeDisplayName = (String)pageContext.getAttribute("collegeDisplayNameId");%>
              </c:forEach>
            </c:if>
            <h6 class="fnt_lbd pb5">Job Done.</h6>
            <p class="txt fnt_lrg m0 p0">
              <i class="icon-ok mr5"></i>
              Your prospectus request has been sent to <a href="<%=uniUrl%>" class="fbold fnt13"><%=showCollegeDisplayName%></a> and should be winging its way to you soon!
            </p>            
         </div> 
         <div class="pros_scroll">            
              <ul class="uni_menu">               
                <li><a>
                  <span class="fl uni fnt_lrg"><%=showCollegeDisplayName%></span>
                  <i class="fa icon-ok fa-1_5x fr"></i>
                  </a>
                </li>                
              </ul>             
          </div>          
         <jsp:include page="/jsp/advertiser/prospectus/prospectusPostEnquiry.jsp"/>         
        </div>
      </div>
    </div>
  </div>
</section>
  <jsp:include page="/jsp/common/wuFooter.jsp" />
  <c:if test="${not empty sessionScope.prospectusPEList}">
  <script type="text/javascript">    
    insightIntLogging("<%=collegeId%>","post enquiry");
  </script>
  </c:if>
  <%if(!GenericValidator.isBlankOrNull(newUserFlag) && "Y".equals(newUserFlag)){%>
  <script type="text/javascript"> 
     GARegistrationLogging('register', "Prospectus", '<%=gaCollegeName%>','<%=GA_newUserRegister%>'); 
  </script>
  <%}%>
</body>