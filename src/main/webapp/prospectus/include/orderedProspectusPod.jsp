<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonFunction,WUI.utilities.GlobalConstants" autoFlush="true" %>

<%
  String allProsCount = request.getAttribute("allProsCount")!=null? request.getAttribute("allProsCount").toString():"";
  CommonFunction common = new CommonFunction();
  String gaCollegeName = "";
%>

<c:if test="${not empty requestScope.userProsList}">
<div class="myfive_subCnt" id="orderedPros">
 <!--List View Container-->  
 <div class="list_cnr fl cf">
  <h3 class="fnt_lrg">
   <span class="Ptit">Prospectuses you've ordered </span>
   <a class="select_all fr fnt_lrg" onclick="orderedViewAll();">
    <span class="v_txt">VIEW ALL</span>
    <span class="count">(<%=allProsCount%>)</span>
   </a>
  </h3>
  <div class="list_view fl">
    <c:forEach var="userPros" items="${requestScope.userProsList}" varStatus="index">
      <div class="row" onclick="">
        <div class="c10">
         <div class="img_ppn">
          <img alt="${userPros.collegeNameDisplay}"
               title="${userPros.collegeNameDisplay}"
               src="${userPros.collegeLogoPath}"></img>
         </div>
        </div>
       
        <div class="c11 fnt_lbd">
         <span class="fnt_lbd">${userPros.collegeNameDisplay}</span>
         <span class="mypros_dt">
           <c:if test="${not empty userPros.dateSent}">
             Ordered on ${userPros.dateSent}
           </c:if>
         </span>
         <c:set var="defineCollegeDispName" value="${userPros.collegeNameDisplay}"/>
         <%
          String collegeDispName = ((String)pageContext.getAttribute("defineCollegeDispName")).replaceAll("&","and");
         %>
         <%--email domain changed by Sangeeth as part of rebranding changes 30_JAN_19 REL--%>        
         <a class="fnt_lrg fl blue"
            href="mailto:<%=GlobalConstants.UK_DOMESTIC_EMAIL%>?Subject=A%20student%20hasn't%20received%20their%20prospectus%20yet&amp;body=Hi%20Whatuni%20team,%0D%0A%0D%0AI%20ordered%20a%20prospectus%20<c:if test="${not empty userPros.dateSent}">on%20${userPros.dateSent}</c:if>%20from%20<%=collegeDispName%>%20but%20haven't%20received%20it%20yet.%0D%0A%0D%0ABest%20wishes,%0D%0A
                ${userPros.detailUserName}"
            title="${userPros.collegeNameDisplay}">Not received it yet?</a>
        </div>
       
        <div class="c12">          
         <div class="btn_grn">   
            <c:if test="${not empty userPros.finalChoiceExistsFlag}">   
              <c:if test="${userPros.finalChoiceExistsFlag eq 'N'}">
                 <c:if test="${not empty userPros.collegeNameDisplay}">
                 <c:set var="collegeName" value="${userPros.collegeNameDisplay}" />
                <%gaCollegeName = common.replaceSpecialCharacter((String)pageContext.getAttribute("collegeName"));%>
              </c:if>
              <div class="btn_add_final">
               
                <div class="btn_grn oml_cnt" id="basket_div_${userPros.collegeId}">
                                  <div class="f5button f5_add_btn" >
                                    <a class="btn4 add" href="javaScript:void(0);" onclick='addBasket("${userPros.collegeId}", "C", this,"basket_div_${userPros.collegeId}", "basket_pop_div_${userPros.collegeId}","", "", "<%=gaCollegeName%>");'>
                                    <div class="cer1"><span class="fa fa-heart"></span>COMPARE</div></a>
                                </div>
                                </div>
                                <div class="btn_grn oml_cnt" id="basket_pop_div_${userPros.collegeId}" style="display:none;">
                                <div class="f5button f5_add_btn">
                                  <a class="btn4 add act" href="javaScript:void(0);" onclick='addBasket("${userPros.collegeId}", "C", this,"basket_div_${userPros.collegeId}", "basket_pop_div_${userPros.collegeId}");'>
                                    <div class="cer1"><span class="fa fa-heart hrt_act"></span>COMPARE</div>
                                  </a>
                                </div>
                                 </div>  
                </div>
              </c:if>
            </c:if>
         </div>         
        </div>
      </div>
    </c:forEach>
  </div>
 </div>  
 <!--List View Ends-->
</div>
</c:if>