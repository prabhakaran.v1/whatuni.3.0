<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*, WUI.utilities.CommonFunction, WUI.utilities.CommonUtil" autoFlush="true" %>

    <%  String metaRobots = request.getAttribute("metarobots") != null ? String.valueOf(request.getAttribute("metarobots")) : "noindex,follow"; //21-Jan-2014 Release
        String urlString  = request.getAttribute("loginUrl")  != null ?  String.valueOf(request.getAttribute("loginUrl")) : "/home.html";
        String pageno	          =   (String) (request.getAttribute("pageno") !=null ? request.getAttribute("pageno") : "1");
        String totalCount        =   request.getAttribute("allProsCount")!= null ? request.getAttribute("allProsCount").toString() : "1";
        String url = (String)request.getAttribute("urlString");
        String studyLevel = "";        
        String networkId = (String)request.getAttribute("netWorkId");
        String urlArray[] = url.split("/");
        String uClass = "actl";
        String pgClass = "nacr"; 
        String pageName = "PROSPECTUS BROWSE LOCATION";  
               studyLevel = (String)request.getAttribute("studyLevel");
        String queryString = (String)request.getAttribute("modqueryStr");
               queryString = queryString!= null & queryString != "" ? queryString:"";
        String logCollegeId = request.getParameter("collegeid");        
        //
        String queryStringPros = (String)request.getAttribute("modqueryStrWO");
               queryStringPros = queryStringPros!= null & queryStringPros != "" ? queryStringPros:"";
        //
        //String prospectusURL = "/degrees/prospectus/";
        String prospectusURL = "/degrees"+url+".html";        
        String fullQueryStr = (String)request.getAttribute("queryStr");
        fullQueryStr = fullQueryStr!=null && fullQueryStr !="" ? "?"+fullQueryStr: "";
        String sort = request.getAttribute("SORT") == null || ((String)request.getAttribute("SORT")).trim().length() == 0 ? "" :  "&sort="+((String)request.getAttribute("SORT")).trim();
        url = prospectusURL+"?mypage=my_prospectus"+sort;
        int sliderCount = 0;
        //'asc_times_ranking
        String universitySortStr = (fullQueryStr.contains("sort=asc_college") || fullQueryStr.contains("sort=desc_college")) ? (fullQueryStr.contains("sort=asc_college") ? fullQueryStr.replace("asc_college","desc_college").replace("pageno="+pageno ,"pageno=1") : fullQueryStr.replace("desc_college","asc_college").replace("pageno="+pageno ,"pageno=1")) : (queryString!="" ?   (queryString + "&mypage=my_prospectus&sort=asc_college&pageno=1"): "?mypage=my_prospectus&sort=asc_college&pageno=1"); // Yogeswari :: 19.05.2015 :: for adding sort type when pagination.
        String universitySortClass = (fullQueryStr.contains("sort=asc_college") || fullQueryStr.contains("sort=desc_college")) ? (fullQueryStr.contains("sort=asc_college") ? "fa fa-angle-up fa-1_4x fl" : "fa fa-angle-down fa-1_4x fl") : "fa fa-angle-up fa-1_4x fl";
        String ratinSortStr = (fullQueryStr.contains("sort=asc_student_rating") || fullQueryStr.contains("sort=desc_student_rating")) ? (fullQueryStr.contains("sort=asc_student_rating") ? fullQueryStr.replace("asc_student_rating","desc_student_rating").replace("pageno="+pageno ,"pageno=1") : fullQueryStr.replace("desc_student_rating","asc_student_rating").replace("pageno="+pageno ,"pageno=1")) : (queryString!="" ?   (queryString + "&mypage=my_prospectus&sort=asc_student_rating&pageno=1"): "?mypage=my_prospectus&sort=asc_student_rating&pageno=1"); // Yogeswari :: 19.05.2015 :: for adding sort type when pagination.
        String ratinSortStrClass = (fullQueryStr.contains("sort=asc_student_rating") || fullQueryStr.contains("sort=desc_student_rating")) ? (fullQueryStr.contains("sort=asc_student_rating") ? "fa fa-angle-up fa-1_4x fl" : "fa fa-angle-down fa-1_4x fl") : "fa fa-angle-up fa-1_4x fl";
        
         if(studyLevel.equalsIgnoreCase("M")){
            pageName = "PROSPECTUS BROWSE LOCATION";
         }else if(studyLevel.equalsIgnoreCase("L")){               
            pageName = "PG PROSPECTUS BROWSE LOCATION";
         }
        String canonicalLink = "";
        CommonFunction commonFun = new CommonFunction();
        String httpStr = commonFun.getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
         if(fullQueryStr!="" && fullQueryStr != null){
           canonicalLink = httpStr + "www.whatuni.com/degrees/prospectus/";
           metaRobots = "noindex,follow";
        }else{
           metaRobots = "index,follow";
        }
        String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null);
        String prospectusJs =CommonUtil.getResourceMessage("wuni.prospectusJsName.js", null);
        CommonFunction common = new CommonFunction();
        String gaCollegeName = "";
    %>

   <body id="www-whatuni-com">     
     <header class="clipart">
      <div class="">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>      
      
     </header>
     <section id="myPros" class="Main_Cnr My_Pros">        
       <div class="ad_cnr">
         <div class="content-bg pros">
           <div class="subCnr">             
                          
            <!------------------------------------------------------>
             <c:if test="${not empty requestScope.userProsList}">
             <div class="myfive_subCnt all_pros">
             <div class="list_cnr fl cf pb40">
               <h3 class="fnt_lrg">
                 <span class="Ptit_cnr">
                   <span class="Ptit">All prospectuses ordered </span>                   
                 </span>                 
               </h3>	
               <div class="row-fluid">
                 <hgroup>
                   <h6 class="lv_c1"><span class="fl fnt_lbd">University name</span>
                    <a href="<%=prospectusURL%><%=universitySortStr%>"><i id="arrowClass" class="<%=universitySortClass%>"></i></a>
                   </h6>
                   <h6 class="lv_c2"><span class="fl fnt_lbd">Student rating</span>
                    <a href="<%=prospectusURL%><%=ratinSortStr%>"><i id="arrowClass" class="<%=ratinSortStrClass%>"></i></a>
                   </h6>                   
                 </hgroup>
               </div>
               <div class="row-fluid">
                 <div class="list_view fl" >
                   
                   <c:forEach var="userPros" items="${requestScope.userProsList}" varStatus="index">
                   <div class="row">
                     <div class="c1">
                       <div class="img_ppn">
                         <img class="lazy-load" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" data-src="${userPros.collegeLogoPath}" alt="${userPros.collegeNameDisplay}" title="${userPros.collegeNameDisplay}" />    
                       </div>
                     </div>
                     <div class="c2 fnt_lbd">
                       <span class="fnt_lbd">${userPros.collegeNameDisplay}</span>
                       <c:if test="${not empty userPros.openDays}">
                             <a class="fnt_lrg fl blue" href="${userPros.openDaysUrl}" title="${userPros.collegeNameDisplay}"> Next Openday ${userPros.openDays}</a>
                       </c:if>
                     </div>
                     <div class="c3">                      
                       <div class="fnt_lrg">STUDENT RATING</div>
                         <div class="rev_return pl10 mb15">
                         <fmt:parseNumber var = "overallRating" type = "number" value = "${userPros.overallRating}" />
                         <c:if test="${not empty overallRating}">
                           <c:if test="${overallRating gt 0}"> 
                           <span class="rat${userPros.overAllRatingRounded} t_tip">
													                <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                     <span class="cmp">
								                       <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                      <div class="line"></div>
								                     </span>
								                    <%--End of rating tool tip code--%>
													              </span>
                           <span class="rat_value fl">(${userPros.overAllRatingRounded})</span>
                           </c:if>
                       </c:if>
                         </div>
                       </div>                       
                       <div class="c4 fnt_lbd">
                         <div class="btn_grn">
                           <div class="btn_add_final">
                             <c:if test="${not empty userPros.finalChoiceExistsFlag}">
                               <c:if test="${userPros.finalChoiceExistsFlag eq 'N'}"> 
                            <%--Change add to final five button into compare by Prabha--%>
                                 <c:if test="${not empty userPros.collegeNameDisplay}">
                                  <c:set var="collegeName" value="${userPros.collegeNameDisplay}"/>
                                  <%gaCollegeName = common.replaceSpecialCharacter((String)pageContext.getAttribute("collegeName"));%>
                                </c:if>
                                <div class="btn_grn oml_cnt" id="basket_div_${userPros.collegeId}">
                                  <div class="f5button f5_add_btn" >
                                    <a class="btn4 add" href="javaScript:void(0);" onclick='addBasket("${userPros.collegeId}", "C", this,"basket_div_${userPros.collegeId}", "basket_pop_div_${userPros.collegeId}","", "", "<%=gaCollegeName%>");'>
                                    <div class="cer1"><span class="fa fa-heart"></span>COMPARE</div></a>
                                  </div>
                                </div>
                                <div class="btn_grn oml_cnt" id="basket_pop_div_${userPros.collegeId}" style="display:none;">
                                  <div class="f5button f5_add_btn">
                                    <a class="btn4 add act" href="javaScript:void(0);" onclick='addBasket("${userPros.collegeId}", "C", this,"basket_div_${userPros.collegeId}", "basket_pop_div_${userPros.collegeId}");'>
                                      <div class="cer1"><span class="fa fa-heart hrt_act"></span>COMPARE</div>
                                    </a>
                                  </div>
                                </div>  
                              </c:if>
                            </c:if>
                          </div>
                         </div>
                       </div>
                     </div>
                    </c:forEach>
                   </div>
                 </div>
               </div>
               </div>
               <jsp:include page="/reviewRedesign/include/reviewPagination.jsp">
                <jsp:param name="pageno" value="<%=pageno%>"/>            
                <jsp:param name="pagelimit"  value="10"/>
                <jsp:param name="searchhits" value="<%=totalCount%>"/>
                <jsp:param name="recorddisplay" value="10"/>              
                <jsp:param name="pageURL" value="<%=url%>"/>                
                <jsp:param name="pageTitle" value=""/>
              </jsp:include>                
            </c:if>
            <!------------------------------------------------------>             
           </div>   
         </div>         
       </div>
     </section>
     <jsp:include page="/jsp/common/wuFooter.jsp" /> 
     <script>
     var mypros = jQuery.noConflict();
     pros(document).ready(function(){  
       lazyloadetStarts();
     });
     pros(window).scroll(function(){ // scroll event 
        lazyloadetStarts();
    });
     </script>
  </body>



