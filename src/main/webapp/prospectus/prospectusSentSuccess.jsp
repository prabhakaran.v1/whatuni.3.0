<%@ page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalFunction, WUI.utilities.CommonFunction, org.apache.commons.validator.GenericValidator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

   
    <%
      String token = ""+session.getAttribute("_synchronizerToken");
      CommonFunction comFun = new CommonFunction();      
      String postenquirysuggestionJsName = CommonUtil.getResourceMessage("wuni.post.enquiry.suggestion.js", null);      
    %>




<body id="www-whatuni-com"> 
<header class="clipart">
  <div class="ad_cnr">
    <div class="content-bg">
      <div id="desktop_hdr">    
        <jsp:include page="/jsp/common/wuHeader.jsp" />
      </div>
    </div>  
  </div>  
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=postenquirysuggestionJsName%>"></script>
</header>
<section class="main_cnr pt40 pb40">
  <div class="ad_cnr">
    <div id="sub_container" class="content-bg">
      <div class="sub_cnr abt">
        <div class="fr rht_sec main_success">
          <div class="success p20">
            <h6 class="fnt_lbd pb5">Congratulations</h6>
              <p class="txt fnt_lrg m0 p0">
                <i class="icon-ok mr5"></i>
                  Your prospectus request has been successfully sent to the following unis
              </p>
         </div>     
          <div class="pros_scroll">
            <c:if test="${not empty requestScope.prospectusSentCollegeNames}">
              <ul class="uni_menu">
               <c:forEach var="prospectusSentCollegeNames" items="${requestScope.prospectusSentCollegeNames}">
                <li><a>
                  <span class="fl uni fnt_lrg">${prospectusSentCollegeNames.collegeDisplayName}</span>
                  <i class="fa icon-ok fa-1_5x fr"></i>
                  </a>
                </li>
                </c:forEach>
              </ul> 
            </c:if>
          </div>
          <%-- Commented on 20_Nov_2018 by Prabha
          <logic:present name="prospectusPEList" scope="request" >
            <form action="/degrees/send-mywhatuni-postenquiry.html?_synchronizerToken=<%=token%>&type=DLDPROSPECTUS" method="post" id="postprospecuts" name="postprospectusform">
              <div class="list_cnr fl cf pb40">
                <h3 class="fnt_lrg">
                  Other students also downloaded these... 
                  <a id="sr_selectAll" onclick="javascript:selectAllForDld('SELECT','postprospecuts')" class="select_all fr fnt_lrg">SELECT ALL</a>
                  <a id="sr_DeSelectAll" onclick="javascript:selectAllForDld('DESELECT','postprospecuts')" class="select_all fr fnt_lrg" style="display:none;"  >DESELECT ALL</a>
                </h3>
                <div class="list_view fl">
                  <logic:iterate id="prospectusPEList" name="prospectusPEList" scope="request" indexId="i" type="com.wuni.util.valueobject.ql.PostEnquiryVO">        
                     <bean:define id="postCollegeId" name="prospectusPEList" property="collegeId" type="java.lang.String"/> 
                     <bean:define id="postCollegeName" name="prospectusPEList" property="collegeName" type="java.lang.String"/> 
                     <logic:notEmpty name="prospectusPEList" property="courseId">
                       <bean:define id="postCourseId" name="prospectusPEList" property="courseId" type="java.lang.String"/>
                       <%poCourseId=postCourseId;%>
                     </logic:notEmpty>                     
                     <%String gaCollegeName = comFun.replaceSpecialCharacter(postCollegeName);%>
                     <div id="dpRow_<%=i%>" class="row" onclick="javascript:addDLProspectus('<%=i%>');">
                       <div class="c1">
                         <div class="img_ppn">
                           <img alt="<bean:write name="prospectusPEList" property="collegeDisplayName"/>" title="<bean:write name="prospectusPEList" property="collegeDisplayName"/>" src="<bean:write name="prospectusPEList" property="collegeLogo"/>">
                         </div>
                       </div>
                       <div class="c2 fnt_lbd">
                         <span class="fnt_lbd"> <bean:write name="prospectusPEList" property="collegeDisplayName"/> </span>
                         <input type="hidden"  name="emailcheck" value="<%=postCollegeId%>#<bean:write name="prospectusPEList" property="subOrderItemId"/>#<bean:write name="prospectusPEList" property="cpeQualificationNetworkId"/>#<bean:write name="prospectusPEList" property="collegeDisplayName"/>#Y#<bean:write name="prospectusPEList" property="dpURL"/>#<%=postCollegeName%>#<bean:write name="prospectusPEList" property="emailPrice"/>#<%=poCourseId%>" id="hidden_dpRow_<%=i.intValue()%>" />                         
                       </div>
                       <a onclick="javascript:addDLProspectus('<%=i%>');">
                         <i id="addRemoveIcon_<%=i%>" class="fa fa-plus fa-1_5x" onclick="javascript:addDLProspectus('<%=i%>');"></i>
                       </a>
                     </div>         
                   </logic:iterate>
                 </div>             
                <div class="fr w100p mt20">
                  <a class="btn1 bg_orange fr mt10" onclick="postProspectus(this);" >
                      DOWNLOAD
                     <i class="fa fa-long-arrow-right"></i>
                  </a>
                </div> 
              </div>    
              <div class="fr fr mt20">
                <span class="fnt_lbd fnt_14">Return to</span>
                <a class="link_blue fnt_lbd fnt_14" href="<%=comFun.getDomainName(request, "N")%>">homepage</a>
              </div>
            </form>
          </logic:present>--%>
        </div>
      </div>
    </div>
  </div>
</section>

<jsp:include page="/jsp/common/wuFooter.jsp" />

<%
  String bulkProsIntProviders = (String)request.getAttribute("bulkProsIntProviders");
  String prosProviderId = "", prosProviderName = "", prosProvierQual = "",cDimQualificationType="",cDimLevel="";
  String prosUserId = (String)request.getAttribute("bulkProsUserId");
  if(bulkProsIntProviders!=null && !"".equals(bulkProsIntProviders)){
    String bulkProsProviderSplit[] =  bulkProsIntProviders.split("##");      
    for(int blk=0; blk<bulkProsProviderSplit.length; blk++)
    {
      String blkProsProvider[]  = bulkProsProviderSplit[blk].split("#");
      for(int pro=0; pro<blkProsProvider.length; pro++)
      {          
        prosProviderId = blkProsProvider[0];                    
        prosProviderName = new GlobalFunction().getReplacedString(blkProsProvider[1]);          
        prosProvierQual = blkProsProvider[2];
        if(prosProvierQual!=null && "M".equals(prosProvierQual)){
          cDimQualificationType = "undergraduate qualification";
          cDimLevel             = "Undergraduate";
        }else{
          cDimQualificationType = "postgraduate qualification";
          cDimLevel             = "Postgraduate";
        }
      %>
      <script type="text/javascript">
        searchEventTracking("interaction","Prospectus","<%=prosProviderName%>");
        bklProsInsightLogging("<%=prosProviderId%>","<%=cDimQualificationType%>","<%=cDimLevel%>","<%=prosUserId%>");
      </script>  
      <%break;
      }
     }
    }
    String newUserFlag = (String)request.getAttribute("newUserFlag");
    String typeOfEnquiry = (String)request.getAttribute("typeOfEnquiry");
    String GA_newUserRegister = (String)request.getAttribute("GA_newUserRegister");    
    if(!GenericValidator.isBlankOrNull(newUserFlag) && "Y".equals(newUserFlag)){
    %>
  <script type="text/javascript">
    GARegistrationLogging('register', '<%=typeOfEnquiry%>', 'Bulk-prospectus', '<%=GA_newUserRegister%>');
  </script>
  <%}%>
</body>
