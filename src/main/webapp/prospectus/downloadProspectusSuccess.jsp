<%Long startJspBuild = new Long(System.currentTimeMillis());%>

<%@page import="WUI.utilities.SessionData, java.util.*, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants, WUI.utilities.CommonUtil, org.apache.commons.validator.GenericValidator" autoFlush="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO" %>

     <%
       String collegeId   =  (String) request.getAttribute("collegeId");
       String GA_newUserRegister   =  (String) request.getAttribute("GA_newUserRegister");//5_AUG_2014
       String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");    
       if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0){ 
        cpeQualificationNetworkId = "2";
       }request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);
       String tmp_collegeName = request.getAttribute("collegeName") != null && request.getAttribute("collegeName").toString().trim().length() > 0 ? (String)request.getAttribute("collegeName") : "";    
       String gaCollegeName = new  CommonFunction().replaceURL(tmp_collegeName);       
       request.setAttribute("hitbox_eventtag", GlobalConstants.REQUEST_PROSPECTUS_EVENT);
       request.setAttribute("hitbox_college_id", collegeId);
       request.setAttribute("hitbox_college_name", tmp_collegeName);    
       String newUserFlag = (String)request.getAttribute("newUserFlag");
      String redirecturl="";
      if(request.getAttribute("downloadRequetedUrl")!=null){      
          redirecturl=(String)request.getAttribute("downloadRequetedUrl");
        if(redirecturl!=null &&redirecturl.length()>0 ){
          if(redirecturl.startsWith("http")){ 
          }else{
          redirecturl=new CommonFunction().getSchemeName(request)+redirecturl;
          }
        }
      } 
     boolean dircTrackExist = false;
     String postenquirysuggestionJsName = CommonUtil.getResourceMessage("wuni.post.enquiry.suggestion.js", null);
    %>  

  <body>
    <c:if test="${not empty requestScope.pixelTracking}">${requestScope.pixelTracking}</c:if> <!--18_NOV_2014 Added by Amir for pixel tracking in email success page-->
    <% String urlString  = request.getQueryString()!=null ? request.getQueryString() : "";%>
    <header class="clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">    
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>
        </div>  
      </div>      
      <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/<%=postenquirysuggestionJsName%>"></script>
    </header>     
     <section class="main_cnr pt40 pb40">
      <div class="ad_cnr">
        <div id="sub_container" class="content-bg">
          <div class="sub_cnr abt">
            <div class="fr rht_sec main_success">
              <div class="success p20">
                <h6 class="fnt_lbd pb5">BOOM</h6>
                <p class="txt fnt_lrg m0 p0">
                  <i class="icon-ok mr5"></i>
                  You have successfully downloaded the <a href="<SEO:SEOURL pageTitle="unilanding" >${requestScope.collegeNameDisplay}<spring:message code="wuni.seo.url.split.character" /><%=collegeId%></SEO:SEOURL>" title='${requestScope.collegeNameDisplay}'>${requestScope.collegeNameDisplay}</a> prospectus.
                </p>            
             </div> 
             <div class="pros_scroll">
                <c:if test="${not empty requestScope.prospectusSentCollegeNames}">
                  <ul class="uni_menu">
                   <c:forEach var="prospectusSentCollegeNames" items="${requestScope.prospectusSentCollegeNames}">
                    <li><a>
                      <span class="fl uni fnt_lrg">${prospectusSentCollegeNames.collegeDisplayName}</span>
                      <i class="fa icon-ok fa-1_5x fr"></i>
                      </a>
                    </li>
                    </c:forEach>
                  </ul> 
                </c:if>
              </div> 
              <div class="success p20">            
                <p class="txt fnt_lrg m0 p0">
                  <i class="icon-ok mr5"></i>
                  To help you organise your uni search, your enquiry has been saved to your <a href="/degrees/mywhatuni.html">MyWhatUni</a> profile (and if you don't know what MyWhatuni is by now, then frankly, you should be ashamed - visit <a href="/degrees/mywhatuni.html">MyWhatuni</a> IMMEDIATELY and fill out all your information, then we might just forgive you). Remember - the more info you fill in, the better we can personalise Whatuni for you (we're nice like that).
                </p>
             </div>         
             <jsp:include page="/jsp/advertiser/prospectus/prospectusPostEnquiry.jsp"/>         
            </div>
          </div>
        </div>
      </div>
    </section>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
    <script type="text/javascript">
      <%if(dircTrackExist){%>    
        setTimeout("prospectus('<%=redirecturl%>')",2000);
      <%}else{%>
        prospectus('<%=redirecturl%>');
      <%}%>
    </script>
    <%if(!GenericValidator.isBlankOrNull(newUserFlag) && "Y".equals(newUserFlag)){%>
    <script type="text/javascript"> 
       GARegistrationLogging('register' ,'Prospectus-dl', '<%=gaCollegeName%>', '<%=GA_newUserRegister%>');
    </script>
    <%}%>
  </body>