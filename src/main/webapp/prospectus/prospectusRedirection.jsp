<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page import="java.util.*,javax.swing.*" %>
<%@ page import="WUI.utilities.SessionData" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="html"%>
   <body id="www-whatuni-com">
      <%
        String redirectionURL ="";
        redirectionURL   =   session.getAttribute("prospectus_redirect_url") !=null ? String.valueOf(session.getAttribute("prospectus_redirect_url")) :  "/home.html";
        redirectionURL   =   redirectionURL !=null && !redirectionURL.equalsIgnoreCase(".html") ? redirectionURL : "/home.html";
        if(session.getAttribute("prospectus_redirect_url") != null) { session.removeAttribute("prospectus_redirect_url"); }
      %>
      <html:form action="<%=redirectionURL%>">
         <script language="JavaScript" type="text/javascript">
            document.forms[0].submit();
         </script>
      </html:form>
    </body>



 