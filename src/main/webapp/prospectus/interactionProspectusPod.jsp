<%@ page import="java.util.*,WUI.utilities.CommonUtil" autoFlush="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:if test="${not empty requestScope.porspectusList}">
<div class="fr mb30">
  <ul class="uni_menu">
      <div class="order_menu cf">
        <div class="pros_count fl">
          <div class="cu_txt">
          <span class="fl count_value fnt_lbd" id="interaction_pros_count">
            <c:if test="${not empty requestScope.prospectusBasketCount}">
                ${requestScope.prospectusBasketCount}
            </c:if>
          </span>
          <span class="ics_order fr"></span>            
        </div>
        <span class="txt fnt_lbd">Prospectuses</span>
        </div>
      </div>
      <c:forEach var="porspectusList" items="${requestScope.porspectusList}" begin="0" end="20" varStatus="index"> 
        <c:if test="${not empty porspectusList.status}">
          <c:if test="${porspectusList.status eq 'D'}">
            <li id="divpopup_${porspectusList.subOrderItemId}" class="cancel">
              <a onclick="javascript:addRemoveFromInteraction('${porspectusList.collegeId}','C','${porspectusList.subOrderItemId}','RP')">                  
                <span class="fl uni fnt_lrg">${porspectusList.collegeNameDisplay}</span>
                <i id="addpop_${porspectusList.subOrderItemId}" class="fa fa-plus fa-1_5x fr"></i>                                            
               </a>
            </li>
          </c:if> 
           <c:if test="${porspectusList.status eq 'A'}">
             <li id="divpopup_${porspectusList.subOrderItemId}" class="">
              <a onclick="javascript:addRemoveFromInteraction('${porspectusList.collegeId}','C','${porspectusList.subOrderItemId}','RP')">   
                <span class="fl uni fnt_lrg">${porspectusList.collegeNameDisplay}</span>
                <i id="addpop_${porspectusList.subOrderItemId}" class="fa fa-remove fa-1_5x fr"></i>
              </a>
              </li>
           </c:if>
         </c:if>
      </c:forEach>
       </ul>
       <c:if test="${not empty requestScope.porspectusListSize}">
         <c:if test="${requestScope.porspectusListSize gt 20}">    
           <a class="fr mt10 mb20 fnt_lrg blue" id="viewAllProsepectusOrdered" onclick="javascript:displayViewMoreBlock();" >VIEW ALL</a>
         </c:if>
       </c:if>
       <ul class="uni_menu" id="viewMoreBlockProspectus" style="display:none;">
        <c:forEach var="porspectusList" items="${requestScope.porspectusList}" begin="0" end="20" varStatus="indexs">
          <c:if test="${not empty porspectusList.status}">
            <c:if test="${porspectusList.status eq 'D'}">         
              <li id="divpopup_${porspectusList.subOrderItemId}" class="cancel">
                <a onclick="javascript:addRemoveFromInteraction('${porspectusList.collegeId}','C','${porspectusList.subOrderItemId}','RP')">                  
                  <span class="fl uni fnt_lrg">${porspectusList.collegeNameDisplay}</span>
                  <i id="addpop_${porspectusList.subOrderItemId}" class="fa fa-plus fa-1_5x fr"></i>                                            
                 </a>
              </li>
            </c:if> 
             <c:if test="${porspectusList.status eq 'A'}">  
               <li id="divpopup_${porspectusList.subOrderItemId}" class="">
                <a onclick="javascript:addRemoveFromInteraction('${porspectusList.collegeId}','C','${porspectusList.subOrderItemId}','RP')">   
                  <span class="fl uni fnt_lrg">${porspectusList.collegeNameDisplay}</span>
                  <i id="addpop_${porspectusList.subOrderItemId}" class="fa fa-remove fa-1_5x fr"></i>
                </a>
                </li>
                </c:if>
           </c:if>
        </c:forEach>
       </ul>   
    </div>    
</c:if>                    