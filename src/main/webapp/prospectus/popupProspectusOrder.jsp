<%@ page import="java.util.*,WUI.utilities.CommonUtil" autoFlush="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${not empty requestScope.porspectusList}">
<div style="display:block;" class="selected_pros">
<span class="icon_basket"></span>
  <h5 class="fnt_lbd fnt_lrg">
       <c:if test="${not empty requestScope.prospectusBasketCount}">
         ${requestScope.prospectusBasketCount}
       </c:if>Prospectuses selected</h5>
      <div class="pros_scroll">
          <ul class="uni_menu">
             <c:forEach var="porspectusList" items="${requestScope.porspectusList}"> 
                  <c:if test="${porspectusList.status eq 'D'}">
                    <li id="divpopup_${porspectusList.subOrderItemId}" class="cancel">
                      <a onclick="javascript:addRemoveFromPopup('${porspectusList.collegeId}','C','${porspectusList.subOrderItemId}','RP')">                  
                        <span class="fl uni fnt_lrg">${porspectusList.collegeNameDisplay}</span>
                        <i id="addpop_${porspectusList.subOrderItemId}" class="fa fa-plus fa-1_5x fr"></i>                                            
                       </a>
                    </li>
                  </c:if>
                   <c:if test="${porspectusList.status eq 'A'}">
                     <li id="divpopup_${porspectusList.subOrderItemId}" class="">
                      <a onclick="javascript:addRemoveFromPopup('${porspectusList.collegeId}','C','${porspectusList.subOrderItemId}','RP')">   
                        <span class="fl uni fnt_lrg">${porspectusList.collegeNameDisplay}</span>
                        <i id="addpop_${porspectusList.subOrderItemId}" class="fa fa-remove fa-1_5x fr"></i>
                      </a>
                     </li>
                 </c:if>
              </c:forEach>
           </ul>
      </div>
   </div>
   
</c:if>


                     