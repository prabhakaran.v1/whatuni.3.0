<%@ page import="WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

 
  
<body id="www-whatuni-com">
    <% String urlString  = request.getQueryString()!=null ? request.getQueryString() : "";%>
    <div class="clipart">
    <div id="content-bg">
    <div id="wrapper">
        <jsp:include page="/jsp/common/wuHeader.jsp" />      
           <%-- <div id="leftcolumn">
                <jsp:include page="/include/loggedPod.jsp" >
                    <jsp:param name="urlname" value="home" />
                    <jsp:param name="querystringname" value="<%=urlString%>" />
                    <jsp:param name="OVERWRITESESSION" value="NO" /> 
                </jsp:include>
                
            </div> --%>
            <div id="content-blk">
                <div id="content-left">
                    <div id="prospectus" class="sendemailuni">
                        <h2><span><p class="heightfix">Exceeded prospectus limit</p></span></h2>
                        <div class="clear-1">
                            <c:if test="${not empty requestScope.userprospectuscount}">
                                <c:if test="${requestScope.userprospectuscount eq 'MAX'}">
                                    <p>Sorry, you have made more than <c:if test="${not empty requestScope.maxprospectus}">${requestScope.maxprospectus}</c:if> requests for prospectuses today.  While Whatuni and the unis process these requests you will be unable to make any more.</p>  
                                    <p>Keep an eye out for your prospectuses in the post and once they have come please feel free to come back and order some more.</p>
                                </c:if>    
                                <c:if test="${requestScope.userprospectuscount lt 200}">
                                    <p>In order to supply universities with a manageable number of prospectus requests, we have limited the number of prospectuses it is possible to order to <c:if test="${not empty requestScope.maxprospectus}">${requestScope.maxprospectus}</c:if>.</p>
                                    <p>You have already ordered ${requestScope.maxprospectus} prospectuses today and your last request contained ${requestScope.currentProspectusCount}. Please click OK to go back and reduce the number of prospectuses you have ordered to <c:if test="${not empty requestScope.remainingprospectus}">${requestScope.remainingprospectus}</c:if>.</p>
                                </c:if>     
                            </c:if>        
                            <p><input type="button" class="button" name="submit" value="OK" onclick="javascript:history.back()"  /></p>
                    </div>
                </div>
            </div>
            <div id="content-right">
            <jsp:include page="/include/latestMembers.jsp" />
            </div> 
            </div>
         </div>
    </div>
    </div>
    <jsp:include page="/jsp/common/wuFooter.jsp" />
</body>
</html>
