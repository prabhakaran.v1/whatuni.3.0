<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="java.util.*,WUI.utilities.CommonUtil" autoFlush="true" %>
<%
  int sliderCount = Integer.parseInt(request.getParameter("count"));
  String  prospectusURL = request.getParameter("prospectusURL");
  String  prosepectusSortType = request.getParameter("prosepectusSortType");
%>

<c:if test="${not empty requestScope.studentAwardsList}">
  <%sliderCount++; String selectAllInst="";%>
   <section class="ltst_art">
    <article class="content-bg slct_all">
      <div class="flexslider2">
        <h3 class="fnt_lrg">
           <c:forEach var="studentAwardsList" items="${requestScope.studentAwardsList}" varStatus="indexg" end="0">
           <span class="Ptit_cnr"> 
            <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">Whatuni Student Choice Awards</a>
            <a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">Whatuni Student Choice Awards</a>
            <span class="fnt_lrg pl10 Puni">(${studentAwardsList.totalCount}
                 <c:if test="${studentAwardsList.totalCount gt 1}">universities</c:if>
                 <c:if test="${studentAwardsList.totalCount eq 1}">university</c:if>)</span>
           </span>
           </c:forEach>
            <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
            <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
          </h3> 
          <div id="mslider_<%=sliderCount%>" class="pros" style="display:none;overflow:hidden;"></div> 
        <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow: hidden;">                                                     
          <div class="slider_cnr"  id="menu_slider_<%=sliderCount%>">
           <div class="slide_menu">
            <div class="mt15 ml20">
              <ul class="slides"> 
                <li>
                
                 <c:forEach var="studentAwardsList" items="${requestScope.studentAwardsList}" varStatus="studentAwardIndex">
                        <c:set var="studentAwardIndex" value="${studentAwardIndex.index}"/> 
                        <c:if test="${not empty studentAwardsList.subOrderItemId}">
                          <c:if test="${not empty studentAwardsList.collegeId}">
                             <c:set var="studentsubOrderItemId" value="${studentAwardsList.subOrderItemId}"/>
                             <c:set var="studentcollegeId" value="${studentAwardsList.collegeId}"/>
                                 <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("studentsubOrderItemId")+"SP"+(String)pageContext.getAttribute("studentcollegeId")+"GP";%>
                            </c:if>
                         </c:if>
                      <%int studentAwardIndex = Integer.parseInt((pageContext.getAttribute("studentAwardIndex")).toString());
                      if(studentAwardIndex%4==0 && studentAwardIndex!=0){ %>
                         </li><li>
                       <%}%> 
                       <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                       <c:if test="${studentAwardsList.collegeInBasket eq 'TRUE'}">
                          <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                        </c:if>
                        <div class="<%=columnClassName%>" id="div_slider_<%=sliderCount%>_${studentAwardsList.subOrderItemId}" onclick="javascript:addProspectusBasket('${studentAwardsList.collegeId}','C','${studentAwardsList.subOrderItemId}','RP','<%=sliderCount%>','whatuni student choice awards','${studentAwardsList.gaCollegeName}')">
                          <a class="fr mr20" onclick="javascript:addProspectusBasket('${studentAwardsList.collegeId}','C','${studentAwardsList.subOrderItemId}','RP','<%=sliderCount%>','whatuni student choice awards','${studentAwardsList.gaCollegeName}')">                                            
                           <i id="add_slider_<%=sliderCount%>_${studentAwardsList.subOrderItemId}" class="<%=plusClassName%>"></i>
                          </a>
                          <c:if test="${not empty studentAwardsList.collegeLogoPath}">
                            <div class="img_ppn">
                             <%if(sliderCount>1){ %>
                               <img id="imgslider_<%=sliderCount%>_<%=studentAwardIndex%>" alt="${studentAwardsList.collegeDisplayName}" title="${studentAwardsList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${studentAwardsList.collegeLogoPath}">
                            <%}else{%>
                              <img id="imgslider_<%=sliderCount%>_<%=studentAwardIndex%>" alt="${studentAwardsList.collegeDisplayName}" title="${studentAwardsList.collegeDisplayName}" src="${studentAwardsList.collegeLogoPath}">
                            <%}%>                                              
                            </div>
                          </c:if>
                          <div class="ftc">
                            <h6 class="fnt_lbd">${studentAwardsList.collegeDisplayName}</h6>
                            <c:if test="${not empty studentAwardsList.overallRating}">
                              <fmt:parseNumber var = "overallRating" type = "number" value = "${studentAwardsList.overallRating}" />
                             <c:if test="${overallRating gt 0}"> 
                              <div class="art_cnt">STUDENT RATING
                                <div class="rev_return">
                                  <span class="rat${studentAwardsList.overAllRatingRounded} t_tip">
																	                   <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                            <span class="cmp">
								                              <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                             <div class="line"></div>
								                            </span>
								                           <%--End of rating tool tip code--%>
																	                 </span>
                                  <span class="rat_value fl">(${studentAwardsList.overallRating})</span>
                                </div>
                              </div>
                             </c:if>
                            </c:if>
                             <c:if test="${not empty studentAwardsList.timesRanking}">
                              <div class="art_cnt">The CompUniGuide ranking:  ${studentAwardsList.timesRanking}</div>
                            </c:if>                                              
                          </div>
                      </div>
                  </c:forEach>
                </li>
              </ul>
            </div>
          </div>
         </div> 
        </div>
        <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','whatuni student choice awards')" class="select_all fr fnt_lrg">SELECT ALL</a>
        <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','whatuni student choice awards')" class="select_all fr fnt_lrg">SELECT ALL</a>
      </div>       
    </article>
  </section>
 </c:if>
 <c:if test="${not empty requestScope.popularRegionList}">
    <%sliderCount++; String selectAllInst=""; String totalCount="";%>
     <section class="ltst_art">
      <article class="content-bg slct_all">
        <div class="flexslider2">
          <h3 class="fnt_lrg">
             <c:forEach var="popularRegionList" items="${requestScope.popularRegionList}" varStatus="indexh" end="0">
              <span class="Ptit_cnr">
              <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">${popularRegionList.displayName}</a>
              <a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">${popularRegionList.displayName}</a>
              <span class="fnt_lrg pl10 Puni">(
                ${popularRegionList.totalCount}
                <c:if test="${popularRegionList.totalCount gt 1}">universities</c:if>
                <c:if test="${popularRegionList.totalCount eq 1}">university</c:if>
              )</span>
              </span>
             </c:forEach>
              <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
              <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
            </h3> 
            <div id="mslider_<%=sliderCount%>" class="pros" style="display:none;overflow:hidden;"></div> 
          <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow: hidden;">
                                     
            <div class="slider_cnr" id="menu_slider_<%=sliderCount%>"><div class="slide_menu">
              <div class="mt15 ml20">
                <ul class="slides"> 
                  <li>
                  
                  <c:forEach var="popularRegionList" items="${requestScope.popularRegionList}" varStatus="popularRegionProsIndex">
                        <c:set var="popularRegionProsIndex" value="${popularRegionProsIndex.index}"/> 
                        <c:if test="${not empty popularRegionList.subOrderItemId}">
                          <c:if test="${not empty popularRegionList.collegeId}">
                             <c:set var="popularsubOrderItemId" value="${popularRegionList.subOrderItemId}"/>
                             <c:set var="popularcollegeId" value="${popularRegionList.collegeId}"/>
                                 <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("popularsubOrderItemId") +"SP"+ (String)pageContext.getAttribute("popularcollegeId") +"GP";%>
                            </c:if>
                         </c:if>
                        <%int popularRegionProsIndex = Integer.parseInt((pageContext.getAttribute("popularRegionProsIndex")).toString());
                        if(popularRegionProsIndex%4==0 && popularRegionProsIndex!=0){ %>
                           </li><li>
                         <%}%> 
                         <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                              <c:if test="${popularRegionList.collegeInBasket eq 'TRUE'}">
                                   <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                              </c:if>
                          <div class="<%=columnClassName%>" id="div_slider_<%=sliderCount%>_${popularRegionList.subOrderItemId}" onclick="javascript:addProspectusBasket('${popularRegionList.collegeId}','C','${popularRegionList.subOrderItemId}','RP','<%=sliderCount%>','popular regions','${popularRegionList.gaCollegeName}')">
                            <a class="fr mr20" onclick="javascript:addProspectusBasket('${popularRegionList.collegeId}','C','${popularRegionList.subOrderItemId}','RP','<%=sliderCount%>','popular regions','${popularRegionList.gaCollegeName}')">                                            
                              <i id="add_slider_<%=sliderCount%>_${popularRegionList.subOrderItemId}" class="<%=plusClassName%>"></i>
                            </a> 
                            <c:if test="${not empty popularRegionList.collegeLogoPath}">
                              <div class="img_ppn">
                                <%if(sliderCount>1){ %>
                                   <img id="imgslider_<%=sliderCount%>_<%=popularRegionProsIndex%>" alt="${popularRegionList.collegeDisplayName}" title="${popularRegionList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${popularRegionList.collegeLogoPath}">
                                <%}else{%>
                                  <img id="imgslider_<%=sliderCount%>_<%=popularRegionProsIndex%>" alt="${popularRegionList.collegeDisplayName}" title="${popularRegionList.collegeDisplayName}" src="${popularRegionList.collegeLogoPath}">
                                <%}%>
                              </div>
                            </c:if>
                            <div class="ftc">
                              <h6 class="fnt_lbd">${popularRegionList.collegeDisplayName}</h6>
                             <c:if test="${not empty popularRegionList.overallRating}">
                             <fmt:parseNumber var = "overallRating" type = "number" value = "${popularRegionList.overallRating}" />
                             <c:if test="${overallRating gt 0}"> 
                                <div class="art_cnt">STUDENT RATING
                                  <div class="rev_return">
                                    <span class="rat${popularRegionList.overAllRatingRounded} t_tip">
																																		    <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                              <span class="cmp">
								                                <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                               <div class="line"></div>
								                              </span>
								                              <%--End of rating tool tip code--%>
																		                  </span>
                                    <span class="rat_value fl">(${popularRegionList.overallRating})</span>
                                  </div>
                                </div>
                               </c:if>
                              </c:if>
                               <c:if test="${not empty popularRegionList.timesRanking}">
                                <div class="art_cnt">The CompUniGuide ranking:  ${popularRegionList.timesRanking}</div>
                              </c:if>                                             
                            </div>
                        </div>
                    </c:forEach>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
          <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','popular regions')" class="select_all fr fnt_lrg">SELECT ALL</a>       
          <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','popular regions')" class="select_all fr fnt_lrg">SELECT ALL</a>       
       </div>       
      </article>
    </section>
</c:if>
 <c:if test="${not empty requestScope.campusUniProsList}">
    <%sliderCount++; String selectAllInst=""; String totalCount=""; %>
     <section class="ltst_art">
      <article class="content-bg slct_all">
        <div class="flexslider2">
         <h3 class="fnt_lrg">
           <c:forEach var="campusUniProsList" items="${requestScope.campusUniProsList}" varStatus="indexi" end="0">
            <span class="Ptit_cnr">  
              <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">Campus universities</a>
              <a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">Campus universities</a>
              <span class="fnt_lrg pl10 Puni">(${campusUniProsList.totalCount}
                <c:if test="${campusUniProsList.totalCount gt 1}">universities</c:if>
                <c:if test="${campusUniProsList.totalCount eq 1}">university</c:if>)</span>
            </span>
            </c:forEach>
              <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
              <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
            </h3> 
            <div id="mslider_<%=sliderCount%>" class="pros" style="display:none;overflow:hidden;"></div> 
          <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow: hidden;">
                                       
            <div class="slider_cnr" id="menu_slider_<%=sliderCount%>"><div class="slide_menu">
              <div class="mt15 ml20">
                <ul class="slides"> 
                  <li>
                  
                   <c:forEach var="campusUniProsList" items="${requestScope.campusUniProsList}" varStatus="campusUnisProsIndex">
                        <c:set var="campusUnisProsIndex" value="${campusUnisProsIndex.index}"/> 
                        <c:if test="${not empty campusUniProsList.subOrderItemId}">
                          <c:if test="${not empty campusUniProsList.collegeId}">
                             <c:set var="campussubOrderItemId" value="${campusUniProsList.subOrderItemId}"/>
                             <c:set var="campuscollegeId" value="${campusUniProsList.collegeId}"/>
                                 <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("campussubOrderItemId")+"SP"+(String)pageContext.getAttribute("campuscollegeId")+"GP";%>
                            </c:if>
                         </c:if>
                        <%int campusUnisProsIndex = Integer.parseInt((pageContext.getAttribute("campusUnisProsIndex")).toString());
                        if(campusUnisProsIndex%4==0 && campusUnisProsIndex!=0){ %>
                           </li><li>
                         <%}%> 
                         <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                         <c:if test="${campusUniProsList.collegeInBasket eq 'TRUE'}">
                          <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                         </c:if>
                          <div class="<%=columnClassName%>" onclick="javascript:addProspectusBasket('${campusUniProsList.collegeId}','C','${campusUniProsList.subOrderItemId}','RP','<%=sliderCount%>','campus universities','${campusUniProsList.gaCollegeName}')" id="div_slider_<%=sliderCount%>_${campusUniProsList.subOrderItemId}">
                            <a class="fr mr20" onclick="javascript:addProspectusBasket('${campusUniProsList.collegeId}','C','${campusUniProsList.subOrderItemId}','RP','<%=sliderCount%>','campus universities','${campusUniProsList.gaCollegeName}')">                                            
                              <i id="add_slider_<%=sliderCount%>_${campusUniProsList.subOrderItemId}" class="<%=plusClassName%>"></i>
                            </a>
                            <c:if test="${not empty campusUniProsList.collegeLogoPath}">
                              <div class="img_ppn">
                                <%if(sliderCount>1){ %>
                                <img id="imgslider_<%=sliderCount%>_<%=campusUnisProsIndex%>" alt="${campusUniProsList.collegeDisplayName}" title="${campusUniProsList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${campusUniProsList.collegeLogoPath}">
                                <%}else{%>
                                <img id="imgslider_<%=sliderCount%>_<%=campusUnisProsIndex%>" alt="${campusUniProsList.collegeDisplayName}" title="${campusUniProsList.collegeDisplayName}" src="${campusUniProsList.collegeLogoPath}">
                                <%}%>
                              </div>
                            </c:if>
                            <div class="ftc">
                              <h6 class="fnt_lbd">${campusUniProsList.collegeDisplayName}</h6>
                             <c:if test="${not empty campusUniProsList.overallRating}">
                               <fmt:parseNumber var = "overallRating" type = "number" value = "${campusUniProsList.overallRating}" />
                               <c:if test="${overallRating gt 0}"> 
                                <div class="art_cnt">STUDENT RATING
                                  <div class="rev_return">
                                    <span class="rat${campusUniProsList.overAllRatingRounded} t_tip">
																		                    <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                              <span class="cmp">
								                                <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                               <div class="line"></div>
								                              </span>
								                              <%--End of rating tool tip code--%>
																		                  </span>
                                    <span class="rat_value fl">(${campusUniProsList.overallRating})</span>
                                  </div>
                                </div>
                                </c:if>
                              </c:if>
                               <c:if test="${not empty campusUniProsList.timesRanking}">
                                <div class="art_cnt">The CompUniGuide ranking:  ${campusUniProsList.timesRanking}</div>
                             </c:if>                                           
                            </div>
                        </div>
                    </c:forEach>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
         <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','campus universities')" class="select_all fr fnt_lrg">SELECT ALL</a>       
         <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','campus universities')" class="select_all fr fnt_lrg">SELECT ALL</a>       
       </div>       
      </article>
    </section>
  </c:if>
  <c:if test="${not empty requestScope.nonCampusUniProsList}">
   <%sliderCount++; String selectAllInst=""; String totalCount="";%>
   <section class="ltst_art">
    <article class="content-bg slct_all">
      <div class="flexslider2">
       <h3 class="fnt_lrg">
          <c:forEach var="nonCampusUniProsList" items="${requestScope.nonCampusUniProsList}" varStatus="indexj" end="0">
           <span class="Ptit_cnr">
            <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">Non campus universities</a>
            <a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">Non campus universities</a>
            <span class="fnt_lrg pl10 Puni">(${nonCampusUniProsList.totalCount} 
               <c:if test="${nonCampusUniProsList.totalCount gt 1}">universities</c:if>
                <c:if test="${nonCampusUniProsList.totalCount eq 1}">university</c:if>)</span>
           </span>
          </c:forEach>
            <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
            <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
          </h3>  
          <div id="mslider_<%=sliderCount%>" class="pros" style="display:none;overflow:hidden;"></div> 
        <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow: hidden;">                                    
          <div class="slider_cnr" id="menu_slider_<%=sliderCount%>"><div class="slide_menu">
            <div class="mt15 ml20">
              <ul class="slides"> 
                <li>
                   <c:forEach var="nonCampusUniProsList" items="${requestScope.nonCampusUniProsList}" varStatus="nonCmpsProsIndex">
                        <c:set var="nonCmpsProsIndex" value="${nonCmpsProsIndex.index}"/> 
                        <c:if test="${not empty nonCampusUniProsList.subOrderItemId}">
                          <c:if test="${not empty nonCampusUniProsList.collegeId}">
                             <c:set var="noncampussubOrderItemId" value="${nonCampusUniProsList.subOrderItemId}"/>
                             <c:set var="noncampuscollegeId" value="${nonCampusUniProsList.collegeId}"/>
                             <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("noncampussubOrderItemId")+"SP"+(String)pageContext.getAttribute("noncampuscollegeId")+"GP";%>
                        </c:if>
                      </c:if>
                      <%int nonCmpsProsIndex = Integer.parseInt((pageContext.getAttribute("nonCmpsProsIndex")).toString());
                      if(nonCmpsProsIndex%4==0 && nonCmpsProsIndex!=0){ %>
                         </li><li>
                       <%}%> 
                        <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                        <c:if test="${nonCampusUniProsList.collegeInBasket eq 'TRUE'}">
                          <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                        </c:if>
                        <div class="<%=columnClassName%>" onclick="javascript:addProspectusBasket('${nonCampusUniProsList.collegeId}','C','${nonCampusUniProsList.subOrderItemId}','RP','<%=sliderCount%>','non campus universities','${nonCampusUniProsList.gaCollegeName}')" id="div_slider_<%=sliderCount%>_${nonCampusUniProsList.subOrderItemId}">
                            <a class="fr mr20" onclick="javascript:addProspectusBasket('${nonCampusUniProsList.collegeId}','C','${nonCampusUniProsList.subOrderItemId}','RP','<%=sliderCount%>','non campus universities','${nonCampusUniProsList.gaCollegeName}')">                                            
                              <i id="add_slider_<%=sliderCount%>_${nonCampusUniProsList.subOrderItemId}" class="<%=plusClassName%>"></i>
                            </a>
                          <c:if test="${not empty nonCampusUniProsList.collegeLogoPath}">
                            <div class="img_ppn">
                              <%if(sliderCount>1){ %>
                               <img id="imgslider_<%=sliderCount%>_<%=nonCmpsProsIndex%>" alt="${nonCampusUniProsList.collegeDisplayName}" title="${nonCampusUniProsList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${nonCampusUniProsList.collegeLogoPath}">
                              <%}else{%>
                               <img id="imgslider_<%=sliderCount%>_<%=nonCmpsProsIndex%>" alt="${nonCampusUniProsList.collegeDisplayName}" title="${nonCampusUniProsList.collegeDisplayName}" src="${nonCampusUniProsList.collegeLogoPath}">
                              <%}%>                              
                            </div>
                          </c:if>
                          <div class="ftc">
                            <h6 class="fnt_lbd">${nonCampusUniProsList.collegeDisplayName}</h6>
                             <c:if test="${not empty nonCampusUniProsList.overallRating}">
                               <fmt:parseNumber var = "overallRating" type = "number" value = "${nonCampusUniProsList.overallRating}" />
                               <c:if test="${overallRating gt 0}"> 
                                <div class="art_cnt">STUDENT RATING
                                <div class="rev_return">
                                  <span class="rat${nonCampusUniProsList.overAllRatingRounded} t_tip">
																	                   <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                            <span class="cmp">
								                              <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                             <div class="line"></div>
								                            </span>
								                            <%--End of rating tool tip code--%>
																	                 </span>
                                  <span class="rat_value fl">(${nonCampusUniProsList.overallRating})</span>
                                </div>
                              </div>
                              </c:if>
                            </c:if>
                             <c:if test="${not empty nonCampusUniProsList.timesRanking}">
                              <div class="art_cnt">The CompUniGuide ranking:  ${nonCampusUniProsList.timesRanking}</div>
                           </c:if>                                            
                          </div>
                      </div>
                  </c:forEach>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
       <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','non campus universities')" class="select_all fr fnt_lrg">SELECT ALL</a>       
       <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','non campus universities')" class="select_all fr fnt_lrg">SELECT ALL</a>       
     </div>     
    </article>
  </section>
</c:if>
<c:if test="${not empty requestScope.cityProsList}">
  <%sliderCount++; String selectAllInst=""; String totalCount="";%>
   <section class="ltst_art">
    <article class="content-bg slct_all">
      <div class="flexslider2">
       <h3 class="fnt_lrg">
           <c:forEach var="cityProsList" items="${requestScope.cityProsList}" varStatus="indexk" end="0">
           <span class="Ptit_cnr"> 
            <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">City</a>
            <a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">City</a>
            <span class="fnt_lrg pl10 Puni">(${cityProsList.totalCount} 
               <c:if test="${cityProsList.totalCount gt 1}">universities</c:if>
                <c:if test="${cityProsList.totalCount eq 1}">university</c:if>)</span>
            </span>
           </c:forEach>
            <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
            <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
          </h3> 
          <div id="mslider_<%=sliderCount%>" class="pros" style="display:none;overflow:hidden;"></div> 
        <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow: hidden;">                                    
          <div class="slider_cnr" id="menu_slider_<%=sliderCount%>"><div class="slide_menu">
            <div class="mt15 ml20">
              <ul class="slides"> 
                <li>
                 <c:forEach var="cityProsList" items="${requestScope.cityProsList}" varStatus="cityProsIndex">
                   <c:set var="cityProsIndex" value="${cityProsIndex.index}"/> 
                     <c:if test="${not empty cityProsList.subOrderItemId}">
                       <c:if test="${not empty cityProsList.collegeId}">
                          <c:set var="citysubOrderItemId" value="${cityProsList.subOrderItemId}"/>
                          <c:set var="citycollegeId" value="${cityProsList.collegeId}"/>
                             <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("citysubOrderItemId")+"SP"+(String)pageContext.getAttribute("citycollegeId")+"GP";%>
                        </c:if>
                      </c:if>
                      <%int cityProsIndex = Integer.parseInt((pageContext.getAttribute("cityProsIndex")).toString());
                      if(cityProsIndex%4==0 && cityProsIndex!=0){ %>
                         </li><li>
                       <%}%> 
                        <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                        <c:if test="${cityProsList.collegeInBasket eq 'TRUE'}">
                          <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                        </c:if>
                        
                        <div class="<%=columnClassName%>" onclick="javascript:addProspectusBasket('${cityProsList.collegeId}','C','${cityProsList.subOrderItemId}','RP','<%=sliderCount%>','city','${cityProsList.gaCollegeName}')" id="div_slider_<%=sliderCount%>_${cityProsList.subOrderItemId}">
                            <a class="fr mr20" onclick="javascript:addProspectusBasket('${cityProsList.collegeId}','C','${cityProsList.subOrderItemId}','RP','<%=sliderCount%>','city','${cityProsList.gaCollegeName}')">                                            
                              <i id="add_slider_<%=sliderCount%>_${cityProsList.subOrderItemId}" class="<%=plusClassName%>"></i>
                            </a>
                            <c:if test="${not empty cityProsList.collegeLogoPath}">
                            <div class="img_ppn">
                              <%if(sliderCount>1){ %>
                                <img id="imgslider_<%=sliderCount%>_<%=cityProsIndex%>" alt="${cityProsList.collegeDisplayName}" title="${cityProsList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${cityProsList.collegeLogoPath}">
                              <%}else{%>
                                <img id="imgslider_<%=sliderCount%>_<%=cityProsIndex%>" alt="${cityProsList.collegeDisplayName}" title="${cityProsList.collegeDisplayName}" src="${cityProsList.collegeLogoPath}">
                              <%}%>
                              
                            </div>
                          </c:if>
                          <div class="ftc">
                            <h6 class="fnt_lbd">${cityProsList.collegeDisplayName}</h6>
                          <c:if test="${not empty cityProsList.overallRating}">
                             <fmt:parseNumber var = "overallRating" type = "number" value = "${cityProsList.overallRating}" />
                            <c:if test="${overallRating gt 0}"> 
                                <div class="art_cnt">STUDENT RATING
                                <div class="rev_return">
                                  <span class="rat${cityProsList.overAllRatingRounded} t_tip">
																	                   <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                            <span class="cmp">
								                              <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                             <div class="line"></div>
								                            </span>
								                            <%--End of rating tool tip code--%>
																	                 </span>
                                  <span class="rat_value fl">(${cityProsList.overallRating})</span>
                                </div>
                              </div>
                             </c:if>
                            </c:if>
                            <c:if test="${not empty cityProsList.timesRanking}">
                              <div class="art_cnt">The CompUniGuide ranking:  ${cityProsList.timesRanking}</div>
                            </c:if>                                              
                          </div>
                      </div>
                  </c:forEach>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
        <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','city')" class="select_all fr fnt_lrg">SELECT ALL</a>       
        <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','city')" class="select_all fr fnt_lrg">SELECT ALL</a>       
     </div>       
    </article>
  </section>
</c:if>
<c:if test="${not empty requestScope.countrySideProsList}">
  <%sliderCount++; String selectAllInst=""; String totalCount="";%>
   <section class="ltst_art">
    <article class="content-bg slct_all">
      <div class="flexslider2">
        <h3 class="fnt_lrg">
         <c:forEach var="countrySideProsList" items="${requestScope.countrySideProsList}" varStatus="indexg" end="0">
           <span class="Ptit_cnr"> 
            <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">Countryside</a>
            <a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">Countryside</a>
            <span class="fnt_lrg pl10 Puni">(${countrySideProsList.totalCount} 
               <c:if test="${countrySideProsList.totalCount gt 1}">universities</c:if>
                <c:if test="${countrySideProsList.totalCount eq 1}">university</c:if>)</span>
           </span>
          </c:forEach>
            <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
            <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
          </h3>   
          <div id="mslider_<%=sliderCount%>" class="pros" style="display:none;overflow:hidden;"></div> 
        <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow: hidden;">                                  
          <div class="slider_cnr" id="menu_slider_<%=sliderCount%>"><div class="slide_menu">
            <div class="mt15 ml20">
              <ul class="slides"> 
                <li>
                
                  <c:forEach var="countrySideProsList" items="${requestScope.countrySideProsList}" varStatus="countrySideProsIndex">
                        <c:set var="countrySideProsIndex" value="${countrySideProsIndex.index}"/> 
                        <c:if test="${not empty countrySideProsList.subOrderItemId}">
                          <c:if test="${not empty countrySideProsList.collegeId}">
                             <c:set var="countrysubOrderItemId" value="${countrySideProsList.subOrderItemId}"/>
                             <c:set var="countrycollegeId" value="${countrySideProsList.collegeId}"/>
                             <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("countrysubOrderItemId")+"SP"+(String)pageContext.getAttribute("countrycollegeId")+"GP";%>
                        </c:if>
                      </c:if>
                      <%int countrySideProsIndex = Integer.parseInt((pageContext.getAttribute("countrySideProsIndex")).toString());
                      if(countrySideProsIndex%4==0 && countrySideProsIndex!=0){ %>
                         </li><li>
                       <%}%>
                        <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                        <c:if test="${countrySideProsList.collegeInBasket eq 'TRUE'}">
                          <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                        </c:if>
                       <div class="<%=columnClassName%>" onclick="javascript:addProspectusBasket('${countrySideProsList.collegeId}','C','${countrySideProsList.subOrderItemId}','RP','<%=sliderCount%>','countryside','${countrySideProsList.gaCollegeName}')" id="div_slider_<%=sliderCount%>_${countrySideProsList.subOrderItemId}" >
                          <a class="fr mr20" onclick="javascript:addProspectusBasket('${countrySideProsList.collegeId}','C','${countrySideProsList.subOrderItemId}','RP','<%=sliderCount%>','countryside','${countrySideProsList.gaCollegeName}')">                                            
                            <i id="add_slider_<%=sliderCount%>_${countrySideProsList.subOrderItemId}" class="<%=plusClassName%>"></i>
                          </a>
                          <c:if test="${not empty countrySideProsList.collegeLogoPath}">
                             <div class="img_ppn">
                              <%if(sliderCount>1){ %>
                                <img id="imgslider_<%=sliderCount%>_<%=countrySideProsIndex%>" alt="${countrySideProsList.collegeDisplayName}" title="${countrySideProsList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${countrySideProsList.collegeLogoPath}">
                              <%}else{%>
                                <img id="imgslider_<%=sliderCount%>_<%=countrySideProsIndex%>" alt="${countrySideProsList.collegeDisplayName}" title="${countrySideProsList.collegeDisplayName}" src="${countrySideProsList.collegeLogoPath}">
                              <%}%>
                            </div>
                          </c:if>
                          <div class="ftc">
                            <h6 class="fnt_lbd">${countrySideProsList.collegeDisplayName}</h6>
                             <c:if test="${not empty countrySideProsList.overallRating}">
                               <fmt:parseNumber var = "overallRating" type = "number" value = "${countrySideProsList.overallRating}" />
                               <c:if test="${overallRating gt 0}"> 
                               <div class="art_cnt">STUDENT RATING
                                <div class="rev_return">
                                  <span class="rat${countrySideProsList.overAllRatingRounded} t_tip">
																	                   <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                            <span class="cmp">
								                              <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                             <div class="line"></div>
								                            </span>
								                            <%--End of rating tool tip code--%>
																	                 </span>
                                  <span class="rat_value fl">(${countrySideProsList.overallRating})</span>
                                </div>
                              </div>
                             </c:if>
                            </c:if>
                             <c:if test="${not empty countrySideProsList.timesRanking}">
                              <div class="art_cnt">The CompUniGuide ranking:  ${countrySideProsList.timesRanking}</div>
                            </c:if>                                              
                          </div>
                      </div>
                  </c:forEach>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
       <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','countryside')" class="select_all fr fnt_lrg">SELECT ALL</a>
       <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','countryside')" class="select_all fr fnt_lrg">SELECT ALL</a>
     </div>          
    </article>
  </section>
</c:if>
<c:if test="${not empty requestScope.seaSideProsList}">
<%sliderCount++; String selectAllInst=""; String totalCount="";%>
   <section class="ltst_art">
    <article class="content-bg slct_all">
      <div class="flexslider2">
       <h3 class="fnt_lrg">
          <c:forEach var="seaSideProsList" items="${requestScope.seaSideProsList}" varStatus="indexg" end="0">
           <span class="Ptit_cnr"> 
            <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">Seaside</a>
            <a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">Seaside</a>
            <span class="fnt_lrg pl10 Puni">(${seaSideProsList.totalCount}
                 <c:if test="${seaSideProsList.totalCount gt 1}">universities</c:if>
                <c:if test="${seaSideProsList.totalCount eq 1}">university</c:if>)</span>
           </span>
          </c:forEach>
            <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
            <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
          </h3> 
          <div id="mslider_<%=sliderCount%>" class="pros" style="display:none;overflow:hidden;"></div> 
        <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow: hidden;">
                                     
          <div class="slider_cnr" id="menu_slider_<%=sliderCount%>">
            <div class="slide_menu">
            <div class="mt15 ml20">
              <ul class="slides"> 
                <li>
                   <c:forEach var="seaSideProsList" items="${requestScope.seaSideProsList}" varStatus="seasSideProsIndex">
                     <c:set var="seasSideProsIndex" value="${seasSideProsIndex.index}"/> 
                        <c:if test="${not empty seaSideProsList.subOrderItemId}">
                          <c:if test="${not empty seaSideProsList.collegeId}">
                             <c:set var="seasubOrderItemId" value="${seaSideProsList.subOrderItemId}"/>
                             <c:set var="seacollegeId" value="${seaSideProsList.collegeId}"/>
                             <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("seasubOrderItemId")+"SP"+(String)pageContext.getAttribute("seacollegeId")+"GP";%>
                        </c:if>
                      </c:if>
                      <%int seasSideProsIndex = Integer.parseInt((pageContext.getAttribute("seasSideProsIndex")).toString());
                      if(seasSideProsIndex%4==0 && seasSideProsIndex!=0){ %>
                         </li><li>
                       <%}%>
                        <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                        <c:if test="${seaSideProsList.collegeInBasket eq 'TRUE'}">
                          <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                        </c:if>
                        <div class="<%=columnClassName%>" onclick="javascript:addProspectusBasket('${seaSideProsList.collegeId}','C','${seaSideProsList.subOrderItemId}','RP','<%=sliderCount%>','seaside','${seaSideProsList.gaCollegeName}')" id="div_slider_<%=sliderCount%>_${seaSideProsList.subOrderItemId}" >
                          <a class="fr mr20" onclick="javascript:addProspectusBasket('${seaSideProsList.collegeId}','C','${seaSideProsList.subOrderItemId}','RP','<%=sliderCount%>','seaside','${seaSideProsList.gaCollegeName}')">                                            
                           <i id="add_slider_<%=sliderCount%>_${seaSideProsList.subOrderItemId}" class="<%=plusClassName%>"></i>
                          </a>   
                           <c:if test="${not empty seaSideProsList.collegeLogoPath}">                       
                            <div class="img_ppn">
                             <%if(sliderCount>1){ %>
                              <img id="imgslider_<%=sliderCount%>_<%=seasSideProsIndex%>" alt="${seaSideProsList.collegeDisplayName}" title="${seaSideProsList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${seaSideProsList.collegeLogoPath}">
                            <%}else{%>
                              <img id="imgslider_<%=sliderCount%>_<%=seasSideProsIndex%>" alt="${seaSideProsList.collegeDisplayName}" title="${seaSideProsList.collegeDisplayName}" src="${seaSideProsList.collegeLogoPath}">
                            <%}%>
                            </div>
                          </c:if>
                          <div class="ftc">
                            <h6 class="fnt_lbd">${seaSideProsList.collegeDisplayName}</h6>
                            <c:if test="${not empty seaSideProsList.overallRating}">
                               <fmt:parseNumber var = "overallRating" type = "number" value = "${seaSideProsList.overallRating}" />
                               <c:if test="${overallRating gt 0}"> 
                              <div class="art_cnt">STUDENT RATING
                                <div class="rev_return">
                                  <span class="rat${seaSideProsList.overAllRatingRounded} t_tip">
																	                   <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                            <span class="cmp">
								                              <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                             <div class="line"></div>
								                            </span>
								                            <%--End of rating tool tip code--%>
																	                 </span>
                                  <span class="rat_value fl">(${seaSideProsList.overallRating})</span>
                                </div>
                              </div>
                             </c:if> 
                            </c:if>
                              <c:if test="${not empty seaSideProsList.timesRanking}">
                                 <div class="art_cnt">The CompUniGuide ranking:  ${seaSideProsList.timesRanking}</div>
                              </c:if>                                              
                          </div>
                      </div>
                  </c:forEach>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
       <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','seaside')" class="select_all fr fnt_lrg">SELECT ALL</a>       
       <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','seaside')" class="select_all fr fnt_lrg">SELECT ALL</a>       
     </div> 
    </article>
  </section>
</c:if>
<c:if test="${not empty requestScope.townProsList}">
 <%sliderCount++;String selectAllInst=""; String totalCount="";%>
   <section class="ltst_art">
    <article class="content-bg slct_all">
      <div class="flexslider2">
         <h3 class="fnt_lrg">
          <c:forEach var="townProsList" items="${requestScope.townProsList}" varStatus="indexg" end="0">
            <span class="Ptit_cnr">
            <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">Town</a>
            <a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">Town</a>
            <span class="fnt_lrg pl10 Puni">(${townProsList.totalCount} 
               <c:if test="${townProsList.totalCount gt 1}">universities</c:if>
                <c:if test="${townProsList.totalCount eq 1}">university</c:if>)</span>
           </span>
          </c:forEach>
            <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
            <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
          </h3>
          <div id="mslider_<%=sliderCount%>" class="pros" style="display:none;overflow:hidden;"></div> 
        <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow: hidden;">
                                     
          <div class="slider_cnr" id="menu_slider_<%=sliderCount%>">
           <div class="slide_menu">
            <div class="mt15 ml20">
              <ul class="slides"> 
                <li>
                  <c:forEach var="townProsList" items="${requestScope.townProsList}" varStatus="townProsIndex">

                    <c:set var="townProsIndex" value="${townProsIndex.index}"/> 
                      <c:if test="${not empty townProsList.subOrderItemId}">
                        <c:if test="${not empty townProsList.collegeId}">
                          <c:set var="townsubOrderItemId" value="${townProsList.subOrderItemId}"/>
                          <c:set var="towncollegeId" value="${townProsList.collegeId}"/>
                          <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("townsubOrderItemId")+"SP"+(String)pageContext.getAttribute("towncollegeId")+"GP";%>
                        </c:if>
                      </c:if>
                      <%int townProsIndex = Integer.parseInt((pageContext.getAttribute("townProsIndex")).toString());

                      if(townProsIndex%4==0 && townProsIndex!=0){ %>
                         </li><li>
                       <%}%>
                        <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                         <c:if test="${townProsList.collegeInBasket eq 'TRUE'}">
                          <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                        </c:if>
                       <div class="<%=columnClassName%>" onclick="javascript:addProspectusBasket('${townProsList.collegeId}','C','${townProsList.subOrderItemId}','RP','<%=sliderCount%>','town','${townProsList.gaCollegeName}')" id="div_slider_<%=sliderCount%>_${townProsList.subOrderItemId}">
                          <a class="fr mr20" onclick="javascript:addProspectusBasket('${townProsList.collegeId}','C','${townProsList.subOrderItemId}','RP','<%=sliderCount%>','town','${townProsList.gaCollegeName}')">                                            
                            <i id="add_slider_<%=sliderCount%>_${townProsList.subOrderItemId}" class="<%=plusClassName%>"></i>
                          </a>   
                          <c:if test="${not empty townProsList.collegeLogoPath}">
                            <div class="img_ppn">
                              <%if(sliderCount>1){ %>
                                 <img id="imgslider_<%=sliderCount%>_<%=townProsIndex%>" alt="${townProsList.collegeDisplayName}" title="${townProsList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${townProsList.collegeLogoPath}">
                              <%}else{%>
                                 <img id="imgslider_<%=sliderCount%>_<%=townProsIndex%>" alt="${townProsList.collegeDisplayName}" title="${townProsList.collegeDisplayName}" src="${townProsList.collegeLogoPath}">
                              <%}%>
                            </div>
                          </c:if>
                          <div class="ftc">
                            <h6 class="fnt_lbd">${townProsList.collegeDisplayName}</h6>
                            <c:if test="${not empty townProsList.overallRating}">
                               <fmt:parseNumber var = "overallRating" type = "number" value = "${townProsList.overallRating}" />
                               <c:if test="${overallRating gt 0}"> 
                              <div class="art_cnt">STUDENT RATING
                                <div class="rev_return">
                                  <span class="rat${townProsList.overAllRatingRounded} t_tip">
																	                   <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                            <span class="cmp">
								                              <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                             <div class="line"></div>
								                            </span>
								                            <%--End of rating tool tip code--%>
																	                 </span>
                                  <span class="rat_value fl">(${townProsList.overallRating})</span>
                                </div>
                              </div>
                             </c:if>
                            </c:if>
                             <c:if test="${not empty townProsList.timesRanking}">
                              <div class="art_cnt">The CompUniGuide ranking:  ${townProsList.timesRanking}</div>
                            </c:if>                                             
                          </div>
                      </div>
                  </c:forEach>
                </li>
              </ul>
            </div>
          </div>
        </div>
       </div>
       
       <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','town')" class="select_all fr fnt_lrg">SELECT ALL</a>        
       <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','town')" class="select_all fr fnt_lrg">SELECT ALL</a>        
     </div>      
    </article>
  </section>
</c:if>
