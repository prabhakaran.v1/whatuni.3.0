<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page import="WUI.utilities.GlobalFunction, WUI.utilities.CommonUtil" autoFlush="true"%>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Redirecting...</title>
  </head>
  <body id="www-whatuni-com">
  
  <%--"/degrees/prospectuslink.html?z=4574&amp;url=http://www.kcl.ac.uk/prospectus/"--%>
  
  <% 
    String externalurl = request.getParameter("url");
    String collegeid = request.getParameter("cid");
    if (collegeid == null || collegeid.trim().length() == 0){ collegeid = request.getParameter("z");}
    String clientIp = request.getHeader( "CLIENTIP");
    if( clientIp==null || clientIp.length()==0){
      clientIp = request.getRemoteAddr();
    }   
    String userAgent = request.getHeader("user-agent");
    String requestUrl = CommonUtil.getRequestedURL(request); //Change the getRequestURL by Prabha on 28_Jun_2016
    String refererUrl = request.getHeader("referer");
    String jsLogId = new GlobalFunction().trackExternalProspectus(request, clientIp, userAgent,collegeid,externalurl,requestUrl,refererUrl,session);
    response.sendRedirect(externalurl);
  %>
  </body>
</html>