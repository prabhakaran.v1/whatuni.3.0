<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="java.util.*, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants, WUI.utilities.CommonUtil" autoFlush="true" %>

  <%  
        String metaRobots = request.getAttribute("metarobots") != null ? String.valueOf(request.getAttribute("metarobots")) : "noindex,follow"; //21-Jan-2014 Release
        String urlString  = request.getAttribute("loginUrl")  != null ?  String.valueOf(request.getAttribute("loginUrl")) : "/home.html";
        String url = (String)request.getAttribute("urlString");
        String url1 = "";
        String url2 = "";
        String studyLevelId = "";
        String studyLevel = "";
        String pageno = (String)request.getAttribute("pageNo");
        String networkId = (String)request.getAttribute("netWorkId");
        String urlArray[] = url.split("/");
        String uClass = "actl";
        String pgClass = "nacr";
        String pageName = "PROSPECTUS BROWSE LOCATION";  
               studyLevel = (String)request.getAttribute("studyLevel");
        String queryString = (String)request.getAttribute("modqueryStr");
               queryString = queryString!= null & queryString != "" ? queryString:"";
        String logCollegeId = request.getParameter("collegeid");
        
        //
        String queryStringPros = (String)request.getAttribute("modqueryStrWO");
               queryStringPros = queryStringPros!= null & queryStringPros != "" ? queryStringPros:"";
        //
        String prospectusURL = "/degrees/prospectus/";
        String prosepectusSortType =  ("L").equalsIgnoreCase(studyLevel)?"level=L":"level=M";
        String prosCateLvlTracking = ("M".equalsIgnoreCase(studyLevel)  ? "university-prospectuses-ug" : "university-prospectuses-pg");        
        //
        String fullQueryStr = (String)request.getAttribute("queryStr");
        fullQueryStr = fullQueryStr!=null && fullQueryStr !="" ? "?"+fullQueryStr: "";
        //
        String UGTabUrl = prospectusURL +  (queryStringPros!="" ?   queryStringPros + "&level=M" : "?level=M") ;
        String PGTabUrl = prospectusURL + (queryStringPros!="" ? queryStringPros + "&level=L" : "?level=L") ;
        int sliderCount = 0;
        
        String universitySortStr = (fullQueryStr.contains("sort=asc_college") || fullQueryStr.contains("sort=desc_college")) ? (fullQueryStr.contains("sort=asc_college") ? fullQueryStr.replace("asc_college","desc_college") : fullQueryStr.replace("desc_college","asc_college")) : (queryString!="" ?   (queryString + "&sort=asc_college" ): "?sort=asc_college") ;
        String universitySortClass = (fullQueryStr.contains("sort=asc_college") || fullQueryStr.contains("sort=desc_college")) ? (fullQueryStr.contains("sort=asc_college") ? "fa fa-angle-up fa-1_4x fl" : "fa fa-angle-down fa-1_4x fl") : "fa fa-angle-up fa-1_4x fl";
        String ratinSortStr = (fullQueryStr.contains("sort=asc_student_rating") || fullQueryStr.contains("sort=desc_student_rating")) ? (fullQueryStr.contains("sort=asc_student_rating") ? fullQueryStr.replace("asc_student_rating","desc_student_rating") : fullQueryStr.replace("desc_student_rating","asc_student_rating")) : (queryString!="" ?   (queryString + "&sort=asc_student_rating" ): "?sort=asc_student_rating") ;
        String ratinSortStrClass = (fullQueryStr.contains("sort=asc_student_rating") || fullQueryStr.contains("sort=desc_student_rating")) ? (fullQueryStr.contains("sort=asc_student_rating") ? "fa fa-angle-up fa-1_4x fl" : "fa fa-angle-down fa-1_4x fl") : "fa fa-angle-up fa-1_4x fl";
        String timesRankingSortStr = (fullQueryStr.contains("sort=asc_times_ranking") || fullQueryStr.contains("sort=desc_times_ranking")) ? (fullQueryStr.contains("sort=asc_times_ranking") ? fullQueryStr.replace("sort=asc_times_ranking","sort=desc_times_ranking") : fullQueryStr.replace("sort=desc_times_ranking","sort=asc_times_ranking")) : (queryString!="" ?   (queryString + "&sort=asc_times_ranking" ): "?sort=asc_times_ranking") ;
        String timesRankingClass = (fullQueryStr.contains("sort=asc_times_ranking") || fullQueryStr.contains("sort=desc_times_ranking")) ? (fullQueryStr.contains("sort=asc_times_ranking") ? "fa fa-angle-up fa-1_4x fl" : "fa fa-angle-down fa-1_4x fl") : "fa fa-angle-up fa-1_4x fl";
             if(studyLevel.equalsIgnoreCase("M")){
                pageName = "PROSPECTUS BROWSE LOCATION";
             }else if(studyLevel.equalsIgnoreCase("L")){               
                pageName = "PG PROSPECTUS BROWSE LOCATION";
             }
        String httpStr = new CommonFunction().getSchemeName(request); //SSL preparation change for 29_Mar_2016, By Thiyagu G.
        String canonicalLink = httpStr + "www.whatuni.com/degrees/prospectus/";        
        if(fullQueryStr!="" && fullQueryStr != null){
           //canonicalLink = httpStr + "www.whatuni.com/degrees/prospectus/";
           metaRobots = "noindex,follow";
        }else{
           metaRobots = "index,follow";
        }
        //
        String flexSliderCssName = CommonUtil.getResourceMessage("wuni.flexslider.css", null);
        String lazyLoadJs = CommonUtil.getResourceMessage("wuni.lazyLoadJs.js", null);
        String prospectusJs = CommonUtil.getResourceMessage("wuni.prospectusJsName.js", null);
        String collegeNameDisplay = (request.getAttribute("collegeDispName") != null) ? (String)request.getAttribute("collegeDispName") : "";
    %><%--16-Apr-2014--%>
   <%--Changed WU scheme as part of SSL work for 08_MAR_2016, By Thiyagu G.--%> 
   <% String emailUrl = GlobalConstants.WHATUNI_SCHEME_NAME + ResourceBundle.getBundle("com.resources.ApplicationResources").getString("review.autocomplete.ip") + request.getContextPath() + urlString;
      request.setAttribute("searchText","order a university and college prospectus from whatuni");
      session.setAttribute("prospectus_redirect_url", "/prospectus/university-prospectus-college-prospectus/order-prospectus.html");
   %>
   <body id="www-whatuni-com">
     <header class="clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp" />
          </div>                
        </div>      
      </div>
      
     </header>
     <section id="myPros" class="Main_Cnr">
        <div class="top_scroll_order">
              <div class="ad_cnr">
               <div class="content-bg">   
                 <div class="sbcr mb-10 pros_brcumb">
                   <jsp:include page="/seopods/breadCrumbs.jsp" />              
                 </div>
               <% if(("L").equalsIgnoreCase(studyLevel)){%>
                  <h2 class="fnt_lbd">Postgraduate prospectuses</h2>
                <%}else{%>
                <h2 class="fnt_lbd">University prospectuses</h2>
                <%}%>
                </div>
                </diV>
              <div class="" id="scrollfreezediv">
               <div class="ad_cnr">
               <div class="content-bg">   
              <div class="pros_search fl">
                
                <form id="prospectusForm" method="post" action="/home.html" class="topsearch" onsubmit="javascript:return prospectusSearch();" >
                  <div class="sel_degcnr">
                    <div class="sel_deg">
                      <span class="sld_deg fnt_lbk">
                      <%String degreeValue = ("L").equalsIgnoreCase(studyLevel)?"Postgraduate":"Undergraduate";%>
                      <%=degreeValue%>
                      </span>
                      <i class="fa fa-angle-down"></i>								
                    </div>                    
                    <ul class="drop_deg" style="display: none;">
                      <li rel="tabmenu1" class="fl <%=("M").equalsIgnoreCase(studyLevel)?"act":""%>"><a class="fnt_lrg" href="<%=UGTabUrl%>">Undergraduate</a></li>
                      <li rel="tabmenu2" class="fl <%=("L").equalsIgnoreCase(studyLevel)?"act":""%>"><a class="fnt_lrg" href="<%=PGTabUrl%>">Postgraduate</a></li>
                    </ul>                    
                  </div>
                  <fieldset class="fl">
                     <input class="fnt_lrg tx_bx" type="text" autocomplete="off" name="prospectusKwd" id="prospectusKwd" onkeyup="autoCompleteProspectusBrowse(event,this,'<%=studyLevel%>');javascript:setProsAutoCompleteClass(this);" value="Enter uni name or subject" onclick="javascript:clearProsSearchText(this);" onblur="javascript:setProsSearchText(this);" onkeypress="javascript:if(event.keyCode==13){return prospectusSearch();}"/>            
                     <input type="submit" class="icon_srch fr" value=""/>
                     <i class="fa fa-search fa-1_5x"></i>
                     <input type="hidden" id="prospectusKwd_hidden" value="" />
                     <input type="hidden" id="prospectusKwd_id" value="" />
                     <input type="hidden" id="prospectusKwd_display" value="" />
                     <input type="hidden" id="prospectusKwd_url" value="" />
                     <input type="hidden" id="prospectusKwd_alias" value="" />
                     <input type="hidden" id="prospectusKwd_location" value=""/>            
                   </fieldset>
                   <input type="hidden" id="prosSelQual" value=""/>
                </form>
              </div>
              <div class="order_top fr" id="basketDisabled">
                <div class="order_menu cf">
                  <div class="pros_count fl">
                    <div class="cu_txt"> 
                      <span class="fl count_value fnt_lbd" id="prospectusBasketCount">
                      <c:if test="${not empty requestScope.prospectusBasketCount}">
                            ${requestScope.prospectusBasketCount}
                       </c:if>
                      </span>
                      <span class="ics_order fr"></span>
                    </div>                    
                    <a class="fl fnt_lbd pro_blk">
                      <span class="txt fnt_lbd" onclick="javascript:openProspectusPopup()">Prospectuses</span>                      
                    </a>
                  </div>
                  <div class="pros_fr">
                    <c:if test="${not empty sessionScope.userInfoList}">
                         <a class="btn1 bg_orange fl mt10" onclick="javascript:checkorderCount('loggedIn');">ORDER NOW<i class="fa fa-long-arrow-right"></i></a>
                    </c:if>
                    <c:if test="${empty sessionScope.userInfoList}">
                         <a class="btn1 bg_orange fl mt10" onclick="javascript:checkorderCount('NonloggedIn');">ORDER NOW<i class="fa fa-long-arrow-right"></i></a>
                    </c:if>
                  </div>
                </div>
                <div id="prospectusPopDiv" style="display:none;"></div>
                
              </div>
              </div>
              </div>
             </div> 
              
           </div>
       <div class="ad_cnr">
         <div class="content-bg pros">
           <div class="subCnr">
             <%--myprospectus pod for logged in users alone, 19_May_2015 By Thiyagu G.--%>
             <%@include  file="/prospectus/include/orderedProspectusPod.jsp" %>
             <div id="tabmenu1" class="pros_subCnt fl" style="display: block;">
              <c:if test="${not empty requestScope.propsectusSrList}"> 
              <%String selectAllInst = "";%>
                <div class="list_cnr fl cf pb40">
                  <h3 class="fnt_lrg">
                    <span class="Ptit_cnr">
                   <span class="Ptit"> Showing results for <c:if test="${not empty requestScope.prospectusSearchType}"> '${requestScope.prospectusSearchType}'</c:if></span>
                    <span class="fnt_lrg pl10 Puni">
                      <c:if test="${not empty requestScope.searchTypeCount}">
                           ${requestScope.searchTypeCount}
                           <fmt:parseNumber var = "searchTypeCount" type = "number" value = "${requestScope.searchTypeCount}" />
                           <c:if test="${searchTypeCount gt 1}">universities</c:if>
                           <c:if test="${searchTypeCount eq 1}">university</c:if>
                      </c:if>
                      </span>
                      </span>
                      <c:forEach var="propsectusSrList" items="${requestScope.propsectusSrList}" varStatus="prospectusSRIndex">
                        <c:if test="${not empty propsectusSrList.subOrderItemId}">
                          <c:if test="${not empty propsectusSrList.collegeId}">
                             <c:set var="srressubOrderItemId" value="${propsectusSrList.subOrderItemId}"/>
                             <c:set var="srrescollegeId" value="${propsectusSrList.collegeId}"/>
                               <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("srressubOrderItemId")+"SP"+(String)pageContext.getAttribute("srrescollegeId")+"GP";%>
                         </c:if>
                       </c:if>
                     </c:forEach>
                     <a id="sr_selectAll" onclick="javascript:addremoveallsrPros('<%=selectAllInst%>','sr_selectAll')" class="select_all fr fnt_lrg">SELECT ALL</a>
                  </h3>
                  <div class="row-fluid">
                    <hgroup>
                      <h6 class="lv_c1"><span class="fl fnt_lbd">University name</span><a href="<%=prospectusURL%><%=universitySortStr%>"><i id="arrowClass" class="<%=universitySortClass%>"></i></a></h6>
                      <h6 class="lv_c2"><span class="fl fnt_lbd">Student rating</span><a href="<%=prospectusURL%><%=ratinSortStr%>"><i id="arrowClass" class="<%=ratinSortStrClass%>"></i></a></h6>
                      <h6 class="lv_c3"><span class="fl fnt_lbd">The CompUniGuide ranking</span><a href="<%=prospectusURL%><%=timesRankingSortStr%>"><i id="arrowClass" class="<%=timesRankingClass%>"></i></a></h6>
                    </hgroup>
                  </div>
                   <div class="list_view fl">
                   <c:forEach var="propsectusSrList" items="${requestScope.propsectusSrList}" varStatus="prospectusSRIndex">
                        <c:set var="prospectusSRIndex" value="${prospectusSRIndex.index}"/> 
                        <c:if test="${not empty propsectusSrList.subOrderItemId}">
                          <c:if test="${not empty propsectusSrList.collegeId}">
                             <c:set var="srsubOrderItemId" value="${propsectusSrList.subOrderItemId}"/>
                             <c:set var="srcollegeId" value="${propsectusSrList.collegeId}"/>
                             <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("srsubOrderItemId")+"SP"+(String)pageContext.getAttribute("srcollegeId")+"GP";%>
                        </c:if>
                     </c:if>                    
                 
                     <%String rowClassName = "row"; String plusClassName = "fa fa-plus fa-1_5x";%>
                     <c:if test="${propsectusSrList.collegeInBasket eq 'TRUE'}">
                       <%rowClassName = "row act"; plusClassName = "fa fa-remove fa-1_5x";%>
                     </c:if>
                     <%  int prospectusSRIndex = Integer.parseInt((pageContext.getAttribute("prospectusSRIndex")).toString()); %>
                     <div class="<%=rowClassName%>" id="div_search_res_${propsectusSrList.subOrderItemId}" onclick="javascript:addSearchResultProspectusBasket('${propsectusSrList.collegeId}', 'C', '${propsectusSrList.subOrderItemId}','RP','${propsectusSrList.gaCollegeName}');">
                       <div class="c1">
                         <div class="img_ppn">
                          <img id="searchSlider_<%=prospectusSRIndex%>" alt="${propsectusSrList.collegeDisplayName}" title="${propsectusSrList.collegeDisplayName}" src="${propsectusSrList.collegeLogoPath}">
                        </div>
                      </div>
                    <div class="c2 fnt_lbd">
                     <span class="fnt_lbd">
                      ${propsectusSrList.collegeDisplayName}
                     </span> 
                      <c:if test="${not empty propsectusSrList.openDays}">
                         <a class="fnt_lrg fl blue" href="${propsectusSrList.openDaysUrl}" title="${propsectusSrList.collegeDisplayName}"> Next Openday ${propsectusSrList.openDays} </a>
                      </c:if>
                    </div>
                    <div class="c3">
                     <c:if test="${not empty propsectusSrList.overallRating}">
                      <fmt:parseNumber var = "overallRating" type = "number" value = "${propsectusSrList.overallRating}" />
                      <c:if test="${overallRating gt 0}"> 
                         <div class="fnt_lrg">STUDENT RATING</div>
                          <div class="rev_return pl10 mb15">
                            <span class="rat${propsectusSrList.overAllRatingRounded} t_tip">
														                <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                      <span class="cmp">
								                        <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                       <div class="line"></div>
								                      </span>
								                      <%--End of rating tool tip code--%>
														              </span>
                            <span class="rat_value fl">(${propsectusSrList.overallRating})</span>
                          </div>
                        </c:if>
                      </c:if>
                    </div>
                    <div class="c4 fnt_lbd">
                       <c:if test="${not empty propsectusSrList.timesRanking}">
                         ${propsectusSrList.timesRanking}
                       </c:if>
                    </div>
                    <a onclick="javascript:addSearchResultProspectusBasket('${propsectusSrList.collegeId}', 'C', '${propsectusSrList.subOrderItemId}','RP','${propsectusSrList.gaCollegeName}');">
                    <i id="add_search_res_${propsectusSrList.subOrderItemId}" class="<%=plusClassName%>"></i>                   
                    </a>
                    </div>                  
                  </c:forEach>
                  </div>
                </div>
                
              </c:if>       
              <%--Added prospectus not found message with searched uni for 11_Aug_2015, By Thiyagu G--%>
              <c:if test="${empty requestScope.propsectusSrList}">
                <c:if test="${not empty requestScope.prosSearchNoResults}">              
                    <p class="err error_msge"><spring:message code="wuni.prospectus.search.no.result.text1" arguments="<%= collegeNameDisplay%>"/></p>
                    <p class="err error_msge"><spring:message code="wuni.prospectus.search.no.result.text2" arguments="<%= collegeNameDisplay%>"/></p>                    
                 </c:if>
              </c:if>
               <div class="table_view">                    
                   <script type="text/javascript">
                     loadProspectusSlider();                     
                    </script>
                    <% String selectAllInst = "";%>
                    <c:if test="${not empty requestScope.subjectRankingList}">
                     <%sliderCount++;%>
                     <section class="ltst_art">
                      <article class="content-bg slct_all">
                        <div class="flexslider2">
                          <h3 class="fnt_lrg">
                           <c:forEach var="subjectRankingList" items="${requestScope.subjectRankingList}" varStatus="indexd" end="0">
                             <span class="Ptit_cnr"> 
                              <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">${subjectRankingList.displayName}</a>
                              <a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">${subjectRankingList.displayName}</a>
                              <span class="fnt_lrg pl10 Puni">(${subjectRankingList.totalCount}
                                <fmt:parseNumber var = "totalCount" type = "number" value = "${subjectRankingList.totalCount}" />  
                                <c:if test="${totalCount gt 1}">universities</c:if>
                                <c:if test="${totalCount eq 1}">university</c:if>)</span>
                              </span>
                              <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
                              <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
                             </c:forEach>
                            </h3> 
                          <div id="mslider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow:hidden;"></div> 
                          <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow:hidden;">
                            <div class="slider_cnr" id="menu_slider_<%=sliderCount%>">
                             <div class="slide_menu">
                              <div class="mt15 ml20">
                                <ul class="slides"> 
                                  <li>
                                     <c:forEach var="subjectRankingList" items="${requestScope.subjectRankingList}" varStatus="timesIndex">
                                      <c:if test="${not empty subjectRankingList.subOrderItemId}">
                                       <c:if test="${not empty subjectRankingList.collegeId}">
                                        <c:set var="subOrderItemId" value="${subjectRankingList.subOrderItemId}"/>
                                         <c:set var="collegeId" value="${subjectRankingList.collegeId}"/>
                                          <c:set var="timesIndex" value="${timesIndex.index}"/> 
                                           <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("subOrderItemId")+"SP"+(String)pageContext.getAttribute("collegeId")+"GP";%>
                                         </c:if>
                                       </c:if> 
                                        <%
                                        int timesIndex = Integer.parseInt(pageContext.getAttribute("timesIndex").toString());
                                        if(timesIndex%4==0 && timesIndex!=0){ %>
                                           </li><li>
                                         <%}%>                                         
                                          <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                                          <c:if test="${subjectRankingList.collegeInBasket eq 'TRUE'}">
                                            <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                                          </c:if>
                                          <div class="<%=columnClassName%>"  onclick="javascript:addProspectusBasket('${subjectRankingList.collegeId}','C','${subjectRankingList.subOrderItemId}','RP','<%=sliderCount%>','CompUniGuide subject ranking for','${subjectRankingList.gaCollegeName}')" id="div_slider_<%=sliderCount%>_${subjectRankingList.subOrderItemId}" >
                                            <a class="fr mr20" onclick="javascript:addProspectusBasket('${subjectRankingList.collegeId}','C','${subjectRankingList.subOrderItemId}','RP','<%=sliderCount%>','CompUniGuide subject ranking for','${subjectRankingList.gaCollegeName}')">                                            
                                              <i id="add_slider_<%=sliderCount%>_${subjectRankingList.subOrderItemId}" class="<%=plusClassName%>"></i>
                                            </a>
                                            <c:if test="${not empty subjectRankingList.collegeLogoPath}">
                                              <div class="img_ppn">
                                                <%if(sliderCount>1){ %>
                                                <img id="imgslider_<%=sliderCount%>_<%=timesIndex%>" alt="${subjectRankingList.collegeDisplayName}" title="${subjectRankingList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${subjectRankingList.collegeLogoPath}">
                                                <%}else{%>
                                                <img id="imgslider_<%=sliderCount%>_<%=timesIndex%>" alt="${subjectRankingList.collegeDisplayName}" title="${subjectRankingList.collegeDisplayName}" src="${subjectRankingList.collegeLogoPath}">
                                                <%}%>
                                              </div>
                                            </c:if>
                                            <div class="ftc">
                                              <h6 class="fnt_lbd">${subjectRankingList.collegeDisplayName}</h6>
                                              <c:if test="${not empty subjectRankingList.overallRating}">
                                                 <fmt:parseNumber var = "overallRating" type = "number" value = "${subjectRankingList.overallRating}" />
                                                 <c:if test="${overallRating gt 0}"> 
                                                <div class="art_cnt">STUDENT RATING
                                                  <div class="rev_return">
                                                    <span class="rat${subjectRankingList.overAllRatingRounded} t_tip">
																										                            <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                                              <span class="cmp">
								                                                <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                                               <div class="line"></div>
								                                              </span>
								                                              <%--End of rating tool tip code--%>
																										                          </span>
                                                    <span class="rat_value fl">(${subjectRankingList.overallRating})</span>
                                                  </div>
                                                </div>
                                                </c:if>
                                              </c:if>
                                               <c:if test="${not empty subjectRankingList.timesRanking}">
                                                <div class="art_cnt">The CompUniGuide ranking:  ${subjectRankingList.timesRanking}</div>
                                              </c:if>                                             
                                            </div>
                                        </div>
                                       </c:forEach>
                                    </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                          <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','CompUniGuide subject ranking for')" class="select_all fr fnt_lrg">SELECT ALL</a>                       
                          <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','CompUniGuide subject ranking for')" class="select_all fr fnt_lrg">SELECT ALL</a>                       
                       </div> 
                      </article>
                    </section>
                  </c:if> 
                           
                  
                  <c:if test="${not empty requestScope.otherStudentsProsList}">
                     <%sliderCount++; selectAllInst = "";%>
                     <section class="ltst_art">
                      <article class="content-bg slct_all">
                        <div class="flexslider2">
                         <h3 class="fnt_lrg">
                          <c:forEach var="otherStudentsProsList" items="${requestScope.otherStudentsProsList}" varStatus="indexb" end="0">
                           <span class="Ptit_cnr"> 
                             <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display:none;">Other students also picked</a>
                             <a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">Other students also picked</a>
                             <span class="fnt_lrg pl10 Puni">
                             <c:if test="${empty requestScope.prosSearchNoResults}">
                             (${otherStudentsProsList.totalCount}
                               <fmt:parseNumber var = "totalCount" type = "number" value = "${otherStudentsProsList.totalCount}" />  
                               <c:if test="${totalCount gt 1}">universities</c:if>
                                  <c:if test="${totalCount eq 1}">university</c:if> 
                               )                             
                             </c:if>
                             &nbsp;</span>
                          </span>
                          </c:forEach>
                           <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" style="display:none;" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
                           <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
                          </h3>   
                          <div id="mslider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow:hidden;"></div> 
                          <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow:hidden;">
                                                     
                            <div class="slider_cnr" id="menu_slider_<%=sliderCount%>">
                             <div class="slide_menu">
                              <div class="mt15 ml20">
                                <ul class="slides"> 
                                  <li>
                                    <c:forEach var="otherStudentsProsList" items="${requestScope.otherStudentsProsList}" varStatus="otherStdProsIndex">
                                       <c:if test="${not empty otherStudentsProsList.subOrderItemId}">
                                          <c:if test="${not empty otherStudentsProsList.collegeId}">
                                            <c:set var="othersubOrderItemId" value="${otherStudentsProsList.subOrderItemId}"/>
                                             <c:set var="othercollegeId" value="${otherStudentsProsList.collegeId}"/>
                                             <c:set var="otherStdProsIndex" value="${otherStdProsIndex.index}"/> 
                                                 <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("othersubOrderItemId")+"SP"+(String)pageContext.getAttribute("othercollegeId")+"GP";%>
                                            </c:if>
                                         </c:if>
                                     <% int otherStdProsIndex = Integer.parseInt((pageContext.getAttribute("otherStdProsIndex")).toString());
                                        if(otherStdProsIndex%4==0 && otherStdProsIndex!=0){ 
                                        	%>
                                           </li><li>
                                         <%}%> 
                                           <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                                            <c:if test="${otherStudentsProsList.collegeInBasket eq 'TRUE'}">
                                               <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                                            </c:if>
                                           <div class="<%=columnClassName%>" onclick="javascript:addProspectusBasket('${otherStudentsProsList.collegeId}','C','${otherStudentsProsList.subOrderItemId}','RP','<%=sliderCount%>','other students also picked','${otherStudentsProsList.gaCollegeName}')" id="div_slider_<%=sliderCount%>_${otherStudentsProsList.subOrderItemId}">
                                             <a class="fr mr20" onclick="javascript:addProspectusBasket('${otherStudentsProsList.collegeId}','C','${otherStudentsProsList.subOrderItemId}','RP','<%=sliderCount%>','other students also picked','${otherStudentsProsList.gaCollegeName}')">                                            
                                               <i id="add_slider_<%=sliderCount%>_${otherStudentsProsList.subOrderItemId}" class="<%=plusClassName%>"></i>
                                              </a>
                                              <c:if test="${not empty otherStudentsProsList.collegeLogoPath}">
                                              <div class="img_ppn">
                                               <%if(sliderCount>1){ %>
                                                <img id="imgslider_<%=sliderCount%>_<%=otherStdProsIndex%>" alt="${otherStudentsProsList.collegeDisplayName}" title="${otherStudentsProsList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${otherStudentsProsList.collegeLogoPath}">
                                                <%}else{%>
                                                 <img id="imgslider_<%=sliderCount%>_<%=otherStdProsIndex%>" alt="${otherStudentsProsList.collegeDisplayName}" title="${otherStudentsProsList.collegeDisplayName}" src="${otherStudentsProsList.collegeLogoPath}">
                                                <%}%>
                                              </div>
                                            </c:if>
                                            <div class="ftc">
                                              <h6 class="fnt_lbd">${otherStudentsProsList.collegeDisplayName}</h6>
                                              <c:if test="${not empty otherStudentsProsList.overallRating}">
                                                 <fmt:parseNumber var = "overallRating" type = "number" value = "${otherStudentsProsList.overallRating}" />
                                                 <c:if test="${overallRating gt 0}"> 
                                                 <div class="art_cnt">STUDENT RATING
                                                  <div class="rev_return">
                                                    <span class="rat${otherStudentsProsList.overAllRatingRounded} t_tip">
																										                            <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                                              <span class="cmp">
								                                                <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                                               <div class="line"></div>
								                                              </span>
								                                              <%--End of rating tool tip code--%>
																										                          </span>
                                                    <span class="rat_value fl">(${otherStudentsProsList.overallRating})</span>
                                                  </div>
                                                </div>
                                                </c:if>
                                              </c:if>
                                              <c:if test="${not empty otherStudentsProsList.timesRanking}">
                                                <div class="art_cnt">The CompUniGuide ranking:  ${otherStudentsProsList.timesRanking}</div>
                                              </c:if>                                             
                                            </div>
                                        </div>
                                   </c:forEach>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                         </div>
                         <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','other students also picked')" class="select_all fr fnt_lrg">SELECT ALL</a>
                         <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','other students also picked')" class="select_all fr fnt_lrg">SELECT ALL</a>
                        </div>                     
                      </article>
                    </section>
                  </c:if>
                  <c:if test="${not empty requestScope.recommendUniList}">
                     <%sliderCount++; selectAllInst =""; %>
                     <section class="ltst_art">
                      <article class="content-bg slct_all">
                        <div class="flexslider2">
                         <h3 class="fnt_lrg">
                              <c:forEach var="recommendUniList" items="${requestScope.recommendUniList}" varStatus="indexc" end="0">
                                <span class="Ptit_cnr"> 
                                <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">Recommended for you</a>
								<a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">Recommended for you</a>
                                <span class="fnt_lrg pl10 Puni">(${recommendUniList.totalCount}
                                <fmt:parseNumber var = "totalCount" type = "number" value = "${recommendUniList.totalCount}" />  
                                 <c:if test="${totalCount gt 1}">universities</c:if>
                                <c:if test="${totalCount eq 1}">university</c:if>
                              )</span>
							  </span>
                              </c:forEach>
                              <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
                              <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
                          </h3>  
                          <div id="mslider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow:hidden;"></div> 
                          <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow:hidden;">                                                      
                            <div class="slider_cnr" id="menu_slider_<%=sliderCount%>">
                             <div class="slide_menu">
                              <div class="mt15 ml20">
                                <ul class="slides"> 
                                  <li>
                                  
                                    <c:forEach var="recommendUniList" items="${requestScope.recommendUniList}" varStatus="recommendIndex">
                                      <c:if test="${not empty recommendUniList.subOrderItemId}">
                                       <c:if test="${not empty recommendUniList.collegeId}">
                                        <c:set var="recsubOrderItemId" value="${recommendUniList.subOrderItemId}"/>
                                         <c:set var="reccollegeId" value="${recommendUniList.collegeId}"/>
                                          <c:set var="recommendIndex" value="${recommendIndex.index}"/>
                                                 <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("recsubOrderItemId")+"SP"+(String)pageContext.getAttribute("reccollegeId")+"GP";%>
                                            </c:if>
                                         </c:if>
                                        <%
                                        int recommendIndex = Integer.parseInt((pageContext.getAttribute("recommendIndex")).toString());
                                        if(recommendIndex%4==0 && recommendIndex!=0){ %>
                                           </li><li>
                                         <%}%> 
                                         <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                                          <c:if test="${recommendUniList.collegeInBasket eq 'TRUE'}">
                                            <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                                          </c:if>
                                         
                                          <div class="<%=columnClassName%>" onclick="javascript:addProspectusBasket('${recommendUniList.collegeId}','C','${recommendUniList.subOrderItemId}','RP','<%=sliderCount%>','recommended for you','${recommendUniList.collegeDisplayName}')" id="div_slider_<%=sliderCount%>_${recommendUniList.subOrderItemId}">
                                            <a class="fr mr20" onclick="javascript:addProspectusBasket('${recommendUniList.collegeId}','C','${recommendUniList.subOrderItemId}','RP','<%=sliderCount%>','recommended for you','${recommendUniList.collegeDisplayName}')">                                            
                                              <i id="add_slider_<%=sliderCount%>_${recommendUniList.subOrderItemId}" class="<%=plusClassName%>"></i>
                                            </a>
                                             <c:if test="${not empty recommendUniList.collegeLogoPath}">
                                              <div class="img_ppn">
                                                <%if(sliderCount>1){ %>
                                                <img id="imgslider_<%=sliderCount%>_<%=recommendIndex%>" alt="${recommendUniList.collegeDisplayName}" title="${recommendUniList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${recommendUniList.collegeLogoPath}">
                                                <%}else{%>
                                                 <img id="imgslider_<%=sliderCount%>_<%=recommendIndex%>" alt="${recommendUniList.collegeDisplayName}" title="${recommendUniList.collegeDisplayName}" src="${recommendUniList.collegeLogoPath}">
                                                <%}%>
                                              </div>
                                            </c:if>
                                            <div class="ftc">
                                              <h6 class="fnt_lbd">${recommendUniList.collegeDisplayName}</h6>
                                              <c:if test="${not empty recommendUniList.overallRating}">
                                              <fmt:parseNumber var = "overallRating" type = "number" value = "${recommendUniList.overallRating}" />
                                                <c:if test="${overallRating gt 0}"> 
                                                <div class="art_cnt">STUDENT RATING
                                                  <div class="rev_return">
                                                    <span class="rat${recommendUniList.overAllRatingRounded} t_tip">
																										                            <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                                              <span class="cmp">
								                                                <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                                               <div class="line"></div>
								                                              </span>
								                                              <%--End of rating tool tip code--%>
																										                          </span>
                                                    <span class="rat_value fl">(${recommendUniList.overallRating})</span>
                                                  </div>
                                                </div>
                                                </c:if>
                                              </c:if>
                                               <c:if test="${not empty recommendUniList.timesRanking}">
                                                <div class="art_cnt">The CompUniGuide ranking:  ${recommendUniList.timesRanking}</div>
                                              </c:if>                                              
                                            </div>
                                        </div>
                                    </c:forEach>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                         <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','recommended for you')" class="select_all fr fnt_lrg">SELECT ALL</a>
                         <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','recommended for you')" class="select_all fr fnt_lrg">SELECT ALL</a>
                       </div> 
                     </article>
                    </section>
                  </c:if>
                  <c:if test="${not empty requestScope.uniNearProsList}">     
                            
                      <%sliderCount++; selectAllInst="";%>
                     <section class="ltst_art">
                      <article class="content-bg slct_all">
                        <div class="flexslider2">
                           <h3 class="fnt_lrg">
                             <c:forEach var="uniNearProsList" items="${requestScope.uniNearProsList}" varStatus="indexe" end="0">
                              <span class="Ptit_cnr"> 
                              <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">Universities near you </a>
							  <a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">Universities near you </a>
                              <span class="fnt_lrg pl10 Puni">(${uniNearProsList.totalCount}  
                                 <fmt:parseNumber var = "totalCount" type = "number" value = "${uniNearProsList.totalCount}" /> 
                                <c:if test="${totalCount gt 1}">universities</c:if>
                                  <c:if test="${totalCount eq 1}">university</c:if>)</span>
								</span>
							</c:forEach>
                          <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
                              <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
                            </h3>     
                            <div id="mslider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow:hidden;"></div> 
                            <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow:hidden;">                                                      
                            <div class="slider_cnr" id="menu_slider_<%=sliderCount%>">
                             <div class="slide_menu">
                              <div class="mt15 ml20">
                                <ul class="slides"> 
                                  <li>
                                   <c:forEach var="uniNearProsList" items="${requestScope.uniNearProsList}" varStatus="unisNearProsIndex">
                                       <c:if test="${not empty uniNearProsList.subOrderItemId}"> 
                                          <c:if test="${not empty uniNearProsList.collegeId}"> 
                                             <c:set var="uniNearsubOrderItemId" value="${uniNearProsList.subOrderItemId}"/>
                                             <c:set var="uniNearcollegeId" value="${uniNearProsList.collegeId}"/>
                                             <c:set var="unisNearProsIndex" value="${unisNearProsIndex.index}"/> 
                                                 <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("uniNearsubOrderItemId") +"SP"+ (String)pageContext.getAttribute("uniNearcollegeId") +"GP";%>
                                            </c:if>
                                         </c:if>
                                        <%int unisNearProsIndex = Integer.parseInt((pageContext.getAttribute("unisNearProsIndex")).toString());
                                          if(unisNearProsIndex%4==0 && unisNearProsIndex!=0){ %>
                                           </li><li>
                                         <%}%>
                                           <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                                            <c:if test="${uniNearProsList.collegeInBasket eq 'TRUE'}">
                                              <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                                            </c:if>
                                          <div class="<%=columnClassName%>" onclick="javascript:addProspectusBasket('${uniNearProsList.collegeId}','C','${uniNearProsList.subOrderItemId}','RP','<%=sliderCount%>','universities near you','${uniNearProsList.gaCollegeName}')" id="div_slider_<%=sliderCount%>_${uniNearProsList.subOrderItemId}">
                                             <a class="fr mr20" onclick="javascript:addProspectusBasket('${uniNearProsList.collegeId}','C','${uniNearProsList.subOrderItemId}','RP','<%=sliderCount%>','universities near you','${uniNearProsList.gaCollegeName}')">                                            
                                               <i id="add_slider_<%=sliderCount%>_${uniNearProsList.subOrderItemId}" class="<%=plusClassName%>"></i>
                                              </a>
                                              <c:if test="${not empty uniNearProsList.collegeLogoPath}">
                                              <div class="img_ppn">
                                                <%if(sliderCount>1){ %>
                                                  <img id="imgslider_<%=sliderCount%>_<%=unisNearProsIndex%>" alt="${uniNearProsList.collegeDisplayName}" title="${uniNearProsList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${uniNearProsList.collegeLogoPath}">
                                                <%}else{%>
                                                  <img id="imgslider_<%=sliderCount%>_<%=unisNearProsIndex%>" alt="${uniNearProsList.collegeDisplayName}" title="${uniNearProsList.collegeDisplayName}" src="${uniNearProsList.collegeLogoPath}">
                                                <%}%>
                                              </div>
                                            </c:if>
                                            <div class="ftc">
                                              <h6 class="fnt_lbd">${uniNearProsList.collegeDisplayName}</h6>
                                              <c:if test="${not empty uniNearProsList.overallRating}">
                                              <fmt:parseNumber var = "overallRating" type = "number" value = "${uniNearProsList.overallRating}" />
                                               <c:if test="${overallRating gt 0}"> 
                                                <div class="art_cnt">STUDENT RATING
                                                  <div class="rev_return">
                                                    <span class="rat${uniNearProsList.overAllRatingRounded} t_tip">
																				   						                          <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                                               <span class="cmp">
								                                                 <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                                                <div class="line"></div>
								                                               </span>
								                                               <%--End of rating tool tip code--%>
																										                          </span>
                                                    <span class="rat_value fl">(${uniNearProsList.overallRating})</span>
                                                  </div>
                                                </div>
                                                </c:if>
                                             </c:if>
                                             
                                             <c:if test="${not empty uniNearProsList.timesRanking}">
                                                <div class="art_cnt">The CompUniGuide ranking:  ${uniNearProsList.timesRanking}</div>
                                            </c:if>                                            
                                            </div>
                                        </div>
                                    </c:forEach>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                          <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','universities near you')" class="select_all fr fnt_lrg">SELECT ALL</a>  
                          <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','universities near you')" class="select_all fr fnt_lrg">SELECT ALL</a>  
                       </div>                       
                      </article>
                    </section>
                  </c:if>
                  <c:if test="${not empty requestScope.timesRankingList}">  
                  <%sliderCount++; selectAllInst="";%>
                   <section class="ltst_art">
                    <article class="content-bg slct_all">
                      <div class="flexslider2">
                        <h3 class="fnt_lrg">
                           <c:forEach var="timesRankingList" items="${requestScope.timesRankingList}" varStatus="indexf" end="0">
                            <span class="Ptit_cnr"> 
                            <a id="mslideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" class="Ptit" style="display: none;">CompUniGuide ranking</a>
							<a id="slideratag_<%=sliderCount%>" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');" class="Ptit">CompUniGuide ranking</a>
                            <span class="fnt_lrg pl10 Puni">(${timesRankingList.totalCount}
                             <fmt:parseNumber var = "totalCount" type = "number" value = "${timesRankingList.totalCount}" /> 
                             <c:if test="${totalCount gt 1}">universities</c:if>
                               <c:if test="${totalCount eq 1}">university</c:if>)</span>
                           </span>
						   </c:forEach>
                            <a id="mslideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('mslider_<%=sliderCount%>');" style="display: none;"><i id="hideShow_mslider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
                            <a id="slideratag1_<%=sliderCount%>" class="fr mr20 Pshow" onclick="javascript:hideShowSliderBlock('slider_<%=sliderCount%>');"><i id="hideShow_slider_<%=sliderCount%>" class="fa fa-plus-circle" rel="article1"></i></a>
                          </h3> 
                        <div id="mslider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow:hidden;"></div> 
                        <div id="slider_<%=sliderCount%>" class="flexslider pros" style="display:none;overflow:hidden;">                                                     
                          <div class="slider_cnr"  id="menu_slider_<%=sliderCount%>">
                           <div class="slide_menu">
                            <div class="mt15 ml20">
                              <ul class="slides"> 
                                <li>
                                   <c:forEach var="timesRankingList" items="${requestScope.timesRankingList}" varStatus="timesRankingProsIndex">
                                     <c:if test="${not empty timesRankingList.subOrderItemId}"> 
                                     <c:if test="${not empty timesRankingList.collegeId}"> 
                                         <c:set var="timesRankingsubOrderItemId" value="${timesRankingList.subOrderItemId}"/>
                                         <c:set var="timesRankingcollegeId" value="${timesRankingList.collegeId}"/>
                                         <c:set var="timesRankingProsIndex" value="${timesRankingProsIndex.index}"/> 
                                                 <%selectAllInst = selectAllInst + (String)pageContext.getAttribute("timesRankingsubOrderItemId")+"SP"+(String)pageContext.getAttribute("timesRankingcollegeId")+"GP";%>
                                            </c:if>
                                         </c:if>
                                      <%
                                      int timesRankingProsIndex = Integer.parseInt((pageContext.getAttribute("timesRankingProsIndex")).toString());
                                      if(timesRankingProsIndex%4==0 && timesRankingProsIndex!=0){ %>
                                         </li><li>
                                       <%}%> 
                                        <%String columnClassName = "ftv"; String plusClassName = "fa fa-plus fa-1_5x";%>
                                            <c:if test="${timesRankingList.collegeInBasket eq 'TRUE'}">
                                              <%columnClassName = "ftv1 ftv selected"; plusClassName = "fa fa-remove fa-1_5x";%>
                                            </c:if>
                                        <div class="<%=columnClassName%>" onclick="javascript:addProspectusBasket('${timesRankingList.collegeId}','C','${timesRankingList.subOrderItemId}','RP','<%=sliderCount%>','CompUniGuide ranking','${timesRankingList.collegeDisplayName}')" id="div_slider_<%=sliderCount%>_${timesRankingList.subOrderItemId}">
                                          <a class="fr mr20" onclick="javascript:addProspectusBasket('${timesRankingList.collegeId}','C','${timesRankingList.subOrderItemId}','RP','<%=sliderCount%>','CompUniGuide ranking','${timesRankingList.collegeDisplayName}')">                                            
                                           <i id="add_slider_<%=sliderCount%>_${timesRankingList.subOrderItemId}" class="<%=plusClassName%>"></i>
                                          </a>
                                          <c:if test="${not empty timesRankingList.collegeLogoPath}"> 
                                            <div class="img_ppn">
                                             <%if(sliderCount>1){ %>
                                               <img id="imgslider_<%=sliderCount%>_<%=timesRankingProsIndex%>" alt="${timesRankingList.collegeDisplayName}" title="${timesRankingList.collegeDisplayName}" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 0)%>" data-src="${timesRankingList.collegeLogoPath}">
                                            <%}else{%>
                                              <img id="imgslider_<%=sliderCount%>_<%=timesRankingProsIndex%>" alt="${timesRankingList.collegeDisplayName}" title="${timesRankingList.collegeDisplayName}" src="${timesRankingList.collegeLogoPath}">
                                            <%}%>                                              
                                            </div>
                                          </c:if>
                                          <div class="ftc">
                                            <h6 class="fnt_lbd">${timesRankingList.collegeDisplayName}</h6>
                                            <c:if test="${not empty timesRankingList.overallRating}">
                                             <fmt:parseNumber var = "overallRating" type = "number" value = "${timesRankingList.overallRating}" />
                                             <c:if test="${overallRating gt 0}"> 
                                              <div class="art_cnt">STUDENT RATING
                                                <div class="rev_return">
                                                  <span class="rat${timesRankingList.overAllRatingRounded} t_tip">
																									                           <%--Over all rating tool tip added by Prabha on 27_JAN_2016_REL--%>
								                                            <span class="cmp">
								                                              <div class="hdf5"><spring:message code="wuni.tooltip.overall.keystats" /></div>
									                                             <div class="line"></div>
								                                            </span>
								                                            <%--End of rating tool tip code--%>
																									                         </span>
                                                  <span class="rat_value fl">(${timesRankingList.overallRating})</span>
                                                </div>
                                              </div>
                                             </c:if>
                                            </c:if>
                                            <c:if test="${not empty timesRankingList.timesRanking}">
                                               <div class="art_cnt">The CompUniGuide ranking:  ${timesRankingList.timesRanking}</div>
                                            </c:if>                                            
                                          </div>
                                      </div>
                                  </c:forEach>
                                </li>
                              </ul>
                            </div>
                          </div>
                         </div> 
                        </div>
                        <a id="selectAll_mslider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_mslider_<%=sliderCount%>','<%=sliderCount%>','CompUniGuide ranking')" class="select_all fr fnt_lrg">SELECT ALL</a>
                        <a id="selectAll_slider_<%=sliderCount%>" style="display:none;" onclick="javascript:addremoveallPros('<%=selectAllInst%>','selectAll_slider_<%=sliderCount%>','<%=sliderCount%>','CompUniGuide ranking')" class="select_all fr fnt_lrg">SELECT ALL</a>
                      </div>
                    </article>
                  </section>
                </c:if>
                  <jsp:include page="/prospectus/includeSliders.jsp"> 
                  <jsp:param name="count" value="<%=sliderCount%>"/>
                  <jsp:param name="prospectusURL" value="<%=prospectusURL%>"/>
                  <jsp:param name="prosepectusSortType" value="<%=prosepectusSortType%>"/>
                </jsp:include>  
                <input type="hidden" value="<%=prosCateLvlTracking%>" id="prosEvntCategory"/>
               </div>
             </div>
             <div class="fr order_btm mt30 mr20">
                <c:if test="${not empty sessionScope.userInfoList}">
                   <a class="btn1 bg_orange fl mt10" onclick="javascript:checkorderCount('loggedIn');">ORDER NOW<i class="fa fa-long-arrow-right"></i></a>
               </c:if>
               <c:if test="${empty sessionScope.userInfoList}">
                 <a class="btn1 bg_orange fl mt10" onclick="javascript:checkorderCount('NonloggedIn');">ORDER NOW<i class="fa fa-long-arrow-right"></i></a>
               </c:if>
			 </div>
           </div> 
           <%--Load the article pod through ajax call, added on 13_Jun_2017, By Thiyagu G.--%>
           <section class="artfc p0 mt38 cm_art_pd" id="articlesPod"></section>
           <input type="hidden" id="scrollStop" value="" />
         </div>         
       </div>
     </section>
     <jsp:include page="/jsp/common/wuFooter.jsp" />
     <script type="text/javascript">
     showFirstDivs();
     autocompleteOnScroll();
     closeWhenClickOutSide();
     </script>
     <%if(logCollegeId!=null && !"".equals(logCollegeId)){%>
      <script type="text/javascript">
        providerPageViewLogging("<%=logCollegeId%>","PROVIDER_PROSPECTUS_LIST"); //9_DEC_14 Added by Amir for ProviderStats logging 
      </script> 
     <%}%>
  </body>



