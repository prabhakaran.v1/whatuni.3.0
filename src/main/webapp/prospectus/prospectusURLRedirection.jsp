<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page import="WUI.utilities.SessionData, WUI.utilities.GlobalConstants,  WUI.utilities.CommonUtil,WUI.utilities.CommonFunction" autoFlush="true"%>
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
  <head><title></title></head>
  <body id="www-whatuni-com">
  <jsp:useBean class="com.wuni.util.sql.GetPLSQL" id="redirect" scope="session"/>
   <% 
      String url          =   new CommonFunction().getSchemeName(request) + new CommonUtil().getProperties().getString("web-link-url");
      String collegeId    =   request.getParameter("z") != null && request.getParameter("z").trim().length() > 0? request.getParameter("z") : "";
      String weburl       =  request.getParameter("weburl") != null && request.getParameter("weburl").trim().length() > 0? request.getParameter("weburl") : "";
      String clientIp     =   request.getHeader( "CLIENTIP");
      if( clientIp==null || clientIp.length()==0){
           clientIp   =   request.getRemoteAddr();
      }   
      String clientBrowser      =   request.getHeader("user-agent");
      String queryString_1      =   "x="+new SessionData().getData(request,"x")+"&y=&a="+GlobalConstants.WHATUNI_AFFILATE_ID+"&p_java=y&p_ip_add="+clientIp+"&p_http_user_agent="+clientBrowser+"&z="+collegeId+"&p_profile_id=&p_url="+weburl;
      response.sendRedirect(url+ "obj_pls_track_wuni_website?" + queryString_1);
    %>
  </body>
</html>