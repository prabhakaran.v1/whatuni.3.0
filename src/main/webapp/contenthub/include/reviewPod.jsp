<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.CommonFunction"%>
<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="clear"></div>
<%
CommonFunction comFn = new CommonFunction();
  String questionTitle = (String)request.getAttribute("questionTitle");
  String collegeId = (String)request.getAttribute("collegeId");
  String collegeName = (String)request.getAttribute("collegeName");
  collegeName = comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(collegeName)));
  String reviewlightboxJs = CommonUtil.getResourceMessage("wuni.reviewlightbox.js", null);
%>
  <script type="text/javascript" language="javascript" src="<%=CommonUtil.getJsPath()%>/js/review/<%=reviewlightboxJs%>"></script>
 <c:if test="${not empty requestScope.TOTAL_REVIEW_COUNT}">
  <c:if test="${requestScope.TOTAL_REVIEW_COUNT ne 0}">
 <section class="cont_fluid row4 rw18 rrev_sec1" id="con-for-nav-33">
      <div class="csticky-holder">
        <article class="clft_cnt1 csticky trans">
          <article class="col_lft">
            <div class="cont_rat">  <img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/reviews.svg",0)%>" alt="Student Reviews">
              <h1>Reviews from students like you</h1>
              <p>What's it like to study at this institution? Current students and alumni share their opinion...</p>
                <a class="gt_prs" onclick="GAInteractionEventTracking('viewreviews', 'Reviews', 'Review Click', '${requestScope.COLLEGE_NAME_GA}');" href="<SEO:SEOURL pageTitle="uni-student-reviews">${requestScope.COLLEGE_NAME}<spring:message code="wuni.seo.url.split.character" />${requestScope.COLLEGE_ID}</SEO:SEOURL>" title="Reviews at ${requestScope.COLLEGE_NAME_DISPLAY}">VIEW ${requestScope.TOTAL_REVIEW_COUNT} REVIEWS<i class="fa fa-long-arrow-right"></i></a>
          </div>
          </article>
        </article>
        <article class="col_rgt rrev_sec" id="write_review_pod">
          <div class="trnk_cnt">
            <div class="stud_rvwcnt">
              <div class="reviews late_rev">
            <div class="lrhd_sec">
                <h2 class="sub_tit fnt_lrg fl txt_lft whclr" id="reviewTitle">Latest reviews</h2>
                <c:if test="${not empty requestScope.reviewSubjectList}">
                <fieldset class="fs_col2 fr" id="location" onclick="openDropdown()">
                  <div class="od_dloc" id="selectedsubject"><span>All subjects</span><span><i class="fa fa-angle-down"></i></span></div>
                  <div class="opsr_lst" id="subjectDropDown">
                  <ul>
                  <li><span><a onclick="showSubjectReview('all','<%=collegeId%>','')">All subjects </a></span></li>
                  <c:forEach var="reviewSubjectList" items="${requestScope.reviewSubjectList}" varStatus="index"> 
                  <c:set var="indexValue" value="${index.index}"/>
                    <li onclick="showSubjectReview('${indexValue}','<%=collegeId%>','${reviewSubjectList.categoryCode}')"><span><a id="subjectdropdownid_${indexValue}"> ${reviewSubjectList.subjectName}</a> </span></li>
                  </c:forEach>
                  </ul>
                  </div>
                </fieldset>
                </c:if>
            </div>
             <div id="subjectReviewDetails">
             <jsp:include page="/contenthub/include/reviewAjaxPod.jsp"/>
           </div>
            <jsp:include page="/contenthub/include/reviewBreakDown.jsp"/>  
          </div>
            </div>
          </div>
        </article>
      </div>
      <input type="hidden" id="sectionName_33" name="sectionName_33" value="REVIEW" /> </section>
</c:if>
</c:if>