<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction"%>
<%String clsName = "";
  CommonFunction common = new CommonFunction();
  String collegeName = "0";%>

<c:if test="${not empty requestScope.opendaysInfoList}">
  <c:forEach var="opendaysInfoList" items="${requestScope.opendaysInfoList}">
    <div class="comLgh" id="lbDivNew">
      <div id="mbd">
        <div class="fcls nw"><a class="go_visit  " onclick="hm();"><i class="fa fa-times"></i></a></div>
        <div class="pform nlr bgw opday" style="display: block;">
          <div class="reg-frm">
            <div class="sp_pad cf w100p">
              <div class="sav_pro">
                <div class="sp_title"><h2><i class="fa fa-check"></i> OPEN DAYS</h2></div>
                <div class="save_pro_cnr no_bg">
                <div class="opd_chcnt">
                  <div class="col_left fl"><img src="<%=CommonUtil.getImgPath("",0)%>${opendaysInfoList.collegeLogo}" alt="${opendaysInfoList.collegeDispName}"></div>
                  <div class="col_rght fl"><h3 class="tit uni_hd">${opendaysInfoList.collegeDispName} </h3></div>
                </div>
                <div class="mdte_cnt">
                  <div class="mdte_lft fl"><span>${opendaysInfoList.openDate}</span></div>
                  <c:if test="${not empty requestScope.opendayDetailList}">
                  <div class="mdte_rgt fr">
                    <a onclick="opendayDropdown();" id="moreLnk">More dates <i class="fa fa-angle-down"></i></a>
                    <ul id="opendayDropdown">
                    <c:forEach var="opendayDetailList" items="${requestScope.opendayDetailList}">
                       <c:if test="${not empty opendaysInfoList.collegeDispName}">
                          <c:set var="collegeNameDisp" value="${opendaysInfoList.collegeDispName}"/>
                           <%collegeName = common.replaceSpecialCharacter((String)pageContext.getAttribute("collegeNameDisp"));%>
                         </c:if>
                         <c:if test="${opendayDetailList.selectedTxt eq 'Y'}">
                         <%clsName = "active";%>
                         </c:if>
                        <li class="<%=clsName%>"><a onclick="showOpendayAjax('${opendaysInfoList.collegeId}', '${opendayDetailList.eventId}');">${opendayDetailList.opendayDetail}</a></li>
                        <%clsName = "";%>
                      </c:forEach>
                    </ul>
                  </div>
                  </c:if>
                </div>  
                <div class="sus_uni">
                  <h3 class="tit uni_hd">${opendaysInfoList.venue}</h3>
                  <c:if test="${not empty opendaysInfoList.startTime}">
                  <p class="info dte"><span>${opendaysInfoList.startTime}</span><c:if test="${not empty opendaysInfoList.endTime}"> - <span>${opendaysInfoList.endTime}</span></c:if> </p>  
                  </c:if>
                  <h4 class="tit cou_tit">${opendaysInfoList.qualDesc}</h4>
                  <p class="info">${opendaysInfoList.eventDesp}</p>
                </div>
              </div>
              <div class="fl w100p txt_cnr">
              <c:if test="${not empty opendaysInfoList.bookingUrl}">
                  <a id="btnLogin" class="btn1 rgfw" href="${opendaysInfoList.bookingUrl}" target="_blank" onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Reserve A Place', '<%=collegeName%>');addOpenDaysForReservePlace('${opendaysInfoList.openday}', '${opendaysInfoList.monthYear}', '${opendaysInfoList.collegeId}', '${opendaysInfoList.suborderItemId}', '${opendaysInfoList.networkId}'); cpeODReservePlace(this,'${opendaysInfoList.collegeId}','${opendaysInfoList.suborderItemId}','${opendaysInfoList.networkId}','${opendaysInfoList.bookingUrl}');" title="view calendar">Reserve a place</a>
                </c:if>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </c:forEach>
</c:if>