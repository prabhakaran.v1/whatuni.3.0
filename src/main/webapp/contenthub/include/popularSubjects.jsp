<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil,WUI.utilities.GlobalConstants,WUI.utilities.CommonFunction"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<% 
 CommonUtil util = new CommonUtil(); 
 CommonFunction common = new CommonFunction();
 String subClass = "";
 String subjectName = "";
%>

<c:if test="${not empty requestScope.POPULAR_SUBJECTS_LIST}">
    
    <div class="clear"></div>
    
    <section class="popular-course rw20" id="con-for-nav-35">
      <article class="pop_cont">
          
        <h2 class="crs_title">Popular subjects</h2>        
        <ul class="card_list">        
          <c:forEach items="${requestScope.POPULAR_SUBJECTS_LIST}" var="popularSubjectsList">
            <c:if test="${not empty popularSubjectsList.imageName}">
              <c:if test="${popularSubjectsList.imageName eq 'ps_default.png'}">
                <jsp:scriptlet>subClass = "psl2_hov";</jsp:scriptlet>
              </c:if>
              <c:if test="${popularSubjectsList.imageName ne 'ps_default.png'}">
                <jsp:scriptlet>subClass = "";</jsp:scriptlet>
              </c:if>
            </c:if>
            <jsp:scriptlet>subjectName = "";</jsp:scriptlet>
            <li>
              <c:if test="${not empty popularSubjectsList.advertName}">
                  <c:set var="advertName" value="${popularSubjectsList.advertName}"/>
                    <%if((String)pageContext.getAttribute("advertName")!=null){subjectName = common.replaceSpecialCharacter((String)pageContext.getAttribute("advertName"));}%>
              </c:if>
              <c:if test="${empty popularSubjectsList.advertName}">
                  <c:if test="${not empty popularSubjectsList.subjectName}">
                    <c:set var="subName" value="${popularSubjectsList.subjectName}"/>
                    <%if("".equals(subjectName)&& (String)pageContext.getAttribute("subName")!= null){ subjectName = common.replaceSpecialCharacter((String)pageContext.getAttribute("subName"));}%>
                  </c:if>                  
              </c:if>                   
              <a onclick="GAInteractionEventTracking('popularsubjects', 'Popular Subjects', '${requestScope.COLLEGE_NAME_GA}', 'Subject|<%=subjectName%>');" href="${popularSubjectsList.popularSubjectURL}" class="card_pod <%=subClass%>">                                              <%--need to change dynamic--%>     
                <%-- ::START:: wu578_20180711 - Sabapathi: commenting this background image as per CRD  --%>
                <c:if test="${not empty popularSubjectsList.imageName}">
                  <c:set var="imageNameId" value="${popularSubjectsList.imageName}"/>
                  <%String imgPath = CommonUtil.getImgPath("/wu-cont/images/content_hub/popular_subjects/"+(String)pageContext.getAttribute("imageNameId"),0);%>
                  <img src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"                     
                    data-src='<%=util.contentHubDeviceSpecificSectionPath(imgPath,GlobalConstants._322JPG)%>'
                    data-src-ipad='<%=util.contentHubDeviceSpecificSectionPath(imgPath,GlobalConstants._225JPG)%>'
                    data-src-mobile='<%=util.contentHubDeviceSpecificSectionPath(imgPath,GlobalConstants._290JPG)%>'
                    data-src-tab='<%=util.contentHubDeviceSpecificSectionPath(imgPath,GlobalConstants._225JPG)%>'
                    data-img-load-type='lazyLoad'
                    data-one-pix="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"
                    alt="${popularSubjectsList.subjectName}">
                </c:if>
                <%-- ::END:: wu578_20180711 - Sabapathi: commenting this background image as per CRD  --%>
                <div class="title">
                  <h4>${popularSubjectsList.subjectName}</h4>
                </div>
              </a>
            </li>
          </c:forEach>          
        </ul>
        
        <div class="catalog">
          <a onclick="GAInteractionEventTracking('popularsubjects', 'Popular Subjects', '${requestScope.COLLEGE_NAME_GA}', 'View all courses');" href="${requestScope.VIEW_ALL_COURSE_URL}" class="catlog_link">View all courses <span class="fa fa-long-arrow-right" aria-hidden="true"></span> </a>
        </div>
          
      </article>
      <input type="hidden" id="sectionName_35" name="sectionName_35" value="POPULAR SUBJECTS"/>
    </section>
    
</c:if>  