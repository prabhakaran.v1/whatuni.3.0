<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
  CommonUtil util = new CommonUtil();
  String map_url = GlobalConstants.MAP_URL + "&size=1080x768&scale=2&maptype=roadmap&markers=color:red|";
%>
<input type="hidden" id="cumbApiKey" value="<%=GlobalConstants.MAPBOX_API_KEY%>"/>
<input type="hidden" id="cumbScpt" value="<%=GlobalConstants.MAPBOX_SCRIPT%>"/>
<input type="hidden" id="cumbCss" value="<%=GlobalConstants.MAPBOX_CSS%>"/>
<input type="hidden" id="cumbStyle" value="<%=GlobalConstants.MAPBOX_STYLE%>"/><%--Added by Sangeeth.s for FEB_12_19_REL for mapbox--%>
<c:if test="${not empty requestScope.CAMPUSES_VENUE_LIST}">
    <section class="campus row2 rw19" id="con-for-nav-34">
        <div class="campus-main"><h2>Campuses</h2></div>    
          <div id="mySliderTabs" class="clr tabcontent active" >
              <ul class="clr">
                  <c:forEach var="campusList" items="${requestScope.CAMPUSES_VENUE_LIST}" varStatus="row">
                  <c:set var="rowValue" value="${row.index}"/>
                    <li><a data-id="<%=(Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1)%>"  class="tab-slide-img" href="#<%=(Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1)%>image">${campusList.venueName}</a></li>
                  </c:forEach>
              </ul>
              <c:forEach var="campusList" items="${requestScope.CAMPUSES_VENUE_LIST}" varStatus="row">
                <c:set var="rowValue" value="${row.index}"/>
                <c:set var="imgPath" value="${campusList.mediaPath}"/>
                <%--<div id="<bean:write name="campusList" property="venueId" />"><div class="tabimages"><img src="/degrees/img/contenthub/img/slide4.jpg" alt="Campuses imag<%=((Integer)row+1)%>" /></div></div>--%>
                <div id="<%=(Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1)%>image">
                  <div class="tabimages">
                   <div class="gal_head">
                   <p>
                   <c:if test="${not empty campusList.promotionalText}">
                   ${campusList.promotionalText}
                   </c:if>
                   </p>
                   </div>
                    <div id="imageHolder<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>">
                      <img id="imageCampus<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" 
                      data-src="${campusList.mediaPath}" 
                      data-src-ipad='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._768PX)%>'
                      data-src-tab='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._768PX)%>'
                      data-src-mobile='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._320PX)%>'
                      data-img-load-type='lazyLoad'
                      data-one-pix="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"
                      data-media-name="${campusList.mediaName}"
                      class="Campus image" alt="Campuses imag<%=(Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1)%>" />
                    </div>
                     <c:if test="${campusList.onlineFlag ne 'Y'}">
                      <div id="mapHolder<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" class="map_staimg" style="display:none">
                      <div id="addressmap<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" style="display:none;">
                      <p style="font-weight:bold;">
                    <strong>
                        <c:if test="${not empty campusList.addressOne}">
                         ${campusList.addressOne}, 
                        </c:if>
                       <c:if test="${not empty campusList.addressTwo}">
                        ${campusList.addressTwo}, 
                       </c:if>
                      <c:if test="${not empty campusList.countryState}">
                        ${campusList.countryState},  
                      </c:if>   
                       ${campusList.postcode}                         
                     </strong><br/>
                     </p>
                     </div>
                    <%--Changed static google map to mapbox for Feb_12_19 rel by Sangeeth.S--%>
                     <div class="mapbox_cnt">
                       <div id="map<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" class="mbx_canv"></div>
                       <a id='viewGleMap<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>' class='mbxbtn' href="<%=GlobalConstants.MAP_SEARCH_URL%>${campusList.latitude},${campusList.longitude}" target="_blank"><%=GlobalConstants.VIEW_GOOGLE_MAP%></a>
                     </div>
                     <input type="hidden" id="didMapShown<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" name="didMapShown<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" value="N"/>
                     <input type="hidden" id="didMapLoaded<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" name="didMapLoaded<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>" value="N"/>
                     <input type="hidden" id="map<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>Script" name="map<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>Script" value=""/>
                     <div id="evalmap<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>"></div>
                     <script type="text/javascript">
                       $jcub(window).on("scroll", function(e) {       
                         buildMap('<%=Integer.parseInt(pageContext.getAttribute("rowValue").toString())+1%>', '${campusList.latitude}', '${campusList.longitude}');
                       });
                     </script>                     

                    </div>            
                    </c:if>
                  </div>
                </div>
              </c:forEach>
               <input type="hidden" id="currentMap" name="currentMap" value=""/>
          </div>
         <div class="ts_cnt">
             <ul class="tab_sec fadeInUp wow" data-wow-delay=".6s">
                 <li id="image" onclick="showCampusImage();" class="gal-btn active">
                    <span>
                      <svg version="1.1" viewBox="0 0 22 22" xmlns="http://www.w3.org/2000/svg">
                        <g fill="none" fill-rule="evenodd">
                          <g fill="#707070">
                            <path d="m19.75 2.8125c0.33854 0 0.63151 0.1237 0.87891 0.37109 0.2474 0.2474 0.37109 0.54036 0.37109 0.87891v14.375c0 0.33854-0.1237 0.63151-0.37109 0.87891-0.2474 0.2474-0.54036 0.37109-0.87891 0.37109h-17.5c-0.33854 0-0.63151-0.1237-0.87891-0.37109-0.2474-0.2474-0.37109-0.54036-0.37109-0.87891v-14.375c0-0.33854 0.1237-0.63151 0.37109-0.87891s0.54036-0.37109 0.87891-0.37109h17.5zm0 15.625v-1.1523l-0.029297-0.029297c-0.0065104-0.0065105-0.016276-0.0097656-0.029297-0.0097656l-4.1016-4.2383-2.2852 2.0703c-0.14323 0.10417-0.29622 0.15951-0.45898 0.16602-0.16276 0.0065105-0.29622-0.055338-0.40039-0.18555l-5.1758-5.1563-5.0195 4.8633v3.6719h17.5zm0-2.9297v-11.445h-17.5v8.9453l4.5703-4.4336c0.065104-0.065104 0.13672-0.11068 0.21484-0.13672s0.16276-0.039062 0.25391-0.039062c0.078125 0 0.15625 0.019531 0.23438 0.058594s0.14974 0.097656 0.21484 0.17578l5.1562 5.1172 2.3633-2.0508c0.13021-0.10417 0.27018-0.14974 0.41992-0.13672 0.14974 0.013021 0.2832 0.071614 0.40039 0.17578l3.6719 3.7695zm-4.375-6.4453c-0.33854 0-0.63151-0.12044-0.87891-0.36133-0.2474-0.24089-0.37109-0.53711-0.37109-0.88867 0-0.33854 0.1237-0.63151 0.37109-0.87891 0.2474-0.2474 0.54036-0.37109 0.87891-0.37109s0.63151 0.1237 0.87891 0.37109c0.2474 0.2474 0.37109 0.54036 0.37109 0.87891 0 0.35156-0.1237 0.64779-0.37109 0.88867-0.2474 0.24089-0.54036 0.36133-0.87891 0.36133z"/>
                          </g>
                        </g>
                      </svg>
                    Image
                    </span>
                 </li>
                 <li id="map" onclick="showCampusMap();" class="map-btn">
                    <span>
                      <svg version="1.1" viewBox="0 0 22 22" xmlns="http://www.w3.org/2000/svg">
                        <g fill="none" fill-rule="evenodd">
                          <g fill="#707070">
                            <path d="m11.078 1.25c1.1719 0 2.22 0.20182 3.1445 0.60547 0.92448 0.40365 1.7025 0.95377 2.334 1.6504 0.63151 0.69662 1.1133 1.5072 1.4453 2.4316s0.49805 1.901 0.49805 2.9297c0 1.0417-0.20508 2.1061-0.61523 3.1934s-0.92773 2.1549-1.5527 3.2031c-0.625 1.0482-1.3021 2.041-2.0312 2.9785-0.72917 0.9375-1.4128 1.7708-2.0508 2.5 0 0.013021-0.097655 0.10091-0.29297 0.26367-0.19531 0.16276-0.42318 0.24414-0.68359 0.24414h-0.058594c-0.27344 0-0.50456-0.081379-0.69336-0.24414-0.1888-0.16276-0.2832-0.25065-0.2832-0.26367-0.67709-0.78125-1.4095-1.6341-2.1973-2.5586-0.78776-0.92448-1.5169-1.8978-2.1875-2.9199-0.67058-1.0221-1.2305-2.0736-1.6797-3.1543-0.44922-1.0807-0.67383-2.1615-0.67383-3.2422 0-1.0287 0.20182-2.0052 0.60547-2.9297s0.95052-1.735 1.6406-2.4316c0.69011-0.69662 1.4941-1.2467 2.4121-1.6504 0.91797-0.40365 1.8913-0.60547 2.9199-0.60547zm0.23438 18.652c0.98959-1.1328 1.8555-2.2135 2.5977-3.2422 0.74219-1.0287 1.3607-2.0019 1.8555-2.9199 0.49479-0.91797 0.86588-1.7839 1.1133-2.5977 0.2474-0.81381 0.37109-1.5723 0.37109-2.2754 0-0.79427-0.12044-1.5723-0.36133-2.334-0.24089-0.76172-0.61523-1.4421-1.123-2.041s-1.1491-1.0807-1.9238-1.4453c-0.77474-0.36459-1.696-0.54688-2.7637-0.54688-0.85938 0-1.6732 0.16927-2.4414 0.50781s-1.4388 0.79752-2.0117 1.377c-0.57292 0.57943-1.0286 1.2565-1.3672 2.0312-0.33854 0.77474-0.50781 1.5918-0.50781 2.4512 0 0.92448 0.19531 1.862 0.58594 2.8125s0.88541 1.8848 1.4844 2.8027c0.59896 0.91797 1.2565 1.8034 1.9727 2.6562 0.71615 0.85287 1.3932 1.6504 2.0313 2.3926l0.33203 0.37109 0.078125 0.078125c0.013021-0.013021 0.029297-0.026042 0.048828-0.039063 0.019531-0.013021 0.029297-0.026042 0.029297-0.039062zm-0.29297-14.883c1.0417 0 1.9271 0.36783 2.6562 1.1035 0.72917 0.73568 1.0938 1.6178 1.0938 2.6465 0 1.0417-0.36458 1.9271-1.0938 2.6562-0.72917 0.72917-1.6146 1.0938-2.6562 1.0938-1.0287 0-1.9108-0.36458-2.6465-1.0938-0.73568-0.72917-1.1035-1.6146-1.1035-2.6562 0-1.0287 0.36783-1.9108 1.1035-2.6465 0.73568-0.73568 1.6178-1.1035 2.6465-1.1035zm0 6.25c0.69011 0 1.276-0.24739 1.7578-0.74219 0.48177-0.49479 0.72266-1.0872 0.72266-1.7773s-0.24414-1.2793-0.73242-1.7676c-0.48828-0.48828-1.0775-0.73242-1.7676-0.73242s-1.2793 0.24414-1.7676 0.73242c-0.48828 0.48828-0.73242 1.0775-0.73242 1.7676s0.24739 1.2825 0.74219 1.7773c0.49479 0.49479 1.0872 0.74219 1.7773 0.74219z"/>
                          </g>
                        </g>
                      </svg>
                    Map
                    </span>
                 </li>
             </ul>
         </div>
         <input type="hidden" id="sectionName_34" name="sectionName_34" value="CAMPUS"/>
    </section>
    <script type="text/javascript">
      formCampusSlider();
    </script>
</c:if>