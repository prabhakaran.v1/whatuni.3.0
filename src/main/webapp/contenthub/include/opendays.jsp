<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="WUI.utilities.CommonUtil, WUI.utilities.CommonFunction, com.wuni.util.seo.SeoUrls, WUI.utilities.GlobalConstants" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
  CommonFunction common = new CommonFunction();
  String collegeName = request.getAttribute("COLLEGE_NAME") != null && request.getAttribute("COLLEGE_NAME").toString().trim().length() > 0 ? common.replaceSpecialCharacter((String)request.getAttribute("COLLEGE_NAME")).trim() : "0";
  String wp_collegeId = request.getAttribute("COLLEGE_ID") != null && request.getAttribute("COLLEGE_ID").toString().trim().length() > 0 ? (String)request.getAttribute("COLLEGE_ID") : "0";
  String providerOpendayUrl = new SeoUrls().constructOpendaysSeoUrl(collegeName, wp_collegeId);
  CommonUtil util = new CommonUtil();
%>
<c:if test="${not empty requestScope.OPEN_DAY_LIST}">
    <c:forEach var="openDayList" items="${requestScope.OPEN_DAY_LIST}" varStatus="row" end="0">
      <section class="opendays-section clr rw17" id="con-for-nav-32">          <%--not in images --%> 
           <img class="opendays image" src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" 
             data-src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/open_day_generic.jpg", 1)%>"
             data-src-ipad="<%=util.contentHubDeviceSpecificSectionPath(CommonUtil.getImgPath("/wu-cont/images/content_hub/open_day_generic.jpg", 1),GlobalConstants._768PX)%>"
             data-src-tab="<%=util.contentHubDeviceSpecificSectionPath(CommonUtil.getImgPath("/wu-cont/images/content_hub/open_day_generic.jpg", 1),GlobalConstants._768PX)%>" 
             data-src-mobile="<%=util.contentHubDeviceSpecificSectionPath(CommonUtil.getImgPath("/wu-cont/images/content_hub/open_day_generic.jpg", 1),GlobalConstants._768PX)%>" 
             data-img-load-type='lazyLoad'
             data-one-pix="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" alt="Openday Section">
          <%--<img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/open_days_img.png",0)%>" alt="Openday Section">--%>
          <div class="o_left">
              <div class="content">
                  <h1 class="opd_hd"><spring:message code="ch.od.tite.text"/></h1>
                  <a href="javascript:void(0);" class="h_split"></a>
                  <c:if test="${openDayList.eventCategoryId ne '6'}">
                    <p class="o_cal"><i class="fa fa-calendar-o"></i>NEXT OPEN DAY</p>
                    <p class="o_dte">${openDayList.openDate}</p>
                    <p class="o_ugd mt10">${openDayList.qualDesc}</p>
                  </c:if>
					<c:if
						test="${openDayList.eventCategoryId eq '6'}">
						<p class="o_ugd mt10">
							<spring:message code="ch.vt.od.desc" />
						</p>
					</c:if>
					<c:if test="${not empty requestScope.hideOpendaysFlag}">
                    <c:if test="${requestScope.hideOpendaysFlag ne 'Y'}">
                      <c:if test="${not empty requestScope.opendayListSize}">
                        <c:if test="${requestScope.opendayListSize gt 1}">
                          <a class="o_btn clr_blk" 
                            <%-- onclick="GAInteractionEventTracking('visitwebsite', 'engagement', 'Reserve A Place-Request', '<%=collegeName%>');showOpendayPopup('<%=wp_collegeId%>', '');" --%>
                            href="<%=providerOpendayUrl%>">
                            <spring:message code="book.open.day.button.text"/><i class="fa fa-long-arrow-right ml10"></i></a>
                        </c:if>
                        <c:if test="${requestScope.opendayListSize eq 1}">
                          <a class="o_btn clr_blk" target="_blank" href="${openDayList.bookingUrl}" onclick="GANewAnalyticsEventsLogging('open-days-type', '${eventCategoryNameGa}', '<%=collegeName%>');GAInteractionEventTracking('visitwebsite', 'interaction', 'Reserve A Place', '${openDayList.collegeDispName}');addOpenDaysForReservePlace('<c:out value="${openDayList.openday}" escapeXml="false" />', '<c:out value="${openDayList.monthYear}" escapeXml="false" />', '<c:out value="${openDayList.collegeId}" escapeXml="false" />', '${openDayList.suborderItemId}', '${openDayList.networkId}'); cpeODReservePlace(this,'${openDayList.collegeId}','${openDayList.suborderItemId}','${openDayList.networkId}','${openDayList.bookingUrl}');"><spring:message code="book.open.day.button.text"/><i class="fa fa-long-arrow-right ml10"></i></a>
                        </c:if>
                      </c:if>
                    </c:if>
                  </c:if>
                  <a href="javascript:void(0);" class="h_split"></a>
                  <c:if test="${openDayList.eventCategoryId ne '6'}">
                    <p class="o_ugd f_bld">Can’t make this one?</p>
                  </c:if>
                  <a class="o_mor f_bld mt20" href="<%=providerOpendayUrl%>" onclick="GAInteractionEventTracking('visitwebsite', 'More open days', 'Click', '<%=collegeName%>');"><spring:message code="view.all.event.uni"/><%--More open days--%><i class="fa fa-long-arrow-right ml10"></i></a>
              </div>
          </div>
          <input type="hidden" id="sectionName_32" name="sectionName_32" value="OPENDAYS"/>
      </section>
    </c:forEach>
</c:if>
