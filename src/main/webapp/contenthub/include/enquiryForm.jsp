<%@page import="com.wuni.token.TokenFormController"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="org.apache.struts.Globals,WUI.utilities.CommonUtil,WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%  
    CommonFunction common = new CommonFunction();
    String[] yoeArr = common.getYearOfEntryArr();
    String YOE_1 = yoeArr[0];
    String YOE_2 = yoeArr[1];
    String YOE_3 = yoeArr[2];
    String YOE_4 = yoeArr[3];
%>

<section class="form-section clr rw24" id="con-for-nav-37">
   <article class="form-wrap clr">
      <div class="form-cont">
         
		 <div class="top_cont remv_gplus">
            <div class="clr">
               <h2>Got a question?</h2>                                                                     <%--need to change dynamic--%>
               <h3>To: <span class="gtque_img"><img src="${requestScope.COLLEGE_LOGO}" alt="${requestScope.COLLEGE_NAME_DISPLAY}"></span> <span class="gtque_txt">${requestScope.COLLEGE_NAME_DISPLAY}</span></h3>
               <c:if test="${empty sessionScope.userInfoList}">
                 <div class="alhv_ac">Already have an account?<a class="sgntx" title="Sign in"> Sign in</a>
                   <jsp:include page="/jsp/advertiser/include/AlreadyRegisteredPod.jsp" />
                 </div>
               </c:if>
            </div>
         </div>
        
        <div id="success" style="display:none" class="frm_succ_cnt">
            <h2><img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/frm_suc_tik.png", 1)%>"> Your enquiry has been sent.</h2>
            <p>You wil hear from them soon</p>
        </div>
        
        <div id="failed" style="display:none" class="frm_succ_cnt frm_fail">
            <h2><img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/frm_fail.svg", 1)%>"> Your enquiry has been not sent.</h2>
            <p>Sorry! got missed</p>
        </div>
        
        <div id="duplicate" style="display:none" class="frm_succ_cnt frm_fail">
            <h2><img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/frm_fail.svg", 1)%>">You've exceeded the number of times you can contact this provider through Whatuni. Soz.</h2>
            <p>Sorry! got missed</p>
        </div>
        
        <c:if test="${not empty requestScope.USER_DETAILS_LIST}">
                <c:forEach var="userDetailList" items="${requestScope.USER_DETAILS_LIST}" varStatus="row" end="0">
                <div id="enqForm">
                 <form class="profrm fl" id="chEnquiryFormId">
             
                    <fieldset class="textwrap" id="textAreaId">
                       <span class="form-label">Your message</span>
                       <textarea id="message" onblur="limitCharCountEnquiry(this,4000,'charCount');enquiryFormValidationAjax(this, 'enquiryBox');" onfocus="limitCharCountEnquiry(this,4000,'charCount');" onkeyup="limitCharCountEnquiry(this,4000,'charCount');" onkeydown="limitCharCountEnquiry(this,4000,'charCount');">Hello, I read about ${requestScope.COLLEGE_NAME_DISPLAY} on Whatuni.com, and would like to request more information about the institution.</textarea>
                       <div class="enqerr1" id="errorTextArea" style="display: none;"></div>                      
                    </fieldset>
              
                    <span class="form-label btm clr">Your details</span>
                    <fieldset class="fl w49 mb37" id="divfname">
                       <input class="finm iptxt" type="text" name="FirstName" value="${userDetailList.firstName}" maxlength="40" onblur="labelTopOnBlur(this);hideTooltip('firstNameYText');enquiryValidate(this.id);" onfocus="showTooltip('firstNameYText');" id="fname" autocomplete="off">
                       <label for="fname">
                          First name*
                          <name></name>
                       </label>
                       <span class="bul_ttip" id="firstNameYText" style="display: none;">                  
                          <div>We'd like this information so we don't call you by the wrong name. That'd be kinda rude.</div>
                          <span class="ttip_arw"></span>
                       </span>
                       <div class="enqerr1" id="errorfname" style="display: none;"></div>
                    </fieldset>
              
                    <fieldset class="fr w49 mb37" id="divlname">
                       <input class="lanm iptxt" id="lname" type="text" name="LastName" value="${userDetailList.lastName}" maxlength="40" onblur="labelTopOnBlur(this);enquiryValidate(this.id);" autocomplete="off">
                       <label for="lname">Last name* </label>
                       <div class="enqerr1" id="errorlname" style="display: none;"></div>
                    </fieldset>
              
                    <fieldset class="em_add mb37" id="divEmailId">
                     <c:if test="${not empty userDetailList.emailAddress}">
                      <input class="email iptxt" id="email" type="email"  value="${userDetailList.emailAddress}" name="emailAddress" maxlength="120" onfocus="showTooltip('emailYText')" onblur="labelTopOnBlur(this);hideTooltip('emailYText');" onkeydown="hideEmailDropdown(event, 'autoEmailId');" autocomplete="off" disabled="disabled">
                     </c:if>
                      <c:if test="${empty userDetailList.emailAddress}">
                       <input class="email iptxt" id="email" type="email"  value="${userDetailList.emailAddress}" name="emailAddress" maxlength="120" onfocus="showTooltip('emailYText')" onblur="labelTopOnBlur(this);hideTooltip('emailYText');" onkeydown="hideEmailDropdown(event, 'autoEmailId');" autocomplete="off" >
                      </c:if>
                       <label for="email" class="lbco" id="emailId_lbl">Email address*</label>
                       <span id="emailYText" class="bul_ttip" style="display: none;">                  
                          <div>Tell us your email and we'll reward you with...an email.</div>
                          <span class="ttip_arw"></span>
                       </span>
                       <div class="enqerr1" id="errorEmail" style="display:none;"></div>
                       <div class="enqerr1" id="errorEmailExists" style="display:none;"></div>
                    </fieldset>
                    <c:if test="${empty userDetailList.emailAddress}">
                      <fieldset class="mb37" id="divPwd">
                         <input class="finm iptxt" id="password" type="password" name="password" maxlength="20" onblur="labelTopOnBlur(this);enquiryValidate(this.id);" autocomplete="off">                       
                         <label for="password">Password* </label>
                         <span class="pwd_eye"><a onblur="javascript:hideCubPassword('eyeId')"  href="javascript:showCubPassword('eyeId');"><i id="eyeId" class="fa fa-eye" aria-hidden="true"></i></a></span>
                         <div class="enqerr1" id="errorPassword" style="display: none;"></div>
                      </fieldset>
                    </c:if>
                    
                    <fieldset class="fl w49 mb37" id="postcodeField">
                       <input class="finm iptxt" id="postcode" type="text" name="postcode" maxlength="8" value="${userDetailList.postCode}" onblur="labelTopOnBlur(this);enquiryValidate(this.id);enquiryFormValidationAjax(this, 'postcodeBox');" autocomplete="off">
                       <label for="postcode">
                          Postcode
                          <name></name>
                       </label>
                       <div class="enqerr1" id="postcodeErrMsg" style="display: none;"></div>
                    </fieldset>
              
                    <fieldset class="fr w49 pst_code htext-wrap">
                       <a onclick="showAndHideToolTip('postcodeText');" onmouseover="showTooltip('postcodeText');" onmouseout="hideTooltip('postcodeText');"class="helptext">
                          Why do we need your postcode?
                          <span id="postcodeText" class="bul_ttip" style="display: none;">                     
                             <div>We use this information to help assess the reach of our products. This is completely optional.</div>
                             <span class="ttip_arw"></span>
                          </span>
                       </a>
                    </fieldset>
              
                   <input type="text" class="finm iptxt doughnutSummaryTitle" id="beeTrap" autocomplete="off">
              
                    <span class="form-label year_lbl">When would you like to start?*</span>
                    <fieldset class="year-options" id="yoeFeild">
                       <ul>
                          <%String yoe1Checked = "";%>
                          <c:set var="yearOfEntry1" value="<%=YOE_1%>"/>
                          <c:if test="${userDetailList.yeartoJoinCourse eq yearOfEntry1}">
                            <%yoe1Checked = "checked=\"checked\"";%>
                          </c:if>
                          <li>
                             <input id="year1" type="radio" value="<%=YOE_1%>" name="yeartoJoinCourse1" <%=yoe1Checked%>>
                             <label aria-labelledby="<%=YOE_1%>"><%=YOE_1%></label>
                          </li>
                          <%String yoe2Checked = "";%>
                          <c:set var="yearOfEntry2" value="<%=YOE_2%>"></c:set>
                          <c:if test="${userDetailList.yeartoJoinCourse eq yearOfEntry2}">
                            <%yoe2Checked = "checked=\"checked\"";%>
                          </c:if>
                          <li>
                             <input id="year2" type="radio" value="<%=YOE_2%>" name="yeartoJoinCourse1" <%=yoe2Checked%>>
                             <label aria-labelledby="<%=YOE_2%>"><%=YOE_2%></label>
                          </li>
                          <%String yoe3Checked = "";%>
                          <c:set var="yearOfEntry3" value="<%=YOE_3%>"></c:set>
                          <c:if test="${userDetailList.yeartoJoinCourse eq yearOfEntry3}">
                            <%yoe3Checked = "checked=\"checked\"";%>
                          </c:if>
                          <li>
                             <input id="year3" type="radio" value="<%=YOE_3%>" name="yeartoJoinCourse1" <%=yoe3Checked%>>
                             <label aria-labelledby="<%=YOE_3%>"><%=YOE_3%></label>
                          </li>
                          <%String yoe4Checked = "";%>
                            <c:set var="yearOfEntry4" value="<%=YOE_4%>"></c:set>
                          <c:if test="${userDetailList.yeartoJoinCourse eq yearOfEntry4}" >
                            <%yoe4Checked = "checked=\"checked\"";%>
                          </c:if>
                          <li>
                             <input id="year4" type="radio" value="<%=YOE_4%>" name="yeartoJoinCourse1" <%=yoe4Checked%>>
                             <label aria-labelledby="<%=YOE_4%>"><%=YOE_4%></label>
                          </li>
                       </ul>
                       <div class="enqerr1" id="yoeErrMsg" style="display:none;"></div>
                    </fieldset>
                    <!-- Data sharing start -->
                      <c:if test="${not empty requestScope.consentFlag}"> 
                      <c:forEach var="clientLeadConsentList" items="${requestScope.clientLeadConsentList}">
                      <div class="sgnup_chkbx data_share">
                                <h3 class="fnt_lbd fnt20 mb20 mt20">University E-Newsletter Sign Up <span class="fnt_lrg gry_txt">(optional)</span></h3>
                                <p class="tms_txt">
                                    You can now opt in to receive e-newsletters from your favourite universities on Whatuni. Please read the university privacy notice before subscribing. You will need to contact the University directly if you wish to update your mailing preference.
                                </p>
                                <div class="btn_chk"> <span class="chk_btn">
										<!-- <html:checkbox path="consentFlag" id="consentFlag"/> -->
										<input type="checkbox" name="" id="consentFlag"/>
										<span class="chk_mark"></span> </span>
                                    <p>
                                        <label for="consentFlag" class="chkbx_100">${clientLeadConsentList.consentText}</label></p>
                                </div>
                                <div class="borderbot fl mt20 mb40"></div>
                            </div>
                            </c:forEach>
                           <!--  <html:hidden id="consentFlagCheck" path="consentFlagCheck" value="YES"/> -->
                            <input type="hidden" id="consentFlagCheck" value="YES">
                            </c:if>
                    <!-- Data sharing end -->
                 <div class="sgnup_chkbx">
              <h3 class="fnt_lbd fnt20 mb20 mt20">Stay up to date by email <span class="fnt_lrg gry_txt">(optional)</span></h3>
													<div class="btn_chk">
														<span class="chk_btn">
                         <c:if test="${userDetailList.marketingEmail eq 'Y'}">   
                              <input type="checkbox" name="" id="marketingEmailFlag" checked="checked"/>
                         </c:if>
                         <c:if test="${userDetailList.marketingEmail eq 'N'}">
                            <input type="checkbox" name="" id="marketingEmailFlag"/>
                          </c:if>
                              <span class="chk_mark"></span>
                            </span>
														<p><label for="marketingEmailFlag" class="fnt_lbd chkbx_100">Newsletters <span class="cmrk_tclr">(Tick to opt in)</span> </label> <br>
														Emails from us providing you the latest university news, tips and guides</p>
                          </div>
													<div class="btn_chk">
														<span class="chk_btn">
                           <c:if test="${userDetailList.solusEmail eq 'Y'}">   
                            <input type="checkbox" name="" id="solusEmailFlag" checked="checked"/>
                            </c:if>
                            <c:if test="${userDetailList.solusEmail eq 'N'}">   
                            <input type="checkbox" name="" id="solusEmailFlag"/>
                            </c:if>
                              <span class="chk_mark"></span>
                            </span>
														<p> <label for="solusEmailFlag" class="fnt_lbd chkbx_100"> University updates <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                            <spring:message code="wuni.solus.flag.text"/></p></p>
                          </div>
													<div class="btn_chk">
														<span class="chk_btn">
                            <c:if test="${userDetailList.surveyEmail eq 'Y'}">   
                             <input type="checkbox" name="" id="surveyEmailFlag" checked="checked"/>
                             </c:if>
                             <c:if test="${userDetailList.surveyEmail eq 'N'}">   
                             <input type="checkbox" name="" id="surveyEmailFlag"/>
                             </c:if>
                              <span class="chk_mark"></span>
                            </span>
														<p><label for="surveyEmailFlag" class="fnt_lbd chkbx_100">Surveys <span class="cmrk_tclr">(Tick to opt in)</span></label> <br>
                            <spring:message code="wuni.survey.flag.text"/></p></p>
                          </div>
												<div class="borderbot fl mt20 mb40"></div>
              
                     </div>                                                             
                                 
                    <fieldset id="termsFld" class="acpttext">
                    
                        <fieldset class="mt0 rgfm-lt">                            
                              <span class="chk_btn">
                                <input type="checkbox" value="" class="chkbx1" id="chEnquiryFormProceed"/>
                                <span class="chk_mark"></span>
                              </span>
                              <label for="chEnquiryFormProceed" id="normalsignup">
																               <span class="rem1">I confirm I'm over 13 and agree to the <a href="javascript:void(0);" onclick="showTermsConditionPopup()" class="link_blue" title="terms and conditions">terms and conditions</a> and <a href="javascript:void(0);" onclick="showPrivacyPolicyPopup()" class="link_blue" title="privacy notice">privacy notice</a>, and to become a member of the Whatuni community.<span class="as_trk">*</span> </span>
                              </label>  
                               <p class="qler1" id="ch_termsc_error" style="display:none;"></p>
                               <p class="qler1" id="beeTrapError" style="display:none;"></p>
                            </fieldset>
                    
                    
                       
                       <div class="sendmail"> 
                          <a type="button" class="frmsub clr" onclick="return validateInteractionFormContenthub();" id="sendEmail">
                            <div class="frm_txt">REQUEST INFO</div>
                            <div class="frm_ldr" id="loader" style="display:none">
                            <img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/hm_ldr.gif",1)%>" alt="loader"/>
                            </div>
                          </a>
                       </div>
                       
                    </fieldset>
                    
                    <input type="hidden" id="tokenValid" name="<%=TokenFormController.getTokenKey()%>" value="<%= session.getAttribute(TokenFormController.getTokenKey()) %>" >
                    <input type="hidden" id="emailAlreadyExistsflag" value="">
                 </form>
                </div>
              </c:forEach>
        </c:if>
        <input type="hidden" id="isEngagementDone" name="isEngagementDone" value="N"/>
      </div>
   </article>
   <input type="hidden" id="sectionName_37" name="sectionName_37" value="OPEN ENQUIRY FORM"/>
   <input type="hidden" id="enquiryMessageErrHidden" value="false"/>
   <input type="hidden" id="postCodeErrHidden" value="false"/>
</section>

<script type="text/javascript">
     jQuery(document).ready(function() {
       var arr = ["email", "hideTooltip('emailYText');", "addFltCls('email', 'emailId_lbl');", "clearErrorMsg('divEmailId','errorEmail');"];
       jQuery("#email").autoEmail(arr);
     });
     clearFieldsWhenLoggedOut();
</script>