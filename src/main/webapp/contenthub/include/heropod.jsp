<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.wuni.util.seo.SeoUrls, WUI.utilities.CommonUtil, WUI.utilities.GlobalConstants, WUI.utilities.CommonFunction, com.wuni.util.seo.ContentHubUrls" %>

<%
  CommonFunction common = new CommonFunction();
  CommonUtil util = new CommonUtil();  
  ContentHubUrls url = new ContentHubUrls();
  String basketCollegeName = request.getAttribute("COLLEGE_NAME") != null && request.getAttribute("COLLEGE_NAME").toString().trim().length() > 0 ? common.replaceSpecialCharacter((String)request.getAttribute("COLLEGE_NAME")).trim() : "0";
  String wp_collegeId = request.getAttribute("COLLEGE_ID") != null && request.getAttribute("COLLEGE_ID").toString().trim().length() > 0 ? (String)request.getAttribute("COLLEGE_ID") : "0";
  String wp_collegeName = request.getAttribute("COLLEGE_NAME") != null && request.getAttribute("COLLEGE_NAME").toString().trim().length() > 0 ? (String)request.getAttribute("COLLEGE_NAME") : "0";
  String coureId = "0";
  String clearingyear   = GlobalConstants.CLEARING_YEAR;
  String requestURL = (String)request.getAttribute("REQUEST_URI");
  requestURL = requestURL.indexOf("/degrees") > -1 ? requestURL.replace("/degrees", "") : requestURL;
  requestURL = requestURL.indexOf(".html") > -1 ? requestURL.replace(".html", "/") : requestURL;    
  String homeUrl = url.getHomeUrl();
  String findUniUrl = url.getFindUniUrl();
  String latCollegeName = common.replaceURL(common.getCollegeName(wp_collegeId, request)).toLowerCase();
  String clearingURL = new SeoUrls().constructCourseUrl("all", "CLEARING", latCollegeName);
  String userJourneyFlag = (String)request.getAttribute("USER_JOURNEY_FLAG");
  String mobileMenuText = "NEXT STEPS";
  if("CLEARING".equalsIgnoreCase(userJourneyFlag)){
    mobileMenuText = "APPLY";
  }
  SeoUrls seoUrl = new SeoUrls();
%>

<section class="hero-section rw1" id="con-for-nav-0">
  
  <c:if test="${not empty requestScope.GALLERY_LIST}">
      <div class="overlay" onclick="playAndPause('background-video', event, 'overlay_div')"></div>
      <c:forEach var="galleryList" items="${requestScope.GALLERY_LIST}" varStatus="row"  end="0">
        <c:set var="imgPath" value="${galleryList.mediaPath}"/>
        <c:if test="${galleryList.mediaTypeId ne 30}">
          <div class="hero_img" style="display: block;">
            <img id="heroPodImg"  src="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"
            data-src="${galleryList.mediaPath}"                                                                                 
            data-src-ipad='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._768PX)%>'
            data-src-tab='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._768PX)%>'
            data-src-mobile='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPath"),GlobalConstants._320PX)%>'
            data-img-load-type='lazyLoad'
            data-one-pix="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>"
            data-media-name="${galleryList.mediaName}"
            alt="Hero image">
          </div>
            <script type="text/javascript">  
                  jQuery(document).ready(function() {
                    //
                    lazyLoadHeroImage();
                    //
                    ga('set', 'dimension12', ($$D('sectionName_0').value).toLowerCase());
                    sectionWiseLoggingMap['section0_SectionDimension'] = "Y";                   
                    //
                    imageImpressionViewOnGA($$D('heroPodImg').getAttribute('data-media-name'));
                    //
                    sectionsDBStatsLog(0);
                    sectionWiseLoggingMap['section0_SectionCont'] = "Y";
                  });
            </script>
            <input type="hidden" id="sectionEmbed_0" name="sectionEmbed_0" value="CH_HERO_IMAGE"/>
            <div class="scr_mov"><a class="next"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/arrow.svg",0)%>" width="30" height="30" alt="arrow_icon"></a></div>
        </c:if>
        <c:if test="${galleryList.mediaTypeId eq 30}">
          <div id="hero_video" class="hm_vid_cont vid-content">
            <c:set var="imgPathHero" value="${galleryList.thumbNailPath}"/>
            <div id="bg_hero" class="vid_bgg">
               <div class="play-icon" onclick="playAndPause('background-video', event, 'overlay_div')"> <img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/play-icon.png",0)%>" alt="hero image play icon" style="display: block;"> </div>
               <img id="heroVideoImg" src='<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>'
               data-src='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPathHero"),GlobalConstants._768PX)%>'
               data-src-ipad='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPathHero"),GlobalConstants._768PX)%>'
               data-src-mobile='<%=util.contentHubDeviceSpecificSectionPath((String)pageContext.getAttribute("imgPathHero"),GlobalConstants._320PX)%>'
               data-img-load-type='lazyLoad'
               data-one-pix="<%=CommonUtil.getImgPath("/wu-cont/images/img_px.gif", 1)%>" alt="hero image"/>
            </div>
            <div id="wapper_hero_video" class="home-header__player" style="display:none">              
              <video preload="none" onclick="playAndPause('background-video',event, '')" onpause="updateVideoMapOnPlayPause(this, 'pause', event);hideAndShowPlayIcon('con-for-nav-0', 'pause')" onplay="updateVideoMapOnPlayPause(this, 'play', event);hideAndShowPlayIcon('con-for-nav-0', 'play')" id="background-video" class="heropod" controlslist="nodownload nofullscreen" controls="" playsinline="" data-media-name="${galleryList.mediaName}">
                <source src="${galleryList.mediaPath}" type="video/mp4">               
              </video>
            </div>
            <script type="text/javascript">  
                  jQuery(document).ready(function() {
                    //
                    lazyLoadHeroImage();
                    //
                    ga('set', 'dimension12', ($$D('sectionName_0').value).toLowerCase());
                    sectionWiseLoggingMap['section0_SectionDimension'] = "Y";                   
                    //
                    sectionsDBStatsLog(0);
                    sectionWiseLoggingMap['section0_SectionCont'] = "Y";
                  });
            </script>
            <input type="hidden" id="sectionEmbed_0" name="sectionEmbed_0" value="CH_HERO_VIDEO"/>
          </div>
          <div class="scr_mov"><a class="next"><img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/arrow.svg",0)%>" width="30" height="30" alt="arrow icon"></a></div>
        </c:if>
       </c:forEach>
   </c:if>
  <div class="content abrd" onclick="playAndPause('background-video', event, 'overlay_div')">
      <div class="pag_cnt">
          <ul>
              <li><a href="<%=homeUrl%>"><i class="fa fa-home fa-1_5x"></i></a></li>
              <li>/</li>
              <li><a href="<%=findUniUrl%>">Find a uni</a></li>
              <li>/</li>
              <li>${requestScope.COLLEGE_NAME_DISPLAY}</li>
          </ul>
      </div>
      <div class="rat_sec lft fadeIn wow" data-wow-delay=".6s">                                   <%--uni_log.png not in local images(dynamic) --%>
          <div class="logo"><img src="${requestScope.COLLEGE_LOGO}" alt="Provider logo"></div>
          <div class="rat_rht">
              <h1>${requestScope.COLLEGE_NAME_DISPLAY}</h1>
              <c:if test="${requestScope.TOTAL_REVIEW_COUNT ne 0}">
              <div class="ratingcnt">
                  <div class="rat_cnt">
                      <div class="rat_wrp">
                        <c:set var="starCounts" value="${requestScope.OVERALL_RATING}"/>                                  
                        <%for(int lp=1;lp<=5;lp++){
                            if( lp <= Integer.parseInt(pageContext.getAttribute("starCounts").toString())){%>
                            <i class="fa fa-star fa-2" aria-hidden="true"></i>
                          <%}else{%>                              
                            <i class="fa fa-star-o fa-2" aria-hidden="true"></i>
                          <%}                              
                          }%>   
                      </div>
                      <span> 
                        (${requestScope.ABSOLUTE_RATING}) 
                        <c:if test="${not empty requestScope.TOTAL_REVIEW_COUNT}">
                          <c:if test="${requestScope.TOTAL_REVIEW_COUNT ne 0}">
                            <a id="rvw_skip_link" class="vr_bold" data-rvw-ga="${requestScope.COLLEGE_NAME_DISPLAY}" title="Reviews at ${requestScope.COLLEGE_NAME_DISPLAY}"><spring:message code="view.review.link.name"/></a>
                          </c:if>
                        </c:if>
                      </span>
                  </div>    
              </div>
              </c:if>
                <c:if test="${requestScope.USER_JOURNEY_FLAG ne 'CLEARING'}">
                  <jsp:include page="/jsp/advertiser/ip/include/headShortlist.jsp">
                    <jsp:param name="richProfile" value="false"/>
                    <jsp:param name="fromPage" value="content-hub"/>
                    <jsp:param name="basketCollegeName" value="<%=basketCollegeName%>"/>
                  </jsp:include>
                  <div class="rat_sec rht">
                    <ul>
                      <li><a href="${requestScope.VIEW_ALL_COURSE_URL}" onclick="GAInteractionEventTracking('viewcourses', 'View courses', 'Click', '<%=basketCollegeName%>');">View courses <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></li>
                    </ul>
                  </div>
                </c:if>
              
              
          </div>
      </div>
  </div>
  <div class="ibtn_sec">
    <div class="ibtn_lft fl">                                                   <%--uni_log.png not in local images(dynamic) --%>
        <div class="ibtn_lgo fl"><img src="${requestScope.COLLEGE_LOGO}" alt="University logo"></div>
        <div class="iblgo_rht fl" title="${requestScope.COLLEGE_NAME_DISPLAY}">${requestScope.COLLEGE_NAME_DISPLAY}</div>
        <ul class="fr nxt_stps">
            <li class="sti_mnu"><a><i class="fa fa-bars"></i> MENU</a></li>
            <li><a><%=mobileMenuText%></a></li>
        </ul>
    </div>
    <c:if test="${not empty requestScope.ENQUIRY_INFO_LIST}">
        <c:forEach var="enquiryInfoList" items="${requestScope.ENQUIRY_INFO_LIST}" end="0">
          <div class="ibtn_rht fr">
            <div class="nxt_cls"><a><img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/ft_clsicn.png",0)%>" alt="Interaction button close icon"> </a></div>
            <ul class="fr">        
                  <c:if test="${requestScope.USER_JOURNEY_FLAG ne 'CLEARING'}">
                  
	            <c:if test="${enquiryInfoList.websiteFlag eq 'Y'}">
                <li><a class="visit_web" target="_blank" onclick="sponsoredListGALogging(${COLLEGE_ID});GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=basketCollegeName%>', '');addBasket('<%=wp_collegeId%>', 'C', 'visitweb','basket_div_<%=wp_collegeId%>', 'basket_pop_div_<%=wp_collegeId%>', '', '', '<%=basketCollegeName%>'); cpeWebClickWithCourse(this,'<%=wp_collegeId%>',0,'${enquiryInfoList.subOrderItemId}','${enquiryInfoList.networkId}','${enquiryInfoList.website}');" href="${enquiryInfoList.website}">VISIT WEBSITE</a></li>
              </c:if>
              <%--Checking hideOpendaysFlag for showing opendays button in contenthub page by Prabha on 31_July_2018--%>
              <c:if test="${not empty requestScope.OPEN_DAY_LIST}">
                  <c:if test="${not empty requestScope.hideOpendaysFlag}">
                    <c:if test="${requestScope.hideOpendaysFlag ne 'Y'}">
                      <c:if test="${not empty requestScope.opendayListSize}">
                        <c:forEach var="openDayList" items="${requestScope.OPEN_DAY_LIST}" varStatus="row" end="0">
                          <c:if test="${requestScope.opendayListSize gt 1}">
                            <li>
                              <a class="bk_opndy" 
                                <%-- onclick="sponsoredListGALogging(${COLLEGE_ID});GAInteractionEventTracking('visitwebsite', 'engagement', 'Book Open Day Request', '<%=basketCollegeName%>');showOpendayPopup('<%=wp_collegeId%>', '');" --%>
                               href="<%=seoUrl.constructOpendaysSeoUrl(wp_collegeName, wp_collegeId)%>">
                               <spring:message code="book.open.day.button.text"/></a>
                            </li>
                          </c:if>
                          <c:if test="${requestScope.opendayListSize eq 1}">
                            <li><a class="bk_opndy" target="_blank" href="${openDayList.bookingUrl}" onclick="GANewAnalyticsEventsLogging('open-days-type', '${eventCategoryNameGa}', '<%=basketCollegeName%>');sponsoredListGALogging(${COLLEGE_ID});GAInteractionEventTracking('visitwebsite', 'interaction', 'Reserve A Place', '<%=basketCollegeName%>');addOpenDaysForReservePlace('<c:out value="${openDayList.openday}" escapeXml="false" />', '<c:out value="${openDayList.monthYear}" escapeXml="false" />', '<c:out value="${openDayList.collegeId}" escapeXml="false" />', '${openDayList.suborderItemId}', '${openDayList.networkId}'); cpeODReservePlace(this,'${openDayList.collegeId}','${openDayList.suborderItemId}','${openDayList.networkId}','${openDayList.bookingUrl}');"><spring:message code="book.open.day.button.text"/></a></li>
                          </c:if>
                        </c:forEach>
                      </c:if>
                    </c:if>
                  </c:if>
              </c:if>
	      <%--End of opendays button code--%>
              <c:if test="${enquiryInfoList.prospectusFlag eq 'Y'}">
                <c:if test="${not empty enquiryInfoList.prospectus and empty enquiryInfoList.prospectusWebform}">
                    <li><a class="get_pros" onclick="sponsoredListGALogging(${COLLEGE_ID});GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request', '<%=basketCollegeName%>');return prospectusRedirect('/degrees','<%=wp_collegeId%>','<%=coureId%>','','','${enquiryInfoList.subOrderItemId}');">GET PROSPECTUS</a></li>
                </c:if>
                
                <c:if test="${not empty enquiryInfoList.prospectusWebform}">
                  <li><a class="get_pros" target="_blank" onclick="sponsoredListGALogging(${COLLEGE_ID});GAInteractionEventTracking('prospectuswebform', 'interaction', 'prospectus webform', '<%=basketCollegeName%>', '');cpeProspectusWebformClick(this,'<%=wp_collegeId%>','${enquiryInfoList.subOrderItemId}','${enquiryInfoList.networkId}','${enquiryInfoList.prospectusWebform}');" href="${enquiryInfoList.prospectusWebform}">GET PROSPECTUS</a></li>
                </c:if>
              </c:if>
	      
              <c:if test="${enquiryInfoList.emailFlag eq 'Y'}">
                <c:if test="${not empty enquiryInfoList.email and empty enquiryInfoList.emailWebform}">
                    <li><a class="req_info" onclick="sponsoredListGALogging(${COLLEGE_ID});GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '<%=basketCollegeName%>');dimLogReqAndPros();" id="requestInfoId" href="<SEO:SEOURL pageTitle="sendcollegemail" ><%=wp_collegeName%>#<%=wp_collegeId%>#<%=coureId%>#0#n#${enquiryInfoList.subOrderItemId}</SEO:SEOURL>">REQUEST INFO</a></li>
                </c:if>
                
                <c:if test="${not empty enquiryInfoList.emailWebform}">
                  <li><a class="req_info" target="_blank" onclick="sponsoredListGALogging(${COLLEGE_ID});GAInteractionEventTracking('emailwebform', 'interaction', 'email webform', '<%=basketCollegeName%>', '');cpeEmailWebformClick(this,'<%=wp_collegeId%>','${enquiryInfoList.subOrderItemId}','${enquiryInfoList.networkId}','${enquiryInfoList.emailWebform}');" href="${enquiryInfoList.emailWebform}">REQUEST INFO</a></li>
                </c:if>
              </c:if>
             </c:if>
             <c:if test="${not empty requestScope.CLEARING_COURSE_EXIST_FLAG }">
               <c:if test="${requestScope.CLEARING_COURSE_EXIST_FLAG eq 'Y' }">
                 <li><a class="visit_web" href="<%=clearingURL%>" title="CLEARING COURSES">CLEARING COURSES</a></li>
               </c:if>
             </c:if> 
             <c:if test="${not empty requestScope.USER_JOURNEY_FLAG }">
               <c:if test="${requestScope.USER_JOURNEY_FLAG eq 'CLEARING'}">
                  <c:if test="${not empty enquiryInfoList.subOrderItemId }">
                    <c:if test="${enquiryInfoList.subOrderItemId gt 0 }">
                      <c:if test="${enquiryInfoList.websiteFlag eq 'Y'}">
                        <li><a class="req_info" target="_blank" onclick="GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', '<%=basketCollegeName%>', <c:out value="${enquiryInfoList.websitePrice}" escapeXml="false" />);                    
                        cpeWebClickClearing(this,'<%=wp_collegeId%>','<c:out value="${enquiryInfoList.subOrderItemId}" escapeXml="false" />','<c:out value="${enquiryInfoList.networkId}" escapeXml="false" />','<c:out value="${enquiryInfoList.website}" escapeXml="false" />');"
                        href="/degrees/visitwebredirect.html?id=<c:out value="${enquiryInfoList.subOrderItemId}" escapeXml="false" />"
                        title="Visit <%=wp_collegeName%> website">VISIT WEBSITE</a></li>
                      </c:if>
                      
                      <c:if test="${not empty enquiryInfoList.hotline }">
                      <li><a class="get_pros" id="hotlineTextId" title="Call now <%=wp_collegeName%>" 
                        onclick="callHotline('${enquiryInfoList.hotline}','hotlineTextId'); 
                        GAInteractionEventTracking('Webclick', 'interaction', 'hotline', '<%=basketCollegeName%>');
                        cpeHotlineClearing(this,'<%=wp_collegeId%>','<c:out value="${enquiryInfoList.subOrderItemId}" escapeXml="false" />','<c:out value="${enquiryInfoList.networkId}" escapeXml="false" />','<c:out value="${enquiryInfoList.hotline}" escapeXml="false" />');">
                        <i class="fa fa-phone"></i> CALL NOW</a></li>
                        <input id="hotlineNoId" type="hidden" value="${enquiryInfoList.hotline}">
                      </c:if>
                    </c:if>
                  </c:if>
               </c:if>
             </c:if>
             
              
            </ul>
          </div>
        </c:forEach>
    </c:if>
    <c:if test="${empty requestScope.ENQUIRY_INFO_LIST}">
      <c:if test="${not empty requestScope.CLEARING_COURSE_EXIST_FLAG }">
        <c:if test="${requestScope.CLEARING_COURSE_EXIST_FLAG eq 'Y' }">
          <div class="ibtn_rht fr">
              <div class="nxt_cls"><a><img src="<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/ft_clsicn.png",0)%>" alt="Interaction button close icon"> </a></div>
              <ul class="fr">
                <li><a class="visit_web" href="<%=clearingURL%>" title="CLEARING COURSES">CLEARING COURSES</a></li>
              </ul>
            </div>
        </c:if>
      </c:if>
    </c:if>
    
    
  </div>
  <input type="hidden" id="sectionName_0" name="sectionName_0" value="HOME"/>
</section>
<input type="hidden" id="switchProvUrl" value="<%=requestURL%>"/>