<%@ taglib uri="/WEB-INF/tlds/SEO_URL_TLD.tld" prefix="SEO"%>
<%@ page import="WUI.utilities.SessionData" autoFlush="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="WUI.utilities.CommonUtil, com.wuni.util.seo.SeoUrls, com.wuni.util.seo.SeoPageNamesAndFlags, org.apache.commons.validator.GenericValidator,WUI.utilities.SessionData, WUI.utilities.CommonFunction, WUI.utilities.GlobalConstants"%>
<%
String collegeId = request.getAttribute("COLLEGE_ID") != null ? (String)request.getAttribute("COLLEGE_ID") : "";
String requestUrl = request.getAttribute("REQUEST_URL") != null ? (String)request.getAttribute("REQUEST_URL") : "";
String collegeName = request.getAttribute("COLLEGE_NAME") != null ? (String)request.getAttribute("COLLEGE_NAME") : "";
String canonicalUrl = GlobalConstants.WHATUNI_SCHEME_NAME+GlobalConstants.WHATUNI_DOMAIN+ new SeoUrls().construnctUniHomeURL(collegeId, collegeName);
String enquiryExistFlag = request.getAttribute("enquiryExistFlag") != null ? (String)request.getAttribute("enquiryExistFlag") : "";
String exitOpenDayRes = "" , odEvntAction = "false";
    if(session.getAttribute("exitOpRes")!=null && !"".equals(session.getAttribute("exitOpRes"))){
      exitOpenDayRes = (String)session.getAttribute("exitOpRes");
    }    
    if(!"".equals(exitOpenDayRes) && "true".equals(exitOpenDayRes)){
      odEvntAction = "true";
    }
    String ctrClrClassName = "";
    String seoPageName = SeoPageNamesAndFlags.UNI_LANDING_PAGE_NAME;
    String seoParamFlag = SeoPageNamesAndFlags.UNI_LANDING_PAGE_FLAG;
    String clearingFlag = request.getAttribute("USER_JOURNEY_FLAG") != null ? (String)request.getAttribute("USER_JOURNEY_FLAG") : "";   
      if("CLEARING".equalsIgnoreCase(clearingFlag)){
        ctrClrClassName = "clrch-bt";

      }
    String orderItemId = !GenericValidator.isBlankOrNull((String) request.getAttribute("ORDER_ITEM_ID")) ? (String)request.getAttribute("ORDER_ITEM_ID") : "";
    String subOrderItemId = !GenericValidator.isBlankOrNull((String) request.getAttribute("SUBORDER_ITEM_ID")) ? (String)request.getAttribute("SUBORDER_ITEM_ID") : "";
    String myhcProfileId = !GenericValidator.isBlankOrNull((String) request.getAttribute("MYHC_PROFILE_ID")) ? (String)request.getAttribute("MYHC_PROFILE_ID") : "";
    String profileType = !GenericValidator.isBlankOrNull((String) request.getAttribute("PROFILE_TYPE")) ? (String)request.getAttribute("PROFILE_TYPE") : "";
%>
  <body onload="lazyLoadingCub();">
      <%
        String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId"); 
        cpeQualificationNetworkId = GenericValidator.isBlankOrNull((String) request.getAttribute("collegeName")) ? "2": cpeQualificationNetworkId;
        String widgetId = new CommonFunction().getWUSysVarValue("WU_EMAIL_WIDGETID");
      %>
      <c:if test="${not empty requestScope.pixelTrackingCode}">
          ${requestScope.pixelTrackingCode}
       </c:if>
    <header class="md clipart">
      <div class="ad_cnr">
        <div class="content-bg">
          <div id="desktop_hdr">
            <jsp:include page="/jsp/common/wuHeader.jsp"/>
          </div>
        </div>
      </div>
    </header><div style="display:none;">
      <jsp:include page="/seopods/breadCrumbs.jsp">
        <jsp:param name="reviewProfilePage" value="st_fix pros_brcumb"/>
      </jsp:include>
    </div><section class="cont_fluid uni_mnu">
      <article class="col_rgt">
        <div class="nxt_cls">
          <a href="javascript:void(0)">
            <!--<i class="fa fa-times"></i>-->
            <img src='<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/ns_clsicn.png",0)%>'
                 alt="close uni info menu"></img>
          </a>
        </div>
        <div class="richrght_cnt">
          <ul class="rghtcont-list">
            <c:if test="${not empty requestScope.SECTIONS_LIST}">
                <c:forEach var="sectionsList" items="${requestScope.SECTIONS_LIST}" varStatus="row">
                <c:set var="rowValue" value="${row.index}"/>
                  <li data='nav-<%=(Integer.parseInt(pageContext.getAttribute("rowValue").toString())) + 1%>'>
                    <a href="javascript:void(0)">
                      ${sectionsList.displaySectionName}  
                    </a>
                  </li>
                </c:forEach>
            </c:if>
            <c:if test="${not empty requestScope.UNI_STATS_LIST}">
                <c:if test="${requestScope.ALL_STATS_EMPTY_FLAG ne 'Y'}">
                  <li data="nav-31">
                    <a href="javascript:void(0)">Key stats</a>
                  </li>
                </c:if>
            </c:if>
            <c:if test="${not empty requestScope.OPEN_DAY_LIST}">
                <%-- <li data="nav-32"><a href="javascript:void(0)">Open
                     days</a></li>--%>
            </c:if>
            <c:if test="${not empty requestScope.TOTAL_REVIEW_COUNT}">
                <c:if test="${requestScope.TOTAL_REVIEW_COUNT ne 0}">
                  <li data="nav-33">
                    <a href="javascript:void(0)">Student reviews</a>
                  </li>
                </c:if>
            </c:if>
            <c:if test="${not empty requestScope.CAMPUSES_VENUE_LIST}">
                <li data="nav-34">
                  <a href="javascript:void(0)">Campuses</a>
                </li>
            </c:if>
            <c:if test="${not empty requestScope.POPULAR_SUBJECTS_LIST}">
                <li data="nav-35">
                  <a href="javascript:void(0)">Popular subjects</a>
                </li>
            </c:if>
            <c:if test="${not empty requestScope.GALLERY_LIST}">
                <c:if test="${requestScope.GALLERY_FLAG eq 'Y'}">
                  <li data="nav-36">
                    <a href="javascript:void(0)">
                      Life at 
                      ${requsetScope.COLLEGE_NAME_DISPLAY}
                    </a>
                  </li>
                </c:if>
            </c:if>
            <c:if test="${not empty requestScope.EMAIL_FLAG}">
              <c:if test="${requestScope.EMAIL_WEBFORM_FLAG eq 'N'}">
                <li data="nav-37">
                  <a href="javascript:void(0)">Got a question?</a>
                </li>
              </c:if>
            </c:if>
            <%-- <logic:present name="SOCIAL_NETWORK_URL_LIST" scope="request">
                 <logic:notEmpty name="SOCIAL_NETWORK_URL_LIST" scope="request">
                 <li data="nav-38"><a href="javascript:void(0)">Social
                 media</a></li> </logic:notEmpty> </logic:present>--%>
          </ul>
        </div>
      </article>
    </section><ul class="nav-dots atz" id="atz">
      <c:if test="${not empty requestScope.SECTIONS_LIST}">
          <c:forEach var="sectionsList" items="${requestScope.SECTIONS_LIST}" varStatus="row">
          <c:set var="rowValue" value="${row.index}"/>
            <li id='nav-<%=(Integer.parseInt(pageContext.getAttribute("rowValue").toString())) + 1%>'>
              <span class="bul_ttip">
                ${sectionsList.displaySectionName}        
              </span>
            </li>
          </c:forEach>
      </c:if>
      <c:if test="${not empty requestScope.UNI_STATS_LIST}">
          <c:if test="${requestScope.ALL_STATS_EMPTY_FLAG ne 'Y'}">
            <li id="nav-31">
              <span class="bul_ttip">Key stats</span>
            </li>
          </c:if>
      </c:if>
      <c:if test="${not empty requestScope.OPEN_DAY_LIST}">
          <li id="nav-32">
            <span class="bul_ttip">Virtual tours</span>
          </li>
      </c:if>
       <c:if test="${not empty requestScope.TOTAL_REVIEW_COUNT}">
          <c:if test="${requestScope.TOTAL_REVIEW_COUNT ne 0}">
            <li id="nav-33">
              <span class="bul_ttip">Student reviews</span>
            </li>
          </c:if>
      </c:if>
      <c:if test="${not empty requestScope.CAMPUSES_VENUE_LIST}">
          <li id="nav-34">
            <span class="bul_ttip">Campuses</span>
          </li>
      </c:if>
       <c:if test="${not empty requestScope.POPULAR_SUBJECTS_LIST}">
          <li id="nav-35">
            <span class="bul_ttip">Popular subjects</span>
          </li>
      </c:if>
      <c:if test="${not empty requestScope.GALLERY_LIST}">
          <c:if test="${requestScope.GALLERY_FLAG eq 'Y'}">
            <li id="nav-36">
              <span class="bul_ttip">
                Life at 
                ${requestScope.COLLEGE_NAME_DISPLAY}
              </span>
            </li>
          </c:if>
      </c:if>
      <c:if test="${not empty requestScope.EMAIL_FLAG and requestScope.EMAIL_FLAG eq 'Y' and requestScope.EMAIL_WEBFORM_FLAG eq 'N' }">
            <li id="nav-37">
              <span class="bul_ttip">Got a question?</span>
            </li>
      </c:if>
      <%-- <logic:present name="SOCIAL_NETWORK_URL_LIST" scope="request">
           <logic:notEmpty name="SOCIAL_NETWORK_URL_LIST" scope="request"> <li
           id="nav-38"><span class="bul_ttip">Social media</span></li>
           </logic:notEmpty> </logic:present>--%>
    </ul><div class="container1 <%=ctrClrClassName%>">
      <jsp:include page="/contenthub/include/heropod.jsp"/>
       
      <jsp:include page="/contenthub/include/sections.jsp"/>
       
      <jsp:include page="/contenthub/include/keystats.jsp"/>
       
      <jsp:include page="/contenthub/include/opendays.jsp"/>
       
      <jsp:include page="/contenthub/include/reviewPod.jsp"/>
       
      <jsp:include page="/contenthub/include/campus.jsp"/>
       
      <jsp:include page="/contenthub/include/popularSubjects.jsp"/>
       
      <jsp:include page="/contenthub/include/gallery.jsp"/>
        <c:if test="${requestScope.USER_JOURNEY_FLAG ne 'CLEARING'}">
          <c:if test="${not empty requestScope.EMAIL_FLAG and requestScope.EMAIL_FLAG eq 'Y' and requestScope.EMAIL_WEBFORM_FLAG eq 'N'}">
            <jsp:include page="/contenthub/include/enquiryForm.jsp"/>
          </c:if>
        </c:if>
      <%-- <jsp:include page="/contenthub/include/social.jsp" />--%>
       
      <jsp:include page="/jsp/common/wuFooter.jsp">
        <jsp:param name="FROM_PAGE" value="CONTENT_HUB"/>
      </jsp:include>
       
      <%-- <jsp:include page="/jsp/common/socialBox.jsp" />--%>
    </div><%-- Hidden Inputs--%><input type="hidden" id="collegeName" value='${requestScope.COLLEGE_NAME}'/>
    <input type="hidden" id="collegeId" value='${requestScope.COLLEGE_ID}'/>
    <input type="hidden" id="collegeNameDisplay" value='${requestScope.COLLEGE_NAME_DISPLAY}'/>
    <input type="hidden" id="collegeNameGa" value='${requestScope.COLLEGE_NAME_GA}'/>
    <input type="hidden" id="totalReviewCount" value='${requestScope.TOTAL_REVIEW_COUNT}'/>
    <input type="hidden" id="overAllRating" value='${requestScope.OVERALL_RATING}'/>
    <input type="hidden" id="orderItemId" value='<%=orderItemId%>'/>
    <input type="hidden" id="subOrderItemId" value='<%=subOrderItemId%>'/>
    <input type="hidden" id="myhcProfileId" value='<%=myhcProfileId%>'/>
    <input type="hidden" id="profileType" value='<%=profileType%>'/>
    <input type="hidden" id="networkId" value="<%=cpeQualificationNetworkId%>"/>
    <input type="hidden" name="typeOfEnquiry" value="send-college-email" id="typeOfEnquiry"></input>
    <input type="hidden" name="qlFlag" value="N" id="qlFlag"></input>
    <input type="hidden" id="requestUrl" name="requestUrl" value='${requestScope.REQUEST_URL}'></input>
    <%-- <input type="hidden" id="refferalUrl" name="refferalUrl" value="<bean:write name="REFERER_URL"scope="request"/>">--%>
    <input type="hidden" name="studyLevelId" value="M" id="studyLevelId"></input>
    <input type="hidden" id="btSubmit" name="btSubmit" value="Proceed"></input>
    <input type="hidden" name="autoEmailFlag" value="" id="autoEmailFlag"></input>
    <input type="hidden" name="widgetId" id="widgetId" value="<%=widgetId%>"></input>
    <%String domainList = new CommonUtil().getProperties().getString("wuni.form.email.domain.list");%>
    <input type="hidden" id="domainLstId" value='<%=domainList%>'/>
    <input type="hidden" id="referrerURL_GA" value='${requestScope.REFERER_URL}'/>
    <input type="hidden" id="regLoggingType" value="global-nav"/>
    <input type="hidden" id="cubProfileType" value="CH_INST_PROFILE"/>
    <input type="hidden" id="currentSection" name="currentSection" value=""/>
    <input type="hidden" id="currentSectionLandingTime" name="currentSectionLandingTime" value=""/>
    <input type="hidden" id="prevSection" name="prevSection" value=""/>
    <input type="hidden" id="curDimVal" name="curDimVal" value=""/>
    <input type="hidden" id="exitOpenDayResFlg" value="<%=exitOpenDayRes%>"/>
    <input type="hidden" id="odEvntAction" value="<%=odEvntAction%>"/>
    <input type="hidden" id="enquiryExistFlag" value="<%=enquiryExistFlag%>"/>
    <%String browserip = GlobalConstants.WHATUNI_SCHEME_NAME + new CommonUtil().getProperties().getString("review.autocomplete.ip");%>
    <input type="hidden" id="refferalUrl" name="refferalUrl" value='<%=(request.getHeader("referer") !=null ? request.getHeader("referer") : browserip+"/degrees/home.html")%>'/>
    <%-- Hidden Inputs--%>
    <div class="bk_to_top" style="display: none;" id="back-top-div">
      <a href="">
        <img src='<%=CommonUtil.getImgPath("/wu-cont/images/content_hub/icons/chub_bk2top.png",0)%>'
             alt="go to top"></img>
      </a>
    </div><script type="text/javascript">  
        jQuery(document).ready(function() {
          contentScroll();
          jQuery(window).on('load', playOrPauseVideoOnScroll);
          //videoPlayAndPauseOnCarousal();
          sectionsStatsOnScrollStops();
          cubEnqEngagement();
          prePopulateLabelOnTop();
          //wu582_20181023 - Sabapathi: added script for stats logging on page load
	  sectionsDBStatsLog('view')
        });
      </script>
      <script type="text/javascript">
       $scroll(document).ready(function() {
      $scroll(".rvbx_shdw,.rev_lbox .revcls a").click(function(){
        $scroll(".rev_lbox").removeAttr("style");
        $scroll(".rev_lbox").addClass("fadeOut").removeClass("fadeIn");
        $scroll("html").removeClass("rvscrl_hid");
      });
      $scroll(".revlst_rht .rlst_wrap .rev_mre a").click(function(){
        $scroll(".rev_lbox").css({'z-index':'111111','visibility':'visible'});
        $scroll(".rev_lbox").addClass("fadeIn").removeClass("fadeOut");
        $scroll("html").addClass("rvscrl_hid");
      });
      adjustStyle();
    });
            function adjustStyle() {
      var width = document.documentElement.clientWidth;
        function rev_Lightbox(){
        var revnmehgt = $scroll(".rvbx_cnt .revlst_lft").outerHeight();
        var lbxhgt = $scroll(".rev_lbox .rvbx_cnt").height();
        var txth2hgt = $scroll(".rvbx_cnt h2").outerHeight();
        var txth3hgt = $scroll(".rvbx_cnt h3").outerHeight();
      if (width <= 480) {
        /* var mt15 = 15;var bor2 = 2; var updhgt = 18; */
        var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
        var scrhgt = lbxhgt - lbxmin; 
        $scroll(".lbx_scrl").css("height", +scrhgt+ "px");
      }else if ((width >= 481) && (width <= 992)) {
        /* var mt15 = 15;var bor2 = 2; var updhgt = 18; */
        var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
        var scrhgt = lbxhgt - lbxmin; 
        $scroll(".lbx_scrl").css("height", +scrhgt+ "px");
      }else{
      /* var mt15 = 15;var bor2 = 2; */
        var lbxmin = 17 + txth2hgt + txth3hgt;
        var scrhgt = lbxhgt - lbxmin; 
        $scroll(".lbx_scrl").css("height", +scrhgt+ "px");
      }
    }
    rev_Lightbox();  
    }
    $scroll(window).resize(function() {
       adjustStyle();  
    });  
      </script>
  </body>
