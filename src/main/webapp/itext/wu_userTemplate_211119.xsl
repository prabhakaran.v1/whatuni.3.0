<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
   <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" />
   <xsl:template match="/">
      <html>
         <head>
            <title>
               Your data report from Whatuni
            </title>
            <style type="text/css">
               .web_url{font-family:Lato-Regular;font-size:14px;color:#00bbfd}
               .active{color:#707070;}
               .inactive{color:#c0c0c0;}
            </style>
         </head>
         <body bgcolor="#e0e0e0" width="100%" style="margin: 0;" yahoo="yahoo">
            <table style="" align="center" width="750px" cellpadding="0" cellspacing="0" bgcolor="#fff">
               <tr>
                  <td colspan="2" align="left" style="padding-top:22px;padding-bottom:22px;padding-right:25px;">
                     <img src="http://www.whatuni.com/wu-cont/images/whatuni_logo_dpdf.png" width="93" height="102" alt="Whatuni logo" />
                  </td>
               </tr>
               <tr>
                 <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:8px;line-height:24px">
                   We confirm that whatuni.com processes your personal details on the basis of our legitimate business interests for all activities apart from the mailing preferences (newsletters, university updates, reminders and surveys) where we rely on your consent. 
                   </td>
                   </tr>
               <tr>
                   <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:8px;line-height:24px">
                   We process the personal information you provide to us when you register or when you update your profile. We do not collect any special categories data. As a minimum, we collect your name, email address and your IP addresses. In addition, we may collect your date of birth, postal address, nationality, school/university information, predicted grades, achieved grades, study level and intended year of entry.  
                   </td>
                   </tr>
               <tr>
                   <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:8px;line-height:24px">
                   The personal information we process enables us to customise the service we provide to you by helping you find courses you are eligible to apply to. We also offer you the option to make enquiries to universities directly, or to download prospectuses and guides.
                   </td>
                   </tr>
               <tr>
                   <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:8px;line-height:24px">
                   Each time you make an enquiry, or download a prospectus, we share your information (name, email address, enquiry details) with the University so that they can respond to you directly.
                   </td>
                   </tr>
               <tr>
                   <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:8px;line-height:24px">
                   You have the right to request rectification, erasure, restriction or to object to such a processing. You can also withdraw your consent for marketing activities at any point by changing your mailing preferences under your profile, or clicking on the unsubscribe link in any email you receive from us. Furthermore, you have the right to launch a complaint with the ICO if you feel our processing is not fair or if we fail to respond to your individual rights under GDPR.
                   </td>
                   </tr>
               <tr>
                   <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:8px;line-height:24px">
                   We use targeted advertising on our website and off site (after visiting our website and browsing the web elsewhere) and may send you targeted ads based on the courses and Universities you have searched. This is done through cookies and smart pixels, please refer to our <a href="https://www.whatuni.com/degrees/cookies.html" style="text-decoration: none;color:#00bbfd">cookie policy</a> for more details
                   </td>
                   </tr>
               <tr>
                   <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:8px;line-height:24px">
                   Your information is stored in the United Kingdom. We operate a strict access policy and your information is only accessed on a need to know basis by our teams in the UK and India. We have intercompany data sharing agreements that ensures the same rules are followed through the organisation and everyone works with the same high security and privacy standard when processing your personal information.  We will keep your information for three years following your last interaction with this website.  
                   </td>
                   </tr>
               <tr>
                   <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:8px;line-height:24px">
                   For more details on how we process personal data, please visit our <a href="https://www.whatuni.com/wu-cont/privacy.htm" style="text-decoration: none;color:#00bbfd">Privacy Notice</a>
                 </td>
               </tr>
               <!-- Basic info section start -->
               <tr>
                  <td colspan="2" style="padding-top:32px;color: #707070;font-family:Lato-Bold;font-size:24px;">My details</td>
               </tr>
               <tr>
                  <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:8px;line-height:24px">
                     This PDF contains all of the data about you that we have collected about you since you registered with Whatuni
                  </td>
               </tr>
               <xsl:if test="my_details/personal_detail/signup_date">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-bold;font-size:14px;">Sign up date</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:5px">
                        <xsl:value-of select="my_details/personal_detail/signup_date" />
                     </td>
                  </tr>
               </xsl:if>
               <tr>
                  <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Date when we started tracking</td>
               </tr>
               <tr>
                  <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:6px;line-height:20px">
                     We track user activity using cookies, and began tracking your browsing activity on Whatuni before you subsequently registered with us. You can read more about our Cookie Policy <a href="https://www.whatuni.com/degrees/cookies.html" style="text-decoration: none;color:#00bbfd">here</a>
                  </td>
               </tr>
               <tr>
                  <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;font-size:16px;color:#c0c0c0" width="150px">BASIC INFO</td>
               </tr>
               <xsl:if test="my_details/personal_detail/basic_info/last_updated_date">
                  <tr>
                     <td colspan="2" style="font-size:14px;color:#c0c0c0">
                        Last updated [
                        <xsl:value-of select="my_details/personal_detail/basic_info/last_updated_date" />
                        ]
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="my_details/personal_detail/basic_info/image_detail">
                  <tr>
                     <td colspan="2" width="80" style="padding:30px 0 0">
                        <xsl:value-of select="my_details/personal_detail/basic_info/image_detail" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/first_name != '') and not(contains(my_details/personal_detail/basic_info/first_name, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="font-family:Lato-Bold;color:#707070;padding-top:30px;font-size:14px;">First name</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;color:#707070;font-size:14px;padding-top:5px">
                        <xsl:value-of select="my_details/personal_detail/basic_info/first_name" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/last_name != '') and not(contains(my_details/personal_detail/basic_info/last_name, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="font-family:Lato-Bold;padding-top:30px;color: #707070;font-size:14px;">Last name</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/last_name" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/email_id != '') and not(contains(my_details/personal_detail/basic_info/email_id, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Email</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/email_id" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/YOE != '') and not(contains(my_details/personal_detail/basic_info/YOE, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Year of entry</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/YOE" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/DOB != '') and not(contains(my_details/personal_detail/basic_info/DOB, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Date of birth</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/DOB" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/study_level != '') and not(contains(my_details/personal_detail/basic_info/study_level, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Study level</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/study_level" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/nationality != '') and not(contains(my_details/personal_detail/basic_info/nationality, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Nationality</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/nationality" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/country != '') and not(contains(my_details/personal_detail/basic_info/country, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Country of residence</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/country" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/address_1 != '') and not(contains(my_details/personal_detail/basic_info/address_1, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Address</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/address_1" disable-output-escaping="yes" />
                        <xsl:if test="(my_details/personal_detail/basic_info/address_2 != '') and not(contains(my_details/personal_detail/basic_info/address_2, '[CDATA[]]>'))">
                           ,
                           <xsl:value-of select="my_details/personal_detail/basic_info/address_2" disable-output-escaping="yes" />
                        </xsl:if>
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/town != '') and not(contains(my_details/personal_detail/basic_info/town, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Town</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/town" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/post_code != '') and not(contains(my_details/personal_detail/basic_info/post_code, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Postcode</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/post_code" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/phone_number != '') and not(contains(my_details/personal_detail/basic_info/phone_number, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Phone Number</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/phone_number" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/school != '') and not(contains(my_details/personal_detail/basic_info/school, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">School</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/school" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="((my_details/personal_detail/basic_info/grade_type != '') and not(contains(my_details/personal_detail/basic_info/grade_type, '[CDATA[]]>'))) or my_details/user_qual">
                   <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Predicted / achieved grades</td>
                   </tr>
                </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/grade_type != '') and not(contains(my_details/personal_detail/basic_info/grade_type, '[CDATA[]]>'))">
                  
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/grade_type" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/grade_detail != '') and not(contains(my_details/personal_detail/basic_info/grade_detail, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/grade_detail" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>  
               <xsl:if test="my_details/user_qual">                 
                 <xsl:for-each select="my_details/user_qual">                 
                   <xsl:if test="user_qualification != ''">
                        <tr>
                           <td colspan="2" width="750" style="padding:23px 0 3px 0;color:#707070;line-height:20px;font-size:14px;width:100%;">
                              <strong>Qualification: </strong> 
                              <xsl:value-of select="user_qualification" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="subject_desc != ''">
                        <tr>
                           <td colspan="2" width="750" style="padding:3px 0 3px 0;color:#707070;line-height:20px;font-size:14px;width:100%;">
                               <strong>Subject: </strong> 
                              <xsl:value-of select="subject_desc" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="user_grade != ''">
                        <tr>
                           <td colspan="2" width="750" style="padding:3px 0 3px 0;color:#707070;line-height:20px;font-size:14px;width:100%;">
                               <strong>Grade: </strong> 
                              <xsl:value-of select="user_grade" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                   </xsl:for-each>
                 </xsl:if> 
               
               <xsl:choose>
                  <xsl:when test="my_details/personal_detail/basic_info/del_college_flag != 'Y'">
                     <xsl:if test="(my_details/personal_detail/basic_info/university != '') and not(contains(my_details/personal_detail/basic_info/university, '[CDATA[]]>'))">
                        <tr>
                           <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Your Undergraduate university</td>
                        </tr>
                        <tr>
                           <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                              <xsl:value-of select="my_details/personal_detail/basic_info/university" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                     <tr>
                        <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Your Undergraduate university</td>
                     </tr>
                     <tr>
                        <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                           Deleted institution<br></br>
                           The Institution you selected is no longer available on Whatuni.com
                        </td>
                     </tr>
                  </xsl:otherwise>
               </xsl:choose>
               <xsl:choose>
                  <xsl:when test="my_details/personal_detail/basic_info/del_course_flag != 'Y'">
                     <xsl:if test="(my_details/personal_detail/basic_info/course != '') and not(contains(my_details/personal_detail/basic_info/course, '[CDATA[]]>'))">
                        <tr>
                           <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Your Undergraduate course</td>
                        </tr>
                        <tr>
                           <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                              <xsl:value-of select="my_details/personal_detail/basic_info/course" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                  </xsl:when>
                  <xsl:otherwise>
                     <tr>
                        <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Your Undergraduate course</td>
                     </tr>
                     <tr>
                        <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                           Deleted course<br></br>
                           The Course you selected is no longer available on Whatuni.com
                        </td>
                     </tr>
                  </xsl:otherwise>
               </xsl:choose>
               <xsl:if test="(my_details/personal_detail/basic_info/year_comp_expected != '') and not(contains(my_details/personal_detail/basic_info/year_comp_expected, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Year completed/expected</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/year_comp_expected" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/award_class != '') and not(contains(my_details/personal_detail/basic_info/award_class, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Award classification</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/award_class" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/current_empt_status != '') and not(contains(my_details/personal_detail/basic_info/current_empt_status, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Employment status</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/current_empt_status" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/study_mode != '') and not(contains(my_details/personal_detail/basic_info/study_mode, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Study mode</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/study_mode" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/degree_type != '') and not(contains(my_details/personal_detail/basic_info/degree_type, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Degree type</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/degree_type" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/research_interests != '') and not(contains(my_details/personal_detail/basic_info/research_interests, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Research interests</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/research_interests" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/industry != '') and not(contains(my_details/personal_detail/basic_info/industry, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Industry</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/industry" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>
               <xsl:if test="(my_details/personal_detail/basic_info/seniority_level != '') and not(contains(my_details/personal_detail/basic_info/seniority_level, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Seniority level</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/personal_detail/basic_info/seniority_level" disable-output-escaping="yes" />
                     </td>
                  </tr>
               </xsl:if>                   
               <xsl:if test="my_details/ip_address">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">IP address(es) we have detected whilst you were using Whatuni</td>
                  </tr>
                  <xsl:for-each select="my_details/ip_address">
                     <tr>
                        <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                           <xsl:value-of select="user_ip" disable-output-escaping="yes" />
                           <xsl:if test="position()!=last()">,</xsl:if>
                           <br />
                        </td>
                     </tr>
                  </xsl:for-each>
               </xsl:if>
               <tr>
                  <td colspan="2" style="padding-top:25px;">
                     <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                  </td>
               </tr>
               <!-- Basic info section End -->
               <!-- Mail setting section Start -->
               <tr>
                  <td colspan="2" style="padding-top:25px;font-family:Lato-Bold;font-size:24px;color: #707070;">Mailing preferences</td>
               </tr>
               <tr>
                  <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:25px;line-height:20px;">Here you can preview all emails you have subscribed to. Once subscribed, your data is held in our email marketing database.</td>
               </tr>
               <tr>
                  <td colspan="2" height="32" />
               </tr>
               <xsl:variable name="temp" select="my_details/mail_setting/marketing_flag" />
               <xsl:variable name="marketingFlag">
                  <xsl:choose>
                     <xsl:when test="$temp = 'Y'">active</xsl:when>
                     <xsl:otherwise>inactive</xsl:otherwise>
                  </xsl:choose>
               </xsl:variable>
               <tr>
                  <td colspan="2" style="color:#c0c0c0;font-family:Lato-Regular;font-size:12px;line-height:20px;">
                     <xsl:choose>
                        <xsl:when test="$temp = 'Y'">RECEIVING</xsl:when>
                        <xsl:otherwise>NOT RECEIVING</xsl:otherwise>
                     </xsl:choose>
                  </td>
               </tr>
               <tr>
                  <td colspan="2" class="{$marketingFlag}" style="padding:8px 0;font-family:Lato-Bold;font-size:14px;line-height:20px;">Newsletters</td>
               </tr>
               <tr>
                  <td colspan="2" class="{$marketingFlag}" style="font-family:Lato-Regular;font-size:14px;line-height:20px;">Emails from us providing you the latest university news, tips and guides.</td>
               </tr>
               <tr>
                  <td colspan="2" height="32" />
               </tr>
               <xsl:variable name="temp1" select="my_details/mail_setting/solus_flag" />
               <xsl:variable name="solusFlag">
                  <xsl:choose>
                     <xsl:when test="$temp1 = 'Y'">active</xsl:when>
                     <xsl:otherwise>inactive</xsl:otherwise>
                  </xsl:choose>
               </xsl:variable>
               <tr>
                  <td colspan="2" style="color:#c0c0c0;font-family:Lato-Regular;font-size:12px;line-height:20px;">
                     <xsl:choose>
                        <xsl:when test="$temp1 = 'Y'">RECEIVING</xsl:when>
                        <xsl:otherwise>NOT RECEIVING</xsl:otherwise>
                     </xsl:choose>
                  </td>
               </tr>
               <tr>
                  <td colspan="2" class="{$solusFlag}" style="padding:8px 0;font-family:Lato-Bold;font-size:14px;line-height:20px;">University updates</td>
               </tr>
               <tr>
                  <td colspan="2" class="{$solusFlag}" style="font-family:Lato-Regular;font-size:14px;line-height:20px;">Emails on behalf of universities and carefully selected third-party partners.</td>
               </tr>
               <tr>
                  <td colspan="2" height="32" />
               </tr>
               <xsl:variable name="temp2" select="my_details/mail_setting/reminder_flag" />
               <xsl:variable name="reminderFlag">
                  <xsl:choose>
                     <xsl:when test="$temp2 = 'N'">active</xsl:when>
                     <xsl:otherwise>inactive</xsl:otherwise>
                  </xsl:choose>
               </xsl:variable>
               <tr>
                  <td colspan="2" style="color:#c0c0c0;font-family:Lato-Regular;font-size:12px;line-height:20px;">
                     <xsl:choose>
                        <xsl:when test="$temp2 = 'N'">RECEIVING</xsl:when>
                        <xsl:otherwise>NOT RECEIVING</xsl:otherwise>
                     </xsl:choose>
                  </td>
               </tr>
               <tr>
                  <td class="{$reminderFlag}" style="padding:8px 0;font-family:Lato-Bold;font-size:14px;line-height:20px;">Reminders</td>
               </tr>
               <tr>
                  <td colspan="2" class="{$reminderFlag}" style="font-family:Lato-Regular;font-size:14px;line-height:20px;">To remind you about upcoming course start dates, your shortlisted courses and any courses you emailed about.</td>
               </tr>
               <tr>
                  <td colspan="2" height="32" />
               </tr>
               <xsl:variable name="temp3" select="my_details/mail_setting/survey_flag" />
               <xsl:variable name="surveyFlag">
                  <xsl:choose>
                     <xsl:when test="$temp3 = 'Y'">active</xsl:when>
                     <xsl:otherwise>inactive</xsl:otherwise>
                  </xsl:choose>
               </xsl:variable>
               <tr>
                  <td colspan="2" style="color:#c0c0c0;font-family:Lato-Regular;font-size:12px;line-height:20px;">
                     <xsl:choose>
                        <xsl:when test="$temp3 = 'Y'">RECEIVING</xsl:when>
                        <xsl:otherwise>NOT RECEIVING</xsl:otherwise>
                     </xsl:choose>
                  </td>
               </tr>
               <tr>
                  <td colspan="2" class="{$surveyFlag}" style="padding:8px 0;font-family:Lato-Bold;font-size:14px;line-height:20px;">Surveys</td>
               </tr>
               <tr>
                  <td colspan="2" class="{$surveyFlag}" style="font-family:Lato-Regular;font-size:14px;line-height:20px;">Have your say on important education issues and the services you receive from us and our partners</td>
               </tr>
               <tr>
                  <td colspan="2" style="padding-top:25px;">
                     <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                  </td>
               </tr>
               <!-- Mail setting section End -->
               <!-- Privacy setting section Start -->
               <xsl:if test="my_details/privacy_setting/friends_activity != 'N'">
                  <tr>
                     <td colspan="2" style="padding-top:25px;font-family:Lato-Bold;font-size:24px;color: #707070;">Privacy</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;padding-top: 23px;">Who can see my activity</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="padding:8px 0;color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">My school friends and Facebook connections can see my activity on Whatuni.</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- Privacy setting section End -->
               
               
               <xsl:if test="my_details/wugo_user">
                 <tr>
                     <td style="padding-top:25px;font-family:Lato-Bold;font-size:24px;color: #707070;" colspan="2">Applications made through Whatuni GO</td>
                  </tr>
                  <xsl:if test="my_details/wugo_com_user_app_detail">
				   <tr>
                     <td style="padding-top:25px;font-family:Lato-Bold;font-size:24px;color: #707070;" colspan="2">Your Confirmed Application</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:25px;line-height:20px;">As part of the Whatuni GO application process, your personal details were shared with the university you have made an application to. Now that you have requested a copy of the personal data we store, we have also advised the university’s DPO and they will respond to you separately.</td>
                  </tr>
				   <xsl:if test="my_details/wugo_com_user_app_detail">
                    
                    <xsl:for-each select="my_details/wugo_com_user_app_detail">
                     <tr>
                        <td colspan="2" style="padding-top:23px">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;">
                                    <xsl:if test="app_college_logo">
                                       <xsl:value-of select="app_college_logo" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td width="675" style="padding-left:15px;" valign="top">
                                    <table cellpadding="0" cellspacing="0" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="app_applictaion_date">
                                          <tr>
                                             <td style="color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                APPLIED ON
                                                <xsl:value-of select="app_applictaion_date" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:if test="app_course_title">
                                                <tr>
                                                   <td style="padding:8px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="app_course_title" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                             <xsl:if test="app_college_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="app_college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                             <xsl:if test="app_application_status">
                                          <tr>
                                             <td style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                <xsl:value-of select="app_application_status" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <!--
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_title">
                                                <tr>
                                                   <td style="padding:8px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_title" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted course<br></br>
                                                   The course you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       -->
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  </xsl:if>
                  </xsl:if>
                  <xsl:if test="my_details/wugo_user_qual">
                   <xsl:for-each select="my_details/wugo_user_qual">
                 
                   <xsl:if test="user_qualification != ''">
                        <tr>
                           <td colspan="2" width="750" style="padding:23px 0 3px 0;color:#707070;line-height:20px;font-size:14px;width:100%;">
                              <strong>Qualification: </strong> 
                              <xsl:value-of select="user_qualification" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="subject_desc != ''">
                        <tr>
                           <td colspan="2" width="750" style="padding:3px 0 3px 0;color:#707070;line-height:20px;font-size:14px;width:100%;">
                               <strong>Subject: </strong> 
                              <xsl:value-of select="subject_desc" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="user_grade != ''">
                        <tr>
                           <td colspan="2" width="750" style="padding:3px 0 3px 0;color:#707070;line-height:20px;font-size:14px;width:100%;">
                               <strong>Grade: </strong> 
                              <xsl:value-of select="user_grade" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                   </xsl:for-each>
                   </xsl:if>
                   <xsl:if test="my_details/wugo_user_basic_info">
                     <xsl:for-each select="my_details/wugo_user_basic_info">
                        <xsl:if test="answer_value != ''">
                          <tr>
                            <td colspan="2" width="750" style="padding:23px 0 3px 0;color:#707070;line-height:20px;font-size:14px;width:100%;">
                              <strong>Question: </strong> 
                              <xsl:value-of select="question_title" disable-output-escaping="yes" />
                            </td>
                          </tr>
                        </xsl:if>
                        <xsl:if test="answer_value != ''">
                          <tr>
                            <td colspan="2" width="750" style="padding:3px 0 3px 0;color:#707070;line-height:20px;font-size:14px;width:100%;">
                              <strong>Answer: </strong> 
                              <xsl:value-of select="answer_value" disable-output-escaping="yes" />
                            </td>
                          </tr>
                        </xsl:if>
                      </xsl:for-each>
                    </xsl:if>
                    <xsl:if test="(my_details/wugo_user_personal_info/wugo_user_firstname != '') and not(contains(my_details/wugo_user_personal_info/wugo_user_firstname, '[CDATA[]]>'))">
                      <tr>
                        <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Name</td>
                      </tr>
                      <tr>
                        <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                          <xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_firstname" disable-output-escaping="yes" /> <xsl:if test="(contains(my_details/wugo_user_personal_info/wugo_user_lastname, '[CDATA[]]>'))"><xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_lastname" disable-output-escaping="yes" /></xsl:if> <xsl:if test="(contains(my_details/wugo_user_personal_info/wugo_user_middlename, '[CDATA[]]>'))"> <xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_middlename" disable-output-escaping="yes" /></xsl:if>
                        </td>
                      </tr>
                    </xsl:if>
                    <xsl:if test="(my_details/wugo_user_personal_info/wugo_user_dob != '') and not(contains(my_details/wugo_user_personal_info/wugo_user_dob, '[CDATA[]]>'))">
                      <tr>
                        <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Date of birth</td>
                      </tr>
                      <tr>
                        <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                          <xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_dob" disable-output-escaping="yes" />
                        </td>
                      </tr>
                    </xsl:if>
                   <xsl:if test="(my_details/wugo_user_personal_info/wugo_user_gender != '') and not(contains(my_details/wugo_user_personal_info/wugo_user_gender, '[CDATA[]]>'))">
                      <tr>
                        <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Gender</td>
                      </tr>
                      <tr>
                        <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                          <xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_gender" disable-output-escaping="yes" />
                        </td>
                      </tr>
                    </xsl:if>
                    <xsl:if test="(my_details/wugo_user_personal_info/wugo_user_postcode != '') and not(contains(my_details/wugo_user_personal_info/wugo_user_postcode, '[CDATA[]]>'))">
                      <tr>
                        <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Postcode</td>
                      </tr>
                      <tr>
                        <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                          <xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_postcode" disable-output-escaping="yes" />
                        </td>
                      </tr>
                    </xsl:if>
                    <xsl:if test="(my_details/wugo_user_personal_info/wugo_user_number != '') and not(contains(my_details/wugo_user_personal_info/wugo_user_number, '[CDATA[]]>'))">
                      <tr>
                        <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Contact number</td>
                      </tr>
                      <tr>
                        <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                          <xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_number" disable-output-escaping="yes" />
                        </td>
                      </tr>
                    </xsl:if>
                    <xsl:if test="(my_details/wugo_user_personal_info/wugo_user_contact != '') and not(contains(my_details/wugo_user_personal_info/wugo_user_contact, '[CDATA[]]>'))">
                      <tr>
                        <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Phone type</td>
                      </tr>
                      <tr>
                        <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                          <xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_contact" disable-output-escaping="yes" />
                        </td>
                      </tr>
                    </xsl:if>
                    <xsl:if test="(my_details/wugo_user_personal_info/wugo_user_ucas_id != '') and not(contains(my_details/wugo_user_personal_info/wugo_user_ucas_id, '[CDATA[]]>'))">
                      <tr>
                        <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">UCAS ID</td>
                      </tr>
                      <tr>
                        <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                          <xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_ucas_id" disable-output-escaping="yes" />
                        </td>
                      </tr>
                    </xsl:if>
                    <xsl:if test="(my_details/wugo_user_personal_info/wugo_user_address1 != '') and not(contains(my_details/wugo_user_personal_info/wugo_user_address1, '[CDATA[]]>'))">
                  <tr>
                     <td colspan="2" style="padding-top:30px;color: #707070;font-family:Lato-Bold;font-size:14px;">Address</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                        <xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_address1" disable-output-escaping="yes" />
                        <xsl:if test="(my_details/wugo_user_personal_info/wugo_user_address2 != '') and not(contains(my_details/wugo_user_personal_info/wugo_user_address2, '[CDATA[]]>'))">
                           ,
                           <xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_address2" disable-output-escaping="yes" />
                        </xsl:if>
                     </td>
                  </tr>
               </xsl:if>
                    <xsl:if test="(my_details/wugo_user_personal_info/wugo_user_nationality != '') and not(contains(my_details/wugo_user_personal_info/wugo_user_nationality, '[CDATA[]]>'))">
                      <tr>
                        <td colspan="2" style="padding-top:30px;font-family:Lato-Bold;color: #707070;font-size:14px;">Nationality (ISO country code)</td>
                      </tr>
                      <tr>
                        <td colspan="2" style="font-family:Lato-Regular;font-size:14px;padding-top:5px;color: #707070;">
                          <xsl:value-of select="my_details/wugo_user_personal_info/wugo_user_nationality" disable-output-escaping="yes" />
                        </td>
                      </tr>
                    </xsl:if>
                       <xsl:if test="my_details/wugo_other_user_app_detail">
                     <tr>
                     <td style="padding-top:25px;font-family:Lato-Bold;font-size:24px;color: #707070;" colspan="2">Incomplete Applications</td>
                  </tr>
                    <xsl:for-each select="my_details/wugo_other_user_app_detail">
                     <tr>
                        <td colspan="2" style="padding-top:23px">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;">
                                    <xsl:if test="app_college_logo">
                                       <xsl:value-of select="app_college_logo" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td width="675" style="padding-left:15px;" valign="top">
                                    <table cellpadding="0" cellspacing="0" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="app_applictaion_date">
                                          <tr>
                                             <td style="color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                APPLIED ON
                                                <xsl:value-of select="app_applictaion_date" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:if test="app_course_title">
                                                <tr>
                                                   <td style="padding:8px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="app_course_title" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                             <xsl:if test="app_college_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="app_college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                             <xsl:if test="app_application_status">
                                          <tr>
                                             <td style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                <xsl:value-of select="app_application_status" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <!--
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_title">
                                                <tr>
                                                   <td style="padding:8px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_title" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted course<br></br>
                                                   The course you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       -->
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  </xsl:if>
                   
                   <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               
               <!-- My prospectus section Start -->
               <xsl:if test="my_details/my_prospectus">
                  <tr>
                     <td style="padding-top:25px;font-family:Lato-Bold;font-size:24px;color: #707070;" colspan="2">Prospectuses</td>
                  </tr>
                  <xsl:for-each select="my_details/my_prospectus">
                     <tr>
                        <td colspan="2" style="padding-top:23px">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td width="675" style="padding-left:15px;" valign="top">
                                    <table cellpadding="0" cellspacing="0" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="order_date">
                                          <tr>
                                             <td style="color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                ORDERED ON
                                                <xsl:value-of select="order_date" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_name">
                                                <tr>
                                                   <td style="padding:8px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted course<br></br>
                                                   The course you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:if test="study_level">
                                          <tr>
                                             <td style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                <xsl:value-of select="study_level" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:if test="webform_url">
                                          <tr>
                                             <td style="color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;word-wrap: break-word;">
                                                <xsl:value-of select="webform_url" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- My prospectus section End -->
               <!-- My email enquiries section Start -->
               <xsl:if test="my_details/my_email">
                  <tr>
                     <td style="padding-top:25px;font-family:Lato-Bold;font-size:24px;color: #707070;" colspan="2">Email enquiries</td>
                  </tr>
                  <tr>
                     <td colspan="2" style="font-family:Lato-Regular;font-size:14px;color: #707070;padding-top:25px;line-height:20px;">Below are the universities you have sent an enquiry to. You can view the enquiry messages you've sent.</td>
                  </tr>
                  <xsl:for-each select="my_details/my_email">
                     <tr>
                        <td colspan="2" style="padding-top:23px;">
                           <table cellpadding="0" cellspacing="0" style="width:100%">
                              <tr>
                                 <td style="width:74px;" valign="top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td width="675" style="padding-left:15px;">
                                    <table cellpadding="0" cellspacing="0" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="enquiry_date">
                                          <tr>
                                             <td style="color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                ENQUIRED ON
                                                <xsl:value-of select="enquiry_date" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted course<br></br>
                                                   The course you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:if test="enquiry_text">
                                          <tr>
                                             <td style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                <xsl:value-of select="enquiry_text" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:if test="webform_url">
                                          <tr>
                                             <td style="color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;word-wrap: break-word;">
                                                <xsl:value-of select="webform_url" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- My email enquiries section End -->
               <!-- My webform section Start -->
               <xsl:if test="my_details/my_webform">
                  <tr>
                     <td style="padding-top:25px;font-family:Lato-Bold;font-size:24px;color: #707070;" colspan="2">Clicks to external webpages</td>
                  </tr>
                  <xsl:for-each select="my_details/my_webform">
                     <tr>
                        <td colspan="2" style="padding-top:23px;">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;" valign="top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td width="675" style="padding-left:15px;">
                                    <table cellpadding="0" cellspacing="0" width="100%" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="enquiry_date">
                                          <tr>
                                             <td style="padding:0 0 4px 0;color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                CLICKED ON
                                                <xsl:value-of select="enquiry_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_name">
                                                <tr>
                                                   <td style="padding:4px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted course<br></br>
                                                   The Course you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:if test="webform_url">
                                          <tr>
                                             <td style="color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;word-wrap: break-word;">
                                                <xsl:value-of select="webform_url" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- My webform section End -->
               <!-- My open days reserve place section Start -->
               <xsl:if test="my_details/reserve_place">
                  <tr>
                     <td style="padding-top:25px;font-family:Lato-Bold;font-size:24px;color: #707070;" colspan="2">Open day 'reserve a place' clicks</td>
                  </tr>
                  <xsl:for-each select="my_details/reserve_place">
                     <tr>
                        <td colspan="2" style="padding-top:23px">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;" valign="top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td width="675" style="padding-left:15px;" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="enquiry_date">
                                          <tr>
                                             <td style="color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                CLICKED ON
                                                <xsl:value-of select="enquiry_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:8px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:if test="study_level">
                                          <tr>
                                             <td style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                <xsl:value-of select="study_level" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:if test="webform_url">
                                          <tr>
                                             <td style="color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                <xsl:value-of select="webform_url" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- My open days reserve place section End -->
               <!-- Save to calendar section Start -->
               <xsl:if test="my_details/save_calendar">
                  <tr>
                     <td style="padding-top:25px;font-size:24px;color: #707070;font-family:Lato-Bold;" colspan="2">Open days saved to the Whatuni Calendar</td>
                  </tr>
                  <xsl:for-each select="my_details/save_calendar">
                     <tr>
                        <td colspan="2" style="padding-top:40px;">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;" valign="top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td style="padding-left:15px;">
                                    <table cellpadding="0" cellspacing="0" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="save_date">
                                          <tr>
                                             <td style="color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                SAVED ON
                                                <xsl:value-of select="save_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:if test="study_level">
                                          <tr>
                                             <td style="padding:4px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                <xsl:value-of select="study_level" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:if test="openday_date">
                                          <tr>
                                             <td style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                <xsl:value-of select="openday_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- Save to calendar section End -->
               <!-- Clearing Hotline section Start -->
               <xsl:if test="my_details/hotline">
                  <tr>
                     <td style="padding-top:25px;font-size:24px;color: #707070;font-family:Lato-Bold;" colspan="2">University hotlines you called during Clearing</td>
                  </tr>
                  <xsl:for-each select="my_details/hotline">
                     <tr>
                        <td colspan="2" style="padding-top:40px;">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;" valign="top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td style="padding-left:15px;">
                                    <table cellpadding="0" cellspacing="0" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="enquiry_date">
                                          <tr>
                                             <td style="padding:0 0 4px 0;color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                CALLED ON
                                                <xsl:value-of select="enquiry_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_name">
                                                <tr>
                                                   <td style="padding:4px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted course<br></br>
                                                   The Course you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:if test="webform_url">
                                          <tr>
                                             <td style="color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                <xsl:value-of select="webform_url" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- Clearing Hotline section End -->
               <!-- Clearing Webclick section Start -->
               <xsl:if test="my_details/clearing_webclick">
                  <tr>
                     <td style="padding-top:25px;font-size:24px;color: #707070;font-family:Lato-Bold;" colspan="2">Clearing webclick</td>
                  </tr>
                  <xsl:for-each select="my_details/clearing_webclick">
                     <tr>
                        <td colspan="2" style="padding-top:40px;">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;" valign="top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td style="padding-left:15px;">
                                    <table cellpadding="0" cellspacing="0" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="enquiry_date">
                                          <tr>
                                             <td style="padding:0 0 4px 0;color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                CLICKED ON
                                                <xsl:value-of select="enquiry_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_name">
                                                <tr>
                                                   <td style="padding:4px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted course<br></br>
                                                   The Course you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:4px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:if test="webform_url">
                                          <tr>
                                             <td width="200" style="padding:4px 0 0;width:200px;color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;overflow-wrap: break-word;word-wrap:break-word;word-break:break-word;">
                                                <xsl:value-of select="webform_url" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- Clearing Webclick section End -->
               <!-- Feature provider hotline section Start -->
               <xsl:if test="my_details/feature_uni">
                  <tr>
                     <td style="padding-top:25px;font-size:24px;color: #707070;font-family:Lato-Bold;" colspan="2">Clearing popular university hotline</td>
                  </tr>
                  <xsl:for-each select="my_details/feature_uni">
                     <tr>
                        <td colspan="2" style="padding-top:23px">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;" valign="top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td width="675" style="padding-left:15px;" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="enquiry_date">
                                          <tr>
                                             <td style="color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                CALLED ON
                                                <xsl:value-of select="enquiry_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:8px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:if test="webform_url">
                                          <tr>
                                             <td style="color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                <xsl:value-of select="webform_url" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- Feature provider hotline section End -->
               <!-- User basekt section Start -->
               <xsl:if test="my_details/my_comparision">
                  <tr>
                     <td style="padding-top:25px;font-size:24px;color: #707070;font-family:Lato-Bold;" colspan="2">Your comparison basket</td>
                  </tr>
                  <xsl:for-each select="my_details/my_comparision">
                     <tr>
                        <td colspan="2" style="padding-top:23px">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;" valign="top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td style="padding-left:15px;">
                                    <table cellpadding="0" cellspacing="0" width="100%" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="added_date">
                                          <tr>
                                             <td style="padding:0 0 4px 0;color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                ADDED ON
                                                <xsl:value-of select="added_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_name">
                                                <tr>
                                                   <td style="padding:4px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted course<br></br>
                                                   The Course you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:4px 0 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:if test="comp_type != ''">
                                          <tr>
                                             <td style="padding:4px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                <xsl:value-of select="comp_type" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- User basekt section End -->
               <!-- User Final 5 section Start -->
               <xsl:if test="my_details/my_final5">
                  <tr>
                     <td style="padding-top:25px;font-size:24px;color: #707070;font-family:Lato-Bold;" colspan="2">Your Final 5</td>
                  </tr>
                  <xsl:for-each select="my_details/my_final5">
                     <tr>
                        <td colspan="2" style="padding-top:23px">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <xsl:if test="choice_no">
                                    <td width="25" style="color:#707070;width:25px;vertical-align:top;font-weight:600;padding-top:5px">
                                       <xsl:value-of select="choice_no" disable-output-escaping="yes" />
                                    </td>
                                 </xsl:if>
                                 <td style="width:74px;vertical-align:top" valign="top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td style="padding-left:15px;vertical-align:top">
                                    <table cellpadding="0" cellspacing="0" width="100%" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="added_date">
                                          <tr>
                                             <td style="padding:0 0 4px 0;color:#c0c0c0;line-height:20px;font-size:12px;">
                                                ADDED ON
                                                <xsl:value-of select="added_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_name">
                                                <tr>
                                                   <td style="padding:4px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted course<br></br>
                                                   The Course you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:if test="college_name">
                                          <tr>
                                             <td style="padding:4px 0 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- User Final 5 section End -->
               <!-- User seached section Start -->
               <xsl:if test="my_details/saved_searches">
                  <tr>
                     <td colspan="2" width="750" style="padding-top:40px;font-size:24px;color: #707070;font-family:Lato-Bold;width:100%;">Saved searches</td>
                  </tr>
                  <xsl:for-each select="my_details/saved_searches">
                     <xsl:if test="search_date != ''">
                        <tr>
                           <td colspan="2" width="750" style="padding-top:23px;color:#c0c0c0;line-height:20px;font-size:12px;width:100%;">
                              SEARCHED ON
                              <xsl:value-of select="search_date" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="search_date != ''">
                        <tr>
                           <td colspan="2" width="750" style="padding:8px 0 8px 0;color:#707070;line-height:20px;font-size:14px;font-family:Lato-Bold;width:100%;">
                              <xsl:if test="(college_display_name != '') and not(contains(college_display_name, '[CDATA[]]>'))">
                                 <xsl:value-of select="college_display_name" disable-output-escaping="yes" />
                                 <xsl:if test="clearing != ''">
                                    -
                                    <xsl:value-of select="clearing" disable-output-escaping="yes" />
                                 </xsl:if>
                                 <br />
                              </xsl:if>
                              <xsl:if test="search_subject != '' and not(contains(college_display_name, '[CDATA[]]>'))">
                                 <xsl:value-of select="search_subject" disable-output-escaping="yes" />
                                 <xsl:if test="clearing != ''">
                                    -
                                    <xsl:value-of select="clearing" disable-output-escaping="yes" />
                                 </xsl:if>
                                 <br></br>
                              </xsl:if>
                              <xsl:if test="ucas_code_srch != ''">
                                 <strong>UCAS code</strong>:  
                                 <xsl:value-of select="ucas_code_srch" disable-output-escaping="yes" />
                              </xsl:if>
                              <xsl:if test="jacs_code != ''">
                                 <strong>JACS code</strong>: 
                                 <xsl:value-of select="jacs_code" disable-output-escaping="yes" />
                              </xsl:if>
                           </td>
                        </tr>
                        <tr>
                           <td colspan="2" width="750" style="padding:3px 0 3px 0;color:#707070;line-height:20px;font-size:14px;width:100%;">
                              <xsl:if test="grade != ''">
                                 <strong>Grade</strong>: 
                                 <xsl:value-of select="grade" disable-output-escaping="yes" />
                              </xsl:if>
                              <xsl:if test="grade_value != ''">
                                 -
                                 <xsl:value-of select="grade_value" disable-output-escaping="yes" />
                              </xsl:if>
                              <xsl:if test="study_mode != ''">
                                 <br />
                                 <strong>Study mode</strong>: 
                                 <xsl:value-of select="study_mode" disable-output-escaping="yes" />
                              </xsl:if>
                              <xsl:if test="location_type_str != ''">
                                 <br />
                                 <strong>Location type</strong>: 
                                 <xsl:value-of select="location_type_str" disable-output-escaping="yes" />
                              </xsl:if>
                              <xsl:if test="location != ''">
                                 <br />
                                 <strong>Location</strong>: 
                                 <xsl:value-of select="location" disable-output-escaping="yes" />
                              </xsl:if>
                              <xsl:if test="postcode != ''">
                                 <br />
                                 <strong>Postcode</strong>: 
                                 <xsl:value-of select="postcode" disable-output-escaping="yes" />
                              </xsl:if>
                              <xsl:if test="module_search != ''">
                                 <br />
                                 <strong>Module search</strong>: 
                                 <xsl:value-of select="module_search" disable-output-escaping="yes" />
                              </xsl:if>
                              <xsl:if test="employment_rate != ''">
                                 <br />
                                 <strong>Employment rate</strong>: 
                                 <xsl:value-of select="employment_rate" disable-output-escaping="yes" />
                              </xsl:if>
                              <xsl:if test="sorting != ''">
                                 <br />
                                 <strong>Sorting</strong>: 
                                 <xsl:value-of select="sorting" disable-output-escaping="yes" />
                              </xsl:if>
                              <xsl:if test="your_pref != ''">
                                 <br />
                                 <strong>Your preference</strong>: 
                                 <xsl:value-of select="your_pref" disable-output-escaping="yes" />
                              </xsl:if>
                              <xsl:if test="campus_type != ''">
                                 <br />
                                 <strong>Campus type</strong>: 
                                 <xsl:value-of select="campus_type" disable-output-escaping="yes" />
                              </xsl:if>
                           </td>
                        </tr>
                     </xsl:if>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:20px;">
                        <hr width="100%" style="width:100%;height:1px;margin-left: auto;margin-right: auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- User seached section End -->
               <!-- I want to be section Start -->
               <xsl:if test="my_details/previous_study_iwtb">
                  <tr>
                     <td colspan="2" style="padding-top:40px;font-size:24px;color: #707070;font-family:Lato-Bold;">I want to be search</td>
                  </tr>
                  <xsl:for-each select="my_details/previous_study_iwtb">
                     <xsl:if test="entered_date != ''">
                        <tr>
                           <td colspan="2" style="padding-top:23px;color:#c0c0c0;line-height:20px;font-size:12px;">
                              SEARCHED ON
                              <xsl:value-of select="entered_date" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="display_name != ''">
                        <tr>
                           <td colspan="2" style="padding:8px 0 4px 0;color:#707070;line-height:20px;font-size:14px;font-family:Lato-Bold">
                              <xsl:value-of select="display_name" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="year_of_entry != ''">
                        <tr>
                           <td colspan="2" style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-size:14px;">
                              I'm in: Year 
                              <xsl:value-of select="year_of_entry" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- I want to be section End -->
               <!-- What course i do section Start -->
               <xsl:if test="my_details/previous_study_wcid">
                  <tr>
                     <td colspan="2" style="padding-top:40px;font-size:24px;color: #707070;font-family:Lato-Bold;">Previously studied subjects</td>
                  </tr>
                  <xsl:for-each select="my_details/previous_study_wcid">
                     <xsl:if test="entered_date != ''">
                        <tr>
                           <td colspan="2" style="padding-top:23px;color:#c0c0c0;line-height:20px;font-size:12px;">
                              SEARCHED ON
                              <xsl:value-of select="entered_date" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="subject != ''">
                        <tr>
                           <td colspan="2" style="padding:8px 0 4px 0;color:#707070;line-height:20px;font-size:14px;font-family:Lato-Bold">
                              <xsl:value-of select="subject" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="grade != ''">
                        <tr>
                           <td colspan="2" style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-size:14px;">
                              <xsl:value-of select="grade" disable-output-escaping="yes" />
                              <xsl:if test="grade_point != ''">
                                 -
                                 <xsl:value-of select="grade_point" disable-output-escaping="yes" />
                              </xsl:if>
                           </td>
                        </tr>
                     </xsl:if>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- What course i do section End -->
               <!-- Article section Start -->
               <xsl:if test="my_details/article_read">
                  <tr>
                     <td colspan="2" style="padding-top:40px;font-size:24px;color: #707070;font-family:Lato-Bold;">Articles read</td>
                  </tr>
                  <xsl:for-each select="my_details/article_read">
                     <xsl:if test="read_date != ''">
                        <tr>
                           <td colspan="2" style="padding-top:23px;color:#c0c0c0;line-height:20px;font-size:12px;">
                              READ ON
                              <xsl:value-of select="read_date" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="article_title != ''">
                        <tr>
                           <td colspan="2" width="100%" style="padding:8px 0 0;color:#707070;line-height:20px;font-size:14px;font-family:Lato-Bold">
                              <xsl:value-of select="article_title" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- Article section End -->
               <!-- Article pdf section Start -->
               <xsl:if test="my_details/article_pdf">
                  <tr>
                     <td colspan="2" style="padding-top:40px;font-size:24px;color: #707070;font-family:Lato-Bold;">Articles pdf downloads</td>
                  </tr>
                  <xsl:for-each select="my_details/article_pdf">
                     <xsl:if test="downloaded_date != ''">
                        <tr>
                           <td colspan="2" style="padding-top:23px;color:#c0c0c0;line-height:20px;font-size:12px;">
                              DOWNLOADED ON
                              <xsl:value-of select="downloaded_date" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="guide_name != ''">
                        <tr>
                           <td colspan="2" width="100%" style="padding:8px 0 0;color:#707070;line-height:20px;font-size:14px;font-family:Lato-Bold">
                              <xsl:value-of select="guide_name" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- Article pdf section End -->
               <!-- Sponser provider webclick section Start -->
               <xsl:if test="my_details/article_webclick">
                  <tr>
                     <td style="padding-top:25px;font-size:24px;color: #707070;font-family:Lato-Bold;" colspan="2">Article webclick</td>
                  </tr>
                  <xsl:for-each select="my_details/article_webclick">
                     <tr>
                        <td colspan="2" style="padding-top:23px">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;" valign="top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td width="675" style="padding-left:15px;" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="enquiry_date">
                                          <tr>
                                             <td style="color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                CLICKED ON
                                                <xsl:value-of select="enquiry_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:if test="webform_url">
                                          <tr>
                                             <td style="color:#707070;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                <xsl:value-of select="webform_url" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- Sponser provider webclick section End -->
               <!-- User reviews section Start -->
               <xsl:if test="my_details/my_reviews">
                  <tr>
                     <td colspan="2" style="padding-top:40px;font-size:24px;color: #707070;font-family:Lato-Bold;">Reviews written</td>
                  </tr>
                  <xsl:for-each select="my_details/my_reviews">
                     <tr>
                        <td colspan="2" style="padding-top:23px;">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;vertical-align:top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td style="padding-left:15px;vertical-align:top" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="written_date">
                                          <tr>
                                             <td style="color:#c0c0c0;line-height:20px;font-size:12px;">
                                                WRITTEN ON
                                                <xsl:value-of select="written_date" disable-output-escaping="yes" />
                                                <xsl:if test="status">
                                                   (
                                                   <xsl:value-of select="status" disable-output-escaping="yes" />
                                                   )
                                                </xsl:if>
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_name">
                                                <tr>
                                                   <td style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                     <xsl:for-each select="reviews">
                        <tr>
                           <td colspan="2" style="padding-top:30px;font-family:Lato-Regular;color:#707070;width:100%">
                              <span style="display: inline-block;width:100%;" />
                              <xsl:value-of select="category" disable-output-escaping="yes" />
                              <span style="display: inline-block;padding-left:10px">
                                 <xsl:value-of select="rating" disable-output-escaping="yes" />
                              </span>
                           </td>
                        </tr>
                        <xsl:if test="category_question">
                           <tr>
                              <td colspan="2" style="padding:8px 0px;font-size:14px;color:#707070;font-family:Lato-Bold;line-height:18px">
                                 <xsl:value-of select="category_question" disable-output-escaping="yes" />
                              </td>
                           </tr>
                        </xsl:if>
                        <xsl:if test="comments">
                           <tr>
                              <td colspan="2" style="font-size:14px;color:#707070;line-height:18px;">
                                 <xsl:value-of select="comments" disable-output-escaping="yes" />
                              </td>
                           </tr>
                        </xsl:if>
                     </xsl:for-each>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- User reviews section End -->
               <!-- User read reviews section Start -->
               <xsl:if test="my_details/read_review">
                  <tr>
                     <td colspan="2" style="padding-top:25px;font-size:24px;color: #707070;font-family:Lato-Bold;">Reviews read</td>
                  </tr>
                  <xsl:for-each select="my_details/read_review">
                     <tr>
                        <td colspan="2" style="padding-top:23px;">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;vertical-align:top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td style="padding-left:15px;vertical-align:top" valign="top">
                                    <table cellpadding="0" cellspacing="0" width="100%" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="viewed_date">
                                          <tr>
                                             <td style="color:#c0c0c0;line-height:20px;font-size:12px;">
                                                VIEWED ON
                                                <xsl:value-of select="viewed_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_name">
                                                <tr>
                                                   <td style="padding:4px 0 8px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted course<br></br>
                                                   The Course you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2" style="padding-top:30px;font-family:Lato-Regular;color:#707070;width:100%">
                           <span style="display: inline-block;width:100%;" />
                           Overall Rating
                           <span style="display: inline-block;padding-left:10px">
                              <xsl:value-of select="rating" disable-output-escaping="yes" />
                           </span>
                        </td>
                     </tr>
                     <xsl:if test="category_question">
                        <tr>
                           <td colspan="2" style="padding:8px 0px;font-size:14px;color:#707070;font-family:Lato-Bold;line-height:18px">
                              <xsl:value-of select="category_question" disable-output-escaping="yes" />
                           </td>
                        </tr>
                     </xsl:if>
                     <xsl:if test="comments">
                        <tr>
                           <td colspan="2" style="font-size:14px;color:#707070;line-height:18px;">
                              <xsl:value-of select="comments" disable-output-escaping="yes" />
                              <xsl:if test="view_more">
                                 <xsl:value-of select="view_more" disable-output-escaping="yes" />
                              </xsl:if>
                           </td>
                        </tr>
                     </xsl:if>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- User read reviews section End -->
               <!-- Timeline section Start -->
               <xsl:if test="my_details/timeline_steps">
                  <tr>
                     <td colspan="2" style="padding-top:25px;font-size:24px;color: #707070;font-family:Lato-Bold;">Timeline steps</td>
                  </tr>
                  <xsl:for-each select="my_details/timeline_steps">
                     <tr>
                        <td colspan="2" style="padding-top:23px;color:#c0c0c0;line-height:20px;font-size:12px;">
                           <xsl:choose>
                              <xsl:when test="completed_date != ''">
                                 COMPLETED ON
                                 <xsl:value-of select="completed_date" disable-output-escaping="yes" />
                              </xsl:when>
                              <xsl:otherwise>NOT YET COMPLETED</xsl:otherwise>
                           </xsl:choose>
                        </td>
                     </tr>
                     <xsl:if test="step_name">
                        <tr>
                           <xsl:choose>
                              <xsl:when test="completed_date != ''">
                                 <td colspan="2" style="padding:8px 0 0;color:#707070;line-height:20px;font-size:14px;font-family:Lato-Bold">
                                    <xsl:value-of select="step_name" disable-output-escaping="yes" />
                                 </td>
                              </xsl:when>
                              <xsl:otherwise>
                                 <td colspan="2" style="padding:8px 0 0;color:#c0c0c0;line-height:20px;font-size:14px;font-family:Lato-Bold">
                                    <xsl:value-of select="step_name" disable-output-escaping="yes" />
                                 </td>
                              </xsl:otherwise>
                           </xsl:choose>
                        </tr>
                     </xsl:if>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- Timeline section End -->
               <!-- Non advertiser pdf download section Start -->
               <xsl:if test="my_details/non_adv_pdf">
                  <tr>
                     <td style="padding-top:25px;font-size:24px;color: #707070;font-family:Lato-Bold;" colspan="2">University help pack download</td>
                  </tr>
                  <xsl:for-each select="my_details/non_adv_pdf">
                     <tr>
                        <td colspan="2" style="padding-top:23px;">
                           <table cellpadding="0" cellspacing="0" style="width:100%;">
                              <tr>
                                 <td style="width:74px;" valign="top">
                                    <xsl:if test="logo_path">
                                       <xsl:value-of select="logo_path" disable-output-escaping="yes" />
                                    </xsl:if>
                                 </td>
                                 <td style="padding-left:15px;">
                                    <table cellpadding="0" cellspacing="0" align="left" style="font-family:Lato-Regular">
                                       <xsl:if test="enquiry_date">
                                          <tr>
                                             <td style="color:#c0c0c0;line-height:20px;font-family:Lato-Regular;font-size:12px;">
                                                DOWNLOADED ON
                                                <xsl:value-of select="enquiry_date" disable-output-escaping="yes" />
                                             </td>
                                          </tr>
                                       </xsl:if>
                                       <xsl:choose>
                                          <xsl:when test="del_course_flag != 'Y'">
                                             <xsl:if test="course_name">
                                                <tr>
                                                   <td style="padding:8px 0 4px 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="course_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted course<br></br>
                                                   The Course you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                       <xsl:choose>
                                          <xsl:when test="del_college_flag != 'Y'">
                                             <xsl:if test="college_name">
                                                <tr>
                                                   <td style="padding:4px 0 0;color:#707070;line-height:20px;font-family:Lato-Bold;font-size:14px;">
                                                      <xsl:value-of select="college_name" disable-output-escaping="yes" />
                                                   </td>
                                                </tr>
                                             </xsl:if>
                                          </xsl:when>
                                          <xsl:otherwise>
                                             <tr>
                                                <td style="color:#c0c0c0;padding:8px 0 4px 0;line-height:20px;font-family:Lato-Regular;font-size:14px;">
                                                   Deleted institution<br></br>
                                                   The Institution you selected is no longer available on Whatuni.com
                                                </td>
                                             </tr>
                                          </xsl:otherwise>
                                       </xsl:choose>
                                    </table>
                                 </td>
                              </tr>
                           </table>
                        </td>
                     </tr>
                  </xsl:for-each>
                  <tr>
                     <td colspan="2" style="padding-top:25px;">
                        <hr width="100%" style="width:100%;height:1px;margin:0 auto;background-color:#e2e2e2;color:#e2e2e2;border: 0 none;" />
                     </td>
                  </tr>
               </xsl:if>
               <!-- Non advertiser pdf download section Start -->
            </table>
         </body>
      </html>
   </xsl:template>
</xsl:stylesheet>