<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method='xml' version='1.0' encoding='UTF-8' indent='yes'/>
<xsl:template match="/">
<html>
<head>
<style>
body{padding:0;margin:0;font-family:Calibri, Arial, sans-serif;line-height:24px;font-size:13px}
.page{min-height:845px;width:100%;padding-bottom:0px;padding-left:40px; padding-right:40px;padding-top:0px;}
.page1{padding:0px;}
.page1 img {max-width:100%; width:100%;}
.page2, .page3, .page4, .page8, .page11 ,.page12, .page13,.stable {page-break-before: always; padding-top:20px;}
.cntr{width:100%;margin:0 auto}
h1,h2,h3,h4,h5,h6,p,th,td, div,span{margin:0;padding:0;color:#707070}
a{color:#01B8FA}
h1{font-size:45px;padding:10px 0; line-height: 50px}
h2{font-size:20px;padding:10px 0;line-height:20px;}
h3{font-size:18px; padding-bottom: 10px;}
hr{border-top:1px solid #707070;height:1px}
p{line-height:18px;padding-bottom:15px}
strong{font-size:13px;padding-bottom:10px;color:#707070}
table{width:100%;padding-bottom:15px}
th,td{text-align:left;padding:7px 15px; font-size:13px;line-height:18px;}
th{background:#707070;color:#fff; font-weight:normal;}
table {width:100%}
table tr:nth-child(3n+3) td{background:#F0F0F0}	
.graybg{background:#CCC;padding:20px;width:100%;}
.lgbx{width:150px;background:#fff;box-sizing:border-box;padding:10px;height:150px;border:1px solid #cecece;flaot:left;}
.lgbx img{width:100%;vertical-align:middle;display:inline-block}
.lgrgt{float:right;width:400px; position: relative; height: 170px}
.lgrgt h2,.ucsbx h2{margin:0;padding:0}
h4 {font-size:14px; font-weight:normal;}
.ucsbx{background:#fff;width:150px;box-sizing:border-box;padding:10px;float:left;position: absolute; bottom: 0;}
.ucsbx h2{font-size:36px;font-weight:normal}
.ucsbx h2 span{font-size:14px;display:block}
.uc{text-transform:uppercase;padding-top:5px;font-size:13px; padding-bottom:0px;}
.page11 img{width:100%}
.w50 {width: 50%;}
.fl {float: left;}
.fr{float: right;}
.social {padding: 50px 0; font-size: 25px; font-weight: bold;}
.social a {text-decoration: none;}
.social img {vertical-align: middle; padding-right: 20px; width:70px;}
.button {height:40px;width:200px;border: 1px solid #fff;font-size: 16px;font-weight: 100;display: block;text-transform: uppercase;color: #fff;background: #00bbfd; width: 200px; float: left;text-decoration: none; font-weight: bold;}
.button span {color: #fff;}
.links a {font-size: 18px;text-decoration: none;}
.links span{float: right;}
.links h4 {font-size: 24px;padding-bottom: 15px;font-weight: normal;padding-top: 15px;}
.links h3 {font-size: 30px;padding-top: 15px;padding-bottom: 15px;}
.chart,.chart img {width: 100%;}
.w33 {width: 33%;}
.w33 h4 {font-weight: normal; font-size: 14px;}
.w33 p {font-size: 25px; font-weight: normal}
.indx tr td {padding-left:0;}
table.indx tr td a{text-decoration:none}
tr.bg1 td {background:#F0F0F0;}
</style>
</head>

<body>
  <div class="cntr">
    <div class="page page1"> <img src="http://www.whatuni.com/wu-cont/html/pdf/home.png?" alt=""/>
    </div>
    <div class="page page3">
	  <table width="100%" height="100" style="width:100%;margin-bottom:30px;" class="graybg">
		<tr>
		  <td width="140" style="width:140px;" class="lgbx">
		    <xsl:value-of select="ConvertXMLtoPDF/headerPod/providerLogo" disable-output-escaping="yes"/>
		  </td>		
		  <td width="400" style="width:400px;"  class="lgrgt">
		    <table>
		      <tr>
		        <td colspan="2">
				  <h2><xsl:value-of select="ConvertXMLtoPDF/headerPod/courseTitle" disable-output-escaping="yes"/></h2>
                  <h4><xsl:value-of select="ConvertXMLtoPDF/headerPod/providerName" disable-output-escaping="yes"/></h4>
				  <xsl:if test="ConvertXMLtoPDF/headerPod/studentRating">
				    <p>
					  Student rating <xsl:value-of select="ConvertXMLtoPDF/headerPod/roundRating" disable-output-escaping="yes"/>
				      <xsl:if test="ConvertXMLtoPDF/headerPod/actualRating">(<xsl:value-of select="ConvertXMLtoPDF/headerPod/actualRating" />) </xsl:if>
				      <xsl:value-of select="ConvertXMLtoPDF/headerPod/studentRating" disable-output-escaping="yes"/>
					</p>
				  </xsl:if>
				</td>                 
              </tr>
			  <xsl:if test="ConvertXMLtoPDF/headerPod/ucasCode">
                <tr>
		          <td width="150" style="width:150px; padding-left:15px;">
		            <table>
		              <tr>
		                <td class="ucsbx">
		                  <p style="padding-bottom:0">UCAS CODE</p><h2 style="padding-top:0px"><xsl:value-of select="ConvertXMLtoPDF/headerPod/ucasCode"/></h2>
		                </td>
		              </tr>
		            </table>
		          </td>     
		          <td width="250" style="width:250px;"></td>                    
                </tr>
			  </xsl:if>
            </table>
		  </td>
		</tr>
	  </table>
	  <xsl:if test="ConvertXMLtoPDF/intro">
		<strong id="cd">Course description</strong>
		<br></br>
		<xsl:value-of select="ConvertXMLtoPDF/intro" disable-output-escaping="yes"/>
	  </xsl:if>
      <!--Get the course description count for adding class for alignment by Prabha on 24-Nov-2015-->
	  <xsl:variable name="temp" select="ConvertXMLtoPDF/intro"/>
	  <xsl:variable name="className">
        <xsl:choose>
          <xsl:when test="string-length($temp) &gt; 2000">stable</xsl:when>
          <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
	  <!--End of the adding a class-->
	  <xsl:if test="ConvertXMLtoPDF/studyOptions">
        <p style="padding-top:10px;" class="{$className}"><strong id="so" >Study options</strong></p>
		<table cellpadding="0" cellspacing="0" border="0" width="100%" class="">
          <tr>
            <th style="width:100px" width="100">STUDY MODE</th>
            <th style="width:100px" width="100">CAMPUS</th>
            <th style="width:100px" width="100">DURATION</th>
            <th style="width:100px" width="100">STARTS</th>
          </tr>
		  <xsl:for-each select="ConvertXMLtoPDF/studyOptions">
		  	<tr>
			  <!--Added for adding style in mod 2 rows-->
              <xsl:choose>
			    <xsl:when test="(position() mod 2) = 0">
				  <xsl:attribute name="bgcolor">#CCC</xsl:attribute>
				  <xsl:attribute name="style">background:#CCC</xsl:attribute>
				</xsl:when>
			  </xsl:choose>	
              <!--End of the adding style-->			  
			  <td><xsl:value-of select="studyLevel" disable-output-escaping="yes"/></td>
			  <td><xsl:value-of select="campus"/></td>
			  <td><xsl:value-of select="duration"/></td>
			  <td><xsl:value-of select="starts"/></td>
		    </tr>
          </xsl:for-each>
        </table>
	  </xsl:if>
	 
	   <xsl:if test="ConvertXMLtoPDF/entryreqLink">
	   <br></br>
		<p style="padding-top:10px;"><strong id="er" style="font-size:18px;">Entry requirements</strong><br></br><br></br>
		<xsl:value-of select="ConvertXMLtoPDF/entryreqLink" disable-output-escaping="yes"/>
		</p>
	  </xsl:if>
	  <xsl:if test="ConvertXMLtoPDF/feesLink">
       <!-- <p style="padding-top:10px;" ><strong id="so" >Tuition fees</strong></p>
		<table cellpadding="0" cellspacing="0" border="0" width="100%" class="">
          <tr>
            <th style="width:100px" width="100">STUDY MODE</th>
            <th style="width:100px" width="100">HOME FEES</th>
            <th style="width:100px" width="100">REST OF UK FEES</th>
            <th style="width:100px" width="100">EU FEES</th>
          </tr>
		  <xsl:for-each select="ConvertXMLtoPDF/TuitionFees">
		  	<tr>-->
			  <!--Added for adding style in mod 2 rows-->
           <!--   <xsl:choose>
			    <xsl:when test="(position() mod 2) = 0">
				  <xsl:attribute name="bgcolor">#CCC</xsl:attribute>
				  <xsl:attribute name="style">background:#CCC</xsl:attribute>
				</xsl:when>
			  </xsl:choose>	-->
              <!--End of the adding style-->			  
			  <!--<td><xsl:value-of select="studyLevel" disable-output-escaping="yes"/></td>
			  <td><xsl:value-of select="homeFees"/></td>
			  <td><xsl:value-of select="restOfFees"/></td>
			  <td><xsl:value-of select="EUFees"/></td>
		    </tr>
          </xsl:for-each>
        </table>--><br></br>
		<p style="padding-top:10px;"><strong id="er" style="font-size:18px;">Course fees</strong><br></br><br></br>
		<xsl:value-of select="ConvertXMLtoPDF/feesLink" disable-output-escaping="yes"/>
		</p>
	  </xsl:if>
	  
	</div>
    
	
	
	<!--Module data-->
	
	
	
	<xsl:if test="ConvertXMLtoPDF/module">
	  <div class="page page5">
	    <strong id="hya" style="font-size:18px;">Modules</strong>
	  </div>
	  <div class="page page5">
	    <xsl:for-each select="ConvertXMLtoPDF/module">
	      <h4 style="font-size:18px; padding-top:10px; font-weight:bold"><xsl:value-of select="moduleYear"/></h4>
		  <xsl:value-of select="moduleIntro" disable-output-escaping="yes"/>
			<!--<xsl:value-of select="modulegrpDesc" disable-output-escaping="yes"/>-->
		  <xsl:for-each select="title">
		    <xsl:if test="moduleTitle">
	        <strong><xsl:value-of select="moduleTitle"/></strong>
			<br></br>
			</xsl:if>
	        <xsl:value-of select="moduleDes" disable-output-escaping="yes"/>
	      </xsl:for-each>
	    </xsl:for-each>
	  </div>
	</xsl:if>
	<!--End of module data-->
	<!--Review data-->
   
	  <div class="page page8">
	    <table width="100%" height="100" style="width:100%;" class="graybg">
          <tr>
            <td width="140" style="width:140px;" class="lgbx"><xsl:value-of select="ConvertXMLtoPDF/headerPod/providerLogo" disable-output-escaping="yes"/></td>
			<td width="400" style="width:400px;"  class="lgrgt">
              <table>
                <tr>
                  <td colspan="3">
                    <h2><xsl:value-of select="ConvertXMLtoPDF/headerPod/providerName" disable-output-escaping="yes"/></h2>
                    <xsl:if test="ConvertXMLtoPDF/headerPod/studentRating">
				      <p>
					    Student rating <xsl:value-of select="ConvertXMLtoPDF/headerPod/roundRating" disable-output-escaping="yes"/>
				        <xsl:if test="ConvertXMLtoPDF/headerPod/actualRating">(<xsl:value-of select="ConvertXMLtoPDF/headerPod/actualRating" />) </xsl:if>
				        <xsl:value-of select="ConvertXMLtoPDF/headerPod/studentRating" disable-output-escaping="yes" />
					  </p>
				    </xsl:if>
                  </td>
                </tr>
				<xsl:if test="ConvertXMLtoPDF/ranking">
                  <tr>
				    <xsl:if test="ConvertXMLtoPDF/ranking/whatuniRank">
                      <td class="" width="100" style="width:100px;">
                        <h4 class="uc">WHATUNI RANKING</h4>
                        <xsl:value-of select="ConvertXMLtoPDF/ranking/whatuniRank" disable-output-escaping="yes"/>
                      </td>
				    </xsl:if>
				    <xsl:if test="ConvertXMLtoPDF/ranking/timesRank">
                      <td width="100" style="width:100px;">
			            <h4 class="uc">COMPLETE UNIVERSITY GUIDE RANKING</h4>
                        <xsl:value-of select="ConvertXMLtoPDF/ranking/timesRank" disable-output-escaping="yes"/>
			          </td>
				    </xsl:if>
				    <xsl:if test="ConvertXMLtoPDF/ranking/totalStudents">
			          <td style="width:100px;">
			            <h4 class="uc">TOTAL UNDERGRADUATE STUDENTS</h4>
                        <xsl:value-of select="ConvertXMLtoPDF/ranking/totalStudents" disable-output-escaping="yes"/>
			          </td>
				    </xsl:if>
			      </tr>
				</xsl:if>
              </table>
            </td>
	      </tr>
        </table>
	  </div>
	  <!--View all courses button-->
	<xsl:if test="ConvertXMLtoPDF/viewCourseLink">
	  <div class="page page9">
	    <table>
	  	  <tr>			
		    <td>&#160;</td>
		    <td>&#160;</td>
		    <td style="width:250px; height:50px; padding-top:15px;" align="right">			   
			  <a href="#" style="width:250px; height:50px; text-align:left;color: #fff;background:#00bbfd;font-weight:bold;padding-left:20px;text-decoration:none; text-transform:uppercase;  ">
			    <table width="200" style="width:250px;height:50px;">
                  <tbody>
				    <tr>
				      <td width="250px" align="left">
                      <xsl:value-of select="ConvertXMLtoPDF/viewCourseLink" disable-output-escaping="yes"/>
						
				      </td>
                      <td align="right" width="50px"><a href="#" style="font-weight: bold;text-align: right;text-decoration: none;color: #FFFFFF; display:block; width:50px; height:100%;line-height:24px; padding-left:0px;font-family: Myriad Pro,Arial;text-transform:uppercase; font-size:24px;">  <img src="http://www.whatuni.com/wu-cont/images/newsletter/mail/arrow.jpg?" width="18" class="resimg" align="right" style="vertical-align: middle; padding-left:40px; width:18px;" alt="" /></a></td>
				    </tr>
                  </tbody>
			    </table>
		      </a>
		    </td>
		    <td width="10" style="width:10px;">&#160;</td>
		  </tr>
	    </table>
	  </div>
</xsl:if>
	<!--End of view all courses-->
	<xsl:if test="ConvertXMLtoPDF/location">
	    <div class="page page9">
        <!--Loaction-->
          <h3 id="wwa">Location</h3>
			
			<xsl:if test="ConvertXMLtoPDF/location/address">		
          <div class="w50 fl">
            <xsl:value-of select="ConvertXMLtoPDF/location/address" disable-output-escaping="yes"/>
			
          </div>
		  </xsl:if>
			<div class="w50 fl">
			<xsl:if test="ConvertXMLtoPDF/location/nearByTubeStation">
        NEAREST TUBE STATION:<br />
		   <xsl:value-of select="ConvertXMLtoPDF/location/nearByTubeStation" disable-output-escaping="yes"/>
		   <br></br>
			 <br></br>
			 </xsl:if>
      <xsl:if test="ConvertXMLtoPDF/location/nearByStation">
      NEAREST TRAIN STATION:<br />
		  <xsl:value-of select="ConvertXMLtoPDF/location/nearByStation" disable-output-escaping="yes"/>
		  <br></br>
			<a href="www.thetrainline.com" style="text-decoration:none">Book a train ticket </a>	 
       </xsl:if>
			</div>
         <!--Loaction-->
        </div>
	  </xsl:if>
	
		
	  <div class="page page9">
	    <xsl:if test="ConvertXMLtoPDF/aboutProvider">
	      <h3 id="wwa" style="padding-top:15px;">What we say about <xsl:value-of select="ConvertXMLtoPDF/headerPod/providerName" disable-output-escaping="yes"/></h3>
	      <xsl:value-of select="ConvertXMLtoPDF/aboutProvider" disable-output-escaping="yes"/>
		</xsl:if>
		<xsl:if test="ConvertXMLtoPDF/studentSays">
		<h3 id="wss" style="padding-top:15px;">What students say about <xsl:value-of select="ConvertXMLtoPDF/headerPod/providerName" disable-output-escaping="yes"/></h3>
		<xsl:for-each select="ConvertXMLtoPDF/studentSays">
	      <xsl:value-of select="saysAbout" disable-output-escaping="yes"/>
		  <xsl:if test="overviewRating">
	        <p class="uc">OVERALL UNIVERSITY RATING <xsl:value-of select="overviewRating" disable-output-escaping="yes"/></p>
	        <xsl:value-of select="overviewComments" disable-output-escaping="yes"/>
		  </xsl:if>
	      <xsl:if test="jobRating"> 
	        <p class="uc">JOB PROSPECTS <xsl:value-of select="jobRating" disable-output-escaping="yes"/></p>
	        <xsl:value-of select="jobComments" disable-output-escaping="yes"/>
	      </xsl:if>  
	      <xsl:if test="courseRating">  
	        <p class="uc">COURSE AND LECTURERS <xsl:value-of select="courseRating" disable-output-escaping="yes"/></p>
	        <xsl:value-of select="courseComments" disable-output-escaping="yes"/>
	      </xsl:if>  
	      <xsl:if test="studentRating"> 
	        <p class="uc">STUDENT UNION <xsl:value-of select="studentRating" disable-output-escaping="yes"/></p>
	        <xsl:value-of select="studentComments" disable-output-escaping="yes"/>
	      </xsl:if> 
	      <xsl:if test="accomodationRating"> 
	        <p class="uc">ACCOMMODATION <xsl:value-of select="accomodationRating" disable-output-escaping="yes"/></p>
	        <xsl:value-of select="accomodationComments" disable-output-escaping="yes"/>
	      </xsl:if> 
	      <xsl:if test="uniRating">  
	        <p class="uc">UNI FACILITIES <xsl:value-of select="uniRating" disable-output-escaping="yes"/></p>
	        <xsl:value-of select="uniComments" disable-output-escaping="yes"/>
	      </xsl:if>
	      <xsl:if test="cityRating"> 
	        <p class="uc">CITY LIFE <xsl:value-of select="cityRating" disable-output-escaping="yes"/></p>
	        <xsl:value-of select="cityComments" disable-output-escaping="yes"/>
	      </xsl:if> 
	      <xsl:if test="clubRating">
	        <p class="uc">CLUBS AND SOCIETIES <xsl:value-of select="clubRating" disable-output-escaping="yes"/></p>
	        <xsl:value-of select="clubComments" disable-output-escaping="yes"/>
	      </xsl:if> 
	      <xsl:if test="supportRating">
	        <p class="uc">STUDENT SUPPORT <xsl:value-of select="supportRating" disable-output-escaping="yes"/></p>
	        <xsl:value-of select="supportComment" disable-output-escaping="yes"/>
	      </xsl:if> 
		  <xsl:if test="recommendUni">
	        <p class="uc">Having said what you have said would you recommend your uni?</p>
		    <table>
	          <tr>
			    <td align="left" style="padding:0px;">
			      <xsl:value-of select="recommendUni" disable-output-escaping="yes"/>
			    </td>
			  </tr>
		    </table>
	      </xsl:if>
	    </xsl:for-each>
	    <xsl:value-of select="ConvertXMLtoPDF/moreReview" disable-output-escaping="yes"/>
			</xsl:if>
	  </div>
	
	<!--End of review data-->
	<!--Other courses-->
	<xsl:if test="ConvertXMLtoPDF/otherCoursesPod">
	  <div class="page page12">
        <h3 id="ocy" style="font-size:22px;">Other courses you might like</h3>
		<xsl:for-each select="ConvertXMLtoPDF/otherCoursesPod/otherCourses">
		  <table width="100%" height="100" style="width:100%;" class="graybg">
            <tr>
              <td width="140" style="width:140px;" class="lgbx">
				<xsl:value-of select="providerLogo" disable-output-escaping="yes"/>
			  </td>
              <td width="400" style="width:400px;"  class="lgrgt">
                <table>
                  <tr>
                    <td colspan="3">
			   		  <h2><xsl:value-of select="courseTitle" disable-output-escaping="yes"/></h2>
			          <h4><xsl:value-of select="providerName" disable-output-escaping="yes"/></h4>
					  <xsl:if test="studentRating">
				        <p>
						  Student rating <xsl:value-of select="roundRating" disable-output-escaping="yes"/>
				          <xsl:if test="actualRating">(<xsl:value-of select="actualRating" />) </xsl:if>
				          <xsl:value-of select="studentRating" disable-output-escaping="yes"/>
						</p>
				      </xsl:if>
				    </td>
                  </tr>
                  <tr>
									<xsl:if test="ucasCode">
                    <td width="125" style="width:125px; padding-left:15px;">
		              <table>
		                <tr>
		                  <td class="ucsbx"><p style="padding-bottom:0">UCAS CODE</p><h2 style="padding-top:0px"><xsl:value-of select="ucasCode"/></h2></td>
		                </tr>
		              </table>
		            </td>
								</xsl:if>
                    <td width="50" style="width:50px;"></td>
			        <td width="200"> 
					  <table>
                        <tr>
                          <td heihgt="40" style="width:200px; height:40px; text-align:left;color: #fff;background: #00bbfd;padding-left:0px;text-decoration:none; text-transform:uppercase;border-radius:10px;">
			                <a href="#" style="width:200px; height:40px; text-align:left;color: #fff;background:#00bbfd;font-weight:bold;padding-left:20px;text-decoration:none; text-transform:uppercase;  ">
			   			      <table width="200" style="width:200px;">
                                <tbody>
					              <tr>
 								    <td width="150px" align="left">
                                     <xsl:value-of select="courseLink" disable-output-escaping="yes"/>
																			
			                        </td>
			                        <td align="right" width="50px"><a href="#" style="font-weight: bold;text-align: right;text-decoration: none;color: #FFFFFF; display:block; width:50px; height:100%;line-height:24px; padding-left:0px;font-family: Myriad Pro,Arial;text-transform:uppercase; font-size:24px;">  <img src="http://www.whatuni.com/wu-cont/images/newsletter/mail/arrow.jpg?" width="18" class="resimg" align="right" style="vertical-align: middle; padding-left:40px; width:18px;" alt="" /></a></td>
								  </tr>
                                </tbody>
							  </table>
	        		        </a>
			   			  </td>
			            </tr>
                      </table>
			   	    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <table><tr><td height="20"></td></tr></table>
	    </xsl:for-each>
	  </div>
	</xsl:if>
	<!--End of other courses-->
    <div class="page page13">
      <h1 id="git">Get in touch with your questions</h1>
      <table width="100%" style="width:100%">
		<tr>
		  <td width="70" style="width:70px;"><img src="http://www.whatuni.com/wu-cont/html/pdf/tw.png?" valign="middle" width="70" height="68" alt=""/></td>
		  <td class="social"><a href="https://www.twitter.com/whatuni" style="text-decoration:none">@whatuni</a> </td>
		</tr>
      </table>
	  <table width="100%" style="width:100%">
		<tr>
		  <td width="70" style="width:70px;"> <img src="http://www.whatuni.com/wu-cont/html/pdf/fb.png?" valign="middle" width="70" height="68" alt=""/></td>
		  <td class="social"><a href="https://www.facebook.com/whatuni" style="text-decoration:none">facebook.com/whatuni</a> </td>
		</tr>
	  </table>
	  <table width="100%" style="width:100%">
		<tr>
		  <td width="70" style="width:70px;"> <img src="http://www.whatuni.com/wu-cont/html/pdf/email.png?" valign="middle" width="70" height="68" alt=""/></td>
		  <td class="social"><a href="#" style="text-decoration:none">Editor@whatuni.com</a> </td>
		</tr>
	  </table>
    </div>
  </div>
</body>
</html>
</xsl:template>
</xsl:stylesheet>