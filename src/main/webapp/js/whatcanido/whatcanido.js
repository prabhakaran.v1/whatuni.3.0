var contextPath = "/degrees";
var ErrorMessage = { 
  gradesErrMsg : "Please select the grades.",
  subjectErrMsg    : "Please select atleast one subject.",
  subErrorMsg : "Please choose the subject from the list",
  tarrifErrMsg  : "Please enter tariff points.",
  ajaxUniErrMsg : "Please select uni from ajax list.",
  tarrifLimitMsg : "Cannot search for the given points.",
  validTarrifMsg : "Please enter valid tariff points.",
  subjectValErrMsg : "Please choose valid subject from the list."
}
function $$D(id){ return document.getElementById(id); }
function blockNoneDiv(thisid, value){  if($$D(thisid) !=null){ $$D(thisid).style.display = value;  }  }
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}

function setSelectedVal(obj, spanid){  
  var dobid = obj.id
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  $$D(spanid).innerHTML = text;
}
var $fp = jQuery.noConflict();
$fp(window).scroll(function(){ 
    lazyloadetStarts();
});
var selectedQualId = "1";
var selectedQualTypeId = $$D("qualTypeIdStr") ? $$D("qualTypeIdStr").value : "AL";
var selectQualification = "A level";
var wcidSubNo = 3;
function onChageQualGrades(obj){
 var splitVal = (obj.value != '') ? obj.value.split('|') : "";
 selectedQualId = splitVal[0];
 selectedQualTypeId = splitVal[1];
 selectQualification = splitVal[2];
 var qual = splitVal[0];
 var previousWcidSubNo = wcidSubNo;
 wcidSubNo = ($$D(qual+"NoOfSub").value <= 6) ? $$D(qual+"NoOfSub").value : "4";
 if(qual!="0"){
   blockNoneDiv('errorQual', 'none');
 }
 if($$D("clearTarrif")){
  $$D("clearTarrif").innerHTML = '<div class="wcs_subgrd"><label class="wcs_sub">Your subjects</label><label class="wcs_grd">Your grades</label></div>';
  for(var z=0; z < wcidSubNo; z++){
   var itrVal = z + 1;
   $fp('.grd_row').append(getNewFieldValue(itrVal)); 
   if(selectedQualTypeId != "TP"){
    onChageQualGradesSub(selectedQualId,itrVal);
   }
  }
 } 
 $fp("#addSubject").removeClass("wcs_dis");
 blockNoneDiv("gradesError",'none');
 blockNoneDiv("subjectError",'none');
}
function onChageQualGradesSub(selObjVal, selectCount){
  var qualArray;
  if($$D(selObjVal)) {
   qualArray = ($$D(selObjVal).value).split(',');
  }
  $$D("setOptVal"+selectCount).innerHTML = "";
  var gradeSpanCount = selectCount;
  if($$D('gradeSpan'+gradeSpanCount)){
    $$D('gradeSpan'+gradeSpanCount).innerHTML = qualArray[0]; 
  }
  for(var j=0; j<qualArray.length; j++){
    var option = document.createElement("option");
    option.value = qualArray[j];
    option.appendChild(document.createTextNode(qualArray[j]));
    $$D("setOptVal"+selectCount).appendChild(option);
  }
}
function keypressSubmit(obj, event){
 blockNoneDiv("subjectError",'none');
 if( event.keyCode == 13){
  return false;
 }
}
function wcidQualSubjectList(event, obj){
  var qual = selectedQualTypeId;
  var id = obj.id;
  var hiddenId = id+"_hidden";   
  $$(id).style.background = "#fff";
  if($$(id).value == "") {
   $$(id).style.background = "#fff";
  }
  if(($$(hiddenId).value != "" || $$(hiddenId).value != null) && event.keyCode != 13){
   $$D(hiddenId).value = "";
  }  
  if(trimString(obj.value)){
    if(trimString(obj.value) != ""){
      ajax_showOptions(obj,'qualsubects',event,"indicator",'ulqual='+ qual);
    }
  }else{
    $$(hiddenId).value = "";
  }  
}
$fp(document).ready(function(){
  var maxField = 6; 
  var addButton = $fp('#addSubject'); 
  var wrapper = $fp('.grd_row'); 
  var addNewSubFlag = 0;
  var removeId = "";
  $fp(addButton).click(function () { 
    if (wcidSubNo < maxField) { 
      wcidSubNo++;
      var subjectNoArg = wcidSubNo;
      var removedIdArr = removeId.split("/");
      if(addNewSubFlag > 0){
       subjectNoArg = removedIdArr[0];
       removeId = removeId.substring(2);
       addNewSubFlag--;
      }
      $fp(wrapper).append(getFieldValue(subjectNoArg)); 
      if(selectedQualTypeId != "TP"){
       onChageQualGradesSub(selectedQualId,subjectNoArg);
      }
      if(wcidSubNo == maxField){
        $fp(this).addClass("wcs_dis");
      }
      blockNoneDiv("subjectError",'none');
    } 
  });
  $fp(wrapper).on('click', '.btn_act3', function (e) { 
    e.preventDefault();
    $fp("#addSubject").removeClass("wcs_dis");
    $fp(this).parents('.rmv_act').remove(); 
    wcidSubNo--;
    removeId += this.id.substring(7) + "/";
    addNewSubFlag++;
    blockNoneDiv("subjectError",'none');
  });
  $fp('#whatcanidoWidget').click(function(e) {
   var screenWidth = document.documentElement.clientWidth;
   if(screenWidth <= 992 && $$D('toolTipDiv')){
     if(e.target.id != 'toolTipDiv' && e.target.id != 'toolTipDiv1' && e.target.id != 'toolTipDiv2' && e.target.id != 'toolTipDiv3' && e.target.id != 'toolTipDiv4' && e.target.id != 'toolTipDiv5') {
      $fp('.scr_tip').each(function() {
        $fp(".scr_tip").hide();
      });
     }
   }
  });
});
  
function getFieldValue(countSub){
 var gradeSpan_countSub = "gradeSpan"+countSub;
 var yourGrades = getSelectOrTextField(gradeSpan_countSub,countSub);
 var fieldHTML = '<div class="rmv_act"><fieldset class="ucas_fldrw wcs_fldrmv"><div class="add_fld add_qualtxt remv_qualtxt"><a class="btn_act3" id="rmv_act'+countSub+'" href="javascript:void(0);"><i class="fa fa-times"></i></a></div><div class="ucas_tbox"><input type="text" id="wcidSub'+countSub+'" name="wcidSub'+countSub +'" onkeypress="return keypressSubmit(this, event);" onkeyup="wcidQualSubjectList(event, this);" placeholder="Enter subject here..." autocomplete="off" /><input type="hidden" id="wcidSub'+countSub +'_hidden" name="wcidSub'+countSub+'_hidden" /></div><div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox"><span class="sbox_txt" id="gradeSpan'+countSub+'">A</span><i class="fa fa-angle-down fa-lg"></i></div>'+yourGrades+'</div></fieldset></div>'; 
 return fieldHTML;
}

function getNewFieldValue(countSubNew){
 var gradeSpan_countSubNew = "gradeSpan"+countSubNew;
 var yourGrades = getSelectOrTextField(gradeSpan_countSubNew,countSubNew);
 var fieldHTML = '<fieldset class="ucas_fldrw"><div class="ucas_tbox"><input type="text" id="wcidSub'+countSubNew+'" name="wcidSub'+countSubNew +'" onkeypress="return keypressSubmit(this, event);" onkeyup="wcidQualSubjectList(event, this);" placeholder="Enter subject here..." autocomplete="off" /><input type="hidden" id="wcidSub'+countSubNew+'_hidden" name="wcidSub'+countSubNew+'_hidden" /></div><div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox"><span class="sbox_txt" id="gradeSpan'+countSubNew+'">A</span><i class="fa fa-angle-down fa-lg"></i></div>'+yourGrades+'</div></fieldset>'; 
 return fieldHTML;
}
function getSelectOrTextField(gradeSpan,countselOrTxt){
 var onChangeSelectVal = "setSelectedVal(this, '"+gradeSpan+"');";
 var yourGrades = '<select class="ucas_slist" name="setOptVal" id="setOptVal'+countselOrTxt+'" onchange="'+onChangeSelectVal+'"></select>';
 if(selectedQualTypeId == "TP"){
   yourGrades = '<input type="text\" autocomplete="off\" id="tarrifPoints'+countselOrTxt+'" maxlength="3">';
 }
 return yourGrades;
}
var ajaxURLValue = "";
function submitQualification(){
 ajaxURLValue = contextPath + "/what-can-i-do-widget.html?pageName=wcidResults&qualDispalyName=" + selectQualification ;
 ajaxURLValue += "&qualification=" + selectedQualTypeId;
 if(gradesSubmitValidation()){
   var ajax = new sack();
   var url = ajaxURLValue;  
   ajax.requestFile = url;
   ajax.onCompletion = function(){displayWCIDResults(ajax);};
   ajax.runAJAX(); 
   $fp('#wcidSubmit').addClass("load_icon");
 }
}

function gradesSubmitValidation(){
 var submitValue = "";
 var atleastOneSub = false;
 for(var i=1; i<=6; i++){
   if($$D("wcidSub"+i) && $$D("wcidSub"+i+"_hidden")){
     var subVal = $$D("wcidSub"+i).value;
     var subHidVal = $$D("wcidSub"+i+"_hidden").value;
     if(subVal != "" && subHidVal != ""){
       ajaxURLValue += "&qualSub"+i + "=" + subHidVal;
       if(selectedQualTypeId != "TP"){ 
        if($$D('setOptVal'+i) && $$D('setOptVal'+i).value != ""){
          ajaxURLValue += "&qualGrades"+i + "=" + $$D('setOptVal'+i).value;
        } else {
          $$D("gradesError").innerHTML = ErrorMessage.gradesErrMsg;
          blockNoneDiv("gradesError",'block');
          return false;
        }
       } else if(selectedQualTypeId == "TP" && $$D("tarrifPoints"+i)){
         var ucasPoints = $$D("tarrifPoints"+i).value;
         if(ucasPoints != "" && !isNaN(ucasPoints) && ucasPoints > 0) { 
          ajaxURLValue += "&qualGrades"+i + "=" + $$D("tarrifPoints"+i).value;
         } else {
           if(ucasPoints != "" && isNaN(ucasPoints)){
            $$D("gradesError").innerHTML = ErrorMessage.validTarrifMsg;
           } else if(ucasPoints != "" && ucasPoints <= 0){
            $$D("gradesError").innerHTML = ErrorMessage.tarrifLimitMsg;
           } else {
            $$D("gradesError").innerHTML = ErrorMessage.tarrifErrMsg;
           }
           blockNoneDiv("gradesError",'block');
           blockNoneDiv("subjectError",'none');
           return false;
         }
       }
       atleastOneSub = true;
     }else if(selectedQualTypeId == "TP" && $$D("tarrifPoints"+i) && $$D("tarrifPoints"+i).value != "" && (subVal == "" && subHidVal == "")){
       $$D("subjectError").innerHTML = ErrorMessage.subErrorMsg;
       blockNoneDiv("subjectError",'block');
       blockNoneDiv("gradesError",'none');
       return false;    
     } 
     if($$D("wcidSub"+i).value != "" && $$D("wcidSub"+i+"_hidden").value == ""){
       $$D("subjectError").innerHTML = ErrorMessage.subjectValErrMsg;
       blockNoneDiv("subjectError",'block');
       blockNoneDiv("gradesError",'none');
       return false;
     }
   }
 }
 if(!atleastOneSub){
  $$D("subjectError").innerHTML = ErrorMessage.subjectErrMsg;
  blockNoneDiv("subjectError",'block');
  blockNoneDiv("gradesError",'none');
  return false;
 }
 return true;
}
function displayWCIDResults(ajax){
 var response = ajax.response; 
 window.scrollTo(0,0);
 $$D('whatcanidoResults').innerHTML = response;
 blockNoneDiv("whatcanidoHome",'none');
 $fp('#wcidSubmit').removeClass("load_icon");
 blockNoneDiv("whatcanidoResults",'block');
 showToolTipEightSec('typeform','toolTip0');
}
function showToolTipEightSec(parentDiv, childDiv){
 setTimeout(function() {
  if($$D(parentDiv)){
   blockNoneDiv(childDiv,"block"); 
   setTimeout(function() {
    blockNoneDiv(childDiv,"none");
   }, 8000);
  }
 }, 1000);
}
function showToolTip(showId){
  blockNoneDiv(showId,'block');
}
function hideToolTip(showId){
  blockNoneDiv(showId,'none');
}
function goToPreviousPage(){
  window.scrollTo(0,0);
  blockNoneDiv("whatcanidoResults",'none');
  blockNoneDiv("whatcanidoHome",'block');
  blockNoneDiv("gradesError",'none');
  blockNoneDiv("subjectError",'none');
  blockNoneDiv("unicourseDiv",'none');
}

function openNewWindow(link){
  window.open(link, "_blank");
}
function ulGAEventTracking(category, action, label, gaaccount){
  var eventCategory = 'widget-what-can-i-do';
  var eventAction = 'view degrees button';
  if(action == 'SG'){
    eventAction = 'subject guide button';
  }
  var eventLabel = label.toLowerCase();
  var eventVal = 1;
  var booleanValue = true;  
  ga('create', gaaccount , 'auto', {'sampleRate': 80 , 'name': 'salesTracker'});
  ga('salesTracker.send', 'event', eventCategory , eventAction , eventLabel , eventVal , {nonInteraction: booleanValue}); 
}
function ulSearchResults(jacsSubject,jacsCode, searchGrades, entryLevel){
  var navKwd = trimString(jacsSubject);
  var qualDesc = 'degree';
  var srGrades = searchGrades;
  var srQual = entryLevel;  
  var contextPath='/degrees';
  var url1 = "/"+qualDesc+"-courses/search?jacs="+jacsCode;
  var url2='';
  if(srQual!="0" && srGrades!="0"){
    url2 = "&entry-level="+srQual+"&entry-points="+srGrades;
  }  
  var finalUrl = url1 + url2;
  finalUrl = finalUrl.toLowerCase();
  window.open(finalUrl,'_blank');
	return false;
}
//
function getJobInfo(jacsCode, logId, entryLevel, entryPoints, tariffPoint){
 if(entryLevel == 'TP'){
    tariffPoint = entryPoints;
  }
  clearDiv('unicourseDiv');
  blockNoneDiv('loadingIcon', 'block');
  blockNoneDiv('whatcanidoResults','none');
  var url = contextPath + "/what-can-i-do-widget.html?pageName=UNI-RESULTS&";
  url += "jacsCode=" + jacsCode +"&logId=" + logId + "&tariffPoint=" + tariffPoint +"&entryLevel=" + entryLevel +"&entryPoints=" + entryPoints;
  ajaxCall(url, 'unicourseDiv');
}
function ajaxCall(url, id){
  var ajax = new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){unicourseResults(ajax, id);};
  ajax.runAJAX(); 
}
function getSortingResult(pageNo, sortType, sortOrder, flag, collegeId, ukprn, empFlag){
  blockNoneDiv("loadingIcon", "block");
  blockNoneDiv("unicourseDiv", "none");
  window.scrollTo(0,0);
  var jacsCode = $$D('jacsCode').value;
  var logId = $$D('logId').value;
  var entryLevel = $$D('entryLevel').value;
  var entryPoints = $$D('entryPoints').value;
  var tariffPoint = $$D('tariffPoint').value;
  if(flag != "YES"){
    var collegeId = trimString($$D('uniName_hidden').value);
    var ukprn = trimString($$D('uniName_ukprn').value);
  }
  if(empFlag != "Y"){
    empFlag = "F";
  }
  var url = contextPath + "/what-can-i-do-widget.html?pageName=UNI-RESULTS&";
  url += "jacsCode=" + jacsCode +"&logId=" + logId + "&sortingType=";
  url += sortType + "&sortingOrder=" + sortOrder + "&pageNo=" + pageNo +"&employeeInfoFlag=" + empFlag;  
  url += "&collegeId=" + collegeId + "&ukprn=" + ukprn + "&entryLevel=" + entryLevel + "&entryPoints=" + entryPoints + "&tariffPoint=" +tariffPoint;
  if(empFlag == 'Y'){
    ajaxCall(url, 'unicourseDiv');
  }else{
    ajaxCall(url, 'sortingPod');
  }
}
function unicourseResults(ajax, id){
  var response = ajax.response;  
  $$D(id).innerHTML = response;
  blockNoneDiv("unicourseDiv", "block");
  blockNoneDiv("loadingIcon", "none");
  blockNoneDiv("whatcanidoHome",'none');
  blockNoneDiv("whatcanidoResults",'none');
  blockNoneDiv(id,'block');
  if($$D('unicourseDiv') && $$D('unicourseDiv').style.display == "block"){
    if($$D("sortingPod") && $$D("sortingPod").style.display == "none"){
      avgStatsDonut();
    }
  }
  if($$D("sortingPod") && $$D("sortingPod").style.display == "block"){
    $fp('#sortUniPod').remove();
    blockNoneDiv("loadingIcon",'none');
  }
}
function jobIndustryResults(url, id, collegeId, index){
  var ajax = new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){jobIndustryRes(ajax, id, collegeId, index);};
  ajax.runAJAX(); 
}
function avgStatsDonut(){
  var chart = "script_graduates_stats-"; //Avg stats donut chat id
  for(var i = 1; i <= 2; i++){
    var ele = chart + i;
    for(var lp = 0; lp <= 2; lp++){
      var elements = $$D(ele + lp);
	     if(elements){
	       eval(elements.innerHTML);
	     }
    }
  }
}
function hideShowAvgPod(podName){
  if(podName == "jobIndustryStats"){
    blockNoneDiv('ratePod', 'none');
    blockNoneDiv('jobIndustryStats', 'block');
  }else if("ratePod"){
    blockNoneDiv('ratePod', 'block');
    blockNoneDiv('jobIndustryStats', 'none');
  }
}

function hideJobIndustry(param, id){
  if(param == "JOB"){
    $$D('industryLinks' + id).className = "";
    $$D('jobLinks' + id).className = "act";
    blockNoneDiv("dnut_cnr_1_"+id,"none");
    blockNoneDiv("dnut_cnr_2_"+id, "block");
  }else{
    $$D('industryLinks' + id).className = "act";
    $$D('jobLinks' + id).className = "";
    blockNoneDiv("dnut_cnr_1_"+id,"block");
    blockNoneDiv("dnut_cnr_2_"+id,"none");
  }
}
function hideAvgStats(param){
  if(param == "JOB"){
    blockNoneDiv('avgDnut_cnr2', 'block');
    blockNoneDiv('avgDnut_cnr1', 'none');
    $$D('intLink').className = "";
    $$D('jobLink').className = "act";
  }else{
    blockNoneDiv('avgDnut_cnr2', 'none');
    blockNoneDiv('avgDnut_cnr1', 'block');
    $$D('intLink').className = "act";
    $$D('jobLink').className = "";
  }
}
function getSortResult(sortType, sortingFlag, collegeId, ukprn){
  var sortAction = "D";
  if(sortType == "EMP_RATE"){
    if(sortingFlag == "D"){
      sortAction = "A";
    }
  }
  if(sortType == "SALARY"){
    if(sortingFlag == "D"){
      sortAction = "A";
    }
  }
  blockNoneDiv("loadingIcon", "block");
  getSortingResult('1', sortType, sortAction, 'YES', collegeId, ukprn, 'F');
}
function showUKpod(){
  $fp("#tgldiv").toggleClass("tgldiv");
		$fp(".ftr_pod").toggleClass("psrl");
  $fp(".btn_ftr .btn_com").toggleClass('arw')
  $fp('html, body').animate({
    scrollTop: $fp("#tgldiv").offset().top
  }, 500);
}
function uniList(event, object){    
  var jacsCode = $$D('jacsCode').value;
  if(trimString(object.value)!=""){
    ajax_showOptions(object,'uninamelist',event,"indicator",'jacsCode='+ jacsCode);    
    blockNoneDiv("errordiv", "none");
  }else{
    blockNoneDiv("errordiv", "block");
    $$D('uniName_hidden').value = "";
    $$D('uniName_hidden').value = "";
    $$D('uniName_ukprn').value = "";
    $$D('uniName').value = "";
    $$D('uniName_display').value = "";
    $$D("errordiv").innerHTML = ErrorMessage.ajaxUniErrMsg;
  }  
}
function getJobIndustry(collegeId, ukprn, id){
  if($$D('expnd_lnk'+id) && trimString($$D('exp_cnt'+id).innerHTML) == ""){
    var jacsCode = $$D('jacsCode').value;
    var logId = $$D('logId').value;
    var entryLevel = $$D('entryLevel').value;
  var entryPoints = $$D('entryPoints').value;
  var tariffPoint = $$D('tariffPoint').value;
    var url = contextPath + "/what-can-i-do-widget.html?pageName=JOB-INDUSTRY&";
    url += "jacsCode=" + jacsCode + "&logId="+logId+"&collegeId="+collegeId + "&ukprn=" + ukprn + "&entryLevel=" + entryLevel + "&entryPoints="+entryPoints+ "&tariffPoint="+tariffPoint;
    jobIndustryResults(url, 'exp_cnt' + id, collegeId, id); 
  }else{        
    $fp('#expnd_lnk'+id).next(".exp_cnt").slideToggle();
    $fp('#expnd_lnk'+id).find(".fa-angle-up").toggleClass("fa-angle-down");
  }
}

function clearDiv(divId){
  if($$D(divId)){
    $$D(divId).innerHTML == "";
  }
}
function getCollegeSearch(param){
  blockNoneDiv("errordiv","none");
  var uniName = trimString($$D('uniName').value);
  var uniDisplayName = trimString($$D('uniName_display').value);
  if(param == "UNILOAD"){
    if(uniName == "" || uniName != uniDisplayName){
      blockNoneDiv("errordiv","block");
      $$D("errordiv").innerHTML = ErrorMessage.ajaxUniErrMsg;
      $$D('uniName_hidden').value = "";
      $$D('uniName_ukprn').value = "";
      $$D('uniName_display').value = "";
      return false;
    }else{
      getSortingResult('1', 'EMP_RATE', 'D');
      if($$D('uniClear')){
        blockNoneDiv('uniClear', 'block');
      }
    }
  }else if(param == "UNLOAD"){
    $$D('uniName_hidden').value = "";
    $$D('uniName_ukprn').value = "";
    $$D('uniName').value = "";
    $$D('uniName_display').value = "";
    getSortingResult('1', 'EMP_RATE', 'D');
    blockNoneDiv('uniClear', 'none');
  }
}
function keypressUniSearchSubmit(event){
  if(event.keyCode==13){
    return getCollegeSearch('UNILOAD');
  }
}
function goToBackPage(){
  window.scrollTo(0,0);
  blockNoneDiv("whatcanidoResults",'block');
  blockNoneDiv("whatcanidoHome",'none');
  blockNoneDiv("gradesError",'none');
  blockNoneDiv("subjectError",'none');
  blockNoneDiv("unicourseDiv",'none');
} 
function jobIndustryRes(ajax, id, collegeId, index){
  clearDiv(id);
  $$D(id).innerHTML = ajax.response; 
  blockNoneDiv("whatcanidoHome",'none');
  $fp("#"+id).slideToggle();
  $fp('#expnd_lnk'+index).find(".fa-angle-up").toggleClass("fa-angle-down");
  if($$D(id)){
    var element_1 = "script_graduates_" + collegeId + "_chart-";
    drawDonut(element_1);
  }
}
function drawDonut(element){ //Draw donut chart for uni job and industry
  for(var lp = 1; lp <= 2; lp++){
    var ele = element + lp + "-";
    for(var i = 0; i <= 2; i++){
      var elements = $$D(ele + i);
	     if(elements){
	       eval(elements.innerHTML);
	     }
    }
  }
}
function closeWidgetLightBox(){
  result = window.confirm("Do you really want to leave?  You will lose your progress");
  if(result==true){
  blockNoneDiv("fade_NRW","none");
  blockNoneDiv("holder_NRW","none");
  blockNoneDiv("close_NRW","none");
  window.location.assign(contextPath + "/courses/");
  }
}

