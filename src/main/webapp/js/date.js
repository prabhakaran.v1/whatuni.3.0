   /**
     * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
     */
    // Declaring valid date character, minimum year and maximum year
    var dtCh= "/";
    var minYear=1900;
    var maxYear=2100;
 
   function isDate(dtStr)
    {
        var pos1=dtStr.indexOf(dtCh)
        var pos2=dtStr.indexOf(dtCh,pos1+1)
        var strMonth=dtStr.substring(0,pos1)
        var strDay=dtStr.substring(pos1+1,pos2)
        var strYear=dtStr.substring(pos2+1)
        strYr=strYear
        if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
        if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
        if(strYr.length<4) {
            return false;
        }
        for (var i = 1; i <= 3; i++) {
                if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
        }
        month=parseInt(strMonth)
        day=parseInt(strDay)
        year=parseInt(strYr)
        if(year < minYear || year > maxYear)
        {
            return false;
        }    
        if (pos1==-1 || pos2==-1)
        {
                return false
        }
       return true
     }
 
    function validateDate()
    {
     var errormessage = "";
      if(document.getElementById("reportName").value=='') 
           errormessage = ">> Oops. Please select report name\n";
      if(!isDate(document.getElementById("startDate").value))
        errormessage =  errormessage + ">> Invalid Date..! Format should be (DD/MM/YYYY)";
      if(errormessage != '')
      {
          alert("Validation Error : \n" + errormessage);
          return false;
      }    
      var fld  = document.getElementById("startDate");
      var RegExPattern = /^((((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00)))|(((0[1-9]|[12]\d|3[01])(0[13578]|1[02])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|[12]\d|30)(0[13456789]|1[012])((1[6-9]|[2-9]\d)?\d{2}))|((0[1-9]|1\d|2[0-8])02((1[6-9]|[2-9]\d)?\d{2}))|(2902((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00))))$/;
      var errorMessage = 'Please enter valid date Format dd/mm/yyyy.';
      if ((fld.value.match(RegExPattern)) && (fld.value!='')) {
      } else {
           alert("Invalid Date..! Format should be DD/MM/YYYY");
          fld.focus();
          return false;
      } 
    }    