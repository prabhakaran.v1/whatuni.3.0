//This files common for user registeration light box and enquiry, Created By Thiyagu G for 03_Nov_2015, version is wu546.
var $fp = jQuery.noConflict();
var formErrorMessage = { 
  postCodeSuccessMsg    : "Got it!"
}
function setErrorAndSuccessMsg(classNameId, classes, msgId, messages){$$(classNameId).className = classes;$$(msgId).innerHTML = messages;$$(msgId).style.display = "block";}
function setDropdownSelectedValue(obj, spanid){
  var dobid = obj.id
  var el = $$(dobid);
  var text = el.options[el.selectedIndex].text;
  $$(spanid).innerHTML = text;
}
function hideandshowAddFinderReg(obj){
  var countryOfRes = obj.value;
  if(countryOfRes != null && (countryOfRes == '210' || countryOfRes == '55' || countryOfRes == '139' || countryOfRes == '256' || countryOfRes == '257')){    
    blockNone('addFinder', 'block');
    blockNone('postcodeField', 'block');
    blockNone('postcodeSection', 'block');
    blockNone('addressFields', 'block');
    setErrorAndSuccessMsg("postcodeIndexField", "w50p fr adlog", "errorPostcodeIndex", "");
    if($$('lblPostcodeIndex')){$$('lblPostcodeIndex').className = "lbco";}
  }else{
    blockNone('addFinder', 'none');
    blockNone('postaddid', 'none');
    blockNone('postcodeField', 'none');
    blockNone('postcodeSection', 'none');
    blockNone('addressFields', 'block');    
  }
}
function getPostCodeAddReg(fromCall){
  var setClass="w50p fr mb0";
  if(fromCall=='nonreg'){
    setClass="w50p fl adlog";
  }
  var postcodeVal = $$("postcode").value;  
  if(postcodeVal == ''){    
    setErrorAndSuccessMsg('postcodeField',setClass+' qlerr','postcodeErrMsg', "We can't find this address. Have you moved?");
    //Added for changeing error msg color by Prabha on 27_JAN_2016_REL
    if($$('postcodeErrMsg')){
      $$('postcodeErrMsg').className = "qler1";
    }
    //End of error msg code
    return false;
  }else{
    $$("postCodeAddSpan").innerHTML = "Please select"; //Added for set default value for post code by Prabha on 03_NOV_2015_REL
    var newPostCode = checkPostCode(trimString(postcodeVal));    
    if(newPostCode != ""){      
      setErrorAndSuccessMsg('postcodeField',setClass+' sumsg','postcodeErrMsg', "");
      var url="/degrees/getpostcodeaddress.html?postcode="+newPostCode;
      var ajax=new sack();
      ajax.requestFile = url;	
      ajax.onCompletion = function(){ getPostCodeDataReg(ajax, setClass); };	
      ajax.runAJAX();  
    }else{      
      setErrorAndSuccessMsg('postcodeField',setClass+' qlerr','postcodeErrMsg', "We can't find this address. Have you moved?");
      blockNone('addressFields', 'block');
      blockNone('postcodeSection', 'none');      
    }  
  }
 } 
 function getPostCodeDataReg(ajax, setClass){  
  var postCodeRes = ajax.response;
  blockNone('addressFields', 'block');
	if(postCodeRes.trim() == 'NORESULTS'){    
    setErrorAndSuccessMsg('postcodeField',setClass+' qlerr','postcodeErrMsg', "We can't find this address. Have you moved?");
    return false;
	}else{
    $$("postCodAdd").innerHTML = "";
    var dataArrayMain = ajax.response.split("**");
    for(var i = 0; i < dataArrayMain.length; i++){
      var address = dataArrayMain[i];
      var optionArray = address.split("$$");
      addElement('postCodAdd', optionArray[1], optionArray[0]);
    }
    setErrorAndSuccessMsg("postcodeField", setClass+" sumsg", "postcodeErrMsg", formErrorMessage.postCodeSuccessMsg); //changed text by Prabha on 31_July_2018_rel
    blockNone('postaddid', 'block');
    //blockNone('postcodeField', 'block');
    blockNone('postcodeSection', 'block');
    blockNone('addressFields', 'block');
	}  
}
function registratonValidations(id){
  if(id == "firstName" || id == "surName"){
    var errMsgClass;
    var susMsgClass;
    if(id == "firstName"){
      errMsgClass = "w50p fl fst_lg1 qlerr";
      susMsgClass="w50p fl fst_lg1 sumsg";
      classNameId = "firstName_fieldset";
      msgId = "firstName_error";
    }else if(id == "surName"){
      errMsgClass = "w50p fr fst_lg1 qlerr";
      susMsgClass="w50p fr fst_lg1 sumsg";
      classNameId = "surName_fieldset";
      msgId = "surName_error";
    }
    if(trimString($$(id).value) == "" || trimString($$(id).value).toLowerCase() == "first name" || trimString($$(id).value).toLowerCase() == "last name"){      
      setErrorAndSuccessMsg(classNameId, errMsgClass, msgId, "We still don't know your name. Remind us?");
      $$(msgId).className = "qler1";
    }else{
      if(id == "firstName"){
        setErrorAndSuccessMsg(classNameId, susMsgClass, msgId, 'Nice to meet you! Great name!');
        $$(msgId).className = "sutxt";
      }
      if(id == "surName"){
        setErrorAndSuccessMsg(classNameId, susMsgClass, msgId, '');
        $$(msgId).className = "sutxt";
      }
    }
  }    
  //Added by Indumathi.S Nov-03-15 Rel For validate the password field on blur
  if(id == 'password') {
    var password = ""; 
    if($$D("password")){password = trimString( $$D("password").value );}    
    if(isEmpty(password) || password == 'Password*'){ message = false; setErrorAndSuccessMsg('password_fieldset','w100p fl fst_lg1 qlerr','password_error', ErrorMessage.error_reg_login_password);
    } else {
      $fp('#password').next('label').addClass("top_lb");
      setErrorAndSuccessMsg('password_fieldset','w100p fl fst_lg1 sumsg','password_error','');
    }
    if(!isEmpty(password)){
      setErrorAndSuccessMsg('password_fieldset','w100p fl fst_lg1 sumsg','password_error','');
    }   
    if(!isEmpty(password) && password.length < 6){
       message = false; setErrorAndSuccessMsg('password_fieldset','w100p fl fst_lg1 qlerr','password_error', ErrorMessage.error_password_length);
    }
  }  
  if(id == 'yeartoJoinCourse1' || id == 'yeartoJoinCourse2' || id == 'yeartoJoinCourse3' || id == 'yeartoJoinCourse4'){
    var yoe = '';
    for (i=0; i<document.getElementsByTagName('input').length; i++) {
      if (document.getElementsByTagName('input')[i].type == 'radio'){
        if(document.getElementsByTagName('input')[i].checked == true){
         yoe = document.getElementsByTagName('input')[i].value;
         break;
        }
      } 
    }  
    var cuYear = new Date().getFullYear();
    if(yoe < cuYear){
      setErrorAndSuccessMsg("yoe_fieldset", "ql-inp ErrMsg", "yoe_error", "<p>Please choose when would you like to start?</p>");       
    }else{
      setErrorAndSuccessMsg("yoe_fieldset", "ql-inp", "yoe_error", "");
    }  
  }  
  if(id == "dateDOB" || id == "monthDOB" || id == "yearDOB"){
    var elDate = $$("dateDOB");
    var textDate = elDate.options[elDate.selectedIndex].text;
    var elMonth = $$("monthDOB");    
    var textMonth = elMonth.options[elMonth.selectedIndex].text;
    var elYear = $$("yearDOB");
    var textYear = elYear.options[elYear.selectedIndex].text;        
    var datemsg = "";    
    var inputDate = textDate+"/"+elMonth.selectedIndex+"/"+textYear;
    datemsg = isValidDOBDate(inputDate,false,id);
    if (datemsg == "") {
        getAge(inputDate, "registration");        
    }else if(datemsg != "calling_onchange") {
      setErrorAndSuccessMsg("userDOB", "w100p fl tl_pos_rt db_pos qlerr", "userDobErrMsg", datemsg);
      $$("userDobErrMsg").className = "qler1";
    }
  }     
  if(id == "postcode"){    
    if(trimString($$(id).value) != "" && trimString($$(id).value).toLowerCase() != "enter uk postcode"){      
      var retMsg = isValidUKpostcodeWithoutSpace(trimString($$(id).value));      
      if(retMsg){
        setErrorAndSuccessMsg("postcodeField", "w50p fr mb0 sumsg", "postcodeErrMsg", formErrorMessage.postCodeSuccessMsg);//changed text by Prabha on 31_July_2018_rel
        $$("postcodeErrMsg").className = "sutxt";        
      }else{
        setErrorAndSuccessMsg("postcodeField", "w50p fr mb0 qlerr", "postcodeErrMsg", "We can't find this address. Have you moved?");
        $$("postcodeErrMsg").className = "qler1";
      }
    }else{
      setErrorAndSuccessMsg("postcodeField", "w50p fr mb0", "postcodeErrMsg", "");
    }
  } 
}

function ebookRegistrationValidations(id){
  if(id == "firstName" || id == "surName"){
    var errMsgClass;
    var susMsgClass;
    if(id == "firstName"){
      errMsgClass = "fl tx-bx faild";
      susMsgClass="fl tx-bx sucs";
      classNameId = "firstName_fieldset";
      msgId = "firstName_error";
    }else if(id == "surName"){
      errMsgClass = "fr tx-bx faild";
      susMsgClass="fr tx-bx sucs";
      classNameId = "surName_fieldset";
      msgId = "surName_error";
    }
    if(trimString($$(id).value) == "" || trimString($$(id).value).toLowerCase() == "first name" || trimString($$(id).value).toLowerCase() == "last name"){      
      setErrorAndSuccessMsg(classNameId, errMsgClass, msgId, "We still don't know your name. Remind us?");
      $$(msgId).className = "fail-msg";
    }else{
      if(id == "firstName"){
        setErrorAndSuccessMsg(classNameId, susMsgClass, msgId, 'Nice to meet you! Great name!');
        $$(msgId).className = "su-msg";
      }
      if(id == "surName"){
        setErrorAndSuccessMsg(classNameId, susMsgClass, msgId, '');
        $$(msgId).className = "su-msg";
      }
    }
  }    
  //Added by Indumathi.S Nov-03-15 Rel For validate the password field on blur
  if(id == 'password') {
    var password = ""; 
    if($$D("password")){password = trimString( $$D("password").value );}    
    if(isEmpty(password) || password == 'Password*'){ message = false; setErrorAndSuccessMsg('password_fieldset','tx-bx w-100 pwd_feld faild','password_error', ErrorMessage.error_reg_login_password);
    } else {setErrorAndSuccessMsg('password_fieldset','tx-bx w-100 pwd_feld sucs','password_error','');}
    if(!isEmpty(password)){
      setErrorAndSuccessMsg('password_fieldset','tx-bx w-100 pwd_feld sucs','password_error','');
    }   
    if(!isEmpty(password) && password.length < 6){
       message = false; setErrorAndSuccessMsg('password_fieldset','tx-bx w-100 pwd_feld faild','password_error', ErrorMessage.error_password_length);
    }
  }  
  if(id == 'yeartoJoinCourse1' || id == 'yeartoJoinCourse2' || id == 'yeartoJoinCourse3' || id == 'yeartoJoinCourse4'){
    var yoe = '';
    for (i=0; i<document.getElementsByTagName('input').length; i++) {
      if (document.getElementsByTagName('input')[i].type == 'radio'){
        if(document.getElementsByTagName('input')[i].checked == true){
         yoe = document.getElementsByTagName('input')[i].value;
         break;
        }
      } 
    }  
    var cuYear = new Date().getFullYear();
    if(yoe < cuYear){
      setErrorAndSuccessMsg("yoe_fieldset", "ql-inp ErrMsg", "yoe_error", "<p>Please choose when would you like to start?</p>");       
    }else{
      setErrorAndSuccessMsg("yoe_fieldset", "ql-inp", "yoe_error", "");
    }  
  }  
}

function isNumeric(input){
    return (input - 0) == input && (''+input).replace(/^\s+|\s+$/g, "").length > 0;
}
function enquiryValidations(id){
  if(id=="fname" || id == "lname"){    
    var errMsgClass;
    var susMsgClass;
    if(id == "fname"){
      errMsgClass = "w50p fl adlog qlerr";
      susMsgClass="w50p fl adlog sumsg";
      classNameId = "divfname";
      msgId = "errorfname";
    }else if(id == "lname"){
      errMsgClass = "w50p fr adlog qlerr";
      susMsgClass="w50p fr adlog sumsg";
      classNameId = "divlname";
      msgId = "errorlname";
    }
    if(trimString($$(id).value) == "" || trimString($$(id).value).toLowerCase() == "first name" || trimString($$(id).value).toLowerCase() == "last name"){      
      setErrorAndSuccessMsg(classNameId, errMsgClass, msgId, "We still don't know your name. Remind us?");
      $$(msgId).className = "qler1";
    }else{
      if(id == "fname"){
        setErrorAndSuccessMsg(classNameId, susMsgClass, msgId, 'Nice to meet you! Great name!');
        $$(msgId).className = "sutxt";
      }
      if(id == "lname"){
        setErrorAndSuccessMsg(classNameId, susMsgClass, msgId, '');
        $$(msgId).className = "sutxt";
      }
    }
  }
  if(id == "emailAddress" || id=="email"){    
    if(isEmpty($$(id).value) || $$(id).value == 'Enter email address'){ 
      setErrorAndSuccessMsg('divEmail','w100p adlog qlerr','errorEmail', 'Something seems to be missing from your email. Awkward.');
      $$("errorEmail").className = "qler1";      
    } else if(!emailCheck($$(id).value)){ 
      setErrorAndSuccessMsg('divEmail','w100p adlog qlerr','errorEmail', 'Something seems to be missing from your email. Awkward.'); 
      $$("errorEmail").className = "qler1";      
    }else{
      setErrorAndSuccessMsg('divEmail','w100p adlog sumsg','errorEmail', "You've just got to be on email"); 
      $$('errorEmail').className = "sutxt";      
    }
  }
  if(id == "yeartoJoinCourse1" || id == "yeartoJoinCourse2" || id == "yeartoJoinCourse3" || id == "yeartoJoinCourse4"){
    var yoe = '';
    for (i=0; i<document.getElementsByTagName('input').length; i++) {
      if (document.getElementsByTagName('input')[i].type == 'radio'){
        if(document.getElementsByTagName('input')[i].checked == true){
         yoe = document.getElementsByTagName('input')[i].value;
         break;
        }
      } 
    }
    var cuYear = new Date().getFullYear();
    if(yoe < cuYear){
      setErrorAndSuccessMsg("yoeFeild", "ql-inp ErrMsg", "yoeErrMsg", "<p>Please choose when would you like to start?</p>");    
      returnflag = false;
    }else{
      setErrorAndSuccessMsg("yoeFeild", "ql-inp", "yoeErrMsg", "");
    }
  }
  
  if(id == "dateDOB" || id == "monthDOB" || id == "yearDOB"){
    var textDate = $$("dateDOB").value;    
    var textMonth = $$("monthDOB").value;    
    var textYear = $$("yearDOB").value;
    var datemsg = "";
    var errFlag=true;
    var errMsg =''; 
    //Added By Thiyagu G for 19_Apr_2016.
    if(isEmpty(textDate) || isEmpty(textMonth) || isEmpty(textYear)){
      if($fp('#divuserDOB').hasClass('sumsg')){
        setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog ", "userDobErrMsg", "");
        $$("userDobErrMsg").className = "";
      }      
      return false;
    }    
    if(isEmpty(textDate) && isEmpty(textMonth) && isEmpty(textYear)){
      errFlag=false;
      setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog qlerr", "userDobErrMsg", "Happy birthday to you on the.....?");
      $$("userDobErrMsg").className = "qler1";
    }    
    if(textDate!="" && !isNumeric(textDate)){
      errFlag=false;
      setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog qlerr", "userDobErrMsg", "Please enter the valid date.");
      $$("userDobErrMsg").className = "qler1";
    }
    if(textMonth!="" && !isNumeric(textMonth)){
      errFlag=false;
      setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog qlerr", "userDobErrMsg", "Please enter the valid month");
      $$("userDobErrMsg").className = "qler1";      
    }
    if(textYear!="" && !isNumeric(textYear)){
      errFlag=false;
      setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog qlerr", "userDobErrMsg", "Please enter the valid year");
      $$("userDobErrMsg").className = "qler1";      
    }
    if(errFlag){
      var inputDate = textDate+"/"+textMonth+"/"+textYear;
      datemsg = isValidDOBDate(inputDate,errFlag,id);    
      if (datemsg=="") {
          getAge(inputDate, "enquiry");        
      }else if(datemsg != "calling_onchange") {
        setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog qlerr", "userDobErrMsg", datemsg);
        $$("userDobErrMsg").className = "qler1";
      }
    }
  }  
  if(id == "countryOfResidence" || id=="countryOfResidenceSel"){    
    var elNationality;
    //Added by Indumathi.S Nov-03-15 Rel
    $$("postcode").value = "";
    $$("address1").value = "";
    $$("address2").value = "";
    $fp('#postCodAdd').attr('selectedIndex', '-1');
    $$("postCodeAddSpan").innerHTML = "Please select";
    $$("town").value = ""; $$D("postcodeIndex").value = "";
    blockNone('postcodeSection', 'none');
    setErrorAndSuccessMsg('divAddress1','w100p fl adlog','errorAddress1','');
    setErrorAndSuccessMsg("divTown", "w50p fl adlog", "errorTown", "");
    setErrorAndSuccessMsg("postcodeField", "w50p fr mb0", "postcodeErrMsg", "");
    if(id == "countryOfResidence"){
      elNationality = $$("countryOfResidence");
    }
    if(id=="countryOfResidenceSel"){
      elNationality = $$("countryOfResidenceSel");
    }
    var textelNationality = elNationality.options[elNationality.selectedIndex].text;    
    var nationalityText = trimString(textelNationality.replace(/\s/g,'_'));      
    if(textelNationality == "" || textelNationality == "Country of residence"){    
      setErrorAndSuccessMsg("divCountryOfRes", "mb0 qlerr", "errorCountry", "What parts are you from, friend?");      
      $$("errorCountry").className = "qler1";
    }else{
      setErrorAndSuccessMsg("divCountryOfRes", "mb0 sumsg", "errorCountry", "");
    }    
  }   
  if(id == "postcode"){
    if(trimString($$(id).value) != "" && trimString($$(id).value).toLowerCase() != "enter uk postcode"){      
      var retMsg = isValidUKpostcodeWithoutSpace(trimString($$(id).value));
      if(retMsg){
        setErrorAndSuccessMsg("postcodeField", "w50p fr sumsg", "postcodeErrMsg", formErrorMessage.postCodeSuccessMsg);
        $$('postcodeErrMsg').className = "sutxt";
      }else{
        setErrorAndSuccessMsg("postcodeField", "w50p fr qlerr", "postcodeErrMsg", "We can't find this address. Have you moved?");
        $$("postcodeErrMsg").className = "qler1";
      }
    }    
  }else if(id == "postcodeIndex"){ //Added By Thiyagu G for 19_Apr_2016.
    if(trimString($$(id).value) != ""){
      setErrorAndSuccessMsg("postcodeField", "w50p fl adlog sumsg", "postcodeErrMsg", formErrorMessage.postCodeSuccessMsg); //changed text by Prabha on 31_July_2018_rel
      $$('postcodeErrMsg').className = "sutxt";
      if($fp('#lbregistration').length){
        $fp('#postcodeIndex').next('label').addClass("lbco top_lb");
      }
    }
  }  
  if(id == "address1"){    
    if(trimString($$(id).value) == ""){      
      setErrorAndSuccessMsg("divAddress1", "w100p fl qlerr", "errorAddress1", "Please enter Address line 1");      
      $$("errorAddress1").className = "qler1";
    }else{      
      setErrorAndSuccessMsg("divAddress1", "w100p fl sumsg", "errorAddress1", "");      
    }
  }
  if(id == "town"){    
    if(trimString($$(id).value) == ""){
        setErrorAndSuccessMsg("divTown", "w50p fl adlog qlerr", "errorTown", "Please enter town");
        $$("errorTown").className = "qler1";
    }else{
      setErrorAndSuccessMsg("divTown", "w50p fl adlog sumsg", "errorTown", "");
    }
  }
  if(id == "password"){ 
    var password = $$(id).value;
    if(trimString(password) == ""){
      setErrorAndSuccessMsg("divPassword", "w100p adlog qlerr", "errorPassword", "We still don't know your password. Remind us?");
      $$("errorPassword").className = "qler1";
    } else if(!isEmpty(password) && password.length < 6){
      setErrorAndSuccessMsg("divPassword", "w100p adlog qlerr", "errorPassword", "Your password should be 6 characters or more");
      $$("errorPassword").className = "qler1";
    }else{
      setErrorAndSuccessMsg("divPassword", "w100p adlog sumsg", "errorPassword", "");
    }
  }  
}
//Added By Thiyagu G for 19_Apr_2016.
function checkDOB(){
  var textDate = $$("dateDOB").value;    
  var textMonth = $$("monthDOB").value;    
  var textYear = $$("yearDOB").value;
  var erFlag=true;
  if(isEmpty(textDate)){    
    setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog qlerr", "userDobErrMsg", "Please enter the date");
    $$("userDobErrMsg").className = "qler1";
    erFlag=false;
  }
  if(isEmpty(textMonth)){    
    setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog qlerr", "userDobErrMsg", "Please enter the month");
    $$("userDobErrMsg").className = "qler1";
    erFlag=false;
  }
  if(isEmpty(textYear)){    
    setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog qlerr", "userDobErrMsg", "Please enter the year");
    $$("userDobErrMsg").className = "qler1";
    erFlag=false;
  }
  if(erFlag){
    enquiryValidations("yearDOB");  
  }  
}
function getAge(birth, callingfrom) {    
  var today = new Date();
  var nowyear = today.getFullYear();
  var nowmonth = today.getMonth();
  var nowday = today.getDate(); 
  
  var dateArray = birth.split("/");
  var birthday = dateArray[0];
  var birthmonth = dateArray[1];
  var birthyear = dateArray[2];
  
  var age = nowyear - birthyear;
  var age_month = nowmonth - birthmonth;
  var age_day = nowday - birthday;
  //Added validation for the age under 13.
  var err_msg = "You must be at least 13 years old";
  if(age == 13 && birthmonth > (today.getMonth()+1)){
    if(callingfrom == "registration"){      
      setErrorAndSuccessMsg("userDOB", "w100p fl tl_pos_rt db_pos qlerr", "userDobErrMsg", err_msg);
      $$("userDobErrMsg").className = "qler1";
    }else{
      setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog qlerr", "userDobErrMsg", err_msg);
      $$("userDobErrMsg").className = "qler1";        
    }
  } else if(age == 13 && birthmonth >= (today.getMonth()+1) && parseInt(birthday) > today.getDate()){
    if(callingfrom == "registration"){      
      setErrorAndSuccessMsg("userDOB", "w100p fl tl_pos_rt db_pos qlerr", "userDobErrMsg", err_msg);
      $$("userDobErrMsg").className = "qler1";
    }else{
      setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog qlerr", "userDobErrMsg", err_msg);
      $$("userDobErrMsg").className = "qler1";        
    }
  } else if(age > 13 || (age == 13 && birthmonth >= (today.getMonth()+1) && parseInt(birthday) <= today.getDate())){
    if(callingfrom == "registration"){      
      setErrorAndSuccessMsg("userDOB", "w100p fl tl_pos_rt db_pos sumsg", "userDobErrMsg", "Don't expect a birthday card. Soz");
      $$("userDobErrMsg").className = "sutxt";
    }else{
      setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog sumsg", "userDobErrMsg", "Don't expect a birthday card. Soz");
      $$("userDobErrMsg").className = "sutxt";        
    }    
  }else if(age > 13 || (age == 13 && birthmonth <= (today.getMonth()+1))){
    if(callingfrom == "registration"){      
      setErrorAndSuccessMsg("userDOB", "w100p fl tl_pos_rt db_pos sumsg", "userDobErrMsg", "Don't expect a birthday card. Soz");
      $$("userDobErrMsg").className = "sutxt";
    }else{
      setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog sumsg", "userDobErrMsg", "Don't expect a birthday card. Soz");
      $$("userDobErrMsg").className = "sutxt";        
    }    
  } else if(age < 13){
    if(callingfrom == "registration"){      
      setErrorAndSuccessMsg("userDOB", "w100p fl tl_pos_rt db_pos qlerr", "userDobErrMsg", err_msg);
      $$("userDobErrMsg").className = "qler1";
    }else{
      setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog qlerr", "userDobErrMsg", err_msg);
      $$("userDobErrMsg").className = "qler1";        
    }
  }/*else{
    if(callingfrom == "registration"){      
      setErrorAndSuccessMsg("userDOB", "w100p fl tl_pos_rt db_pos", "userDobErrMsg", "");
      $$("userDobErrMsg").className = "";
    }else{
      setErrorAndSuccessMsg("divuserDOB", "w100p fl adlog", "userDobErrMsg", "");
      $$("userDobErrMsg").className = ""; 
    }
  }*/
} 
function setPostCodeAddressReg(obj){
  var postCodeAdd = obj.value;
  if(postCodeAdd != null && postCodeAdd != ""){
  var dataarray=postCodeAdd.split("###");
    $$('address1').value = dataarray[0];
    $$('address2').value = dataarray[1];
    $$('town').value = dataarray[2];
    $$('postcodeIndex').value = $$('postcode').value;
    if($$('address1Field')){setErrorAndSuccessMsg("address1Field", "w100p fl sumsg", "address1ErrMsg", "");}
    if($$('divAddress1')){setErrorAndSuccessMsg("divAddress1", "w100p fl sumsg", "errorAddress1", "");}
    if($$('townField')){setErrorAndSuccessMsg("townField", "w50p fl sumsg", "townErrMsg", "");}
    if($$('divTown')){setErrorAndSuccessMsg("divTown", "w50p fl sumsg", "errorTown", "");}
  }else{
    $$('address1').value = "";
    $$('address2').value = "";
    $$('town').value = "";
    $$('postcodeIndex').value = "";    
    $$('address1').placeholder  = "Address line 1";
    $$('address2').placeholder  = "Address line 2";
    $$('town').placeholder  = "Please enter town/city";       
    $$('postcodeIndex').placeholder  = "Please enter";    
    if($$('address1Field')){setErrorAndSuccessMsg("address1Field", "w100p fl qlerr", "address1ErrMsg", "Please enter Address line 1");}
    if($$('divAddress1')){setErrorAndSuccessMsg("divAddress1", "w100p fl qlerr", "errorAddress1", "Please enter Address line 1");}
    if($$('townField')){setErrorAndSuccessMsg("townField", "w50p fl qlerr", "townErrMsg", "Please enter town");}
    if($$('divTown')){setErrorAndSuccessMsg("divTown", "w50p fl qlerr", "errorTown", "Please enter town");}
    alert('Choose valid address from list');    
  }
}
function setPostCodeAddressEnq(obj){
  var postCodeAdd = obj.value;
  if(postCodeAdd != null && postCodeAdd != ""){
  var dataarray=postCodeAdd.split("###");
    $$('address1').value = dataarray[0];
    $$('address2').value = dataarray[1];
    $$('town').value = dataarray[2];
    $$('postcodeIndex').value = $$('postcode').value;
    $$('lblAddress1').className = "lbco top";
    $$('lblAddress2').className = "lbco top";    
    $$('lblTown').className = "lbco top";
    $$('lblPostcodeIndex').className = "lbco top";
    if($$('address1Field')){setErrorAndSuccessMsg("address1Field", "w100p fl adlog sumsg", "address1ErrMsg", "");}
    if($$('divAddress1')){setErrorAndSuccessMsg("divAddress1", "w100p fl adlog sumsg", "errorAddress1", "");}
    if($$('townField')){setErrorAndSuccessMsg("townField", "w50p fl adlog sumsg", "townErrMsg", "");}
    if($$('divTown')){setErrorAndSuccessMsg("divTown", "w50p fl adlog sumsg", "errorTown", "");}
  }else{
    $$('address1').value = "";
    $$('address2').value = "";
    $$('town').value = "";
    $$('postcodeIndex').value = "";
    $$('lblAddress1').className = "lbco";
    $$('lblAddress2').className = "lbco";    
    $$('lblTown').className = "lbco";
    $$('lblPostcodeIndex').className = "lbco";    
    if($$('address1Field')){setErrorAndSuccessMsg("address1Field", "w100p fl adlog qlerr", "address1ErrMsg", "Please enter Address line 1");}
    if($$('divAddress1')){setErrorAndSuccessMsg("divAddress1", "w100p fl adlog qlerr", "errorAddress1", "Please enter Address line 1");}
    if($$('townField')){setErrorAndSuccessMsg("townField", "w50p fl adlog qlerr", "townErrMsg", "Please enter town");}
    if($$('divTown')){setErrorAndSuccessMsg("divTown", "w50p fl adlog qlerr", "errorTown", "Please enter town");}
    alert('Choose valid address from list');    
  }
}
function isValidDOBDate(dateStr, validateEmptyDate,id) {
    var msg = "";    
    var dateArray = dateStr.split("/");        
    day = dateArray[0];
    month = dateArray[1];
    year = dateArray[2];  
    var today = new Date();
    if (day == '' || day == 'Day' || day < 1 || day > 31) {
      if(validateEmptyDate || id == "dateDOB"){
        msg = "Day must be between 1 and 31.";
        return msg;
      }else{
        return "calling_onchange";
      }
    }
    if (month == '' || month == 'Month' || parseInt(month) < 1 || parseInt(month) > 12) { // check month range
      if(validateEmptyDate || id == "monthDOB"){
        msg = "Month must be between 1 and 12.";
        return msg;
      }else{
        return "calling_onchange";
      }
    }  
    if ((parseInt(month)==4 || parseInt(month)==6 || parseInt(month)==9 || parseInt(month)==11) && parseInt(day)==31) {
        msg = "Month "+month+" doesn't have 31 days!";
        return msg;
    }
    if (parseInt(month) == 2) { // check for february 29th
      var isleap = (parseInt(year) % 4 == 0 && (parseInt(year) % 100 != 0 || parseInt(year) % 400 == 0));
      if (year != "Year" && (day>29 || (day==29 && !isleap))) {
          msg = "February " + year + " doesn't have " + day + " days!";
          return msg;
      }
    }    
    if (year == 'Year' || year < 1900 || year > (today.getFullYear() - 13)) {    //added condition to reduce year 13 for 03_Oct_2017, By Thiyagu G
      if(validateEmptyDate || id == "yearDOB"){    
        msg = "Please enter a year between 1900 to "+(today.getFullYear() - 13); 
        return msg;
      }else{    
        return "calling_onchange";
      }
    }
    if (year == '' || year < 1900 || year > (today.getFullYear() - 13)) {    
      if(validateEmptyDate || id == "yearDOB"){    
        msg = "Please enter a year between 1900 to "+(today.getFullYear() - 13);
        return msg;
      }else{    
        return "calling_onchange";
      }
    }
    var age = today.getFullYear() - year;
    /*var m = today.getMonth() - month;
    if (m < 0 || (m === 0 && today.getDate() < parseInt(day))) {
        age--;
    }*/
    if(age == 13 && month > (today.getMonth()+1)){
      msg = "You must be at least 13 years old";
      return msg;
    } else if(age == 13 && month >= (today.getMonth()+1) && parseInt(day) > today.getDate()){
      msg = "You must be at least 13 years old";
      return msg;
    } else {
      msg = "";
      return msg;
    }
    return msg;  // date is valid
}
function checkPostCode(toCheck) {
  var alpha1 = "[abcdefghijklmnoprstuwyz]";                       // Character 1
  var alpha2 = "[abcdefghklmnopqrstuvwxy]";                       // Character 2
  var alpha3 = "[abcdefghjkpmnrstuvwxy]";                         // Character 3
  var alpha4 = "[abehmnprvwxy]";                                  // Character 4
  var alpha5 = "[abdefghjlnpqrstuwxyz]";                          // Character 5
  var BFPOa5 = "[abdefghjlnpqrst]";                               // BFPO alpha5
  var BFPOa6 = "[abdefghjlnpqrstuwzyz]";                          // BFPO alpha6
  var pcexp = new Array ();
  pcexp.push (new RegExp ("^(bf1)(\\s*)([0-6]{1}" + BFPOa5 + "{1}" + BFPOa6 + "{1})$","i"));
  pcexp.push (new RegExp ("^(" + alpha1 + "{1}" + alpha2 + "?[0-9]{1,2})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));
  pcexp.push (new RegExp ("^(" + alpha1 + "{1}[0-9]{1}" + alpha3 + "{1})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));
  pcexp.push (new RegExp ("^(" + alpha1 + "{1}" + alpha2 + "{1}" + "?[0-9]{1}" + alpha4 +"{1})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));
  pcexp.push (/^(GIR)(\s*)(0AA)$/i);
  pcexp.push (/^(bfpo)(\s*)([0-9]{1,4})$/i);
  pcexp.push (/^(bfpo)(\s*)(c\/o\s*[0-9]{1,3})$/i);
  pcexp.push (/^([A-Z]{4})(\s*)(1ZZ)$/i);  
  pcexp.push (/^(ai-2640)$/i);
  var postCode = toCheck;
  var valid = false;
  for ( var i=0; i<pcexp.length; i++) {
    if (pcexp[i].test(postCode)) {
      pcexp[i].exec(postCode);  
      postCode = RegExp.$1.toUpperCase() + " " + RegExp.$3.toUpperCase();
      postCode = postCode.replace (/C\/O\s*/,"c/o "); 
      if (toCheck.toUpperCase() == 'AI-2640') {postCode = 'AI-2640'}; 
      valid = true;  
      break;
    }
  }
  if (valid) {return postCode;} else return false;
}
function isValidUKpostcodeWithoutSpace(postcode){
  var newPostCode = checkPostCode(postcode);  
  if(newPostCode){
	   return true;
	 }else{
   return false; 	
	} 
}
function addElement(element, name, value){
 	if($$(element)){
		$$(element).options[$$(element).options.length]=new Option(name,value);
	}
}
function onchangeQual(obj){
  var qualification = obj.value;
  var valueArray = {"Alevels":5, "SQAHighers":8, "SQAAdvancedHighers":8, "BTEC":3};  
  if(obj.value == null || obj.value == '' ){
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";
        
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
    showErrorMsg("gradesErrMsg", "");
  }else if(obj.value == '74'){
    var count = valueArray.Alevels;
    createSelectBox('A*select', count);  
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count);  
    createSelectBox('Eselect', count);  

    $$("A*mainspan").style.display = "block";
    $$("Amainspan").style.display = "block";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "block";
    
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
    if($$("A*span")){$$("A*span").innerHTML = '0';}
    if($$("Aspan")){$$("Aspan").innerHTML = '0';}
    if($$("Bspan")){$$("Bspan").innerHTML = '0';}
    if($$("Cspan")){$$("Cspan").innerHTML = '0';}
    if($$("Dspan")){$$("Dspan").innerHTML = '0';}
    if($$("Espan")){$$("Espan").innerHTML = '0';}
    showErrorMsg("gradesErrMsg", "");
  }else if(obj.value == '75' || obj.value == '76'){
    var count = valueArray.SQAHighers;

      createSelectBox('Aselect', count);  
      createSelectBox('Bselect', count);  
      createSelectBox('Cselect', count);  
      createSelectBox('Dselect', count);  

    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "block";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "none";
    
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
    if($$("Aspan")){$$("Aspan").innerHTML = '0';}
    if($$("Bspan")){$$("Bspan").innerHTML = '0';}
    if($$("Cspan")){$$("Cspan").innerHTML = '0';}
    if($$("Dspan")){$$("Dspan").innerHTML = '0';}
    showErrorMsg("gradesErrMsg", "");
  }else if(obj.value == '555'){
    var count = valueArray.BTEC;
    
      createSelectBox('D*select', count);  
      createSelectBox('BtecDselect', count);  
      createSelectBox('Mselect', count);  
      createSelectBox('Pselect', count);  

    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";
    
    $$("D*mainspan").style.display = "block";
    $$("BtecDmainspan").style.display = "block";
    $$("Mmainspan").style.display = "block";
    $$("Pmainspan").style.display = "block";
    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
    
    if($$("D*span")){$$("D*span").innerHTML = '0';}
    if($$("BtecDspan")){$$("BtecDspan").innerHTML = '0';}
    if($$("Mspan")){$$("Mspan").innerHTML = '0';}
    if($$("Pspan")){$$("Pspan").innerHTML = '0';}
    showErrorMsg("gradesErrMsg", "");
  }else if(obj.value == '77'){  
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";    
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";    
    $$("ucaspoins").style.display = "block";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
  }else if(obj.value == '78'){
  
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";    
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "block";
    $$("other").style.display = "none";
    showErrorMsg("gradesErrMsg", "");
  }else{        
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "block";
    showErrorMsg("gradesErrMsg", "");
  }  
}  
function createSelectBox(selectBoxId, count){
  if($$(selectBoxId) != null){
    $$(selectBoxId).innerHTML = "";
    for(var i = 0; i <= count; i++){
      var option = document.createElement("option");
      option.value = ""+i;
      option.appendChild(document.createTextNode(i));
      $$(selectBoxId).appendChild(option);
    }
  }
}

function showTooltip(id){
  blockNone(id, 'block');
}
function hideTooltip(id){
  blockNone(id, 'none');
}
function enquiryGradesValidation(fnameId){
  
}
function enquiryGradesPointsSum(){
  var index;
  var tempText = 0;
  var tempGradesText = 0;
  var gradePoints;
  var gradePointsText;
  var gradesValue;
  var gradesSum;
  var msgFlag = 0;
  var grades = ["A*select", "Aselect", "Bselect", "Cselect", "Dselect", "Eselect"];
  var gradesValues = {Astarselect:6, Aselect:5, Bselect:4, Cselect:3, Dselect:2, Eselect:1};
  gradePoints = document.getElementById("grade_210");
  gradePointsText = gradePoints.options[gradePoints.selectedIndex].text;    
  if("Please Select" == gradePointsText){
    setErrorAndSuccessMsg("gradesMsg", "row-fluid qlf-nwfs grads mb10 qlerr", "gradesErrMsg", "Please select qualifications.");
    $$('gradesErrMsg').className="qler1";
  }
  if("A Levels" == gradePointsText){
    for	(index = 0; index < grades.length; index++) {
      if(grades[index] == "A*select"){
        gradesValue = gradesValues["Astarselect"];
      }else{
        gradesValue = gradesValues[grades[index]];
      }
      gradePoints = document.getElementById(grades[index]);
      gradePointsText = gradePoints.options[gradePoints.selectedIndex].text;
      gradesSum = gradesValue * gradePointsText;
      tempText += gradesSum;
      tempGradesText += parseInt(gradePointsText);
    }
  }  
  if(tempGradesText != 0 && tempText != 0){
    if(tempGradesText <= 6 && tempText >= 13){
      msgFlag = 1;
    }else if(tempGradesText <= 6 && tempText < 13){
      msgFlag = 2;
    }
  }
  return msgFlag;
}
//Added by Indumathi.S for responsive Nov-03-15 Rel
$fp(document).ready(function(){
  $fp("#mbd, #content-blk").click(function(e) {
    if(e.target.id != 'yeartoJoinCourse1' && e.target.id != 'yeartoJoinCourse2' && e.target.id != 'yeartoJoinCourse3' && e.target.id != 'yeartoJoinCourse4') {
      blockNone('yeartoJoinCourse1YText', 'none');
      blockNone('yeartoJoinCourse2YText', 'none');
      blockNone('yeartoJoinCourse3YText', 'none');
      blockNone('yeartoJoinCourse4YText', 'none');
    }
  }); 
   $fp('.c_txt').blur(function () {
 // alert("1");
  var x = $fp(this).val();
if (x != "") {
$fp(this).next('label').addClass("top_lb");
} else {
$fp(this).next('label').removeClass("top_lb");
}
});
$fp('.c_txt').each(function(index) { 
//alert("2");
var xy = $fp(this).val();
if (xy != "") {
$fp(this).next('label').addClass("top_lb");
} else {
$fp(this).next('label').removeClass("top_lb");
} 
});
});
//
function labelTopOnBlur(o) {
	var value = $fp(o).val();
	if (value != "") { $fp(o).next('label').addClass("top_lb"); } 
	else { $fp(o).next('label').removeClass("top_lb"); }
}
//Added by Indumathi.S for focusing the field where error message is shown Nov-03-15
var flag = 0;
function focusElement(divId,lastElem){
if(document.getElementById(divId)) {
    var searchEles = document.getElementById(divId).children;
    for(var i = 0; i < searchEles.length; i++) {
      if ($fp(searchEles[i]).children().length > 0 ) {
        subFocusElement($fp(searchEles[i]),lastElem);
      }
    }
    flag = 0;
  }
}
function subFocusElement(divId,lastElem){
  var searchEles = $fp(divId).children();  
  for(var i = 0; i < searchEles.length; i++) {
    if ($fp(searchEles[i]).children().length > 0 ) {
      if($fp(searchEles[i]).hasClass('qlerr') && $fp(searchEles[i]).attr("id") != lastElem) {
        var childDiv = $fp(searchEles[i]).children();
        if(flag == 0){
          for(var j = 0; j < childDiv.length; j++) {
            var divPosition = $fp(childDiv[j]).offset();
            if(childDiv[j].tagName == 'INPUT') {
              //alert("inside input-->"+childDiv[j]);
              $fp('html, body').animate({ scrollTop: divPosition.top - 100 }, "slow");
              $fp(childDiv[j]).focus();
              flag = flag + 1;
            } else if(childDiv[j].tagName == 'DIV' || childDiv[j].tagName == 'FIELDSET') {
            //alert("inside DIV-->"+childDiv[j]);
              $fp('html, body').animate({ scrollTop: divPosition.top - 100 }, "slow");
              flag = flag + 1;
              break;
            }
          }
        }
      } else {
        subFocusElement($fp(searchEles[i]),lastElem);
      }
    } 
  }
}
function showAndHideToolTip(id){
  if($$(id).style.display == "block") {
   blockNone(id, 'none');
  } else {
   blockNone(id, 'block');
  }
}
function postCodeValCheck(id){
    if(trimString($$(id).value) != ""){
      setErrorAndSuccessMsg("postcodeIndexField", "w50p fr adlog sumsg", "errorPostcodeIndex", formErrorMessage.postCodeSuccessMsg); //changed text by Prabha on 31_July_2018_rel
      $$('errorPostcodeIndex').className = "sutxt";
    }else{
      setErrorAndSuccessMsg("postcodeIndexField", "w50p fr adlog qlerr", "errorPostcodeIndex", "Please enter the postcode");
      $$("errorPostcodeIndex").className = "qler1";
    }
}