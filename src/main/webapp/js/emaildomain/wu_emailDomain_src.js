var $$DD = jQuery.noConflict();
function $1(docid){ return document.getElementById(docid); }
$$DD.fn.extend({
 autoEmail : function(paramVal){
	 if(paramVal != null & paramVal.length > 0){
      var domainRstlObj = new domainRstl();      
      $emailIdObj = $$DD("#" + paramVal[0]);
      $emailId = paramVal[0];      
      paramVal.splice(0, 1);
      domainRstlObj.key_up_params = "";
      for(var lps in paramVal){
        domainRstlObj.key_up_params += paramVal[lps]
      }
      var divTag = "<div id=\"autoEmailId\" style=\"display:none\"></div>";      
      if ($$DD($emailIdObj).next('label').length > 0) {
        $$DD($emailIdObj).next('label').after(divTag); 
      }else{
        $$DD($emailIdObj).after(divTag); 
      }      
      domainRstl(domainRstlObj);    
	 }
 }
});

var $emailIdObj;
var $emailId;
function domainRstl(domainRstlObj){ (
  function(){      
    var $ajaxDivObj = $$DD("#autoEmailId");    
    var email_event_fns = {};
    var key_up_params;
    var $curentObj = domainRstlObj;
    var parentDivClass = "autocomplete new_embgr";
    var symbolAt = "@";
    var openDivClassSlct = "ky_embgr";
	  var openDivClass  = "optionDiv";      
    email_event_fns.on_key_up = function(event){
      email_event_fns.on_ddl_show_rstls(event);
    }
    email_event_fns.call_domain_fn = function(event){
      //alert('$curentObj.key_up_params : '+$curentObj.key_up_params);
  	  eval($curentObj.key_up_params);
  	  $$DD("#emailsuccess").hide();
   	  email_event_fns.load_list_domain(event);
    }
    email_event_fns.on_ddl_down_fn = function(event){
  	  if($ajaxDivObj.css("display") == "block"){
    	  var divClass = "." + openDivClassSlct;
    	  if($ajaxDivObj.children().hasClass(openDivClassSlct)){
		    var $crntLiObj = $$DD(divClass);
		  	if($crntLiObj.index() < ($ajaxDivObj.children().length - 1)){
		  	  $crntLiObj.removeClass(openDivClassSlct).next().addClass(openDivClassSlct);
		  	  email_event_fns.roll_over_ddl();
		  	}
		  }else{
			  $$DD("#autoEmailId div:first").addClass(openDivClassSlct);
		  }
    }}      
    email_event_fns.on_ddl_up_fn = function(event){
    	if($ajaxDivObj.css("display") == "block"){
    	  if($ajaxDivObj.children().hasClass(openDivClassSlct)){
    		var divClass = "." + openDivClassSlct;
		    var $crntLiObj = $$DD(divClass);
		  	if($$DD($crntLiObj).index() > 0){
		  	  $$DD($crntLiObj).removeClass(openDivClassSlct).prev().addClass(openDivClassSlct);
		  	  email_event_fns.roll_over_ddl();
		  	}}
      }  
    }
    email_event_fns.on_submit_fn =  function(event){
    	if($ajaxDivObj.css("display") == "block"){
    	  if($ajaxDivObj.children().hasClass(openDivClassSlct)){
      		setDomainEmail($$DD("." + openDivClassSlct).attr('id'));
    	  }
     	}else{
     	  eval($curentObj.key_up_params);	
     	}
    }      
    email_event_fns.on_ddl_show_rstls = function(events){
      switch(events.keyCode){
        case 40:
        	email_event_fns.on_ddl_down_fn(events);
         	break;
        case 38:
         	email_event_fns.on_ddl_up_fn(events);
	  	    break;
	  	  case 13:
	  	   	email_event_fns.on_submit_fn(events);
	  	   	break;
	  	  default:
	  	   	email_event_fns.call_domain_fn(events);
	  	    break;
      }
    }      
    $$DD($emailIdObj).keyup(function(e){
    	email_event_fns.on_key_up(e);
	  });      
    email_event_fns.load_list_domain = function(event){
    	try{
        if($$D('domainLstId')){
        	$ajaxDivObj.html('').hide();
    	    if($emailIdObj.val().indexOf(symbolAt) > -1){
    	      var emailInpValArr = $emailIdObj.val().split(symbolAt);
    	      var resultData = "";
    	      if(emailInpValArr != null && emailInpValArr.length == 2){
    	    	if(!isBlankOrNull(emailInpValArr[0]) && emailInpValArr[0].trim().length > 0){
    	    	  resultData = email_event_fns.get_list_domain_fn(emailInpValArr, true);
    	    	}}
    	      if(!isBlankOrNull(resultData) && resultData.length > 0){
    	    	var autoClsName = parentDivClass;
    	    	if($$D('autoEmlAddCls')){
    	        autoClsName += " " + getIdValue('autoEmlAddCls');
    	    	}
          var emailWidth = parseInt($emailIdObj.css('width').split('px')[0]);
          var emailObjId = $emailIdObj.attr('id');
          var divWidth = 25;
          if($emailIdObj.attr('id') == 'email'){
            divWidth = 38;
          }
          emailWidth = emailWidth + divWidth; 
      			$ajaxDivObj.addClass(autoClsName).html(resultData).css({"width" : emailWidth, "position" : "absolute"}).show();
      		  }
    	    }
    	  }
    	}catch(e){
    	  console.log("Error   " + e);
    	}
    }
      
    email_event_fns.get_list_domain_fn = function(emailInpValArr, isSubstitute){
    	var openDiv = "<div ";
      var clsOpenDiv = ">";
  	  var endDiv = "</div>";
  	  var $domainLstObj = $$DD("#domainLstId");
  	  var matchesArr = [];
  	  var resultData = "";
  	  var domainLstArr = $domainLstObj.val().split("||");
  	  var loopMxSze = (domainLstArr) ? domainLstArr.length : 0;
  	  if(loopMxSze > 0){
  	    if(isSubstitute){
  	      var emlAftSymb = $$DD.trim(emailInpValArr[1]);
    		  var inpEmlSize = emlAftSymb.length;
    		  for(var lp = 0; lp < loopMxSze; lp++){
    		    if(emlAftSymb == (domainLstArr[lp].substring(0,inpEmlSize))){
    			    matchesArr.push(domainLstArr[lp]);
    		    }
    		  }
        }else{
  	    	matchesArr = domainLstArr;
  	    }
  	    var idAttrVal = "", curntText = "", dmnTxt = "", arrAftAtTxt="";
  		  if(matchesArr != null && matchesArr.length > 0){
  			loopMxSze = matchesArr.length;
  			for(var selector = 0; selector < loopMxSze; selector++){
          idAttrVal = ("emalDivId" + selector);
          dmnTxt = "";
          arrAftAtTxt = $$DD.trim(emailInpValArr[1]);
          if(arrAftAtTxt != matchesArr[selector]){
            dmnTxt = "<b>" + ((isSubstitute && !isBlankOrNull(arrAftAtTxt)) ? matchesArr[selector].substring(arrAftAtTxt.length) : matchesArr[selector]) + "</b>"
          }
          curntText = $$DD.trim(emailInpValArr[0]) + symbolAt + arrAftAtTxt + dmnTxt;             
          resultData += openDiv + "class=\"" + openDivClass + "\"" 
                       + " onclick=\"setDomainEmail('" + idAttrVal +"');\"" 
                       + " id=\""+ idAttrVal + "\" " + clsOpenDiv + curntText + endDiv;
  			}}
      }
  	  return resultData;
    }
    email_event_fns.roll_over_ddl = function(){
      var ids = $ajaxDivObj.find("div.ky_embgr").attr("id");
    	var liOffSetTop = $1(ids).offsetTop;
    	var divId = 'autoEmailId';
    	if($1(divId).offsetHeight < liOffSetTop){
    	  $1(divId).scrollTop = liOffSetTop - $1(divId).offsetTop;	  
    	}else{
    	  $1(divId).scrollTop = 0;
    	}
    }
  })();
}
function setDomainEmail(ids){
  var divHtml = $$DD("#" + ids).html();
  if(!isBlankOrNull(divHtml)){
	$$DD("#autoEmailId").html('').hide();
	$$DD("#useralreadyexist").hide();
	$$DD("#"+$emailId).val('').val(divHtml.replace("<b>", "").replace("</b>", "").replace("<B>", "").replace("</B>", "")).trigger('blur');
  }
}
$$DD(document).ready(function(){
  $$DD(document).click(function (event) { 
    if ($$DD(event.target).closest("#"+$emailId).length === 0 && $$DD(event.target).closest("#autoEmailId").length === 0) {
      $$DD("#autoEmailId").html('').hide();
    }
  });
});
function isBlankOrNull(val){
if(val != "" || val != null) { return false;} else { return true;}
}
function addFltCls(id, ids){
 var xy = $$DD("#"+id).val();
  if (xy != "") {
    $$DD("#" + ids).addClass("top");
  }else {
    $$DD("#" + ids).removeClass("top");
  } 
}
function clearErrorMsg(divId, msgId){
 var rmvcls = 'qlerr';
 var bnId = 'errorEmailExist';
 if("divEmailId" == divId){
   rmvcls = 'enqerr';
   bnId = 'errorEmailExists';   
 }
 if("wugoEmailId" == divId){
   divId = 'email';
   rmvcls = 'faild';
 }
 if(msgId == "ebookEmailAddress_error"){
   rmvcls = 'faild';
 }
 $$DD("#" + msgId).hide();
 $$DD("#" + divId).removeClass(rmvcls);
 if($$(bnId)){blockNone(bnId, 'none') }
}
function clearLoginErrorMsg(msgId){ 
 $$DD("#" + msgId).hide();
}
function hideEmailDropdown(event, autoEmaiId){
  if(event.keyCode==9){
    if($$D(autoEmaiId)){
      blockNone(autoEmaiId, 'none'); 
    }
  }
}
$$DD(document).ready(function(){
  $$DD(document).on("keypress", ":input:not(div)", function(event) {
      if (event.keyCode == 13) {
          event.preventDefault();
      }
  });
});