function validatePostCode(pv_postcode){ //check postcode format is valid
 test = pv_postcode;
 size = test.length
 test = test.toUpperCase(); //Change to uppercase
 var displayMessage = 'Please enter a valid postcode.\n or: If you are sure that you have entered a valid postcode but it is not being accepted by the system';
 validPostCodeChars = "abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
 s = stripCharsInBag(test,validPostCodeChars);
 if (s != "")
 {
  //alert("Please enter a valid Postcode, plus space e.g.W6 0QU.");
  alert(displayMessage);
  document.getElementById("postcode").focus();
  return false;
  }
 while (test.slice(0,1) == " ") //Strip leading spaces
  {test = test.substr(1,size-1);size = test.length
  }
 while(test.slice(size-1,size)== " ") //Strip trailing spaces
  {test = test.substr(0,size-1);size = test.length
  }
 if (size < 6 || size > 8){ //Code length rule
  alert(displayMessage);
  document.getElementById("postcode").focus();
  return false;
  }
 if (!(isNaN(test.charAt(0)))){ //leftmost character must be alpha character rule
  alert(displayMessage);
  document.getElementById("postcode").focus();
   return false;
  }
 if (isNaN(test.charAt(size-3))){ //first character of inward code must be numeric rule 
   alert(displayMessage);
  document.getElementById("postcode").focus();
   return false;
  }

 if (!(isNaN(test.charAt(size-2)))){ //second character of inward code must be alpha rule
   alert(displayMessage);
   document.getElementById("postcode").focus();
   return false;
  }

 if (!(isNaN(test.charAt(size-1)))){ //third character of inward code must be alpha rule
   alert(displayMessage);
   document.getElementById("postcode").focus();
   return false;
  }

 if (!(test.charAt(size-4) == " ")){//space in position length-3 rule
   alert(displayMessage);
   document.getElementById("postcode").focus();
   return false;
 }

 count1 = test.indexOf(" ");count2 = test.lastIndexOf(" ");
 if (count1 != count2){//only one space rule
   alert(displayMessage);
   document.getElementById("postcode").focus();
   return false;
  }
  return true;
}

function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}
