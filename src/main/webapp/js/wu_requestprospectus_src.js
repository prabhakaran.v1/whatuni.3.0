 var address1;
 var address2;
 var town;
 var opener_postcode;
 var globalCurrentErrorMsg = '';
 var dataSharing = '';
 var newsLetter = '';
 var updates = '';
 var surveys = '';
 var consentGA = '';
function trimStr(inStr){
  return inStr.replace(/^\s+|\s+$/g,"");
}
var dev = jQuery.noConflict();
function $$(id){
  return document.getElementById(id);
}

function hideSubmitButton(id){
  $$(id).style.display='none'; 
}

function showSubmitButton(id){
  $$(id).style.display='block'; 
}

function isNotNullAndUndex(value) {
  return (value != '' && value != undefined && value != null && value != 'null');
}

var ErrorMessage = { 
  error_login_password         : 'Please enter your password',
  error_firstName              : 'Please enter your first name',
  error_surName                : 'Please enter your last name',
  error_userName               : 'Enter your user name',
  error_valid_email            : "That doesn't look like a real email address?",
  error_user_email             : 'Please enter your email',
  error_country                : 'Please select country of residence', 
  error_address1                : 'Please enter Address line 1', 
  error_town                    : 'Please enter town', 
  error_date                   : "Happy birthday to you on the.....?",
  error_dob                    : "Don't expect a birthday card. Soz",
  //Added new validation messages for user registration and enquiry page, By Thiyagu G for 03_Nov_2015.
  error_reg_firstName          : "We still don't know your name. Remind us?",
  error_reg_surName            : "We still don't know your last name. Remind us?",
  error_reg_user_email         : 'Something seems to be missing from your email. Awkward.',
  error_reg_confirm_pass       : "We still don't know your confirm password. Remind us?",
  error_reg_valid_email        : 'Something seems to be missing from your email. Awkward.',
  error_reg_yoe                : 'When are you becoming a fresher?',
  error_dob                    : "Happy birthday to you on the.....?",
  error_country                : 'Please select country of residence', 
  error_address1               : 'Please enter Address line 1', 
  error_town                   : 'Please enter town',
  error_dob_sucs               : "Don't expect a birthday card. Soz",
  error_umessage               : "There are some illegal characters in the text you supplied. Tut tut!",
  error_email_sucs             : "You've just got to be on email",
  error_termsc                 : 'Please agree to our terms and conditions',
  error_reg_login_password     : "We still don't know your password. Remind us?",
  error_password_length        : "Your password should be 6 characters or more",
  error_empty_basket           : "Oops! your prospectus basket is empty!",
  sucess_post_code             : "Got it!",
  error_non_eng_language	   : "Please double check your message and remove any special characters",
  error_bee_trap               : "Your request cannot be processed at this time"
}

 var eq = jQuery.noConflict();  
function showErrorMsg(thisid, errmsg, diverror){  if($$(thisid) !=null){ $$(thisid).style.display = 'block';  $$(thisid).innerHTML = errmsg;} if(diverror!=""){ eq("#"+diverror).addClass("qlerr")} }
 
function validateInteractionFormAndLogOmniture(obj,whichInteractionForm, collegeName, cpeValue){	
	var collegeId = $$('omnitureCollegeId').value;
	var subOrderItemId = $$('subOrderItemId').value;
	var cpeQualificationNetworkId = $$('cpeQualificationNetworkId').value;
	var utmSource;
 var autoEmailFlag;
  if($$('autoEmailFlag')){
    autoEmailFlag = $$('autoEmailFlag').value;
  }
	var validationFlag = false;
	if(whichInteractionForm != null && whichInteractionForm == 'COLLEGE_EMAIL'){
    utmSource = "Email";
		validationFlag = validateForm(whichInteractionForm);  
                var msg = $$('umessage').value;
                if(msg.indexOf('\[url=') != -1 || msg.indexOf('/link') != -1){
                    alert("Sorry your email contains undeliverable content.  Please make sure your message contains no urls, advertising or offensive language before sending.");
                    $$('sendEmail').style.display='block'; 
                    return false;
                }
	}else if(whichInteractionForm != null && whichInteractionForm == 'COLLEGE_PROSPECTUS'){
    utmSource = "Prospectus";
		  validationFlag = validateForm();
	}else if(whichInteractionForm != null && whichInteractionForm == 'DOWNLOAD_PROSPECTUS'){
    utmSource = "Prospectus-dl";
   validationFlag = validateForm();//3_JUN_2014
  }
	if(validationFlag && autoEmailFlag == "Y"){
    var widgetId = $$('widgetId').value;
    document.getElementById('qlform').action = "/degrees/send-college-email.html?utm_source="+utmSource+"&utm_medium="+cpeValue+"&utm_term="+widgetId+"&utm_content="+collegeName+"&utm_campaign=Automated_Interaction";
  }
	if((whichInteractionForm != null && whichInteractionForm == 'COLLEGE_EMAIL')){
    if(validationFlag){      
      ga('send', 'pageview', {'hitCallback': function(){document.getElementById('qlform').submit();}}); //Added by Amir for UA logging for 24-NOV-15 release
      
      if($$('dim12Val') && $$('dim12Val').value != "" && $$('referrerURL_GA') && (($$('referrerURL_GA').value).indexOf('university-profile') > -1)){        
        ga('set', 'dimension1', 'contenthub.jsp');
        ga('set', 'dimension12', ($$('dim12Val').value).toLowerCase());        
        var locUrl = $$('referrerURL_GA').value;        
        ga('set', 'location', locUrl);
      }
      if($$('enquiryExistFlag') && $$('enquiryExistFlag').value != 'Y'){
        GAInteractionEventTracking('emailsubmit', 'interaction', 'Email', collegeName, cpeValue);
        if($$('dataShareFlag')){
        if($$('dataShareFlag').checked){
        	dataSharing = "Data Sharing"
        }
        }
		if($$('surveyEmailFlag')){
        if($$('surveyEmailFlag').checked){
        	surveys ="Surveys";
        }}
		if($$('solusEmailFlag')){
        if($$('solusEmailFlag').checked){
        	updates = "Updates";
        }}
		if($$('marketingEmailFlag')){
        if($$('marketingEmailFlag').checked){
        	newsLetter = "Newsletter";	
        }}
        consentGA = dataSharing +"|"+newsLetter+"|"+updates+"|"+surveys;
        GAInteractionEventTracking('emailsubmit', 'Opt In', 'Enquiry Form', consentGA,'');
      }
    }else{
      showSubmitButton('sendEmail');
    }
	}else if((whichInteractionForm != null && whichInteractionForm == 'COLLEGE_PROSPECTUS')){   
    if(validationFlag){//3_JUN_2014      
      ga('send', 'pageview', {'hitCallback': function(){document.getElementById('qlform').submit();}}); //Added by Amir for UA logging for 24-NOV-15 release
      if($$('dim12Val') && $$('dim12Val').value != "" && $$('referrerURL_GA') && (($$('referrerURL_GA').value).indexOf('university-profile') > -1)){
        ga('set', 'dimension1', 'contenthub.jsp');
        ga('set', 'dimension12', ($$('dim12Val').value).toLowerCase());
        var locUrl = $$('referrerURL_GA').value;
        ga('set', 'location', locUrl);
      }
      if($$('enquiryExistFlag') && $$('enquiryExistFlag').value != 'Y'){
        GAInteractionEventTracking('prospectussubmit', 'interaction', 'Prospectus', collegeName, cpeValue);
        if($$('dataShareFlag')){
            if($$('dataShareFlag').checked){
            	dataSharing = "Data Sharing"
            }
            }
			if($$('surveyEmailFlag')){
            if($$('surveyEmailFlag').checked){
            	surveys ="Surveys";
            }}
			if($$('solusEmailFlag')){
            if($$('solusEmailFlag').checked){
            	updates = "Updates";
            }}
			if($$('marketingEmailFlag')){
            if($$('marketingEmailFlag').checked){
            	newsLetter = "Newsletter";	
            }}
            consentGA = dataSharing +"|"+newsLetter+"|"+updates+"|"+surveys;
            GAInteractionEventTracking('prospectussubmit', 'Opt In', 'Prospectus Form', consentGA,'');
      }   
    }else{
      showSubmitButton('getProspectus');
    }    
	}else if(whichInteractionForm != null && whichInteractionForm == 'DOWNLOAD_PROSPECTUS'){
    if(validationFlag){//3_JUN_2014    
    ga('send', 'pageview', {'hitCallback': function(){document.getElementById('qlform').submit();}}); //Added by Amir for UA logging for 24-NOV-15 release
    GAInteractionEventTracking('dpsubmit', 'interaction', 'Prospectus-dl', collegeName, cpeValue);
    }else{
      showSubmitButton('downloadProspectus');
    }
  }      
  
  if(!validationFlag){    
    focusElement("pros-pod","divCountryOfRes");  
  }
return false;  
}

function validateForm(whichInteractionForm){
 var returnflag=true;
 eq(".qlerr").removeClass("qlerr"); 
 if(eq('div').hasClass('.qler1')){
   eq('.qler1').hide();
 }
 var alphaNumberic = /(([A-Za-z].*[0-9])|([0-9].*[A-Za-z]))/;
 var specialCharacter = /[^A-Za-z \d]/;
 var letters = /^[\-a-zA-Z0-9\s\.\,\:\[\]\`\;\~\''\""\-\@\#\?\%\/\=\&\^\{\}\<\>\$\*\|\!\(\)\-\+\_\\]*$/;
 
 var fName = trimString($$('fname').value);
 var lName = trimString($$('lname').value);
 var email = trimString($$('email').value);
 var country = $$('countryOfResidence') ? trimString($$('countryOfResidence').value) : "";
 var pgFlag = $$("pgFlag").value;
 var postcode = trimString($$("postcodeIndex").value); 
 //var textDate = trimString($$("dateDOB").value); 
 //var textMonth = trimString($$("monthDOB").value); 
 //var textYear = trimString($$("yearDOB").value);
 var datemsg = "";  
 var errFlag=true;
 var errMsg ='';
 var addrLine1 = $$('address1') ? trimString($$('address1').value) : "";
 var town = $$('town') ? trimString($$('town').value) : "";
 var password = $$("password") ? encodeURIComponent(trimString($$("password").value)) : ""; 
 if($$("password")){
 if(isEmpty(password) || password == 'Password*'){ returnflag = false; setErrorAndSuccessMsg('divPassword','w100p adlog qlerr','errorPassword', ErrorMessage.error_reg_login_password);  
 } else if(!isEmpty(password) && password.length < 6){
   returnflag = false; setErrorAndSuccessMsg('divPassword','w100p adlog qlerr','errorPassword', ErrorMessage.error_password_length);
 } else{setErrorAndSuccessMsg('divPassword','w100p adlog sumsg','errorPassword','');}  
 }
 if(email != null && trimString(email) != ''){
  if(!emailCheck(email)){    
   setErrorAndSuccessMsg('divEmail','w100p adlog qlerr','errorEmail', ErrorMessage.error_valid_email);
   returnflag = false;
  }else{   
   var isdisabled = $$('email').readOnly;   
   if(isdisabled!=true){
     if($$('emailAlreadyExistsflag') && $$('emailAlreadyExistsflag').value=='yes'  ){
         var text =  "The email address " + email + " is already registered with us, please login. This is to protect your personal data";
         showErrorMsg('errorEmail', text, 'divEmail');
        returnflag = false;
     } 
   }
  }
 }else{   
   setErrorAndSuccessMsg('divEmail','w100p adlog qlerr','errorEmail', ErrorMessage.error_reg_valid_email);
   returnflag = false;
 }
 if($$("postcode") && $$('postcodeField').style.display != 'none'){
   if($$("postcode").value!=""){
     getPostCodeAddReg();
   }else{
     setErrorAndSuccessMsg('postcodeField','w50p fr adlog','postcodeErrMsg', "");
   } 
 }else{
  if($$('addressFields')&& $$('addressFields').style.display != 'none'){
  if(trimString($$("postcodeIndex").value) != ""){ //Changed By Thiyagu G for 19_Apr_2016.
    setErrorAndSuccessMsg("postcodeIndexField", "w50p fr adlog sumsg", "errorPostcodeIndex", ErrorMessage.sucess_post_code);
    $$('errorPostcodeIndex').className = "sutxt";
  }else{
    setErrorAndSuccessMsg("postcodeIndexField", "w50p fr adlog qlerr", "errorPostcodeIndex", "Please enter the postcode");
    $$("errorPostcodeIndex").className = "qler1";
    returnflag = false;
  }
  }
 }  
 //Changed to new validation message, By Thiyagu G for 03_Nov_2015.
 if(isEmpty(fName) || trimString(fName) == ''){
   returnflag = false;
   setErrorAndSuccessMsg('divfname','w50p fl adlog qlerr','errorfname',ErrorMessage.error_reg_firstName);
 }else if(whichInteractionForm == "COLLEGE_EMAIL"){
   if(!letters.test(fName)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
	 setErrorAndSuccessMsg('divfname','w50p fl adlog qlerr','errorfname',ErrorMessage.error_non_eng_language); 
	 returnflag = false;
   }
 }else{
   setErrorAndSuccessMsg('divfname','w50p fl adlog sumsg','errorfname', 'Nice to meet you! Great name!');
   $$D('errorfname').className="sutxt";
 }
 
 if(isEmpty(lName) || trimString(lName) == ''){
  returnflag = false;
  setErrorAndSuccessMsg('divlname','w50p fr adlog qlerr','errorlname',ErrorMessage.error_reg_firstName);
 }else if(whichInteractionForm == "COLLEGE_EMAIL"){
   if(!letters.test(lName)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
     setErrorAndSuccessMsg('divlname','w50p fr adlog qlerr','errorlname',ErrorMessage.error_non_eng_language); 
     returnflag = false;
   }
 } 
  var yoe = '';
  for (i=0; i<document.getElementsByTagName('input').length; i++) {
    if (document.getElementsByTagName('input')[i].type == 'radio'){
      if(document.getElementsByTagName('input')[i].checked == true){
       yoe = document.getElementsByTagName('input')[i].value;
       break;
      }
    } 
  }
  var cuYear = new Date().getFullYear();
  if(yoe < cuYear){
    setErrorAndSuccessMsg("yoeFeild", "ql-inp ErrMsg", "yoeErrMsg", "<p>Please choose when would you like to start?</p>");    
    returnflag = false;
  }else{
    setErrorAndSuccessMsg("yoeFeild", "ql-inp", "yoeErrMsg", "");
  }
 

 if($$('countryOfResidence')){
   if(country != null &&  country != '' && trimString(country) != 'Country of residence'){
      if($$("postcode") && $$("postcode").value!=""){
        getPostCodeAddReg();
      }else{
        setErrorAndSuccessMsg('postcodeField','w50p fr mb0 qlerr','postcodeErrMsg', "");
      }
      if(addrLine1 == null || trimString(addrLine1) == ''){
        showErrorMsg('errorAddress1', '', 'divAddress1');
        returnflag = false;      
      }else{blockNone('errorAddress1', 'none');}
      if((town == null || trimString(town) == '')){
        showErrorMsg('errorTown', '', 'divTown');
        returnflag = false;
      }else{blockNone('errorTown', 'none');}
    }else{     
     setErrorAndSuccessMsg('divCountryOfRes','mb0 qlerr','errorCountry', ErrorMessage.error_country);
     var pos = dev('#divCountryOfRes').offset().top;
     dev('body, html').animate({scrollTop: pos});
     returnflag = false;    
    }
  }
 
if(whichInteractionForm == "COLLEGE_EMAIL"){
 if(trimString(postcode) != "" && alphaNumberic.test(postcode) && !specialCharacter.test(postcode)){ //Added by Kailash L for adding validation for post code JIRA-817
     setErrorAndSuccessMsg("postcodeField", "w50p fl adlog sumsg", "postcodeErrMsg", formErrorMessage.postCodeSuccessMsg); 
     $$('postcodeErrMsg').className = "sutxt";
   } else{
	  if(!letters.test(postcode)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
	    setErrorAndSuccessMsg('postcodeField','w50p fl adlog qlerr','postcodeErrMsg',ErrorMessage.error_non_eng_language); 
	    returnflag = false;
	  }else if(trimString(postcode) != ""){
        returnflag = false;
        setErrorAndSuccessMsg("postcodeField", "w50p fl adlog qlerr", "postcodeErrMsg", "Please check you have entered your postcode correctly - max. 8 characters"); 
        $fp("#postcodeErrMsg").removeClass("sutxt").addClass("qler1");
      }else{
         $fp("#postcodeField").removeClass('qlerr');
         $fp("#postcodeField").removeClass('sumsg');
         $fp("#postcodeErrMsg").removeClass("qler1").addClass("sutxt");
         $fp("#postcodeErrMsg").html('');
         $fp("#postcodeErrMsg").hide();
      }
  }  
}

if(whichInteractionForm == "COLLEGE_EMAIL"){
  if($fp("#beeTrap") && isNotNullAndUndex($fp("#beeTrap").val())){
   returnflag = false; 
   showErrorMsg('beeTrapError',ErrorMessage.error_bee_trap); 
   GANewAnalyticsEventsLogging('Enquiry Form', 'Honey Pot', 'Field Filled');
  }else{
    blockNone("beeTrapError","none");
  } 	  
}

  if($$('errorumessage1')){
    blockNone("errorumessage1","none");
  }
  if($$('umessage')){
    var msg = $$('umessage').value;
    msg = trimString(msg);   
    var flag = containsSpecialCharacter($$('umessage'));
    var tagFlag = containsHtmlTags($$('umessage')); //Added by Prabha on 13_06_17
    if(!letters.test(msg)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
      showErrorMsg('errorumessage', ErrorMessage.error_non_eng_language, 'divfumessage');
      returnflag = false;
    }else if(trimString(flag)!="" || trimString(tagFlag)!=""){
      showErrorMsg('errorumessage', ErrorMessage.error_umessage, 'divfumessage');
      returnflag = false;     
    }else if(msg.length < 50){ //Added by Kailash L for adding validation for text area JIRA-817
      showErrorMsg('errorumessage', "Please be as descriptive as possible with your request - min. 50 characters", 'divfumessage');
      returnflag = false;   
    } else{
     blockNone("errorumessage","none");
    }
  }
   if($$D("enquiryFormProceed") == null || $$D("enquiryFormProceed").checked == false){ 
     returnflag = false; showErrorMsg('termsc_error',ErrorMessage.error_termsc); 
   }else{
     blockNone("termsc_error","none");
   }						 
  if(returnflag==false){return false; /*dev("getProspectus").show(); */}
  return true;
}
function validateBulkProspectusIntrctnmAndLogOmniture(obj,whichInteractionForm, collegeName, cpeValue){
  /*var pgFlag = document.getElementById("pgFlag").value;
  if(pgFlag!="Y"){
    var result = getEntryGradeMultiSet();
    if(result==false){
      return false;
    }
  }*/
  var utmSource;
  var validationFlag = false;
  if(whichInteractionForm != null && whichInteractionForm == 'COLLEGE_PROSPECTUS'){
    utmSource = "Prospectus";
    validationFlag = validateProspectusForm();
  }
  if((whichInteractionForm != null && whichInteractionForm == 'COLLEGE_PROSPECTUS')){
    if(validationFlag){//3_JUN_2014          
      // _gaq.push(['_set','hitCallback',function() {document.getElementById('qlform').submit();}]);      
      //GAInteractionEventTracking('prospectussubmit', 'interaction', 'Prospectus', '', '');   
    	if($$("collegeIdLists")){
    	var collegeIdValue = $$("collegeIdLists").value;
    	var collegeIdArray = collegeIdValue.split("###");
    	for (i = 0; i < collegeIdArray.length; i++) {
    	  if($$('consentFlag_'+collegeIdArray[i])){
    	    if($$('consentFlag_'+collegeIdArray[i]).checked){
    	      dataSharing = "Data Sharing"
    	    }
    	  }
    	}
        }
		  if($$('surveyEmailFlag')){
          if($$('surveyEmailFlag').checked){
          	surveys ="Surveys";
          }
		  }
		  if($$('solusEmailFlag')){
          if($$('solusEmailFlag').checked){
          	updates = "Updates";
          }
		  }
		  if($$('marketingEmailFlag')){
          if($$('marketingEmailFlag').checked){
          	newsLetter = "Newsletter";	
          }
          }
          consentGA = dataSharing +"|"+newsLetter+"|"+updates+"|"+surveys;
          GAInteractionEventTracking('prospectussubmit', 'Opt In', 'Prospectus Form', consentGA,'');
      document.getElementById('qlform').submit();
    }else{
      //document.getElementById('qlform').submit();
    }    
  }    
  focusElement("pros-pod","divCountryOfRes");
  return validationFlag;  
}
function isValidEmail(email){
	if(email.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) == -1){
		return false;
	}
	return true;
}
function emailCheck(email){
  if(/^\w+([-+.'']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(trimString(email))){
    return true
  }
  return false
}
function isValidDate(date){
	if(date.search(/^(((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00|[048])))$/) == -1){
		return false;
	}
	return true;
}
function isValidUKpostcode(postcode){
	if(postcode.search(/^[a-zA-Z]((\d)|((\d[a-zA-Z])|([a-zA-Z]\d)|(\d\d))|(([a-zA-Z]\d\d)|([a-zA-Z]{2}\d)|([a-zA-Z]\d[a-zA-Z])))[ ]\d[a-zA-Z]{2}$/) == -1){
		return false;
	}
	return true;
}
function validateProspectusForm(){
  var fName = trimString($$('fname').value);
	 var lName = trimString($$('lname').value);	 
  var email = trimString($$('email').value);  
  //var textDate = trimString($$("dateDOB").value);  
 // var textMonth = trimString($$("monthDOB").value);  
 // var textYear = trimString($$("yearDOB").value);
  var datemsg = "";
  var errMsg ='';
	 var postcode = trimString($$("postcodeIndex").value);
	 //var nonUkResident = $$('nonukresident').value;
	 var addrLine1 = trimString($$('address1').value);
	 var town = trimString($$('town').value);
	 var country = $$('countryOfResidenceSel').value;
   var password = $$("password") ? trimString($$("password").value) : ""; 
  if ($$('termsc') != null){
	   var tacFlag = $$('termsc').value;	
  }	
  var message = true;
  //Changed to new validation message, By Thiyagu G for 03_Nov_2015.
  if($$("password")){//added password validation by sangeeth.S in 13.7.18 to fix bug
    if(isEmpty(password) || password == 'Password*'){ message = false; setErrorAndSuccessMsg('divPassword','w100p adlog qlerr','errorPassword', ErrorMessage.error_reg_login_password);  
    } else if(!isEmpty(password) && password.length < 6){
     message = false; setErrorAndSuccessMsg('divPassword','w100p adlog qlerr','errorPassword', ErrorMessage.error_password_length);
    } else{setErrorAndSuccessMsg('divPassword','w100p adlog sumsg','errorPassword','');}  
  }
  if(isEmpty(fName) || trimString(fName) == ''){message = false;setErrorAndSuccessMsg('divfname','w50p fl adlog qlerr','errorfname',ErrorMessage.error_reg_firstName);}
  else{setErrorAndSuccessMsg('divfname','w50p fl adlog sumsg','errorfname', 'Nice to meet you! Great name!');$$D('errorfname').className="sutxt";}	 
  if(isEmpty(lName) || trimString(lName) == ''){message = false;setErrorAndSuccessMsg('divlname','w50p fr adlog qlerr','errorlname',ErrorMessage.error_reg_firstName);}
  if(email != null && trimString(email) != ''){
   if(!emailCheck(email)){ 
   showErrorMsg('errorEmail', ErrorMessage.error_valid_email, 'divEmail');
   message = false;
   returnflag = false;
  }else{   
   var isdisabled = $$('email').readOnly;   
   if(isdisabled!=true){
     if($$('emailAlreadyExistsflag') && $$('emailAlreadyExistsflag').value=='yes'  ){
         var text =  "The email address " + email + " is already registered with us, please login. This is to protect your personal data";
         showErrorMsg('errorEmail', text, 'divEmail');
        message = false;
        returnflag = false;
   } 
  }
  }
 }else{
   showErrorMsg('errorEmail', ErrorMessage.error_reg_valid_email, 'divEmail');
   message = false;
   returnflag = false;
 }
 var yoe = '';
 for (i=0; i<document.getElementsByTagName('input').length; i++) {
   if (document.getElementsByTagName('input')[i].type == 'radio'){
     if(document.getElementsByTagName('input')[i].checked == true){
      yoe = document.getElementsByTagName('input')[i].value;
      break;
     }
   } 
 }
 var cuYear = new Date().getFullYear();
 if(yoe < cuYear || yoe == "No"){
   setErrorAndSuccessMsg("yoeFeild", "ql-inp ErrMsg", "yoeErrMsg", "<p>Please choose when would you like to start?</p>");    
   message = false;
   returnflag = false;
 }else{
   setErrorAndSuccessMsg("yoeFeild", "ql-inp", "yoeErrMsg", "");
 }  
  if((country != null &&  country != '' && trimString(country) != 'Country of residence')){    
    if(trimString($$("postcodeIndex").value) != ""){ //Changed By Thiyagu G for 19_Apr_2016.
    setErrorAndSuccessMsg("postcodeIndexField", "w50p fr adlog sumsg", "errorPostcodeIndex", "Got it!");
    $$('errorPostcodeIndex').className = "sutxt";
  }else{
    setErrorAndSuccessMsg("postcodeIndexField", "w50p fr adlog qlerr", "errorPostcodeIndex", "Please enter the postcode");
    $$("errorPostcodeIndex").className = "qler1";
    returnflag = false;
  }
     if($$("postcode") && $$('postcodeField').style.display != 'none'){
    if($$("postcode").value!=""){
      getPostCodeAddReg();
    }else{
      setErrorAndSuccessMsg('postcodeField','w50p fr','postcodeErrMsg', "");
    }
    }else{
    if($$('addressFields') && $$('addressFields').style.display != 'none'){
  if(trimString($$("postcodeIndex").value) != ""){ //Changed By Thiyagu G for 19_Apr_2016.
    setErrorAndSuccessMsg("postcodeIndexField", "w50p fr adlog sumsg", "postcodeErrMsg", ErrorMessage.sucess_post_code);
    $$('errorPostcodeIndex').className = "sutxt";
  }else{
    setErrorAndSuccessMsg("postcodeIndexField", "w50p fr adlog qlerr", "errorPostcodeIndex", "Please enter the postcode");
    $$("errorPostcodeIndex").className = "qler1";
    returnflag = false;
    message = false;
  }
  }
    }
    if(addrLine1 == null || trimString(addrLine1) == ''){      
      message = false;
      setErrorAndSuccessMsg('divAddress1','w100p fl adlog qlerr','errorAddress1', ErrorMessage.error_address1);      
    }else{setErrorAndSuccessMsg('divAddress1','w100p fl adlog','errorAddress1', ''); }
    if((town == null || trimString(town) == '')){
      message = false;
      setErrorAndSuccessMsg('divTown','w50p fl adlog qlerr','errorTown', ErrorMessage.error_town);      
    }else{setErrorAndSuccessMsg('divTown','w50p fl adlog','errorTown', '');}
  }else{    
    message = false;
    setErrorAndSuccessMsg('divCountryOfRes','mb0 qlerr','errorCountry', ErrorMessage.error_country);     
    var pos = dev('#divCountryOfRes').offset().top;
    dev('body, html').animate({scrollTop: pos});
  }
  //Added validate prospectus cout::Prabha::27_JAN_2016
	 if($$('interaction_pros_count')){
	   var pros_count = parseInt($$('interaction_pros_count').innerHTML);
	   if(pros_count < 1){
	     message = false;
      setErrorAndSuccessMsg('basketEmptyErr','err err_pros qler1','basketEmptyErr', ErrorMessage.error_empty_basket);  
	   }
	 }else{
	   message = false;
    setErrorAndSuccessMsg('basketEmptyErr','err err_pros qler1','basketEmptyErr', ErrorMessage.error_empty_basket);  
	 }
  if($$D("enquiryFormProceed") == null || $$D("enquiryFormProceed").checked == false){ 
    message = false; showErrorMsg('termsc_error',ErrorMessage.error_termsc); 
  }else{
    blockNone("termsc_error","none");
  }						 
  //End prospectus code
	 return message;
}
function setPostCodeAddressProspectus(obj){
  var postCodeAdd = obj.value;
  if(postCodeAdd != null && postCodeAdd != ""){
  var dataarray=postCodeAdd.split("###");
    $$('address1').value = dataarray[0];
    $$('address2').value = dataarray[1];
    $$('town').value = dataarray[2];
    $$('postcodeIndex').value = $$('postcode').value;    
    $$('lblAddress1').className = "lbco top";
    $$('lblAddress2').className = "lbco top";    
    $$('lblTown').className = "lbco top";
    $$('lblPostcodeIndex').className = "lbco top";
    setErrorAndSuccessMsg("divAddress1", "w100p fl adlog sumsg", "errorAddress1", "");
    setErrorAndSuccessMsg("divTown", "w50p fl adlog sumsg", "errorTown", "");
    setErrorAndSuccessMsg("postcodeIndexField", "w50p fr adlog sumsg", "errorPostcodeIndex", "");
    
  }else{
    $$('address1').value = "";
    $$('address2').value = "";
    $$('town').value = "";
    $$('postcodeIndex').value = "";
    $$('lblAddress1').className = "";
    $$('lblAddress2').className = "";    
    $$('lblTown').className = "";
    $$('lblPostcodeIndex').className = "";
    $$('lblAddress1').className = "lbco";
    $$('lblAddress2').className = "lbco";    
    $$('lblTown').className = "lbco";
    $$('lblPostcodeIndex').className = "lbco";
    setErrorAndSuccessMsg("divAddress1", "w100p fl adlog qlerr", "errorAddress1", "");
    setErrorAndSuccessMsg("divTown", "w50p fl adlog qlerr", "errorTown", "");
    alert('Choose valid address from list');    
  }
}
function disableUKresident() {
  with(document) {
      if (getElementById("nonukresident").checked == true){
          getElementById("postcode").disabled = true;
          getElementById("postcode").className = "ql-inpt disb";
          getElementById("nationality").style.display = 'block';
      }else{
          getElementById("postcode").disabled = false;
          getElementById("postcode").className = "ql-inpt";
          getElementById("nationality").style.display = 'none';
      }
 }
}
function showTermsConditionPopup(){ 
   var TCversion = $$('TCVersion') ? $$('TCVersion').value : '/degrees/help/termsAndCondition.htm';
   window.open(TCversion,"termspopup","width=650,height=400,scrollbars=yes,resizable=yes"); return false;
}
function showPrivacyPolicyPopup(){
    var PRVersion = $$('PRVersion') ? $$('PRVersion').value : '/degrees/help/privacy.htm';
    window.open(PRVersion,"termspopup","width=650,height=400,scrollbars=yes,resizable=yes"); return false;
}
function setProspectusCookie(){
with(document){
  SetCookie('emailAdd',getElementById('email').value,'','/','','');
  SetCookie('fname',getElementById('fname').value,'','/','','');
  SetCookie('lname',getElementById('lname').value,'','/','','');
  SetCookie('postcodePros',getElementById('postcode').value,'','/','','');
  SetCookie('nationalityPros',getElementById('nationality1').value,'','/','','');
  SetCookie('nonukresidentPros',getElementById('nonukresident').checked,'','/','','');
  SetCookie('address1',getElementById('address1').value,'','/','','');
  SetCookie('address2',getElementById('address2').value,'','/','','');
  SetCookie('town',getElementById('town').value,'','/','','');
  SetCookie('undergraduate',getElementById('undergraduate').checked,'','/','','');
  SetCookie('postgraduate',getElementById('postgraduate').checked,'','/','','');
  SetCookie('tac',getElementById('tac').checked,'','/','','');
  }
}  
function setDownloadProspectusCookie(){
    with(document){
        SetCookie('emailAdd',getElementById('email').value,'','/','','');
        SetCookie('fname',getElementById('fname').value,'','/','','');
        SetCookie('lname',getElementById('lname').value,'','/','','');
        SetCookie('undergraduate',getElementById('undergraduate').checked,'','/','','');
        SetCookie('postgraduate',getElementById('postgraduate').checked,'','/','','');
    }
}
function loadProspecusCookies(){
  var emailAddress = GetCookie('emailAdd');
  var firstName = GetCookie('fname');
  var lastName = GetCookie('lname');
  var postcode = GetCookie('postcodePros');
  var address1 = GetCookie('address1');
  var address2 = GetCookie('address2');
  var town = GetCookie('town');
  var undergraduate = GetCookie('undergraduate');
  var postgraduate = GetCookie('postgraduate');
  var nonukresident = GetCookie('nonukresidentPros');
  var tremcond = GetCookie('tac');
  var nationality = GetCookie('nationalityPros');
  with(document){
    if(emailAddress !=null && emailAddress!="") { getElementById('email').value=trimString(emailAddress); }
    if(firstName !=null && firstName!="")       { getElementById('fname').value=firstName; }
    if(lastName !=null && lastName!="")         { getElementById('lname').value=lastName; }
     if(nationality !=null && nationality!="")  { getElementById('nationality1').value=nationality; }
    if(postcode !=null && postcode!="")         { getElementById('postcode').value=postcode; }
    if(address1 !=null && address1!="")         { getElementById('address1').value=address1; }
    if(address2 !=null && address2!="")         { getElementById('address2').value=address2; }
    if(town !=null && town!="")                 { getElementById('town').value=town; }
    if(undergraduate == "true" &&  postgraduate == "true"){
       getElementById('undergraduate').checked=true;
       getElementById('postgraduate').checked=true;
    } else if(undergraduate == "true" && postgraduate == "false")  {
       getElementById('undergraduate').checked=true;
    } else if(postgraduate == "true" && undergraduate == "false") {
       getElementById('postgraduate').checked = true;
    }
    if(nonukresident !=null && nonukresident == "true") {
      getElementById('nonukresident').checked=true;
    }
    if(tremcond !=null && tremcond == "true") {
      getElementById('tac').checked=true;
    }
 }
}
function setEmailCookie(){
  with(document){
    SetCookie('emailAddress',trimString(getElementById('email').value),'','/','','');
    SetCookie('firstName',getElementById('fname').value,'','/','','');
    SetCookie('lastName',getElementById('lname').value,'','/','','');
    SetCookie('umessage',getElementById('umessage').value,'','/','','');
    if ($$('termsc') != null){
      SetCookie('termsc',getElementById('termsc').checked,'','/','','');
    }
   }
}
function loadEmailCookies(){
  var emailAddress = GetCookie('emailAddress');
  var firstName = GetCookie('firstName');
  var lastName = GetCookie('lastName');
  var umessage = GetCookie('umessage');
  var termsc = GetCookie('termsc');
  with(document){
    if(emailAddress !=null && emailAddress!="") { getElementById('email').value=trimString(emailAddress); }
    if(firstName !=null && firstName!="")       { getElementById('fname').value=firstName;  }
    if(lastName !=null && lastName!="")         { getElementById('lname').value=lastName; }
    if(umessage !=null && umessage!="")         { getElementById('umessage').value=umessage; }
    if(termsc !=null && termsc == "true")       { getElementById('termsc').checked=termsc; }
  }
}
function SetCookie (name,value,expires,path,domain,secure) {
	document.cookie = name + "=" + escape (value) +
	((expires) ? "; expires=" + expires.toGMTString() : "") +
	((path) ? "; path=" + path : "") +
	((domain) ? "; domain=" + domain : "") +
	((secure) ? "; secure" : "");
}
function GetCookie (name){
  var arg = name + "=";
  var alen = arg.length;
  var clen = document.cookie.length;
  var i = 0;
  while (i < clen) {
     var j = i + alen;
     if(document.cookie.substring(i, j) == arg) {
         return getCookieVal (j);
     }
     i = document.cookie.indexOf(" ", i) + 1;
     if (i == 0) break;
   }
  return null;
}
function getCookieVal (offset){
   var endstr = document.cookie.indexOf (";", offset);
   if (endstr == -1) { endstr = document.cookie.length; }
   return unescape(document.cookie.substring(offset, endstr));
}
function changeYearProspectus(){
  if ($$("studyLevel").value == "Postgraduate"){
      $$("futurestudent").style.display='block';
      $$("nonfuturestudent").style.display='none';
  }else{
      $$("futurestudent").style.display='none';
      $$("nonfuturestudent").style.display='block';
  }
}
function setBulkProspectusCookie(){
  with(document){
    SetCookie('emailbp',getElementById('emailbp').value,'','/','','');
    SetCookie('fnamebp',getElementById('fnamebp').value,'','/','','');
    SetCookie('lnamebp',getElementById('lnamebp').value,'','/','','');
    SetCookie('add1bp',getElementById('add1bp').value,'','/','','');
    SetCookie('add2bp',getElementById('add2bp').value,'','/','','');
    SetCookie('townbp',getElementById('townbp').value,'','/','','');
    SetCookie('postcodebp',getElementById('postcodebp').value,'','/','','');
    SetCookie('studyLevel',getElementById('studyLevel').value,'','/','','');
    SetCookie('genderProsbp',getElementById('genderProsbp').value,'','/','','');
    SetCookie('mobileNobp',getElementById('mobileNo').value,'','/','','');
    if (getElementById('studyLevel').value == 'Postgraduate'){
         SetCookie('prosYearUniEntrybp',getElementById('prosYearUniEntrybp').value,'','/','','');
        }else{
        SetCookie('prosYearFustudbp',getElementById('prosYearFustudbp').value,'','/','','');
        }
   }
}
function loadBulkProspecusCookies(){
  var emailAddress = GetCookie('emailbp');
  var firstName = GetCookie('fnamebp');
  var lastName = GetCookie('lnamebp');
  var gender = GetCookie('genderProsbp');
  var postcode = GetCookie('postcodebp');
  var address1 = GetCookie('add1bp');
  var address2 = GetCookie('add2bp');
  var town = GetCookie('townbp');
  var studyLevelbp = GetCookie('studyLevel');
  var prosYearFuStudbp = GetCookie('prosYearFustudbp');
  var prosYearUniEntrybp = GetCookie('prosYearUniEntrybp');
  var mobNo = GetCookie('mobileNobp');
  with(document){
   if(mobNo !=null && mobNo!="")  { getElementById('mobileNo').value=mobNo; }
    if(emailAddress !=null && emailAddress!="") { getElementById('emailbp').value=trimString(emailAddress); }
    if(firstName !=null && firstName!="")       { getElementById('fnamebp').value=firstName; }
    if(lastName !=null && lastName!="")         { getElementById('lnamebp').value=lastName; }
    if(gender !=null && gender!="")             { getElementById('genderProsbp').value=gender; }
    if(postcode !=null && postcode!="")         { getElementById('postcodebp').value=postcode; }
    if(address1 !=null && address1!="")         { getElementById('add1bp').value=address1; }
    if(address2 !=null && address2!="")         { getElementById('add2bp').value=address2; }
    if(town !=null && town!="")                 { getElementById('townbp').value=town; }
    if(studyLevelbp !=null && studyLevelbp!="")  { getElementById('studyLevel').value=studyLevelbp; }

    if(prosYearFuStudbp !=null && prosYearFuStudbp!="")   { getElementById('prosYearFustudbp').value=prosYearFuStudbp; }
    if(prosYearUniEntrybp !=null && prosYearUniEntrybp!="")   { getElementById('prosYearUniEntrybp').value=prosYearUniEntrybp; }
     if(studyLevelbp == 'Postgraduate'){
       getElementById("futurestudent").style.display='block';
       getElementById("nonfuturestudent").style.display='none';
    } else  {
       getElementById("futurestudent").style.display='none';
       getElementById("nonfuturestudent").style.display='block';
    }
 }
}
function validateBulkProspectus(){
 var totalcheckboxcount=0;
 var totalchecked = 0;
 $$("postcodebp").value = replacePostcode($$("postcodebp").value)
 setBulkProspectusCookie()
 with(document){
   if(getElementById("checkcount") != null){
      totalcheckboxcount = getElementById("checkcount").value;
      for(var cou = 0; cou <=totalcheckboxcount; cou++){
        if(getElementById("collegecheck_"+cou) !=null){
             if(getElementById("collegecheck_"+cou).checked == true){
               totalchecked++;
             }
        }
      }
      if (totalchecked > 0 && totalchecked > 30){
       if (validateFormElements() == 'TRUE'){
          displayStaticMessage("<p>You have selected " + totalchecked + " institutions to order prospectuses from.</p>"+
                           "<center>" +  "<a href='#' onclick='submitBulkProspectus();return false'>Order</a>" +
                            "<a href='#' onclick='closeMessage();return false'>Refine selection</a>" + "</center>"

                            );
                            return false;
          }
      }
       return true;
    }
  }
}
function submitBulkProspectus(){
    $$("bulkprospectus").action = $$("bulkprospectus").action +"?buttxt=submit";
    $$("bulkprospectus").submit();
    closeMessage();
    return false;
}
function displayMessage(url){
  messageObj = new DHTML_modalMessage();	// We only create one object of this class
  messageObj.setShadowOffset(5);	// Large shadow
  messageObj.setSource(url);
  messageObj.setCssClassMessageBox(false);
  messageObj.setSize(400,200);
  messageObj.setShadowDivVisible(true);	// Enable shadow for these boxes
  messageObj.display();
}
function displayStaticMessage(messageContent,cssClass){
  messageObj = new DHTML_modalMessage();	// We only create one object of this class
  messageObj.setShadowOffset(5);	// Large shadow
  messageObj.setHtmlContent(messageContent);
  messageObj.setSize(300,90);
  messageObj.setCssClassMessageBox(cssClass);
  messageObj.setSource(false);	          // no html source since we want to use a static message here.
  messageObj.setShadowDivVisible(false);  // Disable shadow for these boxes
  messageObj.display();
}
function closeMessage(){   
   messageObj.close();
}
function validateFormElements(){
  with(document){
    if (trimString(getElementById("emailbp").value) != ''
        && trimString(getElementById("fnamebp").value) != ''
        && trimString(getElementById("lnamebp").value) != ''
        && trimString(getElementById("add1bp").value) != ''
        && trimString(getElementById("townbp").value) != ''
        && trimString(getElementById("postcodebp").value) != ''
        && ((getElementById("studyLevel").value == 'Postgraduate'
        && getElementById("prosYearUniEntrybp").value != 'Please select')
        || (getElementById("studyLevel").value != 'Postgraduate'
        && getElementById("prosYearFustudbp").value != 'Please select')) ){
         return "TRUE";
    }else{
        return "FALSE";
    }
  }
}
function createSelectBox(selectBoxId, count){
  if($$(selectBoxId) != null){
    $$(selectBoxId).innerHTML = "";
    for(var i = 0; i <= count; i++){
      var option = document.createElement("option");
      option.value = ""+i;
      option.appendChild(document.createTextNode(i));
      $$(selectBoxId).appendChild(option);
    }
  }
}
function onchangeQualEnquiryForm(obj){
  var qualification = obj.value;
  var valueArray = {"Alevels":5, "SQAHighers":8, "SQAAdvancedHighers":8, "BTEC":3};  
  if(obj.value == null || obj.value == '' ){
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";        
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
  }else if(obj.value == '74'){
    var count = valueArray.Alevels;
    createSelectBox('A*select', count);  
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count);  
    createSelectBox('Eselect', count);
    $$("A*mainspan").style.display = "block";
    $$("Amainspan").style.display = "block";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "block";    
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
  }else if(obj.value == '75' || obj.value == '76'){
    var count = valueArray.SQAHighers;
      createSelectBox('Aselect', count);  
      createSelectBox('Bselect', count);  
      createSelectBox('Cselect', count);  
      createSelectBox('Dselect', count);
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "block";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "none";    
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
  }else if(obj.value == '555'){
    var count = valueArray.BTEC;    
      createSelectBox('D*select', count);  
      createSelectBox('BtecDselect', count);  
      createSelectBox('Mselect', count);  
      createSelectBox('Pselect', count);
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";    
    $$("D*mainspan").style.display = "block";
    $$("BtecDmainspan").style.display = "block";
    $$("Mmainspan").style.display = "block";
    $$("Pmainspan").style.display = "block";    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
  }else if(obj.value == '77'){  
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";    
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";    
    $$("ucaspoins").style.display = "block";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
  }else if(obj.value == '78'){  
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";    
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "block";
    $$("other").style.display = "none";
  }else{        
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "block";
  }
  checkChanges();
} 
function setGradesEntryPointsValue(){  
  var qual = $$('grade_210') != null ? $$('grade_210').value : "";
  var valueArray = {"Alevels":5, "SQAHighers":8, "SQAAdvancedHighers":8, "BTEC":3 };
  var mutisetVal = $$('multi_set') != null ? document.getElementById('multi_set').value : "";
    var astar = mutisetVal != null && mutisetVal.indexOf("A*") > -1 ? mutisetVal.substring(mutisetVal.indexOf("A*>")+3, mutisetVal.indexOf("A*>")+4) : "0";
    var avalue = mutisetVal != null && mutisetVal.indexOf("<A>") > -1 ? mutisetVal.substring(mutisetVal.indexOf("A>")+2, mutisetVal.indexOf("A>")+3) : "0";
    var bvalue = mutisetVal != null && mutisetVal.indexOf("B") > -1 ? mutisetVal.substring(mutisetVal.indexOf("B")+2, mutisetVal.indexOf("B")+3) : "0";
    var cvalue = mutisetVal != null && mutisetVal.indexOf("C") > -1 ? mutisetVal.substring(mutisetVal.indexOf("C")+2, mutisetVal.indexOf("C")+3) : "0";
    var dvalue = mutisetVal != null && mutisetVal.indexOf("D") > -1 ? mutisetVal.substring(mutisetVal.indexOf("D")+2, mutisetVal.indexOf("D")+3) : "0";
    var evalue = mutisetVal != null && mutisetVal.indexOf("E") > -1 ? mutisetVal.substring(mutisetVal.indexOf("E")+2, mutisetVal.indexOf("E")+3) : "0";
    
    var dstarvalue = mutisetVal != null && mutisetVal.indexOf("D*") > -1 ? mutisetVal.substring(mutisetVal.indexOf("D*>")+3, mutisetVal.indexOf("D*>")+4) : "0";
    var btecdvalue = mutisetVal != null && mutisetVal.indexOf("<D>") > -1 ? mutisetVal.substring(mutisetVal.indexOf("D>")+2, mutisetVal.indexOf("D>")+3) : "0";
    var mvalue = mutisetVal != null && mutisetVal.indexOf("M") > -1 ? mutisetVal.substring(mutisetVal.indexOf("M")+2, mutisetVal.indexOf("M")+3) : "0";
    var pvalue = mutisetVal != null && mutisetVal.indexOf("P") > -1 ? mutisetVal.substring(mutisetVal.indexOf("P")+2, mutisetVal.indexOf("P")+3) : "0";
    
    var freeText = mutisetVal != null && mutisetVal.indexOf("free-text") > -1 ? mutisetVal.substring(mutisetVal.indexOf("<free-text>")+11, mutisetVal.indexOf("</free-text>")) : "";
    if(qual == '77'){
      $$("ucaspoins").value = freeText;
    }else if(qual == '78'){
      $$("bacc").value = freeText;
    }else if(qual == '79'){
      $$("other").value = freeText;
    }    
 if(qual == '74'){
    var count = valueArray.Alevels;
    createSelectBox('A*select', count);  
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count);  
    createSelectBox('Eselect', count);
    $$("A*select").value = astar;
    $$("Aselect").value = avalue;
    $$("Bselect").value = bvalue;
    $$("Cselect").value = cvalue;
    $$("Dselect").value = dvalue;
    $$("Eselect").value = evalue;
    $$("astarab").style.display = "inline-block";
    $$("cde").style.display = "inline-block";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "block";
    $$("Amainspan").style.display = "block";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "block";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    $$("A*span").innerHTML = astar;
    $$("Aspan").innerHTML = avalue;
    $$("Bspan").innerHTML = bvalue;
    $$("Cspan").innerHTML = cvalue;
    $$("Dspan").innerHTML = dvalue;
    $$("Espan").innerHTML = evalue;
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
 }else if(qual == '75' || qual == '76'){
    var count = valueArray.SQAHighers;
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count); 
    $$("Aselect").value = avalue;
    $$("Bselect").value = bvalue;
    $$("Cselect").value = cvalue;
    $$("Dselect").value = dvalue;
    $$("astarab").style.display = "inline-block";
    $$("cde").style.display = "inline-block";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "block";
    //$$("Amainspan").className = "";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "none";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    $$("Aspan").innerHTML = avalue;
    $$("Bspan").innerHTML = bvalue;
    $$("Cspan").innerHTML = cvalue;
    $$("Dspan").innerHTML = dvalue;
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
  }else if(qual == '555'){
    var count = valueArray.BTEC;
    createSelectBox('D*select', count);  
    createSelectBox('BtecDselect', count);  
    createSelectBox('Mselect', count);  
    createSelectBox('Pselect', count); 
    $$("D*select").value = dstarvalue;
    $$("BtecDselect").value = btecdvalue;
    $$("Mselect").value = mvalue;
    $$("Pselect").value = pvalue;
    $$("astarab").style.display = "none";
    $$("cde").style.display = "none";
    $$("dstard").style.display = "inline-block";
    $$("mp").style.display = "inline-block";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    //$$("Amainspan").className = "";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";    
    $$("D*mainspan").style.display = "block";    
    $$("BtecDmainspan").style.display = "block";    
    $$("Mmainspan").style.display = "block";
    $$("Pmainspan").style.display = "block";
    $$("D*span").innerHTML = dstarvalue;
    $$("BtecDspan").innerHTML = btecdvalue;
    $$("Mspan").innerHTML = mvalue;
    $$("Pspan").innerHTML = pvalue;
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
  }else if(qual == '77' || qual == '78' || qual == '79'){
    
    $$("astarab").style.display = "none";
    $$("cde").style.display = "none";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";    
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    if(qual == '77'){
      $$("ucaspoins").style.display = "block";
    }else if(qual == '78'){
      $$("bacc").style.display = "block";
    }else if(qual == '79'){
      $$("other").style.display = "block";
    }
  }
}
function getEntryGradeMultiSet(){
  window.onbeforeunload = null;
  var qual = document.getElementById('grade_210').value;
  var astarvalue = document.getElementById('A*select').value;
  var avalue = document.getElementById('Aselect').value;
  var bvalue = document.getElementById('Bselect').value;
  var cvalue = document.getElementById('Cselect').value;
  var dvalue = document.getElementById('Dselect').value;
  var evalue = document.getElementById('Eselect').value;  
  var dstarvalue = document.getElementById('D*select').value;
  var btecdvalue = document.getElementById('BtecDselect').value;
  var mvalue = document.getElementById('Mselect').value;
  var pvalue = document.getElementById('Pselect').value;  
  var ucaspoin = document.getElementById('ucaspoins').value;
  var bacc = document.getElementById('bacc').value;
  var other = document.getElementById('other').value;
  var dontAllowGrades = "";
  if(document.getElementById('dontgrade')){
    if(document.getElementById('dontgrade').checked){
      dontAllowGrades = "Y"
    }else{
      dontAllowGrades = "N";
    }
  }
  var values = "";
  var finalValue = "";
  if(dontAllowGrades == 'Y'){
    document.getElementById('multi_set').value = finalValue;
    document.getElementById('grade_210').value = "";
  }else{
    if(qual == '74'){
      values += astarvalue!=null && astarvalue!= 'undefined' && astarvalue!= '0' ? "<A*>"+ astarvalue + '</A*>' : "";
      values += avalue!=null && avalue!= 'undefined' && avalue!= '0' ? "<A>"+ avalue + '</A>' : "";
      values += bvalue!=null && bvalue!= 'undefined' && bvalue!= '0' ? "<B>"+ bvalue + '</B>' : "";
      values += cvalue!=null && cvalue!= 'undefined' && cvalue!= '0' ? "<C>"+ cvalue + '</C>' : "";
      values += dvalue!=null && dvalue!= 'undefined' && dvalue!= '0' ? "<D>"+ dvalue + '</D>' : "";
      values += evalue!=null && evalue!= 'undefined' && evalue!= '0' ? "<E>"+ evalue + '</E>' : "";
      if((parseInt(astarvalue) + parseInt(avalue) + parseInt(bvalue) + parseInt(cvalue) + parseInt(dvalue) + parseInt(evalue)) > 6){
        //alert('Woah, steady on there! You can only enter a maximum of six grades (and if you really are enough of a genius to be doing more than six, just enter your top ones).');
        if($$('sendEmail')){
          $$('sendEmail').style.display='block';
        }
        createGradesSelectEnquiry('A-levels', 5);
        showLightBoxLoginForm('grade-range-popup',650,500, 'A-LEVEL');
        if($$('gradesErrMsg')){$$('gradesErrMsg').style.display="none";}
        return false;
      }      
    }else if(qual == '75' || qual == '76'){
      values += avalue!=null && avalue!= 'undefined' && avalue!= '0' ? "<A>"+ avalue + '</A>' : "";
      values += bvalue!=null && bvalue!= 'undefined' && bvalue!= '0' ? "<B>"+ bvalue + '</B>' : "";
      values += cvalue!=null && cvalue!= 'undefined' && cvalue!= '0' ? "<C>"+ cvalue + '</C>' : "";
      values += dvalue!=null && dvalue!= 'undefined' && dvalue!= '0' ? "<D>"+ dvalue + '</D>' : "";
      if((parseInt(avalue) + parseInt(bvalue) + parseInt(cvalue) + parseInt(dvalue)) > 10){
        //alert('Woah, steady on there! You can only enter a maximum of ten grades (and if you really are enough of a genius to be doing more than six, just enter your top ones).');
        if($$('sendEmail')){
          $$('sendEmail').style.display='block';
        }
        createGradesSelectEnquiry('SQA Highers', 8);
        showLightBoxLoginForm('grade-range-popup',650,500, 'SQA Highers');
        if($$('gradesErrMsg')){$$('gradesErrMsg').style.display="none";}
        return false;
      }      
    }if(qual == '555'){      
      values += dstarvalue!=null && dstarvalue!= 'undefined' && dstarvalue!= '0' ? "<D*>"+ dstarvalue + '</D*>' : "";
      values += btecdvalue!=null && btecdvalue!= 'undefined' && btecdvalue!= '0' ? "<D>"+ btecdvalue + '</D>' : "";
      values += mvalue!=null && mvalue!= 'undefined' && mvalue!= '0' ? "<M>"+ mvalue + '</M>' : "";
      values += pvalue!=null && pvalue!= 'undefined' && pvalue!= '0' ? "<P>"+ pvalue + '</P>' : "";      
      if((parseInt(dstarvalue) + parseInt(btecdvalue) + parseInt(mvalue) + parseInt(pvalue)) > 3){
        if($$('sendEmail')){
          $$('sendEmail').style.display='block';
        }
        createGradesSelectEnquiry('BTEC', 3);
        showLightBoxLoginForm('grade-range-popup',650,500, 'BTEC');
        if($$('gradesErrMsg')){$$('gradesErrMsg').style.display="none";}
        return false;
      }      
    }else if(qual == '77'){
      values += ucaspoin!=null && ucaspoin!= 'undefined' && ucaspoin!= "" && ucaspoin!= 'Enter tariff points'? "<free-text>"+ trimString(ucaspoin) +"</free-text>" : "";
      if(ucaspoin!=null && ucaspoin!= 'undefined' && ucaspoin!= "" && ucaspoin!= 'Enter tariff points'){
        if(isNaN(ucaspoin)){
            if($$('sendEmail')){
              $$('sendEmail').style.display='block';
            }        
            document.getElementById('ucaspoins').value = "";
            showLightBoxLoginForm('grade-range-popup',650,500, 'Please enter a number for your tariff points.');
            if($$('gradesErrMsg')){$$('gradesErrMsg').style.display="none";}
            return false;
        }
        if((parseInt(ucaspoin)) > 999){
          //alert('Woah, steady on there! You can only enter a maximum of 999 tariff points.');
          if($$('sendEmail')){
            $$('sendEmail').style.display='block';
          }
          document.getElementById('ucaspoins').value = "";
          showLightBoxLoginForm('grade-range-popup',650,500, 'UCAS TARRIF');
          if($$('gradesErrMsg')){$$('gradesErrMsg').style.display="none";}
          return false;
        }    
      }
    }else if(qual == '78'){
      values += bacc!=null && bacc!= 'undefined' && bacc!= "" && bacc!= 'Enter score' ? "<free-text>"+ trimString(bacc) +"</free-text>" : "";
      if(bacc!=null && bacc!= 'undefined' && bacc!= "" && bacc!= 'Enter score'){
        if(isNaN(parseInt(bacc))){
            if($$('sendEmail')){
              $$('sendEmail').style.display='block';
            }        
            document.getElementById('bacc').value = "";
            showLightBoxLoginForm('grade-range-popup',650,500, 'Please enter a number for your Baccalaureate points.');
            if($$('gradesErrMsg')){$$('gradesErrMsg').style.display="none";}
            return false;
        }
        if((parseInt(bacc)) > 99){
          //alert('Woah, steady on there! You can only enter a maximum of 999 tariff points.');
          if($$('sendEmail')){
            $$('sendEmail').style.display='block';
          }
          document.getElementById('bacc').value = ""; 
          showLightBoxLoginForm('grade-range-popup',650,500, 'BACC');
          if($$('gradesErrMsg')){$$('gradesErrMsg').style.display="none";}
          return false;
        }
      }
    }else if(qual == '79'){
      values += other!=null && other!= 'undefined' && other!= "" && other!= 'Please give details' ? "<free-text>"+ trimString(other) +"</free-text>" : "";
    }
    if(values != ""){
      finalValue = "<entry-grades>"+values+"</entry-grades>"
    }
    document.getElementById('multi_set').value = finalValue;
    }
    return true;
}
function clearDefault(obj, text){
   var id = obj.id;
  if($$(id).value == text){
    $$(id).value = "";
	obj.style.color = "#333";
  }

}

function setDefault(obj, text){
  var id = obj.id;
  if($$(id).value == ""){
    $$(id).value = text;
	obj.style.color = "#888";
  }
}
function setDOBValues(){
  /*var dateDOB = document.getElementById("dateDOB");
  var text = dateDOB.options[dateDOB.selectedIndex].text;
  document.getElementById("dateSpan").innerHTML = text;
  var monthDOB = document.getElementById("monthDOB");
  var text = monthDOB.options[monthDOB.selectedIndex].text;
  document.getElementById("monthSpan").innerHTML = text;
  var yearDOB = document.getElementById("yearDOB");
  var text = yearDOB.options[yearDOB.selectedIndex].text;
  document.getElementById("yearSpan").innerHTML = text;*/
  if($$("networkFlag").value!="3"){
    var grade_210 = $$("grade_210");
    var text = grade_210.options[grade_210.selectedIndex].text;
    if(text == 'Please select'){
      var gradesOptions= $$('grade_210').options; 
      var optionLabel = "";
      for (var j= 0; j< gradesOptions.length; j++) {  
        if (gradesOptions[j].value == "74") {
          gradesOptions[j].selected= true;
          optionLabel = gradesOptions[j].text;
          break;
        }
      }
      $$("gradeSelected").innerHTML = optionLabel;    
    }else{
      $$("gradeSelected").innerHTML = text;
    }  
  }
  if($$("countryOfResidence")){
    var countryOfResidence = $$("countryOfResidence");
    var countryText = countryOfResidence.options[countryOfResidence.selectedIndex].text;
    $$("countrySpan").innerHTML = countryText;  
    if(countryText == 'United Kingdom' || countryText == 'England' || countryText == 'Northern Ireland' || countryText == 'Scotland' || countryText == 'Wales'){
      $$('addFinder').style.display= 'block';
    }  
  }
}
function setGradesValueFromCookies(){
  var gradeValues=getCookieValues("grades_prepopulate");  
  if(gradeValues!=null){
    var arryGradeValues = gradeValues.split("##SPLIT##");
    if(arryGradeValues!=null){      
      $$('grade_210').value = arryGradeValues[0];
      var elGrades = $$("grade_210");      
      $$("gradeSelected").innerHTML = elGrades.options[elGrades.selectedIndex].text;      
      $$('multi_set').value = arryGradeValues[1];
    }
  }
}
function getCookieValues(c_name){
    var c_value = document.cookie;                
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1){
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1){
        c_value = null;
    }
    else{
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1){
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
    }
    return c_value;
}
function setMultiSetvalueEnquiryForm(){
  var qual = $$('grade_210') != null ? $$('grade_210').value : "";
  var valueArray = {"Alevels":5, "SQAHighers":8, "SQAAdvancedHighers":8, "BTEC":3};
  var mutisetVal = $$('multi_set') != null ? document.getElementById('multi_set').value : "";
    var astar = mutisetVal != null && mutisetVal.indexOf("A*") > -1 ? mutisetVal.substring(mutisetVal.indexOf("A*>")+3, mutisetVal.indexOf("A*>")+4) : "0";
    var avalue = mutisetVal != null && mutisetVal.indexOf("<A>") > -1 ? mutisetVal.substring(mutisetVal.indexOf("A>")+2, mutisetVal.indexOf("A>")+3) : "0";
    var bvalue = mutisetVal != null && mutisetVal.indexOf("B") > -1 ? mutisetVal.substring(mutisetVal.indexOf("B")+2, mutisetVal.indexOf("B")+3) : "0";
    var cvalue = mutisetVal != null && mutisetVal.indexOf("C") > -1 ? mutisetVal.substring(mutisetVal.indexOf("C")+2, mutisetVal.indexOf("C")+3) : "0";
    var dvalue = mutisetVal != null && mutisetVal.indexOf("D") > -1 ? mutisetVal.substring(mutisetVal.indexOf("D")+2, mutisetVal.indexOf("D")+3) : "0";
    var evalue = mutisetVal != null && mutisetVal.indexOf("E") > -1 ? mutisetVal.substring(mutisetVal.indexOf("E")+2, mutisetVal.indexOf("E")+3) : "0";    
    var dstarvalue = mutisetVal != null && mutisetVal.indexOf("D*") > -1 ? mutisetVal.substring(mutisetVal.indexOf("D*>")+3, mutisetVal.indexOf("D*>")+4) : "0";
    var btecdvalue = mutisetVal != null && mutisetVal.indexOf("<D>") > -1 ? mutisetVal.substring(mutisetVal.indexOf("D>")+2, mutisetVal.indexOf("D>")+3) : "0";
    var mvalue = mutisetVal != null && mutisetVal.indexOf("M") > -1 ? mutisetVal.substring(mutisetVal.indexOf("M")+2, mutisetVal.indexOf("M")+3) : "0";
    var pvalue = mutisetVal != null && mutisetVal.indexOf("P") > -1 ? mutisetVal.substring(mutisetVal.indexOf("P")+2, mutisetVal.indexOf("P")+3) : "0";    
    var freeText = mutisetVal != null && mutisetVal.indexOf("free-text") > -1 ? mutisetVal.substring(mutisetVal.indexOf("<free-text>")+11, mutisetVal.indexOf("</free-text>")) : "";
    if(qual == '77'){
      $$("ucaspoins").value = freeText;
    }else if(qual == '78'){
      $$("bacc").value = freeText;
    }else if(qual == '79'){
      $$("other").value = freeText;
    }
 if(qual == '74'){
    var count = valueArray.Alevels;
    createSelectBox('A*select', count);  
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count);  
    createSelectBox('Eselect', count);  
    $$("A*mainspan").style.display = "block";
    $$("Amainspan").style.display = "block";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "block";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";    
    $$("A*select").value = astar;
    $$("Aselect").value = avalue;
    $$("Bselect").value = bvalue;
    $$("Cselect").value = cvalue;
    $$("Dselect").value = dvalue;
    $$("Eselect").value = evalue;    
    $$("A*span").innerHTML = astar;
    $$("Aspan").innerHTML = avalue;
    $$("Bspan").innerHTML = bvalue;
    $$("Cspan").innerHTML = cvalue;
    $$("Dspan").innerHTML = dvalue;
    $$("Espan").innerHTML = evalue;    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
 }else if(qual == '75' || qual == '76'){
    var count = valueArray.SQAHighers;
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count); 
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "block";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "none";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    $$("Aselect").value = avalue;
    $$("Bselect").value = bvalue;
    $$("Cselect").value = cvalue;
    $$("Dselect").value = dvalue;    
    $$("Aspan").innerHTML = avalue;
    $$("Bspan").innerHTML = bvalue;
    $$("Cspan").innerHTML = cvalue;
    $$("Dspan").innerHTML = dvalue;
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
  }else if(qual == '555'){
    var count = valueArray.BTEC;
    createSelectBox('D*select', count);  
    createSelectBox('BtecDselect', count);  
    createSelectBox('Mselect', count);  
    createSelectBox('Pselect', count); 
    $$("D*select").value = dstarvalue;
    $$("BtecDselect").value = btecdvalue;
    $$("Mselect").value = mvalue;
    $$("Pselect").value = pvalue;
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";    
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";    
    $$("D*mainspan").style.display = "block";    
    $$("BtecDmainspan").style.display = "block";    
    $$("Mmainspan").style.display = "block";
    $$("Pmainspan").style.display = "block";
    $$("D*span").innerHTML = dstarvalue;
    $$("BtecDspan").innerHTML = btecdvalue;
    $$("Mspan").innerHTML = mvalue;
    $$("Pspan").innerHTML = pvalue;    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
  }else if(qual == '77' || qual == '78' || qual == '79'){
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    if(qual == '77'){
      $$("ucaspoins").style.display = "block";
    }else if(qual == '78'){
      $$("bacc").style.display = "block";
    }else if(qual == '79'){
      $$("other").style.display = "block";
    }
  }
}
function gradescharacterLimit(obj, limit){
  var textValue = obj.value;
  var textLength = textValue.length;
  if(textLength > limit){
    alert("You can enter only "+limit+" characters");
    obj.value = "";
    obj.focus();
  }
  return false;
}
function showReadmoreSummary(){//Side pod
  var expandDescText = document.getElementById('courseSummary');
  var collabsDescText = document.getElementById('subTextSummary');
  if(expandDescText != null && expandDescText.style.display == 'none'){
    expandDescText.style.display='block';
    collabsDescText.style.display='none';
  }else if(collabsDescText != null && collabsDescText.style.display == 'none') {
    expandDescText.style.display='none';
    collabsDescText.style.display='block';
  }  
}
function addRemoveFromInteraction(assocId, assocType, subOrderItemId, enqType){
  var dowhat = "";
  thisvalue = $$("addpop_"+subOrderItemId);
  divValue =  $$("divpopup_"+subOrderItemId);
  var prosCount = parseInt($$('interaction_pros_count').innerHTML);
  if(thisvalue.className == "fa fa-plus fa-1_5x fr"){
    dowhat = "add";
    if($$('consentflagdiv_'+assocId)){
      $$('removedUni').value = null;
      if(prosCount == 0){
        $$('consentPodDiv').style.display = 'block';
      }
      $$('consentflagdiv_'+assocId).style.display = 'block';
      
    }
  }else if(thisvalue.className == "fa fa-remove fa-1_5x fr"){
    dowhat = "remove";
    if($$('consentflagdiv_'+assocId)){
      $$('removedUni').value = assocId;
      $$('consentflagdiv_'+assocId).style.display = 'none'; 
      if(prosCount == 1){
    	  $$('consentPodDiv').style.display = 'none';
      }
    }
  } 
  var url='/degrees/addprosbasket.html?assocId='+assocId+'&dowhat='+dowhat+"&assocType="+assocType+"&subOrderItemId="+subOrderItemId+'&enqType='+enqType+'&fromPopup=fromPopup&frominteraction=frominteraction';
  var ajax=new sack()
  ajax.requestFile=url
  ajax.onCompletion=function(){prosResponseDataForInteraction(ajax);}
  ajax.runAJAX()
}
function prosResponseDataForInteraction(ajax){
  $$('prospectusPopDiv').style.display = 'block';
  var response = ajax.response; 
  var reponseArr = response.split("##SPLIT##");
  $$('prospectusPopDiv').innerHTML = reponseArr[0];
}
function createGradesSelectEnquiry(gradeType, count){
  if(gradeType == "A-levels"){
    createSelectBox('A*select', count);  
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count);  
    createSelectBox('Eselect', count);  
    if($$("A*span")){$$("A*span").innerHTML = '0';};
    if($$("Aspan")){$$("Aspan").innerHTML = '0';};
    if($$("Bspan")){$$("Bspan").innerHTML = '0';};
    if($$("Cspan")){$$("Cspan").innerHTML = '0';};
    if($$("Dspan")){$$("Dspan").innerHTML = '0';};
    if($$("Espan")){$$("Espan").innerHTML = '0';};
  }else if(gradeType == "SQA Highers" || gradeType == "SQA Advanced Highers"){
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count);  
    if($$("Aspan")){$$("Aspan").innerHTML = '0';};
    if($$("Bspan")){$$("Bspan").innerHTML = '0';};
    if($$("Cspan")){$$("Cspan").innerHTML = '0';};
    if($$("Dspan")){$$("Dspan").innerHTML = '0';};
  }else if(gradeType == "BTEC"){
    createSelectBox('D*select', count);  
    createSelectBox('BtecDselect', count);  
    createSelectBox('Mselect', count);  
    createSelectBox('Pselect', count);  
    if($$("D*span")){$$("D*span").innerHTML = '0';};
    if($$("BtecDspan")){$$("BtecDspan").innerHTML = '0';};
    if($$("Mspan")){$$("Mspan").innerHTML = '0';};
    if($$("Pspan")){$$("Pspan").innerHTML = '0';};
  }
}
function displayViewMoreBlock(){
  if ($$('viewMoreBlockProspectus')){
    if($$('viewMoreBlockProspectus').style.display == 'none'){
      $$('viewMoreBlockProspectus').style.display = 'block';
      $$('viewAllProsepectusOrdered').style.display = 'none';
    }
  }
}
function bklProsInsightLogging(providerId,studyLevelDesc,StudyLevel,userId){  
  ga('set', 'dimension1', 'prospectus submitted');
  ga('set', 'dimension2', 'whatuni');    
  ga("set", "dimension3", providerId);
  ga("set", "dimension4", studyLevelDesc);
  ga("set", "dimension5", StudyLevel);
  //ga("set", "dimension15", userId);  
  ga("send", "pageview");
}
function disableProsUKresident() {
  with(document) {
      if ($$("nonukresident").checked == false){
          $$("postcode").disabled = true;
          $$("postcode").className = "ql-inpt disb";
          $$("countryOfResfieldset").style.display = 'block';
      }else{
          $$("postcode").disabled = false;
          $$("postcode").className = "ql-inpt";
          $$("countryOfResfieldset").style.display = 'none';
      }
 }
} 
function checkExistEmail(id){  
  var email = $$(id).value;
  if(email == ''){
    setErrorAndSuccessMsg('divEmail','w100p adlog qlerr','errorEmail', 'Something seems to be missing from your email. Awkward.');
    $$D("errorEmail").className = "qler1";
    return false;
  }else{
    var url= "/degrees/commonaction.html";    
    var ajax=new sack();
    ajax.requestFile = url;	
    
    ajax.setVar("emailAddress", trimString(email));
    ajax.setVar("actionType", "checkEmail");
    
    ajax.onCompletion = function(){ showResponseFun(ajax, email); };	
    ajax.runAJAX();
  }
}
function showResponseFun(ajax, textVal){
  var response = ajax.response;
  if(response.trim() == "EXIST"){    
    text =  "The email address " + textVal + " is already registered with us, please login. This is to protect your personal data";    
    setErrorAndSuccessMsg('divEmail','w100p adlog qlerr','errorEmail', text);
    $$D('errorEmail').className="qler1";
    if($$('emailAlreadyExistsflag')){$$('emailAlreadyExistsflag').value="yes"; }
    return false;
  }else{
  if( textVal.includes("@") && textVal.split('@')[1].length > 0){
   if(!emailCheck(trimString(textVal))){      
      setErrorAndSuccessMsg('divEmail','w100p adlog qlerr','errorEmail', 'Something seems to be missing from your email. Awkward.'); 
      $$D("errorEmail").className = "qler1";
      return false;
    }else if(!isEmpty(textVal)){     
     setErrorAndSuccessMsg('divEmail','w100p adlog sumsg','errorEmail', "You've just got to be on email");
     $$D('errorEmail').className="sutxt";        
    }
   if($$('emailAlreadyExistsflag')){$$('emailAlreadyExistsflag').value="no"; }   
  }
  }
}
function showhideAddressFields(){
  if($$("countryOfResidence")){
    if($$("countryOfResidence").value !="" &&  $$("countryOfResidence").value !=null ){
       blockNone("addressFields","block");
       var countryOfResidence = document.getElementById("countryOfResidence");
       var countryText = countryOfResidence.options[countryOfResidence.selectedIndex].text;
        $$("countrySpan").innerHTML = countryText;
        if(countryText == 'United Kingdom' || countryText == 'England' || countryText == 'Northern Ireland' || countryText == 'Scotland' || countryText == 'Wales'){
          $$('addFinder').style.display= 'block';
        }  
    }
  }
}
//Script for Sign in concertina added by Prabha on 15-DEC-2015
 dev(document).ready(function () {
  dev('.lgnFld').css('display','none');
  dev('.res_faq .lgnFld').css('display','block');
  dev('.sgntx').click(function(){
     dev('.lgnFld').slideToggle('slow');
     dev('.sgntx i').toggleClass('fa-minus-circle');
   });
   dev('#signInLink').click(function(){
     dev('#loginFieldPod').slideToggle('slow');
   });
   dev("#marketingEmailFlag").click(function(){
      if(dev("#marketingEmailFlag").is(':checked')) {
        dev("input[id='marketingEmailFlag']").val("Y");
      } else { dev("input[id='marketingEmailFlag']").val("N"); }                  
   });
   
   dev("#solusEmailFlag").click(function(){
      if(dev("#solusEmailFlag").is(':checked')) {
        dev("input[id='solusEmailFlag']").val("Y");
      } else { dev("input[id='solusEmailFlag']").val("N"); }                  
   });
   
   dev("#surveyEmailFlag").click(function(){
      if(dev("#surveyEmailFlag").is(':checked')) {
        dev("input[id='surveyEmailFlag']").val("Y");
      } else { dev("input[id='surveyEmailFlag']").val("N"); }                  
   });
 });
 function setEventListeners(){
  var eventFlag;
  if($$D("isMobileUA").value=="No"){
    eventFlag="mouseover";
  }else{
    eventFlag="click";
  }
  $$D("yyoe1").addEventListener(eventFlag, function( event ) {
     blockNone('yyoe1Text', 'block');
  }, false);
  $$D("yyoe2").addEventListener(eventFlag, function( event ) {
     blockNone('yyoe2Text', 'block');
  }, false);
  $$D("yyoe3").addEventListener(eventFlag, function( event ) {
     blockNone('yyoe3Text', 'block');
  }, false);
  $$D("yyoe4").addEventListener(eventFlag, function( event ) {
     blockNone('yyoe4Text', 'block');
  }, false);
}
function showProspectusPassword(id){
  if(dev("#"+id).attr('type') == "password") {
    dev("#"+id).prop("type", "text");
    $$D("eyeId").className = "fa fa-eye-slash";
  } else {
    dev("#"+id).prop("type", "password");
    $$D("eyeId").className = "fa fa-eye";
  }
}
function hideprospectuspassword(id){
  dev("#"+id).prop("type", "password");
  $$D("eyeId").className = "fa fa-eye";
}

//Added by Hema.S on 20-feb-2018 for UCAS new Fees Structure changes
//var dev = jQuery.noConflict();
//dev(document).ready(function(){
// dev(".scrltab a:first-child").addClass("tabact");
//  var text = dev('.scrltab a.tabact').text();
//  var act_tab_sel_1 = dev('.scrltab a.tabact').attr('href');
//  dev(act_tab_sel_1).addClass('active');
//  dev( ".scrltab a" ).each(function() {
//    dev( ".scrltab a" ).click(function(event) {
//      event.preventDefault();
//      var act_tab_sel = dev('.scrltab a.tabact').attr('href');
//      dev(act_tab_sel).removeClass('active');
//      dev(act_tab_sel).addClass('hide');  
//      var tar_tab_sel = dev(this).attr('href');
//      dev(tar_tab_sel).removeClass('hide');
//      dev(tar_tab_sel).addClass('active');           
//      dev( this ).siblings().removeClass("tabact");
//      //dev( this ).toggleClass("tabact");
//      dev( this ).addClass("tabact");
//      text = dev('.scrltab a.tabact').text();
//      //dev("#mbleview").text(text+'<i class="fa fa-angle-down fa-lg"></i>');
//      document.getElementById("mbleview").innerHTML = text +'<i class="fa fa-angle-down fa-lg"></i>';
//      if(dev(document).width() < 768 ) {
//        dev('.scrltab').hide();
//      }
//      dev(".feerw_rt .fstu_lnk a").click(function(e)
//     {
//      e.preventDefault();
//      });
//    });
//    document.getElementById("mbleview").innerHTML = text +'<i class="fa fa-angle-down fa-lg"></i>';
//  });
//  dev(".fee_nav .fnav_wrp").click(function(event) {
//    dev(".scrltab").slideToggle(500);
//  });
//});

function prospectusShowhideAddressFields(){
  if($$("countryOfResidenceSel")){
    if($$("countryOfResidenceSel").value !="" &&  $$("countryOfResidenceSel").value !=null ){
      blockNone("addressFields","block");
	  var countryOfResidenceSel = document.getElementById("countryOfResidenceSel");
	  var countryText = countryOfResidenceSel.options[countryOfResidenceSel.selectedIndex].text;
	  $$("countryOfResidence").innerHTML = countryText;
	  if(countryText == 'United Kingdom' || countryText == 'England' || countryText == 'Northern Ireland' || countryText == 'Scotland' || countryText == 'Wales'){
	     $$('addFinder').style.display= 'block';
	  }  
    }
  }
}

eq(window).on('load', function(){
  if($$("countryOfResidenceSel")){
    prospectusShowhideAddressFields();	
  }
});