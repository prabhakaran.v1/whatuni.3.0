var jq = jQuery.noConflict();
function setSlider(){
	jq(".slider").each( function(){
		
		var devslider = jq(this),
				devitemscontainer = devslider.find(".slider-items-container");
		
		if (devitemscontainer.find(".slider-item.active").length == 0){
			devitemscontainer.find(".slider-item").first().addClass("active");
		}
		
		function setWidth(){
			var totalWidth = 0
			
			jq(devitemscontainer).find(".slider-item").each( function(){
				totalWidth += jq(this).outerWidth();
			});
			
			devitemscontainer.width(totalWidth);
			
		}
		function setTransform(){
			
			var devactiveItem = devitemscontainer.find(".slider-item.active"),
					activeItemOffset = devactiveItem.offset().left,
					itemsContainerOffset = devitemscontainer.offset().left,
					totalOffset = activeItemOffset - itemsContainerOffset
			
			devitemscontainer.css({"transform": "translate( -"+totalOffset+"px, 0px)"})
			
		}
		function nextSlide(){
			var activeItem = devitemscontainer.find(".slider-item.active"),
					activeItemIndex = activeItem.index(),
					sliderItemTotal = devitemscontainer.find(".slider-item").length,
					nextSlide = 0;
			
			if (activeItemIndex + 1 > sliderItemTotal - 1){
				nextSlide = 0;
			}else{
				nextSlide = activeItemIndex + 1
			}
			
			var nextSlideSelect = devitemscontainer.find(".slider-item").eq(nextSlide),
					itemContainerOffset = devitemscontainer.offset().left,
					totalOffset = nextSlideSelect.offset().left - itemContainerOffset
			
			devitemscontainer.find(".slider-item.active").removeClass("active");
			nextSlideSelect.addClass("active");
			devslider.find(".dots").find(".dot").removeClass("active")
			devslider.find(".dots").find(".dot").eq(nextSlide).addClass("active");
			devitemscontainer.css({"transform": "translate( -"+totalOffset+"px, 0px)"})
			
		}
		function prevSlide(){
			var activeItem = devitemscontainer.find(".slider-item.active"),
					activeItemIndex = activeItem.index(),
					sliderItemTotal = devitemscontainer.find(".slider-item").length,
					nextSlide = 0;
			
			if (activeItemIndex - 1 < 0){
				nextSlide = sliderItemTotal - 1;
			}else{
				nextSlide = activeItemIndex - 1;
			}
			
			var nextSlideSelect = devitemscontainer.find(".slider-item").eq(nextSlide),
					itemContainerOffset = devitemscontainer.offset().left,
					totalOffset = nextSlideSelect.offset().left - itemContainerOffset
			
			devitemscontainer.find(".slider-item.active").removeClass("active");
			nextSlideSelect.addClass("active");
			devslider.find(".dots").find(".dot").removeClass("active")
			devslider.find(".dots").find(".dot").eq(nextSlide).addClass("active");
			devitemscontainer.css({"transform": "translate( -"+totalOffset+"px, 0px)"})
			
		}
		function makeDots(){
			var activeItem = devitemscontainer.find(".slider-item.active"),
					activeItemIndex = activeItem.index(),
					sliderItemTotal = devitemscontainer.find(".slider-item").length;
			
			for (i = 0; i < sliderItemTotal; i++){
				devslider.find(".dots").append("<div class='dot'></div>")
			}
			
			devslider.find(".dots").find(".dot").eq(activeItemIndex).addClass("active")
			
		}
		
		setWidth();
		setTransform();
		makeDots();
		
		jq(window).resize( function(){
					setWidth();
					setTransform();
		});
		
		var nextBtn = devslider.find(".controls").find(".next-btn"),
				prevBtn = devslider.find(".controls").find(".prev-btn");
		
		nextBtn.on('click', function(e){
			e.preventDefault();
			nextSlide();
		});
		
		prevBtn.on('click', function(e){
			e.preventDefault();
			prevSlide();
		});
		
		devslider.find(".dots").find(".dot").on('click', function(e){
			
			var dotIndex = jq(this).index(),
					totalOffset = devitemscontainer.find(".slider-item").eq(dotIndex).offset().left - devitemscontainer.offset().left;
					
			devitemscontainer.find(".slider-item.active").removeClass("active");
			devitemscontainer.find(".slider-item").eq(dotIndex).addClass("active");
			devslider.find(".dots").find(".dot").removeClass("active");
			jq(this).addClass("active")
			
			devitemscontainer.css({"transform": "translate( -"+totalOffset+"px, 0px)"})
			
		});
		
	});
	
}

setSlider();