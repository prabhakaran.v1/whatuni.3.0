function $$D(id){return document.getElementById(id);}
var contextPath = "/degrees";
var $jq = jQuery.noConflict();
function showDropDown(id){
	$jq('#'+id).show();
}
function hideDropDown(){
	//$jq('#drp_location').hide();
}
function subjectAjax(subjectCode,regionId,sortCode,pageNo){
	var sortCode = sortCode != "" ? sortCode : $jq('#selectedSortCode').val();
	var subject = subjectCode != "" ? subjectCode : $jq('#selectedSubjectCode').val();
	var region = regionId != "" ? regionId : $jq('#selectedRegionId').val();
	var action = "DROPDOWN_AJAX";
	  var param = "?action="+action+"&subject="+subject+"&region="+region+"&sortCode="+sortCode+"&pageno="+pageNo;
	  var url = contextPath + "/get-multiple-type-cpc-landing-page.html" + param;
	  $jq.ajax({
	    url: url, 
	    type: "GET",
	    async: false,
	    success: function(response){},
	    complete: function(response){
	      $jq("#middleID").html(response.responseText);
	      var subjectNameText = $jq('#selectedSubjectName').val() != "" ? $jq('#selectedSubjectName').val() : "Filter by subject";
	      var locationNameText = $jq('#selectedRegionName').val() != "" ? $jq('#selectedRegionName').val() : "Filter by region";
	      var sortName = $jq('#sortName').val();
	      $jq('#subjectName').val(subjectNameText);
	      $jq('#locationName').val(locationNameText);
	      $jq('#sortTextId').text(sortName);
	      $jq('#drp_location').hide();
	      $jq('#drp_subject').hide();
	      $jq('#drp_sortId').hide();
	    }
	  });  
	  }



