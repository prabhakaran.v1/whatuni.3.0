var cpcHead = jQuery.noConflict();

cpcHead(document).ready(function(){
  cpcHead("#click_view1").click(function()
  {
    cpcHead(this).next().slideToggle("slow");
	cpcHead("#click_view1_span").toggleClass("minus");
	cpcHead("#click_view2").next().hide();
	cpcHead("#click_view2_span").removeClass("minus");
  });
  cpcHead("#click_view2").click(function()
  {
	cpcHead(this).next().slideToggle("slow");
	cpcHead("#click_view2_span").toggleClass("minus");
	cpcHead("#click_view1").next().hide();
	cpcHead("#click_view1_span").removeClass("minus");
  });
  cpcHead("#navbar").click(function() {
  cpcHead("#hdr_menu").slideToggle();
	cpcHead(".navbar").toggleClass("navact");
	cpcHead("#navbar").hasClass("navact") ? cpcHead("html").addClass("rvscrl_hid") : cpcHead("html").removeClass("rvscrl_hid");
	cpcHead("#shadowId").hasClass("shadow") ? cpcHead("#shadowId").removeClass("shadow") : cpcHead("#shadowId").addClass("shadow");
	cpcHead("#navbar").hasClass("navact") ? cpcHead("#fa_bars").attr("style", "display:none") : cpcHead("#fa_bars").attr("style", "display:block");	
  });	  
});	
