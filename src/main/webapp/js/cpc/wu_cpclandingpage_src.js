var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";
var contextPath = "/degrees";
var jq = jQuery.noConflict();
var vidpct = ['','10%','20%','30%','40%','50%','60%','70%','80%','90%','100%'];
var videoRanMap = {};
function videoPlayedDurationPercentage(videoElement) {
	
	if(videoRanMap[videoElement.id+'_percentage'] == null) {
		videoRanMap[videoElement.id+'_percentage'] = "0%";
	}
	if(videoRanMap[videoElement.id+'_imp5sec'] == null) {
		videoRanMap[videoElement.id+'_imp5sec'] = "N";
	}	
	
	videoElement.ontimeupdate = function(){ 
		var curVidDrtn = videoElement.duration;
		var tenPcntSec = curVidDrtn/10;
		//
		if(videoRanMap[videoElement.id+'_imp5sec'] != "Y") {
			videoRanMap[videoElement.id+'_imp5sec'] = "Y";			
			GANewAnalyticsEventsLogging('Video Impression View', jq('#gaCollegeName').val(), 'cpc landing page lightbox');
		} 
		
		for(var cnt=1; cnt<=10; cnt++){
			if(Math.round(this.currentTime) >= Math.round(tenPcntSec*cnt)) {
				if(videoRanMap[videoElement.id+'_percentage'].indexOf(vidpct[cnt]) < 0) {
					videoRanMap[videoElement.id+'_percentage'] += ','+vidpct[cnt];
					//console.log('video10percentage  > Video Duration > '+$$D('collegeNameGa').value+'|'+videoTitle+' > '+vidpct[cnt]);
					GANewAnalyticsEventsLogging('Video Duration', jq('#gaCollegeName').val()+'|cpc landing page lightbox', vidpct[cnt]);
				}
			}
		}		
	};	
}
//
function getDeviceWidth() { return (window.innerWidth > 0) ? window.innerWidth : screen.width; }
function isNotNullAnddUndef(variable) {return (variable !== null && variable !== undefined && variable !== '');}
function $$D(id){
  return document.getElementById(id);
}
function getDeviceWidth() { return (window.innerWidth > 0) ? window.innerWidth : screen.width; }
function isBlankOrNullString(str) {
  var flag = true;
  if(str.trim() != "" && str.trim().length > 0) {
    flag = false;
  }
  return flag;
}
function showDropDown(id){
  if(jq('#'+id).is(':visible')){
	jq('#'+id).hide();
  }else{
	jq('#'+id).show();	
  }  
}
function hideDropDown(){
	//jq('#drp_location').hide();
}
function subjectAjax(subjectCode,regionId,sortCode,pageNo,cpcType){
	jq('#loaderImg').show();
	jq('#middleID').hide();
	var sortCode = sortCode != "" ? sortCode : jq('#selectedSortCode').val();
	var browseCatId = subjectCode != "" ? subjectCode : jq('#selectedSubjectCode').val();
	var region = regionId != "" ? regionId : jq('#selectedRegionId').val();
	var campaign = jq('#campaign').val();
	var pageType = jq('#pageType').val();
	var preview = jq('#preview').val();
	 	sortCode = isNotNullAnddUndef(sortCode) ? sortCode : "";
	 	browseCatId = isNotNullAnddUndef(browseCatId) ? browseCatId : "";
	 	region = isNotNullAnddUndef(region) ? region : "";
	 	preview = isNotNullAnddUndef(preview) ? preview : "N";
    if(browseCatId == '0'){
    	browseCatId = '';	
    }
    if(region == '0'){
    	region = '';
    }
	var resCnt = '';
	var cpcTypeUrl = "/get-single-type-cpc-landing-page.html";
	if(pageType == "MULTIPLE"){
		cpcTypeUrl = "/get-multiple-type-cpc-landing-page.html";	
	}
	if(pageType == "SINGLE"){
		resCnt = jq('#courseCountId').val();
	}
	var action = "DROPDOWN_AJAX";
	var param = "?campaign="+campaign+"&action="+action+"&browseCatId="+browseCatId+"&region="+region+"&sortCode="+sortCode+"&pageno="+pageNo+"&preview="+preview;
	  var url = contextPath + cpcTypeUrl + param;
	  jq.ajax({
	    url: url, 
	    type: "GET",
	    async: false,
	    success: function(response){},
	    complete: function(response){
	     jq("#middleID").html(response.responseText);
	     jq('#loaderImg').hide();
	 	jq('#middleID').show();
	      var subjectNameText =jq('#selectedSubjectName').val() != "" ?jq('#selectedSubjectName').val() : "Filter by subject";
	      var locationNameText =jq('#selectedRegionName').val() != "" ?jq('#selectedRegionName').val() : "Filter by region";
	      var sortName =jq('#sortName').val() != "" ? jq('#sortName').val() : "Most info";
	     jq('#subjectName').val(subjectNameText);
	     jq('#locationName').val(locationNameText);
	     jq('#sortTextId').text(sortName);	     
	     jq('#drp_location').hide();
	     jq('#drp_subject').hide();
	     jq('#drp_sortId').hide();
	     jq('#loaderImg').hide();
	     if(browseCatId != '' && browseCatId != undefined && subjectNameText!= 'Filter by subject'){
	 		GANewAnalyticsEventsLogging('CPC Landing Page','Subject Filter',subjectNameText);
	 	}
	 	if(region != '' && region != undefined && locationNameText!= 'Filter by region'){
	 		GANewAnalyticsEventsLogging('CPC Landing Page','Region Filter',locationNameText);	
	 	}
	 	if(sortCode != '' && sortCode != undefined){
	 		GANewAnalyticsEventsLogging('CPC Landing Page','Sort By', sortName);		
	 	}
	     if(pageType == "SINGLE"){
	 		resCnt = jq('#courseCountId').val();
	 		(isNotNullAnddUndef(resCnt) && resCnt > 1) ? jq('#sortId').show() : jq('#sortId').hide();	
	 		resCnt = (resCnt == '1') ? resCnt+' course' : resCnt+' courses';
	 		jq('#crsCntTxtId').text(resCnt)
	 	 }
	     if(pageType == "MULTIPLE"){	
	    	resCnt = jq('#uniCountId').val();
	    	(isNotNullAnddUndef(resCnt) && resCnt > 1) ? jq('#sortId').show() : jq('#sortId').hide();	
	    	resCnt = (resCnt == '1') ? resCnt+' university' : resCnt+' universities';
		 	jq('#uniCntTxtId').text(resCnt)	    	
	     }
	     var scrollTopPos = jq('#middleSectionId').offset().top;
         jq('body, html').animate({scrollTop: scrollTopPos},"slow");
	    }
	  });  
	  retina();
	  }
function reviewLightBox() {
  jq(document).ready(function() {
  adjustStyle();
  jq(".rvbx_shdw,.rev_lbox .revcls a").click(function(event){
  event.preventDefault();
  jq(".rev_lbox").removeAttr("style");
  jq(".rev_lbox").addClass("fadeOut").removeClass("fadeIn");
  jq("html").removeClass("rvscrl_hid");
  });
  });
}

function ajaxCpcLightBox(collegeId,landingPage) {
  jq('#loaderImg').show();
  var campaign=jq("#campaign").val();
  var subject =jq('#selectedSubjectCode').val();
  var location =jq('#selectedRegionId').val();
  var preview =jq('#preview').val();
  var clearingFlag =jq('#clearingFlag').val();
  subject = isNotNullAnddUndef(subject) ? subject : "";
  location = isNotNullAnddUndef(location) ? location : "";
  preview = isNotNullAnddUndef(preview) ? preview : "";
  var url = contextPath + "/get-cpc-lightbox.html?"+ "collegeId=" + collegeId + "&landingId=" + campaign + "&subject=" + subject + "&location=" + location + "&landingPage=" + landingPage + "&preview=" + preview;
  url += (isNotNullAnddUndef(clearingFlag)) ? "&clearingFlag=" + clearingFlag : "";
  eventTimeTracking();
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    type: "POST",
    async: false,
    success: function(response){
    var resId = 'revLightBox';
    jq("#"+resId).html(response);
    jq('#loaderImg').hide();
    cpclightBoxCall(clearingFlag);
    //setTimeout(function(){cpcrevLightbox();}, 2000);
    retina();
    playLightBoxVideo('0');
     }
   });
}

function cpclightBoxCall(clearingFlag){
  var clearingClass = (isNotNullAnddUndef(clearingFlag) && 'Y' == clearingFlag) ? " clr_cpc" : "";
  jq("#revLightBox").addClass("rev_lbox fadeIn cpc_ltbox"+ clearingClass);
  jq("#revLightBox").css({'display':'block','opacity' : '1','z-index':'3','visibility':'visible'});
  jq("html").addClass("rvscrl_hid");
}
jq(window).resize(function() {
	//cpcrevLightbox();
});
jq(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
	//cpcrevLightbox();
}

function cpcrevLightbox(){
  var parentDivHeight = jq("#parentDivHeight").height();				
  var div1Height = jq("#div1Height").height();				
  var div2Height = parentDivHeight - div1Height;
  jq("#div2Height").height(div2Height);
}
function closeLigBox(){
  jq(".rev_lbox").removeAttr("style");
  jq(".rev_lbox").addClass("fadeOut").removeClass("fadeIn");
  jq("html").removeClass("rvscrl_hid");  
}
function clickPlayAndPause(videoId,e,thumbImgId,plyIcnId) {
  var thumbNailImg = 'thumbnailImg';
  var cpcPlayIcon = 'cpc_play_icon';
  if(thumbImgId != '' && thumbImgId != undefined && thumbImgId != null && thumbImgId != 'null'){
	thumbNailImg = thumbImgId; 
  }
  if(plyIcnId != '' && plyIcnId != undefined && plyIcnId != null && plyIcnId != 'null'){
	cpcPlayIcon = plyIcnId; 
  }
  var videoIdElement = jq('#'+ videoId).get(0);
  var vidId = $jq('#'+ videoId);
  if(videoIdElement.paused){
    jq("#vide_icn_"+videoId).hide();
    vidId.show();
    e.preventDefault();
	videoIdElement.play();
	vidId.attr('controls', '');
	 jq('#'+thumbNailImg).hide();
	  jq('#'+cpcPlayIcon).hide();
  }	else {
	vidId.hide();
	jq("#vide_icn_"+videoId).show();
	e.preventDefault();
	videoIdElement.pause(); 
	jq('#'+thumbNailImg).show();
	jq('#'+cpcPlayIcon).show();
  }
}
jq( window ).on( "load", function() {

});
jq(document).ready(function(){
	var pageType = jq('#pageType').val();
	var cpcPageType = '';
	var gaCollegeName = jq('#gaCollegeName').val();
	if(pageType == "MULTIPLE"){
		cpcPageType = 'Multi CPC Landing';	
	}else if(pageType == "SINGLE"){
		cpcPageType = 'Single CPC Landing';
	}
	if(jq('#drp_subject ul li').length > 0){
	  var fltFstSub = jq("#drp_subject ul li:first").text().trim();	
	  if(fltFstSub != '' && fltFstSub != undefined && gaCollegeName != '' && pageType == "SINGLE"){
		  GANewAnalyticsEventsLogging(cpcPageType,fltFstSub, gaCollegeName);		  
	  }
	  if(jq('#drp_location ul li').length > 0){	  	
		var fltFstLoc = jq("#drp_location ul li:first").text().trim();	
		if(fltFstSub != '' && fltFstSub != undefined && fltFstLoc != '' && fltFstLoc != undefined && pageType == "MULTIPLE"){
		  GANewAnalyticsEventsLogging(cpcPageType,fltFstSub, fltFstLoc);		  
		}	  
	  }
	}
	
});
jq(document).on('click','.next-btn, .prev-btn, .dot',function(e) {		  
	jq('.slider video').each(function(){				
		playAndPauseFn(jq(this).attr('id'),'pause');
	});
	jq('.slider img').each(function(){	
		  var src = jq(this).attr('src');
		  var dataSrc = jq(this).attr('data-src');
		  //if(((src.indexOf('/img_px') > -1)){
		  if(src.indexOf('/img_px') > -1){
			jq(this).attr('src', dataSrc);  
		  }			
		  //}
		});	
});
function videoPauseOnClose() {
  jq('.vid_sec2 video').each(function(){				
    playAndPauseFn(jq(this).attr('id'),'pause');
  });
  //document.location.reload(true);
  setTimeout(function(){
	  var pageType = jq('#pageType').val();
	  var cpcPageType = '';
	  var gaCollegeName = null;
	  var studyLevelDesc = jq('#gaStudyLevelDesc').val();
	      studyLevelDesc = (studyLevelDesc != undefined && studyLevelDesc != '') ? studyLevelDesc : null;
	  if(pageType == "MULTIPLE"){
			cpcPageType = 'multi cpc landing';		
		}else if(pageType == "SINGLE"){
			cpcPageType = 'single cpc landing';
			gaCollegeName = jq('#gaDimCollegeName').val();
		}
	  ga('set', {
		    'dimension1': cpcPageType,
		    'dimension3': gaCollegeName,
		    'dimension5': studyLevelDesc
		});
  }, 1000);
}
jq(window).scroll(function(){
  jq('video').each(function(){
	  var vidId = jq(this).attr('id');
	 if(vidId== "cpc_libox_video"){		 
		 if(!isScrolledIntoView(jq('#'+vidId).get(0), false)){
	       playAndPauseFn(jq(this).attr('id'),'pause');	 
		 }		    
	 }else {
		 playAndPauseFn(jq(this).attr('id'),'pause'); 
	 }	 
  }); 
});
function playAndPauseFn(videoId, action){
var video = jq('#'+ videoId).get(0);		
/*if ( video.paused ) {
jq("#vide_icn_"+videoId).hide();
jq('#'+ videoId).show();
video.play();
//$(".pause").show();
} else {
	video.pause();
	//jq('#'+ videoId).hide();
	//jq("#vide_icn_"+videoId).show();
	//$(".pause").hide();
}*/
if(action == 'pause'){
 video.pause();
 //jq('#'+ videoId).hide();
 //jq("#vide_icn_"+videoId).show();
 }
}
jq(document).on('click touchstart', function(e){
//jq(document).mouseup(function (e) {
	   ajaxFilterHide(e);
});
function ajaxFilterHide(e){
  var container = jq('[data-id=filterDivId_prt]');
  jq.each(container, function(key, value) {
  if (!jq(value).is(e.target) // if the target of the click isn't the container...
     && jq(value).has(e.target).length === 0) // ... nor a descendant of the container
    {
	 var id = jq(value).attr("id");
     id= id.replace('_prt','')
	 if(jq('#'+id).is(':visible')){      
       jq('#'+id).hide();              
     }
   }
  });
}

/*jq(document).ready(function(){	
 // jq("#dsk_adv_submenu").removeAttr("style");    
  jq("#navbar").click(function() {
	jq("#hdr_menu").slideToggle();
	jq(".navbar").toggleClass("navact");
  });
  
  
});
*/

function scrollToViewCourses() {
  var scrollTopPos = jq('#middleSectionId').offset().top;
  jq('body, html').animate({scrollTop: scrollTopPos},"slow");
}

function closeOpenDaydRes(){
  if($$D("odEvntAction")) {    
      if($$D("odEvntAction").value=="false"){        
        var url = contextPath + "/open-days/reserve-place.html?reqAction=exitOpenDaysRes";       
        var ajaxObj = new sack();
        ajaxObj.requestFile = url;	  
        ajaxObj.onCompletion = function(){setOpendaysExitVal(ajaxObj);};	
        ajaxObj.runAJAX();       
      }
    }    
}
function setOpendaysExitVal(ajaxObj){  
  if(ajaxObj.response!="" && ajaxObj.response=="true"){
    $$D("exitOpenDayResFlg").value = ajaxObj.response; 
    $$D("odEvntAction").value = "true";   
  }  
}
function lightBoxLogging(obj,collegeName){
  //GANewAnalyticsEventsLogging('Video Impression View',collegeName,'cpc landing page lightbox');
  videoPlayedDurationPercentage(obj);
}
function showOpendayPopup(collegeId, eventId){
  var url=""
  sm('newvenue', '650', 500, 'opendayPopup');
  var studyLevelId = $jq("#paramStudyLevelId").val();
  url=contextPath + "/openday-popup.html?collegeId="+collegeId+"&eventId="+eventId+"&studyLevelId="+studyLevelId;
  blockNone("ajax-div", "block");
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){ displayFormData(ajax); };
  ajax.runAJAX();
}

function showOpendayAjax(collegeId, eventId){
  hm();
  showOpendayPopup(collegeId, eventId);
} 
function opendayDropdown(){
  $jq("#opendayDropdown").toggle(); 
}
var ua = window.navigator.userAgent;
var iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
var webkit = !!ua.match(/WebKit/i);
var iOSSafari = iOS && webkit && !ua.match(/CriOS/i);
function playLightBoxVideo(index) {
	index = jq('#vidMedId_'+index).val();
	if($jq('#cpc_libox_video_'+index).length > 0 && !iOSSafari) {
		clickPlayAndPause('cpc_libox_video_'+index, event, 'thumbnailImg_'+index,'cpc_play_icon_'+index);    
	}
	var pageType = jq('#pageType').val();
	var cpcPageType = '';
	var gaCollegeName = jq('#gaDimCollegeName').val();
	var studyLevelDesc = jq('#gaStudyLevelDescLB').val();
	var loggedInUserId = jq('#loggedInUserId').val();
	var clearingFlag =jq('#clearingFlag').val();
	if(pageType == "MULTIPLE"){
		cpcPageType = 'multi cpc landing lightbox';	
	}else if(pageType == "SINGLE"){
		cpcPageType = 'single cpc landing lightbox';
	}
	ga('set', 'dimension1', cpcPageType);
	if(gaCollegeName != undefined && gaCollegeName != ''){
	  ga('set', 'dimension3', gaCollegeName);	
	}
	if(studyLevelDesc != undefined && studyLevelDesc != ''){
	  ga('set', 'dimension5', studyLevelDesc);	
	}
	if(loggedInUserId != undefined && loggedInUserId != '' && "0" != loggedInUserId){
	  ga('set', 'dimension7', loggedInUserId);	
	}	
	if(isNotNullAnddUndef(clearingFlag) && "Y" == clearingFlag){
	  ga('set', 'dimension14', "Clearing");	
	}	
}
document.addEventListener('scroll', function (event) {
    if (event.target.id === 'parentDivHeight') { // or any other filtering condition        
    	jq('video').each(function(){				
    	   var vidId = jq(this).attr('id');
    	   if(!isScrolledIntoView(jq('#'+vidId).get(0), false)){
		     playAndPauseFn(jq(this).attr('id'),'pause');	 
		   }   		
    	});
    }
}, true /*Capture event*/);


function changeContact(hotlineNumber, anchorId) {
	  jq("#" + anchorId).attr("title", hotlineNumber);
	  jq("#" + anchorId).html("<i class='fa fa-phone' aria-hidden='true'></"+ "i>"+hotlineNumber);
	  var mobileFlag = $jq("#mobileFlag").val();
	  if(mobileFlag == 'true'){
	    setTimeout(function(){
	     location.href='tel:'+ hotlineNumber;
	    },1000)
	  } 
	}