function drawRPdoughnutchart(value1,value2,currentElement,color)
{
  if(value1!="" && value2!="")
  {
    var $dchart = jQuery.noConflict();
    var limit1  = parseInt(value1);
    var limit2  = parseInt(value2);
    var defColor = "#a65266";      
    
    if(color!="" && color=="plain") {
      defColor = "#dededf";
    }  
    
    $dchart("#" + currentElement).drawDoughnutChart([
    {  
      value : limit1,  
      color: "#fc7169"      
    },
    { 
      value:  limit2,   
      color: defColor      
    }  
    ]);
  } 
}