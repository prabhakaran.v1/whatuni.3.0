var uc = jQuery.noConflict();
function $$(id){return document.getElementById(id)}
function blockNone(thisid, value){if($$(thisid)!=null){$$(thisid).style.display=value;}}
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
var contextPath = "/degrees";
function specialTextValidate(name) {
  var specialChar = "\\#%;?/_&*!@$^()+={}[]|:<>~`";   
  if(name.indexOf('"') != -1){return false;}
  for(var i=0; i<specialChar.length; i++){  
  var str = (name).indexOf(specialChar.charAt(i));
  if(str != -1) {return false;}}return true;
}
function trimString(str){str=this !=window? this : str
return str.replace(/^\s+/g,'').replace(/\s+$/g,'')}
var subField2 = 1;
var subField3 = 1;
var maxField = 6; //Input fields increment limitation
var xdivId = 1;
var xdiv = 1; //Initial field counter is 1
var xFieldCnt = 3; //Initial field counter is 1
uc(document).ready(function(){
 loadYearOfEntryData('qualse1');//Added by Indumathi.S JUN_28_16 REL
 var pageHt = uc(document).height(); 
 uc(".ucas_cnt").height(pageHt); 
 //Add field
 var deMaxField = 6; //Input fields increment limitation 
 var maxDiv = 2; //Input fields increment limitation 
 var addfield1 = uc('.btn_act2'); //Add button selector
 var addButton = uc('#subAdd'); //Add button selector
 var subAdd = uc('#subAdd2'); //Add button selector
 var grdrow = uc('#grdrow2'); //Input field wrapper 
 var wrapper = uc('.grd_row'); //Input field wrapper
 var addqualif1 = uc('.add_qualif'); //Input field wrapper 
 var xField = 3; //Initial field counter is 1 
 var sub2Field = 1; //Initial field counter is 1
 var gradeSpanVal;  
 uc(addButton).click(function () { //Once add button is clicked  
  if (xFieldCnt < deMaxField) { //Check maximum number of input fields
   xField++; //Increment field counter
   xFieldCnt++;
   var rmvAct = '1rmvActDiv'+xField;
   var grades = '1grades'+xField;
   var gradeSpan = '1gradeSpan'+xField;
   gradeSpanVal = "'"+gradeSpan+"'";
   var selGrade = '1selGrade'+xField;
   var ucas = '1ucas'+xField;
   var qualSub = '1qualSub'+xField;
   var grdHid = '1grdHid'+xField;
   var subHid = '1subHid'+xField;
   var qualSubHid = '1qualSub'+xField+'_hidden';
   var rSubRow = '1rSubRow'+xField;
   var qualse1Obj = "'qualse1'";
   var optionSet;
   var qualse1 = $$('qualse1').value;
   optionSet = '<div class="ucas_sbox" id="'+grades+'"><span class="sbox_txt" id="'+gradeSpan+'">A*</span><span class="grdfilt_arw"></span></div><select class="ucas_slist" onchange="setSelectedVal(this, '+gradeSpanVal+');getUcasPointsAndScore('+qualse1Obj+');" id="'+selGrade+'"></select>';
   var fieldHTML = '<div class="rmv_act" id="'+rmvAct+'"><fieldset class="ucas_fldrw"><div class="ucas_selcnt rsize_wdth">'+optionSet+'<input type="hidden" id="'+grdHid+'" name="'+grdHid+'" /><input type="hidden" id="'+subHid+'" name="'+subHid+'" /><input type="text" id="'+ucas+'" name="'+ucas+'" style="display:none;" /></div><div class="ucas_tbox"><input type="text" id="'+qualSub+'" name="'+qualSub+'" onkeyup="ucasQualSubjectList(event, this, '+qualse1Obj+');" onblur="getUcasPointsAndScore('+qualse1Obj+');" placeholder="Enter subject here..." autocomplete="off" /><input type="hidden" id="'+qualSubHid+'" name="'+qualSubHid+'" /></div><div class="add_fld"><a id="'+rSubRow+'" onclick="javascript:removeSubRow(1,'+xField+');" class="btn_act1"><i class="fa fa-times"></i></a></div></fieldset></div>'; //New input field html    
   uc(wrapper).append(fieldHTML); // Add field html
   dynamicCreateGradeSelectBox('qualse1', selGrade, gradeSpan);
   var addQualSubVal = $$('qualSubCnt1').value;
   addQualSubVal++; //Increment field counter
   $$('qualSubCnt1').value=addQualSubVal;
   $$('subCnt1').value=xFieldCnt;
   if($$('subCnt1').value == deMaxField){
    uc('#subAdd1').addClass('ucas_dis');
   }
  }
 });  
 
 uc(addfield1).click(function () { //Once add button is clicked
  if (xdiv <= maxDiv) { //Check maximum number of input fields
   xdiv++; //Increment field counter
   xdivId++;
   $$('qualsTotCnt').value=xdivId;
   var idvar = "'qualspanu"+xdivId+"'";
   var qualspan = "qualspanu"+xdivId;
   var qual = "qualification"+xdivId;
   var qualse = "qualse"+xdivId;   
   var grdrow = "grdrow"+xdivId;
   var reGrdRow = "reGrdRow"+xdivId;
   var qualSubCnt = "qualSubCnt"+xdivId;
   var subCnt = "subCnt"+xdivId;
   var grdParam = 'grdParam'+xdivId;
   var subParam = 'subParam'+xdivId;   
   var divSec = xdivId+'divSec';
   var errQualse1 = 'err_'+qualse;
   var fieldHTML1 = '<div class="ucas_refld rmv_act1" id="'+divSec+'"><div class="ucas_row"><label>Qualification</label><fieldset class="ucas_fldrw"><div class="ucas_selcnt"><div class="ucas_sbox"><span class="sbox_txt" id='+qualspan+'>A Level</span><span class="grdfilt_arw"></span></div><select class="ucas_slist" name="'+qual+'" onchange="setSelectedVal(this, '+idvar+');onChageQualGrades(this);" id="'+qualse+'">'+$$('qualOptions').innerHTML+'</select><div class="err" id="'+errQualse1+'" style="display:none;"></div><input type="hidden" id="'+qualSubCnt+'" value="1" /><input type="hidden" id="'+subCnt+'" value="1" /><input type="hidden" id="'+grdParam+'" name="'+grdParam+'" /><input type="hidden" id="'+subParam+'" name="'+subParam+'" /></div></fieldset></div><div class="ucas_row grd_row" id="'+grdrow+'"><label>Grade</label> </div><div class="add_fld add_qualtxt remv_qualtxt"><a class="btn_act3" id="'+reGrdRow+'"><i class="fa fa-times"></i> Remove</a></div></div>'; //New input field html
   uc(addqualif1).append(fieldHTML1); // Add field html
   defaultSubjectSet(xdivId, qualse);
   if(xdiv==3){
    uc('#addQual').addClass('ucas_dis');
   }
  }
 });
 
 uc(wrapper).on('click', '.btn_act1', function (e) { //Once remove button is clicked
  e.preventDefault();
  uc(this).parents('.rmv_act').remove(); //Remove field html  
  xFieldCnt--; //Decrement field counter  
 });
 uc(addqualif1).on('click', '.btn_act3', function (e) { //Once remove button is clicked
  e.preventDefault();
  uc(this).parents('.rmv_act1').remove(); //Remove field html
  xdiv--; //Decrement field counter  
  if(xdiv<3){
    uc('#addQual').removeClass('ucas_dis');
    getYourScoreDataAjax();
   }
 }); 
});
function defaultSubjectSet(xdivId, qualse){
  var deMaxSub = '';
  var grdsCntId = 'grdsCnt_'+$$(qualse).value;
  if($$(grdsCntId)){deMaxSub = $$(grdsCntId).value;}   
  var qualse1Obj = "'qualse"+xdivId+"'";
  var subAdd = "subAdd"+xdivId;
  var grdrow = "grdrow"+xdivId;
  var finalSubSet = '';  
  for(var k=1;k<=deMaxSub;k++){
    var grdHid = xdivId+'grdHid'+k;
    var subHid = xdivId+'subHid'+k;    
    var qualSub = xdivId+'qualSub'+k;
    var qualSubHid = xdivId+'qualSub'+k+'_hidden';
    var grades = xdivId+'grades'+k;
    var selGrade = xdivId+'selGrade'+k;
    var gradeSpan = xdivId+'gradeSpan'+k;
    var rmvAct = xdivId+'rmvActDiv'+k;
    var rSubRow = xdivId+'rSubRow'+k;
    gradeSpanVal = "'"+gradeSpan+"'";   
    optionSet = '<div class="ucas_sbox" id="'+grades+'"><span class="sbox_txt" id="'+gradeSpan+'">A*</span><span class="grdfilt_arw"></span></div><select class="ucas_slist" onchange="setSelectedVal(this, '+gradeSpanVal+');getUcasPointsAndScore('+qualse1Obj+');" id="'+selGrade+'"></select>';    
    var addDel = '<a id="'+rSubRow+'" onclick="javascript:removeSubRow('+xdivId+','+k+');" class="btn_act1"><i class="fa fa-times"></i></a>'
    if(k==1){
      var adBtCls = 'btn_act';
      if(deMaxSub==6){
        adBtCls = 'btn_act ucas_dis';
      }
      addDel = '<a class="'+adBtCls+'" id="'+subAdd+'" onclick="addNewRowSub('+xdivId+');"><i class="fa fa-plus"></i></a>';
    }
    var subjSet = '<div class="rmv_act" id="'+rmvAct+'"><fieldset class="ucas_fldrw"><div class="ucas_selcnt rsize_wdth">'+optionSet+'<input type="hidden" id="'+grdHid+'" name="'+grdHid+'" /><input type="hidden" id="'+subHid+'" name="'+subHid+'" /></div><div class="ucas_tbox"><input type="text" id="'+qualSub+'" name="'+qualSub+'" onkeyup="ucasQualSubjectList(event, this, '+qualse1Obj+');" onblur="getUcasPointsAndScore('+qualse1Obj+');" placeholder="Enter subject here..." autocomplete="off" /><input type="hidden" id="'+qualSubHid+'" name="'+qualSubHid+'" /></div><div class="add_fld"> '+addDel+' </div></fieldset></div>';
    finalSubSet = finalSubSet + subjSet;    
  }
  $$(grdrow).innerHTML = finalSubSet;
  $$("qualSubCnt"+xdivId).value=deMaxSub;
  $$("subCnt"+xdivId).value=deMaxSub;
  for(var m=1;m<=deMaxSub;m++){
    var selGrade = xdivId+'selGrade'+m;
    var gradeSpan = xdivId+'gradeSpan'+m;
    dynamicCreateGradeSelectBox(qualse, selGrade, gradeSpan);
  }
  
}
function setSelectedVal(obj, spanid){  
  var dobid = obj.id
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  document.getElementById(spanid).innerHTML = text;
}
function addNewRowSub(xId){  
  var qualSubVal = $$('qualSubCnt'+xId).value;
  var subCntVal = $$('subCnt'+xId).value;  
  var grdrowId = 'grdrow'+xId;
  var reSubBtn = qualSubVal+xId;
  var nSubAdd = 'subAdd'+xId;  
  if (subCntVal < maxField) { //Check maximum number of input fields   
   qualSubVal++; //Increment field counter
   subCntVal++; //Increment field counter
   var rmvAct = xId+'rmvActDiv'+qualSubVal;
   var grades = xId+'grades'+qualSubVal;
   var gradeSpan = xId+'gradeSpan'+qualSubVal;
   var gradeSpanVal = "'"+gradeSpan+"'";
   var selGrade = xId+'selGrade'+qualSubVal;
   var ucas = xId+'ucas'+qualSubVal;
   var qualSub = xId+'qualSub'+qualSubVal;
   var grdHid = xId+'grdHid'+qualSubVal;
   var subHid = xId+'subHid'+qualSubVal;
   var qualSubHid = xId+'qualSub'+qualSubVal+'_hidden';
   var rSubRow = xId+'rSubRow'+qualSubVal;
   var qualse1Obj = "'qualse"+xId+"'";
   var qualse1 = $$('qualse'+xId).value;
   var optionSet;   
   optionSet = '<div class="ucas_sbox" id="'+grades+'"><span class="sbox_txt" id="'+gradeSpan+'">A*</span><span class="grdfilt_arw"></span></div><select class="ucas_slist" onchange="setSelectedVal(this, '+gradeSpanVal+');getUcasPointsAndScore('+qualse1Obj+');" id="'+selGrade+'"></select>';
   var subFieldHTML = '<div class="rmv_act" id="'+rmvAct+'"><fieldset class="ucas_fldrw"><div class="ucas_selcnt rsize_wdth">'+optionSet+'<input type="hidden" id="'+grdHid+'" name="'+grdHid+'" /><input type="hidden" id="'+subHid+'" name="'+subHid+'" /><input type="text" id="'+ucas+'" name="'+ucas+'" style="display:none;" /></div><div class="ucas_tbox"><input type="text" id="'+qualSub+'" name="'+qualSub+'" onkeyup="ucasQualSubjectList(event, this, '+qualse1Obj+');" onblur="getUcasPointsAndScore('+qualse1Obj+');" placeholder="Enter subject here..." autocomplete="off"><input type="hidden" id="'+qualSubHid+'" name="'+qualSubHid+'" /></div><div class="add_fld"><a id="'+rSubRow+'" onclick="javascript:removeSubRow('+xId+','+qualSubVal+');" class="btn_act1"><i class="fa fa-times"></i></a></div></fieldset></div>'; //New input field html
   uc('#'+grdrowId).append(subFieldHTML); // Add field html
   dynamicCreateGradeSelectBox('qualse'+xId, selGrade, gradeSpan);
   $$('qualSubCnt'+xId).value=qualSubVal;
   $$('subCnt'+xId).value=subCntVal;   
   if($$('subCnt'+xId).value == maxField){
    uc('#'+nSubAdd).addClass('ucas_dis');
   }
  }
}
function removeSubRow(xId, rowVal){
  var nSubAdd = 'subAdd'+xId;
  var subCntVal = $$('subCnt'+xId).value;
  var reDivId = xId+'rmvActDiv'+rowVal;
  if($$(reDivId)){uc('#'+reDivId).remove();}
  var nGrdHidId = xId+'grdHid'+rowVal;
  var qualSubId = xId+'qualSub'+rowVal;
  if($$(nGrdHidId)){$$(nGrdHidId).value="";}
  if($$(qualSubId)){$$(qualSubId).value="";}  
  getUcasPointsAndScore('qualse'+xId);
  subCntVal--; //Decrement field counter
  $$('subCnt'+xId).value=subCntVal;  
  if($$('subCnt'+xId).value < 6){
    uc('#'+nSubAdd).removeClass('ucas_dis');    
  }
}
function onChageQualGrades(obj){
  var qual = obj.value;
  var qualId = obj.id;
  var hId = qualId.substring(6);
  var qualIds = obj.id.substring(6);
  clearQualsSet(qual, qualIds);
  var qSubCnt = $$('qualSubCnt'+hId).value;
  var grdHid = hId+'grdHid';
  var subHid = hId+'subHid';
  var selGrade = hId+'selGrade';
  var grdParam = 'grdParam'+hId;
  var qualSub = hId+'qualSub';  
  var qualSubHidden = hId+'qualSub';
  $$(grdParam).value="";
  var errQual = 'err_'+qualId;
  if($$(qualId) && $$(qualId).value!="0"){
    blockNone(errQual, 'none');
    $$(grdParam).value=$$(qualId).value;
    defaultSubjectSet(hId, qualId);
  }else{    
    blockNone(errQual, 'block');
    if($$(errQual)){$$(errQual).innerHTML="Please select qualification";}    
    //return false;
  }  
  for(var i = 1; i <=qSubCnt; i++){
    var nGrdHid = grdHid+i;
    var nQualSub = qualSub+i;
    var nSelGrade = selGrade+i;
    var nQualSubHidden = qualSubHidden+i+'_hidden';
    var nSubHid = subHid+i;    
    if($$(nQualSub) && $$(nQualSub).value!=''){
      $$(nGrdHid).value = $$(nSelGrade).value;      
      if($$(nGrdHid)){
        $$(grdParam).value += '-'+$$(nGrdHid).value;         
      }
    }else{
      if($$(nGrdHid)){$$(nGrdHid).value="";} 
      if($$(nQualSubHidden)){$$(nQualSubHidden).value="";}
    }
  }  
  getYourScoreDataAjax();  
}
function displayUCASInput(qualId, id, type, count){  
  for(var i = 1; i <=count; i++){
    var fId = qualId+id+i;    
    if($$(fId)){
      $$(fId).style.display = type;
    }    
  }
}
function dynamicCreateGradeSelectBox(qual, selectBoxId, spanId){
  var valueArray;
  var newSelBoxId = selectBoxId;
  var newSpanId = spanId;
  var grdId = $$(qual).value;
  if($$(qual) && $$(qual).value !="0"){
    valueArray = ($$(grdId).value).split(',');  
    var arrayLength = valueArray.length;   
    if($$(newSelBoxId) && $$(newSelBoxId) != null){
      $$(newSelBoxId).innerHTML = "";
      for(var i = 0; i < arrayLength; i++){
        var option = document.createElement("option");
        option.value = valueArray[i];
        option.appendChild(document.createTextNode(valueArray[i]));
        $$(newSelBoxId).appendChild(option);
      }
      if($$(newSpanId)){$$(newSpanId).innerHTML = valueArray[0];}
    }
    if($$(newSpanId)){$$(newSpanId).disabled = false;uc('#'+newSpanId).removeClass('ucas_dis');}    
  }else{
    if($$(newSpanId)){$$(newSpanId).disabled = true;uc('#'+newSpanId).addClass('ucas_dis');}    
  }
}
function createGradeSelectBox(qualId, qual, selectBoxId, spanId){
  var valueArray;    
  var nQualSubCnt = 'qualSubCnt'+qualId;
  var nQualSubLen;
  if($$(nQualSubCnt)){nQualSubLen=$$(nQualSubCnt).value;}
  for(var k=1;k<=nQualSubLen;k++){
    var newSelBoxId = qualId+selectBoxId+k;
    var newSpanId = qualId+spanId+k;
    if($$(qual) && $$(qual).value !="0"){
      valueArray = ($$(qual).value).split(',');
      var arrayLength = valueArray.length; 
      if($$(newSelBoxId) && $$(newSelBoxId) != null){
        $$(newSelBoxId).innerHTML = "";
        for(var i = 0; i < arrayLength; i++){
          var option = document.createElement("option");
          option.value = valueArray[i];
          option.appendChild(document.createTextNode(valueArray[i]));
          $$(newSelBoxId).appendChild(option);
        }
        if($$(newSpanId)){$$(newSpanId).innerHTML = valueArray[0];}
      }
      if($$(newSpanId)){$$(newSpanId).disabled = false;uc('#'+newSpanId).removeClass('ucas_dis');}      
    }else{
      if($$(newSpanId)){$$(newSpanId).disabled = true;uc('#'+newSpanId).addClass('ucas_dis');}      
    }
  }
}
function ucasQualSubjectList(event, obj, qualId){
  var qual = $$(qualId).value;
  var id = obj.id;
  var hiddenId = id+"_hidden";  //Added by Indumathi.S FEB-16-16 
  $$(id).style.background = "#fff";
  if($$(id).value == "") {
   $$(id).style.background = "#f8f8f8";
  }
  if(specialTextValidate(trimString(obj.value))){
    if(trimString(obj.value) != ""){
      ajax_showOptions(obj,'ucasqualsubects',event,"indicator",'');
    }
  }else{
    $$(hiddenId).value = "";
  }  
}
function getUcasPointsAndScore(qualId){
  var hId = qualId.substring(6);
  var qSubCnt = $$('qualSubCnt'+hId).value;
  var grdHid = hId+'grdHid';
  var subHid = hId+'subHid';
  var selGrade = hId+'selGrade';
  var grdParam = 'grdParam'+hId;
  var qualSub = hId+'qualSub';  
  var qualSubHidden = hId+'qualSub';
  $$(grdParam).value="";
  if($$(qualId) && $$(qualId).value!="0"){
    $$(grdParam).value=$$(qualId).value;
    blockNone(errQual, 'none');    
  }else{
    var errQual = 'err_'+qualId;
    blockNone(errQual, 'block');
    if($$(errQual)){$$(errQual).innerHTML="Please select qualification";}
    
    if($$('userUcasPoints') && $$('userUcasPoints').value!="0"){
      $$('userUcasPoints').value="0";
      $$('userUcasScorePrcnt').value="0";
      var score = "'score'";
      var g1 = "'1'";
      var g2 = "'99'";
      var doughnut = "'ukIntl_doughnut'";    
      $$('innerScoreDiv').innerHTML = '<div class="scr_lft"><!--Chart--><div class="chartbox fl w100p"><div id="ukIntl_doughnut" class="chart fl"><span>POINTS</span></div><script type="text/javascript" id="script_how_you_are_assesed">drawUCASdoughnutchart('+g1+','+g2+','+doughnut+');</script></div><!--Chart--></div><div class="scr_rht"><div class="scrtxt_top"><span class="scr_txt">With your score</span><span class="scr_val">0%</span></div><div class="scr_pbar"><span style="width:0%"></span></div><div class="scr_desc"><p>You could qualify for a place on approximately 0% of all UK courses.</p></div><div class="sub_btn"><a onclick="getTariffPointsTableDataAjax('+score+');">SEE FULL UCAS POINTS TABLE <i class="fa fa-long-arrow-right"></i></a></div></div>';
      var elements = uc("script[id^='script_']");
      if (elements.length > 0) {           
       for (var i = 0; i < elements.length; i++) {
        var ele = elements[i].id;
        if($$(ele)){eval($$(ele).innerHTML);}
       }
      }
    } 
    
    return false;
  }  
  for(var i = 1; i <=qSubCnt; i++){
    var nGrdHid = grdHid+i;
    var nQualSub = qualSub+i;
    var nSelGrade = selGrade+i;
    var nQualSubHidden = qualSubHidden+i+'_hidden';
    var nSubHid = subHid+i;    
    if($$(nQualSub) && $$(nQualSub).value!=''){
      $$(nGrdHid).value = $$(nSelGrade).value;      
      if($$(nGrdHid)){
        $$(grdParam).value += '-'+$$(nGrdHid).value;         
      }
    }else{
      if($$(nGrdHid)){$$(nGrdHid).value="";} 
      if($$(nQualSubHidden)){$$(nQualSubHidden).value="";}
    }
  }
  getYourScoreDataAjax();
}
function getYourScoreDataAjax() {
  var hId = $$('qualsTotCnt').value;
  var yoe;
  var grdParm1='';
  var grdParm2='';
  var grdParm3='';
  var finalUrl = contextPath + "/ucas/scoreDataAjax.html?ajaxFlag=score";
  var url1;
  var url2;
  var url3;
  var url4;
  var ajFlag="N";
  if($$('yoesel').value!=""){    
    finalUrl = finalUrl + "&yoe="+$$('yoesel').value;
  }
  var parameters='';
  var z=1;
  for(var k=1;k<=hId;k++){
    var grdParams = 'grdParam'+k;
    if($$(grdParams)){      
      var grdParameter = 'grdParam'+z;
      if($$(grdParams).value!='' && $$(grdParams).value.indexOf('-') > -1){
        parameters = parameters + "&"+grdParameter+"="+$$(grdParams).value;
        blockNone('subErrMsg', 'none');
        ajFlag="Y";
      }
      z++;
    }
  }
  finalUrl = finalUrl + parameters;  
  if(ajFlag=="Y"){
    var ajax = new sack();  
    ajax.requestFile = finalUrl;
    ajax.onCompletion = function(){ loadYourScoreData(ajax); };
    ajax.runAJAX();  
  }else{
   if($$('userUcasPoints') && $$('userUcasPoints').value!="0"){
      $$('userUcasPoints').value="0";
      $$('userUcasScorePrcnt').value="0";
      var score = "'score'";
      var g1 = "'1'";
      var g2 = "'99'";
      var doughnut = "'ukIntl_doughnut'";    
      $$('innerScoreDiv').innerHTML = '<div class="scr_lft"><!--Chart--><div class="chartbox fl w100p"><div id="ukIntl_doughnut" class="chart fl"><span>POINTS</span></div><script type="text/javascript" id="script_how_you_are_assesed">drawUCASdoughnutchart('+g1+','+g2+','+doughnut+');</script></div><!--Chart--></div><div class="scr_rht"><div class="scrtxt_top"><span class="scr_txt">With your score</span><span class="scr_val">0%</span></div><div class="scr_pbar"><span style="width:0%"></span></div><div class="scr_desc"><p>You could qualify for a place on approximately 0% of all UK courses.</p></div><div class="sub_btn"><a onclick="getTariffPointsTableDataAjax('+score+');">SEE FULL UCAS POINTS TABLE <i class="fa fa-long-arrow-right"></i></a></div></div>';
      var elements = uc("script[id^='script_']");
      if (elements.length > 0) {           
       for (var i = 0; i < elements.length; i++) {
        var ele = elements[i].id;
        if($$(ele)){eval($$(ele).innerHTML);}
       }
      }
    } 
   return false;
  }            
}     
function loadYourScoreData(ajax){
  $$('scoreDiv').innerHTML = "";
  var response = ajax.response;     
  $$('scoreDiv').innerHTML = response;
  if($$('userUcasPoints') && $$('userUcasPoints').value==0 && $$('userUcasScorePrcnt') && $$('userUcasScorePrcnt').value==0){
    blockNone('scoreDiv', 'none');
    blockNone('tariffPod', 'block');
  }else{
    blockNone('scoreDiv', 'block');
    blockNone('tariffPod', 'none');
  }
  var elements = uc("script[id^='script_']");
  if (elements.length > 0) {           
   for (var i = 0; i < elements.length; i++) {
    var ele = elements[i].id;
    if($$(ele)){eval($$(ele).innerHTML);}
   }
  }
}
function getTariffPointsTableDataAjax(flag) {  
  if(flag=='score'){
    var yoe = $$("yoesel").value;  
    if(yoe!="0"){
      if($$('tariffDiv').innerHTML==""){
        TariffPointsTableAjax(flag);
      }else{
        blockNone('tariffPod', 'block');
        blockNone('tariffDiv', 'block');
        blockNone('scoreDiv', 'none');
      }  
    }
  }
  if(flag=='tariff'){    
    TariffPointsTableAjax(flag);
  }
}
function TariffPointsTableAjax(flag){
  var yoe='';
  var tfqualse1ect='1';
  var tfqualval;
  if(flag=='score'){
    if($$('qualOptions') && $$('qualOptions').innerHTML != ""){
      $$('tfqualse1ect').innerHTML = $$('qualOptions').innerHTML;    
    }
  }  
  if($$('yoesel').value!="0"){
    yoe = $$('yoesel').value;
    }
  if($$('tfqualse1ect').value!=""){
    tfqualse1ect = $$('tfqualse1ect').value; 
    var e=$$('tfqualse1ect');
    tfqualval = e.options[e.selectedIndex].innerHTML;
  }  
  var ajax = new sack();
  url = contextPath + "/ucas/tariffDataAjax.html?ajaxFlag=tariff&yoe="+yoe+"&tfqual="+tfqualse1ect+"&tfqualVal="+tfqualval;//Added tfqualval by Indumathi.S FEB-16-16 
  ajax.requestFile = url;
  ajax.onCompletion = function(){ loadTariffPointsTableData(ajax, tfqualval); };
  ajax.runAJAX();
}
function loadTariffPointsTableData(ajax, tfqualval){
  $$('tariffDiv').innerHTML = "";
  var response = ajax.response;
  blockNone('tariffPod', 'block');
  blockNone('scoreDiv', 'none');  
  $$('tariffDiv').innerHTML = response;   
  $$('qualTH').innerHTML = 'UCAS Tariff points' ;//Added by Indumathi.S FEB-16-16
}
function goBackToScore(){
  blockNone('tariffPod', 'none');
  blockNone('scoreDiv', 'block');
}
function submitUcasClacSubject(callFlag){
  var totSubParam='';
  var qualsTotCnt = $$('qualsTotCnt').value;  
  var uUcPnts='0';
  var uUcScPrcnt='0';
  if($$('userUcasPoints') && $$('userUcasScorePrcnt') && $$('userUcasPoints').value!="0"){
    uUcPnts = $$('userUcasPoints').value;
    uUcScPrcnt = $$('userUcasScorePrcnt').value;
        totSubParam=uUcPnts+'-'+uUcScPrcnt;  
    for(var i=1; i<=qualsTotCnt; i++){
     var nQualSubCnt = 'qualSubCnt'+i;     
     var nQualSub = i+'qualSub';     
     if($$(nQualSubCnt)){
      var qSubCnt = $$(nQualSubCnt).value;
      for(var j=1; j<=qSubCnt; j++){
       var newQualSubId = nQualSub+j+'_hidden';
       if($$(newQualSubId) && $$(newQualSubId).value!=""){
        totSubParam = totSubParam +'-' +$$(newQualSubId).value;
       }      
      }
     }
    }
    var ajax = new sack();
    url = contextPath + "/ucas/submitUserScoreAjax.html?ajaxFlag="+callFlag+"&totSubParam="+totSubParam;
    ajax.requestFile = url;
    ajax.onCompletion = function(){ loadUcasClacSubjectData(ajax); };
    ajax.runAJAX();  
  }else{    
    if(callFlag=="submit"){
      totSubParam='';      
      for(var i=1; i<=qualsTotCnt; i++){
       var nQualSubCnt = 'qualSubCnt'+i;     
       var nQualSub = i+'qualSub';
       if($$(nQualSubCnt)){
        var qSubCnt = $$(nQualSubCnt).value;
        for(var j=1; j<=qSubCnt; j++){
         var newQualSubId = nQualSub+j;
         if($$(newQualSubId) && $$(newQualSubId).value!=""){
          totSubParam = totSubParam +'-' +$$(newQualSubId).value;
         }
        }
       }
      }
      if(totSubParam.indexOf('-') == -1){
        blockNone('subErrMsg', 'block');
        $$('subErrMsg').innerHTML="Please select at least one subject";
      }
    }
  }
}
function loadUcasClacSubjectData(ajax){  
   $$('ucas_calc_sucs').innerHTML = "";
   var response = ajax.response;
   $$('ucas_calc_sucs').innerHTML = response;   
   window.scrollTo(0,0);//Added by Indumathi.S to scroll top of the window FEB-16-16
   blockNone('ucas_calc', 'none');
   blockNone('ucas_calc_sucs', 'block');
}
function youCouldStudySubmit(callFlag){
  var totSubParam='';
  var qualsTotCnt = $$('qualsTotCnt').value;
  var uUcPnts='0';
  var uUcScPrcnt='0';
  if($$('subjectsList').value!="0"){
    if($$('userUcasPoints') && $$('userUcasScorePrcnt') && $$('userUcasPoints').value!="0"){
      uUcPnts = $$('userUcasPoints').value;
      uUcScPrcnt = $$('userUcasScorePrcnt').value;  
          totSubParam=uUcPnts+'-'+uUcScPrcnt;  
          totSubParam = totSubParam +'-'+ $$('subjectsList').value;    
      var ajax = new sack();
      url = contextPath + "/ucas/youCouldStudySubmitAjax.html?ajaxFlag="+callFlag+"&totSubParam="+totSubParam;
      ajax.requestFile = url;
      ajax.onCompletion = function(){ loadYouCouldStudyData(ajax); };
      ajax.runAJAX();
    } 
  }else{
    blockNone('resultsDpwn', 'none');
  }  
}
function loadYouCouldStudyData(ajax){
   $$('resultsDpwn').innerHTML = "";
   var response = ajax.response;
   $$('resultsDpwn').innerHTML = response;
   blockNone('resultsDpwn', 'block');
}
function clearQualsSet(qual, id){  
  var nQualSub = id+'qualSub';
  var nQualSubCnt = 'qualSubCnt'+id;
  var nRmvActDiv = id+'rmvActDiv';  
  var nSubCnt = 'subCnt'+id;
  var nGrdParam = 'grdParam'+id;
  var nGrdsCnt = 'grdsCnt_'+qual;
  var nSubAdd = 'subAdd'+id;
  var nQualSubLen;
  if($$(nGrdParam)){$$(nGrdParam).value="";}
  if($$(nQualSubCnt)){nQualSubLen = $$(nQualSubCnt).value;}  
  if($$(nSubCnt)){$$(nSubCnt).value="1";}  
  for(var j=1;j<=nQualSubLen;j++){
    var newQualSub = nQualSub+j;
    var newRmvActDiv = nRmvActDiv+j;
    var newQualSubHid = nQualSub+j+'_hidden';
    if($$(newQualSub)){$$(newQualSub).value="";}
    if($$(newQualSubHid)){$$(newQualSubHid).value="";}    
    if($$(newRmvActDiv)){
     uc('#'+newRmvActDiv).remove();
    }
  }    
}
function clearYoeSet(){
  var qualsTotCnt = $$('qualsTotCnt').value;  
  for(var i=1; i<=qualsTotCnt;i++){    
    var nDivSec = i+'divSec';
    var nQualSub = i+'qualSub';
    var nQualSubCnt = 'qualSubCnt'+i;
    var nRmvActDiv = i+'rmvActDiv';
    var grdParam = 'grdParam'+i;
    var nQualSubLen;
    if($$(grdParam)){$$(grdParam).value="";}
    if($$(nQualSubCnt)){nQualSubLen = $$(nQualSubCnt).value;}
    for(var j=1;j<=nQualSubLen;j++){
      var newQualSub = nQualSub+j;
      var newQualSubHid = nQualSub+j+'_hidden';
      var newRmvActDiv = nRmvActDiv+j;
      if($$(newQualSub)){$$(newQualSub).value="";}
      if($$(newQualSubHid)){$$(newQualSubHid).value="";}
      if($$(newRmvActDiv)){   
       uc('#'+newRmvActDiv).remove();
       xFieldCnt = 3;
       $$('subCnt1').value="3";
      }
    }
    $$('qualspanu1').innerHTML="A Level";
    $$('qualse1').value="1";
    if($$(nDivSec)){   
     uc('#'+nDivSec).remove();
     $$('qualsTotCnt').value="1";
     xdiv=1;          
    }
  }  
  uc('#addQual').removeClass('ucas_dis');  
  blockNone('scoreDiv', 'block');
  blockNone('tariffPod', 'none');
  if($$('userUcasPoints') && $$('userUcasPoints').value!="0"){
    $$('userUcasPoints').value="0";
    $$('userUcasScorePrcnt').value="0";
    var score = "'score'";
    var g1 = "'1'";
    var g2 = "'99'";
    var doughnut = "'ukIntl_doughnut'";    
    $$('innerScoreDiv').innerHTML = '<div class="scr_lft"><!--Chart--><div class="chartbox fl w100p"><div id="ukIntl_doughnut" class="chart fl"><span>POINTS</span></div><script type="text/javascript" id="script_how_you_are_assesed">drawUCASdoughnutchart('+g1+','+g2+','+doughnut+');</script></div><!--Chart--></div><div class="scr_rht"><div class="scrtxt_top"><span class="scr_txt">With your score</span><span class="scr_val">0%</span></div><div class="scr_pbar"><span style="width:0%"></span></div><div class="scr_desc"><p>You could qualify for a place on approximately 0% of all UK courses.</p></div><div class="sub_btn"><a onclick="getTariffPointsTableDataAjax('+score+');">SEE FULL UCAS POINTS TABLE <i class="fa fa-long-arrow-right"></i></a></div></div>';
    var elements = uc("script[id^='script_']");
    if (elements.length > 0) {           
     for (var i = 0; i < elements.length; i++) {
      var ele = elements[i].id;
      if($$(ele)){eval($$(ele).innerHTML);}
     }
    }
  } 
}

function loadYearOfEntryData(qualId){
  $$(qualId).innerHTML = "";
  $$(qualId).innerHTML = $$('qualOptions').innerHTML;
  var qualsTotCnt = $$('qualsTotCnt').value;
  clearYoeSet();
  for(var i=1; i<=qualsTotCnt;i++){
    var nQualSubCnt = 'qualSubCnt'+i;
    var nQualSeId = 'qualse'+i;
    var nSubAdd = 'subAdd'+i;
    var nSelGrade = i+'selGrade';
    var nQualSub = i+'qualSub';   
    var nQualspanu = 'qualspanu'+i;
      var nGradeSpan = i+'gradeSpan';
    if($$(nQualspanu)){
      $$(nQualspanu).disabled = false;
      uc('#'+nQualspanu).removeClass('ucas_dis');
    }
    if($$(nQualSeId)){
      $$(nQualSeId).disabled = false;      
    }
    if($$(nSubAdd)){
      $$(nSubAdd).disabled = false;
      uc('#'+nSubAdd).removeClass('ucas_dis');
      blockNone(nSubAdd, 'block');
    }
    if($$(nQualSubCnt)){
      var qSubCnt = $$(nQualSubCnt).value;
      for(var j=1; j<=qSubCnt; j++){
        var newSelGrade = nSelGrade+j;
        var newQualSub = nQualSub+j;
        var newGradeSpan = nGradeSpan+j;
        if($$(newGradeSpan)){
          $$(newGradeSpan).disabled = false;
          uc('#'+newGradeSpan).removeClass('ucas_dis');
        }
        if($$(newSelGrade)){
          $$(newSelGrade).disabled = false;          
        }
        if($$(newQualSub)){
          $$(newQualSub).disabled = false;
          uc('#'+newQualSub).removeClass('ucas_dis');
        }
      }
    }
  }
  defaultSubjectSet(1, 'qualse1');
  blockNone('addQual', 'block');  
  blockNone('subErrMsg', 'none');
  uc('#addQual').removeClass('ucas_dis');  
  if($$('err_qualse1')){blockNone('err_qualse1', 'none');}
}
function autoCompleteUcasKeywordBrowse(event,object){  
  var qualDesc = 'degree';
  var qualCode = "M";  
  var id = object.id;
  $$(id).style.background = "#fff";
  if($$(id).value == "") {
   $$(id).style.background = "#f8f8f8";
  }
  if($$('ajax_listOfOptions')){
    $$('ajax_listOfOptions').className="";
  }	   
  ajax_showOptions(object,'ucaskeywordbrowse',event,"indicator",'');
}
function delUcasPointsForSR(){
  blockNone('fcUcas', 'none');
  $$('fcUcasPoints').value="";
}
function ucasSearchSubmit(clFlag){
  var ucasKwd = trimString($$('ucasKwd').value);  
  if(ucasKwd!=""){
    var srUrl='';    
    var finalUrl='';
    var newUrl1="/degree-courses/search";
    var newUrl2;
    if(clFlag=="NO"){
      newUrl2= "?clearing&q="+ucasKwd;
    }else{
      newUrl2= "?q="+ucasKwd;
    }
    finalUrl = newUrl1 + newUrl2;
    if($$("ucasKeyword_url").value != "" ||  $$("ucasmatchbrowsenode").value != ""){
      finalUrl = $$("ucasKeyword_url").value != "" ? $$("ucasKeyword_url").value : $$("ucasmatchbrowsenode").value;
    }    
    var fcPoints = $$('fcUcasPoints').value;
    srUrl = finalUrl;
    if(fcPoints!=""){
      srUrl = finalUrl+"&entry-level=ucas-points&entry-points="+fcPoints+"&rf=g";
      //setUcasSessionCookie('gradePopup','NO','wucookiepolicy');
      var url = contextPath+"/create-grade-popup-cookie.html?cookieName=gradePopup&cookieValue=NO";      
      var ajaxObj = new sack();
      ajaxObj.requestFile = url;	  
      ajaxObj.onCompletion = function(){};	
      ajaxObj.runAJAX();
    }
    $$("ucasKeyword_hidden").value="";
    $$("ucasKeyword_id").value="";
    $$("ucasKeyword_display").value="";
    $$("ucasKeyword_url").value="";    
    openNewWindow(srUrl.toLowerCase());
  }else{
    alert('Search error.\n Please enter a valid keyword and qualification.');
  }
}
function openNewWindow(link){
  window.open(link, "_blank");
}
function setUcasSessionCookie(c_name,value){  
  var c_value;
  var IEversion = findBrowser();
  if (IEversion !== false) {
    c_value=escape(value) + ";";
  }else{
    c_value=escape(value) + "; expires=0";
  } 
  document.cookie=c_name + "=" + c_value+"; path=/";
}
function findBrowser() {
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf('MSIE ');
  if (msie > 0) { // IE 10 or older
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }
  var trident = ua.indexOf('Trident/');
  if (trident > 0) { // IE 11
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }
  var edge = ua.indexOf('Edge/');
  if (edge > 0) { // IE 12
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }	
  return false;
}
function drawUCASdoughnutchart(value1,value2,currentElement,color){
  if(value1!="" && value2!=""){
    var $dchart = jQuery.noConflict();
    var limit1  = parseInt(value1);
    var limit2  = parseInt(value2);    
    var defColor = "#f2f2f2";    
    if(color!="" && color=="plain") {
      defColor = "#f2f2f2";
    }
    $dchart("#" + currentElement).drawDoughnutChart([
    {  
      value : limit1,  
      color: "#00bbfd"      
    },
    { 
      value:  limit2,   
      color: defColor      
    }    
    ]);
  } 
}
function searchAgainFn(){//Added by Indumathi.S back to previous page FEB-16-16
 window.scrollTo(0,0);
 blockNone('ucas_calc_sucs', 'none');
 blockNone('ucas_calc', 'block');
 clearYoeSet();
 defaultSubjectSet(1, 'qualse1');
}

function ucasClose(){
  window.location = contextPath + "/home.html";
}