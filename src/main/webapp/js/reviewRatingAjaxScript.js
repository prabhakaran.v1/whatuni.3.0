function voteWrittenReview(contextPath,reviewId){
  var status=$$("rateReview").checked == true ? "checked" : "unchecked";
  var rateUrl = contextPath+"/rateReview.html?reviewId="+reviewId+"&status="+status;
  var ajaxRequest=new sack();
  ajaxRequest.requestFile = rateUrl;	// Specifying which file to get
  ajaxRequest.onCompletion = function(){ responseData(ajaxRequest); };	// Specify function that will be executed after file has been found
  ajaxRequest.runAJAX();
  function responseData(ajaxRequest){
    with(document){
      $$("dispRating").innerHTML=ajaxRequest.response;
      $$("dispRating").style.display='block';
      if(ajaxRequest.response == 'Rated this review successfully'){
        $$("rcnt").innerHTML=parseInt($$("rcount").value)+parseInt(1);
        if(parseInt($$("rcount").value)+parseInt(1) ==1){
           $$("rcnt_txt").innerHTML =" person has "
        }else{
          $$("rcnt_txt").innerHTML =" people have "
        }
        $$("reviewCount").style.display='block';
      }
    }
  }
}