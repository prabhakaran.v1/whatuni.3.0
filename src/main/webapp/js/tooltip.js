var dhtip=false;var shadow=false;var shadowsize=3;var MaxWidth=400;var MinWidth=400;var dhiframe=false
var tooltip_is_msie=(navigator.userAgent.indexOf('MSIE')>=0&&navigator.userAgent.indexOf('opera')==-1&&document.all)?true:false
function showTooltip(e,tooltipTxt){
var bodyWidth=Math.max(document.body.clientWidth,document.documentElement.clientWidth)-20
if(!dhtip){
dhtip=document.createElement('DIV');dhtip.id='dhtip';shadow=document.createElement('DIV');shadow.id='shadow'
document.body.appendChild(dhtip);document.body.appendChild(shadow)
if(tooltip_is_msie){
dhiframe=document.createElement('DIV')
dhiframe.frameborder='5'
dhiframe.style.backgroundColor='#FFFFFF'
dhiframe.src='#'
dhiframe.style.zIndex=100
dhiframe.style.position='absolute'
document.body.appendChild(dhiframe)}}
dhtip.style.display='block';shadow.style.display='block'
if(tooltip_is_msie)dhiframe.style.display='block'
var st=Math.max(document.body.scrollTop,document.documentElement.scrollTop)
if(navigator.userAgent.toLowerCase().indexOf('safari')>=0)st=0
var leftPos=e.clientX+10
dhtip.style.width=null
dhtip.innerHTML=document.getElementById("tabletext").innerHTML
dhtip.style.left=leftPos+'px'
dhtip.style.top=e.clientY+10+st+'px'
shadow.style.left=leftPos+shadowsize+'px'
shadow.style.top=e.clientY+10+st+shadowsize+'px'
if(dhtip.offsetWidth>MaxWidth){
dhtip.style.width=MaxWidth+'px'}
var tooltipWidth=dhtip.offsetWidth
if(tooltipWidth<MinWidth)tooltipWidth=MinWidth
dhtip.style.width=tooltipWidth+'px'
shadow.style.width=dhtip.offsetWidth+'px'
shadow.style.height=dhtip.offsetHeight+'px'
if((leftPos+tooltipWidth)>bodyWidth){
dhtip.style.left=(shadow.style.left.replace('px','')-((leftPos+tooltipWidth)-bodyWidth))+'px'
shadow.style.left=(shadow.style.left.replace('px','')-((leftPos+tooltipWidth)-bodyWidth)+shadowsize)+'px'}
if(tooltip_is_msie){
dhiframe.style.left=dhtip.style.left
dhiframe.style.top=dhtip.style.top
dhiframe.style.width=dhtip.offsetWidth+'px'
dhiframe.style.height=dhtip.offsetHeight+'px'}}
function hideTooltip(){
dhtip.style.display='none'
shadow.style.display='none'
if(tooltip_is_msie)dhiframe.style.display='none'}
