var jq = jQuery.noConflict();
var contextPath = "/degrees";

function $$(id) {
	return document.getElementById(id);
}

function isBlankOrNullString(str) {
	var flag = true;
	if (str.trim() != "" && str.trim().length > 0) {
		flag = false;
	}
	return flag;
}

function isNotBlankOrNull(input) {
	return (input != null && input.trim().length > 0 && input != undefined) ? true : false;
}

function trimString(inString) {
	return inString.replace(/^\s+|\s+$/g, "");
}

function blockNone(thisid, value) {
	if ($$(thisid) != null) {
		$$(thisid).style.display = value;
	}
}

function getDeviceWidth() {
	return (window.innerWidth > 0) ? window.innerWidth : screen.width;
}

var deviceWidth = "";

var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
jq.fn.notBlankOrNull = function(input) {
	if (!isNaN(input)) {
		input = input.toString();
	}
	if (input != null && input != "" && input != undefined && (input != null && input.trim().length > 0)) {
		return true;
	} else {
		return false;
	}
}
// Ready functions
jq(document).ready(function() {
	deviceWidth = getDeviceWidth();
	jq(".cata-sub-nav").on('scroll', function() {
		
		var scrollLeft = jq(this).scrollLeft();
		if (jq(this).scrollLeft() + jq(this).innerWidth() >= jq(this)[0].scrollWidth) {
			jq(".nav-next").hide();
		} else {
			jq(".nav-next").show();
		}
		if (scrollLeft == 0) {
			jq(".nav-prev").hide();
		} else {
			jq(".nav-prev").show();
		}
	});
});

/* slider starts here(Releated Subjects) */
jq(".nav-next").on("click", function() {
	jq(".cata-sub-nav").animate({
		scrollLeft: '+=460'
	}, 200);
});
jq(".nav-prev").on("click", function() {
	jq(".cata-sub-nav").animate({
		scrollLeft: '-=460'
	}, 200);
});

/* slider ends here(Releated Subjects) */


/*****************EventTracking for subject filter*************************/
jq(document).on("click", "#subjetFilter a", function(event) {
	var id = jq(this).attr('id');   
   if (!(jq("#" + id).hasClass("active"))) {
		var categoryType = jq('#' + id).attr('data-categoryType');
		var filterType = jq('#' + id).attr('data-filterType');
		var filtersApplied = jq('#' + id).attr('data-filtersApplied');
	    
        searchEventTracking(categoryType, filterType, filtersApplied);
	 }	
});

/*Middle content section */

/*GTM and Chain-process-GA methods for Profile page, PR page and CD page links*/
jq(document).on("click", "a[data-uni-link], a[data-pr-link], a[data-cd-link], a[data-viewallpr-link]", function(event) {
	var collegeId = isNotBlankOrNull(jq(this).attr("data-collegeId")) ? jq(this).attr("data-collegeId") : '';
	var index = isNotBlankOrNull(jq(this).attr("data-index")) ? jq(this).attr("data-index") : '';
	var productClickPageGTM = (jq(this).attr("data-productClick-page") && isNotBlankOrNull(jq(this).attr("data-productClick-page"))) ? jq(this).attr("data-productClick-page") : '';

	sponsoredListGALogging(collegeId);
	gtmproductClick(index, productClickPageGTM, collegeId);

});
/*End of: GTM and Chain-process-GA methods for Profile page, PR page and CD page links*/

/*CTA button click method*/
jq(document).on("click", "a[data-nonadv-link]", function(event) {
	var nonAdvertDetails = jq(this).attr("data-nonadvpdfdownload");

	nonAdvPdfDownload(nonAdvertDetails,"searchResults");
	 
});

jq(document).on("click", "a[data-visitweb-link], a[data-prospectuswebform-link], a[data-prospectsform-link], a[data-requestinfowebform-link], a[data-requestinfoform-link], a[data-opendaywebform-link]", function(event) {

	/*GTM, Chain-process-GA and Interaction-event-GA for Request Info, Get Prospects, Visit Website and Open Day Buttons*/
	var collegeId = isNotBlankOrNull(jq(this).attr("data-collegeId")) ? jq(this).attr("data-collegeId") : '';
	var index = isNotBlankOrNull(jq(this).attr("data-index")) ? jq(this).attr("data-index") : '';
	var productClickPageGTM = (jq(this).attr("data-productClick-page") && isNotBlankOrNull(jq(this).attr("data-productClick-page"))) ? jq(this).attr("data-productClick-page") : '';
	var buttonType = isNotBlankOrNull(jq(this).attr("data-buttonType")) ? jq(this).attr("data-buttonType") : '';
	var eventCategory = isNotBlankOrNull(jq(this).attr("data-eventCategory")) ? jq(this).attr("data-eventCategory") : '';
	var eventAction = isNotBlankOrNull(jq(this).attr("data-eventAction")) ? jq(this).attr("data-eventAction") : '';
	var collegeName = isNotBlankOrNull(jq(this).attr("data-collegeName")) ? jq(this).attr("data-collegeName") : '';
	var webPrice = isNotBlankOrNull(jq(this).attr("data-webPrice")) ? jq(this).attr("data-webPrice") : '';
	var courseId = isNotBlankOrNull(jq(this).attr("data-courseId")) ? jq(this).attr("data-courseId") : '';
	var subOrderItemId = isNotBlankOrNull(jq(this).attr("data-subOrderItemId")) ? jq(this).attr("data-subOrderItemId") : '';
	var networkId = isNotBlankOrNull(jq(this).attr("data-networkId")) ? jq(this).attr("data-networkId") : '';
	var subOrderWebsite = isNotBlankOrNull(jq(this).attr("data-suborderWebsite")) ? jq(this).attr("data-suborderWebsite") : '';
	var sponsoredFlag = isNotBlankOrNull(jq(this).attr("data-sponsoredListFlag")) ? jq(this).attr("data-sponsoredListFlag") : '';
	var subOrderProspectusWebForm = isNotBlankOrNull(jq(this).attr("data-subOrderProspectusWebform")) ? jq(this).attr("data-subOrderProspectusWebform") : '';
	var subjectOrKeyword = isNotBlankOrNull(jq(this).attr("data-subjectOrKeyword")) ? jq(this).attr("data-subjectOrKeyword") : '';
	var sponsoredOrderItem = isNotBlankOrNull(jq(this).attr("data-sponsoredOrderItem")) ? jq(this).attr("data-sponsoredOrderItem") : '';
	var subOrderEmailWebForm = isNotBlankOrNull(jq(this).attr("data-subOrderEmailWebForm")) ? jq(this).attr("data-subOrderEmailWebForm") : '';
	var opendayDate = isNotBlankOrNull(jq(this).attr("data-opendayDate")) ? jq(this).attr("data-opendayDate") : '';
	var opendayMonthYear = isNotBlankOrNull(jq(this).attr("data-opendayMonthYear")) ? jq(this).attr("data-opendayMonthYear") : '';
	var opendayBookingUrl = isNotBlankOrNull(jq(this).attr("data-opendayBookingUrl")) ? jq(this).attr("data-opendayBookingUrl") : '';
	var eventCategoryNameGA = isNotBlankOrNull(jq(this).attr("data-eventCatoryNameGA")) ? jq(this).attr("data-eventCatoryNameGA") : '';

	sponsoredListGALogging(collegeId);
	gtmproductClick(index, productClickPageGTM, collegeId);
	GAInteractionEventTracking(buttonType, eventCategory, eventAction, collegeName, webPrice);

	/*End of: GTM, Chain-process-GA and Interaction-event-GA for Request Info, Get Prospects, Visit Website and Open Day Buttons*/

	/*Visit Website button CPE(Stats) log and Add Baskets methods calling*/
	if (jq(this).attr("data-visitweb-link") == "visit_web") {

		jq.fn.statsLoggingWithCourseId(collegeId, courseId, subOrderItemId, networkId, subOrderWebsite, sponsoredFlag);
		addBasket(courseId, 'O', 'visitweb', 'basket_div_' + courseId, 'basket_pop_div_' + courseId, '', '', collegeName, 'visit_web_lb_new');
	}
	/*End of: Visit Website button CPE(Stats log) and Add Baskets methods calling*/

	/*Get Prospectus web form CPE(Stats) log method calling*/
	if (jq(this).attr("data-prospectuswebform-link") == "get_prospectus_webform") {
		jq.fn.statsLoggingGetProspectus(collegeId, subOrderItemId, networkId, subOrderProspectusWebForm, sponsoredFlag);
	}
	/*End of: Get Prospectus web form CPE(Stats) log method calling*/

	/*Get Prospectus Form method calling*/
	if (jq(this).attr("data-prospectsform-link") == "get_prospectus_form") {
		prospectusRedirect(contextPath, collegeId, courseId, subjectOrKeyword, '', subOrderItemId, '', '', sponsoredOrderItem, 'prospectus_lb_new');
	}
	/*End of: Get Prospectus Form method calling*/

	/*Request Info web form CPE(Stats) log method calling*/
	if (jq(this).attr("data-requestinfowebform-link") == "request_info_webform") {
		jq.fn.statsLoggingRequestInfo(collegeId, subOrderItemId, networkId, subOrderEmailWebForm, sponsoredFlag);
	}
	/*End of: Request Info web form CPE(Stats) log method calling*/

	/*Request Info AddRoll log method calling*/
	if (jq(this).attr("data-requestinfoform-link") == "request_info_form") {
		jq.fn.adRollLoggingRequestInfo(collegeId, this.href);
	}
	/*End of: Request Info AddRoll log method calling*/

	/*Open day web form - Reverse open day , CPE(Stats)log , GANewAnalyticsEventsLogging and adBlockerEnable method calling*/
	if (jq(this).attr("data-opendaywebform-link") == "open_day_webform") {

		jq.fn.addOpenDaysForReservePlace(opendayDate, opendayMonthYear, collegeId, subOrderItemId, networkId);
		jq.fn.statsLoggingBookEvent(collegeId, subOrderItemId, networkId, opendayBookingUrl, sponsoredFlag);
		GANewAnalyticsEventsLogging('open-days-type', eventCategoryNameGA, collegeName);
		adBlockerEnable(opendayBookingUrl);
	}
	/*End of: Open day web form - Reverse open day , CPE(Stats)log , GANewAnalyticsEventsLogging and adBlockerEnable method calling*/

});

/*CTA button click method end*/

jq(document).on("click", "a[data-addcompare], a[data-removecompare], a[data-viewcompare]", function(event) {
	var courseId = isNotBlankOrNull(jq(this).attr("data-courseId")) ? jq(this).attr("data-courseId") : '';
	var collegeName = isNotBlankOrNull(jq(this).attr("data-collegeName")) ? jq(this).attr("data-collegeName") : '';
	var courseTitle = isNotBlankOrNull(jq(this).attr("data-courseTitle")) ? jq(this).attr("data-courseTitle") : '';

	if (jq(this).attr("data-addcompare") == "add-comparison") {
		addBasket(courseId, 'O', this, 'basket_div_' + courseId, 'basket_pop_div_' + courseId, '', '', collegeName);

		searchEventTracking('view comparison', 'clicked', courseTitle + ': ' + collegeName);
	}

	if (jq(this).attr("data-removecompare") == "remove-comparison") {
		addBasket(courseId, 'O', this, 'basket_div_' + courseId, 'basket_pop_div_' + courseId);

		searchEventTracking('view comparison', 'removed', courseTitle + ': ' + collegeName);
	}

	if (jq(this).attr("data-viewcompare") == "view-comparison") {
		showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');
	}
});
/*End of Middle content section */



/*To set height for filter*/
jq.fn.setHeightForFiltBox = function() {
	// Scrollbar height
	var filterBlock = jq('#filterOpenType').val() + 'Block';
	if (jq('#pageName').val() == 'Institution Profile' || jq('#pageName').val() == 'Course Details') {
		filterBlock = 'filterSlider';
	}
	var filterOpenType = jq('#filterOpenType').val();
	var pageWidth = document.documentElement.clientWidth;
	var pageHeight = document.documentElement.clientHeight;


	if(filterOpenType != ''){
		 topFiltHeight = jq('#'+filterOpenType).find(".fltr_hd").outerHeight();
		}
	topFiltHeight = 97;
	//console.log("topFiltHeight-->"+topFiltHeight);

	var btmHeight = jq(".btm_pod").innerHeight() - 120;
	if (jq('.btm_pod').is(':visible')) {
		btmHeight = jq(".btm_pod").innerHeight();
	}

	//console.log("btmHeight-->"+btmHeight);
	var scrollheight = pageHeight - topFiltHeight - btmHeight - 30;
	//console.log("scrollheight-->"+scrollheight);
	// For Devices
	if (pageWidth <= 480) {
		scrollheight = pageHeight - topFiltHeight - btmHeight;
	}

	if (pageWidth > 480 && pageWidth <= 992) {
		scrollheight = pageHeight - topFiltHeight - btmHeight;
	}
	var innerHg = jq.fn.getInnerHg();
	if (scrollheight < innerHg) {
		jq(".grd_fltr .stk_pod").addClass("flrt_bshd");
	} else {
		jq(".grd_fltr .stk_pod").removeClass("flrt_bshd");
	}
	jq(".slimScrollDiv .slim-scroll").height(scrollheight);
	jq(".slimScrollDiv .slim-scroll").css('max-height', scrollheight);
}

jq.fn.getInnerHg = function() {
	var totalHg = 0;
	if (jq('#filterOpenType').val() == 'yourGrades') {
		totalHg += jq('#additional_qual_0') != undefined ? jq('#additional_qual_0').height() : 0;
		totalHg += jq('#additional_qual_1') != undefined ? jq('#additional_qual_1').height() : 0;
		totalHg += jq('#additional_qual_2') != undefined ? jq('#additional_qual_2').height() : 0;
		totalHg += jq('#additional_qual') != undefined ? jq('#additional_qual').height() : 0;
	}
	return totalHg;
}

/*End of height for filter function*/

/*Start of close button function in filters*/
jq('body').on('click', '.closebtn, .clr_fltr .overlay', function(event) {
	var filterOpenType = jq('#filterOpenType').val();
	jq.fn.copyFields();
	if (filterOpenType != 'yourGrades') {
		jq('#clr_fltr, #sidenav, #' + filterOpenType).hide();
		jq('#filterOpenType').val('');
		var root = document.getElementsByTagName('html')[0];
		root.className = '';
	}
});

jq.fn.copyFields = function() {

	jq('#regionHidden').val(jq('#regionHiddenUrl').val());
	jq('#studyModeHidden').val(jq('#studyModeHiddenUrl').val());
	jq('#locationTypeHidden').val(jq('#locationTypeHiddenUrl').val());

	jq('#russellHidden').val(jq('#russellHiddenUrl').val());
	jq('#employmentRateHidden').val(jq('#employmentRateHiddenUrl').val());
	jq('#campusTypeHidden').val(jq('#campusTypeHiddenUrl').val());
	jq('#keywordHidden').val(jq('#keywordHiddenUrl').val());
	jq('#subjectHidden').val(jq('#subjectHiddenUrl').val());
	jq('#qualificationHidden').val(jq('#qualificationHiddenUrl').val());
	jq('#universityHidden').val(jq('#universityHiddenUrl').val());

	jq('#scoreHidden').val(jq('#scoreHiddenUrl').val());
}
/*End of close button function in filters*/

/*Start of url Formation function for filters*/
jq.fn.urlFormation = function(pageFlag) {
	var qualification = jq('#qualificationHidden').val();
	var university = jq('#universityHidden').val();
	qualification = jq.fn.notBlankOrNull(qualification) ? qualification : jq.fn.notBlankOrNull(university) ? 'all' : 'degree';
	var searchUrl = contextPath + '/';
	searchUrl += qualification + "-courses/" + ((jq.fn.notBlankOrNull(university)) ? "csearch.html?" : "search.html?");
	var filterString = searchUrl;
	if (pageFlag == 'gradefilter') {
		filterString = "";
	}
	var subject = jq('#subjectHidden').val();
	var region = jq('#regionHidden').val();
	var studyMode = jq('#studyModeHidden').val();
	var locationType = jq('#locationTypeHidden').val();
	var keyword = jq('#keywordHidden').val();
	var campusType = jq('#campusTypeHidden').val();
	var employmentRate = jq('#employmentRateHidden').val();
	var russellGroup = jq('#russellHidden').val();
	var score = jq('#scoreHidden').val();
	var sortBy = jq('#sortHidden').val();
	filterString += (jq.fn.notBlankOrNull(subject)) ? "subject=" + subject : (jq.fn.notBlankOrNull(keyword)) ? "&q=" + keyword : "";
	filterString += (jq.fn.notBlankOrNull(university)) ? "&university=" + university : "";
	filterString += ((jq.fn.notBlankOrNull(region)) && (region != 'UNITED-KINGDOM') && !(jq.fn.notBlankOrNull(university))) ? "&location=" + region : "";
	filterString += (jq.fn.notBlankOrNull(locationType) && !(jq.fn.notBlankOrNull(university))) ? "&location-type=" + locationType : "";
	filterString += (jq.fn.notBlankOrNull(campusType) && !(jq.fn.notBlankOrNull(university))) ? "&campus-type=" + campusType : "";
	filterString += (jq.fn.notBlankOrNull(studyMode)) ? "&study-mode=" + studyMode : "";
	filterString += (jq.fn.notBlankOrNull(employmentRate) && !(jq.fn.notBlankOrNull(university))) ? "&employment-rate-min=" + employmentRate + "&employment-rate-max=100" : "";
	filterString += (jq.fn.notBlankOrNull(russellGroup) && !(jq.fn.notBlankOrNull(university))) ? "&russell-group=" + russellGroup : "";

	filterString += (jq.fn.notBlankOrNull(score)) ? "&score=" + score : "";
	filterString += (jq.fn.notBlankOrNull(sortBy) && sortBy != 'R' && !(jq.fn.notBlankOrNull(university))) ? "&sort=" + sortBy : "";
	if (pageFlag == 'C') {
		filterString = "";
		filterString = searchUrl;
		filterString += (jq.fn.notBlankOrNull(subject)) ? "&subject=" + subject : (jq.fn.notBlankOrNull(keyword)) ? "&q=" + keyword : "";
		filterString += (jq.fn.notBlankOrNull(university)) ? "&university=" + university : "";
		filterString += (jq.fn.notBlankOrNull(sortBy) && sortBy != 'R') ? "&sort=" + sortBy : "";
	}
	searchURL = filterString.toLowerCase();
	jq('#loaderTopNavImg').show();
	window.location.href = searchURL;
}

/*End of url Formation function for filters*/

/*while applying filter*/
jq('body').on('click', '#apply a', function(event) {
	var filterOpenType = jq('#filterOpenType').val();
	jq('#' + filterOpenType).find('a').each(function(index) {
		if (jq(this).hasClass('slct_chip') && jq(this).is(":visible")) {
			var filter = jq(this).attr('data-filter-show');

			var gaValue = jq(this).attr('data-' + filter + '-ga-value');
			if (filter == 'qualification') {
				var subjectNameurl = jq(this).attr('data-' + filter + '-subject-name-url');
				jq('#subjectHidden').val(subjectNameurl);
			}

			if (jq.fn.notBlankOrNull(gaValue)) {
				filterGALogging(gaValue);
			}
		}
	});
	jq('#' + filterOpenType).find('input').each(function(index) {
		var filter = jq(this).attr('data-filter-show');
		var gaValue = "";
		if (jq("#" + filter).prop('checked')) {
			gaValue = filter + ',yes';
		} else {
			gaValue = filter + ',no';
		}
		if (jq.fn.notBlankOrNull(gaValue) && jq.fn.notBlankOrNull(filter)) {
			filterGALogging(gaValue);
		}
	});
    
	if (jq('#filterOpenType').val() != 'yourGrades') {
		jq.fn.urlFormation();
	}
});

/*While clearing filters*/
jq('body').on('click', '#clear', function(event) {
	jq('.grd_chips').find('a').each(function(index) {
		if (jq(this).hasClass('slct_chip') && jq(this).is(":visible")) {
			var filter = jq(this).attr('data-filter-show');
			jq('#' + filter + 'Div').find('a').removeClass('slct_chip');
			jq('#' + filter + 'Hidden').val('');
		}
	});
	var filterOpenType = jq('#filterOpenType').val();
	jq('.switch input, .coa_cont input').each(function(index) {
		if (jq(this).hasClass('slct_chip') && jq('.switch, .coa_cont').is(":visible")) {
			var filter = jq(this).attr('data-filter-show');
			jq('#' + filter).removeClass('slct_chip');
			jq('#' + filter).prop('checked', false);
			jq('#' + filter + 'Hidden').val('');
		}
	});
});

/*While opening filters*/
jq('body').on('click', '[data-filter-option]', function(event) {
	
	if(jq("#filterCSSId").length == 0){
       dynamicLoadJS(jq("#contextJsPath").val()+jq("#filterCSS").val(), 'filterCSSId');  
    }

    if(jq("#slimScrollJSId").length == 0){
       dynamicLoadJS(jq("#contextJsPath").val()+jq("#slimScrollJs").val(), 'slimScrollJSId');  
    }

	jq('.ajax_ul').slimscroll({});
	
	var filterType = jq(this).attr('data-filter-option');
	if (filterType == 'clearAllFilter') {
		if(jq('#sesUcasPt').val() != ''){j.fn.clearSessionGradeFilter('no_redirect');}

		jq.fn.urlFormation('C');
	} else if (filterType != 'yourGrades') {
		if (filterType == 'universityFilter') {
			jq.fn.getInstituionList(this, filterType);
		}
		if (filterType != 'universityFilter') {
			jq('#clr_fltr, #sidenav, #' + filterType).show();
			var root = document.getElementsByTagName('html')[0];
			root.className = 'scrl_dis';
			jq('#bottom_button').hide();
			jq.fn.setHeightForFiltBox();
			jq('#filterOpenType').val(filterType);
		}

	}
	if (jq(this).hasClass('btnbg')) {
		jq('#bottom_button').show();
		jq.fn.setHeightForFiltBox();
	}
});

/*While changing filters*/
jq('body').on('click', '[data-filter-show]', function(event) {
	var filter = jq(this).attr('data-filter-show');
	if (filter == 'employmentRate' || filter == 'campusType' || filter == 'qualification' || filter == 'studyMode' || filter == 'region') {
		if (jq(this).hasClass('slct_chip')) {
			jq('#' + filter + 'Div').find('a').removeClass('slct_chip');
			jq('#' + filter + 'Hidden').val('');
		} else {
			jq('#' + filter + 'Div').find('a').removeClass('slct_chip');
			jq(this).addClass('slct_chip');
		}
	} else {
		jq(this).toggleClass('slct_chip', '');
	}
	jq('#bottom_button').show();
	var value = "";
	var uniName = "";

	if (filter == 'russell') {
		if (jq('#' + filter).attr('class') == 'slct_chip') {
			jq('#' + filter).attr('data-' + filter + '-value', 'Yes');
		}
		if (jq("#" + filter).prop('checked')) {
			jq('#' + filter + 'Hidden').val('Yes');
		} else {
			jq('#' + filter + 'Hidden').val('');
		}
	} else {
		value = jq('#' + filter + 'Div').find('.slct_chip').map(function(index) {
			uniName = jq(this).attr('data-' + filter + '-ga-value');
			return jq(this).attr('data-' + filter + '-value');
		}).get().join(',');
		jq('#' + filter + 'Hidden').val(value);
	}
	jq.fn.setHeightForFiltBox();
	if (filter == 'university') {

		if(jq.fn.notBlankOrNull(uniName)) {
    		searchEventTracking("SR-search", "university", uniName);    		
    	}
		jq.fn.urlFormation();
	}
});


/*To get university list using ajax*/
jq.fn.getInstituionList = function(thisObj, filterType) {
	var inputAttr = {};
	jq('#loaderTopNavImg').show();
	var url = '/search-result/get-institution-ajax.html'
	jq.ajax({
		type: "POST",
		url: contextPath + url,
		cache: true,
		success: function(result) {
			jq('#ajax_listOfOptions_university').html(result);
			jq('#loaderTopNavImg').hide();
			jq('#ajaxUniversityName').val('');
			jq('#clr_fltr, #sidenav, #' + filterType).show();
			var root = document.getElementsByTagName('html')[0];
			root.className = 'scrl_dis';
			jq('#bottom_button').hide();
			jq.fn.setHeightForFiltBox();
			jq('#filterOpenType').val(filterType);
			jq('.ajax_ul').slimscroll({});
			if (jq(thisObj).hasClass('btnbg')) {
				jq('#bottom_button').show();
				jq.fn.setHeightForFiltBox();
			}
		}
	});
}

/*While searching university name*/
jq('body').on('keyup', '#ajaxUniversityName', function(event) {
	var value = jq(this).val().trim().toLowerCase();
	value = value.replace(reg, " ");
	value = value.trim();
	if (!(jq.fn.notBlankOrNull(value))) {
		jq('#ajaxUniDropDown').hide();
		jq('#ajaxUniDropDown').parent('.slimScrollDiv').hide();
		return;
	}
	if (value.length > 0) {
		var flag = "N";
		jq('#ajaxUniDropDown').show();
		jq('#ajaxUniDropDown').parent('.slimScrollDiv').show();
		jq("#ajaxUniDropDown li a").each(function() {
			var listText = jq(this).text().toLowerCase();
			var listId = jq(this).parent('li').attr('id');
			var indexPos = listText.indexOf(value);
			var prexfix = '<u>';
			var suffix = '</u>';
			if (indexPos >= 0) {
				var endPos = indexPos + value.length + prexfix.length;
				var texWithunderLine = jq(this).text().substring(0, indexPos) + prexfix + jq(this).text().substring(indexPos);
				texWithunderLine = texWithunderLine.substring(0, endPos) + suffix + texWithunderLine.substring(endPos);
				jq(this).html(texWithunderLine);
				jq('#' + listId).show();
			} else {
				jq("#" + listId).hide();
			}
		});
	} else {
		jq('#ajaxUniDropDown').hide();
		jq('#ajaxUniDropDown').parent('.slimScrollDiv').hide();
	}
});

/*While applying sortby*/
jq('body').on('click', '[data-sort-show]', function(event) {
	if (jq('#filterOpenType').val() != 'yourGrades') {
		var sortBy = jq(this).attr('data-sort-show');
		var eventAction = jq(this).attr('data-' + sortBy + '-ga-value');
		jq('#sortHidden').val(jq(this).attr('data-' + sortBy + '-value'));
		if (sortBy =='overallRating') {
		searchEventTracking("sort", eventAction, "overall rating");
		} else {
			searchEventTracking("sort", eventAction, "clicked");
		}
		jq.fn.urlFormation();
	}
});

//Ga logging function for filters
function filterGALogging(gaValue) {
	var eventCategory = "filter";
	var eventAction = "";
	var eventLabel = "";
	if (gaValue.indexOf(",") > -1) {
		var eventValue = gaValue.split(',');
		eventAction = eventValue[0];
		eventLabel = gaValue.slice(gaValue.indexOf(",")+1);
		if(jq.fn.notBlankOrNull(eventAction) && jq.fn.notBlankOrNull(eventLabel)) {
			searchEventTracking(eventCategory, eventAction, eventLabel);
		}
	}
}

//Ad-blocker enable method
function adBlockerEnable(adUrl) {
	if (isNotNullAndUndex(adUrl)) {
		window.open(adUrl, '_blank');
	}
}

/*****************EventTracking*************************/
function searchEventTracking(CategoryType, filterType, filtersApplied, extra) {
	eventTimeTracking();
	var interactionType = true;
	var loginType = "0"
	if (extra != undefined && extra != "") {
		loginType = extra
	} else {
		if (document.getElementById("loginStaus")) {
			if (document.getElementById("loginStaus").value) {
				loginType = "1";
			}
		}
	}
	loginType = parseInt(loginType);
	//TODO CHANGE THIS ID TO LIVE : UA-22939628-2 OR TEST ID : UA-37421000-1 or Mobile Test : UA-44578438-1    
	ga('send', 'event', CategoryType, filterType, filtersApplied, loginType, {
		nonInteraction: interactionType
	}); //Added by Amir for UA logging for 24-NOV-15 release  
}


/* DB Stats logging starts*/

jq.fn.searchResultOnScrollEvent = function() {
	var sponsoredId = jq('#SPONSORED_LISTING');
	var dataId = "data-id";
	var statsNotLogged = "sponsored_stats_not_logged";
	var statsLogged = "sponsored_stats_logged";
	var collegeId = jq('#collegeIdSponsored').val();
	var collegeName = jq('#collegeNameSponsored').val();
	var orderItemIdSponsored = jq('#orderItemIdSponsored').val();
	var browseCatCode = jq("#paramSubjectCode").val();
	var subject = jq("#subjectHidden").val();
	var qualification = jq("#paramStudyLevelId").val();
	var networkId = jq("#networkId").val();

	if (sponsoredId.length && sponsoredId != undefined && sponsoredId != null) {
		if (jq.fn.isScrollIntoView(sponsoredId, false) && sponsoredId.attr(dataId) == statsNotLogged) {
			jq.fn.searchResultSponsoredViewLogging(collegeId, 'SPONSORED_PAGE_VIEW', collegeName, orderItemIdSponsored, browseCatCode, subject, qualification, networkId);
			sponsoredId.attr(dataId, statsLogged);
		}
	}
}

jq.fn.searchResultSponsoredViewLogging = function(collegeId, clickType, externalUrl, orderItemId, browseCatCode, subject, qualification, networkId) {
	if (isBlank(networkId)) {
		networkId = '2';
	}
	if (isBlank(externalUrl)) {
		externalUrl = '';
	}
	var subOrderItemId = "";
	jq.fn.statsLoggingBookEventAndSponsoredViewAjax(collegeId, subOrderItemId, networkId, externalUrl, clickType, orderItemId, browseCatCode, subject, qualification, orderItemId);
}


jq.fn.statsLoggingWithCourseId = function(collegeId, courseId, subOrderItemId, networkId, externalUrl, sponsoredFlag) {
	if (!isNotBlankOrNull(networkId)) {
		networkId = '2';
	}
	if (!isNotBlankOrNull(subOrderItemId)) {
		subOrderItemId = '0';
	}
	var sourceOrderItemId = "";
	if (sponsoredFlag == "SPONSORED_LISTING") {
		sourceOrderItemId = jq("#orderItemIdSponsored").val();
	}


	jq.fn.statsLoggingWithCourseIdAjax(collegeId, courseId, subOrderItemId, networkId, externalUrl, "WEBSITE", sourceOrderItemId, sponsoredFlag);
	jq.fn.insightIntLogging(collegeId, "webclick");
}


jq.fn.statsLoggingGetProspectus = function(collegeId, subOrderItemId, networkId, externalUrl, sponsoredFlag) {
	if (!isNotBlankOrNull(networkId)) {
		networkId = '2';
	}

	if (!isNotBlankOrNull(subOrderItemId)) {
		subOrderItemId = '0';
	}

	var sourceOrderItemId = "";
	if (sponsoredFlag == "SPONSORED_LISTING") {
		sourceOrderItemId = jq("#orderItemIdSponsored").val();
	}

	jq.fn.statsLoggingRequestInfoAndGetProspectusAjax(collegeId, subOrderItemId, networkId, externalUrl, "PROSPECTUS_WEBFORM", sourceOrderItemId, sponsoredFlag);
}

jq.fn.statsLoggingRequestInfo = function(collegeid, subOrderItemId, networkId, externalUrl, sponsoredFlag) {
	if (!isNotBlankOrNull(networkId)) {
		networkId = '2';
	}

	if (!isNotBlankOrNull(subOrderItemId)) {
		subOrderItemId = '0';
	}

	var sourceOrderItemId = "";
	if (sponsoredFlag == "SPONSORED_LISTING") {
		sourceOrderItemId = jq("#orderItemIdSponsored").val();
	}

	jq.fn.statsLoggingRequestInfoAndGetProspectusAjax(collegeid, subOrderItemId, networkId, externalUrl, "EMAIL_WEBFORM", sourceOrderItemId, sponsoredFlag);
}

jq.fn.statsLoggingBookEvent = function(collegeId, subOrderItemId, networkId, externalUrl, sponsoredFlag) {
	jq.fn.closeOpenDaydRes();
	if (!isNotBlankOrNull(networkId)) {
		networkId = '2';
	}

	if (!isNotBlankOrNull(subOrderItemId)) {
		subOrderItemId = '0';
	}
	var sourceOrderItemId = "";
	if (sponsoredFlag == "SPONSORED_LISTING") {
		sourceOrderItemId = jq("#orderItemIdSponsored").val();
	}

	jq.fn.statsLoggingBookEventAndSponsoredViewAjax(collegeId, subOrderItemId, networkId, externalUrl, "PROVIDER_OD_RESERVE_PLACE", "", "", "", "", sourceOrderItemId, sponsoredFlag);
}


function nonAdvertiasorPdfDownloadClick(obj, collegeId, courseId, subOrderItemId, networkId, externalUrl){
	if (!isNotBlankOrNull(subOrderItemId)) {
		subOrderItemId = '0';
	}
	
  jq.fn.statsLoggingWithCourseIdAjax(collegeId, courseId, subOrderItemId, networkId, externalUrl, "NON_ADV_PDF_DOWNLOAD","","");  
}

jq.fn.statsLoggingWithCourseIdAjax = function(collegeId, courseId, subOrderItemId, networkId, externalUrl, clickType, sourceOrderItemId, sponsoredFlag) {
	if (!isNotBlankOrNull(sponsoredFlag)) {
		sponsoredFlag = "";
	}
	var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z=' + collegeId + "&subOrderItemId=" + subOrderItemId + "&networkId=" + networkId + "&externalUrl=" + encodeURIComponent(externalUrl) + "&clickType=" + clickType + "&vwcourseId=" + courseId + '&screenwidth=' + deviceWidth + '&sourceOrderItemId=' + sourceOrderItemId + '&sponsoredFlag=' + sponsoredFlag;
	jq.post(url, function(data) {
		if (data != "") {
			try {
				__adroll.record_user({
					"adroll_segments": data
				});
			} catch (err) {}
		}
	});
}

jq.fn.statsLoggingRequestInfoAndGetProspectusAjax = function(collegeId, subOrderItemId, networkId, externalUrl, clickType, sourceOrderItemId, sponsoredFlag) {
	if (!isNotBlankOrNull(sponsoredFlag)) {
		sponsoredFlag = "";
	}
	var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z=' + collegeId + "&subOrderItemId=" + subOrderItemId + "&networkId=" + networkId + "&externalUrl=" + encodeURIComponent(externalUrl) + "&clickType=" + clickType + '&screenwidth=' + deviceWidth + '&sourceOrderItemId=' + sourceOrderItemId + '&sponsoredFlag=' + sponsoredFlag;
	jq.post(url, function(data) {
		if (data != "") {
			try {
				__adroll.record_user({
					"adroll_segments": data
				});
			} catch (err) {}
		}
	});
}

jq.fn.statsLoggingBookEventAndSponsoredViewAjax = function(collegeId, subOrderItemId, networkId, externalUrl, clickType, orderItemId, browseCatCode, subject, qualification, sourceOrderItemId, sponsoredFlag) {
	if (!isNotBlankOrNull(sourceOrderItemId)) {
		sourceOrderItemId = "";
	}
	if (!isNotBlankOrNull(sponsoredFlag)) {
		sponsoredFlag = "";
	}
	var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z=' + collegeId + "&subOrderItemId=" + subOrderItemId + "&networkId=" + networkId + "&externalUrl=" + encodeURIComponent(externalUrl) + "&clickType=" + clickType + '&screenwidth=' + deviceWidth + '&orderItemId=' + orderItemId + "&categoryCode=" + browseCatCode + "&featuredSubject=" + subject + "&studyLevelId=" + qualification + "&sourceOrderItemId=" + sourceOrderItemId + "&sponsoredFlag=" + sponsoredFlag;
	jq.post(url, function() {
		return false;
	});
}

jq.fn.closeOpenDaydRes = function() {
	if ($$("odEvntAction")) {
		if ($$("odEvntAction").value == "false") {
			var url = contextPath + "/open-days/reserve-place.html?reqAction=exitOpenDaysRes";
			jq.get(url, function(data) {
				jq.fn.setOpendaysExitVal(data);
			});
		}
	}
}

jq.fn.setOpendaysExitVal = function(data) {
	if (data != "" && data == "true") {
		$$("exitOpenDayResFlg").value = data;
		$$("odEvntAction").value = "true";
	}
}

jq.fn.addOpenDaysForReservePlace = function(opendate, openyear, collegeId) {
	var url = "/degrees/add-opendays-for-reserveplace.html?collegeId= " + collegeId + "&opendate=" + opendate + "&openyear=" + openyear;
	jq.post(url, function() {});
}


jq.fn.adRollLoggingRequestInfo = function(collegeId, pagename) {
	var url = '/degrees/adrollmarketingajax.html?collegeid=' + collegeId + "&buttonType=" + "RI";
	jq.post(url, function(data) {
		if (data != "" || data != null) {
			try {
				__adroll.record_user({
					"adroll_segments": data
				});
			} catch (err) {}
		}
		location.href = pagename;
	});
}

function advertiserExternalUrlStatsLogging(logType, z, url, orderItemId){      
  deviceWidth = getDeviceWidth();
  var url = "/degrees/advertiser-external-url-stats-logging.html?logType="+logType+"&z="+z+"&url="+encodeURIComponent(url)+'&screenwidth='+deviceWidth +'&orderItemId='+orderItemId;
  var ajax = new sack();
  ajax.requestFile = url;
  ajax.runAJAX();
}

function adRollLoggingGetPros(collegeid){
 var ajax=new sack();
  var url = '/degrees/adrollmarketingajax.html?collegeid='+collegeid+"&buttonType="+"GP";
  ajax.requestFile = url;
  ajax.onCompletion = function(){ responseDataforAdRollGP(ajax); };	
  ajax.runAJAX();
}

function responseDataforAdRollGP(ajax){
  var segment = ajax.response;
  if(ajax.response!=""){
      try {
      __adroll.record_user({"adroll_segments": segment});      
    } catch(err){}  
  }
}
/* DB Stats logging ends*/

jq.fn.insightIntLogging = function(collegeId, interactionType) {
	var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?hcCollegeId=' + collegeId + '&screenwidth=' + deviceWidth;
	jq.post(url, function(data) {
		jq.fn.insightIntLoggingData(data, interactionType);
	});
}

jq.fn.insightIntLoggingData = function(data, interactionType) {
	var institutionId = trimString(data);
	var insDimQualType = "";
	var insDimLevelType = "";
	var insDimUID = "";
	var insDimYOE = "";

	if ($$("insDimQualType")) {
		insDimQualType = jq("insDimQualType").value;
	}
	if ($$("insDimLevelType")) {
		insDimLevelType = jq("insDimLevelType").value;
	}
	if ($$("insDimUID")) {
		insDimUID = jq("insDimUID").value;
	}
	if ($$("insDimYOE")) {
		insDimYOE = jq("insDimYOE").value;
	}

	ga('create', 'UA-52581773-4', 'auto', {
		'name': 'newTracker'
	});
	ga('newTracker.set', 'contentGroup1', 'whatuni');
	ga('newTracker.set', 'dimension1', interactionType);
	ga('newTracker.set', 'dimension2', 'whatuni');
	ga('newTracker.set', 'dimension3', institutionId);

	if (insDimQualType != "") {
		ga('newTracker.set', 'dimension4', insDimQualType);
	}
	if (insDimLevelType != "") {
		ga('newTracker.set', 'dimension5', insDimLevelType);
	}
	if (insDimUID != "") {
		ga('newTracker.set', 'dimension15', insDimUID);
	}
	if (insDimYOE != "") {
		ga('newTracker.set', 'dimension19', insDimYOE);
	}
	ga('newTracker.send', 'pageview');
}


jq(document).on("click", "#headerSectionDiv a", function(event) {
	
	var gaValue = jq('#topNavAnchorTagId').attr('data-topNav-ga-value');
	if (gaValue.indexOf(",") > -1) {
		var eventValue = gaValue.split(',');
		var eventAction = eventValue[0];
		var eventLabel = gaValue.slice(gaValue.indexOf(",")+1);
		if(jq.fn.notBlankOrNull(eventAction) && jq.fn.notBlankOrNull(eventLabel)) {
			searchEventTracking("New Search", eventAction, eventLabel);
		}
	}
});
j.fn.clearSessionGradeFilter = function (formURL){	
	var inputArr = {}
	j('#loaderTopNavImg').show();
	inputArr.ajaxForNull = 'Y';
	  j.ajax({
		type : "POST",
		url : contextPathGf + '/delete-srgrade-filter-info.html',
		data : inputArr,
		cache : true,
		success : function(result) {
		  if(formURL != 'no_redirect'){			  
			j('#loaderTopNavImg').hide();
			j('#loaderTopNavImg').show();
			window.location.href = formURL;
		  }			  
		}
		});
	}

