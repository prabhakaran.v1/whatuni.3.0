var jq = jQuery.noConflict();
var contextPath = "/degrees";
var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
jq.fn.notBlankOrNull = function(input){if(!isNaN(input)){input = input.toString();}if(input != null && input != "" && input != undefined && (input != null && input.trim().length > 0)){return true;}else{return false;}}
/* slider starts here(Releated Subjects) */
jq(document).ready(function () {
	jq('.ajax_ul').slimscroll({});
  jq(".cata-sub-nav").on('scroll', function() {
	val = jq(this).scrollLeft();
	if(jq(this).scrollLeft() + jq(this).innerWidth()>=jq(this)[0].scrollWidth){
	  jq(".nav-next").hide();
	} else {
	  jq(".nav-next").show();
	}
	if(val == 0){
	  jq(".nav-prev").hide();
	} else {
	  jq(".nav-prev").show();
	}
  });
  console.log( 'init-scroll: ' + jq(".nav-next").scrollLeft() );
  jq(".nav-next").on("click", function(){
    jq(".cata-sub-nav").animate( { scrollLeft: '+=460' }, 200);
  });
  jq(".nav-prev").on("click", function(){
    jq(".cata-sub-nav").animate( { scrollLeft: '-=460' }, 200);
  });

/* slider ends here(Releated Subjects) */


  /*****************EventTracking*************************/
  jq(document).on("click", "#subjetFilter a", function(event) {
  	   var id = jq(this).attr('id');
       var categoryType = jq('#' + id).attr('data-categoryType');
       var filterType = jq('#' + id).attr('data-filterType');
       var filtersApplied = jq('#' + id).attr('data-filtersApplied');

      eventTimeTracking(); 
	  var interactionType = true;
	  var loginType = "0"      
	  if(extra!=undefined && extra!="") {    
	    loginType = extra
	  }else{    
	    if(document.getElementById("loginStaus")) {    
	      if(document.getElementById("loginStaus").value) {
	        loginType = "1";
	      }
	    }  
	  }
	  loginType = parseInt(loginType);
	  //TODO CHANGE THIS ID TO LIVE : UA-22939628-2 OR TEST ID : UA-37421000-1 or Mobile Test : UA-44578438-1    
	  ga('send', 'event', categoryType , filterType , filtersApplied , loginType , {nonInteraction: interactionType});   
  });

});

jq.fn.setHeightForFiltBox = function(){
	// Scrollbar height
	var filterBlock = jq('#filterOpenType').val() + 'Block';
	if(jq('#pageName').val() == 'Institution Profile' || jq('#pageName').val() == 'Course Details'){
		filterBlock = 'filterSlider';
	}
	var filterOpenType = jq('#filterOpenType').val();
	var pageWidth = document.documentElement.clientWidth;
	var pageHeight = document.documentElement.clientHeight;
	
	var topFiltHeight = jq('#'+filterOpenType).find(".fltr_hd").outerHeight();
	topFiltHeight = 97; 
	//console.log("topFiltHeight-->"+topFiltHeight);
	
	var btmHeight = jq(".btm_pod").innerHeight() - 120;
	if(jq('.btm_pod').is(':visible')){
		btmHeight = jq(".btm_pod").innerHeight();
	}
	
	//console.log("btmHeight-->"+btmHeight);
	var scrollheight = pageHeight - topFiltHeight - btmHeight - 30;
	//console.log("scrollheight-->"+scrollheight);
	// For Devices
	if(pageWidth <= 480){
		scrollheight = pageHeight - topFiltHeight - btmHeight;
	}

	if(pageWidth > 480 && pageWidth <= 992){
		scrollheight = pageHeight - topFiltHeight - btmHeight;
	}
	var innerHg = jq.fn.getInnerHg();
	if( scrollheight < innerHg){
	  jq(".grd_fltr .stk_pod").addClass("flrt_bshd");
	}
	else{
	  jq(".grd_fltr .stk_pod").removeClass("flrt_bshd");
	}
	jq(".slimScrollDiv .slim-scroll").height(scrollheight);
	jq(".slimScrollDiv .slim-scroll").css('max-height', scrollheight);
}

jq.fn.getInnerHg = function(){
	  var totalHg = 0;
	  if(jq('#filterOpenType').val() == 'yourGrades'){
		totalHg += jq('#additional_qual_0') != undefined ? jq('#additional_qual_0').height() : 0;
		totalHg += jq('#additional_qual_1') != undefined ? jq('#additional_qual_1').height() : 0;
		totalHg += jq('#additional_qual_2') != undefined ? jq('#additional_qual_2').height() : 0;
		totalHg += jq('#additional_qual') != undefined ? jq('#additional_qual').height() : 0;
	  }
	  return totalHg;
	}

jq('body').on('click', '.closebtn, .clr_fltr .overlay', function(event){
	var filterOpenType = jq('#filterOpenType').val();
	jq.fn.copyFields();
	if(filterOpenType != 'yourGrades'){
	  jq('#clr_fltr, #sidenav, #'+filterOpenType).hide();
	  jq('#filterOpenType').val('');
	  var root = document.getElementsByTagName( 'html')[0];   
	    root.className = '';
	}
});

jq.fn.copyFields = function(){
	
	jq('#regionHidden').val(jq('#regionHiddenUrl').val());
	jq('#studyModeHidden').val(jq('#studyModeHiddenUrl').val());
	jq('#locationTypeHidden').val(jq('#locationTypeHiddenUrl').val());
	jq('#russellGroupHidden').val(jq('#russellGroupHiddenUrl').val());
	jq('#employmentRateHidden').val(jq('#employmentRateHiddenUrl').val());
	jq('#campusTypeHidden').val(jq('#campusTypeHiddenUrl').val());
	jq('#keywordHidden').val(jq('#keywordHiddenUrl').val());
	jq('#subjectHidden').val(jq('#subjectHiddenUrl').val());
	jq('#qualificationHidden').val(jq('#qualificationHiddenUrl').val());
	jq('#universityHidden').val(jq('#universityHiddenUrl').val());
}

jq.fn.urlFormation = function(pageFlag) {
	 var qualification = jq('#qualificationHidden').val();
	 var university = jq('#universityHidden').val();
	 qualification = jq.fn.notBlankOrNull(qualification) ? qualification : jq.fn.notBlankOrNull(university) ? 'all' : 'degree';
	 var searchUrl = contextPath +'/';
	 searchUrl += qualification + "-courses/"+((jq.fn.notBlankOrNull(university)) ? "csearch.html?" : "search.html?");
	 var filterString = searchUrl;
	 if(pageFlag == 'gradefilter'){	 
	   filterString = "";   
	 }
	 var subject = jq('#subjectHidden').val();
	 var region = jq('#regionHidden').val();
	 var studyMode = jq('#studyModeHidden').val();
	 var locationType = jq('#locationTypeHidden').val();
	 var keyword = jq('#keywordHidden').val();
	 var campusType = jq('#campusTypeHidden').val();
	 var employmentRate = jq('#employmentRateHidden').val();
	 var russellGroup = jq('#russellGroupHidden').val();
	// var pageNo = jq('#pageNoHidden').val();
	 var sortBy = jq('#sortHidden').val();
	 filterString += (jq.fn.notBlankOrNull(subject)) ? "subject=" + subject : (jq.fn.notBlankOrNull(keyword)) ? "&q=" + keyword : "";
	 filterString += (jq.fn.notBlankOrNull(university)) ? "&university=" + university : "";
	 filterString += ((jq.fn.notBlankOrNull(region)) && (region != 'UNITED-KINGDOM') && !(jq.fn.notBlankOrNull(university))) ? "&location=" + region : "";
	 filterString += (jq.fn.notBlankOrNull(locationType) && !(jq.fn.notBlankOrNull(university))) ? "&location-type=" + locationType : "";
	 filterString += (jq.fn.notBlankOrNull(campusType) && !(jq.fn.notBlankOrNull(university))) ? "&campus-type=" + campusType : "";
	 filterString += (jq.fn.notBlankOrNull(studyMode)) ? "&study-mode=" + studyMode : "";
	 filterString += (jq.fn.notBlankOrNull(employmentRate) && !(jq.fn.notBlankOrNull(university))) ? "&employment-rate-min=" + employmentRate + "&employment-rate-max=100" : "";
	 filterString += (jq.fn.notBlankOrNull(russellGroup) && !(jq.fn.notBlankOrNull(university))) ? "&russell-group=" + russellGroup : "";
	 filterString += (jq.fn.notBlankOrNull(sortBy) && sortBy != 'R' && !(jq.fn.notBlankOrNull(university))) ? "&sort=" + sortBy : "";
	// filterString += (jq.fn.notBlankOrNull(pageNo) && pageNo != "1" && !(jq.fn.notBlankOrNull(university))) ? "&pageno=" + pageNo : "";
	 if(pageFlag == 'C'){
		filterString = "";
		filterString = searchUrl;
		filterString += (jq.fn.notBlankOrNull(subject)) ? "&subject=" + subject : (jq.fn.notBlankOrNull(keyword)) ? "&q=" + keyword : ""; 
		filterString += (jq.fn.notBlankOrNull(university)) ? "&university=" + university : "";
	    filterString += (jq.fn.notBlankOrNull(sortBy) && sortBy != 'R') ? "&sort=" + sortBy : "";
     }
	 searchURL = filterString.toLowerCase();
	 jq('#loaderTopNavImg').show(); 
	 window.location.href = searchURL;
}

jq('body').on('click', '#apply a', function(event) {
	var filterOpenType = jq('#filterOpenType').val();	
	jq('#' + filterOpenType).find('a').each(function(index) {
		if (jq(this).hasClass('slct_chip') && jq(this).is(":visible")) {
			var filter = jq(this).attr('data-filter-show');
			if(filter == 'qualification'){
			  var subjectNameurl = jq(this).attr('data-' + filter + '-subject-name-url');
			  jq('#subjectHidden').val(subjectNameurl);
			}			
		}
	});
		
	if (jq('#filterOpenType').val() != 'yourGrades') {
		jq.fn.urlFormation();
	}
});

jq('body').on('click', '#clear', function(event) {
	jq('.grd_chips').find('a').each(function(index) {
		if (jq(this).hasClass('slct_chip') && jq(this).is(":visible")) {
			var filter = jq(this).attr('data-filter-show');
			if (filter == 'subject') {
				jq('#' + filter + 'Div').find('a').hide();
			}
			jq('#' + filter + 'Div').find('a').removeClass('slct_chip');
			jq('#' + filter + 'Hidden').val('');
		}
	});
	var filterOpenType = jq('#filterOpenType').val();
	jq('.switch input, .coa_cont input').each(function(index){
		if (jq(this).hasClass('slct_chip') && jq('.switch, .coa_cont').is(":visible")) {
		var filter = jq(this).attr('data-filter-show');
		jq('#' + filter).removeClass('slct_chip');
		jq('#'+filter).prop('checked', false);
		jq('#' + filter + 'Hidden').val('');
		}
	});
});

jq('body').on('click','[data-filter-option]' ,function(event){
	  var filterType = jq(this).attr('data-filter-option');
	  if(filterType == 'clearAllFilter'){
			jq.fn.urlFormation('C');
		}else if(filterType != 'yourGrades'){	
			if(filterType == 'universityFilter'){
				jq.fn.getInstituionList(this,filterType);
			}
			if(filterType != 'universityFilter'){
				jq('#clr_fltr, #sidenav, #'+filterType).show();
				var root = document.getElementsByTagName( 'html')[0];   
			    root.className = 'scrl_dis';
				jq('#bottom_button').hide();
				jq.fn.setHeightForFiltBox();
				jq('#filterOpenType').val(filterType);
			}
			
		  }
	  if(jq(this).hasClass('btnbg')){
		  jq('#bottom_button').show();
		  jq.fn.setHeightForFiltBox();
	  }
	});
jq('body').on('click', '[data-filter-show]', function(event){
	var filter = jq(this).attr('data-filter-show');
	if(filter == 'employmentRate' || filter == 'campusType'|| filter == 'qualification'|| filter == 'studyMode' || filter == 'region'){
      if(jq(this).hasClass('slct_chip')){
        jq('#'+filter+'Div').find('a').removeClass('slct_chip');
		jq('#' + filter + 'Hidden').val('');
	  }else{
	    jq('#'+filter+'Div').find('a').removeClass('slct_chip');
        jq(this).addClass('slct_chip') ;
	  }	
	}else{
	  jq(this).toggleClass('slct_chip','');	
	}
	jq('#bottom_button').show();
	var value = "";
	var uniName = "";
	if(filter == 'russellGroup') {
		if(jq('#'+filter).attr('class') == 'slct_chip') {jq('#'+filter).attr('data-'+filter+'-value', 'Yes');}
		if(jq("#"+filter).prop('checked')) {
			jq('#' + filter + 'Hidden').val('Yes');
		} else {
			jq('#' + filter + 'Hidden').val('');			
		}		
	} else {
	value = jq('#'+filter+'Div').find('.slct_chip').map(function(index){
		uniName = jq(this).attr('data-'+filter+'-ga-value');
		return jq(this).attr('data-'+filter+'-value');
	  }).get().join(',');
    jq('#'+filter+'Hidden').val(value);
	}
	jq.fn.setHeightForFiltBox();
    if(filter == 'university') {
    	jq.fn.urlFormation();
    }
});

jq.fn.getInstituionList = function(thisObj, filterType){
	  var inputAttr = {};
	  jq('#loaderTopNavImg').show(); 
	  var url = '/search-result/get-institution-ajax.html'
	  jq.ajax({
				type : "POST",
				url : contextPath + url,
				cache : true,
				success : function(result) {				
					jq('#ajax_listOfOptions_university').html(result);
					jq('#loaderTopNavImg').hide(); 
					jq('#ajaxUniversityName').val('');
					jq('#clr_fltr, #sidenav, #'+filterType).show();
					var root = document.getElementsByTagName( 'html')[0];   
				    root.className = 'scrl_dis';
					jq('#bottom_button').hide();
					jq.fn.setHeightForFiltBox();
					jq('#filterOpenType').val(filterType);
					jq('.ajax_ul').slimscroll({});	
					if(jq(thisObj).hasClass('btnbg')){
						  jq('#bottom_button').show();
						  jq.fn.setHeightForFiltBox();
					  }		
				}
		  });
		}

jq('body').on('keyup', '#ajaxUniversityName', function(event) {
	var value = jq(this).val().trim().toLowerCase();
	value = value.replace(reg, " ");
	value = value.trim();
    if (!(jq.fn.notBlankOrNull(value))) {
		  jq('#ajaxUniDropDown').hide();
		  jq('#ajaxUniDropDown').parent('.slimScrollDiv').hide();
        return;
    }
	  if (value.length > 0) {
		var flag = "N";
		jq('#ajaxUniDropDown').show();
		  jq('#ajaxUniDropDown').parent('.slimScrollDiv').show();
		jq("#ajaxUniDropDown li a").each(function() {
		  var listText = jq(this).text().toLowerCase();
		  var listId = jq(this).parent('li').attr('id');
		  var indexPos = listText.indexOf(value);
		  var prexfix = '<u>';
		  var suffix = '</u>';
		  if (indexPos >= 0 ) {
			var endPos = indexPos + value.length + prexfix.length;
			var texWithunderLine = jq(this).text().substring(0, indexPos) + prexfix + jq(this).text().substring(indexPos);
			texWithunderLine = texWithunderLine.substring(0, endPos) + suffix + texWithunderLine.substring(endPos);
			jq(this).html(texWithunderLine);
			jq('#' + listId).show();
		  } else {
			  jq("#"+listId).hide();
		  }
	    });
	  } else {
		  jq('#ajaxUniDropDown').hide();
		  jq('#ajaxUniDropDown').parent('.slimScrollDiv').hide();
	  }
});

jq('body').on('click', '[data-sort-show]', function(event) {
	if (jq('#filterOpenType').val() != 'yourGrades') {
		var sortBy = jq(this).attr('data-sort-show');
		jq('#sortHidden').val(jq(this).attr('data-' + sortBy + '-value'));
		jq.fn.urlFormation();
	}
});

//Ad-blocker enable method
function adBlockerEnable(adUrl){
  if(isNotNullAndUndex(adUrl)){
	window.open(adUrl,'_blank');
  }
}

/*Chaing process ga logging method*/
function sponsoredListGALogging(institutionId){
  var boostedInstId = !isBlankOrNullString(adv("#boostedInstId").val()) ? adv("#boostedInstId").val() : "";
  var sponsoredInstId = !isBlankOrNullString(adv("#sponsoredInstId").val()) ? adv("#sponsoredInstId").val() : "";
  
  var sponsoredTypeInstitutionId;
  var sponsoreTypeFlag;
  if(institutionId == sponsoredInstId){
	sponsoreTypeFlag = "sponsored";
	sponsoredTypeInstitutionId = sponsoredInstId;
  }else if(institutionId == boostedInstId){
	sponsoreTypeFlag = "manual";
	sponsoredTypeInstitutionId = boostedInstId;
  }else{
	sponsoreTypeFlag = "none";
	sponsoredTypeInstitutionId = "";
  }
  var chainProcessSessionURL = "/degrees/chain-process-ga-session.html?sponsoreTypeFlag="+sponsoreTypeFlag+"&sponsoredTypeInstitutionId="+sponsoredTypeInstitutionId;
  jq.post(chainProcessSessionURL, function(data) {	
  });
  
  var fieldObject;
  if(institutionId == boostedInstId){
	fieldObject = "Manual";  
  }else if(institutionId == sponsoredInstId){
	fieldObject = "Sponsored";    
  }else if(institutionId != boostedInstId && institutionId != sponsoredInstId){
	fieldObject = "None";   
  }
  ga('set', 'dimension17', fieldObject);  
}
/*Chaing process ga logging method End*/