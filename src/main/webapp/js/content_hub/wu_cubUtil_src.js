var $jcub = jQuery.noConflict();
var vidpct = ['','10%','20%','30%','40%','50%','60%','70%','80%','90%','100%'];
var ua = window.navigator.userAgent;
var iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
var webkit = !!ua.match(/WebKit/i);
var iOSSafari = iOS && webkit && !ua.match(/CriOS/i);
var iOSIphone = !!ua.match(/iPhone/i);
var iOSSafariIphone = iOSIphone && webkit && !ua.match(/CriOS/i);
var lastScrollTop = 0;
var upOrDownScrollFlag = "down";
var capusLoaded = "loading";
var videoRanMap = {};
var sectionWiseLoggingMap = {section:"con-for-nav-0", section0_SectionCont:"N", section0_SectionVideo:"N", section0_ImgImp:"N", section0_ContImp:"N", section0_SectionDimension: "N"};
var videoStatusMap = {};
var isMac = navigator.platform.toUpperCase().indexOf('MAC')>=0;
var userAgent, ieReg, isIE;
var alphaNumberic = /(([A-Za-z].*[0-9])|([0-9].*[A-Za-z]))/;
var specialCharacter = /[^A-Za-z \d]/;
var letters = /^[\-a-zA-Z0-9\s\.\,\:\[\]\`\;\~\''\""\-\@\#\?\%\/\=\&\^\{\}\<\>\$\*\|\!\(\)\-\+\_\\]*$/;
var nonEngLanguageErrorMsg = "Please double check your message and remove any special characters";
userAgent = window.navigator.userAgent;
ieReg = /msie|Trident.*rv[ :]*11\./gi;
isIE = ieReg.test(userAgent);
var contentHubErrorMsg = { 
  success_post_code : "Got it!"
}
var map = [];
//
function $$D(id){
  return document.getElementById(id);
}
//
var ua = navigator.userAgent.toLowerCase(); 
var isSafari = ua.indexOf('safari') != -1 ? ua.indexOf('chrome') > -1 ? false : true : false;
//
function getDeviceWidth() { return (window.innerWidth > 0) ? window.innerWidth : screen.width; }
//
var media;
//
function isBlankOrNullValue(o) {
  var flag = true;
  if((o) && ((o.value).trim() != "") && ((o.value).trim().length > 0)) {
    flag = false;
  }
  return flag;
}
//
function isBlankOrNullInnerHtml(o) {
  var flag = true;
  if((o) && ((o.innerHTML).trim() != "") && ((o.innerHTML).trim().length > 0)) {
    flag = false;
  }
  return flag;
}

function isNotNullAndUndex(value) {
  return (value != '' && value != undefined && value != null && value != 'null');
}

//onclick video play and pause 
function playAndPause(videoId, e, elementType){
  if(elementType != 'overlay_div') {
	e.preventDefault();
  }
  if($$D(videoId) && (!isMac || !isSafari || elementType == 'overlay_div')){  
    var videoIdElement = $jcub('#'+ videoId).get(0);
    var videoBgId = 'bg_hero';
	if(videoIdElement.paused){
      if($$D('bg_'+videoId) && $$D('wrapper_'+videoId)){//for section hiding thumbnail and showing video on click pre loaded play icon
        $$D('wrapper_'+videoId).style.display = "block";
        $$D('bg_'+videoId).style.display = "none";
		videoBgId = 'bg_'+videoId;
		if(!iOSSafari && videoIdElement.currentTime == 0) {
			videoIdElement.muted = true;
		}
		var id = videoId.replace('video_section_', '');
		id = Number(id) + 1;
		if(sectionWiseLoggingMap['section'+id+'_SectionVideo'] != 'Y') {
			sectVidDbStatsLog(videoIdElement.getAttribute('data-media-id'), 'B', id);
			sectionWiseLoggingMap['section'+id+'_SectionVideo'] = "Y";
		}
      }else if ('background-video' == videoId){//for hero pod
        $$D('wapper_hero_video').style.display = "block";							
        $$D('bg_hero').style.display = "none";
		videoBgId = 'bg_hero';
		if(sectionWiseLoggingMap['section0_SectionCont'] != 'Y') {
			sectionsDBStatsLog(0);
			sectionWiseLoggingMap['section0_SectionCont'] = "Y";
		}
      }else if('reviewVideo' == videoId){//for review pod
        $$D('wrapper_review').style.display = "block";
        $$D('bg_review').style.display = "none";
		videoBgId = 'bg_review';
      }else if($$D('bg_'+videoId)) {//for gallery
        $$D('bg_'+videoId).style.display = "none";
		$$D('bg_'+videoId).style.height = '0px';
        $$D(videoId).style.display = "block";
		videoBgId = 'bg_'+videoId;
      }
	  if($$D(videoBgId).style.display != "block") {
		  videoIdElement.play();
		  videoStatusMapUpdate(videoIdElement, 'N', 'N', '');
	  }
    }else{
      videoIdElement.pause();
	  videoIdElement.addEventListener("pause", function(e){ videoStatusMapUpdate(videoIdElement, 'N', 'Y', '');e.preventDefault();});
    }
  }
}
//this function to show and hide play icon for heropod and gallery pod
function hideAndShowPlayIcon(podSectId, action){
  if(podSectId=='con-for-nav-0'){
    if('play' == action){    
      $jcub("#"+podSectId+" .play-icon img").addClass("animate_icon");
    }else if('pause' == action){    
      //$jcub("#"+podSectId+" .play-icon img").removeClass("animate_icon");
    }
  }else{
    if('play' == action){    
      $jcub("#"+ podSectId).hide();
	  $jcub('#carousel').hide();
    }else if('pause' == action){    
      $jcub("#"+ podSectId).show();
	  $jcub('#carousel').show();
    }   
  }
}
//on click pause and play in gallery pod
function onclickPlayAndpauseGalleryVideo(sliderUlId, e, elementType){
  if(elementType != 'overlay_div') {
	e.preventDefault();
  }
  if(!isMac || !isSafari || elementType == 'overlay_div') {
  $jcub('#'+sliderUlId).find('li').each(function(){
    if($jcub(this).has('video')&& $jcub(this).attr('class') && ($jcub(this).attr('class')).indexOf('flex-active-slide') > -1){                 
      var videoElementId = $jcub(this).find('video').attr('id');      
      if($$D(videoElementId)){    
        var video = $jcub('#'+ videoElementId).get(0);
        if(video.paused){          
    if($$D('bg_'+video.id).style.display != "block") {
      video.play();
      videoStatusMapUpdate(video, 'N', 'N', '');		  
    }
        }else{
          video.pause();
      video.addEventListener("pause", function(e){ videoStatusMapUpdate(video, 'N', 'Y', '');e.preventDefault();});
        }
      }      
    }
   });
  }
}
//
jQuery(document).ready(function($) {
    
	media = $jcub('video').not("[autoplay='autoplay']");
	
    $jcub(".next").click(function() {
        $jcub('html,body').animate({ scrollTop:$jcub('#con-for-nav-1').offset().top}, 'slow');
        $jcub('#nav-1').addClass('active')
    });
	// For scroll to top in gallery pod while clicking flex nav arrow and slides in bottom strip 
	  $(document).on('click', '#slider .flex-direction-nav li a, #carousel .slides li', function() {
	    var offTop = $("#con-for-nav-36").offset().top;
	    $("html,body").animate({
	        scrollTop: offTop
	    }, 800);
	  });  
	  //
	$jcub(window).on("load", function() {
        contentHubVideo();
		//setCampusImageVariation();
		faceBookScript();
    	// To quick lazy load of image in sections
        var transImg2 = $jcub('#con-for-nav-1 img');
        if(transImg2.length > 0){
          for(var i=0; i < transImg2.length; i++){
            var transImgSrc2 = transImg2[i].getAttribute("src");
            if((transImgSrc2.indexOf('/img_px') > -1)) {
              transImg2[i].src = getDeviceSpecificDataSrc(transImg2[i]);
            }
          }
        }
		//    
		if($$D('imageCampus1')) {
			$$D('imageCampus1').onload = function () {
			   setCampusHeight();
			}
		}		
    //To change the preload none to metadata for the iphone
    if(iOSSafariIphone){   
      $jcub('video').each(function(){        
        $jcub("#"+this.id).attr("preload", "metadata");         
      });      
    }
    //
	});
	$jcub(window).on("resize", function() {
		contentHubVideo();
		//setCampusImageVariation();
	});
	
	// compare
	$jcub( ".ratingcnt + .com_sec .cmp_hov").hover(function() {
		$jcub(".ratingcnt + .com_sec .cmp_hov i").toggleClass("fa-heart");
	});
	$jcub( ".com_sec.act .cmp_hov").hover(function() {
		$jcub(".com_sec.act .cmp_hov i").addClass("fa-heart");
	});
	$jcub( ".com_sec .cmp_hov" ).click(function() {
		$jcub(".ratingcnt + .com_sec .cmp_hov i").removeClass("fa-heart");
	});
	//
	
    //Quick link Skip links
    $jcub(".atz li").each(function() {
        $jcub(this).click(function() {
            $jcub(".atz li").removeClass('active');
            var elementClick = $jcub(this).attr("id");
			var id = elementClick.replace('nav-', '');
            var offTop = $jcub("#con-for-" + elementClick).offset().top;
			//
			var tolerancePixel = 50;
			if(id >= 31) {
				tolerancePixel = -79;
			}
            $jcub("html,body").animate({
                scrollTop: offTop + tolerancePixel
            }, 500, function() {$jcub('#'+elementClick).addClass('active');} );
			//
			if(sectionWiseLoggingMap['section'] != 'con-for-' + elementClick) {
				sectionWiseLoggingMap['section'] = 'con-for-nav-'+id;
				/*
				sectionWiseLoggingMap['section'] = 'con-for-nav-'+id;
				sectionWiseLoggingMap['section'+id+'_SectionCont'] = "N";
				sectionWiseLoggingMap['section'+id+'_SectionVideo'] = "N";
				sectionWiseLoggingMap['section'+id+'_ImgImp'] = "N";
				sectionWiseLoggingMap['section'+id+'_ContImp'] = "N";
				*/
				if((id == 33 || id == 36) && sectionWiseLoggingMap['section'+id+'_SectionCont'] != 'Y') {
					sectionsDBStatsLog(id);
					sectionWiseLoggingMap['section'+id+'_SectionCont'] = "Y";
				}
				//console.log('ga > dimension12 > '+$$D('sectionName_'+id).value);
				ga('set', 'dimension12', ($$D('sectionName_'+id).value).toLowerCase());
				sectionWiseLoggingMap['section'+id+'_SectionDimension'] = "Y";
				if(sectionWiseLoggingMap['section'+id+'_ContImp'] != 'Y') {
					//console.log('ga > sectionafter2seconds  > Content Impression > '+$$D('sectionName_'+id).value+' > '+$$D('collegeNameGa').value);
					GAInteractionEventTracking('sectionafter2seconds', 'Content Impression', ($$D('sectionName_'+id).value).toLowerCase(), $$D('collegeNameGa').value);
					sectionWiseLoggingMap['section'+id+'_ContImp'] = "Y";
				}
			}
        });
    });
	
    $jcub(".rghtcont-list li").each(function() {
        $jcub(this).click(function(e) {
            var elementClick1 = $jcub(this).attr("data");
            var eltop = $jcub("#con-for-" + elementClick1);
            var offTop = eltop.offset().top;
            $jcub("html,body").animate({
                scrollTop: offTop
            }, 800);
            var id = elementClick1.replace('nav-', '');
			if(sectionWiseLoggingMap['section'] != 'con-for-' + elementClick1) {
				sectionWiseLoggingMap['section'] = 'con-for-nav-'+id;
				/*
				sectionWiseLoggingMap['section'+id+'_SectionCont'] = "N";
				sectionWiseLoggingMap['section'+id+'_SectionVideo'] = "N";
				sectionWiseLoggingMap['section'+id+'_ImgImp'] = "N";
				sectionWiseLoggingMap['section'+id+'_ContImp'] = "N";
				*/
				if((id == 33 || id == 36) && sectionWiseLoggingMap['section'+id+'_SectionCont'] != 'Y') {
					sectionsDBStatsLog(id);
					sectionWiseLoggingMap['section'+id+'_SectionCont'] = "Y";
				}
				//console.log('ga > dimension12 > '+$$D('sectionName_'+id).value);
				ga('set', 'dimension12', ($$D('sectionName_'+id).value).toLowerCase());
				sectionWiseLoggingMap['section'+id+'_SectionDimension'] = "Y";
				if(sectionWiseLoggingMap['section'+id+'_ContImp'] != 'Y') {
					//console.log('ga > sectionafter2seconds  > Content Impression > '+$$D('sectionName_'+id).value+' > '+$$D('collegeNameGa').value);
					GAInteractionEventTracking('sectionafter2seconds', 'Content Impression', ($$D('sectionName_'+id).value).toLowerCase(), $$D('collegeNameGa').value);
					sectionWiseLoggingMap['section'+id+'_ContImp'] = "Y";
				}
			}
            $jcub('.ibtn_sec').removeClass("ibtn_fixed");
            $jcub('html,body').removeClass('scrl_dis');
            $jcub('.ibtn_sec,.ibtn_sec .ibtn_rht,.ibtn_sec .ibtn_lft,.uni_mnu').removeAttr("style");
             e.preventDefault();   
        });
    });
    //Quick link Skip links
	
    //Back to top    
    $jcub('#back-top-div a').on('click', function(e) {
        e.preventDefault();
        $jcub('html,body').animate({
            scrollTop: 0
        }, 700);
    });
	
    //Sticky Pod
    $jcub('.nxt_stps li:nth-child(2) a').on('click', function(e) {
        e.preventDefault();
        $jcub('.ibtn_sec').addClass("ibtn_fixed");
        $jcub('.ibtn_sec .ibtn_rht').show();
        $jcub('.ibtn_sec .ibtn_lft').hide();
    });
    $jcub('.nxt_stps li.sti_mnu a').on('click', function(e) {
        e.preventDefault();
        $jcub('.uni_mnu').show();
        //$jcub('html,body').addClass('scrl_dis');
        $jcub('.ibtn_sec .ibtn_lft,.ibtn_sec').hide();
    });
    $jcub('.nxt_cls a').on('click', function(e) {
        e.preventDefault();
        $jcub('.ibtn_sec').removeClass("ibtn_fixed");
        $jcub('html,body').removeClass('scrl_dis');
        $jcub('.ibtn_sec,.ibtn_sec .ibtn_rht,.ibtn_sec .ibtn_lft,.uni_mnu').removeAttr("style");
        $jcub('.ibtn_sec').addClass('ibtnsec');
    });
	//

    $jcub(window).scroll(function(){
        //Back to top
     	var thumb_item = $jcub(window).width();
        if(thumb_item < 769){
            var faqhght = $jcub(window).height();
            var scrval = $jcub(window).scrollTop();
            if (faqhght < scrval) {
                $jcub("#back-top-div").css("display", "block");
            } else {
                $jcub("#back-top-div").css("display", "none");
            }
        }
		//
		var st = $jcub(this).scrollTop();
		if (st > lastScrollTop){
		   upOrDownScrollFlag = "down";
		} else {
		   upOrDownScrollFlag = "up";
		}
		lastScrollTop = st;
		//
		//setCampusHeight();
		//
		var deviceWidth =  getDeviceWidth();
		if((deviceWidth >= 320 && deviceWidth <= 480)) {
			clearSectionWiseLoggingMap();
			sectionsStatsLogOnView();
			otherSectionsImageAndDBstatsLog();
			playOrPauseVideoOnScroll();
		}
	});

    $jcub(window).on("load scroll", function() {
        // Sticky menu
	var scnWidth = $jcub(window).width();
        var contHght = $jcub("#con-for-nav-0").outerHeight();
        var scrTop = $jcub(window).scrollTop();
        if(scnWidth > 767){
			if (contHght < scrTop) {
				$jcub(".ibtn_sec").addClass("ibtnsec");
			} else {
				$jcub(".ibtn_sec").removeClass("ibtnsec");
			}
        }
        // Sticky menu
    });
    
    $jcub('#slider1 .flex-direction-nav li, #slider1 .flex-control-nav').click(function() {
        $jcub('body,html').animate({
            scrollTop: $jcub('#con-for-nav-31').offset().top
        }, 1200);
    });
	
    if(screen.width >= 768){    
      $jcub(".deg_tab ul li a").click(function(){
        $jcub(".deg_tab ul li:nth-child(2) a, .deg_tab ul li:nth-child(3) a").toggleClass("op_act");
      }); 
    }else{
      $jcub(".deg_tab ul li:nth-child(2) a, .deg_tab ul li:nth-child(3) a").removeClass("op_act");   
    }
});
//
function contentHubVideo(){
	var videos = document.getElementsByTagName("video");
	for(var i=0; i < videos.length; i++) {
		var width = "";
		var elementsHgtWdh = "";
		if(videos[i].className == 'sectionpod') {
			width = $jcub(".col_lft").innerWidth();
			elementsHgtWdh = ".col_lft .vid-content,.col_lft .vid-content video";
		} else if(videos[i].className == 'rightpod') {
			width = $jcub("#col_rgt").innerWidth();
			elementsHgtWdh = ".col_rgt .vid-content,.col_rgt .vid-content video";
		} else if(videos[i].className == 'heropod') {
			width = $jcub(window).innerWidth();
			elementsHgtWdh = ".hm_vid_cont,.hm_vid_cont.vid-content video";
		} else if(videos[i].className == 'flexpod') {
			width = $jcub(window).innerWidth();
			elementsHgtWdh = ".flex-viewport,.flex-viewport .slides video";
		}
		var vidw = videos[i].videoWidth;
		var vidh = videos[i].videoHeight;
		var vid_vari = vidh/vidw; 
		var video_dht = Math.round(width * vid_vari);
		//alert("width=>"+width+"height=>"+video_dht);
		if(videos[i].className != 'flexpod') {
			$jcub(elementsHgtWdh).css({
				  'width':'100%',
				  'height':video_dht+ 'px',
			});
		}
	}
}
//
function setCampusImageVariation(){
	var width = $jcub(window).innerWidth();
    var campWdth = $jcub(".tabimages img").outerWidth();
    var campHght = $jcub(".tabimages img").outerHeight();
    var campImg_vari = campHght/campWdth; 
    var campImg_hgt = Math.round(width * campImg_vari);
    $jcub(".ui-slider-tabs-content-container").css({
	      'width':'100%',
	      'height':campImg_hgt+ 'px',
	}); 
}
//
function videoStatusMapUpdate(videoElement, autopause, manualpause, unmute) {
	if(manualpause != '' && manualpause.trim().length != 0) { videoStatusMap[videoElement.id+'_manualpause'] = manualpause; }
	if(autopause != '' && autopause.trim().length != 0) { videoStatusMap[videoElement.id+'_autopause'] = autopause; }
	if(unmute != '' && unmute.trim().length != 0) { videoStatusMap[videoElement.id+'_unmute'] = unmute; }
}
//
function updateVideoMapOnPlayPause(videoElement, state, e) {
	if(state == 'pause') {
		videoStatusMapUpdate(videoElement, 'N', 'Y', '');
		e.preventDefault();
	} else {
		videoStatusMapUpdate(videoElement, 'N', 'N', '');
	}
}
//
function videoPlayOrPause(videoElement) {
	$jcub("#slider .flex-viewport video").each(function() {
		var vidElement = $jcub(this).get(0);
		if(!iOSSafari){
			vidElement.muted = true;
			videoStatusMapUpdate(vidElement, '', '', 'N');
		}
		if(videoStatusMap[vidElement.id+'_manualpause'] != 'Y' && !vidElement.paused) {
			vidElement.pause();
			vidElement.addEventListener("pause", function(e){ videoStatusMapUpdate(vidElement, 'Y', 'N', 'N');e.preventDefault();});
		}
		$jcub('#'+vidElement.id).prev().show();
	});
	if(videoElement != null) {
		if(!iOSSafari){
		      videoElement.muted = videoStatusMap[videoElement.id+'_unmute'] != 'Y' ? true : false;
			  if(videoStatusMap[videoElement.id+'_manualpause'] != 'Y') {
				videoElement.play();
				videoStatusMapUpdate(videoElement, 'N', 'N', '');
			  }
			  $$D('bg_'+videoElement.id).style.display = "none";
			  $$D(videoElement.id).style.display = "block";
			  $$D('bg_'+videoElement.id).style.height = '0px';
		}   
		videoPlayedDurationPercentage(videoElement);
			$jcub('#'+videoElement.id).prev().hide();
		}
}
//
function videoPlayedDurationPercentage(videoElement) {
	
	if(videoRanMap[videoElement.id+'_percentage'] == null) {
		videoRanMap[videoElement.id+'_percentage'] = "0%";
	}
	if(videoRanMap[videoElement.id+'_imp5sec'] == null) {
		videoRanMap[videoElement.id+'_imp5sec'] = "N";
	}
	if(videoRanMap[videoElement.id+'_complete'] == null) {
		videoRanMap[videoElement.id+'_complete'] = "N";
	}
	if(videoRanMap[videoElement.id+'_unmute'] == null) {
		videoRanMap[videoElement.id+'_unmute'] = "N";
	}
	var videoUnMuted = "N";	
	var videoTitle = videoElement.getAttribute('data-media-name');
	
	videoElement.ontimeupdate = function(){
		var curVidDrtn = videoElement.duration;
		var tenPcntSec = curVidDrtn/10;
		//
		if(this.currentTime >= 5 && videoRanMap[videoElement.id+'_imp5sec'] != "Y") {
			videoRanMap[videoElement.id+'_imp5sec'] = "Y";
			//console.log('videoafter5sec  > Video Impression View > '+$$D('collegeNameGa').value+' > '+videoTitle);
			GAInteractionEventTracking('videoafter5sec', 'Video Impression View', $$D('collegeNameGa').value, videoTitle);
		} 
		/*
		if(this.currentTime < 5){
			videoRanMap[videoElement.id+'_imp5sec'] = "N";
			videoRanMap[videoElement.id+'_percentage'] = "0%";
			videoRanMap[videoElement.id+'_complete'] = "N";
			videoRanMap[videoElement.id+'_unmute'] = "N";
		}
		*/
		//
		for(var cnt=1; cnt<=10; cnt++){
			if(this.currentTime >= tenPcntSec*cnt) {
				if(videoRanMap[videoElement.id+'_percentage'].indexOf(vidpct[cnt]) < 0) {
					videoRanMap[videoElement.id+'_percentage'] += ','+vidpct[cnt];
					//console.log('video10percentage  > Video Duration > '+$$D('collegeNameGa').value+'|'+videoTitle+' > '+vidpct[cnt]);
					GAInteractionEventTracking('video10percentage', 'Video Duration', $$D('collegeNameGa').value+'|'+videoTitle, vidpct[cnt]);
				}
			}
		}
		//
		if(this.currentTime == curVidDrtn && videoRanMap[videoElement.id+'_complete'] != "Y") {
			videoRanMap[videoElement.id+'_complete'] = "Y";
			//console.log('videocomplete  > Video Complete > '+$$D('collegeNameGa').value+' > '+videoTitle);
			GAInteractionEventTracking('videocomplete', 'Video Complete', $$D('collegeNameGa').value, videoTitle);
		}
	};
	videoElement.onvolumechange = function(event) {
		if(!videoElement.muted) {
			videoStatusMapUpdate(videoElement, '', '', 'Y');
		} else {
			videoStatusMapUpdate(videoElement, '', '', 'N');
		}
		event.preventDefault();
		if(!videoElement.muted && videoUnMuted != 'Y' && videoRanMap[videoElement.id+'_unmute'] != "Y") {
			videoUnMuted = "Y";
			videoRanMap[videoElement.id+'_unmute'] = "Y";
			//console.log('videounmute  > Video View > '+$$D('collegeNameGa').value+' > '+videoTitle);
			GAInteractionEventTracking('videounmute', 'Video View', $$D('collegeNameGa').value, videoTitle);
		}
		
	};
}
//
function playOrPauseVideoOnScroll(){
	var srlTop = $jcub(window).scrollTop();
	$jcub(media).each(function(index, el) {
		var yTopMedia = $jcub(this).offset().top;
		var videoElement = $jcub(this).get(0);
		var deviceWidth =  getDeviceWidth();
		var timout = 0;
		var heroPod = false;
		if($jcub('#'+videoElement.id).attr('class') && ($jcub('#'+videoElement.id).attr('class')).indexOf('heropod') > -1) {
			heroPod = true;
			var firstSectionvideo = $jcub('#videoSectionHolder_0 video')[0];
			if(firstSectionvideo != null && isScrolledIntoView($jcub('#videoSectionHolder_0'), true)) {
				heroPod = false;
			}
		}
		//
		var leftPod = false;
		if($jcub('#'+videoElement.id).attr('class') && ($jcub('#'+videoElement.id).attr('class')).indexOf('sectionpod') > -1) {
			leftPod = true;
			timout = (deviceWidth >= 320 && deviceWidth <= 480) ? 4000 : 0;
			//leftPodPlayAndPause();      
		}
		//
		var rigthPod = false;
		if($jcub('#'+videoElement.id).attr('class') && ($jcub('#'+videoElement.id).attr('class')).indexOf('rightpod') > -1) {
			rigthPod = true;
		}
		//
		var flexPod = false;
		if($jcub('#'+videoElement.id).attr('class') && ($jcub('#'+videoElement.id).attr('class')).indexOf('flexpod') > -1) {
			flexPod = true;
		}
		//
		if(flexPod) {
			var videoElementFlexPod = $jcub('#slider .flex-active-slide video')[0];
			if(videoElementFlexPod != null) {
				if(isScrolledIntoView($$D('con-for-nav-36'), false)) {					
					if(!iOSSafari){
						videoElementFlexPod.muted = videoStatusMap[videoElementFlexPod.id+'_unmute'] != 'Y' ? true : false;
						if(videoStatusMap[videoElementFlexPod.id+'_manualpause'] != 'Y') {
							videoElementFlexPod.play();
						}
						$$D('bg_'+videoElementFlexPod.id).style.display = "none";
						$$D(videoElementFlexPod.id).style.display = "block";
						$$D('bg_'+videoElementFlexPod.id).style.height = '0px';
					}
					videoPlayedDurationPercentage(videoElementFlexPod);
				} else {
					if(!iOSSafari){
						videoElementFlexPod.muted = true;
						videoStatusMapUpdate(videoElementFlexPod, '', '', 'N');
					}
					if(videoStatusMap[videoElementFlexPod.id+'_manualpause'] != 'Y' && !videoElementFlexPod.paused) {
						videoElementFlexPod.pause();
						videoElementFlexPod.addEventListener("pause", function(e){videoStatusMapUpdate(videoElementFlexPod, 'Y', 'N', 'N');e.preventDefault();});
					}
				}
			}
		}
		//
		if(isScrolledIntoView(videoElement, false)) {
			setTimeout(function(){
				if(!flexPod) {          
					if((leftPod && (deviceWidth >= 320 && deviceWidth <= 480)) || (rigthPod && isScrolledIntoView($$D('con-for-nav-33'), false)) || heroPod){
						if(!iOSSafari){
							//videoElement.muted = (leftPod && !vidElement.muted ? false : true);
							videoElement.muted = videoStatusMap[videoElement.id+'_unmute'] != 'Y' ? true : false;
							if(videoStatusMap[videoElement.id+'_manualpause'] != 'Y') {
								videoElement.play();
							}
						}
						/*
						$jcub('.sectionpod').each(function() {
							playAndPauseSectVideo($jcub(this).get(0), 'pause');
						});
						*/
						if(leftPod) {
							if(!iOSSafari){
								$$D('bg_'+videoElement.id).style.display = "none";
								$$D('wrapper_'+videoElement.id).style.display = "block";
								var contNo = videoElement.id.replace('video_section_', '');
								if(sectionWiseLoggingMap['section'+(Number(contNo)+1)+'_SectionVideo'] != "Y") {
									sectVidDbStatsLog(videoElement.getAttribute('data-media-id'), 'B', contNo); // DB stats mobile video
									sectionWiseLoggingMap['section'+(Number(contNo)+1)+'_SectionVideo'] = "Y";
								}
							}
						}
						if(heroPod) {              
							$jcub('.hero-section .overlay').css('display','block');              
							if(!iOSSafari){
								$jcub("#con-for-nav-0 .play-icon img").addClass("animate_icon");
								$$D('wapper_hero_video').style.display = "block";							
								$$D('bg_hero').style.display = "none";
								if(sectionWiseLoggingMap['section0_SectionCont'] != 'Y') {
									sectionsDBStatsLog(0);
									sectionWiseLoggingMap['section0_SectionCont'] = "Y";
								}
							}
							//
							//Hero video
							var setVidHght = $jcub('.hero_img').outerHeight();
							$jcub('.hero-section .home-header__player').css('height',setVidHght)
						}				
						if(rigthPod && isScrolledIntoView($jcub('#con-for-nav-33'), false)) {
							$jcub('#'+videoElement.id).prev().hide();
							if(!iOSSafari){
								$$D('wrapper_review').style.display = "block";
								$$D('bg_review').style.display = "none";
							}
						}
						videoPlayedDurationPercentage(videoElement);
					}					
				}
			}, timout);
		} else if(!isScrolledIntoView(videoElement, false)) {
			if(!flexPod) {
				if((leftPod && (deviceWidth >= 320 && deviceWidth <= 480)) || rigthPod || heroPod){
					if(!iOSSafari){
						videoElement.muted = true;
						videoStatusMapUpdate(videoElement, '', '', 'N');
					}
					if(videoStatusMap[videoElement.id+'_manualpause'] != 'Y' && !videoElement.paused) {
						videoElement.pause();
						videoElement.addEventListener("pause", function(e){videoStatusMapUpdate(videoElement, 'Y', 'N', 'N');e.preventDefault();});
					}
					if(heroPod) {
						$jcub('.hero-section .overlay').css('display','block');
						//$jcub('.hero-section .overlay,.hero-section .hero_img').css('display','block');
						$jcub(".hero-section .play-icon img").removeClass("animate_icon");
						$jcub("#con-for-nav-0 .play-icon img").show();
						$jcub('.hero-section .home-header__player').removeAttr("style");
					}
					if(leftPod) {
						$jcub('#'+videoElement.id).parent().prev().show();		
					}
					if(rigthPod) {
						$jcub('#'+videoElement.id).prev().show();	
					}
				}
			}
		}
	});
}
//
function playAndPauseSectVideo(vidElement, action){
  if('play' == action){    
	  if(!iOSSafari){
      //vidElement.muted = (!vidElement.muted ? false : true); //section pod mute
	  vidElement.muted = videoStatusMap[vidElement.id+'_unmute'] != 'Y' ? true : false;
	  if(videoStatusMap[vidElement.id+'_manualpause'] != 'Y') {
		vidElement.play();
		$jcub('#'+vidElement.id).parent().prev().hide();
	  }
    }
  }else if('pause' == action){
    if(!iOSSafari){
		vidElement.muted = true;
		videoStatusMapUpdate(vidElement, '', '', 'N');
	}
    if(videoStatusMap[vidElement.id+'_manualpause'] != 'Y' && !vidElement.paused) {
		vidElement.pause();
		setTimeout(function() {
			videoStatusMapUpdate(vidElement, 'Y', 'N', 'N');
		}, 100);		
	}
    $jcub('#'+vidElement.id).parent().prev().show();
  }
}
function hideShowIconOnClickPlayAndPauseVideo(vidId, action){
  if('play' == action){    
	  $jcub('#'+vidId).parent().prev().hide();
  }else if('pause' == action){    
    $jcub('#'+vidId).parent().prev().show();
  }
}
//
function sectVidDbStatsLog(videoId, videoType, contNo){
   var offsetlft = "";
   var sectSubOrderItemId = $$D(contNo+'_sectSubOrderItemId_'+videoId) ? $$D(contNo+'_sectSubOrderItemId_'+videoId).value : "";
   if(!$$D(contNo+'_sectSubOrderItemId_'+videoId)){
     sectSubOrderItemId = $$D('subOrderItemId').value;
   }
   //console.log('section video stats > ' +videoId);
   //
   if($$("theImages") !=null){
     offsetlft= "&offsetlft=0";
   }   
   var ajax=new sack();
   var deviceWidth =  getDeviceWidth();
   var url = contextPath+'/vids-video.html?vrid='+videoId+"&cid="+$$D('collegeId').value+"&refererUrl="+$$D("refferalUrl").value+"&videoType="+videoType+offsetlft+"&pageName=contentHub"+"&subOrderItemId="+sectSubOrderItemId+"&networkId="+$$D('networkId').value+'&screenwidth='+deviceWidth;   
   ajax.requestFile = url;	
   ajax.onCompletion = function(){};
   ajax.runAJAX();
   //
}
//
function imageImpressionViewOnGA(position) {		
  //console.log('ga > imageimpression  > Image Impression > '+$$D('collegeNameGa').value+' > '+position);
  GAInteractionEventTracking('imageimpression', 'Image Impression', $$D('collegeNameGa').value, position);
}
//
function sectionsDBStatsLog(contNo) {
	var collegeId = $$D("collegeId").value;
	var networkId = $$D("networkId").value;
	var sectionProfileId = $$D('sectionProfileId_'+contNo) ? $$D('sectionProfileId_'+contNo).value : "";
	var sectionName = $$D('sectionName_'+contNo) ? $$D('sectionName_'+contNo).value : "";
	var sectMyhcProfileId = $$D('sectMyhcProfileId_'+contNo) ? $$D('sectMyhcProfileId_'+contNo).value : "";
  if(!($$D('sectMyhcProfileId_'+contNo))){
     sectMyhcProfileId = $$D("myhcProfileId").value;
  }
  //console.log('DB STATS (ga) > '+collegeId+' > '+networkId+' > '+sectionProfileId+' > '+sectionName);
	contentHubProfSecStatsLog(collegeId, networkId, sectionProfileId, sectionName,sectMyhcProfileId);
}
//
function otherSectionCustomDimensionsLogs(i, currentSection, sectionName) {
	setTimeout(function(){
		if($$D('sectionName_'+i) && sectionName == currentSection) {
			if(sectionWiseLoggingMap['section'+i+'_SectionDimension'] != "Y") {
				//console.log('ga > dimension12 > '+sectionName);
				ga('set', 'dimension12', (sectionName).toLowerCase());
				sectionWiseLoggingMap['section'+i+'_SectionDimension'] = "Y";
			}
			//
			if(sectionWiseLoggingMap['section'+i+'_ContImp'] != "Y") {
				//console.log('ga > sectionafter2seconds  > Content Impression > '+sectionName+' > '+$$D('collegeNameGa').value);
				GAInteractionEventTracking('sectionafter2seconds', 'Content Impression', sectionName.toLowerCase(), $$D('collegeNameGa').value);
				sectionWiseLoggingMap['section'+i+'_ContImp'] = "Y";
			}
		}
	}, 2000);
}
//
function otherSectionsImageAndDBstatsLog() {
	var sections = $jcub('section');
	for(var i=0; i < sections.length; i++) {
		var id = $jcub(sections[i]).attr('id');
		if($jcub(sections[i]).length > 0) {
			//
			if(id != null && id != 'undefined' && id != '') {
				var contNo = id.replace('con-for-nav-', '');
				//
				if(contNo == 0 || contNo >= 31) {
					if(isScrolledIntoView(sections[i], false) && sectionWiseLoggingMap['section'] == id) {
						$$D('currentSection').value = $$D('sectionName_'+contNo).value;
						otherSectionCustomDimensionsLogs(contNo, $$D('sectionName_'+contNo).value, $$D('currentSection').value);
					}
				}
				//
				if(id.indexOf('33') > -1 || id.indexOf('36') > -1) {
					if(isScrolledIntoView(sections[i], false) && sectionWiseLoggingMap['section'] == id) {
						if(sectionWiseLoggingMap['section'+contNo+'_SectionCont'] != "Y") {
							sectionsDBStatsLog(contNo);
							sectionWiseLoggingMap['section'+contNo+'_SectionCont'] = "Y";
						}
					}
				}
				//
				if(id.indexOf('34') > -1) { // campus
					var imgSrcCampus = $jcub('#'+id+' #mySliderTabs .ui-slider-tabs-content-container .selected img[data-media-name]')[0];
					if(imgSrcCampus != null && isScrolledIntoView(imgSrcCampus, false)) {
						//console.log('imgSrcCampus > '+imgSrcCampus.getAttribute('data-media-name'));
						if(sectionWiseLoggingMap['section'+imgSrcCampus.id+'_ImgImp'] != "Y") {
							imageImpressionViewOnGA(imgSrcCampus.getAttribute('data-media-name'));
							sectionWiseLoggingMap['section'+imgSrcCampus.id+'_ImgImp'] = "Y";
						}
					}
				} 
				if(id.indexOf('36') > -1) { // Gallery				
					var imgSrcGallery = $jcub('#'+id+' #slider .flex-active-slide img[data-media-name]')[0];
					if(imgSrcGallery != null) {
						if(isScrolledIntoView(imgSrcGallery, false)) {
							//console.log('imgSrcGallery > '+imgSrcGallery.getAttribute('data-media-name'));
							if(sectionWiseLoggingMap['section'+imgSrcGallery.id+'_ImgImp'] != "Y") {
								imageImpressionViewOnGA(imgSrcGallery.getAttribute('data-media-name'));
								sectionWiseLoggingMap['section'+imgSrcGallery.id+'_ImgImp'] = "Y";
							}
						}
					}
				}
				//
			}
			//
		}
	}
}
//
function lazyLoadingCub() {
	//
	$jcub(window).on("scroll orientationchange", function(e) {
		if(e.type == 'orientationchange') {
			var imgSrcList = document.getElementsByTagName('img');
			//
			for(var i = 0; i < imgSrcList.length; i++){
				var imgLoadType = imgSrcList[i].getAttribute("data-img-load-type");
				if(imgLoadType != null && imgLoadType == 'lazyLoad') {
					imgSrcList[i].src = imgSrcList[i].getAttribute('data-one-pix');
				}
			}
		}
		contentHubImgLazyLoad('onscroll');
		//
	}); 
}
//
function getDeviceSpecificDataSrc(imgsrc){
  var deviceWidth =  getDeviceWidth();
  var datasrc = null;
  if(deviceWidth >= 1024){
    datasrc = imgsrc.getAttribute('data-src');
  }else if(deviceWidth >= 768){
    datasrc = imgsrc.getAttribute('data-src-ipad');
  }else if(deviceWidth >= 480 && deviceWidth <= 767){
    datasrc = imgsrc.getAttribute('data-src-tab');
  }else if(deviceWidth >= 320 && deviceWidth <= 480){
    datasrc = imgsrc.getAttribute('data-src-mobile');
  }
  return datasrc;
}
//
function lazyLoadHeroImage() {
	var imageSrc = $$D('heroPodImg') ? $$D('heroPodImg') : $$D('heroVideoImg') ? $$D('heroVideoImg') : null;
	if(imageSrc != null && (isScrolledIntoView(imageSrc, false) || isScrolledIntoView($$D('con-for-nav-0'), false))) {
			var imgSrc = imageSrc.getAttribute("src");
			if((imgSrc.indexOf('/img_px') > -1)) {
				imageSrc.src = getDeviceSpecificDataSrc(imageSrc);
			}
	}		
}
//
function contentHubImgLazyLoad(e) {
	//
	var imgSrcList = document.getElementsByTagName('img');
	var deviceWidth = getDeviceWidth();
	//
	for(var i = 0; i < imgSrcList.length; i++){
		var imgSrc = imgSrcList[i].getAttribute("src");
		if(isScrolledIntoView(imgSrcList[i], false)) {
			if((imgSrc.indexOf('/img_px') > -1)) {
				if(imgSrcList[i].className == 'Campus image' || imgSrcList[i].className == 'Carousal image') {
					if(isScrolledIntoView($$D('con-for-nav-34'), false) && imgSrcList[i].className == 'Campus image') {
						imgSrcList[i].src = getDeviceSpecificDataSrc(imgSrcList[i]);
					} else if(isScrolledIntoView($$D('con-for-nav-36'), false) && imgSrcList[i].className == 'Carousal image') {
						imgSrcList[i].src = getDeviceSpecificDataSrc(imgSrcList[i]);
					}
				} else {
					if(imgSrcList[i].className == 'sections' && !(deviceWidth >= 320 && deviceWidth <= 480)) {
						// do nothing
					} else {
						console.log('check section  > '+imgSrcList[i].className);
						imgSrcList[i].src = getDeviceSpecificDataSrc(imgSrcList[i]);
					}
				}
				if(isIE && (imgSrcList[i].className == 'Gallery image' || imgSrcList[i].className == 'Campus image' || imgSrcList[i].className == 'opendays image')) {
					var obj;
					if(imgSrcList[i].className == 'Gallery image') {
					   obj = $jcub(imgSrcList[i]).closest("li");
					} else if(imgSrcList[i].className == 'Campus image' || imgSrcList[i].className == 'opendays image') {
					   obj = $jcub(imgSrcList[i]).parent();
					}
					$jcub(obj).css('backgroundImage', 'url(' + imgSrcList[i].src + ')') .addClass('compat-object-fit');
				}
			}
			
		}
	}		
}
//
function isScrolledIntoView(element, fullyInView) {
	if(element !=null) {
		var pageTop = $jcub(window).scrollTop();
		var pageBottom = pageTop + $jcub(window).height();
		var elementTop = $jcub(element).offset().top;
		var elementBottom = elementTop + $jcub(element).height();
		/*
		if((elementBottom >= pageTop)) {
			pageBottom = pageBottom - ((pageBottom/100) * 0.65);
		} else if((elementTop  <= pageBottom)){
			pageTop = pageTop - ((pageTop/100) * 0.65);
		}
		*/
		//console.log($jcub(element).attr('id')+' > '+ elementTop+ ' < = '+pageBottom+' && '+elementBottom+' >= '+pageTop);
		//console.log($jcub(element).attr('id')+' > '+'fullyInView > '+((elementTop  <= pageBottom) && (elementBottom >= pageTop)));
		if (fullyInView === true) {
			return ((pageTop < elementTop) && (pageBottom > elementBottom));
		} else {
			return ((elementTop  <= pageBottom) && (elementBottom >= pageTop));
		}
		//console.log('fullyInView > '+((elementTop  <= pageBottom) && (elementBottom >= pageTop)));
	} else {
		return false;
	}
	//
}
//
function formGallerySlider() {
	var deviceWidth = getDeviceWidth();
	var thumb_item = $jcub(window).width();
	var width = 171;
	if(thumb_item <= 992){
		width = 90;
	}
	//console.log('length > '+$jcub('.slides li').length);
	if($jcub('.slides li').length > 0) {
		var length = $jcub('#slider .slides li').length;
		if(length > 1) {
			$jcub('#carousel').flexslider({
				animation: "slide",
				controlNav: false,
				directionNav: length > 5 ? true : false,
				animationLoop: false,
				slideshow: false,
				itemWidth: width,
				itemMargin: 5,
				//maxItems: 5, // don't remove this maxitems
				asNavFor: '#slider'
			});
		}else{
			$jcub('#carousel').hide();
		}
		//
		$jcub('#slider').flexslider({
			animation: "slide",
			controlNav: false,
			directionNav: length > 1 ? true : false,
			animationLoop: false,
			slideshow: false,
			sync: "#carousel",
			start: function(slider) {
				$jcub('body').removeClass('loading');
				$$D('mediaName').innerHTML = $$D('mediaName_1') ? $$D('mediaName_1').innerHTML : "";
				var curSlide = slider.find("li:nth-of-type("+(slider.animatingTo+1)+") video")[0];
				if(curSlide != null && !curSlide.paused || (deviceWidth >= 320 && deviceWidth <= 480)) { // hide if video
					$jcub('#carousel').hide();
				} else {
					$jcub('#carousel').show();
				}
			},
			before: function(slider){
				//
				var curSlide = slider.find("li:nth-of-type("+(slider.animatingTo+1)+") video")[0];
				$$D('mediaName').innerHTML = $$D('mediaName_'+(slider.animatingTo+1)) ? $$D('mediaName_'+(slider.animatingTo+1)).innerHTML : "";
				videoPlayOrPause(curSlide);
				if(curSlide != null && !curSlide.paused || deviceWidth >= 320 && deviceWidth <= 480) { // hide if video
					$jcub('#carousel').hide();
				} else {
					$jcub('#carousel').show();
				}
				//
				var imgSrc = slider.find("li:nth-of-type("+(slider.animatingTo+1)+") img");
				if(imgSrc[0] != null && imgSrc[0].getAttribute('data-media-name') != null && sectionWiseLoggingMap['section'+imgSrc[0].id+'_ImgImp'] != "Y") {
					imageImpressionViewOnGA(imgSrc[0].getAttribute('data-media-name'));
					sectionWiseLoggingMap['section'+imgSrc[0].id+'_ImgImp'] = "Y";
				}
				//
				if(iOSSafari){
					var sliderNo = slider.animatingTo+1;
					
					setTimeout(function(){
						if($$D('bg_vid-slider'+sliderNo) && $$D('vid-slider'+sliderNo).style.display == 'none') { 
							$$D('bg_vid-slider'+sliderNo).style.display = 'block'; 
						}
					}, 100);
				}
			}
		});
	}
}
//
var navCtrl = null;
function lboxNewMapInitialize(latitude, longtitude){
  var deviceWidth =  getDeviceWidth();		
  var navPos = (deviceWidth <= 992) ? "top-left" : "bottom-left";
  var mapId = $$D('currentMap').value;
  var id = mapId.replace('map','');  
  var mapShownVal = $$D('didMapShown'+id);
  if(mapShownVal.value == 'N'){
    mapShownVal.value = 'Y'; 
    mapboxgl.accessToken = $jcub('#cumbApiKey').val();  
    map[id] = new mapboxgl.Map({
        container: mapId,
        center:[longtitude, latitude],      
        style: $jcub('#cumbStyle').val(),
    });
    navCtrl = new mapboxgl.NavigationControl();
    map[id].addControl(navCtrl, navPos);
    var contentString = $jcub('#address'+mapId).html();
    new mapboxgl.Marker() .setLngLat([longtitude, latitude])
    .setPopup(new mapboxgl.Popup().setHTML(contentString))
      .addTo(map[id]);    
      bound = new mapboxgl.LngLatBounds([longtitude, latitude],[longtitude, latitude]);
    map[id].fitBounds([bound.getNorthEast(), bound.getSouthWest()],{animate: false});	    
    var adjZoomLevel = ((map[id].getZoom()/2)-4);
    map[id].setZoom(map[id].getZoom()- adjZoomLevel);    
    //
    map[id].on('load', function() {
      map[id].resize();
    });
    //
  }  
}
//
$jcub(window).on('load orientationchange', function (e) {
    setTimeout(function() {
      var deviceWidth =  getDeviceWidth();		
      var navPos = (deviceWidth <= 992) ? "top-left" : "bottom-left";
      for(var i=0;i<map.length;i++){        
        if(map[i]!=undefined && map[i]!=''){
          map[i].resize();          
          map[i].addControl(navCtrl, navPos);                    
        }
      }
    }, 500);
});
//
var contextPath = "/degrees";
function buildMap(id, lat, lng) {
	var mapId = $$D('map'+id);
	var hiddenId = $$D('didMapLoaded'+id);
	if(mapId && hiddenId){
		if(hiddenId.value == 'N'){
			hiddenId.value = 'Y';
			var url = contextPath + "/jsp/uni/include/lazyloadMap.jsp?latitudeStr="+lat+"&longitudeStr="+lng;
			var ajax = new sack();
			ajax.requestFile = url; 
			if(!$$D('mapJs')) {
				var script = document.createElement("script");
				script.type = "text/javascript";
				script.id = "mapJs";				
				script.src = $jcub('#cumbScpt').val();
        var css = document.createElement("link");
        css.rel = "stylesheet";
        css.type = "text/css";
        css.href = $jcub('#cumbCss').val();
        //console.log(css+""+script);
        if(script.readyState){//IE
					script.onreadystatechange = function(){
						if(script.readyState == "loaded" || script.readyState == "complete"){
							script.onreadystatechange = null;
						}
					};
				}else{
					script.onload = function(){};
				}
				if(typeof script != "undefined"){
					document.getElementsByTagName("head")[0].appendChild(script);
          document.getElementsByTagName("head")[0].appendChild(css);
				}
			}
			ajax.onCompletion = function(){onCompleteMapCampus(ajax, mapId)}; 
			ajax.runAJAX();
		}
	}
}
//
function onCompleteMapCampus(ajax,mapId){
  var response = ajax.response;
  response = response.replace('script_google_map', 'script_google_'+(mapId.id));
  $$D(mapId.id+'Script').value = response;
  //$$D('eval'+(mapId.id)).innerHTML = response;
  //setTimeout(function() {
  //   if($$D('script_google_'+(mapId.id))){
 //      eval($$D('script_google_'+(mapId.id)).innerHTML);
 //    }
 // }, 3000);  
}
//
function showCampusMap() {
	var id = $jcub('#mySliderTabs ul li.selected a').attr('data-id');
  var mapShownVal = $$D('didMapShown'+id);  
	$jcub(".map-btn").addClass('active');
	$jcub(".gal-btn").removeClass("active");
	//$jcub("#map"+id).html('');
	$$D("currentMap").value = "map"+id;
  if(mapShownVal.value == 'N'){
	  $jcub("#map"+id).html($$D('map'+id+'Script').value);
	}
  $jcub("#imageHolder"+id).hide();
	$jcub("#mapHolder"+id).show();
  if(map[id]!=undefined && map[id]!=''){    
    map[id].resize();     
  } 
}
//
function showCampusImage() {
	var id = $jcub('#mySliderTabs ul li.selected a').attr('data-id');
	$jcub('.gal-btn').addClass('active');
	$jcub(".map-btn").removeClass("active");
	$jcub("#imageHolder"+id).show();
	$jcub("#mapHolder"+id).hide();
	//
}
//
function setCampusImgHeightIfDesktop() {
	var deviceWidth = getDeviceWidth();
	if(deviceWidth >= 1024) {
		$jcub("#mySliderTabs .tabimages img").css("height", 898);
	}
}
//
function formCampusSlider() {
	// --- Form the image slider
	//setCampusImgHeightIfDesktop();
	var indicatorAndPanelArrowFlag1 = $jcub('#mySliderTabs ul li').length > 1 ? true : false;
	//
	var slider = $jcub("div#mySliderTabs").sliderTabs({
		autoplay: false,
		mousewheel: false,
		indicators: indicatorAndPanelArrowFlag1,
		panelArrows: indicatorAndPanelArrowFlag1,
		mousewheel: false,
		defaultTab: 1,
		tabs: true,
		tabArrows: true,
		tabHeight: 50
	});
	// --- Event to check whether there is a map section available for the particular venue
	$jcub("#mySliderTabs .tab-slide-img, .ui-slider-tabs-rightPanelArrow, .ui-slider-tabs-leftPanelArrow, .ui-slider-tabs-indicator").click(function(e) {
		setTimeout(function() {
			var id = $jcub('#mySliderTabs ul li.selected a').attr('data-id');
			//
			var currentVisibleSection = $jcub('#image').hasClass('active') ? "IMAGE" : "MAP";
			//
			if($jcub('#mapHolder'+id).length > 0) {
				$jcub(".ts_cnt").show();
			} else {
				$jcub(".ts_cnt").hide();
			}
			if(currentVisibleSection == 'IMAGE') {
				if($$D('imageHolder'+id)) {
					var imgSrc = $jcub('#imageHolder'+id+' img');
					if(imgSrc[0] != null && imgSrc[0].getAttribute('data-media-name') != null && sectionWiseLoggingMap['section'+imgSrc[0].id+'_ImgImp'] != "Y") {
						imageImpressionViewOnGA(imgSrc[0].getAttribute('data-media-name'));
						sectionWiseLoggingMap['section'+imgSrc[0].id+'_ImgImp'] != "Y";
					}
				}
			}
			//
			if(currentVisibleSection == "MAP") {
				showCampusMap();
			} else {
				showCampusImage();
			}
			//
		}, 50);
	});
	// --- Event to scroll to campus top while navigating to next sliders
	$jcub('.ui-slider-tabs-leftPanelArrow,.ui-slider-tabs-rightPanelArrow,.ui-slider-tabs-list li,.tab_sec li').click(function() {
        $jcub('body,html').animate({
            scrollTop: $jcub('.campus').offset().top
        }, 1200);
    });
	//	
	if($jcub('#mapHolder1').length == 0) {
		$jcub(".ts_cnt").hide();
	} else {
		$jcub(".ts_cnt").show();
	}
}
//

//
function setCampusHeight() {
	//
	var setHeight = $jcub('.ui-slider-tabs-content-container').height();
	if(setHeight != null) {
		var campusContentArray = $jcub('.ui-slider-tab-content');
		if($$D('imageCampus1')) {
			if(capusLoaded != "isLoaded" && $jcub('#1image').hasClass('ui-slider-tab-content selected')) {
				/*
				if($$D('imageCampus1') && $jcub('#1image').hasClass('ui-slider-tab-content selected')) {
					if(setHeight != $$D('imageCampus1').clientHeight) {
						$jcub('.ui-slider-tabs-content-container').height($$D('imageCampus1').clientHeight);
					}
				}
				*/
				for(var i = 1; i <= campusContentArray.length; i++) {
					if($$D('map'+i)) { 
						if($$D('imageCampus1').clientHeight != $$D('map'+i).style.height) {
							$$D('map'+i).style.height = $$D('imageCampus1').clientHeight+'px'; 
						}
					}
				}
				capusLoaded = "isLoaded";
			}
		}
		//
	}
}
//
function drawRPdoughnutchart(value1,value2,currentElement,color){
  if(value1!="" && value2!="")  {
    var limit1  = parseInt(value1);
    var limit2  = parseInt(value2);
    var defColor = "#dededf";      
    
    if(color!="" && color=="plain") {
      defColor = "#dededf";
    } 
	
	var options = {        
		percentageInnerCutout: 125,
		segmentStrokeWidth: 0
	};
    
    $jcub("#" + currentElement).drawDoughnutChart([
      {  
        value : limit1,  
        color: "#fc7169"      
      },
      { 
        value:  limit2,   
        color: defColor
      }
    ], options);
  } 
}
//Added extra param section related myhc profile for cmmt
function contentHubProfSecStatsLog(collegeId, networkId, sectionProfileId, sectionName,sectMyhcProfileId) {
    var profileType = $$D("cubProfileType").value;
	if(sectionName.trim().toUpperCase() == 'HOME') {
		profileType = $$D('sectionEmbed_0') ? $$D('sectionEmbed_0').value : "";
	}
	contentHubProfSecStatsLogginAjax("?tabName=" + sectionName + "&collegeId=" + collegeId + "&cpeQualificationNetworkId=" + networkId + "&myhcProfileId=" + sectMyhcProfileId + "&profileId=" + sectionProfileId + "&journeyType=&richProfileType=" + profileType)
}
//
function contentHubProfSecStatsLogginAjax(url) {
	//
  var deviceWidth = getDeviceWidth();
	var r = "/degrees/unilandingStatsAjax.html" + url +'&screenwidth='+deviceWidth,
        i = new sack;
    i.requestFile = r, i.runAJAX()
	//
}
//
function sectionsStatsLogOnView() {
	var deviceWidth = getDeviceWidth();
	if(!(deviceWidth >= 320 && deviceWidth <= 480)){
		var id = $jcub(".trans").eq(($jcub(".trans").length)-1).closest( "section" ).attr('id');
		if(id != null && id != 'undefined' && id != '') {
			var contNo = id.replace('con-for-nav-', '');
			//
			// To quick lazy load of image in sections
			var transImg = $jcub('#'+id+' img');
			if(transImg.length > 0){
			  for(var i=0; i < transImg.length; i++){
				var transImgSrc = transImg[i].getAttribute("src");
				if((transImgSrc.indexOf('/img_px') > -1)) {
				  //console.log('trans img > '+transImg[i].className);
				  transImg[i].src = getDeviceSpecificDataSrc(transImg[i]);
				}
			  }
			}
			//	
			if(isScrolledIntoView($jcub('#'+id), false)) {
				//
				// --- video play and stats
				//
				var videoSrc = $jcub('#'+id+' video[data-media-name]');
				if(id == 'con-for-nav-1' && videoSrc[0] != null) {
					if(!(isScrolledIntoView($jcub('#videoSectionHolder_0'), true))) {
						videoSrc[0] = '';
					} else {
						if($$D('background-video')){
							if(videoStatusMap[$$D('background-video').id+'_manualpause'] != 'Y' && !$$D('background-video').paused) {
								$$D('background-video').pause();
								setTimeout(function() {
									videoStatusMapUpdate($$D('background-video'), 'Y', 'N', 'N');
								}, 100);
							}
						}
					}
				}				
				//
				$jcub('.sectionpod').each(function() {
					var videoElement = $jcub(this).get(0);
					if(videoSrc[0] == null || (videoSrc[0] == '') || ((videoSrc[0] != null && videoElement != null) && (videoSrc[0].id != videoElement.id))) {
						playAndPauseSectVideo(videoElement, 'pause');
					}
				});
				//
				if(contNo < 31) {
					if(videoSrc.length == 1 && videoSrc[0] != null && videoSrc[0] != '') {
						setTimeout(function(){
							if(!(videoSrc[0].id == "video_section_0" && !(isScrolledIntoView($jcub('#videoSectionHolder_0'), true)) && !iOSSafari)) {
								playAndPauseSectVideo(videoSrc[0], 'play')  // play video
							}
							if(!iOSSafari){
								$$D('bg_'+videoSrc[0].id).style.display = "none";
								$$D('wrapper_'+videoSrc[0].id).style.display = "block";
							}
							if(sectionWiseLoggingMap['section'+contNo+'_SectionVideo'] != "Y") {
								sectVidDbStatsLog(videoSrc[0].getAttribute('data-media-id'), 'B', contNo); // DB stats
								sectionWiseLoggingMap['section'+contNo+'_SectionVideo'] = "Y";
							}
							videoPlayedDurationPercentage(videoSrc[0]); // GA stats
							/*
							if($$D('background-video')){
								if(!$jcub('#background-video').paused){
									$jcub('#background-video')[0].pause();
								}
							}
							*/
						}, 4000);
					}
				}
				//
				// --- content db stats
				//
				if($$D('sectionName_'+contNo)) {
					$$D('currentSection').value = $$D('sectionName_'+contNo).value;
					if(contNo < 31) {						
						if(sectionWiseLoggingMap['section'+contNo+'_SectionCont'] != "Y" && sectionWiseLoggingMap['section'] == id) {
							sectionsDBStatsLog(contNo);  // DB stats
							sectionWiseLoggingMap['section'+contNo+'_SectionCont'] = "Y";
						}
						setTimeout(function(){
							if($$D('sectionName_'+contNo).value == $$D('currentSection').value && sectionWiseLoggingMap['section'] == id) {
								if(sectionWiseLoggingMap['section'+contNo+'_SectionDimension'] != "Y") {
									//console.log('ga > dimension12 > '+$$D('sectionName_'+contNo).value);
									ga('set', 'dimension12', ($$D('sectionName_'+contNo).value).toLowerCase());
									sectionWiseLoggingMap['section'+contNo+'_SectionDimension'] = "Y";
								}
								//
								if(sectionWiseLoggingMap['section'+contNo+'_ContImp'] != "Y") {
									//console.log('ga > sectionafter2seconds  > Content Impression > '+$$D('sectionName_'+contNo).value+' > '+$$D('collegeNameGa').value);
									GAInteractionEventTracking('sectionafter2seconds', 'Content Impression', ($$D('sectionName_'+contNo).value).toLowerCase(), $$D('collegeNameGa').value);
									sectionWiseLoggingMap['section'+contNo+'_ContImp'] = "Y";
								}
								//
								// --- image impression
								//
								var imgSrc = $jcub('#'+id+' img[data-media-name]');
								if(imgSrc.length == 1) {
									if(imgSrc[0].getAttribute('data-media-name') != null) {
										if(sectionWiseLoggingMap['section'+contNo+'_ImgImp'] != "Y") {
											imageImpressionViewOnGA(imgSrc[0].getAttribute('data-media-name')); // GA stats
											sectionWiseLoggingMap['section'+contNo+'_ImgImp'] = "Y";
										}
									}
								}
							}
						},2000);
					}
				}
			} else {
				$jcub('.sectionpod').each(function() {
					var videoElement = $jcub(this).get(0);
					playAndPauseSectVideo(videoElement, 'pause');
				});
			}
		}
	} else if(deviceWidth >= 320 && deviceWidth <= 480){
		for(var i=1; i < 31; i++) {
			if($jcub('#con-for-nav-'+i).length > 0) {
				if(isScrolledIntoView($jcub('#con-for-nav-'+i), false)) {
					// --- content db stats
					if($$D('sectionName_'+i)) {
						$$D('currentSection').value = $$D('sectionName_'+i).value;
						if(i < 31) {
							if(sectionWiseLoggingMap['section'+i+'_SectionCont'] != "Y") {
								sectionsDBStatsLog(i);  // DB stats
								sectionWiseLoggingMap['section'+i+'_SectionCont'] = "Y";
							}
							sectionLogsForMobile(i, $$D('currentSection').value, $$D('sectionName_'+i).value);
						}
					}
				}
			}
		}
	}
}
//
function sectionLogsForMobile(i, currentSection, sectionName) {
	setTimeout(function(){
		if($$D('sectionName_'+i) && sectionName == currentSection &&  sectionWiseLoggingMap['section'] == 'con-for-nav-'+i) {
			if(sectionWiseLoggingMap['section'+i+'_SectionDimension'] != "Y") {
				//console.log('ga > dimension12 > '+sectionName);
				ga('set', 'dimension12', sectionName.toLowerCase());
				sectionWiseLoggingMap['section'+i+'_SectionDimension'] = "Y";
			}
			//
			if(sectionWiseLoggingMap['section'+i+'_ContImp'] != "Y") {
				//console.log('ga > sectionafter2seconds  > Content Impression > '+sectionName+' > '+$$D('collegeNameGa').value);
				GAInteractionEventTracking('sectionafter2seconds', 'Content Impression', sectionName.toLowerCase(), $$D('collegeNameGa').value);
				sectionWiseLoggingMap['section'+i+'_ContImp'] = "Y";
			}
			//
			var imgSrc = $jcub('#con-for-nav-'+i+' img[data-media-name]');
			if(imgSrc != null && imgSrc.length == 1) {
				if(sectionWiseLoggingMap['section'+i+'_ImgImp'] != "Y") {
					imageImpressionViewOnGA(imgSrc[0].getAttribute('data-media-name')); // GA stats
					sectionWiseLoggingMap['section'+i+'_ImgImp'] = "Y";
				}
			}
		}
	},2000);
}
//
function isSectionInView(id) {
	var deviceWidth = getDeviceWidth();
	if($jcub('#'+id).length > 0 && isScrolledIntoView($jcub('#'+id), false)) {
		if(!(deviceWidth >= 320 && deviceWidth <= 480)) {
			var contNo = id.replace('con-for-nav-','');
			if(contNo == 0 && $jcub('#atz li .active').length == 0) {
				//sectionWiseLoggingMap = {};
				return true;
			}
			if(contNo > 0 && contNo < 31) {
				var idTrans = $jcub(".trans").eq(($jcub(".trans").length)-1).closest( "section" ).attr('id');
				if(idTrans == id) {
					if(contNo == 1) {
						if($jcub('#nav-1').hasClass('active') && (idTrans.indexOf('2') < 0)) {
							return true;
						} else {
							return false;
						}
					}
					return true; 
				} else { 
					return false; 
				}
			}
			if($jcub('#'+(id.replace('con-for-', ''))).length > 0 && $jcub('#'+(id.replace('con-for-', ''))).hasClass('active')) {
				return true;
			}
		} else {
			return true;
		}
	}
	return false;
}
//
function clearSectionWiseLoggingMap() {
	for(var i=0; i <= 38; i++) {
		if(isSectionInView('con-for-nav-'+i)) {
			var id = 'con-for-nav-'+i;
			var contNo = i+1;
			if(upOrDownScrollFlag == 'down') {
				contNo = i+1;
			} else {
				contNo = i-1;
			}
			if(!isSectionInView('con-for-nav-'+(contNo))) {
				if(sectionWiseLoggingMap['section'] != id) {
					//sectionWiseLoggingMap = {};
					sectionWiseLoggingMap['section'] = id;
					//sectionWiseLoggingMap['section'+i+'_SectionCont'] = "N";
					//sectionWiseLoggingMap['section'+i+'_SectionVideo'] = "N";
					//sectionWiseLoggingMap['section'+i+'_ImgImp'] = "N";
					//sectionWiseLoggingMap['section'+i+'_ContImp'] = "N";
					sectionWiseLoggingMap['section'+i+'_SectionDimension'] = "N";
				}
			}	
		}
	}
}
//
function highLightSectionDots() {
	var scroll = $jcub(this).scrollTop();
	$jcub(".atz li").each(function() {
		var elementClick = $jcub(this).attr("id");
		if($jcub("#con-for-" + elementClick).length)  {
			offTop = $jcub("#con-for-" + elementClick).offset().top;
			if (scroll > offTop - 130) {
				$jcub(this).siblings().removeClass("active");
				$jcub(this).addClass('active');
			} else if (offTop == 0) {
				$jcub(this).siblings().removeClass("active");
				$jcub(this).addClass('active');
			} else {
				$jcub(this).removeClass('active');
			}
		}
	});
}
//
function sectionsStatsOnScrollStops() {
	var deviceWidth = getDeviceWidth();
	$jcub(window).scrollEnd(function(){
		highLightSectionDots();
		if(!(deviceWidth >= 320 && deviceWidth <= 480)) {
			clearSectionWiseLoggingMap();
			sectionsStatsLogOnView();
			otherSectionsImageAndDBstatsLog();
			playOrPauseVideoOnScroll();
		}
	}, 100);
}
//
$jcub.fn.scrollEnd = function(callback, timeout) {         
  $jcub(this).scroll(function(){
    var $jcubthis = $jcub(this);
    if ($jcubthis.data('scrollTimeout')) {
      clearTimeout($jcubthis.data('scrollTimeout'));
    }
    $jcubthis.data('scrollTimeout', setTimeout(callback,timeout));
  });
};
//
function contentScroll() {
  var width = document.documentElement.clientWidth;
  if(width > 767){
    $jcub(window).scroll(function() {
      //
      scrol= $jcub(window).scrollTop();
      half= $jcub( window ).height()/2;
	  var previous = 0;
	  for(var i=1; i <= 38; i++) {
		//console.log(' >>>> '+i);
		if(i == 1) {
			if($jcub('#con-for-nav-1').length) {
				if ((scrol > $jcub('#con-for-nav-1 .col_rgt').offset().top)) {
					$jcub('.csticky').addClass('sti');
					previous = i;				
					//$jcub('#con-for-nav-'+i+' .csticky').removeClass('trans');
				} else $jcub('.csticky').removeClass('sti');
			}
		} 
		if(i == 31 || i == 32) {
			var height = half;
			if($jcub('#con-for-nav-'+previous).length) {
				height = $jcub('#'+($jcub('#con-for-nav-'+previous).attr('id'))+' .col_lft') ? $jcub('#'+($jcub('#con-for-nav-'+previous).attr('id'))+' .col_lft').height() : $jcub('#con-for-nav-'+previous).height();
			}
			if($jcub('#con-for-nav-'+i).length) {
				if (scrol > ($jcub('#con-for-nav-'+i).offset().top - height)) {
					previous = i;
					$jcub('#con-for-nav-'+i+' .csticky').addClass('trans');
					$jcub('.csticky').removeClass('sti');
				} else $jcub('#con-for-nav'+i+' .csticky').removeClass('trans'); 
			}
		} 
		if(i == 33) {
			//
			if($jcub('#con-for-nav-'+previous).length) {
				height = $jcub('#'+($jcub('#con-for-nav-'+previous).attr('id'))+' .col_lft') ? $jcub('#'+($jcub('#con-for-nav-'+previous).attr('id'))+' .col_lft').height() : $jcub('#con-for-nav-'+previous).height();
			}
			if($jcub('#con-for-nav-33').length) {
				if (scrol > ($jcub('#con-for-nav-33 .col_rgt').offset().top - height)) {
					$jcub('#con-for-nav-33 .csticky').addClass('sti');
					$jcub('#con-for-nav-33 .csticky').addClass('trans');
					if($jcub('#con-for-nav-31').length == 0) {
						$jcub('#con-for-nav-33 .csticky').css({'z-index':'13'});
					} 
					previous = i;
				} else {
					$jcub('#con-for-nav-33 .csticky').removeClass('trans');              
				}
				//
				if (scrol > ($jcub('#con-for-nav-33 .col_rgt').offset().top - height)) {
					$jcub('#con-for-nav-33 .csticky').addClass('sti');
					$jcub('#con-for-nav-33 .csticky').removeClass('trans_up');
					previous = i;
				} 
			}
			//
		} 
		if(i > 33) {
			if($jcub('#con-for-nav-'+previous).length) {
				height = $jcub('#'+($jcub('#con-for-nav-'+previous).attr('id'))+' .col_lft') ? $jcub('#'+($jcub('#con-for-nav-'+previous).attr('id'))+' .col_lft').height() : $jcub('#con-for-nav-'+previous).height();
			}
			if($jcub('#con-for-nav-'+i).length) {
				if (scrol >= $jcub('#con-for-nav-'+i).offset().top - height) {
					if($jcub('#con-for-nav-33').length) { 
						$jcub('#con-for-nav-33 .csticky').removeClass('sti');
						$jcub('#con-for-nav-33 .csticky').addClass('trans_up');
					}
					previous = i;
					$jcub('.csticky').removeClass('sti');
				}
			}
		} 
		if(i >= 2 && i < 31) {			
			var height = half;
			if($jcub('#con-for-nav-'+(i-1)).length > 0) {
				height = $jcub('#con-for-nav-'+(i-1)+' .col_lft') ? $jcub('#con-for-nav-'+(i-1)+' .col_lft').height() : $jcub('#con-for-nav-'+(i-1)).height();
			}
			if($jcub('#con-for-nav-'+i).length > 0) {
				if (scrol > ($jcub('#con-for-nav-'+i+' .col_rgt').offset().top -  height)) {
					$jcub('#con-for-nav-'+i+' .csticky').addClass('trans');
					previous = i;
				} else {
					$jcub('#con-for-nav-'+i+' .csticky').removeClass('trans');              
				}
			}
		}
		
	  }
      
    });
  }
}
//
function labelTopOnBlur(o) {
	var value = $jcub(o).val();
	if (value != "") { $jcub(o).next('label').addClass("top_lb"); } 
	else { $jcub(o).next('label').removeClass("top_lb"); }
}
//
function prePopulateLabelOnTop() {
  $jcub('#chEnquiryFormId input[type="text"],input[type="password"],input[type="email"]').each(function() {
    labelTopOnBlur(this);
  });
}
//
function enquiryValidate(id){
  if(id=="fname" || id == "lname"){    
    var errMsgClass;
    var susMsgClass;
    if(id == "fname"){
      errMsgClass = "fl w49 mb37 enqerr";
      susMsgClass="fl w49 mb37 suc_msg";
      classNameId = "divfname";
      msgId = "errorfname";
    }else if(id == "lname"){
      errMsgClass = "fr w49 mb37 enqerr";
      susMsgClass="fr w49 mb37 suc_msg";
      classNameId = "divlname";
      msgId = "errorlname";
    }
    if(trimString($$D(id).value) == "" || trimString($$D(id).value).toLowerCase() == "first name" || trimString($$D(id).value).toLowerCase() == "last name"){      
      setErrAndSucMsg(classNameId, errMsgClass, msgId, "We still don't know your name. Remind us?");
      $$D(msgId).className = "enqerr1";
    }else if(id == "fname" && trimString($$D(id).value) != "" && !letters.test($$D(id).value)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
      setErrAndSucMsg(classNameId, errMsgClass, msgId, nonEngLanguageErrorMsg); 
      $$D(msgId).className = "enqerr1";
    }else if(id == "lname" && trimString($$D(id).value) != "" && !letters.test($$D(id).value)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
      setErrAndSucMsg(classNameId, errMsgClass, msgId, nonEngLanguageErrorMsg); 
      $$D(msgId).className = "enqerr1";
    }else{
      if(id == "fname"){
        setErrAndSucMsg(classNameId, susMsgClass, msgId, 'Nice to meet you! Great name!');
        $$D(msgId).className = "suc_txt";
      }
      if(id == "lname"){
        setErrAndSucMsg(classNameId, susMsgClass, msgId, '');
        $$D(msgId).className = "suc_txt";
      }
    }
  }
  if(id == "emailAddress" || id=="email"){
    blockNone('errorEmailExists', 'none');
    if(isEmpty($$D(id).value) || $$D(id).value == 'Enter email address'){ 
      setErrAndSucMsg('divEmailId','em_add mb37 enqerr','errorEmail', 'Something seems to be missing from your email. Awkward.');
      $$D("errorEmail").className = "enqerr1";      
    } else if(!emailCheck($$D(id).value)){ 
      setErrAndSucMsg('divEmailId','em_add mb37 enqerr','errorEmail', 'Something seems to be missing from your email. Awkward.'); 
      $$D("errorEmail").className = "enqerr1";      
    }else{
      setErrAndSucMsg('divEmailId','em_add mb37 suc_msg','errorEmail', "You've just got to be on email"); 
      $$D('errorEmail').className = "suc_txt";      
    }
  }
   if(id == 'password') {
		var password = ""; 
		if($$D("password")){password = trimString( $$D("password").value );}
		if(isEmpty(password)){ message = false; setErrAndSucMsg('divPwd','mb37 enqerr','errorPassword', "We still don't know your password. Remind us?");
		} else {setErrAndSucMsg('divPwd','mb37 suc_msg','errorPassword','');}
		if(!isEmpty(password) && password.length < 6){
		   message = false; setErrAndSucMsg('divPwd','mb37 enqerr','errorPassword', 'Your password should be 6 characters or more');
		}
	} 
  if(id == "yeartoJoinCourse1" || id == "yoeFeild" || id == "yeartoJoinCourse3" || id == "yeartoJoinCourse4"){//need to check and remove if yoe is defaultly checked
    var yoe = '';
    for (i=0; i<document.getElementsByTagName('input').length; i++) {
      if (document.getElementsByTagName('input')[i].type == 'radio'){
        if(document.getElementsByTagName('input')[i].checked == true){
         yoe = document.getElementsByTagName('input')[i].value;
         break;
        }
      } 
    }
    var cuYear = new Date().getFullYear();
    if(yoe < cuYear){
      setErrAndSucMsg("yoeFeild", "year-options", "yoeErrMsg", "<p>Please choose when would you like to start?</p>");
      $$D("yoeErrMsg").className = "enqerr1";
      returnflag = false;
    }
    /*else{
      setErrAndSucMsg("yoeFeild", "form-label year_lbl", "yoeErrMsg", "");
    }*/
  }
  if(id == "postcode"){
    if(trimString($$D(id).value) != "" && alphaNumberic.test($$D(id).value) && !specialCharacter.test($$D(id).value)){ //Added by Kailash L for adding validation for post code JIRA-817 
      setErrAndSucMsg("postcodeField", "fl w49 mb37 suc_msg", "postcodeErrMsg", contentHubErrorMsg.success_post_code); //changed text by Prabha on 31_July_2018_rel
      $$D('postcodeErrMsg').className = "suc_txt";
    }else if(trimString($$D(id).value) != "" && !letters.test($$D(id).value)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
      setErrAndSucMsg('postcodeField', 'fl w49 mb37 enqerr', 'postcodeErrMsg', nonEngLanguageErrorMsg); 
      $$D("postcodeErrMsg").className = "enqerr1";
  	}else{
      setErrAndSucMsg("postcodeField", "fl w49 mb37 enqerr", "postcodeErrMsg", "Please check you have entered your postcode correctly - max. 8 characters");
      $$D("postcodeErrMsg").className = "enqerr1";
    }
  }  
}
//
function checkExistsEmail(id){
  var email = $$D(id).value;
  if(trimString(email) == ''){
    setErrAndSucMsg('divEmailId','em_add mb37 enqerr','errorEmail', 'Something seems to be missing from your email. Awkward.');
    $$D("errorEmail").className = "enqerr1";
    return false;
  }else{
    var url= "/degrees/commonaction.html";    
    var ajax=new sack();
    ajax.requestFile = url;	
    
    ajax.setVar("emailAddress", trimString(email));
    ajax.setVar("actionType", "checkEmail");
        
    ajax.onCompletion = function(){ showEmailResponseStatus(ajax, email); };	
    ajax.runAJAX();
  }
}
function showEmailResponseStatus(ajax, textVal){
  var response = ajax.response;
  if(response.trim() == "EXIST"){    
    text =  "The email address " + textVal + " is already registered with us, please login. This is to protect your personal data";    
    setErrAndSucMsg('divEmailId','em_add mb37 enqerr','errorEmail', text);
    $$D('errorEmail').className="enqerr1";
    if($$('emailAlreadyExistsflag')){$$('emailAlreadyExistsflag').value="yes"; }
    return false;
  }else{
    if(textVal.includes('@') && textVal.split('@')[1].length > 0){
      if(!emailCheck(trimString(textVal))){      
        setErrAndSucMsg('divEmailId','em_add mb37 enqerr','errorEmail', 'Something seems to be missing from your email. Awkward.'); 
        $$D("errorEmail").className = "enqerr1";        
      }else if(!isEmpty(textVal)){     
        setErrAndSucMsg('divEmailId','em_add mb37 suc_msg','errorEmail', "You've just got to be on email");
        $$D('errorEmail').className="suc_txt";           
      }
      if($$D('emailAlreadyExistsflag')){$$D('emailAlreadyExistsflag').value="no"; }         
    }else{
      setErrAndSucMsg('divEmailId','em_add mb37 enqerr','errorEmail', 'Something seems to be missing from your email. Awkward.');
      $$D("errorEmail").className = "enqerr1";
    }     
  }
  return false;
}
//
function getAge(birth, callingfrom) {    
  var today = new Date();
  var nowyear = today.getFullYear();
  var nowmonth = today.getMonth();
  var nowday = today.getDate(); 
  
  var dateArray = birth.split("/");
  var birthday = dateArray[0];
  var birthmonth = dateArray[1];
  var birthyear = dateArray[2];
  
  var age = nowyear - birthyear;
  var age_month = nowmonth - birthmonth;
  var age_day = nowday - birthday;
  //Added validation for the age under 13.
  var err_msg = "You must be at least 13 years old";
  if(age == 13 && birthmonth > (today.getMonth()+1)){
    if(callingfrom == "registration"){      
      setErrAndSucMsg("divuserDOBId", "dob-field mb37 clr enqerr", "userDobErrMsg", err_msg);
      $$D("userDobErrMsg").className = "enqerr1";
    }else{
      setErrAndSucMsg("divuserDOBId", "dob-field mb37 clr enqerr", "userDobErrMsg", err_msg);
      $$D("userDobErrMsg").className = "enqerr1";        
    }
  } else if(age == 13 && birthmonth >= (today.getMonth()+1) && parseInt(birthday) > today.getDate()){
    if(callingfrom == "registration"){      
      setErrAndSucMsg("divuserDOBId", "dob-field mb37 clr enqerr", "userDobErrMsg", err_msg);
      $$D("userDobErrMsg").className = "enqerr1";
    }else{
      setErrAndSucMsg("divuserDOBId", "dob-field mb37 clr enqerr", "userDobErrMsg", err_msg);
      $$D("userDobErrMsg").className = "enqerr1";        
    }
  } else if(age > 13 || (age == 13 && birthmonth >= (today.getMonth()+1) && parseInt(birthday) <= today.getDate())){
    if(callingfrom == "registration"){      
      setErrAndSucMsg("divuserDOBId", "dob-field mb37 clr suc_msg", "userDobErrMsg", "Don't expect a birthday card. Soz");
      $$D("userDobErrMsg").className = "suc_txt";
    }else{
      setErrAndSucMsg("divuserDOBId", "dob-field mb37 clr suc_msg", "userDobErrMsg", "Don't expect a birthday card. Soz");
      $$D("userDobErrMsg").className = "suc_txt";        
    }    
  }else if(age > 13 || (age == 13 && birthmonth <= (today.getMonth()+1))){
    if(callingfrom == "registration"){      
      setErrAndSucMsg("divuserDOBId", "dob-field mb37 clr suc_msg", "userDobErrMsg", "Don't expect a birthday card. Soz");
      $$D("userDobErrMsg").className = "suc_txt";
    }else{
      setErrAndSucMsg("divuserDOBId", "dob-field mb37 clr suc_msg", "userDobErrMsg", "Don't expect a birthday card. Soz");
      $$D("userDobErrMsg").className = "suc_txt";        
    }    
  } else if(age < 13){
    if(callingfrom == "registration"){      
      setErrAndSucMsg("divuserDOBId", "dob-field mb37 clr enqerr", "userDobErrMsg", err_msg);
      $$("userDobErrMsg").className = "enqerr1";
    }else{
      setErrAndSucMsg("divuserDOBId", "dob-field mb37 clr enqerr", "userDobErrMsg", err_msg);
      $$D("userDobErrMsg").className = "enqerr1";        
    }
  }
}
//
function validateFormContentHub() {
    var valFlag = true;    
    var s = trimString($$D("fname").value),
        n = trimString($$D("lname").value),
        t = trimString($$D("email").value),        
        a = $$D("postcode").value; 
		   // c = trimString($$D("dateDOBId").value),
       // r = trimString($$D("monthDOBId").value),
        //o = trimString($$D("yearDOBId").value);                
	  var p ="";   
    for (i = 0; i < document.getElementsByTagName("input").length; i++){
      if ("radio" == document.getElementsByTagName("input")[i].type && 1 == document.getElementsByTagName("input")[i].checked) {
          p = document.getElementsByTagName("input")[i].value;          
          break;
      }
	  }
	  if(""!=p&&"undefined"!=p){
      setErrAndSucMsg("yoeFeild", "year-options", "yoeErrMsg", "");
      $$D('yoeErrMsg').className = "suc_txt";
    }else{
      setErrAndSucMsg("yoeFeild", "year-options enqerr", "yoeErrMsg", "Which year you would like to start uni?");
      $$D("yoeErrMsg").className = "enqerr1";
      valFlag = false;
    }
    if($$D('password')){      
      var password = ""; 
      if($$D("password")){password = trimString( $$D("password").value );}
      if(isEmpty(password)){ message = false; setErrAndSucMsg('divPwd','mb37 enqerr','errorPassword', "We still don't know your password. Remind us?");
      } else {setErrAndSucMsg('divPwd','mb37 suc_msg','errorPassword','');}
      if(!isEmpty(password) && password.length < 6){
         message = false; setErrAndSucMsg('divPwd','mb37 enqerr','errorPassword', 'Your password should be 6 characters or more');
      }
    }
    if(""!=s){
      if(!letters.test(s)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
    	setErrAndSucMsg('divfname', 'fl w49 mb37 enqerr', 'errorfname', nonEngLanguageErrorMsg); 
        $$D("errorfname").className = "enqerr1";
        valFlag = false; 
      }else{
        setErrAndSucMsg("divfname", "fl w49 mb37 suc_msg", "errorfname", 'Nice to meet you! Great name!');
        $$D('errorfname').className = "suc_txt"; 
      }
    }else{
      setErrAndSucMsg("divfname", "fl w49 mb37 enqerr", "errorfname", "We still don't know your name. Remind us?");
      $$D("errorfname").className = "enqerr1";
      valFlag = false;
    }
     if(""!=n){
      if(!letters.test(n)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
    	setErrAndSucMsg('divlname', 'fr w49 mb37 enqerr', 'errorlname', nonEngLanguageErrorMsg); 
        $$D("errorlname").className = "enqerr1";
        valFlag = false;   
      }else{
        setErrAndSucMsg("divlname", "fr w49 mb37 suc_msg", "errorlname", "");
        $$D('errorlname').className = "suc_txt";      
      }
    }else{
      setErrAndSucMsg("divlname", "fr w49 mb37 enqerr", "errorlname", "We still don't know your name. Remind us?");
      $$D("errorlname").className = "enqerr1";
      valFlag = false;
    }
    if(isEmpty(t) || t == 'Enter email address'){ 
      setErrAndSucMsg('divEmailId','em_add mb37 enqerr','errorEmail', 'Something seems to be missing from your email. Awkward.');
      $$D("errorEmail").className = "enqerr1";
      valFlag = false;
    }else if(!emailCheck(t)){ 
      setErrAndSucMsg('divEmailId','em_add mb37 enqerr','errorEmail', 'Something seems to be missing from your email. Awkward.'); 
      $$D("errorEmail").className = "enqerr1";
      valFlag = false;
    }else if($$D('emailAlreadyExistsflag') && $$D("emailAlreadyExistsflag").value == "yes"){   
      valFlag = false
    }else{
      setErrAndSucMsg('divEmailId','em_add mb37 suc_msg','errorEmail', "You've just got to be on email"); 
      $$D('errorEmail').className = "suc_txt";      
    }
    
    if(trimString(a) != "" && alphaNumberic.test(a) && !specialCharacter.test(a)){ //Added by Kailash L for adding validation for post code JIRA-817
      setErrAndSucMsg("postcodeField", "fl w49 mb37 suc_msg", "postcodeErrMsg", contentHubErrorMsg.success_post_code); //changed text by Prabha on 31_July_2018_rel
      $$D('postcodeErrMsg').className = "suc_txt";
    }else if(trimString(a) != "" && !letters.test(a)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
      setErrAndSucMsg('postcodeField', 'fl w49 mb37 enqerr', 'postcodeErrMsg', nonEngLanguageErrorMsg); 
      $$D("postcodeErrMsg").className = "enqerr1";
      valFlag = false;
	}else if($jcub('#postCodeErrHidden').val() == 'true'){
	  setErrAndSucMsg("postcodeField", "fl w49 mb37 enqerr", "postcodeErrMsg", "Please check you have entered your postcode correctly - max. 8 characters");
      $$D("postcodeErrMsg").className = "enqerr1";
      valFlag = false;
	}else{
      setErrAndSucMsg("postcodeField", "fl w49 mb37 enqerr", "postcodeErrMsg", "Please check you have entered your postcode correctly - max. 8 characters");
      $$D("postcodeErrMsg").className = "enqerr1";
      valFlag = false;   
    }
     if($$D('chEnquiryFormProceed').checked != true){
     // $$D("ch_termsc_error").innerHTML = 'Please agree to our terms and conditions';
      setErrAndSucMsg('termsFld','acpttext enqerr1','ch_termsc_error', 'Please agree to our terms and conditions');
      //$$D("ch_termsc_error").className = "enqerr1";
     // blockNone('ch_termsc_error', 'block');
      valFlag = false;
    }else{
      	blockNone('ch_termsc_error', 'none');
    }  
     
   if($jcub("#beeTrap") && isNotNullAndUndex($jcub("#beeTrap").val())){
	 valFlag = false; 
	 setErrAndSucMsg('termsFld', 'acpttext enqerr1', 'beeTrapError', 'Your request cannot be processed at this time'); 
	 GANewAnalyticsEventsLogging('Enquiry Form', 'Honey Pot', 'Field Filled');
   }else{
      blockNone("beeTrapError","none");
   } 
  
  if($$D('message')){ //Added by Kailash L for adding validation for text area JIRA-817         
  var textAredMsg = $$D("message").value; 
  textAredMsg = trimString(textAredMsg);
  if(!letters.test(textAredMsg)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
	setErrAndSucMsg('textAreaId', 'textwrap enqerr', 'errorTextArea', nonEngLanguageErrorMsg);
    valFlag = false;
  }else if(textAredMsg.length < 50 || $jcub('#enquiryMessageErrHidden').val() == 'true'){
   	setErrAndSucMsg("textAreaId", "textwrap enqerr", "errorTextArea", "Please be as descriptive as possible with your request - min. 50 characters");
    valFlag = false;
  } else{
   	blockNone("errorTextArea","none");
   	$jcub("#textAreaId").removeClass("enqerr");
  }
}    
    return valFlag;
}
//
function showCubPassword(id) {
	var pwd = $$D("password");
	if(pwd.getAttribute("type")=="password" && !isEmpty($$D("password").value)){
		pwd.setAttribute("type","text");
		$$D(id).className = "fa fa-eye-slash";
	} else {
		pwd.setAttribute("type","password");
		$$D(id).className = "fa fa-eye";
	}
}
function hideCubPassword(id) {
  var pwd = $$D("password");
  pwd.setAttribute("type","password");
  $$D(id).className = "fa fa-eye";
}
//
function cubEnqEngagement() {
	$jcub('#enqForm input').click(function () {
        if($$D('isEngagementDone').value != 'Y') {
			GAInteractionEventTracking('emailbutton', 'engagement', 'Open Email-Request', $$D('collegeNameGa').value);
			$$D('isEngagementDone').value = "Y";
		}
    });
}
//
function validateInteractionFormContenthub() {
  clearErrorMsg('divEmailId','errorEmail');
  var r = validateFormContentHub();
  if (r) {
  var marketingEmailFlag = "N";
  var solusEmailFlag = "N";
  var surveyEmailFlag = "N";
  var consentFlag = "OFF";
  var dataSharing = '';
  var surveys = "";
  var updates = "";
  var newsLetter = "";
  var consentGA = "";
  var consentFlagCheck = "";
  if($$D('consentFlagCheck')){
   consentFlagCheck = $$D('consentFlagCheck').value;
  }
  var deviceWidth = getDeviceWidth();
  if($$D('marketingEmailFlag').checked == true){marketingEmailFlag = "Y";newsLetter = "Newsletter";}
  if($$D('solusEmailFlag').checked == true){solusEmailFlag = "Y";updates = "Updates";}
  if($$D('surveyEmailFlag').checked == true){surveyEmailFlag = "Y";surveys ="Surveys";}
  if($$D('consentFlag')){if($$D('consentFlag').checked == true){
  consentFlag = "ON"; 
  dataSharing = "Data Sharing";}}
  consentGA = dataSharing +"|"+newsLetter+"|"+updates+"|"+surveys;
  if($$D('enquiryExistFlag') && $$D('enquiryExistFlag').value != "Y"){
    GAInteractionEventTracking('emailbutton', 'interaction', 'Open Email-Submit', $$D('collegeNameGa').value);
    GAInteractionEventTracking('emailsubmit', 'Opt In', 'Enquiry Form', consentGA,'');
  }
    $$D('sendEmail').style.display = 'none';
                $$D('loader').style.display = 'block';
		var url= "/degrees/send-college-email.html?";
		var queryString = "firstName="+$$D('fname').value + 
		"&lastName="+$$D('lname').value + 
		"&message="+$$D('message').value + 
		"&emailAddress="+$$D('email').value + 
		"&password="+($$D('password')? $$D('password').value : "" )+ 
		"&postCode="+$$D('postcode').value + 
		"&yeartoJoinCourse="+$jcub("input[name='yeartoJoinCourse1']:checked").val() + 
		"&collegeId="+$$D('collegeId').value + 
		"&courseId=0&subOrderItemId="+$$D('subOrderItemId').value + 
		"&profileType="+$$D('profileType').value + 
		"&studyLevelId="+$$D('studyLevelId').value + 
		"&cpeQualificationNetworkId="+$$D('networkId').value + 
		"&typeOfEnquiry="+$$D('typeOfEnquiry').value + 
		"&qlFlag="+$$D('qlFlag').value + 
		"&requestUrl="+$$D('requestUrl').value + 
		"&refferalUrl="+$$D('refferalUrl').value + 
		"&btSubmit="+$$D('btSubmit').value + 
		"&autoEmailFlag="+$$D('autoEmailFlag').value + 
		"&widgetId="+$$D('widgetId').value + 
	"&marketingEmail="+marketingEmailFlag + 
	"&solusEmail="+solusEmailFlag + 
	"&surveyEmail="+surveyEmailFlag + 
		"&"+$$D('tokenValid').getAttribute("name")+"="+$$D('tokenValid').value + "&fromPage=content-hub" +
    '&screenwidth='+deviceWidth+"&consentFlag="+consentFlag+"&consentFlagCheck="+consentFlagCheck; 
		//console.log("token: "+$$D('tokenValid').value);
    var ajax=new sack();
		ajax.requestFile = url + queryString;	
		ajax.onCompletion = function(){ showResponse(ajax); };	
		ajax.runAJAX();
		return true;		
	}
  return false; 
}
//
function showResponse(ajax) {
  //console.log(ajax.response);
	var response = (ajax.response).trim();	
  var resArr = null; 
  var emailExistCH = null;
  if(response.indexOf("##SPLIT##") > -1){
    resArr = response.split('##SPLIT##');    
    response = resArr[0];
    emailExistCH = resArr[1];
  }
  if(response == 'success') {
		blockNone('success', 'block');
		blockNone('failed', 'none');
		blockNone('duplicate', 'none');
		blockNone('enqForm', 'none');       
  
	} else if(response == 'emailExists'){
    $$D('tokenValid').value = emailExistCH;    
   if($$D('errorEmailExists')){
     var email = $$D('email') ? $$D('email').value: '';
     setErrAndSucMsg('divEmailId','em_add mb37 enqerr','errorEmailExists', 'The email address ' +email+' is already registered with us, please login. This is to protect your personal data');
   }
   
   $$D("errorEmailExists").className = "enqerr1";
   blockNone('errorEmail', 'none');
   blockNone('errorEmailExists', 'block');
		 blockNone('failed', 'none');
		 blockNone('duplicate', 'none');
   $$D('sendEmail').style.display = 'block';
   $$D('loader').style.display = 'none';
 }else if(response == 'duplicate') {
		blockNone('success', 'none');
		blockNone('failed', 'none');
		blockNone('duplicate', 'block');
		blockNone('enqForm', 'none');    
	} else {
		blockNone('failed', 'block');
		blockNone('success', 'none');
		blockNone('duplicate', 'none');
	}
  $jcub("html, body").animate({
      scrollTop: $jcub("#con-for-nav-37").offset().top
  }, "slow");
}
//
function hideTooltip(id) {
  blockNone(id, 'none');
}
//
function showTooltip(id) {
  var dev = jQuery.noConflict();
  if(dev(document).width() > 1024 ) {
    blockNone(id, 'block');   
  }
}
function showAndHideToolTip(id) {
  if($$D(id).style.display == "block"){
   blockNone(id, 'none');
  } else {
   blockNone(id, 'block');
  }
}
//
function blockNone(thisid, value){ 
  if($$D(thisid) !=null){
	$$D(thisid).style.display = value;
  }  
}
//
function setErrAndSucMsg(classNameId, classes, msgId, messages){
  $$D(classNameId).className = classes;
  $$D(msgId).innerHTML = messages;
  $$D(msgId).style.display = "block";
}
//
function trimString(inString){
  return inString.replace(/^\s+|\s+$/g,"");
}
//
function isNumeric(input){
    return (input - 0) == input && (''+input).replace(/^\s+|\s+$/g, "").length > 0;
}
//
function emailCheck(e) {
    return !!/^\w+([-+.'']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(trimString(e))
}
//
function isEmpty(value){  if(trimString(value) ==''){ return true;} else {  return false; } }
//
function isValidDOB(dateStr, validateEmptyDate,id) {
    var msg = "";    
    var dateArray = dateStr.split("/");        
    day = dateArray[0];
    month = dateArray[1];
    year = dateArray[2];  
    var today = new Date();
    if (day == '' || day == 'Day' || day < 1 || day > 31) {
      if(validateEmptyDate || id == "dateDOBId"){
        msg = "Day must be between 1 and 31.";
        return msg;
      }else{
        return "calling_onchange";
      }
    }
    if (month == '' || month == 'Month' || parseInt(month) < 1 || parseInt(month) > 12) { // check month range
      if(validateEmptyDate || id == "monthDOBId"){
        msg = "Month must be between 1 and 12.";
        return msg;
      }else{
        return "calling_onchange";
      }
    }  
    if ((parseInt(month)==4 || parseInt(month)==6 || parseInt(month)==9 || parseInt(month)==11) && parseInt(day)==31) {
        msg = "Month "+month+" doesn't have 31 days!";
        return msg;
    }
    if (parseInt(month) == 2) { // check for february 29th
      var isleap = (parseInt(year) % 4 == 0 && (parseInt(year) % 100 != 0 || parseInt(year) % 400 == 0));
      if (year != "Year" && (day>29 || (day==29 && !isleap))) {
          msg = "February " + year + " doesn't have " + day + " days!";
          return msg;
      }
    }    
    if (year == 'Year' || year < 1900 || year > (today.getFullYear() - 13)) {    //added condition to reduce year 13 for 03_Oct_2017, By Thiyagu G
      if(validateEmptyDate || id == "yearDOBId"){    
        msg = "Please enter a year between 1900 to "+(today.getFullYear() - 13); 
        return msg;
      }else{    
        return "calling_onchange";
      }
    }
    if (year == '' || year < 1900 || year > (today.getFullYear() - 13)) {    
      if(validateEmptyDate || id == "yearDOBId"){    
        msg = "Please enter a year between 1900 to "+(today.getFullYear() - 13);
        return msg;
      }else{    
        return "calling_onchange";
      }
    }
    var age = today.getFullYear() - year;    
    if(age == 13 && month > (today.getMonth()+1)){
      msg = "You must be at least 13 years old";
      return msg;
    } else if(age == 13 && month >= (today.getMonth()+1) && parseInt(day) > today.getDate()){
      msg = "You must be at least 13 years old";
      return msg;
    } else {
      msg = "";
      return msg;
    }
    return msg;  // date is valid
}
//
//
var context_path = "/degrees";
//
function loadSocialFeed(row){
  var socialPodLoaded = $$D('isSocialPodLoaded'+row).value;
  if(socialPodLoaded == "N") {
	  var feedType = $$D('feedType'+row).value;
	  if("TWITTER"==(feedType)){
		$jcub('.twitter-timeline').css({'width':$jcub('#social_TWITTER').outerWidth()});
		twitterScript(row);
		$$D('isSocialPodLoaded'+row).value = "Y";
	  }else if("FACEBOOK"==(feedType)){     
		loadFbTab(row);
		$$D('isSocialPodLoaded'+row).value = "Y";
	  }else if("INSTAGRAM"==feedType){
		instagramScript(row);
	  }
  }
}
//
function twitterScript(row){
  !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
}
//
function faceBookScript(){
  var jqfb = jQuery.noConflict();
  !function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=374120612681083";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk');
}
//
function loadFbTab(row){    
  var url = "";  
  if($$D("provFeedUrl"+row) && !isEmpty($$D("provFeedUrl"+row).value)){    
    url = context_path + "/fbAjax.html?provFbUrl="+($$D("provFeedUrl"+row).value).replace("@","");      
  }   
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){loadFbRes(ajax)};
  ajax.runAJAX();         
}
//
function loadFbRes(ajax){
  var res = ajax.response;
  blockNone("facebookLnk", "block");
  $$D("facebookDiv").innerHTML = res;
  fbLoadTab();
}
//
function fbLoadTab(){
  var jqfb = jQuery.noConflict();
  var providerUrl = 'whatuni';
  if($$D("provFbUrl")){
    providerUrl = $$D("provFbUrl").value;
  }
  var container_width = jqfb('#social_FACEBOOK').outerWidth();
  var container_height = (jqfb('#social_FACEBOOK').height() == 0 ? 720 : jqfb('#social_FACEBOOK').height());
  jqfb('#container').html('<div class="fb-like-box" ' +
  'data-href="https://www.facebook.com/'+ providerUrl +
  '" data-width="' + container_width + '" data-height="'+container_height+'" data-show-faces="false" ' +
  'data-stream="true" data-header="true"></div>');
  FB.XFBML.parse();
}
//
function instagramScript(){
 if($$D('provFeedUrl')){
   getInstagramId($$D('provFeedUrl').value);
 }
 if($$D('provFeedUrl') && $$D('provFeedUrl').value != ""){
   var jsPath = $$D('contextJsPath').value;
   var script = document.createElement("script");
   script.type = "text/javascript";  
   script.src = jsPath + "/js/scout/instafeed.min.js"; //check the instafee.min.js(codeNotProper)
	 if(typeof script != "undefined"){
     document.getElementsByTagName("head")[0].appendChild(script);
   }
   if($$D('instagramCntDiv') && isEmpty($$D('instagramCntDiv').innerHTML)){
     setTimeout(function(){loadInstagram();}, 3000);
   }
 }
}
//
function loadInstagram(){
  var url = context_path + "/jsp/scout/include/instagramFeed.jsp";
	var ajax = new sack();
  ajax.requestFile = url; 
  ajax.onCompletion = function(){onCompleteInstagram(ajax, 'instagramCntDiv')}; 
  ajax.runAJAX();
}
//
function onCompleteInstagram(ajax,divId){
  var response = ajax.response;
  $$D(divId).innerHTML = "";
  $$D(divId).innerHTML = response;
  blockNone(divId, 'block');
  if($$D("script_instagram")){
    eval($$D("script_instagram").innerHTML);
  }
}
//
function getInstagramId(username){
  $jcub.ajax({
    url: "//api.instagram.com/v1/users/search?q="+username+"&access_token=378422198.1677ed0.6a0287f3b8eb40fba5d0c2bf893efd28",
    dataType: 'jsonp',
	   type: 'GET',
    username: username,
    success: function(data){
      for(i = 0; i < data.data.length; ++i){
        $$D('instaId').value = data.data[i].id;
        if(this.username == data.data[i].username){
          $$D('instaId').value = data.data[i].id;
          break;
        }
      }
    }
  });
}
//
function showOpendayPopup(collegeId, eventId){
  var url=""
  sm('newvenue', '650', 500, 'opendayPopup');
  url=contextPath + "/openday-popup.html?collegeId="+collegeId+"&eventId="+eventId;
  blockNone("ajax-div", "block");
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){ displayFormData(ajax); };
  ajax.runAJAX();
}

function showOpendayAjax(collegeId, eventId){
  hm();
  showOpendayPopup(collegeId, eventId);
} 
function opendayDropdown(){
  $jcub("#opendayDropdown").toggle(); 
}
//
function getCookie(cookieName) {
    var name = cookieName + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//
function clearFieldsWhenLoggedOut() {
	var loggedId = getCookie('wcache');   
 if($$D("loggedInUserId") && ($$D("loggedInUserId").value == "0" || $$D("loggedInUserId").value=="")){ 
  $jcub('#chEnquiryFormId input').each(function(){
     if(this.type != 'radio' && this.type != 'hidden'){
       this.value = "";
     }     
     if(this.type == 'checkbox'){
       var chkId = $jcub(this).attr('id');
       $jcub('#'+chkId).prop('checked', false);
     }
   });
   $jcub('#year1').prop('checked', true);
   $jcub("#email").prop('disabled', false);    
		//$jcub('#message').val('');
  }
}
function closeOpenDaydRes(){
  if($$D("odEvntAction")) {    
      if($$D("odEvntAction").value=="false"){        
        var url = contextPath + "/open-days/reserve-place.html?reqAction=exitOpenDaysRes";      
        //$$D("ajax-div").style.display="block";
        var ajaxObj = new sack();
        ajaxObj.requestFile = url;	  
        ajaxObj.onCompletion = function(){setOpendaysExitVal(ajaxObj);};	
        ajaxObj.runAJAX();       
      }
    }    
}
function setOpendaysExitVal(ajaxObj){  
  if(ajaxObj.response!="" && ajaxObj.response=="true"){
    $$D("exitOpenDayResFlg").value = ajaxObj.response; 
    $$D("odEvntAction").value = "true";   
  }  
}
//
function changeLightBoxPos(){
  var scnWidth = $jcub(window).width();
  var mboxWidth = $jcub('#mbox').width();
  if($jcub('#mbox').is(":visible") == true){
    $jcub('#mbox').width((scnWidth - mboxWidth)/2);
  }
}
//
function dimLogReqAndPros(){
  var dim12Val = ($$D('currentSection')) ? ('' != $$D('currentSection').value ? ($$D('currentSection').value).toLowerCase() : "home") : "";
  var ReqInfohref = $jcub('#requestInfoId').attr('href');
  var ReqInfoQryStrVal = ReqInfohref+'?prevpagedim12='+encodeURIComponent(dim12Val);
  $jcub('#requestInfoId').attr('href', ReqInfoQryStrVal);
}
function changeQuestion(id){
  //$rp(".rvbar_cnt").hide();
 // $rp("#star_"+id).show();
   var starArray = ["5star_","4star_","3star_","2star_","1star_"]
 var starwidthId = ["5starWidthId","4starWidthId","3starWidthId","2starWidthId","1starWidthId"];
 var starTextId = ["5StarId","4StarId","3StarId","2StarId","1StarId"];
 for(i=0; i<5;i++){
   var fiveStarValue = $$D(starArray[i]+id).value;
   var styrlrere = $$D(starwidthId[i]).style;
   $$D(starwidthId[i]).style = "width:"+fiveStarValue+"%";
   $$D(starTextId[i]).innerHTML= fiveStarValue+"%";
 }
  var selectdid = $jcub("#dropdownid_"+id).text();
  $jcub("#question1").html("<span>"+selectdid+"</span>"+'<span><i class="fa fa-angle-down"></i></span>');
  $jcub('#question1').attr('title',selectdid);
}
function showSubjectReview(id,collegeId,subjectId){
  var selectdid = $jcub("#subjectdropdownid_"+id).text();
  var questionText = $jcub("#selectedsubject").text();
  if(id == 'all'){
    $$D("selectedsubject").innerHTML = "<span>"+'All Subjects'+"</span>"+'<span><i class="fa fa-angle-down"></i></span>';
    $$D("reviewTitle").innerHTML = "Latest reviews";
  }
  else {
   $$D("selectedsubject").innerHTML = "<span>"+selectdid+"</span>"+'<span><i class="fa fa-angle-down"></i></span>';
   $$D("reviewTitle").innerHTML = "Latest "+selectdid+" reviews";
  }
  //Added for GA event logging in review search page by Prabha on SEP_24_2019_REL
  if(selectdid != ''){
    ga('send', 'event', 'Review page filter', 'Subject', selectdid.trim(), 1, {nonInteraction: true});
  }
  //End of GA event logging
  var contextPath = "/degrees";
  var ajax=new sack();
  var url = contextPath+'/get-subject-review.html?collegeId='+collegeId+"&subjectId="+subjectId;   
  ajax.requestFile = url;	
  ajax.onCompletion = function(){subjectReviewDetails(ajax)};
  ajax.runAJAX();
}
function subjectReviewDetails(ajax){
  var response = ajax.response;
  if(response != null){
    var id =  $$D("subjectReviewDetails");
   $$D("subjectReviewDetails").innerHTML = ajax.response;
  }
  $jcub("#subjectDropDown").css({'display':'none','z-index':'-1'});
}
function setReviewSearchText(curid, searchmsg){var ids = curid.id; if($$(ids).value=='') { $$(ids).value=searchmsg;} }
function clearReviewSearchText(curid, searchmsg){var ids = curid.id;if($$(ids).value==searchmsg){$$(ids).value="";}}
function commonPhraseViewMoreAndLess(viewMoreOrLessBtnId){//Added this for common phrases view less and more pod by Hema.S on 18_DEC_2018_REL
  if("comPhraseViewMore"==viewMoreOrLessBtnId){    
    $jcub( ".extraCP" ).show();
    blockNone("comPhraseViewMore","none");
    blockNone("comPhraseViewLess","block");
  }else{
    $jcub( ".extraCP" ).hide();
    blockNone("comPhraseViewMore","block");
    blockNone("comPhraseViewLess","none");
  }
}
//Modified the the functionality to keyword search specific alone by Sangeeth.S for FEB_12_19 rel
function commonPhrasesListProfilePageURL(formNameId,orderBy,obj){  //Added this for looking specific pod by Hema.S on 18_DEC_2018_REL
  var searchUrl = ""; 
  var finalUrl = "";  
  var keyword = ""; 
  var queryStringUrl = "";
  var collegeName= $$('collegeName').value;
  var collegeId = $$('collegeId').value;
  if("reviewCommonPhraseKwdForm"==formNameId && obj){
    keyword  = obj.innerText;
  }else{
    keyword    = $$('reviewSearchKwd').value;   
  }    
  if(keyword.trim()=="" || keyword=="Something specific?"){
    keyword = "";
  }       
  if(keyword!=""){
   ga('send', 'event', 'Review page filter', 'Keyword', keyword.trim(), 1, {nonInteraction: true});
   var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
      keyword = keyword.replace(reg," ");
      keyword = replaceAll(keyword," ","-");   
      collegeName = replaceAll(collegeName," ","-");
      queryStringUrl = "?university-name="+collegeName.toLowerCase()+"&college-id="+collegeId+"&keyword=" + encodeURIComponent(keyword);
      searchUrl = "/university-course-reviews/search/"+queryStringUrl;
  }  
  if(searchUrl!=""){
    finalUrl = searchUrl;
  }
  if(finalUrl!=""){finalUrl= finalUrl.toLowerCase()};//Added by Sangeeth.S for FEB_12_19 rel lower case of urls  
  if(formNameId!=undefined && $$(formNameId)){
    $$(formNameId).action = finalUrl;
    $$(formNameId).submit();
  }else{
    if(finalUrl ==""){finalUrl="/university-course-reviews/";}	
    location.href = finalUrl;
  }    
  return false;
}
function validateSrchIcn(srchIconId,inputId) {   
   if($jcub('#'+srchIconId) && $jcub('#'+inputId).val().trim()!=""){
     $jcub('#'+srchIconId).removeClass("srch_dis")
   }else{
     $jcub('#'+srchIconId).addClass("srch_dis")
   }
}
function limitCharCountEnquiry(limitField, limitNum) { //Added by Kailash L for adding validation for text area JIRA-817
    var limitFieldLength = trimString(limitField.value).length;
    if(!letters.test(limitField.value)){ // Added this to validate if Non english language is entered in text area by Sujitha V on 17_SEP_2020
      setErrAndSucMsg('textAreaId', 'textwrap enqerr', 'errorTextArea', nonEngLanguageErrorMsg);
    }else if(limitFieldLength < 50){
      setErrAndSucMsg("textAreaId", "textwrap enqerr", "errorTextArea", "Please be as descriptive as possible with your request - min. 50 characters");
    } else{
    	blockNone("errorTextArea","none");
       	$jcub("#textAreaId").removeClass("enqerr");
    }
}

function enquiryFormValidationAjax(selectedField, fieldName){
  var enquiryBoxField = "";
  var postcodeBoxField = "";
  if(isNotNullAndUndex(fieldName) && fieldName == "enquiryBox"){
	enquiryBoxField = selectedField.value;
  } 
  if(isNotNullAndUndex(fieldName) && fieldName == "postcodeBox"){	  
    postcodeBoxField = selectedField.value;
  }
  
  var enquiryFormValidationURL = contextPath+"/enquiry-form-validation-ajax.html?enquiryBoxField="+encodeURIComponent(enquiryBoxField)+"&postcodeBoxField="+encodeURIComponent(postcodeBoxField);
  $jcub.post(enquiryFormValidationURL, function(data) {
    if(data.trim() === "enquiryMessageError"){
      $jcub("#enquiryMessageErrHidden").val("true");
    }else if(data.trim() === "enquiryMessageNoErrors"){
    	 $jcub("#enquiryMessageErrHidden").val("false");
    }
    
    if(data.trim() === "postcodeError"){
      $jcub("#postCodeErrHidden").val("true");
    }else if(data.trim() === "postcodeNoErrors"){
      $jcub("#postCodeErrHidden").val("false");
    }
  });	
}
