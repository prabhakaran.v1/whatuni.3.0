function uniOpenDayResults(o){
  var	uniId = trimString($$((o.id+"_hidden")).value);
  var	uniNameUrl = trimString($$((o.id+"_url")).value);
  var empUni = "We don't have a record of that university! Make sure you've got the name spelled right (you should be able to select from one of the options that appear as you start typing)";
  if(subUni != uniNameUrl || subUni1 != uniNameUrl){
  if(uniNameUrl == '' || uniId == ''){
      alert(empUni);
      resetUniNAME(o);
      return false;
    }
  }else{
    alert(empUni);
    resetUniNAME(o);
    return false;
  }
  opendaySRURL('university', uniId);
}

function autoCompleteUniForSearchResults(e,o){
  ajax_showOptions(o,'opendaysearch',e,"navindicator",'z=','0','');
  //ajax_showOptions(o,'opdkeywordbrowse',e,"opd_search_indicator",'siteflag=opddesktop', '0', '');
}

function multicheckfn(currentObjId){
  var currentObj = document.getElementById(currentObjId);
  var selectedValues = "";
  var locType = "";
  var evntFilterType = "";  

    locType = (document.getElementById(currentObj.id).value).toLowerCase();
    if(document.getElementById("hidden_date").value == ""){
    document.getElementById("hidden_date").value = currentObj.id;
    } else{
    var val = document.getElementById("hidden_date").value;
    val = val + "," + currentObj.id;
    document.getElementById("hidden_date").value = val;    
    }
 
 opendaySRURL("dateFilter",  document.getElementById("hidden_date").value); 
}

function deleteFilters(filterType, filterVal){
  if(filterType == 'dateFilter'){
    deleteDate($$D('hidden_date').value, filterVal);
    opendaySRURL("dateFilter",  document.getElementById("hidden_date").value); 
  }else if(filterType == 'universityFilter'){
    
    opendaySRURL("dateFilter",  document.getElementById("hidden_date").value); 
  }else if(filterType == 'locationFilter'){
  
    opendaySRURL("dateFilter",  document.getElementById("hidden_date").value); 
  }else if(filterType == 'studyLevelFilter'){
  
    opendaySRURL("dateFilter",  document.getElementById("hidden_date").value); 
  }
}

function deleteDate(filterVal, deleteVal){
  var dateArr = filterVal.split(',');
  var selectedVal = "";
  for(var lp = 0; lp < dateArr.length; lp++){
    if(dateArr[lp] != deleteVal){
      selectedVal = selectedVal + "," + dateArr[lp];
    }
  }
  document.getElementById("hidden_date").value = selectedVal;
}


function opendaySRURL(filterId,value){
  //CollegeId, keyword, venuename, studymode
  var searchURL = $$D("openDayUrl").value;
  var filterText = "";
  var filterString = "";
  var separator = "";
  var queryStringSep = "?nd";
  var recentFilter = "";

  var collegeid  = $$D("hidden_collegeid").value;
  var location = $$D("hidden_location").value;
  var pageNo = $$D("pageNo").value;
  var keyword = $$D("hidden_keyword").value;
  var date =  $$D("hidden_date").value;
  var monthSearchFlag =  $$D("hidden_monthSearchFlag").value;
  var evntFilterType = "";

 if(filterId == 'university'){  
    $$D("hidden_collegeid").value = value;
    collegeid  = $$D("hidden_collegeid").value;
  }else if(filterId == 'dateFilter'){
    $$D("hidden_dt").value = $$D("hidden_date").value;
  }
  
 filterString = searchURL;
 filterString += trimString($$D("hidden_dt").value) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "date="+$$D("hidden_dt").value) : ""; 
 filterString += trimString($$D("hidden_odQualification").value) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "level="+$$D("hidden_odQualification").value) : ""; 
 filterString += trimString(collegeid) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "collegeid="+collegeid) : ""; 
 filterString += trimString(location) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "location="+location) : ""; 
 filterString += trimString(pageNo) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "pageNo="+pageNo) : "";
 filterString += trimString(keyword) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "keyword="+keyword) : "";
 filterString += trimString(monthSearchFlag) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "flag="+monthSearchFlag) : "";
 searchURL = filterString;
 if(filterId == 'university'){
   $$D("uniNameForm").action = searchURL.toLowerCase();
	 $$D("uniNameForm").submit();
 } else {
   document.forms[0].action = searchURL.toLowerCase();
	 document.forms[0].submit();
 }
 return false;
}

function loadSlider(empRateMin,empRateMax)
{  
 var start  = 0;
 var end = 100;
 var startonload  = empRateMin;
  var endonload   = empRateMax;
   if(startonload !=null && trimString(startonload) != "" && endonload !=null && trimString(endonload) != ""  ){
    start = parseInt(startonload);
    end = parseInt(endonload);
  }  
 var sjq = jQuery.noConflict();
	sjq(function() {
	sjq( "#slider" ).slider({
	range: true,
	min: 0,
	max: 100,
	step:5,
 animate : true,
	values: [start, end],
	slide: function( event, ui ) {
  sjq("#pemp_rate_display").val(ui.values[0] + "-" + ui.values[1]); 
  sjq("#hidden_empRateMin").val(ui.values[0]);
  sjq("#hidden_empRateMax").val(ui.values[1])
  document.getElementById("empTextBox").style.display = "block";
	}
	});
 sjq("#pemp_rate_display").val(sjq("#slider").slider("values",0) + "-"+sjq("#slider").slider("values", 1));
 sjq("#hidden_empRateMin").val(sjq("#slider").slider("values",0));
 sjq("#hidden_empRateMax").val(sjq("#slider").slider("values", 1)); 
	});
	}

function trimString(str){
str=this !=window? this : str
return str.replace(/^\s+/g,'').replace(/\s+$/g,'')}	

function defaultSelectCheckBox(){
 var locationType = document.getElementById("hidden_locType") ? document.getElementById("hidden_locType").value : "";
 if(locationType!=""){
       var newArr = locationType.split("-");
          for(var i = 0; i < newArr.length; i++ ){
            if(document.getElementById(newArr[i])){
            document.getElementById(newArr[i]).checked = true;
            }
          }   
   }  
}
function removeValue(list, value) {
  list = list.split(',');
  list.splice(list.indexOf(value), 1);
  return list.join(',');
}


function deleteFilterParams(filterType, filterValue){
   var searchURL = $$D("openDayUrl").value;
   var filterString = searchURL;   //

  
   var qualification = $$D("hidden_odQualification").value;
   var collegeid = $$D("hidden_collegeid").value;
   var location = $$D("hidden_location").value;
   var keyword = $$D("hidden_keyword").value;
   var date = $$D("hidden_date").value;
   var monthSearchFlag =  $$D("hidden_monthSearchFlag").value;
   //
   if(filterType == 'date'){
    var selectedDate = $$D('hidden_date').value;
    date = removeValue(selectedDate, filterValue);
   }
   
   if( filterType == 'russell'){
     russell = "";   
   }else if(filterType == 'campus' ){
      campusType = "";
   }else if(filterType == 'empRate' ){
      empRateMax = "";
      empRateMin = "";
   }else if(filterType == 'qualification'){
     qualification = "";
   }else if(filterType == 'college'){
     collegeid = "";
   }else if(filterType == 'location'){
     location = "";
   }
  
  
   filterString += trimString(qualification) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "level="+qualification) : "";
   filterString += trimString(collegeid) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "collegeid="+collegeid) : "";
   filterString += trimString(location) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "location="+location) : "";
   filterString += trimString(keyword) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "keyword="+keyword) : "";
   filterString += trimString(date) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "date="+date) : "";
   filterString += trimString(monthSearchFlag) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "flag="+monthSearchFlag) : "";
   searchURL = filterString;
   document.forms[0].action = searchURL.toLowerCase();
	  document.forms[0].submit();  
}

function clearDefaultText1(thisobj, defvalue){
  if(thisobj.value == defvalue){
    thisobj.value = '';
  }
}
function setDefaultText1(thisobj, defvalue){
 if(thisobj.value == ''){
   thisobj.value = defvalue;
 }
}

function closeOpenDay(from){
  if(from=='fromlogin'){
   document.location=$$D("fullOpenDayUrlwithparam").value;   
  }
  hm();
}

function showFilterBlock(filterDiv){
 if(filterDiv == "Y"){
   document.getElementById("filterTop").style.display = 'block';
 }
}

function toggleMenu(id, link) {
  var e = document.getElementById(id);
  var l = document.getElementById(link);
  if (e.style.display == 'block' || e.style.display == '' ) {   
    e.style.display = 'none';
    l.className = 'rdat';
  } else {
    e.style.display = 'block';
    l.className = 'rdat actct';
  }
}

var sjq = jQuery.noConflict();
sjq(document).ready(function(){
  sjq('.ucod_Cnr').click(function(e){    
    if(e.target.className != 'res_place' && e.target.className != 'savTag' && e.target.className != 'fa fa-caret-right savTag' && e.target.className != 'far fa-calendar-alt savTag'){
      window.location.href = sjq(this).closest('div.ucod_Cnr').find('input').val();
    }
  });
  //Added to hide and show dropdown filter for tablet and mobile by Sangeeth.S
  var width = document.documentElement.clientWidth;  
  if(width < 1024){    
    sjq('#studyLevel, #location, #month').click(function(e){      
      var id = sjq(this).attr('id');      
      var drpdnId="drp_"+id;
      var drpIdNone="";
      if("drp_month"==drpdnId){          
          drpIdNone="#drp_location, #drp_studyLevel";
        }else if("drp_location"==drpdnId){           
           drpIdNone="#drp_month, #drp_studyLevel";
        }else if("drp_studyLevel"==drpdnId){           
           drpIdNone="#drp_month, #drp_location";
      }      
      if(sjq('#drp_'+id).css('display') == 'none'){          
        sjq('#drp_'+id).css({'display':'block', 'z-index':'2'}); //Added z-index to fix alignment issue for the 23_oct_18 rel by Sangeeth.S 
        if(sjq(drpIdNone)){sjq(drpIdNone).css({'display':'none', 'z-index':''});} 
      }else{
        sjq('#drp_'+id).css({'display':'none', 'z-index':''});          
      }     
    });
  }
  //
});
