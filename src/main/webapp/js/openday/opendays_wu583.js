var contextPath = "/degrees";
//
var opdup = jQuery.noConflict();
opdup(window).scroll(function(){ // scroll event 
    lazyloadetStarts();    
});
opdup(document).ready(function(){
  lazyloadetStarts();
  opdup(window).scroll(function(){ // scroll event       
    //load articles pod on scroll on 13_06_2017, By Thiyagu G.    
    if($$("scrollStop")){
      if(opdup("#scrollStop").val() != 1){
        opdup("#scrollStop").val(1);
        loadArticlesPodJS('OPENDAY_LANDING_PAGE');
      }
      setTimeout(function() {articlesSlider();}, 800);
    }
  });
  if($$("scrollStop")){
    opdup("#scrollStop").val(0);
  }
  $$('sky').display = "none";
  setBannerHgt();
  
});
function setBannerHgt(){
  if($$('opnHead') && $$('opnHro')){
    var opnHgt =  $$('opnHead').clientHeight + $$('opnHro').clientHeight;
    opnHgt = opnHgt + 100;
    //alert("opnHgt--1-->"+opnHgt);
    //opdup('#sky').css('top:'+opnHgt);
    if($$('cookiePopup')){opnHgt = opnHgt + $$('cookiePopup').clientHeight;}
    //alert("opnHgt--2-->"+opnHgt);
    document.getElementById("sky").style.top = opnHgt+'px';
    $$('sky').display = "block";
 }
}
opdup(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
  setTimeout(function() {
    opdup(window).trigger('resize');        
  }, 500);
}
opdup(window).resize(function() { 
  setBannerHgt();
  mobileSpecific("openDayHeroImage");
});
function regionSearch(region){
   var finaUrl = "/open-days/search";
   finaUrl = finaUrl + "/?location="+region;
   window.location.href = finaUrl.toLowerCase();
}
function getWeekAndDaysData(obj, cuMonth) {
   var ajax = new sack();
   url = contextPath + "/opendaysWeekAndDateInfoAjax.html?month="+cuMonth;
   ajax.requestFile = url;
   ajax.onCompletion = function(){ loadWeekAndDaysDataAjax(ajax, obj, cuMonth); };
   ajax.runAJAX();
}
function loadWeekAndDaysDataAjax(ajax, obj, cuMonth){
   $$("weekAndDays").innerHTML = "";
   var response = ajax.response;
   $$("weekAndDays").innerHTML = response;
   clearLiClasses();
   var actId = "li_"+cuMonth;
   $$(actId).className = "act";
   prepopulateMonthDrp();
   lazyloadetStarts();
}
function getDaysData(obj, cuMonth, stDate, enDate) {
   var ajax = new sack();
   url = contextPath + "/opendaysWeekAndDateInfoAjax.html?month="+cuMonth+"&startDate="+stDate+"&endDate="+enDate;
   ajax.requestFile = url;
   ajax.onCompletion = function(){ loadDaysDataAjax(ajax, obj, cuMonth); };
   ajax.runAJAX();
}
function loadDaysDataAjax(ajax, obj, cuMonth){
   $$("weekAndDays").innerHTML = "";
   var response = ajax.response;
   $$("weekAndDays").innerHTML = response;
   var array_of_li =  document.querySelectorAll("ul li");
   var idTxt = "li_";
   var actId = 0;
   clearLiClasses();
   actId = "li_"+cuMonth;
   $$(actId).className = "act";
   prepopulateMonthDrp();
   var width = document.documentElement.clientWidth;
   if (width >= 992) {
     if(obj.id == "weekNext"){
       if($$("midMonth").value != $$("nextMonth").value){     
         moveToNextMonth();
       }
    }   
    if(obj.id == "weekPrev"){    
      if($$("prevMonth").value != $$("midMonth").value){
        moveToPrevMonth();
      }
     }
   }
   lazyloadetStarts();
}
function clearLiClasses(){
   var array_of_li =  document.querySelectorAll("ul li");
   var i = 0;
   for(i=0; i<array_of_li.length;i++){
     if(document.getElementsByTagName("LI")[i].className == "act"){
        document.getElementsByTagName("LI")[i].className = "";
     }
   }
}
function moveToNextMonth(){
    opdup("#weekNext").click(function() {
        opdup(".mov_nxt").click();
    });
}
function moveToPrevMonth(){    
    opdup("#weekPrev").click(function() {        
        opdup(".mov_pre").click();
    });
}
function $opd(id){
  return document.getElementById(id);
}
opdup(document).ready(function(){     
  var totalPos;
  if($$("totalPos")){
    if($$("totalPos").value != ""){
      totalPos = parseInt($$("totalPos").value);
    }  
  }
  var odStatusPos;
  if($$("odStatusPos")){
    if($$("odStatusPos").value != ""){
      odStatusPos = parseInt($$("odStatusPos").value);
    }  
  }  
  if($$("totalPos")){
  var startPos = 0;  
  if(odStatusPos > 8){
      var tmpStartPos = odStatusPos - 6;	
      var tmpEndPos = odStatusPos + 6;  
      if( tmpEndPos>totalPos){
          if(odStatusPos > 12){
            startPos = totalPos - 12;	
          }		
          if(odStatusPos > 12 && odStatusPos <= startPos){
            startPos = odStatusPos - 1;
          }
      }else{
        startPos = tmpStartPos;      
      }
  }  
  var width = document.documentElement.clientWidth;
  if (width > 992){
    opdup(".op_mth_slider").jCarouselLite({
    btnNext: ".mov_nxt",
    btnPrev: ".mov_pre",
    speed: 600,
    visible:12,      
    start: parseInt(startPos), 
    circular: false,
    afterEnd: function(){
        opdup('#leftValue').val(opdup('.op_mth_slider ul').css("left"));
     }
    });
    opdup('#leftValue').val(opdup('.op_mth_slider ul').css("left"));
  }
    }
    opdup(document).on( "click", ".op_day_cnr .day_tit_cnr", function() {
    opdup(this).next(".op_date_cnr").toggleClass('show_off');
    opdup(this).find(".fa").toggleClass('fa-minus-circle');
    lazyloadetStarts();
  });
  prepopulateMonthDrp();
});
      
opdup(document).ready(function(){  //Added for wu_539 07-APR-2015 by Karthi for mobile dropdown header
  opdup("#hd1").removeAttr("style");
	opdup('.lft_nav .brw').click(function(){
		opdup(this).toggleClass('act');
		opdup(this).parents().next().toggleClass('act');
	});
  /*Month Dropdown*/
    opdup(".select_box_month").click(function(){
      opdup("#select_cont").slideToggle();
    });  
    opdup(".op_mth_slider ul li a").click(function(){
      opdup(".select_box_month .sld_mth ").html(opdup(this).html())
      opdup("#select_cont").slideToggle();
    });	
  /*Month Dropdown*/
  if(document.getElementById("odLocMap")){
    mapLoad();
  }
  opdup(".select_box_month").one("click", function(){
    opdup('#select_cont').scrollTop(opdup("#select_cont li:nth-child("+(parseInt($$("odStatusPos").value)+1)+")").position().top - opdup('#select_cont li:first').position().top);
  });
  opdup('.ucod_Cnr').click(function(e){
    if(opdup('#GAMPagename').val() == 'opendaybrowse.jsp' || opdup('#GAMPagename').val() == 'opendaysearchresults.jsp' && opdup('#GAMPagename').val() == 'viewopendaysnew.jsp'){
    if(e.target.className != 'res_place' && e.target.className != 'savTag' && e.target.className != 'fa fa-caret-right savTag' && e.target.className != 'red_act savTag' &&  e.target.className != 'far fa-calendar-minus savTag' && e.target.className != 'far fa-calendar-alt savTag'){
      window.location.href = opdup(this).closest('div.ucod_Cnr').find('input').val();
    }
    }
  });
});

function prepopulateMonthDrp(){//Added for wu_539 07-APR-2015 by Karthi to prepopulate value in dropdown
  if(opdup(".op_mth_slider .act a").html() != null && opdup(".op_mth_slider .act a").html().trim() != ""){ 
    opdup(".select_box_month .sld_mth ").html(opdup(".op_mth_slider .act a").html());
  }
}



function openDayOrientation(){//Added for wu_539 07-APR-2015 by Karthi for orientation of tablet view, triggers when portait to landscape 
  var totalPos;
  if($$("totalPos")){
    if($$("totalPos").value != ""){
      totalPos = parseInt($$("totalPos").value);
    }  
  }
  var odStatusPos;
  if($$("odStatusPos")){
    if($$("odStatusPos").value != ""){
      odStatusPos = parseInt($$("odStatusPos").value);
    }  
  }  
  if($$("totalPos")){
    var startPos = 0;  
    if(odStatusPos > 8){
      var tmpStartPos = odStatusPos - 6;	
      var tmpEndPos = odStatusPos + 6;  
      if( tmpEndPos>totalPos){
          if(odStatusPos > 12){
            startPos = totalPos - 12;	
          }		
          if(odStatusPos > 12 && odStatusPos <= startPos){
            startPos = odStatusPos - 1;
          }
      }else{
        startPos = tmpStartPos;      
      }
    }
    var width = opdup('#leftValue').val();
    if(width.trim() == ''){
      opdup(".op_mth_slider").jCarouselLite({
        btnNext: ".mov_nxt",
        btnPrev: ".mov_pre",
        speed: 600,
        start: parseInt(startPos),
        visible:12,
        circular: false,
        afterEnd: function(){
          opdup('#leftValue').val(opdup('.op_mth_slider ul').css("left"));
        }
      });
      opdup('#leftValue').val(opdup('.op_mth_slider ul').css("left"));
    }else{
      opdup('.op_mth_slider ul').css({"left": width, "width": totalPos*77});
    }
  }
}
    
function mapLoad(){//Added for wu_539 07-APR-2015 by Karthi for opendays map initializing 
  opdup.mapster.isTouch = false;
  opdup('#shape1_img').mapster({
    fillColor: '435772', 
    stroke: false, 
    singleSelect: true,
    fillOpacity : 1
  });
  var wwidth1=opdup(window).width();	
  if(opdup('#shape1')) {
    opdup('#shape1 area').each(function() {
    var id = opdup(this).attr('id');					
    opdup(this).mouseover(function() {					
      //Dynamic position of tool tip in mobile devices
      if(wwidth1<1000)
      {
        var rectcoords = opdup(this).attr('coords');
        coords = rectcoords.split(',');
        var box = {
          left: parseInt(coords[0],10),
          top: parseInt(coords[1],10),
          width:  parseInt(coords[2],10)-parseInt(coords[0],10),
          height:  parseInt(coords[3],10)-parseInt(coords[1],10)
        };					
        var imgPos = opdup('#shape1_img').offset();
        $x = imgPos.left + box.left + box.width/2 - 65; // 65 is the info width/2												
        //$y = imgPos.top + box.top -20 -40 -1; // 20 is the animation, 160 is the
        $y = box.top -20 -40 -1; // 20 is the animation, 160 is the													
        opdup('.tip_'+id).css({ left: + $x, top: + $y });	
        opdup('.tip_'+id).css("right","initial");						
      }
      opdup('.tip_'+id).fadeIn();            					 
    });			
    opdup(this).mouseout(function() {
      var id = opdup(this).attr('id');
      opdup('.tip_'+id).fadeOut();
    });								
  });
 }	
}
function removeOpendays(eventId,divId){
  var url = "/degrees/removeOpendDay.html?eventId="+eventId;   
  var divStyle = "";
  if(opdup("#"+divId)){divStyle = opdup("#"+divId).css( "display");}
  var ajaxObj = new sack();
      ajaxObj.requestFile = url;	      
      ajaxObj.onCompletion = function(){removeOpendaysResponse(ajaxObj, divStyle,divId);};	
      ajaxObj.runAJAX(); 
}

function removeOpendaysResponse(ajax,divStyle,divId){
  var resp = ajax.response;
  
  if(resp!=null && resp!=""){
    $$D("futureOpenDaysDiv").innerHTML = resp;
  }
  if("flex"==divStyle){
   if(divId=="goingtopodid"){
   viewMoreFod();
   }
   else if(divId=="havebeen"){viewMorePod();}
  }
 // if(opdup("#"+divId)){$$D(divId).style.display = divStyle; }
 // opdup(window).scrollTop(opdup(window).scrollTop()+1);
  var divPosition =  opdup("#futureOpenDaysDiv").offset().top; 
  opdup('html, body').animate({scrollTop: divPosition});
}
function viewMoreResult(value,type){
  var hs = jQuery.noConflict();
  var divPosition = hs("#divcodsec"+type).offset().top;
  if(value== 'plus'){
    $$D("view_more_plus_"+type).style.display="none";
    $$D("view_more_minus_"+type).style.display="block";
  }else{
    $$D("view_more_plus_"+type).style.display="block";
    $$D("view_more_minus_"+type).style.display="none";  
    divPosition =  hs("#"+type+"OpenDaysDiv").offset().top +100; 
  }
  hs("#divcodsec"+type).animate({ height: 'toggle'},"slow");  
  hs('html, body').animate({scrollTop: divPosition});
}
function landingViewMoreResult(index){
  if($$D("view_more_"+index)){
    if($$D("view_more_"+index).style.display=='block' ||  $$D("view_more_"+index).style.display==''){
       blockNone("view_more_"+index , 'none');
       blockNone("view_more_plus_"+index , 'block');
       blockNone("view_more_minus_"+index , 'none');       
    }else{
       blockNone("view_more_"+index , 'block');
       blockNone("view_more_plus_"+index , 'none');
       blockNone("view_more_minus_"+index , 'block');
    }
  }
}
function closeOpenDaydRes(){
  if($$d("resUniIds")) {
    if($$d("odEvntAction")) {    
      if($$d("odEvntAction").value=="false"){        
        $$d("resUniIds").value ="";
        var url = "/degrees/open-days/reserve-place.html?reqAction=exitOpenDaysRes";      
        $$d("ajax-div").style.display="block";
        var ajaxObj = new sack();
        ajaxObj.requestFile = url;	  
        ajaxObj.onCompletion = function(){setOpendaysExitVal(ajaxObj);};	
        ajaxObj.runAJAX();       
      }
    }    
    hm();
  }
}
function setOpendaysExitVal(ajaxObj)
{  
  if(ajaxObj.response!="" && ajaxObj.response=="true"){
    $$d("exitOpenDayResFlg").value = ajaxObj.response; 
    $$d("odEvntAction").value = "true";   
  }  
}
function GAEventTracking(category, action, opt_label, opt_value){
  eventTimeTracking();
  var booleanValue = true;
  if(action=="reserve-place"){
    booleanValue = false;
  }
  opt_value = parseInt(1);  
  ga('send', 'event', category , action , opt_label , opt_value , {nonInteraction: booleanValue}); //Added by Amir for UA logging for 24-NOV-15 release  
}
function closeOpenDay(from){
  if(from=='fromlogin'){
    document.location=$$D("fullOpenDayUrlwithparam").value;   
  }
  hm();
}
function closeOpenDayFn(from){
  document.location=from;   
  hm();
}
function opendayFb(date, url, gaCollegeName){ //Post to facebook for openday :: Prabha :: 31_05_16
 var opendayUrl = window.location.hostname + url;
 var collegeDispName = "";
 if($$D('opendaySaveAjaxCollegeName')){collegeDispName = $$D('opendaySaveAjaxCollegeName').value;}
 var quote = "Im going to the open day at " + collegeDispName + " on " + date + " after finding it on Whatuni.com";
 if(gaCollegeName != ""){
   eventTimeTracking();
   ga('send', 'event', 'Post to facebook', 'save to calendar', gaCollegeName);   
   window.open('https://www.facebook.com/sharer.php?u='+encodeURIComponent(opendayUrl)+'&quote='+encodeURIComponent(quote), 'sharer', 'toolbar=0,width=626,height=436');
 }
 return false;
}