function uniOpenDayResults(o){
  var	uniId = trimString($$((o.id+"_hidden")).value);
  var	uniNameUrl = trimString($$((o.id+"_url")).value);
  var empUni = "We don't have a record of that university! Make sure you've got the name spelled right (you should be able to select from one of the options that appear as you start typing)";
  if(subUni != uniNameUrl || subUni1 != uniNameUrl){
  if(uniNameUrl == '' || uniId == ''){
      alert(empUni);
      resetUniNAME(o);
      return false;
    }
  }else{
    alert(empUni);
    resetUniNAME(o);
    return false;
  }
  opendaySRURL('university', uniId);
}

function autoCompleteUniForSearchResults(e,o){
	ajax_showOptions(o,'z',e,"navindicator",'z=','0','382');
}

function multicheckfn(currentObjId){
  var currentObj = document.getElementById(currentObjId);
  var selectedValues = "";
  var locType = "";
  var evntFilterType = "";  
  if(currentObj != null && currentObj.checked == true){
    locType = (document.getElementById(currentObj.id).value).toLowerCase();
    if(document.getElementById("hidden_date").value == ""){
    document.getElementById("hidden_date").value = currentObj.id;
    } else{
    var val = document.getElementById("hidden_date").value;
    val = val + "," + currentObj.id;
    document.getElementById("hidden_date").value = val;    
    }
  }else  if(currentObj != null && currentObj.checked == false){
       if( document.getElementById("hidden_date").value != ""){
        var selArray = document.getElementById("hidden_date").value;
        var arr = selArray.split("-");
          for(var i = 0; i <arr.length; i++ ){
              if(arr[i] != currentObj.id){
                  if(selectedValues != ""){
                    selectedValues = selectedValues + "," + arr[i];
                  }else{
                    selectedValues = arr[i];
                  }
              }
          }
      }
     document.getElementById("hidden_date").value = selectedValues;   
  }  
 opendaySRURL("dateFilter",  document.getElementById("hidden_date").value); 
}

function deleteFilters(filterType, filterVal){
  if(filterType == 'dateFilter'){
    deleteDate($$D('hidden_date').value, filterVal);
    opendaySRURL("dateFilter",  document.getElementById("hidden_date").value); 
  }else if(filterType == 'universityFilter'){
    
    opendaySRURL("dateFilter",  document.getElementById("hidden_date").value); 
  }else if(filterType == 'locationFilter'){
  
    opendaySRURL("dateFilter",  document.getElementById("hidden_date").value); 
  }else if(filterType == 'studyLevelFilter'){
  
    opendaySRURL("dateFilter",  document.getElementById("hidden_date").value); 
  }
}

function deleteDate(filterVal, deleteVal){
  var dateArr = filterVal.split(',');
  var selectedVal = "";
  for(var lp = 0; lp < dateArr.length; lp++){
    if(dateArr[lp] != deleteVal){
      selectedVal = selectedVal + "," + dateArr[lp];
    }
  }
  document.getElementById("hidden_date").value = selectedVal;
}


function opendaySRURL(filterId,value){
  //CollegeId, keyword, venuename, studymode
  var searchURL = $$D("openDayUrl").value;
  var filterText = "";
  var filterString = "";
  var separator = "";
  var queryStringSep = "?nd";
  var recentFilter = "";

  var collegeid  = $$D("hidden_collegeid").value;
  var location = $$D("hidden_location").value;
  var pageNo = $$D("pageNo").value;
  var keyword = $$D("hidden_keyword").value;
  var date =  $$D("hidden_date").value;
  var monthSearchFlag =  $$D("hidden_monthSearchFlag").value;
  var evntFilterType = "";

 if(filterId == 'empRate'){  
     var empMax = $$D("hidden_empRateMax").value;
     var empMin = $$D("hidden_empRateMin").value;      
     /*if((empMax == "" || empMax == "100"  ) &&  (empMin == "0" || empMin == "" )){
        alert("Please choose a range");
        return false;
      }else{*/
        $$D("hidden_empRateMax").value  = trimString(replaceSpecialCharacter($$("hidden_empRateMax").value));    
        $$D("hidden_empRateMin").value  = trimString(replaceSpecialCharacter($$("hidden_empRateMin").value));
      //}
  }else if(filterId == 'locationType'){
    $$D("hidden_locType").value = $$D("hidden_locationType").value;
  }  else if(filterId == 'university'){  
    $$D("hidden_collegeid").value = value;
    collegeid  = $$D("hidden_collegeid").value;
  }else if(filterId == 'dateFilter'){
    $$D("hidden_dt").value = $$D("hidden_date").value;
  }
  
 filterString = searchURL;
 filterString += trimString($$D("hidden_dt").value) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "date="+$$D("hidden_dt").value) : ""; 
 filterString += trimString($$D("hidden_odQualification").value) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "level="+$$D("hidden_odQualification").value) : ""; 
 filterString += trimString($$D("hidden_campusType").value) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "cmps="+$$D("hidden_campusType").value) : "";
 filterString += trimString($$D("hidden_locType").value) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "lc-tpe="+$$D("hidden_locType").value) : ""; 
 filterString += trimString(collegeid) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "collegeid="+collegeid) : ""; 
 filterString += trimString(location) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "location="+location) : ""; 
 filterString += trimString(pageNo) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "pageNo="+pageNo) : "";
 filterString += trimString(keyword) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "keyword="+keyword) : "";
 filterString += trimString(monthSearchFlag) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "flag="+monthSearchFlag) : "";
 searchURL = filterString;
 if(filterId == 'university'){
   $$D("uniNameForm").action = searchURL.toLowerCase();
	 $$D("uniNameForm").submit();
 } else {
   document.forms[0].action = searchURL.toLowerCase();
	 document.forms[0].submit();
 }
 return false;
}

function loadSlider(empRateMin,empRateMax)
{  
 var start  = 0;
 var end = 100;
 var startonload  = empRateMin;
  var endonload   = empRateMax;
   if(startonload !=null && trimString(startonload) != "" && endonload !=null && trimString(endonload) != ""  ){
    start = parseInt(startonload);
    end = parseInt(endonload);
  }  
 var sjq = jQuery.noConflict();
	sjq(function() {
	sjq( "#slider" ).slider({
	range: true,
	min: 0,
	max: 100,
	step:5,
 animate : true,
	values: [start, end],
	slide: function( event, ui ) {
  sjq("#pemp_rate_display").val(ui.values[0] + "-" + ui.values[1]); 
  sjq("#hidden_empRateMin").val(ui.values[0]);
  sjq("#hidden_empRateMax").val(ui.values[1])
  document.getElementById("empTextBox").style.display = "block";
	}
	});
 sjq("#pemp_rate_display").val(sjq("#slider").slider("values",0) + "-"+sjq("#slider").slider("values", 1));
 sjq("#hidden_empRateMin").val(sjq("#slider").slider("values",0));
 sjq("#hidden_empRateMax").val(sjq("#slider").slider("values", 1)); 
	});
	}

function trimString(str){
str=this !=window? this : str
return str.replace(/^\s+/g,'').replace(/\s+$/g,'')}	

function defaultSelectCheckBox(){
 var locationType = document.getElementById("hidden_locType").value;
 if(locationType!=""){
       var newArr = locationType.split("-");
          for(var i = 0; i < newArr.length; i++ ){
            if(document.getElementById(newArr[i])){
            document.getElementById(newArr[i]).checked = true;
            }
          }   
   }  
}
function removeValue(list, value) {
  list = list.split(',');
  list.splice(list.indexOf(value), 1);
  return list.join(',');
}


function deleteFilterParams(filterType, filterValue){
   var searchURL = $$D("openDayUrl").value;
   var filterString = searchURL;   //

   var loctype = document.getElementById("hidden_locType").value;
   var qualification = $$D("hidden_odQualification").value;
   var collegeid = $$D("hidden_collegeid").value;
   var location = $$D("hidden_location").value;
   var keyword = $$D("hidden_keyword").value;
   var date = $$D("hidden_date").value;
   var monthSearchFlag =  $$D("hidden_monthSearchFlag").value;
   //
   if(filterType == 'date'){
    var selectedDate = $$D('hidden_date').value;
    date = removeValue(selectedDate, filterValue);
   }
   
   if( filterType == 'russell'){
     russell = "";   
   }else if(filterType == 'campus' ){
      campusType = "";
   }else if(filterType == 'empRate' ){
      empRateMax = "";
      empRateMin = "";
   }else if(filterType == 'locationType' ){
     loctype = filterValue;
   }else if(filterType == 'qualification'){
     qualification = "";
   }else if(filterType == 'college'){
     collegeid = "";
   }else if(filterType == 'location'){
     location = "";
   }
  
  
   filterString += trimString(qualification) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "level="+qualification) : "";
   filterString += trimString(collegeid) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "collegeid="+collegeid) : "";
   filterString += trimString(location) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "location="+location) : "";
   filterString += trimString(keyword) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "keyword="+keyword) : "";
   filterString += trimString(date) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "date="+date) : "";
   filterString += trimString(monthSearchFlag) != "" ? (((filterString.indexOf('?') > -1) ? "&" : "?") + "flag="+monthSearchFlag) : "";
   searchURL = filterString;
   document.forms[0].action = searchURL.toLowerCase();
	  document.forms[0].submit();  
}

function clearDefaultText1(thisobj, defvalue){
  if(thisobj.value == defvalue){
    thisobj.value = '';
  }
}
function setDefaultText1(thisobj, defvalue){
 if(thisobj.value == ''){
   thisobj.value = defvalue;
 }
}

function closeOpenDay(from){
  if(from=='fromlogin'){
   document.location=$$D("fullOpenDayUrlwithparam").value;   
  }
  hm();
}

function showFilterBlock(filterDiv){
 if(filterDiv == "Y"){
   document.getElementById("filterTop").style.display = 'block';
 }
}

function toggleMenu(id, link) {
  var e = document.getElementById(id);
  var l = document.getElementById(link);
  if (e.style.display == 'block' || e.style.display == '' ) {   
    e.style.display = 'none';
    l.className = 'rdat';
  } else {
    e.style.display = 'block';
    l.className = 'rdat actct';
  }
} 