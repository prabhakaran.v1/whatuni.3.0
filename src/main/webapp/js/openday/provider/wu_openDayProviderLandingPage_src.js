var jq = jQuery.noConflict();

var vidpct = ['', '10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%'];
var videoRanMap = {};
var eventCategory = "Book Open Day Landing";
var deviceWidth = jq(window).width() || document.documentElement.clientWidth;
var currentPageUrl = window.location.href;


function isBlankOrNullString(str) {
	var flag = true;
	if (str.trim() != "" && str.trim().length > 0) {
		flag = false;
	}
	return flag;
}

function isBlankOrNull(value) {
	var flag = true;
	if (value != null && value != "") {
		flag = false;
	}
	return flag;
}

function $$D(docid) {
	return document.getElementById(docid);
}

function isNotNullAndUndef(variable) {
	return (variable !== null && variable !== undefined && variable !== '');
}

function isNotNullAndUndex(value) {
	return (value != '' && value != undefined && value != null && value != 'null');
}

jq(window).scroll(function() {
	opendayProviderLandinOnScrollEvent();
});

jq(document).ready(function() {
	jq(document).on("click", "#switchTab li", function(event) {
		var queryString = "";
		var url = "/degrees/switch-profile-network.html";
		var openDayProviderLandingURL = jq("#openDayProviderLandingURL").val();
		var id = jq(this).attr('id');
		if (id == "undergraduate") {
			queryString = "?nwId=2";
		} else {
			queryString = "?nwId=3";
		}
		if (!(jq("#" + id).hasClass("opdBookTabActv"))) {
			var urlOpen = url + queryString;
			jq.post(urlOpen, function(data) {
			if (deviceWidth < 480){		
			    window.location.href = openDayProviderLandingURL + "#" + id;
			} else {
			  window.location.href = openDayProviderLandingURL;
			}
			});
		}
	});

	jq(document).on("click", ".radio", function(event) {
		var id = jq(this).attr('id');
		var eventsPosition = jq('#' + id).attr('data-id');
		var fieldClicked = jq('#' + id).attr('data-lightBoxId');
		var gaEventTime = jq('#' + id).attr('data-gaEventTime');
		var qualType = jq('#' + id).attr('data-qualType');
		if (eventsPosition.indexOf("lightBox") != -1) {
			jq('#openDayEvent_' + eventsPosition).prop("checked", true);
			jq("#selectButton").removeClass("disab_btn");
		} else {
			var eventIdProviderLanding = jq('#openDayEvent_' + eventsPosition).val();
			openDayPageUrl(eventIdProviderLanding);
		}

		openDayProviderLandingGALogging(fieldClicked, '', gaEventTime, qualType);
	});

	jq(document).on("click", ".evntBookBtn a", function(event) {
		var id = jq(this).attr('id');
		if (!(jq("#" + id).hasClass("bkopnday_disabld"))) {
			var bookingUrl = jq('#' + id).attr('data-url');
			var eventAction = jq('#' + id).attr('data-eventAction');
			var collegeName = jq('#' + id).attr('data-collegeName');
			var websitePrice = jq('#' + id).attr('data-websitePrice');
			var collegeId = jq('#' + id).attr('data-collegeId');
	        var subOrderItemId = jq('#' + id).attr('data-subOrderItemId');
	        var networkId = jq('#' + id).attr('data-networkId');
	        var openDay = jq('#' + id).attr('data-openDay');
	        var openMonth = jq('#' + id).attr('data-openMonth');

			GAEventTracking('open-days-type', eventAction, collegeName);
			GAEventTracking('open-days', 'reserve-place', collegeName, '1');
			GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', collegeName, websitePrice);
			addOpenDaysForReservePlace(openDay, openMonth, collegeId, subOrderItemId, networkId);
	        cpeODReservePlace("", collegeId, subOrderItemId, networkId, bookingUrl);
			window.open(bookingUrl);
		} else{
			var eventsPosition = id.split('_').pop().trim(); 
			var eventIdProviderLanding = jq('#openDayEvent_' + eventsPosition).val();
			openDayPageUrl(eventIdProviderLanding);
		}
		event.stopPropagation();
	});
	jq(document).on("click", "#openDayCloseButton", function(event) {
		jq("#revLightBox").removeClass("opdlpnw_ltbox");	
	});
});

jq(window).on('load',function() {
   if(deviceWidth < 480){
	  if(currentPageUrl.indexOf('?') > -1) {
		 var selectedRadio = jq('input[type=radio][name=openDayEvents]:checked').attr('id');
		 var eventsPosition = selectedRadio.split('_').pop().trim();	 
		 jq('html, body').animate({
            scrollTop: jq("#eventLabelId_"+eventsPosition).offset().top
		 }, 1000);   
	  }
   } 
   var eventTime = jq("#gaEventTime").val();
   var qualTime = jq("#gaQualType").val();
   openDayProviderLandingGALogging('select_open_day', '', eventTime, qualTime);
});

/*Light box function starts */
function ajaxOpenDayLightBox(collegeId, displayOpenDayCount, networkId) {
	var urlOpen = "/degrees/ajax/opendaylightbox.html?" + "collegeId=" + collegeId + "&displayOpenDayCount=" + displayOpenDayCount + "&networkId=" + networkId;
	jq.post(urlOpen, function(data) {
		jq("html").addClass("rvscrl_hid");
		jq("#revLightBox").html(data);
		jq("#revLightBox").addClass("rev_lbox opdlpnw_ltbox");
		jq(".rev_lbox").css({
			'z-index': '111111',
			'visibility': 'visible'
		});
		jq(".rev_lbox").addClass("fadeIn").removeClass("fadeOut");
		var selectedEventId = jq('input[type=radio][name=openDayEvents]:checked').attr('id');
		jq("#selectedEventId").val(selectedEventId);
	});
}

function openDayPageUrl(eventIdProviderLanding) {
	var eventId = jq("input[name='openDayEvents']:checked").val();
	if (isNotNullAndUndef(eventIdProviderLanding)) {
		eventId = eventIdProviderLanding
	}
	var eventTimelb = jq("#openDayEventsEventTime_lightBox").val();
	if (isNotNullAndUndef(eventTimelb)) {
		if (eventTimelb.endsWith("/")) {
			eventTimelb = eventTimelb.replace('/', '');
		}
		GANewAnalyticsEventsLogging(eventCategory, "Selected - Lightbox", eventTimelb);
	}
	var openDayProviderLandingURL = jq("#openDayProviderLandingURL").val();
    jq('#loaderTopNavImg').show();
	window.location.href = openDayProviderLandingURL + "?eventId=" + eventId;	
//    if (deviceWidth < 480){
//    	window.location.href = openDayProviderLandingURL + "?eventId=" + eventId + "#eventLabelId_" + eventsPosition;	
//    } else {
//    	window.location.href = openDayProviderLandingURL + "?eventId=" + eventId;	
//    }
	
}
/*Light box function ends */

/*function openDayBookingFormUrl(){
  var eventId = jq("input[name='openDayEvents']:checked").val();
  var collegeName = jq("#collegeName").val();
  var collegeId = jq("#collegeId").val();
  if(isNullAndUndef(eventId)){
    window.location.href = "/degrees/openday/booking-form/" + collegeName + "/" + collegeId + ".html?eventId=" + eventId;
  }
}*/

function openDayProviderLandingGALogging(fieldClicked, pdfName, eventTime, qualType) {
	//eventTimeTracking();

	var eventAction = "",
		eventLabel;
	var collegeName = jq("#collegeNameGA").val();

	if (!isBlankOrNull(eventTime)) {
		var eventTiming = eventTime == "Anytime/" ? "Anytime" : eventTime;
		if (eventTime.endsWith("/")) {
			eventTiming = eventTime.replace('/', '');
		}
	}
	/*Logging for Open day header section*/
	if (fieldClicked == "institution_click_header") {
		eventAction = "View Institution";
		eventLabel = collegeName;
	}
	if (fieldClicked == "institution_review_header") {
		eventAction = "View Reviews";
		eventLabel = collegeName;
	}
	/*Ended for Open day header section Log*/

	/*Logging for Select an open day pod*/
	if (fieldClicked == "select_open_day") {
		eventAction = "Selected - Top 3";
		eventLabel = eventTiming;
	}

	if (fieldClicked == "select_open_day_lightBox") {
		//eventAction = "Selected - Lightbox";
		//eventLabel = eventTiming;
		jq("#openDayEventsEventTime_lightBox").val(eventTiming);
	}

	if (fieldClicked == "view_more_open_day") {
		eventAction = "View More";
		eventLabel = collegeName;
	}
	/*Ended Select an open day pod log*/

	/*Logging for open day map*/
	if (fieldClicked == "open_day_map") {
		eventAction = "Map Clicked";
		eventLabel = collegeName;
	}
	if (fieldClicked == "open_day_city_guide") {
		eventAction = "City Guide Clicked";
		eventLabel = collegeName;
	}
	/*Ended open day map log*/

	/*Logging for Addition resources pod*/
	if (fieldClicked == "open_day_additional_res") {
		eventAction = "Resource Click";
		eventLabel = pdfName;
	}
	/*Ended Addition resources pod log*/

	/*Logging for Book open day button*/
	/* if(fieldClicked == "book_open_day_btn"){
	eventCategory = "Book Open Day"; 
	eventAction = " Onsite Open Day Booking Request";
	eventLabel = collegeName;  
  }
  if(fieldClicked == "book_open_day_btn"){
	eventCategory = "engagement"; 
	eventAction = " Onsite Open Day Booking Request";
	eventLabel = collegeName;  
  }*/
	/*Ended Book open day button log*/
	if (fieldClicked != "select_open_day_lightBox") {
		GANewAnalyticsEventsLogging(eventCategory, eventAction, eventLabel);
	}
}

function opendayProviderLandinOnScrollEvent() {
	var gaSend = "send",
		gaEvent = "event",
		gaAction = "Content Viewed";
	var gaNotLogged = "ga_not_logged";
	var gaLogged = "ga_logged";
	var dataId = "data-id";
	var testimonialsId = jq('#testimonials_id');
	var galleryId = jq('#gallery_id');
	var additionalResId = jq('#addition_res_id');
	var contentSectionId = jq('#content_sec_parent_id');
	var locationPodId = jq('#location_id');
	//var socialProofId = jq('#social_proof_id');

	if (galleryId.length && isNotNullAndUndef(galleryId)) {
		if (isScrollIntoView(galleryId, false) && galleryId.attr(dataId) == gaNotLogged) {
			ga(gaSend, gaEvent, eventCategory, gaAction, "Gallery");
			galleryId.attr(dataId, gaLogged);
		}
	}

	if (locationPodId.length && isNotNullAndUndef(locationPodId)) {
		if (isScrollIntoView(locationPodId, false) && locationPodId.attr(dataId) == gaNotLogged) {
			ga(gaSend, gaEvent, eventCategory, gaAction, "Where you'll study");
			locationPodId.attr(dataId, gaLogged);
		}
	}

	if (testimonialsId.length && isNotNullAndUndef(testimonialsId)) {
		if (isScrollIntoView(testimonialsId, false) && testimonialsId.attr(dataId) == gaNotLogged) {
			ga(gaSend, gaEvent, eventCategory, gaAction, "Testimonials");
			testimonialsId.attr(dataId, gaLogged);
		}
	}

	if (additionalResId.length && isNotNullAndUndef(additionalResId)) {
		if (isScrollIntoView(additionalResId, false) && additionalResId.attr(dataId) == gaNotLogged) {
			ga(gaSend, gaEvent, eventCategory, gaAction, "Additional Resources");
			additionalResId.attr(dataId, gaLogged);
		}
	}

	var contentSectionClassName = document.getElementsByClassName("opd_ctils");
	if (contentSectionId.length && isNotNullAndUndef(contentSectionId)) {
		var contentSectionIDs = [];
		var contentId = "";
		contentSectionId.find(".opd_rit_ctils").each(function() {
			contentSectionIDs.push(this.id);
		});
		for (var i = 0; i < contentSectionIDs.length; i++) {
			var isGALogged = jq("#" + contentSectionIDs[i]).attr(dataId);
			contentId = jq("#" + contentSectionIDs[i]);
			if (isScrollIntoView(contentId, false) && isGALogged == gaNotLogged) {
				var contentSectionTitle = jq("#" + contentSectionIDs[i]).attr("data-value");
				ga(gaSend, gaEvent, eventCategory, gaAction, contentSectionTitle);
				jq("#" + contentSectionIDs[i]).attr(dataId, gaLogged);
			}
		}
	}
}

/*jq(window).on('load', function() {
  var socialProofId = jq('#social_proof_id');
  if(socialProofId.length && isNullAndUndef(socialProofId)){
    if(isScrollIntoView(socialProofId, false) && socialProofId.attr("data-id") == "ga_not_logged"){
	  ga("send", "event", eventCategory, "Content Viewed", "Social Proofing");
	  socialProofId.attr("data-id", "ga_logged");
    }
  }
});*/

function isScrollIntoView(element, fullyInView) {
	var pageTop = jq(window).scrollTop();
	var pageBottom = pageTop + jq(window).height();
	var elementTop = jq(element).offset().top;
	var elementBottom = elementTop + jq(element).height();
	if (fullyInView === true) {
		return ((pageTop < elementTop) && (pageBottom > elementBottom));
	} else {
		return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
	}
}

//This is used to update the slider image src
jq(document).on('click', '.next-btn, .prev-btn, .dot', function(e) {
	jq('.slider video').each(function() {
		playAndPauseFn(jq(this).attr('id'), 'pause');
	});
	jq('.slider img').each(function() {
		var src = jq(this).attr('src');
		var dataSrc = jq(this).attr('data-src');
		if (src.indexOf('/img_px') > -1) {
			jq(this).attr('src', dataSrc);
		}
	});
});

function clickPlayAndPause(videoId, e, thumbNailId, playIconId) {
	var thumbNailImg = 'thumbnailImg';
	var opendayPlayIcon = 'openday_play_icon';
	if (isNotNullAndUndex(thumbNailId)) {
		thumbNailImg = thumbNailId
	}
	if (isNotNullAndUndex(playIconId)) {
		opendayPlayIcon = playIconId
	}
	var videoIdElement = jq('#' + videoId).get(0);
	var vidId = $jq('#' + videoId);
	if (videoIdElement.paused) {
		//jq("#vide_icn_"+videoId).hide();
		vidId.show();
		e.preventDefault();
		videoIdElement.play();
		vidId.attr('controls', '');
		jq('#' + thumbNailImg).hide();
		jq('#' + opendayPlayIcon).hide();
	} else {
		vidId.hide();
		//jq("#vide_icn_"+videoId).show();
		e.preventDefault();
		videoIdElement.pause();
		jq('#' + thumbNailImg).show();
		jq('#' + opendayPlayIcon).show();
	}
}

function videoPlayedDurationPercentageForOpenday(videoElement) {
	//var collegeName = !isBlankOrNullString(jq("#collegeNameGA").val()) ? jq("#collegeNameGA").val() : "";
	var videoTitle = videoElement.getAttribute('data-media-name');
	if (videoRanMap[videoElement.id + '_percentage'] == null) {
		videoRanMap[videoElement.id + '_percentage'] = "0%";
	}
	if (videoRanMap[videoElement.id + '_imp5sec'] == null) {
		videoRanMap[videoElement.id + '_imp5sec'] = "N";
	}

	videoElement.ontimeupdate = function() {
		var curVidDrtn = videoElement.duration;
		var tenPcntSec = curVidDrtn / 10;
		//
		if (videoRanMap[videoElement.id + '_imp5sec'] != "Y") {
			videoRanMap[videoElement.id + '_imp5sec'] = "Y";
			GANewAnalyticsEventsLogging(eventCategory, 'Video Impression', videoTitle);
		}
		for (var cnt = 1; cnt <= 10; cnt++) {
			if (Math.round(this.currentTime) >= Math.round(tenPcntSec * cnt)) {
				if (videoRanMap[videoElement.id + '_percentage'].indexOf(vidpct[cnt]) < 0) {
					videoRanMap[videoElement.id + '_percentage'] += ',' + vidpct[cnt];
					//console.log('video10percentage  > Video Duration > '+$$D('collegeNameGa').value+'|'+videoTitle+' > '+vidpct[cnt]);
					GANewAnalyticsEventsLogging(eventCategory, 'Video Duration', vidpct[cnt] + '|' + videoTitle);
				}
			}
		}
	};
}

jq(window).scroll(function() {
	jq('video').each(function() {
		var vidId = jq(this).attr('id');
		if (vidId == "openday_gallery_video") {
			if (!isScrolledIntoView(jq('#' + vidId).get(0), false)) {
				playAndPauseFn(jq(this).attr('id'), 'pause');
			}
		} else {
			playAndPauseFn(jq(this).attr('id'), 'pause');
		}
	});
});

function playAndPauseFn(videoId, action) {
	var video = jq('#' + videoId).get(0);
	if (action == 'pause') {
		video.pause();
	}
}

function GAEventTracking(category, action, opt_label, opt_value) {
	eventTimeTracking();
	var booleanValue = true;
	if (action == "reserve-place") {
		booleanValue = false;
	}
	opt_value = parseInt(1);
	ga('send', 'event', category, action, opt_label, opt_value, {
		nonInteraction: booleanValue
	});
}