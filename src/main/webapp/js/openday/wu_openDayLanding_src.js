function $$d(id){
  return document.getElementById(id);
}

var adv = jQuery.noConflict();
  adv(window).scroll(function(){ // scroll event 
  var windowTop = adv(window).scrollTop(); // returns number  
  if($$d("odAdvertiser")){
    if($$d("odAdvertiser").value=="Y"){
      var width = document.documentElement.clientWidth;
      if (width > 992) {
        if(150 < windowTop){
          adv('#fixedscrolltop').addClass('sticky_top_nav');        
          adv('#fixedscrolltop').css({ position: 'fixed', top: 0 });      
        }else{
          adv('#fixedscrolltop').removeClass('sticky_top_nav');
          adv('#fixedscrolltop').css('position','static');
        } 
      }
    }
  }  
});


/* commented out as per Luke feedback
adv(document).ready(function(){
  adv(".hdr_menu ul > li a").click(function(event){
    if(document.getElementById("odEvntAction").value=="false") {				 		
      reserveopenDayPlace();
      event.preventDefault();  	
    }	
  }); 
});
*/

var $cc = jQuery.noConflict();
$cc(document).ready(function() {
/*
  var odStatusPos   =  parseInt($$d("odCurPos").value);
  var totalPos =  parseInt($$d("odtotPos").value);
  var startPos = 0;  
   if(odStatusPos > 8){
      var tmpStartPos = odStatusPos - 6;	
      var tmpEndPos = odStatusPos + 6;
      if( tmpEndPos>totalPos){
          if(odStatusPos > 12){
            startPos = totalPos - 12;	
          }		
          if(odStatusPos > 12 && odStatusPos <= startPos){
            startPos = odStatusPos - 1;
          }
      }else{
        startPos = tmpStartPos;      
      }
  }	  
  var width = document.documentElement.clientWidth;
  if (width > 992) {
    $cc(".op_mth_slider").jCarouselLite({
      btnNext: ".mov_nxt",
      btnPrev: ".mov_pre",
      speed: 600,
      start: parseInt(startPos),
      visible:12,
      circular: false,
      afterEnd: function(){
        adv('#leftValue').val(adv('.op_mth_slider ul').css("left"));
      }
    });
    adv('#leftValue').val(dev('.op_mth_slider ul').css("left"));
  }
  if(adv(".op_mth_slider .act a").html() != null && adv(".op_mth_slider .act a").html().trim() != ""){ //Added for wu_539 07-APR-2015 by Karthi to prepopulate value in dropdown
    adv(".select_box_month .sld_mth ").html(adv(".op_mth_slider .act a").html());
  }
  */
  
       $cc(window).scroll(function(){
        var topOfElement = $cc('.opd_res').offset().top;
        var scrollTopPosition  = $cc(window).scrollTop();
        //alert(topOfElement);
        //alert(scrollTopPosition);
     
        if(scrollTopPosition < topOfElement){
         $cc('.ods_sticky').removeClass('sti');
        }else{
         $cc('.ods_sticky').addClass('sti');
        }
       });


  
});

function reserveopenDayPlace(srchProvPage) 
{    
  if($$d("resUniIds")) {
    $$d("resUniIds").value ="";
    if($$d("exitOpenDayResFlg")){      
      if($$d("exitOpenDayResFlg").value=="" || srchProvPage=="true"){
        sm('newvenue','650','500', 'other');  
        var url = "/degrees/open-days/reserve-place.html?reqAction=";      
        $$d("ajax-div").style.display="block";
        var ajaxObj = new sack();
        ajaxObj.requestFile = url;	      
        ajaxObj.onCompletion = function(){displayFormData(ajaxObj);};	
        ajaxObj.runAJAX(); 
      }
    } 
  }
}

function addOpenDayProv()
{  
  var adOp = jQuery.noConflict();
  var innerCnt        = "";
  var styleId         = "";
  var tempURL         = "";
  var uniId           = "";
  var UniDisplayName  = "";
  var resOpenUniIds   = "";
  var uniIdsArray     = "";
  var found           = "";  
  var tempFound		     = "";	
  var deleteFound	    = "";	
  
  if($$d("uniName_id")){
    if($$d("uniName_id").value!=""){
      uniId = $$d("uniName_id").value;
      UniDisplayName = $$d("uniName_display").value;            
    }else{
      alert("Please pick a university from the dropdown list."); 
    }
  }
  tempFound = findResUniIdExist("resUniIds",uniId,"add");  
  if(tempFound==false){
    deleteFound = findResUniIdExist("delResUniIds",uniId,"delete");	
    tempFound = deleteFound;	
  }
  found = tempFound;    
  if(found==false){
    resOpenUniIds = addResUniIds("resUniIds",uniId);
  }    
  if(found==false){  
    if($$d("hidStyleId")){
      if($$d("hidStyleId").value==""){
        $$d("hidStyleId").value = "1";
        styleId = $$d("hidStyleId").value;
      }else{
        styleId = parseInt($$d("hidStyleId").value) + 1;
        $$d("hidStyleId").value = styleId;
      }
    }  
    if(uniId!="" && UniDisplayName!="") {
        tempURL = "<li id=\"resopenli_"+styleId+"\"><a onclick=\"javascript:addRemoveOpenDays('resopen_"+styleId+"','"+uniId+"')\">" +  
                  "<span class=\"fl uni fnt_lrg\">"+UniDisplayName+"</span>\n" + 
                  "<i class=\"fa fa-remove fa-1_5x fr\" id=\"resopeni_"+styleId+"\"></i></a></li>";                
        $$d("resUniIds").value = resOpenUniIds;
    }                                                       
    if(tempURL!="") {
      $$d("noUni").style.display="none";       
      $$d("reserveUniList").style.display="block";             
      innerCnt = $$d("addReserveUniList").innerHTML;          
      $$d("addReserveUniList").innerHTML =  innerCnt + tempURL;  
      clearAutoUniNAME('uniName');
    }  
  }else{	
    if(deleteFound!=true){
      alert("You've already picked that university once!");
    }	
  }
}

function addRemoveOpenDays(divId,univId)
{  
  var li_IdName = "";
  var li_IName  = "";
  var curIndex  = "";
  var SplitDiv  = "";    
  
  SplitDiv  = divId.split("_");
  if(SplitDiv.length>0) {    
    curIndex  = SplitDiv[1];
    li_IdName = SplitDiv[0] + "li_" + curIndex;
    li_IName  = SplitDiv[0] + "i_"  + curIndex;
  }        
  if($$d(li_IdName)) {
    if($$d(li_IdName).className==""){      
      $$d(li_IdName).className = "cancel";
    }else{
      $$d(li_IdName).className = "";
    }
  }  
  if($$d(li_IName)) {
    if($$d(li_IName).className=="fa fa-remove fa-1_5x fr"){
      $$d(li_IName).className = "fa fa-plus fa-1_5x fr";
      addRemoveUniIds('add',univId,curIndex);      
    }else{
      $$d(li_IName).className = "fa fa-remove fa-1_5x fr";
      addRemoveUniIds('remove',univId,curIndex);      
    }    
  }    
}

function addRemoveUniIds(type,univId,curIndex) {  
  var uniIds = "";
  var delUniIds = "";
  if(type=="add"){    
    uniIds = removeUniIdExist("resUniIds",univId);	
    delUniIds = addResUniIds("delResUniIds",univId+"_"+curIndex);
  }else{    
    uniIds = addResUniIds("resUniIds",univId);	
    delUniIds = removeUniIdExist("delResUniIds",univId+"_"+curIndex);		
  }  
  $$d("resUniIds").value = uniIds;  
  $$d("delResUniIds").value = delUniIds;  
}

function clearAutoUniNAME(id){    
  if($$d(id) != null){
    $$d(id).value = "Enter uni name";
  }
  if($$d(id+'_hidden') != null){
    $$d(id+"_hidden").value = "";
		}
		if($$d(id+'_id') != null){
  			$$d(id+"_id").value = "";
		}
		if($$d(id+'_display') != null){
  			$$d(id+"_display").value = "";
		}
		if($$d(id+'_url') != null){
  			$$d(id+"_url").value = "";
		}
		if($$d(id+'_alias') != null){
  			$$d(id+"_alias").value = "";
  }
}


function closeOpenDaydRes()
{
  if($$d("resUniIds")) {
    if($$d("odEvntAction")) {    
      if($$d("odEvntAction").value=="false"){        
        $$d("resUniIds").value ="";
        var url = "/degrees/open-days/reserve-place.html?reqAction=exitOpenDaysRes";      
        $$d("ajax-div").style.display="block";
        var ajaxObj = new sack();
        ajaxObj.requestFile = url;	  
        ajaxObj.onCompletion = function(){setOpendaysExitVal(ajaxObj);};	
        ajaxObj.runAJAX();       
      }
    }    
    hm();
  }
}  

function setOpendaysExitVal(ajaxObj)
{  
  if(ajaxObj.response!="" && ajaxObj.response=="true"){
    $$d("exitOpenDayResFlg").value = ajaxObj.response; 
    $$d("odEvntAction").value = "true";   
  }  
}

function closeOpenDay(from){
  if(from=='fromlogin'){
    document.location=$$D("fullOpenDayUrlwithparam").value;   
  }
  hm();
}


function GAEventTracking(category, action, opt_label, opt_value){
  eventTimeTracking();
  var booleanValue = true;
  if(action=="reserve-place"){
    booleanValue = false;
  }
  opt_value = parseInt(1);  
  ga('send', 'event', category , action , opt_label , opt_value , {nonInteraction: booleanValue}); //Added by Amir for UA logging for 24-NOV-15 release  
}
function opendayFb(date, url, gaCollegeName){ //Post to facebook for openday :: Prabha :: 31_05_16
 var opendayUrl = window.location.hostname + url;
 var collegeDispName = "";
 if($$D('opendaySaveAjaxCollegeName')){collegeDispName = $$D('opendaySaveAjaxCollegeName').value;}
 //var quote = "Im going to the open day at " + collegeDispName + " on " + date + " after finding it on Whatuni.com";
 var quote = "Im attending " + collegeDispName + " event on " + date + " after finding it on Whatuni.com";
 if(gaCollegeName != ""){
   eventTimeTracking();
   ga('send', 'event', 'Post to facebook', 'save to calendar', gaCollegeName);   
   window.open('https://www.facebook.com/sharer.php?u='+encodeURIComponent(opendayUrl)+'&quote='+encodeURIComponent(quote), 'sharer', 'toolbar=0,width=626,height=436');
 }
 return false;
}
//End of post to facebook code
function viewMoreResult(index){
  if($$D("view_more_"+index)){
    if($$D("view_more_"+index).style.display=='block' ||  $$D("view_more_"+index).style.display==''){
       blockNone("view_more_"+index , 'none');
       blockNone("view_more_plus_"+index , 'block');
       blockNone("view_more_minus_"+index , 'none');       
    }else{
       blockNone("view_more_"+index , 'block');
       blockNone("view_more_plus_"+index , 'none');
       blockNone("view_more_minus_"+index , 'block');
    }
  }
}

function removeUniIdExist(objId,uniId)
{
  var delUniIds = "";
  var uniIdsArray = "";
  var finalIds = "";
  if($$d(objId)) {
    delUniIds = $$d(objId).value;      	
    uniIdsArray   = delUniIds.split(",");       
    for(var i=0;i<uniIdsArray.length; i++) {
      if(uniIdsArray[i]==uniId){				
        uniIdsArray.splice(i,1);
        finalIds = uniIdsArray;			
      }
    }     
  }		
  return finalIds;
}

function addResUniIds(objId,uniId)
{
  var addedUniIds = "";
  if($$d(objId)) 
  {
    if($$d(objId).value==""){
      addedUniIds = uniId;
    }else{        
      addedUniIds = $$d(objId).value;
      addedUniIds = addedUniIds + "," + uniId;    
    }    
  }  
  return addedUniIds;
}

function findResUniIdExist(objId,uniId,type)
{
  var UniIds = "";
  var uniIdsArray = "";
  var found = "";
  var srchId = "";
  var styleId = "";
  var tempSplit = "";
  if($$d(objId)) {
    UniIds = $$d(objId).value;      	
    uniIdsArray   = UniIds.split(",");       
    for(var i=0;i<uniIdsArray.length; i++) {
      srchId = uniIdsArray[i];	
      if(type=="delete"){			
        tempSplit = srchId.split("_");					
        if(srchId!=""){
          srchId  =  tempSplit[0];
          styleId =  tempSplit[1];    
        }									
      }
      if(srchId!=uniId){
        found = false;        
      }else{
        found = true;			
        break;
      }      		  
    }   		
    if(found==true && type=="delete"){
      addRemoveOpenDays('resopen_'+styleId,srchId);
      clearAutoUniNAME('uniName');
    }
  }		
  return found;
}

adv(document).ready(function(){  //Added for wu_539 07-APR-2015 by Karthi for mobile dropdown header
  adv("#hd1").removeAttr("style");
	adv('.lft_nav .brw').click(function(){
		adv(this).toggleClass('act');
		adv(this).parents().next().toggleClass('act');
	});
  /*Month Dropdown*/
    adv(".select_box_month").click(function(){
      adv("#select_cont").slideToggle();
    });  
    adv(".op_mth_slider ul li a").click(function(){
      adv(".select_box_month .sld_mth ").html(adv(this).html())
      adv("#select_cont").slideToggle();
    });	
    adv(".select_box_month").one("click", function(){
      if($$d("odCurPos") && $$d("odtotPos") && $$d("odCurPos").value != ""){
        var count = parseInt($$d("odCurPos").value);
        if($$d("odCurPos").value != $$d("odtotPos").value){
          count++;
        }
        adv('#select_cont').scrollTop(adv("#select_cont li:nth-child("+count+")").position().top - adv('#select_cont li:first').position().top);
      }
   });
  /*Month Dropdown*/
});

function openDayOrientation(){ //Added for wu_539 07-APR-2015 by Karthi for orientation of tablet view, triggers when portait to landscape
/*  var odStatusPos   =  parseInt($$d("odCurPos").value);
  var totalPos =  parseInt($$d("odtotPos").value);
  var startPos = 0;  
   if(odStatusPos > 8){
      var tmpStartPos = odStatusPos - 6;	
      var tmpEndPos = odStatusPos + 6;
      if( tmpEndPos>totalPos){
          if(odStatusPos > 12){
            startPos = totalPos - 12;	
          }		
          if(odStatusPos > 12 && odStatusPos <= startPos){
            startPos = odStatusPos - 1;
          }
      }else{
        startPos = tmpStartPos;      
      }
  }	  
  var width = adv('#leftValue').val();
  if(width.trim() == ''){
   $cc(".op_mth_slider").jCarouselLite({
     btnNext: ".mov_nxt",
     btnPrev: ".mov_pre",
     speed: 600,
     start: parseInt(startPos),
     visible:12,
     circular: false,
     afterEnd: function(){
       adv('#leftValue').val(adv('.op_mth_slider ul').css("left"));
     }
   });
   adv('#leftValue').val(dev('.op_mth_slider ul').css("left"));
  }else{
    adv('.op_mth_slider ul').css({"left": width, "width": totalPos*77});
  }*/
}

function triggerAddToCalendar(eventId) {  
  if($$d("calBtn_"+eventId)){
    if($$d("newOdReg")) {          
      if($$d("newOdReg").value=="Y"){
        var providerName = $$d("odColName").value;                 
        GARegistrationLogging('register','add-open-days', providerName);
        $$d("newOdReg").value = "";
      }
    }  
    adv("#calBtn_"+eventId).click();
  }else if($$d("calBtnAct_"+eventId)){     
    showLightBoxLoginForm('openday-popup',650,500, 'OPENDAY-EXIST-ALERT','','fromlogin',eventId);
  }
}

function triggerResUniAjax(resUniId) {
  if($$d("resUniIds")){
    if($$d("resUniIds").value==""){
      $$d("resUniIds").value = resUniId;
      if($$d("newOdReg")) {          
        if($$d("newOdReg").value=="Y"){            
          GARegistrationLogging('register','clicked','opendayslightbox');
          $$d("newOdReg").value = "";
        }
      }
      loadSocialMediaLogin();      
      setTimeout(function(){reLoadOdPage()}, 300);      
    }
  }  
}

function reLoadOdPage(){
  location.href = $$d("fullOpenDayUrlwithparam").value;
}
