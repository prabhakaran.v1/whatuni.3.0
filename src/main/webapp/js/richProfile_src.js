var $rp = jQuery.noConflict();
var context_path = "/degrees";
function $$D(id){return document.getElementById(id)}
function blockNone(thisid, value){  if($$d(thisid) !=null){ $$d(thisid).style.display = value;  }  }
function isEmpty(value){  if(trimString(value) ==''){ return true;} else {  return false; } }
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
function addClassName(id, classname){if($$D(id)){$$D(id).className = classname;}}

//Modified by Indumathi.S For unique content changes May_31_2016
function richProfileSlider(pos,length,extra) {
  if(pos == 1 && extra =="first") {
    $rp('#uniimg_head_id').show();
    $rp('#uniimg_head_mob_id').show();
  }else{
    $rp('#uniimg_head_id').hide();
    $rp('#uniimg_head_mob_id').hide();
  }
  var curpos = parseInt(pos);     
  for(var len=1; len<=length; len++) {
    if(pos==len){              
      $rp("#richImg"+pos).fadeIn("slow");   
      $rp("#uniimg_head_id"+pos).fadeIn("slow"); 
      $rp("#uniimg_head_mob_id"+pos).fadeIn("slow"); 
      $rp("#wuscaRat"+pos).fadeIn("slow"); 
      $rp("#wuscaRat_mob"+pos).fadeIn("slow"); 
      $rp("#proSec"+pos).fadeIn("slow");      
      $rp("#rpPrePos").val(parseInt(curpos)-1);
      if(pos == 1 && extra=="first") {
        $rp("#rpNexPos").val(parseInt(curpos)); 
        $rp("#rpCurPos").val(parseInt(0));
      }else{
        $rp("#rpNexPos").val(parseInt(curpos)+1);
        $rp("#rpCurPos").val(parseInt(curpos));
      }
      if(pos==1){
       $rp('#prev_lnk').hide();    
       $rp('#sec_prev_lnk').hide();       
      }else{
       $rp('#prev_lnk').show();
       $rp('#sec_prev_lnk').show();
      }
    }else{
      $rp("#richImg"+len).hide();
      $rp("#uniimg_head_id"+len).hide();
      $rp("#uniimg_head_mob_id"+len).hide();
      $rp("#wuscaRat"+len).hide();
      $rp("#wuscaRat_mob"+len).hide();
      $rp("#proSec"+len).hide();      
    }
    var width = document.documentElement.clientWidth;
    if (width >=1024) {
      if($$D("profileSectionDesktop"+len) && $$D("profileSectionDesktop"+len).value != "") { 
        $$D("profileSectionSpan"+len).innerHTML = $$D("profileSectionDesktop"+len).value;
      }
    }
  }
  if(extra=="first"){
    blockNone("proSec"+pos,"none");
    blockNone("uniimg_head_id1","none");
    blockNone("uniimg_head_mob_id1","none");
    blockNone("wuscaRat1","none");
    blockNone("wuscaRat_mob1","none");
  }else{
    if($$D("mblOrient").value=="true"){
      uniScrollTop();
    }  
  }
  if($$D("curPofSec")) { $$D("curPofSec").value= $$D("profSec_"+curpos) ? $$D("profSec_"+curpos).value : ""; }
  showScrollBar();
}
//Added by Indumathi.S for profile logging July-26-2016
function GAEventTrackingRichIP(Webclick,Profile,sectionName,collegeName){
 if($$D("richProfileType") && $$D("richProfileType").value != "RICH_SUB_PROFILE") {
  if($$D("TagName")) {
    collegeName = $$D("TagName").value;
  }
  GAInteractionEventTracking(Webclick,Profile,sectionName,collegeName);
  pageViewLogging();
 }
}
function pageViewLogging(){
  if($$D("pageName")) {
    ga('set', 'dimension1', $$D("pageName").value);
  }
  if($$D("gaPageCollegeName")){
    ga('set', 'dimension3', $$D("gaPageCollegeName").value);      
  }  
  ga('set', 'dimension4', 'Advertiser'); 
  ga('send', 'pageview');   
}
//End
function showScrollBar(){
 if($$D("richProfileType") && $$D("richProfileType").value != "RICH_SUB_PROFILE") { 
  var dwidth = $rp(this).width();//device width
  if(dwidth > 1950){
		$rp("#rpProfileSec").css("width","1280");
		$rp("#richProIntroSec").css("width","1280");
	}else if(dwidth > 1600){
		$rp("#rpProfileSec").css("width","800");
		$rp("#richProIntroSec").css("width","800");		
	}else if(dwidth > 1366){
		$rp("#rpProfileSec").css("width","700");
		$rp("#richProIntroSec").css("width","700");		
	}else{
		$rp("#rpProfileSec").css("width","100%");
		$rp("#richProIntroSec").css("width","100%");		
	}
	var dheight = $rp(".uni_fwdth .pro_infoco").outerHeight();//container height
  if(dheight >= 480 && ($$D("richProIntroSec") && $$D("richProIntroSec").style.display == "none")){
    $rp(".uni_fwdth .pro_infoco").css({
     'height':'480',
     'overflow-y':'scroll'
    });
  } else if($$D("richProIntroSec") && $$D("richProIntroSec").style.display == "block") {
    $rp(".uni_fwdth .pro_infoco").removeAttr("style");
  } else {
    $rp(".uni_fwdth .pro_infoco").css({
     'overflow-y':'hidden',
     'height':'auto',
    });  
  }
  var img_wd;
  if(dwidth > 1950){
    img_dht = 992;
    img_wd = 100;
    $rp("#h_image8").css({
     'width':img_wd + '%',
     'margin-left':'auto',
     'margin-right':'auto',
     'padding':'0',
     'background-position':'top center'
    }); 
  }
 }
}

function showAndHideToolTip(divId){
 if($rp("#"+ divId).is(':visible')){
   $rp("#"+ divId).hide();
 } else {
   $rp("#"+ divId).show();
 }
}
function showToolTip(showId){
  blockNone(showId,'block');
}
function hideToolTip(showId){
  blockNone(showId,'none');
}
function stickyButton() {
 var width = document.documentElement.clientWidth;
 var offtop = $rp('.btn_intr .btns_interaction').offset().top;
 var cnt_hght = $rp('.btn_intr .btns_interaction').innerHeight();
 var tcnt_hght = offtop + cnt_hght;
 var divPosition;
 if ((width >= 320) && (width <= 992)) {
  $rp(window).scroll(function(){ 
   width = document.documentElement.clientWidth;
   if ($rp(this).scrollTop() > tcnt_hght) { 
    var windowTop = $rp(window).scrollTop(); 
    $rp('.btn_intr').addClass('btn_cntrl');
    $rp(".btn_intr .more_info").css("display","block");
    $rp(".btn_intr .btns_interaction").addClass("btn_vis");
    $rp("#socialBoxMin").addClass("intr_sb_pos");
    $rp("#needHelpDiv").addClass("intr_sb_pos");
    $rp("#socialBoxMin").removeClass("intr_sb_pos1");
    $rp("#needHelpDiv").removeClass("intr_sb_pos1");
    if(width > 992){
     $rp(".btn_intr .more_info").removeAttr("style");
     $rp(".btn_intr .more_info").css("display","none");
    }
   }else {
    $rp('.btn_intr').removeClass('btn_cntrl');
    $rp(".btn_intr .more_info").removeAttr("style");
    $rp(".btn_intr .more_info").css("display","none");
    $rp(".btn_intr .btns_interaction").addClass("btn_vis");
    $rp("#socialBoxMin").removeClass("intr_sb_pos");
    $rp("#needHelpDiv").removeClass("intr_sb_pos");
    $rp("#socialBoxMin").removeClass("intr_sb_pos1"); 
    $rp("#needHelpDiv").removeClass("intr_sb_pos1"); 
   }
   
  });
 }
}
//End May_31_2016
function showContent(section)
{
  var totCnt    = parseInt($$D("feaVideoLen").value);
  var sectionId = parseInt(section);
  $rp('#richProIntroSec').hide();   
  $rp('#rpProfileSec').show();  
  richProfileSlider(sectionId,totCnt);
  lazyloadetStarts();
  richProfSecStatsLog();
}

function sliderPlay(play)
{    
  var totCnt  =   parseInt($$D("feaVideoLen").value);
  var prevPos =   parseInt($$D("rpPrePos").value);
  var nextPos =   parseInt($$D("rpNexPos").value);
  var curPos  =   parseInt($$D("rpCurPos").value);
  
  $rp('#richProIntroSec').hide();   
  $rp('#rpProfileSec').show();    
  var spanCount = 1;
  if(play=="prev")    
  { spanCount = curPos-1;
    if(prevPos==0) {      
      richProfileSlider(totCnt,totCnt);      
    }else{      
      richProfileSlider(curPos-1,totCnt);      
    }
  }
  else if(play=="next")
  { spanCount = curPos+1;    
    if(nextPos==totCnt+1) {           
      richProfileSlider("1",totCnt);      
    }else{      
      richProfileSlider(curPos+1,totCnt);      
    }
  }  //Added ga event tracking Indumathi.S 26_07_2016
  if($$D("richProfileType") && $$D("richProfileType").value != "RICH_SUB_PROFILE") {
    var profileSectionName = ""; var collegeName = ""; 
    if(curPos == totCnt) { spanCount = 1; }
    if($$D("uniimg_head_id"+spanCount) && $$D("uniimg_head_id"+spanCount).title != '') {
      profileSectionName = $$D("uniimg_head_id"+spanCount).title;
    }
    if($$D("TagName")) {
      collegeName = $$D("TagName").value;
    }
    GAInteractionEventTracking('Webclick', 'Profile Content', profileSectionName, collegeName);
    pageViewLogging();
  //End
  }
  uniScrollTop();
  lazyloadetStarts();
  richProfSecStatsLog(); 
}

function uniScrollTop() {
 var width = document.documentElement.clientWidth;
  var divPosition = 160; 
  if(width <= 992) {
   divPosition = 50;
  } 
  $rp('html, body').animate({
    scrollTop: $rp('.uni_toppos').offset().top - divPosition
  }, "slow");
}
function backToIntro() {  
  //Added ga event tracking Indumathi.S 26_07_2016
  if($$D("richProfileType") && $$D("richProfileType").value != "RICH_SUB_PROFILE") {
    if($$D("TagName")) {
      collegeName = $$D("TagName").value;
    }
    GAInteractionEventTracking('Webclick', 'Profile Content', 'Uni info', collegeName);
    pageViewLogging();
    //End
  }
  var totCnt  =   parseInt($$D("feaVideoLen").value);
  $rp("#richProIntroSec").show();
  $rp("#rpProfileSec").hide();  
  richProfileSlider("1",totCnt,"first");
  $rp('#uniimg_head_id').show(); 
  $rp('#wuscaRat').show();
}

function richProfSecStatsLog(logType){
  var journeyType   =   "";
  var collegeId     =   $$D("collegeId").value;
  var networkId     =   $$D("networkId").value;  
  var curSecId      =   (logType == 'logOnLoad') ? "1" : $$D("rpCurPos").value;
  var sectionId     =   $$D("richSecProfId_"+curSecId).value;
  //var myhcProfileId =   $$D("myhcProfileId").value;
  var myhcProfileId =   $$D("richSecMyhcProfId_"+curSecId).value;//Added to handle the clearing stats log for CMMT
  var richProfileType = $$D("richProfileType").value;  
  var queryString = "?tabName=" + sectionId +"&collegeId="+collegeId+"&cpeQualificationNetworkId="+networkId+"&myhcProfileId="+myhcProfileId+"&profileId="+sectionId+"&journeyType="+journeyType+"&richProfileType="+richProfileType;  
  richProfStatsLoggingAjax(queryString);
}

function richProfStatsLoggingAjax(queryString) 
{
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  var ajaxURL = "/degrees" + "/unilandingStatsAjax.html" + queryString +'&screenwidth='+deviceWidth;
  var ajax=new sack();
  ajax.requestFile = ajaxURL;
  //ajax.onCompletion = function(){ uniStatsLoggingAjaxResponse(ajax); };
  ajax.runAJAX();
}

function drawRPdoughnutchart(value1,value2,currentElement,color)
{
  if(value1!="" && value2!="")
  {    
    var limit1  = parseInt(value1);
    var limit2  = parseInt(value2);
    var defColor = "#a65266";  
    if(color!="" && color=="plain") {
      defColor = "#dededf";
    }  
    $rp("#" + currentElement).drawDoughnutChart([
    {  
      value : limit1,  
      color: "#fc7169",
      prcct: "0"
    },
    { 
      value:  limit2,   
      color: defColor,
      prcct: "0"
    }  
    ]);
  } 
}

function showMoresLess()
{  
  if( $$D("spMoreDiv")){
    if($$D("spMoreDiv").style.display == 'block' || $$D("spMoreDiv").style.display == ''  ){
      $$D("spMoreDiv").style.display = 'none';
      $$D("spViewMorwLink").style.display = 'block';
      $$D("spViewLessLink").style.display = 'none';
    }else{
      $$D("spMoreDiv").style.display = 'block';
      $$D("spViewMorwLink").style.display = 'none';
      $$D("spViewLessLink").style.display = 'block';
    }
  }
  lazyloadetStarts(); //Added for trigger lazyloading when clicking the view more by Prabha on 24_Jan_2017
}

var jsFlag = "true";
$rp(window).scroll(function(){ // scroll event 
  var windowTop = $rp(window).scrollTop(); // returns number  
  var docHeight = $rp(document).height();
  var diff = docHeight - 1200;
  var width = document.documentElement.clientWidth;
  if (width > 992) {    
    if (311 < windowTop){
      $rp('#stickyTop').addClass('sticky_top_nav');        
      $rp('#stickyTop').css({ position: 'fixed', top: 0 });
      $rp('.hero_cnr').css("margin-top","160px");
    } else {
      $rp('#stickyTop').removeClass('sticky_top_nav');
      $rp('#stickyTop').css('position','static');
      $rp('.hero_cnr').css("margin-top","0px");
    } 
  }  
});

$rp(document).ready(function(){ //Added for wu_537 24-FEB-2015 by Karthi for mobile dropdown header and mobile RP map
  $rp("#hd1").removeAttr("style");
	$rp('.lft_nav .brw').click(function(){
		$rp(this).toggleClass('act');
		$rp(this).parents().next().toggleClass('act');
	});
	$rp('.srh').click(function(){
		$rp(this).toggleClass('act');
		$rp('.top_search').toggleClass('act')
	});
	$rp('.ic-mob-close').click(function(){
		$rp(this).parents().find('.act').removeClass('act');
	});
//	$rp("#act_map").click(function(){
//		$rp(".bghgim").toggleClass("map_vis");
//		$rp("#act_map .fa").toggleClass("fa-minus-circle");
//		$rp(".fnt_lrg").toggleClass("bor_rom");
//    if($$D("act_map")){
//      var className = $rp('#act_map .fa').attr('class');
//      if(className != 'fa fa-plus-circle color1 pr0'){
//        lboxNewMapInitialize($$D("latitude").value, $$D("longitude").value);
//      }
//    }
//  });

  //Added by Indumathi.S for Sticky button Jan-27-16
  $rp(".btn_intr .more_info").click(function(){
   $rp(".btn_cntrl .btns_interaction").removeClass("btn_vis");
   $rp(".btn_cntrl .more_info").css("display","none");
   if(checkChildDiv("buttonCls")){
    $rp("#socialBoxMin").addClass("intr_sb_pos1"); 
    $rp("#needHelpDiv").addClass("intr_sb_pos1");
   }
  });
  $rp(".int_cl").click(function(){
   $rp('.btn_intr').addClass('btn_cntrl');
   $rp(".btn_intr .btns_interaction").addClass("btn_vis");
   $rp(".btn_cntrl .more_info").css("display","block");
   $rp("#socialBoxMin").removeClass("intr_sb_pos1"); 
   $rp("#needHelpDiv").removeClass("intr_sb_pos1");
  });
  $rp(".f5_clse_btn").click(function(){
    stickyButton();
  }); 
  $rp(".cook_cls").click(function(){
   stickyButton();   
  });
  if (!$rp("#addTofnchTickerList").is(":visible")){
   if($rp("#cookie-lightbox") && !$rp("#cookie-lightbox").is(":visible")){
    stickyButton(); 
   }
  }
  $rp(window).scroll(function(){ // scroll event 
    //load articles pod on scroll on 13_06_2017, By Thiyagu G.
    if($$D("scrollStop")){
      if($rp("#scrollStop").val() != 1){
        $rp("#scrollStop").val(1);
        loadArticlesPodJS('PROFILE_PAGE');    
      }
    }
  });
  if($$D("scrollStop")){
    $rp("#scrollStop").val(0);
  }
  showScrollBar(); //Added for positioning badgets correct position by Prabha
});

function skipLinkSetPosition(divname){
  var divPosition = $rp('#'+divname).offset();
  $rp('html, body').animate({
    scrollTop: divPosition.top - 100
  }, "slow");
}

function richVideoJsLoad(videoPath,coverimage){
  $rp(".vidpl").addClass("load_icon");
  var script = document.createElement("script");
  script.type = "text/javascript";  
  script.src = $$D('contextJsPath').value + '/js/video/jwplayer.js';
  if(script.readyState){//IE
    script.onreadystatechange = function(){
      if(script.readyState == "loaded" || script.readyState == "complete"){
        script.onreadystatechange = null;
        jwplayerSetup('flashbanner1', videoPath, coverimage, 590, 380);
      }
    };
  }else{
    script.onload = function(){
      jwplayerSetup('flashbanner1', videoPath, coverimage, 590, 380);
    };
  }
  if(typeof script != "undefined"){
    document.getElementsByTagName("head")[0].appendChild(script);
  }
  setTimeout(function(){richVideoPlayer();}, 5000);
}

function richVideoPlayer(){ //Added code for rich video player :: Prabha :: 31_May_2016
  var scrnwidth = document.documentElement.clientWidth;
  var scrnheight = document.documentElement.clientHeight;
  var vid_ctrl = document.getElementsByTagName("video")[0];
		var vd_exwidth = vid_ctrl.videoWidth;
		var vd_exhght = vid_ctrl.videoHeight;
		var vd_width = vd_exwidth;
		var vd_height = vd_exhght; 
		var vd_avg = vd_height/vd_width;
		var vd_twdth =  scrnwidth * vd_avg;
  if(scrnwidth >= 768 && scrnwidth <= 1024){vd_twdth = $rp("#flashbanner1").outerWidth();}
		var scrnhgt = scrnheight/2;
		if(vd_exwidth <= 1024 && scrnwidth > 1024){
		  $rp("#flashbanner1,.vd_inner").css({'width':vd_exwidth+'px','height':vd_exhght+'px'});
		  $rp(".reviewscnt, #h_image2").css({'height':vd_exhght + 'px'});
		  $rp(".rp_vdcnt").fadeIn().show().css("display","block");
		}else{
    $rp("#flashbanner1").css({'width':'100%','height':'100%'});
		  $rp(".reviewscnt,.vd_inner").removeAttr("style");
    $rp(".rp_vdcnt").fadeIn().show().css("display","block");
		  $rp(".clipart,.md.clipart").css("z-index","1");
    $rp(".reviewscnt").css({'height':'100%'});
    if(scrnwidth >= 768 && scrnwidth <= 1024){
      $rp(".reviewscnt").css({'height':vd_twdth}); 
    }else{
		    $rp(".reviewscnt").css({'height':vd_twdth + 'px'}); 
    }
  }
  $rp(".vidpl").removeClass("load_icon");
  var divPosition = $rp(".reviewscnt").offset();
  $rp('html, body').animate({ scrollTop: divPosition.top - 100 }, "slow");
		$rp(".rp_vdcnt").removeClass("vdo_notran");
		$rp(".rp_vdcnt").addClass("vdo_tran");
}
function closeRichVideo(){
  $rp(".rp_vdcnt").fadeOut('slow');
  heroImageChanges();
  jwplayer("flashbanner1").stop();	
	 jwplayer('flashbanner1').remove();
  $rp(".reviewscnt").removeAttr("style");
};

function heroImageChanges(){
  var dwidth = $rp(this).width();
  if(dwidth > 1024){
    var img_height = 992;
    var img_width=1900;
	   var img_height=992;
	   var variation = img_height/img_width
	   var img_dht = Math.round(dwidth * variation);
    if(dwidth > 993 && dwidth <= 1600){
	     img_dht = img_dht - 100;
	   }else if(dwidth > 1600 && dwidth < 1949){
	     img_dht=img_dht-100;
    }
  }else if(dwidth > 1950){
	   img_dht = 992;
  }
  $rp("#h_image2").height(img_dht + "px"); 
}
//Added by Indumathi.S For mobile specific images on Jan_24_2017
function mobileSpecificImage(id){
 if($$D(id)) {
  var devWidth = $rp(window).width() || document.documentElement.clientWidth; 
  var url = $$D(id).getAttribute('data-src');	
  var urlString = 'url("' + url + '")'
  var imageIndex =  url.lastIndexOf("/") + 1;
  var hrImg = url.substring(imageIndex).split('.');
  var orgUrl = url.substring(0, imageIndex) + hrImg[0];
	if(devWidth < 480){
	  urlString = 'url("'+orgUrl +'_480px.' + hrImg[1] + '")';
	} else if(devWidth > 481 && devWidth < 992){	  	
    urlString = 'url("'+ orgUrl +'_992px.' + hrImg[1] + '")';
	}
	$$D(id).style.backgroundImage = urlString;
 }
}
function mobileSpecificImg(id) {
	 if($$D(id)) {
		  var devWidth = $rp(window).width() || document.documentElement.clientWidth;
		  var srcUrl = $rp("#" + id).attr('src');
		  var url = $rp("#" + id).attr('data-img');	 
		  var urlString = url;
		  if (urlString != undefined) {
		    var imageIndex =  url.lastIndexOf("/") + 1;
		    var hrImg = url.substring(imageIndex).split('.');
		    var orgUrl = url.substring(0, imageIndex) + hrImg[0];
		    if(devWidth < 480){
		      urlString = orgUrl +'_480px.' + hrImg[1];
		    } else if(devWidth > 481 && devWidth < 992){	  	
		      urlString = orgUrl +'_992px.' + hrImg[1];
		    }
		    if (srcUrl != urlString) {
		      $rp("#h_image8").attr("src", urlString);
		    }
		  }
    }
}
  
//Added lazy lo0ading script by Prabha on 24_Jan_2016_REL
$rp(window).scroll(function(){
  var mobileFlag = $rp("#mobileFlag").val()
  mobileSpecificImage("h_image2");
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
lazyloadetStarts();
if (mobileFlag != "false" && mobileFlag != undefined){
	mobileSpecificImg("h_image8");
}
});

function isInViewport(elem){
  var element = $rp(elem);
  var curPos = element.offset();
  var curTop = curPos.top;
  var screenHeight = $rp(window).height();
  return (curTop > screenHeight) ? false : true;
}
function changeQuestion(id){
  //$rp(".rvbar_cnt").hide();
 // $rp("#star_"+id).show();
   var starArray = ["5star_","4star_","3star_","2star_","1star_"]
 var starwidthId = ["5starWidthId","4starWidthId","3starWidthId","2starWidthId","1starWidthId"];
 var starTextId = ["5StarId","4StarId","3StarId","2StarId","1StarId"];
 for(i=0; i<5;i++){
   var fiveStarValue = $$D(starArray[i]+id).value;
   var styrlrere = $$D(starwidthId[i]).style;
   $$D(starwidthId[i]).style = "width:"+fiveStarValue+"%";
   $$D(starTextId[i]).innerHTML= fiveStarValue+"%";
 }
  var selectdid = $rp("#dropdownid_"+id).text();
  $rp("#question1").html("<span>"+selectdid+"</span>"+'<span><i class="fa fa-angle-down"></i></span>');
  $rp('#question1').attr('title',selectdid);
}
function showSubjectReview(id,collegeId,subjectId){
  var selectdid = $rp("#subjectdropdownid_"+id).text();
  var questionText = $rp("#selectedsubject").text();
  if(id == 'all'){
    $$D("selectedsubject").innerHTML = "<span>"+'All Subjects'+"</span>"+'<span><i class="fa fa-angle-down"></i></span>';
    $$D("reviewTitle").innerHTML = "Latest reviews";
  }
  else {
   $$D("selectedsubject").innerHTML = "<span>"+selectdid+"</span>"+'<span><i class="fa fa-angle-down"></i></span>';
   $$D("reviewTitle").innerHTML = "Latest "+selectdid+" reviews";
  }
  //Added for GA event logging in review search page by Prabha on SEP_24_2019_REL
  if(selectdid != ''){
    ga('send', 'event', 'Review page filter', 'Subject', selectdid.trim(), 1, {nonInteraction: true});
  }
  //End of GA event logging
  var contextPath = "/degrees";
  var ajax=new sack();
  var url = contextPath+'/get-subject-review.html?collegeId='+collegeId+"&subjectId="+subjectId;   
  ajax.requestFile = url;	
  ajax.onCompletion = function(){subjectReviewDetails(ajax)};
  ajax.runAJAX();
}
function subjectReviewDetails(ajax){
  var response = ajax.response;
  if(response != null){
    var id =  $$D("subjectReviewDetails");
   $$D("subjectReviewDetails").innerHTML = ajax.response;
  }
  $rp("#subjectDropDown").css({'display':'none','z-index':'-1'});
}
function setReviewSearchText(curid, searchmsg){var ids = curid.id; if($$(ids).value=='') { $$(ids).value=searchmsg;} }
function clearReviewSearchText(curid, searchmsg){var ids = curid.id;if($$(ids).value==searchmsg){$$(ids).value="";}}
function commonPhraseViewMoreAndLess(viewMoreOrLessBtnId){//Added this for common phrases view less and more pod by Hema.S on 18_DEC_2018_REL
  if("comPhraseViewMore"==viewMoreOrLessBtnId){    
    adv( ".extraCP" ).show();
    blockNone("comPhraseViewMore","none");
    blockNone("comPhraseViewLess","block");
  }else{
    adv( ".extraCP" ).hide();
    blockNone("comPhraseViewMore","block");
    blockNone("comPhraseViewLess","none");
  }
}
//Modified the the functionality to keyword search specific alone by Sangeeth.S for FEB_12_19 rel
function commonPhrasesListProfilePageURL(formNameId,orderBy,obj){  //Added this for looking specific pod by Hema.S on 18_DEC_2018_REL
  var searchUrl = ""; 
  var finalUrl = "";  
  var keyword = ""; 
  var queryStringUrl = "";
  var eventAction = "";
  var eventLabel = "";
  if("reviewCommonPhraseKwdForm"==formNameId && obj){
    keyword  = obj.innerText;
  }else{
    keyword    = $$('reviewSearchKwd').value;   
  }    
  if(keyword.trim()=="" || keyword=="Something specific?"){
    keyword = "";
  }       
  if(keyword!=""){
    eventLabel = keyword;
    var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
    keyword = keyword.replace(reg," ");
    keyword = replaceAll(keyword," ","-");          
    queryStringUrl = "?keyword=" + encodeURIComponent(keyword);
    searchUrl = "/university-course-reviews/search/"+queryStringUrl;
  }  
  if(searchUrl!=""){
    finalUrl = searchUrl;
  }
  //Added for GA event logging in review search page by Prabha on SEP_24_2019_REL
  if((formNameId == 'reviewSearchKwdForm' || formNameId == 'reviewCommonPhraseKwdForm') && (keyword!="")){
    eventAction = "Keyword";
  }
  if(eventAction != ''){
    ga('send', 'event', 'Review page filter', eventAction, eventLabel.trim(), 1, {nonInteraction: true});
  }
  //End of GA event logging
  if(finalUrl!=""){finalUrl= finalUrl.toLowerCase()};//Added by Sangeeth.S for FEB_12_19 rel lower case of urls  
  if(formNameId!=undefined && $$(formNameId)){
    $$(formNameId).action = finalUrl;
    $$(formNameId).submit();
  }else{
    if(finalUrl ==""){finalUrl="/university-course-reviews/";}	
    location.href = finalUrl;
  }    
  return false;
}
function validateSrchIcn(srchIconId,inputId) {   
   if(adv('#'+srchIconId) && adv('#'+inputId).val().trim()!=""){
     adv('#'+srchIconId).removeClass("srch_dis")
   }else{
     adv('#'+srchIconId).addClass("srch_dis")
   }
}
