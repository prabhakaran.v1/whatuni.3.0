function addDLProspectus(indexVal){
   var thisvalue = $$D("dpRow_"+indexVal);
   var divValue = $$D("addRemoveIcon_"+indexVal);
   if(thisvalue.className == "row"){
       thisvalue.className = "act row";
       divValue.className = "fa fa-remove fa-1_5x";
   }else if(thisvalue.className == "act row"){
      thisvalue.className = "row";
       divValue.className = "fa fa-plus fa-1_5x";
   }
}
function selectAllForDld(value,formId){
 var divClassname = "act row";
 var symbolClassName = "fa fa-remove fa-1_5x";
 if(value =="DESELECT"){
    divClassname = "row";
    symbolClassName = "fa fa-plus fa-1_5x";
    $$D("sr_selectAll").style.display = 'block';
    $$D("sr_DeSelectAll").style.display = 'none'
  }else{
     $$D("sr_selectAll").style.display = 'none';
    $$D("sr_DeSelectAll").style.display = 'block'
  }
 var myform = document.getElementById(formId);
 var inputs = myform.getElementsByClassName("row");
 for(var i = 0; i < inputs.length; i++){
    var thisvalue = document.getElementById("dpRow_"+i);
    thisvalue.className = divClassname;
    document.getElementById("addRemoveIcon_"+i).className = symbolClassName;
  }    
}

function postProspectus(obj){
  idArr = new Array;
  var j = 0;
  var myform = document.getElementById("postprospecuts");
  var inputs = myform.getElementsByClassName("act row"); 
  if(inputs.length == 0){
   alert("Please select to download");
   return false;
  }
  for(var i = 0; i < inputs.length; i++){
         idArr[j] = inputs[i].id;
         j = j+1;
  } 
  var reminputs = myform.getElementsByClassName("row");  
  for(var k = 0; k < reminputs.length; k++){
    var idd = reminputs[k].id;
    if($$D(idd).className == "row"){    
      var element = document.getElementById("hidden_"+idd);
          element.parentNode.removeChild(element);
    }
  }
  for (i=0;i<idArr.length;i++) {
    if(document.getElementById(idArr[i]) != null){   
      var checkValue = document.getElementById("hidden_"+idArr[i]).value;
        var splitValue = checkValue.split("#");
        var collegeId = splitValue[0];
        var subOrderItemId = splitValue[1];
        var networkId = splitValue[2];
        var collegeName = splitValue[6];
        var dpFlag = splitValue[4];
        var emaiPrice = splitValue[7];
        var dpURL = "";
      if (i==idArr.length-1) {                     //Changed event action prospectus-dl to webform by Sangeeth.S for July_3_18 rel
          ga('send', 'pageview', {'hitCallback': function(){document.postprospectusform.submit();}}); //Added by Amir for UA logging for 24-NOV-15 release
          GAInteractionEventTracking('dpsubmit', 'interaction', 'prospectus webform', collegeName, Number(emaiPrice));
          dpURL = splitValue[5];
          window.open(dpURL, '_blank');
          insightIntLogging(collegeId,'download prospectus submitted');        
      }else{      
          GAInteractionEventTracking('dpsubmit', 'interaction', 'prospectus webform', collegeName, Number(emaiPrice));
          dpURL = splitValue[5];
          window.open(dpURL, '_blank');
          insightIntLogging(collegeId,'download prospectus submitted');        
      }
    }
  }  
}
function waitPreloadPage() {     
  if(document.getElementById("prepage") !=null){
     if (document.getElementById){
      document.getElementById("prepage").style.visibility="hidden";
    }else{
      if (document.layers){ 
       document.prepage.visibility = 'hidden';
      }
      else { 
        document.all.prepage.style.visibility = 'hidden';
     }
   }
 }    
}
function prospectus(url){
  url=url.replace(new RegExp('&amp;', 'g'),'&');
  var theLink = ''; 
  theLink = document.getElementById('siteId');
  theLink.style.display = 'none';
  if(url){  
    theLink.href = url;    
  }  
  if((theLink) && (theLink.click)) {   
    if (document.all) {        
      theLink.click();
    }else{
      window.open(url);    
    }
  }else {  
    window.open(url);  
  }
}
function waitPreloadPage() { 
  if(document.getElementById("prepage") !=null){
    if (document.getElementById){
      document.getElementById("prepage").style.visibility="hidden";
    }else{
      if (document.layers){ 
        document.prepage.visibility = 'hidden';
      }
      else { 
        document.all.prepage.style.visibility = 'hidden';
      }
    }
  }    
}
//Post to facebook for enquiry success page by Prabha on 31_05_16
function openFbPopUpFb(uniUrl){
  var url = window.location.hostname + uniUrl;
  var quote = "I'm interested in going to ";
  var uniDispName = "";
  var enquiryType = "";
  var courseName = "";
  if($$D('prospectusDispCollegeName')){uniDispName = $$D('prospectusDispCollegeName').value;}
  if($$D('prospectusCourseName')){courseName = $$D('prospectusCourseName').value;}
  if(courseName != ""){
    quote += uniDispName +" to study "+ courseName +" after researching on Whatuni.com";
    enquiryType = "course specific enquiry";
  }else{
    quote += uniDispName +" after researching on Whatuni.com";
    enquiryType = "provider specific enquiry";
  }
  if($$D('prospectusGaCollegeName') && $$D('prospectusGaCollegeName').value != ""){
    ga('send', 'event', 'Post to facebook', enquiryType, $$D('prospectusGaCollegeName').value);
  }
  window.open('https://www.facebook.com/sharer.php?u='+encodeURIComponent(url)+'&quote='+encodeURIComponent(quote), 'sharer', 'toolbar=0,width=626,height=436');
  return false;
} 
//End of post to facebook code