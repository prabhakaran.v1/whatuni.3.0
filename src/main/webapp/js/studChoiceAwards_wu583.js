var contextPath = "/degrees";
function $$(id){return document.getElementById(id)}
var currentAwardYear = $$("currAwardYear").value;
function blockNone(thisid, value){  if($$(thisid) !=null){ $$(thisid).style.display = value;  }  }
var AwardBitUrl = { 
  uniOfTheYear : "http://bit.ly/1RuLyh3",
  jobProspects : "http://bit.ly/1LGjUez",
  courseAndLecturers : "http://bit.ly/21GNQJl",
  studentUnion  : "http://bit.ly/1WHL9Gk",
  accommodation : "http://bit.ly/22vwPYh",
  uniFacilities : "http://bit.ly/1MBN49N",
  cityLife : "http://bit.ly/1S5DGNk",
  clubsAndSocieties : "http://bit.ly/1UFjmIt",
  studentSupport : "http://bit.ly/1q1O81N",
  givingBack : "http://bit.ly/2llEJEv",
  postgraduate : "http://bit.ly/2llmrDj",
  international : "http://bit.ly/1S5DMo5",
  indHigherEducation : "http://bit.ly/2p3KSoD",
  furtherEducationColleges : "http://bit.ly/2FzfwMT"
}
var CategoryList = { 
  uniOfTheYear : "university-of-the-year",
  jobProspects    : "job-prospects",
  courseAndLecturers : "course-and-lecturers",
  studentUnion  : "student-union",
  accommodation : "accommodation",
  uniFacilities : "uni-facilities",
  cityLife : "city-life",
  clubsAndSocieties : "clubs-and-societies",
  studentSupport : "student-support",
  givingBack : "giving-back",
  postgraduate : "postgraduate",
  international : "international",
  indHigherEducation : "independent-higher-education",
  furtherEducationColleges : "further-education-college"
}
var CategoryId = {
  uniOfTheYear : "4",
  jobProspects    : "12",
  courseAndLecturers : "9",
  studentUnion  : "5",
  accommodation : "7",
  uniFacilities : "8",
  cityLife : "11",
  clubsAndSocieties : "6",
  studentSupport : "41",
  givingBack : "46",
  postgraduate : "40",
  international : "10",
  indHigherEducation : "48",
  furtherEducationColleges : "49"
}
function loadUniAwardResult() {
  var UnivId = "" ;  
  var categoryId =  $$("categoryId").value;
  if($$("uniName_id")){
    if($$("uniName_id").value==""){
      alert("Please enter university name.");
      return false;
    }else{
      UnivId = $$("uniName_id").value;           
      var ajax = new sack();
      blockNone("studchLoadResgif","block");  
      var loadYear = $$("loadYear") ? $$("loadYear").value : "";
      url = contextPath+"/ajax-studchoice-results.html?studCateId="+categoryId+"&loadUnivId="+UnivId+"&loadYear="+loadYear+"&ajaxSrch=true";            
      ajax.requestFile = url;
      ajax.onCompletion = function(){loadStudAjaxRes(ajax);};
      ajax.runAJAX();
    }  
  }
}
function loadStudentYear(objVal){
  currentAwardYear = $$("currentAwardYear") ? $$("currentAwardYear").value : currentAwardYear;
  if(objVal == currentAwardYear && currentAwardYear > 2015){
    window.location.href="/student-awards-winners/university-of-the-year/";
    $$("loadYear").value = objVal;
  } else {
    window.location.href=contextPath+"/student-awards-winners/" + objVal +".html";
  }
}
function loadStudCateResult(bckCateId) {
  var loadYear = $$("loadYear") ? $$("loadYear").value : "";
  var categoryId =  $$("categoryId").value;
  currentAwardYear = $$("currentAwardYear") ? $$("currentAwardYear").value : currentAwardYear;
  if(loadYear < currentAwardYear) { 
    var UnivId = "" ;
    var categoryText = $$("categoryId").options[$$("categoryId").selectedIndex].text;  
    $$("awdcatSpan").innerHTML = categoryText;    
    if($$("uniName")){
      if($$("uniName").value=="Enter university name"){
        resetHidUniValue();
      }
    }
    if($$("uniName_id")){
      if($$("uniName_id").value!=""){ 
        UnivId = $$("uniName_id").value;
      }
    }  
    var ajax = new sack();
    blockNone("studchLoadResgif", "block");  
    if(bckCateId!=undefined && bckCateId!=""){
      $$("uniName").value="Enter university name";
      resetHidUniValue();
      url = contextPath+"/ajax-studchoice-results.html?studCateId="+categoryId+"&loadUnivId=&loadYear="+loadYear+"&ajaxSrch=true";    
    }else{
      url = contextPath+"/ajax-studchoice-results.html?studCateId="+categoryId+"&loadUnivId="+UnivId+"&loadYear="+loadYear+"&ajaxSrch=true";  
    }
    ajax.requestFile = url;
    ajax.onCompletion = function(){loadStudAjaxRes(ajax);};
    ajax.runAJAX();  
  } else {
    //window.location.href="/student-awards-winners/" + getCategoryName(categoryId) + "/";    
    var finalUrl = "/student-awards-winners/" + getCategoryName(categoryId) + "/";
    if(bckCateId!=undefined && bckCateId!=""){
      $$("uniName_id").value="";
      $$("uniName_display").value="";
    }
    $$("wuscaUniForm").action = finalUrl.toLowerCase();
    $$("wuscaUniForm").submit();
    return true;
  }
}
//Added by Indumathi.S for Mar-29-2016 Rel URL restructuting for current page
function getCategoryName(catId){
 var catName = CategoryList.uniOfTheYear;
  if(CategoryId.uniOfTheYear == catId){
    catName = CategoryList.uniOfTheYear;
  } else if(CategoryId.jobProspects == catId){
    catName = CategoryList.jobProspects;
  } else if(CategoryId.courseAndLecturers == catId){
    catName = CategoryList.courseAndLecturers;
  } else if(CategoryId.studentUnion == catId){
    catName = CategoryList.studentUnion;
  } else if(CategoryId.accommodation == catId){
    catName = CategoryList.accommodation;
  } else if(CategoryId.uniFacilities == catId){
    catName = CategoryList.uniFacilities;
  } else if(CategoryId.cityLife == catId){
    catName = CategoryList.cityLife;
  } else if(CategoryId.clubsAndSocieties == catId){
    catName = CategoryList.clubsAndSocieties;
  } else if(CategoryId.studentSupport == catId){
    catName = CategoryList.studentSupport;
  } else if(CategoryId.givingBack == catId){
    catName = CategoryList.givingBack;
  } else if(CategoryId.postgraduate == catId){
    catName = CategoryList.postgraduate;
  } else if(CategoryId.international == catId){
    catName = CategoryList.international;
  } else if(CategoryId.indHigherEducation == catId){
    catName = CategoryList.indHigherEducation;
  } else if(CategoryId.furtherEducationColleges == catId){
    catName = CategoryList.furtherEducationColleges;
  }
  return catName;
}
function loadStudAjaxRes(ajax) {
  var awdRes = jQuery.noConflict();  
  $$("studChoiceRes").innerHTML = "";   
  removeExistIds()
  var response = ajax.response;     
  blockNone("studchLoadResgif","none");
  $$("studChoiceRes").innerHTML = response;  
  lazyloadetStarts();
  if($$("cateSponserImg") && $$("cateSponserImg").value!=""){                
    blockNone("sponserDiv","block");         
    var cateSponserImg = $$("cateSponserImg").value;
    var cateSponserTxt = ($$("cateSponserTxt").value).replace(".jpg","");    
    awdRes("#cateSpoImg").attr({"src" : cateSponserImg,"alt" : cateSponserTxt});
    if($$("cateSponserURL")){
      if($$("cateSponserURL").value!=""){         
        var cateSponserURL = $$("cateSponserURL").value;        
        awdRes("#cateSpoUrl").attr({"href" : cateSponserURL});
      }
    }
  }else{
    blockNone("sponserDiv","none"); 
    $$("cateSpoImg").src = "";  
  }  
  if($$("shwsky")){    
    blockNone("sky","none");
  }else{
    blockNone("sky", "block");
  }
}

function loadAjaxPaginationRes() {      
  var awdRes = jQuery.noConflict();    
  awdRes(window).scroll(function() {
    if($$("curPageNo")) {
      if(parseInt($$("curPageNo").value) < parseInt($$("totPageNo").value)) {             
        var curPageNo = $$("curPageNo").value;        
        var childDiv  = "#loadAjaxDiv_"+curPageNo;                
        if($$("loadAjaxDiv_"+curPageNo)) {          
          if(isScrolledIntoView(awdRes(childDiv))) {                          
            var nextPage = parseInt($$("curPageNo").value) + 1;        
            studAjaxLoadNextset(nextPage);     
            removeExistIds();  
          }  
        }        
      }
    } 
  });     
}

function studAjaxLoadNextset(nextPage) {  
  var categoryId =  $$("categoryId").value;
  var ajax = new sack();
  blockNone("loader", "block");
  blockNone("loader_text", "block");
  var loadYear = $$("loadYear") ? $$("loadYear").value : "";
  url = contextPath+"/ajax-studchoice-results.html?studCateId="+categoryId+"&loadYear="+loadYear+"&pageNo="+nextPage;    
  ajax.requestFile = url;
  ajax.onCompletion = function(){appendPageResult(ajax,nextPage);};
  ajax.runAJAX();  
}

function appendPageResult(ajax,nextPage) {
  if($$('next_results_'+nextPage)){
    var response = ajax.response;   
    blockNone("loader", "none");
    blockNone("loader_text", "none");
    $$('next_results_'+nextPage).innerHTML = response;      
  }  
}

var stawd = jQuery.noConflict();
stawd(document).ready(function(){ 
  stawd("#hd1").removeAttr("style");
  stawd('.lft_nav .brw').click(function(){
    stawd(this).toggleClass('act');
    stawd(this).parents().next().toggleClass('act');    
  });
});

function removeExistIds() {
  var awdRes = jQuery.noConflict();
  awdRes("#cateSponserImg").remove();
  awdRes("#cateSponserURL").remove(); 
  awdRes("#cateSponserTxt").remove();
  awdRes("#curPageNo").remove();
  awdRes("#totPageNo").remove();
}

function setSchUniDefaultTEXT(text,id){
  if($$(id).value==""){
    $$(id).value = text;
  }  
}

var opdup = jQuery.noConflict();
opdup(window).scroll(function(){ // scroll event 
    lazyloadetStarts();
});
opdup(document).ready(function(){
    lazyloadetStarts();
});

function isScrolledIntoView(elem){
  var docViewTop = opdup(window).scrollTop();
  var docViewBottom = docViewTop + opdup(window).height();
  var elemTop = opdup(elem).offset().top;
  var elemBottom = elemTop + opdup(elem).height();
  return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom));
}

function resetHidUniValue(){
  if($$("uniName_hidden")){
    $$("uniName_hidden").value = "";    
  }
  if($$("uniName_id")){  
    $$("uniName_id").value = "";    
  }
  if($$("uniName_display")){  
    $$("uniName_display").value = "";      
  }
  if($$("uniName_url")){  
    $$("uniName_url").value = "";  		
  }
  if($$("uniName_alias")){
    $$("uniName_alias").value = "";  
  }  
}
//Added by Indumathi.S showhide tweet this 29_Mar_16
function showAndHideTweet(tweetId){
  var jq = jQuery.noConflict(); 
  jq("#"+tweetId).toggle();
  jq(".chktxt").each(function() {
    if(this.id != tweetId) { 
      blockNone(this.id,"none"); 
    }
  });
}
function showTweetThis(tweetId){
 blockNone(tweetId,"block");
}

function hideTweetThis(tweetId){
  blockNone(tweetId,"none");
}

function tweetThisUrlFnc(colgDispNameId,obj) {
  var res = encodeURIComponent($$(colgDispNameId).value + "'s place in "+ currentAwardYear +" #WUSCA. Read full results: " + categoryBasedUrl());
  twitterUrl = "http://www.twitter.com/intent/tweet?text=" + res;
  window.open(twitterUrl, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');
  eventTimeTracking();
  ga('send', 'event', 'tweet-this', 'clicked', $$(colgDispNameId).value);  
}

function categoryBasedUrl(){
 var awardUrl = (window.location.href).split("/");
 var category = awardUrl[awardUrl.length-2];
 if(category == CategoryList.uniOfTheYear){
   return AwardBitUrl.uniOfTheYear;
 } else if(category == CategoryList.jobProspects){
   return AwardBitUrl.jobProspects;
 } else if(category == CategoryList.courseAndLecturers){
   return AwardBitUrl.courseAndLecturers;
 } else if(category == CategoryList.studentUnion){
   return AwardBitUrl.studentUnion;
 } else if(category == CategoryList.accommodation){
   return AwardBitUrl.accommodation;
 } else if(category == CategoryList.uniFacilities){
   return AwardBitUrl.uniFacilities;
 } else if(category == CategoryList.cityLife){
   return AwardBitUrl.cityLife;
 } else if(category == CategoryList.clubsAndSocieties){
   return AwardBitUrl.clubsAndSocieties;
 } else if(category == CategoryList.studentSupport){
   return AwardBitUrl.studentSupport;
 } else if(category == CategoryList.givingBack){
   return AwardBitUrl.givingBack;
 } else if(category == CategoryList.postgraduate){
   return AwardBitUrl.postgraduate;
 } else if(category == CategoryList.international){
   return AwardBitUrl.international;
 } else if(category == CategoryList.indHigherEducation){
   return AwardBitUrl.indHigherEducation;
 } else if(category == CategoryList.furtherEducationColleges){
   return AwardBitUrl.furtherEducationColleges;
 }  
 return AwardBitUrl.uniOfTheYear;
}

//End
//Added to display the interactions buton on hovering 
stawd(document).on({
    mouseenter: function () {
       console.log("inside doc");
        stawd(this).find(".c9").css({"visibility":"visible"});
    },
    mouseleave: function () {
        stawd(this).find(".c9").css({"visibility":"hidden"});
    }
}, '.row-fluid');