var contextPath = "/degrees";
var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";
var numbers = /^[0-9]+$/;
function $$D(id){ return document.getElementById(id); }
function blockNoneDiv(thisid, value){  if($$D(thisid) !=null){ $$D(thisid).style.display = value;  }  }
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
var $ = jQuery.noConflict();
///
function isNullAndUndef(variable) {return (variable !== null && variable !== undefined && variable !== '');}
function isNotNullAnddUndef(variable) {return (variable !== null && variable !== undefined && variable !== '');}
var successFlag = 'Y';
function setGradeVal(obj,grdeArrId){  
  var optVal = '';
  var type = $($(obj).get(0)).prop("tagName").toLowerCase();
  console.log(type);  
  if(type == "input"){
    /*var ucsIpId = jq(obj).attr('id');	  
    var subIpId = ucsIpId.replace('span_grde', 'qualSub');
    var ucsIpId = ucsIpId.replace('span_grde', 'span_grde');
    var errDivId = ucsIpId.replace('span_grde', 'err_msg');  
	jq('#'+subIpId).removeClass('faild');
	jq('#'+ucsIpId).removeClass('faild');
	jq('#'+errDivId).hide();*/
	optVal = $(obj).val();
  }else{
	optVal = $(obj).children("option:selected").val();
  }
  var spanValId = 'span_'+ grdeArrId;
  $('#'+grdeArrId).val(optVal);
  $('#'+spanValId).text(optVal);
}
//
function setAjaxSubjectVal(ajaxOptionObj){
	  var grandParentDivID = $(ajaxOptionObj).parent().parent().parent().attr('id');
	  var parentDivID = $(ajaxOptionObj).parent().parent().attr('id');
	  var subSrchIpID = parentDivID.replace('subjectAjaxList_', '');
	  var subjectNameVal = $(ajaxOptionObj).text().trim();
	  var subjectIdVal = ajaxOptionObj.id;
	  var subArrHidId = 'sub' + grandParentDivID.replace('ajaxdiv', '');
	  $('#'+subSrchIpID).val(subjectNameVal);
	  $('#'+grandParentDivID).hide();
	  $('#'+subArrHidId).val(subjectIdVal);
	  $('#'+subSrchIpID).removeClass('faild');
	  $('#'+subSrchIpID).blur();
	  $("#selectAns_12").removeClass("cur-disabled");
	  $("#selectAns_41").removeClass("cur-disabled");
	  $("#selectAns_42").removeClass("cur-disabled");
	}
	//
	var srch_cur_let = new Array();
	function cmmtQualSubjectList(obj, id, action, resultDivId){
	 var url = contextPath + "/uni-course-requirements-match.html";
	 var resId = resultDivId;
	  action = 'QUAL_SUB_AJAX';
	 var freeText = $('#'+obj.id).val().trim();
	 var subArrId = 'sub_'+ id.replace('qualSub_', '');
	 $('#'+subArrId).val('');
	 if(srch_cur_let[obj.id] == freeText){
	   return;
	 }
	 srch_cur_let[obj.id]= freeText;
	 var qualSelId = id.substring(id.indexOf('_')+1, id.lastIndexOf("_"));
	 var qualId = $('#qualsel_'+qualSelId).children("option:selected").val();
	 qualId = qualId.indexOf('gcse') > -1 ? qualId.substring(0, qualId.indexOf("_")) : qualId;
	 if(isNotNullAnddUndef(freeText) && (freeText.length > 2 || qualId == '18')){ 
	  //
	  var stringJson =	{
	    "action": action,
	    "subQualId": qualId,
	    "qualSubList": createJSON()
	  };
	  var jsonData = JSON.stringify(stringJson);
	  var param = "?action="+action+"&free-text="+freeText;
	  var url = contextPath + "/uni-course-requirements-match.html" + param;
	  $.ajax({
	    url: url, 
	    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
	    dataType: 'json',
	    type: "POST",
	    data: jsonData,
	    async: false,
	    complete: function(response){
	      var result = response.responseText;
	      var selectDivId= "subjectAjaxList_"+id;
	      if(result.trim() == ''){
	        result = '<div class="nores">No results found for ' + freeText + '</div>';
	      };
	      var selectBox = '<div name="subjectAjaxList" onchange="" id='+selectDivId+' class=""><div id="ajax_listOfOptions" class="ajx_cnt">'+result+'</div></div>';
	      $('[data-id=wugoSubAjx]').each(function(){
	        $(this).html('');
	      });
	      $("#"+resId).html(selectBox);
	      var ajxwd = $('#'+id).outerWidth() + 'px';     
	      var ajxht = document.getElementById(id).offsetHeight + 'px';      
	      $('#ajax_listOfOptions').css('top', ajxht);
	      $('#ajax_listOfOptions').css('width', ajxwd);
	      $("#"+resId).show();  
	    }
	  });
	  }
	  else{
	    $('#'+resId).hide();
	    if(!isNotNullAnddUndef(freeText)){
	      $('#'+id).removeClass('faild');
	    }
	  }
	}

	function appendGradeDropdown(iloop, gdeId, selVal){
		  var qualId  = $('#qualsel_'+iloop).val();
		  var data = $('#qualGrde_'+iloop+'_'+qualId).val().split(',');
		  var id = gdeId;
		  $.each(data, function(value) {
		  var $option = $("<option/>", {
		    value: data[value],
		    text: data[value]
		  });
		  $('#'+id).append($option);
		  $("#"+id).val(selVal);
		  });
		}
		function addNewRowSub(qualSelId){
		  var qualId = $('#'+qualSelId).children("option:selected").val();
		  appendQualDiv($('#'+qualSelId), 1);
		}
		//selecting the value of subject dropdown
		function setSelectedVal(formId, id){
		  var value = $('#'+id).children("option:selected").text();
		  $('#'+formId).html(value);
		}
		function appendQualDiv(obj, addVal, loop, pageType, addQualFlag){
		  var qualId = '1';
		  if('add_Qual' == addQualFlag){
		    qualId = '1';
		  } else{
		    qualId = obj.value;
		  }
		  var qualIdForQualArr = qualId;
		  var subArrDataId = '';
		  var count = $('#qualSubj_'+loop+'_'+qualId).val();
		  subArrDataId = 'data-id="level_3"';
		  if(isNotNullAnddUndef(addVal)){ 
		    count = count + addVal;
		  }
		  var result = "";
		  var resSt = '<div class="ucas_row grd_row" id="grdrow_'+loop+'"><div class="rmv_act" id="subSec_' + loop +'"><fieldset class="ucas_fldrw">';
		  var resEd = '</fieldset></div>';
		  var hidSubCntId = 'countAddSubj_'+ loop;
		  $('#'+hidSubCntId).val(count);
		  $('#total_'+hidSubCntId).val(count);
		  var gradeVal = $('#qualGrde_'+loop+'_'+qualId).val();
		  var gdeArr = gradeVal.split(',');
		  var gradeSpanEd = '</div>';
		  result = '<div class="ucas_row grd_row" id="grdrow_'+loop+'">';
//		  result += '<fieldset class="ucas_fldrw quagrd_tit"><div class="ucas_tbox tx-bx"><h5 class="cmn-tit txt-upr qual_sub">Subject</h5></div><div class="ucas_selcnt rsize_wdth">'
//		         +'<h5 class="cmn-tit txt-upr qual_grd">Grade</h5></div></fieldset>';
		 
		  for(lp = 0; lp < count; lp++){
		    var removeSub = "'subSec_"+loop + "_" + lp +"'";
		    var grdeArrId = "'grde_"+loop+"_"+lp+"'";
		    var subTxtId = "'qualSub_"+loop+"_"+lp+"'";
		    var subAjaxId = "'ajaxdiv_"+loop+"_"+lp+"'";
		    var actionParam = "'QUAL_SUB_AJAX'";
		    var grdeSpanId = "'span_grde_" + +loop+"_"+lp+"'";
		    var qualGrdId = "'qualGrd_"+loop+"_"+lp+"'";
		    var grdStr = "'GRADE'";
		    var subValId = "'sub_"+loop+"_"+lp+"'";   
		    var removeLinkImg = $('#removeLinkImg').val();
		    var countAddSubId = "'countAddSubj_"+loop+"'";
		    var addSubstyle = "display:none";
		    result += '<div class="rmv_act" id="subSec_'+loop + '_' + lp +'"><fieldset class="ucas_fldrw">'
		           + '<div class="ucas_tbox w-390 tx-bx"><input type="text" data-id="qual_sub_id" id="qualSub_'+loop+'_'+lp+'" name="'+qualId + 'sub_'+ lp +'" placeholder="Enter subject" value="" onkeyup="cmmtQualSubjectList(this, '+subTxtId+', '+actionParam+', '+subAjaxId+');"  class="frm-ele" autocomplete="off"><div id="ajaxdiv_'+loop+'_'+lp+'" data-id="wugoSubAjx" class="ajaxdiv"></div></div>';		           
		    if(qualId != '19'){        
	        result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="span_grde_'+loop+'_'+lp+'">'+gdeArr[0]+'</span><span class="grdfilt_arw"></span></div>'
		           + '<select class="ucas_slist" onchange="setGradeVal(this, '+grdeArrId+');" id="qualGrd_'+ loop +'_'+ lp +'"></select>'
		           + '<script>appendGradeDropdown('+loop+', '+qualGrdId+'); </script>'
		           + '</div>';
	        }else {    
	        result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_tbox w-390 tx-bx fl_w100 tps" id="1grades1"><input onkeyup="setGradeVal(this, '+grdeArrId+');" onfocus="clearFields(this)" type="text" maxlength="3" class="frm-ele fl_100" id="span_grde_'+loop+'_'+lp+'" placeholder="" value="'+gdeArr[0]+'" autocomplete="off"></input></div>'
	               + '</div>';	   
	        }      
		    result += '<div class="add_fld"> <a id="1rSubRow3" onclick="removeSubject('+removeSub+', '+loop+');" class="btn_act1 bck f-14 pt-5"><span class="hd-desk"><img class="aq_img" src="'+removeLinkImg+'" title="Remove subject &amp; grade"></span></a></div>'
		           + resEd
		           + '<input class="subjectArr" '+subArrDataId+' type="hidden" id="sub_'+loop+'_'+lp+'" value="" />'
		           + '<input class="gradeArr" type="hidden" id="grde_'+loop+'_'+lp+'" value="'+gdeArr[0]+'" />'
		           + '<input class="qualArr" type="hidden" id="qual_'+loop+'_'+lp+'" value="'+qualIdForQualArr+'" />'
		           + '<input class="qualSeqArr" type="hidden" id="qual_'+loop+'_'+lp+'" value="'+(parseInt(loop)+1)+'" />';
		  
		  }    
		  if(count < 6){
		    addSubstyle = "display:block";
		  } else{
		    addSubstyle = "display:none";
		  }
		  result += '<div class="ad-subjt" id="addSubject_'+loop+'" style="'+addSubstyle+'">'
		         + '<a class="btn_act bck fnrm" id="subAdd'+loop+'" onclick="addSubject('+qualIdForQualArr+','+countAddSubId+', '+loop+');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>'
		         + '</div>';  
		  result += '</div>';
		  $('#subGrdDiv'+loop).text('');
		  $('#subGrdDiv'+loop).append(result);
		  $('#subGrdDiv'+loop).find("script").each(function(i) {
		    eval($(this).text());
		  });
		  if(1 == count){
		    $('#grdrow_'+loop).find('a:first').hide();
		  } else{$('#grdrow_'+loop).find('a:first').show();}
		  if('add_Qual' == addQualFlag || 'add_Qual_gcse' == addQualFlag){
		    $('#qualsel_'+loop).val(qualId);
		  }
		  var selTextVal = $('#qualsel_'+loop+' option:selected').text();  
		  $('#newQualDesc').val(selTextVal);
		  $('#qualspan_'+loop).text(selTextVal);
		  var qualSubListVal = createJSON();
		  if(qualSubListVal.length > 0){
		    //getUcasPoints('', '', 'CHANGE_QUAL');
		  } else{
		    var crsReqUcasPts = $('#coureReqTariffPts').val();
		    $('#oldGrade').text("0");
		    $('#newGrade').text("0");
		  }   
		}
		var maxSubLimit = 6;
		function removeSubject(subDivId, loop){
		  var removeVal = subDivId;
		  var subId = 'qualSub_' + removeVal.replace('subSec_','');  
		  $('#'+ removeVal).remove();
		  var res = removeVal.substring(6);
		  $('#sub'+res).val('');
		  $('#grde'+res).val('');
		  $('#qual'+res).val('');
		  $('#qual'+res).val('');
		  var rmvUcas = "sub"+res;
		  //getUcasPoints(rmvUcas,'', 'REMOVE_SUB');
		  var subCount = $('#countAddSubj_' + loop).val();
		  var count = subCount - 1;
		  var hidSubCntId = 'countAddSubj_'+loop;
		  $('#'+hidSubCntId).val(count);
		  if(count <= maxSubLimit) { $('#addSubject_'+loop).show(); }
		  if(1 == count){
		    $('#grdrow_'+loop).find('a:first').hide();
		  } else{$('#grdrow_'+loop).find('a:first').show();}
		}
		function setSelectedVal(formId, id){
		  var value = $('#'+id).children("option:selected").text();
		  var selecIndex = $('#'+id).children("option:selected").val();
		  $('#'+formId).html(value);
		  var loop = formId.substring(6);
		  $('#userQualId'+loop).val();
		}
		function addSubject(qualId, subCntId, loop){
		  var selectedQual = $('#qualsel_'+loop).val();
		  var subCount = $('#' + subCntId).val();
		  var totSubCnt = $('#total_'+ subCntId).val();
		  var subArrDataId = '';
		  var qualIdForQualArr = selectedQual;
		  result = '<div class="ucas_row grd_row" id="grdrow_'+qualId+''+loop+'">';
		  if(isNotNullAnddUndef(selectedQual) && selectedQual.indexOf('_gcse') > -1){
		    var gradeVal = $('#qualGrde_'+loop+'_'+selectedQual).val();
		    qualIdForQualArr = qualIdForQualArr.substring(0, selectedQual.indexOf('_gcse'));
		    maxSubLimit = 20;
		    subArrDataId = '';
		  } else{
		    var gradeVal = $('#qualGrde_'+loop+'_'+selectedQual).val();
		    if(qualId == 18) {maxSubLimit = 3;} 
		    else { maxSubLimit = 6; }
		    subArrDataId = 'data-id="level_3"';
		  }
		  var gdeArr = gradeVal.split(',');
		  var enOrDisTopNavFn = "enOrDisTopNav('offerEditQual');";
		  if(subCount <= maxSubLimit) {
		    var count = parseInt(subCount) + 1;
		    var totCnt = parseInt(totSubCnt) + 1;
		    var hidSubCntId = subCntId;
		    $('#'+hidSubCntId).val(count);
		    $('#total_'+hidSubCntId).val(totCnt);
		    var removeSub = "'subSec_"+loop + "_" + totCnt +"'";
		    var grdeArrId = "'grde_"+loop+"_"+totCnt+"'";
		    var subTxtId = "'qualSub_"+loop+"_"+totCnt+"'";
		    var scrhIpId = "'qualSub_"+loop+"_"+totCnt+"'";
		    var subAjaxId = "'ajaxdiv_"+loop+"_"+totCnt+"'";
		    var actionParam = "'QUAL_SUB_AJAX'";
		    var qualGrdId = "'qualGrd_"+loop+"_"+totCnt+"'";
		    var grdStr = "'GRADE'";
		    var subValId = "'sub_"+loop+"_"+totCnt+"'";
		    var removeLinkImg = $('#removeLinkImg').val();
		    result = '<div class="rmv_act" id="subSec_'+loop + '_' + totCnt +'"><fieldset class="ucas_fldrw">'
		           + '<div class="ucas_tbox w-390 tx-bx"><input type="text" data-id="qual_sub_id" id="qualSub_'+loop+'_'+totCnt+'" name="'+qualId + 'sub_'+ totCnt +'" placeholder="Enter subject" value="" onkeyup="cmmtQualSubjectList(this, '+scrhIpId+', '+actionParam+', '+subAjaxId+');"  class="frm-ele" autocomplete="off"><div id="ajaxdiv_'+loop+'_'+totCnt+'" data-id="wugoSubAjx" class="ajaxdiv"></div></div>';		           
		    if(qualId != '19'){        
	        result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="span_grde_' + loop +'_'+totCnt+'">'+gdeArr[0]+'</span><span class="grdfilt_arw"></span></div>'
		           + '<select class="ucas_slist" onchange="setGradeVal(this, '+grdeArrId+');" id="qualGrd_'+ loop +'_'+ totCnt +'"></select>'
		           + '<script>appendGradeDropdown('+loop+', '+qualGrdId+');</script>'
		           + '</div>';
	        }else {    
	        result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_tbox w-390 tx-bx fl_w100 tps" id="1grades1"><input onkeyup="setGradeVal(this, '+grdeArrId+');" onfocus="clearFields(this)" type="text" maxlength="3" class="frm-ele fl_100" id="span_grde_'+loop+'_'+lp+'"  placeholder="" value="'+gdeArr[0]+'" autocomplete="off"></input></div>'
	               + '</div>';	   
	        }     
		    result += '<div class="add_fld"> <a id="1rSubRow3" onclick="removeSubject('+removeSub+', '+loop+');" class="btn_act1 bck f-14 pt-5"><span class="hd-desk"><img class="aq_img" src="'+removeLinkImg+'" title="Remove subject &amp; grade"></span></a></div>'
		           + '<input class="subjectArr" '+subArrDataId+' type="hidden" id="sub_'+loop+'_'+totCnt+'" value="" />'
		           + '<input class="gradeArr" type="hidden" id="grde_'+loop+'_'+totCnt+'" value="'+gdeArr[0]+'" />'
		           + '<input class="qualArr" type="hidden" id="qual_'+loop+'_'+totCnt+'" value="'+qualIdForQualArr+'" />'
		           + '<input class="qualSeqArr" type="hidden" id="qual_'+loop+'_'+totCnt+'" value="'+(parseInt(loop)+1)+'" />';
		    if(count == maxSubLimit){$('#addSubject_'+loop).hide();} 
		  } else{ $('#addSubject_'+loop).hide(); }
		    result += '</div>';
		    $('#subGrdDiv'+loop).find('.rmv_act').last().after(result);
		    $('#subGrdDiv'+loop).find("script").each(function(i) {
		  });
		  if(count > 1){
		    $('#grdrow_'+loop).find('a:first').show();
		  }
		}
		function createJSON(){
		  var qualId = '';$('.qualArr').each(function(){qualId += this.value + ','; });
		  var subId = '';$('.subjectArr').each(function(){subId += this.value + ',';});
		  var grade = '';$('.gradeArr').each(function(){grade += this.value + ',';});
		  var qualSeq = '';$('.qualSeqArr').each(function(){qualSeq += this.value + ',';});
		  var count = subId.length;
		  var qualIdArr = (qualId != null && qualId != '') ? qualId.split(',') : '';
		  var subIdArr = (subId != null && subId != '') ? subId.split(',') : '';
		  var gradeArr = (grade != null && grade != '') ? grade.split(','): '';
		  var qualSeqArr = (qualSeq != null && qualSeq != '') ? qualSeq.split(','): '';
		  var obj = [];
		  for (i = 0; i < qualIdArr.length; i += 1) {
		    var qual = qualIdArr[i];var sub = subIdArr[i];var grde = gradeArr[i]; var lp = i+1;var qualSeq = qualSeqArr[i];
		    if(qual != '' && sub!= '' && qual != '19'){  
		      tmp = {
		        "qualId": qual,
		        "subId": sub,
		        "grade": grde,
		        "qualSeqId": qualSeq,
		        "subSeqId": lp
		      };
		     obj.push(tmp);
		    }else if(qual != '' && sub!= '' && qual == '19' && (grde != '' && grde > 0 && grde < 1000 && (grde.match(numbers)))){  
		      tmp = {
		        "qualId": qual,
		        "subId": sub,
		        "grade": grde,
		        "qualSeqId": qualSeq,
		        "subSeqId": lp
		      };
		     obj.push(tmp);
			}
		  }
		  return obj;
		}
		function addQualification(qualDivId, nxtQualDivCnt, addQualBtnId){
			  $('#'+qualDivId).show();
			  var qualloop = nxtQualDivCnt+1;
			  if($('#level_3_qual_'+ qualloop).css('display') == 'block'){
			    $('#addQualBtn_'+ nxtQualDivCnt).hide();
			  }  
			  $("#qualsel_"+nxtQualDivCnt).val("1");
			  appendQualDiv(1, '', nxtQualDivCnt, 'new_entry_page', 'add_Qual');
			  if($('#level_3_qual_'+nxtQualDivCnt).css('display')!='none' && $('[data-id=level_3_add_qualif]').eq(1).attr('id') == ('level_3_qual_'+nxtQualDivCnt)){
			    var curAddQualCont =  $("[data-id=level_3_add_qualif]").eq(1);    
			    $("[data-id=level_3_add_qualif]").last().after(curAddQualCont);      
			  }
			  var errorflag = true;
			  return errorflag;
						}
///
		
function checkNull(id){
 if($$D(id)){
  var inString = $$D(id).value;
 if (inString.length != 0 && inString != 'null' && inString != 'undefined' && inString != 'NaN') {
  return true;
 }
 }
 return false;
}
var Constants = {
  gtmQuizCategory : "",
	quizContent : "",
  loadImgConst : '<img src="'+wu_scheme+document.domain+'/wu-cont/images/cht_loader.gif" alt="" width="40"/>',
  pqAlevelErrMsg : '<p class="rd">Woah, steady on there!</p><p>You can only enter a max of 6 grades. Put in your best ones if you have more</p>',
  pqSqaErrMsg : '<p class="rd">Woah, steady on there!</p><p>You can only enter a max of 10 grades. Put in your best ones if you have more</p>',
  pqBtecErrMsg : '<p class="rd">Woah, steady on there!</p><p>You can only enter a max of 3 grades. Put in your best ones if you have more</p>',
  dimensionLog11 : ""
};
function decodeUsingTextArea(label) {
	var textArea = document.createElement('textarea');
	textArea.innerHTML = label;
	return textArea.value;
}
function isBlankOrNull(value){
	if(value == null || value == 'null' || value == '' || value == 'undefined' || value == 'Not chosen' || value == 'Not selected'){return true;}
	return false;
}
var quizquestion = null;
var quizbox = null;
var tempId = "";
var adjustHeight = 0;
var quizproceedTime  = null;
function clearAllTimeout() {
  // clear all timeout 
  if(quizquestion!=null) {
    clearTimeout(quizquestion);
  }
  if(quizbox!=null) {
    clearTimeout(quizbox);
  }  
}
//
function skipQuestionFn(){
	$("#skipQuesDiv").hide(); $("#skipAndViewResults").show();		
  $("#footerContent").addClass('ft_skp_que');
	$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
}
//
function getIndexQuizDetails(){
  $(document).ready(function () {     
    var root = window.parent.document.getElementsByTagName( 'html')[0];     
    root.className += ' scrl_dis';
    var holder = window.parent.document.getElementById("holder_NRW");
    if(!isBlankOrNull(holder)) { holder.className += " holder_NRW"; }
    var iframe_NRW = window.parent.document.getElementById("iframe_NRW");
    if(!isBlankOrNull(iframe_NRW)) { iframe_NRW.style.height = "100%"; }    
    //
    var questionListSize = $("#questionListSize").val();
    var questionId = $("#questionId").val();
    var timeDelay =  ($$D('fromPlace').value == 'true') ? 0 : $("#timeDeplay").val();
    setTimeout(function() {
      var indexid = 0;
      if($$D('fromPlace').value == 'true') {
        indexid = 1;
      }
      for(index=indexid; index<questionListSize; index++){
        if(index == 0){
          $("#image_"+questionId+index).hide();	
          $("#questionTxt_"+questionId+index).show();
          $("#question_"+questionId+(index+1)).show(); 
        }
      }
	  //
    if($$D('fromPlace').value == 'true') {
      getQuizDetails('13', '', 'Yes', '1', '', 'yesNo');
    }
    //
    }, timeDelay);
    //
    $('.srt_list .fav_ico').click(function () {
      $(this).toggleClass('animated flash fav_fill');
      if ($(this).hasClass('animated')) {
        $('.add_fav').addClass('rev');
      } else {
        $('.add_fav').removeClass('rev');
      }
    });
    
    if(!isBlankOrNull($('#TAKE_QUIZ').val())) {
			Constants.gtmQuizCategory = 'Quiz Return';			
		} else {
			Constants.gtmQuizCategory = 'Quiz';
		}    
  });
}
//
function closeSkipDiv(id) { 
  $("#"+id).hide(); $("#footerContent").removeClass("ft_skp_que");
  $("#skipQuesDiv").show();   	
  $("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
}
function chatbotIframeClose(type, podName){
  if(type == "clearing"){    
   // updateUserTypeSession('default',"clearing");
    if('chatbot' == podName){
      GANewAnalyticsEventsLogging('Chatbot - Clearing', 'Clearing', 'Yes');
      closeChatBox("/university-clearing/");
    }
  }
}   
function getQuizDetails(questionId, optionValue, optionText, currentQuesId, skipExitFlag, ansTxt){
  var filterString = "";
  var qualificationCode = "";
  var searchCategoryCode = "";		
  var previousQualGrade = "";
  var previousQualSel = "";	
  var jacsCode = "";
  var optionUrlTextForRemoveFilter = "";
  //this question is added newly for clearing journey - Jeyalakshmi.D 08/08/2019 rel
  if('39' == questionId && 'YES' == optionValue) {//answer for the new question -yes
    chatbotIframeClose('clearing', 'chatbot'); 
    return false;
  }
  else if('4' == questionId && 'NO' == optionValue) {//answer for the new question -No
    GANewAnalyticsEventsLogging('Chatbot - Clearing', 'Non-Clearing', 'Not Ready'); 
  }
  //
  else if(currentQuesId == "getQuestionId") { currentQuesId = $("#questionId").val(); }
  var proceed = true; var proceedTime = 0;			
  if((ansTxt == 'yesNo' && $$D('answer_'+currentQuesId) == null)  || ansTxt == 'removeFilter') {
    if(ansTxt == 'removeFilter') {
      optionUrlTextForRemoveFilter = optionText.split('##SPLIT##')[1];
      optionText = optionText.split('##SPLIT##')[0];
      optionText = "Remove " + optionText + " filter";
    }
    if(optionText != 'Add Qualification') {
      $("#mainSection").append('<div class="cht_usr" id="answer_'+currentQuesId+'">'+optionText+'</div>'); 		
    }
  }
  if(proceed) { $("#footerContent").hide(); $("#yesNoDiv").hide(); $("#skipQuesDiv").hide(); $("#skipAndViewResults").hide(); $("#footerContent").removeClass("ft_skp_que"); }			
  //	
  if(questionId == 'done' && proceed && $("#filterFlag").val() == 'location'){
    var location = $("input[name='location']:checked").val(); 
    var locDetails = location.split("|");	
    $("#region").val(locDetails[1]); $("#valueToBeShown").val(locDetails[1]);
    $("#regionFlag").val("N");
    $("#nextQuestionId").val(locDetails[2]);
    $("#question_"+currentQuesId+"_q").hide();
    $("#filterFlag").val("");     
  } else if(questionId == 'done' && proceed && $("#filterFlag").val() == 'locType'){
    var locationType = ""; var locType = "";
    $("input[name='locationType']:checked").each(function() {
       locationType += this.value.split('|')[0] + ", "; 
       locType += this.value.split('|')[1] + ",";
    });
    locationType = locationType.substr(0,locationType.length-2);
    locType = locType.substr(0,locType.length-1);
    //
    if($("#filterName").val().toLowerCase() == 'your preference') {
      $("#reviewCategory").val(locType);
      $("#reviewCategoryName").val(locationType);
      $("#filterFlag").val("");      
    } else {	
      locType = ""; $("input[name='locationType']:checked").each(function() { locType += this.value.split('|')[0] + ","; });
      locType = locType.substr(0,locType.length-1);
      $("#locationType").val(locType);
      $("#locationTypeName").val(locationType);  				
    }
    $("#question_"+currentQuesId+"_q").hide();	$("#valueToBeShown").val(locationType); 
  } else if(questionId == 'done' && proceed && $("#filterFlag").val() == 'single_select'){
    var val = $("input[name='radioOpt']:checked").val(); 
    var splitVal = val.split("|");
    if($("#filterName").val().toLowerCase() == "assesment type") {
      $("#assessmentType").val(splitVal[0]);
      $("#assessmentTypeId").val(splitVal[1]); 
    } else if($("#filterName").val().toLowerCase() == "previous qual"){ 
      $("#previousQual").val(splitVal[1]); optionValue = splitVal[1]; $("#filterFlag").val("");
      $("#previousQualSelected").val(splitVal[0]); 
    } else if($("#filterName").val().toLowerCase() == "study mode"){ 
      $("#studyMode").val(splitVal[1]); $("#filterFlag").val("");
      $("#studyModeTextName").val(splitVal[0]);       
    } else if($("#filterName").val().toLowerCase() == "subject") {
      $("#searchCategoryCode").val(splitVal[2]);
      $("#searchCategoryId").val(splitVal[1]);
      $("#keywordSearch").val(splitVal[0]);	
    } else if($("#filterName").val().toLowerCase() == "jacs subject") {					
      $("#jacsCode").val(splitVal[1]);
      $("#keywordSearch").val(splitVal[0]);	
    } else if($("#filterName").val().toLowerCase() == "subject qual") { 
      if(splitVal[1] == "0") { $("#nextQuestionId").val(splitVal[2]); }
      $("#subjectQualName").val(splitVal[0]);
      $("#previousQualSelected").val(splitVal[0]);
      $("#subjectQualKeyVal").val(splitVal[1]);
      $("#previousQual").val(splitVal[1]);
      optionValue = splitVal[1];
    } else {
      $("#qualification").val(splitVal[1]);
      $("#qualificationName").val(splitVal[0]);
    }
    $("#valueToBeShown").val(splitVal[0]); 				
    $("#question_"+currentQuesId+"_q").hide(); 
  }	
  var filterNameTemp = $("#filterName").val().toLowerCase();
  if(filterNameTemp == "subject1" || filterNameTemp == "subject2" || filterNameTemp == "subject3" || filterNameTemp == "subject4" || filterNameTemp == "subject5" || filterNameTemp == "subject6"){
    optionValue = $("#subjectQualKeyVal").val();
  } 
  if(questionId == 'done' && proceed && !isBlankOrNull($("#gradeCount")) && $("#filterName").val() == 'Grade') {
    proceed = validateSelectedGrades("quiz",'','','');				
  }
  //newly added
  if(questionId == '12' && optionText == 'Save') {
	  var qualSubListVal = createJSON();
	  if(qualSubListVal.length > 0){
	    var stringJson =	{
	      "qualSubList": createJSON()
	    };
	    var jsonData = JSON.stringify(stringJson);
	    GANewAnalyticsEventsLogging('Qualification Filter', 'Start', 'Chatbot');
	 }
  }
  if(questionId == 'done' && proceed) {  $("#gradeSaveDiv").hide();
    if($("#filterFlag").val() == 'single_select' && $("#filterName").val().toLowerCase() == "jacs subject") { 
    setTimeout(function(){ $("#mainSection").append('<div class="cht_usr" id="answer_'+$("#questionId").val()+'">'+$("#valueToBeShown").val()+'</div>'); }, 500);	
    subSaveSubject($("#questionId").val(), $("#valueToBeShown").val(), 'jacsSubject'); proceed = false; }
    else { $("#mainSection").append('<div class="cht_usr" id="answer_'+currentQuesId+'">'+$("#valueToBeShown").val()+'</div>'); }
  }
  if(questionId == 'done' && proceed && $("#filterFlag").val() == 'locType'){	
    var loadImg = Constants.loadImgConst;
    $("#mainSection").append('<div class="cht_bot"><div class="cht_ico"></div><div id="searchLoad'+currentQuesId+'" class="cht_msg cht_min">'+ loadImg + '</div></div>');							
    proceedTime = 2000;
    setTimeout(function(){ var comment = ""; var length = 0;
      $("input[name='locationType']:checked").each(function() {					   
         comment += this.value.split('|')[2]; length++;
      });
      if(length == 1) { $("#searchLoad"+currentQuesId).html(comment); } else {
        $("#searchLoad"+currentQuesId).html($("#commentForLocType").val());
      } 			
    }, 2000); $("#filterFlag").val(""); 
  }	
  if(questionId == 'done' && proceed && !isBlankOrNull($("#gradeCount")) && $("#filterName").val() == 'Grade' && parseInt($("#gradeCount").val()) > 2) {				
    proceedTime = 2000;
    var loadImg = Constants.loadImgConst;				
    $("#mainSection").append('<div class="cht_bot"><div class="cht_ico"></div><div id="searchLoad'+currentQuesId+'" class="cht_msg cht_min">'+ loadImg +'</div></div>');				
    setTimeout(function(){  $("#searchLoad"+currentQuesId).html($("#answerOpt_0").html()); }, 2000);					
  }  
  
  // GTM event tracking 
  var label = $("#answer_"+currentQuesId).html();
  var currentQuestionName = $("#questionName").val(); 
  if(currentQuesId == '11') {
    //
    var gradeAnswer = $("#answer_"+currentQuesId).html();    
    var gradeAnswerArr = (gradeAnswer.trim()).split(' ');    
    var gradeResult = "";
    //
    for (var i = 0; i < gradeAnswerArr.length; i++) {      
      var gradeLengthGTM = gradeAnswerArr[i].length;
      var gradeValueGTM = gradeAnswerArr[i].charAt(0);
      if(gradeAnswerArr[i].indexOf('A*') > -1) {
        gradeLengthGTM = gradeAnswerArr[i].length/2;
        gradeValueGTM = 'A*';
      }
      gradeResult += gradeLengthGTM + gradeValueGTM;
      if(gradeAnswerArr.length > 1 && i != gradeAnswerArr.length-1) {
        gradeResult += ',';
      }
    }
    //   
    label = $("#answer_"+(currentQuesId-1)).html() + ' | ' + $("#pqSelectedGrades").val().replace(/-/g,",");
  }
  //if(optionValue == 'skpExt' || $("#nextQuestionId").val() == "12" || $("#filterName").val() == 'Grade' || ($("#nextQuestionId").val() == "10" && !isBlankOrNull($("#previousQual").val()))) {
    qualificationCode = !isBlankOrNull($("#qualification").val()) ? $("#qualification").val() : "";				
    searchCategoryCode = !isBlankOrNull($("#searchCategoryCode").val()) ? $("#searchCategoryCode").val() : "";				
    previousQualGrade = !isBlankOrNull($("#pqSelectedGrades").val()) ? $("#pqSelectedGrades").val() : "";
    previousQualSel = !isBlankOrNull($("#previousQual").val()) ? $("#previousQual").val() : "";	
    jacsCode = !isBlankOrNull($("#jacsCode").val()) ? $("#jacsCode").val() : ""; 
    if(!isBlankOrNull(jacsCode)) { searchCategoryCode = ""; }
  //}
  if(optionValue == 'skpNxt' || optionValue == 'skpExt') {
    label = optionValue == 'skpNxt' ? 'Skip' : 'Skip to end';				
    optionValue = "";			    
  }			
  if(!isBlankOrNull(currentQuesId)) {  
    if(currentQuestionName == 'Quiz Type') {
      label = ((label == 'Yes I know!') || (label == 'Yes I know')) ? 'standard' : 'discovery';  
      if(label == 'standard') { Constants.dimensionLog11 = "Standard"; }
    } else if(currentQuestionName == 'Discovery Type') {
      label = ((label == 'Yes I know!') || (label == 'Yes I know')) ? 'I want to be' : 'What should i do';         
      Constants.dimensionLog11 = label;
    } else if(currentQuestionName == 'Priorities') {				
      for(i=0; i < 3; i++) {
        if(label.indexOf(',') > -1) {
          label = label.replace(',', ' |');
          continue;
        } else {
          break;
        }
      }
    }
    if(!isBlankOrNull(Constants.dimensionLog11)){
      ga('set', 'dimension11', Constants.dimensionLog11);
    }
    if(((currentQuestionName == 'Suggested Subject' || currentQuestionName == 'Jacs subject') && proceed == false) || (currentQuestionName == 'Grades' && currentQuesId != '11')) {
      // do nothing
    } else {
      if(!isBlankOrNull(label)) { label = label.toLowerCase();}
      GANewAnalyticsEventsLogging(Constants.gtmQuizCategory , currentQuestionName , decodeUsingTextArea(label));
      $$D('ga_Log_on_force_exit').value = currentQuestionName;
    }
  }				
  //					  			
  if(quizproceedTime!=null) {
    clearTimeout(quizproceedTime);
  }
 
  quizproceedTime = setTimeout(function() {
    var subject1_id, subject2_id, subject3_id, subject4_id, subject5_id, subject6_id, qualTypeId; 
    var subj1_tariff_points, subj2_tariff_points, subj3_tariff_points, subj4_tariff_points, subj5_tariff_points, subj6_tariff_points;    	
    //      
    if(proceed) {	  
      var filterGradeTemp = $("#filterName").val().toLowerCase();
          if((filterGradeTemp == "grade1" || filterGradeTemp == "grade2" || filterGradeTemp == "grade3" || filterGradeTemp == "grade4" || filterGradeTemp == "grade5" || filterGradeTemp == "grade6") && optionText.toLowerCase() == "done"){
            subject1_id = !isBlankOrNull($("#subject1").val()) ? $("#subject1").val() : "";
            subject2_id = !isBlankOrNull($("#subject2").val()) ? $("#subject2").val() : "";
            subject3_id = !isBlankOrNull($("#subject3").val()) ? $("#subject3").val() : ""; 
            subject4_id = !isBlankOrNull($("#subject4").val()) ? $("#subject4").val() : "";
            subject5_id = !isBlankOrNull($("#subject5").val()) ? $("#subject5").val() : "";
            subject6_id = !isBlankOrNull($("#subject6").val()) ? $("#subject6").val() : "";
            subj1_tariff_points = !isBlankOrNull($("#grade1").val()) ? $("#grade1").val() : "";
            subj2_tariff_points = !isBlankOrNull($("#grade2").val()) ? $("#grade2").val() : "";
            subj3_tariff_points = !isBlankOrNull($("#grade3").val()) ? $("#grade3").val() : "";
            subj4_tariff_points = !isBlankOrNull($("#grade4").val()) ? $("#grade4").val() : "";
            subj5_tariff_points = !isBlankOrNull($("#grade5").val()) ? $("#grade5").val() : "";
            subj6_tariff_points = !isBlankOrNull($("#grade6").val()) ? $("#grade6").val() : "";	
            $("#pqSelectedGrades").val($("#tempQualGrades").val());
          }
          //
          var searchFilterURL = formSearchURL('').split('search?')[1];
          searchFilterURL = returnRemovedFilterURL(searchFilterURL, optionUrlTextForRemoveFilter, true);
          $("#selectedFilterUrl").val(encodeURIComponent(searchFilterURL));
          //
          qualTypeId = $("#subjectQualKeyVal").val();
          $("#question_"+currentQuesId+"_q").remove(); 
          if(questionId == 'done') { questionId = $("#nextQuestionId").val(); }
          questionId = !isBlankOrNull(questionId) ? questionId : "1";
          optionValue = !isBlankOrNull(optionValue) ? optionValue : "";
          //
		  //Added condition to fix bug by Sangeeth.S for 20_Nov_18 rel 
		  if(!isBlankOrNull(optionUrlTextForRemoveFilter)){
			 var removeFilter = optionUrlTextForRemoveFilter.toLowerCase()
       if(removeFilter == 'b9c' || removeFilter == 'bd' || removeFilter == 'bed' || removeFilter == 'sqa-adv' || removeFilter == 'sqa-higher' || removeFilter == 'a-level' || removeFilter == 'btec'){
         previousQualGrade = "";
         previousQualSel = "";
         qualTypeId  = "";			 
         $("#previousQual").val(""); 
         $("#pqSelectedGrades").val(""); 
         $("#previousQualSelected").val("");
       }
		  }			  
		  //		  
          filterString += !isBlankOrNull(questionId) ? "&questionId=" + questionId : "";
          filterString += !isBlankOrNull(optionValue) ? "&answer=" + optionValue : "";
          filterString += !isBlankOrNull(qualificationCode) ? "&qualificationCode=" + qualificationCode : "";
          filterString += !isBlankOrNull(searchCategoryCode) ? "&categoryCode=" + searchCategoryCode : "";
          filterString += !isBlankOrNull(previousQualGrade) ? "&previousQualGrades=" + previousQualGrade : ""; 
          filterString += !isBlankOrNull(previousQualSel) ? "&previousQual=" + previousQualSel : "";
          filterString += !isBlankOrNull(qualTypeId) ? "&qualTypeId=" + qualTypeId : "";
          filterString += !isBlankOrNull(subject1_id) ? "&subject1_id=" + subject1_id : "";
          filterString += !isBlankOrNull(subject2_id) ? "&subject2_id=" + subject2_id : "";		  
		      filterString += !isBlankOrNull(subject3_id) ? "&subject3_id=" + subject3_id : "";
          filterString += !isBlankOrNull(subject4_id) ? "&subject4_id=" + subject4_id : "";
          filterString += !isBlankOrNull(subject5_id) ? "&subject5_id=" + subject5_id : ""; 
          filterString += !isBlankOrNull(subject6_id) ? "&subject6_id=" + subject6_id : "";
          filterString += !isBlankOrNull(subj1_tariff_points) ? "&subj1_tariff_points=" + subj1_tariff_points : "";
          filterString += !isBlankOrNull(subj2_tariff_points) ? "&subj2_tariff_points=" + subj2_tariff_points : "";
          filterString += !isBlankOrNull(subj3_tariff_points) ? "&subj3_tariff_points=" + subj3_tariff_points : "";
		      filterString += !isBlankOrNull(subj4_tariff_points) ? "&subj4_tariff_points=" + subj4_tariff_points : "";
          filterString += !isBlankOrNull(subj5_tariff_points) ? "&subj5_tariff_points=" + subj5_tariff_points : "";
          filterString += !isBlankOrNull(subj6_tariff_points) ? "&subj6_tariff_points=" + subj6_tariff_points : "";
		      filterString += !isBlankOrNull(jacsCode) ? "&jacsCode=" + jacsCode : "";
          filterString += !isBlankOrNull(Constants.dimensionLog11) ? "&dimension11=" + Constants.dimensionLog11 : "";
          filterString += !isBlankOrNull($("#keywordSearch").val()) ? "&keywordSearchSubject=" + $("#keywordSearch").val().toLowerCase().replace(/[^A-Z0-9]+/ig, "-") : "";          
          filterString += !isBlankOrNull($("#selectedFilterUrl").val()) ? "&selectedFilterUrl=" + $("#selectedFilterUrl").val() : "";
          tempId = "";
//      var quiz = new sack();
//      quiz.requestFile = contextPath + "/chatbot-widget.html?pageName=quizResults" + filterString;	
//      quiz.onCompletion = function(){displayQuizDetails(quiz);};	
//      quiz.runAJAX();
      var url = contextPath + "/chatbot-widget.html?pageName=quizResults" + filterString;
      $.ajax({
        url: url, 
        headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
        dataType: 'json',
        type: "POST",
        data: jsonData,
        async: false,
        success: function(response){
        },
        complete: function(response){
          displayQuizDetails(response);
        }
      });
    }
  }, proceedTime);
}
//
function checkEmpty(value) {
    return value != "";
}
//
function returnRemovedFilterURL(searchFilterURL, filterUrlText, flagToFormURL) {
  if(!isBlankOrNull(searchFilterURL) && !isBlankOrNull(filterUrlText)) {
    searchFilterURL = decodeURIComponent(searchFilterURL);
    searchFilterURL = $('<textarea/>').html(searchFilterURL).text();
    var urlArray = searchFilterURL.split('&');
    for(var i=0; i < urlArray.length; i++) {
      if(urlArray[i].indexOf(filterUrlText) > -1) {
        var paramName = urlArray[i].split('=')[0];
        var paramValue = urlArray[i].split('=')[1];
        if(flagToFormURL) {
          var paramValueToBeReplacedinURL = paramValue;
          paramValue = paramValue.replace(filterUrlText, '');
          if(paramValue != "") {
            var array = paramValue.split(',');
            array = array.filter(checkEmpty);
            paramValue = array.join(",").toString();
            searchFilterURL = searchFilterURL.replace(paramValueToBeReplacedinURL, paramValue);
            $('.'+paramName).val(paramValue);
          } else {
            searchFilterURL = searchFilterURL.replace(urlArray[i], '');
            $('.'+paramName).val('');			
          }
          if(paramName == 'entry-level') {
            $('.entry-points').val('');
            if((urlArray[i+1])&&urlArray[i+1].indexOf('entry-points') > -1){
               searchFilterURL = searchFilterURL.replace(urlArray[i+1], '');	
            }
          }
          searchFilterURL = searchFilterURL.indexOf('&&') > -1 ? searchFilterURL.replace(/&&/g, '&') : searchFilterURL;         
        } else {
          $("."+paramName).val(paramValue);
          return;
        }
      }
    }
  }
  return searchFilterURL;
}
//
function doGAlogOnExit() {
  GANewAnalyticsEventsLogging('Quiz', 'Minimise Chat', $$D('ga_Log_on_force_exit').value);
}
//
function displayQuizDetails(quiz){
  if(quiz.responseText!=""){   
    var timeout = 2000;
    var quizResp = quiz.responseText.split("###MOBILESKIP###");
    var mobileSkip = quizResp[1].split("###SELECTEDURL###")[0];
    var responseForQuiz = quizResp[0].split("###PREVIOUSQUAL###");
    var previousQualResponse = responseForQuiz[1];
    var responseTemp = responseForQuiz[0].split("###ANSWEROPTION###");
    $("#answerOptionHidden").html(responseTemp[1]);
    var responseText = responseTemp[0].split("###QUESTION###");
    var selectedUrl = quiz.responseText.split("###SELECTEDURL###")[1];
    //
    var questionId = trimString(responseText[1]); $("#questionId").val(questionId);
    var answerType = trimString(responseText[2]); 
    var filterName = trimString(responseText[3]); $("#filterName").val(filterName);
    if("location" == filterName.toLowerCase()) { $("#filterFlag").val(filterName.toLowerCase()); }
    var answerDisplayStyle = trimString(responseText[4]); 
    var ctaButtonText = trimString(responseText[5]); 
    $("#nextQuestionId").val(trimString(responseText[6]));
    var quizQuestionLength = trimString(responseText[7]); $("#questionLength").val(trimString(responseText[7]));
    $("#percentProgressBar").css("width", trimString(responseText[8]) +"%");
    $("#courseCount").val(trimString(responseText[9])); var courseCount = $("#courseCount").val();
    if(!isBlankOrNull(courseCount) && courseCount == "0"){
      //$("#qualificationName").val("Undergraduate");
      //$("#qualification").val("M");	
      //$("#previousQual").val(""); 
      //$("#pqSelectedGrades").val(""); 
      //$("#previousQualSelected").val(""); 				
    }
    if(questionId == '12') {
      returnRemovedFilterURL(selectedUrl, 'entry-level', false);
      returnRemovedFilterURL(selectedUrl, 'entry-points', false);
    }
    $("#questionName").val(trimString(responseText[10]));
    $("#keywordSearch").val(trimString(responseText[11]));
    var pQualification = "";
    var gradeStr = "";
    var gradeLevel = "";
    if(responseText.length == 13) {
       $("#multipleNextQuestionId").val(trimString(responseText[12]));
    }else if(responseText.length > 13) {
      pQualification = trimString(responseText[12]);
      gradeStr = trimString(responseText[13]);
      gradeLevel = trimString(responseText[14]);
    }
    //
    var quizQuestions = responseText[0].split("###SEPARATOR###");
    if(parseInt(quizQuestionLength) != 0){
    	setTimeout(function(){ $("#mainSection").append(quizQuestions[0]);}, 1000);
     if(responseText[2] == "DROPDOWN") { $("#mainSection").append(quizQuestions[1]);}
    }
    var index = 0;
    
    quizResponseDivResults(index, quizQuestionLength, questionId, answerType, quizQuestions, filterName, pQualification, gradeStr, gradeLevel, ctaButtonText, answerDisplayStyle, timeout, previousQualResponse, mobileSkip);   
  }
}
function quizResponseDivResults(index, quizQuestionLength, questionId, answerType, quizQuestions, filterName, pQualification, gradeStr, gradeLevel, ctaButtonText, answerDisplayStyle, timeout, previousQualResponse, mobileSkip){      
    quizbox = setTimeout(function () { 
      if(index<parseInt(quizQuestionLength)){                 
        if(index == 0) {
          $("#image_"+questionId+index).hide();	
					$("#questionTxt_"+questionId+index).show();
					$("#question_"+questionId+(index+1)).show(); 
					if(index == quizQuestions.length-2 && answerType == 'AJAX') { 
						$("#searchsubject_"+questionId+index).show();
						setTimeout(function(){ $("#searchsubject_"+questionId+index).focus();}, 100);				
					} 
          if((index+1) < parseInt(quizQuestionLength)) { $("#mainSection").append(quizQuestions[index+1]); }
        } else {          
          //          
          $("#image_"+questionId+index).hide();	
          if((index == quizQuestions.length-2) && (questionId == 2)) { 
            $("#chtMsg_"+questionId+index).addClass('quz_hm'); 
          }							          
          if($$D("mobileFlag") && $$D("mobileFlag").value == "true" && questionId == 2 && index == quizQuestions.length-2) {
            var tempHTML = $("#questionTxt_"+questionId+index).html();
            tempHTML = tempHTML.replace("quiz_hme", "quiz_hme_mob");
            $("#questionTxt_"+questionId+index).html(tempHTML);
          }
          //
          $("#questionTxt_"+questionId+index).show(); 
          if(filterName == "Subject grade") {							
            setTimeout(function(){ $("#gradeSubject").focus(); }, 100);
          }
          $("#question_"+questionId+(index+1)).show();
          if(index == quizQuestions.length-2 && answerType == 'AJAX') { $("#searchsubject_"+questionId+index).show();
            setTimeout(function(){ $("#searchsubject_"+questionId+index).focus();}, 100);							           												
          }					
          //         						 
          if(answerDisplayStyle == 'GRADE_BOX') {
            onChageQualGradesQuiz(pQualification, gradeStr, gradeLevel);										
          }
          if((index+1) < parseInt(quizQuestionLength)) { $("#mainSection").append(quizQuestions[index+1]); }  
          //
          $("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000); 
				}         
        //
        if(index == parseInt(quizQuestionLength)-1) {
          var tempDivLength = quizQuestions.length - parseInt(quizQuestionLength); 
          if(tempDivLength == 2) {
            $("#mainSection").append(quizQuestions[index+1]);
            $("#footerContent").html(quizQuestions[index+2]); $("#footerContent").append(mobileSkip);
            if(!isBlankOrNull(trimString($("#footerContent").html())) && answerType != 'GRADE_DROPDOWN') { $("#footerContent").show(); }
          } else {
            $("#footerContent").html(trimString(quizQuestions[index+1]));  $("#footerContent").append(mobileSkip);
            if(!isBlankOrNull(trimString($("#footerContent").html())) && answerType != 'GRADE_DROPDOWN') { $("#footerContent").show(); }
          }              
          if(answerDisplayStyle == 'GRADE_BOX') {
            $("#question_"+questionId+"_q").show(); $("#gradeSaveDiv").show();
          } else {
            $("#question_"+questionId+"_q").show();
          }							         							
          $("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);          
          if(answerType == 'GRADE_DROPDOWN'){
             $("#mainSection").append(previousQualResponse);
          }
          if(answerDisplayStyle == 'BUTTONS' && answerType != 'GRADE_DROPDOWN'){ 
            $("#yesNoDiv").show(); 							
          } else if(answerDisplayStyle == 'FULL_BUTTON') { 
            $("#fullButton").show(); 							
          } else if(answerDisplayStyle == 'ADD_QUAL_BUTTONS') {
        	$("#addQualButton").show();
          } 
          else if(ctaButtonText == 'Done') {																				 
            $("#selectAns").addClass("cbbtn_dis");
            if(filterName == 'Location') { $("#selectAns").removeClass("cbbtn_dis"); }							
            if(answerType != 'GRADE_DROPDOWN') { $("#skipQuesDiv").show(); }	
            //            
            $("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);             
          }          					 	      
        }
        index++;
        quizResponseDivResults(index, quizQuestionLength, questionId, answerType, quizQuestions, filterName, pQualification, gradeStr, gradeLevel, ctaButtonText, answerDisplayStyle, timeout, previousQualResponse, mobileSkip);   			
      }     
      if(parseInt(quizQuestionLength) == 0 && answerType == 'GRADE_DROPDOWN'){
        $("#mainSection").append(previousQualResponse); $("#footerContent").html(quizQuestions[0]);
        $("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
      }
    }, timeout); 
}

function getCourseList(searchId) {			
  if(quizbox!=null) {
    clearTimeout(quizbox);
  }			
  var id = searchId.split("_")[1];
  if($$D(searchId)){
    var subject = $$D(searchId).value;
    var qualificationCode = $$D('qualification').value;
    if(subject.length > 2){	
      var ajaxUrl = "";      
     var filterNameTemp = $("#filterName").val().toLowerCase();
     if(filterNameTemp == "subject1" || filterNameTemp == "subject2" || filterNameTemp == "subject3" || filterNameTemp == "subject4" || filterNameTemp == "subject5" || filterNameTemp == "subject6"){						
		   ajaxUrl = "/chatbot-widget.html?pageName=qual-subject-ajax&subjectText=" + subject + "&qualTypeId=" + $("#subjectQualKeyVal").val();										
     } else {						            
       ajaxUrl = "/chatbot-widget.html?pageName=course-list-ajax&keywordText=" + subject + "&qualCode=" + qualificationCode;														        								
     }
      var course = new sack();
      course.requestFile = contextPath + ajaxUrl;	
      course.onCompletion = function(){displayCourseList(course, searchId);};	
      course.runAJAX();      																														 
    } else {							
      $("#searchResultList").hide();
    }
  } 						
}		
    
function displayCourseList(course, searchId){					
  if(course.response != null && $$D(searchId).value.length > 2) {
    $("#searchResultList").html(course.response); 
    $('#searchResultList').show();	   
    if($$D(searchId).style.display == "none") { $('#searchResultList').hide(); }
  }else {	    						
    $("#searchResultList").hide();
  }	      	
}

function getJobOrIndustryList(searchId) {			
  if(quizbox!=null) {
    clearTimeout(quizbox);
  }			
  var id = searchId.split("_")[1];
  if($$D(searchId)){
    var jobIndustry = $$D(searchId).value;
    if(jobIndustry.length > 2){	    
      ajaxUrl = "/chatbot-widget.html?pageName=job-industry-list-ajax&jobOrIndustry=" + jobIndustry;										
      var jobList = new sack();
      jobList.requestFile = contextPath + ajaxUrl;	
      jobList.onCompletion = function(){displayJobOrIndustryList(jobList, searchId);};	
      jobList.runAJAX(); 																																																			 
    } else {							
       $("#jobOrIndustryResultList").hide();
    }
  } 						
}		
    
function displayJobOrIndustryList(jobList, searchId){
	if(jobList.response != null && $$D(searchId).value.length > 2) {
    $("#jobOrIndustryResultList").html(jobList.response); 
    $('#jobOrIndustryResultList').show();	    
    if($$D(searchId).style.display == "none") { $('#jobOrIndustryResultList').hide(); }
  }else {														
    $("#jobOrIndustryResultList").hide();
  }						
}

function checkYourResults(nextQuestionId) {	
  var quizDimension10 = "";
  if(nextQuestionId == '0') { 	    
    ga('set', 'dimension10', "No"); setDimension10("No");
    var searchURL = $$D("refererUrl") ? $$D("refererUrl").value : "";
    closeChatBox(searchURL);
  } else {
    setDimension10("Complete");
    
    var searchURL = formSearchURL('consider-count');
    /*
    if(!isBlankOrNull(region) || !isBlankOrNull(locationTypeName) || !isBlankOrNull(studyMode) || !isBlankOrNull(assessmentType) || !isBlankOrNull(reviewCategory) || !isBlankOrNull(previousQualGrade) || !isBlankOrNull(previousQual)){
      searchURL += "&sort=bm";
    }
    */
    searchURL += (!isBlankOrNull($("#courseCount").val()) && $("#courseCount").val() == "0") ? "&rf=n" : "";
    searchURL = searchURL.toLowerCase();
    $$D("loadingImg").style.display = "block";
    GANewAnalyticsEventsLogging(Constants.gtmQuizCategory , "Show Results" , "show results");
    ga('set', 'dimension10', "Complete");  
    closeChatBox(searchURL);       
  }
}
//
function formSearchURL(considerCount) {
    //
    var subjectName = !isBlankOrNull($("#keywordSearch").val()) ? $("#keywordSearch").val().toLowerCase().replace(/[^A-Z0-9]+/ig, "-") : "";
    var qualificationCode = !isBlankOrNull($("#qualification").val()) ? $("#qualification").val() : "";
    var region = !isBlankOrNull($("#region").val()) ? $("#region").val().toLowerCase().replace(/ /g,"-") : "";	
    region = (region == "all-uk") ? "united-kingdom" : region;
    var locationTypeName = !isBlankOrNull($("#locationType").val()) ? $("#locationType").val().toLowerCase().replace(/ /g,'-') : "";      
    var studyMode = !isBlankOrNull($("#studyMode").val()) ? $("#studyMode").val() : "";     
    var assessmentType = !isBlankOrNull($("#assessmentType").val()) ? $("#assessmentType").val().toLowerCase().replace(/ /g,"-") : "";
    var reviewCategory = !isBlankOrNull($("#reviewCategory").val()) ? $("#reviewCategory").val().toLowerCase().replace(/_/g,"-") : "";
    var previousQualGrade = !isBlankOrNull($("#pqSelectedGrades").val()) ? $("#pqSelectedGrades").val() : ""; 
    var previousQual = !isBlankOrNull($("#previousQual").val()) ? $("#previousQual").val().toLowerCase().replace(/_/g,"-") : "";   
    var jacsCode = !isBlankOrNull($("#jacsCode").val()) ? $("#jacsCode").val() : "";     
    //	
    var qualPath = "degree-courses";
    if("A" == qualificationCode){
      qualPath = "foundation-degree-courses";
    } else if("N" == qualificationCode){
      qualPath = "hnd-hnc-courses";
    }else if("T" == qualificationCode){
      qualPath = "access-foundation-courses";
    }    
    var searchURL = wu_scheme+document.domain + "/" + qualPath + "/search?" ;
    searchURL += !isBlankOrNull(jacsCode) ? "jacs=" + jacsCode : ""; 
    searchURL += (!isBlankOrNull(subjectName) && isBlankOrNull(jacsCode)) ? "subject=" + subjectName : "";
    var flag = true;
    if(considerCount == 'consider-count') {
      if(($("#courseCount").val() == "0")) {
        flag = false;
      }
    }
    if(flag) {
      searchURL += !isBlankOrNull(region) && (region != "united-kingdom") ? "&location=" + region : "";
      searchURL += !isBlankOrNull(locationTypeName) ? "&location-type=" + locationTypeName : "";
      searchURL += !isBlankOrNull(studyMode) ? "&study-mode=" + studyMode : "";
      searchURL += !isBlankOrNull(assessmentType) ? "&assessment-type=" + assessmentType : "";
      searchURL += !isBlankOrNull(reviewCategory) ? "&your-pref=" + reviewCategory : "";
      var entryLevelQueryString = !isBlankOrNull(previousQual) ? "&entry-level=" + previousQual : "";
      if(considerCount == 'consider-count' && previousQual.trim() == "0") {
        entryLevelQueryString = "";
      }
      searchURL += entryLevelQueryString;
      searchURL += !isBlankOrNull(previousQualGrade) ? "&entry-points=" + previousQualGrade : ""; 
    }
    //
    return searchURL;
}
//
function setDimension10(label) {			 
  var ajaxUrl = "/chatbot-widget.html?pageName=set-dimension-ajax&labelText=" + label;														        								
  var course = new sack();
  course.requestFile = contextPath + ajaxUrl;	
  course.onCompletion = function(){ };	
  course.runAJAX();      																														 						
}		
//
function saveSubject(divId) {
	var index = divId.split("_")[1];
	var value = $("#course_list_"+index).val();
	var courseDetails = value.split("|");
	var subjectName = courseDetails[0];
	var questionId = $("#questionId").val();	
	var nextQuestionId = $("#nextQuestionId").val();
	$(".subjectInpAjx").hide();
	$("#searchCategoryCode").val(courseDetails[2]);
	$("#searchCategoryId").val(courseDetails[1]);
	$("#keywordSearch").val(subjectName);												
	$("#searchsubject").hide();	
	$('#searchResultList').remove();
	if(!isBlankOrNull($("#filterName").val())) { 
		var subDiv = $("#filterName").val().toLowerCase();
		$("#"+subDiv).val(courseDetails[1]); 
	}
	setTimeout(function(){ $("#mainSection").append('<div class="cht_usr" id="answer_'+questionId+'">'+subjectName+'</div>'); subSaveSubject(questionId, subjectName);}, 500);
}
//
function subSaveSubject(questionId, subjectName, jacsSubject){
	if(!isBlankOrNull($("#answerOpt_0").html())) {
	setTimeout(function(){ 
    var loadImg = Constants.loadImgConst;							
		$("#mainSection").append('<div class="cht_bot"><div class="cht_ico"></div><div id="searchLoad'+questionId+'" class="cht_msg cht_min">'+ loadImg +'</div></div>');
		$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
	}, 500);
	var timeout = 2000; 
	setTimeout(function(){ 
		$("#searchLoad"+questionId).html('<span">'+subjectName+'</span>'+$("#answerOpt_0").html());
		if(jacsSubject != "jacsSubject") {
		for(var i=1; i<parseInt($("#answerOptLength").val()); i++) {
			timeout += 1000; 
			$("#mainSection").append('<div class="cht_bot cht_snd"><div class="cht_msg cht_min">'+$("#answerOpt_"+i).html()+'</div></div>');
			$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
		} }
	}, timeout); }	
	setTimeout(function(){  getQuizDetails($("#nextQuestionId").val(), '', '', questionId); }, timeout);
}
//
function saveJobOrIndustry(divId) {	
	var index = divId.split("_")[1];
	var value = $("#job_list_"+index).val();
	var jobOrIndustryDetails = value.split("|");
	var jobOrIndustryName = jobOrIndustryDetails[0];
	var questionId = $("#questionId").val();	
	var nextQuestionId = $("#nextQuestionId").val();
	if(jobOrIndustryDetails[1] == "0") { nextQuestionId = $("#multipleNextQuestionId").val(); }
	$(".subjectInpAjx").hide();															
	$('#jobOrIndustryResultList').remove();
	$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
	$("#jobOrIndustryName").val(jobOrIndustryName);	
	setTimeout(function(){ $("#mainSection").append('<div class="cht_usr" id="answer_'+questionId+'">'+jobOrIndustryName+'</div>'); getQuizDetails(nextQuestionId, jobOrIndustryDetails[1]+','+jobOrIndustryDetails[2] , '', questionId); }, 500);
	//	
}
//
function selectQualification(divId, flag, regionVal){
	$("#selectAns").removeClass("cbbtn_dis");
  if(flag == "locType"){
    setTimeout(function(){ var locType = $("input[name='locationType']:checked").length;	
			if(locType == 0) { $("#selectAns").addClass("cbbtn_dis");  } }, 5);
  }
	if(flag == "locType" && $("#filterName").val().toLowerCase() == 'your preference') {						
		setTimeout(function(){ var typeLength = $("input[name='locationType']:checked").length;
		if(typeLength > 3) { $('#'+divId).find('input').prop("checked", false); }	
		if(typeLength == 0) { $("#selectAns").addClass("cbbtn_dis");  }		}, 5);
		$("#filterFlag").val('locType'); 
	} else if(tempId != divId) { tempId = divId;
		var qualDetails = "";
		if(flag == "location") { 
			$("#filterFlag").val('location');			
		} else if(flag == "locType") {
			$("#filterFlag").val('locType'); 
			setTimeout(function(){ var typeLength = $("input[name='locationType']:checked").length;	
			if(typeLength == 0) { $("#selectAns").addClass("cbbtn_dis");  } }, 5);
		} else if(flag == "region") {
			var sval = $("#"+divId).html();
			$("#region").val(regionVal);  
			if(regionVal == "ENGLAND"){ $("#regionFlag").val("N"); } else { $("#regionFlag").val("Y"); }
			$(".sub_region").hide();     
      $("#mainSection .cht_bot:last-child").remove();		
			if(!isBlankOrNull(sval)) { $("#mainSection").append('<div class="cht_usr" id="answer_'+$("#questionId").val()+'">'+sval+'</div>'); }			
			getQuizDetails($("#nextQuestionId").val(), '', '', $("#questionId").val()); 						
		} else if(divId == 'single_select'){
			$("#filterFlag").val('single_select');				 
		}
		if($("#filterName").val() == "Qualification") {
			var index = divId.split("_")[1];
			var value = $("#qualHidden_"+index).val();
			qualDetails = value.split("|");	
			$("#qualification").val(qualDetails[1]);
			$("#qualificationName").val(qualDetails[0]);
			$("#nextQuestionId").val(qualDetails[2]);
			$("#mainSection").append('<div class="cht_usr" id="answer_'+$("#questionId").val()+'">'+qualDetails[0]+'</div>');
			getQuizDetails($("#nextQuestionId").val(), '', '', $("#questionId").val());			
			$("#question_"+$("#questionId").val()+"_q").remove();
		}  				
	}
}
//
function selectSubjectGrade(divId, flag) {
	$("#selectAns").removeClass("cbbtn_dis");
	if(tempId != divId) { tempId = divId;
		if(flag == "grade") {
			var sval = $("#"+divId + " input").val();	
      if($$D("tempQualGrades1")) { $("#tempQualGrades").val($("#tempQualGrades1").val()); }
      $("#question_previous_qual").remove();
			if(!isBlankOrNull(sval)) { $("#mainSection").append('<div class="cht_usr" id="answer_'+$("#questionId").val()+'">'+sval+'</div>'); 
			  GANewAnalyticsEventsLogging(Constants.gtmQuizCategory, $("#questionName").val(), decodeUsingTextArea(sval));
			}						 								
			previousQualGradeCalculation(sval);
			$("#yesNoDiv").show(); $("#footerContent").show(); 			
			$("#quiz-cont").animate({ scrollTop: $('#quiz-cont')[0].scrollHeight }, 1000);
			if(!isBlankOrNull($("#filterName").val())) { 
				var subDiv = $("#filterName").val().toLowerCase();
				$("#"+subDiv).val(sval); 
			}
		}
	}
}
//
function onChageQualGradesQuiz(qualification, gradeStr, gradeLevel){
 if($$D("dynamicQualList")){
  $$D("dynamicQualList").innerHTML = ''; 
  var noOfGrades = gradeStr.split(',').length;  
  var qualGrades = gradeStr.split(',');	
  var grade = 0;
  $$D("numberOfGrades").value = noOfGrades;
  for(var z=0; z < noOfGrades; z++){                                    
   if(!isBlankOrNull(gradeStr)){	
	   var newFieldValue = '<li><h3 id="gradeTitle_'+z+'">' + qualGrades[z] + '</h3><span class="toggle"><span class="minus" onclick="increaseDecreaseGrades('+z+',\'decrease\', \'\', \'\');"><i class="fa fa-minus" aria-hidden="true"></i></span><span class="blu_box" id="gradeValue_' +z+ '">'+grade+'</span><span class="plus" onclick="increaseDecreaseGrades('+z+',\'increase\', \'\', \'\');"><i class="fa fa-plus" aria-hidden="true"></i></span></span>';
	   newFieldValue += '<input type="hidden" name="points_' +z+ '" id="points_' +z+ '" maxlength="3" value="'+gradeLevel+'"></li>';								  
	   $$D('dynamicQualList').innerHTML += newFieldValue;	
	   $('#previousQualSavBtn').addClass("cbbtn_dis"); 
   }
  }      
 }
}
//
function increaseDecreaseGrades(id, flag){
  var numberOfGrades = $$D("numberOfGrades").value;
  var maxVal = 0; var curVal = 0; var totalGrade = 0; var count = 0;
  if($$D('gradeValue_'+id) && $$D('points_'+id)){
	maxVal = $$D('points_'+id).value;
	curVal = $$D('gradeValue_'+id).innerHTML;
	if(curVal < maxVal && flag == 'increase') {  curVal++; }
	if(curVal > 0 && flag == 'decrease') { curVal--; } 
	$$D('gradeValue_'+id).innerHTML = curVal;
	for(var i=0; i<numberOfGrades; i++) {
	  if($$D("gradeValue_"+i) && $$D("gradeTitle_"+i)) { 
		totalGrade += parseInt($$D("gradeValue_"+i).innerHTML);  		
		var gradeVal = $('#gradeTitle_'+i).html(); 
		if(gradeVal.toUpperCase() == "A*") { count += parseInt($$D("gradeValue_"+i).innerHTML)+1; }	
		else if(gradeVal.toUpperCase() == "A") { count += parseInt($$D("gradeValue_"+i).innerHTML); }	
	  }
	}	
  if(parseInt(totalGrade) > 0) {  				
    $('#previousQualSavBtn').removeClass("cbbtn_dis"); 
	} else { $('#previousQualSavBtn').addClass("cbbtn_dis"); } 
	  $("#gradeCount").val(count); $("#gradeSaveDiv").show();
  }
}
//
function validateSelectedGrades(){
	var numberOfGrades = $$D("numberOfGrades").value;
  var selectedGrade = $("#previousQual").val(); 	
	var selectedGrades = ""; 
	var gradeTextVal = "";
	var selectedGradesDisplay = "";
	var totalGrade = 0;
	for(var i=0; i<numberOfGrades; i++) {
	  if($$D("gradeValue_"+i) && $$D("gradeTitle_"+i)) { 
      totalGrade += parseInt($$D("gradeValue_"+i).innerHTML);  
      selectedGrades += $$D("gradeValue_"+i).innerHTML + $$D("gradeTitle_"+i).innerHTML;	
      selectedGradesDisplay += " "; 
      for(var j=0; j< parseInt($$D("gradeValue_"+i).innerHTML); j++) {
        selectedGradesDisplay += $$D("gradeTitle_"+i).innerHTML;
      }
      if(i+1 != numberOfGrades) { selectedGrades += "-"; }
	  } 
	}	
	gradeTextVal = "<entry-grades>" + gradeTextVal + "</entry-grades>";
	var errorMsg = ""; var errorFlag = true;
	if(selectedGrade == "A_LEVEL" && totalGrade > 6 ) {
		errorFlag = false;
		errorMsg = Constants.pqAlevelErrMsg;
	} else if((selectedGrade == "SQA_HIGHER" || selectedGrade == "SQA_ADV") && totalGrade > 10) {
		errorFlag = false;
		errorMsg = Constants.pqSqaErrMsg;
	} else if(selectedGrade == "BTEC" && totalGrade > 3) {
		errorFlag = false;
		errorMsg = Constants.pqBtecErrMsg;
	} 
	if(!errorFlag) { $$D("pqerrmsg").innerHTML = errorMsg; $("#previousQualErrMsg").show(); $("#footerContent").show(); }
	if(errorFlag) { 
    $("#pqSelectedGrades").val(selectedGrades.toLowerCase()); 
    $("#valueToBeShown").val(selectedGradesDisplay);
    $("#pqSelToDisplay").val(selectedGradesDisplay);						   		 
	} 	
	return errorFlag;
}
//
function closeErrMsgFunc(id){
 if($("#"+id)) { $("#"+id).hide(); }
 $("#gradeSaveDiv").show(); 
}
//
function displaySubLocation(){
  if ($('.sub_region').css('display') == 'none') {
    $('.sub_region').css('display','block');
  } else {
    $('.sub_region').css('display','none');
  }
}
//
function previousQualGradeCalculation(grade1){
	var tempQualGrades = $("#tempQualGrades").val() + "-";
	var gradeIndex = tempQualGrades.indexOf(grade1+"-");
	var newGradeIndex = gradeIndex - 1;
	var incrementValue = parseInt(tempQualGrades.charAt(newGradeIndex)) + 1;
	var replacedString = tempQualGrades.substr(0, newGradeIndex) + incrementValue + tempQualGrades.substr(newGradeIndex + 1);
	replacedString = replacedString.substring(0, replacedString.length-1);
	$("#tempQualGrades").val(replacedString);
}
//
function updateSessionForChatbotDisbaled(){
  var url = contextPath+"/session/chatbot-disabled.html";
  var ajaxObj = new sack();
  ajaxObj.requestFile = url;	  
  ajaxObj.onCompletion = function(){
    if(ajaxObj.response == 'SUCEESS'){
      $('#isChatbotCookieClosed').val('Y');} 
    else {
      $('#isChatbotCookieClosed').val('');
    }
  };	
  ajaxObj.runAJAX();  
}
//
function closeChatBox(searchURL){   
  var root = window.parent.document.getElementsByTagName( 'html')[0];   
  root.className -= 'scrl_dis'; 
  var width = window.screen.width;
  if(width <= 768) {    
     if(!isBlankOrNull(searchURL)) { window.parent.location.href = searchURL; } else { window.history.go(-1); }
  } else {                
    if(window.parent.document.getElementById("socialBoxMin")) {
      window.parent.document.getElementById("socialBoxMin").style.display = "block"; 
    }
    if(!isBlankOrNull(searchURL)) { window.parent.location.href = searchURL; }
    else {
      window.parent.document.getElementById("fade_NRW").style.display = "none";
      window.parent.document.getElementById("holder_NRW").style.display = "none";
      window.parent.document.getElementById("close_NRW").style.display = "none";    
      if(window.parent.document.getElementById('iframe_NRW').contentWindow.document.getElementById("chatbotWidget")){
        window.parent.document.getElementById('iframe_NRW').contentWindow.document.getElementById("chatbotWidget").innerHTML = "";
      }   
    }
  }  
}
function showUcasPtVal(){	
  var ucsErrFlag = false;
  var qualId = '';jq('.qualArr').each(function(){qualId += this.value + ','; });
  var subId = '';jq('.subjectArr').each(function(){subId += this.value + ',';});
  var grade = '';var gradeId = '';jq('.gradeArr').each(function(){grade += this.value + ',';gradeId += this.id + ','; });	  
  var qualSeq = '';jq('.qualSeqArr').each(function(){qualSeq += this.value + ',';});
  var count = subId.length;
  var qualIdArr = (qualId != null && qualId != '') ? qualId.split(',') : '';
  var subIdArr = (subId != null && subId != '') ? subId.split(',') : '';
  var gradeArr = (grade != null && grade != '') ? grade.split(','): '';
  var gradeIdArr = (gradeId != null && gradeId != '') ? gradeId.split(','): '';
  var qualSeqArr = (qualSeq != null && qualSeq != '') ? qualSeq.split(','): '';	   
  for (i = 0; i < qualIdArr.length; i += 1) {
    var qual = qualIdArr[i];var sub = subIdArr[i];var grde = gradeArr[i];var grdeId = gradeIdArr[i]; var lp = i+1;var qualSeq = qualSeqArr[i];
    if(qual != '' && sub!= '' && qual == '19' && !(grde != '' && grde > 0 && grde < 1000 && (grde.match(numbers)))){  
    	/*var subIpId = grdeId.replace('grde', 'qualSub');
    	var ucsIpId = grdeId.replace('grde', 'span_grde');
    	var errDivId = grdeId.replace('grde', 'err_msg');
    	jq('#'+subIpId).addClass('faild');
    	jq('#'+ucsIpId).addClass('faild');
        //var errId = obj.id.substring(8);//qualSub_0_0 //span_grde_0_0//grde_0_0	        
        var texterr = "<p>Please enter the valid ucas point for the entered subject</p>";
        jq('#'+errDivId).html(texterr);
  	   jq('#'+errDivId).show();
  	 jq('#pId').html("Please enter the qualification");
	  jq('#errorDiv').show();*/
	  ucsErrFlag = true;
    }
  }
  return ucsErrFlag;
}
function clearFields(obj){
   if(jq(obj).val() == '0'){
	   jq(obj).val(''); 
   }	
}