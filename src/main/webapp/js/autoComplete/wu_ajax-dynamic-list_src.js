/**
 * Author:	Mohamed Syed
 * Date:	wu316_20110823
 * Purpose:	Script to populate the autocomplete contents
 * Version:	1.0
 */
//
var ajaxBox_offsetX = 0;
var ajaxBox_offsetY = 0;
//var ajax_list_externalFile = 'http://mdev.whatuni.com/degrees/ajax/autocomplete.html'; //TODO uncomment this when deploy the WAR in LIVE.
var ajax_list_externalFile = '/degrees/ajax/autocomplete.html';
var minimumLettersBeforeLookup = 3;
var selectedText = ""
var ajaxImageElementName = "";
var ajax_list_objects = new Array();
var ajax_list_cachedLists = new Array();
var ajax_list_activeInput = false;
var ajax_list_activeItem
var ajax_list_optionDivFirstItem = false;
var ajax_list_currentLetters = new Array();
var ajax_optionDiv = false;
var ajax_optionDiv_iframe = false;
var ajax_list_MSIE = false;
var currentListIndex = 0;
//
if(navigator.userAgent.indexOf('MSIE') >= 0 && navigator.userAgent.indexOf('Opera') < 0){
	ajax_list_MSIE = true;
}
//
function stringTrimer(inString){
  return inString.replace(/^\s+|\s+$/g,"");
}
//
function $$(id){return document.getElementById(id)}
//
function ajax_getTopPos(inputObj){
	var returnValue = inputObj.offsetTop;
	while((inputObj = inputObj.offsetParent) != null){
		returnValue += inputObj.offsetTop;
	}
	return returnValue;
}
//
function ajax_list_cancelEvent(){
	return false;
}
//
function ajax_getLeftPos(inputObj, userleft){
	var returnValue = inputObj.offsetLeft;
	while((inputObj = inputObj.offsetParent) != null){
		returnValue += inputObj.offsetLeft;
	}
	if(userleft != null && userleft != 'undefined' && userleft != ''){
	   returnValue = parseInt(returnValue) + parseInt(userleft);
	}
	return returnValue;
}
//
function ajax_option_setValue(e,inputObj){	
  var noOpnFlag = true;
  var $ = jQuery.noConflict();
  if(e==false)e = event;
  var spantext = $(this).find(".nocrse").html();
  //Restricting the keyword search in selecting the ajax option through keyboard navigation in (clearing subject landing page)
  if((spantext == null || spantext == "") && ($(inputObj).length)){   
    spantext = $(inputObj).find(".nocrse").html();
    if(!(spantext == null || spantext == "") && $$('keywordSearchFlag')){
      $$('keywordSearchFlag').value = 'no';
    }
  }
  //
  if('optionDivSelected' == e.target.className && 'sub_titl opd_nav' == e.target.firstChild.className){noOpnFlag=false;}//Added by Sangeeth.S for 31_July_rel 
  if(('sub_titl opd_nav' != e.target.className && 'noopn' != e.target.className && noOpnFlag && (spantext == null || spantext == ""))){
      if(!inputObj){
        inputObj = this;
      }
      //<span id="span_3769" style="display: none;">3769###Bangor University###Bangor University_DISPLAY</span>
      var iD = inputObj.id;
      if('sub_titl opd_nav' != inputObj.firstChild.className && (spantext == null || spantext == "")){ //Added by Sangeeth.S for 31_July_rel 
        var spanVal = "";
        if(ajax_list_MSIE){
          spanVal = $$(("span_"+iD)).innerText;
        }else{
          spanVal = $$(("span_"+iD)).textContent;
        }
        var spanValArray = spanVal.split(/###/gi);
       //Get the selected option text content in dropdown, By Prabha V. on 08_Mar_2015      
       if($$(("SUB_"+iD)) || $$(("UNI_"+iD)) || $$(("UCAS_"+iD))){
         var sub = "";
         if($$(("SUB_"+iD))){
           if(ajax_list_MSIE){sub = $$(("SUB_"+iD)).innerText;}else{sub = $$(("SUB_"+iD)).textContent;}
         }else if($$(("UNI_"+iD))){
           if(ajax_list_MSIE){ sub = $$(("UNI_"+iD)).innerText;}else{ sub = $$(("UNI_"+iD)).textContent;}
         }else if($$(("UCAS_"+iD))){
           if(ajax_list_MSIE){sub = $$(("UCAS_"+iD)).innerText;}else{sub = $$(("UCAS_"+iD)).textContent;}
         }
         tmpValue = sub;
       }else if(($$('navKwd') && $$('navKwd').value != "Please enter subject or uni") || $$('navKwdCS') || $$('compKwd') || $$('navKwdCd')){
         tmpValue = spanValArray[2];
	   }else{
          tmpValue = spanValArray[2];
       }
       if($$("curPos") && $$("curPos").value != "" && parseInt($$("curPos").value) >= 0){
         tmpValue = spanValArray[2];
       }
       
      if($$('interstitialSubjectSearch_l1') != null || $$('interstitialSubjectSearch_l2') != null) {
        $$('interstitialSubjectSearch_fs').innerHTML = tmpValue + " ";
        $$('interstitialSubjectSearch_fl').style.display = 'block';
       }
       if(ajax_list_activeInput.name != null && (ajax_list_activeInput.name).indexOf("interstitialSubjectSearch") > -1) {
        tmpValue = replaceAll(tmpValue,'"', '');
       }
       //End of the option selection code
      
        selectedText = tmpValue;
        ajax_list_activeInput.value = tmpValue;		
        //
        var loaderObj;
        if(ajax_list_activeInput.name != null && (ajax_list_activeInput.name).indexOf("interstitialSubjectSearch") > -1) {
          loaderObj = $isp("#"+ajax_list_activeInput.id).closest('.inptsr');
        }
        //
        if(ajax_list_activeInput.name != null && (ajax_list_activeInput.name).indexOf("interstitialSubjectSearch_l1") > -1) {
          $isp('#subjectL2Div input:hidden').val('');
          $isp('#interstitialSubjectSearch_l2').val('');
        }
        var ipName = ajax_list_activeInput.name;
        if($$(ipName + '_hidden')){
          $$(ipName + '_hidden').value = spanValArray[0];
        }
        if($$(ipName + '_ukprn')){
          $$(ipName + '_ukprn').value = spanValArray[1];                
        }
        if($$(ipName + '_id')){
          $$(ipName + '_id').value = spanValArray[0];
        }
        if($$(ipName + '_url')){
          $$(ipName + '_url').value = spanValArray[1];
          if((ajax_list_activeInput.name).indexOf("interstitialSubjectSearch") > -1 && isBlankOrNullString(spanValArray[1]) && isBlankOrNullString($$('interstitialSubjectSearch_l1_url'))) {
            $$('keyword_hidden').value = tmpValue;
            $$('interstitialSubjectSearch_l1_url').value = "";
            $$('interstitialSubjectSearch_l2_url').value = "";
          }else if((ajax_list_activeInput.name).indexOf("courseName") > -1 && isBlankOrNullString(spanValArray[1]) && isBlankOrNullString($$('courseName_url'))){
            $$('keyword_hidden').value = tmpValue;
            $$('courseName_url').value = "";
          }
        }
        if($$(ipName + '_display')){
       $$(ipName + '_display').value = tmpValue;
       }
        if($$(ipName + '_location')){
          $$(ipName + '_location').value = spanValArray[3];
        }	
        if($$(ipName + '_flag')){//Added by Indumathi.S 
         $$(ipName + '_flag').value = spanValArray[1];
        }
        //
        if($$(ipName + '_l2flag')){// 
         $$(ipName + '_l2flag').value = spanValArray[4];
         if($$(ipName + '_l2flag').value == 'N') {
          subjectLevelTwoList();
         } 
        }        
        if($$(ipName + '_code')) { // Adding category code for Advanced search intertitail page
          $$(ipName + '_code').value = spanValArray[2];
          getCourseCount(loaderObj);
          $isp('#sub_error_msg').hide();
        }
       if($$("mobilebrowsecat_id")) {
        globalOnfocus();
       } 
        if($$("myFinalchVal")) {
        if(($$("myFinalchVal").value=="addFcUni") || ($$("myFinalchVal").value=="saveFcUni")) {
          saveFinalChSlot();   
        }  
       }
       //
       if($$("curPos") && $$("curPos").value != "" && parseInt($$("curPos").value) >= 0){
         updateFinalChoice();   
       }
       //
       var focusId = ajax_list_activeInput.id;
       if(focusId.indexOf("qualSub") > -1){
         $$(focusId).focus(); //set focus to paraent text box for 16_Feb_2016, By Thiyagu G.
         $$(focusId).blur(); 
       } 
        ajax_options_hide();
        if("uniSrchTpId"==focusId || "keywordTpNav"==focusId){
          $$(focusId).blur(); 
        }
	  }
      var clrFlag = ($$("clearingSwitchFlag") && $$("clearingSwitchFlag").value == 'ON')  ? $$("clearingSwitchFlag").value : 'OFF';
      if("navKwd"==focusId){navSearchSubmit(clrFlag, 'Top');}
      if("clearingKwd"==focusId){clearingSearchSubmit(true, clrFlag);}
      if("navKwdCS"==focusId){csSearchSubmit(clrFlag);}
      if("navKwdCd"==focusId){navSearchSubmitCD(clrFlag);}
      if("compKwd"==focusId){compSearchSubmit(clrFlag);}
      //if("uniOpenDayName"==focusId){onclickfunction();}
      if("navMobKwd"==focusId){navSearchSubmit(clrFlag, 'topMobNav');}
      if("revUniName"==focusId){searchMoreReviewListURL('uniNameForm');}
      if("revSubjectId"==focusId){searchMoreReviewListURL('subNameForm');}
    // if("courseName"==focusId){courseSearchSubmit(courseName);}
      if("uniSrchTpId"==focusId){redirectUniHOME($$('uniSrchTpId'),'uniSrchTpForm','finduniTp');}
    }
    if($$('GAMPagename').value == "subjectlandingpage.jsp") {
      getSelectedSubject();
    }
  }
//
function ajax_options_hide(){
	if(ajax_optionDiv){
 if(ajax_optionDiv.className == "ajax_mob" || ajax_optionDiv.className == "opd_ajax"){
      ajax_optionDiv.className = "";
    }
		ajax_optionDiv.style.display = 'none';
		//$$("topDiv").style.display='none';
	}
	if(ajax_optionDiv_iframe){
		ajax_optionDiv_iframe.style.display = 'none';
		//$$("topDiv").style.display='none';
}	}
//
function ajax_options_rollOverActiveItem(item,fromKeyBoard){
  if(ajax_list_activeItem){      
      ajax_list_activeItem.className = 'optionDiv';      
  }    
  item.className = 'optionDivSelected';    
  ajax_list_activeItem = item;
  if(fromKeyBoard){
    if(ajax_list_activeItem.offsetTop > ajax_optionDiv.offsetHeight){
      ajax_optionDiv.scrollTop = ajax_list_activeItem.offsetTop - ajax_optionDiv.offsetHeight + ajax_list_activeItem.offsetHeight + 2;
    }
    if(ajax_list_activeItem.offsetTop < ajax_optionDiv.scrollTop){
      ajax_optionDiv.scrollTop = 0;
    }	
  }  
  if($$('SH')){$$('SH').className = "";}
  if($$('UH')){$$('UH').className = "";}
  if($$('UCH')){$$('UCH').className = "";}
  if($$('OD')){$$('OD').className = "";}//17_Mar_2015 - Assigned empty for open days search objects, by Thiyagu G.
  if($$('RG')){$$('RG').className = "";}
  if($$('SG')){$$('SG').className = "";}//08_Mar_2015 - Ajax dropdown structure changes, By Prabha V.
  if($$('KW')){$$('KW').className = "";}
  if($$('RCT')){$$('RCT').className = "";}
  if($$('ADV')){$$('ADV').className = "optionDiv ajx_adsrch";}//31_Jul_2018 - Ajax dropdown structure changes, By Sangeeth.S
}
//
function ajax_option_list_buildList(letters,paramToExternalFile){
	ajax_optionDiv.innerHTML = '';
	ajax_list_activeItem = false;
	if(ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()].length <= 1){
      if(paramToExternalFile == 'jobOrIndustry' || paramToExternalFile == 'qualsubects' || ($$("curPos") && parseInt($$("curPos").value) >= 0 && paramToExternalFile == 'w') || paramToExternalFile == 'opdkeywordbrowse' || 'opendaysearch' == paramToExternalFile || 'interstitialsearch' == paramToExternalFile || paramToExternalFile == 'reviewSubjectSearch'|| paramToExternalFile == 'reviewUniSearch' || paramToExternalFile == 'coursekeywordbrowse'){
      var span = document.createElement('span');//Added error msg in ajax list Indumathi.S 8-Mar-16 Rel  
      span.id = 'choose';
      if(!($$('GAMPagename').value == "subjectlandingpage.jsp")){
        span.innerHTML = 'No results found for ' + letters;
      }
      ajax_optionDiv.appendChild(span);
      $$('ajax_listOfOptions').style.display = 'block';
      return;
    }else {
      ajax_options_hide();
      return;
    }
	}
	ajax_list_optionDivFirstItem = false;
	var optionsAdded = false;
//	var rootDiv = document.createElement('DIV');
//	rootDiv.id = 'rootdiv';
	
 var chooseTxt = true;
 if(stringTrimer(paramToExternalFile) == 'keywordbrowse' || stringTrimer(paramToExternalFile) == 'csKeywordbrowse' || stringTrimer(paramToExternalFile) == 'compKeywordbrowse' || stringTrimer(paramToExternalFile) == 'clkeywordbrowse' || stringTrimer(paramToExternalFile) == 'recentAndAdvSrch' || stringTrimer(paramToExternalFile) == 'mobkeywordbrowse'
    || stringTrimer(paramToExternalFile) == 'interstitialsearch' || stringTrimer(paramToExternalFile) == 'subjectSearchTopNav' || stringTrimer(paramToExternalFile) == 'topNavUniSearch'){ 
  chooseTxt = false;
 }
 if(chooseTxt){
   var span = document.createElement('span');
	  span.id = 'choose';
	  span.innerHTML = 'Please choose from below:';
   ajax_optionDiv.appendChild(span);
 }
	for(var no=0; no < ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()].length-1; no++){
		if(ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()][no].length == 0){
			continue;
		}
		optionsAdded = true;
		var div = document.createElement('DIV');
		var items = ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()][no].split(/###/gi);
		if(ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()].length == 1 && ajax_list_activeInput.value == items[0]){
			ajax_options_hide();
			return;
		}
		//3769###Bangor University###Bangor University_DISPLAY|
		div.innerHTML = stringTrimer(items[items.length-2]);
    div.id = stringTrimer(items[0]);
    if(paramToExternalFile == 'jobOrIndustry'){
      div.id = stringTrimer(items[2]);
    }    
    div.className = 'optionDiv';   
    div.onmouseover = function(){
			ajax_options_rollOverActiveItem(this,false);
		} //
    if(stringTrimer(paramToExternalFile) != 'keywordbrowse' && stringTrimer(paramToExternalFile) != 'opdkeywordbrowse' && 
    stringTrimer(paramToExternalFile) != 'qualuniofinterstsubects' && stringTrimer(paramToExternalFile) != 'prospectusbrowse' && 
    stringTrimer(paramToExternalFile) != 'clkeywordbrowse' && stringTrimer(paramToExternalFile) != 'csKeywordbrowse' && 
    stringTrimer(paramToExternalFile) != 'ucaskeywordbrowse' && stringTrimer(paramToExternalFile) != 'compKeywordbrowse' &&
    stringTrimer(paramToExternalFile) != 'mobkeywordbrowse' && stringTrimer(paramToExternalFile) != 'recentAndAdvSrch' &&
    stringTrimer(paramToExternalFile) != 'interstitialsearch' && stringTrimer(paramToExternalFile) != 'reviewUniSearch' && 
    stringTrimer(paramToExternalFile) != 'reviewSubjectSearch' && stringTrimer(paramToExternalFile) != 'coursekeywordbrowse' &&
    stringTrimer(paramToExternalFile) != 'subjectSearchTopNav' && stringTrimer(paramToExternalFile) != 'topNavUniSearch'){ 
      div.title = stringTrimer(items[items.length-2]);
    }
    var titleText = stringTrimer(items[0]);
    //alert("titleText : "+titleText);
    var titletextFlag = "true";
    
    if(stringTrimer(paramToExternalFile) == 'keywordbrowse' || stringTrimer(paramToExternalFile) == 'opdkeywordbrowse' || 
    stringTrimer(paramToExternalFile) == 'prospectusbrowse' || stringTrimer(paramToExternalFile) == 'clkeywordbrowse' ||
    stringTrimer(paramToExternalFile) == 'csKeywordbrowse' || stringTrimer(paramToExternalFile) == 'ucaskeywordbrowse' || 
    stringTrimer(paramToExternalFile) == 'mobkeywordbrowse' || stringTrimer(paramToExternalFile) == 'recentAndAdvSrch' || 
    stringTrimer(paramToExternalFile) == 'coursekeywordbrowse' || stringTrimer(paramToExternalFile) == 'subjectSearchTopNav' ||
    stringTrimer(paramToExternalFile) == 'topNavUniSearch' || stringTrimer(paramToExternalFile) == 'compKeywordbrowse'){       
      if(titleText =='SH' ||  titleText =='UH' || titleText =='UCH' || titleText =='OD' || titleText =='RG' || titleText =='SG' || titleText =='KW' || titleText == 'RCT' || titleText == 'ADV'){ //17_Mar_2015 - Added condition for heading non clickable, by Thiyagu G.
         titletextFlag = "false";         
      }
    }
  if(titletextFlag == "true") {    
		div.onclick = ajax_option_setValue;
  }
		//
		var span1 = document.createElement('span');
		span1.id = ('span_' + stringTrimer(items[0]));
    if(paramToExternalFile == 'jobOrIndustry'){
      span1.id = ('span_' + stringTrimer(items[2]));
    }    
		span1.innerHTML = stringTrimer(ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()][no]); //("span_" + stringTrimer(items[1]));//assigning actual college-name which will be used in URL.
		span1.style.display = 'none';
		div.appendChild(span1);
		//
		if(!ajax_list_optionDivFirstItem){
			ajax_list_optionDivFirstItem = div;
		}
		ajax_optionDiv.appendChild(div);
		
    //Added hidden value advanced search qualification for the interstial page
    if('ADV' == stringTrimer(items[0])){
      var inputName = ajax_list_activeInput.name;      
      if($$('keyword_qualification')){//for desktop
        $$('keyword_qualification').value = stringTrimer(items[1]);
      }
      var width = window.screen.width;
      if(width <= 768 && $$$('keywordMob_qualification')) {     
        $$('keywordMob_qualification').value = stringTrimer(items[1]);
      }
      //if($$(inputName + '_qualification')){
      //  $$(inputName + '_qualification').value = stringTrimer(items[1]);
      //}   
    }     
    //
	}   
  //
//	ajax_optionDiv.appendChild(rootDiv);
//	var tmpWidth = ajax_optionDiv.style.width;
//	if(tmpWidth.indexOf('px') >= 0){
//	  tmpWidth = tmpWidth.substring(0,tmpWidth.indexOf('px'));
//	  span.style.width = parseInt(tmpWidth) - 15 + 'px';
//	  rootDiv.style.width = ajax_optionDiv.style.width;
//	}
	if(optionsAdded){
		ajax_optionDiv.style.display = 'block';
		if(ajax_optionDiv_iframe){
			ajax_optionDiv_iframe.style.display='';
		}
		ajax_options_rollOverActiveItem(ajax_list_optionDivFirstItem,true);
  }	
  if($$('SH')){$$('SH').className = "";}
  if($$('UH')){$$('UH').className = "";}
  if($$('UCH')){$$('UCH').className = "";}
  if($$('OD')){$$('OD').className = "";} //17_Mar_2015 - Assigned empty for open days search objects, by Thiyagu G.
  if($$('RG')){$$('RG').className = "";}
  if($$('SG')){$$('SG').className = "";}
  if($$('KW')){$$('KW').className = "";}
  if($$('RCT')){$$('RCT').className = "";}
  if($$('ADV')){$$('ADV').className = "optionDiv ajx_adsrch";}//31_Jul_2018 - Ajax dropdown structure changes, By Sangeeth.S
}
//
function ajax_option_list_showContent(ajaxIndex,inputObj,paramToExternalFile,whichIndex){
	if(whichIndex != currentListIndex){
		return
	}
	var letters = inputObj.value;
	var content = ajax_list_objects[ajaxIndex].response;
  if(paramToExternalFile == 'keywordbrowse' || paramToExternalFile == 'opdkeywordbrowse' || paramToExternalFile == 'compKeywordbrowse' 
  || paramToExternalFile == 'clkeywordbrowse' || paramToExternalFile == 'csKeywordbrowse' || paramToExternalFile == 'ucaskeywordbrowse'
  || paramToExternalFile == 'mobkeywordbrowse' || paramToExternalFile == 'recentAndAdvSrch' || paramToExternalFile == 'subjectSearchTopNav'){
    var keywordContentArray = content.split('@@@');
    if(paramToExternalFile == 'compKeywordbrowse'){    
      $$('compKeywordbrowse').value = "";
      if(keywordContentArray[0] != 'NOMATCH'){
        $$('compKeywordbrowse').value = keywordContentArray[0];
      }
    }else if(paramToExternalFile == 'opdkeywordbrowse'){  //17_Mar_2015 - Assigned ajax url for open days search, by Thiyagu G.
        $$('opdmatchbrowsenode').value = "";
        if(keywordContentArray[0] != 'NOMATCH'){
          $$('opdmatchbrowsenode').value = keywordContentArray[0];
        }
    }else if(paramToExternalFile == 'clkeywordbrowse'){  //09_Jun_2015 - Assigned ajax url for clearing home course search in clearing home page, by Thiyagu G.
        $$('clmatchbrowsenode').value = "";
        if(keywordContentArray[0] != 'NOMATCH'){
          $$('clmatchbrowsenode').value = keywordContentArray[0];
        }
    }else if(paramToExternalFile == 'csKeywordbrowse'){  //21_July_2015 - Assigned ajax url for course search in CS page, by Thiyagu G.
        $$('csmatchbrowsenode').value = "";
        if(keywordContentArray[0] != 'NOMATCH'){
          $$('csmatchbrowsenode').value = keywordContentArray[0];
        }
    }else if(paramToExternalFile == 'ucaskeywordbrowse'){  //21_July_2015 - Assigned ajax url for course search in CS page, by Thiyagu G.
        $$('ucasmatchbrowsenode').value = "";
        if(keywordContentArray[0] != 'NOMATCH'){
          $$('ucasmatchbrowsenode').value = keywordContentArray[0];
        }
    }else if(paramToExternalFile == 'mobkeywordbrowse'){  //21_July_2015 - Assigned ajax url for course search in CS page, by Thiyagu G.
        $$('matchbrowsenodemob').value = "";
        if(keywordContentArray[0] != 'NOMATCH'){
          $$('matchbrowsenodemob').value = keywordContentArray[0];
        }
    }else if(paramToExternalFile == 'subjectSearchTopNav'){
        $$('matchbrowsenodeTpNav').value = "";
        if(keywordContentArray[0] != 'NOMATCH'){
          $$('matchbrowsenodeTpNav').value = keywordContentArray[0];
        }
    }else{
        $$('matchbrowsenode').value = "";
        if(keywordContentArray[0] != 'NOMATCH'){
          $$('matchbrowsenode').value = keywordContentArray[0];
        }
    }
    if(keywordContentArray.length > 1){
      content =  keywordContentArray[1];
    }
  }
  var elements = content.split('|');
	ajax_list_cachedLists[paramToExternalFile][letters.toLowerCase()] = elements;
	ajax_option_list_buildList(letters,paramToExternalFile);
	if($$(ajaxImageElementName)){
		$$(ajaxImageElementName).style.display = 'none';
}	}
//
function ajax_option_resize(inputObj, userleft){
	if(ajax_list_MSIE && $$('coursetitle') != null){
		ajax_optionDiv.style.top = ((ajax_getTopPos(inputObj) + inputObj.offsetHeight + ajaxBox_offsetY) - 20) + 'px';
	}else{
		ajax_optionDiv.style.top = (ajax_getTopPos(inputObj) + inputObj.offsetHeight + ajaxBox_offsetY) + 'px';
	}
	ajax_optionDiv.style.left = (ajax_getLeftPos(inputObj,userleft) + ajaxBox_offsetX) + 'px';
  ajax_optionDiv.style.width = (ajax_getWidth(inputObj)) + 'px';
	if(ajax_optionDiv_iframe){
		ajax_optionDiv_iframe.style.left = ajax_optionDiv.style.left;
		ajax_optionDiv_iframe.style.top = ajax_optionDiv.style.top;
}	}
//
function ajax_showOptions(inputObj,paramToExternalFile,e,image,extraParamList, userleft, userwidth){
    if(e.keyCode == 13 || e.keyCode == 9){
		return;
	}
    if(ajax_list_currentLetters[inputObj.name] == inputObj.value && "recentAndAdvSrch"!=paramToExternalFile){
		return;
	}
    ajax_list_cachedLists[paramToExternalFile] = new Array();
    ajax_list_currentLetters[inputObj.name] = inputObj.value;    
    if($$("curPos") && $$("curPos").value != "" && parseInt($$("curPos").value) >= 0){
        ajax_optionDiv.className = "opd_ajax";
    }else if(document.getElementById("finalChLightBox")){
      if(document.getElementById("finalChLightBox").value=="true"){        
        ajax_optionDiv.className = "opd_ajax_f5";
      }
    }
    else if(document.getElementById("mychTapView")){
      if(document.getElementById("mychTapView").value=="true"){        
        ajax_optionDiv.className = "opd_ajax_f5";
      }
    }else if(document.getElementById("resReviews")){
      if(document.getElementById("resReviews").value=="true"){        
        if('keywordbrowse' == paramToExternalFile || 'mobkeywordbrowse' == paramToExternalFile){    //Added by sangeeth.S for the Aug_28_18 rel
          ajax_optionDiv.className = "ajx_cnt ajx_res rw_ajax";
        }else if('recentAndAdvSrch' == paramToExternalFile || 'coursekeywordbrowse' ==paramToExternalFile){
          ajax_optionDiv.className = "ajx_cnt"; 
        }
        else{
          ajax_optionDiv.className = "rw_ajax";
        }
      }     
    }else if('opdkeywordbrowse' == paramToExternalFile || 'opendaysearch' == paramToExternalFile){//Added by sangeeth.S for 31_July_18 rel
       ajax_optionDiv.className = 'no_opds';
    }else if('keywordbrowse' == paramToExternalFile || 'mobkeywordbrowse' == paramToExternalFile || 'clkeywordbrowse' == paramToExternalFile || 'csKeywordbrowse' == paramToExternalFile 
    || 'compKeywordbrowse' == paramToExternalFile || 'coursekeywordbrowse' ==paramToExternalFile || 'subjectSearchTopNav' == paramToExternalFile || 'topNavUniSearch' == paramToExternalFile){
       ajax_optionDiv.className = 'ajx_cnt ajx_res';
    }else if('recentAndAdvSrch' == paramToExternalFile || 'interstitialsearch' == paramToExternalFile){
       ajax_optionDiv.className = 'ajx_cnt';
    }else{
       ajax_optionDiv.className = '';
    }
    /*
     if(document.getElementById('ajax_listOfOptions') && document.getElementById('ajax_listOfOptions').parentNode.id == 'ajxcnt_mob' && 'navMobKwd' != inputObj.id){
      ajax_optionDiv = false;
      console.log("if");
    }else if('navMobKwd' == inputObj.id && document.getElementById('ajxcnt_mob') && document.getElementById('ajxcnt_mob').innerHTML == ''){
      console.log("else if");
      ajax_optionDiv = false;
    }
    
    */
    if('navMobKwd' == inputObj.id && document.getElementById('ajxcnt_mob') && document.getElementById('ajxcnt_mob').innerHTML == ''){
      if(document.getElementById('ajax_listOfOptions')){
        document.getElementById('ajax_listOfOptions').remove();
      }
      ajax_optionDiv = false;
    }else if('navMobKwd' != inputObj.id && !(document.getElementById('ajax_listOfOptions'))){
      ajax_optionDiv = false;
    }
    
    if(!ajax_optionDiv){
      ajax_optionDiv = document.createElement('DIV');
      ajax_optionDiv.id = 'ajax_listOfOptions';
      //Added for adding class at first on focus
      if('recentAndAdvSrch' == paramToExternalFile || 'interstitialsearch' == paramToExternalFile ){
        ajax_optionDiv.className = 'ajx_cnt';
      }else if('subjectSearchTopNav' == paramToExternalFile || 'topNavUniSearch' == paramToExternalFile){
    	  ajax_optionDiv.className = 'ajx_cnt ajx_res';  
      }
      //
      if(userwidth != null && userwidth != 'undefined' && userwidth != ''){
        ajax_optionDiv.style.width = userwidth + "px";
      }else{
         ajax_optionDiv.style.width = inputObj.offsetWidth + "px";
      }
      //span.style.width=parseInt(ajax_optionDiv.style.width-10+'px');
      if(document.getElementById('ajxuni_mob') && document.getElementById('isClearingSRPage') && 'uniName' == inputObj.id && document.getElementById('check_mobile_hidden') && document.getElementById('check_mobile_hidden').value == 'true'){
        document.getElementById('ajxuni_mob').appendChild(ajax_optionDiv);
      }else if(('recentAndAdvSrch' == paramToExternalFile || 'mobkeywordbrowse' == paramToExternalFile)&& 'navMobKwd' == inputObj.id && document.getElementById('ajxcnt_mob')){
        document.getElementById('ajxcnt_mob').appendChild(ajax_optionDiv);
      }else{
        document.body.appendChild(ajax_optionDiv);
      }
      
      if(ajax_list_MSIE){
        ajax_optionDiv_iframe = document.createElement('IFRAME');
        ajax_optionDiv_iframe.border = '0';
        if(userwidth != null && userwidth != 'undefined' && userwidth != ''){
          ajax_optionDiv_iframe.style.width = userwidth + 'px';
        }else{
          ajax_optionDiv_iframe.style.width = ajax_optionDiv.clientWidth + 'px';
        }
        ajax_optionDiv_iframe.style.height = ajax_optionDiv.clientHeight + 'px';
        ajax_optionDiv_iframe.id = 'ajax_listOfOptions_iframe';
        document.body.appendChild(ajax_optionDiv_iframe);
        if(userwidth != null && userwidth != 'undefined' && userwidth != ''){
          ajax_optionDiv_iframe.style.width = userwidth + "px";
        }else{
           ajax_optionDiv_iframe.style.width = inputObj.offsetWidth + "px";
        }
      }
      var allInputs = document.getElementsByTagName('INPUT');
      for(var no=0; no < allInputs.length; no++){
        if(!allInputs[no].onkeyup){
          allInputs[no].onfocus = ajax_options_hide;
        }
      }
      var allSelects = document.getElementsByTagName('SELECT');
      for(var no=0; no < allSelects.length; no++){
        allSelects[no].onfocus = ajax_options_hide;
      }
      var oldonkeydown = document.body.onkeydown;
      if(typeof oldonkeydown != 'function'){
        document.body.onkeydown=ajax_option_keyNavigation;
      }else{
        document.body.onkeydown=function(){
          oldonkeydown();
          ajax_option_keyNavigation();
      }	}
      var oldonresize = document.body.onresize;
      var iObj = inputObj;
      if(inputObj.id == 'coursetitle'){
        iObj = $$('coursetitle');
      }
      if(typeof oldonresize != 'function'){
        document.body.onresize = function(){
          ajax_option_resize(iObj,userleft);
        }
      }else{
        document.body.onresize = function(){
          oldonresize();
          ajax_option_resize(iObj,userleft);
      }	}
    }else{
      if(userwidth != null && userwidth != 'undefined' && userwidth != ''){
        ajax_optionDiv.style.width = userwidth + "px";
      }else{
         ajax_optionDiv.style.width = inputObj.offsetWidth + "px";
      }
      if(ajax_list_MSIE){
        if(userwidth != null && userwidth != 'undefined' && userwidth != ''){
          ajax_optionDiv_iframe.style.width = userwidth + "px";
        }else{
          ajax_optionDiv_iframe.style.width = inputObj.offsetWidth + "px";
      }	}
      var oldonresize = document.body.onresize;
      var iObj = inputObj;
      if(inputObj.id == 'coursetitle'){
        iObj = $$('coursetitle');
      }
      if(typeof oldonresize != 'function'){
        document.body.onresize = function(){
          ajax_option_resize(iObj,userleft);
        }
      }else{
        document.body.onresize = function(){
          oldonresize();
          ajax_option_resize(iObj,userleft);
        }
      }
    }
    if(inputObj.value.length < minimumLettersBeforeLookup && "recentAndAdvSrch"!=paramToExternalFile){
		ajax_options_hide();
		return;
	}
    ajax_optionDiv.style.top = (ajax_getTopPos(inputObj) + inputObj.offsetHeight + ajaxBox_offsetY) + 'px';
    if(userleft != null && userleft != ''){
        ajax_optionDiv.style.left = ((parseInt(ajax_getLeftPos(inputObj,userleft)) + parseInt(ajaxBox_offsetX))) + 'px';
        //ajax_optionDiv.style.left=(ajax_getLeftPos(inputObj)+ajaxBox_offsetX)+'px';
    }else{
       ajax_optionDiv.style.left = (ajax_getLeftPos(inputObj) + ajaxBox_offsetX) + 'px';
    }
    if(ajax_optionDiv_iframe){
		ajax_optionDiv_iframe.style.left = ajax_optionDiv.style.left;
		ajax_optionDiv_iframe.style.top = ajax_optionDiv.style.top;
	}
    ajax_list_activeInput = inputObj;
    ajax_optionDiv.onselectstart = ajax_list_cancelEvent;
    currentListIndex++;
    if(ajax_list_cachedLists[paramToExternalFile][inputObj.value.toLowerCase()]){
		ajax_option_list_buildList(inputObj.value,paramToExternalFile,currentListIndex)
    }else{
		var tmpIndex = currentListIndex/1;
		ajax_optionDiv.innerHTML = '';
		var ajaxIndex = ajax_list_objects.length;
		ajax_list_objects[ajaxIndex] = new sack();
		var extraParameter = "";
		/* 
		if(extraParamList != '' && extraParamList != null){
			var divList = extraParamList.split(",")
			for(i=0; i<divList.length; i++){
				var values = divList[i].split(":");
				extraParameter += values[0] + "=" + $$(values[1]).value + "&";
		}	}
		*/
    var inputObjValue = inputObj.value.replace(" ","+");
    if(inputObj.id == 'interstitialSubjectSearch_l1'){
      inputObjValue = encodeURIComponent(inputObj.value);
    }
    if('subjectSearchTopNav' == paramToExternalFile || 'topNavUniSearch' == paramToExternalFile){
    	inputObjValue = inputObjValue.replace(/[%_]/g, '');
    	inputObjValue = encodeURIComponent(inputObjValue);
    }
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//http://www.whatuni.com/degrees/ajax/autocomplete.html?t=[Z/W]&l=[FREE_TEXT]&z=[COLLEGE_ID]&r=[RAND_NO]
		var url = ajax_list_externalFile + '?t=' + paramToExternalFile + '&l=' + inputObjValue + '&' + extraParamList;
		//var url = ajax_list_externalFile + '?t=' + paramToExternalFile + '&l=' + inputObj.value.replace(" ","+") + '&' + extraParameter;
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		ajax_list_objects[ajaxIndex].requestFile = url;
		ajax_list_objects[ajaxIndex].onCompletion = function(){
			ajax_option_list_showContent(ajaxIndex,inputObj,paramToExternalFile,tmpIndex);
		}
		ajaxImageElementName = image;
		//$$("topDiv").style.display='block';
		if($$(ajaxImageElementName)){
			$$(ajaxImageElementName).style.display = 'block';
		}
		ajax_list_objects[ajaxIndex].runAJAX();
}	}
//
function ajax_option_keyNavigation(e){
	if(!ajax_optionDiv){
		return;
	}
	if(ajax_optionDiv.style.display == 'none'){
		return;
	}
	if(document.all)e = event;
    e = e == undefined ? event : e;
	if(e.keyCode == 38){
		if(!ajax_list_activeItem){
			return;
		}
		if(ajax_list_activeItem && !ajax_list_activeItem.previousSibling){
			return;
		}
		ajax_options_rollOverActiveItem(ajax_list_activeItem.previousSibling,true);
	}
	if(e.keyCode == 40){
		if(!ajax_list_activeItem){
			ajax_options_rollOverActiveItem(ajax_list_optionDivFirstItem,true);
		}else{
			if(!ajax_list_activeItem.nextSibling){
				return;
			}
			ajax_options_rollOverActiveItem(ajax_list_activeItem.nextSibling,true)
	}	}
	if(e.keyCode == 13 || e.keyCode == 9){
		if(ajax_list_activeItem && ajax_list_activeItem.className == 'optionDivSelected'){
			ajax_option_setValue(e,ajax_list_activeItem);   
		}
                //17_Mar_2015 - Removed below condition for submitting enter click in search textbox, by Thiyagu G.
		/*if(e.keyCode == 13){
			return false;
		}else{
			return true;
		}*/
                return true;
	}
	if(e.keyCode == 27){
    ajax_options_hide();
	}
}
//
document.documentElement.onclick = autoHideList;
//
function autoHideList(e){
var $ = jQuery.noConflict();
	if(document.all){
		e = event;
	}
	if(e.target){
		source = e.target;
	}else if(e.srcElement){
		source = e.srcElement;
	}
	if(source.nodeType == 3){
		source = source.parentNode;
	}
  var hideFlag = true;  
  if('optionDivSelected' == e.target.className && 'sub_titl opd_nav' == e.target.firstChild.className){hideFlag=false;}
  if($('.optionDivSelected').find('.nocrse').length){hideFlag=false;}
	if(source.tagName.toLowerCase() != 'input' && source.tagName.toLowerCase() != 'textarea' &&'sub_titl opd_nav' != e.target.className && 'noopn' != e.target.className && hideFlag){
    //
    if(ajax_list_activeInput && ajax_list_activeInput.id.indexOf('interstitialSubjectSearch') > -1 && isBlankOrNullString($$('keyword_hidden').value) && isBlankOrNullString($$('interstitialSubjectSearch_l1_url').value)) {
      $$('keyword_hidden').value = ajax_list_activeInput.value;
      $$('interstitialSubjectSearch_fs').innerHTML = ajax_list_activeInput.value + " ";
      $$('interstitialSubjectSearch_fl').style.display = 'block';
      getCourseCount($isp("#"+ajax_list_activeInput.id).closest('.inptsr'));
    }
    //
    ajax_options_hide();
	}
}
//
function ajax_getWidth(inputObj){
  var returnValue = inputObj.clientWidth;
	return returnValue;
}