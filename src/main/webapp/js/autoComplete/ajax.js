function sack(file){
this.xmlhttp=null
this.resetData=function(){
this.method="POST"
this.queryStringSeparator="?"
this.argumentSeparator="&"
this.URLString=""
this.encodeURIString=true
this.execute=false
this.element=null
this.elementObj=null
this.requestFile=file
this.vars=new Object()
this.responseStatus=new Array(2)}
this.resetFunctions=function(){
this.onLoading=function(){}
this.onLoaded=function(){}
this.onInteractive=function(){}
this.onCompletion=function(){}
this.onError=function(){}
this.onFail=function(){}}
this.reset=function(){
this.resetFunctions()
this.resetData()}
this.createAJAX=function(){
try{
this.xmlhttp=new ActiveXObject("Msxml2.XMLHTTP")
}catch(e1){
try{
this.xmlhttp=new ActiveXObject("Microsoft.XMLHTTP")
}catch(e2){
this.xmlhttp=null}}
if(! this.xmlhttp){
if(typeof XMLHttpRequest !="undefined"){
this.xmlhttp=new XMLHttpRequest()
}else{
this.failed=true}}}
this.setVar=function(name,value){
this.vars[name]=Array(value,false)}
this.encVar=function(name,value,returnvars){
if(true==returnvars){
return Array(encodeURIComponent(name),encodeURIComponent(value))
}else{
this.vars[encodeURIComponent(name)]=Array(encodeURIComponent(value),true)}}
this.processURLString=function(string,encode){
encoded=encodeURIComponent(this.argumentSeparator)
regexp=new RegExp(this.argumentSeparator+"|"+encoded)
varArray=string.split(regexp)
for(i=0;i<varArray.length;i++){
urlVars=varArray[i].split("=")
if(true==encode){
this.encVar(urlVars[0],urlVars[1])
}else{
this.setVar(urlVars[0],urlVars[1])}}}
this.createURLString=function(urlstring){
if(this.encodeURIString&&this.URLString.length){
this.processURLString(this.URLString,true)}
if(urlstring){
if(this.URLString.length){
this.URLString+=this.argumentSeparator+urlstring
}else{
this.URLString=urlstring}}
this.setVar("rndval",new Date().getTime())
urlstringtemp=new Array()
for(key in this.vars){
if(false==this.vars[key][1]&&true==this.encodeURIString){
encoded=this.encVar(key,this.vars[key][0],true)
delete this.vars[key]
this.vars[encoded[0]]=Array(encoded[1],true)
key=encoded[0]}
urlstringtemp[urlstringtemp.length]=key+"="+this.vars[key][0]}
if(urlstring){
this.URLString+=this.argumentSeparator+urlstringtemp.join(this.argumentSeparator)
}else{
this.URLString+=urlstringtemp.join(this.argumentSeparator)}}
this.runResponse=function(){
eval(this.response)}
this.runAJAX=function(urlstring){
if(this.failed){
this.onFail()
}else{
this.createURLString(urlstring)
if(this.element){
this.elementObj=document.getElementById(this.element)}
if(this.xmlhttp){
var self=this
if(this.method=="GET"){
totalurlstring=this.requestFile+this.queryStringSeparator+this.URLString
this.xmlhttp.open(this.method,totalurlstring,true)
}else{
this.xmlhttp.open(this.method,this.requestFile,true)
try{
this.xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
}catch(e){alert("error");}}
this.xmlhttp.onreadystatechange=function(){
switch(self.xmlhttp.readyState){
case 1:
self.onLoading()
break
case 2:
self.onLoaded()
break
case 3:
self.onInteractive()
break
case 4:
self.response=self.xmlhttp.responseText
self.responseXML=self.xmlhttp.responseXML
self.responseStatus[0]=self.xmlhttp.status
self.responseStatus[1]=self.xmlhttp.statusText
if(self.execute){
self.runResponse()}
if(self.elementObj){
elemNodeName=self.elementObj.nodeName
elemNodeName.toLowerCase()
if(elemNodeName=="input"
|| elemNodeName=="select"
|| elemNodeName=="option"
|| elemNodeName=="textarea"){
self.elementObj.value=self.response
}else{
self.elementObj.innerHTML=self.response}}
if(self.responseStatus[0]=="200"){
self.onCompletion()
}else{
self.onError()}
self.URLString=""
break}}
this.xmlhttp.send(this.URLString)}}}
this.reset()
this.createAJAX()}
function autocompleteOff(element){var form=document.getElementById(element);if(form){form.setAttribute("autocomplete","off");}}
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
function replaceURL(inString){
var reg=/\-The-/gi
var specialChar=new Array("(The)","(the)","the-","The ","the ","'","~","!","@","#","$","%","^","&","*","(",")","{","}","[","]","<",">",",",".","?","/","|",";",":","+","=","_","`")
inString=inString.replace(reg," ")
if(specialChar.length>0){
inString=inString.replace(reg," ")
for(charStart=0;charStart<specialChar.length;charStart++){inString=replaceAll(inString,specialChar[charStart],"");}}
return inString}
function replaceHypen(inString){
if(inString !=null){
inString=trimString(inString)
inString=replaceAll(inString,"&","and")
inString=replaceAll(inString," ","-")
inString=replaceAll(inString,"-+","-")
inString=replaceAll(inString,"--","-")}
return inString}
function replaceAll(replacetext,findstring,replacestring){
var strReplaceAll=replacetext;var intIndexOfMatch=strReplaceAll.indexOf(findstring)
while(intIndexOfMatch !=-1){
strReplaceAll=strReplaceAll.replace(findstring,replacestring);intIndexOfMatch=strReplaceAll.indexOf(findstring)}
return strReplaceAll}
function replaceSpecialCharacter(inString){
var outString=inString
var temp=""
if(outString !=null){
var data=outString
for(var i=0;i<data.length;i++){
var character=data.charCodeAt(i)
if(!((character>=48&&character<=57)||(character>=65&&character<=90)||(character>=97&&character<=122)|| character==45)){
temp=temp+" "
}else{
temp=temp+data.substring(i,i+1)}}
outString=temp}
return outString}
function showContactUsPopup(){
window.open("/degrees/help/contactUs.jsp","contactuspopup","width=620,height=450,scrollbars=no,resizable=no");return false}
function clearValue(e,ts){if(e.keyCode !=13&&e.keyCode !=9&&document.getElementById('collegeName_hidden')!=null){document.getElementById('collegeName_hidden').value="";}}
function hideExpandDiv(divids){document.getElementById(divids).style.display=document.getElementById(divids).style.display=='none' ? 'block' : 'none'}
function searchTextValidate(name) {
var specialChar = "\\,#%;?/_&*!@$^()+={}[]|':<>.~`";   
if(name.indexOf('"') != -1){return false;}
for(var i=0; i<specialChar.length; i++){  
var str = (name).indexOf(specialChar.charAt(i));
if(str != -1) {return false;}}return true;
}
function $$(docid){return document.getElementById(docid);}
// Script that are related to the navigation part 
 function navlistingToggle(id){
  if(id=='coursechoices'){$$("unichoices").style.display='none';if($$("loginbox") !=null){$$("loginbox").style.display='none';}$$("loglink").className="login";}
  if(id=='unichoices'){$$("coursechoices").style.display='none';if($$("loginbox") !=null){$$("loginbox").style.display='none';}$$("loglink").className="login";}
  if(id=='browsesubnav'){$$("unichoices").style.display='none';$$("coursechoices").style.display='none';if($$("loginbox") !=null){$$("loginbox").style.display='none';}$$("loglink").className="login";}
		if(id=='loginbox'){
			$$("unichoices").style.display='none';
			$$("coursechoices").style.display='none';
			$$("loglink").className=$$("loglink").className+' login_hghlt';
		 }
		if($$(id).style.display=='none'){$$(id).style.display='block'
		}else{$$(id).style.display='none'; $$("loglink").className="login";}
	}
 function hideShowSubNav(dowhat){
   if(dowhat=='show'){$$("browsesubnav").style.display='block'; $$("browselnk_p").className = "sub_list grads sub_over"; $$("browselnk").className = "grads";}
   if(dowhat=='hide'){$$("browsesubnav").style.display='none'; $$("browselnk_p").className = "sub_list"; $$("browselnk").className = "";}
}
function validateLogin(){
   var email =  trimString( $$("user_emailid").value );
   var password = trimString( $$("user_password").value );
   if(!isEmailAddressValid(email)){$$("user_emailid").focus(); return false;}
   if(password==''){alert("Oops - please enter a valid password");$$("user_password").focus(); return false;}
   var url="/degrees/studentinfo.html?email="+ email + "&pass="+password;
   $$("loadinggif").style.display="block";
   var ajax=new sack();
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ showInfo(ajax, true); };	
   ajax.runAJAX();
   return false;
}

function loginToSystem(email, password){
   var url="/degrees/studentinfo.html?email="+ email + "&pass="+password;
   var ajax=new sack();
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ showInfo(ajax, false); };	
   ajax.runAJAX();
   return false;
}

function showInfo(ajax, refresh){
    var response = ajax.response;
    var dataArray  = response.split('BREAKBREAK');
    if(dataArray.length > 0 && dataArray[0] == 'loginfail'){
      $$('loadinggif').style.display='none';
      $$('errormessagefp').style.display='block';
      $$('errormessagefp').innerHTML = dataArray[1];
    }else{
       $$('loglink').style.display='none';
       if($$('loginbox') !=null){ $$('loginbox').style.display='none';}   
       if($$('logoutlink') !=null){ $$('logoutlink').style.display='block';}
      // alert(dataArray[0]);
       $$('usermsg').innerHTML = dataArray[0];
       //$$('usermsg').setAttribute("class", "user")
       $$('usermsg').className = "user";
       $$('unilist').innerHTML =dataArray[1];
       $$('sp_college_count').innerHTML ='('+dataArray[2]+' saved)';
       $$('courselist').innerHTML =dataArray[3];
       $$('sp_course_count').innerHTML ='('+dataArray[4]+' saved)';
       if(refresh){
         // window.location.href=replaceAll(window.location.href,"/degrees/registrationQuestion.html","/degrees/home.html");
          window.location.href=replaceAll(window.location.href,"/userLogin.html?e=logout","/home.html");
      }    
   }
  return false;
 }
function TogglePod(id, style){
  if(style == 'none'){
     $$(id).style.display='none';
   }else{  
     $$(id).style.display='block';
   }  
}