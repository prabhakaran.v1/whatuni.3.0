var adv = jQuery.noConflict();
adv(window).scroll(function(){ // scroll event 
  var windowTop = adv(window).scrollTop(); // returns number  
  var docHeight = adv(document).height();
  var diff = docHeight - 1200;
  var width = document.documentElement.clientWidth;
if (width > 992) {            
  x=adv("#clearAdv").position();      
  if (150 < windowTop){
    adv('#stickyTop').addClass('sticky_top_nav');        
    adv('#stickyTop').css({ position: 'fixed', top: 0 });
  }
  else {
    adv('#stickyTop').removeClass('sticky_top_nav');
    adv('#stickyTop').css('position','static');  
  } 
  }
  if($$('viewportspamboxDiv')){//Added for 1-sep-2015 release by priyaa - lazy load pod
   loadLazyLoadPod('viewportspamboxDiv ',true, 'loadNewLetterPodOnScroll()');
   }
   //Added code for lazy load trending and most popular advice pod for 03_NOV_2015 release by Prabha  
   if($$('viewportTrendingPodDiv')){
     loadLazyLoadPod('viewportTrendingPodDiv ',true, 'loadTrendingAndMostPopularPod("TRENDING","")');
   }
   if($$('viewportMostPulrPodDiv')){
     loadLazyLoadPod('viewportMostPulrPodDiv ',true, 'loadTrendingAndMostPopularPod("MOST","CONTENT_PAGE_MOST_POPULAR_POD_TOP")');
   }
   if($$('mostArticlesPodButtom')){
     loadLazyLoadPod('mostArticlesPodButtom ',true, 'loadTrendingAndMostPopularPod("MOST","CONTENT_PAGE_MOST_POPULAR_POD_BOTTOM")');
   }
   lazyloadetStarts();    
}); 
var flag=false;
adv(document).ready(function(){    
  adv("#hd1").removeAttr("style");
  lazyloadetStarts();
  adv('.lft_nav .brw').click(function(){
    adv(this).toggleClass('act');
    adv(this).parents().next().toggleClass('act');  
  });
  adv('.srh').click(function(){
    adv(this).toggleClass('act');
    adv('.top_search').toggleClass('act')
  });
  adv('.ic-mob-close').click(function(){
    adv(this).parents().find('.act').removeClass('act');
  });
  //Youtube video auto play on 03_Oct_2017, By Thiyagu G
  var i=1;
  adv("iframe").each(function(){
    if(adv(this).attr("src") != '' && adv(this).attr("src") != undefined){      
      var iframSrc = adv(this).attr("src");      
      if(iframSrc!=undefined && iframSrc.indexOf("//www.youtube.com/embed/") > -1){
        adv(this).attr("id", "player"+i);
        adv(this).attr("class", "yt_players");        
        var iframSrcPram;var symbol;
        if(iframSrc.indexOf("?") > -1){symbol = "&";}else{symbol = "?";}
        iframSrcPram = iframSrc+symbol+"rel=1&wmode=1paque&enablejsapi=1;showinfo=1;controls=1";        
        adv(this).attr("src", iframSrcPram);
        flag=true;i++;
      }
    }				
  });  
});
function $$(id){return document.getElementById(id);}
function adviceSearchSubmit(pageNo, pageName){     
    var searchKeyword = trimString($$("keyword").value);
    $$("page").value = pageNo;
    $$("pageName").value = pageName;    
    if(searchKeyword != "" && searchKeyword != "Enter keyword"){        
        //var finaUrl = "/degrees/article-search.html?keyword="+searchKeyword+"&page="+pageNo;
        //var finaUrl = "/degrees/article-search.html";
        var finaUrl = "/article-search/";
        $$("articleSearchForm").action = finaUrl.toLowerCase();
	$$("articleSearchForm").submit();
    }else{
        alert("Please enter a valid keyword.");
        $$('keyword').value = '';
        $$('keyword').focus();
        return false;
    }
    return true;
}
function paginationSubmit(pageName, pageNo){
    $$("pageName").value = pageName;
    $$("page").value = pageNo;      
    if(pageName == "adviceSearchResults"){
        adviceSearchSubmit(pageNo, pageName);
    }else if(pageName == "primaryCat"){
        var primaryCategory = $$("primaryCategory").value;          
        if(primaryCategory != ""){
            var finaUrl = "/advice/"+primaryCategory+"?page="+pageNo;
            $$("primaryCatForm").action = finaUrl.toLowerCase();
            $$("primaryCatForm").submit();
        }    
    } else if(pageName == "secondaryCat"){
        var primaryCategory = $$("primaryCategory").value;
        var secondaryCategory = $$("secondaryCategory").value;    
        if(primaryCategory != "" && secondaryCategory != ""){        
            var finaUrl = "/advice/"+primaryCategory+"/"+secondaryCategory+"?page="+pageNo;
            $$("secondaryCatForm").action = finaUrl.toLowerCase();
            $$("secondaryCatForm").submit();
        }    
    }    
}
function setSelectedVal(obj, spanid){
  var dobid = obj.id
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  document.getElementById(spanid).innerHTML = text;
  location.href = (obj.options[obj.selectedIndex].value).trim();
  //location.href = "http://192.168.1.97:8988"+(obj.options[obj.selectedIndex].value).trim();  
}
function keypressSubmit(pageNo, pageName, event){
    if(event.keyCode==13){
        var searchKeyword = trimString($$("keyword").value);
        if(searchKeyword != "" && searchKeyword != "Enter keyword"){            
                return adviceSearchSubmit(pageNo, pageName);            
        }else{
            alert("Please enter a valid keyword.");
            $$('keyword').value = '';
            $$('keyword').focus();
            return false;
        }    
    }
}
function showhide(a,b,videoPath,width, height,coverimage, stdVideoPath){
  dynamicJsLoad(document.getElementById("contextJsPath").value+'/js/video/jwplayer.js',a,b,videoPath,width, height,coverimage, stdVideoPath);
}
function dynamicJsLoad(src,a, b,videoPath,width, height, coverimage,stdVideoPath){
  var script = document.createElement("script");
  script.type = "text/javascript";  
  script.src = src;
  if(script.readyState){//IE
    script.onreadystatechange = function(){
    if (script.readyState == "loaded" || script.readyState == "complete"){
      script.onreadystatechange = null;
      loadJwPlayer(a,b,videoPath,width, height,coverimage);
        if(a=='video_emb'){      
        loadStdChoiceJwplayer(stdVideoPath);
       }
     }
    };
    } else {
      script.onload = function(){
       loadJwPlayer(a,b,videoPath,width, height,coverimage);
       if(a=='video_emb'){      
        loadStdChoiceJwplayer(stdVideoPath);
       }
      };
    }
  if (typeof script != "undefined"){
    document.getElementsByTagName("head")[0].appendChild(script);
  }
}
/*function onYouTubeIframeAPIReady() { //Youtube video auto play on 03_Oct_2017, By Thiyagu G
  if(flag){
    var videos = document.getElementsByTagName('iframe'),
      players = [],
      playingID = null;
    for (var i = 0; i < videos.length; i++) {
      var currentIframeID = videos[i].id;
      if(currentIframeID != "" && currentIframeID != undefined){
        players[currentIframeID] = new YT.Player(currentIframeID);
      }
      videos[i].onmouseover = function(e) {
        var currentHoveredElement = e.target;
        if (playingID) {
          players[playingID].pauseVideo();
        }
        players[currentHoveredElement.id].playVideo();
        playingID = currentHoveredElement.id;
      };
    }
  }
}*/
var players = new Array();
		function onYouTubeIframeAPIReady() {
			var temp = adv("iframe.yt_players");
			for (var i = 0; i < temp.length; i++) {
				var t = new YT.Player(adv(temp[i]).attr('id'), {
					events: {
						'onStateChange': onPlayerStateChange
					}
				});
				players.push(t);
			}
		}
		function onPlayerStateChange(event) {
			if (event.data == YT.PlayerState.PLAYING) {
				var temp = event.target.a.src;
    var tempId = event.target.a.id;
				var tempPlayers = adv("iframe.yt_players");
				for (var i = 0; i < players.length; i++) {
					if (players[i].a.id != tempId){ players[i].pauseVideo();}
				}
			}
		}