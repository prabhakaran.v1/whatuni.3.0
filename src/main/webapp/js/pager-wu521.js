var ul;
var uld;
var liItems;
var imageNumber;
var imageWidth;
var prev, next;
var currentPostion = 0;
var currentpage = 0;
var currentslide =0;
var currentpage_array = new Array();
var currentslide_array = new Array(); 

function init(){
	//ul = document.getElementById('upperPart');
	//uld = document.getElementById('downPart');
	//liItems = ul.children;
	//imageNumber = liItems.length;
	imageWidth = 146;
	//ul.style.width = parseInt(imageWidth * imageNumber) + 'px';
}

function arrayassign(cnt){
for(var i=0;i<cnt;i++)
{
currentpage_array[i]= 0;
currentslide_array[i]=0;
}
}

function animate(opts){
	var start = new Date;
	var id = setInterval(function(){
		var timePassed = new Date - start;
		var progress = timePassed / opts.duration;
		if (progress > 1){
			progress = 1;
		}
		var delta = opts.delta(progress);
		opts.step(delta);
		if (progress == 1){
			clearInterval(id);
			opts.callback();
		}
	}, opts.delay || 17);
	//return id;
}

function slideTo(slideToGo,chk){
	var direction;
	if (chk =='p'){ current= slideToGo+1;}
	else{ current = slideToGo-1;}
	var numOfslideToGo = Math.abs(slideToGo - current);
	// slide toward left
	direction = currentslide > slideToGo? 1 : -1;
	currentPostion = -1 * current * imageWidth ;
	var opts = {
		duration:200,
		delta:function(p){return p;},
		step:function(delta){
			ul.style.left = parseInt(currentPostion + direction * delta * imageWidth * numOfslideToGo) + 'px';
			uld.style.left = parseInt(currentPostion + direction * delta * imageWidth  * numOfslideToGo) + 'px';
		},
		callback:function(){currentslide = slideToGo;}	
};
	animate(opts);
	}


/*function onClickPrev(count){
	if (currentslide != 0){
if(currentslide == 1) { 
  document.getElementById("fprev").style.display="none";
  document.getElementById("fnext").style.display="block";
  }else{
  document.getElementById("fprev").style.display="block";
  document.getElementById("fnext").style.display="block";
  }
slideTo(currentslide - 1,'p');
document.getElementById("pge").innerHTML=currentslide+'-'+(currentslide+2)+ ' of '+count;
}
}

function onClickNext(count){
	if (currentslide < imageNumber-3){
	if(currentslide == count-4) { 
    document.getElementById('fnext').style.display="none";
    document.getElementById('fprev').style.display="block";
  }else{
    document.getElementById('fprev').style.display="block";
    document.getElementById('fnext').style.display="block";
  }
		slideTo(currentslide + 1,'n');
		document.getElementById("pge").innerHTML=(currentslide+2)+'-'+(currentslide+4)+ ' of '+count;
	}				
}*/

function slideToFun(mainid,slideToGo,chk){
	var direction;
	var current;
	if (chk =='p'){ current= slideToGo+1;}
	else{ current = slideToGo-1;}
	//alert(current);
	var numOfslideToGo = Math.abs(slideToGo - current);
	// slide toward left
	direction = current > slideToGo ? 1 : -1;
	currentPos = -1 * current * imageWidth;
	var opts = {
		duration:200,
		delta:function(p){return p;},
		step:function(delta){
			document.getElementById("upperPart_"+mainid).style.left = parseInt(currentPos + direction * delta * imageWidth * numOfslideToGo ) + 'px';
			document.getElementById("downPart_"+mainid).style.left = parseInt(currentPos + direction * delta * imageWidth * numOfslideToGo ) + 'px';
		},
		callback:function(){currentslide_array[mainid-1] = slideToGo;}	
	};
	animate(opts);
}

function onClickPrev(mainid,count){
 if(currentslide_array[mainid-1]!=0){
   if(currentslide_array[mainid-1] == 1)
  { 
  document.getElementById("fprev_"+mainid).style.display="none";
  document.getElementById("fnext_"+mainid).style.display="block";
  }else{
  document.getElementById("fprev_"+mainid).style.display="block";
  document.getElementById("fnext_"+mainid).style.display="block";
  }
  slideToFun(mainid,currentslide_array[mainid-1]- 1,'p');
  document.getElementById("pge_"+mainid).innerHTML=(currentslide_array[mainid-1])+'-'+(currentslide_array[mainid-1]+4)+ ' of '+count;
 }
}

function onClickNext(mainid,count){
 if(currentslide_array[mainid-1] <count-5){
  if(currentslide_array[mainid-1] == count-6)
  { 
  document.getElementById("fnext_"+mainid).style.display="none";
  document.getElementById("fprev_"+mainid).style.display="block";
  }else{
  document.getElementById("fnext_"+mainid).style.display="block";
  document.getElementById("fprev_"+mainid).style.display="block";
  }
  slideToFun(mainid,currentslide_array[mainid-1] + 1,'n');
  document.getElementById("pge_"+mainid).innerHTML=(currentslide_array[mainid-1]+2)+'-'+(currentslide_array[mainid-1]+6)+ ' of '+count;
 } 
}
