var contextPath = "/degrees";
//
var opdup = jQuery.noConflict();

opdup(document).ready(function() {
  opdup("#hd1").removeAttr("style");
  opdup('.lft_nav .brw').click(function() {
    opdup(this).toggleClass('act');
    opdup(this).parents().next().toggleClass('act');
  });
  opdup(".select_box_month").click(function() {
    opdup("#select_cont").slideToggle();
  });
  opdup(".op_mth_slider ul li a").click(function() {
    opdup(".select_box_month .sld_mth ").html(opdup(this).html());
    opdup("#select_cont").slideToggle();
  });

  var origin = window.location.origin;
  var pathName = window.location.pathname;
  pathName = pathName.replace('/degrees', '').replace('.html', '');
  var iframeUrl = origin + '/university-reviews' + pathName;
  var iframeSrc = opdup('#iframeId').attr('src');
  if (iframeUrl != iframeSrc) {
    opdup('#iframeId').attr('src', iframeUrl);
  }
  function handleMessage(event) {
    if (event.data != null && event.data != undefined) {
      var iframeElement = opdup('#iframeId');
      if (iframeElement != '' && iframeElement != undefined) {
         var height = 'height:' + event.data[0] + 'px !important';
        iframeElement.attr('style', height);
        iframeElement.width(event.data[1]);
        opdup('#loaderImg').css('display', 'none');
      }
    }
  }
  if (window.addEventListener) {
    window.addEventListener('message', handleMessage, false);
  } else {
    window.attachEvent('onmessage', handleMessage);
  }
});