$(document).ready(function () {
		var fullPageCreated = false;
  function createFullpage() {
				if (fullPageCreated === false) {
					 fullPageCreated = true;
					   $('#fullpage').fullpage({
						    navigation: true,
						    navigationPosition: 'right',
						    responsive: 767,
						    'onLeave': function (index, nextIndex, direction) {
							     if(index== 1 && direction =="down"){
								      $('#slide2 .col-lg-4').addClass('animated fadeInUp');
							     } else if(index > 1 && direction =="down"){
								      $('#slide' + index + ' .col-lg-4').removeClass('animated fadeInUp fadeInDown');
								      $('#slide' + (index + 1) + ' .col-lg-4').addClass('animated fadeInUp');
							     } else if(index == 2 && direction =="up"){
								      $('#slide' + index + ' .col-lg-4').removeClass('animated fadeInDown fadeInUp');
								      $('#slide' + (index - 1) + ' .col-lg-4').addClass('animated fadeInDown');
							     } else if(index > 2 && direction =="up"){
								      $('#slide' + index + ' .col-lg-4').removeClass('animated fadeInDown fadeInUp');
								      $('#slide' + (index - 1) + ' .col-lg-4').addClass('animated fadeInDown');
							     } 
						    }
					   });
				  }
			 }
			 var alp_hgt = $(window).width();
			 if (alp_hgt >= 768) {
				  createFullpage();
			 }
    $(".menu-icon").click(function () {
				  $(".resp-mnu").toggleClass("mnu_act");
				  $(".menu-icon").toggleClass("open");
				  $("#fp-nav").toggleClass("nav_pos");
    });
});
function validateALPSignup(action){
  var message = true;
  var firstName = encodeURIComponent(trimString($$D("firstName").value)); 
  var lastName = encodeURIComponent(trimString($$D("lastName").value));    
  var emailAddress = trimString($$D("email").value);
  if(isEmpty(firstName)){
    //alert("We still don't know your name. Remind us?");
    message = false;
  }else{
    $('#firstName').css("color","#546E7A");
  } 
  if(isEmpty(lastName)){
    //alert("We still don't know your last name. Remind us?");
    message = false;
  }else{
    $('#lastName').css("color","#546E7A");
  }
  if(isEmpty(emailAddress)){
    //alert("Something seems to be missing from your email. Awkward.");
    message = false;
  }else if(!checkValidEmail($$D("email").value)){
    //alert("Something seems to be missing from your email. Awkward.");
    message = false;
    $('#email').css("color","#546E7A");
  }
  if(!message){
    $('#signUpLink').addClass('btn_dis');
  }else{
    $('#signUpLink').removeClass('btn_dis');
  }
  if(action == 'SUBMIT' && message){
    $('#lodingImg').show();
    $('#signUpLink').hide();
    var url= context_path + "/whatuni-mobile-app-ajax.html?actionFlag=SIGNUP&firstName=" + firstName + "&lastName=" + lastName + "&emailAddress=" + trimString(emailAddress);
    var ajax=new sack();
    ajax.requestFile = url;	
    ajax.onCompletion = function(){ showALPResponseMsg(ajax); };	
    ajax.runAJAX();
    return false;
  }
}
function showALPResponseMsg(ajax){
  var responseArr = new Array(); 
  responseArr = ajax.response.split("##SPLIT##");
  setTimeout(function(){
  if(responseArr[0] != "ERROR"){
    $('#lodingImg').hide();
    $('#successPod').show();
    $('#signupForm').hide();
    if(responseArr[0] == 'DUPLICATE_EMAIL'){
      $('#alreadyReg').show();
      $('#successMsg').hide();
    }else{
      $('#alreadyReg').hide();
      $('#successMsg').show();
    }
  }
  }, 1000);
}