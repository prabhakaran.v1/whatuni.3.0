var contextPath = "/degrees";
var jq = jQuery.noConflict();  
jq(document).ready(function () {
		var fullPageCreated = false;
  function createFullpage() {
				if (fullPageCreated === false) {
					 fullPageCreated = true;
					   jq('#fullpage').fullpage({
						    navigation: true,
						    navigationPosition: 'right',
						    responsive: 767,
						    'onLeave': function (index, nextIndex, direction) {
							     if(index== 1 && direction =="down"){
								      jq('#slide2 .col-lg-4').addClass('animated fadeInUp');
							     } else if(index > 1 && direction =="down"){
								      jq('#slide' + index + ' .col-lg-4').removeClass('animated fadeInUp fadeInDown');
								      jq('#slide' + (index + 1) + ' .col-lg-4').addClass('animated fadeInUp');
							     } else if(index == 2 && direction =="up"){
								      jq('#slide' + index + ' .col-lg-4').removeClass('animated fadeInDown fadeInUp');
								      jq('#slide' + (index - 1) + ' .col-lg-4').addClass('animated fadeInDown');
							     } else if(index > 2 && direction =="up"){
								      jq('#slide' + index + ' .col-lg-4').removeClass('animated fadeInDown fadeInUp');
								      jq('#slide' + (index - 1) + ' .col-lg-4').addClass('animated fadeInDown');
							     } 
						    }
					   });
				  }
			 }
			 var alp_hgt = jq(window).width();
			 if (alp_hgt >= 768) {
				  createFullpage();
			 }
    jq(".menu-icon").click(function () {
				  jq(".resp-mnu").toggleClass("mnu_act");
				  jq(".menu-icon").toggleClass("open");
				  jq("#fp-nav").toggleClass("nav_pos");
    }); 
    
    	jq( document ).on("click", "#changeSettingsLightBox", function(event) {	
    		jq("#newCookieSettings").show();
    	});
    	
    	jq( document ).on("click", "#saveCookieButton", function(event) {
    	  cookieSaveFn();
    	  jq("#newCookieSettings").hide();
    	  jq("html").addClass("rvscrl_hid");
    	});
    	
    	jq( document ).on("click", "#cookiesList li", function(event) {
    		var id = jq(this).attr('id');
    		if (id == "functionalCookies"){
    			jq("#functionalSection").show();
    			jq("#yourPrivacySection, #strictlyNecessarySection, #performanceSection, #targetingSection").hide();
    			jq("#functionalCookies_a").addClass("activlnk");
    			jq("#strictlyNecessaryCookies_a, #yourPrivacy_a, #performanceCookies_a, #targetingCookies_a").removeClass("activlnk");
    		} else if (id == "strictlyNecessaryCookies") {
    			jq("#strictlyNecessarySection").show();
    			jq("#yourPrivacySection, #functionalSection, #performanceSection, #targetingSection").hide();
    			jq("#strictlyNecessaryCookies_a").addClass("activlnk");
    			jq("#functionalCookies_a, #yourPrivacy_a, #performanceCookies_a, #targetingCookies_a").removeClass("activlnk");
    		} else if (id == "yourPrivacy") {
    			jq("#yourPrivacySection").show();
    			jq("#functionalSection, #strictlyNecessarySection, #performanceSection, #targetingSection").hide();
    			jq("#yourPrivacy_a").addClass("activlnk");
    			jq("#functionalCookies_a, #strictlyNecessaryCookies_a, #performanceCookies_a, #targetingCookies_a").removeClass("activlnk");
    		} else if (id == "performanceCookies") {
    			jq("#performanceSection").show();
    			jq("#yourPrivacySection, #strictlyNecessarySection, #functionalSection, #targetingSection").hide();
    			jq("#performanceCookies_a").addClass("activlnk");
    			jq("#functionalCookies_a, #strictlyNecessaryCookies_a, #yourPrivacy_a, #targetingCookies_a").removeClass("activlnk");
    		} else if (id == "targetingCookies") {
    			jq("#targetingSection").show();
    			jq("#yourPrivacySection, #strictlyNecessarySection, #performanceSection, #functionalSection").hide();
    			jq("#targetingCookies_a").addClass("activlnk");
    			jq("#functionalCookies_a, #strictlyNecessaryCookies_a, #yourPrivacy_a, #performanceCookies_a").removeClass("activlnk");
    		}
    	});    	
   
});
function validateALPSignup(action){
  var message = true;
  var firstName = encodeURIComponent(trimString($$D("firstName").value)); 
  var lastName = encodeURIComponent(trimString($$D("lastName").value));    
  var emailAddress = encodeURIComponent(trimString($$D("email").value));
  if(isEmpty(firstName)){
    //alert("We still don't know your name. Remind us?");
    message = false;
  }else{
    jq('#firstName').css("color","#546E7A");
  } 
  if(isEmpty(lastName)){
    //alert("We still don't know your last name. Remind us?");
    message = false;
  }else{
    jq('#lastName').css("color","#546E7A");
  }
  if(isEmpty(emailAddress)){
    //alert("Something seems to be missing from your email. Awkward.");
    message = false;
  }else if(!checkValidEmail($$D("email").value)){
    //alert("Something seems to be missing from your email. Awkward.");
    message = false;
    jq('#email').css("color","#546E7A");
  }
  if(!message){
    jq('#signUpLink').addClass('btn_dis');
  }else{
    jq('#signUpLink').removeClass('btn_dis');
  }
  if(action == 'SUBMIT' && message){
    jq('#lodingImg').show();
    jq('#signUpLink').hide();
    var url= context_path + "/whatuni-mobile-app-ajax.html?actionFlag=SIGNUP&firstName=" + firstName + "&lastName=" + lastName + "&emailAddress=" + trimString(emailAddress);
    var ajax=new sack();
    ajax.requestFile = url;	
    ajax.onCompletion = function(){ showALPResponseMsg(ajax); };	
    ajax.runAJAX();
    return false;
  }
}
function showALPResponseMsg(ajax){
  var responseArr = new Array(); 
  responseArr = ajax.response.split("##SPLIT##");
  setTimeout(function(){
  if(responseArr[0] != "ERROR"){
    jq('#lodingImg').hide();
    jq('#successPod').show();
    jq('#signupForm').hide();
    if(responseArr[0] == 'DUPLICATE_EMAIL'){
      jq('#alreadyReg').show();
      jq('#successMsg').hide();
    }else{
      jq('#alreadyReg').hide();
      jq('#successMsg').show();
    }
  }
  }, 1000);
}
//added cookie ajax method for header cookie policy pop-up by Hema.S on 09.07.2018

function createHeaderCookie(cookieName,cookieValue){
  var url = contextPath+"/cookies/create-cookie-popup.html?cookieName="+cookieName+"&cookieValue="+cookieValue;      
  var ajaxObj = new sack();
  ajaxObj.requestFile = url;	  
  ajaxObj.onCompletion = function(){setCookieLifeTime(ajaxObj);};	
  ajaxObj.runAJAX();  
}
function setCookieLifeTime(ajaxObj){
  if(ajaxObj.response == 'SUCEESS'){
   // blockNone('cookiePopup', 'none');
    jq(".cookins").slideUp(500);     
    jq('body').addClass("padrt");
    window.location.reload(true);
     //jq("body").animate({"padding-top": "0"});
  }
}
//added for cookie policy popup by Hema.S on 31.07.2018_rel
function getCookie(c_name){
    var c_value = document.cookie;                
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1){
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1){
        c_value = null;
    }
    else{
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1){
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start,c_end));
    }
    return c_value;
}
function checkcookie(){
  manageCookieData('cookie_splash_flag', 'get');
}
function manageCookieData(cookieName, processType){
  var url = contextPath+"/cookies/create-cookie-popup.html?cookieName="+cookieName+"&processType="+processType;      
  var ajaxObj = new sack();
  ajaxObj.requestFile = url;	  
  ajaxObj.onCompletion = function(){manageCookieDataResponse(ajaxObj, cookieName);};	
  ajaxObj.runAJAX();
}
function manageCookieDataResponse(ajaxObj,cookieName){
  if('cookie_splash_flag' == cookieName){
    if(ajaxObj.response == "NO"){      
      document.getElementById("cookiePopup").style.display = "none";
      document.body.classList.remove('spactp');
    }else if(ajaxObj.response == "YES") {
      document.body.classList.add('spactp');
    }
  }
}
function cookieSaveFn(){
	var strictCk = "0";
	var functCk = jq('#functCkId').is(":checked") ? "0" : "1";
	var perfCk = jq('#perfCkId').is(":checked") ? "0" : "1";
	var targetCk = jq('#targetCkId').is(":checked") ? "0" : "1";	
	var consentCkVal = strictCk + functCk + perfCk + targetCk;
	createJavaSideSecureCookie('cookieconsent',consentCkVal);
}
function createJavaSideSecureCookie(cookieName, cookieValue) {
 	var ajxurl = contextPath+"/create-cookies.html?cookieName="+cookieName+"&cookieValue="+cookieValue;    
 	jq.ajax({
		url: ajxurl,
		type : "POST",
		success: function(result) {
			  window.location.reload(true);
		}
	});	
 }