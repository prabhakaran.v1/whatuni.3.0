var contextPath = "/degrees";
$(document).ready(function () {
			$(".menu-icon").click(function () {
			 $(".resp-mnu").toggleClass("mnu_act");
			 $("body,html").toggleClass("scrl_dis");
    $(".menu-icon").toggleClass("open");
			 $("#fp-nav").toggleClass("nav_pos");
		 });            
   function set_hght(){
    var ma_hght = $(window).height();
    $("#get_hght").css('height',ma_hght);
   }
   set_hght();
   $(window).resize(function(){
     set_hght();     
   });    
});
function $$(id){return document.getElementById(id);}
function submitAppContactUsPage(action){
   var firstName = encodeURIComponent(trimString($$("firstName").value));
   var lastName = encodeURIComponent(trimString($$("lastName").value));
   var emailAddress = trimString($$("emailAddress").value);
   var message = trimString($$("message").value);
   var errFlag = true;
   if(isEmpty(firstName)){
       errFlag = false;
   }
   if(isEmpty(lastName)){
      errFlag = false;
   }
   if(isEmpty(emailAddress)){
      errFlag = false;
   }else if(!checkValidEmail(trimString($$("emailAddress").value))){
      errFlag = false;
   }
   if(isEmpty(message)){
     errFlag = false;
   }else{
     var htmlTags = containsHtmlTags($$("message"));
     var invalidWord = containsSpecialCharacter($$("message"));
     if((invalidWord != null && trimString(invalidWord) != "") || (htmlTags != null && trimString(htmlTags) != "")){
       errFlag = false;       
     }
   }   
   if(!errFlag){
     $('#sendMailLink').addClass('btn_dis');
   }else{
     $('#sendMailLink').removeClass('btn_dis');
   }
   if(action == 'SUBMIT' && errFlag){
    $('#sendMailLink').hide();
    $('#lodingImg').show();    
    var url= context_path + "/whatuni-mobile-app/contact-us.html?actionFlag=SENDMAIL&firstName="+firstName+"&lastName="+lastName+"&emailAddress="+emailAddress+"&message="+message;
    var ajax=new sack();
    ajax.requestFile = url;	
    ajax.onCompletion = function(){ showContactUsResponse(ajax); };	
    ajax.runAJAX();
    return false;
   }
}
function showContactUsResponse(ajax){
  $('#lodingImg').hide();
  $('#appcontactus').hide();  
  $("#emailSuccessMsg").html(ajax.response);
  $('#emailSuccessMsg').show();
  $('#emailSuccessBtn').show();  
}
function checkValidEmail(email){
  if(/^\w+([-+.'']\w+)*@\w+([-._]\w+)*\.\w+([-._]\w+)*$/.test(trimString(email))){
    return true
  }
  return false
}
function containsSpecialCharacter(inputObj){
if(inputObj != null){
		var inputText = inputObj.value;
		if(vulnerableKeywords != null){
			for(var i= 0; i< vulnerableKeywords.length; i++){  
				var inputIndex = (inputText.toLowerCase()).indexOf(vulnerableKeywords[i]);
				if(inputIndex != -1) {
					return vulnerableKeywords[i];
				}
			}
		}
	}
	return "";
}
function containsHtmlTags(inputObj){
  if(inputObj != null){
		  var inputText = inputObj.value;
		  if(htmlTags != null){
			   for(var i= 0; i< htmlTags.length; i++){  
				    var inputIndex = (inputText.toLowerCase()).indexOf(htmlTags[i]);
				    if(inputIndex != -1) {
					     return htmlTags[i];
				    }
			   }
		  }
	 }
	 return "";
}