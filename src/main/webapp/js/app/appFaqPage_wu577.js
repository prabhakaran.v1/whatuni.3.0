$(document).ready(function () {
			$(".menu-icon").click(function () {
				$(".resp-mnu").toggleClass("mnu_act");
				$("body,html").toggleClass("scrl_dis");
    $(".menu-icon").toggleClass("open");
				$("#fp-nav").toggleClass("nav_pos");
			});            
   function set_hght(){
    var ma_hght = $(window).height();
    $("#get_hght").css('height',ma_hght);
   }
   set_hght();
   $(window).resize(function(){
       set_hght();     
   });
   $(".nav-pills li a, #dtn_skplnk").click(function(evn){
      evn.preventDefault();
      $('html,body').scrollTo(this.hash, this.hash); 
   });
   var aChildren = $(".nav-pills li").children(); // find the a children of the list items
   var aArray = []; // create the empty aArray
   for (var i=0; i < aChildren.length; i++) {    
       var aChild = aChildren[i];
       var ahref = $(aChild).attr('href');
       aArray.push(ahref);
   }     
  //Back to top
   $(window).scroll(function(){
     var faqhght = $(window).height();
     var scrval = $(window).scrollTop();
     if(faqhght < scrval){
       $("#back-top").css("display","block"); 
     }else{$("#back-top").css("display","none");}
  });   
   $("#back-top a").click(function() {
     $("body,html").animate({scrollTop: 0}, 700);
    return false;
   });
});