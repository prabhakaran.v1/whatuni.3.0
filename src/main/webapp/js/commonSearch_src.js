var subKwd = "Keyword or UCAS code"
var subQual = 'Select qualification'
var subUni = 'Enter uni name'
var subUni1 = 'Enter uni name here'
var opdUniLoc = 'Enter uni name'
var opdUniLoc1 = 'Enter uni name or location here'
var stdAwdUni  = "Enter university name";
var emptyKwd = ' Search error.\n Please select a subject or uni from the drop down.'
var emptyUni = 'Please select a uni from the drop down or browse using our A-Z list.'
var emptyUni1 = 'Please enter a valid institution name';
var screenWidth = document.documentElement.clientWidth;
var navQualNetworkId = "";
var contextPath = "/degrees";
var ucasDef = 'Please enter UCAS code';
var adv = jQuery.noConflict();
//
function $$(id){return document.getElementById(id)}
function $$D(docid){ return document.getElementById(docid); }
function getDeviceWidth() { return (window.innerWidth > 0) ? window.innerWidth : screen.width; }

function isNotNullAndUndex(value) {
  return (value != '' && value != undefined && value != null && value != 'null');
}

function isBlankOrNullString(str) {
  var flag = true;
  if(str.trim() != "" && str.trim().length > 0) {
    flag = false;
  }
  return flag;
}
//
function autoCompleteResUniNAME(e,o){
	ajax_showOptions(o,'z',e,"navindicator",'z=','0');
}
//
function autoCompleteUniNAME(e,o,pagename){
  var extraParam = 'z=';
  var autoCompletType = 'z';
  var width = '382';
  var uniAjxTpFlg = false;
  //alert(ajax_getWidth(o));
  if('REVIEW_PAGE'==pagename){
    autoCompletType = 'reviewUniSearch';
    extraParam += '&type=REVIEW_PAGE';
    width = ajax_getWidth(o);
  }else if('TOP_NAV_UNI'==pagename){
	autoCompletType = 'topNavUniSearch';
	extraParam += '&type=topNavUni';
	width = ajax_getWidth(o);
	var inputTextVal = adv('#'+o.id).val().trim().replace(/[%_]/g, '');    
    if(isThisValidTextAjaxKeyword(inputTextVal) && inputTextVal.length > 2){
      uniAjxTpFlg = true;      
      ajax_options_hide();
    }
  }  
  if((screenWidth >=320) && (screenWidth <= 992) && (pagename == 'finduni')){     
    ajax_showOptions(o,autoCompletType,e,"navindicator",extraParam,'0');
  }else if('TOP_NAV_UNI'==pagename && uniAjxTpFlg){
	ajax_showOptions(o,autoCompletType,e,"navindicator",extraParam,'0',width);  
  }else if('TOP_NAV_UNI'!=pagename){
    ajax_showOptions(o,autoCompletType,e,"navindicator",extraParam,'0',width);
  }
  //condition for top nav search uni ajax postion alignment
  if('TOP_NAV_UNI'==pagename && uniAjxTpFlg){    
	adv("#ajax_listOfOptions").appendTo("#uniSrchDivId");
	adv("#ajax_listOfOptions").removeAttr("style");
	ajax_optionDiv.style.width = (ajax_getWidth(o)) + 'px';
    if(o.value.length < 3 || (ajax_list_cachedLists[autoCompletType][o.value.toLowerCase()] && ajax_list_cachedLists[autoCompletType][o.value.toLowerCase()].length <= 1)){
      ajax_options_hide();
    }
    var x = event.which || event.keyCode;
    if(x != 13){  
      adv('#errMsgUniSrchlbox').hide();
    }
  }  
  //
}
//
function autoCompleteSubjectName(e,o){
  var width = '382';
  width = ajax_getWidth(o);
  ajax_showOptions(o,'reviewSubjectSearch',e,"navindicator",'z=','0',width);
}
//
function autoCompleteFnchUniName(e,o){   	
  if((screenWidth >=320) && (screenWidth <= 992)) {     
    ajax_showOptions(o,'z',e,"navindicator",'z=','0');
  }else{
    ajax_showOptions(o,'z',e,"navindicator",'z=','0','382');
  }
}

//
function autoCompleteCourseNAME(e,o,uniObj) {
	var	uniId = trimString($$(uniObj+"_hidden").value);   
	var extraParam = 'z=' + uniId;
 if((screenWidth >=320) && (screenWidth <= 992)) {     
  ajax_showOptions(o,'w',e,"course_indicator",extraParam,'0');
 }else{
  ajax_showOptions(o,'w',e,"course_indicator",extraParam,'0','382');
 }
}
//
function autoCompleteUniSearch(e,o){
	ajax_showOptions(o,'z',e,"navindicator",'z=','-221','382');
}

//
function collegeAutoCompleteUNI(e,o){
	ajax_showOptions(o,'z',e,"reviewindicator",'z=');
}
//3_JUN_2014
function collegeSchlShipAutoCompleteUNI(e,o){
 	ajax_showOptions(o,'z',e,"reviewindicator",'type=SCH');
}
//3_JUN_2014
function collegeOpenDayAutoCompleteUNI(e,o){
	 ajax_showOptions(o,'z',e,"search_indicator",'type=OD');
}
//
function collegeAutoCompleteVideo(e,o){
	ajax_showOptions(o,'z',e,"videoimage",'z=');
}
//
function autoCompleteUniCollege(e,o){
	ajax_showOptions(o,'z',e,"shUniIndicator",'z=');
}
//
function autoCompleteUniReview(e,o){
	ajax_showOptions(o,'z',e,"search_indicator",'z=');
}
//
function autoCompleteStudentRating(e,o){
	ajax_showOptions(o,'z',e,"overall_uni_indicator",'z=');
}
//
function autoCompleteCourseUni(e,o){
	ajax_showOptions(o,'z',e,"indicator",'z=');
}
function autoCompleteFindUni(e,o){
	ajax_showOptions(o,'z',e,"subjectIndicatorA",'z=');
}
function autoCompleteSearchUni(e,o){
	ajax_showOptions(o,'z',e,"subjectIndicatorB",'z=');
}
//
function autoCompleteUniInREVIEWS(e,o){
	ajax_showOptions(o,'z',e,"uni_indicator",'z=');
}
//
function autoCompleteCourseInREVIEWS(e,o,uniObj) {
	var	uniId = trimString($$((uniObj.id+"_hidden")).value)
	var extraParam = 'z=' + uniId;
	ajax_showOptions(o,'w',e,"course_indicator",extraParam);
}

//
function autoCompleteCourseInSurvey(e,o,uniObj) {
	var	uniId = trimString($$((uniObj.id+"_hidden")).value)
	var extraParam = 'z=' + uniId;
	ajax_showOptions(o,'coursesearchsurvey',e,"course_indicator",extraParam);
}
//
function autoCompleteCourseInStdREVIEWS(e,o,uniObj) {
	var	uniId = trimString($$((uniObj.id+"_hidden")).value)
	var extraParam = 'z=' + uniId;
	ajax_showOptions(o,'std',e,"course_indicator",extraParam);
}
//
function autoCompleteAddVideoUNI(e,o){
	ajax_showOptions(o,'z',e,"indicator",'z=');
}
//basic form course interested
function autoCompleteCourseInt(event,object){
 var subOrderItemId = $$('subOrderItemId').value;
 var collegeId = $$('collegeId').value;
	ajax_showOptions(object,'courselist',event,"indicator",'subOrderItemId='+ subOrderItemId + '&z=' +collegeId);
}
//
function clearUniNAME(e,o){
	if(e.keyCode != 13 && e.keyCode != 9){
		if($$(o.id+'_hidden') != null){
			$$(o.id+"_hidden").value = "";
		}
		if($$(o.id+'_id') != null){
			$$(o.id+"_id").value = "";
		}
		if($$(o.id+'_display') != null){
			$$(o.id+"_display").value = "";
		}
		if($$(o.id+'_url') != null){
			$$(o.id+"_url").value = "";
		}
		if($$(o.id+'_alias') != null){
			$$(o.id+"_alias").value = "";
}	}	}
//
function setUniDefaultTEXT(o){
  var oVal = $$(o.id).value;
  oVal = replaceAll(oVal, " ", "");
  if(oVal == ''){
    $$(o.id).value = "Enter uni name";
  }	
}
//
function clearUniDefaultTEXT(o, helpTxt){  
  var oVal = trimString($$(o.id).value);
  if(oVal == subUni || oVal == subUni1 || oVal==stdAwdUni || oVal==helpTxt || oVal==ucasDef){
    $$(o.id).value = "";
  }	
}
//
function setOpdUniDefaultTEXT(o){
    var oVal = $$(o.id).value;
    oVal = replaceAll(oVal, " ", "");
    if(oVal == ''){
            $$(o.id).value = "Enter uni name";
    }	
}
//
function clearOpdUniDefaultTEXT(o){
    var oVal = trimString($$(o.id).value);
    if(oVal == opdUniLoc || oVal == opdUniLoc1){
            $$(o.id).value = "";
    }	
}
//Added additional parameter inorder to show the error message in light box - Indumathi wu_546
function redirectUniHOME(o,formObjId,frompage){
  var uniName = trimString(o.value)
  var uniId = trimString($$((o.id+"_hidden")).value)
  var uniNameId = trimString($$((o.id+"_id")).value)
  var uniNameDisplay = trimString($$((o.id+"_display")).value)
  var uniNameAlias = trimString($$((o.id+"_alias")).value)
  var uniNameUrl = trimString($$((o.id+"_url")).value)
  //
  //alert(" 0) redirectUniHOME: \n 1) uniName: " + uniName + "\n 2) uniId: " + uniId+ "\n 3) uniNameId: " + uniNameId+ "\n 4) uniNameDisplay: " + uniNameDisplay + "\n 5) uniNameAlias: " + uniNameAlias + "\n 6) uniNameUrl: " + uniNameUrl)
  //
  if( formObjId == 'uniSrchTpForm'){
	if(uniNameUrl == '' || uniId == ''){
	  adv('#errMsgUniSrchlbox').show();
	  return false;
	}
  }else{
	if(subUni != uniNameUrl || subUni1 != uniNameUrl){
	  if(uniNameUrl == '' || uniId == ''){
	    if(frompage=="finduni"){
	      openLightboxMsg("emptyUni");
	    }else{alert(emptyUni);}
	    resetUniNAME(o);
	    return false;
	  }
	}else{
	  if(frompage=="finduni"){
	    openLightboxMsg("emptyUni");
	  }else{alert(emptyUni);}
	  resetUniNAME(o);
	  return false;
	}
  }
  //
  var isTrackingRequired = "";
  if(document.getElementById("pageFrom")){
    isTrackingRequired = trimString(document.getElementById("pageFrom").value); 
  }
  if(isTrackingRequired !=null && isTrackingRequired !="" && isTrackingRequired =="ChoosingUni"){
    searchEventTracking('finduni','click', uniNameDisplay);
  }
  if( formObjId == 'uniSrchTpForm'){
	if(!adv("#homeHeroImgDivId").is(":visible")){
	  ga('set', 'dimension1', 'topnav');
	}  
    GANewAnalyticsEventsLogging('University Search', 'Search', uniNameDisplay.toLowerCase());
  }
  var seoUniName = replaceHypen(replaceSpecialCharacter(replaceURL(uniNameUrl)));
  var seoUniName = seoUniName.toLowerCase();
  var uniURL = "/university-profile/" + seoUniName + "/" + uniId + "/"; //URL restructure - Added Uni Home URL for 08_Mar_2016 By Thiyagu G.
  //
  //$$("homeSearchBean").action = uniURL;
  //$$("homeSearchBean").submit();
  if($$(formObjId)){
    ($$(formObjId)).action = uniURL;
    ($$(formObjId)).submit();
  }
  return false;
}
//
function resetUniNAME(o){
	if($$((o.id+"_hidden")) != null){$$((o.id+"_hidden")).value = '';}
	if($$((o.id+"_id")) != null){$$((o.id+"_id")).value = '';}
	if($$((o.id+"_display")) != null){$$((o.id+"_display")).value = '';}
	if($$((o.id+"_alias")) != null){$$((o.id+"_alias")).value = '';}
	if($$((o.id+"_url")) != null){$$((o.id+"_url")).value = '';}
}
//
function redirectUniVIDEOS(o,formObjId){
	var uniName = trimString(o.value)
	var	uniId = trimString($$((o.id+"_hidden")).value)
	var	uniNameId = trimString($$((o.id+"_id")).value)
	var	uniNameDisplay = trimString($$((o.id+"_display")).value)
	var	uniNameAlias = trimString($$((o.id+"_alias")).value)
	var	uniNameUrl = trimString($$((o.id+"_url")).value)
	//
	//alert(" 0) redirectUniVIDEOS: \n  1) uniName: " + uniName + "\n 2) uniId: " + uniId+ "\n 3) uniNameId: " + uniNameId+ "\n 4) uniNameDisplay: " + uniNameDisplay + "\n 5) uniNameAlias: " + uniNameAlias + "\n 6) uniNameUrl: " + uniNameUrl);
	//
	if(subUni != uniNameUrl || subUni1 != uniNameUrl){
		if(uniNameUrl == '' || uniId == ''){
			alert(emptyUni);
			resetUniNAME(o);
			return false;
		}
	}else{
		alert(emptyUni);
		resetUniNAME(o);
		return false;
	}
	//
	var seoUniName = replaceHypen(replaceSpecialCharacter(replaceURL(uniNameUrl)));
	var uniVideoURL = 	"/degrees/college-videos/" + seoUniName + "-videos/highest-rated/" + uniId + "/1/student-videos.html";
	uniVideoURL = uniVideoURL.toLowerCase();
	//
	($$(formObjId)).action = uniVideoURL;
	($$(formObjId)).submit();
	return false;
}
//
function redirectUniREVIEWS(o,formObjId){
	var uniName = trimString(o.value)
	var	uniId = trimString($$((o.id+"_hidden")).value)
	var	uniNameId = trimString($$((o.id+"_id")).value)
	var	uniNameDisplay = trimString($$((o.id+"_display")).value)
	var	uniNameAlias = trimString($$((o.id+"_alias")).value)
	var	uniNameUrl = trimString($$((o.id+"_url")).value)
	//
	//alert(" 0) redirectUniREVIEWS: \n  1) uniName: " + uniName + "\n 2) uniId: " + uniId+ "\n 3) uniNameId: " + uniNameId+ "\n 4) uniNameDisplay: " + uniNameDisplay + "\n 5) uniNameAlias: " + uniNameAlias + "\n 6) uniNameUrl: " + uniNameUrl);
	//
	if(subUni != uniNameUrl || subUni1 != uniNameUrl){
		if(uniNameUrl == '' || uniId == ''){
			alert(emptyUni);
			resetUniNAME(o);
			return false;
		}
	}else{
		alert(emptyUni);
		resetUniNAME(o);
		return false;
	}
	//
	var seoUniName = replaceHypen(replaceSpecialCharacter(replaceURL(uniNameUrl)));
	var uniReviewURL = "/degrees/reviews/uni/" + seoUniName + "-reviews/highest-rated/0/0/" + uniId + "/1/studentreviews.html";
	uniReviewURL = uniReviewURL.toLowerCase();
	//
	($$(formObjId)).action = uniReviewURL;
	($$(formObjId)).submit();
	return false;
}
//
function redirectUniSubjectREVIEWS(o,formObjId){
	var uniName = trimString(o.value)
	var	uniId = trimString($$((o.id+"_hidden")).value)
	var	uniNameId = trimString($$((o.id+"_id")).value)
	var	uniNameDisplay = trimString($$((o.id+"_display")).value)
	var	uniNameAlias = trimString($$((o.id+"_alias")).value)
	var	uniNameUrl = trimString($$((o.id+"_url")).value)
	//
	//alert(" 0) redirectUniSubjectREVIEWS: \n  1) uniName: " + uniName + "\n 2) uniId: " + uniId+ "\n 3) uniNameId: " + uniNameId+ "\n 4) uniNameDisplay: " + uniNameDisplay + "\n 5) uniNameAlias: " + uniNameAlias + "\n 6) uniNameUrl: " + uniNameUrl);
	//
	if(subUni != uniNameUrl || subUni1 != uniNameUrl){
		if(uniNameUrl == '' || uniId == ''){
			alert(emptyUni);
			resetUniNAME(o);
			return false;
		}
	}else{
		alert(emptyUni);
		resetUniNAME(o);
		return false;
	}
	//
	var subjectId = ($$('sid').value != null && $$('sid').value != '' ? $$('sid').value : "0");
	var seoUniName = replaceHypen(replaceSpecialCharacter(replaceURL(uniNameUrl)));
	var uniReviewURL = "/degrees/reviews/uni/" + seoUniName + "-reviews/highest-rated/" + "0/" + subjectId + "/" + uniId + "/1/studentreviews.html";
	uniReviewURL = uniReviewURL.toLowerCase();
	//
	($$(formObjId)).action = uniReviewURL;
	($$(formObjId)).submit();
	return false;
}

//
function searchKeywordValidateChar(name,displayMessage){
	var i;var status=true;var specialChar="\\,#%;?/_"
	for(i=0;i<specialChar.length;i++){
		var str=($$(name).value).indexOf(specialChar.charAt(i))
		if(str !=-1){status=false;}
	}
	if(!status){alert(displayMessage+"\n\n# % \ ; ? / _ ");}
	return status
}

//THIS CODE RELATED TO INSTITUTION FINDER ADDED BY ANBU
function orderbyInstitution(formObjId){
  var pageNo = "1";
  var networkId = $$("networkId").value;
  var searchTxt = $$("searchTxt").value;
  var orderBy = $$("orderBy").value;
	 var uniReviewURL = "/degrees/find-university/?nid=" + networkId + "&pageNo=" + pageNo + "&orderBy=" + orderBy + "&searchTxt=" + searchTxt;	//Added for 13-JAN-2014 Release
	 //	 
  location.href=uniReviewURL;
}

function nextInstitutions(navType, startIndex, linkid){
  var displayText = "none";
  var noofrow = $$('noofrow').value;
  var linkobj = $$(linkid);
  if(linkobj.className != 'nvm'){
    for(var i = 0; i <= noofrow; i++){   
      if(navType == 'next' && i >= startIndex){
        displayText = 'block';      
      } else if(navType == 'next' && i < startIndex){
        displayText = 'none';
      }      
      if(navType == 'prev' && i >= startIndex){
        displayText = 'none';
      } else if(navType == 'prev' && i < startIndex){
        displayText = 'block';
      }
      if($$('img_'+i) != null){
        $$('img_'+i).style.display = displayText;
      }    
    }
  
    if(navType == 'next'){      
      $$('prevActive').className ='vm';
      $$('nextActive').className ='nvm';
    }else if(navType == 'prev'){
      $$('prevActive').className ='nvm';
      $$('nextActive').className ='vm';
    }
  }
}

function noSplash()
{  
  var deviceWidth =  getDeviceWidth();
  var url=contextPath + "/newuserregistration.html?method=nosplash" + '&screenwidth='+deviceWidth;      
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){closeLightBox()};
  ajax.runAJAX();      
}

function closeLightBox()
{
  if($$('mbd')) {
    $$('ol').style.display = "none";	
    $$('mbox').style.display = "none";
    $$('mbd').innerHTML = "";  
 }
}
//This function is to set the auto complete off for the first form
function autocompleteOff(element) {
  var frm =  jQuery.noConflict();
  self.focus(); //Bring window to foreground if necessary.
  var form = frm(element); // grab the form element.
  if(form){
    form.setAttribute("autocomplete","off"); // Turn off Autocomplete for the form, if the browser supports it.
  }
}
/*END*/
var profileDropDownFlag = false;
adv(document).ready(function(){    

//Added by Indumathi.S For tab changes in Header Jan_24_2017.
adv("#hdr_menu").find('a').focus(function(){ 
  if(this.id == 'clearDivId') { adv("#mob_menu_act").hide(); }
  var nextVal = this; 
  if(this.id == 'dsk_reviews' || this.id == 'deskNavTop') { nextVal =  adv(this).next(".subNavDkLink") ; } 
  adv(nextVal).next(".chose_inr").show();   
  if(adv(this).hasClass('subNavDkLink')){   
    adv(this).addClass("uline");
    adv(this).parents("#hdr_menu > ul > li").addClass("nactv");
  }              
  else{adv(this).parent("li").addClass("nactv");}
});
adv("#hdr_menu,.un_logo,#hdrmenu2,.mn_icons").find("a,input,#navQualList li a").blur(function()
{                                                               
  adv(this).css("border","0px")
  adv(this).parent("li").removeClass("nactv");
  adv(this).removeAttr("style");
  adv(this).parents("#hdr_menu > ul > li").removeClass("nactv");
  if(adv(this).hasClass('subNavDkLink')){
    adv(this).removeClass("uline");
    if(adv(this).parent("li").is(":last-child")){adv(this).closest(".chose_inr").hide();}
    if(adv(this).parent("li").is(":last-child")){adv("#navQual").next("#navQualList").hide();} 
  }                              
});
adv(".un_logo,#hdrmenu2,.mn_icons").find("a,input").focus(function(){
 adv(this).css("outline","2px solid #02ade2");
  adv(".lst_cont").css("outline","0 solid #02ade2");
});

adv("#hdrmenu2").find('#navQual,#navQualList li a').focus(function(event){ 
 adv("#navQual").next("#navQualList").show();
 if(adv(this).hasClass('subNavDkLink')){   
    adv(this).addClass("uline");
	adv("#navQualList li a").css("outline","0 solid #02ade2");
  }              
  else{adv(this).removeClass("uline");}
});
var width = document.documentElement.clientWidth; 
if(width >= 1024){
 adviceResizeHideNShow();
}
adv("#dsk_reviews").focus(function () {
  adv("#dsk_adv_submenu").hide();
});
adv("#deskNavTop, #comparison_tb").focus(function () {
  adv("#dsk_rev_submenu").hide();
});
adv("#hdrmenu2").find('.noalert').focus(function(){ 
 profileDropDownFlag = true;
 adv(".noalert").next("#mob_menu_act").show();
});
adv(".acc_set_pod ul li a").click(function() {
  if(!profileDropDownFlag) { adv(this).next().slideToggle("fast"); }
  profileDropDownFlag = false;
});	
adv(document).keyup(function(e) { 
 if (e.keyCode === 27){ hm(); }
});
//End
  adv("#hd1").removeAttr("style")
  adv('.lft_nav .brw').click(function(){
  adv(this).toggleClass('act');
  adv(this).parents().next().toggleClass('act');
});                
  adv('.srh').click(function(){
  adv(this).toggleClass('act');
  adv('.top_search').toggleClass('act')
});
  adv('.ic-mob-close').click(function(){
  adv(this).parents().find('.act').removeClass('act');
});
});
adv(window).resize(function() {
 adviceResizeHideNShow();
});

function adviceResizeHideNShow(){  
   adv(document).click(function(e) {
   if(!adv(this).hasClass('subNavDkLink') && document.documentElement.clientWidth >= 1024){
   adv("#dsk_rev_submenu").hide();
   adv("#dsk_adv_submenu").hide();
   }   
   if(e.target.id != 'navQual') { adv("#navQualList").hide(); }
  });
  adv("#reviewTabChg").mouseover(function() {
  if(document.documentElement.clientWidth >= 1024){
   adv("#dsk_rev_submenu").show();
   adv("#dsk_adv_submenu").hide(); }
  });
  adv("#adviceTabChg").mouseover(function() {
   if(document.documentElement.clientWidth >= 1024){ adv("#dsk_adv_submenu").show();
   adv("#dsk_rev_submenu").hide(); }
  });
  adv( "#reviewTabChg, #adviceTabChg" ).mouseout(function() {
   if(document.documentElement.clientWidth >= 1024){ adv("#dsk_adv_submenu").hide();
   adv("#dsk_rev_submenu").hide(); }
  });
}
function adviceShowHide(divId, divId1){ //Moved below script function from advice page to common, 24_Feb_2015 By Thiyagu G
   var divObj = document.getElementById(divId);
   var jqs =  jQuery.noConflict();
   var divObj1 = document.getElementById(divId1);
   if(divObj.style.display == 'block'){
       divObj.style.display = "none";       
       divObj1.innerHTML = '<i class="fa fa-plus"></i>VIEW MORE';
   }else{
       jqs(window).scrollTop(jqs(window).scrollTop()+1);
       divObj.style.display = "block";       
       divObj1.innerHTML = '<i class="fa fa-minus"></i>VIEW LESS';
   }    
}
function providerReviewsShowHide(viewLess, viewMore, viewMoreBtn){ //Added below script function for review view more and less, 24_Feb_2015 By Thiyagu G
   var hidId    = viewLess;
   var viewLess = document.getElementById(viewLess);
   var viewMore = document.getElementById(viewMore);   
   var viewMoreBtn = document.getElementById(viewMoreBtn);   
   var collegeId = ""; 
   var reviewId = "";    
   hidId = hidId.replace("viewLess_","");   
   if($$('collegeId')){
    collegeId = $$('collegeId').value;
   }
   if($$("hidReviewId_"+hidId)){
      reviewId = $$("hidReviewId_"+hidId).value;      
   }   
   if(viewMore.style.display == 'none'){
       viewLess.style.display = "none";
       viewMore.style.display = "block";
       viewMoreBtn.innerHTML = '- View less'; 
   }else{
       viewLess.style.display = "block";
       viewMore.style.display = "none";
       viewMoreBtn.innerHTML = '+ View more';       
   }    
}
function userReviewLogfn(collegeId,reviewId)
{    
  var url=contextPath + "/user-review-logfn.html?collegeId="+collegeId+"&reviewId="+reviewId;      
  var ajax=new sack();
  ajax.requestFile = url;  
  ajax.onCompletion = function(){};
  ajax.runAJAX();      
}
//
function clearingPopupClose(type, podName, frm){
  if(type == "clearing"){ 
    if('chatbot' == podName){
      GANewAnalyticsEventsLogging('Chatbot - Clearing', 'Clearing', 'Start');
      GANewAnalyticsEventsLogging('Chatbot - Clearing', 'Clearing', 'Yes');
      if(frm == 'CAROSAL'){
        $$('question4_carosal').style.display = "block";
        $$('question5_carosal').style.display = "block";
        $$('loading_image_5_carosal').style.display = "block";
        $$('footerContent_home_page').style.display = "none";
      }else{
        $$('question4').style.display = "block";
        $$('question5').style.display = "block";
        $$('loading_image_5').style.display = "block";
        $$('footerContent').style.display = "none";
        $$('loading_image_5').scrollIntoView();
        
      }
     
     setTimeout(function(){ updateUserTypeSession('SUB_PAGE',"clearing");}, 2000);
      
    }else{
      updateUserTypeSession('default',"clearing");
    }
  }else if(type == "non-clearing"){
    updateUserTypeSession('default',"non-clearing",'',podName);
    }
  //hm();
 if('top-nav' != podName){
  adv(".rev_lbox").removeAttr("style");
  adv("html").removeClass("rvscrl_hid");
 }
}
//
function callSubmitFunction(functionName, flag, from){
  if(functionName == 'newSubmitSearch'){ newSubmitSearch(flag,'ON', from)}
  else if(functionName == 'newCompSubmitSearch'){ newCompSubmitSearch(flag,'ON')}
  else if(functionName == 'coursesearchsubmit'){ coursesearchsubmit(flag,'ON')}
  else if(functionName == 'newSubmitSearchCD'){ newSubmitSearchCD(flag,'ON')}
  else if(functionName == 'mobileUniViewSubmit'){ mobileUniViewSubmit(flag,'ON')}
  else if(functionName == 'csSubmitSearch'){ csSubmitSearch(flag,'ON')}
}
//
function updateUserTypeSession(userAgent, type, functionName, from){
    var qualType = "";
    var fromPage = "";
    if(from == 'profile_page'){
      fromPage = "&fromPage="+from;
    }
    if($$('navQual')){
      qualType = $$('navQual').value;
    }
    if(navQualNetworkId == "3") { qualType = "Postgraduate"; } //Added by indumathi.S May_31_2016
    var url="/degrees/clearing/update-user-type-session.html?type=" +type+"&navQual="+qualType+fromPage;    
    var ajax=new sack();
        ajax.requestFile = url;	
        ajax.onCompletion = function(){
            if(type == 'clearing'){
              if(userAgent == 'default'){ redirectToClearingHomePage();     
              }else if(userAgent == 'SUB_PAGE'){redirectToSubLandingPage();
              
              }
              if(functionName != null){
                callSubmitFunction(functionName, true, from);
              }
            }else{
              if(functionName != null){
                callSubmitFunction(functionName, false, from);
                if(from == 'top-nav'){document.location.href = "/degrees/home.html";}
              }
            }
        };
        ajax.runAJAX();
}
function redirectToClearingHomePage(){location.href = "/degrees/university-clearing-2016.html";}
function redirectToSubLandingPage(){location.href = "/degrees/university-clearing-2020.html";}
function updateSearchBars(userAgent, type){
  if(userAgent == 'desktop'){
    if(type == 'clearing'){
      if($$("header")){$$("header").className = $$("header").className + " clr15 ";}
      //if($$("nav-clearing-2015")){setQual($$("nav-clearing-2015"));}
    }else{
      if($$("header")){$$("header").className = "hrd";}
    }
  }
  //
  if(userAgent == 'mobile'){
    if(type == 'clearing'){
      if($$("course_search_clearing_2015")){$$("course_search_clearing_2015").className = $$("course_search_clearing_2015").className + " clr_bor ";}
    }else{
      if($$("course_search_clearing_2015")){$$("course_search_clearing_2015").className = "sbx";}
    }
  }
  //Added for the wugo top nav
  if($$("clearingSwitchFlag") && $$("clearingSwitchFlag").value=="ON"){      
    var hdrCls = $$("headerClass") ? $$("headerClass").value : "hrdr";  
    if($$("header")){$$("header").className = hdrCls;}     
  }
}


//write a review
function $$$(id){return document.getElementById(id);}
function openLightBox(pageFlag, fromPage){
  //Added to fix bug for Oct_23_18 rel by Sangeeth.s (To restrict showing the save changes button from profile page in widgets)
  if(adv('#off_postop')){
    adv('#off_postop').removeClass('pro_set_sti');
  }
  var width = window.screen.width;
  var domainPathWidget = $$$("domainPathWidget").value;  //TODO uncomment this line when we deploy the WAR in LIVE.
  //var domainPathWidget = "http://192.168.1.167:8993/"; 
  var pagePath;
  var frame = document.getElementById("iframe_NRW");
  frameDoc = frame.contentDocument || frame.contentWindow.document;
  frameDoc.documentElement.innerHTML = "";
  frameDoc.removeChild(frameDoc.documentElement);
  if(pageFlag=='review'){
    pagePath = domainPathWidget+"/university-course-reviews/add-review/";
  }else if(pageFlag=='ucas'){
    pagePath = domainPathWidget+"/degrees/ucas-calculator.html";
  }else if(pageFlag=='iwanttobe'){ 
	if(adv("#revLightBox").is(":visible")){closeLigBox();}
	pagePath = domainPathWidget+"/degrees/i-want-to-be-widget.html";
  }else if(pageFlag=='whatcanido'){
	if(adv("#revLightBox").is(":visible")){closeLigBox();}  
    pagePath = domainPathWidget+"/degrees/what-can-i-do-widget.html";
  }else if(pageFlag == 'gradefilter') {
	 var queryStringValue = dev('#queryStr').val();
	 var selected_qual = dev('#selected_qual').val();
	 if(queryStringValue != "" ){
	 document.getElementById("holder_NRW").className += "grdflt_main";
	 $$$("close_NRW").style.display = "none";
	 
	 //var eventCat = "Enter qualification Filter"; eventCat = fromPage == 'EDIT_QUAL' ? 'Edit qualification Filter' : 'Enter qualification Filter';
	 //ga('set', 'dimension1', 'gradefilter');
	 var eventLabel = 'Search Results';
	 if('Profile' == fromPage){
	   eventLabel = 'Profile';	 
	 }
	 //eventTimeTracking();
	 GANewAnalyticsEventsLogging('Qualification Filter', 'Start', eventLabel);
	 pagePath = "/degrees/uni-course-requirements-match.html";
	 if(queryStringValue != 'undefined' && queryStringValue != null){
		 pagePath += queryStringValue + "&qualification="+selected_qual + "&pageName=srGradeFilterPage";  
	 }
	 //pagePath = domainPathWidget+"/degrees/uni-course-requirements-match.html" + () ? queryStringValue : '';   
	 }else{
		 pagePath = "/degrees/uni-course-requirements-match.html";
		 pagePath += urlFormation(pageFlag) + "&qualification="+selected_qual + "&pageName=mywhatuni"; 
	 }
  }else if(pageFlag=='chatbot'){ 
    if($$('chatbot_promo')) {
      $$('chatbot_promo').style.display = 'none';
      $$('needHelpDiv').className = 'cbot_cnt';
    }
    pagePath = domainPathWidget+"/degrees/chatbot-widget.html?from="+fromPage;
  }else if(pageFlag=='interstitial'){	  
    pagePath = domainPathWidget+"/degrees/course/advance-search.html";
    if(width <= 768 && $$$('keywordMob_qualification')) {
      qualStr = "?qual="+$$$('keywordMob_qualification').value;
      qualStr += ($$$('isClearingSelectedMob') && 'Y'==$$$('isClearingSelectedMob').value) ? "&clearing" : "";
    } else if($$$('keywordTpNav_qualification')){
      qualStr = "?qual="+adv('#keywordTpNav_qualification').val();
      qualStr += adv('#keywordTpNav_id').val()!="" ? "&sub_id="+adv('#keywordTpNav_id').val() : "";
      qualStr += adv('#keywordTpNav_display').val()!="" ? "&sub_name="+adv('#keywordTpNav_display').val() : "";
      var subNameUrl = adv('#keywordTpNav_url').val()!="" ? adv('#keywordTpNav_url').val() : "";
      if(subNameUrl != '' && subNameUrl.indexOf('=') > -1){
    	qualStr += "&sub_url_text="+(subNameUrl).substring(subNameUrl.indexOf('=')+1);  
      } 
      qualStr += adv('#locUrlTextTP').val()!="" ? "&loc_name="+adv('#locUrlTextTP').val() : "";      
      qualStr += ($$$('isClearingSelectedTpNav') && 'Y'==$$$('isClearingSelectedTpNav').value) ? "&clearing" : "";
      
    }
    if(adv("#revLightBox").is(":visible")){closeLigBox();}
    if($$$('keywordTpNav_qualification')){
		pagePath += qualStr;
		//
		if(!adv("#homeHeroImgDivId").is(":visible")){ga('set', 'dimension1', 'topnav');}
    }
	GANewAnalyticsEventsLogging('Advanced Search', 'advanced search', 'begin search');
    //
    document.getElementById("holder_NRW").className += " holder_adv";
    document.getElementById("iframe_NRW").style.height = "100%";
    document.getElementsByTagName( 'html')[0].className = ' scrl_dis';
    //
  }
  if($$$("socialBoxDiv")) { $$$("socialBoxDiv").style.display = "none"; }
  if($$$("socialBoxMin")) { $$$("socialBoxMin").style.display = "none"; }
  if (width <= 768) {
    window.location.assign(pagePath);
  } else {
      $$$("iframe_NRW").src = pagePath; 
      $$$("fade_NRW").style.display = "block";
      $$$("holder_NRW").style.display = "block";
      $$$("close_NRW").style.display = "block";   
      if(pageFlag=='gradefilter') {
          $$$("close_NRW").style.display = "none";
        }
      if($$$("lightBoxName") && (pageFlag=='iwanttobe' || pageFlag=='whatcanido' || pageFlag=='chatbot' || pageFlag=='interstitial')) { $$$("lightBoxName").value = "widgetReload" };
  }
  if(width < 993){
    $$$("hdr_menu").style.display = "none";
  }
  resizeDivs();      
}
var dev = jQuery.noConflict();
dev(window).resize(function() {
  resizeDivs();
});
function resizeDivs(){
  var wh = window.innerHeight;
  var ww = window.innerWidth;
  if($$$("holder_NRW")){$$$("holder_NRW").style.width = (ww - 100) + "px";}
  if($$$("holder_NRW")){$$$("holder_NRW").style.height = (wh - 100) + "px";}
}
function closeReviewLightBox(){
  $$$("fade_NRW").style.display = "none";
  $$$("holder_NRW").style.display = "none";
  $$$("close_NRW").style.display = "none";    //Added condition on 08_Aug_17, By Thiyagu G for Bug 39147.
  if(document.getElementById('iframe_NRW').contentWindow.document.getElementById("iwanttobeWidget")){
    document.getElementById('iframe_NRW').contentWindow.document.getElementById("iwanttobeWidget").innerHTML = "";
  }  
  if(document.getElementById('iframe_NRW').contentWindow.document.getElementById("whatcanidoWidget")){
    document.getElementById('iframe_NRW').contentWindow.document.getElementById("whatcanidoWidget").innerHTML = "";
  }
  if(document.getElementById('iframe_NRW').contentWindow.document.getElementById("homePage_AMW")){
    document.getElementById('iframe_NRW').contentWindow.document.getElementById("homePage_AMW").innerHTML = "";
    document.getElementById('iframe_NRW').contentWindow.document.getElementById("homePage_AMW").style.display="none";
  }
  //
  var homePageAMWId;
  if(document.getElementById('iframe_NRW').contentWindow.document.getElementById('success_AMW')){
    homePageAMWId = document.getElementById('iframe_NRW').contentWindow.document.getElementById('success_AMW'); 
  }
  if(homePageAMWId){
    clearAllDivNRW();
  }
  var width = window.screen.width;
  if(width >= 993){
    //$$$("hdr_menu").style.display = "block";    
  }else if($$$("hdrMenuHide")){
    //$$$("hdr_menu").style.display = "block";
  }
  if(dev("#socialBoxMin").is(":hidden")) {              
     dev("#socialBoxMain").show();
     dev("#socialBoxTweetUs").hide();    
  }
}
function clearingCoursesearchsubmit(functionName){
  var selectedObject = ($$("region")) ? ($$("region")) : ($$("mobile_qual"));
  var selectedValue = selectedObject.options[selectedObject.selectedIndex].value;
  if(selectedValue == 'CLEARING'){
    updateUserTypeSession('','clearing',functionName);
  }else{
    updateUserTypeSession('','non-clearing',functionName);
  }
}
function updateclearingChanges(obj){
  var selectionId = obj.id
  var el = document.getElementById(selectionId);
  var text = el.options[el.selectedIndex].text;
  if(text.indexOf('Clearing') > -1){
    $$('course_search_clearing_2015').className = 'sbx clr_bor';
  }else{
    $$('course_search_clearing_2015').className = 'sbx';
  }
}    

function customConfirm(){
	var result = true; 
 var successAMWId;
 if(document.getElementById('iframe_NRW').contentWindow.document.getElementById('success_AMW')){
  successAMWId = document.getElementById('iframe_NRW').contentWindow.document.getElementById('success_AMW'); 
 }
 if(document.getElementById('iframe_NRW').contentWindow.document.getElementById('whatcanidoWidget')){
  successAMWId = document.getElementById('iframe_NRW').contentWindow.document.getElementById('whatcanidoWidget');
 }	
 if(document.getElementById('iframe_NRW').contentWindow.document.getElementById('iwanttobeWidget')){
  successAMWId = document.getElementById('iframe_NRW').contentWindow.document.getElementById('iwanttobeWidget');
 } 
 
	if(successAMWId && successAMWId.style.display == 'block'){
		result = true;
	}else {
		result = window.confirm("Do you really want to leave?  You will lose your progress");
	}
	if(result == true){
    if($$$('iframe_NRW').src.indexOf('chatbot') > -1 ) {
      $$$('iframe_NRW').contentWindow.doGAlogOnExit();
    }
		closeReviewLightBox();
		if($$$("lightBoxName") && $$$("lightBoxName").value == "widgetReload") {			
      var root = document.getElementsByTagName( 'html')[0];   
      root.className -= 'scrl_dis';
      var holder = $$$("holder_NRW");
      if($$$("holder_NRW")) { holder.className -= " holder_NRW"; }
      var iframe_NRW = $$$("iframe_NRW");
      if($$$("iframe_NRW")) { iframe_NRW.style.height = "99%"; } 
		}
		//timersLB =  setTimeout(function(){showLightBoxLoginForm('popup-newlogin',650,500, 'Y','','splash-page')},180000);
		if($$$("socialBoxMin")) { 
			$$$("socialBoxMin").style.display = "block"; 
		}
	}
}
//
function switchProfileURL(networkType,from){
  var switchProvUrl = "";  
  if($$("switchProvUrl") && from==undefined){  
    switchProvUrl = $$("switchProvUrl").value;
  }  
  var url = "/degrees/switch-profile-network.html";
  var queryString = "";  
  if(switchProvUrl.indexOf('/clearing') > -1){
      switchProvUrl = switchProvUrl.replace('/clearing','');
    }
  if(networkType=="UG"){	   
    queryString = "?nwId=2";
  }else if(networkType=="PG"){  
    queryString = "?nwId=3";
    navQualNetworkId = "3"; //Added by indumathi.S May_31_2016                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
  }else if(networkType == 'CL'){
    queryString = "?nwId=2";
    switchProvUrl = switchProvUrl + 'clearing';    
  }      
  var ajaxURL = url + queryString;  
  var ajax=new sack();
  ajax.requestFile = ajaxURL;
  ajax.onCompletion = function(){ redirectProvURL(switchProvUrl,from); };
  ajax.runAJAX();  
}
//
function redirectProvURL(switchProvUrl,from){   
  if(switchProvUrl!=""){
    location.href = switchProvUrl;
  }else if(from!=undefined && (from=="topNav" || from=="topMobNav" || from=="top_nav_popup")){
    newSubmitSearch(false,"OFF", from);    
  }
}
function setSessionCookie(c_name,value){  
  var c_value;
  var IEversion = detectIE();
  if (IEversion !== false) {
    c_value=escape(value) + ";";
  }else{
    c_value=escape(value) + "; expires=0";
  } 
  document.cookie=c_name + "=" + c_value+"; path=/";
}

function setNavNetworkType(navQual, fromFlag){
  var navFlag = 'topNav';  
  if(fromFlag == 'topMobNav'){
    navFlag = 'topMobNav';   
  }else if(fromFlag == 'top_nav_popup'){
    navFlag = 'top_nav_popup';
  }
  if($$(navQual).value!="" && $$(navQual).value=="Postgraduate"){      
    switchProfileURL('PG',navFlag);
  }else{
    switchProfileURL('UG',navFlag);
  }
}
//Added by Indumathi.S For mobile CTA changes Jan-27-16
function checkChildDiv(divId){
 var childDiv = document.getElementById(divId).children;
 if (childDiv[1].tagName == "A") {
  dev('.btns_interaction').addClass("nadv_req_inf"); 
  return false;   
 }
 return true;
}
//
function clearAllDivNRW(){
	var divArr =["homePage_AMW", "accommodation_AMW", "0_rating_AMW", "1_rating_AMW", "2_rating_AMW", "3_rating_AMW", "4_rating_AMW", "5_rating_AMW", "6_rating_AMW","7_rating_AMW", "8_rating_AMW", "registration_AMW", "success_AMW"];
	for(var i=0;i<divArr.length;i++){
		$$$('iframe_NRW').contentWindow.$$$(divArr[i]).innerHTML = "";
		$$$('iframe_NRW').contentWindow.$$$(divArr[i]).style.display = "none";
		$$$('iframe_NRW').contentWindow.$$$('percent_AMW').innerHTML = "";
		$$$('iframe_NRW').contentWindow.$$$('grnBgId_AMW').style.width = "0%";
	}	
}
dev(document).ready(function() {
   if(window.location.hash) {
     var hash = window.location.hash.substring(1);
     if(hash.indexOf("rev_") > -1 || hash.indexOf("reviews") > -1){
       var scrnwidth = document.documentElement.clientWidth;
       var targetId = dev('#'+hash);
       var timeLineTop = 0; if($$$("dskTimeline")){timeLineTop = dev("#dskTimeline").offset().top - 200;}
       var reivewsTop = dev("#revSecId").offset().top - dev("#fixedscrolltop").offset().top - timeLineTop;
       var heroslider = dev("#heroslider") ? dev("#heroslider").offset().top : 0;
       if(scrnwidth < 993){
         reivewsTop = dev("#revSecId").offset().top + 130;
       }
       var scrolTop = hash.indexOf("reviews") > -1 ? reivewsTop : targetId.offset().top - heroslider - 180;
       if(targetId){
         dev('html,body').animate({
           scrollTop: scrolTop
         }, 1200);
       }
     }
   }
});

function showAndHideMobileSearch(id, dispVal){
  if(dispVal == "block" && ($$("subGuides").innerHTML).trim() == ""){    
    var ajax=new sack();
    var url = '/degrees/courses/getPopularSubjectsAjax.html';
    ajax.requestFile = url;
    ajax.onCompletion = function(){ popularSubjectsAjax(ajax, id, dispVal); };	
    ajax.runAJAX();      
  }else{
    $$(id).style.display = dispVal;
    var srb_hgt = document.documentElement.clientHeight;    
    dev(".srb_wrap").css("height", srb_hgt);
    if(dispVal == "block"){      
      dev("html,body").css("overflow", "hidden");
      dev(".bk_to_top").addClass("set_pos");
      //dev(".bk_to_top").css("z-index", "-1");
      dev(".ser_cls a").focus();
      dev('#ajxcnt_mob').html('');
      //console.log('113223');
      dev("#atz, .ibtn_sec").addClass("ch_hm_srbx");
      dev('html, body').addClass('hmsrh_fxd');
    }else{
      dev("html,body").css("overflow", "visible");
      dev(".bk_to_top").removeClass("set_pos");
      //dev(".bk_to_top").css("z-index", "-1");
      dev("#navMobQual").val("Degrees");
      dev("#navMobQualList").hide();
      dev('#ajxcnt_mob').html('');
      dev(".sfld_rw2").find("#navMobKwd").css("outline","");
      //console.log('1132dfd23');
      dev("#atz, .ibtn_sec").removeClass("ch_hm_srbx");
      dev('html, body').removeClass('hmsrh_fxd');
    }
  }
}
function popularSubjectsAjax(ajax, id, dispVal){
  var resp = ajax.response;  
  $$("subGuides").innerHTML = resp;
  $$(id).style.display = dispVal;
  var srb_hgt = document.documentElement.clientHeight;    
  dev(".srb_wrap").css("height", srb_hgt);
  if(dispVal == "block"){    
    dev("html,body").css("overflow", "hidden");
    dev(".bk_to_top").addClass("set_pos");
    //dev(".bk_to_top").css("z-index", "-1");
    dev(".ser_cls a").focus();
    dev("#atz, .ibtn_sec").addClass("ch_hm_srbx");
    dev('html, body').addClass('hmsrh_fxd');
    dev('#ajxcnt_mob').html('');
   }else{
    dev("html,body").css("overflow", "visible");
    dev(".bk_to_top").removeClass("set_pos");
    //dev(".bk_to_top").css("z-index", "-1");
    dev("#navMobQual").val("Degrees");
    dev('#ajxcnt_mob').html('');
    dev("#navMobQualList").hide();
    dev(".sfld_rw2").find("#navMobKwd").css("outline","");
    dev("#atz, .ibtn_sec").removeClass("ch_hm_srbx");
    dev('html, body').removeClass('hmsrh_fxd');
  }
}
var de = jQuery.noConflict();
de(document).ready(function() {
  adStyle();
});
//de(window).on('orientationchange', orientationHandler);
function orientationHandler(e) {
  setTimeout(function() {
    dev(window).trigger('resize');      
  }, 500);
}
de(window).resize(function() {
  if($$('ajax_listOfOptions') && $$('ajax_listOfOptions').style.display == "block"){        
    $$('ajax_listOfOptions').style.display="none";
  }
  adStyle();
});
function adStyle(){
  var width = document.documentElement.clientWidth;
  var srb_hgt = document.documentElement.clientHeight;    
  dev(".srb_wrap").css("height", srb_hgt);
  if(width <= 992){
    adv(".srb_wrap").scroll(function(){
      if(adv('.srb_wrap').scrollTop() > 50){ //Commented hiding ajax dropdown while scrolling the page by Prabha on 28_Aug_2018
       // if($$('ajax_listOfOptions')){$$('ajax_listOfOptions').style.display="none"};
      }
    });   
   adv("#navMobKwd").click(function() {
     if($$('ajax_listOfOptions')){
       if($$('keywordMob_display')){  
        $$('keywordMob_display').value = '';
       }
      // if($$('ajax_listOfOptions')){$$('ajax_listOfOptions').style.display="none"};
     }
   });
  }
}

dev("#navMobKwd").bind("keyup change", function(e) {
  if(adv("#ajax_listOfOptions") && adv("#navMobKwd") && adv("#navMobKwd").val().length > 0){
    var screenTop = adv("#navMobKwd").offset().top + $$$("navMobKwd").offsetHeight + 'px';
    dev('#ajax_listOfOptions').css('top', screenTop);
  }
});
function articleJsDynamicLoad(src){
  var script = document.createElement("script");
  script.type = "text/javascript";  
  script.src = src;
  if (typeof script != "undefined"){
    document.getElementsByTagName("head")[0].appendChild(script);
  }
}
//load articles pod on scroll on 13_06_2017, By Thiyagu G.
function loadArticlesPodJS(pageName){
  var afPath = $$("contextJsPath").value+'/js/advice/topArticlesPod.js';
  if (adv('head script[src="' + afPath + '"]').length > 0){
  }else{
    articleJsDynamicLoad(afPath);
  }
  setTimeout(function() {onScrollArticlesPod(pageName);}, 400);
}
//To get recent search for 31_Jul_18 rel by Sangeeth.S
function recentSearchAjax(event, object){  
  if(dev("#"+ object.id).val().length < 3 || "Please enter subject or uni" == dev("#"+ object.id).val()){
    var userId = (dev("#loggedInUserId") && dev("#loggedInUserId").val() != undefined && dev("#loggedInUserId").val()!="") ? dev("#loggedInUserId").val() : "0" ;  
    var width = object.id == 'navKwd'? '385' : '0'; 
    var navQual = object.id == 'navMobKwd' ? dev("#navMobQual").val() : dev("#navQual").val();
    //var navKwd = trimString($$(navKwd).value);
    var navQual = trimString(navQual); 
    var qualDesc = 'degree';
    var qualCode = navQual;
    var qualCode = "M";  
    if(navQual == 'Degrees' || navQual == 'Undergraduate'){
      qualDesc = 'degree'; qualCode='M';
    }else if(navQual == 'Postgraduate'){
      qualDesc = 'postgraduate'; qualCode='L';
    }else if(navQual == 'Foundation degree'){
      qualDesc = 'foundation-degree'; qualCode='A';
    }else if(navQual == 'Access & foundation'){
      qualDesc = 'access-foundation'; qualCode='T';
    }else if(navQual == 'HND / HNC'){
      qualDesc = 'hnd-hnc'; qualCode='N';
    //}else if(navQual == 'UCAS CODE'){
    //  qualDesc = 'ucas_code'; qualCode='UCAS_CODE';
    } else if(navQual == $$("clearingYear").value || 'Clearing/Adjustment' == navQual){ //24_JUN_2014
      qualDesc = 'degree'; qualCode='M';
      dev('#isClearingSelected').val('Y');
      dev('#isClearingSelectedMob').val('Y');
    }
    if(!(navQual == $$("clearingYear").value || 'Clearing/Adjustment' == navQual)){
      dev('#isClearingSelected').val('N');
      dev('#isClearingSelectedMob').val('N');
    }
    ajax_showOptions(object,'recentAndAdvSrch',event,"indicator",'userId='+userId+'&navQual='+qualCode, '0', width);
    if(object.id == 'navMobKwd'){
      ajax_optionDiv.style.width = (ajax_getWidth(object)) + 'px';
      var x = adv("#navMobKwd").offset();    
      ajax_optionDiv.style.top = x.top+$$$("navMobKwd").offsetHeight+'px';    
    }
  }
}
function checkApplicationPageStatus(){
  var loggedInUserId = $$D("loggedInUserId").value;
  if(loggedInUserId == '0') {
  callLightBox('0##SPLIT##APPLICATION_ERROR_MESSAGE_LIGHTBOX');
  }
  else {
  var url = contextPath+"/get-application-page.html?action=APPLICATION-PAGE&fromUserPage=USER_PROFILE_AJAX";      
  var ajaxObj = new sack();
  ajaxObj.requestFile = url;	  
  ajaxObj.onCompletion = function(){applicatioPageStatus(ajaxObj);};	
  ajaxObj.runAJAX(); 
  }
}
function applicatioPageStatus(ajaxObj){
  var applUrl = '/degrees/get-application-page.html?action=APPLICATION-PAGE';
  if(ajaxObj.response== '0'){
   callLightBox('0##SPLIT##APPLICATION_ERROR_MESSAGE_LIGHTBOX');
  }
  if(ajaxObj.response != '0'){
   if('mysettings.jsp' == dev('#GAMPagename').val()){     
     settingValidation(applUrl, event,'application-page')  
   }else{
    location.href = applUrl;
   }
  }
}
function callLightBox(result, params){
  var resultArr = result.split("##SPLIT##");
  var resId = "revLightBox";
  var loc = "";
 var hotLine = dev('#hotLine').val();
  var collegeDisplayName = dev('#collegeDisplayName').val();
  var qualErrorCode = resultArr[2];
  if(resultArr[0] == "QUAL_ERROR"){
    var offerName = dev('#offerName').val();
    var pageNo = dev('#pageNo').val();
    var from = resultArr.length > 7 ? resultArr[8] : "";
    
    var param = "?qualErrorCode=" + qualErrorCode+"&oldQualDesc="+resultArr[3]+"&newQualDesc="+resultArr[4]+"&offerName="+offerName+"&pageNo="+pageNo+"&from="+from+ "&oldGrade="+resultArr[4]+"&newGrade="+resultArr[6]+"&subCount="+resultArr[7];
    loc = "/jsp/whatunigo/include/entryReqLiBox.jsp"+param;
  }else if(resultArr[0] == "TERMS_AND_COND"){
  }else if(resultArr[0] == "TIMER_EXPIRY"){
    loc = "/jsp/whatunigo/include/timerExpirySection.jsp";
  }else if(resultArr[0] == "ERROR"){
    loc = "/jsp/whatunigo/include/errorPage.jsp?errorMsg="+resultArr[1];
  }else if(resultArr[0] == "NO_PLACE"){
    loc = "/jsp/whatunigo/include/noPlaceLiBox.jsp?errorMsg="+resultArr[1];
  }else if(resultArr[1] == "CANCEL_OFFER"){
   loc = "/jsp/whatunigo/include/cancelOfferLightBox.jsp?hotLine="+hotLine+"&collegeDisplayName="+collegeDisplayName; 
  }else if(resultArr[1] == "APPLICATION_ERROR_MESSAGE_LIGHTBOX"){
   loc = "/jsp/whatunigo/include/applicationErrorMessageLightbox.jsp";
  }else if(resultArr[0] == "SAVE_APPLICATION"){
    loc = "/jsp/whatunigo/include/saveApplicationLibox.jsp";
  }else if(resultArr[0] == "FIVE_MIN"){
    loc = "/jsp/whatunigo/include/fiveMinLiBox.jsp";
  }else if(resultArr[0] == "BACK_TO_COURSE"){
    loc = "/jsp/whatunigo/include/exitLightBox.jsp";
  }else if(resultArr[0] == "TOP_NAV_SEARCH_POPUP"){
    //loc = "/jsp/common/topNavSearchAjaxPopup.jsp";
	dev('#loaderTopNavImg').show();   
	if(params == 'clearing'){
      loc = "/ajax/common/top-navigation.html?clearing";
    }else{
      loc = "/ajax/common/top-navigation.html";
    }
  }
  var url = contextPath + loc;
  dev.ajax({
    url: url, 
    /*data: {
      "page-id": pageId, 
      "next-page-id": nxtPageId, 
      "status": status,
      "action": action,
      "htmlId": htmlId,
      "resultQus": resultQus,
      "resultAnsArr": resultAnsArr,
      "resultOptArr": resultOptArr
      "qualErrorCode" : resultArr[2]
    },*/
    type: "POST", 
    success: function(result){
      if(resultArr[0] == "TOP_NAV_SEARCH_POPUP"){
    	dev('#loaderTopNavImg').hide();  
      }else{
    	dev('#loadingImg').hide(); 
      }       
      dev("#"+resId).html(result);
      lightBoxCall(resultArr[0]);
      revLightbox();
    }
  });
}
function lightBoxCall(podtype){
 if(podtype == "TOP_NAV_SEARCH_POPUP"){
   dev("#revLightBox").addClass("rev_lbox rvlbx_ui"); 
   if(isNotNullAndUndex(dev("#clearingonoff")) && dev("#clearingonoff").val() == "ON"){
	 if(dev("#ucasSrcDiv")){
	   dev("#ucasSrcDiv").css("display", "none"); 
	 }   	 
   }
 }else if(podtype=="POSTCLEARING_LIGHTBOX") {
	 jq("#revLightBox").addClass("rev_lbox clr_sf_cnr");
 }
 else{
   dev("#revLightBox").addClass("rev_lbox");	 
 }	 
 dev(".rev_lbox").css({'z-index':'111111','visibility':'visible'});
 dev(".rev_lbox").addClass("fadeIn").removeClass("fadeOut");
 dev("html").addClass("rvscrl_hid");
 //closeLiBox();
}
function revLightbox(){
  var width = document.documentElement.clientWidth;
  var revnmehgt = dev(".rvbx_cnt .revlst_lft").outerHeight();
  var lbxhgt = dev(".rev_lbox .rvbx_cnt").height();
  var txth2hgt = dev(".rvbx_cnt h2").outerHeight();
  var txth3hgt = dev(".rvbx_cnt h3").outerHeight();
  if (width <= 480) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    dev(".lbx_scrl").css("height", +scrhgt+ "px");
   }else if ((width >= 481) && (width <= 992)) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    dev(".lbx_scrl").css("height", +scrhgt+ "px");
   }else{
     var lbxmin = 17 + txth2hgt + txth3hgt;
     var scrhgt = lbxhgt - lbxmin; 
     dev(".lbx_scrl").css("height", +scrhgt+ "px"); 
   }
}
function closeLigBox(selectedValue){
  dev(".rev_lbox").removeAttr("style");
  dev(".rev_lbox").addClass("fadeOut").removeClass("fadeIn");
  dev("html").removeClass("rvscrl_hid");
   var pageName = dev('#GAMPagename').val();
  if(pageName == 'newusergradelandingpage.jsp' || pageName == 'gradefilterlandingpage.jsp'){
    dev('.slqualif .ucas_slist').removeAttr("style");
  }
  if(selectedValue == 'opendayLightBox'){
	 var selectedEventId = dev("#selectedEventId").val(); 
	 if (!dev("#"+selectedEventId).is(":checked")) {
		 dev('#' + selectedEventId).prop("checked", true);
	 }
  }
}
function clearingShowVideoResp(){
dev('#loadingImg').show(); 
var resId = "revLightBox";
var loc = "/clearing/watchvideo/loadWatchVideo.jsp";
var url = contextPath + loc;
  dev.ajax({
    url: url,     
    type: "POST", 
    success: function(result){
      dev('#loadingImg').hide(); 
      dev("#"+resId).html(result);
      lightBoxCall();
      revLightbox();      
    }
  });
}
function showUserMenu(menuId){
  //dev('#'+menuId).show();
  //if(!profileDropDownFlag) {
    adv('#'+menuId).slideToggle("fast");
  //}
  profileDropDownFlag = false;
}

function sponsoredListGALogging(institutionId){
  var boostedInstId = !isBlankOrNullString(adv("#boostedInstId").val()) ? adv("#boostedInstId").val() : "";
  var sponsoredInstId = !isBlankOrNullString(adv("#sponsoredInstId").val()) ? adv("#sponsoredInstId").val() : "";
  //var sponsoredFlag = !isBlankOrNullString(adv("#sponsoredListFlag").val()) ? adv("#sponsoredListFlag").val() : "";
  //var manualFlag = !isBlankOrNullString(adv("#manualBoostingFlag").val()) ? adv("#manualBoostingFlag").val() : "";
  
  var sponsoredTypeInstitutionId;
  var sponsoreTypeFlag;
  if(institutionId == sponsoredInstId){
	sponsoreTypeFlag = "sponsored";
	sponsoredTypeInstitutionId = sponsoredInstId;
  }else if(institutionId == boostedInstId){
	sponsoreTypeFlag = "manual";
	sponsoredTypeInstitutionId = boostedInstId;
  }else{
	sponsoreTypeFlag = "none";
	sponsoredTypeInstitutionId = "";
  }
  var chainProcessSessionURL = "/degrees/chain-process-ga-session.html?sponsoreTypeFlag="+sponsoreTypeFlag+"&sponsoredTypeInstitutionId="+sponsoredTypeInstitutionId;
  jq.post(chainProcessSessionURL, function(data) {	
  });
  
  var fieldObject;
  if(institutionId == boostedInstId){
	fieldObject = "Manual";  
  }else if(institutionId == sponsoredInstId){
	fieldObject = "Sponsored";    
  }else if(institutionId != boostedInstId && institutionId != sponsoredInstId){
	fieldObject = "None";   
  }
  ga('set', 'dimension17', fieldObject);  
}
/*Clearing switchoff updates GALog*/
adv(window).on("load", function(){
	//var pageName = adv("#GAMPagename").val();
	var pageName = adv("#pagePathGALog").val();
	var studyLevel = adv("#studyLevelGA").val();
	var levelName = "Profile Level";
	if(studyLevel == "Clearing 2020") {
		studyLevel = "Clearing";
		//levelName = "Clearing Level"
		//pageName = "clearinguniprofile.jsp"	
	}
	if(isNotNullAndUndex(studyLevel) && isNotNullAndUndex(pageName)) {
		GANewAnalyticsEventsLogging(levelName,studyLevel,pageName);
	}	
})