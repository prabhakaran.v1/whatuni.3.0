function $$(id){return document.getElementById(id);}
var jq = jQuery.noConflict();
var unsavedMail = false;
var width = document.documentElement.clientWidth;
function displayText(id){
  document.getElementById(id).style.display="block";
}
function hideText(id){
  document.getElementById(id).style.display="none";
}
function disableNonUKSchool() {
  if ($$("nonUKFlag").checked == true){
    $$("ukSchoolName").disabled = true;
    $$("ukSchoolName").value = "";
    setSuccessMsgs("ukSchoolNameFeild", "w100p fl");
    $$("nonUKFS").style.display = 'block';
  }else{
    $$("ukSchoolName").disabled = false;
    $$("nonUKFS").style.display = 'none';
    $$("nonUKSchool").value = "";
  }
}
function disableEnableUni(){
  if ($$("NON_UK_UNIVERSITY_FLAG").checked == true){
    $$("firstUniId").disabled = true;
    $$("firstUniId").value = ""; 
    $$("FS_NON_UK_UNIVERSITY_NAME").style.display = 'block';
    $$("firstCourseId_display").value = "";
    $$("firstCourseId_hidden").value = "";
    $$("firstUniId_hidden").value = "";    
    $$("firstCourseId").value = "Please enter your course name";    
  }else{
    $$("firstUniId").disabled = false;
    $$("FS_NON_UK_UNIVERSITY_NAME").style.display = 'none';
    $$("firstUniId_hidden").value = "";
    $$("firstUniId").value = "Please enter your University name";
    $$("NON_UK_UNIVERSITY_NAME").value = "Please enter uni name";
    $$("firstCourseId").value = "Please enter your course name";
  }
}
function enableAreyou(obj){  
  var areYouVal;
  if(obj){areYouVal = obj.value;}
  if(areYouVal == '486'){
    $$("FS_CURR_EMP_INDUSTRY").style.display = "block";
    $$("FS_CURR_EMP_SENIORITY_LEVEL").style.display = "block";
  }else{
    $$("FS_CURR_EMP_INDUSTRY").style.display = "none";
    $$("FS_CURR_EMP_SENIORITY_LEVEL").style.display = "none";
    $$("CURR_EMP_INDUSTRY").value = "";
    $$("CURR_EMP_SENIORITY_LEVEL").value = "";
    $$("dropsapn_CURR_EMP_INDUSTRY").innerHTML = "Please select industry";
    $$("dropsapn_CURR_EMP_SENIORITY_LEVEL").innerHTML = "Please select seniority level";
  }
  $$("CURRENT_EMPLOYMENT_ARE_YOU").value = areYouVal;
}
function enableUGPG(obj){
  var studyLevelVal = obj.value;
  if(studyLevelVal == '453'){
    $$("ugdetails").style.display = "none";
    $$("pgdetails").style.display = "block";
  }else{
    $$("ugdetails").style.display = "block";
    $$("pgdetails").style.display = "none";
  } 
  $$("MWU_STUDY_LEVEL").value = studyLevelVal;
  $$("MWU_STUDY_LEVEL_PG").value = studyLevelVal;
}
function autoCompleteUKSchool(event,object){
	ajax_showOptions(object,'ukschoollist',event,"indicator",'','',''); 
}
function autoCompleteUniCourse(event, object){
  $$('firstCourseId_hidden').value = "";
  var collegeId = $$('firstUniId_hidden').value;
	 var extraParam = 'z=' + collegeId;
	 ajax_showOptions(object,'firstcourse',event,"indicator",extraParam);
}
function qualvalidation(obj){
  var selId = obj.id;
  var qual = "";
  if(qual == "" || qual == null){
    alert('Please Select Predicted Grades Qualification.');
    $$(selId).value = '0';
    return false;
  }
}
function setDOBValues(){
  var dateDOB = $$("dateDOB");
  var text = dateDOB.options[dateDOB.selectedIndex].text;
  $$("dateSpan").innerHTML = text;

  var monthDOB = $$("monthDOB");
  var text = monthDOB.options[monthDOB.selectedIndex].text;
  $$("monthSpan").innerHTML = text;

  var yearDOB = $$("yearDOB");
  var text = yearDOB.options[yearDOB.selectedIndex].text;
  $$("yearSpan").innerHTML = text;

  var nationality = $$("nationality");
  var text = nationality.options[nationality.selectedIndex].text;
  $$("natSpan").innerHTML = text;
  
  //var grade_210 = $$("grade_210");
 
  
  var countryOfResidence = $$("countryOfResidence");
  var countryText = countryOfResidence.options[countryOfResidence.selectedIndex].text;
  $$("countrySpan").innerHTML = countryText;
  if(countryText == 'United Kingdom' || countryText == 'England' || countryText == 'Northern Ireland' || countryText == 'Scotland' || countryText == 'Wales'){
    $$('addFinder').style.display= 'block';
  }  
  enableAreyou($$("CURRENT_EMPLOYMENT_ARE_YOU"));
  enableUGPG($$("MWU_STUDY_LEVEL"));
}
function getPostCodeAdd(){
  var postcodeVal = $$("postcode").value;
  if(postcodeVal == 'Enter UK postcode'){
      setErrorAndSuccessMsgs("postcodeField", "w50p fr ErrMsg", "postcodeErrMsg", "<p>Hmm, that doesn't seem to be a real postcode.</p>");
    return false;
  }
  //code for prepopulationg the please select country by Prabha on 03_NOV_2015_REL
  $$("postCodeAddSpan").innerHTML = "Please select";
  if($$("postaddid") && $$("postaddid").style.display == "block"){
    $$("postaddid").className = "w100p fr";
  }
  
  var newPostCode = checkPostCode(trimString(postcodeVal));
  if(newPostCode != ""){
    var url="/degrees/getpostcodeaddress.html?postcode="+newPostCode;
   var ajax=new sack();
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ getPostCodeData(ajax); };	
   ajax.runAJAX();  
  }else{
    setErrorAndSuccessMsgs("postcodeField", "w50p fr ErrMsg", "postcodeErrMsg", "<p>Hmm, that doesn't seem to be a real postcode.</p>");
  }  
 } 
function getPostCodeData(ajax){  
  var postCodeRes = ajax.response;
	if(postCodeRes.trim() == 'NORESULTS'){
    setErrorAndSuccessMsgs("postcodeField", "w50p fr ErrMsg", "postcodeErrMsg", "<p>Hmm, that doesn't seem to be a real postcode.</p>");   
    return false;
	}else{
    $$("postCodAdd").innerHTML = "";
    var dataArrayMain = ajax.response.split("**");
    for(var i = 0; i < dataArrayMain.length; i++){
      var address = dataArrayMain[i];
      var optionArray = address.split("$$");
      addElement('postCodAdd', optionArray[1], optionArray[0]);
    }
    //$$('postCodAdd').innerHTML= ajax.response;
    $$('postaddid').style.display= 'block';
    $$('postcodeField').style.display= 'block';
    $$('postcodeSection').style.display= 'block';    
	}  
}
function setPostCodeAddress(obj){
  var postCodeAdd = obj.value;
  if(postCodeAdd != null && postCodeAdd != ""){
  var dataarray=postCodeAdd.split("###");
    $$('address1').value = dataarray[0];
    $$('address2').value = dataarray[1];
    $$('town').value = dataarray[2];
    $$('postcodeIndex').value = $$('postcode').value;
    setSuccessMsgs("address1Feild", "w100p fl SusMsg");
    setSuccessMsgs("address2Feild", "w100p fl SusMsg");
    setSuccessMsgs("townField", "w50p fl SusMsg");
    setSuccessMsgs("postcodeIndexField", "w50p fr SusMsg");
    setSuccessMsgs("postaddid", "w100p fr SusMsg");
  }else{
    $$('address1').value = "Address line 1";
    $$('address2').value = "Address line 2";
    $$('town').value = "Please enter town/city";
    $$('postcodeIndex').value = "Please enter";    
    setSuccessMsgs("address1Feild", "w100p fl");
    setSuccessMsgs("address2Feild", "w100p fl");
    setSuccessMsgs("townField", "w50p fl");
    setErrorAndSuccessMsgs("townField", "w50p fl", "townErrMsg", "<p></p>");
    setSuccessMsgs("postcodeIndexField", "w50p fr");
    setSuccessMsgs("postaddid", "w100p fr");
    alert('Choose valid address from list');
  }
}

function setAttributeDDValue(attributeBufferIds){  
  var selectebufferIds = attributeBufferIds;
  if(selectebufferIds != null && selectebufferIds != ""){
    var splitselected = selectebufferIds.split("##");
    for(var i=0;i<splitselected.length;i++){
      var splitselectId = splitselected[i];
      var el = $$(splitselectId);
      var selectSpanId = "dropsapn_"+splitselectId;
      if ($$(splitselectId)){
        var text = el.options[el.selectedIndex].text;        
        $$(selectSpanId).innerHTML = text;
      }
    }
  }
}
function createSelectBox(selectBoxId, count){
  if($$(selectBoxId) != null){
    $$(selectBoxId).innerHTML = "";
    for(var i = 0; i <= count; i++){
      var option = document.createElement("option");
      option.value = ""+i;
      option.appendChild(document.createTextNode(i));
      $$(selectBoxId).appendChild(option);
    }    
  }
}
function onchangeQual(obj){
  var qualification = obj.value;  
  var valueArray = {"Alevels":5, "SQAHighers":8, "SQAAdvancedHighers":8, "BTEC":3};
  if(obj.value == null || obj.value == '' ){
    $$("astarab").style.display = "none";
    $$("cde").style.display = "none";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
    setErrorAndSuccessMsgs("gradesMsg", "row-fluid row2 qlf-nwfs grads", "gradesErrMsg", "<p></p>");
  }else if(obj.value == '74'){
    var count = valueArray.Alevels;
    createSelectBox('A*select', count);  
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count);  
    createSelectBox('Eselect', count);      
    
    $$("astarab").style.display = "inline-block";
    $$("cde").style.display = "inline-block";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "block";
    $$("Amainspan").style.display = "block";
    //$$("Amainspan").className = "pfgrades";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "block";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
    
    if($$("A*span")){$$("A*span").innerHTML = '0';}
    if($$("Aspan")){$$("Aspan").innerHTML = '0';}
    if($$("Bspan")){$$("Bspan").innerHTML = '0';}
    if($$("Cspan")){$$("Cspan").innerHTML = '0';}
    if($$("Dspan")){$$("Dspan").innerHTML = '0';}
    if($$("Espan")){$$("Espan").innerHTML = '0';}
    setErrorAndSuccessMsgs("gradesMsg", "row-fluid row2 qlf-nwfs grads", "gradesErrMsg", "<p></p>");
  }else if(obj.value == '75' || obj.value == '76'){
    var count = valueArray.SQAHighers;

      createSelectBox('Aselect', count);  
      createSelectBox('Bselect', count);  
      createSelectBox('Cselect', count);  
      createSelectBox('Dselect', count);      
    
    $$("astarab").style.display = "inline-block";
    $$("cde").style.display = "inline-block";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "block";
    //$$("Amainspan").className = "";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "none";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
    
    if($$("Aspan")){$$("Aspan").innerHTML = '0';}
    if($$("Bspan")){$$("Bspan").innerHTML = '0';}
    if($$("Cspan")){$$("Cspan").innerHTML = '0';}
    if($$("Dspan")){$$("Dspan").innerHTML = '0';}
    setErrorAndSuccessMsgs("gradesMsg", "row-fluid row2 qlf-nwfs grads", "gradesErrMsg", "<p></p>");
  }else if(obj.value == '555'){
    var count = valueArray.BTEC;

      createSelectBox('D*select', count);  
      createSelectBox('BtecDselect', count);  
      createSelectBox('Mselect', count);  
      createSelectBox('Pselect', count);        

    $$("astarab").style.display = "none";
    $$("cde").style.display = "none";
    $$("dstard").style.display = "inline-block";
    $$("mp").style.display = "inline-block";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    //$$("Amainspan").className = "";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";
    
    $$("D*mainspan").style.display = "block";
    $$("BtecDmainspan").style.display = "block";    
    $$("Mmainspan").style.display = "block";
    $$("Pmainspan").style.display = "block";
    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
    
    if($$("D*span")){$$("D*span").innerHTML = '0';}
    if($$("BtecDspan")){$$("BtecDspan").innerHTML = '0';}
    if($$("Mspan")){$$("Mspan").innerHTML = '0';}
    if($$("Pspan")){$$("Pspan").innerHTML = '0';}
    setErrorAndSuccessMsgs("gradesMsg", "row-fluid row2 qlf-nwfs grads", "gradesErrMsg", "<p></p>");
  }else if(obj.value == '77'){
    $$("astarab").style.display = "none";
    $$("cde").style.display = "none";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";    
    
    $$("ucaspoins").style.display = "block";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";
    setErrorAndSuccessMsgs("gradesMsg", "row-fluid row2 qlf-nwfs grads", "gradesErrMsg", "<p></p>");
  }else if(obj.value == '78'){
    $$("astarab").style.display = "none";
    $$("cde").style.display = "none";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "block";
    $$("other").style.display = "none";
    setErrorAndSuccessMsgs("gradesMsg", "row-fluid row2 qlf-nwfs grads", "gradesErrMsg", "<p></p>");
  }else{
    $$("astarab").style.display = "none";
    $$("cde").style.display = "none";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
        
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "block";
    setErrorAndSuccessMsgs("gradesMsg", "row-fluid row2 qlf-nwfs grads", "gradesErrMsg", "<p></p>");
  }
}
function setGradesEntryPointsValue(){  
  var qual = "";
  var valueArray = {"Alevels":5, "SQAHighers":8, "SQAAdvancedHighers":8, "BTEC":3 };
  var mutisetVal = $$('multi_set') != null ? $$('multi_set').value : "";
    var astar = mutisetVal != null && mutisetVal.indexOf("A*") > -1 ? mutisetVal.substring(mutisetVal.indexOf("A*>")+3, mutisetVal.indexOf("A*>")+4) : "0";
    var avalue = mutisetVal != null && mutisetVal.indexOf("<A>") > -1 ? mutisetVal.substring(mutisetVal.indexOf("A>")+2, mutisetVal.indexOf("A>")+3) : "0";
    var bvalue = mutisetVal != null && mutisetVal.indexOf("B") > -1 ? mutisetVal.substring(mutisetVal.indexOf("B")+2, mutisetVal.indexOf("B")+3) : "0";
    var cvalue = mutisetVal != null && mutisetVal.indexOf("C") > -1 ? mutisetVal.substring(mutisetVal.indexOf("C")+2, mutisetVal.indexOf("C")+3) : "0";
    var dvalue = mutisetVal != null && mutisetVal.indexOf("D") > -1 ? mutisetVal.substring(mutisetVal.indexOf("D")+2, mutisetVal.indexOf("D")+3) : "0";
    var evalue = mutisetVal != null && mutisetVal.indexOf("E") > -1 ? mutisetVal.substring(mutisetVal.indexOf("E")+2, mutisetVal.indexOf("E")+3) : "0";
    
    var dstarvalue = mutisetVal != null && mutisetVal.indexOf("D*") > -1 ? mutisetVal.substring(mutisetVal.indexOf("D*>")+3, mutisetVal.indexOf("D*>")+4) : "0";
    var btecdvalue = mutisetVal != null && mutisetVal.indexOf("<D>") > -1 ? mutisetVal.substring(mutisetVal.indexOf("D>")+2, mutisetVal.indexOf("D>")+3) : "0";
    var mvalue = mutisetVal != null && mutisetVal.indexOf("M") > -1 ? mutisetVal.substring(mutisetVal.indexOf("M")+2, mutisetVal.indexOf("M")+3) : "0";
    var pvalue = mutisetVal != null && mutisetVal.indexOf("P") > -1 ? mutisetVal.substring(mutisetVal.indexOf("P")+2, mutisetVal.indexOf("P")+3) : "0";
    
    var freeText = mutisetVal != null && mutisetVal.indexOf("free-text") > -1 ? mutisetVal.substring(mutisetVal.indexOf("<free-text>")+11, mutisetVal.indexOf("</free-text>")) : "";
    if(qual == '77'){
      $$("ucaspoins").value = freeText;
    }else if(qual == '78'){
      $$("bacc").value = freeText;
    }else if(qual == '79'){
      $$("other").value = freeText;
    }    
 if(qual == '74'){
    var count = valueArray.Alevels;
    createSelectBox('A*select', count);  
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count);  
    createSelectBox('Eselect', count);  

    $$("A*select").value = astar;
    $$("Aselect").value = avalue;
    $$("Bselect").value = bvalue;
    $$("Cselect").value = cvalue;
    $$("Dselect").value = dvalue;
    $$("Eselect").value = evalue;
    
    $$("astarab").style.display = "inline-block";
    $$("cde").style.display = "inline-block";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "block";
    $$("Amainspan").style.display = "block";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "block";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    
    $$("A*span").innerHTML = astar;
    $$("Aspan").innerHTML = avalue;
    $$("Bspan").innerHTML = bvalue;
    $$("Cspan").innerHTML = cvalue;
    $$("Dspan").innerHTML = dvalue;
    $$("Espan").innerHTML = evalue;
    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";

 }else if(qual == '75' || qual == '76'){
    var count = valueArray.SQAHighers;
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count); 

    $$("Aselect").value = avalue;
    $$("Bselect").value = bvalue;
    $$("Cselect").value = cvalue;
    $$("Dselect").value = dvalue;
    
    $$("astarab").style.display = "inline-block";
    $$("cde").style.display = "inline-block";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "block";
    //$$("Amainspan").className = "";
    $$("Bmainspan").style.display = "block";
    $$("Cmainspan").style.display = "block";
    $$("Dmainspan").style.display = "block";
    $$("Emainspan").style.display = "none";
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";

    $$("Aspan").innerHTML = avalue;
    $$("Bspan").innerHTML = bvalue;
    $$("Cspan").innerHTML = cvalue;
    $$("Dspan").innerHTML = dvalue;
    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";

  }else if(qual == '555'){
    var count = valueArray.BTEC;
    createSelectBox('D*select', count);  
    createSelectBox('BtecDselect', count);  
    createSelectBox('Mselect', count);  
    createSelectBox('Pselect', count); 

    $$("D*select").value = dstarvalue;
    $$("BtecDselect").value = btecdvalue;
    $$("Mselect").value = mvalue;
    $$("Pselect").value = pvalue;
    
    $$("astarab").style.display = "none";
    $$("cde").style.display = "none";
    $$("dstard").style.display = "inline-block";
    $$("mp").style.display = "inline-block";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    //$$("Amainspan").className = "";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";    
    $$("D*mainspan").style.display = "block";    
    $$("BtecDmainspan").style.display = "block";    
    $$("Mmainspan").style.display = "block";
    $$("Pmainspan").style.display = "block";

    $$("D*span").innerHTML = dstarvalue;
    $$("BtecDspan").innerHTML = btecdvalue;
    $$("Mspan").innerHTML = mvalue;
    $$("Pspan").innerHTML = pvalue;
    
    $$("ucaspoins").style.display = "none";
    $$("bacc").style.display = "none";
    $$("other").style.display = "none";

  }else if(qual == '77' || qual == '78' || qual == '79'){
    
    $$("astarab").style.display = "none";
    $$("cde").style.display = "none";
    $$("dstard").style.display = "none";
    $$("mp").style.display = "none";
    $$("A*mainspan").style.display = "none";
    $$("Amainspan").style.display = "none";
    $$("Bmainspan").style.display = "none";
    $$("Cmainspan").style.display = "none";
    $$("Dmainspan").style.display = "none";
    $$("Emainspan").style.display = "none";    
    $$("D*mainspan").style.display = "none";
    $$("BtecDmainspan").style.display = "none";    
    $$("Mmainspan").style.display = "none";
    $$("Pmainspan").style.display = "none";
    
    if(qual == '77'){
      $$("ucaspoins").style.display = "block";
    }else if(qual == '78'){
      $$("bacc").style.display = "block";
    }else if(qual == '79'){
      $$("other").style.display = "block";
    }
  }
}
function getGradesEntryPoints(){  
 window.onbeforeunload = null;
 //Bug fix for course name not prepopulating in my profile page by Hemalatha.K on 17_DEC_2019_REL
 if(jq('#NON_UK_UNIVERSITY_FLAG').is(':checked')) {
   jq('#firstCourseId_display').val(jq('#firstCourseId').val()); 
 }
 return true;
}
function createGradesSelectEnquiry(gradeType, count){
  if(gradeType == "A-levels"){
    createSelectBox('A*select', count);  
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count);  
    createSelectBox('Eselect', count);  
    if($$("A*span")){$$("A*span").innerHTML = '0';}
    if($$("Aspan")){$$("Aspan").innerHTML = '0';}
    if($$("Bspan")){$$("Bspan").innerHTML = '0';}
    if($$("Cspan")){$$("Cspan").innerHTML = '0';}
    if($$("Dspan")){$$("Dspan").innerHTML = '0';}
    if($$("Espan")){$$("Espan").innerHTML = '0';}    
  }else if(gradeType == "SQA Highers" || gradeType == "SQA Advanced Highers"){
    createSelectBox('Aselect', count);  
    createSelectBox('Bselect', count);  
    createSelectBox('Cselect', count);  
    createSelectBox('Dselect', count);  
    if($$("Aspan")){$$("Aspan").innerHTML = '0';}
    if($$("Bspan")){$$("Bspan").innerHTML = '0';}
    if($$("Cspan")){$$("Cspan").innerHTML = '0';}
    if($$("Dspan")){$$("Dspan").innerHTML = '0';}    
  }if(gradeType == "BTEC"){
    createSelectBox('D*select', count);  
    createSelectBox('BtecDselect', count);  
    createSelectBox('Mselect', count);  
    createSelectBox('Pselect', count);      
    if($$("D*span")){$$("D*span").innerHTML = '0';}
    if($$("BtecDspan")){$$("BtecDspan").innerHTML = '0';}
    if($$("Mspan")){$$("Mspan").innerHTML = '0';}
    if($$("Pspan")){$$("Pspan").innerHTML = '0';}    
  }
}

function clearDefault(obj, text){
   var id = obj.id;
  if($$(id).value == text){
    $$(id).value = "";
	obj.style.color = "#333";
  }
}
function setDefault(obj, text){
  var id = obj.id;
  if($$(id).value == ""){
    $$(id).value = text;
	obj.style.color = "#888";
  }
}
function hideandshowAddFinder(obj){
  var countryOfRes = obj.value;
  if(countryOfRes != null && (countryOfRes == '210' || countryOfRes == '55' || countryOfRes == '139' || countryOfRes == '256' || countryOfRes == '257')){    
    $$('addFinder').style.display= 'block';
    $$('postcodeField').style.display= 'block';
    $$('postcodeSection').style.display= 'block'; 
    $$('address_field_1').style.display= 'block';
    $$('address_field_2').style.display= 'block';
  }else{
	$$('address_field_1').style.display= 'block';
	$$('address_field_2').style.display= 'block';
    $$('addFinder').style.display= 'none';
    $$('postaddid').style.display= 'none';
    $$('postcodeField').style.display= 'none';
    $$('postcodeSection').style.display= 'none';    
    $$('address1').value = "Address line 1";
    $$('address2').value = "Address line 2";
    $$('town').value = "Please enter town/city";   
    $$('postcode').value = "Enter UK postcode";   
    $$('postcodeIndex').value = "Please enter";
    setSuccessMsgs("address1Feild", "w100p fl");
    setSuccessMsgs("address2Feild", "w100p fl");
    setSuccessMsgs("townField", "w50p fl");
    setErrorAndSuccessMsgs("townField", "w50p fl", "townErrMsg", "<p></p>");
    setSuccessMsgs("postcodeIndexField", "w50p fr");
    setSuccessMsgs("postaddid", "w100p fr");
  }
}

function savePersonalNotes(userId, collegeId, courseId, pnType, noteid){
  var userId = userId;
  var collegeId = collegeId;
  var courseId = courseId;
  var enquiryType = pnType;
  var personalnotes = $$(noteid).value;
  personalnotes = personalnotes.trim();
  if(personalnotes == null || personalnotes == "" || personalnotes == 'Enter any comments here'){
    alert('Please enter your personal notes.');
    return false;
  }
  var url="/degrees/savepersonalnotes.html?courseId="+courseId+"&userId="+userId+"&collegeId="+collegeId+"&etype="+enquiryType+"&pernote="+escape(personalnotes);
  //alert("url--------"+url)
  var ajax=new sack();
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ savepersonalNotesResponse(ajax, noteid, personalnotes); };	
   ajax.runAJAX();
 }  
function savepersonalNotesResponse(ajax, noteId, pnote){
  $$(noteId).innerHTML = pnote;
  $$(noteId+"_s").innerHTML = "Your notes have been saved";
}

function selectEnquiryOrderBy(obj, type){
  var orderval = obj.value;
  if(orderval == "" || orderval == ''){
    alert('Pelase select proper order by value.');
    return false;
  }
  location.href = "/degrees/mywhatuni.html?tabname="+type+"&enqOrder="+orderval;
}

function increaseRows(obj, length, e){
  var id = obj.id;
  if($$(id).value.length%length == 0){    
    $$(id).rows = $$(id).rows + 1;
  }else if(e.keyCode==13){    
    $$(id).rows = $$(id).rows + 1;
  }
}

function mywhatuniLimitCharCount(limitField, limitNum) {
	var limitFieldLength = limitField.value.length
    if ( limitFieldLength > limitNum) {	
        limitField.value = limitField.value.substring(0, limitNum);
    }
	return false;	
}
function autocompleteOff(element) {
	self.focus(); //Bring window to foreground if necessary.
	var form = $$(element); // grab the form element.
	if(form)
	{
		form.setAttribute("autocomplete","off"); // Turn off Autocomplete for the form, if the browser supports it.
  }
}
function updatePassword(){
 $$("loadinggifreg").style.display="block";
 $$("newPwdErrorMsgdiv").style.display="none";
 $$("confirmPwdErrorMsgdiv").style.display="none";
 $$("errorOldMsgdiv").style.display="none";
 var oldpwd =  trimString($$("poppassword").value);  
  var msg = "Y"; 
  if(oldpwd == ""){
    var errorMsg = "Current password field cannot be left blank";$$("errorOldMsgdiv").innerHTML = errorMsg;$$("errorOldMsgdiv").style.display="block"; msg = "N";
  }
  var pwd =  trimString($$("newpassword").value);
  var confirmPassword = trimString($$("confirmPassword").value);
  var errorMsg = ""; 
  if(confirmPassword == ""){ msg = "N"; errorMsg = "Re-enter Password field cannot be left blank";$$("confirmPwdErrorMsgdiv").innerHTML = errorMsg;$$("confirmPwdErrorMsgdiv").style.display="block";}
   if(pwd == ""){ msg = "N"; errorMsg = "New password field cannot be left blank";$$("newPwdErrorMsgdiv").innerHTML = errorMsg;$$("newPwdErrorMsgdiv").style.display="block"; }
  if( msg == "N"){ 
    $$("loadinggifreg").style.display="none";
    return false;
  }else if(pwd == oldpwd){
   $$("newPwdErrorMsgdiv").innerHTML = "The values entered in the Current and New password cannot be the same, please try again";
   $$("newPwdErrorMsgdiv").style.display="block";
    $$("loadinggifreg").style.display="none";
    return false;  
  }else if(pwd != confirmPassword){
   $$("confirmPwdErrorMsgdiv").innerHTML = "The New password fields do not match, please try again";
   $$("confirmPwdErrorMsgdiv").style.display="block";
   $$("loadinggifreg").style.display="none";
   return false;
  }
  var encryptedNewPwd = encryptValues(pwd);
  var encryptedOldPwd = encryptValues(oldpwd);
  var ajax=new sack();
  url = contextPath + "/changePassword.html?method=updatePassword&pwd=" + encodeURIComponent(encryptedNewPwd)+"&oldpwd="+encodeURIComponent(encryptedOldPwd);
  ajax.requestFile = url;
  ajax.onCompletion = function(){ updatePasswordAjax(ajax); };
  ajax.runAJAX();
}
function updatePasswordAjax(ajax){
  var response = ajax.response;
  $$("loadinggifreg").style.display="none";
  if(response == "SUCCESS"){
    $$("changepassword").style.display="none";
    $$("sucessmessageblock").style.display="block";
  }else{
    var errorMsg = "Password entered does not match our records"
    $$("errorOldMsgdiv").innerHTML = errorMsg;
    $$("errorOldMsgdiv").style.display="block";   
  }
}
function encryptValues(value){
  var xorKey = 129; 
  var encryptedValue = ""; 
  for (var i = 0; i < value.length; ++i) {
    encryptedValue += String.fromCharCode(xorKey ^ value.charCodeAt(i));
  }  
  return encryptedValue;
}
function addElement(element, name, value){
 	if($$(element)){
		$$(element).options[$$(element).options.length]=new Option(name,value);
	}
}
function loadPaginationData(tabName, pageNo, orderby){
  var url='/degrees/mywhatuni.html?tabname='+tabName+"&pageno="+pageNo;
  if(orderby != null && orderby != ""){
    url = url +"&enqOrder="+orderby;
  }
  location.href = url;
}
function checkImageFromat(){
    var extensions = new Array("jpg","jpeg");
    var image_file = $$("userPhotoUpload").value;    
    if(image_file.length>0){
    var pos = image_file.lastIndexOf('.') + 1;
    var ext = image_file.substring(pos, image_file.image_length);
    var final_ext = ext.toLowerCase();
    for (i = 0; i < extensions.length; i++){
      if(extensions[i] == final_ext){
        return true;
      }
    }    
    return false;
    }else{ 
    return true;
    }
  }
function uploadedFileValidations(){  
  var input, file;  
  if (!window.FileReader) {
      $$("userPic").className = "row-fluid row1 ErrMsg";
      $$("userPicErrMsg").innerHTML = "<p>Oops, the file API isn't supported on this browser yet..</p>";
      if($$('load_icon')){
          $$('load_icon').style.display = 'none';
        }
      $$("userPicErrMsg").style.display = "block";
      return false;
  }
  input = $$('userPhotoUpload');
  if (!input) {      
      $$("userPic").className = "row-fluid row1 ErrMsg";
      $$("userPicErrMsg").innerHTML = "<p>Oops, couldn't find the file input element.</p>";
      if($$('load_icon')){
          $$('load_icon').style.display = 'none';
        }
      $$("userPicErrMsg").style.display = "block";
      return false;
  }else {    
      file = input.files[0];
      var size = file.size;
      if(!checkImageFromat()){
        $$("userPic").className = "row-fluid row1 ErrMsg";
        $$("userPicErrMsg").innerHTML = "<p>Sorry, but we cannot upload this picture - we only accept pictures in JPEG format. Please try again with a JPEG version.</p>";
        if($$('load_icon')){
          $$('load_icon').style.display = 'none';
        }
        $$("userPicErrMsg").style.display = "block";
        return false;
      }else if(size > 2097152){
        $$("userPic").className = "row-fluid row1 ErrMsg";
        $$("userPicErrMsg").innerHTML = "<p>Sorry, but we cannot accept pictures larger than 2 megabytes in size. Please try again with a smaller picture.</p>";
        if($$('load_icon')){
          $$('load_icon').style.display = 'none';
        }
        $$("userPicErrMsg").style.display = "block";
        return false;
      }else{        
        $$("userPic").className = "row-fluid row1 SusMsg";
        $$("userPicErrMsg").innerHTML = "<p>Looking good.</p>";
        $$("userPicErrMsg").style.display = "block";
        return true;
      }      
  }  
}
var newWindow = null;
function loadUserImageUploadPopup(){  
  var newWindow = window.open("","_blank");  
  newWindow.location.href = "/degrees/mywhatuni.html?butValue=imageupload";  
}
var submitFlag = false; 
 function uploadUserImageAjax(obj) {
  var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";//Added wu_scheme by Indumathi.S Mar-29-2016
  var loadImg = '<img src="'+wu_scheme+document.domain+'/wu-cont/images/ldr.gif"/>';
  if($$('load_icon')){
    $$('load_icon').style.display = 'block';
  }
  var imageCropFlag;
  if(document.documentElement.clientWidth <= 992){
    imageCropFlag = "No";
  }else{
    imageCropFlag = "Yes";
  }
   if(uploadedFileValidations()){
   jq("#mywhatuniId").on('submit',(function(e) {
    if(!submitFlag){    
        e.preventDefault();
        var formObj = jq(this);   
        var inputArr = {};
        var formURL = formObj.attr("action")+"?butValue=AjaxUpload&imageCropFlag="+imageCropFlag;
     inputArr.butvalue="AjaxUpload";
     inputArr.imageCropFlag = imageCropFlag;
        jq.ajax({
            url: formURL,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(res){ 
            	submitFlag = false;
              if(imageCropFlag.toLowerCase() == "yes" && document.documentElement.clientWidth > 992){
                location.href = "/degrees/mywhatuni.html?butValue=imageupload&data="+res;
              }else{
                location.href = "/degrees/mywhatuni.html";
              }
            }           
         });
         submitFlag = true;
         }
      }));
   jq("#mywhatuniId").submit();
   }
 }
jq(document).ready(function()	{
		jq(".info_tit").click(function(){
			jq(this).next(".sett_cnr").toggleClass('show_off');
      jq(this).next(".seton_cont").toggleClass('show_off');
			jq(this).find(".fa").toggleClass('fa-plus-circle');
		});	
  jq("#hd1").removeAttr("style");
  jq('.lft_nav .brw').click(function(){
  jq(this).toggleClass('act');
  jq(this).parents().next().toggleClass('act');
 });
});	
function setDefaultProfile(obj, text){  
  if(trimString($$(obj).value) == ""){
    $$(obj).value = text;
  }
}
function clearDefaultProfile(obj, text){   
  if(trimString($$(obj).value) == text){
    $$(obj).value = "";
	   $$(obj).style.color = "#333";
  }
}
function setSelectedDate(obj, spanid){    
  var dobid = obj.id  
  var el = $$(dobid);
  var text = el.options[el.selectedIndex].text;
  $$(spanid).innerHTML = text;
  unsavedMail = true;
}
function profileValidations(id){
  if(id == "firstName" || id == "lastName"){
    var errMsgClass;
    var susMsgClass;
    if(id == "firstName"){
      errMsgClass = "w50p ErrMsg fl";
      susMsgClass="w50p SusMsg fl";
      classNameId = "userFName";
      msgId = "firstNameErrMsg";
    }else if(id == "lastName"){
      errMsgClass = "w50p ErrMsg fr";
      susMsgClass="w50p SusMsg fr";
      classNameId = "userLName";
      msgId = "lastNameErrMsg";
    }    
    if(trimString($$(id).value) == "" || trimString($$(id).value).toLowerCase() == "first name" || trimString($$(id).value).toLowerCase() == "last name"){
      setErrorAndSuccessMsgs(classNameId, errMsgClass, msgId, "<p>Come on, no need to be shy...</p>");
    }else{
      var msg = "<p>That's a top name, "+trimString($$(id).value)+".</p>";
      setErrorAndSuccessMsgs(classNameId, susMsgClass, msgId, msg);
    }
  }
  if(id == "yeartoJoinCourse1" || id == "yeartoJoinCourse2" || id == "yeartoJoinCourse3" || id == "yeartoJoinCourse4"){
    var yoe = '';
    for (i=0; i<document.getElementsByTagName('input').length; i++) {
      if (document.getElementsByTagName('input')[i].type == 'radio'){
        if(document.getElementsByTagName('input')[i].checked == true){
         yoe = document.getElementsByTagName('input')[i].value;
         break;
        }
      } 
    }
    var cuYear = new Date().getFullYear();
    if(yoe < cuYear){
      setErrorAndSuccessMsgs("yoeFeild", "ql-inp pt10 ErrMsg", "yoeErrMsg", "<p>Please choose when would you like to start?</p>");
      $$("yoeFeilds").scrollIntoView();
      return false;
    }else{
      setErrorAndSuccessMsgs("yoeFeild", "ql-inp pt10", "yoeErrMsg", "");
    }
  }
  if(id == "dateDOB" || id == "monthDOB" || id == "yearDOB"){       
    var elDate = $$("dateDOB");
    var textDate = elDate.options[elDate.selectedIndex].text;
    var elMonth = $$("monthDOB");    
    var textMonth = elMonth.options[elMonth.selectedIndex].text;
    var elYear = $$("yearDOB");
    var textYear = elYear.options[elYear.selectedIndex].text;        
    var datemsg = "";    
    var inputDate = textDate+"/"+elMonth.selectedIndex+"/"+textYear;
    datemsg = isValidDOBDate(inputDate,false,id);    
    if (datemsg == "") {
        getAge(inputDate);        
    }else if(datemsg != "calling_onchange") {
      setErrorAndSuccessMsgs("userDOB", "w100p fl ErrMsg", "userDobErrMsg", "<p>"+datemsg+"</p>");
    }
  }
  if(id == "nationality"){
    var nationalityArray = {French_Guyana:"Bonjour!", French_Polynesia:"Bonjour!", Italy:"Ciao!", Spain:"Hola!", Germany:"Guten Tag!", American_Samoa:"Sup!", India:"Namaste"};
    var elNationality = $$("nationality");
    var textelNationality = elNationality.options[elNationality.selectedIndex].text;    
    var nationalityText = trimString(textelNationality.replace(/\s/g,'_'));    
    if(nationalityArray[nationalityText] != undefined){
      setErrorAndSuccessMsgs("userNationality", "w100p SusMsg fr", "userNationalityErrMsg", "<p>"+nationalityArray[nationalityText]+"</p>");
    }else{      
      if(textelNationality == "Nationality"){    
        setErrorAndSuccessMsgs("userNationality", "w100p ErrMsg fr", "userNationalityErrMsg", "<p>What parts are you from, friend?</p>");        
      }else{
        setErrorAndSuccessMsgs("userNationality", "w100p SusMsg fr", "userNationalityErrMsg", "<p></p>");
      }
    }
  }
  if(id == "countryOfResidence"){
    var nationalityArray = {French_Guyana:"Bonjour!", French_Polynesia:"Bonjour!", Italy:"Ciao!", Spain:"Hola!", Germany:"Guten Tag!", American_Samoa:"Sup!", India:"Namaste"};
    var elNationality = $$("countryOfResidence");
    var textelNationality = elNationality.options[elNationality.selectedIndex].text;    
    var nationalityText = trimString(textelNationality.replace(/\s/g,'_'));    
    if(nationalityArray[nationalityText] != undefined){
      setErrorAndSuccessMsgs("userCountryOfResidence", "w100p SusMsg fr", "userCountryOfResidenceErrMsg", "<p>"+nationalityArray[nationalityText]+"</p>");
    }else{      
      if(textelNationality == "Please select"){    
        setErrorAndSuccessMsgs("userCountryOfResidence", "w100p ErrMsg fr", "userCountryOfResidenceErrMsg", "<p>What parts are you from, friend?</p>");        
      }else{
        setErrorAndSuccessMsgs("userCountryOfResidence", "w100p SusMsg fr", "userCountryOfResidenceErrMsg", "<p></p>");
      }
    }
  }  
  if(id == "town"){    
    if(trimString($$(id).value) != "" && trimString($$(id).value).toLowerCase() != "please enter town/city"){      
        setErrorAndSuccessMsgs("townField", "w50p fl SusMsg", "townErrMsg", "<p>Lovely neighbourhood.</p>");
    }else{
      setErrorAndSuccessMsgs("townField", "w50p fl", "townErrMsg", "<p></p>");
    }
  }
  var postcodeErrMsg = "<p>Hmm, that doesn't seem to be a real postcode.</p>";  
  if(id == "postcode"){    
    if(trimString($$(id).value) != "" && trimString($$(id).value).toLowerCase() != "enter uk postcode"){      
      var retMsg = isValidUKpostcodeWithoutSpace(trimString($$(id).value));
      if(retMsg){
        setErrorAndSuccessMsgs("postcodeField", "w50p fr SusMsg", "postcodeErrMsg", "");
      }else{
        setErrorAndSuccessMsgs("postcodeField", "w50p fr ErrMsg", "postcodeErrMsg", postcodeErrMsg);
      }
    }    
  }
  if(id == "postcodeIndex"){   
    if(trimString($$(id).value) != "" && trimString($$(id).value).toLowerCase() != "please enter"){   
      var retMsg = isValidUKpostcodeWithoutSpace(trimString($$(id).value));
      if(retMsg){   
        setErrorAndSuccessMsgs("postcodeIndexField", "w50p fr SusMsg", "postcodeIndexErrMsg", "");
      }else{
        setErrorAndSuccessMsgs("postcodeIndexField", "w50p fr ErrMsg", "postcodeIndexErrMsg", postcodeErrMsg);
      }
    }
  }
}
function profileSubmitValidations(){
  var fnObj = $$("firstName");
  var lnObj = $$("lastName");
  if(fnObj.id == "firstName" || lnObj.id == "lastName"){
    var errMsgClass;
    var susMsgClass;
    if(fnObj.id == "firstName"){
      errMsgClass = "w50p ErrMsg fl";      
      classNameId = "userFName";
      msgId = "firstNameErrMsg";
    }else if(lnObj.id == "lastName"){
      errMsgClass = "w50p ErrMsg fr";      
      classNameId = "userLName";
      msgId = "lastNameErrMsg";
    }
    if(trimString($$(fnObj.id).value) == "" || trimString($$(lnObj.id).value) == "" || trimString($$(fnObj.id).value).toLowerCase() == "first name" || trimString($$(lnObj.id).value).toLowerCase() == "last name"
    || trimString($$(fnObj.id).value).toLowerCase() == "last name" || trimString($$(lnObj.id).value).toLowerCase() == "first name"){      
      setErrorAndSuccessMsgs(classNameId, errMsgClass, msgId, "<p>Come on, no need to be shy...</p>");
      clearLightBoxVal();
      $$(fnObj.id).scrollIntoView();
      return false;
    }
  }
  var yoe = '';
  for (i=0; i<document.getElementsByTagName('input').length; i++) {
    if (document.getElementsByTagName('input')[i].type == 'radio'){
      if(document.getElementsByTagName('input')[i].checked == true){
       yoe = document.getElementsByTagName('input')[i].value;
       break;
      }
    } 
  }
  var cuYear = new Date().getFullYear();
  if(yoe < cuYear){
    setErrorAndSuccessMsgs("yoeFeild", "ql-inp pt10 ErrMsg", "yoeErrMsg", "<p>Please choose when would you like to start?</p>");
    $$("yoeFeilds").scrollIntoView();
    return false;
  }else{
    setErrorAndSuccessMsgs("yoeFeild", "ql-inp pt10", "yoeErrMsg", "");
  }
  var elDate = $$("dateDOB");
  var textDate = elDate.options[elDate.selectedIndex].text;
  var elMonth = $$("monthDOB");    
  var textMonth = elMonth.options[elMonth.selectedIndex].text;
  var elYear = $$("yearDOB");
  var textYear = elYear.options[elYear.selectedIndex].text;        
  var datemsg = "";
  if(elDate.options[elDate.selectedIndex].value != '' || elMonth.options[elMonth.selectedIndex].value != '' || elYear.options[elYear.selectedIndex].value != ''){
  var inputDate = textDate+"/"+elMonth.selectedIndex+"/"+textYear;
  datemsg = isValidDOBDate(inputDate,true);    
  if (datemsg != "") {      
    setErrorAndSuccessMsgs("userDOB", "w100p fl ErrMsg", "userDobErrMsg", "<p>"+datemsg+"</p>");
    clearLightBoxVal();
    $$("dateDOB").scrollIntoView();
    return false;
  }
  }
  var elNationality = $$("nationality");
  var textelNationality = elNationality.options[elNationality.selectedIndex].text;
 
  var postcodeErrMsg = "<p>Hmm, that doesn't seem to be a real postcode.</p>";
  if(trimString($$("postcode").value) != "" && trimString($$("postcode").value) != "Enter UK postcode"){
    var pcRetMsg = isValidUKpostcodeWithoutSpace(trimString($$("postcode").value));  
    if(pcRetMsg){
      setErrorAndSuccessMsgs("postcodeField", "w50p fr", "postcodeErrMsg", "<p></p>");
    }else{
      setErrorAndSuccessMsgs("postcodeField", "w50p fr ErrMsg", "postcodeErrMsg", postcodeErrMsg);
      clearLightBoxVal();
      $$("postcode").scrollIntoView();
      return false;
    }
  }
  /*if(trimString($$("postcodeIndex").value) != "" && trimString($$("postcodeIndex").value) != "Please enter"){
    var pciRetMsg = isValidUKpostcodeWithoutSpace(trimString($$("postcodeIndex").value));
    if(pciRetMsg){
      setErrorAndSuccessMsgs("postcodeIndexField", "w50p fr", "postcodeIndexErrMsg", "");
    }else{
      setErrorAndSuccessMsgs("postcodeIndexField", "w50p fr ErrMsg", "postcodeIndexErrMsg", postcodeErrMsg);
      $$("postcodeIndex").scrollIntoView();
      return false;
    }
  }*/
  if($$("ukSchoolName") && (($$("ukSchoolName").value).trim() == "" || ($$("ukSchoolName").value).trim() == "School / college name")){
    if($$("ukSchoolName_hidden")){$$("ukSchoolName_hidden").value = "";}
  }
  yearToJoinCourse();//Added by Indumathi.S Jan-27-16 
  mailingPreference('newsLetters','solusEmail','remainderMail','allMail','friendsActivity', 'surveyMail', 'catActivity'); //Added survey mail on 31_May_2016, By Thiyagu G.
  if($$("mail_a_href").value == "newURL") {
    callSaveUserTimeLine('setURL');
  }
  jq("#mywhatuniId").submit(); 
  return true;
}
function yearToJoinCourse(){  
  var yeartoJoinCourse = document.getElementsByName("yeartoJoinCourse");
  var c = -1;  
  for(var i=0; i < yeartoJoinCourse.length; i++){
     if(yeartoJoinCourse[i].checked) {
        c = i;         
     }
  }
  if (c == -1){    
    $$("yeartoJoinCourseErrMsg").innerHTML = "<p>What year would you like to start your new course?</p>";
    $$("yeartoJoinCourseErrMsg").style.display = "block";
    $$("yeartoJoinCourseErrMsg").scrollIntoView();
  }else{    
    $$("yeartoJoinCourseErrMsg").innerHTML = "";
    $$("yeartoJoinCourseErrMsg").style.display = "none";    
  }  
}
function setErrorAndSuccessMsgs(classNameId, classes, msgId, messages){  
  $$(classNameId).className = classes;
  $$(msgId).innerHTML = messages;
  $$(msgId).style.display = "block";
}
function setSuccessMsgs(classNameId, classes){  
  $$(classNameId).className = classes;  
}
function updateSuccessMessage(obj){
  if(obj.id == "lastName" && ($$(obj.id).value).toLowerCase() != "last name"){
    setSuccessMsgs("userLName", "w50p fr SusMsg");
  }
  if(obj.id == "ukSchoolName" && ($$(obj.id).value).toLowerCase() != "school / college name"){
    setSuccessMsgs("ukSchoolNameFeild", "w100p fl SusMsg");
  }
  if(obj.id == "address1" && ($$(obj.id).value).toLowerCase() != "address line 1"){
    setSuccessMsgs("address1Feild", "w100p fl SusMsg");
  }
  if(obj.id == "address2" && ($$(obj.id).value).toLowerCase() != "address line 2"){
    setSuccessMsgs("address2Feild", "w100p fl SusMsg");
  }
}
function getAge(birth) {    
    var today = new Date();
    var nowyear = today.getFullYear();
    var nowmonth = today.getMonth();
    var nowday = today.getDate(); 
    var dateArray = birth.split("/");
    var birthday = dateArray[0];
    var birthmonth = dateArray[1];
    var birthyear = dateArray[2];
    var age = nowyear - birthyear;
    var age_month = nowmonth - birthmonth;
    var age_day = nowday - birthday;
    var err_msg = "Sorry, you must be over 13 to use this site";
    if(age == 13 && birthmonth > (today.getMonth()+1)){   
      setErrorAndSuccessMsgs("userDOB", "w100p fl ErrMsg", "userDobErrMsg", err_msg);      
    } else if(age == 13 && birthmonth >= (today.getMonth()+1) && parseInt(birthday) > today.getDate()){
      setErrorAndSuccessMsgs("userDOB", "w100p fl ErrMsg", "userDobErrMsg", err_msg);
    } else if(age < 13){
      setErrorAndSuccessMsgs("userDOB", "w100p fl ErrMsg", "userDobErrMsg", err_msg);
    }else if ((age == 18 && age_month <= 0 && age_day <=0) || age < 18) {
        setErrorAndSuccessMsgs("userDOB", "w100p fl", "userDobErrMsg", "");
    }else if(age >= 18 && age <= 25){
       setErrorAndSuccessMsgs("userDOB", "w100p fl SusMsg", "userDobErrMsg", "<p>Wow, you're ancient.</p>");
    }else {
        setErrorAndSuccessMsgs("userDOB", "w100p fl SusMsg", "userDobErrMsg", "<p>Wow! You don't look a day older than 21.</p>");
    } 
} 
function isValidDOBDate(dateStr, validateEmptyDate,id) {
    var msg = "";    
    var dateArray = dateStr.split("/");        
    day = dateArray[0];
    month = dateArray[1];
    year = dateArray[2];
    var today = new Date();
    if (day == 'Day' || day < 1 || day > 31) {
      if(validateEmptyDate || id == "dateDOB"){
        msg = "Day must be between 1 and 31.";
        return msg;
      }else{
        return "calling_onchange";
      }
    }
    if (month < 1 || month > 12) { // check month range
      if(validateEmptyDate || id == "monthDOB"){
        msg = "Please select a month from the drop down";
        return msg;
      }else{
        return "calling_onchange";
      }
    }  
    if ((month==4 || month==6 || month==9 || month==11) && day==31) {
        msg = "Month "+month+" doesn't have 31 days!";
        return msg;
    }
    if (month == 2) { // check for february 29th
      var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
      if (year != "Year" && (day>29 || (day==29 && !isleap))) {
          msg = "February " + year + " doesn't have " + day + " days!";
          return msg;
      }
    }
    if (year == 'Year') {
      if(validateEmptyDate || id == "yearDOB"){
        msg = "Please select year";
        return msg;
      }else{
        return "calling_onchange";
      }
    }
    var age = today.getFullYear() - year;
    var m = today.getMonth() - month;    
    if(age == 13 && month > (today.getMonth()+1)){
      msg = "Sorry, you must be over 13 to use this site";
      return msg;
    } else if(age == 13 && month >= (today.getMonth()+1) && parseInt(day) > today.getDate()){
      msg = "Sorry, you must be over 13 to use this site";
      return msg;
    } else {
      msg = "";
      return msg;
    }
    return msg;  // date is valid
}
function gradesABBValidation(){
  var gradesSumValue = gradesPointsSum();
  //alert("gradesSumValue : "+gradesSumValue);
  if(gradesSumValue == 1){    
    setErrorAndSuccessMsgs("gradesMsg", "row-fluid row2 qlf-nwfs grads SusMsg", "gradesErrMsg", "<p>"+$$("firstName").value+", you're pretty much a genius.</p>");    
  }else if(gradesSumValue == 2){
    setErrorAndSuccessMsgs("gradesMsg", "row-fluid row2 qlf-nwfs grads", "gradesErrMsg", "<p></p>");    
  }else{
    getGradesEntryPoints();
  }
}
function gradesPointsSum(){
  var index;
  var tempText = 0;
  var tempGradesText = 0;
  var gradePoints;
  var gradePointsText;
  var gradesValue;
  var gradesSum;
  var msgFlag = 0;
  var grades = ["A*select", "Aselect", "Bselect", "Cselect", "Dselect", "Eselect"];
  var gradesValues = {Astarselect:6, Aselect:5, Bselect:4, Cselect:3, Dselect:2, Eselect:1};
  //gradePoints = $$("grade_210");
  /*gradePointsText = gradePoints.options[gradePoints.selectedIndex].text;  
  if("A Levels" == gradePointsText){
    for	(index = 0; index < grades.length; index++) {
      if(grades[index] == "A*select"){
        gradesValue = gradesValues["Astarselect"];
      }else{
        gradesValue = gradesValues[grades[index]];
      }
      gradePoints = $$(grades[index]);
      gradePointsText = gradePoints.options[gradePoints.selectedIndex].text;
      gradesSum = gradesValue * gradePointsText;
      tempText += gradesSum;
      tempGradesText += parseInt(gradePointsText);
    }
  }*/  
  if(tempGradesText != 0 && tempText != 0){
    if(tempGradesText <= 6 && tempText >= 13){
      msgFlag = 1;
    }else if(tempGradesText <= 6 && tempText < 13){
      msgFlag = 2;
    }
  }
  return msgFlag;
}
function isValidUKpostcodeWithoutSpace(postcode){
  var newPostCode = checkPostCode(postcode);  
  if(newPostCode){
	   return true;
	 }else{
   return false; 	
	} 
}
function checkPostCode(toCheck) {
  var alpha1 = "[abcdefghijklmnoprstuwyz]";                       // Character 1
  var alpha2 = "[abcdefghklmnopqrstuvwxy]";                       // Character 2
  var alpha3 = "[abcdefghjkpmnrstuvwxy]";                         // Character 3
  var alpha4 = "[abehmnprvwxy]";                                  // Character 4
  var alpha5 = "[abdefghjlnpqrstuwxyz]";                          // Character 5
  var BFPOa5 = "[abdefghjlnpqrst]";                               // BFPO alpha5
  var BFPOa6 = "[abdefghjlnpqrstuwzyz]";                          // BFPO alpha6
  var pcexp = new Array ();
  pcexp.push (new RegExp ("^(bf1)(\\s*)([0-6]{1}" + BFPOa5 + "{1}" + BFPOa6 + "{1})$","i"));
  pcexp.push (new RegExp ("^(" + alpha1 + "{1}" + alpha2 + "?[0-9]{1,2})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));
  pcexp.push (new RegExp ("^(" + alpha1 + "{1}[0-9]{1}" + alpha3 + "{1})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));
  pcexp.push (new RegExp ("^(" + alpha1 + "{1}" + alpha2 + "{1}" + "?[0-9]{1}" + alpha4 +"{1})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));
  pcexp.push (/^(GIR)(\s*)(0AA)$/i);
  pcexp.push (/^(bfpo)(\s*)([0-9]{1,4})$/i);
  pcexp.push (/^(bfpo)(\s*)(c\/o\s*[0-9]{1,3})$/i);
  pcexp.push (/^([A-Z]{4})(\s*)(1ZZ)$/i);  
  pcexp.push (/^(ai-2640)$/i);
  var postCode = toCheck;
  var valid = false;
  for ( var i=0; i<pcexp.length; i++) {
    if (pcexp[i].test(postCode)) {
      pcexp[i].exec(postCode);  
      postCode = RegExp.$1.toUpperCase() + " " + RegExp.$3.toUpperCase();
      postCode = postCode.replace (/C\/O\s*/,"c/o "); 
      if (toCheck.toUpperCase() == 'AI-2640') {postCode = 'AI-2640'}; 
      valid = true;  
      break;
    }
  }
  if (valid) {return postCode;} else return false;
}

function hideLoader(){
  jq("#load_icon").fadeOut();
  jq("#loading_icon").hide();
}
//Added by Indumathi.S For mailing preference Jan-27-16
function checkAndEnabledPrivacy(privacyFlag,divId){   
 var flag = false;//Checkbox: on = false; off = true;
 if((privacyFlag == "N" && divId != "remainderMail") || (divId == "remainderMail" && privacyFlag == "Y")){ 
  flag = true;
 }
 $$(divId).checked = flag;
}
function checkFlag(divId){
 var flag = "Y";
 if(($$(divId).checked == true && divId != "remainderMail") || (divId == "remainderMail" && $$(divId).checked == false) ){    
  if(divId == "catActivity" && $$("catActivityHidden").value == ""){
   flag = $$("catActivityHidden").value; 
  }else{
    flag = "N";  
  }
 }
 return flag;
}
function mailingPreference(newsLetters, solusEmail, remainderMail, allMail, privacyId, surveyMail, catPrivacyId) { 
 $$('newsLettersId').value = checkFlag(newsLetters);
 $$('solusEmailId').value = checkFlag(solusEmail);
 $$('remainderMailId').value = checkFlag(remainderMail);
 $$('permitEmailId').value = checkFlag(allMail);
 $$('surveyMailId').value = checkFlag(surveyMail); //Checked survey mail on 31_May_2016, By Thiyagu G.
 $$('privacy').value = checkFlag(privacyId);
 $$('catPrivacy').value = checkFlag(catPrivacyId);
}
function disableFlags(){
 jq(".email").addClass("disabled");
}
function onOffEmail(CheckId){ 
 if(CheckId == "allMail" && $$(CheckId).checked == true){  
  jq("#newsLetters").prop('checked', true);
  jq("#solusEmail").prop('checked', true);
  jq("#remainderMail").prop('checked', true);
  jq("#surveyMail").prop('checked', true);  //enabled survey mail on 31_May_2016, By Thiyagu G.
  //jq(".email").addClass("disabled");  
  jq('#remainderMailText').show();
 }else if(CheckId == "allMail" && $$(CheckId).checked == false){	   
  jq("#newsLetters").prop('checked', false);
  jq("#solusEmail").prop('checked', false);
  jq("#remainderMail").prop('checked', false);
  jq("#surveyMail").prop('checked', false);  //diabled survey mail on 31_May_2016, By Thiyagu G.
 // jq(".email").removeClass("disabled");  
  jq('#remainderMailText').hide();
 }
}
function setCatoolActvityFlag(obj){
  if(obj.id == "catActivity" && $$("catActivityHidden").value == "" && $$(obj.id).checked == true){
    $$("catActivityHidden").value = "N";
  }
}
function openLightboxMsg(messageType){
 sm('newvenue', 650, 500, 'final5lbb');	
 url = contextPath + "/jsp/mywhatuni/Redesign/include/errorMessage.jsp?pageType="+messageType;  
 var ajaxObj = new sack();
 ajaxObj.requestFile = url;	      
 ajaxObj.onCompletion = function(){displayFormData(ajaxObj);};	
 ajaxObj.runAJAX();   
}
function displayFormData(ajax){  
 $$("newvenueform").innerHTML=ajax.response;   
 if($$("lightbxScript")){
  eval($$("lightbxScript").innerHTML);
 }    
 $$("newvenueform").style.display="block";
}
//
jq(document).ready(function(){
 jq("input[type='checkbox']").on('change', function(){
  unsavedMail = true;
 });
 jq("input[type='radio']").on('change', function(){
  unsavedMail = true;
 });
 
 //Added for save button changes in my profile page by Hemalatha.K on 17_DEC_2019_REL
 jq("input, select").on('input', function(){
	showSaveButton();
 });
 jq(document).on("click", "a", function(event){
  var exitPopupFlag = true;
  var a_href = jq(this).attr('href');
  if('checkApplicationPageStatus();' == jq(this).attr("onclick")){
    exitPopupFlag = false;
  }
  if($$("functionCallFlag").value == "redirect" && !unsavedMail){
   callSaveUserTimeLine();
  }else if(this.className != "edt_fnl5_btn ed_f5_btn msg_f5_clse" && this.className != "close" 
   && this.className != "noalert" && a_href != "" && a_href != "javascript:void(0);" && a_href != "javascript:saveMailSettings();"
   && this.className != "navbar ui-link navact" && this.className != "navbar ui-link" && a_href != "javascript:resetSettings();") {
    if(!jq(this).hasClass('noalert') && exitPopupFlag){
     settingValidation(a_href, event);
    }
  }  
  if(this.id == 'clearHiddenVal'){
    $$("mail_a_href").value = "";
    $$("functionCallFlag").value = "no";
  }
 }); 
 jq("input[type='submit']").click(function(event) {
  if(this.id != 'saveDetails' && $$("keyword_url") && $$("keyword_url").value !=""){
   settingValidation($$("keyword_url").value, event);
  }
 });
});

function showSaveButton() {
  jq("#off_postop").css("display","block");
}
function setUnsavedFlag(){
 unsavedMail = true;
}
function saveMailSettings(){
 if(unsavedMail) {
  $$("functionCallFlag").value = "no";
  profileSubmitValidations();
 } 
}
function resetSettings(){
 if($$("mbox").style.display == "block") {
  var a_href = $$("mail_a_href").value;
  if($$("functionCallFlag").value == "no") {
   callSaveUserTimeLine();
  } else {
   if(a_href!=""){
    $$("functionCallFlag").value = "no";
    location.href = a_href;
   }else{
    hm();
   } 
  }
 }
}
function settingValidation(a_href, event, toPage){
 if($$("mail_a_href")){
  if(a_href!=undefined && a_href!=""){
   $$("mail_a_href").value = a_href;
  }
 }  
 if(unsavedMail){
  openLightboxMsg("saveMailSettings");
  if($$("functionCallFlag").value == "redirect") {
   $$("functionCallFlag").value = "no";
   $$("mail_a_href").value = "newURL";
  } else {
    $$("functionCallFlag").value = "none";
    event.preventDefault();
  }
 }else if(toPage == 'application-page'){
   location.href = a_href;
 }
}
function callSaveUserTimeLine(flag){
 if($$("definetLineId") && $$("defineUserTimeLineURL") && $$("defineHrefType")) {
  $$("functionCallFlag").value = "yes";
  saveUserTimeLine($$("definetLineId").value, $$("defineUserTimeLineURL").value, $$("defineHrefType").value, flag);
 }
}
function clearLightBoxVal(){
  hm();
  $$("mail_a_href").value = "";
}
jq(document).ready(function(){
  var mobScrl = 0;  
  jq(window).scroll(function(){    
  var topOfElement = jq('#footer-bg').offset().top;
  var scrollTopPosition  = jq(window).scrollTop()+jq(window).height(); 
  jq('#off_postop').addClass('pro_set_sti');
  if(scrollTopPosition > topOfElement){
   jq('#off_postop').removeClass('pro_set_sti');
   jq("#needHelpDiv").removeClass("prf_cbpos");
   jq("#needHelpDiv").removeClass("intr_sb_pos");
   jq("#footer-bg").css('margin-top','0px');//Added by Indumathi.S Feb-16-2016
  }else{
   jq('#off_postop').addClass('pro_set_sti');
   jq("#needHelpDiv").addClass("prf_cbpos");
   jq("#footer-bg").css('margin-top','60px');
  }
  //Added by Hemalatha.K for 17_DEC_2019_REL
  var style = jq("#off_postop").css('display');
  if(style == 'none') {
	jq("#needHelpDiv").removeClass("prf_cbpos");
  }
 }); 
 //Added for displaying the error message viewport via scrolling in the SAR's pdf for FEB_12_19 rel by Sangeeth.S 
 setTimeout(function() {
   if(jq('#pdfErrMsg') && jq('#pdfErrMsg').length > 0){     
     mobScrl = (jq('#dlData') && jq('#dlData').length) ? jq('#dlData').offset().top : 0;
     if((width > 480) && (width <= 992)){     
       var dlObj = '#dlData';            
       jq(dlObj).next(".sett_cnr").toggleClass('show_off');
       jq(dlObj).next(".seton_cont").toggleClass('show_off');
       jq(dlObj).find(".fa").toggleClass('fa-plus-circle');       
     }else{
       mobScrl = (jq('#pdfErrMsg').length) ? jq('#pdfErrMsg').offset().top : 0;     
     }
     if(mobScrl != 0){                  
         jq('body,html').animate({         
              scrollTop: mobScrl
         }, 2000);     
     }
   } 
  }, 1000); 
 //
});
//End
function enabledTextMessage(){
  if($$("remainderMail").checked == true){    
    jq('#remainderMailText').show();
  }else{    
    jq('#remainderMailText').hide();
  }
}
function callLightBox(result, params){
  var resultArr = result.split("##SPLIT##");
  var resId = "revLightBox";
  var loc = "";
  var hotLine = jq('#hotLine').val();
  var collegeDisplayName = jq('#collegeDisplayName').val();
  var qualErrorCode = resultArr[2];
  if(resultArr[1] == "APPLICATION_ERROR_MESSAGE_LIGHTBOX"){
   loc = "/jsp/whatunigo/include/applicationErrorMessageLightbox.jsp";
  }
  var url = contextPath + loc;
  jq.ajax({
    url: url, 
    /*data: {
      "page-id": pageId, 
      "next-page-id": nxtPageId, 
      "status": status,
      "action": action,
      "htmlId": htmlId,
      "resultQus": resultQus,
      "resultAnsArr": resultAnsArr,
      "resultOptArr": resultOptArr
      "qualErrorCode" : resultArr[2]
    },*/
    type: "POST", 
    success: function(result){
     jq('#loadingImg').hide(); 
      jq("#"+resId).html(result);
      lightBoxCall();
      revLightbox();
    }
  });
}
function lightBoxCall(){
 jq("#revLightBox").addClass("rev_lbox");
 jq(".rev_lbox").css({'z-index':'111111','visibility':'visible'});
 jq(".rev_lbox").addClass("fadeIn").removeClass("fadeOut");
 jq("html").addClass("rvscrl_hid");
 //closeLiBox();
}
function revLightbox(){
  var width = document.documentElement.clientWidth;
  var revnmehgt = jq(".rvbx_cnt .revlst_lft").outerHeight();
  var lbxhgt = jq(".rev_lbox .rvbx_cnt").height();
  var txth2hgt = jq(".rvbx_cnt h2").outerHeight();
  var txth3hgt = jq(".rvbx_cnt h3").outerHeight();
  if (width <= 480) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    jq(".lbx_scrl").css("height", +scrhgt+ "px");
   }else if ((width >= 481) && (width <= 992)) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    jq(".lbx_scrl").css("height", +scrhgt+ "px");
   }else{
     var lbxmin = 17 + txth2hgt + txth3hgt;
     var scrhgt = lbxhgt - lbxmin; 
     jq(".lbx_scrl").css("height", +scrhgt+ "px"); 
   }
}
function closeLigBox(){
  jq(".rev_lbox").removeAttr("style");
  jq(".rev_lbox").addClass("fadeOut").removeClass("fadeIn");
  jq("html").removeClass("rvscrl_hid");
}

function mySettingsShowhideAddressFields(){
  if($$("countryOfResidence")){
    if($$("countryOfResidence").value !="" && $$("countryOfResidence").value !=null ){
      displayText("address_field_1");
      displayText("address_field_2");
      var countryOfResidenceSel = document.getElementById("countryOfResidence");
      var countryText = countryOfResidenceSel.options[countryOfResidenceSel.selectedIndex].text;
	  $$("countrySpan").innerHTML = countryText;
      if(countryText == 'United Kingdom' || countryText == 'England' || countryText == 'Northern Ireland' || countryText == 'Scotland' || countryText == 'Wales'){
	    $$('addFinder').style.display= 'block';
	  } 
    }
  }
}

jq(window).on('load', function() {
  if(jq("#countryOfResidence")){
	mySettingsShowhideAddressFields();
  }
});
