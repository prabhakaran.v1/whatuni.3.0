var hh=0;
var contextPath = "/degrees";
var emailexistmsg="It looks like you're already a Whatuni member, just enter your password to complete this action."
var deviceWidth =  null;
function getDeviceWidth() { return (window.innerWidth > 0) ? window.innerWidth : screen.width; }
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
var cnt = 1;
var co = jQuery.noConflict();
function addBasket(assocId, assocType, thisvalue, divId, popDivId, incompare, vwlb, collegeName){
//Added collegeName param in all places when it call add comparision by Prabha on 29_Mar_2016
  var dowhat="";
  var loadId = "";
  var popId = '';
  var fromVisitWeb = "";
  var defaultpop = '';
  if(vwlb=="fromlighbox"){
     var element = document.getElementById(thisvalue);
     if(element.className == "cmlst"){
        dowhat = "add";
        loadId = "load_"+assocId;
      }else if(element.className == "cmlst act"){
        dowhat = "remove";
        loadId = "load1_"+assocId;
      }
  }else{
    if(thisvalue.className == "" || thisvalue.className == "savTag" || thisvalue.className == "cmp_hov"){
      dowhat = "add";
      loadId = "load_"+assocId;
    }else if(thisvalue.className == "act" || thisvalue.className == "cmp_hov act"){
      dowhat = "remove";
      loadId = "load1_"+assocId;
    }else if (thisvalue.className == "fclose"){
      dowhat = "remove"
    }else if(thisvalue=="visitweb"){//Added for 11-AUG-2015_REL
      dowhat = "add";
      fromVisitWeb = "vweb";
    }
    if(thisvalue.className == "btn4 add"){
      dowhat = "add";
      loadId = "load_"+assocId;
    }else if(thisvalue.className == "btn4 add act"){
      dowhat = "remove";
      loadId = "load1_"+assocId;
    }
  }
  
  var assocId = assocId;
  popId = "pop_"+assocId;
  defaultpop = "defaultpop_"+assocId;
  //Added wu_scheme by Indumathi.S Mar-29-2016
  var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";
  var loadImg = '<img src="'+wu_scheme+document.domain+'/wu-cont/images/ldr.gif"/>';
  if($$(loadId)){
    $$(loadId).innerHTML = loadImg;
    $$(loadId).style.display = 'block';
  }
  var fromFlag = "";
  if(incompare=='Y'){
    fromFlag = "comp";
  }
  var comparepage = "NORMAL";
  if($$("comparepage")){
    comparepage = $$("comparepage").value;
  }
  var req;
  deviceWidth =  getDeviceWidth();
  var url=contextPath+'/addbasket.html?assocId='+assocId+'&fromAjax=true&dowhat='+dowhat+"&assocType="+assocType+"&comppage="+comparepage+"&fromFlag="+fromFlag+'&screenwidth='+deviceWidth;
  if(typeof XMLHttpRequest !="undefined"){req=new XMLHttpRequest();}
  else if(window.ActiveXObject){req=new ActiveXObject("Microsoft.XMLHTTP");}
  req.open("POST",url,true);req.onreadystatechange=handlerequest;req.send(url)
  function handlerequest(){
    if(req.readyState==4){
      if(req.status==200){
        var resp=req.responseText;
        if((resp).indexOf("SESSION_EXPIRED") > -1) {
          var loginUrl = resp.split("##")[1];
          window.open(loginUrl, "_self");
          return;
        }
        var dataarray=resp.split("BREAKBREAK");
        var dataArrayLength = dataarray.length;
        if(dataArrayLength > 2){
            $$("s_count").innerHTML = dataarray[3];
            if(dataarray[3] != '0'){
            	co("#topProfIconId").addClass('short_lst');	
            }else{
            	co("#topProfIconId").removeClass('short_lst');	
            }            
            $$("s_count_drop_down").innerHTML = dataarray[3];
            if($$D("s_count_nav")){$$D("s_count_nav").innerHTML = dataarray[3];}
            if($$(loadId)){
              $$(loadId).style.display = 'none';
            }
            if(dowhat == "add"){
              if($$(divId)){$$(divId).style.display = 'none';}
              if($$(popDivId)){$$(popDivId).style.display = 'block';}
              if($$(popId)){
                if(trimString(dataarray[0])!=''){
                  $$(popId).innerHTML=dataarray[0];
                  if($$(defaultpop)){$$(defaultpop).style.display = 'none';}
                  $$(popId).style.display = 'block';                  
                }
              }
              //
             if(vwlb=="fromlighbox"){
              if($$("lb_"+divId)){
                  if($$("lb_"+divId)){blockNone(("lb_"+divId),'block');}
                  if($$("lb_"+popDivId)){blockNone(("lb_"+popDivId),'none');}
                   if($$("compStatusTextLb")){
                    $$("compStatusTextLb").innerHTML = "ADDED";
                    $$("compStatusimageLb").className = "fa fa-check"; 
                    $$("compStatustxtColourLb").className = "grntxt"; 
                   }
                }
             }
              if($$("currentcompare")){
                if($$("currentcompare").value == "" || $$("currentcompare").value == null){
                  $$("currentcompare").value = popId;
                }else{
                  //var prpopDivId = $$("currentcompare").value;
                  //$$(prpopDivId).style.display = 'none';
                  $$("currentcompare").value = popId;
                }
              }              
              //Added for basket insights log by Prabha on 29_Mar_2016_Rel
             if(dowhat == "add" && collegeName != ""){
               comparisionAnalyticsEventsLogging(collegeName);
             }
  //End of basket insights code
            }else if(dowhat == "remove"){
              if($$(divId)){$$(divId).style.display = 'block';}
              if($$(popDivId)){$$(popDivId).style.display = 'none';}
              if($$(popId)){
                $$(popId).innerHTML="";
              }
              if(vwlb=="fromlighbox"){
                if($$("lb_"+popDivId)){
                    if($$("lb_"+divId)){blockNone(("lb_"+divId),'none');}
                    if($$("lb_"+popDivId)){blockNone(("lb_"+popDivId),'block');}
                    $$("compStatusTextLb").innerHTML = "REMOVED";
                    $$("compStatusimageLb").className = "fa fa-times"; 
                    $$("compStatustxtColourLb").className = "grntxt grntxt_rem"; 
                }
              } 
              if($$("currentcompare")){
                $$("currentcompare").value = "";
              }                            
              if($$("srcompcnt1")){
                $$("srcompcnt1").innerHTML = "["+dataarray[3]+"] ";
              }              
            }
        }else{
          if(thisvalue!="visitweb" && vwlb!="fromlighbox"){//Added for 11-AUG-2015_REL
              if($$(loadId)){
              $$(loadId).style.display = 'none';
            }
            showLightBoxLoginForm('grade-range-popup',650,500, 'COMAPRE-BASKET');
            return false;           
          }
         }             
        var currbasketId = "";
        var selectedbasketid = "";
     /*   if($$('selectedBasketId')){
         selectedbasketid = $$('selectedBasketId').value;
        }
        if($$('sessionBasketId')){
          currbasketId = $$('sessionBasketId').value;
        }*/
      
       // if(currbasketId==selectedbasketid){
        if(incompare=='Y'){
          loadtopbasket();
          resizeCells();
          var podName = (assocType == 'C') ? 'UNI_POD' : 'COURSE_POD';
          onScrollSuggestedPod(podName);
         }
        //}
        if(thisvalue=="visitweb"){//Added for 11-AUG-2015_REL        
          if(dataarray[2]!="ALREADY_ADDED"){   
          var showFlag = "";
           if($$("dontShowCompSplashHidden")){
           showFlag = $$("dontShowCompSplashHidden").value;
           }
           if(showFlag==""){
              if(jq("#pageName").val() == "searchResults"){
				 clearTimeout(0);
			   } else{
				 clearTimeout(timersLB);
			 }
            setTimeout(function() {showLightboxVW(assocId,assocType) },4000);}
            return true;
         }
        }        
        setCompareUncompareAll('addBasket', dataarray[0]);
      }
     }
    }
  }
//Added code for the bug:28713 on 03_Oct_2017, By Thiyagu G
co(document).ready(function(){
  setCompareUncompareAll('pageLoad','');
});
function setCompareUncompareAll(fromFlag, dataarray){
  if($$("compareallcourses")){
    var coursesIds = ($$("compareallcourses").value).split(",");    
    var compFlag = true;
    var opType;
    for(var i=0;i<coursesIds.length;i++){
      var popDiv = "basket_pop_div_"+coursesIds[i];      
      if($$(popDiv) && $$(popDiv).style.display == 'none'){
        compFlag = false;
      }
    }    
    if(compFlag){      
      $$("uncompareall").style.display = 'block';
      $$("compareall").style.display = 'none';
      if(fromFlag == "pageLoad"){
        $$("loadPopCourseIds").style.display = 'block';
        $$("popCourseIds").style.display = 'none';
      }else{
        $$("popCourseIds").innerHTML = dataarray;
      }        
      opType = "A";      
    }else{      
      $$("uncompareall").style.display = 'none';
      $$("compareall").style.display = 'block';
      if(fromFlag == "pageLoad"){
        $$("loadPopCourseIds").style.display = 'none';
        $$("popCourseIds").style.display = 'block';
      }else{
        $$("popCourseIds").innerHTML="";
      }
      opType = "R";        
    }    
    $$("comparestatus").value = opType;
    if(opType == 'A'){
      $$("compareall").style.display ="none";
      $$("uncompareall").style.display ="block";      
    }else{
      $$("compareall").style.display ="block";
      $$("uncompareall").style.display ="none";      
    }    
  }
}
//Method for add comparision insights log :: Prabha on 29_Mar_2016
function comparisionAnalyticsEventsLogging(inVal){
  var collegNames = inVal.split("##SPLIT##"); //##SPLIT## - GlobalConstants.SPLIT_CONSTANT
  for(var lp = 0; lp < collegNames.length; lp++){
    ga('create', 'UA-52581773-4', 'auto', {'name': 'newTracker'});
    ga('newTracker.send', 'event', 'view comparison' , 'clicked' , collegNames[lp]);
  }
}
 
function deleteComparison(assocId, assocType, basketId, basketNo, tabSelected,collegeId){
  var ajax=new sack(); 
  deviceWidth =  getDeviceWidth();
  var url=contextPath+"/addbasket.html?fromAjax=true&dowhat=remove"+"&assocId="+assocId+"&assocType="+assocType+"&basketId="+basketId+"&basketNo="+basketNo+"&tabSelected="+tabSelected+'&screenwidth='+deviceWidth;
  ajax.requestFile = url;
  ajax.onCompletion = function(){deleteComparisonResponse(ajax, basketId, basketNo,collegeId) };	
  ajax.runAJAX();
}

function deleteClearingComparison(assocId, assocType, basketId, basketNo, tabSelected,collegeId,clearingtype){
  sm('newvenue', 490, 500, 'other');
   document.getElementById("ajax-div").style.display="block";
  var ajax=new sack();  
  deviceWidth =  getDeviceWidth();
  var url=contextPath+"/addbasket.html?fromAjax=true&dowhat=remove"+"&assocId="+assocId+"&assocType="+assocType+"&basketId="+basketId+"&basketNo="+basketNo+"&tabSelected="+tabSelected+"&fromClearing="+clearingtype+"&fromFlag=comp"+'&screenwidth='+deviceWidth;
  ajax.requestFile = url;
  ajax.onCompletion = function(){deleteComparisonResponse(ajax, basketId, basketNo,collegeId) };	
  ajax.runAJAX();
}

function deleteComparisonResponse(ajax, basketId, basketNo,collegeId){
  var response = ajax.response;
   document.getElementById("compareRightPod").innerHTML  = ajax.response;
    loadtopbasket();
    var divId = "basket_div_"+collegeId;
    var popDivId = "basket_pop_div_"+collegeId;
    var popId = "pop_"+collegeId;
    if($$(divId)){$$(divId).style.display = 'block';}
    if($$(popDivId)){$$(popDivId).style.display = 'none';}
    if($$(popId)){
      $$(popId).innerHTML="";
    }
    if($$("currentcompare")){
      $$("currentcompare").value = "";
    } 
 var currbasketId = "";
 var selectedbasketid = "";
/*  if($$('selectedBasketId')){
   selectedbasketid = $$('selectedBasketId').value;
  }
  if($$('sessionBasketId')){
    currbasketId = $$('sessionBasketId').value;
  }*/
 // if(currbasketId==selectedbasketid){
    if(document.getElementById("comparisonListSize")){
      var upperCount = document.getElementById("comparisonListSize").value;
      if(upperCount == null || upperCount == 'null'){upperCount = 0;}
      $$("s_count").innerHTML = upperCount;
      if(dataarray[3] != '0'){
      	co("#topProfIconId").addClass('short_lst');	
      }else{
      	co("#topProfIconId").removeClass('short_lst');	
      }
      $$("s_count_drop_down").innerHTML = upperCount;
      if($$D("s_count_nav")){$$D("s_count_nav").innerHTML = upperCount;}
    }
 // } 
    if($$("emptybasketno_"+basketNo)){
      $$("pnts_"+basketNo).style.display = 'none';
    }
    // hm();
}

function showPop(id){
  document.getElementById(id).style.display='none';
  return false;
}

function compareAll(collegeCourseIds, compareallType, opType,incompare, collegeNames){
//Added collegeNames param in all places when it call add compare all by Prabha on 29_Mar_2016
  $$("load_icon").style.display="block";
  var dowhat = 'add';
  if(opType == 'R'){
  var dowhat = 'remove';
 }
  var comparepage = "NORMAL";
  if($$("comparepage")){
    comparepage = $$("comparepage").value;
  }
  
  var ajax=new sack();
  deviceWidth =  getDeviceWidth();
  var url=contextPath+"/addbasket.html?compareallIds="+escape(collegeCourseIds)+"&compareallType="+compareallType+"&comppage="+comparepage+"&dowhat="+dowhat+'&screenwidth='+deviceWidth;
  ajax.requestFile = url;
  ajax.onCompletion = function(){ handlecompareallrequest(ajax, collegeCourseIds, opType,incompare, collegeNames); };	
  ajax.runAJAX();
}

function handlecompareallrequest(ajax, collegeCourseIds, opType,incompare, collegeNames){
  var resp=ajax.response;
  //alert("---resp--- "+resp);
  var dataarray=resp.split("BREAKBREAK");
  var dataArrayLength = dataarray.length;
  $$("load_icon").style.display="none";
  if(dataArrayLength > 2){
    $$("s_count").innerHTML = dataarray[3];
    if(dataarray[3] != '0'){
    	co("#topProfIconId").addClass('short_lst');	
    }else{
    	co("#topProfIconId").removeClass('short_lst');	
    }
    $$("s_count_drop_down").innerHTML = dataarray[3];
    if($$D("s_count_nav")){$$D("s_count_nav").innerHTML = dataarray[3];}
    $$('popCourseIds').innerHTML = dataarray[0];  //, 03_Feb_2015 By Thiyagu G
    if($$("hide")){$$("hide").style.display = 'block';}
    var idDataarray=collegeCourseIds.split(",");
    for(var i = 0; i < idDataarray.length; i++){
      if(opType == 'A'){
        if($$('basket_div_'+idDataarray[i])!=null ){
          $$('basket_div_'+idDataarray[i]).style.display="none";
        }
        if($$('basket_pop_div_'+idDataarray[i])!=null ){
          $$('basket_pop_div_'+idDataarray[i]).style.display="block";          
          $$('pop_'+idDataarray[i]).style.display="none";          
        }
      }else{
        if($$('basket_pop_div_'+idDataarray[i])!=null ){
          $$('basket_pop_div_'+idDataarray[i]).style.display="none";          
          $$('pop_'+idDataarray[i]).style.display="none";
        }
        if($$('basket_div_'+idDataarray[i])!=null ){
          $$('basket_div_'+idDataarray[i]).style.display="block";
          $$('popCourseIds').style.display="block"; //, 03_Feb_2015 By Thiyagu G
        }
      }
    }   
    //removed compare text related script for add to compare redesign, 03_Feb_2015 By Thiyagu G
    $$("comparestatus").value = opType;
    if(opType == 'A'){
      $$("compareall").style.display ="none";
      $$("uncompareall").style.display ="block";      
    }else{
      $$("compareall").style.display ="block";
      $$("uncompareall").style.display ="none";      
    }
    //
    if(opType == "A" && collegeNames != ""){
      comparisionAnalyticsEventsLogging(collegeNames); //Insights log for basket by Prabha on 29_Mar_2016
    }
    //
  }else{
    showLightBoxLoginForm('grade-range-popup',650,500, 'COMAPRE-BASKET');
    return false;  
  }
  var currbasketId = "";
  var selectedbasketid = "";
/*  if($$("selectedBasketId")){
   selectedbasketid = $$("selectedBasketId").value;
  }
  if($$("sessionBasketId")){
    currbasketId = $$("sessionBasketId").value;
  }*/
 // if(currbasketId==selectedbasketid){  
    if(incompare=='Y'){
     loadtopbasket();
     var listSise = 0;
       if(document.getElementById("comparisonListSize")){
         listSise = document.getElementById("comparisonListSize").value;
         if(listSise>0){
           setDesktopView(listSise);
         }
       }
     resizeCells();
    } 
 //} 
}
//added this script to set compare all status for add to compare redesign, 03_Feb_2015 By Thiyagu G
function setCompareAll(){
  if($$("currentcompare")){
    var opType = $$("currentcompare").value;    
    if(opType == 'A'){
      $$("compareall").style.display ="none";
      $$("uncompareall").style.display ="block";  
    }else{
      $$("compareall").style.display ="block";
      $$("uncompareall").style.display ="none";  
    }
  }
}
function closeLogin(){
hm('newvenue')}
function resetregpod(){
noneblock("lab_pass",'none')
noneblock("lab_forgot",'none')
noneblock("lab_indended_yr",'block')
noneblock("lab_terms",'block')
noneblock("lab_updates",'block');
noneblock("regpodreset",'none')
noneblock("loadingpodgif",'none')
$$("errormessagepodregn").innerHTML=""
return false}

function checkUpdates() {
noneblock("lab_updates",'block')
$$("podUpdates").checked = false;
}

function noneblock(id,value){if($$(id)!=null){$$(id).style.display=value}}

//myShortList.js
//================================
var emptymysearch="No previous searches on Whatuni are recorded right now. \n\n As you explore Whatuni, your  searches will be saved here. You can then click back to them quickly and easily."
var message1="After searching, tick the boxes next to";var message2="to compare in detail, save and order multiple prospectuses"
var message=message1+" uni names "+message2
var cmessage=message1+" course names "+message2
var prosmess="You can only compare up to 4 choices in one go"


function visitWebSiteForceSave(collegeId,vwcid, vwsid, vwnid, vwurl){  
  assocId = collegeId;
  var deviceWidth =  getDeviceWidth();
  var url=contextPath+'/addbasket.html?assocId='+assocId+'&fromAjax=true&dowhat=add&assocType=O'+'&screenwidth='+deviceWidth;
   if(typeof XMLHttpRequest !="undefined"){req=new XMLHttpRequest();}
    else if(window.ActiveXObject){req=new ActiveXObject("Microsoft.XMLHTTP");}
    req.open("POST",url,true);req.onreadystatechange=handlerequest;req.send(url)
    function handlerequest(){
      if(req.readyState==4){
        if(req.status==200){
           var resp=req.responseText;
           var dataarray=resp.split("BREAKBREAK")
           var dataArrayLength = dataarray.length;
           if(dataArrayLength > 2){
              $$D("s_count").innerHTML = dataarray[3];
              if(dataarray[3] != '0'){
              	co("#topProfIconId").addClass('short_lst');	
              }else{
              	co("#topProfIconId").removeClass('short_lst');	
              }
              $$D("s_count_drop_down").innerHTML = dataarray[3];
              if($$D("s_count_nav")){$$D("s_count_nav").innerHTML = dataarray[3];}
           }
            cpeWebClick('',vwcid, vwsid, vwnid, vwurl);
        }
      }
    }
 }
//This method is to show comparison content Lightbox  on click of visit website
function showLightboxVW(assocId,assocType){
 var comparepage = "NORMAL";
  if($$("comparepage")){
    comparepage = $$("comparepage").value;
  }
  var url = contextPath +"/showbasketcontentajax.html?assocId="+assocId+"&assocType="+assocType+"&compareType="+comparepage;
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){ showLightboxVWresp(ajax); };
  ajax.runAJAX();
} 

function showLightboxVWresp(ajax){
  var resp=trimString(ajax.response);
  if(resp!="" && resp!=null){
     sm('newvenue', 650, 500, 'vwCompbox');	
     document.getElementById("newvenueform").innerHTML=ajax.response; 
     document.getElementById("ajax-div").style.display="none";
     document.getElementById("newvenueform").style.display="block";
  }
}
//This method is to set the Dont show again comparison lightbox
 function dontShowCompLB(){
  //setSessionCookie('DS_WC_TO_COM','TRUE');//DONT_SHOW_WEB_CLICK_TO_COMPARISON
  createSecureCookie('DS_WC_TO_COM','Y');
  var url = contextPath +"/setcomplbsessionattrajax.html";
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){dontShowCompLBresp(ajax); };
  ajax.runAJAX();
 }
//Added for secure cookie by Hemalatha.K on 17-DEC-2019_REL
 function createSecureCookie(cookieName, cookieValue) {
 	var url = contextPath+"/create-cookies.html?cookieName="+cookieName+"&cookieValue="+cookieValue;    
 	  var ajaxObj = new sack();
 	  ajaxObj.requestFile = url;	  
 	  ajaxObj.onCompletion = function(){};	
 	  ajaxObj.runAJAX();
 }
function dontShowCompLBresp(ajax){
  if($$("dontShowCompSplashHidden")){
    $$("dontShowCompSplashHidden").value="TRUE";
  }
  hm();
}