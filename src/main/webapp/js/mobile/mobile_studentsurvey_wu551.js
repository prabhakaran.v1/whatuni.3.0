var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";//Added wu_scheme by Indumathi.S Mar-29-2016
//var ulSubmitURL = document.getElementById("seourlpt").value;
function $$DD(id){
  return document.getElementById(id);
}
function setSelectedVal(obj, spanid){
  var dobid = obj.id
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  document.getElementById(spanid).innerHTML = text;
}


function disableNonUKSchool() {
  if (document.getElementById("nonUKFlag").checked == true){
    document.getElementById("schoolName").disabled = true;
    document.getElementById("schoolName").value = "Enter school name (optional)";
    document.getElementById("schoolName_hidden").value = "";
    document.getElementById("nonUKDiv").style.display = 'block';
  }else{
    document.getElementById("schoolName").disabled = false;
    document.getElementById("nonUKSchool").value = "Enter school name";
    document.getElementById("nonUKDiv").style.display = 'none';
  }
}

function autoCompleteUKSchool(event,object){
	ajax_showOptions(object,'ukschoollist',event,"indicator",'');
}  

function qualSubjectList(event, object){  
  var qual = document.getElementById('qualsel').value; 
  var id = object.id;
  var hiddenId = id+"_hidden";
  if(qual == 'Qualification'){
      $$DD("errordiv1").innerHTML = "Please select subject qualification first and then enter the subject"
      $$DD("errordiv1").style.display = 'block';
      return false;
  }
  if(($$DD(hiddenId).value != "" || $$DD(hiddenId).value != null) && event.keyCode != 13){
   $$DD(hiddenId).value = "";
  }
  ajax_showOptions(object,'qualsubects',event,"indicator",'ulqual='+ qual);
}

function qualUniOfInterstSubjectList(event, object){
  var qual = document.getElementById('qualsel').value; 
  var id = object.id;
  var hiddenId = id+"_hidden";
  if(($$D(hiddenId).value != "" || $$D(hiddenId).value != null) && event.keyCode != 13){
   $$D(hiddenId).value = "";
  }
  ajax_showOptions(object,'qualuniofinterstsubects',event,"indicator",'ulqual='+ qual);
}

function chooseULIndustry(event, object){
  var id = object.id;
  var hiddenId = id+"_hidden";
  if(($$D(hiddenId).value != "" || $$D(hiddenId).value != null) && event.keyCode != 13){
   $$D(hiddenId).value = "";
  }
	ajax_showOptions(object,'ulindustry',event,"indicator", '');
}

function chooseULJob(event, object){
  var id = object.id;
  var hiddenId = id+"_hidden";
  if(($$D(hiddenId).value != "" || $$D(hiddenId).value != null) && event.keyCode != 13){
   $$D(hiddenId).value = "";
  }
	ajax_showOptions(object,'uljob',event,"indicator", '');
}

function clearDefault(obj, text){
  var id = obj.id;
  if(document.getElementById(id).value == text){
    document.getElementById(id).value = "";
  }
  $$DD(id).style.color = '#000000';
}

function setDefault(obj, text){
  var id = obj.id;
  if(document.getElementById(id).value == ""){
    document.getElementById(id).value = text;
    if(text == 'Enter industry, eg Engineering'){
      $$DD("industryName_hidden").value = "";
    }else if(text == 'Enter job name, eg Doctor'){
      $$DD("jobName_hidden").value = "";
    }
  }
  $$DD(id).style.color = '#888888';
}

function clearAlterText(objId, text, text1){    
    if(document.getElementById(objId).value != "" && document.getElementById(objId).value != text){
        document.getElementById(objId).value = "";
        document.getElementById(objId).value = text;
        $$DD(objId).style.color = '#888888';
    }    
}

function autocompleteOff(element) {
	self.focus(); //Bring window to foreground if necessary.
	var form = document.getElementById(element); // grab the form element.
	if(form)
	{
		form.setAttribute("autocomplete","off"); // Turn off Autocomplete for the form, if the browser supports it.
  }
}

function submitWhatcanido(userLogin){
  var validationFlag = subjectValidation();
  if(validationFlag == 'ATLEASTONE'){
    $$DD("errordiv").innerHTML = "Please select subjects from the list."
    $$DD("errordiv").style.display = 'block';
    //alert('Please enter atleast one Subject.....');
    return false;
  }else if(validationFlag == 'SUBMIT'){
    //alert(userLogin);
    if(userLogin == 'N'){
      showLightBoxLoginForm('popup-newlogin',650,500, 'whatcanido');
    }else{
      document.getElementById('ulsearchform').action = ulSubmitURL;
      document.getElementById('ulsearchform').submit();
      return false;
    }
  }else if(validationFlag == 'NOTVALIDTARIFF'){
    $$DD("errordiv").innerHTML = "Cannot search with more than 999 points."
    $$DD("errordiv").style.display = 'block';
    //alert('Cannot search with more than 999 points.');
    return false;
  }else if(validationFlag == 'ATLEASTONETP'){
    $$DD("errordiv").innerHTML = "Please enter tariff points."
    $$DD("errordiv").style.display = 'block';
    //alert('Please enter tariff points....');
    return false;
  }
}
function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}
function submitStudentSurveyYes(userAgent){
    var collegeName = myTrim($$DD("collegeName").value);
    var courseTitle = myTrim($$DD("courseTitle").value);
    var courseTitleHidn = myTrim($$DD("courseTitle_hidden").value);
    var collegeNameDisplay = myTrim($$DD("collegeName_display").value);
    var courseTitleDisplay = myTrim($$DD("courseTitle_display").value);
    if(collegeName == 'eg. Cardiff University' || collegeName == ""){
        $$DD("errordiv1").innerHTML = "Please enter college name."
        $$DD("errordiv1").style.display = 'block';
        return false;
    }else{
        $$DD("errordiv1").innerHTML = ""
        $$DD("errordiv1").style.display = 'none';        
    }
    if(courseTitle == 'eg. Maths' || courseTitle == ""){
        $$DD("errordiv2").innerHTML = "Please enter course."
        $$DD("errordiv2").style.display = 'block';
        return false;
    }else{
        $$DD("errordiv2").innerHTML = "Please enter course."
        $$DD("errordiv2").style.display = 'none';        
    }
    if(courseTitleHidn == ""){
        $$DD("errordiv2").innerHTML = "Please select course from ajax list."
        $$DD("errordiv2").style.display = 'block';
        return false;
    }else{
        $$DD("errordiv2").innerHTML = "Please enter course."
        $$DD("errordiv2").style.display = 'none';        
    }
    if(collegeName != collegeNameDisplay){
        $$DD("errordiv1").innerHTML = "Please select university from ajax list."
        $$DD("errordiv1").style.display = 'block';
        return false;
    }else{
        $$DD("errordiv1").innerHTML = ""
        $$DD("errordiv1").style.display = 'none';        
    }
    if(courseTitle != courseTitleDisplay){
        $$DD("errordiv2").innerHTML = "Please select course from ajax list."
        $$DD("errordiv2").style.display = 'block';
        return false;
    }else{
        $$DD("errordiv2").innerHTML = ""
        $$DD("errordiv2").style.display = 'none';        
    }
    if(userAgent=='mobile'){
        document.getElementById('studentsurveynewform').action = "/degrees/student-survey-yes-submit.html";
        document.getElementById('studentsurveynewform').submit();
    }else{
        document.getElementById('studentsurveynewform').action = "/degrees/student-survey-submit.html";
        document.getElementById('studentsurveynewform').submit();
    }
    return true;    
}

function clearCourseValue(){
    $$DD("courseTitle").value = "";
    $$DD("courseTitle_hidden").value = "";
}

function setDefaultCourseValue(obj, courseId){
    if(obj.value==''){ obj.value='eg. Cardiff University';}
    if($$DD(courseId).value==''){ $$DD(courseId).value='eg. Maths';}
}

function submitStudentSurveyNo(){
  var validationFlag = subjectValidation();   
  if(validationFlag == 'ATLEASTONE'){
    $$DD("errordiv1").innerHTML = "Please select subjects from the list."
    $$DD("errordiv1").style.display = 'block';    
    return false;
  }else if(validationFlag == 'NOTVALIDTARIFF'){
    $$DD("errordiv1").innerHTML = "Cannot search with more than 999 points."
    $$DD("errordiv1").style.display = 'block';    
    return false;
  }else if(validationFlag == 'ATLEASTONETP'){
    $$DD("errordiv1").innerHTML = "Please enter tariff points."
    $$DD("errordiv1").style.display = 'block';    
    return false;
  }else if(validationFlag == 'SUBMIT'){
    var qualSubjName7 = $$DD("qualSubjName7").value;
      var qualSubjName8 = $$DD("qualSubjName8").value;
      var qualSubjName9 = $$DD("qualSubjName9").value;  
      if((qualSubjName7 == 'Please enter subject' || qualSubjName7 == "") && (qualSubjName8 == 'Please enter subject' || qualSubjName8 == "") && (qualSubjName9 == 'Please enter subject' || qualSubjName9 == "")){
          $$DD("errordiv").innerHTML = "Please start typing and choose at least one subject you are interested in at uni from the list."
          $$DD("errordiv").style.display = 'block';
          return false;
      }else{
        document.getElementById('studentsurveynewform').action = "/degrees/student-survey-no-submit.html";
        document.getElementById('studentsurveynewform').submit();
        return true;   
      }
  
    
  }
}

function submitIWantotobe(userLogin){
  var jobCode = trimString($$DD("jobName_hidden").value);
  var industryCode = trimString($$DD("industryName_hidden").value);
  var jobName = trimString($$DD("jobName").value);
  var industryName = trimString($$DD("industryName").value);
  if((jobCode == "" || jobCode == null) && jobName != 'Enter job name, eg Doctor'){
    $$DD("errordiv").innerHTML = "There is no data available for "+jobName+", this is either because there is no data available for that job, or the job has been classified at a higher level. Eg. Mental Health Nurse will be classified under Nurse."
    $$DD("errordiv").style.display = 'block';
    return false;
  }
  if((industryCode == "" || industryCode == null) && industryName != 'Enter industry, eg Engineering'){
    $$DD("errordiv").innerHTML = "There is no data available for "+industryName+", this is either because there is no data available for that industry, or the industry has been classified at a higher level."
    $$DD("errordiv").style.display = 'block';
    return false;
  }
  if((jobCode != "" && industryCode != "") || (jobCode == "" && industryCode == "")){
    $$DD("errordiv").innerHTML = "Please select either Job or Industry from the list."
    $$DD("errordiv").style.display = 'block';
    //alert('Please enter either Job or Industry.');
    return false;
  }
  if(userLogin == 'N'){
    showLightBoxLoginForm('popup-newlogin',650,500, 'iwanttobe');
  }else{
    document.getElementById('ulsearchform').action = ulSubmitURL;
    document.getElementById('ulsearchform').submit();
    return false;
  }
}

function submitIWanttobenonloggedin(){
  //alert('inside the search ');
  var jobCode = trimString($$DD("jobName_hidden").value);
  var industryCode = trimString($$DD("industryName_hidden").value);
  //alert('---job---  '+$$DD("jobName_hidden").value);
  //alert('---industry---  '+$$DD("industryName_hidden").value);
  if(((jobCode != null || jobCode != "") && (industryCode != null || industryCode != "")) || ((jobCode == null || jobCode == "") && (industryCode == null || industryCode == ""))){
    alert('Please enter either Job or Industry.');
    return false;
  }
}

function onchageQual(obj){
  //var qual = obj.value;
  //alert('---qual---- '+qual);
  var qual = $$DD(obj).value;  
  var one = $$DD("selgradeone").value;
  var two = $$DD("selgradetwo").value;
  var three = $$DD("selgradethree").value;
  var four = $$DD("selgradefour").value;
  var five = $$DD("selgradefive").value;
  var six = $$DD("selgradesix").value;
  if(qual == 'AL'){
    $$DD("fileset1").style.display = "inline-block";
    $$DD("fileset2").style.display = "inline-block";
    $$DD("fileset3").style.display = "inline-block";
    $$DD("fileset4").style.display = "inline-block";
    $$DD("fileset5").style.display = "none";
    $$DD("fileset6").style.display = "none";
    displayBlockUCASInput('gradediv', 'block', 6);
    displayBlockUCASInput('ucas', 'none', 6);
    createSelectBoxUL(qual, 'selgradeone', 'grade1span');   
    createSelectBoxUL(qual, 'selgradetwo', 'grade2span');   
    createSelectBoxUL(qual, 'selgradethree', 'grade3span');   
    createSelectBoxUL(qual, 'selgradefour', 'grade4span');   
    $$DD("qualSubjName5").value = "Subject"; $$DD("qualSubjName6").value = "Subject";
    $$DD("qualSubjName5_hidden").value = ""; $$DD("qualSubjName5_hidden").value = "";
    $$DD("grade1span").innerHTML = one;
    $$DD("grade2span").innerHTML = two;
    $$DD("grade3span").innerHTML = three;
    $$DD("grade4span").innerHTML = four;    
  }else if(qual == 'H' || qual == 'AH'){
    //alert('H');
    displayBlockUCASInput('fileset', 'inline-block', 6);
    displayBlockUCASInput('gradediv', 'block', 6);
    displayBlockUCASInput('ucas', 'none', 6);
    createSelectBoxUL(qual, 'selgradeone', 'grade1span');   
    createSelectBoxUL(qual, 'selgradetwo', 'grade2span');   
    createSelectBoxUL(qual, 'selgradethree', 'grade3span');   
    createSelectBoxUL(qual, 'selgradefour', 'grade4span');   
    createSelectBoxUL(qual, 'selgradefive', 'grade5span');   
    createSelectBoxUL(qual, 'selgradesix', 'grade6span');    
    if(one=="A*"){$$DD("grade1span").innerHTML = "A";}else{$$DD("grade1span").innerHTML = one;}    
    if(one=="A*"){$$DD("grade2span").innerHTML = "A";}else{$$DD("grade2span").innerHTML = two;}    
    if(one=="A*"){$$DD("grade3span").innerHTML = "A";}else{$$DD("grade3span").innerHTML = three;}    
    if(one=="A*"){$$DD("grade4span").innerHTML = "A";}else{$$DD("grade4span").innerHTML = four;}    
    if(one=="A*"){$$DD("grade5span").innerHTML = "A";}else{$$DD("grade5span").innerHTML = five;}    
    if(one=="A*"){$$DD("grade6span").innerHTML = "A";}else{$$DD("grade6span").innerHTML = six;}        
  }else if(qual == 'TP'){
    $$DD("fileset1").style.display = "inline-block";
    $$DD("fileset2").style.display = "inline-block";
    $$DD("fileset3").style.display = "inline-block";
    $$DD("fileset4").style.display = "none";
    $$DD("fileset5").style.display = "none";
    $$DD("fileset6").style.display = "none";
    displayBlockUCASInput('gradediv', 'none', 6);
    displayBlockUCASInput('ucas', 'block', 3);
    $$DD("qualSubjName4").value = "Subject"; $$DD("qualSubjName5").value = "Subject"; $$DD("qualSubjName6").value = "Subject";
    $$DD("qualSubjName4_hidden").value = ""; $$DD("qualSubjName5_hidden").value = ""; $$DD("qualSubjName6_hidden").value = "";    
  } else if(qual == 'Qualification'){
    $$DD("grade1span").innerHTML = one;
    $$DD("grade2span").innerHTML = two;
    $$DD("grade3span").innerHTML = three;
    $$DD("grade4span").innerHTML = four;
    $$DD("grade5span").innerHTML = five;
    $$DD("grade6span").innerHTML = six;
  }
}

function onchageClearQual(obj){
    var qual = $$DD(obj).value;
    if(qual == "AL"){
        $$DD("qualSubjName1").value = "Subject"; $$DD("qualSubjName2").value = "Subject"; $$DD("qualSubjName3").value = "Subject"; 
        $$DD("qualSubjName4").value = "Subject";
        $$DD("qualSubjName1_hidden").value = ""; $$DD("qualSubjName2_hidden").value = ""; $$DD("qualSubjName3_hidden").value = "";
        $$DD("qualSubjName4_hidden").value = "";
        $$DD("grade1span").innerHTML = "A*";       $$DD("grade2span").innerHTML = "A*";       $$DD("grade3span").innerHTML = "A*";
        $$DD("grade4span").innerHTML = "A*";
    }else if(qual == "H" || qual == "AH"){
        $$DD("qualSubjName1").value = "Subject"; $$DD("qualSubjName2").value = "Subject"; $$DD("qualSubjName3").value = "Subject"; 
        $$DD("qualSubjName4").value = "Subject"; $$DD("qualSubjName5").value = "Subject"; $$DD("qualSubjName6").value = "Subject";
        $$DD("qualSubjName1_hidden").value = ""; $$DD("qualSubjName2_hidden").value = ""; $$DD("qualSubjName3_hidden").value = "";
        $$DD("qualSubjName4_hidden").value = ""; $$DD("qualSubjName5_hidden").value = ""; $$DD("qualSubjName6_hidden").value = "";
        $$DD("grade1span").innerHTML = "A";       $$DD("grade2span").innerHTML = "A";       $$DD("grade3span").innerHTML = "A";
        $$DD("grade4span").innerHTML = "A";       $$DD("grade5span").innerHTML = "A";       $$DD("grade6span").innerHTML = "A";
    }else if(qual == "TP"){
        $$DD("qualSubjName1").value = "Subject"; $$DD("qualSubjName2").value = "Subject"; $$DD("qualSubjName3").value = "Subject";         
        $$DD("qualSubjName1_hidden").value = ""; $$DD("qualSubjName2_hidden").value = ""; $$DD("qualSubjName3_hidden").value = "";        
        $$DD("ucas1").value = "";      $$DD("ucas2").value = "";     $$DD("ucas3").value = "";
    }else if(qual == "Qualification"){
        $$DD("qualSubjName1").value = "Subject"; $$DD("qualSubjName2").value = "Subject"; $$DD("qualSubjName3").value = "Subject"; 
        $$DD("qualSubjName4").value = "Subject"; $$DD("qualSubjName5").value = "Subject"; $$DD("qualSubjName6").value = "Subject";
        $$DD("qualSubjName1_hidden").value = ""; $$DD("qualSubjName2_hidden").value = ""; $$DD("qualSubjName3_hidden").value = "";
        $$DD("qualSubjName4_hidden").value = ""; $$DD("qualSubjName5_hidden").value = ""; $$DD("qualSubjName6_hidden").value = "";
        $$DD("grade1span").innerHTML = "A*";       $$DD("grade2span").innerHTML = "A*";       $$DD("grade3span").innerHTML = "A*";
        $$DD("grade4span").innerHTML = "A*";       $$DD("grade5span").innerHTML = "A*";       $$DD("grade6span").innerHTML = "A*";
    } 
    $$DD("qualSubjName7").value = "Please enter subject"; $$DD("qualSubjName8").value = "Please enter subject";
    $$DD("qualSubjName9").value = "Please enter subject";
    $$DD("qualSubjName7_hidden").value = ""; $$DD("qualSubjName8_hidden").value = ""; $$DD("qualSubjName9_hidden").value = "";
}

function displayBlockUCASInput(id, type, count)
{
  //alert('1');
  for(var i = 1; i <=count; i++){
    var fId = id+i;
    //alert(fId);
    $$DD(fId).style.display = type;
    //alert($$DD(fId).style.display);
  }
}

function createSelectBoxUL(qual, selectBoxId, spanId){
  var valueArray;
  if(qual == 'AL'){
    valueArray = ["A*", "A", "B", "C", "D", "E"];
  }else if(qual == 'H' || qual == 'AH'){
    valueArray = ["A", "B", "C", "D"]
  }
  var arrayLength = valueArray.length;
  //alert(arrayLength);
  if($$DD(selectBoxId) != null){
    $$(selectBoxId).innerHTML = "";
    for(var i = 0; i < arrayLength; i++){
      var option = document.createElement("option");
      option.value = valueArray[i];
      option.appendChild(document.createTextNode(valueArray[i]));
      $$DD(selectBoxId).appendChild(option);
    }
    $$DD(spanId).innerHTML = valueArray[0];
  }
}


function showMoreLess(obj, rowNum){
  var id = obj.id;
  if(id == 'more_'+rowNum){
    document.getElementById('expandtable_'+rowNum).style.display = 'block';
    document.getElementById('collapsetable_'+rowNum).style.display = 'none';
  }else if(id == 'less_'+rowNum){
    document.getElementById('collapsetable_'+rowNum).style.display = 'block';
    document.getElementById('expandtable_'+rowNum).style.display = 'none';
  }
} 

function empInfo(jacsCode, searchGrades, equiTP){
  $$DD("empJacsCode").value = jacsCode;
  $$DD("searchGrades").value = searchGrades;
  $$DD("equiTP").value = equiTP;
  //alert($$DD("empJacsCode").value);
  document.getElementById('ulsearchform').action = ulSubmitURL;
  document.getElementById('ulsearchform').submit();
}
  
function showUniEmpInfo(ukprn, collegeId, indexId){
  //alert('inside the function');
  if(trimString($$DD("ukprn_"+indexId).innerHTML) == "" ){
    //alert('inside the function innerHTML empty');
    var loadId = "load_"+indexId;
    var loadImg = '<img src="'+wu_scheme+document.domain+'/wu-cont/images/ajax-ldr.gif"/>';//Added wu_scheme by Indumathi.S Mar-29-2016
    if($$(loadId)){
      $$(loadId).innerHTML = loadImg;
      $$(loadId).style.display = 'block';
    }    
    var uniUKPRN = ukprn;
    var ulLogId = $$DD("empLogid").value;
    var ulJacscode = $$DD("empJacsCode").value;
    var grades = $$DD("grades").value;
    var tp = $$DD("tariffpoints").value;
    var url="/degrees/loadUniEmpInfo.html?ukprn="+uniUKPRN+"&logId="+ulLogId+"&jacscode="+ulJacscode+"&collegeid="+collegeId+"&searchgrades="+grades+"&equitp="+tp;
    //alert("url--------"+url)
    var ajax=new sack();
    ajax.requestFile = url;	
    ajax.onCompletion = function(){ showUniEmpInfoResponse(ajax, uniUKPRN, indexId); };	
    ajax.runAJAX();  
  }else{
    //alert('inside the function innerHTML not empty');
    if($$DD("ukprn_"+indexId).style.display == 'none'){
      $$DD("ukprn_"+indexId).style.display= 'block';
    $$DD("div_ul_"+indexId).className= 'hd';
    }else{
      $$DD("ukprn_"+indexId).style.display= 'none';
    $$DD("div_ul_"+indexId).className= 'hd act';
    }
  }
}

function showUniEmpInfoResponse(ajax, uniUKPRN, indexId){
  var ukprn = uniUKPRN;
  if(ajax.response != ''){
    $$DD("ukprn_"+indexId).innerHTML= ajax.response;
    $$DD("ukprn_"+indexId).style.display= 'block';
    $$DD("div_ul_"+indexId).className= 'hd';
    if($$DD(ukprn+"-JS1")){eval($$DD(ukprn+"-JS1").innerHTML);}
    if($$DD(ukprn+"-JS2")){eval($$DD(ukprn+"-JS2").innerHTML);}
    var loadId = "load_"+indexId;
    if($$(loadId)){
      $$(loadId).style.display = 'none';
    }    
  }else{
  } 
}

function showIWanttobeUniEmpInfo(ukprn, collegeId, indexId){

  if(trimString($$DD("ukprn_"+indexId).innerHTML) == "" ){
    var uniUKPRN = ukprn;
    var searchType = $$DD("iwantSearchType").value;
    var searchCode = $$DD("iwantSearchCode").value;
    var loadId = "load_"+indexId;
    var loadImg = '<img src="'+wu_scheme+document.domain+'/wu-cont/images/ajax-ldr.gif"/>';//Added wu_scheme by Indumathi.S Mar-29-2016
    if($$(loadId)){
      $$(loadId).innerHTML = loadImg;
      $$(loadId).style.display = 'block';
    }    
    var url="/degrees/loadIWanttobeUniEmpInfo.html?ukprn="+uniUKPRN+"&searchtype="+searchType+"&searchcode="+searchCode+"&collegeid="+collegeId;
    //alert("url--------"+url)
    var ajax=new sack();
    ajax.requestFile = url;	
    ajax.onCompletion = function(){ showIWanttobeUniEmpInfoResponse(ajax, uniUKPRN, indexId); };	
    ajax.runAJAX();  
  }else{
    //alert('inside the function innerHTML not empty');
    if($$DD("ukprn_"+indexId).style.display == 'none'){
      $$DD("ukprn_"+indexId).style.display= 'block';
    $$DD("div_ul_"+indexId).className= 'hd';
    }else{
      $$DD("ukprn_"+indexId).style.display= 'none';
    $$DD("div_ul_"+indexId).className= 'hd act';
    }  
  }
}

function showIWanttobeUniEmpInfoResponse(ajax, uniUKPRN, indexId){
  var ukprn = uniUKPRN;
  if(ajax.response != ''){
    var searchName = $$DD("iwantSearchName").value;
    $$DD("ukprn_"+indexId).innerHTML= ajax.response;
    $$DD("ukprn_"+indexId).style.display= 'block';
    $$DD("div_ul_"+indexId).className= 'hd';
    $$DD("text_"+ukprn).innerHTML= searchName;
    var loadId = "load_"+indexId;
    if($$(loadId)){
      $$(loadId).style.display = 'none';
    }     
  }else{
  } 
}


function uniEmpInfoRatePaginationSort(pageNo, sorybywhich, sortbyhow, ulsearchtype){
  var uniPageNo = pageNo;
  var uniSortbywhich = sorybywhich;
  var uniSortbyhow = sortbyhow;
  var url; var grades; var eqiTP;
  var loadImg = '<img src="'+wu_scheme+document.domain+'/wu-cont/images/ldr.gif"/>';//Added wu_scheme by Indumathi.S Mar-29-2016
  if($$("load")){
    $$("load").innerHTML = loadImg;
    $$("load").style.display = 'block';
  }  
  if(ulsearchtype == 'WCID'){
    var ulLogId = $$DD("empLogid").value;
    var ulJacscode = $$DD("empJacsCode").value;
    grades = $$DD("grades").value;
    eqiTP = $$DD("tariffpoints").value;
    url="/degrees/whatcanidoempratesortpagination.html?pageno="+uniPageNo+"&logId="+ulLogId+"&jacscode="+ulJacscode+"&sortbywhich="+uniSortbywhich+"&sortbyhow="+uniSortbyhow;
  }else{
    var ulLogId = $$DD("empLogid").value;
    var searchType = $$DD("iwantSearchType").value;
    var searchCode = $$DD("iwantSearchCode").value;
    url="/degrees/iwanttobeempratesortpagination.html?pageno="+uniPageNo+"&searchtype="+searchType+"&searchcode="+searchCode+"&sortbywhich="+uniSortbywhich+"&sortbyhow="+uniSortbyhow;
  }
  //alert("url--------"+url)
  var ajax=new sack();
  ajax.requestFile = url;	
  ajax.onCompletion = function(){
    if(ulsearchtype == 'WCID'){
      uniEmpRateSortPaginationResponse(ajax, grades, eqiTP); 
    }else{
      IWantotbeEmpRateSortPaginationResponse(ajax); 
    }
  };
  ajax.runAJAX();
}

function uniEmpRateSortPaginationResponse(ajax, grades, eqiTP){
  $$DD("uniempinfo").innerHTML= ajax.response;
  $$DD("grades").value = grades;
  $$DD("tariffpoints").value = eqiTP;
  if($$("load")){
    $$("load").style.display = 'none';
  }  
}

function IWantotbeEmpRateSortPaginationResponse(ajax){
  $$DD("uniempinfo").innerHTML= ajax.response;
  if($$("load")){
    $$("load").style.display = 'none';
  }  
} 


function ulSearchResults(jacsSubject,jacsCode, searchGrades)
{
  var navKwd = trimString(jacsSubject);
  var qualDesc = 'degree';
  var navKwd1 = replaceAll(navKwd," ","+");
  var navKwd2 = replaceAll(navKwd," ","-");
  var country1 = 'united+kingdom';
  var country2 = 'united-kingdom';
  var srGrades = searchGrades;
  var srQual = $$DD("searchQual").value;
  var contextPath='/degrees';
  var url1 = "/degrees" + "/courses/" + qualDesc + "-courses/" + navKwd2 + "-" + qualDesc + "-courses-" + country2 + "/0" ;
  var url2 = "/m/" + country1 + "/" + country1 + "/25/0/0/0/r/0/1/0/uc/"+srQual+"/"+srGrades+"/";
  var url3 = "";
  var url3 = "page.html?jacs="+jacsCode;
  var finalUrl = url1 + url2 + url3;
  finalUrl = finalUrl.toLowerCase();
  window.open(finalUrl,'_blank');
	return false;
}

function iwnattobeEmpInfo(){
  document.getElementById('ulsearchform').action = ulSubmitURL;
  document.getElementById('ulsearchform').submit();
  return false;  
}

function tryAgaingFormSubmit(){
  document.getElementById('tryagainform').action = ulSubmitURL;
  document.getElementById('tryagainform').submit();
  return false;  
}

function prepopulateValues(){
  var qual = $$DD("qualsel").value;
  var gradeone = $$DD("selgradeone").value;
  var gradetwo = $$DD("selgradetwo").value;
  var gradethree = $$DD("selgradethree").value;
  var gradefour = $$DD("selgradefour").value;
  var gradefive = $$DD("selgradefive").value;
  var gradesix = $$DD("selgradesix").value;

  var qualVal = $$DD("qualsel");
  var text = qualVal.options[qualVal.selectedIndex].text;
  document.getElementById("qualspanul").innerHTML = text;  
  
  
  if(qual == 'AL'){
    $$DD("fileset1").style.display = "block";
    $$DD("fileset2").style.display = "block";
    $$DD("fileset3").style.display = "block";
    $$DD("fileset4").style.display = "block";
    $$DD("fileset5").style.display = "none";
    $$DD("fileset6").style.display = "none";
    displayBlockUCASInput('gradediv', 'block', 6);
    displayBlockUCASInput('ucas', 'none', 6);
    
    $$DD("grade1span").innerHTML = gradeone;
    $$DD("grade2span").innerHTML = gradetwo;
    $$DD("grade3span").innerHTML = gradethree;
    $$DD("grade4span").innerHTML = gradefour;
    
  }else if(qual == 'H' || qual == 'AH'){
    displayBlockUCASInput('fileset', 'block', 6);
    displayBlockUCASInput('gradediv', 'block', 6);
    displayBlockUCASInput('ucas', 'none', 6);

    $$DD("grade1span").innerHTML = gradeone;
    $$DD("grade2span").innerHTML = gradetwo;
    $$DD("grade3span").innerHTML = gradethree;
    $$DD("grade4span").innerHTML = gradefour;
    $$DD("grade5span").innerHTML = gradefive;
    $$DD("grade6span").innerHTML = gradesix;

  }else if(qual == 'TP'){
    $$DD("fileset1").style.display = "block";
    $$DD("fileset2").style.display = "block";
    $$DD("fileset3").style.display = "block";
    $$DD("fileset4").style.display = "none";
    $$DD("fileset5").style.display = "none";
    $$DD("fileset6").style.display = "none";

    displayBlockUCASInput('gradediv', 'none', 6);
    displayBlockUCASInput('ucas', 'block', 3);
  }  
}

function subjectValidation(){
  var qual = $$DD("qualsel").value;
  var noOfSub;
  var flag = 'ATLEASTONE';
  var j = 0;
  if(qual == 'AL'){
    noOfSub = 4; 
  }else if(qual == 'H' || qual == 'AH'){
    noOfSub = 6; 
  }else{
    noOfSub = 3; 
  }
  //alert('inside the function...');
  //alert(noOfSub);
  for(var i = 1; i <=noOfSub; i++){
    //alert(i);
    //var nextI = parseInt(i) + 1;
    //alert('---nextI----'+nextI);
    var currentSubStyleId = "qualSubjName"+i+"_hidden";
    //var nextSubStyleId = "qualSubjName"+nextI+"_hidden";
    //alert('---currentSubStyleId----- '+currentSubStyleId);
    //alert('---nextSubStyleId----- '+nextSubStyleId);
    var currentSub = trimString($$DD(currentSubStyleId).value);
    //alert('---currentSub---- '+currentSub);
    //var nexSub;
    //if($$DD(nextSubStyleId)){
    //  nexSub = trimString($$DD(nextSubStyleId).value); 
    //}
    //alert('---nexSub---- '+nexSub);
    if(currentSub != 'Subject' && currentSub != ""){
      flag = 'SUBMIT';
    }
    /*if(currentSub == "" && nexSub !== ""){
      flag = 'NOORDER';
      break;
    }*/
    if(qual == 'TP'){
      var ucasPoints = trimString($$DD("ucas"+i).value);
      if(ucasPoints == "" || ucasPoints == null){
        j = parseInt(j) + 1;
      }
      if(ucasPoints != "" && (isNaN(ucasPoints) || ucasPoints > 999)){
        flag = 'NOTVALIDTARIFF';
        break;
      }
    }
  }
  if( j == 3){
    flag = 'ATLEASTONETP';
  }


  return flag;
}

function whatcanidothirdtosecond(){
  document.getElementById('thirdtosecond').action = ulSubmitURL;
  document.getElementById('thirdtosecond').submit();
  return false; 
}

function whatcanidobacktosubject(obj){
  var id = obj.id;  
  document.getElementById('thirdtosecond').action = ulSubmitURL;
  document.getElementById('thirdtosecond').submit();
  return false; 
}

function iwanttobetryagain(){
  document.getElementById('tryagainform').action = ulSubmitURL;
  document.getElementById('tryagainform').submit();
}

function iwanttobeThirdtoSecond(){
  document.getElementById('iwantobethirdtosecond').action = ulSubmitURL;
  document.getElementById('iwantobethirdtosecond').submit();
}

function ulGAEventTracking(category, action, label, gaaccount){
  var eventCategory = 'widget-what-can-i-do';
  if(category == 'IWTB'){
    eventCategory = 'widget-i-want-to-be';
  }
  var eventAction = 'view degrees button';
  if(action == 'SG'){
    eventAction = 'subject guide button';
  }
  var eventLabel = label.toLowerCase();;
  var eventVal = 1;
  var booleanValue = true;  
  ga('create', gaaccount , 'auto', {'sampleRate': 80 , 'name': 'salesTracker'}); //Added by Amir for UA logging for 24-NOV-15 release
  ga('salesTracker.send', 'event', eventCategory , eventAction , eventLabel , eventVal , {nonInteraction: booleanValue}); //UA logging for 24-NOV-15 release  
}

function ulOnloadClick(){
  //alert('inside the ulOnloadClick');
  document.getElementById('div_ul_0').onclick();
}