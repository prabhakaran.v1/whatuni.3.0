 
var loadprocess = "";
var context_path = "/degrees";
function $$D(docid){ return document.getElementById(docid); }
function blockNone(thisid, value){  if($$D(thisid) !=null){ $$D(thisid).style.display = value;  }  }
function isEmpty(value){  if(trimString(value) ==''){ return true;} else {  return false; } }
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
var IEFlag = "Microsoft Internet Explorer";
var version = parseFloat(navigator.appVersion.split("MSIE")[1]);
function showErrorMsg(thisid, errmsg){  if($$D(thisid) !=null){ $$D(thisid).style.display = 'block';  $$D(thisid).innerHTML = errmsg;}  }

var objects = { 
   errors_div_1    : 'loginerrorMsg',
   errors_div_2    : 'registerErrMsg',
   errors_div_3    : 'forgotErrMsg'
} 
var ErrorMessage = { 
  errors_header                : "<p><strong><i>Please correct the following errors to proceed:</strong></i></p><ul>",
  errors_header_1              : "<ul>",
  errors_footer                : "</ul>",    
  error_login_password         : 'Enter your password',
  error_firstName              : 'Enter your first name',
  error_surName                : 'Enter your surname',
  error_userName               : 'Enter your user name',
  error_login_user_email       : 'Please enter your email and password',
  error_user_email             : 'Please enter your email',
  error_confirm_pass           : 'Your passwords do not match, please try again',
  error_valid_email            : 'Please enter your valid email address',
  error_year_of_entry          : 'What year would you like to start your new course?',
  error_termsc                 : 'Please agree to our terms and conditions',
  error_captcha                : 'Please enter the captcha code'
}

function AddErrors(errormessage){
   return "<li>"+errormessage+"</li>"
}

function showErrors(divid, message, showheader){
   if(showheader == null){
      message = ErrorMessage.errors_header + message + ErrorMessage.errors_footer;
   }else{
     message = ErrorMessage.errors_header_1 + message + ErrorMessage.errors_footer;
   }  
   $$D(divid).innerHTML = message;   
   blockNone(divid,'block');
}

//CHECKING VALID NUMBER FORMAT 
function checkValidEmail(email){

	if(email.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) == -1){
        return false;}
	return true;
}
//

//
function validateUserLogin(){
  var message = true;
  if($$D("loginpwd_errors")){$$D("loginpwd_errors").style.display = 'none'};
  if($$D("loginemail_errors")){$$D("loginemail_errors").style.display = 'none'};
  if($$D("loginemail_error")){$$D("loginemail_error").style.display = 'none'};
  if($$D("loginpass_error")){$$D("loginpass_error").style.display = 'none'};
  $$D("remember_login").value =  $$D("check").checked;
  $$D("emailAddress_hidden").value = $$D("emailAddress").value;
 
  var email =  trimString($$D("emailAddress").value);
  var password = trimString($$D("password").value);;
  if(isEmpty(email) || email == 'Enter email address'){ 
    message = false; 
    showErrorMsg('loginemail_error', ErrorMessage.error_user_email);   
  } else if(!checkValidEmail(email)){ message = false; showErrorMsg('loginemail_error', ErrorMessage.error_valid_email);  message = false;  }
  if(isEmpty(password) || password == 'Enter your password'){ message = false; showErrorMsg('loginpass_error', ErrorMessage.error_login_password);   message = false; }
  if(message == false){
    return false;
   }
  }
//
function validateUserRegister(){
  var message = true;
  if($$D("emailAddress_errors")){$$D("emailAddress_errors").style.display = 'none'}; 
  
  if($$D("firstName_error")){$$D("firstName_error").style.display = 'none';}
  if($$D("surName_error")){$$D("surName_error").style.display = 'none';}
  if($$D("emailAddress_error")){$$D("emailAddress_error").style.display = 'none';}
  if($$D("confirmPassword_error")){$$D("confirmPassword_error").style.display = 'none';}
  if($$D("password_error")){$$D("password_error").style.display = 'none';}
  if($$D("termsc_error")){$$D("termsc_error").style.display = 'none';}
  if($$D("error_year_of_entry")){$$D("error_year_of_entry").style.display = 'none';}
  var firstName = trimString($$D("firstName").value );   
  var surName = trimString($$D("surName").value );    
  var emailAddress =  trimString($$D("emailAddress").value );
  var newsLetter =  trimString($$D("newsLetter").value );
  var termsc =  trimString($$D("termsc").value );
  var password = trimString( $$D("password").value );
  var confirmPassword = trimString( $$D("confirmPassword").value );  
  var yoeVal = document.getElementById("year_to_join").value ;
  var firstNameErr = "false";
  var surNameErr = "false";
  var emailErr = "false";
  var pwdErr = "false";  
  var confirmPwdErr = "false";
  var errSet = "N";
  var yoeErr = "false";
  if(isEmpty(firstName) || firstName == 'Enter first name'){ message = false; showErrorMsg('firstName_error',ErrorMessage.error_firstName); firstNameErr = "true"; }
  if(isEmpty(surName) || surName == 'Enter surname'){ message = false; showErrorMsg('surName_error', ErrorMessage.error_surName); surNameErr = "true";}    
  if(isEmpty(emailAddress) || emailAddress == 'Enter email address'){ message = false; showErrorMsg('emailAddress_error', ErrorMessage.error_user_email); emailErr = "true";
  } else if(!checkValidEmail(emailAddress)){ message = false; showErrorMsg('emailAddress_error', ErrorMessage.error_valid_email); emailErr = "true";}
  if(isEmpty(password) || password == 'Enter password'){ message = false; showErrorMsg('password_error', ErrorMessage.error_login_password); pwdErr = "true";}
  if(isEmpty(confirmPassword) || confirmPassword == 'Enter confirm password'){ message = false; showErrorMsg('confirmPassword_error', ErrorMessage.error_confirm_pass); confirmPwdErr = "true";
  } else if(password != confirmPassword){message = false; showErrorMsg('confirmPassword_error', ErrorMessage.error_confirm_pass); confirmPwdErr = "true";}
  if($$D("termsc") == null || $$D("termsc").checked == false){ message = false; showErrorMsg('termsc_error',ErrorMessage.error_termsc); }  
  if($$D("newsLetter") != null && $$D("newsLetter").checked == false){newsLetter="N"; $$D("newsLetter_hidden").value="N";} else{newsLetter="Y"; $$D("newsLetter_hidden").value="Y"; }  
   if($$D("termsc") != null && $$D("termsc").checked == true){ $$D("terms_hidden").value="Y";} else{ $$D("terms_hidden").value="Y"; }
  if(isEmpty(yoeVal) || yoeVal == 'Choose year'){ message = false; showErrorMsg('error_year_of_entry', ErrorMessage.error_year_of_entry); yoeErr = "true";}
   $$D("remember_login").value =  $$D("check").checked;
   $$D("emailAddress_hidden").value = $$D("emailAddress").value;
   
 if(firstNameErr=="true"){
    $$D("firstName").focus();
    errSet = "Y";
 }
 if(errSet == "N"){
   if(surNameErr=="true"){
    $$D("surName").focus();
    errSet = "Y";
   }   
  if(emailErr=="true"){
      $$D("emailAddress").focus();
       errSet = "Y";
   }
   if(pwdErr=="true"){
       $$D("regtextpwd").focus();
       errSet = "Y";
   } 
   if(confirmPwdErr=="true"){
      $$D("confirmtextpwd").focus();
       errSet = "Y";
   }
  if(yoeErr=="true"){
      $$D("year_to_join").focus();
       errSet = "Y";
   }
 }
  if(message == false){
    return false;
  }
}
//
function showForgotDiv(){
 blockNone('forgotdiv', 'block');
}

//
function changePassword(passworddisplay, passwordhide, hidepwdId, currentId){ 
 if( $$D(hidepwdId).value.length<=0){
   if(currentId=='textpwd' || currentId=='regtextpwd' || currentId=='confirmtextpwd'||currentId=='loginpassword'){
     $$D(passwordhide).style.display="block";
     $$D(passwordhide).style.color = "#333";
     $$D(passworddisplay).style.display="none";     
     $$D(hidepwdId).focus();
   } else {
     $$D(passwordhide).style.display="none";
     $$D(passworddisplay).style.display="block";     
   }   
  }
}

function showBasicTermsPopup(){
   window.open("/degrees/help/termsAndCondition.htm","termspopup","width=650,height=400,scrollbars=yes,resizable=yes"); return false;
}

function clearDefaultLogin(obj, text){  
  if($$D(obj).value == text){
    $$D(obj).value = "";
    $$D(obj).style.color = "#333";
  }
}
function setDefaultLogin(obj, text){  
  if($$D(obj).value == ""){
    $$D(obj).value = text;
	   $$D(obj).style.color = "#888";
  }
}

function validateforgotpassword(){
 if($$D("emailAddress_err")){$$D("emailAddress_err").style.display = 'none'};  
 if($$D("emailAddress_error")){$$D("emailAddress_error").style.display = 'none'}; 
 if($$D("captchaCode_err")){$$D("captchaCode_err").style.display = 'none'};  
 if($$D("captchaCode_error")){$$D("captchaCode_error").style.display = 'none'}; 
  var emailAddress =  trimString($$D("emailAddress").value );
  var captchaCode =  trimString($$D("captchaCode").value );
  var message = true;
  $$D("emailAddress_hidden").value = $$D("emailAddress").value;
  if(isEmpty(emailAddress) || emailAddress == 'Enter email address'){ message = false; showErrorMsg('emailAddress_error', ErrorMessage.error_user_email);
  } else if(!checkValidEmail(emailAddress)){ message = false; showErrorMsg('emailAddress_error', ErrorMessage.error_valid_email); }
  if(isEmpty(captchaCode)){ message = false; showErrorMsg('captchaCode_error', ErrorMessage.error_captcha); } 
  if(message == false){
    return false;
  }
}

function cleardefaultText(id){
 if($$D(id).value == 'Enter email address'){ 
   $$D(id).value = '';
 }
}

function setSelectedtoSpan(obj, spanid){
    var selectionId = obj.id
    var el = document.getElementById(selectionId);
    var text = el.options[el.selectedIndex].text;
    document.getElementById(spanid).innerHTML = text;
}

function setUniOfYearDefault(val){
 for(var i = 0; i<5;i++ ){
  if(val == $$D("year_to_join").options[i].value)
    $$D("year_to_join").options[i].selected = true
  }
}

function validateResetPassword(){ //Added for wu_537 24-FEB-2015 by Karthi for reset password
  if($$D("confirmPassword_error")){$$D("confirmPassword_error").style.display = 'none';}
  if($$D("password_error")){$$D("password_error").style.display = 'none';}
  if($$D("captchaCode_error")){$$D("captchaCode_error").style.display = 'none';}
  if($$D("captchaCode_err")){$$D("captchaCode_err").style.display = 'none';}
  var password = trimString( $$D("password").value );
  var confirmPassword = trimString( $$D("confirmPassword").value );  
  var captchaCode =  trimString($$D("captchaCode").value );
  var message = true;
  if(isEmpty(password) || password == 'Enter password'){ message = false; showErrorMsg('password_error', ErrorMessage.error_login_password); pwdErr = "true";}
  if(isEmpty(confirmPassword) || confirmPassword == 'Enter confirm password'){ message = false; showErrorMsg('confirmPassword_error', ErrorMessage.error_confirm_pass); confirmPwdErr = "true";
  } else if(password != confirmPassword){message = false; showErrorMsg('confirmPassword_error', ErrorMessage.error_confirm_pass); confirmPwdErr = "true";}
  if(isEmpty(captchaCode)){ message = false; showErrorMsg('captchaCode_error', ErrorMessage.error_captcha); } 
  if(message == false){
    return false;
  }
  return true;
}