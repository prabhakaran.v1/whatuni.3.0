function ajaxloadnextset(pages){
var pageNo = $$("pageNo").value;
var pagesVal = parseInt(pages)-1 ;
pageNo = parseInt(pageNo) + 1;
if(pages >= pageNo){
$$("loader").style.display = "block";
$$("loader_text").style.display = "block";
var ajax=new sack();
var url= "/degrees" +  $$("urldata_1").value + pageNo + $$("urldata_2").value;
  var isClearing = $$("isclearingPage").value;
  if(isClearing=="TRUE"){url=url+"?nd&clearing";}
  url = (url.indexOf('?nd') > -1)? (url + "&ajaxFlag=Y&pageNO="+pageNo):(url + "?ajaxFlag=Y&pageNO="+pageNo); 
  ajax.requestFile = url;
  ajax.onCompletion = function(){ printresponse(ajax, pageNo, pages); };	
  ajax.runAJAX();
 }
}

function loadAjaxfunction(){
  var awdRes = jQuery.noConflict();  
  awdRes(window).scroll(function() {
    if($$("pageNo")) {
       if(parseInt($$("pageNo").value) < parseInt($$("totPageNo").value)) {
        var curPageNo = $$("pageNo").value;        
        var childDiv  = "#loadAjaxDiv_"+curPageNo;                
        if($$("loadAjaxDiv_"+curPageNo)) { 
          if(isScrolledIntoView(awdRes(childDiv))) {                          
            var nextPage = parseInt($$("pageNo").value) + 1;        
            ajaxloadnextset(nextPage); 
            resetHiddenValue();
          }  
        }        
      }
    } 
  }); 
}
function printresponse(ajax, pageNo, pages){
  var resp=ajax.response;
  $$("loader").style.display = "none";
  $$("loader_text").style.display = "none";
  $$('next_results_'+pageNo).innerHTML = resp;  
  $$('page_no_'+pageNo).innerHTML="Page "+pageNo;
 // $$("pageNo").value = pageNo;
}

function isScrolledIntoView(elem){
var opdup = jQuery.noConflict();
  var docViewTop = opdup(window).scrollTop();
  var docViewBottom = docViewTop + opdup(window).height();
  var elemTop = opdup(elem).offset().top;
  var elemBottom = elemTop + opdup(elem).height();
  return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom));
}

function resetHiddenValue(){
  var searchRes = jQuery.noConflict();
  searchRes("#pageNo").remove();
  searchRes("#totPageNo").remove();
}

function ajaxUniHOME(o,formObjId){
	var uniName = trimString(o.value)
	var	uniId = trimString($$((o.id+"_hidden")).value)
	var	uniNameId = trimString($$((o.id+"_id")).value)
	var	uniNameDisplay = trimString($$((o.id+"_display")).value)
	var	uniNameAlias = trimString($$((o.id+"_alias")).value)
	var	uniNameUrl = trimString($$((o.id+"_url")).value)
	//
	//alert(" 0) redirectUniHOME: \n 1) uniName: " + uniName + "\n 2) uniId: " + uniId+ "\n 3) uniNameId: " + uniNameId+ "\n 4) uniNameDisplay: " + uniNameDisplay + "\n 5) uniNameAlias: " + uniNameAlias + "\n 6) uniNameUrl: " + uniNameUrl)
	//
	if(subUni != uniNameUrl || subUni1 != uniNameUrl){
		if(uniNameUrl == '' || uniId == ''){
			alert(emptyUni);
			resetUniNAME(o);
			return false;
		}
	}else{
		alert(emptyUni);
		resetUniNAME(o);
		return false;
	}
	//
 var isTrackingRequired = "";
 if(document.getElementById("pageFrom")){
  isTrackingRequired = trimString(document.getElementById("pageFrom").value); 
 }
 if(isTrackingRequired !=null && isTrackingRequired !="" && isTrackingRequired =="ChoosingUni"){
   searchEventTracking('finduni','click', uniNameDisplay);
 }
	var uniURL = uniNameUrl;
	($$(formObjId)).action = uniURL;
	($$(formObjId)).submit();
	return false;
}
//
function resetUniNAME(o){
	if($$((o.id+"_hidden")) != null){$$((o.id+"_hidden")).value = '';}
	if($$((o.id+"_id")) != null){$$((o.id+"_id")).value = '';}
	if($$((o.id+"_display")) != null){$$((o.id+"_display")).value = '';}
	if($$((o.id+"_alias")) != null){$$((o.id+"_alias")).value = '';}
	if($$((o.id+"_url")) != null){$$((o.id+"_url")).value = '';}
}