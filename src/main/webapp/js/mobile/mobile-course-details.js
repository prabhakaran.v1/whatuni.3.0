 function loadFirstDelivery(){  
     document.getElementById('tc0').onclick(); 
  }
  
  
var contextPath = "/degrees";
function fetchModuleData(id, groupId, courseId, index, link) {
  if(document.getElementById("tc"+index).className == "hd act"){
    if(document.getElementById("module_data_"+index).innerHTML == ""){
       var ajax = new sack();
       url = contextPath + "/loadmoduledata.html?method=loadModuleData&moduleGroupId="+groupId + "&courseId=" + courseId +"&index="+index + "&mobileFlag=Y";
       ajax.requestFile = url;
       ajax.onCompletion = function(){ loadModuleData(ajax, index); };
       ajax.runAJAX();
      }else{
        closeOtherDiv1('brw-cat ms1', 'tc', 'hd act');      
        document.getElementById("tc"+index).className = "hd";
        document.getElementById("module_data_"+index).style.display = "block";
      }
   } else{
      document.getElementById("tc"+index).className = "hd act";
      document.getElementById("module_data_"+index).style.display = "none";
   }
} 

function loadModuleData(ajax, index){
  var response = ajax.response;
  closeOtherDiv1('brw-cat ms1', 'tc', 'hd act');  
  document.getElementById("tc"+index).className = "hd";
  document.getElementById("module_data_"+index).style.display = "block";
  document.getElementById("module_data_"+index).innerHTML = response;  
}


function loadModuleDetailDescription(moduleId, moduleGroupId, courseId, rowindex, mobileIndex){
 if(document.getElementById("inner_div_"+ mobileIndex +"_"+rowindex).className == "hd act"){
    if(document.getElementById("module_data_detail_" +moduleGroupId + "_" +rowindex).innerHTML == ""){
      var ajax = new sack();
       url = contextPath + "/loadmoduledata.html?method=loadModuleDetail&moduleGroupId="+ moduleGroupId + "&moduleId="+moduleId + "&courseId=" + courseId + "&mobileFlag=Y" +"&index="+rowindex;
       ajax.requestFile = url;
       ajax.onCompletion = function(){ loadModuleDetailData(ajax, rowindex, moduleGroupId, mobileIndex); };
       ajax.runAJAX();
    }else{
       closeOtherDiv1('brw-cat m1', "inner_div_"+ mobileIndex+"_", 'hd act');        
       document.getElementById("inner_div_"+ mobileIndex +"_"+rowindex).className = "hd";
       document.getElementById("module_data_detail_"+ moduleGroupId + "_"+rowindex).style.display = "block";
   }
  }else{
      document.getElementById("inner_div_"+ mobileIndex +"_"+rowindex).className = "hd act";
      document.getElementById("module_data_detail_"+ moduleGroupId + "_" +rowindex).style.display = "none";
  }
}
function loadModuleDetailData(ajax, rowindex, moduleGroupId, mobileIndex){

  var response = ajax.response;
  closeOtherDiv1('brw-cat m1', "inner_div_"+ mobileIndex+"_", 'hd act');       
  document.getElementById("inner_div_"+ mobileIndex +"_"+rowindex).className = "hd";
  document.getElementById("module_data_detail_"+ moduleGroupId +"_"+rowindex).style.display = "block";
  document.getElementById("module_data_detail_"+ moduleGroupId +"_"+rowindex).innerHTML = response;
}  

function closeOtherDiv1(displayClassname, activeId, activeClassName){
  var cusid_ele = document.getElementsByClassName(displayClassname);
  for (var i = 0; i < cusid_ele.length; ++i) {
      if(document.getElementById(activeId+[i])){
        //alert('inside the if conditon');
        var ele = document.getElementById(activeId+[i]);
        ele.className = activeClassName;
      }      
      var item = cusid_ele[i];  
      item.style.display = 'none';
  }
} 

function closeOtherDiv(displayClassname, activeId, activeClassName){
  var cusid_ele = document.getElementsByClassName(displayClassname);
  for (var i = 0; i < cusid_ele.length; ++i) {
      if(document.getElementById(activeId+[i])){
        //alert('inside the if conditon');
        var ele = document.getElementById(activeId+[i]);
        ele.className = activeClassName;
      }      
      var item = cusid_ele[i];  
      item.style.display = 'none';
  }
}    
  function showMoreLess(id, rowId){
    if(id+rowId == 'lesscontent'+rowId){
      document.getElementById('morecontent'+rowId).style.display = "block";
      document.getElementById('lesscontent'+rowId).style.display = "none";
    }else{
      document.getElementById('lesscontent'+rowId).style.display = "block";
      document.getElementById('morecontent'+rowId).style.display = "none";
    }
  }
  
function toggle(id, link) {
var d = document.getElementById(id);
if(link.className == 'hd' && (d.style.display == '')){
    
    link.className = 'hd act';
    d.style.display = 'none';
  }else{
     closeOtherDiv();
     var e = document.getElementById(id);
     if (e.style.display == '') {
      e.style.display = 'none';
      link.className = 'hd act';
     } else {
      e.style.display = '';
      link.className = 'hd';
    }
  }
}


function closeOtherDiv(){
  var cusid_ele = document.getElementsByClassName('brw-cat');
  for (var i = 0; i < cusid_ele.length; ++i) {
      var item = cusid_ele[i];  
      item.style.display = 'none';
  }
  for (var x = 1; x <= cusid_ele.length; ++x) {
      if(document.getElementById("tc"+[x])){
      var ele = document.getElementById("tc"+[x]);
      ele.className = 'hd act';
      }      
  }
}  

function toggle1(id, link) {
  var e = document.getElementById(id);
  if (e.style.display == '') {
    e.style.display = 'none';
    link.className = 'hd act';
  } else {
    e.style.display = '';
    link.className = 'hd';
  }
}  