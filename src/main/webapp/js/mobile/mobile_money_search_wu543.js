function replaceAll(replacetext,findstring,replacestring){
var strReplaceAll=replacetext;var intIndexOfMatch=strReplaceAll.indexOf(findstring)
while(intIndexOfMatch !=-1){
strReplaceAll=strReplaceAll.replace(findstring,replacestring);intIndexOfMatch=strReplaceAll.indexOf(findstring)}
return strReplaceAll}

function $$(id){
  return document.getElementById(id);
}

function SetCookie(name,value,expires,path,domain,secure){
document.cookie=name+"="+escape(value)+
((expires)? "; expires="+expires.toGMTString(): "")+
((path)? "; path="+path : "")+
((domain)? "; domain="+domain : "")+
((secure)? "; secure" : "")}

function showFilterPage(){    
   var pageUrl = "/degrees" + document.getElementById("urlString").value;
   var pagename = returnPageName(pageUrl);
   pageUrl = returnClearingRefineUrl(pageUrl);
   pageUrl = pageUrl.replace("/page.html", "/refine.html");//30_JUN_2015_REL     
   document.getElementById("searchBean").action =  pageUrl;
   document.getElementById("pageName").value =  pagename;
	  document.getElementById("searchBean").submit();
 }


function changeFunc(obj,s2,id) {    
    $$(s2).innerHTML = obj.value + " " +id;
}

function changeFunc1(obj,s2) {
    $$(s2).innerHTML = obj.value;
}
 
function changeEntryPoints(obj, entryLevelReset, astarvaluereset, avaluereset, bvaluereset, cvaluereset, dvaluereset, evaluereset, dstarvaluereset, btecdvaluereset, mvaluereset, pvaluereset, tariffPoints) {        
        var valueArray = {"Alevels":5, "SQAHighers":8, "SQAAdvancedHighers":8, "BTEC":3};
        //var enableBoxes = {"Alevels":"A*, A*span, A, Aspan, B, Bspan, C, Cspan, D, Dspan, E, Espan", "SQAHighers":"A, Aspan, B, Bspan, C, Cspan, D, Dspan", "SQAAdvancedHighers":"A, Aspan, B, Bspan, C, Cspan, D, Dspan", "Tarrif Point":"tp"};
        if(obj.value == "A-levels"){
            var optString = "";
            var count = valueArray.Alevels;
            var selBoxAStar = document.getElementById("selBoxA*");
            var selBoxA = document.getElementById("selBoxA");
            var selBoxB = document.getElementById("selBoxB");
            var selBoxC = document.getElementById("selBoxC");
            var selBoxD = document.getElementById("selBoxD");
            var selBoxE = document.getElementById("selBoxE");
 
            $$('selBoxA*').innerHTML = "";
            $$('selBoxA').innerHTML = "";
            $$('selBoxB').innerHTML = "";
            $$('selBoxC').innerHTML = "";
            $$('selBoxD').innerHTML = "";
            $$('selBoxE').innerHTML = "";
            for(var i = 0; i<=count; i++){
                var opt = document.createElement("option");
                opt.value=""+i;
                opt.appendChild(document.createTextNode(i));
                selBoxAStar.appendChild(opt);
                var opt1 = document.createElement("option");
                opt1.value=""+i;
                opt1.appendChild(document.createTextNode(i));
                selBoxA.appendChild(opt1);
                var opt2 = document.createElement("option");
                opt2.value=""+i;
                opt2.appendChild(document.createTextNode(i));
                selBoxB.appendChild(opt2);
                var opt3 = document.createElement("option");
                opt3.value=""+i;
                opt3.appendChild(document.createTextNode(i));
                selBoxC.appendChild(opt3);
                var opt4 = document.createElement("option");
                opt4.value=""+i;
                opt4.appendChild(document.createTextNode(i));
                selBoxD.appendChild(opt4);
                var opt5 = document.createElement("option");
                opt5.value=""+i;
                opt5.appendChild(document.createTextNode(i));
                selBoxE.appendChild(opt5);
            }
            $$("A*").style.display = "block";
            $$("A").style.display = "block";
            $$("B").style.display = "block";
            $$("C").style.display = "block";
            $$("D").style.display = "block";
            $$("E").style.display = "block";
            $$("D*").style.display = "none";
            $$("BtecD").style.display = "none";
            $$("M").style.display = "none";
            $$("P").style.display = "none";
            $$("tp").style.display = "none";
             $$("parentA*").className ="flft"
            $$("parentA").className = "frgt";
            $$("parentB").className = "flft";
            $$("parentC").className = "frgt";
            $$("parentD").className = "flft";
            $$("parentE").className = "frgt";
            
        }else if(obj.value == "SQA Highers" || obj.value == "SQA Advanced Highers"){
            var count = valueArray.SQAHighers;
            var optString = "";
            var selBoxA = document.getElementById("selBoxA");
            var selBoxB = document.getElementById("selBoxB");
            var selBoxC = document.getElementById("selBoxC");
            var selBoxD = document.getElementById("selBoxD");
            $$('selBoxA').innerHTML = "";
            $$('selBoxB').innerHTML = "";
            $$('selBoxC').innerHTML = "";
            $$('selBoxD').innerHTML = "";
            for(var i = 0; i<=count; i++){
                var chopt = document.createElement("option");
                chopt.value=""+i;
                chopt.appendChild(document.createTextNode(i));
                var chopt1 = document.createElement("option");
                chopt1.value=""+i;
                chopt1.appendChild(document.createTextNode(i));
                var chopt2 = document.createElement("option");
                chopt2.value=""+i;
                chopt2.appendChild(document.createTextNode(i));
                var chopt3 = document.createElement("option");
                chopt3.value=""+i;
                chopt3.appendChild(document.createTextNode(i));
                selBoxA.appendChild(chopt);
                selBoxB.appendChild(chopt1);
                selBoxC.appendChild(chopt2);
                selBoxD.appendChild(chopt3);
            }
            //$$("upt").disabled = false;
            $$("A*").style.display = "none";             
            $$("A").style.display = "block";          
            $$("B").style.display = "block";        
            $$("C").style.display = "block";         
            $$("D").style.display = "block";         
            $$("E").style.display = "none"; 
            $$("D*").style.display = "none";
            $$("BtecD").style.display = "none";
            $$("M").style.display = "none";
            $$("P").style.display = "none";
            $$("tp").style.display = "none";
            $$("parentA*").className =""
            $$("parentA").className = "flft";
            $$("parentB").className = "frgt";
            $$("parentC").className = "flft";
            $$("parentD").className = "frgt";
            $$("parentE").className = "";
            
        }else if(obj.value == "BTEC"){
            var optString = "";
            var count = valueArray.BTEC;
            var selBoxDStar = document.getElementById("selBoxD*");
            var selBoxBtecD = document.getElementById("selBoxBtecD");
            var selBoxM = document.getElementById("selBoxM");
            var selBoxP = document.getElementById("selBoxP");            
 
            $$('selBoxD*').innerHTML = "";
            $$('selBoxBtecD').innerHTML = "";
            $$('selBoxM').innerHTML = "";
            $$('selBoxP').innerHTML = "";
            
            for(var i = 0; i<=count; i++){
                var opt = document.createElement("option");
                opt.value=""+i;
                opt.appendChild(document.createTextNode(i));
                selBoxDStar.appendChild(opt);
                var opt1 = document.createElement("option");
                opt1.value=""+i;
                opt1.appendChild(document.createTextNode(i));
                selBoxBtecD.appendChild(opt1);
                var opt2 = document.createElement("option");
                opt2.value=""+i;
                opt2.appendChild(document.createTextNode(i));
                selBoxM.appendChild(opt2);
                var opt3 = document.createElement("option");
                opt3.value=""+i;
                opt3.appendChild(document.createTextNode(i));
                selBoxP.appendChild(opt3);                
            }
            $$("A*").style.display = "none";            
            $$("A").style.display = "none";            
            $$("B").style.display = "none";           
            $$("C").style.display = "none";
            $$("D").style.display = "none";
            $$("E").style.display = "none";
            $$("D*").style.display = "block";
            $$("BtecD").style.display = "block";
            $$("M").style.display = "block";
            $$("P").style.display = "block";            
            $$("tp").style.display = "none";
            $$("parentA*").className =""
            $$("parentA").className = "";
            $$("parentB").className = "";
            $$("parentC").className = "";
            $$("parentD").className = "";
            $$("parentE").className = "";
            $$("parentD*").className ="flft"
            $$("parentBtecD").className = "frgt";
            $$("parentM").className = "flft";
            $$("parentP").className = "frgt"
            
        }else if(obj.value == "Tariff Points"){           
            $$("A*").style.display = "none";            
            $$("A").style.display = "none";            
            $$("B").style.display = "none";           
            $$("C").style.display = "none";
            $$("D").style.display = "none";
            $$("E").style.display = "none";
            $$("D*").style.display = "none";
            $$("BtecD").style.display = "none";
            $$("M").style.display = "none";
            $$("P").style.display = "none";
            $$("tp").style.display = "block";
            $$("parentA*").className =""
            $$("parentA").className = "";
            $$("parentB").className = "";
            $$("parentC").className = "";
            $$("parentD").className = "";
            $$("parentE").className = "";
            $$("parentD*").className =""
            $$("parentBtecD").className = "";
            $$("parentM").className = "";
            $$("parentP").className = "";
        }else{
            $$('selBoxA*').innerHTML = '0';
            $$('selBoxA').innerHTML = '0';
            $$('selBoxB').innerHTML = '0';
            $$('selBoxC').innerHTML = '0';
            $$('selBoxD').innerHTML = '0';
            $$('selBoxE').innerHTML = '0';
            $$('selBoxD*').innerHTML = '0';
            $$('selBoxBtecD').innerHTML = '0';
            $$('selBoxM').innerHTML = '0';
            $$('selBoxP').innerHTML = '0';
            $$("tp").style.display = "none";
            $$("upt").disabled = true;
        }
        if(obj.value == entryLevelReset){
            $$('sBoxA*').innerHTML = astarvaluereset + " A*";
            $$('sBoxA').innerHTML = avaluereset + " A";
            $$('sBoxB').innerHTML = bvaluereset + " B";
            $$('sBoxC').innerHTML = cvaluereset + " C";
            $$('sBoxD').innerHTML = dvaluereset + " D";
            $$('sBoxE').innerHTML = evaluereset + " E";
            $$('sBoxD*').innerHTML = astarvaluereset + " D*";
            $$('sBoxBtecD').innerHTML = avaluereset + " D";
            $$('sBoxM').innerHTML = bvaluereset + " M";
            $$('sBoxP').innerHTML = cvaluereset + " P";
            $$("tp").value = tariffPoints;
        }
        else{
            $$('sBoxA*').innerHTML = '0 A*';
            $$('sBoxA').innerHTML = '0 A';
            $$('sBoxB').innerHTML = '0 B';
            $$('sBoxC').innerHTML = '0 C';
            $$('sBoxD').innerHTML = '0 D';
            $$('sBoxE').innerHTML = '0 E';
            $$('sBoxD*').innerHTML = '0 D*';
            $$('sBoxBtecD').innerHTML = '0 D';
            $$('sBoxM').innerHTML = '0 M';
            $$('sBoxP').innerHTML = '0 P';
            $$("tp").value = "";
        }
}


function loadAjaxfunction(){
 var awdRes = jQuery.noConflict();  
 awdRes(window).scroll(function() {
    if($$("pageNo")) {
       if(parseInt($$("pageNo").value) < parseInt($$("totPageNo").value)) {
        var curPageNo = $$("pageNo").value;        
        var childDiv  = "#loadAjaxDiv_"+curPageNo;                
        if($$("loadAjaxDiv_"+curPageNo)) {         
          
          if(isScrolledIntoView(awdRes(childDiv))) {                          
            var nextPage = parseInt($$("pageNo").value) + 1;
            ajaxloadnextset(nextPage); 
            resetHiddenValue();
          }  
        }        
      }
    } 
  });     
}

function isScrolledIntoView(elem){
var opdup = jQuery.noConflict();
  var docViewTop = opdup(window).scrollTop();
  var docViewBottom = docViewTop + opdup(window).height();
  var elemTop = opdup(elem).offset().top;
  var elemBottom = elemTop + opdup(elem).height();
  return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom));
}

function ajaxloadnextset(pages){
var pageNo = $$("pageNo").value;
var pagesVal = parseInt(pages)-1 ;
pageNo = parseInt(pageNo) + 1;
if(pages >= pageNo){
$$("loader").style.display = "block";
$$("loader_text").style.display = "block";
  var ajax=new sack();
  var url= "/degrees" +  $$("urldata_1").value + pageNo + $$("urldata_2").value;  
  var isClearing = $$("isclearingPage").value;
  if(isClearing=="TRUE"){url=url+"?nd&clearing";}
  var appendString = (url.indexOf('?nd') > -1)? "&":"?"; 
  url = url + appendString +"ajaxFlag=Y&pageNO="+pageNo+"&mobileStudyMode="+ $$("studyModeFromSearch").value;
  ajax.requestFile = url;
  ajax.onCompletion = function(){ printresponse(ajax, pageNo, pages); };	
  ajax.runAJAX();
 }
}

function printresponse(ajax, pageNo, pages){
  var resp=ajax.response;
  $$("loader").style.display = "none";
  $$("loader_text").style.display = "none";
  $$('next_results_'+pageNo).innerHTML = resp;  
  $$('page_no_'+pageNo).innerHTML="Page "+pageNo;
 
   if($$('gambanner'+pageNo+'2')){
	   eval($$('gambanner'+pageNo+'2').innerHTML);
  }
   if($$('gambanner'+pageNo+'5')){
	   eval($$('gambanner'+pageNo+'5').innerHTML);
  }
}

function resetHiddenValue(){
 var searchRes = jQuery.noConflict();
 searchRes("#pageNo").remove();
 searchRes("#totPageNo").remove();
}

function submitFilterKeyword(){ 
      $$("errordiv").innerHTML = "";
      $$("errordiv").style.display = 'none';
      var selectedSubject = "";
      var selectedSubjectValue =  "";
      var selectedStudyMode = "";
      var pageName = "page.html";//30_JUN_2015_REL
      if(document.getElementById('pageName')){
        pageName = document.getElementById('pageName').value;
        if(pageName=="clcourse"){//30_JUN_2015_REL
          pageName  = "page.html?nd&clearing";
        }else{
          pageName  = "page.html";
        }
      }
      if(document.getElementById('selBoxF')){
        selectedSubject = document.getElementById('selBoxF').options[document.getElementById('selBoxF').selectedIndex].text;
        selectedSubjectValue = document.getElementById('selBoxF').options[document.getElementById('selBoxF').selectedIndex].value;
      }    
      if(document.getElementById('selBoxH')){
        selectedStudyMode = document.getElementById('selBoxH').options[document.getElementById('selBoxH').selectedIndex].value;
      }
      var selectedLocation = document.getElementById('selBoxL').options[document.getElementById('selBoxL').selectedIndex].text;
      var selectedLocationValue = document.getElementById('selBoxL').options[document.getElementById('selBoxL').selectedIndex].value;      
      //      
      var searchUrl = $$("currentRule").value;
      var contextPath='/degrees';
      var entryLevel = "";
      var existingUrl = "";
      var dataarray = searchUrl.split("/")
      var urlLength = dataarray.length;
      var studyMode = $$("paramStudyLevelId").value;
      var entryPoints = "0";   
      var entryLevelValue = "";   
      var subjecname = $$("subjectname").value;
      var errmsg = "";
      if(studyMode == "M"){
       entryLevelValue = $$("selBox1").value;        
       switch(entryLevelValue){
          case 'A-levels': entryLevel='A_LEVEL';break
          case 'SQA Highers': entryLevel = 'SQA_HIGHER';break
          case 'SQA Advanced Highers': entryLevel = 'SQA_ADV';break
          case 'BTEC': entryLevel = 'BTEC';break
          case 'Tariff Points': entryLevel = 'UCAS_POINTS';break
        }
      if(entryLevelValue == "A-levels"){
        var astarValue = $$("selBoxA*").value;
        var aValue = $$("selBoxA").value;
        var bValue = $$("selBoxB").value;
        var cValue = $$("selBoxC").value;
        var dValue = $$("selBoxD").value;
        var eValue = $$("selBoxE").value;
        if(astarValue=="0" && aValue == "0" && bValue == "0" && cValue == "0" && dValue == "0" && eValue == "0"){
              entryPoints = "0";
            }else{
              entryPoints = astarValue+'A*-'+aValue+'A-'+bValue+'B-'+cValue+'C-'+dValue+'D-'+eValue+'E';
            }
        }else if(entryLevelValue == "SQA Highers" || entryLevelValue == "SQA Advanced Highers"){
            var aValue = $$("selBoxA").value;
            var bValue = $$("selBoxB").value;
            var cValue = $$("selBoxC").value;
            var dValue = $$("selBoxD").value;
            if($$("selBoxA").value == '0' && $$("selBoxB").value == '0' && $$("selBoxC").value == '0' && $$("selBoxD").value == '0'){
              entryPoints = "0";
            }else{
              entryPoints = aValue+'A-'+bValue+'B-'+cValue+'C-'+dValue+'D';
            }
        }else if(entryLevelValue == "BTEC"){
            var dstarValue = $$("selBoxD*").value;
            var btecdValue = $$("selBoxBtecD").value;
            var mValue = $$("selBoxM").value;
            var pValue = $$("selBoxP").value;        
            if(dstarValue=="0" && btecdValue == "0" && mValue == "0" && pValue == "0"){
              entryPoints = "0";
            }else{
              entryPoints = dstarValue+'D*-'+btecdValue+'D-'+mValue+'M-'+pValue+'P';
            }
        }else{
            if($$("tp").value == null || $$("tp").value.trim() ==""){
                errmsg = "Please enter the tariff points";
                $$("errordiv").innerHTML = errmsg;
                $$("errordiv").style.display = 'block';
               return false;
            }
            if(isNaN($$('tp').value)){
                errmsg = "Please enter a number for your tariff points.";
                $$("errordiv").innerHTML = errmsg;
                $$("errordiv").style.display = 'block';
                return false;
            }
            entryPoints = $$('tp').value; 
            if(entryPoints != null && entryPoints.length>=4){
              errmsg = "Tarrif points, you can enter only 3 characters";
              $$("errordiv").innerHTML = errmsg;
              $$("errordiv").style.display = 'block';
              return false;
            }            
         }
        }
        if(entryPoints == "0" && selectedSubjectValue != ""){      
          var seo_qual=''
		        var qual=$$("paramStudyLevelId").value;
	         switch(qual){
			         case 'M': seo_qual='degree';break
			         case 'N': seo_qual='hnd-hnc';break
			         case 'A': seo_qual='foundation-degree';break
			         case 'T': seo_qual='access-foundation';break
			         case 'L': seo_qual='postgraduate';break
		        }            
          var urlCatName = replaceHypen(replaceURL(selectedSubject));
              urlCatName = replaceAll(urlCatName, "All-", "");          
          var urlLocName = "";
            if(selectedLocationValue == ""){selectedLocation = "";}
                urlLocName = replaceHypen(replaceURL(selectedLocation));
                urlLocName = replaceAll(urlLocName, "ALL-", "");
                var urlLocNameplus = "";
                if(urlLocName == ""){
                  urlLocName = "united-kingdom";
                  urlLocNameplus = "united+kingdom";
                }else{
                   urlLocNameplus = urlLocName.split("-").join("+");
                } 
           $$("mobileStudyMode").value = selectedStudyMode;
          var url1 = contextPath + "/courses/" + seo_qual + "-courses/" + urlCatName + "-" + seo_qual + "-courses-" + urlLocName;
          var url2 = "/" + qual + "/" + urlLocNameplus + "/r/"+selectedSubjectValue+"/"+ pageName;//30_JUN_2015_REL		       
          var finalUrl= url1+url2;          
        }       
        
        else{
         var seo_qual=''
		           var qual=$$("paramStudyLevelId").value;
         switch(qual){
			            case 'M': seo_qual='degree';break
			            case 'N': seo_qual='hnd-hnc';break
			            case 'A': seo_qual='foundation-degree';break
			            case 'T': seo_qual='access-foundation';break
			            case 'L': seo_qual='postgraduate';break
		           }
        var urlCatName = "";
        var subnameChanged = "";
        if(selectedSubjectValue != ""){
            urlCatName = replaceHypen(replaceURL(selectedSubject));
            urlCatName = replaceAll(urlCatName, "All-", "");
            urlCatNamePlus = urlCatName.split("-").join("+");
            subnameChanged = "Y";
        }
        if(selectedSubjectValue == ""){
           if(subjecname !=null && subjecname !='null'){
             urlCatName = replaceHypen(replaceURL(subjecname));
             urlCatNamePlus = urlCatName.split("-").join("+");
            }else{
             urlCatNamePlus = dataarray[4];
             urlCatName = dataarray[4].split("+").join("-");
             }
        }       
        //        
        var urlLocName = "";
        if(selectedLocationValue == ""){selectedLocation = "";}
        urlLocName = replaceHypen(replaceURL(selectedLocation));
        urlLocName = replaceAll(urlLocName, "ALL-", "");
        var urlLocNameplus = "";
        if(urlLocName == ""){
          urlLocName = "united-kingdom";
          urlLocNameplus = "united+kingdom";
        }else{
          urlLocNameplus = urlLocName.split("-").join("+");
        }
        //
         var urlStudyMode = selectedStudyMode;
         if(urlStudyMode == ""){
          urlStudyMode = "0";
         }
         if(entryPoints=="0"){entryLevel = 0;}
         var url9 = dataarray[11];         
         if(url9 == "0"){ 
           if(subnameChanged == "Y"){
		           $$("studyLevelHidden").value = selectedSubject;
             var hid_sub_code=$$("qual_other_info").options[$$("studyLevelHidden").selectedIndex].text               
		           if(hid_sub_code !=''){
			            url9=hid_sub_code
               urlCatNamePlus = "0";
		           }else{
			            url9='0'
		           }
           }
         }else{
           urlCatNamePlus = "0";
         }         
         var url1 = contextPath + "/courses/" + seo_qual + "-courses/" + urlCatName + "-" + seo_qual + "-courses-" + urlLocName + "/" + urlCatNamePlus;
         var url2 = "/" + qual + "/" + urlLocNameplus + "/" + urlLocNameplus + "/25/0/"+urlStudyMode+"/" + url9 + "/r/"+  dataarray[13] +"/1/0/uc/"+ entryLevel + "/" + entryPoints + "/"+ pageName;//30_JUN_2015_REL
         
         var finalUrl = url1 + url2;
        }
        var urlll = finalUrl.toLowerCase();
        $$("filter").action=finalUrl.toLowerCase();        
        $$("filter").submit();   
}

function submitFilterBrowse(){ 
  $$("errordiv").innerHTML = "";
  $$("errordiv").style.display = 'none';
  var selectedSubject = "";
  var selectedSubjectValue =  "";
  var selectedStudyMode = "";
  var pageName = "page.html";//30_JUN_2015_REL
  if(document.getElementById('pageName')){
     pageName = document.getElementById('pageName').value;     
     if(pageName=="clcourse"){//30_JUN_2015_REL
        pageName  = "page.html?nd&clearing";
     }else{
        pageName  = "page.html";
     }
  }
  if(document.getElementById('selBoxF')){
    selectedSubject = document.getElementById('selBoxF').options[document.getElementById('selBoxF').selectedIndex].text;
    selectedSubjectValue = document.getElementById('selBoxF').options[document.getElementById('selBoxF').selectedIndex].value;
  }  
  if(document.getElementById('selBoxH')){
     selectedStudyMode = document.getElementById('selBoxH').options[document.getElementById('selBoxH').selectedIndex].value;
  }
   var selectedLocation = document.getElementById('selBoxL').options[document.getElementById('selBoxL').selectedIndex].text;
   var selectedLocationValue = document.getElementById('selBoxL').options[document.getElementById('selBoxL').selectedIndex].value;
   var searchUrl = $$("currentRule").value;
   var subjecname = $$("subjectname").value;
   var contextPath='/degrees';
        var entryLevel = "";
        var existingUrl = "";
        var dataarray = searchUrl.split("/")
        var urlLength = dataarray.length;
        var studyMode = $$("paramStudyLevelId").value;
        var entryPoints = "0";   
        var entryLevelValue = "";   
        if(studyMode == "M"){
        entryLevelValue = $$("selBox1").value;        
        switch(entryLevelValue){
          case 'A-levels': entryLevel='A_LEVEL';break
          case 'SQA Highers': entryLevel = 'SQA_HIGHER';break
          case 'SQA Advanced Highers': entryLevel = 'SQA_ADV';break
          case 'BTEC': entryLevel = 'BTEC';break
          case 'Tariff Points': entryLevel = 'UCAS_POINTS';break
        }
        if(entryLevelValue == "A-levels"){
            var astarValue = $$("selBoxA*").value;
            var aValue = $$("selBoxA").value;
            var bValue = $$("selBoxB").value;
            var cValue = $$("selBoxC").value;
            var dValue = $$("selBoxD").value;
            var eValue = $$("selBoxE").value;
            if(astarValue=="0" && aValue == "0" && bValue == "0" && cValue == "0" && dValue == "0" && eValue == "0"){
              entryPoints = "0";
            }else{
              entryPoints = astarValue+'A*-'+aValue+'A-'+bValue+'B-'+cValue+'C-'+dValue+'D-'+eValue+'E';
            }
        }else if(entryLevelValue == "SQA Highers" || entryLevelValue == "SQA Advanced Highers"){
            var aValue = $$("selBoxA").value;
            var bValue = $$("selBoxB").value;
            var cValue = $$("selBoxC").value;
            var dValue = $$("selBoxD").value;
            if($$("selBoxA").value == '0' && $$("selBoxB").value == '0' && $$("selBoxC").value == '0' && $$("selBoxD").value == '0'){
              entryPoints = "0";
            }else{
              entryPoints = aValue+'A-'+bValue+'B-'+cValue+'C-'+dValue+'D';
            }
        }else if(entryLevelValue == "BTEC"){
            var dstarValue = $$("selBoxD*").value;
            var btecdValue = $$("selBoxBtecD").value;
            var mValue = $$("selBoxM").value;
            var pValue = $$("selBoxP").value;        
            if(dstarValue=="0" && btecdValue == "0" && mValue == "0" && pValue == "0"){
              entryPoints = "0";
            }else{
              entryPoints = dstarValue+'D*-'+btecdValue+'D-'+mValue+'M-'+pValue+'P';
            }
        }else{        
            if($$("tp").value == null || $$("tp").value.trim() ==""){
                errmsg = "Please enter the tariff points";
                $$("errordiv").innerHTML = errmsg;
                $$("errordiv").style.display = 'block';
               return false;
            }
            if(isNaN($$('tp').value)){
                errmsg = "Please enter a number for your tariff points.";
                $$("errordiv").innerHTML = errmsg;
                $$("errordiv").style.display = 'block';
                return false;
            }
            entryPoints = $$('tp').value; 
            if(entryPoints != null && entryPoints.length>=4){
              errmsg = "Tariff points, you can enter only 3 characters";
              $$("errordiv").innerHTML = errmsg;
              $$("errordiv").style.display = 'block';
              return false;
            }    
          }
        }
        
       if(entryPoints == "0"){
           var seo_qual=''
		        var qual=$$("paramStudyLevelId").value;
	         switch(qual){
			         case 'M': seo_qual='degree';break
			         case 'N': seo_qual='hnd-hnc';break
			         case 'A': seo_qual='foundation-degree';break
			         case 'T': seo_qual='access-foundation';break
			         case 'L': seo_qual='postgraduate';break
		        }           
          var urlCatName = replaceHypen(replaceURL(selectedSubject));
              urlCatName = replaceAll(urlCatName, "All-", "");
              
          var urlLocName = "";
            if(selectedLocationValue == ""){selectedLocation = "";}
                urlLocName = replaceHypen(replaceURL(selectedLocation));
                urlLocName = replaceAll(urlLocName, "ALL-", "");
                var urlLocNameplus = "";
                if(urlLocName == ""){
                  urlLocName = "united-kingdom";
                  urlLocNameplus = "united+kingdom";
                }else{
                   urlLocNameplus = urlLocName.split("-").join("+");
                } 
          $$("mobileStudyMode").value = selectedStudyMode;
          var url1 = contextPath + "/courses/" + seo_qual + "-courses/" + urlCatName + "-" + seo_qual + "-courses-" + urlLocName;
          var url2 = "/" + qual + "/" + urlLocNameplus + "/r/"+selectedSubjectValue+"/"+ pageName;//30_JUN_2015_REL	       
          var finalUrl= url1+url2;         
        } else{
         var seo_qual=''
		           var qual=$$("paramStudyLevelId").value;
         switch(qual){
			            case 'M': seo_qual='degree';break
			            case 'N': seo_qual='hnd-hnc';break
			            case 'A': seo_qual='foundation-degree';break
			            case 'T': seo_qual='access-foundation';break
			            case 'L': seo_qual='postgraduate';break
		           }
        var urlCatName = replaceHypen(replaceURL(selectedSubject));
            urlCatName = replaceAll(urlCatName, "All-", "");
        if(selectedSubjectValue == ""){
          urlCatName = replaceHypen(replaceURL(subjecname));
        }
        urlCatNamePlus = urlCatName.split("-").join("+");
        //        
        var urlLocName = "";
        if(selectedLocationValue == ""){selectedLocation = "";}
        urlLocName = replaceHypen(replaceURL(selectedLocation));
        urlLocName = replaceAll(urlLocName, "ALL-", "");
        var urlLocNameplus = "";
        if(urlLocName == ""){
           urlLocName = "united-kingdom";
           urlLocNameplus = "united+kingdom";
        }else{
          urlLocNameplus = urlLocName.split("-").join("+");
        }
        //
        var urlStudyMode = selectedStudyMode;
         if(urlStudyMode == ""){
          urlStudyMode = "0";
         }
        var url_10='0';
		      $$("studyLevelHidden").value = selectedSubject;
        var hid_sub_code=$$("qual_other_info").options[$$("studyLevelHidden").selectedIndex].text               
		        if(hid_sub_code !=''){
			          url_10=hid_sub_code
		        }else{
			         url_10='0'
		         }
         if(entryPoints=="0"){entryLevel = 0;}      
         var url1 = contextPath + "/courses/" + seo_qual + "-courses/" + urlCatName + "-" + seo_qual + "-courses-" + urlLocName + "/0";
         var url2 = "/" + qual + "/" + urlLocNameplus + "/0" +  "/25/0/"+urlStudyMode+"/" + url_10 +"/r/0/1/0/uc/"+ entryLevel + "/" + entryPoints + "/"+ pageName;//30_JUN_2015_REL
         
         var finalUrl = url1 + url2;
        }
        var urlll = finalUrl.toLowerCase();
        $$("filter").action=finalUrl.toLowerCase();        
        $$("filter").submit();   
}

function resetAll(){

  
  if(document.getElementById('selBoxF')){ 
    document.getElementById('selBoxF').value =  $$("resetSubjectName").value;
    var subjectoptions= document.getElementById('selBoxF').options;    
    for (var j= 0; j< subjectoptions.length; j++) {  
    
        
        if (subjectoptions[j].value == $$("resetSujectId").value) {
            subjectoptions[j].selected= true;
            document.getElementById('selBoxF').selectedIndex = j;
            break;
        }
    }
    if($$("sBox1S")){$$("sBox1S").innerHTML = $$("resetSubjectName").value;}  
  }
  if(document.getElementById('selBoxL')){ 
     document.getElementById('selBoxL').value =  $$("resetLocationNmae").value;
     var locationoptions= document.getElementById('selBoxL').options;
     for (var i= 0; i< locationoptions.length; i++) {
        if (locationoptions[i].value == $$("resetLocationNmae").value) {
            locationoptions[i].selected= true;
            document.getElementById('selBoxL').selectedIndex = i;
            break;
         }
     }
    if($$("resetLocationNmae").value == ""){$$("resetLocationNmae").value = "Anywhere";}
    if($$("sBox1L")){  $$("sBox1L").innerHTML = $$("resetLocationNmae").value;}
  } 
  
 if(document.getElementById('selBoxH')){
    document.getElementById('selBoxH').value =  $$("resetStudyModeDesc").value;
    var studyModeoptions= document.getElementById('selBoxH').options;      
    for (var j= 0; j< studyModeoptions.length; j++) {  
        if (studyModeoptions[j].value == $$("resetStudyModeCode").value) {
            studyModeoptions[j].selected= true;
            document.getElementById('selBoxH').selectedIndex = j;
            break;
        }
    }
    if($$("sBox1C")){$$("sBox1C").innerHTML = $$("resetStudyModeDesc").value;}  
 }
 return false;
}

function backButton(){
  var searchUrl = $$("currentRule").value;
  var pagename = $$("pageName").value;
  searchUrl = "/degrees" + searchUrl;
  searchUrl = removeClearingRefineUrl(searchUrl);
  var url = replaceAll(searchUrl, "refine", pagename+".html");
  if(document.getElementById('selBoxH')){
    var selectedStudyMode = document.getElementById('selBoxH').options[document.getElementById('selBoxH').selectedIndex].value;
    $$("mobileStudyMode").value = selectedStudyMode;
  }
  $$("filter").action=url.toLowerCase();        
  $$("filter").submit();  
}

function deleteSubjectFilter(catId,catName){
   var pageUrl =  document.getElementById("urlString").value;
   var pagename = returnPageName(pageUrl);
   var dataarray = pageUrl.split("/")
   var url2 = dataarray[2];
   var url3 = dataarray[3];
   catName = replaceHypen(replaceURL(catName)).toLowerCase();
   url3 = catName + "-" + url3.substring(url3.indexOf(dataarray[2]));  
   var url7 = dataarray[7];
   var slash = "/";
   var newUrl = "/degrees/" + dataarray[1] + slash  + dataarray[2] + slash + url3 + slash + dataarray[4] + slash + dataarray[5] + slash + dataarray[6] + slash + catId + "/"+ pagename;
   $$("mobileStudyMode").value = $$("studyModeFromSearch").value;
   document.getElementById("searchBean").action =  newUrl.toLowerCase();
	  document.getElementById("searchBean").submit();
}

function deleteLocationFilter(selectedLocation){
   var pageUrl =  document.getElementById("urlString").value;
   var pagename = returnPageName(pageUrl);
   var dataarray = pageUrl.split("/")
   var url5 = dataarray[5].split("+").join("-");
   var url5loc = replaceHypen(replaceURL(selectedLocation)).toLowerCase();
   var url3 = dataarray[3];
   url3 =  replaceAll(url3, url5, "united-kingdom");
   url5 =  "united+kingdom"
   var slash = "/";
   var newUrl = "/degrees/" + dataarray[1] + slash  + dataarray[2] + slash + url3 + slash + dataarray[4] + slash + url5 + slash + dataarray[6] + slash + dataarray[7] + "/"+ pagename;
   $$("mobileStudyMode").value = $$("studyModeFromSearch").value;
   document.getElementById("searchBean").action =  newUrl.toLowerCase();
	  document.getElementById("searchBean").submit();
}

function deleteSubjectFilterKwd(catId,catName,catCode){
   var pageUrl =  document.getElementById("urlString").value;
   var pagename = returnPageName(pageUrl);
   var dataarray = pageUrl.split("/")
   var url2 = dataarray[2];
   var url3 = dataarray[3];
   catName = replaceHypen(replaceURL(catName)).toLowerCase();
   url3 = catName + "-" + url3.substring(url3.indexOf(dataarray[2]));  
   var slash = "/";
   var newUrl1 = "/degrees/" + dataarray[1] + slash  + dataarray[2] + slash + url3 + slash + dataarray[4] + slash + dataarray[5] + slash + dataarray[6] + slash;
   var newUrl2 = dataarray[7] + slash + dataarray[8] + slash + dataarray[9] + slash + dataarray[10] + slash + catCode + slash + dataarray[12] + slash + dataarray[13] + slash + dataarray[14] + slash + dataarray[15] + slash +dataarray[16] + slash  + dataarray[17] + slash + dataarray[18] + "/"+ pagename;
   var newUrl = newUrl1 + newUrl2;
   window.location.href=newUrl.toLowerCase();
}

function deleteLocationFilterKwd(selectedLocation){
   var pageUrl =  document.getElementById("urlString").value;
   var pagename = returnPageName(pageUrl);
   var dataarray = pageUrl.split("/")
   var url6 = dataarray[6].split("+").join("-");
   var url6loc = replaceHypen(replaceURL(selectedLocation)).toLowerCase();
   var url3 = dataarray[3];
   url3 =  replaceAll(url3, url6, "united-kingdom");
   url6 =  "united+kingdom"
   var slash = "/";
   var newUrl1 = "/degrees/" + dataarray[1] + slash  + dataarray[2] + slash + url3 + slash + dataarray[4] + slash + dataarray[5] + slash + url6 + slash;
   var newUrl2 = url6 + slash + dataarray[8] + slash + dataarray[9] + slash + dataarray[10] + slash + dataarray[11] + slash + dataarray[12] + slash + dataarray[13] + slash + dataarray[14] + slash + dataarray[15] + slash +dataarray[16]  + slash + dataarray[17] + slash + dataarray[18] + "/"+ pagename;
   var newUrl = newUrl1 + newUrl2;
   window.location.href=newUrl.toLowerCase();
}

function deleteCourseTypeFilterKwd(){
   var pageUrl =  document.getElementById("urlString").value;
   var pagename = returnPageName(pageUrl);
   var dataarray = pageUrl.split("/")
   var slash = "/";
   var newUrl1 = "/degrees/" + dataarray[1] + slash  + dataarray[2] + slash + dataarray[3] + slash + dataarray[4] + slash + dataarray[5] + slash + dataarray[6] + slash;
   var newUrl2 = dataarray[7] + slash + dataarray[8] + slash + dataarray[9] + slash + "0" + slash + dataarray[11] + slash + dataarray[12] + slash + dataarray[13] + slash + dataarray[14] + slash + dataarray[15] + slash +dataarray[16]  + slash + dataarray[17] + slash + dataarray[18] + "/"+ pagename;
   var newUrl = newUrl1 + newUrl2;
   window.location.href=newUrl.toLowerCase();
}

function removeGradeFilter(){
  var pageUrl =  document.getElementById("urlString").value;
  var pagename = returnPageName(pageUrl);
   var dataarray = pageUrl.split("/")
   var slash = "/";
   var newUrl1 = "/degrees/" + dataarray[1] + slash  + dataarray[2] + slash + dataarray[3] + slash + dataarray[4] + slash + dataarray[5] + slash + dataarray[6] + slash;
   var newUrl2 = dataarray[7] + slash + dataarray[8] + slash + dataarray[9] + slash + dataarray[10] + slash + dataarray[11] + slash + dataarray[12] + slash + dataarray[13] + slash + dataarray[14] + slash + dataarray[15] + slash +dataarray[16]  + "/0/0" + "/"+ pagename;
   var newUrl = newUrl1 + newUrl2;
   window.location.href=newUrl.toLowerCase();

}

function deleteCourseTypeFilterBrowse(){
   var pageUrl =  document.getElementById("urlString").value;
   var pagename = returnPageName(pageUrl);
   var dataarray = pageUrl.split("/");
   var slash = "/";
   var newUrl = "/degrees/" + dataarray[1] + slash  + dataarray[2] + slash + dataarray[3] + slash + dataarray[4] + slash + dataarray[5] + slash + dataarray[6] + slash + dataarray[7] + "/"+ pagename;
   window.location.href = newUrl.toLowerCase();
}
   
function returnPageName(pageUrl){//15_JUL_2014_REL
 var clrpagetype = $$D("isclearingPage").value;//30_JUN_2015_REL
 if(clrpagetype =="TRUE"){
    return "/page.html?nd&clearing";
   } 
  return "/page.html";
}
   
function returnClearingRefineUrl(pageUrl){//15_JUL_2014_REL
  var clrpagetype = $$D("isclearingPage").value;//30_JUN_2015_REL
  if(clrpagetype =="TRUE"){
    pageUrl = pageUrl.replace("/courses/","/clearing-courses/" ) ;
   } 
  return pageUrl;
}

function removeClearingRefineUrl(pageUrl){//15_JUL_2014_REL
 pageUrl = pageUrl.replace("/clearing-courses/","/courses/" ) ;
 return pageUrl;
}