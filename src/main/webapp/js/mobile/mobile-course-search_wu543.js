function autoCompleteBrowse(event,object){
  document.getElementById('selQual').value = document.getElementById('mobile_qual').value;
	ajax_showOptions(object,'keywordbrowse',event,"indicator",'qual='+ document.getElementById('mobile_qual').value + '&siteflag=mobile');
}
function coursesearchsubmit(){
  var mobileKeywor = document.getElementById("mobilebrowsecat").value;
  if(mobileKeywor == 'Enter subject or keyword' || mobileKeywor == ''){
    alert('Please enter a valid keyword.');
    $$('mobilebrowsecat').value = '';
    $$('mobilebrowsecat').focus();
    return false;
  }
  var qualification = document.getElementById("mobile_qual").value;
  var browseCatId = document.getElementById("mobilebrowsecat_hidden").value;
  var matchBrowseNode = document.getElementById("matchbrowsenode").value;
  var finalUrl = "";
  var contextPath='/degrees';
  var qualDesc = 'degree';
  var qualCode = "M";
  var country1 = 'united+kingdom';
  var country2 = 'united-kingdom';
  var pagename = "/page.html";
  if(qualification == 'M'){
    qualDesc = 'degree'; qualCode='M';
  }else if(qualification == 'L'){
    qualDesc = 'postgraduate'; qualCode='L';
  }else if(qualification == 'A'){
    qualDesc = 'foundation-degree'; qualCode='A';
  }else if(qualification == 'T'){
    qualDesc = 'access-foundation'; qualCode='T';
  }else if(qualification == 'N'){
    qualDesc = 'hnd-hnc'; qualCode='N';
  } else if(qualification == 'CLEARING'){
    qualDesc = 'degree'; qualCode='M';
    pagename = "/page.html?nd&clearing";
  }  
  if((browseCatId != null && browseCatId != '') || (matchBrowseNode != null && matchBrowseNode != '')){
    var catName = document.getElementById("mobilebrowsecat_url").value;
    finalUrl = document.getElementById("mobilebrowsecat_url").value != "" ? document.getElementById("mobilebrowsecat_url").value : matchBrowseNode;
  }else{
    if(mobileKeywor != 'Enter subject or keyword'){
      if(!searchTextValidate(mobileKeywor)){
        alert('Please enter a valid keyword.');
        $$('mobilebrowsecat').focus();
        return false;
      }
    }
    var navKwd1 = mobileKeywor.split(" ").join("+");
    var navKwd2 = mobileKeywor.split(" ").join("-");
    var url1 = contextPath + "/courses/" + qualDesc + "-courses/" + navKwd2 + "-" + qualDesc + "-courses-" + country2 + "/" + navKwd1;
    var url2 = "/" + qualCode + "/" + country1 + "/" + country1 + "/25/0/0/0/r/0/1/0/uc/0/0"+pagename;
    finalUrl = url1 + url2;   
  }
    document.getElementById("mobilesearchform").action = finalUrl.toLowerCase();
    document.getElementById("mobilesearchform").submit();    
}

function clearBrowseDet(){
  document.getElementById("mobilebrowsecat_hidden").value = "";
  document.getElementById("mobilebrowsecat_id").value = "";
  document.getElementById("mobilebrowsecat_display").value = "";
  document.getElementById("mobilebrowsecat_url").value = "";
  document.getElementById("mobilebrowsecat_alias").value = "";
  document.getElementById("mobilebrowsecat_location").value = "";
  document.getElementById("mobilebrowsecat").value = "";
  document.getElementById("matchbrowsenode").value = "";
}

function resetAajxList(obj){
  var selVal = obj.value;
  var prevQualVal = document.getElementById("selQual").value ;
  if(prevQualVal != selVal){
    $$('mobilebrowsecat').value = 'Enter subject or keyword';
    $$('mobilebrowsecat_hidden').value = "";
    $$('mobilebrowsecat_id').value = "";
    $$('mobilebrowsecat_url').value = "";
    $$('mobilebrowsecat_display').value = "";
    $$('matchbrowsenode').value = "";
  }  
}