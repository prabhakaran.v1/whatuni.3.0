function setSelectedtoSapn(obj, spanid){
    var selectionId = obj.id
    var el = document.getElementById(selectionId);
    var text = el.options[el.selectedIndex].text;
    document.getElementById(spanid).innerHTML = text;
    if(spanid == 'dropsapn_190'){
      var countryOfRes = obj.value;
      if(countryOfRes != null && (countryOfRes == 'United Kingdom' || countryOfRes == 'England')){
        $$('addFinder').style.display= 'block';
      }else{
        $$('addFinder').style.display= 'none';
      }      
    }
}

function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
function replaceURL(inString){
var reg=/\-The-/gi
var specialChar=new Array("(The)","(the)","the-","The ","the ","'","~","!","@","#","$","%","^","&","*","(",")","{","}","[","]","<",">",",",".","?","/","|",";",":","+","=","_","`")
inString=inString.replace(reg," ")
if(specialChar.length>0){
inString=inString.replace(reg," ")
for(charStart=0;charStart<specialChar.length;charStart++){inString=replaceAll(inString,specialChar[charStart],"");}}
return inString}
function replaceHypen(inString){
if(inString !=null){
inString=trimString(inString)
inString=replaceAll(inString,"&","and")
inString=replaceAll(inString," ","-")
inString=replaceAll(inString,"-+","-")
inString=replaceAll(inString,"--","-")}
return inString}
function replaceAll(replacetext,findstring,replacestring){
var strReplaceAll=replacetext;var intIndexOfMatch=strReplaceAll.indexOf(findstring)
while(intIndexOfMatch !=-1){
strReplaceAll=strReplaceAll.replace(findstring,replacestring);intIndexOfMatch=strReplaceAll.indexOf(findstring)}
return strReplaceAll}
function replaceSpecialCharacter(inString){
var outString=inString
var temp=""
if(outString !=null){
var data=outString
for(var i=0;i<data.length;i++){
var character=data.charCodeAt(i)
if(!((character>=48&&character<=57)||(character>=65&&character<=90)||(character>=97&&character<=122)|| character==45)){
temp=temp+" "
}else{
temp=temp+data.substring(i,i+1)}}
outString=temp}
return outString}
function $$D(docid){ return document.getElementById(docid); }
function searchTextValidate(name) {
	var specialChar = "\\,#%;?/_&*!@$^()+={}[]|':<>.~`";   
	if(name.indexOf('"') != -1){return false;}
	for(var i= 0; i<specialChar.length; i++){  
	var str = (name).indexOf(specialChar.charAt(i));
	if(str != -1) {return false;}}return true;
}

function toggle(id, link) {
var d = document.getElementById(id);
if(link.className == 'hd' && (d.style.display == '')){
    
    link.className = 'hd act';
    d.style.display = 'none';
  }else{
     closeOtherDiv();
     var e = document.getElementById(id);
     if (e.style.display == '') {
      e.style.display = 'none';
      link.className = 'hd act';
     } else {
      e.style.display = '';
      link.className = 'hd';
    }
  }
}

function closeOtherDiv(){
  var cusid_ele = document.getElementsByClassName('brw-cat');
  for (var i = 0; i < cusid_ele.length; ++i) {
      var item = cusid_ele[i];  
      item.style.display = 'none';
  }
  for (var x = 1; x <= cusid_ele.length; ++x) {
      if(document.getElementById("tc"+[x])){
      var ele = document.getElementById("tc"+[x]);
      ele.className = 'hd act';
      }      
  }
}

function toggleProfileTabs(id, link, index, profileId, myhcProfileId, buttonlabel,journeyType,tabSize ) {
  var e = document.getElementById(id);
  if (e.style.display == 'block') {
    e.style.display = 'none';
    link.className = 'hd act';
  } else {
      for(var z = 0 ; z < tabSize; ++z){
        if(document.getElementById("tcx"+[z])){
          var elem = document.getElementById("tcx"+[z]);
          elem.className = 'hd act';
        } 
      }  
    var elements = document.getElementsByClassName("brw-cat");
    for (var i = 0; i < elements.length; i++) {
      elements[i].style.display = 'none';
    }  
    e.style.display = 'block';
    link.className = 'hd';
    if(i!=0){
    document.getElementById("jump"+(index-1)).scrollIntoView();
    } else{
      document.getElementById("jump"+(index)).scrollIntoView();
    }    
    showHideProfileTabs(profileId, '', journeyType, profileId, myhcProfileId,'Y');
  }  
  
}

function showHideProfileTabs(currentdiv, flag, journeyType, profileId, myhcProfileId){ //24_JUN_2014
  var profileTabStr = $$D("profileTabString").value;;
  var profileTabs = profileTabStr.split('##SPLITTEXT##');
  var collegeId = $$D("collegeId").value;;
  var cpeQualificationNetworkId = $$D("cpeQualNetworkId").value;  
    for (var lp=0; lp < profileTabs.length; lp++){   
        if(currentdiv == profileTabs[lp]){
            var queryString = "?tabName=" + currentdiv+"&collegeId="+collegeId+"&cpeQualificationNetworkId="+cpeQualificationNetworkId+"&myhcProfileId="+myhcProfileId+"&profileId="+profileId+"&journeyType="+journeyType+"&mobileFlag=Y";
            if(flag!='N'){
              uniStatsLoggingAjax(queryString);
            }  
     }
  }
}

function uniStatsLoggingAjax(queryString){
    var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
    var ajaxURL = "/degrees" + "/unilandingStatsAjax.html" + queryString +'&screenwidth='+deviceWidth;
      var ajax=new sack();
          ajax.requestFile = ajaxURL;
          //ajax.onCompletion = function(){ uniStatsLoggingAjaxResponse(ajax); };
          ajax.runAJAX();
}
