function gradescharacterLimit(obj, limit){
  var textValue = obj.value;
  var textLength = textValue.length;
  if(textLength > limit){
    alert("You can enter only "+limit+" characters");
    obj.value = "";
    obj.focus();
  }
  return false;
}

function clearDefault(obj, text){
   var id = obj.id;
  if($$(id).value == text){
    $$(id).value = "";
	obj.style.color = "#333";
  }

}

function setDefault(obj, text){
  var id = obj.id;
  if($$(id).value == ""){
    $$(id).value = text;
	obj.style.color = "#888";
  }
}

function changeFunc(obj,s2,id) {    
    $$(s2).innerHTML = obj.value + " " +id;
}


function createSelectBox(selectBoxId, count){
  if($$(selectBoxId) != null){
    $$(selectBoxId).innerHTML = "";
    for(var i = 0; i <= count; i++){
      var option = document.createElement("option");
      option.value = ""+i;
      option.appendChild(document.createTextNode(i));
      $$(selectBoxId).appendChild(option);
    }
  }
}


function setMultiSetvalueEnquiryForm(){
  
  var qual = $$('grade_210') != null ? $$('grade_210').value : "";
  var valueArray = {"Alevels":5, "SQAHighers":8, "SQAAdvancedHighers":8};
  var mutisetVal = $$('multi_set') != null ? document.getElementById('multi_set').value : "";
    var astar = mutisetVal != null && mutisetVal.indexOf("A*") > -1 ? mutisetVal.substring(mutisetVal.indexOf("A*>")+3, mutisetVal.indexOf("A*>")+4) : "0";
    var avalue = mutisetVal != null && mutisetVal.indexOf("A") > -1 ? mutisetVal.substring(mutisetVal.indexOf("A>")+2, mutisetVal.indexOf("A>")+3) : "0";
    var bvalue = mutisetVal != null && mutisetVal.indexOf("B") > -1 ? mutisetVal.substring(mutisetVal.indexOf("B")+2, mutisetVal.indexOf("B")+3) : "0";
    var cvalue = mutisetVal != null && mutisetVal.indexOf("C") > -1 ? mutisetVal.substring(mutisetVal.indexOf("C")+2, mutisetVal.indexOf("C")+3) : "0";
    var dvalue = mutisetVal != null && mutisetVal.indexOf("D") > -1 ? mutisetVal.substring(mutisetVal.indexOf("D")+2, mutisetVal.indexOf("D")+3) : "0";
    var evalue = mutisetVal != null && mutisetVal.indexOf("E") > -1 ? mutisetVal.substring(mutisetVal.indexOf("E")+2, mutisetVal.indexOf("E")+3) : "0";
    var freeText = mutisetVal != null && mutisetVal.indexOf("free-text") > -1 ? mutisetVal.substring(mutisetVal.indexOf("<free-text>")+11, mutisetVal.indexOf("</free-text>")) : "";
    if(qual == '77'){
      $$("ucaspoins").value = freeText;
    }else if(qual == '78'){
      $$("bacc").value = freeText;
    }else if(qual == '79'){
      $$("other").value = freeText;
    }
 if(qual == '74'){
    var count = valueArray.Alevels;
    createSelectBox('selBoxA*', count);  
    createSelectBox('selBoxA', count);  
    createSelectBox('selBoxB', count);  
    createSelectBox('selBoxC', count);  
    createSelectBox('selBoxD', count);  
    createSelectBox('selBoxE', count);  

    $$("parentA*").style.display = "block";
    $$("parentA").style.display = "block";
    $$("parentB").style.display = "block";
    $$("parentC").style.display = "block";
    $$("parentD").style.display = "block";
    $$("parentE").style.display = "block";

    $$("parentA*").className ="flft"
    $$("parentA").className = "frgt";
    $$("parentB").className = "flft";
    $$("parentC").className = "frgt";
    $$("parentD").className = "flft";
    $$("parentE").className = "frgt";
    
    
    $$("selBoxA*").value = astar;
    $$("selBoxA").value = avalue;
    $$("selBoxB").value = bvalue;
    $$("selBoxC").value = cvalue;
    $$("selBoxD").value = dvalue;
    $$("selBoxE").value = evalue;
    
    $$("sBoxA*").innerHTML = astar+" A*";
    $$("sBoxA").innerHTML = avalue+" A";
    $$("sBoxB").innerHTML = bvalue+" B";
    $$("sBoxC").innerHTML = cvalue+" C";
    $$("sBoxD").innerHTML = dvalue+" D";
    $$("sBoxE").innerHTML = evalue+" E";
    
    $$("ucaspoins_div").style.display = "none";
    $$("bacc_div").style.display = "none";
    $$("other_div").style.display = "none";
 }else if(qual == '75' || qual == '76'){
    var count = valueArray.SQAHighers;
    createSelectBox('selBoxA', count);  
    createSelectBox('selBoxB', count);  
    createSelectBox('selBoxC', count);  
    createSelectBox('selBoxD', count);  

    $$("parentA*").style.display = "none";
    $$("parentA").style.display = "block";
    $$("parentB").style.display = "block";
    $$("parentC").style.display = "block";
    $$("parentD").style.display = "block";
    $$("parentE").style.display = "none";

    $$("parentA*").className =""
    $$("parentA").className = "flft";
    $$("parentB").className = "frgt";
    $$("parentC").className = "flft";
    $$("parentD").className = "frgt";
    $$("parentE").className = "";
    
    $$("selBoxA").value = avalue;
    $$("selBoxB").value = bvalue;
    $$("selBoxC").value = cvalue;
    $$("selBoxD").value = dvalue;
    
    $$("sBoxA").innerHTML = avalue+" A";
    $$("sBoxB").innerHTML = bvalue+" B";
    $$("sBoxC").innerHTML = cvalue+" C";
    $$("sBoxD").innerHTML = dvalue+" D";

    $$("ucaspoins_div").style.display = "none";
    $$("bacc_div").style.display = "none";
    $$("other_div").style.display = "none";

  }else if(qual == '77' || qual == '78' || qual == '79'){
    $$("parentA*").style.display = "none";
    $$("parentA").style.display = "none";
    $$("parentB").style.display = "none";
    $$("parentC").style.display = "none";
    $$("parentD").style.display = "none";
    $$("parentE").style.display = "none";

    if(qual == '77'){
      $$("ucaspoins_div").style.display = "block";
    }else if(qual == '78'){
      $$("bacc_div").style.display = "block";
    }else if(qual == '79'){
      $$("other_div").style.display = "block";
    }
  }
}


function onchangeQualEnquiryForm(obj){
  var qualification = obj.value;
  var valueArray = {"Alevels":5, "SQAHighers":8, "SQAAdvancedHighers":8};
  if(obj.value == null || obj.value == '' ){

    $$("parentA*").style.display = "none";
    $$("parentA").style.display = "none";
    $$("parentB").style.display = "none";
    $$("parentC").style.display = "none";
    $$("parentD").style.display = "none";
    $$("parentE").style.display = "none";
    $$("ucaspoins_div").style.display = "none";
    $$("bacc_div").style.display = "none";
    $$("other_div").style.display = "none";
  }else if(obj.value == '74'){
    var count = valueArray.Alevels;
    createSelectBox('selBoxA*', count);  
    createSelectBox('selBoxA', count);  
    createSelectBox('selBoxB', count);  
    createSelectBox('selBoxC', count);  
    createSelectBox('selBoxD', count);  
    createSelectBox('selBoxE', count);  

    $$("parentA*").style.display = "block";
    $$("parentA").style.display = "block";
    $$("parentB").style.display = "block";
    $$("parentC").style.display = "block";
    $$("parentD").style.display = "block";
    $$("parentE").style.display = "block";

    $$("parentA*").className ="flft"
    $$("parentA").className = "frgt";
    $$("parentB").className = "flft";
    $$("parentC").className = "frgt";
    $$("parentD").className = "flft";
    $$("parentE").className = "frgt";

    $$("ucaspoins_div").style.display = "none";
    $$("bacc_div").style.display = "none";
    $$("other_div").style.display = "none";

    $$("sBoxA*").innerHTML = "0 A*";
    $$("sBoxA").innerHTML = "0 A";
    $$("sBoxB").innerHTML = "0 B";
    $$("sBoxC").innerHTML = "0 C";
    $$("sBoxD").innerHTML = "0 D";
    $$("sBoxE").innerHTML = "0 E";

  }else if(obj.value == '75' || obj.value == '76'){
    var count = valueArray.SQAHighers;

    createSelectBox('selBoxA', count);  
    createSelectBox('selBoxB', count);  
    createSelectBox('selBoxC', count);  
    createSelectBox('selBoxD', count);  

    $$("parentA*").style.display = "none";
    $$("parentA").style.display = "block";
    $$("parentB").style.display = "block";
    $$("parentC").style.display = "block";
    $$("parentD").style.display = "block";
    $$("parentE").style.display = "none";

    $$("parentA*").className =""
    $$("parentA").className = "flft";
    $$("parentB").className = "frgt";
    $$("parentC").className = "flft";
    $$("parentD").className = "frgt";
    $$("parentE").className = "";

    $$("sBoxA").innerHTML = "0 A";
    $$("sBoxB").innerHTML = "0 B";
    $$("sBoxC").innerHTML = "0 C";
    $$("sBoxD").innerHTML = "0 D";
    
    $$("ucaspoins_div").style.display = "none";
    $$("bacc_div").style.display = "none";
    $$("other_div").style.display = "none";
  }else if(obj.value == '77'){
  
    $$("parentA*").style.display = "none";
    $$("parentA").style.display = "none";
    $$("parentB").style.display = "none";
    $$("parentC").style.display = "none";
    $$("parentD").style.display = "none";
    $$("parentE").style.display = "none";
    
    $$("ucaspoins_div").style.display = "block";
    $$("bacc_div").style.display = "none";
    $$("other_div").style.display = "none";

  }else if(obj.value == '78'){
  
    $$("parentA*").style.display = "none";
    $$("parentA").style.display = "none";
    $$("parentB").style.display = "none";
    $$("parentC").style.display = "none";
    $$("parentD").style.display = "none";
    $$("parentE").style.display = "none";
    
    $$("ucaspoins_div").style.display = "none";
    $$("bacc_div").style.display = "block";
    $$("other_div").style.display = "none";
  }else{
        
    $$("parentA*").style.display = "none";
    $$("parentA").style.display = "none";
    $$("parentB").style.display = "none";
    $$("parentC").style.display = "none";
    $$("parentD").style.display = "none";
    $$("parentE").style.display = "none";
    
    $$("ucaspoins_div").style.display = "none";
    $$("bacc_div").style.display = "none";
    $$("other_div").style.display = "block";
  }
}  


function getEntryGradeMultiSet(){  
  window.onbeforeunload = null;
  var qual = document.getElementById('grade_210').value;
  var astarvalue = document.getElementById('selBoxA*').value;
  var avalue = document.getElementById('selBoxA').value;
  var bvalue = document.getElementById('selBoxB').value;
  var cvalue = document.getElementById('selBoxC').value;
  var dvalue = document.getElementById('selBoxD').value;
  var evalue = document.getElementById('selBoxE').value;
  var ucaspoin = document.getElementById('ucaspoins').value;
  var bacc = document.getElementById('bacc').value;
  var other = document.getElementById('other').value;
  var dontAllowGrades = "";
  if(document.getElementById('dontgrade')){
    if(document.getElementById('dontgrade').checked){
      dontAllowGrades = "Y"
    }else{
      dontAllowGrades = "N";
    }
  }
  var values = "";
  var finalValue = "";
  if(dontAllowGrades == 'Y'){
    document.getElementById('multi_set').value = finalValue;
    document.getElementById('grade_210').value = "";
  }else{
    if(qual == '74'){
      values += astarvalue!=null && astarvalue!= 'undefined' && astarvalue!= '0' ? "<A*>"+ astarvalue + '</A*>' : "";
      values += avalue!=null && avalue!= 'undefined' && avalue!= '0' ? "<A>"+ avalue + '</A>' : "";
      values += bvalue!=null && bvalue!= 'undefined' && bvalue!= '0' ? "<B>"+ bvalue + '</B>' : "";
      values += cvalue!=null && cvalue!= 'undefined' && cvalue!= '0' ? "<C>"+ cvalue + '</C>' : "";
      values += dvalue!=null && dvalue!= 'undefined' && dvalue!= '0' ? "<D>"+ dvalue + '</D>' : "";
      values += evalue!=null && evalue!= 'undefined' && evalue!= '0' ? "<E>"+ evalue + '</E>' : "";
      
    }else if(qual == '75' || qual == '76'){
      values += avalue!=null && avalue!= 'undefined' && avalue!= '0' ? "<A>"+ avalue + '</A>' : "";
      values += bvalue!=null && bvalue!= 'undefined' && bvalue!= '0' ? "<B>"+ bvalue + '</B>' : "";
      values += cvalue!=null && cvalue!= 'undefined' && cvalue!= '0' ? "<C>"+ cvalue + '</C>' : "";
      values += dvalue!=null && dvalue!= 'undefined' && dvalue!= '0' ? "<D>"+ dvalue + '</D>' : "";
    }else if(qual == '77'){
      values += ucaspoin!=null && ucaspoin!= 'undefined' && ucaspoin!= "" && ucaspoin!= 'Enter tariff points'? "<free-text>"+ ucaspoin +"</free-text>" : "";
    }else if(qual == '78'){
      values += bacc!=null && bacc!= 'undefined' && bacc!= "" && bacc!= 'Enter score' ? "<free-text>"+ bacc +"</free-text>" : "";
    }else if(qual == '79'){
      values += other!=null && other!= 'undefined' && other!= "" && other!= 'Please give details' ? "<free-text>"+ other +"</free-text>" : "";
    }
    if(values != ""){
      finalValue = "<entry-grades>"+values+"</entry-grades>"
    }
    document.getElementById('multi_set').value = finalValue;
    }
    return true;
}

function validateInteractionFormAndLogOmniture(obj,whichInteractionForm, collegeName, cpeValue){	
	var validationFlag = false;
	if(whichInteractionForm != null && whichInteractionForm == 'COLLEGE_EMAIL'){
		validationFlag = validateEmailForm();
                var msg = $$('umessage').value;
                if(msg.indexOf('\[url=') != -1 || msg.indexOf('/link') != -1){
                    alert("Sorry your email contains undeliverable content.  Please make sure your message contains no urls, advertising or offensive language before sending.");
                    $$('sendEmail').style.display='block'; 
                    return false;
                }
	}else if(whichInteractionForm != null && whichInteractionForm == 'COLLEGE_PROSPECTUS'){
		validationFlag = validateProspectusForm();
	}
	if((whichInteractionForm != null && whichInteractionForm == 'COLLEGE_EMAIL')){
    if(validationFlag){
      _gaq.push(['_set','hitCallback',function() {document.forms['qlformbean'].submit();}]);
      GAInteractionEventTracking('emailsubmit', 'interaction', 'Email', collegeName, cpeValue);
    }else{
      document.forms['qlformbean'].submit();
    }
	}else if(whichInteractionForm != null && whichInteractionForm == 'COLLEGE_PROSPECTUS'){
    if(validationFlag){
      _gaq.push(['_set','hitCallback',function() {document.forms['qlformbean'].submit();}]);
      GAInteractionEventTracking('prospectussubmit', 'interaction', 'Prospectus', collegeName, cpeValue);
    }else{ 
      document.forms['qlformbean'].submit();
    }
  }
  return true;  
}

function validateEmailForm(){
	var email = $$('email').value;
	var fName = $$('fname').value;
	var lName = $$('lname').value;
  var yearOfEntry = $$('year_to_join').value;
  if ($$('termsc') != null){
   var tacFlag = $$('termsc').value;
  }
  var returnflag = true;	
  if(fName == null || trimString(fName) == '' || fName == 'First name'){
    returnflag = false;
  }                
  if(lName == null || trimString(lName) == '' || lName == 'Last name'){
    returnflag = false;
  }                
  if(email != null && trimString(email) != ''){
    if(!isValidEmail(email)){
      returnflag = false;
    }
  }else{
    returnflag = false;
  }
  if(yearOfEntry == null || trimString(yearOfEntry) == ''){
    returnflag = false;
  }                

 if ($$('termsc') != null){
	 if (!$$("termsc").checked){returnflag = false;}
 }
  if(returnflag){			
      return true;
  } else {			
      return false ;
  }	
}

function isValidEmail(email){
	if(email.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) == -1){
		return false;
	}
	return true;
}

function validateProspectusForm(){
	var fName = $$('fname').value;
	var lName = $$('lname').value;
	var email = $$('email').value;  
  var yearOfEntry = $$('year_to_join').value;
	var postcode = $$('postcode').value;
	var addrLine1 = $$('address1').value;
	var addrLine1 = $$('address2').value;
	var town = $$('town').value;
	var nationality = $$('nationality1').value;
	var country = $$('courntryofres').value;
  if ($$('termsc') != null){
   var tacFlag = $$('termsc').value;	
  }
      var errors = "";
      var returnflag = true;		 
      if(fName == null || trimString(fName) == '' || fName == 'First name'){
        returnflag = false;
      }                
      if(lName == null || trimString(lName) == '' || lName == 'Last name'){
        returnflag = false; 
      }                
      if(email != null && trimString(email) != ''){
        if(!isValidEmail(email)){
          returnflag = false;
        }
      }else{
        returnflag = false;
      }
      if(yearOfEntry == null || trimString(yearOfEntry) == ''){
        returnflag = false; 
      }   
      if(nationality == null || trimString(nationality) == ''){
        returnflag = false; 
      }   
      if(country == null || trimString(country) == ''){
        returnflag = false; 
      }   
      
      if(postcode == null || trimString(postcode) == '' || trimString(postcode) == 'Enter your UK postcode'){
        returnflag = false;
      }
      if(addrLine1 == null || trimString(addrLine1) == '' || trimString(addrLine1) == 'Address line 1'){
        returnflag = false;
      }
      if(addrLine1 == null || trimString(addrLine1) == '' || trimString(addrLine1) == 'Address line 2'){
        returnflag = false;
      }
      if(town == null || trimString(town) == '' || trimString(town) == 'Town'){
        returnflag = false;
      }		
       if ($$('termsc') != null){
        if(!$$("termsc").checked){
          returnflag = false;
        }	
      }        
      if(returnflag){			
              return true;
      } else {			
              $$('getProspectus').style.display = 'block';
              return false ;
      }		
}

function hideSubmitButton(id){
  $$(id).style.display='none'; 
}
