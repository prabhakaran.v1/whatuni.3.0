var Chart = function (s) {
    function x(a, c, b, e) {
        function h() {
            g += f;
            var k = a.animation ? A(d(g), null, 0) : 1;
            e.clearRect(0, 0, q, u);
            a.scaleOverlay ? (b(k), c()) : (c(), b(k));
            if (1 >= g) D(h);
            else if ("function" == typeof a.onAnimationComplete) a.onAnimationComplete()
        }
        var f = a.animation ? 1 / A(a.animationSteps, Number.MAX_VALUE, 1) : 1,
            d = B[a.animationEasing],
            g = a.animation ? 0 : 1;
        "function" !== typeof c && (c = function () {});
        D(h)
    }

    function A(a, c, b) {
        return !isNaN(parseFloat(c)) && isFinite(c) && a > c ? c : !isNaN(parseFloat(b)) &&
            isFinite(b) && a < b ? b : a
    }
    var r = this,
        B = {
            easeOutBounce: function (a) {
                return (a /= 1) < 1 / 2.75 ? 1 * 7.5625 * a * a : a < 2 / 2.75 ? 1 * (7.5625 * (a -= 1.5 / 2.75) * a + 0.75) : a < 2.5 / 2.75 ? 1 * (7.5625 * (a -= 2.25 / 2.75) * a + 0.9375) : 1 * (7.5625 * (a -= 2.625 / 2.75) * a + 0.984375)
            }
        }, q = s.canvas.width,
        u = s.canvas.height;
    window.devicePixelRatio && (s.canvas.style.width = q + "px", s.canvas.style.height = u + "px", s.canvas.height = u * window.devicePixelRatio, s.canvas.width = q * window.devicePixelRatio, s.scale(window.devicePixelRatio, window.devicePixelRatio));

    this.Pie = function (a,
        c) {
        r.Pie.defaults = {
            segmentShowStroke: !0,
            segmentStrokeColor: "#fff",
            segmentStrokeWidth: 2,
            animation: !0,
            animationSteps: 100,
            animationEasing: "easeOutBounce",
            animateRotate: !0,
            animateScale: !1,
            onAnimationComplete: null
        };
        var b = c ? y(r.Pie.defaults, c) : r.Pie.defaults;
        return new I(a, b, s)
    };
    this.Doughnut = function (a, c) {
        r.Doughnut.defaults = {
            segmentShowStroke: !0,
            segmentStrokeColor: "#fff",
            segmentStrokeWidth: 2,
            percentageInnerCutout: 50,
            animation: !0,
            animationSteps: 100,
            animationEasing: "easeOutBounce",
            animateRotate: !0,
            animateScale: !1,
            onAnimationComplete: null
        };
        var b = c ? y(r.Doughnut.defaults, c) : r.Doughnut.defaults;
        return new J(a, b, s)
    };
    var I = function (a, c, b) {
        for (var e = 0, h = Math.min.apply(Math, [u / 2, q / 2]) - 5, f = 0; f < a.length; f++) e += a[f].value;
        x(c, null, function (d) {
            var g = -Math.PI / 2,
                f = 1,
                j = 1;
            c.animation && (c.animateScale && (f = d), c.animateRotate && (j = d));
            for (d = 0; d < a.length; d++) {
                var l = j * a[d].value / e * 2 * Math.PI;
                b.beginPath();
                b.arc(q / 2, u / 2, f * h, g, g + l);
                b.lineTo(q / 2, u / 2);
                b.closePath();
                b.fillStyle = a[d].color;
                b.fill();
                c.segmentShowStroke && (b.lineWidth =
                    c.segmentStrokeWidth, b.strokeStyle = c.segmentStrokeColor, b.stroke());
                g += l
            }
        }, b)
    }, J = function (a, c, b) {
            for (var e = 0, h = Math.min.apply(Math, [u / 2, q / 2]) - 5, f = h * (c.percentageInnerCutout / 100), d = 0; d < a.length; d++) e += a[d].value;
            x(c, null, function (d) {
                var k = -Math.PI / 2,
                    j = 1,
                    l = 1;
                c.animation && (c.animateScale && (j = d), c.animateRotate && (l = d));
                for (d = 0; d < a.length; d++) {
                    var m = l * a[d].value / e * 2 * Math.PI;
                    b.beginPath();
                    b.arc(q / 2, u / 2, j * h, k, k + m, !1);
                    b.arc(q / 2, u / 2, j * f, k + m, k, !0);
                    b.closePath();
                    b.fillStyle = a[d].color;
                    b.fill();
                    c.segmentShowStroke &&
                        (b.lineWidth = c.segmentStrokeWidth, b.strokeStyle = c.segmentStrokeColor, b.stroke());
                    k += m
                }
            }, b)
        }, D = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (a) {
            window.setTimeout(a, 1E3 / 60)
        }, F = {}
};