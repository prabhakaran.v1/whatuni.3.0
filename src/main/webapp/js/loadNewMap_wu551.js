function lboxNewMapInitialize(latitude, longtitude){            
    var myLatlng = new google.maps.LatLng(latitude, longtitude);                  
     var myOptions = {
        zoom: 18,
        center: new google.maps.LatLng(latitude,longtitude),
       scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        sensor:false,
        styles:[{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#e0efef"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"hue":"#1900ff"},{"color":"#c0e8e8"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill"},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":100},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","stylers":[{"color":"#7dcdcd"}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"visibility":"on"},{"lightness":700}]}]
    };
    var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";//Added wu_scheme by Indumathi.S Mar-29-2016
    var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);  
     var iconBase = wu_scheme +document.domain+ '/wu-cont/images/map-img.png';   //
      var point = new google.maps.LatLng(latitude,longtitude); 
      var marker=new google.maps.Marker({
      position:myLatlng,
      icon:iconBase,
      map:map
      });     
    marker.setMap(map); 
    google.maps.event.trigger(map, "resize");   
    createMarkerNewMapPage(marker,map);   
}

//
function createMarkerNewMapPage(marker, map){
  var contentString = document.getElementById("locationinfoMaphh").innerHTML;
  google.maps.event.addListener(marker, 'click', function() {
    infowindow = new google.maps.InfoWindow({
      content: contentString
  });
    infowindow.open(map,marker);
  });
}