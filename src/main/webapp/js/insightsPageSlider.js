function articleLoader(){
   var jq = jQuery.noConflict();
   jQuery.noConflict();
     
		   jq(window).load(function() {
						  jq('#f_true').flexslider({
						  animation: "slide",
          slideshow:false,
          animationSpeed: 600,
          animationLoop: false,
          slideshowSpeed : 4000,
          itemWidth: 400,
          itemMargin: -1,
          video: false,
          useCSS: false,
          pauseOnAction:true,
          minItems: getWuGridSize(),
          maxItems: getWuGridSize(),
						  start: function(slider){
           flexslider  = slider; 
								   jq('div.ltstvds').mouseover(function(){
									   slider.pause();
								   });
								   jq('div.ltstvds').mouseout(function() {
									   slider.resume();
								   });
          jq('.clone').each(function (){
             
             var ids = jq(this).find("img").map(function(){
             return jq(this).attr('id')}).get(); 
             for (var i=0; i<ids.length; i++) {
                var art_src = jq("#"+ids[i]).attr("data-src");
                jq('#f_true').find(".clone").find("#"+ids[i]).attr("src",art_src).removeAttr('id').removeAttr("data-src");
             }
            for(var nxtSlide=(slider.currentSlide*3)+1;nxtSlide<=3;nxtSlide++){
            var src = jq("#img"+nxtSlide).attr("data-src");
            jq("#img"+nxtSlide).attr("src",src);
            }
           });
								  },
         after: function (slider) {
            //instead of going over every single slide, we will just load the next immediate slide
            var nextslide = slider.currentSlide+1;           
            //
            if(nextslide!=1){
               for(var nxtSlide=Math.pow(2,nextslide);nxtSlide<nextslide*3;nxtSlide++){
                  jq("#img"+nxtSlide).each(function () {
                    var src = jq("#img"+nxtSlide).attr("data-src");
                     jq("#img"+nxtSlide).attr("src",src).removeAttr("data-src");
                  });
              }
            }
            //
          }
									});
						   jq('.flexslider').flexslider({
						      animation: "slide"
									 });
						  });			
}
function getWuGridSize() {
  var dev2 = jQuery.noConflict();
  var gridSize = (dev2(window).width() <= 480) ? 1 : (dev2(window).width() <= 992) ? 2 : 3;
  return gridSize;
	}