var hh=0;
var contextPath = "/degrees";
var emailexistmsg="It looks like you're already a Whatuni member, just enter your password to complete this action."
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
var cnt = 1;
function addBasket(assocId, assocType, thisvalue, divId, popDivId, incompare, vwlb, collegeName){
//Added collegeName param in all places when it call add comparision by Prabha on 29_Mar_2016
  var dowhat="";
  var loadId = "";
  var popId = '';
  var fromVisitWeb = "";
  var defaultpop = '';
  if(vwlb=="fromlighbox"){
     var element = document.getElementById(thisvalue);
     if(element.className == "cmlst"){
        dowhat = "add";
        loadId = "load_"+assocId;
      }else if(element.className == "cmlst act"){
        dowhat = "remove";
        loadId = "load1_"+assocId;
      }
  }else{
    if(thisvalue.className == ""){
      dowhat = "add";
      loadId = "load_"+assocId;
    }else if(thisvalue.className == "act"){
      dowhat = "remove";
      loadId = "load1_"+assocId;
    }else if (thisvalue.className == "fclose"){
      dowhat = "remove"
    }else if(thisvalue=="visitweb"){//Added for 11-AUG-2015_REL
      dowhat = "add";
      fromVisitWeb = "vweb";
    }
    if(thisvalue.className == "btn4 add"){
      dowhat = "add";
      loadId = "load_"+assocId;
    }else if(thisvalue.className == "btn4 add act"){
      dowhat = "remove";
      loadId = "load1_"+assocId;
    }
  }  
  var assocId = assocId;
  popId = "pop_"+assocId;
  defaultpop = "defaultpop_"+assocId;
  //Added wu_scheme by Indumathi.S Mar-29-2016
  var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";
  var loadImg = '<img src="'+wu_scheme+document.domain+'/wu-cont/images/ldr.gif"/>';
  if($$(loadId)){
    $$(loadId).innerHTML = loadImg;
    $$(loadId).style.display = 'block';
  }
  var fromFlag = "";
  if(incompare=='Y'){
    fromFlag = "comp";
  }
  var comparepage = "NORMAL";
  if($$("comparepage")){
    comparepage = $$("comparepage").value;
  }
  var req;
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  var url=contextPath+'/addbasket.html?assocId='+assocId+'&fromAjax=true&dowhat='+dowhat+"&assocType="+assocType+"&comppage="+comparepage+"&fromFlag="+fromFlag+'&screenwidth='+deviceWidth;
  if(typeof XMLHttpRequest !="undefined"){req=new XMLHttpRequest();}
  else if(window.ActiveXObject){req=new ActiveXObject("Microsoft.XMLHTTP");}
  req.open("POST",url,true);req.onreadystatechange=handlerequest;req.send(url)
  function handlerequest(){
    if(req.readyState==4){
      if(req.status==200){
        var resp=req.responseText;
        if((resp).indexOf("SESSION_EXPIRED") > -1) {
          var loginUrl = resp.split("##")[1];
          window.open(loginUrl, "_self");
          return;
        }
        var dataarray=resp.split("BREAKBREAK");
        var dataArrayLength = dataarray.length;
        if(dataArrayLength > 2){
            $$("s_count").innerHTML = dataarray[3];
            $$("s_count_drop_down").innerHTML = dataarray[3];
            if($$D("s_count_nav")){$$D("s_count_nav").innerHTML = dataarray[3];}
            if($$(loadId)){
              $$(loadId).style.display = 'none';
            }
            if(dowhat == "add"){
              if($$(divId)){$$(divId).style.display = 'none';}
              if($$(popDivId)){$$(popDivId).style.display = 'block';}
              if($$(popId)){
                if(trimString(dataarray[0])!=''){
                  $$(popId).innerHTML=dataarray[0];
                  if($$(defaultpop)){$$(defaultpop).style.display = 'none';}
                  $$(popId).style.display = 'block';                  
                }
              }              
             if(vwlb=="fromlighbox"){
              if($$("lb_"+divId)){
                  if($$("lb_"+divId)){blockNone(("lb_"+divId),'block');}
                  if($$("lb_"+popDivId)){blockNone(("lb_"+popDivId),'none');}
                   if($$("compStatusTextLb")){
                    $$("compStatusTextLb").innerHTML = "ADDED";
                    $$("compStatusimageLb").className = "fa fa-check"; 
                    $$("compStatustxtColourLb").className = "grntxt"; 
                   }
                }
             }
              if($$("currentcompare")){
                if($$("currentcompare").value == "" || $$("currentcompare").value == null){
                  $$("currentcompare").value = popId;
                }else{                  
                  $$("currentcompare").value = popId;
                }
              }              
              //Added for basket insights log by Prabha on 29_Mar_2016_Rel
             if(dowhat == "add" && collegeName != ""){
               comparisionAnalyticsEventsLogging(collegeName);
             }
  //End of basket insights code
            }else if(dowhat == "remove"){
              if($$(divId)){$$(divId).style.display = 'block';}
              if($$(popDivId)){$$(popDivId).style.display = 'none';}
              if($$(popId)){
                $$(popId).innerHTML="";
              }
              if(vwlb=="fromlighbox"){
                if($$("lb_"+popDivId)){
                    if($$("lb_"+divId)){blockNone(("lb_"+divId),'none');}
                    if($$("lb_"+popDivId)){blockNone(("lb_"+popDivId),'block');}
                    $$("compStatusTextLb").innerHTML = "REMOVED";
                    $$("compStatusimageLb").className = "fa fa-times"; 
                    $$("compStatustxtColourLb").className = "grntxt grntxt_rem"; 
                }
              } 
              if($$("currentcompare")){
                $$("currentcompare").value = "";
              }                            
              if($$("srcompcnt1")){
                $$("srcompcnt1").innerHTML = "["+dataarray[3]+"] ";
              }              
            }
        }else{
          if(thisvalue!="visitweb" && vwlb!="fromlighbox"){//Added for 11-AUG-2015_REL
              if($$(loadId)){
              $$(loadId).style.display = 'none';
            }
            showLightBoxLoginForm('grade-range-popup',650,500, 'COMAPRE-BASKET');
            return false;           
          }
         }             
        var currbasketId = "";
        var selectedbasketid = "";
        if(incompare=='Y'){
          loadtopbasket();
          resizeCells();
          var podName = (assocType == 'C') ? 'UNI_POD' : 'COURSE_POD';
          onScrollSuggestedPod(podName);
         }        
        if(thisvalue=="visitweb"){//Added for 11-AUG-2015_REL        
          if(dataarray[2]!="ALREADY_ADDED"){   
          var showFlag = "";
           if($$("dontShowCompSplashHidden")){
           showFlag = $$("dontShowCompSplashHidden").value;
           }
           if(showFlag==""){
            clearTimeout(timersLB); 
            setTimeout(function() {showLightboxVW(assocId,assocType) },4000);}
            return true;
         }
        }        
      }
     }
    }
  }
//Method for add comparision insights log :: Prabha on 29_Mar_2016
function comparisionAnalyticsEventsLogging(inVal){
  var collegNames = inVal.split("##SPLIT##"); //##SPLIT## - GlobalConstants.SPLIT_CONSTANT
  for(var lp = 0; lp < collegNames.length; lp++){
    ga('create', 'UA-52581773-4', 'auto', {'name': 'newTracker'});
    ga('newTracker.send', 'event', 'view comparison' , 'clicked' , collegNames[lp]);
  }
}
//This method is to show comparison content Lightbox  on click of visit website
function showLightboxVW(assocId,assocType){
 var comparepage = "NORMAL";
  if($$("comparepage")){
    comparepage = $$("comparepage").value;
  }
  var url = contextPath +"/showbasketcontentajax.html?assocId="+assocId+"&assocType="+assocType+"&compareType="+comparepage;
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){ showLightboxVWresp(ajax); };
  ajax.runAJAX();
}
function showLightboxVWresp(ajax){
  var resp=trimString(ajax.response);
  if(resp!="" && resp!=null){
     sm('newvenue', 650, 500, 'vwCompbox');	
     document.getElementById("newvenueform").innerHTML=ajax.response; 
     document.getElementById("ajax-div").style.display="none";
     document.getElementById("newvenueform").style.display="block";
  }
}
//This method is to set the Dont show again comparison lightbox
function dontShowCompLB(){
  setSessionCookie('DS_WC_TO_COM','TRUE');//DONT_SHOW_WEB_CLICK_TO_COMPARISON
  var url = contextPath +"/setcomplbsessionattrajax.html";
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){dontShowCompLBresp(ajax); };
  ajax.runAJAX();
}
function dontShowCompLBresp(ajax){
  if($$("dontShowCompSplashHidden")){
    $$("dontShowCompSplashHidden").value="TRUE";
  }
  hm();
}