//24-JUN-2014
var contextPath = "/degrees";
function $$d(id){
  return document.getElementById(id);
}
function fetchClearingSubjectData(obj, spanid) {
  var el = document.getElementById(obj.id);
  var text = el.options[el.selectedIndex].text;
  var value = el.options[el.selectedIndex].value;    
  document.getElementById(spanid).innerHTML = text;  
  var ajax = new sack();
  url = contextPath + "/popularClearingSubjectAjax.html?subjectId="+value;
  ajax.requestFile = url;
  ajax.onCompletion = function(){ loadClearingSubjectData(ajax, value, text, spanid); };
  ajax.runAJAX();          
}     
function loadClearingSubjectData(ajax, value, text, spanid){
  document.getElementById("clTips").innerHTML = "";
  var response = ajax.response;  
  document.getElementById("clTips").innerHTML = response;
  if(document.getElementById('selectClearingSubjects')){
    document.getElementById('selectClearingSubjects').value = value;
    document.getElementById(spanid).innerHTML = text;    
  }  
  if(document.getElementById("subjectHeading")){
    document.getElementById("subjectHeading").innerHTML = text;
  }
  lazyloadetStarts();
}
var adv = jQuery.noConflict();
adv(document).ready(function(){      
  lazyloadetStarts();   
});
function getAnswer(index){
  if($$d("answer_"+index).style.display == "none"){
    $$d("answer_"+index).style.display = "block";
    $$d("icon_"+index).className = "fa fa-minus-circle";
  }
  else if($$d("answer_"+index).style.display == "block"){
    $$d("answer_"+index).style.display = "none";
    $$d("icon_"+index).className = "fa fa-plus-circle";
  }
}
//
adv(document).ready(function(){ 
  history.pushState(null, null, document.URL);
  window.addEventListener('popstate', function () {
    //var pageurl = jq('#refererUrl').val();
    var pageurl = document.URL;
    document.location.href = pageurl;
    //history.pushState(null, null, pageurl);
  });
});
//