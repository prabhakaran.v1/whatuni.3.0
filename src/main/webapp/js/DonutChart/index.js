function drawCddoughnutchart(index,currentElement,colour){
var $dchart = jQuery.noConflict();
var limit1 = document.getElementById(index).value;
limit1 = parseInt(limit1);
limit2 = 100 - limit1;
limit2 = parseInt(limit2);
$dchart("#" + currentElement).drawDoughnutChart([
  {  
    value : limit1,  
    color: colour
  },
  { 
    value:  limit2,   
    color: "#dededf" 
  }
]);

}

function drawCdAssesseddoughnutchart(value1,value2,value3,currentElement){
var $dchart = jQuery.noConflict();
var limit1  = parseInt(value1);
var limit2  = parseInt(value2);
var limit3  = parseInt(value3);

$dchart("#" + currentElement).drawDoughnutChart([
  {  
    value : limit1,  
    color: "#fc7169",
    prcct: "0"
  },
  { 
    value:  limit2,   
    color: "#a65266",
    prcct: "0"
  },
  {
    value:  limit3,
    color: "#edc756" ,
    prcct: "0"
  }
]);

}
