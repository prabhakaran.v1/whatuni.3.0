function drawSubjectdoughnutchart(index,currentElement,colour){
var limit1 = document.getElementById(index).value;
var adv = jQuery.noConflict();
limit1 = parseFloat(limit1);
limit2 = 100 - limit1;
if(limit2 < 1){
limit2 = 1;
}
limit2 = parseInt(limit2);
adv("#" + currentElement).drawDoughnutChart([
  {  
    value : limit1,  
    color: colour
  },
  { 
    value:  limit2,   
    color: "#dededf" 
  }
]);

}