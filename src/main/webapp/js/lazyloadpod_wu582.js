//Added for 1-sep-2015 release by priyaa - lazy load pod
var context_path = "/degrees";
var jj =  jQuery.noConflict();
(function(jj){
    /**
     * Copyright 2012, Digital Fusion
     * Licensed under the MIT license.
     * http://teamdf.com/jquery-plugins/license/
     *
     * @author Sam Sehnert
     * @desc A small plugin that checks whether elements are within
     *       the user visible viewport of a web browser.
     *       only accounts for vertical position, not horizontal.
     */
    var $w = jj(window);
    jj.fn.visible = function(partial,hidden,direction){
        if (this.length < 1)
            return;
        var $t        = this.length > 1 ? this.eq(0) : this,
            t         = $t.get(0),
            vpWidth   = $w.width(),
            vpHeight  = $w.height(),
            direction = (direction) ? direction : 'both',
            clientSize = hidden === true ? t.offsetWidth * t.offsetHeight : true;
        if (typeof t.getBoundingClientRect === 'function'){
            // Use this native browser method, if available.
            var rec = t.getBoundingClientRect(),
                tViz = rec.top    >= 0 && rec.top    <  vpHeight,
                bViz = rec.bottom >  0 && rec.bottom <= vpHeight,
                lViz = rec.left   >= 0 && rec.left   <  vpWidth,
                rViz = rec.right  >  0 && rec.right  <= vpWidth,
                vVisible   = partial ? tViz || bViz : tViz && bViz,
                hVisible   = partial ? lViz || rViz : lViz && rViz;
            if(direction === 'both')
                return clientSize && vVisible && hVisible;
            else if(direction === 'vertical')
                return clientSize && vVisible;
            else if(direction === 'horizontal')
                return clientSize && hVisible;
        } else {
            var viewTop         = $w.scrollTop(),
                viewBottom      = viewTop + vpHeight,
                viewLeft        = $w.scrollLeft(),
                viewRight       = viewLeft + vpWidth,
                offset          = $t.offset(),
                _top            = offset.top,
                _bottom         = _top + $t.height(),
                _left           = offset.left,
                _right          = _left + $t.width(),
                compareTop      = partial === true ? _bottom : _top,
                compareBottom   = partial === true ? _top : _bottom,
                compareLeft     = partial === true ? _right : _left,
                compareRight    = partial === true ? _left : _right;
            if(direction === 'both')
                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop)) && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
            else if(direction === 'vertical')
                return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop));
            else if(direction === 'horizontal')
                return !!clientSize && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
        }
    };
})(jQuery);
function loadLazyLoadPod(id, cont, functionName) {
 if (elementExistsInViewPort(id,cont) == true && window[id+"_lazy"] == undefined) {      
       window[id+"_lazy"] = true;
       eval(functionName);
  }
}
var adv = jQuery.noConflict();
function elementExistsInViewPort(id, cond) {
  return adv("#"+id).visible(cond);
}
function loadNewLetterPodOnScroll(){
  var url=context_path+"/loadAdviceAjaxPods.html";
  var ajax=new sack();
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ loadAdvicePagePodOnScrollAjax(ajax); };	
   ajax.runAJAX();
}
//Method for lazyload trending and most info ajax call by Prabha on 03_NOV_2015_REL
function loadTrendingAndMostPopularPod(podName, podPositionName){
  var url=context_path+"/loadAdviceAjaxPods.html?primaryCategory="+jj('#primaryCategory').val()+"&articleId="+jj('#articleId').val();
  url += "&podName="+podName+"&podposition="+podPositionName+"&parentCategory="+jj('#parentCategory').val()+"&secondaryCategory="+jj('#secondaryCategory').val(); 
  var ajax=new sack();
  ajax.requestFile = url;	
  ajax.onCompletion = function(){ loadAdvicePagePodOnScrollAjax(ajax, podName, podPositionName); };	
  ajax.runAJAX();
}
//Method for response writing in a div by Prabha on 03_NOV_2015_REL 
function loadAdvicePagePodOnScrollAjax(ajax, podName, podPositionName){
  if(podName == "TRENDING"){
    jj('#trendingPodDiv').html(ajax.response);
  }else if(podName == "MOST"){
    if(podPositionName == "CONTENT_PAGE_MOST_POPULAR_POD_BOTTOM"){     
      var podTitle = "Most popular";
      var fStr = '<h2 id="podHeading">Top articles for you</h2>';
      var sStr = '<h2 id="podHeading">'+podTitle+'</h2>';
      var response = ajax.response.replace(fStr, sStr);
      jj('#mostArticlesPodButtom').html(response);
      jj("#articlesPodLoading").hide();
    }else{
      jj('#MostPulrPodDiv').html(ajax.response);
    }    
  }else{    
    jj('#spamboxDiv').html(ajax.response);
    dynamicLoadJS(document.getElementById("contextJsPath").value+'/js/'+document.getElementById("facebookLoginJSName").value);
    /* Commented to fix bug in email ajax input in dont miss out pod by Sangeeth.S for Oct_23_18 rel
    var $mdv1 = jQuery.noConflict();
    $mdv1.getScript(document.getElementById("contextJsPath").value+"/js/emaildomain/"+document.getElementById("emailDomainJsName").value, function(){        
      var arr = ["spmEmailAddress", "", ""];
      $mdv1("#spmEmailAddress").autoEmail(arr);        
    });*/
  }
}

