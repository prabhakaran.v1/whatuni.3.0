var $jq = jQuery.noConflict();
var vidpct = ['','10%','20%','30%','40%','50%','60%','70%','80%','90%','100%'];
var videoRanMap = {};
var deviceWidth =  null;
function getDeviceWidth() { return (window.innerWidth > 0) ? window.innerWidth : screen.width; }
//
function $$D(id){
  return document.getElementById(id);
}

function clickPlayAndPause(videoId, e) {
  var videoIdElement = $jq('#'+ videoId).get(0);
  var vidId = $jq('#'+ videoId);
  if(videoIdElement.paused){
	$jq("#featured_image").hide();
	$jq(".cl_vcnr").hide();
	vidId.show();
	e.preventDefault();
	videoIdElement.play();
	vidId.attr('controls', '');
	videoDurationPercentage(videoIdElement);
  }	else {
	vidId.hide();
	$jq("#featured_image").show();
	$jq(".cl_vcnr").show();	
	e.preventDefault();
	videoIdElement.pause(); 
	videoDurationPercentage(videoIdElement);
  }
}
$jq(document).ready(function() {
  //retina();
  onLoadVideoCall();
});
function onLoadVideoCall() {
 var videoId = $jq('#featured_video');
 var vidId = $$D("featured_video");
 var collegeId = $jq('#collegeId').val();
 var networkId = $jq('#networkId').val();
 var videoIdElement = videoId.get(0);
 if(videoId.length) {
   if(videoId.hasClass('autoPlay')) {
	 vidId.muted = true; 	
	 $jq("#featured_image").hide();
	 $jq(".cl_vcnr").hide();
	 videoId.show();
	 videoIdElement.play();
	 videoId.attr('controls', '');
	 featuredDbStatsLogging(collegeId, networkId, "FEATUREDVIDEO");
	 videoDurationPercentage(videoIdElement);
   } 
   else if(videoId.hasClass('fiveSecPlay')) {
	 setTimeout(function() {
	  vidId.muted = true;  
	  $jq("#featured_image").hide();
	  $jq(".cl_vcnr").hide();
	  videoId.show();
	  videoIdElement.play();
	  videoId.attr('controls', '');
	  featuredDbStatsLogging(collegeId, networkId, "FEATUREDVIDEO");
	  videoDurationPercentage(videoIdElement);
	 }, 5000); 
   }	 	
  }
}
function videoDurationPercentage(videoElement) {
  if(videoRanMap[videoElement.id+'_percentage'] == null) {
	videoRanMap[videoElement.id+'_percentage'] = "0%";
  }
  if(videoRanMap[videoElement.id+'_imp5sec'] == null) {
	videoRanMap[videoElement.id+'_imp5sec'] = "N";
  }
  if(videoRanMap[videoElement.id+'_complete'] == null) {
	videoRanMap[videoElement.id+'_complete'] = "N";
  }
  videoElement.ontimeupdate = function(){
  var curVidDrtn = videoElement.duration;
  var tenPcntSec = curVidDrtn/10;
  //
  if(videoRanMap[videoElement.id+'_imp5sec'] != "Y") {
	videoRanMap[videoElement.id+'_imp5sec'] = "Y";
    GANewAnalyticsEventsLogging('Video Impression View', $jq('#collegeNameGA').val(), 'sponsored listing');
  } 
  for(var cnt=1; cnt<=10; cnt++){
	if(this.currentTime >= tenPcntSec*cnt) {
	  if(videoRanMap[videoElement.id+'_percentage'].indexOf(vidpct[cnt]) < 0) {
		 videoRanMap[videoElement.id+'_percentage'] += ','+vidpct[cnt];
		 GANewAnalyticsEventsLogging('Video Duration', $jq('#collegeNameGA').val() + "|" + 'sponsored listing', vidpct[cnt]);
	  }
	}
  }
//  if(this.currentTime == curVidDrtn && videoRanMap[videoElement.id+'_complete'] != "Y") {
//	videoRanMap[videoElement.id+'_complete'] = "Y";
//	GANewAnalyticsEventsLogging('Video Complete', $$D('collegeNameGA').value, vidpct[cnt]);
//  }
 };
}
//
function featuredDbStatsLogging(collegeId, networkId, featuredFlag,requestUrl){
  var ajax=new sack();
  deviceWidth =  getDeviceWidth();
  var clickType = "FeaturedClick";
  var networkId = $jq('#networkId').val();
  var externalUrl = '';
  if(requestUrl != undefined && requestUrl != ''){
    externalUrl = "&externalUrl="+encodeURIComponent(requestUrl);
  }
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z='+collegeId+"&screenwidth="+deviceWidth+"&featuredFlag="+featuredFlag+"&networkId="+networkId+"&clickType="+clickType + externalUrl;     
  ajax.requestFile = url;
  ajax.onCompletion = function(){ responseData(ajax); };	
  ajax.runAJAX();
}
function responseData(ajax){
  return false;
}
//to find retina device
function isRetina() {
    var query = '(-webkit-min-device-pixel-ratio: 1.5),\
    (min--moz-device-pixel-ratio: 1.5),\
    (-o-min-device-pixel-ratio: 3/2),\
    (min-device-pixel-ratio: 1.5),\
    (min-resolution: 144dpi),\
    (min-resolution: 1.5dppx)';
    if (window.devicePixelRatio > 1 || (window.matchMedia && window.matchMedia(query).matches)) {
    return true;
    }
    return false;
    }
// retina call for mobile images
function retina(){
  var devicePixel = window.devicePixelRatio;
  devicePixel = devicePixel > 2.5 ? 3 : devicePixel > 1.5 && devicePixel < 2.5 ? 2 : 1;
  var devWidth = $jq(window).width() || document.documentElement.clientWidth; 
	if($jq('#featured_image').length > 0 && devWidth > 480){	
		var imgObj = $jq('#featured_image').get(0);
			var imgSource = imgObj.getAttribute("data-src");
		    var isRetinaImageNeed = imgObj.getAttribute('data-rjs');
		    if(( imgSource != null && imgSource.indexOf('px@') <= -1) && isScrolledIntoView(imgObj, false)){
		      if(imgSource != null && isRetinaImageNeed != null && isRetinaImageNeed != undefined && isRetinaImageNeed == 'Y'){
		        var lastSlash = imgSource.lastIndexOf('.');
		        if(lastSlash != null){
		            var path = imgSource.substring(0, lastSlash);
		                file = imgSource.substring(lastSlash + 1);
		                if(devWidth < 480){
		                	retinaSrc = path + '_278px' + '@'+devicePixel+'x.' + file;
		                  }else if(devWidth > 481 && devWidth < 992){
		            //retinaSrc = path + '_369px.' + file;  
		                } else {
		            //retinaSrc = path + '_469px.' + file;  
		                }
		                imgObj.src = retinaSrc;
		                imgObj.setAttribute("data-src",retinaSrc);
		          }
		      }
			
		}
	} else {
		if (window.isRetina()) {		
			     if(devicePixel > 1){		    	 
			      var images = document.getElementsByTagName('img'); 
			      for (var i = 0; i < images.length; i++) {
			        var imgSource = images[i].getAttribute("data-src");
			        var isRetinaImageNeed = images[i].getAttribute('data-rjs');
			        if(( imgSource != null && imgSource.indexOf('px@') <= -1) && isScrolledIntoView(images[i], false)){
			          if(imgSource != null && isRetinaImageNeed != null && isRetinaImageNeed != undefined && isRetinaImageNeed == 'Y'){
			            var lastSlash = imgSource.lastIndexOf('.');
			            if(lastSlash != null){
			              var path = imgSource.substring(0, lastSlash);
			                  file = imgSource.substring(lastSlash + 1);
			                  if(devWidth < 480){
			                	if(imgSource.indexOf('_278px') > -1){
			                		retinaSrc = path + '@' + devicePixel + 'x.' + file;
			                	} else {
			                  	    retinaSrc = path + '_278px@' + devicePixel + 'x.' + file;
			                	}
			                  }else if(devWidth > 481 && devWidth < 992){
                           if(imgSource.indexOf('_369px') <= -1){
			                       retinaSrc = path + '_369px.' + file;
                            }  
			                  } else {
			              retinaSrc = path + '_469px.' + file;  
			                    }
			                  images[i].src = retinaSrc;
							  images[i].setAttribute("data-src",retinaSrc);
			            }
			          }
			        }
			      }
			    }
			}
	}
}
//fn to find visible images in screen 
function isScrolledIntoView(element, fullyInView){
var pageTop = $jq(window).scrollTop();      
var pageBottom = pageTop + $jq(window).height();
var elementTop = $jq(element).offset().top;
var elementBottom = elementTop + $jq(element).height();
if (fullyInView === true) {
return ((pageTop < elementTop) && (pageBottom > elementBottom));
} else {
return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
}
}

$jq(window).on('load', function() {
	retina();
});

function featuredBrandGaLogging(){
  ga('set', 'dimension17', 'Featured');
}