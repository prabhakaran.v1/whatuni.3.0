var $ns = jQuery.noConflict();
var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";
var contextPath = "/degrees";
var crsDef = 'Enter subject'
var univDef = 'Enter university name';
var locDef = 'Select location'
var ucasDef = 'Please enter UCAS code'
var regionIdArray;
var regionIdUrlArray;
var showCount = 2;

//
function isBlankOrNullString(str) {
  var flag = true;
  if(str != null && str.trim() != "" && str.trim().length > 0) {
    flag = false;
  }
  return flag;
}
//
function showHideLocDrpDn(id, type){
  if(type == 'show'){
	if($ns('#ajax_listOfOptions').is(':visible')){
	  $ns('#ajax_listOfOptions').hide();
	}  
    $ns('#'+id).show();
  }else if(type == 'hide'){
    $ns('#'+id).hide();
  }
  $ns('#locDrpdnFlg').val('n');
  $ns('#locUrlTextTP').val('');
}

function locSelectFn(locUrlText,locName){
  $ns('#locSrchId').val(locName);
  $ns('#locDrpDnId').hide();
  $ns('#locDrpdnFlg').val('y');
  $ns('#locUrlTextTP').val(locUrlText);
  //$ns.fn.constructAndFillRegion();
}

function setQualTP(styLvlId, styLvlUrlText){
  var styLvlTxt = $ns('#'+styLvlId).html();
	$ns('#navQualTP').val((styLvlTxt).replace(/&amp;/g, '&'));
	$ns('#topNavQualList').hide();
  var preQual = $ns('#selQualTpNav').val();
  if(preQual != styLvlTxt){
  var qualValue = $ns('#navQualTP').val();  
  $ns('#keywordTpNav_hidden').val('');
  $ns('#keywordTpNav_id').val('');
  $ns('#keywordTpNav_url').val('');
  $ns('#keywordTpNav_display').val('');
  $ns('#matchbrowsenodeTpNav').val('');
  }
  $ns("#navQualTP").next("#topNavQualList").hide();
  $ns("#keywordTpNav").focus();
  $ns('#keywordTpNav_qualification').val(styLvlUrlText);
  //$ns("#keywordTpNav").val('');
  //adv("#hdrmenu2").find("#navKwd").css("outline","2px solid #02ade2");
}
//
function autoCompleteKeywordBrowseTP(event,object){
  var navQual = $ns('#navQualTP').val().trim(); 
  var qualDesc = 'degree';
  var qualCode = "M";
  if(navQual == 'Degrees' || navQual == 'Undergraduate'){
    qualDesc = 'degree'; qualCode='M';
  }else if(navQual == 'Postgraduate'){
    qualDesc = 'postgraduate'; qualCode='L';
  }else if(navQual == 'Foundation degree'){
    qualDesc = 'foundation-degree'; qualCode='A';
  }else if(navQual == 'Access & foundation'){
    qualDesc = 'access-foundation'; qualCode='T';
  }else if(navQual == 'HND / HNC'){
    qualDesc = 'hnd-hnc'; qualCode='N';
  }else if(navQual == 'UCAS CODE'){
    qualDesc = 'ucas_code'; qualCode='UCAS_CODE';
  }
  $ns('#selQualTpNav').val(navQual);
  if($$('ajax_listOfOptions')){
    $$('ajax_listOfOptions').className="ajx_cnt ajx_res";
  }
  var inputTextVal = $ns('#'+object.id).val().trim().replace(/[%_]/g, '');
  var validLen = true;
  if(inputTextVal.length < 3){
	validLen = false;
	ajax_options_hide();
  }
  if(isThisValidTextAjaxKeyword(inputTextVal) && validLen){    
	ajax_showOptions(object,'subjectSearchTopNav',event,"indicator",'qual='+ qualCode +'&siteflag=desktop');	
    $ns('#errMsgSubSrchlbox').hide();
    $ns("#ajax_listOfOptions").appendTo("#subSrchDivId");
    $ns("#ajax_listOfOptions").removeAttr("style");
    if(object.value.length < 3){ajax_options_hide();}
  }  
}

//
$ns(document).on('click touchstart', function(e){
  ajaxOptHide(e, 'locSrchDivId', 'locDrpDnId');
  ajaxOptHide(e, 'styLvlDivId', 'topNavQualList'); 
});
//
$ns(document).ready(function() {  
  $ns(document).on("click","#srchUlId li", function(e){
	$ns('#errMsgSubSrchlbox').hide();
	$ns('#errMsgUniSrchlbox').hide();
	$ns('#errMsgAdvSrchlbox').hide();
	var contents = $ns('.tab-content');
	var clickedEleId = $ns(this).attr('data-tab');
	var target = $ns("#"+clickedEleId);
	$ns(".tab-link").not(this).removeClass('current');
	$ns(this).addClass('current');
	target.addClass('current');
	contents.not(target).removeClass('current');
	//$ns("#ajax_listOfOptions").remove();
	ajax_list_currentLetters = [];
  });
});
//
function ajaxOptHide(e,contrId, hidEle){
  var container = $ns('#'+contrId);
  $ns.each(container, function(key, value) {
    if (!$ns(value).is(e.target) // if the target of the click isn't the container...
        && $ns(value).has(e.target).length === 0) // ... nor a descendant of the container
    {
      $ns('#'+hidEle).hide();
    }
  });
}
//
function adviceSearchSubmitTp(){     
  var searchKeyword = $ns('#advSrchTpId').val().trim();        
  if(searchKeyword != "" && searchKeyword != "Enter keyword"){
	if(!$ns("#homeHeroImgDivId").is(":visible")){
	  ga('set', 'dimension1', 'topnav');
	}  
	GANewAnalyticsEventsLogging('Advice Search', 'Search', searchKeyword);
	$ns('#errMsgAdvSrchlbox').hide();
	var finalUrl = "/article-search/";
    $$("advSrchFormTp").action = finalUrl.toLowerCase();
	$$("advSrchFormTp").submit();
  }else{
    //alert("Please enter a valid keyword.");
    $ns('#errMsgAdvSrchlbox').show();
    return false;
  }
  return true;
}
function isThisValidTextAjaxKeyword(inputString) {
  var pattern = /^[\\!\"#$%&()*+\-,./:;<=>?@\[\]^_{|}~ 0-9]*$/g;
  var result = inputString.match(pattern);
  if(result != null) {
    result = result.toString();
  } else {
    result = "";
  }
  if(!isBlankOrNullString(result)) {
    return false;
  } else {
    return true;
  }
}