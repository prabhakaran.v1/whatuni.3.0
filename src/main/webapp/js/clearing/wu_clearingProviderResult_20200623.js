var jq = jQuery.noConflict();
var contextPath = "/degrees";
jq.fn.notBlankOrNull = function(input){if(!isNaN(input)){input = input.toString();}if(input != null && input != "" && input != undefined && (input != null && input.trim().length > 0)){return true;}else{return false;}}
var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;

jq.fn.urlFormation = function(pageFlag) {
	 var splitedURL = jq("sortingUrl").val(); 
	 var searchUrl = splitedURL.split("?");
	 splitedURL = searchUrl[0];
	 var filterString = splitedURL;
	 if(pageFlag == 'gradefilter'){	 
	   filterString = "";   
	 }
	 filterString += jq.fn.notBlankOrNull("isclearingPage") ?  "?clearing" : ""; 
	 
	 filterString += checkNull("hidden_score") ? "&score="+$$D("hidden_score").value : "";
	 searchURL = filterString; 
	 return searchURL;
}

jq.fn.formatUrl = function(url){
  var formattedUrl = url;
  if (formattedUrl != null) {
    if (formattedUrl.indexOf("?&")) {
	  formattedUrl = formattedUrl.replace("?&", "?");
	}
	if (formattedUrl.indexOf("?") == formattedUrl.length - 1) {
	  formattedUrl = formattedUrl.replace("?", "");
	}
  }
  return formattedUrl;
}

jq(document).ready(function(){
	jq('.ajax_ul').slimscroll({});
	jq(".srt1").click(function(){
	     if(jq("#sortBy").is(':visible')){
	       jq("#sortBy").hide();
	     } else {
	       jq("#sortBy").show();
	     }
	  });
    
    var userUcasPoint = jq("#userUcasPoint").val();
    
    if(_.isEmpty(userUcasPoint)){
    	jq("#gradeOptionToolTip").show();	
    	jq('#gradeOption').addClass('wuclrr_bactv_hvr');
    }
	
    jq("#chanceOfAcceptanceId").hover(function(){
        jq("#chanceofAccepatnceTooltip").show();
        }, function(){
         jq("#chanceofAccepatnceTooltip").hide();
      });

       jq("#closeChanceOfAcceptance").click(function(){
           jq("#chanceofAccepatnceTooltip").hide();
           return false;
      });
      
       jq("#gradeOption").hover(function(){
           jq("#gradeOptionToolTip").show();
           jq('#gradeOption').addClass('wuclrr_bactv_hvr');
           }, function(){
            jq("#gradeOptionToolTip").hide();
            jq('#gradeOption').removeClass('wuclrr_bactv_hvr');
       });
   
       jq("#closeGradeOption").click(function(){
           jq("#gradeOptionToolTip").hide();
           return false;
      });

});

jq.fn.setHeightForFiltBox = function(){
	// Scrollbar height
	var filterBlock = jq('#filterOpenType').val() + 'Block';
	if(jq('#pageName').val() == 'Institution Profile' || jq('#pageName').val() == 'Course Details'){
		filterBlock = 'filterSlider';
	}
	var filterOpenType = jq('#filterOpenType').val();
	var pageWidth = document.documentElement.clientWidth;
	var pageHeight = document.documentElement.clientHeight;
	
	var topFiltHeight = jq('#'+filterOpenType).find(".fltr_hd").outerHeight();
	topFiltHeight = 97; 
	//console.log("topFiltHeight-->"+topFiltHeight);
	
	var btmHeight = jq(".btm_pod").innerHeight() - 120;
	if(jq('.btm_pod').is(':visible')){
		btmHeight = jq(".btm_pod").innerHeight();
	}
	
	//console.log("btmHeight-->"+btmHeight);
	var scrollheight = pageHeight - topFiltHeight - btmHeight - 30;
	//console.log("scrollheight-->"+scrollheight);
	// For Devices
	if(pageWidth <= 480){
		scrollheight = pageHeight - topFiltHeight - btmHeight;
	}

	if(pageWidth > 480 && pageWidth <= 992){
		scrollheight = pageHeight - topFiltHeight - btmHeight;
	}
	var innerHg = jq.fn.getInnerHg();
	if( scrollheight < innerHg){
	  jq(".grd_fltr .stk_pod").addClass("flrt_bshd");
	}
	else{
	  jq(".grd_fltr .stk_pod").removeClass("flrt_bshd");
	}
	jq(".slimScrollDiv .slim-scroll").height(scrollheight);
	jq(".slimScrollDiv .slim-scroll").css('max-height', scrollheight);
}
//
jq.fn.getInnerHg = function(){
	  var totalHg = 0;
	  if(jq('#filterOpenType').val() == 'yourGrades'){
		totalHg += jq('#additional_qual_0') != undefined ? jq('#additional_qual_0').height() : 0;
		totalHg += jq('#additional_qual_1') != undefined ? jq('#additional_qual_1').height() : 0;
		totalHg += jq('#additional_qual_2') != undefined ? jq('#additional_qual_2').height() : 0;
		totalHg += jq('#additional_qual') != undefined ? jq('#additional_qual').height() : 0;
	  }
	  return totalHg;
	}

jq('body').on('click', '.closebtn, .clr_fltr .overlay', function(event){
	var filterOpenType = jq('#filterOpenType').val();
	jq.fn.copyFields();
	if(filterOpenType != 'yourGrades'){
	  jq('#clr_fltr, #sidenav, #'+filterOpenType).hide();
	  //jq.fn.urlFormation();
	  jq('#filterOpenType').val('');
	  var root = document.getElementsByTagName( 'html')[0];   
	    root.className = '';
	}
});

jq.fn.urlFormation = function(pageFlag) {
	 var splitedURL = jq("#pageUrl").val(); 
	 var qualification = jq('#qualificationHidden').val();
	 var university = jq('#universityHidden').val();
	 qualification = (pageFlag == 'PR_C') ? 'all' : qualification;
	 qualification = jq.fn.notBlankOrNull(qualification) ? qualification : jq.fn.notBlankOrNull(university) ? 'all' : 'degree';
	 var searchUrl = contextPath +'/';//splitedURL.split("?");
	 searchUrl += qualification + "-courses/"+((jq.fn.notBlankOrNull(university)) ? "csearch.html?" : "search.html?");
	 //splitedURL = searchUrl[0];
	 var filterString = searchUrl;
	 if(pageFlag == 'gradefilter'){	 
	   filterString = "";   
	 }
	 var flag = (pageFlag == 'PR_C'|| pageFlag == 'C') ? 'Url' : '';
	 filterString += 'clearing';
	 //filterString += jq.fn.notBlankOrNull("isclearingPage") ?  "?clearing" : "";
	 var subject = jq('#subjectHidden').val();
	 var region = jq('#regionHidden').val();
	 var studyMode = jq('#studyModeHidden').val();
	 var hybrid = jq('#hybridHidden').val();
	 var locationType = jq('#locationTypeHidden').val();
	 var acceptance = jq('#acceptanceHidden').val();
	 
	 var keyword = jq('#keywordHidden').val();
	 var campus = jq('#campusHidden').val();
	 var scholarship = jq('#scholarshipHidden').val();
	 var accommodation = jq('#accommodationHidden').val();
	 var score = jq('#scoreHidden').val();
	 var sortBy = jq('#sortHidden').val();
	 //var pageNo = jq('#pageNoHidden').val();
	 
	 filterString += (jq.fn.notBlankOrNull(subject)) ? "&subject=" + subject : (jq.fn.notBlankOrNull(keyword)) ? "&q=" + keyword : "";
	 filterString += (jq.fn.notBlankOrNull(university)) ? "&university=" + university : "";
	 filterString += ((jq.fn.notBlankOrNull(region)) && (region != 'united-kingdom')) ? "&location=" + region : "";
	 filterString += (jq.fn.notBlankOrNull(locationType)) ? "&location-type=" + locationType : "";
	 filterString += (jq.fn.notBlankOrNull(studyMode)) ? "&study-mode=" + studyMode : "";
	 //filterString += (jq.fn.notBlankOrNull(qualification)) ? "&qualification=" + qualification : "";
	 filterString += (jq.fn.notBlankOrNull(campus)) ? "&campus-type=" + campus : "";
	 filterString += (jq.fn.notBlankOrNull(scholarship)) ? "&scholarship=" + scholarship : "";
	 filterString += (jq.fn.notBlankOrNull(accommodation)) ? "&accommodation=" + accommodation : "";
	 filterString += (jq.fn.notBlankOrNull(score)) ? "&score=" + score : "";
	 filterString += (jq.fn.notBlankOrNull(hybrid)) ? "&hybrid=" + hybrid : "";
   filterString += (jq.fn.notBlankOrNull(acceptance)) ? "&acceptance=" + acceptance : "";
	 filterString += (jq.fn.notBlankOrNull(sortBy) && sortBy != 'TA'&& sortBy != 'R') ? "&sort=" + sortBy : "";
	 //filterString += (jq.fn.notBlankOrNull(pageNo) && pageNo != "1" && !(jq.fn.notBlankOrNull(subject))) ? "&pageno=" + pageNo : "";
	 
	 if(pageFlag == 'C' || pageFlag == 'PR_C'){
	   filterString = "";
	   filterString = searchUrl;
	   filterString += 'clearing';
	   if(pageFlag != 'PR_C'){filterString += (jq.fn.notBlankOrNull(subject)) ? "&subject=" + subject : (jq.fn.notBlankOrNull(keyword)) ? "&q=" + keyword : "";} 
	   filterString += (jq.fn.notBlankOrNull(university)) ? "&university=" + university : "";
	   filterString += (jq.fn.notBlankOrNull(sortBy) && sortBy != 'TA'&& sortBy != 'R') ? "&sort=" + sortBy : "";
	 }
	 
	 searchURL = filterString;
	 jq('#loaderTopNavImg').show(); 
	 window.location.href = searchURL;
	// return searchURL;
}

jq.fn.applyFilter = function(filterName){
	
}

jq('body').on('click', '#apply a', function(event){
	var filterOpenType = jq('#filterOpenType').val();
	jq('#' + filterOpenType).find('a').each(function(index) {
		if (jq(this).hasClass('slct_chip') && jq(this).is(":visible")) {
			var filter = jq(this).attr('data-filter-show');
			var gaValue = jq(this).attr('data-' + filter + '-ga-value');
			if (jq.fn.notBlankOrNull(gaValue)) {
				filterGALogging(gaValue);
			}
		}
	});
	if (filterOpenType != 'acceptanceFilter') {
		jq('#' + filterOpenType).find('input').each(function(index) {
			var filter = jq(this).attr('data-filter-show');
			var gaValue = "";
			if (jq("#" + filter).prop('checked')) {
				gaValue = filter + ',Yes';
			} else {
				gaValue = filter + ',No';
			}
			if (jq.fn.notBlankOrNull(gaValue) && jq.fn.notBlankOrNull(filter)) {
				filterGALogging(gaValue);
			}
		});
	} else {
		var gaValue = "";
		jq('#acceptanceDiv input').each(function(index){
			var value = jq(this).attr('data-acceptance-value');
			  if(jq(this).prop('checked')){
				  gaValue = getAcceptanceValue(value);
			  } 
		});
		if (jq.fn.notBlankOrNull(gaValue)) {
			gaValue = "chance-of-acceptance," + gaValue;
			filterGALogging(gaValue);
		}
	}
 if(jq('#filterOpenType').val() != 'yourGrades'){
	jq.fn.urlFormation();
 }
});
var stretch ="";
var possible ="";
var likely ="";
function getAcceptanceValue(value) {
	if(value.indexOf('likely') > -1){
		likely = 'Very Likely'; 
	 }
	if(value.indexOf('possible') > -1){
		possible = 'Likely'; 
	}
	if(value.indexOf('stretch') > -1){
		stretch = 'Possible'; 
	}	
	return likely+'|'+possible+'|'+stretch;
}

jq('body').on('click', '[data-sort-show]', function(event){
  if(jq('#filterOpenType').val() != 'yourGrades'){
	var sortBy = jq(this).attr('data-sort-show');
	jq('#sortHidden').val(jq(this).attr('data-'+sortBy+'-value'));
	jq.fn.urlFormation();
  } 	
});
jq('body').on('click', '[data-filter-show]', function(event){
	var filter = jq(this).attr('data-filter-show');
	 var id = jq(this).attr('data-subject-id');
		var type = jq(this).attr('data-filter-type');
	if(filter == 'qualification' || filter == 'region' || filter == 'studyMode' || filter == 'hybrid' || filter == 'campus'){
      if(jq(this).hasClass('slct_chip')){
        jq('#'+filter+'Div').find('a').removeClass('slct_chip');
		jq('#' + filter + 'Hidden').val('');
	  }else{
	    jq('#'+filter+'Div').find('a').removeClass('slct_chip');
        jq(this).addClass('slct_chip') ;
	  }	
	}else{
	  jq(this).toggleClass('slct_chip','');	
	}
		if(filter == 'subject' && type == 'A') {
			jq('#'+filter+'Div').find('a').removeClass('slct_chip').hide();
			 jq('#subject_'+id).addClass('slct_chip').show();
			 jq('#'+id).addClass('slct_chip');
			 jq('#ajaxSubjectDropDown').hide();
			 jq('#ajaxSubjectDropDown').parent('.slimScrollDiv').hide();
			 jq('#subjectAjax').val('');
		}
		if(filter == 'subject' && type == 'B') {
			 jq('#subject_'+id).removeClass('slct_chip').hide();
			 jq('#'+id).removeClass('slct_chip');			
		}
		if(filter != 'subject'){
	      jq('#bottom_button').show();
		}
	var value = "";
	
	if(filter == 'scholarship' || filter == 'accommodation') {
		if(jq('#'+filter).attr('class') == 'slct_chip') {jq('#'+filter).attr('data-'+filter+'-value', 'y');}
		if(jq("#"+filter).prop('checked')) {
			jq('#' + filter + 'Hidden').val('y');
		} else {
			jq('#' + filter + 'Hidden').val('');			
		}		
	} else if(filter == 'acceptance') {
		value = jq('#acceptanceDiv input').map(function(index){
			  if(jq(this).prop('checked')){
				return jq(this).attr('data-'+filter+'-value');
			  } 
		}).get().join(',');
	    jq('#'+filter+'Hidden').val(value);
	} else {
	value = jq('#'+filter+'Div').find('.slct_chip').map(function(index){
		return jq(this).attr('data-'+filter+'-value');
	  }).get().join(',');
    jq('#'+filter+'Hidden').val(value);
	}
	jq.fn.setHeightForFiltBox();
});

jq('body').on('click','[data-filter-option]' ,function(event){
  var filterType = jq(this).attr('data-filter-option');
  if(filterType == 'clearAllFilter'){
	    if(jq('#sesUcasPt').val() != ''){j.fn.clearSessionGradeFilter('no_redirect');}
		jq.fn.urlFormation('PR_C');
	}else if(filterType == 'acceptanceFilter' && jq('#acceptanceFilter').children().length == 0){
		
	}else if(filterType != 'yourGrades'){	
	if(filterType == 'subjectFilter'){
		jq.fn.getSubjectList(this,filterType);
	}
	if(filterType == 'acceptanceFilter'){
		jq('#acceptanceDiv input').each(function(index){
			var hiddenValue = jq('#acceptanceHidden').val();
			var value = jq(this).attr('data-acceptance-value');
			if(jq.fn.notBlankOrNull(hiddenValue) && hiddenValue.indexOf(value) > -1) {
				jq(this).prop('checked', true);				
			} else {
				jq(this).prop('checked', false);								
			}
		});
	}
	if(filterType != 'subjectFilter'){
	jq('#clr_fltr, #sidenav, #'+filterType).show();
	var root = document.getElementsByTagName( 'html')[0];   
    root.className = 'scrl_dis';
	
    jq('#bottom_button').hide();
	jq.fn.setHeightForFiltBox();
	jq('#filterOpenType').val(filterType);
	}
  }
  if(jq(this).hasClass('btnbg')){
	  jq('#bottom_button').show();
	  jq.fn.setHeightForFiltBox();
  }
});

jq.fn.getSubjectList = function(thisObj, filterType){
	var collegeId = jq('#collegeIdHidden').val();
	var qual = jq('#qualCodeHidden').val();
	var url = '/clearing/get-subject-ajax.html?'
	url += (jq.fn.notBlankOrNull(collegeId)) ? "&id=" + collegeId : "";
	url += (jq.fn.notBlankOrNull(qual)) ? "&qual=" + qual : "";
	url = jq.fn.formatUrl(url);
	jq('#loaderTopNavImg').show(); 
	  jq.ajax({
				type : "POST",
				url : contextPath + url,
				cache : true,
				success : function(result) {
					jq('#subjectFilter').html(result);
					jq('#loaderTopNavImg').hide();
					jq('#clr_fltr, #sidenav, #'+filterType).show();
					var root = document.getElementsByTagName( 'html')[0];   
				    root.className = 'scrl_dis';
					jq('#bottom_button').hide();
					jq.fn.setHeightForFiltBox();
					jq('#filterOpenType').val(filterType);
					jq('.ajax_ul').slimscroll({});
				//	console.log(jq('#subjectHidden').val());
					jq("#ajaxSubjectDropDown li a").each(function() {
						var value = jq(this).parent('li').attr('data-subject-value');
						if(value == jq('#subjectHidden').val()) {
							jq(this).parent('li').addClass('slct_chip');
						}
					});
					jq("#subjectDiv a").each(function() {
						var value = jq(this).attr('data-subject-value');
						if(value == jq('#subjectHidden').val()) {
							jq(this).addClass('slct_chip').show();
						}
					});
				}
		});
}


jq('body').on('click', '[data-subject-value]',function(event){
	var value = jq(this).attr('data-subject-value');
	jq('#subjectHidden').val(value);
	if(jq.fn.notBlankOrNull(jq(this).attr('data-subject-ga-value')) ) {
		var gaValue = "subject,"+jq(this).attr('data-subject-ga-value');
		filterGALogging(gaValue);
	}
	jq.fn.urlFormation();
});

jq.fn.copyFields = function(){
	jq('#subjectHidden').val(jq('#subjectHiddenUrl').val());
	jq('#regionHidden').val(jq('#regionHiddenUrl').val());
	jq('#studyModeHidden').val(jq('#studyModeHiddenUrl').val());
	jq('#hybridHidden').val(jq('#hybridHiddenUrl').val());
	jq('#locationTypeHidden').val(jq('#locationTypeHiddenUrl').val());
	jq('#acceptanceHidden').val(jq('#acceptanceHiddenUrl').val());
	 
	jq('#keywordHidden').val(jq('#keywordHiddenUrl').val());
	jq('#campusHidden').val(jq('#campusHiddenUrl').val());
	jq('#scholarshipHidden').val(jq('#scholarshipHiddenUrl').val());
	jq('#accommodationHidden').val(jq('#accommodationHiddenUrl').val());
	jq('#scoreHidden').val(jq('#scoreHiddenUrl').val());
	jq('#sortHidden').val(jq('#sortHiddenUrl').val());
	jq('#qualificationHidden').val(jq('#qualificationHiddenUrl').val());
	jq('#universityHidden').val(jq('#universityHiddenUrl').val());
	//jq('#pageNoHidden').val(jq('#pageNoHiddenUrl').val());
}

jq('body').on('click', '#clear', function(event){
jq('.grd_chips').find('a').each(function(index){
  if(jq(this).hasClass('slct_chip') && jq(this).is(":visible")){
    var filter = jq(this).attr('data-filter-show');
			if (filter == 'subject') {
				jq('#' + filter + 'Div').find('a').hide();
			}
    jq('#'+filter+'Div').find('a').removeClass('slct_chip');
	jq('#'+filter+'Hidden').val('');
  }
});
var filterOpenType = jq('#filterOpenType').val();
if(filterOpenType == 'acceptanceFilter'){
	  jq('input[data-acceptance-value]').prop('checked', false); 
	  jq('#acceptanceHidden').val('');
  }
jq('.switch input, .coa_cont input').each(function(index){
	if (jq(this).hasClass('slct_chip') && jq('.switch, .coa_cont').is(":visible")) {
	var filter = jq(this).attr('data-filter-show');
	jq('#' + filter).removeClass('slct_chip');
	jq('#'+filter).prop('checked', false);
	jq('#' + filter + 'Hidden').val('');
	}
});
	jq("#ajax_listOfOptions_subject li a").each(function(indx) {
		if(jq(this).hasClass('slct_chip')) {
			jq(this).removeClass('slct_chip');
		}
	});
});

function changeContact(hotlineNumber, anchorId) {
  jq("#" + anchorId).attr("title", hotlineNumber);
  jq("#" + anchorId).html("<i class='fa fa-phone' aria-hidden='true'></"+ "i>"+hotlineNumber);
  var mobileFlag = $jq("#check_mobile_hidden").val();
  if(mobileFlag == 'true'){
    setTimeout(function(){
     location.href='tel:'+ hotlineNumber;
    },1000)
  } 
}

jq('body').on('keyup', '#subjectAjax', function(event) {
	var value = jq('#subjectAjax').val().trim().toLowerCase();
	value = value.replace(reg, " ");
	value = value.trim();
    if (!(jq.fn.notBlankOrNull(value))) {
        return;
    }
	  if (value.length > 0) {
		var flag = "N";
		jq('#ajaxSubjectDropDown').show();
		jq("#ajaxSubjectDropDown li a").each(function() {
		  var listText = jq(this).text().toLowerCase();
		  var listId = jq(this).parent('li').attr('id');
		  var indexPos = listText.indexOf(value);
		  var prexfix = '<u>';
		  var suffix = '</u>';
		  if (indexPos >= 0 && (jq("#"+listId).attr('class') != 'slct_chip')) {
			var endPos = indexPos + value.length + prexfix.length;
			var texWithunderLine = jq(this).text().substring(0, indexPos) + prexfix + jq(this).text().substring(indexPos);
			texWithunderLine = texWithunderLine.substring(0, endPos) + suffix + texWithunderLine.substring(endPos);
			jq(this).html(texWithunderLine);
			jq('#' + listId).show();
			
			//jq('.ajax_ul').slimscroll({});
		  } else {
			  jq("#"+listId).hide();
		  }
	    });
	  } else {
		  jq('#ajaxSubjectDropDown').hide();
		  jq('#ajaxSubjectDropDown').parent('.slimScrollDiv').hide();
	  }
});

jq(document).on('click touchstart', function (event) {
  if (jq(event.target).closest("#ajaxSubjectDropDown").length === 0 && jq(event.target).closest("#subjectAjax").length === 0 && jq(event.target).closest("#sub_srch_icon").length === 0) {
		
		jq('#ajaxSubjectDropDown').hide();
		jq('#ajaxSubjectDropDown').parent('.slimScrollDiv').hide();
  }
});

function filterGALogging(gaValue) {
	var eventCategory = "filter";
	var eventAction = "";
	var eventLabel = "";
	if (gaValue.indexOf(",") > -1) {
		var eventValue = gaValue.split(',');
		eventAction = eventValue[0];
		eventLabel = gaValue.slice(gaValue.indexOf(",")+1);
		if(jq.fn.notBlankOrNull(eventAction) && jq.fn.notBlankOrNull(eventLabel)) {
			searchEventTracking(eventCategory, eventAction, eventLabel);
		}
	}
}

function getMatchTypeValue(id) {
	var course = jq("#courseMatchTypeMap").val();
	var uni = jq("#uniMatchTypeMap").val();
	var matchType = "";
	if(course != null && course != "" && course != undefined) {
	course.trim().slice(1, -1).split(',').forEach(function(v) {
	    var val = v.trim().split('=');
	    if(val[0] == id){
	    	matchType = val[1];
	    } 
	  });
	}
	if(uni != null && uni != "" && uni != undefined) {
	  uni.trim().slice(1, -1).split(',').forEach(function(v) {
	    var val = v.trim().split('=');
	    if(val[0] == id){
	    	matchType = val[1];
	    } 
	  });
	}
	  return matchType;	
}

function matchTypeGALogging(id) {
	var gaSetValue = getMatchTypeValue(id);
	ga('set', 'dimension19', gaSetValue);
}
