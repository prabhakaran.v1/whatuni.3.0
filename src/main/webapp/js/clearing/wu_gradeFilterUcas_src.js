var j = jQuery.noConflict();
var contextPathGf = '/degrees';
var filterValClicked = 'N';
var isFilterOpened = 'N';
var numbers = /^[0-9]+$/;
var MPDpPt = 0;    
var DPDpPt = 0;    
var DMDpPt =0;
var gradeFilterData = {};
var podType = '';
var firstValidQual = true;
var secondValidQual = true;
var thirdValidQUal = true;
var wudWidth = document.documentElement.clientWidth;
var wudHeight = document.documentElement.clientHeight;
j.fn.notBlankOrNull = function(input){if(!isNaN(input)){input = input.toString();}if(input != null && input != "" && input != undefined && (input != null && input.trim().length > 0)){return true;}else{return false;}}
j(document).ready(function(){
	 //alert('inside');
	j(window).on('resize orientationchange', function(){
		//alert('resize');
		j.fn.setHeightForFiltBoxGf();
		if(wudWidth <= 480){
		  applyAfterResize();
		}
	});
	j(document).on('focus blur', 'input[type=text]', function(e){
	    var $obj = j(this);
	    var nowWithKeyboard = (e.type == 'focusin');
	    j('body').toggleClass('view-withKeyboard', nowWithKeyboard);
	    onKeyboardOnOff(nowWithKeyboard);
	});
	j('.ajax_ul').slimscroll({});
	
	
	  j('body').on('click', '#clearGf', function(){
		  j.fn.formSubmitSRAndPR('N');
	  });
	  
	  j('body').on('click', '#applyGf', function(){
		  var podName = j('#filterPodName').val();
		  if( podName == 'ucas_cal'){
			  j.fn.formSubmitSRAndPR('Y', 'ucas_cal');
		  }else{
			  j.fn.formSubmitSRAndPR('Y');
		  }
		  
	  });
	  j('body').on('click', '#applyGfCal', function(){
		  j.fn.formSubmitSRAndPR('Y', 'ucas_cal');
	  });
	  
		j('body').on('click', '.drp_val', function(){
			  var count = j(this).attr('data-grade-option-open');
			  var topPos = 0;
			  if(count.indexOf('credit') > -1){				   
				  var grdOrd = '';
				  var dmp_point = j(this).attr('data-grade-option-point');
				  if(count == 'D_credit_0' || count == 'D_credit_1' || count == 'D_credit_2'){
					  grdOrd = count.replace('D_credit_','');
					  j('#grade_option_M_credit_'+grdOrd).addClass('dsp_none').hide();
					  j('#grade_option_P_credit_'+grdOrd).addClass('dsp_none').hide();
					  j('#grade_option_M_credit_'+grdOrd).parent('.slimScrollDiv').addClass('dsp_none').hide();
					  j('#grade_option_P_credit_'+grdOrd).parent('.slimScrollDiv').addClass('dsp_none').hide();
					  closeheDp(true);
				  }
				  if(count == 'M_credit_0' || count == 'M_credit_1' || count == 'M_credit_2'){
					  grdOrd = count.replace('M_credit_','');
					  j('#grade_option_D_credit_'+grdOrd).addClass('dsp_none').hide();
					  j('#grade_option_P_credit_'+grdOrd).addClass('dsp_none').hide();
					  j('#grade_option_D_credit_'+grdOrd).parent('.slimScrollDiv').addClass('dsp_none').hide();
					  j('#grade_option_P_credit_'+grdOrd).parent('.slimScrollDiv').addClass('dsp_none').hide();
					  closeheDp(true);
				  }
				  if(count == 'P_credit_0' || count == 'P_credit_1' || count == 'P_credit_3'){
					  grdOrd = count.replace('P_credit_','');
					  j('#grade_option_D_credit_'+grdOrd).addClass('dsp_none').hide();
					  j('#grade_option_M_credit_'+grdOrd).addClass('dsp_none').hide();
					  j('#grade_option_D_credit_'+grdOrd).parent('.slimScrollDiv').addClass('dsp_none').hide();
					  j('#grade_option_M_credit_'+grdOrd).parent('.slimScrollDiv').addClass('dsp_none').hide();
					  closeheDp(true);
				  }
			  }
			  if(j('#grade_option_'+count).is(":visible")){
				j('#grade_option_'+count).addClass('dsp_none'); 
				j('#grade_option_'+count).hide(); 
				j('#grade_option_'+count).parent('.slimScrollDiv').addClass('dsp_none');
				j('#grade_option_'+count).parent('.slimScrollDiv').hide();				
			  }else{
				j('#grade_option_'+count).removeClass('dsp_none');
				j('#grade_option_'+count).show();
				if(j('#qual_prepopulate_'+count).length > 0){
					topPos = j('#qual_prepopulate_'+count).offset().top	
				}
				
				j('#grade_option_'+count).parent('.slimScrollDiv').removeClass('dsp_none');
				j('#grade_option_'+count).parent('.slimScrollDiv').show();
				if(!(count.indexOf('credit') > -1)){
				  closeheDp(null,count);
				}				
				
			  }	
			  console.log(j(this).offset().top);
			  console.log(j('#grade_option_'+count).offset().top);
			  console.log('topPos'+topPos);			  
			  if(j('#grade_option_'+count).is(":visible")){
			  if((count == '1' > -1 ||count.indexOf('2') > -1) && j('#qual_prepopulate_'+count).length > 0 && topPos == j('#qual_prepopulate_'+count).offset().top){
			    j('.slimScrollDiv .custm_scrl').animate({
			        scrollTop: j('#grade_option_'+count).offset().top
			    },'slow');
			  }
			  }
			  
			});
		
		
		j('body').on('click', '[data-load-qual-id-value]', function(){
		  var qualOptionLevel = j(this).attr('data-load-qual-order-option');
		  if(j(this).attr('data-load-qual-id-value') != '' && j(this).attr('data-load-qual-id-value') != undefined){
			 j.fn.loadGradeFilter(this,'','N',qualOptionLevel);
			 j('#errorMsgGrd').text('');
			 j('#errorMsgGrd').hide();
		  }
		});
		
		j('[data-link-value]').click(function(){
			var inputUrl = j(this).attr('data-link-value');
			if(inputUrl != '' && inputUrl != undefined){
			  window.location.href = inputUrl;
			}
		});
				
	j(document).on('click touchstart', function (event) {
		var filterOpenType = j('#filterOpenType').val();
		if(isFilterOpened == 'Y' && j(event.target).closest("#filterOptionBlock a").length == 0 
				&& j(event.target).closest("[data-grade-remove-option]").length == 0 
				&& j(event.target).closest("#filterSlider").length === 0 
				&& !j('#filterSlider').hasClass('dsp_none') 
				&& j(event.target).html().indexOf('credits') < -1
				&& j(event.target).closest("[data-favourite-icon]").length === 0){
			
			if(filterValClicked == 'Y'){
			  //j.fn.formSubmitSRAndPR('Y');
			}else{
			  j.fn.closefilter(filterOpenType);	
			}
			
		  }		 
		if(j(event.target).closest(".overlay").length === 1){
		  	closeNav();
		  }		 

	  if(j(event.target).closest("#grade_option_0").length === 0 && j(event.target).closest(".drp_val").length === 0) {
			j('#grade_option_0').addClass('dsp_none').hide();			
			j('#grade_option_0').parent('.slimScrollDiv').addClass('dsp_none').hide();			
			var ajaxListActiveItem = j("#ajax_listOfOptions_grade0").find('.selected').get(0);
			j(ajaxListActiveItem).removeClass('selected');
		  }
	  
	  if(j(event.target).closest("#grade_option_1").length === 0 && j(event.target).closest(".drp_val").length === 0) {
			j('#grade_option_1').addClass('dsp_none').hide();			
			j('#grade_option_1').parent('.slimScrollDiv').addClass('dsp_none').hide();			
			var ajaxListActiveItem = j("#ajax_listOfOptions_grade1").find('.selected').get(0);
			j(ajaxListActiveItem).removeClass('selected');
		  }
	  if(j(event.target).closest("#grade_option_2").length === 0 && j(event.target).closest(".drp_val").length === 0) {
			j('#grade_option_2').addClass('dsp_none').hide();			
			j('#grade_option_2').parent('.slimScrollDiv').addClass('dsp_none').hide();			
			var ajaxListActiveItem = j("#ajax_listOfOptions_grade2").find('.selected').get(0);
			j(ajaxListActiveItem).removeClass('selected');
		  }
	  	for(var i = 0; i < 3 ; i++){
		    if(j(event.target).closest("#grade_option_D_credit_"+i).length === 0 && j(event.target).closest(".drp_val").length === 0) {
			  j('#grade_option_D_credit_'+i).addClass('dsp_none').hide();		 
			  j('#grade_option_D_credit_'+i).parent('.slimScrollDiv').addClass('dsp_none').hide();				 		  
		    }
		    if(j(event.target).closest("#grade_option_M_credit_"+i).length === 0 && j(event.target).closest(".drp_val").length === 0) {			  		  
			  j('#grade_option_M_credit_'+i).addClass('dsp_none').hide();
			  j('#grade_option_M_credit_'+i).parent('.slimScrollDiv').addClass('dsp_none').hide();	 			  	  
		    }
		    if(j(event.target).closest("#grade_option_P_credit_"+i).length === 0 && j(event.target).closest(".drp_val").length === 0) {			 
			  j('#grade_option_P_credit_'+i).addClass('dsp_none').hide();
			  j('#grade_option_P_credit_'+i).parent('.slimScrollDiv').addClass('dsp_none').hide();		  
			}
	  	}
	  j.fn.setHeightForFiltBoxGf();
    });	
    
	j('body').on('click','[data-filter-option]' ,function(event){
		var filterType = j(this).attr('data-filter-option');		 
		j('#filterPodName').val('');
		if(filterType != '' && filterType != undefined){
			if(filterType != 'clearAll'){
				//
			  if(filterType == 'yourGrades'){
				  j.fn.loadGradeFilter(this,'yourGrades');
			  }	
			}else{
				//
				//j.fn.gaEventLogging("Filters", "Clear All", "Clicked", "Y");
				//
				var formURL = j.fn.formSRAndPRURL('C');
				if(j('#studyLevelHidden').val() != '' || j('#studyLevelPreHidden').val() != ''){
					j.fn.clearSessionGradeFilter(formURL);	
				}else{
					j('#loaderTopNavImg').show();
					window.location.href = formURL;	
				}
			}
		}
		return false;
	});	
	
	j.fn.loadGradeFilter = function(thisObj,filterType, addQualFlag,divId, studyLevel, entryPoints){
		var filterCount = j('#grade_qual_0').val() == undefined ? '0' : j('#grade_qual_1').val() == undefined ? '1' : '2' ;
		if(filterType == 'yourGrades' && j('#yourGradesBlock').html().trim() != ''){
			j.fn.gradeOpenFilter();
		}else{
			var inputAttr = {};
			if(filterType != 'yourGrades' && addQualFlag != 'Y'){
				j(thisObj).parent('li').addClass('drp_act');
				inputAttr.qualId = j(thisObj).attr('data-load-qual-id-value');	
				inputAttr.qualLevel = j(thisObj).text();
			}
			var countIncrease = 'N';
			if(j('#filterHitCount').val() == '0' && j('#studyLevelPreHidden').val() != '' && addQualFlag != 'Y'){
				inputAttr.studyLevel = j('#studyLevelPreHidden').val();
				inputAttr.entryPoints = j('#gradePointsPreHidden').val();
				countIncrease = 'Y';
			}
			if(addQualFlag == 'Y'){
				inputAttr.addQualFlag = 'Y';
			}
			if(studyLevel != undefined && entryPoints != undefined){
				inputAttr.studyLevel = studyLevel;
				inputAttr.entryPoints = entryPoints;
				inputAttr.removeFlag = "Y";
			}
			var count = ((divId == '' || divId == undefined) ? filterCount : divId)
			inputAttr.filterHitCount = count;
			//alert('test');
			j('#loaderTopNavImg').show(); 
			j.ajax({
				type : "POST",
				url :  contextPathGf + "/get-grade-filter-info.html",
				data : inputAttr,
				cache : true,
				success : function(result) {
			      if(divId != '' && divId != undefined && addQualFlag != 'Y'){
			    	  j('#grade_filter_' + divId).html(result);
			    	  j('#loaderTopNavImg').hide();
			    	  j('#grade_option_'+divId).children().removeClass('drp_act');
			    	  j('#qual_prepopulate_'+divId).html(j(thisObj).text());
			    	  j('#grade_option_'+divId).addClass('dsp_none');
			    	  j('#grade_option_'+divId).hide();
			    	  j('#grade_option_'+divId).parent('.slimScrollDiv').addClass('dsp_none');
			    	  j('#grade_option_'+divId).parent('.slimScrollDiv').hide();
			    	  if(j('#grade_qual_'+divId).val() != undefined){
				  		  j('#qual_prepopulate_'+divId).html(j('#grade_qual_'+divId).val())
				  	  }
			    	  j.fn.gradeOpenFilter();
			    	  j.fn.showAndHideApplyAndClearBtn();
			    	  j.fn.setHeightForFiltBoxGf();
			    	  j.fn.updatePoints();
			    	  j.fn.showAndHideApplyAndClearBtn();
			      }else {
			    	  var showFlag = true; 
			    	  if(addQualFlag == 'Y'){
			    		  j('#additional_qual_'+(parseInt(j('#filterHitCount').val())-1)).after(result);
			    		  j('#loaderTopNavImg').hide();
			    		  j('#filterHitCount').val(parseInt(j('#filterHitCount').val())+1);
			    		  addQualClick = 'Y';
			    		  if(inputAttr.removeFlag == 'Y' && j('#grade_qual_'+count).val() == 'Access to HE Diploma'){
				    		 heDipDropDownGen('1');
				    	  }
			    	  }else {
			    		  j('#yourGradesBlock').html(result);
			    		  j('#loaderTopNavImg').hide();
			    		  j('#filterHitCount').val(parseInt(j('#filterHitCount').val())+1);
//				    	  j(thisObj).addClass('flbt_act');  
			    		  var aObj = j('#grade_option_0 li:nth-child(2) a');
			    		  if(j('#grade_qual_'+count).val() == 'Please select'){
			    		    j.fn.loadGradeFilter(aObj,'','N','0');
			    		    showFlag = false; 
			    		  }
			    	  }
			    	  if(j('#grade_qual_'+count).val() != undefined){
				  		  j('#qual_prepopulate_'+count).html(j('#grade_qual_'+count).val())
				  	  }
			    	  if(showFlag){
		    		    j.fn.gradeOpenFilter();
			    	    j.fn.showAndHideApplyAndClearBtn();
			    	    j.fn.setHeightForFiltBoxGf();
			    	    j.fn.updatePoints();
			    	    j.fn.showAndHideApplyAndClearBtn();
			    	  }
			    	  
			      }
			      j('#grade_option_0,#grade_option_1,#grade_option_2').parent('.slimScrollDiv').addClass('dsp_none').hide();
			      if(j('#filterHitCount').val() == '3' || (j('#grade_qual_0').val() != undefined && j('#grade_qual_1').val() != undefined && j('#grade_qual_2').val() != undefined)){
		    		j('#additional_qual').addClass('dsp_none').hide();  
		    		j('#filterHitCount').val('3')
		          }else{
		        	  j('#additional_qual').removeClass('dsp_none').show();
		        	  j('#filterHitCount').val(j('#grade_qual_0').val() == undefined ? '0' : j('#grade_qual_1').val() == undefined ? '1' : '2')  
		        	  if(countIncrease == 'Y'){
				    	  j('#filterHitCount').val(j('#grade_qual_0').val() == undefined ? '0' : j('#grade_qual_1').val() == undefined ? '1' : '2')  
				      }
		          }
			      isFilterOpened = 'Y';
				}
			});
		}
	};
	
	j.fn.gradeOpenFilter = function(){
		j('#filterSlider').removeClass('rginfl ');
	    j('#filterOpenType').val('yourGrades');
		j('#overlay,#filterSlider,#yourGradesBlock,#ucasPoints').removeClass('dsp_none');
		j('#overlay,#filterSlider,#yourGradesBlock,#ucasPoints').show();
		var root = document.getElementsByTagName( 'html')[0];   
	    root.className = 'scrl_dis';
		j('#mainBody').addClass('bdy_lft');
		j('#filterSlider').addClass('slide');
		j('#filterSlider').show();
	    j('.ajax_ul').slimscroll({});
	}
	j.fn.initCap = function(inputString) {
		var actValueArry = inputString.split(' ');
		var initCapVal = '';
		for (var i = 0; i < actValueArry.length; i++) {
			initCapVal = initCapVal+ actValueArry[i].substring(0, 1).toUpperCase() + ''+ actValueArry[i].substring(1).toLowerCase() + ' ';
		}
		return initCapVal.trim();
	}

	j.fn.initCapNearUTagCenter = function(inputString) {
		var actValueArry = inputString.split(' ');
		var initCapVal = '';
		for (var i = 0; i < actValueArry.length; i++) {
			if (actValueArry[i].indexOf('<u>') == 0) {
				initCapVal = initCapVal + '<u>'+ actValueArry[i].substring(3, 4).toUpperCase() + actValueArry[i].substring(4).toLowerCase() + ' ';
			} else {
				initCapVal = initCapVal + j.fn.initCap(actValueArry[i]) + ' ';
			}
		}
		return initCapVal.trim();
	}
    var tabFlag = '';    

	j.fn.openAndCloseFilter = function(filterOpenType) {
		if (filterOpenType == 'close') {
			filterOpenType = j('#filterOpenType').val();
			if(filterValClicked == 'Y'){
			  //j.fn.formSubmitSRAndPR('Y');
			  //j.fn.closefilter(filterOpenType);	
		    }else{
			  j.fn.closefilter(filterOpenType);	
			}
		} 
	}
	
	j.fn.closefilter = function(filterOpenType){
		j('#filterSlider').removeClass('rginfl ').removeClass('slide');
	    j('#filterOpenType').val('');
		j('#overlay,#filterSlider,'+'#'+filterOpenType+'Block').addClass('dsp_none');
		j('#overlay,#filterSlider,'+'#'+filterOpenType+'Block').hide();
		var root = document.getElementsByTagName( 'html')[0];   
	    root.className = '';
		j('#mainBody').removeClass('bdy_lft');
	    j('.ajax_ul').slimscroll({});
	    setTimeout(function(){ j('#filterOptionBlock').find('a[data-filter-option='+filterOpenType+']').focus(); }, 100);
	}
	
	
	
	j.fn.formSubmitSRAndPR = function(applyFlag,isClose) {
		var filterOpenType = j('#filterOpenType').val();
		var filterType = filterOpenType;
		var hiddenId = j.fn.returnHiddenId(filterType, 'Hidden');
		if (applyFlag == 'N') {
			j('#' + hiddenId).val('');
			j('#' + filterType + 'List').find('a').removeClass('slct_chip');
			filterValClicked = 'Y';
			if(filterType == 'yourGrades'){
				j.fn.appendGradeFilter('N')	
			}
		} else{
		  var formURL = j.fn.formSRAndPRURL('SR',isClose)
		  if(applyFlag == 'Y' && (j('#errorMsgGrd').is(':visible') || (firstValidQual == false || secondValidQual == false || thirdValidQUal == false))){
			  j('#errorMsgGrd').show();
		  }else{
			j('#overlay,#filterSlider,' + '#' + filterOpenType + 'Block').addClass('dsp_none');
			j('#overlay,#filterSlider,' + '#' + filterOpenType + 'Block').hide();
			j('#mainBody').removeClass('bdy_lft');
			j('#filterSlider').removeClass('slide').removeClass('rginfl ');			
			if(filterOpenType == 'yourGrades' && (applyFlag == 'N' || j.fn.appendGradeFilter('Y') == '')){
				j.fn.clearSessionGradeFilter(formURL);	
			}else if(filterOpenType == 'yourGrades' && (applyFlag == 'Y' && j.fn.appendGradeFilter('Y') != '')) {
				j.fn.saveGradeFilter(formURL,isClose);
			}else {
				j('#loaderTopNavImg').show();
				window.location.href = formURL;	
			}
			var root = document.getElementsByTagName( 'html')[0];   
		    root.className = '';
		  }
			//setTimeout(function(){ window.location.href = formURL;}, 400);
		} 

	}

	j.fn.formSRAndPRURL = function(flag,isClose) {
	  var formURL = '';
	  var isSRorPR = j('#pageName').val();
      var studyLvel = j('#studyLevelPreHidden').val();
      var pageName = j('#pageName').val();
      var entryPoints = j('#gradePointsPreHidden').val();
      var points = j('#pointsPreHidden').val();      
      formURL = window.location.href;
      var subject = '';
      var filterOpenType = j('#filterOpenType').val();
      var queryParam = '';
      if(flag == 'C' && pageName == 'Search Results'){
    	   
      }else if(flag == 'C'){
    	   
      } else if(flag != 'C'){
    	  
          var queryParam = '';          
          formURL = removingOldGradeParam(formURL);
          queryParam += (filterOpenType == 'yourGrades') ? j.fn.appendGradeFilter('Y') : (studyLvel != '' && entryPoints != '') ? '&studylevel='+ studyLvel + '&entrypoints=' + entryPoints + '&score=' + points:'' ;
          formURL += queryParam;          
      }		
      formURL = j.fn.formatUrl(formURL);
	  return formURL.toLowerCase();
	}
	
	j.fn.appendGradeFilter = function(flag){
		var gradeFilter = '';
		var studyLevel = '';
		var entryPoints = '';
		firstValidQual = true;
		secondValidQual = true;
		thirdValidQUal = true;
		var arr = ['1','2','3']
		j.each(arr, function(index){
			if(j('#grade_qual_'+index).val() != '' && j('#grade_qual_'+index).val() != undefined){
				if(flag == 'N'){
					if(j('#grade-option-flag-'+index).val() == 'Y'){
					  j('#grade_filter_'+index).find('span').map(function(indexInner){
						j(this).text('0');
					  });
				    }else if(j('#grade-option-flag-'+index).val() == 'T'){
				       j('#ucas_'+index).val('');
				    }else if(j('#grade-option-flag-'+index).val() == 'D'){
				    	j('#qual_prepopulate_D_credit_'+index+'_span').text('0 credits');
				    	j('#qual_prepopulate_M_credit_'+index+'_span').text('0 credits');
				    	j('#qual_prepopulate_P_credit_'+index+'_span').text('0 credits');
				    }else{
				    	j('#grade_filter_'+index).find('a').removeClass('slct_chip');
				    }
					j('#ucasPoints_span').text('0');
					j('#total_points_'+index).val() != undefined ? j('#total_points_'+index).val('0') : 0;
					j('#errorMsgGrd').hide();
					j.fn.setHeightForFiltBoxGf();
				}else {
				  var totalPoint=0;
			 	  var appendGradeAndVal = '';
			 	  if(j('#grade-option-flag-'+index).val() == 'N'){
			 		 j('#grade_filter_'+index).find('a').each(function(indexInner){
			 			 if(j(this).hasClass('slct_chip')){
			 				studyLevel += (studyLevel == '' ? j('#grade_qual_url_'+index).val() : ','+ j('#grade_qual_url_'+index).val()); 
			 				entryPoints += entryPoints == '' ? j(this).text().trim() : ',' + j(this).text().trim();  
			 			 }
			 		 });
			 	 }else if(j('#grade-option-flag-'+index).val() == 'T'){
			 		var ucasPoint =  j('#ucas_'+index).val().trim();
			 		if(ucasPoint != 0 && ucasPoint != '' && ucasPoint > 0 && ucasPoint < 1000 && (ucasPoint.match(numbers))){
			 		  studyLevel += (studyLevel == '' ? j('#grade_qual_url_'+index).val() : ','+ j('#grade_qual_url_'+index).val());  
				 	  entryPoints += entryPoints == '' ? ucasPoint : ',' + ucasPoint;	
			 		}else if(ucasPoint != 0 && ucasPoint != ''){
			 		  j('#errorMsgGrd').text('Please enter the valid UCAS points');
			    	  j('#errorMsgGrd').show();
			 		}			 					 		
			     }else if(j('#grade-option-flag-'+index).val() == 'D'){
			    	var dpt = parseInt(j('#qual_prepopulate_D_credit_'+index+'_span').text().replace(/credits/gi, '').trim());
			    	var mpt = parseInt(j('#qual_prepopulate_M_credit_'+index+'_span').text().replace(/credits/gi, '').trim());
			    	var ppt =parseInt(j('#qual_prepopulate_P_credit_'+index+'_span').text().replace(/credits/gi, '').trim());
			    	var heEntryPt = 'D'+dpt + '-M' + mpt + '-P' + ppt;
			    	var totCrd = dpt + mpt + ppt;
			    	if(totCrd == 45){
			    	  studyLevel += (studyLevel == '' ? j('#grade_qual_url_'+index).val() : ','+ j('#grade_qual_url_'+index).val());  
					  entryPoints += entryPoints == '' ? heEntryPt : ',' + heEntryPt;
					  j('#errorMsgGrd').hide();
			    	}else if(totCrd > 0 && totCrd < 45){
			    		j('#errorMsgGrd').text('A total of 45 credits for Access to HE Diploma must be added for tariff points to be calculated');
			    		j('#errorMsgGrd').show();
			    		j.fn.setHeightForFiltBoxGf();
			    		if(index == 0){firstValidQual = false;}if(index == 1){secondValidQual = false;}if(index == 2){thirdValidQUal = false;}
			    	}else{
			    		j('#errorMsgGrd').hide();
			    	}
			     }else{
			 		j('#grade_filter_'+index).find('span').map(function(indexInner){
					  appendGradeAndVal += (indexInner == 0 ? j(this).text().trim() + j('#grade_name_'+index+'_'+indexInner).text().trim() : '-'+j(this).text().trim() + j('#grade_name_'+index+'_'+indexInner).text().trim());
					  totalPoint += parseInt(j(this).text().trim());
					});  
			 	  }
				  if(totalPoint != 0){
					studyLevel += (studyLevel == '' ? j('#grade_qual_url_'+index).val() : ','+ j('#grade_qual_url_'+index).val());
					entryPoints += (entryPoints=='' ? appendGradeAndVal : ',' + appendGradeAndVal);
				  }
				}  
			}
		});
		if(studyLevel != '' && entryPoints != ''){
			  //gradeFilter = '&studylevel='+ studyLevel + '&entrypoints=' + entryPoints + '&score='+j('#ucasPoints_span').text().trim();
			  gradeFilter = '&score='+j('#ucasPoints_span').text().trim();
			  gradeFilterData.userStudyLevelEntry = studyLevel;
			  gradeFilterData.userEntryPoint = entryPoints;
			  gradeFilterData.ucasPoint = j('#ucasPoints_span').text().trim();
		}
		return gradeFilter;
	}

	j.fn.returnHiddenId = function(filterType, appendValue) {
		var hiddenId = filterType + appendValue;
		return hiddenId;
	}	
	
	
	j('body').on('click','[data-grade-remove-option]',function(){
		var gradeOptionQual = j(this).attr('data-grade-remove-option');
		j('#additional_qual_'+gradeOptionQual).remove();
		var additionQualExist = '';
		filterValClicked = 'Y';	
		if(gradeOptionQual == '1'){
		  iterateValFromLi = '2';
		  if(j('#grade-option-flag-'+iterateValFromLi).val() != '' && j('#grade-option-flag-'+iterateValFromLi).val() != undefined){
			  additionQualExist = 'Y';  
		  }
		}else if(gradeOptionQual == '2'){	
		  iterateValFromLi = '1';
		  if(j('#grade-option-flag-'+iterateValFromLi).val() != '' && j('#grade-option-flag-'+iterateValFromLi).val() != undefined){
			  additionQualExist = 'Y';  
		  }
		}
		if(additionQualExist != ''){
			var entry_points = '';
			if(j('#grade-option-flag-'+iterateValFromLi).val() == 'Y'){
				entry_points = j('#grade_filter_'+iterateValFromLi).find('span').map(function(index){
					return  (j(this).text().trim() + j('#grade_name_'+iterateValFromLi+'_'+index).text().trim());
				}).get().join('-');	
			}else if(j('#grade-option-flag-'+iterateValFromLi).val() == 'T'){
		 		var ucasPoint =  j('#ucas_'+iterateValFromLi).val().trim();
		 		if(ucasPoint != 0 && ucasPoint != '' && ucasPoint > 0 && ucasPoint < 1000 && (ucasPoint.match(numbers))){		 		  
		 			entry_points += entry_points == '' ? ucasPoint : ',' + ucasPoint;	
		 		}			 					 		
		     }else if(j('#grade-option-flag-'+iterateValFromLi).val() == 'D'){
		    	var dpt = parseInt(j('#qual_prepopulate_D_credit_'+iterateValFromLi+'_span').text().replace(/credits/gi, '').trim());
		    	var mpt = parseInt(j('#qual_prepopulate_M_credit_'+iterateValFromLi+'_span').text().replace(/credits/gi, '').trim());
		    	var ppt =parseInt(j('#qual_prepopulate_P_credit_'+iterateValFromLi+'_span').text().replace(/credits/gi, '').trim());
		    	var heEntryPt = 'D'+dpt + '-M' + mpt + '-P' + ppt;
		    	var totCrd = dpt + mpt + ppt;
		    	//if(totCrd == 45){
		    	  
		    	entry_points += entry_points == '' ? heEntryPt : ',' + heEntryPt;
				  
		    	//}
		     }else{
			   j('#grade_filter_'+iterateValFromLi).find('a').each(function(index){
				if(j(this).hasClass('slct_chip')){
					entry_points +=	 j(this).text().trim();  
				}
			  });	
			}
			var studyLevel = j('#grade_qual_url_'+iterateValFromLi).val();
			j('#additional_qual_'+iterateValFromLi).remove();
			j.fn.loadGradeFilter('','', 'Y','',studyLevel,entry_points);
		}
		j('#filterHitCount').val('1');
		j('#additional_qual').removeClass('dsp_none');
		j('#additional_qual').show();
		j('#errorMsgGrd').hide();
		j('#errorMsgGrd').text('');
		j.fn.updatePoints();
		j.fn.showAndHideApplyAndClearBtn();
	});
	var addQualClick = 'Y';
	j('body').on('click','#additional_qual_in', function(){
		if(addQualClick == 'Y'){
			addQualClick = 'N';
			j.fn.loadGradeFilter('','', 'Y','');	
		}
	});
	
	j('body').on('click', '[data-grade-option-flag]', function(){
		j('#errorMsgGrd').hide();
		j('#errorMsgGrd').text('');
		var gradeOptionFlag = j(this).attr('data-grade-option-flag');
		filterValClicked = 'Y';
		if(gradeOptionFlag == 'Y'){
			var id=  j(this).attr('data-load-qual-order-option');
			var num =  j(this).attr('data-grade-index');
			var value = j(this).attr('data-grade-index-type');
			var totalMaxPoint = j(this).attr('data-grade-total-max-point');
			var maxPoint = j(this).attr('data-grade-max-point');
			var replaceVal = 0;
			if(j.fn.validateGradeFilterPoints(id,num,value,totalMaxPoint,maxPoint)){
				replaceVal += parseInt(j('#point_'+id+'_'+num).text()) + parseInt(value);
				replaceVal = replaceVal < 0 ? 0 : replaceVal;
				if(replaceVal > 0){
					j('#grd_prnt_id_'+id+'_'+num).addClass("grd_drk");
				} else{
					if(j('#grd_prnt_id_'+id+'_'+num).hasClass('grd_drk')){
						 j('#grd_prnt_id_'+id+'_'+num).removeClass("grd_drk");
					}
				}			  
				j('#point_'+id+'_'+num).text(replaceVal);
			}
		//}else if(j('#grade-option-flag-'+index).val() == 'T'){
	 			 		
		}else if(gradeOptionFlag == 'D'){		  
		  var crdIdx = j(this).attr('data-credit-index');
		  var creditVal = j(this).text();		  
		  var crdTypePts = j(this).attr('data-load-hedip-id-value');
		  var crdType = j(this).attr('data-load-hedip-id');
		  var crdTypeOrd = j(this).attr('data-load-credit-order-option');
		  var crdPts = j(this).val();
		  j('#qual_prepopulate_'+crdTypeOrd+'_span').text(creditVal);
		  if(j('#grade_option_'+crdTypeOrd).is(":visible")){
			j('#grade_option_'+crdTypeOrd).addClass('dsp_none').hide(); 			
			j('#grade_option_'+crdTypeOrd).parent('.slimScrollDiv').addClass('dsp_none').hide();			
			console.log('hide');
		  }
		  heDipDropDownGen(crdIdx,creditVal,crdTypePts,crdType,crdTypeOrd)		  	  
		}else {
		  var clrIdVal=  j(this).attr('data-load-qual-order-option');
		  j('#grade_filter_'+clrIdVal).find('a').removeClass('slct_chip');
		  j(this).addClass('slct_chip');
		  j('#total_points_'+clrIdVal).val(j(this).attr('data-grade-points'));
		}
		var arr = ['1','2','3']
		var isValueSelected = 'N';
		j.each(arr, function(index){
	    var updatePoint = 0;	
		if(j('#grade-option-flag-'+index).val() == 'N' && j('#grade-option-flag-'+index).val() != undefined){
	 		 j('#grade_filter_'+index).find('a').each(function(indexInner){
	 			 if(j(this).hasClass('slct_chip')){
	 				updatePoint =  parseInt(j(this).attr('data-grade-points'));
	 				isValueSelected = 'Y';
	 			 }
	 		 });
	 		j('#total_points_'+index).val(updatePoint);
	 	}else if(j('#grade-option-flag-'+index).val() == 'T' && j('#grade-option-flag-'+index).val() != undefined){
	 	  var ucasQualPoint = j('#ucas_'+index).val().trim()
	 	  if(ucasQualPoint != '' && ucasQualPoint > 0 && ucasQualPoint < 1000 && (ucasQualPoint.match(numbers))){
			  j('#total_points_'+index).val(parseInt(ucasQualPoint));
		  }else{
		  	j('#total_points_'+index).val(0);
		  }    	 		
		}else if(j('#grade-option-flag-'+index).val() == 'D' && j('#grade-option-flag-'+index).val() != undefined){
			var crdTypeDIdVal = '3.2';
			var crdTypeMIdVal = '2.1333';
			var crdTypePIdVal = '1.0666';
			var dptc = parseInt(j('#qual_prepopulate_D_credit_'+index+'_span').text().replace(/credits/gi, '').trim());
			var mptc = parseInt(j('#qual_prepopulate_M_credit_'+index+'_span').text().replace(/credits/gi, '').trim());
			var pptc =parseInt(j('#qual_prepopulate_P_credit_'+index+'_span').text().replace(/credits/gi, '').trim());
			var totCrdt = dptc + mptc + pptc;
	    	if(totCrdt == 45){
			  var dpt = (parseInt(j('#qual_prepopulate_D_credit_'+index+'_span').text().replace(/credits/gi, '').trim()) * crdTypeDIdVal);
			  var mpt = (parseInt(j('#qual_prepopulate_M_credit_'+index+'_span').text().replace(/credits/gi, '').trim()) * crdTypeMIdVal);
			  var ppt = (parseInt(j('#qual_prepopulate_P_credit_'+index+'_span').text().replace(/credits/gi, '').trim()) * crdTypePIdVal);
			  var totPts = Math.round(dpt + mpt + ppt);
			  j('#total_points_'+index).val(totPts);
	    	}else{
	    		j('#total_points_'+index).val('0');
	    	}
		}else if(j('#grade-option-flag-'+index).val() != undefined){
	 		j('#grade_filter_'+index).find('span').map(function(indexInner){
	 			if(parseInt(j('#point_'+index+'_'+indexInner).text().trim()) > 0){
	 				isValueSelected = 'Y';
	 			}
	 			updatePoint +=  (parseInt(j('#point_'+index+'_'+indexInner).text().trim()) * parseInt(j(this).next().attr('data-grade-points')));
			});  
	 		j('#total_points_'+index).val(updatePoint);
	 	  }
		});
		j.fn.updatePoints();
		j.fn.showAndHideApplyAndClearBtn(isValueSelected);
	});
	
	j.fn.updatePoints = function(){
		var totalPoints = 0;
		totalPoints += j('#total_points_0').val() != undefined ? parseInt(j('#total_points_0').val()) : 0;
		totalPoints += j('#total_points_1').val() != undefined ? parseInt(j('#total_points_1').val()) : 0;
		totalPoints += j('#total_points_2').val() != undefined ? parseInt(j('#total_points_2').val()) : 0;
		j('#ucasPoints_span').text(totalPoints);
	}
	j.fn.validateGradeFilterPoints = function (id,num,value,totalMaxPoint,maxPoint){
		var currentSpan = parseInt(j('#point_'+id+'_'+num).text().trim());
		var totalSpan = 0;
		j('#grade_filter_'+id).find('span').each(function(index){
			totalSpan += parseInt(j(this).text().trim());
		});
		currentSpan += parseInt(value);
		totalSpan += parseInt(value);
		maxPoint = parseInt(maxPoint);	
		totalMaxPoint = parseInt(totalMaxPoint);	
		if(currentSpan <= maxPoint && totalSpan <= totalMaxPoint){
			return true;
		}
		return false;
	}
	
	j.fn.showAndHideApplyAndClearBtn = function(isValueSelected){
	  var filterType = j('#filterOpenType').val();
	  if(isValueSelected == 'Y'){
		  j('#bottom_button_gf').removeClass('dsp_none');
		  j('#bottom_button_gf').show();
	  }else if(filterType == 'yourGrades'){
		j('#pointsPreHidden').val() != ''  || j('#ucasPoints_span').text().trim() != '0' ? j('#bottom_button_gf').removeClass('dsp_none').show() : j('#bottom_button_gf').addClass('dsp_none').hide();
	  }else{
		j('#'+filterType+'Hidden').val() != '' || j('#'+filterType+'PreHidden').val() != '' ? j('#bottom_button_gf').removeClass('dsp_none').show() : j('#bottom_button_gf').addClass('dsp_none').hide();
	  }
	  j.fn.setHeightForFiltBoxGf();
	}
	
	j.fn.setHeightForFiltBoxGf = function(){
		// Scrollbar height
		var filterBlock = j('#filterOpenType').val() + 'Block';
		/*if(j('#pageName').val() == 'Institution Profile' || j('#pageName').val() == 'Course Details'){
			filterBlock = 'filterSlider';
		}*/
		var wuWidth = document.documentElement.clientWidth;
		var wuHeight = document.documentElement.clientHeight;
		var pageType = j('#GAMPagename').val().trim();
		if(pageType != 'clearingbrowsemoneypage.jsp' && pageType != 'clearingcoursesearchresult.jsp'){
			wuHeight = wuHeight + 0;
	    }
		var topFiltHeight = j('#' + filterBlock).find("#fltr_hd_id").outerHeight() + 29;
		if(pageType == 'clearingbrowsemoneypage.jsp' || pageType == 'clearingcoursesearchresult.jsp'){
			//topFiltHeight = topFiltHeight + 96;
	    }
		var btmHeight = j("#bottom_button_gf").innerHeight() - 120;
		if(j('#bottom_button_gf').is(':visible')){
			btmHeight = j("#bottom_button_gf").innerHeight();
		}
		var scrollheight = wuHeight - topFiltHeight - btmHeight + 18;
		// For Devices
		if(wuWidth <= 480){
			scrollheight = wuHeight - topFiltHeight - btmHeight;
		}

		if(wuWidth > 480 && wuWidth <= 992){
			scrollheight = wuHeight - topFiltHeight - btmHeight;
		}
		var innerHg = j.fn.getInnerHg();
		if( scrollheight < innerHg){
		  j(".fltr_cntr .fltr_hd .stc_hd").addClass("flrt_bshd");
		}
		else{
		  j(".fltr_cntr .fltr_hd .stc_hd").removeClass("flrt_bshd");
		}
		j(".slimScrollDiv .custm_scrl").height(scrollheight);
		j(".slimScrollDiv .custm_scrl").css('max-height', scrollheight);
	}
	//
	j.fn.getInnerHg = function(){
		  var totalHg = 0;
		  if(j('#filterOpenType').val() == 'yourGrades'){
			totalHg += j('#additional_qual_0') != undefined ? j('#additional_qual_0').height() : 0;
			totalHg += j('#additional_qual_1') != undefined ? j('#additional_qual_1').height() : 0;
			totalHg += j('#additional_qual_2') != undefined ? j('#additional_qual_2').height() : 0;
			totalHg += j('#additional_qual') != undefined ? j('#additional_qual').height() : 0;
		  }
		  return totalHg;
		}
	//ucas qualification

	//setup before functions
	var typingTimer;                //timer identifier
	var doneTypingInterval = 250;  //time in ms,
	var $input = j('#ucas_0');	
	j('body').on('keypress', '#ucas_0,#ucas_1,#ucas_2', function(){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode > 31 && (keycode < 48 || keycode > 57)){
        	return false;
        }                
        return true;
    });
	//on keyup, start the countdown	
	j('body').on('keyup', '#ucas_0,#ucas_1,#ucas_2', function(){		  
	  clearTimeout(typingTimer);
	  typingTimer = setTimeout(doneTyping, doneTypingInterval);	
	  if(wudWidth <= 480){	
		j('#bottom_button_gf').hide();
	  }
	});

	//on keydown, clear the countdown 
	j('body').on('keydown', '#ucas_0,#ucas_1,#ucas_2', function(){		
	  clearTimeout(typingTimer);
	  if(wudWidth <= 480){	
		j('#bottom_button_gf').hide();
	  }	    
	});
	
	//user is "finished typing," do something
	function doneTyping () {
		//alert('typing');
		if(wudWidth <= 480){	
			j('#bottom_button_gf').hide();
		}
		j('#errorMsgGrd').hide();
		j('#errorMsgGrd').text('');
		//j("[data-grade-option-flag]").trigger( "click" );
		var gradeOptionFlag = j(this).attr('data-grade-option-flag');
		filterValClicked = 'Y';
		if(gradeOptionFlag == 'Y'){
			var id=  j(this).attr('data-load-qual-order-option');
			var num =  j(this).attr('data-grade-index');
			var value = j(this).attr('data-grade-index-type');
			var totalMaxPoint = j(this).attr('data-grade-total-max-point');
			var maxPoint = j(this).attr('data-grade-max-point');
			var replaceVal = 0;
			if(j.fn.validateGradeFilterPoints(id,num,value,totalMaxPoint,maxPoint)){
				replaceVal += parseInt(j('#point_'+id+'_'+num).text()) + parseInt(value);
				replaceVal = replaceVal < 0 ? 0 : replaceVal;
				if(replaceVal > 0){
					j('#grd_prnt_id_'+id+'_'+num).addClass("grd_drk");
				} else{
					if(j('#grd_prnt_id_'+id+'_'+num).hasClass('grd_drk')){
						 j('#grd_prnt_id_'+id+'_'+num).removeClass("grd_drk");
					}
				}			  
				j('#point_'+id+'_'+num).text(replaceVal);
			}
		//}else if(j('#grade-option-flag-'+index).val() == 'T'){
	 			 		
		//}else if(j('#grade-option-flag-'+index).val() == 'D'){
		  
		}else {
		  var clrIdVal=  j(this).attr('data-load-qual-order-option');
		  j('#grade_filter_'+clrIdVal).find('a').removeClass('slct_chip');
		  j(this).addClass('slct_chip');
		  j('#total_points_'+clrIdVal).val(j(this).attr('data-grade-points'));
		}
		var arr = ['1','2','3']
		var isValueSelected = 'N';
		j.each(arr, function(index){
	    var updatePoint = 0;	
		if(j('#grade-option-flag-'+index).val() == 'N' && j('#grade-option-flag-'+index).val() != undefined){
	 		 j('#grade_filter_'+index).find('a').each(function(indexInner){
	 			 if(j(this).hasClass('slct_chip')){
	 				updatePoint =  parseInt(j(this).attr('data-grade-points'));
	 				isValueSelected = 'Y';
	 			 }
	 		 });
	 		j('#total_points_'+index).val(updatePoint);
	 	}else if(j('#grade-option-flag-'+index).val() == 'T' && j('#grade-option-flag-'+index).val() != undefined){
	 	  var ucasQualPoint = j('#ucas_'+index).val().trim()
	 	  if(ucasQualPoint != '' && ucasQualPoint > 0 && ucasQualPoint < 1000 && (ucasQualPoint.match(numbers))){
			  j('#total_points_'+index).val(parseInt(ucasQualPoint));
		  }else{
		  	j('#total_points_'+index).val(0);
		  }    	 		
		}else if(j('#grade-option-flag-'+index).val() == 'D' && j('#grade-option-flag-'+index).val() != undefined){
			  
		}else if(j('#grade-option-flag-'+index).val() != undefined){
	 		j('#grade_filter_'+index).find('span').map(function(indexInner){
	 			if(parseInt(j('#point_'+index+'_'+indexInner).text().trim()) > 0){
	 				isValueSelected = 'Y';
	 			}
	 			updatePoint +=  (parseInt(j('#point_'+index+'_'+indexInner).text().trim()) * parseInt(j(this).next().attr('data-grade-points')));
			});  
	 		j('#total_points_'+index).val(updatePoint);
	 	  }
		});
		j.fn.updatePoints();	
		if(wudWidth <= 480){	
			j('#bottom_button_gf').hide();
		}else{
			j.fn.showAndHideApplyAndClearBtn(isValueSelected);			
		}
		
	}

	//
	
});
j.fn.saveGradeFilter = function (formURL,pod){		 
	gradeFilterData.ajaxForNull = 'Y';
	j('#loaderTopNavImg').show();
	  j.ajax({
		type : "POST",
		url : contextPathGf + '/save-grade-filter-info.html',
		data : gradeFilterData,
		cache : true,
		success : function(result) {
		  j('#loaderTopNavImg').hide();
		  if(pod == 'ucas_cal'){
			  var uacsPt = j('#ucasPoints_span').text().trim();
			  var podType = j('#callPodName').val().trim();
			  j('#ucasPoint').val(uacsPt);
			  var eventLbl = uacsPt + '|'  + gradeFilterData.userStudyLevelEntry;
			  ga('set', 'dimension15', uacsPt);
			  if(podType == 'cdpod'){
				  GANewAnalyticsEventsLogging('filter', 'clearing-cd-ucas', eventLbl);
			  //}else if(podType == ''){
				  //GANewAnalyticsEventsLogging('Filter', 'clearing-pr-ucas', eventLbl);
			  }else if(podType == 'homePod' || podType == 'topNav'){
				  GANewAnalyticsEventsLogging('filter', 'clearing-search-ucas', eventLbl);  
			  }			  			  
			  if(podType == 'topNav'){
				  var callUserType = j('#callUserType').val();
				  if(callUserType == 'CLEARING'){
					  callSubLandPage();
				  }else if(callUserType != 'CLEARING'){
					  showComSearchPopup();
				  }
			  }else if(podType == 'cdpod'){j('#loaderTopNavImg').show();location.reload();}
		  }else{
			  var uacsPt = j('#ucasPoints_span').text().trim();
			  var eventLbl = uacsPt + '|'  + gradeFilterData.userStudyLevelEntry;
			  var podType = j('#pageNameGf').val().trim().toLowerCase();
			  ga('set', 'dimension15', uacsPt);
			  if(podType == 'clearingbrowsemoneypage.jsp'){
				GANewAnalyticsEventsLogging('filter', 'clearing-search-ucas', eventLbl);
			  }else if(podType == 'clearingcoursesearchresult.jsp'){
				  GANewAnalyticsEventsLogging('filter', 'clearing-pr-ucas', eventLbl);
			  }			  
			  j('#loaderTopNavImg').show();
			  window.location.href = formURL;  
		  }		  
		}
		});
	}
j.fn.clearSessionGradeFilter = function (formURL){	
	var inputArr = {}
	j('#loaderTopNavImg').show();
	inputArr.ajaxForNull = 'Y';
	  j.ajax({
		type : "POST",
		url : contextPathGf + '/delete-grade-filter-info.html',
		data : inputArr,
		cache : true,
		success : function(result) {
		  if(formURL != 'no_redirect'){			  
			j('#loaderTopNavImg').hide();
			j('#loaderTopNavImg').show();
			window.location.href = formURL;
		  }			  
		}
		});
	}
//common fn for URL formation
j.fn.formatUrl = function(url){
	var formattedUrl = url;
	if (formattedUrl != null) {
		if (formattedUrl.indexOf("?&")) {
			formattedUrl = formattedUrl.replace("?&", "?");
		}
		if (formattedUrl.indexOf("?") == formattedUrl.length - 1) {
			formattedUrl = formattedUrl.replace("?", "");
		}
		if (formattedUrl.indexOf("&") == formattedUrl.length - 1) {
			formattedUrl = formattedUrl.replace("&", "");
		}
	}
	return formattedUrl;
}
function heDipDropDownGen(crdIdx,creditVal,crdTypePts,crdType,crdTypeOrd){				
	var heReqPoint =  j('#heReqPoint_'+crdIdx).val();
	var dpt = parseInt(j('#qual_prepopulate_D_credit_'+crdIdx+'_span').text().replace(/credits/gi, '').trim());
	var mpt = parseInt(j('#qual_prepopulate_M_credit_'+crdIdx+'_span').text().replace(/credits/gi, '').trim());
	var ppt =parseInt(j('#qual_prepopulate_P_credit_'+crdIdx+'_span').text().replace(/credits/gi, '').trim());
	 MPDpPt = mpt + ppt;
	 genCreditOptionD('D_credit_'+crdIdx,null,MPDpPt,true,crdIdx); //grade_option_M_credit_0 ul
	 DPDpPt = dpt + ppt;
	 genCreditOptionD('M_credit_'+crdIdx,null,DPDpPt,true,crdIdx); 
	 DMDpPt = dpt + mpt;
	 genCreditOptionD('P_credit_'+crdIdx,null,DMDpPt,true,crdIdx);
	 var curTotPt = dpt+mpt+ppt;
     j('#heCrdPoint_'+crdIdx).val(curTotPt);
     j('#heRemainPoint_'+crdIdx).val(heReqPoint - curTotPt);	
     if(j('#errorMsgGrd').is(':visible') && (curTotPt == 45 || curTotPt == 0)){
       j('#errorMsgGrd').hide();
	 }
  }	
function genCreditOptionD(creditType,totalPt,dpLim,optLimFlag,crdIdx){
 var crdType = '';
 var crdTypeId = '';
 var crdTypeIdVal = '';
 var optLim = 45;
 var crdTypeVal = '';
 if(creditType == 'D_credit_'+crdIdx){crdType = 'D'; crdTypeIdVal = '3.2';}
 if(creditType == 'M_credit_'+crdIdx){crdType = 'M'; crdTypeIdVal = '2.1333';}
 if(creditType == 'P_credit_'+crdIdx){crdType = 'P'; crdTypeIdVal = '1.0666';}
 if(optLimFlag){optLim = 45 - dpLim};			 
 var options =  ''; 
 for(var i=0; (i <= 45 && i <= optLim); i += 3){ 			     
   options += '<li class="ajaxlist" data-credit-index="'+crdIdx+'" data-grade-option-flag="D" data-load-hedip-id-value="'+crdTypeIdVal+'" data-load-hedip-id="'+crdType+'" data-load-credit-order-option="'+crdType+'_credit_'+crdIdx+'" value="'+Math.round(i * crdTypeIdVal)+'"><a href="javascript:void(0);">'+i+' credits</a></li>'
 }			    
 j('#grade_option_'+creditType).html(options);//replace the selection's html with the new options //grade_option_M_credit_0 ul eg
	  
}
j('body').on('click', '.closebtn', function(event){
  var filterOpenType = j('#filterOpenType').val();
  if(filterOpenType == 'yourGrades'){
	  j("#filterSlider").hide();
	  j('#filterPodName').val('');
	  var root = document.getElementsByTagName( 'html')[0];   
	  root.className = '';
  }	
});
function closeNav(){
	j("#filterSlider").hide();
	j('#filterPodName').val('');
	var root = document.getElementsByTagName( 'html')[0];   
    root.className = '';
}
j('body').on('click', '.clr_fltr .overlay', function(event){
	closeNav();
});
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
function removingOldGradeParam(formURL){
  var oldStudylevel = 	getParameterByName('studylevel');
  var oldEntrypoints = 	getParameterByName('entrypoints');
  var oldPoints = 	getParameterByName('score');
  if(oldStudylevel!=''&&oldStudylevel!=null){
	  oldStudylevel = '&studylevel='+ oldStudylevel;
	  formURL = formURL.replace(oldStudylevel,'');	  
  }
  if(oldEntrypoints!=''&&oldEntrypoints!=null){
	  oldEntrypoints = '&entrypoints='+ oldEntrypoints;
	  formURL = formURL.replace(oldEntrypoints,'');	  
  }
  if(oldPoints!=''&&oldPoints!=null){
	  oldPoints = '&score='+ oldPoints;
	  formURL = formURL.replace(oldPoints,'');	  
  }
  return formURL;
}
function getUcasCalculator(podName){	
	j.fn.loadGradeFilter(this,'yourGrades');
	j('#filterPodName').val('ucas_cal');
	closeLigBox();
	//getUcasCalculator('cdpod')
	podType = podName;
	j('#callPodName').val(podName);
}
function closeheDp(mainQualHide,except){
  for(var i = 0; i < 3 ; i++){			  
	if(!mainQualHide){			
	  j('#grade_option_D_credit_'+i).addClass('dsp_none').hide();		 
	  j('#grade_option_D_credit_'+i).parent('.slimScrollDiv').addClass('dsp_none').hide();			 
	  j('#grade_option_M_credit_'+i).addClass('dsp_none').hide();
	  j('#grade_option_M_credit_'+i).parent('.slimScrollDiv').addClass('dsp_none').hide();		 		
	  j('#grade_option_P_credit_'+i).addClass('dsp_none').hide();
	  j('#grade_option_P_credit_'+i).parent('.slimScrollDiv').addClass('dsp_none').hide();		 
	  if(except != i){
		  j('#grade_option_'+i).addClass('dsp_none').hide(); 				 
		  j('#grade_option_'+i).parent('.slimScrollDiv').addClass('dsp_none').hide();
	  }			  		
	}
	if(mainQualHide){
		j('#grade_option_'+i).addClass('dsp_none').hide(); 				 
		j('#grade_option_'+i).parent('.slimScrollDiv').addClass('dsp_none').hide();				
	}	  
		  
  }
}
function onKeyboardOnOff(isOpen) {
    // Write down your handling code
  if(wudWidth <= 480){
    if (isOpen) {
        // keyboard is open
    	//alert('open');
    	j('#bottom_button_gf').hide();
    } else {
        // keyboard is closed
    	//alert('closed');
      j.fn.showAndHideApplyAndClearBtn();      
  	  j.fn.setHeightForFiltBoxGf();  	  
    }
   }
}

var originalPotion = false;
j(document).ready(function(){
    if (originalPotion === false) originalPotion = j(window).width() + j(window).height();
});

/**
 * Determine the mobile operating system.
 * This function returns one of 'iOS', 'Android', 'Windows Phone', or 'unknown'.
 *
 * @returns {String}
 */
function getMobileOperatingSystem() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

      // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "winphone";
    }

    if (/android/i.test(userAgent)) {
        return "android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return "ios";
    }

    return "";
}

function applyAfterResize() {

    if (getMobileOperatingSystem() != 'ios') {
        if (originalPotion !== false) {
            var wasWithKeyboard = j('body').hasClass('view-withKeyboard');
            var nowWithKeyboard = false;

                var diff = Math.abs(originalPotion - (j(window).width() + j(window).height()));
                if (diff > 100) nowWithKeyboard = true;

            j('body').toggleClass('view-withKeyboard', nowWithKeyboard);
            if (wasWithKeyboard != nowWithKeyboard) {
                onKeyboardOnOff(nowWithKeyboard);
            }
        }
    }
}

