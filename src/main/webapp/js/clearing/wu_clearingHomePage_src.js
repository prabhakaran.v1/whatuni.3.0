var jq = jQuery.noConflict();
var splitCharacter = "###";
var clearingSrUrlq = "/degree-courses/search?clearing&q=";
var clearingSrUrlSub = "/degree-courses/search?clearing&subject=";
var srch_cur_let = new Array();
var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
var deviceWidth = null;

function getDeviceWidth() {
    return (window.innerWidth > 0) ? window.innerWidth : screen.width;
}

function isNotUNE(value) {
    return (!_.isNull(value) && !_.isUndefined(value) && !_.isEmpty(value));
}

function doc(id) {
    return document.getElementById(id);
}

jq(document).ready(function() {

	 var clearingHome = jq("#clearingHome").val();
	 if(clearingHome == "CLEARINGHOME") {
       lazyloadetStarts();
	 }

    jq(document).on("click", "#srchUlId li", function(e) {
        var contents = jq('.tab-content');
        var clickedEleId = jq(this).attr('data-tab');
        var target = jq("#" + clickedEleId);
        jq(".tab-link").not(this).removeClass('current');
        jq(this).addClass('current');
        target.addClass('current');
        contents.not(target).removeClass('current');
        jq("#ajax_listOfOptions_clearing").remove();
        jq('#clearing_locDrpDnId').hide()
        jq('#clearing_errMsgSubSrchlbox').hide();
    });

    jq('body').on('keypress', '#topClearingSubjectName', function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == 13) {
            clearingSRUrl();
        }
    });
    
    jq('body').on('keydown', '#topClearingSubjectName', function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode != 13 && keycode != 9) {
            var subjectName_hidden = jq("#subjectName_hidden").val();
            var subjectName_urlHidden = jq("#subjectName_urlHidden").val();
            if (isNotUNE(subjectName_hidden) && isNotUNE(subjectName_urlHidden)) {
                jq("#subjectName_hidden").val("");
                jq("#subjectName_urlHidden").val("");
            }
        }
    });

    jq('body').on('keypress', '#ucasPoint', function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode > 31 && (keycode < 48 || keycode > 57))
            return false;

        return true;
    });

    var clearingHome = jq("#clearingHome").val();
    //if(clearingHome == "CLEARINGHOME") {  
	    var oldonkeydownWu = document.body.onkeydown;
	    if (typeof oldonkeydownWu != 'function') {
	        document.body.onkeydown = ajaxOptionNavigation;
	    } else {
	        document.body.onkeydown = function() {
	            oldonkeydownWu();
	            ajaxOptionNavigation();
	        }
	     }
     //} 
});

function clearingSRUrl() {

    var subject = jq('#topClearingSubjectName').val().trim();
    if (_.isEmpty(subject)) {
        jq('#clearing_errMsgSubSrchlbox').children('p').text('Please enter subject');
        jq('#clearing_errMsgSubSrchlbox').show();
    } else {
        var subjectHid = jq('#subjectName_hidden').val();
        var subjectUrlHid = jq('#subjectName_urlHidden').val();

        if (isNotUNE(subjectHid) && isNotUNE(subjectUrlHid)) {

             clearingSrUrlSub += subjectUrlHid;

            var locationValue = jq('#locationUrl_hidden').val();
            if (isNotUNE(locationValue)) {
                clearingSrUrlSub += "&location=" + locationValue;
            }
            var ucasPoint = jq('#ucasPoint').val().trim();
            if (!_.isEmpty(ucasPoint)) {
                clearingSrUrlSub += "&score=" + ucasPoint;
            }
            jq('#loaderTopNavImg').show();
            window.location.href = clearingSrUrlSub.toLowerCase();

        } else {
            var keywordSearch = jq('#topClearingSubjectName').val();
            var keywordmatchBrowseCatId = jq('#keywordmatchBrowseCatId').val();
            var clearingSrUrlFinal = "";
            keywordSearch = keywordSearch.replace(reg, " ");
            keywordSearch = keywordSearch.trim();
            keywordSearch = replaceAll(keywordSearch, " ", "-");
            if (!isNotUNE(keywordSearch)) {
                jq('#clearing_errMsgSubSrchlbox').children('p').text('Please enter valid subject');
                jq('#clearing_errMsgSubSrchlbox').show();
                return;
            }

            if (isNotUNE(keywordmatchBrowseCatId) ? clearingSrUrlFinal = clearingSrUrlSub : clearingSrUrlFinal = clearingSrUrlq);

            clearingSrUrlFinal += keywordSearch;

            var locationValue = jq('#locationUrl_hidden').val();
            if (isNotUNE(locationValue)) {
                clearingSrUrlFinal += "&location=" + locationValue;
            }
            var ucasPoint = jq('#ucasPoint').val().trim();
            if (!_.isEmpty(ucasPoint)) {
                clearingSrUrlFinal += "&score=" + ucasPoint;
            }
            jq('#loaderTopNavImg').show();
            window.location.href = clearingSrUrlFinal.toLowerCase();
        }
    }
}

/*
 * This function triggers the ajax event on subject and university --- starts  
 */
function ajaxList(event, id, resultDiv, divId) {
    var url = "";
    var keywordSearch = jq('#' + id).val().trim();
    keywordSearch = keywordSearch.replace(reg, " ");
    keywordSearch = keywordSearch.trim();
    var subjectName_hidden = "";
    if (srch_cur_let[id] == keywordSearch) {
        return;
    }
    srch_cur_let[id] = keywordSearch;
    if (id == "topClearingSubjectName") {
        subjectName_hidden = jq('#subjectName_hidden').val();
        url = "/degrees/ajax/clearing-courseSubject.html?subjectKeywordSearch=" + keywordSearch;
    } else {
        url = "/degrees/ajax/clearing-university.html?uniKeywordSearch=" + keywordSearch;
    }

    
    if (jq('#ajax_listOfOptions_clearing').length === 0) {
        jq("#" + divId).append("<div id='ajax_listOfOptions_clearing' class='ajx_cnt ajx_res comn_dd' style='display:none;'></div>");
    }

    if (isNotUNE(keywordSearch) && (keywordSearch.length > 2) && _.isEmpty(subjectName_hidden)) {
        var urlOpen = url;
        jq.post(urlOpen, function(data) {
            jq('#clearing_errMsgSubSrchlbox').hide()
            jq("#" + resultDiv).html(data);
            if (jq('#freeText').length) {
                jq('#freeText').text('"' + keywordSearch + '"');
                keywordSearch = replaceAll(keywordSearch, " ", "-");
                var clearingSrUrlqFinal = clearingSrUrlq + keywordSearch;
                jq("#keyWordSearch").attr("href", clearingSrUrlqFinal.toLowerCase());
            }
            var ajxwd = jq('#' + id).outerWidth() + 'px';
            var ajxht = doc(id).offsetHeight + 'px';
            jq("#" + resultDiv).css('top', ajxht);
            jq("#" + resultDiv).css('width', ajxwd);
            jq("#" + resultDiv).show();
        });
    } else {
        jq('#' + resultDiv).hide();
    }
}

function ajaxDrpDownSelect(id, selectedValue, url) {
    jq('#' + id).val(selectedValue);
    jq('#' + id).blur();
    jq('#ajax_listOfOptions_clearing').hide();
    jq('#clearing_locDrpDnId').hide();

    if (id == "topClearingSubjectName" && isNotUNE(url) && isNotUNE(selectedValue)) {
        jq('#subjectName_hidden').val(selectedValue);
        jq('#subjectName_urlHidden').val(url);
    } else {
        jq('#locationUrl_hidden').val(url);
    }
}

function ajaxDrpDownSelectUni(url) {
    window.location.href = url;
    jq('#loadingImg').show();
}

function clearingShowHideLocDrpDn(id) {
    
   if (jq('#'+id).is(':visible')) {
        jq('#'+id).hide();
   } else {
       jq('#'+id).show();
  }
}

/*
 * This function will be called when mouse event happens outside the container --- starts  
 */
jq(document).mouseup(function(e) {
    if (jq('#tab-1').hasClass('current') || (jq('#tab-4') && jq('#tab-4').hasClass('current'))) {
        hideAjax(e, "clearing_subSrchDivId", 'ajax_listOfOptions_clearing');
    } else {
        hideAjax(e, "uniDivId", 'ajax_listOfOptions_clearing');
    }
    hideAjax(e, "clearing_locSrchDivId", 'clearing_locDrpDnId');
});

function hideAjax(e, id, hideDiv) {
    var container = jq("#" + id);
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        jq('#' + hideDiv).hide();
    }
}

/*
 * This function will be called when scrolling happens --- starts
 */
jq(window).scroll(function() {
    var clearingHome = jq("#clearingHome").val();
    if(clearingHome == "CLEARINGHOME") {
    clearingHomeOnScrollEvent();
   }
});

function clearingHomeOnScrollEvent() {
    var featuredUniId = jq('#featuredUni');
    var dataId = "data-id";
    var statsNotLogged = "stats_not_logged";
    var statsLogged = "stats_logged";

    if (featuredUniId.length && isNotUNE(featuredUniId)) {
        if (isScrollIntoView(featuredUniId, false) && featuredUniId.attr(dataId) == statsNotLogged) {
            var liCount = jq('ul#ulFeaturedUni li').length;
            var collegeId = jq('#collegeIdValues').val();
            var collegeName = jq('#collegeNameValues').val();
            var orderItemId = jq('#orderItemIdHomeFeatured').val();

            var collegeIDArr = _.split(collegeId, splitCharacter, liCount);
            var collegeNameArr = _.split(collegeName, splitCharacter, liCount);
            var orderItemIdArr = _.split(orderItemId, splitCharacter, liCount);
            for (var i = 0; i < liCount; i++) {
                advertiserExternalUrlStatsLogging('clearingFeaturedProvidersBoosting', collegeIDArr[i], collegeNameArr[i], orderItemIdArr[i] );
            }

            featuredUniId.attr(dataId, statsLogged);
        }
    }
}

function isScrollIntoView(element, fullyInView) {
    var pageTop = jq(window).scrollTop();
    var pageBottom = pageTop + jq(window).height();
    var elementTop = jq(element).offset().top;
    var elementBottom = elementTop + jq(element).height();
    if (fullyInView === true) {
        return ((pageTop < elementTop) && (pageBottom > elementBottom));
    } else {
        return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
    }
}
/*
 * This function will be called when navigation using keyboard happens --- starts 
 */
function ajaxOptionNavigation(e) {
    var ajaxOptionObj = jq('#ajax_listOfOptions_clearing').get(0);
    var ajaxListActiveItem = jq('#ajax_listOfOptions_clearing').find('.optionDivSelected').get(0);
    
    if (!ajaxOptionObj) {
        return;
    }
    if (ajaxOptionObj.style.display == 'none') {
        return;
    }
    
    if (document.all) e = event;
    e = e == undefined ? event : e;
    var divId = e.srcElement.id;
    

    if (e.keyCode == 38) {
        if (!ajaxListActiveItem) {
            return;
        }
        if (ajaxListActiveItem && !ajaxListActiveItem.previousElementSibling) {
            return;
        }
        if (ajaxListActiveItem.previousElementSibling) {
            ajaxListActiveItem.className = 'optionDiv';
            ajaxListActiveItem.previousElementSibling.className = 'optionDivSelected';
        }
    }
    if (e.keyCode == 40) {
        if (!ajaxListActiveItem) {
            var firstOptDiv = jq('#ajax_listOfOptions_clearing').find('.optionDiv').first().get(0);
            if (!_.isUndefined(firstOptDiv)) {
                firstOptDiv.className = 'optionDivSelected';
            }
        } else {
            if (!ajaxListActiveItem.nextSibling) {
                return;
            }
            if (ajaxListActiveItem.nextElementSibling) {
                ajaxListActiveItem.className = 'optionDiv';
                ajaxListActiveItem.nextElementSibling.className = 'optionDivSelected';
            }
        }
    }
    if (e.keyCode == 13 || e.keyCode == 9) {
        if (ajaxListActiveItem && ajaxListActiveItem.className == 'optionDivSelected') {
            var selOptObj = jq('#ajax_listOfOptions_clearing').find('.optionDivSelected').get(0);
            if (_.isEqual(divId, "topClearingSubjectName")) {
                ajaxDrpDownSelect('topClearingSubjectName', selOptObj.innerText, selOptObj.childNodes[1].firstElementChild.innerText);
            } else {
                var uniAjaxText = selOptObj.textContent.trim();
                if (!_.includes(uniAjaxText, "No clearing courses available on Whatuni")) {
                    var uniUrl = _.trim(uniAjaxText, selOptObj.innerText);
                    ajaxDrpDownSelectUni(uniUrl);
                }
            }
        }
        return true;
    }
    if (e.keyCode == 27) {
        ajaxOptHideEscape();
    }
}

function ajaxOptHideEscape() {
    jq('#ajax_listOfOptions_clearing').hide();
    jq('#clearing_locDrpDnId').hide();
}

function getAnswer(index) {
    if (doc("answer_" + index).style.display == "none") {
        doc("answer_" + index).style.display = "block";
        doc("icon_" + index).className = "fa fa-minus-circle";
    } else if (doc("answer_" + index).style.display == "block") {
        doc("answer_" + index).style.display = "none";
        doc("icon_" + index).className = "fa fa-plus-circle";
    }
}
