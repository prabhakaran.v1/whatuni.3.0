var jq = jQuery.noConflict();
var contextPath = "/degrees";
jq.fn.notBlankOrNull = function(input){if(!isNaN(input)){input = input.toString();}if(input != null && input != "" && input != undefined && (input != null && input.trim().length > 0)){return true;}else{return false;}}
var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;

function isNotUNE(value) {
    return (!_.isNull(value) && !_.isUndefined(value) && !_.isEmpty(value));
}

var vidpct = ['', '10%', '20%', '30%', '40%', '50%', '60%', '70%', '80%', '90%', '100%'];
var videoRanMap = {};
var deviceWidth = null;

function getDeviceWidth() {
    return (window.innerWidth > 0) ? window.innerWidth : screen.width;
}

function doc(id) {
    return document.getElementById(id);
}

var retinaSrc = "";
var videoPlayedOnce = false;

jq.fn.urlFormation = function(pageFlag) {
	 var splitedURL = jq("sortingUrl").val(); 
	 var searchUrl = splitedURL.split("?");
	 splitedURL = searchUrl[0];
	 var filterString = splitedURL;
	 if(pageFlag == 'gradefilter'){	 
	   filterString = "";   
	 }
	 filterString += jq.fn.notBlankOrNull("isclearingPage") ?  "?clearing" : ""; 
	 
	 filterString += checkNull("hidden_score") ? "&score="+$$D("hidden_score").value : "";
	 searchURL = filterString; 
	 return searchURL;
}

jq.fn.formatUrl = function(url){
  var formattedUrl = url;
  if (formattedUrl != null) {
    if (formattedUrl.indexOf("?&")) {
	  formattedUrl = formattedUrl.replace("?&", "?");
	}
	if (formattedUrl.indexOf("?") == formattedUrl.length - 1) {
	  formattedUrl = formattedUrl.replace("?", "");
	}
  }
  return formattedUrl;
}

jq(document).ready(function() {
    jq('.ajax_ul').slimscroll({});
    jq(".srt1").click(function(){
	     if(jq("#sortBy").is(':visible')){
	       jq("#sortBy").hide();
	     } else {
	       jq("#sortBy").show();
	     }
	  });
    
    var userUcasPoint = jq("#userUcasPoint").val();

    if(_.isEmpty(userUcasPoint)){
    	jq("#gradeOptionToolTip").show();
    	jq('#gradeOption').addClass('wuclrr_bactv_hvr');
    }
    
    jq("#chanceOfAcceptanceId").hover(function(){
      jq("#chanceofAccepatnceTooltip").show();
      }, function(){
       jq("#chanceofAccepatnceTooltip").hide();
    });

     jq("#closeChanceOfAcceptance").click(function(){
         jq("#chanceofAccepatnceTooltip").hide();
         return false;
    });
    
     jq("#gradeOption").hover(function(){
         jq("#gradeOptionToolTip").show();
         jq('#gradeOption').addClass('wuclrr_bactv_hvr');
         }, function(){
          jq("#gradeOptionToolTip").hide();
          jq('#gradeOption').removeClass('wuclrr_bactv_hvr');
     });
 
     jq("#closeGradeOption").click(function(){
         jq("#gradeOptionToolTip").hide();
         return false;
    });

});

jq(window).on('load', function() {
	retina();	
});

jq.fn.setHeightForFiltBox = function(){
	// Scrollbar height
	var filterBlock = jq('#filterOpenType').val() + 'Block';
	if(jq('#pageName').val() == 'Institution Profile' || jq('#pageName').val() == 'Course Details'){
		filterBlock = 'filterSlider';
	}
	var filterOpenType = jq('#filterOpenType').val();
	var pageWidth = document.documentElement.clientWidth;
	var pageHeight = document.documentElement.clientHeight;
	var topFiltHeight;
	if(filterOpenType != ''){
	 topFiltHeight = jq('#'+filterOpenType).find(".fltr_hd").outerHeight();
	}
	topFiltHeight = 97; 
	//console.log("topFiltHeight-->"+topFiltHeight);
	
	var btmHeight = jq(".btm_pod").innerHeight() - 120;
	if(jq('.btm_pod').is(':visible')){
		btmHeight = jq(".btm_pod").innerHeight();
	}
	
	//console.log("btmHeight-->"+btmHeight);
	var scrollheight = pageHeight - topFiltHeight - btmHeight - 30;
	//console.log("scrollheight-->"+scrollheight);
	// For Devices
	if(pageWidth <= 480){
		scrollheight = pageHeight - topFiltHeight - btmHeight;
	}

	if(pageWidth > 480 && pageWidth <= 992){
		scrollheight = pageHeight - topFiltHeight - btmHeight;
	}
	var innerHg = jq.fn.getInnerHg();
	if( scrollheight < innerHg){
	  jq(".grd_fltr .stk_pod").addClass("flrt_bshd");
	}
	else{
	  jq(".grd_fltr .stk_pod").removeClass("flrt_bshd");
	}
	jq(".slimScrollDiv .slim-scroll").height(scrollheight);
	jq(".slimScrollDiv .slim-scroll").css('max-height', scrollheight);
}
//
jq.fn.getInnerHg = function(){
	  var totalHg = 0;
	  if(jq('#filterOpenType').val() == 'yourGrades'){
		totalHg += jq('#additional_qual_0') != undefined ? jq('#additional_qual_0').height() : 0;
		totalHg += jq('#additional_qual_1') != undefined ? jq('#additional_qual_1').height() : 0;
		totalHg += jq('#additional_qual_2') != undefined ? jq('#additional_qual_2').height() : 0;
		totalHg += jq('#additional_qual') != undefined ? jq('#additional_qual').height() : 0;
	  }
	  return totalHg;
	}

jq('body').on('click', '.closebtn, .clr_fltr .overlay', function(event){
	var filterOpenType = jq('#filterOpenType').val();
	jq.fn.copyFields();
	if(filterOpenType != 'yourGrades'){
	  jq('#clr_fltr, #sidenav, #'+filterOpenType).hide();
	  //jq.fn.urlFormation();
	  jq('#filterOpenType').val('');
	  var root = document.getElementsByTagName( 'html')[0];   
	    root.className = '';
	}
});


jq.fn.urlFormation = function(pageFlag) {
	 var splitedURL = jq("#pageUrl").val(); 
	 var qualification = jq('#qualificationHidden').val();
	 var university = jq('#universityHidden').val();
	 qualification = jq.fn.notBlankOrNull(qualification) ? qualification : jq.fn.notBlankOrNull(university) ? 'all' : 'degree';
	 var searchUrl = contextPath +'/';//splitedURL.split("?");
	 searchUrl += qualification + "-courses/"+((jq.fn.notBlankOrNull(university)) ? "csearch.html?" : "search.html?");
	 //splitedURL = searchUrl[0];
	 var filterString = searchUrl;
	 if(pageFlag == 'gradefilter'){	 
	   filterString = "";   
	 }
	 filterString += 'clearing';
	 //filterString += jq.fn.notBlankOrNull("isclearingPage") ?  "?clearing" : "";
	 var subject = jq('#subjectHidden').val();
	 var region = jq('#regionHidden').val();
	 var studyMode = jq('#studyModeHidden').val();
	 var hybrid = jq('#hybridHidden').val();
	 var locationType = jq('#locationTypeHidden').val();
	 var acceptance = jq('#acceptanceHidden').val();
	 
	 var keyword = jq('#keywordHidden').val();
	 var campus = jq('#campusHidden').val();
	 var scholarship = jq('#scholarshipHidden').val();
	 var accommodation = jq('#accommodationHidden').val();
	 var score = jq('#scoreHidden').val();
	 var sortBy = jq('#sortHidden').val();
	 //var pageNo = jq('#pageNoHidden').val();
	// jq.fn.notBlankOrNull(regin) ? 
	 
	 filterString += (jq.fn.notBlankOrNull(subject)) ? "&subject=" + subject : (jq.fn.notBlankOrNull(keyword)) ? "&q=" + keyword : "";
	 filterString += (jq.fn.notBlankOrNull(university)) ? "&university=" + university : "";
	 filterString += ((jq.fn.notBlankOrNull(region)) && (region != 'united-kingdom') && !(jq.fn.notBlankOrNull(university))) ? "&location=" + region : "";
	 filterString += (jq.fn.notBlankOrNull(locationType) && !(jq.fn.notBlankOrNull(university))) ? "&location-type=" + locationType : "";
	 filterString += (jq.fn.notBlankOrNull(studyMode)) ? "&study-mode=" + studyMode : "";
	 //filterString += (jq.fn.notBlankOrNull(qualification)) ? "&qualification=" + qualification : "";
	 filterString += (jq.fn.notBlankOrNull(campus) && !(jq.fn.notBlankOrNull(university))) ? "&campus-type=" + campus : "";
	 filterString += (jq.fn.notBlankOrNull(scholarship) && !(jq.fn.notBlankOrNull(university))) ? "&scholarship=" + scholarship : "";
	 filterString += (jq.fn.notBlankOrNull(accommodation) && !(jq.fn.notBlankOrNull(university))) ? "&accommodation=" + accommodation : "";
	 filterString += (jq.fn.notBlankOrNull(score)) ? "&score=" + score : "";
	 filterString += (jq.fn.notBlankOrNull(hybrid)) ? "&hybrid=" + hybrid : "";
   filterString += (jq.fn.notBlankOrNull(acceptance)) ? "&acceptance=" + acceptance : "";
	 filterString += (jq.fn.notBlankOrNull(sortBy) && sortBy != 'R' && !(jq.fn.notBlankOrNull(university))) ? "&sort=" + sortBy : "";
	 //filterString += (jq.fn.notBlankOrNull(pageNo) && pageNo != "1" && !(jq.fn.notBlankOrNull(university))) ? "&pageno=" + pageNo : "";
	 
	 if(pageFlag == 'C'){
	   filterString = "";
	   filterString = searchUrl;
	   filterString += 'clearing';
	   filterString += (jq.fn.notBlankOrNull(subject)) ? "&subject=" + subject : (jq.fn.notBlankOrNull(keyword)) ? "&q=" + keyword : ""; 
	   filterString += (jq.fn.notBlankOrNull(university)) ? "&university=" + university : "";
	   filterString += (jq.fn.notBlankOrNull(sortBy) && sortBy != 'R') ? "&sort=" + sortBy : "";
	 }
	 
	 searchURL = filterString;
	 jq('#loaderTopNavImg').show(); 
	 window.location.href = searchURL;
	// return searchURL;
}

jq.fn.applyFilter = function(filterName){
	
}
jq.fn.copyFields = function(){
	jq('#subjectHidden').val(jq('#subjectHiddenUrl').val());
	jq('#regionHidden').val(jq('#regionHiddenUrl').val());
	jq('#studyModeHidden').val(jq('#studyModeHiddenUrl').val());
	jq('#hybridHidden').val(jq('#hybridHiddenUrl').val());
	jq('#locationTypeHidden').val(jq('#locationTypeHiddenUrl').val());
	jq('#acceptanceHidden').val(jq('#acceptanceHiddenUrl').val());
	 
	jq('#keywordHidden').val(jq('#keywordHiddenUrl').val());
	jq('#campusHidden').val(jq('#campusHiddenUrl').val());
	jq('#scholarshipHidden').val(jq('#scholarshipHiddenUrl').val());
	jq('#accommodationHidden').val(jq('#accommodationHiddenUrl').val());
	jq('#scoreHidden').val(jq('#scoreHiddenUrl').val());
	jq('#sortHidden').val(jq('#sortHiddenUrl').val());
	jq('#qualificationHidden').val(jq('#qualificationHiddenUrl').val());
	jq('#universityHidden').val(jq('#universityHiddenUrl').val());
	//jq('#pageNoHidden').val(jq('#pageNoHiddenUrl').val());
}

jq('body').on('click', '#apply a', function(event) {
	var filterOpenType = jq('#filterOpenType').val();	

	jq('#' + filterOpenType).find('a').each(function(index) {
		if (jq(this).hasClass('slct_chip') && jq(this).is(":visible")) {
			var filter = jq(this).attr('data-filter-show');
			var gaValue = jq(this).attr('data-' + filter + '-ga-value');
			if(filter == 'qualification'){
			  var subjectNameurl = jq(this).attr('data-' + filter + '-subject-name-url');
			  jq('#subjectHidden').val(subjectNameurl);
			}			
			if (jq.fn.notBlankOrNull(gaValue)) {
				filterGALogging(gaValue);
			}
		}
	});
	if (filterOpenType != 'acceptanceFilter') {
		jq('#' + filterOpenType).find('input').each(function(index) {
			var filter = jq(this).attr('data-filter-show');
			var gaValue = "";
			if (jq("#" + filter).prop('checked')) {
				gaValue = filter + ',Yes';
			} else {
				gaValue = filter + ',No';
			}
			if (jq.fn.notBlankOrNull(gaValue) && jq.fn.notBlankOrNull(filter)) {
				filterGALogging(gaValue);
			}
		});
	} else {
		var gaValue = "";
		jq('#acceptanceDiv input').each(function(index){
			var value = jq(this).attr('data-acceptance-value');
			  if(jq(this).prop('checked')){
				  gaValue = getAcceptanceValue(value);
			  } 
		});
		if (jq.fn.notBlankOrNull(gaValue)) {
			gaValue = "chance-of-acceptance," + gaValue;
			filterGALogging(gaValue);
		}
	}
	if (jq('#filterOpenType').val() != 'yourGrades') {
		jq.fn.urlFormation();
	}
});

var stretch ="";
var possible ="";
var likely ="";
function getAcceptanceValue(value) {
	if(value.indexOf('likely') > -1){
		likely = 'Very Likely'; 
	 }
	if(value.indexOf('possible') > -1){
		possible = 'Likely'; 
	}
	if(value.indexOf('stretch') > -1){
		stretch = 'Possible'; 
	}	
	return likely+'|'+possible+'|'+stretch;
}

jq('body').on('click', '[data-sort-show]', function(event) {
	if (jq('#filterOpenType').val() != 'yourGrades') {
		var sortBy = jq(this).attr('data-sort-show');
		jq('#sortHidden').val(jq(this).attr('data-' + sortBy + '-value'));
		jq.fn.urlFormation();
	}
});
jq('body').on('click', '[data-filter-show]', function(event){
	var filter = jq(this).attr('data-filter-show');
	if(filter == 'qualification' || filter == 'region' || filter == 'studyMode' || filter == 'hybrid' || filter == 'campus'){
      if(jq(this).hasClass('slct_chip')){
        jq('#'+filter+'Div').find('a').removeClass('slct_chip');
		jq('#' + filter + 'Hidden').val('');
	  }else{
	    jq('#'+filter+'Div').find('a').removeClass('slct_chip');
        jq(this).addClass('slct_chip') ;
	  }	
	}else{
	  jq(this).toggleClass('slct_chip','');	
	}
	jq('#bottom_button').show();
	var value = "";
	var uniName = "";
	if(filter == 'scholarship' || filter == 'accommodation') {
		if(jq('#'+filter).attr('class') == 'slct_chip') {jq('#'+filter).attr('data-'+filter+'-value', 'y');}
		if(jq("#"+filter).prop('checked')) {
			jq('#' + filter + 'Hidden').val('y');
		} else {
			jq('#' + filter + 'Hidden').val('');			
		}		
	} else if(filter == 'acceptance') {
		value = jq('#acceptanceDiv input').map(function(index){
			  if(jq(this).prop('checked')){
				return jq(this).attr('data-'+filter+'-value');
			  } 
		}).get().join(',');
	    jq('#'+filter+'Hidden').val(value);
	} else {
	value = jq('#'+filter+'Div').find('.slct_chip').map(function(index){
		uniName = jq(this).attr('data-'+filter+'-ga-value');
		return jq(this).attr('data-'+filter+'-value');
	  }).get().join(',');
    jq('#'+filter+'Hidden').val(value);
	}
	jq.fn.setHeightForFiltBox();
    if(filter == 'university') {
    	if(jq.fn.notBlankOrNull(uniName)) {
    		var gaValue = 'clearing-university,'+uniName;
        	filterGALogging(gaValue);    		
    	}
    	jq.fn.urlFormation();
    }
});

jq('body').on('click','[data-filter-option]' ,function(event){
  var filterType = jq(this).attr('data-filter-option');
  if(filterType == 'clearAllFilter'){
	    if(jq('#sesUcasPt').val() != ''){j.fn.clearSessionGradeFilter('no_redirect');}
		jq.fn.urlFormation('C');
	}else if(filterType == 'acceptanceFilter' && jq('#acceptanceFilter').children().length == 0){
		
	}else if(filterType != 'yourGrades'){	
	if(filterType == 'universityFilter'){
		jq.fn.getInstituionList(this,filterType);
	}
	if(filterType == 'acceptanceFilter'){
		jq('#acceptanceDiv input').each(function(index){
			var hiddenValue = jq('#acceptanceHidden').val();
			var value = jq(this).attr('data-acceptance-value');
			if(jq.fn.notBlankOrNull(hiddenValue) && hiddenValue.indexOf(value) > -1) {
				jq(this).prop('checked', true);				
			} else {
				jq(this).prop('checked', false);								
			}
		});
	}
	if(filterType != 'universityFilter'){
		jq('#clr_fltr, #sidenav, #'+filterType).show();
		var root = document.getElementsByTagName( 'html')[0];   
	    root.className = 'scrl_dis';
		jq('#bottom_button').hide();
		jq.fn.setHeightForFiltBox();
		jq('#filterOpenType').val(filterType);
	}
	
  }
  //var clsName = jq(this).attr('class');
  if(jq(this).hasClass('btnbg')){
	  jq('#bottom_button').show();
	  jq.fn.setHeightForFiltBox();
  }
});

jq.fn.getInstituionList = function(thisObj, filterType){
  var inputAttr = {};
  //inputAttr.parentQual = j('#parentQual').val();
  jq('#loaderTopNavImg').show(); 
  var url = '/clearing/get-institution-ajax.html'
  jq.ajax({
			type : "POST",
			url : contextPath + url,
	//		data : inputAttr,
			cache : true,
			success : function(result) {				
				jq('#ajax_listOfOptions_university').html(result);
				jq('#loaderTopNavImg').hide(); 
				jq('#ajaxUniversityName').val('');
				jq('#clr_fltr, #sidenav, #'+filterType).show();
				var root = document.getElementsByTagName( 'html')[0];   
			    root.className = 'scrl_dis';
				jq('#bottom_button').hide();
				jq.fn.setHeightForFiltBox();
				jq('#filterOpenType').val(filterType);
				jq('.ajax_ul').slimscroll({});	
				if(jq(thisObj).hasClass('btnbg')){
					  jq('#bottom_button').show();
					  jq.fn.setHeightForFiltBox();
				  }	
			}
	  });
	}
jq('body').on('click', '#clear', function(event) {
	jq('.grd_chips').find('a').each(function(index) {
		if (jq(this).hasClass('slct_chip') && jq(this).is(":visible")) {
			var filter = jq(this).attr('data-filter-show');
			if (filter == 'subject') {
				jq('#' + filter + 'Div').find('a').hide();
			}
			jq('#' + filter + 'Div').find('a').removeClass('slct_chip');
			jq('#' + filter + 'Hidden').val('');
		}
	});
	var filterOpenType = jq('#filterOpenType').val();
	if(filterOpenType == 'acceptanceFilter'){
		  jq('input[data-acceptance-value]').prop('checked', false); 
		  jq('#acceptanceHidden').val('');
	  }
	jq('.switch input, .coa_cont input').each(function(index){
		if (jq(this).hasClass('slct_chip') && jq('.switch, .coa_cont').is(":visible")) {
		var filter = jq(this).attr('data-filter-show');
		jq('#' + filter).removeClass('slct_chip');
		jq('#'+filter).prop('checked', false);
		jq('#' + filter + 'Hidden').val('');
		}
	});

	jq("#ajax_listOfOptions_subject li a").each(function(indx) {
		if(jq(this).hasClass('slct_chip')) {
			jq(this).removeClass('slct_chip');
		}
	});
});

function changeContact(hotlineNumber, anchorId) {

    jq("#" + anchorId).attr("title", hotlineNumber);
    jq("#" + anchorId).html("<i class='fa fa-phone' aria-hidden='true'></" + "i>" + hotlineNumber);
    var mobileFlag = jq("#check_mobile_hidden").val();
    if (mobileFlag == 'true') {
        setTimeout(function() {
            location.href = 'tel:' + hotlineNumber;
        }, 1000)
    }
}


jq('body').on('keyup', '#ajaxUniversityName', function(event) {
	var value = jq(this).val().trim().toLowerCase();
	value = value.replace(reg, " ");
	value = value.trim();
    if (!(jq.fn.notBlankOrNull(value))) {
		  jq('#ajaxUniDropDown').hide();
		  jq('#ajaxUniDropDown').parent('.slimScrollDiv').hide();
        return;
    }
	  if (value.length > 0) {
		var flag = "N";
		jq('#ajaxUniDropDown').show();
		  jq('#ajaxUniDropDown').parent('.slimScrollDiv').show();
		jq("#ajaxUniDropDown li a").each(function() {
		  var listText = jq(this).text().toLowerCase();
		  var listId = jq(this).parent('li').attr('id');
		  var indexPos = listText.indexOf(value);
		  var prexfix = '<u>';
		  var suffix = '</u>';
		  if (indexPos >= 0 ) {
			var endPos = indexPos + value.length + prexfix.length;
			var texWithunderLine = jq(this).text().substring(0, indexPos) + prexfix + jq(this).text().substring(indexPos);
			texWithunderLine = texWithunderLine.substring(0, endPos) + suffix + texWithunderLine.substring(endPos);
			jq(this).html(texWithunderLine);
			jq('#' + listId).show();
			
			//jq('.ajax_ul').slimscroll({});
		  } else {
			  jq("#"+listId).hide();
		  }
	    });
	  } else {
		  jq('#ajaxUniDropDown').hide();
		  jq('#ajaxUniDropDown').parent('.slimScrollDiv').hide();
	  }
});


jq(document).on('click touchstart', function (event) {

  if (jq(event.target).closest("#ajaxUniDropDown").length === 0 && jq(event.target).closest("#ajaxUniversityName").length === 0 && jq(event.target).closest("#uni_srch_icon").length === 0 && jq(event.target).closest('.slimScrollDiv').length === 0) {
	  jq('#ajaxUniDropDown').hide();
	  jq('#ajaxUniDropDown').parent('.slimScrollDiv').hide();
  }
});

jq(window).scroll(function() {
    videoPause();
	clearingSearchResultOnScrollEvent();
    clearingFeatured();
});


function isScrollIntoView(element, fullyInView) {
    var pageTop = jq(window).scrollTop();
    var pageBottom = pageTop + jq(window).height();
    var elementTop = jq(element).offset().top;
    var elementBottom = elementTop + jq(element).height();
    if (fullyInView === true) {
        return ((pageTop < elementTop) && (pageBottom > elementBottom));
    } else {
        return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
    }
}

function filterGALogging(gaValue) {
	var eventCategory = "filter";
	var eventAction = "";
	var eventLabel = "";
	if (gaValue.indexOf(",") > -1) {
		var eventValue = gaValue.split(',');
		eventAction = eventValue[0];
		eventLabel = gaValue.slice(gaValue.indexOf(",")+1);
		if(jq.fn.notBlankOrNull(eventAction) && jq.fn.notBlankOrNull(eventLabel)) {
			searchEventTracking(eventCategory, eventAction, eventLabel);
		}
	}
}

function clearingSearchResultOnScrollEvent() {
    var sponsoredId = jq('#SPONSORED_LISTING');
    var dataId = "data-id";
    var statsNotLogged = "stats_not_logged";
    var statsLogged = "stats_logged";
    var collegeId = jq('#collegeIdSponsored').val();
    var collegeName = jq('#collegeNameSponsored').val();
    var orderItemIdSponsored = jq('#orderItemIdSponsored').val();
    var browseCatCode = jq("#browseCatCodeFeatured").val();
    var subject = jq("#subjectFeatured").val();
    var qualification = jq("#qualificationFeatured").val();


    if (sponsoredId.length && isNotUNE(sponsoredId)) {
        if (isScrollIntoView(sponsoredId, false) && sponsoredId.attr(dataId) == statsNotLogged) {
            clearingSearchResultSponsoredViewLogging(collegeId, 'CLEARING_SPONSORED_PAGE_VIEW', collegeName, '', orderItemIdSponsored, browseCatCode,subject, qualification );
            sponsoredId.attr(dataId, statsLogged);
        }
    }
}

function clearingFeatured() {
    var featureId = jq('#featured_clearing');
    var dataId = "data-id";
    var statsNotLoggedFeatured = "stats_not_logged_featured";
    var statsLoggedFeatured = "stats_logged_featured";
    var collegeId = jq('#collegeIdFeatured').val();
    var collegeName = jq('#collegeNameFeatured').val();
    var orderItemId = jq("#orderItemIdFeatured").val();

    if (featureId.length && isNotUNE(featureId)) {
        if (isScrollIntoView(featureId, false) && featureId.attr(dataId) == statsNotLoggedFeatured) {
            featuredClearingDbStatsLogging(collegeId, '2', 'FEATURED_CLEARING_BOOSTING', collegeName, orderItemId);
            featureId.attr(dataId, statsLoggedFeatured);
        }
    }
    
	var featureIdVideo = jq('#featured_videoId');
	var dataIdVideo = "data-id";
    var statsNotLoggedFeaturedVideo = "stats_not_logged_featured_video";
    var statsLoggedFeaturedVideo = "stats_logged_featured_video";

    if (featureIdVideo.length && isNotUNE(featureIdVideo)) {
        if (isScrollIntoView(featureIdVideo, false) && featureIdVideo.attr(dataIdVideo) == statsNotLoggedFeaturedVideo) {
            featuredClearingDbStatsLogging(collegeId, '2', 'FEATURED_CLEARING_VIEO_VIEW', collegeName, orderItemId);
            featureIdVideo.attr(dataIdVideo, statsLoggedFeaturedVideo);
        }
    }
}

function videoPause() {
	var featureIdVideo = jq('#featured_video_clearing');
    if (featureIdVideo.length && isNotUNE(featureIdVideo)) {
        if (!isScrollIntoView(featureIdVideo, false)) {
            var videoIdElement = jq('#featured_video_clearing').get(0);
            var vidId = jq('#featured_video_clearing');
            if (!videoIdElement.paused) {
                jq(".cl_vcnr").hide();
                videoIdElement.pause();
                videoDurationPercentage(videoIdElement);
            }
        }
        if (isScrollIntoView(featureIdVideo, false)) {
            if (!videoPlayedOnce) {
        	onLoadFeaturedVideoCall();
            videoPlayedOnce = true;
           }
        }
        
    }
}

function isScrollIntoView(element, fullyInView) {
    var pageTop = jq(window).scrollTop();
    var pageBottom = pageTop + jq(window).height();
    var elementTop = jq(element).offset().top;
    var elementBottom = elementTop + jq(element).height();
    if (fullyInView === true) {
        return ((pageTop < elementTop) && (pageBottom > elementBottom));
    } else {
        return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
    }
}

function clickPlayAndPauseVideo(videoId, e) {
    var videoIdElement = jq('#' + videoId).get(0);
    var vidId = jq('#' + videoId);
    if (videoIdElement.paused) {
        jq(".cl_vcnr").hide();
        e.preventDefault();
        videoIdElement.play();
        vidId.attr('controls', '');
        videoDurationPercentage(videoIdElement);
    } else {
        jq(".cl_vcnr").show();
        e.preventDefault();
        videoIdElement.pause();
        videoDurationPercentage(videoIdElement);
    }
}

function onLoadFeaturedVideoCall() {
    var videoId = jq('#featured_video_clearing');
    var vidId = doc("featured_video_clearing");
    //var collegeId = jq('#collegeId').val();
    //var networkId = jq('#networkId').val();
    var videoIdElement = videoId.get(0);
    if (videoId.length) {
        if (videoId.hasClass('autoPlay')) {
            vidId.muted = true;
            jq("#featured_image_clearing").hide();            
            jq(".cl_vcnr").hide();
            videoId.show();
            if (videoIdElement.paused) {
              videoIdElement.play();
            }
            
            videoId.attr('controls', '');
            //featuredDbStatsLogging(collegeId, networkId, "FEATUREDVIDEO");
            videoDurationPercentage(videoIdElement);
        } else if (videoId.hasClass('fiveSecPlay')) {        	
        	jq("#loadIconflv").show();
            setTimeout(function() {
                vidId.muted = true;
                jq("#featured_image_clearing").hide();
                jq("#loadIconflv").hide();
                jq(".cl_vcnr").hide();
                videoId.show();
                if (videoIdElement.paused) {
                  videoIdElement.play();
                }                
                videoId.attr('controls', '');
                //featuredDbStatsLogging(collegeId, networkId, "FEATUREDVIDEO");
                videoDurationPercentage(videoIdElement);
            }, 5000);
        }
    }
}


function videoDurationPercentage(videoElement) {
    if (videoRanMap[videoElement.id + '_percentage'] == null) {
        videoRanMap[videoElement.id + '_percentage'] = "0%";
    }
    if (videoRanMap[videoElement.id + '_imp5sec'] == null) {
        videoRanMap[videoElement.id + '_imp5sec'] = "N";
    }
    if (videoRanMap[videoElement.id + '_complete'] == null) {
        videoRanMap[videoElement.id + '_complete'] = "N";
    }
    videoElement.ontimeupdate = function() {
        var curVidDrtn = videoElement.duration;
        var tenPcntSec = curVidDrtn / 10;
        //
        if (videoRanMap[videoElement.id + '_imp5sec'] != "Y") {
            videoRanMap[videoElement.id + '_imp5sec'] = "Y";
            GANewAnalyticsEventsLogging('Video Impression View', jq('#gaCollegeNameFeatured').val(), 'featured brand');
        }
        for (var cnt = 1; cnt <= 10; cnt++) {
            if (this.currentTime >= tenPcntSec * cnt) {
                if (videoRanMap[videoElement.id + '_percentage'].indexOf(vidpct[cnt]) < 0) {
                    videoRanMap[videoElement.id + '_percentage'] += ',' + vidpct[cnt];
                    GANewAnalyticsEventsLogging('Video Duration', jq('#gaCollegeNameFeatured').val() + "|" + 'featured brand', vidpct[cnt]);
                }
            }
        }
    };
}

function featuredClearingDbStatsLogging(collegeId, networkId, featuredFlag, requestUrl, orderItemId) {
    var ajax = new sack();
    deviceWidth = getDeviceWidth();
    var clickType = "FEATURED_CLICK_CLEARING";
    var networkId = '2';
    var externalUrl = '';
    if (requestUrl != undefined && requestUrl != '') {
        externalUrl = "&externalUrl=" + encodeURIComponent(requestUrl);
    }
    var browseCatCode = jq("#browseCatCodeFeatured").val();
    var subject = jq("#subjectFeatured").val();
    var qualification = jq("#qualificationFeatured").val();
    
    var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z=' + collegeId + "&screenwidth=" + deviceWidth + "&featuredFlag=" + featuredFlag + "&networkId=" + networkId + "&clickType=" + clickType + "&orderItemId=" + orderItemId+ "&categoryCode="+ browseCatCode +"&featuredSubject="+ subject + "&studyLevelId="+  qualification + externalUrl;
    ajax.requestFile = url;
    ajax.onCompletion = function() {
        responseDataClearingFeatured(ajax,featuredFlag,requestUrl);
    };
    ajax.runAJAX();
}

function responseDataClearingFeatured(ajax,featuredFlag,requestUrl) {
	if(featuredFlag == 'INTERNAL'){
		window.location.href = requestUrl;
	} else{
		return false;
	}
	
   return false;
}

//to find retina device
function isRetina() {
    var query = '(-webkit-min-device-pixel-ratio: 1.5),\
	    (min--moz-device-pixel-ratio: 1.5),\
	    (-o-min-device-pixel-ratio: 3/2),\
	    (min-device-pixel-ratio: 1.5),\
	    (min-resolution: 144dpi),\
	    (min-resolution: 1.5dppx)';
    if (window.devicePixelRatio > 1 || (window.matchMedia && window.matchMedia(query).matches)) {
        return true;
    }
    return false;
}
// retina call for mobile images
function retina() {
    var devicePixel = window.devicePixelRatio;
    devicePixel = devicePixel > 2.5 ? 3 : devicePixel > 1.5 && devicePixel < 2.5 ? 2 : 1;
    var devWidth = jq(window).width() || document.documentElement.clientWidth;
    if (window.isRetina()) {
        if (devicePixel >= 1) {
            if (jq('#featured_image_clearing').length > 0) {
                var images = document.getElementsByTagName('img');
                for (var i = 0; i < images.length; i++) {
                    var imgSource = images[i].getAttribute("data-src");
                    var isRetinaImageNeed = images[i].getAttribute('data-rjs');
                    if ((imgSource != null && imgSource.indexOf('px@') <= -1) && isScrollIntoView(images[i], false)) {
                        if (imgSource != null && isRetinaImageNeed != null && isRetinaImageNeed != undefined && isRetinaImageNeed == 'Y') {
                            var lastSlash = imgSource.lastIndexOf('.');
                            if (lastSlash != null) {
                                var path = imgSource.substring(0, lastSlash);
                                file = imgSource.substring(lastSlash + 1);
                                if (devWidth < 480) {
                                    if (imgSource.indexOf('_278px') > -1) {
                                        retinaSrc = path + '@' + devicePixel + 'x.' + file;
                                    } else {
                                        retinaSrc = path + '_278px@' + devicePixel + 'x.' + file;
                                    }
                                } else if (devWidth > 481 && devWidth < 992) {
                                    //if (imgSource.indexOf('_369px') > -1) {
                                        retinaSrc = path + '.' + file;
                                    //} else {
                                      //  retinaSrc = path + '_369px.' + file;
                                    //}
                                } else {
                                    //if (imgSource.indexOf('_469px') > -1) {
                                        retinaSrc = path + '.' + file;
                                    //} else {
                                        //retinaSrc = path + '_469px.' + file;
                                    //}
                                }
                                images[i].src = retinaSrc;
                                images[i].setAttribute("data-src", retinaSrc);
                            }
                        }
                    }
                }
            }
        }
    }
}

function getMatchTypeValue(id) {
	var course = jq("#courseMatchTypeMap").val();
	var uni = jq("#uniMatchTypeMap").val();
	var matchType = "";
	if(course != null && course != "" && course != undefined) {
	course.trim().slice(1, -1).split(',').forEach(function(v) {
	    var val = v.trim().split('=');
	    if(val[0] == id){
	    	matchType = val[1];
	    } 
	  });
	}
	if(uni != null && uni != "" && uni != undefined) {
	  uni.trim().slice(1, -1).split(',').forEach(function(v) {
	    var val = v.trim().split('=');
	    if(val[0] == id){
	    	matchType = val[1];
	    } 
	  });
	}
	  return matchType;	
}

function matchTypeGALogging(id) {
	var gaSetValue = getMatchTypeValue(id);
	ga('set', 'dimension19', gaSetValue);
}

function featuredBrandGaLogging(){
	ga('set', 'dimension17', 'Featured');
}

