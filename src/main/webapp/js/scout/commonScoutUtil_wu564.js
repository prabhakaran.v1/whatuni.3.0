var ScoutErrSusMsg = {
  error_dob_empty : "Happy birthday to you on the.....?",  
  sucess_post_code : "We know where you live...",
  error_postcode_wrong : "We can't find this address. Please enter valid post code.",
  sucess_dob_birth : "Don't expect a birthday card. Soz",
  error_email : "Something seems to be missing from your email. Awkward.",
  error_email_already : " is already registered with us, ",
  sucess_email : "You've just got to be on email",
  error_dob_day : "Day must be between 1 and 31.",
  error_dob_month : "Month must be between 1 and 12",
  error_dob_year_min : "Year must be greater than ",
  error_dob_year_max : "Year must be lesser than ",
  error_login_email : "Please enter your email",
  error_login_email_wrong : "Please enter a valid email address",
  error_login_password : "Please enter your password",
  error_date_number : "Enter day as numeric only",
  error_month_number : "Enter month as numeric only",
  error_year_number : "Enter year as numeric only",
  error_age_wrong : "Please enter valid date",
  error_age_less_thirteen : "You must be at least 13 years old"
};
var flag = 0;
var $fp = jQuery.noConflict();
function focusElement(divId){
  var searchEles = document.getElementById(divId).children;
  flag = 0;
  for(var i = 0; i < searchEles.length; i++) {
    if ($fp(searchEles[i]).children().length > 0 ) {
     if(flag == 0) { subFocusElement($fp(searchEles[i])); } else { return false; }
    }
  }
  flag = 0;
}
function subFocusElement(divId){
  var searchEles = $fp(divId).children(); 
  if($fp(divId).hasClass('qlerr')) {
    checkErrorClassAndFocus(divId); 
  } else {
    for(var i = 0; i < searchEles.length; i++) {
      if ($fp(searchEles[i]).children().length > 0 ) {
        if($fp(searchEles[i]).hasClass('qlerr')) {
          checkErrorClassAndFocus(searchEles[i]);
        } else {
          subFocusElement($fp(searchEles[i]));
        }
      } 
    }
  }
}
function checkErrorClassAndFocus(searchEles){
  var childDiv = $fp(searchEles).children();
  if(flag == 0){
    for(var j = 0; j < childDiv.length; j++) {
      var divPosition = $fp(childDiv[j]).offset();
      if(childDiv[j].tagName == 'INPUT') {
        $fp(window).scrollTop(divPosition.top - 100);
        $fp(childDiv[j]).focus(); flag++;
      } else if(childDiv[j].tagName == 'DIV' || childDiv[j].tagName == 'FIELDSET') {
        $fp(window).scrollTop(divPosition.top - 100); flag++;
      }
    }
  }
}
function isValidDOBDate(dateStr, msgId){
  var msg = "";    
  var dateArray = dateStr.split("/");        
  day = dateArray[0];
  month = dateArray[1];
  year = dateArray[2];        
  var d = new Date();
  var n = d.getFullYear();
  if(isEmpty(day) && isEmpty(month) && isEmpty(year)){
    writeinnerHTML(msgId, ScoutErrSusMsg.error_dob_empty);
    return false;
  }
  if(day < 1 || day > 31){
    writeinnerHTML(msgId, ScoutErrSusMsg.error_dob_day);
    return false;
  }
  if(month < 1 || month > 12){ // check month range
    writeinnerHTML(msgId, ScoutErrSusMsg.error_dob_month);
    return false;
  }
  if(year < 1900){
    writeinnerHTML(msgId, ScoutErrSusMsg.error_dob_year_min + '1900');
    return false;
  }
  if(year > (n - 13)){
    writeinnerHTML(msgId, ScoutErrSusMsg.error_dob_year_max + (n - 13));
    return false;
  }
  if((month==4 || month==6 || month==9 || month==11) && day==31){
    writeinnerHTML(msgId, "Month "+month+" doesn't have 31 days!");
    return false;
  }
  if(month == 2){ // check for february 29th
    var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
    if((day>29 || (day==29 && !isleap))){
      writeinnerHTML(msgId, "February " + year + " doesn't have " + day + " days!");
      return false;
    }
  }    
  return true;  // date is valid
}
