var contextPath = "/degrees";
var ErrorMessage = { 
  autocompleteErrMsg : "Please enter an industry or a job you&#39d like to do.",
  situationErrMsg    : "Please select the situation best describes you.",
  yearOfEntryErrMsg  : "Please select the year.",
}
function $$D(id){ return document.getElementById(id); }
function blockNoneDiv(thisid, value){  if($$D(thisid) !=null){ $$D(thisid).style.display = value;  }  }
function blockNoneDivId(thisid, value){  if(thisid){ thisid.style.display = value;  }  }
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}

function chooseJobOrIndustry(event, object){
 var id = object.id;
 var hiddenId = id+"_display"; 
 if(($$D(hiddenId).value != "" || $$D(hiddenId).value != null) && event.keyCode != 13){
  $$D(hiddenId).value = "";
 }  
 if(($$D('jobOrIndustry_id').value != "" || $$D('jobOrIndustry_id').value != null) && event.keyCode != 13){
  $$D('jobOrIndustry_id').value = "";
 }  
 if(object.value){        
  blockNoneDiv("errordiv","none");
  $$D("errordiv").innerHTML = "";
  if(trimString(object.value)!=""){
   ajax_showOptions(object,'jobOrIndustry',event,"indicator", '');
  }  
 }else{        
  blockNoneDiv("errordiv","block");
  $$D("errordiv").innerHTML = ErrorMessage.autocompleteErrMsg;
  $$D("jobOrIndustry_display").value = "";        
 }	
}
function iwanttobeKeypressSubmit(obj, event){
  if(event.keyCode==13 && (($$D('jobOrIndustry') && $$D('jobOrIndustry').value == "")
     || ($$D('jobOrIndustry_id') && ($$D('jobOrIndustry_id').value == ""  && $$D('jobOrIndustry').value != '')))){
    blockNoneDiv("errordiv","block");
    $$D("errordiv").innerHTML = ErrorMessage.autocompleteErrMsg;
    $$D("jobOrIndustry_display").value = "";
    return false;
  } else if(event.keyCode==13){
    return false;
  }
  return true;
}
function checkedRadio(name) {
 var flag = false;
 var radioButtons = document.getElementsByName(name);
 for(var x = 0; x < radioButtons.length; x++) {
  if(radioButtons[x].checked) {
   flag = true;
   if(name=='radio11'){
     if($$D("yearOfEntryVal")) { $$D("yearOfEntryVal").value = radioButtons[x].value; }
     blockNoneDiv("errordiv2","none");
   }
  }
 }
 return flag;
}
function setCheckedVal(value){
 var ajaxFlag = true;
 if($$D("checkSchoolType") && $$D("checkSchoolType").value == value){
  ajaxFlag = false;
 }
 if(ajaxFlag){
  var ajax = new sack();
  url = contextPath + "/i-want-to-be-widget.html?pageName=yearOfEntry&schoolType="+value;  
  ajax.requestFile = url;
  ajax.onCompletion = function(){displayYearOfEntryResult(ajax);};
  ajax.runAJAX(); 
 }
}
function displayYearOfEntryResult(ajax){
 var response = ajax.response;   
 $$D('schoolOrCollegeYear').innerHTML = response;  
 blockNoneDiv("errordiv1","none");
}
function submitSearchResults(){
 clearDiv('iwanttoberesults');
 if(mandatoryValidation()){
  blockNoneDiv("errordiv","none");
  var extraParam = "";
  extraParam += assignValue('jobOrIndustry_id','jobOrIndustryId');
  extraParam += assignValue('jobOrIndustry_flag','jobOrIndustryFlag');
  extraParam += assignValue('jobOrIndustry_display','jobOrIndustryName');
  extraParam += assignValue('schoolType','schoolType');
  extraParam += assignValue('yearOfEntryVal','yearOfEntryVal');
  var ajax = new sack();
  url = contextPath + "/i-want-to-be-widget.html?pageName=results" + extraParam;  
  ajax.requestFile = url;
  ajax.onCompletion = function(){displayResult(ajax);};
  ajax.runAJAX();  
 }
}
function assignValue(divName,variable){
 var assignedVal = "";
 if($$D(divName)) {
  assignedVal = "&" + variable + "=" + trimString($$D(divName).value);
 }
 return assignedVal;
}
function displayResult(ajax) {
 var response = ajax.response;   
 if(response != null && trimString(response).length > 0) {
 $$D('iwanttoberesults').innerHTML = response; 
 blockNoneDiv('iwanttobesearch','none');
 blockNoneDiv('subjectResultDiv','none');
 blockNoneDiv('subjectStatsDiv','none');
 blockNoneDiv('iwanttoberesults','block');
 window.scrollTo(0,0);
 showToolTipEightSec('results0','toolTip0');
 } 
}
function showToolTipEightSec(parentDiv, childDiv){
 setTimeout(function() {
  if($$D(parentDiv)){
   blockNoneDiv(childDiv,"block"); 
   setTimeout(function() {
    blockNoneDiv(childDiv,"none");
   }, 8000);
  }
 }, 1000);
}
function mandatoryValidation(){
 var jobCode = $$D('jobOrIndustry_id').value;
 var jobName = $$D('jobOrIndustry').value;
 var submitFlag = true;
 if((jobName == "") || ((jobCode == "" || jobCode == null) && jobName != '') || (jobCode !="" && jobName == "")){
  blockNoneDiv("errordiv","block");
  $$D("errordiv").innerHTML = ErrorMessage.autocompleteErrMsg;
  $$D("jobOrIndustry_display").value = "";
  submitFlag = false;
 }
 if(!checkedRadio('radio1')){
  blockNoneDiv("errordiv1","block");
  $$D("errordiv1").innerHTML = ErrorMessage.situationErrMsg;
  submitFlag = false;
 } else {
   blockNoneDiv("errordiv1","none");
 }
 if(!checkedRadio('radio11')){
  blockNoneDiv("errordiv2","block"); 
  if($$D("errordiv2")) { $$D("errordiv2").innerHTML = ErrorMessage.yearOfEntryErrMsg; }
  submitFlag = false;
 }
 return submitFlag;
}
function goToPreviousPage(){
 window.scrollTo(0,0);
 blockNoneDiv('iwanttobesearch','block');
 blockNoneDiv('iwanttoberesults','none');
 blockNoneDiv('subjectResultDiv','none');
 blockNoneDiv('subjectStatsDiv','none');
}
function showToolTip(showId){
  blockNoneDiv(showId,'block');
}
function hideToolTip(showId){
  blockNoneDiv(showId,'none');
}

function openNewWindow(link){
  window.open(link, "_blank");
}
 //Added by Prabha 16-02-2016
function getSubjectStats(pageFrom, jacsCode, jacsSubject, ultimateSearchId){
 clearDiv('subjectStatsDiv');
 var url = contextPath + "/i-want-to-be-widget.html?pageName=";
 blockNoneDiv('iwanttobesearch', 'none');
 blockNoneDiv('iwanttoberesults', 'none');
 blockNoneDiv('subjectStatsDiv', 'none');
 url +=  pageFrom + "&jacsCode=" + jacsCode + "&ultimateSearchId=" + ultimateSearchId + "&jacsSubject=" + encodeURIComponent(jacsSubject);
 callAjax(pageFrom, $$D('subjectStatsDiv'), url);
 
}
function getSubjectResult(pageFrom, pageNo, jacsCode, ultimateSearchId, sortByWhich, salHid, empHid, pageFlag){
  clearDiv('subjectResultDiv');
  var url = contextPath + "/i-want-to-be-widget.html?pageName=";
  var jacsSubject = $$D('jacsSubject').value
  blockNoneDiv('iwanttobesearch', 'none');
  blockNoneDiv('iwanttoberesults', 'none');
  blockNoneDiv('subjectStatsDiv', 'none');
  blockNoneDiv('subjectResultDiv', 'none');
  url +=  pageFrom + "&jacsCode=" + jacsCode + "&ultimateSearchId=" + ultimateSearchId;
  url += "&sortBy=" + sortByWhich  + "&pageNo=" + pageNo + "&salSortByHow=" + salHid+ "&empSortByHow="+empHid +"&jacsSubject="+encodeURIComponent(jacsSubject)+"&paginationFlag="+pageFlag;
  callAjax(pageFrom, $$D('subjectResultDiv'), url);
}
function clearDiv(divId){
 if($$D(divId)){
   $$D(divId).innerHTML == "";
 }
}

function callAjax(pageFrom, showDivId, url){
  var ajax = new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){onCompleteAjax(ajax, showDivId)}; 
  ajax.runAJAX();
}
function onCompleteAjax(ajax, id){
  var response = ajax.response;
  blockNoneDivId(id, 'block');
   window.scrollTo(0,0);
  id.innerHTML=response;
  if($$D('subjectStatsDiv') && $$D('subjectStatsDiv').style.display == "block"){
    var element = "script_graduates_";
    for(var i = 0; i < 3; i++){
      var elements = $$D(element + i);
	     if(elements){
	       eval(elements.innerHTML);
	     }
    }
    lazyloadetStarts();
  }
  if($$D('subjectResultDiv') && $$D('subjectResultDiv').style.display == "block"){
  
  var adv = jQuery.noConflict();
  adv(window).scroll(function(){ // scroll event 
    lazyloadetStarts();
});
}
}
function subjectResultPagination(pageNo,jacsCode, sortByWhich, ultimateSearchId, salSortBy, empSortBy, pageFlag){
 
  getSubjectResult('SUBJECT-RESULT', pageNo, jacsCode, ultimateSearchId, sortByWhich, salSortBy, empSortBy, pageFlag);
  }

function subjectResultNav(pageNo,jacsCode, sortByWhich, ultimateSearchId, salSortBy, empSortBy, pageFlag){
  if(pageNo == "1"){
    if(sortByWhich == "SALARY"){
      if(salSortBy == ""){
        salSortBy = "D";
      }else if(salSortBy == "D" && pageFlag == "D"){
        salSortBy = "A";
      }else if(salSortBy == "A" && pageFlag == "A"){
        salSortBy = "D";
      }
    }else{
      if(empSortBy == ""){
        empSortBy = "D";
      }else if(empSortBy == "D" && pageFlag == "D"){
        empSortBy = "A";
      }else if(empSortBy == "A" && pageFlag == "A"){
        empSortBy = "D";
      }
    }
  }
  getSubjectResult('SUBJECT-RESULT', pageNo, jacsCode, ultimateSearchId, sortByWhich, salSortBy, empSortBy);
  } 
function hideAndShowTip(id){
 var adv = jQuery.noConflict();

 if(adv("#scrTip"+ id).is(':visible')){
       adv("#scrTip"+ id).hide();
     } else {
       adv("#scrTip"+ id).show();
     }

}
function showUKStats(){
  var adv = jQuery.noConflict();
  adv("#tgldiv").toggleClass("tgldiv");
  adv(".btn_ftr .btn_com").toggleClass('arw')
  adv('html, body').animate({
    scrollTop: adv("#tgldiv").offset().top
  }, 500);
}
function closeWidgetLightBox(){
  result = window.confirm("Do you really want to leave?  You will lose your progress");
  if(result==true){
  blockNoneDiv("fade_NRW","none");
  blockNoneDiv("holder_NRW","none");
  blockNoneDiv("close_NRW","none");
  window.location.assign(contextPath + "/courses/");
  }
}
var $iwtb = jQuery.noConflict();
$iwtb(document).ready(function(){
  $iwtb('#iwanttobeWidget').click(function(e) {
   var screenWidth = document.documentElement.clientWidth;
   if(screenWidth <= 992 && $$D('toolTipDiv')){
     if(e.target.id != 'toolTipDiv' && e.target.id != 'toolTipDiv1' && e.target.id != 'toolTipDiv2' && e.target.id != 'toolTipDiv3' && e.target.id != 'toolTipDiv4' && e.target.id != 'toolTipDiv5') {
      $iwtb('.scr_tip').each(function() {
        $iwtb(".scr_tip").hide();
      });
     }
   }
  });
});
function goToBack(){
 window.scrollTo(0,0);
 blockNoneDiv('iwanttobesearch','none');
 blockNoneDiv('iwanttoberesults','block');
 blockNoneDiv('subjectResultDiv','none');
 blockNoneDiv('subjectStatsDiv','none');
}
function goTobackUniStats(){
 window.scrollTo(0,0);
 blockNoneDiv('iwanttobesearch','none');
 blockNoneDiv('iwanttoberesults','none');
 blockNoneDiv('subjectResultDiv','none');
 blockNoneDiv('subjectStatsDiv','block');
}
//Added ulGAEventTracking for view degrees button 19_Apr_16 Rel
function ulGAEventTracking(action, label, gaaccount){
  var eventCategory = 'widget-i-want-to-be';
  var eventAction = 'view degrees button';
  if(action == 'SG'){
    eventAction = 'subject guide button';
  }
  var eventLabel = label.toLowerCase();
  var eventVal = 1;
  var booleanValue = true;  
  ga('create', gaaccount , 'auto', {'sampleRate': 80 , 'name': 'salesTracker'});
  ga('salesTracker.send', 'event', eventCategory , eventAction , eventLabel , eventVal , {nonInteraction: booleanValue}); 
}
//End