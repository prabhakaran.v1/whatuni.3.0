var opener_qualification;
var opener_description;
var opener_category_code
var opener_category_id

var msg1 = "Please enter a valid institution name";
var msg2 = "Enter email address here";
var msg3 = "Enter uni name here";
var msg4 = "Enter postcode or town here";
var msg5 = "Enter course keyword";
var msg6 = "Submitted from";
var msg7 = "Submitted to";

function trimString (str) {
     str = this != window? this : str;
     return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
 }

 function replaceAll(replacetext, findstring, replacestring) {
    var strReplaceAll = replacetext; var intIndexOfMatch = strReplaceAll.indexOf( findstring );
    while (intIndexOfMatch != -1){
        strReplaceAll = strReplaceAll.replace(findstring, replacestring); intIndexOfMatch = strReplaceAll.indexOf(findstring);
    }
    return strReplaceAll;
 }
 
function setDefaultText(curid){
    var ids = curid.id;
    if(document.getElementById(ids).value=='' && ids!='registeredForm' && ids!='registeredTo') { document.getElementById(ids).value=msg3; }    
    if(ids == 'registeredForm'){ if(document.getElementById(ids).value=='') { document.getElementById(ids).value=msg6; } }
    if(ids == 'registeredTo'){ if(document.getElementById(ids).value=='') { document.getElementById(ids).value=msg7; } }
 }

function clearDefaultText(curid){
    with(document) {
        var ids = curid.id; getElementById(ids).value = trimString(getElementById(ids).value);
        if(getElementById(ids).value==msg3) { getElementById(ids).value=""; }   
        if(getElementById(ids).value==msg6) { getElementById(ids).value=""; }
        if(getElementById(ids).value==msg7) { getElementById(ids).value=""; }
    }
 } 
 
 function clearUniNameValue(e,ts) {
   if(e.keyCode != 13 && e.keyCode != 9) { document.getElementById(ts.id+"_hidden").value=""; }  
 }
  function checkValueUniName() {
    if(document.getElementById('colName_hidden').value == '') { 
      alert(msg1); return false; 
    } else {
      return true;
    }
 }

 function clearValue(e, ts) {
   if(e.keyCode != 13 && e.keyCode != 9) { document.getElementById(ts.id+"_hidden").value=""; }  
 }

 function checkValidCompetitor() {
    var collegeId = document.getElementById('cid').value; var competitorCollegeId = document.getElementById('collegeName_hidden').value;
    if(collegeId != '' && competitorCollegeId != '') {
        if(collegeId == competitorCollegeId) { alert("Oops. Can't be the same college as competitors"); return false; 
        } else{ return true; }
    }
    if (competitorCollegeId == '')  { alert('Please select a valid college'); }
    return false;
 }

  function editSwearing(obj){
    var componentId = obj.id; var splitId = componentId.substring(componentId.lastIndexOf('_')+1);
    for(var rowid=0; rowid<=20; rowid++)  {
        with(document) {
            if(getElementById("label_"+rowid) !=null) {
                if(splitId == rowid) {
                  var inString = getElementById("label_"+rowid).innerHTML;
                  inString = replaceAll(inString, "&amp;", "&"); inString = replaceAll(inString, "&lt;", "<");  inString = replaceAll(inString, "&gt;", ">");
                  getElementById("text_"+rowid).value=inString; getElementById("label_"+rowid).style.display='none';
                  getElementById("text_"+rowid).style.display='block'; getElementById("editlink_"+rowid).style.display='none'; getElementById("updatelink_"+rowid).style.display='block';
                } else {
                  getElementById("label_"+rowid).style.display='block'; getElementById("text_"+rowid).style.display='none';
                  getElementById("editlink_"+rowid).style.display='block'; getElementById("updatelink_"+rowid).style.display='none';
               }
           }
        }
    }
 }
 
 function callSearch() {
    value = document.getElementById("firstname").value;
    if(value != null && value == '') { alert("Oops. Please enter a member name to search"); return false; }
 }

 function updateUrl(obj) {
   var componentId = obj.id; var splitId = componentId.substring(componentId.lastIndexOf('_')+1);
   with(document) {
       if(getElementById("updatelink_"+splitId) !=null) {
         getElementById("updatelink_"+splitId).href=getElementById("updatelink_"+splitId).href + getElementById("text_"+splitId).value;
         return true;
       }  
    }
   return false;
 }
 
 
 function showSrchTab(id,id1,active,deactive,link,delink,dlink,vdelink) {
    with (document){
       getElementById (id).style.display = "block";
       getElementById (id1).style.display = "none";
       getElementById (active).className = "";
       getElementById (deactive).className = "active";
       getElementById (link).style.display = "inline";
       getElementById (delink).style.display = "none";
       getElementById (dlink).style.display = "none";
       getElementById (vdelink).style.display = "inline";
   }  
 }


 function showSrch (id, qid) {
   with(document){
      if(getElementById (id).style.display == "block"){
        getElementById (id).style.display = "none";
        getElementById (id).style.background = "#FFFFFF";
      }
      else{
       getElementById (id).style.display = "block";
       getElementById (id).style.background = "#D9E98F";
       getElementById(qid).focus();
      }
    }   
 }
 
 function showRadioSrch (id, qid) {
    if(id != 'Overall-Rating') {
        var word = /\w/g;
        var nonword = /\W/g;
        var wordflag = false;
        var nonwordflag = false;
        reviewtext = document.getElementById("Btarea").value;
        wordflag = isValidText(reviewtext, nonword);
        nonwordflag = isValidText(reviewtext, word);
        if((wordflag || nonwordflag) && trimString(reviewtext) !='') {
            document.getElementById(id).style.display = "block";
            document.getElementById(qid).focus();
         }
        else {
          alert("To complete your review we need you to write about your overall experience as a minimum � that amount of time must leave you with something to say!");
        }
      }  
    else {
      document.getElementById (id).style.display = "block";
      document.getElementById(qid).focus();
    }
 }

 function isValidText(reviewtext, pattern) {
    var index = -1;
    if(reviewtext != null) {
        index = reviewtext.search(pattern); 
    }
    return (index != -1) ? true : false;
  }

 function chRadioColor (id) {
     document.getElementById (id).style.background = "#D9E98F";
 }

 function hideSrch (id) {
   document.getElementById (id).style.display = "none";
 }

 function chColor (id) {
   document.getElementById (id).style.background = "#D9E98F";
 }

 function chColorBack (id) {
   document.getElementById (id).style.background = "#FFFFFF";
 } 
 
  function openDiv(divname, qid) {
    var textareaname = qid + "tarea";
    document.getElementById(divname).style.display = "block"
    document.getElementById (divname).style.background = "#D9E98F";
    document.getElementById(textareaname).focus();
  }
	
	function editOpenDays(obj){
    var componentId = obj.id; var splitId = componentId.substring(componentId.lastIndexOf('_')+1);
    for(var rowid=0; rowid<=20; rowid++)  {
        with(document) {
            if(getElementById("date_"+rowid) !=null) {
                if(splitId == rowid) {
                  var inString = getElementById("date_"+rowid).value;
                  inString = replaceAll(inString, "&amp;", "&"); inString = replaceAll(inString, "&lt;", "<");  inString = replaceAll(inString, "&gt;", ">");
                  getElementById("date_"+rowid).value=inString; getElementById("datelab_"+rowid).style.display='none';
                  getElementById("date_"+rowid).style.display='block'; getElementById("editlink_"+rowid).style.display='none'; getElementById("updatelink_"+rowid).style.display='block';
								
                  var noteString = getElementById("notes_"+rowid).value;
                  noteString = replaceAll(noteString, "&amp;", "&"); noteString = replaceAll(noteString, "&lt;", "<");  noteString = replaceAll(noteString, "&gt;", ">");
                  getElementById("notes_"+rowid).value=noteString; getElementById("noteslab_"+rowid).style.display='none'; 
                  getElementById("notes_"+rowid).style.display='block'; getElementById("editlink_"+rowid).style.display='none'; getElementById("updatelink_"+rowid).style.display='block';
                  getElementById("dateimg_"+rowid).style.display='block';
              } else {
                  getElementById("datelab_"+rowid).style.display='block'; getElementById("date_"+rowid).style.display='none'; 
  		  getElementById("noteslab_"+rowid).style.display='block'; getElementById("notes_"+rowid).style.display='none';
                  getElementById("editlink_"+rowid).style.display='block'; getElementById("updatelink_"+rowid).style.display='none';
		  getElementById("dateimg_"+rowid).style.display='none';
               }
           }
        }
    }
 }
 
 function updateOpendaysUrl(obj) {
   var componentId = obj.id; var splitId = componentId.substring(componentId.lastIndexOf('_')+1);
   with(document) {
       if(getElementById("updatelink_"+splitId) !=null) {
           if((getElementById("date_"+splitId).value!=getElementById("oldDate_"+splitId).value) || getElementById("notes_"+splitId).value!=getElementById("oldNotes_"+splitId).value) {
                getElementById("updatelink_"+splitId).href=getElementById("updatelink_"+splitId).href +"newDate="+getElementById("date_"+splitId).value +"&newNotes="+getElementById("notes_"+splitId).value;
		return true;
            } else {
		getElementById("datelab_"+splitId).style.display='block'; getElementById("date_"+splitId).style.display='none';
                getElementById("noteslab_"+splitId).style.display='block'; getElementById("notes_"+splitId).style.display='none';
                getElementById("editlink_"+splitId).style.display='block'; getElementById("updatelink_"+splitId).style.display='none';
		getElementById("dateimg_"+splitId).style.display='none';
            }
       }  
    }
   return false;
 }
 
function openCategoryWindow(qualification) {
    opener_qualification = qualification;
    newwin = window.open('/degrees//admin-browse-category.html?qual='+qualification,"browsewindow","width=355,height=500,resizable=no,scrollbars=yes");
 }
   
   
function checkCategoryData(){
    with(document){
        if(getElementById("browseCategoryId") != null) {
            if(trimString(getElementById("browseCategoryId").value) ==''){
                alert("Oops! please enter Browse Category Id.. or select using browse link");
                return false;
            }
        }
        if(getElementById("categoryCode") != null) {
            if(trimString(getElementById("categoryCode").value) ==''){
            alert("Oops! please enter Browse category code.. or select using browse link");
                return false;
            }
        }
    }
    return true;
 }
 
 
function updateNewCollege(college_id, college_name)
{
    var old_college_id   = "";
    var old_college_name = "";
    var new_college_id   = college_id;
    var new_college_name = college_name;
    if(document.getElementById("old_college_id") !=null){
        old_college_id = document.getElementById("old_college_id").value;
    }
    if(document.getElementById("old_college_name") !=null){
        old_college_name = document.getElementById("old_college_name").value;
    }
     var answer = confirm("Are you sure to replace \n\n \"" + old_college_name +"\" (with) \""+ new_college_name+"\"");
	if (answer){
            var result = confirm("All datas related to \""+ old_college_name + "\" will replaced with \""+ new_college_name +"\"\n\n\ Are you sure to proceed....?");
            if (result){
                document.getElementById("new_college_id").value=new_college_id;
                if(trimString(document.getElementById("new_college_id").value) != '' &&  trimString(document.getElementById("old_college_id").value) != ''){
                    document.getElementById("update-college-div").style.display='block';
                    document.forms[0].submit();
                } else{
                    alert("Problem with data.. please try again...");
                    return false;
                }
           }         
      }   
  }

function UpdateReviewInfo(id, type, college, reviewid, curindex){
    var url = "/degrees/adminlistreview.html?ajaxrequest=yes&rid="+id.value+"&type="+type+"&cid="+college;
    var ajax=new sack();
    ajax.requestFile = url;	// Specifying which file to get
    if(curindex != 'single'){
    ajax.onCompletion = function(){ showReviewStatus(ajax, reviewid, college, type, curindex) };	// Specify function that will be executed after file has been found
    }else{
    ajax.onCompletion = function(){showReviewStatus(updateReviewStatus(ajax)) };
    }
    ajax.runAJAX();
    return false;
}
function showReviewStatus(ajax, reviewid, college, type, curindex){
    //deselect all the ration button based on the selected type
    for(var count=0; count<10; count++){
     if(type =='G'){ if(document.getElementById('radiobtn_g_'+count) !=null) { document.getElementById('radiobtn_g_'+count).checked=false;}}   
     if(type =='B'){ if(document.getElementById('radiobtn_b_'+count) !=null) { document.getElementById('radiobtn_b_'+count).checked=false;}}   
     if(document.getElementById('show_response_'+count) !=null) { document.getElementById('show_response_'+count).innerHTML="";}
    }  
     if(type=="G"){document.getElementById('radiobtn_g_'+curindex).checked=true;}
     if(type=="B"){document.getElementById('radiobtn_b_'+curindex).checked=true;}
     document.getElementById('show_response_'+curindex).innerHTML=ajax.response;
     //window.location.href=window.location.href;
     //return false;
}
function updateReviewStatus(ajax){
document.getElementById('show_response').innerHTML=ajax.response;
}
function checkCheckboxEnabled(){
    var checkboxes = document.getElementsByName('checkApprove');    
    var checkCount=0;
    for (var i=0; i<checkboxes.length; i++) {
        if (checkboxes[i].checked) {     
            checkCount++;
        }
    }
    if(checkCount < 2){
        document.getElementById('approve').value = " Approve selected review ";
    }else{
        document.getElementById('approve').value = " Approve selected reviews ";
    }
}
function checkEnabledReviewList(){
    var checkboxes = document.getElementsByName('checkApprove');
    var selected = [];
    var collegeIdSelected = [];
    var count = 0;
    for (var i=0; i<checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            selected.push(checkboxes[i].value);
            var id = "collegeP_"+i;            
            collegeIdSelected.push(document.getElementById(id).value);
            count++;
        }
    }    
    if(count==0){
        alert("Please select reviews");
        return false;
    }else{
        document.getElementById('multiSelectIds').value = selected;    
        document.getElementById('selectCollegeIds').value = collegeIdSelected;        
        return true;    
    }    
}
function setSelectedFocus(backreviewid){    
    if(document.getElementById(backreviewid)){
        document.getElementById(backreviewid).scrollIntoView();
    }
}