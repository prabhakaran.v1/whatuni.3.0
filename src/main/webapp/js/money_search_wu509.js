function $$(id){
  return document.getElementById(id);
}

function submitSearchLink(thisobjval, type, urlType, spanId){
with(document){
//$$(spanId).innerHTML = thisobjval.value;
var seo_qual=''
var qual=$$("paramStudyLevelId").value;
if(type=='QUAL'){
  qual = thisobjval.value;
}
switch(qual){
  case 'M,N,A,T,L': seo_qual='college';break
  case 'M': seo_qual='degree';break
  case 'N': seo_qual='hnd-hnc';break
  case 'A': seo_qual='foundation-degree';break
  case 'T': seo_qual='access-foundation';break
  case 'L': seo_qual='postgraduate';break
}
var sort_by="R"
var OrderbyAction=$$("sortingUrl").value
//alert('----------OrderbyAction------------ '+OrderbyAction);
if(OrderbyAction !=null&&OrderbyAction !=''){
var contextPath=$$("contextPath").value
var urlDatas=OrderbyAction.split("/")
//alert("-------------------urlDatas-------------------   "+urlDatas);
var base_url_0=urlDatas[0]
var base_url_1=urlDatas[1]
var base_url_2=seo_qual+'-courses'
var base_url_3=''
if(OrderbyAction.indexOf("ucas-code")>=0){base_url_3='ucas-code-';}
var subjectName=''
if($$("subjectname").value !=null&&$$("subjectname").value !='null'){
  subjectName=$$("subjectname").value
}else{
  subjectName=urlDatas[4]
}
subjectName = replaceURL(subjectName);
var regioname = $$("paramlocationValue").value;
if(type=='REGION'){
  regioname = thisobjval.value;
}
if(regioname !=''){
  base_url_3=base_url_3+replaceHypen(replaceAll(replaceAll(subjectName,"+","-")," ","-"))
  +"-"+seo_qual+'-courses-'
  +(replaceHypen(replaceURL(regioname)))
  +"/"+replaceHypen(urlDatas[4])
}else{
   base_url_3=base_url_3+replaceHypen(replaceAll(replaceAll(subjectName,"+","-")," ","-"))
   +"-"+seo_qual+'-courses'
   +"/"+replaceHypen(urlDatas[4])
}
var base_url_4=qual
var base_url_5=''
if(regioname !=''){
  base_url_5=replaceAll(regioname," ","+")
}else{
  base_url_5='0'}
if(regioname !=''){
  var base_url_6= base_url_5
}else{
   if(urlDatas[7]=='0'){
    var base_url_6=base_url_5
   }else{
     var found=false
     for(var pp=0;pp<locarray.length;pp++){
        if(urlDatas[7]==locarray[pp]){
         found=true}}
        if(found){
         var base_url_6=base_url_5
        }else{
          var base_url_6=urlDatas[7]}
    }
}
var base_url_7=urlDatas[8]
var base_url_8='0'
var base_url_9=''
var studyModeId = $$("paramstudyModeValue").value;
if(type=='SMODE'){
  studyModeId = thisobjval.value;
}

if(studyModeId !=''){
    base_url_9=studyModeId
}else{
   base_url_9='0'
}
var base_url_10=urlDatas[11]
var base_url_11=sort_by
if($$("researchflag") !=null && $$("researchflag").value=='yes'){
  var base_url_12='0'
}else{
  var base_url_12=urlDatas[13]
} 
var base_url_13=urlDatas[14]
var base_url_14=urlDatas[15]
var collegefilter = $$("dropdownCollegeId").value;
if(type=='UC_C_U'){
  collegefilter = thisobjval.value;
}

$$("refineFlag").value="";
if(type=='SMODE' || type=='UC_C_U'){
$$("refineFlag").value="TRUE";
}
$$("omnitureFlag").value="TRUE";
//alert("----------collegefilter-----------  "+collegefilter);
var base_url_15=collegefilter;

if(OrderbyAction.indexOf("ucas-code")>=0){
    var base_url_16 = urlDatas[16]; //This is for entryLevel added by Sekhar for wu403_14022012
    var base_url_17 = urlDatas[17]; //This is for entryPoints
}else{
    var base_url_16 = urlDatas[17]; //This is for entryLevel added by Sekhar for wu403_14022012
    var base_url_17 = urlDatas[18]; //This is for entryPoints
}

if(urlType != -1){
    var final_url=contextPath+base_url_0+"/"+base_url_1+"/"+base_url_2+"/"+base_url_3+"/"+base_url_4+"/"+base_url_5+"/"+base_url_6+"/"+base_url_7+"/"+base_url_8+"/"+base_url_9+"/"+base_url_10+"/"+base_url_11+"/"+base_url_12+"/"+base_url_13+"/"+base_url_14+"/"+base_url_15+"/"+base_url_16+"/"+base_url_17+"/page.html";
} else{
    var final_url=contextPath+base_url_0+"/"+base_url_1+"/"+base_url_2+"/"+base_url_3+"/"+base_url_4+"/"+base_url_5+"/"+base_url_6+"/"+base_url_7+"/"+base_url_8+"/"+base_url_9+"/"+base_url_10+"/"+base_url_11+"/"+base_url_12+"/"+base_url_13+"/"+base_url_14+"/"+base_url_15+"/"+base_url_16+"/"+base_url_17+"/uniview.html";
}
//alert("-------final_url-------------------  "+final_url);
$$("searchBean").action=final_url.toLowerCase();
$$("searchBean").submit()}
}
}

function clearingSubmitSearchLink(thisobjval, type, urlType, spanId){
with(document){
//$$(spanId).innerHTML = thisobjval.value;
var seo_qual=''
var qual=$$("paramStudyLevelId").value;
seo_qual='degree';
var sort_by="R"
var OrderbyAction=$$("sortingUrl").value
//alert('----------OrderbyAction------------ '+OrderbyAction);
if(OrderbyAction !=null&&OrderbyAction !=''){
var contextPath=$$("contextPath").value
var urlDatas=OrderbyAction.split("/")
//alert("-------------------urlDatas-------------------   "+urlDatas);
var base_url_0=urlDatas[0]
var base_url_1=urlDatas[1]
var base_url_2=seo_qual+'-courses'
var base_url_3=''
if(OrderbyAction.indexOf("ucas-code")>=0){base_url_3='ucas-code-';}
var subjectName=''
if($$("subjectname").value !=null&&$$("subjectname").value !='null'){
  subjectName=$$("subjectname").value
}else{
  subjectName=urlDatas[4]
}
subjectName = replaceURL(subjectName);
var regioname = $$("paramlocationValue").value;
if(type=='REGION'){
  regioname = thisobjval.value;
}
if(regioname !=''){
  base_url_3=base_url_3+replaceHypen(replaceAll(replaceAll(subjectName,"+","-")," ","-"))
  +"-"+seo_qual+'-courses-'
  +(replaceHypen(replaceURL(regioname)))
  +"/"+replaceHypen(urlDatas[4])
}else{
   base_url_3=base_url_3+replaceHypen(replaceAll(replaceAll(subjectName,"+","-")," ","-"))
   +"-"+seo_qual+'-courses'
   +"/"+replaceHypen(urlDatas[4])
}
var base_url_4=qual
var base_url_5=''
if(regioname !=''){
  base_url_5=replaceAll(regioname," ","+")
}else{
  base_url_5='0'}
if(regioname !=''){
  var base_url_6= base_url_5
}else{
   if(urlDatas[7]=='0'){
    var base_url_6=base_url_5
   }else{
     var found=false
     for(var pp=0;pp<locarray.length;pp++){
        if(urlDatas[7]==locarray[pp]){
         found=true}}
        if(found){
         var base_url_6=base_url_5
        }else{
          var base_url_6=urlDatas[7]}
    }
}
var base_url_7=urlDatas[8]
var base_url_8='0'
var base_url_9=''
var studyModeId = $$("paramstudyModeValue").value;
if(type=='SMODE'){
  studyModeId = thisobjval.value;
}

if(studyModeId !=''){
    base_url_9=studyModeId
}else{
   base_url_9='0'
}
var base_url_10=urlDatas[11]
var base_url_11=sort_by
if($$("researchflag") !=null && $$("researchflag").value=='yes'){
  var base_url_12='0'
}else{
  var base_url_12=urlDatas[13]
} 
var base_url_13=urlDatas[14]
var base_url_14=urlDatas[15]
var collegefilter = $$("dropdownCollegeId").value;
if(type=='UC_C_U'){
  collegefilter = thisobjval.value;
}

$$("refineFlag").value="";
if(type=='SMODE' || type=='UC_C_U'){
$$("refineFlag").value="TRUE";
}
$$("omnitureFlag").value="TRUE";
//alert("----------collegefilter-----------  "+collegefilter);
var base_url_15=collegefilter;

if(OrderbyAction.indexOf("ucas-code")>=0){
    var base_url_16 = urlDatas[16]; //This is for entryLevel added by Sekhar for wu403_14022012
    var base_url_17 = urlDatas[17]; //This is for entryPoints
}else{
    var base_url_16 = urlDatas[17]; //This is for entryLevel added by Sekhar for wu403_14022012
    var base_url_17 = urlDatas[18]; //This is for entryPoints
}

if(urlType != -1){
    var final_url=contextPath+base_url_0+"/"+base_url_1+"/"+base_url_2+"/"+base_url_3+"/"+base_url_4+"/"+base_url_5+"/"+base_url_6+"/"+base_url_7+"/"+base_url_8+"/"+base_url_9+"/"+base_url_10+"/"+base_url_11+"/"+base_url_12+"/"+base_url_13+"/"+base_url_14+"/"+base_url_15+"/"+base_url_16+"/"+base_url_17+"/clcourse.html";
} else{
    var final_url=contextPath+base_url_0+"/"+base_url_1+"/"+base_url_2+"/"+base_url_3+"/"+base_url_4+"/"+base_url_5+"/"+base_url_6+"/"+base_url_7+"/"+base_url_8+"/"+base_url_9+"/"+base_url_10+"/"+base_url_11+"/"+base_url_12+"/"+base_url_13+"/"+base_url_14+"/"+base_url_15+"/"+base_url_16+"/"+base_url_17+"/cluniview.html";
}
//alert("-------final_url-------------------  "+final_url);
$$("searchBean").action=final_url.toLowerCase();
$$("searchBean").submit()}
}
}


function SetCookie(name,value,expires,path,domain,secure){
document.cookie=name+"="+escape(value)+
((expires)? "; expires="+expires.toGMTString(): "")+
((path)? "; path="+path : "")+
((domain)? "; domain="+domain : "")+
((secure)? "; secure" : "")}

function mapSubmitSearch(loc){
   var pageUrl = $$("loginpagelink").value;
   var urlArray=pageUrl.split("/");
   var newPageUrl="";
   for(var i=0;i<urlArray.length;i++){
      if(i==6){
         newPageUrl+=urlArray[7]+'-'+urlArray[5]+'-'+replaceAll(loc," ","-")+"/";
      }else if(i == 9){
         newPageUrl+=replaceAll(loc," ","+")+"/";
      }else if(i == 10){
         newPageUrl+=replaceAll(loc," ","+")+"/";
      }else{
        newPageUrl+=urlArray[i]+"/";
      }
     }
   newPageUrl=newPageUrl.substring(0,newPageUrl.length-1);
   window.location.href=newPageUrl;
 }
 
 function openMapWindow(form_name, pwidth, pheight, lat, lon,studyLevel){
    sm('newvenue', pwidth, pheight);
    var url="/degrees/mapsearchwindow.html?maplat="+lat+"&maplon="+lon+ "&studyLevel=" + studyLevel;
    document.getElementById("ajax-div").style.display="block";
    var ajax=new sack();
    ajax.requestFile = url;	// Specifying which file to get
    ajax.onCompletion = function(){ mapFormData(ajax); };	// Specify function that will be executed after file has been found
    ajax.runAJAX();
 }
  function mapFormData(ajax){
    $$("newvenueform").innerHTML=ajax.response; 
    $$("ajax-div").style.display="none";
    $$("newvenueform").style.display="block";
 }
function showMap(index, county, title){
  $$("overmap").className= "county"+index;
  $$("overmap").alt = title +" - click to show uni's here";
  $$("overmap").title = title +" - click to show uni's here";
  $$("overmap").onclick = function() {mapSubmitSearch(county); };
 }

/*Money page Fading out Indiator below*/
function settime(){
  setTimeout("slowly.fade('fadeO')",1000);
}
var opacity = 100;
var slowly = {
  fade : function (id) {
    this.fadeLoop(id, opacity);
  },
  fadeLoop : function (id, opacity) {
    //setTimeout("slowly.fadeLoop()",20000);
    var o = document.getElementById(id);
    
    if(o){
      if (opacity >= 30) {
        slowly.setOpacity(o, opacity);
        opacity -= 7;
        window.setTimeout("slowly.fadeLoop('" + id + "', " + opacity + ")", 150);
      } else {
        o.style.display = "none";
      }
    }
  }, 
  setOpacity : function (o, opacity) {
    o.style.filter = "alpha(style=0,opacity:" + opacity + ")";/* IE*/
    o.style.KHTMLOpacity = opacity / 100;/* Konqueror*/
    o.style.MozOpacity = opacity / 100;/* Mozilla (old)*/
    o.style.opacity = opacity / 100;/* Mozilla (new)*/
  }
}
