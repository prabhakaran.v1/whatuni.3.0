var swHead = jQuery.noConflict();

swHead(document).ready(function(){
  changeToMblNav();
  swHead("#click_view1").click(function()
  {
    swHead(this).next().slideToggle("slow");
    swHead("#click_view1_span").toggleClass("minus");
    swHead("#click_view2").next().hide();
    swHead("#click_view2_span").removeClass("minus");
    swHead("#hdr_menu > ul > li:nth-child(6)").toggleClass("nactv");
    swHead("#hdr_menu > ul > li:nth-child(7)").removeClass("nactv");
  });
  swHead("#click_view2").click(function()
  {
    swHead(this).next().slideToggle("slow");
    swHead("#click_view2_span").toggleClass("minus");
    swHead("#click_view1").next().hide();
    swHead("#click_view1_span").removeClass("minus");
    swHead("#hdr_menu > ul > li:nth-child(6)").removeClass("nactv");
    swHead("#hdr_menu > ul > li:nth-child(7)").toggleClass("nactv");
  });
  swHead("#navbar").click(function() {
    swHead("#hdr_menu").slideToggle();
    swHead(".navbar").toggleClass("navact");
    swHead("#mob_nav_rt ul li #mob_menu_act").hide();
  });
  swHead("#mob_nav_rt ul li").click(function() {
    swHead(this).find("#mob_menu_act").slideToggle("fast");
    swHead(".navbar").removeClass("navact");
    swHead("#hdr_menu").hide();	
  });  		  
});	

swHead(window).resize(function() {       
  changeToMblNav();
});

function changeToMblNav() {  
  var width = document.documentElement.clientWidth;    
  if(width < 993){    
    if(document.getElementById("desktop_hdr")){            
      swHead("#click_view1").removeAttr("style");
      swHead('#dsk_reviews').hide();            
    }          
  }else{    
    if(document.getElementById("hide_Menu").value!="Y") {
      swHead("#hdr_menu").removeAttr("style");
    }     
    swHead("#dsk_rev_submenu").removeAttr("style");
    swHead("#dsk_adv_submenu").removeAttr("style");    
    swHead('#dsk_reviews').show();    
    swHead("#dsk_reviews").removeAttr("style");
    swHead("#click_view1").removeAttr("style");
    swHead('#click_view1').hide();      
  }  
}