var flexslider;
function hideHeroImage(videoPath, width, height,coverimage,stdVideoPath){
  searchEventTracking('video','click', 'How does it work video');
  $$D("heroslider").style.display = 'none';
  jQuery("#heroslider_bluscreen").fadeIn('slow');
  $$D("heroslider_bluscreen").style.display = 'block';
  showhide('video_emb','img_anc',videoPath,width,height,coverimage,stdVideoPath);
}

function showhide(a,b,videoPath,width, height,coverimage, stdVideoPath){
  dynamicJsLoad($$D('contextJsPath').value + '/js/video/jwplayer.js',a,b,videoPath,width, height,coverimage, stdVideoPath); //Path added by Prabha on DEC-15
}

function dynamicJsLoad(src,a, b,videoPath,width, height, coverimage,stdVideoPath){
  var script = document.createElement("script");
  script.type = "text/javascript";  
  script.src = src;
  if(script.readyState){//IE
    script.onreadystatechange = function(){
    if (script.readyState == "loaded" || script.readyState == "complete"){
      script.onreadystatechange = null;
      loadJwPlayer(a,b,videoPath,width, height,coverimage);
        if(a=='video_emb'){      
        loadStdChoiceJwplayer(stdVideoPath, coverimage);
       }
     }
    };
    } else {
      script.onload = function(){
       loadJwPlayer(a,b,videoPath,width, height,coverimage);
       if(a=='video_emb'){      
        loadStdChoiceJwplayer(stdVideoPath, coverimage);
       }
      };
    }
  if (typeof script != "undefined"){
    document.getElementsByTagName("head")[0].appendChild(script);
  }
}

function hidePlayer(){
 $$D("heroslider_bluscreen").style.display = 'none';
 jwplayer("video_emb").stop();
 jQuery("#heroslider").fadeIn('slow');
 $$D("heroslider").style.display = 'block';  
} 
function dynLoadJS(src){
  var script = document.createElement("script");
  script.type = "text/javascript";  
  script.src = src;
  if (typeof script != "undefined"){
    document.getElementsByTagName("head")[0].appendChild(script);
  }
}
function articleLoader(){
  var filePath = document.getElementById("contextJsPath").value+'/js/jquery/jquery.flexslider-2.2.min.js';
   if (adv('head script[src="' + filePath + '"]').length > 0){
   }else{
    dynLoadJS(filePath);
   }
   var j = jQuery.noConflict();
   jQuery.noConflict();
     setTimeout(function() {
		   //j(window).load(function() {
						  j('#f_true').flexslider({
						  animation: "slide",
          slideshow:false,
          animationSpeed: 600,
          animationLoop: false,
          slideshowSpeed : 4000,
          itemWidth: 400,
          itemMargin: -1,
          video: false,
          useCSS: false,
          pauseOnAction:true,
          minItems: getWuGridSize(),
          maxItems: getWuGridSize(),
						  start: function(slider){
           flexslider  = slider; 
								   j('div.ltstvds').mouseover(function(){
									   slider.pause();
								   });
								   j('div.ltstvds').mouseout(function() {
									   slider.resume();
								   });
          j('.clone').each(function (){
             
             var ids = j(this).find("img").map(function(){
             return j(this).attr('id')}).get(); 
             for (var i=0; i<ids.length; i++) {
                var art_src = j("#"+ids[i]).attr("data-src");
                j('#f_true').find(".clone").find("#"+ids[i]).attr("src",art_src).removeAttr('id').removeAttr("data-src");
             }
            for(var nxtSlide=(slider.currentSlide*4)+1;nxtSlide<=4;nxtSlide++){
            var src = j("#img"+nxtSlide).attr("data-src");
            j("#img"+nxtSlide).attr("src",src);
            }
            /* var nextslide = slider.currentSlide+1;
             var itemImg = j(this).find("a").children("img");
             var src2 = j("#img"+nextslide).attr("data-src");
             itemImg.attr("src","http://images1.content-wu.com/wu-cont/images/img_px.gif").removeAttr("id");
             j("#img"+nextslide).attr("src",src2).removeAttr("data-src");*/
           });
								  },
         after: function (slider) {
            //instead of going over every single slide, we will just load the next immediate slide
            var nextslide = slider.currentSlide+1;           
            //
            if(nextslide!=1){
               for(var nxtSlide=Math.pow(2,nextslide);nxtSlide<nextslide*4;nxtSlide++){
                  j("#img"+nxtSlide).each(function () {
                    var src = j("#img"+nxtSlide).attr("data-src");
                     j("#img"+nxtSlide).attr("src",src).removeAttr("data-src");
                  });
              }
            }
            //
          }
									});
						   j('.flexslider').flexslider({
						      animation: "slide"
									 });
						  //});	
    }, 1000);
}

function loadStdChoiceJwplayer(stdVideoPath, coverimage){
    $$D("video_emb_std_choice").innerHTML = "";
    $$D("std_video").style.display = "none";
   $$D("img_std_choice_icon").style.display = "block";
	  jwplayerSetup('flashbanner', stdVideoPath, coverimage, 631, 353, stdVideoPath); //coverImage added by Prabha on 29_Mar_16
}

function loadFacebookTab(){  
  var url=contextPath + "/fbAjax.html";      
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){loadFacebookRes(ajax)};
  ajax.runAJAX();      
}

function loadFacebookRes(ajax){
var res = ajax.response;
$$D("facebookLnk").style.display = 'block';
$$D("facebookDiv").innerHTML = res;
}

function showhidesocialTabs(id) {
	if (id == "twitter") {
		$$D("twitterTab").className = "act";
		$$D("twitterLnk").style.display = "block";
		$$D("facebookTab").className = "";
		$$D("facebookLnk").style.display = "none";
		$$D("flickrTab").className = "fl_dts";
		$$D("flickrLnk").style.display = "none";
	} else if (id == "facebook") {
		$$D("facebookTab").className = "act";
		loadFacebookTab();
		$$D("facebookLnk").style.display = "block";
		if ($$D("twitterTab")) {
			$$D("twitterTab").className = "";
			$$D("twitterLnk").style.display = "none";
		}
		$$D("flickrTab").className = "fl_dts";
		$$D("flickrLnk").style.display = "none";
	} else if (id == "flickr") {
		loadFlickrTab();
		$$D("flickrTab").className = "act fl_dts";
		$$D("flickrLnk").style.display = "block";
		$$D("facebookTab").className = "";
		$$D("facebookLnk").style.display = "none";
		if ($$D("twitterTab")) {
			$$D("twitterTab").className = "";
			$$D("twitterLnk").style.display = "none";
		}
	}
}

function getWuGridSize() {
  var dev2 = jQuery.noConflict();
  var gridSize = (dev2(window).width() <= 480) ? 1 : (dev2(window).width() <= 992) ? 2 : 4;
  return gridSize;
}
function loadFlickrTab(){    
  var  url = contextPath + "/flickAjax.html?homeFlickrURL=yes";  
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){loadFlickrRes(ajax)};
  ajax.runAJAX();     
}
function loadFlickrRes(ajax){
  var res = ajax.response;   
  $$D("flickr_badge_wrapper").innerHTML = res;
}
var $jq = jQuery.noConflict();
$jq(document).ready(function() {
  $jq('#callUniProfile').click(function(event){
    event.preventDefault();    
    location.href = $jq('#collegeNameUrl').val();
  });  
  $jq(window).scroll(function(){
    //load articles pod on scroll on 13_06_2017, By Thiyagu G.
    if($$D("scrollStop")){
      if($jq("#scrollStop").val() != 1){
        $jq("#scrollStop").val(1);
        loadArticlesPodJS('HOMEPAGE');
        setTimeout(function() {articleLoader();}, 500);
      }
    }
  }); 
  if($$D("scrollStop")){
    $jq("#scrollStop").val(0);
  }
});
function alpGaLogging(){
  eventTimeTracking();
  ga("send", "event", "App", "Homepage", "Find out more");  
}
function changeContact(hotlineNumber, gaCollegeName, anchorId, collegeId) {
  $jq("#" + anchorId).attr("title", hotlineNumber);
  $jq("#" + anchorId).html("<i class='fa fa-phone' aria-hidden='true'></"+ "i>"+hotlineNumber);
  var mobileFlag = $jq("#check_mobile_hidden").val();
  if(mobileFlag == 'true'){
    setTimeout(function(){
     location.href='tel:'+ hotlineNumber;
    },1000)
  } 
}
function mobileSpecificHeroImage(id,divImgId){
  if($$D(id)) {
  var devWidth = $jq(window).width() || document.documentElement.clientWidth;   
  //var url = $$D(id).getAttribute('data-src');
  var url = $jq('#'+id).val();
  var urlString = 'url("' + url + '")';  
  var imageIndex =  url.lastIndexOf("/") + 1;
  var hrImg = url.substring(imageIndex).split('.');
  var orgUrl = url.substring(0, imageIndex) + hrImg[0];
  if(devWidth < 567){    
    urlString = 'url("'+ orgUrl +'_480px.' + hrImg[1] + '")';
  } else if(devWidth > 568 && devWidth < 992){	  	
    urlString = 'url("'+ orgUrl +'_992px.' + hrImg[1] + '")';
  }
  $$D(divImgId).style.background = urlString + ' no-repeat center top';
 }
}



