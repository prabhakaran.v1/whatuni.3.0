var jqy = jQuery.noConflict();
var contextPath = "/degrees";
var ipadMinWidth = 768;
var ipadMaxWidth = 992;
var width = document.documentElement.clientWidth;
function final5ContSlider(){
  var wi = document.documentElement.clientWidth;
  jQuery.noConflict();
  if(jqy('#mobileFlag') && (jqy('#mobileFlag').val() != 'mobile')){
  if(wi > ipadMaxWidth){
  jqy('.flex_slider').each(function() {
    currentElement ="";
    currentElement = jqy(this).attr('id');
    if(currentElement != undefined){
      jqy('#' + currentElement).flexslider({  
        animation:"slide",
        slideshow: false,
        animationLoop:false,
        slideshowSpeed:4000,
        animationDuration:700,
        keyboardNav:true,
        touch: true,
        video: false,  
        slideToStart:0,
        pauseOnHover:true,
        itemWidth: 193,                   
			     itemMargin: 0, 
        minItems: 5,                    
			     maxItems: 10, 
			     prevText: "",           
			     nextText: "",
        start: function(slider){
          flexSlider = slider;
          jqy(".flex-viewport").removeAttr("style");
          jqy('body').removeClass('loading');
        },
        after: function(slider){ 
          slider.pause(); 
          var nextslide = slider.currentSlide;
          currentElement = slider.attr('id');
          if(nextslide!=0){
            for(var nxtSlide=nextslide*1;nxtSlide<(nextslide + 1);nxtSlide++){
              jqy("#img"+currentElement+"_"+nxtSlide).each(function () {
                var src = jqy("#img"+currentElement+"_"+nxtSlide).attr("data-src");
                jqy("#img"+currentElement+"_"+nxtSlide).attr("src",src).removeAttr("data-src");
              });
            }
          }
        },
        before: function(slider){ 
          slider.pause(); 
        },
      });
    }
  });
  }
  }
}
jqy(window).ready(function(){
  jqy("#scrollContentHidden").val(0);
  swapPosSet();
  changeBtnStyle();
  
  jqy(".hdr_menu ul > li a, #mob_menu_act ul > li a").click(function(event){  
    var a_href = jqy(this).attr('href');  
    if(jqy('#leaveFromUrl')){
      if(a_href!=undefined && a_href!=""){
        jqy('#leaveFromUrl').val(a_href);
      }
    }
    if(jqy('#reorderAction').val() == 'C'){
      openFcLightbox('exitCnfrm');
      event.preventDefault();  	
    }
  });
  jqy("#navKwd").click(function(event){
    jqy('#curPos').val("");
  });
  var refresh = jqy("#refresh");
  refresh.val() == 'yes' ? location.reload(true) : refresh.val('yes');
});
jqy(window).scroll(function() {
  swapPosSet();
});
function swapPosSet(){
  var width = document.documentElement.clientWidth;
  var windowScrollTop = jqy(window).scrollTop();
  if(width > ipadMaxWidth && jqy('.swap_cnt')){
    if(windowScrollTop > 400){
      jqy('.swap_wrp').removeClass('com_swp_pos');
    }else{
      jqy('.swap_wrp').addClass('com_swp_pos');
    }
  }
}
function onScrollFriendsActivity(){
  var ajaxURL = contextPath + "/get-friends-activity-content.html";
  var ajax=new sack();
  ajax.requestFile = ajaxURL;
  ajax.onCompletion = function(){ friendsActivityResponse(ajax)};
  ajax.runAJAX();
}
   
function friendsActivityResponse(ajax){
  var response = ajax.response;
  if(jqy('#friendsActivityPod')){
    jqy('#friendsActivityPod').html(response);
    if(jqy('#ban-div')){
      jqy('#ban-div').addClass('ad_cnr');
    }
  }
}

function onScrollSuggestedPod(podName){
  var ajaxURL = contextPath + "/get-suggested-pod.html?podName=" + podName;
  var ajax=new sack();
  ajax.requestFile = ajaxURL;
  ajax.onCompletion = function(){ sugestedPodResponse(ajax, podName)};
  ajax.runAJAX();
}
   
function sugestedPodResponse(ajax, podName){
  var width = document.documentElement.clientWidth;
  var response = ajax.response;
  final5ContSlider();
  if(jqy('#otherUni') && podName == 'UNI_POD'){jqy('#otherUni').html(response);}
  if(jqy('#otherCourse') && podName == 'COURSE_POD'){jqy('#otherCourse').html(response);}
  jqy("#otherUniLoading").hide();
  jqy("#otherCourseLoading").hide();
  if(jqy('#mobileFlag') && (jqy('#mobileFlag').val() != 'desktop')){
    if(width < ipadMaxWidth){
    viewMoreLess('', 'otherUni');
    viewMoreLess('', 'otherCourse');
    }
  }
  final5ContSlider();
  var wid = document.documentElement.clientWidth;
  if(jqy('#mobileFlag') && (jqy('#mobileFlag').val() == 'ipad')){
    if(wid >= ipadMinWidth && wid <= ipadMaxWidth){
      jqy('.flex_slider .flex-viewport #uniPod').removeAttr("style");
      jqy('#uniPod li').removeAttr("style");
      jqy('#uniPod li').show();
      jqy('.flex_slider .flex-viewport #coursePod').removeAttr("style");
      jqy('#coursePod li').removeAttr("style");
      jqy('#coursePod li').show();
      viewMoreLess('', 'otherUni');
      viewMoreLess('', 'otherCourse');
    }
  }
}

function viewMoreLess(action, podId){
  var wid = document.documentElement.clientWidth;
  var numConst = (wid >= ipadMinWidth) ? 6 : 5;
  if(action == "less"){
    jqy('#' + podId + ' li').each(function() {
      jqy(this).index() >= numConst ? jqy(this).hide() : jqy(this).show();
    });
    if(jqy('#' + podId + ' li').index() > numConst){
      jqy('#'+podId+'_less').hide();
      jqy('#'+podId+'_more').show();
    }
  }else if(action == "more"){
    jqy('#' + podId + ' li').each(function() { jqy(this).show(); });
    if(jqy('#' + podId + ' li').index() > numConst){
      jqy('#'+podId+'_less').show();
      jqy('#'+podId+'_more').hide();
    }
  }else{
    jqy('.slides li').each(function() {
      jqy(this).index() >= numConst ? jqy(this).hide() : jqy(this).show();
    });
    jqy('#coursePod_more, #uniPod_more').show();
    jqy('#uniPod_less, #coursePod_less').hide();
    if(jqy('#' + podId + ' .slides li').index() <= numConst){
      jqy('#'+podId+'_more, #' + podId + '_less').hide();
    }
  }
}

function updateFinalChoice(){
  var curPos = jqy('#curPos').val();
  if(curPos >= 0){
    if(jqy('#courseTitle_' + curPos)){jqy('#courseTitle_' + curPos).hide();}
    if(jqy('#courseId_' + curPos +'_href')){jqy('#courseId_' + curPos +'_href').hide();}
    addRemoveCourse('ADD_COURSE', curPos);
  }
}
jqy('.ca-nav span').click(function(){lazyloadetStarts();});

jqy(document).ready(function(){
  jqy('#subCont').removeClass('ad_bnr');
  if(jqy('#blankStateId') && jqy('#blankStateId').is(':visible')){
    jqy('#compareContainersection').hide();
  }
  //alert(jqy('#mobileFlag').val());
  if(jqy('#mobileFlag') && (jqy('#mobileFlag').val() == 'ipad')){
    var wid = document.documentElement.clientWidth;
    if(wid >= ipadMinWidth && wid <= ipadMaxWidth){
      jqy('#flexSliderCSS').disabled = true;
      }
  }
}); 

function loadCSSResp(flag){
 var wid = document.documentElement.clientWidth;
  if(jqy('#mobileFlag') && (jqy('#mobileFlag').val() == 'ipad')){
  var css = jqy("#flexSliderCSS");
    if(wid >= ipadMinWidth && wid <= ipadMaxWidth){
      if(flag == 'orientation'){
        onScrollSuggestedPod('UNI_POD');
        onScrollSuggestedPod('COURSE_POD');
        final5ContSlider();
      }
    }else{
      jqy('#flexSliderCSS').disabled = false;
      final5ContSlider();
      onScrollSuggestedPod('UNI_POD');
      onScrollSuggestedPod('COURSE_POD');
      final5ContSlider();
    }
  }
}

function leavePage(){
 var a_href = jqy('#leaveFromUrl').val();
 if(a_href!=""){
  location.href = a_href;
 }else{
  hm();
 } 
}