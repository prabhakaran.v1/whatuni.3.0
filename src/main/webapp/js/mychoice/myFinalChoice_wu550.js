var addCrsVal = false;
var clrUsrCnt = false;
var fcJq = jQuery.noConflict(); 
var screenWidth = document.documentElement.clientWidth;
var fnContextPath = "/degrees";

function $$d(id){
  return document.getElementById(id);
}
//
function clearFcDefaultText(obj,setval,index) {
  var oVal = trimString($$d(obj.id).value);  
  if(oVal=="Enter uni" || oVal=="Enter course"){    
    $$d(obj.id).value = "";        
  }  
  $$d("myFinalchVal").value = setval;
  $$d("myFnchPos").value = index;
  $$d("editUniCrsVal").value="";
  hideUserActionPod();  
}
//
function setFcDefaultText(obj,defVal) {  
  var curPos = $$d('myFnchPos').value;   
  var oVal = $$d(obj.id).value;  
  if(oVal == ''){    
    $$d(obj.id).value = defVal;    
  }
  if(defVal!=undefined && defVal=="Enter course"){
    if($$d("usrPenCnt_"+curPos)){
      if(fcJq("#usrPenCnt_"+curPos).html().trim()!=""){     
        fcJq("#usrPenCnt_"+curPos).show();
      }
    }  
    clrUsrCnt = false;  
  }
}
//
function clearFcUniNAME(e,o) {  
if(e.keyCode != 13 && e.keyCode != 9){    
  if($$d(o.id+'_hidden') != null){
    $$d(o.id+"_hidden").value = "";        
    if($$d('myFnchPos')){
      var curPos = $$d('myFnchPos').value;
      if(o.id == "crsName"+curPos){        
        if($$d("crsIdExist_"+curPos)){
          if($$d("crsName"+curPos+"_hidden").value=="" && $$d("crsIdExist_"+curPos).value == "true"){
            $$d("crsIdExist_"+curPos).value = "false";            
          }  
        }        
      }
    }   
  }    
}   
}
//
function switchUniChoice(index,stauts) {    
  resetEntireSlot(index);  
  if($$d("homeFnchDisp")){
    $$d("homeFnchDisp").className = "wu_home pst_ur_f5";
  }        
  if(stauts!="D"){
    editFinalChSlot();
  }  
  if(stauts!="" && stauts!=undefined){  
    fcJq("#finalDefPos_"+index).show();
    fcJq("#finalChPos_"+index).hide();
    fcJq("#finalUniSlot_"+index).hide();
    fcJq("#finalCrsSlot_"+index).hide();
    fcJq("#uniName"+index).hide();
  }else{
    fcJq("#finalDefPos_"+index).hide();             
    fcJq("#finalChPos_"+index).show();
    fcJq("#finalCrsSlot_"+index).hide();    
    fcJq("#finalUniSlot_"+index).show();
    fcJq("#uniName"+index).show();              
  }  
  if($$d("savedChPos_"+index)){
    fcJq("#savedChPos_"+index).hide();
  }    
  changeSlotStyle();  
  hideUserActionPod();
  enableSwapOpt();
  hideAddUni(index);
}
//
function changeSlotStyle(type) {  
  var chUniStyle = false;
  var chCrsStyle = false; 
  for(var i=1;i<=5;i++){    
    if($$d("choicePosStatus_"+i)){
      if(!chUniStyle){
        if($$d("finalUniSlot_"+i).style.display=="" || $$d("finalUniSlot_"+i).style.display=="block"){
          chUniStyle = true;
        }
        if($$d("savedChPos_"+i)){
          if($$d("savedChPos_"+i).style.display=="block"){
            chUniStyle = true;
          }
        }  
      }
      if(chUniStyle){
        if($$d("finalCrsSlot_"+i).style.display=="block"){
          chCrsStyle = true;
        }  
        if($$d("savedChPos_"+i)){
          if($$d("savedChPos_"+i).style.display=="block"){        
            chCrsStyle = true;
          }
        } 
        if(chCrsStyle){
          break;
        }
      }
    } 
  }  
  for(var i=1;i<=5;i++){
    if(($$d("fnchSlotPos_"+i).className)!="fnl5_drag f5_uni f5_cour"){
      if(chCrsStyle && chUniStyle){
        $$d("fnchSlotPos_"+i).className = "fnl5_drag f5_cour";
      }else if(chUniStyle){
        $$d("fnchSlotPos_"+i).className = "fnl5_drag f5_uni"; 
      }else{
        if($$d("fnchSlotPos_"+i)){
          $$d("fnchSlotPos_"+i).className = "fnl5_drag";
        }  
      }
    }    
  }  
  if($$d("fnchCompCnt")){    
    if(type!=undefined && type=="pageload"){ 
      enableSwapOpt();
    }  
  }
  chUniStyle = false;
  chCrsStyle = false;
}  
//
function hideAddUni(curPos){
  for(var i=1;i<=5;i++){
    if($$d("finalUniSlot_"+i)){          
      if(i!=curPos){
        if(($$d("finalUniSlot_"+i).style.display=="block" || $$d("finalUniSlot_"+i).style.display=="") && $$d("uniName"+i+"_hidden").value==""){          
          fcJq("#finalDefPos_"+i).show();
          fcJq("#finalUniSlot_"+i).hide();
          fcJq("#finalChPos_"+i).hide();   
          if($$d("fnchSlotPos_"+i).className=="fnl5_drag f5_uni f5_cour"){
          $$d("fnchSlotPos_"+i).className = "fnl5_drag f5_cour";
        }      
      }
    }    
  }
}
}
//
function resetEntireSlot(curPos){  
  $$d("finalChoiceId_"+curPos).value="";          
  fcJq("textarea#uniName"+curPos).html("");    
  fcJq("#uniName"+curPos).val("Enter uni");  
  $$d("uniName"+curPos+"_hidden").value="";
  $$d("uniName"+curPos).className ="vbox";    
  
  if($$d("fnchSlotPos_"+curPos).className=="fnl5_drag f5_cour"){
    $$d("fnchSlotPos_"+curPos).className = "fnl5_drag f5_uni f5_cour";
  }
  
  fcJq("textarea#crsName"+curPos).html("");   
  fcJq("#crsName"+curPos).val("Enter course");   
  $$d("crsName"+curPos+"_hidden").value="";  
  $$d('finalChPos_'+curPos).style.display="none";           
  
  fcJq("#usrPenCnt_"+curPos).html("");  
  fcJq("#usrPenCnt_"+curPos).hide();
  fcJq("#usrPenAct_"+curPos).hide();
  fcJq("#fnchUsrActSlot_"+curPos).html("");      
  
  if($$d("uniLogo_"+curPos)){
    $$d("uniLogo_"+curPos).src = "";    
  }  
  
  fcJq("#crsTitle_"+curPos).html("");
  fcJq("#uniTitle_"+curPos).html("");  
  fcJq("#svdUsrPenCnt_"+curPos).html("");     
  
  fcJq("#uniLogoShw_"+curPos).hide();    
  fcJq("#uniTxtShw_"+curPos).hide(); 
  
  if($$d("crsIdExist_"+curPos)){
    $$d("crsIdExist_"+curPos).value="false"; 
  }
}
//
function saveFinalChSlot() {  
  var curPos      = $$d('myFnchPos').value; 
  var univId      = $$d('uniName'+curPos);
  var courseId    = $$d('crsName'+curPos);        
  var crsSlotId   = $$d('finalCrsSlot_'+curPos); 
  var usrPenActId = $$d('usrPenAct_'+curPos); 
  var usrPenCntId = $$d('usrPenCnt_'+curPos);
  var hidUnivId   = $$d('uniName'+curPos+'_hidden');   
  var hidCourseId = $$d('crsName'+curPos+'_hidden');                
  
  if(hidCourseId.value!=""){
    courseId.className ="vbox focus";
    courseId.innerHTML = courseId.value;    
    updateFinalChSlot(curPos,'E');        
  }else if(hidUnivId.value!=""){      
    univId.className ="vbox focus";
    courseId.className ="vbox";    
    univId.innerHTML = univId.value;    
    updateFinalChSlot(curPos,'E');      
  }  
}
//
function updateFinalChSlot(curPos,status){
  var queryString = "";      
  var finalChoiceId = $$d('finalChoiceId_'+curPos).value;                
  
  if(status!=""){
    var hidUnivId = $$d('uniName'+curPos+'_hidden').value;
    var hidCrsId  = $$d('crsName'+curPos+'_hidden').value;        
    if(hidUnivId!=""){
      queryString = "&collegeId="+hidUnivId;
    }    
    if(hidCrsId!=""){
      queryString = queryString + "&courseId="+hidCrsId;
    }            
  }          
  if(finalChoiceId!=""){
    queryString = queryString + "&fnChoiceId="+finalChoiceId;
  }              
  if(finalChoiceId=="" && status=='D'){
    switchUniChoice(curPos,'D');
    $$d("choicePosStatus_"+curPos).value = "N";
    $$d("fnchSlotPos_"+curPos).className = "fnl5_drag f5_cour";
    changeSlotStyle();
  }else{  
    queryString = "?myaction=save&slotPosId=" + curPos + queryString + "&slotPosStatus="+status;          
    var ajaxURL = fnContextPath+"/myfinalchoice.html" + queryString;                      
    
    var ajax=new sack();
    ajax.requestFile = ajaxURL;
    ajax.onCompletion = function(){ slotPodResponse(ajax,status,curPos); };
    ajax.runAJAX();      
  }
}
//
function slotPodResponse(ajax,status,curPos){
  var newFinalPosId = "";  
  var usrStatusCnt  = "";
  var uniProvLogo   = "";    
  if(ajax.response!="EXIST"){  
  var crsSlotId     = $$d('finalCrsSlot_'+curPos);
  var usrPenActId   = $$d('usrPenAct_'+curPos); 
  var fnChSplitDetails = (ajax.response).split("#_#CHOICECNT#_#");    
  var splitDetails  =  fnChSplitDetails[0].split("SUCEESS###");                      
  if(splitDetails.length>0){
    if(status=="E"){      
      var uniTitle = $$d("uniName"+curPos).value;
      if(uniTitle!="") {
        if(uniTitle.length > 51){
          uniTitle = uniTitle.substr(0, 48) + "...";
        } 
        $$d("uniTitle_"+curPos).innerHTML = uniTitle;        
      }        
      if(splitDetails.length >2) {            
        newFinalPosId = splitDetails[1];    
        if(newFinalPosId!="" && newFinalPosId!=undefined){
          $$d('finalChoiceId_'+curPos).value = newFinalPosId;          
        }
        if(splitDetails.length==3){          
          usrStatusCnt = splitDetails[2];          
        }else{                  
          uniProvLogo  = splitDetails[2];
          usrStatusCnt = splitDetails[3];        
        }  
        
        if(uniProvLogo!="" && uniProvLogo!=undefined){
          if($$d("uniLogo_"+curPos)){                                
            fcJq('#uniName'+curPos).hide();
            $$d("uniLogo_"+curPos).src = uniProvLogo;                                 
            $$d("uniTitle_"+curPos).innerHTML = "";                        
            blockNone("uniLogoShw_"+curPos, "block");
          }               
        }else{
          fcJq('#uniName'+curPos).hide();          
          blockNone("uniTxtShw_"+curPos, "block");
          blockNone("uniLogoShw_"+curPos, "none");
        }
      }else{        
        usrStatusCnt = splitDetails[1];
      }      
      if(usrStatusCnt!="" && usrStatusCnt!=undefined){
        $$d('usrPenCnt_'+curPos).innerHTML = usrStatusCnt;                  
      }            
      if(fcJq("#usrPenCnt_"+curPos).html().trim()!=""){
        fcJq('#usrPenAct_'+curPos).show();
        fcJq("#usrPenCnt_"+curPos).show();                
      }
      if(crsSlotId.style.display=="none"){
        crsSlotId.style.display="block";    
        changeSlotStyle();                          
      }     
      $$d('choicePosStatus_'+curPos).value = "E";
      $$d('slotClr_'+curPos).className = "Br_lne bg_white";      
    }else if(status=="D"){        
      switchUniChoice(curPos,'D');
      $$d("fnchSlotPos_"+curPos).className = "fnl5_drag f5_cour";
      $$d('choicePosStatus_'+curPos).value = "N";
      changeSlotStyle();
    }
  }  
  if(fnChSplitDetails[1]!="" && fnChSplitDetails[1]!=undefined){
    if($$d("fnchCompCnt")){
      $$d("fnchCompCnt").value = fnChSplitDetails[1];     
      enableSwapOpt();
      if($$d("mychNavCnt")){
        $$d("mychNavCnt").innerHTML = fnChSplitDetails[1];
      }
    }  
  }  
  var confirmedUnis="";
    for(var z=1;z<=5;z++){
      var uniNameId = 'uniName'+z+'_hidden';
      if($$d(uniNameId) && $$d(uniNameId).value != ""){
        confirmedUnis = confirmedUnis + "," + $$d(uniNameId).value;  
      }      
    }
    $$d("confirmedUniIds").value=confirmedUnis;
  }else{
    openFcLightbox('crsExist');    
  }
}
//
function redirectFnChUniHOME(o,formObjId){
  var uniName = "";
  var	uniId = "";           
  uniName = trimString(o.value);    
  if($$(o.id+"_hidden")){   
    uniId = trimString($$(o.id+"_hidden").value);
  }  
  return false;
}
//
function usrStatusActionPod(curPos,objId){  
  var queryString = "";            
  var userActSlotPod =  $$d(objId);    
  if(userActSlotPod){
    var jqId = "#"+objId;      
    if(fcJq(jqId).html().trim().length==0){            
      var hidUnivId = $$d('uniName'+curPos+'_hidden').value;
      var hidCrsId  = $$d('crsName'+curPos+'_hidden').value;        
      if(hidUnivId!=""){
        queryString = "&collegeId="+hidUnivId;
      }    
      if(hidCrsId!=""){
        queryString = queryString + "&courseId="+hidCrsId;
      }                                   
      if(queryString!=""){
        queryString = "?myaction=userAction" + queryString;          
      }      
      var ajaxURL = fnContextPath+"/myfinalchoice.html" + queryString;
      var ajax=new sack();
      ajax.requestFile = ajaxURL;
      ajax.onCompletion = function(){displayUserStatusPod(ajax,curPos,userActSlotPod);};
      ajax.runAJAX();      
    }else{            
      if((userActSlotPod).style.display=="none"){
        userActSlotPod.style.display="block";
      }else{
        userActSlotPod.style.display="none";
      }
      hideUserActionPod(curPos);
    }
  }
}
//
function displayUserStatusPod(ajax,curPos,objId) {    
  var userActSlotPod = objId;  
  if(userActSlotPod){
    userActSlotPod.innerHTML = ajax.response;
    if(userActSlotPod.innerHTML!=""){
      userActSlotPod.style.display="block";
      hideUserActionPod(curPos);
    }    
  }  
}
//
function loadConfirmRes(ajax){      
  if(ajax.response!="" && ajax.response=="SUCCESS"){        
    if($$d("confirmedUniIds") && $$d("confirmedUniIds").value!=""){
      FinalFiveAnalyticsEventsLogging('Final 5', 'Confirmed', 'confirmedUniIds');
    }
    openFcLightbox('confirmslots'); 
  }
}
//
function reLoadHomePageRespView(){
  var ajaxURL = fnContextPath+"/myfinalchoice.html?myaction=homepageTapView";          
  var ajax=new sack();
  ajax.requestFile = ajaxURL;  
  ajax.onCompletion = function(){loadHomePageTapView(ajax);};
  ajax.runAJAX();  
}
function loadHomePageTapView(ajax){  
  fcJq("#myfinalHome").html(ajax.response);  
}
//
function editFinalChSlot(){  
  for(var i=1;i<=5;i++){    
    if($$d("choicePosStatus_"+i)){    
      if($$d("choicePosStatus_"+i).value=="C"){
        if($$d('savedChPos_'+i)){          
          blockNone('savedChPos_'+i,"none");
        }
        blockNone('finalUniSlot_'+i,"block");
        blockNone('finalCrsSlot_'+i,"block");
        blockNone('usrPenAct_'+i,"block");
        blockNone('usrPenCnt_'+i,"block");
        blockNone('cancelSlotCh_'+i,"block");                
        $$d('choicePosStatus_'+i).value = "E";       
        if($$d('crsName'+i+'_hidden').value!=""){          
          $$d("crsName"+i).className ="vbox focus";       
        }
        $$d("slotClr_"+i).className ="Br_lne bg_white";        
      }
    }
  }
  changeButtonAction();
}
//
function loadFnChTicker() {  
  var fnchSlots = document.getElementsByClassName('myFnChLbl');
  var courseId        = "";
  var collegeId       = "";
  var collegeDispName = "";
  var courseTitle     = "";
  var collegeDetails  = "";
  var courseDetails   = "";
  var addFnchSlots    = "";
  var addFnchSlotSplit= "";
  var startTag        = "";
  var endTag          = "";
  var dispFnchSlot    = "";
      screenWidth     = document.documentElement.clientWidth;
  for (var len = 0; len < fnchSlots.length; len++) {        
    addFnchSlots = fnchSlots[len].innerHTML; 
    addFnchSlotSplit = addFnchSlots.split("##");
    if(addFnchSlotSplit.length>1){      
      collegeDetails  = addFnchSlotSplit[0].split("#"); 
      courseDetails   = addFnchSlotSplit[1].split("#"); 
      collegeDispName = collegeDetails[0];    
      collegeId       = collegeDetails[1];
      courseTitle     = courseDetails[0];    
      courseId        = courseDetails[1];      
      if(screenWidth>992){
        if(collegeDispName.length > 68){
          collegeDispName = collegeDispName.substr(0, 65) + "...";
        }
        if(courseTitle.length > 76){
          courseTitle = courseTitle.substr(0, 72) + "...";
        }
      }else if(screenWidth >=320 && screenWidth<768){
        if(collegeDispName.length > 30){
          collegeDispName = collegeDispName.substr(0, 26) + "...";
        }
        if(courseTitle.length > 32){
          courseTitle = courseTitle.substr(0, 28) + "...";
        }        
      }else if(screenWidth >=768 && screenWidth<=992){ 
        if(collegeDispName.length > 26){
          collegeDispName = collegeDispName.substr(0, 22) + "...";
        }
        if(courseTitle.length > 30){
          courseTitle = courseTitle.substr(0, 26) + "...";
        }        
      }
      endTag = "<h2>"+collegeDispName+"</h2><p>"+courseTitle+"</p></div><div class=\"f5_msg_btn\"><a href=\"javascript:addToFinalChSlot('"+collegeId+"','"+courseId+"','"+len+"');\"><i class=\"fa fa-plus-square\"></i> ADD TO FINAL 5</a></div></li>";      
    }else{      
      collegeDetails  = addFnchSlotSplit[0].split("#"); 
      collegeDispName = collegeDetails[0];  
      collegeId       = collegeDetails[1];          
      if(screenWidth>992){
        if(collegeDispName.length > 68){
          collegeDispName = collegeDispName.substr(0, 65) + "...";
        }       
      }else if(screenWidth >=320 && screenWidth<768){
        if(collegeDispName.length > 30){
          collegeDispName = collegeDispName.substr(0, 26) + "...";
        }      
      }else if(screenWidth >=768 && screenWidth<=992){ 
        if(collegeDispName.length > 26){
          collegeDispName = collegeDispName.substr(0, 22) + "...";
        }
      }  
      endTag          = "<h2 class=\"f5_uni_pos\">"+collegeDispName+"</h2></div><div class=\"f5_msg_btn\"><a href=\"javascript:addToFinalChSlot('"+collegeId+"','','"+len+"');\"><i class=\"fa fa-plus-square\"></i> ADD TO FINAL 5</a></div></li>";
    }    
    if(dispFnchSlot==""){
      startTag     = "<li class=\"slide_cont\" id=\"fnchTick_"+len+"\"><div class=\"f5_msg_cnt\">";
      dispFnchSlot = startTag + endTag;       
    }else{
      startTag     = "<li class=\"slide_cont\" id=\"fnchTick_"+len+"\"><div class=\"f5_msg_cnt\">";
      var temp     = startTag + endTag;
      dispFnchSlot = dispFnchSlot + temp;
    }                         
  }  
  if(dispFnchSlot!=""){      
    $$d("addTofnchTicker").innerHTML = dispFnchSlot;    
    if($$d("addTofnchTicker").innerHTML!=""){
      blockNone("tickerTape", "none");
      blockNone("addTofnchTickerList", "block");      
    }  
  }           
}
//
fcJq(window).resize(function() {       
  changeTickerStyle();
  blockNone("swapStatus", "none");
});
function changeTickerStyle(){
  screenWidth = document.documentElement.clientWidth;
  if($$d("addTofnchTicker")){
    if($$d("tickerNonRespPage")){
      if((screenWidth > 768) && (screenWidth <= 992)) {    
        dev(".fnchtick ul .slide_cont").addClass("f5_cont_li"); 
      }else{    
        dev(".fnchtick ul .slide_cont").removeClass("f5_cont_li"); 
      }
    } 
  }
}
//
function closeFnch(){
  var $fnch = jQuery.noConflict();
  var fnChTickerSlideDc = "#fnchTick_";
  var fnChTickerSlide = "",temp = "";  
  if($$d("addToFnSlot")){
    if($$d("addToFnSlot").value!=""){  
      fnChTickerSlide = fnChTickerSlideDc + $$d("addToFnSlot").value + " .f5_msg_btn";            
      temp = fnChTickerSlideDc + $$d("addToFnSlot").value + "0" + " .f5_msg_btn";       
      if($fnch(fnChTickerSlide).length!=0){      
        $fnch(fnChTickerSlide).hide(); 
        $fnch(temp).hide();
      }
    }   
  }      
  if($fnch('.fnchtick ul li').length == 0){
    $fnch("#addTofnchTickerList").hide();
    $fnch("#tickerTape").show();        
  }
  hm();
}
//
function dispMyfnchSugestPod(ajaxObj){      
  $$d("finalChSugestPod").innerHTML = ajaxObj.response;  
}
//
function closeFinalFivePod(){  
  $$d("addTofnchTickerList").style.display = "none";  
  blockNone('tickerTape','block');
}
////////////////////////////////////////////////////////////////////
function blockNone(thisid, value){  if($$d(thisid) !=null){ $$d(thisid).style.display = value;  }  }
//
function hideSliderArrows(){
  blockNone('finalFivePrev','none');
  blockNone('finalFiveNext','none');
}
//
function checkFinalFiveCookie(){  
  var showFinalFive=getFinalFiveCookie("showFinalFive");
  if (showFinalFive!=null && showFinalFive=="Dont"){        
    blockNone('addTofnchTickerList','none');
    blockNone('tickerTape','block');
  }
}
//
function getFinalFiveCookie(c_name){
  var c_value = document.cookie;
  var c_start = c_value.indexOf(" " + c_name + "=");
  if (c_start == -1){
      c_start = c_value.indexOf(c_name + "=");
  }
  if (c_start == -1){
      c_value = null;
  }
  else{
      c_start = c_value.indexOf("=", c_start) + 1;
      var c_end = c_value.indexOf(";", c_start);
      if (c_end == -1){
          c_end = c_value.length;
      }
      c_value = unescape(c_value.substring(c_start,c_end));
  }
  return c_value;
}
var $sty = jQuery.noConflict();  
$sty(document).ready(function(){
  setTimeout(function(){javaScript:stickyBottom();},1000);  
});
$sty(window).scroll(function(){
  var styHeight = $sty( window ).height() - 88;  
  if(styHeight > 500){
    $sty('#addTofnchTickerList').addClass('f5_sticky_foo');
    $sty('#addTofnchTickerList').css({ position: 'fixed', top: styHeight, 'z-index': '99999' });
  }
});
function stickyBottom(){
  var styHeight = $sty( window ).height() - 88;  
  if(styHeight > 568){
    $sty('#addTofnchTickerList').addClass('f5_sticky_foo');
    $sty('#addTofnchTickerList').css({ position: 'fixed', top: styHeight, 'z-index': '99999' });
  }
}
//
function openFcLightbox(messageType){
 sm('newvenue', 650, 500, 'final5lbb');	 
 var fromHome = "";
 if($$d("myfnchMainpage")){
  fromHome = "&fromFnchHome=true";
 }
 url = contextPath + "/jsp/myfinalchoice/include/errorMessageLb.jsp?pageType="+messageType+fromHome;  
 var ajaxObj = new sack();
 ajaxObj.requestFile = url;	      
 ajaxObj.onCompletion = function(){displayFormData(ajaxObj);};	
 ajaxObj.runAJAX();   
}
//
function loadfnchUrsCnt(count){  
  $$d("fnchCompCnt").value = count;   
}
//
function clearusrCnt(pos){    
  if(!clrUsrCnt){
    var curPos = $$d('myFnchPos').value;         
    if(curPos==""){
      curPos = pos;
    }    
    fcJq("#usrPenCnt_"+curPos).hide();    
    var hidUnivId   = $$d('uniName'+curPos+'_hidden');
    var hidCourseId = $$d('crsName'+curPos+'_hidden');            
    if($$d("crsIdExist_"+curPos)){                        
      if(fcJq("textarea#crsName"+curPos).val()!="Enter course" && hidCourseId.value=="" && hidUnivId.value!=""){
        $$d("crsIdExist_"+curPos).value = "false";
      }                  
      if(hidCourseId.value=="" && hidUnivId.value!="" && $$d("crsIdExist_"+curPos).value=="false"){        
        updateFinalChSlot(curPos,'E');        
        $$d("crsIdExist_"+curPos).value = "";
        $$d('crsName'+curPos).className ="vbox";        
      }        
    } 
    clrUsrCnt = true;  
  }  
}
//////////////To Edit Final choice pod//////////////////////////////////////////////////////
function editMyFinalchoiceSlot(param){
  if(param==undefined){
    editFinalChSlot();
    enableSwapOpt();
  }  
  var ajaxURL = fnContextPath+"/myfinalchoice.html?myaction=editFnch";                                
  var ajax=new sack();
  ajax.requestFile = ajaxURL;  
  ajax.onCompletion = function(){viewMyFnchEditMode(ajax,param)};
  ajax.runAJAX();      
}
////////////////////////Redirect to myfinal choice to edit mode////////////////////////////
function viewMyFnchEditMode(ajax,param){
  if(param!=undefined && param=="edit"){    
    if(ajax.response=="SUCEESS"){
      location.href = fnContextPath+"/myfinalchoice.html";
    }  
  }  
}
//////////////////////To change Button from State from Confrim to Edit vice versa///////////
function changeButtonAction(){
  if($$d("dispConfirmButStyle")){    
    blockNone("dispConfirmButStyle", "block");
    blockNone("dispFinalChEditStyle", "none");
  }
  if($$d("inButdispFinalChEditStyle")){    
    blockNone("inButdispFinalChEditStyle", "none");
    blockNone("inButdispConfirmButStyle", "block");
  }
  if($$d("mychEditLightbox")){    
    if($$d("cnfrmEditLightbox")){
      if(!$$d("myfinalHome")){        
        blockNone("mychEditLightbox", "none");
        blockNone("cnfrmEditLightbox", "block");        
      }  
    }   
  }  
}
////////////////////Add from Suggestion pod////////////////////////
function addSugtoFnchSlot(sugPos){    
  var queryString = "";
  var drpUnivId = $$d('drpColId_'+sugPos).value;
  var drpCrsId  = $$d('drpCrsId_'+sugPos).value;  
  
  if(drpUnivId!=""){
    queryString = "&collegeId="+drpUnivId;
  }    
  if(drpCrsId!=""){
    queryString = queryString + "&courseId="+drpCrsId;
  }
  queryString = "?myaction=save" + queryString + "&slotPosStatus=E";          
  var ajaxURL = fnContextPath+"/myfinalchoice.html" + queryString;    
  if($$d("sugChoicegif")){
    $$d("sugChoicegif").style.display="block";
  }
  hideSugOpt();
  var ajax=new sack();
  ajax.requestFile = ajaxURL;
  ajax.onCompletion = function(){reloadFinalchoicePod(ajax,'suggestpod')};
  ajax.runAJAX(); 
}
/////////////////////////Reload Final choice pod when same course is added/////////
function refreshFinalChoice(){
  screenWidth = document.documentElement.clientWidth;
  if((screenWidth >=320) && (screenWidth <= 992)) {
    location.href = fnContextPath+"/myfinalchoice.html";
  }else{
    if($$d("choice_container")){
      var ajaxURL = fnContextPath+"/myfinalchoice.html?myaction=loadSwapChoicePod";          
      var ajax=new sack();  
      ajax.requestFile = ajaxURL;  
      ajax.onCompletion = function(){reloadFinalChoiceView(ajax,'refresh')};
      ajax.runAJAX();  
    }else{
      hm();
    }
  }  
}
/////////////////////////Reload Final choice pod////////////////////////
function reloadFinalchoicePod(ajax,param){   
  var splitRes = (ajax.response).split("#_#CHOICECNT#_#");  
  if(ajax.response=="SUCCESS" || splitRes[0]=="SUCCESS"){
    var ajaxURL = fnContextPath+"/myfinalchoice.html?myaction=loadSwapChoicePod";          
    var ajax=new sack();  
    ajax.requestFile = ajaxURL;  
    ajax.onCompletion = function(){reloadFinalChoiceView(ajax,param)};
    ajax.runAJAX();  
    
    if(splitRes[1]!=undefined && splitRes[1]!=""){    
      if($$d("fnchCompCnt")){
        $$d("fnchCompCnt").value = splitRes[1];        
        if($$d("mychNavCnt")){
          $$d("mychNavCnt").innerHTML = splitRes[1];
        }
      }    
    }
  }else{
    if(ajax.response=="EXIST"){
      reloadSuggestionPod();
      openFcLightbox('crsExist');
    }else{    
      openFcLightbox('fiveslots');
    }  
  }
}
//////////////////////////////Reload Final choice pod view///////////////
function reloadFinalChoiceView(ajax,param){  
  screenWidth = document.documentElement.clientWidth;
  if($$d("swapLoaderGif")){
    $$d("swapLoaderGif").style.display = "none";      
    if((screenWidth >=320) && (screenWidth <= 992)) {      
      blockNone("swapStatus", "block");            
    }else{
      blockNone("swapStatus", "none");
    }
  }  
  fcJq("#choice_container").html(ajax.response);    
  changeSlotStyle('pageload');
  changeButtonAction();
  if(param!=undefined && param=="suggestpod"){    
    if($$d("mblOrient")){
      if($$d("mblOrient").value==""){
        skipLinkSetPosition('choice_container');
      }  
    }  
    reloadSuggestionPod();    
  }else if(param!=undefined && param=="refresh"){
    hm();
  }  
}
/////////////////////////////Reload Final choice Suggestion pod/////////////
function reloadSuggestionPod(){
  var url = fnContextPath+"/myfinalchoice.html?myaction=fnchChSugestAjaxPod";      
  var ajaxObj = new sack();
  ajaxObj.requestFile = url;	      
  ajaxObj.onCompletion = function(){dispMyfnchSugestPod(ajaxObj);};	
  ajaxObj.runAJAX();     
}
///////////////////////Adding from Ticker tape to Final choice Lightbox///////
function addToFinalChSlot(collegeId,courseId,curpos){
  var queryString = ""; 
  var status      = "E";
  var addFinalHideId = "addFinal_";  
  if(collegeId!=undefined && collegeId!=""){
    queryString = "&collegeId="+collegeId;
    addFinalHideId = addFinalHideId+collegeId;
  }
  if(courseId!=undefined && courseId!="" && courseId!="0"){
    queryString = queryString + "&courseId="+courseId;    
  }  
  queryString = "?myaction=save" + queryString + "&slotPosStatus="+status;
  var ajaxURL = fnContextPath+"/myfinalchoice.html" + queryString + "&addedFromSrch=true";  
  var ajax=new sack();
  ajax.requestFile = ajaxURL;
  ajax.onCompletion = function(){showFinalChLightBox(ajax,curpos, '', addFinalHideId);};
  ajax.runAJAX();
}
//////////////////Display Final choice Lightbox//////////////////////////////

function showFinalChLightBox(ajax,curpos,extraParam, addFinalHideId) {  
  screenWidth  = document.documentElement.clientWidth;
  var splitRes = (ajax.response).split("#_#CHOICECNT#_#");   
  if(splitRes[0]=="SUCCESS"){    
    if(addFinalHideId != '0'){      
      dev('#'+addFinalHideId).addClass('btn_view disp_off'); 
    }
    if(splitRes[1]!=undefined && splitRes[1]!=""){    
      if($$d("fnchCompCnt")){
        $$d("fnchCompCnt").value = splitRes[1];        
        if($$d("mychNavCnt")){
          $$d("mychNavCnt").innerHTML = splitRes[1];
        }
      }    
    }
    var extParam = "";    
    if((screenWidth >=320) && (screenWidth <= 992)) {             
      location.href = fnContextPath+"/myfinalchoice.html";
    }else{	
      sm('newvenue','650','500', 'finalChoice');  
      if(extraParam!=undefined && extraParam!="") {
       extParam = "&extParam=true"; 
      }      
      var url = fnContextPath+"/myfinalchoice.html?myaction=showFnchLightbox"+extParam;          
      $$d("ajax-div").style.display="block";
      var ajaxObj = new sack();
      ajaxObj.requestFile = url;	      
      ajaxObj.onCompletion = function(){displayFormData(ajaxObj);};	
      ajaxObj.runAJAX();  
      if(curpos!==undefined && curpos!=""){
        $$d('addToFnSlot').value = curpos;     
      }    
    }    
  }else{
    if(splitRes[0]=="EXIST"){      
      openFcLightbox('crsExist');
    }else{    
      openFcLightbox('fiveslots');
    }  
  }	
}
///////////////////////////////Invoked to check if swap can be done///////////////////////
function positionSwap(){  
  var fromSwapId = "";
  var toSwapId = "";
  var fromSwpfnchId = "";
  var toSwpfnchId = "";
  var fromSwpStatus = "";
  var toSwpStatus = "";
  var stopSwap = "";
  
  if($$d("swapFrom")){
    fromSwapId = $$d("swapFrom").value;
  }  
  if($$d("swapTo")){
    toSwapId = $$d("swapTo").value;
  }    
  
  if(fromSwapId == toSwapId){    
    stopSwap = "true";
    openFcLightbox('stopSameslot');   
  }else if(fromSwapId!= toSwapId){  
    if($$d("finalChoiceId_"+fromSwapId)) {  
      fromSwpfnchId = $$d("finalChoiceId_"+fromSwapId).value;      
    }  
    if($$d("finalChoiceId_"+toSwapId)) {
      toSwpfnchId = $$d("finalChoiceId_"+toSwapId).value      
    }      
    fromSwpStatus = $$d("choicePosStatus_"+fromSwapId).value;
    toSwpStatus = $$d("choicePosStatus_"+toSwapId).value;            
    
    if(fromSwpStatus=="N" && toSwpStatus=="N"){
      stopSwap = "true";
      openFcLightbox('stopEmptyslot');       
    }      
  }    
  if(stopSwap!="true"){    
    blockNone("swapLoaderGif", "block");
    swapMyFinalChoicePos(toSwpfnchId,fromSwapId,fromSwpfnchId,toSwapId);
  }
}
///////////////////////Swaping action will be performed//////////////////
function swapMyFinalChoicePos(finalChId1,finalChPos1,finalChId2,finalChPos2){
  var queryString = "";   
  if(finalChId1!="" && finalChId1!=undefined){
    queryString = "finalChId1="+finalChId1;
  }
  if(finalChId2!="" && finalChId2!=undefined){
    if(queryString!=""){
      queryString = queryString + "&finalChId2="+finalChId2;
    }else{
      queryString = queryString + "finalChId2="+finalChId2;
    }  
  }
  var url = fnContextPath+"/myfinalchoice.html?myaction=swapMyChoicePos&"+queryString+"&finalChPos2="+finalChPos2+"&finalChPos1="+finalChPos1;      
  var ajaxObj = new sack();
  ajaxObj.requestFile = url;	        
  ajaxObj.onCompletion = function(){reloadFinalchoicePod(ajaxObj)};	
  ajaxObj.runAJAX();     
}
////////////////////////////load swap choice position text///////////////////
function showSwapPosText(selObj,dispObj){  
  var selectedObject = $$(selObj);
  var selectedValue  = selectedObject.options[selectedObject.selectedIndex].text;
  $$(dispObj).innerHTML = selectedValue;  
  blockNone("swapStatus", "none");        
}
///////////////////Choice slot position confirmed///////////////////////////
function confirmFinalChSlot(){  
  if($$d("fnchCompCnt") && $$d("fnchCompCnt").value==0){
   openFcLightbox('zeroslots');  
  }else{
    var ajaxURL = fnContextPath+"/myfinalchoice.html?myaction=confirm";                                
    var ajax=new sack();
    ajax.requestFile = ajaxURL;  
    ajax.onCompletion = function(){loadConfirmRes(ajax);};
    ajax.runAJAX();          
  }
}
////////////////choice slot confirmation from lightbox////////////////////////
function confirmMySlot(){  
  if($$d("fnchCompCnt") && $$d("fnchCompCnt").value==0){   
   openFcLightbox('zeroslots');  
  }else{
    closeFnch();
    if($$d("confirmedUniIds") && $$d("confirmedUniIds").value!=""){
      FinalFiveAnalyticsEventsLogging('Final 5', 'Confirmed', 'confirmedUniIds');
    }
    var ajaxURL = fnContextPath+"/myfinalchoice.html?myaction=confirm";
    var ajax=new sack();
    ajax.requestFile = ajaxURL;  
    ajax.onCompletion = function(){showFinalChLightBox(ajax,'','edit', '0');};
    ajax.runAJAX();      
  }
}
//
function skipLinkSetPosition(divname){
  var $fp = jQuery.noConflict();     
  var divPosition = $fp('#'+divname).offset();
  $fp('html, body').animate({
      scrollTop: divPosition.top - 100
      }, "slow");
}
//
function hideSugOpt(param){
  if(param!=undefined && param=="show"){
    if($$d("sugChoicegif")){      
      blockNone("sugChoicegif", "none");
    }
    hm();
  }
  for(i=1;i<=5;i++){
    if($$d("addSugBtn_"+i)){
      if(param!=undefined && param=="show"){        
        blockNone("addSugBtn_"+i, "block");
      }else{        
        blockNone("addSugBtn_"+i, "none");
      }  
    }    
  }  
}
//
function hideUserActionPod(curPos){
for(i=1;i<=5;i++){
  if($$d("fnchUsrActSlot_"+i) || $$d("svdFnchUsrActSlot_"+i)){  
    if(curPos!=undefined && curPos!=""){      
      if(i!=curPos){
        if($$d("fnchUsrActSlot_"+i)){
          if($$d("fnchUsrActSlot_"+i).style.display=="block"){                    
            blockNone("fnchUsrActSlot_"+i, "none");
          }                
        }
        if($$d("svdFnchUsrActSlot_"+i)){
          if($$d("svdFnchUsrActSlot_"+i).style.display=="block"){                    
            blockNone("svdFnchUsrActSlot_"+i, "none");
          }        
        }
      }
    }else{      
      if($$d("fnchUsrActSlot_"+i)){
        if($$d("fnchUsrActSlot_"+i).style.display=="block"){                  
          blockNone("fnchUsrActSlot_"+i, "none");
        }      
      }
      if($$d("svdFnchUsrActSlot_"+i)){
        if($$d("svdFnchUsrActSlot_"+i).style.display=="block"){                  
          blockNone("svdFnchUsrActSlot_"+i, "none");
        }      
      }  
    }
  }    
}
}
//
function enableSwapOpt(){
if($$d("shwSwapOpt")){
  if($$d("inButdispConfirmButStyle").style.display=="block"){
    if($$d("fnchCompCnt").value>=2){        
      fcJq("#shwSwapOpt").show();
    }else{        
      fcJq("#shwSwapOpt").hide();
    }
  }  
} 
}
//
function setMyfnchPos(pos){  
  $$d("myFnchPos").value = pos;
}
//
fcJq(document).ready(function(){
fcJq(".hdr_menu ul > li a").click(function(event){  
  var a_href = fcJq(this).attr('href');  
  if($$d("fnch_a_href")){
    if(a_href!=undefined && a_href!=""){
      $$d("fnch_a_href").value = a_href;
    }
  }  
  if($$d("exitFnchCnrfm")){
    if($$d("dispConfirmButStyle")){      
      var fnCount = $$d("fnchCompCnt").value;      
      if($$d("exitFnchCnrfm").value=="false" && $$d("dispConfirmButStyle").style.display=="block" && parseInt(fnCount)>0) {
        exitFnchConfirm();
        event.preventDefault();  	
      }	
    }
  }
}); 
});
//
function exitFnchConfirm(){
  openFcLightbox('exitCnfrm');
}
//
function resetCnfChoice(){
 $$d("exitFnchCnrfm").value="true"; 
 var a_href = $$d("fnch_a_href").value;
 if(a_href!=""){
  location.href = a_href;
 }else{
  hm();
 } 
}
