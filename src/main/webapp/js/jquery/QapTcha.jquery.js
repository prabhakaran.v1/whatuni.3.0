jQuery.QapTcha = {
	build : function(options)
	{
        var defaults = {
			txtLock : 'Locked : form can\'t be submited',
			txtUnlock : 'Unlocked : form can be submited',
            txtSlider : 'Slide to submit',
			disabledSubmit : true
        };		
		if(this.length>0)
		return jQuery(this).each(function(i) {
			/** Vars **/
			var 
				opts = $.extend(defaults, options),      
				$this = $(this),
				form = $('form').has($this),
				Clr = jQuery('<div>',{'class':'clr'}),
				bgSlider = jQuery('<div>',{id:'bgSlider'}),
                Slider = jQuery('<div>',{id:'Slider'}),
			//	Icons = jQuery('<div>',{id:'Icons'}),
		//		TxtStatus = jQuery('<div>',{id:'TxtStatus',text:opts.txtLock}),
				inputQapTcha = jQuery('<input>',{name:'iQapTcha',value:generatePass(),type:'hidden'});
			
			/** Disabled submit button **/
			if(opts.disabledSubmit) form.find('input[type=\'submit\']').attr('disabled','disabled');
			
			/** Construct DOM **/
			bgSlider.appendTo($this);
		//	Icons.insertAfter(bgSlider);

			//Clr.insertAfter(Icons);
		//	TxtStatus.insertAfter(Clr);
			inputQapTcha.appendTo($this);
			Slider.appendTo(bgSlider);
			$this.show();
			Slider.draggable({
				containment: bgSlider,
				axis:'x',
    
 			revert: function(event){
 					        if(opts.autoRevert)
 					        {
						          if(parseInt(Slider.css("left")) > (bgSlider.width()-Slider.width()-1)) return false;
						          if(parseInt(Slider.css("left")) > (bgSlider.width()-Slider.width()-10)) return false;
 						         else return true;
 					        }
            },
				stop: function(event,ui){
                                var leftPs = ui.position.left;
                                var totalwidth = bgSlider.width();
                                var pc = (leftPs/totalwidth)*100;
                                if(screenWidth <= 992 && pc < 80){
                                 Slider.css( { left: "0px"} );
                                }
					if( pc > 80)
                                
                                
					//if(ui.position.left > 180)
					{
						Slider.draggable('disable').css('cursor','default');
						inputQapTcha.val("");
                        bgSlider.addClass("sucess");                        
                     //   TxtStatus.text(opts.txtSlider);
					//	TxtStatus.css({color:'#307F1F'}).text(opts.txtUnlock);
					//	Icons.css('background-position', '-16px 0');
						if(opts.disabledSubmit) form.find('input[type=\'submit\']').removeAttr('disabled');
                        
					}
					
				}
			});
			
			function generatePass() {
		        var chars = 'azertyupqsdfghjkmwxcvbn23456789AZERTYUPQSDFGHJKMWXCVBN';
		        var pass = '';
		        for(i=0;i<10;i++){
		            var wpos = Math.round(Math.random()*chars.length);
		            pass += chars.substring(wpos,wpos+1);
		        }
		        return pass;
		    }
			
		});
	}
}; jQuery.fn.QapTcha = jQuery.QapTcha.build;