function dynamicVideoPlayerJsLoad(videoPath,coverimage){
  var script = document.createElement("script");
  script.type = "text/javascript";  
  script.src = $$('contextJsPath').value + '/js/video/jwplayer.js'; //Common path code added by Prabha on 27-JAN-16
  if(script.readyState){//IE
    script.onreadystatechange = function(){
    if (script.readyState == "loaded" || script.readyState == "complete"){
      script.onreadystatechange = null;
      showLightBox(videoPath, coverimage);       
     }
    };
    } else {
      script.onload = function(){
       showLightBox(videoPath, coverimage);       
      };
    }
  if (typeof script != "undefined"){
    document.getElementsByTagName("head")[0].appendChild(script);
  }
}

function showLightBox(videoPath, coverimage){
  if(!document.getElementById("mbox")){
    initmb();            
  }
  sm('box', 590, 400);
  jwplayerSetup('flashbanner', videoPath, coverimage, 590, 380, videoPath); //Jwplayer code added by Prabha on 27-JAN-16
  $$D("linksdiv").style.display = "block";
}
function closeHomeVideo(){	    
  //Remove the video content by Prabha on 27-JAN-2016
  jwplayer("flashbanner").stop();	
	jwplayer('flashbanner').remove();
  hm('box');
}