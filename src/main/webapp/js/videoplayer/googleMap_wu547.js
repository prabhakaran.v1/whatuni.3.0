//CODE RELATED TO WHATUNI REGISTRATION & LOGIN FORM ADDED BY ANBU.R 12-11-2012RELEASE
function showLightBoxGmapLB(form_name, pwidth, pheight, param1, param2, param3, param4, param5){
  var url="";
  sm('newvenue', pwidth, pheight, 'other');	
  if(form_name == 'google-map-lightbox'){
   url = contextPath + "/googleMapLightBox.html?cid="+param3;
  }
  document.getElementById("ajax-div").style.display="block";
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){ displayFormDataGmapLB(ajax,param1, param2, param4, param5); };
  ajax.runAJAX();  
}

function displayFormDataGmapLB(ajax,param_1, param_2, param4, param5){
  document.getElementById("newvenueform").innerHTML=ajax.response; 
  document.getElementById("ajax-div").style.display="none";
  document.getElementById("newvenueform").style.display="block";
  GAMapTracking(param4, param5);
  lboxMapInitialize(param_1, param_2);
}

function lboxMapInitialize(latitude, longtitude){ 
  var myLatlng = new google.maps.LatLng(latitude, longtitude);  
  var latlngbounds = new google.maps.LatLngBounds();
  var latStr = document.getElementById("latStr").value.split("###");
  var longStr = document.getElementById("longStr").value.split("###");
  //
  var myOptions = {
      zoom: 13,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
   }
   var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
   var iconBase = 'http://images1.content-wu.com/wu-cont/images/loc-pin.png';
   for(var latlonval=0; latlonval< latStr.length; latlonval++) {
     	var latitude = latStr[latlonval];
		    var longitude = longStr[latlonval]; 
      //
      var point = new google.maps.LatLng(latitude,longitude); 
      var marker=new google.maps.Marker({
      position:point,
      icon:iconBase,
      map:map
      });     
    google.maps.event.trigger(map, "resize");
    marker.setMap(map); 
    if(document.getElementById("locationinfo"+latlonval)){
    createMarker(marker,map,document.getElementById("locationinfo"+latlonval).innerHTML);
   }
  }
  }

function createMarker(marker, map, contentString){
  google.maps.event.addListener(marker, 'click', function() {
    infowindow = new google.maps.InfoWindow({
      content: contentString
  });
    infowindow.open(map,marker);
  });
}
function GAMapTracking(collegeName, pageName){
  //TODO CHANGE THIS ID TO LIVE : UA-22939628-2 OR TEST ID : UA-37421000-1 or Mobile Test : UA-44578438-1     
  ga('send', 'event', 'location_map' , pageName , collegeName); //Added by Amir for UA logging for 24-NOV-15 release
}