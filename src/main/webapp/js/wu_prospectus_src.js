var contextPath = "/degrees";
var kwDefs = "Enter uni name or subject";
var pros = jQuery.noConflict();
var triggered = false;
var flexSlider;
function loadProspectusSlider(){
    var currentElement ="";
    var prjq = jQuery.noConflict();
        jQuery.noConflict();        
        var width = document.documentElement.clientWidth; 
          prjq(window).on('load',function() {
          prjq('.flexslider').each(function() {
            currentElement ="";
            currentElement = prjq(this).attr('id');
            prjq('#'+ currentElement).flexslider({                              
              slideshow: false,
              animationLoop:false,
              animation:"slide",
              slideshowSpeed:4000,
              animationDuration:700,
              keyboardNav:true,
              touch: true,
              video: false,  
              slideToStart:0,
              pauseOnHover:true,   
              start: function(slider){
                flexSlider = slider;
                prjq(".flex-viewport").removeAttr("style");
                prjq('.slides').css({"width" : "6900px", "margin-left" : "0px;"});
              },
              after: function(slider){ 
                slider.pause(); 
                  var nextslide = slider.currentSlide;
                  currentElement = slider.attr('id');
                  if(nextslide!=0){
                     for(var nxtSlide=nextslide*4;nxtSlide<(nextslide*4 + 4);nxtSlide++){
                        prjq("#img"+currentElement+"_"+nxtSlide).each(function () {
                          var src = prjq("#img"+currentElement+"_"+nxtSlide).attr("data-src");
                           prjq("#img"+currentElement+"_"+nxtSlide).attr("src",src).removeAttr("data-src");
                        });
                    }
                  }
              },
              before: function(slider){ 
                      slider.pause(); 
              },
               });
            });  
       });
}
function prospectusSearch(){
    var finalUrl = $$("prospectusKwd_url").value != "" ? $$("prospectusKwd_url").value : "";   
    if(finalUrl!=""){
   	   $$("prospectusForm").action = finalUrl.toLowerCase();
	      $$("prospectusForm").submit();
    }else{
      alert("Search error.\n Please enter a valid subject or uni.");
        $$('prospectusKwd').value = '';
        $$('prospectusKwd').focus();
        return false;
    }
	   return false; 
}
function setProsSearchText(curid){
	var ids = curid.id;
	  if($$(ids).value == ''){
      $$(ids).value = kwDefs;
  }
}
function clearProsSearchText(curid){
	 var ids=curid.id;
	 if($$(ids).value == kwDefs){$$(ids).value = "";}
	 $$(ids).value = "";	
	 $$('prospectusKwd_url').value = "";
}
function clearProsSearchText(curid){
	 var ids=curid.id;
	 if($$(ids).value == kwDefs){$$(ids).value = "";}
	 $$(ids).value = "";	
	 $$('prospectusKwd_url').value = "";
}
function hideShowSliderBlock(id){ 
 var findId = id.substring(0, 1);
 var otherId = id.substring(1, id.length); 
    if($$(id).style.display == 'block' || $$(id).style.display == '') {
      $$(id).style.overflow = 'hidden';
      $$(id).style.display = 'none';
      $$("hideShow_"+id).className = "fa fa-plus-circle";
      $$("selectAll_"+id).style.display = 'none';                       
    }else{    
      $$(id).style.overflow = 'hidden';
      $$(id).style.display = 'block';
      $$("hideShow_"+id).className = "fa fa-minus-circle";
      $$("selectAll_"+id).style.display = 'block';       
      //Added below condition to load flex slider design properly, 19_May_2015 By Thiyagu G.
      if(document.documentElement.clientWidth > 992){        
        pros('#'+id).find('.slides').css({"width" : "6900px", "margin-left" : "0px;"});    
        dev(window).trigger('resize');
      }
    }
    lazyloadetStarts();
}
function addProspectusBasket(assocId, assocType, subOrderItemId, enqType,slider,carouselType,ProviderName){
var dowhat = "";
  thisvalue = $$("add_slider_"+slider+"_"+subOrderItemId);
  divValue =  $$("div_slider_"+slider+"_"+subOrderItemId);
var prosEvntCategory = $$("prosEvntCategory").value;  
  if(thisvalue.className == "fa fa-plus fa-1_5x"){
    dowhat = "add";
  }else if(thisvalue.className == "fa fa-remove fa-1_5x"){
    dowhat = "remove";
  } 
 var url='/degrees/addprosbasket.html?assocId='+assocId+'&dowhat='+dowhat+"&assocType="+assocType+"&subOrderItemId="+subOrderItemId+'&fromAjax=true'+'&enqType='+enqType;
  if(typeof XMLHttpRequest !="undefined"){req=new XMLHttpRequest();}
  else if(window.ActiveXObject){req=new ActiveXObject("Microsoft.XMLHTTP");}
  req.open("POST",url,true);req.onreadystatechange=handlerequest;req.send(url)
  function handlerequest(){
    if(req.readyState==4){
      if(req.status==200){ 
          if(thisvalue.className == "fa fa-plus fa-1_5x"){
            thisvalue.className = "fa fa-remove fa-1_5x";
            divValue.className = "ftv1 ftv selected";            
          }else if(thisvalue.className == "fa fa-remove fa-1_5x"){
            thisvalue.className = "fa fa-plus fa-1_5x";
            divValue.className = "ftv";
          }
         $$('prospectusBasketCount').innerHTML = "";
         $$('prospectusBasketCount').innerHTML = req.responseText;         
         highlightAllDivElements(subOrderItemId,dowhat,slider);
         if(dowhat=="add"){
          searchEventTracking(prosEvntCategory,carouselType+"-select",ProviderName);
         }else{
          searchEventTracking(prosEvntCategory,carouselType+"-deselect",ProviderName);
         }
      }
    }
  }  
}
function showFirstDivs(){  
  if( $$('slider_1')){
  $$('slider_1').style.display = 'block';
  $$('selectAll_slider_1').style.display = 'block';
  $$('hideShow_slider_1').className = 'fa fa-minus-circle';
  }
  if($$('slider_2')){
    $$('slider_2').style.display = 'block';
    $$('selectAll_slider_2').style.display = 'block';
    $$("hideShow_slider_2").className = "fa fa-minus-circle";
  }  
  if(document.documentElement.clientWidth > 992){        
    pros('.slides').css({"width" : "6900px", "margin-left" : "0px;"});    
    pros(window).trigger('resize');
  }
}
function addremoveallPros(ids, divid, sliderId,carouselType){
  var prosEvntCategory = $$("prosEvntCategory").value;
   var dowhat = "";
   enqType = "RP"
   assocType = "C"
  thisvalue = $$(divid);
  if(thisvalue.innerHTML == "SELECT ALL"){
    dowhat = "add";
  }else if(thisvalue.innerHTML == "DESELECT ALL"){
    dowhat = "remove";
  }
 var rows = ids.split('GP');
 var suborderItemIds = "";
 for(var i=0;i<rows.length;i++){
   var columns = rows[i].split('SP');
   for(var j=0;j<columns.length-1 ;j++){
      var subIds = columns[0];
      if(subIds!=""){
        if(suborderItemIds!=""){
         suborderItemIds = suborderItemIds +  "," + subIds;
        }else{
          suborderItemIds =  subIds;           
        }
      }
    }
  }  
  var url='/degrees/addprosbasket.html?dowhat='+dowhat+"&assocType="+assocType+"&selectallids="+ids+'&enqType='+enqType;
  if(typeof XMLHttpRequest !="undefined"){req=new XMLHttpRequest();}
  else if(window.ActiveXObject){req=new ActiveXObject("Microsoft.XMLHTTP");}
  req.open("POST",url,true);req.onreadystatechange=handlerequest;req.send(url)
  function handlerequest(){
    if(req.readyState==4){
      if(req.status==200){
         if(thisvalue.innerHTML == "SELECT ALL"){
            thisvalue.innerHTML = "DESELECT ALL";
          }else if(thisvalue.innerHTML == "DESELECT ALL"){
            thisvalue.innerHTML = "SELECT ALL";
          }
          $$('prospectusBasketCount').innerHTML = "";
          $$('prospectusBasketCount').innerHTML = req.responseText;
          var subItemIds = suborderItemIds.split(',');
          for(var k=0; k<subItemIds.length; k++){          
           var addId = $$("add_slider_"+sliderId+"_"+subItemIds[k]);
           var divId = $$("div_slider_"+sliderId+"_"+subItemIds[k]);    
             if(dowhat=='add'){
               addId.className = "fa fa-remove fa-1_5x";
               divId.className = "ftv1 ftv selected";
            }else if(dowhat=='remove'){
              addId.className = "fa fa-plus fa-1_5x";
              divId.className = "ftv";
            }          
           highlightAllDivElements(subItemIds[k],dowhat, sliderId);
          }
          if(dowhat=="add"){
            searchEventTracking(prosEvntCategory,"selectall",carouselType);
          }else{
            searchEventTracking(prosEvntCategory,"deselectall",carouselType);
          }
        }
     }
  } 
}
function openProspectusPopup(){
var url='/degrees/openProspectusPopUp.html';
var ajax=new sack()
ajax.requestFile=url
ajax.onCompletion=function(){prosPopupResponseData(ajax);}
ajax.runAJAX();
}
function prosPopupResponseData(ajax){
 $$('prospectusPopDiv').style.display = 'block'; 
 var response = ajax.response;
 var reponseArr = response.split("##SPLIT##");
 $$('prospectusPopDiv').innerHTML = reponseArr[0];
 }
function prosResponseData(ajax, subOrderItemId,action){
 $$('prospectusPopDiv').style.display = 'block'; 
     var response = ajax.response;
    var reponseArr = response.split("##SPLIT##");
    $$('prospectusPopDiv').innerHTML = reponseArr[0];
    $$('prospectusBasketCount').innerHTML = "";
    $$('prospectusBasketCount').innerHTML = reponseArr[1];
    highlightAllDivElementsforPopup(subOrderItemId,action);
}

function addRemoveFromPopup(assocId, assocType, subOrderItemId, enqType){
var dowhat = "";
  thisvalue = $$("addpop_"+subOrderItemId);
  divValue =  $$("divpopup_"+subOrderItemId);
  if(thisvalue.className == "fa fa-plus fa-1_5x fr"){
    dowhat = "add";
  }else if(thisvalue.className == "fa fa-remove fa-1_5x fr"){
    dowhat = "remove";
  }
 
 var url='/degrees/addprosbasket.html?assocId='+assocId+'&dowhat='+dowhat+"&assocType="+assocType+"&subOrderItemId="+subOrderItemId+'&enqType='+enqType+'&fromPopup=fromPopup';
 var ajax=new sack()
ajax.requestFile=url
ajax.onCompletion=function(){prosResponseData(ajax, subOrderItemId,dowhat);}
ajax.runAJAX();
}
function addRemoveFromPopupResponseData(ajax,thisvalue,divValue){          
    if(thisvalue.className == "fa fa-plus fa-1_5x"){
      thisvalue.className = "fa fa-remove fa-1_5x";
      divValue.className = "";
      
    }else if(thisvalue.className == "fa fa-remove fa-1_5x"){
      thisvalue.className = "fa fa-plus fa-1_5x";
      divValue.className = "cancel";
    }
 $$('prospectusPopDiv').style.display = 'block';
 $$('prospectusPopDiv').innerHTML = ajax.response;
}
function addSearchResultProspectusBasket(assocId, assocType, subOrderItemId, enqType,ProviderName){
var prosEvntCategory = $$("prosEvntCategory").value;
var dowhat = "";
  thisvalue = $$("add_search_res_"+subOrderItemId);
  divValue =  $$("div_search_res_"+subOrderItemId);
  if(thisvalue.className == "fa fa-plus fa-1_5x"){
    dowhat = "add";
  }else if(thisvalue.className == "fa fa-remove fa-1_5x"){
    dowhat = "remove";
  } 
 var url='/degrees/addprosbasket.html?assocId='+assocId+'&dowhat='+dowhat+"&assocType="+assocType+"&subOrderItemId="+subOrderItemId+'&fromAjax=true'+'&enqType='+enqType;
  if(typeof XMLHttpRequest !="undefined"){req=new XMLHttpRequest();}
  else if(window.ActiveXObject){req=new ActiveXObject("Microsoft.XMLHTTP");}
  req.open("POST",url,true);req.onreadystatechange=handlerequest;req.send(url)
  function handlerequest(){
    if(req.readyState==4){
      if(req.status==200){
         $$('prospectusBasketCount').innerHTML = "";
          $$('prospectusBasketCount').innerHTML = req.responseText;
          if(thisvalue.className == "fa fa-plus fa-1_5x"){
            thisvalue.className = "fa fa-remove fa-1_5x";
            divValue.className = "row act";
            
          }else if(thisvalue.className == "fa fa-remove fa-1_5x"){
            thisvalue.className = "fa fa-plus fa-1_5x";
            divValue.className = "row";
          }
          if(dowhat=="add"){
            searchEventTracking(prosEvntCategory,"srlist-select",ProviderName);
          }else{
            searchEventTracking(prosEvntCategory,"srlist-deselect",ProviderName);
          }
      }
    }
  }  
}
function addremoveallsrPros(ids, divid, assocType, enqType){
  var prosEvntCategory = $$("prosEvntCategory").value;
   var dowhat = "";
   enqType = "RP"
   assocType = "C"
  thisvalue = $$(divid);
  if(thisvalue.innerHTML == "SELECT ALL"){
    dowhat = "add";
  }else if(thisvalue.innerHTML == "DESELECT ALL"){
    dowhat = "remove";
  }
 var rows = ids.split('GP');
 var suborderItemIds = "";
 for(var i=0;i<rows.length;i++){
   var columns = rows[i].split('SP');
   for(var j=0;j<columns.length-1 ;j++){
      var subIds = columns[0];
      if(subIds!=""){
        if(suborderItemIds!=""){
         suborderItemIds = suborderItemIds +  "," + subIds;
        }else{
          suborderItemIds =  subIds;           
        }
      }
    }
  }  
  var url='/degrees/addprosbasket.html?dowhat='+dowhat+"&assocType="+assocType+"&selectallids="+ids+'&enqType='+enqType;
  if(typeof XMLHttpRequest !="undefined"){req=new XMLHttpRequest();}
  else if(window.ActiveXObject){req=new ActiveXObject("Microsoft.XMLHTTP");}
  req.open("POST",url,true);req.onreadystatechange=handlerequest;req.send(url)
  function handlerequest(){
    if(req.readyState==4){
      if(req.status==200){
         if(thisvalue.innerHTML == "SELECT ALL"){
            thisvalue.innerHTML = "DESELECT ALL";
          }else if(thisvalue.innerHTML == "DESELECT ALL"){
            thisvalue.innerHTML = "SELECT ALL";
          }
          $$('prospectusBasketCount').innerHTML = "";
          $$('prospectusBasketCount').innerHTML = req.responseText;
          var subItemIds = suborderItemIds.split(',');
          for(var k=0; k<subItemIds.length; k++){           
           var addId = $$("add_search_res_"+subItemIds[k]);
           var divId =  $$("div_search_res_"+subItemIds[k]);    
             if(dowhat=='add'){
               addId.className = "fa fa-remove fa-1_5x";
               divId.className = "row act";
            }else if(dowhat=='remove'){
              addId.className = "fa fa-plus fa-1_5x";
              divId.className = "row";
            }
          }
          if(dowhat=="add"){
            searchEventTracking(prosEvntCategory,"selectall","srlist");
          }else{
            searchEventTracking(prosEvntCategory,"deselectall","srlist");
          }
        }
     }
  } 
}
function checkorderCount(userExists){
  var orderCount = $$('prospectusBasketCount').innerHTML;
  var prosEvntCategory = $$("prosEvntCategory").value;
  if(orderCount!=null && orderCount!="" && (!isNaN(orderCount))){
     if(orderCount > 0)  {       
        searchEventTracking(prosEvntCategory,"order-now","clicked");
        eventTimeTracking();
        ga('send', 'event', 'Skulduggery', 'Bulk Prospectus Button', orderCount.trim());// ga logging to capture number of bulk prospectus trying to be ordered        
        if(userExists=="loggedIn"){
         bulkProspectusRedirect();
        }else if(userExists=="NonloggedIn"){
          window.location.href= contextPath+"/orderprospectus.html";//GA logging to be done
        }
     }else {
       alert("Sorry, there are currently no university prospectuses in your basket");
       return false;
      }
  }else{
     alert("Please add a prospectus to order!");
     return false;
  }
 }
function bulkProspectusRedirect(){
  var submitUrl;
  var ajaxURL = "/degrees/basicformdetailsexists.html";
  sm('newvenue', 490, 500, 'other');
  document.getElementById("ajax-div").style.display="block";
  var ajax=new sack();
  ajax.requestFile = ajaxURL;
  ajax.onCompletion = function(){ redirectBulkProspectus(ajax); };
  ajax.runAJAX();
}
function redirectBulkProspectus(ajax){
 var response = ajax.response;
  var respArr =  response.split("##SPLIT##");
  if(response[0] =="Y"){
      var token = respArr[1];
      var url  =  contextPath +"/orderprospectus/send-college-email.html?sendCollegeEmail=yes&_synchronizerToken="+token;
      //_gaq.push(['_set','hitCallback',function() {window.location.href=url;}]);
      //GAInteractionEventTracking('prospectussubmit', 'interaction', 'Prospectus', '', '');
      window.location.href=url;
  }else if(response[0] =="N"){
      var url  =  contextPath + "/orderprospectus.html";
      location.href=url;
  }   
}
function highlightAllDivElements(divid,action,sliderId){
  for(var i =1;i<=13;i++){
  if(i!=sliderId){
  if( $$("add_slider_"+i+"_"+divid)){
     var thisvalue = $$("add_slider_"+i+"_"+divid);
     var divValue =  $$("div_slider_"+i+"_"+divid);    
     if(action=="add"){
        thisvalue.className = "fa fa-remove fa-1_5x";
        divValue.className = "ftv1 ftv selected";    
     }
     else if(action=="remove"){
        thisvalue.className = "fa fa-plus fa-1_5x";
        divValue.className = "ftv";
     }    
   }
   }
 }
  if( $$("add_search_res_"+divid)){
    if(action=="add"){
       $$("add_search_res_"+divid).className = "fa fa-remove fa-1_5x";
       $$("div_search_res_"+divid).className = "row act";  
   }
   else if(action=="remove"){
       $$("add_search_res_"+divid).className = "fa fa-plus fa-1_5x";
        $$("div_search_res_"+divid).className = "row";
   }    
 }
}
function highlightAllDivElementsforPopup(divid,action){
  for(var i =1;i<=13;i++){
  if( $$("add_slider_"+i+"_"+divid)){
     var thisvalue = $$("add_slider_"+i+"_"+divid);
     var divValue =  $$("div_slider_"+i+"_"+divid);
     if(action=="add"){
        thisvalue.className = "fa fa-remove fa-1_5x";
        divValue.className = "ftv1 ftv selected";    
     }
     else if(action=="remove"){
        thisvalue.className = "fa fa-plus fa-1_5x";
        divValue.className = "ftv";
     }    
   }
 }
  if( $$("add_search_res_"+divid)){
    if(action=="add"){
       $$("add_search_res_"+divid).className = "fa fa-remove fa-1_5x";
       $$("div_search_res_"+divid).className = "row act";  
   }
   else if(action=="remove"){
       $$("add_search_res_"+divid).className = "fa fa-plus fa-1_5x";
       $$("div_search_res_"+divid).className = "row";
   }    
 }
}
 function closeWhenClickOutSide(){  
 var $ddp = jQuery.noConflict();
 $ddp(document).ready(function(){
  $ddp("#www-whatuni-com").mouseup(function(e){
        var subject = $ddp("#prospectusPopDiv");
        if(e.target.id != subject.attr('id') && !subject.has(e.target).length){
           subject.hide();
        }
    });
  });
}
function prospectusAjax_getLeftPos(inputObj, userleft){
	var returnValue = inputObj.offsetLeft;
	while((inputObj = inputObj.offsetParent) != null){
		returnValue += inputObj.offsetLeft;
	}
	if(userleft != null && userleft != 'undefined' && userleft != ''){
	   returnValue = parseInt(returnValue) + parseInt(userleft);
	}
	return returnValue;
}

function autocompleteOnScroll(){
var $fp = jQuery.noConflict();
        $fp(window).scroll(function(){
        if($$('ajax_listOfOptions')){
         //$$('ajax_listOfOptions').style.display="none";
         }
      if ($fp(this).scrollTop() > 300) {
          var leftPos = prospectusAjax_getLeftPos($$('prospectusKwd'),0);
          if(document.documentElement.clientWidth > 992){
           $fp('#scrollfreezediv').addClass('pros_fixed');
          }
           $fp('#ajax_listOfOptions').addClass('ajax_fixed');
           if($$('ajax_listOfOptions')){
              $$('ajax_listOfOptions').style.left = leftPos;               
          }
      } else {
          if(document.documentElement.clientWidth > 992){
            $fp('#scrollfreezediv').removeClass('pros_fixed');
          }
          $fp('#ajax_listOfOptions').removeClass('ajax_fixed');
      }
  }); 
}
function setProsAutoCompleteClass(){
 if($$D("scrollfreezediv")){
    if($$D("scrollfreezediv").className == 'pros_fixed'){
        if($$('ajax_listOfOptions')){
         $$('ajax_listOfOptions').className="ajax_fixed";
         }
    }
 }
}
pros(document).ready(function(){  //Added for wu_541 19-MAY-2015 by Karthi for mobile dropdown header
	pros('.lft_nav .brw').click(function(){
		pros(this).toggleClass('act');
		pros(this).parents().next().toggleClass('act');
	});
	pros(".sel_deg").click(function(){			
	  pros(".drop_deg").slideToggle();
 	});
    pros("ul.drop_deg li").click(function(){
	  pros(".sld_deg").html(pros(this).html())
	  pros(".drop_deg").slideToggle();	 
	});	
});
pros(window).scroll(function(){ // scroll event 
    lazyloadetStarts();    
});
pros(document).ready(function(){
    lazyloadetStarts();     
});
//Added below load script for flex sliders, 19_May_2015 By Thiyagu G.
pros(window).on('load',function() {  
  if(document.documentElement.clientWidth <= 992){    
    var cnt = pros("div[id^='slider_']").length;    
    for(var i = 1; i <= cnt; i++){    
      pros('#mslider_'+i).html(pros('#slider_'+i).html());      
    }
    pros("div[id^='mslider_']").removeClass('flexslider').find('.slides').removeClass('slides').removeAttr("style").find('li').removeAttr("style");
    pros("div[id^='slider_']").css("display", "none");
    pros("a[id^='slideratag_']").css("display", "none");
    pros("a[id^='slideratag1_']").css("display", "none");
    pros("a[id^='selectAll_slider_']").css("display", "none");
    pros("div[id^='mslider_']").css("width", "100%");
    pros('#mslider_1,#mslider_2').css("display","block");    
    pros('#hideShow_mslider_1,#hideShow_mslider_2').removeClass('fa fa-plus-circle').addClass('fa fa-minus-circle');    
    pros('#selectAll_mslider_1,#selectAll_mslider_2').css("display", "block");    
    pros("a[id^='mslideratag_']").css("display", "block");
    pros("a[id^='mslideratag1_']").css("display", "block");    
  }else{    
    pros('#slider_1,#slider_2').css("display","block");
    pros('#hideShow_slider_1,#hideShow_slider_2').removeClass('fa fa-plus-circle').addClass('fa fa-minus-circle');    
    pros('#selectAll_slider_1,#selectAll_slider_2').css("display", "block");
    pros("div[id^='mslider_']").css("display", "none");
    pros("a[id^='mslideratag_']").css("display", "none");
    pros("a[id^='mslideratag1_']").css("display", "none");
    pros("a[id^='selectAll_mslider_']").css("display", "none");    
    pros("a[id^='slideratag_']").css("display", "block");
    pros("a[id^='slideratag1_']").css("display", "block");    
    pros("#hdr_menu").css("display", "block");
    
    showFirstDivs();    
  }
});
//Added below script function to load myprospectus page, 19_May_2015 By Thiyagu G.
function orderedViewAll(){  
  var finaUrl = "/degrees/prospectus/?mypage=my_prospectus";
  location.href = finaUrl.toLowerCase();	 
}