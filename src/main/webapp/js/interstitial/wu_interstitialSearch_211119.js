var $isp = jQuery.noConflict();
var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";
var contextPath = "/degrees";
var gradeMaxPoints = [6, 10, 10, 3];
var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
//
function $$(obj) {
 return document.getElementById(obj);
}
//
function isBlankOrNullString(str) {
  var flag = true;
  if(str.trim() != "" && str.trim().length > 0) {
    flag = false;
  }
  return flag;
}
//
function isBlankOrNullValue(o) {
  var flag = true;
  if((o) && ((o.value).trim() != "") && ((o.value).trim().length > 0)) {
    flag = false;
  }
  return flag;
}
//
function isBlankOrNullInnerHtml(o) {
  var flag = true;
  if((o) && ((o.innerHTML).trim() != "") && ((o.innerHTML).trim().length > 0)) {
    flag = false;
  }
  return flag;
}
//
$isp(document).ready(function() {
		if($$('keyword_hidden').value == "" || $$('interstitialSubjectSearch_l1').value != $$('keyword_hidden').value) {	
		  getCourseCount();	
		  $$('keyword_hidden').value = $$('interstitialSubjectSearch_l1').value;
		  $$('interstitialSubjectSearch_fs').innerHTML = $$('interstitialSubjectSearch_l1').value + " ";
		  $$('interstitialSubjectSearch_fl').style.display = 'block';
    }
});
//
function returnSubjectListQueryString(objectName) {
  var qualCode = "qual=M";
  var l1Flag = $isp('#interstitialSubjectSearch_l1_l2flag') ? $isp('#interstitialSubjectSearch_l1_l2flag').val() == 'N' ? '&l1flag=Y' : '&l1flag=N' : "";
  var clearingFlag = ("Clearing/Adjustment" == $isp('#qualType_hidden').val()) ? "&pagename=CLEARING" : "";
  var parentCategoryId = "";
  if(objectName == "interstitialSubjectSearch_l2") {
    parentCategoryId = ($isp('#interstitialSubjectSearch_l1_id').val() && $isp('#interstitialSubjectSearch_l1_id').val() != "") ? "&parentcatid=" + $isp('#interstitialSubjectSearch_l1_id').val() : "";
  }
  var identifier = $isp('input[type=radio][name=qual]:checked').attr('id');
  if(!isBlankOrNullString(identifier)){
    qualCode = "qual="+$isp('#code'+identifier).val();
  }
  var QUERY_STRING = qualCode + l1Flag + clearingFlag + parentCategoryId;
  return QUERY_STRING;
}
//
function subjectLevelTwoList(){
   var QUERY_STRING = returnSubjectListQueryString("interstitialSubjectSearch_l2");
   var ajax=new sack();
   var url = contextPath+'/advance-search/subject-level-two-list.html?'+QUERY_STRING;   
   ajax.requestFile = url;	
   ajax.onCompletion = function(){responseSubjectList(ajax)};
   ajax.runAJAX();
   //
}
//
function responseSubjectList(ajax) {
  var response = ajax.response;
  $isp('#interstitialSubjectSearch_l2').html(response);
  $isp('#l1subjectname').html($isp('#subjectL1Div .inptxt').val());
  //$$('subjectL2Div').style.display = 'block';
}
//
function autoCompleteInterstitialSubjectSearch(event,object){//24_JUN_2014
  //
  if (object.id == "interstitialSubjectSearch_l1") {
    //$$('subjectL2Div').style.display = 'none';
    $isp('#subjectL2Div #interstitialSubjectSearch_l2').html('');
    $isp('#subjectL2Div #interstitialSubjectSearch_l2_label').html('Please select');
    $isp('#subjectL1Div input:hidden').val('');
    //$isp('#interstitialSubjectSearch_l1_url').val('');
    $isp('#interstitialSubjectSearch_l2_url').val('');
    $isp('#keyword_hidden').val('');
    $isp('#footer').hide();
  }
  if(isThisValidSubjectAjaxKeyword(object.value)) {
    $isp('#keyowrd_error_msg').hide();
    var QUERY_STRING = returnSubjectListQueryString(object.id);
    ajax_showOptions(object,'interstitialsearch',event,"indicator", QUERY_STRING, '0', object.offsetWidth);
    $isp('#sub_error_msg').hide();
  } else {
    $isp('#keyowrd_error_msg').show();
    $isp('#ajax_listOfOptions').hide();
    $isp('#sub_error_msg').hide();
  }
}
//
function manageSubjectLevelTwoDropdown(o) {
  if($isp(o).val() != '-1') {
    $isp('#interstitialSubjectSearch_l2_url').val($isp('#interstitialSubjectSearch_l2 option:selected').val());
    $isp('#interstitialSubjectSearch_l2_label').html($isp('#interstitialSubjectSearch_l2 option:selected').text());
    $isp('#interstitialSubjectSearch_fs').html($isp('#interstitialSubjectSearch_l2 option:selected').text());
    //$isp('#interstitialSubjectSearch_fl').show();
    getCourseCount($isp('#l2holder'));
  } else {
    $isp('#interstitialSubjectSearch_l2_url').val('');
    $isp('#interstitialSubjectSearch_l2_label').html('Please select');
    //$isp('#interstitialSubjectSearch_fl').show();
    getCourseCount($isp('#subjectL1Div'));
  }
}
//
function selectFilters(filterObjName, filterType, obj) {
  var objId = filterObjName;
  var loaderObj;
  var multiQualErrFlag = false;
  if(!isBlankOrNullString(filterObjName) && !isBlankOrNullString(filterType)) {
    // radio button selection
    if(filterType == 'single') {
      var value = $isp('#'+filterObjName).val();
      if(filterObjName.indexOf('country') > -1) {
        if(value == 'england') {
          $isp('#sub_region_list').show();
          $isp('#sub_region_list input:radio').removeAttr('checked');
        } else if(filterObjName.indexOf('subcountry') < 0) {
          $isp('#sub_region_list').hide();
          $isp('#sub_region_list input:radio').removeAttr('checked');
        }
      }
      if(filterObjName.indexOf('subcountry') > -1) {
        objId = 'sub_region_list';
      }
      var qualCode;
      var labelValue = (!isBlankOrNullString($isp("#"+filterObjName).val())) ? $isp('#label'+filterObjName).html() : "";
      if(filterObjName.indexOf('qual') > -1) {
        // -- clear when qualification switches --
        if($isp('#subjectL1Div .inptxt').val() != "") {
          $isp('#sub_error_msg').show();
        }
        $isp('#subjectL2Div #interstitialSubjectSearch_l2').html('');
        $isp('#subjectL1Div .inptxt').val('');
        $isp('#subjectL2Div #interstitialSubjectSearch_l2_label').html('Please select');
        $isp('#subjectL1Div input:hidden').val('');
        $isp('#interstitialSubjectSearch_l1_url').val('');
        $isp('#interstitialSubjectSearch_l2_url').val('');
        $isp('#keyword_hidden').val('');
        $isp('#l1subjectname').html('');
        //$isp('#subjectL2Div').hide();
        $isp('#footer').hide()
        //
        $isp('.parent_div').hide();
        if(labelValue == 'Clearing/Adjustment') {
          $isp('#qualType_hidden').val(labelValue);
        } else {
          $isp('#qualType_hidden').val('Others');
        }
        var qualType = $isp('#qualType_hidden').val();
        if(qualType.indexOf('Clearing/Adjustment') > -1) {
          $isp('#reviewPod').hide();
          $isp('#reviewCategory_hidden').val('');
          $isp(".review_check").removeAttr("checked");
          $isp('#clearing_grade').show();
        } else {
          $isp('#reviewPod').show();
          $isp('#others_grade').show();
        }
        qualCode = $isp('#code'+filterObjName).val();
        //
      }
      filterObjName = filterObjName.split('_')[0];
      if(filterObjName.indexOf('subcountry') > -1) {
         filterObjName = filterObjName.replace('sub', '');
      }
      //
      if(filterObjName.indexOf('grade') > -1) {
        $isp('#grade_hidden').val(value);
      } else {
        $isp('#'+filterObjName+'_hidden').val(value);
      } 
      //
      $isp('#code'+filterObjName+'_hidden').val(qualCode);
      //
      if(filterObjName.indexOf('qual') < 0) {
        if(filterObjName.indexOf('grade') > -1) {
          //$isp('#grade_fs').html(labelValue + " ");
          //$isp('#grade_fl').show();
        } else {
          $isp('#'+filterObjName+'_fs').html(labelValue + " ");
          if(!isBlankOrNullString(labelValue)) { 
            $isp('#'+filterObjName+'_fl').show(); 
          } else { 
            $isp('#'+filterObjName+'_fl').hide(); 
          }  
        } 
      }
    // checkbox selection
    } else if(filterType == 'multiple') {
      var values = $isp('[name='+filterObjName+']:checked').map(function() {return this.value;}).get().join(',');
      $isp('#'+filterObjName+'_hidden').val(values);
      /*
      var labelValue = "";
      $isp('input:checkbox[name='+filterObjName+']:checked').each(function () {
            labelValue += $isp('#label'+$isp(this).attr("id")).html() + ",";
      });
      labelValue = labelValue.replace(/.$/," ");
      */
      $isp('#'+(obj.id)+'_fs').hide();
      $isp('#'+(obj.id)+'_fl').hide();
      if ($isp('#'+(obj.id)).is(':checked')) {
        $isp('#'+(obj.id)+'_fs').show();
        $isp('#'+(obj.id)+'_fl').show();
      }
    // tariff point selection
    } else if(filterType == 'grades_tariff' || filterType == 'clrgrades_tariff') {
      var value = (filterType == 'grades_tariff') ? $isp('#point_4_0').val() : $isp('#clrpoint_4_0').val();
      if(!isBlankOrNullString(value)) {
        if(filterType == 'grades_tariff') { 
          $isp('#apply_4').removeAttr('disabled'); 
        } else {
          $isp('#clrapply_4').removeAttr('disabled');
        }
      }
      value = (value < 0 || value > 999) ? 0 : value;
      $isp('#grade_points_hidden').val(value);
      // grades selection
      if(filterType == 'grades') {
        loaderObj = $isp("#grade_4").closest('.rady');
      } else {
        loaderObj = $isp("#clrgrade_4").closest('.rady');
      }
    } else if(filterType == 'grades' || filterType == 'clrgrades') {
      var size = (filterType == 'clrgrades') ? $isp('.clrlevel_'+filterObjName).length : $isp('.level_'+filterObjName).length;
      var values = "";
      var allZeroFlag = true;
      for(var i = 0; i < size; i++) {
        var gradeLevel = (filterType == 'clrgrades') ? $isp('#clrgrdlvl_'+filterObjName+'_'+i).html() : $isp('#grdlvl_'+filterObjName+'_'+i).html();
        var gradePoint = (filterType == 'clrgrades') ? $isp('#clrpoint_'+filterObjName+'_'+i).html() : $isp('#point_'+filterObjName+'_'+i).html();
        if(gradePoint != '0') {
          allZeroFlag = false
        }
        values = values + gradePoint + gradeLevel;
        if((i+1) < size) {
          values = values + "-";
        }
      }
      if(filterType == 'grades') {
        loaderObj = $isp("#grade_"+filterObjName).closest('.rady');
      } else {
        loaderObj = $isp("#clrgrade_"+filterObjName).closest('.rady');
      }
      if(allZeroFlag) {
        //$isp('#grade_hidden').val('');
        //$isp('#grade_applied_hidden').val('');
        $isp('#grade_points_hidden').val('');
        //$isp('#grade_points_applied_hidden').val('');        
        //$isp('#apply_'+filterObjName).attr('disabled','disabled');
        //$isp('#clrapply_'+filterObjName).attr('disabled','disabled');
      } else if(!($isp('.errmsg').is(':visible'))) {
        $isp('#grade_points_hidden').val(values.toLowerCase());
        $isp('#apply_'+filterObjName).removeAttr('disabled');
        $isp('#clrapply_'+filterObjName).removeAttr('disabled');
      }
    } else if(filterType == 'qualification') {
  	  if($isp('.frm-ele').val() != '') {
    	    $isp('#'+filterType+'_fl').show();
    	    GANewAnalyticsEventsLogging('Qualification Filter', 'Start', 'Advanced Search');
    	    if($isp("#"+filterObjName)){
    	    	loaderObj = $isp("#"+filterObjName).closest('.aplybtn');	
    	    }    	    
          var selVal = [];
    	  for (var i = 0; i < 3; i++) {
    	    if(jq('#qualspan_'+i).is(':visible')){
    	      selVal.push(jq('#qualspan_'+i).text().trim());   
    	    }	
    	  }
    	  if (selVal.length > 1) {
    		for (var i = 0; i < selVal.length; i++) {  
    		  for (var j = i+1; j < selVal.length; j++) {
    			if(selVal[i] == selVal[j]) {
    			  multiQualErrFlag = true;
    			 }
    		  }
    		}
    	  }
    	if(multiQualErrFlag) { 
    		  jq('#pId').html("Oops, looks like you have entered the same qualification type twice. Please ensure you add each qualification type once.");
    		  jq('#errorDiv').show();
    	}else{
    		jq('#errorDiv').hide();
    	}
  	  }
    }
  } 
  //
  if(filterObjName.indexOf('grade') < 0) {
    if(filterType == 'multiple') {
      if($isp(obj)) {
        loaderObj = $isp(obj).closest('.rady');
      } 
    } else if(filterType == 'single') {
      loaderObj = objId == 'sub_region_list' ? $isp("#"+objId) : $isp("#"+objId).closest('.rady');
    }
  }
  //
  if(filterType != 'grades' && filterType != 'clrgrades' && filterType.indexOf('grades_tariff') < 0 && filterObjName.indexOf('grade') < 0 && !multiQualErrFlag) {
    getCourseCount(loaderObj);  
  }
  //
}
//
function applyGrades(type, no) {
  $isp('#grade_applied_hidden').val($isp('#grade_hidden').val());
  $isp('#grade_points_applied_hidden').val($isp('#grade_points_hidden').val());
  if(!isBlankOrNullString($isp('#labelgrade_'+no).html())) {
    $isp('#grade_fs').html($isp('#labelgrade_'+no).html() + " ");
  }
  $isp('#grade_fl').show();
  getCourseCount($isp("#"+type+"grade_"+no).closest('.rady'));
}
//
function validateTariffPoints(e) {
  var obj = e.target;
  var value = obj.value;
  objId = (obj.id).replace('point_4_0', '');
  $isp('#'+objId+'error_msg').hide();
  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57 && e.which < 96 || e.which > 105)) {
      // Show error div
      $isp('#'+objId+'error_msg').html('Please enter valid tariff points from 0 to 999');
      $isp('#'+objId+'error_msg').show();
  } else {
    if(value > 999 || value < 0) {
      // show error div
      $isp('#'+objId+'error_msg').show();
    } 
  }
}
//
function clearFilters(filter, allOrOneFlag, inputType) {
  $isp('#loadingImg').show();
  if(allOrOneFlag == 'ALL') {
    $isp('input:checkbox').removeAttr('checked');
    $isp('input:radio').removeAttr('checked');
    //$isp('input:radio[name!="qual"]').removeAttr('checked');
    $isp('input:text').val('');
    $isp('#subjectL1Div input:hidden').val('');
    //$isp('#subjectL2Div input:hidden').val('');
    $isp('#input_hiddens input:hidden[class*="_nodef"]').val('');
    //$isp('#subjectL2Div').hide();
    $isp('.gnum').html('0');
    $isp('#footer, #footer_zero_count, #footer_with_count').hide();
    $isp('.errmsg').hide();
    $isp('.aplybtn input').attr('disabled','disabled');
    $isp('#gradeParent div[id^="grades_"]').hide();
    $isp('#clrgradeParent div[id^="clrgrades_"]').hide();
    $isp('#sub_region_list').hide();
	$isp('#level_3_qual_0').css('display','none'); 
	$isp('#level_3_qual_1').css('display','none');  
	$isp('#level_3_qual_2').css('display','none');    	 
	$isp('#addQualBtn_1').css('display','none');   	
	addQualification('level_3_qual_0', 0, 'addQualBtn_0');
    $isp('#add_fld').hide();
    $isp('#filter_ul li').hide();
    $isp("html, body").animate({ scrollTop: 0 }, "slow");
  } else if(allOrOneFlag == 'ONE') {
    if(inputType == 'radio') {
      $isp('input:radio[name='+filter+']').removeAttr('checked');
      $isp('input:radio[name=sub'+filter+']').removeAttr('checked'); // For subregions
      $isp('#sub_region_list').hide();
      $isp('#'+filter+'_hidden').val('');
      $isp('#'+filter+'_points_hidden').val('');
      if(filter == 'qual') {
        $isp('#code'+filter+'_hidden').val('');
      }
      if(filter == 'grade') {
        $isp('.gnum').html('0');
        $isp('.gnum').removeClass('gcolr');
        $isp('input:radio[name="clrgrade"]').removeAttr('checked');
        $isp('input:radio[name="grade"]').removeAttr('checked');
        $isp('div[id^="grades_"]').hide();
        $isp('div[id^="clrgrades_"]').hide();
        $isp('.errmsg').hide();
        $isp('.aplybtn input').attr('disabled','disabled');
        $isp('#'+filter+'_applied_hidden').val('');
        $isp('#'+filter+'_points_applied_hidden').val('');
      }
      $isp('#'+filter+'_fl').hide();
    } else if(inputType == 'text') {
      $isp('#'+filter+'_l1').val('');
      $isp('#'+filter+'_l2').val('');
      $isp('.inptsr input:hidden').val('');
      $isp('#'+filter+'_l1_url').val('');
      $isp('#'+filter+'_l2_url').val('');
      $isp('#keyword_hidden').val('');
      $isp('#'+filter+'_fl').hide();
      $isp('#sub_error_msg').show();
      $isp('#footer').hide();
    } else if(inputType == 'checkbox') {
      $isp('input:checkbox[id='+filter+']').removeAttr('checked');
      $isp('#'+filter+'_hidden').val('');
      $isp('#'+filter+'_fl').hide();
      var objName = filter.split('_')[0];
      var values = $isp('[name='+objName+']:checked').map(function() {return this.value;}).get().join(',');
      $isp('#'+objName+'_hidden').val(values);
    } else if(inputType == 'select') {
    	$isp('#level_3_qual_0').css('display','none'); 
    	$isp('#level_3_qual_1').css('display','none');  
    	$isp('#level_3_qual_2').css('display','none');   	 	 
    	$isp('#addQualBtn_1').css('display','none');   	
    	addQualification('level_3_qual_0', 0, 'addQualBtn_0');
        $isp('#add_fld').hide();
        $isp('#'+filter+'_fl').hide();
    }
  }
  getCourseCount();
}
//
function increaseOrDecreaseGradePoints(incOrDecFlag, outNo, inNo, type) {
  //
  var value = $isp('#'+type+'point_'+outNo+'_'+inNo).html();
  var modified = (incOrDecFlag == 'inc') ? parseInt(value)+1 : (incOrDecFlag == 'dec') ? parseInt(value)-1 : parseInt(value);
  var gradeLimit = parseInt($isp('#'+type+'gradeLevel_'+outNo).val());
  modified = (modified > gradeLimit || modified < 0) ? value : modified;
  $isp('#'+type+'point_'+outNo+'_'+inNo).html(modified);
  if(modified > 0) {
    $isp('#'+type+'point_'+outNo+'_'+inNo).addClass('gcolr');
  } else {
    $isp('#'+type+'point_'+outNo+'_'+inNo).removeClass('gcolr');
  }
  //
  var gradePoints = $isp('#'+type+'grades_'+outNo+' .gnum').map(function() {return this.innerHTML;}).get().join();
  var gradePointsArray = new Array();
  var gradePointsArray = gradePoints.split(",");
  var totalPoints = gradePointsArray.reduce(function(acc, val) { return parseInt(acc) + parseInt(val); }, 0)
  //
  if(totalPoints > gradeMaxPoints[outNo] && totalPoints != 0) {
    var gradeMaxPointsInText = ["six", "ten", "ten", "three"]
    var msg = "Woah, steady on there! You can only enter a maximum of "+gradeMaxPointsInText[outNo]+" grades (and if you really are enough of a genius to be doing more than "+gradeMaxPointsInText[outNo]+", just enter your top ones)."
    $isp('#'+type+'error_msg').html(msg);
    $isp('#'+type+'error_msg').show();
    $isp('#'+type+'apply_'+outNo).attr('disabled','disabled');
  } else {
    $isp('#grade_points_hidden').val('');
    $isp('#'+type+'error_msg').hide();
    $isp('#'+type+'apply_'+outNo).removeAttr('disabled');
  }
  //
}
//
function enableGradePoints(no, type) {
  var appliedNo = "";
  if(!isBlankOrNullString($isp('#grade_applied_hidden').val())) {
    var appliedObjId = $isp('input:radio[value='+$isp('#grade_applied_hidden').val()+']').attr('id');
    appliedNo = appliedObjId.split('_')[1];
  }
  $isp('.'+type+'points').hide();
  $isp('.'+type+'points').hide();
  if(appliedNo == no) {
    $isp('#grade_points_hidden').val($isp('#grade_points_applied_hidden').val());
  } else {
    //$isp('#grade_points_applied_hidden').val($isp('#grade_points_hidden').val());
    $isp('#grade_points_hidden').val('');
  }
  $isp('#'+type+'grades_'+no).show();
  $isp('#'+type+'error_msg').hide();
  $isp('.grdval span:not([id^='+type+'point_'+appliedNo+']).gnum').html('0');
  $isp('.grdval span:not([id^='+type+'point_'+appliedNo+']).gnum').removeClass('gcolr');
  var appliedPointsHidden = $isp('#grade_points_applied_hidden').val().split('-');
  if(appliedNo == no && appliedPointsHidden != null) {
    for(var i=0; i<appliedPointsHidden.length; i++) {
      $isp('span[id^='+type+'point_'+appliedNo+'_'+i+']').html(appliedPointsHidden[i].charAt(0));
      if(appliedPointsHidden[i].charAt(0) != '0') {
        $isp('span[id^='+type+'point_'+appliedNo+'_'+i+']').addClass('gcolr');
      } else {
        $isp('span[id^='+type+'point_'+appliedNo+'_'+i+']').removeClass('gcolr');
      }
    }
  }
  if(appliedNo != '4') {
    $isp('#'+type+'point_4_0').val('');
  } else {
    $isp('#'+type+'point_4_0').val($isp('#grade_points_applied_hidden').val());
  }
  if(appliedNo != no) {
    $isp('#'+type+'apply_'+no).attr('disabled','disabled');
  }
}
//
function validateReview(o) {
 var size = $isp('[name="reviewCategory"]:checked').length
 if(size > 3) {
  $isp(o).removeAttr('checked');
 } else {
  selectFilters('reviewCategory', 'multiple', o)
 }
}
//
function checkEmpty(filter) {
    return filter != "";
}
//
function keywordURL(keyword) {
  var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
  keyword = keyword.replace(reg," ");
  keyword = replaceAll(keyword," ","-");
  keyword = keyword.toLowerCase();
  return keyword;
}
//
function replaceAll(replacetext,findstring,replacestring) {
  var strReplaceAll = replacetext;
  var intIndexOfMatch = strReplaceAll.indexOf(findstring);
  while(intIndexOfMatch != -1){
    strReplaceAll = strReplaceAll.replace(findstring,replacestring);
    intIndexOfMatch = strReplaceAll.indexOf(findstring)
  }
  return strReplaceAll;
}
//
function submitResults() {
    $isp('#loadingImg').show();
    //
    var eventLabel = "";
    //
    var subject = !isBlankOrNullString($isp("#interstitialSubjectSearch_l1_url").val()) ? $isp("#interstitialSubjectSearch_l1_url").val() : "";
    subject = !isBlankOrNullString($isp("#interstitialSubjectSearch_l2_url").val()) ? $isp("#interstitialSubjectSearch_l2_url").val() : subject;
    var keyword = !isBlankOrNullString($isp("#keyword_hidden").val()) ? keywordURL($isp("#keyword_hidden").val()) : "";
    //
    var qual = !isBlankOrNullString($isp("#qual_hidden").val()) ? $isp("#qual_hidden").val() : "";
    var country = !isBlankOrNullString($isp("#country_hidden").val()) ? $isp("#country_hidden").val() : "";	
    var locationType = !isBlankOrNullString($isp("#locationType_hidden").val()) ? $isp("#locationType_hidden").val() : "";      
    var studyMode = !isBlankOrNullString($isp("#studymode_hidden").val()) ? $isp("#studymode_hidden").val() : "";     
    var assessmentType = !isBlankOrNullString($isp("#assessment_hidden").val()) ? $isp("#assessment_hidden").val() : "";
    var reviewCategory = !isBlankOrNullString($isp("#reviewCategory_hidden").val()) ? $isp("#reviewCategory_hidden").val() : "";
    var gradePoints = !isBlankOrNullString($isp("#grade_points_applied_hidden").val()) ? $isp("#grade_points_applied_hidden").val() : ""; 
    var gradeLevel = !isBlankOrNullString($isp("#grade_applied_hidden").val()) ? $isp("#grade_applied_hidden").val() : "";    
    
    var score = !isBlankOrNullString($isp("#clearing_score").val()) ? $isp("#clearing_score").val() : "";    
    
    //
    if(!isBlankOrNullString(gradeLevel)) {
      if(isBlankOrNullString(gradePoints)) {
        gradeLevel = "";
      }
    } else if(isBlankOrNullString(gradeLevel)) {
      gradePoints = "";
    }
    //
    var countryGA = country;
    if(isBlankOrNullString(country)) {
      countryGA = "all-uk";
    } 
    var studyModeGA = studyMode;
    if(isBlankOrNullString(studyMode)) {
      studyModeGA = "all-study-types";
    }
    //
    var selectedFilterArray = [subject, qual, countryGA, locationType, studyModeGA, gradeLevel, gradePoints, reviewCategory];
    //selectedFilterArray = selectedFilterArray.filter(checkEmpty);
    eventLabel = selectedFilterArray.join("|").toString();
    //
    var searchURL = wu_scheme+document.domain + "/" + qual + "-courses/search?" ;
    if("Clearing/Adjustment" == $isp('#qualType_hidden').val()) {
      searchURL += "clearing&"
    }
    searchURL += (!isBlankOrNullString(subject)) ? "subject=" + subject : (!isBlankOrNullString(keyword)) ? "q=" + keyword : "";
    searchURL += !isBlankOrNullString(country) ? "&location=" + country : "";
    searchURL += !isBlankOrNullString(locationType) ? "&location-type=" + locationType : "";
    searchURL += !isBlankOrNullString(studyMode) ? "&study-mode=" + studyMode : "";
    searchURL += !isBlankOrNullString(assessmentType) ? "&assessment-type=" + assessmentType : "";
    searchURL += !isBlankOrNullString(reviewCategory) ? "&your-pref=" + reviewCategory : "";
    if("Clearing/Adjustment" == $isp('#qualType_hidden').val()) {
      //searchURL += !isBlankOrNullString(score) ? "&score=0," + score : "";
    }else{
    searchURL += !isBlankOrNullString(gradeLevel) ? "&entry-level=" + gradeLevel : "";    
    searchURL += !isBlankOrNullString(gradePoints) ? "&entry-points=" + gradePoints : ""; 
    }
    searchURL += !isBlankOrNullString(score) ? "&score=0," + score : "";
    //
    GANewAnalyticsEventsLogging('Search', 'advanced search', eventLabel.toLowerCase());
    //
    if(!isErrorExistsInForm()) {
      closeInterstitialPage(searchURL);
    }
    //
}
//
function isErrorExistsInForm() {
  if(($$('error_msg') && $$('error_msg').style.display == 'block') || ($$('clrerror_msg') && $$('clrerror_msg').style.display == 'block')) {
    return true;
  }
}
//
function slideDownOrUp(objName, o, event) {
  var className = $isp(event.target).attr('class') ? $isp(event.target).attr('class') : "";
  if(!(className.indexOf('tooltext') > -1)) {
    if(o.className == 'fa fa-angle-down') {
      $isp('#'+objName).slideUp(400);
      o.className = 'fa fa-angle-up';
    } else if(o.className == 'fa fa-angle-up') {
      $isp('#'+objName).slideDown(400);
      o.className = 'fa fa-angle-down';
    }
  }
}
//
function searchKeyWordWhenSearched(e) {
  if(e.keyCode == 27 || (e.keyCode == 13 && e.target.id == 'interstitialSubjectSearch_l1')){
    e.preventDefault();
    if($$('keyword_hidden').value == "" || $$('interstitialSubjectSearch_l1').value != $$('keyword_hidden').value) {
        $$('keyword_hidden').value = $$('interstitialSubjectSearch_l1').value;
        $$('interstitialSubjectSearch_fs').innerHTML = $$('interstitialSubjectSearch_l1').value + " ";
        $$('interstitialSubjectSearch_fl').style.display = 'block';
        getCourseCount($isp("#interstitialSubjectSearch_l1").closest('.inptsr'));
    }
    ajax_options_hide();
  }
}
//
function getCourseCount(loaderObj) {
  var url = "/degrees/advance-search/course-count.html?";
  var pageName = ("Clearing/Adjustment" == $isp('#qualType_hidden').val()) ? "pagename=CLEARING" : "pagename=COURSE_COUNT_AJAX";
  var qual = (!isBlankOrNullValue($$('codequal_hidden'))) ? '&qual='+$isp('#codequal_hidden').val() : "";
  var subject = (!isBlankOrNullValue($$('interstitialSubjectSearch_l1_url'))) ? '&subject='+$isp('#interstitialSubjectSearch_l1_url').val() : "";
  //subject = (!isBlankOrNullValue($$('interstitialSubjectSearch_l2_url'))) ? '&subject='+$isp('#interstitialSubjectSearch_l2_url').val() : subject;
  var keyword = !isBlankOrNullString($isp("#keyword_hidden").val()) ? '&keyword='+keywordURL($isp("#keyword_hidden").val()) : "";
  var loc = (!isBlankOrNullValue($$('locationType_hidden'))) ? '&locationtype='+$isp('#locationType_hidden').val() : "";
  var country = (!isBlankOrNullValue($$('country_hidden'))) ? '&country='+$isp('#country_hidden').val() : "";
  var studymode = (!isBlankOrNullValue($$('studymode_hidden'))) ? '&studymode='+$isp('#studymode_hidden').val() : "";
  var grade = (!isBlankOrNullValue($$('grade_applied_hidden'))) ? '&grade='+$isp('#grade_applied_hidden').val() : "";
  var points = (!isBlankOrNullValue($$('grade_points_applied_hidden'))) ? '&points='+$isp('#grade_points_applied_hidden').val() : "";
  var assessment = (!isBlankOrNullValue($$('assessment_hidden'))) ? '&assessment='+$isp('#assessment_hidden').val() : "";
  var review = (!isBlankOrNullValue($$('reviewCategory_hidden'))) ? '&review='+$isp('#reviewCategory_hidden').val() : "";
  var getCountFlag = true;
  if(!isBlankOrNullString(grade)) {
    if(isBlankOrNullString(points)) {
      grade = "";
      $isp('#grade_applied_hidden').val('');
    }
  } else if(isBlankOrNullString(grade)) {
    points = "";
    $isp('#grade_points_applied_hidden').val('');
    $isp('#clearing_score').val('');
  }
  if(isErrorExistsInForm()) {
    getCountFlag = false;
  }
   if((!isBlankOrNullString(qual) && ((!isBlankOrNullString(subject)) ||  !isBlankOrNullString(keyword)) && getCountFlag)) {
	 if(loaderObj != undefined){
		 addOrRemoveLoader(loaderObj, 'add'); 
	 }	   
     var qualSubListVal = createJSON();
     if((qualSubListVal.length >= 0)){
	   var stringJson =	{
	    "qualSubList": createJSON()
	   };
	 }
	 var jsonData = JSON.stringify(stringJson);
	 var param = pageName + qual + subject + keyword + loc + country + studymode + grade + points + assessment + review;
	 url = url + param; 
	 jq.ajax({
	   url: url, 
	   headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
	   dataType: 'json',
	   type: "POST",
	   data: jsonData,
	   async: false,
	   success: function(response){
	   },
	   complete: function(response){
	    setFooterStickyBasedOnCount(response, loaderObj)
	   }
	 })
  }
//  if((!isBlankOrNullString(qual) && ((!isBlankOrNullString(subject)) ||  !isBlankOrNullString(keyword)) && getCountFlag)) {
//    addOrRemoveLoader(loaderObj, 'add');
//    url = url + pageName + qual + subject + keyword + loc + country + studymode + grade + points + assessment + review;
//    var ajaxObj = new sack();
//    ajaxObj.requestFile = url;	  
//    ajaxObj.onCompletion = function(){setFooterStickyBasedOnCount(ajaxObj, loaderObj)};	
//    ajaxObj.runAJAX();
//  } else {
//    $isp('#loadingImg').hide();
//  }
  //

  else {
    $isp('#loadingImg').hide();
  }
  //
}
//
function addOrRemoveLoader(obj, type) {
  if(type == 'add') {
    $isp(obj).addClass('asldr_icn');
  } else {
    $isp(obj).removeClass('asldr_icn');
  }
}
//
function setFooterStickyBasedOnCount(response, loaderObj){
  var respText = response.responseText;
  var respTextArr = respText.split("##");
  var count = respTextArr[0];
  var score = respTextArr[1];
  if(count == "" || count == 0 || count == undefined) {
    $isp('#footer_zero_count').show();
    $isp('#footer_with_count').hide();
  } else {
    $isp('#footer_zero_count').hide();
    $isp('#footer_with_count').show();
    
    if(score != "" && score != 0){$isp('#clearing_score').val(score);}
    
    var btnText = "";
    if(count == 1) {
      btnText = 'View '+count+' course';
    } else {
      btnText = 'View '+count+' courses';
    }
    $isp('#filter_submit_button').html(btnText);
  }
  $isp('#footer').show();
  //
  addOrRemoveLoader(loaderObj, 'remove');
  //
  $isp('#loadingImg').hide();
  //
  setWholeDivPaddingDynamically();
  //
}
//
function setWholeDivPaddingDynamically() {
  // --- Set padding bottom of parent div by the height of footer which is dynamically changing --- 
  $$("parentAdv").style.paddingBottom = $isp("#footer").outerHeight() + 'px';
  //
}
//
function closeInterstitialPage(URL) {
  var root = window.parent.document.getElementsByTagName( 'html')[0];   
  root.className = root.className.replace('scrl_dis', ''); 
  if(deviceWidth <= 768) {    
     if(!isBlankOrNullString(URL)) { window.parent.location.href = URL; } else { window.history.go(-1); }
  } else {                
    if(!isBlankOrNullString(URL)) { window.parent.location.href = URL; }
    else {
      window.parent.document.getElementById("fade_NRW").style.display = "none";
      window.parent.document.getElementById("holder_NRW").className = "";
      window.parent.document.getElementById("iframe_NRW").style.height = "99%";
      window.parent.document.getElementById("holder_NRW").style.display = "none";
      window.parent.document.getElementById("close_NRW").style.display = "none";
      $isp('#loadingImg').hide();
    }
  }  
}
//
function hideCloseIfDesktop() {
  if(!(deviceWidth <= 768)) {
    $isp('#closeHere').hide();
  }
}
//
// --- Keep this function at last for more clarity :: Write new function above this if any---
//
function isThisValidSubjectAjaxKeyword(inputString) {
  var pattern = /^[\\!\"#$%&()*+\-,./:;<=>?@\[\]^_{|}~ 0-9]*$/g;
  var result = inputString.match(pattern);
  if(result != null) {
    result = result.toString();
  } else {
    result = "";
  }
  if(!isBlankOrNullString(result)) {
    return false;
  } else {
    return true;
  }
}
//