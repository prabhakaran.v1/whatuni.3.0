var width = document.documentElement.clientWidth;
var adv = jQuery.noConflict();
function viewMoreResult(viewId, collegeId){
  var viewMoreId = viewId.replace("view_","");      
  if($$("viewMore_"+viewMoreId)){    
    if($$("viewMore_"+viewMoreId).style.display =="none") {
      $$("viewMore_"+viewMoreId).style.display = "block";  
      $$("view-less_"+viewMoreId).style.display = "block";
      $$("view-more_"+viewMoreId).style.display = "none";        
    }else{
      $$("viewMore_"+viewMoreId).style.display = "none";
      $$("view-more_"+viewMoreId).style.display = "block";
      $$("view-less_"+viewMoreId).style.display = "none";
    }
  }        
}
function clearReviewSearchText(curid, searchmsg){var ids = curid.id;if($$(ids).value==searchmsg){$$(ids).value="";}}
function setReviewSearchText(curid, searchmsg){var ids = curid.id; if($$(ids).value=='') { $$(ids).value=searchmsg;} }
function setDropDownValue(obj,s2){
  if("sortTextId"==s2){
    $$(s2).innerHTML = obj.innerText;
  }else{
    $$(s2).innerHTML = obj.text; 
  }  
}
function clearReviewUni(){  
  if($$('providerId')){
    $$('providerId').value = "";
    $$('providerUrl').value = "";
  }    
}
function commonPhraseViewMoreAndLess(viewMoreOrLessBtnId){
  if("comPhraseViewMore"==viewMoreOrLessBtnId){    
    adv( ".extraCP" ).show();
    blockNone("comPhraseViewMore","none");
    blockNone("comPhraseViewLess","block");
  }else{
    adv( ".extraCP" ).hide();
    blockNone("comPhraseViewMore","block");
    blockNone("comPhraseViewLess","none");
  }
}
function sortSearchReviewListURL(obj){
  var orderBy=obj.id;    
  searchMoreReviewListURL("",orderBy);
}
function emptyInputValue(id){
 if($$(id)){
   $$(id).value = "";
 }
}
function removeFilterParams(filterId){
 if("revUniName" == filterId){
   emptyInputValue('revUniName_hidden');
   emptyInputValue('revUniName_id');
   emptyInputValue('revUniName_display');
   emptyInputValue('revUniName_url');
   emptyInputValue('providerId');
   emptyInputValue('providerUrl');
 }else if("revSubjectId" == filterId){   
   emptyInputValue('revSubjectId_hidden');
 }else if("reviewSearchKwd" == filterId){
   emptyInputValue('reviewSearchKwd');
 }else if("navQualTP" == filterId){
	 $$("navQualTP").value = "All";
 }
 searchMoreReviewListURL("filterRemoved");
}
function searchMoreReviewListURL(formNameId,orderBy,obj){
  var eventAction = "";
  var eventLabel = "";
  
  var collegeId       = "";
  var collegeUrl      = "";        
  var providerUrl     = "";
  var searchUrl       = "";
  var orderByUrl      = "";
  var finalUrl        = "";  
  var subjectId       = "";
  var keyword         = "";
  var uniSrchUrl      = "";
  var queryStringUrl  = "";     
  
  var reviewSearchUrl = "";
  var studyLevelUrl = "";  
  var studyLevel = "";
  if(orderBy==undefined || orderBy==''){  
    if($$('revUniName_id')){
      collegeId = $$('revUniName_id').value;
    }  
    if($$('revUniName_url')){
      collegeUrl = $$('revUniName_url').value;
    }
    if(!$$('revUniName_id')){
      if($$('providerId')){
        collegeId = $$('providerId').value;
      }
      if($$('providerUrl')){
        collegeUrl = $$('providerUrl').value;
      }
    }else if($$('revUniName_id')){
      if($$('revUniName_id').value==""){
        if($$('providerId')){
          collegeId = $$('providerId').value;
          collegeUrl = $$('providerUrl').value;
        }  
      }
    }   
    subjectId  = $$('revSubjectId_hidden').value.toLowerCase();
    studyLevel  = $$('navQualTP').value.toLowerCase();
    if("reviewCommonPhraseKwdForm"==formNameId && obj){
      keyword  = obj.innerText;
      eventAction = "";
    }else{
      eventAction = "";
      keyword    = ($$('reviewSearchKwd').value).trim();   
    }    
    if('uniNameForm'==formNameId && collegeId==""){
      alert("Please choose university to proceed");
      return false;
    }
    if('subNameForm'==formNameId && subjectId==""){
      alert("Please choose subject to proceed");
      return false;
    }
    if(collegeId=="" && (keyword=="Something specific?" || keyword.trim()=="") && subjectId=="" && formNameId != "filterRemoved" && studyLevel==""){
      alert("Please select any filters to proceed");
      if(orderBy=="" || orderBy==undefined){
        return false;
      } 
    }    
    if('reviewSearchKwdForm'==formNameId && (keyword=="Something specific?" || keyword.trim()=="")){
      //alert("Please enter keyword to proceed");
      return false;
    }
    if($$('providerRes')){
      if(collegeId!="" && (keyword=="Something specific?" || keyword.trim()=="") && subjectId==""){
        //alert("Please select any filters to proceed");
        return false;
      }
    }
    if(keyword=="Something specific?" || keyword.trim()==""){
      keyword = "";
    }
    
    studyLevelUrl = ("all" != studyLevel) ? "?study-level=" + studyLevel : "";
	if(collegeId!="" && collegeUrl!=""){ 
		if(formNameId == 'uniNameForm'){
		      eventLabel = $$('revUniName_display').value;
		      }
  	  uniSrchUrl += "&university-name=" + collegeUrl.toLowerCase() + "&college-id=" + collegeId;    
      }
if(keyword!="" || subjectId!="" || studyLevel!="all"){
	if(studyLevelUrl != ""){
		queryStringUrl = studyLevelUrl; 
	} else{
    queryStringUrl += "?";
  }
	if(keyword!="" && (formNameId == 'reviewSearchKwdForm' || formNameId == 'reviewCommonPhraseKwdForm')){
		 eventLabel = keyword;
        var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
        keyword = keyword.replace(reg," ");
        keyword = replaceAll(keyword," ","-");
      }
	 var subName = $$('revSubjectId').value;
     if(formNameId == 'subNameForm' && subjectId!="" && !(formNameId == 'reviewSearchKwdForm' || formNameId == 'reviewCommonPhraseKwdForm')){
       eventLabel = subName;
     }
      queryStringUrl += uniSrchUrl;
      queryStringUrl += (""!=subjectId) ? "&subject=" + subjectId : "";
      queryStringUrl += (""!=keyword) ? "&keyword=" + encodeURIComponent(keyword) : "";
      queryStringUrl = queryStringUrl.replace("?&","?");
    }
    
if(collegeId!="" && keyword=="" && subjectId=="" && studyLevel == "all"){
	eventLabel = $$('revUniName_display').value;
    //console.log("eventLabel-->"+eventLabel);
    //
    providerUrl = "/university-course-reviews/" + collegeUrl.toLowerCase() + "/" + collegeId + "/";
  }else if(collegeId=="" && keyword=="" && subjectId=="" && studyLevel == "all"){      
    reviewSearchUrl = "/university-course-reviews/";
  }else if(collegeId!="" || keyword!="" || subjectId!=""  || studyLevel != "all"){      
    searchUrl = "/university-course-reviews/search/"+queryStringUrl;
  }
}else{
  var currentUrl = "";
  if($$('curReviewUrl')){
    currentUrl = $$('curReviewUrl').value;      
    if(currentUrl.indexOf("?")!=-1){
      orderByUrl = currentUrl + "&orderby=" + orderBy;
    }else{
      orderByUrl = currentUrl + "?orderby=" + orderBy;
    }
    finalUrl = orderByUrl;
  }
}
  //Added for GA event logging in review search page by Prabha on SEP_24_2019_REL
  if((formNameId == 'reviewSearchKwdForm' || formNameId == 'reviewCommonPhraseKwdForm') && (keyword!="")){
    eventAction = "Keyword";
  }else if(formNameId == 'subNameForm' && subjectId!=""){
    eventAction = "Subject";
  }else if(formNameId == 'uniNameForm' && collegeId!=""){
    eventAction = "University";
  }
  if(eventAction != ''){
    ga('send', 'event', 'Review page filter', eventAction, eventLabel.trim(), 1, {nonInteraction: true});
  }
  //End of GA event logging
  
  if(providerUrl!=""){
	    finalUrl = providerUrl  + orderByUrl;
	  }else if(reviewSearchUrl!=""){
	    finalUrl = reviewSearchUrl  + orderByUrl;
	  }else if(searchUrl!=""){
	    finalUrl = searchUrl  + orderByUrl;
	  }
  if(finalUrl!=""){finalUrl= finalUrl.toLowerCase()};//Added by sangeeth.s for lower casing of urls for FEB_12_19 rel
	  if(orderByUrl!=""){    
	    location.href = finalUrl;
	  }else {
	    if(formNameId!=undefined && $$(formNameId)){
	      $$(formNameId).action = finalUrl;
	      $$(formNameId).submit();
	    }else{
		  if(finalUrl==""){finalUrl="/university-course-reviews/";}	
	      location.href = finalUrl;
	    }
	  }  
	  return false;
	}
  
function validateSrchIcn(srchIconId,inputId) {   
   if(adv('#'+srchIconId) && adv('#'+inputId).val().trim()!=""){
     adv('#'+srchIconId).removeClass("srch_dis");
     if("uni_icn"==srchIconId){
       emptyInputValue('revUniName_hidden');
       emptyInputValue('revUniName_id');
       emptyInputValue('revUniName_display');
       emptyInputValue('revUniName_url');
       emptyInputValue('providerId');
       emptyInputValue('providerUrl');       
     }
     if("sub_icn"==srchIconId){
       emptyInputValue('revSubjectId_hidden');       
     }
   }else{
     adv('#'+srchIconId).addClass("srch_dis");     
   }
}
adv(document).ready(function(){ 
  var width = document.documentElement.clientWidth;  
  if(width < 1024){    
    adv('#sortId').click(function(e){      
      var id = adv(this).attr('id');      
      var drpdnId="drp_"+id;                 
      if(adv('#drp_'+id).css('display') == 'none'){          
        adv('#drp_'+id).css({'display':'block', 'z-index':'2'});        
      }else{
        adv('#drp_'+id).css({'display':'none', 'z-index':''});          
      }     
    });
  }
  if(adv('#awardPodImg').isInViewport()){
    lazyLoadSpecImg($$('awardPodImg'));
  }
});
adv(window).scroll(function(){
  if(adv('#awardPodImg').isInViewport()){
    lazyLoadSpecImg($$('awardPodImg'));
  }
});
//Added to lazy load specific image
function lazyLoadSpecImg(imgObj){
  var imgSrc = imgObj.getAttribute("src");
  if((imgSrc.indexOf('/img_px') > -1)) {
    imgObj.src = imgObj.getAttribute('data-src');
  }
}
function setQualTP(styLvlUrlText){
	  adv('#topNavQualList').hide();
	  adv('#navQualTP').val(styLvlUrlText);
	  GANewAnalyticsEventsLogging('Review page filter', 'Study Level', styLvlUrlText.toLowerCase());
}
function viewAllReviewsGaLogging(institutionName, collegeId) {
	GANewAnalyticsEventsLogging('Reviews', 'View Reviews', institutionName);
	var finalUrl="/university-course-reviews/"+ institutionName.toLowerCase() + "/" + collegeId + "/";
	finalUrl = replaceAll(finalUrl," ","-");
    location.href = finalUrl;
}
function viewProfileGaLogging(institutionName) {
	 GANewAnalyticsEventsLogging('Reviews', 'View Profile', institutionName);
}