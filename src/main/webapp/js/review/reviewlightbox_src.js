 var dev = jQuery.noConflict();
function reviewLightBox() {
  dev(document).ready(function() {
  adjustStyle();
  dev(".rvbx_shdw,.rev_lbox .revcls a,.rev_lbox .clr_sf_cnr").click(function(event){
  event.preventDefault();
  dev(".rev_lbox").removeAttr("style");
  dev(".rev_lbox").addClass("fadeOut").removeClass("fadeIn");
  dev("html").removeClass("rvscrl_hid");
  var postClearingSwitch = dev('#postClearingOnOff').val();
  if(postClearingSwitch == 'ON') {
	   dev("#revLightBox").removeClass("clr_sf_cnr"); 
	   dev("#revLightBox").html('');
  }
  });
  });
}
dev(window).resize(function() {
  revLightbox();
})
dev(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
revLightbox();
}
function revLightbox(){
  var width = document.documentElement.clientWidth;
  var revnmehgt = dev(".rvbx_cnt .revlst_lft").outerHeight();
  var lbxhgt = dev(".rev_lbox .rvbx_cnt").height();
  var txth2hgt = dev(".rvbx_cnt h2").outerHeight();
  var txth3hgt = dev(".rvbx_cnt h3").outerHeight();
  if (width <= 480) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    dev(".lbx_scrl").css("height", +scrhgt+ "px");
   }else if ((width >= 481) && (width <= 992)) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    dev(".lbx_scrl").css("height", +scrhgt+ "px");
   }else{
     var lbxmin = 17 + txth2hgt + txth3hgt;
     var scrhgt = lbxhgt - lbxmin; 
     dev(".lbx_scrl").css("height", +scrhgt+ "px"); 
   }
}
function ajaxReviewLightBox(reviewId) {
  var url = "/degrees/ajax/reviewlightbox.html?"+ "reviewId=" + reviewId;
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){ ajaxResponse(ajax); }
  ajax.runAJAX();
}
function ajaxResponse(ajax) {
  dev("#revLightBox").html(ajax.response);
  dev("#revLightBox").addClass("rev_lbox");
  dev(".rev_lbox").css({'z-index':'111111','visibility':'visible'});
  dev(".rev_lbox").addClass("fadeIn").removeClass("fadeOut");
  dev("html").addClass("rvscrl_hid");
  reviewLightBox();
  revLightbox();
}
