var contextPath = "/degrees";
var $fp = jQuery.noConflict(); 
function isNotNullAndUndex(value) {
  return (value != '' && value != undefined && value != null && value != 'null');
}
function fetchModuleData(id, groupId, courseId, index, link) {
  var evntFilterType = "";  
  if(document.getElementById("pagename")){
    if(document.getElementById("pagename").value=="clcdetail"){
      evntFilterType = "clearing-";
    }
  }
  if(document.getElementById("hideShow_content"+index).className == "fa fa-plus-circle fnt_24 color1 fr mt6"){
    if(document.getElementById("module_data_"+index).innerHTML == ""){
       var ajax = new sack();
       url = contextPath + "/loadmoduledata.html?method=loadModuleData&moduleGroupId="+groupId + "&courseId=" + courseId +"&index="+index;
       ajax.requestFile = url;
       ajax.onCompletion = function(){ loadModuleData(ajax, index); };
       ajax.runAJAX();
      }else{
        closeOtherDiv('outer', 'hideShow_content', 'fa fa-plus-circle fnt_24 color1 fr mt6', index);      
        document.getElementById("hideShow_content"+index).className = "fa fa-minus-circle fnt_24 color1 fr mt6";
        document.getElementById("module_data_"+index).style.display = "block";
        document.getElementById("module_data_"+index).className = "clear";
        searchEventTracking('show/hide',evntFilterType+'cdmodules','more');
      }
   } else{
      document.getElementById("hideShow_content"+index).className = "fa fa-plus-circle fnt_24 color1 fr mt6";
      document.getElementById("module_data_"+index).style.display = "none";
       document.getElementById("module_data_"+index).className = "clear disp_off";
      searchEventTracking('show/hide',evntFilterType+'cdmodules','less');
   }
} 

function loadModuleData(ajax, index){
  var evntFilterType = "";  
  if(document.getElementById("pagename")){
    if(document.getElementById("pagename").value=="clcdetail"){
      evntFilterType = "clearing-";
    }
  }
  var response = ajax.response;
  closeOtherDiv('outer', 'hideShow_content', 'fa fa-plus-circle fnt_24 color1 fr mt6',index);  
  document.getElementById("hideShow_content"+index).className = "fa fa-minus-circle fnt_24 color1 fr mt6";
  document.getElementById("module_data_"+index).style.display = "block";
   document.getElementById("module_data_"+index).className = "clear";
  document.getElementById("module_data_"+index).innerHTML = response;
  if(document.getElementById("modulehideshowevent").value != ""){
    searchEventTracking('show/hide',evntFilterType+'cdmodules','more');
  }else{
    document.getElementById("modulehideshowevent").value = "YES";
  }
  
}


function loadModuleDetailDescription(moduleId, moduleGroupId, courseId, rowindex){
 if(document.getElementById("plmn_"+ moduleGroupId +"_"+rowindex).className == "fa fa-plus-circle fnt_18 color1 pr15"){
    if(document.getElementById("module_data_detail_" +moduleGroupId + "_" +rowindex).innerHTML == ""){
      var ajax = new sack();
       url = contextPath + "/loadmoduledata.html?method=loadModuleDetail&moduleGroupId="+ moduleGroupId + "&moduleId="+moduleId + "&courseId=" + courseId +"&index="+rowindex;;
       ajax.requestFile = url;
       ajax.onCompletion = function(){ loadModuleDetailData(ajax, rowindex, moduleGroupId); };
       ajax.runAJAX();
    }else{
       var divToClose = "module_data_detail_"+ moduleGroupId + "_";
       closeOtherDiv('inner', "plmn_"+ moduleGroupId+"_", 'fa fa-plus-circle fnt_18 color1 pr15', rowindex, divToClose);   
       document.getElementById("plmn_"+ moduleGroupId +"_"+rowindex).className = "fa fa-minus-circle fnt_18 color1 pr15";
       document.getElementById("module_data_detail_"+ moduleGroupId + "_"+rowindex).style.display = "block";
   }
  }else{
      document.getElementById("plmn_"+ moduleGroupId +"_"+rowindex).className = "fa fa-plus-circle fnt_18 color1 pr15";
      document.getElementById("module_data_detail_"+ moduleGroupId + "_" +rowindex).style.display = "none";
  }
}
function loadModuleDetailData(ajax, rowindex, moduleGroupId){

  var response = ajax.response;
  var divToClose = "module_data_detail_"+ moduleGroupId + "_";
  closeOtherDiv('inner', "plmn_"+ moduleGroupId+"_", 'fa fa-plus-circle fnt_18 color1 pr15', rowindex, divToClose);       
  document.getElementById("plmn_"+ moduleGroupId +"_"+rowindex).className = "fa fa-minus-circle fnt_18 color1 pr15";
  document.getElementById("module_data_detail_"+ moduleGroupId +"_"+rowindex).style.display = "block";
  document.getElementById("module_data_detail_"+ moduleGroupId +"_"+rowindex).innerHTML = response;
  //eval(document.getElementById('detshow').innerHTML);
}


  function loadFirstDelivery(){ 
     if(document.getElementById('tc0')){
      //document.getElementById("srcEvent").value="no";  
      document.getElementById('tc0').onclick();
     } 
  }
function closeOtherDiv(displayDiv, activeId, activeClassName, indexTobeopen, divToClose){
  //var cusid_ele = document.getElementsByClassName(displayClassname);
  var count;
  var divToCloseHere;
  if(displayDiv == 'outer'){
    count = document.getElementById("outount").value;
    divToCloseHere = "module_data_";
  }else{
    count = document.getElementById("innercount").value;
    divToCloseHere = divToClose;
  }
  
  for (var i = 0; i < count; ++i) {
    if(i!=indexTobeopen){ 
      if(document.getElementById(activeId+[i])){
        //alert('inside the if conditon');
        var ele = document.getElementById(activeId+[i]);
        ele.className = activeClassName;
      }  
      if(document.getElementById(divToCloseHere+[i])){
        var item = document.getElementById(divToCloseHere+[i]);  
        item.style.display = 'none';
      }
    }
  }
}

function showMoreLess(id, rowId){
  if(id+rowId == 'lesscontent'+rowId){
    document.getElementById('morecontent'+rowId).style.display = "block";
    document.getElementById('lesscontent'+rowId).style.display = "none";
  }else{
    document.getElementById('lesscontent'+rowId).style.display = "block";
    document.getElementById('morecontent'+rowId).style.display = "none";
  }
}

  function autocompleteOnScroll(){
        var $fp = jQuery.noConflict();       
        $fp(window).scroll(function(){
          if(document.getElementById('ajax_listOfOptions')){
           // document.getElementById('ajax_listOfOptions').style.display="none";
           }
        if ($fp(this).scrollTop() > 100) {  
           var windowTop = $fp(window).scrollTop(); // returns number  
           var docHeight = $fp(document).height();
           var diff = docHeight - 1200; 
           $fp('#fixedscrolltop').addClass('top_fixed');
           if($fp('#clearingSkipLinks') && $fp('#clearingSkipLinks').val() == 'CLEARING_CD_PAGE'){
        	 $fp('#sidebar').addClass('sticky_left clr_skip');  
           }else{
             $fp('#sidebar').addClass('sticky_left');
           }
           if(document.getElementById('cdRating')){
            document.getElementById('cdRating').style.display = "none";
           }
           if( document.getElementById('cdSpName')){
            document.getElementById('cdSpName').style.display = "none";
           }
           if( document.getElementById('bcrummbs')){
            document.getElementById('bcrummbs').style.display = "none";    
           }
           if(windowTop > diff){
        	 if($fp('#clearingSkipLinks') && $fp('#clearingSkipLinks').val() == 'CLEARING_CD_PAGE'){
               $fp('#sidebar').removeClass('sticky_left clr_skip');  
             }else{
               $fp('#sidebar').removeClass('sticky_left');        
             }
           }
      } else {
          $fp('#fixedscrolltop').removeClass('top_fixed');     
          if($fp('#clearingSkipLinks') && $fp('#clearingSkipLinks').val() == 'CLEARING_CD_PAGE'){
              $fp('#sidebar').removeClass('sticky_left clr_skip');  
            }else{
              $fp('#sidebar').removeClass('sticky_left');        
            }
          if(document.getElementById('cdRating')){
           document.getElementById('cdRating').style.display = "block";
          }
          if( document.getElementById('cdSpName')){
           document.getElementById('cdSpName').style.display = "block";
          }
          if( document.getElementById('bcrummbs')){
           document.getElementById('bcrummbs').style.display = "block";  
          }
      }
      lazyloadetStarts(); //Added by Prabha on 24_Jan_2017_REL
  }); 
}

function showHideDivText(div1,div2){
 if( document.getElementById(div1)){
  if(document.getElementById(div1).style.display == 'block' || document.getElementById(div1).style.display == ''  ){
    document.getElementById(div1).style.display = 'none';
    document.getElementById(div2).style.display = 'block';
  }else{
     document.getElementById(div1).style.display = 'block';
     document.getElementById(div2).style.display = 'none';
  }
  }
}
function setSelectedValEntryReq(obj, spanid){
  var dobid = obj.id
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  dev('#'+spanid).text(text);
  dev(".entryClass,.addtionalInfoClass").hide();//added for course detail page entry requirments structure change by Hema.S on 15.5.2018
  var id = dev("#dropdownId").children(":selected").attr("id");
  dev("#entryPoints_"+id).show();
  dev("#additionalInfo_"+id).show();
}
function setSelectedVal(obj, spanid,index){
  var dobid = obj.id
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  document.getElementById(spanid).innerHTML = text;
  if(text=='After 6 months'){
    document.getElementById("avgSalaryAfterFourtyMonthsDiv"+index).style.display = 'none';
    document.getElementById("avgSalaryAfterSixMonthsDiv"+index).style.display = 'block';
  }else{
     document.getElementById("avgSalaryAfterFourtyMonthsDiv"+index).style.display = 'block';
    document.getElementById("avgSalaryAfterSixMonthsDiv"+index).style.display = 'none';
  }  
}
function setSelectedValTimeSubRanking(obj, spanid){
  var dobid = obj.id;
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  var valText = el.options[el.selectedIndex].value;
  document.getElementById("currentRankingId").innerHTML = valText;
  if(text.length>25){
   text = text.substring(0,25) + '..';
  }
  document.getElementById(spanid).innerHTML = text; 
}

function showMoresLess(){  
  if( document.getElementById("spMoreDiv")){
    if(document.getElementById("spMoreDiv").style.display == 'block' || document.getElementById("spMoreDiv").style.display == ''  ){
      document.getElementById("spMoreDiv").style.display = 'none';
      document.getElementById("spViewMorwLink").style.display = 'block';
      document.getElementById("spViewLessLink").style.display = 'none';
    }else{
       document.getElementById("spMoreDiv").style.display = 'block';
          document.getElementById("spViewMorwLink").style.display = 'none';
      document.getElementById("spViewLessLink").style.display = 'block';
    }
  }
  lazyloadetStarts(); //Added by Prabha on 24_Jan_2017
}

function setSelectedoptionText(obj, spanid){
  var dobid = obj.id
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  document.getElementById(spanid).innerHTML = text;  
}

function oppOnChange(collegeId, courseId, obj){  
  var dobid = obj.id;
  var el = document.getElementById(dobid);
  var opportunityId = el.options[el.selectedIndex].value;
  var isCLearing = "";
  var qtxt = "";
  if(document.getElementById("isClearingCourse")){
    isCLearing = document.getElementById("isClearingCourse").value;
    qtxt = "&clearing=clearing";
  }
  var url="/degrees/loadKISDataAjax.html?courseId="+courseId+"&collegeId="+collegeId+"&opportunityId="+opportunityId+"&ajaxFlag=Y"+qtxt;
  var ajax=new sack();
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ courseAjaxKISData(ajax,opportunityId); };	
   ajax.runAJAX();
 }
 
function courseAjaxKISData(ajax,opportunityId){
  if(ajax.response != ''){
    document.getElementById("ajaxDivContent").innerHTML = ajax.response;
    document.getElementById("oppId").value = opportunityId;
    var $aj = jQuery.noConflict();
    var applyFlag = document.getElementById("applyNowFlag").value;
    if($aj('#clearingonoff').val() != 'OFF'){
      var scorelabel = isNotNullAndUndex($aj('#scoreLabel_hidden').val()) ? $aj('#scoreLabel_hidden').val() : "";
      if(isNotNullAndUndex(scorelabel)){
    	chanceOfAcceptance(scorelabel);  
      }      
    }    
    applyOnChange(applyFlag);
    var elements = $aj("script[id^='script_']");
    if (elements.length > 0) {           
        for (var i = 0; i < elements.length; i++) {
           var ele = elements[i].id;
           if($$(ele)){eval($$(ele).innerHTML);
           }
        }
     }
      entryRequirments();
      feestabact();
      
   }
   //load articles pod on opportunity change on 13_06_2017, By Thiyagu G.
   loadArticlesPodJS('COURSE_DETAIL_PAGE');      
   setTimeout(function() {articlesSlider();}, 500);
}

function chanceOfAcceptance(scoreLabelValue){
  var scoreLabelText = scoreLabelValue;
  var scoreToolTip =  $fp("#scoreLabelTooltip_hidden").val();
  var scoreLevel = $fp('#scoreLevel_hidden').val();
  $fp('#scoreLabel_spanId').text(scoreLabelText);
  $fp('#scoreToolTip_divId').text(scoreToolTip); 
  var indicatorClass = scoreLevel == "LIKELY" ? "txtgrncol" : scoreLevel == "POSSIBLE" ? "txtgrncol" : "txtorngcol";
  var indicatorClassDataId = $fp('#scoreColorIndicator_divId').get(0).getAttribute("data-id-indicator");  
  $fp('#scoreColorIndicator_divId').removeClass(indicatorClassDataId).addClass(indicatorClass);  
  var indicatorClass1 = scoreLevel != "LIKELY" ? " brdrgry" : "";
  $fp('#scoreColor_spanId1').removeClass('brdrgry').addClass(indicatorClass1); 
  var indicatorClass2 = scoreLevel == "STRETCH" ? " brdrgry" : "";
  $fp('#scoreColor_spanId2').removeClass('brdrgry').addClass(indicatorClass2); 
}

function applyOnChange(applyFlag) {
  
  var applyNowvalue= document.getElementById("applyNowFlagAjax").value;
  var checkApply = applyNowvalue;
  if(document.getElementById("applyNowPod") != null) {
  if(checkApply == "Y") {
    document.getElementById("applyNowPod").style.display = "inline-block";
  }  else {
    document.getElementById("applyNowPod").style.display = "none";
  }
  }
}
function skipLinkSetPositions(divname){
  var $fp = jQuery.noConflict(); 
  var clearingSkipLinks = "";
  var divPosition = $fp('#'+divname).offset();
  var skipLinksDivPosition = "";
  if($fp("#clearingSkipLinks")){
	  skipLinksDivPosition = 280; 
  }else{
	  skipLinksDivPosition = 320;
  }
  
  $fp('html, body').animate({
      scrollTop: divPosition.top - skipLinksDivPosition
      }, "slow");
}


function navSearchSubmitCD(flag)
{	  
  var clearFlag = flag;  
  if(checkNavKeywordCd())
  {
    if(clearFlag == 'ON'){
      var navQual = trimString($$('navQualCd').value); //24_JUN_2014
      var clearingYear = '';
      if($$('clearingYear')){
        clearingYear = $$('clearingYear').value;
      }
      if(navQual == clearingYear || navQual == 'Clearing/Adjustment'){
         updateUserTypeSession('','clearing','newSubmitSearchCD');
         //newSubmitSearchCD(true,flag)  
      }else{
        updateUserTypeSession('','non-clearing','newSubmitSearchCD');
        //newSubmitSearchCD(false,flag)
      }
    }else{
      newSubmitSearchCD(false,flag)
    }
  }else{
    return false;
  }
}
function checkNavKeywordCd()
{
  var navKwd = trimString($$('navKwdCd').value);
  var navQual = trimString($$('navQualCd').value);

  if(navKwd == kwDef || navKwd == '' || navKwd == ucasDef){
    alert(empKw);
    $$('navKwdCd').value = '';
    $$('navKwdCd').focus();
    return false;
  }
//  if(navKwd != kwDef){
//    if(!searchTextValidate(navKwd)){
//      alert(empKw);
//      $$('navKwd').focus();
//      return false;
//    }
//  }
  if(navQual == qualDef){
    alert(empKw);
    $$('navQualCd').focus();
    return false;
  }
  return true;
}
function newSubmitSearchCD(confFlag,clearingFlag)
{
  var clearingSearch = confFlag;
  var clearFlag = clearingFlag;   
  var navKwd = trimString($$('navKwdCd').value);
  var navQual = trimString($$('navQualCd').value); 
  var qualDesc = 'degree';
  var qualCode = navQual;
  var qualCode = "M";
  var clearingYear = '';
  if($$('clearingYear')){
    clearingYear = $$('clearingYear').value;
  }
  if(navQual == 'Degrees' || navQual == 'Undergraduate'){
    qualDesc = 'degree'; qualCode='M';
  }else if(navQual == 'Postgraduate'){
    qualDesc = 'postgraduate'; qualCode='L';
  }else if(navQual == 'Foundation degree'){
    qualDesc = 'foundation-degree'; qualCode='A';
  }else if(navQual == 'Access & foundation'){
    qualDesc = 'access-foundation'; qualCode='T';
  }else if(navQual == 'HND / HNC'){
    qualDesc = 'hnd-hnc'; qualCode='N';
  }else if(navQual == 'UCAS CODE'){
    qualDesc = 'ucas_code'; qualCode='UCAS_CODE';
  } else if(navQual == clearingYear || navQual == 'Clearing/Adjustment'){ //24_JUN_2014
    qualDesc = 'degree'; qualCode='M';
  }

  var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
  navKwd = navKwd.replace(reg," ");
  if(navKwd == kwDef || navKwd == '' || navKwd == null || navKwd == ucasDef){
    alert(empKw);
    $$('navKwdCd').value = '';
    $$('navKwdCd').focus();
    return false;
  }  
  var navKwd1 = replaceAll(navKwd," ","+");
  var navKwd2 = replaceAll(navKwd," ","-");
  var country1 = 'united+kingdom';
  var country2 = 'united-kingdom';
  var newUrl2 = "";
  var newUrl1 = "/"+qualDesc+"-courses/search";
  if(clearFlag == 'ON'){
    if(clearingSearch){      
      newUrl2 = "?clearing&q="+navKwd2;
    }else{      
      newUrl2 = "?q="+navKwd2;
    }
  }else{  
    newUrl2 = "?q="+navKwd2;
  }  
  var finalUrl = newUrl1 + newUrl2;
 
  if($$("keywordCd_url").value != "" ||  $$("matchbrowsenodeCd").value != ""){
    finalUrl = $$("keywordCd_url").value != "" ? $$("keywordCd_url").value : $$("matchbrowsenodeCd").value;
  }
	$$("navSearchFormCd").action = finalUrl.toLowerCase();
	$$("navSearchFormCd").submit();
	return false;
}

function toggleQualListCd(display){
	if($$('navQualListCd').style.display == 'none'){
		$$('navQualListCd').style.display = 'block'
	} else {
		$$('navQualListCd').style.display = 'none'
	}	
	if(display != null || display != 'undefined'){
		$$('navQualListCd').style.display = display
	}
}

function autoCompleteKeywordBrowseCd(event,object){
  var navQual = trimString($$('navQualCd').value); 
  var qualDesc = 'degree';
  var qualCode = "M";
  var clearingYear = '';
  if($$('clearingYear')){
    clearingYear = $$('clearingYear').value;
  }
  if(navQual == 'Degrees' || navQual == 'Undergraduate'){
    qualDesc = 'degree'; qualCode='M';
  }else if(navQual == 'Postgraduate'){
    qualDesc = 'postgraduate'; qualCode='L';
  }else if(navQual == 'Foundation degree'){
    qualDesc = 'foundation-degree'; qualCode='A';
  }else if(navQual == 'Access & foundation'){
    qualDesc = 'access-foundation'; qualCode='T';
  }else if(navQual == 'HND / HNC'){
    qualDesc = 'hnd-hnc'; qualCode='N';
  }else if(navQual == 'UCAS CODE'){
    qualDesc = 'ucas_code'; qualCode='UCAS_CODE';   //Added by Sangeeth.S for Jul_3_18 rel
  }else if(navQual == clearingYear || navQual == 'Clearing/Adjustment'){
    qualDesc = 'CLEARING'; qualCode='CLEARING';
  }
  $$('selQualCd').value = navQual;
	ajax_showOptions(object,'keywordbrowse',event,"indicator",'qual='+ qualCode +'&siteflag=desktop', '0', '385');
   var $cdAutoCom = jQuery.noConflict();
   $cdAutoCom('#ajax_listOfOptions').addClass('btm_search');
}


function clearNavSearchTextCd(curid){
	 var ids=curid.id;
	 if($$(ids).value == kwDef){$$(ids).value = "";}
	 $$(ids).value = "";
  $$('keywordCd_hidden').value = "";
  $$('keywordCd_id').value = "";
  $$('keywordCd_url').value = "";
  $$('keywordCd_display').value = "";
	 $$('matchbrowsenodeCd').value = "";
	 $$('keywordCd_url').value = "";
}

//
function setNavSearchTextCd(curid){
	var ids = curid.id;
  var navQual = trimString($$('navQualCd').value); 
	if($$(ids).value == ''){
    if(navQual=='UCAS CODE'){
      $$(ids).value = ucasDef;
    }else{
      $$(ids).value = kwDef;
    }
  }
}

function setQualCd(o){
	$$('navQualCd').value = (o.innerHTML).replace(/&amp;/g, '&');
	$$('navQualListCd').style.display = 'none';
  var preQual = $$('selQualCd').value;
  if(preQual != o.innerHTML){
  var qualValue = $$('navQualCd').value;
  if(qualValue.trim()=='UCAS CODE'){
      $$('navKwdCd').value = ucasDef;
  }else{
      $$('navKwdCd').value = kwDef;
  }
    $$('keywordCd_hidden').value = "";
    $$('keywordCd_id').value = "";
    $$('keywordCd_url').value = "";
    $$('keywordCd_display').value = "";
    $$('matchbrowsenodeCd').value = "";
  }
}


function showhideDivCd(index){
  if($$("keyStats_"+index)){
     if($$("keyStats_"+index).style.display == 'block' || $$("keyStats_"+index).style.display == ''){
        $$("keyStats_"+index).style.display = 'none';  
        $$("arrow_key_stats_"+index).className = 'fa fa-angle-right fa-lg fa-1_5x';  
     }else{
        $$("keyStats_"+index).style.display = 'block';  
        $$("arrow_key_stats_"+index).className = 'fa fa-angle-down fa-lg fa-1_5x';  
     }
 }
}

function showhideDivPreviousStudy(index){
  if($$("prev_study_"+index)){
     if($$("prev_study_"+index).style.display == 'block'){
        $$("prev_study_"+index).style.display = 'none';  
        $$("arrow_prev_study_"+index).className = 'fa fa-angle-right fa-lg fa-1_5x';  
     }else{
        $$("prev_study_"+index).style.display = 'block';  
        $$("arrow_prev_study_"+index).className = 'fa fa-angle-down fa-lg fa-1_5x';  
     }
 }
}

function cdPageVideoShow(videoId, collegeId, videoType){
  var pageName = 'cd_page';
  var val = showVideoCdPage(videoId, collegeId, videoType, pageName);
 return val;
}
var adv = jQuery.noConflict();
function showVideoCdPage(videoid, collegeid, videoType, pageName){  
  var filePath = document.getElementById("contextJsPath").value+'/js/video/jwplayer.js';
   if (adv('head script[src="' + filePath + '"]').length > 0){
   }else{
    dynamicLoadJS(filePath);
  }
  setTimeout(function(){showVideoAjaxCd(videoid, collegeid, videoType, pageName)}, 2000);
  }
function showVideoAjaxCd(videoid, collegeid, videoType, pageName){

   var offsetlft = "";
   if($$("theImages") !=null){
       offsetlft= "&offsetlft=0";
   }
   var contextPath = "/degrees";
   var ajax=new sack();
   var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
   var networkId = $$('networkId') ? "&networkId="+$$('networkId').value : "";
   var url = contextPath+'/vids-video.html?vrid='+videoid+"&cid="+collegeid+"&refererUrl="+$$("refererUrl").value+"&videoType="+videoType+offsetlft + networkId +'&screenwidth='+deviceWidth+'&pageName='+pageName;   
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ showVideoWindowCd(ajax); };
   ajax.runAJAX();
}
function showVideoWindowCd(ajax){  
   var data=ajax.response.split("|BREAK|");	
   var contextPath = $$('contextPath').value;
      if(!document.getElementById("mbox")){
        initmb();            
      }
      if (data[4] == 'CUSTOM'){sm('box', 590, 460);}else{sm('box', 590, 490);}
			jwplayerSetup('flashbanner', data[5], "<%=CommonUtil.getJsPath()%>/js/limelightvideoplayer/isight.gif", 590, 380, data[5]); //Added by Prabha on 27-JAN-16
  var linkobj =  $$('linksdiv');
  linkobj.innerHTML =data[20];
}

function checkskiplink(){
  if(window.location.hash) {
      var hash = window.location.hash.substring(1); //Puts hash in variable, and removes the # character
     if (hash=='fees'){
       var $fp = jQuery.noConflict();     
        var divPosition = $fp('#fees').offset();
        $fp('html, body').animate({
            scrollTop: divPosition.top - 320
            }, "slow");
     }
     if (hash=='entryrequirments'){
       var $fp = jQuery.noConflict();     
        var divPosition = $fp('#entryrequirments').offset();
        $fp('html, body').animate({
            scrollTop: divPosition.top - 320
            }, "slow");
     }
  }
}
//added print css ga logg :: Prabha :: 27_JAN_2016_REL
function printCssBtn(){
  eventTimeTracking();
  window.print();
  ga('send', 'event', 'Print', 'Downloaded', 'Course Details', 1, {nonInteraction: true});  
}
//load articles pod on scroll on 13_06_2017, By Thiyagu G.
var cd = jQuery.noConflict();
cd(document).ready(function(){ 
  cd(window).scroll(function(){ // scroll event
    if($$("scrollStop")){
      if(cd("#scrollStop").val() != 1){
        cd("#scrollStop").val(1);
        loadArticlesPodJS('COURSE_DETAIL_PAGE');   
        setTimeout(function() {articlesSlider();}, 500);
      }
    }    
  });
  if($$("scrollStop")){
    cd("#scrollStop").val(0);
  }
});

//added by Hema.S on 02-02-2018 for ucas new Fees structure Changes
var dev = jQuery.noConflict();
dev(document).ready(function(){
   feestabact();
   entryRequirments();
});
function feestabact(){
  dev(".scrltab a:first-child").addClass("tabact");
  var text = dev('.scrltab a.tabact').text();
  var act_tab_sel_1 = dev('.scrltab a.tabact').attr('href');
  if(dev('.scrltab a').length == 0){
    dev('.tution_fees').hide();
    dev('#sideTutionTab').hide();
  }
  dev(act_tab_sel_1).addClass('active');
  dev( ".scrltab a" ).each(function() {
    dev(this).click(function(event) {
      event.preventDefault();
      lazyloadetStarts();
      var act_tab_sel = dev('.scrltab a.tabact').attr('href');
      dev(act_tab_sel).removeClass('active');
      dev(act_tab_sel).addClass('hide'); 
      //dev(act_tab_sel).show();
      var tar_tab_sel = dev(this).attr('href');
      dev(tar_tab_sel).removeClass('hide');
      dev(tar_tab_sel).addClass('active');           
      dev( this ).siblings().removeClass("tabact");
      //dev( this ).toggleClass("tabact");
      dev( this ).addClass("tabact");
      text = dev('.scrltab a.tabact').text();
      lazyloadetStarts();
      document.getElementById("mbleview").innerHTML = text +'<i class="fa fa-angle-down fa-lg"></i>';
      if(dev(document).width() < 768 ) {
        dev('.scrltab').hide();
      }
      dev(".feerw_rt .fstu_lnk a").click(function(e)
     {
      e.preventDefault();
      });
    });
    document.getElementById("mbleview").innerHTML = text +'<i class="fa fa-angle-down fa-lg"></i>';
  });
  dev(".fee_nav .fnav_wrp").click(function(event) {
    dev(".scrltab").slideToggle(700);
  }); 
  }
function showAndHideToolTip(id){
  if(document.getElementById(id).style.display == "block") {
   document.getElementById(id).style.display = "none";
   
  } else {
   document.getElementById(id).style.display = "block";
  }
}
function hideTooltip(id){     
    document.getElementById(id).style.display = "none";
}
//
function showTooltip(id){
  if(dev(document).width() > 1024 ) {
    document.getElementById(id).style.display = "block";
  }
}
//added for etnry requirments structure change by Hema.S on 11.5.2018
function entryRequirments(){
  var id = dev("#dropdownId").children(":selected").attr("id");
  dev("#entryPoints_"+id).show();
  dev("#additionalInfo_"+id).show(); 
}
function subReviewBreakDown(id,collegeId,subjectId,subjectName){//Added for subject breakdown list by Hema.S on 18_DEC_2018_REL
  var selectDropDownid =  $fp("#subjectDropDownID_"+id).text();
  document.getElementById("allSubjectDropDownId").innerHTML = "<span>"+selectDropDownid+"</span>"+'<span><i class="fa fa-angle-down"></i></span>';
  document.getElementById("selectedSubjectDropDown").innerHTML = "Latest "+selectDropDownid+" reviews";
  var contextPath = "/degrees";
   if(selectDropDownid != ''){
    ga('send', 'event', 'Review page filter', 'Subject', selectDropDownid.trim(), 1, {nonInteraction: true});
  }
  var ajax=new sack();
  var url = contextPath+'/get-review-break-down-list.html?collegeId='+collegeId+"&subjectId="+subjectId+"&subjectName="+subjectName;   
  ajax.requestFile = url;	
  ajax.onCompletion = function(){subjectReviewBreakDown(ajax)};
  ajax.runAJAX();
}

function subjectReviewBreakDown(ajax){
  var response = ajax.response;
  if(response != null){
  document.getElementById("subjectDropDownId").innerHTML = ajax.response;
  }
   dev("#subjectDropDown").css({'display':'none','z-index':'-1'});
}
function changeTitle(id){//Added this for getting review pod title by Hema.S on 18_DEC_2018_REL
  var starArray = ["subject5Star_","subject4Star_","subject3Star_","subject2Star_","subject1Star_","5star_","4star_","3star_","2star_","1star_"]
  var starwidthId = ["subject5StarWidthId","subject4StarWidthId","subject3StarWidthId","subject2StarWidthId","subject1StarWidthId","5starWidthId","4starWidthId","3starWidthId","2starWidthId","1starWidthId"];
  var starTextId = ["subject5StartextId","subject4StartextId","subject3StartextId","subject2StartextId","subject1StartextId","5StarId","4StarId","3StarId","2StarId","1StarId"];
  for(i=0; i<10;i++){
    var starValue = $$d(starArray[i]+id).value;
    $$d(starwidthId[i]).style = "width:"+starValue+"%";
    $$d(starTextId[i]).innerHTML= starValue+"%";
  }
  $fp("#starId_"+id).show();
  var selectdid =  $fp("#questionDropDown_"+id).text();
  var questionText =  $fp("#questionSelectedText").text();
  document.getElementById("questionSelectedText").innerHTML = "<span>"+selectdid+"</span>"+'<span><i class="fa fa-angle-down"></i></span>';
  
  
}
function commonPhraseViewMoreAndLess(viewMoreOrLessBtnId){//Added this for common phrases view less and more pod by Hema.S on 18_DEC_2018_REL
  if("comPhraseViewMore"==viewMoreOrLessBtnId){    
    adv( ".extraCP" ).show();
    blockNone("comPhraseViewMore","none");
    blockNone("comPhraseViewLess","block");
  }else{
    adv( ".extraCP" ).hide();
    blockNone("comPhraseViewMore","block");
    blockNone("comPhraseViewLess","none");
  }
}
//Modified the the functionality to keyword search specific alone by Sangeeth.S for FEB_12_19 rel
function commonPhrasesListURL(formNameId,orderBy,obj){  //Added this for looking specific pod by Hema.S on 18_DEC_2018_REL
  var searchUrl = ""; 
  var finalUrl = "";  
  var keyword = ""; 
  var queryStringUrl = "";
  var eventAction = "";
  var eventLabel = "";
  if("reviewCommonPhraseKwdForm"==formNameId && obj){
    keyword  = obj.innerText;
  }else{
    keyword    = $$('reviewSearchKwd').value;   
  }    
  if(keyword.trim()=="" || keyword=="Something specific?"){
    keyword = "";
  }       
  if(keyword!=""){
    eventLabel = keyword;
    var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
    keyword = keyword.replace(reg," ");
    keyword = replaceAll(keyword," ","-");          
    queryStringUrl = "?keyword=" + encodeURIComponent(keyword);
    searchUrl = "/university-course-reviews/search/"+queryStringUrl;
  }  
  if(searchUrl!=""){
    finalUrl = searchUrl;
  }
 //Added for GA event logging in review search page by Prabha on SEP_24_2019_REL
  if((formNameId == 'reviewSearchKwdForm' || formNameId == 'reviewCommonPhraseKwdForm') && (keyword!="")){
    eventAction = "Keyword";
  }
  if(eventAction != ''){
    ga('send', 'event', 'Review page filter', eventAction, eventLabel.trim(), 1, {nonInteraction: true});
  }
  //End of GA event logging
  if(finalUrl!=""){finalUrl= finalUrl.toLowerCase()};//Added by Sangeeth.S for FEB_12_19 rel lower case of urls  
  if(formNameId!=undefined && $$(formNameId)){
    $$(formNameId).action = finalUrl;
    $$(formNameId).submit();
  }else{
    if(finalUrl ==""){finalUrl="/university-course-reviews/";}	
    location.href = finalUrl;
  }    
  return false;
}
function setReviewSearchText(curid, searchmsg){var ids = curid.id; if($$(ids).value=='') { $$(ids).value=searchmsg;} }
function clearReviewSearchText(curid, searchmsg){var ids = curid.id;if($$(ids).value==searchmsg){$$(ids).value="";}}
function validateSrchIcn(srchIconId,inputId) {   
   if(adv('#'+srchIconId) && adv('#'+inputId).val().trim()!=""){
     adv('#'+srchIconId).removeClass("srch_dis")
   }else{
     adv('#'+srchIconId).addClass("srch_dis")
   }
}
function openReviewDropdown(){
var width = document.documentElement.clientWidth;
if(width <= 1024){  
  dev('#fieldsetId').click(function(event){
if(event.target.className != 'liclass') {
          dev("#reviewDropDown").css({'display':'block','z-index':'1'});   
          }
          });
}
else if(width > 1024){
 dev("#fieldsetId").hover(function(){
          dev("#reviewDropDown").css({'display':'block','z-index':'1'});
          }, function(){
          dev("#reviewDropDown").removeAttr("style");
      });
}
}
function openDropdown(){
var width = document.documentElement.clientWidth; 
if(width <= 1024){
           dev("#location").click(function(){
          dev("#subjectDropDown").css({'display':'block','z-index':'1'});
          }, function(){
          dev("#subjectDropDown").removeAttr("style");
      });
}
else if(width > 1024){
 dev("#location").hover(function(){
          dev("#subjectDropDown").css({'display':'block','z-index':'1'});
          }, function(){
          dev("#subjectDropDown").removeAttr("style");
      });
}
}
function closeDropdown(){
 dev("#reviewDropDown").css({'display':'none','z-index':'-1'});
}
function wugoaplynwpage(collegeId, courseId, subOrderIteId){
 var opportunityId = (document.getElementById("oppId") && document.getElementById("oppId").value != "") ? document.getElementById("oppId").value : null;
 var domainSpecPath = document.getElementById("domainSpecPath").value;
 if(dev("#loadingImg")){dev("#loadingImg").show();}
  getWugoCpeStatsLogging(collegeId, courseId, opportunityId, subOrderIteId);
 
 window.location.href = domainSpecPath+"/degrees/wugo/apply-now.html?courseId="+courseId+"&opportunityId="+opportunityId;
}
function changeContactButtonCD(hotlineNumber, networkId, gaCollegeName, suborderItemId, anchorId, collegeId) {
  dev("#" + anchorId).attr("title", hotlineNumber);
  dev("#" + anchorId).html("<i class='fa fa-phone' aria-hidden='true'></"+ "i>"+hotlineNumber);
  var mobileFlag = dev("#check_mobile_hidden").val();
  if(mobileFlag == 'true'){
    setTimeout(function(){
     location.href='tel:'+ hotlineNumber;
    },1000)
  } 
}
//
function wugoApplyNow(collegeId, courseId, subOrderIteId){
  var opportunityId = (document.getElementById("oppId") && document.getElementById("oppId").value != "") ? document.getElementById("oppId").value : null;
  var domainSpecPath = document.getElementById("domainSpecPath").value;
  if(dev("#loadingImg")){dev("#loadingImg").show();}
  var url = contextPath + "/ajaxlogging/cpe-web-click-db-logging.html"; 
  var deviceWidth =  getDeviceWidth();
  var networkId = '2';
  dev.ajax({
    url: url,
    async: false,
    data: {
      "vwcourseId": courseId,
      "z": collegeId,
      "externalUrl": opportunityId,
      "screenwidth": deviceWidth,
      "networkId": networkId,
      "clickType": "APPLY_NOW",
      "subOrderItemId": subOrderIteId
    },
    type: "POST",
    complete: function(result){
      //if(dev("#loadingImg")){dev("#loadingImg").hide();}
      //alert("result--->"+result);
    }
  });
  window.location.href = domainSpecPath+"/degrees/wugo/apply-now.html?courseId="+courseId+"&opportunityId="+opportunityId;
}
//
function showUniInfoDescCD(vmId, lessDescId, moreDescId){
 dev("#"+vmId).css('display:none');
 dev("#"+lessDescId).css('display:none');
 dev("#"+moreDescId).css('display:block');
}

function changeContact(hotlineNumber, anchorId) {
  $fp("#" + anchorId).attr("title", hotlineNumber);
  $fp("#" + anchorId).html("<i class='fa fa-phone' aria-hidden='true'></"+ "i>"+hotlineNumber);
  var mobileFlag = $fp("#check_mobile_hidden").val();
  if(mobileFlag == 'true'){
    setTimeout(function(){
     location.href='tel:'+ hotlineNumber;
    },1000)
  } 
}
