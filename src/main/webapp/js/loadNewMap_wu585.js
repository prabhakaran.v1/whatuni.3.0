//Addded mapbox script map by sangeeth.s for FEB_12_19 rel  
var mapRes = '';
var mpb = jQuery.noConflict();
function lboxNewMapInitialize(latitude, longtitude){
  mapboxgl.accessToken = mpb('#mbApiKey').val();
  var map = new mapboxgl.Map({
    container: "map_div",
    center:[ longtitude, latitude],      
    style: mpb('#mbStyle').val(),
  });
  map.addControl(new mapboxgl.NavigationControl(), 'bottom-right');
  var contentString = document.getElementById("locationinfoMaphh").innerHTML;
  new mapboxgl.Marker() .setLngLat([longtitude, latitude])
  .setPopup(new mapboxgl.Popup().setHTML(contentString))
    .addTo(map);    
  bound = new mapboxgl.LngLatBounds([longtitude, latitude],[longtitude, latitude]);
  map.fitBounds([bound.getNorthEast(), bound.getSouthWest()],{animate: false});	  
  var adjZoomLevel = ((map.getZoom()/2)-2);
  map.setZoom(map.getZoom()- adjZoomLevel);
  mapRes = map;
}
mpb(window).on('load orientationchange', function (e) {
    setTimeout(function() {      
      if(mapRes!=''){
        mapRes.resize();     
      }
    }, 200);
});
//