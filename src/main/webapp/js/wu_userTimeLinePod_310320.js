var showSlide = false;
function $$UT(id){return document.getElementById(id)}
function viewUserTimeLinePod() {  
  var $tlv = jQuery.noConflict();
  var pageName = $tlv("#pageNameTimeLine").val();
  if(pageName == "clearingEbookSuccess.jsp" || pageName == "clearingEbookLandingPage.jsp"){
	$tlv(".clipart").prepend("<div class=\"large\" style=\"display:none\"></div>");  
  }else{
    $tlv(".clipart").prepend("<div class=\"large\"></div>");
  }
  
  $tlv('body').addClass("md");
  $tlv(".clipart").removeClass("clipart").addClass("clipart md");      
  
  $tlv('ul#timeLineUL > li > ul > li').mouseover(function(){         
    var className = $tlv('.clipart').attr('class')
    $tlv('body').removeClass("md").addClass("lg");  
    $tlv(".clipart").removeClass(className).addClass("clipart lg");   
    lazyloadetStarts();    
    if($tlv('#clearingHome').length){
      $tlv("#sky").css("top", "750px");
    }
  });
} 

function showHomeTimeLineImg(){  
  var $tlv = jQuery.noConflict();
  $tlv('ul#timeLineUL > li > ul > li').mouseover(function(){               
    lazyloadetStarts();    
  });
}

function hideTimeLinePod(){    
  var $tlh = jQuery.noConflict();
  jQuery.noConflict();      
  var className = $tlh('.clipart').attr('class');    
  
  if(!showSlide){  
    if(className=="clipart lg"){      
      $tlh(".clipart").removeClass(className).addClass("clipart sl");   
      $tlh('body').removeClass("lg").addClass("sl");
      $tlh("#timeLineCnt").slideUp(1000);      
      $tlh('#timeLineCnt').hide();      
      $tlh("#hdr_menu").css("display", "block");
      if($tlh('#clearingHome').length){
        $tlh("#sky").css("top", "600px");
      }
    }else if(className=="clipart sl"){
      $tlh("#timeLineCnt").slideDown(1000);
      $tlh(".clipart").removeClass(className).addClass("clipart md");              
      $tlh('#timeLineCnt').show();      
      $tlh('body').removeClass("sl").addClass("md");
      if($tlh('#clearingHome').length){
        $tlh("#sky").css("top", "670px");
      }
      showSlide = true;
    }else if(className=="clipart md"){
      $tlh(".clipart").removeClass(className).addClass("clipart sl");
      $tlh("#timeLineCnt").slideUp(500,function() {$tlh('body').removeClass("md").addClass("sl");});
      $tlh('#timeLineCnt').hide();
      if($tlh('#clearingHome').length){
        $tlh("#sky").css("top", "600px");
      }
    }
    
   }   
   $tlh("#hideTimeLine").click(function(){
    showSlide = false;    
   });
}
//Restricted the function to execute if mysettings alert box is shown Indumathi.S Jan-27-16
function saveUserTimeLine(tlLineId,timeLineURL,type,flag){  
 var  url = "/degrees/user-timeline/update.html?tlLineId="+tlLineId;  
 if(flag == "setURL"){
  $$("mail_a_href").value = timeLineURL;
 }else if($$UT("functionCallFlag") == null || $$UT("functionCallFlag").value != "no"){
  if($$UT("functionCallFlag")) { $$UT("functionCallFlag").value = "no"; }
  var req;   
  if(typeof XMLHttpRequest != "undefined") {
   req=new XMLHttpRequest();
  }else if(window.ActiveXObject) {
   req=new ActiveXObject("Microsoft.XMLHTTP");
  }
  req.open("POST",url,true);
  req.onreadystatechange=function(){
   if (req.readyState==4){
    openTimeLineURL(timeLineURL,type);
   }
  };
  req.send(url);	
 } else if($$UT("definetLineId") && $$UT("defineUserTimeLineURL") && $$UT("defineHrefType") && $$UT("functionCallFlag")) {
  $$UT("definetLineId").value = tlLineId;
  $$UT("defineUserTimeLineURL").value = timeLineURL;
  $$UT("defineHrefType").value = type;
  $$UT("functionCallFlag").value = "redirect";
 }
}
//End
function openTimeLineURL(timeLineURL,type) {  
  if(type=="_self"){    
   self.location.href = timeLineURL;    
  }
}