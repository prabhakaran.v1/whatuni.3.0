var $jq = jQuery.noConflict();
var jsPath = $jq("#jsPathHidden").val();

var newSearchResults_scriptTag = document.createElement('script');
newSearchResults_scriptTag.src = jsPath + 'wu_newSearchResults_20201110.js';    
document.body.appendChild(newSearchResults_scriptTag)

var cpeStats_scriptTag = document.createElement('script');
cpeStats_scriptTag.src = jsPath + 'wu_cpeStatsLogging_20200917.js';    
document.body.appendChild(cpeStats_scriptTag)

var socialbox_scriptTag = document.createElement('script');
socialbox_scriptTag.src = jsPath + 'javascripts/wu_socialBox_20200721.js';    
document.body.appendChild(socialbox_scriptTag)

var switchHeader_scriptTag = document.createElement('script');
switchHeader_scriptTag.src = jsPath + 'home/wu_switchHeader_20200721.js';    
document.body.appendChild(switchHeader_scriptTag)

var myFinalChoice_scriptTag = document.createElement('script');
myFinalChoice_scriptTag.src = jsPath + 'mychoice/myFinalChoice_wu565.js';    
document.body.appendChild(myFinalChoice_scriptTag)

var interaction_scriptTag = document.createElement('script');
interaction_scriptTag.src = jsPath + 'javascripts/wu_interaction_20200721.js';    
document.body.appendChild(interaction_scriptTag)
