var $jq = jQuery.noConflict();
var GTM_SR_JSON_ARRAY={};
var GTM_PR_JSON_ARRAY={};

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-127963276-1', {'name': 'ecTracker'});
ga('ecTracker.require', 'ec');
$jq(document).ready(function(){
  var pageName = $jq('#eCommPageName').val();
  var userType = $jq('#userType').val();
  if(pageName == 'Search Results Page'){
  var instIds = $jq("#instIds").val();
  var instNames = $jq("#instNames").val();
  var qual = $jq("#qualificationInSR").val();
  var subjName = $jq("#subjectname").val();
  var pageNo = $jq('#pageNum').val();
  var region = $jq('#GTMRegion').val();
  var cpe = $jq('#GTMCpe2').val();
  var jacs = $jq('#GTMJacs').val();
  var keyword = $jq('#GTMKeyword').val();
  var LDCS = $jq('#GTMLDCS').val();
  var subjectL1 = $jq('#subjectL1').val();
  var subjectL2 = $jq('#subjectL2').val();
  var instId = "";
  var instName = "";
  if(instIds != ""){
    instId = instIds.split("###");
    instName = instNames.split("###");
  }
  var SearchResults = [];
  if(instId.length > 0){
    for(var i=1; i<=instId.length;i++ ){
      var insPosition = ((pageNo-1) * 10) + i;
      var item = {};
      var matchTypeValue = null;
      if(userType == 'CLEARING') {
    	  matchTypeValue = getMatchTypeValue(instId[i-1]);
      }
      item ["Id"] = instId[i-1];
      item ["Name"] = instName[i-1];
      item ["List"] = "Search Results";
      item ["Brand"] = instName[i-1];
      item ["Category"] = region;
      item ["Variant"] = qual;
      item ["Position"] = insPosition;
      item ["Price"] = "1";
      item ["Dimension2"] = instId[i-1];
      item ["Dimension3"] = instName[i-1];
      item ["Dimension4"] = qual;
      item ["Dimension5"] = LDCS;
      item ["Dimension6"] = jacs;
      item ["Dimension7"] = cpe;
      item ["Dimension8"] = region;
      item ["Dimension9"] = keyword;
      item ["Dimension10"] = subjectL1;
      item ["Dimension11"] = subjectL2;
      if(matchTypeValue != null && matchTypeValue != "" ) {
      item ["Dimension14"] = matchTypeValue;
      }
      SearchResults.push(item);
      //ga('ecTracker.ec:addImpression', item);	  
    }
  }
  GTM_SR_JSON_ARRAY["SearchResults"] = SearchResults;  
  $jq('#gtmJsonDataSR').text(JSON.stringify(GTM_SR_JSON_ARRAY));
  //ga('ecTracker.send', 'pageview');
  }
  if(pageName == 'Provider Results page'){
    var ProviderResults = [];
	var courseTitles = $jq("#courseTitles").val();
	var courseTitle = courseTitles.split("###");
	var courseIds = $jq("#courseId").val();
	var courseId = courseIds.split("###");
	var courseDomesticfees = $jq("#courseDomesticfee").val();
	if(courseDomesticfees != null && courseDomesticfees != "" && courseDomesticfees != undefined) {
	  var courseDomesticfee = courseDomesticfees.split("###");
	}
	var institutionName = $jq("#instName").val();
	var institutionId = $jq("#institutionId").val();
	var subjectName = $jq("#subjectName").val();
	var qualification = $jq("#paramStudyLevelId").val();
	var pageNo = $jq('#pageNo').val();
	var region = $jq('#GTMRegion').val();
	var cpe = $jq('#GTMCpe2').val();
	var jacs = $jq('#GTMJacs').val();
	var keyword = $jq('#GTMKeyword').val();
	var LDCS = $jq('#GTMLDCS').val();
	var subjectL1 = $jq('#subjectL1').val();
	var subjectL2 = $jq('#subjectL2').val();
	for(var i=1; i<=courseTitle.length;i++ ){
	  var coursePosition = i != null ? i : 0 ;
	  coursePosition = ((pageNo-1) * 10) + i;
	  var item = {};
	  var matchTypeValue = null;
      if(userType == 'CLEARING') {
        matchTypeValue = getMatchTypeValue(courseId[i-1]);
      }
	  item ["Id"] = courseId[i-1];
	  item ["Name"] = courseTitle[i-1];
	  item ["List"] = "Provider Results";
	  item ["Brand"] = institutionName;
	  item ["Category"] = region;
	  item ["Variant"] = qualification;
	  item ["Position"] = coursePosition;
	  item ["Price"] = "1";
	  item ["Dimension2"] = courseId[i-1];
      item ["Dimension3"] = courseTitle[i-1];
      item ["Dimension4"] = qualification;
      item ["Dimension5"] = LDCS;
      item ["Dimension6"] = jacs;
      item ["Dimension7"] = cpe;
      item ["Dimension8"] = region;
      item ["Dimension9"] = keyword;
      item ["Dimension10"] = subjectL1;
      item ["Dimension11"] = subjectL2;
      if(matchTypeValue != null && matchTypeValue != "") {
        item ["Dimension14"] = matchTypeValue;
      }
	  ProviderResults.push(item);
	  //ga('ecTracker.ec:addImpression', item);	 
	}
	GTM_PR_JSON_ARRAY["ProviderResults"] = ProviderResults;
	$jq('#gtmJsonDataPR').text(JSON.stringify(GTM_PR_JSON_ARRAY));
	//ga('ecTracker.send', 'pageview'); 
  }
});

function gtmproductClick(eventPosition, productClickPage, institutionId){
	//alert("dfsdf")
  var pageName = $jq('#eCommPageName').val();
  var userType = $jq('#userType').val();
  var position = eventPosition != null ? eventPosition : 0  
  var productClickPageName = productClickPage;
  var ecom_boostedInstId = !isBlankOrNullString(adv("#boostedInstId").val()) ? adv("#boostedInstId").val() : "";
  var ecom_sponsoredInstId = !isBlankOrNullString(adv("#sponsoredInstId").val()) ? adv("#sponsoredInstId").val() : "";
  var ecom_sponsoredFlag = !isBlankOrNullString(adv("#sponsoredListFlag").val()) ? adv("#sponsoredListFlag").val() : "";
  var ecom_manualFlag = !isBlankOrNullString(adv("#manualBoostingFlag").val()) ? adv("#manualBoostingFlag").val() : "";
  if(pageName == 'Search Results Page'){
    var qualification = $jq('#qualificationInSR').val() ? $jq('#qualificationInSR').val() : "";
    var subjectName = $jq('#subjectname').val() ? $jq('#subjectname').val() : "";         
    var SR_PAGE_PRODUCT_CLICK = [];
    var productClick = {};
    productClick["Id"] = GTM_SR_JSON_ARRAY.SearchResults[position].Id;                   
    productClick["Name"] =  GTM_SR_JSON_ARRAY.SearchResults[position].Name;
    productClick["List"] =  "Search Results";
    productClick["Brand"] =  GTM_SR_JSON_ARRAY.SearchResults[position].Brand;                    
    productClick["Category"] = GTM_SR_JSON_ARRAY.SearchResults[position].Dimension8;
    productClick["Variant"] =  qualification;
    productClick["Position"] =  GTM_SR_JSON_ARRAY.SearchResults[position].Position;
    productClick["Price"] = "1";
    productClick["Dimension2"] = GTM_SR_JSON_ARRAY.SearchResults[position].Id;
    productClick["Dimension3"] = GTM_SR_JSON_ARRAY.SearchResults[position].Name;
    productClick["Dimension4"] = qualification;
    productClick["Dimension5"] = GTM_SR_JSON_ARRAY.SearchResults[position].Dimension5;
    productClick["Dimension6"] = GTM_SR_JSON_ARRAY.SearchResults[position].Dimension6;
    productClick["Dimension7"] = GTM_SR_JSON_ARRAY.SearchResults[position].Dimension7;
    productClick["Dimension8"] = GTM_SR_JSON_ARRAY.SearchResults[position].Dimension8;
    productClick["Dimension9"] = GTM_SR_JSON_ARRAY.SearchResults[position].Dimension9;
    productClick["Dimension10"] = GTM_SR_JSON_ARRAY.SearchResults[position].Dimension10;
    productClick["Dimension11"] = GTM_SR_JSON_ARRAY.SearchResults[position].Dimension11;
     
    if((productClickPageName == "sr_to_cd") && ($jq('#sr_to_cd_'+position).length) || (productClickPageName == "sr_to_cd_view_modules")){
      productClick["Dimension12"] = "Course Details";
    }else if((productClickPageName == "sr_to_pr") && ($jq('#sr_to_pr_'+position).length) || (productClickPageName == "sr_to_pr_courses_available")){
      productClick["Dimension12"] = "Provider Results";
    }
    
    if(productClickPageName == "sponsor_type_featured_college_name" || productClickPageName == "sponsor_type_featured_reviews" || productClickPageName == "sponsor_type_featured_view_courses"){
      productClick["Dimension13"] = "Featured";	
    }else if(institutionId == ecom_boostedInstId && ecom_manualFlag && "MANUAL_BOOSTING" == ecom_manualFlag){
      productClick["Dimension13"] = "Manual";	
    }else if(institutionId == ecom_sponsoredInstId && ecom_sponsoredFlag && "SPONSORED_LISTING" == ecom_sponsoredFlag){
      productClick["Dimension13"] = "Sponsored";	
    }
    if(userType == 'CLEARING') {
    productClick["Dimension14"] = GTM_SR_JSON_ARRAY.SearchResults[position].Dimension14;
    }
    SR_PAGE_PRODUCT_CLICK.push(productClick);
    ga('ecTracker.ec:addProduct',productClick);
    ga('ecTracker.ec:setAction', 'click', {list: 'Search Results'});

   
    ga('ecTracker.send', 'event', 'UX', 'click', 'Results', {
    });
  }
  else if(pageName == 'Provider Results page'){
    var subjectName = $jq("#subjectName").val();
    var qualification = $jq("#paramStudyLevelId").val(); 
    var PR_PAGE_PRODUCT_CLICK = [];
    var productClick = {};
    productClick["Id"] =   GTM_PR_JSON_ARRAY.ProviderResults[position].Id;                 
    productClick["Name"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Name;
    productClick["List"] = "Provider Results";
    productClick["Brand"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Brand;
    productClick["Category"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Dimension8;
    productClick["Variant"] = qualification;
    productClick["Position"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Position;
    productClick["Price"] = "1";
    productClick["Dimension2"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Id;
    productClick["Dimension3"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Name;
    productClick["Dimension4"] = qualification,
    productClick["Dimension5"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Dimension5;
    productClick["Dimension6"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Dimension6;
    productClick["Dimension7"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Dimension7;
    productClick["Dimension8"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Dimension8;
    productClick["Dimension9"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Dimension9;
    productClick["Dimension10"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Dimension10;
    productClick["Dimension11"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Dimension11;
    if((productClickPageName == "pr_to_cd") && ($jq('#pr_to_cd_'+position).length) || (productClickPageName == "pr_to_cd_view_modules")){
      productClick["Dimension12"] = "Course Details";	
    }
    if(userType == 'CLEARING') {
      productClick["Dimension14"] = GTM_PR_JSON_ARRAY.ProviderResults[position].Dimension14;
    }
    PR_PAGE_PRODUCT_CLICK.push(productClick);
    ga('ecTracker.ec:addProduct',productClick);
	ga('ecTracker.ec:setAction', 'click', {list: 'Provider Results'});
	   
    ga('ecTracker.send', 'event', 'UX', 'click', 'Results', {
    	
    });
  }
}


 