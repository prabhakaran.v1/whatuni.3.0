function isValidText(reviewtext, pattern) {
  var index = -1;
  if(reviewtext != null) { index = reviewtext.search(pattern);  }
  return (index != -1) ? true : false;
}
function changeMoreReviewListURL(formtype)  {
   with(document) {
       var current_pagno = $$('current_pageno').value; var current_sort = $$('current_sortorder').value;
       var current_sid   = $$('current_sid').value;    var current_iama = $$('current_iama').value;
       if(formtype == 'subject') {current_sid  = $$('subjectid_value').options[$$('subjectid_value').selectedIndex].value; current_pagno ="1";}    
       if(formtype == 'iama') { current_iama = $$('usertype_value').options[$$('usertype_value').selectedIndex].value; current_pagno ="1";} 
       var formaction   =  $$('more_review_action').value;
       var urlArray=formaction.split("/");
       var finalUrl = '/degrees';
        for (var i=0;i<urlArray.length;i++){
          if(($$("reviewSearchKeyword") !=null && ($$("reviewSearchKeyword").value =='0')) && $$('subjectid_value').options[$$('subjectid_value').selectedIndex].value !=''){
             if (i == 1){ finalUrl = finalUrl + '/'+replaceHypen(replaceURL($$('subjectid_value').options[$$('subjectid_value').selectedIndex].text.toLowerCase()))+'-course-reviews'; }
          }else{
             if (i == 1){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          }   
          if (i == 3){ finalUrl = finalUrl+'/'+ (trimString(current_sort) == ''? 'highest-rated' : current_sort); }
          if (i == 4){ finalUrl = finalUrl+'/'+(trimString(current_iama) == '' || trimString(current_iama) == 'Please select'? '0' : current_iama); }
          if (i == 5){  finalUrl = finalUrl+'/'+ (trimString(current_sid) == ''? '0' : current_sid); }
          if (i == 6){ finalUrl = finalUrl+'/'+ (trimString(current_pagno) == ''? '1' : current_pagno); }
        }
        if($$("reviewSearchKeyword") !=null && ($$("reviewSearchKeyword").value !='0')){
           finalUrl = finalUrl.toLowerCase()+"/subject-reviews.html";
        }else{    
          if(trimString(current_sid) ==''){
              finalUrl = finalUrl.toLowerCase()+"/reviews.html";
          }else{
             finalUrl = finalUrl.toLowerCase()+"/coursereviews.html";
          }
        }    
        if(formtype == 'subject'){
             $$('subfilterform').action = finalUrl.toLowerCase(); $$('subfilterform').submit(); return false;
         }else{
            $$('iamafilterform').action = finalUrl.toLowerCase(); $$('iamafilterform').submit(); return false;
        }
    }
 }
 function changeMoreReviewList(formtype)  {
 with(document) {
       var current_pagno = $$('current_pageno').value; var current_sort = $$('current_sortorder').value;
       var current_sid   = $$('current_sid').value;    var current_iama = $$('current_iama').value;
       if(formtype == 'subject') {current_sid  = $$('subjectid_value').options[$$('subjectid_value').selectedIndex].value; current_pagno ="1";}    
       if(formtype == 'iama') { current_iama = $$('usertype_value').options[$$('usertype_value').selectedIndex].value; current_pagno ="1";} 
       var formaction   =  $$('more_review_action').value;
       // /reviews/uni/bangor-university-reviews/lowest-rated/0/0/3769/1/studentreviews.html
       var urlArray=formaction.split("/");
       var finalUrl = '/degrees';
        for (var i=0;i<urlArray.length;i++){
          if (i == 0){ finalUrl = finalUrl + urlArray[i]; }
          if (i == 1){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 2){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 3){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 4){ finalUrl = finalUrl+'/'+ (trimString(current_sort) == ''? 'highest-rated' :   replaceAll(current_sort, "_","-")  ); }
          if (i == 5){ finalUrl = finalUrl+'/'+(trimString(current_iama) == '' || trimString(current_iama) == 'Please select'? '0' : current_iama); }
          if (i == 6){  finalUrl = finalUrl+'/'+ (trimString(current_sid) == ''? '0' : current_sid); }
          if (i == 7){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 8){ finalUrl = finalUrl+'/'+ (trimString(current_pagno) == ''? '1' : current_pagno); }
        }
        finalUrl = finalUrl.toLowerCase()+"/studentreviews.html";
        if(formtype == 'subject'){
             $$('subfilterform').action = finalUrl.toLowerCase(); $$('subfilterform').submit(); return false;
         }else{
            $$('iamafilterform').action = finalUrl.toLowerCase(); $$('iamafilterform').submit(); return false;
        }
    }
 }
