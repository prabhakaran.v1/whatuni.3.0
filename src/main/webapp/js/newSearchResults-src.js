function $$D(id){return document.getElementById(id);}
function toggleMenu(id, link) {
  var e = $$D(id);
  var l = $$D(link);
  if (e.style.display == 'block' || e.style.display == '' ) {   
    e.style.display = 'none';
    l.className = 'rdat';
  } else {
    e.style.display = 'block';
    l.className = 'rdat actct';
  }
} 
function trimString(str){
str=this !=window? this : str
return str.replace(/^\s+/g,'').replace(/\s+$/g,'')}
function removeGradesFilter(){
  $$D("hidden_entryLevel").value = "";   
  $$D("hidden_entryPoints").value = "";
  if($$D("hidden_score")){
    $$D("hidden_score").value = "";
  }
  if($$D("hidden_rf") && $$D("hidden_rf").value == "g"){
    $$D("hidden_rf").value = "";
  }
  searchURL = urlFormation();
  $$D("searchBean").action = searchURL.toLowerCase();
   $$D("searchBean").submit();  
}

function clearDefaultText1(thisobj, defvalue){
  if(thisobj.value == defvalue){
    thisobj.value = '';
  }
}
function setDefaultText1(thisobj, defvalue){
 if(thisobj.value == ''){
   thisobj.value = defvalue;
 }
}

function enableButton(id,thisobj,defvalue){
 if(thisobj){
  if(thisobj.value == defvalue || thisobj.value == ''){
   $$D(id).disabled = "disabled";
  }else{
   $$D(id).disabled = "";
  }
 }
}
function hideButton(){
 $jq('#postcodeButton').removeClass('sd_icn');    
}
function showButton(){
  if($jq('#postCodeText').val() == "Postcode"){
    $jq('#postcodeButton').addClass('sd_icn');   
  } 
}
function enableDropDown(){
  $jq('#pstDpDown').removeClass('disa_rem'); 
  document.getElementById('dpPostcode').disabled = false;
}
function loadFirstDelivery(moduleGroupId){
  if($$D("module"+moduleGroupId+"1")){
    $$D("srcEvent").value="no";
    $$D("module"+moduleGroupId+"1").onclick();     
  }
}
function loadFirstDeliveryProvider(moduleGroupId){
  if($$D("module"+moduleGroupId+"0")){
    $$D("srcEvent").value="no";
    $$D("module"+moduleGroupId+"0").onclick();     
  }
}
function hideFn(val){
  if(val=="second"){
   $$D("articleTab").style.display = "block";  
   $$D("first").className = "";  
   $$D("second").className = "act";  
  }else{
   $$D("articleTab").style.display = "none";  
   $$D("second").className = "";  
   $$D("first").className = "act";  
  }
}
function multicheckApplyFn(currentObjId){
  var currentObj = $$D(currentObjId).value;
  var courseType = "";
  if(currentObjId == 'goApplyFlag'){
    ga('set', 'dimension14', "WUGO"); courseType = 'go apply';
  } else {  ga('set', 'dimension14', "");courseType = 'all courses';} 
  searchEventTracking("filter",'course-type',courseType);
  $$D("hidden_applyFlag").value = currentObj;
  searchResultsURL("applyFlag",  $$D("hidden_applyFlag").value); 
}
function multicheckfn(currentObjId){
  var currentObj = $$D(currentObjId);
  var selectedValues = "";
  var locType = "";
  var evntFilterType = "";  
  if($$D("searchtype")){
    if($$D("searchtype").value=="CLEARING_SEARCH"){
      evntFilterType = "clearing-";
    }
  }
  if(currentObj != null && currentObj.checked == true){
    locType = currentObj.id;
    searchEventTracking("filter",evntFilterType+'location-type',locType);
    if( $$D("hidden_locationType").value ==""){
    $$D("hidden_locationType").value = currentObj.id;
    
    } else{
    var val = $$D("hidden_locationType").value;
    val = val + "," + ($$D(currentObj.id).value).toLowerCase();
    $$D("hidden_locationType").value = val;    
    }
  }else  if(currentObj != null && currentObj.checked == false){
       if( $$D("hidden_locationType").value != ""){
        var selArray = $$D("hidden_locationType").value;
        var arr = selArray.split(",");
          for(var i = 0; i <arr.length; i++ ){
              if(arr[i] != currentObj.id){
                  if(selectedValues != ""){
                    selectedValues = selectedValues + "," + arr[i];
                  }else{
                    selectedValues = arr[i];
                  }
              }
          }
      }
     $$D("hidden_locationType").value = selectedValues;   
  }  
 searchResultsURL("locationType",  $$D("hidden_locationType").value); 
 currentObjId.checked = true;
}
//commented the below code on 21 Nov- Jeya for hiding preference pod in SR page
//function multiCheckPreffn(currentObjId){
//  var currentObj = $$D(currentObjId);
//  var selectedValues = "";
//  var prefType = "";
//  var evntFilterType = "";  
//  if($$D("searchtype")){
//    if($$D("searchtype").value=="CLEARING_SEARCH"){
//      evntFilterType = "clearing-";
//    }
//  }
//  if(currentObj != null && currentObj.checked == true){
//    prefType = currentObj.id;
//    if($$D("hidden_preferenceType").value.split(",").length >= 3){
//     //alert("Please remove any one of your selection");
//     currentObj.checked = false;
//     return false;
//    }
//    var prefTypeGALabel =  prefType.replace('-', ' ').toLowerCase();
//    searchEventTracking("filter",evntFilterType+'priorities',prefTypeGALabel);
//    if( $$D("hidden_preferenceType").value ==""){
//    $$D("hidden_preferenceType").value = currentObj.id;
//    
//    } else{
//    var val = $$D("hidden_preferenceType").value;
//    val = val + "," + ($$D(currentObj.id).value).toLowerCase();
//    $$D("hidden_preferenceType").value = val;    
//    }
//  }else  if(currentObj != null && currentObj.checked == false){
//       if( $$D("hidden_preferenceType").value != ""){
//        var selArray = $$D("hidden_preferenceType").value;
//        var arr = selArray.split(",");
//          for(var i = 0; i <arr.length; i++ ){
//              if(arr[i] != currentObj.id){
//                  if(selectedValues != ""){
//                    selectedValues = selectedValues + "," + arr[i];
//                  }else{
//                    selectedValues = arr[i];
//                  }
//              }
//          }
//      }
//     $$D("hidden_preferenceType").value = selectedValues;   
//  }  
// searchResultsURL("pref-type",  $$D("hidden_preferenceType").value); 
//}


//Modified the URL pattern for both SR and PR page Jan-27-16 Indumathi.S
function searchResultsURL(filterId,value){
  var searchURL = "";
  var filterText = "";
  var filterString = "";
  var recentFilter = "";
  var empMaxi = $$D("hidden_empRateMax") ? $$D("hidden_empRateMax").value : "";
  var empMini = $$D("hidden_empRateMin") ? $$D("hidden_empRateMin").value : "";    
  var ucasMaxi = $$D("hidden_ucasMax") ? $$D("hidden_ucasMax").value : "";
  var ucasMini = $$D("hidden_ucasMin") ? $$D("hidden_ucasMin").value : "";   
  var dist = $$D("hidden_distance") ? $$D("hidden_distance").value : "";  
  var evntFilterType = "";  
  if($$D("searchtype")){
    if($$D("searchtype").value=="CLEARING_SEARCH"){
      evntFilterType = "clearing-";
    }
  }
  //var modDefaultText = $$D("hidden_modDefaultText").value;
  var modDefaultText = "e.g. Module name";  
  if((empMaxi == "" || empMaxi == "100"  ) &&  (empMini == "0" || empMini == "" )){
    $$D("hidden_empRateMax").value = "";
    $$D("hidden_empRateMin").value = "";
  }
  if((ucasMaxi == "" || ucasMaxi == "480"  ) &&  (ucasMini == "0" || ucasMini == "" )){
    if($$D("hidden_ucasMax")){ $$D("hidden_ucasMax").value = "";}
    if($$D("hidden_ucasMin")){$$D("hidden_ucasMin").value = "";}
  }
  if(filterId == 'modules'){
      filterText = $$D("moduleText").value;
      if(filterText == "" || filterText == modDefaultText ){
        alert("Please enter module text");
        return false;
      }else{      
        $$D("hidden_module").value  = trimString(replaceSpecialCharacter($$D("moduleText").value));        
        clearRefineFilters('m');
        searchEventTracking('SR-search',evntFilterType+'module',$$D("hidden_module").value);
      }
      $$('hidden_rf').value = "M";
  }else if(filterId == 'ucas'){
     var ucasMax = $$D("hidden_ucasMax").value;
     var ucasMin = $$D("hidden_ucasMin").value;      
     if((ucasMax == "" || ucasMax == "480"  ) &&  (ucasMin == "0" || ucasMin == "" )){
        alert("Please choose a range");
        return false;
      }else{
        $$D("hidden_ucasMax").value  = trimString(replaceSpecialCharacter($$D("hidden_ucasMax").value));    
        $$D("hidden_ucasMin").value  = trimString(replaceSpecialCharacter($$D("hidden_ucasMin").value));  
        $$D("hidden_ucasTarrif_max").value = $$D("hidden_ucasMax").value;
        $$D("hidden_ucasTarrif_min").value = $$D("hidden_ucasMin").value;
        clearRefineFilters('u');
        searchEventTracking('filter',evntFilterType+'ucas-slider',$$D("hidden_ucasMin").value+"-"+$$D("hidden_ucasMax").value);
      }
      $$('hidden_rf').value = "";
  }else if(filterId == 'empRate'){  
     var empMax = $$D("hidden_empRateMax").value;
     var empMin = $$D("hidden_empRateMin").value;         
        $$D("hidden_empRateMax").value  = trimString(replaceSpecialCharacter($$D("hidden_empRateMax").value));    
        $$D("hidden_empRateMin").value  = trimString(replaceSpecialCharacter($$D("hidden_empRateMin").value));
        clearRefineFilters('e');
        if(empMin != "0" && empMin != ""){
          searchEventTracking('filter',evntFilterType+'emprate-slider',$$D("hidden_empRateMin").value+"-"+$$D("hidden_empRateMax").value);
        }      
      $$('hidden_rf').value = "";
  }else if(filterId == 'postcode'){
    var postcode = $$D("postCodeText").value;
    if(postcode !=null && postcode != "" && postcode != "Postcode"){
      $$D("hidden_postCode").value = postcode;
      if($$D("hidden_distance").value!=null && $$D("hidden_distance").value != ""){
         $$D("hidden_dist").value = $$D("hidden_distance").value;         
         clearRefineFilters('p');
         searchEventTracking('filter',evntFilterType+'postcode',$$D("hidden_dist").value);
      }else{
           alert("Please choose distance");
           return false;
      }
      if($$D('hidden_location')){
        $$D('hidden_location').value = "";
      }
      $$('hidden_rf').value = "P";
    }else if($$D("hidden_postCode").value != "Postcode"){
        alert("Please enter postcode");
        return false;
    }
  }else if(filterId == 'locationType'){
    $$D("hidden_locType").value = $$D("hidden_locationType").value;
    $$D("hidden_locationType").checked = true;
    clearRefineFilters('l');
  }else if(filterId == 'pref-type'){
    $$D("hidden_prefType").value = $$D("hidden_preferenceType").value;
    clearRefineFilters('l');
  }else if(filterId == 'applyFlag'){
     $$D("hidden_appFlag").value = $$D("hidden_applyFlag").value;
  }
 searchURL = urlFormation(); 
 $$D("searchBean").action = searchURL.toLowerCase();
 $$D("searchBean").submit();
 return false;
}
function clearRefineFilters(fromFlag){
  if($$D("hidden_resultExists") && $$D("hidden_resultExists").value == "N" && $$D("hidden_rf").value == "p" && fromFlag!='p'){
    $$D("hidden_distance").value="";
    $$D("hidden_dist").value="";
    $$D("hidden_rf").value = "";
  }else if($$D("hidden_resultExists") && $$D("hidden_resultExists").value == "N" && $$D("hidden_rf").value == "m" && fromFlag!='m'){
    $$D("hidden_module").value = "";
    $$D("hidden_rf").value = "";
  }else if($$D("hidden_resultExists") && $$D("hidden_resultExists").value == "N" && $$D("hidden_rf").value == "g" && fromFlag!='g'){
    $$('hidden_entryLevel').value = "";
    $$('hidden_entryPoints').value = "";    
    $$D("hidden_rf").value = "";
  }  
}
function checkNull(id){
 if($$D(id)){
  var inString = $$D(id).value;
 if (inString.length != 0 && inString != 'null' && inString != 'undefined' && inString != 'NaN') {
  return true;
 }
 }
 return false;
}
function urlFormation(pageFlag){
 var splitedURL = $$D("sortingUrl").value; 
 var searchUrl = splitedURL.split("?");
 splitedURL = searchUrl[0];
 var filterString = splitedURL;
 if(pageFlag == 'gradefilter'){	 
   filterString = "";   
 }
 filterString += checkNull("isclearingPage") ?  "?clearing" : ""; 
 if(checkNull("hidden_q") && !checkNull("isclearingPage")){
  filterString += "?q="+$$D("hidden_q").value;  
 }else if(checkNull("hidden_q") && checkNull("isclearingPage")){
  filterString += "&q="+$$D("hidden_q").value;
 }else if(checkNull("hidden_subject") && !checkNull("isclearingPage")){
  filterString += "?subject="+$$D("hidden_subject").value; 
 }else if(checkNull("hidden_subject") && checkNull("isclearingPage")){
  filterString += "&subject="+$$D("hidden_subject").value; 
 }else if(checkNull("hidden_ucasCode") && !checkNull("isclearingPage")){
  filterString += "?ucas-code="+$$D("hidden_ucasCode").value; 
 }else if(checkNull("hidden_ucasCode") && checkNull("isclearingPage")){
  filterString += "&ucas-code="+$$D("hidden_ucasCode").value; 
 }else if(checkNull("hidden_jacs") && !checkNull("isclearingPage")){
  filterString += "?jacs="+$$D("hidden_jacs").value; 
 }else if(checkNull("hidden_jacs") && checkNull("isclearingPage")){
  filterString += "&jacs="+$$D("hidden_jacs").value; 
 }
 if(!checkNull("isclearingPage") && !(checkNull("hidden_q") || checkNull("hidden_subject") || checkNull("hidden_jacs") || checkNull("hidden_ucasCode"))) {
   filterString += checkNull("hidden_university") ? "?university="+$$D("hidden_university").value : "";
 } else {
   filterString += checkNull("hidden_university") ? "&university="+$$D("hidden_university").value : "";
 }  
 filterString += checkNull("hidden_module") ? "&module="+ replaceHypen(replaceURL($$D("hidden_module").value)).toLowerCase() : "";  
 filterString += checkNull("hidden_locationType") ? "&location-type="+$$D("hidden_locationType").value : "";
 filterString += checkNull("hidden_location") ? "&location="+$$D("hidden_location").value : ""; 
 filterString += checkNull("hidden_postCode") ? "&postcode="+$$D("hidden_postCode").value : "";
 if(checkNull("hidden_dist") && checkNull("hidden_postCode")){
   filterString += "&distance="+$$D("hidden_dist").value;
 } 
 filterString += checkNull("hidden_campusType") ? "&campus-type="+$$D("hidden_campusType").value : "";
 filterString += checkNull("hidden_studyMode") ? "&study-mode="+$$D("hidden_studyMode").value : "";
 var empMaxi = $$D("hidden_empRateMax").value;
 var empMini = $$D("hidden_empRateMin").value;
 if((empMaxi == "" || empMaxi == "100"  ) &&  (empMini == "0" || empMini == "")){
   $$D("hidden_empRateMax").value = "";
   $$D("hidden_empRateMin").value = "";
 }
 filterString += checkNull("hidden_empRateMax") ? "&employment-rate-max="+$$D("hidden_empRateMax").value : "";
 filterString += checkNull("hidden_empRateMin") ? "&employment-rate-min="+$$D("hidden_empRateMin").value : "";
 filterString += checkNull("hidden_ucasTarrif_max") ? "&ucas-points-max="+$$D("hidden_ucasTarrif_max").value : "";
 filterString += checkNull("hidden_ucasTarrif_min") ? "&ucas-points-min="+$$D("hidden_ucasTarrif_min").value : "";
 filterString += checkNull("hidden_russell") ? "&russell-group="+$$D("hidden_russell").value : ""; 
 filterString += checkNull("hidden_entryLevel") ? "&entry-level="+$$D("hidden_entryLevel").value : "";
 filterString += checkNull("hidden_entryPoints") ? "&entry-points="+$$D("hidden_entryPoints").value : ""; 
 if(checkNull("hidden_rf") && $$D("hidden_rf").value != "n"){
   filterString += checkNull("hidden_rf") ? "&rf="+$$D("hidden_rf").value : "";
 }
 filterString += checkNull("hidden_sortorder") ? "&sort="+$$D("hidden_sortorder").value : "";
 filterString += checkNull("hidden_russell") ? "&russell-group="+$$D("hidden_russell").value : ""; 
 filterString += checkNull("hidden_assessed") ? "&assessment-type="+$$D("hidden_assessed").value : ""; 
 filterString += checkNull("hidden_preferenceType") ? "&your-pref="+$$D("hidden_preferenceType").value : "";   
 if(($$D('hidden_applyFlag') && $$D('hidden_applyFlag').value == "y")) {
   filterString += "&applyflag=Y";
 }
 filterString += checkNull("hidden_score") ? "&score="+$$D("hidden_score").value : "";
 searchURL = filterString; 
 return searchURL;
}
function checkNullAndUndefined(val){
  if(val!='' && val!='undefined')return true;
  return false;
}
function providerResultsURL(filterId,value){
  var searchURL = "";
  var evntFilterType = "";  
  var pageUrl =  $$D("sortingUrl").value;
  var pageName = returnPRPageName(pageUrl);
  if(pageName=="/clcsearch.html"){
    evntFilterType = "clearing-";    
  }
  var modDefaultText = "e.g. Module name";
  var filterText = "";
 var empMaxi = $$D("hidden_empRateMax").value;
  var empMini = $$D("hidden_empRateMin").value;    
  var ucasMaxi = $$D("hidden_ucasTarrif_max").value;
  var ucasMini = $$D("hidden_ucasTarrif_min").value;   
  var dist = $$D("hidden_dist").value;  
  var sort = $$D("hidden_sortorder").value;  
  
  if(dist == "" || dist == "10" ){
    $$D("hidden_dist").value = "";
  }
  if((empMaxi == "" || empMaxi == "100"  ) &&  (empMini == "0" || empMini == "" )){
    $$D("hidden_empRateMax").value = "";
    $$D("hidden_empRateMin").value = "";
  }
  if((ucasMaxi == "" || ucasMaxi == "480"  ) &&  (ucasMini == "0" || ucasMini == "" )){
    $$D("hidden_ucasTarrif_max").value = "";
    $$D("hidden_ucasTarrif_min").value = "";
  }
  if(filterId == 'modules'){
      filterText = $$D("moduleText").value;
      if(filterText == "" || filterText == modDefaultText ){
        alert("Please enter module text");
        return false;
      }else{
        $$D("hidden_module").value  = trimString(replaceSpecialCharacter($$D("moduleText").value));    
        searchEventTracking('PR-search',evntFilterType+'module',$$D("hidden_module").value);
      }
  } 
 searchURL = urlFormation();
 $$D("searchBean").action = searchURL.toLowerCase();
	$$D("searchBean").submit();
	return false;
}

function defaultSelectCheckBox(id){
 var filterType = $$D(id) ? $$D(id).value : "";
 if(filterType!=""){
   var newArr = filterType.split(",");
   for(var i = 0; i < newArr.length; i++ ){
     if($$D(newArr[i])){
       $$D(newArr[i]).checked = true;
     }
   }   
 }  
}
function defaultApplyCheckBox(id){
  var filterType = $$D(id) ? $$D(id).value : "";
  if(filterType!=""){
    var newArr = filterType.toLowerCase();
    if(newArr == "y") {
      opdub('#goApplyFlag').attr('checked',true);
    } else {
      opdub('#allCourseFlag').attr('checked',true);
    }
  } else if(filterType ==""){  
     opdub('#allCourseFlag').attr('checked',true);
  }
}
function uniSearchInResults1(o,indexOfUrl){
	var	uniId = trimString($$D((o.id+"_hidden")).value);
	var	uniNameUrl = trimString($$D((o.id+"_url")).value);
	var empUni = "We don't have a record of that university! Make sure you've got the name spelled right (you should be able to select from one of the options that appear as you start typing)";
	if(subUni != uniNameUrl || subUni1 != uniNameUrl){
		if(uniNameUrl == '' || uniId == ''){
			alert(empUni);
			resetUniNAME(o);
			return false;
		}
	}else{
		alert(empUni);
		resetUniNAME(o);
		return false;
	}
  var contextPath=$$D("contextPath").value;
  var beforeCollege = "";
  var afterCollege = "";
  var final_url = "";
  var searchUrl = $$D("sortingUrl").value;
  var dataArray = searchUrl.split("/");
  var urlLength = dataArray.length;
  var searchtype = $$D("searchtype").value;
  var tailURL = "";
  var evntFilterType = "";
   tailURL = "page.html";
  if(searchtype == 'NORMAL_SEARCH'){  
      tailURL = "page.html";
  }else{
      evntFilterType = "clearing-";
  }
  var seo_qual=''
	 var qual=$$D("paramStudyLevelId").value;
      switch(qual){
              case 'M': seo_qual='degree';break
              case 'N': seo_qual='hnd-hnc';break
              case 'A': seo_qual='foundation-degree';break
              case 'T': seo_qual='access-foundation';break
              case 'L': seo_qual='postgraduate';break
      }
      var url_1=seo_qual+'-courses';
      var url_2="csearch";
      var url_herb = "herb";
      var qeyStr=searchUrl.split("?")[1];
      final_url="/"+url_1+"/"+url_2+"?"+qeyStr+"&university="+uniNameUrl;  
      if(searchUrl.indexOf("/herb/") != -1){
        final_url="/"+url_1+"/"+url_herb+"/"+url_2+"?"+qeyStr+"&university="+uniNameUrl;  
      }
      
 var provDispName = $$D("uniName_display").value.toLowerCase();
 searchEventTracking('SR-search',evntFilterType+'university',provDispName); 
	$$D("searchBean").action = final_url.toLowerCase();
	$$D("searchBean").submit();
	return false;
}

function checkPostCode (postcode) {
  var regPostcode = /^([a-zA-Z]){1}([0-9][0-9]|[0-9]|[a-zA-Z][0-9][a-zA-Z]|[a-zA-Z][0-9][0-9]|[a-zA-Z][0-9]){1}([ ])([0-9][a-zA-z][a-zA-z]){1}$/;
  if(regPostcode.test(postcode) == false){
    return false;
  }else{
    return true;
  }
} 
//Added by Indumathi.S Jan-27-16 Rel URL restructuring
function deleteSearchFilterParams(filterType, filterValue){
  if(filterType == 'empRate' ){
      $$D("hidden_empRateMax").value = "";
      $$D("hidden_empRateMin").value = "";
   }else if(filterType == 'ucasTariff' ){
      $$D("hidden_ucasTarrif_max").value = "";
      $$D("hidden_ucasTarrif_min").value = "";
   }else if(filterType == 'postCode'){
     $$D("hidden_postCode").value  = "";
     $$D("hidden_dist").value = "";
  }else if(filterType == 'subject' || filterType == 'locationType' || filterType == 'preferenceType'){
    $$D("hidden_"+filterType).value = filterValue;
  } 
   else {
    $$D("hidden_"+filterType).value = "";
     }
   var searchURL = urlFormation();
  $$D("searchBean").action = searchURL.toLowerCase();
	$$D("searchBean").submit();  
}

function deleteProviderStudyLevelFilter(){
  var slash = "/";
  var newUrl = "";
  newUrl = slash + "all-courses" + slash  + "csearch"; 
  if($$D('sortingUrl')){
    var herbUrl = $$D('sortingUrl').value;
    if(herbUrl.indexOf('/herb/') >= 0){
      newUrl = slash + "all-courses" + slash + "herb" + slash + "csearch"; 
    }
  }
  var queryStringValue = $$D("queryStringVal").value;
  if(queryStringValue != "" ){
    newUrl = newUrl + "?" + queryStringValue;      
  }
  $$D("searchBean").action = newUrl.toLowerCase();
  $$D("searchBean").submit();  
}

function deleteProviderUniParams(){
  var pageUrl =  $$D("sortingUrl").value;
  var dataarray = pageUrl.split("/");
  var queryStringValue = $$D("queryStringVal").value;
  var herbFlag = "N";
  if(pageUrl.indexOf('/herb/') != -1){herbFlag = "Y";}
  var newUrl = "";
  if(queryStringValue.indexOf('subject') == -1 && queryStringValue.indexOf('jacs') == -1 && queryStringValue.indexOf('ucas-code') == -1) {
    newUrl = contextPath + "/courses/";
  } else {
    newUrl = "/" + dataarray[1] + "/" + "search";
    if(herbFlag == "Y"){
      newUrl = "/" + dataarray[1] + "/" + "herb" + "/" + "search";
    }
    var param = "";
    if(queryStringValue != "" ){
      var queryArrayValue = queryStringValue.split("&");
      for(var i=0; i<queryArrayValue.length; i++){
        if((queryArrayValue[i].indexOf('university') > -1) || (queryArrayValue[i].indexOf('pageno') > -1) || ((queryArrayValue[i].indexOf('sort') > -1) && (queryArrayValue[i] == "sort=ta" || queryArrayValue[i] == "sort=td"))){
         queryArrayValue[i] = "";
        } 
      }
      queryArrayValue = jQuery.grep(queryArrayValue, function(n){ return (n); });
      for(var i=0; i<queryArrayValue.length; i++){
        param += queryArrayValue[i];
        if(i+1 != queryArrayValue.length){ param += "&"; }
      }    
      newUrl += (param != "" ) ? "?" + param : ""; 
    }
  }
  $$D("searchBean").action = newUrl.toLowerCase();
  $$D("searchBean").submit(); 
}

function deleteProviderFilterParams(filterType, filterValue){
  if(filterType == 'empRate' ){
    $$D("hidden_empRateMax").value = "";
    $$D("hidden_empRateMin").value = "";
  }else if(filterType == 'ucasTariff' ){
    $$D("hidden_ucasTarrif_max").value = "";
    $$D("hidden_ucasTarrif_min").value = "";
  }else if(filterType == 'locationType' ){
    $$D("hidden_locationType").value = filterValue;
  }else if(filterType == 'postCode'){
    $$D("hidden_postCode").value = "";
    $$D("hidden_dist").value = "";
  }else if(filterType == 'studyMode'){    
    $$D("hidden_studyMode").value = "";
  }else if(filterType == 'subject'){    
    $$D("hidden_subject").value = "";
    $$D("hidden_ucasCode").value = "";
  }else {
    $$D("hidden_" + filterType).value = "";  
  }
  var searchURL = urlFormation();
  $$D("searchBean").action = searchURL.toLowerCase();
  $$D("searchBean").submit();  
}
//End Indumathi.S Jan-27-16
function showFilterBlock(){
 var blocked = $$D("filterDiv").value;
 if(blocked == "Y"){
   $$D("filterblock").style.display = 'block';
   $$D("Filter").style.display = 'block';
   $$D("filterTop").style.display = 'block';
 }
}

function articleShowHide(id, link ,index,postId) {
 var e = $$D(id);
 if (e.style.display == '' || e.style.display == 'none')  {       
  if($$D("articleDesc"+index).innerHTML == ""){
    var ajax = new sack();
    url = contextPath + "/loadCategoryArticle.html?postId="+postId;
    ajax.requestFile = url;
    ajax.onCompletion = function(){ loadCategoryArticleDate(ajax,index,id,link); };
    ajax.runAJAX();
  }else{    
    e.style.display = '';
    link.className = 'addcou1 act';
    link.innerHTML = "View less <em></em>"    
    $$D('articleDesc'+index).style.display='block';
    $$D('articleroot'+index).className='sart2';
    $$D('articleDiv'+index).style.display='none';
  }  
 }else {
  e.style.display = 'none';
  link.className = 'addcou1';
  link.innerHTML = "View more <em></em>"
  $$D('articleDesc'+index).style.display='none';
  $$D('articleroot'+index).className='sart1';
  $$D('articleDiv'+index).style.display='block';
 }
} 

function loadCategoryArticleDate(ajax, index,id,link){
 var e = $$D(id);
 e.style.display = '';
 link.className = 'addcou1 act';
 link.innerHTML = "View less <em></em>"   
 var response = ajax.response;
 $$D('articleDesc'+index).innerHTML = response;
 $$D('articleDesc'+index).style.display='block';
 $$D('articleroot'+index).className='sart2';
 $$D('articleDiv'+index).style.display='none';
}

function loadfirstArticle(){
 if($$D("articleLink0")){
  $$D("articleLink0").onclick();     
 }
}
 
function autoCompleteUniForSearchResults(e,o){
 ajax_showOptions(o,'z',e,"navindicator",'z=','0','382');
}

function defualtOpenFilter(url){
 var qString = url;
 if(qString.indexOf("&russell-group") != -1){
  if( $$D("russell")){
   $$D("russell").style.display = 'block';
   $$D("russellh1").className = 'rdat actct';
  }
 }
 if(qString.indexOf("&postcode") != -1){
  if($$D("postcode")){
   $$D("postcode").style.display = 'block';
   $$D("postcodeh1").className = 'rdat actct';
  }
 }
 if(qString.indexOf("&employment-rate-max") != -1){
  if( $$D("empRate")){
   $$D("empRate").style.display = 'block';
   $$D("empRateh1").className = 'rdat actct';
  }
 }
 if(qString.indexOf("&campus-type") != -1){
  if( $$D("campus")){
   $$D("campus").style.display = 'block';
   $$D("campush1").className = 'rdat actct';
  }
 }
 if(qString.indexOf("&study-mode") != -1){
  if( $$D("campus")){
   $$D("studyMode").style.display = 'block';
   $$D("studyModeh1").className = 'rdat actct';
  }
 }
 if(qString.indexOf("&location-type") != -1){
  if( $$D("locationType")){
   $$D("locationType").style.display = 'block';
   $$D("locationhTypeh1").className = 'rdat actct';
  }
 }
 if(qString.indexOf("&ucas-points-max") != -1){
  if( $$D("UCASTarif")){
   $$D("UCASTarif").style.display = 'block';
   $$D("UCASTarifh1").className = 'rdat actct';
  }
 }
 
 if(qString.indexOf("&assessment-type") != -1){
  if( $$D("assessment")){
   $$D("assessment").style.display = 'block';
   $$D("assessmenth1").className = 'rdat actct';
  }
 }
 if(qString.indexOf("&study-mode") != -1){
   if( $$D("studyMode")){
   $$D("studyMode").style.display = 'block';
   $$D("studyModeh1").className = 'rdat actct';
  }
 }
 
 
 if(qString.indexOf("&your-pref") != -1){
  if( $$D("reviewCategory")){
   $$D("reviewCategory").style.display = 'block';
   $$D("reviewCategoryh1").className = 'rdat actct';
  }
 }
}
 
function addco(id, link, val) {   
 var evntFilterType = "";  
 if($$D("searchtype")){
  if($$D("searchtype").value=="CLEARING_SEARCH"){
   evntFilterType = "clearing-";
  }
 }
 var e = $$D(id);
 if (e.style.display == '') {
  e.style.display = 'none';
  link.className = 'addcou';
  link.innerHTML = "View " + val + " additional courses available <em></em>";
  searchEventTracking('show/hide',evntFilterType+'additional-courses','less');
 } else {
  e.style.display = '';
  link.className = 'addcou act';
  link.innerHTML = "View fewer courses <em></em>";
  searchEventTracking('show/hide',evntFilterType+'additional-courses','more');
 }
} 

function loadmoduledata(index,moduleGroupId,url){
 var evntFilterType = "";  
 if($$D("searchtype")){
  if($$D("searchtype").value=="CLEARING_SEARCH"){
   evntFilterType = "clearing-";
  }
 }
 if($$D("module"+moduleGroupId+index).className == "sh pls"){
  $$D("module"+moduleGroupId+index).className = "sh mns";
  $$D("module_data_"+moduleGroupId+index).style.display = "block";      
  if($$D("srcEvent")) {        
   if($$D("srcEvent").value!="no") {
    searchEventTracking('show/hide',evntFilterType+'modules','more');         
   }
   $$D("srcEvent").value="";
  }       
 } else{
  $$D("module"+moduleGroupId+index).className = "sh pls";
  $$D("module_data_"+moduleGroupId+index).style.display = "none";
  searchEventTracking('show/hide',evntFilterType+'modules','less');
 }
} 

function loadModuleData(ajax,moduleGroupId,index){
 var response = ajax.response;
 $$D("module"+moduleGroupId+index).className = "sh mns";
 $$D("module_data_"+moduleGroupId+index).style.display = "block";
 $$D("module_data_"+moduleGroupId+index).innerHTML = response;  
}

function ProviderdefaultOpenFilter(studyLevel){
 if(studyLevel!=""){
  $$D("qualList").style.display = 'block';
  $$D("qualListh1").className = 'rdat actct';
 }  
}

function replaceAllSpecialCharacter(inString){
 var outString=inString
 var temp=""
 if(outString !=null){
  var data=outString
  for(var i=0;i<data.length;i++){
   var character=data.charCodeAt(i)
   if(!((character>=48&&character<=57)||(character>=65&&character<=90)||(character>=97&&character<=122))){
    temp=temp+" "
   }else{temp=temp+data.substring(i,i+1)}}
  outString=temp
 }
 return outString
}
function defaultenablemodulebutton(modTitle){
  var defaultModuleTitle = modTitle;
  if(defaultModuleTitle!= null && defaultModuleTitle!="" &&  defaultModuleTitle!= "null"){
  enableButton('moduleButton',this, '<%=moduleDefaultText%>')
  }
}
function returnPageName(pageUrl){
  return "/page.html";
}
function returnPRPageName(pageUrl){
 return "/csearch.html";
}
function loadSlider(empRateMin,empRateMax){  
  var pstOptions= $$D('dpEmprate').options; 
  var optionLabel = "";
  for (var j= 0; j< pstOptions.length; j++) {  
    if (pstOptions[j].value == empRateMin) {
      pstOptions[j].selected= true;
      optionLabel = pstOptions[j].text;
      break;
    }else{
      optionLabel = "Choose % rate";
    }
  }
  $$D("divEmprate").innerHTML = optionLabel;
  //$$D('empTxt').value = (empRateMin != "") ? empRateMin + "% and above" : "Choose emp rate";  
  if($$D("hidden_empRateMin")){$$D("hidden_empRateMin").value = empRateMin;}
  if($$D("hidden_empRateMax")){$$D("hidden_empRateMax").value = empRateMax;}
}	
function loadPostcodeSlider(distance){
  //$$D('pstCodeTxt').value = distance + " miles";
 var pstOptions= $$D('dpPostcode').options; 
  var optionLabel = "";
  for (var j= 0; j< pstOptions.length; j++) {  
    if (pstOptions[j].value == distance) {
      pstOptions[j].selected= true;
      optionLabel = pstOptions[j].text;
      break;
    }else{
      optionLabel = "10 miles";
    }
  }
  $$D("divPostcode").innerHTML = optionLabel;
	}

//Modified script function for grade filter popup show and hide in responsive 03_Nov_2015 By Indumathi.
var $jq = jQuery.noConflict();
function loadGradePopup(){
  var gradePopup=null;  
  if (gradePopup==null && $$D("userid").value == 0 && $$D("paramStudyLevelId").value == "M"){
    var screenWidth = document.documentElement.clientWidth;
    if(screenWidth <= 992){ // Code added for removing the shadow in mobile and tablet 03_NOV_2015
      //$jq('.sr_resp').removeClass('nobg');
      $jq('.clipart,.sr-cont,.bc,.com,#footer-bg,.mbc').removeClass('hd_pos');
      $jq('body.md #sky').css({'z-index':'0'});   
      $jq('.sticky_foo').css({'z-index':'100000'});
      $jq('.gr_filt_cont').css({'display':'none'});
    }else{
      //$jq('.sr_resp').addClass('nobg');
      /*$jq('.clipart,.sr-cont,.bc,.com,#footer-bg,.mbc').addClass('hd_pos');
      $jq("#hi_img").css("display", "block");
      $jq('body.md #sky').css({'z-index':'-1'});
      $jq('.sticky_foo').css({'z-index':'-1'});
      $jq('.gr_filt_cont').css({'display':'block'});*/
      
      /*$jq("#hi_img").css("display", "none");
      $jq('.clipart,.sr-cont,.bc,.com,#footer-bg').removeClass('hd_pos');
      $jq('.mbc ').removeClass('hd_pos');      
      $jq('.gr_filt_cont').css({'display':'none'});
      $jq('body.md #sky').css({'z-index':'0'}); 
      $jq('.sticky_foo').css({'z-index':'100000'});*/
      
    }
  }else if(gradePopup=="NO" || $$D("userid").value != 0){    
    //$jq('.sr_resp').removeClass('nobg');
    $jq('.clipart,.sr-cont,.bc,.com,#footer-bg').removeClass('hd_pos');  
    $jq('.mbc ').removeClass('hd_pos');
    $jq('.gr_filt_cont').css({'display':'none'});
    $jq('body.md #sky').css({'z-index':'0'});   
    $jq('.sticky_foo').css({'z-index':'100000'});
  }
}
function removeGradePopup(){
  /*var IEversion = detectIE();
  if (IEversion !== false) {
    setCookie('gradePopup','NO',1,'wucookiepolicy')
  } else {    
    setSessionCookie('gradePopup','NO','wucookiepolicy');
  }*/
  $jq("#hi_img").css("display", "none");
  $jq('.clipart,.sr-cont,.bc,.com,#footer-bg').removeClass('hd_pos');
  $jq('.mbc ').removeClass('hd_pos');  
  //$jq('.gr_filt_cont').css({'display':'none'});
  $jq('body.md #sky').css({'z-index':'0'}); 
  $jq('.sticky_foo').css({'z-index':'100000'});
  
  $jq(".sr_resp").removeClass("nobg");
  $jq(".sr-cont").removeClass("hd_pos");
  $jq("header").removeClass("hd_pos");
}

//Added by Indumathi.S for responsive Nov-03-15 Rel
$jq(document).ready(function(){
  $jq("#mob_sort_by").click(function(){
     if($jq("#abcr1").is(':visible')){
       $jq("#abcr1").hide();
     } else {
       $jq("#abcr1").show();
     }
     if($jq("#abcr11").is(':visible')){
       $jq("#abcr11").hide();
     } else {
       $jq("#abcr11").show();
     }
  });
  $jq(".fl_lr").click(function(){
    $jq(".sr_resp").toggleClass("sr_resp_mov");
    $jq(".fltr").toggleClass("fltr_hide");
  });
  $jq(".filt_cls").click(function(){
    $jq(".sr_resp").removeClass("sr_resp_mov");
    $jq(".fltr").removeClass("fltr_hide");
  });
  if(document.documentElement.clientWidth <= 767){
    $jq(".mstinf, #stdrank").hide();
  }
  $jq(".mch_on .mch_opn").click(function(event){
    $jq(".mch_opn").siblings().removeClass("act");
    $jq(this).toggleClass("act");
    event.preventDefault();
  });
  if($jq("#matchId_0 li").length <= 5){
    $jq('.mch_opn em').css("display", "none");
  }
  dev(".sr_adv .mch_scr a").click(function(){
    dev("#mch_"+$jq(this).attr("id")).show();
  });
  
  dev(".sr_adv .mch_scr a").mouseover(function(){
    dev("#mch_"+$jq(this).attr("id")).show();
    if(document.documentElement.clientWidth >= 1024){
    lazyloadetStarts();
    dev("#dskTooltip_"+$jq(this).attr("id")).show();
    }
  });
  
  dev(".sr_adv .mch_scr a").mouseout(function(){
    if(document.documentElement.clientWidth >= 1024){
     dev("#dskTooltip_"+$jq(this).attr("id")).hide();
    }
  });
  
  
  dev(".mch_cls a").click(function(){
  //dev("#mch_"+$jq(this).attr("id")).hide();
    dev(".mch_on").hide();
  });
  
  //Added by Minu, filter results btn GA logging for 10-Nov-2020 Rel 
  $jq("#filterResults").click(function(){
	var pageName = $jq("#filterResults").attr('data-pagename') != null ? (jq("#filterResults").attr('data-pagename')).toLowerCase() : "";
	GANewAnalyticsEventsLogging('Mobile Filter', 'Start', pageName, '')
    $jq(".sr_resp").toggleClass("sr_resp_mov");
	$jq(".fltr").toggleClass("fltr_hide");
  });
  //Added by Minu, enter grade tool tip hide show for 10-Nov-2020 Rel
  var userUcasPoint = jq("#userUcasPoint").val();
  if(userUcasPoint != null && userUcasPoint == ""){
  	jq("#srgradeOptionToolTip").show();
  }
  
  jq("#srcloseGradeOption").click(function(){
      jq("#srgradeOptionToolTip").hide();
      return false;
  });
  
  //Added by Minu, click function of enter grade for 10-Nov-2020 Rel
  $jq("#srgradeOption").click(function(){
    var qualificationExistFlag = $jq("#userQualificationExistFlag").val();
    var paramStudyLevelId = $jq("#paramStudyLevelId").val();
	if("Y" == qualificationExistFlag && "" != paramStudyLevelId && "L" != paramStudyLevelId){
      openLightBox('gradefilter', 'EDIT_QUAL');  
    } else {
      openLightBox('gradefilter');
    }
  });
 });
 
//Method to show the grade filter popup for responsive view ajax call Added by Prabha on wu_546 03-NOV_2015
function showPopupGradeFilter(form_name, pwidth, pheight, paramStudyLevelId, urlString){
  var url=""
  sm('newvenue', pwidth, pheight, 'gradefilterresp');
  url=contextPath + "/mobile-grade-popup.html?studyLevelId="+paramStudyLevelId+"&urlString="+urlString;
  blockNone("ajax-div", "block");
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){ displayFormData(ajax); };
  ajax.runAJAX();
 }
//Added by Indumathi.S for hidding the ajax dropdown on scroll
$jq(document).ready(function() {
   $jq(".iselect").on("click",function() {
    $jq("#dynamicDiv").addClass("gf_pos");  
    setTimeout(function(){ 
      if($jq(this).find('option:selected').val() != "0"){
        $jq("#dynamicDiv").removeClass("gf_pos");
      }
    }, 3000);
   });  
  var screenWidth = document.documentElement.clientWidth;
  $jq('#filter').on('scroll',function() {
    if (screenWidth <= 992) {
      blockNone("ajax_listOfOptions","none");  
    }    
  });
  $jq(window).scroll(function() {
    if (screenWidth <= 992) {
      //blockNone("ajax_listOfOptions","none");      
    }
    if(screenWidth <= 1024){    
      $jq(".pys").click(function(){
        $jq(".sr_resp").removeClass("nobg");
        $jq(".sr-cont").removeClass("hd_pos");
        $jq("header").removeClass("hd_pos");
        $jq("#dynamicDiv").removeClass("gf_pos");
      });
    }
  });
  if(screenWidth > 1024){
    $jq(".pys, .gf_pos, .iselect").mouseenter(function(){
      $jq(".sr_resp").addClass("nobg");
      $jq(".sr-cont").addClass("hd_pos");
      $jq("header").addClass("hd_pos");
    });   
    $jq(".iselect").mouseenter(function(){
      $jq(".sr_resp").addClass("nobg");
      $jq(".sr-cont").addClass("hd_pos");
      $jq("header").addClass("hd_pos");
    });
    $jq(".pys").mouseleave(function(){
      $jq(".sr_resp").removeClass("nobg");
      $jq(".sr-cont").removeClass("hd_pos");
      $jq("header").removeClass("hd_pos");
      $jq("#dynamicDiv").removeClass("gf_pos");
    });  
  }
  if(screenWidth <= 1024){
    $jq(".sr_resp").removeClass("nobg");
    $jq(".sr-cont").removeClass("hd_pos");
    $jq("header").removeClass("hd_pos");
    $jq("#dynamicDiv").removeClass("gf_pos");
  }
});
function setSelectedValue(obj, spanid, hidId){  
  var dobid = obj.id
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  var dpVal = el.options[el.selectedIndex].value;
  document.getElementById(spanid).innerHTML = text;
  document.getElementById(hidId).value = dpVal;
  if(spanid == "divEmprate"){    
    $$D("hidden_empRateMax").value = 100;
  }  
  $jq('.fltr').removeClass('fltr_hide');   
}
function hideShowPstDpDown(id){
  $jq('#'+id).show();
}
function setPstValue(o){
	$$('pstCodeTxt').value = (o.innerHTML).replace(/&amp;/g, '&');
	$$('pstCodeList').style.display = 'none'; 
 $jq("#pstCodeTxt").next("#pstCodeList").hide();  
 $jq("#hidden_distance").val(replaceAll($$('pstCodeTxt').value," miles","")); 
}
/*function setEmpValue(o){
  alert("1");
	$$('empTxt').value = (o.innerHTML).replace(/&amp;/g, '&');
	$$('empList').style.display = 'none'; 
 $jq("#empTxt").next("#empList").hide();  
 $jq("#hidden_empRateMin").val($$('empTxt').value.split("%")[0]); 
 $jq("#hidden_empRateMax").val(100);
 if($$('empTxt').value != "Choose emp rate"){
  $jq("#empTextBox").show();  
 } 
 alert("2");
}*/
$jq(document).click(function(e) {
  if(e.target.id == ""){
    $jq("#pstCodeList").hide();
    $jq("#empList").hide();
  }  
  //if(!$jq("#"+e.target.id).hasClass('fnt_lbk')) {}
});
function closeNoResultDiv(){
  $jq("#noResultDiv").hide();
}

function showOpendayPopup(collegeId, eventId){
  var url=""
  sm('newvenue', '650', 500, 'opendayPopup');
  var studyLevelId = $jq("#paramStudyLevelId").val();
  url=contextPath + "/openday-popup.html?collegeId="+collegeId+"&eventId="+eventId+"&studyLevelId="+studyLevelId;
  blockNone("ajax-div", "block");
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){ displayFormData(ajax); };
  ajax.runAJAX();
}

function showOpendayAjax(collegeId, eventId){
  hm();
  showOpendayPopup(collegeId, eventId);
} 
function opendayDropdown(){
  $jq("#opendayDropdown").toggle(); 
}

function closeOpenDaydRes(){
  if($$D("odEvntAction")) {    
      if($$D("odEvntAction").value=="false"){        
        var url = contextPath + "/open-days/reserve-place.html?reqAction=exitOpenDaysRes";      
        //$$D("ajax-div").style.display="block";
        var ajaxObj = new sack();
        ajaxObj.requestFile = url;	  
        ajaxObj.onCompletion = function(){setOpendaysExitVal(ajaxObj);};	
        ajaxObj.runAJAX();       
      }
    }    
}
function setOpendaysExitVal(ajaxObj){  
  if(ajaxObj.response!="" && ajaxObj.response=="true"){
    $$D("exitOpenDayResFlg").value = ajaxObj.response; 
    $$D("odEvntAction").value = "true";   
  }  
}

// SR and PR page clearing 

var jQcl = jQuery.noConflict();
function hideSRMessage(hideId){
  jQcl('#'+hideId).hide();
}



function locationOrPostCode() {
 if(jQcl('#locationOrPostcode').val() == '-1'){
 }else {
   if(jQcl('#locationOrPostcode').val() == '1'){
     jQcl('#postCodeDiv,#postcode_span').hide();
     jQcl('#location,#location_span').show()
     jQcl('#locationspanu1').html('Location');
     $$D('location_span').className = 'fr togflt_img tog_min';
     
   }else {
    jQcl('#location,#location_span').hide()
    jQcl('#locationspanu1').html('Post code');
    jQcl('#postCodeDiv,#postcode,#postcode_span').show();
    $$D('postcode_span').className = 'fr togflt_img tog_min';
   }
 }
}
function showLiForSubject(){
  jQcl('li#subjectmore_li').hide();
  jQcl('ul#subject_ul  li:gt(10)').show();
}

function toggleMenuClearing(id, link) {
  var e = $$D(id);
  var l = $$D(link+'_span');
  if (e.style.display == 'block' || e.style.display == '' ) {   
    e.style.display = 'none';
    l.className = 'fr togflt_img tog_plus';
  } else {
    e.style.display = 'block';
    l.className = 'fr togflt_img tog_min';
  }
} 

function clrFilterInMob(){
 jQcl('#ajax_listOfOptions').remove();
}


function hideUcasMessage(){
  jQcl('#empty_ucas_score').hide();
}

var opdub = jQuery.noConflict();

function getClearingAjaxCourseInfo(id, collegeName){
  opdub('#clrSRPR').show(); 
  var inputAttr = {}; 
  inputAttr.hidden_q = opdub('#hidden_q').val();
  inputAttr.hidden_subject = opdub('#hidden_subject').val();   
  inputAttr.hidden_university = opdub('#hidden_university').val();
  inputAttr.hidden_module = opdub('#hidden_module').val();
  inputAttr.hidden_locType = opdub('#hidden_locType').val();
  inputAttr.hidden_locationType = opdub('#hidden_locationType').val();
  inputAttr.hidden_location = opdub('#hidden_location').val();
  inputAttr.hidden_postCode = opdub('#hidden_postCode').val();
  inputAttr.hidden_dist = opdub('#hidden_dist').val();
  inputAttr.hidden_campusType = opdub('#hidden_campusType').val();
  inputAttr.hidden_studyMode = opdub('#hidden_studyMode').val();
  inputAttr.hidden_empRateMax = opdub('#hidden_empRateMax').val();
  inputAttr.hidden_empRateMin = opdub('#hidden_empRateMin').val();
  inputAttr.hidden_ucasTarrif_max = opdub('#hidden_ucasTarrif_max').val();
  inputAttr.hidden_ucasTarrif_min = opdub('#hidden_ucasTarrif_min').val();
  inputAttr.hidden_russell = opdub('#hidden_russell').val();
  inputAttr.hidden_entryLevel = opdub('#hidden_entryLevel').val();
  inputAttr.hidden_entryPoints = opdub('#hidden_entryPoints').val();
  inputAttr.hidden_sortorder = opdub('#hidden_sortorder').val();
  inputAttr.hidden_queryString = opdub('#hidden_queryString').val();
  inputAttr.hidden_removeModule = opdub('#hidden_removeModule').val();
  inputAttr.hidden_jacs = opdub('#hidden_jacs').val();
  inputAttr.hidden_pageno = opdub('#hidden_pageno').val();
  inputAttr.hidden_ucasCode = opdub('#hidden_ucasCode').val();
  inputAttr.hidden_resultExists = opdub('#hidden_resultExists').val();
  inputAttr.hidden_searchCount = opdub('#hidden_searchCount').val();
  inputAttr.hidden_preferenceType = opdub('#hidden_preferenceType').val();
  inputAttr.hidden_prefType = opdub('#hidden_prefType').val();
  inputAttr.hidden_assessment = opdub('#hidden_assessment').val();
  inputAttr.hidden_rf = opdub('#hidden_rf').val();
  inputAttr.requestURLForAjax = opdub('#requestURLForAjax').val();
  inputAttr.paramStudyLevelId = opdub('#paramStudyLevelId').val();
  inputAttr.collegeName = collegeName;
  inputAttr.hidden_score = opdub('#hidden_score').val();
  inputAttr.hidden_appFlag = opdub('#hidden_appFlag').val();
  inputAttr.hidden_applyFlag = opdub('#hidden_applyFlag').val();
  var urlname = "/degrees/ajax/display-clearing-course-list-info.html";
   opdub.ajax({
     type: "POST",
     url: urlname,
     data :  inputAttr,
     async : false,
     success: function(result) {
      //alert(result);
      opdub("#course_list_"+id).hide();
      opdub("#course_list_resp_"+id).html(result);
      opdub('#prPageURLLink_'+id).show();
      setTimeout(function(){ opdub('#clrSRPR').hide(); }, 1000); 
     }
   });
}     


function changeContactButton(hotlineNumber, networkId, gaCollegeName, suborderItemId, anchorId, collegeId) {
  opdub("#" + anchorId).attr("title", hotlineNumber);
  opdub("#" + anchorId).html("<i class='fa fa-phone' aria-hidden='true'></"+ "i>"+hotlineNumber);
  var mobileFlag = opdub("#check_mobile_hidden").val();
  if(mobileFlag == 'true'){
    setTimeout(function(){
     location.href='tel:'+ hotlineNumber;
    },1000)
  } 
}
//go apply badge lightbox - Jeya - 08Aug2019

function goApplyLightBox() {
  var loc = "/jsp/whatunigo/include/applicationErrorMessageLightbox.jsp?srText=SR_MESSAGE";
  var url = contextPath + loc;
  var resId = "revLightBox";
    opdub.ajax({
     url: url, 
     type: "POST", 
     success: function(result){
       opdub("#"+resId).html(result);
       lightBoxCall();
       revLightbox();
     }
   });  
}
function SRApplyNow(collegeId, courseId, oppId, subOrderIteId){
  var opportunityId = oppId;
  var domainSpecPath = document.getElementById("domainSpecPath").value;
  if(opdub("#clrSRPR")){opdub("#clrSRPR").show();}

  var url = contextPath + "/ajaxlogging/cpe-web-click-db-logging.html"; 
  var deviceWidth =  getDeviceWidth();
  var networkId = '2';
  opdub.ajax({
    url: url,
    async: false,
    data: {
      "vwcourseId": courseId,
      "z": collegeId,
      "externalUrl": opportunityId,
      "screenwidth": deviceWidth,
      "networkId": networkId,
      "clickType": "APPLY_NOW",
      "subOrderItemId": subOrderIteId
    },
    type: "POST",
    complete: function(result){
      //if(dev("#loadingImg")){dev("#loadingImg").hide();}
      //alert("result--->"+result);
    }
  });
  window.location.href = domainSpecPath+"/degrees/wugo/apply-now.html?courseId="+courseId+"&opportunityId="+opportunityId;
}