var contextPath = "/degrees";
//
var opdup = jQuery.noConflict();

opdup(document).ready(function(){  
  opdup("#hd1").removeAttr("style");
  opdup('.lft_nav .brw').click(function(){
		opdup(this).toggleClass('act');
		opdup(this).parents().next().toggleClass('act');
	});  
 opdup(".select_box_month").click(function(){
  opdup("#select_cont").slideToggle();
 });  
 opdup(".op_mth_slider ul li a").click(function(){
  opdup(".select_box_month .sld_mth ").html(opdup(this).html())
  opdup("#select_cont").slideToggle();
 });	
});