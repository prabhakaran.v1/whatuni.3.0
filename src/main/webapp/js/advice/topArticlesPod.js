var tap = jQuery.noConflict();
var flexslider;
function $$(id){return document.getElementById(id);}
function onScrollArticlesPod(pageName){  
  var oppid='';
  if($$('opportunityId')){
    oppid = $$('opportunityId').value;
  }  
  var ajaxURL = tap('#contextPath').val() + "/get-articles-pod.html?pagename="+pageName+"&oppid="+oppid;
  var ajax=new sack();
  ajax.requestFile = ajaxURL;
  ajax.onCompletion = function(){ onScrollArticlesPodResponse(ajax, pageName)};
  ajax.runAJAX();
}
function onScrollArticlesPodResponse(ajax, pageName){
  var response = ajax.response;  
  if(tap('#articlesPod')){
    tap('#articlesPod').html(response);
    setTimeout(function() {tap("#articlesPodLoading").hide();}, 500);
  }
}
function articlesSlider(){
  var fPath = $$("contextJsPath").value+'/js/jquery/jquery.flexslider-2.2.min.js';
  if (adv('head script[src="' + fPath + '"]').length > 0){
  }else{
    articleJsDynamicLoad(fPath);
  }  
  jQuery.noConflict();
  setTimeout(function() {
		  tap('#artSlider').flexslider({
				  animation: "slide",
      slideshow:false,
      animationSpeed: 600,
      animationLoop: false,
      slideshowSpeed : 4000,
      itemWidth: 400,
      itemMargin: -1,
      video: false,
      useCSS: false,
      pauseOnAction:true,
      minItems: getArticleWuGridSize(),
      maxItems: getArticleWuGridSize(),
				  start: function(slider){
        flexslider  = slider; 
						  tap('div.ltstvds').mouseover(function(){
							  slider.pause();
						  });
						  tap('div.ltstvds').mouseout(function() {
							  slider.resume();
						  });
        tap('.clone').each(function (){             
          var ids = tap(this).find("img").map(function(){
          return tap(this).attr('id')}).get(); 
          for (var i=0; i<ids.length; i++) {
            var art_src = tap("#"+ids[i]).attr("data-src");
              tap('#artSlider').find(".clone").find("#"+ids[i]).attr("src",art_src).removeAttr('id').removeAttr("data-src");
          }
          for(var nxtSlide=(slider.currentSlide*4)+1;nxtSlide<=4;nxtSlide++){
            var src = tap("#img"+nxtSlide).attr("data-src");
            tap("#img"+nxtSlide).attr("src",src);
          }            
         });
						},
      after: function (slider) {          
        var nextslide = slider.currentSlide+1;
        if(nextslide!=1){
          for(var nxtSlide=Math.pow(2,nextslide);nxtSlide<nextslide*4;nxtSlide++){
            tap("#img"+nxtSlide).each(function () {
              var src = tap("#img"+nxtSlide).attr("data-src");
              tap("#img"+nxtSlide).attr("src",src).removeAttr("data-src");
            });
          }
        }            
      }
				});
				tap('.flexslider').flexslider({
				  animation: "slide"
				});						  	
  }, 500);
}
function getArticleWuGridSize() {  
  var gridSize = (tap(window).width() <= 480) ? 1 : (tap(window).width() <= 992) ? 2 : 4;
  return gridSize;
}