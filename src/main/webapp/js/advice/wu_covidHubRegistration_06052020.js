var covid_jq = jQuery.noConflict();
var block = "block", none="none", loadingGif="loadinggif";
function $$D(docid){ return document.getElementById(docid); }
function blockNone(thisid, value){ if($$D(thisid) !=null){ $$D(thisid).style.display = value; } }
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
function isEmpty(value){  if(trimString(value) ==''){ return true;} else {  return false; } }
function showErrorMsg(thisid, errmsg){ if($$D(thisid) !=null){ $$D(thisid).style.display = block;  $$D(thisid).innerHTML = errmsg;} }

function checkValidEmail(email){
  if(/^\w+([-+.'']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(trimString(email))){return true}
  return false
}

var CovidHubRegErrorMessage = { 
   error_firstName              : 'Please enter your first name',
   error_surName                : 'Please enter your last name',
   error_user_email             : 'Please enter your email',
   error_valid_email            : 'Please enter a valid email address',
   error_termsc                 : 'Please agree to our terms and conditions'
}

function covidHubRegistrationBox(){
  var message = true;
  var firstName = trimString(covid_jq("#covid_fname").val());   
  var surName = trimString(covid_jq("#covid_lname").val());    
  var emailAddress =  trimString(covid_jq("#covid_email").val());
  
  if(isEmpty(firstName) || firstName == 'First name*') { 
    message = false; showErrorMsg('firstName_covid_error', CovidHubRegErrorMessage.error_firstName); 
  }else{ 
    blockNone('firstName_covid_error', none);
  }
  if(isEmpty(surName) || surName == 'Last name*') {
    message = false; showErrorMsg('lastName_covid_error', CovidHubRegErrorMessage.error_surName); 
  }else{
    blockNone('lastName_covid_error', none);
  }    
  if(isEmpty(emailAddress) || emailAddress == 'Email address'){ 
    message = false; showErrorMsg('email_covid_error', CovidHubRegErrorMessage.error_user_email);
  } else if(!checkValidEmail(trimString(emailAddress))){ 
    message = false; showErrorMsg('email_covid_error', CovidHubRegErrorMessage.error_valid_email); 
  }else{
    blockNone('email_covid_error', none);
  }
  //if($("#checkSurfaceEnvironment-1").prop('checked') == true
  if(covid_jq("#covid_spamBox").prop('checked') == false){
    message = false; showErrorMsg('checkbox_covid_error', CovidHubRegErrorMessage.error_termsc); 
  }else{
    blockNone('checkbox_covid_error', none);
  }
  if(!message){      
    message = true;
  }else {    
    var url = "/degrees/newuserregistration.html?submittype=covidreg&firstName="+ firstName + "&surName=" + surName;
    blockNone(loadingGif, block);
    var ajax=new sack();
        ajax.requestFile = url;	        
    ajax.setVar("emailAddress", trimString(emailAddress));         
    ajax.onCompletion = function(){ showResponseMsgCovid(ajax); };	
    ajax.runAJAX();
   return false;
  }    
}

function showResponseMsgCovid(ajax){
  var response = ajax.response;
  var covidMsgParam = covid_jq("#covidMsgParam");
  if(response.trim() == "Already Subscribed"){
	covidMsgParam.html("You have already subscribed.");    
	blockNone(loadingGif, none);
	covidMsgParam.addClass("qler1");
  }else if(response.trim() == "Successfully Subscribed"){
	GARegistrationLogging('register', 'covid-hub', covid_jq("#GAMPagename").val());
	covidMsgParam.html("Thanks, you have subscribed successfully!");
    covidMsgParam.addClass("");    
	blockNone(loadingGif, none);
  }else if(response.trim() == "Your Account has been locked"){
    covidMsgParam.html("Your Account has been locked.");    
    blockNone(loadingGif, none);
    covidMsgParam.addClass("qler1");
  }
}

function addAndRemoveOverlapClassName(currentField){
  var currentFieldValue = covid_jq(currentField).val();
  if(!isEmpty(currentFieldValue)){ 
    covid_jq(currentField).next('label').addClass("top"); 
  }else{ 
   covid_jq(currentField).next('label').removeClass("top"); 
  }
}