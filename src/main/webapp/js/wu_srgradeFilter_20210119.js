var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";
var contextPath = "/degrees";
var numbers = /^[0-9]+$/;
function $$D(docid){ return document.getElementById(docid); }
var jq = jQuery.noConflict();
function isNullAndUndef(variable) {return (variable !== null && variable !== undefined && variable !== '');}
function isNotNullAnddUndef(variable) {return (variable !== null && variable !== undefined && variable !== '');}
function isBlankOrNullString(str) {
  var flag = true;
  if(str.trim() != "" && str.trim().length > 0) {
    flag = false;
  }
  return flag;
}
var successFlag = 'Y';
function setGradeVal(obj,grdeArrId){
  var optVal = '';
  var type = jq(jq(obj).get(0)).prop("tagName").toLowerCase();
  console.log(type);  
  if(type == "input"){
    var ucsIpId = jq(obj).attr('id');	  
    var subIpId = ucsIpId.replace('span_grde', 'qualSub');
    var ucsIpId = ucsIpId.replace('span_grde', 'span_grde');
    var errDivId = ucsIpId.replace('span_grde', 'err_msg');  
	jq('#'+subIpId).removeClass('faild');
	jq('#'+ucsIpId).removeClass('faild');
	jq('#'+errDivId).hide();
	optVal = jq(obj).val();
  }else{
	optVal = jq(obj).children("option:selected").val();
  }
  var spanValId = 'span_'+ grdeArrId;
  jq('#'+grdeArrId).val(optVal);
  jq('#'+spanValId).text(optVal);
}

//
function setAjaxSubjectVal(ajaxOptionObj){
  var grandParentDivID = jq(ajaxOptionObj).parent().parent().parent().attr('id');
  var parentDivID = jq(ajaxOptionObj).parent().parent().attr('id');
  var subSrchIpID = parentDivID.replace('subjectAjaxList_', '');
  var subjectNameVal = jq(ajaxOptionObj).text().trim();
  var subjectIdVal = ajaxOptionObj.id;
  var subArrHidId = 'sub' + grandParentDivID.replace('ajaxdiv', '');
  var errId = subSrchIpID.substring(8);
  var qualId =  jq('#qualId').val();
  var heGrade = jq('#heCredits').val();
  jq('#err_msg_'+errId).hide();
  jq('#'+subSrchIpID).val(subjectNameVal);
  jq('#'+grandParentDivID).hide();
  jq('#'+subArrHidId).val(subjectIdVal);
  jq('#'+subSrchIpID).removeClass('faild');
  jq('#'+subSrchIpID).blur(); 
  var pageName = jq('#page-name').val();
  if(pageName == "srGradeFilterPage") {
    getUcasPoints(subArrHidId);
  }
  if(pageName == "advanceSearchPage" && !isBlankOrNullString(jq('#qualSub_0_0').val())) {
    if ('20' == qualId){
	   if ('45' == heGrade) {
		   jq('#apply_0').removeAttr('disabled');
	   }
    } else {
      jq('#apply_0').removeAttr('disabled');
    }
  }
}
//
var srch_cur_let = new Array();
function cmmtQualSubjectList(obj, id, action, resultDivId){
 var selected_qual = jq('#selected_qual').val();
 var url = contextPath + "/uni-course-requirements-match.html";
 var resId = resultDivId;
 if(successFlag = 'Y'){
	jq('#errorDiv').hide();
 }
  action = 'QUAL_SUB_AJAX';
 var freeText = jq('#'+obj.id).val().trim();
 var subArrId = 'sub_'+ id.replace('qualSub_', '');
 jq('#'+subArrId).val('');
 if(srch_cur_let[obj.id] == freeText){
   return;
 }
 srch_cur_let[obj.id]= freeText;
 var qualSelId = id.substring(id.indexOf('_')+1, id.lastIndexOf("_"));
 var qualId = jq('#qualsel_'+qualSelId).children("option:selected").val();
 qualId = qualId.indexOf('gcse') > -1 ? qualId.substring(0, qualId.indexOf("_")) : qualId;
 if(isNotNullAnddUndef(freeText) && (freeText.length > 2 || qualId == '18')){ 
  //
  var stringJson =	{
    "action": action,
    "subQualId": qualId,
    "qualSubList": createJSON()
  };
  var jsonData = JSON.stringify(stringJson);
  var param = "?action="+action+"&free-text="+freeText;
  var url = contextPath + "/uni-course-requirements-match.html" + param;
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    dataType: 'json',
    type: "POST",
    data: jsonData,
    async: false,
    complete: function(response){
      var result = response.responseText;
      var selectDivId= "subjectAjaxList_"+id;
      if(result.trim() == ''){
        result = '<div class="nores">No results found for ' + freeText + '</div>';
      };
      var selectBox = '<div name="subjectAjaxList" onchange="" id='+selectDivId+' class=""><div id="ajax_listOfOptions" class="ajx_cnt">'+result+'</div></div>';
      jq('[data-id=wugoSubAjx]').each(function(){
        jq(this).html('');
      });
      jq("#"+resId).html(selectBox);
      var ajxwd = jq('#'+id).outerWidth() + 'px';     
      var ajxht = document.getElementById(id).offsetHeight + 'px';      
      jq('#ajax_listOfOptions').css('top', ajxht);
      jq('#ajax_listOfOptions').css('width', ajxwd);
      jq("#"+resId).show();  
    }
  });
  }
  else{
    jq('#'+resId).hide();
    if(!isNotNullAnddUndef(freeText)){
      jq('#'+id).removeClass('faild');
      var grdIpId = id.replace('qualSub','span_grde');
      jq('#'+grdIpId).removeClass('faild');
    }
  }
}
function addTopCls(){
  jq('.frm-ele').each(function(index) { 
    var xy = jq(this).val();
    if (xy != "") {
      jq(this).next('label').addClass("top_lb");
    }else {
      jq(this).next('label').removeClass("top_lb");
    } 
  });
}
function lightBoxCall(){
 	jq("#gradeFilterRevLightBox").addClass("rev_lbox grdcrs_ui");
  jq("#gradeFilterRevLightBox").css({'display':'block','opacity' : '1','z-index':'3','visibility':'visible'});
 //jq(".rev_lbox").addClass("fadeIn").removeClass("fadeOut");
 jq("html").addClass("rvscrl_hid");
}
jq(window).resize(function() {
  revLightbox();
});
jq(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
revLightbox();
}
function closeLigBox(){
  jq("#gradeFilterRevLightBox").removeClass("rev_lbox");  
}
function revLightbox(){
  var width = document.documentElement.clientWidth;
  var revnmehgt = jq(".rvbx_cnt .revlst_lft").outerHeight();
  var lbxhgt = jq(".rev_lbox .rvbx_cnt").height();
  var txth2hgt = jq(".rvbx_cnt h2").outerHeight();
  var txth3hgt = jq(".rvbx_cnt h3").outerHeight();
  if (width <= 480) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    jq(".lbx_scrl").css("height", +scrhgt+ "px");
  } else if ((width >= 481) && (width <= 992)) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    jq(".lbx_scrl").css("height", +scrhgt+ "px");
  } else{
    var lbxmin = 17 + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    jq(".lbx_scrl").css("height", +scrhgt+ "px"); 
  }
}
function appendGradeDropdown(iloop, gdeId, selVal){
  var qualId  = jq('#qualsel_'+iloop).val();
  var data = jq('#qualGrde_'+iloop+'_'+qualId).val().split(',');
  var id = gdeId;
  jq.each(data, function(value) {
  var $option = jq("<option/>", {
    value: data[value],
    text: data[value]
  });
  jq('#'+id).append($option);
  jq("#"+id).val(selVal);
  });
}
function addNewRowSub(qualSelId){
  var qualId = jq('#'+qualSelId).children("option:selected").val();
  appendQualDiv(jq('#'+qualSelId), 1);
}
//selecting the value of subject dropdown
function setSelectedVal(formId, id){
  var value = jq('#'+id).children("option:selected").text();
  jq('#'+formId).html(value);
}
function appendQualDiv(obj, addVal, loop, pageType, addQualFlag){
  var qualId = '1';
  if('add_Qual' == addQualFlag){
    qualId = '1';
  } else{
    qualId = obj.value;
  }
  var qualIdForQualArr = qualId;
  var subArrDataId = '';
  var count = jq('#qualSubj_'+loop+'_'+qualId).val();
  subArrDataId = 'data-id="level_3"';
  if(isNotNullAnddUndef(addVal)){ 
    count = count + addVal;
  }
  var result = "";
  var resSt = '<div class="ucas_row grd_row" id="grdrow_'+loop+'"><div class="rmv_act" id="subSec_' + loop +'"><fieldset class="ucas_fldrw">';
  var resEd = '</fieldset></div>';
  var hidSubCntId = 'countAddSubj_'+ loop;
  jq('#'+hidSubCntId).val(count);
  jq('#total_'+hidSubCntId).val(count);
  var gradeVal = jq('#qualGrde_'+loop+'_'+qualId).val();
  var gdeArr = gradeVal.split(',');
  var gradeSpanEd = '</div>';
  if (qualId!= '20')  {
		result += '<div class="ucas_row grd_row" id="grdrow_'+loop+'"><fieldset class="ucas_fldrw quagrd_tit"><div class="ucas_tbox tx-bx"><h5 class="cmn-tit qual_sub">Subject</h5></div><div class="ucas_selcnt rsize_wdth">';
	  }  
  if(qualId == '19'){
	result +='<h5 class="cmn-tit qual_grd">Points</h5></div></fieldset>';	  
  }else if (qualId != '20'){
	result +='<h5 class="cmn-tit qual_grd">Grade</h5></div></fieldset>';  
  }
  
  var enOrDisTopNavFn = "enOrDisTopNav('offerEditQual');";
  for(lp = 0; lp < count; lp++){
    var removeSub = "'subSec_"+loop + "_" + lp +"'";
    var grdeArrId = "'grde_"+loop+"_"+lp+"'";
    var subTxtId = "'qualSub_"+loop+"_"+lp+"'";
    var subAjaxId = "'ajaxdiv_"+loop+"_"+lp+"'";
    var actionParam = "'QUAL_SUB_AJAX'";
    var grdeSpanId = "'span_grde_" + +loop+"_"+lp+"'";
    var qualGrdId = "'qualGrd_"+loop+"_"+lp+"'";
    var grdStr = "'GRADE'";
    var subValId = "'sub_"+loop+"_"+lp+"'";   
    var removeLinkImg = jq('#removeLinkImg').val();
    var countAddSubId = "'countAddSubj_"+loop+"'";
    var addSubstyle = "display:none";
    var pageName = jq('#page-name').val();
    if(pageName != "srGradeFilterPage") {
    if (qualId != '20') {
      result += '<div class="rmv_act" id="subSec_'+loop + '_' + lp +'"><fieldset class="ucas_fldrw">';
      result += '<div class="ucas_tbox w-390 tx-bx"><input type="text" data-id="qual_sub_id" id="qualSub_'+loop+'_'+lp+'" name="'+qualId + 'sub_'+ lp +'" placeholder="Enter subject" value="" onkeyup="cmmtQualSubjectList(this, '+subTxtId+', '+actionParam+', '+subAjaxId+');"  class="frm-ele" autocomplete="off"><div id="ajaxdiv_'+loop+'_'+lp+'" data-id="wugoSubAjx" class="ajaxdiv"></div></div>';
    }
    if (qualId == '20') {
    	// Access to HE starts
		result += '<div class="subgrd_fld acc_hedip" id="subGrdDiv0"><div class="ucas_row grd_row" id="grdrow_'+loop+'"><div class="rmv_act"><div class="rmv_act" id="subSec_'+loop + '_' + lp +'"><fieldset class="ucas_fldrw"><div class="ucas_tbox w100"><input type="text" data-id="qual_sub_id" id="qualSub_'+loop+'_'+lp+'" name="'+qualId + 'sub_'+ lp +'" placeholder="Enter subject" onkeyup="cmmtQualSubjectList(this, '+subTxtId+', '+actionParam+', '+subAjaxId+');" value="" class="frm-ele" autocomplete="off"><div id="ajaxdiv_'+loop+'_'+lp+'" data-id="wugoSubAjx" class="ajaxdiv" ></div></div><div id="err_msg_0_0" class="err_field fl_w100"></div></fieldset></div>';
      for(var size = 0; size< gdeArr.length;size++) {
   	  var gradeTypeName = gdeArr[size] == 'D'?'Distinction':gdeArr[size] == 'M'?'Merit':'Pass';
      result += '<div class="rmv_act" id="subSec_'+loop + '_' + lp +'"><fieldset class="ucas_fldrw">'
          +'<div class="ucas_tbox"><div class="hedip_txt">' + gradeTypeName +'</div></div>' 
    	  +'<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"> <span class="sbox_txt" id="span_grde_'+loop+'_'+lp+'_'+gdeArr[size]+'_credit">0 credits</span><span class="grdfilt_arw"></span> </div>'
          +'<select class="ucas_slist qus_slt" onchange=setGradeVal(this,"grde_'+loop+'_'+lp+'_'+gdeArr[size]+'_credit");heDipDropDownGen("'+loop+'","'+lp+'"); id=grade_option_'+gdeArr[size]+'_credit_'+loop+'>';
          for(var index = 0 ; index <= 45 ; index+=3){
  			result += "<option value = "+index+"credits>"+index+"credits </option>";
  		  }
          result += '</select></div><div id="err_msg_0_1" class="err_field fl_w100"></div>'
          +'</fieldset></div>';
      }
        /*  +'<div class="rmv_act" id="subSec_0_2"><fieldset class="ucas_fldrw">'
          +'<div class="ucas_tbox"><div class="hedip_txt">Merit</div></div>'
          +'<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"> <span class="sbox_txt" id="span_grde_0_2">0 credits</span><span class="grdfilt_arw"></span> </div><select class="ucas_slist qus_slt" id="qualGrd_0_0"><option value="A*">0 credits</option><option value="A">0 credits</option><option value="B">0 credits</option><option value="C">0 credits</option><option value="D">0 credits</option><option value="E">0 credits</option></select></div>'
          +'<div id="err_msg_0_1" class="err_field fl_w100"></div></fieldset></div>'
          +'<div class="rmv_act" id="subSec_0_2"><fieldset class="ucas_fldrw">'
          +'<div class="ucas_tbox"><div class="hedip_txt">Pass</div></div>'
          +'<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"> <span class="sbox_txt" id="span_grde_0_2">0 credits</span><span class="grdfilt_arw"></span> </div><select class="ucas_slist qus_slt"  id="qualGrd_0_0"><option value="A*">0 credits</option><option value="A">0 credits</option><option value="B">0 credits</option><option value="C">0 credits</option><option value="D">0 credits</option><option value="E">0 credits</option></select></div>'
          +'<div id="err_msg_0_1" class="err_field fl_w100"></div></fieldset></div>'*/
      result +='</div></div></div>';
      // Access to HE ends
    } else if(qualId != '19'){          
    result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="span_grde_'+loop+'_'+lp+'">'+gdeArr[0]+'</span><span class="grdfilt_arw"></span></div>'
           + '<select class="ucas_slist qus_slt" onchange="setGradeVal(this, '+grdeArrId+');" id="qualGrd_'+ loop +'_'+ lp +'"></select>'
           + '<script>appendGradeDropdown('+loop+', '+qualGrdId+'); </script>'
           + '</div>';
    }else {    
    result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_tbox w-390 tx-bx fl_w100 sr_ucas" id="1grades1"><input onkeyup="setGradeVal(this, '+grdeArrId+');" onfocus="clearFields(this)" onblur="ucasPtVal(this)" type="text" maxlength="3" class="frm-ele fl_100" id="span_grde_'+loop+'_'+lp+'" placeholder="" value="'+gdeArr[0]+'" autocomplete="off"></input></div>'
           + '</div>';	   
    }
    if (qualId != '20') {
      result += '<div class="add_fld"> <a id="1rSubRow3" onclick="removeSubject('+removeSub+', '+loop+');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="'+removeLinkImg+'" title="Remove subject &amp; grade"></span></a></div>';
    }
    result += '<div id="err_msg_'+loop+'_'+lp+'" class="err_field fl_w100" style="display:none;"></div>'
           + resEd
           + '<input class="subjectArr" '+subArrDataId+' type="hidden" id="sub_'+loop+'_'+lp+'" value="" />'
           + '<input class="gradeArr" type="hidden" id="grde_'+loop+'_'+lp+'" value="'+gdeArr[0]+'" />'
           + '<input class="qualArr" type="hidden" id="qual_'+loop+'_'+lp+'" value="'+qualIdForQualArr+'" />'
           + '<input class="qualSeqArr" type="hidden" id="qual_'+loop+'_'+lp+'" value="'+(parseInt(loop)+1)+'" />'
           + '<input class="qualId" type="hidden" id="qualId" value="'+qualId+'" />'
           + '<input class="cedits" type="hidden" id="heCredits" value="" />';
    } else {
      result += '<div class="rmv_act" id="subSec_'+loop + '_' + lp +'"><fieldset class="ucas_fldrw">'
             + '<div class="ucas_tbox w-390 tx-bx"><input type="text" data-id="qual_sub_id" id="qualSub_'+loop+'_'+lp+'" name="'+qualId + 'sub_'+ lp +'" placeholder="Enter subject" value="" onblur="getUcasPoints('+subValId+', '+grdStr+');" onkeyup="cmmtQualSubjectList(this, '+subTxtId+', '+actionParam+', '+subAjaxId+');"  class="frm-ele" autocomplete="off"><div id="ajaxdiv_'+loop+'_'+lp+'" data-id="wugoSubAjx" class="ajaxdiv"></div></div>'
      if(qualId != '19'){        
      result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="span_grde_'+loop+'_'+lp+'">'+gdeArr[0]+'</span><span class="grdfilt_arw"></span></div>'
             + '<select class="ucas_slist" onchange="setGradeVal(this, '+grdeArrId+');getUcasPoints('+subValId+', '+grdStr+');" id="qualGrd_'+ loop +'_'+ lp +'"></select>'
             + '<script>appendGradeDropdown('+loop+', '+qualGrdId+'); </script>'
             + '</div>';
      }else{
      result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_tbox w-390 tx-bx fl_w100 sr_ucas" id="1grades1"><input onkeyup="setGradeVal(this, '+grdeArrId+');" onblur="getUcasPoints('+subValId+', '+grdStr+');ucasPtVal(this)" onfocus="clearFields(this)" type="text" maxlength="3" class="frm-ele fl_100" id="span_grde_'+loop+'_'+lp+'" placeholder="" value="'+gdeArr[0]+'" autocomplete="off"></input></div>'
             + '</div>';	  
      }
      result += '<div class="add_fld"> <a id="1rSubRow3" onclick="removeSubject('+removeSub+', '+loop+');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="'+removeLinkImg+'" title="Remove subject &amp; grade"></span></a></div>'
             + '<div id="err_msg_'+loop+'_'+lp+'" class="err_field fl_w100" style="display:none;"></div>'
             + resEd
             + '<input class="subjectArr" '+subArrDataId+' type="hidden" id="sub_'+loop+'_'+lp+'" value="" />'
             + '<input class="gradeArr" type="hidden" id="grde_'+loop+'_'+lp+'" value="'+gdeArr[0]+'" />'
             + '<input class="qualArr" type="hidden" id="qual_'+loop+'_'+lp+'" value="'+qualIdForQualArr+'" />'
             + '<input class="qualSeqArr" type="hidden" id="qual_'+loop+'_'+lp+'" value="'+(parseInt(loop)+1)+'" />';
 
    }
  
  }    
  if(count < 6){
    addSubstyle = "display:block";
  } else{
    addSubstyle = "display:none";
  }
  if (qualId != '20') {
  result += '<div class="ad-subjt" id="addSubject_'+loop+'" style="'+addSubstyle+'">'
         + '<a class="btn_act bck fnrm" id="subAdd'+loop+'" onclick="addSubject('+qualIdForQualArr+','+countAddSubId+', '+loop+');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>'
         + '</div>';  
  }
  result += '</div>';
  jq('#subGrdDiv'+loop).text('');
  jq('#subGrdDiv'+loop).append(result);
  jq('#subGrdDiv'+loop).find("script").each(function(i) {
    eval(jq(this).text());
  });
  if(1 == count){
    jq('#grdrow_'+loop).find('a:first').hide();
  } else{jq('#grdrow_'+loop).find('a:first').show();}
  if('add_Qual' == addQualFlag || 'add_Qual_gcse' == addQualFlag){
    jq('#qualsel_'+loop).val(qualId);
  }
  var selTextVal = jq('#qualsel_'+loop+' option:selected').text();  
  jq('#newQualDesc').val(selTextVal);
  jq('#qualspan_'+loop).text(selTextVal);
  var qualSubListVal = createJSON();
  var pageName = jq('#page-name').val();
  if(pageName == "srGradeFilterPage") {
    if(qualSubListVal.length > 0){
      getUcasPoints('', '', 'CHANGE_QUAL');
    } else{
      var crsReqUcasPts = jq('#coureReqTariffPts').val();
      jq('#oldGrade').text("0");
      jq('#newGrade').text("0");
    }  
  } 
  if(pageName == "advanceSearchPage" && isBlankOrNullString(jq('#qualSub_0_0').val())) {
    jq('#apply_0').attr('disabled','disabled');
  }
}
var maxSubLimit = 6;
function removeSubject(subDivId, loop){
  var removeVal = subDivId;
  var subId = 'qualSub_' + removeVal.replace('subSec_','');  
  jq('#'+ removeVal).remove();
  var res = removeVal.substring(6);
  jq('#sub'+res).val('');
  jq('#grde'+res).val('');
  jq('#qual'+res).val('');
  jq('#qual'+res).val('');
  var rmvUcas = "sub"+res;
  var pageName = jq('#page-name').val();
  if(pageName == "srGradeFilterPage") {
    getUcasPoints(rmvUcas,'', 'REMOVE_SUB');
  }
  var subCount = jq('#countAddSubj_' + loop).val();
  var count = subCount - 1;
  var hidSubCntId = 'countAddSubj_'+loop;
  jq('#'+hidSubCntId).val(count);
  if(count <= maxSubLimit) { jq('#addSubject_'+loop).show(); }
  if(1 == count){
    jq('#grdrow_'+loop).find('a:first').hide();
  } else{jq('#grdrow_'+loop).find('a:first').show();}
}
function setSelectedVal(formId, id){
  var value = jq('#'+id).children("option:selected").text();
  var selecIndex = jq('#'+id).children("option:selected").val();
  jq('#'+formId).html(value);
  var loop = formId.substring(6);
  jq('#userQualId'+loop).val();
  getUcasPoints('','', 'REMOVE_SUBJECT');
}
function addSubject(qualId, subCntId, loop){
  var selectedQual = jq('#qualsel_'+loop).val();
  var subCount = jq('#' + subCntId).val();
  var totSubCnt = jq('#total_'+ subCntId).val();
  var subArrDataId = '';
  var qualIdForQualArr = selectedQual;
  result = '<div class="ucas_row grd_row" id="grdrow_'+qualId+''+loop+'">';
  if(isNotNullAnddUndef(selectedQual) && selectedQual.indexOf('_gcse') > -1){
    var gradeVal = jq('#qualGrde_'+loop+'_'+selectedQual).val();
    qualIdForQualArr = qualIdForQualArr.substring(0, selectedQual.indexOf('_gcse'));
    maxSubLimit = 20;
    subArrDataId = '';
  } else{
    var gradeVal = jq('#qualGrde_'+loop+'_'+selectedQual).val();
    if(qualId == 18) {maxSubLimit = 3;} 
    else { maxSubLimit = 6; }
    subArrDataId = 'data-id="level_3"';
  }
  var gdeArr = gradeVal.split(',');
  var enOrDisTopNavFn = "enOrDisTopNav('offerEditQual');";
  if(subCount <= maxSubLimit) {
    var count = parseInt(subCount) + 1;
    var totCnt = parseInt(totSubCnt) + 1;
    var hidSubCntId = subCntId;
    jq('#'+hidSubCntId).val(count);
    jq('#total_'+hidSubCntId).val(totCnt);
    var removeSub = "'subSec_"+loop + "_" + totCnt +"'";
    var grdeArrId = "'grde_"+loop+"_"+totCnt+"'";
    var subTxtId = "'qualSub_"+loop+"_"+totCnt+"'";
    var scrhIpId = "'qualSub_"+loop+"_"+totCnt+"'";
    var subAjaxId = "'ajaxdiv_"+loop+"_"+totCnt+"'";
    var actionParam = "'QUAL_SUB_AJAX'";
    var qualGrdId = "'qualGrd_"+loop+"_"+totCnt+"'";
    var grdStr = "'GRADE'";
    var subValId = "'sub_"+loop+"_"+totCnt+"'";
    var removeLinkImg = jq('#removeLinkImg').val();
    var pageName = jq('#page-name').val();
    if(pageName != "srGradeFilterPage") {
      result = '<div class="rmv_act" id="subSec_'+loop + '_' + totCnt +'"><fieldset class="ucas_fldrw">'
           + '<div class="ucas_tbox w-390 tx-bx"><input type="text" data-id="qual_sub_id" id="qualSub_'+loop+'_'+totCnt+'" name="'+qualId + 'sub_'+ totCnt +'" placeholder="Enter subject" value="" onkeyup="cmmtQualSubjectList(this, '+scrhIpId+', '+actionParam+', '+subAjaxId+');"  class="frm-ele" autocomplete="off"><div id="ajaxdiv_'+loop+'_'+totCnt+'" data-id="wugoSubAjx" class="ajaxdiv"></div></div>'
	  if(qualId != '19'){ 
	    result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="span_grde_' + loop +'_'+totCnt+'">'+gdeArr[0]+'</span><span class="grdfilt_arw"></span></div>'
		   + '<select class="ucas_slist" onchange="setGradeVal(this, '+grdeArrId+');" id="qualGrd_'+ loop +'_'+ totCnt +'"></select>'
		   + '<script>appendGradeDropdown('+loop+', '+qualGrdId+');</script>'
		   + '</div>';
	  }else{
	    result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_tbox w-390 tx-bx fl_w100 sr_ucas" id="1grades1"><input onkeyup="setGradeVal(this, '+grdeArrId+');" onblur="ucasPtVal(this);" onfocus="clearFields(this)" type="text" maxlength="3" class="frm-ele fl_100" id="span_grde_'+loop+'_'+totCnt+'" placeholder="" value="'+gdeArr[0]+'" autocomplete="off"></input></div>'
		   + '</div>';	 
	  }   
	  result += '<div class="add_fld"> <a id="1rSubRow3" onclick="removeSubject('+removeSub+', '+loop+');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="'+removeLinkImg+'" title="Remove subject &amp; grade"></span></a></div>'
           +'<div id="err_msg_'+loop+'_'+ totCnt +'" class="err_field fl_w100" style="display:none;"></div>'
		   + '<input class="subjectArr" '+subArrDataId+' type="hidden" id="sub_'+loop+'_'+totCnt+'" value="" />'
		   + '<input class="gradeArr" type="hidden" id="grde_'+loop+'_'+totCnt+'" value="'+gdeArr[0]+'" />'
		   + '<input class="qualArr" type="hidden" id="qual_'+loop+'_'+totCnt+'" value="'+qualIdForQualArr+'" />'
		   + '<input class="qualSeqArr" type="hidden" id="qual_'+loop+'_'+totCnt+'" value="'+(parseInt(loop)+1)+'" />';
    } else {
     result = '<div class="rmv_act" id="subSec_'+loop + '_' + totCnt +'"><fieldset class="ucas_fldrw">'
           + '<div class="ucas_tbox w-390 tx-bx"><input type="text" data-id="qual_sub_id" id="qualSub_'+loop+'_'+totCnt+'" name="'+qualId + 'sub_'+ totCnt +'" placeholder="Enter subject" value="" onblur="getUcasPoints('+subValId+', '+grdStr+');" onkeyup="cmmtQualSubjectList(this, '+scrhIpId+', '+actionParam+', '+subAjaxId+');"  class="frm-ele" autocomplete="off"><div id="ajaxdiv_'+loop+'_'+totCnt+'" data-id="wugoSubAjx" class="ajaxdiv"></div></div>'
	   if(qualId != '19'){ 
	    result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="span_grde_' + loop +'_'+totCnt+'">'+gdeArr[0]+'</span><span class="grdfilt_arw"></span></div>'
	       	+ '<select class="ucas_slist qus_slt" onchange="setGradeVal(this, '+grdeArrId+');getUcasPoints('+subValId+', '+grdStr+');" id="qualGrd_'+ loop +'_'+ totCnt +'"></select>'
	       	+ '<script>appendGradeDropdown('+loop+', '+qualGrdId+');</script>'
	       	+ '</div>';
	   }else{
	    result += '<div class="ucas_selcnt rsize_wdth"><div class="ucas_tbox w-390 tx-bx fl_w100 sr_ucas" id="1grades1"><input onkeyup="setGradeVal(this, '+grdeArrId+');" onblur="getUcasPoints('+subValId+', '+grdStr+');ucasPtVal(this);" onfocus="clearFields(this)" type="text" maxlength="3" class="frm-ele fl_100" id="span_grde_'+loop+'_'+totCnt+'" placeholder="" value="'+gdeArr[0]+'" autocomplete="off"></input></div>'
	           + '</div>';	 
	   }  	
     result += '<div class="add_fld"> <a id="1rSubRow3" onclick="removeSubject('+removeSub+', '+loop+');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="'+removeLinkImg+'" title="Remove subject &amp; grade"></span></a></div>'
        	+ '<div id="err_msg_'+loop+'_'+ totCnt +'" class="err_field fl_w100" style="display:none;"><p>Please enter the subject</p></div>'
        	+ '<input class="subjectArr" '+subArrDataId+' type="hidden" id="sub_'+loop+'_'+totCnt+'" value="" />'
        	+ '<input class="gradeArr" type="hidden" id="grde_'+loop+'_'+totCnt+'" value="'+gdeArr[0]+'" />'
        	+ '<input class="qualArr" type="hidden" id="qual_'+loop+'_'+totCnt+'" value="'+qualIdForQualArr+'" />'
        	+ '<input class="qualSeqArr" type="hidden" id="qual_'+loop+'_'+totCnt+'" value="'+(parseInt(loop)+1)+'" />';
    }
    if(count == maxSubLimit){jq('#addSubject_'+loop).hide();} 
  } else{ jq('#addSubject_'+loop).hide(); }
    result += '</div>';
    jq('#subGrdDiv'+loop).find('.rmv_act').last().after(result);
    jq('#subGrdDiv'+loop).find("script").each(function(i) {
  });
  if(count > 1){
    jq('#grdrow_'+loop).find('a:first').show();
  }
}
function createJSON(){
  var qualId = '';jq('.qualArr').each(function(){qualId += this.value + ','; });
  var subId = '';jq('.subjectArr').each(function(){subId += this.value + ',';});
  var grade = '';jq('.gradeArr').each(function(){grade += this.value + ',';});
  var qualSeq = '';jq('.qualSeqArr').each(function(){qualSeq += this.value + ',';});
  var count = subId.length;
  var qualIdArr = (qualId != null && qualId != '') ? qualId.split(',') : '';
  var subIdArr = (subId != null && subId != '') ? subId.split(',') : '';
  var gradeArr = (grade != null && grade != '') ? grade.split(','): '';
  var qualSeqArr = (qualSeq != null && qualSeq != '') ? qualSeq.split(','): '';
  var obj = [];
  for (i = 0; i < qualIdArr.length; i += 1) {
    var qual = qualIdArr[i];var sub = subIdArr[i];var grde = gradeArr[i]; var lp = i+1;var qualSeq = qualSeqArr[i];
     if(qual != '' && sub!= ''){  
        tmp = {
                "qualId": qual,
                "subId": sub,
                "grade": grde,
                "qualSeqId": qualSeq,
                "subSeqId": lp
              };
             obj.push(tmp);
    }
  }
  return obj;
}
function getUcasPoints(subId, frm, removeSubject){
  var pageName = jq('#page-name').val();
  if(pageName == "srGradeFilterPage") {
  var subVal = jq('#'+subId).val();
  var maxUcasPoints = jq('#ucasMaxPoints').val();
  var action = 'CALCULATE_UCAS_POINTS';
  var qualSubListVal = createJSON();
  if((qualSubListVal.length > 0) || (subVal != null && subVal != "") || (isNotNullAnddUndef(subVal) && frm == 'GRADE') || ('REMOVE_SUBJECT' == removeSubject) || ('REMOVE_QUAL' == removeSubject) || ('CHANGE_QUAL' == removeSubject)){
    var stringJson =	{
    "action": action,
    "qualSubList": createJSON()
  };
  var jsonData = JSON.stringify(stringJson);
  var param = "?action="+action;
  var url = contextPath + "/uni-course-requirements-match.html" + param;
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    dataType: 'json',
    type: "POST",
    data: jsonData,
    async: false,
    success: function(response){
    },
    complete: function(response){
    if(response.responseText.indexOf("##SPLIT##")>-1){
      var result = response.responseText.split("##SPLIT##")[1];
      var res = isNotNullAnddUndef(result) ? result :"0";
      jq('#ucasPt').text(result);
      jq('#userUcasScore').val(result);
      updateSlider(res);
      var ucasArrowChnge = ((parseInt(result))/maxUcasPoints * 100) - 2;
      if(parseInt(ucasArrowChnge) <= 100) {
        var ucasArrowPtr = ucasArrowChnge;
      } else {
        var ucasArrowPtr = 100- 2;
      }
      jq('#ucasArrow').attr('style',"left:" + ucasArrowPtr+"%");
    }
    }
  });
  } else {
    sliderUpdate();
  }
}
}
function ucasArrow(result) {
  var ucasPoints = jq('#ucasPt').text().trim();
  var ucasScore = parseInt(ucasPoints);
  var maxUcasPoints = jq('#ucasMaxPoints').val();
  var minScore = jq('#minSliderVal').text().trim();
  var maxScore = jq('#maxSliderVal').text().trim();
  var ucasArrowChnge = ((parseInt(ucasPoints))/maxUcasPoints * 100) - 2;
  if(parseInt(ucasArrowChnge) <= 100) {
    var ucasArrowPtr = ucasArrowChnge;
  } else {
    var ucasArrowPtr = 100- 2;
  }
  jq('#ucasArrow').attr('style',"left:" + ucasArrowPtr+"%");
  var ucasPt = isNotNullAnddUndef(ucasPoints) ? ucasPoints :"0";
  jq("#cmm_rngeSlider").ionRangeSlider({
    type: "double",
    grid: true,
    min: 0,
    max: maxUcasPoints,
    from: minScore,
    to: maxScore,
    prefix: "",
    onChange: function getMinMaxSliderVal(data) {
      var minVal = data.from;
      var maxVal = data.to;
      jq('#minSliderVal').text(minVal);
      jq('#maxSliderVal').text(maxVal);
    },
    onUpdate: function getMinMaxSliderVal(data) {
    var minVal = data.from;
    var maxVal = data.to;
    jq('#minSliderVal').text(minVal);
    jq('#maxSliderVal').text(maxVal);
  }
  });
}
function updateSlider(result) {
  var res = isNotNullAnddUndef(result) ? result : 0;
  var ucasPoints = jq('#ucasPt').text().trim();
  var ucasScore = parseInt(ucasPoints);
  var slider = jq("#cmm_rngeSlider").data("ionRangeSlider");
    slider.update({
    from: 0,
    to: res,
    onChange: function getMinMaxSliderVal(data) {
      var minVal = data.from;
      var maxVal = data.to;
      jq('#minSliderVal').text(minVal);
      jq('#maxSliderVal').text(maxVal);
    }
  });
  ucasArrow(res);
}
function sliderUpdate() {
  updateSlider();
  jq('#ucasPt').text('0');
  var ucasArrowPtr = "-2";
  jq('#ucasArrow').attr('style',"left:" + ucasArrowPtr+"%"); 
}
function removeQualification(qualDivId, preQualDivCnt, addQualBtnId){
  jq('#'+addQualBtnId).hide();
  jq('#'+qualDivId).hide();
  jq('#addQualBtn_'+preQualDivCnt).show();
  var qualloop = preQualDivCnt+1;
  jq("#qualsel_"+qualloop).val("1");
  if(jq('#level_3_qual_1').css('display') == 'none' && jq('#level_3_qual_2').css('display') == 'none'){
    jq('#addQualBtn_0').show();
    jq('#addQualBtn_1').hide();
  }
  appendQualDiv(1, '', qualloop, 'new_entry_page', 'add_Qual');  
}
function addQualification(qualDivId, nxtQualDivCnt, addQualBtnId){
  jq('#'+addQualBtnId).hide();
  jq('#'+qualDivId).show();
  jq('#addQualBtn_'+ nxtQualDivCnt).show();
  var qualloop = nxtQualDivCnt+1;
  if(jq('#level_3_qual_'+ qualloop).css('display') == 'block'){
    jq('#addQualBtn_'+ nxtQualDivCnt).hide();
  }  
  jq("#qualsel_"+nxtQualDivCnt).val("1");
  appendQualDiv(1, '', nxtQualDivCnt, 'new_entry_page', 'add_Qual');
  if(jq('#level_3_qual_'+nxtQualDivCnt).css('display')!='none' && jq('[data-id=level_3_add_qualif]').eq(1).attr('id') == ('level_3_qual_'+nxtQualDivCnt)){
    var curAddQualCont =  jq("[data-id=level_3_add_qualif]").eq(1);    
    jq("[data-id=level_3_add_qualif]").last().after(curAddQualCont);      
  }
}
function removeQualEntryReq(rmQualId, qualNo){
  jq('#qualspan_'+qualNo).text('A Level');
  jq('#qualsel_'+qualNo).val('1');
  appendQualDiv(1, '', qualNo, '', 'add_Qual');  
  var preQualNo = 0;
  if(qualNo != 0){
	preQualNo = parseInt(qualNo)-1;
  }
  jq('#'+rmQualId).hide();
  if(qualNo != 0){//restricting the showing of add qual btn for qual id 0 since we are showing dynamically below for firs position qual
    jq('#addQualBtn_'+ preQualNo).show();  
  }
 
 
  
  //3 qualifications scenario
  manageAddQualBtn();  
  
  //Adding add qual button for 0th qualification first postion while removing it
  if(qualNo == 0){
	  if( jq('#addQualBtn_2').length){
 		 jq('#addQualBtn_2').remove();
 	   } 
	 // jq('#addQualBtn_'+qualNo).remove();
	  jq('.ucas_mid').find('[data-id=level_3_add_qualtxt]').hide();
	  var addQualBtnDiv = '<div class="add_qualtxt" data-id="level_3_add_qualtxt" id="addQualBtn_2" style="display: block;"><a href="javascript:void(0);" onclick="addQualEntryReq('+(parseInt(qualNo))+')" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> Add a qualification</a></div></div></div>';
	  jq('.ucas_mid').find('[data-id=level_3_add_qualtxt]').last().after(addQualBtnDiv);
  }
  var count = 0;
  var qual3DpLen = jq('[data-id=level_3_add_qualif]:visible');
  for (count = 0; count < qual3DpLen.length;count++){     
	  count = count;
  }
  if(count == 0){
	jq("#cmm_wrap_id").hide();
	jq('#loadingImg').show();
	deleteUserRecords();	
  }
  
}
//
function addQualEntryReq(qualNo){
  var entryReyQualCnt = jq('[data-id=level_3_add_qualif]').length;
  if(entryReyQualCnt < 3 && jq('#level_3_qual_'+ qualNo).length == 0){
    var addQualResult = '';
    var addQualDiv = '';
    var addQUalDivEnd = ''; 
    var level_3_qual_id = "'level_3_qual_"+qualNo+"'";
    var l3q_rw_id = "'qual_level_"+ qualNo +"'";
    var add_qual_no = "'"+qualNo+"'";
    var add_qual_sel_id = "qualsel_"+qualNo;
    var emp_str = "''";    
    var nxt_level_3_qual_id = "'level_3_qual_"+(parseInt(qualNo)+1)+"'";
    var qualSelOption = jq('#qualsel_0').html();
    var qualSubGrdHidId = jq('#qual_sub_grd_hidId').html();
    if(qualSubGrdHidId.indexOf('_1_') > -1){
      qualSubGrdHidId = qualSubGrdHidId.replace(/_1_/g, '_'+qualNo+'_');
    }else if(qualSubGrdHidId.indexOf('_2_') > -1){
      qualSubGrdHidId = qualSubGrdHidId.replace(/_2_/g, '_'+qualNo+'_');	
    }else{
      qualSubGrdHidId = qualSubGrdHidId.replace(/_0_/g, '_'+qualNo+'_');
    }
    
    addQualDiv = 
                '<div class="adqual_rw" data-id="level_3_add_qualif" id="level_3_qual_'+qualNo+'"><div class="ucas_row"><h5 class="cmn-tit txt-upr">Qualifications</h5><fieldset class="ucas_fldrw">'
               + '<div class="remq_cnt"><div class="ucas_selcnt"><fieldset class="ucas_sbox"><span class="sbox_txt" id="qualspan_'+qualNo+'">A Level </span><i class="fa fa-angle-down fa-lg"></i></fieldset>'
               + '<select name="qualification'+qualNo+'" onchange="appendQualDiv(this, '+emp_str+', '+add_qual_no+');" id="'+add_qual_sel_id+'" class="ucas_slist">'
               + qualSelOption 
               + '</select>'
               + qualSubGrdHidId
               + '</div></div><div class="add_fld" id ="removeQualbtn_'+qualNo+'" style="display:block;"><a id="remQual_'+qualNo+'" onclick="removeQualEntryReq('+level_3_qual_id+', '+add_qual_no+');" class="btn_act1 bck f-14 pt-5">'
               + '<span class="hd-mob rq_mob">Remove Qualification</span></a></div></fieldset><div class="err" id="qualSubId_'+qualNo+'_error" style="display:none;"></div></div>'								
               + '<div class="subgrd_fld" id="subGrdDiv'+qualNo+'"></div>'
               + '<input type="hidden" class="total_countAddSubj_'+qualNo+'" id="total_countAddSubj_'+qualNo+'" value="3"><input type="hidden" class="countAddSubj_'+qualNo+'" id="countAddSubj_'+qualNo+'" value="3">'
   addQUalDivEnd = '</div>';     
   if((parseInt(qualNo)+1) <= 3){
     var addQualNo = qualNo;
     if((parseInt(qualNo)+1) == 3){addQualNo = 0;}            
       if(parseInt(qualNo) == 2){
    	   if( jq('#addQualBtn_'+qualNo).length){
    		 jq('#addQualBtn_'+qualNo).remove();
    	   }    	   
    	   var addQualBtnDiv = '<div class="add_qualtxt" data-id="level_3_add_qualtxt" id="addQualBtn_'+qualNo+'" style="display: block;"><a href="javascript:void(0);" onclick="addQualEntryReq('+(parseInt(addQualNo))+')" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> Add a qualification</a></div></div></div>';
           addQUalDivEnd = addQUalDivEnd;
       }else{
    	   var addQualBtnDiv = '<div class="add_qualtxt" data-id="level_3_add_qualtxt" id="addQualBtn_'+qualNo+'" style="display: block;"><a href="javascript:void(0);" onclick="addQualEntryReq('+(parseInt(addQualNo)+1)+')" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> Add a qualification</a></div></div></div>';
           addQUalDivEnd = addQUalDivEnd;   
       }
       
     }
     addQualResult = addQualDiv + addQUalDivEnd;
     jq('.ucas_mid').find('[data-id=level_3_add_qualif]').last().after(addQualResult);
     jq('.ucas_mid').find('[data-id=level_3_add_qualtxt]').last().after(addQualBtnDiv);
     var preQualCnt = parseInt(qualNo) - 1;
     jq('#addQualBtn_'+preQualCnt).hide();
     if((parseInt(qualNo)+1) > 2){
       jq('#addQualBtn_'+qualNo).hide();
     }
     appendQualDiv(1, '', qualNo, '', 'add_Qual');
     jq('#qualspan_'+qualNo).text('A Level');
     jq('#qualsel_'+qualNo).val('1');     
  } else{   
    if(parseInt(qualNo) < 3){
      if(parseInt(qualNo) != 0){
    	  jq('#level_3_qual_'+ qualNo).show();
          jq('#addQualBtn_'+ qualNo).show();
          jq('#addQualBtn_'+ (parseInt(qualNo)-1)).hide();  
      }else if(parseInt(qualNo) == 0){
    	//3 qualifications scenario
    	  jq('#level_3_qual_'+ qualNo).show();
    	  if(jq('#level_3_qual_2').css('display') == 'block' && jq('#level_3_qual_0').css('display') == 'none'){
    		jq('#addQualBtn_2').show(); 
    	  } 
    	  if(jq('#level_3_qual_2').css('display') == 'block' && jq('#level_3_qual_1').css('display') == 'none'){
    	    jq('#addQualBtn_2').show();
    	    jq('#addQualBtn_0').hide();
    	  }
    	  if(jq('#level_3_qual_1').css('display') == 'block' && jq('#level_3_qual_0').css('display') == 'block'){
    		jq('#addQualBtn_0').hide();
    		jq('#addQualBtn_2').hide();
    	    jq('#addQualBtn_1').show();
    	  }  
    	  if(jq('#level_3_qual_2').css('display') == 'none' && jq('#level_3_qual_1').css('display') == 'none'){
    	    jq('#addQualBtn_0').show();
    	    jq('#addQualBtn_2').hide();
    	    jq('#addQualBtn_1').hide();
    	  }
    	  if(jq('#level_3_qual_0').css('display') == 'block' && jq('#level_3_qual_1').css('display') == 'block' && jq('#level_3_qual_2').css('display') == 'block' ){
      	    jq('#addQualBtn_0').hide();
      	    jq('#addQualBtn_2').hide();
      	    jq('#addQualBtn_1').hide();
      	  }
    	  manageAddQualBtn();
    	  var curAddQualCont =  jq("[data-id=level_3_add_qualif]").eq(0);
          jq("[data-id=level_3_add_qualif]").last().after(curAddQualCont); 
      }	
      
    } else if(parseInt(qualNo) == 3){
      var addShowQualNo = 1;
      jq('#level_3_qual_'+ addShowQualNo).show();
      jq('#addQualBtn_'+ (parseInt(qualNo)-1)).hide();
      qualNo = addShowQualNo;
    }
    if(entryReyQualCnt == 3){
      if(jq('#level_3_qual_1').css('display') == 'block'){
        jq('#addQualBtn_2').hide();
      }
      if(jq('#level_3_qual_2').css('display') == 'block'){
        jq('#addQualBtn_1').hide();
      }
      if(jq('#level_3_qual_'+qualNo).css('display')!='none' && jq('[data-id=level_3_add_qualif]').eq(1).attr('id') == ('level_3_qual_'+qualNo)){
        var curAddQualCont =  jq("[data-id=level_3_add_qualif]").eq(1);
        jq("[data-id=level_3_add_qualif]").last().after(curAddQualCont);      
      }
    }
  }  
}
function onloadShowingAddQualBtn(){
  var l3QualCnt = jq('[data-id=level_3_add_qualif]').length;
  var addQualbtnCnt = jq('[data-id=level_3_add_qualif]').find('.add_qualtxt').length;
  if(l3QualCnt!= 3){
    jq('[data-id=level_3_add_qualtxt]').last().show();     
  } else if(l3QualCnt == 3){
    jq('.add_qualtxt').hide();
  }  
}
function getMatchedCourse() {
  var qualSelId = ''; var qual = '';var sciErrFlag = false; var ucsErrFlag = false;
  var subId = ''; 
  jq('[data-id=level_3]').each(function(){  
	  var curSubId = this.id;
	  if(this.value != '') {//This condition added for science practical error qualification-Jeya 08Aug2019
	    subId += this.value + '';
	    var qualLoop = curSubId.substring(curSubId.indexOf('_')+1, curSubId.lastIndexOf("_"));
	    qualSelId += jq('#qualsel_'+qualLoop).val() +',';
	  }
  });
  if(qualSelId.indexOf('19') > -1) {
	ucsErrFlag = showUcasPtVal();
  }
  if(qualSelId.indexOf('18') > -1) {
    sciErrFlag = true;
    qualSelId = qualSelId.substring(0, qualSelId.length - 1);
    var qualIdArr = qualSelId.split(',');
    if(qualIdArr == '18') {
     sciErrFlag = true;
    } else {
    for (i = 0; i < qualIdArr.length; i += 1) {
      if(qualIdArr[i] == '1' || qualIdArr[i] == '2' || qualIdArr[i] == '3' || qualIdArr[i] == '4') {
        sciErrFlag = false;
      }
    }
  }}
  var kwSubId = '';
  var kwErrFlag = false;
  var multiQualErrFlag = false;
  var emptySubFlag = false;
  jq('[data-id=qual_sub_id]').each(function(){  
    var qualSelId = "";
    var curSubId = this.id;
    kwSubId += this.value + '';  
    var curHidId = curSubId.replace('qualSub_','sub_');    
    var curSubVal = jq('#'+curSubId).val();
    var curHidVal = jq('#'+curHidId).val();
    if(curSubVal!='' && curHidVal ==''){
     kwErrFlag = true;         
    }
  });
  if(kwErrFlag){
	  jq('#pId').html("Please enter the qualification");
	  jq('#errorDiv').show();
  } else if(!isNotNullAnddUndef(subId)) { 
	 jq('#pId').html("Please enter the qualification");
     jq('#errorDiv').show();
     emptySubFlag = true;
  }
  var selVal = [];
  for (var i = 0; i < 3; i++) {
    if(jq('#qualspan_'+i).is(':visible')){
      selVal.push(jq('#qualspan_'+i).text().trim());   
    }	
  }
  if (selVal.length > 1) {
	for (var i = 0; i < selVal.length; i++) {  
	  for (var j = i+1; j < selVal.length; j++) {
		if(selVal[i] == selVal[j]) {
		  multiQualErrFlag = true;
		 }
	  }
	}
  }
  if(sciErrFlag) {
	jq('#pId').html("You need to add an A-Level Science qualification to accompany your Science Practical grade. Please add your A-Level science grade to continue"); 
	jq('#errorDiv').show();
  }  
  else if(multiQualErrFlag) {
 	jq('#pId').html("Oops, looks like you have entered the same qualification type twice. Please ensure you add each qualification type once."); 
 	jq('#errorDiv').show();
  }else if(kwErrFlag || emptySubFlag || ucsErrFlag){
	  // do nothing
  } else {
	jq('#errorDiv').hide();
    var action = 'QUAL_MATCHED_COURSE';
    var ucasPoints = jq('#ucasPt').text().trim();
    var minUcas = jq('#minSliderVal').text().trim();
    var maxUcas = jq('#maxSliderVal').text().trim();
    var ucasPt = minUcas + ',' + maxUcas;
    var subject = jq('#subject').val();
    var q = jq('#q').val();
    var university = jq('#university').val();
    var qualCode = jq('#qualCode').val();
    var loc1 = isNotNullAnddUndef(jq('#country_hidden').val()) ? jq('#country_hidden').val() : '';
//
    var postcode = jq('#postcode').val();
    var studyMode = jq('#study-mode').val();
    var locationType = jq('#location-type').val();
    var campusType = jq('#campus-type').val();
    var module = jq('#module').val();
    var rf = jq('#rf').val();
    var empRateMax = jq('#empRateMax').val();
    var empRateMin = jq('#empRateMin').val();
    var russellGroup = jq('#russellGroup').val();
    var yourPref = jq('#your-pref').val();
    var jacs = jq('#jacs').val();
    var distance = jq('#distance').val();
    var university = jq('#university').val();
    var qualCode = jq('#qualCode').val();
    var pageName = jq('#page-name').val();
    var empRate = jq('#empRate').val();
    var ucasCode = jq('#ucas-code').val();
    var assessmentType = jq('#assessment-type').val();
  //
    var userUcasScore = jq('#userUcasScore').val();
    var eventLblVal = ucasPoints + "|" + minUcas + "|" + maxUcas;
    qualGALog();
    gaEventLog('Qualification Filter', 'UCAS Range', eventLblVal);    
    viewCourseGAlog();
    var param = "?action="+action + "&ucasPoints=" + ucasPt +"&subject=" +subject +"&location="+loc1+"&q=" +q +"&university="+university+"&qualCode="+qualCode+"&userUcasScore="+userUcasScore;
    param += !isBlankOrNullString(module) ? "&module=" + module.trim() : "";
	param += !isBlankOrNullString(rf) ? "&rf=" + rf.trim() : "";
	param += !isBlankOrNullString(postcode) ? "&postcode=" + postcode.trim() : "";
	param += !isBlankOrNullString(studyMode)? "&study-mode=" + studyMode.trim() : "";
	param += !isBlankOrNullString(locationType) ? "&location-type=" + locationType.trim() : "";
	param += !isBlankOrNullString(campusType) ? "&campus-type=" + campusType.trim() : "";
	param += !isBlankOrNullString(empRateMax) ? "&employment-rate-max=" + empRateMax.trim() : "";
	param += !isBlankOrNullString(empRateMin) ? "&employment-rate-min=" + empRateMin.trim() : "";
	param += !isBlankOrNullString(russellGroup) ? "&russell-group=" + russellGroup.trim() : "";
	param += !isBlankOrNullString(yourPref) ? "&your-pref=" + yourPref.trim() : "";
	param += !isBlankOrNullString(jacs) ? "&jacs=" + jacs.trim() : "";
	param += distance != "25" ? "&distance=" + distance.trim() : "";
	param += !isBlankOrNullString(university) ? "&university=" + university.trim() : "";
	//param += !isBlankOrNullString(qualCode) ? "&qualCode=" + qualCode.trim() : "";
	//param += !isBlankOrNullString(pageName) ? "&page-name=" + pageName.trim() : "";
	//param += !isBlankOrNullString(empRate) ? "&empRate=" + empRate.trim() : "";
	//param += !isBlankOrNullString(ucasCode) ? "&ucas-code=" + ucasCode.trim() : "";
	//param += !isBlankOrNullString(assessmentType) ? "&assessment-type=" + assessmentType.trim() : "";
    var url = contextPath + "/uni-course-requirements-match.html" + param;
    jq('#loadingImg').show();
    jq.ajax({
      url: url, 
      headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
      type: "POST",
      async: false,
      success: function(response){
    },
    complete: function(response){
    if(response.responseText.indexOf("##SPLIT##")>-1){
      var result = response.responseText.split("##SPLIT##")[1];
      var resId = 'gradeFilterRevLightBox';
      var loc = "/jsp/gradefilter/gradeFilterLiBox.jsp?coursecount=" + result;
      var url = contextPath + loc;
      jq.ajax({
        url: url, 
        type: "POST", 
        success: function(result){
        jq('#loadingImg').hide();
        jq("#"+resId).html(result);
        lightBoxCall();
        revLightbox();
      }
    });
  }
  }
 });
 }
}
function saveAndContinue() {
 var qualSelId = ''; var qual = '';var sciErrFlag = false;var ucsErrFlag = false;
  var subId = ''; jq('[data-id=level_3]').each(function(){  
  var curSubId = this.id;
  if(this.value != '') {//This condition added for science practical error qualification-Jeya 08Aug2019
    subId += this.value + '';
    var qualLoop = curSubId.substring(curSubId.indexOf('_')+1, curSubId.lastIndexOf("_"));
    qualSelId += jq('#qualsel_'+qualLoop).val() +',';
  }
  });
  if(qualSelId.indexOf('19') > -1) {
	ucsErrFlag = showUcasPtVal();
  }
  if(qualSelId.indexOf('18') > -1) {
    sciErrFlag = true;
    qualSelId = qualSelId.substring(0, qualSelId.length - 1);
    var qualIdArr = qualSelId.split(',');
    if(qualIdArr == '18') {
     sciErrFlag = true;
    } else {
    for (i = 0; i < qualIdArr.length; i += 1) {
      if(qualIdArr[i] == '1' || qualIdArr[i] == '2' || qualIdArr[i] == '3' || qualIdArr[i] == '4') {
        sciErrFlag = false;
      }
    }
  }}
  var kwSubId = '';
  var kwErrFlag = false;
  var multiQualErrFlag = false;
  var emptySubFlag = false;
  jq('[data-id=qual_sub_id]').each(function(){  
    var qualSelId = "";
    var curSubId = this.id;
    kwSubId += this.value + '';  
    var curHidId = curSubId.replace('qualSub_','sub_');    
    var curSubVal = jq('#'+curSubId).val();
    var curHidVal = jq('#'+curHidId).val();
    if(curSubVal!='' && curHidVal ==''){
     kwErrFlag = true;         
    }
  });
  if(kwErrFlag){
	  jq('#pId').html("Please enter the qualification");
	  jq('#errorDiv').show();
  } else if(!isNotNullAnddUndef(subId)) { 
	 jq('#pId').html("Please enter the qualification");
     jq('#errorDiv').show();
     emptySubFlag = true;
  }
  var selVal = [];
  for (var i = 0; i < 3; i++) {
    if(jq('#qualspan_'+i).is(':visible')){
      selVal.push(jq('#qualspan_'+i).text().trim());   
    }	
  }
  if (selVal.length > 1) {
	for (var i = 0; i < selVal.length; i++) {  
	  for (var j = i+1; j < selVal.length; j++) {
		if(selVal[i] == selVal[j]) {
		  multiQualErrFlag = true;
		 }
	  }
	}
  }
  if(sciErrFlag) {
	jq('#pId').html("You need to add an A-Level Science qualification to accompany your Science Practical grade. Please add your A-Level science grade to continue"); 
	jq('#errorDiv').show();
  }  
  else if(multiQualErrFlag) {
 	jq('#pId').html("Oops, looks like you have entered the same qualification type twice. Please ensure you add each qualification type once."); 
 	jq('#errorDiv').show();
  }else if(kwErrFlag || emptySubFlag || ucsErrFlag){
	  // do nothing
  } else {
	jq('#errorDiv').hide();	
	  var action = 'SAVE_REDIRECT';
	  var pageName = jq('#page-name').val();
	  
	  var stringJson = {
	    "action": action,
	    "qualSubList": createJSON()
	  };
	  var ucasPoints = jq('#ucasPt').text().trim();
	  var minUcas = jq('#minSliderVal').text().trim();
	  var maxUcas = jq('#maxSliderVal').text().trim();
	  var srScore = minUcas +","+maxUcas;
	  var subject = jq('#subject').val();
	  var q = jq('#q').val();
	  var jsonData = JSON.stringify(stringJson);
	  var param = "?action="+action;
	  var url = contextPath + "/uni-course-requirements-match.html" + param;  
	  jq.ajax({
	    url: url, 
	    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
	    dataType: 'json',
	    type: "POST",
	    data: jsonData,
	    async: false,
	    complete: function(response){
	    if(!(isBlankOrNullString(pageName)) && (pageName == "srGradeFilterPage")) {
	    	var score = "&score="+srScore;
	    	var srUrl = srPageUrlFormation();		
	    	srCloseLightbox(srUrl + score, "save");
	    }else if(!(isBlankOrNullString(pageName)) && (pageName == "mywhatuni")){
	    	var myProfileUrl = srPageUrlFormation();	
	    	srCloseLightbox(myProfileUrl, "save");
	    }else{
	      if(isNotNullAnddUndef(subject)){
	    	srCloseLightbox("/degree-courses/search?subject="+subject+"&score="+srScore);
	      } else if(isNotNullAnddUndef(q)){
	    	srCloseLightbox("/degree-courses/search?q="+q+"&score="+srScore);
	      }
	    }
	   }
	  });
  }
}
  jq(document).mouseup(function (e) {
   ajaxOptHide(e);
});
function ajaxOptHide(e){
  var container = jq('[data-id=wugoSubAjx]');
  jq.each(container, function(key, value) {
  if (!jq(value).is(e.target) // if the target of the click isn't the container...
     && jq(value).has(e.target).length === 0) // ... nor a descendant of the container
    {
     if(jq(value).is(':visible')){
       var fldId = jq(value).attr('id').replace('ajaxdiv', 'qualSub');
       var obj = jq('#'+fldId).get(0);
       onblurkwCheck(obj);
       jq(value).hide();
     }
   }
  });
}
jq(document).ready(function() {
	var root = window.parent.document.getElementsByTagName('html')[0];     
    root.className += ' scrl_dis';
  if(jq('#page-name') == "srGradeFilterPage"  ){
    var oldonkeydownWugo = document.body.onkeydown;
    if(typeof oldonkeydownWugo != 'function'){
      document.body.onkeydown=wugoAjaxOptionNavi;
    } else{
      document.body.onkeydown=function(){
      oldonkeydownWugo();
      wugoAjaxOptionNavi();
      }	
    }
  }
});
function wugoAjaxOptionNavi(e){
  var ajaxOptionObj = jq('#ajax_listOfOptions').get(0);
  var ajaxListActiveItem = jq('#ajax_listOfOptions').find('.optionDivSelected').get(0);
  if(document.all)e = event;
    if(!ajaxOptionObj){
      return;
    }
    if(ajaxOptionObj.style.display == 'none'){
      return;
    }
    if(e.keyCode == 38){
       if(!ajaxListActiveItem){
         return;
       }
       if(ajaxListActiveItem && !ajaxListActiveItem.previousElementSibling){
         return;
       }
       if(ajaxListActiveItem.previousElementSibling){
         ajaxListActiveItem.className = 'optionDiv';
         ajaxListActiveItem.previousElementSibling.className = 'optionDivSelected';
       }
     }
     if(e.keyCode == 40){
       if(!ajaxListActiveItem){
         var firstOptDiv = jq('#ajax_listOfOptions').find('.optionDiv').first().get(0);
         firstOptDiv.className = 'optionDivSelected';
       }else{
          if(!ajaxListActiveItem.nextSibling){
            return;
	  }      
	  if(ajaxListActiveItem.nextElementSibling){
            ajaxListActiveItem.className = 'optionDiv';
            ajaxListActiveItem.nextElementSibling.className = 'optionDivSelected';
          }
       }
     }
     if(e.keyCode == 13 || e.keyCode == 9){
       if(ajaxListActiveItem && ajaxListActiveItem.className == 'optionDivSelected'){
       var selOptObj = jq('#ajax_listOfOptions').find('.optionDivSelected').get(0);
       setAjaxSubjectVal(selOptObj);
    }
    return true;
  }
  if(e.keyCode == 27){
    ajaxOptHide(e);
  }
}
function gaEventLog(eventCategory, eventAction, eventLabel){
  if(isNotNullAnddUndef(eventLabel)){
    GANewAnalyticsEventsLogging(eventCategory, eventAction, eventLabel);
  }
}
function GANewAnalyticsEventsLogging(eventCategory, eventAction, eventLabel){
eventTimeTracking();
ga('send', 'event', eventCategory , eventAction , eventLabel); //Added by Amir for UA logging for 24-NOV-15 release 
}
function qualGALog(){
  var qualDes = [];
  var subDes = [];   
  var qual3DpLen = jq('[data-id=level_3_add_qualif]:visible');
  var qual2DpLen = jq('[data-id=level_2_add_qualif]:visible');
  for (var ql = 0; ql < qual3DpLen.length;ql++){     
   var lvl3SubIpLen = jq('[data-id=level_3_add_qualif]').eq(ql).find('[data-id=qual_sub_id]')
   qualDes[ql] = jq('#qualspan_'+ql).text();
   subDes[ql] = '';
   for(var sb = 0; sb < lvl3SubIpLen.length ; sb++){
     var subIpId = lvl3SubIpLen.eq(sb).attr('id');
     var subValIpId = subIpId.replace('qualSub','');
     var subNameVal = jq('#'+subIpId).val();
     var subIdVal  = jq('#sub'+subValIpId).val();
     var grdIdVal = jq('#grde'+subValIpId).val();
     if(subNameVal!= '' && subIdVal != '' && grdIdVal != ''){
       if(sb != lvl3SubIpLen.length - 1){
         subDes[ql] = subDes[ql] + subNameVal + '|' + grdIdVal + '|' ;
       } else{
         subDes[ql] = subDes[ql] + subNameVal + '|' + grdIdVal;
       }
     }
   }
   gaEventLog('Qualification Filter', qualDes[ql], subDes[ql]);
  }
  if(qual2DpLen.length > 0){
    var lvl2SubIpLen = jq('[data-id=level_2_add_qualif]').find('[data-id=qual_sub_id]');
    qualDes[0] = 'GCSE';
    subDes[0] = '';
    for(var sb = 0; sb < lvl2SubIpLen.length ; sb++){
      var subIpId = lvl2SubIpLen.eq(sb).attr('id');
      var subValIpId = subIpId.replace('qualSub','');
      var subNameVal = jq('#'+subIpId).val();
      var subIdVal  = jq('#sub'+subValIpId).val();
      var grdIdVal = jq('#grde'+subValIpId).val();
      if(subNameVal != '' && subIdVal != '' && grdIdVal != ''){
        if(sb != lvl2SubIpLen.length - 1){
          subDes[0] = subDes[0] + subNameVal + '|' + grdIdVal + '|';
        } else{
          subDes[0] = subDes[0] + subNameVal + '|' + grdIdVal;
        }
      }
    }
    gaEventLog('Qualification Filter', qualDes[0], subDes[0]);
  }
}  
function skipAndViewResults() {
	//var score = !isBlankOrNullString(jq('#score').val()) ? jq('#score').val() : "";
	var param = srPageUrlFormation();
	srCloseLightbox(param);
   
}
var inputIDS = [];
function onblurkwCheck(obj){     
  var kwErrFlag = false;
  jq('[data-id=qual_sub_id]').each(function(){             
    var curSubId = this.id
    var curHidId = curSubId.replace('qualSub_','sub_');
    var gradeDpId =  curSubId.replace('qualSub_','qualGrd_');
    var curSubVal = jq('#'+curSubId).val();
    var curHidVal = jq('#'+curHidId).val();
    if(curSubVal!='' && curHidVal ==''){
      kwErrFlag = true;
      inputIDS[curSubId] = 'Y';      
    } else{
      inputIDS[curSubId] = 'N';
    }
    if(kwErrFlag&&inputIDS[curSubId]=='Y'&& obj.id == curSubId){      
      jq(obj).addClass('faild');
      var errId = obj.id.substring(8);
      var texterr = "<p>Please enter the subject</p>";
      if(kwErrFlag){  jq('#err_msg_'+errId).html(texterr);
	  jq('#err_msg_'+errId).show();
      } 
    }
  });
}  
function srCloseLightbox(searchURL, param){   
  var urlFormation = isBlankOrNullString(searchURL)? srPageUrlFormation() : searchURL;
  var score = jq('#srScore').val();
  var scoreParam = !isBlankOrNullString(score) ? "&score=" + score : "";
  var root = window.parent.document.getElementsByTagName( 'html')[0];   
  root.className -= 'scrl_dis'; 
  //var referralUrl = jq('#referralUrl').val();
  var width = window.screen.width;
  if(width <= 768) {
	  //
      if(window.parent.document.getElementById("loaderTopNavImg")){
        window.parent.document.getElementById("loaderTopNavImg").style.display = "block";	
      }        
      // 
      if(isNullAndUndef(urlFormation)) {
        if(param=="save"){
          window.location.href = urlFormation;
        } else {
          window.parent.location.href = urlFormation + scoreParam; 
        }
      } /*else { 
        if(isNotNullAnddUndef(referralUrl)) {
          window.location.href = referralUrl;
         } else { 
           window.history.go(-1); 
         }
      }*/
  } else {                
	if(isNullAndUndef(urlFormation)) { 
	  window.parent.document.getElementById("fade_NRW").style.display = "none";
	  window.parent.document.getElementById("holder_NRW").style.display = "none";
	  if(window.parent.document.getElementById("loaderTopNavImg")){
        window.parent.document.getElementById("loaderTopNavImg").style.display = "block";
      }
	  if(searchURL == ""){
		  window.parent.location.href = urlFormation + scoreParam; 	
	  }else{
	    window.parent.location.href = urlFormation; 	
	  }
	} else {
	  window.parent.document.getElementById("fade_NRW").style.display = "none";
	  window.parent.document.getElementById("holder_NRW").style.display = "none";
	  if(window.parent.document.getElementById('iframe_NRW').contentWindow.document.getElementById("chatbotWidget")){
	    window.parent.document.getElementById('iframe_NRW').contentWindow.document.getElementById("chatbotWidget").innerHTML = "";
	  }   
	}
  }
}
function deleteUserRecords() {
  var action = 'DELETE_RECORD';
  var pageName = jq('#page-name').val();

  var stringJson = {
    "action": action,
  };
  var jsonData = JSON.stringify(stringJson);
  var ajaxParam = "?action="+action + "&delPageName="+pageName;
  var url = contextPath + "/uni-course-requirements-match.html" + ajaxParam;
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    dataType: 'json',
    type: "POST",
    data: jsonData,
    async: false,
    success: function(response){
    },
    complete: function(response){
    if(response.responseText.indexOf("##SPLIT##")>-1){
      var result = response.responseText.split("##SPLIT##")[1];
      var pagePath = "/degrees/uni-course-requirements-match.html";
      var param = "";
      param = srPageUrlFormation();
      var qryParam = param.split('?')[1] != undefined ? '?'+param.split('?')[1]  : '';  
      
      if(pageName == "srGradeFilterPage"){
        qryParam += "&pageName="+pageName;
      }else if(pageName == "mywhatuni"){
    	qryParam += "?pageName="+pageName; 
      }
      
      var finUrl = pagePath + qryParam;
      var width = window.screen.width;
      if (width <= 768) {
    	   if(pageName )
    	    window.location.assign(finUrl);
      }else{
    	  var iframe = window.parent.document.getElementById('iframe_NRW');          
          iframe.src = finUrl;  
      }      
    }
    }
  });
}
 function viewCourseGAlog(){
	 var optVal=[];
	 var selVal = [];
	 var qualDes = [];
	 var subDes = [];
	 jq('#qualsel_0 option').each(function(){		 
		 if(jq(this).text().trim() != 'Please Select'){
		   optVal.push(jq(this).text().trim());
		 }
	 });	 
	 var qual3DpLen = jq('[data-id=level_3_add_qualif]:visible');
	 for (var ql = 0; ql < qual3DpLen.length;ql++){   
   		 
		 var lvl3SubIpLen = qual3DpLen.eq(ql).find('[data-id=qual_sub_id]')
   	     var qualSelBxId = ((qual3DpLen.eq(ql)).attr('id')).replace('level_3_qual','qualspan');
		 qualDes[ql] = jq('#'+qualSelBxId).text();
   	     subDes[ql] = '';
	   	 for(var sb = 0; sb < lvl3SubIpLen.length ; sb++){
		   var subIpId = lvl3SubIpLen.eq(sb).attr('id');
		   var subValIpId = subIpId.replace('qualSub','');
		   var subNameVal = jq('#'+subIpId).val();
		   var subIdVal  = jq('#sub'+subValIpId).val();
		   var grdIdVal = jq('#grde'+subValIpId).val();
		   if(subNameVal!= '' && subIdVal != '' && grdIdVal != ''){   	       
			 subDes[ql] = subDes[ql] + subNameVal + '|' + grdIdVal;   	       
		   }		   
	   	 }
	   	 if(isNotNullAnddUndef(subDes[ql]) && isNotNullAnddUndef(qualDes[ql])){
	   	   selVal.push(qualDes[ql].trim());	 
	   	 }   		    		 
	  }  
	 
	 var optValClone = optVal.slice(0);
	 if(selVal.length > 0 && optVal.length > 0){
		 for(var i=0; i<selVal.length;i++){
		   var valSet = false;
		   for(var y=0; y<optVal.length;y++){
		     var optValIdx = jq.inArray(selVal[i],optValClone)	     
		     if(optValIdx !== -1 && !valSet){
		       optVal[optValIdx] = selVal[i]+'|';
		       valSet = true;
		     }else{
		       if(optVal[y].indexOf('|') <= 0){      
		         optVal[y] = '|';	         
		       }      
		     }
		   }
		 }
		 var qualFilter = '';
		 for(var z=0 ; z < optVal.length ; z++){
			 if(optVal[z].indexOf('|') <= 0){      
		         optVal[z] = '|';	         
		     }
		   qualFilter = qualFilter + optVal[z];
		 }
		 var qualGa = qualFilter.slice(0, -1);		 
		 GANewAnalyticsEventsLogging('Qualification Filter', 'Combination', qualGa);
     }
 }
 function manageAddQualBtn(){
	//3 qualifications scenario
	if(jq('#level_3_qual_1').css('display') == 'block' && jq('#level_3_qual_0').css('display') == 'block'){
	  jq('#addQualBtn_0').hide();
	  jq('#addQualBtn_1').show(); 
	} 
	  
	  if(jq('#level_3_qual_0').css('display') == 'block' && jq('#level_3_qual_1').css('display') == 'none'){
	   jq('#addQualBtn_0').show(); 
	   jq('#addQualBtn_1').hide();
	   jq('#addQualBtn_2').hide();
	  } 
	  if(jq('#level_3_qual_0').css('display') == 'none' && jq('#level_3_qual_1').css('display') == 'block'){
		   jq('#addQualBtn_0').hide(); 
		   jq('#addQualBtn_1').hide();
		   jq('#addQualBtn_2').show();
	  } 
	  if(jq('#level_3_qual_2').css('display') == 'block' && jq('#level_3_qual_0').css('display') == 'none'){
		jq('#addQualBtn_2').show(); 
	  } 
	  if(jq('#level_3_qual_2').css('display') == 'block' && jq('#level_3_qual_1').css('display') == 'none'){
	    jq('#addQualBtn_2').show();
	    jq('#addQualBtn_0').hide();
	  }   
	  if(jq('#level_3_qual_2').css('display') == 'none' && jq('#level_3_qual_1').css('display') == 'none'){
	    jq('#addQualBtn_0').show();
	    jq('#addQualBtn_2').hide();
	    jq('#addQualBtn_1').hide();
	  }
	  if(jq('#level_3_qual_0').css('display') == 'none' && jq('#level_3_qual_1').css('display') == 'block' && jq('#level_3_qual_2').css('display') == 'block'){
	    jq('#addQualBtn_0').hide(); 
	    jq('#addQualBtn_1').hide();
	    jq('#addQualBtn_2').show();
	  } 
		  
	if(jq('#level_3_qual_0').css('display') == 'block' && jq('#level_3_qual_1').css('display') == 'none' && jq('#level_3_qual_2').css('display') == 'block'){
		jq('#addQualBtn_0').show();
		jq('#addQualBtn_1').hide(); 
		jq('#addQualBtn_2').hide(); 
	 } 
	if(jq('#level_3_qual_0').css('display') == 'block' && jq('#level_3_qual_1').css('display') == 'block' && jq('#level_3_qual_2').css('display') == 'none'){
		jq('#addQualBtn_0').hide();
		jq('#addQualBtn_1').show(); 
		jq('#addQualBtn_2').hide(); 
	 }
	if(jq('#level_3_qual_0').css('display') == 'block' && jq('#level_3_qual_1').css('display') == 'none' && jq('#level_3_qual_2').css('display') == 'none'){
	   jq('#addQualBtn_0').show(); 
	   jq('#addQualBtn_1').hide();
	   jq('#addQualBtn_2').hide();
	 } 
	if(jq('#level_3_qual_0').css('display') == 'none' && jq('#level_3_qual_1').css('display') == 'none' && jq('#level_3_qual_2').css('display') == 'block'){
	  jq('#addQualBtn_0').hide(); 
	  jq('#addQualBtn_1').hide();
	  jq('#addQualBtn_2').show();
	 }
	if(jq('#level_3_qual_0').css('display') == 'none' && jq('#level_3_qual_1').css('display') == 'block' && jq('#level_3_qual_2').css('display') == 'none'){
	  jq('#addQualBtn_0').hide(); 
	  jq('#addQualBtn_1').hide();
	  jq('#addQualBtn_2').show();
    }
	
	if(jq('#level_3_qual_0').css('display') == 'none' && jq('#level_3_qual_1').css('display') == 'none' && jq('#level_3_qual_2').css('display') == 'block'){
	  jq('#addQualBtn_0').hide(); 
	  jq('#addQualBtn_1').hide();
	  jq('#addQualBtn_2').show();
	}
 }
 
function srPageUrlFormation(){
   var subject = jq('#subject').val();
   var q = jq('#q').val();
   var location = jq('#country_hidden').val();
   var postcode = jq('#postcode').val();
   var studyMode = jq('#study-mode').val();
   var locationType = jq('#location-type').val();
   var campusType = jq('#campus-type').val();
   var module = jq('#module').val();
   var rf = jq('#rf').val();
   var empRateMax = jq('#empRateMax').val();
   var empRateMin = jq('#empRateMin').val();
   var russellGroup = jq('#russellGroup').val();
   var yourPref = jq('#your-pref').val();
   var jacs = jq('#jacs').val();
   var distance = jq('#distance').val();
   var university = jq('#university').val();
   var pageName = jq('#page-name').val();
   var urlQual = jq('#urlQual').val();
   var score = jq('#score').val();
   var qualCode = jq('#qualCode').val();  
   var empRate = jq('#empRate').val();
   var ucasCode = jq('#ucas-code').val();
   var assessmentType = jq('#assessment-type').val();
   var url = "";
   
   var param = "";
   param += !isBlankOrNullString(subject) ? "subject=" + subject.trim() : "";
   param += !isBlankOrNullString(jacs) ? (!isBlankOrNullString(subject) ? "&" : "") + "jacs=" + jacs.trim() : "";
   param += !isBlankOrNullString(q) ? "&q=" + q.trim() : "";
   param += !isBlankOrNullString(module) ? "&module=" + module.trim() : "";
   param += !isBlankOrNullString(location) ? "&location=" + location.trim() : "";
   param += !isBlankOrNullString(postcode) ? "&postcode=" + postcode.trim() : "";
   param += !isBlankOrNullString(studyMode)? "&study-mode=" + studyMode.trim() : "";
   param += !isBlankOrNullString(locationType) ? "&location-type=" + locationType.trim() : "";
   param += !isBlankOrNullString(campusType) ? "&campus-type=" + campusType.trim() : "";
   param += !isBlankOrNullString(empRateMax) ? "&employment-rate-max=" + empRateMax.trim() : "";
   param += !isBlankOrNullString(empRateMin) ? "&employment-rate-min=" + empRateMin.trim() : "";
   param += !isBlankOrNullString(russellGroup) ? "&russell-group=" + russellGroup.trim() : "";
   param += !isBlankOrNullString(yourPref) ? "&your-pref=" + yourPref.trim() : "";
   
   param += distance != "25" ? "&distance=" + distance.trim() : "";
   param += !isBlankOrNullString(university) ? "&university=" + university.trim() : "";
   param += !isBlankOrNullString(rf) ? "&rf=" + rf.trim() : "";
   //param += !isBlankOrNullString(score) ? "&score=" + score.trim() : ""; 
   
   if(!(isBlankOrNullString(pageName)) && (pageName == "srGradeFilterPage")){
     url = "/" + urlQual + "-courses/" + (!isBlankOrNullString(university) ? "csearch?" : "search?") + param;
   }else if(!(isBlankOrNullString(pageName)) && (pageName == "mywhatuni")){
	  url = "/degrees/mywhatuni.html";
   }
   return url;
 }
function showUcasPtVal(){	
  var ucsErrFlag = false;
  var qualId = '';jq('.qualArr').each(function(){qualId += this.value + ','; });
  var subId = '';jq('.subjectArr').each(function(){subId += this.value + ',';});
  var grade = '';var gradeId = '';jq('.gradeArr').each(function(){grade += this.value + ',';gradeId += this.id + ','; });	  
  var qualSeq = '';jq('.qualSeqArr').each(function(){qualSeq += this.value + ',';});
  var count = subId.length;
  var qualIdArr = (qualId != null && qualId != '') ? qualId.split(',') : '';
  var subIdArr = (subId != null && subId != '') ? subId.split(',') : '';
  var gradeArr = (grade != null && grade != '') ? grade.split(','): '';
  var gradeIdArr = (gradeId != null && gradeId != '') ? gradeId.split(','): '';
  var qualSeqArr = (qualSeq != null && qualSeq != '') ? qualSeq.split(','): '';	   
  for (i = 0; i < qualIdArr.length; i += 1) {
    var qual = qualIdArr[i];var sub = subIdArr[i];var grde = gradeArr[i];var grdeId = gradeIdArr[i]; var lp = i+1;var qualSeq = qualSeqArr[i];
    if(qual != '' && sub!= '' && qual == '19' && !(grde != '' && grde > 0 && grde < 1000 && (grde.match(numbers)))){  
    	var subIpId = grdeId.replace('grde', 'qualSub');
    	var ucsIpId = grdeId.replace('grde', 'span_grde');
    	var errDivId = grdeId.replace('grde', 'err_msg');
    	jq('#'+subIpId).addClass('faild');
    	jq('#'+ucsIpId).addClass('faild');
        //var errId = obj.id.substring(8);//qualSub_0_0 //span_grde_0_0//grde_0_0	        
        var texterr = "<p>Please enter the UCAS points for each subject</p>";
        jq('#'+errDivId).html(texterr);
  	   jq('#'+errDivId).show();
  	 //jq('#pId').html("Please enter the qualification");
	 // jq('#errorDiv').show();
	  ucsErrFlag = true;
    }
  }
  return ucsErrFlag;
}
function ucasPtVal(obj){
  var ucsPt = jq(obj).val();
  var ucsIpId = jq(obj).attr('id');
  var qualId = ucsIpId.replace('span_grde', 'qual');
  var subId = ucsIpId.replace('span_grde', 'sub');
  var grdeId = ucsIpId.replace('span_grde', 'grde');
  var qual = jq('#'+qualId).val();
  var sub = jq('#'+subId).val();
  var grde = jq('#'+grdeId).val();
  var subIpId = ucsIpId.replace('span_grde', 'qualSub');
  var ucsIpId = ucsIpId.replace('span_grde', 'span_grde');
  var errDivId = ucsIpId.replace('span_grde', 'err_msg');    
  if(qual != '' && sub!= '' && qual == '19' && !(grde != '' && grde > 0 && grde < 1000 && (grde.match(numbers)))){  	
  	jq('#'+subIpId).addClass('faild');
  	jq('#'+ucsIpId).addClass('faild');
    var texterr = "<p>Please enter the UCAS points for each subject</p>";
    jq('#'+errDivId).html(texterr);
	jq('#'+errDivId).show();	
  }
}
function clearFields(obj){
   if(jq(obj).val() == '0'){
	   jq(obj).val(''); 
   }	
}
function heDipDropDownGen(crdIdx,lp){
	var heReqPoint =  jq('#heReqPoint_'+crdIdx).val();
	var dpt = parseInt(jq('#span_grde_'+crdIdx+'_'+lp+'_D_credit').text().replace('/credits/gi', '').trim());
	var mpt = parseInt(jq('#span_grde_'+crdIdx+'_'+lp+'_M_credit').text().replace(/credits/gi, '').trim());
	var ppt =parseInt(jq('#span_grde_'+crdIdx+'_'+lp+'_P_credit').text().replace(/credits/gi, '').trim());
	var MPDpPt = mpt + ppt;
	genCreditOptionD('D_credit_'+crdIdx,null,MPDpPt,true,crdIdx,dpt); 
	var DPDpPt = dpt + ppt;
	genCreditOptionD('M_credit_'+crdIdx,null,DPDpPt,true,crdIdx,mpt); 
	var DMDpPt = dpt + mpt;
	genCreditOptionD('P_credit_'+crdIdx,null,DMDpPt,true,crdIdx,ppt);
	var curTotPt = dpt+mpt+ppt;
	if(curTotPt == 45){
	  jq("#heCredits").val(curTotPt);
	  jq("#grde_"+crdIdx+"_"+lp).val('D'+dpt + '-M' + mpt + '-P' + ppt);
	  if (!isBlankOrNullString(jq('#qualSub_0_0').val())){
	    jq('#apply_0').removeAttr('disabled');
	  }
    }

  }	
function genCreditOptionD(creditType,totalPt,dpLim,optLimFlag,crdIdx,gradeval){
 var crdType = '';
 var crdTypeId = '';
 var crdTypeIdVal = '';
 var optLim = 45;
 var crdTypeVal = '';
 if(creditType == 'D_credit_'+crdIdx){crdType = 'D'; crdTypeIdVal = '3.2';}
 if(creditType == 'M_credit_'+crdIdx){crdType = 'M'; crdTypeIdVal = '2.1333';}
 if(creditType == 'P_credit_'+crdIdx){crdType = 'P'; crdTypeIdVal = '1.0666';}
 if(optLimFlag){optLim = 45 - dpLim};			 
 var options =  ''; 
 for(var i=0; (i <= 45 && i <= optLim); i += 3){
	 var select = '';
	 if(i == gradeval) {
		 select = 'selected'; 
	 }
   options += '<option value='+i+'credits [data-crd-type]="'+crdType+'" [data-crd-typeid-val]="'+crdTypeIdVal+'"' +select+'>'+i+'credits</option>'
 }			    
 jq('#grade_option_'+creditType).html(options);//replace the selection's html with the new options //grade_option_M_credit_0 ul eg
	  
}