var contextPath = "/degrees";
$(document).ready(function () {
	getJavaSideSecureCookie('cookieconsent');
});
function getJavaSideSecureCookie(cookieName) {
 	var ajxurl = contextPath+"/create-cookies.html?cookieName="+cookieName+"&processType=getCookieValue";    
 	$.ajax({
		url: ajxurl,
		type : "POST",
		success: function(result) {
		  var cookieConsent = result;	
		  if(cookieConsent != null && cookieConsent != undefined && cookieConsent != '' && !cookieConsent.length < 4){
			// 0 - opt in and 1 - opt out
	        var performanceCookieFlag = cookieConsent.substring(2,3) == '0' ? false : true;//performance
	        var targetingCookieFlag = cookieConsent.substring(3,4) == '0' ? false : true;//targetting
	        var pageNameDim1 = $("#pageNameDim1").val();
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');	            
    	      window['ga-disable-UA-52581773-4'] = performanceCookieFlag;	    	    
            //ga('create', 'UA-52581773-1', 'auto', {'sampleRate': 80}); 
              //Insights account changed and removed sampling for 08_MAR_2016, By Thiyagu G.
              ga('create', 'UA-52581773-4', 'auto', {'name': 'newTracker'}); //TODO fortest TEST: UA-37421000-2 LIVE - UA-52581773-1 changed to UA-52581773-1        
              ga('newTracker.require', 'displayfeatures');
              ga('newTracker.set', 'contentGroup1', 'whatuni');
              ga('newTracker.set', 'dimension1', pageNameDim1);
              ga('newTracker.set', 'dimension2', 'whatuni');          
              ga('newTracker.send', 'pageview');  	        
		  }
		}
	});	
 }