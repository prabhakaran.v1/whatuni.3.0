function isValidText(reviewtext, pattern) {
  var index = -1;
  if(reviewtext != null) { index = reviewtext.search(pattern);  }
  return (index != -1) ? true : false;
}
function changeMoreReviewListURL(formtype)  {
   with(document) {
       var current_pagno = "1";
       var current_sort = $$('current_sortorder').value;
       var current_sid   = $$('current_sid').value;
       current_sid  = $$('subjectid_value').options[$$('subjectid_value').selectedIndex].value;  
       //
       var uniName_hidden = $$('uniName_hidden').value;  
       var uniName = $$('uniName').value; 
       if(uniName == "Enter uni name"){ uniName ="";}
       if((uniName_hidden ==null || uniName_hidden == '') && (uniName!=null && uniName!= "" ) ){
        alert("Please choose from the list of uni");  
        return false;
       }
        if((uniName_hidden ==null || uniName_hidden == '') ){
         uniName_hidden = "0";
         uniName="";
       }else{

         $$('institutionName').value = uniName;
       }     
       var current_iama = uniName_hidden;//university Id
       //
       var keyword = $$('reviewSearchKwd').value;
       if(keyword == "Looking for something specific?"){ keyword ="";}
       if(keyword ==null || keyword == ''){
         keyword = '';
       }else{ 
         keyword = replaceAll(trimString(keyword).toLowerCase(), " ","-");    
         keyword = replaceHypen(replaceURL(keyword));
       }
       //
       var formaction   =  $$('more_review_action').value;
       var urlArray=formaction.split("/");
       var finalUrl = '/degrees';
        for (var i=0;i<urlArray.length;i++){  
          if (i == 1){ finalUrl = finalUrl + '/'+"university-course-reviews"; }
          if (i == 3){ finalUrl = finalUrl+'/'+ (trimString(current_sort) == ''? 'highest-rated' : current_sort); }
          if (i == 4){ finalUrl = finalUrl+'/'+(trimString(current_iama) == '' || trimString(current_iama) == 'Please select'? '0' : current_iama); }
          if (i == 5){  finalUrl = finalUrl+'/'+ (trimString(current_sid) == ''? '0' : current_sid); }
          if (i == 6){ finalUrl = finalUrl+'/'+ (trimString(current_pagno) == ''? '1' : current_pagno); }
        }
        finalUrl = finalUrl.toLowerCase()+"/reviews.html";      
        
        if(keyword !=null && keyword !=''){
          finalUrl = finalUrl + "?key="+ keyword;
        }
        finalUrl = finalUrl.toLowerCase();
       }         
      $$('subfilterform').action = finalUrl.toLowerCase();
      $$('subfilterform').submit(); 
      return false;        
    }
    
    
 function changeMoreReviewList(formtype)  {
 with(document) {
       var current_pagno = $$('current_pageno').value; var current_sort = $$('current_sortorder').value;
       var current_sid   = $$('current_sid').value; 
       current_sid  = $$('subjectid_value').options[$$('subjectid_value').selectedIndex].value; current_pagno ="1";
       var formaction   =  $$('more_review_action').value;
       var urlArray=formaction.split("/");
       var finalUrl = '/degrees';
        for (var i=0;i<urlArray.length;i++){
          if (i == 0){ finalUrl = finalUrl + urlArray[i]; }
          if (i == 1){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 2){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 3){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 4){ finalUrl = finalUrl+'/'+ (trimString(current_sort) == ''? 'highest-rated' :   replaceAll(current_sort, "_","-")  ); }
          if (i == 5){ finalUrl = finalUrl+'/0';}
          if (i == 6){  finalUrl = finalUrl+'/'+ (trimString(current_sid) == ''? '0' : current_sid); }
          if (i == 7){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 8){ finalUrl = finalUrl+'/'+ (trimString(current_pagno) == ''? '1' : current_pagno); }
        }
        var keyword = $$('reviewSearchKwd').value;
        if(keyword == "Looking for something specific?"){ keyword ="";}
        if(keyword ==null || keyword == ''){
           keyword = '';
        }else{ 
           keyword = replaceAll(trimString(keyword).toLowerCase(), " ","-");    
           keyword = replaceHypen(replaceURL(keyword));
        }        
        finalUrl = finalUrl.toLowerCase()+"/studentreviews.html";
        if(keyword !=null && keyword !=''){
          finalUrl = finalUrl + "?key="+ keyword;
        }
        $$('iamafilterform').action = finalUrl.toLowerCase(); 
        $$('iamafilterform').submit(); return false;
    }
 }
 
 function clearReviewSearchText(curid, searchmsg) {var ids = curid.id;if($$(ids).value==searchmsg){$$(ids).value="";}}
 function setReviewSearchText(curid, searchmsg){var ids = curid.id; if($$(ids).value=='') { $$(ids).value=searchmsg;} }
