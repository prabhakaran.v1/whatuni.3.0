var defText = "First, choose a subject or university";
var contextPath = "/degrees";
var jq = jQuery.noConflict();
var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
function isNotNullAnddUndef(variable) {return (variable !== null && variable !== undefined && variable !== '');}
function $$(id){return document.getElementById(id)}
//
function isBlankOrNullString(str) {
  var flag = true;
  if(str.trim() != "" && str.trim().length > 0) {
    flag = false;
  }
  return flag;
}

function autoCompleteCourseKeywordBrowse(event,object){
  if($$('ajax_listOfOptions')){
   $$('ajax_listOfOptions').className="ajx_cnt ajx_res";
  }
  ajax_showOptions(object,'coursekeywordbrowse',event,"course_search_indicator",'siteflag=coursedesktop', '0', '');
  var width = document.documentElement.clientWidth;
  var x = jq("#ajaxSrchDivId").offset();
  ajax_optionDiv.style.top = x.top+jq("#ajaxSrchDivId").height()+'px';
  ajax_optionDiv.style.left = x.left + 'px';
  if (width <= 992) {
    ajax_optionDiv.style.width = (ajax_getWidth(object)) + 'px';
    jq('#ajax_listOfOptions').attr("class", "ajx_cnt ajx_res");
  }
}
var errormsg =  "Please choose a course or uni from list";
function courseSearchSubmit(obj){
  var uniOpdSearch = obj.value.trim();    
  if(uniOpdSearch == "" || uniOpdSearch.toLowerCase() == "first, choose a subject or university"){
    alert(errormsg);
    ajax_options_hide();
    if(uniOpdSearch == "" || uniOpdSearch.toLowerCase() == "first, choose a subject or university"){
      obj.value = '';
      obj.focus();
    }
    return false;
  } else{
      
     uniOpdSearch = uniOpdSearch.replace(reg," ");
     uniOpdSearch = replaceAll(uniOpdSearch," ","-");
     if(jq('#courseName_display').val() != '' && jq('#courseName_display').val() != ''){
         getSelectedSubject();       
     }else if(jq('#keywordSearchFlag').val() != 'no'){
       var finalUrl = contextPath+"/wugo/qualifications/search.html?q="+uniOpdSearch;
       if(finalUrl != ""){
       $$("courseSearchForm").action = finalUrl.toLowerCase(); 
       if($$("coursematchbrowsenode").value == finalUrl){ 
         $$("courseSearchForm").submit();
       }else {
         location.href = finalUrl.toLowerCase();
       }   
       }
     }
     jq('#keywordSearchFlag').val('');
  } 
  return false;    
}
function setCourseDefaultTEXT(o){
  var oVal = $$(o.id).value;
  oVal = replaceAll(oVal, " ", "");
  if(oVal == ''){
    $$(o.id).value = "First, choose a subject or university";
  }
}
function clearCourseDefaultTEXT(o){
  var oVal = trimString($$(o.id).value);
  if(oVal == defText){
    $$(o.id).value = "";
  }	
}
function clearCourseName(e,o){
  if(e.keyCode != 13 && e.keyCode != 9){
    if($$(o.id+'_hidden') != null){
      $$(o.id+"_hidden").value = "";
    }
    if($$(o.id+'_id') != null){
      $$(o.id+"_id").value = "";
    }
    if($$(o.id+'_display') != null){
      $$(o.id+"_display").value = "";
    }
    if($$(o.id+'_url') != null){
      $$(o.id+"_url").value = "";
    }	
  }	
}


function gaEventLog(eventCategory, eventAction, eventLabel){
  //ga('send', 'event', eventCategory , eventAction , eventLabel);
  if(isNotNullAnddUndef(eventLabel)){
    GANewAnalyticsEventsLogging(eventCategory, eventAction, eventLabel.toLowerCase());
  }
}


function getSelectedSubject(){
  var courseName = jq('#courseName_display').val();
  var courseName_url = jq('#courseName_url').val();
  if(courseName_url.indexOf('/university-profile') > -1) {
    window.location.href = courseName_url;
    jq('#loadingImg').show();
    //subjectRemove(courseName);
  }else{
    jq('#selectedSubjectDiv').html(getCourseNameAndLocation(courseName));
    jq('#selectedSubjectDiv, #selectedSubject').show();
  }
}

function getCourseNameAndLocation(courseName){
   var str = "<ul id='ulId'>";
   if(!isBlankOrNullString(courseName)) {
     str += "<li id='selectedSubject' onclick='subjectRemove(courseName);'><span class='subCrse'>" + courseName + "</span><span onclick='subjectRemove(courseName);' class='lst_cls'></span></li>";
   } 
   str += "</ul>";
   return str;
}

function subjectRemove(courseName){
  var liCount = jq('#selectedSubjectDiv ul li').length;
  jq('#courseName').val('').focus();
  var courseName_url = jq('#courseName_url').val();
  if(courseName_url.indexOf('/university-profile') > -1) {
    jq('#courseName').val(courseName);
    jq('#loadingImg').show();
  }
  if (liCount > 0){
    if(jq('#selectedSubjectDiv')){
      jq('#selectedSubjectDiv').hide();
      clearPebble();
      disableLocationAndViewCoursesBtn();
      jq("input:radio").removeAttr("checked");
      jq('#country_hidden').val('');
    }
  }
}

function clearPebble() {
  jq('#courseName_hidden, #courseName_id, #courseName_url, #courseName_display').val('');
} 

function selectSubRegions(regionName){
  var countryName = jq('#' + regionName).val();
  if(regionName.indexOf('country') > -1) {
    if(countryName == 'england') {
      jq('#sub_region').show();
      jq('#sub_region input:radio').removeAttr('checked');
    } else if(regionName.indexOf('subcountry')< 0){
       jq('#sub_region').hide();
       jq('#sub_region input:radio').removeAttr('checked');
    }
  }
  jq('#country_hidden').val(countryName);
}

function oblurCourseSelect(obj){
  var keywordSearch = obj.value;
  keywordSearch = keywordSearch.replace(reg," ");
  keywordSearch = replaceAll(keywordSearch," ","-");
  var finalUrl = !isBlankOrNullString(jq('#courseName_url').val()) ? jq('#courseName_url').val() : (contextPath+"/wugo/qualifications/search.html?q="+keywordSearch);
  if(!isBlankOrNullString(finalUrl)){
    jq('#courseName_display').val(obj.value);
    getSelectedSubject();
    getCourseNameAndLocation(keywordSearch);
    ajax_options_hide();
  }
}

function viewCourse(course, country){
  var courseName = $$(course.id).value.trim();
  var location =   $$(country.id).value;
  var courseName_id = jq('#courseName_id').val();
  courseName = courseName.replace(reg," ");
  courseName = replaceAll(courseName," ","-");
  courseName = courseName.toLowerCase();
  var hidden_url = jq('#courseName_url').val();
  var pageName = jq('#subjectLandingPage').val();
  var gaLocation = jq('#'+location).text();
  var eventLabelLocation = gaLocation != '' ? gaLocation : 'All';
  if($$(course.id)){
    var eventLabel = $$(course.id).value + "|" + eventLabelLocation;
    gaEventLog('WUGO Journey', 'Search', eventLabel);
  }
  var subjectLandingPageUrl = contextPath+"/get-subject-landing-courses-count.html";
  
  if(!isBlankOrNullString(courseName_id)){
    url = subjectLandingPageUrl + "?subject="+hidden_url; 
    url = location != '' ? url +"&location="+location : url;
  }else if(isBlankOrNullString(courseName_id)){
    url = subjectLandingPageUrl + "?q="+courseName;
    url = location != '' ? url +"&location="+location : url;
  }
  jq('#loadingImg').show();
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    type: "POST",
    async: false,
    success: function(response){
  },
  complete: function(response){
  if(response.responseText.indexOf("##SPLIT##")>-1){
    var result = response.responseText.split("##SPLIT##")[1];
    if(result <= 0) {
      var resId = "revLightBox";
      var loc = "/jsp/whatunigo/gradeFilterLiBox.jsp?coursecount=" + result +"&pageName="+pageName;
      var url = contextPath + loc;
      jq.ajax({
        url: url, 
        type: "POST", 
        success: function(result){
          jq('#loadingImg').hide();
          jq("#"+resId).html(result);
          lightBoxCall();
          revLightbox();
        }
      });  
    }else{
      var qualSearchPageUrl = contextPath + "/wugo/qualifications/search.html";
      var qualPageUrl = "";
      if(!isBlankOrNullString(courseName_id)) {
        qualPageUrl = qualSearchPageUrl + "?subject="+hidden_url;
        window.location.href = location != '' ? qualPageUrl +"&location="+location : qualPageUrl; 
      }else if(isBlankOrNullString(courseName_id)){
        qualPageUrl = qualSearchPageUrl + "?q="+courseName;
        window.location.href = location != '' ? qualPageUrl +"&location="+location : qualPageUrl;
      }
    }
  }
  }
 });
}

jq(function () {
  jq('#courseName').keyup(function () {
    if(jq(this).val().trim() != ''){
       jq('#viewCourseDiv').removeClass('csl_disbld');
       jq('#locationDiv').removeClass('csl_disbld');
    }else{
      disableLocationAndViewCoursesBtn();
      subjectRemove();
    }
  });
});

function disableLocationAndViewCoursesBtn(){
  jq('#viewCourseDiv').addClass('csl_disbld');
  jq('#locationDiv').addClass('csl_disbld');
}

jq(document).ready(function() {
 jq("input:radio").removeAttr("checked");
 jq('#country_hidden').val('');
});

function clearPeppleOnKeyUp(){
  if(jq('#courseName_display').val().trim() == ''){
	  jq('#selectedSubjectDiv').hide();
	  clearPebble();
	  jq("input:radio").removeAttr("checked");
	  jq('#country_hidden').val('');
  }
}