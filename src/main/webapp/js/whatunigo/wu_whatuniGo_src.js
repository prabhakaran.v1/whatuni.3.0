var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";
var error_tc = "Please agree to our terms and conditions";
var error_qual = "Please tick to confirm you have reviewed your qualifications, ensuring they're correct.";
var contextPath = "/degrees";
var jq = $.noConflict();
function isNullAndUndef(variable) {return (variable !== null && variable !== undefined && variable !== '');}
function isNotNullAnddUndef(variable) {return (variable !== null && variable !== undefined && variable !== '');}
function isNumeric(input){return (input - 0) == input && (''+input).replace(/^\s+|\s+$/g, "").length > 0;}
var SPLIT_VAL = '##SPLIT##';
//
function generateApplication(){
  jq('#action').val('GENERATE_APPLICATION');
  jq('#loadingImg').show();
  jq('#confirmRevBtn').hide();
  gaEventLog('Accept Offer Form', 'Final Review', 'Confirm');
  var collegeName = jq('#collegeName').val();
  gaEventLog('WUGO Journey', 'Offer Confirmed', collegeName);
  navigation();
  //wugoStatsLogging('GENERATE_APPLICATION');
}
//
function offerSuccess(id, pageId, nexPageId, status, type){
  stopinterval();
  //need to change to location href
  //
  
  if('offerFormSuccess'){
    jq('#action').val('STATIC_CONTENT');
    jq('#htmlId').val('ACCEPTING_OFFER_SUCCESS');
    var courseId = jq('#applyNowCourseId').val();
    var opportunityId = jq('#applyNowOpportunityId').val();
    var successURL = contextPath + "/wugo/apply-offer/success.html?action=static_content&htmlId=accepting_offer_success&page-id="+pageId+"&next-page-id="+nexPageId+"&courseId="+courseId+"&opportunityId="+opportunityId;
    window.location.href = successURL;
    //navigation(pageId, nexPageId, status, '', '', '', '');      
  }
}
//
function setGradeVal(obj,grdeArrId){
  var optVal = jq(obj).children("option:selected").val();
  var spanValId = 'span_'+ grdeArrId;
  jq('#'+grdeArrId).val(optVal);
  jq('#'+spanValId).text(optVal);
}
//
function setAjaxSubjectVal(ajaxOptionObj){
  //console.log("option data >> " + ajaxOptionObj.id + " value " + jq(ajaxOptionObj).val());
  //console.log(ajaxOptionObj);
  var grandParentDivID = jq(ajaxOptionObj).parent().parent().parent().attr('id');
  var parentDivID = jq(ajaxOptionObj).parent().parent().attr('id');
  var subSrchIpID = parentDivID.replace('subjectAjaxList_', '');
  //var subjectNameVal = jq(ajaxOptionObj).val();
  var subjectNameVal = jq(ajaxOptionObj).text().trim();
  var subjectIdVal = ajaxOptionObj.id;
  var subArrHidId = 'sub' + grandParentDivID.replace('ajaxdiv', '');
  jq('#'+subSrchIpID).val(subjectNameVal);
  jq('#'+grandParentDivID).hide();
  jq('#'+subArrHidId).val(subjectIdVal);
  getUcasPoints(subArrHidId);
  jq('#'+subSrchIpID).blur();
  //console.log("parentDivID: " + parentDivID + " subSrchIpID: " + subSrchIpID + " subjectNameVal: " + subjectNameVal + " subjectIdVal: " + subjectIdVal + " ");
}
//
var srch_cur_let = new Array();
function cmmtQualSubjectList(obj, id, action, resultDivId){
  //var url = contextPath + "/clearing-match-maker-tool.html";
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  var resId = resultDivId;
  action = 'QUAL_SUB_AJAX';
  var freeText = jq('#'+obj.id).val().trim();
  var subArrId = 'sub_'+ id.replace('qualSub_', '');
  jq('#'+subArrId).val('');
  if(srch_cur_let[obj.id] == freeText){
 		return;
  }
  srch_cur_let[obj.id]= freeText;
  var qualSelId = id.substring(id.indexOf('_')+1, id.lastIndexOf("_"));
  var qualId = jq('#qualsel_'+qualSelId).children("option:selected").val();
  qualId = qualId.indexOf('gcse') > -1 ? qualId.substring(0, qualId.indexOf("_")) : qualId;
  if(isNotNullAnddUndef(freeText) && (freeText.length > 2 || qualId == '18')){
     var stringJson =	{
        "action": action,
        "courseId": courseId,
        "opportunityId": opportunityId,        
        "subQualId": qualId,
        "qualSubList": createJSON()
      };
      var jsonData = JSON.stringify(stringJson);
       var param = "?action="+action +"&free-text="+freeText+"&opportunityId="+opportunityId+"&courseId="+courseId+"&screenwidth="+deviceWidth;
       var url = contextPath + "/clearing-match-maker-tool-edit.html" + param;
       jq.ajax({
        url: url, 
        headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
        dataType: 'json',
        type: "POST",
        data: jsonData,
        async: false,
        complete: function(response){
          var result = response.responseText;
          var selectDivId= "subjectAjaxList_"+id;
          if(result.trim() == ''){
            result = '<div class="nores">No results found for ' + freeText + '</div>';
          };
          var selectBox = '<div name="subjectAjaxList" onchange="" id='+selectDivId+' class=""><div id="ajax_listOfOptions" class="ajx_cnt">'+result+'</div></div>';
          jq('[data-id=wugoSubAjx]').each(function(){
            jq(this).html('');
          });
          jq("#"+resId).html(selectBox);
          var ajxwd = jq('#'+id).outerWidth() + 'px';     
          var ajxht = $$D(id).offsetHeight + 'px';      
          jq('#ajax_listOfOptions').css('top', ajxht);
          jq('#ajax_listOfOptions').css('width', ajxwd);
          jq("#"+resId).show();
        }
      });
   }else{
    //jq('[data-id=wugoSubAjx]').each(function(){
        jq('#'+resId).hide();
    //});
  }
}
//
function valQualTC(id){
  if('qualTCId'==id && jq('#'+id)){
    if(jq('#'+id).prop("checked") == true){
      enableMarkComplete('entryReqSecId');
      return true;
    }else{
      showErrorMsg(id+'_error', error_qual);
      return false;
    }
  }
}
//
function submitOfferTC(id, pageId, nexPageId, status, type){
  if('offerFormTCId'==id && jq('#'+id)){
    if($$("offerFormTCId").checked == true){
      jq('#action').val('STATIC_CONTENT');
      jq('#htmlId').val('FINAL_REVIEW');
      navigation(pageId, nexPageId, status, '', '', '', '');
    }else{
      showErrorMsg('offerFormTC_error', error_tc);   
    }
  }  
}
//
function userRevBtnShow(id){
 if('4'==id){
   if('YES'==jq('#hid_4').val()){
     jq('#qus_5').show();
     jq('#qus_6').show();
     jq('#qus_7').show();
     jq('#qus_8').show();
   }
 }
}
//
function navigation(pageId, nxtPageId, status, resultQus, resultAnsArr, resultOptArr, type, secId, markAsFlag){
  //console.log("nav --- resultQus--->"+resultQus);
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  jq('#loadingImg').show();
  var action = jq('#action').val();
  if('EXIT_LB'==type){action = 'DELETE_USER_DATA';}  
  var htmlId = jq('#htmlId').val();
  var sectionId = 'section_'+secId;
  var applicantCount = jq('#applicationCountId').val();
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  var resId = (type == 'LB') ? "revLightBox" : isNullAndUndef(secId) ? sectionId : "middleContent";
  if(action == "INTL"){ resId = "middleContent"; }
  var formStatus = (isNotNullAnddUndef(jq('#formStatusId').val())) ? jq('#formStatusId').val() : "";
  var offerName = jq('#offerName').val();
  if('top-nav'==status){
    formStatus= '';
    gaEventLog('Accept Offer Form', 'Step Selected', nxtPageId);
  }else if('back'==status){
    formStatus= '';
  }
  
  var url = contextPath + "/clearing-match-maker-tool.html";
  jq.ajax({
    url: url, 
    data: {
      "page-id": pageId, 
      "next-page-id": nxtPageId, 
      "status": formStatus,
      "action": action,
      "htmlId": htmlId,
      "resultQus": resultQus,
      "resultAnsArr": resultAnsArr,
      "resultOptArr": resultOptArr,
      "courseId": courseId,
      "opportunityId": opportunityId,
      "applicationCount": applicantCount,
      "offerName": offerName,
      "screenwidth": deviceWidth
    },
    type: "POST", 
    success: function(result){
      jq('#loadingImg').hide();
      //console.log(result);
      if(isNotNullAnddUndef(result) && result.indexOf('GENERATE_APPLICATION') > -1){
        wugoStatsLogging('GENERATE_APPLICATION');
        var accSubFlag = result.replace('GENERATE_APPLICATION_','')
        if('SUCCESS' == accSubFlag.trim()){
           offerSuccess('offerFormSuccess','7','8');
        }
      }
      if('SECTION_REVIEW_FLAG' != result && 'GENERATE_APPLICATION_SUCCESS' != result && (isNotNullAnddUndef(result) && !(result.indexOf('DELETION_USER_MESSAGE') > -1))){
        jq("#"+resId).html(result);
        jq('#action').val('');
        if(isNullAndUndef(secId) != '' && action != "INTL"){
          var pos = jq('#'+sectionId).offset().top;
          //window.scrollTo(jq('#'+sectionId), 100); 
          jq('body, html').animate({scrollTop: pos});
        }else{
          if(markAsFlag == 'Y'){
            var markAsPos = jq('#markAsComFlagId').offset().top;
          jq('body, html').animate({scrollTop: markAsPos});
          }else{
            jq(window).scrollTop(0);
          }
        }
        addTopCls();
        if(type == 'LB'){
          lightBoxCall();
          revLightbox();
        }
      }else{
        if('SECTION_REVIEW_FLAG' == result){
          jq('#action').val('');
        }
      }
    },
    error: function(result){
     //alert("result--->"+result);
    }
    
  });
}
//
function callEditQual(pageNo, action, from){
    closeLigBox();
    if('NEW_USER' != from){
    if('Y'==jq('#reviewEditQualPage').val()){
      editReviewQual('4', 'QUAL_EDIT');
    }else{
      editQual(pageNo, action);
    }
  }
}
//QUAL_ERROR##SPLIT##FAILED##SPLIT##1##SPLIT##undefined##SPLIT##undefined##SPLIT##56##SPLIT##undefined##SPLIT##null
function callLightBox(result, params){
  var resultArr = result.split("##SPLIT##");
  var resId = "revLightBox";
  var loc = "";
  var hotLine = jq('#hotLine').val();
  var collegeDisplayName = jq('#collegeDisplayName').val();
  var qualErrorCode = resultArr[2];
  if(resultArr[0] == "QUAL_ERROR"){
    var offerName = jq('#offerName').val();
    var pageNo = jq('#pageNo').val();
    var from = resultArr.length > 7 ? resultArr[8] : "";
    
    var param = "?qualErrorCode=" + qualErrorCode+"&oldQualDesc="+resultArr[3]+"&newQualDesc="+resultArr[4]+"&offerName="+offerName+"&pageNo="+pageNo+"&from="+from+ "&oldGrade="+resultArr[4]+"&newGrade="+resultArr[6]+"&subCount="+resultArr[7];
    loc = "/jsp/whatunigo/include/entryReqLiBox.jsp"+param;
  }else if(resultArr[0] == "TERMS_AND_COND"){
  }else if(resultArr[0] == "TIMER_EXPIRY"){
    loc = "/jsp/whatunigo/include/timerExpirySection.jsp";
  }else if(resultArr[0] == "ERROR"){
    loc = "/jsp/whatunigo/include/errorPage.jsp?errorMsg="+resultArr[1];
  }else if(resultArr[0] == "NO_PLACE"){
    loc = "/jsp/whatunigo/include/noPlaceLiBox.jsp?errorMsg="+resultArr[1];
  }else if(resultArr[1] == "CANCEL_OFFER"){
   loc = "/jsp/whatunigo/include/cancelOfferLightBox.jsp?hotLine="+hotLine+"&collegeDisplayName="+collegeDisplayName; 
  }else if(resultArr[1] == "APPLICATION_ERROR_MESSAGE_LIGHTBOX"){
   loc = "/jsp/whatunigo/include/applicationErrorMessageLightbox.jsp";
  }else if(resultArr[0] == "SAVE_APPLICATION"){
    loc = "/jsp/whatunigo/include/saveApplicationLibox.jsp";
  }else if(resultArr[0] == "FIVE_MIN"){
    loc = "/jsp/whatunigo/include/fiveMinLiBox.jsp";
  }else if(resultArr[0] == "BACK_TO_COURSE"){
    loc = "/jsp/whatunigo/include/exitLightBox.jsp";
  }
  var url = contextPath + loc;
  
  jq.ajax({
    url: url, 
    /*data: {
      "page-id": pageId, 
      "next-page-id": nxtPageId, 
      "status": status,
      "action": action,
      "htmlId": htmlId,
      "resultQus": resultQus,
      "resultAnsArr": resultAnsArr,
      "resultOptArr": resultOptArr
      "qualErrorCode" : resultArr[2]
    },*/
    type: "POST", 
    success: function(result){
     jq('#loadingImg').hide(); 
      jq("#"+resId).html(result);
      lightBoxCall();
      revLightbox();
    }
  });
}
//
function addTopCls(){
  jq('.frm-ele').each(function(index) { 
    var xy = jq(this).val();
    if (xy != "") {
      jq(this).next('label').addClass("top_lb");
    }else {
      jq(this).next('label').removeClass("top_lb");
    } 
  });
}
//
function getErrorMsg(id){return errorMsg.id;}
//
function validateFormData(id, type, reviewFlag, basicInfoFormFlag){
  var markAsComFlag = true;
  var errorFlag = true;
  var ele = jq('#'+id).val();
  if(basicInfoFormFlag == 'Y'){
    if(type == "RADIO"){
      var radioId = (id.indexOf('RADIO_') > -1) ? id.replace('RADIO_','') : id;
      ele = jq('#hid_'+radioId).val();
    }
    if(type == "DROPDOWN"){ele = jq('#'+id).val();}
  }
  jq('.su-msg').hide(); 
  //jq('.fail-msg').hide();
  if((type == "RADIO" || type == "DROPDOWN") && basicInfoFormFlag == 'Y'){// basic info form validation
    if(type == "RADIO" && !isNotNullAnddUndef(ele)){
      jq('#'+id+'_Err').text("Please choose YES/NO(Luke to supply error message)");
      jq('#'+id+'_Err').show();          
      //sucessFlag = false;
      markAsComFlag = false;
    }else if(type == "DROPDOWN" && (!isNotNullAnddUndef(ele) || ele == 'Please Choose')){
      jq('#'+id+'_Err').text("Please choose type of offer(Luke to supply error message)");
      jq('#'+id+'_Err').show();          
      //sucessFlag = false;
      markAsComFlag = false;
    }else{
      jq('#'+id+'_Err').hide();
    }
    
  }else if(type == "TEXT"){
    if(!isNullAndUndef(ele)){
      if( id != 'TEXT_20'){
        if(id == 'TEXT_10' || id == 'TEXT_11'){jq('#'+id+'_Err').text("We still don't know your name. Remind us?")};
        //if(id == 'TEXT_15'){jq('#'+id+'_Err').text("Please Enter UCAS ID (Luke to supply text)"); }
        //if(id == 'TEXT_19'){jq('#'+id+'_Err').text("Please enter the address line 1"); }
        //if(id == 'TEXT_20'){jq('#'+id+'_Err').text("Please enter the address line 2"); }
        //if(id == 'TEXT_21'){jq('#'+id+'_Err').text("Please enter the town/city name"); }
        if(id == 'TEXT_22'){jq('#'+id+'_Err').text("Please enter the postcode"); }
        if(id == 'PHONE_17'){jq('#'+id+'_Err').text("Please provide us with your telephone number so that the university can contact you"); }
        
        if(id != 'TEXT_15' && id != 'TEXT_12'){jq('#'+id).addClass('faild');}
        jq('#'+id).removeClass('sucs');
        jq('#'+id+'_Err').show();
        jq('#'+id).next('label').removeClass("top_lb");
        markAsComFlag = false;
      }else{
        jq('#'+id).next('label').removeClass("top_lb");
      }
    }else{
      if(id != 'TEXT_20'){
        ele = (isNotNullAnddUndef(ele)) ? ele.replace(/\-/g, '') : '';
        if(((id == 'PHONE_17' || id == "TEXT_15" )&& !isNumeric(ele)) || (id == "TEXT_15" && ele.length != 10) || (id == "PHONE_17" && ele.length < 11)){
          //if(id == "TEXT_15"){jq('#'+id+'_Err').text("Oops! please enter valid UCAS ID (Luke to supply error message)");}
          if(id == "PHONE_17"){jq('#'+id+'_Err').text("Please enter valid phone number");} 
          if(id == "TEXT_15" && ele.length != 10){
            jq('#'+id).addClass('faild');
          }
          if(id != "TEXT_15"){jq('#'+id).addClass('faild');}
          jq('#'+id).removeClass('sucs');
          jq('#'+id+'_Err').show();
          jq('#'+id).next('label').addClass("top_lb");
          markAsComFlag = false;
        }else{
          if(id == 'TEXT_10'){jq('#'+id+'_Sus').text("Nice to meet you! Great name!");}
          jq('#'+id).removeClass('faild');
          jq('#'+id).addClass('sucs');
          jq('#'+id+'_Err').hide();
          jq('#'+id+'_Sus').show();
          jq('#'+id).next('label').addClass("top_lb");
        }
      }else{
        jq('#'+id).next('label').addClass("top_lb");
      }
    }
  }else if(type == "DROPDOWN" || type == "DATE"){
      if(id == 'DATE_DD' || id == 'DATE_MM' || id == 'DATE_YYYY' || id == 'DATE_13'){
        var dayVal = jq('#DATE_DD').val(); var monthVal = jq('#DATE_MM').val(); var yearVal = jq('#DATE_YYYY').val();
        var date = dayVal + "/" + monthVal + "/" + yearVal;
        if(reviewFlag == "Y" && (dayVal == "" || monthVal == "" || yearVal == "")){
          jq('#DATE_Err').text("Oops! Please enter valid date of birth");
          jq('#DATE_Err').show();
          sucessFlag = false;
          markAsComFlag = false;
        }else if((dayVal != "" && monthVal != "" && yearVal != "") && !isValidDate(date)){
          jq('#DATE_Err').text("Oops! Please enter valid date of birth");
          jq('#DATE_Err').show();
          markAsComFlag = false;
        }else{
          jq('#DATE_Err').hide();
        }
      }else if(id == 'DROPDOWN_17'){
        var con = jq('#PHONE_17').val()
        var mob = jq('#DROPDOWN_17').val()
        
        if(!isNullAndUndef(con)){
          //jq('#PHONE_17_Err').text("Please tell us what type of phone number you have provided");
          //jq('#PHONE_17_Err').show();
          //jq('#PHONE_17').addClass('faild');          
          //jq('#PHONE_17').next('label').removeClass("top_lb");
          //sucessFlag = false;
          markAsComFlag = false;
        }else if(!isNumeric(con)) {
          //jq('#PHONE_17_Err').text("Please tell us what type of phone number you have provided");
          //jq('#PHONE_17_Err').show();
          //jq('#PHONE_17').addClass('faild');     
          //jq('#PHONE_17').next('label').addClass("top_lb");
          //sucessFlag = false;
          markAsComFlag = false;
        }else{
          jq('#PHONE_17_Err').hide();
          jq('#PHONE_17').addClass('sucs');
          jq('#PHONE_17').next('label').addClass("top_lb");
        }
        if(!isNullAndUndef(mob) || mob == 'Please Choose' || mob == 'Phone type'){
          jq('#'+id+'_Err').text("Please tell us what type of phone number you have provided");
          jq('#'+id+'_Err').show();          
          //sucessFlag = false;
          markAsComFlag = false;
        }else{
          jq('#'+id+'_Err').hide();          
        }
        
        //jq('#'+id+'_Err').text("Oops! Please enter valid contact number (Luke to supply error message)");} 
      }else if(!isNullAndUndef(ele) || ele == "Please Choose"){
        if(id == 'DROPDOWN_14'){ jq('#'+id+'_Err').text("Oops! please select gender");}
        if(id == 'DROPDOWN_9'){ jq('#'+id+'_Err').text("Please provide your title");}
        if(id == 'DROPDOWN_18'){ jq('#'+id+'_Err').text("Please select country of residence");}
          jq('#'+id+'_Err').show();
          markAsComFlag = false;
      }else{
        jq('#'+id+'_Err').hide();
      }      
  }
  if(reviewFlag == 'Y'){
    return markAsComFlag;
  }
}
//
function formSubmit(submitId, valCheckId, callType){
  var sucessFlag = true;
  var markAsComFlag = true;
  var saveAndConFlag = true; // for allowing user to save and continue without giving value
  var title = 'DROPDOWN_9';
  var firstName = 'TEXT_10';
  var lastName = 'TEXT_11';
  var middleName = 'TEXT_12';
  var day = 'DATE_DD';
  var month = 'DATE_MM';
  var year = 'DATE_YYYY';
  var gender = 'DROPDOWN_14';
  var ucasId = 'TEXT_15';
  var nation = 'DROPDOWN_25';
  
  var titleVal = jq('#'+title).val().trim();
      titleVal = (isNotNullAnddUndef(titleVal)) ? titleVal : null;
  var firstNameVal = jq('#'+firstName).val().trim();
      firstNameVal = (isNotNullAnddUndef(firstNameVal)) ? firstNameVal : null;
  var lastNameVal = jq('#'+lastName).val().trim();
      lastNameVal = (isNotNullAnddUndef(lastNameVal)) ? lastNameVal : null;
  var middleNameVal = jq('#'+middleName).val().trim();
      middleNameVal = (isNotNullAnddUndef(middleNameVal)) ? middleNameVal : null;
  var dayVal = jq('#'+day).val().trim();      
  var monthVal = jq('#'+month).val().trim();      
  var yearVal = jq('#'+year).val().trim();      
  var genderVal = jq('#'+gender).val().trim();
      genderVal = (isNotNullAnddUndef(genderVal)) ? genderVal : null;
  var ucasIdVal = jq('#'+ucasId).val().trim();
      ucasIdVal = (isNotNullAnddUndef(ucasIdVal)) ? ucasIdVal.replace(/\-/g, '') : null;
  var date = dayVal + "/" + monthVal + "/" + yearVal;
      date = (isNotNullAnddUndef(dayVal) && isNotNullAnddUndef(monthVal )&& isNotNullAnddUndef(yearVal)) ? date : null;
  var nationVal = jq('#'+nation).val().trim();
      nationVal = (isNotNullAnddUndef(nationVal)) ? nationVal : null;
  //jq('.su-msg').hide(); 
  //jq('.fail-msg').hide();
 if(!isNullAndUndef(titleVal)){
   if( "uncheck" != valCheckId && "userFormSubmit" != submitId){
     jq('#'+title+'_Err').text("Please provide your title");
     jq('#'+title).addClass('faild');
     jq('#'+title+'_Err').show();
     jq('#'+title).next('label').removeClass("top_lb");
   }
   sucessFlag = false;
   markAsComFlag = false;
 }
  if(!isNullAndUndef(firstNameVal)){
     if( "uncheck" != valCheckId && "userFormSubmit" != submitId){
       jq('#'+firstName+'_Err').text("We still don't know your name. Remind us?");
       jq('#'+firstName).addClass('faild');
       jq('#'+firstName+'_Err').show();
       jq('#'+firstName).next('label').removeClass("top_lb"); 
     }
     sucessFlag = false;
     markAsComFlag = false;
  }
  if(!isNullAndUndef(lastNameVal)){
     if( "uncheck" != valCheckId && "userFormSubmit" != submitId){
       jq('#'+lastName+'_Err').text("We still don't know your name. Remind us?");
       jq('#'+lastName).addClass('faild');
       jq('#'+lastName+'_Err').show();
       jq('#'+lastName).next('label').removeClass("top_lb");
     }
     sucessFlag = false;
     markAsComFlag = false;
  }
  if(!isNullAndUndef(middleNameVal)){
     if( "uncheck" != valCheckId){
      // jq('#'+middleName+'_Err').text("We still don't know your name. Remind us?");
       //jq('#'+middleName).addClass('faild');
       //jq('#'+middleName+'_Err').show();
       jq('#'+middleName).next('label').removeClass("top_lb");
     }
     //sucessFlag = false;
     //markAsComFlag = false;
  }
  if(submitId == 'MARK_AS_COMPLETE' && (dayVal == "" || monthVal == "" || yearVal == "")){
    if( "uncheck" != valCheckId){
      jq('#DATE_Err').text("Oops! Please enter valid date of birth");
      jq('#DATE_Err').show();
    }
    sucessFlag = false;
    markAsComFlag = false;
    saveAndConFlag = false;
  }else if(isNotNullAnddUndef(submitId) && !(dayVal == "" && monthVal == "" && yearVal == "") && !(dayVal != "" && monthVal != "" && yearVal != "")){
    if( "uncheck" != valCheckId){
      jq('#DATE_Err').text("Oops! Please enter valid date of birth");
      jq('#DATE_Err').show();
    }
    sucessFlag = false;
    markAsComFlag = false;
    saveAndConFlag = false;
  }else if((dayVal != "" && monthVal != "" && yearVal != "") && !isValidDate(date)){
    if( "uncheck" != valCheckId){
      jq('#DATE_Err').text("Oops! Please enter valid date of birth");
      jq('#DATE_Err').show();
    }
    sucessFlag = false;
    markAsComFlag = false;
    saveAndConFlag = false;
  }
  if(!isNullAndUndef(genderVal)){
    if( "uncheck" != valCheckId && "userFormSubmit" != submitId){
      jq('#'+gender+'_Err').text("Please provide us with your Gender");
      jq('#'+gender+'_Err').show();
    }
    sucessFlag = false;
    markAsComFlag = false;
  }
  if(!isNullAndUndef(nationVal)){
   if( "uncheck" != valCheckId && "userFormSubmit" != submitId){
     jq('#'+nation+'_Err').text("Please select Nationality");
     jq('#'+nation+'_Err').show();
   }
   sucessFlag = false;
   markAsComFlag = false;
 }
  if(isNotNullAnddUndef(submitId) && isNotNullAnddUndef(ucasIdVal) && (!isNumeric(ucasIdVal) || ucasIdVal.length != 10)){
    if( "uncheck" != valCheckId){
     jq('#'+ucasId).addClass('faild');
     jq('#'+ucasId+'_Err').show();
     if(!isNullAndUndef(ucasIdVal)){jq('#'+ucasId).next('label').addClass("top_lb");}
    }
    sucessFlag = false;
    markAsComFlag = false;
    saveAndConFlag = false;
  }
  /*else if(!isNullAndUndef(ucasIdVal)){
    if( "uncheck" != valCheckId){
     //jq('#'+ucasId+'_Err').text("Oops! please enter UCAS ID (Luke to supply error message)");
     jq('#'+ucasId).addClass('faild');
     jq('#'+ucasId+'_Err').show();
     jq('#'+ucasId).next('label').removeClass("top_lb");
    }
    sucessFlag = false;
    markAsComFlag = false;  
  }*/
  /*else if(isNotNullAnddUndef(ucasIdVal) && (!isNumeric(ucasIdVal) || ucasIdVal.length != 10)){
    //jq('#'+ucasId+'_Err').text("Oops! please enter valid UCAS ID (Luke to supply error message)");
     jq('#'+ucasId).addClass('faild');
     jq('#'+ucasId+'_Err').show();
     jq('#'+ucasId).next('label').addClass("top_lb");
     sucessFlag = false;
     saveAndConFlag = false;
  }*/
  enableOrDisbaleBtn('markAsComFlagId', markAsComFlag, callType);
  
  if(!isNotNullAnddUndef(submitId) || (submitId == 'MARK_AS_COMPLETE' && !(markAsComFlag))){
    sucessFlag = false;
    saveAndConFlag = false;
  }  
  if(sucessFlag || saveAndConFlag){
    var quesArr = [];
    var ansArr = [];
    var OptionArr = [];
    quesArr = "9,10,11,12,13,14,15,25";
    ansArr = null +SPLIT_VAL+ firstNameVal+SPLIT_VAL+lastNameVal+SPLIT_VAL+middleNameVal+SPLIT_VAL+date+SPLIT_VAL+null+SPLIT_VAL+ucasIdVal+SPLIT_VAL+null;
    OptionArr = titleVal+','+null+','+null+','+null+','+null+','+genderVal+','+null+','+nationVal;
    if(!isNotNullAnddUndef(jq('#formStatusId').val())){
      jq('#formStatusId').val('I');
    }
    var currentPageId = jq('#currentPageId').val();
    currentPageId = 'Page ' + currentPageId;
    if(submitId != 'MARK_AS_COMPLETE'){
      gaEventLog('Accept Offer Form', currentPageId, 'Continue');
    }else{
      gaEventLog('Accept Offer Form', currentPageId, 'Mark As Complete');
    }
    if('save_call'==callType){
      navigation(2, 2, 'I', quesArr, ansArr, OptionArr);
    }else if(submitId == 'MARK_AS_COMPLETE'){
      jq('#formStatusId').val('C');
      navigation(2, 2, 'C', quesArr, ansArr, OptionArr, '', '', 'Y');
    }else{
      navigation(2, 3, 'I', quesArr, ansArr, OptionArr);
    }
  }
}
//
function validateBasicInfo(pageId, nxtPageId, status, frm){
   if(frm == 'LBOX'){
     closeLigBox();
   }
  var quesStr = "";
  var ansStr = "";
  var optStr = "";
  var validateStr = jq('#validateStr').val();
  validateStr != "" ? validateStr.substring(0, validateStr.length - 1) : "";
  var validateArr = validateStr.split(",");
  var gaLabel = "";
  for(var lp = 0; lp < validateArr.length; lp++){
    ansStr += null + SPLIT_VAL;
    if(validateArr[lp].indexOf('hid_') != -1){
      var radioBtn = jq('#'+validateArr[lp]);
      if(radioBtn && radioBtn.val() != '' ){
        quesStr += validateArr[lp].replace('hid_', '') + ',';
        gaLabel += jq('#'+validateArr[lp]).val() + '|';
        //console.log("<----quesStr---->"+quesStr);
        optStr += radioBtn.attr('class') + ',';
        //console.log("<----optStr---->"+optStr);
      }else{
        quesStr += validateArr[lp].replace('hid_', '') + ',';
        optStr += null + ',';
        gaLabel += '|';
      }
    }else if(validateArr[lp].indexOf('sel_') != -1){
     jq('#middleContent select').each(function(index) {
       if(jq(this).find(":selected").text() != "Please Choose"){
         if(jq('#hid_4').val() != 'NO'){
           quesStr += this.id + ",";
           optStr += this.value + ",";
           gaLabel += jq(this).find(":selected").text() + '|';
         }else{
           quesStr += this.id + ",";
           gaLabel += '|';
           optStr += null + ",";
         }
       }else{
         quesStr += this.id + ",";
         gaLabel += '|';
         optStr += null + ",";
       }
     });
   }
}

var difference = jq(validateStr).not(quesStr).get();
//console.log("difference----------->"+difference);
if(difference != ""){
  return false;
}else{
  var que1 = jq("#hid_1").val();
  var que2 = jq("#hid_2").val();
  var que3 = jq("#hid_3").val();
  //var que4 = jq("#hid_4").val();
  if((que1 != '' && que2 != '' && que3 != '') && (que1 != "NO" || que2 != "YES" || que3 != "NO")){
      //
      if(frm != 'LBOX'){
        callLightBox("QUAL_ERROR##SPLIT##INTL_EXIT_LIGHT_BOX##SPLIT##INTL_USER");
        return false;
      }
      //
    jq('#action').val('INTL');
    gaLabel = isNotNullAnddUndef(gaLabel) ? gaLabel.substring(0, gaLabel.length - 1) : "";
    gaEventLog('Accept Offer Form', 'User Exit Point', gaLabel);
  }else{
    var currentPageId = jq('#currentPageId').val();
    currentPageId = 'Page ' + currentPageId;
    gaEventLog('Accept Offer Form', currentPageId, 'Continue');
  }
  if(!isNotNullAnddUndef(jq('#formStatusId').val())){
    jq('#formStatusId').val('I');
  }

  navigation(pageId, nxtPageId, status, quesStr, ansStr, optStr);
}
}
//
function markAsCompBasicInfo(pageId, nxtPageId, status){
  var quesStr = "";
  var ansStr = "";
  var optStr = "";
  var validateStr = jq('#validateStr').val();
  validateStr = validateStr != "" ? validateStr.substring(0, validateStr.length - 1) : "";
  var validateArr = validateStr.split(",");
  var gaLabel = "";
  var markAsCompStr = "";
  var ansVals = "";
  var mrkComErrMsg = false; 
  for(var lp = 0; lp < validateArr.length; lp++){
    ansStr += null + SPLIT_VAL;
    if(validateArr[lp].indexOf('hid_') != -1){
      var radioBtn = jq('#'+validateArr[lp]);
      var divId = validateArr[lp].replace('hid', 'div');
        if(jq('#'+divId).is(":visible")){
          markAsCompStr += lp+ ',';
          isNotNullAnddUndef(jq('#'+ validateArr[lp]).val()) ? ansVals += jq('#'+ validateArr[lp]).val()+ ',' : "";
        }
      if(radioBtn && radioBtn.val() != '' ){
        quesStr += validateArr[lp].replace('hid_', '') + ',';
        gaLabel += jq('#'+validateArr[lp]).val() + '|';
        //console.log("<----quesStr---->"+quesStr);
        optStr += radioBtn.attr('class') + ',';
        //console.log("<----optStr---->"+optStr);
      }else{
        quesStr += validateArr[lp].replace('hid_', '') + ',';
        optStr += null + ',';
        gaLabel += '|';
      }
    }else if(validateArr[lp].indexOf('sel_') != -1){
      
     jq('#middleContent select').each(function(index) {
       if(jq(this).find(":selected").text() != "Please Choose"){
         if(jq('#hid_4').val() != 'NO' && jq('#hid_4').val() != ''){
           quesStr += this.id + ",";
           optStr += this.value + ",";
           gaLabel += jq(this).find(":selected").text() + '|';
           var divId = validateArr[lp].replace('sel', 'div');
        if(jq('#'+divId).is(":visible")){
          markAsCompStr += lp + ','; 
        }
           isNotNullAnddUndef(jq(this).find(":selected").text()) ? ansVals += jq(this).find(":selected").text() + ',' : "";
         }else{
           quesStr += this.id + ",";
           gaLabel += '|';
           optStr += null + ",";
         }
       }else{
          if(jq('#hid_4').val() != 'NO' && jq('#hid_4').val() != ''){
            quesStr += this.id + ",";
            gaLabel += '|';
            optStr += null + ",";
            mrkComErrMsg = true;          
          }
       }
     });
   }
}
markAsCompStr = markAsCompStr != "" ? markAsCompStr.substring(0, markAsCompStr.length - 1): "";
ansVals = ansVals != "" ? ansVals.substring(0, ansVals.length - 1): "";
var markAsCompStrArr = markAsCompStr.split(',');
var ansValsArr = ansVals.split(',');

//alert("markAsCompStr--->"+markAsCompStrArr.length+"<---quesStr--->"+ansValsArr.length);

if(markAsCompStrArr.length != ansValsArr.length || mrkComErrMsg){
  jq('#bsinfoErr').text('Please provide an answer to all mandatory questions');
  jq('#bsinfoErr').show();
  enableMarkComplete();
  return false;
}else{
  jq('#pageNav_'+pageId).addClass('stp_saved');
}

var differenceArr = jq(markAsCompStr).not(ansVals).get();

//alert("differenceArr----->"+differenceArr);

var difference = jq(validateStr).not(quesStr).get();
//console.log("difference----------->"+difference);
if(difference != "" || mrkComErrMsg){
  return false;
}else{
  var que1 = jq("#hid_1").val();
  var que2 = jq("#hid_2").val();
  var que3 = jq("#hid_3").val();
  //var que4 = jq("#hid_4").val();
  if((que1 != '' && que2 != '' && que3 != '') && (que1 != "NO" || que2 != "YES" || que3 != "NO")){
    
    //
    callLightBox("QUAL_ERROR##SPLIT##INTL_EXIT_LIGHT_BOX##SPLIT##INTL_USER");
    jq("#markAsComFlagId").prop("checked", false);
     jq('#markAsComFlagId').attr('disabled',false);
      return false;
      //jq('#action').val('INTL');
      //
    //gaEventLog('Accept Offer Form', 'User Exit Point', gaLabel);
  }else{
    var currentPageId = jq('#currentPageId').val();
    currentPageId = 'Page ' + currentPageId;
    //gaEventLog('Accept Offer Form', currentPageId, 'Continue');
    gaEventLog('Accept Offer Form', currentPageId, 'Mark As Complete');
    nxtPageId = pageId;
  }
  jq('#formStatusId').val('C');
  var pos = jq('#markAsComFlagId').offset().top;
  navigation(pageId, nxtPageId, status, quesStr, ansStr, optStr, '', '', 'Y');
  jq('body, html').animate({scrollTop: pos});
}
}


//
function contactFormSubmit(submitId, valCheckId, callType){
  var sucessFlag = true;
  var markAsComFlag = true;
  var saveAndConFlag = true;
  var email = 'TEXT_16';
  var phone = 'PHONE_17';
  var phType = 'DROPDOWN_17';
  var country = 'DROPDOWN_18';
  var addr1 = 'TEXT_19';
  var addr2 = 'TEXT_20';
  var town = 'TEXT_21';
  var postCode = 'TEXT_22';
  
  var emailVal = jq('#' + email).val().trim();
      emailVal = (isNotNullAnddUndef(emailVal)) ? emailVal : null;
  var phoneVal = jq('#' + phone).val().trim();
    phoneVal = (isNotNullAnddUndef(phoneVal)) ? phoneVal : null;
  var phTypeVal = jq('#' + phType).val().trim();
    phTypeVal = (isNotNullAnddUndef(phTypeVal)) ? phTypeVal : null;
  var countryVal = jq('#' + country).val().trim();
    countryVal = (isNotNullAnddUndef(countryVal)) ? countryVal : null;
  var addr1Val = jq('#' + addr1).val().trim();
    addr1Val = (isNotNullAnddUndef(addr1Val)) ? addr1Val : null;
  var addr2Val = jq('#' + addr2).val().trim();
    addr2Val = (isNotNullAnddUndef(addr2Val)) ? addr2Val : null;
  var townVal = jq('#' + town).val().trim();
    townVal = (isNotNullAnddUndef(townVal)) ? townVal : null;
  var postCodeVal = jq('#' + postCode).val().trim();
    postCodeVal = (isNotNullAnddUndef(postCodeVal)) ? postCodeVal : null;
    
  if(!isNullAndUndef(emailVal)){
    if( "uncheck" != valCheckId){
      jq('#DROPDOWN_9').next('label').removeClass("top_lb");
    }
     sucessFlag = false;
     markAsComFlag = false;
  }else if(isNotNullAnddUndef(emailVal)){
    //need to do validation check
  }
  if(!isNullAndUndef(phoneVal)){
    if( "uncheck" != valCheckId && "contactFormSubmit" != submitId){
      jq('#'+phone+'_Err').text("Please provide us with your telephone number so that the university can contact you");
      jq('#'+phone).addClass('faild');
      jq('#'+phone+'_Err').show();
      jq('#'+phone).next('label').removeClass("top_lb");
    }
     sucessFlag = false;
     markAsComFlag = false;
  }else if(isNotNullAnddUndef(phoneVal) && !isNumeric(phoneVal) && phoneVal.length < 11){
    if( "uncheck" != valCheckId){
      jq('#'+phone+'_Err').text("Please enter valid phone number");
      jq('#'+phone).addClass('faild');
      jq('#'+phone+'_Err').show();
      jq('#'+phone).next('label').addClass("top_lb");
    }
     sucessFlag = false;
     markAsComFlag = false;
     saveAndConFlag = false;
  }
  if(!isNullAndUndef(phTypeVal)){
    if( "uncheck" != valCheckId && "contactFormSubmit" != submitId){
      jq('#'+phType+'_Err').text("Please tell us what type of phone number you have provided");
      jq('#'+phType).addClass('faild');
      jq('#'+phType+'_Err').show();
      jq('#'+phType).next('label').removeClass("top_lb");
    } 
     sucessFlag = false;
     markAsComFlag = false;
  }else{jq('#'+phType+'_Err').hide();}
  if(!isNotNullAnddUndef(submitId) && !isNullAndUndef(countryVal)){
     if( "uncheck" != valCheckId){
       jq('#'+country+'_Err').text("Please select country of residence");
       jq('#'+country).addClass('faild');
       jq('#'+country+'_Err').show();
       jq('#'+country).next('label').removeClass("top_lb");
     }
     sucessFlag = false;
     markAsComFlag = false;
  }else if(!isNullAndUndef(countryVal)){
     if( "uncheck" != valCheckId && "contactFormSubmit" != submitId){
       jq('#'+country+'_Err').text("What parts are you from, friend?");
       jq('#'+country).addClass('faild');
       jq('#'+country+'_Err').show();
       jq('#'+country).next('label').removeClass("top_lb");
     }
     sucessFlag = false;
     markAsComFlag = false;
  }
  if(!isNullAndUndef(addr1Val)){
   if( "uncheck" != valCheckId && "contactFormSubmit" != submitId){
     jq('#'+addr1).addClass('faild');
     jq('#'+addr1).next('label').removeClass("top_lb");
   }
   sucessFlag = false;
   markAsComFlag = false;
  }
  if(!isNullAndUndef(townVal)){
    if( "uncheck" != valCheckId && "contactFormSubmit" != submitId){
      //jq('#'+postCode).text("Please enter town");
      jq('#'+town).addClass('faild');
      jq('#'+town).next('label').removeClass("top_lb");
    }
    sucessFlag = false;
    markAsComFlag = false;
  }
  if(!isNullAndUndef(postCodeVal)){
     if( "uncheck" != valCheckId && "contactFormSubmit" != submitId){
       jq('#'+postCode).text("Please enter the postcode");
       jq('#'+postCode).addClass('faild');
       jq('#'+postCode+'_Err').show();
       jq('#'+postCode).next('label').removeClass("top_lb");
     }
     sucessFlag = false;
     markAsComFlag = false;
  }else if(isNotNullAnddUndef(emailVal)){
    //need to do validation check 
  }
  enableOrDisbaleBtn('markAsComFlagId', markAsComFlag , callType);
 
  if(!isNotNullAnddUndef(submitId) || submitId == 'MARK_AS_COMPLETE' && !(markAsComFlag)){
    sucessFlag = false;
    saveAndConFlag = false;
  }
  if(sucessFlag || saveAndConFlag){
    var quesArr = [];
    var ansArr = [];
    var OptionArr = [];
    quesArr = "16,17,18,19,20,21,22";
    ansArr = emailVal +SPLIT_VAL+ phoneVal+SPLIT_VAL+null+SPLIT_VAL+addr1Val+SPLIT_VAL+addr2Val+SPLIT_VAL+townVal+SPLIT_VAL+postCodeVal;
    OptionArr = null+','+phTypeVal+','+countryVal+','+null+','+null+','+null+','+null;    
    if(!isNotNullAnddUndef(jq('#formStatusId').val())){
      jq('#formStatusId').val('I');
    }
    var currentPageId = jq('#currentPageId').val();
    currentPageId = 'Page ' + currentPageId;
    if(submitId != 'MARK_AS_COMPLETE'){
      gaEventLog('Accept Offer Form', currentPageId, 'Continue');    
    }else{
      gaEventLog('Accept Offer Form', currentPageId, 'Mark As Complete');
    }
    if('save_call'==callType){
      navigation(3, 3, 'I', quesArr, ansArr, OptionArr);
    }else if(submitId == 'MARK_AS_COMPLETE'){
      jq('#formStatusId').val('C');
      navigation(3, 3, 'I', quesArr, ansArr, OptionArr, '', '', 'Y');
    }else{
      navigation(3, 4, 'I', quesArr, ansArr, OptionArr);
    }
  }
}
//
function isValidDate(date){
	if(date.search(/^(((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00|[048])))$/) == -1){
		return false;
	}
	return true;
}
//
function validateFrom(pageId, nxtPageId, status){
 var resultQus = "";
 var resultQusArr = [];
 var resultAnsArr = [];
 var resultOptArr = [];
 jq('#middleContent input[type=radio]:checked').each(function(index) {
 var len = jq('#middleContent input[type=radio]:checked').size();
  resultQus += this.name +  ",";
  resultAnsArr += null + ",";
  resultOptArr += this.id + ",";
});
jq('#middleContent select').each(function(index) {
 var len = jq('#middleContent select').size();
 //console.log("SELECT------>"+jq(this).children("option:selected").val());
 if(jq(this).find(":selected").val() != "Please Choose"){
  resultQus += this.id + ",";
  resultAnsArr += null + ",";
  resultOptArr += this.value + ",";
  }
});
jq('#middleContent input[type=text]').each(function(index) {
 var len = jq('#middleContent input[type=text]').size();
  //console.log("TEXT------>"+this.value);
  if (this.value != "") {
    resultQus += this.id + ",";
    resultAnsArr += null + ",";
    resultOptArr += this.value + ",";
  }
});
 var qusHiden = jq('#qusArr').val() != "" ? jq('#qusArr').val().substring(0, jq('#qusArr').val().length - 1) : "";
 var qusArr = qusHiden.split(',');
 
 var qusLen = qusHiden != "" && qusArr.length > 0 ? qusArr.length : 0;
 var resQusHid = resultQus != "" ? resultQus.substring(0, resultQus.length - 1) : ""; 
 resultQusArr = resQusHid.split(',');
 var resQusLen = resQusHid != "" && resultQusArr.length > 0 ? resultQusArr.length : 0;
 if(resQusLen == qusLen){
   //console.log("resultQus------>"+resultQus);
   //console.log("resultAnsArr------->"+resultAnsArr);
   //console.log("resultOptArr--------->"+resultOptArr);
   //console.log("resultQus.length----->"+resultQus.length);
   //console.log("resultAnsArr.length----->"+resultAnsArr.length);
   //console.log("resultOptArr.length----->"+resultOptArr.length);
   navigation(pageId, nxtPageId, status, resultQus, resultAnsArr, resultOptArr);
 }else {
   var difference = jq(qusArr).not(resultQusArr).get();
   var errorQus = "";
   var index = "";
   //console.log("qusArr---------->"+qusArr);
   //console.log("resultQusArr--------->"+resultQusArr);
   //console.log("difference----------->"+difference);
   //console.log("resultAnsArr-------->"+resultAnsArr);
   }
}

function selectChange(formId, id){
  jq('#select_' + id + '_span').text(jq('#'+formId+' select :selected').text());  
  //console.log('selectChange--->'+jq('#'+formId+' select :selected').text());
  enableMarkComplete();
  if('div_5'==formId){
   jq('#bsinfoErr').hide();
  }
}
//
function preSelect(){
jq('#middleContent select').each(function(index) {
 var len = jq('#middleContent select').size();
 //console.log("SELECT------>"+this.id+"<--->"+jq(this).children("option:selected").val());
 if(jq(this).find(":selected").val() != "Please Choose"){
 //console.log("pre-populate--->"+jq('#'+this.id+' select :selected').text());
 jq('#select_' + this.id + '_span').text(jq('#middleContent select :selected').text());
  }
});
}
//
function prepopulateDOB(id){
  var DOB = jq('#' + id).val();
  //console.log("DOB-->"+DOB);
  if(DOB != null && DOB != ""){
    var dob = DOB.split("/");
    if(dob.length > 2){
      setSelectVal('DATE_DD', dob[0]);
      setSelectVal('DATE_MM', dob[01]);
      setSelectVal('DATE_YYYY', dob[2]);
    }
  }
}
//
function setSelectVal(id, val){
  jq("#"+id+" option[value=" + val + "]").attr('selected', 'selected');
}
//
function reviewSection(pageId){
  jq('#sectId').val(pageId);
  //jq('.accordion_body').hide();
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  jq('#i_1').removeClass('fa-minus-circle').addClass('fa-plus-circle');
  jq('#i_2').removeClass('fa-minus-circle').addClass('fa-plus-circle');
  jq('#i_3').removeClass('fa-minus-circle').addClass('fa-plus-circle');
  jq('#i_4').removeClass('fa-minus-circle').addClass('fa-plus-circle');
  
  //jq('.fa fa-minus-circle').removeClass('fa-minus-circle').addClass('fa-plus-circle');
  var url = contextPath + "/clearing-match-maker-tool.html";
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  var secObj = jq("#section_"+pageId);
  
  var action = "";  
  if(pageId == 4) {
    action = "QUAL_GET";
  } else {
    action = "GET";
  }
  if(secObj && secObj.text() == ""){
  jq('#loadingImg').show();
  jq.ajax({
    url: url, 
    data: {
      "page-id": pageId, 
      "next-page-id": pageId, 
      "status": 'I',
      "action": action,
      "courseId": courseId,
      "opportunityId": opportunityId,
      "screenwidth": deviceWidth
      },
    type: "POST", 
    success: function(result){
      jq('#loadingImg').hide();
      jq('#sectId').val(pageId);
      jq("#section_"+pageId).html(result);
      jq("#section_"+pageId).show();
      jq('#i_'+pageId).removeClass('fa-plus-circle').addClass('fa-minus-circle');
      for(var lp = 1; lp < 5; lp++){
        if(pageId != lp){jq("#section_"+lp).hide();}
      }
      enableOrDisbaleBtn('revSubmitFlagId');
      if(jq('#secRev_form_'+pageId).length){
        if(jq('#secRev_form_'+pageId).prop('checked') == false){    
          jq('#revSecFlag_'+pageId).val('N');
        }else{
          jq('#revSecFlag_'+pageId).val('Y');
        }
      }
      enableDisTCondiBtn();
    }
    
  });
  }else if(secObj.is(":visible")){
    secObj.hide();
    jq('#i_'+pageId).removeClass('fa-minus-circle').addClass('fa-plus-circle');
  }else{
    secObj.show();
    jq('#i_'+pageId).removeClass('fa-plus-circle').addClass('fa-minus-circle');
  }
  for(var lp = 1; lp < 5; lp++){
    if(pageId != lp){
      jq("#section_"+lp).hide();
    }
  }
  
}
//
function changeBtn(id, callFlag){
  if(id == '4'){
  var radioValue = jq("#hid_4").val();
  var hid = 'hid_';
  var div5 = jq('#div_5');var div6 = jq('#div_6');var div7 = jq('#div_7');var div8 = jq('#div_8');  
  if(radioValue != "YES"){
    jq('#div_5 a').removeClass('bn-gry active').addClass('bn-outln');
    jq('#div_6 a').removeClass('bn-gry active').addClass('bn-outln');
    jq('#div_7 a').removeClass('bn-gry active').addClass('bn-outln');
    jq('#div_8 a').removeClass('bn-gry active').addClass('bn-outln');
    jq('#qus_5').hide();jq('#qus_6').hide();jq('#qus_7').hide();jq('#qus_8').hide();
    jq('#sect_5').hide();jq('#sect_6').hide();jq('#sect_7').hide();jq('#sect_8').hide();
    div5.hide();div6.hide();div7.hide();div8.hide();
    jq("#div_5 select")[0].selectedIndex = 0;
    jq('#div_5 select').attr('selectedIndex', 0);
    jq('#select_5_span').text(jq('#div_5 select').val());
    jq('#div_5 select').each(function(index) {
      jq(this).children("option:selected").val('');
      jq('#select_5_span').text('Please Choose');
    });
    jq('#'+hid +'6').val(""); jq('#'+hid +'7').val(""); jq('#'+hid +'8').val("");
    
    jq('#tit_5').hide();jq('#tit_6').hide();jq('#tit_7').hide();jq('#tit_8').hide();
    jq('#head_5').hide();jq('#head_6').hide();jq('#head_7').hide();jq('#head_8').hide();
  }else if(radioValue == "YES" && (callFlag == "ONLOAD" || callFlag == "REVIEW_PAGE")){
    jq('#qus_5').show();jq('#qus_6').show();jq('#qus_7').show();jq('#qus_8').show();
    jq('#sect_5').hide();jq('#sect_6').hide();jq('#sect_7').hide();jq('#sect_8').hide();
    jq('#tit_5').show();jq('#tit_6').show();jq('#tit_7').show();jq('#tit_8').show();
    jq('#head_5').show();jq('#head_6').show();jq('#head_7').show();jq('#head_8').show();
    div5.show(); div6.show(); div7.show(); div8.show();
    jq('#div_5 select').show();jq('#div_6 a').show();jq('#div_7 a').show();jq('#div_8 a').show();
  }else{
    jq('#qus_5').show();jq('#qus_6').show();jq('#qus_7').show();jq('#qus_8').show();
    jq('#sect_5').show();jq('#sect_6').show();jq('#sect_7').show();jq('#sect_8').show();
    jq('#tit_5').hide();jq('#tit_6').hide();jq('#tit_7').hide();jq('#tit_8').hide();
    jq('#head_5').hide();jq('#head_6').hide();jq('#head_7').hide();jq('#head_8').hide();
    div5.show(); div6.show(); div7.show(); div8.show();
    
    jq('#div_5 select').show();jq('#div_6 a').show();jq('#div_7 a').show();jq('#div_8 a').show();
  }
  }
  enableMarkComplete();
  //enableOrDisableTopNavigation();
}
//
function changeRadio(id, hId){
  jq('.opt_'+hId).removeClass('bn-gry active').addClass('bn-outln');
  jq('#'+id).addClass('bn-outln bn-gry active');
  jq('#hid_'+hId).val(jq('#'+id).text());
  var cls = jq('#hid_'+hId).attr('class');
  jq('#hid_'+hId).removeClass(cls).addClass(id);
  jq('#bsinfoErr').hide();
}
//
function valUserRevTCBtn(sectId){
  if(jq('#secRev_form_'+sectId).prop('checked') == true){
    var markAsComFlag = true;
    sectId = (isNotNullAnddUndef(sectId) && 'null' != sectId) ? sectId : jq('#sectId').val();
    var sectionLen = jq(".accordion_body").length;
    var pageId = parseInt(sectId) +1;
    if(sectId < sectionLen){
      reviewSection(pageId);
      var secObj = jq("#section_"+pageId);      
      jq('#i_'+sectId).removeClass('fa-minus-circle').addClass('fa-plus-circle');
      if(secObj.is(":visible")){
        secObj.show();
        jq('#i_'+pageId).removeClass('fa-plus-circle').addClass('fa-minus-circle');
      }
    }else{
    }
    enableOrDisbaleBtn('revSubmitFlagId');
    updateReviewFlag(sectId);
  }else{
    jq('#secRev_form_'+sectId).prop("checked", true);
  }
}
//
function updateReviewFlag(pageId){
  jq('#action').val('SECTION_REVIEW');
  navigation(pageId, pageId);
}
//
function enOrDisTopNav(fromPage){
  var currentPageId = jq('#currentPageId').val();
  var curPageListId = 'pageNav_'+currentPageId;
  jq("#pageNavULId li").each(function(){
    if(curPageListId == jq(this).attr('id')){
      jq(this).removeClass("stp_saved").addClass('stp_pro disbldCur');
      
    }else{
      jq(this).addClass('disbld');
    } 
  }); 
  jq('#formStatusId').val('I');
  jq('#markAsComFlagId').attr('disabled',false);
  //if('offerEditQual'==fromPage){
    //jq(this).removeClass("stp_cmplt").addClass('stp_pro');
    jq("#markAsComFlagId").prop("checked", false);
    //jq('#markAsComFlagId').attr('disabled',true);
  //}
}
//
function enableDisTCondiBtn(){
  var flag = 0;
  for(var lp = 1; lp < 5; lp++){
    if(jq('#revSecFlag_'+lp).val() == 'Y'){
      flag += 1;
    }
  }
  if(flag == 4){
    jq("#revSubmitFlagId").removeClass("disbld");
  }
}
//
function enableDisContiBtn(){
  var flag = 0;
  for(var lp = 1; lp < 5; lp++){
    if(jq('#form_'+lp).val() == 'C'){
      flag += 1;
    }
  }
  if(flag == 4){
    //jq("#contBtn").removeClass("disbld");
  }
  return flag;
}
//
function enableOrDisbaleBtn(id, markAsComFlag, callType){  
  //For user review section
  if('revSubmitFlagId' == id){
    var markAsComFlag = true;
    jq('[data-id=secRevBtn]').each(function(){
      if(isNotNullAnddUndef(jq(this))){
        if(jq(this).prop('checked') == false){    
          markAsComFlag = false;
        }
      }
    });
    if(markAsComFlag){
      //jq('#revSubmitFlagId').removeAttr('disabled');
      jq("#revSubmitFlagId").removeClass("disbld");
      //jq('#formStatusId').val('I'); Need to check and change status
    }else{
      jq("#revSubmitFlagId").addClass("disbld");
      jq('#formStatusId').val('I');
      //jq('#revSubmitFlagId').attr('disabled',true);    
    }
  }
  //other section
  if('markAsComFlagId'== id){
    if(markAsComFlag){
      //jq('#markAsComFlagId').removeAttr('disabled');
      //jq("#markAsComFlagId").prop("checked", false);
      if('onload'!=callType){
        //jq('#formStatusId').val('I');      
      }
    }else{
      jq("#markAsComFlagId").prop("checked", false);
      jq("#markAsComFlagId").removeAttr("disabled");
      jq('#formStatusId').val('I');
    }
  }
}
function enableMarkComplete(id, callType, pageId){
  var markAsComFlag = true;
  if('entryReqSecId'==id && 'onload'!=callType){
    if(jq('#qualTCId').prop('checked') == false){    
      markAsComFlag = false;
      //jq("#contBtn").addClass("disbld");
      if(isNotNullAnddUndef(pageId)){ jq('#pageNav_'+pageId).removeClass("stp_saved").addClass('stp_pro');}
    }else{
      if(isNotNullAnddUndef(pageId)){ jq('#pageNav_'+pageId).removeClass("stp_saved").addClass('stp_pro');}
    }  
  }else {//for basic info section
    var classList = jq('.bas_lst pt-40');
    jq(".bas_lst div:visible").each(function() {
      var accessId = jq(this).attr('id');
      if(isNotNullAnddUndef(accessId)){
        //console.log("list " + accessId);
        accessId = (accessId.indexOf('div_') != -1) ? accessId.replace('div_','') : accessId;
        //console.log("list mod " + accessId);
        if(accessId=='5'){
          var selectVal = jq('#select_'+accessId+'_span').text();
          if(selectVal == 'Please Choose'){
            markAsComFlag = false;
          }        
        }else{
          var fieldVal = jq('#hid_'+accessId).val();
          //console.log('#hid_'+accessId);    
          //console.log("fieldVal"+fieldVal);
          if(!isNotNullAnddUndef(fieldVal)){
            markAsComFlag = false;
          }
        }
      }
    });
  }
  enableOrDisbaleBtn('markAsComFlagId', markAsComFlag, callType);
  /*
  if(markAsComFlag){
    jq('#markAsComFlagId').removeAttr('disabled');
    jq('#formStatusId').val('C');
  }else{
    jq("#markAsComFlagId").prop("checked", false);
    jq('#markAsComFlagId').attr('disabled',true);
  }
  */
}
//
//For user review section buttton
function enableRevSecBtn(pageStatusId){
  var pageStatus = jq('#'+pageStatusId).val();
  var formRevFlag = jq('#'+pageStatusId+'_rev').val();
  if('C' == pageStatus){  
    jq('#secRev_'+pageStatusId).removeAttr('disabled');
  }else{
     jq('#secRev_'+pageStatusId).attr('disabled',true);
  }
  if('Y' == formRevFlag){
    //jq('#secRev_'+pageStatusId).prop("checked", true);
  }
}
//for changing the form status with mark
function markAsComplete(status, loading){  
  if('C' == status){
    var currentPageId = jq('#currentPageId').val();
    if(jq('#markAsComFlagId').prop("checked") == false){
      jq("#markAsComFlagId").prop("checked", true);
    }else{ if(loading != "onload"){gaEventLog('Accept Offer Form', ('Page '+ currentPageId), 'Mark As Complete');}}
    }
  jq('#formStatusId').val(status);
  
}
//
function lightBoxCall(){
 jq("#revLightBox").addClass("rev_lbox");
 jq(".rev_lbox").css({'z-index':'111111','visibility':'visible'});
 jq(".rev_lbox").addClass("fadeIn").removeClass("fadeOut");
 jq("html").addClass("rvscrl_hid");
 //closeLiBox();
}
jq(window).resize(function() {
  revLightbox();
});
jq(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
revLightbox();
}
function revLightbox(){
  var width = document.documentElement.clientWidth;
  var revnmehgt = jq(".rvbx_cnt .revlst_lft").outerHeight();
  var lbxhgt = jq(".rev_lbox .rvbx_cnt").height();
  var txth2hgt = jq(".rvbx_cnt h2").outerHeight();
  var txth3hgt = jq(".rvbx_cnt h3").outerHeight();
  if (width <= 480) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    jq(".lbx_scrl").css("height", +scrhgt+ "px");
   }else if ((width >= 481) && (width <= 992)) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    jq(".lbx_scrl").css("height", +scrhgt+ "px");
   }else{
     var lbxmin = 17 + txth2hgt + txth3hgt;
     var scrhgt = lbxhgt - lbxmin; 
     jq(".lbx_scrl").css("height", +scrhgt+ "px"); 
   }
}
//
function editSec(id){
  var secRevBtnId = jq('#sect_'+id).parent().parent().attr('id');  
  var secId = jq('#sect_'+id);
  jq('#tit_'+id).hide();jq('#head_'+id).hide();
  secId.show();
  if(secRevBtnId.indexOf('section_') > -1){
    secRevBtnId = 'secRev_form_' + secRevBtnId.replace('section_', '');
    jq('#'+secRevBtnId).attr('disabled',true); 
    jq('#'+secRevBtnId).prop('checked', false);
  }  
}
//
function editReviewQual(sectNo,action){
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  jq('#action').val(action);
  var actionVal = jq('#action').val();
  var sectionId = 'section_'+sectNo;
  var resId = sectionId;
  var url = contextPath + "/clearing-match-maker-tool.html";
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  jq('#loadingImg').show();
  jq.ajax({
    url: url, 
    data: {
      "page-id": sectNo, 
      "action": actionVal,
      "courseId": courseId,
      "opportunityId": opportunityId,
      "screenwidth": deviceWidth
    },
    type: "POST", 
    success: function(result){
      jq('#loadingImg').hide();
      jq("#"+resId).html(result);
      jq('#action').val("");
      jq('#secRev_form_'+sectNo).attr('disabled',true); 
      jq('#secRev_form_'+sectNo).prop('checked', false);
    },
  });
}
//
function saveChange(id, format, secId){
  var qusStr = '';
  var ansStr = '';
  var optStr = '';
  jq('#action').val('UPDATE');
  if(format == 'TEXT'){
    var txtId = jq('#TEXT_'+id);
    if(txtId && !isNullAndUndef(txtId.val())){
      
    }else{
      qusStr = id;
      ansStr = txtId.val();
      optStr = null;
    }   
  }else if(format == 'DROPDOWN'){
    if(id == '17'){
      ansStr = jq('#PHONE_17').val();
    }else{ansStr = null;}
    var divId = 'div_'+id+' select';
    jq('#'+divId).each(function(index) {
       if(jq(this).find(":selected").val() != "Please Choose"){
         qusStr = id;//this.id;
         optStr = this.value;
       }
     });
  }else if(format == 'DATE'){
    qusStr =  "13";
    var day = 'DATE_DD';
    var month = 'DATE_MM';
    var year = 'DATE_YYYY';
    var dayVal = jq('#'+day).val();
    var monthVal = jq('#'+month).val();
    var yearVal = jq('#'+year).val();
    var date = dayVal + "/" + monthVal + "/" + yearVal;
    ansStr = date;
    optStr = null;
  }else if(format == 'RADIO'){
    var hId = 'hid_';
  if(jq('#'+hId+id) && jq('#'+hId+id).val() != ''){
    qusStr = id;
    optStr = jq('#'+hId+id).attr('class');
    ansStr = null;
    }else{
        qusStr = id;
        optStr = null;
        ansStr = null;
      }
  var que1 = jq('#hid_1').val(); var que2 = jq('#hid_2').val(); var que3 = jq('#hid_3').val();
  if((que1 != '' && que2 != '' && que3 != '')&& (que1 != "NO" || que2 != "YES" || que3 != "NO")){jq('#action').val('INTL');}
  }
  //console.log('last--quesStr-->'+qusStr+'<-optStr-->'+optStr +"<----ansStr----->"+ansStr);
  var reviewFlag = 'Y';
  var basicInfoFormFlag = 'N';  
  if(secId == '1'){// to check the basic info form questions id ()
    basicInfoFormFlag = 'Y';  
  }
  /*
  if(id= '4'){
    var radIdVal = jq('#hid_'+id).val();
    if(radIdVal == 'NO'){
      qusStr = qusStr + ',5,6,7,';
      ansStr = ansStr + ',null,null,null,null,';
      optStr = optStr + ',,null,null,';
    } 
  }
  */
  var saveChangeFlag = validateFormData(format+'_'+id, format, reviewFlag, basicInfoFormFlag);
  if(saveChangeFlag){
    navigation(secId, secId+1, 'I', qusStr, ansStr, optStr, '', secId);
  }//reviewSection(secId);
  if(secId == 1){
    selectChange('div_5', '5');userRevBtnShow('4');enableRevSecBtn('form_1');
  }
}
//
function reviewSubmit(pageId, nexPageId, status, type){
 jq('#action').val('STATIC_CONTENT');
 jq('#htmlId').val('ACCEPTING_OFFER_TERMS_AND_CONDITION');
 gaEventLog('Accept Offer Form', 'Page 5', 'TERMS AND CONDITIONS');
 navigation(pageId, nexPageId, status);
}
//
function editQual(pageId,action){
  var act1 = isNotNullAnddUndef(action) ? action : "EDIT";  
  jq('#action').val(act1);
  navigation(pageId,pageId, 'I');
}
//
function saveQual(pageId, nxtPageId, status, action, markFlag){
  var submitFlag = valQualTC('qualTCId');
  enableMarkComplete('entryReqSecId');
  var qualConfirmFlag = "N";
  var completeAct = enableDisContiBtn();
  if(submitFlag){
  if(markFlag == 'Y'){jq('#pageNav_'+pageId).addClass('stp_saved');jq('#formStatusId').val('C');}
  
  if(markFlag != 'Y'){
    if(completeAct != 4){
      callLightBox("QUAL_ERROR##SPLIT##Please complete all the steps to continue##SPLIT##STEP_COMPLETE");
      return false;
    }
  }
  
  qualConfirmFlag = "Y";
  var qualSub = [];
  var arrLen = 0;  
  jq('.qualSubj').each(function(){        
    qualSub[arrLen] = jq(this).attr('id');
    arrLen = arrLen +1;
  });
  var secId = jq('#sect_'+pageId);
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  var qualSub1 = jq('#'+qualSub[0]).val();
  var qualSub2 = jq('#'+qualSub[1]).val();
  var qualSub3 = jq('#'+qualSub[2]).val();
  var qualArr = jq('#qualId').val();
  var formStatus = (isNotNullAnddUndef(jq('#formStatusId').val())) ? jq('#formStatusId').val() : "";
  var url = contextPath + "/clearing-match-maker-tool.html";
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  //
  
  if("Y" == markFlag){
    gaEventLog('Accept Offer Form', ('Page '+ pageId), 'Mark As Complete');
  }else if(jq('#gaUcasPoints')){
    gaEventLog('Accept Offer Form', 'Page 4', 'Continue');
    qualGaLogging('Accept Offer Form');
    var gaUcasPoints = jq('#gaUcasPoints').val();
    gaEventLog('Accept Offer Form', 'UCAS Points', gaUcasPoints);
  }
  jq('#loadingImg').show();
  //
    jq.ajax({
    url: url, 
    data: {
      "page-id": pageId, 
      "next-page-id": nxtPageId, 
      "status": formStatus,
      "action": action,
      "courseId": courseId,
      "opportunityId": opportunityId,
      "qualIdArr": qualArr,
      "qualSub1": qualSub1,
      "qualSub2": qualSub2,
      "qualSub3": qualSub3,
      "screenwidth": deviceWidth,
      "qualConfirmFlag": qualConfirmFlag,
      "htmlId": "ACCEPTING_OFFER_TERMS_AND_CONDITION"
    },
    type: "POST", 
    success: function(result){
      jq('#loadingImg').hide();
      if(result.indexOf("##SPLIT##")>-1){
        callLightBox(result);
      }else{
       jq("#middleContent").html(result);
      if(isNullAndUndef(secId) != ''){
        if(markFlag == 'Y'){
          var markAsPos = jq('#markAsComFlagId').offset().top;
          jq('body, html').animate({scrollTop: markAsPos});
        }else{
          window.scrollTo(secId, 100); 
        }
      }else{
        if(markFlag == 'Y'){
          var markAsPos = jq('#markAsComFlagId').offset().top;
          jq('body, html').animate({scrollTop: markAsPos});
        }else{
          jq(window).scrollTop(0);
        }
      }
      addTopCls();
      
    }
  
    }
  });
  }
}
//
function qualGaLogging(eventCat, fromPod){ 
  if('EDIT_QUAL_GA' == fromPod || 'NEW_USER' == fromPod){  
    var qualDes = [];
    var subDes = [];   
    var qual3DpLen = jq('[data-id=level_3_add_qualif]:visible');
    var qual2DpLen = jq('[data-id=level_2_add_qualif]:visible');
    for (var ql = 0; ql < qual3DpLen.length;ql++){     
     var lvl3SubIpLen = jq('[data-id=level_3_add_qualif]').eq(ql).find('[data-id=qual_sub_id]').filter(function(){return this.value;});
     qualDes[ql] = jq('#qualspan_'+ql).text();
     subDes[ql] = '';
     for(var sb = 0; sb < lvl3SubIpLen.length ; sb++){
       var subIpId = lvl3SubIpLen.eq(sb).attr('id');
       var subValIpId = subIpId.replace('qualSub','');
       var subNameVal = jq('#'+subIpId).val();
       var subIdVal  = jq('#sub'+subValIpId).val();
       var grdIdVal = jq('#grde'+subValIpId).val();
       if(subNameVal!= '' && subIdVal != '' && grdIdVal != ''){
         if(sb != lvl3SubIpLen.length - 1){
           subDes[ql] = subDes[ql] + subNameVal + '|' + grdIdVal + '|' ;
         }else{
           subDes[ql] = subDes[ql] + subNameVal + '|' + grdIdVal;
         }
       }
     }
     gaEventLog(eventCat, qualDes[ql], subDes[ql]);
    }
    if(qual2DpLen.length > 0){
      var lvl2SubIpLen = jq('[data-id=level_2_add_qualif]').find('[data-id=qual_sub_id]').filter(function(){return this.value;});;
      qualDes[0] = 'GCSE';
      subDes[0] = '';
      for(var sb = 0; sb < lvl2SubIpLen.length ; sb++){
       var subIpId = lvl2SubIpLen.eq(sb).attr('id');
       var subValIpId = subIpId.replace('qualSub','');
       var subNameVal = jq('#'+subIpId).val();
       var subIdVal  = jq('#sub'+subValIpId).val();
       var grdIdVal = jq('#grde'+subValIpId).val();
       if(subNameVal != '' && subIdVal != '' && grdIdVal != ''){
         if(sb != lvl2SubIpLen.length - 1){
          subDes[0] = subDes[0] + subNameVal + '|' + grdIdVal + '|';
         }else{
          subDes[0] = subDes[0] + subNameVal + '|' + grdIdVal;
         }
       }
      }
      gaEventLog(eventCat, qualDes[0], subDes[0]);
    }
  }else{
    if(jq('#qualId').length && jq('.gaQualDesc').length){
      var qualIds = jq('#qualId').val().split(',');
      var qualDes = jq('.gaQualDesc').last().val().split(',');
      //var qualIds = jq('.qualId').val().split(',');
      //var qualDes = jq('.gaQualDesc').val().split(',');
      for(var lp = 0; lp < qualIds.length; lp++){
        if(isNotNullAnddUndef(qualIds[lp])){
          var userSubGrde = jq('#gaSubGrde_'+qualIds[lp]).val();
          userSubGrde = isNotNullAnddUndef(userSubGrde) ? userSubGrde.substring(0, userSubGrde.length - 1) : "";
          var qualification = qualDes[lp];
          gaEventLog(eventCat, qualification, userSubGrde);
        }
      }
    }
  }
}
//
function closeLiBox(){
 jq(".rvbx_shdw,.rev_lbox .revcls a").click(function(event){
  //event.preventDefault();
  //jq(".rev_lbox").removeAttr("style");
  //jq(".rev_lbox").addClass("fadeOut").removeClass("fadeIn");
  //jq("html").removeClass("rvscrl_hid");
  });
}
//
function appendGradeDropdown(iloop, gdeId, selVal){
  var qualId  = jq('#qualsel_'+iloop).val();
  var data = jq('#qualGrde_'+iloop+'_'+qualId).val().split(',');
 
  //console.log('appendGradeDropdown--data-->'+data);
  var id = gdeId;
  //console.log('appendGradeDropdown--id-->'+id);
  jq.each(data, function(value) {
  //console.log('done-->'+data[value]);
  var $option = jq("<option/>", {
    value: data[value],
    text: data[value]
  });
  //console.log('-'+id+'--->'+$option);
  jq('#'+id).append($option);
  jq("#"+id).val(selVal);
  });
}
//
function addNewRowSub(qualSelId){
 var qualId = jq('#'+qualSelId).children("option:selected").val();
 appendQualDiv(jq('#'+qualSelId), 1);
}
//selecting the value of subject dropdown
function setSelectedVal(formId, id){
 var value = jq('#'+id).children("option:selected").text();
 jq('#'+formId).html(value);
}
//
function appendQualDiv(obj, addVal, loop, pageType, addQualFlag){
  var qualId = '1';
  if('add_Qual'==addQualFlag){
    qualId = '1';
  }else{
    qualId = obj.value;
  }
  var qualIdForQualArr = qualId;
  var subArrDataId = '';
  if(isNotNullAnddUndef(qualId) && qualId.indexOf('_gcse') > -1){
    var count = jq('#qualSubj_'+loop+'_'+qualId.substring(0, qualId.indexOf('_gcse'))+'_gcse').val();
    qualIdForQualArr = qualIdForQualArr.substring(0, qualId.indexOf('_gcse'));
    subArrDataId = '';
  }else{
    var count = jq('#qualSubj_'+loop+'_'+qualId).val();
    subArrDataId = 'data-id="level_3"';
  }
  if(isNotNullAnddUndef(addVal)){ 
    count = count + addVal;
    //console.log(count);
  }
  
  //jq('#qualspan_'+loop).html(jq('#qualsel_'+loop).children("option:selected").text());
  
  //setSelectedVal('qualsel_'+loop, 'qualspanu'+loop);
  var result = "";
  var resSt = '<div class="ucas_row grd_row" id="grdrow_'+loop+'"><div class="rmv_act" id="subSec_' + loop +'"><fieldset class="ucas_fldrw">';
  var resEd = '</fieldset></div>';
  //updating the hidden subjec count
  var hidSubCntId = 'countAddSubj_'+ loop;
  jq('#'+hidSubCntId).val(count);
  jq('#total_'+hidSubCntId).val(count);
  //var totSubCnt = 'total_' + hidSubCntId;
  var gradeVal = jq('#qualGrde_'+loop+'_'+qualId).val();
  var gdeArr = gradeVal.split(',');
  //var subTxt = '<div class="ucas_tbox w-390 tx-bx"><input type="text" name="sub_17" placeholder="Accounting" value="Accounting" class="frm-ele"></div>';
  //var gradeSpanSt = '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="1gradeSpan1">A</span><i class="fa fa-angle-down fa-lg"></i></div>';
  //var select = '<select class="ucas_slist" onchange="setSelectedVal();getUcasPointsAndScore('qualse1');" id="qualGrd_1_1"><option value="A*">A*</option><option value="A"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">A</font></font></option><option value="B">B</option><option value="C">C</option><option value="D">D</option><option value="E">E</option></select>';
  var gradeSpanEd = '</div>';
  //var removeLink = '<div class="add_fld"> <a id="1rSubRow3" onclick="javascript:removeSubRow(1,3);" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="images/cmm_close_icon.svg" title="Remove subject &amp; grade"></span></a></div>';
  result = '<div class="ucas_row grd_row" id="grdrow_'+loop+'">';
  //if('new_entry_page' == pageType){
    result += '<fieldset class="ucas_fldrw quagrd_tit"><div class="ucas_tbox tx-bx"><h5 class="cmn-tit txt-upr qual_sub">Subject</h5></div><div class="ucas_selcnt rsize_wdth">'
    +'<h5 class="cmn-tit txt-upr qual_grd">Grade</h5></div></fieldset>';
  //}
  var enOrDisTopNavFn = "enOrDisTopNav('offerEditQual');";
  for(lp = 0; lp < count; lp++){
   var removeSub = "'subSec_"+loop + "_" + lp +"'";
   var grdeArrId = "'grde_"+loop+"_"+lp+"'";
   var subTxtId = "'qualSub_"+loop+"_"+lp+"'";
   var subAjaxId = "'ajaxdiv_"+loop+"_"+lp+"'";
   var actionParam = "'QUAL_SUB_AJAX'";
   var grdeSpanId = "'span_grde_" + +loop+"_"+lp+"'";
   var qualGrdId = "'qualGrd_"+loop+"_"+lp+"'";
   var grdStr = "'GRADE'";
   var subValId = "'sub_"+loop+"_"+lp+"'";   
   var removeLinkImg = jq('#removeLinkImg').val();
   var countAddSubId = "'countAddSubj_"+loop+"'";
   var addSubstyle = "display:none";
    result += '<div class="rmv_act" id="subSec_'+loop + '_' + lp +'"><fieldset class="ucas_fldrw">'
    + '<div class="ucas_tbox w-390 tx-bx"><input type="text" data-id="qual_sub_id" id="qualSub_'+loop+'_'+lp+'" name="'+qualId + 'sub_'+ lp +'" placeholder="Enter subject" value="" onkeyup="cmmtQualSubjectList(this, '+subTxtId+', '+actionParam+', '+subAjaxId+');"  class="frm-ele" autocomplete="off"><div id="ajaxdiv_'+loop+'_'+lp+'" data-id="wugoSubAjx" class="ajaxdiv"></div></div>'
    + '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="span_grde_'+loop+'_'+lp+'">'+gdeArr[0]+'</span><i class="fa fa-angle-down fa-lg"></i></div>'
    + '<select class="ucas_slist" onchange="setGradeVal(this, '+grdeArrId+');getUcasPoints('+subValId+', '+grdStr+');" id="qualGrd_'+ loop +'_'+ lp +'"></select>'
    + '<script>appendGradeDropdown('+loop+', '+qualGrdId+'); </script>'
    + '</div>'
    + '<div class="add_fld"> <a id="1rSubRow3" onclick="removeSubject('+removeSub+', '+loop+');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="'+removeLinkImg+'" title="Remove subject &amp; grade"></span></a></div>'
    + resEd
    
     
     + '<input class="subjectArr" '+subArrDataId+' type="hidden" id="sub_'+loop+'_'+lp+'" value="" />'
     + '<input class="gradeArr" type="hidden" id="grde_'+loop+'_'+lp+'" value="'+gdeArr[0]+'" />'
     + '<input class="qualArr" type="hidden" id="qual_'+loop+'_'+lp+'" value="'+qualIdForQualArr+'" />'
     + '<input class="qualSeqArr" type="hidden" id="qual_'+loop+'_'+lp+'" value="'+(parseInt(loop)+1)+'" />';
  
  }    
  if(count < 6){
    addSubstyle = "display:block";
  }else{
    addSubstyle = "display:none";
  }
    result += '<div class="ad-subjt" id="addSubject_'+loop+'" style="'+addSubstyle+'">'
    + '<a class="btn_act bck fnrm" id="subAdd'+loop+'" onclick="addSubject('+qualIdForQualArr+','+countAddSubId+', '+loop+');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>'
    + '</div>';  
  
  result += '</div>';
  jq('#subGrdDiv'+loop).text('');
  jq('#subGrdDiv'+loop).append(result);
  //setTimeout(function(){eval(jq("#subGrdDiv").html());}, 4000);
  jq('#subGrdDiv'+loop).find("script").each(function(i) {
    //console.log('------>'+jq(this).text());
    eval(jq(this).text());
  });
  if(1 == count){
    jq('#grdrow_'+loop).find('a:first').hide();
  }else{jq('#grdrow_'+loop).find('a:first').show();}
  var selTextVal = jq('#qualsel_'+loop+' option:selected').text();
  if(loop != '18'){
    jq('#newQualDesc').val(selTextVal);
  }
  jq('#qualspan_'+loop).text(selTextVal);
  var qualSubListVal = createJSON();
  if(qualSubListVal.length > 0){
    getUcasPoints('', '', 'CHANGE_QUAL');
  }else{
    var crsReqUcasPts = jq('#coureReqTariffPts').val();
    jq('#ucasPt').text("0 points");
    jq('#labelUcasPt').html('I confirm I have correctly entered my qualification information and have achieved 0 points.');
    jq('#oldGrade').text("0");
    jq('#newGrade').text("0");
  }  
}
//
var maxSubLimit = 6;
//
function removeSubject(subDivId, loop){
  var removeVal = subDivId;
  var subId = 'qualSub_' + removeVal.replace('subSec_','');
  var subHid = 'sub_' + removeVal.replace('subSec_','');
  jq('#'+ removeVal).remove();
  jq('#'+ subHid).val('');
  var subCount = jq('#countAddSubj_' + loop).val();
  var count = subCount - 1;
  var hidSubCntId = 'countAddSubj_'+loop;
  jq('#'+hidSubCntId).val(count);
  if(count <= maxSubLimit) { jq('#addSubject_'+loop).show(); }
  if(1 == count){
    jq('#grdrow_'+loop).find('a:first').hide();
  }else{jq('#grdrow_'+loop).find('a:first').show();}
  getUcasPoints(subId,'','REMOVE_SUB');
}
//
function addSubject(qualId, subCntId, loop){
  var selectedQual = jq('#qualsel_'+loop).val();
  var subCount = jq('#' + subCntId).val();
  var totSubCnt = jq('#total_'+ subCntId).val();
  var subArrDataId = '';
  var qualIdForQualArr = selectedQual;
  result = '<div class="ucas_row grd_row" id="grdrow_'+qualId+''+loop+'">';
   //updating the hidden subjec count
  //var count = parseInt(subCount) + 1;
  //var hidSubCntId = subCntId;
  //jq('#'+hidSubCntId).val(count);
  if(isNotNullAnddUndef(selectedQual) && selectedQual.indexOf('_gcse') > -1){
    var gradeVal = jq('#qualGrde_'+loop+'_'+selectedQual).val();
    qualIdForQualArr = qualIdForQualArr.substring(0, selectedQual.indexOf('_gcse'));
    maxSubLimit = 20;
    subArrDataId = '';
  }else{
    var gradeVal = jq('#qualGrde_'+loop+'_'+selectedQual).val();
    if(qualId == 18) {maxSubLimit = 3;} 
    else { maxSubLimit = 6; }
    subArrDataId = 'data-id="level_3"';
  }
  var gdeArr = gradeVal.split(',');
  var enOrDisTopNavFn = "enOrDisTopNav('offerEditQual');";
  //
  if(subCount <= maxSubLimit) {
    var count = parseInt(subCount) + 1;
    var totCnt = parseInt(totSubCnt) + 1;
    var hidSubCntId = subCntId;
    jq('#'+hidSubCntId).val(count);
    jq('#total_'+hidSubCntId).val(totCnt);
//    for(var lp = subCount; lp <= 6; lp++){
//      var idCnt = parseInt(lp) + 1;
      var removeSub = "'subSec_"+loop + "_" + totCnt +"'";
      var grdeArrId = "'grde_"+loop+"_"+totCnt+"'";
      var subTxtId = "'qualSub_"+loop+"_"+totCnt+"'";
       var scrhIpId = "'qualSub_"+loop+"_"+totCnt+"'";
   var subAjaxId = "'ajaxdiv_"+loop+"_"+totCnt+"'";
   var actionParam = "'QUAL_SUB_AJAX'";
   var qualGrdId = "'qualGrd_"+loop+"_"+totCnt+"'";
    var grdStr = "'GRADE'";
    var subValId = "'sub_"+loop+"_"+totCnt+"'";
    var removeLinkImg = jq('#removeLinkImg').val();
      result = '<div class="rmv_act" id="subSec_'+loop + '_' + totCnt +'"><fieldset class="ucas_fldrw">'
      + '<div class="ucas_tbox w-390 tx-bx"><input type="text" data-id="qual_sub_id" id="qualSub_'+loop+'_'+totCnt+'" name="'+qualId + 'sub_'+ totCnt +'" placeholder="Enter subject" value="" onkeyup="cmmtQualSubjectList(this, '+scrhIpId+', '+actionParam+', '+subAjaxId+');"  class="frm-ele" autocomplete="off"><div id="ajaxdiv_'+loop+'_'+totCnt+'" data-id="wugoSubAjx" class="ajaxdiv"></div></div>'
      + '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="span_grde_' + loop +'_'+totCnt+'">'+gdeArr[0]+'</span><i class="fa fa-angle-down fa-lg"></i></div>'
      + '<select class="ucas_slist" onchange="setGradeVal(this, '+grdeArrId+');getUcasPoints('+subValId+', '+grdStr+');" id="qualGrd_'+ loop +'_'+ totCnt +'"></select>'
      + '<script>appendGradeDropdown('+loop+', '+qualGrdId+');</script>'
      + '</div>'
      + '<div class="add_fld"> <a id="1rSubRow3" onclick="removeSubject('+removeSub+', '+loop+');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="'+removeLinkImg+'" title="Remove subject &amp; grade"></span></a></div>'
     // + resEd;
    
    
    + '<input class="subjectArr" '+subArrDataId+' type="hidden" id="sub_'+loop+'_'+totCnt+'" value="" />'
     + '<input class="gradeArr" type="hidden" id="grde_'+loop+'_'+totCnt+'" value="'+gdeArr[0]+'" />'
     + '<input class="qualArr" type="hidden" id="qual_'+loop+'_'+totCnt+'" value="'+qualIdForQualArr+'" />'
     + '<input class="qualSeqArr" type="hidden" id="qual_'+loop+'_'+totCnt+'" value="'+(parseInt(loop)+1)+'" />';
    
    if(count == maxSubLimit){jq('#addSubject_'+loop).hide();} 
   // } 
  }else{ jq('#addSubject_'+loop).hide(); }
  result += '</div>';
  //jq('#subGrdDiv'+loop+ '.rmv_act').append(result);
  jq('#subGrdDiv'+loop).find('.rmv_act').last().after(result);
  jq('#subGrdDiv'+loop).find("script").each(function(i) {
    //console.log('------>'+jq(this).text());
    eval(jq(this).text());
  });
  if(count > 1){
    jq('#grdrow_'+loop).find('a:first').show();
  }
}
//
function createQusJSON(qusIds, ansVals, optIds){
  var qusArr = (qusIds != null && qusIds != '') ? qusIds.split(',') : '';
  var ansArr = (ansVals != null && ansVals != '') ? ansVals.split(',') : '';
  var optArr = (optIds != null && optIds != '') ? optIds.split(',') : '';
  
  var obj = [];
  for (i = 0; i < qusArr.length; i += 1) {
    var qus = qusArr[i];var ans = ansArr[i];var opt = optArr[i];
    //console.log('qus------>'+qus);
    //console.log('ans------>'+ans);
    //console.log('opt-------->'+opt);
    if(qus != ''){    
      tmp = {
        "qusId": qus,
        "ansValue": ans,
        "optionId": opt
      };
      obj.push(tmp);
    }
  }
}
//
function passOfferJSON(pageId, nxtPageId, status, resultQus, resultAnsArr, resultOptArr, type, secId){

  var formStatus = (isNotNullAnddUndef(jq('#formStatusId').val())) ? jq('#formStatusId').val() : "";
  var resId = (type == 'LB') ? "revLightBox" : isNullAndUndef(secId) ? sectionId : "middleContent";
  var sectionId = 'section_'+secId;
  var action = jq('#action').val();
  var htmlId = jq('#htmlId').val();
  var applicantCount = jq('#applicationCountId').val();
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  var offerName = jq('#offerName').val();
  if('top-nav'==status){formStatus= '';}
  
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  var param = "?page-id="+pageId+"&next-page-id="+nxtPageId+"&status="+formStatus
  +"&action="+action+"&htmlId="+htmlId+"&courseId="+courseId+"&opportunityId="+opportunityId+"&applicationCount="+applicantCount+"&offerName="+offerName+"&screenwidth="+deviceWidth;
  var stringJson =	{"qusAnsList": createQusJSON(resultQus, resultAnsArr, resultOptArr)};
  var jsonData = JSON.stringify(stringJson);
  var url = contextPath + "/clearing-match-maker-tool.html" + param;
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    dataType: 'json',
    type: "POST",
    data: jsonData,
    async: false,
	  complete: function(response){
	    if(isNotNullAnddUndef(response) && result.indexOf('GENERATE_APPLICATION') > -1){
        var accSubFlag = response.replace('GENERATE_APPLICATION_','')
        if('SUCCESS' == accSubFlag.trim()){
          offerSuccess('offerFormSuccess','7','8');
        }
      }
	    if('SECTION_REVIEW_FLAG' != response && 'GENERATE_APPLICATION_SUCCESS' != response){
        jq("#"+resId).html(response);
        jq('#action').val('');
        if(isNullAndUndef(secId) != '' && action != "INTL"){
          jq.scrollTo(jq('#'+sectionId), 100); 
        }else{
          jq(window).scrollTop(0);
        }
        addTopCls();
        if(type == 'LB'){
          lightBoxCall();
          revLightbox();
        }
      }else{
        if('SECTION_REVIEW_FLAG' == response){
          jq('#action').val('');
        }
      }
	  }
  });
}
//
function createJSON(){
  var qualId = '';jq('.qualArr').each(function(){qualId += this.value + ','; });
  var subId = '';jq('.subjectArr').each(function(){subId += this.value + ',';});
  var grade = '';jq('.gradeArr').each(function(){grade += this.value + ',';});
  var qualSeq = '';jq('.qualSeqArr').each(function(){qualSeq += this.value + ',';});
  var count = subId.length;
  //console.log("count-->"+count);
  var qualIdArr = (qualId != null && qualId != '') ? qualId.split(',') : '';
  var subIdArr = (subId != null && subId != '') ? subId.split(',') : '';
  var gradeArr = (grade != null && grade != '') ? grade.split(','): '';
  var qualSeqArr = (qualSeq != null && qualSeq != '') ? qualSeq.split(','): '';
  
  
  var obj = [];
  for (i = 0; i < qualIdArr.length; i += 1) {
    var qual = qualIdArr[i];var sub = subIdArr[i];var grde = gradeArr[i]; var lp = i+1;var qualSeq = qualSeqArr[i];
    //console.log('qual------>'+qual);
    //console.log('sub------>'+sub);
    //console.log('grde-------->'+grde);
    if(qual != '' && sub!= ''){    
    tmp = {
      "qualId": qual,
      "subId": sub,
      "grade": grde,
      "qualSeqId": qualSeq,
      "subSeqId": lp
    };

    obj.push(tmp);
    }
}
 //console.log("obj----->"+obj);
 return obj;
  
}
//
function removeQualification(qualDivId, preQualDivCnt, addQualBtnId){
  jq('#'+addQualBtnId).hide();
  jq('#'+qualDivId).hide();
  jq('#addQualBtn_'+preQualDivCnt).show();
  //reseting the values while removing
  var qualloop = preQualDivCnt+1;
  jq("#qualsel_"+qualloop).val("1");
  if(jq('#level_3_qual_1').css('display') == 'none' && jq('#level_3_qual_2').css('display') == 'none'){
    jq('#addQualBtn_0').show();
    jq('#addQualBtn_1').hide();
  }
  appendQualDiv(1, '', qualloop, 'new_entry_page', 'add_Qual');  
  //
}
//
function addQualification(qualDivId, nxtQualDivCnt, addQualBtnId){
  jq('#'+addQualBtnId).hide();
  jq('#'+qualDivId).show();
  jq('#addQualBtn_'+ nxtQualDivCnt).show();
  var qualloop = nxtQualDivCnt+1;
  if(jq('#level_3_qual_'+ qualloop).css('display') == 'block'){
    jq('#addQualBtn_'+ nxtQualDivCnt).hide();
  }  
  jq("#qualsel_"+nxtQualDivCnt).val("1");
  appendQualDiv(1, '', nxtQualDivCnt, 'new_entry_page', 'add_Qual');
  if(jq('#level_3_qual_'+nxtQualDivCnt).css('display')!='none' && jq('[data-id=level_3_add_qualif]').eq(1).attr('id') == ('level_3_qual_'+nxtQualDivCnt)){
    var curAddQualCont =  jq("[data-id=level_3_add_qualif]").eq(1);    
    jq("[data-id=level_3_add_qualif]").last().after(curAddQualCont);      
  }
}
//
function removeQualEntryReq(rmQualId, qualNo){
  if(qualNo == '17'){
    jq('#qualsel_'+qualNo).val('17_gcse_old_grade');
    var gcseSelObj = jq('#qualsel_'+qualNo).get(0);
    appendQualDiv(gcseSelObj, '', qualNo, '', '');
    //jq('#'+rmQualId).hide();
  }else{
    appendQualDiv(1, '', qualNo, '', 'add_Qual');  
    jq('#qualspan_'+qualNo).text('A Level');
    jq('#qualsel_'+qualNo).val('1');
    var preQualNo = parseInt(qualNo)-1;
    jq('#'+rmQualId).hide();
    jq('#addQualBtn_'+ preQualNo).show();
    if(jq('#add_qualif_1').css('display') == 'block'){
      jq('#addQualBtn_2').hide();
    }
    if(jq('#add_qualif_2').css('display') == 'block' && jq('#add_qualif_1').css('display') == 'none'){
      jq('#addQualBtn_2').show();
      jq('#addQualBtn_0').hide();
    }
    if(jq('#add_qualif_2').css('display') == 'none' && jq('#add_qualif_1').css('display') == 'none'){
      jq('#addQualBtn_0').show();
    }
  }
}
//
function addQualEntryReq(qualNo){
  //var entryReyQualCnt = jq('.add_qualif').length;
  var entryReyQualCnt = jq('[data-id=level_3_add_qualif]').length;
  if(entryReyQualCnt < 3 && jq('#add_qualif_'+ qualNo).length == 0){
    var addQualResult = '';
    var addQualDiv = '';
    var addQUalDivEnd = ''; 
    var add_qualif_id = "'add_qualif_"+qualNo+"'";
    var l3q_rw_id = "'qual_level_"+ qualNo +"'";
    var add_qual_no = "'"+qualNo+"'";
    var add_qual_sel_id = "qualsel_"+qualNo;
    var emp_str = "''";    
    var nxt_add_qualif_id = "'add_qualif_"+(parseInt(qualNo)+1)+"'";
    var qualSelOption = jq('#qualsel_0').html();
    var qualSubGrdHidId = jq('#qual_sub_grd_hidId').html();
    var toolTipText = jq('#toolTip_0').html();
    qualSubGrdHidId = qualSubGrdHidId.replace(/_0_/g, '_'+qualNo+'_');
    //console.log(qualSubGrdHidId);
     addQualDiv = '<div class="add_qualif" data-id="level_3_add_qualif" id="add_qualif_'+qualNo+'"><div class="ucas_refld"><h3 class="un-tit">Level 3 <span class="hlp tool_tip">'+toolTipText+'</span></h3><div class="l3q_rw" id="'+l3q_rw_id+'">'
     + '<div class="adqual_rw"><div class="ucas_row"><h5 class="cmn-tit txt-upr">Qualifications</h5><fieldset class="ucas_fldrw">'
     + '<div class="remq_cnt"><div class="ucas_selcnt"><fieldset class="ucas_sbox"><span class="sbox_txt" id="qualspan_'+qualNo+'">A Level </span><i class="fa fa-angle-down fa-lg"></i></fieldset>'
     + '<select name="qualification'+qualNo+'" onchange="appendQualDiv(this, '+emp_str+', '+add_qual_no+');" id="'+add_qual_sel_id+'" class="ucas_slist">'
     //+ '<option value="0">Please Select</option><option value="1" selected="selected">A Level </option><option value="2">AS Level </option><option value="8">BTEC Extended Diploma </option><option value="9">BTEC Diploma </option><option value="16">BTEC Foundation Diploma </option></select>'
     + qualSelOption 
     + '</select>'
     //+ '<input type="hidden" id="qualSubj_'+qualNo+'_1" name="qualSubj_1" value="3"><input type="hidden" id="qualGrde_'+qualNo+'_1" name="" value="A*,A,B,C,D,E"><input type="hidden" id="qualSubj_'+qualNo+'_2" name="qualSubj_2" value="1"><input type="hidden" id="qualGrde_'+qualNo+'_2" name="" value="A,B,C,D,E">'
     //+ '<input type="hidden" id="qualSubj_'+qualNo+'_8" name="qualSubj_8" value="1"><input type="hidden" id="qualGrde_'+qualNo+'_8" name="" value="D*D*D*,D*D*D,D*DD,DDD,DDM,DMM,MMM,MMP,MPP,PPP"><input type="hidden" id="qualSubj_'+qualNo+'_9" name="qualSubj_9" value="1">'								
     //+ '<input type="hidden" id="qualGrde_'+qualNo+'_9" name="" value="D*D*,D*D,DD,DM,MM,MP,PP"><input type="hidden" id="qualSubj_'+qualNo+'_16" name="qualSubj_16" value="1"><input type="hidden" id="qualGrde_'+qualNo+'_16" name="" value="D*,D,M,P">'								
     + qualSubGrdHidId
     + '</div></div><div class="add_fld" id ="removeQualbtn_'+qualNo+'" style="display:block;"><a id="remQual_'+qualNo+'" onclick="removeQualEntryReq('+add_qualif_id+', '+add_qual_no+');" class="btn_act1 bck f-14 pt-5">'
     + '<span class="hd-mob">Remove Qualification</span></a></div></fieldset><div class="err" id="qualSubId_'+qualNo+'_error" style="display:none;"></div></div>'								
     + '<div class="subgrd_fld" id="subGrdDiv'+qualNo+'"></div>'
     + '<input type="hidden" class="total_countAddSubj_'+qualNo+'" id="total_countAddSubj_'+qualNo+'" value="3"><input type="hidden" class="countAddSubj_'+qualNo+'" id="countAddSubj_'+qualNo+'" value="3">'
     addQUalDivEnd = '</div></div>';     
     if((parseInt(qualNo)+1) <= 3){
       var addQualNo = qualNo;
       if((parseInt(qualNo)+1) == 3){addQualNo = 0;}            
       var addQualBtnDiv = '<div class="add_qualtxt" id="addQualBtn_'+qualNo+'" style="display: block;"><a href="javascript:void(0);" onclick="addQualEntryReq('+(parseInt(addQualNo)+1)+')" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> ADD A QUALIFICATION</a></div></div></div>';
       addQUalDivEnd = addQUalDivEnd + addQualBtnDiv;
     }
     addQualResult = addQualDiv + addQUalDivEnd;
     
     //jq('.ucas_mid').find('.add_qualif').last().after(addQualResult);
     jq('.ucas_mid').find('[data-id=level_3_add_qualif]').last().after(addQualResult);
     var preQualCnt = parseInt(qualNo) - 1;
     jq('#addQualBtn_'+preQualCnt).hide();
     if((parseInt(qualNo)+1) > 2){
       jq('#addQualBtn_'+qualNo).hide();
     }
     appendQualDiv(1, '', qualNo, '', 'add_Qual');
     jq('#qualspan_'+qualNo).text('A Level');
     jq('#qualsel_'+qualNo).val('1');     
  }else{   
    if(parseInt(qualNo) < 3){
      jq('#add_qualif_'+ qualNo).show();
      jq('#addQualBtn_'+ qualNo).show();
      jq('#addQualBtn_'+ (parseInt(qualNo)-1)).hide();
    }else if(parseInt(qualNo) == 3){
      var addShowQualNo = 1;
      jq('#add_qualif_'+ addShowQualNo).show();
      jq('#addQualBtn_'+ (parseInt(qualNo)-1)).hide();
      qualNo = addShowQualNo;
    }
    if(entryReyQualCnt == 3){
      if(jq('#add_qualif_1').css('display') == 'block'){
        jq('#addQualBtn_2').hide();
      }
      if(jq('#add_qualif_2').css('display') == 'block'){
        jq('#addQualBtn_1').hide();
      }
      if(jq('#add_qualif_'+qualNo).css('display')!='none' && jq('[data-id=level_3_add_qualif]').eq(1).attr('id') == ('add_qualif_'+qualNo)){
        var curAddQualCont =  jq("[data-id=level_3_add_qualif]").eq(1);
        //jq(".add_qualif").last().after(curAddQualCont);
        jq("[data-id=level_3_add_qualif]").last().after(curAddQualCont);      
      }
    }
  }  
}
//
function onloadShowingAddQualBtn(){
  var l3QualCnt = jq('[data-id=level_3_add_qualif]').length;
  var addQualbtnCnt = jq('[data-id=level_3_add_qualif]').find('.add_qualtxt').length;
  if(l3QualCnt!= 3){
    jq("[data-id=level_3_add_qualif]").last().find('.add_qualtxt').show();     
  }else if(l3QualCnt == 3){
    jq('.add_qualtxt').hide();
  }
}
//
function getUcasPoints(subId, frm, removeSubject){
  var subVal = jq('#'+subId).val();
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  var action = 'CALCULATE_UCAS';
  var qualSubListVal = createJSON();
  var crsReqUcasPts = jq('#coureReqTariffPts').val();
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  if(qualSubListVal.length > 0){
    if((subVal != null && subVal != '') || (isNotNullAnddUndef(subVal) && frm == 'GRADE') || ('REMOVE_SUB' == removeSubject) || ('CHANGE_QUAL' == removeSubject)){
      var stringJson =	{
        "action": action,
        "courseId": courseId,
        "opportunityId": opportunityId,
        "qualSubList": createJSON()
      };
      var jsonData = JSON.stringify(stringJson);
       var param = "?action="+action +"&opportunityId="+opportunityId+"&courseId="+courseId+"&screenwidth="+deviceWidth;
       var url = contextPath + "/clearing-match-maker-tool-edit.html" + param;
       jq.ajax({
        url: url, 
        headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
        dataType: 'json',
        type: "POST",
        data: jsonData,
        async: false,
        complete: function(response){
      //alert("ucas-->"+response);
       if(response.responseText.indexOf("##SPLIT##")>-1){
         var result = response.responseText.split("##SPLIT##")[1];
         jq('#ucasPt').text(result + " points");
	       jq('#newGrade').val(result);
         jq('#labelUcasPt').html('I confirm I have correctly entered my qualification information and have achieved '+ result + ' points.');
         var quallConId = 'qualTCId';
         if(jq('#'+quallConId).length > 0){
           if(jq('#'+quallConId).prop("checked") == true){
             jq('#'+quallConId).prop("checked",false);                          
           }
           if('ACCEPTING_OFFER'==jq('#offerName').val()){
             enOrDisTopNav('ACCEPTING_OFFER_QUAL_EDIT');
           }
         }
       }  
      }
      });
    }
  }else{
    jq('#ucasPt').text("0 points");
    jq('#labelUcasPt').html('I confirm I have correctly entered my qualification information and have achieved 0 points.');
    jq('#oldGrade').text("0");
    jq('#newGrade').text("0");
  }
}
//
function valQualUpdate(action, id, subValFlag){
 var markAsComFlag = true;var sciErrFlag = false;
 var qualSelId = '';
 //if(jq('#'+id)){
   if("NO_VALIDATION" != subValFlag){
     var subId = '';
     var kwSubId = '';
     var kwErrFlag = false;
     jq('[data-id=level_3]').each(function(){ 
       var curSubId = this.id;
 if(this.value != '') {
    var qualLoop = curSubId.substring(curSubId.indexOf('_')+1, curSubId.lastIndexOf("_"));
    subId += this.value + '';
    qualSelId += jq('#qualsel_'+qualLoop).val() +',';
   }
  });
  if(qualSelId.indexOf('18') > -1) {
    sciErrFlag = true;
    qualSelId = qualSelId.substring(0, qualSelId.length - 1);
    var qualIdArr = qualSelId.split(',');
      if(qualIdArr == '18') {
     sciErrFlag = true;
    } else {
    for (i = 0; i < qualIdArr.length; i += 1) {
      if(qualIdArr[i] == '1' || qualIdArr[i] == '2' || qualIdArr[i] == '3' || qualIdArr[i] == '4') {
        sciErrFlag = false;
      }
    }
  }}
     //qual_sub_id
     jq('[data-id=qual_sub_id]').each(function(){ 
       kwSubId += this.value + '';  
       var qualSelId = "";
       var curSubId = this.id;
       var curHidId = curSubId.replace('qualSub_','sub_');      
       //console.log(this.id);
       var curSubVal = jq('#'+curSubId).val();
       var curHidVal = jq('#'+curHidId).val()
       if(curSubVal!='' && curHidVal ==''){
         markAsComFlag = false;
         kwErrFlag = true;         
       }
     });
     if(!isNotNullAnddUndef(subId)){
       markAsComFlag = false;      
       callLightBox("QUAL_ERROR##SPLIT##Please select a subject from the dropdown list##SPLIT##QUAL_VALIDATION");
     } else if(sciErrFlag) {  markAsComFlag = false;callLightBox("QUAL_ERROR##SPLIT##Please select a subject from the dropdown list##SPLIT##SCIENCE_PRACT_ERROR");  }
     else{
       if(kwErrFlag){
         callLightBox("QUAL_ERROR##SPLIT##Please select a subject from the dropdown list##SPLIT##KEYWORD_ERROR");         
       }else{         
         showErrorMsg('subErrMsg', '');         
       }
     }
   } 
   if('qualTCId'==id && jq('#'+id).length > 0){
     if(jq('#'+id).prop("checked") == true){       
       showErrorMsg(id+'_error', '');
     }else{
       showErrorMsg(id+'_error', error_qual);
       markAsComFlag = false;
     }
   }
   enableOrDisbaleBtn('markAsComFlagId', markAsComFlag);
   return markAsComFlag;
   
 //}
}
//
function passJSONObj(action, nxtPageId, reviewFlag, sectNo, valCheck, from, markFlag){
  var submitFlag = true;
  var checkValidation = false;
  var qualConfirmFlag = "N";
  if((("PROV_QUAL_UPDATE" == action) || "EDIT" == action || "USER_REVIEW_QUAL_UPDATE" == action)){
    checkValidation = true;
    submitFlag = false;
  }
  if(checkValidation){    
    submitFlag = valQualUpdate(action,'qualTCId', valCheck);
  }
  var oldGrade = jq('#oldGrade').val();
  var newGrade = jq('#newGrade').val();
  var oldQualDesc = jq('#oldQualDesc').val();
  if(isNotNullAnddUndef(oldQualDesc) && oldQualDesc.indexOf(',') > -1){
    oldQualDesc = (oldQualDesc != "") ? oldQualDesc.substring(0, oldQualDesc.length - 1) : "";
  }
  var fromPage = from;
  var newQualDesc = jq('#newQualDesc').val();
  if(isNotNullAnddUndef(newQualDesc) && newQualDesc.indexOf(',') > -1){
    newQualDesc = (newQualDesc != "") ? newQualDesc.substring(0, newQualDesc.length - 1) : "";
  }
  var offerName = jq('#offerName').val();
  if(submitFlag){
  var completeAct = enableDisContiBtn();
  if(markFlag == 'Y'){jq('#pageNav_4').addClass('stp_saved');jq('#formStatusId').val('C');}
  qualConfirmFlag = "Y";
   if(action != 'PROV_QUAL_UPDATE' && markFlag != 'Y' && action != 'ALTERNATIVE_COURSE'){
    if(completeAct != 4){
      callLightBox("QUAL_ERROR##SPLIT##Please complete all the steps to continue##SPLIT##STEP_COMPLETE");
      return false;
    }
  }
  
  
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  var formStatus = (isNotNullAnddUndef(jq('#formStatusId').val())) ? jq('#formStatusId').val() : '';
  var stringJson =	{
    "pageId":"4",									
    "userStatus": formStatus,
    "action": action,
    "qualConfirmFlag": qualConfirmFlag,
    "qualSubList": createJSON()
  };
  //level_3_add_qualif
  //level_2_add_qualif
  
  //qual_sub_id
  
	var jsonData = JSON.stringify(stringJson);
  var htmlId = jq('#htmlId').val();
  var sectionId = 'section_' + sectNo;
  var applicantCount = jq('#applicationCountId').val();
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  var resId = "middleContent";
  
  if(action == 'PROV_QUAL_UPDATE' || action == 'EDIT'){
    //gaEventLog('Apply Now Form', 'Page 4', 'Continue');
    if(jq('#gaUcasPoints')){
      qualGaLogging('Apply Now Form', from);
      var gaUcasPoints = jq('#gaUcasPoints').val();
      gaEventLog('Apply Now Form', 'UCAS Points', gaUcasPoints);
    }
    //wugoStatsLogging(action);
  }
  
  var param = "?action=" + action +"&next-page-id="+nxtPageId +"&review-flag="+reviewFlag +"&opportunityId="+opportunityId
  +"&courseId="+courseId+"&oldGrade="+oldGrade+"&newGrade="+newGrade+"&offerName="+offerName+"&oldQualDesc="+oldQualDesc+"&newQualDesc="+newQualDesc+"&screenwidth="+deviceWidth+"&markAsFlag="+markFlag; 
  var url = contextPath + "/clearing-match-maker-tool-edit.html" + param;
  jq('#loadingImg').show();
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    dataType: 'json',
    type: "POST",
    data: jsonData,
    async: false,
    complete: function(response){
      var result = response.responseText;
      jq('#loadingImg').hide();
      if(result.indexOf("##SPLIT##")>-1){
        result += "##SPLIT##" + fromPage;
        callLightBox(result);
      }else if(isNotNullAnddUndef(sectNo)){
        jq("#"+sectionId).html(result);
      }else{
        jq("#middleContent").html(result);
        addTopCls();
        if(markFlag == 'Y'){
          var markAsPos = jq('#markAsComFlagId').offset().top;
          jq('body, html').animate({scrollTop: markAsPos});
        }else{
          jq(window).scrollTop(0);
        }
        //
        
      //
      }
    
  }
 });
  }  
}
function timerExpiry(pageId, nxtPageId, status,timerStatus) {
  var formStatus = (isNotNullAnddUndef(jq('#formStatusId').val())) ? jq('#formStatusId').val() : '';
  var url = contextPath + "/clearing-match-maker-tool.html";
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
    jq('#loadingImg').show();
    jq.ajax({
    url: url, 
    data: {
      "page-id": pageId, 
      "next-page-id": nxtPageId, 
      "status": formStatus,
      "timerstatus":timerStatus,
      "courseId": courseId,
      "opportunityId": opportunityId,
      "screenwidth": deviceWidth
    },
    type: "POST", 
    success: function(result){
      jq('#loadingImg').hide();
      if(result.indexOf("##SPLIT##")>-1){
        callLightBox(result);
      }
   }  
});
}
//

//
function getAlternatePage(){
  //closeLiBox();
  jq(".rev_lbox").removeAttr("style");
  jq(".rev_lbox").addClass("fadeOut").removeClass("fadeIn");
  jq("html").removeClass("rvscrl_hid");
  jq('#action').val('ALTERNATIVE_COURSE');
  passJSONObj('ALTERNATIVE_COURSE');
  stopinterval();
}
function closeLigBox(){
  jq(".rev_lbox").removeAttr("style");
  jq(".rev_lbox").addClass("fadeOut").removeClass("fadeIn");
  jq("html").removeClass("rvscrl_hid");
}
//
jq(document).mouseup(function (e)
{
    ajaxOptHide(e);
    
});
//
function ajaxOptHide(e){
  var container = jq('[data-id=wugoSubAjx]');
  jq.each(container, function(key, value) {
      if (!jq(value).is(e.target) // if the target of the click isn't the container...
          && jq(value).has(e.target).length === 0) // ... nor a descendant of the container
      {
          jq(value).hide();
      }
  });
}
//
jq(document).ready(function() {
  var oldonkeydownWugo = document.body.onkeydown;
  if(typeof oldonkeydownWugo != 'function'){
    document.body.onkeydown=wugoAjaxOptionNavi;
  }else{
    document.body.onkeydown=function(){
      oldonkeydownWugo();
      wugoAjaxOptionNavi();
    }	
  }
 
  jq(document).on("click", "a", function(event){
    if(jq('#GAMPagename').val() == 'newEntryReqLandingPage.jsp' || jq('#GAMPagename').val() == 'whatunigolandingpage.jsp'){
      var e = event;
      if (!jq("#middleContent").is(e.target) // if the target of the click isn't the container...
          && jq("#middleContent").has(e.target).length === 0) // ... nor a descendant of the container
      {
        var a_href = jq(this).attr('href');      
        if("javascript:void(0)"!=a_href && ""!=a_href && "javascript:void(0);"!=a_href){
        jq('#exitUrlId').val(a_href);
          var currClass = event.target.className;
          var currPageId = jq('#currentPageId').val();
          var formStatusId = jq('#formStatusId').val();
          if(a_href != "" && isNotNullAnddUndef(a_href) && (currPageId == 1 || currPageId == 2 || currPageId == 3)){
            //alert("currClass-->"+currClass+"<---a_href--->"+a_href);
            jq('#loadingImg').show();
            callLightBox("SAVE_APPLICATION");
            jq('#loadingImg').hide();
            event.preventDefault();
          }
        }
      }
    }
  });
  
  jq('.un_logo clr15, .footer_bg,').click(function(e){
  if(jq('#GAMPagename').val() == 'newEntryReqLandingPage.jsp' || jq('#GAMPagename').val() == 'whatuniGoLandingPage.jsp'){
    //alert("Navigate");
    event.preventDefault();
  }
});
});
//
function deleteUserDataCall(){
  gaEventLog('Accept Offer Form', 'Save Application', 'no');
  closeLigBox();
  jq('#action').val("DELETE_USER_DATA");
  jq('#loadingImg').show();
  navigation('','','','','','','EXIT_LB');  
  jq('#loadingImg').hide();
  location.href=jq('#exitUrlId').val();
}
//
function savePageCall(){
  gaEventLog('Accept Offer Form', 'Save Application', 'yes');
  closeLigBox();
  var currPageId = jq('#currentPageId').val();
  jq('#loadingImg').show();
  if(currPageId == '1'){
    validateBasicInfo('1', '1', 'I');
  }else if(currPageId == '2'){
    formSubmit('userFormSubmit','','save_call');
  }else if(currPageId == '3'){
    contactFormSubmit('contactFormSubmit','','save_call');
  }  
  jq('#loadingImg').hide();
  location.href=jq('#exitUrlId').val();
}
//
function wugoAjaxOptionNavi(e){
	var ajaxOptionObj = jq('#ajax_listOfOptions').get(0);
  var ajaxListActiveItem = jq('#ajax_listOfOptions').find('.optionDivSelected').get(0);
  if(document.all)e = event;
	if(!ajaxOptionObj){
		return;
	}
	if(ajaxOptionObj.style.display == 'none'){
		return;
	}
	if(e.keyCode == 38){
		if(!ajaxListActiveItem){
			return;
		}
		if(ajaxListActiveItem && !ajaxListActiveItem.previousElementSibling){
			return;
		}
    if(ajaxListActiveItem.previousElementSibling){
      ajaxListActiveItem.className = 'optionDiv';
      ajaxListActiveItem.previousElementSibling.className = 'optionDivSelected';
	  }
		
	}
	if(e.keyCode == 40){
		if(!ajaxListActiveItem){
      var firstOptDiv = jq('#ajax_listOfOptions').find('.optionDiv').first().get(0);
      firstOptDiv.className = 'optionDivSelected';
      //jq('#ajax_listOfOptions').find('.optionDiv').first().removeClass('optionDiv');
			//jq('#ajax_listOfOptions').find('.optionDiv').first().addClass('optionDivSelected');
		}else{
			if(!ajaxListActiveItem.nextSibling){
				return;
			}      
			if(ajaxListActiveItem.nextElementSibling){
        ajaxListActiveItem.className = 'optionDiv';
        ajaxListActiveItem.nextElementSibling.className = 'optionDivSelected';
	    }
  }	}
	if(e.keyCode == 13 || e.keyCode == 9){
		if(ajaxListActiveItem && ajaxListActiveItem.className == 'optionDivSelected'){
			//ajax_option_setValue(e,ajax_list_activeItem);
      var selOptObj = jq('#ajax_listOfOptions').find('.optionDivSelected').get(0);
      setAjaxSubjectVal(selOptObj);
		}
    return true;
	}
	if(e.keyCode == 27){
    ajaxOptHide(e);
	}
}
//
function gotoBackSearch(){
 document.location.href = jq('#refererUrl').val(); 
}

function errorLiboxCall(type, errorCode){
  var resTxt = type+"##SPLIT##"+errorCode;
  callLightBox(resTxt);
}
function backToProvPage(){
  var action = 'prov_qual';
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  document.location.href = contextPath + "/clearing-match-maker-tool.html?&action-param=provisional_form&action="+action+"&courseId="+courseId+"&opportunityId="+opportunityId;
}
//
function backToOfferPage(){
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  var url = contextPath + "/clearing-match-maker-tool.html?&action-param=offer_form&courseId="+courseId+"&opportunityId="+opportunityId;
  document.location.href = url;
}
//
function shareFBTweet(type, from){
  var url = jq('#domainSpecPath').val() + "/university-clearing/";
  var courseName = jq('#courseName').val();
  var collegeDispName = jq('#collegeDispName').val();
  var collegeName = jq('#collegeName').val();
  var evntLabel = collegeName;
  var evntCat = "Accept Offer Form";
  var evntAct = (type == "FB") ? "Facebook Share" : "Twitter Share";
  if(from == "APP"){
    evntCat = "Application Page";
  }
  gaEventLog(evntCat, evntAct, evntLabel);
  
  var quote = "I've just applied for a place at "+ collegeDispName +" using #WhatuniGo. So excited to start my university journey here.";
  if(type == "FB"){
    window.open('https://www.facebook.com/sharer.php?u='+encodeURIComponent(url)+'&quote='+encodeURIComponent(quote), 'sharer', 'toolbar=0,width=626,height=436');
  }else if(type == "TWEET"){
    var twitterUrl = "http://www.twitter.com/intent/tweet?text=" + encodeURIComponent(quote);
    window.open(twitterUrl, '', 'menubar=no,toolbar=no,resizable=no,scrollbars=no,height=400,width=600');
  }
}
//
function gaEventLog(eventCategory, eventAction, eventLabel){
  //ga('send', 'event', eventCategory , eventAction , eventLabel);
  if(isNotNullAnddUndef(eventLabel)){
    GANewAnalyticsEventsLogging(eventCategory, eventAction, eventLabel.toLowerCase());
  }
}
//
function finalReviewBack(){
  gaEventLog('Accept Offer Form', 'Final Review', 'Back');
  navigation('5', '5', 'top-nav');
}
//
function backLink(pageNo, prevPageNo){
  if(prevPageNo != '' && prevPageNo != '4'){
    gaEventLog('Accept Offer Form', ('Page '+pageNo), 'Back');
    navigation(pageNo,prevPageNo,'back');
  }else if(prevPageNo == '4'){
    gaEventLog('Accept Offer Form', 'Final Review', 'Back');
    navigation('4', '4', 'back');
  }else{
    gaEventLog('Accept Offer Form', 'Page 1', 'Back');
    backToOfferPage();
  }
}
//
function wugoStatsLogging(action){
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  var courseId = jq('#applyNowCourseId').val();
  var collegeId = jq('#collegeId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  var url = contextPath + "/clearing-match-maker-tool.html"; 
  //action = "provisional-offer-loading";
    jq.ajax({
    url: url,
    async: false,
    data: {
      "courseId": courseId,
      "collegeId": collegeId,
      "opportunityId": opportunityId,
      "screenwidth": deviceWidth,
      "stats-param": "STATS_LOG",
      "action": action
    },
    type: "POST",
    success: function(result){
      //alert("result--->"+result);
    },
    error: function(result){
     //alert("result--->"+result);
    }
    
  });
}
//
jq(document).ready(function(){ 
  history.pushState(null, null, document.URL);
  window.addEventListener('popstate', function () {
    //var pageurl = jq('#refererUrl').val();
    var pageurl = document.URL;
    document.location.href = pageurl;
    //history.pushState(null, null, pageurl);
  });
});
//
function showLoadingImg(){
 jq('#loadingImg').show(); 
}
//
function contactGaLogging(){
 gaEventLog('Accept Offer Form', 'User Exit Point', 'Contact'); 
}
//
function cancelOfferCallGAlog(){
  var collegeName = jq('#collegeName').val();
  gaEventLog('Application Page', 'Cancel Confirmed Offer', collegeName);
  gaEventLog('WUGO Journey', 'Cancel Confirmed Offer', collegeName);
}
//
function articleGAlogging(eventAction){
  var collegeName = jq('#collegeName').val();
   var pageName = jq('#pageNameHidden').val();
  eventAction = 'Article Slot ' + eventAction;
  if(pageName == 'Application Page'){
  gaEventLog('Application Page', eventAction, collegeName);
  }
  else {
  gaEventLog('Accept Offer Form', eventAction, collegeName);
  }
}
//
function changeDefaultVal(selId){
  if(selId == 'div_18'){
    jq('#TEXT_19').val('');jq('#TEXT_20').val('');jq('#TEXT_21').val('');jq('#TEXT_22').val('');
    jq('#TEXT_19').removeClass('sucs');jq('#TEXT_21').removeClass('sucs');jq('#TEXT_22').removeClass('sucs');
  }else if(selId == 'div_5'){
    jq('#div_6 a').removeClass('bn-gry active').addClass('bn-outln');
    jq('#div_7 a').removeClass('bn-gry active').addClass('bn-outln');
    jq('#hid_6').val(""); jq('#hid_7').val("");
  }
}
function focusErrFld(){
  if(jq(".fail-msg:visible:first").length > 0){
    var posId = jq(".fail-msg:visible:first").attr('id').replace('_Err','');
        posId = jq('#'+posId).length > 0 ? posId : jq(".fail-msg:visible:first").attr('id');
    var pos = jq('#'+posId).offset().top - 50;
    jq('body, html').animate({scrollTop: pos});
  }
}
//
function callAcceptOffer(oppId, courseId){
  showLoadingImg();
  wugoStatsLogging('OFFER_FORM');
  showLoadingImg();
  window.location.href = contextPath + "/clearing-match-maker-tool.html?action-param=offer_form&courseId="+courseId+"&opportunityId="+oppId;
}
//

//
function automaticHyphenAfterThreeDigits(textId, onLoad) {
  var ucasId = jq('#' + textId).val();
  ucasId = ucasId.split('-').join('');
  if (ucasId != null && ucasId != '') {
    var finalValue = ucasId.match(/.{1,3}/g).join('-');
    var hyphenCount = finalValue.split('-').length;
    if (hyphenCount <= 3) {
      jq('#' + textId).val(finalValue);
    } 
    else if(onLoad == 'onLoad'){
      var lastIndex = finalValue.lastIndexOf('-');
      finalValue    = finalValue.substring(0,lastIndex) + finalValue.substring(lastIndex+1);
      jq('#' + textId).val(finalValue);
    }
  }
}
