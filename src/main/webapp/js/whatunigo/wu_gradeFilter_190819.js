var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";
var contextPath = "/degrees";
var jq = $.noConflict();
function isNullAndUndef(variable) {return (variable !== null && variable !== undefined && variable !== '');}
function isNotNullAnddUndef(variable) {return (variable !== null && variable !== undefined && variable !== '');}
var successFlag = 'Y';
function setGradeVal(obj,grdeArrId){
  var optVal = jq(obj).children("option:selected").val();
  var spanValId = 'span_'+ grdeArrId;
  jq('#'+grdeArrId).val(optVal);
  jq('#'+spanValId).text(optVal);
}

//
function setAjaxSubjectVal(ajaxOptionObj){
  var grandParentDivID = jq(ajaxOptionObj).parent().parent().parent().attr('id');
  var parentDivID = jq(ajaxOptionObj).parent().parent().attr('id');
  var subSrchIpID = parentDivID.replace('subjectAjaxList_', '');
  var subjectNameVal = jq(ajaxOptionObj).text().trim();
  var subjectIdVal = ajaxOptionObj.id;
  var subArrHidId = 'sub' + grandParentDivID.replace('ajaxdiv', '');
  jq('#'+subSrchIpID).val(subjectNameVal);
  jq('#'+grandParentDivID).hide();
  jq('#'+subArrHidId).val(subjectIdVal);
  getUcasPoints(subArrHidId);
  jq('#'+subSrchIpID).removeClass('faild');
  jq('#'+subSrchIpID).blur();
}
//
var srch_cur_let = new Array();


function cmmtQualSubjectList(obj, id, action, resultDivId){
 var url = contextPath + "/wugo/qualifications/search.html";
 var resId = resultDivId;
  action = 'QUAL_SUB_AJAX';
 var freeText = jq('#'+obj.id).val().trim();
 var subArrId = 'sub_'+ id.replace('qualSub_', '');
 jq('#'+subArrId).val('');
 if(srch_cur_let[obj.id] == freeText){
		return;
 }
 srch_cur_let[obj.id]= freeText;
 var qualSelId = id.substring(id.indexOf('_')+1, id.lastIndexOf("_"));
 var qualId = jq('#qualsel_'+qualSelId).children("option:selected").val();
 qualId = qualId.indexOf('gcse') > -1 ? qualId.substring(0, qualId.indexOf("_")) : qualId;
 if(isNotNullAnddUndef(freeText) && (freeText.length > 2 || qualId == '18')){ 
  //
  var stringJson =	{
    "action": action,
    "subQualId": qualId,
    "qualSubList": createJSON()
  };
  var jsonData = JSON.stringify(stringJson);
  var param = "?action="+action+"&free-text="+freeText;
  var url = contextPath + "/wugo/qualifications/search.html" + param;
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    dataType: 'json',
    type: "POST",
    data: jsonData,
    async: false,
    
    complete: function(response){
      var result = response.responseText;
      var selectDivId= "subjectAjaxList_"+id;
      if(result.trim() == ''){
        result = '<div class="nores">No results found for ' + freeText + '</div>';
      };
      var selectBox = '<div name="subjectAjaxList" onchange="" id='+selectDivId+' class=""><div id="ajax_listOfOptions" class="ajx_cnt">'+result+'</div></div>';
      jq('[data-id=wugoSubAjx]').each(function(){
        jq(this).html('');
      });
      jq("#"+resId).html(selectBox);
      var ajxwd = jq('#'+id).outerWidth() + 'px';     
      var ajxht = $$D(id).offsetHeight + 'px';      
      jq('#ajax_listOfOptions').css('top', ajxht);
      jq('#ajax_listOfOptions').css('width', ajxwd);
      jq("#"+resId).show();  
    }
  });
  //
  }
  else{
   jq('#'+resId).hide();
   if(!isNotNullAnddUndef(freeText)){
     jq('#'+id).removeClass('faild');
   }
  }
}
//
function callLightBox(result, params){
  var resultArr = result.split("##SPLIT##");
  var resId = "revLightBox";
  var loc = "";
  if(resultArr[0] == "QUAL_ERROR"){
    loc = "/jsp/whatunigo/include/entryReqLiBox.jsp";
  }else if(resultArr[0] == "TERMS_AND_COND"){
  }else if(resultArr[0] == "TIMER_EXPIRY"){
    loc = "/jsp/whatunigo/include/timerExpirySection.jsp";
  }
  var url = contextPath + loc;
  
  jq.ajax({
    url: url, 
    type: "POST", 
    success: function(result){
      jq("#"+resId).html(result);
      lightBoxCall();
      revLightbox();
    }
  });
}
//
function addTopCls(){
  jq('.frm-ele').each(function(index) { 
    var xy = jq(this).val();
    if (xy != "") {
      jq(this).next('label').addClass("top_lb");
    }else {
      jq(this).next('label').removeClass("top_lb");
    } 
  });
}
function lightBoxCall(){
 jq("#revLightBox").addClass("rev_lbox");
 jq(".rev_lbox").css({'z-index':'111111','visibility':'visible'});
 jq(".rev_lbox").addClass("fadeIn").removeClass("fadeOut");
 jq("html").addClass("rvscrl_hid");
}
jq(window).resize(function() {
  revLightbox();
});
jq(window).on('orientationchange', orientationChangeHandler);
function orientationChangeHandler(e) {
revLightbox();
}
function closeLigBox(){
  jq(".rev_lbox").removeAttr("style");
  jq(".rev_lbox").addClass("fadeOut").removeClass("fadeIn");
  jq("html").removeClass("rvscrl_hid");
  dev('.slqualif .ucas_slist').removeAttr("style");
}
function revLightbox(){
  var width = document.documentElement.clientWidth;
  var revnmehgt = jq(".rvbx_cnt .revlst_lft").outerHeight();
  var lbxhgt = jq(".rev_lbox .rvbx_cnt").height();
  var txth2hgt = jq(".rvbx_cnt h2").outerHeight();
  var txth3hgt = jq(".rvbx_cnt h3").outerHeight();
  if (width <= 480) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    jq(".lbx_scrl").css("height", +scrhgt+ "px");
   }else if ((width >= 481) && (width <= 992)) {
    var lbxmin = 35 + revnmehgt + txth2hgt + txth3hgt;
    var scrhgt = lbxhgt - lbxmin; 
    jq(".lbx_scrl").css("height", +scrhgt+ "px");
   }else{
     var lbxmin = 17 + txth2hgt + txth3hgt;
     var scrhgt = lbxhgt - lbxmin; 
     jq(".lbx_scrl").css("height", +scrhgt+ "px"); 
   }
}
function appendGradeDropdown(iloop, gdeId, selVal){
  var qualId  = jq('#qualsel_'+iloop).val();
  var data = jq('#qualGrde_'+iloop+'_'+qualId).val().split(',');
 
  //console.log('appendGradeDropdown--data-->'+data);
  var id = gdeId;
  //console.log('appendGradeDropdown--id-->'+id);
  jq.each(data, function(value) {
  //console.log('done-->'+data[value]);
  var $option = jq("<option/>", {
    value: data[value],
    text: data[value]
  });
  //console.log('-'+id+'--->'+$option);
  jq('#'+id).append($option);
  jq("#"+id).val(selVal);
  });
}

function addNewRowSub(qualSelId){
 var qualId = jq('#'+qualSelId).children("option:selected").val();
 appendQualDiv(jq('#'+qualSelId), 1);
}
//selecting the value of subject dropdown
function setSelectedVal(formId, id){
 var value = jq('#'+id).children("option:selected").text();
 jq('#'+formId).html(value);
}
//
function appendQualDiv(obj, addVal, loop, pageType, addQualFlag){
  var qualId = '1';
  if('add_Qual' == addQualFlag){
    qualId = '1';
  }else if('add_Qual_gcse' == addQualFlag){
    qualId = '17_gcse_old_grade';
  }else{
    qualId = obj.value;
  }
  var qualIdForQualArr = qualId;
  var subArrDataId = '';
  if(isNotNullAnddUndef(qualId) && qualId.indexOf('_gcse') > -1){
    var count = jq('#qualSubj_'+loop+'_'+qualId.substring(0, qualId.indexOf('_gcse'))+'_gcse').val();
    qualIdForQualArr = qualIdForQualArr.substring(0, qualId.indexOf('_gcse'));
    subArrDataId = '';
  }else{
    var count = jq('#qualSubj_'+loop+'_'+qualId).val();
    subArrDataId = 'data-id="level_3"';
  }
  if(isNotNullAnddUndef(addVal)){ 
    count = count + addVal;
    //console.log(count);
  }
  
  //jq('#qualspan_'+loop).html(jq('#qualsel_'+loop).children("option:selected").text());
  
  //setSelectedVal('qualsel_'+loop, 'qualspanu'+loop);
  var result = "";
  var resSt = '<div class="ucas_row grd_row" id="grdrow_'+loop+'"><div class="rmv_act" id="subSec_' + loop +'"><fieldset class="ucas_fldrw">';
  var resEd = '</fieldset></div>';
  //updating the hidden subjec count
  var hidSubCntId = 'countAddSubj_'+ loop;
  jq('#'+hidSubCntId).val(count);
  jq('#total_'+hidSubCntId).val(count);
  //var totSubCnt = 'total_' + hidSubCntId;
  var gradeVal = jq('#qualGrde_'+loop+'_'+qualId).val();
  var gdeArr = gradeVal.split(',');
  //var subTxt = '<div class="ucas_tbox w-390 tx-bx"><input type="text" name="sub_17" placeholder="Accounting" value="Accounting" class="frm-ele"></div>';
  //var gradeSpanSt = '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="1gradeSpan1">A</span><i class="fa fa-angle-down fa-lg"></i></div>';
  //var select = '<select class="ucas_slist" onchange="setSelectedVal();getUcasPointsAndScore('qualse1');" id="qualGrd_1_1"><option value="A*">A*</option><option value="A"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">A</font></font></option><option value="B">B</option><option value="C">C</option><option value="D">D</option><option value="E">E</option></select>';
  var gradeSpanEd = '</div>';
  //var removeLink = '<div class="add_fld"> <a id="1rSubRow3" onclick="javascript:removeSubRow(1,3);" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="images/cmm_close_icon.svg" title="Remove subject &amp; grade"></span></a></div>';
  result = '<div class="ucas_row grd_row" id="grdrow_'+loop+'">';
  //if('new_entry_page' == pageType){
    result += '<fieldset class="ucas_fldrw quagrd_tit"><div class="ucas_tbox tx-bx"><h5 class="cmn-tit txt-upr qual_sub">Subject</h5></div><div class="ucas_selcnt rsize_wdth">'
    +'<h5 class="cmn-tit txt-upr qual_grd">Grade</h5></div></fieldset>';
  //}
  var enOrDisTopNavFn = "enOrDisTopNav('offerEditQual');";
  for(lp = 0; lp < count; lp++){
   var removeSub = "'subSec_"+loop + "_" + lp +"'";
   var grdeArrId = "'grde_"+loop+"_"+lp+"'";
   var subTxtId = "'qualSub_"+loop+"_"+lp+"'";
   var subAjaxId = "'ajaxdiv_"+loop+"_"+lp+"'";
   var actionParam = "'QUAL_SUB_AJAX'";
   var grdeSpanId = "'span_grde_" + +loop+"_"+lp+"'";
   var qualGrdId = "'qualGrd_"+loop+"_"+lp+"'";
   var grdStr = "'GRADE'";
   var subValId = "'sub_"+loop+"_"+lp+"'";   
   var removeLinkImg = jq('#removeLinkImg').val();
   var countAddSubId = "'countAddSubj_"+loop+"'";
   var addSubstyle = "display:none";
    result += '<div class="rmv_act" id="subSec_'+loop + '_' + lp +'"><fieldset class="ucas_fldrw">'
    + '<div class="ucas_tbox w-390 tx-bx"><input type="text" data-id="qual_sub_id" id="qualSub_'+loop+'_'+lp+'" name="'+qualId + 'sub_'+ lp +'" placeholder="Enter subject" value="" onblur="getUcasPoints('+subValId+', '+grdStr+');" onkeyup="cmmtQualSubjectList(this, '+subTxtId+', '+actionParam+', '+subAjaxId+');"  class="frm-ele" autocomplete="off"><div id="ajaxdiv_'+loop+'_'+lp+'" data-id="wugoSubAjx" class="ajaxdiv"></div></div>'
    + '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="span_grde_'+loop+'_'+lp+'">'+gdeArr[0]+'</span><i class="fa fa-angle-down fa-lg"></i></div>'
    + '<select class="ucas_slist" onchange="setGradeVal(this, '+grdeArrId+');getUcasPoints('+subValId+', '+grdStr+');" id="qualGrd_'+ loop +'_'+ lp +'"></select>'
    + '<script>appendGradeDropdown('+loop+', '+qualGrdId+'); </script>'
    + '</div>'
    + '<div class="add_fld"> <a id="1rSubRow3" onclick="removeSubject('+removeSub+', '+loop+');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="'+removeLinkImg+'" title="Remove subject &amp; grade"></span></a></div>'
    + resEd
    
     
     + '<input class="subjectArr" '+subArrDataId+' type="hidden" id="sub_'+loop+'_'+lp+'" value="" />'
     + '<input class="gradeArr" type="hidden" id="grde_'+loop+'_'+lp+'" value="'+gdeArr[0]+'" />'
     + '<input class="qualArr" type="hidden" id="qual_'+loop+'_'+lp+'" value="'+qualIdForQualArr+'" />'
     + '<input class="qualSeqArr" type="hidden" id="qual_'+loop+'_'+lp+'" value="'+(parseInt(loop)+1)+'" />';
  
  }    
  if(count < 6){
    addSubstyle = "display:block";
  }else{
    addSubstyle = "display:none";
  }
    result += '<div class="ad-subjt" id="addSubject_'+loop+'" style="'+addSubstyle+'">'
    + '<a class="btn_act bck fnrm" id="subAdd'+loop+'" onclick="addSubject('+qualIdForQualArr+','+countAddSubId+', '+loop+');"> <span class="fl plus">+</span><span class="fl ad_sub">Add subject</span></a>'
    + '</div>';  
  
  result += '</div>';
  jq('#subGrdDiv'+loop).text('');
  jq('#subGrdDiv'+loop).append(result);
  //setTimeout(function(){eval(jq("#subGrdDiv").html());}, 4000);
  jq('#subGrdDiv'+loop).find("script").each(function(i) {
    //console.log('------>'+jq(this).text());
    eval(jq(this).text());
  });
  if(1 == count){
    jq('#grdrow_'+loop).find('a:first').hide();
  }else{jq('#grdrow_'+loop).find('a:first').show();}
  if('add_Qual' == addQualFlag || 'add_Qual_gcse' == addQualFlag){
    jq('#qualsel_'+loop).val(qualId);
  }
  var selTextVal = jq('#qualsel_'+loop+' option:selected').text();  
  jq('#newQualDesc').val(selTextVal);
  jq('#qualspan_'+loop).text(selTextVal);
  var qualSubListVal = createJSON();
  if(qualSubListVal.length > 0){
    getUcasPoints('', '', 'CHANGE_QUAL');
  }else{
    var crsReqUcasPts = jq('#coureReqTariffPts').val();
   // jq('#ucasPt').text("0 points");
    //jq('#labelUcasPt').html('I confirm I have correctly entered my qualification information and have achieved 0 points.');
    jq('#oldGrade').text("0");
    jq('#newGrade').text("0");
  }  
}
//
var maxSubLimit = 6;
//

function removeSubject(subDivId, loop){
  var removeVal = subDivId;
  var subId = 'qualSub_' + removeVal.replace('subSec_','');  
  jq('#'+ removeVal).remove();
 var res = removeVal.substring(6);
    jq('#sub'+res).val('');
    jq('#grde'+res).val('');
    jq('#qual'+res).val('');
    jq('#qual'+res).val('');
    var rmvUcas = "sub"+res;
    getUcasPoints(rmvUcas,'', 'REMOVE_SUB');
  var subCount = jq('#countAddSubj_' + loop).val();
  var count = subCount - 1;
  var hidSubCntId = 'countAddSubj_'+loop;
  jq('#'+hidSubCntId).val(count);
  if(count <= maxSubLimit) { jq('#addSubject_'+loop).show(); }
  if(1 == count){
    jq('#grdrow_'+loop).find('a:first').hide();
  }else{jq('#grdrow_'+loop).find('a:first').show();}
 // getUcasPoints(subId,'','REMOVE_SUB');
}
//
//selecting the value of subject dropdown
function setSelectedVal(formId, id){
 var value = jq('#'+id).children("option:selected").text();
 var selecIndex = jq('#'+id).children("option:selected").val();
 jq('#'+formId).html(value);
 var loop = formId.substring(6);
 jq('#userQualId'+loop).val();
 getUcasPoints('','', 'REMOVE_SUBJECT');
}
function addSubject(qualId, subCntId, loop){
  var selectedQual = jq('#qualsel_'+loop).val();
  var subCount = jq('#' + subCntId).val();
  var totSubCnt = jq('#total_'+ subCntId).val();
  var subArrDataId = '';
  var qualIdForQualArr = selectedQual;
  result = '<div class="ucas_row grd_row" id="grdrow_'+qualId+''+loop+'">';
   //updating the hidden subjec count
  //var count = parseInt(subCount) + 1;
  //var hidSubCntId = subCntId;
  //jq('#'+hidSubCntId).val(count);
  if(isNotNullAnddUndef(selectedQual) && selectedQual.indexOf('_gcse') > -1){
    var gradeVal = jq('#qualGrde_'+loop+'_'+selectedQual).val();
    qualIdForQualArr = qualIdForQualArr.substring(0, selectedQual.indexOf('_gcse'));
    maxSubLimit = 20;
    subArrDataId = '';
  }else{
    var gradeVal = jq('#qualGrde_'+loop+'_'+selectedQual).val();
    if(qualId == 18) {maxSubLimit = 3;} 
    else { maxSubLimit = 6; }
    subArrDataId = 'data-id="level_3"';
  }
  var gdeArr = gradeVal.split(',');
  var enOrDisTopNavFn = "enOrDisTopNav('offerEditQual');";
  //
  if(subCount <= maxSubLimit) {
    var count = parseInt(subCount) + 1;
    var totCnt = parseInt(totSubCnt) + 1;
    var hidSubCntId = subCntId;
    jq('#'+hidSubCntId).val(count);
    jq('#total_'+hidSubCntId).val(totCnt);
//    for(var lp = subCount; lp <= 6; lp++){
//      var idCnt = parseInt(lp) + 1;
      var removeSub = "'subSec_"+loop + "_" + totCnt +"'";
      var grdeArrId = "'grde_"+loop+"_"+totCnt+"'";
      var subTxtId = "'qualSub_"+loop+"_"+totCnt+"'";
       var scrhIpId = "'qualSub_"+loop+"_"+totCnt+"'";
   var subAjaxId = "'ajaxdiv_"+loop+"_"+totCnt+"'";
   var actionParam = "'QUAL_SUB_AJAX'";
   var qualGrdId = "'qualGrd_"+loop+"_"+totCnt+"'";
    var grdStr = "'GRADE'";
    var subValId = "'sub_"+loop+"_"+totCnt+"'";
    var removeLinkImg = jq('#removeLinkImg').val();
      result = '<div class="rmv_act" id="subSec_'+loop + '_' + totCnt +'"><fieldset class="ucas_fldrw">'
      + '<div class="ucas_tbox w-390 tx-bx"><input type="text" data-id="qual_sub_id" id="qualSub_'+loop+'_'+totCnt+'" name="'+qualId + 'sub_'+ totCnt +'" placeholder="Enter subject" value="" onblur="getUcasPoints('+subValId+', '+grdStr+');" onkeyup="cmmtQualSubjectList(this, '+scrhIpId+', '+actionParam+', '+subAjaxId+');"  class="frm-ele" autocomplete="off"><div id="ajaxdiv_'+loop+'_'+totCnt+'" data-id="wugoSubAjx" class="ajaxdiv"></div></div>'
      + '<div class="ucas_selcnt rsize_wdth"><div class="ucas_sbox mb-ht w-84" id="1grades1"><span class="sbox_txt" id="span_grde_' + loop +'_'+totCnt+'">'+gdeArr[0]+'</span><i class="fa fa-angle-down fa-lg"></i></div>'
      + '<select class="ucas_slist" onchange="setGradeVal(this, '+grdeArrId+');getUcasPoints('+subValId+', '+grdStr+');" id="qualGrd_'+ loop +'_'+ totCnt +'"></select>'
      + '<script>appendGradeDropdown('+loop+', '+qualGrdId+');</script>'
      + '</div>'
      + '<div class="add_fld"> <a id="1rSubRow3" onclick="removeSubject('+removeSub+', '+loop+');" class="btn_act1 bck f-14 pt-5"><span class="hd-mob">Remove</span><span class="hd-desk"><img class="aq_img" src="'+removeLinkImg+'" title="Remove subject &amp; grade"></span></a></div>'
     // + resEd;
    
    
    + '<input class="subjectArr" '+subArrDataId+' type="hidden" id="sub_'+loop+'_'+totCnt+'" value="" />'
     + '<input class="gradeArr" type="hidden" id="grde_'+loop+'_'+totCnt+'" value="'+gdeArr[0]+'" />'
     + '<input class="qualArr" type="hidden" id="qual_'+loop+'_'+totCnt+'" value="'+qualIdForQualArr+'" />'
     + '<input class="qualSeqArr" type="hidden" id="qual_'+loop+'_'+totCnt+'" value="'+(parseInt(loop)+1)+'" />';
    
    if(count == maxSubLimit){jq('#addSubject_'+loop).hide();} 
   // } 
  }else{ jq('#addSubject_'+loop).hide(); }
  result += '</div>';
  //jq('#subGrdDiv'+loop+ '.rmv_act').append(result);
  jq('#subGrdDiv'+loop).find('.rmv_act').last().after(result);
  jq('#subGrdDiv'+loop).find("script").each(function(i) {
    //console.log('------>'+jq(this).text());
    //eval(jq(this).text());
  });
  if(count > 1){
    jq('#grdrow_'+loop).find('a:first').show();
  }
}
//
function createJSON(){
  var qualId = '';jq('.qualArr').each(function(){qualId += this.value + ','; });
  var subId = '';jq('.subjectArr').each(function(){subId += this.value + ',';});
  var grade = '';jq('.gradeArr').each(function(){grade += this.value + ',';});
  var qualSeq = '';jq('.qualSeqArr').each(function(){qualSeq += this.value + ',';});
  var count = subId.length;
  var qualIdArr = (qualId != null && qualId != '') ? qualId.split(',') : '';
  var subIdArr = (subId != null && subId != '') ? subId.split(',') : '';
  var gradeArr = (grade != null && grade != '') ? grade.split(','): '';
  var qualSeqArr = (qualSeq != null && qualSeq != '') ? qualSeq.split(','): '';
   
  var obj = [];
  for (i = 0; i < qualIdArr.length; i += 1) {
    var qual = qualIdArr[i];var sub = subIdArr[i];var grde = gradeArr[i]; var lp = i+1;var qualSeq = qualSeqArr[i];
     if(qual != '' && sub!= ''){  
    tmp = {
      "qualId": qual,
      "subId": sub,
      "grade": grde,
      "qualSeqId": qualSeq,
      "subSeqId": lp
    };

    obj.push(tmp);
    }
}
 
 return obj;
  
}
function getUcasPoints(subId, frm, removeSubject){
  var subVal = jq('#'+subId).val();
  var maxUcasPoints = jq('#ucasMaxPoints').val();
  var action = 'CALCULATE_UCAS_POINTS';
   var qualSubListVal = createJSON();
   if((qualSubListVal.length > 0) || (subVal != null && subVal != "") || (isNotNullAnddUndef(subVal) && frm == 'GRADE') || ('REMOVE_SUBJECT' == removeSubject) || ('REMOVE_QUAL' == removeSubject) || ('CHANGE_QUAL' == removeSubject)){
    var stringJson =	{
    "action": action,
    "qualSubList": createJSON()
  };
  var jsonData = JSON.stringify(stringJson);
  var param = "?action="+action;
  var url = contextPath + "/wugo/qualifications/search.html" + param;
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    dataType: 'json',
    type: "POST",
    data: jsonData,
    async: false,
    success: function(response){
  },
  complete: function(response){
  if(response.responseText.indexOf("##SPLIT##")>-1){
    var result = response.responseText.split("##SPLIT##")[1];
    var res = isNotNullAnddUndef(result) ? result :"0";
    jq('#ucasPt').text(result);
    jq('#userUcasScore').val(result);
   // jq('#ucasVal').text("Your UCAS score " + result + " points");
    updateSlider(res);
    var ucasArrowChnge = ((parseInt(result))/maxUcasPoints * 100) - 2;
    if(parseInt(ucasArrowChnge) <= 100) {
     var ucasArrowPtr = ucasArrowChnge;
    }
    else {
      var ucasArrowPtr = 100- 2;
    }
    jq('#ucasArrow').attr('style',"left:" + ucasArrowPtr+"%");
  }
  }
  });
  }

else {
    sliderUpdate();
}
}

function ucasArrow(result) {
var ucasPoints = jq('#ucasPt').text().trim();
var ucasScore = parseInt(ucasPoints);
var maxUcasPoints = jq('#ucasMaxPoints').val();
var minScore = jq('#minSliderVal').text().trim();
var maxScore = jq('#maxSliderVal').text().trim();
  var ucasArrowChnge = ((parseInt(ucasPoints))/maxUcasPoints * 100) - 2;
    if(parseInt(ucasArrowChnge) <= 100) {
     var ucasArrowPtr = ucasArrowChnge;
    }
    else {
      var ucasArrowPtr = 100- 2;
    }
    jq('#ucasArrow').attr('style',"left:" + ucasArrowPtr+"%");
var ucasPt = isNotNullAnddUndef(ucasPoints) ? ucasPoints :"0";
   jq("#cmm_rngeSlider").ionRangeSlider({
   type: "double",
   grid: true,
   min: 0,
   max: maxUcasPoints,
   from: minScore,
   to: maxScore,
//   min_interval:10,
//   from_shadow:5,
//   to_shadow:5,
   prefix: "",
   onChange: function getMinMaxSliderVal(data) {
     var minVal = data.from;
     var maxVal = data.to;
     jq('#minSliderVal').text(minVal);
     jq('#maxSliderVal').text(maxVal);
     var errorMsgVal = '<p id="errSlider">We will not display any Apply buttons for courses which need more than '+ucasPoints+' UCAS points. However we will still show the courses and you are able to call the university directly if you' +"'" +'re confident of gaining a place with your current grades.</p>';
     if(maxVal > ucasPoints && ucasScore != 0) {
      jq("#gradeForm").addClass("errinf");
      jq('#errSlider').remove();
      jq('#ucasVal').append(errorMsgVal);
      jq('#ucasVal').show();
    }
    else if(maxVal <= ucasPoints){
      jq("#gradeForm").removeClass("errinf");
      jq('#ucasVal').hide();
      jq('#errSlider').remove();
    }
   },
   onUpdate: function getMinMaxSliderVal(data) {
     var minVal = data.from;
     var maxVal = data.to;
     jq('#minSliderVal').text(minVal);
     jq('#maxSliderVal').text(maxVal);
   }
});
}
function updateSlider(result) {
 var res = isNotNullAnddUndef(result) ? result : 0;
 var ucasPoints = jq('#ucasPt').text().trim();
 var ucasScore = parseInt(ucasPoints);
 var slider = jq("#cmm_rngeSlider").data("ionRangeSlider");
   slider.update({
        from: 0,
        to: res,
//        min_interval:10,
//         from_shadow:5,
//         to_shadow:5,
        onChange: function getMinMaxSliderVal(data) {
     var minVal = data.from;
     var maxVal = data.to;
     jq('#minSliderVal').text(minVal);
     jq('#maxSliderVal').text(maxVal);
     var errorMsgVal = '<p id="errSlider">We will not display any Apply buttons for courses which need more than '+ucasPoints+' UCAS points. However we will still show the courses and you are able to call the university directly if you' +"'" +'re confident of gaining a place with your current grades.</p>';
     if(maxVal > ucasPoints && ucasScore != 0) {
      jq("#gradeForm").addClass("errinf");
      jq('#errSlider').remove();
      jq('#ucasVal').append(errorMsgVal);
      jq('#ucasVal').show();
    } else if(maxVal <= ucasPoints){
      jq('#ucasVal').hide();
      jq("#gradeForm").removeClass("errinf");
      jq('#errSlider').remove();
    }
    }
    });
    ucasArrow(res);
}
function sliderUpdate() {
  updateSlider();
  jq('#ucasPt').text('0');
   //jq('#ucasVal').text("Your UCAS score : 0 points");
  var ucasArrowPtr = "-2";
  jq('#ucasArrow').attr('style',"left:" + ucasArrowPtr+"%"); 
  jq("#gradeForm").removeClass("errinf");
  jq('#ucasVal').hide();
}

function removeQualification(qualDivId, preQualDivCnt, addQualBtnId){
  jq('#'+addQualBtnId).hide();
  jq('#'+qualDivId).hide();
  jq('#addQualBtn_'+preQualDivCnt).show();
  //reseting the values while removing
  var qualloop = preQualDivCnt+1;
  jq("#qualsel_"+qualloop).val("1");
  if(jq('#level_3_qual_1').css('display') == 'none' && jq('#level_3_qual_2').css('display') == 'none'){
    jq('#addQualBtn_0').show();
    jq('#addQualBtn_1').hide();
  }
  appendQualDiv(1, '', qualloop, 'new_entry_page', 'add_Qual');  
  //
}
//
function addQualification(qualDivId, nxtQualDivCnt, addQualBtnId){
  jq('#'+addQualBtnId).hide();
  jq('#'+qualDivId).show();
  jq('#addQualBtn_'+ nxtQualDivCnt).show();
  var qualloop = nxtQualDivCnt+1;
  if(jq('#level_3_qual_'+ qualloop).css('display') == 'block'){
    jq('#addQualBtn_'+ nxtQualDivCnt).hide();
  }  
  jq("#qualsel_"+nxtQualDivCnt).val("1");
  appendQualDiv(1, '', nxtQualDivCnt, 'new_entry_page', 'add_Qual');
  if(jq('#level_3_qual_'+nxtQualDivCnt).css('display')!='none' && jq('[data-id=level_3_add_qualif]').eq(1).attr('id') == ('level_3_qual_'+nxtQualDivCnt)){
    var curAddQualCont =  jq("[data-id=level_3_add_qualif]").eq(1);    
    jq("[data-id=level_3_add_qualif]").last().after(curAddQualCont);      
  }
}
//
function removeQualEntryReq(rmQualId, qualNo){
  if(qualNo == '17'){
    var gcseSelObj = jq('#qualsel_'+qualNo).get(0);
    appendQualDiv(gcseSelObj, '', qualNo, '', '');
    jq('#'+rmQualId).hide();
  }else{
    jq('#qualspan_'+qualNo).text('A Level');
    jq('#qualsel_'+qualNo).val('1');
    appendQualDiv(1, '', qualNo, '', 'add_Qual');  
    var preQualNo = parseInt(qualNo)-1;
    jq('#'+rmQualId).hide();
    jq('#addQualBtn_'+ preQualNo).show();
    if(jq('#add_qualif_1').css('display') == 'block'){
      jq('#addQualBtn_2').hide();
    }
    if(jq('#add_qualif_2').css('display') == 'block' && jq('#add_qualif_1').css('display') == 'none'){
      jq('#addQualBtn_2').show();
      jq('#addQualBtn_0').hide();
    }
    if(jq('#add_qualif_2').css('display') == 'none' && jq('#add_qualif_1').css('display') == 'none'){
      jq('#addQualBtn_0').show();
    }
  }
}
//
function addQualEntryReq(qualNo){
  //var entryReyQualCnt = jq('.add_qualif').length;
  var entryReyQualCnt = jq('[data-id=level_3_add_qualif]').length;
  if(entryReyQualCnt < 3 && jq('#add_qualif_'+ qualNo).length == 0){
    var addQualResult = '';
    var addQualDiv = '';
    var addQUalDivEnd = ''; 
    var add_qualif_id = "'add_qualif_"+qualNo+"'";
    var l3q_rw_id = "'qual_level_"+ qualNo +"'";
    var add_qual_no = "'"+qualNo+"'";
    var add_qual_sel_id = "qualsel_"+qualNo;
    var emp_str = "''";    
    var nxt_add_qualif_id = "'add_qualif_"+(parseInt(qualNo)+1)+"'";
    var qualSelOption = jq('#qualsel_0').html();
    var qualSubGrdHidId = jq('#qual_sub_grd_hidId').html();
    qualSubGrdHidId = qualSubGrdHidId.replace(/_0_/g, '_'+qualNo+'_');
    //console.log(qualSubGrdHidId);
     addQualDiv = '<div class="add_qualif" data-id="level_3_add_qualif" id="add_qualif_'+qualNo+'"><div class="ucas_refld"><div class="l3q_rw" id="'+l3q_rw_id+'">'
     + '<div class="adqual_rw"><div class="ucas_row"><h5 class="cmn-tit txt-upr">Qualifications</h5><fieldset class="ucas_fldrw">'
     + '<div class="remq_cnt"><div class="ucas_selcnt"><fieldset class="ucas_sbox"><span class="sbox_txt" id="qualspan_'+qualNo+'">A Level </span><span class="grdfilt_arw"></span></fieldset>'
     + '<select name="qualification'+qualNo+'" onchange="appendQualDiv(this, '+emp_str+', '+add_qual_no+');" id="'+add_qual_sel_id+'" class="ucas_slist">'
     //+ '<option value="0">Please Select</option><option value="1" selected="selected">A Level </option><option value="2">AS Level </option><option value="8">BTEC Extended Diploma </option><option value="9">BTEC Diploma </option><option value="16">BTEC Foundation Diploma </option></select>'
     + qualSelOption 
     + '</select>'
     //+ '<input type="hidden" id="qualSubj_'+qualNo+'_1" name="qualSubj_1" value="3"><input type="hidden" id="qualGrde_'+qualNo+'_1" name="" value="A*,A,B,C,D,E"><input type="hidden" id="qualSubj_'+qualNo+'_2" name="qualSubj_2" value="1"><input type="hidden" id="qualGrde_'+qualNo+'_2" name="" value="A,B,C,D,E">'
     //+ '<input type="hidden" id="qualSubj_'+qualNo+'_8" name="qualSubj_8" value="1"><input type="hidden" id="qualGrde_'+qualNo+'_8" name="" value="D*D*D*,D*D*D,D*DD,DDD,DDM,DMM,MMM,MMP,MPP,PPP"><input type="hidden" id="qualSubj_'+qualNo+'_9" name="qualSubj_9" value="1">'								
     //+ '<input type="hidden" id="qualGrde_'+qualNo+'_9" name="" value="D*D*,D*D,DD,DM,MM,MP,PP"><input type="hidden" id="qualSubj_'+qualNo+'_16" name="qualSubj_16" value="1"><input type="hidden" id="qualGrde_'+qualNo+'_16" name="" value="D*,D,M,P">'								
     + qualSubGrdHidId
     + '</div></div><div class="add_fld" id ="removeQualbtn_'+qualNo+'" style="display:block;"><a id="remQual_'+qualNo+'" onclick="removeQualEntryReq('+add_qualif_id+', '+add_qual_no+');" class="btn_act1 bck f-14 pt-5">'
     + '<span class="hd-mob">Remove Qualification</span></a></div></fieldset><div class="err" id="qualSubId_'+qualNo+'_error" style="display:none;"></div></div>'								
     + '<div class="subgrd_fld" id="subGrdDiv'+qualNo+'"></div>'
     + '<input type="hidden" class="total_countAddSubj_'+qualNo+'" id="total_countAddSubj_'+qualNo+'" value="3"><input type="hidden" class="countAddSubj_'+qualNo+'" id="countAddSubj_'+qualNo+'" value="3">'
     addQUalDivEnd = '</div></div>';     
     if((parseInt(qualNo)+1) <= 3){
       var addQualNo = qualNo;
       if((parseInt(qualNo)+1) == 3){addQualNo = 0;}            
       var addQualBtnDiv = '<div class="add_qualtxt" id="addQualBtn_'+qualNo+'" style="display: block;"><a href="javascript:void(0);" onclick="addQualEntryReq('+(parseInt(addQualNo)+1)+')" class="fl btn_act2 bn-blu mt-40"><span class="fl plus">+</span> ADD A QUALIFICATION</a></div></div></div>';
       addQUalDivEnd = addQUalDivEnd + addQualBtnDiv;
     }
     addQualResult = addQualDiv + addQUalDivEnd;
     
     //jq('.ucas_mid').find('.add_qualif').last().after(addQualResult);
     jq('.ucas_mid').find('[data-id=level_3_add_qualif]').last().after(addQualResult);
     var preQualCnt = parseInt(qualNo) - 1;
     jq('#addQualBtn_'+preQualCnt).hide();
     if((parseInt(qualNo)+1) > 2){
       jq('#addQualBtn_'+qualNo).hide();
     }
     appendQualDiv(1, '', qualNo, '', 'add_Qual');
     jq('#qualspan_'+qualNo).text('A Level');
     jq('#qualsel_'+qualNo).val('1');     
  }else{   
    if(parseInt(qualNo) < 3){
      jq('#add_qualif_'+ qualNo).show();
      jq('#addQualBtn_'+ qualNo).show();
      jq('#addQualBtn_'+ (parseInt(qualNo)-1)).hide();
    }else if(parseInt(qualNo) == 3){
      var addShowQualNo = 1;
      jq('#add_qualif_'+ addShowQualNo).show();
      jq('#addQualBtn_'+ (parseInt(qualNo)-1)).hide();
      qualNo = addShowQualNo;
    }
    if(entryReyQualCnt == 3){
      if(jq('#add_qualif_1').css('display') == 'block'){
        jq('#addQualBtn_2').hide();
      }
      if(jq('#add_qualif_2').css('display') == 'block'){
        jq('#addQualBtn_1').hide();
      }
      if(jq('#add_qualif_'+qualNo).css('display')!='none' && jq('[data-id=level_3_add_qualif]').eq(1).attr('id') == ('add_qualif_'+qualNo)){
        var curAddQualCont =  jq("[data-id=level_3_add_qualif]").eq(1);
        //jq(".add_qualif").last().after(curAddQualCont);
        jq("[data-id=level_3_add_qualif]").last().after(curAddQualCont);      
      }
    }
  }  
}
//
function onloadShowingAddQualBtn(){
  var l3QualCnt = jq('[data-id=level_3_add_qualif]').length;
  var addQualbtnCnt = jq('[data-id=level_3_add_qualif]').find('.add_qualtxt').length;
  if(l3QualCnt!= 3){
    jq("[data-id=level_3_add_qualif]").last().find('.add_qualtxt').show();     
  }else if(l3QualCnt == 3){
    jq('.add_qualtxt').hide();
  }
}
function saveAndValidate() {
 var subject = jq('#subject').val();
 var q = jq('#q').val();
 var ucasPoints = jq('#ucasPt').text().trim();
 var userUcasScore = jq('#userUcasScore').val();
 var referralUrl = jq('#referralUrl').val();
 var minUcas = jq('#minSliderVal').text().trim();
 var maxUcas = jq('#maxSliderVal').text().trim();
 var srScore = minUcas +","+maxUcas;
 var loc1 = jq('#country_hidden').val();
 var location_url = "";
 var eventLblVal = ucasPoints + "|" + minUcas + "|" + maxUcas;
 gaEventLog('Qualification Filter', 'UCAS Range', eventLblVal);
  if('Y' == successFlag) {
  var action = 'SAVE_VALIDATE';
  var stringJson = {
    "action": action,
    "qualSubList": createJSON()
  };
  var jsonData = JSON.stringify(stringJson);
  var param = "?action="+action;
  var url = contextPath + "/wugo/qualifications/search.html" + param;
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    dataType: 'json',
    type: "POST",
    data: jsonData,
    async: false,
    success: function(response){
  },
  complete: function(response){
  if(response.responseText.indexOf("##SPLIT##")>-1){
    var resultArr = response.responseText.split("##SPLIT##");
    addTopCls();
    if(resultArr[0] == "VAL_ERROR"){
    if(resultArr[1] == 1) { 
    if(isNotNullAnddUndef(referralUrl)) {
       location_url = referralUrl+"&score="+srScore;
       location.href = isNotNullAnddUndef(loc1) ? location_url +"&location="+loc1 : location_url;
    }else{
       if(isNotNullAnddUndef(subject)){
         location_url  = "/degree-courses/search?clearing&subject="+subject+"&score="+srScore;
         location.href = isNotNullAnddUndef(loc1) ? location_url +"&location="+loc1 : location_url;
       }else if(isNotNullAnddUndef(q)){
         location_url = "/degree-courses/search?clearing&q="+q+"&score="+srScore;
         location.href = isNotNullAnddUndef(loc1) ? location_url +"&location="+loc1 : location_url;
      }
     }
   }
   else {
     var resId = "revLightBox";
     if(resultArr[1] == 2) { gaEventLog('Qualification Filter', 'Invalid Qualification', resultArr[2]); }
     else if(resultArr[1] == 0) {
       var eventlblval = resultArr[3] + "|" + resultArr[4].replace(',','');
       if(resultArr[5] != "") {eventlblval += "|" + resultArr[5].replace('and',''); }
       gaEventLog('Qualification Filter', 'Invalid Combination', eventlblval); }
     var loc = "/jsp/whatunigo/gradeFilterLiBox.jsp?valerror=" + resultArr[1] + "&qualification=" + resultArr[2] + "&qualification1=" + resultArr[3] + "&qualification2=" + resultArr[4] + "&qualification3=" + resultArr[5];
     var url = contextPath + loc;
     jq.ajax({
     url: url, 
     type: "POST", 
     success: function(result){
       jq("#"+resId).html(result);
       lightBoxCall();
       revLightbox();
     }
   });
  }
 }
 }
 }
});
}
}
function getMatchedCourse() {
  var qualSelId = ''; var qual = '';var sciErrFlag = false;
  var subId = ''; jq('[data-id=level_3]').each(function(){  
  var curSubId = this.id;
  if(this.value != '') {//This condition added for science practical error qualification-Jeya 08Aug2019
    subId += this.value + '';
    var qualLoop = curSubId.substring(curSubId.indexOf('_')+1, curSubId.lastIndexOf("_"));
    qualSelId += jq('#qualsel_'+qualLoop).val() +',';
  }
  });
  if(qualSelId.indexOf('18') > -1) {
    sciErrFlag = true;
    qualSelId = qualSelId.substring(0, qualSelId.length - 1);
    var qualIdArr = qualSelId.split(',');
    if(qualIdArr == '18') {
     sciErrFlag = true;
    } else {
    for (i = 0; i < qualIdArr.length; i += 1) {
      if(qualIdArr[i] == '1' || qualIdArr[i] == '2' || qualIdArr[i] == '3' || qualIdArr[i] == '4') {
        sciErrFlag = false;
      }
//      else {
//      //science practical error validation(qual id = 18)Jeya 08Aug2019
//       sciErrFlag = true;
//       break;
//      }
    }
  }}
  var kwSubId = '';
  var kwErrFlag = false;
  jq('[data-id=qual_sub_id]').each(function(){  
    var qualSelId = "";
    var curSubId = this.id;
    kwSubId += this.value + '';  
    var curHidId = curSubId.replace('qualSub_','sub_');    
    var curSubVal = jq('#'+curSubId).val();
    var curHidVal = jq('#'+curHidId).val();
    if(curSubVal!='' && curHidVal ==''){
     kwErrFlag = true;         
    }
  });
  if(kwErrFlag){var loc = "/jsp/whatunigo/include/entryReqLiBox.jsp?qualErrorCode=KEYWORD_ERROR"; successFlag = 'N';var url = contextPath + loc;
    var resId = "revLightBox";
    jq.ajax({
     url: url, 
     type: "POST", 
     success: function(result){
       jq("#"+resId).html(result);
       lightBoxCall();
       revLightbox();
     }
   }); } 
    else if(!isNotNullAnddUndef(subId)) {var loc = "/jsp/whatunigo/include/entryReqLiBox.jsp?qualErrorCode=QUAL_VALIDATION"; successFlag = 'N';var url = contextPath + loc;
      var resId = "revLightBox";
     jq.ajax({
     url: url, 
     type: "POST", 
     success: function(result){
       jq("#"+resId).html(result);
       lightBoxCall();
       revLightbox();
     }
   }); }
   else if(sciErrFlag) { var loc = "/jsp/whatunigo/include/entryReqLiBox.jsp?qualErrorCode=SCIENCE_PRACT_ERROR"; successFlag = 'N';var url = contextPath + loc;
      var resId = "revLightBox";
     jq.ajax({
     url: url, 
     type: "POST", 
     success: function(result){
       jq("#"+resId).html(result);
       lightBoxCall();
       revLightbox();
        }
   }); }
   
 else if(successFlag = 'Y'){
  var action = 'QUAL_MATCHED_COURSE';
  var ucasPoints = jq('#ucasPt').text().trim();
  var minUcas = jq('#minSliderVal').text().trim();
  var maxUcas = jq('#maxSliderVal').text().trim();
  var ucasPt = minUcas + ',' + maxUcas;
  var subject = jq('#subject').val();
  var q = jq('#q').val();
  var university = jq('#university').val();
  var qualCode = jq('#qualCode').val();
  var loc1 = isNotNullAnddUndef(jq('#country_hidden').val()) ? jq('#country_hidden').val() : '';
  var userUcasScore = jq('#userUcasScore').val();
  
 // var eventLblVal = ucasPoints + "|" + minUcas + "|" + maxUcas;
  qualGALog();
 // gaEventLog('Qualification Filter', 'UCAS Range', eventLblVal);
  var param = "?action="+action + "&ucasPoints=" + ucasPt +"&subject=" +subject +"&location="+loc1+"&q=" +q +"&university="+university+"&qualCode="+qualCode+"&userUcasScore="+userUcasScore;
  var url = contextPath + "/wugo/qualifications/search.html" + param;
  jq('#loadingImg').show();
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    type: "POST",
    async: false,
    success: function(response){
  },
  complete: function(response){
  if(response.responseText.indexOf("##SPLIT##")>-1){
    var result = response.responseText.split("##SPLIT##")[1];
    var resId = "revLightBox";
    var loc = "/jsp/whatunigo/gradeFilterLiBox.jsp?coursecount=" + result;
    var url = contextPath + loc;
    jq.ajax({
    url: url, 
    type: "POST", 
    success: function(result){
      jq('#loadingImg').hide();
      jq("#"+resId).html(result);
      lightBoxCall();
      revLightbox();
    }
  });
  }
  }
 });
 }
}
function saveAndContinue() {
  var action = 'SAVE_REDIRECT';
   var referralUrl = jq('#referralUrl').val();
   var stringJson = {
    "action": action,
    "qualSubList": createJSON()
  };
  var ucasPoints = jq('#ucasPt').text().trim();
  var minUcas = jq('#minSliderVal').text().trim();
  var maxUcas = jq('#maxSliderVal').text().trim();
 var srScore = minUcas +","+maxUcas;
  var subject = jq('#subject').val();
  var q = jq('#q').val();
  var jsonData = JSON.stringify(stringJson);
  var param = "?action="+action;
  var url = contextPath + "/wugo/qualifications/search.html" + param;
  jq.ajax({
    url: url, 
    headers: {'Content-Type': 'application/json', 'Accept': 'text/html'},
    dataType: 'json',
    type: "POST",
    data: jsonData,
    async: false,
  complete: function(response){
  if(isNotNullAnddUndef(referralUrl)) {
   location.href = referralUrl+"&score="+srScore;
 }else{
    if(isNotNullAnddUndef(subject)){
      location.href = "/degree-courses/search?clearing&subject="+subject+"&score="+srScore;
    }
    else if(isNotNullAnddUndef(q)){
       location.href = "/degree-courses/search?clearing&q="+q+"&score="+srScore;
    }
  }
 }
 });
 
}
jq(document).mouseup(function (e) {
 ajaxOptHide(e);
});
//
function ajaxOptHide(e){
  var container = jq('[data-id=wugoSubAjx]');
  jq.each(container, function(key, value) {
      if (!jq(value).is(e.target) // if the target of the click isn't the container...
          && jq(value).has(e.target).length === 0) // ... nor a descendant of the container
      {
          if(jq(value).is(':visible')){
            var fldId = jq(value).attr('id').replace('ajaxdiv', 'qualSub');
            var obj = jq('#'+fldId).get(0);
            onblurkwCheck(obj);
            jq(value).hide();
          }
      }
  });
}
jq(document).ready(function() {
  var oldonkeydownWugo = document.body.onkeydown;
  if(typeof oldonkeydownWugo != 'function'){
    document.body.onkeydown=wugoAjaxOptionNavi;
  }else{
    document.body.onkeydown=function(){
      oldonkeydownWugo();
      wugoAjaxOptionNavi();
    }	
  }
});
function wugoAjaxOptionNavi(e){
  var ajaxOptionObj = jq('#ajax_listOfOptions').get(0);
  var ajaxListActiveItem = jq('#ajax_listOfOptions').find('.optionDivSelected').get(0);
  if(document.all)e = event;
    if(!ajaxOptionObj){
      return;
    }
    if(ajaxOptionObj.style.display == 'none'){
      return;
    }
    if(e.keyCode == 38){
       if(!ajaxListActiveItem){
         return;
       }
       if(ajaxListActiveItem && !ajaxListActiveItem.previousElementSibling){
         return;
       }
       if(ajaxListActiveItem.previousElementSibling){
         ajaxListActiveItem.className = 'optionDiv';
         ajaxListActiveItem.previousElementSibling.className = 'optionDivSelected';
       }
     }
     if(e.keyCode == 40){
       if(!ajaxListActiveItem){
         var firstOptDiv = jq('#ajax_listOfOptions').find('.optionDiv').first().get(0);
         firstOptDiv.className = 'optionDivSelected';
       }else{
          if(!ajaxListActiveItem.nextSibling){
	    return;
	  }      
	  if(ajaxListActiveItem.nextElementSibling){
            ajaxListActiveItem.className = 'optionDiv';
            ajaxListActiveItem.nextElementSibling.className = 'optionDivSelected';
          }
       }
     }
     if(e.keyCode == 13 || e.keyCode == 9){
       if(ajaxListActiveItem && ajaxListActiveItem.className == 'optionDivSelected'){
       var selOptObj = jq('#ajax_listOfOptions').find('.optionDivSelected').get(0);
       setAjaxSubjectVal(selOptObj);
    }
    return true;
  }
  if(e.keyCode == 27){
    ajaxOptHide(e);
  }
}

function gaEventLog(eventCategory, eventAction, eventLabel){
  //ga('send', 'event', eventCategory , eventAction , eventLabel);
  if(isNotNullAnddUndef(eventLabel)){
    GANewAnalyticsEventsLogging(eventCategory, eventAction, eventLabel.toLowerCase());
  }
}
function qualGALog(){
  var qualDes = [];
  var subDes = [];   
  var qual3DpLen = jq('[data-id=level_3_add_qualif]:visible');
  var qual2DpLen = jq('[data-id=level_2_add_qualif]:visible');
  for (var ql = 0; ql < qual3DpLen.length;ql++){     
   var lvl3SubIpLen = jq('[data-id=level_3_add_qualif]').eq(ql).find('[data-id=qual_sub_id]')
   qualDes[ql] = jq('#qualspan_'+ql).text();
   subDes[ql] = '';
   for(var sb = 0; sb < lvl3SubIpLen.length ; sb++){
     var subIpId = lvl3SubIpLen.eq(sb).attr('id');
     var subValIpId = subIpId.replace('qualSub','');
     var subNameVal = jq('#'+subIpId).val();
     var subIdVal  = jq('#sub'+subValIpId).val();
     var grdIdVal = jq('#grde'+subValIpId).val();
     if(subNameVal!= '' && subIdVal != '' && grdIdVal != ''){
       if(sb != lvl3SubIpLen.length - 1){
         subDes[ql] = subDes[ql] + subNameVal + '|' + grdIdVal + '|' ;
       }else{
         subDes[ql] = subDes[ql] + subNameVal + '|' + grdIdVal;
       }
     }
   }
   gaEventLog('Qualification Filter', qualDes[ql], subDes[ql]);
  }
  if(qual2DpLen.length > 0){
    var lvl2SubIpLen = jq('[data-id=level_2_add_qualif]').find('[data-id=qual_sub_id]');
    qualDes[0] = 'GCSE';
    subDes[0] = '';
    for(var sb = 0; sb < lvl2SubIpLen.length ; sb++){
     var subIpId = lvl2SubIpLen.eq(sb).attr('id');
     var subValIpId = subIpId.replace('qualSub','');
     var subNameVal = jq('#'+subIpId).val();
     var subIdVal  = jq('#sub'+subValIpId).val();
     var grdIdVal = jq('#grde'+subValIpId).val();
     if(subNameVal != '' && subIdVal != '' && grdIdVal != ''){
       if(sb != lvl2SubIpLen.length - 1){
        subDes[0] = subDes[0] + subNameVal + '|' + grdIdVal + '|';
       }else{
        subDes[0] = subDes[0] + subNameVal + '|' + grdIdVal;
       }
     }
    }
    gaEventLog('Qualification Filter', qualDes[0], subDes[0]);
  }
}  
function skipAndViewResults() {
var subject = jq('#subject').val();
var loc = jq('#country_hidden').val();
var q = jq('#q').val();
 var university = jq('#university').val();
 var location_url = "";
 jq('#loadingImg').show();
 if(isNotNullAnddUndef(subject)){
   location_url = "/degree-courses/search?clearing&subject="+subject;
   location.href = isNotNullAnddUndef(loc) ? location_url +"&location="+loc : location_url;
 }
 else if(isNotNullAnddUndef(q)){
   location_url = "/degree-courses/search?clearing&q="+q;
   location.href = isNotNullAnddUndef(loc) ? location_url +"&location="+loc : location_url;
 }
  else if(isNotNullAnddUndef(university)){
    location.href = "/degrees/degree-courses/csearch.html?clearing&university="+university;
  }
}
var inputIDS = [];
function onblurkwCheck(obj){     
  //setTimeout(function(){     
	var kwErrFlag = false;
	jq('[data-id=qual_sub_id]').each(function(){             
	var curSubId = this.id
	var curHidId = curSubId.replace('qualSub_','sub_');
  var gradeDpId =  curSubId.replace('qualSub_','qualGrd_');
	var curSubVal = jq('#'+curSubId).val();
	var curHidVal = jq('#'+curHidId).val();
	if(curSubVal!='' && curHidVal ==''){
	  kwErrFlag = true;
	  inputIDS[curSubId] = 'Y';      
	}else{
	  inputIDS[curSubId] = 'N';
	}
	if(kwErrFlag&&inputIDS[curSubId]=='Y'&& obj.id == curSubId){      
	  jq(obj).addClass('faild');
	  if(kwErrFlag){ var loc = "/jsp/whatunigo/include/entryReqLiBox.jsp?qualErrorCode=AJAX_SEL_ERROR"; successFlag = 'N';var url = contextPath + loc;
    var resId = "revLightBox";
    jq.ajax({
     url: url, 
     type: "POST", 
     success: function(result){
       jq("#"+resId).html(result);
       lightBoxCall();
       revLightbox();
       jq('#'+gradeDpId).hide();
     }
   }); } 
	}
	});
  //},100);  
}