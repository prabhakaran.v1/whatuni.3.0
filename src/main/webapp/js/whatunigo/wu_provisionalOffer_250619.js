var contextPath = "/degrees";
function isNotNullAnddUndef(variable) {return (variable !== null && variable !== undefined && variable !== '');}
function showPass(id, passId) {
	var pwd = $$D(passId);
	if(pwd.getAttribute("type")=="password" && !isEmpty(pwd.value)){
		pwd.setAttribute("type","text");
		$$D(id).className = "fa fa-eye-slash";
	}
}
function hidePass(id, passId){
  var pwd = $$D(passId);
  pwd.setAttribute("type","password");
  $$D(id).className = "fa fa-eye";
}
function registratonValidations(id){
  if(id == "firstName" || id == "surName"){
    var errMsgClass;
    var susMsgClass;
    if(id == "firstName"){
      errMsgClass = "frm-ele faild";
      susMsgClass="frm-ele sucs";
      classNameId = id;
      msgId = "firstName_error";
    }else if(id == "surName"){
      errMsgClass = "frm-ele faild";
      susMsgClass="frm-ele sucs";
      classNameId = id;
      msgId = "surName_error";
    }
    if(trimString($$(id).value) == "" || trimString($$(id).value).toLowerCase() == "first name" || trimString($$(id).value).toLowerCase() == "last name"){      
      setErrorAndSuccessMsg(classNameId, errMsgClass, msgId, "We still don't know your name. Remind us?");
      $$(msgId).className = "fail-msg";
    }else{
      if(id == "firstName"){
        setErrorAndSuccessMsg(classNameId, susMsgClass, msgId, 'Nice to meet you! Great name!');
        $$(msgId).className = "su-msg";
      }
      if(id == "surName"){
        setErrorAndSuccessMsg(classNameId, susMsgClass, msgId, '');
        $$(msgId).className = "su-msg";
      }
    }
  }      
  if(id == 'password') {
    var password = ""; 
    if($$D("password")){password = trimString( $$D("password").value );}    
    if(isEmpty(password) || password == 'Password*'){ message = false; setErrorAndSuccessMsg(id,'frm-ele faild','password_error', ErrorMessage.error_reg_login_password);
    } else {setErrorAndSuccessMsg(id,'frm-ele sucs','password_error','');}
    if(!isEmpty(password)){
      setErrorAndSuccessMsg(id,'frm-ele sucs','password_error','');
    }   
    if(!isEmpty(password) && password.length < 6){
       message = false; setErrorAndSuccessMsg(id,'frm-ele faild','password_error', ErrorMessage.error_password_length);
    }
  }         
}
function valCmmtUserRegister(){  
  registerErrorDisplayFlag();
  var message = true;
  var fbUserId;
  var newsLetter = null;
  var firstName = encodeURIComponent(trimString($$D("firstName").value));   
  var surName = encodeURIComponent(trimString($$D("surName").value));    
  var emailAddress =  trimString($$D("email").value);  
  var termsc =  trimString($$D("termsc").value);
  var password = encodeURIComponent(trimString($$D("password").value)); 
  var datemsg = "";          
  var postcode = "";  
  var regType = $$D("regLoggingType").value;
  var submittedFrom = "";
  var vwcid = "";
  var prepopulatepassword = ""; 
  var marketingEmailRegFlag = ($$D("marketingEmailRegFlag") && $$D("marketingEmailRegFlag").value != "")? $$D("marketingEmailRegFlag").value : "N";
  var solusEmailRegFlag = ($$D("solusEmailRegFlag") && $$D("solusEmailRegFlag").value != "") ? $$D("solusEmailRegFlag").value : "N";
  var surveyEmailRegFlag = ($$D("surveyEmailRegFlag") && $$D("surveyEmailRegFlag").value != "") ? $$D("surveyEmailRegFlag").value : "N";
  if($$D("fbuserregister")){submittedFrom = trimString( $$D("fbuserregister").value );}
  //Added FB user id to store and generate FB image path for 19_May_2015, by Thiyagu G.
  if(submittedFrom != ""){fbUserId =  trimString($$D("fbUserId").value);}
  if($$D("prepopualtepassword")){prepopulatepassword = trimString( $$D("prepopualtepassword").value );}  
  var pSubmitType = trimString( $$D("submitType").value );  
  var yoe = trimString($$D("yoeId").value);;
  for (i=0; i<document.getElementsByTagName('input').length; i++) {
    if (document.getElementsByTagName('input')[i].type == 'radio'){
      if(document.getElementsByTagName('input')[i].checked == true){
       yoe = document.getElementsByTagName('input')[i].value;
       break;
      }
    } 
  }
  var pdfId = "";
  var rememberMe; 
  if(isEmpty(firstName) || firstName == 'First name*'){ message = false;setErrorAndSuccessMsg('firstName','frm-ele faild','firstName_error',ErrorMessage.error_reg_firstName);}
  else{setErrorAndSuccessMsg('firstName','frm-ele sucs','firstName_error', 'Nice to meet you! Great name!');$$D('firstName_error').className="sutxt";}
  if(isEmpty(surName) || surName == 'Last name*'){ message = false; setErrorAndSuccessMsg('surName','frm-ele faild','surName_error', ErrorMessage.error_reg_surName); }
  if(emailAddress==''){
    message = false;
    setErrorAndSuccessMsg('email','frm-ele faild','registerErrMsg', 'Please enter a valid email address');
  }else if(!checkValidEmail(emailAddress)){
    message = false;
    setErrorAndSuccessMsg('email','frm-ele faild','registerErrMsg', ErrorMessage.error_reg_user_email);
  }
  //checkEmailExist("emailAddress");
  if($$D("emailValidateFlag").value=="false"){      
    message = false;    
  }
  
  if(prepopulatepassword==''){if(isEmpty(password) || password == 'Password*'){ message = false; setErrorAndSuccessMsg('password','frm-ele faild','password_error', ErrorMessage.error_reg_login_password);  
  } else if(!isEmpty(password) && password.length < 6){
    message = false; setErrorAndSuccessMsg('password','frm-ele faild','password_error', ErrorMessage.error_password_length);
  } else{setErrorAndSuccessMsg('password','frm-ele sucs','password_error','');}}     
     
  if($$D("termsc") == null || $$D("termsc").checked == false){ message = false; showErrorMsg('termsc_error',ErrorMessage.error_termsc); }     
  if(!message){     
    message = true;
    scrollToRegDiv('wugo_reg');
  }else {    
    var url = "";
    if(submittedFrom==''){
      url= context_path+"/newuserregistration.html?submittype=applynowreg&firstName="+firstName+"&surName="+surName;
      url = url+"&newsLetter="+newsLetter+"&pSubmitType="+pSubmitType+"&pdfId="+pdfId+"&yoe="+yoe+"&vwcid="+vwcid;
      url = url+"&postcode="+postcode+"&userCMMType="+ regType+"&marketingEmailFlag="+marketingEmailRegFlag;
      url = url+"&solusEmailFlag="+solusEmailRegFlag+"&surveyEmailFlag="+surveyEmailRegFlag;
    }else{      
      var socialRegType = "";
      if($$D("socialRegType")){
        socialRegType = $$D("socialRegType").value;
      }
      url= context_path+"/newuserregistration.html?submittype=fbreg&firstName="+firstName+"&surName="+surName+"&newsLetter="+newsLetter;
      url = url+"&pSubmitType="+pSubmitType+"&pdfId="+pdfId+"&yoe="+yoe+"&lightboxSubmit=Y&socialType="+socialRegType+"&vwcid="+vwcid+"&fbUserId="+fbUserId;
      url = url+"&postcode="+postcode+"&userCMMType="+ regType+"&marketingEmailFlag="+marketingEmailRegFlag;
      url = url+"&solusEmailFlag="+solusEmailRegFlag+"&surveyEmailFlag="+surveyEmailRegFlag;      
    }
    $$("loadinggifreg").style.display="block";    
    url = url + "&screenwidth="+$$D('screenwidth').value;
    var ajax=new sack();
    ajax.requestFile = url;	
    
    ajax.setVar("emailAddress", encodeURIComponent(trimString(emailAddress)));
    ajax.setVar("password", encodeURIComponent(password));
    ajax.setVar("rememberMe", rememberMe);
    
    ajax.onCompletion = function(){ showResponseMsg(ajax, "registerErrMsg"); };	
    ajax.runAJAX();                
    return false;
  }
}  
//
function valCmmtUserLogin(){  
  loginErrorDisplayFlag();
  var message = true;
  var email =  trimString( $$("email").value );
  var password = trimString( $$("password").value );
  var pSubmitType = trimString( $$("submitType").value );
  var pdfId = "";//$$D("pdfId").value;  
  var vwcid =  "";//$$D("vwcid").value;
  var vwcourseId =  "";//$$D("vwcourseId").value;
  var vwurl =  "";//$$D("vwurl").value;
  var surveyUrlUserId =  "";
  if($$D("surveyUrlUserId")){
   surveyUrlUserId =  $$D("surveyUrlUserId").value;
  }  
  var rememberMe;
  if($$("loginRemember").checked == true){
    rememberMe = "Y";
  }else{
    rememberMe = "N";
  }  
  if(isEmpty(email) || email == 'Enter email address'){ 
    message = false;
    setErrorAndSuccessMsg('email','frm-ele faild','loginemail_error', 'Please enter a valid email address');     
  } else if(!checkValidEmail(email)){ 
    message = false;
    setErrorAndSuccessMsg('email','frm-ele faild','loginemail_error', 'Please enter a valid email address');
    //setErrorAndSuccessMsg('email','frm-ele faild','loginemail_error', ErrorMessage.error_valid_email);    
  } else{
    setErrorAndSuccessMsg('email','frm-ele','loginemail_error', '');
  }
  if(isEmpty(password) || password == 'Enter password'){ 
    message = false; 
    setErrorAndSuccessMsg('password','frm-ele faild','loginpass_error', ErrorMessage.error_login_password);    
  }else{
    setErrorAndSuccessMsg('password','frm-ele','loginpass_error', '');
  }  
  if(!message){      
    message = true;
  }else {
    var url= context_path + "/newuserregistration.html?submittype=applynowlogin&pSubmitType=" + pSubmitType + "&pdfId=" + pdfId + "&vwcid=" + vwcid + "&vwurl=" + vwurl+"&vwcourseId="+vwcourseId+"&surveyUrlUserId="+surveyUrlUserId;    
    //blockNone('loadinggif', 'block');
    blockNone('loadinggifreg', 'block');
      var ajax=new sack();
      ajax.requestFile = url;	
      ajax.setVar("emailAddress", trimString(email));
      ajax.setVar("password", encodeURIComponent(password));
      ajax.setVar("rememberMe", rememberMe);      
      ajax.onCompletion = function(){ showResponseMsg(ajax, "loginerrorMsg"); };	
      ajax.runAJAX();
     return false;
  }
}
function registerErrorDisplayFlag(){
  blockNone('firstName_error', 'none');
  blockNone('surName_error', 'none'); 
  blockNone('emailAddress_error', 'none');
  blockNone('registerErrMsg', 'none');
  blockNone('password_error', 'none');  
  blockNone('termsc_error', 'none');
  blockNone('registerErrMsg', 'none'); 
}
function offerLoadingProgressBar() {
  var elem = $$("myBar");   
  var width = 0;
  var id = setInterval(frame, 100);
  function frame() {
    if (width >= 100) {
      clearInterval(id);
      jq('#action').val('provisonal-offer-congrats');      
      var courseId = jq('#applyNowCourseId').val();
      var opportunityId = jq('#applyNowOpportunityId').val();
      var successURL = contextPath + "/wugo/provisional-offer/success.html?action=provisonal-offer-congrats&courseId="+courseId+"&opportunityId="+opportunityId;
      window.location.href = successURL;
      //navigation('', '1', 'I', '', '', '');
    } else {
      width++; 
      elem.style.width = width + '%'; 
      $$("prLabel").innerHTML = width * 1 + '%';
    }
  }
}

function completeProvForm(){
  if($$("provFormTCId").checked == true){
    stopinterval();
    jq('#action').val('provisional-offer-loading');
    var collegeName = jq('#collegeName').val();
    gaEventLog('Apply Now Form', 'Complete', collegeName);
    gaEventLog('WUGO Journey', 'Form 1 Complete', collegeName);
    wugoStatsLogging('provisional-offer-loading');
    navigation('', '', 'I','','','');
  }else{
    showErrorMsg('provFormTC_error',ErrorMessage.error_termsc);   
  }
}
//
var dev = jQuery.noConflict();
var timerStatus = [];
var setIrlId = [];
dev(document).ready(function(){
  stopinterval();
  var timerIdT1 = 'timer';//dev('#timerIdT1').val(); 
  var cookieName = 'offer_timer';
  var cookieVal = getCookie(cookieName);
  if( cookieVal == null && (timerStatus[cookieName] != 'Y' || dev('#statusT1').val() != 'Y')){
    //startTimer1();
  }else if(cookieVal != null){
    //timerMsgFunc(cookieName)//for page refresh scenario
  }
  //timerMsgFunc('timer_'+timerIdT1);
});
function startTimer1(timerType){
  var cookieName = 'offer_timer';
  var expMinVal = dev('#endTimeT1').val();
  if(expMinVal != '' && expMinVal != null && expMinVal != 'null'){
    var timer1Exp = new Date();      
    var t1Exp = timer1Exp.getTime()+ (parseInt(expMinVal));    
    var expiryTimeSec = expMinVal/1000;    
    timerStatus[cookieName] = 'Y';
    dev('#statusT1').val('Y');
    if(isNotNullAnddUndef(expMinVal)){
      setTimerCookie(cookieName, t1Exp,expiryTimeSec,'');
      timerMsgFunc(cookieName, timerType);
    }
  }
}  

function timerMsgFunc(cookieName, timerType){
  var cookieVal = getCookie(cookieName);
  setIrlId.push(setInterval(function(){    
    //var timer1Data = cookieVal.split("##");
    var timer1Data = cookieVal;
    var date = new Date();
    var endTimeT1 = dev('#endTimeT1').val();    
    var endTimerT1 = timer1Data;
    var currentTime = Date.parse(date);
   // if('timer-2' == timerType){ clearInterval(setIrlId); }
    //console.log(endTimerT1 + "  " + currentTime);
    //console.log("cookieVal--->"+cookieVal);
    
    if(cookieVal!= null){
      var t = endTimerT1 - currentTime;
      var day = Math.floor(t/(1000*60*60*24));
      var hours = Math.floor((t%(1000*60*60*24))/(1000*60*60));
      var min = Math.floor((t%(1000*60*60))/(1000*60));
      var sec = Math.floor((t%(1000*60))/1000);
      if(((endTimerT1 - currentTime)/1000) > 0){
       // var min = Math.floor((((endTimerT1 - currentTime)/1000) / 60 ));
       // var seconds = ((endTimerT1 - currentTime)/1000) % 60;
       //dev('#timer').html( min + " : "+ Math.round(seconds));
      // timer = pad2(day) + ' : ' + pad2(hours) + ' : ' + pad2(min) + " : "+ pad2(sec);
       var timer = hideDaysHoursInTimer(pad2(day), pad2(hours), pad2(min), pad2(sec));
       dev('#timer').html(timer);
        if(pad2(hours) == '00' && pad2(min) == '05' && pad2(sec) == '00'){
          jq('#loadingImg').show();
          callLightBox('FIVE_MIN');
        }
      }else{
        //here to include expire call
        //clearInterval(setIrlId);
        stopinterval();
        timerOneExpiryCall(cookieVal, timerType); 
        //dev('#timer').html("cookie not setted or expired");
        
      }
    }else{
      //dev('#timer').html("cookie not setted or expired");
      stopinterval();
      //clearInterval(setIrlId);
    }
  }, 1000));
}
function pad2(number) {
  return (number < 10 ? '0' : '') + number
}
function stopinterval(){
  //clearInterval(setIrlId);
  for (var i=0; i < setIrlId.length; i++) {
    clearInterval(setIrlId[i]);
  }
  //console.log("setIrlId--->"+setIrlId);
  return false;
}
function timerOneExpiryCall(timer1Val, timerType){
  jq('#loadingImg').show();
  var courseId = jq('#applyNowCourseId').val();
  var opportunityId = jq('#applyNowOpportunityId').val();
  var timType = "timer-1-expiry&timerOneStatus=EXPIRED";
  var action = "timer-1-expiry";
  var eventAct = "Timer 1";
  if('TIMER_2'== timerType){
    timType = "timer-2-expiry&timerTwoStatus=EXPIRED";
    action = "timer-2-expiry";
    eventAct = "Timer 2";
  }
  if("CANCEL_TIMER_1" == timerType){
    timType = "timer-1-expiry&timerOneStatus=CANCELLED";
    action = "CANCEL_OFFER";
    eventAct = "CANCEL_OFFER";
  }else if("CANCEL_TIMER_2" == timerType){
    timType = "timer-2-expiry&timerTwoStatus=CANCELLED";
    action = "CANCEL_OFFER";
    eventAct = "CANCEL_OFFER";
  }else if("BACK_TO_COURSE" == timerType){
    timType = "timer-2-expiry&timerTwoStatus=CANCELLED";
    action = "CANCEL_OFFER";
    eventAct = "CANCEL_OFFER";
  }
  if(eventAct == "Timer 1" || eventAct == "Timer 2"){
    ga('set', 'dimension1', 'timerexpiry');
    gaEventLog('Timers', eventAct, 'Expired');
  }
  wugoStatsLogging(action);
  var timer1ExpUrl = '/degrees/clearing-match-maker-tool.html?action='+timType+'&timer1Data='+timer1Val+'&courseId='+courseId+'&opportunityId='+opportunityId;
  jq.ajax( {
    url:timer1ExpUrl,
    type: "POST",
    complete:function(response) {
      if(response.responseText.indexOf("##SPLIT##")>-1){
        if("CANCEL_TIMER_1" == timerType || "CANCEL_TIMER_2" == timerType ){
          location.href = "/university-clearing/";
        }else if("BACK_TO_COURSE" == timerType){
          gotoBackSearch();
        }else {
          callLightBox(response.responseText);  
        }
      }else{
        if("CANCEL_TIMER_1" == timerType || "CANCEL_TIMER_2" == timerType ){
        location.href = "/university-clearing/";
        }else if("BACK_TO_COURSE" == timerType){
          gotoBackSearch();
        }else{
          var resTxt = "TIMER_EXPIRY##SPLIT##"+timerType;
          callLightBox(resTxt);
        }
      }
    }
  });
}
//Added to set the timer cookie by sangeeth.S for CMMT
function setTimerCookie(c_name,value,maxAge,cookieType){   	
	var c_value=escape(value) + ((maxAge==null) ? "" : ";max-age=" + maxAge);	
  document.cookie=c_name + "=" + c_value+"; path=/"; 
  //console.log("in setting cooki");
}
//
function gaEventLog(eventCategory, eventAction, eventLabel){
  //ga('send', 'event', eventCategory , eventAction , eventLabel);
  if(isNotNullAnddUndef(eventLabel)){
    GANewAnalyticsEventsLogging(eventCategory, eventAction, eventLabel.toLowerCase());
  }
}
//
function cancelTimer(timerType){
  stopinterval();
  if("BACK_TO_COURSE" == timerType){
    //do nothing
  }else{
    var collegeName = jq('#collegeName').val();
    gaEventLog('Application Page', 'Cancel Incomplete Offer', collegeName);
    gaEventLog('WUGO Journey', 'Cancel Incomplete Offer', collegeName);
  }
  timerOneExpiryCall('offer_timer',timerType);
}
//
function continueApp(){
  var collegeName = jq('#collegeName').val();
  gaEventLog('Application Page', 'Continue Application', collegeName);
}

/*function hideDaysHoursInTimer(days, hours, mins){
  var finalTimer = '';
  if(days != '00'){
    finalTimer = days + ':';
  }
  
  if(days != '00' && hours != '00'){
    finalTimer += hours + ':';
  }else if(hours != '00'){
    finalTimer += hours + ':';
  }
  
  if(days != '00' && hours != '00' && mins != '00'){
    finalTimer += mins + ':';
  }else if(hours != '00' && mins != '00'){
    finalTimer += mins + ':';
  }else if(mins != '00'){
    finalTimer += mins + ':';
  }
  return finalTimer;
}*/

function hideDaysHoursInTimer(days, hours, mins, sec){
  var finalTimer = '';
  finalTimer = days != '00' ? days + ':' : '';
  finalTimer += days != '00' && hours != '00' ? hours + ':' : 
                hours != '00' ?  hours + ':' : '';
  finalTimer +=  mins + ':' + sec;
  return finalTimer;            
}