 var dev1 = jQuery.noConflict();
function showhidesocialTabs(id){
  if(id=="twitter"){
    subFuncShowhideSocialTabs("block", "none", "none", "", "act", "");
  }else if(id=="facebook"){
    loadFacebookTab();
    subFuncShowhideSocialTabs("none", "block", "none", "", "", "act");
  }else if(id=="instagram"){
   loadInstagramTab();  
   subFuncShowhideSocialTabs("none", "none", "block", "act", "", "");
  }
}
function subFuncShowhideSocialTabs(twitLnkDisplay, fbLnkDisplay, instagramLnkDisplay, instagramTabName, twitterTabName, facebookTabName){
  blockNone("twitterLnk", twitLnkDisplay);
  blockNone("facebookLnk", fbLnkDisplay);
  blockNone("instagramLnk", instagramLnkDisplay);
  addClassName("instagramTab", instagramTabName);
  addClassName("twitterTab", twitterTabName);
  addClassName("facebookTab", facebookTabName);
}
function loadInstagramTab(){
 if($$D('instaName')){
   getInstagramId($$D('instaName').value);
 }
 if($$D('instaName') && $$D('instaName').value != ""){
   var jsPath = $$D('contextJsPath').value;
   var script = document.createElement("script");
   script.type = "text/javascript";  
   script.src = jsPath + "/js/scout/instafeed.min.js";
	  if(typeof script != "undefined"){
     document.getElementsByTagName("head")[0].appendChild(script);
   }
   if($$D('instagramCntDiv') && isEmpty($$D('instagramCntDiv').innerHTML)){
     setTimeout(function(){loadInstagram();}, 3000);
   }
 }
}
function loadInstagram(){
  var url = context_path + "/jsp/scout/include/instagramFeed.jsp";
		var ajax = new sack();
  ajax.requestFile = url; 
  ajax.onCompletion = function(){onCompleteInstagram(ajax, 'instagramCntDiv')}; 
  ajax.runAJAX();
}
function onCompleteInstagram(ajax,divId){
  var response = ajax.response;
  $$D(divId).innerHTML = "";
  $$D(divId).innerHTML = response;
  blockNone(divId, 'block');
	 if($$D("script_instagram")){
	   eval($$D("script_instagram").innerHTML);
	 }
}
function loadFacebookRes(ajax){
  var res = ajax.response;
  blockNone("facebookLnk", "block");
  $$D("facebookDiv").innerHTML = res;
  facebookLoadTab();
}
function loadFacebookTab(){    
  var url = "";  
  if($$D("provFbUrl") && !isEmpty($$D("provFbUrl").value)){
    url = context_path + "/fbAjax.html?provFbUrl="+$$D("provFbUrl").value;      
  }else{  
    url = context_path + "/fbAjax.html?provFbUrl=whatuni";      
  }    
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){loadFacebookRes(ajax)};
  ajax.runAJAX();         
}
function facebookLoadTab(){
  var jqfb = jQuery.noConflict();
  var providerUrl = 'whatuni';
  if($$D("provFbUrl")){
    providerUrl = $$D("provFbUrl").value;
  }
  var container_width = jqfb('#container').width();
  jqfb('#container').html('<div class="fb-like-box" ' +
  'data-href="https://www.facebook.com/'+ providerUrl +
  '" data-width="' + container_width + '" data-height="730" data-show-faces="false" ' +
  'data-stream="true" data-header="true"></div>');
  FB.XFBML.parse( );
}
function getInstagramId(username){
  dev1.ajax({
    url: "//api.instagram.com/v1/users/search?q="+username+"&access_token=378422198.1677ed0.6a0287f3b8eb40fba5d0c2bf893efd28",
    dataType: 'jsonp',
	   type: 'GET',
    username: username,
    success: function(data){
      for(i = 0; i < data.data.length; ++i){
        $$D('instaId').value = data.data[i].id;
        if(this.username == data.data[i].username){
          $$D('instaId').value = data.data[i].id;
          break;
        }
      }
    }
  });
}
function loadFBJS(){
  var jqfb = jQuery.noConflict();
  jqfb("#facebookTab").click(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=374120612681083";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
  showhidesocialTabs('facebook');
}