var context_path = "/degrees";
function $$D(docid){ return document.getElementById(docid); }
function blockNone(thisid, value){  if($$D(thisid) !=null){ $$D(thisid).style.display = value;  }  }
function isEmpty(value){  if(trimString(value) ==''){ return true;} else {  return false; } }
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
function addClassName(id, classname){if($$D(id)){$$D(id).className = classname;}}
function writeinnerHTML(id, txt){if($$D(id)){$$D(id).innerHTML = txt;blockNone(id, 'block');}}
function changeErrCls(id, cls){if($$D(id)){dev1('#'+id).addClass(cls);}}
function checkValidEmail(email){
  if(/^\w+([-+.'']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(trimString(email))){
    return true
  }
  return false
}
function checkPostCode(toCheck) {
  var alpha1 = "[abcdefghijklmnoprstuwyz]";                       // Character 1
  var alpha2 = "[abcdefghklmnopqrstuvwxy]";                       // Character 2
  var alpha3 = "[abcdefghjkpmnrstuvwxy]";                         // Character 3
  var alpha4 = "[abehmnprvwxy]";                                  // Character 4
  var alpha5 = "[abdefghjlnpqrstuwxyz]";                          // Character 5
  var BFPOa5 = "[abdefghjlnpqrst]";                               // BFPO alpha5
  var BFPOa6 = "[abdefghjlnpqrstuwzyz]";                          // BFPO alpha6
  var pcexp = new Array ();
  pcexp.push (new RegExp ("^(bf1)(\\s*)([0-6]{1}" + BFPOa5 + "{1}" + BFPOa6 + "{1})$","i"));
  pcexp.push (new RegExp ("^(" + alpha1 + "{1}" + alpha2 + "?[0-9]{1,2})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));
  pcexp.push (new RegExp ("^(" + alpha1 + "{1}[0-9]{1}" + alpha3 + "{1})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));
  pcexp.push (new RegExp ("^(" + alpha1 + "{1}" + alpha2 + "{1}" + "?[0-9]{1}" + alpha4 +"{1})(\\s*)([0-9]{1}" + alpha5 + "{2})$","i"));
  pcexp.push (/^(GIR)(\s*)(0AA)$/i);
  pcexp.push (/^(bfpo)(\s*)([0-9]{1,4})$/i);
  pcexp.push (/^(bfpo)(\s*)(c\/o\s*[0-9]{1,3})$/i);
  pcexp.push (/^([A-Z]{4})(\s*)(1ZZ)$/i);  
  pcexp.push (/^(ai-2640)$/i);
  var postCode = toCheck;
  var valid = false;
  for ( var i=0; i<pcexp.length; i++) {
    if (pcexp[i].test(postCode)) {
      pcexp[i].exec(postCode);  
      postCode = RegExp.$1.toUpperCase() + " " + RegExp.$3.toUpperCase();
      postCode = postCode.replace (/C\/O\s*/,"c/o "); 
      if (toCheck.toUpperCase() == 'AI-2640') {postCode = 'AI-2640'}; 
      valid = true;  
      break;
    }
  }
  if (valid) {return postCode;} else return false;
}
function isValidUKpostcodeWithoutSpace(postcode){
  var newPostCode = checkPostCode(postcode);  
  if(newPostCode){
	   return true;
	 }else{
    return false; 	
	 } 
}
function isNumeric(input){
  return (input - 0) == input && (''+input).replace(/^\s+|\s+$/g, "").length > 0;
}
function getCookie(c_name){
  var c_value = document.cookie;                
  var c_start = c_value.indexOf(" " + c_name + "=");
  if(c_start == -1){
    c_start = c_value.indexOf(c_name + "=");
  }
  if(c_start == -1){
    c_value = null;
  }else{
    c_start = c_value.indexOf("=", c_start) + 1;
    var c_end = c_value.indexOf(";", c_start);
    if(c_end == -1){
      c_end = c_value.length;
    }
    c_value = unescape(c_value.substring(c_start,c_end));
  }
  return c_value;
}
function showTermsConditionPopup(){  
  window.open(context_path + "/help/termsAndCondition.htm","termspopup","width=650,height=400,scrollbars=yes,resizable=yes"); return false;
}
function showPrivacyPolicyPopup(){
  window.open(context_path + "/help/privacy.htm","termspopup","width=650,height=400,scrollbars=yes,resizable=yes"); return false;
}
function setSessionCookie(c_name,value,exdays){
	 var c_value=escape(value) + "; expires=0";
  if(exdays != 0){
		  c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdays.toUTCString());
	 }
  document.cookie=c_name + "=" + c_value+"; path=/";
}