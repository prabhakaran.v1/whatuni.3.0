var adv = jQuery.noConflict();
function $$d(id){
  return document.getElementById(id);
}
adv(window).scroll(function(){ // scroll event 
  var windowTop = adv(window).scrollTop(); // returns number  
  var docHeight = adv(document).height();
  var diff = docHeight - 1200;
  var width = document.documentElement.clientWidth;
  if (width > 992) {    
    if (150 < windowTop){
      adv('#stickyTop').addClass('sticky_top_nav');        
      adv('#stickyTop').css({ position: 'fixed', top: 0 });            
    } else {
      adv('#stickyTop').removeClass('sticky_top_nav');
      adv('#stickyTop').css('position','static');         
    } 
  }  
});
adv(document).ready(function(){
  adv(window).scroll(function(){ // scroll event 
    if($$d("scrollStop")){
      if(adv("#scrollStop").val() != 1){
        adv("#scrollStop").val(1);
        loadArticlesPodJS('PROFILE_PAGE');
      }
    }
  });
  if($$d("scrollStop")){
    adv("#scrollStop").val(0);
  }
});
//Added by Indumathi.S for profile logging July-26-2016
function GAEventTrackingNormalIP(Webclick,Profile,sectionName,profSecId){
 if(adv("#"+profSecId).hasClass('fa-minus-circle')) {
   if($$D("profileType") && $$D("profileType").value != "SUB_PROFILE" && $$D("profileType").value !="CLEARING_PROFILE" && $$D("profileType").value != "CLEARING_LANDING") {
    if($$D("collegeNameHidden")) { collegeName = $$D("collegeNameHidden").value; }
    GAInteractionEventTracking(Webclick,Profile,sectionName,collegeName);
    pageViewLogging();
   }
  }
}

function pageViewLogging(){
  var pageName = "";
  if($$D("pageName")) {
    pageName = $$D("pageName").value;
  }
  if(pageName =="uniview.jsp") {
    ga('set', 'dimension1', $$D("pageName").value);
    if($$D("gaPageCollegeName")){
      ga('set', 'dimension3', $$D("gaPageCollegeName").value);      
    }  
    if($$D("nonAdvertFlag").value == "true") {
      ga('set', 'dimension4', 'NonAdvertiser');
    } else {
      ga('set', 'dimension4', 'Advertiser');
    }
    ga('send', 'pageview'); 
  }
}
//End
function viewProfileSecCnt(curSecId,profileId,journeyType,myhcProfileId) {    
  var totProfSecLen = "";
  var curProfSecId  = "";
  
  if($$d("ProfSecLen")){
    totProfSecLen = parseInt($$d("ProfSecLen").value);
    curProfSecId  = parseInt(curSecId);
  }    
  if(totProfSecLen!=""){    
    for(var len=1; len<=totProfSecLen; len++) {
      if(len==curProfSecId){                
        if($$d("profSecCnt_"+curProfSecId).style.display=="none"){
          adv("#profSecCnt_"+curProfSecId).show();
          if($$d('profSecPos_'+(curProfSecId-1))) { skipLinkSetPosition('profSecPos_'+(curProfSecId-1)); }
          var secClassName = adv("#profSecId_"+curProfSecId).attr('class');             
          adv("#profSecId_"+curProfSecId).removeClass(secClassName).addClass("fa fa-minus-circle fnt_24 color1 fr mt6");                
          lazyloadetStarts();
          blockNone("vidPlayId_"+curProfSecId, "block");
          profileSecStatsLogging(profileId,journeyType,myhcProfileId);
        }else{
          adv("#profSecCnt_"+curProfSecId).hide();
          var secClassName = adv("#profSecId_"+curProfSecId).attr('class');             
          adv("#profSecId_"+curProfSecId).removeClass(secClassName).addClass("fa fa-plus-circle fnt_24 color1 fr mt6");                          
        }        
      }else{                
        adv("#profSecCnt_"+len).hide();
        var secClassName = adv("#profSecId_"+len).attr('class');        
        adv("#profSecId_"+len).removeClass(secClassName).addClass("fa fa-plus-circle fnt_24 color1 fr mt6");                
      }
    }
  }          
}

function changeMapHeight() {
  var uniVenueHeight = adv("#uniVenueCnt").height();  
  if(uniVenueHeight>270){
    adv('#map_canvas').height(uniVenueHeight-10);
  }
}


function profileSecStatsLogging(profileId,journeyType,sectMyhcProfileId) {
  var profileSecId = profileId;
  var collegeId =  $$d("collegeId").value;  
  var myhcProfileId = $$d("myhcProfileId").value;
  if(sectMyhcProfileId!=null && sectMyhcProfileId!=undefined){
    myhcProfileId = sectMyhcProfileId;
  }
  //var myhcProfileId =   $$D("normalSecMyhcProfId_"+curSecId).value;//Added to handle the clearing stats log for CMMT
  var cpeQualificationNetworkId = $$d("cpeQualNetworkId").value;
  if($$d("ugContentFlag") && "y" == $$d("ugContentFlag").value.toLowerCase()) {
    cpeQualificationNetworkId = "2";
  }
  var queryString = "?tabName=" +profileSecId+"&collegeId="+collegeId+"&cpeQualificationNetworkId="+cpeQualificationNetworkId+"&myhcProfileId="+myhcProfileId+"&profileId="+profileSecId+"&journeyType="+journeyType;  
  profStatsLoggingAjax(queryString);
}


function profStatsLoggingAjax(queryString) 
{
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  var ajaxURL = "/degrees" + "/unilandingStatsAjax.html" + queryString +'&screenwidth='+deviceWidth;
  var ajax=new sack();
  ajax.requestFile = ajaxURL;
  //ajax.onCompletion = function(){ uniStatsLoggingAjaxResponse(ajax); };
  ajax.runAJAX();
}
function skipLinkSetPosition(divname){  
  var width = document.documentElement.clientWidth;
  var height = "";
  if(width<=992){
    height = 0;
  }else{
    height = 80;
  }
  if($$d(divname)){
    var $fp = jQuery.noConflict();     
    var divPosition = $fp('#'+divname).offset();  
    $fp('html, body').animate({            
      scrollTop: (divPosition.top - height)  
    }, "slow");
  }  
}

function showMoresLess()
{  
  if($$d("spMoreDiv")){
    if($$d("spMoreDiv").style.display == 'block' || $$d("spMoreDiv").style.display == ''){
      blockNone("spMoreDiv", "none");
      blockNone("spViewMorwLink", "block");
      blockNone("spViewLessLink", "none");     
    }else{
      blockNone("spMoreDiv", "block");
      blockNone("spViewMorwLink", "none");
      blockNone("spViewLessLink", "block");
    }
  }
  lazyloadetStarts(); //Added for trigger lazyloading when clicking the view more by Prabha on 24_Jan_2017
}
//Added by Indumathi.S May_31_2016
function showOtherAcdDep(){
  var divPosition = adv(".clr_prof_ptb3").offset();
  adv('html, body').animate({ scrollTop: divPosition.top - 100 }, "slow");
}

function showAndHideToolTip(divId){
 if(adv("#"+ divId).is(':visible')){
   adv("#"+ divId).hide();
 } else {
   adv("#"+ divId).show();
 }
}
function showToolTip(showId){
  blockNone(showId,'block');
}
function hideToolTip(showId){
  blockNone(showId,'none');
}
//End May_31_2016
adv(window).scroll(function(){
!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
lazyloadetStarts();
});

function changeQuestion(id){
var width = document.documentElement.clientWidth;
 var starArray = ["5star_","4star_","3star_","2star_","1star_"]
 var starwidthId = ["5starWidthId","4starWidthId","3starWidthId","2starWidthId","1starWidthId"];
 var starTextId = ["5StarId","4StarId","3StarId","2StarId","1StarId"];
 for(i=0; i<5;i++){
   var fiveStarValue = $$d(starArray[i]+id).value;
   $$d(starwidthId[i]).style = "width:"+fiveStarValue+"%";
   $$d(starTextId[i]).innerHTML= fiveStarValue+"%";
 }
  var selectdid = adv("#dropdownid_"+id).text();
   $$d("question1").innerHTML = "<span>"+selectdid+"</span>"+'<span><i class="fa fa-angle-down"></i></span>';
}
 
function showSubjectReview(id,collegeId,subjectId){
  var selectdid = adv("#subjectdropdownid_"+id).text();
  var questionText = adv("#selectedsubject").text();
  if(id == 'all'){
    $$d("selectedsubject").innerHTML = "<span>"+'All Subjects'+"</span>"+'<span><i class="fa fa-angle-down"></i></span>';
    $$d("reviewTitle").innerHTML = "Latest reviews";
  }
  else {
   $$d("selectedsubject").innerHTML = "<span>"+selectdid+"</span>"+'<span><i class="fa fa-angle-down"></i></span>';
   $$d("reviewTitle").innerHTML = "Latest "+selectdid+" reviews";
  }
  //Added for GA event logging in review search page by Prabha on SEP_24_2019_REL
  if(selectdid != ''){
    ga('send', 'event', 'Review page filter', 'Subject', selectdid.trim(), 1, {nonInteraction: true});
  }
  //End of GA event logging
  var contextPath = "/degrees";
  var ajax=new sack();
  var url = contextPath+'/get-subject-review.html?collegeId='+collegeId+"&subjectId="+subjectId;   
  ajax.requestFile = url;	
  ajax.onCompletion = function(){subjectReviewDetails(ajax)};
  ajax.runAJAX();
}
function subjectReviewDetails(ajax){
  var response = ajax.response;
  if(response != null){
    var id =  $$d("subjectReviewDetails");
   $$d("subjectReviewDetails").innerHTML = ajax.response;
  }
  adv("#subjectDropDown").css({'display':'none','z-index':'-1'});
}
function setReviewSearchText(curid, searchmsg){var ids = curid.id; if($$(ids).value=='') { $$(ids).value=searchmsg;} }
function clearReviewSearchText(curid, searchmsg){var ids = curid.id;if($$(ids).value==searchmsg){$$(ids).value="";}}
function commonPhraseViewMoreAndLess(viewMoreOrLessBtnId){//Added this for common phrases view less and more pod by Hema.S on 18_DEC_2018_REL
  if("comPhraseViewMore"==viewMoreOrLessBtnId){    
    adv( ".extraCP" ).show();
    blockNone("comPhraseViewMore","none");
    blockNone("comPhraseViewLess","block");
  }else{
    adv( ".extraCP" ).hide();
    blockNone("comPhraseViewMore","block");
    blockNone("comPhraseViewLess","none");
  }
}
//Modified the the functionality to keyword search specific alone by Sangeeth.S for FEB_12_19 rel
function commonPhrasesListProfilePageURL(formNameId,orderBy,obj){  //Added this for looking specific pod by Hema.S on 18_DEC_2018_REL
  var searchUrl = ""; 
  var finalUrl = "";  
  var keyword = ""; 
  var queryStringUrl = "";        
  if("reviewCommonPhraseKwdForm"==formNameId && obj){
    keyword  = obj.innerText;
  }else{
    keyword    = $$('reviewSearchKwd').value;   
  }    
  if(keyword.trim()=="" || keyword=="Something specific?"){
    keyword = "";
  }       
  if(keyword!=""){
   ga('send', 'event', 'Review page filter', 'Keyword', keyword.trim(), 1, {nonInteraction: true});
   var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
      keyword = keyword.replace(reg," ");
      keyword = replaceAll(keyword," ","-");          
      queryStringUrl = "?keyword=" + encodeURIComponent(keyword);
      searchUrl = "/university-course-reviews/search/"+queryStringUrl;
  }  
  if(searchUrl!=""){
    finalUrl = searchUrl;
  }
  if(finalUrl!=""){finalUrl= finalUrl.toLowerCase()};//Added by Sangeeth.S for FEB_12_19 rel lower case of urls  
  if(formNameId!=undefined && $$(formNameId)){
    $$(formNameId).action = finalUrl;
    $$(formNameId).submit();
  }else{
    if(finalUrl ==""){finalUrl="/university-course-reviews/";}	
    location.href = finalUrl;
  }    
  return false;
}
function validateSrchIcn(srchIconId,inputId) {   
   if(adv('#'+srchIconId) && adv('#'+inputId).val().trim()!=""){
     adv('#'+srchIconId).removeClass("srch_dis")
   }else{
     adv('#'+srchIconId).addClass("srch_dis")
   }
}