var flexslider;

function hideHeroImage(videoPath, width, height,coverimage,stdVideoPath){
  searchEventTracking('video','click', 'How does it work video');
  $$D("heroslider").style.display = 'none';
  jQuery("#heroslider_bluscreen").fadeIn('slow');
  $$D("heroslider_bluscreen").style.display = 'block';
  showhide('video_emb','img_anc',videoPath,width,height,coverimage,stdVideoPath);
}

function showhide(a,b,videoPath,width, height,coverimage, stdVideoPath){
  dynamicJsLoad($$D('contextJsPath').value + '/js/video/jwplayer.js',a,b,videoPath,width, height,coverimage, stdVideoPath); //Path added by Prabha on DEC-15
}

function dynamicJsLoad(src,a, b,videoPath,width, height, coverimage,stdVideoPath){
  var script = document.createElement("script");
  script.type = "text/javascript";  
  script.src = src;
  if(script.readyState){//IE
    script.onreadystatechange = function(){
    if (script.readyState == "loaded" || script.readyState == "complete"){
      script.onreadystatechange = null;
      loadJwPlayer(a,b,videoPath,width, height,coverimage);
        if(a=='video_emb'){      
        loadStdChoiceJwplayer(stdVideoPath);
       }
     }
    };
    } else {
      script.onload = function(){
       loadJwPlayer(a,b,videoPath,width, height,coverimage);
       if(a=='video_emb'){      
        loadStdChoiceJwplayer(stdVideoPath);
       }
      };
    }
  if (typeof script != "undefined"){
    document.getElementsByTagName("head")[0].appendChild(script);
  }
}

function hidePlayer(){
 $$D("heroslider_bluscreen").style.display = 'none';
 jwplayer("video_emb").stop();
 jQuery("#heroslider").fadeIn('slow');
 $$D("heroslider").style.display = 'block';  
}
function articleLoader(){
   var j = jQuery.noConflict();
   jQuery.noConflict();
     
		   j(window).load(function() {
						  j('#f_true').flexslider({
						  animation: "slide",
          slideshow:false,
          animationSpeed: 600,
          animationLoop: false,
          slideshowSpeed : 4000,
          itemWidth: 400,
          itemMargin: -1,
          video: false,
          useCSS: false,
          pauseOnAction:true,
          minItems: getWuGridSize(),
          maxItems: getWuGridSize(),
						  start: function(slider){
           flexslider  = slider; 
								   j('div.ltstvds').mouseover(function(){
									   slider.pause();
								   });
								   j('div.ltstvds').mouseout(function() {
									   slider.resume();
								   });
          j('.clone').each(function (){
             
             var ids = j(this).find("img").map(function(){
             return j(this).attr('id')}).get(); 
             for (var i=0; i<ids.length; i++) {
                var art_src = j("#"+ids[i]).attr("data-src");
                j('#f_true').find(".clone").find("#"+ids[i]).attr("src",art_src).removeAttr('id').removeAttr("data-src");
             }
            for(var nxtSlide=(slider.currentSlide*3)+1;nxtSlide<=3;nxtSlide++){
            var src = j("#img"+nxtSlide).attr("data-src");
            j("#img"+nxtSlide).attr("src",src);
            }
            /* var nextslide = slider.currentSlide+1;
             var itemImg = j(this).find("a").children("img");
             var src2 = j("#img"+nextslide).attr("data-src");
             itemImg.attr("src","http://images1.content-wu.com/wu-cont/images/img_px.gif").removeAttr("id");
             j("#img"+nextslide).attr("src",src2).removeAttr("data-src");*/
           });
								  },
         after: function (slider) {
            //instead of going over every single slide, we will just load the next immediate slide
            var nextslide = slider.currentSlide+1;           
            //
            if(nextslide!=1){
               for(var nxtSlide=Math.pow(2,nextslide);nxtSlide<nextslide*3;nxtSlide++){
                  j("#img"+nxtSlide).each(function () {
                    var src = j("#img"+nxtSlide).attr("data-src");
                     j("#img"+nxtSlide).attr("src",src).removeAttr("data-src");
                  });
              }
            }
            //
          }
									});
						   j('.flexslider').flexslider({
						      animation: "slide"
									 });
						  });			
}

function loadStdChoiceJwplayer(stdVideoPath){
    $$D("video_emb_std_choice").innerHTML = "";
    $$D("std_video").style.display = "none";
   $$D("img_std_choice_icon").style.display = "block";
	 jwplayerSetup('flashbanner', stdVideoPath, "http://images2.content-wu.com/wu-cont/images/video_top.jpg", 631, 353, stdVideoPath); //Added by Prabha on DEC-15
}

function loadFacebookTab(){  
  var url=contextPath + "/fbAjax.html";      
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){loadFacebookRes(ajax)};
  ajax.runAJAX();      
}

function loadFacebookRes(ajax){
var res = ajax.response;
$$D("facebookLnk").style.display = 'block';
$$D("facebookDiv").innerHTML = res;
}

function showhidesocialTabs(id){
  if(id=="twitter"){
     $$D("twitterTab").className = "act";
     $$D("twitterLnk").style.display = "block";
     $$D("facebookTab").className = "";
     $$D("facebookLnk").style.display = "none";
     $$D("flickrTab").className = "fl_dts";
     $$D("flickrLnk").style.display = "none";
  }else if(id=="facebook"){
     $$D("facebookTab").className = "act";
     loadFacebookTab();
     $$D("facebookLnk").style.display = "block";
     $$D("twitterTab").className = "";
     $$D("twitterLnk").style.display = "none";
     $$D("flickrTab").className = "fl_dts";
     $$D("flickrLnk").style.display = "none";
  }else if(id=="flickr"){
     loadFlickrTab();  
     $$D("flickrTab").className = "act fl_dts";
     $$D("flickrLnk").style.display = "block";
     $$D("facebookTab").className = "";
     $$D("facebookLnk").style.display = "none";
     $$D("twitterTab").className = "";
     $$D("twitterLnk").style.display = "none";        
  }
}


function getWuGridSize() {
  var dev2 = jQuery.noConflict();
  var gridSize = (dev2(window).width() <= 480) ? 1 : (dev2(window).width() <= 992) ? 2 : 3;
  return gridSize;
	}

function loadFlickrTab(){    

   var  url = contextPath + "/flickAjax.html?homeFlickrURL=yes";          
  //alert("url:"+url);
  var ajax=new sack();
  ajax.requestFile = url;
  ajax.onCompletion = function(){loadFlickrRes(ajax)};
  ajax.runAJAX();     
}

function loadFlickrRes(ajax){
  var res = ajax.response;   
  $$D("flickr_badge_wrapper").innerHTML = res;
}