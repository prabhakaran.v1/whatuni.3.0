/**
 * 
 */
var jq = jQuery.noConflict();
var contextPath = "/degrees";

var vulnerableKeywords = ["onclick", "`", "{", "}", "~", "|", "^", "http:", "javascript:", "https:", "ftp:", "type='text/javascript'", "type=\"text/javascript\"", "language='javascript'", "language=\"javascript\""];
var htmlTags = ["<!DOCTYPE", "<!--", "<acronym", "<abbr", "<address", "<applet", "<area", "<article", "<aside", "<audio", "<a", "<big", "<bdi", "<basefont", "<base", "<bdo", "<blockquote", "<body", "<br", "<button", "<b", "<center", "<canvas", "<caption", "<cite", "<code", "<colgroup", "<col", "<dir", "<datalist", "<dd", "<del", "<details", "<dfn", "<dialog", "<div", "<dl", "<dt", "<embed", "<em", "<frameset", "<frame", "<font", "<fieldset", "<figcaption", "<figure", "<footer", "<form", "<header", "<head", "<h1", "<h2", "<h3", "<h4", "<h5", "<h6", "<hr", "<html", "<iframe", "<img", "<ins", "<input", "<i", "<kbd", "<keygen", "<label", "<legend", "<link", "<li", "<main", "<map", "<mark", "<menuitem", "<menu", "<meta", "<meter", "<noscript", "<noframes", "<nav", "<object", "<ol", "<optgroup", "<option", "<output", "<param", "<\pre", "<progress", "<p", "<q", "<rp", "<rt", "<ruby", "<svg", "<samp", "<script", "<section", "<select", "<small", "<strike", "<source", "<span", "<strong", "<style", "<sub", "<summary", "<sup", "<s", "<table", "<thead", "<tbody", "<tfoot", "<tt", "<td", "<th", "<tr", "<text\area", "<time", "<title", "<track", "<ul", "<u", "<var", "<video", "<wbr", "<\/acronym", "<\/abbr", "<\/address", "<\/applet", "<\/area", "<\/article", "<\/aside", "<\/audio", "<\/a", "<\/big", "<\/bdi", "<\/basefont", "<\/base", "<\/bdo", "<\/blockquote", "<\/body", "<\/br", "<\/button", "<\/b", "<\/center", "<\/canvas", "<\/caption", "<\/cite", "<\/code", "<\/colgroup", "<\/col", "<\/dir", "<\/datalist", "<\/dd", "<\/del", "<\/details", "<\/dfn", "<\/dialog", "<\/div", "<\/dl", "<\/dt", "<\/embed", "<\/em", "<\/frameset", "<\/frame", "<\/font", "<\/fieldset", "<\/figcaption", "<\/figure", "<\/footer", "<\/form", "<\/header", "<\/head", "<\/h1", "<\/h2", "<\/h3", "<\/h4", "<\/h5", "<\/h6", "<\/hr", "<\/html", "<\/iframe", "<\/img", "<\/ins", "<\/input", "<\/i", "<\/kbd", "<\/keygen", "<\/label", "<\/legend", "<\/link", "<\/li", "<\/main", "<\/map", "<\/mark", "<\/menuitem", "<\/menu", "<\/meta", "<\/meter", "<\/noscript", "<\/noframes", "<\/nav", "<\/object", "<\/ol", "<\/optgroup", "<\/option", "<\/output", "<\/param", "<\/pre", "<\/progress", "<\/p", "<\/q", "<\/rp", "<\/rt", "<\/ruby", "<\/svg", "<\/samp", "<\/script", "<\/section", "<\/select", "<\/small", "<\/strike", "<\/source", "<\/span", "<\/strong", "<\/style", "<\/sub", "<\/summary", "<\/sup", "<\/s", "<\/table", "<\/thead", "<\/tbody", "<\/tfoot", "<\/tt", "<\/td", "<\/th", "<\/tr", "<\/textarea", "<\/time", "<\/title", "<\/track", "<\/ul", "<\/u", "<\/var", "<\/video", "<\/wbr"];

function $$(id) {
	return document.getElementById(id);
}

jq(document).ready(function() {

	isItMobile();
	
	loadMobileSiteURL();

	var compSpalshValue = getJavaCookie("DS_WC_TO_COM", "get");
	if (compSpalshValue == "TRUE") {
		if ($$("dontShowCompSplashHidden")) {
			$$("dontShowCompSplashHidden").value = "TRUE";
		}
	}
	
	//1024 resolution script start here
	var widthSky = document.documentElement.clientWidth;
	if (widthSky > 992) {
		var skypescrapperres = parseInt((screen.width), 10);
		if (skypescrapperres < 1280) {
			if (document.getElementById("content-bg")) {
				document.getElementById("content-bg").className = "csrn_rn";
			}
			if (document.getElementById("wrapper")) {
				document.getElementById("wrapper").className = "wsrn_rn";
			}
			if (document.getElementById("sky")) {
				document.getElementById("sky").className = "hidden";
			}

			if (document.getElementById("content-cmpr")) {
				document.getElementById("content-cmpr").style.padding = "0px";
				document.getElementById("content-cmpr").style.width = "100%";
			}
			if (document.getElementById("clrb1")) {
				document.getElementById("clrb1").style.left = "15px";
			}
		}
	}

	initmb();

	jq.fn.setWrapperWidth();
	
	setTimeout(function() {
		displayTickerTape()
	}, 2000);


	scrollToTopReadyFunction(); 
	
	scrollToTopResizeFunction(); 

	if (document.documentElement.clientWidth < 992) {
		dynamicLoadJS(document.getElementById("contextJsPath").value + '/js/jquery/' + document.getElementById("smartBannerJs").value);
		setTimeout(function() {
			$scroll(function() {
				$scroll.smartbanner({});
			})
		}, 500);
	}

	//checkCookie();  

});

jq(window).scroll(function() {

	scrollToTopScrollFunction();
});


jq.fn.setWrapperWidth = function() {
	var res = screen.width;
	if ($$("content-bg")) {
		if (res == 1024) {
			$$("content-bg").className = "wrp1";
		}
	}
}

function displayTickerTape() {
	if ($$("tickerTape")) {
		$$("tickerTape").style.display = "block";
	}
}

