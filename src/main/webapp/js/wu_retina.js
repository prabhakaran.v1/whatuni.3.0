var $jq = jQuery.noConflict();

function isBlankOrNull(value){
  var flag = true;
  if(value != null && value != ""){
    flag = false;
  }
  return flag;
}

//To find retina device
function isRetina() {
  var query = '(-webkit-min-device-pixel-ratio: 1.5),\
  (min--moz-device-pixel-ratio: 1.5),\
  (-o-min-device-pixel-ratio: 3/2),\
  (min-device-pixel-ratio: 1.5),\
  (min-resolution: 144dpi),\
  (min-resolution: 1.5dppx)';
  if (window.devicePixelRatio > 1 || (window.matchMedia && window.matchMedia(query).matches)) {
    return true;
  }
  return false;
}

function retina(){
  var deviceWidth = $jq(window).width() || document.documentElement.clientWidth;
  var isRetinaImageNeed = "", isLazyLoadNeed="";
  var dataSrc="data-src", dataRjs="data-rjs", img="img", retinaNeed='Y', px_annotation="px@", 
      dataLazyLoadFlag="data-lazyLoad-flag", lazyLoadDone="lazy_done", lazyLoadNotDone="lazy_not_done", px="px";
  
  var cpc_mobile_414="_414", cpc_desktop_1500="_1500", cpc_mobile_343="_343", cpc_tab_232="_232",
      cpc_desktop_355="_355", cpc_dskandtab_628="_628", cpc_vid_thumb_tab_357="_357", cpc_vid_thumb_desktop_543="_543";
  
  //CPC Hero Image retina
  if(devicePixel() >= 1){
    if($jq('#homeHeroImgDivId').length > 0){
      var heroImageId = $jq('#homeHeroImgDivId').get(0);  
      var heroimageSrc = heroImageId.getAttribute(dataSrc);
      isRetinaImageNeed = heroImageId.getAttribute(dataRjs);
      isLazyLoadNeed = heroImageId.getAttribute(dataLazyLoadFlag);
      if((!isBlankOrNull(heroimageSrc) && heroimageSrc.indexOf(px_annotation) <= -1) && isScrolledIntoView(heroImageId, false) && isLazyLoadNeed == lazyLoadNotDone){
        if(!isBlankOrNull(heroimageSrc) && !isBlankOrNull(isRetinaImageNeed)&& isRetinaImageNeed != undefined && isRetinaImageNeed == retinaNeed){
          var heroImgLastDot = heroimageSrc.lastIndexOf('.');  
          if(!isBlankOrNull(heroImgLastDot)){ 
            var heroImagepath = heroimageSrc.substring(0, heroImgLastDot);
     	    var fileExtenstion = heroimageSrc.substring(heroImgLastDot + 1);
     	    var heroImageRetinaSrc = "", heroImgPixel = "";
     	    if(window.isRetina()){
     	      if(deviceWidth < 480){
     	        heroImgPixel = cpc_mobile_414;
     	      }else if(deviceWidth > 481 && deviceWidth < 992){
     	        heroImgPixel = cpc_desktop_1500; 
     	      } 
     	   } else{
     	     heroImgPixel = cpc_desktop_1500; 
  	       }
     	    if(heroimageSrc.indexOf(cpc_mobile_414) > -1 || heroimageSrc.indexOf(cpc_desktop_1500) > -1) {
     	    	heroImageRetinaSrc = heroImagepath + appendingPixel() + fileExtenstion;
     	    } else {
     	    	heroImageRetinaSrc = heroImagepath + heroImgPixel + px + appendingPixel() + fileExtenstion;	
     	    }     	   
     	   heroImageId.style.backgroundImage = 'url(' + heroImageRetinaSrc + ')';
           heroImageId.setAttribute(dataSrc, heroImageRetinaSrc);
           heroImageId.setAttribute(dataLazyLoadFlag, lazyLoadDone);
         } 
       }
     }
   }
      
   //CPC Middle Content Retina
   if(($jq('#contentBgId').length > 0 && $jq('#slider').length == 0) || $jq('.cpc_sngl_vid')){
     var imagesTagName =  document.getElementsByTagName(img); 
     for(var i=0; i<imagesTagName.length; i++){
       var middleImagesSrc = imagesTagName[i].getAttribute(dataSrc);	
       isRetinaImageNeed = imagesTagName[i].getAttribute(dataRjs);
       isLazyLoadNeed = imagesTagName[i].getAttribute(dataLazyLoadFlag);
       if((!isBlankOrNull(middleImagesSrc) && middleImagesSrc.indexOf(px_annotation) <= -1) && isScrolledIntoView(imagesTagName[i], false) && isLazyLoadNeed == lazyLoadNotDone){         
          var middleImageslastDot = middleImagesSrc.lastIndexOf('.');	 
          if(!isBlankOrNull(middleImageslastDot)){
            var middleImagesPath = middleImagesSrc.substring(0, middleImageslastDot); 
            var fileExtenstion = middleImagesSrc.substring(middleImageslastDot + 1);
            var middleImagesRetinaSrc = "",  middleImagesPixel = "";
            if(!isBlankOrNull(middleImagesSrc) && !isBlankOrNull(isRetinaImageNeed) && isRetinaImageNeed != undefined && isRetinaImageNeed == retinaNeed){
              if(window.isRetina()){
         	    if(deviceWidth < 480){
         	      middleImagesPixel = cpc_mobile_343;
         	    }else if(deviceWidth > 481 && deviceWidth < 992){
         	      middleImagesPixel = cpc_tab_232;
         	    }
         	  }else{
         	    middleImagesPixel = cpc_desktop_355;
      	      }
              if(middleImagesSrc.indexOf(cpc_mobile_343) > -1 || middleImagesSrc.indexOf(cpc_tab_232) > -1 || middleImagesSrc.indexOf(cpc_desktop_355) > -1) {
              	middleImagesRetinaSrc = middleImagesPath + appendingPixel() + fileExtenstion;
         	      } else {
         	    	  middleImagesRetinaSrc = middleImagesPath + middleImagesPixel + px + appendingPixel() + fileExtenstion;	
         	      } 
              } else {
            	  middleImagesRetinaSrc = middleImagesSrc;
              }              
              imagesTagName[i].src = middleImagesRetinaSrc;
              imagesTagName[i].setAttribute(dataSrc, middleImagesRetinaSrc);
              imagesTagName[i].setAttribute(dataLazyLoadFlag, lazyLoadDone);        
          }  
        }
      }
    }   
      
    //CPC LightBox Retina
    if($jq('#slider').length > 0){
      var lightBoxImageTagName =  document.getElementsByTagName(img); 
      for(var i=0; i<lightBoxImageTagName.length; i++){
        var lightBoxSrc = lightBoxImageTagName[i].getAttribute(dataSrc);	
        isRetinaImageNeed = lightBoxImageTagName[i].getAttribute(dataRjs);
        isLazyLoadNeed = lightBoxImageTagName[i].getAttribute(dataLazyLoadFlag);
        if((!isBlankOrNull(lightBoxSrc) && lightBoxSrc.indexOf(px_annotation) <= -1) && isScrolledIntoView(lightBoxImageTagName[i], false) && isLazyLoadNeed == lazyLoadNotDone){
          if(!isBlankOrNull(lightBoxSrc) && !isBlankOrNull(isRetinaImageNeed) && isRetinaImageNeed != undefined && isRetinaImageNeed == retinaNeed){  
            var lightBoxImageLastDot = lightBoxSrc.lastIndexOf('.');	  
            if(!isBlankOrNull(lightBoxImageLastDot)){
              var lightBoxImagePath = lightBoxSrc.substring(0, lightBoxImageLastDot); 
              var fileExtenstion = lightBoxSrc.substring(lightBoxImageLastDot + 1);
              var lightBoxImageRetinaSrc = "", lightBoxImagePixel = "";	
              if(window.isRetina()){
         	    if(deviceWidth < 480){
         	      lightBoxImagePixel = cpc_mobile_343;
         	    }else if(deviceWidth > 481 && deviceWidth < 992){
         	      lightBoxImagePixel = cpc_dskandtab_628
         	    } 
         	  }else{
         	    lightBoxImagePixel = cpc_dskandtab_628;
      	      }
              if(lightBoxSrc.indexOf(cpc_mobile_343) > -1 || lightBoxSrc.indexOf(cpc_dskandtab_628) > -1) {
            	  lightBoxImageRetinaSrc = lightBoxImagePath + appendingPixel() + fileExtenstion;
         	  } else {
                  lightBoxImageRetinaSrc = lightBoxImagePath + lightBoxImagePixel + px + appendingPixel() + fileExtenstion;
         	  }
              lightBoxImageTagName[i].src = lightBoxImageRetinaSrc;
              lightBoxImageTagName[i].setAttribute(dataSrc, lightBoxImageRetinaSrc);
              lightBoxImageTagName[i].setAttribute(dataLazyLoadFlag, lazyLoadDone);
            }
          }
        }
      }
    }  
  }
}

function devicePixel(){
  var devicePixel = window.devicePixelRatio;
  devicePixel = devicePixel > 2.5 ? 2 : devicePixel > 1.5 && devicePixel < 2.5 ? 2 : 1;	
  return devicePixel;
}

function appendingPixel(){
  var deviceWidth = $jq(window).width() || document.documentElement.clientWidth;
  var devicePixelValue = "";
  var devicePixelValueMobile = devicePixel() == 1 ? "." : ('@' + devicePixel() + 'x.');
  devicePixelValue = deviceWidth < 480 ? (devicePixelValueMobile) : ".";
  return devicePixelValue;
}

//fn to find visible images in screen 
function isScrolledIntoView(element, fullyInView){
  var pageTop = $jq(window).scrollTop();      
  var pageBottom = pageTop + $jq(window).height();
  var elementTop = $jq(element).offset().top;
  var elementBottom = elementTop + $jq(element).height();
  if (fullyInView === true) {
    return ((pageTop < elementTop) && (pageBottom > elementBottom));
  } else {
    return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
  }
}

$jq(window).on('load', function() {
	retina();
});

$jq(window).scroll(function() {
	retina();
});
