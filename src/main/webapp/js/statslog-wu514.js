function callStatLogRequest(pWhat, pWhere, ipaddress) {
	var httpRequest;
	var lHost = ipaddress;
	var lPath = "%2Fwhatuni%2F";
	var lNum = Math.floor(Math.random() * 1000000000);
	var lURL = unescape(lHost + lPath + pWhat + pWhere + "%26rn%3D" + lNum);
	if (window.XMLHttpRequest) {
		httpRequest = new XMLHttpRequest();
		if (httpRequest.overrideMimeType) {
			httpRequest.overrideMimeType('text/xml');
		}
	} else if (window.ActiveXObject) {
		try {
			httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) { 
			try {
				httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
			}catch (e) {}
		}
	}
	if (!httpRequest) {
		return false;
	}
	httpRequest.open('GET', lURL, true);
	httpRequest.send('');
	return true;
}
