var deviceWidth =  null;
function trimString(inString){
  return inString.replace(/^\s+|\s+$/g,"");
}

function $$(id){
  return document.getElementById(id);
}

function getDeviceWidth() { return (window.innerWidth > 0) ? window.innerWidth : screen.width; }

function cpeApplyNowWebClick(obj, collegeid, subOrderItemId, networkId, externalUrl){
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    	networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
	    subOrderItemId = '0';
  }
  cpeDbStatsLogging(collegeid, subOrderItemId, networkId, externalUrl, "APPLYNOW");
}

function cpeWebClick(obj, collegeid, subOrderItemId, networkId, externalUrl){
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }
  cpeDbStatsLoggingWithAdroll(collegeid, subOrderItemId, networkId, externalUrl, "WEBSITE");
  insightIntLogging(collegeid,"webclick");
}

function cpeWebClickClearing(obj, collegeid, subOrderItemId, networkId, externalUrl){
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }
  cpeDbStatsLogging(collegeid, subOrderItemId, networkId, externalUrl, "WEBSITE_CLEARING");
}

function cpeHotlineClearing(obj, collegeid, subOrderItemId, networkId, externalUrl){
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }
  cpeDbStatsLogging(collegeid, subOrderItemId, networkId, externalUrl, "HOTLINE");
}
function cpeFeaturedHotlineClearing(obj, collegeid, networkId, externalUrl){
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  cpeDbStatsLogging(collegeid, '', networkId, externalUrl, "FEATURED_HOTLINE");
}
function cpeProfileInnerLinkWebClick(obj, collegeid, subOrderItemId, networkId, sectionType){
  
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }  
  var externalUrl = substringFromHref(obj);  
  var sectionNameObj = $$("currentSectionName");  
  var sectionName = "Overview";
  
  if(sectionNameObj != null) {
    sectionName = sectionNameObj.value;
  }  
  if(sectionName == null || trimString(sectionName).length == 0 || sectionName == 'null'){
	   sectionName = 'Overview';
  }    
  sectionName = getSeoSectionName(sectionName);  
  var innerLinkProfileType = "WEBSITE_PROFILE_INNERLINK";  
  //Added to handle the clearing stats log for CMMT
  if("CLEARING" == sectionType){
    innerLinkProfileType = "CLEARING_WEBSITE_PROFILE_INNERLINK";
  }
  cpeDbStatsLogging(collegeid, subOrderItemId, networkId, externalUrl,innerLinkProfileType, sectionName);
}

function cpeProspectusWebformClick(obj, collegeid, subOrderItemId, networkId, externalUrl){
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }
  cpeDbStatsLoggingWithAdroll(collegeid, subOrderItemId, networkId, externalUrl, "PROSPECTUS_WEBFORM");
}

function cpeEmailWebformClick(obj, collegeid, subOrderItemId, networkId, externalUrl){
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }
  cpeDbStatsLoggingWithAdroll(collegeid, subOrderItemId, networkId, externalUrl, "EMAIL_WEBFORM");
}

function nonAdvertiasorPdfDownloadClick(obj, collegeid, courseId, subOrderItemId, networkId, externalUrl){
  /*if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }*/
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }
  cpeDbStatsLoggingWithCourse(collegeid, courseId, subOrderItemId, networkId, externalUrl, "NON_ADV_PDF_DOWNLOAD");  
}

function cpeProspectusFormSubmit(obj, collegeid, subOrderItemId, cpeQualificationNetworkId){
}
//wu411_20120612 Sekhar K for Download Prospectus
function cpeDownloadProspectusFormSubmit(obj, collegeid, subOrderItemId, cpeQualificationNetworkId){
}

function cpeEmailFormSubmit(obj, collegeid, subOrderItemId, cpeQualificationNetworkId){
}

function cpeKISInnerLink(obj, collegeid, subOrderItemId, networkId, clickType){
 
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }  
  var externalUrl = substringFromHref(obj);  
  kisDbStatsLogging(collegeid, subOrderItemId, networkId, externalUrl, clickType, "");
}


function cpeSpotLightVideo(collegeid, externalUrl, reloadPage){
  if(collegeid!=0 && collegeid!="" && collegeid!=null){
    if(reloadPage!="" && reloadPage!=null){
       cpeSpotlightDbStatsLogging(collegeid, '', '', encodeURIComponent(externalUrl), "SPOTLIGHT_VIDEO",'',externalUrl, reloadPage);       
   }else{
       cpeDbStatsLogging(collegeid, '', '', encodeURIComponent(externalUrl), "SPOTLIGHT_VIDEO");
   }
  }
}

function cpeSpotlightDbStatsLogging(collegeid, subOrderItemId, networkId, externalUrl, clickType, sectionName, hrefUrl,isblank){
  //alert('cpeDbStatsLogging("'+collegeid+'", "'+subOrderItemId+'", "'+networkId+'", "'+externalUrl+'", "'+clickType+'", "'+sectionName+'")'); /* remove SOP */
  var ajax=new sack();
  /* puts an entry in "wu_stats_log_pkg.add_log_fn" */
  deviceWidth =  getDeviceWidth();
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z='+collegeid+"&subOrderItemId="+subOrderItemId+"&networkId="+networkId+"&externalUrl="+externalUrl+"&clickType="+clickType+"&sectionName="+sectionName+'&screenwidth='+deviceWidth;     
  //alert("url: "+url);  
  ajax.requestFile = url;
  /* Specify function that will be executed after file has been found */
  ajax.onCompletion = function(){ responseSpotlightData(ajax, hrefUrl,isblank); };	
  ajax.runAJAX();
}

function responseSpotlightData(ajax, url,isblank){
  document.location.href = url;
}

//18_Nov_2014 added hero image stats view and click by Thiyagu G.
function cpeHeroImage(collegeid, externalUrl, reloadPage, clickType){
  if(collegeid!=0 && collegeid!="" && collegeid!=null){
    if(reloadPage!="" && reloadPage!=null){       
       cpeHeroImageDbStatsLogging(collegeid, '', '', encodeURIComponent(externalUrl), clickType,'',externalUrl, reloadPage);       
   }else{       
       cpeDbStatsLogging(collegeid, '', '', encodeURIComponent(externalUrl), clickType);
   }
  }
}
function cpeHeroImageDbStatsLogging(collegeid, subOrderItemId, networkId, externalUrl, clickType, sectionName, hrefUrl,isblank){  
  var ajax=new sack();
  /* puts an entry in "wu_stats_log_pkg.add_log_fn" */
  deviceWidth =  getDeviceWidth();
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z='+collegeid+"&subOrderItemId="+subOrderItemId+"&networkId="+networkId+"&externalUrl="+externalUrl+"&clickType="+clickType+"&sectionName="+sectionName+'&screenwidth='+deviceWidth;       
  ajax.requestFile = url;
  /* Specify function that will be executed after file has been found */
  ajax.onCompletion = function(){ responseHeroImageData(ajax, hrefUrl,isblank); };	
  ajax.runAJAX();
}
function responseHeroImageData(ajax, url,isblank){
  return false;
}

//18_Nov_2014 added advice details db stats logging by Thiyagu G.
function cpeAdviceDetailPage(postId, clickType){
  if(postId!=null && postId!=0 && postId!=""){
    cpeDbStatsLogging('', '', '', postId, clickType, '');
  }
}
//9_DEC_2014 added by Amir for provider db stats logging
function providerPageViewLogging(collegeId,clickType,externalUrl) {
  var subOrderItemId = "0";  
  var networkId      = "2";     
  if(externalUrl == null || trimString(externalUrl).length == 0 || externalUrl == 'null'){
    externalUrl = '';
  }
  if(clickType == 'PROVIDER_RESULTS_LIST') {
    var studyModeId = $$('studymodeId') ? "&studymodeid="+$$('studymodeId').value : "";
  }
  cpeDbStatsLogging(collegeId, subOrderItemId, networkId, externalUrl, clickType, '', studyModeId);
}

function cpeDbStatsLogging(collegeid, subOrderItemId, networkId, externalUrl, clickType, sectionName, studyModeId){
  //alert('cpeDbStatsLogging("'+collegeid+'", "'+subOrderItemId+'", "'+networkId+'", "'+externalUrl+'", "'+clickType+'", "'+sectionName+'")'); /* remove SOP */
  var ajax=new sack();
  /* puts an entry in "wu_stats_log_pkg.add_log_fn" */
  deviceWidth =  getDeviceWidth();
  studyModeId = (studyModeId == null) ? "" : studyModeId;
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z='+collegeid+"&subOrderItemId="+subOrderItemId+"&networkId="+networkId+"&externalUrl="+encodeURIComponent(externalUrl)+"&clickType="+clickType+"&sectionName="+sectionName+'&screenwidth='+deviceWidth + studyModeId;;     
  //alert("url: "+url);  
  ajax.requestFile = url;
  /* Specify function that will be executed after file has been found */
  ajax.onCompletion = function(){ responseData(ajax); };	
  ajax.runAJAX();
}


function richProfileVideoStatsLog(collegeId,subOrderItemId,networkId,profileId,clickType)
{    
  var sectionName = document.getElementById("curPofSec").value;  
  var ajax=new sack();
  /* puts an entry in "wu_stats_log_pkg.add_log_fn" */
  deviceWidth =  getDeviceWidth();
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z='+collegeId+"&subOrderItemId="+subOrderItemId+"&networkId="+networkId+"&externalUrl="+sectionName+"&clickType="+clickType+"&sectionName="+sectionName+"&richProfile=true&profileId="+profileId+'&screenwidth='+deviceWidth;      
  ajax.requestFile = url;
  /* Specify function that will be executed after file has been found */
  ajax.onCompletion = function(){ responseData(ajax); };	
  ajax.runAJAX();  
}

function kisDbStatsLogging(collegeid, subOrderItemId, networkId, externalUrl, clickType, sectionName){
  var ajax=new sack();
  /* puts an entry in "wu_stats_log_pkg.add_log_fn" */
  deviceWidth =  getDeviceWidth();
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z='+collegeid+"&subOrderItemId="+subOrderItemId+"&networkId="+networkId+"&externalUrl="+escape(externalUrl)+"&clickType="+clickType+"&sectionName="+sectionName+'&screenwidth='+deviceWidth;     
  //alert("url: "+url);  
  ajax.requestFile = url;
  /* Specify function that will be executed after file has been found */
  ajax.onCompletion = function(){ responseData(ajax); };	
  ajax.runAJAX();
}
function clearingHomeVideoStatsLog(collegeId,subOrderItemId,networkId,profileId,clickType, sectionName){
  var ajax=new sack();
  /* puts an entry in "wu_stats_log_pkg.add_log_fn" */
  deviceWidth = getDeviceWidth();
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z='+collegeId+"&subOrderItemId="+subOrderItemId+"&networkId="+networkId+"&externalUrl="+sectionName+"&clickType="+clickType+"&sectionName="+sectionName+"&profileId="+profileId +'&screenwidth='+deviceWidth;      
  ajax.requestFile = url;
  /* Specify function that will be executed after file has been found */
  ajax.onCompletion = function(){ responseData(ajax); };	
  ajax.runAJAX();  
}

function responseData(ajax){
  return false;
}

function substringFromHref(obj){
	var p_url = "";
	if(obj != null){
		var hrefValue = obj.href;
		//alert("1) obj: "+obj);
		if((hrefValue.indexOf('OBJ_PLS_TRACK_WUNI_WEBSITE') > -1) || (hrefValue.indexOf('p_url') > -1)){
			//alert("11) hrefValue: "+hrefValue);
			var hrefArray = hrefValue.split("&");
			for(var i=0; i<hrefArray.length; i++){
				//alert("111) hrefArray[i]: "+hrefArray[i]);
				if(hrefArray[i].indexOf('p_url=') > -1){
					p_url = hrefArray[i];
				}						
			}
			p_url = p_url.substring(6);	
			//alert("222) p_url: "+p_url);
			//var p_url=hrefValue.substring(hrefValue.indexOf('p_url=')+2,hrefValue.length)
			//alert("p_url: "+p_url);	
		} else {
			p_url = hrefValue
			//alert("333) p_url: "+p_url);
		}
	}
	//alert("999) p_url: "+p_url);
	return p_url;
}

function getSeoSectionName(sectionName){
	if(sectionName == 'Overview') return 'Overview';
	if(sectionName == 'Accommodation & Location') return 'Location';
	if(sectionName == 'Student Union & Entertainment') return 'Entertainment';
	if(sectionName == 'Courses, Academics & Facilities') return 'Facilities';
	if(sectionName == 'Welfare') return 'Welfare';
	if(sectionName == 'Fees And Funding') return 'Funding';
	if(sectionName == 'Job Prospects') return 'Prospects';
	if(sectionName == 'Contact Details') return 'Contact';
	return sectionName;
}

function insightIntLogging(collegeId,interactionType) {
  var ajax=new sack();
  deviceWidth =  getDeviceWidth();
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?hcCollegeId='+collegeId+'&screenwidth='+deviceWidth;
  ajax.requestFile = url;  
  ajax.onCompletion = function(){ insightIntLoggingData(ajax, interactionType); };	
  ajax.runAJAX();
}

function insightIntLoggingData(ajax, interactionType) {  
 var institutionId = trimString(ajax.response);
 var insDimQualType   =   "";
 var insDimLevelType  =   "";
 var insDimUID        =   "";
 var insDimYOE        =   "";  
 
 if($$("insDimQualType")){
  insDimQualType  = $$("insDimQualType").value;
 } 
 if($$("insDimLevelType")){
  insDimLevelType  = $$("insDimLevelType").value;
 } 
 if($$("insDimUID")){
  insDimUID  = $$("insDimUID").value;
 } 
 if($$("insDimYOE")){
  insDimYOE  = $$("insDimYOE").value;
 }     
 //Insights account changed and removed sampling for 08_MAR_2016, By Thiyagu G.
 ga('create', 'UA-52581773-4', 'auto', {'name': 'newTracker'});
 ga('newTracker.set', 'contentGroup1', 'whatuni');
 ga('newTracker.set', 'dimension1',interactionType);
 ga('newTracker.set', 'dimension2','whatuni');
 ga('newTracker.set', 'dimension3',institutionId);  
 
 if(insDimQualType!=""){
  ga('newTracker.set', 'dimension4',insDimQualType);
 }
 if(insDimLevelType!=""){
  ga('newTracker.set', 'dimension5',insDimLevelType);
 }
 if(insDimUID!=""){
  ga('newTracker.set', 'dimension15',insDimUID);
 }
 if(insDimYOE!=""){
  ga('newTracker.set', 'dimension19',insDimYOE);  
 }    
 ga('newTracker.send', 'pageview');  
}

//Added for adroll remarketing 13-Jan-2015_rel
function adRollLoggingRequestInfo(collegeid,pagename){
 var ajax=new sack();
  var url = '/degrees/adrollmarketingajax.html?collegeid='+collegeid+"&buttonType="+"RI";
  ajax.requestFile = url;
  ajax.onCompletion = function(){ responseDataforAdRollRI(ajax,pagename); };	
  ajax.runAJAX();
}

function responseDataforAdRollRI(ajax,pagename){
  var segment = ajax.response;
  if(ajax.response!="" || ajax.response!=null){
      try {
      __adroll.record_user({"adroll_segments": segment});      
    } catch(err){}  
  }
  location.href = pagename;
}

function adRollLoggingGetPros(collegeid){
 var ajax=new sack();
  var url = '/degrees/adrollmarketingajax.html?collegeid='+collegeid+"&buttonType="+"GP";
  ajax.requestFile = url;
  ajax.onCompletion = function(){ responseDataforAdRollGP(ajax); };	
  ajax.runAJAX();
}

function responseDataforAdRollGP(ajax,pagename){
  var segment = ajax.response;
  if(ajax.response!=""){
      try {
      __adroll.record_user({"adroll_segments": segment});      
    } catch(err){}  
  }
}

function cpeDbStatsLoggingWithAdroll(collegeid, subOrderItemId, networkId, externalUrl, clickType, sectionName){
  //alert('cpeDbStatsLogging("'+collegeid+'", "'+subOrderItemId+'", "'+networkId+'", "'+externalUrl+'", "'+clickType+'", "'+sectionName+'")'); /* remove SOP */
  var ajax=new sack();
  var adRollValue  = "";
  /* puts an entry in "wu_stats_log_pkg.add_log_fn" */
  if(sectionName =="WEBSITE"){
    adRollValue = "VW";
  }else if(sectionName =="PROSPECTUS_WEBFORM"){
   adRollValue = "GP";
  }else if(sectionName =="EMAIL_WEBFORM"){
   adRollValue = "RI";
  }
  deviceWidth =  getDeviceWidth();
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z='+collegeid+"&subOrderItemId="+subOrderItemId+"&networkId="+networkId+"&externalUrl="+encodeURIComponent(externalUrl)+"&clickType="+clickType+"&sectionName="+sectionName+"&adRollType="+adRollValue+'&screenwidth='+deviceWidth;     
  //alert("url: "+url);  
  ajax.requestFile = url;
  /* Specify function that will be executed after file has been found */
  ajax.onCompletion = function(){ responseDataforAdRoll(ajax); };	
  ajax.runAJAX();
}
//To log webclick with courseid for 11-AUG-2015_REL
function cpeWebClickWithCourse(obj, collegeid, courseId, subOrderItemId, networkId, externalUrl){
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }
  cpeDbStatsLoggingWithCourseAdroll(collegeid, courseId, subOrderItemId, networkId, externalUrl, "WEBSITE");  
  insightIntLogging(collegeid,"webclick");
}
//
function getWugoCpeStatsLogging(collegeId, courseId, opportunityId, subOrderIteId){
  var networkId = '2';
  
  wugoCpeStatsLogging(collegeId, courseId, networkId, opportunityId, "APPLY_NOW", subOrderItemId);
}
//
function wugoCpeStatsLogging(collegeid, courseId, networkId, externalUrl, clickType, subOrderItemId){
  var ajax=new sack();
  deviceWidth =  getDeviceWidth();
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z='+collegeid+"&networkId="+networkId+"&clickType="+clickType+"&vwcourseId="+courseId+"&screenwidth="+deviceWidth+"&externalUrl="+externalUrl+"&subOrderItemId="+subOrderItemId;     
  ajax.requestFile = url;
  ajax.onCompletion = function(){ responseData(ajax); };	
  ajax.runAJAX();
}
//
function cpeWebClickClearingWithCourse(obj, collegeid, courseId, subOrderItemId, networkId, externalUrl){
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }
  cpeDbStatsLoggingWithCourse(collegeid, courseId, subOrderItemId, networkId, externalUrl, "WEBSITE_CLEARING");
}
//
function cpeDbStatsLoggingWithCourse(collegeid, courseId, subOrderItemId, networkId, externalUrl, clickType, sectionName){
  var ajax=new sack();
  deviceWidth =  getDeviceWidth();
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z='+collegeid+"&subOrderItemId="+subOrderItemId+"&networkId="+networkId+"&externalUrl="+encodeURIComponent(externalUrl)+"&clickType="+clickType+"&sectionName="+sectionName+"&vwcourseId="+courseId+'&screenwidth='+deviceWidth;     
  ajax.requestFile = url;
  ajax.onCompletion = function(){ responseData(ajax); };	
  ajax.runAJAX();
}
//
function cpeDbStatsLoggingWithCourseAdroll(collegeid, courseId, subOrderItemId, networkId, externalUrl, clickType, sectionName){
  var ajax=new sack();
  var adRollValue  = "";
  if(sectionName =="WEBSITE"){
    adRollValue = "VW";
  }else if(sectionName =="PROSPECTUS_WEBFORM"){
   adRollValue = "GP";
  }else if(sectionName =="EMAIL_WEBFORM"){
   adRollValue = "RI";
  }
  deviceWidth = getDeviceWidth();
  var url = '/degrees/ajaxlogging/cpe-web-click-db-logging.html?z='+collegeid+"&subOrderItemId="+subOrderItemId+"&networkId="+networkId+"&externalUrl="+encodeURIComponent(externalUrl)+"&clickType="+clickType+"&sectionName="+sectionName+"&adRollType="+adRollValue+"&vwcourseId="+courseId+'&screenwidth='+deviceWidth;     
  ajax.requestFile = url;
  ajax.onCompletion = function(){ responseDataforAdRoll(ajax); };	
  ajax.runAJAX();
}
//
function responseDataforAdRoll(ajax){
  var segment = ajax.response;
  //Thiyagu
  if(ajax.response!=""){
      try {
      __adroll.record_user({"adroll_segments": segment});      
    } catch(err){}  
  }  
}


function cpeODAddToCalendar(collegeid, subOrderItemId, networkId, externalUrl){
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }
  cpeDbStatsLogging(collegeid, subOrderItemId, networkId, 'Add to Calendar', "PROVIDER_OPEN_DAYS_ADD_CALENDAR");
}


function cpeODReservePlace(obj,collegeid, subOrderItemId, networkId, externalUrl){
closeOpenDaydRes();
  if(networkId == null || trimString(networkId).length == 0 || networkId == 'null'){
    networkId = '2';
  }
  if(subOrderItemId == null || trimString(subOrderItemId).length == 0 || subOrderItemId == 'null'){
    subOrderItemId = '0';
  }
  cpeDbStatsLogging(collegeid, subOrderItemId, networkId, externalUrl, "PROVIDER_OD_RESERVE_PLACE");
}
function addOpenDaysForReservePlace(opendate, openyear, collegeId, suborderItemId, qualId){
  var url="/degrees/add-opendays-for-reserveplace.html?collegeId= " + collegeId + "&opendate=" + opendate + "&openyear=" + openyear;
  var ajax=new sack();
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ };	
   ajax.runAJAX();
}

function cpearticlesponsorurl(collegeId, externalUrl){
  cpeDbStatsLogging(collegeId,'','',externalUrl,"ARTICLE_SPONSORSHIP_WEBCLICK");
}

function loadInsightJs()
{
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');  
}

