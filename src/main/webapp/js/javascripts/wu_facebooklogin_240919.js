var homepageurl = window.location.protocol+ "//"+window.location.hostname; 
var loadprocess = "";
var context_path = "/degrees";
var gaLoginFrom = "";
function $$D(docid){ return document.getElementById(docid); }
function blockNone(thisid, value){  if($$D(thisid) !=null){ $$D(thisid).style.display = value;  }  }
function isEmpty(value){  if(trimString(value) ==''){ return true;} else {  return false; } }
function trimString(str){str=this !=window? this : str;return str.replace(/^\s+/g,'').replace(/\s+$/g,'');}
var IEFlag = "Microsoft Internet Explorer";
var version = parseFloat(navigator.appVersion.split("MSIE")[1]);
function showErrorMsg(thisid, errmsg){  if($$D(thisid) !=null){ $$D(thisid).style.display = 'block';  $$D(thisid).innerHTML = errmsg;}  }
var fromLandingPage;
var objects = { 
   errors_div_1    : 'loginerrorMsg',
   errors_div_2    : 'registerErrMsg',
   errors_div_3    : 'forgotErrMsg'
} 
var ErrorMessage = { 
  errors_header                : "<p><strong><i>Please correct the following errors to proceed:</strong></i></p><ul>",
  errors_header_1              : "<ul>",
  errors_footer                : "</ul>",    
  error_login_password         : 'Please enter your password',
  error_firstName              : 'Please enter your first name',
  error_surName                : 'Please enter your last name',
  error_userName               : 'Enter your user name',
  error_login_user_email       : 'Please enter your email and password',
  error_user_email             : 'Please enter your email',
  error_confirm_pass           : 'Please enter your confirm password',
  error_valid_email            : 'Please enter a valid email address',
  error_yoe                    : 'What year would you like to start your new course?',
  error_termsc                 : 'Please agree to our terms and conditions',
  error_pass_mismatch          : 'Your passwords do not match, please try again',
  error_slider                 : 'Please slide to submit',
  error_postcode               : "Hmm, that doesn't seem to be a real postcode.",
  //Added new msgs, Thiyagu G for 03_Nov_2015.
  error_reg_login_password     : "We still don't know your password. Remind us?",
  error_reg_firstName          : "We still don't know your name. Remind us?",
  error_reg_surName            : "We still don't know your last name. Remind us?",
  error_reg_user_email         : 'Something seems to be missing from your email. Awkward.',
  error_reg_confirm_pass       : "We still don't know your confirm password. Remind us?",
  error_reg_valid_email        : 'Something seems to be missing from your email. Awkward.',
  error_reg_yoe                : 'When are you becoming a fresher?',
  error_dob                    : "Happy birthday to you on the.....?",
  error_country                : 'Please select country of residence', 
  error_address1               : 'Please enter Address line 1', 
  error_town                   : 'Please enter town',
  error_dob_sucs               : "Don't expect a birthday card. Soz",
  error_email_sucs             : "You've just got to be on email",
  error_password_length        : "Your password should be 6 characters or more",
  error_confirm_password_length        : "Your confirm password should be 6 characters or more",
  //Added new msgs, Thiyagu G for 08_Aug_2017.
  error_fb_login_reg           : "Please enter registered email id and password",
  error_fb_reg                 : "We couldn't get your details from Facebook. Please fill out the form below"  
}
function AddErrors(errormessage){
   return "<li>"+errormessage+"</li>"
}
function showErrors(divid, message, showheader){
   if(showheader == null){
      message = ErrorMessage.errors_header + message + ErrorMessage.errors_footer;
   }else{
     message = ErrorMessage.errors_header_1 + message + ErrorMessage.errors_footer;
   }  
   $$D(divid).innerHTML = message;   
   blockNone(divid,'block');
}
//CHECKING VALID NUMBER FORMAT 
function checkValidEmail(email){
	if(/^\w+([-+.'']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(trimString(email))){return true}
 return false
}
//THIS CODE RELATED TO FACEBOOK LOGIN
var facebooktype;
varFbURL = "";

function loginInfoFacebook(page, fromPage) {  
  facebooktype = page;
  fromLandingPage = fromPage;
  gaLoginFrom = $$D("regLoggingType").value;//5_AUG_2014
  varFbURL =  $$D("referrerURL_GA").value;//5_AUG_2014
  loadprocess = "YES";
  connectFacebookEnquiry();
 //alert('----after connectFacebookEnquiry----- ');
  FB.login(function(response) {	
    if (response.session) { } else {}
      }, {scope:'email'}); //read_stream
}
//
function connectFacebookEnquiry(){ 
  FB.init({appId: '374120612681083', status: true, cookie: true, xfbml: true}); //TODO Live: 374120612681083 or Test:201887956613152
  FB.Event.subscribe('auth.login', function(response) {	login(); });
  FB.getLoginStatus(function(response) { 
     if (response.session || response.status == 'connected') {
        login(); 		
     }
  });    
}
function login(){fqlQuery();}
function fqlQuery(globalUserIds){    
		 //FB.api('/me', function(response) {     
   FB.api('/me', 'POST', {"fields":"id,email,first_name,last_name"}, function(response) {
 	 var firstname = response.first_name;
   var lastname = response.last_name ;     
   var username = response.username ;
   var email = response.email;   
   var fbUserId = response.id; //Aded FB id 19_May_2015, Thiyagu G.
   var image = "https://graph.facebook.com/"+response.id+"/picture?type=square&return_ssl_results=1";  //large, small, square, normal
   if(email!="" && email!=undefined) {
    socialMediaRegisterProcess(firstname, lastname, username, email, image,'fbreg','fblogin', fbUserId);
   }else{
    if($$D('lbsighuplogin').style.display == 'block'){
      showErrorMsg('fblbsighuplogin_error', ErrorMessage.error_fb_login_reg);
    }else if ($$D('lbregistration').style.display == 'block'){
      showErrorMsg('fblbregistration_error', ErrorMessage.error_fb_reg);
    }else{
      showErrorMsg('fblblogin_error', ErrorMessage.error_fb_login_reg);
    }
      
   }
	  });
}
function fbFriends(){
 FB.api('/v3.0/me/friends?limit=50&offset=0', function(response) {
  obj = JSON.parse(JSON.stringify(response));
  var lengths = obj.data != null ? obj.data.length : 0;
  var globalUserIds = '';
  for(var i=0; i<lengths; i++){
    globalUserIds = globalUserIds + obj.data[i].id + ',';
  }
  globalUserIds = btoa(globalUserIds); //Added Base64 encoding by Prabha on 31_07_2018
  fqlQuery(globalUserIds);
 });
}
function socialMediaRegisterProcess(firstname, lastname, username, email, image,regSubmitType,socialRegType, fbUserId){      
   var pSubmitType = trimString($$D("submitType") ? $$D("submitType").value : "");
   var nonmywu = trimString($$D("nonmywu") ? $$D("nonmywu").value : "");
   var pnotes = trimString($$D("pnotes") ? $$D("pnotes").value : "");
   var downloadurl = trimString($$D("downloadurl") ? $$D("downloadurl").value : "");
   var pdfName = trimString($$D("pdfName") ? $$D("pdfName").value : "");
   var referrerURL_GA = trimString($$D("referrerURL_GA") ? $$D("referrerURL_GA").value : "");
   var regLoggingType = trimString($$D("regLoggingType") ? $$D("regLoggingType").value : "");
   if(($$D("pageIdentifier") && $$D("pageIdentifier").value == "lead-capture")) {
    /*
    if(($$D("middlePageFBRegFlag") && $$D("middlePageFBRegFlag").value == 'Y') || ($$D("middlePageSigninFlag") && $$D("middlePageSigninFlag").value == 'Y')) {
      regLoggingType = 'lead-capture';
    }
    */
    if(fromLandingPage  == 'fb_clearing_landing') {
      regLoggingType = 'lead-capture';
    }
   }
   var pdfId = $$D("pdfId") ? $$D("pdfId").value : "";    
   var vwcid = trimString($$D("vwcid") ? $$D("vwcid").value : "");
   var vwcourseId = trimString($$D("vwcourseId") ? $$D("vwcourseId").value : "");   
   var vwsid = trimString($$D("vwsid") ? $$D("vwsid").value : "");   
   var vwnid = trimString($$D("vwnid") ? $$D("vwnid").value : "");
   var vwprice  = trimString($$D("vwprice") ? $$D("vwprice").value : "");
   var vwurl = trimString($$D("vwurl") ? $$D("vwurl").value : "");
   var vwcname = trimString($$D("vwcname") ? $$D("vwcname").value : "");
   var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
   var url= context_path + "/newuserregistration.html?submittype="+regSubmitType+"&firstName="+firstname+"&surName="+lastname;
       url = url+"&userName="+username+"&socialType="+socialRegType+"&pSubmitType="+pSubmitType+"&pdfId="+pdfId+"&nonmywu="+nonmywu;
       url = url+"&pnotes="+pnotes+"&downloadurl="+downloadurl+"&pdfName="+pdfName+"&referrerURL_GA="+referrerURL_GA+"&gaLoggingType="+regLoggingType+"&submitType="+pSubmitType;
       url = url+"&vwcid="+vwcid+"&vwurl="+vwurl+"&vwcourseId="+vwcourseId+"&vwsid="+vwsid+"&vwnid="+vwnid+"&vwprice="+vwprice+"&vwcname="+vwcname;
       url = url+"&fbUserId="+fbUserId+"&userCMMType="+regLoggingType +'&screenwidth='+deviceWidth;
   if($$D("resUniIds")){
    if($$D("resUniIds").value!=""){
      url = url + "&resUniIds="+$$D("resUniIds").value;
      $$D("resOpenDaysExist").value = "true";    
    }  
   }     
   var ajax=new sack();        
       ajax.requestFile = url;	
       
       ajax.setVar("emailAddress", trimString(email));
       
       ajax.onCompletion = function(){ prepopulate(ajax); };	 
       ajax.runAJAX();
}
//
function prepopulate(ajax){   
  var response = ajax.response;  
  var jq = jQuery.noConflict();
  if(response.indexOf("##SPLIT##")>-1){   
   showResponseMsg(ajax, "loginerrorMsg");
  }else if(($$D("pageIdentifier") && $$D("pageIdentifier").value == "lead-capture") && (fromLandingPage == 'fb_clearing_landing')) {
    $$D('clearingLandingRegDiv').innerHTML = "";
    $$D('clearingLandingRegDiv').innerHTML =response;
  }else{
    var nonAdvParam = $$D("nonAdvLogoFlag") ? $$D("nonAdvLogoFlag").value : "";   
    if($$D("ampPage") && $$D("ampPage").value == "true" && $$D('mbox') && $$D('mbox').style.display == "none"){
      blockNone('newvenueform', 'block');
      sm('newvenue', 650, 560, 'other');	
      dynamicLoadJS(document.getElementById("contextJsPath").value+'/js/'+$$D("commonUserProfileJSName").value); 
      
    }
    $$D('newvenueform').innerHTML = "";    
    $$D('newvenueform').innerHTML =response;  
    $$D("nonAdvLogoFlag").value = nonAdvParam;
    jq('#fbUserId').next('label').addClass("top_lb");
    jq('#firstName').next('label').addClass("top_lb");
    jq('#surName').next('label').addClass("top_lb");
    //Added provider image, 24_Nov_2015, Thiyagu G.
    if(nonAdvParam != ""){
      blockNone('divNonAdvBlock', 'block');
      blockNone('signupwithemail', 'none');
      blockNone('signUpWithBlock', 'none');
      blockNone('divDivider', 'block');
      blockNone('pdfSignup', 'block');    
      blockNone('normalsignup', 'none');
      var addpm = nonAdvParam.split("#");
      if(addpm[7]!=""){
        $$D('nonAdvUniLogo').src=addpm[7];
        blockNone('nonAdvUniLogo', 'block');        
      }else{        
        blockNone('nonAdvUniLogo', 'none');        
      }
      $$D('addNonAdvDiv').className = "reg-frmt bgw rqinf_cnt";      
    }else{
      blockNone('divNonAdvBlock', 'none');
      blockNone('signupwithemail', 'block');
      blockNone('signUpWithBlock', 'block');
      blockNone('divDivider', 'none');
      blockNone('pdfSignup', 'none');      
      blockNone('normalsignup', 'block');
      $$D('addNonAdvDiv').className = "reg-frmt bgw";
    }    
    //Added provider image, 24_Nov_2015, by Thiyagu G
    //show user exists err msg, Thiyagu G for 03_Nov_2015.
    if($$D("alreadyRegistered")){
      $$D("alreadyRegistered").innerHTML = "This email address is not registered with us, please register and try again";      
      blockNone('alreadyRegistered', 'block');
    }  
  }
  //YOE mouseover for desktop, By Thiyagu G 27_Dec_2015 Start.
  var eventFlag;
  if($$D("isMobileUA").value=="No"){
    eventFlag="mouseover";
  }else{
    eventFlag="click";
  }
  if($$D("yeartoJoinCourse1")){$$D("yeartoJoinCourse1").addEventListener(eventFlag, function( event ) {
     blockNone('yeartoJoinCourse1YText', 'block');
  }, false);}
  if($$D("yeartoJoinCourse2")){$$D("yeartoJoinCourse2").addEventListener(eventFlag, function( event ) {
     blockNone('yeartoJoinCourse2YText', 'block');
  }, false);}
  if($$D("yeartoJoinCourse3")){$$D("yeartoJoinCourse3").addEventListener(eventFlag, function( event ) {
     blockNone('yeartoJoinCourse3YText', 'block');
  }, false);}
  if($$D("yeartoJoinCourse4")){$$D("yeartoJoinCourse4").addEventListener(eventFlag, function( event ) {
     blockNone('yeartoJoinCourse4YText', 'block');
  }, false);}
  //YOE mouseover for desktop, By Thiyagu G 27_Dec_2015 End.
  //alignment & get grades from cookie and show, By Thiyagu G for 03_Nov_2015.
  if($$D("lbDivNew")){
    $$D("lbDivNew").className = "comLgh rlbox";
  }  
}
/*END OF FACEBOOK LOGIN*/
function validateUserLogin(){  
  loginErrorDisplayFlag();
  var message = true;
  var email =  trimString( $$("loginemail").value );
  var password = trimString( $$("loginpass").value );
  var pSubmitType = trimString( $$("submitType").value );
  var pdfId = $$D("pdfId").value;  
  var vwcid =  $$D("vwcid").value;
  var vwcourseId =  $$D("vwcourseId").value;
  var vwurl =  $$D("vwurl").value;
  var surveyUrlUserId =  "";
  if($$D("surveyUrlUserId")){
   surveyUrlUserId =  $$D("surveyUrlUserId").value;
  }  
  var rememberMe;
  if($$("loginRemember").checked == true){
    rememberMe = "Y";
  }else{
    rememberMe = "N";
  }  
  if(isEmpty(email) || email == 'Enter email address'){ 
    message = false; 
    showErrorMsg('loginemail_error', ErrorMessage.error_user_email);    
  } else if(!checkValidEmail(email)){ message = false; showErrorMsg('loginemail_error', ErrorMessage.error_valid_email); }
  if(isEmpty(password) || password == 'Enter password'){ message = false; showErrorMsg('loginpass_error', ErrorMessage.error_login_password); }  
  if(!message){      
    message = true;
  }else {
  var url= context_path + "/newuserregistration.html?submittype=login&pSubmitType=" + pSubmitType + "&pdfId=" + pdfId + "&vwcid=" + vwcid + "&vwurl=" + vwurl+"&vwcourseId="+vwcourseId+"&surveyUrlUserId="+surveyUrlUserId;
  if(($$D("pageIdentifier") && $$D("pageIdentifier").value == "lead-capture") && ($$D("middlePageSigninFlag") && $$D("middlePageSigninFlag").value == 'Y')) {
    url = url + "&userCMMType=lead-capture";
  }
  if($$D("resUniIds")){
   if($$D("resUniIds").value!=""){  
    url = url + "&resUniIds="+$$D("resUniIds").value;
    $$D("resOpenDaysExist").value = "true";    
   } 
  }
  blockNone('loadinggif', 'block');
    var ajax=new sack();
    ajax.requestFile = url;	
    ajax.setVar("emailAddress", trimString(email));
			 ajax.setVar("password", encodeURIComponent(password));
    ajax.setVar("rememberMe", rememberMe);
			// ajaxObj.setVar("rememberMe", $$D("rememberMe").checked);
    ajax.onCompletion = function(){ showResponseMsg(ajax, "loginerrorMsg"); };	
    ajax.runAJAX();
   return false;
    }
  }
function newSpamBoxReg(){
  var message = true;
  var firstName = trimString($$D("spmFirstName").value );   
  var surName = trimString($$D("spmSurName").value );    
  var emailAddress =  trimString($$D("spmEmailAddress").value );
  var checkSpamBox = $$D("spamBox").value;
  var yoe = '';  
  
  var yearOfEntry = document.getElementsByName('yeartoJoinCourse');
  for (i = 0; i < yearOfEntry.length; i++) {
    if(yearOfEntry[i].checked) {
      yoe = yearOfEntry[i].value;			
    }
  }  
  if(isEmpty(firstName) || firstName == 'First name*') { 
    message = false; showErrorMsg('firstName_spmerror',ErrorMessage.error_firstName); 
  }else{
    blockNone('firstName_spmerror', 'none');
  }
  if(isEmpty(surName) || surName == 'Last name*') {
    message = false; showErrorMsg('surName_spmerror', ErrorMessage.error_surName); 
  }else{
    blockNone('surName_spmerror', 'none');
  }    
  if(isEmpty(emailAddress) || emailAddress == 'Email address'){ 
    message = false; showErrorMsg('emailAddress_spmerror', ErrorMessage.error_user_email);
  } else if(!checkValidEmail(trimString(emailAddress))){ 
    message = false; showErrorMsg('emailAddress_spmerror', ErrorMessage.error_valid_email); 
  }else{
    blockNone('emailAddress_spmerror', 'none');
  }
  if(isEmpty(yoe)){ 
    message = false; showErrorMsg('yoe_spmerror',ErrorMessage.error_yoe); 
  }else{
    blockNone('yoe_spmerror', 'none');
  }
  if($$D("spamBox").checked == false){
    message = false; showErrorMsg('sBoxChkErr', ErrorMessage.error_termsc); 
  }else{
    blockNone('sBoxChkErr', 'none');
  }
  if(!message){      
    message = true;
  }else {    
    var url = context_path + "/newuserregistration.html?submittype=spamreg&firstName="+ firstName + "&surName=" + surName + "&yoe=" +yoe;
    blockNone('loadinggif', 'block');
    var ajax=new sack();
        ajax.requestFile = url;	
        
    ajax.setVar("emailAddress", trimString(emailAddress));
	         
        ajax.onCompletion = function(){ showResponseMsgSpam(ajax); };	
        ajax.runAJAX();
   return false;
  }    
}
function spamBoxReg(){
  $$D("msgParam").innerHTML="";
  var message = true;
  var email =  trimString( $$("emailAdd").value );
  if(isEmpty(email) || email == 'Enter your email address'){ 
    message = false; 
    showErrorMsg('msgParam', ErrorMessage.error_user_email);  
    $$D('msgParam').className = 'err';
    return false;
  } else if(!checkValidEmail(email)){
    message = false; showErrorMsg('msgParam', ErrorMessage.error_valid_email);
    $$D('msgParam').className = 'err';
    return false;
  }

  if(!message){      
    message = true;
  }else {
    var url= context_path + "/newuserregistration.html?submittype=spamreg&emailAddress="+trimString(email);    
    blockNone('loadinggif', 'block');
    var ajax=new sack();
        ajax.requestFile = url;	
         ajax.setVar("emailAddress", trimString(email));
        ajax.onCompletion = function(){ showResponseMsgSpam(ajax); };	
        ajax.runAJAX();
   return false;
  }
}
function showResponseMsgSpam(ajax){
  var response = ajax.response;
  if(response.trim() == "Already Subscribed"){
    $$D("msgParam").innerHTML="You have already subscribed.";    
    blockNone('loadinggif', 'none');
    $$D('msgParam').className = 'err';
  }else if(response.trim() == "Successfully Subscribed"){
    GARegistrationLogging('register', 'spam-box', 'spam-box' );
    $$D("msgParam").innerHTML="Thanks, you have subscribed successfully!";
    $$D('msgParam').className = '';    
    blockNone('loadinggif', 'none');
  }else if(response.trim() == "Your Account has been locked"){
    $$D("msgParam").innerHTML="Your Account has been locked.";    
    blockNone('loadinggif', 'none');
    $$D('msgParam').className = 'err';
  }
}
function loginErrorDisplayFlag(){
  blockNone('loginemail_error', 'none');
  blockNone('loginpass_error', 'none');
  blockNone('loginerrorMsg', 'none');  
}
function registerErrorDisplayFlag(){
  blockNone('firstName_error', 'none');
  blockNone('surName_error', 'none');
  blockNone('userName_error', 'none');
  blockNone('emailAddress_error', 'none');
  blockNone('registerErrMsg', 'none');
  blockNone('password_error', 'none');  
  blockNone('termsc_error', 'none');
  blockNone('registerErrMsg', 'none');
  blockNone('yoe_error', 'none');
}
function showResponseMsg(ajax, divid){

  if($$D("ampPage") && $$D("ampPage").value == "true"){
    gaLoginFrom = "";//$$D("regLoggingType") ? $$D("regLoggingType").value : "";
    varFbURL = "";//$$D("referrerURL_GA") ? $$D("referrerURL_GA").value : "";
  }
  var vwcid = $$D("vwcid") ? $$D("vwcid").value : "";
  var responseArr = new Array(); 
  responseArr = ajax.response.split("##SPLIT##"); 
  if(responseArr[0] != "Blocked"){
  var loggedInFrom = ""; 
   if($$D("regLoggingType")){loggedInFrom = $$D("regLoggingType").value;}
  if($$D("loginGradsSave") && $$D("loginGradsSave").value=='YES'){    
    if(divid == 'registerErrMsg'){//5_AUG_2014
     var gradeFilter = $$D("GAgradeFilter").value;     
     GARegistrationLogging('register', 'grade-filter', gradeFilter);
    }else if(divid == 'loginerrorMsg'){
      if(("fblogin" == facebooktype || "gpluslogin" == facebooktype) && responseArr[1] == "Y" ){
        var gradeFilter = $$D("GAgradeFilter").value; 
        GARegistrationLogging('register', 'grade-filter', gradeFilter);    
      }    
    }    
    entryFilterSubmit(responseArr[2]);
    return false;
  }
  if(responseArr[0] == "YES"){
     if($$D("downloadurl") && $$D("downloadurl").value!="" && $$D("downloadurl").value!=null){
     var urlToPdfFile = $$D("downloadurl").value;
     var pdfName =  $$D("pdfName").value;
     searchEventTracking('pdf','Download', pdfName);
     var pdfId = $$D("pdfId").value;
     $$D('pdfHidden'+pdfId).href = urlToPdfFile;
     $$D('pdfHidden'+pdfId).click();    
  }  
 if(divid == 'loginerrorMsg'){
  if("fblogin" == facebooktype || "gpluslogin" == facebooktype){ //Added condition for insights log (google plus sign in) by Prabha on 29_Mar_2016 
    if(responseArr[2] == "Y"){
      if(gaLoginFrom == 'add-open-days'){
        var providerName = $$D("openDayProviderName").value; 
        GARegistrationLogging('register', gaLoginFrom, providerName);
      }else if(gaLoginFrom == 'view-comparison'){               
        GARegistrationLogging('register', 'Final 5', gaLoginFrom);
      }else{ 
        if(gaLoginFrom == 'whatuni-go'){
          GARegistrationLogging('register', gaLoginFrom, varFbURL);        
        }
      }
    }
  }      
  if(loggedInFrom == 'add-open-days'){
   callOpenDays('N');//openday registration param added by Prabha :: 27_jan_2016
  }  
  if("fblogin" == facebooktype || "gpluslogin" == facebooktype){ //Added condition for insights log (google plus sign in) by Prabha on 29_Mar_2016 
    //Insights account changed and removed sampling for 08_MAR_2016, By Thiyagu G.
    ga('create', 'UA-52581773-4', 'auto', {'name': 'newTracker'});
    ga('newTracker.set', 'contentGroup1', 'whatuni');
    ga('newTracker.set', 'dimension1', 'user login');
    ga('newTracker.set', 'dimension2', 'whatuni');
    ga('newTracker.set', 'dimension15', responseArr[1]);              
    ga('newTracker.send', 'pageview');        
  }else{
    ga('newTracker.set', 'contentGroup1', 'whatuni');
    ga('newTracker.set', 'dimension1', 'user login');
    ga('newTracker.set', 'dimension2', 'whatuni');
    if(responseArr[2]!=null && responseArr[2]!=undefined) {
      ga('newTracker.set', 'dimension15', responseArr[2]);  
    }
    if(responseArr[2]!=null && responseArr[3]!=undefined) {
      ga('newTracker.set', 'dimension19', responseArr[3]);  
    }  
    ga('newTracker.send', 'pageview');        
  }
 } 
 if(divid == 'registerErrMsg'){
  if($$D("regLoggingType")){
    if($$D("regLoggingType").value != ""){
      var loginFrom = $$D("regLoggingType").value; 
      var referrerURL_GA = $$D("referrerURL_GA").value;     
      if($$D("ampPage") && $$D("ampPage").value == "true"){
        loginFrom = "AMP page";
      }
      
      if(loginFrom == 'add-open-days'){
        var providerName = $$D("openDaySProviderName").value;         
        GARegistrationLogging('register', loginFrom, providerName);
         callOpenDays('Y');//openday registration param added by Prabha :: 27_jan_2016     
      }else if(loginFrom == 'view-comparision'){
        GARegistrationLogging('register', 'Final 5', loginFrom);
      }else{
        if($$D("resUniIds")){
          if($$D("resUniIds").value!=""){
            GARegistrationLogging('register','clicked' ,'opendayslightbox');
          }
        }else{
          //Added pdf download & logging, 24_Nov_2015, Thiyagu G.
          if(loginFrom == 'non-request-info'){
            var addpm = $$D('nonAdvLogoFlag').value.split("#");
            GARegistrationLogging('register', 'non-advertiser-pdf', addpm[0], $$D('loggedInUserId').value);            
          }else{
            if(loginFrom == 'view-comparison'){
              loginFrom = 'Final 5';
            }
            if(loginFrom != 'whatuni-go'){
              GARegistrationLogging('register', loginFrom, referrerURL_GA);
            }
          }
        }  
      }
    }
  }  
  ga('newTracker.set', 'dimension1', 'user register');
  ga('newTracker.set', 'dimension2', 'whatuni');
  if("fblogin" == facebooktype || "gpluslogin" == facebooktype){ //Fix the insight bug by Prabha on 29_Mar_2016
    ga('newTracker.set', 'dimension15', responseArr[1]);  
  }else{
  ga('newTracker.set', 'dimension15', responseArr[2]);  
  ga('newTracker.set', 'dimension19', responseArr[4]);  
  }
  ga('newTracker.send', 'pageview');
 } 
 
if((vwcid==null || vwcid=="" ) && (loggedInFrom!="add-open-days") ){ //3_FEB_2015
  if($$D("loginFrom") != null && $$D("loginFrom").value == "Y" && (loggedInFrom!="non-request-info")){
    if( $$D("resOpenDaysExist")){
      if($$D("resOpenDaysExist").value=="true"){
         document.location = $$D("fullOpenDayUrlwithparam").value;
       }else{          
         document.location= homepageurl;
        }
      }else if($$D("ampPage") && $$D("ampPage").value == "true"){
        document.location = $$D("ampPageUrl").value;
      }else if($$D("pageIdentifier") && $$D("pageIdentifier").value == "lead-capture"){
        if(fromLandingPage == 'fb_clearing_landing') {
          GAInteractionEventTracking('signupbutton', 'Lead Capture', 'Register', 'Sign Up');
          GAInteractionEventTracking('leadcapturebutton', 'register', 'WUGO Lead Capture', 'Clicked');
          fbq('track', 'CompleteRegistration'); // FB tracking
          document.location = "/success/a-level-btec-exam-survival-guide";
        } else {
          document.location = "/a-level-btec-exam-survival-guide";
        }
      }else{
         document.location= homepageurl;
      }
    }else if($$D("loginFrom") != null && $$D("loginFrom").value == "Y" && (loggedInFrom=="non-request-info")){ //Added else if condition to open the success lightbox and download the pdf for 24_Nov_2015, By Thiyagu G.
      var addpm = $$D('nonAdvLogoFlag').value.split("#");
      var referrerURLGA = $$D("referrerURL_GA").value;      
      showLightBoxLoginForm('pdf-success-popup', 664, 500, 'Y','','pdf-success-reg');
      var pdfUrl = $$D("contextPath").value+"/get-pdf?from="+addpm[8]+"&collegeId="+addpm[2]+"&courseId="+addpm[6];      
      window.open(pdfUrl, "_blank", "", "");
      var fullPath = $$D("domainSpecPath").value+pdfUrl;
      nonAdvertiasorPdfDownloadClick(this, addpm[2], addpm[6], addpm[3], addpm[4], "");
      GAInteractionEventTracking('emailwebform', 'NonAdvertiserPdf', 'Email-Submit', addpm[0], addpm[1]);
      
      ga('create', 'UA-52581773-4', 'auto', {'name': 'newTracker'});
      ga('newTracker.set', 'contentGroup1', 'whatuni');
      ga('newTracker.set', 'dimension1', 'free pdf download submitted');
      ga('newTracker.set', 'dimension2', 'whatuni');
      ga('newTracker.set', 'dimension3', addpm[9]);
      ga('newTracker.send', 'pageview');        
    
      $$D("pdfFromPageURL").value = referrerURLGA;
      //document.location= referrerURLGA;
    }else if($$D("loginFrom") != null && $$D("loginFrom").value == "PR"){
      document.location="/degrees/mywhatuni.html?tabname=PR";
    }else if($$D("loginFrom") != null && ($$D("loginFrom").value == "iwanttobe" || $$D("loginFrom").value == "whatcanido")){
      $$D('ulsearchsubmit').click();
      return false;
    }else if($$D("loginFrom") != null && $$D("loginFrom").value == "nonLoggedTimeline"){      
        document.location= homepageurl;      
    }else if($$D("loginFrom") != null && $$D("loginFrom").value == "view-comparison"){  
        document.location="/degrees/comparison";
    }else if($$D("loginFrom") != null && $$D("loginFrom").value == "final-choice"){  
        document.location="/degrees/comparison";
    }else{
      if($$D("nonmywu")){
        var nonmywu = $$D("nonmywu").value;
        if(nonmywu != null && nonmywu != "" && nonmywu == "NONMYWU"){
          var pnotes = $$D("pnotes").value;  
          var loctionURL = document.location;
          document.location.href = "/degrees/nonloggedmywu.html?nonmywu="+nonmywu+"&pnotes="+escape(pnotes);
        }else{
          //document.location.reload(true);
          var tempURL = window.location.href;
			       //tempURL = tempURL.replace("http://mtest.whatuni.com","");
      if($$D("ampPage") && $$D("ampPage").value == "true"){
        document.location = $$D("ampPageUrl").value;
      }else{
          document.location = tempURL;
          }
        }
      }else{
        if(divid == 'registerErrMsg'){
          if($$D("regLoggingType") && $$D("regLoggingType").value == "whatuni-go"){
            var collegeName = $$D('collegeName') ? $$D('collegeName').value : "";
            collegeName = (collegeName!="") ? collegeName.trim() : "";
            gaEventLog('Apply Now Form', 'Register', 'Sign Up');
            gaEventLog('WUGO Journey', 'Sign Up', collegeName);
            gaEventLog('register', 'WUGO Application', collegeName);
            document.location="/degrees/clearing-match-maker-tool.html?action-param=provisional_form&action=prov_qual&courseId="+$$D("courseId").value+"&opportunityId="+$$D("opportunityId").value;
            //$$D("wuGoFormBean").submit();
          }else{
            document.location="/degrees/mywhatuni.html";
          }
	    } else if(divid == 'ebookEmailAddress_error'){
          //
          GAInteractionEventTracking('signupbutton', 'Lead Capture', 'Register', 'Sign Up');
          GAInteractionEventTracking('leadcapturebutton', 'register', 'WUGO Lead Capture', 'Clicked');
          fbq('track', 'CompleteRegistration'); // FB tracking
          //
          document.location="/success/a-level-btec-exam-survival-guide";
        }else{
          //document.location.reload(true);
          var tempURL = window.location.href;
			       //tempURL = tempURL.replace("http://mtest.whatuni.com","");
          if($$D("ampPage") && $$D("ampPage").value == "true"){
            document.location = $$D("ampPageUrl").value;
          } else if($$D("pageIdentifier") && $$D("pageIdentifier").value == "lead-capture"){
            //
            GAInteractionEventTracking('signinbutton', 'Lead Capture', 'Register', 'Sign In');
            //
            document.location = "/success/a-level-btec-exam-survival-guide";
		  } else{
            if($$D("regLoggingType") && $$D("regLoggingType").value == "whatuni-go"){
              var collegeName = $$D('collegeName') ? $$D('collegeName').value : "";
              gaEventLog('Apply Now Form', 'Register', 'Sign In');
              gaEventLog('WUGO Journey', 'Sign In', collegeName);
              document.location="/degrees/clearing-match-maker-tool.html?action-param=provisional_form&action=prov_qual&courseId="+$$D("courseId").value+"&opportunityId="+$$D("opportunityId").value;
             //$$D("wuGoFormBean").submit();
            }else{
              document.location = tempURL;
            }
          }
        }
      }
    }
  }
   if(vwcid!=null && vwcid!=""){
    vistwebsitelogging();
    setTimeout(function(){moretext()},8000); 
    return false;   
  }  
  }else {    
     if("fbsignup" == facebooktype){divid="registerErrMsg";};
     $$D(''+divid).innerHTML=ajax.response;    
     blockNone('loadinggif', 'none');
     if(($$D("pageIdentifier") && $$D("pageIdentifier").value == "lead-capture") && ($$D("middlePageSigninFlag") && $$D("middlePageSigninFlag").value == 'Y')) {
       $$D("middlePageSigninFlag").value = 'N';
     }
     if($$D("loadinggifreg")){blockNone('loadinggifreg', 'none');}     
     blockNone(divid, 'block');
     if($$D("regLoggingType") && $$D("regLoggingType").value == "whatuni-go"){
       if('registerErrMsg'== divid){
         //setErrorAndSuccessMsg('email','frm-ele faild','loginemail_error', '');
         //setErrorAndSuccessMsg('password','frm-ele faild','loginpass_error', '');
         scrollToRegDiv('wugo_reg');
       }
       if('loginerrorMsg'== divid){
         //setErrorAndSuccessMsg('email','frm-ele faild','loginemail_error', '');
         //setErrorAndSuccessMsg('password','frm-ele faild','loginpass_error', ''); 
       }
     }
  }
 } else{
     showLightBoxLoginForm('blockedUser',650,500, 'resetPassword');
 }
}
//New field's validation and store, Thiyagu G for 03_Nov_2015.
dev(document).ready(function(){
  dev("#marketingEmailFlag").click(function(){
    if(dev("#marketingEmailFlag").is(':checked')) {
      dev("input[id='marketingEmailFlag']").val("Y");
    } else { dev("input[id='marketingEmailFlag']").val("N"); }                  
  });   
  dev("#solusEmailFlag").click(function(){
    if(dev("#solusEmailFlag").is(':checked')) {
      dev("input[id='solusEmailFlag']").val("Y");
    } else { dev("input[id='solusEmailFlag']").val("N"); }                  
  });   
  dev("#surveyEmailFlag").click(function(){
    if(dev("#surveyEmailFlag").is(':checked')) {
      dev("input[id='surveyEmailFlag']").val("Y");
    } else { dev("input[id='surveyEmailFlag']").val("N"); }                  
  });
});
function setRegFlags(id){
  if(dev("#"+id).is(':checked')) {
    dev("input[id='"+id+"']").val("Y");
  } else { dev("input[id='"+id+"']").val("N"); }
}
function validateUserRegister(){  
  registerErrorDisplayFlag();
  var message = true;
  var fbUserId;
  var newsLetter = null;
  var firstName = encodeURIComponent(trimString($$D("firstName").value));   
  var surName = encodeURIComponent(trimString($$D("surName").value));    
  var emailAddress =  trimString($$D("emailAddress").value);
  if($$D("newsLetter")){newsLetter = trimString($$D("newsLetter").value);}
  var termsc =  trimString($$D("termsc").value);
  var password = encodeURIComponent(trimString($$D("password").value));  
 // var elDate = $$D("dateDOB");
//  var textDate = elDate.options[elDate.selectedIndex].text;
 // var elMonth = $$D("monthDOB");    
//  var textMonth = elMonth.options[elMonth.selectedIndex].text;
//  var elYear = $$D("yearDOB");
 // var textYear = elYear.options[elYear.selectedIndex].text;        
  var datemsg = "";  
//  var inputDate = textDate+"/"+elMonth.selectedIndex+"/"+textYear;            
  var postcode = $$D("postcodeIndex") ? $$D("postcodeIndex").value : "";  
  var regType = $$D("regLoggingType").value;
  var submittedFrom = "";
  var vwcid = trimString( $$D("vwcid").value );
  var prepopulatepassword = ""; 
  var marketingEmailRegFlag = ($$D("marketingEmailRegFlag") && $$D("marketingEmailRegFlag").value != "")? $$D("marketingEmailRegFlag").value : "N";
  var solusEmailRegFlag = ($$D("solusEmailRegFlag") && $$D("solusEmailRegFlag").value != "") ? $$D("solusEmailRegFlag").value : "N";
  var surveyEmailRegFlag = ($$D("surveyEmailRegFlag") && $$D("surveyEmailRegFlag").value != "") ? $$D("surveyEmailRegFlag").value : "N";
  if($$D("fbuserregister")){submittedFrom = trimString( $$D("fbuserregister").value );}
  //Added FB user id to store and generate FB image path for 19_May_2015, by Thiyagu G.
  if(submittedFrom != ""){fbUserId =  trimString($$D("fbUserId").value);}
  if($$D("prepopualtepassword")){prepopulatepassword = trimString( $$D("prepopualtepassword").value );}  
  var pSubmitType = trimString( $$D("submitType").value );  
  var yoe = '';
  for (i=0; i<document.getElementsByTagName('input').length; i++) {
    if (document.getElementsByTagName('input')[i].type == 'radio'){
      if(document.getElementsByTagName('input')[i].checked == true){
       yoe = document.getElementsByTagName('input')[i].value;
       break;
      }
    } 
  }
  var pdfId = $$D("pdfId").value;
  var rememberMe; 
  var failMessage = ' qlerr';
  var successMessage = ' sumsg';
  
  var redirectURL = "registerErrMsg";
    if(regType == 'lead-capture') {
      redirectURL = "ebookEmailAddress_error";
    }
    
  if(regType == 'lead-capture') {
    failMessage = ' faild';
    successMessage = ' sucs';
  }
  if(isEmpty(firstName) || firstName == 'First name*'){ message = false;setErrorAndSuccessMsg('firstName_fieldset',$$D('firstName_fieldset').className +  failMessage,'firstName_error',ErrorMessage.error_reg_firstName);}
  else{setErrorAndSuccessMsg('firstName_fieldset',$$D('firstName_fieldset').className + successMessage,'firstName_error', 'Nice to meet you! Great name!');$$D('firstName_error').className="sutxt";}
  if(isEmpty(surName) || surName == 'Last name*'){ message = false; setErrorAndSuccessMsg('surName_fieldset',$$D('surName_fieldset').className + failMessage,'surName_error', ErrorMessage.error_reg_surName); }
  if(emailAddress==''){
    message = false;
    setErrorAndSuccessMsg('emailAddress_fieldset',$$D('emailAddress_fieldset').className + failMessage, redirectURL, ErrorMessage.error_valid_email);
  }else if(!checkValidEmail(emailAddress)){
    message = false;
    setErrorAndSuccessMsg('emailAddress_fieldset',$$D('emailAddress_fieldset').className + failMessage, redirectURL, ErrorMessage.error_reg_user_email);
  }
  //checkEmailExist("emailAddress");
  if($$D("emailValidateFlag").value=="false"){      
    message = false;    
  }
  //Modified by Indumathi.S for showing success message for password Nov-03-15
  if(prepopulatepassword==''){if(isEmpty(password) || password == 'Password*'){ message = false; setErrorAndSuccessMsg('password_fieldset',$$D('password_fieldset').className + failMessage,'password_error', ErrorMessage.error_reg_login_password);  
  } else if(!isEmpty(password) && password.length < 6){
    message = false; setErrorAndSuccessMsg('password_fieldset',$$D('password_fieldset').className + failMessage,'password_error', ErrorMessage.error_password_length);
  } else{setErrorAndSuccessMsg('password_fieldset',$$D('password_fieldset').className + successMessage,'password_error','');}}  
  var yoe = '';
  for (i=0; i<document.getElementsByTagName('input').length; i++) {
    if (document.getElementsByTagName('input')[i].type == 'radio'){
      if(document.getElementsByTagName('input')[i].checked == true){
       yoe = document.getElementsByTagName('input')[i].value;
       break;
      }
    } 
  }  
     
  if($$D("termsc") == null || $$D("termsc").checked == false){ message = false; showErrorMsg('termsc_error',ErrorMessage.error_termsc); }  
  if($$D("newsLetter") != null && $$D("newsLetter").checked == false){newsLetter="N";}
  if(isEmpty(yoe)){ message = false; setErrorAndSuccessMsg('yoe_fieldset',$$D('yoe_fieldset').className + failMessage,'yoe_error',ErrorMessage.error_reg_yoe); }
  if(!message){ 
    if(($$D("mbox").style.display == "none") && ($$D("pageIdentifier") && $$D("pageIdentifier").value == "lead-capture") && ($$D("regForm"))) {
      //focusElement("ebook-reg-pod", "userCountryOfResidence");
      var searchEles = dev('#regForm fieldset');
      for(var i = 0; i < searchEles.length; i++) {
        if(dev(searchEles[i]).hasClass('faild')) {  
          var elemTop = dev(searchEles[i]).offset();
          dev('html, body').animate({ scrollTop: elemTop.top - 100 }, "slow");
          break;
        }
      }
    } else {
      focusElement("pros-pod", "userCountryOfResidence");
    }
    message = true;
  }else {    
    var url = "";
    if(submittedFrom==''){
      url= context_path+"/newuserregistration.html?submittype=registration&firstName="+firstName+"&surName="+surName;
      url = url+"&newsLetter="+newsLetter+"&pSubmitType="+pSubmitType+"&pdfId="+pdfId+"&yoe="+yoe+"&vwcid="+vwcid;
      url = url+"&postcode="+postcode+"&userCMMType="+ regType+"&marketingEmailFlag="+marketingEmailRegFlag;
      url = url+"&solusEmailFlag="+solusEmailRegFlag+"&surveyEmailFlag="+surveyEmailRegFlag;
    }else{      
      var socialRegType = "";
      if($$D("socialRegType")){
        socialRegType = $$D("socialRegType").value;
      }
      url= context_path+"/newuserregistration.html?submittype=fbreg&firstName="+firstName+"&surName="+surName+"&newsLetter="+newsLetter;
      url = url+"&pSubmitType="+pSubmitType+"&pdfId="+pdfId+"&yoe="+yoe+"&lightboxSubmit=Y&socialType="+socialRegType+"&vwcid="+vwcid+"&fbUserId="+fbUserId;
      url = url+"&postcode="+postcode+"&userCMMType="+ regType+"&marketingEmailFlag="+marketingEmailRegFlag;
      url = url+"&solusEmailFlag="+solusEmailRegFlag+"&surveyEmailFlag="+surveyEmailRegFlag;      
    }
    $$("loadinggifreg").style.display="block";
    if($$D("resUniIds")){
      if($$D("resUniIds").value!=""){
        url = url + "&resUniIds="+$$D("resUniIds").value;
        $$D("resOpenDaysExist").value = "true";    
      }  
    }
    url = url + "&screenwidth="+$$D('screenwidth').value;
    var ajax=new sack();
    ajax.requestFile = url;	
    
    ajax.setVar("emailAddress", encodeURIComponent(trimString(emailAddress)));
    ajax.setVar("password", encodeURIComponent(password));
    ajax.setVar("rememberMe", rememberMe);
    
    ajax.onCompletion = function(){ showResponseMsg(ajax, redirectURL); };	
    ajax.runAJAX();                
    return false;
  }
}  
//
function showForgotDiv(){
 blockNone('forgotdiv', 'block');
}
//
function changePassword(passworddisplay, passwordhide, hidepwdId, currentId){  
 if( $$D(hidepwdId).value.length<=0){
   if(currentId=='textpwd' || currentId=='regtextpwd' || currentId=='confirmtextpwd'){     
     $$D(passwordhide).style.display="block";
     $$D(passwordhide).style.color = "#333";
     $$D(passworddisplay).style.display="none";     
     $$D(hidepwdId).focus();
   } else {     
     $$D(passwordhide).style.display="none";
     $$D(passworddisplay).style.display="block";     
   }   
  }else{    
    $$D(passwordhide).style.display="block";    
    $$D(passwordhide).style.color = "#333";
    $$D(passworddisplay).style.display="none";
  }  
}
function clearAutoEmail(){
  if($$D("autoEmailIdLogin")){    
    blockNone("autoEmailIdLogin", 'none');
  }  
}
function showBasicTermsPopup(){
   var TCversion = $$('TCVersion') ? $$('TCVersion').value : '/degrees/help/termsAndCondition.htm';
   window.open(TCversion,"termspopup","width=650,height=400,scrollbars=yes,resizable=yes"); return false;
}

function clearDefaultLogin(obj, text){  
  if($$D(obj).value == text){
    $$D(obj).value = "";
	   $$D(obj).style.color = "#333";        
  }
}
function setDefaultLogin(obj, text){  
  if($$D(obj).value == ""){
    $$D(obj).value = text;
	   $$D(obj).style.color = "#888";     
  }
}

function logStatsForPDFDownlaod(pdfid, userid){
  var url= context_path + "/statsajaxpdfdownload.html?pdfid="+pdfid+"&userid="+ userid;
  var ajaxObj = new sack();
      ajaxObj.requestFile = url;	
      ajaxObj.onCompletion = function(){showRespStatsPDFDownlaod(ajaxObj); };	
      ajaxObj.runAJAX();        
}
function showRespStatsPDFDownlaod(ajaxObj){
   var response = ajaxObj.response;   
}
//
function moretext(){
 var vwprice =  $$D("vwprice").value;
 var vwurl =  $$D("vwurl").value;
 var vwcname  =  $$D("vwcname").value;
 var vwsid  =  $$D("vwsid").value;
 GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', vwcname, vwprice);
 var hrefurl = $$D('vwHidden'+vwsid); 
 var newWin = window.open(hrefurl,'_blank'); 
 if(!newWin || newWin.closed || typeof newWin.closed=='undefined')
  { 
    showLightBoxLoginForm('visitwebsitePopupblocked',650,500, 'Y','','visitwebsitePopupblocked', vwsid);    
  }else{   
   document.location="/degrees/mywhatuni.html";
  }  
 return true;
}
function vistwebsitelogging(){  
  var vwcourseId =  $$D("vwcourseId").value;
  var vwcid =  $$D("vwcid").value;
  var vwsid =  $$D("vwsid").value;
  var vwnid =  $$D("vwnid").value;
  var vwurl =   $$D('vwHidden'+vwsid).href;
  var vwprice =  $$D("vwprice").value;
  if(vwcourseId!="" && vwcourseId!= null){    
    visitWebSiteForceSave(vwcourseId,vwcid, vwsid, vwnid, vwurl);
  }
}
function closeForceRegLB(){
  var vwcid =  $$D("vwcid").value;
  var vwsid =  $$D("vwsid").value;
  var vwnid =  $$D("vwnid").value;
  var vwurl =  $$D('vwHidden'+vwsid).href;
  var vwprice =  $$D("vwprice").value;
  var vwcname =  $$D("vwcname").value;
  GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick', vwcname, vwprice);
  hm();
  $$D('vwHidden'+vwsid).click();
  cpeWebClick('',vwcid, vwsid, vwnid, vwurl);
}
function closeForceRegPopupLB(vwsid){
  var vwurl =  $$D('vwHidden'+vwsid).href;  
  hm();
  $$D('vwHidden'+vwsid).click();  
  document.location="/degrees/mywhatuni.html";
}
var $jq = jQuery.noConflict();
function checkPassword(){
  var password = trimString($$D("password").value );     
  if(!isEmpty(password)){
    if($$D("bgSlider").className != "sucess"){
      $jq("#Slider").draggable('enable');
    }
  }else{
    $jq("#Slider").draggable('disable');
  }
}

//
function scrollToRegDiv(scrDiv) {
  var frmId = "regForm";  
  var adjTop = 0;
  if("wugo_reg"==scrDiv){    
    frmId = "firstName_fieldset";    
  }
  jQuery('html, body').animate({
        scrollTop: jQuery("#"+frmId).offset().top - adjTop
  }, 1000); 
}
//

function validateResetPassword(){
  var message = true;
  blockNone('password_error', 'none'); 
  blockNone('qapTcha_errorDes', 'none');
  blockNone('qapTcha_errorMob', 'none');
  blockNone('successMessageDiv', 'none');
  var password = trimString($$D("password").value );     
  if(isEmpty(password)){ message = false; 
    showErrorMsg('password_error',ErrorMessage.error_login_password);
    $jq("#Slider").draggable('disable');
  }else if(!isEmpty(password) && password.length < 6){ 
    message = false;
    showErrorMsg('password_error',ErrorMessage.error_password_length);
    $jq("#Slider").draggable('disable');
  } 
  if($$D("bgSlider").className != "sucess"){ // Slider validation added by Prabha on 03-NOV-15
    message = false;
    $$D("qapTcha_errorMob").className = "errormessage er_msg_mob";
    showErrorMsg('qapTcha_errorDes', ErrorMessage.error_slider);
    showErrorMsg('qapTcha_errorMob', ErrorMessage.error_slider);
  }else{
    $$D("qapTcha_errorMob").className = "errormessage er_msg_mob success1"; 
  }
    if(message == false){return false;}
    return true;
}
function checkEmail(){
  var emailAddress = trimString($$D("firstname").value);   
  if(isEmpty(emailAddress) || emailAddress == 'Enter email address'){
    $jq("#Slider").draggable('disable');
  }else{
    if($$D("bgSlider").className != "sucess"){
      $jq("#Slider").draggable('enable');
}
  }
}
function validateForgotPassword(){
  var message = true;
  blockNone('emailAddress_err', 'none');
  blockNone('emailAddress_error', 'none');
  blockNone('qapTcha_errorDes', 'none');
  blockNone('qapTcha_errorMob', 'none');
 blockNone('invalidEmailErrDiv', 'none');
 blockNone('successMessageDiv', 'none');
  var emailAddress = trimString($$D("firstname").value);   
  if(isEmpty(emailAddress)){ message = false; showErrorMsg('emailAddress_error', ErrorMessage.error_user_email);
    $jq("#Slider").draggable('disable');
  }else{
  if(!checkValidEmail(emailAddress)){ message = false; showErrorMsg('emailAddress_error', ErrorMessage.error_valid_email); 
    //$jq("#Slider").draggable('disable');
  }
    if($$D("bgSlider").className != "sucess"){
      $jq("#Slider").draggable('enable');
    }
  }
  if($$D("bgSlider").className != "sucess"){ // Slider validation added by Prabha on 03-NOV-15
    message = false;
   
      $$D("qapTcha_errorMob").className = "errormessage er_msg_mob";
      showErrorMsg('qapTcha_errorDes', ErrorMessage.error_slider);
      showErrorMsg('qapTcha_errorMob', ErrorMessage.error_slider);
    
  }else{    
    $$D("qapTcha_errorMob").className = "errormessage er_msg_mob success1";
 }
  if(message == false){return false;}
  return true;
}
function callOpenDays(param){
   var providerName = $$D("openDayProviderName").value; 
        var openDaydate = $$D("openDaySDateDay").value; 
        var openDayMonth = $$D("openDaySDateMonthYear").value; 
        var openDayCollegeId = $$D("openDaySCollegeId").value; 
        var openDayEventId = $$D("openDaySEventId").value;     
        var suborderItemId= $$D("openDaySubOrderItemId").value;  
        var qualId= $$D("openDayCpeQualId").value;
        var bookingUrl = $$D("openDayBookingUrl").value;
        var webPrice = $$D("openDayWebPrice").value;
        //Added bookingUrl and webPrice for logging reserve place button by Prabha on 03_07_2018
        addOpendaystocalendar(openDaydate,openDayMonth,openDayCollegeId,openDayEventId,'fromlogin',suborderItemId,qualId,openDayEventId, param, webPrice, bookingUrl);//openday registration param added by Prabha :: 27_jan_2016
}
var clientId = '1020284304684-4gcs668csbpv2tfg8jq9hggp1nj2a1vo.apps.googleusercontent.com';
var apiKey   = 'AIzaSyB4FQ9qdn5cvqU8N4h7NYVds81lxRc2OxI';
var scopes   = 'email';

function loginWithGooglePlus(){
  facebooktype = 'gpluslogin'; //added for fixing the bug in insights YOE, by Prabha on 29_Mar_2016 
  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
}

function handleAuthResult(authResult) {
  if (authResult['status']['signed_in']) {    
    makeGoogleApiCall();
  }else {
    alert("Not logged in");
  }
}
function checkChanges(){}
function makeGoogleApiCall() {
  var email       = "";
  var firstName   = "";
  var lastName    = "";
  var displayName = "";
  var userImage   = "";
  //var gPlusUserId   = "";
  //Load the Google+ API
  gapi.client.load('plus', 'v1', function(){
  //Assemble the API request
    var request = gapi.client.plus.people.get({userId: 'me'});
    request.execute(function(resp) {      
      if(resp.emails !=  null && resp.emails!=""){
        for (var i=0; i < resp.emails.length; i++) {  
          email = resp.emails[i].value;        
        }
      }
      firstName   = resp.name.givenName;
      lastName    = resp.name.familyName;
      displayName = resp.displayName;
      userImage   = resp.image.url;  
      //gPlusUserId = resp.id;
      //socialMediaRegisterProcess(firstName, lastName, displayName, email, userImage,'fbreg','gpluslogin',gPlusUserId);
      socialMediaRegisterProcess(firstName, lastName, displayName, email, userImage,'fbreg','gpluslogin','');
    });
  });
}
//Check the postcode is exist and show the appropriate message, By Thiyagu G for 03_Nov_2015.
function checkEmailExist(id){
    var email = $$D(id).value;
    if(email == ''){
      setErrorAndSuccessMsg('emailAddress_fieldset','w100p fst_lg1 qlerr','registerErrMsg', ErrorMessage.error_reg_user_email);
      $$D("registerErrMsg").className = "qler1";
      $$D("emailValidateFlag").value="false";
      return false;
    }
    var url= "/degrees/commonaction.html";
    var ajax=new sack();
    ajax.requestFile = url;	
    
    ajax.setVar("emailAddress", trimString(email));
    ajax.setVar("actionType", "checkEmail");
    
    ajax.onCompletion = function(){ showResponseFun(ajax, email); };	
    ajax.runAJAX();
}  

function showResponseFun(ajax, textVal){
  var response = ajax.response;  
  if(response.trim() == "EXIST"){    
    text =  "The email address " + textVal + " is already registered with us, please login. This is to protect your personal data";    
    setErrorAndSuccessMsg('emailAddress_fieldset','w100p fst_lg1 qlerr','registerErrMsg', text);
    $$D('registerErrMsg').className="qler1";    
    if($$('emailAlreadyExistsflag')){$$('emailAlreadyExistsflag').value="yes"; }
    $$D("emailValidateFlag").value="false";
    return false;
  }else{
    if($$('emailAlreadyExistsflag')){$$('emailAlreadyExistsflag').value="no"; }
    if(!isEmpty(textVal) && textVal.indexOf("@") > -1 && textVal.split('@')[1].length > 0){
      if(!checkValidEmail(trimString(textVal))){      
        setErrorAndSuccessMsg('emailAddress_fieldset','w100p fst_lg1 qlerr','registerErrMsg', ErrorMessage.error_reg_user_email); 
        $$D("registerErrMsg").className = "qler1";
        $$D("emailValidateFlag").value="false";
        return false;
      }else if(!isEmpty(textVal)){     
       setErrorAndSuccessMsg('emailAddress_fieldset','w100p fst_lg1 sumsg','registerErrMsg', ErrorMessage.error_email_sucs);
       $$D('registerErrMsg').className="sutxt";             
       $$D("emailValidateFlag").value="true";
      }    
    }else{
      setErrorAndSuccessMsg('emailAddress_fieldset','w100p fst_lg1 qlerr','registerErrMsg', ErrorMessage.error_reg_user_email); 
      $$D("registerErrMsg").className = "qler1";
      $$D("emailValidateFlag").value="false";
      return false;
    }    
  }
}
function showPassword(id, passId) {
	var pwd = $$D(passId);
	if(pwd.getAttribute("type")=="password" && !isEmpty($$D("password").value)){
		pwd.setAttribute("type","text");
		$$D(id).className = "fa fa-eye-slash";
	} else {
		pwd.setAttribute("type","password");
		$$D(id).className = "fa fa-eye";
	}
}
function hidepassword(id, passId){
  var pwd = $$D(passId);
  pwd.setAttribute("type","password");
  $$D(id).className = "fa fa-eye";
}
var dev = jQuery.noConflict();
dev(document).ready(function(){
 dev(".scrltab a:first-child").addClass("tabact");
  var text = dev('.scrltab a.tabact').text();
  var act_tab_sel_1 = dev('.scrltab a.tabact').attr('href');
  dev(act_tab_sel_1).addClass('active');
  dev( ".scrltab a" ).each(function() {
    dev( ".scrltab a" ).click(function(event) {
      event.preventDefault();
      var act_tab_sel = dev('.scrltab a.tabact').attr('href');
      dev(act_tab_sel).removeClass('active');
      dev(act_tab_sel).addClass('hide');  
      var tar_tab_sel = dev(this).attr('href');
      dev(tar_tab_sel).removeClass('hide');
      dev(tar_tab_sel).addClass('active');           
      dev( this ).siblings().removeClass("tabact");
      //dev( this ).toggleClass("tabact");
      dev( this ).addClass("tabact");
      text = dev('.scrltab a.tabact').text();
      //dev("#mbleview").text(text+'<i class="fa fa-angle-down fa-lg"></i>');
      document.getElementById("mbleview").innerHTML = text +'<i class="fa fa-angle-down fa-lg"></i>';
      //if(dev(document).width() < 768 ) {
        dev('.scrltab').hide();
      //}
      //dev('.scrltab').show();
      dev(".feerw_rt .fstu_lnk a").click(function(e)
     {
      e.preventDefault();
      });
    });
    document.getElementById("mbleview").innerHTML = text +'<i class="fa fa-angle-down fa-lg"></i>';
  });
  dev(".fee_nav .fnav_wrp").click(function(event) {
    dev(".scrltab").slideToggle(500);
  });
});
//
function clearingLandingUserPageFlag(type) {
  if(type == 'SIGN_EMAIL') {
    $$D('middlePageSigninFlag').value = 'Y';
  }
}
//
function resetClearingLandingPageFlags() {
  if($$D('middlePageSigninFlag') && $$D('middlePageSigninFlag').value == 'Y') {$$D('middlePageSigninFlag').value = 'N' }
}
//
function getDeviceSpecificDataSrc(imgsrc){
  var deviceWidth =  (window.innerWidth > 0) ? window.innerWidth : screen.width;
  var datasrc = null;
  if(deviceWidth >= 1024){
    datasrc = imgsrc.getAttribute('data-src');
  }else if(deviceWidth >= 768){
    datasrc = imgsrc.getAttribute('data-src-ipad');
  }else if(deviceWidth >= 480 && deviceWidth <= 767){
    datasrc = imgsrc.getAttribute('data-src-tab');
  }else if(deviceWidth >= 320 && deviceWidth <= 480){
    datasrc = imgsrc.getAttribute('data-src-mobile');
  }
  return datasrc;
}
//
function lazyLoadHeroImage() {
	var imageSrc = $$D('heroPodImg') ? $$D('heroPodImg') : null;
	if(imageSrc != null) {
			var imgSrc = imageSrc.getAttribute("src");
			if((imgSrc.indexOf('/img_px') > -1)) {
				imageSrc.src = getDeviceSpecificDataSrc(imageSrc);
			}
	}		
}
//
function isScrolledIntoView(element, fullyInView) {
	if(element !=null) {
		var pageTop = dev(window).scrollTop();
		var pageBottom = pageTop + dev(window).height();
		var elementTop = dev(element).offset().top;
		var elementBottom = elementTop + dev(element).height();
		/*
		if((elementBottom >= pageTop)) {
			pageBottom = pageBottom - ((pageBottom/100) * 0.65);
		} else if((elementTop  <= pageBottom)){
			pageTop = pageTop - ((pageTop/100) * 0.65);
		}
		*/
		//console.log(dev(element).attr('id')+' > '+ elementTop+ ' < = '+pageBottom+' && '+elementBottom+' >= '+pageTop);
		//console.log(dev(element).attr('id')+' > '+'fullyInView > '+((elementTop  <= pageBottom) && (elementBottom >= pageTop)));
		if (fullyInView === true) {
			return ((pageTop < elementTop) && (pageBottom > elementBottom));
		} else {
			return ((elementTop  <= pageBottom) && (elementBottom >= pageTop));
		}
		//console.log('fullyInView > '+((elementTop  <= pageBottom) && (elementBottom >= pageTop)));
	} else {
		return false;
	}
	//
}

/*function emailLabelAlignment(emailId){
  var emailAddress  = $$D(emailId.id);
  if(emailAddress == "emailAddress"){
    $$D('#emailAddress').next('label').addClass("top_lb");
  }
}*/

/*function emailLabelAlignment(emailId){
   var jq = jQuery.noConflict();
   //var emailAddress = jq(emailId.id);
   if("emailAddress" == emailId){
    jq('#emailAddress').next('label').addClass("top_lb");
   }
}*/