/**
 * Author:	Mohamed Syed
 * Date:	wu318_20111020
 * Purpose:	functions related to search-related-page
 * Version:	1.0
 */
//
var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";//Added wu_scheme by Indumathi.S Mar-29-2016
//
var kwDef = 'Please enter subject or uni'
var ucasDef = 'Please enter UCAS code'
var qualDef = 'Select qualification'
var uniDef = 'Enter uni name'
var empKw = ' Search error.\n Please enter a valid keyword and qualification.'
var empUni = 'Please select a uni from the drop down or browse using our A-Z list.'
var clKw = 'ENTER SUBJECT';
var subUni = 'Enter uni name';
//


function setSelectedDateSearch(obj, spanid){
  var dobid = obj.id
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  document.getElementById(spanid).innerHTML = text;
  
	$$('navQual').value = obj.value;
  var preQual = $$('selQual').value;
  if(preQual != obj.innerHTML){
    $$('navKwd').value = kwDef;
    $$('keyword_hidden').value = "";
    $$('keyword_id').value = "";
    $$('keyword_url').value = "";
    $$('keyword_display').value = "";
    $$('matchbrowsenode').value = "";
  }  
}

function setQual(o){
	$$('navQual').value = (o.innerHTML).replace(/&amp;/g, '&');
	$$('navQualList').style.display = 'none';
  var preQual = $$('selQual').value;
  if(preQual != o.innerHTML){
  var qualValue = $$('navQual').value;
  if(qualValue.trim()=='UCAS CODE'){
      $$('navKwd').value = ucasDef;
  }else{
      $$('navKwd').value = kwDef;
  }
    $$('keyword_hidden').value = "";
    $$('keyword_id').value = "";
    $$('keyword_url').value = "";
    $$('keyword_display').value = "";
    $$('matchbrowsenode').value = "";
  }
  adv("#navQual").next("#navQualList").hide();
  adv("#hdrmenu2").find("#navKwd").focus();  
  $$D("navKwd").value = "";
  adv("#hdrmenu2").find("#navKwd").css("outline","2px solid #02ade2");
}

function toggleQualListMob(display){
	if($$('navMobQualList') && $$('navMobQualList').style.display == 'none'){		
  $$('navMobQualList').style.display = 'block';
	} else {
  $$('navMobQualList').style.display = 'none';		
	}	
	if(display != null || display != 'undefined'){
		$$('navMobQualList').style.display = display
	}
}
function setMobQual(o){
	var navMobQual = (o.innerHTML).replace(/&amp;/g, '&');
 //alert(navMobQual);
 adv('#navMobQual').val(navMobQual);
 adv('#navQual').val(navMobQual);
	$$('navMobQualList').style.display = 'none';
  var preQual = $$('selMobQual').value;
  if(preQual != o.innerHTML){
  var qualValue = $$('navMobQual').value;
  if(qualValue.trim()=='UCAS CODE'){
      $$('navMobKwd').value = ucasDef;
  }else{
      $$('navMobKwd').value = kwDef;
  }
    $$('keywordMob_hidden').value = "";
    $$('keywordMob_id').value = "";
    $$('keywordMob_url').value = "";
    $$('keywordMob_display').value = "";
    $$('matchbrowsenodemob').value = "";
  }
  adv("#navMobQual").next("#navMobQualList").hide();
  adv(".sfld_rw2").find("#navMobKwd").focus();  
  $$D("navMobKwd").value = "";
  adv(".sfld_rw2").find("#navMobKwd").css("outline","2px solid #02ade2");
}
//
function toggleQualList(display){
	if($$('navQualList').style.display == 'none'){
		$$('navQualList').style.display = 'block'
	} else {
		$$('navQualList').style.display = 'none'
	}	
	if(display != null || display != 'undefined'){
		$$('navQualList').style.display = display
	}
}
function hideBlock() {
    $$('navQualList').style.display = 'none'
}
//

function setQualCL(o){
	$$('navQualCL').value = (o.innerHTML);
	$$('navQualListCL').style.display = 'none';
  var preQual = $$('selQual').value;
  if(preQual != o.innerHTML){
      $$('navKwdCL').value = clKw;
    $$('keyword_hidden').value = "";
    $$('keyword_id').value = "";
    $$('keyword_url').value = "";
    $$('keyword_display').value = "";
    $$('matchbrowsenode').value = "";
  }
	//toggleQualList();
}
//
function toggleQualListCL(display){
	if($$('navQualListCL').style.display == 'none'){
		$$('navQualListCL').style.display = 'block'
	} else {
		$$('navQualListCL').style.display = 'none'
	}	
	if(display != null || display != 'undefined'){
		$$('navQualListCL').style.display = display
	}
}
function hideBlockCL() {
    $$('navQualListCL').style.display = 'none'
}
//


function subjectOnchange(obj, sBox1S){
    //$$(sBox1S).innerHTML = obj.value;
    location.href = obj.value;
}
/*Start
Added by sekhar for wu403_14022012*/
function changeFunc(obj,s2) {  
    hideGradeErrDiv($$("grdeErr")); // hide responsive grade filter error message div added by Prabha on NOV_03_2015
    $$(s2).innerHTML = obj.value;
    var entryLevel = document.getElementById('selBox1').value;
    $$("upt").disabled = false;
    $$("upt").className = 'updt_active';
    if(entryLevel == "A-levels"){
        if($$("selBoxA*").value == '0' && $$("selBoxA").value == '0' && $$("selBoxB").value == '0' && $$("selBoxC").value == '0' && $$("selBoxD").value == '0' && $$("selBoxE").value == '0'){
            $$("upt").disabled = true;
            $$("upt").className = 'updt'; 
        }
    }
    if(entryLevel == "SQA Highers" || entryLevel == "SQA Advanced Highers"){
        if($$("selBoxA").value == '0' && $$("selBoxB").value == '0' && $$("selBoxC").value == '0' && $$("selBoxD").value == '0'){
            $$("upt").disabled = true;
            $$("upt").className = 'updt'; 
        }
    }
    if(entryLevel == "BTEC"){
        if($$("selBoxD*").value == '0' && $$("selBoxBtecD").value == '0' && $$("selBoxM").value == '0' && $$("selBoxP").value == '0'){
            $$("upt").disabled = true;
            $$("upt").className = 'updt'; 
        }
    }
}
//
function returnKeyFormSub(){
 return entryPointFilterSubmit();
}

function changeFunc1(obj,s2) {
    hideGradeErrDiv($$("grdeErr")); // hide responsive grade filter error message div added by Prabha on NOV_03_2015
    $$("upt").disabled = true;
    $$("upt").className = 'updt'; 
    $$(s2).innerHTML = obj.value;
}
function onClickTarif(){
    hideGradeErrDiv($$("grdeErr")); // hide responsive grade filter error message div added by Prabha on NOV_03_2015
    $$("upt").disabled = false;
    $$("upt").className = 'updt_active';
}

function changeEntryPoints(obj, entryLevelReset, astarvaluereset, avaluereset, bvaluereset, cvaluereset, dvaluereset, evaluereset,dstarvaluereset, btecdvaluereset, mvaluereset, pvaluereset, tariffPoints) {    
        var grdeErrDiv = $$("grdeErr");
        hideGradeErrDiv(grdeErrDiv); // hide responsive grade filter error message div added by Prabha on NOV_03_2015
        var valueArray = {"Alevels":5, "SQAHighers":8, "SQAAdvancedHighers":8, "BTEC":3};
        //var enableBoxes = {"Alevels":"A*, A*span, A, Aspan, B, Bspan, C, Cspan, D, Dspan, E, Espan", "SQAHighers":"A, Aspan, B, Bspan, C, Cspan, D, Dspan", "SQAAdvancedHighers":"A, Aspan, B, Bspan, C, Cspan, D, Dspan", "Tarrif Point":"tp"};        
        if(obj.value == "A-levels"){      
            var optString = "";
            var count = valueArray.Alevels;
            var selBoxAStar = document.getElementById("selBoxA*");
            var selBoxA = document.getElementById("selBoxA");
            var selBoxB = document.getElementById("selBoxB");
            var selBoxC = document.getElementById("selBoxC");
            var selBoxD = document.getElementById("selBoxD");
            var selBoxE = document.getElementById("selBoxE");            
            $$('selBoxA*').innerHTML = "";
            $$('selBoxA').innerHTML = "";
            $$('selBoxB').innerHTML = "";
            $$('selBoxC').innerHTML = "";
            $$('selBoxD').innerHTML = "";
            $$('selBoxE').innerHTML = "";                        
            for(var i = 0; i<=count; i++){
                var opt = document.createElement("option");
                opt.value=""+i;
                opt.appendChild(document.createTextNode(i));
                selBoxAStar.appendChild(opt);
                var opt1 = document.createElement("option");
                opt1.value=""+i;
                opt1.appendChild(document.createTextNode(i));
                selBoxA.appendChild(opt1);
                var opt2 = document.createElement("option");
                opt2.value=""+i;
                opt2.appendChild(document.createTextNode(i));
                selBoxB.appendChild(opt2);
                var opt3 = document.createElement("option");
                opt3.value=""+i;
                opt3.appendChild(document.createTextNode(i));
                selBoxC.appendChild(opt3);
                var opt4 = document.createElement("option");
                opt4.value=""+i;
                opt4.appendChild(document.createTextNode(i));
                selBoxD.appendChild(opt4);
                var opt5 = document.createElement("option");
                opt5.value=""+i;
                opt5.appendChild(document.createTextNode(i));
                selBoxE.appendChild(opt5);
            }            
            $$("pttxt").innerHTML = 'Your grades/expected grades';
            changeGradeText('grade'); // code added wu_546
           // $$("pthover").innerHTML = 'Select the number of each grade you have (or what you are hoping to get), then hit update to make sure your results only show the unis whose requirements you meet.';
            $$("A*").style.display = "block";
            $$("A*span").style.display = "block";
            $$("A").style.display = "block";
            $$("Aspan").style.display = "block";
            $$("B").style.display = "block";
            $$("Bspan").style.display = "block";
            $$("C").style.display = "block";
            $$("Cspan").style.display = "block";
            $$("D").style.display = "block";
            $$("Dspan").style.display = "block";
            $$("E").style.display = "block";
            $$("Espan").style.display = "block";            
            $$("D*").style.display = "none";
            $$("D*span").style.display = "none";
            $$("BtecD").style.display = "none";
            $$("BtecDspan").style.display = "none";
            $$("M").style.display = "none";
            $$("Mspan").style.display = "none";
            $$("P").style.display = "none";
            $$("Pspan").style.display = "none";            
            $$("tp").style.display = "none"; 
            // code added wu_546
            $$("Aspan").className = "gd";
            $$("Bspan").className = "gd";
            $$("Cspan").className = "gd";
            $$("Dspan").className = "gd";
        }else if(obj.value == "SQA Highers" || obj.value == "SQA Advanced Highers"){            
            var count = valueArray.SQAHighers;
            var optString = "";
            var selBoxA = document.getElementById("selBoxA");
            var selBoxB = document.getElementById("selBoxB");
            var selBoxC = document.getElementById("selBoxC");
            var selBoxD = document.getElementById("selBoxD"); 
            $$('selBoxA').innerHTML = "";
            $$('selBoxB').innerHTML = "";
            $$('selBoxC').innerHTML = "";
            $$('selBoxD').innerHTML = "";
            for(var i = 0; i<=count; i++){
                var chopt = document.createElement("option");
                chopt.value=""+i;
                chopt.appendChild(document.createTextNode(i));
                var chopt1 = document.createElement("option");
                chopt1.value=""+i;
                chopt1.appendChild(document.createTextNode(i));
                var chopt2 = document.createElement("option");
                chopt2.value=""+i;
                chopt2.appendChild(document.createTextNode(i));
                var chopt3 = document.createElement("option");
                chopt3.value=""+i;
                chopt3.appendChild(document.createTextNode(i));
                selBoxA.appendChild(chopt);
                selBoxB.appendChild(chopt1);
                selBoxC.appendChild(chopt2);
                selBoxD.appendChild(chopt3);
            }
            //$$("upt").disabled = false;
            $$("pttxt").innerHTML = 'Your grades/expected grades';
            changeGradeText('grade'); // code added wu_546
           // $$("pthover").innerHTML = 'Select the number of each grade you have (or what you are hoping to get), then hit update to make sure your results only show the unis whose requirements you meet.';
            $$("A*").style.display = "none";
            $$("A*span").style.display = "none";
            $$("A").style.display = "block";
            $$("Aspan").style.display = "block";
            $$("B").style.display = "block";
            $$("Bspan").style.display = "block";
            $$("C").style.display = "block";
            $$("Cspan").style.display = "block";
            $$("D").style.display = "block";
            $$("Dspan").style.display = "block";
            $$("E").style.display = "none";
            $$("Espan").style.display = "none";
            
            $$("D*").style.display = "none";
            $$("D*span").style.display = "none";
            $$("BtecD").style.display = "none";
            $$("BtecDspan").style.display = "none";
            $$("M").style.display = "none";
            $$("Mspan").style.display = "none";
            $$("P").style.display = "none";
            $$("Pspan").style.display = "none";
            
            $$("tp").style.display = "none";
            // code added wu_546
            $$("Aspan").className = "gd gd1";
            $$("Bspan").className = "gd gd1";
            $$("Cspan").className = "gd gd1";
            $$("Dspan").className = "gd gd1";
        }else if(obj.value == "BTEC"){            
            var count = valueArray.BTEC;
            var optString = "";                        
            var selBoxDStar = document.getElementById("selBoxD*");
            var selBoxBtecD = document.getElementById("selBoxBtecD");
            var selBoxM = document.getElementById("selBoxM");
            var selBoxP = document.getElementById("selBoxP");
            $$('selBoxD*').innerHTML = "";
            $$('selBoxBtecD').innerHTML = "";
            $$('selBoxM').innerHTML = "";
            $$('selBoxP').innerHTML = "";
            for(var i = 0; i<=count; i++){
                var chopt = document.createElement("option");
                chopt.value=""+i;
                chopt.appendChild(document.createTextNode(i));
                var chopt1 = document.createElement("option");
                chopt1.value=""+i;
                chopt1.appendChild(document.createTextNode(i));
                var chopt2 = document.createElement("option");
                chopt2.value=""+i;
                chopt2.appendChild(document.createTextNode(i));
                var chopt3 = document.createElement("option");
                chopt3.value=""+i;
                chopt3.appendChild(document.createTextNode(i));
                selBoxDStar.appendChild(chopt);
                selBoxBtecD.appendChild(chopt1);
                selBoxM.appendChild(chopt2);
                selBoxP.appendChild(chopt3);                
            }
            //$$("upt").disabled = false;
            $$("pttxt").innerHTML = 'Your grades/expected grades';
            changeGradeText('grade'); // code added wu_546
           // $$("pthover").innerHTML = 'Select the number of each grade you have (or what you are hoping to get), then hit update to make sure your results only show the unis whose requirements you meet.';
            $$("A*").style.display = "none";
            $$("A*span").style.display = "none";
            $$("A").style.display = "none";
            $$("Aspan").style.display = "none";
            $$("B").style.display = "none";
            $$("Bspan").style.display = "none";
            $$("C").style.display = "none";
            $$("Cspan").style.display = "none";
            $$("D").style.display = "none";
            $$("Dspan").style.display = "none";
            $$("E").style.display = "none";
            $$("Espan").style.display = "none";
            
            $$("D*").style.display = "block";
            $$("D*span").style.display = "block";
            $$("BtecD").style.display = "block";
            $$("BtecDspan").style.display = "block";
            $$("M").style.display = "block";
            $$("Mspan").style.display = "block";
            $$("P").style.display = "block";
            $$("Pspan").style.display = "block";
            
            $$("tp").style.display = "none";
            // code added wu_546
            $$("Aspan").className = "gd";
            $$("Bspan").className = "gd";
            $$("Cspan").className = "gd";
            $$("Dspan").className = "gd";
        }else if(obj.value == "Tariff Points"){            
            //$$("upt").disabled = false;
            $$("pttxt").innerHTML = 'Your points/expected points';
            changeGradeText('tariff'); // code added wu_546
           // $$("pthover").innerHTML = 'Type in your tariff points (or what you are hoping to get) and hit update to make sure your results only show the unis whose requirements you meet.';
            $$("A*").style.display = "none";
            $$("A*span").style.display = "none";
            $$("A").style.display = "none";
            $$("Aspan").style.display = "none";
            $$("B").style.display = "none";
            $$("Bspan").style.display = "none";
            $$("C").style.display = "none";
            $$("Cspan").style.display = "none";
            $$("D").style.display = "none";
            $$("Dspan").style.display = "none";
            $$("E").style.display = "none";
            $$("Espan").style.display = "none";
            $$("tp").style.display = "block";
            
            $$("D*").style.display = "none";
            $$("D*span").style.display = "none";
            $$("BtecD").style.display = "none";
            $$("BtecDspan").style.display = "none";
            $$("M").style.display = "none";
            $$("Mspan").style.display = "none";
            $$("P").style.display = "none";
            $$("Pspan").style.display = "none";
            
        }else{
            $$('selBoxA*').innerHTML = '0';
            $$('selBoxA').innerHTML = '0';
            $$('selBoxB').innerHTML = '0';
            $$('selBoxC').innerHTML = '0';
            $$('selBoxD').innerHTML = '0';
            $$('selBoxE').innerHTML = '0';
            
            $$('selBoxD*').innerHTML = '0';
            $$('selBoxBtecD').innerHTML = '0';
            $$('selBoxM').innerHTML = '0';
            $$('selBoxP').innerHTML = '0';
            
            $$("tp").style.display = "none";
            $$("upt").disabled = true;
        }
        if(obj.value == entryLevelReset){
            $$('sBoxA*').innerHTML = astarvaluereset;
            $$('sBoxA').innerHTML = avaluereset;
            $$('sBoxB').innerHTML = bvaluereset;
            $$('sBoxC').innerHTML = cvaluereset;
            $$('sBoxD').innerHTML = dvaluereset;
            $$('sBoxE').innerHTML = evaluereset;
            
            $$('sBoxD*').innerHTML = dstarvaluereset;
            $$('sBoxBtecD').innerHTML = btecdvaluereset;
            $$('sBoxM').innerHTML = mvaluereset;
            $$('sBoxP').innerHTML = pvaluereset;
            
            $$("tp").value = tariffPoints;
        }
        else{
            $$('sBoxA*').innerHTML = '0';
            $$('sBoxA').innerHTML = '0';
            $$('sBoxB').innerHTML = '0';
            $$('sBoxC').innerHTML = '0';
            $$('sBoxD').innerHTML = '0';
            $$('sBoxE').innerHTML = '0';
            
            $$('sBoxD*').innerHTML = '0';
            $$('sBoxBtecD').innerHTML = '0';
            $$('sBoxM').innerHTML = '0';
            $$('sBoxP').innerHTML = '0';
            
            $$("tp").value = "";
        }
}
/* Method to change the text for grades and tariff added by Prabha on NOV_03_2015 */
function changeGradeText(action){
  var mbeGdeTxt = 'Your expected grades?';
  var mbeTarifTxt = 'Your points/expected points';
  if(action == "grade"){
    if($$("yr_exp2")){
      $$("yr_exp2").innerHTML = mbeGdeTxt;
    }else{
      $$("yr_exp1").innerHTML = mbeGdeTxt;
    }
  }else{
    if($$("yr_exp2")){
      $$("yr_exp2").innerHTML = mbeTarifTxt;
    }else{
      $$("yr_exp1").innerHTML = mbeTarifTxt;
    }
  }
}
/*Method to hide the grade filter error div added wu_546 */
function hideGradeErrDiv(grdeErrDiv){
  if(grdeErrDiv){
    grdeErrDiv.innerHTML = "";
    grdeErrDiv.style.display = "none";
  }
}
function customAlertPopup(pagename, respGrde){
  if(respGrde == "found"){
    hideGradeErrDiv($$("grdeErr")); // hide responsive grade filter error message div added wu_546
  }
  if(pagename=='grade_search') {
    var validationFlag = '';
    if(respGrde == "found"){
      validationFlag = validateEntryPointsRange(respGrde); // respGrde param added wu_546
    }else{
      validationFlag = validateEntryPointsRange();
    }    
    if(validationFlag == 'PROCEED'){
      var loginFlag = "";
      setEnterFilterValues(); // Set the grade filter value to hidden
      if($$("userid").value==0) {  
        showLightBoxLoginForm('grade-popup',650,500);
      }else{
        entryFilterSubmit();
      }      
    }else if(validationFlag == 'A-LEVEL' || validationFlag == 'SQA Highers' || validationFlag == 'UCAS TARRIF'  || validationFlag == 'BTEC'){
      if(respGrde == "found"){
        showResponsiveGradeError(validationFlag, $$("grdeErr")); // code added by Prabha on NOV_03_2015
      }else{
        showLightBoxLoginForm('grade-range-popup',650,500, validationFlag);
      }
    }
  }else if(pagename=='clsearch'){    
    showLightBoxLoginForm('clearing-popup',650,500);   
  }
  return false;
}
/* Method to set the values for grade filter to hidden */
function setEnterFilterValues(){
  $$("lb_selBox1").value = $$("selBox1").value;
  $$("lb_selBoxA*").value = $$("selBoxA*").value;
  $$("lb_selBoxA").value = $$("selBoxA").value;
  $$("lb_selBoxB").value = $$("selBoxB").value;
  $$("lb_selBoxC").value = $$("selBoxC").value;
  $$("lb_selBoxD").value = $$("selBoxD").value;
  $$("lb_selBoxE").value = $$("selBoxE").value;
  $$("lb_selBoxD*").value = $$("selBoxD*").value;
  $$("lb_selBoxBtecD").value = $$("selBoxBtecD").value;
  $$("lb_selBoxM").value = $$("selBoxM").value;
  $$("lb_selBoxP").value = $$("selBoxP").value;
  $$("lb_tp").value = $$("tp").value;
}
/* Method to show the error message for grade filter added by Prabha on NOV_03_2015 */
function showResponsiveGradeError(validationFlag, grdeErrDiv){
  grdeErrDiv.style.display = "block";
  if(validationFlag == 'A-LEVEL'){
    grdeErrDiv.innerHTML += "Woah, steady on there! You can only enter a maximum of six grades (and if you really are enough of a genius to be doing more than six, just enter your top ones).";
  }else if(validationFlag == 'SQA Highers'){
    grdeErrDiv.innerHTML += "Woah, steady on there! You can only enter a maximum of ten grades (and if you really are enough of a genius to be doing more than ten, just enter your top ones).";
  }else if(validationFlag == 'UCAS TARRIF'){
    grdeErrDiv.innerHTML += "Woah, steady on there! You can only enter a maximum of 999 tariff points.";
  }else if(validationFlag == 'BTEC'){
    grdeErrDiv.innerHTML += "Woah, steady on there! You can only enter a maximum of three grades (and if you really are enough of a genius to be doing more than three, just enter your top ones).";
  }
}
function entryPointFilterSubmit(loginFlag){
  var entryLevelValue = $$("lb_selBox1").value;
    switch(entryLevelValue){
      case 'A-levels': entryLevel='74';break
      case 'SQA Highers': entryLevel = '75';break
      case 'SQA Advanced Highers': entryLevel = '76';break
      case 'BTEC': entryLevel = '555';break
      case 'Tariff Points': entryLevel = '77';break          
    }
    var xmlTemp='<entry-grades>';
    if(entryLevelValue == "A-levels"){      
      xmlTemp=xmlTemp+'<A*>'+$$("lb_selBoxA*").value+'</A*>';
      xmlTemp=xmlTemp+'<A>'+$$("lb_selBoxA").value+'</A>';
      xmlTemp=xmlTemp+'<B>'+$$("lb_selBoxB").value+'</B>';
      xmlTemp=xmlTemp+'<C>'+$$("lb_selBoxC").value+'</C>';
      xmlTemp=xmlTemp+'<D>'+$$("lb_selBoxD").value+'</D>';
      xmlTemp=xmlTemp+'<E>'+$$("lb_selBoxE").value+'</E>';          
    }else if(entryLevelValue == "SQA Highers" || entryLevelValue == "SQA Advanced Highers"){      
      xmlTemp=xmlTemp+'<A>'+$$("lb_selBoxA").value+'</A>';
      xmlTemp=xmlTemp+'<B>'+$$("lb_selBoxB").value+'</B>';
      xmlTemp=xmlTemp+'<C>'+$$("lb_selBoxC").value+'</C>';
      xmlTemp=xmlTemp+'<D>'+$$("lb_selBoxD").value+'</D>';
    }else if(entryLevelValue == "BTEC"){      
      xmlTemp=xmlTemp+'<D*>'+$$("lb_selBoxD*").value+'</D*>';
      xmlTemp=xmlTemp+'<D>'+$$("lb_selBoxBtecD").value+'</D>';
      xmlTemp=xmlTemp+'<M>'+$$("lb_selBoxM").value+'</M>';
      xmlTemp=xmlTemp+'<P>'+$$("lb_selBoxP").value+'</P>';
    }else{
      xmlTemp=xmlTemp+'<free-text>'+$$("lb_tp").value+'</free-text>';      
    }
    xmlTemp=xmlTemp+'</entry-grades>';    
    var gradesXmlValue = entryLevel+'##SPLIT##'+xmlTemp;
    setSessionCookie('grades_prepopulate',gradesXmlValue);
  if(loginFlag) {
    $$("loginGradsSave").value ="YES";
    showLightBoxLoginForm('popup-newlogin',650,500,'gradeFilter','','grade-filter',$$("lb_selBox1").value);
  }else{          
    entryFilterSubmit();
  } 
}

function validateEntryPointsRange(respGrde){
  if(respGrde == "found"){
    hideGradeErrDiv($$("grdeErr")); // hide responsive grade filter error message div added by Prabha on NOV_03_2015
  }
  var entryLevelValue = $$("selBox1").value;  
  if(entryLevelValue == "A-levels"){
      var astarValue = $$("selBoxA*").value;
      var aValue = $$("selBoxA").value;
      var bValue = $$("selBoxB").value;
      var cValue = $$("selBoxC").value;
      var dValue = $$("selBoxD").value;
      var eValue = $$("selBoxE").value;
      var intastarvalue = parseInt(astarValue);
      var intavalue = parseInt(aValue);
      var intbvalue = parseInt(bValue);
      var intcvalue = parseInt(cValue);
      var intdvalue = parseInt(dValue);
      var intevalue = parseInt(eValue);
      if((intastarvalue + intavalue + intbvalue + intcvalue + intdvalue + intevalue) > 6){
        createGradesSelect('A-levels', 5);
        return "A-LEVEL";
      }
  }else if(entryLevelValue == "SQA Highers" || entryLevelValue == "SQA Advanced Highers"){
      var aValue = $$("selBoxA").value;
      var bValue = $$("selBoxB").value;
      var cValue = $$("selBoxC").value;
      var dValue = $$("selBoxD").value;
      var intavalue = parseInt(aValue);
      var intbvalue = parseInt(bValue);
      var intcvalue = parseInt(cValue);
      var intdvalue = parseInt(dValue);
      if((intavalue + intbvalue + intcvalue + intdvalue) > 10){
        createGradesSelect('SQA Highers', 8);
        return "SQA Highers";
      }
  }else if(entryLevelValue == "BTEC"){
      var dstarValue = $$("selBoxD*").value;
      var btecdValue = $$("selBoxBtecD").value;
      var mValue = $$("selBoxM").value;
      var pValue = $$("selBoxP").value;
      var intdstarvalue = parseInt(dstarValue);
      var intbtecdvalue = parseInt(btecdValue);
      var intmvalue = parseInt(mValue);
      var intpvalue = parseInt(pValue);      
      if((intdstarvalue + intbtecdvalue + intmvalue + intpvalue) > 3){        
        createGradesSelect('BTEC', 3);
        return "BTEC";
      }
  }else{
      if($$("tp").value == null || $$("tp").value.trim() ==""){
          $$("tp").value = "";
          if(respGrde == "found"){ // Check the responsive grade filter code added by Prabha on NOV_03_2015
            $$("grdeErr").style.display = "block";          
            $$("grdeErr").innerHTML += "Please enter the tariff points.";
          }else{
            showLightBoxLoginForm('grade-range-popup',650,500, 'Please enter the tariff points.');
          }
          return false;
      }
      if(isNaN($$('tp').value)){
          $$("tp").value = "";
          if(respGrde == "found"){ // Check the responsive grade filter code added by Prabha on NOV_03_2015
            $$("grdeErr").style.display = "block";          
            $$("grdeErr").innerHTML += "Please enter a number for your tariff points.";
          }else{
            showLightBoxLoginForm('grade-range-popup',650,500, 'Please enter a number for your tariff points.');
          }
          return false;
      }
      var entryPoints = $$('tp').value; 
      if(entryPoints > 999){
        $$("tp").value = "";
        return "UCAS TARRIF";
      }
  }
  return "PROCEED";
}

function entryFilterSubmit(gruserId){        
  var evntFilterType = "";  
  if(document.getElementById("searchtype")){
    if(document.getElementById("searchtype").value=="CLEARING_SEARCH"){
      evntFilterType = "clearing-";
    }
  }
  with(document){  
        var existingUrl = $$("sortingUrl").value;
        var contextPath=$$("contextPath").value
        var entryLevel = "";
        var existingUrl = "";
        var dataarray = existingUrl.split("/")
        var urlLength = dataarray.length;
        var entryLevelValue = $$("lb_selBox1").value;
        switch(entryLevelValue){
          case 'A-levels': entryLevel='A-LEVEL';break
          case 'SQA Highers': entryLevel = 'SQA-HIGHER';break
          case 'SQA Advanced Highers': entryLevel = 'SQA-ADV';break
          case 'BTEC': entryLevel = 'BTEC';break
          case 'Tariff Points': entryLevel = 'UCAS-POINTS';break          
        }
        if(entryLevelValue == "A-levels"){
            var astarValue = $$("lb_selBoxA*").value;
            var aValue = $$("lb_selBoxA").value;
            var bValue = $$("lb_selBoxB").value;
            var cValue = $$("lb_selBoxC").value;
            var dValue = $$("lb_selBoxD").value;
            var eValue = $$("lb_selBoxE").value;
            var entryPoints = astarValue+'A*-'+aValue+'A-'+bValue+'B-'+cValue+'C-'+dValue+'D-'+eValue+'E';
        }else if(entryLevelValue == "SQA Highers" || entryLevelValue == "SQA Advanced Highers"){
            var aValue = $$("lb_selBoxA").value;
            var bValue = $$("lb_selBoxB").value;
            var cValue = $$("lb_selBoxC").value;
            var dValue = $$("lb_selBoxD").value;
            var entryPoints = aValue+'A-'+bValue+'B-'+cValue+'C-'+dValue+'D';
        }else if(entryLevelValue == "BTEC"){
            var aStarValue = $$("lb_selBoxD*").value;
            var aValue = $$("lb_selBoxBtecD").value;
            var bValue = $$("lb_selBoxM").value;
            var cValue = $$("lb_selBoxP").value;            
            var entryPoints = aStarValue+'D*-'+aValue+'D-'+bValue+'M-'+cValue+'P';            
        }else{           
            var entryPoints = $$('lb_tp').value;
        }  
        }
        var tailURL = "";
        var searchType = $$("searchtype").value;
        if(searchType == 'NORMAL_SEARCH'){
          tailURL = "/page.html"
        }else{
          tailURL = "/clcourse.html"
        }   
      var seo_qual=''
	    var qual=$$("paramStudyLevelId").value;
      switch(qual){
              case 'M': seo_qual='degree';break
              case 'N': seo_qual='hnd-hnc';break
              case 'A': seo_qual='foundation-degree';break
              case 'T': seo_qual='access-foundation';break
              case 'L': seo_qual='postgraduate';break
      }
      var sort_by="R";
      $$('hidden_entryLevel').value = entryLevel;
      $$('hidden_entryPoints').value = entryPoints;
    var userId = $$("userid").value;
    if(gruserId != "" && gruserId != null && gruserId != 'undefined'){
      userId = gruserId;
      $$("ISEXISTSGRADE").value = "NO";
    }    
    var isExists = $$("ISEXISTSGRADE");    
    if(isExists!=null && isExists!=""){
      if(userId != "" && userId !="0" && isExists != null && isExists.value == "YES" && saveMyGrades()){
        var flag=confirm("These predicted grades aren't the same as the grades in your profile! \nClick \"OK\" to update your profile with these grades. \nClick \"Cancel\" to filter the results by these grades, without saving them to your profile.");        
        if(flag){
           $$("updategradesflg").value ="YES";
        }
      } else if(userId != "" && userId !="0" && isExists.value == "NO"){
          $$("updategradesflg").value ="YES";
          if($$("updategradesflg").value);
      }
    } 
    clearRefineFilters();
    $$('hidden_rf').value = "G";
    searchUrl = urlFormation();
    var final_url=searchUrl;
    if($$("filterEntryPoint")){$$("filterEntryPoint").action=final_url.toLowerCase();}
    searchEventTracking(evntFilterType+'filter',entryLevel.toLowerCase(),entryPoints.toLowerCase());
    //Set cookie to stop grade filter popup within the session for 11_Aug_2015 By Thiyagu G.
    var IEversion = detectIE();
    if (IEversion !== false) {
      setCookie('gradePopup','NO',1,'wucookiepolicy')
    } else {    
      setSessionCookie('gradePopup','NO');
    }
    if($$("filterEntryPoint")){$$("filterEntryPoint").submit();}else{location.href = final_url.toLowerCase();}
}    


function saveMyGrades(){       
   var message = false;
   var entrylevel = $$("tp").value;
   var selBox1 = $$("selBox1").value;
   var selBoxAs = $$("selBoxA*").value;
   var selBoxA = $$("selBoxA").value;   
   var selBoxB = $$("selBoxB").value;   
   var selBoxC = $$("selBoxC").value;   
   var selBoxD = $$("selBoxD").value;
   var selBoxE = $$("selBoxE").value;  
   
   var selBoxDs = $$("selBoxD*").value;
   var selBoxBtecD = $$("selBoxBtecD").value;   
   var selBoxM = $$("selBoxM").value;   
   var selBoxP = $$("selBoxP").value;  
   
   var astarvalue = $$("astarvalue").value;
   var avalue = $$("avalue").value;
   var bvalue = $$("bvalue").value;
   var cvalue = $$("cvalue").value;
   var dvalue = $$("dvalue").value;
   var evalue = $$("evalue").value;
   
   var dstarvalue = $$("dstarvalue").value;
   var btecdvalue = $$("btecdvalue").value;
   var mvalue = $$("mvalue").value;
   var pvalue = $$("pvalue").value;
   
   var tpoints = $$("tariffPointValue").value;   
   if(selBox1 == "Tariff Points" && entrylevel != tpoints){
     message = true;
   }   
   if($$("selBox1") != null && $$("selBox1").value == "A-levels"){
      if(selBoxAs != astarvalue || avalue != selBoxA || selBoxB != bvalue || selBoxC != cvalue || selBoxD != dvalue || selBoxE != evalue){        
        message = true;
      }
   }
   if($$("selBox1") != null && ($$("selBox1").value == "SQA Highers" || $$("selBox1").value == "SQA Advanced Highers")){
      if(avalue != selBoxA || selBoxB != bvalue || selBoxC != cvalue || selBoxD != dvalue || selBoxE != evalue){
        message = true;
      }
   }
   if($$("selBox1") != null && ($$("selBox1").value == "BTEC")){
      if(selBoxDs != dstarvalue || selBoxBtecD != btecdvalue || selBoxM != mvalue || selBoxP != pvalue){
        message = true;
      }
   }  
   return message;
}

/*  End  Added by sekhar for wu403_14022012*/

function navSearchSubmit(flag, fromFlag){  
  var clearFlag = flag;
  var navKwd = 'navKwd';
  var navQual = 'navQual';
  if(fromFlag == 'topMobNav'){
    navKwd = 'navMobKwd';
    navQual = 'navMobQual';
  }
  if(checkNavKeyword(navKwd, navQual)){
    if(clearFlag == 'ON'){
      var navQual = trimString($$(navQual).value); //Added by Sangeeth.S for Jul_3_18 rels
      if(navQual == $$("clearingYear").value || navQual == 'Clearing/Adjustment'){
        updateUserTypeSession('','clearing','newSubmitSearch', fromFlag);        
      }else{
        updateUserTypeSession('','non-clearing','newSubmitSearch', fromFlag);        
      }
    }else{    
      setNavNetworkType(navQual, fromFlag);      
    }
  }else{
    return false;
  }
  return false;
}
function checkNavKeyword(kwdId, qualId){
  var navKwd = trimString($$(kwdId).value);
  var navQual = trimString($$(qualId).value);
  if(navKwd == kwDef || navKwd == '' || navKwd == ucasDef){
    alert(empKw);
    $$(kwdId).value = '';
    $$(kwdId).focus();
    return false;
  }
  if(navQual == qualDef){
    alert(empKw);
    $$(qualId).focus();
    return false;
  }
  return true;
}
function newSubmitSearch(confFlag,clearingFlag, fromFlag){  
  var clearingSearch = confFlag;
  var clearFlag = clearingFlag;
  var navKwd = 'navKwd';
  var navQual = 'navQual';
  var keywordUrl = 'keyword_url';
  var matchbrowsenode = 'matchbrowsenode';
  var navSearchForm = 'navSearchForm';
  if(fromFlag == 'topMobNav'){
    navKwd = 'navMobKwd';
    navQual = 'navMobQual';
    keywordUrl = 'keywordMob_url';
    matchbrowsenode = 'matchbrowsenodemob';
    navSearchForm = 'navMobSearchForm';
  }
  var navKwd = trimString($$(navKwd).value);
  var navQual = trimString($$(navQual).value); 
  var qualDesc = 'degree';
  var qualCode = navQual;
  var qualCode = "M";
  if(navQual == 'Degrees'){
    qualDesc = 'degree'; qualCode='M';
  }else if(navQual == 'Postgraduate'){
    qualDesc = 'postgraduate'; qualCode='L';
  }else if(navQual == 'Foundation degree'){
    qualDesc = 'foundation-degree'; qualCode='A';
  }else if(navQual == 'Access & foundation'){
    qualDesc = 'access-foundation'; qualCode='T';
  }else if(navQual == 'HND / HNC'){
    qualDesc = 'hnd-hnc'; qualCode='N';
  }else if(navQual == 'UCAS CODE'){
    qualDesc = 'ucas_code'; qualCode='UCAS_CODE';
  } else if(navQual == $$("clearingYear").value){ //24_JUN_2014
    qualDesc = 'degree'; qualCode='M';
  }
  var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
  navKwd = navKwd.replace(reg," ");
  if(navKwd == kwDef || navKwd == '' || navKwd == null || navKwd == ucasDef){
    alert(empKw);
    $$('navKwd').value = '';
    $$('navKwd').focus();
    return false;
  }  
  var navKwd1 = replaceAll(navKwd," ","+");
  var navKwd2 = replaceAll(navKwd," ","-");
  var country1 = 'united+kingdom';
  var country2 = 'united-kingdom';  
  var newUrl2 = "";
  var newUrl1 = "/"+qualDesc+"-courses/search";
  if(clearFlag == 'ON'){
    if(clearingSearch){      
      newUrl2 = "?clearing&q="+navKwd2;
    }else{      
      newUrl2 = "?q="+navKwd2;
    }
  }else{  
    newUrl2 = "?q="+navKwd2;
  }
  var finalUrl = newUrl1 + newUrl2;
  if($$(keywordUrl).value != "" ||  $$(matchbrowsenode).value != ""){
    finalUrl = $$(keywordUrl).value != "" ? $$(keywordUrl).value : $$(matchbrowsenode).value;
  }
	 $$(navSearchForm).action = finalUrl.toLowerCase();
	 $$(navSearchForm).submit();
	 return false;
}
//
function clearingSearchSubmit(clearingSearch, clearFlag){	
	var navKwd = trimString($$('clearingKwd').value);
 var navQual = $$("clearingYear").value;	
	var qualDesc = 'degree';
	var qualCode = "M";
  var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
  navKwd = navKwd.replace(reg," ");
  if(navKwd == kwDef || navKwd == '' || navKwd == null || navKwd == ucasDef){
    alert(empKw);
    $$('clearingKwd').value = '';
    $$('clearingKwd').focus();
    return false;
  } 
	var navKwd1 = replaceAll(navKwd," ","+");
	var navKwd2 = replaceAll(navKwd," ","-");
	var country1 = 'united+kingdom';
	var country2 = 'united-kingdom';
var newUrl2 = "";
  var newUrl1 = "/"+qualDesc+"-courses/search";
  if(clearFlag == 'ON'){
    if(clearingSearch){      
      newUrl2 = "?clearing&q="+navKwd2;
    }else{      
      newUrl2 = "?q="+navKwd2;
    }
  }else{  
    newUrl2 = "?q="+navKwd2;
  }  
  var finalUrl = newUrl1 + newUrl2;
if($$("clSearch_url").value != "" ||  $$("clmatchbrowsenode").value != ""){
    finalUrl = $$("clSearch_url").value != "" ? $$("clSearch_url").value : $$("clmatchbrowsenode").value;
  }
	$$("clearingSearchForm").action = finalUrl.toLowerCase();
	$$("clearingSearchForm").submit();
	return false
}
//
function clearNavSearchText(curid){
	var ids=curid.id;
	if($$(ids).value == kwDef){$$(ids).value = "";}
	$$(ids).value = "";
	$$('matchbrowsenode').value = "";
	$$('keyword_url').value = "";
}

function clearNavSearchTextCL(curid){
	 var ids=curid.id;
	 if($$(ids).value == kwDef){$$(ids).value = "";}
  $$(ids).value = "";
  $$('clSearch_hidden').value = "";
  $$('clSearch_id').value = "";
  $$('clSearch_url').value = "";
  $$('clSearch_display').value = "";
	 $$('clmatchbrowsenode').value = "";
}

function clearNavSearchTextCS(curid){
	 var ids=curid.id;
	 if($$(ids).value == kwDef){$$(ids).value = "";}
  $$(ids).value = "";
  $$('csKeyword_hidden').value = "";
  $$('csKeyword_id').value = "";
  $$('csKeyword_url').value = "";
  $$('csKeyword_display').value = "";
	 $$('csmatchbrowsenode').value = "";
}

//
function setNavSearchText(curid){
	var ids = curid.id;
  var navQual = trimString($$('navQual').value); 
	if($$(ids).value == ''){
    if(navQual=='UCAS CODE'){
      $$(ids).value = ucasDef;
    }else{
      $$(ids).value = kwDef;
    }
  }
}

function setNavSearchTextCL(curid){
	var ids = curid.id;
	if($$(ids).value == ''){$$(ids).value = clKw};
}
//
function searchTextValidate(name) {
	var specialChar = "\\,#%;?/_&*!@$^()+={}[]|':<>.~`";   
	if(name.indexOf('"') != -1){return false;}
	for(var i= 0; i<specialChar.length; i++){  
	var str = (name).indexOf(specialChar.charAt(i));
	if(str != -1) {return false;}}return true;
}
//
function showHideBestCourses(parentId, childList){
	var parentClass = $$(parentId).className;	
	if(parentClass.indexOf('close') >= 0){	
		parentClass = parentClass.replace("close","open");
		$$(parentId).className = parentClass;
		$$(childList).style.display = "block";
	}else if(parentClass.indexOf('open') >= 0){
		parentClass = parentClass.replace("open","close");
		$$(parentId).className = parentClass;
		$$(childList).style.display = "none";
	}
}
//
function toggleRefine(id){	
	/*
	if($$('subFilterList') != null)$$('subFilterList').style.display = 'none'
	if($$('quaFilterList') != null)$$('quaFilterList').style.display = 'none'	
	if($$('smFilterList') != null)$$('smFilterList').style.display = 'none'
	if($$('locFilterList') != null)$$('locFilterList').style.display = 'none'
	if($$('insFilterList') != null)$$('insFilterList').style.display = 'none'	
	$$(id).style.display = 'block'
	*/
	if(id == 'subFilterList'){
		if($$('quaFilterList') != null)$$('quaFilterList').style.display = 'none'	
		if($$('smFilterList') != null)$$('smFilterList').style.display = 'none'
		if($$('locFilterList') != null)$$('locFilterList').style.display = 'none'
		if($$('insFilterList') != null)$$('insFilterList').style.display = 'none'	
		if($$(id).style.display == 'none'){
			$$(id).style.display = 'block'
		} else {
			$$(id).style.display = 'none'
		}
	}
		if(id == 'quaFilterList'){
		if($$('subFilterList') != null)$$('subFilterList').style.display = 'none'	
		if($$('smFilterList') != null)$$('smFilterList').style.display = 'none'
		if($$('locFilterList') != null)$$('locFilterList').style.display = 'none'
		if($$('insFilterList') != null)$$('insFilterList').style.display = 'none'	
		if($$(id).style.display == 'none'){
			$$(id).style.display = 'block'
		} else {
			$$(id).style.display = 'none'
		}
	}
		if(id == 'smFilterList'){
		if($$('quaFilterList') != null)$$('quaFilterList').style.display = 'none'	
		if($$('subFilterList') != null)$$('subFilterList').style.display = 'none'
		if($$('locFilterList') != null)$$('locFilterList').style.display = 'none'
		if($$('insFilterList') != null)$$('insFilterList').style.display = 'none'	
		if($$(id).style.display == 'none'){
			$$(id).style.display = 'block'
		} else {
			$$(id).style.display = 'none'		}
	}
		if(id == 'locFilterList'){

		if($$('quaFilterList') != null)$$('quaFilterList').style.display = 'none'	
		if($$('smFilterList') != null)$$('smFilterList').style.display = 'none'
		if($$('subFilterList') != null)$$('subFilterList').style.display = 'none'
		if($$('insFilterList') != null)$$('insFilterList').style.display = 'none'	
		if($$(id).style.display == 'none'){
			$$(id).style.display = 'block'
		} else {
			$$(id).style.display = 'none'
		}
	}
		if(id == 'insFilterList'){
		if($$('quaFilterList') != null)$$('quaFilterList').style.display = 'none'	
		if($$('smFilterList') != null)$$('smFilterList').style.display = 'none'
		if($$('locFilterList') != null)$$('locFilterList').style.display = 'none'
		if($$('subFilterList') != null)$$('subFilterList').style.display = 'none'	
		if($$(id).style.display == 'none'){
			$$(id).style.display = 'block'
		} else {
			$$(id).style.display = 'none'
		}
	}
}
//
function hideBlocks(){
$$('subFilterList').style.display = 'none'
$$('quaFilterList').style.display = 'none'	
$$('smFilterList').style.display = 'none'
$$('locFilterList').style.display = 'none'
$$('insFilterList').style.display = 'none'
}

function prospectusFilter(obj, type, url, networkid){
    var ftype = type;
    var requestedUrl = url;
    var networkId = networkid;
    var urlData = requestedUrl.split("/");
    var urlDataLength = urlData.length;
    if(ftype == 'QUAL'){
        var qual = obj;
        if(qual == 'L'){
            qual = 3;
        }else{
            qual = 2;
        }
        if(urlDataLength > 4){
            var urlString = urlData[1]+"/"+urlData[2]+"/"+qual+"/"+urlData[4]+"/"+urlData[5]+"/"+urlData[6]+"/"+urlData[7]+"/"+urlData[8];
        }else{
            var urlString = urlData[1]+"/"+urlData[2]+"/"+qual+"/"+0+"/"+"r"+"/"+0+"/"+1+"/"+urlData[3];
        }
    }else if(ftype == 'REGION'){
        var regioname = obj.value;
        if(urlDataLength > 4){
            var urlString = urlData[1]+"/"+urlData[2]+"/"+urlData[3]+"/"+replaceAll(regioname," ","+")+"/"+urlData[5]+"/"+urlData[6]+"/"+urlData[7]+"/"+urlData[8];
        }else{
            var urlString = urlData[1]+"/"+urlData[2]+"/"+networkId+"/"+replaceAll(regioname," ","+")+"/"+"r"+"/"+0+"/"+1+"/"+urlData[3];
        }
    }else if(ftype == 'RATING'){
        var rating = obj.value;
        if(urlDataLength > 4){
            var urlString = urlData[1]+"/"+urlData[2]+"/"+urlData[3]+"/"+urlData[4]+"/"+rating+"/"+urlData[6]+"/"+urlData[7]+"/"+urlData[8];
        }else{
            var urlString=  urlData[1]+"/"+urlData[2]+"/"+networkId+"/"+0+"/"+rating+"/"+0+"/"+1+"/"+urlData[3];
        }
    }else if(ftype == 'UNI'){
      	var	uniId = trimString($$((obj.id+"_hidden")).value)
        var uniName = trimString(obj.value)
        var	uniNameId = trimString($$((obj.id+"_id")).value)
        var	uniNameDisplay = trimString($$((obj.id+"_display")).value)
        var	uniNameAlias = trimString($$((obj.id+"_alias")).value)
        var	uniNameUrl = trimString($$((obj.id+"_url")).value)

        if(subUni != uniNameUrl || subUni1 != uniNameUrl){
          if(uniNameUrl == '' || uniId == ''){
            alert(empUni);
            resetUniNAME(o);
            return false;
          }
        }else{
          alert(empUni);
          resetUniNAME(o);
          return false;
        }

        if(urlDataLength > 4){
            var urlString = urlData[1]+"/"+urlData[2]+"/"+urlData[3]+"/"+urlData[4]+"/"+urlData[5]+"/"+uniId+"/"+urlData[7]+"/"+urlData[8];
        }else{
            var urlString=  urlData[1]+"/"+urlData[2]+"/"+networkId+"/"+0+"/"+"r"+"/"+uniId+"/"+1+"/"+urlData[3];
        }
    }
    var finalUrl = "/degrees/" + urlString;
    location.href = finalUrl.toLowerCase();
}



//wu413_20120710 added by sekhar_k, for university search with in the search results.
function uniSearchInResults(o,formObjId, indexOfUrl){
//alert("---formObjId------------  "+formObjId);
	var uniName = trimString(o.value);
	var	uniId = trimString($$((o.id+"_hidden")).value);
	var	uniNameId = trimString($$((o.id+"_id")).value);
	var	uniNameDisplay = trimString($$((o.id+"_display")).value);
	var	uniNameAlias = trimString($$((o.id+"_alias")).value);
	var	uniNameUrl = trimString($$((o.id+"_url")).value);
	//
	//alert(" 0) redirectUniOPENDAYS: \n  1) uniName: " + uniName + "\n 2) uniId: " + uniId+ "\n 3) uniNameId: " + uniNameId+ "\n 4) uniNameDisplay: " + uniNameDisplay + "\n 5) uniNameAlias: " + uniNameAlias + "\n 6) uniNameUrl: " + uniNameUrl);
	//
	if(subUni != uniNameUrl || subUni1 != uniNameUrl){
		if(uniNameUrl == '' || uniId == ''){
			alert(empUni);
			resetUniNAME(o);
			return false;
		}
	}else{
		alert(empUni);
		resetUniNAME(o);
		return false;
	}
  var contextPath=$$("contextPath").value
  var beforeCollege = "";
  var afterCollege = "";
  var final_url = "";
  var searchUrl = $$("sortingUrl").value;
  var dataArray = searchUrl.split("/");
  var urlLength = dataArray.length;
  var searchtype = $$("searchtype").value;
  var tailURL = "";
  if(searchtype == 'NORMAL_SEARCH'){
    if(indexOfUrl != -1){
      tailURL = "page.html";
    }else{
      tailURL = "uniview.html";
    }
  }else{
    if(indexOfUrl != -1){
      tailURL = "clcourse.html";
    }else{
      tailURL = "cluniview.html";
    }
  }  
  if(urlLength > 9){
      for(var i = 1; i < urlLength-7; i++){
          beforeCollege = beforeCollege + dataArray[i] +"/";
      }
      for(var i = 14; i < urlLength-1; i++){
          afterCollege = afterCollege + dataArray[i] +"/";
      }
      //afterCollege = afterCollege + dataArray[19];
      //alert("-beforeCollege---------------"+beforeCollege);
      //alert("----afterCollege------------  "+afterCollege);
        final_url=contextPath+"/"+beforeCollege+uniId+"/"+afterCollege+tailURL;
  }else{
      var seo_qual=''
	    var qual=$$("paramStudyLevelId").value;
            switch(qual){
                    case 'M': seo_qual='degree';break
                    case 'N': seo_qual='hnd-hnc';break
                    case 'A': seo_qual='foundation-degree';break
                    case 'T': seo_qual='access-foundation';break
                    case 'L': seo_qual='postgraduate';break
            }
            var sort_by="R"
            var contextPath=$$("contextPath").value
            var url_1=seo_qual+'-courses'
            var url_2=replaceHypen(replaceURL($$("subjectname").value))+"-"+seo_qual+'-courses-'+(replaceHypen(replaceURL($$("paramlocationValue").value)))
            var url_3=replaceHypen(replaceURL($$("subjectname").value))
            var url_4=qual
            var url_5=replaceAll($$("paramlocationValue").value," ","+")
            var url_6=replaceAll($$("paramlocationValue").value," ","+")
            var url_7='25'
            var url_8='0'
            var url_9='0'
            var url_10='0';
            var url_11=sort_by
            var url_12=uniId
            var url_13='1'
            var url_14='0'
            var url_15= $$("dropdownCollegeId").value;
            var url_16 = '0';
            var url_17= '0';
            //var url_18 = dataArray[8];
              final_url=contextPath+"/"+'courses'+"/"+url_1+"/"+url_2+"/"+url_3+"/"+url_4+"/"+url_5+"/"+url_6+"/"+url_7+"/"+url_8+"/"+url_9+"/"+url_10+"/"+url_11+"/"+url_12+"/"+url_13+"/"+url_14+"/"+url_15+"/"+url_16+"/"+url_17+"/"+tailURL;
  }
  //alert("----final_url---------------  "+final_url);
	document.forms[0].action = final_url.toLowerCase();
	document.forms[0].submit();
	return false;
}

//wu416_20120918 added by sekhar_k, for removing filters.
function removeUniversityFilter(indexOfUrl){
  var contextPath=$$("contextPath").value
  var beforeCollege = "";
  var afterCollege = "";
  var final_url = "";
  var searchUrl = $$("sortingUrl").value;
  //alert("-----------searchUrl-------------  "+searchUrl);
  var dataArray = searchUrl.split("/");
  //alert("-------------dataArray-------------  "+dataArray);
  var urlLength = dataArray.length;
  //alert("------------urlLength----------  "+urlLength);
      for(var i = 1; i < urlLength-7; i++){
          beforeCollege = beforeCollege + dataArray[i] +"/";
      }
      for(var i = 14; i < urlLength-1; i++){
          afterCollege = afterCollege + dataArray[i] +"/";
      }
      //afterCollege = afterCollege + dataArray[19];
      //alert("-beforeCollege---------------"+beforeCollege);
      //alert("----afterCollege------------  "+afterCollege);
      var searchtype = $$("searchtype").value;
      var tailURL = "";
      if(searchtype == 'NORMAL_SEARCH'){
        if(indexOfUrl != -1){
          tailURL = "page.html";
        }else{
          tailURL = "uniview.html";
        }
      }else{
        if(indexOfUrl != -1){
          tailURL = "clcourse.html";
        }else{
          tailURL = "cluniview.html";
        }
      }  
      final_url=contextPath+"/"+beforeCollege+'0'+"/"+afterCollege+tailURL;
  //alert("----final_url---------------  "+final_url);
	document.forms[0].action = final_url.toLowerCase();
	document.forms[0].submit();
	return false;
}

function removeGradeFilter(indexOfUrl){
  var contextPath=$$("contextPath").value
  var beforeCollege = "";
  var afterCollege = "";
  var final_url = "";
  var searchUrl = $$("sortingUrl").value;
  //alert("-----------searchUrl-------------  "+searchUrl);
  var dataArray = searchUrl.split("/");
  //alert("-------------dataArray-------------  "+dataArray);
  var urlLength = dataArray.length;
  //alert("------------urlLength----------  "+urlLength);
      for(var i = 1; i < urlLength-3; i++){
          beforeCollege = beforeCollege + dataArray[i] +"/";
      }
      //afterCollege = afterCollege + dataArray[19];
      //alert("-beforeCollege---------------"+beforeCollege);
      //alert("----afterCollege------------  "+afterCollege);
      var searchtype = $$("searchtype").value;
      var tailURL = "";
      if(searchtype == 'NORMAL_SEARCH'){
        if(indexOfUrl != -1){
          tailURL = "page.html";
        }else{
          tailURL = "uniview.html";
        }
      }else{
        if(indexOfUrl != -1){
          tailURL = "clcourse.html";
        }else{
          tailURL = "cluniview.html";
        }
      }  
     final_url=contextPath+"/"+beforeCollege+'0'+"/"+'0'+"/"+tailURL;
  //alert("----final_url---------------  "+final_url);
	document.forms[0].action = final_url.toLowerCase();
	document.forms[0].submit();
	return false;
}


function studyLevelOnclick(collegeId, courseId, studyLevel, opportunityId){
  
  var courseId = courseId;
  var studyLevel = studyLevel;
  var pageName = document.getElementById("pagename").value;
  var url="/degrees/loadKISDataAjax.html?courseid="+courseId+"&studylevel="+studyLevel+"&collegeid="+collegeId+"&oppid="+opportunityId+"&ctype="+pageName;
  //alert("url--------"+url)
  var ajax=new sack();
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ courseKISData(ajax); };	
   ajax.runAJAX();
 }
 
function courseKISData(ajax){
	if(ajax.response != ''){
		$$('kisdata').innerHTML= ajax.response;
    if($$('gscript')){eval($$('gscript').innerHTML);}
    if($$('lfd')){eval($$('lfd').innerHTML);}
    if($$('indl')){eval($$('indl').innerHTML);}
	}else{
	}  
}
//

function addOpendays(opendate, openyear, collegeId, eventId, divId, popDivId, loadImagId, indexId){
  var url="/degrees/addmyopendays.html?collegeId= " + collegeId + "&opendate=" + opendate + "&openyear=" + openyear + "&eventid=" + eventId;
  var ajax=new sack();
  var loadImage = '<img src="'+wu_scheme+document.domain+'/wu-cont/images/ldr.gif"/>';//Added wu_scheme by Indumathi.S Mar-29-2016
  $$(loadImagId).innerHTML = loadImage;
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ opendaysResponse(ajax, opendate, openyear, divId, popDivId, loadImagId, indexId); };	
   ajax.runAJAX();
}
function opendaysResponse(ajax, opendate, openyear, divId, popDivId, loadImagId, indexId){
    var confirmMsg = " open day on " + opendate + " " + openyear + " has been added to your MyWhatUni calendar";
    var returnStatus = new Array();
    if(ajax.response != null){
      returnStatus = ajax.response.split("##SPLIT##");
    }
    $$(loadImagId).style.display = 'none';
    if(returnStatus != null && (returnStatus[0] == "SUCCESS")){
      var opendayalertText = returnStatus[2] + "##SPLIT##" + returnStatus[3] + "##SPLIT##" + returnStatus[1];
      $$(divId).style.display = 'none';
      $$(popDivId).style.display = 'block';
      var dateDiv = "date_div_"+indexId;
      var dateDiv1 = "date_p_"+indexId;
      if($$(dateDiv)){$$(dateDiv).style.display = 'none';}
      if($$(dateDiv1)){$$(dateDiv1).style.display = 'block';}
      showLightBoxLoginForm('grade-range-popup',650,500, 'OPENDAY-SUCESS-ALERT', opendayalertText);
      //alert(returnStatus[1] + " " + returnStatus[2]);
    } else if(returnStatus[0] == "ISEXISTS"){
      showLightBoxLoginForm('grade-range-popup',650,500, 'OPENDAY-EXIST-ALERT');
      //alert("This opendays already add to your MyWhatuni calendar");
    }
}
//
function addOpendaystocalendar(opendate, openyear, collegeId, eventId, from, suborderItemId, qualId, indexId, param, webPrice, bookingUrl){
  var url="/degrees/addmyopendays.html?collegeId=" + collegeId + "&opendate=" + opendate + "&openyear=" + openyear + "&eventid=" + eventId +"&param="+param;
  var ajax=new sack();
  var loadImage = '<img src="'+wu_scheme+document.domain+'/wu-cont/images/ldr.gif"/>';//Added wu_scheme by Indumathi.S Mar-29-2016
  //$$(loadImagId).innerHTML = loadImage;
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ opendaystocalendarResponse(ajax, collegeId, opendate, openyear,from, suborderItemId, qualId, indexId, webPrice, bookingUrl); };	
   ajax.runAJAX();
}
function opendaystocalendarResponse(ajax, collegeId, opendate, openyear,from, suborderItemId, qualId, indexId, webPrice, bookingUrl){
    var confirmMsg = " open day on " + opendate + " " + openyear + " has been added to your MyWhatUni calendar";
    var returnStatus = new Array();
    if(ajax.response != null){
      returnStatus = ajax.response.split("##SPLIT##");
    }
    if(returnStatus != null && (returnStatus[0] == "SUCCESS")){
      GAEventTracking('open-days', 'save-to-calendar',replaceSpecialCharacter(returnStatus[2]));
      //Added bookingUrl and webPrice for logging reserve place button by Prabha on 03_07_2018
      var opendayalertText = returnStatus[2] + "#" + returnStatus[3] + "#" + returnStatus[1] + "#" + collegeId + "#" + opendate + "#" +  openyear + "#" + suborderItemId + "#" + qualId + "#" +  webPrice + "#" + bookingUrl; //CollegeId added for forming the url by Prabha 31_may_2016
      document.getElementById("calBtnAct_"+indexId).style.display = 'block';
      document.getElementById("calBtn_"+indexId).style.display = 'none';
      cpeODAddToCalendar(collegeId, suborderItemId, qualId);
      closeOpenDaydRes();
      showLightBoxLoginForm('openday-popup',650,500, 'OPENDAY-SUCESS-ALERT', opendayalertText, from,indexId);
      
    } else if(returnStatus[0] == "ISEXISTS"){
      showLightBoxLoginForm('openday-popup',650,500, 'OPENDAY-EXIST-ALERT','',from,indexId);
    }
}

function dropdownPrepopulate(stringVal){
  var splitVal = stringVal.split("#");
  var length = splitVal.length;
  for(var i = 0; i < length; i++){
    var selectId = splitVal[i]+"_select";
    var spanId = splitVal[i]+"_span";
    if (document.getElementById(selectId)){
      if(document.getElementById(selectId).value != '0'){
        var el = document.getElementById(selectId);
        var text = el.options[el.selectedIndex].text;
        document.getElementById(spanId).innerHTML = text;
      }
    }
  }
}

function createGradesSelect(gradeType, count){
  if(gradeType == "A-levels"){
    var optString = "";
    var selBoxAStar = document.getElementById("selBoxA*");
    var selBoxA = document.getElementById("selBoxA");
    var selBoxB = document.getElementById("selBoxB");
    var selBoxC = document.getElementById("selBoxC");
    var selBoxD = document.getElementById("selBoxD");
    var selBoxE = document.getElementById("selBoxE");
  
    $$('selBoxA*').innerHTML = "";
    $$('selBoxA').innerHTML = "";
    $$('selBoxB').innerHTML = "";
    $$('selBoxC').innerHTML = "";
    $$('selBoxD').innerHTML = "";
    $$('selBoxE').innerHTML = "";
    for(var i = 0; i<=count; i++){
        var opt = document.createElement("option");
        opt.value=""+i;
        opt.appendChild(document.createTextNode(i));
        selBoxAStar.appendChild(opt);
        var opt1 = document.createElement("option");
        opt1.value=""+i;
        opt1.appendChild(document.createTextNode(i));
        selBoxA.appendChild(opt1);
        var opt2 = document.createElement("option");
        opt2.value=""+i;
        opt2.appendChild(document.createTextNode(i));
        selBoxB.appendChild(opt2);
        var opt3 = document.createElement("option");
        opt3.value=""+i;
        opt3.appendChild(document.createTextNode(i));
        selBoxC.appendChild(opt3);
        var opt4 = document.createElement("option");
        opt4.value=""+i;
        opt4.appendChild(document.createTextNode(i));
        selBoxD.appendChild(opt4);
        var opt5 = document.createElement("option");
        opt5.value=""+i;
        opt5.appendChild(document.createTextNode(i));
        selBoxE.appendChild(opt5);
    }
      $$('sBoxA*').innerHTML = '0';
      $$('sBoxA').innerHTML = '0';
      $$('sBoxB').innerHTML = '0';
      $$('sBoxC').innerHTML = '0';
      $$('sBoxD').innerHTML = '0';
      $$('sBoxE').innerHTML = '0';
  }else if(gradeType == "SQA Highers" || gradeType == "SQA Advanced Highers"){
    var optString = "";
    var selBoxA = document.getElementById("selBoxA");
    var selBoxB = document.getElementById("selBoxB");
    var selBoxC = document.getElementById("selBoxC");
    var selBoxD = document.getElementById("selBoxD");
    $$('selBoxA').innerHTML = "";
    $$('selBoxB').innerHTML = "";
    $$('selBoxC').innerHTML = "";
    $$('selBoxD').innerHTML = "";
    for(var i = 0; i<=count; i++){
        var chopt = document.createElement("option");
        chopt.value=""+i;
        chopt.appendChild(document.createTextNode(i));
        var chopt1 = document.createElement("option");
        chopt1.value=""+i;
        chopt1.appendChild(document.createTextNode(i));
        var chopt2 = document.createElement("option");
        chopt2.value=""+i;
        chopt2.appendChild(document.createTextNode(i));
        var chopt3 = document.createElement("option");
        chopt3.value=""+i;
        chopt3.appendChild(document.createTextNode(i));
        selBoxA.appendChild(chopt);
        selBoxB.appendChild(chopt1);
        selBoxC.appendChild(chopt2);
        selBoxD.appendChild(chopt3);
    }
      $$('sBoxA').innerHTML = '0';
      $$('sBoxB').innerHTML = '0';
      $$('sBoxC').innerHTML = '0';
      $$('sBoxD').innerHTML = '0';
    } else if(gradeType == "BTEC"){
    var optString = "";
    var selBoxDStar = document.getElementById("selBoxD*");
    var selBoxBtecD = document.getElementById("selBoxBtecD");
    var selBoxM = document.getElementById("selBoxM");
    var selBoxP = document.getElementById("selBoxP");
      
    $$('selBoxD*').innerHTML = "";
    $$('selBoxBtecD').innerHTML = "";
    $$('selBoxM').innerHTML = "";
    $$('selBoxP').innerHTML = "";
    
    for(var i = 0; i<=count; i++){
        var opt = document.createElement("option");
        opt.value=""+i;
        opt.appendChild(document.createTextNode(i));
        selBoxDStar.appendChild(opt);
        var opt1 = document.createElement("option");
        opt1.value=""+i;
        opt1.appendChild(document.createTextNode(i));
        selBoxBtecD.appendChild(opt1);
        var opt2 = document.createElement("option");
        opt2.value=""+i;
        opt2.appendChild(document.createTextNode(i));
        selBoxM.appendChild(opt2);
        var opt3 = document.createElement("option");
        opt3.value=""+i;
        opt3.appendChild(document.createTextNode(i));
        selBoxP.appendChild(opt3);     
    }
    $$('sBoxD*').innerHTML = '0';
    $$('sBoxBtecD').innerHTML = '0';
    $$('sBoxM').innerHTML = '0';
    $$('sBoxP').innerHTML = '0';      
  }
}

function showShareButtons(){
  var $zz = jQuery.noConflict();
  if(document.getElementById("socialMediaPod")){
    if($$("socialMediaPod")!= null  && $$('socialMediaPod').innerHTML != ""){
      if ($$("socialMediaPod")!= null &&   $$("socialMediaPod").style.display=="none") {
        $$("socialMediaPod").style.display="block";
      }else{
        $$("socialMediaPod").style.display="none";
      }
    }else{
      $zz('#socialMediaPod').load("/degrees/jsp/thirdpartytools/showShareButtonPod.jsp");
      $$("socialMediaPod").style.display="block";
    }
  }
}

function autoCompleteKeywordBrowse(event,object){
  var navQual = trimString($$('navQual').value); 
  var qualDesc = 'degree';
  var qualCode = "M";
  if(navQual == 'Degrees'){
    qualDesc = 'degree'; qualCode='M';
  }else if(navQual == 'Postgraduate'){
    qualDesc = 'postgraduate'; qualCode='L';
  }else if(navQual == 'Foundation degree'){
    qualDesc = 'foundation-degree'; qualCode='A';
  }else if(navQual == 'Access & foundation'){
    qualDesc = 'access-foundation'; qualCode='T';
  }else if(navQual == 'HND / HNC'){
    qualDesc = 'hnd-hnc'; qualCode='N';
  }else if(navQual == 'UCAS CODE'){
    qualDesc = 'ucas_code'; qualCode='UCAS_CODE';
  }
  $$('selQual').value = navQual;
  if($$('ajax_listOfOptions')){
    $$('ajax_listOfOptions').className="";
  }
	 ajax_showOptions(object,'keywordbrowse',event,"indicator",'qual='+ qualCode +'&siteflag=desktop', '0', '385');  
}

function autoCompleteMobKeywordBrowse(event,object){
  var navQual = trimString($$('navQual').value); 
  var qualDesc = 'degree';
  var qualCode = "M";
  if(navQual == 'Degrees'){
    qualDesc = 'degree'; qualCode='M';
  }else if(navQual == 'Postgraduate'){
    qualDesc = 'postgraduate'; qualCode='L';
  }else if(navQual == 'Foundation degree'){
    qualDesc = 'foundation-degree'; qualCode='A';
  }else if(navQual == 'Access & foundation'){
    qualDesc = 'access-foundation'; qualCode='T';
  }else if(navQual == 'HND / HNC'){
    qualDesc = 'hnd-hnc'; qualCode='N';
  }else if(navQual == 'UCAS CODE'){
    qualDesc = 'ucas_code'; qualCode='UCAS_CODE';
  }
  $$('selQual').value = navQual;
  if($$('ajax_listOfOptions')){
    $$('ajax_listOfOptions').className="";
  }
	 ajax_showOptions(object,'mobkeywordbrowse',event,"indicator",'qual='+ qualCode +'&siteflag=desktop', '0', 0);  
}

function autoCompleteProspectusBrowse(event,object, level){
  var navQual = level; 
  var qualDesc = 'degree';
  var qualCode = "M";
  if(level == 'M' || level == 'm' ){
    qualDesc = 'degree'; qualCode='M';
  }else if(level == 'L' || level == 'l'){
    qualDesc = 'postgraduate'; qualCode='L';
  }
	ajax_showOptions(object,'prospectusbrowse',event,"indicator",'qual='+ qualCode, '0', '385');
}

function pageImpressionLog(id, pageName){
  var url="/degrees/pageimpression-jslog.html?id= " + id + "&pagename=" + pageName;
  var ajax=new sack();
   ajax.requestFile = url;	
   ajax.onCompletion = function(){};	
   ajax.runAJAX();
}

function advertiserExternalUrlStatsLogging(logType, z, url){      
  var url = "/degrees/advertiser-external-url-stats-logging.html?logType="+logType+"&z="+z+"&url="+encodeURIComponent(url);
  var ajax = new sack();
  ajax.requestFile = url;
  ajax.runAJAX();
}

function kisexanpd(obj, rowNum){  
  var id = obj.id;
  if(id == 'more_'+rowNum){
    $$('expandtable_'+rowNum).style.display = 'block';
    $$('collapsetable_'+rowNum).style.display = 'none';
  }else if(id == 'less_'+rowNum){
    $$('collapsetable_'+rowNum).style.display = 'block';
    $$('expandtable_'+rowNum).style.display = 'none';
  }
} 
function showHideDiv(divId, paraId, arrowId){  
  var divObj = $$(divId);
  var paraObj = $$(paraId);
  var arrowId = $$(arrowId);
  if(divObj){
    if(divObj.style.display == 'none'){       
      divObj.style.display = 'block';
      paraObj.className = 'yqhead active';
      arrowId.className = 'd-arw';
    }else if(divObj.style.display == 'block' || divObj.style.display == ''){
      divObj.style.display = 'none';
      paraObj.className = 'yqhead';
      arrowId.className = 'r-arw';
    }
  }
}
function showShare() {  
  $$('oshare').style.display="block";
}
function hideShare() {  
  $$('oshare').style.display="none";
}
function showhideloginreg(obj){
	var id = obj.id;
	if(id == 'log_head'){    
		$$('reg').style.display = 'none';
		$$('login').style.display = 'block';
		$$(id).className = 'lgn act';
		$$('reg_head').className = 'rgr';
	}else{    
		$$('reg').style.display = 'block';
		$$('login').style.display = 'none';
		$$(id).className = 'rgr act';
		$$('log_head').className = 'lgn';
	}
}
function setWrapperWidth(){  
 var res = screen.width;
 if($$("content-bg")){
     if(res == 1024){ 
        $$("content-bg").className = "wrp1";
     }
  }
}
function displayTickerTape(){    
  if($$("tickerTape")){
    $$("tickerTape").style.display="block"; 
  }
}
var s1=null;
function closeVideo(){  
  s1="";	
  $$('flashbanner').innerHTML ="";
  hm('box');
}
var adv = jQuery.noConflict();
function showVideo(videoid, collegeid, videoType){
  var filePath = document.getElementById("contextJsPath").value+'/js/video/jwplayer.js';
   if (adv('head script[src="' + filePath + '"]').length > 0){
   }else{
    dynamicLoadJS(filePath);
  }
  setTimeout(function(){showVideoAjax(videoid, collegeid, videoType)}, 2000);  
}
function showVideoAjax(videoid, collegeid, videoType){
   var offsetlft = "";
   if($$("theImages") !=null){
       offsetlft= "&offsetlft=0";
   }
   var contextPath = $$('contextPath').value;
   var ajax=new sack();
   var url = contextPath+'/vids-video.html?vrid='+videoid+"&cid="+collegeid+"&refererUrl="+$$("refererUrl").value+"&videoType="+videoType+offsetlft;   
   //alert(url);   
   ajax.requestFile = url;	
   ajax.onCompletion = function(){ showVideoWindow(ajax); };
   ajax.runAJAX();
}
function showVideoWindow(ajax){  
  var data=ajax.response.split("|BREAK|");	
  var contextPath = $$('contextPath').value;   
  if(!document.getElementById("mbox")){
    initmb();            
  }
  if (data[4] == 'CUSTOM'){sm('box', 590, 460);}else{sm('box', 590, 590);}      
	 jwplayerSetup('flashbanner', data[5], "<%=CommonUtil.getJsPath()%>/js/limelightvideoplayer/isight.gif", 590, 380, data[5]); //Jwplayer code added by Prabha on 27-JAN-16
  var linkobj =  $$('linksdiv');
  linkobj.innerHTML =data[20] + data[21];
  $$('linksdiv').style.display = "block";
  $$('slideshowcontainer').style.display = "none";  
}

function autoCompleteKeywordBrowseCL(event,object){//24_JUN_2014
  var navQual = trimString($$('navQual').value); 
  var qualDesc = 'degree';
  var qualCode = "M";
  if(navQual == 'Degrees'){
    qualDesc = 'degree'; qualCode='M';
  }else if(navQual == 'Postgraduate'){
    qualDesc = 'postgraduate'; qualCode='L';
  }else if(navQual == 'Foundation degree'){
    qualDesc = 'foundation-degree'; qualCode='A';
  }else if(navQual == 'Access & foundation'){
    qualDesc = 'access-foundation'; qualCode='T';
  }else if(navQual == 'HND / HNC'){
    qualDesc = 'hnd-hnc'; qualCode='N';
  }else if(navQual == 'UCAS CODE'){
    qualDesc = 'ucas_code'; qualCode='UCAS_CODE';    //Added by Sangeeth.S for Jul_3_18 rel
  }else if(navQual == $$("clearingYear").value || navQual == 'Clearing/Adjustment'){
    qualDesc = 'CLEARING'; qualCode='CLEARING';
  }
  $$('selQual').value = navQual;
	ajax_showOptions(object,'keywordbrowse',event,"indicator",'qual='+ qualCode +'&siteflag=desktop', '0', '385');
}
function autoCompleteMobKeywordBrowseCL(event,object){//24_JUN_2014
  var navQual = trimString($$('navMobQual').value); 
  var qualDesc = 'degree';
  var qualCode = "M";
  if(navQual == 'Degrees'){
    qualDesc = 'degree'; qualCode='M';
  }else if(navQual == 'Postgraduate'){
    qualDesc = 'postgraduate'; qualCode='L';
  }else if(navQual == 'Foundation degree'){
    qualDesc = 'foundation-degree'; qualCode='A';
  }else if(navQual == 'Access & foundation'){
    qualDesc = 'access-foundation'; qualCode='T';
  }else if(navQual == 'HND / HNC'){
    qualDesc = 'hnd-hnc'; qualCode='N';
  }else if(navQual == 'UCAS CODE'){
    qualDesc = 'ucas_code'; qualCode='UCAS_CODE';   //Added by Sangeeth.S for Jul_3_18 rel
  }else if(navQual == $$("clearingYear").value || navQual == 'Clearing/Adjustment'){
    qualDesc = 'CLEARING'; qualCode='CLEARING';
  }
  $$('selQual').value = navQual;
	ajax_showOptions(object,'mobkeywordbrowse',event,"indicator",'qual='+ qualCode +'&siteflag=desktop', '0', 0);
}
function autoCompleteKeywordBrowseClearingAlone(event,object){
  var qualDesc = 'CLEARING';
  var qualCode = "CLEARING";  
  $$('selQual').value = $$("clearingYear").value;
	 ajax_showOptions(object,'clkeywordbrowse',event,"indicator",'qual='+ qualCode +'&siteflag=desktop', '0', 0);
}
//17_Mar_2015 - Open day uni name and location search ajax call, by Thiyagu G.
function autoCompleteOpendaysKeywordBrowse(event,object){
    var search = jQuery.noConflict();
    ajax_showOptions(object,'opdkeywordbrowse',event,"opd_search_indicator",'siteflag=opddesktop', '0', '');
    var width = document.documentElement.clientWidth;
    var x = search("#ajaxSrchDivId").offset();
    ajax_optionDiv.style.top = x.top+search("#ajaxSrchDivId").height()+'px';
    ajax_optionDiv.style.left = x.left + 'px';
    if (width <= 992) {
      ajax_optionDiv.style.width = (ajax_getWidth(object)) + 'px';
      search('#ajax_listOfOptions').attr("class", "ajax_mob no_opds");
    }
}
var opdEmpKw =  "Please choose a uni from list";
function regionSearch(value){
    var finaUrl = "/open-days/search/?location="+value;    
    window.location.href = finaUrl.toLowerCase();
}
function replaceSpaceToPlus(inString){
if(inString !=null){
inString=trimString(inString)
inString=replaceAll(inString,"&","and")
inString=replaceAll(inString," ","+")
inString=replaceAll(inString,"-+","-")
inString=replaceAll(inString,"--","-")}
return inString}
function opendaysSearchSubmit(obj){
    var uniOpdSearch = obj.value;    
    if(uniOpdSearch == "" || uniOpdSearch.toLowerCase() == "enter uni name" || $$("uniOpenDayName_hidden").value == ""){
        //console.log(">>uniOpdSearch "+ uniOpdSearch + "hidden " + $$("uniOpenDayName_hidden").value);
        alert(opdEmpKw);
        if(uniOpdSearch == "" || uniOpdSearch.toLowerCase() == "enter uni name"){ //Added by sangeeth for Bug Fix 31_July_18 rel
          obj.value = '';
          obj.focus();
        }
        return false;
    } else{
      
        var finalUrl = $$("uniOpenDayName_url").value != "" ? $$("uniOpenDayName_url").value : $$("opdmatchbrowsenode").value;
        if(finalUrl != ""){
            $$("opdSearchForm").action = finalUrl.toLowerCase();            
            $$("opdSearchForm").submit();
            return true;    
        }
//        else{
//             if(uniOpdSearch != "" || uniOpdSearch.toLowerCase() != "enter uni name or location"){
//                 $$("opdSearchForm").action = "/open-days/search/?keyword="+replaceSpaceToPlus(trimString(uniOpdSearch)).toLowerCase();
//                 $$("opdSearchForm").submit();    
//                 return true;
//             }   
//        }
  
    }    
}
//17_Mar_2015 - show "Add to comparision" on mouse hover in SR and PR centre pod, by Thiyagu G.
function addToComparisonHover(cId, indx){
    var showId = "show_"+cId;
    var showId1 = "show1_"+cId;
    var percId = "perc_"+indx;
    if($$(showId) && !$$(percId)){
        $$(showId).style.display = "block";
    }else if($$(showId1) && !$$(percId)){
        $$(showId1).style.display = "block";
    }
}
function addToComparisonOut(cId, indx){
    var showId = "show_"+cId;
    var showId1 = "show1_"+cId;
    var percId = "perc_"+indx;
    if($$(showId) && !$$(percId)){
        $$(showId).style.display = "none";
    }else if($$(showId1) && !$$(percId)){
        $$(showId1).style.display = "none";
    }
}
//Added for opendays page redesign i'am going to pod and have been pod view more and less button by Hema.S on 25.06.2018
function viewMoreFod(){
  //$$('moreFod').style.display = "block";
  var elements = document.getElementsByClassName("ucod_Cnr moreViewInfo")
  for (var i = 0; i < elements.length; i++){
    elements[i].style.display = "flex";
  }
  lazyloadetStarts();
  $$('morebtn').style.display = "none";
  $$('lessbtn').style.display = "block";
}
function viewMorePod(){
  //$$('moreFod').style.display = "block";
  var elements = document.getElementsByClassName("ucod_Cnr moreViewInfohavebeenpod")
  for (var i = 0; i < elements.length; i++){
    elements[i].style.display = "flex";
  }
  lazyloadetStarts();
  $$('morebtnhavebeen').style.display = "none";
  $$('lessbtnhavebeen').style.display = "block";
}
function viewLessFod(divId){
 var elements = document.getElementsByClassName("ucod_Cnr moreViewInfo");
    for (var i = 0; i < elements.length; i++){
        elements[i].style.display = "none";
    }
  $$('morebtn').style.display = "block";
  $$('lessbtn').style.display = "none";
  skipLinkSetPosition(divId);
}
function viewLessPod(divId){
 var elements = document.getElementsByClassName("ucod_Cnr moreViewInfohavebeenpod")
    for (var i = 0; i < elements.length; i++){
        elements[i].style.display = "none";
    }
  $$('morebtnhavebeen').style.display = "block";
  $$('lessbtnhavebeen').style.display = "none";
  skipLinkSetPosition(divId);
  
}
function skipLinkSetPosition(divname){ //added scroll top method for view less button by Hema.S on 29.06.2018
  var divPosition = adv('#'+divname).offset();
  adv('html, body').animate({
      scrollTop: divPosition.top - 320
      }, "slow");
}
//Added for open days landing page Hero Image Script by Hema.S on 25.06.2018
function mobileSpecific(id){
var dev = jQuery.noConflict();
 if($$(id)) {
  var devWidth = adv(window).width() || document.documentElement.clientWidth;   
	var url = $$(id).getAttribute('src');
        var imageIndex =  url.lastIndexOf("/") + 1;
	var hrImg = url.substring(imageIndex).split('.');
  var orgUrl = url.substring(0, imageIndex) + hrImg[0];
   orgUrl = (orgUrl.indexOf('_480px') > -1) ? orgUrl.replace('_480px','') : orgUrl.indexOf('_992px') > -1 ? orgUrl.replace('_992px','') : orgUrl;
   if(devWidth <= 480 ){         
     url = orgUrl +'_480px.'+ hrImg[1] ;
   }
   if(devWidth >= 481 &&  devWidth <= 992){
     url = orgUrl +'_992px.' + hrImg[1];
   }
   else if(devWidth > 992){
    url = orgUrl +'.' +hrImg[1];
   }
  $$(id).src = url;
 }
}
//Added for remove special character in ajax GA logging by Hema.S on 29.06.2018
function replceSpecialcharacter(inputString){
  inputString = inputString.replace("'"," ");
  return inputString;
}
var $mhr = jQuery.noConflict();
$mhr(document).ready(function(){  
mobileSpecific("openDayHeroImage");
  var deviceAgent = navigator.userAgent.toLowerCase();
  var agentID = deviceAgent.match(/(ipad)/);  
  if (agentID) {    
    $mhr("div[class^='dtls']").removeAttr('onmouseover').removeAttr('onmouseout');
  }
});