/**
 * Author:	Thiyagu G
 * Date:	wu544_20150721
 * Purpose:	functions related to course search related pages
 * Version:	1.0
**/
var adv = jQuery.noConflict();
function $$(id){ return document.getElementById(id); }
function blockNoneDiv(thisid, value){  if($$D(thisid) !=null){ $$D(thisid).style.display = value;  }  }
adv(window).scroll(function(){ // scroll event 
var wid = document.documentElement.clientWidth;
  if(wid > 992 && $$('ajax_listOfOptions')){
    $$('ajax_listOfOptions').style.display="none";
  }
  var windowTop = adv(window).scrollTop(); // returns number    
  var width = document.documentElement.clientWidth;
  if (width > 992) {
    if (180 < windowTop){
      adv('#scrollfreezediv').addClass('pros_fixed');
      adv('#scrollfreezediv').css({ position: 'fixed', top: 0 });
      adv('#ajax_listOfOptions').css('top', '406px');
    }
    else {
      adv('#scrollfreezediv').removeClass('pros_fixed');
      adv('#scrollfreezediv').css('position','static');    
    } 
  }
  lazyloadetStarts();
});
adv(document).ready(function(){
	adv( document ).on("click", "#showMoreSubjects", function(event) {
		adv("#moreSubjects").show();
		adv("#popularSubjectList").hide();
	});
	adv( document ).on("click", "#showLessSubjects", function(event) {
		adv("#popularSubjectList").show();
		adv("#moreSubjects").hide();
	});
	
  lazyloadetStarts();
});

function courseGALogging(eventLabel, podType) {
	if (podType == "subject") {
		GAEventTracking("Find A Course", "Popular Subjects", eventLabel);
	} 
	if (podType == "article") {
		ArticlesGAEventsLogging("Articles", "Choosing a course", eventLabel);
	}		
}

//
function toggleQualListCS(display){
	if($$('navQualListCS') && $$('navQualListCS').style.display == 'none'){
		blockNoneDiv('navQualListCS','block');
	} else {
		blockNoneDiv('navQualListCS','none');
	}	
	if(display != null || display != 'undefined'){
		$$('navQualListCS').style.display = display
	}
}
//
function setQualCS(o){
	$$('navQualCS').value = (o.innerHTML).replace(/&amp;/g, '&');
	blockNoneDiv('navQualListCS','none');
  var preQual = $$('selQualCS').value;
  if(preQual != o.innerHTML){
  var qualValue = $$('navQualCS').value;
  if(qualValue.trim()=='UCAS CODE'){
      $$('navKwdCS').value = ucasDef;
  }else{
      $$('navKwdCS').value = kwDef;
  }
    $$('csKeyword_hidden').value = "";
    $$('csKeyword_id').value = "";
    $$('csKeyword_display').value = "";
    $$('csKeyword_url').value = "";
    $$('csmatchbrowsenode').value = "";
  }
}
//
function csSearchSubmit(flag){
  var clearFlag = flag;
  var clearingYear = '';
  if($$('clearingYear')){
    clearingYear = $$('clearingYear').value;
  }
  if(checkCsKeyword()){
    if(clearFlag == 'ON'){
      var navQual = trimString($$('navQualCS').value); //24_JUN_2014
      if(navQual == clearingYear || navQual == 'Clearing/Adjustment'){
        updateUserTypeSession('','clearing','csSubmitSearch');        
      }else{
        updateUserTypeSession('','non-clearing','csSubmitSearch');        
      }
    }else{
      csSubmitSearch(false,flag)
    }
  }else{
    return false;
  }
}
//
function checkCsKeyword(){
  var navKwd = trimString($$('navKwdCS').value);
  var navQual = trimString($$('navQualCS').value);
  if(navKwd == kwDef || navKwd == '' || navKwd == ucasDef){
    alert(empKw);
    $$('navKwdCS').value = '';
    $$('navKwdCS').focus();
    return false;
  }
  if(navQual == qualDef){
    alert(empKw);
    $$('navQualCS').focus();
    return false;
  }
  return true;
}
//
function csSubmitSearch(confFlag,clearingFlag){
  var clearingSearch = confFlag;
  var clearFlag = clearingFlag;   
  var navKwd = trimString($$('navKwdCS').value);
  var navQual = trimString($$('navQualCS').value);   
  var qualDesc = 'degree';
  var qualCode = navQual;
  var qualCode = "M";
  var clearingYear = '';
  if($$('clearingYear')){
    clearingYear = $$('clearingYear').value;
  }
  if(navQual == 'Degrees' || navQual == 'Undergraduate'){
    qualDesc = 'degree'; qualCode='M';
  }else if(navQual == 'Postgraduate'){
    qualDesc = 'postgraduate'; qualCode='L';
  }else if(navQual == 'Foundation degree'){
    qualDesc = 'foundation-degree'; qualCode='A';
  }else if(navQual == 'Access & foundation'){
    qualDesc = 'access-foundation'; qualCode='T';
  }else if(navQual == 'HND / HNC'){
    qualDesc = 'hnd-hnc'; qualCode='N';
  }else if(navQual == 'UCAS CODE'){
    qualDesc = 'ucas_code'; qualCode='UCAS_CODE';
  } else if(navQual == clearingYear || navQual == 'Clearing/Adjustment'){ //24_JUN_2014
    qualDesc = 'degree'; qualCode='M';
  }
  var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
  navKwd = navKwd.replace(reg," ");
  if(navKwd == kwDef || navKwd == '' || navKwd == null || navKwd == ucasDef){
    alert(empKw);
    $$('navKwdCS').value = '';
    $$('navKwdCS').focus();
    return false;
  }  
  var navKwd1 = replaceAll(navKwd," ","+");
  var navKwd2 = replaceAll(navKwd," ","-");
  var country1 = 'united+kingdom';
  var country2 = 'united-kingdom';
  var newUrl2 = "";
  var newUrl1 = "/"+qualDesc+"-courses/search";
  if(clearFlag == 'ON'){
    if(clearingSearch){      
      newUrl2 = "?clearing&q="+navKwd2;
    }else{      
      newUrl2 = "?q="+navKwd2;
    }
  }else{  
    newUrl2 = "?q="+navKwd2;
  }  
  var finalUrl = newUrl1 + newUrl2;
  if($$("csKeyword_url").value != "" ||  $$("csmatchbrowsenode").value != ""){
    finalUrl = $$("csKeyword_url").value != "" ? $$("csKeyword_url").value : $$("csmatchbrowsenode").value;
  }
	$$("courseSearchForm").action = finalUrl.toLowerCase();
	$$("courseSearchForm").submit();
	return false;
}
//
function autoCompleteKeywordBrowseCS(event,object){//24_JUN_2014
  var navQual = trimString($$('navQualCS').value); 
  var qualDesc = 'degree';
  var qualCode = "M";
  var clearingYear = '';
  if($$('clearingYear')){
    clearingYear = $$('clearingYear').value;
  }
  if(navQual == 'Degrees' || navQual == 'Undergraduate'){
    qualDesc = 'degree'; qualCode='M';
  }else if(navQual == 'Postgraduate'){
    qualDesc = 'postgraduate'; qualCode='L';
  }else if(navQual == 'Foundation degree'){
    qualDesc = 'foundation-degree'; qualCode='A';
  }else if(navQual == 'Access & foundation' || navQual == 'Access & Foundation'){
    qualDesc = 'access-foundation'; qualCode='T';
  }else if(navQual == 'HND/HNC' || navQual == 'HND / HNC'){
    qualDesc = 'hnd-hnc'; qualCode='N';
  }else if(navQual == 'UCAS CODE'){
    qualDesc = 'ucas_code'; qualCode='UCAS_CODE';
  }else if(navQual == clearingYear || navQual == 'Clearing/Adjustment'){
    qualDesc = 'CLEARING'; qualCode='CLEARING';
  }
  $$('selQualCS').value = navQual;
  if($$('ajax_listOfOptions')){
    $$('ajax_listOfOptions').className="ajx_cnt ajx_res";
  }
	 ajax_showOptions(object,'csKeywordbrowse',event,"indicator",'qual='+ qualCode +'&siteflag=desktop', '0', 0);
}
function hideBlockCS(){
    $$('navQualListCS').style.display = 'none'
}
function podChangeToSearchAgain(results, entry){  
 var width = window.screen.width;   
 if(results == "iwtbResults"){
  openLightBox('iwanttobe');
 }//Modified by Indumathi.S for iwanttobe and whatcanido redesign Mar-08-16
 if(results == "wcidResults"){
  openLightBox('whatcanido');
 }
}
//For showing the ajax dropdown in sticky top Mar-08-16 Indumathi.S
function setCourseAutoCompleteClass(){
  if($$("scrollfreezediv")){
    if($$("scrollfreezediv").className == 'pros_fixed'){
      if($$('ajax_listOfOptions')){
        $$('ajax_listOfOptions').className="ajx_cnt ajx_res ajax_fixed";
      }
    }
  }
}