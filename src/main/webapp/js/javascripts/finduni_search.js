var $fp = jQuery.noConflict();
function $$D(id){return document.getElementById(id)}
function regionUniSearch(value){
   var finalUrl = contextPath + "/find-university.html?region="+value;    
   window.location.href = finalUrl.toLowerCase();
}
//For reloading the page while clicking on back button
$fp(document).ready(function() {
  var d = new Date();
  d = d.getTime();
  if ($$D('reloadValue') != null && $$D('reloadValue') != "") {
    if ($fp('#reloadValue').val().length == 0) {
      $fp('#reloadValue').val(d);
      $fp('body').show();
    } else {
      $fp('#reloadValue').val('');
      window.location.reload(true);
      $$D("searchTxt").value = "";
    }
  } 
  $fp(".atz ul li").each(function () {
    $fp(this).click(function () {
      $fp(".atz ul li").removeClass('active'); //Initially remove "selected" class if any
      $fp(this).addClass('active');
    });
  });
  autocompleteOnScroll();
  lazyloadetStarts();
  adjustStyle();
});
//To get the university list in ajax call
function getUniBrowserList(obj, searchTxt) {
  skipLinkPosition('uniList');
  blockNone("loader","block"); 
  $$D("region").value = "";
  blockNone("displayRegion","none");
  var ajax = new sack();
  url = contextPath + "/findUniversityInfoAjax.html?searchTxt="+searchTxt + "&ajaxFlag=Y"; 
  $$D("searchTxt").value = searchTxt;
  ajax.requestFile = url;
  ajax.onCompletion = function(){ loadUniBrowserListAjax(ajax, obj, searchTxt); };
  ajax.runAJAX();
}

function loadUniBrowserListAjax(ajax, obj, searchTxt){
  $$D("uniList").innerHTML = "";
  removeExistIds();
  blockNone("loader","none");
  var response = ajax.response;
  $$D("uniList").innerHTML = response;
  lazyloadetStarts();
  clearListClasses();
  var actId = "li_"+searchTxt;
  $$D(actId).className = "active";
  var pageurl = window.location.href;//To change the url
  var splitUrl = pageurl.split("?","1");
  var newUrl = splitUrl[0].concat("?view=az");
  window.history.pushState({path:newUrl},'',newUrl);
}
//To move the position under specified div
function skipLinkPosition(divname){  
  if($$D(divname)){    
    var divPosition = $fp('#'+divname).offset();  
    $fp('html, body').animate({
      scrollTop: divPosition.top - 320
    }, "slow");
  }  
}
//Clearing the class in ajax call
function clearListClasses(){
  var array_of_li =  document.querySelectorAll("ul li");
  var i = 0;
  for(i=0; i<array_of_li.length;i++){
    if(document.getElementsByTagName("LI")[i].className == "active"){
      document.getElementsByTagName("LI")[i].className = "";
    }
  }
}
//For showing the ajax dropdown in sticky top
function setUniAutoCompleteClass(){
  if($$D("fixedscrolltop")){
    if($$D("fixedscrolltop").className == 'sticky_top_nav'){
      if($$('ajax_listOfOptions')){
        $$('ajax_listOfOptions').className="ajax_fixed ajx_fix_funi";
      }
    }
  }
}

function autocompleteOnScroll(){ 
  $fp(window).scroll(function(){
    if($fp('#fixedscrolltop').hasClass('sticky_top_nav')){
      if($$D('ajax_listOfOptions')){
        blockNone("ajax_listOfOptions","none");
      }
    }
    //For ajax dropdown on scroll
    if ($fp(this).scrollTop() > 300) {
      var leftPos = ajax_getLeftPos($$('uniName'),0);
      if(document.documentElement.clientWidth > 992){
        $fp('#fixedscrolltop').addClass('sticky_top_nav');
        $fp('#fixedscrolltop').css({ position: 'fixed', top: 0 }); 
        $fp('#uniList').css('margin-top','250px');
      }
      if($fp('#fixedscrolltop').hasClass('sticky_top_nav')){
        $fp('#ajax_listOfOptions').addClass('ajax_fixed ajx_fix_funi');
      }
      if($$('ajax_listOfOptions')){
         $$('ajax_listOfOptions').style.left = leftPos;               
      }
    } else {
      if(document.documentElement.clientWidth > 992){
        $fp('#fixedscrolltop').removeClass('sticky_top_nav');
        $fp('#fixedscrolltop').css({ position: 'static',top: 0 });
        $fp('#uniList').css('margin-top','0px');
      }
      $fp('#ajax_listOfOptions').removeClass('ajax_fixed ajx_fix_funi');
    }
    //For sticky top
    if(document.documentElement.clientWidth <= 992){      
      if((document.documentElement.clientWidth > 480) && (document.documentElement.clientWidth <= 992)){
        if ($fp(this).scrollTop() > 150) {
           $fp('#mobilefixedscrolltop').addClass('sticky_top_nav');
           $fp('#mobilefixedscrolltop').css({ position: 'fixed', top: 0 });   
           $fp('#uniList').css('margin-top','100px');		
        }else{
           $fp("#mobilefixedscrolltop").removeClass("sticky_top_nav");
           $fp('#mobilefixedscrolltop').css({ position: 'relative',top: 0 });
           $fp('#uniList').css('margin-top','0px');
        }
      }else{
         if ($fp(this).scrollTop() > 190) {
           $fp('#mobilefixedscrolltop').addClass('sticky_top_nav');
           $fp('#mobilefixedscrolltop').css({ position: 'fixed', top: 0 });   
           $fp('#uniList').css('margin-top','0px');		
        }else{
           $fp("#mobilefixedscrolltop").removeClass("sticky_top_nav");
           $fp('#mobilefixedscrolltop').css({ position: 'relative',top: 0 });
           //$fp('#uniList').css('margin-top','0px');
        }
      }      
      adjustStyle();
    } else { 
       $fp("#mobilefixedscrolltop").removeAttr("style").removeClass("sticky_top_nav");
    }
  }); 
}

$fp(window).scroll(function(){ // scroll event 
  lazyloadetStarts();
});

function isScrolledIntoView(elem){
  var docViewTop = $fp(window).scrollTop();
  var docViewBottom = docViewTop + $fp(window).height();
  var elemTop = $fp(elem).offset().top;
  var elemBottom = elemTop + $fp(elem).height();
  return ((elemBottom >= docViewTop) && (elemTop <= docViewBottom));
}
//To get the next set of university list  
function loadAjaxPaginationRes() {            
  $fp(window).scroll(function() {
    if($$D("curPageNo")) {
      if(parseInt($$D("curPageNo").value) < parseInt($$D("totPageNo").value)) {             
        var curPageNo = $$D("curPageNo").value;        
        var childDiv  = "#loadAjaxDiv_"+curPageNo;                
        if($$D("loadAjaxDiv_"+curPageNo)) {          
          if(isScrolledIntoView($fp(childDiv))) {                          
            var nextPage = parseInt($$D("curPageNo").value) + 1;        
            uniAjaxLoadNextset(nextPage);     
            removeExistIds();  
          }  
        }        
      }
    } 
  });     
}

function uniAjaxLoadNextset(nextPage) {  
  var searchTxt =  $$D("searchTxt").value;
  var region = $$D("region").value;
  var ajax = new sack();
  blockNone("loader","block");
  blockNone("loader_text","block"); 
  url = contextPath + "/findUniversityInfoAjax.html?searchTxt="+searchTxt + region + "&pageNo="+nextPage + "&ajaxFlag=Y";  
  ajax.requestFile = url;
  ajax.onCompletion = function(){appendPageResult(ajax,nextPage);};
  ajax.runAJAX();  
}

function appendPageResult(ajax,nextPage) {
  if($$D('next_results_'+nextPage)){
    var response = ajax.response;   
    blockNone("loader","none");
    blockNone("loader_text","none");
    $$D('next_results_'+nextPage).innerHTML = response;
    if($$('gambanner'+nextPage)){eval($$('gambanner'+nextPage).innerHTML);}
  }  
}

function removeExistIds() {
  $fp("#curPageNo").remove();
  $fp("#totPageNo").remove();
}

//To show error message in light box
function openLightboxMsg(messageType){
  sm('newvenue', 650, 500, 'final5lbb');	
  url = contextPath + "/jsp/unifinder/include/errorMessage.jsp?pageType="+messageType;  
  var ajaxObj = new sack();
  ajaxObj.requestFile = url;	      
  ajaxObj.onCompletion = function(){displayFormData(ajaxObj);};	
  ajaxObj.runAJAX();   
}
function displayFormData(ajax){  
  $$D("newvenueform").innerHTML=ajax.response;   
  if($$D("lightbxScript")){
    eval($$D("lightbxScript").innerHTML);
  }    
  $$D("ajax-div").style.display="none";
  $$D("newvenueform").style.display="block";
}