var uniDefaultText = "eg. Cardiff University";
var courseDefaultText = "eg. Maths";
function $$DD(id){
  return document.getElementById(id);
}

function autocompleteOffS(element) {
  if(document.getElementsByTagName) {
    var inputElements = document.getElementsByTagName("input");
    for(i=0; inputElements[i]; i++) {
      if (inputElements[i].className && (inputElements[i].className.indexOf("ql-inpt") != -1)) {
        inputElements[i].setAttribute("autocomplete","off");
      }
    }
  }
}
//This method is called when user details are updated in yes page
function submitStudentSurveyYes(userAgent){
    var collegeName = myTrim($$DD("collegeName").value);
    var courseTitle = myTrim($$DD("courseTitle").value);
    var courseTitleHidn = myTrim($$DD("courseTitle_hidden").value);
    var collegeNameDisplay = myTrim($$DD("collegeName_display").value);
    var courseTitleDisplay = myTrim($$DD("courseTitle_display").value);
    if(collegeName == 'eg. Cardiff University' || collegeName == ""){
        $$DD("errordiv1").innerHTML = "Please enter college name.";
        blockNone("errordiv1",'block');
        return false;
    }else{
        $$DD("errordiv1").innerHTML = "";
        blockNone("errordiv1",'none');
    }if(collegeName != collegeNameDisplay){
        $$DD("errordiv1").innerHTML = "Please select a university from the drop down list.";
        blockNone("errordiv1",'block');
        return false;
    }else{
        $$DD("errordiv1").innerHTML = ""
        blockNone("errordiv1",'none');
    }
    if(courseTitle == 'eg. Maths' || courseTitle == ""){
        $$DD("errordiv2").innerHTML = "Please enter course."
        blockNone("errordiv2",'block');
        return false;
    }else{
        $$DD("errordiv2").innerHTML = "Please enter course.";
        blockNone("errordiv2",'none');
    }
    if(courseTitleHidn == ""){
        $$DD("errordiv2").innerHTML = "Please select a course from the drop down list."
        blockNone("errordiv2",'block');
        return false;
    }else{
        $$DD("errordiv2").innerHTML = "Please enter course.";
        blockNone("errordiv2",'none');
    }
    if(courseTitle != courseTitleDisplay){
        $$DD("errordiv2").innerHTML = "Please select a course from the drop down list.";
        blockNone("errordiv2",'block');
        return false;
    }else{
        $$DD("errordiv2").innerHTML = ""
        $$DD("errordiv2").style.display = 'none';        
    }
    $$DD('studentsurveynewform').action = "/degrees/student-survey-submit.html";//21_JULY_2015 added submit value type
    $$DD('submitVal').value="Submit";
    $$DD('studentsurveynewform').submit();    
    return true;    
}

function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}

function clearDefaultValue(obj,defaultTextType){
var defaultText = courseDefaultText;
if(defaultTextType=='uni'){defaultText=uniDefaultText}
  if(obj){    
    if(obj.value==defaultText){ obj.value='';}
    var elementId = obj.id;
    obj.value = "";
    $$DD(elementId+"_hidden").value="";
   obj.style.colour= 'rgba(112,112,112,.5)';
  }
}

function setDefaultValue(obj,defaultTextType){
var defaultText = courseDefaultText;
if(defaultTextType=='uni'){defaultText=uniDefaultText}
  if(obj.value==''){obj.value=defaultText; obj.style.colour= 'rgba(112,112,112,.5)';}
}

function clearhiddenvalues(){
  $$('surveyNoBrowsekKwd_url').value = "";
  $$('surveyNoBrowsekKwd_hidden').value = "";
  $$('surveyNoBrowsekKwd_id').value = "";
  $$('surveyNoBrowsekKwd_display').value = "";
  $$('surveyNoBrowsekKwd_location').value = "";  
}

function autoCompleteSurveyKeywordBrowse(event,object){ //21_JULY_2015_REL method called while autocomplete
  clearhiddenvalues();
  var qualCode = "CLEARING";
	 ajax_showOptions(object,'keywordbrowse',event,"indicator",'qual='+ qualCode +'&siteflag=desktopsurvey', '0', '385');
}

function surveySearchSubmit(){//21_JULY_2015_REL method called when clearing search button is called on no student survey page
  blockNone("surveyNoBrowsekKwdError",'none');
  var navKwd = trimString($$('surveyNoBrowsekKwd').value);
  var qualDesc = 'degree';
  var qualCode = "M";
  var kwDef = "eg. Maths";
  var reg = /[/,!@#$%^&*()_{}[]|\\:;<>]/gi;
  navKwd = navKwd.replace(reg," ");
  if($$("surveyNoBrowsekKwd_url").value == ""){
    if($$("surveyNoBrowsekKwdError")){
      $$("surveyNoBrowsekKwdError").innerHTML = "Please select a subject from the drop down list.";
      blockNone("surveyNoBrowsekKwdError",'block');
    }
    $$('surveyNoBrowsekKwd').value = '';
    $$('surveyNoBrowsekKwd').focus();
    return false;
  }  
  var navKwd1 = replaceAll(navKwd," ","+");
  var navKwd2 = replaceAll(navKwd," ","-");
  var country1 = 'united+kingdom';
  var country2 = 'united-kingdom';
  var contextPath='/degrees';
  var url1 = "/degrees" + "/courses/" + qualDesc + "-courses/" + navKwd2 + "-" + qualDesc + "-courses-" + country2 + "/" + navKwd1;
  var url2 = "/" + qualCode + "/" + country1 + "/" + country1 + "/25/0/0/0/r/0/1/0/uc/0/0/";
  var url3 = "page.html?nd&clearing"; 
  var finalUrl = url1 + url2 + url3; 
  if($$("surveyNoBrowsekKwd_url").value != "" ||  $$("surveyNomatchbrowsenode").value != ""){
    finalUrl = $$("surveyNoBrowsekKwd_url").value != "" ? $$("surveyNoBrowsekKwd_url").value : $$("surveyNomatchbrowsenode").value;
  }
  submitSurveyNo(finalUrl.toLowerCase());
}

//This method is called when yoe is updated in notint page
function submitSurveyNotInt(){
  var courseYear = document.getElementsByName('courseYear');
  var courseYear_value;
  for(var i = 0; i < courseYear.length; i++){
    if(courseYear[i].checked){
        courseYear_value = courseYear[i].value;
    }
  }
  if(courseYear_value ==''|| courseYear_value ==null  || courseYear_value==undefined){
      $$DD("surveyNotintYOEError").innerHTML = "Please choose the year you are starting uni.";
      $$DD("surveyNotintYOEError").style.display = 'block';   
      return false;
  }
  $$DD('submitVal').value="Submit";
  $$DD('studentsurveynotintform').action = "/degrees/student-survey-notint-submit.html";
  $$DD('studentsurveynotintform').submit();
  return true;   
}

function submitSurveyNo(finalUrl){
  var userid = $$DD("surveyuserId").value
  var subjectCode = $$DD("surveyNoBrowsekKwd_location").value
  var url = "/degrees/student-survey-no-submit.html?submitId=Submit_No&submitVal=Submit&userId="+userid+"&subjectCode="+subjectCode; 
  var ajax=new sack();
      ajax.requestFile = url;
      ajax.onCompletion = setTimeout(function(){ submitSureveyNoResponse(ajax,finalUrl); },1000);	
      ajax.runAJAX();
}

function submitSureveyNoResponse(ajax,finalUrl){
	 $$DD("studentsurveynoform").action = finalUrl.toLowerCase();
	 $$DD("studentsurveynoform").submit();
}