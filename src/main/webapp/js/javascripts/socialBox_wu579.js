var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";//Added wu_scheme by Indumathi.S Mar-29-2016
//
var fch1 = jQuery.noConflict();
function openTweetUs(hideId, showId){
 fch1("#"+hideId).hide();
 fch1("#"+showId).show();
}
//Added by Indumathi.S Nov-24-15 Rel
var gaAccount = fch1("#gaAccount").val();
function initSocialBoxScripts(){  
 if (fch1('#hideSocialBoxFlag').length<=0){ 
  var screenWidth = document.documentElement.clientWidth;
  var userType = getCookie("userType");  
  var timeOutVal = 1;   
  if (fch1("#addTofnchTickerList").is(":visible")){
   fch1("#socialBoxMin").addClass("sbx_cnt");
  } else {
    setTimeout(function() {
     if (!fch1("#tickerTape").is(":visible")){
      fch1("#socialBoxMin").addClass("sbx_cnt1");
     }
    }, 2500);
  }
  fch1(".f5_clse_btn").click(function(){
   fch1("#socialBoxMin").removeClass("sbx_cnt");
  }); 
  fch1(".f5_msg_rht a").click(function(){
   fch1("#socialBoxMin").removeClass("sbx_cnt");
  });
  fch1("select").click(function(){
   fch1("#socialBoxDiv").css("display","none");
	  fch1("#socialBoxMin").css("display","block");
  });
    fch1("#socialBoxMin").fadeIn("slow");
  var HCFBMsgSrt=true;
	fch1('#HCtimeLineFB').on('keyup', function(e){		
	 var keypressed=e.keyCode || e.which;
	 if (keypressed==13) {
		fch1('#FBBoxLink').get(0).click();
	 }
	 if(HCFBMsgSrt){
		HCFBMsgSrt=false;
		GaEventTrack ('' ,0 ,'Facebook-messagestart', 'Social Plug In', 0, false,gaAccount);
	 }
	});
	var HCMailMsgSrt=true;
	fch1('#socialBoxMsg').on('keyup', function(e){		
	 if(HCMailMsgSrt){
    HCMailMsgSrt=false;
	  GaEventTrack ('', 0, 'Email-messagestart', 'Social Plug In', 0, false, gaAccount);
	 }
  });
  var HCTweetMsgSrt=true;
  fch1('#tweetMsgBox').on('keyup', function(e){		
   var mes = fch1(this).val();
   mes=replaceAll(mes,'#','%23');
   mes=replaceAll(mes,'&','%26');
   fch1("#tweetBoxLink").attr("href", "https://twitter.com/intent/tweet?screen_name=whatuni&text="+mes);
   var keypressed=e.keyCode || e.which;
   if (keypressed==13) {
    fch1('#tweetBoxLink').get(0).click();
   }
   if(HCTweetMsgSrt){
    HCTweetMsgSrt=false;
    GaEventTrack ('',0,'Twitter-messagestart','Social Plug In',0,false,gaAccount);
   }
  });
  fch1('#tweetBoxLink').on('click', function(){
   if((fch1("#tweetMsgBox").val().trim()!="")&&(fch1("#tweetMsgBox").val().trim()!=null)){
    fch1('#tweetMsgBox').val("");
    GaEventTrack ('', 0, 'Twitter-submit', 'Social Plug In', 0, false, gaAccount);
   }else{
    fch1("#tweetBoxLink").attr("href", "javascript:void(0);");
    alertErrorMsg('Please enter your message','tweetMsgBox');
   }				
  });
  var HCLoadSocScripts=true;
  fch1(".socialBoxDivCls").click( function() {
   if(fch1("#socialBoxMin").is(":hidden")) {
    fch1("#socialBoxDiv").fadeOut("fast", function(){
     fch1("#socialBoxMin").fadeIn("slow");
     setSessionCookie('social_popup', 'NO');
     fch1("#socialBoxMain").show();//Added by Indumathi.S
     fch1("#socialBoxTweetUs").hide();
    });   
    var minzCnt=Number(GetCookie('socialBoxMinimise'))+1;
    if(minzCnt<=5){
     GaEventTrack ('', 0, 'Minimise', 'Social Plug In', 0, false, gaAccount);
     SetCookie('socialBoxMinimise',minzCnt,'','/','',''); //Added by Indumathi.S Jan-27-15
    }
   }else{
    if (HCLoadSocScripts){
     HCLoadSocScripts=false;
     !function(d,s,id){
      var js,fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
      if(!d.getElementById(id)){
       js = d.createElement(s);
       js.id = id;
       js.src = p + "://platform.twitter.com/widgets.js";
       fjs.parentNode.insertBefore(js,fjs);
      }
     }(document, "script", "twitter-wjs");     
    }
    fch1("#socialBoxMin").hide();
    fch1("#socialBoxDiv").fadeIn("slow");
    }    
   });
   
   fch1("#speakToHerb").click(function() {
      ga('send', 'event', 'Herb Quiz', 'Start Quiz', 'speak to herb'); 
   });
   
   fch1(".socialBoxTab").click( function() {    
    if(!fch1(this).hasClass("chtact")){
    fch1(".socialBoxTab").each(function(){
     if(fch1(this).hasClass("chtact")){
      fch1(".socialBoxTab").removeClass("chtact");
      fch1(".socialBoxTab").children(".socialBoxTabDiv").hide();
     }
    });
    fch1(this).addClass("chtact");
    fch1(this).children(".socialBoxTabDiv").fadeIn("fast");
   }
  });
 }
}
function showSocBoxSuccess(divid){
 fch1("#"+divid).fadeIn("slow", function(){
  setTimeout(function(){fch1("#"+divid).fadeOut("slow");}, 5000);
 });
}
function alertErrorMsg(msg,id){
 alert(msg);
 fch1("#"+id).val(fch1("#"+id).val().trim());
 fch1("#"+id).focus();
}
function GaEventTrack (CollegeName,CpeValue,event,Category,CollegeId,BooleanFlag,gaaccount){
 ga('create', gaaccount, 'auto', {'sampleRate': 80, 'name': 'salesTracker'});
 ga('send', 'event', Category, event, CollegeName, CpeValue, true); 
}
//Code added by Prabha on DEC-15
fch1(document).ready(function(){
//var cookiePopupDiv = document.getElementById("cookie-lightbox");
// if(cookiePopupDiv && cookiePopupDiv.style.display == "none"){
 initSocialBoxScripts();
//}
});
//Added by Indumathi.S Jan-27-Rel
function getCookieVal(offset){
 var endstr = document.cookie.indexOf (";", offset);
 if (endstr == -1) { endstr = document.cookie.length; }
 return unescape(document.cookie.substring(offset, endstr));
}
function GetCookie(name){
 var arg = name + "=";
 var alen = arg.length;
 var clen = document.cookie.length;
 var i = 0;
 while (i < clen){
  var j = i + alen;
	if(document.cookie.substring(i, j) == arg){
   return getCookieVal(j);
	}
	i = document.cookie.indexOf(" ", i) + 1;
	if (i == 0) break; 
 }
 return null;
}
function SetCookie(name,value,expires,path,domain,secure){  	
 document.cookie = name + "=" + escape (value) +
 ((expires) ? "; expires=" + expires.toGMTString() : "") +
 ((path) ? "; path=" + path : "") +
 ((domain) ? "; domain=" + domain : "") +
 ((secure) ? "; secure" : "");
}
function backToMainPod(){
  fch1("#socialBoxTweetUs").hide();
  fch1("#socialBoxMain").show();
}
