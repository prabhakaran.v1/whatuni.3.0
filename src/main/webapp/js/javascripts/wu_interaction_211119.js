function prospectusRedirect(contextPath, collegeId, courseId, subject, text, subOrderItemId, typeofEnquiry, adRoll){
  //alert('----prospectusRedirect----- ');
  var submitUrl;
 if(adRoll =="" ||adRoll == null) {
  adRollLoggingGetPros(collegeId); 
 }
 if(document.getElementById("enquiryFormProceed1")){
   if(document.getElementById("enquiryFormProceed1").checked == false){
     showErrorMsg('termsc_error1',ErrorMessage.error_termsc);
     return false;
   }else{
     blockNone("termsc_error1","none");
   }
 }
	var url = contextPath + '/prospectuswebredirect.html?z=' + collegeId;
	if(typeof XMLHttpRequest != "undefined"){
		req = new XMLHttpRequest();
	}else if(window.ActiveXObject){
		req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("POST", url, true);
	req.onreadystatechange = handlerequest;
	req.send(url);
	function handlerequest(){
		if(req.readyState == 4){
			if(req.status == 200){
				var respon=req.responseText;
				//var paramvalues = (courseId != '' ? "&w=" + courseId : "") + (subject != '' ? "&subject="+subject : "") + (opendays == 'opendays' ? "&opendays=true" : "");
				var courseid = courseId != '' ? courseId : "0";
				var subjectId = subject != '' ? replaceAll(replaceSpecialCharacter(subject), " ", "+") : "0";
				//var opendayFlag = opendays == 'opendays' ? "Y" : "n";
				var subOrderItemIID = (subOrderItemId != null || subOrderItemId != '') ? subOrderItemId : "0";
        var ajaxType;
        if(typeofEnquiry == 'EMAIL'){
          submitUrl= contextPath + "/email/" + respon + "-email/" + collegeId + "/" + courseid + "/" + subjectId + "/" + "n-" + subOrderItemIID +"/send-college-email.html";
          ajaxType = "EMAIL";
        }else if(typeofEnquiry == 'DP'){
          submitUrl = contextPath + "/prospectus/" + respon + "-prospectus/" + collegeId + "/" + courseid + "/" + subjectId + "/" + "n-" + subOrderItemIID + "/download-prospectus.html";
          ajaxType = "DP";
        }else{
          var dim12Val = ($$D('currentSection')) ? ('' != $$D('currentSection').value ? ($$D('currentSection').value).toLowerCase() : "home") : "";
          
  				submitUrl = contextPath + "/prospectus/" + respon + "-prospectus/" + collegeId + "/" + courseid + "/" + subjectId + "/" + "n-" + subOrderItemIID + "/order-prospectus.html";
          if(dim12Val != ""){
          submitUrl+= "?prevpagedim12="+encodeURIComponent(dim12Val);
          }
          
          ajaxType = "RP";
        }
				//window.location.href = submitUrl.toLowerCase()
				//return false
        var postEnquiry = text;
        var ajaxURL = "/degrees/oneclickajaxURL.html?p_college_id="+collegeId+"&p_course_id="+courseId+"&p_suborder_item="+subOrderItemId+"&p_subject="+subject+"&type="+ajaxType;
        if(postEnquiry == 'PE'){
          ajaxURL = ajaxURL+"&postEnquiry=P" 
        }
        //alert(ajaxURL);
        sm('newvenue', 490, 500, 'other');
        document.getElementById("ajax-div").style.display="block";
        var ajax=new sack();
        //alert("ajaxURL:"+ajaxURL);
        ajax.requestFile = ajaxURL;
        ajax.onCompletion = function(){ displayOnclickFormData(ajax, submitUrl, postEnquiry); };
        ajax.runAJAX();   
			}
		}
	}
//	return false
 
}

function downloadProspectusRedirect(contextPath, collegeId, courseId, subject, text, subOrderItemId){
 //alert('----downloadProspectusRedirect----- ');
 var submitUrl;
	var url = contextPath + '/prospectuswebredirect.html?z=' + collegeId;
	if(typeof XMLHttpRequest != "undefined"){
		req = new XMLHttpRequest();
	}else if(window.ActiveXObject){
		req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("POST", url, true);
	req.onreadystatechange = handlerequest;
	req.send(url);
	function handlerequest(){
		if(req.readyState == 4){
			if(req.status == 200){
				var respon=req.responseText;
				//var paramvalues = (courseId != '' ? "&w=" + courseId : "") + (subject != '' ? "&subject="+subject : "") + (opendays == 'opendays' ? "&opendays=true" : "");
				var courseid = courseId != '' ? courseId : "0";
				var subjectId = subject != '' ? replaceAll(replaceSpecialCharacter(subject), " ", "+") : "0";
				//var opendayFlag = opendays == 'opendays' ? "Y" : "n";
				var subOrderItemIID = (subOrderItemId != null || subOrderItemId != '') ? subOrderItemId : "0";
				submitUrl = contextPath + "/prospectus/" + respon + "-prospectus/" + collegeId + "/" + 
								courseid + "/" + subjectId + "/" + "n-" + subOrderItemIID + "/download-prospectus.html";
				//window.location.href = submitUrl.toLowerCase()
				//return false
        var postEnquiry = text;
        var ajaxURL = "/degrees/oneclickajaxURL.html?p_college_id="+collegeId+"&p_course_id="+courseId+"&p_suborder_item="+subOrderItemId+"&p_subject="+subject+"&type=DP";
        if(postEnquiry == 'PE'){
          ajaxURL = ajaxURL+"&postEnquiry=P" 
        }
        sm('newvenue', 490, 500, 'other');
        document.getElementById("ajax-div").style.display="block";
        //alert("ajaxURL:"+ajaxURL);
        var ajax=new sack();
        ajax.requestFile = ajaxURL;
        ajax.onCompletion = function(){ displayOnclickFormData(ajax, submitUrl, postEnquiry); };
        ajax.runAJAX();   
			}
		}
	}
//	return false
 
}

function oneclickandHideShowRequest(contextPath, collegeId, courseId, subject, text, subOrderItemId){
  //alert('inside oneclickandHideShowRequest function----');
  var submitUrl;
	var url = contextPath + '/prospectuswebredirect.html?z=' + collegeId;
  //alert('---url---- '+url);
	if(typeof XMLHttpRequest != "undefined"){
		req = new XMLHttpRequest();
	}else if(window.ActiveXObject){
		req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	req.open("POST", url, true);
	req.onreadystatechange = handlerequest;
	req.send(url);
	function handlerequest(){
		if(req.readyState == 4){
			if(req.status == 200){
				var respon=req.responseText;
        //alert('---respon--- '+respon)
				//var paramvalues = (courseId != '' ? "&w=" + courseId : "") + (subject != '' ? "&subject="+subject : "") + (opendays == 'opendays' ? "&opendays=true" : "");
				var courseid = courseId != '' ? courseId : "0";
				var subjectId = subject != '' ? replaceAll(replaceSpecialCharacter(subject), " ", "+") : "0";
				//var opendayFlag = opendays == 'opendays' ? "Y" : "n";
				var subOrderItemIID = (subOrderItemId != null || subOrderItemId != '') ? subOrderItemId : "0";
        submitUrl=contextPath+"/email/"+respon+"-email/"+collegeId+"/"+courseid+"/"+subjectId+"/"+"n-"+subOrderItemIID+"/send-college-email.html";
                //alert('----submitUrl----- '+submitUrl);
				//window.location.href = submitUrl.toLowerCase()
				//return false
        var postEnquiry = text;
        //alert(postEnquiry);
        var ajaxURL = "/degrees/oneclickajaxURL.html?p_college_id="+collegeId+"&p_course_id="+courseId+"&p_suborder_item="+subOrderItemId+"&p_subject="+subject+"&type=EMAIL";
        if(postEnquiry == 'PE'){
          ajaxURL = ajaxURL+"&postEnquiry=P" 
        }
        sm('newvenue', 490, 500, 'other');
        document.getElementById("ajax-div").style.display="block";
        //alert("ajaxURL:"+ajaxURL);
        var ajax=new sack();
        ajax.requestFile = ajaxURL;
        ajax.onCompletion = function(){ displayOnclickFormData(ajax, submitUrl, postEnquiry); };
        ajax.runAJAX();   
			}
		}
	}
}


function oneclickBoxSubmit(enquiryType, collegeName, cpeValue){
  //alert("-------------oneclickBoxSubmit-------------");
  var original = document.getElementById('or_url').value;
  var oneclickCheckBox = "";
  var newsLetterFlag = "";
  if(document.getElementById('oneClickCheckBox').checked){
    document.getElementById('checboxflag').value = "Y";
  }
  if(document.getElementById('newsLetter').checked){
    document.getElementById('newsflag').value = "Y";
  }
  var OlEnquiryExistFlag = document.getElementById('OlEnquiryExistFlag') ? document.getElementById('OlEnquiryExistFlag').value : "N";
    document.getElementById('oneqlform').action = original;
  if(enquiryType == 'send-college-email'){
     var message;
    if(document.getElementById('umessage')){
      message = document.getElementById('umessage').value;
      document.getElementById('onemessage').value = message;
    }
    document.getElementById('oneqlform').action = original;    
    ga('send', 'pageview', {'hitCallback': function(){document.getElementById('oneqlform').submit();}}); //Added by Amir for UA logging for 24-NOV-15 release
    if(OlEnquiryExistFlag != "Y"){
      GAInteractionEventTracking('emailsubmit', 'interaction', 'Email', collegeName, cpeValue);    
    }
  }else if(enquiryType == 'order-prospectus'){    
    ga('send', 'pageview', {'hitCallback': function(){document.getElementById('oneqlform').submit();}}); //Added by Amir for UA logging for 24-NOV-15 release
    if(OlEnquiryExistFlag != "Y"){
      GAInteractionEventTracking('prospectussubmit', 'interaction', 'Prospectus', collegeName, cpeValue);
    }
  }else if(enquiryType == 'download-prospectus'){    
    ga('send', 'pageview', {'hitCallback': function(){document.getElementById('oneqlform').submit();}}); //Added by Amir for UA logging for 24-NOV-15 release
    GAInteractionEventTracking('dpsubmit', 'interaction', 'Prospectus-dl', collegeName, cpeValue);
  } 
} 

function displayOnclickFormData(ajax, submitUrl, postEnquiry){
  var originalURL = submitUrl;
  var responseArr = new Array();
  responseArr = ajax.response.split("##SPLIT##");
  
  //alert(responseArr);
  if(responseArr[0] == "NOBOX"){    
    var newsLetter = responseArr[1];    
    //originalURL = originalURL+"?oneClick=Proceed&checboxflag=Y&newsflag=Y";  
    //Added for the contenthub ga log in 20th_Feb_2018 release 
    originalURL = (originalURL.indexOf('?') > -1) ? (originalURL+"&oneClick=Proceed&checboxflag=Y&newsflag=Y") : (originalURL+"?oneClick=Proceed&checboxflag=Y&newsflag=Y");
    if(postEnquiry == 'PE'){
      originalURL = originalURL+"&postEnquiry=P";
    }
    var message;
    if(document.getElementById('umessage')){
      message = document.getElementById('umessage').value;
      message = escape(message);
      originalURL = originalURL+"&umessage="+message;
    }
    var typeofEnquiry = responseArr[2];
    var gaCollegeName = responseArr[3];
    var cpeValue = parseInt(responseArr[4]);
    var token = responseArr[5];
    var enquiryExistFlag = responseArr[6];
    originalURL = originalURL+"&_synchronizerToken="+token;
    //alert("originalURL---"+originalURL);
    if(typeofEnquiry == 'send-college-email'){      
      ga('send', 'pageview', {'hitCallback': function(){
      if(document.getElementById("ajax-div")){
        hm('newvenue');
      }      
      window.location.href=originalURL;
      }}); //Added by Amir for UA logging for 24-NOV-15 release
      if(enquiryExistFlag != "Y"){
        GAInteractionEventTracking('emailsubmit', 'interaction', 'Email', gaCollegeName, cpeValue);
      }
      
    }else if(typeofEnquiry == 'order-prospectus'){      
      ga('send', 'pageview', {'hitCallback': function(){
      if(document.getElementById("ajax-div")){
        hm('newvenue');
      }
      window.location.href=originalURL;}}); //Added by Amir for UA logging for 24-NOV-15 release
      if(enquiryExistFlag != "Y"){
        GAInteractionEventTracking('prospectussubmit', 'interaction', 'Prospectus', gaCollegeName, cpeValue);
      }
    }else if(typeofEnquiry == 'download-prospectus'){      
      ga('send', 'pageview', {'hitCallback': function(){
      if(document.getElementById("ajax-div")){
        hm('newvenue');        
      }
      window.location.href=originalURL;}}); //Added by Amir for UA logging for 24-NOV-15 release
      GAInteractionEventTracking('dpsubmit', 'interaction', 'Prospectus-dl', gaCollegeName, cpeValue);
    }
  }else if(responseArr[1] == "BOX"){    
    document.getElementById("newvenueform").innerHTML=responseArr[0]; 
    document.getElementById("ajax-div").style.display="none";
    document.getElementById("newvenueform").style.display="block";
    document.getElementById("or_url").value=submitUrl;
  }else if(responseArr[0] == "EXIST"){    
    if(postEnquiry == 'PE'){
      //originalURL = originalURL+"?postEnquiry=P";
      //Added for the contenthub ga log in 20th_Feb_2018 release 
      originalURL = (originalURL.indexOf('?') > -1) ? (originalURL+"&postEnquiry=P") : (originalURL+"?postEnquiry=P");      
    }
    if(document.getElementById("ajax-div")){
      hm('newvenue');        
    }
    location.href=originalURL;
  }else if(responseArr[0] == "EMAIL"){    
    document.getElementById('sendCollegeEmailBtn').style.display = 'none';
    document.getElementById('sendCollegeEmail').style.display = 'block';
    hm();
  }
  if(responseArr[0].indexOf('updateScreenWidth()') > -1) {
    eval(document.getElementById('updateScreenWidth').innerHTML);
  } 
}

function changeMyInfo(enquiryType){
  if(enquiryType == 'send-college-email'){
    document.getElementById('sendCollegeEmailBtn').style.display = 'none';
    document.getElementById('sendCollegeEmail').style.display = 'block';
    var tokenId = document.getElementById('tokenId').value;    
    var myform = document.getElementById("qlform");
    var inputs = myform.getElementsByTagName("input");
    for(var i = 0; i < inputs.length; i++){
      if (inputs[i].name == "_synchronizerToken") {
        inputs[i].value = tokenId;
      }
    }
    hm();
  }else{
    var originalURL = document.getElementById("or_url").value;
    location.href = originalURL;
  }
}


function displayOneClickBox(ajax){
  document.getElementById("newvenueform").innerHTML=ajax.response;  
  document.getElementById("ajax-div").style.display="none";
  document.getElementById("newvenueform").style.display="block";
}

function sendCollegeEmail(contextPath,collegeId,courseId,subject,opendays,subOrderItemId)
{
  adRollLoggingRequestInfo(collegeId);
  var url=contextPath+'/prospectuswebredirect.html?z='+collegeId;
  if(typeof XMLHttpRequest !="undefined")
  {
    req=new XMLHttpRequest();
  }else if(window.ActiveXObject){
    req=new ActiveXObject("Microsoft.XMLHTTP");
  }
  req.open("POST",url,true);
  req.onreadystatechange=handlerequest;
  req.send(url);
  function handlerequest()
  {
    if(req.readyState==4)
    {
      if(req.status==200)
      {
        var respon=req.responseText;
        var paramvalues=(courseId !='' ? "&w="+courseId : "")+(subject !='' ? "&subject="+subject : "")+(opendays=='opendays' ? "&opendays=true" : "");
        var courseid=courseId !='' ? courseId : "0";
        var subjectId=subject !='' ? trimString(replaceSpecialCharacter(subject)): "0";
        var opendayFlag=opendays=='opendays' ? "Y" : "N";
        var subOrderItemIID = (subOrderItemId != null || subOrderItemId != '') ? subOrderItemId : "0";
        var submitUrl=contextPath+"/email/"+respon+"-email/"+collegeId+"/"+courseid+"/"+subjectId+"/"+opendayFlag+"-"+subOrderItemIID+"/send-college-email.html";
        window.location.href=submitUrl.toLowerCase();
        return false;
      }
    }
  }
  return false;
}