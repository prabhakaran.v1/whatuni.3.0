var wu_scheme = ("https:" == document.location.protocol) ? "https://" : "http://";//Added wu_scheme by Indumathi.S Mar-29-2016
function $$DD(id){
  return document.getElementById(id);
}
var ulSubmitURL;
if($$DD("seourlpt")){ulSubmitURL = $$DD("seourlpt").value;}
function setSelectedVal(obj, spanid){  
  var dobid = obj.id
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  document.getElementById(spanid).innerHTML = text;
}
function disableNonUKSchool() {
  if (document.getElementById("nonUKFlag").checked == true){
    document.getElementById("schoolName").disabled = true;
    document.getElementById("schoolName").value = "Enter school name (optional)";
    document.getElementById("schoolName_hidden").value = "";
    document.getElementById("nonUKDiv").style.display = 'block';
  }else{
    document.getElementById("schoolName").disabled = false;
    document.getElementById("nonUKSchool").value = "Enter school name";
    document.getElementById("nonUKDiv").style.display = 'none';
  }
}
function disableNonUKSchoolWCSID() {  
  if (document.getElementById("nonUKFlagWCSID").checked == true){    
    document.getElementById("schoolNameWCSID").disabled = true;
    document.getElementById("schoolNameWCSID").value = "Enter school name (optional)";
    document.getElementById("schoolNameWCSID_hidden").value = "";
    document.getElementById("nonUKDivWCSID").style.display = 'block';
    $$DD("errordivWCSID").style.display = "none";
    $$DD("errordivWCSID").innerHTML = "";
    $$DD("errordiv1WCSID").style.display = "none";
    $$DD("errordiv1WCSID").innerHTML = "";
  }else{
    document.getElementById("schoolNameWCSID").disabled = false;
    document.getElementById("nonUKSchoolWCSID").value = "Enter school name";
    document.getElementById("nonUKDivWCSID").style.display = 'none';
  }
}
function autoCompleteUKSchool(event,object){
  if(specialTextValidate(trimString(object.value))){ 
      if(trimString(object.value)!=""){
          ajax_showOptions(object,'ukschoollist',event,"indicator",'');
      }  
  }else{  
     if($$DD("schoolName_hidden")) {
      $$DD("schoolName_hidden").value = "";  
     }
  }   
}  
function qualSubjectList(event, object){
    var qual = $$D('qualsel').value;    
    var id = object.id;
    var hiddenId = id+"_hidden";    
    if(($$D(hiddenId).value != "" || $$D(hiddenId).value != null) && event.keyCode != 13){
        $$D(hiddenId).value = "";
    }    
    if(specialTextValidate(trimString(object.value))){        
        if(trimString(object.value) != ""){
            ajax_showOptions(object,'qualsubects',event,"indicator",'ulqual='+ qual);    
        }
    }else{ 
        $$DD(hiddenId).value = "";        
    }
}

function clearDefault(obj, text){
  var id = obj.id;  
  if($$DD(id).value == text){
    $$DD(id).value = "";
  }
  $$DD(id).style.color = '#000000';
}
function keypressSubmit(obj, event){
    if(event.keyCode==13){
        return submitWhatcanido('Y');
    }
}
function iwanttobeKeypressSubmit(obj, event){
    if(event.keyCode==13){      
        return submitIWantotobe('Y');
    }
}

function setDefault(obj, text){
  var id = obj.id;
  if(document.getElementById(id).value == ""){
    document.getElementById(id).value = text;
    if(text == 'Enter industry, eg Engineering'){
      $$DD("industryName_hidden").value = "";
    }else if(text == 'Enter job name, eg Doctor'){
      $$DD("jobName_hidden").value = "";
    }
  }
  $$DD(id).style.color = '#888888';
}

function clearAlterText(objId, text, text1){
    var objId_hid = objId+"_hidden";
    if($$DD(objId).value != "" && $$DD(objId).value != text){        
        $$DD(objId).value = "";
        $$DD(objId_hid).value = "";
        $$DD(objId).value = text;
        $$DD(objId).style.color = '#888888';
    }    
}

function autocompleteOff(element) {
	self.focus(); //Bring window to foreground if necessary.
	var form = document.getElementById(element); // grab the form element.
	if(form)
	{
		form.setAttribute("autocomplete","off"); // Turn off Autocomplete for the form, if the browser supports it.
  }
}

function submitWhatcanido(userLogin){  
  var validationFlag = subjectValidation();  
  if(validationFlag == 'ATLEASTONE'){
    $$DD("errordivWCSID").innerHTML = "Please select subjects from the list."
    $$DD("errordivWCSID").style.display = 'block';
    //alert('Please enter atleast one Subject.....');
    return false;
  }else if(validationFlag == 'SUBMIT'){
    //alert(userLogin);
    if(userLogin == 'N'){
      showLightBoxLoginForm('popup-newlogin',650,500, 'whatcanido');
    }else{
      var qualSubjName1 = $$DD("qualSubjName1").value;
      var qualSubjName2 = $$DD("qualSubjName2").value;
      var qualSubjName3 = $$DD("qualSubjName3").value;
      var qualSubjName4 = $$DD("qualSubjName4").value;
      var qualSubjName5 = $$DD("qualSubjName5").value;
      var qualSubjName6 = $$DD("qualSubjName6").value;
      var schoolName = $$DD("schoolName") ? $$DD("schoolName").value : "";      
      var qualSubjName1hidden = $$DD("qualSubjName1_hidden").value;
      var qualSubjName2hidden = $$DD("qualSubjName2_hidden").value;
      var qualSubjName3hidden = $$DD("qualSubjName3_hidden").value;
      var qualSubjName4hidden = $$DD("qualSubjName4_hidden").value;
      var qualSubjName5hidden = $$DD("qualSubjName5_hidden").value;
      var qualSubjName6hidden = $$DD("qualSubjName6_hidden").value;
      var schoolNamehidden = $$DD("schoolName_hidden") ? $$DD("schoolName_hidden").value : "";      
      if((qualSubjName1 !="" && qualSubjName1 !="Subject" && qualSubjName1hidden=="") || (qualSubjName2 !="" && qualSubjName2 !="Subject" && qualSubjName2hidden=="") 
      || (qualSubjName3 !="" && qualSubjName3 !="Subject" && qualSubjName3hidden=="") || (qualSubjName4 !="" && qualSubjName4 !="Subject" && qualSubjName4hidden=="")
      || (qualSubjName5 !="" && qualSubjName5 !="Subject" && qualSubjName5hidden=="") || (qualSubjName6 !="" && qualSubjName6 !="Subject" && qualSubjName6hidden=="")){
          $$DD("errordivWCSID").innerHTML = "Please choose valid subject from the list."
          $$DD("errordivWCSID").style.display = 'block';
          $$DD("errordiv1WCSID").innerHTML = ""
          $$DD("errordiv1WCSID").style.display = 'none';
          return false;
      }
      if((schoolName !="" && schoolName !="Enter school name (optional)" && schoolNamehidden=="")){
          $$DD("errordiv1WCSID").innerHTML = "Please choose valid school name from the list."
          $$DD("errordiv1WCSID").style.display = 'block';
          return false;
      }
      if(schoolName !="Enter school name (optional)"){
        if($$DD("schoolName") && $$DD("schoolName").value != $$DD("schoolName_display").value){
              $$DD("errordiv1WCSID").innerHTML = "Please choose valid school name from the list."
              $$DD("errordiv1WCSID").style.display = 'block';
              return false;
          }
      }
      document.getElementById('viewDegreessubmit').style.display = "none";
      ulSubmitURL = (ulSubmitURL != null && ulSubmitURL != undefined) ? ulSubmitURL : "/degrees/courses/a-level-choices.html";
      var csSubmitURL = ulSubmitURL + "?pageno=2";      
      document.getElementById('ulsearchformWCSID').action = csSubmitURL;            
      document.getElementById('ulsearchformWCSID').submit();
      return false;
    }
  }else if(validationFlag == 'NOTVALIDTARIFF'){
    $$DD("errordivWCSID").innerHTML = "Cannot search with more than 999 points."
    $$DD("errordivWCSID").style.display = 'block';    
    return false;
  }else if(validationFlag == 'ATLEASTONETP'){
    $$DD("errordivWCSID").innerHTML = "Please enter tariff points."
    $$DD("errordivWCSID").style.display = 'block';    
    return false;
  }
}

function submitWhatcanidoWCSID(userLogin){  
  var validationFlag = subjectValidation();   
  if(validationFlag == 'ATLEASTONE'){
    if($$DD("errordiv")){
      $$DD("errordiv").innerHTML = "Please select subjects from the list."
      $$DD("errordiv").style.display = 'block';   
    }
    return false;
  }else if(validationFlag == 'SUBMIT'){    
    if(userLogin == 'N'){
      showLightBoxLoginForm('popup-newlogin',650,500, 'whatcanido');
    }else{
      var qualSubjName1 = $$DD("qualSubjName1").value;
      var qualSubjName2 = $$DD("qualSubjName2").value;
      var qualSubjName3 = $$DD("qualSubjName3").value;
      var qualSubjName4 = $$DD("qualSubjName4").value;
      var qualSubjName5 = $$DD("qualSubjName5").value;
      var qualSubjName6 = $$DD("qualSubjName6").value;
      var schoolName = $$DD("schoolName")  ?  $$DD("schoolName").value : "";  
      var qualSubjName1hidden = $$DD("qualSubjName1_hidden").value;
      var qualSubjName2hidden = $$DD("qualSubjName2_hidden").value;
      var qualSubjName3hidden = $$DD("qualSubjName3_hidden").value;
      var qualSubjName4hidden = $$DD("qualSubjName4_hidden").value;
      var qualSubjName5hidden = $$DD("qualSubjName5_hidden").value;
      var qualSubjName6hidden = $$DD("qualSubjName6_hidden").value;
      var schoolNamehidden = $$DD("schoolName_hidden") ? $$DD("schoolName_hidden").value : "";      
      if((qualSubjName1 !="" && qualSubjName1 !="Subject" && qualSubjName1hidden=="") || (qualSubjName2 !="" && qualSubjName2 !="Subject" && qualSubjName2hidden=="") 
      || (qualSubjName3 !="" && qualSubjName3 !="Subject" && qualSubjName3hidden=="") || (qualSubjName4 !="" && qualSubjName4 !="Subject" && qualSubjName4hidden=="")
      || (qualSubjName5 !="" && qualSubjName5 !="Subject" && qualSubjName5hidden=="") || (qualSubjName6 !="" && qualSubjName6 !="Subject" && qualSubjName6hidden=="")){
         if($$DD("errordiv")){ 
          $$DD("errordiv").innerHTML = "Please choose valid subject from the list."
          $$DD("errordiv").style.display = 'block';
         }
         if($$DD("errordiv1")){
          $$DD("errordiv1").innerHTML = ""
          $$DD("errordiv1").style.display = 'none';
         }
          return false;
      }
      if((schoolName !="" && schoolName !="Enter school name (optional)" && schoolNamehidden=="" && $$DD("errordiv1"))){
          $$DD("errordiv1").innerHTML = "Please choose valid school name from the list."
          $$DD("errordiv1").style.display = 'block';
          return false;
      }
      if(schoolName !="Enter school name (optional)"){
        if($$DD("schoolName") && $$DD("schoolName").value != $$DD("schoolName_display").value){
           if($$DD("errordiv1")){
              $$DD("errordiv1").innerHTML = "Please choose valid school name from the list."
              $$DD("errordiv1").style.display = 'block';
              return false;
            }
          }
      }
      document.getElementById('viewDegreessubmit').style.display = "none";
      ulSubmitURL = (ulSubmitURL != null && ulSubmitURL != undefined) ? ulSubmitURL : "/degrees/courses/a-level-choices.html";
      var csSubmitURL = ulSubmitURL + "?pageno=2";
      //alert("firstpagesubmit WCSID : "+document.getElementById('ulsearchsubmitWCSID').value);      
      document.getElementById('ulsearchformWCSID').action = csSubmitURL;            
      document.getElementById('ulsearchformWCSID').submit();
      return false;
    }
  }else if(validationFlag == 'NOTVALIDTARIFF'){
    if($$DD("errordiv")){
      $$DD("errordiv").innerHTML = "Cannot search with more than 999 points."
      $$DD("errordiv").style.display = 'block';    
    }
    return false;
  }else if(validationFlag == 'ATLEASTONETP'){
    if($$DD("errordiv")){
      $$DD("errordiv").innerHTML = "Please enter tariff points."
      $$DD("errordiv").style.display = 'block';  
    }
    return false;
  }
}


function submitIWantotobe(userLogin){  
  var jobCode = trimString($$DD("jobName_hidden").value);
  var industryCode = trimString($$DD("industryName_hidden").value);
  var jobName = trimString($$DD("jobName").value);
  var industryName = trimString($$DD("industryName").value);
  var schoolName = $$DD("schoolName") ? trimString($$DD("schoolName").value) : "";
  //var schoolNameDisplay = trimString($$DD("schoolName_display").value);  
  if((jobCode == "" || jobCode == null) && jobName != 'Enter job name, eg Doctor' && $$DD("errordiv")){
    $$DD("errordiv").innerHTML = "There is no data available for "+jobName+", this is either because there is no data available for that job, or the job has been classified at a higher level. Eg. Mental Health Nurse will be classified under Nurse."
    $$DD("errordiv").style.display = 'block';
    return false;
  }
  if((industryCode == "" || industryCode == null) && industryName != 'Enter industry, eg Engineering' && $$DD("errordiv")){
    $$DD("errordiv").innerHTML = "There is no data available for "+industryName+", this is either because there is no data available for that industry, or the industry has been classified at a higher level."
    $$DD("errordiv").style.display = 'block';
    return false;
  }
  if((jobCode != "" && industryCode != "") || (jobCode == "" && industryCode == "")){
    if($$DD("errordiv")){
      $$DD("errordiv").innerHTML = "Please select either Job or Industry from the list."
      $$DD("errordiv").style.display = 'block';
    }
    //alert('Please enter either Job or Industry.');
    return false;
  }
  if(userLogin == 'N'){
    showLightBoxLoginForm('popup-newlogin',650,500, 'iwanttobe');
  }else{
    document.getElementById('iwanttobefirst').style.display = "none";    
    ulSubmitURL = (ulSubmitURL != null && ulSubmitURL != undefined) ? ulSubmitURL : $$DD("seourlpt").value;
    var csSubmitURL = ulSubmitURL + "?pageno=2";
    document.getElementById('ulsearchform').action = csSubmitURL;
    document.getElementById('ulsearchform').submit();
    return false;
  }
}

function submitIWanttobenonloggedin(){
  //alert('inside the search ');
  var jobCode = trimString($$DD("jobName_hidden").value);
  var industryCode = trimString($$DD("industryName_hidden").value);
  //alert('---job---  '+$$DD("jobName_hidden").value);
  //alert('---industry---  '+$$DD("industryName_hidden").value);
  if(((jobCode != null || jobCode != "") && (industryCode != null || industryCode != "")) || ((jobCode == null || jobCode == "") && (industryCode == null || industryCode == ""))){
    alert('Please enter either Job or Industry.');
    return false;
  }
}

function onchageQual(obj){
  var qual = obj.value;  
  if(qual == 'AL'){
    $$DD("fileset1").style.display = "block";
    $$DD("fileset2").style.display = "block";
    $$DD("fileset3").style.display = "block";
    $$DD("fileset4").style.display = "block";
    $$DD("fileset5").style.display = "none";
    $$DD("fileset6").style.display = "none";
    displayBlockUCASInput('gradediv', 'block', 6);
    displayBlockUCASInput('ucas', 'none', 6);
    createSelectBoxUL(qual, 'selgradeone', 'grade1span');   
    createSelectBoxUL(qual, 'selgradetwo', 'grade2span');   
    createSelectBoxUL(qual, 'selgradethree', 'grade3span');   
    createSelectBoxUL(qual, 'selgradefour', 'grade4span');   
    $$DD("qualSubjName5").value = "Subject"; $$DD("qualSubjName6").value = "Subject";
    $$DD("qualSubjName5_hidden").value = ""; $$DD("qualSubjName5_hidden").value = "";
    
    //
    $$DD("qualSubjName1").value = "Subject"; $$DD("qualSubjName2").value = "Subject";
    $$DD("qualSubjName1_hidden").value = ""; $$DD("qualSubjName2_hidden").value = "";
    $$DD("qualSubjName3").value = "Subject"; $$DD("qualSubjName4").value = "Subject";
    $$DD("qualSubjName3_hidden").value = ""; $$DD("qualSubjName4_hidden").value = "";
    //
    if($$DD("ucascalc")){$$DD("ucascalc").className = "yrsb-fst nors";}
    if($$DD("csucascalc")){$$DD("csucascalc").style.display = "none";}
  }else if(qual == 'H' || qual == 'AH'){
    //alert('H');
    displayBlockUCASInput('fileset', 'block', 6);
    displayBlockUCASInput('gradediv', 'block', 6);
    displayBlockUCASInput('ucas', 'none', 6);
    createSelectBoxUL(qual, 'selgradeone', 'grade1span');   
    createSelectBoxUL(qual, 'selgradetwo', 'grade2span');   
    createSelectBoxUL(qual, 'selgradethree', 'grade3span');   
    createSelectBoxUL(qual, 'selgradefour', 'grade4span');   
    createSelectBoxUL(qual, 'selgradefive', 'grade5span');   
    createSelectBoxUL(qual, 'selgradesix', 'grade6span');   
    $$DD("qualSubjName4").value = "Subject"; $$DD("qualSubjName5").value = "Subject"; $$DD("qualSubjName6").value = "Subject";
    $$DD("qualSubjName4_hidden").value = ""; $$DD("qualSubjName5_hidden").value = ""; $$DD("qualSubjName6_hidden").value = "";
    $$DD("qualSubjName1").value = "Subject"; $$DD("qualSubjName2").value = "Subject";$$DD("qualSubjName3").value = "Subject"; 
    $$DD("qualSubjName1_hidden").value = ""; $$DD("qualSubjName2_hidden").value = "";$$DD("qualSubjName3_hidden").value = ""; 
    if($$DD("ucascalc")){$$DD("ucascalc").className = "yrsb-fst nors";}
    if($$DD("csucascalc")){$$DD("csucascalc").style.display = "none";}
  }else if(qual == 'TP'){
    $$DD("fileset1").style.display = "block";
    $$DD("fileset2").style.display = "block";
    $$DD("fileset3").style.display = "block";
    $$DD("fileset4").style.display = "none";
    $$DD("fileset5").style.display = "none";
    $$DD("fileset6").style.display = "none";

    displayBlockUCASInput('gradediv', 'none', 6);
    displayBlockUCASInput('ucas', 'block', 3);
    $$DD("qualSubjName4").value = "Subject"; $$DD("qualSubjName5").value = "Subject"; $$DD("qualSubjName6").value = "Subject";
    $$DD("qualSubjName4_hidden").value = ""; $$DD("qualSubjName5_hidden").value = ""; $$DD("qualSubjName6_hidden").value = "";
    $$DD("qualSubjName1").value = "Subject"; $$DD("qualSubjName2").value = "Subject";$$DD("qualSubjName3").value = "Subject"; 
    $$DD("qualSubjName1_hidden").value = ""; $$DD("qualSubjName2_hidden").value = "";$$DD("qualSubjName3_hidden").value = "";     
    if($$DD("ucascalc")){$$DD("ucascalc").className = "yrsb-fst hlp";}
    if($$DD("csucascalc")){$$DD("csucascalc").style.display = "block";}
  }
}
function displayBlockUCASInput(id, type, count){  
  for(var i = 1; i <=count; i++){
    var fId = id+i;    
    $$DD(fId).style.display = type;    
  }
}
function createSelectBoxUL(qual, selectBoxId, spanId){
  var valueArray;
  if(qual == 'AL'){
    valueArray = ["A*", "A", "B", "C", "D", "E"];
  }else if(qual == 'H' || qual == 'AH'){
    valueArray = ["A", "B", "C", "D"]
  }
  var arrayLength = valueArray.length; 
  if($$DD(selectBoxId) != null){
    $$(selectBoxId).innerHTML = "";
    for(var i = 0; i < arrayLength; i++){
      var option = document.createElement("option");
      option.value = valueArray[i];
      option.appendChild(document.createTextNode(valueArray[i]));
      $$DD(selectBoxId).appendChild(option);
    }
    $$DD(spanId).innerHTML = valueArray[0];
  }
}
function showMoreLess(obj, rowNum){
  var id = obj.id;
  if(id == 'more_'+rowNum){
    document.getElementById('expandtable_'+rowNum).style.display = 'block';
    document.getElementById('collapsetable_'+rowNum).style.display = 'none';
  }else if(id == 'less_'+rowNum){
    document.getElementById('collapsetable_'+rowNum).style.display = 'block';
    document.getElementById('expandtable_'+rowNum).style.display = 'none';
  }
}

function empInfo(jacsCode, searchGrades, equiTP){
  $$DD("empJacsCode").value = jacsCode;
  $$DD("searchGrades").value = searchGrades;
  $$DD("equiTP").value = equiTP; 
  document.getElementById('ulsearchform').action = ulSubmitURL+"?pageno=3";
  document.getElementById('ulsearchform').submit();
}
  
function showUniEmpInfo(ukprn, collegeId, indexId){
  //alert('inside the function');
  if(trimString($$DD("ukprn_"+indexId).innerHTML) == "" ){
    //alert('inside the function innerHTML empty');
    var loadId = "load_"+indexId;
    var loadImg = '<img src="'+wu_scheme+document.domain+'/wu-cont/images/ajax-ldr.gif"/>';//Added wu_scheme by Indumathi.S Mar-29-2016
    if($$(loadId)){
      $$(loadId).innerHTML = loadImg;
      $$(loadId).style.display = 'block';
    }    
    var uniUKPRN = ukprn;
    var ulLogId = $$DD("empLogid").value;
    var ulJacscode = $$DD("empJacsCode").value;
    var grades = $$DD("grades").value;
    var tp = $$DD("tariffpoints").value;
    var url="/degrees/loadUniEmpInfo.html?ukprn="+uniUKPRN+"&logId="+ulLogId+"&jacscode="+ulJacscode+"&collegeid="+collegeId+"&searchgrades="+grades+"&equitp="+tp;    
    var ajax=new sack();
    ajax.requestFile = url;	
    ajax.onCompletion = function(){ showUniEmpInfoResponse(ajax, uniUKPRN, indexId); };	
    ajax.runAJAX();  
  }else{    
    if($$DD("ukprn_"+indexId).style.display == 'none'){
      $$DD("ukprn_"+indexId).style.display= 'block';
    $$DD("div_ul_"+indexId).className= 'hd';
    }else{
      $$DD("ukprn_"+indexId).style.display= 'none';
    $$DD("div_ul_"+indexId).className= 'hd act';
    }
  }
}
function showUniEmpInfoResponse(ajax, uniUKPRN, indexId){
  var ukprn = uniUKPRN;
  if(ajax.response != ''){
    $$DD("ukprn_"+indexId).innerHTML= ajax.response;
    $$DD("ukprn_"+indexId).style.display= 'block';
    $$DD("div_ul_"+indexId).className= 'hd';
    if($$DD(ukprn+"-JS1")){eval($$DD(ukprn+"-JS1").innerHTML);}
    if($$DD(ukprn+"-JS2")){eval($$DD(ukprn+"-JS2").innerHTML);}
    var loadId = "load_"+indexId;
    if($$(loadId)){
      $$(loadId).style.display = 'none';
    }    
  }else{
  } 
}
function showIWanttobeUniEmpInfo(ukprn, collegeId, indexId){
  if(trimString($$DD("ukprn_"+indexId).innerHTML) == "" ){
    var uniUKPRN = ukprn;
    var searchType = $$DD("iwantSearchType").value;
    var searchCode = $$DD("iwantSearchCode").value;
    var loadId = "load_"+indexId;
    var loadImg = '<img src="'+wu_scheme+document.domain+'/wu-cont/images/ajax-ldr.gif"/>';//Added wu_scheme by Indumathi.S Mar-29-2016
    if($$(loadId)){
      $$(loadId).innerHTML = loadImg;
      $$(loadId).style.display = 'block';
    }    
    var url="/degrees/loadIWanttobeUniEmpInfo.html?ukprn="+uniUKPRN+"&searchtype="+searchType+"&searchcode="+searchCode+"&collegeid="+collegeId;    
    var ajax=new sack();
    ajax.requestFile = url;	
    ajax.onCompletion = function(){ showIWanttobeUniEmpInfoResponse(ajax, uniUKPRN, indexId); };	
    ajax.runAJAX();  
  }else{    
    if($$DD("ukprn_"+indexId).style.display == 'none'){
      $$DD("ukprn_"+indexId).style.display= 'block';
    $$DD("div_ul_"+indexId).className= 'hd';
    }else{
      $$DD("ukprn_"+indexId).style.display= 'none';
    $$DD("div_ul_"+indexId).className= 'hd act';
    }  
  }
}
function showIWanttobeUniEmpInfoResponse(ajax, uniUKPRN, indexId){  
  var ukprn = uniUKPRN;  
  var loadId = "load_"+indexId;
  if(trimString(ajax.response) != ""){
    var searchName = $$DD("iwantSearchName").value;
    $$DD("ukprn_"+indexId).innerHTML= ajax.response;
    $$DD("ukprn_"+indexId).style.display= 'block';
    $$DD("div_ul_"+indexId).className= 'hd';
    $$DD("text_"+ukprn).innerHTML= searchName;        
    if($$(loadId)){
      $$(loadId).style.display = 'none';
    }     
  }else{    
    if($$(loadId)){
      $$(loadId).style.display = 'none';
    }
  } 
}
function uniEmpInfoRatePaginationSort(pageNo, sorybywhich, sortbyhow, ulsearchtype){
  var uniPageNo = pageNo;
  var uniSortbywhich = sorybywhich;
  var uniSortbyhow = sortbyhow;
  var url; var grades; var eqiTP;
  var loadImg = '<img src="'+wu_scheme+document.domain+'/wu-cont/images/ldr.gif"/>';//Added wu_scheme by Indumathi.S Mar-29-2016
  if($$("load")){
    $$("load").innerHTML = loadImg;
    $$("load").style.display = 'block';
  }  
  var uniId = $$DD('uniName_hidden').value;
  var uniUkprn = $$DD('uniName_ukprn').value;   
  if(ulsearchtype == 'WCID'){    
    var ulLogId = $$DD("empLogid").value;
    var ulJacscode = $$DD("empJacsCode").value;
    grades = $$DD("grades").value;
    eqiTP = $$DD("tariffpoints").value;    
    url="/degrees/whatcanidoempratesortpagination.html?uniId="+uniId+"&uniUkprn="+uniUkprn+"&pageno="+uniPageNo+"&logId="+ulLogId+"&jacscode="+ulJacscode+"&sortbywhich="+uniSortbywhich+"&sortbyhow="+uniSortbyhow;
  }else{   
    var ulLogId = $$DD("empLogid").value;
    var searchType = $$DD("iwantSearchType").value;
    var searchCode = $$DD("iwantSearchCode").value;
    url="/degrees/iwanttobeempratesortpagination.html?uniId="+uniId+"&uniUkprn="+uniUkprn+"&pageno="+uniPageNo+"&searchtype="+searchType+"&searchcode="+searchCode+"&sortbywhich="+uniSortbywhich+"&sortbyhow="+uniSortbyhow;    
  }  
  var ajax=new sack();
  ajax.requestFile = url;  
  ajax.onCompletion = function(){    
    if(ulsearchtype == 'WCID'){
      uniEmpRateSortPaginationResponse(ajax, grades, eqiTP); 
    }else{        
      IWantotbeEmpRateSortPaginationResponse(ajax); 
    }
  };    
  ajax.runAJAX();
  if($$("load")){
    $$("load").style.display = 'none';
  }
}
function uniEmpRateSortPaginationResponse(ajax, grades, eqiTP){  
  $$DD("uniempinfo").innerHTML= ajax.response;
  $$DD("grades").value = grades;
  $$DD("tariffpoints").value = eqiTP;  
  if($$("load")){
    $$("load").style.display = 'none';
  }  
}
function IWantotbeEmpRateSortPaginationResponse(ajax){    
  $$DD("uniempinfo").innerHTML= ajax.response;
  if($$("load")){
    $$("load").style.display = 'none';
  }  
} 
function ulSearchResults(jacsSubject,jacsCode, searchGrades){
  var navKwd = trimString(jacsSubject);
  var qualDesc = 'degree';
  var navKwd1 = replaceAll(navKwd," ","+");
  var navKwd2 = replaceAll(navKwd," ","-");
  var country1 = 'united+kingdom';
  var country2 = 'united-kingdom';
  var srGrades = searchGrades;
  var srQual = $$DD("searchQual").value;  
  var contextPath='/degrees';
  var url1 = "/"+qualDesc+"-courses/search?jacs="+jacsCode;
  var url2='';
  if(srQual!="0" && srGrades!="0"){
    url2 = "&entry-level="+srQual+"&entry-points="+srGrades;
  }  
  var finalUrl = url1 + url2;
  finalUrl = finalUrl.toLowerCase();
  window.open(finalUrl,'_blank');
	return false;
}
function iwnattobeEmpInfo(){
  var csUlSubmitURL = ulSubmitURL+"?pageno=3";  
  document.getElementById('ulsearchform').action = csUlSubmitURL;
  document.getElementById('ulsearchform').submit();
  return false;  
}
function tryAgaingFormSubmit(){
  var csUlSubmitURL = ulSubmitURL+"?pageno=1";
  document.getElementById('tryagainform').action = csUlSubmitURL;
  document.getElementById('tryagainform').submit();
  return false;  
}
function prepopulateValues(){
  var qual = $$DD("qualsel").value;
  var gradeone = $$DD("selgradeone").value;
  var gradetwo = $$DD("selgradetwo").value;
  var gradethree = $$DD("selgradethree").value;
  var gradefour = $$DD("selgradefour").value;
  var gradefive = $$DD("selgradefive").value;
  var gradesix = $$DD("selgradesix").value;
  var qualVal = $$DD("qualsel");
  var text = qualVal.options[qualVal.selectedIndex].text;
  document.getElementById("qualspanul").innerHTML = text;  
  if(qual == 'AL'){
    $$DD("fileset1").style.display = "block";
    $$DD("fileset2").style.display = "block";
    $$DD("fileset3").style.display = "block";
    $$DD("fileset4").style.display = "block";
    $$DD("fileset5").style.display = "none";
    $$DD("fileset6").style.display = "none";
    displayBlockUCASInput('gradediv', 'block', 6);
    displayBlockUCASInput('ucas', 'none', 6);    
    $$DD("grade1span").innerHTML = gradeone;
    $$DD("grade2span").innerHTML = gradetwo;
    $$DD("grade3span").innerHTML = gradethree;
    $$DD("grade4span").innerHTML = gradefour;
    
  }else if(qual == 'H' || qual == 'AH'){
    displayBlockUCASInput('fileset', 'block', 6);
    displayBlockUCASInput('gradediv', 'block', 6);
    displayBlockUCASInput('ucas', 'none', 6);

    $$DD("grade1span").innerHTML = gradeone;
    $$DD("grade2span").innerHTML = gradetwo;
    $$DD("grade3span").innerHTML = gradethree;
    $$DD("grade4span").innerHTML = gradefour;
    $$DD("grade5span").innerHTML = gradefive;
    $$DD("grade6span").innerHTML = gradesix;

  }else if(qual == 'TP'){
    $$DD("fileset1").style.display = "block";
    $$DD("fileset2").style.display = "block";
    $$DD("fileset3").style.display = "block";
    $$DD("fileset4").style.display = "none";
    $$DD("fileset5").style.display = "none";
    $$DD("fileset6").style.display = "none";

    displayBlockUCASInput('gradediv', 'none', 6);
    displayBlockUCASInput('ucas', 'block', 3);
  }  
}
function subjectValidation(){
  var qual = $$DD("qualsel").value;
  var noOfSub;
  var flag = 'ATLEASTONE';
  var j = 0;
  if(qual == 'AL'){
    noOfSub = 4; 
  }else if(qual == 'H' || qual == 'AH'){
    noOfSub = 6; 
  }else{
    noOfSub = 3; 
  }  
  for(var i = 1; i <=noOfSub; i++){  
    var currentSubStyleId = "qualSubjName"+i+"_hidden";  
    var currentSub = trimString($$DD(currentSubStyleId).value);  
    if(currentSub != 'Subject' && currentSub != ""){
      flag = 'SUBMIT';
    }  
    if(qual == 'TP'){
      var ucasPoints = trimString($$DD("ucas"+i).value);
      if(ucasPoints == "" || ucasPoints == null){
        j = parseInt(j) + 1;
      }
      if(ucasPoints != "" && (isNaN(ucasPoints) || ucasPoints > 999)){
        flag = 'NOTVALIDTARIFF';
        break;
      }
    }
  }
  if( j == 3){
    flag = 'ATLEASTONETP';
  }
  return flag;
}

function whatcanidothirdtosecond(){
  document.getElementById('thirdtosecond').action = ulSubmitURL+"?pageno=2";
  document.getElementById('thirdtosecond').submit();
  return false; 
}

function whatcanidobacktosubject(obj){
  var id = obj.id;  
  document.getElementById('thirdtosecond').action = ulSubmitURL+"?pageno=2";
  document.getElementById('thirdtosecond').submit();
  return false; 
}

function iwanttobetryagain(){
  var csUlSubmitURL = ulSubmitURL+"?pageno=1";  
  document.getElementById('tryagainform').action = csUlSubmitURL;
  document.getElementById('tryagainform').submit();
}

function iwanttobeThirdtoSecond(){
  var csUlSubmitURL = ulSubmitURL+"?pageno=2";
  document.getElementById('iwantobethirdtosecond').action = csUlSubmitURL;
  document.getElementById('iwantobethirdtosecond').submit();
}

function ulGAEventTracking(category, action, label, gaaccount){
  var eventCategory = 'widget-what-can-i-do';
  if(category == 'IWTB'){
    eventCategory = 'widget-i-want-to-be';
  }
  var eventAction = 'view degrees button';
  if(action == 'SG'){
    eventAction = 'subject guide button';
  }
  var eventLabel = label.toLowerCase();;
  var eventVal = 1;
  var booleanValue = true;  
  ga('create', gaaccount , 'auto', {'sampleRate': 80 , 'name': 'salesTracker'}); //Added by Amir for UA logging for 24-NOV-15 release
  ga('salesTracker.send', 'event', eventCategory , eventAction , eventLabel , eventVal , {nonInteraction: booleanValue}); //UA logging for 24-NOV-15 release  
}

function ulOnloadClick(){  
  if($$DD('div_ul_2')){    
    $$DD('div_ul_2').onclick();
  }else{
    if($$DD('div_ul_0')){
        $$DD('div_ul_0').onclick();
    }    
  }
}
function specialTextValidate(name) {
var specialChar = "\\#%;?/_&*!@$^()+={}[]|:<>.~`";   
if(name.indexOf('"') != -1){return false;}
for(var i=0; i<specialChar.length; i++){  
var str = (name).indexOf(specialChar.charAt(i));
if(str != -1) {return false;}}return true;
}
function uniList(event, object){    
    var uniNameText = '';    
    if(specialTextValidate(trimString(object.value))){        
        if(trimString(object.value)!=""){
            ajax_showOptions(object,'uninamelist',event,"indicator",'ulqual='+ uniNameText);    
        }else{
            $$DD("errordiv").style.display = "block";
            $$DD("errordiv").innerHTML = "Please select uni from the list.";
        }  
    }else{        
        $$DD("errordiv").style.display = "block";
        $$DD("errordiv").innerHTML = "Please enter valid text and select uni from the list.";
        $$DD("uniName_hidden").value = "";
        $$DD("uniName_ukprn").value = "";        
    }
}
function keypressUniSearchSubmit(event){
    if(event.keyCode==13){
        return fetchWhatCanIDoUniversityData('uniLoad');
    }
}
//05-Aug-2014
function fetchWhatCanIDoUniversityData(id) {   
   var contextPath = "/degrees";   
   var uniId = trimString($$DD('uniName_hidden').value);
   var uniUkprn = trimString($$DD('uniName_ukprn').value);
   var uniName = trimString($$DD('uniName').value);
   var uniNameDisplay = trimString($$DD('uniName_display').value);
   var logId = trimString($$DD('empLogid').value);
   var empJacsCode = trimString($$DD('empJacsCode').value);
   var searchQual = trimString($$DD('searchQual').value);
   var empsortByWhich = trimString($$DD('empsortByWhich').value);
   var empsortByHow = trimString($$DD('empsortByHow').value);
    var url;
   var ajax = new sack();
   if(uniId!=""){
       if($$DD(id).className=="unisbmt"){
           if(uniName!=uniNameDisplay){
             if($$DD("errordiv")){
                $$("errordiv").style.display = "block";
                $$("errordiv").innerHTML = "Please select uni from ajax list.";
              }
                return false;
           }else{
              if($$DD("errordiv")){
                $$("errordiv").style.display = "none";
                $$("errordiv").innerHTML = "";
              }
           }
           url = contextPath + "/whatCanIDoUniversityInfoAjax.html?uniId="+uniId+"&uniUkprn="+uniUkprn+"&logId="+logId+"&empJacsCode="+empJacsCode+"&searchQual="+searchQual+"&empsortByWhich="+empsortByWhich+"&empsortByHow="+empsortByHow+"&objectFlag="+id;     
       }else if($$DD(id).className=="uniclr"){
            $$DD('uniName_hidden').value = "";
            $$DD('uniName_ukprn').value = "";
            $$DD('uniName').value = "";
           url = contextPath + "/whatCanIDoUniversityInfoAjax.html?logId="+logId+"&empJacsCode="+empJacsCode+"&searchQual="+searchQual+"&empsortByWhich="+empsortByWhich+"&empsortByHow="+empsortByHow+"&objectFlag="+id;     
       }   
       //alert("url : "+url);
       ajax.requestFile = url;
       ajax.onCompletion = function(){ loadWhatCanIDoUniversityData(ajax, id); };
       ajax.runAJAX();
   }else{
      if($$DD("errordiv")){
        $$("errordiv").style.display = "block";
        $$("errordiv").innerHTML = "Please select uni from ajax list.";
      }
   }
}

function loadWhatCanIDoUniversityData(ajax, id){    
   $$("uniData").innerHTML = "";
   var response = ajax.response;
   $$("uniData").innerHTML = response;
   if($$DD(id).className=="unisbmt"){
    $$("pgn").style.display = "none";
    $$("uniClear").style.display = "block";
   }
   if($$DD(id).className=="uniclr"){
    $$("pgn").style.display = "inline-block";
    $$("uniClear").style.display = "none";
    if($$DD("errordiv")){
      $$DD("errordiv").style.display = "none";
      $$DD("errordiv").innerHTML = "";
    }
    $$("uniName").value = "Enter uni name";
    $$("uniName").style.color='#a6a6a6';
   }
   ulOnloadClick();
}
function keypressIWantToBeUniSearchSubmit(event){
    if(event.keyCode==13){
        return fetchIWantToBeUniversityData('uniLoad');
    }
}
//05-Aug-2014
function fetchIWantToBeUniversityData(id) {    
   var contextPath = "/degrees";
   var uniId = trimString($$DD('uniName_hidden').value);
   var uniUkprn = trimString($$DD('uniName_ukprn').value);
   var uniName = trimString($$DD('uniName').value);
   var uniNameDisplay = trimString($$DD('uniName_display').value);
   var logId = trimString($$DD('empLogid').value);
   var iwantSearchType = trimString($$DD('iwantSearchType').value);
   var iwantSearchCode = trimString($$DD('iwantSearchCode').value);
   var iwantSearchName = trimString($$DD('iwantSearchName').value);
     var url;
   var ajax = new sack();
   
   if(uniId!=""){
       if($$DD(id).className=="unisbmt"){
           if(uniName!=uniNameDisplay){
               if($$DD("errordiv")){
                $$("errordiv").style.display = "block";
                $$("errordiv").innerHTML = "Please select uni from ajax list.";
               }
                return false;
           }else{
              if($$DD("errordiv")){
                $$("errordiv").style.display = "none";
                $$("errordiv").innerHTML = "";
              }
           }
           url = contextPath + "/iwanttobeuniversityinfoajax.html?uniId="+uniId+"&uniUkprn="+uniUkprn+"&logId="+logId+"&iwantSearchType="+iwantSearchType+"&iwantSearchCode="+iwantSearchCode+"&iwantSearchName="+iwantSearchName+"&objectFlag="+id;
       }else if($$DD(id).className=="uniclr"){
            $$DD('uniName_hidden').value = "";
            $$DD('uniName_ukprn').value = "";
            $$DD('uniName').value = "";
            $$DD('uniName_display').value="";
           url = contextPath + "/iwanttobeuniversityinfoajax.html?logId="+logId+"&iwantSearchType="+iwantSearchType+"&iwantSearchCode="+iwantSearchCode+"&iwantSearchName="+iwantSearchName+"&objectFlag="+id;
       }   
       //alert("url : "+url);
       ajax.requestFile = url;
       ajax.onCompletion = function(){ loadIWantToBeUniversityData(ajax, id); };
       ajax.runAJAX();
   }else{
      if($$DD("errordiv")){
        $$("errordiv").style.display = "block";
        $$("errordiv").innerHTML = "Please select uni from ajax list.";
      }
   }
}

function loadIWantToBeUniversityData(ajax, id){        
   document.getElementById("uniData").innerHTML = "";
   var response = ajax.response;      
   document.getElementById("uniData").innerHTML = response;   
   if($$(id).className=="unisbmt"){    
    $$("pgn").style.display = "none";
    $$("uniClear").style.display = "block";
   }
   if($$(id).className=="uniclr"){    
    $$("pgn").style.display = "inline-block";
    $$("uniClear").style.display = "none";
    if($$DD("errordiv")){
      $$("errordiv").style.display = "none";
      $$("errordiv").innerHTML = "";
    }
    $$("uniName").value = "Enter uni name";
    $$("uniName").style.color='#a6a6a6';
   }
   ulOnloadClick();
}
function employmentShowHide(rowIndex){    
    var aTagId = "xmstcom_"+rowIndex;
    var divId = "collapsetable_"+rowIndex; 
    var divId1 = "expandtable_"+rowIndex;     
    if($$(aTagId).className == "sh pls"){
        $$(aTagId).className = "sh mns";
        if($$(divId1).style.display == "none"){
            $$(divId).style.display = "block";
        }
        if($$(divId).style.display == "none"){
            $$(divId1).style.display = "block";
        }
        $$(aTagId).innerHTML = "<em>show</em>";
    }else{
        $$(aTagId).className = "sh pls";
        $$(divId).style.display = "none";
        $$(divId1).style.display = "none";
        $$(aTagId).innerHTML = "<em>show</em>";
    }        
 }