/**
 * Author:	Thiyagu G
 * Date:	wu528_20140826
 * Purpose:	common JS function, and this will be loaded only in about us pages
 */
//
function trimString(inString){
  return inString.replace(/^\s+|\s+$/g,"");
}
//
function $$(id){
  return document.getElementById(id);
}
//
function specialTextValidate(name) {
    var specialChar = "\\#%;?/_&*!@$^()+={}[]|:<>~`";   
    if(name.indexOf('"') != -1){return false;}
    for(var i=0; i<specialChar.length; i++){  
    var str = (name).indexOf(specialChar.charAt(i));
    if(str != -1) {return false;}}return true;
}
function specialDateTextValidate(name) {    
    var specialChar = "\\#%;?_&*!@$^()+={}[]|:<>.~`";   
    if(name.indexOf('"') != -1){return false;}
    for(var i=0; i<specialChar.length; i++){  
    var str = (name).indexOf(specialChar.charAt(i));
    if(str != -1) {return false;}}return true;
}
function specialEmailTextValidate(name) {
    var specialChar = "\\#%;?/&*!$^()+={}[]|:<>~`";   
    if(name.indexOf('"') != -1){return false;}
    for(var i=0; i<specialChar.length; i++){  
    var str = (name).indexOf(specialChar.charAt(i));
    if(str != -1) {return false;}}return true;
}
function specialAddressTextValidate(name) {
     var specialChar = "\\%;?&*!@$^()+={}[]|:<>~`";   
    if(name.indexOf('"') != -1){return false;}
    for(var i=0; i<specialChar.length; i++){  
    var str = (name).indexOf(specialChar.charAt(i));
    if(str != -1) {return false;}}return true;
}
function checkSlashForDate(name) {
    var specialChar = "/";
    if(name.indexOf('"') != -1){return false;}
    for(var i=0; i<specialChar.length; i++){  
    var str = (name).indexOf(specialChar.charAt(i));
    if(str != -1) {return false;}}return true;
}
//
function isValidEmail(email){    
    if(email.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) == -1){
        return false;
    }
    return true;
}
//
function replaceAll(replacetext, findstring, replacestring){
	var strReplaceAll = replacetext;
	var intIndexOfMatch = strReplaceAll.indexOf(findstring)
	while(intIndexOfMatch != -1){
		strReplaceAll = strReplaceAll.replace(findstring, replacestring);
		intIndexOfMatch = strReplaceAll.indexOf(findstring)
	}
	return strReplaceAll
}
function hideExpandDiv(divids){document.getElementById(divids).style.display=document.getElementById(divids).style.display=='none' ? 'block' : 'none'}
//
function setDefault(obj, text){    
  var id = obj.id;
  if(document.getElementById(id).value == ""){
    document.getElementById(id).value = text;    
  }
  $$(id).style.color = '#888888';  
  var id = obj.id;  
  var errDiv = id+"_error";
  $$(errDiv).innerHTML = "";
  $$(errDiv).style.display = 'none';    
}
function clearDefault(obj, text){
  var id = obj.id;  
  if($$(id).value == text){
    $$(id).value = "";
  }
  $$(id).style.color = '#000000';
}
function bookVistFormValidation(event, object, text){
    var id = object.id;
    var errDiv = id+"_error";      
    //event.keyCode != 13
    if(object.value!="First name*" && object.value!="Last name*" && object.value!="Email address*" && object.value!="Phone*" && object.value!="Job title*"
    && object.value!="School name*" && object.value!="Town name*" && object.value!="DD / MM / YY*" && object.value!="School name" && object.value!="Address line 1" 
    && object.value!="Address line 2" && object.value!="Town / City" && object.value!="Postcode"){
        if(object.value != "" && id != "emailAddress"  && id != "date" && id != "addressLine1" && id != "addressLine2" && id != "town"){
            if(!specialTextValidate(trimString(object.value))){
                setErrorDiv(errDiv, "Please enter valid text in "+text);                
                return false;
            }else{
                clearErrorDiv(errDiv);                
              }
        }
        if(object.value != "" && id == "emailAddress"){
            if(!specialEmailTextValidate(trimString(object.value))){
                setErrorDiv(errDiv, "Please enter valid text in "+text);
                return false;
            }else{
                clearErrorDiv(errDiv);
              }
        }
        if(object.value != "" && id == "date"){
            if(!specialDateTextValidate(trimString(object.value))){
                setErrorDiv(errDiv, "Please enter valid text in "+text);
                $$("date").focus();
                return false;
            }else{
                clearErrorDiv(errDiv);
              }
        }
        if(object.value != "" && (id == "addressLine1" || id == "addressLine2" || id == "town")){
            if(!specialAddressTextValidate(trimString(object.value))){
                setErrorDiv(errDiv, "Please enter valid text in "+text);
                $$("date").focus();
                return false;
            }else{
                clearErrorDiv(errDiv);
              }
        }        
        if(object.value != "" && id == "phone"){
            if(!isNumeric(object.value)){
                setErrorDiv(errDiv, "Please enter numbers only");
                $$("phone").focus();
                return false;
            }else{
                clearErrorDiv(errDiv);
            }
        }
        if(object.value != "" && id != "phone" && id != "emailAddress" && id != "date" && id != "postCode" && id != "addressLine1" && id != "addressLine2" && id != "town"){        
            if(!isAlphabetic(object.value)){
                setErrorDiv(errDiv, "Please enter alphabets only");
                $$(id).focus();
                return false;
            }else{
                clearErrorDiv(errDiv);                
            }        
        }    
    } 
}
function isNumeric(input){
    return (input - 0) == input && (''+input).replace(/^\s+|\s+$/g, "").length > 0;
}
function isAlphabetic(input){
    var alpha = /^[a-zA-Z-,.']+(\s{0,1}[a-zA-Z-,.' ])*$/;
    if(!input.match(alpha)){return false;}else{return true;}
}

function otherTextboxEnabled(){
    var aboutus = document.schoolvisits.aboutus;
    for (i=0;i<aboutus.length;i++) {
      if (aboutus[i].checked) {
          if(aboutus[i].value == "Other"){
            $$("otherTxtbox").style.display = 'block';
          }else{
            $$("otherTxtbox").style.display = 'none';
          }
      }
    }
}
function submitSchoolVisitsPage(){  
      var aboutusFlag = false;
      var aboutusTxt = "";
      var i;    
      var firstName = trimString($$("firstName").value);
      var lastName = trimString($$("lastName").value);
      var emailAddress = trimString($$("emailAddress").value);
      var phone = trimString($$("phone").value);
      var jobTitle = trimString($$("jobTitle").value);
      var schoolName = trimString($$("schoolName").value);
      var town = trimString($$("town").value);
      var date = trimString($$("date").value);      
      var time = $$("time").value;
			var message = $$("message").value;//Prabha
      var errReturnFlag = true;
      if(firstName !="" && firstName == "First name*"){
          setErrorDiv("firstName_error", "Please enter your first name");          
          errReturnFlag = false;
      }else if(firstName == ""){
          setErrorDiv("firstName_error", "Please enter your first name");          
          errReturnFlag = false;
      }else if(!specialTextValidate(trimString(firstName))){
          setErrorDiv("firstName_error", "Please enter valid text in first name");
          errReturnFlag = false;
      }else if(!isAlphabetic(trimString(firstName))){
            setErrorDiv("firstName_error", "Please enter alphabets only");
            errReturnFlag = false;                        
      }else{
        clearErrorDiv("firstName_error");        
      }
      if(lastName !="" && lastName == "Last name*"){
          setErrorDiv("lastName_error", "Please enter your last name");          
          errReturnFlag = false;
      }else if(lastName == ""){
          setErrorDiv("lastName_error", "Please enter your last name");
          errReturnFlag = false;
      }else if(!specialTextValidate(trimString(lastName))){
          setErrorDiv("lastName_error", "Please enter valid text in last name");
          errReturnFlag = false;
      }else if(!isAlphabetic(trimString(lastName))){
            setErrorDiv("lastName_error", "Please enter alphabets only");
            errReturnFlag = false;                        
      }else{        
        clearErrorDiv("lastName_error");
      }
      if(emailAddress !="" && emailAddress == "Email address*"){        
          setErrorDiv("emailAddress_error", "Please enter your email address");
          errReturnFlag = false;
      }else if(emailAddress == ""){        
          setErrorDiv("emailAddress_error", "Please enter your email address");          
          errReturnFlag = false;
      }else if(!specialEmailTextValidate(trimString(emailAddress))){
          setErrorDiv("emailAddress_error", "Please enter valid text in email address");
          errReturnFlag = false;
      }else if(!isValidEmail(trimString(emailAddress))){
          setErrorDiv("emailAddress_error", "Email id is invalid");                    
          errReturnFlag = false;
      }else{        
        clearErrorDiv("emailAddress_error");
      }
      if(phone !="" && phone == "Phone*"){
          setErrorDiv("phone_error", "Please enter your phone number");          
          errReturnFlag = false;
      }else if(phone == ""){
          setErrorDiv("phone_error", "Please enter your phone number");
          errReturnFlag = false;
      }else if(!specialTextValidate(trimString(phone))){
          setErrorDiv("phone_error", "Please enter valid phone number");
          errReturnFlag = false;
      }else if(!isNumeric(trimString(phone))){
            setErrorDiv("phone_error", "Please enter numeric value");//Modified error message by Indumathi Jan-27-16
            errReturnFlag = false;                        
      }else{        
        clearErrorDiv("phone_error");
      }
      if(jobTitle !="" && jobTitle == "Job title*"){
           setErrorDiv("jobTitle_error", "Please enter your job title");
           errReturnFlag = false;
      }else if(jobTitle == ""){
            setErrorDiv("jobTitle_error", "Please enter your job title");
            errReturnFlag = false;
      }else if(!specialTextValidate(trimString(jobTitle))){
            setErrorDiv("jobTitle_error", "Please enter valid text in job title");
            errReturnFlag = false;
      }else if(!isAlphabetic(trimString(jobTitle))){
            setErrorDiv("jobTitle_error", "Please enter alphabets only");
            errReturnFlag = false;
      }else{        
        clearErrorDiv("jobTitle_error");
      }      
      if(schoolName !="" && schoolName == "School name*"){
           setErrorDiv("schoolName_error", "Please enter your school name");
           errReturnFlag = false;           
      }else if(schoolName == ""){
            setErrorDiv("schoolName_error", "Please enter your school name");
            errReturnFlag = false;
      }else if(!specialTextValidate(trimString(schoolName))){
            setErrorDiv("schoolName_error", "Please enter valid text in school name");
            errReturnFlag = false;
      }else if(!isAlphabetic(trimString(schoolName))){
            setErrorDiv("schoolName_error", "Please enter alphabets only");
            errReturnFlag = false;
      }else{
        clearErrorDiv("schoolName_error");        
      }      
      if(town !="" && town == "Town name*"){
          setErrorDiv("town_error", "Please enter your town name");           
          errReturnFlag = false;          
      }else if(town == ""){
          setErrorDiv("town_error", "Please enter your town name");
          errReturnFlag = false;
      }else if(!specialTextValidate(trimString(town))){
          setErrorDiv("town_error", "Please enter valid text in town name");
          errReturnFlag = false;
      }else if(!isAlphabetic(trimString(town))){
            setErrorDiv("town_error", "Please enter alphabets only");
            errReturnFlag = false;
      }else{
        clearErrorDiv("town_error");
      }     
      if(date !="" && date == "DD / MM / YY*"){
          setErrorDiv("date_error", "Please enter your date of visit");           
          errReturnFlag = false;
      }else if(date == ""){
          setErrorDiv("date_error", "Please enter your date of visit");            
          errReturnFlag = false;
      }else if(!specialDateTextValidate(trimString(date))){
          setErrorDiv("date_error", "Please enter a date in correct format - DD/MM/YY");
          errReturnFlag = false;
      }else if(checkSlashForDate(trimString(date))){
          setErrorDiv("date_error", "Please enter a date in correct format - DD/MM/YY");
          errReturnFlag = false;
      }else{
        clearErrorDiv("date_error");
      }      
      if(time == "Choose time"){
          setErrorDiv("time_error", "Please choose your time of visit");
          errReturnFlag = false;          
      }else{
          clearErrorDiv("time_error");          
      }      
      if(date !="" && date != "DD / MM / YY*"){
          var matches = /(\d{2})[/\/](\d{2})[/\/](\d{2})/.exec(date);
          if (matches == null){
              setErrorDiv("date_error", "Please enter a date in correct format - DD/MM/YY");
              errReturnFlag = false;
          }
      }
      var dtStatus = isDate(date);       
      if(!dtStatus){
        errReturnFlag = false;
      }
      
    var aboutus = document.schoolvisits.aboutus;    
    for (i=0;i<aboutus.length;i++) {    
      if (aboutus[i].checked) {          
          aboutusFlag = true;
          aboutusTxt = aboutus[i].value;          
          if(aboutusTxt == "Other"){
            $$("otherTxtbox").style.display = 'block';            
          }
      }
    }
    if(!aboutusFlag){
        setErrorDiv("other_error", "Please select how do you hear about us?");
        errReturnFlag = false;
    }else{
        clearErrorDiv("other_error");        
    }
    if($$("otherTxtbox").style.display == "block"){        
        var otherTxt = trimString($$("otherTxt").value);
        if(otherTxt !="" && otherTxt == "Please state"){
            setErrorDiv("other_error", "Please enter other option");
            errReturnFlag = false;
        }else if(otherTxt ==""){
            setErrorDiv("other_error", "Please enter other option");            
            errReturnFlag = false;
          }else{
            clearErrorDiv("other_error");
          }
    }    
      if($$("message")){       
          var invalidWord = containsSpecialCharacter($$("message"));
          var htmlTags = containsHtmlTags($$('message')); //Added by Prabha on 13_06_17
          if((invalidWord != null && trimString(invalidWord) != "") || (htmlTags != null && trimString(htmlTags) != "")){ 
            setErrorDiv("message_error", "There are some illegal characters in the text you supplied. Tut tut!");
            errReturnFlag = false;
          }else{
           clearErrorDiv("message_error");
          }
      }    
    if(!errReturnFlag){
        return errReturnFlag;
    }
    if(errReturnFlag){
        //28_Oct_2014 changed school visits url from aboutus/schoolvisits to about-us/school-visits by Thiyagu G.
        document.getElementById('schoolvisits').action = "/degrees/about-us/school-visits.html?submitType=sendpartner";
        document.getElementById('schoolvisits').submit();
        return true;
    }
}
function submitOrderDownloadPage(){  
      var aboutusFlag = false;
      var aboutusTxt = "";
      var i;    
      var firstName = $$("firstName").value;
      var lastName = $$("lastName").value;
      var emailAddress = trimString($$("emailAddress").value);
      var phone = trimString($$("phone").value);
      var postCode = trimString($$("postCode").value);
      var errReturnFlag = true;
      if(firstName !="" && firstName == "First name*"){          
          setErrorDiv("firstName_error", "Please enter your first name");
          errReturnFlag = false;
      }else if(firstName == ""){          
          setErrorDiv("firstName_error", "Please enter your first name");
          errReturnFlag = false;
      }else if(!specialTextValidate(trimString(firstName))){          
          setErrorDiv("firstName_error", "Please enter valid text in first name");
          errReturnFlag = false;
      }else if(!isAlphabetic(trimString(firstName))){
            setErrorDiv("firstName_error", "Please enter alphabets only");
            errReturnFlag = false;
      }else{
        clearErrorDiv("firstName_error");        
      }
      if(lastName !="" && lastName == "Last name*"){          
          setErrorDiv("lastName_error", "Please enter your last name");
          errReturnFlag = false;
      }else if(lastName == ""){          
          setErrorDiv("lastName_error", "Please enter your last name");
          errReturnFlag = false;
      }else if(!specialTextValidate(trimString(lastName))){          
          setErrorDiv("lastName_error", "Please enter valid text in last name");
          errReturnFlag = false;
      }else if(!isAlphabetic(trimString(lastName))){
            setErrorDiv("lastName_error", "Please enter alphabets only");
            errReturnFlag = false;
      }else{        
        clearErrorDiv("lastName_error");
      }
      if(emailAddress !="" && emailAddress == "Email address*"){
          setErrorDiv("emailAddress_error", "Please enter your email address");
          errReturnFlag = false;
      }else if(emailAddress == ""){
          setErrorDiv("emailAddress_error", "Please enter your email address");
          errReturnFlag = false;
      }else if(!specialEmailTextValidate(trimString(emailAddress))){
          setErrorDiv("emailAddress_error", "Please enter valid text in email address");
          errReturnFlag = false;
      }else if(!isValidEmail(trimString(emailAddress))){
          setErrorDiv("emailAddress_error", "Please enter valid email address");          
          errReturnFlag = false;
      }else{
        clearErrorDiv("emailAddress_error");
      }
      if(phone !="" && phone == "Phone*"){
          setErrorDiv("phone_error", "Please enter your phone number");
          errReturnFlag = false;
      }else if(phone == ""){          
          setErrorDiv("phone_error", "Please enter your phone number");
          errReturnFlag = false;
      }else if(!specialTextValidate(trimString(phone))){
          setErrorDiv("phone_error", "Please enter valid phone number");
          errReturnFlag = false;
      }else if(!isNumeric(trimString(phone))){
            setErrorDiv("phone_error", "Please enter numeric value");//Modified error message by Indumathi Jan-27-16
            errReturnFlag = false;                        
      }else{        
        clearErrorDiv("phone_error");
      } 
      if(postCode!="Postcode"){        
        if(postCode!="" &&  !isValidUKpostcode(postCode)){
            setErrorDiv("postCode_error", "Please enter valid postcode");
            errReturnFlag = false;
          }else{
            clearErrorDiv("postCode_error");
          }  
      }
      if(!errReturnFlag){
        return errReturnFlag;
      }
    //28_Oct_2014 changed order download url from aboutus/orderdownload to about-us/order-download by Thiyagu G.
    document.getElementById('orderdownload').action = "/degrees/about-us/order-download.html?submitType=sendpartner";
    document.getElementById('orderdownload').submit();
    return true;
}

function setErrorDiv(id, errMsg){
    $$(id).innerHTML = errMsg;
    $$(id).style.display = 'block';
}
function clearErrorDiv(id){
    $$(id).innerHTML = "";
    $$(id).style.display = 'none';
}
function isValidUKpostcode(postcode){
    if(postcode.search(/^[a-zA-Z]((\d)|((\d[a-zA-Z])|([a-zA-Z]\d)|(\d\d))|(([a-zA-Z]\d\d)|([a-zA-Z]{2}\d)|([a-zA-Z]\d[a-zA-Z])))[ ]\d[a-zA-Z]{2}$/) == -1){
        return false;
    }
    return true;
}
function setSelectedVal(obj, spanid){
  var dobid = obj.id
  var el = document.getElementById(dobid);
  var text = el.options[el.selectedIndex].text;
  document.getElementById(spanid).innerHTML = text;
}
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31;
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30;}
		if (i==2) {this[i] = 29;}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12);
	var pos1=dtStr.indexOf(dtCh);
	var pos2=dtStr.indexOf(dtCh,pos1+1);
	var strDay=dtStr.substring(0,pos1);
	var strMonth=dtStr.substring(pos1+1,pos2);
	var strYear=dtStr.substring(pos2+1);
	strYr=strYear;
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1);
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1);
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1);
	}
	month=parseInt(strMonth);
	day=parseInt(strDay);
	year=parseInt(strYr);
	if (pos1==-1 || pos2==-1){
            setErrorDiv("date_error", "Please enter a date in correct format - DD/MM/YY");
            return false;
	}
	if (strMonth.length<1 || month<1 || month>12){
            setErrorDiv("date_error", "Please enter a valid month");
            return false;
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){		
                setErrorDiv("date_error", "Please enter a valid day");
		return false;
	}        
        if(dtStr!="DD / MM / YY*"){        
            if (strYear.length != 2 || year==0){
                setErrorDiv("date_error", "Please enter a valid 2 digit year");
                return false;
            }
        }
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var cuYear = today.getFullYear();
        cuYear = cuYear.toString();         
        var cuYY = cuYear.substr(2,2);        
        if(year < cuYY){
            setErrorDiv("date_error", "Year should greater than current year");
            return false;
        }else if(year <= cuYY && month < mm){
            setErrorDiv("date_error", "Month should greater than current month");
            return false;
        }else if(year <= cuYY && month <= mm && day < dd){
            setErrorDiv("date_error", "Day should greater than current day");
            return false;
        }
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
            setErrorDiv("date_error", "Please enter a valid date");		
            return false
	}
    return true
}
function submitContactUsPage(){
    var firstName = trimString($$("firstName").value);
    var lastName = trimString($$("lastName").value);
    var emailAddress = trimString($$("emailAddress").value);
    var message = trimString($$("message").value);
    var errReturnFlag = true;
    if(firstName == null || trimString(firstName) == "" || firstName == 'First name*'){
        setErrorDiv("firstName_error", "Please enter your first name");
        errReturnFlag = false;      
    }else{
       clearErrorDiv("firstName_error");        
    }
    if(lastName == null || trimString(lastName) == "" || lastName == 'Last name*'){      
       setErrorDiv("lastName_error", "Please enter your last name");
       errReturnFlag = false;
    }else{
       clearErrorDiv("lastName_error");
    }
    if(emailAddress == null || emailAddress.trim() == "" || emailAddress == 'Email address*'){      
       setErrorDiv("emailAddress_error", "Please enter your email address");
       errReturnFlag = false;
    }else if(!isValidEmail(trimString(emailAddress))){
       setErrorDiv("emailAddress_error", "Please enter valid email address");          
       errReturnFlag = false;
    }else{
       clearErrorDiv("emailAddress_error");
    }
    if(message == null || trimString(message) == "" || message == 'Please enter a brief description of your message*'){      
			 setErrorDiv("message_error", "Please enter your message");
       errReturnFlag = false;
    }else{
       var invalidWord = containsSpecialCharacter(document.getElementById("message"));
       if(invalidWord != null && trimString(invalidWord) != ""){ 
		 errReturnFlag = false;
          setErrorDiv("message_error", "There are some illegal characters in the text you supplied. Tut tut!");
			}else{
       clearErrorDiv("message_error");
			 }
    }
    if(!errReturnFlag){
       return errReturnFlag;
    }
    if(errReturnFlag){        
       //28_Oct_2014 changed contact us url from /aboutus/contactus to /about-us/contact-us/ by Thiyagu G.
       document.getElementById('aboutcontactus').action = "/degrees/about-us/contact-us.html?submitType=sendcontactinfo";        
       document.getElementById('aboutcontactus').submit();        
       return true;
    }
}