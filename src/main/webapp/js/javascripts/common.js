/**
 * Author:	Mohamed Syed
 * Date:	wu318_20111020
 * Purpose:	common JS function, and this will be loaded all pages
 */
//
function trimString(inString){
  return inString.replace(/^\s+|\s+$/g,"");
}
//
function $$(id){
  return document.getElementById(id);
}
//
function replaceAll(replacetext, findstring, replacestring){
	var strReplaceAll = replacetext;
	var intIndexOfMatch = strReplaceAll.indexOf(findstring)
	while(intIndexOfMatch != -1){
		strReplaceAll = strReplaceAll.replace(findstring, replacestring);
		intIndexOfMatch = strReplaceAll.indexOf(findstring)
	}
	return strReplaceAll
}
function hideExpandDiv(divids){document.getElementById(divids).style.display=document.getElementById(divids).style.display=='none' ? 'block' : 'none'}
//registration page 
function showBrowsePreview(filename){
var imagepath=filename.value;var lastdot=imagepath.lastIndexOf('.')
var fileExtension=imagepath.substring(lastdot,imagepath.length).toUpperCase()
if(fileExtension==".PJPEG" || fileExtension=='.GIF' || fileExtension=='.X-PNG' || fileExtension=='.JPEG' || fileExtension=='.JPG'){document.images["spacergif"].src=filename.value
}else{images["spacergif"].src="http://images2.content-wu.com/wu-cont/wu-cont/img/whatuni/spacer.gif";}
if($$("fileUpload").value!=''){
$$("defaultshowimageid").style.display='none';$$("spacergif").style.display='block'
if($$("defaultImageId")!=null){$$("defaultImageId").value="";}}}
