//Hero image script given by Raj added by Prabha on 27_JAN_2016_REL
//Modified script for awards page Indumathi.S Mar-08-16
//h_image4 added by Prabha for provider review hero image on 29_Mar_16
var $himg = jQuery.noConflict();
$himg(function(){
	var dwidth = $himg(this).width();//device width
	if(dwidth > 1024){
	  var img_height = 992;//static height
   var img_width=1900;//static width
	var img_height=992;//static height
	  var variation = img_height/img_width
	  //depends upon the device width calculate the height
	  var img_dht = Math.round(dwidth * variation);
	  var img_wd;
	  if(dwidth > 993 && dwidth <= 1600){
	    img_dht = img_dht - 100;
	  }else if(dwidth > 1600 && dwidth < 1949){
	    img_dht=img_dht-100;
	    img_wd=dwidth;
	    $himg("#h_image,#h_image2,#h_image3,#h_image4,#h_image5,#h_image8,#hopd_img").css({
	      'width':img_wd + 'px',
	      'margin-left':'auto',
	      'margin-right':'auto',
	      'padding':'0',
	      'background-size':'inherit!important'
	    }); 
	  }else if(dwidth > 1950){
	    img_dht = 992;
	    img_wd = 1900;
	    $himg("#h_image,#h_image2,#h_image3,#h_image4,#h_image5,#hopd_img").css({
	     'width':img_wd + 'px',
	     'margin-left':'auto',
	     'margin-right':'auto',
	     'padding':'0',
	     'background-size':'inherit!important'
	    }); 
	  }
	  $himg("#h_image,#h_image2,#h_image3,#h_image4,#h_image5,#h_image8,#hopd_img").height(img_dht + "px"); 
    $himg("#h_image3,#h_image4,#h_image5,#h_image8,#hopd_img").height(img_dht + 80 + "px"); //h_image4 added by Prabha for provider review hero image on 29_Mar_16
    $himg(".stubx").height(img_dht + 80 + "px");
	} 
});