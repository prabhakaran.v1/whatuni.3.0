function isValidText(reviewtext, pattern) {
  var index = -1;
  if(reviewtext != null) { index = reviewtext.search(pattern);  }
  return (index != -1) ? true : false;
}
function changeMoreReviewListURL(formtype)  {
   with(document) {
       var current_pagno = $$('current_pageno').value; var current_sort = $$('current_sortorder').value;
       var current_sid   = $$('current_sid').value;    var current_iama = $$('current_iama').value;
       if(formtype == 'subject') {current_sid  = $$('subjectid_value').options[$$('subjectid_value').selectedIndex].value; current_pagno ="1";}    
       if(formtype == 'iama') { current_iama = $$('usertype_value').options[$$('usertype_value').selectedIndex].value; current_pagno ="1";} 
       var formaction   =  $$('more_review_action').value;
       var urlArray=formaction.split("/");
       var finalUrl = '/degrees';
        for (var i=0;i<urlArray.length;i++){
          if(($$("reviewSearchKeyword") !=null && ($$("reviewSearchKeyword").value =='0')) && $$('subjectid_value').options[$$('subjectid_value').selectedIndex].value !=''){
             if (i == 1){ finalUrl = finalUrl + '/'+replaceHypen(replaceURL($$('subjectid_value').options[$$('subjectid_value').selectedIndex].text.toLowerCase()))+'-course-reviews'; }
          }else{
             if (i == 1){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          }   
          if (i == 3){ finalUrl = finalUrl+'/'+ (trimString(current_sort) == ''? 'highest-rated' : current_sort); }
          if (i == 4){ finalUrl = finalUrl+'/'+(trimString(current_iama) == '' || trimString(current_iama) == 'Please select'? '0' : current_iama); }
          if (i == 5){  finalUrl = finalUrl+'/'+ (trimString(current_sid) == ''? '0' : current_sid); }
          if (i == 6){ finalUrl = finalUrl+'/'+ (trimString(current_pagno) == ''? '1' : current_pagno); }
        }
        if($$("reviewSearchKeyword") !=null && ($$("reviewSearchKeyword").value !='0')){
           finalUrl = finalUrl.toLowerCase()+"/subject-reviews.html";
        }else{    
          if(trimString(current_sid) ==''){
              finalUrl = finalUrl.toLowerCase()+"/reviews.html";
          }else{
             finalUrl = finalUrl.toLowerCase()+"/coursereviews.html";
          }
        }    
        if(formtype == 'subject'){
             $$('subfilterform').action = finalUrl.toLowerCase(); $$('subfilterform').submit(); return false;
         }else{
            $$('iamafilterform').action = finalUrl.toLowerCase(); $$('iamafilterform').submit(); return false;
        }
    }
 }
 function changeMoreReviewList(formtype)  {
 with(document) {
       var current_pagno = $$('current_pageno').value; var current_sort = $$('current_sortorder').value;
       var current_sid   = $$('current_sid').value;    var current_iama = $$('current_iama').value;
       if(formtype == 'subject') {current_sid  = $$('subjectid_value').options[$$('subjectid_value').selectedIndex].value; current_pagno ="1";}    
       if(formtype == 'iama') { current_iama = $$('usertype_value').options[$$('usertype_value').selectedIndex].value; current_pagno ="1";} 
       var formaction   =  $$('more_review_action').value;
       // /reviews/uni/bangor-university-reviews/lowest-rated/0/0/3769/1/studentreviews.html
       var urlArray=formaction.split("/");
       var finalUrl = '/degrees';
        for (var i=0;i<urlArray.length;i++){
          if (i == 0){ finalUrl = finalUrl + urlArray[i]; }
          if (i == 1){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 2){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 3){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 4){ finalUrl = finalUrl+'/'+ (trimString(current_sort) == ''? 'highest-rated' :   replaceAll(current_sort, "_","-")  ); }
          if (i == 5){ finalUrl = finalUrl+'/'+(trimString(current_iama) == '' || trimString(current_iama) == 'Please select'? '0' : current_iama); }
          if (i == 6){  finalUrl = finalUrl+'/'+ (trimString(current_sid) == ''? '0' : current_sid); }
          if (i == 7){ finalUrl = finalUrl + '/'+ urlArray[i]; }
          if (i == 8){ finalUrl = finalUrl+'/'+ (trimString(current_pagno) == ''? '1' : current_pagno); }
        }
        finalUrl = finalUrl.toLowerCase()+"/studentreviews.html";
        if(formtype == 'subject'){
             $$('subfilterform').action = finalUrl.toLowerCase(); $$('subfilterform').submit(); return false;
         }else{
            $$('iamafilterform').action = finalUrl.toLowerCase(); $$('iamafilterform').submit(); return false;
        }
    }
 }
 
 //----------------added from add review page
 //wu416_20120918 added by Sekhar K for showing session warning message
 var sessionTrackerCount = 0;
    var sessionTimeoutInterval = "40"; //In minutes. TODO Need to change to 40 while deploying live server.
    function sessionTracker()
    {
      //alert("-----------sessionTrackerCount-------------  "+sessionTrackerCount);
      if(sessionTrackerCount+5 == sessionTimeoutInterval)
      {
        var left = (screen.width/2) - (450/2);
        var top = (screen.height/2) - (275/2);
        sessionExpiryWin = window.open('/degrees/help/sessionExpiryMessage.htm',"sessionExpiryWindow", 'toolbar=no, location=no, directories=no, status=no, menubar=no, resizable=no, scrollbars=no, copyhistory=no, height=250, width=450, top='+top+'left='+left);
      }
      sessionTrackerCount++;
      setTimeout('sessionTracker()', 30000);
    }
    setTimeout('sessionTracker()', 30000);
    function callBack(){
     sessionTrackerCount = 0;
     setTimeout('sessionTracker()', 30000);
    }
 
 var msg3 = "Enter uni name here";
      var msg61 = "Enter course name here";
      function clearDefaultText(curid){
        var ids = curid.id;
        $$(ids).value = trimString($$(ids).value)
        if($$(ids).value == msg3){$$(ids).value = "";}        
      }
      function clearDefaultCourseText(curid){
        var ids = curid.id;
        $$(ids).value = trimString($$(ids).value)
        if($$(ids).value == msg61){$$(ids).value = "";}       
      }
      function setDefaultText(curid){
        var ids = curid.id
        if($$(ids).value == ''){$$(ids).value = msg3;}       
      }
      function setDefaultCourseText(curid){
        var ids = curid.id
        if($$(ids).value == ''){$$(ids).value = msg61;}        
      }      
      function collegeNameClear(e){
        if(e.keyCode != 13 && e.keyCode != 9) {
          document.getElementById("collegeName_hidden").value="";
          document.getElementById("coursetitle_hidden").value="";
          if(document.getElementById("coursetitle").value != msg61){
           document.getElementById("coursetitle").value="";
          }
        }
      }
      function courseIdClear(e){
         if(e.keyCode != 13 && e.keyCode != 9) {
              document.getElementById("coursetitle_hidden").value="";
          }	
      }
      function finishReview(){
         with(document){
            for(i=1; i<=10; i++){
                if(getElementById("review_ques_div"+i)){getElementById("review_ques_div"+i).style.display="none";}
             }    
             getElementById("reviewlastcontent").style.display="block";
             getElementById("reviewfirstcontent").style.display="none";
             getElementById("progressbar").style.display="none";
             getElementById("prgtitle").style.display="none";
         }    
      }
      function checkMandatory(){
        with(document){
            var error = false;  var message ="";
              if(getElementById("collegeName_hidden").value == ''){
                  message = message  + '\nPlease choose a University name from the list. Type 3 characters of the name and your University should appear.'
              }else{    
                  if(getElementById("coursetitle_hidden").value == ''){
                      message = message  + '\nPlease choose a course name from the list. Type 3 characters of the name and your course should appear.'
                  }    
                 if(getElementById("rev_title").value == ''){
                    message = message  + "\nDon't be shy - give your review a snappy headline!"
                 }   
              }    
            if(getElementById("recommentyesradio").checked == false && getElementById("recommentnoradio").checked == false ){ error = true;  message = message +  "\n\nOops! Please say whether you would recommend your uni."; }
            if(getElementById("termsandcondition").checked == false) { error = true;  message = message +  "\n\n Oops! Please agree to our terms and conditions otherwise you can't review your uni."; }        
            if(message !=''){ message = "Validation error: " + message; alert(message); return false;}    
       }     
     }      
      function showDivToEdit(a, butname){
        with(document){
          //validate the review data 
          if(butname !=null && butname!='P'){
              var questionarray = new Array('Overall Rating','Course and Lecturers','Accommodation','City Life','Uni Facilities','Clubs and Societies','Student Union','Eye Candy','Job Prospects','Getting a place');
              var test_id = "radio_"+(parseInt(a)-1);
              var checked = false;
              for(i=0; i<10; i++){
                  var ratio_id = test_id + "_"+i;
                  if(getElementById(ratio_id) !=null){ if(getElementById(ratio_id).checked == true){checked = true;}}
              }     
              var message ="";
              if(getElementById("collegeName_hidden").value == ''){
                  message = message  + '\n\nPlease choose a University name from the list. Type 3 characters of the name and your University should appear.'
              }else{    
                  if(getElementById("coursetitle_hidden").value == ''){
                      message = message  + '\n\nPlease choose a course name from the list. Type 3 characters of the name and your course should appear.'
                  }    
                 if(getElementById("rev_title").value == ''){
                    message = message  + "\n\nDon't be shy - give your review a snappy headline!"
                 }   
              }    
              if(message==''){
                  if(checked == false){ message = message + "\nOops! please select the rating for "+ questionarray[parseInt(a)-2];} 
                  if(a == '2') {
                     var word = /\w/g;    var nonword = /\W/g;    var wordflag = false;    var nonwordflag = false;
                     reviewtext = document.getElementById("Btarea").value;   wordflag = isValidText(reviewtext, nonword);   nonwordflag = isValidText(reviewtext, word);
                     if((wordflag || nonwordflag) && trimString(reviewtext) !='') { var commment = true;
                     }else{ message = message +  "\n\nTo complete your review we need you to write about your overall experience as a minimum - that amount of time must leave you with something to say!";
                     }    
                  }   
              }     
              if(message !=''){ message = "Validation error:" + message; alert(message); return;}     
          }     
         // end of validation          
          for(i=1; i<=10; i++){
            getElementById("review_pb"+i).className ="pb"+i;
            if(i==a){
              if(getElementById("review_ques_div"+i) !=null)
                getElementById("review_ques_div"+i).style.display="block";
            }else{
               if(getElementById("review_ques_div"+i))
                  getElementById("review_ques_div"+i).style.display="none";
            }
          }
         //to set the progress bar 
         var clname = "pb"+a+" bgfix";
         getElementById("review_pb"+a).className = clname;
       }  
     } 