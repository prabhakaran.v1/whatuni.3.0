<%@ page import="java.sql.*,javax.sql.DataSource,oracle.jdbc.OracleTypes,javax.naming.InitialContext"%>
<%
  response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
  response.setHeader("Pragma","no-cache"); //HTTP 1.0
  response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
  Connection connection = null;
  CallableStatement cstmt = null;
  DataSource datasource = null;
  try{	      
    InitialContext ic = new InitialContext();
    datasource = (DataSource)ic.lookup("whatuni");          
	if(datasource != null){
	  connection = datasource.getConnection();
	  if(connection != null){
		cstmt = connection.prepareCall("{call hot_monitor.wu_http(?)}");
		if(cstmt != null){
		  //
		  cstmt.registerOutParameter(1, OracleTypes.VARCHAR);
		  cstmt.execute();
		  //
		  String result = (String)cstmt.getObject(1);
		  if(result != null && result.trim().length() > 0) {
			out.print("<h3> " + result + "</h3>");
		  }else {
			out.print("<h3> DB returns NULL</h3>");
		    response.setStatus(500); 
		  }
		}
	  }
	}
  }catch(Exception e){
    response.setStatus(500);
    out.print("<h3> Got below Exception: </h3>");
	out.print("<p> " + e + "</p>");  
  }finally{
	if(connection != null){
	  connection.close();
	}
	datasource = null;
	cstmt = null;
  }
%>