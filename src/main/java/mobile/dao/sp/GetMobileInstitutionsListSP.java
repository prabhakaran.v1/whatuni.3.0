package mobile.dao.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import mobile.form.MobileUniBrowseBean;
import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.review.bean.UniBrowseBean;

import com.layer.util.SpringConstants;

public class GetMobileInstitutionsListSP extends StoredProcedure {
  public GetMobileInstitutionsListSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(SpringConstants.GET_MBL_COLLEGE_FINDER_FN);
    declareParameter(new SqlOutParameter("pc_getdata", OracleTypes.CURSOR, new BrowseUniversityRowMapperImpl()));
    declareParameter(new SqlParameter("p_alpha_char", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_sort_by", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_network_id", OracleTypes.NUMBER));    
    compile();
  }
  public Map execute(MobileUniBrowseBean uniBrowseBean) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_alpha_char", uniBrowseBean.getSearchText());
      inputMap.put("p_page_no", uniBrowseBean.getPageNo());
      inputMap.put("p_sort_by", uniBrowseBean.getOrderBy());
      inputMap.put("p_network_id", uniBrowseBean.getNetworkId());
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
  private class BrowseUniversityRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      UniBrowseBean uniBrowseBean = new UniBrowseBean();
      uniBrowseBean.setCollegeId(resultSet.getString("college_id"));
      uniBrowseBean.setCollegeName(resultSet.getString("college_name"));
      uniBrowseBean.setCollegeNameUrl(resultSet.getString("url"));
      uniBrowseBean.setCollegeNameDisplay(resultSet.getString("college_name_display"));
      uniBrowseBean.setOverallRating(resultSet.getString("overall_rating"));
      uniBrowseBean.setTotalRecordCount(resultSet.getString("total_no_of_ins"));
      return uniBrowseBean;
    }
  }
}
