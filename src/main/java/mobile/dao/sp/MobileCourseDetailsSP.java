package mobile.dao.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import mobile.valueobject.MobileUnistatsInfoVO;
import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.search.TimesRankingVO;
import WUI.utilities.CommonFunction;

import com.layer.util.rowmapper.advert.EnquiryInfoRowMapperImpl;
import com.layer.util.rowmapper.advert.SpListRowMapperImpl;
import com.layer.util.rowmapper.kis.SubjectDataRowMpperImpl;
import com.layer.util.rowmapper.review.UniOverallReviewRowMapperImpl;
import com.layer.util.rowmapper.search.CourseDetailsRowMapperImpl;
import com.layer.util.rowmapper.search.EntryPointsForUCASCourseRowMapperImpl;
import com.layer.util.rowmapper.search.LocationsInfoRowMapperImpl;
import com.layer.util.rowmapper.search.ModuleListRowMapperImpl;
import com.layer.util.rowmapper.search.TopCoursesRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.search.CourseKISDataVO;

public class MobileCourseDetailsSP extends StoredProcedure {

  public MobileCourseDetailsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WU_MOBILE_COURSE_DETAILS_PKG.MCOURSE_DETAILS_INFO_PRC");
    //
    declareParameter(new SqlParameter("p_course_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_opportunity_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pagename", Types.VARCHAR));
    //
    declareParameter(new SqlOutParameter("o_course_info", OracleTypes.CURSOR, new CourseDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_entry_req", OracleTypes.CURSOR, new EntryPointsForUCASCourseRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_assessed_uni_finance", OracleTypes.CURSOR, new HowYouAreAssesedRowMapper()));
    declareParameter(new SqlOutParameter("o_subject_data", OracleTypes.CURSOR, new SubjectDataRowMpperImpl()));
    declareParameter(new SqlOutParameter("o_enquiry_info", OracleTypes.CURSOR, new EnquiryInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_uni_info", OracleTypes.CURSOR, new UniStatsInfoRowMapper()));
    declareParameter(new SqlOutParameter("o_dept_profile", OracleTypes.CURSOR, new SpListRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_overall_review", OracleTypes.CURSOR, new UniOverallReviewRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_location_info", OracleTypes.CURSOR, new LocationsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_top_courses", OracleTypes.CURSOR, new TopCoursesRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_ip_profile_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("oc_subject_times_ranking", OracleTypes.CURSOR, new SubjectTimesRowMapperImpl())); //11_Feb_2014_REL
    declareParameter(new SqlOutParameter("oc_module_details", OracleTypes.CURSOR, new ModuleListRowMapperImpl()));      
    declareParameter(new SqlOutParameter("o_reg_cl_opportunity_id", OracleTypes.VARCHAR));//15_JUL_2014
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", Types.VARCHAR));
  }

  public Map execute(Map inputMap) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_course_id", inputMap.get("courseId"));
    inMap.put("p_opportunity_id", inputMap.get("opportunityId"));
    inMap.put("p_college_id", inputMap.get("collegeId"));
    inMap.put("p_user_id", inputMap.get("userId"));
    inMap.put("p_session_id", inputMap.get("sessionId"));
    inMap.put("p_pagename", inputMap.get("pageName"));
    outMap = super.execute(inMap);
    return outMap;
  }
  
  private class HowYouAreAssesedRowMapper implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CourseKISDataVO kisDataVO = new CourseKISDataVO();
      kisDataVO.setAccomodationLowerPrice(rs.getString("institution_accom_lower"));
      kisDataVO.setAccomodationHigherPrice(rs.getString("institution_accom_upper"));
      kisDataVO.setUkFees(rs.getString("uk_fees"));
      kisDataVO.setIntlFees(rs.getString("intl_fees"));
      kisDataVO.setUkFeesDesc(rs.getString("uk_fee_desc"));
      kisDataVO.setIntFeesDesc(rs.getString("intl_fee_desc"));
      kisDataVO.setCourseWork(rs.getString("avg_pct_coursework"));
      kisDataVO.setPracticalWork(rs.getString("avg_pct_practical_work"));
      kisDataVO.setExam(rs.getString("exam"));      
      return kisDataVO;    
    }
  }
  
  private class UniStatsInfoRowMapper implements RowMapper{
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException{
      MobileUnistatsInfoVO uniStatsInfoVO = new MobileUnistatsInfoVO();
      //
      SeoUrls seoUrl = new SeoUrls();
      uniStatsInfoVO.setCollegeId(resultset.getString("college_id"));
      uniStatsInfoVO.setCollegeName(resultset.getString("college_name"));
      uniStatsInfoVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      uniStatsInfoVO.setUniHomeURL(seoUrl.construnctUniHomeURL(uniStatsInfoVO.getCollegeId(), uniStatsInfoVO.getCollegeName()));
      uniStatsInfoVO.setFullTime(resultset.getString("full_time"));
      uniStatsInfoVO.setPartTime(resultset.getString("part_time"));
      uniStatsInfoVO.setHomeStudents(resultset.getString("home_students"));
      uniStatsInfoVO.setInternationalStudents(resultset.getString("international_students"));
      uniStatsInfoVO.setMatureStudents(resultset.getString("mature_students"));
      uniStatsInfoVO.setNonMatureStudents(resultset.getString("non_mature_students"));
      uniStatsInfoVO.setUnderGraduate(resultset.getString("under_graduate"));
      uniStatsInfoVO.setPostGraduate(resultset.getString("post_graduate"));
      uniStatsInfoVO.setNoOfStudents(resultset.getString("no_of_students"));
      uniStatsInfoVO.setWorkOrStudy(resultset.getString("work_or_study"));
      uniStatsInfoVO.setTimesRank(resultset.getString("times_ranking"));
      return uniStatsInfoVO;
    }
  }

  //Added for 21-1-2014_RELEASE TIMES rANKING
  private class SubjectTimesRowMapperImpl implements RowMapper{
     public Object mapRow(ResultSet rs, int rowNmum) throws SQLException {
       TimesRankingVO timesVO = new TimesRankingVO();
       timesVO.setSubjectName(rs.getString("tr_subject_name"));
       timesVO.setCurrentRanking(rs.getString("current_ranking"));  
       if(!GenericValidator.isBlankOrNull(timesVO.getCurrentRanking())){
        timesVO.setRankOrdinal(new CommonFunction().getOrdinalFor(Integer.parseInt(timesVO.getCurrentRanking())));
       }
       timesVO.setTotalRanking(rs.getString("total_ranking"));
      return timesVO;
     }   
   }
}
