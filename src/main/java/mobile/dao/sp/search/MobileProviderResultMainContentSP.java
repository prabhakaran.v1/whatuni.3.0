package mobile.dao.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import mobile.util.rowmapper.search.ProviderResultsRowMapper;
import mobile.valueobject.SearchVO;
import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.common.UserInfoRowMapperImpl;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;

import com.layer.util.rowmapper.common.CollegeNamesRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.CollegeCpeInteractionDetailsWithMediaVO;
import com.wuni.util.valueobject.advert.SpListVO;
import com.wuni.util.valueobject.search.TopCoursesVO;

public class MobileProviderResultMainContentSP extends StoredProcedure {
  public MobileProviderResultMainContentSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("HOT_WUNI.WU_MOBILE_SEARCH_PKG.GET_PROVIDER_RESULT_DATA_PRC");    
    declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
    declareParameter(new SqlParameter("search_what", Types.VARCHAR));
    declareParameter(new SqlParameter("phrase_search", Types.VARCHAR));
    declareParameter(new SqlParameter("college_name", Types.VARCHAR));    
    declareParameter(new SqlParameter("qualification", Types.VARCHAR));
    declareParameter(new SqlParameter("country", Types.VARCHAR));
    declareParameter(new SqlParameter("town_city", Types.VARCHAR));
    declareParameter(new SqlParameter("postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("search_range", Types.VARCHAR));
    declareParameter(new SqlParameter("postflag", Types.VARCHAR));
    declareParameter(new SqlParameter("location_id", Types.VARCHAR));
    declareParameter(new SqlParameter("study_mode", Types.VARCHAR));
    declareParameter(new SqlParameter("course_duration", Types.VARCHAR));
    declareParameter(new SqlParameter("submit", Types.VARCHAR));
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("aui", Types.VARCHAR));
    declareParameter(new SqlParameter("search_how", Types.VARCHAR));
    declareParameter(new SqlParameter("z", Types.VARCHAR));
    declareParameter(new SqlParameter("s_type", Types.VARCHAR));
    declareParameter(new SqlParameter("search_category", Types.VARCHAR));
    declareParameter(new SqlParameter("search_career", Types.VARCHAR));
    declareParameter(new SqlParameter("career_id", Types.VARCHAR));
    declareParameter(new SqlParameter("nrp", Types.VARCHAR));
    declareParameter(new SqlParameter("area", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pg_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_col", Types.VARCHAR));
    declareParameter(new SqlParameter("p_county_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pheader_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entity_text", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_where", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_browse_link", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_basket_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));    
    declareParameter(new SqlParameter("p_page_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_level", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_points", Types.VARCHAR));
    declareParameter(new SqlParameter("p_meta_page_name", Types.VARCHAR));  
    declareParameter(new SqlParameter("p_meta_page_flag", Types.VARCHAR)); 
    declareParameter(new SqlOutParameter("o_header_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_adv_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_record_count", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_overall_rating", Types.VARCHAR));
    declareParameter(new SqlOutParameter("oc_search_results", OracleTypes.CURSOR, new ProviderResultsRowMapper()));
    declareParameter(new SqlOutParameter("oc_uni_sp_list", OracleTypes.CURSOR, new SpListRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_user_info", OracleTypes.CURSOR, new UserInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_college_names", OracleTypes.CURSOR, new CollegeNamesRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_enquiry_details", OracleTypes.CURSOR, new EnquiryDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_related_courses", OracleTypes.CURSOR, new RelatedCoursesRowMapperImpl()));
    declareParameter(new SqlOutParameter("p_reg_clear_result_exist_flag", Types.VARCHAR));//15_JUL_2014
  }
  
  public Map execute(SearchVO searchVO) {   
  List inputList = null;
    Map outMap = null;
    Map inMap = new HashMap(); 
    try{           
      inMap.put("p_session_id", searchVO.getX());
      inMap.put("p_user_id", searchVO.getY());
      inMap.put("search_what", searchVO.getSearchWhat());
      inMap.put("phrase_search", searchVO.getPhraseSearch());
      inMap.put("college_name", searchVO.getCollegeName());    
      inMap.put("qualification", searchVO.getQualification());
      inMap.put("country", searchVO.getCountry());
      inMap.put("town_city",searchVO.getTownCity());
      inMap.put("postcode", searchVO.getPostcode());
      inMap.put("search_range", searchVO.getSearchRange());
      inMap.put("postflag", searchVO.getPostflag());
      inMap.put("location_id", searchVO.getLocationId());
      inMap.put("study_mode", searchVO.getStudyMode());
      inMap.put("course_duration", searchVO.getCourseDuration());
      inMap.put("submit", searchVO.getSubmit());
      inMap.put("a", searchVO.getAffiliateId());
      inMap.put("aui", searchVO.getAui());
      inMap.put("search_how", searchVO.getSearchHOW());
      inMap.put("z", searchVO.getCollegeId());
      inMap.put("s_type", searchVO.getSType());
      inMap.put("search_category", searchVO.getSearchCategory());
      inMap.put("search_career", searchVO.getSearchCareer());
      inMap.put("career_id", searchVO.getCareerId());
      inMap.put("nrp", searchVO.getNrp());
      inMap.put("area", searchVO.getArea());
      inMap.put("p_pg_type", searchVO.getPgType());
      inMap.put("p_search_col", searchVO.getSearchCol());
      inMap.put("p_county_id", searchVO.getCountyId());
      inMap.put("p_pheader_id", searchVO.getHeaderId());
      inMap.put("p_entity_text", searchVO.getEntityText());
      inMap.put("p_search_where", searchVO.getSearchThru());
      inMap.put("p_user_agent", searchVO.getUserAgent());
      inMap.put("p_search_browse_link", searchVO.getLocationBrowseUrl());
      inMap.put("p_ucas_code", searchVO.getUcasCode());
      inMap.put("p_basket_id", searchVO.getBasketId());
      inMap.put("p_page_no", searchVO.getPageNo());  
      inMap.put("p_page_name", searchVO.getPageName());    
      inMap.put("p_entry_level", searchVO.getEntryLevel());
      inMap.put("p_entry_points", searchVO.getEntryPoints());
      inMap.put("p_meta_page_name", "COURSE PROVIDER MOBILE");    
      inMap.put("p_meta_page_flag", "COURSE_RESULTS");    
      outMap = execute(inMap);
    }catch(Exception e){
      e.printStackTrace();    
    }
    if(TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG){
      new AdminUtilities().logDbCallParameterDetails("WU_MOBILE_SEARCH_PKG.GET_PROVIDER_RESULT_DATA_PRC", inMap);  
    }
    return outMap;
  }
  
  public class EnquiryDetailsRowMapperImpl implements RowMapper {
  
    public Object mapRow(ResultSet enquiryDetailsRS, int rowNum) throws SQLException {
      CollegeCpeInteractionDetailsWithMediaVO enquiryInfo = new CollegeCpeInteractionDetailsWithMediaVO();
      String tmpUrl = null;
      //
      enquiryInfo.setOrderItemId(enquiryDetailsRS.getString("ORDER_ITEM_ID"));
      enquiryInfo.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
      enquiryInfo.setMyhcProfileId(enquiryDetailsRS.getString("MYHC_PROFILE_ID"));
      enquiryInfo.setProfileType(enquiryDetailsRS.getString("PROFILE_TYPE"));
      enquiryInfo.setAdvertName(enquiryDetailsRS.getString("ADVERT_NAME"));
      enquiryInfo.setCpeQualificationNetworkId(enquiryDetailsRS.getString("NETWORK_ID"));
      //
      enquiryInfo.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
      enquiryInfo.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
      enquiryInfo.setHotLineNo(enquiryDetailsRS.getString("hotline"));
      //
      tmpUrl = enquiryDetailsRS.getString("WEBSITE");
      if (tmpUrl != null && tmpUrl.trim().length() > 0) {
          tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
      } else {
        tmpUrl = "";
      }
      enquiryInfo.setSubOrderWebsite(tmpUrl);
      //
      tmpUrl = enquiryDetailsRS.getString("EMAIL_WEBFORM");
      if (tmpUrl != null && tmpUrl.trim().length() > 0) {
          tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
      } else {
        tmpUrl = "";
      }
      enquiryInfo.setSubOrderEmailWebform(tmpUrl);
      //
      tmpUrl = enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
      if (tmpUrl != null && tmpUrl.trim().length() > 0) {
          tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
      } else {
        tmpUrl = "";
      }
      enquiryInfo.setSubOrderProspectusWebform(tmpUrl);
      enquiryInfo.setIpExistsFlag(enquiryDetailsRS.getString("IP_PROFILE_FLAG")); 
      return enquiryInfo; 
    }
  }

  public class RelatedCoursesRowMapperImpl implements RowMapper {
  
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      TopCoursesVO topCoursesVO = new TopCoursesVO();
      SeoUrls seoUrl = new SeoUrls();
      topCoursesVO.setCollegeId(resultset.getString("college_id"));
      topCoursesVO.setCollegeName(resultset.getString("college_name"));
      topCoursesVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      topCoursesVO.setCourseId(resultset.getString("course_id"));
      topCoursesVO.setCourseTitle(resultset.getString("course_title"));
      topCoursesVO.setStudyLevelDesc(resultset.getString("study_level_desc"));
      topCoursesVO.setCourseDetailsPageURL(seoUrl.construnctCourseDetailsPageURL(topCoursesVO.getStudyLevelDesc(), topCoursesVO.getCourseTitle(), topCoursesVO.getCourseId(), topCoursesVO.getCollegeId(), resultset.getString("p_page_name")));
      topCoursesVO.setUniHomeURL(seoUrl.construnctUniHomeURL(topCoursesVO.getCollegeId(), topCoursesVO.getCollegeName()).toLowerCase());
      topCoursesVO.setWebsitePrice(resultset.getString("website_price"));
      topCoursesVO.setWebformPrice(resultset.getString("webform_price"));
      topCoursesVO.setGaCollegeName(new CommonFunction().replaceSpecialCharacter(topCoursesVO.getCollegeName()));
      ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("enquiry_details");
      try{
        if(enquiryDetailsRS != null){
          while(enquiryDetailsRS.next()){
            String tmpUrl = null;
            topCoursesVO.setOrderItemId(enquiryDetailsRS.getString("order_item_id"));
            topCoursesVO.setMyhcProfileId(enquiryDetailsRS.getString("myhc_profile_id"));
            topCoursesVO.setSubOrderItemId(enquiryDetailsRS.getString("suborder_item_id"));
            topCoursesVO.setProfileType(enquiryDetailsRS.getString("profile_type"));
            topCoursesVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("network_id"));
            topCoursesVO.setAdvertName(enquiryDetailsRS.getString("advert_name"));
            topCoursesVO.setSubOrderEmail(enquiryDetailsRS.getString("email"));
            topCoursesVO.setSubOrderProspectus(enquiryDetailsRS.getString("prospectus"));
            tmpUrl = enquiryDetailsRS.getString("website");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            topCoursesVO.setSubOrderWebsite(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("email_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            topCoursesVO.setSubOrderEmailWebform(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("prospectus_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            topCoursesVO.setSubOrderProspectusWebform(tmpUrl);
            topCoursesVO.setMediaPath(enquiryDetailsRS.getString("media_path"));
            topCoursesVO.setMediaThumbPath(enquiryDetailsRS.getString("thumb_media_path"));
            topCoursesVO.setMediaType(enquiryDetailsRS.getString("media_type"));
            topCoursesVO.setMediaId(enquiryDetailsRS.getString("media_id"));
            if("VIDEO".equalsIgnoreCase(topCoursesVO.getMediaType())){
              topCoursesVO.setVideoThumbPath(new GlobalFunction().videoThumbnailFormatChange(topCoursesVO.getMediaPath(), "2"));
            }
            topCoursesVO.setHotLineNo(enquiryDetailsRS.getString("hotline"));
          }
        }
      }catch(Exception e){
        e.printStackTrace();
      }finally{
        try{
          enquiryDetailsRS.close();
        }catch(Exception e){
          throw new SQLException(e.getMessage());
        }
      }
      return topCoursesVO;
    }
  }
  
  public class SpListRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      SpListVO spListVO = new SpListVO();
      SeoUrls seoUrl = new SeoUrls();
      //
      spListVO.setCollegeId(resultset.getString("college_id"));
      spListVO.setCollegeName(resultset.getString("college_name"));
      spListVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      spListVO.setAdvertName(resultset.getString("advert_name"));
      spListVO.setMyhcProfileId(resultset.getString("myhc_profile_id"));
      spListVO.setCpeQualificationNetworkId(resultset.getString("network_id"));
      spListVO.setProfileId("0"); //"resultset.getString("profile_id"));
      spListVO.setSectionDesc("overview"); //resultset.getString("section_desc"));
      //
      spListVO.setSpListHomeURL(seoUrl.construnctSpListHomeURL(spListVO.getCollegeId()));
      spListVO.setSpURL(seoUrl.construnctSpURL(spListVO.getCollegeId(), spListVO.getCollegeName(), spListVO.getMyhcProfileId(), spListVO.getProfileId(), spListVO.getCpeQualificationNetworkId(), spListVO.getSectionDesc()));
      //
      return spListVO;
    }
  }
}
