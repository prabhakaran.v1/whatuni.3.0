package mobile.dao.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import mobile.valueobject.SearchVO;
import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.search.DBExecutionTimeVO;
import spring.valueobject.search.RefineByOptionsVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;

/**
 * PAGE_NAME:   Course specific Money page
 * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-courses/[KEYWORD]-[STUDY_LEVEL_DESC]-courses-[LOCATION1]/[KEYWORD]/[STUDY_LEVEL_ID]/[LOCATION2]/[LOCATION2]/[SEARCH_RANGE]/[COUNTY_ID]/[STUDY_MODE_ID]/[CATEGORY_CODE]/[ORDER_BY]/[COLLEGE_ID]/[PAGE_NO]/[EXTRA_TEXT]/[UC/U/C]/[ENTRY-LEVEL]/[ENTRY-POINTS]/page.html
 * URL_EXAMPLE: /degrees/courses/degree-courses/business-degree-courses-united-kingdom/business/m/united+kingdom/united+kingdom/25/0/a1/0/r/0/1/0/uc/a-level/3a*-1a-0b-0c-0d-0e/page.html
 *
 * @author  Mohamed Syed
 * @since   wu318_20111020 - redesign
 *
 */
public class MobileRefineResultsSP extends StoredProcedure {

  public MobileRefineResultsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WU_MOBILE_SEARCH_PKG.GET_REFINE_OPTIONS_PRC");
    //
    declareParameter(new SqlParameter("search_what", Types.VARCHAR));
    declareParameter(new SqlParameter("phrase_search", Types.VARCHAR));
    declareParameter(new SqlParameter("college_name", Types.VARCHAR));
    declareParameter(new SqlParameter("qualification", Types.VARCHAR));
    declareParameter(new SqlParameter("country", Types.VARCHAR));
    declareParameter(new SqlParameter("town_city", Types.VARCHAR));
    declareParameter(new SqlParameter("postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("search_range", Types.VARCHAR));
    declareParameter(new SqlParameter("location_id", Types.VARCHAR));
    declareParameter(new SqlParameter("study_mode", Types.VARCHAR));
    declareParameter(new SqlParameter("course_duration", Types.VARCHAR));
    declareParameter(new SqlParameter("crs_search", Types.VARCHAR));
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("search_how", Types.VARCHAR));
    declareParameter(new SqlParameter("search_category", Types.VARCHAR));
    declareParameter(new SqlParameter("area", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pg_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_col", Types.VARCHAR));
    declareParameter(new SqlParameter("p_county_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entity_text", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_where", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_browse_link", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_deemed_uni", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_action", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_cat_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_level", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_points", Types.VARCHAR));
    //
    declareParameter(new SqlOutParameter("oc_refine_by_options", OracleTypes.CURSOR, new MobileRefineByOptionsRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_all_subject_display_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_all_location_display_flag", Types.VARCHAR));
    compile();
  }

  public Map execute(SearchVO searchVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("search_what", searchVO.getSearchWhat());
      inMap.put("phrase_search", searchVO.getPhraseSearch());
      inMap.put("college_name", searchVO.getCollegeName());
      inMap.put("qualification", searchVO.getQualification());
      inMap.put("country", searchVO.getCountry());
      inMap.put("town_city", searchVO.getTownCity());
      inMap.put("postcode", searchVO.getPostcode());
      inMap.put("search_range", searchVO.getSearchRange());
      inMap.put("location_id", searchVO.getLocationId());
      inMap.put("study_mode", searchVO.getStudyMode());
      inMap.put("course_duration", searchVO.getCourseDuration());
      inMap.put("crs_search", searchVO.getCrsSearch());
      inMap.put("a", searchVO.getAffiliateId());
      inMap.put("search_how", searchVO.getSearchHOW());
      inMap.put("search_category", searchVO.getSearchCategory());
      inMap.put("area", searchVO.getArea());
      inMap.put("p_pg_type", searchVO.getPgType());
      inMap.put("p_search_col", searchVO.getSearchCol());
      inMap.put("p_county_id", searchVO.getCountyId());
      inMap.put("p_entity_text", searchVO.getEntityText());
      inMap.put("p_search_where", searchVO.getSearchThru());
      inMap.put("p_user_agent", searchVO.getUserAgent());
      inMap.put("p_search_browse_link", searchVO.getRequestURL());
      inMap.put("p_ucas_code", searchVO.getUcasCode());
      inMap.put("p_deemed_uni", searchVO.getDeemedUniFlag());
      inMap.put("p_search_action", searchVO.getSearchAction());
      inMap.put("p_search_cat_id", searchVO.getSearchCategoryId());
      inMap.put("p_page_name", searchVO.getSearchType());
      inMap.put("p_entry_level", searchVO.getEntryLevel());
      inMap.put("p_entry_points", searchVO.getEntryPoints());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("WU_MOBILE_SEARCH_PKG.GET_REFINE_OPTIONS_PRC", inMap);
    }
    return outMap;
  }
    private class DBExecutionTimeRowImpl implements RowMapper {

      public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
        DBExecutionTimeVO dBExecutionTimeVO = new DBExecutionTimeVO();
        dBExecutionTimeVO.setFucntionName(resultset.getString("name"));
        dBExecutionTimeVO.setTimeTaken(resultset.getString("time_taken"));
        return dBExecutionTimeVO;
      }

    }

  public class MobileRefineByOptionsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      RefineByOptionsVO refineByOptionsVO = new RefineByOptionsVO();
      refineByOptionsVO.setRefineOrder(resultset.getString("REFINE_ORDER"));
      refineByOptionsVO.setRefineCode(resultset.getString("REFINE_CODE"));
      refineByOptionsVO.setRefineDesc(resultset.getString("REFINE_DESC"));
      refineByOptionsVO.setCategoryId(resultset.getString("CATEGORY_ID"));
      refineByOptionsVO.setCategoryCode(resultset.getString("CATEGORY_CODE"));
      refineByOptionsVO.setDisplaySequence(resultset.getString("DISP_SEQ"));
      refineByOptionsVO.setParentCountryName(resultset.getString("PARENT_COUNTRY_NAME"));
      refineByOptionsVO.setNumberOfChildLocations(resultset.getString("CHILD_LOCATION_COUNT"));
      return refineByOptionsVO;
    }
  }

}
