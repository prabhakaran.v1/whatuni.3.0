package mobile.dao.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import mobile.valueobject.SearchVO;
import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.search.DBExecutionTimeVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;

import com.layer.util.rowmapper.search.OmnitureLoggingRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.search.CourseSpecificSearchResultsVO;

/**
 * PAGE_NAME:   Course specific Money page
 * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-courses/[KEYWORD]-[STUDY_LEVEL_DESC]-courses-[LOCATION1]/[KEYWORD]/[STUDY_LEVEL_ID]/[LOCATION2]/[LOCATION2]/[SEARCH_RANGE]/[COUNTY_ID]/[STUDY_MODE_ID]/[CATEGORY_CODE]/[ORDER_BY]/[COLLEGE_ID]/[PAGE_NO]/[EXTRA_TEXT]/[UC/U/C]/[ENTRY-LEVEL]/[ENTRY-POINTS]/page.html
 * URL_EXAMPLE: /degrees/courses/degree-courses/business-degree-courses-united-kingdom/business/m/united+kingdom/united+kingdom/25/0/a1/0/r/0/1/0/uc/a-level/3a*-1a-0b-0c-0d-0e/page.html
 *
 * @author  Mohamed Syed
 * @since   wu318_20111020 - redesign
 *
 */
public class MobileCourseSpecificMoneyPageSP extends StoredProcedure {

  public MobileCourseSpecificMoneyPageSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WU_MOBILE_SEARCH_PKG.GET_COURSE_SEARCH_RESULTS_PRC");
    //
    declareParameter(new SqlParameter("x", Types.VARCHAR));
    declareParameter(new SqlParameter("y", Types.VARCHAR));
    declareParameter(new SqlParameter("search_what", Types.VARCHAR));
    declareParameter(new SqlParameter("phrase_search", Types.VARCHAR));
    declareParameter(new SqlParameter("college_name", Types.VARCHAR));
    declareParameter(new SqlParameter("qualification", Types.VARCHAR));
    declareParameter(new SqlParameter("country", Types.VARCHAR));
    declareParameter(new SqlParameter("town_city", Types.VARCHAR));
    declareParameter(new SqlParameter("postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("search_range", Types.VARCHAR));
    declareParameter(new SqlParameter("postflag", Types.VARCHAR));
    declareParameter(new SqlParameter("location_id", Types.VARCHAR));
    declareParameter(new SqlParameter("study_mode", Types.VARCHAR));
    declareParameter(new SqlParameter("course_duration", Types.VARCHAR));
    declareParameter(new SqlParameter("lucky", Types.VARCHAR));
    declareParameter(new SqlParameter("crs_search", Types.VARCHAR));
    declareParameter(new SqlParameter("submit", Types.VARCHAR));
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("search_how", Types.VARCHAR));
    declareParameter(new SqlParameter("s_type", Types.VARCHAR));
    declareParameter(new SqlParameter("search_category", Types.VARCHAR));
    declareParameter(new SqlParameter("search_career", Types.VARCHAR));
    declareParameter(new SqlParameter("career_id", Types.VARCHAR));
    declareParameter(new SqlParameter("ref_id", Types.VARCHAR));
    declareParameter(new SqlParameter("nrp", Types.VARCHAR));
    declareParameter(new SqlParameter("area", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pg_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_col", Types.VARCHAR));
    declareParameter(new SqlParameter("p_county_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entity_text", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_where", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_browse_link", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_deemed_uni", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_action", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));
    declareParameter(new SqlParameter("p_basket_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_cat_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_level", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_points", Types.VARCHAR));
    //
    declareParameter(new SqlOutParameter("oc_search_results", OracleTypes.CURSOR, new MobileCourseSpecificResultsRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_header_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_record_count", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_search_college_ids", OracleTypes.CURSOR, new OmnitureLoggingRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_college_count", Types.NUMERIC));
    declareParameter(new SqlOutParameter("o_clearing_crs_exist", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_total_opp_count", Types.NUMERIC));
    declareParameter(new SqlOutParameter("o_parent_cat_id", Types.NUMERIC));
    declareParameter(new SqlOutParameter("o_parent_cat_code", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_parent_cat_desc", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_reg_clear_result_exist_flag", Types.VARCHAR));//15-JUL-2014
    compile();
  }

  public Map execute(SearchVO searchVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("x", searchVO.getX());
      inMap.put("y", searchVO.getY());
      inMap.put("search_what", searchVO.getSearchWhat());
      inMap.put("phrase_search", searchVO.getPhraseSearch());
      inMap.put("college_name", searchVO.getCollegeName());
      inMap.put("qualification", searchVO.getQualification());
      inMap.put("country", searchVO.getCountry());
      inMap.put("town_city", searchVO.getTownCity());
      inMap.put("postcode", searchVO.getPostcode());
      inMap.put("search_range", searchVO.getSearchRange());
      inMap.put("postflag", searchVO.getPostflag());
      inMap.put("location_id", searchVO.getLocationId());
      inMap.put("study_mode", searchVO.getStudyMode());
      inMap.put("course_duration", searchVO.getCourseDuration());
      inMap.put("lucky", searchVO.getLucky());
      inMap.put("crs_search", searchVO.getCrsSearch());
      inMap.put("submit", searchVO.getSubmit());
      inMap.put("a", searchVO.getAffiliateId());
      inMap.put("search_how", searchVO.getSearchHOW());
      inMap.put("s_type", searchVO.getSType());
      inMap.put("search_category", searchVO.getSearchCategory());
      inMap.put("search_career", searchVO.getSearchCareer());
      inMap.put("career_id", searchVO.getCareerId());
      inMap.put("ref_id", searchVO.getRefId());
      inMap.put("nrp", searchVO.getNrp());
      inMap.put("area", searchVO.getArea());
      inMap.put("p_pg_type", searchVO.getPgType());
      inMap.put("p_search_col", searchVO.getSearchCol());
      inMap.put("p_county_id", searchVO.getCountyId());
      inMap.put("p_entity_text", searchVO.getEntityText());
      inMap.put("p_search_where", searchVO.getSearchThru());
      inMap.put("p_user_agent", searchVO.getUserAgent());
      inMap.put("p_search_browse_link", searchVO.getRequestURL());
      inMap.put("p_ucas_code", searchVO.getUcasCode());
      inMap.put("p_deemed_uni", searchVO.getDeemedUniFlag());
      inMap.put("p_search_action", searchVO.getSearchAction());
      inMap.put("p_page_no", searchVO.getPageNo());
      inMap.put("p_basket_id", searchVO.getBasketId());
      inMap.put("p_search_cat_id", searchVO.getSearchCategoryId());
      inMap.put("p_page_name", searchVO.getSearchType());
      inMap.put("p_entry_level", searchVO.getEntryLevel());
      inMap.put("p_entry_points", searchVO.getEntryPoints());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("WHATUNI_SEARCH3.GET_MONEY_PAGE_MAIN_CONTENT", inMap);
    }
    return outMap;
  }
    private class DBExecutionTimeRowImpl implements RowMapper {

      public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
        DBExecutionTimeVO dBExecutionTimeVO = new DBExecutionTimeVO();
        dBExecutionTimeVO.setFucntionName(resultset.getString("name"));
        dBExecutionTimeVO.setTimeTaken(resultset.getString("time_taken"));
        return dBExecutionTimeVO;
      }

    }
    private class MobileCourseSpecificResultsRowMapperImpl implements RowMapper {
      public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
        CourseSpecificSearchResultsVO courseSpecificSearchResultsVO = new CourseSpecificSearchResultsVO();
        SeoUrls seoUrl = new SeoUrls();
        courseSpecificSearchResultsVO.setCollegeId(resultset.getString("COLLEGE_ID"));
        courseSpecificSearchResultsVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
        courseSpecificSearchResultsVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
        courseSpecificSearchResultsVO.setUniHomeUrl(seoUrl.construnctUniHomeURL(courseSpecificSearchResultsVO.getCollegeId(), courseSpecificSearchResultsVO.getCollegeName()));
        courseSpecificSearchResultsVO.setNumberOfCoursesForThisCollege(resultset.getString("HITS"));
        courseSpecificSearchResultsVO.setOverallRating(resultset.getString("overall_rating"));
        return courseSpecificSearchResultsVO;
      }
    }

}
