package mobile.controller;

import WUI.registration.bean.RegistrationBean;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CookieManager;
import WUI.utilities.SessionData;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MobileLogoutController {
  
  @RequestMapping(value = "/usrlogout,/userlogout", method = {RequestMethod.GET,RequestMethod.POST})
  public ModelAndView execute(@ModelAttribute("registrationbean")RegistrationBean  registrationbean, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    CommonFunction common = new CommonFunction();
    String forwardURL = null;
    //
    String currentPageUrl = (new CommonFunction().getSchemeName(request)+"www.whatuni.com/degrees" + request.getContextPath() + ".html");
    request.setAttribute("currentPageUrl", currentPageUrl);
    //
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { // TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("sessionexpirypage");
    }
    try {     
      forwardURL = (String)session.getAttribute("mobileReferralURL");
      if (session.getAttribute("showUserName") != null) {
          session.removeAttribute("showUserName");
      }
      if (session.getAttribute("userInfoList") != null) {
         session.removeAttribute("userInfoList");
      }
//      if (session.getAttribute("loggedIn") != null) {
//         session.removeAttribute("loggedIn");
//      }
      
      if (session.getAttribute("mobileReferralURL") != null) {
         session.removeAttribute("mobileReferralURL");
      }
      Cookie cookie = CookieManager.createCookie(request,"__wuuid", "");
      cookie.setMaxAge(0);
      cookie.setPath("/");
      cookie.setComment("EXPIRING COOKIE at " + System.currentTimeMillis());
      response.addCookie(cookie);
      new SessionData().addData(request, response, "y", "");
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
       String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
       session.setAttribute("fromUrl", fromUrl);
       return new ModelAndView("loginPage");
      }
      session.removeAttribute("userTrackId");
      new SessionData().addData(request, response, "userTrackId", "");
      if (GenericValidator.isBlankOrNull(forwardURL)  || stringContains(forwardURL, "/signin") || stringContains(forwardURL, "/registration")|| stringContains(forwardURL, "/userforgotPassword") || stringContains(forwardURL, "/myfinalchoice") ) {
        forwardURL = "/degrees/home.html";
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    response.sendRedirect(forwardURL);
    return null;
  }
  
  public boolean stringContains(String str, String containingStr) {
    boolean containsFlag = false;
    if (str.indexOf(containingStr) != -1) {
      containsFlag = true;
    } else {
      containsFlag = false;
    }
    return containsFlag;
  }
}
