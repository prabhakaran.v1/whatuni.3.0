package mobile.controller;

import WUI.openday.form.OpenDaysCalendarBean;

import com.wuni.util.sql.DataModel;

import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;

import com.wuni.util.valueobject.mywhatuni.MyWhatuniGetdataVO;

import org.apache.commons.lang.StringUtils;

import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mobile.valueobject.GetPostcodeAddressVO;
import oracle.sql.STRUCT;

import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class GetPostCodeAddressController{
 
  @RequestMapping(value="/getpostcodeaddress", method=RequestMethod.POST)	
  public String getPostCodeAddress(HttpServletRequest request, HttpServletResponse response) throws Exception{
    String postCode = request.getParameter("postcode");
    DataModel datamodel = new DataModel();
    ResultSet rs = null;
    Vector parameter = new Vector();
    parameter.add(postCode);
    ArrayList addressList = new ArrayList();
    StringBuffer addressBuffer = new StringBuffer();
    try{
      rs = datamodel.getResultSet("HOT_WUNI.WHATUNI_INTERACTIVE_PKG.GET_FIND_ADDRESS_FN", parameter);
      while (rs.next()) {
        java.sql.Array optionsRS = (java.sql.Array)rs.getObject("address_list");
        Object datanum[] = (Object[])optionsRS.getArray();
        for (int i = 0; i < datanum.length; i++) {
          if(i == 0){
            addressBuffer.append("");
            addressBuffer.append("$$");
            addressBuffer.append("Please choose");
            addressBuffer.append("**");
          }
          addressBuffer.append(datanum[i]);
          addressBuffer.append("$$");
          addressBuffer.append(String.valueOf(datanum[i]).replaceAll("###", " "));
          if(i < datanum.length-1){
            addressBuffer.append("**");
          }
          //addressBuffer.append("<option value=\""+datanum[i]+"\">");
          //addressBuffer.append(String.valueOf(datanum[i]).replaceAll("###", " "));
          //addressBuffer.append("</option>");
        }
      }
    }catch(Exception e){
      e.printStackTrace();
    }finally{
      datamodel.closeCursor(rs);
      datamodel = null;
    }
    response.setContentType("text/html");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter outPrint = response.getWriter();
    if(addressBuffer.length() == 0)
    {
      addressBuffer.append("NORESULTS"); 
    }
    outPrint.println(addressBuffer);
    return null;
  }
}
