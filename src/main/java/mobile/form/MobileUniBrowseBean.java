package mobile.form;

import org.apache.struts.action.ActionForm;

public class MobileUniBrowseBean {
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String collegeId = "";
  private String nameStartswith = "";
  private String reviewCount = "";
  private String seoURLString = "";
  private String collegeLocation = "";
  private String networkId = "";
  private String pageNo = "";
  private String orderBy = "";
  private String searchText = "";
  
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setNameStartswith(String nameStartswith) {
    this.nameStartswith = nameStartswith;
  }
  public String getNameStartswith() {
    return nameStartswith;
  }
  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }
  public String getReviewCount() {
    return reviewCount;
  }
  public void setSeoURLString(String seoURLString) {
    this.seoURLString = seoURLString;
  }
  public String getSeoURLString() {
    return seoURLString;
  }
  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }
  public String getCollegeLocation() {
    return collegeLocation;
  }
  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }
  public String getNetworkId() {
    return networkId;
  }
  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }
  public String getPageNo() {
    return pageNo;
  }
  public void setOrderBy(String orderBy) {
    this.orderBy = orderBy;
  }
  public String getOrderBy() {
    return orderBy;
  }
  public void setSearchText(String searchText) {
    this.searchText = searchText;
  }
  public String getSearchText() {
    return searchText;
  }
}
