package mobile.valueobject;

public class GetPostcodeAddressVO {

  private String address = null;
  public void setAddress(String address) {
    this.address = address;
  }
  public String getAddress() {
    return address;
  }
}
