package mobile.valueobject;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ProfileVO {
  //
  private String x = null;
  private String y = null;
  private String collegeId = null;
  private String requestDesc = null;
  private String networdId = null;
  private String clientIp = null;
  private String clientBrowser = null;
  private String collegeSection = null;
  private String requestURL = null;
  private String referralURL = null;
  private String metaPageName = null;
  private String metaPageFlag = null;
  private String userAgent = null;
  private String profileId = null;
  private String myhcProfileId = null;
  private String categoryCode = null;
  private String jsLog = null;//3_JUN_2014_REL
  private String journeyType = null;//24_JUN_2014_REL
  private String trackSessionId = null;
  private String pageName = null;//15_JUL_2014
  private String mobileFlag = null;
  private String profileType = null;//5_AUG_2014
  private String collegeName = null;
  private String profileURL = null;
  private String richProfileType = null;
  private String screenWidth = null;
  private String latitude = null;
  private String longitude = null;  
  private String userUcasScore = null;
  private String yearOfEntry = null;
}
