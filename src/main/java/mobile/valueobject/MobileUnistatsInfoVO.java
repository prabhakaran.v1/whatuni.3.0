package mobile.valueobject;

public class MobileUnistatsInfoVO {

  private String collegeId = null;
  private String collegeName = null;
  private String collegeNameDisplay = null;
  private String timesRank = null;
  private String fullTime = null;
  private String partTime = null;
  private String homeStudents = null;
  private String internationalStudents = null;
  private String underGraduate = null;
  private String postGraduate = null;
  private String matureStudents = null;
  private String nonMatureStudents = null;
  private String noOfStudents = null;
  private String workOrStudy = null;
  private String rankOrdinal = null;
  
  private String uniHomeURL = null;
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setTimesRank(String timesRank) {
    this.timesRank = timesRank;
  }
  public String getTimesRank() {
    return timesRank;
  }
  public void setFullTime(String fullTime) {
    this.fullTime = fullTime;
  }
  public String getFullTime() {
    return fullTime;
  }
  public void setPartTime(String partTime) {
    this.partTime = partTime;
  }
  public String getPartTime() {
    return partTime;
  }
  public void setHomeStudents(String homeStudents) {
    this.homeStudents = homeStudents;
  }
  public String getHomeStudents() {
    return homeStudents;
  }
  public void setInternationalStudents(String internationalStudents) {
    this.internationalStudents = internationalStudents;
  }
  public String getInternationalStudents() {
    return internationalStudents;
  }
  public void setUnderGraduate(String underGraduate) {
    this.underGraduate = underGraduate;
  }
  public String getUnderGraduate() {
    return underGraduate;
  }
  public void setPostGraduate(String postGraduate) {
    this.postGraduate = postGraduate;
  }
  public String getPostGraduate() {
    return postGraduate;
  }
  public void setMatureStudents(String matureStudents) {
    this.matureStudents = matureStudents;
  }
  public String getMatureStudents() {
    return matureStudents;
  }
  public void setNonMatureStudents(String nonMatureStudents) {
    this.nonMatureStudents = nonMatureStudents;
  }
  public String getNonMatureStudents() {
    return nonMatureStudents;
  }
  public void setNoOfStudents(String noOfStudents) {
    this.noOfStudents = noOfStudents;
  }
  public String getNoOfStudents() {
    return noOfStudents;
  }
  public void setWorkOrStudy(String workOrStudy) {
    this.workOrStudy = workOrStudy;
  }
  public String getWorkOrStudy() {
    return workOrStudy;
  }
  public void setUniHomeURL(String uniHomeURL) {
    this.uniHomeURL = uniHomeURL;
  }
  public String getUniHomeURL() {
    return uniHomeURL;
  }
  public void setRankOrdinal(String rankOrdinal) {
    this.rankOrdinal = rankOrdinal;
  }
  public String getRankOrdinal() {
    return rankOrdinal;
  }
}
