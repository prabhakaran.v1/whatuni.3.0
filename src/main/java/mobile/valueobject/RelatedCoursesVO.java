package mobile.valueobject;

public class RelatedCoursesVO {
  //
  private String collegeId = "";
  private String collegeName = "";
  private String courseId = "";
  private String courseName = "";
  private String collegeNameDisplay = "";
  private String enquiryDetails = "";
  private String websiteForm = "";
  private String websitePrice = "";
  private String studyLevelDesc = "";
  private String pageName = "";
  private String courseTitle = "";
//
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }
  public String getCourseName() {
    return courseName;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setEnquiryDetails(String enquiryDetails) {
    this.enquiryDetails = enquiryDetails;
  }
  public String getEnquiryDetails() {
    return enquiryDetails;
  }
  public void setWebsiteForm(String websiteForm) {
    this.websiteForm = websiteForm;
  }
  public String getWebsiteForm() {
    return websiteForm;
  }
  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }
  public String getWebsitePrice() {
    return websitePrice;
  }
  public void setStudyLevelDesc(String studyLevelDesc) {
    this.studyLevelDesc = studyLevelDesc;
  }
  public String getStudyLevelDesc() {
    return studyLevelDesc;
  }
  public void setPageName(String pageName) {
    this.pageName = pageName;
  }
  public String getPageName() {
    return pageName;
  }
  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }
  public String getCourseTitle() {
    return courseTitle;
  }
}
