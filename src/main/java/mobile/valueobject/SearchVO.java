package mobile.valueobject;

public class SearchVO {
  //
  private String x = null;
  private String y = null;
  private String orderByUrl = null; 
  private String locationBrowseUrl = null; 
  private String searchWhat = null; 
  private String phraseSearch = null; 
  private String collegeName = null; 
  private String qualification = null; 
  private String country = null; 
  private String townCity = null; 
  private String postcode = null; 
  private String searchRange = null; 
  private String postflag = null; 
  private String locationId = null; 
  private String studyMode = null; 
  private String courseDuration = null; 
  private String lucky = null; 
  private String crsSearch = null; 
  private String submit = null; 
  private String affiliateId = null; 
  private String searchHOW = null; 
  private String sType = null; 
  private String searchCategory = null; 
  private String searchCategoryId = null; 
  private String searchCareer = null; 
  private String careerId = null; 
  private String refId = null; 
  private String nrp = null; 
  private String area = null; 
  private String pgType = null; 
  private String searchCol = null; 
  private String countyId = null; 
  private String searchThru = null; 
  private String ucasCode = null; 
  private String deemedUniFlag = null;
  private String entityText = null; 
  private String userAgent = null; 
  private String requestURL = null; 
  private String pageNo = null; 
  private String basketId = null; 
  private String searchType = null; 
  private String entryLevel = null; 
  private String entryPoints = null;
  private String searchKeyword = null;
  private String categoryName = null;
  private String categoryCode = null;
  private String pageName = null;  
  private String searchAction = null;
  private String headerId = null;
  private String collegeId = null;
  private String aui = null;
  private String moduleStr = null;//Search redesign
  private String univLocTypeId = null;
  private String campusTypeId = null;
  private String russelGroupId = null;  
  private String univLocTypeName = null;
  private String campusTypeName = null;
  private String russelGroupName = null;  
  private String empRate = null;
  private String ucasTariff = null;
  private String networkId = null;
  private String recentFilterApplied = null;
  private String jacsCode = null;//3_JUN_2014    
  private String userTrackSessionId = null;
  private String urlProviderName = null;
  private String newCollegeName = null;
  private String searchCatCode = null;
  private String searchCatId = null;
  private String regionFlag = null;
  private String assessmentType = null;
  private String reviewSubjectOne = null;
  private String reviewSubjectTwo = null;
  private String reviewSubjectThree = null;
  private String reviewSubjects = null;
  private String goApplyFlag = null;
  private String randomNumber = null;
  private String subjectSessionId = null;
  private String clientIp = null;
  
  public String getClientIp() {
	return clientIp;
  }
  public void setClientIp(String clientIp) {
	this.clientIp = clientIp;
  }
//
  public void setX(String x) {
    this.x = x;
  }
  public String getX() {
    return x;
  }
  public void setY(String y) {
    this.y = y;
  }
  public String getY() {
    return y;
  }
  public void setOrderByUrl(String orderByUrl) {
    this.orderByUrl = orderByUrl;
  }
  public String getOrderByUrl() {
    return orderByUrl;
  }
  public void setLocationBrowseUrl(String locationBrowseUrl) {
    this.locationBrowseUrl = locationBrowseUrl;
  }
  public String getLocationBrowseUrl() {
    return locationBrowseUrl;
  }
  public void setSearchWhat(String searchWhat) {
    this.searchWhat = searchWhat;
  }
  public String getSearchWhat() {
    return searchWhat;
  }
  public void setPhraseSearch(String phraseSearch) {
    this.phraseSearch = phraseSearch;
  }
  public String getPhraseSearch() {
    return phraseSearch;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setQualification(String qualification) {
    this.qualification = qualification;
  }
  public String getQualification() {
    return qualification;
  }
  public void setCountry(String country) {
    this.country = country;
  }
  public String getCountry() {
    return country;
  }
  public void setTownCity(String townCity) {
    this.townCity = townCity;
  }
  public String getTownCity() {
    return townCity;
  }
  public void setPostcode(String postcode) {
    this.postcode = postcode;
  }
  public String getPostcode() {
    return postcode;
  }
  public void setSearchRange(String searchRange) {
    this.searchRange = searchRange;
  }
  public String getSearchRange() {
    return searchRange;
  }
  public void setPostflag(String postflag) {
    this.postflag = postflag;
  }
  public String getPostflag() {
    return postflag;
  }
  public void setLocationId(String locationId) {
    this.locationId = locationId;
  }
  public String getLocationId() {
    return locationId;
  }
  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }
  public String getStudyMode() {
    return studyMode;
  }
  public void setCourseDuration(String courseDuration) {
    this.courseDuration = courseDuration;
  }
  public String getCourseDuration() {
    return courseDuration;
  }
  public void setLucky(String lucky) {
    this.lucky = lucky;
  }
  public String getLucky() {
    return lucky;
  }
  public void setCrsSearch(String crsSearch) {
    this.crsSearch = crsSearch;
  }
  public String getCrsSearch() {
    return crsSearch;
  }
  public void setSubmit(String submit) {
    this.submit = submit;
  }
  public String getSubmit() {
    return submit;
  }
  public void setAffiliateId(String affiliateId) {
    this.affiliateId = affiliateId;
  }
  public String getAffiliateId() {
    return affiliateId;
  }
  public void setSearchHOW(String searchHOW) {
    this.searchHOW = searchHOW;
  }
  public String getSearchHOW() {
    return searchHOW;
  }
  public void setSType(String sType) {
    this.sType = sType;
  }
  public String getSType() {
    return sType;
  }
  public void setSearchCategory(String searchCategory) {
    this.searchCategory = searchCategory;
  }
  public String getSearchCategory() {
    return searchCategory;
  }
  public void setSearchCategoryId(String searchCategoryId) {
    this.searchCategoryId = searchCategoryId;
  }
  public String getSearchCategoryId() {
    return searchCategoryId;
  }
  public void setSearchCareer(String searchCareer) {
    this.searchCareer = searchCareer;
  }
  public String getSearchCareer() {
    return searchCareer;
  }
  public void setCareerId(String careerId) {
    this.careerId = careerId;
  }
  public String getCareerId() {
    return careerId;
  }
  public void setRefId(String refId) {
    this.refId = refId;
  }
  public String getRefId() {
    return refId;
  }
  public void setNrp(String nrp) {
    this.nrp = nrp;
  }
  public String getNrp() {
    return nrp;
  }
  public void setArea(String area) {
    this.area = area;
  }
  public String getArea() {
    return area;
  }
  public void setPgType(String pgType) {
    this.pgType = pgType;
  }
  public String getPgType() {
    return pgType;
  }
  public void setSearchCol(String searchCol) {
    this.searchCol = searchCol;
  }
  public String getSearchCol() {
    return searchCol;
  }
  public void setCountyId(String countyId) {
    this.countyId = countyId;
  }
  public String getCountyId() {
    return countyId;
  }
  public void setSearchThru(String searchThru) {
    this.searchThru = searchThru;
  }
  public String getSearchThru() {
    return searchThru;
  }
  public void setUcasCode(String ucasCode) {
    this.ucasCode = ucasCode;
  }
  public String getUcasCode() {
    return ucasCode;
  }
  public void setDeemedUniFlag(String deemedUniFlag) {
    this.deemedUniFlag = deemedUniFlag;
  }
  public String getDeemedUniFlag() {
    return deemedUniFlag;
  }
  public void setEntityText(String entityText) {
    this.entityText = entityText;
  }
  public String getEntityText() {
    return entityText;
  }
  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }
  public String getUserAgent() {
    return userAgent;
  }
  public void setRequestURL(String requestURL) {
    this.requestURL = requestURL;
  }
  public String getRequestURL() {
    return requestURL;
  }
  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }
  public String getPageNo() {
    return pageNo;
  }
  public void setBasketId(String basketId) {
    this.basketId = basketId;
  }
  public String getBasketId() {
    return basketId;
  }
  public void setSearchType(String searchType) {
    this.searchType = searchType;
  }
  public String getSearchType() {
    return searchType;
  }
  public void setEntryLevel(String entryLevel) {
    this.entryLevel = entryLevel;
  }
  public String getEntryLevel() {
    return entryLevel;
  }
  public void setEntryPoints(String entryPoints) {
    this.entryPoints = entryPoints;
  }
  public String getEntryPoints() {
    return entryPoints;
  }
  public void setSearchKeyword(String searchKeyword) {
    this.searchKeyword = searchKeyword;
  }
  public String getSearchKeyword() {
    return searchKeyword;
  }
  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }
  public String getCategoryName() {
    return categoryName;
  }
  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }
  public String getCategoryCode() {
    return categoryCode;
  }
  public void setPageName(String pageName) {
    this.pageName = pageName;
  }
  public String getPageName() {
    return pageName;
  }
  public void setSearchAction(String searchAction) {
    this.searchAction = searchAction;
  }
  public String getSearchAction() {
    return searchAction;
  }
  public void setHeaderId(String headerId) {
    this.headerId = headerId;
  }
  public String getHeaderId() {
    return headerId;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setAui(String aui) {
    this.aui = aui;
  }
  public String getAui() {
    return aui;
  }
  public void setModuleStr(String moduleStr) {
    this.moduleStr = moduleStr;
  }
  public String getModuleStr() {
    return moduleStr;
  }
  public void setUnivLocTypeId(String univLocTypeId) {
    this.univLocTypeId = univLocTypeId;
  }
  public String getUnivLocTypeId() {
    return univLocTypeId;
  }
  public void setCampusTypeId(String campusTypeId) {
    this.campusTypeId = campusTypeId;
  }
  public String getCampusTypeId() {
    return campusTypeId;
  }
  public void setRusselGroupId(String russelGroupId) {
    this.russelGroupId = russelGroupId;
  }
  public String getRusselGroupId() {
    return russelGroupId;
  }
  public void setEmpRate(String empRate) {
    this.empRate = empRate;
  }
  public String getEmpRate() {
    return empRate;
  }
  public void setUcasTariff(String ucasTariff) {
    this.ucasTariff = ucasTariff;
  }
  public String getUcasTariff() {
    return ucasTariff;
  }

  public void setNetworkId(String networkId)
  {
    this.networkId = networkId;
  }

  public String getNetworkId()
  {
    return networkId;
  }
  public void setRecentFilterApplied(String recentFilterApplied) {
    this.recentFilterApplied = recentFilterApplied;
  }
  public String getRecentFilterApplied() {
    return recentFilterApplied;
  }
  public void setJacsCode(String jacsCode) {
    this.jacsCode = jacsCode;
  }
  public String getJacsCode() {
    return jacsCode;
  }
  public void setUserTrackSessionId(String userTrackSessionId) {
    this.userTrackSessionId = userTrackSessionId;
  }
  public String getUserTrackSessionId() {
    return userTrackSessionId;
  }
  public void setUnivLocTypeName(String univLocTypeName) {
    this.univLocTypeName = univLocTypeName;
  }
  public String getUnivLocTypeName() {
    return univLocTypeName;
  }
  public void setCampusTypeName(String campusTypeName) {
    this.campusTypeName = campusTypeName;
  }
  public String getCampusTypeName() {
    return campusTypeName;
  }
  public void setRusselGroupName(String russelGroupName) {
    this.russelGroupName = russelGroupName;
  }
  public String getRusselGroupName() {
    return russelGroupName;
  }
  public void setUrlProviderName(String urlProviderName) {
    this.urlProviderName = urlProviderName;
  }
  public String getUrlProviderName() {
    return urlProviderName;
  }
  public void setNewCollegeName(String newCollegeName) {
    this.newCollegeName = newCollegeName;
  }
  public String getNewCollegeName() {
    return newCollegeName;
  }
  public void setAssessmentType(String assessmentType) {
    this.assessmentType = assessmentType;
  }
  public String getAssessmentType() {
    return assessmentType;
  }
  public void setReviewSubjectOne(String reviewSubjectOne) {
    this.reviewSubjectOne = reviewSubjectOne;
  }
  public String getReviewSubjectOne() {
    return reviewSubjectOne;
  }
  public void setReviewSubjectTwo(String reviewSubjectTwo) {
    this.reviewSubjectTwo = reviewSubjectTwo;
  }
  public String getReviewSubjectTwo() {
    return reviewSubjectTwo;
  }
  public void setReviewSubjectThree(String reviewSubjectThree) {
    this.reviewSubjectThree = reviewSubjectThree;
  }
  public String getReviewSubjectThree() {
    return reviewSubjectThree;
  }
  public void setSearchCatCode(String searchCatCode) {
    this.searchCatCode = searchCatCode;
  }
  public String getSearchCatCode() {
    return searchCatCode;
  }
  public void setSearchCatId(String searchCatId) {
    this.searchCatId = searchCatId;
  }
  public String getSearchCatId() {
    return searchCatId;
  }
  public void setRegionFlag(String regionFlag) {
    this.regionFlag = regionFlag;
  }
  public String getRegionFlag() {
    return regionFlag;
  }
  public void setReviewSubjects(String reviewSubjects) {
    this.reviewSubjects = reviewSubjects;
  }
  public String getReviewSubjects() {
    return reviewSubjects;
  }

public String getsType() {
   return sType;
}

public void setsType(String sType) {
   this.sType = sType;
}

public String getGoApplyFlag() {
   return goApplyFlag;
}

public void setGoApplyFlag(String goApplyFlag) {
   this.goApplyFlag = goApplyFlag;
}

public String getRandomNumber() {
   return randomNumber;
}

public void setRandomNumber(String randomNumber) {
   this.randomNumber = randomNumber;
}

public String getSubjectSessionId() {
   return subjectSessionId;
}

public void setSubjectSessionId(String subjectSessionId) {
   this.subjectSessionId = subjectSessionId;
}
}
