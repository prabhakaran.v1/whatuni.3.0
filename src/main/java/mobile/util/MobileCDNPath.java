package mobile.util;

import java.util.Random;
import java.util.ResourceBundle;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

public class MobileCDNPath {
  public static String dbName = "grhotc";
  static {
    dbName = new CommonFunction().getSysVarValue("LIVE_DB");
  }

  public MobileCDNPath() {
  }
  
  public static String getCSSPath(){

    ResourceBundle resourcebundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    if(dbName!= null && dbName.equalsIgnoreCase("grdom")){
       return GlobalConstants.WHATUNI_SCHEME_NAME +resourcebundle.getString("wuni.cssdomain")+resourcebundle.getString("wuni.con.path");//Added WHATUNI_SCHEME_NAME by Indumathi Mar-29-16
     }else if(dbName != null && dbName.equalsIgnoreCase("gs2fo")){
       return GlobalConstants.WHATUNI_SCHEME_NAME +resourcebundle.getString("wuni.failover.cssdomain")+resourcebundle.getString("wuni.con.path");
     }else if(dbName != null && dbName.equalsIgnoreCase("domtest")){
         return GlobalConstants.WHATUNI_SCHEME_NAME +resourcebundle.getString("wuni.mobiletest.domain")+resourcebundle.getString("wuni.con.path");
         //return GlobalConstants.WHATUNI_SCHEME_NAME +resourcebundle.getString("wuni.cssdomain")+resourcebundle.getString("wuni.con.path");
         
     }else if(dbName != null && dbName.equalsIgnoreCase("domdev")){
       return GlobalConstants.WHATUNI_SCHEME_NAME +resourcebundle.getString("wuni.mobiledev.domain")+resourcebundle.getString("wuni.con.path");
     }else{
       return GlobalConstants.WHATUNI_SCHEME_NAME +resourcebundle.getString("wuni.cssdomain")+resourcebundle.getString("wuni.con.path");
     }
    //return "/degrees";            //TODO Comment while deploying in live and uncomment above line.
  }

     public static String getJsPath(){
         ResourceBundle resourceBundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
         if(dbName != null && dbName.equalsIgnoreCase("grdom")){
             return GlobalConstants.WHATUNI_SCHEME_NAME +resourceBundle.getString("wuni.jsdomain")+resourceBundle.getString("wuni.con.path");//Added WHATUNI_SCHEME_NAME by Indumathi Mar-29-16
         }else if(dbName != null && dbName.equalsIgnoreCase("gs2fo")){
             return GlobalConstants.WHATUNI_SCHEME_NAME +resourceBundle.getString("wuni.failover.jsdomain")+resourceBundle.getString("wuni.con.path");
         }else if(dbName != null && dbName.equalsIgnoreCase("domtest")){
             return GlobalConstants.WHATUNI_SCHEME_NAME +resourceBundle.getString("wuni.mobiletest.domain")+resourceBundle.getString("wuni.con.path");
             //return GlobalConstants.WHATUNI_SCHEME_NAME +resourceBundle.getString("wuni.jsdomain")+resourceBundle.getString("wuni.con.path");
         }else if(dbName != null && dbName.equalsIgnoreCase("domdev")){
           return GlobalConstants.WHATUNI_SCHEME_NAME +resourceBundle.getString("wuni.mobiledev.domain")+resourceBundle.getString("wuni.con.path");
         }else{
           return GlobalConstants.WHATUNI_SCHEME_NAME +resourceBundle.getString("wuni.jsdomain")+resourceBundle.getString("wuni.con.path");
         }
       //return "/degrees";    //TODO Comment while deploying in live and uncomment above line.
     }
     
      public static String getImgPath(String imagesrc,int imgNum)//String imgNum
      {
        int domainPath = 0;
        int dc = 8; //number of domains
        String imgpath = null;
        try{
         if(imgNum > 0)//(imgNum!=null)
         {
         //int i=Integer.parseInt("imgNum");
          int i=imgNum;
         domainPath = (i>dc)?((i%dc==0)?((i%dc)+dc):(i%dc)):i;
         }
         else
         {
           Random rnd = new Random();
           int i = rnd.nextInt(dc) +1 ;
           domainPath = i;//(i>dc)?((i%dc==0)?((i%dc)+dc):(i%dc)):i;
         }
         String objPath=Integer.toString(domainPath); 
         Object[] obj ={objPath};
         if(dbName != null &&(dbName.equalsIgnoreCase("grdom") || dbName.equalsIgnoreCase("gs2fo"))){
           imgpath = CommonUtil.getResourceMessage("wuni.imagedomain", obj)+imagesrc;
         }else if(dbName != null && dbName.equalsIgnoreCase("domtest")){
           imgpath = CommonUtil.getResourceMessage("wuni.mobiletest.domain", obj)+imagesrc;
           //imgpath = CommonUtil.getResourceMessage("wuni.imagedomain", obj)+imagesrc;  //TODO while giving testing need to comment this one.
         }else if(dbName != null && dbName.equalsIgnoreCase("domdev")){
           imgpath = CommonUtil.getResourceMessage("wuni.mobiledev.domain", obj)+imagesrc;
         }
          imgpath=GlobalConstants.WHATUNI_SCHEME_NAME+imgpath;
         
        }catch(Exception e){
          e.printStackTrace();
        }
        return imgpath;
      } 
}
