package mobile.util;

import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.wuni.util.CommonValidator;
import com.wuni.util.sql.DataModel;

public class MobileUtils {
  public static String dbName = "grhotc";
  static {
    dbName = new CommonFunction().getSysVarValue("LIVE_DB");
  }

  /**
    * This Class is used to identify the request came from mobile version or desktop version and based on that it will return true or false.
    * This will also return false when ever the user decide to see the full website from mobile.
    * */
  public static boolean mobileUserAgentCheck(HttpServletRequest request) {
    /**
    * Below code stores a list of user agent in String array. If all three condition below fails, it will return false.
    * Check for X-WAP-PROFILE in header and return true if its found.
    * Esle if it will look for the user agent with string array and if its matched it will return true.
    * Else if it checks for wap string in headers ACCEPT parameter and return true if found.
    *
    **/
    String[] mobileUesrAgent = GlobalConstants.mobileUesrAgent;
    if (checkFullsiteVersion(request)) {
      return false;
    }
    if (request.getHeader("X-WAP-PROFILE") != null && ((request.getHeader("user-agent").toLowerCase()).indexOf("ipad") == -1)) {
      return true;
    } else if (request.getHeader("user-agent") != null && ((request.getHeader("user-agent").toLowerCase()).indexOf("ipad") == -1)) {
      for (int i = 0; i < mobileUesrAgent.length; i++) {
        if ((request.getHeader("user-agent").toLowerCase()).indexOf(mobileUesrAgent[i]) != -1) {
          return true;
        }
      }
    } else if (request.getHeader("ACCEPT") != null && ((request.getHeader("ACCEPT")).toLowerCase()).indexOf("wap") != -1 && ((request.getHeader("user-agent").toLowerCase()).indexOf("ipad") == -1)) {
      return true;
    }
    return false;
  }
  
  
  /**
    * This Class is used to identify the request came from mobile version or desktop version and based on that it will return true or false.
    * This will also return false when ever the user decide to see the full website from mobile.
    * */
  public static boolean userAgentCheck(HttpServletRequest request) {
    /**
    * Below code stores a list of user agent in String array. If all three condition below fails, it will return false.
    * Check for X-WAP-PROFILE in header and return true if its found.
    * Esle if it will look for the user agent with string array and if its matched it will return true.
    * Else if it checks for wap string in headers ACCEPT parameter and return true if found.
    *
    **/
    String[] mobileUesrAgent = GlobalConstants.mobileUesrAgent;    
    if (request.getHeader("X-WAP-PROFILE") != null && ((request.getHeader("user-agent").toLowerCase()).indexOf("ipad") == -1)) {
      return true;
    } else if (request.getHeader("user-agent") != null && ((request.getHeader("user-agent").toLowerCase()).indexOf("ipad") == -1)) {
      for (int i = 0; i < mobileUesrAgent.length; i++) {
        if ((request.getHeader("user-agent").toLowerCase()).indexOf(mobileUesrAgent[i]) != -1) {
          return true;
        }
      }
    } else if (request.getHeader("ACCEPT") != null && ((request.getHeader("ACCEPT")).toLowerCase()).indexOf("wap") != -1 && ((request.getHeader("user-agent").toLowerCase()).indexOf("ipad") == -1)) {
      return true;
    }
    return false;
  }
  //
  public static boolean checkFullsiteVersion(HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    if (cookies != null && cookies.length > 0) {
      for (int i = 0; i < cookies.length; i++) {
        if ("switchsitever".equalsIgnoreCase(cookies[i].getName()) && "desktop".equalsIgnoreCase(cookies[i].getValue())) {
          return true;
        }
      }
    }
    return false;
  }
  public void loadmobileUserLoggedInformation(HttpServletRequest request, ServletContext servletContext, HttpServletResponse response) {
    HttpSession session = request.getSession();
    new SecurityEvaluator().overrideJSessionId(request, response);
    String userId = new SessionData().getData(request, "y");
    if (userId != null && !userId.equals("0") && userId.trim().length() > 0) {
      if (session.getAttribute("userInfoList") == null) {
        ArrayList userInfoList = new CommonFunction().getUserInformation(userId, request, servletContext);
        if (userInfoList != null && userInfoList.size() > 0) {
          session.setAttribute("userInfoList", userInfoList);
        }
      }
    } else {
      session.removeAttribute("userInfoList");
    }
  }
  //
   /**
    *
    * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-details/[COURSE_TITLE]-course-details/[COURSE_ID]/[OPPORTUNITY_ID]/[COLLEGE_ID]/cdetail.html
    * EXAMPLE:     www.whatuni.com/degrees/courses/Postgraduate-details/Accounting-MPhil-course-details/29837179/OPPid/3769/cdetail.html
    *
    */
   public String mobileCrsOppDetailPageURL(String studyLevelDesc, String courseTitle, String courseId, String collegeId, String opportunityId, String pagename) {//15_JUL_2014
     CommonValidator validate = new CommonValidator();
     if (validate.isBlankOrNull(studyLevelDesc) || validate.isBlankOrNull(courseTitle) || validate.isBlankOrNull(courseId) || validate.isBlankOrNull(collegeId)) {
       validate = null;
       return "/degrees/home.html";
     }
     StringBuffer courseDetailsPageURL = new StringBuffer();
     CommonFunction comFn = new CommonFunction();
     if(courseTitle.indexOf("/") != -1){
         courseTitle = courseTitle.replaceAll("/", " ");
     }
     courseTitle = comFn.replaceHypen(comFn.replaceURL(comFn.replaceSpecialCharacter(courseTitle)));
     //
     courseDetailsPageURL.append("/degrees/courses/");
     courseDetailsPageURL.append(studyLevelDesc);
      //courseDetailsPageURL.append("-details/");
     courseDetailsPageURL.append("/");
     courseDetailsPageURL.append(courseTitle);
     courseDetailsPageURL.append("-course-details/");
     courseDetailsPageURL.append(courseId);
     courseDetailsPageURL.append("/");
     courseDetailsPageURL.append(opportunityId);
     courseDetailsPageURL.append("/");
     courseDetailsPageURL.append(collegeId);
     if("clcsearch".equals(pagename)){//15_JUL_2014
       courseDetailsPageURL.append("/cdetail.html?clearing");       
     }else{
       courseDetailsPageURL.append("/cdetail.html");      
     }
     validate = null;
     comFn = null;
     return courseDetailsPageURL.toString();
   }  
   
   public void autoLogin(HttpServletRequest request, HttpServletResponse response){
     String userId = null;
     userId = new CookieManager().getCookieValue(request, "__wuuid");
     if(!GenericValidator.isBlankOrNull(userId)){
      new SessionData().addData(request, response, "y", String.valueOf(userId));
       setSessionId(request, response,userId);
      new CommonFunction().getUserName(String.valueOf(userId), request);
      HttpSession session = request.getSession();
       session.removeAttribute("userInfoList");
       session.removeAttribute("latestMemberList");
       session.setAttribute("loggedIn", "YES");
      new MobileUtils().loadmobileUserLoggedInformation(request, null, response);  
     }
   }
  
  public void setSessionId(HttpServletRequest request, HttpServletResponse response, String userId){  
    DataModel dataModel =   new DataModel();
    Vector userDetail   =   new Vector();
    userDetail.clear();
    userDetail.add(userId);
    try{
    String sessionId = dataModel.getString("WU_MOBILE_ENQUIRY_PKG.get_session_id_fn", userDetail);
    new SessionData().addData(request, response, "x", String.valueOf(sessionId)); 
    } catch(Exception exception) {
        exception.printStackTrace();
    }
  }
}
