package mobile.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.search.ProviderResultPageVO;

public class ProviderResultsRowMapper implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    ProviderResultPageVO providerResultPageVO = new ProviderResultPageVO();    
    providerResultPageVO.setCourseId(resultset.getString("COURSE_ID"));
    providerResultPageVO.setCourseDescription(resultset.getString("COURSE_DESC"));
    providerResultPageVO.setCourseTitle(resultset.getString("COURSE_TITLE"));
    providerResultPageVO.setCourseDuration(resultset.getString("DURATION"));
    providerResultPageVO.setLearndDirQual(resultset.getString("LEARNDIR_QUAL"));
    providerResultPageVO.setCollegeId(resultset.getString("COLLEGE_ID"));
    providerResultPageVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
    providerResultPageVO.setWebsite(resultset.getString("WEBSITE"));
    providerResultPageVO.setProfile(resultset.getString("PROFILE"));
    providerResultPageVO.setVenueName(resultset.getString("VENUE_NAME"));
    providerResultPageVO.setUcasCode(resultset.getString("UCAS_CODE"));
    providerResultPageVO.setUcasFlag(resultset.getString("UCAS_FLAG"));
    providerResultPageVO.setUcasPoint(resultset.getString("UCAS_TARIFF")); 
    providerResultPageVO.setStudyMode(resultset.getString("STUDY_MODE"));
    providerResultPageVO.setWasThisCourseShortListed(resultset.getString("COURSE_IN_BASKET"));
    providerResultPageVO.setSeoStudyLevelText(resultset.getString("SEO_STUDY_LEVEL_TEXT"));
    providerResultPageVO.setWebsitePurchaseFlag(resultset.getString("WEBSITE_PUR_FLAG"));
    providerResultPageVO.setProviderUrl(resultset.getString("PROVIDER_URL"));
    providerResultPageVO.setDepartmentOrFaculty(resultset.getString("DEPARTMENT_OR_FACULTY"));
    providerResultPageVO.setAvailableStudyModes(resultset.getString("AVAILABLE_STUDY_MODES"));
    providerResultPageVO.setFees(resultset.getString("search_course_fee"));  
    providerResultPageVO.setFeeDuration(resultset.getString("fee_duration"));
    providerResultPageVO.setPlacesAvailable(resultset.getString("places_available"));
    providerResultPageVO.setLastUpdatedDate(resultset.getString("last_updated_date"));
    providerResultPageVO.setOpportunityId(resultset.getString("OPPORTUNITY_ID"));
    return providerResultPageVO; 
  }
}
