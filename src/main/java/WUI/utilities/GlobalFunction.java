package WUI.utilities;

import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.CLOB;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionForm;
import org.springframework.stereotype.Component;

import spring.form.CourseJourneyBean;
import spring.form.VideoReviewListBean;
import WUI.admin.bean.AdminOpenDaysBean;
import WUI.basket.form.BasketBean;
import WUI.members.form.MembersSearchBean;
import WUI.registration.bean.RegistrationBean;
import WUI.review.bean.FriendEmailBean;
import WUI.review.bean.ReviewListBean;
import WUI.search.form.SearchBean;
import WUI.search.form.SearchResultBean;
import WUI.search.form.SubjectProfileBean;
import WUI.uniphotogallery.form.UniPhotoGalleryBean;
import WUI.videoreview.bean.VideoReviewBean;
import WUI.videoreview.controller.VideoDetail;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;
import com.wuni.util.sql.DataModel;
import com.wuni.valueobjects.InsightsVO;

/**
  * @GlobalFunction.java
  * @Version : 2.0
  * @December 3 2007
  * @www.whatuni.com
  * @author By : Balraj. Selva Kumar
  * @Purpose  : Program contains lot of functions that are shared among the dirrerent programs
  * *************************************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                             Rel Ver.
  * *************************************************************************************************************************************************
  * 09.06.2015     Yogeswari                          method to check clearing/non-clearing page
  * 21:07:2015     Yogeswari                          method to return user image path.
  * 10_NOV_2020    Sujitha V                 2.3      Added session for YOE to log in d_session_log(trackExternalProspectus).  wu_20201110
  */
@Component
public class GlobalFunction {

  public GlobalFunction() {
  }

  public String getCategoryId(String categoryCode, String qualification) {
    DataModel dataModel = new DataModel();
    Vector parameters = new Vector();
    String categoryId = "";
    parameters.add(categoryCode);
    parameters.add(qualification);
    try {
      categoryId = dataModel.getString("WU_UTIL_PKG.GET_CATEGORY_ID_FN", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return categoryId;
  }

  public ArrayList getLocationSubCategoryListPod(String subCategoryId) {
    DataModel dataModel = new DataModel();
    Vector parameters = new Vector();
    ResultSet resultset = null;
    CourseJourneyBean bean = null;
    ArrayList locationSubCategoryList = new ArrayList();
    CommonFunction common = new CommonFunction();
    parameters.add(subCategoryId);
    try {
      resultset = dataModel.getResultSet("WU_BROWSE_PKG.GET_SUBCAT_LOC_NAMES_FN", parameters);
      while (resultset.next()) {
        bean = new CourseJourneyBean();
        bean.setSubjectId(resultset.getString("course_id"));
        bean.setSubjectName(resultset.getString("course_name"));
        bean.setSeoSubjectName(common.replaceHypen(common.replaceURL(resultset.getString("course_name"))));
        bean.setCategoryCode(resultset.getString("category_code"));
        bean.setStudyLevelId(resultset.getString("study_level"));
        bean.setCategoryCodeLength(resultset.getString("cat_code_length"));
        bean.setParentCategoryId(resultset.getString("parent_category_code"));
        locationSubCategoryList.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return locationSubCategoryList;
  }

  /**
    *  This function is used to Load teaser profile pod for the given subjectid.
    * @param request
    * @param subjectId
    * @param totalRecord
    * @return Teaser profile As List
    */
  public ArrayList getSubjectTeaserPod(HttpServletRequest request, String subjectId, String totalRecord) {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    String clientIp = request.getHeader("CLIENTIP");
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    }
    String clientBrowser = request.getHeader("user-agent");
    ResultSet resultset = null;
    ArrayList list = new ArrayList();
    SubjectProfileBean bean = null;
    vector.clear();
    vector.add(new SessionData().getData(request, "x"));
    vector.add(new SessionData().getData(request, "y"));
    vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
    vector.add(subjectId);
    vector.add(clientIp);
    vector.add(clientBrowser);
    vector.add(totalRecord);
    String requestUrl = CommonUtil.getRequestedURL(request); //Change the getRequestURL by Prabha on 28_Jun_2016   
    vector.add(requestUrl);
    vector.add(request.getHeader("referer"));
    vector.addElement(new SessionData().getData(request, "userTrackId"));
    try {
      resultset = dataModel.getResultSet("wu_college_info_pkg.get_subject_teaser_list_fn", vector);
      while (resultset.next()) {
        bean = new SubjectProfileBean();
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setSubjectDesc(resultset.getString("subject_desc"));
        bean.setProfileId(resultset.getString("profile_id"));
        bean.setSubjectId(resultset.getString("category_code"));
        bean.setImageId(resultset.getString("image_id"));
        bean.setImagePath(resultset.getString("image_path"));
        bean.setVideoThumbnailUrl("");
        if (resultset.getString("image_path") != null && resultset.getString("image_path").trim().length() == 0) {
          bean.setImageType("noimage");
        } else {
          if (resultset.getString("image_path") != null && (resultset.getString("image_path").indexOf(".flv") >= 0 || resultset.getString("image_path").indexOf(".mp4") > -1)) {
            bean.setImageType("video");
            bean.setVideoThumbnailUrl(new GlobalFunction().getVideoThumbPath(resultset.getString("image_path"), resultset.getString("image_id")));
          } else {
            bean.setImageType("image");
          }
        }
        if (resultset.getString("js_stats_log_id") != null && resultset.getString("js_stats_log_id").trim().length() > 0) {
          request.getSession().setAttribute("stats_log_id", resultset.getString("js_stats_log_id"));
        }
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }

  /**
    *  This function is used to Load teaser profile pod for the given browse subject code.
    * @param request, subjectId, totalRecord
    * @return Teaser profile As List
    */
  public ArrayList getBrowseSubjectTeaserPod(HttpServletRequest request, String subjectCode, String totalRecord) {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    String clientIp = request.getHeader("CLIENTIP");
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    }
    String clientBrowser = request.getHeader("user-agent");
    ResultSet resultset = null;
    ArrayList list = new ArrayList();
    SubjectProfileBean bean = null;
    vector.clear();
    vector.add(new SessionData().getData(request, "x"));
    vector.add(new SessionData().getData(request, "y"));
    vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
    vector.add(subjectCode);
    vector.add(clientIp);
    vector.add(clientBrowser);
    vector.add(totalRecord);
    String requestUrl = CommonUtil.getRequestedURL(request); //Change the getRequestURL by Prabha on 28_Jun_2016
    vector.add(requestUrl);
    vector.add(request.getHeader("referer"));
    vector.add(new SessionData().getData(request, "userTrackId"));
    try {
      resultset = dataModel.getResultSet("wu_common_info_pkg.get_browse_teaser_list_fn", vector);
      while (resultset.next()) {
        bean = new SubjectProfileBean();
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setSubjectDesc(resultset.getString("subject_desc"));
        bean.setProfileId(resultset.getString("profile_id"));
        bean.setSubjectId(resultset.getString("category_code"));
        bean.setImageId(resultset.getString("image_id"));
        bean.setImagePath(resultset.getString("image_path"));
        bean.setVideoThumbnailUrl("");
        if (resultset.getString("image_path") != null && resultset.getString("image_path").trim().length() == 0) {
          bean.setImageType("noimage");
        } else {
          if (resultset.getString("image_path") != null && (resultset.getString("image_path").indexOf(".flv") >= 0 || resultset.getString("image_path").indexOf(".mp4") > -1)) {
            bean.setImageType("video");
            bean.setVideoThumbnailUrl(new GlobalFunction().getVideoThumbPath(resultset.getString("image_path"), resultset.getString("image_id")));
          } else {
            bean.setImageType("image");
          }
        }
        if (resultset.getString("js_stats_log_id") != null && resultset.getString("js_stats_log_id").trim().length() > 0) {
          request.getSession().setAttribute("stats_log_id", resultset.getString("js_stats_log_id"));
        }
        request.setAttribute("teasetSubjectName", resultset.getString("category_name"));
        request.setAttribute("teaserSubjectId", resultset.getString("category_code"));
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }

  /**
    *  This function is used to load the subject profiles for the given collegeid.
    * @param collegeId
    * @param request
    * @return Subject profiles as Collection Object
    */
  public ArrayList getSubjectListProfile(String collegeId, HttpServletRequest request) {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    ResultSet resultset = null;
    ArrayList list = new ArrayList();
    SubjectProfileBean bean = null;
    vector.clear();
    vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
    vector.add(collegeId);
    try {
      resultset = dataModel.getResultSet("wu_college_info_pkg.get_dept_profile_list_fn", vector);
      String collegeName = "";
      while (resultset.next()) {
        bean = new SubjectProfileBean();
        bean.setSubjectId(resultset.getString("category_code"));
        bean.setSectionId(resultset.getString("section_id"));
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setSubjectDesc(resultset.getString("subject_title"));
        bean.setProfileId(resultset.getString("profile_id"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setSubjectVideoUrl(resultset.getString("sub_video_url"));
        list.add(bean);
        collegeName = resultset.getString("college_name");
      }
      if (collegeName != null && collegeName.trim().length() > 0) {
        request.setAttribute("collegeName", collegeName);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }

  /**
    *  This function is used to get the Oracle Server date.
    * @param request
    * @return Oracle Server date as String.
    */
  public String getOracleSysDate(HttpServletRequest request) {
    String oracleSysDate = "";
    DataModel dataModel = new DataModel();
    try {
      Vector vector = new Vector();
      vector.clear();
      oracleSysDate = dataModel.getString("wu_util_pkg.getOracleSysDate", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return oracleSysDate;
  }

  /**
    *  This function is used to get the subject name for the given categorycode.
    * @param caregoryCode
    * @return Subject Name as String.
    */
  public String getSubjectHomeDescription(String caregoryCode) {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    String subjectDesc = "";
    try {
      vector.clear();
      vector.add(caregoryCode);
      subjectDesc = dataModel.getString("wu_course_search_pkg.get_subject_desc_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return subjectDesc;
  }

  /**
    *  This function is used to find the Town for the given postcode.
    * @param postCode
    * @return Town as String.
    */
  public String getPostTown(String postCode) {
    String town = "";
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.add(postCode);
      town = new DataModel().getString("wu_profile_pkg.get_posttown_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return town;
  }

  /**
    *  This function is used to find the section name for the given sectionid.
    * @param sectionId
    * @return Section name as String.
    */
  public String getSectionName(String sectionId) {
    String sectionName = "";
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.add(sectionId);
      sectionName = new DataModel().getString("wu_util_pkg.get_section_name_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return sectionName;
  }

  /**
    *  This function is used to List the collegeid for the facebook application user.
    * @param userId
    * @return Collegeids as Collection object.
    */
  public List getFacebookUserCollegeList(String userId) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    ArrayList collegeIdList = new ArrayList();
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.add(userId); // user Id
      resultset = dataModel.getResultSet("wu_facebook_pkg.get_facebook_user_col_list", vector);
      while (resultset.next()) {
        collegeIdList.add(resultset.getString("college_id"));
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return collegeIdList;
  }

  /**
    *  This function is used to load all the related subject catecory from headerid.
    * @param subCategoryId
    * @param location
    * @param request
    * @param qualification
    * @return Subject category list as Collection object.
    * @throws Exception
    */
  public List getReleatedSubCategoryList(String subCategoryId, String location, HttpServletRequest request, String qualification) throws Exception {
    DataModel dataModel = new DataModel();
    Vector parameters = new Vector();
    CommonFunction common = new CommonFunction();
    ResultSet resultset = null;
    SearchBean bean = null;
    List list = new ArrayList();
    String categoryLocation = location != null ? location.toUpperCase() : location;
    String currentCategoryDesc = "";
    parameters.add(subCategoryId);
    parameters.add(common.replacePlus(categoryLocation));
    int index = 0;
    try {
      resultset = dataModel.getResultSet("wu_browse_pkg.get_subcategory_names_fn", parameters);
      while (resultset.next()) {
        bean = new SearchBean();
        bean.setSubjectId(resultset.getString("course_id"));
        bean.setSubjectName(resultset.getString("course_name"));
        bean.setSubjectCode(resultset.getString("category_code"));
        bean.setParentCategoryId(resultset.getString("parent_category_code"));
        bean.setCategoryCodeLength(resultset.getString("cat_code_length"));
        bean.setSeoStudyLevel(resultset.getString("seo_study_level"));
        if (subCategoryId != null && subCategoryId.equalsIgnoreCase(bean.getSubjectId())) {
          request.setAttribute("categoryName", bean.getSubjectName());
          currentCategoryDesc = bean.getSubjectName().trim();
        }
        list.add(bean);
      }
      if (index == 0) {
        request.setAttribute("categoryName", new CommonFunction().getJourneySubjectCategoryName(subCategoryId, "NAME", request));
      }
      if (list != null && list.size() > 0) {
        CommonUtil utilities = new CommonUtil();
        //http://217.33.19.173/degrees/courses/degree-courses/business-analysis-degree-courses-england/m/england/r/8233/page.html
        String studyLevelDesc = common.seoStudyLevelDescription(utilities.toUpperCase(qualification), "courses", "SEARCH_LIST");
        //
        location = location.replaceAll("GREATER ", "");
        location = location.replaceAll("greater ", "");
        String seoText = common.replaceHypen(getCommonCategoryName((common.getJourneySubjectCategoryName(subCategoryId, "CODE", request)), utilities.toUpperCase(qualification))) + studyLevelDesc + "-" + common.replaceHypen(common.replacePlus(location.replaceAll(",", "").replaceAll("\\.", "")));
        String replaceCategoryText = seoText != null && seoText.indexOf("courses-") >= 0 ? "##CATNAME##-" + "##SEOSTUDYLEVEL##" + seoText.substring(seoText.indexOf("courses-")) : seoText;
        String categoryUrl = GlobalConstants.SEO_PATH_COURSESEARCH_PAGE + studyLevelDesc + "/" + replaceCategoryText + "/" + qualification + "/" + (location != null && location.length() > 0 ? location.replaceAll(" ", "+") : "0") + "/r/##CATCODE##/page.html";
        request.setAttribute("categoryUrl", (categoryUrl != null ? utilities.toLowerCase(categoryUrl.trim()) : categoryUrl));
        request.setAttribute("subjectId", subCategoryId);
        request.setAttribute("categoryCode", subCategoryId);
        String studyText = (studyLevelDesc != null ? studyLevelDesc.replaceAll("-courses", "") : "");
        //--(3)--
        currentCategoryDesc = common.replaceHypen(common.replaceURL(currentCategoryDesc));
        String locationUrl = GlobalConstants.SEO_PATH_COURSEJOUNEY_PAGE + studyText + "-list/" + currentCategoryDesc + "-" + studyText + "-courses-UK/qualification/" + utilities.toUpperCase(qualification) + "/search_category/" + subCategoryId + "/loc.html";
        //--(2)--
        //        String categoryName = getBrBrowseCategoryDescription(subCategoryId);
        //        String locationUrl = GlobalConstants.SEO_PATH_COURSEJOUNEY_PAGE + studyText + "-list/" + categoryName + "-" + studyText + "-courses-UK/qualification/" + utilities.toUpperCase(qualification) + "/search_category/" + subCategoryId + "/loc.html";
        //--(1)--
        //String categoryName = request.getAttribute("categoryName") != null ? request.getAttribute("categoryName").toString() : "";
        //        categoryName = categoryName.replaceAll("Fda","FdA");
        //        categoryName = categoryName.replaceAll("Fdsc","FdSc");
        //        categoryName = categoryName.replaceAll("Fdeng","FdEng");
        //        categoryName = common.replaceHypen(common.replaceURL(categoryName));        
        //        String locationUrl = GlobalConstants.SEO_PATH_COURSEJOUNEY_PAGE + studyText + "-list/" + categoryName + "-" + studyText + "-courses-UK/qualification/" + utilities.toUpperCase(qualification) + "/search_category/" + subCategoryId + "/loc.html";
        request.setAttribute("locationUrl", locationUrl);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }

  /**
    *  This function is used to find the browsetypeid for the given input string.
    * @param browseId
    * @return browsetypeid as String.
    */
  public String getBrowseTypeId(String browseId) {
    String browseTypeId = "";
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.add(browseId);
      browseTypeId = new DataModel().getString("wu_browse_pkg.get_browse_type_id_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return browseTypeId;
  }

  /**
    *  This function is used to load the 10 randomized subjects list.
    * @param studyLevel
    * @return 10 randomize subjects as Collection object.
    */
  public List randomizeSubjectName(String studyLevel) {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    ResultSet resultset = null;
    List subjectlist = new ArrayList();
    vector.clear();
    vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
    vector.add(studyLevel);
    CourseJourneyBean bean = null;
    try {
      resultset = dataModel.getResultSet("wu_browse_pkg.get_randomize_subject_name_fn", vector);
      while (resultset.next()) {
        bean = new CourseJourneyBean();
        bean.setSubjectId(resultset.getString("subject_id"));
        bean.setSubjectName(resultset.getString("subject_name"));
        bean.setStudyLevelText(resultset.getString("study_level"));
        bean.setStudyLevelId(resultset.getString("qualification_code"));
        bean.setCategoryCodeLength(resultset.getString("category_code_length"));
        // To for Subject url pointing to location page 
        /* http://217.33.19.173/degrees/courses/Degree-list/Agricultural-Biology-Degree-courses-UK/qualification/M/search_category/8244/loc.html*/
        String seoStudyLevelDesc = resultset.getString("seo_studylevel_text");
        String subjectName = resultset.getString("subject_name");
        subjectName = (subjectName != null && String.valueOf(subjectName).trim().length() > 0 && String.valueOf(subjectName).indexOf("#") >= 0) ? String.valueOf(subjectName).replaceAll("#", "") : subjectName;
        subjectName = new CommonFunction().replaceHypen(new CommonFunction().replaceURL(subjectName));
        String randomizeURL = GlobalConstants.SEO_PATH_COURSEJOUNEY_PAGE + seoStudyLevelDesc + "list/" + subjectName + "-" + seoStudyLevelDesc + "courses-UK/qualification/" + resultset.getString("qualification_code") + "/search_category/" + resultset.getString("subject_id") + "/loc.html";
        bean.setHyperLinkUrl(randomizeURL);
        subjectlist.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return subjectlist;
  }

  /**
    *  This function is used to load the 10 randomized subjects list based on studylevel.
    * @param studyLevel
    * @param request
    * @return 10 randomized subject list as Collection object.
    */
  public List staticSubjectName(String studyLevel, HttpServletRequest request) {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    ResultSet resultset = null;
    List staticList = new ArrayList();
    vector.clear();
    vector.add(studyLevel);
    CourseJourneyBean bean = null;
    try {
      resultset = dataModel.getResultSet("wu_browse_Pkg.get_static_subject_fn", vector);
      while (resultset.next()) {
        bean = new CourseJourneyBean();
        bean.setSubjectId(resultset.getString("subject_id"));
        bean.setSubjectName(resultset.getString("subject_name"));
        bean.setStudyLevelId(resultset.getString("qualification_code"));
        request.setAttribute("popular_qualification_text", resultset.getString("seo_studylevel_text"));
        request.setAttribute("popular_qualification_code", resultset.getString("qualification_code"));
        // To for Subject url pointing to location page 
        /* http://217.33.19.173/degrees/courses/Degree-list/Agricultural-Biology-Degree-courses-UK/qualification/M/search_category/8244/loc.html*/
        String seoStudyLevelDesc = resultset.getString("seo_studylevel_text");
        bean.setStudyLevelDesc(seoStudyLevelDesc);
        String subjectName = resultset.getString("subject_name");
        subjectName = (subjectName != null && String.valueOf(subjectName).trim().length() > 0 && String.valueOf(subjectName).indexOf("#") >= 0) ? String.valueOf(subjectName).replaceAll("#", "") : subjectName;
        subjectName = subjectName != null && !subjectName.equalsIgnoreCase("null") && subjectName.trim().length() > 0 ? new CommonFunction().replaceHypen(new CommonFunction().replaceURL(subjectName)) : "";
        String randomizeURL = GlobalConstants.SEO_PATH_COURSEJOUNEY_PAGE + seoStudyLevelDesc + "list/" + subjectName + "-" + seoStudyLevelDesc + "courses-UK/qualification/" + resultset.getString("qualification_code") + "/search_category/" + resultset.getString("subject_id") + "/loc.html";
        bean.setHyperLinkUrl(randomizeURL);
        staticList.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return staticList;
  }

  /**
    *  This function is used to load the search header data for the given headerid.
    * @param headerId
    * @return Search header data as Collection object.
    */
  public Map getSearchHeaderData(String headerId) {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    ResultSet resultset = null;
    vector.clear();
    vector.add(headerId);
    Map hashmap = new HashMap();
    try {
      resultset = dataModel.getResultSet("Wu_util_Pkg.get_search_header_data_fn", vector);
      while (resultset.next()) {
        hashmap.put("collegename", resultset.getString("college_name"));
        hashmap.put("qualification", resultset.getString("qualification"));
        hashmap.put("collegeid", resultset.getString("college_id"));
        hashmap.put("phrase_search", resultset.getString("phrase_search"));
        hashmap.put("search_category", resultset.getString("search_category"));
        hashmap.put("search_postcode", resultset.getString("search_postcode"));
        hashmap.put("county_id", resultset.getString("county_id"));
        hashmap.put("search_study_mode", resultset.getString("search_study_mode"));
        hashmap.put("town_city", resultset.getString("town_city"));
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return hashmap;
  }

  /**
    *  This function is used to check the given course id is deleted or not.
    * @param courseId
    * @return course deleted status as String (YES?NO).
    */
  public String checkCourseDeleted(String courseId) {
    DataModel dataModel = new DataModel();
    String courseStatus = "NO";
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.addElement(courseId); // courseId   
      courseStatus = dataModel.executeUpdate("wu_util_pkg.check_course_delete_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return courseStatus;
  }

  /**
    *  This function is used to remove the session scope variables.
    * @param session
    * @param request
    * @return none.
    */
  public void removeSessionData(HttpSession session, HttpServletRequest request, HttpServletResponse response) {
    if (session.getAttribute("backPageURL") != null) {
      session.removeAttribute("backPageURL");
    }
    if (session.getAttribute("prospectus_redirect_url") != null) {
      session.removeAttribute("prospectus_redirect_url");
    }
    if (session.getAttribute("back_to_review_result_page") != null) {
      session.removeAttribute("back_to_review_result_page");
    }
    new SessionData().removeData(request, response, "userIdAfterSubmit");
  }

  /**
    *  This function is used to get the common category name for the given category code from br_browse_category table.
    * @param categoryId
    * @param studyLevelId
    * @return category name as String.
    */
  public String getCommonCategoryName(String categoryId, String studyLevelId) {
    DataModel dataModel = new DataModel();
    String categoryName = "";
    try {
      Vector parameters = new Vector();
      parameters.addElement(new CommonUtil().toUpperCase(categoryId)); // cagtegoryId   
      parameters.addElement(new CommonUtil().toUpperCase(studyLevelId)); // studyLevelId   
      categoryName = dataModel.executeUpdate("wu_browse_pkg.get_common_category_name_fn", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return categoryName;
  }

  /**
    *  This function is used to get the category description to the given category_id code from br_browse_category table. with out any condition check
    * @param categoryId
    * @author Mohamed Syed
    * @return category name as String.
    */
  public String getBrBrowseCategoryDescription(String categoryId) {
    DataModel dataModel = new DataModel();
    String categoryName = "";
    try {
      Vector parameters = new Vector();
      parameters.addElement(categoryId); // cagtegoryId   
      categoryName = dataModel.executeUpdate("WU_BROWSE_PKG.GET_BR_BROWSE_CAT_DESC_FN", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return categoryName;
  }

  /**
    * used to get the category_desc to the given category_code from s_categories table.
    * @param categoryCode
    * @return category desc as String.
    * @author Mohamed Syed
    * @since 2010-Nov-10 --> wu_2.1.3
    */
  public String getCategoryDescFromSCategories(String categoryCode) {
    if (categoryCode == null || categoryCode.trim().length() == 0) {
      return "";
    }
    DataModel dataModel = new DataModel();
    String categoryDesc = "";
    Vector parameters = new Vector();
    parameters.add(categoryCode.toUpperCase()); // cagtegoryId  
    try {
      categoryDesc = dataModel.getString("wu_browse_pkg.get_s_category_desc_fn", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel = null;
      parameters.clear();
      parameters = null;
    }
    return categoryDesc;
  } //getCategoryDescFromSCategories()

  /**
     * used to get the category_desc to the given category_code
     * @param categoryCode
     * @return category desc as String.
     */
  public String getCategoryDescFromBrowseCategories(String categoryCode, String qualificationCode) {
    if (categoryCode == null || categoryCode.trim().length() == 0) {
      return "";
    }
    DataModel dataModel = new DataModel();
    String categoryDesc = "";
    Vector parameters = new Vector();
    parameters.add(categoryCode.toUpperCase()); // cagtegoryId  
    parameters.add(qualificationCode.toUpperCase()); //   
    try {
      categoryDesc = dataModel.getString("WU_BROWSE_PKG.GET_BROWSE_CATEGORY_DESC_FN", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel = null;
      parameters.clear();
      parameters = null;
    }
    return categoryDesc;
  } //getCategoryDescFromSCategories()

  /**
    *  This function is used to find the category name for the given categortid and studylevel.
    * @param categoryId
    * @param studyLevelId
    * @return category name as String.
    */
  public String getCategoryCodeNameFn(String categoryId, String studyLevelId) {
    DataModel dataModel = new DataModel();
    String categoryName = "";
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.addElement(new CommonUtil().toUpperCase(categoryId)); // cagtegoryId   
      vector.addElement(new CommonUtil().toUpperCase(studyLevelId)); // studyLevelId   
      categoryName = dataModel.executeUpdate("wu_browse_pkg.get_category_code_name_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
      ;
    }
    return categoryName;
  }

  /**
    *  This function is used to check the given Email address already present or not(Existing user or New User).
    * @param email
    * @param request
    * @return Userid as String.
    */
  public String checkEmailExist(String email, HttpServletRequest request) {
    String userId = "";
    /*Vector vector = new Vector();
    vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
    vector.add(email);    
    try {
      userId = new DataModel().getString("wu_util_pkg.check_email_exists", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }*/
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
    RegistrationBean registrationBean = new RegistrationBean();       
    registrationBean.setAffilateId(GlobalConstants.WHATUNI_AFFILATE_ID);
    registrationBean.setEmailAddress(email);
    Map checkEmailExistMap = commonBusiness.checkEmailExist(registrationBean);
    if(checkEmailExistMap != null && !checkEmailExistMap.isEmpty()){
      userId = (String)checkEmailExistMap.get("p_user_id");  
    }    
    return userId;
  }

  /**
    *  This function is used to get emailId of given userId.
    * @param userId
    * @return email as String.
    */
  public String getUserEmailId(String userId) {
    Vector vector = new Vector();
    vector.add(userId);
    String email = "";
    try {
      email = new DataModel().getString("HOT_WUNI.WU_UTIL_PKG.GET_USER_EMAIL_FN", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return email;
  }

  /**
    *  This function is used to load the pre login user basket based on the cookie id (cookie id =  basketid)
    * @param request
    * @param userId
    * @param response
    * @return none.
    */
  public void updateBasketContentLogin(HttpServletRequest request, String userId, HttpServletResponse response) {
    request.getSession().removeAttribute("userBasketPodList");
    Vector vector = new Vector();
    String cBasketId = new CommonFunction().checkCookieStatus(request);
    vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
    vector.add(userId);
    vector.add(cBasketId);
    try {
      String oldBasketId = new DataModel().getString("wu_basket_pkg.update_pre_login_user_fn", vector);
      if (oldBasketId != null && !oldBasketId.equals("")) {
        Cookie cookie = CookieManager.createCookie(request,"basketId", oldBasketId);
        response.addCookie(cookie);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
    *  This function is used to update the user basket content when user registration.
    * @param request
    * @param userId
    * @return none.
    */
  public void updateBasketContent(HttpServletRequest request, String userId) {
    request.getSession().removeAttribute("userBasketPodList");
    Vector vector = new Vector();
    String cBasketId = new CommonFunction().checkCookieStatus(request);
    vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
    vector.add(userId);
    vector.add(cBasketId);
    try {
      String updateBasketStatus = new DataModel().getString("wu_basket_pkg.update_pre_register_user_fn", vector);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
    *  This function is used to maintain the last 10 user seacrh in Database.
    * @param pageUrl
    * @param searchName
    * @param userId
    * @param basketId
    * @param response
    * @param userAgent
    * @return Basket id as String.
    */
  public String setMySearches(String pageUrl, String searchName, String userId, String basketId, HttpServletResponse response, String userAgent) {
    DataModel dataModel = new DataModel();
    String returnBasketId = "";
    Vector parameters = new Vector();
    try {
      parameters.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      parameters.add(userId);
      parameters.add(basketId);
      parameters.add(searchName);
      parameters.add(pageUrl);
      parameters.add(userAgent);
      returnBasketId = dataModel.getString("WU_BASKET_PKG.USER_SEARCH_BASKET_FN", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    //Cookie cookie = new CookieManager().createCookie("basketId", returnBasketId);
    //response.addCookie(cookie);
    return returnBasketId;
  }

  /**
    *  This function is used to load the user searches for the given basketid.
    * @param basketId
    * @param request
    * @return Search URLs as Collection object.
    */
  public ArrayList getMySearchesList(String basketId, HttpServletRequest request) {
    String userId = new SessionData().getData(request, "y");
    Vector vector = new Vector();
    DataModel dataModel = new DataModel();
    ResultSet resultSet = null;
    BasketBean bean = null;
    ArrayList searchList = new ArrayList();
    String userIdInput = "";
    String basketIdInput = "";
    if (userId != null && !userId.equals("0") && !userId.equalsIgnoreCase("null") && userId.trim().length() > 0) {
      userIdInput = userId;
      basketIdInput = "";
    } else {
      basketIdInput = basketId;
      userIdInput = "";
    }
    try {
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(userIdInput);
      vector.add(basketIdInput);
      if ((userIdInput != null && userIdInput.trim().length() > 0) || (basketIdInput != null && basketIdInput.trim().length() > 0)) {
        resultSet = dataModel.getResultSet("wu_basket_pkg.get_user_search_basket_fn", vector);
        while (resultSet.next()) {
          bean = new BasketBean();
          bean.setSearchName(resultSet.getString("search_name"));
          bean.setPageUrl(resultSet.getString("search_url"));
          searchList.add(bean);
        }
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return searchList;
  }

  /**
    *  This function is used to load all the college related information for the given collegeid.
    * @param searhCollegeId
    * @param request
    * @param param_keyword
    * @param param_studylevel
    * @param param_cat_code
    * @return College related information as Collection object.
    */
  public ArrayList getCollegeInformation(String searhCollegeId, HttpServletRequest request, String param_keyword, String param_studylevel, String param_cat_code) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    SearchResultBean bean = null;
    ArrayList collegeInfoList = new ArrayList();
    try {
      String userId = new SessionData().getData(request, "y");
      //added for 24th March Release 
      userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0 ? userId : "";
      String basketId = (userId != null && (userId.equals("0") || userId.trim().length() == 0)) ? new CommonFunction().checkCookieStatus(request) : "";
      basketId = (userId != null && userId.trim().length() > 0) ? "" : basketId;
      userId = (basketId != null && basketId.trim().length() > 0) ? "" : userId;
      if ((userId != null && !userId.equals("0") && userId.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
        userId = "0";
        basketId = "";
      }
      String collegeOpendayUrl = "";
      Vector vector = new Vector();
      vector.clear();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(searhCollegeId);
      vector.add(param_keyword);
      vector.add(param_studylevel);
      vector.add(param_cat_code);
      vector.add(userId);
      vector.add(basketId);
      resultset = dataModel.getResultSet("wu_gmap_pkg.get_college_info_fn", vector);
      while (resultset.next()) {
        bean = new SearchResultBean();
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
        bean.setOverAllRating(resultset.getString("overall_rating"));
        bean.setTotalReviews(resultset.getString("review_count"));
        bean.setCollegeLocation(resultset.getString("college_location"));
        if (resultset.getString("scholarship_count") != null && !resultset.getString("scholarship_count").equals("0")) {
          request.setAttribute("showFundingTab", "TRUE");
        }
        request.setAttribute("collegeName", bean.getCollegeName());
        request.setAttribute("collegeNameDisplay", bean.getCollegeNameDisplay());
        request.setAttribute("opendaycount", resultset.getString("openday_count"));
        request.setAttribute("collegeLocation", resultset.getString("college_location"));
        request.setAttribute("next_col_openday", resultset.getString("next_col_openday"));
        request.setAttribute("PROFILE_OVERVIEW_FLAG", resultset.getString("profile_overview_flag"));
        collegeOpendayUrl = resultset.getString("open_day_url");
        if (collegeOpendayUrl != null && collegeOpendayUrl.length() > 0) {
          if(collegeOpendayUrl.indexOf("whatuni.com") >= 0){ //Added WHATUNI_SCHEME_NAME by Indumathi Mar-29-16
            collegeOpendayUrl = (collegeOpendayUrl.indexOf("https://") >= 0) ? collegeOpendayUrl.replace("https://", GlobalConstants.WHATUNI_SCHEME_NAME) : (collegeOpendayUrl.indexOf("http://") >= 0) ? collegeOpendayUrl.replace("http://", GlobalConstants.WHATUNI_SCHEME_NAME) : GlobalConstants.WHATUNI_SCHEME_NAME + collegeOpendayUrl; 
          } else {
            collegeOpendayUrl = (collegeOpendayUrl.indexOf("http://") >= 0 || collegeOpendayUrl.indexOf("https://") >= 0) ? collegeOpendayUrl : "http://" + collegeOpendayUrl;
          }
          request.setAttribute("OPEN_DAY_URL", collegeOpendayUrl);
        }
        collegeInfoList.add(bean);
      }
      request.setAttribute("overallCollegeId", searhCollegeId);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return collegeInfoList;
  }

  /**
    *  This function is used to load the user uploaded college images for the given collegeid.
    * @param collegeId
    * @param keyword
    * @param pageNo
    * @param displayRecord
    * @return College image list as Collection object.
    */
  public List getUserCollegeImageGallery(String collegeId, String keyword, String pageNo, String displayRecord, String recordStatus) {
    ResultSet resultSet = null;
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    UniPhotoGalleryBean bean = null;
    ArrayList userImageList = new ArrayList();
    try {
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(collegeId);
      vector.add(keyword);
      vector.add(pageNo);
      vector.add(displayRecord);
      vector.add(recordStatus);
      resultSet = dataModel.getResultSet("WU_PHOTO_GALLERY_PKG.get_col_user_image_fn", vector);
      while (resultSet.next()) {
        bean = new UniPhotoGalleryBean();
        bean.setImagename(resultSet.getString("image_name"));
        // added for 17th November 2009 Release 
        if (resultSet.getString("image_name") != null && resultSet.getString("image_name").trim().length() > 43) {
          bean.setShortImageName(resultSet.getString("image_name").substring(0, 43) + "...");
        } else {
          bean.setShortImageName(resultSet.getString("image_name"));
        }
        // end of the code added for 17th November 2009 Release 
        bean.setTags(resultSet.getString("tags"));
        if (resultSet.getString("tags") != null && resultSet.getString("tags").trim().length() > 20) {
          bean.setShortTag(resultSet.getString("tags").substring(0, 20) + "...");
        } else {
          bean.setShortTag(resultSet.getString("tags"));
        }
        bean.setAuthor(resultSet.getString("author"));
        bean.setImagePath(resultSet.getString("image_path"));
        bean.setImageHeight(resultSet.getString("image_height") != null ? Integer.parseInt(resultSet.getString("image_height")) : 0);
        bean.setImageWidth(resultSet.getString("image_width") != null ? Integer.parseInt(resultSet.getString("image_width")) : 0);
        if (resultSet.getString("image_path") != null && resultSet.getString("image_path").lastIndexOf(".") > 0) {
          int lastindex = resultSet.getString("image_path").lastIndexOf(".");
          String thumbimage = resultSet.getString("image_path");
          thumbimage = thumbimage.substring(0, lastindex) + "_thumb" + thumbimage.substring(lastindex);
          bean.setImageThumbPath(thumbimage);
        }
        bean.setImageId(resultSet.getString("image_id"));
        userImageList.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return userImageList;
  }

  /**
    *  This function is used to load the user uploaded college image count for the given collegeid.
    * @param collegeId
    * @param keyword
    * @return College image count as String.
    */
  public String getUserCollegeImageCount(String collegeId, String keyword, String recordStatus) {
    String recordCount = "0";
    Vector vector = new Vector();
    try {
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(collegeId);
      vector.add(keyword);
      vector.add(recordStatus);
      recordCount = new DataModel().getString("wu_photo_gallery_pkg.get_col_user_image_cnt_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return recordCount;
  }

  /**
    *  This function is used to check the prospectus puchase flag for the given collegeid.
    * @param collegeId
    * @param request
    * @return none.
    */
  public void checkProspectusPurchaseFlag(String collegeId, HttpServletRequest request) {
    String purchaseflag = "";
    try {
      DataModel dataModel = new DataModel();
      Vector vector = new Vector();
      vector.clear();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(collegeId);
      vector.add("PROSPECTUS");
      purchaseflag = dataModel.getString("wu_util_pkg.check_purchased_flag_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    // added for 30 June 2009 Release to avoid the popup window block in the browser //
    String prospectus_pur_flag[] = purchaseflag.split(GlobalConstants.PROSPECTUS_SPLIT_CHAR);
    request.setAttribute("prospectus_purchase_flag", prospectus_pur_flag[0]);
    if (prospectus_pur_flag != null && prospectus_pur_flag.length > 1) {
      request.setAttribute("prospectus_external_url", prospectus_pur_flag[1]);
    } else {
      request.setAttribute("prospectus_external_url", "");
    }
  }

  /**
    *  This function is used to load the latitude & langtitude value for the given collegeid.
    * @param collegeId
    * @return latitude & Langtitude list as Collection object.
    */
  public ArrayList getCollegeLatLangValue(String collegeId) {
    ArrayList list = new ArrayList();
    ResultSet resultset = null;
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    AdminOpenDaysBean bean = null;
    try {
      vector.add(collegeId);
      resultset = dataModel.getResultSet("wu_admin_pkg.get_street_view_college_fn", vector);
      while (resultset != null && resultset.next()) {
        bean = new AdminOpenDaysBean();
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setPostCode(resultset.getString("postcode"));
        bean.setShortNotes(resultset.getString("description"));
        bean.setLatitudeValue(resultset.getString("latitude"));
        bean.setLongtitudeValue(resultset.getString("longitude"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }

  /**
    *  This function is used to update the userid after successful registration.
    * @param request
    * @return Update status as String.
    */
  public String preLoginUpdateStatusCode(HttpServletRequest request) {
    HttpSession session = request.getSession();
   // DataModel dataModel = new DataModel();
    String value = "";
    /*Commented by Prabha on 28_jun_2016
     Vector vector = new Vector();
    vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
    vector.addElement(String.valueOf(session.getAttribute("reviewid")));
    vector.add(new SessionData().getData(request, "y"));
    try {
      value = dataModel.executeUpdate("wu_reviews_pkg.upd_prelogin_status_code_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }*/
    //Below code is added by Prabha on 28_Jun_2016
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
    RegistrationBean registrationBean = new RegistrationBean();     
    registrationBean.setAffilateId(GlobalConstants.WHATUNI_AFFILATE_ID);
    registrationBean.setReviewId(String.valueOf(session.getAttribute("reviewid")));
    registrationBean.setSessionId(new SessionData().getData(request, "y"));
    commonBusiness.preLoginUpdateStatusCode(registrationBean);
    //End of the code
    // new CommonFunction().sendReviewerMail("", "", request, session, "WU_REVIEW_SUBMISSION"); // commented for 01 December 2009 release  - for sending the email to user from backoffice when activating the review 
    FriendEmailBean friendemailbean = (FriendEmailBean)session.getAttribute("friendemailbean");
    session.setAttribute("friendemailbean", new FriendEmailBean());
    return value;
  }

  /**
    *  This function is used to track the external prosrectus link.
    * @param request
    * @param clientIP
    * @param userAgent
    * @param collegeId
    * @param externalURL
    * @param requestUrl
    * @param refererUrl
    * @return jslogid as String.
    */
  public String trackExternalProspectus(HttpServletRequest request, String clientIP, String userAgent, String collegeId, String externalURL, String requestUrl, String refererUrl, HttpSession session) {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    String jsLogId = "";
    String userUcasScore = StringUtils.isNotBlank((String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE)) ? (String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE) : null; //Added for tariff-logging-ucas-point by Sri Sankari on wu_20200915 release
    String yearOfEntryForDSession = StringUtils.isNotBlank((String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING)) ? (String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING) : null; //Added for getting YOE value in session to log in d_session_log stats by Sujitha V on 2020_NOV_10 rel
    vector.add(new SessionData().getData(request, "x"));
    vector.add(new SessionData().getData(request, "y"));
    vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
    vector.add("Y"); //p_java
    vector.add(clientIP);
    vector.add(userAgent);
    vector.add(collegeId);
    vector.add(externalURL);
    vector.add(requestUrl);
    vector.add(refererUrl);
    vector.add(new SessionData().getData(request, "userTrackId"));
	vector.add(userUcasScore);
	vector.add(yearOfEntryForDSession); //P_YEAR_OF_ENTRY
    try {
      jsLogId = dataModel.executeUpdate("wu_stats_log_pkg.track_external_wuni_prospectus", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return jsLogId;
  }

  /**
    *  This function is used to load the college list for the give alphabetic character.
    * @param alphaCharacter
    * @return Collegelist as Collection object.
    */
  public ArrayList getProspectusDropdownUni(String alphaCharacter) {
    ArrayList list = new ArrayList();
    ResultSet resultset = null;
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    SearchBean bean = null;
    try {
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(alphaCharacter);
      resultset = dataModel.getResultSet("wu_college_info_pkg.get_open_day_college_list_fn", vector);
      while (resultset != null && resultset.next()) {
        bean = new SearchBean();
        bean.setOptionId(resultset.getString("college_id"));
        bean.setOptionText(resultset.getString("college_name"));
        bean.setOptionSeqId(resultset.getString("college_location"));
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }

  /**
    *  This function is used to set the user video hit count to databse.
    * @param videoId
    * @param videoType
    * @return User video hit count as String.
    */
  public String setHitCounter(String videoId, String videoType) {
    DataModel dataModel = new DataModel();
    String hitValue = "";
    try {
      Vector hitVector = new Vector();
      hitVector.clear();
      hitVector.add(GlobalConstants.WHATUNI_AFFILATE_ID); // affiliate_id
      hitVector.add(videoId); // videoId         video id 
      hitVector.add(videoType); // videoType      'V' - video
      hitValue = dataModel.getString("wu_profile_pkg.populate_comm_counter_fn", hitVector);
    } catch (Exception hitException) {
      hitException.printStackTrace();
    } finally {
    }
    return hitValue;
  }

  /**
    *  This function is used to load the interactive pod information for the given collegeid.
    * @param searhCollegeId
    * @param request
    * @return  Interactive pod information as Collection object.
    */
  public List getInteractivePodInfo(String searhCollegeId, HttpServletRequest request) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    SearchResultBean bean = null;
    List list = new ArrayList();
    try {
      Vector vector = new Vector();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(searhCollegeId != null ? searhCollegeId.trim() : searhCollegeId); //10.03.2009 release
      String userId = new SessionData().getData(request, "y");
      //added for 24th March Release 
      userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0 ? userId : "";
      String basketId = (userId != null && (userId.equals("0") || userId.trim().length() == 0)) ? new CommonFunction().checkCookieStatus(request) : "";
      basketId = (userId != null && userId.trim().length() > 0) ? "" : basketId;
      userId = (basketId != null && basketId.trim().length() > 0) ? "" : userId;
      if ((userId != null && !userId.equals("0") && userId.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
        userId = "0";
        basketId = "";
      }
      String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
      if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
        cpeQualificationNetworkId = "2";
      }
      // end // 
      vector.add(userId);
      vector.add(basketId); // added for 24th March Release     
      vector.add(cpeQualificationNetworkId);
      resultset = dataModel.getResultSet("wu_gmap_pkg.get_interactive_pod_info", vector);
      while (resultset.next()) {
        bean = new SearchResultBean();
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
        bean.setWebSite(resultset.getString("college_site"));
        bean.setCollegeLogo(resultset.getString("college_logo"));
        // added for 15th September 2009 release 
        bean.setVideoThumbUrl("");
        bean.setLogoId(resultset.getString("image_id"));
        bean.setLogoType("");
        if (!GenericValidator.isBlankOrNull(resultset.getString("college_logo")) && (resultset.getString("college_logo").indexOf(".flv") >= 0 || resultset.getString("college_logo").indexOf(".mp4") > -1)) {
          bean.setLogoType("video");
          bean.setVideoThumbUrl(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(resultset.getString("image_id"), "1"), 0)); //Modified by Amir 24_FEB_15 towards videoThumbnail issue. 
        } else {
          if (resultset.getString("college_logo") != null && resultset.getString("college_logo").trim().length() > 0) {
            bean.setLogoType("image");
          }
        }
        //end of code added for 15th September 2009 release 
        bean.setCollegeProfileFlag(resultset.getString("college_profile"));
        bean.setEmailPaidFlag(resultset.getString("email_pur_flag"));
        bean.setScholarshipCount(resultset.getString("scholarship_count"));
        // added for 30 June 2009 Release to avoid the popup window block in the browser //
        String prospectus_pur_flag[] = resultset.getString("prospectus_pur_flag").split(GlobalConstants.PROSPECTUS_SPLIT_CHAR);
        bean.setProspectusPaidFlag(prospectus_pur_flag[0]);
        if (prospectus_pur_flag != null && prospectus_pur_flag.length > 1) {
          bean.setProspectusExternalUrl(prospectus_pur_flag[1]);
          request.setAttribute("PROSPECTUS_EXTERNAL_URL", prospectus_pur_flag[1]);
        }
        /////////////////////////////////////////////////////////////////////////
        bean.setWebsitePaidFlag(resultset.getString("website_pur_flag"));
        bean.setSubjectProfileCount(resultset.getString("subject_profile_count"));
        bean.setAllCourseCount(resultset.getString("all_course_count"));
        bean.setPgCourseCount(resultset.getString("pg_course_count"));
        bean.setDegreeCourseCount(resultset.getString("degree_course_count"));
        bean.setAccessFoundationCount(resultset.getString("acc_found_course_cnt"));
        bean.setFoundationCount(resultset.getString("foundation_course_cnt"));
        bean.setHncHndCount(resultset.getString("hnd_hnc_course_cnt"));
        request.setAttribute("collegeName", resultset.getString("college_name"));
        bean.setCollegeInBasket(resultset.getString("college_in_basket"));
        bean.setOverviewShortDesc(resultset.getString("profile_short_text"));
        request.setAttribute("OVER_VIEW_PURCHASE_FLAG", resultset.getString("college_profile"));
        request.setAttribute("WEBSITE_PURCHASE_FLAG", resultset.getString("website_pur_flag"));
        request.setAttribute("PROSPECTUS_PURCHASE_FLAG", prospectus_pur_flag[0]);
        request.setAttribute("SUBJECT_PURCHASE_COUNT", resultset.getString("subject_profile_count"));
        request.setAttribute("EMAIL_PURCHASE_FLAG", resultset.getString("email_pur_flag"));
        request.setAttribute("COLLEGE_IN_BASKET", resultset.getString("college_in_basket"));
        request.setAttribute("COLLEGEID_IN_INTERACTION", (resultset.getString("college_id") != null ? resultset.getString("college_id").trim() : resultset.getString("college_id")));
        bean.setOverAllRating(resultset.getString("overall_rating"));
        bean.setReviewCount(resultset.getString("review_count"));
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    if (list != null && list.size() > 0) {
      request.setAttribute("complete_college_interaction", list);
    }
    return list;
  }

  /**
    *  This function is used to get the college website for a given collegeid.
    * @param collegeId
    * @return College website as String.
    */
  public String getCollegeWebsite(String collegeId) {
    Vector vector = new Vector();
    String collegeWebsite = "";
    try {
      vector.add(collegeId);
      collegeWebsite = new DataModel().getString("wu_gmap_pkg.get_college_website_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
    }
    return collegeWebsite;
  }

  /**
    *  This function to get the SHTML entry content as CLOB content
    * @param htmlID
    * @return String as SHTML Data
    */
  public String getShtmlData(String shtmlId) {
    CLOB shtlContent = null;
    Connection connection = null;
    OracleCallableStatement oraclestmt = null;
    try {
      DataSource datasource = null;
      try {
        InitialContext ic = new InitialContext();
        datasource = (DataSource)ic.lookup("whatuni");
      } catch (Exception exception) {
        exception.printStackTrace();
      }
      connection = datasource.getConnection();
      String query = "{ ?= call wu_email_pkg.get_shtml_content_fn(?,?) }";
      oraclestmt = (OracleCallableStatement)connection.prepareCall(query);
      oraclestmt.registerOutParameter(1, OracleTypes.CLOB);
      oraclestmt.setString(2, GlobalConstants.WHATUNI_AFFILATE_ID);
      oraclestmt.setString(3, shtmlId);
      oraclestmt.execute();
      shtlContent = oraclestmt.getCLOB(1);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      try {
        if (oraclestmt != null) {
          oraclestmt.close();
        }
        if (connection != null) {
          connection.close();
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      }
    }
    return convertCLOBtoStringFn(shtlContent);
  }

  private String convertCLOBtoStringFn(CLOB data) {
    StringBuffer strOut = new StringBuffer();
    if (data != null) {
      String aux = "";
      try {
        BufferedReader br = new BufferedReader(data.getCharacterStream());
        while ((aux = br.readLine()) != null)
          strOut.append(aux);
      } catch (Exception exception) {
        exception.printStackTrace();
      }
    }
    return strOut.toString();
  }

  /**
   *  function to load the thumbnail for the given FLV file
   * @param String videoURL, String videoId
   * @return Thumbpath as String
   */
  public String getVideoThumbPath(String videoURL, String videoId) {
    String thumbUrl = "";
    String thumbNo = "2"; //new CommonFunction().getWUSysVarValue("WU_PROFILE_THUMBNAIL_NO"); //commented for 15th dec release. and hard code the value as 2
    if (!GenericValidator.isBlankOrNull(videoURL)) {
      thumbUrl = videoURL.indexOf(".mp4") > -1 ? videoURL.replaceAll(".mp4", "_tmb/000" + thumbNo + ".jpg") : videoURL.replaceAll(".flv", "_tmb/000" + thumbNo + ".jpg");
    }
    return thumbUrl;
  }

  public ArrayList loadTickerVideo() {
    Vector vector = new Vector();
    ResultSet resultSet = null;
    DataModel dataModel = new DataModel();
    ArrayList tickerVideoList = new ArrayList();
    try {
      resultSet = dataModel.getResultSet("wu_homepage_pkg.get_video_ticker_fn", vector);
      while (resultSet.next()) {
        String limeLightPath = GlobalConstants.LIMELIGHT_VIDEO_PATH;
        VideoReviewListBean bean = new VideoReviewListBean();
        bean.setVideoReviewId(resultSet.getString("review_id"));
        bean.setCollegeId(resultSet.getString("college_id"));
        bean.setCollegeName(resultSet.getString("college_name"));
        bean.setCollegeNameDisplay(resultSet.getString("college_name_display"));
        bean.setVideoType(resultSet.getString("video_type"));
        if (bean.getVideoType() != null && bean.getVideoType().equalsIgnoreCase("V")) {
          bean.setThumbnailUrl(resultSet.getString("thumbnail_assoc_text"));
        } else {
          bean.setVideoUrl(resultSet.getString("video_url"));
          bean.setThumbnailUrl(resultSet.getString("thumbnail_path"));
          //bean.setThumbnailUrl(new GlobalFunction().videoThumbnailFormatChange(new CommonUtil().getImgPath("/commimg/whatuni/", 1) + resultSet.getString("video_assoc_text"), "1"));
          bean.setThumbnailUrl(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnailReviewAppserver(bean, "2"), 1));
        }
        tickerVideoList.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return tickerVideoList;
  }

  public ArrayList loadHomeVideo() {
    Vector vector = new Vector();
    ResultSet resultSet = null;
    DataModel dataModel = new DataModel();
    ArrayList homeVideoList = new ArrayList();
    try {
      resultSet = dataModel.getResultSet("wu_util_pkg.get_uni_of_the_week_fn", vector);
      while (resultSet.next()) {
        String limeLightPath = GlobalConstants.LIMELIGHT_VIDEO_PATH;
        VideoReviewListBean bean = new VideoReviewListBean();
        bean.setCollegeId(resultSet.getString("college_id"));
        bean.setCollegeName(resultSet.getString("college_name"));
        bean.setCollegeNameDisplay(resultSet.getString("college_name_display"));
        //bean.setCollegeProfileFlag(resultSet.getString("college_profile"));
        bean.setCollegeLogo(resultSet.getString("college_logo"));
        //bean.setVideoReviewId(resultSet.getString("review_id"));
        //bean.setVideoReviewTitle(resultSet.getString("review_title"));
        //bean.setMyhcProfileId(resultSet.getString("myhc_profile_id"));
        //bean.setProfileType(resultSet.getString("myhc_profile_type"));
        /*String videoType = resultSet.getString("video_type");
        String videoThumbNumber = resultSet.getString("video_thumb_nos"); //adeed for 28th July 2009 Release
        if (videoType != null && videoType.equalsIgnoreCase("V")) {
          bean.setThumbnailUrl(resultSet.getString("thumbnail_assoc_text"));
        } else {
          bean.setVideoUrl(limeLightPath + resultSet.getString("video_assoc_text"));
          if (videoThumbNumber != null) {
            bean.setThumbnailUrl(new GlobalFunction().videoThumbnailFormatChange(limeLightPath + resultSet.getString("video_assoc_text"), videoThumbNumber));
          }
        }*/
        bean.setMediaId(resultSet.getString("media_id"));
        bean.setThumbnailUrl(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(bean.getMediaId(), "3"), 1));
        homeVideoList.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return homeVideoList;
  }

  /**
    *  This function is used to check the given is valid UCAS Code or not.
    * @param ucasCode
    * @return ucasCode as String.
    */
  public String checkValidUCASCode(String ucasCode) {
    Vector vector = new Vector();
    String ucasValidFlg = "";
    try {
      vector.add(ucasCode);
      ucasValidFlg = new DataModel().getString("wu_browse_pkg.chk_ucas_code_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
    }
    return ucasValidFlg;
  }

  /**
   *  function which helps to redirect the URL 404 if the given collegeis present in the array of colleges ids.
   * This function written to remove all the URL indexed by google for the diplicate college IDS for the same college name
   * @param collegeId
   * @return Status as String.
   */
  public String duplicateCollegeRedirection(String collegeId) {
    String redirectionStatus = "";
    /*
     * wu303_20110222:- Mohamed Syed: these have to be handled in DB level, and these college may be right in future by some jobs.
     *
    String collegeIds[] = new String[] { "3917", "118794", "115318", "43781", "7335", "7931", "114768", "7931", "75584",
                                         "4630", "70168" };
    for (int j = 0; j < collegeIds.length; j++) {
      if (collegeId != null && collegeId.equals(collegeIds[j])) {
        redirectionStatus = "404";
        break;
      }
    }
    */
    return redirectionStatus;
  }

  /**
    *  This function is used to get the category description for the given category code.
    * @param categoryCode
    * @return Categotry code as String
    */
  public String getCatagoryDesc(String categoryCode) {
    DataModel dataModel = new DataModel();
    String categoryDesc = "";
    Vector parameters = new Vector();
    try {
      parameters.add(categoryCode);
      categoryDesc = dataModel.getString("wu_browse_pkg.get_category_desc_fn", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      //nullify unused objects      
      parameters.clear();
      parameters = null;
      dataModel = null;
    }
    return categoryDesc;
  }

  /**
    *  This funciton is used add the featured year of unientry statically from 2010 to 2015 for registration page POD.
    * @return Featured Uni List as collection
    */
  public List getFeaturedYearOfUniList() {
    RegistrationBean registrationbean = null;
    List yearOfUniEntryList = new ArrayList();
    for (int year = 2010; year <= 2015; year++) {
      registrationbean = new RegistrationBean();
      registrationbean.setYearOfPassing(String.valueOf(year));
      yearOfUniEntryList.add(registrationbean);
    }
    return yearOfUniEntryList;
  }

  /**
    *  This function is used to load the related memebers POD for the given course provider.
    * @param collegeId
    * @param request
    * @return none.
    */
  public ArrayList loadUniversityRelatedMembersPod(String collegeId, HttpServletRequest request) {
    DataModel dataModel = new DataModel();
    ResultSet resultSet = null;
    MembersSearchBean bean = null;
    Vector vector = new Vector();
    ArrayList relatedMemberList = new ArrayList();
    try {
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(collegeId);
      resultSet = dataModel.getResultSet("wu_members_pkg.get_uni_related_members_pod_fn", vector);
      while (resultSet.next()) {
        bean = new MembersSearchBean();
        // bean.setCollegeId(resultSet.getString("college_id"));
        //bean.setCollegeName(resultSet.getString("college_name"));
        bean.setUserName(resultSet.getString("username"));
        bean.setGender(resultSet.getString("gender"));
        bean.setUserId(resultSet.getString("user_id"));
        bean.setNationality(resultSet.getString("nationality"));
        bean.setYearOfGraduation(resultSet.getString("year_of_graduation"));
        bean.setIama(resultSet.getString("graduation"));
        /*bean.setUserImage(resultSet.getString("user_photo"));
        String userImageName = resultSet.getString("user_photo");
        String userThumbName = (userImageName != null ? userImageName.substring(0, userImageName.lastIndexOf(".")) + "T" + userImageName.substring(userImageName.lastIndexOf("."), userImageName.length()) : userImageName);
        bean.setUserImage(userThumbName);
        bean.setUserLargeImage(userImageName);*/
        relatedMemberList.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    //request.setAttribute("relatedMemberList", relatedMemberList);
    return relatedMemberList;
  }

  /**
      *  This function is used to display the uni names & subject based times info for the gived subject id.
      * @param subjectId
      * @return unilist as collection
      */
  public ArrayList getTimesLeagueUniList(String subjectId, String keyword) {
    ArrayList uniList = new ArrayList();
    DataModel dataModel = new DataModel();
    ResultSet resultSet = null;
    Vector parameters = new Vector();
    SearchBean bean = null;
    try {
      parameters.add(subjectId);
      parameters.add(keyword);
      resultSet = dataModel.getResultSet("WU_COMMON_INFO_PKG.GET_SUB_TIMES_LEAGUE_UNI_FN", parameters);
      while (resultSet.next()) {
        bean = new SearchBean();
        bean.setCollegeId(resultSet.getString("college_id"));
        bean.setCollege_Name(resultSet.getString("college_name"));
        bean.setCollegeNameDisplay(resultSet.getString("college_name_display"));
        bean.setTimesPosition(resultSet.getString("position_id"));
        uniList.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return uniList;
  }

  /**
    *  This function is used to display the uni names & subject based times info for the gived subject id.
    * @param collegeId
    * @return unilist as collection
    */
  public ArrayList getUniversityCategoryQualList(String collegeId) {
    ArrayList qualList = new ArrayList();
    DataModel dataModel = new DataModel();
    ResultSet resultSet = null;
    Vector vector = new Vector();
    SearchBean bean = null;
    try {
      vector.add(collegeId);
      resultSet = dataModel.getResultSet("wu_browse_pkg.get_uni_category_qual_fn", vector);
      while (resultSet.next()) {
        bean = new SearchBean();
        bean.setStudyLevelId(resultSet.getString("qualification_code"));
        qualList.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return qualList;
  }

  public String getPaginationIndex(String overAllCountForAllPages, String pageno, String recordcountForThisPage) {
    String paginationIndex = "";
    //
    if ((overAllCountForAllPages != null && overAllCountForAllPages.trim().length() > 0) && (pageno != null && pageno.trim().length() > 0) && (recordcountForThisPage != null && recordcountForThisPage.trim().length() > 0)) {
      int i_overAllCountForAllPages = Integer.parseInt(overAllCountForAllPages);
      int i_pageno = Integer.parseInt(pageno);
      int i_recordcountForThisPage = Integer.parseInt(recordcountForThisPage);
      //
      if ((i_overAllCountForAllPages + i_recordcountForThisPage) > ((i_pageno * i_recordcountForThisPage))) {
        int i_numberOne = 0;
        int i_numberTwo = 0;
        //    
        i_numberOne = ((i_pageno * i_recordcountForThisPage) - (i_recordcountForThisPage - 1));
        i_numberTwo = (i_overAllCountForAllPages > ((i_pageno * i_recordcountForThisPage))) ? (i_pageno * i_recordcountForThisPage) : i_overAllCountForAllPages;
        //
        paginationIndex = Integer.toString(i_numberOne);
        paginationIndex = paginationIndex + " - " + Integer.toString(i_numberTwo);
        paginationIndex = paginationIndex + " of " + overAllCountForAllPages;
      }
    }
    //
    return paginationIndex;
  }

  /**
     * Added by sekhar for wu405_13032012
      *  This function is used to get the entry points for displaying GAM attibure.
      * @param entryPoins
      * @return entryPoints as String.
      */
  public String getEntryPointsForGAM(String entryPoints) {
    DataModel dataModel = new DataModel();
    String entry = "";
    try {
      Vector parameters = new Vector();
      parameters.addElement(entryPoints);
      entry = dataModel.executeUpdate("wu_util_pkg.get_entry_points_fn", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return entry;
  }

  /**
   * Added by Sekhar K for getting thumb image for flv and mp4 videos.
   * @param videoURL
   * @param thumbNo
   * @return
   * @since
   */
  public String videoThumbnailFormatChange(String videoURL, String thumbNo) {
    String thumbUrl = "";
    thumbNo = !GenericValidator.isBlankOrNull(thumbNo) ? thumbNo : "1";
    if (!GenericValidator.isBlankOrNull(videoURL)) {
      thumbUrl = videoURL.indexOf(".mp4") > -1 ? videoURL.replaceAll(".mp4", "_tmb/000" + thumbNo + ".jpg") : videoURL.replaceAll(".flv", "_tmb/000" + thumbNo + ".jpg");
    }
    return thumbUrl;
  }

  public String videoThumbnailReviewAppserver(VideoReviewListBean bean, String thumbNo) {
    String thumbUrl = "";
    thumbNo = !GenericValidator.isBlankOrNull(thumbNo) ? thumbNo : "1";
    thumbUrl = "/commimg/video/wu_video/" + bean.getThumbnailUrl() + "_tmb/000" + thumbNo + ".jpg";
    return thumbUrl;
  }

  public String videoThumbnail(String mediaId, String size) {
    String thumbUrl = "/commimg/video/myhc_";
    String richProfHeroVideoThumbUrl = "/commimg/myhotcourses/rich/hero/video/myhc_"; //Added by Amir for 13_JAN_15 
    String richProfSecVideoThumUrl = "/commimg/myhotcourses/rich/video/myhc_"; //Added by Amir for 13_JAN_15
    if ("0".equalsIgnoreCase(size)) {
      thumbUrl = thumbUrl + mediaId + "_120px.jpg";
    } else if ("1".equalsIgnoreCase(size)) {
      thumbUrl = thumbUrl + mediaId + "_84px.jpg";
    } else if ("2".equalsIgnoreCase(size)) {
      thumbUrl = thumbUrl + mediaId + "_116px.jpg";
    } else if ("3".equalsIgnoreCase(size)) {
      thumbUrl = thumbUrl + mediaId + "_140px.jpg";
    } else if ("4".equalsIgnoreCase(size)) {
      thumbUrl = thumbUrl + mediaId + ".jpg";
    } else if ("5".equalsIgnoreCase(size)) {
      thumbUrl = thumbUrl + mediaId + "_84rtpx.jpg";
    } else if ("6".equalsIgnoreCase(size)) {
      thumbUrl = richProfHeroVideoThumbUrl + mediaId + ".jpg";
    } else if ("7".equalsIgnoreCase(size)) {
      thumbUrl = richProfSecVideoThumUrl + mediaId + ".jpg";
    }
    return thumbUrl;
  }

  /**
    * used to get the category_desc to the given category_code
    * @param categoryCode
    * @return category desc as String.
    */
  public String checkIfCategoryOrKeyword(String categoryCode) {
    if (categoryCode == null || categoryCode.trim().length() == 0) {
      return "";
    }
    DataModel dataModel = new DataModel();
    String categoryDesc = "";
    Vector parameters = new Vector();
    parameters.add(categoryCode.toUpperCase()); // cagtegoryId /Keyword 
    try {
      categoryDesc = dataModel.getString("WU_BROWSE_PKG.CHECK_BROWSE_CATEGORY_FN", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel = null;
      parameters.clear();
      parameters = null;
    }
    return categoryDesc;
  }
  //

  public String checkClearingCourseDeleted(String courseId) { //15_JUL_2014
    DataModel dataModel = new DataModel();
    String courseStatus = "NO";
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.addElement(courseId); // courseId   
      courseStatus = dataModel.executeUpdate("WU_UTIL_PKG.check_clear_course_delete_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return courseStatus;
  }

  public String checkFBUserExists(String email) { //5_AUG_2014
    DataModel dataModel = new DataModel();
    String fbUserExistsFlag = "";
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.addElement(email);
      fbUserExistsFlag = dataModel.getString("WU_USER_PROFILE_PKG.GET_FB_REG_USER_FLAG", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return fbUserExistsFlag;
  }
  //

  /**
   * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @getCustomDimesnionValues this method to get the insights dimensions
   * @param jspName
   * @param request
   * @return void
   * @since 29_MARCH_2016
   * Change Log : Change the insights method from data modal to Spring modal and added country, post code for insights by Prabha
   *              Added newPostcode request for CD page insights log by Prabha on 31_May_2016
   *              Added schoolUrn and source type by Prabha on 08_Aug_2017
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void getCustomDimesnionValues(String jspName, HttpServletRequest request) {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    String userId = new SessionData().getData(request, "y");
    userId = !GenericValidator.isBlankOrNull(userId) ? userId : "0";
    
    String postCode = request.getAttribute("cDimPostcode") != null ? request.getAttribute("cDimPostcode").toString() : "";
    String courseId = request.getAttribute("courseId") != null ? request.getAttribute("courseId").toString() : "";
    String qualification = request.getAttribute("qualification") != null ? request.getAttribute("qualification").toString().toUpperCase() : "";
    String categoryCode = request.getAttribute("dimCategoryCode") != null ? request.getAttribute("dimCategoryCode").toString().toUpperCase() : "";
    String location = !GenericValidator.isBlankOrNull((String)request.getAttribute("location")) ? (String)request.getAttribute("location") : "UNITED KINGDOM";
    jspName = (request.getAttribute("getInsightName") != null && !"".equals(request.getAttribute("getInsightName"))) ? request.getAttribute("getInsightName").toString() : jspName;    
    if("coursedetails.jsp".equalsIgnoreCase(jspName)){
      postCode = !GenericValidator.isBlankOrNull((String)request.getAttribute("newPostcode")) ? (String)request.getAttribute("newPostcode") : "";
    }
    String collegeId = request.getAttribute("cDimCollegeId") != null ? request.getAttribute("cDimCollegeId").toString() : "";
    InsightsVO insightsVO = new InsightsVO();
    insightsVO.setPageName(jspName);
    insightsVO.setUserId(userId);
    insightsVO.setPostCode(postCode);
    insightsVO.setCourseId(courseId);
    insightsVO.setQualification(qualification);
    insightsVO.setCategoryCode(categoryCode);
    insightsVO.setLocation(location);
    insightsVO.setCollegeId(collegeId);
    Map insightInfoMap = commonBusiness.getInsightData(insightsVO);
    if (insightInfoMap != null && !insightInfoMap.isEmpty()) {
      ArrayList insightList = (ArrayList)insightInfoMap.get("RetVal");
      if(insightList != null && insightList.size() > 0){
        Iterator insightItr = insightList.iterator();
        while (insightItr.hasNext()){
          InsightsVO insightVO = (InsightsVO)insightItr.next();
          if(!GenericValidator.isBlankOrNull(insightVO.getPageName())){
            request.setAttribute("cDimPageName", insightVO.getPageName());
            if(!GenericValidator.isBlankOrNull(userId) && !"0".equals(userId)){
              request.setAttribute("cDimUID", userId);
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getYearOfEntry())){
              request.setAttribute("cDimTargetYear", insightVO.getYearOfEntry());
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getGradeValue())){
              request.setAttribute("cDimGrade", insightVO.getGradeValue());
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getSchoolName())){
              request.setAttribute("cDimSchool", insightVO.getSchoolName());
            }
            String regionName = insightVO.getRegionName();
            if (!GenericValidator.isBlankOrNull(regionName) && !"UNITED KINGDOM".equalsIgnoreCase(regionName)) {
              request.setAttribute("cDimRegion", regionName.toLowerCase());
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getCpeSubjectName())){
              request.setAttribute("cDimCPE2", insightVO.getCpeSubjectName().toLowerCase());
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getJacs())){
              request.setAttribute("cDimJACS", insightVO.getJacs().toUpperCase());
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getHecos())){
              request.setAttribute("cDimHECOS", insightVO.getHecos());
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getAdvertiser())){
              request.setAttribute("cDimIsAdvertiser", insightVO.getAdvertiser());
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getGranularQual())){
              request.setAttribute("cDimGranularQualificationType", insightVO.getGranularQual());
            }
            String countryName = insightVO.getCountry();
            if (!GenericValidator.isBlankOrNull(countryName)){
              request.setAttribute("cDimCountry", countryName);
            }else{
              request.setAttribute("cDimCountry", "UK");
            }
            //changed quintile coloumn by Hema.S on 28.03.2018
            if(!GenericValidator.isBlankOrNull(insightVO.getQuintile())){
              request.setAttribute("cDimQuintile", insightVO.getQuintile());
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getStudyLevel())){
              request.setAttribute("cDimLevel", insightVO.getStudyLevel());
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getStudyMode())){
              request.setAttribute("cDimStudyMode", insightVO.getStudyMode().toLowerCase());
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getCourseTitle())){
              request.setAttribute("cDimCourseTitle", insightVO.getCourseTitle());
            } 
            if(!GenericValidator.isBlankOrNull(insightVO.getInstitutionId())){
              request.setAttribute("cDimCollegeId", insightVO.getInstitutionId());
            }
            //Added dimension 8, 9 in GA by Prabha on 08_Aug_17
            if(!GenericValidator.isBlankOrNull(insightVO.getSchoolUrn())){
              request.setAttribute("dimSchoolUrn", insightVO.getSchoolUrn());
            }
            if(!GenericValidator.isBlankOrNull(insightVO.getSourceType())){
              request.setAttribute("dimSourceType", insightVO.getSourceType());
            }
            //End of 8,9 code
             if(!GenericValidator.isBlankOrNull(insightVO.getCpeSubjectNames())){
               String cDimPixelCPE2 = insightVO.getCpeSubjectNames();
               String CPE2SubCount = insightVO.getCpeSubjectCount();
               if(!GenericValidator.isBlankOrNull(CPE2SubCount) && new CommonUtil().isNumber(CPE2SubCount) && Integer.parseInt(CPE2SubCount) > 1) {
                 cDimPixelCPE2 = "[" + cDimPixelCPE2 + "]";
               }
               request.setAttribute("cDimPixelCPE2", cDimPixelCPE2);
             }
          }
        }
      }
    }
  }

  /**
    * used to get the studyLevelDesc for studyLevelID
    * @param studyLevelId
    * @return String
   */
  public void getStudyLevelDesc(String studyLevelId, HttpServletRequest request) { //26_AUG_2014
    String studyLevelDesc = "";
    String level = "Undergraduate";
    if ("M".equalsIgnoreCase(studyLevelId)) {
      studyLevelDesc = "undergraduate qualification";
    } //13_MAY_2014
    else if ("L".equalsIgnoreCase(studyLevelId)) {
      studyLevelDesc = "postgraduate qualification";
      level = "Postgraduate";
    } else if ("A".equalsIgnoreCase(studyLevelId)) {
      studyLevelDesc = "foundation degree";
    } else if ("T".equalsIgnoreCase(studyLevelId)) {
      studyLevelDesc = "access to higher education";
    } else if ("N".equalsIgnoreCase(studyLevelId)) {
      studyLevelDesc = "hnd/hnc/higher education awards";
    } else if ("C".equalsIgnoreCase(studyLevelId)) {
      studyLevelDesc = "undergraduate qualification";
    }
    request.setAttribute("cDimQualificationType", studyLevelDesc);
    request.setAttribute("cDimLevel", level);
  }

  /**
    * used to replace the String
    * @param replaceString
    * @return String
   */
  public String getReplacedString(String replaceString) { //16_Sep_2014, added by Thiyagu G
    String replacedStr = replaceString.replaceAll("'", "");
    replacedStr = replacedStr.replaceAll("\"", "");
    return replacedStr;
  }

  public String checkifYearOfEntryExists(String email) { //5_AUG_2014
    DataModel dataModel = new DataModel();
    String fbYoeFlag = "";
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.addElement(email);
      fbYoeFlag = dataModel.getString("WU_USER_PROFILE_PKG.get_fb_reg_user_yoe_flag", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return fbYoeFlag;
  }

  /**
   * Added by Yogeswari 09:06:2015 for clearing splash screen.
   * @param  session
   * @param  requestedInput
   * @return void
   * @since  09:06:2015
   */
  public boolean isClearing(String requestedInput) {
    String[] clearingPage = GlobalConstants.CLEARING_PAGES;
    if ("clearing".equals(requestedInput)) {
      return true;
    } else if ("non-clearing".equals(requestedInput)) {
      return false;
    } else {
      for (int i = 0; i < clearingPage.length; i++) {
        if (requestedInput.contains(clearingPage[i])) {
          return true;
        }
      }
    }
    return false;
  }

  /**
    * Added by Yogeswari to return user image path.
    * @param  userDetails
    * @return String
    * @since  21:07:2015
    */
  public String getUserImagePath(String userDetails) {
    String userImage = "";
    CommonUtil commonUtil = new CommonUtil();
    if (userDetails != null && userDetails.trim().length() > 0 && (userDetails.contains("USER_IMAGE") || userDetails.contains("FB_USER_ID"))) {
      String UserDetailsArray[] = userDetails.split("\\|");
      userImage = UserDetailsArray[1];
      if (userDetails.contains("USER_IMAGE")) {
        userImage = commonUtil.getImgPath(userImage, 1);
      } else {
        userImage = "http://graph.facebook.com/" + userImage + "/picture?type=small";
      }
    } else {
      userImage = commonUtil.getImgPath("/wu-cont/images/rec_act_prof_def_img.png", 1);
    }
    return userImage;
  }
  
  /**
   * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @getInstitutionId this method to get the insights dimensions
   * @param college_Id
   * @param request
   * @return void
   * @since 29_MARCH_2016
   * Change Log : Change the insights method from data modal to Spring modal and added country, post code for insights by Prabha
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public String getInstitutionId(String collegeId) {
    String instituionId = "";
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
    InsightsVO insightsVO = new InsightsVO();
    insightsVO.setCollegeId(collegeId);
    Map insightInfoMap = commonBusiness.getInstitutionId(insightsVO);
    if (!insightInfoMap.isEmpty()) {
      instituionId = insightInfoMap.get("RetVal").toString();      
    }
    return instituionId;
  }

}
