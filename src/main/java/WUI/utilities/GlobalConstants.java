package WUI.utilities;

/**
  * @GlobalConstants.java
  * @Version : 2.0
  * @October 10 2007
  * @www.whatuni.com
  * @author By : Balraj. Selva Kumar
  * @Purpose  : This program used to maintain all the global constans used in the Application.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 08_Mar_2016   Thiyagu G                  2.1    Added Uni Home URL                                          wu_550
  */
public interface GlobalConstants {

  public static final String SEO_URL_SPLIT_CHAR = "#";
  public final String WU_CONTEXT_PATH = "/degrees";
  public final String SEO_PATH_UNI_PROFILE_PAGE = "/university-profile/";
  public final String SEO_PATH_SUBJECT_PROFILE_PAGE = "/subject-profile/";
  public final String SEO_PATH_UNI_REVIEWS = "/reviews/uni/";
  public final String SEO_SUBJECT_LANDING = "/what-is/";
  public final String SEO_PATH_UNI_VIDEOS = "/college-videos/";
  public final String SEO_PATH_CLEARING_YEAR = "2010";
  public final String SEO_PATH_CLEARING_YEAR_DISPLAY = "2011";
  public final String SEO_PATH_PROSPECTUS_YEAR = "2012";
  public final String SEO_PATH_CLEARING = "clearing";
  public final String SEO_PATH_UNI_BROWSE_LOCATION_PAGE = "/university-colleges-uk/";
  public final String SEO_PATH_UNI_BROWSE_LOCATION_PAGE_URL = "university-colleges-";
  public final String SEO_PATH_SCHOLARSHIP_PAGE = "/scholarship-uk/";
  public final String SEO_PATH_UNILANDING_PAGE = "/university-uk/";
  public final String SEO_PATH_NEW_UNILANDING_PAGE = "/university-profile/"; //URL restructure - Added Uni Home URL for 08_Mar_2016 By Thiyagu G.
  public final String SEO_PATH_OPEN_DAY_PAGE = "/open-days/";
  public final String SEO_PATH_UNIBROWSE_PAGE = "/find-university/"; //Modified for 13-Jan-2015
  public final String SEO_PATH_SEARCHVIEW_PAGE = "/universities/reviews/";
  public final String SEO_PATH_COURSEDETAIL_PAGE = "/courses/";
  public final String SEO_PATH_COURSESEARCH_PAGE = "/courses/";
  public final String SEO_PATH_SEARCHRESULT_PAGE = "/courses/";
  public final String SEO_PATH_COURSEJOUNEY_PAGE = "/courses/";
  public final String SEO_PATH_REVIEW_CATEGORY_PAGE = "/reviews/uni/";
  public final String SEO_PATH_DEGREE_COURSES = "degree-courses";
  public final String WHATUNI_AFFILATE_ID = "220703";
  public final String WHATUNI_WEBSITE_ID = "2";
  public final String EMAILSUCCESSMESSAGE = "Your email has been sent to the college successfully!";
  public final String LIMELIGHT_VIDEO_PATH = "http://hotcourses.vo.llnwd.net/o18/WHATUNI/LIVE/"; // + new CommonFunction().getWUSysVarValue("WU_LIMELIGHT_FOLDER");
  public final String LIMELIGHT_VIDEO_FILE_EXT = ".flv";
  public final String LIMELIGHT_VIDEO_PROVIDER_NAME = "Uploaded by the uni";
  public final String REGISTERED_PAGE_REDIRECTION = "registrationpage"; //RegistrationPage
  public final String LOGIN_PAGE_REDIRECTION = "loginPage"; //loginPage
  public final String SEO_PATH_LIST_REVIEW = "/university-course-reviews/"; // list review page
  public final String SEO_PATH_RATING_REVIEW = "/university-rating-reviews/"; // list rating review page gropu by 1&2, 3&4......
  public final String ACCOMADATION = "WUNI ACCOMMODATION & LOCATION";
  public final String OVERVIEW = "WUNI OVERVIEW";
  public final String ENTERTAINMENT = "WUNI STUDENT UNION & ENTERTAINMENT";
  public final String WELFARE = "WUNI WELFARE";
  public final String FEES_FUNDING = "WUNI FEES AND FUNDING";
  public final String CONTACT = "WUNI CONTACT DETAILS";
  public final String REVIEW = "REVIEW";
  public final String CAMPUS = "CAMPUS";
  public final String GALLERY = "GALLERY";
  public final String JOB_PROSPECTS = "WUNI JOB PROSPECTS";
  public final String COURSES_ACADEMICS_FACILITIES = "WUNI COURSES, ACADEMICS & FACILITIES";
  public final String CLEARING_USPS = "CLEARING USPS";
  public final String OUR_CLEARING_GUIDE = "OUR CLEARING GUIDE";
  public final String WHAT_HAPPENS_NEXT = "WHAT HAPPENS NEXT";
  public final String CONTACT_DETAILS = "CONTACT DETAILS";
  public final String COVID_19 = "COVID 19";
  public final int DEFAULT_IMAGE_LENGTH = 50;
  public final String BLANK_IMAGE = "blankimage";
  public final String ORDER_UP_IMAGE = "orderupimage";
  public final String ORDER_UP_2_IMAGE = "orderup2image";
  public final String ORDER_DOWN_IMAGE = "orderdownimage";
  public static final int WU_COOKIE_MAX_AGE = 31104000;
  public static final int WU_COOKIE_POPUP_COOKIE_AGE = 2592000;//Changed cookie pop-up max age from 1 year(31-july-2018_rel) to 30 days on 24-SEP-2019 rel by Sangeeth.S
  public static final int WU_BASKET_COOKIE_MAX_AGE = 15768000; //Basket cookie max age is 6 month added by Prabha on 27-Jan-2016	 
  public final String STUDENTS_JOB_ALT_MSG = "% of students 6 months after leaving the uni with a job or in further study (DLHE survey).";
  public final String SATISFICATION_WITH_UNI_ALT_MSG = "The % of students in The National Student Survey (NSS) that said they were satisfied with the quality of their course.";
  public final String WHATUNI_DOMAIN = "mtest.whatuni.com";
  /*Omniture information*/
  public final String REGISTRATION_EVENT = "EVENT3";
  public final String EMAIL_EVENT = "EVENT13";
  public final String WEBSITE_EVENT = "EVENT14";
  public final String VIEW_COLLEGE_EVENT = "EVENT15";
  public final String REQUEST_PROSPECTUS_EVENT = "EVENT16";
  public final String CONTACT_EVENT = "EVENT17";
  public final String PROSPECTUS_SPLIT_CHAR = "##";
  public final String ADVICE_SP_SPLIT = "##SP##";
  public final String AUTOCOMPLETE_SPLITTER = "###";
  public final String SESSION_ID = "16180339";
  public final String GA_ACCOUNT = "UA-22939628-2";
  public final String INSIGHT_ACCOUNT = "UA-52581773-4";
  public final String GTM_ACCOUNT = "GTM-PKPN9P9";
  //public final String GTM_ACCOUNT = "GTM-WFGC2WJ"; // LIVE ACCOUNT
  public final String MAP_URL = "https://maps.googleapis.com/maps/api/staticmap?maptype=roadmap&key=AIzaSyB1uMdPqhgPKShyQQAjDa2DBr-X_GSGfmM";
  public final String MAP_SEARCH_URL = "https://www.google.com/maps/search/?api=1&query=";
  public static final String IDP_CONNECT_URL = "https://www.idp-connect.com/";
  public final String IMAGE225Px = "_225px";  
  public final String ADMEDO_PIXEL = "wuni_admedo_pixeltracking1x1";
  public final String CONVERSION_PIXEL = "wuni_conversionpixel_1x1"; //Added conversion pixel by Prabha on 03_OCT_2017
  public final String ADMEDO_PAGE_NAMES = "clearingbrowsemoneypage.jsp, browsemoneypageresults.jsp, richprofilelanding.jsp, clearingcoursesearchresult.jsp, coursesearchresult.jsp, coursedetails.jsp, providerreviewresults.jsp, newproviderhome.jsp, opendaysprovlanding.jsp, clearinghome.jsp, contenthub.jsp, advancesearchresults.jsp, advanceproviderresults.jsp";// collegeemailsuccess.jsp";
  public final String CMMT_PAGE_NAMES = "whatuniGoSignIn.jsp, whatuniGoSignUp.jsp, provisionalOfferLandingPage.jsp, newEntryReqLandingPage.jsp, allErrorPage.jsp";
  public final String HIDE_NAV_SRCH_PAGES = CMMT_PAGE_NAMES;
  public final String HIDE_HEADER_PAGES = CMMT_PAGE_NAMES;
  public final String HIDE_TIMELINE_PAGES = CMMT_PAGE_NAMES;
  public final String HIDE_MOBILE_SRCH_PAGES = CMMT_PAGE_NAMES + ",newUser.jsp";
  //
  public final String SMART_PIXEL_PAGE_NAMES = "browsemoneypageresults.jsp, coursesearchresult.jsp, contenthub.jsp, coursedetails.jsp, providerreviews.jsp, viewopendaysnew.jsp, richuniview.jsp, uniview.jsp, richsubjectprofile.jsp, subjectprofile.jsp, qlbasicform.jsp, collegeemailsuccess.jsp, clearingProfile.jsp";
  public final String DEFAULT_SCREEN_WIDTH = "1024";
  //
  public static final String PAGES_THAT_DOESNT_NEEDS_AUTHENTICATION = WU_CONTEXT_PATH + "/home.html#" + WU_CONTEXT_PATH
 			+ "/signin-submit.html";
 	
 	// TODO - Please append here whenever you add ajax request in application
 	public static final String[] AJAX_URLS = { "/reorder-final-choice", "/comparison", "/get-friends-activity-content" , "/get-suggested-pod" };
  //
   	// TODO - Please append here whenever you add ajax request in application
 	public static final String[] AJAX_NORMAL_URLS = { "/addbasket", "/myfinalchoice" };

  //public static final String WHATUNI_SCHEME_NAME = "http://"; //TODO comment this line when we deploy the war in LIVE.
  public static final String WHATUNI_SCHEME_NAME = "https://"; //TODO uncomment this line when we deploy the war in LIVE.
  //video related
  public final String WHATUNI_SITE_VIDEO_FNAME = "http://hotcourses.vo.llnwd.net/o18/WHATUNI/LIVE/www_whatuni_com.flv";
  //
  //DB stats logging related activity values
  public final String REVIEW_GUIDELINES_SHTML = "WU_REVIEW_GUIDELINES";
  public final String BESPOKE_UNI_CNT = "BESPOKE_UNI_CNT";
  public final String BESPOKE_UNI_CNT_NAME = "BESPOKE_UNI_CNT_NAME";
  public final String WHATUNI_STATS_OPEN_DAYS = "WU-OPEN-DAYS: LANDING PAGE: UNIVERSITY";
  public final String WHATUNI_STATS_WEB_CLICK = "HOTCOURSES: WEBSITE CLICK";
  public final String STATS_CATEGORY_SPONSERSHIP = "HOTCOURSES: WEBSITE CLICK: CATEGORY SPONSORSHIP";
  public final String STATS_FEATURED_COURSE = "HOTCOURSES: WEBSITE CLICK: FEATURED COURSES";
  public final String STATS_TICKER_TAPE = "HOTCOURSES: TICKER TAPE: CLICK";
  public final String STATS_COLLEGE_EMAIL = "HOTCOURSES: EMAIL SENT: ASK COLLEGE";
  public final String STATS_COLLEGE_EMAIL_WEBFORM = "HOTCOURSES: EMAIL SENT: WEBSITE CLICK";
  public final String STATS_COLLEGE_PROSPECTUS = "HOTCOURSES: EMAIL SENT: REQUEST PROSPECTUS";
  public final String STATS_COLLEGE_PROSPECTUS_WEBFORM = "HOTCOURSES: EMAIL SENT: REQUEST PROSPECTUS: WEBSITE CLICK";
  public final String STATS_COURSE_OPPORTUNITY_APPLY_NOW = "HOTCOURSES: APPLY NOW";
  public final String CLEARING_STATS_WEB_CLICK = "HOTCOURSES: CLEARING: WEBSITE CLICK";
  public final String DOWNLOAD_PROSPECTUS_WEB_CLICK = "HOTCOURSES: DOWNLOAD PROSPECTUS";
  public final String CLEARING_STATS_KEYWORD_SPONSORSHIP = "HOTCOURSES: CLEARING: CATEGORY SPONSORSHIP";
  public final String CLEARING_STATS_FEATURED_PROVIDERS = "HOTCOURSES: CLEARING: FEATURED PROVIDER";
  public final String CLEARING_STATS_HOTLINE = "HOTCOURSES: CLEARING: HOTLINE";
  public final String CLEARING_STATS_FEATURED_HOTLINE = "HOTCOURSES: CLEARING: FEATURED HOTLINE"; //Added featured hotline stats on 16_May_2017 By Thiyagu G
  public final String STATS_SPOTLIGHT_VIDEO = "HOTCOURSES: SPOTLIGHT: CLICK"; //16_SEP_2014
  public final String STATS_HERO_IMAGE_VIEW = "HOTCOURSES: HERO IMAGE: VIEW"; //18_Nov_2014 added hero image stats view by Thiyagu G
  public final String STATS_HERO_IMAGE_CLICK = "HOTCOURSES: HERO IMAGE: CLICK"; //18_Nov_2014 added hero image stats click by Thiyagu G
  public final String STATS_ADVICE_DETAIL_VIEW = "HOTCOURSES: ARTICLES VIEW"; //18_Nov_2014 added hero image stats click by Thiyagu G
  public final String STATS_PROVIDER_RESULTS_LIST = "HOTCOURSES: PROVIDER RESULTS";
  public final String STATS_PROVIDER_REVIEW_LIST = "HOTCOURSES: REVIEW: LIST";
  public final String STATS_PROVIDER_REVIEW_VIEW = "HOTCOURSES: REVIEW: VIEW";
  public final String STATS_PROVIDER_PROSPECTUS_LIST = "HOTCOURSES: PROSPECTUS PAGE";
  public final String STATS_PROVIDER_SCHOLARSHIP_LIST = "HOTCOURSES: SCHOLARSHIP DETAIL: LIST";
  public final String STATS_PROVIDER_SCHOLARSHIP_VIEW = "HOTCOURSES: SCHOLARSHIP DETAIL: VIEW";
  public final String STATS_PROVIDER_OPENDAYS_VIEW = "HOTCOURSES: OPEN DAYS: VIEW";
  public final String STATS_PROVIDER_RICHPROFILE_SEC_VIDEOCLICK = "HOTCOURSES: RICH PROFILE: VIDEO CLICK";
  public final String STATS_OPEN_DAYS_AD_CALENDAR = "HOTCOURSES: OPEN DAYS: ADD CALENDAR";
  public final String STATS_PROVIDER_OD_RESERVE_PLACE = "HOTCOURSES: OPEN DAYS: WEBSITE CLICK";
  public final String STATS_ARTICLE_SPONSORSHIP_WEBCLICK = "HOTCOURSES: ARTICLE SPONSORSHIP: WEBCLICK";
  public final String STATS_NON_ADVERTAISOR_PDF_DOWNLOAD = "HOTCOURSES: DOWNLOAD: PDF"; //Added new pdf stats key for no advertiasor pdf download for 24_Nov_2015, By Thiyagu G.
  public final String STATS_WUGO_APPLY_NOW =  "HOTCOURSES: WHATUNIGO: APPLY NOW";
  public final String STATS_WUGO_COMPLETE_APPLICATION = "HOTCOURSES: WHATUNIGO: COMPLETE APPLICATION";
  public final String STATS_WUGO_ACCEPT_OFFER = "HOTCOURSES: WHATUNIGO: ACCEPT OFFER";
  public final String STATS_WUGO_CONFIRM_OFFER = "HOTCOURSES: WHATUNIGO: CONFIRM FINALOFFER";
  public final String STATS_WUGO_CANCEL_OFFER = "HOTCOURSES: WHATUNIGO: CANCEL OFFER";
  public final String STATS_WUGO_TIMER1_EXP = "HOTCOURSES: WHATUNIGO: TIMER1: TIME EXPIRED";
  public final String STATS_WUGO_TIMER2_EXP = "HOTCOURSES: WHATUNIGO: TIMER2: TIME EXPIRED";
  public final String STATS_WUGO_TIMER2_NOT_ELIGIBLE = "HOTCOURSES: WHATUNIGO: TIMER2: NOT ELIGIBLE";
  public final String STATS_CLEARING_PROFILE_VIEW = "HOTCOURSES: CLEARING: PROFILE VIEW";
  
  //PRC names
  public final String COMPARE_COURSE_UNI_PRC = "MYWU_COMPARE_PKG.COMPARE_COURSE_UNI_PRC"; ////19_MAY_2015  MywhatuniComparison
  public final String SAVE_COMPARISON_NAME_FN = "MYWU_COMPARE_PKG.SAVE_BASKET_NAME_FN"; ////19_MAY_2015  MywhatuniComparison
  public final String VIEW_COMPARE_TABLE_FN = "MYWU_COMPARE_PKG.GET_BASKET_POD_FN"; ////19_MAY_2015 MywhatuniComparison
  public final String SAVE_BASKET_FN = "MYWU_COMPARE_PKG.ADD_NEW_BASKET_PRC"; ////19_MAY_2015 MywhatuniComparison
  public final String DELETE_BASKET_FN = "MYWU_COMPARE_PKG.DELETE_BASKET_FN"; ////19_MAY_2015 MywhatuniComparison
  //
  public final String SPLIT_CONSTANT = "##SPLIT##";
  public final String INNER_SPLIT_CONSTANT = "##INSPLIT##";
  //
  //Added PRV page names on 11_July_2017, By Thiyagu G.
  public final String PRV_PAGE_NAMES = "contenthub.jsp, richuniview.jsp, coursesearchresult.jsp, coursedetails.jsp, clearingcoursesearchresult.jsp, clearinguniprofile.jsp, clearingCourseDeatils.jsp, viewopendaysnew.jsp, uniview.jsp, richsubjectprofile.jsp, collegeemailsuccess.jsp, collegeprospectussuccess.jsp, subjectprofile.jsp, providerreviews.jsp, clearingunilanding.jsp"; //Added clearingunilanding.jsp to fix the bug:40048, on 08_Aug_2017, By Thiyagu G
  //
  public final String UCAS_LOAD_YEAR_VALUE = "2017";//Added by Indumathi.S JUN_28_16 REL
  //  
  public static final int PROS_COOKIE_MAX_AGE = 2592000;
  public String CLEARING_YEAR = "2020";
  public String NEXT_YEAR = "2019";
  public String YEAR_OF_ENTRY_1 = "2019"; //18_DEC_2018
  public String YEAR_OF_ENTRY_2 = "2020";
  public String YEAR_OF_ENTRY_3 = "2021";
  public String YEAR_OF_ENTRY_4 = "2022";
  public String FIND_A_UNI_URL = "/degrees/find-university/";
  //
  public String FEES_TOOLTIP_YEAR_1 = "2017";
  public String FEES_TOOLTIP_YEAR_2 = "2018";
  //
  public String NETWORK_CODE = "1029355";
  //
  public static final String[] CLEARING_PAGES = { "clearing", "/clcourse", "/clcdetail", "/clcsearch", "/student-survey" }; //21_JUL_2015_REL Added student survey link to lisr of pages where clearing popup should not be shown
  //
  public final String MY_COURSE_SEARCH_PRC = "WU_COURSE_SRCH_PKG.COURSE_SEARCH_PRC"; //21_JULY_2015  My course search
  public static final String GET_CL_SURVEY_RESPONSE_FN = "HOT_WUNI.WU_ALEVEL_RESULTS_PKG.GET_CL_SURVEY_RESPONSE_FN"; //Added by Priyaa for 21_JULY_2015_REL For Student Survey
  public static final String SAVE_SURVEY_RESPONSE_FN = "HOT_WUNI.WU_ALEVEL_RESULTS_PKG.SAVE_SURVEY_RESPONSE_FN";
  public static final String CHECK_DECRYPT_VALUE_FN = "HOT_WUNI.WU_ALEVEL_RESULTS_PKG.CHECK_DECRYPT_VALUE_FN";
  public static final String GET_USER_SOURCE_TYPE_FN = "HOT_WUNI.WU_ALEVEL_RESULTS_PKG.GET_USER_SOURCE_TYPE_FN"; //1-SEP-2015 modified for student survey by priyaa
  public static final String SURVEY_SUBJECT_ATTRIBUTE_ID = "319";
  public static final String SURVEY_CLEARING_RESP_ATTRIBUTE_ID = "344";
  public static final String DEFERRED_YOE_ATTRIBUTE_ID = "309";
  public static final String USER_CHATBOT_MINIMIZED_ATTR_ID = "359";  
  public static final String[] ATTRIBUTE_IDS = { "197", "210", "268", "267"};
  public static final String UG_NETWORK_ID = "2"; //Added by Indumathi NOV-03-15
  public static final String GET_POPULAR_ARTICLE_FN = "wuni_articles_pkg.get_popular_article_fn"; //Procedure for getting trending and most popular article details - Added by Prabha on 03_NOV_2015_REL
  public static final String GET_PDF_XML_CONTENT_PRC = "build_pdf_content_pkg.build_data_prc"; //Procedure for getting XML content for generating PDF - Added by Prabha on 24_NOV_2015_REL

  public static final String SEND_US_MSG_PRC = "HOT_WUNI.WU_EMAIL_PKG.SOCIAL_BOX_SEND_US_MSG_PRC";//Added by Indumathi.S for social box Nov-24 Rel
  public final String PG_UNIVERSITY_NEW_URL_PRC = "WU_PG_REDIRECTION_PKG.GET_NEW_URL_FN"; //To get PG university new subject to form the new URL pettern for 27_Jan_2016, By Thiyagu G.
  public final String GET_PROVIDER_RESULT_PAGE_PRC = "WHATUNI_SEARCH_PKG.get_provider_result_page_prc"; //To get SR page details for 27_Jan_2016, By Thiyagu G.
  public final String GET_SEARCH_RESULT_PAGE_PRC = "WHATUNI_SEARCH_PKG.GET_MONEY_PAGE_CONTENT_PRC"; //To get PR page details for 27_Jan_2016, By Thiyagu G.   
  public final String CALCULATE_UCAS_POINTS_FN = "wu_ucas_calculator_pkg.calculate_ucas_points_fn"; //To get ucas points and score based on the subject, 16_Feb_2015 By Thiyagu G
  public final String CALCULATE_TARIFF_POINTS_TABLE_FN = "wu_ucas_calculator_pkg.display_tariff_points_table_fn"; //To get tariff tables, 16_Feb_2015 By Thiyagu G
  public final String GET_SUBJECT_LIST_PRC = "WU_UCAS_CALCULATOR_PKG.GET_SUBJECT_LIST_PRC"; //Submit subject details, 16_Feb_2015 By Thiyagu G
  public final String GET_UCAS_YEAR_OF_ENTRY_PRC = "WU_UCAS_CALCULATOR_PKG.GET_QUALIFICATION_LIST_PRC"; //To get year of entry 16_Feb_2015 By Thiyagu G
  public final String GET_SEARCH_SUBJECT_LIST_PRC = "WU_UCAS_CALCULATOR_PKG.GET_SEARCH_SUBJECT_LIST_PRC"; //To get the browse nodes 16_Feb_2015 By Thiyagu G
  public final String UCAS_YEAR_OF_ENTRY_FN = "hot_wuni.wu_ucas_calculator_pkg.ucas_year_of_entry_fn"; //To get tariff tables, 16_Feb_2015 By Thiyagu G
  //
  public final String SORT_IWANTTOBE_INFO_PRC = "WU_ULTIMATE_SEARCH_PKG.sort_Iwanttobe_employ_info_prc"; //Added for i want to be page by Prabha 16-feb-16
  public final String GET_IWANTTOBE_STATS_INFO_PRC = "WU_ULTIMATE_SEARCH_PKG.get_Iwanttobe_empl_info_prc"; //Added for i want to be page by Prabha 16-feb-16
  public final String CHECK_COLLEGE_EXIST_FN = "HOT_WUNI.WU_UTIL_PKG.CHECK_COLLEGE_EXIST_FN"; //To check the deleted provider flag for 16_FEB_2016_REL, Prabha
  public final String CHECK_DEGREE_COURSE_FN = "HOT_WUNI.WU_UTIL_PKG.CHECK_DEGREE_COURSE_FN"; //To check the degree courses flag for 16_FEB_2016_REL, Prabha
  public final String HESA_SOURCE = "SOURCE: HESA 2018";//Changed HESA source for 03_MAR_2020 by Sri Sankari.R.  
  public final String GET_QUALIFICATION_LIST_PRC = "wu_ultimate_srch_entry_det_pkg.get_qualification_list_prc";//Added by Inudmathi.S WCID Qualification
  public final String SORT_WHATCANIDO_INFO_PRC = "wu_ultimate_search_pkg.sort_whatcanido_empl_info_prc"; //Added sorting what can i do widget prc by Prabha on 08_MAR_2016
  public final String GET_JOB_INDUSTRY_INFO_PRC = "wu_ultimate_search_pkg.get_industry_job_details_prc"; //Added job and industry info prc for what course i do by Prabha on 08_MAR_2016
  public final String WHAT_CAN_I_DO_GA_PAGE_NAME = "what-can-i-do/common-degrees"; //Added ga page name for what can i do widget by Prabha on 08_Mar_2016
  public final String WHAT_CAN_DO_INSIGHT_NAME = "what-can-i-do/common-degrees"; //Added insight name for what can i do widget by Prabha on 08_Mar_2016
  public final String GET_INSIGHT_PAGE_NAME_FN = "WU_UTIL_PKG.GET_INSIGHT_PAGE_NAME_FN"; //Added insights function name by Prabha on 29_Mar_2016
  public final String SCOUT_PAGE_PRC = "HOT_WUNI.WU_USER_PROFILE_PKG.SCOUT_PAGE_PRC"; //Added by Prabha for scout home page on 19_Apr_2016
  public final String GET_INSTITUTION_ID_FN = "WU_UTIL_PKG.GET_INSTITUTION_ID_FN"; //Added institution id function by Thiyagu G on 31_May_2016
  public final String GET_BREADCRUMB_PRC = "HOT_WUNI.WU_SEO_PKG.BREADCRUMBS_PRC"; //Added breadcrumb prc by Prabha on 21_mar_2017
  public final String GET_SUBJECT_POD_FN = "WUNI_HOMEPAGE_PKG.GET_SUBJECT_POD_FN"; //Added function to get popuplar subjects by Thiyagu G on 21_Mar_2017
  //Added by Indumathi.S for WUSCA url changes Mar_29_2016 Rel
  public final String UNIVERSITY_OF_THE_YEAR = "university-of-the-year";
  public final String AWD_JOB_PROSPECTS = "job-prospects";
  public final String COURSE_AND_LECTURERS = "course-and-lecturers";
  public final String STUDENT_UNION = "student-union";
  public final String ACCOMMODATION = "accommodation";
  public final String UNI_FACILITIES = "uni-facilities";
  public final String CITY_LIFE = "city-life";
  public final String LOCAL_LIFE = "local-life";
  public final String CLUBS_AND_SOCIETIES = "clubs-and-societies";
  public final String SOCIETIES_AND_SPORTS = "societies-and-sports";
  public final String STUDENT_SUPPORT = "student-support";
  public final String GIVING_BACK = "giving-back";
  public final String POSTGRADUATE = "postgraduate";
  public final String INTERNATIONAL = "international";
  public final String INDEPENDENT_HIGHER_EDUCATION = "independent-higher-education";
  public final String FURTHER_EDUCATION_COLLEGES = "further-education-college";
  
  public final String UNIVERSITY_OF_THE_YEAR_ID = "4";
  public final String AWD_JOB_PROSPECTS_ID = "12";
  public final String COURSE_AND_LECTURERS_ID = "9";
  public final String STUDENT_UNION_ID = "5";
  public final String ACCOMMODATION_ID = "7";
  public final String UNI_FACILITIES_ID = "8";
  public final String CITY_LIFE_ID = "11";
  public final String CLUBS_AND_SOCIETIES_ID = "6";
  public final String STUDENT_SUPPORT_ID = "41";
  public final String GIVING_BACK_ID = "46";
  public final String POSTGRADUATE_ID = "40";
  public final String INTERNATIONAL_ID = "10";
  public final String INDEPENDENT_HIGHER_EDUCATION_ID = "48";
  public final String FURTHER_EDUCATION_COLLEGES_ID = "49";
  public final String AWARDS_YEAR_LIST = "2012, 2014, 2015, 2016, 2017, 2018, 2019";
  //End
  public final String PG_NETWORK_ID = "3";
  public final String[] mobileUesrAgent = { "midp", "j2me", "avantg", "docomo", "novarra", "palmos", "palmsource", "240x320", "opwv", 
                                 "chtml", "pda", "windows ce", "mmp/", "blackberry", "mib/", "symbian", "wireless", "nokia", 
                                 "hand", "mobi", "phone", "cdm", "up.b", "audio", "SIE-", "SEC-", "samsung", "HTC", "mot-", 
                                 "mitsu", "sagem", "sony", "alcatel", "lg", "erics", "vx", "NEC", "philips", "mmm", "xx", 
                                 "panasonic", "sharp", "wap", "sch", "rover", "pocket", "benq", "java", "pt", "pg", "vox", 
                                 "amoi", "bird", "compal", "kg", "voda", "sany", "kdd", "dbt", "sendo", "sgh", "gradi", "jb", 
                                 "moto", "webos" }; 
  public final String timeToLoginUrl = "/mywhatuni.html";
  
  public final String DEFAULT_UNI_IMG_PATH = "/wu-cont/images/f5_def_img.png";
  public final String CHECK_USER_STATUS_FN = "wu_user_profile_pkg.check_user_status_fn"; //Added by Thiyagu G to check the user status on 28_Jun_2016
  public final String CHECK_USER_AUTHENTICATE_FN = "authenticate_fn"; //Added by Thiyagu G to check the user authentication on 28_Jun_2016
  public final String CHECK_EMAIL_EXISTS_FN = "wu_util_pkg.check_email_exists"; //Added by Thiyagu G to check email exists on 28_Jun_2016
  public final String USER_YEAR_OF_ENTRY_FN = "wu_util_pkg.get_user_year_of_entry_fn"; //Added by Thiyagu G to get the user year of entry on 28_Jun_2016
  public final String GET_YEAR_OF_ENTRY_FN = "HOT_WUNI.GET_YEAR_OF_ENTRY_FN";
  
  public final String GET_USER_NAME_FN = "wu_review_pkg.get_user_name_fn"; //Added by Prabha for getting user name on 28_Jun_2016
  public final String GET_FIRST_LAST_NAME_FN = "WU_UTIL_PKG.get_first_last_name_fn";
  public final String GET_USER_INFO_FN = "wu_gmap_pkg.get_user_info_fn"; //Added by Prabha for gettting user info on 28_Jun_2016
  public final String PRE_LOGIN_STATUS_CODE_FN = "wu_reviews_pkg.upd_prelogin_status_code_fn"; //Added by Prabha for update login user status on 28_jun_2016
  public final String WU_SYSVAR_VALUE_FN ="wu_util_pkg.get_wu_sysvar_value_fn"; //Added by Prabha for geeting wu sys var value function on 28_Jnu_2016  
  public final String WU_SUBJECT_LIST_FN ="WU_COURSE_SEARCH_PKG.GET_SUBJECT_LIST_FN"; //Added by Prabha for geeting wu sys var value function on 28_Jnu_2016
  public final String GET_BASKET_COUNT_FN = "Wu_basket_Pkg.get_basket_count_fn"; //Added by Thiyagu G to check email exists on 28_Jun_2016
  
  // 
  public final String RESET_PASSWORD_PRC = "WU_USER_PROFILE_PKG.RESET_PASSWORD_PRC"; //Added by Thiyagu G to add a ca track flag for CATOOL.
  public final String UPDATE_CA_TRACK_FLAG_PRC = "WU_USER_PROFILE_PKG.UPDATE_CA_TRACK_FLAG_PRC"; //Added by Thiyagu G to update a ca track flag for CATOOL. 
  
  //
  public final String GET_COURSE_SCHEMA_FN = "HOT_WUNI.WU_COURSE_DETAILS_PKG.GET_COURSE_SCHEMA_DETAIL_FN"; //Added by Prabha for getting schema tag info on 24_Jan_2017
  
  public final String GET_NEW_COLLEGE_NAME_PRC = "WU_UTIL_PKG.GET_NEW_COLLEGE_NAME_PRC"; //To get new college details details for 24_Jan_2017, By Thiyagu G.
  
  public static final String CHECK_ARTICLE_AVILABLE_FN = "WUNI_ARTICLES_PKG.CHECK_ARTICLE_AVILABLE_FN"; //
  
  public final String GET_COURSE_TITLE_FN = "WU_UTIL_PKG.get_course_title_fn"; //Added new function on to get course name 16_May_2017, By Thiyagu G.
  
  public final String GET_COURSE_DETAILS_PRC = "WU_COURSE_DETAILS_PKG.GET_COURSE_DETAILS_PAGE_PRC"; //Added course detail page prc by Prabha on 11_Jul_17
  
  public final String GET_APP_LANDING_PAGE_PRC = "APP_LANDING_PAGE_PKG.ins_app_landing_page_user_prc"; //Added ALP prc by Prabha on 08_Aug_17
  
  public final String GET_ADVANCE_SEARCH_RESULTS_PRC = "HOT_WUNI.WU_ADVANCE_SEARCH_PKG.GET_COURSE_SEARCH_PRC";
  
  //Added Advance search page (Interstitial) below 3 prcs by Sabapathi.S on 28_Aug_18
  public final String GET_INTERSTITIAL_SEARCH_PRC = "HOT_WUNI.WHATUNI_SEARCH_PKG.INTERSTITIAL_PAGE_PRC";
  
  public final String GET_INTERSTITIAL_COURSE_COUNT_PRC = "whatuni_search_pkg.get_adv_search_course_cnt_prc";// "HOT_WUNI.WHATUNI_SEARCH_PKG.GET_COURSE_COUNT_PRC";
  
  public final String GET_SUBJECT_AJAX_FN = "HOT_WUNI.WHATUNI_SEARCH_PKG.GET_SUBJECT_AJAX_FN";
  
  public final String GET_ADVANCE_PROVIDER_RESULTS_PRC = "HOT_WUNI.WU_ADVANCE_SEARCH_PKG.GET_ADVANCE_PR_PAGE_PRC";

  public final String GET_OPENDAYS_DETAILS_PRC = "HOT_WUNI.WUNI_CONTENTHUB_PKG.GET_OPENDAYS_DETAILS_PRC";

  public final String CHATBOT_ACCESS_TOKEN = "246";
  //Added to get the user review for displaying the profile light box, 18_Dec_2018 By Jeyalakshmi
  public final String GET_VIEW_REVIEW_FN = "HOT_WUNI.WU_REVIEW_PKG.GET_VIEW_REVIEW_FN";
  public final String GET_DB_NAME_FN = "HOT_WUNI.WU_UTIL_PKG.GET_DB_NAME_FN";
  
  public final String WU_WEB_SERVICE_SCHEME_NAME = "http://";
  public final String QUIZ_SEPARATOR = "###SEPARATOR###";
  public final String QUIZ_QUESTION_SEP = "###QUESTION###";
  public final String QUIZ_ANSWER_SEP = "###ANSWEROPTION###";
  public final String QUIZ_PREV_QUAL_SEP = "###PREVIOUSQUAL###";
  public final String QUIZ_MOBILE_SKIP_SEP = "###MOBILESKIP###";
  public final String QUIZ_SELECTED_URL_SEP = "###SELECTEDURL###"; //Added for chatbot by Sabapathi on 20_Nov_2018
  public static final String CONTENT_HUB_PAGE_NAME = "CONTENT_HUB_PROFILE_PAGE";
  public static final String CONTENT_HUB_PAGE_GA = "CONTENT_HUB_PROFILE_PAGE";
  public static final String CONTENT_HUB_PAGE_INSIGHTS = "CONTENT_HUB_PROFILE_PAGE";
  public static final String _170PX = "_170px";
  public static final String _500PX = "_500px";
  public static final String _768PX = "_768px";
  public static final String _320PX = "_320px";
  public static final String _580PX = "_580px";
  public static final String _307PX = "_307px";
  public static final String _225JPG = "_225";
  public static final String _290JPG = "_290";
  public static final String _322JPG = "_322";
  public static final String TERMS_AND_COND_VER = "TERMS_AND_CONDITIONS";
  public static final String PRIVACY_VER = "PRIVACY_POLICY";
  public static final String INTERNATIONAL_FEES = "INTERNATIONAL";
  public static final String CHANNEL_ISLAND_FEES = "CHANNEL ISLANDS";
  public static final String DOMESTIC = "DOMESTIC";
  public static final String OTHER_UK = "OTHER UK";
  public static final String NORTHERN_IRELAND_FEES = "NORTHERN IRELAND";
  public static final String CLEARING_NAV_TXT = "Degrees";//changed from Clearing/Adjustment to Degrees for wugo UI change in 30_5_19
  public static final String CLEARING_PROFILE_TAB_TXT = "Clearing / Adjustment";
  public static final String EU_FEES = "EU";
  public static final String SLASH = "/";
  public static final String GET_USER_DOWNLOAD_PDF = "HOT_WUNI.BUILD_USER_PDF_PKG.BUILD_USER_DATA_PRC";
  public static final String ON = "ON";
  public static final String OFF = "OFF";
  public static final String HYPHEN = "-";
  public static final String UK_DOMESTIC_EMAIL = "ukdomestic@idp-connect.co.in";
  public static final String SALES_EMAIL = "sales@idp-connect.com";
  public static final String WHATUNI_TEXT = "Whatuni";
  public static final String TRUE = "true";
  
  public static final String CMMT_SIGN_IN_URL = WHATUNI_SCHEME_NAME+ WHATUNI_DOMAIN + WU_CONTEXT_PATH + "/wugo/apply-now.html?pagename=signin";
  public static final String CMMT_SIGN_UP_URL = WHATUNI_SCHEME_NAME+ WHATUNI_DOMAIN + WU_CONTEXT_PATH + "/wugo/apply-now.html?pagename=signup";
  public static final String FORGOT_PASSWORD_URL = WHATUNI_SCHEME_NAME+ WHATUNI_DOMAIN + WU_CONTEXT_PATH + "/forgotPassword.html";
  public static final String CLEARING_MATCH_MAKER_URL = "/degrees/clearing-match-maker-tool.html";
  public static final String DEGREE_STUDY_LEVEL = "degrees";
  public static final String CLEARING_PAGE_FLAG = "CLEARING";
  public static final String WU_GO_ACCESS_TOKEN = "19B53458C241E277D985CA0A6A25845630760279";
  public static final String AN_USER_ID_COOKIE = "AN_USER_ID";
  public static final String CLEARING_USER_TYPE_COOKIE = "CLEARING_USER_TYPE"; // constant for CLEARING_USER_TYPE cookie name
  public static final String COOKIE_SPLASH_FLAG_COOKIE = "cookie_splash_flag";
  //NOTE : CLEARING_USER_TYPE cookie and USER_TYPE session values are maintained same.
  public static final String CLEARING_USER_TYPE_VAL = "CLEARING"; // this constant value is used for session(USER_TYPE) and cookie(CLEARING_USER_TYPE) values in clearing mode
  public static final String NON_CLEARING_USER_TYPE_VAL = "NON_CLEARING"; // this constant value is used for session(USER_TYPE) and cookie(CLEARING_USER_TYPE) values in non clearing mode
  public static final String WUGO_CONTEXT_PATH = "/wugo";
  public static final String WU_CONT = "/wu-cont";
  //
  
  public static final String WU_LAT = "51.468555";
  public static final String WU_LONG = "-0.209233";
  public static final String VIEW_GOOGLE_MAP = "View Google maps";
  public static final String MAPBOX_API_KEY = "pk.eyJ1IjoiaG90Y291cnNlc2ludGwiLCJhIjoiY2s2MjFkeHlxMDhwMDN0cXd2cTlqb3dlZiJ9.L-TXEMvZMFKb5WfkuFfMEA";
  public static final String MAPBOX_SCRIPT = "https://api.tiles.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.js";
  public static final String MAPBOX_CSS = "https://api.tiles.mapbox.com/mapbox-gl-js/v0.51.0/mapbox-gl.css";
  public static final String MAPBOX_STYLE = "mapbox://styles/mapbox/streets-v10";
  public static final String ERROR = "ERROR";
  public static final String AWARDS_PAGE_URL = "/student-awards-winners/university-of-the-year/";
  public final String ONE_PIXEL_IMG_PATH = "/wu-cont/images/img_px.gif";
  public static final String COOKIE_DATE_FORMAT = "dd-MM-yyyy HH:mm:ss";
  public static final String GRADE_POPUP_COOKIE = "gradePopup";
  public static final String WCACHE_COOKIE = "wcache";
  public static final String PROSPECTUS_BASKET_ID = "prospectusBasketId";
  public static final String SEARCH_POD_PLACEHOLDER_TEXT = "Search for courses, universities, advice";
  public static final String DEFAULT_QUALIFICATION_TEXT = "Undergraduate";
  
  //Added for featured slot in SR page - Jeyalakshmi D for 06 Jan 2020 release
  public static final String FEATURED_INTERNAL_PROVIDER = "HOTCOURSES: FEATURED PROVIDER: INTERNAL LINK";
  public static final String FEATURED_EXTERNAL_PROVIDER = "HOTCOURSES: FEATURED PROVIDER: EXTERNAL LINK";
  public static final String FEATURED_VIDEO = "HOTCOURSES: VIDEO VIEW";
  public static final String FEATURED_PAGE_VIEW = "HOTCOURSES: FEATURED PROVIDER: BOOSTING";
  
  public final String MDEV_DOMAIN = "mdev.whatuni.com";
  public final String MTEST_DOMAIN = "mtest.whatuni.com";
  public final String LIVE_DOMAIN = "www.whatuni.com";
  public final String LAZY_LOAD_FLAG = "lazy_not_done";
  
  public final String NORMAL_JOURNEY = "NORMAL";
  public final String CLEARING_JOURNEY = "CLEARING";
  
  public static final String CPC_HERO_IMGSIZE = "_1500px";
  public static final String CPC_HERO_MOB_IMGSIZE = "_414px";
  public static final String CPC_MOB_IMGSIZE = "_343px";
  public static final String CPC_TAB_IMGSIZE = "_232px";
  public static final String CPC_DESKTOP_IMGSIZE = "_355px";
  
  public static final String INVALID_CAMPAIGN_ID = "--(1.1)--> URL doesn't have valid campaign ID";
  public static final String ERROR_MSG_404 = "404 error message";
  public static final String REASON_FOR_404 = "reason_for_404";
  public static final String WU_LOGO_URL = "/wu-cont/images/logo_print.png";
  
  //Study level for enter grade pod
  public static final String UNDERGRADUATE = "Undergraduate";
  public static final String HND_HNC = "HND/HNC";
  public static final String FOUNDATION_DEGREE = "Foundation degree";
  public static final String ACCESS_FOUNDATION = "Access & foundation";
  public static final String ALL = "All";
  //
  
  public static final String HESA_TOOLTIP_TEXT = "Source: <a href=\\\"/degrees/jsp/search/kisdataStatic.jsp\\\">HESA</a>, 2019";
  public static final String SESSION_KEY_LAT_LONG = "LAT_LONG";
  
  public static final String COVID19_SYSVAR = "COVID19_SYSVAR";
  public static final String COVID19_ON_OFF = "ON";
  public static final String COVID_19_ARTICLE_PAGE = "Due to coronavirus (COVID-19) the information on this page is subject to change.";
  public static final String COVID_19_OPENDAY_PAGE = "Due to Coronavirus (COVID 19) university open day events will be moving to virtual tours until further notice.";
  public static final String COVID_19_SR_PR_CD_PROFILE = "University course intakes may be affected by Coronavirus (COVID -19).";
  public static final String COMMON_COVID_19_HEADER = "Coronavirus (COVID-19) Latest news";
  //for open day event type constants and open day GA values 
  public static final String VIRTUAL_EVENT_TYPE = "Virtual Tour";
  public static final String PHYSICAL_EVENT_TYPE = "University Open Day";
  public static final String ONLINE_EVENT_TYPE = "Online Events";
  public static final String VIRTUAL_GA_TYPE = "Virtual";
  public static final String PHYSICAL_GA_TYPE = "Physical";
  public static final String ONLINE_GA_TYPE = "Online";
  //
  public static final String CLEARING_NETWORK_ID = "7";
  public static final String CLEARING_API_CONTEXT_PATH = "/clearing-api";
  public static final String CLEARING_SWITCH_PAGES = "browseMoneyPageResults.jsp,courseSearchResult.jsp,clearingBrowseMoneyPage.jsp,clearingCourseSearchResult.jsp,clearingSearchResults.jsp,clearingProviderResults.jsp,courseDetails.jsp,contenthub.jsp,newProviderHome.jsp,richProfileLanding.jsp,clearingProfile.jsp";

  public static final String LIKELY_MATCH_TYPE = "Very Likely";
  public static final String STRETCH_MATCH_TYPE = "Possible";
  public static final String POSSIBLE_MATCH_TYPE = "Likely";
  
  public static final String USER_TRACK_ID_COOKIE = "userTrackId";
  public static final String COOKIE_CONSENT_COOKIE = "cookieconsent";
  public static final String CONSENT_COOKIE_DEFAULT_VAL = "0000";  
  public static final int COOKIE_CONSENT__COOKIE_MAX_AGE =  2592000;//30 days cookie
  public static final String NUMBER_ONE = "1";
  public static final String ZERO = "0";
  public static final String FLAG_Y = "Y";
  public static final String FLAG_N = "N";
  public static final String STRICTLY_NECESSARY_COOKIES = "prospectusBasketId,cookie_splash_flag,CLEARING_USER_TYPE,basketId,JSESSIONID,cookieconsent";
  public static final String FUNCTIONAL_COOKIES = "gradePopup,userType,wcache,__wuuid,DS_WC_TO_COM,userTrackId,AN_USER_ID";
  public static final String PERFORMANCE_COOKIE = "";
  public static final String TARGETING_COOKIE = "";
  public static final String SESSION_KEY_USER_UCAS_SCORE = "USER_UCAS_SCORE"; //Added for tariff-logging-ucas-point by Sri Sankari on Sep 2020 release

  public static final String PROFILE_UG_TAB = "Undergraduate";
  public static final String PROFILE_PG_TAB = "Postgraduate";
  public static final String PROFILE_CLEARING_TAB = "Clearing 2020";
  public static final String CLEARING_PROFILE = "CLEARING_PROFILE";
  public static final String FALSE = "false";
  public static final String SUBJECT = "Subject";
}


//////////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////////////