package WUI.utilities;

import org.apache.struts.action.ActionForm;

public class OmnitureProperties {

  public OmnitureProperties() {
  }
  
  String sc_pageno = "";
  String prob40_network = "";
  String prob42_keyword = "";
  String prob43_cpe_category = "";
  String prob44_location = "";
  String prob45_spareSlot = "";

  public void init(){
    sc_pageno = "";
    prob40_network = "";
    prob42_keyword = "";
    prob43_cpe_category = "";
    prob44_location = "";
    prob45_spareSlot = "";
  }
  
  public void flush(){
    sc_pageno = null;
    prob40_network = null;
    prob42_keyword = null;
    prob43_cpe_category = null;
    prob44_location = null;
    prob45_spareSlot = null;
  }

  public void setSc_pageno(String sc_pageno) {
    this.sc_pageno = sc_pageno;
  }

  public String getSc_pageno() {
    return sc_pageno;
  }

  public void setProb40_network(String prob40_network) {
    this.prob40_network = prob40_network;
  }

  public String getProb40_network() {
    return prob40_network;
  }

  public void setProb42_keyword(String prob42_keyword) {
    this.prob42_keyword = prob42_keyword;
  }

  public String getProb42_keyword() {
    return prob42_keyword;
  }

  public void setProb43_cpe_category(String prob43_cpe_category) {
    this.prob43_cpe_category = prob43_cpe_category;
  }

  public String getProb43_cpe_category() {
    return prob43_cpe_category;
  }

  public void setProb44_location(String prob44_location) {
    this.prob44_location = prob44_location;
  }

  public String getProb44_location() {
    return prob44_location;
  }

  public void setProb45_spareSlot(String prob45_spareSlot) {
    this.prob45_spareSlot = prob45_spareSlot;
  }

  public String getProb45_spareSlot() {
    return prob45_spareSlot;
  }

}
