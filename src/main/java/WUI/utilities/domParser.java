package WUI.utilities;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
  * @domParser.java
  * @Version : 2.0
  * @April 20 2007
  * @www.whatuni.com
  * @author By : Balraj. Selva Kumar
  * @Purpose  : This program is used to parse the XML tags.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class domParser {
  public domParser() {
  }
  Document dom;

  /**
    *   This fuction is used to read the XML content from XML file.
    * @param searchText
    * @return XML content as String
    * @throws Exception
    */
    
  public String getXMLContent(String searchText) throws Exception {
    String yahooAPI = "http://api.local.yahoo.com/MapsService/V1/geocode?" + "appid=mapmap.org&location=" + searchText;
    try {
      URL url = new URL(yahooAPI);
      URLConnection uconnection = url.openConnection();
      String inputLine;
      String inputLine2 = "";
      BufferedReader inp = new BufferedReader(new InputStreamReader(uconnection.getInputStream()));
      while ((inputLine = inp.readLine()) != null) {
        inputLine2 += inputLine;
      }
      inp.close();
      return inputLine2;
    } catch (Exception e) {
      e.printStackTrace();
      return "";
    }
  }

  /**
    *   This function is used to parse the XML content.
    * @param stringXML
    * @return Parsed XML content as Document
    */
    
  public Document parseXmlFile(String stringXML) {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    try {
      DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
      String strXML = stringXML;
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document document = builder.parse(new InputSource(new StringReader(strXML)));
      Document dom = document;
      return dom;
    } catch (Exception exception) {
      exception.printStackTrace();
      return null;
    }
  }

  /**
    *   This function is used to parse the Document into List
    * @param dom
    * @param parentTag
    * @param childTag
    * @return Parsed document as List
    */
    
  public ArrayList parseDocument(Document dom, String parentTag, String childTag) {
    Element docEle = dom.getDocumentElement();
    NodeList nl = docEle.getElementsByTagName(parentTag);
    Element rootElement = dom.getDocumentElement();
    ArrayList dataList = new ArrayList();
    dataList.clear();
    if (nl != null && nl.getLength() > 0) {
      for (int i = 0; i < nl.getLength(); i++) {
        Element el = (Element)nl.item(i);
        String name = getTextValue(el, childTag);
        if (name != null)
          dataList.add(name);
      }
    }
    return dataList;
  }

  /**
    *   This function is uead to get the node value from the given XML element
    * @param ele
    * @param tagName
    * @return Node value as String.
    */
    
  private String getTextValue(Element ele, String tagName) {
    String textVal = null;
    NodeList nl = ele.getElementsByTagName(tagName);
    if (nl != null && nl.getLength() > 0) {
      Element el = (Element)nl.item(0);
      textVal = el.getFirstChild().getNodeValue();
    }
    return textVal;
  }

  /**
    *   This function is used to Parse the body content
    * @param XMLContent
    * @param begin_tag
    * @param end_tag
    * @return body content as String.
    * @throws Exception
    */
    
  public String getBodyContent(String XMLContent, String begin_tag, String end_tag) throws Exception {
    String returnData = "";
    try {
      String inputLine2 = XMLContent;
      returnData = inputLine2.substring(inputLine2.indexOf(begin_tag) + begin_tag.length(), inputLine2.indexOf(end_tag));
      return returnData;
    } catch (Exception e) {
      return returnData;
    }
  }
}
