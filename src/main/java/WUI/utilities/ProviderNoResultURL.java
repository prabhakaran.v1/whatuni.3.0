package WUI.utilities;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import WUI.search.form.SearchBean;

import com.wuni.util.sql.DataModel;

/**
   * @ProviderNoResultURL.java
   * @Version : 2.0
   * @www.whatuni.com
   * @author By : Balraj. Selva Kumar
   * @Purpose  : Program used to find the Collegs which has no result.
   * *************************************************************************************************************************
   * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
   * *************************************************************************************************************************
   *
   */
public class ProviderNoResultURL {
  public ProviderNoResultURL() {
  }
  int count = 0;
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
    List categorylist = AllCollegesRandomeCagtegoryList();
    Iterator categoryiterator = categorylist.iterator();
    java.util.ResourceBundle rbundlle = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources");
    String applicationpath = GlobalConstants.WHATUNI_SCHEME_NAME + rbundlle.getString("review.autocomplete.ip"); //Changed WU scheme as part of SSL work for 08_MAR_2016, By Thiyagu G.
    new LogFile().writeLogFile("Started");
    while (categoryiterator.hasNext()) {
      SearchBean bean = (SearchBean)categoryiterator.next();
      String urlString = bean.getHyperLinkUrl();
      String urlArray[] = urlString.split("/");
      try {
        String search_category = urlArray[4] != null && !urlArray[4].trim().equals("0")? new CommonUtil().toUpperCase(urlArray[4].trim()): "";
        String qualification = urlArray[5] != null && !urlArray[5].trim().equals("0")? new CommonUtil().toUpperCase(urlArray[5].trim()): "";
        String study_mode = urlArray[6] != null && !urlArray[6].trim().equals("0")? new CommonUtil().toUpperCase(urlArray[6].trim()): "";
        String postcode = urlArray[7] != null && !urlArray[7].trim().equals("0")? new CommonFunction().replacePlus(new CommonUtil().toUpperCase(urlArray[7].trim())): "";
        String z = urlArray[8] != null && !urlArray[8].trim().equals("0")? urlArray[8].trim(): "";
        String a = GlobalConstants.WHATUNI_AFFILATE_ID;
        String search_how = "V";
        String entityText = "1";
        String user_agent = request.getHeader("user-agent");
        //Changed WU scheme as part of SSL work for 08_MAR_2016, By Thiyagu G.
        String requestURL = GlobalConstants.WHATUNI_SCHEME_NAME+ResourceBundle.getBundle("com.resources.ApplicationResources").getString("review.autocomplete.ip") + request.getRequestURI(); //
        String headerId = getCourseSearchResult("O", "", "", qualification, "", "", postcode, "25", "", "", study_mode, "", "", a, "", search_how, z, "", search_category, "", "", "", "zone", "", "search_text", "", "", entityText, request, "PROVIDER", user_agent, requestURL);
        List courseList = new CommonFunction().getCourseList(headerId, "1", request);
        if (courseList != null && courseList.size() == 0) {
          new LogFile().writeLogFile(applicationpath + request.getContextPath() + urlString);
        }
      } catch (Exception exception) {
        new LogFile().writeLogFile(urlString);
      }
    }
    new LogFile().writeLogFile("completed");
    return mapping.findForward("null");
  }

  /**
    *   This function is used to read all the rendom category list for all the providers.
    * @return Random Category provider list as Collection object.
    */
    
  private List AllCollegesRandomeCagtegoryList() {
    DataModel dataModel = new DataModel();
    ResultSet rs = null;
    SearchBean bean = null;
    ArrayList list = new ArrayList();
    try {
      Vector vector = new Vector();
      vector.clear();
      rs = dataModel.getResultSet("get_uni_rand_cat_temp_fn", vector);
      while (rs.next()) {
        bean = new SearchBean();
        bean.setCollegeId(rs.getString("college_id"));
        bean.setSubjectId(rs.getString("category_code"));
        bean.setSeoStudyLevel(rs.getString("seo_qual_desc"));
        bean.setStudyLevelId(rs.getString("qual_code"));
        String categoryName = rs.getString("category_desc");
        if (categoryName != null && !categoryName.equals("null") && categoryName.trim().length() > 0 && categoryName.indexOf("#") >= 0) {
          categoryName = new CommonFunction().replaceHypen(new CommonFunction().replaceURL(categoryName.replaceAll("#", "")));
        } else {
          categoryName = new CommonFunction().replaceHypen(new CommonFunction().replaceURL(rs.getString("category_desc")));
        }
        bean.setSubjectName(rs.getString("category_desc"));
        String url = "/courses/" + rs.getString("seo_qual_desc");
        url = url + "/" + categoryName + "-courses-at-" + new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(rs.getString("college_name"))));
        url = url + "/" + rs.getString("category_code");
        url = url + "/" + rs.getString("qual_code");
        url = url + "/0";
        url = url + "/" + new CommonFunction().replaceSpacePlus(rs.getString("location"));
        url = url + "/" + rs.getString("college_id");
        url = url + "/csearch.html";
        url = new CommonUtil().toLowerCase(url);
        bean.setHyperLinkUrl(url);
        list.add(bean);
      }
    } catch (Exception exception) {
    }
    return list;
  }

  /**
    *   This function is used to build the search result for the provider based on all criteria.
    * @param search_what
    * @param phrase_search
    * @param college_name
    * @param qualification
    * @param country
    * @param town_city
    * @param postcode
    * @param search_range
    * @param postflag
    * @param location_id
    * @param study_mode
    * @param course_duration
    * @param submit
    * @param a
    * @param aui
    * @param search_how
    * @param z
    * @param s_type
    * @param search_category
    * @param search_career
    * @param career_id
    * @param nrp
    * @param area
    * @param p_pg_type
    * @param p_search_col
    * @param p_county_id
    * @param p_pheader_id
    * @param p_entity_text
    * @param request
    * @param search_from
    * @param bot_flag
    * @param search_url
    * @return search headerid as String
    */
    
  private String getCourseSearchResult(String search_what, String phrase_search, String college_name, String qualification, String country, String town_city, String postcode, String search_range, String postflag, String location_id, String study_mode, String course_duration, String submit, String a, String aui, String search_how, String z, String s_type, String search_category, String search_career, String career_id, String nrp, String area, String p_pg_type, String p_search_col, String p_county_id, String p_pheader_id, String p_entity_text, HttpServletRequest request, String search_from, //included for 28th release 
    String bot_flag, //included for 28th release 
    String search_url) {
    DataModel dataModel = new DataModel();
    Vector searchVector = new Vector();
    searchVector.add(new SessionData().getData(request, "x"));
    searchVector.add(new SessionData().getData(request, "y"));
    searchVector.add(search_what);
    searchVector.add(phrase_search);
    searchVector.add(college_name);
    searchVector.add(qualification);
    searchVector.add(country);
    searchVector.add(town_city);
    searchVector.add(postcode);
    searchVector.add(search_range);
    searchVector.add(postflag);
    searchVector.add(location_id);
    searchVector.add(study_mode);
    searchVector.add(course_duration);
    searchVector.add(submit);
    searchVector.add(a);
    searchVector.add(aui);
    searchVector.add(search_how);
    searchVector.add(z);
    searchVector.add(s_type);
    searchVector.add(search_category);
    searchVector.add(search_career);
    searchVector.add(career_id);
    searchVector.add(nrp);
    searchVector.add(area);
    searchVector.add(p_pg_type);
    searchVector.add(p_search_col);
    searchVector.add(p_county_id);
    searchVector.add(p_pheader_id);
    searchVector.add(p_entity_text);
    searchVector.add(search_from); //included for 28th release 
    searchVector.add(bot_flag); //included for 28th release 
    searchVector.add(search_url); //included for 28th release 
    String headerId = "";
    try {
      headerId = dataModel.getString("whatuni_search.adv_search_do", searchVector);
    } catch (Exception searchException) {
      searchException.printStackTrace();
    }
    return headerId;
  }
}     
    
//http://192.168.1.38:8988/degrees/noresult.html?s=M