package WUI.utilities;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
//import java.io.FileOutputStream;
import java.io.IOException;
//import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.ImageIcon;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionForm;
import org.springframework.stereotype.Component;

import spring.form.LocationUniBrowseBean;
import spring.valueobject.common.YearOfEntryVO;
import WUI.basket.form.BasketBean;
import WUI.homepage.form.HomePageBean;
import WUI.members.form.MembersSearchBean;
import WUI.message.form.FriendsBean;
import WUI.message.form.MessageBean;
import WUI.profile.bean.ProfileBean;
import WUI.registration.bean.RegistrationBean;
import WUI.review.bean.ReviewListBean;
import WUI.search.form.CollegeProfileBean;
import WUI.search.form.CourseDetailsBean;
import WUI.search.form.GooglemapBean;
import WUI.search.form.SearchBean;
import WUI.search.form.SearchResultBean;
import WUI.security.SecurityEvaluator;
import WUI.videoreview.bean.VideoReviewListBean;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.business.IHomeBusiness;
import com.layer.util.SpringConstants;
//import com.sun.image.codec.jpeg.JPEGCodec;
//import com.sun.image.codec.jpeg.JPEGImageEncoder;
import com.wuni.advertiser.prospectus.form.ProspectusBean;
import com.wuni.myfinalchoice.form.MyFinalChoiceBean;
import com.wuni.util.api.yahoo.YahooPlaceFinder;
import com.wuni.util.form.opendays.OpenDaysBean;
import com.wuni.util.form.studentawards.StudentAwardsBean;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.CollegeCpeInteractionDetailsWithMediaVO;
import com.wuni.util.valueobject.openday.OpendaysVO;
import com.wuni.util.valueobject.review.UniOverallReviewsVO;
import com.wuni.util.valueobject.search.CourseDetailsVO;
import com.wuni.valueobjects.BrowseCatfor404pageVO;
import com.wuni.valueobjects.whatunigo.ApplyNowVO;
import com.wuni.whatunigo.WhatuniGoController;

/**
  * @CommonFunction.java
  * @Version : 2.0
  * @Januaru 1 2007
  * @www.whatuni.com
  * @author By : Balraj. Selva Kumar
  * @Purpose  : Program contains lot of functions that are shared among the dirrerent programs
  * *************************************************************************************************************************************
  * Author              Release                   Modification Details                         Change
  * *************************************************************************************************************************************
  * Karthi              WU_537_20150224           Added two functions
  * Sabapathi S.        wu_582_20181023           Added screen width in stats logging        
  * Sangeeth.S		    wu_300320			  	  Added lat and long for stats logging
  * Kailash L           18-Aug-2020               Modified the deprecated JPEGImageEncoder to ImageIO
  * Sujitha V           10_NOV_2020               Added session for YOE to log in d_session_log.
  */
@Component
public class CommonFunction  
{
    public CommonFunction()  {  }

  /**
   *  function that checks the string contains any swearing word and all the words are capitan and return error status
   * @param request, searchstring
   * @return Error as String
   */
  public String checkCapsSwearing(HttpServletRequest request, String searchString) {
    HttpSession session = request.getSession();
    DataModel dataModel = new DataModel();
    String searchText = searchString;
    Vector vector = new Vector();
    String returnError = "";
    vector.clear();
    vector.add(searchText);
    try {
      session = request.getSession();
      returnError = dataModel.getString("wu_util_pkg.Swearing_Check_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return returnError;
  }

  /**
   * 
   * function will return location of the college.
   * 
   * @param collegeId
   * @return
   */
  public String getCollegeLocation(String collegeId) {
    String collegeLocation = "";
    DataModel dataModel = new DataModel();
    Vector parameters = new Vector();      
    parameters.add(collegeId);       
    try {
      collegeLocation = dataModel.getString("WU_GMAP_PKG.GET_COLLEGE_TOWN_FN", parameters);
    } catch(Exception exception) {
      exception.printStackTrace();
    } finally{
      dataModel = null;
      parameters.clear();
      parameters = null;
    }
    return collegeLocation;
   }

  /**
    *  Function that returns the collge Name for the given college_id
    * @param collegeId, request
    * @return college name and String
    */
  public String getCollegeName(String collegeId, HttpServletRequest request) {
    String collegeName = "";
    HttpSession session = request.getSession();
    if (session.getAttribute("collegeName") != null) {
      session.removeAttribute("collegeName");
    }
    try {
      DataModel dataModel = new DataModel();
      Vector vector = new Vector();
      vector.clear();
      vector.add((collegeId != null && !collegeId.equals("null") && collegeId.trim().length() > 0 ? collegeId.trim() : ""));
      collegeName = dataModel.getString("GET_COLLEGE_TITLE_FN", vector);
      if (collegeName != null && collegeName.length() > 0) {
        session.setAttribute("collegeName", collegeName);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return collegeName;
  }
    
    
  /**
   *  Function that returns the collge Name for the given college_id
   * @param collegeId, request
   * @return college name and String
   */
  
   public String getCollegeTitleFunction(String collegeId) 
   {
       String collegeName      =   "";
       try {
         DataModel dataModel       =   new DataModel();
         Vector vector      =    new Vector();
         vector.clear();
         vector.add((collegeId !=null && !collegeId.equals("null") && collegeId.trim().length()>0 ? collegeId.trim() : ""));       
         collegeName = dataModel.getString("Get_College_Title_Fn", vector);
       } catch(Exception exception) {
           exception.printStackTrace();
       }
      return collegeName;
   }

   /**
      *  Function that returns name of the course for the given course_id and clearing flag
      * @param courseId, request
      * @return course name and String
      */
   public String getCourseName(String courseId, String clearingFlag) {
     String courseName = "";
     try {
       
       ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
       CourseDetailsBean courseDetailsBean = new CourseDetailsBean();
       courseDetailsBean.setCourseId(!GenericValidator.isBlankOrNull(courseId) ? courseId : "0");
       courseDetailsBean.setClearingFlag(clearingFlag);
       Map courseNameMap = commonBusiness.getCourseName(courseDetailsBean);
       if(courseNameMap != null){
         courseName = (String)courseNameMap.get("RetVal");        
       }
     } catch (Exception exception) {
       exception.printStackTrace();
     }
     return courseName;
   }

   /**
     *  Function that returns name of the course for the given course_id
     * @param collegeId, request
     * @return course name and String
     */
     
    public String getCollegeAdvertisor(String collegeId) 
   {     
      return "TRUE";
   }

    /**
     *  function that returns username for the given userId
     * @param userId, request
     * @return userName as String
     */
      
   public String getUserName(String userId, HttpServletRequest request){
      HttpSession session   =   request.getSession();
      String userName       =   "";
      if(session.getAttribute("showUserName") != null)  {
          session.removeAttribute("showUserName");
      }    
     /* try {
          DataModel dataModel =   new DataModel();
          Vector userDetail   =   new Vector();
          userDetail.clear();
          userDetail.add(userId);
          userName = dataModel.getString("wu_review_pkg.get_user_name_fn", userDetail);
          if(userName !=null && userName.trim().length()>0) {
            session.setAttribute("showUserName", userName);
         }   
      } catch(Exception exception) {
          exception.printStackTrace();
      }*/
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
      RegistrationBean registrationBean = new RegistrationBean();     
      userId = !GenericValidator.isBlankOrNull(userId) ? userId : "0";
      registrationBean.setUserId(userId);
      Map userNameMap = commonBusiness.getUserName(registrationBean);
      if(userNameMap != null){
        userName = (String)userNameMap.get("RetVal");
        if(!GenericValidator.isBlankOrNull(userName)) {
          session.setAttribute("showUserName", userName);
        }
      }
      return userName;      
   }
   
   //
   public String getUserNameDetais(String userId, HttpServletRequest request){
      String userInitial = "";
      String userName = "";
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
      RegistrationBean registrationBean = new RegistrationBean();     
      userId = !GenericValidator.isBlankOrNull(userId) ? userId : "0";
      registrationBean.setUserId(userId);
      Map userNameMap = commonBusiness.getUserNameDetail(registrationBean);
      if(userNameMap != null){
      ArrayList getCourseDurationList = (ArrayList)userNameMap.get("PC_USER_NAME");
      if(getCourseDurationList != null && !getCourseDurationList.isEmpty()){
        Iterator namesItr = getCourseDurationList.iterator();
        while (namesItr.hasNext()) {
          RegistrationBean registrationVO = (RegistrationBean)namesItr.next();
           String firstName = registrationVO.getFirstName();
           String lastName =  registrationVO.getLastName();
           userName = registrationVO.getUserName();
           if(!GenericValidator.isBlankOrNull(firstName)){
             userInitial = firstName.substring(0, 1);
           }
           if(!GenericValidator.isBlankOrNull(lastName)){
             userInitial += lastName.substring(0, 1);
           }
        }
      }
      }
      request.setAttribute("fullName", userName);
      return userInitial;      
   }
  
  /**
   *  function that returns username for the given userId
   * @param userId, request
   * @return userName as String
   */
    
  public String getUserFirstLastName(String userId, HttpServletRequest request) {    
    String userName       =   "";    
    try {
      DataModel dataModel =   new DataModel();
      Vector userDetail   =   new Vector();
      userDetail.clear();
      userDetail.add(userId);
      userName = dataModel.getString("get_user_name_det_fn", userDetail);
    } catch(Exception exception) {
      exception.printStackTrace();
    }
    return userName;      
  }

  /**
   * function that returns username for the given userId
   * @param userId, request
   * @return userName as String
   * 28_OCT_14 added by Amir
   */
    
  public String getUserYearOfEntry(String userId){    
    String yearOfEntry    =   "";    
    /*try {
        DataModel dataModel =   new DataModel();
        Vector userYearDetail   =   new Vector();
        userYearDetail.clear();
        userYearDetail.add(userId);
        yearOfEntry = dataModel.getString("wu_util_pkg.get_user_year_of_entry_fn", userYearDetail);         
    } catch(Exception exception) {
        exception.printStackTrace();
    }*/    
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    RegistrationBean registrationBean = new RegistrationBean();
    registrationBean.setUserId(userId);
    Map checkEmailExistMap = commonBusiness.getUserYearOfEntry(registrationBean);
    yearOfEntry = (String)checkEmailExistMap.get("p_user_year");    
    return yearOfEntry;      
  }
  
  /**
   * function that returns Y when provider is attached to Richprofile
   * @param collegeId, request
   * @param networkId, request   
   * @return userName as String
   * 13_JAN_15 added by Amir
   */
    
  public String isRichProfileProv(String collegeId,String profileType,String myhcProfileId,String networkId)
  {    
    String isRichProfile    =   "";    
    try {
        DataModel dataModel =   new DataModel();
        Vector richProfileDetail   =   new Vector();
        richProfileDetail.clear();
        richProfileDetail.add(collegeId);        
        richProfileDetail.add(profileType);
        richProfileDetail.add(myhcProfileId);
        richProfileDetail.add(networkId);        
        isRichProfile = dataModel.getString("hot_wuni.wuni_profile_pkg.get_college_rp_flag_fn", richProfileDetail);         
    } catch(Exception exception) {
        exception.printStackTrace();
    }
    return isRichProfile;      
  }   
  
  /**
   * function that returns profile type
   * @param collegeId, request
   * @param myhcProfileId, request   
   * @param networkId, request    
   * @return profileType as String
   * 30_JAN_16 added by Sabapathi
   */
    
  public String getProfileType(String collegeId,String myhcProfileId,String networkId)
  {    
    String profileType    =   "";    
    try {
        DataModel dataModel =   new DataModel();
        Vector instDetails   =   new Vector();
        instDetails.clear();
        instDetails.add(collegeId);        
        instDetails.add(myhcProfileId);
        instDetails.add(networkId);        
        profileType = dataModel.getString("hot_wuni.wuni_profile_pkg.get_college_profile_flag_fn", instDetails);         
    } catch(Exception exception) {
        exception.printStackTrace();
    }
    return profileType;      
  }
  /**
   * @see this function for the profile pages separtely to found profile availabilty considering the clearing profile for the 25_june_2020 rel
   * @param collegeId
   * @param myhcProfileId
   * @param networkId
   * @param profileUrl
   * @return
   */
  public String getInstProfileType(String collegeId,String myhcProfileId,String networkId,String profileUrl)
  {    
    String profileType    =   "";    
    try {
        DataModel dataModel =   new DataModel();
        Vector instDetails   =   new Vector();
        instDetails.clear();
        instDetails.add(collegeId);        
        instDetails.add(myhcProfileId);
        instDetails.add(networkId);
        instDetails.add(profileUrl);
        profileType = dataModel.getString("hot_wuni.wuni_profile_pkg.get_college_profile_flag_fn", instDetails);         
    } catch(Exception exception) {
        exception.printStackTrace();
    }
    return profileType;      
  }  

  /**
    *  Function that returns the school Name for the given school_id
    * @param schoolId, request
    * @return school name and String
   */
    
  /*public String getSchoolName(String schoolId, HttpServletRequest request) 
  {
     String schoolName      =   "";
     try {
         DataModel dataModel       =   new DataModel();
         Vector schoolVector      =    new Vector();
         schoolVector.clear();
         schoolVector.add(schoolId);       
         schoolName = dataModel.getString("wu_members_pkg.get_school_name_fn", schoolVector);
     } catch(Exception exception) {
         exception.printStackTrace();
     }
     return schoolName;
  }*/

   /**
    *  Function that the subject name that related to course journey page 
    * @param subjectCategoryId, request
    * @return school name and String
    */
    
   public String getJourneySubjectCategoryName(String subjectCategoryId, String requestValue, HttpServletRequest request) 
   {
      String categoryName = "";
      try {
           DataModel dataModel = new DataModel();
           Vector parameters = new Vector();
           parameters.add(GlobalConstants.WHATUNI_AFFILATE_ID);       
           parameters.add(subjectCategoryId);       
           parameters.add(requestValue); 
           categoryName = dataModel.getString("wu_browse_pkg.get_category_name_fn", parameters);
         } catch(Exception exception) {
            exception.printStackTrace();
         }
         return categoryName;
     }

    /**
     *  Function that the subject name that related to course journey page 
     * @param subjectCategoryId, request
     * @return school name and String
     */
    
     public String getJourneySubjectCategoryCode(String subjectCategoryId, String studyLevel) 
     {
         String categoryCode       =   "";
         try {
           DataModel dataModel     =   new DataModel();
           Vector categoryVector   =    new Vector();
           categoryVector.clear();
           categoryVector.add(GlobalConstants.WHATUNI_AFFILATE_ID);       
           categoryVector.add(studyLevel);       
           categoryVector.add(subjectCategoryId); 
           categoryCode = dataModel.getString("Wu_browse_pkg.get_category_code_fn", categoryVector);
         } catch(Exception exception) {
             exception.printStackTrace();
         }
         return categoryCode;
     }

  /**
     *  Function that returns the random password
     * @param
     * @return college name and String
     */
  public String getRandomPassword() {
    String password = "";
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    vector.clear();
    try {
      password = dataModel.getString("wu_util_pkg.get_random_password", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return password;
  }

  /**
      *  Function that generated the list of video in the specified college/userid
      * @param form, request, servletContext, collegeId, userId, pageNo, recordDisplay orderBy, reviewStatus
      * @return Video list as collection object
      */
  public List generateUserVideoReviewList(ActionForm form, HttpServletRequest request, ServletContext context, String collegeId, String userId, String pageNo, String recordDisplay, String orderBy, String reviewStatus) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    ArrayList list = new ArrayList();
    String limeLightPath = GlobalConstants.LIMELIGHT_VIDEO_PATH;
    try {
      VideoReviewListBean bean = null;
      Vector vector = new Vector();
      vector.clear();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(collegeId); // college id
      vector.add(userId); // userId id
      vector.add(pageNo); // page no to dsplay 
      vector.add(recordDisplay); // No of records to display 
      vector.add(orderBy); // Sorting Order
      vector.add(reviewStatus); // Status of review "A", "P"  "D"
      resultset = dataModel.getResultSet("wu_course_search_pkg.get_video_review_list_fn", vector);
      while (resultset.next()) {
        bean = new VideoReviewListBean();
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
        //bean.setCourseName(resultset.getString("course_name"));
        bean.setUserId(resultset.getString("user_id"));
        bean.setUserName(resultset.getString("user_name"));
        bean.setVideoReviewId(resultset.getString("review_id"));
        bean.setVideoReviewTitle(resultset.getString("review_title"));
        bean.setVideoReviewDesc(resultset.getString("review_desc"));
        bean.setVideoCategory("v_category");
        bean.setNoOfHits(resultset.getString("no_hit"));
        bean.setVideoUrl(resultset.getString("video_text"));
        bean.setUserNationality(resultset.getString("nationality"));
        bean.setUserGender(resultset.getString("gender"));
        bean.setUserAtUni(resultset.getString("at_uni"));
        String length = resultset.getString("length");
        length = length != null && length.trim().length() > 0 ? length : "";
        bean.setVideoLength(length);
        bean.setVideoType(resultset.getString("video_type"));
        String videoUrl = resultset.getString("video_url");
        String videoType = resultset.getString("video_type");
        if (videoType != null && (videoType.equalsIgnoreCase("B") || videoType.equalsIgnoreCase("U"))) { //Backoffice video
          bean.setUserName(GlobalConstants.LIMELIGHT_VIDEO_PROVIDER_NAME);
        }
        if (videoType != null && videoType.equalsIgnoreCase("V")) { // User video
          bean.setVideoUrl(videoUrl);
          bean.setThumbnailUrl(resultset.getString("thumbnail_assoc_text"));
        } else {
          bean.setVideoUrl(limeLightPath + videoUrl);
          bean.setThumbnailUrl(new GlobalFunction().videoThumbnailFormatChange(limeLightPath + videoUrl, "0"));
        }
        bean.setOverallRating(resultset.getString("overall_rating"));
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }

  /**
      *  Function that check the file present in the specified location or not and return the status (true/false).
      * @param fileName
      * @return boolean
      */
  public boolean checkFilePresent(String fileName) {
    boolean foundFlag = true;
    try {
      java.io.File fileObject = new java.io.File(fileName);
      if (fileObject.exists()) {
        foundFlag = true;
      } else {
        foundFlag = false;
      }
    } catch (Exception exception) {
      exception.printStackTrace();
      foundFlag = false;
    }
    return foundFlag;
  }

  /**
    *  Function that check the file present in the specified location or not and return the status (true/false).
    * @param fileName
    * @return boolean
    */
  public boolean checkImagePresent(String fileName) {
    boolean foundFlag = true;
    try {
      java.io.File fileObject = new java.io.File(fileName);
      if (fileObject.exists()) {
        foundFlag = true;
      } else {
        foundFlag = false;
      }
    } catch (Exception exception) {
      exception.printStackTrace();
      foundFlag = false;
    }
    return foundFlag;
  }

  public String getReivewCategoryButtonLabel(String questionTitle, String collegeId) //CODE_CLEAN remove unused collegeId param
  {
     String buttonLabel = questionTitle;
     if(questionTitle != null ) {
        if(questionTitle.trim().equalsIgnoreCase("Overall Rating")) {
            buttonLabel = "Overall";
        }   
        if(questionTitle.trim().equalsIgnoreCase("Student Union")) {
            buttonLabel = "Student Union";//if you changed this then /review/include/rankingPodsTabs.jsp > line# 21 needs to be changes 
        }   
        if(questionTitle.trim().equalsIgnoreCase("Clubs & Societies") || questionTitle.trim().equalsIgnoreCase("Clubs and Societies") || questionTitle.trim().equalsIgnoreCase("Clubs")) {
            buttonLabel = "Clubs";
        }   
        if(questionTitle.trim().equalsIgnoreCase("Accommodation")) {
            buttonLabel = "Accommodation";
        }   
        if(questionTitle.trim().equalsIgnoreCase("Uni Facilities")) {
            buttonLabel = "Facilities";
        }
        if(questionTitle.trim().equalsIgnoreCase("Course & Lecturers") || questionTitle.trim().equalsIgnoreCase("Course and Lecturers") || questionTitle.trim().equalsIgnoreCase("Course")) {
            buttonLabel = "Course";
        }   
        if(questionTitle.trim().equalsIgnoreCase("Eye Candy")) {
            buttonLabel = "Eye Candy";
        }   
        if(questionTitle.trim().equalsIgnoreCase("City Life")) {
            buttonLabel = "City Life";
        }   
        if(questionTitle.trim().equalsIgnoreCase("Job Prospects"))  {
            buttonLabel = "Job Prospects";
        }   
        if(questionTitle.trim().equalsIgnoreCase("Recommendation"))  {
            buttonLabel = "Recommendation";
        }   
        if (questionTitle.trim().equalsIgnoreCase("Getting a place: top tips")){
          buttonLabel = "Review tips";
        }
    }
    return buttonLabel;
  }

    /**
     *  function to get SEO question title 
     * @param question_title as String
     * @return seo question title as String 
     */
    
    public String seoQuestionTitle(String questionTitle) 
    {
       String seoText = questionTitle;
       if(questionTitle != null ) {
          if(questionTitle.trim().equalsIgnoreCase("Overall Rating") || questionTitle.trim().equalsIgnoreCase("University of the year")) {
              seoText = "rating";
          }   
          if(questionTitle.trim().equalsIgnoreCase("Student Union")) {
              seoText = "student-union";
          }   
          if(questionTitle.trim().equalsIgnoreCase("Clubs & Societies") || questionTitle.trim().equalsIgnoreCase("Clubs and Societies")) {
              seoText = "clubs-and-societies";
          }   
          if(questionTitle.trim().equalsIgnoreCase("Accommodation")) {
              seoText = "student-accommodation";
          }   
          if(questionTitle.trim().equalsIgnoreCase("Uni Facilities")) {
              seoText = "uni-facilities";
          }
          if(questionTitle.trim().equalsIgnoreCase("Course & Lecturers") || questionTitle.trim().equalsIgnoreCase("Course and Lecturers")) {
              seoText = "course";
          }   
          if(questionTitle.trim().equalsIgnoreCase("Eye Candy")) {
              seoText = "eye-candy";
          }   
          if(questionTitle.trim().equalsIgnoreCase("City Life") || questionTitle.trim().equalsIgnoreCase("International") ) {
              seoText = "student-life";
          }   
          if(questionTitle.trim().equalsIgnoreCase("Job Prospects"))  {
              seoText = "job-prospects";
          }   
          if(questionTitle.trim().equalsIgnoreCase("Recommendation"))  {
              seoText = "recommendation";
          }   
          if (questionTitle.trim().equalsIgnoreCase("Getting a place: top tips")){
            seoText = "getting-a-place";
          }
      }
      return seoText;
   }

  /**
   *  function that returns the overall student rating list
   * @param request
   * @return Rating values as Collection object
   */
  public ArrayList getOverAllStudentRating(HttpServletRequest request) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    Vector parameters = new Vector();
    ArrayList list = new ArrayList();
    parameters.add(GlobalConstants.WHATUNI_AFFILATE_ID);
    try {
      BasketBean bean = null;
      resultset = dataModel.getResultSet("WU_MEMBERS_PKG.GET_OVERALL_STUDENT_RATING_FN", parameters);
      while (resultset.next()) {
        bean = new BasketBean();
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
        bean.setOverAllRating(resultset.getString("rating"));
        bean.setQuestionTitle(resultset.getString("question_title"));
        bean.setQuestionId(resultset.getString("review_question_id"));
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }
    
    /**
      *  The function uset to send an email to the receiver
      * @param friends_emails, attached_content, form, request, response, session, forward_to
      * @return none;
     */

     public void sendReviewerMail(String friends_emails, String attached_content, HttpServletRequest request, HttpSession session, String forward_to) 
     {
         String review_id   =  (String) (session.getAttribute("reviewid"));
         String collegeId   =  (String) (session.getAttribute("selectid"));
         String collegeName =  getCollegeTitleFunction(collegeId);  // added for 03rs Mardh 2010 Release
         //String p_url_view_review = "uniwrittenreviews.html?z="+collegeId+"&sort=SUBMITTED_DATE";  // commented for 03rd March 2010 Release 
         String p_url_view_review = uniLandingReviewSEOUrl(collegeId, collegeName); 
                p_url_view_review = p_url_view_review !=null ? p_url_view_review.substring(1, p_url_view_review.length()) : p_url_view_review;
         DataModel dataModel      =   new DataModel();
         try {
            Vector vector = new Vector();
            vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
            vector.add(review_id);
            vector.add(p_url_view_review);
            vector.add(friends_emails);
            vector.add(attached_content);
            vector.add((collegeId !=null && !collegeId.equalsIgnoreCase("null") && collegeId.trim().length()>0 ? collegeId.trim() : ""));
            vector.add(forward_to);
            String value = dataModel.executeUpdate("wu_email_pkg.wu_review_email_fn", vector);
        } catch(Exception exception) {
               exception.printStackTrace();
        }
     }

    /**
     *  function to send mail for the user after Registration 
     * @param userId, voucherCode, request 
     * @return void
     */
   
    public void sendRegistrtionMail(String userId, String voucherCode, HttpServletRequest request) 
    {
       DataModel dataModel  =   new DataModel();
       try {
           String p_url = "x="+new SessionData().getData(request,"x")+"&amp;y="+new SessionData().getData(request,"y")+"&amp;a="+ GlobalConstants.WHATUNI_AFFILATE_ID;  
           Vector vector = new Vector();
           vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);  
           vector.add(userId);                               
           vector.add(voucherCode);                          
           vector.add("WU_REGISTRATION_EMAIL");          
           vector.add(p_url);                            
           dataModel.executeUpdate("wu_email_pkg.registration_mail_fn", vector);
        }
        catch(Exception exception) {
           exception.printStackTrace();
        }
    }

    /**
     *  function to email to the user if there is 404 page error
     * @param emailaddress
     * @return void
     */
    
     public void sendSend404Email(String emailaddress, String errorType, String errorMessage) 
     {
        DataModel dataModel  =   new DataModel();
        try {
           Vector vector = new Vector();
           vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);   
           vector.add(emailaddress);         
           vector.add(errorType);            
           vector.add(errorMessage);        
           vector.add("WU_404_EMAIL");      
           String value = dataModel.executeUpdate("wu_email_pkg.page_404_email_fn", vector);
        } catch(Exception exception) {
           exception.printStackTrace();
        }
    }
        
   /**
    *  function to send email to  user after successfully adding video
    * @param request, videoId
    * return void
    */
        
    public void sendVideoSubmissionMail(HttpServletRequest request, String videoId,String collegeId) 
    {
        DataModel dataModel = new DataModel();
        try {
             String collegeName=getCollegeName(collegeId,request);
            //http://217.33.19.173/degrees/college-videos/oxford-university-videos/submitted-date/3757/1/student-videos.html
            String urlCollegeName=(collegeName !=null && collegeName.trim().length()>0 )?  replaceHypen(replaceSpecialCharacter(replaceURL(collegeName))) : "";
            String viewReviewUrl =  "college-videos/"+urlCollegeName+"-videos/submitted-date/"+collegeId+"/1/student-videos.html";
            Vector vector        =  new Vector();
            vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);    
            vector.add(new SessionData().getData(request,"y")); 
            vector.add("WU_VIDEO_EMAIL");                       
            vector.add(viewReviewUrl.toLowerCase());                        
            String value = dataModel.executeUpdate("wu_email_pkg.video_mail_fn", vector);
        } catch(Exception exception) {
            exception.printStackTrace();
        }
    }

     /**
      *  function to create a thumbnail from sourceFile from given targetFile with given size
      * @param sourceFile, targetFile, thumbNailSize
      * @return void
      */
       
     public void createThumbnail( String orig, String thumb, int maxDim) 
     {
        try {
            Image inImage = new ImageIcon(orig).getImage();
            double scale = (double)maxDim/(double)inImage.getHeight(null);
            if (inImage.getWidth(null) > inImage.getHeight(null)) {
                scale = (double)maxDim/(
                double)inImage.getWidth(null);
            }
            int scaledW = (int)(scale*inImage.getWidth(null));
            int scaledH = (int)(scale*inImage.getHeight(null));
            BufferedImage outImage = new BufferedImage(scaledW, scaledH, BufferedImage.TYPE_INT_RGB);
            AffineTransform tx = new AffineTransform();
            if (scale < 1.0d) {
                tx.scale(scale, scale);
            }
            // Paint image.
            Graphics2D g2d =  outImage.createGraphics();
            g2d.drawImage(inImage, tx, null);
            g2d.dispose();
            // JPEG-encode the image  and write to file.
            //OutputStream os = new FileOutputStream(thumb);
            //JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(os);
            //encoder.encode(outImage);
            //os.close();
            String imgformat = StringUtils.substringAfterLast(thumb, ".");
            imgformat = StringUtils.isNotBlank(imgformat) ? imgformat : "jpg";
            File outputfile = new File(thumb);
            ImageIO.write(outImage, imgformat , outputfile);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
     }
     
    /**
    * method to update chatbot disbaled flag in USER ATTRIBTES
    * @param userId, 
    * @return null
    */
      public void updateChatbotDisabled(String userId) {
        Object[][] attrValues = new Object[1][6];
        attrValues[0][0] = null;
        attrValues[0][1] = null;
        attrValues[0][2] = GlobalConstants.USER_CHATBOT_MINIMIZED_ATTR_ID;
        attrValues[0][3] = "Y";
        attrValues[0][4] = null;
        attrValues[0][5] = null;
        ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
        commonBusiness.updateChatBotDisable(userId, attrValues);
      }
      
     /**
     *  function that returns userInformation with profile infomation and userphoto path for the given userId
     * @param userId, request, servletContext
     * @return userList as collection object
     */
     
      public ArrayList getUserInformation(String userId, HttpServletRequest request, ServletContext context){
          ProfileBean profileBean     =   null;
         // ResultSet resultset  =   null;
          ArrayList list       =   new ArrayList();
         /* DataModel dataModel  =   new DataModel();
          Vector userInfo      =   new Vector();      
          userInfo.add(userId);
          try {
            resultset = dataModel.getResultSet("wu_gmap_pkg.get_user_info_fn", userInfo);
            while(resultset.next()) {
                bean           =   new ProfileBean();
                bean.setUserName(resultset.getString("user_name"));
                bean.setConfirmEmailId(resultset.getString("user_email"));
                bean.setPassword(resultset.getString("password"));                
                bean.setStatusFlag(resultset.getString("mandatory_flag"));
                String userImageDetails = resultset.getString("user_image_details");
                String userImageDetailsType = resultset.getString("user_image_details_type");                                
                if(!GenericValidator.isBlankOrNull(userImageDetailsType)) {
                  if(userImageDetailsType.equals("USER_IMAGE")) {
                    if(!GenericValidator.isBlankOrNull(userImageDetails)) {
                      String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");                      
                      String profileImagePath = "";
                      if(("LIVE").equals(envronmentName)){
                        profileImagePath = new CommonUtil().getImgPath(userImageDetails, 0);
                      }else{
                        profileImagePath = userImageDetails;
                      } 
                      request.setAttribute("croppedImage", profileImagePath);                      
                      bean.setUserImage(profileImagePath);
                    }
                  }else if(userImageDetailsType.equals("FB_USER_ID")) {
                    String fbImagePath = GlobalConstants.WHATUNI_SCHEME_NAME + "graph.facebook.com/"+userImageDetails+"/picture?type=small";
                    request.setAttribute("croppedImage", fbImagePath);
                    bean.setUserImage(fbImagePath);
                  }
                }else{
                  bean.setUserImage("/wu-cont/images/wu_ptu_icon.png");
                }
                bean.setMychNavCnt(resultset.getString("final_choices_cnt"));
                list.add(bean);
             }
          } catch(Exception exception) {
              exception.printStackTrace();
          } finally {
              dataModel.closeCursor(resultset);
          }*/
          try {
          ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
          RegistrationBean registrationBean = new RegistrationBean();     
          registrationBean.setUserId(userId);
          Map userInfoMap = commonBusiness.getUserInformationLogin(registrationBean);         
          /*profileBean = new ProfileBean();             
          profileBean.setUserName("A");
          profileBean.setConfirmEmailId("sk@sk.com");
          profileBean.setPassword("A");
          profileBean.setStatusFlag("N");
          String userImageDetails = null;
          String userImageDetailsType = null;
          profileBean.setMychNavCnt("3");
          list.add(profileBean);*/
            if(userInfoMap != null && !userInfoMap.isEmpty()) {
              ArrayList userInfoList = (ArrayList)userInfoMap.get("RetVal");
              if(userInfoList != null && userInfoList.size() > 0){
                profileBean = new ProfileBean();
                Iterator userInfoItr = userInfoList.iterator();
                while (userInfoItr.hasNext()){
                  ProfileBean bean = (ProfileBean)userInfoItr.next();
                  profileBean.setUserName(bean.getUserName());   
                  profileBean.setForeName(bean.getForeName());
                  profileBean.setConfirmEmailId(bean.getConfirmEmailId());                  
                  profileBean.setPassword(bean.getPassword());
                  profileBean.setStatusFlag(bean.getStatusFlag());
                  profileBean.setIsChatbotDisbaled(bean.getIsChatbotDisbaled());
                  // To get the information about the whether user minimized the chatbot, 25_Sep_2018 By Sabapathi
                  String isChatbotCookieClosed = (String)request.getSession().getAttribute("chatbot_promo_disabled");
                  if(GenericValidator.isBlankOrNull(isChatbotCookieClosed) && !(GenericValidator.isBlankOrNull(bean.getIsChatbotDisbaled()))) {
                    request.getSession().setAttribute("chatbot_promo_disabled", bean.getIsChatbotDisbaled());
                  } else if(!GenericValidator.isBlankOrNull(isChatbotCookieClosed) && (GenericValidator.isBlankOrNull(bean.getIsChatbotDisbaled()))){
                    updateChatbotDisabled(userId);
                  }
                  //
                  String userImageDetails = bean.getImageName();
                  String userImageDetailsType = bean.getImageFlag();
                  if(!GenericValidator.isBlankOrNull(userImageDetailsType)) {
                    if(userImageDetailsType.equals("USER_IMAGE")) {
                      if(!GenericValidator.isBlankOrNull(userImageDetails)) {
                        String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");                      
                        String profileImagePath = "";
                        if("LIVE".equalsIgnoreCase(envronmentName)){
                          profileImagePath = new CommonUtil().getImgPath(userImageDetails, 0);
                        }else{
                          profileImagePath = userImageDetails;
                        } 
                        request.setAttribute("croppedImage", profileImagePath);                      
                        profileBean.setUserImage(profileImagePath);
                      }
                    }else if("FB_USER_ID".equalsIgnoreCase(userImageDetailsType)) {
                      String fbImagePath = GlobalConstants.WHATUNI_SCHEME_NAME + "graph.facebook.com/"+userImageDetails+"/picture?type=small";
                      request.setAttribute("croppedImage", fbImagePath);
                      profileBean.setUserImage(fbImagePath);
                    }
                  }else{
                    profileBean.setUserImage("/wu-cont/images/wu_ptu_icon.png");
                  }
                  profileBean.setMychNavCnt(bean.getMychNavCnt());                  
                  list.add(profileBean);
                }
              }
            }
       } catch(Exception exception) {
           exception.printStackTrace();
       }
            return list;      
     }

  /**
    *  Function that returns the details of the reviews for the given reviewID
    * @param reviewId, request, servletContext, reviewStatus
    * @return Review details as collection Object
    */
  public List getMainDetails(String reviewId, HttpServletRequest request, ServletContext context, String reviewStatus) {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    ArrayList list = new ArrayList();
    ResultSet resultset = null;
    ReviewListBean bean = null;
    vector.clear();
    vector.add(reviewId);
    vector.add(reviewStatus);
    try {
      resultset = dataModel.getResultSet("Wu_review_Pkg.get_view_review_fn", vector);
      while (resultset.next()) {
        bean = new ReviewListBean();
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
        bean.setUserName(resultset.getString("user_name"));
        bean.setUserId(resultset.getString("user_id"));
        bean.setStudCurrentStatus(resultset.getString("at_uni"));
        bean.setCourseName(resultset.getString("course_name"));
        bean.setCourseDeletedFlag(resultset.getString("course_deleted_flag"));
        bean.setReviewTitle(resultset.getString("review_title"));
        bean.setNationality(resultset.getString("nationality"));
        bean.setGender(resultset.getString("gender"));
        bean.setOverallRating(resultset.getString("overall_rating"));
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setCourseId(resultset.getString("course_id"));
        bean.setSeoStudyLevelText(resultset.getString("seo_study_level_text"));
        //NEED TODO - need to remove for Ram's reference
        /*String userImageName = resultset.getString("user_photo");
        String userThumbName = (userImageName != null ? userImageName.substring(0, userImageName.lastIndexOf(".")) + "T" + userImageName.substring(userImageName.lastIndexOf("."), userImageName.length()) : userImageName);
        bean.setUserImage(userThumbName);
        bean.setUserImageLarge(userImageName);*/
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }
    
   /**
    *  Function to check whether the file exist in the given location (URL) 
    * @param fileName
    * @return present status as boolean 
    */
    
    public boolean checkURLFileExist(String fileName, String checkCollegeLogo)
    {
        boolean fileExistFlag = false;
        if(checkCollegeLogo !=null && checkCollegeLogo.trim().equalsIgnoreCase("FALSE")) {
              return fileExistFlag;
        }
        try{
            int response = 0;
            URL url = new URL(fileName);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConnection = (HttpURLConnection)connection;
            httpConnection.connect();
            response = httpConnection.getResponseCode();
            if(response == 200){
                fileExistFlag = true;
             }   
        } catch(Exception exception){
             fileExistFlag = false;
        }
        return fileExistFlag;   
     }

    /**
    *   Function that returns the key value for the Google map from sys var table
    * @param request sysVariable
    * @return SYS_VAR_VALUE as String
    */

    public String getSysVarValue(String sysVariable)
    {
        DataModel dataModel = new DataModel();
        Vector vector = new Vector();
        String sysValue = "";
        vector.clear();
        vector.add(sysVariable);
        try {
            sysValue = dataModel.getString("get_sysvar_value_fn", vector);
        } catch(Exception exception)  { 
            exception.printStackTrace();
        }
        return sysValue; 
    }

    public String getWUSysVarValue(String sysVariable){
      String sysValue = "";
     /*   DataModel dataModel = new DataModel();
        Vector vector = new Vector();
        String sysValue = "";
        vector.clear();
        vector.add(sysVariable);
        try {
            sysValue = dataModel.getString("wu_util_pkg.get_wu_sysvar_value_fn", vector);
        } catch(Exception exception){ 
            exception.printStackTrace();
        }*/
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
      RegistrationBean registrationBean = new RegistrationBean();     
      registrationBean.setDefaultValue(sysVariable);
      Map sysVarValMap = commonBusiness.getWUSysVarValue(registrationBean);
      if(sysVarValMap != null){
        sysValue = (String)sysVarValMap.get("RetVal");
      }
        return sysValue; 
    }
    
    /**
     *@getYearOfEntries method to get the year of entry details of Whatuni site
     * @return YOEDetailsList as ArrayList
     * @throws Exception
     * @since 24-09-2019_REL
     * @author Prabhakaran V.
     */
      public ArrayList getYearOfEntyList() throws Exception{
        ArrayList YOEDetailsList = new ArrayList();
        ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
        Map YOEMap = commonBusiness.getYearOfEntryDetails();
        if(YOEMap != null && !YOEMap.isEmpty()){
          YOEDetailsList = (ArrayList)YOEMap.get("YEAR_LIST");
        }
        return YOEDetailsList;
      }
      
    /**
     * @getYearOfEntryArr method to get the YOE values as a String array
     * @return yearOfEntryArr as String array
     * @throws Exception
     * @since 24-09-2019_REL
     * @author Prabhakaran V.
     */
      public String[] getYearOfEntryArr() throws Exception{
        ArrayList yearOfEntryList = getYearOfEntyList();
        Iterator yoeItr = yearOfEntryList.iterator();
        String yoeStr = "";
        while (yoeItr.hasNext()) {
          YearOfEntryVO yoeVO = (YearOfEntryVO)yoeItr.next();
          yoeStr += yoeVO.getYearofEntry() + GlobalConstants.SPLIT_CONSTANT;
        }
        if(!GenericValidator.isBlankOrNull(yoeStr)){
          yoeStr = yoeStr.substring(0, yoeStr.length()-GlobalConstants.SPLIT_CONSTANT.length());
          String[] yearOfEntryArr = yoeStr.split(GlobalConstants.SPLIT_CONSTANT);
          return yearOfEntryArr;
        }else{
          return null;
        }
      }
    

    /**
     *  Function that returns the region list 
     * @param form, request
     * @return Region list as collection object
    */
    
    public List getRegionList(ActionForm form, HttpServletRequest request) 
    {
       ArrayList regionList = new ArrayList();
       SearchBean bean = null;
       try {
             bean = new SearchBean();
             bean.setOptionId("");
             bean.setOptionText("Select location");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("UNITED KINGDOM");
             bean.setOptionText("Anywhere in the UK");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("-");
             bean.setOptionText("-------------------------------");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("ENGLAND");
             bean.setOptionText("ENGLAND");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("GREATER LONDON");
             bean.setOptionText("Greater London");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("EAST MIDLANDS");
             bean.setOptionText("East Midlands");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("EASTERN ENGLAND");
             bean.setOptionText("Eastern England");
             regionList.add(bean);

             bean = new SearchBean();
             bean.setOptionId("NORTH EAST ENGLAND");
             bean.setOptionText("North East England");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("NORTH WEST ENGLAND");
             bean.setOptionText("North West England");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("SOUTH EAST ENGLAND");
             bean.setOptionText("South East England");
             regionList.add(bean);

             bean = new SearchBean();
             bean.setOptionId("SOUTH WEST ENGLAND");
             bean.setOptionText("South West England");
             regionList.add(bean);

             bean = new SearchBean();
             bean.setOptionId("WEST MIDLANDS");
             bean.setOptionText("West Midlands");
             regionList.add(bean);

             bean = new SearchBean();             
             bean.setOptionId("YORKSHIRE AND HUMBERSIDE");
             bean.setOptionText("Yorkshire and Humber");
             regionList.add(bean);

             bean = new SearchBean();
             bean.setOptionId("-");
             bean.setOptionText("-------------------------------");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("SCOTLAND");
             bean.setOptionText("SCOTLAND");
             regionList.add(bean);

             bean = new SearchBean();             
             bean.setOptionId("NORTHERN SCOTLAND");
             bean.setOptionText("Northern Scotland");
             regionList.add(bean);

             bean = new SearchBean();             
             bean.setOptionId("CENTRAL SCOTLAND");
             bean.setOptionText("Central Scotland");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("SOUTHERN SCOTLAND");
             bean.setOptionText("Southern Scotland");
             regionList.add(bean);

             bean = new SearchBean();
             bean.setOptionId("-");
             bean.setOptionText("-------------------------------");
             regionList.add(bean);

             bean = new SearchBean();
             bean.setOptionId("WALES");
             bean.setOptionText("WALES");
             regionList.add(bean);

             bean = new SearchBean();
             bean.setOptionId("NORTH WALES");
             bean.setOptionText("North Wales");
             regionList.add(bean);

             bean = new SearchBean();
             bean.setOptionId("MID WALES");
             bean.setOptionText("Mid Wales");
             regionList.add(bean);

             bean = new SearchBean();
             bean.setOptionId("SOUTH WALES");
             bean.setOptionText("South Wales");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("-");
             bean.setOptionText("-------------------------------");
             regionList.add(bean);
             
             bean = new SearchBean();
             bean.setOptionId("NORTHERN IRELAND");
             bean.setOptionText("NORTHERN IRELAND");
             regionList.add(bean);
         } catch(Exception exception) {
             exception.printStackTrace();
         } 
         return regionList;
     }

    public List getMostReviewedUni(HttpServletRequest request) 
    {
        DataModel dataModel =   new DataModel();
        ResultSet resultset =   null;    
        ArrayList  list     =   new ArrayList();  
        try {
             HomePageBean bean =  null;
             Vector vector= new Vector();
             resultset = dataModel.getResultSet("wu_util_pkg.get_most_reviewed_uni_fn", vector);
             while(resultset.next()) {
                 bean = new HomePageBean();
                 bean.setCollegeId(resultset.getString("COLLEGE_ID"));    
                 bean.setCollegeName(resultset.getString("COLLEGE_NAME")); 
                 bean.setQuestionTitle(resultset.getString("question_title"));
                 bean.setRating(resultset.getString("rating"));
                 request.setAttribute("awardsquestionid", resultset.getString("question_id"));
                 String questionTitle = resultset.getString("question_title");
                 String seoText = seoQuestionTitle(questionTitle);
                 bean.setSeoQuestionText(seoText);
                 bean.setQuestionId(resultset.getString("question_id"));
                 list.add(bean);
             }
         } catch(Exception exception) {
             exception.printStackTrace();
         } finally {
             dataModel.closeCursor(resultset);
         }
       return list;
    }

    /**
     *  function that get the studymode list 
     * @return study mode list as collection object. 
     */

     public List getStudyMode()  
     {
        DataModel dataModel = new DataModel();
        Vector parameters = new Vector();
        ResultSet resultset = null;
        List studyModeList = new ArrayList();
        SearchBean bean = null;
        try {
           resultset = dataModel.getResultSet("WU_COURSE_SEARCH_PKG.GET_STUDY_MODE_FN", parameters);
           while(resultset.next()) {
              bean = new SearchBean();
              bean.setStudyModeId(resultset.getString("stdymde_opt_id")); 
              bean.setStudyModeDesc(resultset.getString("stdymde_webdesc"));
              bean.setStudyModeValue(resultset.getString("stdymde_value"));
              studyModeList.add(bean);
           }
        } catch(Exception exception) {
            exception.printStackTrace();    
        } finally {
           dataModel.closeCursor(resultset);
        }
        return studyModeList;
    }
    
    /**
     *  function that to get the studyLevel list 
     * @return study level list as collection object
     */
    
    public List getStudyLevel() 
    {
        DataModel dataModel = new DataModel();
        Vector parameters = new Vector();
        ResultSet resultset = null;
        ArrayList list = new ArrayList();
        SearchBean bean = null;
        try {
            resultset = dataModel.getResultSet("WU_COURSE_SEARCH_PKG.GET_QUALIFICATION_LEVEL_FN", parameters);
            while(resultset.next()) {
                bean = new SearchBean();
                bean.setOptionId(resultset.getString("qual_opt_id"));
                bean.setOptionText(resultset.getString("display_name"));
                list.add(bean);
            }
        } catch(Exception exception) {
            exception.printStackTrace();
        } finally {
            dataModel.closeCursor(resultset);
        }
        return list;
    }
    
    /** 
     *  function that returns the course list for the specified college 
     * @param headerId, request, pageNo
     * @return course list as collection object
     */
    
   public ArrayList getCourseList(String headerId, String pageNo, HttpServletRequest request)       
   {
        DataModel dataModel   =   new DataModel();
        ArrayList courseList  =   new ArrayList();
        ResultSet resultset   =   null;
        Vector vector         =   new Vector();
        vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
        vector.add(headerId);
        vector.add(pageNo);
        String userId = new SessionData().getData(request,"y");
        //added for 24th March Release 
               userId = userId !=null && !userId.equals("0") && userId.trim().length()>=0 ? userId : "";
        String basketId = (userId != null && (userId.equals("0") || userId.trim().length()==0)) ? checkCookieStatus(request) : "";
               basketId = (userId !=null && userId.trim().length()>0) ? "" : basketId;
               userId   =  (basketId !=null && basketId.trim().length()>0) ? "" : userId;
         if((userId !=null && !userId.equals("0") && userId.trim().length()==0) && (basketId !=null && basketId.trim().length()==0)){
             userId="0";
             basketId="";
          }
         // end // 
         vector.add(userId);             
         vector.add(basketId);  // added for 24th March Release   
         request.setAttribute("provider_scholorship_search_count", "0");
         request.removeAttribute("showFundingTab");
        try {
        
             resultset = dataModel.getResultSet("Wu_Course_Search_Pkg.show_results_crs", vector);
             while(resultset.next()) {
                 SearchResultBean bean = new SearchResultBean();
                 bean.setCourseid(resultset.getString("course_id"));
                 bean.setCoursetitle(resultset.getString("course_title"));
                 //bean.setStartDetails(resultset.getString("start_details"));///////////
                 bean.setCourseDescription(resultset.getString("course_desc"));
                 bean.setCourseDuration(resultset.getString("duration"));
                 bean.setLearnDirectQual(resultset.getString("learndir_qual"));
                 bean.setCourseCost(resultset.getString("cost_desc"));
                 bean.setProfile(resultset.getString("profile"));
                 bean.setVenueName(resultset.getString("venue_name"));
                 bean.setPostCode(resultset.getString("postcode"));
                 bean.setStudyMode(resultset.getString("study_mode"));
                 bean.setCollegeId(resultset.getString("college_id"));
                 bean.setCollegeName(resultset.getString("college_name"));
                 request.setAttribute("collegeName",bean.getCollegeName());
                 bean.setUcasCode(resultset.getString("ucas_code"));
                 bean.setUcasFlag(resultset.getString("ucas_flag"));
                 bean.setCollegeSite(resultset.getString("website"));
                 bean.setSeoStudyLevelText(resultset.getString("seo_study_level_text"));
                 request.setAttribute("studyLevel",resultset.getString("seo_study_level_text"));
                 //bean.setCollegeInBasket(resultset.getString("college_in_basket"));
                 bean.setCourseInBasket(resultset.getString("course_in_basket"));
                 bean.setWebsitePaidFlag(resultset.getString("website_pur_flag"));
                 if(resultset.getString("scholorship_count") !=null && !resultset.getString("scholorship_count").equals("0")) {
                    request.setAttribute("showFundingTab",  "TRUE");
                    request.setAttribute("provider_scholorship_search_count", resultset.getString("scholorship_count"));
                 }   
                 bean.setSubjectProfile(resultset.getString("provider_url"));
                 bean.setUcasPoint(resultset.getString("ucas_point"));
                 bean.setAvailableStudyModes(resultset.getString("available_study_modes"));
                 courseList.add(bean);
             }
         } catch(Exception exception) {
             exception.printStackTrace();    
         } finally {
             dataModel.closeCursor(resultset);    
         }
         return courseList;
     }
    
 
    public List getCourseContactDetails(String collegeId, String courseId, HttpServletRequest request)
    {
      DataModel dataModel     =   new DataModel();
      CourseDetailsBean bean  =   null;
      ResultSet resultset     =   null;
      List list               =   new ArrayList();    
      try {
          Vector parameters  =  new Vector();
          parameters.add(GlobalConstants.WHATUNI_AFFILATE_ID);
          parameters.add(courseId);  
          parameters.add(collegeId); 
          parameters.add(new SessionData().getData(request,"y")); 
          parameters.add(new SessionData().getData(request,"x"));
          
          
          resultset = dataModel.getResultSet("wu_course_search_pkg.get_course_contact_fn", parameters);          
          while(resultset.next()) {
              //list =  ;
              bean = new CourseDetailsBean();
              bean.setCourseId(resultset.getString("course_id"));
              bean.setCollegeId(resultset.getString("college_id"));
              bean.setCourseTitle(resultset.getString("course_title"));
              
              
              bean.setCollegeName(resultset.getString("college_name"));
             bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
              bean.setCourseVenue1(resultset.getString("address_line_1")); 
              bean.setCourseVenue2(resultset.getString("address_line_2")); 
              bean.setAdmissionEmail(resultset.getString("admission_email"));  
              bean.setWebSite(resultset.getString("intl_website"));
              bean.setTelePhone(resultset.getString("intl_telephone"));
              bean.setTown(resultset.getString("intl_town"));
              bean.setCounty(resultset.getString("intl_county"));
              bean.setPostCode(resultset.getString("intl_postcode")); 
              bean.setLastUpdateDate(resultset.getString("last_update_date"));
              bean.setSeoStudyLevelText(resultset.getString("seo_study_level_text"));
              bean.setCourseStudyLevel(resultset.getString("course_study_level"));
              bean.setCourseSubjectName(resultset.getString("course_subject_name"));  
              request.setAttribute("courseName", resultset.getString("course_title"));
              
              //= = = = = = = = = = = = Scholarship URL = = = = = = = = = = = = = = = 
              if(resultset.getString("scholarship_count") !=null && !resultset.getString("scholarship_count").equals("0")){
                 request.setAttribute("show_scholarship_tab","TRUE");
                 String scholarshipURL = GlobalConstants.SEO_PATH_SCHOLARSHIP_PAGE 
                                         + replaceHypen(replaceSpecialCharacter(replaceURL(resultset.getString("college_name"))))
                                         + (resultset.getString("seo_study_level_text")!=null && !resultset.getString("seo_study_level_text").equalsIgnoreCase("null") && resultset.getString("seo_study_level_text").trim().length()>0 ? "-"+resultset.getString("seo_study_level_text") : "")
                                         + "-scholarships-bursaries/"
                                         + (resultset.getString("study_level")!=null ? resultset.getString("study_level") : "m,n,a,t,l")
                                         + "/" + collegeId
                                         + "/1/bursary-uni.html";
                 request.setAttribute("scholarshipURL", new CommonUtil().toLowerCase(scholarshipURL));
              } 
              //= = = = = = = = = = = =  = = = = = = = = = = = = = = = = = = = = = = = = 
              String collegeName = resultset.getString("college_name") !=null && resultset.getString("college_name").trim().length()>0 ? (replaceHypen(replaceURL(resultset.getString("college_name"))).toLowerCase()) : resultset.getString("college_name");
              request.setAttribute("seoCollegeName", collegeName);
              request.setAttribute("collegeLocation", (resultset.getString("college_location") !=null ? resultset.getString("college_location").toLowerCase(): resultset.getString("college_location")));
              list.add(bean);
           }
      } catch(Exception exception){
          exception.printStackTrace();    
      } finally {
           dataModel.closeCursor(resultset);    
      }
      return list;
    }

    /**
     *  function that returns the course subject list 
     * @param form, request
     * @return subject list as collection object 
     */

     public List getCourseSubject(ActionForm form, HttpServletRequest request) 
     {
        DataModel dataModel = new DataModel();
        Vector parameters = new Vector();
        ResultSet resultset = null;
        ArrayList list = new ArrayList();
        SearchBean bean = null;
        try {
          /*resultset = dataModel.getResultSet("WU_COURSE_SEARCH_PKG.GET_SUBJECT_LIST_FN", parameters);
          while(resultset.next()) {
              bean = new SearchBean();
              bean.setOptionId(resultset.getString("category_code"));
              bean.setOptionText(resultset.getString("subject_name"));
              bean.setOptionSeqId(resultset.getString("subject_id"));
              list.add(bean);
          }*/
            
          ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
          RegistrationBean registrationBean = new RegistrationBean();          
          Map courseSubjectMap = commonBusiness.getCourseSubject(registrationBean);
          if (courseSubjectMap != null && !courseSubjectMap.isEmpty()) {
            ArrayList courseSubjectList = (ArrayList)courseSubjectMap.get("Ret_Sub");
            if(courseSubjectList != null && courseSubjectList.size() > 0){
              Iterator courseSubjectItr = courseSubjectList.iterator();
              while (courseSubjectItr.hasNext()){
                RegistrationBean registrationVO = (RegistrationBean)courseSubjectItr.next();
                  bean = new SearchBean();
                  bean.setOptionId(registrationVO.getCategoryCode());
                  bean.setOptionText(registrationVO.getOptionText());
                  bean.setOptionSeqId(registrationVO.getOptionId());
                  list.add(bean);
              }
            }
          }
            
        } catch(Exception exception) {
            exception.printStackTrace();
        } finally  {
            dataModel.closeCursor(resultset);            
        }
       return list;
    }    

     /**
      * function that returns the the Latitude and longtitude value using Yahoo PlaceFinder for the given postcode
      * @param callParam
      * @param callType
      * @return Latitute and Longtitude value  as String 
      * @author Mohamed Syed
      */

     public String getCoordinate(String callParam, String callType){
      String latitudeLongitude =   "";        
       //wu303_20110222 > yahoo.api is calling to get lat/lng previously it was calling google.api for the same action.
       HashMap latitudeLongitudeMap = new YahooPlaceFinder().getLatitudeLongitude(callParam, callType);       
       latitudeLongitude = latitudeLongitudeMap.get("latitude") + "," + latitudeLongitudeMap.get("longitude");
       //wu303_20110222 > insert these fetched details into our table.
       //new CommonUtilities().insertLatLon(latitudeLongitudeMap);       
        return latitudeLongitude;   
     }

    /**
     *  Function that return county list 
     * @param searchText, postcodePrefix, returnType, searchDistance, request
     * @return county list as collection object. 
     */
     
     public ArrayList getCountyList(String searchText, String postcodePrefix, String returnType, String searchDistance, HttpServletRequest request) 
     {
         DataModel dataModel    =   new DataModel();
         Vector parameters      =   new Vector();
         ResultSet resultset    =   null;
         ArrayList dataList     =   new ArrayList();
         GooglemapBean gmapBean =   null;
         try {
             parameters.add(searchText);
             parameters.add(postcodePrefix);
             parameters.add(searchDistance);
             resultset          =   dataModel.getResultSet("Wu_Gmap_Pkg.show_loc_match", parameters);
             int count          =   0;
             boolean minusOne   =   false;
             while(resultset.next())  {
             
                if(returnType !=null && returnType.equalsIgnoreCase("COUNTY_lIST")){
                    if(resultset.getString("college_name") == null || resultset.getString("college_name").trim().length()==0) {
                       gmapBean = new GooglemapBean();
                       gmapBean.setOptionId(resultset.getString("post_code"));
                       gmapBean.setOptionDescription(resultset.getString("full_loc_name"));
                       gmapBean.setTownName(resultset.getString("town_city"));
                       gmapBean.setLocation(resultset.getString("full_loc_name"));
                       dataList.add(gmapBean);
                    }    
                }
                
                if(returnType !=null && returnType.equalsIgnoreCase("POSTCODE_lIST")) {
                    if(resultset.getString("college_id") !=null &&  resultset.getString("college_id").equals("-1")) {
                        minusOne    = true;
                        break;
                    }
                    count++;
                    gmapBean  = new GooglemapBean();
                    gmapBean.setCollegeId(resultset.getString("college_id"));
                    gmapBean.setCollegeName(resultset.getString("college_name"));
                    gmapBean.setPostCode(resultset.getString("post_code"));
                    gmapBean.setMapCenterPoint(resultset.getString("town_city"));
                    gmapBean.setMapZoomLevel(resultset.getString("full_loc_name"));
                    gmapBean.setLatitudeValue(resultset.getString("latitude"));
                    gmapBean.setLongtitudeValue(resultset.getString("longitude"));
                    dataList.add(gmapBean);                    
                }
             }
             
             if(minusOne) {
                request.setAttribute("minusOne", "true");
             } else {
                 request.setAttribute("minusOne", "false");
             }   
          } catch(Exception exception) {
             exception.printStackTrace();
          } finally {
              dataModel.closeCursor(resultset);            
          }
         return dataList;
     }

     /**
      *  function that returns the review list for the given subject name
      * @param subjectId, collegeid, page_no, no_rec_to_show, reviewStatus, sort_order, servletContext, request
      * @return subject reivew list as collection object
      */

     public List getSubjectReview(String subjectId, String collegeid, String page_no, String no_rec_to_show, String reviewStatus, String sort_order, ServletContext context, HttpServletRequest request, String userType) 
     {
        DataModel dataModel   =   new DataModel();
        ResultSet resultset   =   null;    
        ArrayList list        =   new ArrayList();    
        HomePageBean bean     =   null;
        try {
            Vector vector     =  new Vector();
            vector.clear ();
            vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
            vector.add(collegeid);      
            vector.add(page_no);
            vector.add(no_rec_to_show);
            vector.add(sort_order);
            vector.add(reviewStatus);
            vector.add(subjectId);
            vector.add(userType);
            resultset = dataModel.getResultSet("Wu_course_search_pkg.get_subject_reviews_fn", vector);
            while(resultset.next()) {
                bean = new HomePageBean();
                bean.setReviewId(resultset.getString("review_id"));
                bean.setReviewTitle(resultset.getString("review_title"));
                bean.setUserId(resultset.getString("user_id"));
                bean.setUserName(resultset.getString("user_name") !=null ? resultset.getString("user_name").trim() : resultset.getString("user_name"));
                bean.setCollegeId(resultset.getString("college_id"));
                bean.setCollegeName(resultset.getString("college_name"));
                bean.setCourseName(resultset.getString("course_name"));
                bean.setCourseId(resultset.getString("course_id"));
                bean.setUserGraduate(resultset.getString("at_uni") != null ? resultset.getString("at_uni").trim() : resultset.getString("at_uni") );
                bean.setStudCurrentStatus(resultset.getString("at_uni"));
                bean.setNationality(resultset.getString("nationality") !=null ? resultset.getString("nationality").trim() : resultset.getString("nationality"));
                bean.setUserGender(resultset.getString("gender") !=null ? resultset.getString("gender").toLowerCase() : resultset.getString("gender"));
                bean.setGender(resultset.getString("gender") !=null ? resultset.getString("gender").toLowerCase() : resultset.getString("gender"));
                bean.setOverAllRating(resultset.getString("overall_rating"));
                bean.setOverallRating(resultset.getString("overall_rating"));
                bean.setOverallRatingComment(resultset.getString("overall_rating_comments"));
                bean.setCourseDeletedFlag(resultset.getString("course_deleted_flag"));
                bean.setSubjectId(subjectId);
                bean.setSeoStudyLevelText(resultset.getString("seo_study_level_text"));
                //NEED TODO - need to remove for Ram's reference
                /*String userImageName  =  resultset.getString("user_photo");
                String userThumbName  =  (userImageName !=null ? userImageName.substring(0,userImageName.lastIndexOf(".")) +"T"+ userImageName.substring(userImageName.lastIndexOf("."),userImageName.length()) : userImageName);
                bean.setUserImage(userThumbName);
                bean.setUserImageLarge(userImageName);*/
                list.add(bean);
              }
           } catch(Exception exception) {
               exception.printStackTrace();
           } finally {
               dataModel.closeCursor(resultset);
           }
          return list;
      }           

     /**
      *  function that returns the review count for the given subject name
      * @param subjectId, collegeId, request
      * @return subject reivew count as string
      */

     public String getSubjectReviewCount(String collegeId, String subjectId, HttpServletRequest request, String userType) 
     {
        DataModel dataModel  =   new DataModel();
        String reviewCount   =   "0";    
        try {
            Vector vector = new Vector();
            vector.clear ();
            vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
            vector.add(collegeId);
            vector.add(subjectId);
            vector.add(userType);
            reviewCount = dataModel.getString("wu_course_search_pkg.get_subject_reviews_count_fn", vector);
         } catch(Exception exception) {
               exception.printStackTrace();
         }
        return reviewCount;
      }           

     /**
      *  function that check the session time haven been exceeded beyond the specified time 
      * @param HttpServletRequest request, session
      * @return string
      */

     public boolean checkSessionExpiry(HttpServletRequest request, HttpSession session) 
     {
       boolean returnFlag = false;
       try {
           if(request.getRequestedSessionId()!=null && !(request.isRequestedSessionIdValid())){
              returnFlag =  true;
           }
        } catch(Exception exception) {
          exception.printStackTrace();
       }
       return returnFlag;
    }

    /**
     *  Function that search the college details for the given condition and stored in a table and return the header_id 
     *      with some parameters. this function internally call another procedure by passing the header Id and get the result 
     *      to be display in the first page. 
     * @param search_what, phrase_search, college_name, qualification, country, town_city, postcode, search_range, postflag, 
              location_id, study_mode, course_duration, lucky, crs_search, submit, a, search_how, s_type, search_category, 
              search_career, career_id, ref_id, nrp, area, p_pg_type, p_search_col, p_county_id, request
     * @return college List details as ArrayList
     */
    
      public String getSearch_Adv_Do(  
                                   String search_what, 
                                   String phrase_search,        
                                   String college_name,        
                                   String qualification,       
                                   String country,             
                                   String town_city,           
                                   String postcode,            
                                   String search_range,        
                                   String postflag,            
                                   String location_id,         
                                   String study_mode,          
                                   String course_duration,     
                                   String lucky,               
                                   String crs_search,        
                                   String submit,            
                                   String a,                 
                                   String search_how,        
                                   String s_type,            
                                   String search_category,   
                                   String search_career,     
                                   String career_id,         
                                   String ref_id,            
                                   String nrp,               
                                   String area,              
                                   String p_pg_type,         
                                   String p_search_col,      
                                   String p_county_id,
                                   HttpServletRequest request,
                                   String entityText,
                                   String search_thru,
                                   String ucas_code,     // added for 21th October 2009 release for UCAS code search
                                   String deemedUniFlag // added for 15th dec release deemed uni flag
                                   )
           {                                                   
             DataModel dataModel  =   new DataModel();
             String user_agent    =    request.getHeader("user-agent");
             String requestURL    =   request.getRequestURI();             
             String trimmedPhraseSearch = phrase_search != null ? phrase_search.replaceAll("-"," ") : ""; 
             Vector vector   =    new Vector();
                    vector.add(new SessionData().getData(request,"x"));
                    vector.add(new SessionData().getData(request,"y"));
                    vector.add(search_what);
                    vector.add(trimmedPhraseSearch);
                    vector.add(college_name);
                    vector.add(qualification);
                    vector.add(country);
                    vector.add(town_city);
                    vector.add(postcode);
                    vector.add(search_range);
                    vector.add(postflag);
                    vector.add(location_id);
                    vector.add(study_mode);
                    vector.add(course_duration);
                    vector.add(lucky);
                    vector.add(crs_search);
                    vector.add(submit);
                    vector.add(a);
                    vector.add(search_how);
                    vector.add(s_type);
                    vector.add(search_category);
                    vector.add(search_career);
                    vector.add(career_id);
                    vector.add(ref_id);
                    vector.add(nrp);
                    vector.add(area);
                    vector.add(p_pg_type);
                    vector.add(p_search_col);
                    vector.add(p_county_id);
                    vector.add(entityText);
                    vector.add(search_thru);//p_search_where
                    vector.add(user_agent);
                    vector.add(requestURL);//search_browse_link    
                    vector.add(ucas_code);
                    vector.add(deemedUniFlag);
                    String headerId = "";
                try {
                
                
                  headerId = dataModel.getString("hot_wuni.whatuni_search.adv_col_do", vector);
                 } catch(SQLException sqlException){
                    String errorMessage = sqlException.getMessage();
                    errorMessage = errorMessage != null ? errorMessage.replaceAll("ORA-20001: ", "") : errorMessage;
                    String errorCode[] = errorMessage.split(":");
                    request.setAttribute("SearchErrorMessage", errorMessage);    
                    if(errorCode != null && errorCode.length >=2) {
                        request.setAttribute("SearchErrorCode", errorCode[2]);    
                    }
                 } catch(Exception searchException) {
                    searchException.printStackTrace();    
                }
             return headerId;
     }

    /**
     *  Function that search the college details for the given condition and stored in a table and return the header_id 
     *      with some parameters. this function internally call another procedure by passing the header Id and get the result 
     *      to be display in the first page. 
     * @param search_what, phrase_search, college_name, qualification, country, town_city, postcode, search_range, postflag, 
              location_id, study_mode, course_duration, lucky, crs_search, submit, a, search_how, s_type, search_category, 
              search_career, career_id, ref_id, nrp, area, p_pg_type, p_search_col, p_county_id, request
     * @return college List details as ArrayList
     */
     
      /*public String getSearch_Adv_Do_List(  
                                           String search_what, 
                                           String phrase_search,        
                                           String college_name,        
                                           String qualification,       
                                           String country,             
                                           String town_city,           
                                           String postcode,            
                                           String search_range,        
                                           String postflag,            
                                           String location_id,         
                                           String study_mode,          
                                           String course_duration,     
                                           String lucky,               
                                           String crs_search,        
                                           String submit,            
                                           String a,                 
                                           String search_how,        
                                           String s_type,            
                                           String search_category,   
                                           String search_career,     
                                           String career_id,         
                                           String ref_id,            
                                           String nrp,               
                                           String area,              
                                           String p_pg_type,         
                                           String p_search_col,      
                                           String p_county_id,
                                           HttpServletRequest request)
            {                                                   
                DataModel dataModel  =  new DataModel();
                Vector vector        =  new Vector();
                vector.add(new SessionData().getData(request,"x"));
                vector.add(new SessionData().getData(request,"y"));
                vector.add(search_what);
                vector.add(phrase_search);
                vector.add(college_name);
                vector.add(qualification);
                vector.add(country);
                vector.add(town_city);
                vector.add(postcode);
                vector.add(search_range);
                vector.add(postflag);
                vector.add(location_id);
                vector.add(study_mode);
                vector.add(course_duration);
                vector.add(lucky);
                vector.add(crs_search);
                vector.add(submit);
                vector.add(a);
                vector.add(search_how);
                vector.add(s_type);
                vector.add(search_category);
                vector.add(search_career);
                vector.add(career_id);
                vector.add(ref_id);
                vector.add(nrp);
                vector.add(area);
                vector.add(p_pg_type);
                vector.add(p_search_col);
                vector.add(p_county_id);
                String headerId = "";
              try  {
                headerId = dataModel.getString("hot_wuni.whatuni_search.adv_col_list", vector);
              } catch(Exception searchException) {
                 searchException.printStackTrace();    
              }
            return headerId;
      }*/
         
     /**
      *  Function that returns the records from the temp table by passing the header_id and page number 
      * @param headerId, pageNo, request
      * @return college List details as ArrayList
      */

     /* public ArrayList getSearch_Adv_Result(String headerId, String pageNo, HttpServletRequest request, String categoryCode, String searchKeyword, String profileCategoryName, String studyLevel) 
      {
           int count = 0;
           DataModel dataModel    =   new DataModel();
           ArrayList searchList   =   new ArrayList();
           SearchResultBean bean  =   null;
           ResultSet  resultset   =   null;
           String userId = new SessionData().getData(request,"y");
                  userId = userId !=null && !userId.equals("0") && userId.trim().length()>=0 ? userId : "";
           String basketId = (userId != null && (userId.equals("0") || userId.trim().length()==0)) ?  checkCookieStatus(request) : "";
                  basketId = (userId !=null && userId.trim().length()>0) ? "" : basketId;
                  userId   =  (basketId !=null && basketId.trim().length()>0) ? "" : userId;
           if((userId !=null && !userId.equals("0") && userId.trim().length()==0) && (basketId !=null && basketId.trim().length()==0)){
               userId="0";
               basketId="";
           }
           Vector vector = new Vector();
           vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
           vector.add(headerId);
           vector.add(pageNo);
           vector.add(userId);
           vector.add(basketId);
           vector.add(categoryCode);
           vector.add(searchKeyword);
           vector.add(profileCategoryName);
           vector.add(studyLevel);
           request.setAttribute("scholarshipCount","0");
           String starttag = "<a title=\"No data\" class=\"ned\">";
           String endtag = "</a>";
           boolean showstatsbutton = false;
           String ptospectustext = searchKeyword !=null && searchKeyword.trim().length()>0 ? searchKeyword : (profileCategoryName !=null && profileCategoryName.trim().length()>0 ? profileCategoryName : "");
           try {
           
               resultset = dataModel.getResultSet("wu_course_search_pkg.show_results_col", vector);               
               while(resultset.next()) {
                   showstatsbutton = false;
                   bean = new SearchResultBean();
                   bean.setCollegeId(resultset.getString("college_id"));
                   bean.setCollegeName(resultset.getString("college_name"));
                   bean.setCollegeSite(resultset.getString("website"));
                   bean.setOverAllRating(resultset.getString("overall_rating"));
                   bean.setTotalReviews(resultset.getString("review_count"));
                   bean.setNoOfCourses(new CommonUtil().changeNumberFormat(resultset.getString("hits")));
                   bean.setCollegeInBasket(resultset.getString("college_in_basket"));
                   bean.setSubProfilePurchaseFlag(resultset.getString("sub_profile_pur_flag"));
                   bean.setSubjectCategoryCode(resultset.getString("subject_category_code"));
                   bean.setWebsitePaidFlag(resultset.getString("website_pur_flag"));
                   
                   // added for 30 June 2009 Release to avoid the popup window block in the browser //
                   String prospectus_pur_flag[] =  resultset.getString("prospectus_pur_flag").split(GlobalConstants.PROSPECTUS_SPLIT_CHAR);
                   bean.setProspectusPaidFlag(prospectus_pur_flag[0]);
                   String prospectusurl = "";
                   if(prospectus_pur_flag !=null && prospectus_pur_flag.length>1) {
                      prospectusurl = prospectus_pur_flag[1];
                   }
                    
                    prospectusurl = prospectusurl!=null && prospectusurl.trim().length()>0 ? 
                                   " rel=\"nofollow\" onclick=\"prospectusClick(this,'"+resultset.getString("college_id")+"'); var a='s.tl(';\" href=\""+request.getContextPath()+"/prospectuslink.html?cid="+resultset.getString("college_id")+"&url="+prospectus_pur_flag[1]+"\"":
                                   " onclick=\"return prospectusRedirect('"+request.getContextPath()+"','"+resultset.getString("college_id")+"\','','"+ptospectustext+"');\"";
                   bean.setProspectusExternalUrl(prospectusurl);
                   /////////////////////////////////////////////////////////////////////////
                   
                   request.setAttribute("scholarshipCount", resultset.getString("scholarship_count"));
                   //bean.setCollegeLogo(resultset.getString("college_logo"));    // 13.01.2009 release
                   bean.setCollegeProfileFlag(resultset.getString("college_profile_flag")); // 13.01.2009 release
                   bean.setProfile(resultset.getString("total_profile_count")); // 13.01.2009 release
                   
                   String students_satisfaction  = resultset.getString("students_satisfaction");
                   if(students_satisfaction !=null && !students_satisfaction.equalsIgnoreCase("NED")){
                      bean.setStudentsSatisfication("<a title=\""+GlobalConstants.SATISFICATION_WITH_UNI_ALT_MSG+"\" class=\"ned\">"+students_satisfaction+"%</a>"); // 13.01.2009 release
                       showstatsbutton = true;
                   }else{
                        //bean.setStudentsSatisfication(starttag+"No data"+endtag); // 13.01.2009 release
                         bean.setStudentsSatisfication(""); // 13.01.2009 release
                    }
                    String students_job  = resultset.getString("students_job");
                    if(students_job !=null && !students_job.equalsIgnoreCase("NED")){
                       bean.setStudentsJob("<a title=\""+GlobalConstants.STUDENTS_JOB_ALT_MSG+"\" class=\"ned\">"+students_job+"</a>"+"%"); // 13.01.2009 release
                        showstatsbutton = true;
                    }else{
                         //bean.setStudentsJob(starttag+"No data"+endtag); // 13.01.2009 release
                         bean.setStudentsJob(""); // 13.01.2009 release
                    }
                    String times_ranking  = resultset.getString("times_ranking");
                    if(times_ranking !=null && !times_ranking.equalsIgnoreCase("Not included") && !times_ranking.equalsIgnoreCase("0")){
                       bean.setTimesLeaguePosition(times_ranking); // 13.01.2009 release
                        showstatsbutton = true;
                    }else{
                         //bean.setTimesLeaguePosition("<a title=\"Not included\" class=\"ned\">"+"Not included"+endtag); // 13.01.2009 release
                         bean.setTimesLeaguePosition(""); // 13.01.2009 release
                    }
                    
                    // code added for 16th March 2010 Release 
                    String ucas_point  = resultset.getString("ucas_point");
                    if(ucas_point !=null && !ucas_point.equalsIgnoreCase("NED")){
                       bean.setAvgUcasPoints(ucas_point); // 13.01.2009 release
                        showstatsbutton = true;
                    }else{
                         //bean.setAvgUcasPoints(starttag+"No data"+endtag); // 13.01.2009 release
                         bean.setAvgUcasPoints(""); // 13.01.2009 release
                    }         
                    
                    String student_staff_ratio  = resultset.getString("student_staff_ratio");
                    if(student_staff_ratio !=null && !student_staff_ratio.equalsIgnoreCase("NED")){
                       bean.setStudentStaffRatio(student_staff_ratio); // 13.01.2009 release
                        showstatsbutton = true;
                    }else{
                         //bean.setStudentStaffRatio(starttag+"No data"+endtag); // 13.01.2009 release
                         bean.setStudentStaffRatio(""); // 13.01.2009 release
                    }         
                    
                    String drop_out_rate  = resultset.getString("drop_out_rate");
                    if(drop_out_rate !=null && !drop_out_rate.equalsIgnoreCase("NED")){
                       bean.setDropoutRate(drop_out_rate); // 13.01.2009 release
                        showstatsbutton = true;
                    }else{
                         //bean.setDropoutRate(starttag+"No data"+endtag); // 13.01.2009 release
                         bean.setDropoutRate(""); // 13.01.2009 release
                    }        
                    
                    String int_students  = resultset.getString("int_students");
                    if(int_students !=null && !int_students.equalsIgnoreCase("NED")){
                       bean.setIntlStudRatio(int_students); // 13.01.2009 release
                    }else{
                         //bean.setIntlStudRatio(starttag+"No data"+endtag); // 13.01.2009 release
                         bean.setIntlStudRatio(""); // 13.01.2009 release
                    }           
                    
                    String gender_ratio  = resultset.getString("gender_ratio");
                    if(gender_ratio !=null && !gender_ratio.equalsIgnoreCase("NED")){
                       bean.setMaleFemaleRatio(gender_ratio); // 13.01.2009 release
                    }else{
                         //bean.setMaleFemaleRatio(starttag+"No data"+endtag); // 13.01.2009 release
                         bean.setMaleFemaleRatio(""); // 13.01.2009 release
                    }               
                    
                    String accomodation_fee  = resultset.getString("accomodation_fee");
                    if(accomodation_fee !=null && !accomodation_fee.equalsIgnoreCase("NED")){
                       bean.setAccomodationFees(accomodation_fee); // 13.01.2009 release
                    }else{
                       //bean.setAccomodationFees(starttag+"No data"+endtag); // 13.01.2009 release
                       bean.setAccomodationFees(""); // 13.01.2009 release
                    }    
                    
                    String times_ranking_subject  = resultset.getString("times_ranking_subject");
                    if(times_ranking_subject !=null && !times_ranking_subject.equalsIgnoreCase("NED") && !times_ranking_subject.equalsIgnoreCase("0")){
                       bean.setSubjectTimesRanking(times_ranking_subject); // 13.01.2009 release
                        showstatsbutton = true;
                    }else{
                       //bean.setSubjectTimesRanking(starttag+"No data"+endtag); // 13.01.2009 release
                       bean.setSubjectTimesRanking(""); // 13.01.2009 release
                    }          
                    if(resultset.getString("review_count") !=null && !resultset.getString("review_count").equals("0")){
                       showstatsbutton = true;
                     }
                   
                    // added this code for 15th September 2009 release to integrate w_media table for video and images                     
                    String videoCollegeLogo = resultset.getString("college_logo");
                    if(videoCollegeLogo !=null && videoCollegeLogo.trim().length() > 0 && (videoCollegeLogo.indexOf(".flv") >=0 || videoCollegeLogo.indexOf(".mp4") > -1)) {
                         String college_video_url[] =  videoCollegeLogo.split("###");
                         bean.setProfileVideoUrl(college_video_url[0]);
                         if(college_video_url !=null && college_video_url.length>1) {
                             bean.setVideoReviewId(college_video_url[1]);
                         }    
                         if(college_video_url !=null && college_video_url.length>2) {
                           bean.setVideoUrl(college_video_url[2]);
                           //bean.setVideoThumbnail(new GlobalFunction().getVideoThumbPath(college_video_url[2],college_video_url[1]));
                           bean.setVideoThumbnail(new GlobalFunction().videoThumbnailFormatChange(college_video_url[2], resultset.getString("thumb_no")));  //added for 16th Feb 2010 Release
                         }    
                     } else if(videoCollegeLogo !=null && videoCollegeLogo.trim().length() > 0){
                         bean.setCollegeLogo(videoCollegeLogo);
                     } else{
                        bean.setCollegeLogo(""); 
                     }
                     //bean.setUcasCode(""); // added for 21th October 2009 release for UCAS code search
                   
                   // code added for 03rd MArch 2009 Release
                    String collegeLatLong = resultset.getString("col_lat_long");
                    if(collegeLatLong !=null && collegeLatLong.trim().length() > 0 && collegeLatLong.indexOf(",") >=0) {
                        String collegeLatLong_url[] =  collegeLatLong.split(",");
                        bean.setLattitude(collegeLatLong_url[0]);
                        bean.setLongtitude(collegeLatLong_url[1]);
                    }
                   // end of the code added for 03rd MArch 2009 Release
                   if(showstatsbutton){
                      bean.setStatDisplayButton("YES");
                   }   
                   
                   bean.setCategoryCode(categoryCode);
                   // added for december 15th 2009 release for rerun the search for noreulst
                   if(count == 0){
                     if(resultset.getString("search_action") != null && resultset.getString("search_action").equalsIgnoreCase("SECOND SEARCH")){
                        request.setAttribute("secondSearch","true");
                     }
                     count++;
                   }
                 // ends here -- december 15th 2009 release for rerun the search for noreulst
                   searchList.add(bean);
               }
             } catch(Exception exception) {
                exception.printStackTrace();    
             } finally {
                 dataModel.closeCursor(resultset);            
             }
          return searchList;
      }*/

    /**
     *  Function that returns the total records found in the result table for the given header_id (for college)
     * @param headerId, request
     * @return searchCount as String 
     */
    
    /* public String getSearch_Adv_Result_count(String headerId, HttpServletRequest request)  
     {
         String advSearchCount    =   "0";
         try {
            DataModel dataModel   =   new DataModel();
            Vector vector    =    new Vector();
            vector.clear();
            vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
            vector.add(headerId);       
            advSearchCount = dataModel.getString("wu_course_search_pkg.show_results_col_count", vector);
         } catch(Exception exception) {
             exception.printStackTrace();
         }
        return advSearchCount;
     }
*/
   /**
    *  Function that returns the total records found in the result table for the given header_id (for courses)
    * @param headerId, request
    * @return searchCount as String 
    */

    /*public String getSearchCourseCount(String headerId, HttpServletRequest request)  
    {
       String advSearchCount  = "0";
       try {
          DataModel dataModel = new DataModel();
          Vector vector       = new Vector();
          vector.clear();
          vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
          vector.add(headerId);       
          advSearchCount = dataModel.getString("wu_course_search_pkg.show_results_course_count", vector);
        } catch(Exception exception)  {
           exception.printStackTrace();
       }
       return advSearchCount;
    }*/
     
    /**
     *  function to delete the existing user image if user change the default image using edit registration page 
     * @param userId
     * @param request
     * @return status as bolean
     */
    //NEED TODO - need to remove for Ram's reference
    /*public boolean deleteExistingImage(String imageId, HttpServletRequest request, ServletContext servletContext) 
    {
        boolean statusFlag   = true;
        DataModel datamodel  =   new DataModel();
        try {
           Vector vector   =   new Vector();
           vector.clear();
           vector.add(imageId);
           String imagePath = datamodel.getString("wu_profile_pkg.get_image_path_fn", vector);
           if(imagePath !=null && !imagePath.trim().equalsIgnoreCase("null"))  {
               String filePath =  getWUSysVarValue("USER_IMAGE_FILECHECK_PATH"); 
               String deletePath = filePath + imagePath;
               File deleteFile = new File(deletePath);
               if (deleteFile.isFile()) {
                 deleteFile.delete();
               }
               // TO DELETE USER PHOTO THUMBNAIL 
               deletePath = deletePath.substring(0,deletePath.lastIndexOf(".")) +"T" + deletePath.substring(deletePath.lastIndexOf("."), deletePath.length());;
               File deleteThumbnail = new File(deletePath);
               if (deleteThumbnail.isFile()){
                  deleteThumbnail.delete();
               }
          }
        } catch(Exception exception) {
            exception.printStackTrace();
        }
        return statusFlag;
    }*/

    /** 
      *  Function that returns the detault images list will be selected by the user during registration. 
      * @param request
      * @return Userimage list as collection object
      */
      
    public List defaultImageList(HttpServletRequest request) 
    {
       RegistrationBean bean  = null;
       List list              =   new ArrayList();
       ArrayList imageList    =   new ArrayList();
       try {
           list  =  DefaultImagePropertyList(); 
           Iterator imageIterator = list.iterator();
           while(imageIterator.hasNext()) {
               bean = new RegistrationBean();
               bean.setOptionId((String) (imageIterator.next()));
               bean.setOptionText((String) (imageIterator.next()));
               imageList.add(bean);
           }
       } catch (Exception defaultException) {
         defaultException.printStackTrace();
       }
       return sortValue(true, false, imageList);
    }

    public List DefaultImagePropertyList() 
    {
       List list   =   new ArrayList();
       ResourceBundle bundle =   ResourceBundle.getBundle("com.resources.ApplicationResources");
       list.add(bundle.getString("wuni.default.images_id_1"));
       list.add(bundle.getString("wuni.default.images.text_1"));
       list.add(bundle.getString("wuni.default.images_id_2"));
       list.add(bundle.getString("wuni.default.images.text_2"));
       list.add(bundle.getString("wuni.default.images_id_3"));
       list.add(bundle.getString("wuni.default.images.text_3"));
       list.add(bundle.getString("wuni.default.images_id_4"));
       list.add(bundle.getString("wuni.default.images.text_4"));
       list.add(bundle.getString("wuni.default.images_id_5"));
       list.add(bundle.getString("wuni.default.images.text_5"));
       list.add(bundle.getString("wuni.default.images_id_6"));
       list.add(bundle.getString("wuni.default.images.text_6"));
       list.add(bundle.getString("wuni.default.images_id_7"));
       list.add(bundle.getString("wuni.default.images.text_7"));
       list.add(bundle.getString("wuni.default.images_id_8"));
       list.add(bundle.getString("wuni.default.images.text_8"));
       list.add(bundle.getString("wuni.default.images_id_9"));
       list.add(bundle.getString("wuni.default.images.text_9"));
       list.add(bundle.getString("wuni.default.images_id_10"));
       list.add(bundle.getString("wuni.default.images.text_10"));
       list.add(bundle.getString("wuni.default.images_id_11"));
       list.add(bundle.getString("wuni.default.images.text_11"));
       list.add(bundle.getString("wuni.default.images_id_12"));
       list.add(bundle.getString("wuni.default.images.text_12"));
       list.add(bundle.getString("wuni.default.images_id_13"));
       list.add(bundle.getString("wuni.default.images.text_13"));
       list.add(bundle.getString("wuni.default.images_id_14"));
       list.add(bundle.getString("wuni.default.images.text_14"));
       list.add(bundle.getString("wuni.default.images_id_15"));
       list.add(bundle.getString("wuni.default.images.text_15"));
       list.add(bundle.getString("wuni.default.images_id_16"));
       list.add(bundle.getString("wuni.default.images.text_16"));
       list.add(bundle.getString("wuni.default.images_id_17"));
       list.add(bundle.getString("wuni.default.images.text_17"));
       list.add(bundle.getString("wuni.default.images_id_18"));
       list.add(bundle.getString("wuni.default.images.text_18"));
       list.add(bundle.getString("wuni.default.images_id_19"));
       list.add(bundle.getString("wuni.default.images.text_19"));
       list.add(bundle.getString("wuni.default.images_id_20"));
       list.add(bundle.getString("wuni.default.images.text_20"));
       list.add(bundle.getString("wuni.default.images_id_21"));
       list.add(bundle.getString("wuni.default.images.text_21"));
       list.add(bundle.getString("wuni.default.images_id_22"));
       list.add(bundle.getString("wuni.default.images.text_22"));
       list.add(bundle.getString("wuni.default.images_id_23"));
       list.add(bundle.getString("wuni.default.images.text_23"));
       list.add(bundle.getString("wuni.default.images_id_24"));
       list.add(bundle.getString("wuni.default.images.text_24"));
       list.add(bundle.getString("wuni.default.images_id_25"));
       list.add(bundle.getString("wuni.default.images.text_25"));
       list.add(bundle.getString("wuni.default.images_id_26"));
       list.add(bundle.getString("wuni.default.images.text_26"));
       list.add(bundle.getString("wuni.default.images_id_27"));
       list.add(bundle.getString("wuni.default.images.text_27"));
       list.add(bundle.getString("wuni.default.images_id_28"));
       list.add(bundle.getString("wuni.default.images.text_28"));
       list.add(bundle.getString("wuni.default.images_id_29"));
       list.add(bundle.getString("wuni.default.images.text_29"));
       list.add(bundle.getString("wuni.default.images_id_30"));
       list.add(bundle.getString("wuni.default.images.text_30"));
       list.add(bundle.getString("wuni.default.images_id_31"));
       list.add(bundle.getString("wuni.default.images.text_31"));
       list.add(bundle.getString("wuni.default.images_id_32"));
       list.add(bundle.getString("wuni.default.images.text_32"));
       list.add(bundle.getString("wuni.default.images_id_33"));
       list.add(bundle.getString("wuni.default.images.text_33"));
       list.add(bundle.getString("wuni.default.images_id_34"));
       list.add(bundle.getString("wuni.default.images.text_34"));
       list.add(bundle.getString("wuni.default.images_id_35"));
       list.add(bundle.getString("wuni.default.images.text_35"));
       list.add(bundle.getString("wuni.default.images_id_36"));
       list.add(bundle.getString("wuni.default.images.text_36"));
       list.add(bundle.getString("wuni.default.images_id_37"));
       list.add(bundle.getString("wuni.default.images.text_37"));
       list.add(bundle.getString("wuni.default.images_id_38"));
       list.add(bundle.getString("wuni.default.images.text_38"));
       list.add(bundle.getString("wuni.default.images_id_39"));
       list.add(bundle.getString("wuni.default.images.text_39"));
       list.add(bundle.getString("wuni.default.images_id_40"));
       list.add(bundle.getString("wuni.default.images.text_40"));
       list.add(bundle.getString("wuni.default.images_id_41"));
       list.add(bundle.getString("wuni.default.images.text_41"));
       list.add(bundle.getString("wuni.default.images_id_42"));
       list.add(bundle.getString("wuni.default.images.text_42"));
       list.add(bundle.getString("wuni.default.images_id_43"));
       list.add(bundle.getString("wuni.default.images.text_43"));
       list.add(bundle.getString("wuni.default.images_id_44"));
       list.add(bundle.getString("wuni.default.images.text_44"));
       list.add(bundle.getString("wuni.default.images_id_45"));
       list.add(bundle.getString("wuni.default.images.text_45"));
       list.add(bundle.getString("wuni.default.images_id_46"));
       list.add(bundle.getString("wuni.default.images.text_46"));
       list.add(bundle.getString("wuni.default.images_id_47"));
       list.add(bundle.getString("wuni.default.images.text_47"));
       list.add(bundle.getString("wuni.default.images_id_48"));
       list.add(bundle.getString("wuni.default.images.text_48"));
       list.add(bundle.getString("wuni.default.images_id_49"));
       list.add(bundle.getString("wuni.default.images.text_49"));
       list.add(bundle.getString("wuni.default.images_id_50"));
       list.add(bundle.getString("wuni.default.images.text_50"));
       return list;
   }

    /** 
     *  Function that returns the Study mode description for the given study mode ID
     * @param StudyModeId, request
     * @return StudyModeDescription as String 
     */

    public String getStudyModeDesc(String studyModeId, HttpServletRequest request) 
    {
       String studyModeDesc   =  "";
       try {
          DataModel dataModel = new DataModel();
          Vector vector       =  new Vector();
          vector.clear();
          vector.add(studyModeId);       
          studyModeDesc = dataModel.getString("wu_course_search_pkg.get_study_mode_desc_fn", vector);
       } catch(Exception exception) {
          exception.printStackTrace();
       }
       return studyModeDesc;
    }
    
    /** 
      *  Function that returns the Study Level description for the given study mode ID
      * @param studyLevelId, request
      * @return StudyLevelDescription as String 
      */

    public String getStudyLevelDesc(String studyLevelId, HttpServletRequest request) 
    {
       String studyLevelDesc  =  "";
       try {
          DataModel dataModel =  new DataModel();
          Vector vector       =  new Vector();
          vector.clear();
          vector.add(studyLevelId);
          studyLevelDesc = dataModel.getString("wu_course_search_pkg.get_study_level_desc_fn", vector);
       } catch(Exception exception) {
          exception.printStackTrace();
      }
     return studyLevelDesc;
    }

    /**
    *  function to get the details about the accociation by passing the accociationType and userId
    * @param associationType, userId, sortOrder, request
    * @return college details as collection object. 
    */
     
     /* public ArrayList getBasketContent(String associationType, String userId, String sortOrder, HttpServletRequest request)
      {
          DataModel dataModel =   new DataModel();
          ResultSet resultset =   null;
          ArrayList list      =   new ArrayList();
          //added for 24th March Release
                 userId = userId !=null && !userId.equals("0") && userId.trim().length()>=0 ? userId : "";
          String basketId = (userId != null && (userId.equals("0") || userId.trim().length()==0)) ? checkCookieStatus(request) : "";
                 basketId = (userId !=null && userId.trim().length()>0) ? "" : basketId;
                 userId   =  (basketId !=null && basketId.trim().length()>0) ? "" : userId;
          if((userId !=null && !userId.equals("0") && userId.trim().length()==0) && (basketId !=null && basketId.trim().length()==0)){
               userId="0";
               basketId="";
           }
          //end //
          try {
             Vector vector = new Vector();
             vector.clear ();
             vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
             vector.add(userId);
             vector.add(basketId);
             vector.add(associationType);
             vector.add(sortOrder);
             resultset = dataModel.getResultSet("Wu_basket_Pkg.get_basket_contents_fn", vector);
             while(resultset.next()) {
                list.add(resultset.getString("assoc_id")); 
                list.add(resultset.getString("assoc_name"));
                list.add(resultset.getString("short_list_flag"));
             }
           } catch(Exception exception) {
               exception.printStackTrace();
           } finally {
                dataModel.closeCursor(resultset);
           }
         return list;
      }/*

     /**
     *  function to get the member details about the basket by passing the associationType and userId
     * @param associationType, userId, sortOrder, request
     * @return college details as collection object. 
     */
     
      /*public ArrayList getMembersBasketContent(String associationType, String userId, String sortOrder, HttpServletRequest request)
      {
          DataModel dataModel =   new DataModel();
          ResultSet resultset =   null;
          ArrayList list      =   new ArrayList();
          BasketBean bean     =   null;
          //added for 24th March Release
                 userId = userId !=null && !userId.equals("0") && userId.trim().length()>=0 ? userId : "";
          String basketId = (userId != null && (userId.equals("0") || userId.trim().length()==0)) ? checkCookieStatus(request) : "";
                 basketId = (userId !=null && userId.trim().length()>0) ? "" : basketId;
                  userId   =  (basketId !=null && basketId.trim().length()>0) ? "" : userId;
           if((userId !=null && !userId.equals("0") && userId.trim().length()==0) && (basketId !=null && basketId.trim().length()==0)){
              userId="0";
              basketId="";
           }
          //end //
          try {
             Vector vector =  new Vector();
             vector.clear ();
             vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
             vector.add(userId);
             vector.add(basketId);
             vector.add(associationType);
             vector.add(sortOrder);
             resultset = dataModel.getResultSet("Wu_basket_Pkg.get_basket_contents_fn", vector);
             while(resultset.next()) {
                bean = new BasketBean();
                bean.setSubjectId(resultset.getString("assoc_id"));
                bean.setSubjectName(resultset.getString("assoc_name"));
                bean.setSubjectCode(resultset.getString("assoc_code"));
                list.add(bean);
            }
          } catch(Exception exception)  {
              exception.printStackTrace();
          } finally  {
              dataModel.closeCursor(resultset);
          }
         return list;
      }*/
      
      /**
      *  function to get the count about the Basket type(college or course or subject)  
      * @param accosiationType, userId, request
      * @return Basket count as string
      */
      
      public String getBasketCount(String cookieBaketId, HttpServletRequest request)
      {
        DataModel dataModel  =   new DataModel();
        Vector vector        =   new Vector();
        BigDecimal basketCount   =   null;
        String basketCnt   =   null;
        String basketId = cookieBaketId != null && cookieBaketId.length() > 0 ? cookieBaketId : checkCookieStatus(request);
               basketId = basketId != null && basketId.length() > 0 ? basketId : null;
            
        try {
          /*vector.clear ();
          vector.add(basketId);
          basketCount = dataModel.getString("Wu_basket_Pkg.get_basket_count_fn", vector);*/
          
          ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
          RegistrationBean registrationBean = new RegistrationBean();
          registrationBean.setBasketId(basketId);
          Map basketCountMap = commonBusiness.getBasketCount(registrationBean);
          if(basketCountMap != null && !basketCountMap.isEmpty()){
            basketCount = (BigDecimal)basketCountMap.get("Ret_Cnt");
            if(basketCount != null){
              basketCnt = basketCount.toString();
            }
          }
        } catch(Exception exception)  {
          exception.printStackTrace();
        }
        return basketCnt;
      }

      /**
      *  function to remove the basket in to basket table
      * @param associationId, associationType, request
      * @return Basket Id. 
      */
      
      public String removeBasketDetails(String associationId, String associationType, String basketId, String comparepage, HttpServletRequest request)
      {
         DataModel dataModel =   new DataModel();
         Vector vector       =   new Vector();
         String msg = null;
         comparepage = ("CLEARING".equalsIgnoreCase(comparepage)) ? "C" : "N";
         String logUserId = new SessionData().getData(request, "y");
         try {
             vector.clear ();
             vector.add(associationId);
             vector.add(associationType.toUpperCase());
             vector.add(basketId);
            vector.add(logUserId);
             vector.add(comparepage);
            
             msg = dataModel.getString("Wu_basket_Pkg.remove_from_basket_fn", vector);
          } catch(Exception exception) {
                exception.printStackTrace();
          }
          return msg;
      } 
      
  
  public String removeClearingBasketDetails(String associationId, String associationType, String basketId,String clearingType, HttpServletRequest request)
  {
     DataModel dataModel =   new DataModel();
     Vector vector       =   new Vector();
     String msg = null;
     try {
         vector.clear ();
         vector.add(associationId);
         vector.add(basketId);
         vector.add(clearingType);
         vector.add(associationType.toUpperCase());
         
         msg = dataModel.getString("MYWU_COMPARE_PKG.remove_from_basket_fn", vector);
      } catch(Exception exception) {
            exception.printStackTrace();
      }
      return msg;
  } 


    /**
     *  function that generates the General reviews in the specified course 
     * @param request, servletContext, courseId, pageNo, recordDisplay, orderBy, reviewStatus
     * @return General review list as collection object
     */
     
    public List generateCourseReviewList(HttpServletRequest request, ServletContext context, String courseId, String pageNo, String recordDisplay, String orderBy, String reviewStatus)
    {
       DataModel dataModel   =   new DataModel();
       Vector vector         =   new Vector();
       ArrayList list        =   new ArrayList();
       ResultSet resultset   =   null;    
       ReviewListBean bean   =   null;
       String collegeName    =   "";
       vector.clear();
       vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
       vector.add(courseId);
       vector.add(pageNo);
       vector.add(recordDisplay);
       vector.add(orderBy);
       vector.add(reviewStatus);
       try {
           resultset = dataModel.getResultSet("Wu_Course_Search_Pkg.get_crs_review_list_fn", vector);
           while(resultset.next()) {
               bean = new ReviewListBean();
               bean.setCourseId(courseId);
               bean.setReviewId(resultset.getString("review_id"));
               bean.setReviewTitle(resultset.getString("review_title"));
               bean.setCollegeName(resultset.getString("college_name"));
             bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
               bean.setUserId(resultset.getString("user_id"));
               bean.setUserName(resultset.getString("user_name"));
               bean.setCourseName(resultset.getString("course_name"));
               bean.setOverallRatingComment(resultset.getString("overall_rating_comments"));
               bean.setStudCurrentStatus(resultset.getString("at_uni"));
               bean.setUserGraduate(resultset.getString("at_uni"));
               bean.setCollegeId(resultset.getString("college_id"));
               bean.setOverallRating(resultset.getString("overall_rating"));
               bean.setOverAllRating(resultset.getString("overall_rating"));
               bean.setNationality(resultset.getString("nationality"));
               bean.setGender(resultset.getString("gender"));
               bean.setUserGender(resultset.getString("gender"));
               collegeName = resultset.getString("college_name");
             //NEED TODO - need to remove for Ram's reference
               /*String userImageName =  resultset.getString("user_photo");
               String userThumbName =  (userImageName !=null ? userImageName.substring(0,userImageName.lastIndexOf(".")) +"T"+ userImageName.substring(userImageName.lastIndexOf("."),userImageName.length()) : userImageName);
               bean.setUserImage(userThumbName);
               bean.setUserImageLarge(userImageName);*/
               list.add(bean);
           }
       } catch(Exception exception) {
           exception.printStackTrace();
       }  finally {
          dataModel.closeCursor(resultset);
       }       
       return list;
    }

    /**
     *  function that counts number of review and return the count value 
     * @param request, courseId, reviewStatus, reviewType
     * @return Review count as String
     */
     
     public String getCourseReviewCount(HttpServletRequest request, String courseId, String reviewStatus, String reviewType) 
     {
        String reviewCount  =   "0";
        try{
            DataModel dataModel = new DataModel();
            Vector vector =   new Vector();
            vector.clear();
            vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);             // affliate id 
            vector.add(courseId);                                          // college  id
            vector.add(reviewStatus);                                       // A- for live reviews
            vector.add(reviewType);                                         // G - general Review -> V- Video Review
            reviewCount = dataModel.getString("Wu_Course_Search_Pkg.get_review_crs_count_fn", vector);
          } catch(Exception exception) {
              exception.printStackTrace();
          }
         return reviewCount;
      }

      /**
      *  function to get the subjectcode or subjectId by passing subjectId or subjectCode
      * @param subjectId, subjectcode, request
      * @return subjectId. or subjectCode as String
      */
      
      public String getSubjectCode(String subjectId, String subjectCode, HttpServletRequest request)
      {
         DataModel dataModel = new DataModel();
         Vector vector        = new Vector();
         try {
            vector.clear ();
            vector.add(subjectId);
            vector.add(subjectCode);
            subjectCode = dataModel.getString("Wu_Course_Search_Pkg.get_subject_category_code_fn", vector);
          } catch(Exception exception) {
               exception.printStackTrace();
           }
          return subjectCode;
       }

      /**
      *  function to will return the subject name for the given subject Id or category id   
      * @param subjectid, categpry_code
      * @return subjectName as String . 
      */
      
      public String getSubjectName(String subjectId, String caregoryCode, HttpServletRequest request)
      {
         DataModel dataModel  =  new DataModel();
         Vector parameters        =  new Vector();
         String subjectName   =  "";
         try {
            parameters.add(GlobalConstants.WHATUNI_AFFILATE_ID);
            parameters.add(subjectId);
            parameters.add(caregoryCode);
            subjectName = dataModel.getString("WU_COURSE_SEARCH_PKG.GET_SUBJECT_NAME_FN", parameters);
          } catch(Exception exception) {
                exception.printStackTrace();
          }
         return subjectName;
      }

  /**
   * 
   * this will return country name.
   * 
   * @param postTown
   * @param postCodePrefix
   * @return
   */
      
     public String getCountyName(String postTown, String postCodePrefix)
     {
        DataModel dataModel = new DataModel();
        Vector parameters = new Vector();
        String countyName = "";
        try {
          parameters.add(postTown);
          parameters.add(postCodePrefix);
          countyName = dataModel.getString("WU_COURSE_SEARCH_PKG.GET_COUNTY_NAME_FN", parameters);
        } catch(Exception exception) {
            exception.printStackTrace();
        }
        return countyName;
      }
       
    /**
    *  function to check whether the friend already existing for the logged user 
    * @param userId, friendId, affliateId
    * @return true or false as String
    */
    
     public String checkFriendsRequest(String userId, String friendId, String affliateId) 
     {
         Vector vector       =  new Vector();
         DataModel dataModel =  new DataModel();
         String status       =  "";
         try {
             vector.add(affliateId);
             vector.add(userId);
             vector.add(friendId);
             status = dataModel.getString("wu_members_pkg.chk_friends_exist_fn", vector);
         } catch(Exception exception) {
             exception.printStackTrace();     
         }
        return status;        
     }

    /**
     *  This function will return the collection of messages stored in the message table for the logged user 
     * @param affliateId, userId, groupId, messageStatusDesc, messageTypeDesc, messageId, request
     * @return Mailcontents as arraylist 
     * messageStatusDesc IN ----- INBOX, SENT, DRAFT, PENDING                        
     * messageTypeDesc   IN ------ EMAILS, GROUP_EMAILS, FRIENDS_REQUEST              
     * messageId         IN ----- Message ID of a particular message to be viewed    
     */

    public ArrayList getMessageDetails(String affliateId, String userId, String groupId, String messageStatusDesc, String messageTypeDesc, String messageId, String pageNo, String record_per_page, String orderBy,  HttpServletRequest request, ServletContext context) 
    {
       ArrayList arrayList   =   new ArrayList();
       Vector vector         =   new Vector();
       DataModel dataModel   =    new DataModel();
       ResultSet resultset   =   null;
       try {
         vector.clear();
         vector.addElement(affliateId);
         vector.addElement(userId);
         vector.addElement(groupId);
         vector.addElement(messageStatusDesc);
         vector.addElement(messageTypeDesc);
         vector.addElement(messageId);
         vector.addElement(pageNo);
         vector.addElement(record_per_page);
         vector.addElement(orderBy);
         resultset = dataModel.getResultSet("wu_message_pkg.get_msg_details_fn", vector);
        while (resultset.next()) {
            MessageBean bean = new MessageBean();
            bean.setMessageId(resultset.getString("msg_id"));
            bean.setReceiverId(resultset.getString("receiver_id"));
            bean.setSenderId(resultset.getString("sender_id"));
            bean.setGroupId(resultset.getString("group_id"));
            bean.setRecieverAddress(resultset.getString("receiver"));
            bean.setSenderAddress(resultset.getString("sender"));
            bean.setMessageDate(resultset.getString("msg_date"));
            String subject = resultset.getString("msg_subject") !=null && resultset.getString("msg_subject").trim().length() >= 30 ? resultset.getString("msg_subject").substring(0, 30) + "..." :  resultset.getString("msg_subject").trim();
            bean.setMessageSubject(subject);
            bean.setInboxStatus(resultset.getString("msg_status"));
            bean.setMessageContent(resultset.getString("msg_body"));
            String senderImage      =  resultset.getString("sender_photo");
            String senderThumbImage =  (senderImage !=null ? senderImage.substring(0, senderImage.lastIndexOf(".")) +"T"+ senderImage.substring(senderImage.lastIndexOf("."), senderImage.length()) : senderImage);
             bean.setSenderImage(senderThumbImage);
             bean.setSenderImageLarge(senderImage);
             String receiverImage      =  resultset.getString("receiver_photo");
             String receiverThumbImage =  (receiverImage !=null ? receiverImage.substring(0, receiverImage.lastIndexOf(".")) +"T"+ receiverImage.substring(receiverImage.lastIndexOf("."), receiverImage.length()) : receiverImage);
             bean.setReceiverImage(receiverThumbImage);
             bean.setReceiverImageLarge(receiverImage);
             arrayList.add(bean);
         }
     } catch (Exception exception) {
        exception.printStackTrace();
     } finally {
          dataModel.closeCursor(resultset);
     }
     return arrayList;
   }

    /**
     *  This function will return number of unread messages of the the user for the given messagetype 
     * @param userId, messageType
     * @return count as String 
     * messageType IN ----- INBOX, SENT, DRAFT, PENDING      
     */
    
    public String getUnreadMessageCount(String userId, String messageType) 
    {
        String unReadMessageCount = "0";
        Vector vector  =  new Vector();
        vector.clear();
        vector.add(userId);
        vector.add(messageType);
        DataModel dataModel = new DataModel();
        try {
           unReadMessageCount = dataModel.getString("wu_message_pkg.get_message_cnt_fn", vector);
        } catch(Exception exception) {
           exception.printStackTrace();
        }
        return unReadMessageCount;
    }    
       
   /**
    *  This function will return total number of messages for the user for the given messagetype 
    * @param affliateId, userId, groupId, messageStatusDesc,  messageId, servletContext
    * @return count as String 
    * messageType IN ----- INBOX, SENT, DRAFT, PENDING      
    */
    
    public String getMessageCount(String affliateId, String userId, String groupId, String messageStatusDesc, String messageTypeDesc, String messageId, HttpServletRequest request, ServletContext servletContext) 
    {
        String messageCount     =   "0";
        Vector vector =  new Vector();
        vector.clear();
        vector.clear();
        vector.addElement(affliateId);
        vector.addElement(userId);
        vector.addElement(groupId);
        vector.addElement(messageStatusDesc);
        vector.addElement(messageTypeDesc);
        vector.addElement(messageId);
        DataModel dataModel = new DataModel();
        try {
           messageCount = dataModel.getString("wu_message_pkg.get_msg_details_cnt_fn", vector);
        } catch(Exception exception)  { 
             exception.printStackTrace();
        }
       return messageCount;
    }    
       
    /**
     *  Function that returns the Given string in Title Case format 
     * @param inString 
     * @return outString as  String
     */
    
     public String getTitleCaseFn(String inString) 
     {
        String outString  = new CommonUtil().toTitleCase(inString);
        return outString;
     }
     
    /**
     *  This function will delete the  message of a particular user in particular folder
     * @param userId, folderType, messageId, friendId, changeStatus, link1, link2, affliateId 
     * @return void
     * folderType   IN ------ INBOX, SENT, DRAFT, PENDING      
     * friendId     IN ------ Applicable for FRIENDS_REQUEST only
     * changeStatus IN ------ DELETED, ACCEPTED, DECLINED     
     */
    
    public void deleteMessage(String userId, String folderType, String messageId, String friendId, String changeStatus, String link1, String link2, String affliateId) 
    {
        DataModel dataModel  =  new DataModel();
        Vector vector        =  new Vector();
        String status         =   "";
        vector.clear();
        vector.add(userId);
        vector.add(folderType);
        vector.add(messageId);
        vector.add(friendId);
        vector.add(changeStatus);
        vector.add(link1);
        vector.add(link2);
        vector.add(affliateId);
        try {       
           status = dataModel.getString("wu_message_pkg.change_message_status_fn", vector);
        } catch(Exception exception) {
            exception.printStackTrace();
        }
    }

   /**
     *  Function will send the draft message saved in the draft box to the user.  
     * @param affliateId, receiverId, groupId, messageTypeDesc, messageId, url1, url2
     * @return status as String
     */
   
   public String sendDraftMessage(String affliateId, String receiverId, String groupId, String messageTypeDesc, String messageId, String url1, String url2) 
   {
       DataModel dataModel =  new DataModel();
       Vector vector       =  new Vector();
       String status       =  "";
       vector.clear();
       vector.addElement(affliateId);    
       vector.addElement(receiverId);
       vector.addElement(groupId);
       vector.addElement(messageTypeDesc);
       vector.addElement(messageId);
       vector.addElement(url1);
       vector.addElement(url2);
       try {
          status = dataModel.getString("wu_message_pkg.send_draft_msg_fn", vector); 
       } catch(Exception exception) {
          exception.printStackTrace();
      }
      return status;
    }
    
    /**
     *  function to get the all the friends for the specified user 
     * @param pageNo, recordCount, sortBy,  userId, request, servletContext
     * @return friend list as Collection object.
     */
    
    /*public ArrayList getFriendsList(String pageNo, String recordCount, String sortBy, String userId, HttpServletRequest request, ServletContext context)
    {
        ArrayList friendsList =   new ArrayList();
        Vector vector         =   new Vector();
        DataModel dataModel   =   new DataModel();
        ResultSet resultset   =   null;
        try{
            vector.addElement( GlobalConstants.WHATUNI_AFFILATE_ID);
            vector.addElement(userId);
            vector.addElement(pageNo);
            vector.addElement(recordCount);
            vector.addElement(sortBy);
            resultset = dataModel.getResultSet("wu_members_pkg.get_friends_list_fn", vector);
            FriendsBean bean = null;
            while (resultset.next()) {
               bean   =  new FriendsBean();
               bean.setFriendId(resultset.getString("friend_id"));
               bean.setFriendName(resultset.getString("friend_name"));
               bean.setTown(resultset.getString("town"));
               bean.setEmailId(resultset.getString("email"));
               bean.setDataOfBirth(resultset.getString("dob"));
               bean.setGender(resultset.getString("gender"));
               bean.setNationality(resultset.getString("nationality"));
               bean.setYearOfGraduation(resultset.getString("year_of_graduation"));
               bean.setIama(resultset.getString("graduation"));
               //bean.setUserImage(resultset.getString("user_photo"));
               bean.setHomeTown(resultset.getString("hometown"));
               ArrayList subjectList = getMembersBasketContent("G", resultset.getString("friend_id"), "UNINAME", request);
               if(subjectList !=null && subjectList.size()>0) {
                  bean.setSubjectInterested(subjectList);
                  bean.setSubjectCount(getBasketCount("", request));
               }
               /* TO SET THE FRIENDS PHOTO IN TO BEAN */
                //NEED TODO - need to remove for Ram's reference
               /*String userImageName =  resultset.getString("user_photo");
               String userThumbName =  (userImageName !=null ? userImageName.substring(0,userImageName.lastIndexOf(".")) +"T"+ userImageName.substring(userImageName.lastIndexOf("."),userImageName.length()) : userImageName);
               bean.setUserImage(userThumbName);
               bean.setUserLargeImage(userImageName);*/
              /* friendsList.add(bean);
            }
          } catch (Exception exception) {
            exception.printStackTrace();
          } finally {
              dataModel.closeCursor(resultset);
          }
         return friendsList;
      }*/

   /**
    *  function to get the friends for loading friends pod in whatuni
    * @param userId, request, servletContext
    * @return void
    */
    
    /*public void loadFriendsPod(String userId, HttpServletRequest request, ServletContext servletContext)
    {
        HttpSession session = request.getSession();
        if(session.getAttribute("userFriendsList") !=null) {
            session.removeAttribute("userFriendsList");
        }
        String pageNo       =   "1";
        String recordCount  =   "2";
        String sortBy       =   "USER_ID";
        ArrayList userFriendsList = getFriendsList(pageNo, recordCount, sortBy, userId, request, servletContext);
        if(userFriendsList != null && userFriendsList.size() > 0) {
            session.setAttribute("userFriendsList", userFriendsList);
        }    
    }*/    

    /**
     *  function to get the friends count by passing userid
     * @param userId, request
     * @return string
     */
    
    public String getFriendsCount(String userId, HttpServletRequest request) 
    {
        DataModel dataModel =   new DataModel();
        String friendsCount =  "";
        Vector vector  =   new Vector();
        vector.clear();
        vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
        vector.add(userId);
        try {   
            friendsCount = dataModel.getString("wu_members_pkg.get_friends_list_cnt_fn", vector);
        } catch(Exception exception) {
            exception.printStackTrace();
        }
        return friendsCount;
    }

  /**
   * function to get the details all the records from the baskets to display in the left pod of each page
   * 
   * @param request
   * @param response
   * @return
   */
  public void getBasketPodContent(HttpServletRequest request, HttpServletResponse response) {
    String cookie_basket_id = checkCookieStatus(request);
    String basketCount = null;
    try
    {
      basketCount = getBasketCount(cookie_basket_id, request);
    }catch(Exception e)
    {
      e.printStackTrace();
    }
    String cookieBasketCount = GenericValidator.isBlankOrNull(basketCount) ? "0" : basketCount;
   // int userBasketsContentCount = 0;
   // String userId = new SessionData().getData(request,"y");
   // userBasketsContentCount = getUserBasketContentCount(userId, request);
    int basketContentCount = Integer.parseInt(cookieBasketCount);    
    request.getSession().setAttribute("basketpodcollegecount", String.valueOf(basketContentCount));
  }
  

  

    /**
      *  function to get the latest members (registered users) details
      * @param affliateId, pageNo, recordCount, sortBy
      * @return member list as a List Collection.
      */
    
     public ArrayList getLatestMemberFunction(String affliateId, String pageNo, String recordCount, String sortBy, String userType,  HttpServletRequest request, ServletContext context)        
       {
           Vector vector        =   new Vector();
           vector.clear();
           DataModel dataModel  =   new DataModel();
           ResultSet resultset  =   null;
           ArrayList list       =   new ArrayList();
           try {
               vector.add(affliateId);
               vector.add(userType);
               vector.add(pageNo);
               vector.add(recordCount);
               vector.add(sortBy);
               resultset = dataModel.getResultSet("wu_members_pkg.get_latest_members_fn", vector);
               while(resultset.next()) {
                 MembersSearchBean bean   =  new MembersSearchBean();
                 bean.setUserName(resultset.getString("username"));
                 bean.setForeName(resultset.getString("forename"));
                 bean.setGender(resultset.getString("gender"));
                 bean.setUserId(resultset.getString("user_id"));
                 bean.setNationality(resultset.getString("nationality"));
                 bean.setYearOfGraduation(resultset.getString("year_of_graduation"));
                 bean.setIama(resultset.getString("graduation"));
                 //bean.setUserImage(resultset.getString("user_photo"));
                 bean.setSubjectLinkList(resultset.getString("subject_basket_link"));
                 //NEED TODO - need to remove for Ram's reference
                 /*String userImageName = resultset.getString("user_photo");
                 String userThumbName = (userImageName !=null ? userImageName.substring(0,userImageName.lastIndexOf(".")) +"T"+ userImageName.substring(userImageName.lastIndexOf("."),userImageName.length()) : userImageName);
                 bean.setUserImage(userThumbName);
                 bean.setUserLargeImage(userImageName);*/
                 list.add(bean);
               }
           } catch(Exception exception) {
               exception.printStackTrace();    
           } finally {
               dataModel.closeCursor(resultset);
           }
           return list;
       }

    /**
      *  function to get the latest members (registered users) details
      * @param servletContext, request
      * @return void
      */
    
    public void loadLatestMembersPod(HttpServletRequest request, ServletContext servletContext)
    {
        HttpSession session = request.getSession();
        if(session.getAttribute("latestMemberList") !=null) {
             session.removeAttribute("latestMemberList");
        }
        String affliateId = GlobalConstants.WHATUNI_AFFILATE_ID;
        ArrayList latestMemberList = getLatestMemberPodFunction(affliateId,  request, servletContext );
        if(latestMemberList != null && latestMemberList.size() > 0)  {
            session.setAttribute("latestMemberList", latestMemberList);
        }    
    }    

   /**
     *  function to get the latest members (registered users) details
     * @param affliateId, pageNo, recordCount, sortBy
     * @return member list as a List Collection.
     */
    
    public ArrayList getLatestMemberPodFunction(String affliateId, HttpServletRequest request, ServletContext context)        
    {
       Vector vector  =  new Vector();
       vector.clear();
       DataModel dataModel  =   new DataModel();
       ResultSet resultset  =   null;
       ArrayList list       =   new ArrayList();
       try  {
            vector.add(affliateId);
            resultset = dataModel.getResultSet("wu_homepage_pkg.get_latest_members_pod_fn", vector);
            while(resultset.next()) {
                MembersSearchBean bean   =  new MembersSearchBean();
                bean.setUserName(resultset.getString("username"));
                bean.setGender(resultset.getString("gender"));
                bean.setUserId(resultset.getString("user_id"));
                bean.setNationality(resultset.getString("nationality"));
                bean.setHomeTown(resultset.getString("hometown"));
                bean.setYearOfGraduation(resultset.getString("year_of_graduation"));
                bean.setIama(resultset.getString("i_am_a"));
              //NEED TODO - need to remove for Ram's reference
                /*bean.setUserImage(resultset.getString("user_photo"));
                String userImageName =  resultset.getString("user_photo");
                String userThumbName =  (userImageName !=null ? userImageName.substring(0,userImageName.lastIndexOf(".")) +"T"+ userImageName.substring(userImageName.lastIndexOf("."),userImageName.length()) : userImageName);
                bean.setUserImage(userThumbName);
                bean.setUserLargeImage(userImageName);*/
                list.add(bean);
            }
        } catch(Exception exception) {
            exception.printStackTrace();    
        } finally {
            dataModel.closeCursor(resultset);
        }
       return list;
    }

    /**
      *  function to get the members count
      * @param affliateId
      * @return String
      */
    
    public String getMemberCount(String affliateId, String userType) 
    {
       DataModel dataModel=   new DataModel();
       Vector vector      =   new Vector();
       String memberCount =   "";
       vector.clear();
       vector.add(affliateId);
       vector.add(userType);
       try {   
            memberCount = dataModel.getString("wu_members_pkg.get_latest_members_count_fn", vector);
        } catch(Exception memberCountException) {
            memberCountException.printStackTrace();
        }
        return memberCount;
    }

  /**
      *  function to get the related members details (members who are related to search)
      * @param affliateId, collegeId, assocTypeCode, pageNo, recordCount, servletContext
      * @return related member list as a List Collection.
      */
  public ArrayList getCollegeRelatedMemberFunction(String affliateId, String collegeId, String iamQuestion, String pageNo, String recordCount, HttpServletRequest request, ServletContext context, String resultType) {
    Vector vector = new Vector();
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    ArrayList list = new ArrayList();
    try {
      vector.clear();
      vector.add(affliateId);
      vector.add(collegeId);
      vector.add(iamQuestion);
      vector.add(pageNo);
      vector.add(recordCount);
      resultset = dataModel.getResultSet("wu_members_pkg.get_uni_related_members_fn", vector);
      while (resultset.next()) {
        MembersSearchBean bean = new MembersSearchBean();
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
        bean.setUserName(resultset.getString("username"));
        bean.setForeName(resultset.getString("forename"));
        bean.setGender(resultset.getString("gender"));
        bean.setUserId(resultset.getString("user_id"));
        bean.setNationality(resultset.getString("nationality"));
        bean.setYearOfGraduation(resultset.getString("year_of_graduation"));
        bean.setIama(resultset.getString("graduation"));
        //bean.setUserImage(resultset.getString("user_photo"));
        bean.setSubjectLinkList(resultset.getString("subject_basket_link"));
        //NEED TODO - need to remove for Ram's reference
        /*String userImageName = resultset.getString("user_photo");
        String userThumbName = (userImageName != null ? userImageName.substring(0, userImageName.lastIndexOf(".")) + "T" + userImageName.substring(userImageName.lastIndexOf("."), userImageName.length()) : userImageName);
        bean.setUserImage(userThumbName);
        bean.setUserLargeImage(userImageName);*/
        request.setAttribute("relatedMembersCount", resultset.getString("total_records"));
        request.setAttribute("titleMessage", "Members of \"" + bean.getCollegeNameDisplay() + "\"");
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }

    /**
      *  function to get the related members details (members who are related to search)
      * @param affliateId, subjectId, assocTypeCode, pageNo, recordCount, servletContext
      * @return related member list as a List Collection.
     */

     public ArrayList getSubjectRelatedMemberFunction(String affliateId, String subjectId, String iamQuestion, String pageNo, String recordCount, HttpServletRequest request, ServletContext context, String reportType)        
     {
        Vector vector         =   new Vector();
        DataModel dataModel   =   new DataModel();
        ResultSet resultset   =   null;
        ArrayList list        =   new ArrayList();
        try {
            vector.clear();
            vector.add(affliateId);
            vector.add(subjectId);
            vector.add(iamQuestion);
            vector.add(pageNo);
            vector.add(recordCount);
            resultset = dataModel.getResultSet("wu_members_pkg.get_sub_related_members_fn", vector);
            while(resultset.next()) {
                MembersSearchBean bean   =  new MembersSearchBean();
                bean.setUserName(resultset.getString("username"));
                bean.setForeName(resultset.getString("forename"));
                bean.setGender(resultset.getString("gender"));
                bean.setUserId(resultset.getString("user_id"));
                bean.setNationality(resultset.getString("nationality"));
                bean.setYearOfGraduation(resultset.getString("year_of_graduation"));
                bean.setIama(resultset.getString("graduation"));
                //bean.setUserImage(resultset.getString("user_photo"));
                bean.setSubjectLinkList(resultset.getString("subject_basket_link"));
              //NEED TODO - need to remove for Ram's reference
                /*String userImageName =  resultset.getString("user_photo");
                String userThumbName =  (userImageName !=null ? userImageName.substring(0,userImageName.lastIndexOf(".")) +"T"+ userImageName.substring(userImageName.lastIndexOf("."),userImageName.length()) : userImageName);
                bean.setUserImage(userThumbName);
                bean.setUserLargeImage(userImageName);*/
                request.setAttribute("titleMessage", "Members interested in  \"" +resultset.getString("subject_name")+"\""); 
                list.add(bean);
            }
          } catch(Exception exception) {
              exception.printStackTrace();    
          } finally {
             dataModel.closeCursor(resultset);
          }
          return list;
      }
    
    /**
      *  function to get the related members details (members who are related to search)
      * @param search_text, collegeId, subjectId, assocTypeCode, pageNo, recordCount, servletContext
      * @return related member list as a List Collection.
      */
     
     public ArrayList loadRelatedMembersPod(String collegeId, String subjectId, String search_text, HttpServletRequest request, ServletContext servletContext)
     {
            HttpSession session = request.getSession();
            if(session.getAttribute("relatedMemberList") !=null ) {
                 session.removeAttribute("relatedMemberList");
            }
            String affliateId  =   GlobalConstants.WHATUNI_AFFILATE_ID;
            String pageNo      =   "1";
            String recordCount =   "5";
            ArrayList latestMemberList  =   new ArrayList();
            if(subjectId !=null && subjectId.trim().length()>0) {
                latestMemberList = getSubjectRelatedMemberFunction(affliateId, subjectId, "", pageNo, recordCount, request,  servletContext, "POD");
                request.setAttribute("sid", subjectId);
            }
            if(collegeId !=null && collegeId.trim().length()>0){
                latestMemberList = new GlobalFunction().loadUniversityRelatedMembersPod(collegeId, request);
                request.setAttribute("cid", collegeId);
            }
            if(search_text !=null && search_text.trim().length()>0) {
                request.setAttribute("search_text", search_text);
            }
            return latestMemberList;         
     }    

      /**
        *  function to get the related members count 
        * @param affliateId, search_text, collegeId, subjectId, assocTypeCode, pageNo, recordCount, servletContext
        * @return related member count as string
        */
       
       public String getSubjectRelatedMemberCount(String affliateId, String subjectId, String iamQuestion) 
       {
           DataModel dataModel =   new DataModel();
           Vector vector       =   new Vector();
           String memberCount  =   "";
           vector.clear();
           vector.add(affliateId);
           vector.add(subjectId);
           vector.add(iamQuestion);
           try {   
               memberCount = dataModel.getString("wu_members_pkg.get_sub_related_members_cnt_fn", vector);
           } catch(Exception exception) {
               exception.printStackTrace();
           }
           return memberCount;
       }
       
      /**
      *  Function that returns the arraylist contains the information like college details 
      * by the user for the given user id 
      * @param userId, request
      * @return userName as String
      */
       
      /*public ArrayList getUserCollegeList(String userId, HttpServletRequest request) 
      {
         ProfileBean bean    =   null;
         ResultSet resultset =   null;
         ArrayList list      =   new ArrayList();
         DataModel dataModel =   new DataModel();
         Vector vector       =   new Vector();
         vector.clear();
         vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
         vector.add(userId);
         try {
            resultset   =   dataModel.getResultSet("wu_profile_pkg.get_my_uni_details_fn", vector);
            while(resultset.next()) {
               bean = new ProfileBean();
               bean.setCollegeId(resultset.getString("college_id"));
               bean.setCollegeName(resultset.getString("college_name"));
              bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
               bean.setStatusFlag(resultset.getString("flag"));
               list.add(bean);
            }
         } catch(Exception exception) {
               exception.printStackTrace();
         } finally {
            dataModel.closeCursor(resultset);
         }                 
        return list;
     }*/ 
       
    /**
     *  Function that returns the arraylist contains the information like course details 
     * by the user for the given user id 
     * @param userId, request
     * @return userName as String
    */
        
    /*public ArrayList getUserCourseList(String userId, HttpServletRequest request) 
    {
       ProfileBean bean    =   null;
       ResultSet resultset =   null;
       ArrayList list      =   new ArrayList();
       DataModel dataModel =   new DataModel();
       Vector vector       =   new Vector();
       vector.clear();
       vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
       vector.add(userId);
       try {
         resultset  = dataModel.getResultSet("wu_profile_pkg.get_my_courses_details_fn", vector);
         while(resultset.next()) {
             bean = new ProfileBean();
             bean.setCollegeId(resultset.getString("college_id"));
             bean.setCollegeName(resultset.getString("college_name"));
            bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
             bean.setCourseId(resultset.getString("course_id"));
             bean.setCourseName(resultset.getString("course_name"));
             bean.setCourseDeletedFlag(resultset.getString("course_exist"));
             bean.setStatusFlag(resultset.getString("flag"));
             bean.setSeoStudyLevelText(resultset.getString("seo_study_level_text"));
             list.add(bean);
          }
       } catch(Exception exception) {
           exception.printStackTrace();
       } finally {
           dataModel.closeCursor(resultset);
       }                 
      return list;
    }*/
       
   /**
    *  Function that returns the arraylist contains the information like review details 
    * by the user for the given user id 
    * @param userId, request 
    * @return userName as String
    */
  
    public ArrayList getUserReviewList(String userId, HttpServletRequest request) 
    {
       ProfileBean bean    =   null;
       ResultSet resultset =   null;
       ArrayList list      =   new ArrayList();
       DataModel dataModel =   new DataModel();
       Vector vector       =   new Vector();
       vector.clear();
       vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
       vector.add(userId);
       try {
         resultset = dataModel.getResultSet("wu_profile_pkg.get_my_review_title_details_fn", vector);
         while(resultset.next()) {
             bean = new ProfileBean();
             bean.setReviewId(resultset.getString("review_id"));
             bean.setReviewTitle(resultset.getString("review_title"));
             bean.setReviewDesc(resultset.getString("overall_rating_comments"));
             bean.setStatusFlag(resultset.getString("flag"));
             bean.setCollegeId(resultset.getString("college_id"));
             list.add(bean);
          }
       } catch(Exception exception) {
           exception.printStackTrace();
       } finally {
          dataModel.closeCursor(resultset);
       }                 
       return list;
   }
       
   /**
    *  Function that used to generate the list of colleges that has competitors.  
    * @param collegeId, request
    * @return competitors as collection list
    */
      
    public ArrayList getCompetitorsPod(String collegeId, HttpServletRequest request) 
    {
       SearchResultBean bean  = null;
       ResultSet resultset = null;
       ArrayList list = new ArrayList();
       DataModel dataModel = new DataModel();
       Vector vector = new Vector();
       vector.clear();
       vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
       vector.add(collegeId);
       try{
          resultset = dataModel.getResultSet("wu_course_search_pkg.get_college_competitors_fn", vector);
          while(resultset.next()) {
             bean = new SearchResultBean();
             bean.setCollegeId(resultset.getString("college_id"));
             bean.setCollegeName(resultset.getString("college_name"));
             bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
             bean.setCollegeLogo(resultset.getString("college_logo"));
             bean.setCityName(resultset.getString("city"));
             bean.setCounty(resultset.getString("county_state"));
             bean.setPostCode(resultset.getString("postcode"));
             list.add(bean);
          }
       } catch(Exception exception)  {
            exception.printStackTrace();
      } finally {
           dataModel.closeCursor(resultset);
      }                 
     return list;
    }
    
     /**
      *  Function that used to get the web site tracker  
      * @param sessionId, userId, affliateId, collegeId, p_java_flag, request
      * @return void
      */
     
    public void webSiteTracker(String sessionId, String userId, String affliateId, String collegeId, String p_java_flag, HttpServletRequest request)
    {
       DataModel dataModel  =  new DataModel();
       Vector vector        =  new Vector();
       String clientIp     =   request.getHeader( "CLIENTIP");
       if( clientIp==null || clientIp.length()==0){
           clientIp   =   request.getRemoteAddr();
       }   
       String clientBrowser =  request.getHeader("user-agent"); 
       vector.clear();
       vector.add(sessionId);
       vector.add(userId);
       vector.add(affliateId);
       vector.add(collegeId);
       vector.add(p_java_flag);
       vector.add(clientIp);
       vector.add(clientBrowser);
       try  {
           dataModel.executeUpdate("hot_admin.obj_pls_track_website_fn", vector);
       } catch(Exception exception) {
           exception.printStackTrace();
       }
    }
    
     /**
      *  Function that used to set the API log
      * @param sessionId, userId, affliateId, activity, collegeId, profileId, searchHeaderId, extraText, request
      * @return void
      */
     
    public void setAPILog(String sessionId, String userId, String affliateId, String activity, String collegeId, String profileId, String searchHeaderId, String extraText, HttpServletRequest request)
    {
       DataModel dataModel  =  new DataModel();
       String clientIp     =   request.getHeader( "CLIENTIP");
       if( clientIp==null || clientIp.length()==0){
           clientIp   =   request.getRemoteAddr();
       }   
       String clientBrowser =  request.getHeader("user-agent");
       Vector vector        =  new Vector();
       vector.clear();
       vector.add(sessionId);
       vector.add(userId);
       vector.add(affliateId);
       vector.add(activity);  
       vector.add(collegeId);
       vector.add(profileId);
       vector.add(searchHeaderId);
       vector.add(extraText);
       vector.add(clientIp);
       vector.add(clientBrowser);
       try {
           dataModel.executeUpdate("hot_courses.api_log.add_log", vector);
       } catch(Exception exception) {
           exception.printStackTrace();
       }
    }

  /**
    *
    * Function that used to track the link (college/course profile)
    *
    * @param collegeId or courseId
    * @param keyTypeName
    * @param request
    * @return
    */
  public String collegeProfileTracker(String collegeId, String keyTypeName, HttpServletRequest request, String mobileFlag, HttpSession session) {
    String statLogId = "";
    SessionData sessionData = new SessionData();
    DataModel dataModel = new DataModel();
    Vector parameters = new Vector();
    String clientIp = request.getHeader("CLIENTIP");
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    }
    String clientBrowser = request.getHeader("user-agent");
    String requestUrl = CommonUtil.getRequestedURL(request); //Change the getRequestURL by Prabha on 28_Jun_2016   
    //
    // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
    String screenWidth = GenericValidator.isBlankOrNull(request.getParameter("screenwidth")) ? GlobalConstants.DEFAULT_SCREEN_WIDTH : request.getParameter("screenwidth");
    // wu_300320 - Sangeeth.S: Added lat and long for stats logging
    String[] latAndLong = sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG).split(GlobalConstants.SPLIT_CONSTANT);
    String latitude = (latAndLong.length > 0 && !GenericValidator.isBlankOrNull(latAndLong[0])) ? latAndLong[0].trim() : "";
	String longitude = (latAndLong.length > 1 && !GenericValidator.isBlankOrNull(latAndLong[1])) ? latAndLong[1].trim() : "";
	String userUcasScore = StringUtils.isNotBlank((String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE)) ? (String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE) : null; //Added for tariff-logging-ucas-point by Sri Sankari on wu_20200915 release
	String yearOfEntryForDSession = StringUtils.isNotBlank((String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING)) ? (String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING) : null; //Added for getting YOE value in session to log in d_session_log stats by Sujitha V on 2020_NOV_10 rel
    parameters.add(GlobalConstants.WHATUNI_AFFILATE_ID);
    parameters.add(collegeId);
    parameters.add(new SessionData().getData(request, "x"));
    parameters.add(new SessionData().getData(request, "y"));
    parameters.add(keyTypeName);
    parameters.add(clientBrowser);
    parameters.add(clientIp);
    parameters.add(""); // category coode;
    parameters.add(""); // Extra URL    
    parameters.add(requestUrl);
    parameters.add(request.getHeader("referer"));
    parameters.add(""); // subOrderItemId
    parameters.add("");// profile_id
    parameters.add(mobileFlag);
    parameters.add(""); // network_id
    parameters.add(new SessionData().getData(request, "userTrackId"));
    parameters.add(screenWidth); // screenwidth
    parameters.add(latitude); //latitude added by sangeeth.s for March2020 rel
    parameters.add(longitude); //longitude
    parameters.add(userUcasScore); //P_UCAS_TARIFF
    parameters.add(yearOfEntryForDSession); //P_YEAR_OF_ENTRY
    //
    try {
      statLogId = dataModel.executeUpdate("WU_STATS_LOG_WRAPPER_PKG.obj_track_col_profile_fn", parameters);
    } catch (Exception trackingException) {
      trackingException.printStackTrace();
    }
    return statLogId;
  }

    /**
     *  Function that used to get  the date from the data base
     * @return system date as string
     */

     public String getSysDate() 
     {
         String sysDate = null;
         try {
             DataModel dataModel =   new DataModel();
             Vector vector       =   new Vector();
             vector.clear();
             sysDate = dataModel.getString("Wu_profile_pkg.get_sysdate_fn", vector);
         } catch(Exception exception) {
             exception.printStackTrace();    
         }
        return sysDate;
     }

  /**
   * 
   * to return basic user details to money page
   * 
   * @param request
   * @param servletContext
   * @param response
   */
    
    public void loadUserLoggedInformationForMoneyPage(HttpServletRequest request, ServletContext servletContext, HttpServletResponse response) {
      HttpSession session=request.getSession();
      String userId = new SessionData().getData(request,"y");
      if(userId !=null && !userId.equals("0") && userId.trim().length()>0) {
        if(session.getAttribute("userInfoList") == null) {
          ArrayList userInfoList = getUserInformation(userId, request, servletContext);
          if(userInfoList !=null && userInfoList.size() > 0) {
            session.setAttribute("userInfoList", userInfoList);   
          }
        }    
      } else {
        session.removeAttribute("userInfoList");
      }
    }

   /**
    *  Function that used to get  the user information for using logged information pod
    * @param request, servletContext
    * @return void
    */
     
    public void loadUserLoggedInformation(HttpServletRequest request, ServletContext servletContext, HttpServletResponse response) 
    {
        HttpSession session=request.getSession();
        new SecurityEvaluator().overrideJSessionId(request,response);
        String userId =  new SessionData().getData(request,"y");
         /* if (session.getAttribute("mySearches") == null){
              String basketId = new CookieManager().getCookieValue(request,"basketId");
              ArrayList mySearches = new GlobalFunction().getMySearchesList(basketId,request);
              //if(mySearches != null && mySearches.size() > 0){
                  session.setAttribute("mySearches", mySearches);
              //}
          }*/ //Commented for 19th May release as part of W_USER_SEARCHES
          if(userId !=null && !userId.equals("0") && userId.trim().length()>0) {
              if(session.getAttribute("userInfoList") == null) {
                  ArrayList userInfoList = getUserInformation(userId, request, servletContext);
                  if(userInfoList !=null && userInfoList.size() > 0) {
                       session.setAttribute("userInfoList", userInfoList);   
                  }
              }                
          } else {
              session.removeAttribute("userInfoList");
          } 
          if(session.getAttribute("subjectList") == null) {
              List subjectList = getCourseSubject(null, request);
              session.setAttribute("subjectList", subjectList);
          }
          getBasketPodContent(request, response);
    } 

   /**
    *  Function that generated the list of video created for the specified course
    * @param request, servletContext, courseId, pageNo, recordDisplay orderBy, reviewStatus
    * @return Video list as collection object
    */
      
    public List generateCourseVideoReviewList(HttpServletRequest request, ServletContext context, String courseId, String pageNo, String recordDisplay, String orderBy, String reviewStatus)
     {
       DataModel dataModel = new DataModel();
       ResultSet resultset = null;    
       ArrayList list = new ArrayList();  
       String videoImagePath = getWUSysVarValue("VIDEO_THUMBNAIL_DISPLAY_PATH");
       String videoUploadPath = getWUSysVarValue("VIDEO_THUMBNAIL_UPLOAD_PATH");
       String limeLightPath = GlobalConstants.LIMELIGHT_VIDEO_PATH;
       try
        {
            VideoReviewListBean bean =  null;
            Vector vector  =   new Vector();
            vector.clear();
            vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);     
            vector.add(courseId);                                
            vector.add(pageNo);                                  
            vector.add(recordDisplay);                           
            vector.add(orderBy);                                 
            vector.add(reviewStatus);                            
            resultset = dataModel.getResultSet("Wu_Course_Search_Pkg.get_crs_video_review_list_fn", vector);
            while(resultset.next()) {
                bean = new VideoReviewListBean();
                bean.setCollegeId(resultset.getString("college_id"));
                bean.setCollegeName(resultset.getString("college_name"));
                bean.setCourseName(resultset.getString("course_name"));
                bean.setUserId(resultset.getString("user_id"));
                bean.setUserName(resultset.getString("user_name"));
                bean.setVideoReviewId(resultset.getString("review_id"));
                bean.setVideoReviewTitle(resultset.getString("review_title"));
                bean.setVideoReviewDesc(resultset.getString("review_desc"));
                bean.setVideoCategory(resultset.getString("v_category"));
                bean.setVideoLength(resultset.getString("length"));
                bean.setNoOfHits(resultset.getString("no_hit"));
                bean.setVideoUrl(resultset.getString("video_text"));
                bean.setUserNationality(resultset.getString("nationality"));
                bean.setUserGender(resultset.getString("gender"));
                bean.setUserAtUni(resultset.getString("at_uni"));
                bean.setVideoType(resultset.getString("video_type"));
                String videoUrl  = resultset.getString("video_url");
                String videoType =   resultset.getString("video_type");  
                if(videoType!=null && (videoType.equalsIgnoreCase("B") || videoType.equalsIgnoreCase("U"))) { //Provider video
                    bean.setUserName(GlobalConstants.LIMELIGHT_VIDEO_PROVIDER_NAME);
                }
                if(videoType!=null && videoType.equalsIgnoreCase("V")) { //user video
                   bean.setVideoUrl(videoUrl);
                   request.setAttribute("YOUTUBE","TRUE");
                   boolean foundFlag = checkFilePresent(videoUploadPath+resultset.getString("thumbnail_assoc_text"));
                   if(foundFlag) {
                      bean.setThumbnailUrl(resultset.getString("thumbnail_assoc_text"));
                   } else{
                      bean.setThumbnailUrl(videoImagePath+"youtubeSmall.jpg");
                   }
                } else {
                     bean.setVideoUrl(limeLightPath+videoUrl);
                     bean.setThumbnailUrl(new GlobalFunction().videoThumbnailFormatChange(limeLightPath+videoUrl, "0"));
                }

                bean.setOverallRating(resultset.getString("overall_rating"));
                list.add(bean);
            }
        } catch(Exception exeption) {
            exeption.printStackTrace();
        } finally {
          dataModel.closeCursor(resultset);
        }
       return list;
    }

   /**
     * Function that insert the update video review to data base
     * @param studyLevelId, prefixNo, searchTitle
     * @return string
     */
  
   public String seoStudyLevelDescription(String studyLevelId, String prefixNo, String searchTitle) 
   {
      DataModel dataModel = new DataModel();
      String description = "";
      Vector parameters = new Vector();
      parameters.add(studyLevelId);
      parameters.add(prefixNo);
      parameters.add(searchTitle);
      try {
          description = dataModel.executeUpdate("Wu_review_Pkg.get_seo_study_level_fn", parameters);
      }  catch (Exception commitException) {
            commitException.printStackTrace();;
      }
      return description;
   }
   
   /**
     *  function to check the session time exit the limit specified  in web.xml 
     * @param request
     * @param session
     * @return flag as boolean 
    */
      
    public boolean checkSessionExpired(HttpServletRequest request, HttpServletResponse response, HttpSession session, String pageType) 
    {
        String sessionEnableFlag  = "TRUE"; //getSysVarValue("WU_SESSION_ENABLE");
         boolean returnFlag = false;
         if(pageType !=null && pageType.equalsIgnoreCase("ANONYMOUS")) {
             return false;
         }
         if(pageType !=null && pageType.equalsIgnoreCase("REGISTERED_USER")) {
             if(sessionEnableFlag !=null && sessionEnableFlag.trim().equalsIgnoreCase("TRUE")) {
                 session = request.getSession(false);
                 if((request.getRequestedSessionId()!=null && !(request.isRequestedSessionIdValid()))) {
                     try {
                         new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response);
                     } catch(Exception exception){
                      exception.printStackTrace();
                    }
                     session.setAttribute("message", "sessionexpiry");
                    returnFlag = true;
                 }
             }    
         }   
        return returnFlag;
     }

     public String replaceURL(String inString) 
    {
        String specialChar[] = {"(?i)The ","(?i)\\(the\\)", "'", "~", "!", "@", "#", "$", "%", "^", "&", "\\*", "\\(", "\\)", "\\{", "\\}", "\\[", "\\]", "<", ">", ",", "\\.", "\\?", "\\/", "|", ";", ":", "\\+", "=", "_", "`", "ï¿½", "ï¿½", "(?i)the-"};
        if(specialChar.length>0) {
          if(inString != null && !inString.equals("null") && inString.trim().length()>0){
          for(int charStart = 0; charStart<specialChar.length; charStart++) {
             inString=  inString.replaceAll(specialChar[charStart],"");
          }
        }    
        }    
        return inString !=null ? inString.trim() : inString;
    }

    public String replaceHypen(String inString) 
    {
       if(inString !=null){
          inString=  inString.replaceAll("&","and");
          inString=  inString.replaceAll(" ","-");
          inString=  inString.replaceAll("-+","-");
       }    
      return inString;
    }
    //17_Mar_2015 - replace white space to plus for keyword search, by Thiyagu G.
    public String replaceWhiteSpaceToPlus(String inString) 
    {
       if(inString !=null){
          inString=  inString.replaceAll("&","and");
          inString=  inString.replaceAll(" ","+");
          inString=  inString.replaceAll("-+","-");
       }    
      return inString;
    }

  public String replaceHypenWithSpace(String inString) {
    if (inString != null) {
      inString = inString.replaceAll("-", " ");
    }
    return inString;
  }
  
  public String replaceSpaceWithHypen(String inString) {
    if (inString != null) {
      inString = inString.replaceAll(" ", "-");
      inString = inString.replaceAll("/", "-");
    }
    return inString;
  }
  
  public String replaceSlashWithHypen(String inString) {
    if (inString != null) {
      inString = inString.replaceAll("/", "-");
    }
    return inString;
  }
  
  public String replaceCommoWithSpace(String inString) {
    if (inString != null) {
      inString = inString.replaceAll(",", " ");
    }
    return inString;
  }

  public String replacePlus(String inString) {
    if (inString != null) {
      inString = inString.replaceAll("\\+", " ");
      inString = inString.replaceAll("\\+", " ");
      inString = inString.replaceAll("\\+", " ");
      inString = inString.replaceAll("\\+", " ");
    }
    return inString;
  }

  /**
     *  function that removes all the special character from the URL
     * @param inString
     * @return
     */
    public String replaceSpecialCharacter(String inString)
    {
       String outString = inString; 
        if(outString !=null){
           char data[] = outString.toCharArray();
           for(int i=0;i<data.length;i++){
               int character= data[i];
               if(!((character>=48 && character<=57) || (character>=65 && character<=90) || (character>=97 && character<=122) || character==45)){
                   data[i]=' ';
                }
           }
            outString = new String(data);
        }   
        return outString;
    }
    
    public String removeQuestions(String inString) 
    {
       if(inString !=null){
          inString=  inString.replaceAll("?","");
       }    
      return inString;
    }

    public String replaceSpacePlus(String inString) 
    {
        if(inString !=null) {
           inString=  inString.replaceAll("\\ ","+");
       }    
       return inString;
    }    

  /**
   *  function that generates the all field by using headerId 
   * @param request, servletContext, courseId, pageNo, recordDisplay, orderBy, reviewStatus
   * @return General review list as collection object
   */
     
   public void generateCourseReviewList(String headerId, SearchBean bean, HttpServletRequest request)
   {
       DataModel dataModel = new DataModel();
       ResultSet resultset = null;
       Vector parameters = new Vector();
       parameters.add(headerId);
       try {
          resultset = dataModel.getResultSet("WU_COURSE_SEARCH_PKG.GET_SEARCH_HEADER_DATA_FN", parameters);
          while(resultset.next()) 
          {
               bean.setCourseSearchText(resultset.getString("phrase_search"));
               bean.setCollegeName(resultset.getString("college_name"));
               bean.setStudyLevelId(resultset.getString("qualification"));
               bean.setFilterId(resultset.getString("search_category"));
               bean.setSubjectId(resultset.getString("search_category"));
               bean.setPostCode(resultset.getString("town_city"));
               bean.setCollegeId("");
               bean.setFilterName("");
               bean.setCountyId(resultset.getString("county_id"));
               bean.setOrderId(resultset.getString("search_how"));
               bean.setStudyModeValue(resultset.getString("search_study_mode"));
               bean.setStudyModeId(resultset.getString("search_study_mode"));
               bean.setUcasCode(resultset.getString("ucas_code"));  // added for 21th October 2009 release for UCAS code search
                // added for 15th December 2009 release for deemed uni flag in search
               String deemedUniValue ="";
               if(resultset.getString("search_deemed_uni") != null){
                  if(resultset.getString("search_deemed_uni").equalsIgnoreCase("Y")){
                    deemedUniValue ="u";
                  }else if(resultset.getString("search_deemed_uni").equalsIgnoreCase("N")){
                    deemedUniValue ="c";
                  }
               }else{
                 deemedUniValue ="uc";
               }
               bean.setDeemedUniValue(deemedUniValue);
              // code ended  here for 15th December 2009 release for deemed uni flag in search
            
         }
       } catch(Exception exception) {
           exception.printStackTrace();
       } finally  {
            dataModel.closeCursor(resultset);
      }       
    }
 
    /**
     *  This function used to return true or false for the profile details if present or not present respectively for the given college id 
     * @param collegeId, form
     * @return status of each tab as collection object
     */

     public List getCollegeProfileTabs(String collegeId) 
     {
        DataModel dataModel =   new DataModel();
        ResultSet resultset =   null;
        List list           =   new ArrayList();
        try {
            CollegeProfileBean bean = new CollegeProfileBean();
            Vector profileTabVector      =   new Vector();
            profileTabVector.clear ();
            profileTabVector.add(GlobalConstants.WHATUNI_AFFILATE_ID);             // affiliate_id
            profileTabVector.add(collegeId);                              // college_id
            resultset = dataModel.getResultSet("wu_college_info_pkg.show_profile_tab_fn", profileTabVector);
            while(resultset.next()) {
                bean.setShowOverViewTab(resultset.getString("overview"));
                bean.setShowAccommodationTab(resultset.getString("accomodation"));
                bean.setShowEntertainmentTab(resultset.getString("entertainment"));
                bean.setShowCoursesFacilitiesTab(resultset.getString("course_facility"));
                bean.setShowWelfareTab(resultset.getString("welfare"));
                bean.setShowFeesFundingTab(resultset.getString("fees_funding"));
                bean.setShowJobProspectsTab(resultset.getString("job_prospectus"));
                bean.setShowContactTab(resultset.getString("contact"));
                list.add(bean);
             }
          }  catch(Exception exception) {
              exception.printStackTrace();
          } finally {
              dataModel.closeCursor(resultset);
          }
          return list;
    }
    
     public List sortValue(final boolean ascending,  final boolean sortorder, List collectionObject) {
          List list = collectionObject;
          Comparator comparator = new Comparator() {

                  public int compare(Object o1, Object o2) {
                      RegistrationBean c1 = (RegistrationBean) o1;
                      RegistrationBean c2 = (RegistrationBean) o2;

                          if (sortorder == false)
                              return ascending ? 
                                     c1.getOptionText().compareTo(c2.getOptionText()) : 
                                     c2.getOptionText().compareTo(c1.getOptionText());
                          if (sortorder == true)
                              return ascending ? 
                                     c2.getOptionText().compareTo(c1.getOptionText()) : 
                                     c1.getOptionText().compareTo(c2.getOptionText());
                      return 0;
                  }
              };
          Collections.sort(list, comparator);
          return list;
      }

    /**
     *  Function used to get the I am a question
     * @return as collection object
     */
    
     public List getIamQuestionValues(String showFeatureStudent) 
     {
        DataModel dataModel =   new DataModel();
        ResultSet resultSet =   null;
        List list           =   new ArrayList();
        Vector vector       =   new Vector();
        vector.clear();
        vector.add("345");    // QuestionID for Iam a question
        vector.add(showFeatureStudent);
        try {
            resultSet = dataModel.getResultSet("wu_review_pkg.get_drop_down_list_fn", vector);
            while(resultSet.next()) {
                MembersSearchBean bean = new MembersSearchBean();
                bean.setOptionId(resultSet.getString("Value"));          
                String description = resultSet.getString("description");
                description = description !=null ? description.trim().concat("s") : description;
                bean.setOptionText(new CommonUtil().toTitleCase(description));  
                list.add(bean);
            }
        } catch(Exception exception) {
            exception.printStackTrace();
        } finally {
            dataModel.closeCursor(resultSet);    
        }
       return list;
    }   
    
    /**
     *  function that generated the most review university 
     * @param form, request
     * @return university list as collection object
    */
        
    public List getOrderProspectusUniList(HttpServletRequest request) 
    {
        DataModel dataModel =   new DataModel();
        ResultSet resultset =   null;    
        ArrayList  list     =   new ArrayList();  
        try {
             HomePageBean bean =  null;
             Vector vector     =  new Vector();
             resultset = dataModel.getResultSet("wu_homepage_pkg.get_order_prospectus_pod_fn", vector);
             while(resultset.next()) {
                 bean = new HomePageBean();
                 bean.setCollegeId(resultset.getString("college_id"));    
                 bean.setCollegeName(resultset.getString("college_name")); 
                 list.add(bean);
             }
         } catch(Exception exception) {
             exception.printStackTrace();
         } finally {
             dataModel.closeCursor(resultset);
        }
       return list;
    }   
    
    
    public ArrayList getBrowseLocationList(){
        Vector vector = new Vector();
        ResultSet resultSet = null;
        LocationUniBrowseBean lbrowseBean = null;
        ArrayList luniBrowseList = new ArrayList();
        DataModel dataModel = new DataModel();
        try{
            vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
            resultSet = dataModel.getResultSet("wu_browse_pkg.get_uni_location_fn",vector);
            while (resultSet.next()){
                lbrowseBean = new LocationUniBrowseBean();
                lbrowseBean.setLocationId(resultSet.getString("location_id"));
                lbrowseBean.setLocationName(resultSet.getString("location_name"));
                lbrowseBean.setRowLevel(resultSet.getString("row_level"));
                lbrowseBean.setCountyNameParam(replaceHypen(replaceURL(resultSet.getString("location_name").replaceAll("&","and"))).toLowerCase());
                luniBrowseList.add(lbrowseBean);
            }
        } catch(Exception exception){
            exception.printStackTrace();
        } finally {
            dataModel.closeCursor(resultSet);
        }
        return luniBrowseList;
    }

  public String checkCookieStatus(HttpServletRequest request) {
    Cookie cookies[] = request.getCookies();
    String cookieName = "";
    String cookie_basket_id = "";
    if (cookies != null) {
      for (int i = 0; i < cookies.length; i++) {
        cookieName = cookies[i].getName();
        if (cookieName != null && cookieName.trim().equalsIgnoreCase("basketId")) {
       
          cookie_basket_id = cookies[i].getValue();
          if (cookie_basket_id != null && !cookie_basket_id.equals("")) {
            break;
          }
        }
      }
    }
    return cookie_basket_id;
  }

  /**
   *  function to load the thumbnail for the given FLV file
   * @param videoURL, videoId
   * @return Thumbpath as String
   */
  public String getSearchVideoThumbPath(String videoURL, String thumbNo) {
    String thumbUrl = "";    
    thumbNo = !GenericValidator.isBlankOrNull(thumbNo) ? thumbNo : "1";
    if(!GenericValidator.isBlankOrNull(videoURL)){
      thumbUrl = videoURL.indexOf(".mp4") > -1 ? videoURL.replaceAll(".mp4", "_tmb/000"+ thumbNo +".jpg") : videoURL.replaceAll(".flv", "_tmb/000"+thumbNo+".jpg");     
    }    
    return thumbUrl;
  }
  
   public String uniLandingReviewSEOUrl(String collegeId, String collegeName) {
      String p_url_view_review = "";
             p_url_view_review = GlobalConstants.SEO_PATH_UNI_REVIEWS 
                                + replaceHypen(replaceSpecialCharacter(replaceURL(collegeName))) 
                                + "-reviews/highest-rated/0/0/"
                                + collegeId
                                + "/1/studentreviews.html";  // added for 03rs Mardh 2010 Release
       return (p_url_view_review != null ? p_url_view_review.trim().toLowerCase() : p_url_view_review);
   }

/**
 * This function will append the http to comming url;
 * Added By Sekhar for wu413_20120710
 */
    public String appendingHttptoURL(String tmpUrl){
        String appendUrl = "";
        if(tmpUrl != null && tmpUrl.trim().length() > 0){
           if(tmpUrl.indexOf("www.whatuni.com") >= 0){ //Changed server name www.whatuni.com by Prabha on 21_Mar_16
            appendUrl = (tmpUrl.indexOf("https://") >= 0) ? tmpUrl.replace("https://", GlobalConstants.WHATUNI_SCHEME_NAME) : (tmpUrl.indexOf("http://") >= 0) ? tmpUrl.replace("http://", GlobalConstants.WHATUNI_SCHEME_NAME): GlobalConstants.WHATUNI_SCHEME_NAME + tmpUrl;//Added WHATUNI_SCHEME_NAME by Indumathi Mar-29-16
           } else {
             appendUrl = (tmpUrl.indexOf("http://") >= 0 || tmpUrl.indexOf("https://") >= 0) ? tmpUrl : "http://" + tmpUrl;
           }
        }
        return appendUrl;
    }
  /**
   * This function will return the HTML Editor text for footer and clearing profile pages.
   * wu413_20120710 added by Sekhar K
   */
  public String getHTMLEditorText(String website, String collegeId){
      StringBuffer htmlTextCF = new StringBuffer();
      String htmlText = "";
      DataModel dataModel = new DataModel();
      Vector parameters = new Vector();
      parameters.add(website);
      parameters.add(collegeId);
      ResultSet resultSet = null;//29_OCT_2013
      Clob clobValue;
      try{
        resultSet = dataModel.getResultSet("WU_HTML_EDITOR_PKG.GET_HTML_EDITOR_FN", parameters);
         if(resultSet!=null){
          while(resultSet.next()){
           clobValue =  resultSet.getClob("html_text");
           htmlText = getClobValue(clobValue);
          }
        }
      }catch(Exception e){
        e.printStackTrace();
      }finally{
        dataModel.closeCursor(resultSet);
        dataModel = null;
        parameters.clear();
        parameters = null;
      }
      htmlTextCF.append(htmlText);
      return htmlTextCF.toString();
  }
  
  public ArrayList getOpendayInfo(String collegeId){
      ResultSet resultset = null;
      DataModel dataModel = new DataModel();
      OpendaysVO opendaysVO = new OpendaysVO();
      ArrayList opendayInfo = new ArrayList();
      Vector vector = new Vector();
      try{
        vector.add(collegeId);
        resultset = dataModel.getResultSet("wu_util_pkg.get_redesign_open_day_fn", vector);          
        while(resultset.next()){
            opendaysVO.setCollegeId(resultset.getString("college_id"));
            opendaysVO.setCollegeName(resultset.getString("college_name"));
            opendaysVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
            opendaysVO.setCollegeLocation(resultset.getString("college_location"));
            opendaysVO.setOpendayCount(resultset.getString("openday_count"));
            opendaysVO.setNextOpenday(resultset.getString("next_col_openday"));
            opendaysVO.setOpendayExternalUrl(resultset.getString("open_day_url"));
            opendaysVO.setOpendayText(resultset.getString("open_day_text"));
            opendayInfo.add(opendaysVO);
        }
      
      }catch(SQLException e){
          e.printStackTrace();
      }finally{
          dataModel.closeCursor(resultset);
      }
      return opendayInfo;
  }

  public ArrayList getOverallRatingInfo(String collegeId){
      ResultSet resultset = null;
      DataModel dataModel = new DataModel();
      UniOverallReviewsVO uniOverallReviewsVO = new UniOverallReviewsVO();
      ArrayList overallRatingInfo = new ArrayList();
      Vector vector = new Vector();
      try{
        vector.add(collegeId);
        resultset = dataModel.getResultSet("WUNI_SPRING_PKG.get_overall_review_fn", vector);          
        while(resultset.next()){
            uniOverallReviewsVO.setCollegeId(resultset.getString("college_id"));
            uniOverallReviewsVO.setCollegeName(resultset.getString("college_name"));
            uniOverallReviewsVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
            uniOverallReviewsVO.setReviewCount(resultset.getString("review_count"));
            uniOverallReviewsVO.setOverallRating(resultset.getString("overall_rating"));
            overallRatingInfo.add(uniOverallReviewsVO);
        }
      }catch(SQLException e){
          e.printStackTrace();
      }finally{
          dataModel.closeCursor(resultset);
      }
      return overallRatingInfo;
  }
  /**
   * wu414_20120821 Added By Sekhar K 
   * This function will return the H1 tag information whcih will come from myhc, edited text.
   */
   public String getHTMLEditorH1(String contentType, String categoryName, String pageName, String flag){
     StringBuffer htmlText = new StringBuffer();
     String stringText = "";
     DataModel dataModel = new DataModel();
     Vector parameter = new Vector();
     parameter.add(contentType);
     parameter.add(categoryName);
     parameter.add(pageName);
     parameter.add(flag);
     try{
        stringText = dataModel.getString("WU_HTML_EDITOR_PKG.GET_CATEGORY_BROWSE_HTML_FN", parameter);
     }catch(SQLException e){
       e.printStackTrace();
     }finally{
      dataModel = null;
     }
     htmlText.append(stringText);
     return htmlText.toString();
   }
/**
   * 
   * @param collegeId
   * @param netWorkId
   * @return
   * @since Added by Sekhar for wu417_20121030 for WU QL.
   */
  public ArrayList getUniprofileInfo(String collegeId, String netWorkId){
      ResultSet resultset = null;
      DataModel dataModel = new DataModel();
      CollegeCpeInteractionDetailsWithMediaVO enquiryInfo = null;
      ArrayList uniprofileinfo = new ArrayList();
      Vector vector = new Vector();
      try{
        vector.add(collegeId);
        vector.add(netWorkId);
        resultset = dataModel.getResultSet("wu_cpe_advert_pkg.enquiry_for_redesign_cd_fn", vector);          
        while(resultset.next()){
            enquiryInfo = new CollegeCpeInteractionDetailsWithMediaVO();
            enquiryInfo.setCollegeId(resultset.getString("college_id"));
            enquiryInfo.setCollegeName(resultset.getString("college_name"));
            enquiryInfo.setCollegeNameDisplay(resultset.getString("college_name_display"));
            enquiryInfo.setCollegeLocation(resultset.getString("college_location"));
            enquiryInfo.setProfileShortDesc(resultset.getString("profile_short_desc"));
            
            //
            // media details from db
            enquiryInfo.setMediaId(resultset.getString("MEDIA_ID"));
            enquiryInfo.setMediaType(resultset.getString("MEDIA_TYPE"));
            enquiryInfo.setMediaPath(resultset.getString("MEDIA_PATH"));
            enquiryInfo.setMediaPathThumb(resultset.getString("THUMB_MEDIA_PATH"));
            //customized media details
            //reassign image/video related variables according to mediaType.
            if (enquiryInfo.getMediaType() != null && enquiryInfo.getMediaType().equalsIgnoreCase("VIDEO")) {
              enquiryInfo.setIsMediaAttached("TRUE");
              enquiryInfo.setVideoId(enquiryInfo.getMediaId());
              enquiryInfo.setVideoPathFlv(enquiryInfo.getMediaPath());
              if (enquiryInfo.getVideoPathFlv() != null && enquiryInfo.getVideoPathFlv().trim().length() > 0) {
                enquiryInfo.setVideoPath0001(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "1"));
                enquiryInfo.setVideoPath0002(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "2"));
                enquiryInfo.setVideoPath0003(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "3"));
                enquiryInfo.setVideoPath0004(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "4"));
              }
              enquiryInfo.setImageId("FALSE");
              enquiryInfo.setImagePath("FALSE");
              enquiryInfo.setImagePathThumb("FALSE");
            } else if (enquiryInfo.getMediaType() != null && enquiryInfo.getMediaType().equalsIgnoreCase("PICTURE")) {
              enquiryInfo.setIsMediaAttached("TRUE");
              enquiryInfo.setImageId(enquiryInfo.getMediaId());
              enquiryInfo.setImagePath(enquiryInfo.getMediaPath());
              enquiryInfo.setImagePathThumb(enquiryInfo.getMediaPathThumb());
              if (enquiryInfo.getImagePathThumb() == null || enquiryInfo.getVideoPathFlv().trim().length() == 0) {
                enquiryInfo.setImagePathThumb(enquiryInfo.getImagePath());
              }
              enquiryInfo.setVideoId("FALSE");
              enquiryInfo.setVideoPathFlv("FALSE");
              enquiryInfo.setVideoPath0001("FALSE");
              enquiryInfo.setVideoPath0002("FALSE");
              enquiryInfo.setVideoPath0003("FALSE");
              enquiryInfo.setVideoPath0004("FALSE");
              uniprofileinfo.add(enquiryInfo);
        }
        }
      }catch(SQLException e){
          e.printStackTrace();
      }finally{
          dataModel.closeCursor(resultset);
      }
      return uniprofileinfo;
  }
  /**
    *   This function is used to load the nationality for the user.
    * @param fieldId
    * @return Nationality list as Collection object.
    */
  public List loadNationalityData(String fieldId) {
    ResultSet resultSet = null;
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    ProspectusBean prosBean = null;
    ArrayList nationalityList = new ArrayList();
    try {
      vector.add(fieldId);
      vector.add("YES");
      resultSet = dataModel.getResultSet("wu_review_pkg.get_drop_down_list_fn", vector);
      while (resultSet.next()) {
        prosBean = new ProspectusBean();
        prosBean.setNationalityValue(resultSet.getString("value"));
        prosBean.setNationalityDesc(resultSet.getString("description"));
        nationalityList.add(prosBean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return nationalityList;
  }
  //
  /**
    *   This function is used to load the nationality for the user.  
    */
  public String getCollegeDisplayName(String collegeId) {
     String collegeName = null;
     try {
       DataModel dataModel = new DataModel();
       Vector vector = new Vector();
       vector.clear();
       vector.add(collegeId);
       collegeName = dataModel.getString("GET_COLLEGE_TITLE_FN", vector);
     } catch (Exception exception) {
       exception.printStackTrace();
     }
     return collegeName;
   }
   
   public String getGACPEPrice(String suborederItem, String interactionType){
     String cpeValue = null;
     try {
       DataModel dataModel = new DataModel();
       Vector vector = new Vector();
       vector.clear();
       vector.add(suborederItem);
       vector.add(interactionType);
       cpeValue = dataModel.getString("hot_admin.get_cpe_price_fn", vector);
     } catch (Exception exception) {
       exception.printStackTrace();
     }
     return cpeValue;
   }
   
  public static String getOrdinalFor(int value) 
  {
    int hundredRemainder = value % 100; 
    int tenRemainder = value % 10;
    if(hundredRemainder - tenRemainder == 10) {
      return "th";
    }   
    switch (tenRemainder) {
      case 1:
        return "st";
      case 2:
        return "nd";
      case 3:
        return "rd";
      default:
        return "th";
    }
  }
  
  /**
    *   This function is used to load the random browse categories in 404 page.
    */
  public List browseCategories404() {
    ResultSet resultSet = null;
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    BrowseCatfor404pageVO bean = null;
    ArrayList browseList = new ArrayList();
    try {
      resultSet = dataModel.getResultSet("WU_UTIL_PKG.fournotfour_ran_cat_fn", vector);
      while (resultSet.next()) {
        bean = new BrowseCatfor404pageVO();
        bean.setParentDesc(resultSet.getString("parent_description"));
        bean.setParentCatCode(resultSet.getString("parent_category_code"));
        bean.setChildURl(resultSet.getString("child_url"));
        browseList.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return browseList;
  }  
  public static String getClobValue(Clob htmlContentcb)
  {
    String htmlStr = null;
    StringBuffer htmlContent = new StringBuffer();
    try
    {
      Clob clob = htmlContentcb;
      BufferedReader bufferedreader = new BufferedReader(clob.getCharacterStream());
      while((htmlStr = bufferedreader.readLine()) != null)
      {
        htmlContent.append(htmlStr);
      }
    } catch(Exception ex)
    {
      ex.printStackTrace();
    }
    return htmlContent.toString();
  }
  
  /**
   * This function will return the Mega menu dynamic contents..
   */
  public String getMegamenuDynamicContent(){
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      Map getMegamenumap = commonBusiness.getMegamenuValue();
      Clob clob = (Clob)getMegamenumap.get("o_megamenu");
      String htmlMegamenu = new String();
      htmlMegamenu = getClobValue(clob);
      return htmlMegamenu;
      /*StringBuffer htmlTextMega = new StringBuffer();
      String htmlText = new String();
      DataModel dataModel = new DataModel();
      Vector parameters = new Vector();
      parameters.add("220703");
      try{
        htmlText = dataModel.getString("WU_UTIL_PKG.mega_menu_degrees_list", parameters);
      }catch(Exception e){
        e.printStackTrace();
      }finally{
        dataModel = null;
        parameters.clear();
        parameters = null;
      }
      return htmlText;*/
  } 
  
  /**
    * This function will return the SHTML Content
    */
  public String getSHTMLContent(String shtmlId){
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
     Map getSHTMLMap = commonBusiness.getSHTMLData(shtmlId);
     Clob clob = (Clob)getSHTMLMap.get("P_HTML_CONTENT");
     String htmlContent = new String();
     htmlContent = getClobValue(clob);
     return htmlContent;
   }
   
  public String getCategorydetails(String subjectCategoryId, String requestValue, HttpServletRequest request) 
  {
     String categoryName = "";
     try {
          DataModel dataModel = new DataModel();
          Vector parameters = new Vector();     
          parameters.add(subjectCategoryId);       
          parameters.add(requestValue); 
          categoryName = dataModel.getString("WU_UTIL_PKG.get_category_name_code_fn", parameters);
        } catch(Exception exception) {
           exception.printStackTrace();
        }
        return categoryName;
    }
    //
   public String formProviderResultFilterParam(HttpServletRequest request){
    String filter = "";
    String removeModuleParameter = (String)request.getAttribute("removeModuleParameter");
           removeModuleParameter = !GenericValidator.isBlankOrNull(removeModuleParameter)? removeModuleParameter: "";
    String removePostcodeParameter = (String)request.getAttribute("removePostcodeParameter");
           removePostcodeParameter = !GenericValidator.isBlankOrNull(removePostcodeParameter)? removePostcodeParameter: "";
    String module = "";
    if(!("YES").equals(removeModuleParameter)){
      module = request.getParameter("module");
      module = !GenericValidator.isBlankOrNull(module)? module: "";
    }
    String distance = "";
    String postCode = "";
    if(!("YES").equals(removePostcodeParameter)){
      distance = request.getParameter("distance");
      distance = !GenericValidator.isBlankOrNull(distance)? distance: "";
      postCode = request.getParameter("postcode");
      postCode = !GenericValidator.isBlankOrNull(postCode)? postCode: "";
    }
    String q = request.getParameter("q");
      q = !GenericValidator.isBlankOrNull(q)? q: "";
    String subject = request.getParameter("subject");
      subject = !GenericValidator.isBlankOrNull(subject)? subject : "";
    String campusType = request.getParameter("campus-type");
      campusType = !GenericValidator.isBlankOrNull(campusType)? campusType: "";
    String locType = request.getParameter("location-type");
      locType = !GenericValidator.isBlankOrNull(locType)? locType: "";
    String ucasTarrifMax = request.getParameter("ucas-points-max");
      ucasTarrifMax = !GenericValidator.isBlankOrNull(ucasTarrifMax)? ucasTarrifMax: "";
    String ucasTarrifMin = request.getParameter("ucas-points-min");
      ucasTarrifMin = !GenericValidator.isBlankOrNull(ucasTarrifMin)? ucasTarrifMin: "";
    String empRateMax = request.getParameter("employment-rate-max");
      empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
    String empRateMin = request.getParameter("employment-rate-min");
      empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
    String jacs = request.getParameter("jacs");//3_JUN_2014
      jacs = !GenericValidator.isBlankOrNull(jacs)? jacs: "";
    String isClearing = (String)request.getAttribute("searchClearing");//30_JUN_2015
      isClearing = !GenericValidator.isBlankOrNull(isClearing)? isClearing: "";  
        
    if("TRUE".equals(isClearing)){//30_JUN_2015
      filter += "&clearing";
    }  
    if (!GenericValidator.isBlankOrNull(q)) {
      filter += "&q=" + q;
    }
    if (!GenericValidator.isBlankOrNull(subject)) {
      filter += "&subject=" + subject;
    }
    if (!GenericValidator.isBlankOrNull(module)) {
      filter += "&module=" + module;
    }
    if (!GenericValidator.isBlankOrNull(distance)) {
      filter += "&distance=" + distance;
    }
    if (!GenericValidator.isBlankOrNull(postCode)) {
      filter += "&postcode=" + postCode;
    }
    if (!GenericValidator.isBlankOrNull(empRateMax)) {
      filter += "&employment-rate-max=" + empRateMax;
    }
    if (!GenericValidator.isBlankOrNull(empRateMin)) {
      filter += "&employment-rate-min=" + empRateMin;
    }
    if (!GenericValidator.isBlankOrNull(ucasTarrifMax)) {
      filter += "&ucas-points-max=" + ucasTarrifMax;
    }
    if (!GenericValidator.isBlankOrNull(ucasTarrifMin)) {
      filter += "&ucas-points-min=" + ucasTarrifMin;
    }
    if (!GenericValidator.isBlankOrNull(jacs)) {//3_JUN_2014
      filter += "&jacs=" + jacs;
    }
   return filter;
  }

  public void getBasketDetails(HttpServletRequest request, String basketId)
     {
       BasketBean bean = new BasketBean();
       bean.setBasketId(basketId);      
       ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
       Map getBasketDetailsMap = commonBusiness.getBasketDetails(bean);
       List basketDetails = new ArrayList();
       basketDetails = (ArrayList)getBasketDetailsMap.get("lc_get_basket_pod");
       if(basketDetails != null && basketDetails.size() > 0)
       {
         request.setAttribute("basketDetails", basketDetails);
       }
     }
     
   /**
    * This function is used to get basket details 
    * @param request, basketId, assocType
    * Added for 19_MAY_2015_REL
    */ 
    public void getBasketDetails(HttpServletRequest request, String basketId, String assocType, String userId, String tabSelected)
     {
       BasketBean bean = new BasketBean();
       bean.setBasketId(basketId);
       bean.setAssociationType(tabSelected);
       bean.setUserId(userId);
       ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
       Map getBasketDetailsMap = commonBusiness.getBasketDetails(bean);
       List basketDetails = new ArrayList();
       basketDetails = (ArrayList)getBasketDetailsMap.get("lc_get_basket_pod");
       String basketCount = (String)getBasketDetailsMap.get("p_basket_count");
       request.setAttribute("newBasketCount", basketCount);
        request.setAttribute("assocTypeCode", tabSelected);
       if(basketDetails != null && basketDetails.size() > 0)
       {
         request.setAttribute("comparisonList", basketDetails);
       }
     }

   public String deleteBasket(String basketId, HttpServletRequest request)
   {
     DataModel dataModel  =   new DataModel();
     Vector vector        =   new Vector();
     String msg   =   null;
     try {
       vector.clear ();
       vector.add(basketId);
       msg = dataModel.getString("HOT_WUNI.WU_BASKET_PKG.DELETE_BASKET_FN", vector);
     } catch(Exception exception)  {
       exception.printStackTrace();
     }
     return msg;
   }
   
  public int getUserBasketContentCount(String userId, HttpServletRequest request)
  {
    DataModel dataModel  =   new DataModel();
    Vector vector        =   new Vector();
    String basketContentCount   =   "0";
    String userid = GenericValidator.isBlankOrNull(userId) ? "0" : userId;
    try {
      vector.clear ();
      vector.add(userid);
      basketContentCount = dataModel.getString("wu_user_profile_pkg.get_user_basket_cnt_fn", vector);
    } catch(Exception exception)  {
      exception.printStackTrace();
    }
    basketContentCount = GenericValidator.isBlankOrNull(basketContentCount) ? "0" : basketContentCount;
    return Integer.parseInt(basketContentCount);
  }   
   
   //24-JUN-2014
  /**
    *  Finction that returns all the user selected/uploaded images to the server for the specified userid/groupid 
    * @param form, userId, groupId, request
    * @return userPhotos as List
    */

   public List getAllCoursesAndOpenDaysInformation(String collegeId) 
   {
       DataModel dataModel   =   new DataModel();
       ResultSet resultset   =   null;    
       ArrayList list        =   new ArrayList();  
       try
        {
           ReviewListBean bean = null;
           Vector vector     =   new Vector();
           vector.clear ();           
           vector.add(collegeId);   // collegeId
           resultset = dataModel.getResultSet("WU_UTIL_PKG.GET_UNIV_OD_ALL_COURSE_BTN_FN", vector);
           while(resultset.next()) {
              bean = new ReviewListBean();
              bean.setViewAllCourseUrl(resultset.getString("view_all_course_url"));
              bean.setOpenDaysUrl(resultset.getString("open_days_url"));              
             list.add(bean);
           }
        } catch(Exception exception) {
            exception.printStackTrace();
        } finally {
            dataModel.closeCursor(resultset);
        }
       return list;
  }
  /**
   * Function will return trackid Unique session/ stats logging
   * Created by Amir
   * @param request
   */
  public void getUserTrackSessionID(HttpServletRequest request, HttpServletResponse response) 
  {
      DataModel dataModel = new DataModel();
      ResultSet resultset = null;
      Vector vector = new Vector();    
      String userTrackSessionId = "";
      String userId="";
      String userTrackId="";   
      String tempUserTrackId= "",tempUserId="";        
      
      tempUserTrackId = new SessionData().getData(request, "userTrackId");
      tempUserId      = new SessionData().getData(request,"y");                          
      
      if(tempUserTrackId!=null && !"".equals(tempUserTrackId)) {
        userTrackSessionId = tempUserTrackId;
      }else{
        userTrackSessionId = "0";
      }
      if(tempUserId!=null && !"".equals(tempUserId)) {
        userId = tempUserId;
      }else{
        userId = "0";
      }
      try {
        if(("0".equals(userTrackSessionId) || !"0".equals(userId)))
        {
          vector.add(userTrackSessionId);
          vector.add(userId);
          resultset = dataModel.getResultSet("hot_wuni.gen_tracking_session_id_fn", vector);      
          if(resultset!=null) {
            while (resultset.next()) {
              userId = resultset.getString(1);
              userTrackId = resultset.getString(2);
            }
          }      
          if(userTrackId!=null && !"".equals(userTrackId) && ("".equals(tempUserTrackId) || tempUserTrackId==null)) {
            new SessionData().addData(request, response, "userTrackId", userTrackId);
          }            
        }  
        } catch (Exception exception) {
          exception.printStackTrace();
      } finally {
          dataModel.closeCursor(resultset);
      }                 
    }
  /**
   * Function will return userType as NewUser or ReturnUser based on this user Homepage will be shown
   * 16-Sep-14 Created by Amir
   * @param request
   * @param response
   */
    public void setUserHomePage(HttpServletRequest request,HttpServletResponse response)
    {
      String ckeUserHomeType = new CookieManager().getCookieValue(request, "ckeUserHomeType");            
      String sesUserHomeType = new SessionData().getData(request, "sesUserHomeType");      
      if(ckeUserHomeType==null){
        Cookie cookie = new CookieManager().createCookie("ckeUserHomeType", "newHome");
        response.addCookie(cookie);
        new SessionData().addData(request, response,"sesUserHomeType", "newHome");
      }else if(ckeUserHomeType!=null && (sesUserHomeType==null || "".equals(sesUserHomeType))) {        
        Cookie cookie = new CookieManager().createCookie("ckeUserHomeType", "retHome");
        response.addCookie(cookie);
        new SessionData().addData(request, response,"sesUserHomeType", "retHome");
      }
    }
  /**
   * Function will return user TimeLine pod to show in Homepage
   * 16-Sep-14 Created by Amir
   * @param request    
   * @param userId
   */
    public void getUserTimeLineTrackPod(HttpServletRequest request) 
    {
      String userId = new SessionData().getData(request, "y");
      if(userId != null && !"".equals(userId) && !"0".equals(userId)) {
        IHomeBusiness homeBusiness = (IHomeBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_HOME_BUSINESS);
        Map userTimeLinePodMap = homeBusiness.getUserTimeLinePod(userId);
        if(userTimeLinePodMap != null && userTimeLinePodMap.size() > 0) {
          List userTimeLinePodList = (List)userTimeLinePodMap.get("PC_TIMELINE_ITEMS");
          if(userTimeLinePodList != null && userTimeLinePodList.size() > 0) {
            request.setAttribute("userTimeLinePodList", userTimeLinePodList);
            request.setAttribute("userTimeLineLen", String.valueOf(userTimeLinePodList.size()));
          }
        }
      }
    }
   /**
     * Function will return prospectus cookie id 
     * @param request    
     * Added for prospectus page redesign for 28_OCT_2014_REL
    */
    public String checkProspectusCookieStatus(HttpServletRequest request) {
      Cookie cookies[] = request.getCookies();
      String cookieName = "";
      String cookie_pros_basket_id = "";
      if (cookies != null) {
        for (int i = 0; i < cookies.length; i++) {
          cookieName = cookies[i].getName();
          if (cookieName != null && cookieName.trim().equalsIgnoreCase("prospectusBasketId")) {
            cookie_pros_basket_id = cookies[i].getValue();
            if (cookie_pros_basket_id != null && !cookie_pros_basket_id.equals("")) {
              break;
            }
          }
        }
      }
      return cookie_pros_basket_id;
    }

   /**
     *   This function is used to get the college display name for given collegeid 
     */
   public String getCollegeNameDisplay(String collegeId) {
      String collegeName = null;
      try {
        DataModel dataModel = new DataModel();
        Vector vector = new Vector();
        vector.clear();
        vector.add(collegeId);
        collegeName = dataModel.getString("hot_wuni.wu_util_pkg.get_college_display_fn", vector);
      } catch (Exception exception) {
        exception.printStackTrace();
      }
      return collegeName;
    }
  /**
   * This function is used to get AwardingBodyId for given collegeid 
   * @param collegeId
   * @return
   */
  public String getSchAwardingBodyId(String collegeId) {
     String schAwardId = null;
     try {
       DataModel dataModel = new DataModel();
       Vector vector = new Vector();
       vector.clear();
       vector.add(collegeId);
       schAwardId = dataModel.getString("hot_wuni.wu_sch_search_pkg.get_awarding_body_id", vector);
     } catch (Exception exception) {
       exception.printStackTrace();
     }
     return schAwardId;
   }
  /**
   * This function is used to get collegeId for given ScholarshipId 
   * @param schId
   * @return
   */
  public String getSchCollegeId(String schId) {
     String schCollegeId = null;
     try {
       DataModel dataModel = new DataModel();
       Vector vector = new Vector();
       vector.clear();
       vector.add(schId);
       schCollegeId = dataModel.getString("hot_wuni.wu_sch_search_pkg.get_scholarship_college_id", vector);
     } catch (Exception exception) {
       exception.printStackTrace();
     }
     return schCollegeId;
  }
  /**
   * This function is used to return department profile status 
   * @param myhcProfId
   * @return
   */
  public String isSpProfExpired(String myhcProfId){
    String isSpExpired = null;
    try {
      DataModel dataModel = new DataModel();
      Vector vector = new Vector();
      vector.clear();
      vector.add(myhcProfId);
      isSpExpired = dataModel.getString("hot_wuni.wu_util_pkg.check_profile_expire_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return isSpExpired;
  }
    
  /**
   * @see This function is used to get the encrypted or decrypted userid name for given decrypted or encrypted userid.
   * @param userId
   * @param encryptedUserId
   * @return String
   * @since WU_537_20150224
   * @author Karthi
   */    
  public String getEncryptedOrDecryptedUserId(String userId, String encryptedUserId){
     String decryptOrEncryptUserId = null;
     try {
       DataModel dataModel = new DataModel();
       Vector vector = new Vector();
       vector.clear();
       vector.add(userId);
       vector.add(encryptedUserId);
       decryptOrEncryptUserId = dataModel.getString("wu_user_profile_pkg.do_encrypt_decrypt_user_id_fn", vector);
     } catch (Exception exception) {
       exception.printStackTrace();
     }
     return decryptOrEncryptUserId;
  }
  
  /**
   * @see This function is used to ceck whether the given email or userId is blocked or not.
   * @param emailId
   * @param userId
   * @return String
   * @since WU_537_20150224
   * @author Karthi
   */    
  public String checkEmailBlocked(String emailId, String userId){
     String blockedStatus = "0";
     /*try {
       DataModel dataModel = new DataModel();
       Vector vector = new Vector();
       vector.clear();
       vector.add(emailId);
       vector.add(userId);
       blockedStatus = dataModel.getString("wu_user_profile_pkg.check_user_status_fn", vector);
     } catch (Exception exception) {
       exception.printStackTrace();
     }*/
     ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
     RegistrationBean registrationBean = new RegistrationBean();
     //Removed extra conditions and changed 'p_user_status' String conversion type for 08_Aug_2017, By Thiyagu G
     if(!GenericValidator.isBlankOrNull(emailId)){ 
       registrationBean.setEmailAddress(emailId);
       registrationBean.setUserId(userId);
       Map checkEmailBlockedMap = commonBusiness.checkEmailBlocked(registrationBean);
       if(checkEmailBlockedMap.get("p_user_status") != null){
         blockedStatus = String.valueOf(checkEmailBlockedMap.get("p_user_status"));
       }       
     }    
     return blockedStatus;
  }  
  public String storeReserveOpendayList(HttpServletRequest request,HttpServletResponse response,String openDayResUniIds) {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
    OpenDaysBean openDaysBean = new OpenDaysBean();    
    String userId = new SessionData().getData(request, "y");    
    if(!GenericValidator.isBlankOrNull(openDayResUniIds) && !GenericValidator.isBlankOrNull(userId) && !"0".equals(userId)){    
      openDaysBean.setCollegeId(openDayResUniIds);
      openDaysBean.setUserId(userId);
      commonBusiness.saveReserveOpendayUniList(openDaysBean);
    }
    return null;
  }
  
  /**
   * @see Function is used for users track action on the review viewed
   * @param request
   * @param collegeId
   * @param reviewId
   * @return
   * @author Amirtharaj
   * @Since 17_MAR_15
   *
   * Modification history:
   * **************************************************************************************************************
   * Author     Relase Date              Modification Details
   * **************************************************************************************************************
   * Yogeswari  19.05.2015               Added  tracking_session_id
   */
 
  /**
    * This function is used to load the Top course providers for all the review category.
    * @return Top 10 courseprovider as Collection object.
    * @author Amirtharaj
    * @Since 22_MAR_15
    */
    public String getStudChoiceOverallRatingPod(HttpServletRequest request,String listType) {
      DataModel dataModel = new DataModel();
      String loadYear = "";
      String currYear = new CommonFunction().getWUSysVarValue("CURRENT_YEAR_AWARDS");    
      ResourceBundle resourcebundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
      //String curYearVal = resourcebundle.getString("wuni.studchoice.award.currentyear");
      if(request.getAttribute("lastYearVal")!=null && !"".equals(request.getAttribute("lastYearVal"))){ 
        loadYear = (String)request.getAttribute("lastYearVal");
      }else{
        loadYear = currYear;
      }      
      String loadList = null;
      if (!GenericValidator.isBlankOrNull(listType) && "Y".equals(listType)) {
        loadList = "Y";
      }
      StudentAwardsBean bean = null;
      ResultSet resultSet = null;
      Vector vector = new Vector();
      vector.add(loadYear);    
      vector.add(loadList);
      ArrayList list = new ArrayList();
      try {        
        resultSet = dataModel.getResultSet("wu_reviews_pkg.get_student_awards_fn", vector);
        while (resultSet.next()) {
          bean = new StudentAwardsBean();
          bean.setCollegeId(resultSet.getString("college_id"));
          bean.setCollegeName(resultSet.getString("college_name"));
          bean.setCollegeDisplayName(resultSet.getString("college_name_display"));
          bean.setUniHomeURL(new SeoUrls().construnctUniHomeURL(bean.getCollegeId(), bean.getCollegeName()));
          bean.setQuestionId(resultSet.getString("question_id"));
          bean.setQuestionTitle(resultSet.getString("question_title"));
          bean.setRating(resultSet.getString("rating"));        
          if (!GenericValidator.isBlankOrNull(bean.getRating())) {
            bean.setRatingPercent(String.valueOf(Double.parseDouble(bean.getRating()) * 20) + "%");
          }
          bean.setSponsorBy(resultSet.getString("sponsor_by"));
          bean.setSponsorURL(resultSet.getString("sponsor_url"));
          bean.setDisplaySeq(resultSet.getString("display_seq"));
          String questionTitle = resultSet.getString("question_title");
          if (questionTitle != null && questionTitle.trim().equalsIgnoreCase("Accommodation")) {
            bean.setDisplayTitle("student accommodation");
          } else if (questionTitle != null && questionTitle.trim().equalsIgnoreCase("City Life")) {
            bean.setDisplayTitle("student life");
          } else {
            bean.setDisplayTitle(new CommonUtil().toLowerCase(questionTitle));
          }
          String seoText = new CommonFunction().seoQuestionTitle(resultSet.getString("question_title"));
          bean.setDisplayTitle(seoText);
          list.add(bean);
        }
      } catch (Exception exception) {
        exception.printStackTrace();
        } finally {
          dataModel.closeCursor(resultSet);
      }
      if (list != null && list.size() > 0) {
        request.setAttribute("studAwardList", list);
      }
      request.setAttribute("studAwdCurYear", currYear);    
      //if(!GenericValidator.isBlankOrNull(curYearVal) && !GenericValidator.isBlankOrNull(currYear) && currYear.equalsIgnoreCase(curYearVal.trim())){
        request.setAttribute("studAwdYearDesc", "current");      
      /*}else{
        request.setAttribute("studAwdYearDesc", "last");
      }*/
      return currYear;
    }
    
    /**
    * This function is used to load final choices selected by the user
    * @return final 5 slots provided by the user
    * @author Amirtharaj
    * @Since 21_JUL_15
    */  
    public void getMyFinalChoicePod(HttpServletRequest request){
    String userId = new SessionData().getData(request, "y");
    if(userId != null && !"".equals(userId) && !"0".equals(userId)) {
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      MyFinalChoiceBean myFinalchBean = new MyFinalChoiceBean();
      myFinalchBean.setUserId(userId);
      Map myChoiceHomeMap = commonBusiness.getMyFinalChoiceData(myFinalchBean);
      if (myChoiceHomeMap != null) {
        List myFinalChoiceList = (List)myChoiceHomeMap.get("pc_final_choice_list");
        if (myFinalChoiceList != null && myFinalChoiceList.size() > 0) {
          request.setAttribute("myFinalChoiceList", myFinalChoiceList);
          Iterator myChoiceIter = myFinalChoiceList.iterator();
          while (myChoiceIter.hasNext()) {
            MyFinalChoiceBean finalChoiceBean = (MyFinalChoiceBean)myChoiceIter.next();       
            if(!GenericValidator.isBlankOrNull((String)request.getAttribute("hideConfirmBut")) && "C".equalsIgnoreCase(finalChoiceBean.getSlotPositionStatus())){
              request.setAttribute("hideConfirmBut", "false");
            }else{
              if(GenericValidator.isBlankOrNull((String)request.getAttribute("hideConfirmBut"))){
                if(GenericValidator.isBlankOrNull(finalChoiceBean.getSlotPositionStatus()) || "S".equalsIgnoreCase(finalChoiceBean.getSlotPositionStatus()) || "E".equalsIgnoreCase(finalChoiceBean.getSlotPositionStatus())) {
                  request.setAttribute("hideConfirmBut", "true");
                }else{
                  request.setAttribute("hideConfirmBut", "false");  
                }
              }
            }
            if(GenericValidator.isBlankOrNull((String)request.getAttribute("myFinalChoiceExist")) || "false".equals((String)request.getAttribute("myFinalChoiceExist"))){
              if(!GenericValidator.isBlankOrNull(finalChoiceBean.getFinalChoiceId())){
                request.setAttribute("myFinalChoiceExist","true");          
              }else{
                request.setAttribute("myFinalChoiceExist","false"); 
              }       
            }                                                                       
          }
        }
      }        
    }
  }    
//
/** 
  * This method is used to return the domain name and context path
  * @param request
  * @param includeContextPath
  * @return domainname+conrextpath
  */
 public String getDomainName(HttpServletRequest request, String includeContextPath){
  String domainName = GlobalConstants.WHATUNI_SCHEME_NAME+request.getServerName()+ ("Y".equals(includeContextPath)?request.getContextPath():"");
  return domainName;
 }
  /**
   * @checkProviderDeleted method to check the provider is deleted or not..
   * @param collegeId
   * @return providerStatus as String (YES / NO)
   * @author Prabhakaran V.
   * @since 16:02:2016_REL
   */
  public String checkProviderDeleted(String collegeId){
    String providerStatus = "YES";
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
    ReviewListBean reviewBean = new ReviewListBean();    
    if(!GenericValidator.isBlankOrNull(collegeId)){    
      reviewBean.setCollegeId(collegeId);
      Map providerStatusFlag = commonBusiness.getProviderDeletedFlag(reviewBean);
      providerStatus = (String)providerStatusFlag.get("RetVal");
    }
    return providerStatus;
  }
  /**
   * Method for checking degree course
   * @param collegeId
   * @return Y / N
   */
  public String checkDegreeCourse(String collegeId){
    String degreeStatus = "N";
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
    CourseDetailsBean courseDetailBean = new CourseDetailsBean();    
    if(!GenericValidator.isBlankOrNull(collegeId)){    
      courseDetailBean.setCollegeId(collegeId);
      Map providerStatusFlag = commonBusiness.getDegreeCourseFlag(courseDetailBean);
      degreeStatus = (String)providerStatusFlag.get("RetVal");
    }
    return degreeStatus;
  }
    
  /**
    * This method is used for SSL preparation change for 29_Mar_2016, By Thiyagu G.
    * 
    * This method is used to return the scheme name
    * @param request    
    * @return schemename +"://"
    */
   public String getSchemeName(HttpServletRequest request){    
    return GlobalConstants.WHATUNI_SCHEME_NAME;
   }
  /**
   * @getCourseSchemaList method to get the schema tag in cd page..
   * @param courseId
   * @param collegeId
   * @return schemaTag as String
   * @author Prabhakaran V.
   * @since 24.01.2017
   */
  public String getCourseSchemaList(String courseId, String collegeId, String clearingFlag) { //Added clearing flag for course title changes on 16_May_2017, By Thiyagu G.
    String schemaTag = null;
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    CourseDetailsVO courseVO = new CourseDetailsVO();
    courseVO.setCollegeId(collegeId);
    courseVO.setCourseId(courseId);
    courseVO.setClearingFlag(clearingFlag);
    Map courseSchemaMap = commonBusiness.getCourseScemaInfo(courseVO);
    if (courseSchemaMap != null && !courseSchemaMap.isEmpty()) {
      ArrayList schemaInfoList = (ArrayList)courseSchemaMap.get("RetVal");
      if (schemaInfoList != null && schemaInfoList.size() > 0) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\"@context\": \"http://schema.org\",\n" + "  \"@type\": \"Course\",\n");
        Iterator schemaItr = schemaInfoList.iterator();
        while (schemaItr.hasNext()) {
          CourseDetailsVO schemaVO = (CourseDetailsVO)schemaItr.next();
          buffer.append("\"name\": \"" + schemaVO.getCourseTitle() + "\",\n");
          if(!GenericValidator.isBlankOrNull(schemaVO.getUcasCode())){
            buffer.append("\"courseCode\": \"" + schemaVO.getUcasCode() + "\",\n");
          }
          buffer.append("\"description\": \"" + (!GenericValidator.isBlankOrNull(schemaVO.getCourseSummary()) ? replaceTags(schemaVO.getCourseSummary()) : "Please see our website for full details on this course, including start date and duration, rankings, and further information about the provider.") + "\",\n");
          buffer.append("\"provider\": {\n" + "    \"@type\": \"CollegeOrUniversity\",\n");
          buffer.append("\"name\": \"" + schemaVO.getCollegeNameDisplay() + "\",\n");
          buffer.append("\"sameAs\": \"" + schemaVO.getCdPageUrl() + "\"\n");
          buffer.append("  }");
        }
        schemaTag = buffer.toString();
      }
    }
   return schemaTag;
  }
  
  public String replaceTags(String inVal){
    String outValue = "";
    if(!GenericValidator.isBlankOrNull(inVal)){
      outValue = inVal.replaceAll("\\<.*?>","");
    }
			 return outValue;
  }
  
  public void providerURLredirection(String providerName, String providerId, String pageType, HttpServletResponse response){
    String redirectURL = null;
    if("reviewlist".equals(pageType)){
      redirectURL = new SeoUrls().constructReviewPageSeoUrl(providerName, providerId);
    }else if("opendays".equals(pageType)){
      redirectURL = new SeoUrls().constructOpendaysSeoUrl(providerName, providerId);
    }
    response.setStatus(response.SC_MOVED_PERMANENTLY);
    response.setHeader("Location", redirectURL);
    response.setHeader("Connection", "close");
  }
 public String getDeviceFlag(HttpServletRequest request){
  String deviceFlag = "desktop";
 String userAgent = request.getHeader("user-agent");
  if(userAgent.matches(".*BlackBerry.*")) {
    deviceFlag = "mobile";
	 } else if(userAgent.matches(".*Android.*")) {
    deviceFlag = "mobile";
	 } else if(userAgent.matches(".*iPhone.*")) {
    deviceFlag = "mobile";
	 }else if(userAgent.matches(".*iPad.*")) {
    deviceFlag = "ipad";
	 }
return deviceFlag;
}
 /**
  * @see this method to check whether user is in clearing journey for profile page when clearing is ON.
  * @author Sangeeth.S
  * @param request
  * @return
  */
 public String getClearingUserJourneyFlag(HttpServletRequest request){
   String userJourney = "";
   HttpSession session = request.getSession();
   String clearingOnOff = getWUSysVarValue("CLEARING_ON_OFF");
   String searchType = (String)session.getAttribute("USER_TYPE");
   //String isClearingProfile = request.getQueryString();
   //System.out.println("isClearingProfile" + isClearingProfile);
   //if(GlobalConstants.ON.equalsIgnoreCase(clearingOnOff) && "CLEARING".equalsIgnoreCase(searchType) && !GenericValidator.isBlankOrNull(isClearingProfile) && "clearing".equalsIgnoreCase(isClearingProfile)){
    if(GlobalConstants.ON.equalsIgnoreCase(clearingOnOff) && "CLEARING".equalsIgnoreCase(searchType)){
     userJourney = "CLEARING_JOURNEY";
   }
   //System.out.println("userJourney : "+userJourney);
   return userJourney;
 }
 /**
  * @see to get apply now course details
  * @author Sangeeth.S
  * @param courseId
  * @param opportunityId
  * @return
  */
 public ArrayList getApplyNowCourseDtlsList(String courseId, String opportunityId, String userId){
   ApplyNowVO applyNowVO = new ApplyNowVO();
   Map cmmtApplyNowMap = null;
   ArrayList applyNowCourseDtlList = null;
   //
    applyNowVO.setUserId(userId);
    applyNowVO.setCourseId(courseId);
    applyNowVO.setOpportunityId(opportunityId);
   //DB call
   ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
   //
   //Getting details from DB
   cmmtApplyNowMap = commonBusiness.applyNowCourseDetails(applyNowVO);
   //
   //Setting Request atributes for using in JSP
    if (!CommonUtil.isBlankOrNull(cmmtApplyNowMap)) {                  
      applyNowCourseDtlList = (ArrayList)cmmtApplyNowMap.get("PC_APPLY_COURSE_DET");       
    }
    return applyNowCourseDtlList;
   //
 }
 /**
  * @see to check the user journey for the apply now button click
  * @author Sangeeth.S
  * @param mapping
  * @param form
  * @param request
  * @param response
  * @return 
  * @throws Exception
  */
 public String checkApplyNowUserJourney(HttpServletRequest request, HttpServletResponse response,HttpSession session)throws Exception{
   String journeyName = "PROVISIONAL_FORM";
   WhatuniGoController whatuniGoAction = new WhatuniGoController();
   request.setAttribute("checkUserJourneyFlag","Y");
   new WhatuniGoController().getWugoDetails(request, response, session);
   if(!GenericValidator.isBlankOrNull((String)request.getAttribute("JOURNEY_NAME"))){
     journeyName = (String)request.getAttribute("JOURNEY_NAME");
   }
   return journeyName;
 }  
 /* @see to get the latest updated date from the cookie or privacy or terms&condition policy.
  * @author Sangeeth.S   
  * @return 
  */
 public String getCookieLatestUpdatedDate()throws Exception{
   String cookieLatestUpdatedDate =  null;
   Map cookieLatUpdatedDateMap = null;   
   ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);       
   cookieLatUpdatedDateMap = commonBusiness.getCookieLatestUpdatedDate();   
   if (!CommonUtil.isBlankOrNull(cookieLatUpdatedDateMap)) {                  
     cookieLatestUpdatedDate = (String)cookieLatUpdatedDateMap.get("P_LATEST_DATE");       
   }
   return cookieLatestUpdatedDate;    
 }
 /* @see to get the full url from the request
  * @author    
  * @return 
  */
 public String getFullURL(HttpServletRequest request, boolean queryString) {
	    StringBuffer url = new StringBuffer(new CommonUtil().getRequestedURL(request));
		if (queryString) {
	      if (request.getQueryString() != null) {
		    url.append('?');
			url.append(request.getQueryString());
		  }
		}
		return url.toString();
	  }
 
 /**
  * @see This method is used to set the YOE value in session for d_session_log
  * @author Sujitha V
  * @param request
  * @param yearOfEntry
  */
  public static void getDSessionLogForYOE(HttpServletRequest request, String yearOfEntry) {
    HttpSession session = request.getSession();
    String yoeSession = (String) session.getAttribute(SpringConstants.SESSION_YOE_LOGGING);
    if(!StringUtils.equals(yearOfEntry, yoeSession)) {
      session.setAttribute(SpringConstants.SESSION_YOE_LOGGING, yearOfEntry);	
    }  
  }
}
