package WUI.utilities;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
  *
  * @SessionData.java
  * @Version : 2.0
  * @February 23 2007
  * @www.whatuni.com
  * @author By : Balraj. Selva Kumar
  * @Purpose  : Program used store/retrive/remove the request parameter data in HashMap to maintain the values in the session
  *             x-sessionid, y-userid, a-affiliateid, w-collegeid
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class SessionData {

  /**
    *   This function is used to set all the request parameters in to HsshMap
    * @param request
    * @return none.
    */
  public void setParameterValuestoSessionData(HttpServletRequest request, HttpServletResponse response) {
    Enumeration parameternames = request.getParameterNames();
    addData(request, response, "a", GlobalConstants.WHATUNI_AFFILATE_ID);
    while (parameternames.hasMoreElements()) {
      String parametername = (String)parameternames.nextElement();
      if (parametername != null && (parametername.equals("x") || parametername.equals("y") || parametername.equals("a"))) {
        new SessionData().addData(request, response, parametername, request.getParameter(parametername));
      }
    }
  }

  /**
    *   This function is used to add the corresponding request parameter value to the HashMap(SessionData).
    * @param request
    * @param dataKey
    * @param dataValue
    * @return Error or Empty String.
    */
  public String addData(HttpServletRequest request, HttpServletResponse response, String dataKey, String dataValue) {
    HttpSession session = request.getSession(false);
    HashMap sessionData = null;
    if (session != null) {
      //Changed userTrackId from session to cookie for 24_Jan_2017, By Thiyagu G
      if("userTrackId".equals(dataKey)){
        CookieManager cookieManager = new CookieManager();
        if(!"".equals(dataValue)){          
          Cookie getCookie = cookieManager.getCookie(request, dataKey);          
          if (getCookie == null) {
            Cookie cookie = CookieManager.createCookie(request,"userTrackId", dataValue);
            response.addCookie(cookie);
            addData(request, response, "sessionUserTrackId", dataValue);
          }          
        }else{
          Cookie userIdDeleteCookie = CookieManager.createCookie(request,"userTrackId", "");
          userIdDeleteCookie.setMaxAge(0);
          userIdDeleteCookie.setPath("/");       
          userIdDeleteCookie.setComment("EXPIRING USERID COOKIE at " + System.currentTimeMillis());
          response.addCookie(userIdDeleteCookie);
        }
      }else{
        sessionData = (HashMap)session.getAttribute("sessionData");
        if (sessionData == null) {
          sessionData = new HashMap();
        }
        sessionData.put(dataKey, dataValue);
        session.setAttribute("sessionData", sessionData);
      }      
    } else {
      return "Error! adding data to object";
    }
    return "";
  }

  /**
    *   This function is used to get the particular request parameter value from the HashMap.
    * @param request
    * @param dataKey
    * @return Request parameter value As String.
    */
  public String getData(HttpServletRequest request, String dataKey) {
    HttpSession session = request.getSession();
    HashMap sessionData = null;
    String dataValue = "";
    CookieManager cookieManager = new CookieManager();
    //Changed userTrackId from session to cookie for 24_Jan_2017, By Thiyagu G
    if("userTrackId".equals(dataKey)){
      dataValue = cookieManager.getCookieValue(request, "userTrackId") == null ? "" : cookieManager.getCookieValue(request, "userTrackId");
      if("".equals(dataValue)){
        dataValue = getData(request, "sessionUserTrackId");
      }
      session.removeAttribute("sessionUserTrackId");      
    }else{
      if (session != null) {
        sessionData = (HashMap)session.getAttribute("sessionData");
        if (sessionData != null) {
          dataValue = sessionData.get(dataKey) == null ? "" : String.valueOf(sessionData.get(dataKey));
        }
      } else {
        return "";
      }
    }
    return dataValue;
  }

  /**
    *   This function is used to remove particular request parameter from the HashMap.
    * @param request
    * @param dataKey
    * @return Error or Empty string.
    */
  public String removeData(HttpServletRequest request, HttpServletResponse response, String dataKey) {
    HttpSession session = request.getSession();
    HashMap sessionData = null;    
    //Changed userTrackId from session to cookie for 24_Jan_2017, By Thiyagu G
    if("userTrackId".equals(dataKey)){
      Cookie removeCookie = new CookieManager().deleteCookie(request, "userTrackId");
      response.addCookie(removeCookie);      
    }else{
      if (session != null) {
        sessionData = (HashMap)session.getAttribute("sessionData");
        if (sessionData != null) {
          if (sessionData.get(dataKey) != null) {
            sessionData.remove(dataKey);
          }
        }
        session.setAttribute("sessionData", sessionData);
      } else {
        return "";
      }
    }    
    return "Data Removed from object";
  }

  /**
    *   This function is used to clear the HashMap and stored into the session.
    * @param request
    * @return Success Message as String.
    */
  public String clearData(HttpServletRequest request, HttpServletResponse response) {
    HttpSession session = request.getSession();
    HashMap sessionData = null;
    if (session != null) {
      sessionData = (HashMap)session.getAttribute("sessionData");
      if (sessionData != null) {
        sessionData.clear();
      }
      session.setAttribute("sessionData", sessionData);
    }
    //Delete userTrackId cookie for 24_Jan_2017, By Thiyagu G
    Cookie clearCookie = new CookieManager().deleteCookie(request, "userTrackId");
    response.addCookie(clearCookie);
    return "Data cleared from object";
  }

}
