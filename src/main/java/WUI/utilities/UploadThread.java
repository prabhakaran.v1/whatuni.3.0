package WUI.utilities;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import WUI.admin.bean.UploadCollegeImageBean;
import WUI.admin.bean.UploadVideoBean;
import WUI.admin.controller.UploadVideoAction;
import WUI.videoreview.bean.VideoReviewBean;

import com.wuni.util.sql.DataModel;

/**
 * @UploadThread.java
 * @Version : 2.0
 * @Janaury 15 2008
 * @www.whatuni.com
 * @Created By : Balraj Selvakumar
 * @Purpose : This program used to update the limelight video to their server
 *          *************************************************************************************************************************
 *          Date Name Ver. Changes desc Rel Ver.
 *          *************************************************************************************************************************
 *
 */
public class UploadThread {
	public UploadThread() {
	}

	static final int BUFF_SIZE = 1024;
	static final byte[] buffer = new byte[BUFF_SIZE];

	/**
	 * This function is used to upload the video content & start the Thread for
	 * video uploading in Limelight server.
	 * 
	 * @param request
	 * @param form
	 * @param fileName
	 * @param videoReviewId
	 * @param userId
	 * @param collegeId
	 * @return none.
	 */

	public void UploadContent(HttpServletRequest request, ActionForm form, String fileName, String videoReviewId,
			String userId, String collegeId) {
		Thread thread = new VideoUploadThread("Video_" + videoReviewId, form, request, fileName, videoReviewId, userId,
				collegeId);
		thread.start();
	}

	/**
	 * This function is used to upload the video content & start the Thread for
	 * video uploading in Limelight server.
	 * 
	 * @param form
	 * @param uploadFile
	 * @param request
	 * @param fileName
	 * @param videoReviewId
	 * @param userId
	 * @param collegeId
	 * @return none.
	 */

	public void uploadLimelightVideo(ActionForm form, FormFile uploadFile, HttpServletRequest request, String fileName,
			String videoReviewId, String userId, String collegeId) {
		String returnString = "";
		URLConnection conn = null;
		try {
			String LIMELIGHT_VIDEO_FOLDER = new CommonFunction().getWUSysVarValue("WU_LIMELIGHT_FOLDER");
			URL servlet = new URL("http://www.bvno.net/alp/tools/PostFile.aspx");
			conn = servlet.openConnection();
			conn.setDoOutput(true);
			conn.setDoInput(true);
			conn.setUseCaches(false);
			String boundary = "---------------------------7d226f700d0";
			conn.setRequestProperty("Content-type", "multipart/form-data; boundary=" + boundary);
			conn.setRequestProperty("Cache-Control", "no-cache");
			DataOutputStream out = new DataOutputStream(conn.getOutputStream());
			out.writeBytes("--" + boundary + "\r\n");
			// Passing Form Parameter
			writeParam("CustomerID", "964", out, boundary); // Limelight CustomerID
			writeParam("Profile", "6534", out, boundary); // LimeLight ProfileID
			// writeParam("Category�", "WHATUNI", out, boundary); //Category for gropuping
			writeParam("FileNameToUse", LIMELIGHT_VIDEO_FOLDER + fileName, out, boundary); // LimeLight Video Display Name
			// Passing Video File
			writeFile("File1", uploadFile, out, boundary); // Uploaded Video File Name
			out.flush();
			out.close();
			// To get the response data from LimeLight Networks
			InputStream stream = conn.getInputStream();
			BufferedInputStream in = new BufferedInputStream(stream);
			int i = 0;
			while ((i = in.read()) != -1) {
				returnString = returnString + (char) i;
			}
			if (returnString != null && returnString.trim().length() > 0) {
				conn = null;
				/*
				 * // Check the video file in LUX Thread threads = new
				 * CheckVideoStatus("Status_" + videoReviewId, form, LIMELIGHT_VIDEO_FOLDER +
				 * fileName, request, videoReviewId, userId, collegeId); threads.start();
				 */
			}
			in.close();
		} catch (Exception exception) {
			conn = null;
		}
	}

	/**
	 * This function is used to write the parameters in to the Output Stream.
	 * 
	 * @param name
	 * @param value
	 * @param out
	 * @param boundary
	 * @return none.
	 * @throws Exception
	 */

	public void writeParam(String name, String value, DataOutputStream out, String boundary) throws Exception {
		try {
			out.writeBytes("content-disposition: form-data; name=\"" + name + "\"\r\n\r\n");
			out.writeBytes(value);
			out.writeBytes("\r\n" + "--" + boundary + "\r\n");
		} catch (Exception exception) {
			throw exception;
		}
	}

	/**
	 * This function is used to write the file content into the Output stream.
	 * 
	 * @param name
	 * @param filePath
	 * @param out
	 * @param boundary
	 * @return none.
	 * @throws Exception
	 */

	public void writeFile(String name, FormFile filePath, DataOutputStream out, String boundary) throws Exception {
		try {
			out.writeBytes("content-disposition: form-data; name=\"" + name + "\"; filename=\"" + filePath + "\"\r\n");
			out.writeBytes("content-type: application/octet-stream" + "\r\n\r\n");
			InputStream fileInputStream = filePath.getInputStream();
			while (true) {
				synchronized (buffer) {
					int amountRead = fileInputStream.read(buffer);
					if (amountRead == -1) {
						break;
					}
					out.write(buffer, 0, amountRead);
				}
			}
			fileInputStream.close();
			out.writeBytes("\r\n" + "--" + boundary + "\r\n");
		} catch (Exception fileException) {
			throw fileException;
		}
	}

	/**
	 * This function is used to establish the FIP connection between the Appserver
	 * and Limelight server.
	 * 
	 * @param fileName
	 * @return
	 * @return FTPConnection as URLConnection.
	 * @throws Exception
	 */

	public URLConnection getFTPConection(String fileName) throws Exception {
		URLConnection connection = null;
		StringBuffer sb = new StringBuffer("ftp://");
		sb.append("hotcourses-ht");
		sb.append(":");
		sb.append("44et7m");
		sb.append("@");
		sb.append("hotcourses.upload.llnw.net");
		sb.append("/");
		sb.append(fileName);
		try {
			URL url = new URL(sb.toString());
			connection = url.openConnection();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return connection;
	}

	/**
	 * This function is used to get the collegename for the given collegeid.
	 * 
	 * @param collegeId
	 * @return collegename as String.
	 */

	private String getCollegeName(String collegeId) {
		String collegeName = "";
		try {
			DataModel dataModel = new DataModel();
			Vector vector = new Vector();
			vector.clear();
			vector.add(
					(collegeId != null && !collegeId.equals("null") && collegeId.trim().length() > 0 ? collegeId.trim() : ""));
			collegeName = dataModel.getString("Get_College_Title_Fn", vector);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return collegeName;
	}

	/**
	 * This function is used to send EMAIL to the user who upload the video.
	 * 
	 * @param videoId
	 * @param userId
	 * @param collegeId
	 * @param request
	 * @return none.
	 */

	public void sendVideoSubmissionMail(String videoId, String userId, String collegeId, HttpServletRequest request) {
		DataModel dataModel = new DataModel();
		try {
			CommonFunction comnFn = new CommonFunction();
			String collegeName = getCollegeName(collegeId);
			String urlCollegeName = (collegeName != null && collegeName.trim().length() > 0)
					? comnFn.replaceHypen(comnFn.replaceSpecialCharacter(comnFn.replaceURL(collegeName)))
					: "";
			String p_url_view_review = "college-videos/" + urlCollegeName + "-videos/submitted-date/" + collegeId
					+ "/1/student-videos.html";
			// String p_url_view_review = "videoDetail.html?vrid="+videoId;
			Vector vector = new Vector();
			vector.add(GlobalConstants.WHATUNI_AFFILATE_ID); // ADDLIATE ID
			vector.add(userId); // user_id
			vector.add("WU_VIDEO_EMAIL"); // Html ID
			vector.add(p_url_view_review.toLowerCase()); // URL Link
			String value = dataModel.executeUpdate("wu_email_pkg.video_mail_fn", vector);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
}

/**
 * Thread class is used to create a thread to upload the video to the
 * limelightserver.
 */
class VideoUploadThread extends Thread {
	FormFile theFile = null;
	String fileName = null;
	String userId = null;
	ActionForm form = null;
	String videoReviewId = null;
	HttpServletRequest request = null;
	String collegeId = "";

	public VideoUploadThread(String threadName, ActionForm form, HttpServletRequest request, String fileName,
			String videoReviewId, String userId, String collegeId) {
		super(threadName);
		this.fileName = fileName;
		this.form = form;
		this.videoReviewId = videoReviewId;
		this.request = request;
		this.userId = userId;
		this.collegeId = collegeId;
		/*
		 * if (form instanceof VideoReviewBean) { theFile =
		 * ((VideoReviewBean)form).getFileUpload(); } if (form instanceof
		 * UploadVideoBean) { theFile = ((UploadVideoBean)form).getFileUpload(); } if
		 * (form instanceof UploadCollegeImageBean) { theFile =
		 * ((UploadCollegeImageBean)form).getLogoUpload(); }
		 */
	}

	/**
	 * Override thread run method.
	 */
	public synchronized void run() {
		try {
			new UploadThread().uploadLimelightVideo(form, theFile, request, fileName, videoReviewId, userId, collegeId);
			Thread.sleep(10000);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
}
/**
 * Class is used to create a thread to check the status of the video.
 */
/*
 * class CheckVideoStatus extends Thread { String fileName = null;
 * HttpServletRequest request = null; String videoReviewId = null; String userId
 * = null; ActionForm form = null; String collegeName = ""; String collegeId =
 * ""; public CheckVideoStatus(String threadName, ActionForm form, String
 * fileName, HttpServletRequest request, String videoReviewId, String userId,
 * String collegeId) { super(threadName); this.fileName = fileName; this.request
 * = request; this.videoReviewId = videoReviewId; this.userId = userId;
 * this.form = form; this.collegeId = collegeId; }
 * 
 */
