package WUI.utilities;

import java.util.Vector;

import com.wuni.util.sql.DataModel;

public class ScholarshipUtilities {

  /**
    *   This funciton is used to get the scholarship count for the given course provider.
    * @param collegeId
    * @param qualification
    * @param keyword
    * @param categoryCode
    * @return Scholarship count as String.
    * @throws Exception
    */
  public String getUniScholarshipCount(String collegeId, String qualification, String keyword, String categoryCode) throws Exception {
    String scholarshipCount = "0";
    DataModel datamodel = new DataModel();
    Vector parameters = new Vector();
    try {
      parameters.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      parameters.add(collegeId);
      parameters.add(new CommonFunction().replacePlus(keyword)); // Keyword
      parameters.add(qualification); // Qualification     
      parameters.add(categoryCode); //  CategoryCode           
      scholarshipCount = datamodel.getString("wu_scholarship_pkg.get_provider_sch_cnt_fn", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally{
      //nullify unused objects
      datamodel = null;
      parameters.clear();
      parameters = null;
    }
    return scholarshipCount;
  }

}
