package WUI.utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import spring.form.VideoReviewListBean;
import WUI.cpe.form.CollegeCpeInteractionDetailsWithMediaBean;
import WUI.search.form.HaveYouThoughtAboutBean;
import WUI.search.form.SearchBean;
import WUI.search.form.SimilarCoursesBean;
import WUI.utilities.form.CpeWorstPerformersBean;

import com.wuni.util.CommonValidator;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.sql.DataModel;

/**
 * contains all advert related utilities
 *
 * @author  Mohamed Syed
 * @since   wu_2.1.3__2010-Nov-10
 *
 */
public class AdvertUtilities {

  /**
   * give details about which type of order is this? like CPE/TENDACY/HYPRID
   *
   * @author  Mohamed Syed
   * @since   wu316_20110823
   *
   * @param subOrderItemId
   * @return orderType
   */
  public String getOrderType(String subOrderItemId) {
    CommonValidator validate = new CommonValidator();
    if (validate.isBlankOrNullOrZero(subOrderItemId)) {
      validate = null;
      return null;
    }
    String orderType = "";
    DataModel dataModel = new DataModel();
    Vector parameters = new Vector();
    parameters.add(subOrderItemId);
    //
    try {
      orderType = dataModel.getString("wu_cpe_advert_pkg.get_order_type_fn", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      //nullify unused objects
      dataModel = null;
      parameters.clear();
      parameters = null;
      validate = null;
    }
    return orderType;
  }

  /**
   * will check the given college is advertiser or nor by refering "w_dom_search_boosting"
   *
   * @param courseId
   * @return similarCoursesList
   *
   * @since wu307_20110419
   * @author Mohamed Syed
   */
  public ArrayList getSimilarCourses(String courseId) {
    ArrayList listOfSimilarCourses = new ArrayList();
    if (courseId != null & courseId.trim().length() > 0) {
      courseId = courseId.trim();
      DataModel datamodel = new DataModel();
      ResultSet similarCoursesRS = null;
      ResultSet enquiryRS = null;
      SimilarCoursesBean similarCoursesBean = null;
      String tmpUrl = "";
      //
      Vector parameters = new Vector();
      parameters.add(courseId);
      //
      try {
        similarCoursesRS = datamodel.getResultSet("WU_COURSE_SEARCH_PKG.GET_SIMILAR_COURSES_FN", parameters);
        if (similarCoursesRS != null) {
          while (similarCoursesRS.next()) {
            similarCoursesBean = new SimilarCoursesBean();
            //
            similarCoursesBean.setCourseId(similarCoursesRS.getString("COURSE_ID"));
            if (similarCoursesBean.getCourseId() == null || similarCoursesBean.getCourseId().trim().length() == 0) {
              break;
            }
            //
            similarCoursesBean.setCourseTitle(similarCoursesRS.getString("COURSE_TITLE"));
            similarCoursesBean.setCourseQualification(similarCoursesRS.getString("QUALIFICATION"));
            //
            similarCoursesBean.setCollegeId(similarCoursesRS.getString("COLLEGE_ID"));
            similarCoursesBean.setCollegeName(similarCoursesRS.getString("COLLEGE_NAME"));
            similarCoursesBean.setCollegeNameDisplay(similarCoursesRS.getString("COLLEGE_NAME_DISPLAY"));
            similarCoursesBean.setCollegeLogo(similarCoursesRS.getString("COLLEGE_LOGO"));
            //
            enquiryRS = (ResultSet)similarCoursesRS.getObject("ENQUIRY_DETAILS");
            CollegeCpeInteractionDetailsWithMediaBean collegeCpeInteractionDetailsWithMedia = null;
            try {
              if (enquiryRS != null) {
                while (enquiryRS.next()) {
                  collegeCpeInteractionDetailsWithMedia = new CollegeCpeInteractionDetailsWithMediaBean();
                  //        
                  collegeCpeInteractionDetailsWithMedia.setOrderItemId(enquiryRS.getString("ORDER_ITEM_ID"));
                  collegeCpeInteractionDetailsWithMedia.setSubOrderItemId(enquiryRS.getString("SUBORDER_ITEM_ID"));
                  collegeCpeInteractionDetailsWithMedia.setMyhcProfileId(enquiryRS.getString("MYHC_PROFILE_ID"));
                  collegeCpeInteractionDetailsWithMedia.setProfileType(enquiryRS.getString("PROFILE_TYPE"));
                  collegeCpeInteractionDetailsWithMedia.setAdvertName(enquiryRS.getString("ADVERT_NAME"));
                  collegeCpeInteractionDetailsWithMedia.setCpeQualificationNetworkId(enquiryRS.getString("NETWORK_ID"));
                  //
                  collegeCpeInteractionDetailsWithMedia.setSubOrderEmail(enquiryRS.getString("EMAIL"));
                  collegeCpeInteractionDetailsWithMedia.setSubOrderProspectus(enquiryRS.getString("PROSPECTUS"));
                  //
                  tmpUrl = enquiryRS.getString("WEBSITE");
                  if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                      tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
                  } else {
                    tmpUrl = "";
                  }
                  collegeCpeInteractionDetailsWithMedia.setSubOrderWebsite(tmpUrl);
                  //
                  tmpUrl = enquiryRS.getString("EMAIL_WEBFORM");
                  if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                      tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
                  } else {
                    tmpUrl = "";
                  }
                  collegeCpeInteractionDetailsWithMedia.setSubOrderEmailWebform(tmpUrl);
                  //
                  tmpUrl = enquiryRS.getString("PROSPECTUS_WEBFORM");
                  if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                      tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
                  } else {
                    tmpUrl = "";
                  }
                  collegeCpeInteractionDetailsWithMedia.setSubOrderProspectusWebform(tmpUrl);
                  // media details from db
                  collegeCpeInteractionDetailsWithMedia.setMediaId(enquiryRS.getString("MEDIA_ID"));
                  collegeCpeInteractionDetailsWithMedia.setMediaType(enquiryRS.getString("MEDIA_TYPE"));
                  collegeCpeInteractionDetailsWithMedia.setMediaPath(enquiryRS.getString("MEDIA_PATH"));
                  collegeCpeInteractionDetailsWithMedia.setMediaPathThumb(enquiryRS.getString("THUMB_MEDIA_PATH"));
                  //customized media details
                  //reassign image/video related variables according to mediaType.
                  if (collegeCpeInteractionDetailsWithMedia.getMediaType() != null && collegeCpeInteractionDetailsWithMedia.getMediaType().equalsIgnoreCase("VIDEO")) {
                    collegeCpeInteractionDetailsWithMedia.setIsMediaAttached("TRUE");
                    collegeCpeInteractionDetailsWithMedia.setVideoId(collegeCpeInteractionDetailsWithMedia.getMediaId());
                    collegeCpeInteractionDetailsWithMedia.setVideoPathFlv(collegeCpeInteractionDetailsWithMedia.getMediaPath());
                    if (collegeCpeInteractionDetailsWithMedia.getVideoPathFlv() != null && collegeCpeInteractionDetailsWithMedia.getVideoPathFlv().trim().length() > 0) {
                      collegeCpeInteractionDetailsWithMedia.setVideoPath0001(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "1"));
                      collegeCpeInteractionDetailsWithMedia.setVideoPath0002(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "2"));
                      collegeCpeInteractionDetailsWithMedia.setVideoPath0003(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "3"));
                      collegeCpeInteractionDetailsWithMedia.setVideoPath0004(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "4"));
                    }
                    collegeCpeInteractionDetailsWithMedia.setImageId("FALSE");
                    collegeCpeInteractionDetailsWithMedia.setImagePath("FALSE");
                    collegeCpeInteractionDetailsWithMedia.setImagePathThumb("FALSE");
                  } else if (collegeCpeInteractionDetailsWithMedia.getMediaType() != null && collegeCpeInteractionDetailsWithMedia.getMediaType().equalsIgnoreCase("PICTURE")) {
                    collegeCpeInteractionDetailsWithMedia.setIsMediaAttached("TRUE");
                    collegeCpeInteractionDetailsWithMedia.setImageId(collegeCpeInteractionDetailsWithMedia.getMediaId());
                    collegeCpeInteractionDetailsWithMedia.setImagePath(collegeCpeInteractionDetailsWithMedia.getMediaPath());
                    collegeCpeInteractionDetailsWithMedia.setImagePathThumb(collegeCpeInteractionDetailsWithMedia.getMediaPathThumb());
                    if (collegeCpeInteractionDetailsWithMedia.getImagePathThumb() == null || collegeCpeInteractionDetailsWithMedia.getVideoPathFlv().trim().length() == 0) {
                      collegeCpeInteractionDetailsWithMedia.setImagePathThumb(collegeCpeInteractionDetailsWithMedia.getImagePath());
                    }
                    collegeCpeInteractionDetailsWithMedia.setVideoId("FALSE");
                    collegeCpeInteractionDetailsWithMedia.setVideoPathFlv("FALSE");
                    collegeCpeInteractionDetailsWithMedia.setVideoPath0001("FALSE");
                    collegeCpeInteractionDetailsWithMedia.setVideoPath0002("FALSE");
                    collegeCpeInteractionDetailsWithMedia.setVideoPath0003("FALSE");
                    collegeCpeInteractionDetailsWithMedia.setVideoPath0004("FALSE");
                  }
                  similarCoursesBean.setCollegeCpeInteractionDetailsWithMedia(collegeCpeInteractionDetailsWithMedia);
                  //similarCoursesBean.setCollegeCpeInteractionDetailsWithMedia(extractInteractionDetailsFromRS(enquiryRS));
                }
              }
            } catch (Exception e) {
              e.printStackTrace();
            } finally {
              if (enquiryRS != null) {
                enquiryRS.close();
                datamodel.closeCursor(enquiryRS);
              }
            }
            //
            listOfSimilarCourses.add(similarCoursesBean);
            //collegeCpeInteractionDetailsWithMedia = null;
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        //nullify unused objects
        datamodel.closeCursor(similarCoursesRS);
        if (enquiryRS != null) {
          try {
            enquiryRS.close();
            datamodel.closeCursor(enquiryRS);
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
        datamodel = null;
        similarCoursesRS = null;
        tmpUrl = null;
        parameters.clear();
        parameters = null;
      }
    }
    return listOfSimilarCourses;
  }

  public CollegeCpeInteractionDetailsWithMediaBean extractInteractionDetailsFromRS(ResultSet profileDetailsRS) {
    CollegeCpeInteractionDetailsWithMediaBean collegeCpeInteractionDetailsWithMedia = null;
    if (profileDetailsRS != null) {
      String tmpUrl = "";
      try {
        collegeCpeInteractionDetailsWithMedia = new CollegeCpeInteractionDetailsWithMediaBean();
        //        
        collegeCpeInteractionDetailsWithMedia.setOrderItemId(profileDetailsRS.getString("ORDER_ITEM_ID"));
        collegeCpeInteractionDetailsWithMedia.setSubOrderItemId(profileDetailsRS.getString("SUBORDER_ITEM_ID"));
        collegeCpeInteractionDetailsWithMedia.setMyhcProfileId(profileDetailsRS.getString("MYHC_PROFILE_ID"));
        collegeCpeInteractionDetailsWithMedia.setProfileType(profileDetailsRS.getString("PROFILE_TYPE"));
        collegeCpeInteractionDetailsWithMedia.setAdvertName(profileDetailsRS.getString("ADVERT_NAME"));
        collegeCpeInteractionDetailsWithMedia.setCpeQualificationNetworkId(profileDetailsRS.getString("NETWORK_ID"));
        //
        collegeCpeInteractionDetailsWithMedia.setSubOrderEmail(profileDetailsRS.getString("EMAIL"));
        collegeCpeInteractionDetailsWithMedia.setSubOrderProspectus(profileDetailsRS.getString("PROSPECTUS"));
        //
        tmpUrl = profileDetailsRS.getString("WEBSITE");
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        collegeCpeInteractionDetailsWithMedia.setSubOrderWebsite(tmpUrl);
        //
        tmpUrl = profileDetailsRS.getString("EMAIL_WEBFORM");
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        collegeCpeInteractionDetailsWithMedia.setSubOrderEmailWebform(tmpUrl);
        //
        tmpUrl = profileDetailsRS.getString("PROSPECTUS_WEBFORM");
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        collegeCpeInteractionDetailsWithMedia.setSubOrderProspectusWebform(tmpUrl);
        // media details from db
        collegeCpeInteractionDetailsWithMedia.setMediaId(profileDetailsRS.getString("MEDIA_ID"));
        collegeCpeInteractionDetailsWithMedia.setMediaType(profileDetailsRS.getString("MEDIA_TYPE"));
        collegeCpeInteractionDetailsWithMedia.setMediaPath(profileDetailsRS.getString("MEDIA_PATH"));
        collegeCpeInteractionDetailsWithMedia.setMediaPathThumb(profileDetailsRS.getString("THUMB_MEDIA_PATH"));
        //customized media details
        //reassign image/video related variables according to mediaType.
        if (collegeCpeInteractionDetailsWithMedia.getMediaType() != null && collegeCpeInteractionDetailsWithMedia.getMediaType().equalsIgnoreCase("VIDEO")) {
          collegeCpeInteractionDetailsWithMedia.setIsMediaAttached("TRUE");
          collegeCpeInteractionDetailsWithMedia.setVideoId(collegeCpeInteractionDetailsWithMedia.getMediaId());
          collegeCpeInteractionDetailsWithMedia.setVideoPathFlv(collegeCpeInteractionDetailsWithMedia.getMediaPath());
          if (collegeCpeInteractionDetailsWithMedia.getVideoPathFlv() != null && collegeCpeInteractionDetailsWithMedia.getVideoPathFlv().trim().length() > 0) {
            collegeCpeInteractionDetailsWithMedia.setVideoPath0001(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "1"));
            collegeCpeInteractionDetailsWithMedia.setVideoPath0002(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "2"));
            collegeCpeInteractionDetailsWithMedia.setVideoPath0003(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "3"));
            collegeCpeInteractionDetailsWithMedia.setVideoPath0004(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "4"));
          }
          collegeCpeInteractionDetailsWithMedia.setImageId("FALSE");
          collegeCpeInteractionDetailsWithMedia.setImagePath("FALSE");
          collegeCpeInteractionDetailsWithMedia.setImagePathThumb("FALSE");
        } else if (collegeCpeInteractionDetailsWithMedia.getMediaType() != null && collegeCpeInteractionDetailsWithMedia.getMediaType().equalsIgnoreCase("PICTURE")) {
          collegeCpeInteractionDetailsWithMedia.setIsMediaAttached("TRUE");
          collegeCpeInteractionDetailsWithMedia.setImageId(collegeCpeInteractionDetailsWithMedia.getMediaId());
          collegeCpeInteractionDetailsWithMedia.setImagePath(collegeCpeInteractionDetailsWithMedia.getMediaPath());
          collegeCpeInteractionDetailsWithMedia.setImagePathThumb(collegeCpeInteractionDetailsWithMedia.getMediaPathThumb());
          if (collegeCpeInteractionDetailsWithMedia.getImagePathThumb() == null || collegeCpeInteractionDetailsWithMedia.getVideoPathFlv().trim().length() == 0) {
            collegeCpeInteractionDetailsWithMedia.setImagePathThumb(collegeCpeInteractionDetailsWithMedia.getImagePath());
          }
          collegeCpeInteractionDetailsWithMedia.setVideoId("FALSE");
          collegeCpeInteractionDetailsWithMedia.setVideoPathFlv("FALSE");
          collegeCpeInteractionDetailsWithMedia.setVideoPath0001("FALSE");
          collegeCpeInteractionDetailsWithMedia.setVideoPath0002("FALSE");
          collegeCpeInteractionDetailsWithMedia.setVideoPath0003("FALSE");
          collegeCpeInteractionDetailsWithMedia.setVideoPath0004("FALSE");
        }
      } catch (SQLException e) {
        e.printStackTrace();
      } finally {
        tmpUrl = null;
      }
    }
    return collegeCpeInteractionDetailsWithMedia;
  }

  /**
   * will check the given college is advertiser or nor by refering "w_dom_search_boosting"
   *
   * @param collegeId
   * @return isAdvertiser
   * @since wu_2.1.5__2010-Dec-07
   * @author Mohamed Syed
   */
  public String isAdvertiser(String collegeId, String cpeQualificationNetworkId) {
    String isAdvertiser = "FALSE";
    if (collegeId != null & collegeId.trim().length() > 0) {
      collegeId = collegeId.trim();
      DataModel dataModel = new DataModel();
      Vector parameters = new Vector();
      parameters.add(collegeId);
      parameters.add(cpeQualificationNetworkId);
      try {
        isAdvertiser = dataModel.getString("WU_CPE_ADVERT_PKG.IS_ADVERTISER_FN", parameters);
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        dataModel = null;
        parameters.clear();
        parameters = null;
      }
    }
    return isAdvertiser;
  }

  /**
   * will retun the LIVE status about the particular subOrderItemId
   *
   * @param subOrderItemId
   * @return isLiveSuborderItem
   *
   * @since wu315_20110726
   * @author Mohamed Syed.
   *
   */
  public String isLiveSuborderItem(String subOrderItemId) {
    if (new CommonValidator().isBlankOrNull(subOrderItemId)) {
      return "FALSE";
    }
    String isLiveSuborderItem = "FLASE";
    DataModel dataModel = new DataModel();
    Vector parameters = new Vector();
    parameters.add(subOrderItemId);
    try {
      isLiveSuborderItem = dataModel.getString("WU_CPE_ADVERT_PKG.IS_LIVE_SUBORDER_FN", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel = null;
      parameters.clear();
      parameters = null;
    }
    return isLiveSuborderItem;
  }

  /**
   * will retun the LIVE status about the particular profile based on below parameters by refering hot_admin.w_order_item
   *
   * @param collegeId
   * @param myhcProfileId
   * @param profileId
   * @param cpeQualificationNetworkId
   * @return isThisLiveProfile
   *
   * @since wu_3.0.2__2011-Feb-08
   * @author Mohamed Syed.
   *
   */
  public String isThisLiveProfile(String collegeId, String myhcProfileId, String profileId, String cpeQualificationNetworkId) {
    String isThisLiveProfile = "FLASE";
    if (myhcProfileId != null & myhcProfileId.trim().length() > 0) {
      DataModel dataModel = new DataModel();
      Vector parameters = new Vector();
      parameters.add(collegeId);
      parameters.add(myhcProfileId);
      parameters.add(profileId);
      parameters.add(cpeQualificationNetworkId);
      try {
        isThisLiveProfile = dataModel.getString("WU_CPE_ADVERT_PKG.IS_PROFILE_LIVE_FN", parameters);
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        dataModel = null;
        parameters.clear();
        parameters = null;
      }
    }
    return isThisLiveProfile;
  }

  /**
   * to get the wrost performers in-terms of CPE underdelivery
   *
   * @param keyword
   * @param categoryCode
   * @param qualification
   * @param collegeId
   * @return listOfCpeWorstPerformers
   *
   * @since wu_2.1.5__2010-Dec-07
   * @author Mohamed Syed.
   */
  public ArrayList getCpeWorstPerformers(String keyword, String categoryCode, String qualification, String collegeId, String callFrom) {
    ArrayList listOfCpeWorstPerformers = new ArrayList();
    DataModel dataModel = new DataModel();
    ResultSet cpeWorstPerformersRS = null;
    ResultSet cpeWorstPerformersEnquiryDetailsRS = null;
    CpeWorstPerformersBean cpeWorstPerformersBean = null;
    Vector parameters = new Vector();
    parameters.add(keyword);
    parameters.add(qualification);
    parameters.add(categoryCode);
    parameters.add(collegeId);
    parameters.add(callFrom);
    try {
      cpeWorstPerformersRS = dataModel.getResultSet("WU_CPE_ADVERT_PKG.GET_CPE_WORST_PERFORMER_FN", parameters);
      while (cpeWorstPerformersRS.next()) {
        cpeWorstPerformersBean = new CpeWorstPerformersBean();
        cpeWorstPerformersBean.setCollegeId(cpeWorstPerformersRS.getString("COLLEGE_ID"));
        cpeWorstPerformersBean.setCollegeName(cpeWorstPerformersRS.getString("COLLEGE_NAME"));
        cpeWorstPerformersBean.setCollegeNameDisplay(cpeWorstPerformersRS.getString("COLLEGE_NAME_DISPLAY"));
        cpeWorstPerformersBean.setCollegeLocation(cpeWorstPerformersRS.getString("COLLEGE_LOCATION"));
        cpeWorstPerformersBean.setUniHomeUrl(new SeoUrls().construnctUniHomeURL(cpeWorstPerformersBean.getCollegeId(), cpeWorstPerformersBean.getCollegeName()));
        //Changed  by Sekhar K for wu_320
        if(callFrom.equalsIgnoreCase("OPEN_DAYS")){
            cpeWorstPerformersBean.setOpenDayCount(cpeWorstPerformersRS.getString("OPENDAY_COUNT"));
            cpeWorstPerformersBean.setCollegeLogo(cpeWorstPerformersRS.getString("COLLEGE_LOGO"));
        }
        if(callFrom.equalsIgnoreCase("LOCATION_HOME")){
            cpeWorstPerformersBean.setLdcs1Code(cpeWorstPerformersRS.getString("LDCS1_CODE"));
            cpeWorstPerformersBean.setLdcs1Description(cpeWorstPerformersRS.getString("LDCS1_DESCRIPTION"));
            cpeWorstPerformersBean.setHashRemovedLdcs1Description(cpeWorstPerformersRS.getString("LDCS1_DESCRIPTION"));
            if (cpeWorstPerformersBean.getHashRemovedLdcs1Description() != null && cpeWorstPerformersBean.getHashRemovedLdcs1Description().trim().length() > 0) {
              cpeWorstPerformersBean.setHashRemovedLdcs1Description(cpeWorstPerformersBean.getHashRemovedLdcs1Description().replaceAll("#", "-"));
            }
            cpeWorstPerformersBean.setQualificationCode(cpeWorstPerformersRS.getString("QUALIFICATION_CODE"));
            cpeWorstPerformersBean.setQualificationDescription(cpeWorstPerformersRS.getString("QUALIFICATION_DESCRIPTION"));
            cpeWorstPerformersEnquiryDetailsRS = (ResultSet)cpeWorstPerformersRS.getObject("ENQUIRY_DETAILS");
            try {
              if (cpeWorstPerformersEnquiryDetailsRS != null) {
                while (cpeWorstPerformersEnquiryDetailsRS.next()) {
                  cpeWorstPerformersBean.setOrderItemId(cpeWorstPerformersEnquiryDetailsRS.getString("ORDER_ITEM_ID"));
                  cpeWorstPerformersBean.setSubOrderItemId(cpeWorstPerformersEnquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
                  cpeWorstPerformersBean.setMyhcProfileId(cpeWorstPerformersEnquiryDetailsRS.getString("MYHC_PROFILE_ID"));
                  cpeWorstPerformersBean.setProfileType(cpeWorstPerformersEnquiryDetailsRS.getString("PROFILE_TYPE"));
                  cpeWorstPerformersBean.setProfileAdvertName(cpeWorstPerformersEnquiryDetailsRS.getString("ADVERT_NAME"));
                  cpeWorstPerformersBean.setSubOrderEmail(cpeWorstPerformersEnquiryDetailsRS.getString("EMAIL"));
                  cpeWorstPerformersBean.setSubOrderProspectus(cpeWorstPerformersEnquiryDetailsRS.getString("PROSPECTUS"));
                  cpeWorstPerformersBean.setCpeQualificationNetworkId(cpeWorstPerformersEnquiryDetailsRS.getString("NETWORK_ID"));
                  String tmpUrl = cpeWorstPerformersEnquiryDetailsRS.getString("WEBSITE");
                  if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                      tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
                  } else {
                    tmpUrl = "";
                  }
                  cpeWorstPerformersBean.setSubOrderWebsite(tmpUrl);
                  tmpUrl = cpeWorstPerformersEnquiryDetailsRS.getString("EMAIL_WEBFORM");
                  if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                      tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
                  } else {
                    tmpUrl = "";
                  }
                  cpeWorstPerformersBean.setSubOrderEmailWebform(tmpUrl);
                  tmpUrl = cpeWorstPerformersEnquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
                  if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                      tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
                  } else {
                    tmpUrl = "";
                  }
                  cpeWorstPerformersBean.setSubOrderProspectusWebform(tmpUrl);
                  //collectin media informations
                  cpeWorstPerformersBean.setMediaId(cpeWorstPerformersEnquiryDetailsRS.getString("MEDIA_ID"));
                  cpeWorstPerformersBean.setMediaPath(cpeWorstPerformersEnquiryDetailsRS.getString("MEDIA_PATH"));
                  cpeWorstPerformersBean.setMediaType(cpeWorstPerformersEnquiryDetailsRS.getString("MEDIA_TYPE"));
                  cpeWorstPerformersBean.setMediaThumbPath(cpeWorstPerformersEnquiryDetailsRS.getString("THUMB_MEDIA_PATH"));
                  if (cpeWorstPerformersBean.getMediaId() != null && cpeWorstPerformersBean.getMediaPath() != null && cpeWorstPerformersBean.getMediaType() != null) {
                    if (cpeWorstPerformersBean.getMediaType().equalsIgnoreCase("video")) {
                      //cpeWorstPerformersBean.setMediaThumbPath(new GlobalFunction().videoThumbnailFormatChange(cpeWorstPerformersBean.getMediaPath(), "4"));
                      String thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(cpeWorstPerformersBean.getMediaId(), "4"), 0); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path.
                      cpeWorstPerformersBean.setMediaThumbPath(thumbnailPath);
                    } else if (cpeWorstPerformersBean.getMediaType().equalsIgnoreCase("picture")) {
                      //                  if(cpeWorstPerformersBean.getMediaPath().indexOf("http://") > -1 ){
                      //                    cpeWorstPerformersBean.setMediaPath(cpeWorstPerformersBean.getMediaPath());
                      //                  } else {
                      //                    cpeWorstPerformersBean.setMediaPath(new CommonFunction().getWUSysVarValue("IMAGE_DISPLAY_PATH") + "college/image/" + cpeWorstPerformersBean.getCollegeId() + "/" + cpeWorstPerformersBean.getMediaPath());
                      //                  }
                    } else {
                      cpeWorstPerformersBean.setMediaPath("/img/whatuni/unihome_default.png");
                    }
                  } else {
                    cpeWorstPerformersBean.setMediaId("-999");
                    cpeWorstPerformersBean.setMediaPath("/img/whatuni/unihome_default.png");
                    cpeWorstPerformersBean.setMediaType("PICTURE");
                  }
                  if (cpeWorstPerformersBean.getMediaThumbPath() == null || cpeWorstPerformersBean.getMediaThumbPath().trim().length() == 0) {
                    cpeWorstPerformersBean.setMediaThumbPath(cpeWorstPerformersBean.getMediaPath());
                  }
                }
              }
            } catch (Exception e) {
              e.printStackTrace();
            } finally {
              try {
                if (cpeWorstPerformersEnquiryDetailsRS != null) {
                  cpeWorstPerformersEnquiryDetailsRS.close();
                }
              } catch (Exception e) {
                e.printStackTrace();
              }
            }
        }
         listOfCpeWorstPerformers.add(cpeWorstPerformersBean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(cpeWorstPerformersRS);
      try {
        if (cpeWorstPerformersEnquiryDetailsRS != null) {
          cpeWorstPerformersEnquiryDetailsRS.close();
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return listOfCpeWorstPerformers;
  } //getCpeWorstPerformers

  /**
    * @since wu_2.1.3__2010-Nov-10
    * @author Mohamed Syed
    * @param courseId
    * @param request
    * @return haveYouThoughtAboutList
    *
    * This will give the haveYouThoughtAboutList for a course.
    * LOGIC: Have you thought about? Pod on non advertiser’s course details page.
    * 1)        Building CD page "Have you thought about?" URLs
    *   i.      Fetching 3 relevant colleges:
    *     1.    Filtering by LDCS1 by referring hot_copy.co_course.LDCS1.  Any wu institution running the same course should be included in the next levels of filtering
    *     2.    Filtering Advertiser by joining to the boosting table to ensure it is live advertising
    *     3.    Filtering UCAS points by referring wu_college_unistats – It will expand by 20 points a maximum of 5 times
    *   ii.     Ordering 3 colleges:
    *     1.    VD
    */
  public ArrayList fetchHaveYouThoughtAboutList(String courseId, HttpServletRequest request) {
    ArrayList haveYouThoughtAboutList = new ArrayList();
    HaveYouThoughtAboutBean haveYouThoughtAboutBean = null;
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    Vector parameters = new Vector();
    parameters.add(courseId);
    try {
      resultset = dataModel.getResultSet("WU_CPE_ADVERT_PKG.GET_HAVE_THOUGHT_ABOUT_FN", parameters);
      while (resultset.next()) {
        haveYouThoughtAboutBean = new HaveYouThoughtAboutBean();
        haveYouThoughtAboutBean.setCollegeId(resultset.getString("COLLEGE_ID"));
        haveYouThoughtAboutBean.setCollegeName(resultset.getString("COLLEGE_NAME"));
        haveYouThoughtAboutBean.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
        haveYouThoughtAboutBean.setLdcs1Code(resultset.getString("LDCS1_CODE"));
        haveYouThoughtAboutBean.setLdcs1Description(resultset.getString("LDCS1_DESCRIPTION"));
        if (haveYouThoughtAboutBean.getLdcs1Description() != null && haveYouThoughtAboutBean.getLdcs1Description().trim().length() > 0) {
          haveYouThoughtAboutBean.setHashRemovedLdcs1Description(haveYouThoughtAboutBean.getLdcs1Description().replaceAll("#", "-"));
        }
        haveYouThoughtAboutBean.setQualificationCode(resultset.getString("QUALIFICATION_CODE"));
        haveYouThoughtAboutBean.setQualificationDescription(resultset.getString("QUALIFICATION_DESCRIPTION"));
        haveYouThoughtAboutBean.setUcasPoint(resultset.getString("UCAS_POINT"));
        haveYouThoughtAboutBean.setCombinedValueDistance(resultset.getString("CVD"));
        haveYouThoughtAboutBean.setTenencyBoostiingValue(resultset.getString("TBV"));
        haveYouThoughtAboutList.add(haveYouThoughtAboutBean);
        request.setAttribute("subjectName", haveYouThoughtAboutBean.getLdcs1Description());
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
   
    return haveYouThoughtAboutList;
  } //end of fetchHaveYouThoughtAboutList()

  /**
   * fuction will return all interactivity related details
   *
   * @param id        - any sequence id (like profileId, subOrderItemId, orderItemId and etc)
   * @param whichId   - value can be "profile_id" or "sub_order_item_id", "order_item_id"
   * @param request
   */
  public ArrayList getInteractionDetailsById(String id, String whichId, HttpServletRequest request) {
    String isAdvertiser = "FALSE";
    ArrayList listOfCollegeInteractionDetailsWithMedia = new ArrayList();
    if (id == null || id.trim().length() == 0 || whichId == null || whichId.trim().length() == 0) {
      request.setAttribute("isAdvertiser", isAdvertiser);
      return listOfCollegeInteractionDetailsWithMedia;
    }
    //
    DataModel dataModel = new DataModel();
    ResultSet profileDetailsRS = null;
    String tmpUrl = "";
    CollegeCpeInteractionDetailsWithMediaBean collegeCpeInteractionDetailsWithMedia = null;
    Vector parameters = new Vector();
    parameters.add(id);
    parameters.add(whichId);
    //
    try {
      profileDetailsRS = dataModel.getResultSet("WU_CPE_ADVERT_PKG.GET_INTERACTIVITY_INFO_FN", parameters);
      if (profileDetailsRS != null) {
        while (profileDetailsRS.next()) {
          collegeCpeInteractionDetailsWithMedia = new CollegeCpeInteractionDetailsWithMediaBean();
          collegeCpeInteractionDetailsWithMedia.setOrderItemId(profileDetailsRS.getString("ORDER_ITEM_ID"));
          collegeCpeInteractionDetailsWithMedia.setSubOrderItemId(profileDetailsRS.getString("SUBORDER_ITEM_ID"));
          collegeCpeInteractionDetailsWithMedia.setMyhcProfileId(profileDetailsRS.getString("MYHC_PROFILE_ID"));
          //collegeCpeInteractionDetailsWithMedia.setProfileId(profileDetailsRS.getString("PROFILE_ID"));          
          collegeCpeInteractionDetailsWithMedia.setProfileType(profileDetailsRS.getString("PROFILE_TYPE"));
          collegeCpeInteractionDetailsWithMedia.setAdvertName(profileDetailsRS.getString("ADVERT_NAME"));
          collegeCpeInteractionDetailsWithMedia.setCpeQualificationNetworkId(profileDetailsRS.getString("NETWORK_ID"));
          //
          collegeCpeInteractionDetailsWithMedia.setSubOrderEmail(profileDetailsRS.getString("EMAIL"));
          collegeCpeInteractionDetailsWithMedia.setSubOrderProspectus(profileDetailsRS.getString("PROSPECTUS"));
          //
          tmpUrl = profileDetailsRS.getString("WEBSITE");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          collegeCpeInteractionDetailsWithMedia.setSubOrderWebsite(tmpUrl);
          //
          tmpUrl = profileDetailsRS.getString("EMAIL_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          collegeCpeInteractionDetailsWithMedia.setSubOrderEmailWebform(tmpUrl);
          //
          tmpUrl = profileDetailsRS.getString("PROSPECTUS_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          collegeCpeInteractionDetailsWithMedia.setSubOrderProspectusWebform(tmpUrl);
          // media details from db
          collegeCpeInteractionDetailsWithMedia.setMediaId(profileDetailsRS.getString("MEDIA_ID"));
          collegeCpeInteractionDetailsWithMedia.setMediaType(profileDetailsRS.getString("MEDIA_TYPE"));
          collegeCpeInteractionDetailsWithMedia.setMediaPath(profileDetailsRS.getString("MEDIA_PATH"));
          collegeCpeInteractionDetailsWithMedia.setMediaPathThumb(profileDetailsRS.getString("THUMB_MEDIA_PATH"));
          //customized media details
          //reassign image/video related variables according to mediaType.
          if (collegeCpeInteractionDetailsWithMedia.getMediaType() != null && collegeCpeInteractionDetailsWithMedia.getMediaType().equalsIgnoreCase("VIDEO")) {
            collegeCpeInteractionDetailsWithMedia.setIsMediaAttached("TRUE");
            collegeCpeInteractionDetailsWithMedia.setVideoId(collegeCpeInteractionDetailsWithMedia.getMediaId());
            collegeCpeInteractionDetailsWithMedia.setVideoPathFlv(collegeCpeInteractionDetailsWithMedia.getMediaPath());
            if (collegeCpeInteractionDetailsWithMedia.getVideoPathFlv() != null && collegeCpeInteractionDetailsWithMedia.getVideoPathFlv().trim().length() > 0) {
              //collegeCpeInteractionDetailsWithMedia.setVideoPath0001(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "1"));
              //collegeCpeInteractionDetailsWithMedia.setVideoPath0002(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "2"));
              //collegeCpeInteractionDetailsWithMedia.setVideoPath0003(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "3"));
              //collegeCpeInteractionDetailsWithMedia.setVideoPath0004(new GlobalFunction().videoThumbnailFormatChange(collegeCpeInteractionDetailsWithMedia.getVideoPathFlv(), "4"));
              
              collegeCpeInteractionDetailsWithMedia.setVideoPath0001(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(collegeCpeInteractionDetailsWithMedia.getMediaId(), "1"), 0));
              collegeCpeInteractionDetailsWithMedia.setVideoPath0002(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(collegeCpeInteractionDetailsWithMedia.getMediaId(), "2"), 0));
              collegeCpeInteractionDetailsWithMedia.setVideoPath0003(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(collegeCpeInteractionDetailsWithMedia.getMediaId(), "3"), 0));
              collegeCpeInteractionDetailsWithMedia.setVideoPath0004(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(collegeCpeInteractionDetailsWithMedia.getMediaId(), "4"), 0));

            }
            collegeCpeInteractionDetailsWithMedia.setImageId("FALSE");
            collegeCpeInteractionDetailsWithMedia.setImagePath("FALSE");
            collegeCpeInteractionDetailsWithMedia.setImagePathThumb("FALSE");
          } else if (collegeCpeInteractionDetailsWithMedia.getMediaType() != null && collegeCpeInteractionDetailsWithMedia.getMediaType().equalsIgnoreCase("PICTURE")) {
            collegeCpeInteractionDetailsWithMedia.setIsMediaAttached("TRUE");
            collegeCpeInteractionDetailsWithMedia.setImageId(collegeCpeInteractionDetailsWithMedia.getMediaId());
            collegeCpeInteractionDetailsWithMedia.setImagePath(collegeCpeInteractionDetailsWithMedia.getMediaPath());
            collegeCpeInteractionDetailsWithMedia.setImagePathThumb(collegeCpeInteractionDetailsWithMedia.getMediaPathThumb());
            if (collegeCpeInteractionDetailsWithMedia.getImagePathThumb() == null || collegeCpeInteractionDetailsWithMedia.getVideoPathFlv().trim().length() == 0) {
              collegeCpeInteractionDetailsWithMedia.setImagePathThumb(collegeCpeInteractionDetailsWithMedia.getImagePath());
            }
            collegeCpeInteractionDetailsWithMedia.setVideoId("FALSE");
            collegeCpeInteractionDetailsWithMedia.setVideoPathFlv("FALSE");
            collegeCpeInteractionDetailsWithMedia.setVideoPath0001("FALSE");
            collegeCpeInteractionDetailsWithMedia.setVideoPath0002("FALSE");
            collegeCpeInteractionDetailsWithMedia.setVideoPath0003("FALSE");
            collegeCpeInteractionDetailsWithMedia.setVideoPath0004("FALSE");
          }
          //
          isAdvertiser = collegeCpeInteractionDetailsWithMedia.getSubOrderItemId() != null && collegeCpeInteractionDetailsWithMedia.getSubOrderItemId().trim().length() > 0 ? "TRUE" : "FALSE";
          request.setAttribute("isAdvertiser", isAdvertiser);
          request.setAttribute("subOrderItemId", collegeCpeInteractionDetailsWithMedia.getSubOrderItemId());
          request.setAttribute("subOrderWebsite", collegeCpeInteractionDetailsWithMedia.getSubOrderWebsite());
          request.setAttribute("subOrderEmailWebform", collegeCpeInteractionDetailsWithMedia.getSubOrderEmailWebform());
          request.setAttribute("subOrderProspectusWebform", collegeCpeInteractionDetailsWithMedia.getSubOrderProspectusWebform());
          //
          listOfCollegeInteractionDetailsWithMedia.add(collegeCpeInteractionDetailsWithMedia);
          //collegeCpeInteractionDetailsWithMedia = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //nullify unused objects
      dataModel.closeCursor(profileDetailsRS);
      dataModel = null;
      profileDetailsRS = null;
      tmpUrl = null;
      parameters.clear();
      parameters = null;
      //collegeCpeInteractionDetailsWithMedia = null;
    }
    return listOfCollegeInteractionDetailsWithMedia;
  } // end of getInteractionDetailsById()

  /**
   *
   * will give the details about the uni's IP details alone based on cpeQualificationNetworkId
   *
   * @since wu305_20110322
   * @author Mohamed Syed
   *
   * @param collegeId
   * @param cpeQualificationNetworkId
   * @param request
   * @return
   */
  public ArrayList getCollegeInteractionDetailsWithMedia(String collegeId, String cpeQualificationNetworkId, HttpServletRequest request) {
    String isAdvertiser = "FALSE";
    ArrayList listOfCollegeInteractionDetailsWithMedia = new ArrayList();
    if (collegeId == null || collegeId.trim().length() == 0) {
      request.setAttribute("isAdvertiser", isAdvertiser);
      return listOfCollegeInteractionDetailsWithMedia;
    }
    //
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = "2";
    }
    //
    DataModel dataModel = new DataModel();
    ResultSet profileDetailsRS = null;
    String tmpUrl = "";
    CollegeCpeInteractionDetailsWithMediaBean collegeCpeInteractionDetailsWithMedia = null;
    Vector parameters = new Vector();
    parameters.add(collegeId);
    parameters.add(cpeQualificationNetworkId);
    //
    try {
      profileDetailsRS = dataModel.getResultSet("WU_CPE_ADVERT_PKG.GET_IP_ENQUIRY_WITH_MEDIA_FN", parameters);
      if (profileDetailsRS != null) {
        while (profileDetailsRS.next()) {
          collegeCpeInteractionDetailsWithMedia = new CollegeCpeInteractionDetailsWithMediaBean();
          collegeCpeInteractionDetailsWithMedia.setOrderItemId(profileDetailsRS.getString("ORDER_ITEM_ID"));
          collegeCpeInteractionDetailsWithMedia.setSubOrderItemId(profileDetailsRS.getString("SUBORDER_ITEM_ID"));
          collegeCpeInteractionDetailsWithMedia.setMyhcProfileId(profileDetailsRS.getString("MYHC_PROFILE_ID"));
          //collegeCpeInteractionDetailsWithMedia.setProfileId(profileDetailsRS.getString("PROFILE_ID"));          
          collegeCpeInteractionDetailsWithMedia.setProfileType(profileDetailsRS.getString("PROFILE_TYPE"));
          collegeCpeInteractionDetailsWithMedia.setAdvertName(profileDetailsRS.getString("ADVERT_NAME"));
          collegeCpeInteractionDetailsWithMedia.setCpeQualificationNetworkId(profileDetailsRS.getString("NETWORK_ID"));
          //
          collegeCpeInteractionDetailsWithMedia.setSubOrderEmail(profileDetailsRS.getString("EMAIL"));
          collegeCpeInteractionDetailsWithMedia.setSubOrderProspectus(profileDetailsRS.getString("PROSPECTUS"));
          //
          tmpUrl = profileDetailsRS.getString("WEBSITE");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          collegeCpeInteractionDetailsWithMedia.setSubOrderWebsite(tmpUrl);
          //
          tmpUrl = profileDetailsRS.getString("EMAIL_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          collegeCpeInteractionDetailsWithMedia.setSubOrderEmailWebform(tmpUrl);
          //
          tmpUrl = profileDetailsRS.getString("PROSPECTUS_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          collegeCpeInteractionDetailsWithMedia.setSubOrderProspectusWebform(tmpUrl);
          // media details from db
          collegeCpeInteractionDetailsWithMedia.setMediaId(profileDetailsRS.getString("MEDIA_ID"));
          collegeCpeInteractionDetailsWithMedia.setMediaType(profileDetailsRS.getString("MEDIA_TYPE"));
          collegeCpeInteractionDetailsWithMedia.setMediaPath(profileDetailsRS.getString("MEDIA_PATH"));
          collegeCpeInteractionDetailsWithMedia.setMediaPathThumb(profileDetailsRS.getString("THUMB_MEDIA_PATH"));
          //customized media details
          //reassign image/video related variables according to mediaType.
          if (collegeCpeInteractionDetailsWithMedia.getMediaType() != null && collegeCpeInteractionDetailsWithMedia.getMediaType().equalsIgnoreCase("VIDEO")) {
            collegeCpeInteractionDetailsWithMedia.setIsMediaAttached("TRUE");
            collegeCpeInteractionDetailsWithMedia.setVideoId(collegeCpeInteractionDetailsWithMedia.getMediaId());
            collegeCpeInteractionDetailsWithMedia.setVideoPathFlv(collegeCpeInteractionDetailsWithMedia.getMediaPath());
            if (collegeCpeInteractionDetailsWithMedia.getVideoPathFlv() != null && collegeCpeInteractionDetailsWithMedia.getVideoPathFlv().trim().length() > 0) {
              collegeCpeInteractionDetailsWithMedia.setVideoPath0001(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(collegeCpeInteractionDetailsWithMedia.getMediaId(), "1"), 0));
              collegeCpeInteractionDetailsWithMedia.setVideoPath0002(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(collegeCpeInteractionDetailsWithMedia.getMediaId(), "2"), 0));
              collegeCpeInteractionDetailsWithMedia.setVideoPath0003(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(collegeCpeInteractionDetailsWithMedia.getMediaId(), "3"), 0));
              collegeCpeInteractionDetailsWithMedia.setVideoPath0004(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(collegeCpeInteractionDetailsWithMedia.getMediaId(), "4"), 0));
            }
            collegeCpeInteractionDetailsWithMedia.setImageId("FALSE");
            collegeCpeInteractionDetailsWithMedia.setImagePath("FALSE");
            collegeCpeInteractionDetailsWithMedia.setImagePathThumb("FALSE");
          } else if (collegeCpeInteractionDetailsWithMedia.getMediaType() != null && collegeCpeInteractionDetailsWithMedia.getMediaType().equalsIgnoreCase("PICTURE")) {
            collegeCpeInteractionDetailsWithMedia.setIsMediaAttached("TRUE");
            collegeCpeInteractionDetailsWithMedia.setImageId(collegeCpeInteractionDetailsWithMedia.getMediaId());
            collegeCpeInteractionDetailsWithMedia.setImagePath(collegeCpeInteractionDetailsWithMedia.getMediaPath());
            collegeCpeInteractionDetailsWithMedia.setImagePathThumb(collegeCpeInteractionDetailsWithMedia.getMediaPathThumb());
            if (collegeCpeInteractionDetailsWithMedia.getImagePathThumb() == null || collegeCpeInteractionDetailsWithMedia.getVideoPathFlv().trim().length() == 0) {
              collegeCpeInteractionDetailsWithMedia.setImagePathThumb(collegeCpeInteractionDetailsWithMedia.getImagePath());
            }
            collegeCpeInteractionDetailsWithMedia.setVideoId("FALSE");
            collegeCpeInteractionDetailsWithMedia.setVideoPathFlv("FALSE");
            collegeCpeInteractionDetailsWithMedia.setVideoPath0001("FALSE");
            collegeCpeInteractionDetailsWithMedia.setVideoPath0002("FALSE");
            collegeCpeInteractionDetailsWithMedia.setVideoPath0003("FALSE");
            collegeCpeInteractionDetailsWithMedia.setVideoPath0004("FALSE");
          }
          //
          isAdvertiser = collegeCpeInteractionDetailsWithMedia.getSubOrderItemId() != null && collegeCpeInteractionDetailsWithMedia.getSubOrderItemId().trim().length() > 0 ? "TRUE" : "FALSE";
          request.setAttribute("isAdvertiser", isAdvertiser);
          //request.setAttribute("subOrderItemId", collegeCpeInteractionDetailsWithMedia.getSubOrderItemId());
          //
          listOfCollegeInteractionDetailsWithMedia.add(collegeCpeInteractionDetailsWithMedia);
          //collegeCpeInteractionDetailsWithMedia = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //nullify unused objects
      dataModel.closeCursor(profileDetailsRS);
      dataModel = null;
      profileDetailsRS = null;
      tmpUrl = null;
      parameters.clear();
      parameters = null;
      //collegeCpeInteractionDetailsWithMedia = null;
    }
    return listOfCollegeInteractionDetailsWithMedia;
  } //end of getCollegeInteractionDetailsWithMedia()

  /**
   *
   * @param listOfCollegeInteractionDetailsWithMedia
   * @param request
   * @return
   */
  public String saveSuborderDetailsinRequest(ArrayList listOfCollegeInteractionDetailsWithMedia, HttpServletRequest request) {
    String subOrderItemId = "";
    if (listOfCollegeInteractionDetailsWithMedia != null && listOfCollegeInteractionDetailsWithMedia.size() > 0) {
      CollegeCpeInteractionDetailsWithMediaBean collegeCpeInteractionDetailsWithMedia = null;
      for (int i = 0; i < listOfCollegeInteractionDetailsWithMedia.size(); i++) {
        collegeCpeInteractionDetailsWithMedia = (CollegeCpeInteractionDetailsWithMediaBean)listOfCollegeInteractionDetailsWithMedia.get(i);
        request.setAttribute("orderItemId", collegeCpeInteractionDetailsWithMedia.getOrderItemId());
        request.setAttribute("subOrderItemId", collegeCpeInteractionDetailsWithMedia.getSubOrderItemId());
        request.setAttribute("myhcProfileId", collegeCpeInteractionDetailsWithMedia.getMyhcProfileId());
        request.setAttribute("cpeQualificationNetworkId", collegeCpeInteractionDetailsWithMedia.getCpeQualificationNetworkId());
        request.setAttribute("profileType", collegeCpeInteractionDetailsWithMedia.getProfileType());
        request.setAttribute("subOrderEmail", collegeCpeInteractionDetailsWithMedia.getSubOrderEmail());
        request.setAttribute("subOrderProspectus", collegeCpeInteractionDetailsWithMedia.getSubOrderProspectus());
        request.setAttribute("subOrderWebsite", collegeCpeInteractionDetailsWithMedia.getSubOrderWebsite());
        request.setAttribute("subOrderEmailWebform", collegeCpeInteractionDetailsWithMedia.getSubOrderEmailWebform());
        request.setAttribute("subOrderProspectusWebform", collegeCpeInteractionDetailsWithMedia.getSubOrderProspectusWebform());
      }
    }
    return subOrderItemId;
  }

  /**
   * this fuction will return all interactivity related details
   *
   * @param collegeId
   * @param courseId
   * @param request
   * @param qualification
   */
  public String getProfileDetails(String collegeId, String courseId, HttpServletRequest request, String qualification) {
    if (courseId == null || courseId.trim().length() == 0) {
      return getAdvertDetailsForIpOrSp(collegeId, request, qualification);
    }
    String isAdvertiser = "FALSE";
    String subOrderItemId = "";
    DataModel dataModel = new DataModel();
    ResultSet profileDetailsRS = null;
    qualification = qualification != null && qualification.trim().length() > 0 ? qualification : "M";
    Vector parameters = new Vector();
    parameters.add(collegeId); //collegeId
    parameters.add(""); //boosting text
    parameters.add(courseId); //courseId
    parameters.add(qualification); //qualification
    try {
      profileDetailsRS = dataModel.getResultSet("WU_CPE_ADVERT_PKG.get_profile_details", parameters);
      if (profileDetailsRS != null) {
        while (profileDetailsRS.next()) {
          subOrderItemId = profileDetailsRS.getString("SUBORDER_ITEM_ID");
          isAdvertiser = subOrderItemId != null && subOrderItemId.trim().length() > 0 && !subOrderItemId.equals("0") ? "TRUE" : isAdvertiser;
          request.setAttribute("orderItemId", profileDetailsRS.getString("ORDER_ITEM_ID"));
          request.setAttribute("subOrderItemId", profileDetailsRS.getString("SUBORDER_ITEM_ID"));
          request.setAttribute("subOrderEmail", profileDetailsRS.getString("EMAIL"));
          request.setAttribute("subOrderProspectus", profileDetailsRS.getString("PROSPECTUS"));
          request.setAttribute("myhcProfileId", profileDetailsRS.getString("MYHC_PROFILE_ID"));
          request.setAttribute("profileType", profileDetailsRS.getString("PROFILE_TYPE"));
          request.setAttribute("applyNowFlag", profileDetailsRS.getString("APPLY_NOW_FLAG")); //possible values will be Y/N
          //
          String tmpUrl = profileDetailsRS.getString("WEBSITE");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          request.setAttribute("subOrderWebsite", tmpUrl);
          //
          tmpUrl = profileDetailsRS.getString("EMAIL_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          request.setAttribute("subOrderEmailWebform", tmpUrl);
          //
          tmpUrl = profileDetailsRS.getString("PROSPECTUS_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          request.setAttribute("subOrderProspectusWebform", tmpUrl);
          //
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      dataModel.closeCursor(profileDetailsRS);
    }
    return isAdvertiser;
  } //end of getProfileDetails()

  public String getAdvertDetailsForIpOrSp(String collegeId, HttpServletRequest request, String qualification) {
    String isAdvertiser = "FALSE";
    DataModel dataModel = new DataModel();
    ResultSet profileDetailsRS = null;
    qualification = qualification != null && qualification.trim().length() > 0 ? qualification : "M";
    Vector parameters = new Vector();
    parameters.add(collegeId); //collegeId
    parameters.add(qualification); //qualification
    try {
      profileDetailsRS = dataModel.getResultSet("WU_CPE_ADVERT_PKG.get_ip_and_sp_profile_details", parameters);
      if (profileDetailsRS != null) {
        while (profileDetailsRS.next()) {
          isAdvertiser = profileDetailsRS.getString("ORDER_ITEM_ID") != null && profileDetailsRS.getString("ORDER_ITEM_ID").trim().length() > 0 ? "TRUE" : isAdvertiser;
          request.setAttribute("orderItemId", profileDetailsRS.getString("ORDER_ITEM_ID"));
          request.setAttribute("subOrderItemId", profileDetailsRS.getString("SUBORDER_ITEM_ID"));
          request.setAttribute("subOrderEmail", profileDetailsRS.getString("EMAIL"));
          request.setAttribute("subOrderProspectus", profileDetailsRS.getString("PROSPECTUS"));
          request.setAttribute("myhcProfileId", profileDetailsRS.getString("MYHC_PROFILE_ID"));
          request.setAttribute("profileType", profileDetailsRS.getString("PROFILE_TYPE"));
          String tmpUrl = profileDetailsRS.getString("WEBSITE");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          request.setAttribute("subOrderWebsite", tmpUrl);
          tmpUrl = profileDetailsRS.getString("EMAIL_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          request.setAttribute("subOrderEmailWebform", tmpUrl);
          tmpUrl = profileDetailsRS.getString("PROSPECTUS_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          request.setAttribute("subOrderProspectusWebform", tmpUrl);
          break;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      dataModel.closeCursor(profileDetailsRS);
    }
    return isAdvertiser;
  } //getAdvertDetailsForIpOrSp()

  public void getIpSpProfileDetails(String collegeId, HttpServletRequest request, String qualification,String richProvType, String clearingFlag, String myhcProfileId) { //13_JAN_15 Added rich profile Type argument by Amir for Video Interaction
    DataModel dataModel = new DataModel();
    String richProvProfType = "";
    ResultSet profileDetailsRS = null;
    List ipList = new ArrayList();
    List spList = new ArrayList();
    VideoReviewListBean videobean = new VideoReviewListBean();
    qualification = qualification != null && qualification.trim().length() > 0 ? qualification : "M";    
    if(richProvType!=null && !"".equals(richProvType)){
      richProvProfType = richProvType.trim();
    }              
    Vector parameters = new Vector();
    parameters.add(collegeId); //collegeId
    parameters.add(qualification); //qualification
    parameters.add(richProvProfType); 
    parameters.add(clearingFlag); 
    parameters.add(myhcProfileId);
    try {      
      profileDetailsRS = dataModel.getResultSet("WU_CPE_ADVERT_PKG.get_ip_and_sp_profile_details", parameters);
      if (profileDetailsRS != null) {
        while (profileDetailsRS.next()) {
          String profileType = profileDetailsRS.getString("PROFILE_TYPE");
          if (profileType != null && profileType.trim().length() > 0) {
            if (profileType.equals("IP")) {
              videobean.setOrderItemIdForIp(profileDetailsRS.getString("ORDER_ITEM_ID"));
              videobean.setSubOrderEmailIp(profileDetailsRS.getString("EMAIL"));
              videobean.setSubOrderProspectusIp(profileDetailsRS.getString("PROSPECTUS"));
              videobean.setSubOrderItemIdForIp(profileDetailsRS.getString("SUBORDER_ITEM_ID"));
              videobean.setMyhcProfileIdIp(profileDetailsRS.getString("MYHC_PROFILE_ID"));
              videobean.setProfileTypeIp(profileType);
              videobean.setCpeQualificationNetworkIdforIp(profileDetailsRS.getString("NETWORK_ID"));
              String tmpUrl = profileDetailsRS.getString("WEBSITE");
              if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else {
                tmpUrl = "";
              }
              videobean.setSubOrderWebsiteIp(tmpUrl);
              videobean.setHotlineIp(profileDetailsRS.getString("hotline"));
              tmpUrl = profileDetailsRS.getString("EMAIL_WEBFORM");
              if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else {
                tmpUrl = "";
              }
              videobean.setSubOrderEmailWebformIp(tmpUrl);
              tmpUrl = profileDetailsRS.getString("PROSPECTUS_WEBFORM");
              if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else {
                tmpUrl = "";
              }
              videobean.setSubOrderProspectusWebformIp(tmpUrl);
              ipList.add(videobean);
            } else {
              videobean.setOrderItemIdForSp(profileDetailsRS.getString("ORDER_ITEM_ID"));
              videobean.setSubOrderEmailSp(profileDetailsRS.getString("EMAIL"));
              videobean.setSubOrderProspectusSp(profileDetailsRS.getString("PROSPECTUS"));
              videobean.setSubOrderItemIdForSp(profileDetailsRS.getString("SUBORDER_ITEM_ID"));
              videobean.setMyhcProfileIdSp(profileDetailsRS.getString("MYHC_PROFILE_ID"));
              videobean.setProfileTypeSp(profileType);
              videobean.setCpeQualificationNetworkIdforSp(profileDetailsRS.getString("NETWORK_ID"));
              String tmpUrl = profileDetailsRS.getString("WEBSITE");
              if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else {
                tmpUrl = "";
              }
              videobean.setSubOrderWebsiteSp(tmpUrl);
              videobean.setHotlineSp(profileDetailsRS.getString("hotline"));
              tmpUrl = profileDetailsRS.getString("EMAIL_WEBFORM");
              if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else {
                tmpUrl = "";
              }
              videobean.setSubOrderEmailWebformSp(tmpUrl);
              tmpUrl = profileDetailsRS.getString("PROSPECTUS_WEBFORM");
              if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else {
                tmpUrl = "";
              }
              videobean.setSubOrderProspectusWebformSp(tmpUrl);
              spList.add(videobean);
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      dataModel.closeCursor(profileDetailsRS);
    }
    if ((ipList != null && ipList.size() > 0) || (spList != null && spList.size() > 0)) {
      request.setAttribute("institution-profile-list", ipList);
      request.setAttribute("subject-profile-list", spList);
    }
  }

  public ArrayList getAvailableSubjectProfileList(String collegeId, String qualification) {
    DataModel dataModel = new DataModel();
    ResultSet subectProfileListRS = null;
    ArrayList spProfileList = new ArrayList();
    qualification = qualification != null && qualification.trim().length() > 0 ? qualification : "M";
    Vector parameters = new Vector();
    parameters.add(collegeId); //collegeId
    parameters.add(qualification); //qualification
    SearchBean unilandbean = null;
    try {
      subectProfileListRS = dataModel.getResultSet("WU_CPE_ADVERT_PKG.get_college_sp_profile_details", parameters);
      if (subectProfileListRS != null) {
        while (subectProfileListRS.next()) {
          unilandbean = new SearchBean();
          String advertName = subectProfileListRS.getString("ADVERT_NAME");
          String displayText = advertName != null && advertName.trim().length() > 0 ? new CommonUtil().toTitleCase(advertName) : "";
          unilandbean.setAdvertName(displayText);
          unilandbean.setMyhcProfileId(subectProfileListRS.getString("MYHC_PROFILE_ID"));
          spProfileList.add(unilandbean);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      dataModel.closeCursor(subectProfileListRS);
    }
    return spProfileList;
  }
  /**
   *
   * will give the interaction button details of the clearing profiles
   *
   * @since wu414_20120731
   * @author Sekhar Kasala
   *
   * @param collegeId
   * @param cpeQualificationNetworkId
   * @param request
   * @return
   */
  public ArrayList getCollegeInteractionDetailsWithMediaForclearing(String collegeId, String cpeQualificationNetworkId, HttpServletRequest request) {
    String isAdvertiser = "FALSE";
    ArrayList listOfCollegeInteractionDetailsWithMedia = new ArrayList();
    if (collegeId == null || collegeId.trim().length() == 0) {
      request.setAttribute("isAdvertiser", isAdvertiser);
      return listOfCollegeInteractionDetailsWithMedia;
    }
    //
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = "2";
    }
    //
    DataModel dataModel = new DataModel();
    ResultSet profileDetailsRS = null;
    String tmpUrl = "";
    CollegeCpeInteractionDetailsWithMediaBean collegeCpeInteractionDetailsWithMedia = null;
    Vector parameters = new Vector();
    parameters.add(collegeId);
    parameters.add(cpeQualificationNetworkId);
    //
    try {
      profileDetailsRS = dataModel.getResultSet("WU_CLEARING_PKG.get_clearing_enq_details_fn", parameters);
      if (profileDetailsRS != null) {
        while (profileDetailsRS.next()) {
          collegeCpeInteractionDetailsWithMedia = new CollegeCpeInteractionDetailsWithMediaBean();
          collegeCpeInteractionDetailsWithMedia.setSubOrderItemId(profileDetailsRS.getString("SUBORDER_ITEM_ID"));
          //
          tmpUrl = profileDetailsRS.getString("WEBSITE");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          collegeCpeInteractionDetailsWithMedia.setSubOrderWebsite(tmpUrl);
          collegeCpeInteractionDetailsWithMedia.setHotLineNo(profileDetailsRS.getString("hotline"));
          collegeCpeInteractionDetailsWithMedia.setCpeQualificationNetworkId(cpeQualificationNetworkId);
          // media details from db
          isAdvertiser = collegeCpeInteractionDetailsWithMedia.getSubOrderItemId() != null && collegeCpeInteractionDetailsWithMedia.getSubOrderItemId().trim().length() > 0 ? "TRUE" : "FALSE";
          request.setAttribute("isAdvertiser", isAdvertiser);
          //request.setAttribute("subOrderItemId", collegeCpeInteractionDetailsWithMedia.getSubOrderItemId());
          //
          listOfCollegeInteractionDetailsWithMedia.add(collegeCpeInteractionDetailsWithMedia);
          //collegeCpeInteractionDetailsWithMedia = null;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //nullify unused objects
      dataModel.closeCursor(profileDetailsRS);
      dataModel = null;
      profileDetailsRS = null;
      tmpUrl = null;
      parameters.clear();
      parameters = null;
      //collegeCpeInteractionDetailsWithMedia = null;
    }
    return listOfCollegeInteractionDetailsWithMedia;
  } //end of getCollegeInteractionDetailsWithMedia()

}//end of class
