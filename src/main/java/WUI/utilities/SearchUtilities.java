package WUI.utilities;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import mobile.util.MobileUtils;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Component;

import spring.valueobject.prospectus.BulkProspectusVO;
import spring.valueobject.search.AssessedFilterVO;
import spring.valueobject.search.MoneyPageVO;
import spring.valueobject.search.ProviderResultPageVO;
import spring.valueobject.search.RefineByOptionsVO;
import spring.valueobject.search.ReviewCatFilterVO;
import WUI.search.form.DidYouMeanBean;
import WUI.search.form.KeywordCourseSponsershipBean;
import WUI.search.form.RefineByPodBean;
import WUI.search.form.SearchBean;
import WUI.search.form.SearchResultBean;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;
import com.wuni.advertiser.prospectus.form.ProspectusBean;
import com.wuni.util.CommonValidator;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.SpellingSuggestionVO;
import com.wuni.util.valueobject.interstitialsearch.AssessmentTypeVO;
import com.wuni.util.valueobject.interstitialsearch.ReviewCategoryVO;
import com.wuni.util.valueobject.search.CourseSpecificSearchResultsVO;
import com.wuni.util.valueobject.search.LocationRefineVO;
import com.wuni.util.valueobject.search.SimilarStudyLevelCoursesVO;

@Component
public class SearchUtilities {

  public SearchUtilities() {
  }

  /**
   * will return list of Spelling Suggestions with their browse URL.
   *
   * @author Mohamed Syed
   * @version 1.0
   * @since 2010-Sep-15
   *
   * @param keyword
   * @param qualificationCode
   * @return listOfSpellingSuggestions
   *
   * Modification history:
   * **************************************************************************************************************
   * Author           Relase Date           Modification Details
   * **************************************************************************************************************
   * Thiyagu G        27_Jan_2016           URL restructure -Added new method for generating StudyMode, StudyLevel, 
   *                                        Subject, Location and RefineList for search filter  
   * Sabapathi S      28_Aug_2016           Assessment and preference filter change of URL with out "HERB"
   */
  public ArrayList getSpellingSuggestions(String keyword, String qualificationCode) {
    if (new CommonValidator().isBlankOrNull(keyword)) {
      return null;
    }
    //
    if (new CommonValidator().isBlankOrNull(qualificationCode)) {
      qualificationCode = "M";
    }
    ArrayList listOfSpellingSuggestions = new ArrayList();
    SeoUrls seoUrls = new SeoUrls();
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    String tmpUrl = "";
    SpellingSuggestionVO spellingSuggestionVO = null;
    Vector parameters = new Vector();
    parameters.add(keyword);
    parameters.add(qualificationCode);
    try {
      resultset = datamodel.getResultSet("WU_DIC.GET_SPELLING_SUGGESTION_FN", parameters);
      while (resultset.next()) {
        spellingSuggestionVO = new SpellingSuggestionVO();
        spellingSuggestionVO.setCategoryId(resultset.getString("CATEGORY_ID"));
        spellingSuggestionVO.setCategoryDesc(resultset.getString("CATEGORY_DESC"));
        tmpUrl = seoUrls.construnctNewBrowseMoneyPageURL(qualificationCode, spellingSuggestionVO.getCategoryId(), spellingSuggestionVO.getCategoryDesc(), "page" );
        spellingSuggestionVO.setLinkToBrowseMoneyPage(tmpUrl);
        listOfSpellingSuggestions.add(spellingSuggestionVO);
        tmpUrl = null;
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      //nullify unused objects
      datamodel.closeCursor(resultset);
      resultset = null;
      datamodel = null;
      parameters.clear();
      parameters = null;
      tmpUrl = null;
    }
    return listOfSpellingSuggestions;
  } //getSpellingSuggestions()

  /**
   * @author Mohamed Syed
   * @version 1.0
   * @since 2010-Sep-15
   *
   * will return list of did-you-mean-keywords with their URL.
   *
   * @param keyword
   * @param originalUrlString
   * @return
   */
  public ArrayList getListOfDidYouMeanKeywordDetails(String keyword, String originalUrlString) {
    ArrayList listOfDidYouMeanKeywordDetails = new ArrayList();
    if ((keyword != null && keyword.trim().length() > 0) && (originalUrlString != null && originalUrlString.trim().length() > 0)) {
      ArrayList listOfDidYouMeanKeywords = getListOfDidYouMeanKeywords(keyword);
      if (listOfDidYouMeanKeywords != null && listOfDidYouMeanKeywords.size() > 0) {
        /* what we are going to do is: split the originalUrlString and replace the keyword-part(urlArray[4]) with did-you-mean keywords
       urlArray[0]:
       urlArray[1]: courses
       urlArray[2]: degree-courses
       urlArray[3]: business-studies-degree-courses-united-kingdom    //business-degree-courses-united-kingdom
       urlArray[4]: business+studies                                  //business
       urlArray[5]: m
       urlArray[6]: united+kingdom
       urlArray[7]: united+kingdom
       urlArray[8]: 25
       urlArray[9]: 0
       urlArray[10]: a1
       urlArray[11]: 0
       urlArray[12]: r
       urlArray[13]: 0
       urlArray[14]: 1
       urlArray[15]: 0
       urlArray[16]: uc
       urlArray[17]: page
       urlArray.length: 18
       */
        CommonFunction common = new CommonFunction();
        DidYouMeanBean didYouMeanBean = null;
        String urlArray[] = originalUrlString.split("/");
        String originalKeywordWithPlus = urlArray[4];
        String originalKeyword = originalKeywordWithPlus != null && !originalKeywordWithPlus.trim().equals("0") ? common.replacePlus(originalKeywordWithPlus.trim()) : "";
        String originalKeywordWithHyphen = originalKeyword != null && !originalKeyword.trim().equals("0") ? common.replaceHypen(originalKeyword.trim()) : "";
        String didYouMeanKeyword = "";
        String didYouMeanKeywordWithHyphen = "";
        String didYouMeanKeywordWithPlus = "";
        String urlForDidYouMeanKeyword = "";
        String temp = "";
        for (int i = 0; i < listOfDidYouMeanKeywords.size(); i++) {
          didYouMeanBean = new DidYouMeanBean();
          didYouMeanKeyword = (String)listOfDidYouMeanKeywords.get(i);
          didYouMeanKeywordWithHyphen = didYouMeanKeyword != null && !didYouMeanKeyword.trim().equals("0") ? common.replaceHypen(didYouMeanKeyword.trim()) : "";
          didYouMeanKeywordWithPlus = didYouMeanKeyword != null && !didYouMeanKeyword.trim().equals("0") ? common.replaceSpacePlus(didYouMeanKeyword.trim()) : "";
          urlForDidYouMeanKeyword = "/degrees/" + urlArray[1] + "/" + urlArray[2];
          temp = urlArray[3];
          temp = temp.replaceAll(originalKeywordWithHyphen, didYouMeanKeywordWithHyphen);
          urlForDidYouMeanKeyword = urlForDidYouMeanKeyword + "/" + temp;
          temp = urlArray[4];
          temp = temp.replaceAll(originalKeywordWithPlus, didYouMeanKeywordWithPlus);
          urlForDidYouMeanKeyword = urlForDidYouMeanKeyword + "/" + temp;
          for (int j = 5; j < urlArray.length; j++) {
            urlForDidYouMeanKeyword = urlForDidYouMeanKeyword + "/" + urlArray[j];
          }
          urlForDidYouMeanKeyword = urlForDidYouMeanKeyword + ".html";
          urlForDidYouMeanKeyword = urlForDidYouMeanKeyword.toLowerCase();
          didYouMeanBean.setDidYouMeanKeyword(didYouMeanKeyword);
          didYouMeanBean.setUrlForDidYouMeanKeyword(urlForDidYouMeanKeyword);
          listOfDidYouMeanKeywordDetails.add(didYouMeanBean);
        }
      }
    }
    return listOfDidYouMeanKeywordDetails;
  } //getListOfDidYouMeanKeywordsDetails()

  /**
   * @author Mohamed Syed
   * @version 1.0
   * @since 2010-Sep-15
   *
   * will return list of did-you-mean-keywords from db.
   *
   * @param keyword
   * @return
   */
  public ArrayList getListOfDidYouMeanKeywords(String keyword) {
    ArrayList listOfDidYouMeanKeywords = new ArrayList();
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    Vector parameters = new Vector();
    parameters.add(keyword);
    try {
      resultset = dataModel.getResultSet("WHATUNI_SEARCH3.GET_DID_U_MEAN_FN", parameters);
      while (resultset.next()) {
        if (resultset.getString("SUBJECT_NAME") != null && resultset.getString("SUBJECT_NAME").trim().length() > 0) {
          listOfDidYouMeanKeywords.add(resultset.getString("SUBJECT_NAME").trim());
        }
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return listOfDidYouMeanKeywords;
  } //end of getListOfDidYouMeanKeywords()

  /**
   * @author Mohamed Syed
   * @version 1.0
   * @since 2010-Sep-15
   *
   * will return list of URLS to did-you-mean-keywords.
   * this will be money page URLs
   *
   * @param listOfDidYouMeanKeywords
   * @param originalUrlString
   * @return
   */
  public ArrayList constructListOfUrlsForListOfDidYouMeanKeywords(ArrayList listOfDidYouMeanKeywords, String originalUrlString) {
    ArrayList listOfUrlsForListOfDidYouMeanKeywords = new ArrayList();
    if (listOfDidYouMeanKeywords != null && listOfDidYouMeanKeywords.size() > 0) {
      /* what we are going to do is: split the originalUrlString and replace the keyword-part(urlArray[4]) with did-you-mean keywords
     urlArray[0]:
     urlArray[1]: courses
     urlArray[2]: degree-courses
     urlArray[3]: business-studies-degree-courses-united-kingdom    //business-degree-courses-united-kingdom
     urlArray[4]: business+studies                                  //business
     urlArray[5]: m
     urlArray[6]: united+kingdom
     urlArray[7]: united+kingdom
     urlArray[8]: 25
     urlArray[9]: 0
     urlArray[10]: a1
     urlArray[11]: 0
     urlArray[12]: r
     urlArray[13]: 0
     urlArray[14]: 1
     urlArray[15]: 0
     urlArray[16]: uc
     urlArray[17]: page
     urlArray.length: 18
     */
      CommonFunction common = new CommonFunction();
      String urlArray[] = originalUrlString.split("/");
      String originalKeywordWithPlus = urlArray[4];
      String originalKeyword = originalKeywordWithPlus != null && !originalKeywordWithPlus.trim().equals("0") ? common.replacePlus(originalKeywordWithPlus.trim()) : "";
      String originalKeywordWithHyphen = originalKeyword != null && !originalKeyword.trim().equals("0") ? common.replaceHypen(originalKeyword.trim()) : "";
      String didYouMeanKeyword = "";
      String didYouMeanKeywordWithHyphen = "";
      String didYouMeanKeywordWithPlus = "";
      String urlForDidYouMeanKeyword = "";
      String temp = "";
      for (int i = 0; i < listOfDidYouMeanKeywords.size(); i++) {
        didYouMeanKeyword = (String)listOfDidYouMeanKeywords.get(i);
        didYouMeanKeywordWithHyphen = didYouMeanKeyword != null && !didYouMeanKeyword.trim().equals("0") ? common.replaceHypen(didYouMeanKeyword.trim()) : "";
        didYouMeanKeywordWithPlus = didYouMeanKeyword != null && !didYouMeanKeyword.trim().equals("0") ? common.replaceSpacePlus(didYouMeanKeyword.trim()) : "";
        urlForDidYouMeanKeyword = "/degrees/" + urlArray[1] + "/" + urlArray[2];
        temp = urlArray[3];
        temp = temp.replaceAll(originalKeywordWithHyphen, didYouMeanKeywordWithHyphen);
        urlForDidYouMeanKeyword = urlForDidYouMeanKeyword + "/" + temp;
        temp = urlArray[4];
        temp = temp.replaceAll(originalKeywordWithPlus, didYouMeanKeywordWithPlus);
        urlForDidYouMeanKeyword = urlForDidYouMeanKeyword + "/" + temp;
        for (int j = 5; j < urlArray.length; j++) {
          urlForDidYouMeanKeyword = urlForDidYouMeanKeyword + "/" + urlArray[j];
        }
        urlForDidYouMeanKeyword = urlForDidYouMeanKeyword + ".html";
        urlForDidYouMeanKeyword = urlForDidYouMeanKeyword.toLowerCase();
        listOfUrlsForListOfDidYouMeanKeywords.add(urlForDidYouMeanKeyword);
      }
    }
    return listOfUrlsForListOfDidYouMeanKeywords;
  } //end of constructListOfUrlsForListOfDidYouMeanKeywords()


  public ArrayList customizeProviderResults(ArrayList listOfProviderResultsVO, HttpServletRequest request, String qualification, String pagename) {
    ArrayList customizedProviderResults = new ArrayList();
    if (listOfProviderResultsVO != null && listOfProviderResultsVO.size() > 0) {
      ProviderResultPageVO providerResultPageVO = null;
      HttpSession session = request.getSession();
      SearchResultBean bean = null;
      String scholashipCount = "";
      for (int i = 0; i < listOfProviderResultsVO.size(); i++) {
        providerResultPageVO = (ProviderResultPageVO)listOfProviderResultsVO.get(i);
        bean = new SearchResultBean();
        bean.setCourseid(providerResultPageVO.getCourseId());
        bean.setCoursetitle(providerResultPageVO.getCourseTitle());
        bean.setCourseDescription(providerResultPageVO.getCourseDescription());
        bean.setCourseDuration(providerResultPageVO.getCourseDuration());
        bean.setLearnDirectQual(providerResultPageVO.getLearndDirQual());
        bean.setDepartmentOrFaculty(providerResultPageVO.getDepartmentOrFaculty());
        bean.setCourseCost(providerResultPageVO.getCostDescription());
        bean.setProfile(providerResultPageVO.getProfile());
        bean.setVenueName(providerResultPageVO.getVenueName());
        bean.setPostCode(providerResultPageVO.getPostcode());
        bean.setStudyMode(providerResultPageVO.getStudyMode());
        bean.setCollegeId(providerResultPageVO.getCollegeId());
        bean.setCollegeName(providerResultPageVO.getCollegeName());
        bean.setCollegeNameDisplay(providerResultPageVO.getCollegeNameDisplay());
        request.setAttribute("collegeName", bean.getCollegeName());
        request.setAttribute("collegeNameDisplay", bean.getCollegeNameDisplay());
        bean.setUcasCode(providerResultPageVO.getUcasCode());
        bean.setUcasFlag(providerResultPageVO.getUcasFlag());
        bean.setCollegeSite(providerResultPageVO.getWebsite());
        bean.setSeoStudyLevelText(providerResultPageVO.getSeoStudyLevelText());
        request.setAttribute("studyLevel", bean.getSeoStudyLevelText());
        bean.setCourseInBasket(providerResultPageVO.getWasThisCourseShortListed());
        bean.setWebsitePaidFlag(providerResultPageVO.getWebsitePurchaseFlag());
        scholashipCount = providerResultPageVO.getScholarshipCount();
        if (scholashipCount != null && !(scholashipCount.equals("0") || scholashipCount.equals(""))) {
          request.setAttribute("showFundingTab", "TRUE");
          request.setAttribute("provider_scholorship_search_count", scholashipCount);
        }
        bean.setSubjectProfile(providerResultPageVO.getProviderUrl()); //URL for IP & SP pages
        bean.setUcasPoint(providerResultPageVO.getUcasPoint());
        bean.setAvailableStudyModes(providerResultPageVO.getAvailableStudyModes());
        bean.setFees(providerResultPageVO.getFees());
        bean.setFeeDuration(providerResultPageVO.getFeeDuration());
        if("csearch".equalsIgnoreCase(pagename)){
            bean.setCourseDetailUrl(new SeoUrls().construnctCourseDetailsPageURL(providerResultPageVO.getSeoStudyLevelText(), providerResultPageVO.getCourseTitle(), providerResultPageVO.getCourseId(), providerResultPageVO.getCollegeId(), "COURSE_SPECIFIC"));
        }else if("clcsearch".equalsIgnoreCase(pagename)){
            bean.setCourseDetailUrl(new SeoUrls().construnctCourseDetailsPageURL(providerResultPageVO.getSeoStudyLevelText(), providerResultPageVO.getCourseTitle(), providerResultPageVO.getCourseId(), providerResultPageVO.getCollegeId(), "CLEARING_COURSE_SPECIFIC"));
        }
        bean.setHotLineNo(providerResultPageVO.getHotLineNo());
        //
        bean.setOrderItemId(providerResultPageVO.getOrderItemId());
        bean.setSubOrderItemId(providerResultPageVO.getSubOrderItemId());
        bean.setSubOrderEmail(providerResultPageVO.getSubOrderEmail());
        bean.setSubOrderProspectus(providerResultPageVO.getSubOrderProspectus());
        //
        String tmpUrl = providerResultPageVO.getSubOrderWebsite();
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        bean.setSubOrderWebsite(tmpUrl);
        //
        tmpUrl = providerResultPageVO.getSubOrderEmailWebform();
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        bean.setSubOrderEmailWebform(tmpUrl);
        //
        tmpUrl = providerResultPageVO.getSubOrderProspectusWebform();
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        bean.setSubOrderProspectusWebform(tmpUrl);
        //
        //        String sc_prob40_qualification = session.getAttribute("sc_prob40_qualification") != null ? (String)session.getAttribute("sc_prob40_qualification") : "";
        //        if(sc_prob40_qualification == null || sc_prob40_qualification.trim().length() == 0){
        //          if(qualification.equalsIgnoreCase("M") || qualification.equalsIgnoreCase("N") || qualification.equalsIgnoreCase("A") || qualification.equalsIgnoreCase("T")){      
        //            sc_prob40_qualification = "ug";
        //          }else{
        //            sc_prob40_qualification = "pg";
        //          } 
        //        }
        String sc_prob40_qualification = "";
        /*  Changed by Sekhar K for wu414_20120731
         *  Getting the network id against the suborderitem id, instead of forming from the studylevel. 
        if (qualification.equalsIgnoreCase("M") || qualification.equalsIgnoreCase("N") || qualification.equalsIgnoreCase("A") || qualification.equalsIgnoreCase("T")) {
          sc_prob40_qualification = "ug";
        } else {
          sc_prob40_qualification = "pg";
        }
        bean.setCpeQualificationNetworkDesc(sc_prob40_qualification);
        if (bean.getCpeQualificationNetworkDesc() != null && bean.getCpeQualificationNetworkDesc().trim().length() > 0) {
          if (bean.getCpeQualificationNetworkDesc().equalsIgnoreCase("ug")) {
            bean.setCpeQualificationNetworkId("2");
          } else {
            bean.setCpeQualificationNetworkId("3");
          }
        } else {
          bean.setCpeQualificationNetworkId("2"); //default to ug = 2
        }
        */
        bean.setCpeQualificationNetworkId(providerResultPageVO.getNetworkId());
        bean.setPlacesAvailable(providerResultPageVO.getPlacesAvailable());
        bean.setLastUpdatedDate(providerResultPageVO.getLastUpdatedDate());
        bean.setWebformPrice(providerResultPageVO.getWebformPrice());
        bean.setWebsitePrice(providerResultPageVO.getWebsitePrice());
        session.removeAttribute("cpeQualificationNetworkId");
        session.setAttribute("cpeQualificationNetworkId", bean.getCpeQualificationNetworkId());
        customizedProviderResults.add(bean);
      }
    }
    return customizedProviderResults;
  } //end of customizeProviderResults()

   public ArrayList customizedMobileProviderResults(ArrayList listOfProviderResultsVO, HttpServletRequest request) {
     ArrayList customizedProviderResults = new ArrayList();
     if (listOfProviderResultsVO != null && listOfProviderResultsVO.size() > 0) {
       ProviderResultPageVO providerResultPageVO = null;
       HttpSession session = request.getSession();
       SearchResultBean bean = null;
       String scholashipCount = "";
       String pagename = (String)request.getAttribute("pagename");//15_JUL_2014
       for (int i = 0; i < listOfProviderResultsVO.size(); i++) {
         providerResultPageVO = (ProviderResultPageVO)listOfProviderResultsVO.get(i);
         bean = new SearchResultBean();
         bean.setCourseid(providerResultPageVO.getCourseId());
         bean.setCoursetitle(providerResultPageVO.getCourseTitle());
         bean.setCourseDescription(providerResultPageVO.getCourseDescription());
         bean.setCourseDuration(providerResultPageVO.getCourseDuration());
         bean.setLearnDirectQual(providerResultPageVO.getLearndDirQual());
         bean.setDepartmentOrFaculty(providerResultPageVO.getDepartmentOrFaculty());
         bean.setCourseCost(providerResultPageVO.getCostDescription());
         bean.setProfile(providerResultPageVO.getProfile());
         bean.setVenueName(providerResultPageVO.getVenueName());
         bean.setPostCode(providerResultPageVO.getPostcode());
         bean.setStudyMode(providerResultPageVO.getStudyMode());
         bean.setCollegeId(providerResultPageVO.getCollegeId());
         bean.setCollegeName(providerResultPageVO.getCollegeName());
         bean.setCollegeNameDisplay(providerResultPageVO.getCollegeNameDisplay());
         request.setAttribute("collegeName", bean.getCollegeName());
         request.setAttribute("collegeNameDisplay", bean.getCollegeNameDisplay());
         bean.setUcasCode(providerResultPageVO.getUcasCode());
         bean.setUcasFlag(providerResultPageVO.getUcasFlag());
         bean.setCollegeSite(providerResultPageVO.getWebsite());
         bean.setSeoStudyLevelText(providerResultPageVO.getSeoStudyLevelText());
         request.setAttribute("studyLevel", bean.getSeoStudyLevelText());
         bean.setCourseInBasket(providerResultPageVO.getWasThisCourseShortListed());
         bean.setWebsitePaidFlag(providerResultPageVO.getWebsitePurchaseFlag());
         scholashipCount = providerResultPageVO.getScholarshipCount();
         if (scholashipCount != null && !(scholashipCount.equals("0") || scholashipCount.equals(""))) {
           request.setAttribute("showFundingTab", "TRUE");
           request.setAttribute("provider_scholorship_search_count", scholashipCount);
         }
         bean.setSubjectProfile(providerResultPageVO.getProviderUrl()); //URL for IP & SP pages
         bean.setUcasPoint(providerResultPageVO.getUcasPoint());
         bean.setAvailableStudyModes(providerResultPageVO.getAvailableStudyModes());
         bean.setFees(providerResultPageVO.getFees());
         bean.setFeeDuration(providerResultPageVO.getFeeDuration());
         bean.setCourseDetailUrl(new MobileUtils().mobileCrsOppDetailPageURL(providerResultPageVO.getSeoStudyLevelText(), providerResultPageVO.getCourseTitle(), providerResultPageVO.getCourseId(), providerResultPageVO.getCollegeId(), providerResultPageVO.getOpportunityId(), pagename));//15_JUL_2014
         bean.setHotLineNo(providerResultPageVO.getHotLineNo());
         bean.setOrderItemId(providerResultPageVO.getOrderItemId());
         bean.setSubOrderItemId(providerResultPageVO.getSubOrderItemId());
         bean.setSubOrderEmail(providerResultPageVO.getSubOrderEmail());
         bean.setSubOrderProspectus(providerResultPageVO.getSubOrderProspectus());
         //
         String tmpUrl = providerResultPageVO.getSubOrderWebsite();
         if (tmpUrl != null && tmpUrl.trim().length() > 0) {
             tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
         } else {
           tmpUrl = "";
         }
         bean.setSubOrderWebsite(tmpUrl);
         //
         tmpUrl = providerResultPageVO.getSubOrderEmailWebform();
         if (tmpUrl != null && tmpUrl.trim().length() > 0) {
             tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
         } else {
           tmpUrl = "";
         }
         bean.setSubOrderEmailWebform(tmpUrl);
         //
         tmpUrl = providerResultPageVO.getSubOrderProspectusWebform();
         if (tmpUrl != null && tmpUrl.trim().length() > 0) {
             tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
         } else {
           tmpUrl = "";
         }
         bean.setSubOrderProspectusWebform(tmpUrl);
         //
         //        String sc_prob40_qualification = session.getAttribute("sc_prob40_qualification") != null ? (String)session.getAttribute("sc_prob40_qualification") : "";
         //        if(sc_prob40_qualification == null || sc_prob40_qualification.trim().length() == 0){
         //          if(qualification.equalsIgnoreCase("M") || qualification.equalsIgnoreCase("N") || qualification.equalsIgnoreCase("A") || qualification.equalsIgnoreCase("T")){      
         //            sc_prob40_qualification = "ug";
         //          }else{
         //            sc_prob40_qualification = "pg";
         //          } 
         //        }
         String sc_prob40_qualification = "";
         /*  Changed by Sekhar K for wu414_20120731
          *  Getting the network id against the suborderitem id, instead of forming from the studylevel. 
         if (qualification.equalsIgnoreCase("M") || qualification.equalsIgnoreCase("N") || qualification.equalsIgnoreCase("A") || qualification.equalsIgnoreCase("T")) {
           sc_prob40_qualification = "ug";
         } else {
           sc_prob40_qualification = "pg";
         }
         bean.setCpeQualificationNetworkDesc(sc_prob40_qualification);
         if (bean.getCpeQualificationNetworkDesc() != null && bean.getCpeQualificationNetworkDesc().trim().length() > 0) {
           if (bean.getCpeQualificationNetworkDesc().equalsIgnoreCase("ug")) {
             bean.setCpeQualificationNetworkId("2");
           } else {
             bean.setCpeQualificationNetworkId("3");
           }
         } else {
           bean.setCpeQualificationNetworkId("2"); //default to ug = 2
         }
         */
         bean.setCpeQualificationNetworkId(providerResultPageVO.getNetworkId());
         bean.setPlacesAvailable(providerResultPageVO.getPlacesAvailable());
         bean.setLastUpdatedDate(providerResultPageVO.getLastUpdatedDate());
         bean.setWebformPrice(providerResultPageVO.getWebformPrice());
         bean.setWebsitePrice(providerResultPageVO.getWebsitePrice());
        // session.removeAttribute("cpeQualificationNetworkId");
         //session.setAttribute("cpeQualificationNetworkId", bean.getCpeQualificationNetworkId());
         customizedProviderResults.add(bean);
       }
     }
     return customizedProviderResults;
   } 

  /**
   * will return money-page's main details(mid-content, refineByContent, numberOfTotalRecordsForThatSearchSynario)
   * 
   * @author  Mohamed Syed   
   * @since   wu318_20111101 - redesign
   *
   * @param searchWhat
   * @param phraseSearch
   * @param collegeName
   * @param qualification
   * @param country
   * @param townCity
   * @param postcode
   * @param searchRange
   * @param postflag
   * @param locationId
   * @param studyMode
   * @param courseDuration
   * @param lucky
   * @param crsSearch
   * @param submit
   * @param a
   * @param searchHow
   * @param sType
   * @param searchCategory
   * @param searchCareer
   * @param careerId
   * @param refId
   * @param nrp
   * @param area
   * @param pgType
   * @param searchCol
   * @param countyId
   * @param entityText
   * @param searchThru
   * @param ucasCode
   * @param deemedUniFlag
   * @param pageno
   * @param basketId
   * @param request
   * @param x
   * @param y
   * @param searchKeyword
   * @param categoryName
   * @param categoryCode
   * @return
   * @throws Exception
   */
  public HashMap loadMoneyPageMainContent(String searchWhat, String phraseSearch, String collegeName, String qualification, String country, String townCity, String postcode, String searchRange, String postflag, String locationId, String studyMode, String courseDuration, String lucky, String crsSearch, String submit, String a, String searchHow, String sType, String searchCategory, String searchCategoryId, String searchCareer, String careerId, String refId, String nrp, String area, String pgType, String searchCol, String countyId, String entityText, String searchThru, String ucasCode, String deemedUniFlag, String pageno, String basketId, HttpServletRequest request, String x, String y, String searchKeyword, String categoryName, String categoryCode, String pageUrl, String pageName, String entryLevel, String entryPoints) throws Exception {
    HashMap moneyPageMainContentMap = new HashMap();
    CommonValidator validator = new CommonValidator();
    try {
      String userAgent = request.getHeader("user-agent");
      String requestURL = request.getRequestURI();
      String trimmedPhraseSearch = phraseSearch != null ? phraseSearch.replaceAll("-", " ") : "";
      //prepare input parameters
      String searchType = "";
      if(validator.isNotBlankAndNotNull(pageName) && "PAGE".equalsIgnoreCase(pageName)){
          searchType = "COURSE_SPECIFIC";
      }else if(validator.isNotBlankAndNotNull(pageName) && "UNIVIEW".equalsIgnoreCase(pageName)){
          searchType = "UNI_SPECIFIC";
      }else if(validator.isNotBlankAndNotNull(pageName) && "CLCOURSE".equalsIgnoreCase(pageName)){
          searchType = "CLEARING_COURSE_SPECIFIC";
      }else if(validator.isNotBlankAndNotNull(pageName) && "CLUNIVIEW".equalsIgnoreCase(pageName)){
          searchType = "CLEARING_UNI_SPECIFIC";
      }
      List inputList = new ArrayList();
      inputList.add(x);
      inputList.add(y);
      inputList.add(searchWhat);
      inputList.add(trimmedPhraseSearch);
      inputList.add(collegeName);
      inputList.add(qualification);
      inputList.add(country);
      inputList.add(townCity);
      inputList.add(postcode);
      inputList.add(searchRange);
      inputList.add(postflag);
      inputList.add(locationId);
      inputList.add(studyMode);
      inputList.add(courseDuration);
      inputList.add(lucky);
      inputList.add(crsSearch);
      inputList.add(submit);
      inputList.add(a);
      inputList.add(searchHow);
      inputList.add(sType);
      inputList.add(searchCategory);
      inputList.add(searchCareer);
      inputList.add(careerId);
      inputList.add(refId);
      inputList.add(nrp);
      inputList.add(area);
      inputList.add(pgType);
      inputList.add(searchCol);
      inputList.add(countyId);
      inputList.add(entityText);
      inputList.add(searchThru);
      inputList.add(userAgent);
      inputList.add(requestURL);
      inputList.add(ucasCode);
      inputList.add(deemedUniFlag);
      inputList.add("NEW SEARCH");
      inputList.add(pageno);
      inputList.add(basketId);
      inputList.add(searchCategoryId);
      inputList.add(searchType);
      inputList.add(entryLevel);
      inputList.add(entryPoints);
      //
      ISearchBusiness searchBusiness = (ISearchBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
      Map moneyPageMainContent = new HashMap();
      if (validator.isNotBlankAndNotNull(pageName) && (pageName.equalsIgnoreCase("PAGE") || "CLCOURSE".equalsIgnoreCase(pageName))) { // -- /courses/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/page.html
        moneyPageMainContent = searchBusiness.getCourseSpecificMoneyPage(inputList);
      } else if (validator.isNotBlankAndNotNull(pageName) && (pageName.equalsIgnoreCase("UNIVIEW") || "CLUNIVIEW".equalsIgnoreCase(pageName))) { // -- /courses/*/*/*/*/*/*/*/*/*/*/*/*/*/*/*/uniview.html        
        moneyPageMainContent = searchBusiness.getUniSpecificMoneyPage(inputList);
      }
      //
      ArrayList listOfSearchResultsVO = (ArrayList)moneyPageMainContent.get("o_search_results");
      if(validator.isNotBlankAndNotNull(pageName) && pageName.equalsIgnoreCase("PAGE")){
          CourseSpecificSearchResultsVO courseSpecificSearchResultsVO = null;
          for(int i = 0; i< listOfSearchResultsVO.size(); i++){
            courseSpecificSearchResultsVO = (CourseSpecificSearchResultsVO)listOfSearchResultsVO.get(i);
            String scholarShipCount = courseSpecificSearchResultsVO.getScholarShipCount();
            request.setAttribute("scholarshipCount", scholarShipCount);
          }
      }
      ArrayList listOfRefineByOptionsVO = (ArrayList)moneyPageMainContent.get("o_refine_by_options");
      String headerId = (String)moneyPageMainContent.get("o_header_id");
      String numberOfRecordsFoundForThisSearchSynario = (String)moneyPageMainContent.get("o_record_count");
      ArrayList collegeIdsForThisSearchVO = (ArrayList)moneyPageMainContent.get("o_search_college_ids");
      ArrayList dbExecutionTimeList = (ArrayList)moneyPageMainContent.get("o_pod_time");
      if(dbExecutionTimeList != null && dbExecutionTimeList.size() > 0){
          request.setAttribute("dbExecutionTimeList",dbExecutionTimeList);
      }
      Long dbTime = (Long)moneyPageMainContent.get("dbExecutionTime");
      request.setAttribute("dbExecutionTime", dbTime);
        ArrayList snipetTextList = (ArrayList)moneyPageMainContent.get("o_snippet");
        if(snipetTextList != null && snipetTextList.size() > 0){
             request.setAttribute("snipetTextList", snipetTextList);
        }
       int collegeCount1 = Integer.parseInt(moneyPageMainContent.get("o_college_count").toString()); 
       String collegeCount = new Integer(collegeCount1).toString();
       request.setAttribute("collegeCount", collegeCount);
      //customize refine by options & ser it to request scope    
      HashMap mapOfRefineByOptions = customizeRefineByOptions(listOfRefineByOptionsVO, request, pageUrl);
      //customize search results      
      //ArrayList listOfSearchResults = customizeSearchResults(listOfSearchResultsVO, searchKeyword, categoryCode, categoryName, request, qualification);
      String keywordOrSubject = searchKeyword != null && searchKeyword.trim().length() > 0 ? searchKeyword : (categoryName != null && categoryName.trim().length() > 0 ? categoryName : "");
      request.setAttribute("keywordOrSubject", keywordOrSubject);
      CommonFunction common = new CommonFunction();
      String qualificationName =  common.getStudyLevelDesc(qualification, request);
      request.setAttribute("qualification", qualificationName);
      if (listOfSearchResultsVO != null && listOfSearchResultsVO.size() > 0) {
        moneyPageMainContentMap.put("listOfSearchResults", listOfSearchResultsVO);
      }
      String clearingBrowseLink =  (String)moneyPageMainContent.get("o_clearing_crs_exist");
      //
      moneyPageMainContentMap.put("mapOfRefineByOptions", mapOfRefineByOptions);
      moneyPageMainContentMap.put("headerId", headerId);
      moneyPageMainContentMap.put("numberOfRecordsFoundForThisSearchSynario", numberOfRecordsFoundForThisSearchSynario);
      moneyPageMainContentMap.put("listOfCollegeIdsForThisSearch", collegeIdsForThisSearchVO);
      moneyPageMainContentMap.put("clearingBrowseLink", clearingBrowseLink);
      
    } catch (Exception sqlException) {
      String errorMessage = sqlException.getMessage();
      if (errorMessage != null) {
        errorMessage = errorMessage.replaceAll("ORA-20001: ", "");
        String errorCode[] = errorMessage.split(":");
        request.setAttribute("SearchErrorMessage", errorMessage);
        if (errorCode != null && errorCode.length > 2) {
          request.setAttribute("SearchErrorCode", errorCode[2]);
        }
      }
    }
    return moneyPageMainContentMap;
  } //end of loadMoneyPageMainContent()  

  /**
    * @author Mohamed Syed
    * @version 1.0
    * @since 2010-Aug-04
    *
    * @param listOfSearchResultsVO
    * @param searchKeyword
    * @param categoryCode
    * @param categoryName
    * @param request
    *
    * This will customize the SearchResults by trasfering VO details into BEAN details.
    *
    */
  public ArrayList customizeSearchResults(ArrayList listOfSearchResultsVO, String searchKeyword, String categoryCode, String categoryName, HttpServletRequest request, String qualification) {
    ArrayList listOfSearchResults = new ArrayList();
    if (listOfSearchResultsVO != null && listOfSearchResultsVO.size() > 0) {
      CommonFunction commonFn = new CommonFunction();
      HttpSession session = request.getSession();
      MoneyPageVO moneyPageVO = null;
      SearchResultBean bean = null;
      boolean showstatsbutton = false;
      String collegeId = "";
      int count = 0;
      String ptospectustext = searchKeyword != null && searchKeyword.trim().length() > 0 ? searchKeyword : (categoryName != null && categoryName.trim().length() > 0 ? categoryName : "");
      String searchThumbnail = "";
      for (int i = 0; i < listOfSearchResultsVO.size(); i++) {
        showstatsbutton = false;
        moneyPageVO = (MoneyPageVO)listOfSearchResultsVO.get(i);
        bean = new SearchResultBean();
        collegeId = moneyPageVO.getCollegeId();
        bean.setCollegeId(collegeId);
        bean.setCollegeName(moneyPageVO.getCollegeName());
        bean.setCollegeNameDisplay(moneyPageVO.getCollegeNameDisplay());
        bean.setCollegeSite(moneyPageVO.getWebsite());
        bean.setOverAllRating(moneyPageVO.getOverallRating());
        bean.setTotalReviews(moneyPageVO.getReviewCount());
        bean.setNoOfCourses(new CommonUtil().changeNumberFormat(moneyPageVO.getNumberOfCoursesForThisCollege()));
        bean.setLearndirQualsForThisCollege(moneyPageVO.getLearndirQualsForThisCollege()); //wu312_20110614_Syed: all related qualification fetch to form non link text, PANDA fix             
        bean.setCollegeInBasket(moneyPageVO.getWasThisCollegeShortListed());
        bean.setSubProfilePurchaseFlag(moneyPageVO.getSubjectProfilePurchageFlag());
        bean.setSubjectCategoryCode(moneyPageVO.getSubjectCategoryCode());
        bean.setWebsitePaidFlag(moneyPageVO.getWebsitePurchageFlag());
        //money page region specific uk map related: wu303_20110222
        bean.setRegionName(moneyPageVO.getRegionName());
        if (bean.getRegionName() != null && bean.getRegionName().trim().length() > 0) {
          if (bean.getRegionName().equalsIgnoreCase("NORTHERN ENGLAND")) {
            bean.setRegionSpecificUkMapCssClass("NEmap");
          } else if (bean.getRegionName().equalsIgnoreCase("SOUTHERN ENGLAND")) {
            bean.setRegionSpecificUkMapCssClass("SEmap");
          } else if (bean.getRegionName().equalsIgnoreCase("CENTRAL ENGLAND")) {
            bean.setRegionSpecificUkMapCssClass("CEmap");
          } else if (bean.getRegionName().equalsIgnoreCase("GREATER LONDON")) {
            bean.setRegionSpecificUkMapCssClass("GLmap");
          } else if (bean.getRegionName().equalsIgnoreCase("NORTHERN IRELAND")) {
            bean.setRegionSpecificUkMapCssClass("NImap");
          } else if (bean.getRegionName().equalsIgnoreCase("SCOTLAND")) {
            bean.setRegionSpecificUkMapCssClass("SCmap");
          } else if (bean.getRegionName().equalsIgnoreCase("WALES")) {
            bean.setRegionSpecificUkMapCssClass("Wmap");
          } else {
            bean.setRegionSpecificUkMapCssClass("Umap"); //for default UK map
          }
        } else {
          bean.setRegionSpecificUkMapCssClass("Umap"); //for default UK map
        }
        //
        //wu310_20110517_Syed: newly added for PANDA related fix   
        bean.setNextOpenDay(moneyPageVO.getNextOpenDay());
        bean.setWhatStudentsAreSayingAboutThisCollegeList(moneyPageVO.getWhatStudentsAreSayingAboutThisCollegeList());
        //wu314_20110712_Syed: Stacey-functional-crd#1 - linking to clearing-micro-site
        bean.setClearingInfoList(moneyPageVO.getClearingInfoList());
        //CPE
        bean.setOrderItemId(moneyPageVO.getOrderItemId());
        bean.setSubOrderItemId(moneyPageVO.getSubOrderItemId());
        bean.setMyhcProfileId(moneyPageVO.getMyhcProfileId());
        bean.setAdvertName(moneyPageVO.getAdvertName());
        bean.setMediaPath(moneyPageVO.getMediaPath());
        bean.setMediaTypeId(moneyPageVO.getMediaTypeId());
        bean.setMediaId(moneyPageVO.getMediaId());
        bean.setThumbnailMediaPath(moneyPageVO.getThumbnailMediaPath());
        bean.setThumbnailMediaTypeId(moneyPageVO.getThumbnailMediaTypeId());
        bean.setInteractivityId(moneyPageVO.getInteractivityId());
        bean.setWhichProfleBoosted(moneyPageVO.getWhichProfleBoosted());
        bean.setIsIpExists(moneyPageVO.getIsIpExists());
        bean.setIsSpExists(moneyPageVO.getIsSpExists());
        bean.setSpOrderItemId(moneyPageVO.getSpOrderItemId());
        bean.setIpOrderItemId(moneyPageVO.getIpOrderItemId());
        bean.setMychPrifileIdForIp(moneyPageVO.getMychPrifileIdForIp());
        bean.setMychPrifileIdForSp(moneyPageVO.getMychPrifileIdForSp());
        bean.setSubOrderEmail(moneyPageVO.getSubOrderEmail());
        bean.setSubOrderProspectus(moneyPageVO.getSubOrderProspectus());
        String tmpUrl = moneyPageVO.getSubOrderWebsite();
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        bean.setSubOrderWebsite(tmpUrl);
        tmpUrl = moneyPageVO.getSubOrderEmailWebform();
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        bean.setSubOrderEmailWebform(tmpUrl);
        tmpUrl = moneyPageVO.getSubOrderProspectusWebform();
        if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
        } else {
          tmpUrl = "";
        }
        bean.setSubOrderProspectusWebform(tmpUrl);
        String sc_prob40_qualification = "";
        if (qualification.equalsIgnoreCase("M") || qualification.equalsIgnoreCase("N") || qualification.equalsIgnoreCase("A") || qualification.equalsIgnoreCase("T")) {
          sc_prob40_qualification = "ug";
        } else {
          sc_prob40_qualification = "pg";
        }
        bean.setCpeQualificationNetworkDesc(sc_prob40_qualification);
        if (bean.getCpeQualificationNetworkDesc() != null && bean.getCpeQualificationNetworkDesc().trim().length() > 0) {
          if (bean.getCpeQualificationNetworkDesc().equalsIgnoreCase("ug")) {
            bean.setCpeQualificationNetworkId("2");
          } else {
            bean.setCpeQualificationNetworkId("3");
          }
        } else {
          bean.setCpeQualificationNetworkId("2"); //default to ug = 2
        }
        session.removeAttribute("cpeQualificationNetworkId");
        session.setAttribute("cpeQualificationNetworkId", bean.getCpeQualificationNetworkId());
        if ((bean.getSubOrderProspectusWebform() != null && bean.getSubOrderProspectusWebform().trim().length() > 0 && !bean.getSubOrderProspectusWebform().equalsIgnoreCase("null")) || (bean.getSubOrderProspectus() != null && bean.getSubOrderProspectus().trim().length() > 0 && !bean.getSubOrderProspectus().equalsIgnoreCase("null"))) {
          String subOrderProspectusTargetURL = bean.getSubOrderProspectusWebform();
          subOrderProspectusTargetURL = subOrderProspectusTargetURL != null && subOrderProspectusTargetURL.trim().length() > 0 ? " rel=\"nofollow\" target=\"_blank\" onclick=\"GAInteractionEventTracking('prospectuswebform', 'interaction', 'Webclick','"+commonFn.replaceSpecialCharacter(bean.getCollegeName())+"');cpeProspectusWebformClick(this,'" + collegeId + "','" + bean.getSubOrderItemId() + "','" + bean.getCpeQualificationNetworkId() + "','" + subOrderProspectusTargetURL + "'); var a='s.tl(';\" href=\"" + subOrderProspectusTargetURL + "\"" : " onclick=\"return prospectusRedirect('" + request.getContextPath() + "','" + collegeId + "\','','" + ptospectustext + "','','" + bean.getSubOrderItemId() + "'); GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request','"+bean.getCollegeName()+"');\"";
          bean.setSubOrderProspectusTargetURL(subOrderProspectusTargetURL);
        } else {
          bean.setSubOrderProspectusTargetURL("");
        }
        if (bean.getMediaTypeId() != null && bean.getMediaTypeId().equalsIgnoreCase("2")) {
          bean.setSearchThumbnail(commonFn.getSearchVideoThumbPath(bean.getMediaPath(), moneyPageVO.getThumbNo()));
        } else {
          bean.setSearchThumbnail(bean.getThumbnailMediaPath());
        }
        String prospectus_pur_flag[] = moneyPageVO.getProspectusPurchageFlag().split(GlobalConstants.PROSPECTUS_SPLIT_CHAR);
        bean.setProspectusPaidFlag(prospectus_pur_flag[0]);
        String prospectusurl = "";
        if (prospectus_pur_flag != null && prospectus_pur_flag.length > 1) {
          prospectusurl = prospectus_pur_flag[1];
        }
        prospectusurl = prospectusurl != null && prospectusurl.trim().length() > 0 ? " rel=\"nofollow\" onclick=\"prospectusClick(this,'" + collegeId + "'); var a='s.tl(';\" href=\"" + request.getContextPath() + "/prospectuslink.html?cid=" + collegeId + "&url=" + prospectus_pur_flag[1] + "\"" : " onclick=\"return prospectusRedirect('" + request.getContextPath() + "','" + collegeId + "\','','" + ptospectustext + "');\"";
        bean.setProspectusExternalUrl(prospectusurl);
        request.setAttribute("scholarshipCount", moneyPageVO.getScholarshipCount());
        bean.setCollegeProfileFlag(moneyPageVO.getCollegeProfileFlag());
        bean.setProfile(moneyPageVO.getTotalProfileCount());
        String studentsSatisfaction = moneyPageVO.getStudentsSatisfaction();
        if (studentsSatisfaction != null && !studentsSatisfaction.equalsIgnoreCase("NED")) {
          bean.setStudentsSatisfication("<a title=\"" + GlobalConstants.SATISFICATION_WITH_UNI_ALT_MSG + "\" class=\"ned\">" + studentsSatisfaction + "%</a>");
          showstatsbutton = true;
        } else {
          bean.setStudentsSatisfication("");
        }
        String studentsJob = moneyPageVO.getStudentsJob();
        if (studentsJob != null && !studentsJob.equalsIgnoreCase("NED")) {
          bean.setStudentsJob("<a title=\"" + GlobalConstants.STUDENTS_JOB_ALT_MSG + "\" class=\"ned\">" + studentsJob + "</a>" + "%");
          showstatsbutton = true;
        } else {
          bean.setStudentsJob("");
        }
        String timesRanking = moneyPageVO.getTimesRanking();
        if (timesRanking != null && !timesRanking.equalsIgnoreCase("Not included") && !timesRanking.equalsIgnoreCase("0")) {
          bean.setTimesLeaguePosition(timesRanking);
          showstatsbutton = true;
        } else {
          bean.setTimesLeaguePosition("");
        }
        String ucasPoint = moneyPageVO.getUcasPoint();
        if (ucasPoint != null && !ucasPoint.equalsIgnoreCase("NED")) {
          bean.setAvgUcasPoints(ucasPoint);
          showstatsbutton = true;
        } else {
          bean.setAvgUcasPoints("");
        }
        String studentStaffRatio = moneyPageVO.getStudentStaffRatio();
        if (studentStaffRatio != null && !studentStaffRatio.equalsIgnoreCase("NED")) {
          bean.setStudentStaffRatio(studentStaffRatio);
          showstatsbutton = true;
        } else {
          bean.setStudentStaffRatio("");
        }
        String dropOutRate = moneyPageVO.getDropOutRate();
        if (dropOutRate != null && !dropOutRate.equalsIgnoreCase("NED")) {
          bean.setDropoutRate(dropOutRate);
          showstatsbutton = true;
        } else {
          bean.setDropoutRate("");
        }
        String intStudents = moneyPageVO.getIntStudents();
        if (intStudents != null && !intStudents.equalsIgnoreCase("NED")) {
          bean.setIntlStudRatio(intStudents);
        } else {
          bean.setIntlStudRatio("");
        }
        String genderRatio = moneyPageVO.getGenderRatio();
        if (genderRatio != null && !genderRatio.equalsIgnoreCase("NED")) {
          bean.setMaleFemaleRatio(genderRatio);
        } else {
          bean.setMaleFemaleRatio("");
        }
        String accomodationFee = moneyPageVO.getAccomodationFee();
        if (accomodationFee != null && !accomodationFee.equalsIgnoreCase("NED")) {
          bean.setAccomodationFees(accomodationFee);
        } else {
          bean.setAccomodationFees("");
        }
        String timesRankingSubject = moneyPageVO.getTimesRankingSubject();
        if (timesRankingSubject != null && !timesRankingSubject.equalsIgnoreCase("NED") && !timesRankingSubject.equalsIgnoreCase("0")) {
          bean.setSubjectTimesRanking(timesRankingSubject);
          showstatsbutton = true;
        } else {
          bean.setSubjectTimesRanking("");
        }
        String reviewCount = moneyPageVO.getReviewCount();
        if (reviewCount != null && !reviewCount.equals("0")) {
          showstatsbutton = true;
        }
        String videoCollegeLogo = moneyPageVO.getCollegeLogo();
        if (videoCollegeLogo != null && videoCollegeLogo.trim().length() > 0 && (videoCollegeLogo.indexOf(".flv") >= 0 || videoCollegeLogo.indexOf(".mp4") > -1)) {
          String collegVvideoUrl[] = videoCollegeLogo.split("###");
          bean.setProfileVideoUrl(collegVvideoUrl[0]);
          if (collegVvideoUrl != null && collegVvideoUrl.length > 1) {
            bean.setVideoReviewId(collegVvideoUrl[1]);
          }
          if (collegVvideoUrl != null && collegVvideoUrl.length > 2) {
            bean.setVideoUrl(collegVvideoUrl[2]);
            bean.setVideoThumbnail(commonFn.getSearchVideoThumbPath(collegVvideoUrl[2], moneyPageVO.getThumbNo()));
          }
        } else if (videoCollegeLogo != null && videoCollegeLogo.trim().length() > 0) {
          bean.setCollegeLogo(videoCollegeLogo);
        } else {
          bean.setCollegeLogo("");
        }
        String collegeLatLong = moneyPageVO.getCollegeLatitudeAndLongitude();
        if (collegeLatLong != null && collegeLatLong.trim().length() > 0 && collegeLatLong.indexOf(",") >= 0) {
          String collegeLatLongUrl[] = collegeLatLong.split(",");
          bean.setLattitude(collegeLatLongUrl[0]);
          bean.setLongtitude(collegeLatLongUrl[1]);
        }
        // end of the code added for 03rd MArch 2009 Release
        if (showstatsbutton) {
          bean.setStatDisplayButton("YES");
        }
        bean.setCategoryCode(categoryCode);
        // added for december 15th 2009 release for rerun the search for noreulst
        if (count == 0) {
          String searchAction = moneyPageVO.getSearchAction();
          if (searchAction != null && searchAction.equalsIgnoreCase("SECOND SEARCH")) {
            request.setAttribute("secondSearch", "true");
          }
          count++;
        }
        listOfSearchResults.add(bean);
      }
    }
    return listOfSearchResults;
  }

  /**
   * @author Mohamed Syed
   * @version 1.0
   * @since 2010-Aug-04
   *
   * @param listOfRefineByOptionsVO
   * @param request
   *
   * This will customize the RefineByOptions by trasfering VO details into BEAN details and will set in to request scope
   *
   */
  public HashMap customizeRefineByOptions(ArrayList listOfRefineByOptionsVO, HttpServletRequest request, String pageUrl) {
    HashMap mapOfRefineByOptions = new HashMap();
    if (listOfRefineByOptionsVO != null) {
      RefineByPodBean refineByBean = null;
      RefineByOptionsVO refineByVO = null;
      CommonUtil comUtil = new CommonUtil();
      //intividual refine by options
      ArrayList refineByProviderTypeList = new ArrayList();
      ArrayList refineByQualificationList = new ArrayList();
      ArrayList refineByStudyModeList = new ArrayList();
      ArrayList refineByLocationList = new ArrayList();
      //
      for (int i = 0; i < listOfRefineByOptionsVO.size(); i++) {
        refineByVO = (RefineByOptionsVO)listOfRefineByOptionsVO.get(i);
        refineByBean = new RefineByPodBean();
        if (isNotNullAndNotEmpty(refineByVO.getRefineOrder()) && refineByVO.getRefineOrder().equals("0")) {
          refineByBean.setProviderTypeDesc(refineByVO.getRefineDesc());
          refineByBean.setProviderTypeCode(refineByVO.getRefineCode());
          refineByProviderTypeList.add(refineByBean);
        } else if (isNotNullAndNotEmpty(refineByVO.getRefineOrder()) && refineByVO.getRefineOrder().equals("1")) {
          refineByBean.setQualificationDesc(refineByVO.getRefineDesc());
          refineByBean.setQualificationCode(refineByVO.getRefineCode());
          refineByBean.setCategoryId(refineByVO.getCategoryId());
          refineByBean.setCategoryCode(refineByVO.getCategoryCode());
          refineByQualificationList.add(refineByBean);
        } else if (isNotNullAndNotEmpty(refineByVO.getRefineOrder()) && refineByVO.getRefineOrder().equals("2")) {
          refineByBean.setStudyModeDesc(refineByVO.getRefineDesc());
          refineByBean.setStudyModeCode(refineByVO.getRefineCode());
          refineByStudyModeList.add(refineByBean);
        } else if (isNotNullAndNotEmpty(refineByVO.getRefineOrder()) && isNotNullAndNotEmpty(refineByVO.getRefineCode()) && refineByVO.getRefineOrder().equals("3")) {
          refineByBean.setLocationDesc(comUtil.getSeoFriendlyLocationName(refineByVO.getRefineDesc()));
          refineByBean.setLocationCode(refineByVO.getRefineCode());
          refineByBean.setParentCountryName(refineByVO.getParentCountryName());
          refineByBean.setDisplaySequence(refineByVO.getDisplaySequence());
          refineByBean.setNumberOfChildLocations(refineByVO.getNumberOfChildLocations());
          refineByBean.setBrowseMoneyageLocationRefineUrl(constructBrowseMoneyageLocationRefineUrl(pageUrl, refineByBean.getLocationCode()));
          //
          //          if(refineByBean.getParentCountryName().equalsIgnoreCase("ALL ENGLAND")){
          //            refineByBean.setNumberOfChildLocations("3");
          //          }else if(refineByBean.getParentCountryName().equalsIgnoreCase("ALL LONDON")){
          //            refineByBean.setNumberOfChildLocations("2");
          //          }else if(refineByBean.getParentCountryName().equalsIgnoreCase("ALL SCOTLAND")){
          //            refineByBean.setNumberOfChildLocations("4");
          //          }
          //
          refineByLocationList.add(refineByBean);
        }
      }
      //rebuild refineByProviderTypeList
      refineByBean = null;
      if (refineByProviderTypeList.size() > 1) {
        refineByBean = new RefineByPodBean();
        refineByBean.setProviderTypeDesc("Universities and colleges");
        refineByBean.setProviderTypeCode("uc");
        ArrayList tmpRefineByProviderTypeList = new ArrayList();
        tmpRefineByProviderTypeList.add(refineByBean);
        for (int i = 0; i < refineByProviderTypeList.size(); i++) {
          refineByBean = (RefineByPodBean)refineByProviderTypeList.get(i);
          tmpRefineByProviderTypeList.add(refineByBean);
        }
        refineByProviderTypeList.clear();
        for (int i = 0; i < tmpRefineByProviderTypeList.size(); i++) {
          refineByBean = (RefineByPodBean)tmpRefineByProviderTypeList.get(i);
          refineByProviderTypeList.add(refineByBean);
        }
        tmpRefineByProviderTypeList.clear();
        tmpRefineByProviderTypeList = null;
      }
      //set all refine by list into request scope
      //request.setAttribute("refineByProviderTypeList", refineByProviderTypeList);
      //request.setAttribute("refineByQualificationList", refineByQualificationList);
      //request.setAttribute("refineByStudyModeList", refineByStudyModeList);
      //request.setAttribute("refineByLocationList",refineByLocationList);    
      mapOfRefineByOptions.put("refineByProviderTypeList", refineByProviderTypeList);
      mapOfRefineByOptions.put("refineByQualificationList", refineByQualificationList);
      mapOfRefineByOptions.put("refineByStudyModeList", refineByStudyModeList);
      mapOfRefineByOptions.put("refineByLocationList", refineByLocationList);
    }
    return mapOfRefineByOptions;
  }

  /**
   *
   * @param pageUrl
   * @param location
   * @return
   */
  private String constructBrowseMoneyageLocationRefineUrl(String pageUrl, String location) {
    String firstUrl = "";
    String lastUrl = "";
    String urlQual = "";
    if (pageUrl != null && pageUrl.trim().length() > 0) {
      String urlArray[] = pageUrl.split("/");
      for (int floop = 0; floop < urlArray.length; floop++) {
        if (floop <= 3) {
          firstUrl = firstUrl + urlArray[floop] + "/";
        }
        if (floop == 4) {
          urlQual = urlArray[4];
        }
        if (floop > 5) {
          lastUrl = lastUrl + urlArray[floop] + "/";
        }
      }
      if (firstUrl != null && firstUrl.indexOf("-courses-") >= 0) {
        firstUrl = firstUrl.substring(0, firstUrl.indexOf("-courses-") + 9);
      }
      if (lastUrl != null) {
        lastUrl = lastUrl.substring(0, lastUrl.length() - 1);
      }
    }
    //String replaceHypen = new CommonFunction().replaceHypen(location);
    //String replaceURL = new CommonFunction().replaceURL(replaceHypen);
    String locationURL = firstUrl + new CommonFunction().replaceHypen(new CommonFunction().replaceURL(location)) + "/" + urlQual + "/" + new CommonFunction().replaceURL(location) + "/" + lastUrl;
    locationURL = locationURL != null ? new CommonUtil().toLowerCase(locationURL) : locationURL;
    return locationURL;
  }

  /**
  * @author Latha
  * @version 1.0
  * @since 2010-Aug-10
  *
  * @param searchWhat
  * @param phraseSearch
  * @param collegeName
  * @param qualification
  * @param country
  * @param townCity
  * @param postcode
  * @param searchRange
  * @param postflag
  * @param locationId
  * @param studyMode
  * @param courseDuration
  * @param lucky
  * @param crsSearch
  * @param submit
  * @param a
  * @param searchHow
  * @param sType
  * @param searchCategory
  * @param searchCareer
  * @param careerId
  * @param refId
  * @param nrp
  * @param area
  * @param pgType
  * @param searchCol
  * @param countyId
  * @param entityText
  * @param searchThru
  * @param ucasCode
  * @param deemedUniFlag
  * @param pageno
  * @param basketId
  * @param request
  * @param x
  * @param y
  * @param searchKeyword
  * @param categoryName
  * @param categoryCode
  * @return
  * @throws Exception
  *
  * previously WU bulk prospectus colleges was bulit based on the following steps
    * 1) call to "hot_wuni.whatuni_search.adv_col_do " will return headerId
    * 2) using that headerId call another DB "Wu_prospectus_pkg.show_prospectus_col", this will retun college list for bulk prospectus
    *
    * now we are going to get these main details from one DB-call
    *
    * Change Log
    * *************************************************************************************************************************
    * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
    * *************************************************************************************************************************
    * 2010-Aug-10   Latha                      1.0     Initial draft                                                 WU_2.0.9
    *
  */
  public HashMap loadBulkProspectusColleges(String searchWhat, String phraseSearch, String collegeName, String qualification, String country, String townCity, String postcode, String searchRange, String postflag, String locationId, String studyMode, String courseDuration, String lucky, String crsSearch, String submit, String a, String searchHow, String sType, String searchCategory, String searchCategoryId, String searchCareer, String careerId, String refId, String nrp, String area, String pgType, String searchCol, String countyId, String entityText, String searchThru, String ucasCode, String deemedUniFlag, String pageno, String basketId, HttpServletRequest request, String x, String y, String searchKeyword, String categoryName, String categoryCode) throws Exception {
    HashMap bulkProspectusCollegesMap = new HashMap();
    try {
      String userAgent = request.getHeader("user-agent");
      String requestURL = request.getRequestURI();
      String trimmedPhraseSearch = phraseSearch != null ? phraseSearch.replaceAll("-", " ") : "";
      //prepare input parameters
      List inputList = new ArrayList();
      inputList.add(x);
      inputList.add(y);
      inputList.add(searchWhat);
      inputList.add(trimmedPhraseSearch);
      inputList.add(collegeName);
      inputList.add(qualification);
      inputList.add(country);
      inputList.add(townCity);
      inputList.add(postcode);
      inputList.add(searchRange);
      inputList.add(postflag);
      inputList.add(locationId);
      inputList.add(studyMode);
      inputList.add(courseDuration);
      inputList.add(lucky);
      inputList.add(crsSearch);
      inputList.add(submit);
      inputList.add(a);
      inputList.add(searchHow);
      inputList.add(sType);
      inputList.add(searchCategory);
      inputList.add(searchCareer);
      inputList.add(careerId);
      inputList.add(refId);
      inputList.add(nrp);
      inputList.add(area);
      inputList.add(pgType);
      inputList.add(searchCol);
      inputList.add(countyId);
      inputList.add(entityText);
      inputList.add(searchThru);
      inputList.add(userAgent);
      inputList.add(requestURL);
      inputList.add(ucasCode);
      inputList.add(deemedUniFlag);
      inputList.add("NEW SEARCH");
      inputList.add(pageno);
      inputList.add(basketId);
      inputList.add(searchCategoryId);
      //
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      Map bulkProspectus = commonBusiness.getBulkProspectusColleges(inputList);
      ArrayList listOfCollegesVO = (ArrayList)bulkProspectus.get("o_search_results");
      String headerId = (String)bulkProspectus.get("o_header_id");
      String numberOfColleges = (String)bulkProspectus.get("o_record_count");
      ArrayList listOfColleges = customizeProspectusCollegeResults(listOfCollegesVO, request);
      bulkProspectusCollegesMap.put("listOfColleges", listOfColleges);
      bulkProspectusCollegesMap.put("headerId", headerId);
      bulkProspectusCollegesMap.put("numberOfColleges", numberOfColleges);
    } catch (Exception sqlException) {
      String errorMessage = sqlException.getMessage();
      if (errorMessage != null) {
        errorMessage = errorMessage.replaceAll("ORA-20001: ", "");
        String errorCode[] = errorMessage.split(":");
        request.setAttribute("SearchErrorMessage", errorMessage);
        if (errorCode != null && errorCode.length > 2) {
          request.setAttribute("SearchErrorCode", errorCode[2]);
        }
      }
    }
    return bulkProspectusCollegesMap;
  } //end of bulkProspectusCollegesMap()

  /**
   * @author Latha
   * @version 1.0
   * @since 2010-Aug-10
   *
   * @param listOfCollegesVO
   * @param request
   *
   * This will customize the colleges details by trasfering VO details into BEAN details.
   *
   */
  public ArrayList customizeProspectusCollegeResults(ArrayList listOfCollegesVO, HttpServletRequest request) {
    ArrayList listOfColleges = new ArrayList();
    if (listOfCollegesVO != null) {
      BulkProspectusVO bulkProspectusVO = null;
      ProspectusBean bean = null;
      for (int i = 0; i < listOfCollegesVO.size(); i++) {
        bulkProspectusVO = (BulkProspectusVO)listOfCollegesVO.get(i);
        bean = new ProspectusBean();
        bean.setCollegeId(bulkProspectusVO.getCollegeId());
        bean.setCollegeName(bulkProspectusVO.getCollegeName());
        bean.setCollegeNameDisplay(bulkProspectusVO.getCollegeNameDisplay());
        bean.setWebsitePaidFlag(bulkProspectusVO.getWebsitePurchaseFlag());
        bean.setProspectusPurchaseFlag(bulkProspectusVO.getProspectusPurchaseFlag());
        bean.setSubOrderItemId(bulkProspectusVO.getSubOrderItemId());
        bean.setMyhcProfileId(bulkProspectusVO.getMyhcProfileId());
        bean.setProfileType(bulkProspectusVO.getProfileType());
        bean.setCourseChecked(false);
        listOfColleges.add(bean);
      }
    }
    return listOfColleges;
  }

  /**
   * will return advertiser's featured courses
   * @param request
   * @param keyword
   * @param categoryCode
   * @param categoryId
   * @param qualification
   * @return
   */
  public ArrayList getFeaturedCourseSponsership(HttpServletRequest request, String keyword, String categoryCode, String categoryId, String qualification) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    ArrayList listOfFeaturedCourses = new ArrayList();
    KeywordCourseSponsershipBean sponsershipBean = null;
    String externalUrl = "";
    Vector parameters = new Vector();
    parameters.add(keyword);
    parameters.add(categoryCode);
    parameters.add(categoryId);
    parameters.add(qualification);
    try {
      resultset = dataModel.getResultSet("WU_UTIL_PKG.GET_FEATURED_COURSE_FN", parameters);
      while (resultset.next()) {
        sponsershipBean = new KeywordCourseSponsershipBean();
        sponsershipBean.setCollegeId(resultset.getString("COLLEGE_ID"));
        externalUrl = resultset.getString("URL");
        if(externalUrl.indexOf("whatuni.com") >= 0){
          externalUrl = (externalUrl.indexOf("https://") >= 0) ? externalUrl.replace("https://", GlobalConstants.WHATUNI_SCHEME_NAME) : (externalUrl.indexOf("http://") >= 0) ? externalUrl.replace("http://", GlobalConstants.WHATUNI_SCHEME_NAME) : GlobalConstants.WHATUNI_SCHEME_NAME + externalUrl;
        } else {
          externalUrl = (externalUrl.indexOf("http://") >= 0 || externalUrl.indexOf("https://") >= 0) ? externalUrl : "http://" + externalUrl;
        }
        sponsershipBean.setCollegeExternalUrl(externalUrl);
        sponsershipBean.setKeyword(resultset.getString("KEYWORD"));
        sponsershipBean.setCollegeName(resultset.getString("COLLEGE_NAME"));
        sponsershipBean.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
        sponsershipBean.setCourseTitle(resultset.getString("COURSE_TITLE"));
        sponsershipBean.setCourseDescription(resultset.getString("COURSE_DESC"));
        listOfFeaturedCourses.add(sponsershipBean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    request.setAttribute("listOfFeaturedCourses", listOfFeaturedCourses);
    return listOfFeaturedCourses;
  } //getFeaturedCourseSponsership()

  public ArrayList getCategorySponsership(HttpServletRequest request, String keyword, String categoryCode, String categoryId, String qualification) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    ArrayList listOfKeywordSponsers = new ArrayList();
    ArrayList listOfFeaturedCourses = new ArrayList();
    KeywordCourseSponsershipBean sponsershipBean = null;
    String externalUrl = "";
    String reviewDesc = "";
    Vector parameters = new Vector();
    parameters.add(keyword);
    parameters.add(categoryCode);
    parameters.add(categoryId);
    parameters.add(qualification);
    try {
      resultset = dataModel.getResultSet("WU_UTIL_PKG.GET_CATEGORY_SPONSORSHIP_FN", parameters);
      while (resultset.next()) {
        sponsershipBean = new KeywordCourseSponsershipBean();
        reviewDesc = resultset.getString("REVIEW_DESCRIPTION");
        if (reviewDesc != null && reviewDesc.length() > 0) {
          sponsershipBean.setCollegeId(resultset.getString("COLLEGE_ID"));
          sponsershipBean.setCollegeName(resultset.getString("COLLEGE_NAME"));
          sponsershipBean.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
          externalUrl = resultset.getString("URL");
          if(externalUrl.indexOf("whatuni.com") >= 0){
            externalUrl = (externalUrl.indexOf("https://") >= 0) ? externalUrl.replace("https://", GlobalConstants.WHATUNI_SCHEME_NAME) : (externalUrl.indexOf("http://") >= 0) ? externalUrl.replace("http://", GlobalConstants.WHATUNI_SCHEME_NAME) : GlobalConstants.WHATUNI_SCHEME_NAME + externalUrl;
          } else {
            externalUrl = (externalUrl.indexOf("http://") >= 0 || externalUrl.indexOf("https://") >= 0) ? externalUrl : "http://" + externalUrl;
          }
          sponsershipBean.setCollegeExternalUrl(externalUrl);
          sponsershipBean.setKeyword(resultset.getString("KEYWORD"));
          sponsershipBean.setReviewerName(resultset.getString("REVIEWER_NAME"));
          sponsershipBean.setReviewDescription(resultset.getString("REVIEW_DESCRIPTION"));
          listOfKeywordSponsers.add(sponsershipBean);
        } else {
          sponsershipBean = new KeywordCourseSponsershipBean();
          sponsershipBean.setCollegeId(resultset.getString("COLLEGE_ID"));
          externalUrl = resultset.getString("URL");
          if(externalUrl.indexOf("whatuni.com") >= 0){
            externalUrl = (externalUrl.indexOf("https://") >= 0) ? externalUrl.replace("https://", GlobalConstants.WHATUNI_SCHEME_NAME) : (externalUrl.indexOf("http://") >= 0) ? externalUrl.replace("http://", GlobalConstants.WHATUNI_SCHEME_NAME) : GlobalConstants.WHATUNI_SCHEME_NAME + externalUrl;
          } else {
            externalUrl = (externalUrl.indexOf("http://") >= 0 || externalUrl.indexOf("https://") >= 0) ? externalUrl : "http://" + externalUrl;
          }
          sponsershipBean.setCollegeExternalUrl(externalUrl);
          sponsershipBean.setKeyword(resultset.getString("KEYWORD"));
          sponsershipBean.setCollegeName(resultset.getString("COLLEGE_NAME"));
          sponsershipBean.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
          sponsershipBean.setCourseTitle(resultset.getString("COURSE_TITLE"));
          sponsershipBean.setCourseDescription(resultset.getString("COURSE_DESC"));
          listOfFeaturedCourses.add(sponsershipBean);
          //  break;
        }
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    request.setAttribute("listOfKeywordSponsers", listOfKeywordSponsers);
    request.setAttribute("listOfFeaturedCourses", listOfFeaturedCourses);
    return listOfKeywordSponsers;
  } //getCategorySponsership()

   //wu413_20120710 Added By Sekhar K This function will return the keywordCategory sponsorship for clearing.
   public ArrayList getCategorySponsershipClearing(HttpServletRequest request, String keyword, String categoryCode, String categoryId, String type) {
     DataModel dataModel = new DataModel();
     ResultSet resultset = null;
     ArrayList listOfKeywordSponsers = new ArrayList();
     ArrayList listOfFeaturedCourses = new ArrayList();
     KeywordCourseSponsershipBean sponsershipBean = null;
     String externalUrl = "";
     String reviewDesc = "";
     Vector parameters = new Vector();
     parameters.add(keyword);
     parameters.add(categoryCode);
     parameters.add(categoryId);
     parameters.add(type);
     try {
       resultset = dataModel.getResultSet("HOT_WUNI.WU_UTIL_PKG.GET_CLEARING_CAT_SPONSORS_FN", parameters);
       while (resultset != null && resultset.next()) {
         sponsershipBean = new KeywordCourseSponsershipBean();
           sponsershipBean.setCatSponsorId(resultset.getString("cat_sponsor_id"));
           sponsershipBean.setCollegeId(resultset.getString("COLLEGE_ID"));
           sponsershipBean.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
           externalUrl = resultset.getString("URL");
           if (externalUrl != null && externalUrl.trim().length() > 0) {
             externalUrl = new CommonFunction().appendingHttptoURL(externalUrl);
           } else {
             externalUrl = "";
           }
           sponsershipBean.setCollegeExternalUrl(externalUrl);
           sponsershipBean.setKeyword(resultset.getString("KEYWORD"));
           sponsershipBean.setMediaPath(resultset.getString("media_path"));
           sponsershipBean.setText(resultset.getString("text"));
           listOfKeywordSponsers.add(sponsershipBean);
       }
     } catch (Exception exception) {
       exception.printStackTrace();
     } finally {
       dataModel.closeCursor(resultset);
     }
     request.setAttribute("listOfKeywordSponsers", listOfKeywordSponsers);
     return listOfKeywordSponsers;
   } 

  /**
   * will return refineby pod's value to searchresult page
   * @param headerId
   * @param categoryId
   * @param request
   */
  public void getRefineByPodValues(String headerId, String categoryId, HttpServletRequest request) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    Vector parameters = new Vector();
    RefineByPodBean refineByBean = new RefineByPodBean();
    //intividual refine by options
    ArrayList refineByProviderTypeList = new ArrayList();
    ArrayList refineByQualificationList = new ArrayList();
    ArrayList refineByStudyModeList = new ArrayList();
    //ArrayList refineByLocationList = new ArrayList(); 
    //parameters to fetch refine options
    parameters.add(headerId);
    parameters.add(categoryId);
    try {
      resultset = dataModel.getResultSet("WU_UTIL_PKG.GET_REFINE_POD_FN", parameters);
      while (resultset.next()) {
        refineByBean = new RefineByPodBean();
        refineByBean.setRefineOrder(resultset.getString("REFINE_ORDER"));
        if (isNotNullAndNotEmpty(refineByBean.getRefineOrder()) && refineByBean.getRefineOrder().equals("0")) {
          refineByBean.setProviderTypeDesc(resultset.getString("REFINE_DESC"));
          refineByBean.setProviderTypeCode(resultset.getString("REFINE_CODE"));
          refineByProviderTypeList.add(refineByBean);
        } else if (isNotNullAndNotEmpty(refineByBean.getRefineOrder()) && refineByBean.getRefineOrder().equals("1")) {
          refineByBean.setQualificationDesc(resultset.getString("REFINE_DESC"));
          refineByBean.setQualificationCode(resultset.getString("REFINE_CODE"));
          refineByBean.setCategoryId(resultset.getString("CATEGORY_ID"));
          refineByBean.setCategoryCode(resultset.getString("CATEGORY_CODE"));
          refineByQualificationList.add(refineByBean);
        } else if (isNotNullAndNotEmpty(refineByBean.getRefineOrder()) && refineByBean.getRefineOrder().equals("2")) {
          refineByBean.setStudyModeDesc(resultset.getString("REFINE_DESC"));
          refineByBean.setStudyModeCode(resultset.getString("REFINE_CODE"));
          refineByStudyModeList.add(refineByBean);
        }
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    //rebuild refineByProviderTypeList
    refineByBean = null;
    if (refineByProviderTypeList.size() > 1) {
      refineByBean = new RefineByPodBean();
      refineByBean.setProviderTypeDesc("Universities and colleges");
      refineByBean.setProviderTypeCode("uc");
      ArrayList tmpRefineByProviderTypeList = new ArrayList();
      tmpRefineByProviderTypeList.add(refineByBean);
      for (int i = 0; i < refineByProviderTypeList.size(); i++) {
        refineByBean = (RefineByPodBean)refineByProviderTypeList.get(i);
        tmpRefineByProviderTypeList.add(refineByBean);
      }
      refineByProviderTypeList.clear();
      for (int i = 0; i < tmpRefineByProviderTypeList.size(); i++) {
        refineByBean = (RefineByPodBean)tmpRefineByProviderTypeList.get(i);
        refineByProviderTypeList.add(refineByBean);
      }
      tmpRefineByProviderTypeList.clear();
      tmpRefineByProviderTypeList = null;
    }
    //set all refine by list into request scope
    request.setAttribute("refineByProviderTypeList", refineByProviderTypeList);
    request.setAttribute("refineByQualificationList", refineByQualificationList);
    request.setAttribute("refineByStudyModeList", refineByStudyModeList);
    //request.setAttribute("refineByLocationList",refineByLocationList);
    //
    //nullify unused objects    
    refineByBean = null;
    dataModel = null;
    resultset = null;
    parameters.clear();
    parameters = null;
    refineByProviderTypeList = null;
    refineByQualificationList = null;
    refineByStudyModeList = null;
    //refineByLocationList = null;
  } //end of getRefineByOptions

  public boolean isNullOrEmpty(String str) {
    return (str == null || str.trim().length() == 0);
  }

  public boolean isNotNullAndNotEmpty(String str) {
    return !(str == null || str.trim().length() == 0);
  }

  public String getStrikeTroughDates(String inDates) {
    CommonUtil commonutil = new CommonUtil();
    String finalStartDate = "";
    String startDateStatus = inDates;
    String tmpstartDateArray = "";
    String monthValue = "";
    String tmpValue = "";
    String[] startDateArray = null;
    String[] innerSplit = null;
    boolean postDate = false;
    boolean hasMultipleStartDates = false;
    if (startDateStatus != null) {
      startDateArray = startDateStatus.split(",");
      hasMultipleStartDates = startDateArray != null && startDateArray.length == 1 ? false : true;
      if (hasMultipleStartDates) {
        for (int z = 0; z < startDateArray.length; z++) {
          tmpstartDateArray = startDateArray[z].trim();
          if ((tmpstartDateArray != null && tmpstartDateArray.indexOf("/") > 0) || (tmpstartDateArray != null && tmpstartDateArray.indexOf("-") > 0)) {
            if (tmpstartDateArray.indexOf("/") > 0) {
              tmpstartDateArray = tmpstartDateArray.replaceAll("/", "-");
            }
            innerSplit = tmpstartDateArray.trim().split("-");
            if (innerSplit != null && innerSplit.length == 2) {
              postDate = convertAndComp(tmpstartDateArray.trim());
            } else {
              tmpValue = innerSplit[1].trim();
              if (commonutil.isNumber(tmpValue)) {
                monthValue = convertToMonth(tmpValue.trim());
                monthValue = innerSplit[0].trim() + "-" + monthValue + "-" + innerSplit[2].trim();
                tmpstartDateArray = monthValue.trim();
              }
              postDate = convertToCal(tmpstartDateArray, "");
            }
            if (postDate) {
              if (finalStartDate != null && finalStartDate.trim().length() > 0) {
                finalStartDate = finalStartDate + ", " + tmpstartDateArray;
              } else {
                finalStartDate = tmpstartDateArray;
              }
            } else {
              if (finalStartDate != null && finalStartDate.trim().length() > 0) {
                finalStartDate = finalStartDate + ", <del>" + tmpstartDateArray + "</del>";
              } else {
                finalStartDate = "<del>" + tmpstartDateArray + "</del>";
              }
            }
          } else {
            //only months
            if (finalStartDate != null && finalStartDate.trim().length() > 0) {
              finalStartDate = finalStartDate + ", " + tmpstartDateArray;
            } else {
              finalStartDate = tmpstartDateArray;
            }
          }
        }
      } else {
        tmpstartDateArray = startDateArray[0];
        if ((tmpstartDateArray != null && tmpstartDateArray.indexOf("/") > 0) || (tmpstartDateArray != null && tmpstartDateArray.indexOf("-") > 0)) {
          if (tmpstartDateArray.indexOf("/") > 0) {
            tmpstartDateArray = tmpstartDateArray.replaceAll("/", "-");
          }
          innerSplit = tmpstartDateArray.split("-");
          if (innerSplit != null && innerSplit.length == 2) {
            postDate = convertAndComp(tmpstartDateArray);
          } else {
            tmpValue = innerSplit[1];
            if (commonutil.isNumber(tmpValue)) {
              monthValue = convertToMonth(tmpValue);
              monthValue = innerSplit[0] + "-" + monthValue + "-" + innerSplit[2];
              tmpstartDateArray = monthValue;
            }
            postDate = convertToCal(tmpstartDateArray, "");
          }
          if (postDate) {
            finalStartDate = startDateStatus;
          } else {
            finalStartDate = "<del>" + startDateStatus + "</del>";
          }
        } else {
          finalStartDate = startDateStatus;
        }
      }
    }
    return finalStartDate;
  }

  public static boolean convertAndComp(String twoValue) {
    String[] tmpValueArray = null;
    String[] tmpInnerSplit = new String[3];
    String finalValue = "";
    boolean twoBooleanValue = false;
    tmpValueArray = twoValue.split("-");
    tmpInnerSplit[0] = "01".trim();
    tmpInnerSplit[1] = tmpValueArray[0].trim();
    tmpInnerSplit[2] = tmpValueArray[1].trim();
    finalValue = tmpInnerSplit[0] + "-" + tmpInnerSplit[1] + "-" + tmpInnerSplit[2];
    twoBooleanValue = convertToCal("", finalValue);
    return twoBooleanValue;
  }

  public static String convertToMonth(String monthValue) {
    String month = "";
    if (monthValue != null) {
      int aInt = Integer.parseInt(monthValue);
      DateFormatSymbols dfs = new DateFormatSymbols();
      String[] months = dfs.getMonths();
      if (aInt >= 0 && aInt <= 11) {
        month = months[aInt - 1];
      }
    }
    return month;
  }

  public static boolean convertToCal(String dat, String addmonth) {
    boolean pastDate = false;
    Calendar todayCal = Calendar.getInstance();
    Calendar cal = Calendar.getInstance();
    try {
      DateFormat formatter = new SimpleDateFormat("dd-MMM-yy");
      if (addmonth.trim() != "") {
        Date date = (Date)formatter.parse(addmonth);
        cal.setTime(date);
        cal.add(Calendar.MONTH, +1);
      } else {
        Date date = (Date)formatter.parse(dat);
        cal.setTime(date);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    pastDate = compareCalendar(cal, todayCal, ">");
    return pastDate;
  }

  public static boolean compareCalendar(Calendar cal1, Calendar cal2, String operator) {
    int month1 = 0;
    int month2 = 0;
    int year1 = 0;
    int year2 = 0;
    int day1 = 0;
    int day2 = 0;
    month1 = cal1.get(Calendar.MONTH);
    year1 = cal1.get(Calendar.YEAR);
    day1 = cal1.get(Calendar.DATE);
    month2 = cal2.get(Calendar.MONTH);
    year2 = cal2.get(Calendar.YEAR);
    day2 = cal2.get(Calendar.DATE);
    if (operator.equals(">")) {
      if (year1 < year2) {
        return false;
      } else if (year1 > year2) {
        return true;
      }
      if (month1 < month2) {
        return false;
      } else if (month1 > month2) {
        return true; // this means date 1 is greater than date 2 
      }
      if (day1 <= day2) {
        return false;
      }
    }
    return true; // this means date 1 is greater than date 2
  }
  
 public void optimiseRefineList(ArrayList refineList, String pageURL, HttpServletRequest request){
   Iterator itr = refineList.iterator();
   String filterparam = (String)request.getAttribute("queryStr");
   ArrayList campustypeList = new ArrayList();
   ArrayList russelList = new ArrayList();
   ArrayList locationList = new ArrayList();
   pageURL = "/degrees" + pageURL;
   filterparam = GenericValidator.isBlankOrNull(filterparam)?"":filterparam;
   if(!GenericValidator.isBlankOrNull(filterparam)){
     if(("nd").equals(filterparam.substring(0,2))){
       filterparam = filterparam.substring(2);
     }
   }
   String filterURL = "";
   String url = "";
   RefineByOptionsVO campusVO = new RefineByOptionsVO();
   RefineByOptionsVO russellVO = new RefineByOptionsVO();
   RefineByOptionsVO locVO = new RefineByOptionsVO();
   campusVO.setRefineDesc("All campus types");  
   String allCampusURL = appendFilterVlaues("campus", "",request);
   if(!GenericValidator.isBlankOrNull(allCampusURL)){ allCampusURL = "?nd" + allCampusURL;}
   campusVO.setBrowseMoneyageLocationRefineUrl(pageURL +  allCampusURL);
   campustypeList.add(campusVO);
   russellVO.setRefineDesc("All universities");  
   String allRussellURL = appendFilterVlaues("russell", "",request);
   if(!GenericValidator.isBlankOrNull(allRussellURL)){ allRussellURL = "?nd" + allRussellURL;}
   russellVO.setBrowseMoneyageLocationRefineUrl(pageURL +  allRussellURL);
   russelList.add(russellVO);
   Map locationMap = new HashMap();
   while(itr.hasNext()){
     RefineByOptionsVO campustypeVO = new RefineByOptionsVO();
     RefineByOptionsVO russelVO = new RefineByOptionsVO();
     RefineByOptionsVO locationVO = new RefineByOptionsVO();
     RefineByOptionsVO  refineVO = (RefineByOptionsVO)itr.next();   
     if( ("UNIV_LOC_TYPE").equals(refineVO.getRefineOrder())){
      
       locationVO.setRefineCode(refineVO.getRefineCode());
       locationVO.setRefineDesc(refineVO.getRefineDesc());      
       if(!GenericValidator.isBlankOrNull(filterparam)){
       
            filterURL = appendFilterVlaues("location",  locationVO.getRefineCode(), request);         
         
       }else{          
            filterURL = filterparam+"&lc-tpe="+ locationVO.getRefineCode();
       }
       url = pageURL + "?nd" + filterURL;
       locationVO.setBrowseMoneyageLocationRefineUrl(url);
       locationList.add(locationVO);
       filterURL = "";
     } else if(("CAMPUS_BASED_UNIV").equals(refineVO.getRefineOrder())){
       campustypeVO.setRefineCode(refineVO.getRefineCode());
       campustypeVO.setRefineDesc(refineVO.getRefineDesc());  
       if(!GenericValidator.isBlankOrNull(filterparam)){
        
            filterURL = appendFilterVlaues("campus",  campustypeVO.getRefineCode(), request); 
         
       }else{          
            filterURL = filterparam+"&cmps="+ campustypeVO.getRefineCode();
       }     
       url = pageURL + "?nd" + filterURL;
       campustypeVO.setBrowseMoneyageLocationRefineUrl(url);
       campustypeList.add(campustypeVO);
       filterURL = "";
     } else if(("RUSSELL_GROUP").equals(refineVO.getRefineOrder())){
       russelVO.setRefineCode(refineVO.getRefineCode());
       russelVO.setRefineDesc(refineVO.getRefineDesc());     
       if(!GenericValidator.isBlankOrNull(filterparam)){
       
            filterURL = appendFilterVlaues("russell",  russelVO.getRefineCode(), request); 
         
       }else{          
            filterURL = filterparam+"&rsl="+ russelVO.getRefineCode();
       }       
       url = pageURL + "?nd" + filterURL;
       russelVO.setBrowseMoneyageLocationRefineUrl(url);
       russelList.add(russelVO);
       filterURL ="";
     }   
   }
   locationMap.put("676", "Big City");
   locationMap.put("677", "Seaside");
   locationMap.put("675", "Town");
   locationMap.put("674", "Countryside");
   request.setAttribute("locationMap", locationMap);
   request.setAttribute("campustypeList", campustypeList);
   request.setAttribute("russelList", russelList);
   request.setAttribute("locationList", locationList);
 }


  public String appendFilterVlaues(String filterType, String filterValue, HttpServletRequest request){
    String filter = "";
    String removeModuleParameter = (String)request.getAttribute("removeModuleParameter");
           removeModuleParameter = !GenericValidator.isBlankOrNull(removeModuleParameter)? removeModuleParameter: "";
    String removePostcodeParameter = (String)request.getAttribute("removePostcodeParameter");
           removePostcodeParameter = !GenericValidator.isBlankOrNull(removePostcodeParameter)? removePostcodeParameter: "";
    String russellGroup = request.getParameter("rsl"); 
               russellGroup = !GenericValidator.isBlankOrNull(russellGroup)? russellGroup: "";
    String module = "";
    if(!("YES").equals(removeModuleParameter)){
      module = request.getParameter("mdl");
      module = !GenericValidator.isBlankOrNull(module)? module: "";
    }
    String distance = "";
    String postCode = "";
    if(!("YES").equals(removePostcodeParameter)){
                distance = request.getParameter("dist");
                distance = !GenericValidator.isBlankOrNull(distance)? distance: "";
                postCode = request.getParameter("pc");
                postCode = !GenericValidator.isBlankOrNull(postCode)? postCode:"";
    }
    String empRateMax = request.getParameter("e-rt-max");
                empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
    String empRateMin = request.getParameter("e-rt-min");
                empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
    String campusType = request.getParameter("cmps");
                campusType = !GenericValidator.isBlankOrNull(campusType)? campusType: "";
    String locType = request.getParameter("lc-tpe");
                locType = !GenericValidator.isBlankOrNull(locType)? locType: "";
    String ucasTarrifMax = request.getParameter("trf-max");
                ucasTarrifMax = !GenericValidator.isBlankOrNull(ucasTarrifMax)? ucasTarrifMax: "";
    String ucasTarrifMin = request.getParameter("trf-min");
                ucasTarrifMin = !GenericValidator.isBlankOrNull(ucasTarrifMin)? ucasTarrifMin: "";
    String jacs = request.getParameter("jacs");//3_JUN_2014
                jacs = !GenericValidator.isBlankOrNull(jacs)? jacs: "";
    String isClearing = (String)request.getAttribute("searchClearing");//30_JUN_2015
           isClearing = !GenericValidator.isBlankOrNull(isClearing)? isClearing: "";  
        
    if("TRUE".equals(isClearing)){//30_JUN_2015
      filter += "&clearing";
    }   
    if("russell".equals(filterType)){
     if(!GenericValidator.isBlankOrNull(filterValue)){
      filter += "&rsl="+ filterValue;
     }
    }else{         
      if(!GenericValidator.isBlankOrNull(russellGroup)){
        filter += "&rsl="+russellGroup;
      }
    }
    if("module".equals(filterType)){
      if(!GenericValidator.isBlankOrNull(filterValue)){
        filter += "&mdl="+filterValue;
      }
    }else{
      if(!GenericValidator.isBlankOrNull(module)){
        filter += "&mdl="+module;
      }
    } 
    
    if("postcode".equals(filterType)){
      if(!GenericValidator.isBlankOrNull(filterValue)){
        filter += "&dist="+distance;
      }
      if(!GenericValidator.isBlankOrNull(filterValue)){
        filter += "&pc="+postCode;
      }
    }else{
      if(!GenericValidator.isBlankOrNull(distance)){
        filter += "&dist="+distance;
      }
      if(!GenericValidator.isBlankOrNull(postCode)){
        filter += "&pc="+postCode;
      }
    }    
    if(!GenericValidator.isBlankOrNull(empRateMax)){
      filter += "&e-rt-max="+empRateMax;
    }
    if(!GenericValidator.isBlankOrNull(empRateMin)){
      filter += "&e-rt-min="+empRateMin;
    }
    if("campus".equals(filterType)){
      if(!GenericValidator.isBlankOrNull(filterValue)){
       filter += "&cmps="+ filterValue;
      }
    }else{ 
    if(!GenericValidator.isBlankOrNull(campusType)){
      filter += "&cmps="+campusType;
    }
    }
    if("location".equals(filterType)){
      if(!GenericValidator.isBlankOrNull(filterValue)){
        filter += "&lc-tpe="+ filterValue;
      }
    }else {
      if(!GenericValidator.isBlankOrNull(locType)){
         filter += "&lc-tpe="+locType;
       }    
    }
    if(!GenericValidator.isBlankOrNull(ucasTarrifMax)){
      filter += "&trf-max="+ucasTarrifMax;
    }
    if(!GenericValidator.isBlankOrNull(ucasTarrifMin)){
      filter += "&trf-min="+ucasTarrifMin;
    }
    if(!GenericValidator.isBlankOrNull(jacs)){//3_JUN_2014
      filter += "&jacs="+jacs;
    }
    return filter;
  }
  
  public ArrayList customiseLocationList(String pageURL, ArrayList locationList, HttpServletRequest request, String searchType, String pagename){
    Iterator itr = locationList.iterator();
    ArrayList newLocationList = new ArrayList();
    String filterparam = (String)request.getAttribute("queryStr");
    filterparam = GenericValidator.isBlankOrNull(filterparam)?"":filterparam;
    if(!GenericValidator.isBlankOrNull(filterparam)){
      if(("nd").equals(filterparam.substring(0,2))){
        filterparam = filterparam.substring(2);
      }
    }
     LocationRefineVO loctionVO = new LocationRefineVO();
      if(searchType.equals("BROWSE")){
       loctionVO.setLocationURL(new SeoUrls().constructBrowseLocationURL(pageURL, "united kingdom", filterparam, pagename));
      }else if(searchType.equals("KEYWORD")){
        loctionVO.setLocationURL(new SeoUrls().constructKwdLocationURL(pageURL, "united kingdom", filterparam, pagename));
      }
     loctionVO.setRegionName("All locations");
    loctionVO.setTopRegionName("All locations");
     newLocationList.add(loctionVO);    
    while(itr.hasNext()){
      LocationRefineVO locVO = (LocationRefineVO)itr.next();
      String regionName = locVO.getRegionName().replaceAll("All ", "");
      regionName = regionName.toLowerCase();
      if(searchType.equals("BROWSE")){
        locVO.setLocationURL(new SeoUrls().constructBrowseLocationURL(pageURL, regionName, filterparam, pagename));
      }else if(searchType.equals("KEYWORD")){
        locVO.setLocationURL(new SeoUrls().constructKwdLocationURL(pageURL, regionName, filterparam, pagename));
      }
      newLocationList.add(locVO);
    }
    return newLocationList;
  }
  //
   public ArrayList customiseStudyModeFromBrowseList(String pageURL, ArrayList studyModeList, SearchBean searchBean, HttpServletRequest request, String searchType, String pagename){
     Iterator itr = studyModeList.iterator();
     ArrayList newStudyModeList = new ArrayList();
     String filterparam = (String)request.getAttribute("queryStr");
     filterparam = GenericValidator.isBlankOrNull(filterparam)? "":filterparam;
     if(!GenericValidator.isBlankOrNull(filterparam)){
       if(("nd").equals(filterparam.substring(0,2))){
         filterparam = filterparam.substring(2);
       }
     }     
     RefineByOptionsVO studyModVO = new RefineByOptionsVO();
      if(searchType.equals("BROWSE")){
       studyModVO.setBrowseMoneyageLocationRefineUrl("/degrees/" + pageURL + "/page.html");
      }else if(searchType.equals("KEYWORD")){
       studyModVO.setBrowseMoneyageLocationRefineUrl(new SeoUrls().constructStudyModeURLFromKwd(pageURL, "0", searchBean.getSubjectId(), filterparam, pagename));
      }
     studyModVO.setRefineDesc("All study modes");
     newStudyModeList.add(studyModVO);
     
     while(itr.hasNext()){
       RefineByOptionsVO studyModeVO = (RefineByOptionsVO)itr.next();
       if(searchType.equals("BROWSE")){
         studyModeVO.setBrowseMoneyageLocationRefineUrl(new SeoUrls().constructStudyModeURLFromBrowse(pageURL, studyModeVO.getRefineCode(), searchBean.getSubjectId(), filterparam, pagename));
       }else if(searchType.equals("KEYWORD")){
         studyModeVO.setBrowseMoneyageLocationRefineUrl(new SeoUrls().constructStudyModeURLFromKwd(pageURL, studyModeVO.getRefineCode(), searchBean.getSubjectId(), filterparam, pagename));
       }
       newStudyModeList.add(studyModeVO);
     }
     return newStudyModeList;
   }
  //Added new method for generating StudyMode, StudyLevel, Subject, Location and RefineList for search filter for 27_Jan_2016, By Thiyagu G. Start
  public ArrayList customiseStudyModeListForSR(String pageURL, ArrayList studyModeList, HttpServletRequest request, String searchCount, String resultExists, String rf, String watSearch){
    Iterator itr = studyModeList.iterator();
    ArrayList newStudyModeList = new ArrayList();
    String qryString = request.getQueryString();
    qryString = GenericValidator.isBlankOrNull(qryString)? "":qryString;
    if(!"HERB".equalsIgnoreCase(watSearch)){
      RefineByOptionsVO studyModVO = new RefineByOptionsVO();
      studyModVO.setBrowseMoneyageLocationRefineUrl(new SeoUrls().constructStudyModeURLForSR(request, pageURL, "", qryString, searchCount, resultExists, rf, watSearch));     
      studyModVO.setRefineDesc("All study modes");
      newStudyModeList.add(studyModVO);    
    }
    while(itr.hasNext()){
      RefineByOptionsVO studyModeVO = (RefineByOptionsVO)itr.next();
      if("HERB".equalsIgnoreCase(watSearch) && "Y".equalsIgnoreCase(studyModeVO.getSelectedStudyMode())){
        request.setAttribute("studyModeDisplay", studyModeVO.getRefineDesc());
      }
      studyModeVO.setBrowseMoneyageLocationRefineUrl(new SeoUrls().constructStudyModeURLForSR(request, pageURL, studyModeVO.getRefineTextKey(), qryString, searchCount, resultExists, rf, watSearch));
      newStudyModeList.add(studyModeVO);
    }
    return newStudyModeList;
  }
  
  public ArrayList customiseStudyLevelListForSR(String pageURL, ArrayList studyLevelList, HttpServletRequest request, String searchCount, String resultExists, String rf, String watSearch){
    Iterator itr = studyLevelList.iterator();
    ArrayList newStudyModeList = new ArrayList();
    String qryString = request.getQueryString();
    qryString = GenericValidator.isBlankOrNull(qryString)? "":qryString; 
    while(itr.hasNext()){
      RefineByOptionsVO studyLevelVO = (RefineByOptionsVO)itr.next();      
      studyLevelVO.setBrowseMoneyageLocationRefineUrl(new SeoUrls().constructStudyLevelURLForSR(request, pageURL, studyLevelVO.getRefineTextKey(), studyLevelVO.getBrowseCatTextKey(), qryString, searchCount, resultExists, rf, watSearch));
      newStudyModeList.add(studyLevelVO);
    }
    return newStudyModeList;
  }
  
  /*
  public ArrayList customiseAssessedListForSR(String pageURL, ArrayList assessmentList, HttpServletRequest request, String searchCount, String resultExists, String rf, String watSearch){
    Iterator itr = assessmentList.iterator();
    ArrayList newAssessmentList = new ArrayList();
    String qryString = request.getQueryString();
    qryString = GenericValidator.isBlankOrNull(qryString)? "":qryString; 
    while(itr.hasNext()){
      AssessedFilterVO assessedVO = (AssessedFilterVO)itr.next();
      if("HERB".equalsIgnoreCase(watSearch) && "Y".equalsIgnoreCase(assessedVO.getSelectedAssessment())){
        request.setAttribute("assessmentTypeDisplay", assessedVO.getAssessmentTypeName());
      }
      assessedVO.setBrowseMoneyAssessedRefineUrl(new SeoUrls().constructAssessmentURLForSR(request, pageURL, assessedVO.getAssessmentTypeName(), qryString, searchCount, resultExists, rf, watSearch));
      newAssessmentList.add(assessedVO);
    }
    return newAssessmentList;
  }
  */
  
   //Copied and pasted above method without "HERB" for assessment filter 23_Aug_2018, By Sabapathi.S
   public ArrayList customiseAssessedListForSR(String pageURL, ArrayList assessmentList, HttpServletRequest request, String searchCount, String resultExists, String rf, String watSearch){
     Iterator itr = assessmentList.iterator();
     ArrayList newAssessmentList = new ArrayList();
     String qryString = request.getQueryString();
     qryString = GenericValidator.isBlankOrNull(qryString)? "":qryString; 
     while(itr.hasNext()){
       AssessmentTypeVO assessedVO = (AssessmentTypeVO)itr.next();
       assessedVO.setBrowseMoneyAssessedRefineUrl(new SeoUrls().constructAssessmentURLForSR(request, pageURL, assessedVO.getUrlText(), qryString, searchCount, resultExists, rf, watSearch));
       newAssessmentList.add(assessedVO);
     }
     return newAssessmentList;
   }
  
  /*
  public ArrayList customiseReviewCatListForSR(String pageURL, ArrayList reviewCatList, HttpServletRequest request, String searchCount, String resultExists, String rf, String watSearch){
    Iterator itr = reviewCatList.iterator();
    ArrayList newAssessmentList = new ArrayList();
    String qryString = request.getQueryString();
    qryString = GenericValidator.isBlankOrNull(qryString)? "":qryString; 
    Map reviewCatMap = new HashMap();
    int subCount = 1;
    while(itr.hasNext()){
      ReviewCatFilterVO reviewCatVO = (ReviewCatFilterVO)itr.next(); 
      reviewCatMap.put(reviewCatVO.getReviewCatTextKey(), reviewCatVO.getReviewCatName());
      if("HERB".equalsIgnoreCase(watSearch) && "Y".equalsIgnoreCase(reviewCatVO.getReviewSelectText())){
        request.setAttribute("reviewCatStr_"+subCount, reviewCatVO.getReviewCatName());
        subCount ++;
      }
      reviewCatVO.setBrowseMoneyReviewCatRefineUrl(new SeoUrls().constructReviewCatURLForSR(request, pageURL, reviewCatVO.getReviewCatTextKey(), qryString, searchCount, resultExists, rf, watSearch));
      newAssessmentList.add(reviewCatVO);
    }
    request.setAttribute("reviewCatMap", reviewCatMap);
    return newAssessmentList;
  }
  */
  
   //Copied and pasted above method without "HERB" for preference filter 23_Aug_2018, By Sabapathi.S
   public ArrayList customiseReviewCatListForSR(String pageURL, ArrayList reviewCatList, HttpServletRequest request, String searchCount, String resultExists, String rf, String watSearch){
     Iterator itr = reviewCatList.iterator();
     ArrayList newAssessmentList = new ArrayList();
     String qryString = request.getQueryString();
     qryString = GenericValidator.isBlankOrNull(qryString)? "":qryString; 
     Map reviewCatMap = new HashMap();
     int subCount = 1;
     while(itr.hasNext()){
       ReviewCategoryVO reviewCatVO = (ReviewCategoryVO)itr.next(); 
       reviewCatMap.put(reviewCatVO.getUrlText(), reviewCatVO.getReviewCategoryDisplayName());
       reviewCatVO.setBrowseMoneyReviewCatRefineUrl(new SeoUrls().constructReviewCatURLForSR(request, pageURL, reviewCatVO.getUrlText(), qryString, searchCount, resultExists, rf, watSearch));
       newAssessmentList.add(reviewCatVO);
     }
     request.setAttribute("reviewCatMap", reviewCatMap);
     return newAssessmentList;
   }
  
  public ArrayList customiseSubjectListForSR(String pageURL, ArrayList subjectList, HttpServletRequest request, String searchCount, String resultExists, String rf, String watSearch){
    Iterator itr = subjectList.iterator();
    ArrayList newSubjectList = new ArrayList();
    String qryString = request.getQueryString();
    qryString = GenericValidator.isBlankOrNull(qryString)? "":qryString; 
    while(itr.hasNext()){
      SimilarStudyLevelCoursesVO subjectVO = (SimilarStudyLevelCoursesVO)itr.next();      
      subjectVO.setBrowseMoneyPageUrl(new SeoUrls().constructSubjectURLForSR(request, pageURL, subjectVO, qryString, searchCount, resultExists, rf, watSearch));
      newSubjectList.add(subjectVO);
    }
    return newSubjectList;
  }
  
   public ArrayList customiseLocationListForSR(String pageURL, ArrayList locationList, HttpServletRequest request, String searchCount, String resultExists, String rf, String watSearch){
    Iterator itr = locationList.iterator();
    ArrayList newLocationList = new ArrayList();
    String qryString = request.getQueryString();
    qryString = GenericValidator.isBlankOrNull(qryString)? "":qryString;
    if(!"HERB".equalsIgnoreCase(watSearch)){
      LocationRefineVO loctionVO = new LocationRefineVO();    
      loctionVO.setLocationURL(new SeoUrls().constructLocationURLForSR(request, pageURL, "", qryString, searchCount, resultExists, rf, watSearch));
      loctionVO.setRegionName("All locations");
      loctionVO.setTopRegionName("All locations");
      loctionVO.setLocationTextKey("All locations");
      loctionVO.setRegionTextKey("All locations");
      newLocationList.add(loctionVO);    
    }
    while(itr.hasNext()){
      LocationRefineVO locVO = (LocationRefineVO)itr.next();
      String regionName = locVO.getLocationTextKey();     
      locVO.setLocationURL(new SeoUrls().constructLocationURLForSR(request, pageURL, regionName, qryString, searchCount, resultExists, rf, watSearch));    
      newLocationList.add(locVO);
    }
    return newLocationList;
  }
  
  public void optimiseRefineListForSR(ArrayList refineList, String pageURL, HttpServletRequest request, String searchCount, String resultExists, String rf, String watSearch){
    Iterator itr = refineList.iterator();
    String qryString = request.getQueryString();    
    ArrayList campustypeList = new ArrayList();
    ArrayList russelList = new ArrayList();
    ArrayList locationList = new ArrayList();    
    qryString = GenericValidator.isBlankOrNull(qryString)? "":qryString;    
    String filterURL = "";
    String url = "";
    RefineByOptionsVO campusVO = new RefineByOptionsVO();
    RefineByOptionsVO russellVO = new RefineByOptionsVO();    
    campusVO.setRefineDesc("All campus types");      
    String urlArray[] = pageURL.split("/");
    String separator = "/";
    String newURL = "/" + urlArray[1] + separator + urlArray[2] + ("HERB".equalsIgnoreCase(watSearch) ? (separator + urlArray[3]) : ""); //
    String allCampusURL = new SeoUrls().constructUrlParameters(request, "", "campus-type", qryString, searchCount, resultExists, rf);        
    campusVO.setBrowseMoneyageLocationRefineUrl(newURL +  allCampusURL);
    campustypeList.add(campusVO);
    russellVO.setRefineDesc("All universities");
    String allRussellURL = new SeoUrls().constructUrlParameters(request, "", "russell-group", qryString, searchCount, resultExists, rf);
    russellVO.setBrowseMoneyageLocationRefineUrl(newURL +  allRussellURL);
    russelList.add(russellVO);
    Map locationMap = new HashMap();
    while(itr.hasNext()){
      RefineByOptionsVO campustypeVO = new RefineByOptionsVO();
      RefineByOptionsVO russelVO = new RefineByOptionsVO();
      RefineByOptionsVO locationVO = new RefineByOptionsVO();
      RefineByOptionsVO  refineVO = (RefineByOptionsVO)itr.next();
      if( ("UNIV_LOC_TYPE").equals(refineVO.getRefineOrder())){
        locationVO.setRefineCode(refineVO.getRefineCode());
        locationVO.setRefineDesc(refineVO.getRefineDesc());
        locationVO.setOptionTextKey(refineVO.getOptionTextKey());
        filterURL = new SeoUrls().constructUrlParameters(request, locationVO.getOptionTextKey(), "location-type", qryString, searchCount, resultExists, rf);
        
        url = newURL + filterURL;
        locationVO.setBrowseMoneyageLocationRefineUrl(url);
        locationList.add(locationVO);
        filterURL = "";
      } else if(("CAMPUS_BASED_UNIV").equals(refineVO.getRefineOrder())){
        campustypeVO.setRefineCode(refineVO.getRefineCode());
        campustypeVO.setRefineDesc(refineVO.getRefineDesc());    
        campustypeVO.setOptionTextKey(refineVO.getOptionTextKey());
        filterURL = new SeoUrls().constructUrlParameters(request, campustypeVO.getOptionTextKey(), "campus-type", qryString, searchCount, resultExists, rf);
        
        url = newURL + filterURL;
        campustypeVO.setBrowseMoneyageLocationRefineUrl(url);
        campustypeList.add(campustypeVO);
        filterURL = "";
      } else if(("RUSSELL_GROUP").equals(refineVO.getRefineOrder())){
        russelVO.setRefineCode(refineVO.getRefineCode());
        russelVO.setRefineDesc(refineVO.getRefineDesc());
        russelVO.setOptionTextKey(refineVO.getOptionTextKey());
        filterURL = new SeoUrls().constructUrlParameters(request, russelVO.getOptionTextKey(), "russell-group", qryString, searchCount, resultExists, rf);
        
        url = newURL + filterURL;
        russelVO.setBrowseMoneyageLocationRefineUrl(url);
        russelList.add(russelVO);
        filterURL ="";
      }   
    }
    locationMap.put("big-city", "Big City");
    locationMap.put("seaside", "Seaside");
    locationMap.put("town", "Town");
    locationMap.put("countryside", "Countryside");
    request.setAttribute("locationMap", locationMap);
    request.setAttribute("campustypeList", campustypeList);
    request.setAttribute("russelList", russelList);
    request.setAttribute("locationList", locationList);
  }
  
  
  public void assessmentFilterListForSR(ArrayList refineList, String pageURL, HttpServletRequest request, String searchCount, String resultExists, String rf, String watSearch){
    Iterator itr = refineList.iterator();
    String qryString = request.getQueryString();    
    ArrayList campustypeList = new ArrayList();
    ArrayList russelList = new ArrayList();
    ArrayList locationList = new ArrayList();
    ArrayList assessmentList = new ArrayList();
    qryString = GenericValidator.isBlankOrNull(qryString)? "":qryString;    
    String filterURL = "";
    String url = "";
    RefineByOptionsVO campusVO = new RefineByOptionsVO();
    RefineByOptionsVO russellVO = new RefineByOptionsVO();    
    AssessedFilterVO assessedFltVO = new AssessedFilterVO(); 
    campusVO.setRefineDesc("All campus types");      
    String urlArray[] = pageURL.split("/");
    String separator = "/";
    String newURL = "/" + urlArray[1] + separator + urlArray[2] + ("HERB".equalsIgnoreCase(watSearch) ? (separator + urlArray[3]) : ""); //
    String allCampusURL = new SeoUrls().constructUrlParameters(request, "", "campus-type", qryString, searchCount, resultExists, rf);        
    campusVO.setBrowseMoneyageLocationRefineUrl(newURL +  allCampusURL);
    campustypeList.add(campusVO);
    russellVO.setRefineDesc("All universities");
    String allRussellURL = new SeoUrls().constructUrlParameters(request, "", "russell-group", qryString, searchCount, resultExists, rf);
    russellVO.setBrowseMoneyageLocationRefineUrl(newURL +  allRussellURL);
    russelList.add(russellVO);
    Map locationMap = new HashMap();
    while(itr.hasNext()){
      RefineByOptionsVO campustypeVO = new RefineByOptionsVO();
      RefineByOptionsVO russelVO = new RefineByOptionsVO();
      RefineByOptionsVO locationVO = new RefineByOptionsVO();
      RefineByOptionsVO  refineVO = (RefineByOptionsVO)itr.next();
      if( ("UNIV_LOC_TYPE").equals(refineVO.getRefineOrder())){
        locationVO.setRefineCode(refineVO.getRefineCode());
        locationVO.setRefineDesc(refineVO.getRefineDesc());
        locationVO.setOptionTextKey(refineVO.getOptionTextKey());
        filterURL = new SeoUrls().constructUrlParameters(request, locationVO.getOptionTextKey(), "location-type", qryString, searchCount, resultExists, rf);
        
        url = newURL + filterURL;
        locationVO.setBrowseMoneyageLocationRefineUrl(url);
        locationList.add(locationVO);
        filterURL = "";
      } else if(("CAMPUS_BASED_UNIV").equals(refineVO.getRefineOrder())){
        campustypeVO.setRefineCode(refineVO.getRefineCode());
        campustypeVO.setRefineDesc(refineVO.getRefineDesc());    
        campustypeVO.setOptionTextKey(refineVO.getOptionTextKey());
        filterURL = new SeoUrls().constructUrlParameters(request, campustypeVO.getOptionTextKey(), "campus-type", qryString, searchCount, resultExists, rf);
        
        url = newURL + filterURL;
        campustypeVO.setBrowseMoneyageLocationRefineUrl(url);
        campustypeList.add(campustypeVO);
        filterURL = "";
      } else if(("RUSSELL_GROUP").equals(refineVO.getRefineOrder())){
        russelVO.setRefineCode(refineVO.getRefineCode());
        russelVO.setRefineDesc(refineVO.getRefineDesc());
        russelVO.setOptionTextKey(refineVO.getOptionTextKey());
        filterURL = new SeoUrls().constructUrlParameters(request, russelVO.getOptionTextKey(), "russell-group", qryString, searchCount, resultExists, rf);
        
        url = newURL + filterURL;
        russelVO.setBrowseMoneyageLocationRefineUrl(url);
        russelList.add(russelVO);
        filterURL ="";
      }   
    }
    locationMap.put("big-city", "Big City");
    locationMap.put("seaside", "Seaside");
    locationMap.put("town", "Town");
    locationMap.put("countryside", "Countryside");
    request.setAttribute("locationMap", locationMap);
    request.setAttribute("campustypeList", campustypeList);
    request.setAttribute("russelList", russelList);
    request.setAttribute("locationList", locationList);
  }
  
  
  
  
  //Added new method for generating StudyMode, StudyLevel, Subject, Location and RefineList for search filter for 27_Jan_2016, By Thiyagu G. End
  public ArrayList customisestudyLevelList(String pageURL, ArrayList studyLevelList, SearchBean searchBean, HttpServletRequest request, String searchType, String pagename){
    Iterator itr = studyLevelList.iterator();
    ArrayList newStudyModeList = new ArrayList();
    String filterparam = (String)request.getAttribute("queryStr");
    filterparam = GenericValidator.isBlankOrNull(filterparam)? "":filterparam;
    if(!GenericValidator.isBlankOrNull(filterparam)){
      if(("nd").equals(filterparam.substring(0,2))){
        filterparam = filterparam.substring(2);
      }
    }   
    while(itr.hasNext()){
      RefineByOptionsVO studyLevelVO = (RefineByOptionsVO)itr.next();
      if(searchType.equals("BROWSE")){
        studyLevelVO.setBrowseMoneyageLocationRefineUrl(new SeoUrls().constructStudyLevelURLFromBrowse(pageURL, studyLevelVO.getRefineDesc(),studyLevelVO.getRefineCode(), searchBean, studyLevelVO.getCategoryId(),studyLevelVO.getCategoryName(), filterparam, pagename));
      }else if(searchType.equals("KEYWORD")){
        studyLevelVO.setBrowseMoneyageLocationRefineUrl(new SeoUrls().constructStudyLevelURLFromKwd(pageURL, studyLevelVO , searchBean, filterparam, pagename));
      }
      newStudyModeList.add(studyLevelVO);
    }
    return newStudyModeList;
  }
  //
   public ArrayList customiseSubjectFromBrowseList(String pageURL, ArrayList subjectList, SearchBean searchBean, HttpServletRequest request, String searchType, String pagename){
     Iterator itr = subjectList.iterator();
     ArrayList newSubjectList = new ArrayList();
     String filterparam = (String)request.getAttribute("queryStr");
     filterparam = GenericValidator.isBlankOrNull(filterparam)?"":filterparam;
     if(!GenericValidator.isBlankOrNull(filterparam)){
       if(("nd").equals(filterparam.substring(0,2))){
         filterparam = filterparam.substring(2);
       }
     }
     while(itr.hasNext()){
       SimilarStudyLevelCoursesVO subjectVO = (SimilarStudyLevelCoursesVO)itr.next();
       if(searchType.equals("BROWSE")){
         subjectVO.setBrowseMoneyPageUrl(new SeoUrls().constructBrowseSubjectURL(subjectVO.getBrowseMoneyPageUrl(), searchBean.getRegionId(), filterparam, pagename));
       }
       if(searchType.equals("GRADE")){
         subjectVO.setBrowseMoneyPageUrl(new SeoUrls().constructKeywordSubjectURL(pageURL, subjectVO, searchBean.getRegionId(), filterparam, pagename));
       }
       newSubjectList.add(subjectVO);
     }
     return newSubjectList;
   }
  // 
  public ArrayList customiseProviderStudyModeResult(String urlString, ArrayList studyModeList, HttpServletRequest request, String courseMappingPath, String watSearch) {
    Iterator studyModeItr = studyModeList.iterator();    
    ArrayList newStudyModeList = new ArrayList();
    String filterparam = request.getQueryString();
    if(!"HERB".equalsIgnoreCase(watSearch)){
      RefineByOptionsVO allStudyModeRefineVO = new RefineByOptionsVO();    
      allStudyModeRefineVO.setFilterURL(new SeoUrls().constructStudyModeURLForSR(request, urlString, "", filterparam, "1", "Y", "n", watSearch));
      allStudyModeRefineVO.setRefineDesc("All study modes");
      newStudyModeList.add(allStudyModeRefineVO);
    }
    while (studyModeItr.hasNext()) {
      RefineByOptionsVO studyModeRefineVO = (RefineByOptionsVO)studyModeItr.next();
      if("HERB".equalsIgnoreCase(watSearch) && "Y".equalsIgnoreCase(studyModeRefineVO.getSelectedStudyMode())){
        request.setAttribute("studyModeDisplay", studyModeRefineVO.getRefineDesc());
      }
      studyModeRefineVO.setFilterURL(new SeoUrls().constructStudyModeURLForSR(request, urlString, studyModeRefineVO.getRefineTextKey(), filterparam, "1", "Y", "n", watSearch));
      newStudyModeList.add(studyModeRefineVO);
    }
    return newStudyModeList;
  }
  public ArrayList customiseProviderStudyModeUrlParameterised(String urlString, ArrayList studyModeList, HttpServletRequest request) {
    Iterator studyModeItr = studyModeList.iterator();
    //
    String newURL = "";
    String separator = "/";
    ArrayList newStudyModeList = new ArrayList();
    String filterparam = request.getQueryString();
    String urlArray[] = urlString.split("/");
    String collegeIdAndPageno = "";
    if (urlArray.length == 12) {
    while (studyModeItr.hasNext()) {     
        RefineByOptionsVO studyModeRefineVO = (RefineByOptionsVO)studyModeItr.next();
        collegeIdAndPageno = urlArray[4];
        if (collegeIdAndPageno.indexOf("-") > 0) {
          collegeIdAndPageno = collegeIdAndPageno.substring(0, collegeIdAndPageno.indexOf("-"));
        }
        newURL = "/degrees" + separator + urlArray[1] + separator + urlArray[2] + separator + urlArray[3] + separator + studyModeRefineVO.getRefineCode().toLowerCase() + separator + collegeIdAndPageno + "/csearch.html";
        
        newURL = newURL + "?nd" + appendFilterVlauesforProvder("smode", studyModeRefineVO.getRefineCode().toLowerCase(), request);
        studyModeRefineVO.setFilterURL(newURL);
        newStudyModeList.add(studyModeRefineVO);
      }
    }
    return newStudyModeList;
  }
  public ArrayList customiseProviderSubjectURL(String urlString, ArrayList subjectList, HttpServletRequest request, String watSearch) {
    Iterator subjectItr = subjectList.iterator();
    String newURL = "";
    String separator = "/";
    ArrayList newSubjectList = new ArrayList();
    String filterparam = request.getQueryString();
    String urlArray[] = urlString.split("/");
    String collegeIdAndPageno = "";
    CommonFunction common = new CommonFunction();
    String subjectText = "";   
    String filteredVal = "";
    while (subjectItr.hasNext()) {     
      SimilarStudyLevelCoursesVO subjectVO = (SimilarStudyLevelCoursesVO)subjectItr.next();
      subjectVO.setBrowseMoneyPageUrl(new SeoUrls().constructSubjectURLForSR(request, urlString, subjectVO, filterparam, "1", "Y", "n", watSearch));
      newSubjectList.add(subjectVO);
    }
    return newSubjectList;
  }
  //
   public String removeStudyModefromURL( HttpServletRequest request, String removeJacsFlag) {//3_JUN_2014
     String filter = "";
     String russellGroup = request.getParameter("rsl");
     russellGroup = !GenericValidator.isBlankOrNull(russellGroup)? russellGroup: "";
     String module = request.getParameter("mdl");
     module = !GenericValidator.isBlankOrNull(module)? module: "";
     String distance = request.getParameter("dist");
     distance = !GenericValidator.isBlankOrNull(distance)? distance: "";
     String postCode = request.getParameter("pc");
     postCode = !GenericValidator.isBlankOrNull(postCode)? postCode: "";
     String empRateMax = request.getParameter("e-rt-max");
     empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
     String empRateMin = request.getParameter("e-rt-min");
     empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
     String campusType = request.getParameter("cmps");
     campusType = !GenericValidator.isBlankOrNull(campusType)? campusType: "";
     String locType = request.getParameter("lc-tpe");
     locType = !GenericValidator.isBlankOrNull(locType)? locType: "";
     String ucasTarrifMax = request.getParameter("trf-max");
     ucasTarrifMax = !GenericValidator.isBlankOrNull(ucasTarrifMax)? ucasTarrifMax: "";
     String ucasTarrifMin = request.getParameter("trf-min");
     ucasTarrifMin = !GenericValidator.isBlankOrNull(ucasTarrifMin)? ucasTarrifMin: "";
     String sort = request.getParameter("sort");
     sort = !GenericValidator.isBlankOrNull(sort)? sort: "";
     String jacsCode = request.getParameter("jacs");//3_JUN_2014
     jacsCode = !GenericValidator.isBlankOrNull(jacsCode)? jacsCode: "";
     String isClearing = (String)request.getAttribute("searchClearing");//30_JUN_2015
            isClearing = !GenericValidator.isBlankOrNull(isClearing)? isClearing: "";  
         
     if("TRUE".equals(isClearing)){//30_JUN_2015
       filter += "&clearing";
     }  
     if (!GenericValidator.isBlankOrNull(russellGroup)) {
       filter = "&rsl=" + russellGroup;
     }
     if (!GenericValidator.isBlankOrNull(module)) {
       filter += "&mdl=" + module;
     }
     if (!GenericValidator.isBlankOrNull(distance)) {
       filter += "&dist=" + distance;
     }
     if (!GenericValidator.isBlankOrNull(postCode)) {
       filter += "&pc=" + postCode;
     }
     if (!GenericValidator.isBlankOrNull(empRateMax)) {
       filter += "&e-rt-max=" + empRateMax;
     }
     if (!GenericValidator.isBlankOrNull(empRateMin)) {
       filter += "&e-rt-min=" + empRateMin;
     }
     if (!GenericValidator.isBlankOrNull(campusType)) {
       filter += "&cmps=" + campusType;
     }
     if (!GenericValidator.isBlankOrNull(locType)) {
       filter += "&lc-tpe=" + locType;
     }
     if (!GenericValidator.isBlankOrNull(ucasTarrifMax)) {
       filter += "&trf-max=" + ucasTarrifMax;
     }
     if (!GenericValidator.isBlankOrNull(ucasTarrifMin)) {
       filter += "&trf-min=" + ucasTarrifMin;
     }
     if(GenericValidator.isBlankOrNull(removeJacsFlag)){//3_JUN_2014
       if (!GenericValidator.isBlankOrNull(jacsCode)) {
         filter += "&jacs=" + jacsCode;
       }
     }
     if (!GenericValidator.isBlankOrNull(sort)) {
       filter += "&sort=" + sort;
     }
     return filter;
   }
  
  //
  public String appendFilterVlauesforProvder(String filterType, String filterValue, HttpServletRequest request) {
    String filter = "";
    String russellGroup = request.getParameter("rsl");
    russellGroup = !GenericValidator.isBlankOrNull(russellGroup)? russellGroup: "";
    String module = request.getParameter("mdl");
    module = !GenericValidator.isBlankOrNull(module)? module: "";
    String distance = request.getParameter("dist");
    distance = !GenericValidator.isBlankOrNull(distance)? distance: "";
    String postCode = request.getParameter("pc");
    postCode = !GenericValidator.isBlankOrNull(postCode)? postCode: "";
    String empRateMax = request.getParameter("e-rt-max");
    empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
    String empRateMin = request.getParameter("e-rt-min");
    empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
    String campusType = request.getParameter("cmps");
    campusType = !GenericValidator.isBlankOrNull(campusType)? campusType: "";
    String locType = request.getParameter("lc-tpe");
    locType = !GenericValidator.isBlankOrNull(locType)? locType: "";
    String ucasTarrifMax = request.getParameter("trf-max");
    ucasTarrifMax = !GenericValidator.isBlankOrNull(ucasTarrifMax)? ucasTarrifMax: "";
    String ucasTarrifMin = request.getParameter("trf-min");
    ucasTarrifMin = !GenericValidator.isBlankOrNull(ucasTarrifMin)? ucasTarrifMin: "";
    String smode = request.getParameter("smode");
    smode = !GenericValidator.isBlankOrNull(smode)? smode: "";
    String sort = request.getParameter("sort");
    sort = !GenericValidator.isBlankOrNull(sort)? sort: "";
    String jacsCode = request.getParameter("jacs");//3_JUN_2014
    jacsCode = !GenericValidator.isBlankOrNull(jacsCode)? jacsCode: "";
    String isClearing = (String)request.getAttribute("searchClearing");//30_JUN_2015
           isClearing = !GenericValidator.isBlankOrNull(isClearing)? isClearing: "";  
        
    if("TRUE".equals(isClearing)){//30_JUN_2015
      filter += "&clearing";
    }  
    if ("smode".equals(filterType)) {
      if (!GenericValidator.isBlankOrNull(filterValue)) {
        filter += "&smode=" + filterValue;
      }
    } else {
      if (!GenericValidator.isBlankOrNull(smode)) {
        filter = "&smode=" + smode;
      }
    }
    if (!GenericValidator.isBlankOrNull(russellGroup)) {
      filter += "&rsl=" + russellGroup;
    }
    if (!GenericValidator.isBlankOrNull(module)) {
      filter += "&mdl=" + module;
    }
    if (!GenericValidator.isBlankOrNull(distance)) {
      filter += "&dist=" + distance;
    }
    if (!GenericValidator.isBlankOrNull(postCode)) {
      filter += "&pc=" + postCode;
    }
    if (!GenericValidator.isBlankOrNull(empRateMax)) {
      filter += "&e-rt-max=" + empRateMax;
    }
    if (!GenericValidator.isBlankOrNull(empRateMin)) {
      filter += "&e-rt-min=" + empRateMin;
    }
    if (!GenericValidator.isBlankOrNull(campusType)) {
      filter += "&cmps=" + campusType;
    }
    if (!GenericValidator.isBlankOrNull(locType)) {
      filter += "&lc-tpe=" + locType;
    }
    if (!GenericValidator.isBlankOrNull(ucasTarrifMax)) {
      filter += "&trf-max=" + ucasTarrifMax;
    }
    if (!GenericValidator.isBlankOrNull(ucasTarrifMin)) {
      filter += "&trf-min=" + ucasTarrifMin;
    }
    if (!"jacs".equals(filterType)) {//3_JUN_2014
    if (!GenericValidator.isBlankOrNull(jacsCode)) {
      filter += "&jacs=" + jacsCode;
    }
    }
    if (!GenericValidator.isBlankOrNull(sort)) {
      filter += "&sort=" + sort;
    }
    return filter;
  }
  
  public ArrayList customiseProviderStudyLevelURL(String urlString, ArrayList qualificationList, HttpServletRequest request, String watSearch) {
    Iterator qualificationItr = qualificationList.iterator();    
    ArrayList newQualificationList = new ArrayList();
    String filterparam = request.getQueryString();    
    CommonFunction common = new CommonFunction();    
    String stydyLevelDesc = null;
    while (qualificationItr.hasNext()) {
      RefineByOptionsVO locationRefineVO = (RefineByOptionsVO)qualificationItr.next();   
      stydyLevelDesc = locationRefineVO.getRefineDesc();
      if(!GenericValidator.isBlankOrNull(stydyLevelDesc)){
        stydyLevelDesc = stydyLevelDesc.toLowerCase();
        stydyLevelDesc = common.replaceHypen(common.replaceSpecialCharacter(stydyLevelDesc.toLowerCase()));
      }      
      locationRefineVO.setBrowseMoneyageLocationRefineUrl(new SeoUrls().constructStudyLevelURLForSR(request, urlString, locationRefineVO.getRefineTextKey(), locationRefineVO.getBrowseCatTextKey(), filterparam, "1", "Y", "n", watSearch));
      newQualificationList.add(locationRefineVO);
    }
    return newQualificationList;
  }
  
  public ArrayList customiseProviderAssessmentURL(String urlString, ArrayList assessmentList, HttpServletRequest request, String watSearch) {
    Iterator itr = assessmentList.iterator();    
    ArrayList newAssessmentList = new ArrayList();
    String filterparam = request.getQueryString();    
    while (itr.hasNext()) {
     AssessmentTypeVO assessedVO = (AssessmentTypeVO)itr.next();   
     assessedVO.setBrowseMoneyAssessedRefineUrl(new SeoUrls().constructAssessmentURLForSR(request, urlString, assessedVO.getUrlText(), filterparam, "1", "Y", "n", watSearch));
      newAssessmentList.add(assessedVO);
    }
    return newAssessmentList;
  }
  
  public ArrayList customiseProviderReviewCatURL(String urlString, ArrayList reviewCatList, HttpServletRequest request, String watSearch) {
    Iterator itr = reviewCatList.iterator();
    ArrayList newAssessmentList = new ArrayList();
    String qryString = request.getQueryString();
    qryString = GenericValidator.isBlankOrNull(qryString)? "":qryString; 
    Map reviewCatMap = new HashMap();
    int subCount = 1;
    while(itr.hasNext()){
      ReviewCatFilterVO reviewCatVO = (ReviewCatFilterVO)itr.next(); 
      reviewCatMap.put(reviewCatVO.getReviewCatTextKey(), reviewCatVO.getReviewCatName());
      if("HERB".equalsIgnoreCase(watSearch) && "Y".equalsIgnoreCase(reviewCatVO.getReviewSelectText())){
        request.setAttribute("reviewCatStr_"+subCount, reviewCatVO.getReviewCatName());
        subCount ++;
      }
      reviewCatVO.setBrowseMoneyReviewCatRefineUrl(new SeoUrls().constructReviewCatURLForSR(request, urlString, reviewCatVO.getReviewCatTextKey(), qryString, "1", "Y", "n", watSearch));
      newAssessmentList.add(reviewCatVO);
    }
    request.setAttribute("reviewCatMap", reviewCatMap);
    return newAssessmentList;
  }
  
  public String constructProviderNameURLRewrite(HttpServletRequest request, String providerName) {
    String qryString = request.getQueryString();
    String providerUrlRewrite = new SeoUrls().constructUrlParameters(request, providerName, "university", qryString, "1", "Y", "n");      
    return providerUrlRewrite;
  }
  
  public String checkDuplicateNode(String browseCatId, String qualId, String catcode){//5_AUG_2014
   String description = null;
   DataModel dataModel = new DataModel();
   Vector parameters = new Vector();
    parameters.add(browseCatId);    
    parameters.add(qualId);
    parameters.add(catcode);
    try {
      description = dataModel.getString("WU_UTIL_PKG.GET_CATEGORY_DESC_FN", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel = null;
      parameters.clear();
      parameters = null;
    }
   return description;   
  }
}//end of class