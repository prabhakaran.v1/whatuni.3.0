package WUI.utilities;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
  *
  * @CookieManager.java
  * @Version : 2.0
  * @www.whatuni.com
  * @Purpose  : This program is used to create and maintain cookie.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *  ?
  * 27.01.2016     Prabhakaran V.            1.1     Set the basket cookie max age                              WU_548
  * 27.07.2021	   Sangeeth.S				 1.2		 Added the max age for userTrackId 
  *  												 from old to new create cookie method(bug fix)
  * 10_NOV_2020    Sangeeth.S	  		     1.3     Added cookie consent flag check
  */
public class CookieManager {
  public CookieManager() {
  }

  /**
    *   This function is uead to create a cookie.
    * @param cookieName
    * @param cookieValue
    * @return Created cookie as Cookie
    */
    
  public Cookie createCookie(String cookieName, String cookieValue) {
    Cookie cookie = new Cookie(cookieName, cookieValue);
    cookie.setPath("/");
    if("basketId".equalsIgnoreCase(cookieName)){ 
      cookie.setMaxAge(GlobalConstants.WU_BASKET_COOKIE_MAX_AGE); //Set basket cookie max age 6 month added by Prabha
    }else if(GlobalConstants.USER_TRACK_ID_COOKIE.equalsIgnoreCase(cookieName)){ //Changed userTrackId from session to cookie for 24_Jan_2017, By Thiyagu G
      cookie.setMaxAge(Integer.parseInt(new CommonFunction().getWUSysVarValue("WU_COOKIE_TRACK_USER_MAX_AGE"))); //Set userid cookie max age 1 month added by Thiyagu G       
    }else if("cookie_splash_flag".equalsIgnoreCase(cookieName)){
       cookie.setMaxAge(GlobalConstants.WU_COOKIE_POPUP_COOKIE_AGE);     
    }else{
      cookie.setMaxAge(GlobalConstants.WU_COOKIE_MAX_AGE);
    }
    return cookie;
  }
  
  public static Cookie createCookie(HttpServletRequest request, String cookieName, String cookieValue) {
   //if(GlobalConstants.COOKIE_CONSENT_COOKIE.equalsIgnoreCase(cookieName) || CommonUtil.checkCookiePreference(request, cookieName)) {
   if(!GlobalConstants.COOKIE_CONSENT_COOKIE.equalsIgnoreCase(cookieName)) {
	 Cookie scookie = new Cookie(cookieName, cookieValue);        
     scookie.setPath("/; HttpOnly; Secure");
     if(GlobalConstants.CLEARING_USER_TYPE_COOKIE.equalsIgnoreCase(cookieName)){
       scookie.setMaxAge(GlobalConstants.WU_COOKIE_MAX_AGE);
     }else if("cookie_splash_flag".equalsIgnoreCase(cookieName)){
         scookie.setMaxAge(GlobalConstants.WU_COOKIE_POPUP_COOKIE_AGE);  
     }else if(GlobalConstants.WCACHE_COOKIE.equalsIgnoreCase(cookieName)){
    	 scookie.setMaxAge(GlobalConstants.WU_COOKIE_MAX_AGE);
     }else if("basketId".equalsIgnoreCase(cookieName)){ 
    	 scookie.setMaxAge(GlobalConstants.WU_BASKET_COOKIE_MAX_AGE); //Set basket cookie max age 6 month added by Prabha
     }else if(GlobalConstants.PROSPECTUS_BASKET_ID.equalsIgnoreCase(cookieName)){
    	 scookie.setMaxAge(GlobalConstants.WU_COOKIE_MAX_AGE);
     }else if(GlobalConstants.USER_TRACK_ID_COOKIE.equalsIgnoreCase(cookieName)){ //Changed userTrackId from session to cookie for 24_Jan_2017, By Thiyagu G
    	 scookie.setMaxAge(Integer.parseInt(new CommonFunction().getWUSysVarValue("WU_COOKIE_TRACK_USER_MAX_AGE"))); //Set userid cookie max age 1 month added by Thiyagu G       
     }else if(GlobalConstants.COOKIE_CONSENT_COOKIE.equalsIgnoreCase(cookieName)){ //Changed userTrackId from session to cookie for 24_Jan_2017, By Thiyagu G
    	 scookie.setMaxAge(GlobalConstants.COOKIE_CONSENT__COOKIE_MAX_AGE); //Set cookieconsent cookie max age 1 month      
     }     
     return scookie;
    }else{
    	Cookie cookie = new Cookie(cookieName, "");
    	cookie.setMaxAge(0);
    	cookie.setPath("/; HttpOnly; Secure");
    	return cookie;
    }    
  }
  /**
    *   This funtion is used to get the particular cookie value from Cookie
    * @param request
    * @param cookieName
    * @return Cookie Value as String.
    */
    
  public String getCookieValue(HttpServletRequest request, String cookieName) {
    Cookie cookie = getCookie(request, cookieName);
    String cookieValue = null;
    if (cookie != null) {
      cookieValue = cookie.getValue();
    }
    return cookieValue;
  }

  /**
    *   This function is used to set Max live time for Cookie.
    * @param request
    * @param cookieName
    * @return cookie as Cookie.
    */
    
  public Cookie setMaxCookieAge(HttpServletRequest request, String cookieName) {
    Cookie cookie = getCookie(request, cookieName);
    if (cookie != null) {
      cookie.setMaxAge(GlobalConstants.WU_COOKIE_MAX_AGE);
    }
    return cookie;
  }

  /**
    *   This function is used to get the particular cookie from Cookie.
    * @param request
    * @param cookieName
    * @return cookie as Cookie.
    */
    
  public Cookie getCookie(HttpServletRequest request, String cookieName) {
    Cookie[] cookies = request.getCookies();
    if (cookies != null && cookies.length > 0) {
      for (int i = 0; i < cookies.length; i++) {
        if (cookies[i].getName().equals(cookieName)) {
          return cookies[i];
        }
      }
    }
    return null;
  }
  
  public Cookie deleteCookie(HttpServletRequest request, String cookieName) {
    Cookie cookie = getCookie(request, cookieName);
    if (cookie != null) {
      cookie.setMaxAge(0);
      cookie.setPath("/");      
    }    
    return cookie;
  }    
}
