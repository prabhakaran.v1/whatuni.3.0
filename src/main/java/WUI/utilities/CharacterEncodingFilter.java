package WUI.utilities;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
  * Class         : CharacterEncodingFilter.java  *
  * Description   : Java Filter to set UTF-8 character on request and response of each HTTP Request.  *
  * @version      : 1.0
  *
  * Change Log :
  * **************************************************************************************************************
  * author	        Ver 	Created On      Modified On 	Modification Details 	Change
  * **************************************************************************************************************
  * Mohamed Syed 	1.0   05/Apr/2010     First draft
  *
  */
 
public class CharacterEncodingFilter implements Filter {   

	private FilterConfig filterConfig = null;  
  
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {  		
		
    request = (HttpServletRequest) request;        
		response = (HttpServletResponse) response;    
    
		//set the charecter encoding to UTF-8
    request.setCharacterEncoding("UTF-8");  
	  response.setContentType("text/html;charset=UTF-8");
		
		chain.doFilter(request, response);        
    
	  //do it again, since JSPs will set it to the default 		
		response.setContentType("text/html; charset=UTF-8");        
		request.setCharacterEncoding("UTF-8");    
    
	}
	
	public void init(FilterConfig filterConfig) {        		
		this.filterConfig = filterConfig;    
	}        
	
	public void destroy() {        		
		this.filterConfig = null;    
	}
}