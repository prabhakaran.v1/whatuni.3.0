package WUI.utilities;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mobile.util.MobileUtils;
import spring.valueobject.seo.SEOMetaDetailsVO;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import com.config.SpringContextInjector;
import com.itextpdf.text.pdf.codec.Base64;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.VersionChangesVO;
import com.wuni.util.valueobject.interstitialsearch.GardeFilterVO;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;

/**
  *
  * @CommonUtil.java
  * @Version : 2.0
  * @February 26 2007
  * @www.whatuni.com
  * @author By : Balraj. Selva Kumar
  * @Purpose  : This program is used to group all the utility functions such as string related and email validation
  *             (Most commonly used in many places).
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *10_NOV_2020         Sangeeth.S		     2.1	  Added cookie consent flag check methods        
  */
@Component
public class CommonUtil {
  public static String dbName = "grhotc";
  static {
    dbName = new CommonFunction().getSysVarValue("LIVE_DB");
  }
  
  public CommonUtil() {
  }
  static final int BUFF_SIZE = 1024;
  static final byte[] buffer = new byte[BUFF_SIZE];

  /**
   *  This function is used to tokenize the given input string and add all the tokens in to the vector.
   * @param inputText
   * @param delimiters
   * @return tokens as Vector.
   */
  public Vector getTokens(String inputText, String delimiters) {
    StringTokenizer st = new StringTokenizer(inputText, delimiters);
    Vector tokenVector = new Vector();
    while (st.hasMoreTokens()) {
      String data = (String)st.nextToken();
      tokenVector.add(data != null ? data.trim() : "");
    }
    return tokenVector;
  }

  public String versionChanges(String pageType) {
     String ver = "";
     try{
       ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
       VersionChangesVO versionChangesVO = new VersionChangesVO();
       Map versionMap = null;
       versionChangesVO.setAffliateId(GlobalConstants.WHATUNI_AFFILATE_ID);
       versionChangesVO.setPageType(pageType);
       if(versionMap == null) {
         versionMap = commonBusiness.versionChanges(versionChangesVO);    
       }
       ver = (String)versionMap.get("P_PAGE_URL");
     }catch(Exception e){
	 e.printStackTrace();
     }
    return ver;
  }
  /**
   * @since wu307_20110419
   * @param location
   * @return seoFriendlyLocationName
   */
  public String getSeoFriendlyLocationName(String location) {
   
    String seoFriendlyLocationName = "";
    if (location != null && location.trim().length() > 0) {
      String seoFriendlyLocationNameUpper = location.trim().toUpperCase();
     
      if (seoFriendlyLocationNameUpper.equals("ORKNEY ISLANDS")) {
        seoFriendlyLocationName = "Orkney";
      } else if (seoFriendlyLocationNameUpper.equals("BATH AND NORTH EAST SOMERSET")) {
        seoFriendlyLocationName = "Bath";
      } else if (seoFriendlyLocationNameUpper.equals("BLACKBURN WITH DARWEN")) {
        seoFriendlyLocationName = "Blackburn";
      } else if (seoFriendlyLocationNameUpper.equals("BRIGHTON & HOVE")) {
        seoFriendlyLocationName = "Brighton";
      } else if (seoFriendlyLocationNameUpper.equals("BUCKINGHAMSHIRE")) {
        seoFriendlyLocationName = "Buckingham";
      } else if (seoFriendlyLocationNameUpper.equals("CAMBRIDGESHIRE")) {
        seoFriendlyLocationName = "Cambridge";
      } else if (seoFriendlyLocationNameUpper.equals("CORNWALL AND ISLES OF SCILLY")) {
        seoFriendlyLocationName = "Cornwall";
      } else if (seoFriendlyLocationNameUpper.equals("NORTHAMPTONSHIRE")) {
        seoFriendlyLocationName = "Northampton";
      } else if (seoFriendlyLocationNameUpper.equals("ORKNEY ISLANDS")) {
        seoFriendlyLocationName = "Orkney";
      } else if (seoFriendlyLocationNameUpper.equals("OXFORDSHIRE")) {
        seoFriendlyLocationName = "Oxford";
      } else if (seoFriendlyLocationNameUpper.equals("PERTH & KINROSS")) {
        seoFriendlyLocationName = "Perth";
      } else if (seoFriendlyLocationNameUpper.equals("STAFFORDSHIRE")) {
        seoFriendlyLocationName = "Stafford";
      } else if (seoFriendlyLocationNameUpper.equals("WORCESTERSHIRE")) {
        seoFriendlyLocationName = "Worcester";
      } else {
        seoFriendlyLocationName = location.trim();
      }
    }
   
    return seoFriendlyLocationName;
  }

  /**
   * will return the main region for the given college.
   *
   * @param collegeId
   * @return
   * @autor Mohamed Syed
   * @since 2011-02-08__wu302
   */
  public String getCollegeRegion(String collegeId) {
    String collegeRegion = "";
    DataModel dataModel = new DataModel();
    //
    Vector parameters = new Vector();
    parameters.add(collegeId);
    //
    try {
      collegeRegion = dataModel.getString("WU_UTIL_PKG.GET_REGION_NAME_FN", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel = null;
      parameters.clear();
      parameters = null;
    }
    return collegeRegion;
  }

  /**
   * will fetch CPE relavant mapping value to a category.
   * @param parameters
   * @return
   * @author Mohamed Syed
   */
  public OmnitureProperties getCpeCategory(Vector parameters) {
    OmnitureProperties omnitureProperties = new OmnitureProperties();
    DataModel dataModel = new DataModel();
    ResultSet resultSet = null;
    try {
      resultSet = dataModel.getResultSet("WU_COMMON_INFO_PKG.GET_CPE_CATEGORY_FN", parameters);
      while (resultSet.next()) {
        omnitureProperties.setProb42_keyword(resultSet.getString("keyword"));
        omnitureProperties.setProb43_cpe_category(resultSet.getString("cpe_category"));
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return omnitureProperties;
  }

  /**
   * function will fetch Omniture's CPE related Properties.
   * @param parameters
   * @return
   * @author Mohamed Syed
   */
  public OmnitureProperties getOmnitureProperties(Vector parameters) {
    OmnitureProperties omnitureProperties = new OmnitureProperties();
    DataModel dataModel = new DataModel();
    ResultSet resultSet = null;
    try {
      resultSet = dataModel.getResultSet("WU_COMMON_INFO_PKG.GET_OMNITURE_PROPS_FN", parameters);
      while (resultSet.next()) {
        omnitureProperties.setProb42_keyword(resultSet.getString("keyword"));
        omnitureProperties.setProb43_cpe_category(resultSet.getString("cpe_category"));
        omnitureProperties.setProb44_location(resultSet.getString("location"));
        omnitureProperties.setProb45_spareSlot(resultSet.getString("spare_slot_value"));
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return omnitureProperties;
  }
  //  public String getOmnitureKeywordFromHotcourses(String categoryCode){
  //    String omnitureKeywordFromHotcourses = "";
  //    DataModel dataModel = new DataModel();
  //    Vector parameters = new Vector();
  //    parameters.add(categoryCode);
  //    try {
  //      omnitureKeywordFromHotcourses = dataModel.getString("WU_COMMON_INFO_PKG.GET_OMNITURE_KEYWORD_FN", parameters);
  //      System.out.println("omnitureKeywordFromHotcourses: " +omnitureKeywordFromHotcourses);
  //    } catch (Exception exception) {
  //      exception.printStackTrace();
  //    }    
  //    return omnitureKeywordFromHotcourses;
  //  }
  //  public String getSpareSlotValueForMoneyPage(String headerId, String pageno){
  //    String spareSlotvalueForMoneyPage = "";
  //    DataModel dataModel = new DataModel();
  //    Vector parameters = new Vector();
  //    parameters.add(headerId);
  //    parameters.add(pageno);
  //    try {
  //      spareSlotvalueForMoneyPage = dataModel.getString("WU_UTIL_PKG.GET_SPARE_SLOT_VALUE_FN", parameters);
  //    } catch (Exception exception) {
  //      exception.printStackTrace();
  //    }    
  //    return spareSlotvalueForMoneyPage;
  //  }

  /**
  * This method will delete an excisting image
  * @param imagePath
  * @return true/false
  * @throws Exception
  * @since 29-Oct-2008
  * @author Mohamed Syed.
  * @copyright www.hotcourses.com
  */
  public boolean isImageAvailable(String imagePath) throws Exception {
    boolean isAvailable = false;
    ResourceBundle geoBundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    String uploadPath = geoBundle.getString("log_file_path");
    String deletePath = uploadPath + imagePath;
    try {
      File imageFile = new File(deletePath);
      if (imageFile.exists()) {
        isAvailable = true;
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception(e);
    }
    return isAvailable;
  } //deleteExcistingImage()

  /**
   * This method will delete an excisting image
   * @param clientIpAsDecimal
   * @return affiliateIdByClientIp
   * @since 29-Oct-2008
   * @author Mohamed Syed.
   * @copyright www.hotcourses.com
   */
  public String getAffiliateIdByClientIp(String clientIpAsDecimal) {
    DataModel dataModel = new DataModel();
    ResultSet resultSet = null;
    Vector parameters = new Vector();
    parameters.add(clientIpAsDecimal);
    String affiliateIdByClientIp = "";
    //HashMap countyDetailsForGeoTarget = new HashMap(); 
    try {
      resultSet = dataModel.getResultSet("WU_UTIL_PKG.GET_AFILATE_ID", parameters);
      while (resultSet.next()) {
        //countyDetailsForGeoTarget.put("affiliateId",resultSet.getString("AFFILIATE_ID"));
        //countyDetailsForGeoTarget.put("countryCode2",resultSet.getString("COUNTRY_CODE2"));
        //countyDetailsForGeoTarget.put("countryCode2",resultSet.getString("COUNTRY_CODE3"));
        //countyDetailsForGeoTarget.put("countryName",resultSet.getString("COUNTRY_NAME"));        
        affiliateIdByClientIp = resultSet.getString("AFFILIATE_ID");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return affiliateIdByClientIp;
  }

  /**
   *
   * @param ipAddress
   * @return dec
   * @author Mohamed Syed
   */
  public String convertIpToDecimal(String ipAddress) {
    String IP;
    IP = ipAddress; // "203.197.128.203";//;"61.7.128.0";//sg"61.8.192.0"; //ipAddress;//
    StringTokenizer tokens = new StringTokenizer(IP, ".");
    int[] n = new int[4];
    String fullBin = "";
    for (int i = 0; i < 4; i++) {
      //if (tokens.hasMoreTokens())
      String t = tokens.nextToken();
      n[i] = Integer.parseInt(t);
      //else { showSyntax(); System.exit(1); }
    }
    String[] bin = new String[4];
    for (int i = 0; i < 4; i++) {
      bin[i] = Integer.toBinaryString(n[i]);
      while (bin[i].length() < 8)
        bin[i] = "0" + bin[i];
      fullBin += bin[i];
    }
    BigInteger dec = new BigInteger("0");
    for (int i = 0; i < fullBin.length(); i++) {
      BigInteger a = new BigInteger(fullBin.substring(i, i + 1));
      BigInteger b = new BigInteger("2").pow(fullBin.length() - i - 1);
      dec = dec.add(a.multiply(b));
    }
    return dec.toString();
  }

  /**
   *
   * @param request
   * @return RemoteIp as String
   * @author Mohamed Syed
   */
  public String getRemoteIp(HttpServletRequest request) {
    String clientIp = request.getHeader("CLIENTIP");
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    }
    return clientIp;
  }

  /**
    *  This function is uead to validate the given Date in desired format.
    * @param date
    * @param sysDateString
    * @param inDateFormat
    * @return valid date retrns true invalid date returns false
    */
  public boolean isValidDate(String date, String sysDateString, String inDateFormat) {
    int dateFormatLength = inDateFormat.length();
    boolean validDate = false;
    SimpleDateFormat sdf = new SimpleDateFormat(inDateFormat);
    try {
      if (date.length() != dateFormatLength) {
        return false;
      }
      if ((date != null && date.trim().length() > 0) && (sysDateString != null && sysDateString.trim().length() > 0)) {
        date = date.trim();
        validDate = Pattern.matches("^(([0-2]\\d|[3][0-1])\\/([0]\\d|[1][0-2])\\/[0-9]{4})$|^(([0-2]\\d|[3][0-1])\\/([0]\\d|[1][0-2])\\/[0-9]{4}}\\s([0-1]\\d|[2][0-3])\\:[0-5]\\d\\:[0-5]\\d)$", date.subSequence(0, date.length()));
      }
      if (validDate) {
        Date sysDate = sdf.parse(sysDateString);
        Date inputDate = sdf.parse(date);
        if (sysDate.compareTo(inputDate) < 0) {
          return false;
        }
        String[] input = date.split("/");
        if (input != null && input.length > 0) {
          int year = Integer.parseInt(input[2]);
          int leap = Integer.parseInt(input[2]) % 4;
          int month = Integer.parseInt(input[1]) - 1;
          int day = Integer.parseInt(input[0]);
          int[] lastDay = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
          if ((year > 0) && (month >= 0) && (day > 0)) {
            if ((leap == 0) && (month == 1)) {
              lastDay[month] = lastDay[month] + 1;
            }
            if (day <= lastDay[month]) {
              return true;
            }
          }
        }
      }
    } catch (Exception dateException) {
      dateException.printStackTrace();
      return false;
    }
    return false;
  }

  /**
    *  this funtion is uead to validate the given EMail address in exact Email format
    * @param email
    * @return valid email returns true, invalid email return false
    */
  public static boolean isValidEmail(String email) {
    boolean validEmail = true;
    final String specialChar = "!#$%&'*+-/=?^_`{|}~";
    final String text = "[a-zA-Z0-9" + specialChar + "]";
    final String atom = text + "+";
    final String dot = "\\." + atom;
    final String localPart = atom + "(" + dot + ")*";
    final String letter = "[a-zA-Z]";
    final String letDig = "[a-zA-Z0-9]";
    final String letDigHyp = "[a-zA-Z0-9-]";
    final String label = letDig + letDigHyp + "{0,61}" + letDig;
    final String domain = label + "(\\." + label + ")*\\." + letter + "{2,6}";
    final String addrSpec = "^" + localPart + "@" + domain + "$";
    final Pattern VALID_PATTERN = Pattern.compile(addrSpec);
    boolean flag = email.indexOf("_") != 0;
    validEmail = VALID_PATTERN.matcher(email).matches();
    if (validEmail && flag) {
      Pattern p = Pattern.compile("[^A-Za-z0-9\\.\\@_\\-]+");
      Matcher m = p.matcher(email);
      StringBuffer sb = new StringBuffer();
      boolean result = m.find();
      boolean deletedIllegalChars = false;
      while (result) {
        deletedIllegalChars = true;
        m.appendReplacement(sb, "");
        result = m.find();
      }
      m.appendTail(sb);
      email = sb.toString();
      if (deletedIllegalChars) {
        validEmail = false;
      }
      // Checking for continus dots and dot positions
      if (validEmail) {
        int w = email.indexOf("..");
        int x = email.indexOf(".", 0);
        int y = email.indexOf("@.");
        int z = email.indexOf(".@");
        if (w != -1) {
          validEmail = false;
        }
        if (x == 0) {
          validEmail = false;
        }
        if (y != -1) {
          validEmail = false;
        }
        if (z != -1) {
          validEmail = false;
        }
      }
    }
    return validEmail && flag ? validEmail : false;
  }

  /**
    *  This function is used to valdate the given input is a number.
    * @param inString
    * @return Returns true for number false for non numeric value.
    */
  public boolean isNumber(String inString) {
    boolean isNumber = true;
    if (inString == null || inString.trim().length() == 0 || inString.trim().equals("")) {
      return false;
    }
    String numStr = "0123456789";
    for (int i = 0; i < inString.toCharArray().length; i++) {
      int index = numStr.indexOf(inString.charAt(i));
      if (index < 0) {
        isNumber = false;
        break;
      }
      return isNumber;
    }
    return isNumber;
  }

  /**
    *  This function is used to valdate the given input is a float.
    * @param inString
    * @return Returns true for float false for non float value.
    */
  public boolean isFloat(String inString) {
    boolean isNumber = true;
    if (inString == null || inString.trim().length() == 0 || inString.trim().equals("")) {
      return false;
    }
    String numStr = "0123456789.";
    for (int i = 0; i < inString.toCharArray().length; i++) {
      int index = numStr.indexOf(inString.charAt(i));
      if (index < 0) {
        isNumber = false;
        break;
      }
      return isNumber;
    }
    return isNumber;
  }

  public String fetchCustomSubString(String original, String spliter, boolean removeInwards) throws Exception {
    if (original == null || spliter == null) {
      return "empty/null string passed in";
    }
    original = original.trim();
    spliter = spliter.trim();
    if (original.length() < 1 || spliter.length() < 1) {
      return "empty/null string passed in";
    }
    if (removeInwards) {
      original = (original.lastIndexOf(spliter) > -1) ? (original.substring(0, original.lastIndexOf(spliter))) : original;
    } else {
      original = (original.lastIndexOf(spliter) > -1) ? (original.substring(original.lastIndexOf(spliter))) : original;
    }
    return original;
  } //end of fetchCustomSubString()

  public String getFileExtension(String fileName) {
    if (fileName == null) {
      return "empty/null string passed in";
    }
    fileName = fileName.trim();
    fileName = (fileName.lastIndexOf(".") > -1) ? (fileName.substring(fileName.lastIndexOf("."))) : fileName;
    return fileName;
  } //end of getFileExtension()

  public String removeFileExtension(String fileName) {
    if (fileName == null || fileName.trim().length() < 1) {
      return "empty/null string passed in";
    }
    fileName = fileName.trim();
    fileName = (fileName.lastIndexOf(".") > -1) ? (fileName.substring(0, fileName.lastIndexOf("."))) : fileName;
    return fileName;
  } //end of removeFileExtension()     

  /**
    *  This function is used to find the length of the Video.
    * @param videoFilePath
    * @return Videolength as String.
    */
  public String getVideoPlayLength(String videoFilePath) {
    ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    String limeLightServerURL = bundle.getString("wuni.limelight.output.serverurl") != null ? bundle.getString("wuni.limelight.output.serverurl").trim() : "";
    String videoBitRate = bundle.getString("wuni.limelight.output.videobitrate");
    String audioBitRate = bundle.getString("wuni.limelight.output.audiobitrate");
    String limeLightFileURL = limeLightServerURL + videoFilePath;
    String videoLength = "";
    try {
      boolean status = true;
      if (status) {
        Thread.currentThread().sleep(10000);
        status = false;
      }
      URL url = new URL(limeLightFileURL);
      HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
      InputStream is = httpConnection.getInputStream();
      long size = 0;
      byte buffer[] = new byte[8192];
      int test = -1;
      while ((test = is.read(buffer)) != -1) {
        size = size + test;
      }
      is.close();
      httpConnection.disconnect();
      size = size / 1024;
      long playTime = (size) / (Integer.parseInt(audioBitRate) + Integer.parseInt(videoBitRate));
      playTime = playTime * 8;
      if (playTime > 60) {
        videoLength = ((playTime / 60) > 9) ? String.valueOf(playTime / 60) : "0" + String.valueOf(playTime / 60);
        videoLength += ((size % 60) > 9) ? ":" + String.valueOf(Math.round(playTime % 60)) : ":0" + String.valueOf(Math.round(playTime % 60));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return videoLength;
  }

  /**
    *  This function is used to upload a video to Limelight server.
    * @param request
    * @param form
    * @param uploadFile
    * @param fileName
    * @return Upload status as String.
    */
  public String uploadLimelightVideo(HttpServletRequest request, ActionForm form, FormFile uploadFile, String fileName) {
    String returnString = "";
    URLConnection conn = null;
    try {
      String LIMELIGHT_VIDEO_FOLDER = new CommonFunction().getWUSysVarValue("WU_LIMELIGHT_FOLDER");
      URL servlet = new URL("http://www.bvno.net/alp/tools/PostFile.aspx");
      conn = servlet.openConnection();
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.setUseCaches(false);
      String boundary = "---------------------------7d226f700d0";
      conn.setRequestProperty("Content-type", "multipart/form-data; boundary=" + boundary);
      conn.setRequestProperty("Cache-Control", "no-cache");
      DataOutputStream out = new DataOutputStream(conn.getOutputStream());
      out.writeBytes("--" + boundary + "\r\n");
      //Passing Form Parameter
      writeParam("CustomerID", "964", out, boundary); //Limelight CustomerID
      writeParam("Profile", "6534", out, boundary); //LimeLight ProfileID
      //writeParam("Category�", "WHATUNI", out, boundary);     //Category for gropuping
      writeParam("FileNameToUse", LIMELIGHT_VIDEO_FOLDER + fileName, out, boundary); //LimeLight Video Display Name
      // Passing Video File
      writeFile("File1", uploadFile, out, boundary); //Uploaded Video File Name
      out.flush();
      out.close();
      // To get the response data from LimeLight Networks
      InputStream stream = conn.getInputStream();
      BufferedInputStream in = new BufferedInputStream(stream);
      int i = 0;
      while ((i = in.read()) != -1) {
        returnString = returnString + (char)i;
      }
      if (returnString != null && returnString.trim().length() > 0) {
        conn = null;
        request.setAttribute("responseXML", returnString);
      }
      in.close();
    } catch (Exception videoException) {
      conn = null;
      videoException.printStackTrace();
    }
    return returnString;
  }

  /**
    *  This function is uead to write the value to the Outputstream
    * @param name
    * @param value
    * @param out
    * @param boundary
    * @return none
    * @throws Exception
    */
  public void writeParam(String name, String value, DataOutputStream out, String boundary) throws Exception {
    try {
      out.writeBytes("content-disposition: form-data; name=\"" + name + "\"\r\n\r\n");
      out.writeBytes(value);
      out.writeBytes("\r\n" + "--" + boundary + "\r\n");
    } catch (Exception paramException) {
      throw paramException;
    }
  }

  /**
    *  This function is used to write the file content in to the Output stream.
    * @param name
    * @param filePath
    * @param out
    * @param boundary
    * @return none
    * @throws Exception
    */
  public void writeFile(String name, FormFile filePath, DataOutputStream out, String boundary) throws Exception {
    try {
      out.writeBytes("content-disposition: form-data; name=\"" + name + "\"; filename=\"" + filePath + "\"\r\n");
      out.writeBytes("content-type: application/octet-stream" + "\r\n\r\n");
      InputStream fileInputStream = filePath.getInputStream();
      while (true) {
        synchronized (buffer) {
          int amountRead = fileInputStream.read(buffer);
          if (amountRead == -1) {
            break;
          }
          out.write(buffer, 0, amountRead);
        }
      }
      fileInputStream.close();
      out.writeBytes("\r\n" + "--" + boundary + "\r\n");
    } catch (Exception fileException) {
      throw fileException;
    }
  }

  /**
    *  This function is uead to change the give string into Initcaps.
    * @param inString
    * @return Initcaps value as String
    */
  public String toTitleCase(String inString) {
    try {
      StringBuffer stringBuffer = new StringBuffer();
      inString = inString != null ? inString.toLowerCase() : inString;
      StringTokenizer strTitleCase = new StringTokenizer(inString);
      while (strTitleCase.hasMoreTokens()) {
        String testString = strTitleCase.nextToken();
        stringBuffer.append(testString.replaceFirst(testString.substring(0, 1), testString.substring(0, 1).toUpperCase()) + " ");
      }
      String returnString = stringBuffer.toString();
      int strlength = returnString != null ? returnString.trim().length() : 0;
      String str[] = new String[] { " mba", " Mba", " it", " It" };
      for (int var = 0; var < str.length; var++) {
        if (returnString != null) {
          if ((strlength == str[var].trim().length()) && (returnString.trim().equalsIgnoreCase(str[var].trim()))) {
            returnString = returnString.toUpperCase();
          } else {
            returnString = returnString.indexOf(str[var]) > 0 ? returnString.replaceAll(str[var], str[var].toUpperCase()) : returnString;
          }
        }
      }
      return returnString;
    } catch (Exception exception) {
      return inString;
    }
  }

  /**
    *  This function is used to format the given date.
    * @param inDate
    * @param entityFlag
    * @return Formatted date as String.
    */
  public String getFormattedDate(String inDate, String entityFlag) {
    String myFormatedDate = null;
    try {
      if (inDate != null) {
        DateFormat d1 = new SimpleDateFormat("dd/MM/yyyy");
        Date locdate = (Date)d1.parse(inDate);
        if (entityFlag.equalsIgnoreCase("EDATE")) {
          String inArray[] = new String[3];
          inArray = inDate.split("/");
          int days = Integer.parseInt(inArray[0]);
          int months = Integer.parseInt(inArray[1]);
          int year = Integer.parseInt(inArray[2]);
          if (days == 1) {
            if (months == 1) {
              months = 12;
              year = year - 1;
            } else {
              months = months - 1;
            }
            if (months == 2) {
              days = ((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28;
            } else if (months == 4 || months == 6 || months == 9 || months == 11) {
              days = 30;
            } else {
              days = 31;
            }
          } else {
            days = days - 1;
          }
          locdate = (Date)d1.parse(days + "/" + months + "/" + year);
        }
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE, dd MMMM yyyy");
        myFormatedDate = formatter.format(locdate);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return myFormatedDate;
  }

  /**
    *  This function is used to get the music voucher from www.7digital.com(For user registration offer).
    * @param userId
    * @param request
    * @return Music voucher code as String
    */
  public String getMusicVocher(String userId, HttpServletRequest request) {
    String voucherCode = "";
    // the below part commented for 19th Jan 2010 Release, request by D.W.
    /*String returnString = "";
    URLConnection conn = null;
    String voucherCode = "";
    String userEmail = null;
    try {
      HttpSession session = request.getSession();
      ArrayList questionlist = (ArrayList)session.getAttribute("DynamicQuestionList");
      ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
      String clientId = new CommonFunction().getWUSysVarValue("WU_DIG_MUSIC_USER");
      String clientPassword = new CommonFunction().getWUSysVarValue("WU_DIG_MUSIC_PASS");
      String campaignCode = new CommonFunction().getWUSysVarValue("WU_DIG_MUSIC_CCODE");
      String musicUrl = bundle.getString("wuni.message.music.url");
      // to get the user email address
      if (questionlist != null) {
        Iterator questioniterator = questionlist.iterator();
        RegistrationBean questionBean = null;
        while (questioniterator.hasNext()) {
          questionBean = new RegistrationBean();
          questionBean = (RegistrationBean)questioniterator.next();
          if (questionBean.getQuestionLabel() != null && questionBean.getQuestionLabel().equalsIgnoreCase("Email address")) {
            userEmail = questionBean.getSelectedValue();
          }
        }
      }
      String url = musicUrl + "clientId=" + clientId + "&clientPassword=" + clientPassword + "&campaignCode=" + campaignCode + "&bookingReference=Whatuni" + userId + "&userEmailAddress=" + userEmail;
      URL servlet = new URL(url);
      conn = servlet.openConnection();
      conn.setDoOutput(true);
      conn.setDoInput(true);
      conn.setUseCaches(false);
      // To get the response data from Music download sites
      InputStream stream = conn.getInputStream();
      BufferedInputStream in = new BufferedInputStream(stream);
      int i = 0;
      while ((i = in.read()) != -1) {
        returnString = returnString + (char)i;
      }
      if (returnString != null && returnString.trim().length() > 0) {
        conn = null;
        voucherCode = new domParser().getBodyContent(returnString, "<VoucherCode>", "</VoucherCode>");
      }
      in.close();
    } catch (Exception exception) {
      conn = null;
      exception.printStackTrace();
    }*/
    return voucherCode;
  }

  /**
    *  This function is used to format the given number.
    * @param inNumber
    * @return Formatted number as String.
    */
  public String changeNumberFormat(String inNumber) {
    String outNumber = "";
    if (inNumber != null && inNumber.trim().length() > 0) {
      try {
        NumberFormat convert = NumberFormat.getInstance();
        outNumber = convert.format(Integer.parseInt(inNumber));
      } catch (Exception exception) {
        outNumber = inNumber;
        exception.printStackTrace();
      }
    }
    return outNumber;
  }

  /**
    *  This function is used to change the given string into uppercase.
    * @param inString
    * @return Uppercase as String.
    */
  public String toUpperCase(String inString) {
    String outString = inString;
    try {
      if (inString != null && !inString.equalsIgnoreCase("null") && inString.trim().length() > 0) {
        outString = inString.toUpperCase();
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return outString;
  }

  /**
    *  This function is used to change the given string into lowercase.
    * @param inString
    * @return Lowercase as String.
    */
  public String toLowerCase(String inString) {
    String outString = inString;
    try {
      if (inString != null && !inString.equalsIgnoreCase("null") && inString.trim().length() > 0) {
        outString = inString.toLowerCase();
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return outString;
  }

  /**
    *  This function is used to change the given string into initcaps String.
    * @param inString
    * @return Init caps as String
    */
  public String titleCase(String inString) {
    String result = "";
    try {
      if (inString != null && !inString.equalsIgnoreCase("null") && inString.trim().length() > 0) {
        for (int i = 0; i < inString.length(); i++) {
          String next = inString.substring(i, i + 1);
          if (i == 0) {
            result += next.toUpperCase();
          } else {
            result += next.toLowerCase();
          }
        }
      }
    } catch (Exception exception) {
      return result;
    }
    return (result != null && !result.equalsIgnoreCase("null") && result.trim().length() > 0 ? result : inString);
  }

  /**
    *  This function is uead to load the Application Properties file as ResourceBundle
    * @return Properties file as ResourceBundle.
    */
  public static ResourceBundle getProperties() {
    ResourceBundle bundle = null;
    try {
      bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return bundle;
  }

  /**
    *  This funtion is used to generate the random number between starting & ending limit.
    * @param start
    * @param end
    * @return Random number as int.
    */
  public int generateRandomNumber(int start, int end) {
    int rawRandomNumber;
    int min = start;
    int max = end;
    rawRandomNumber = (int)(Math.random() * (max - min + 1)) + min;
    return rawRandomNumber;
  }

  /**
   *  function that returns the hypenated word for the given word spliting with given length
   * @param stext as String
   * @param position as String
   * @return  hypenatedWord as String
   */
  public String toHyphenationWord(String stext, int position) {
    if (stext != null) {
      StringBuffer str = new StringBuffer();
      int loopcount = stext.length() / position + ((stext.length() / (stext.length() / position)) > 0 ? 1 : 0);
      int start = 0;
      int end = position;
      for (int i = 0; i < loopcount; i++) {
        str.append(stext.substring(start, end) + "\n-");
        start = end;
        if (end + position > stext.length()) {
          end = stext.length();
        } else {
          end = end + position;
        }
      }
      str.append(stext.substring(start));
      String returnString = str.toString();
      returnString = returnString.endsWith("\n-") ? returnString.substring(0, returnString.length() - 3) : returnString;
      returnString = returnString != null ? returnString.replaceAll("- ", "-") : returnString;
      return returnString;
    } else {
      return stext;
    }
  }

  /**
   *  function that returns the hypenated word for the given word spliting with given length
   * @return dbname as String
   */
  public String getDbName() {
    if (dbName != null && dbName.length() == 0) {
      dbName = new CommonFunction().getSysVarValue("LIVE_DB");
    }
    return dbName;
  }

  /**
   * will return the number with TH/ND/ST/RD
   * for example 1st, 2nd, 3rd, 4th
   * @param inputNumber
   * @return
   */
  public String getSuperStringSTNDRDTH(String inputNumber) {
    String superString = "NaN";
    if (isDigit(inputNumber)) {
      superString = "";
      int len = inputNumber.length();
      String lastDigit = inputNumber.substring(len - 1, len);
      String lastPreviousDigit = "-1";
      if (len > 1) {
        lastPreviousDigit = inputNumber.substring(len - 2, len - 1);
      }
      int lastDigitInt = Integer.parseInt(lastDigit);
      int lastPreviousDigitInt = Integer.parseInt(lastPreviousDigit);
      if (lastDigitInt == 1 && lastPreviousDigitInt != 1 && lastPreviousDigitInt < 10) {
        superString = "st";
      } else if (lastDigitInt == 2 && lastPreviousDigitInt != 1 && lastPreviousDigitInt < 10) {
        superString = "nd";
      } else if (lastDigitInt == 3 && lastPreviousDigitInt != 1 && lastPreviousDigitInt < 10) {
        superString = "rd";
      } else if (lastDigitInt >= 4 || lastDigitInt == 0 || lastPreviousDigitInt == 1) {
        superString = "th";
      }
    }
    return superString;
  } //end getSuperStringSTNDRDTH

  public String removeSpecialCharAndConstructSeoText(String inString) {
    if (inString != null) {
      inString = inString.trim();
      inString = inString.replaceAll("&", "and");
      inString = inString.replaceAll("/", "or");
      inString = inString.replaceAll(",", " ");
      inString = inString.replaceAll(" ", "-");
      inString = inString.replaceAll("-+", "-");
      inString = inString.replaceAll("'", "");
      inString = inString.toLowerCase();
    }
    return inString;
  }

  /**
   * checking given number is numbe or not
   * @param inputNumber
   * @return
   */
  public boolean isDigit(String inputNumber) {
    if (inputNumber.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")) {
      return true;
    } else {
      return false;
    }
  } //end of isDigit()

  /**
   * will return tickertape values
   * @return
   * @throws Exception
   * @author Mohamed Syed
   */
  public ArrayList getTickerTapes() throws Exception {
    ArrayList listOfTickerTapes = new ArrayList();
    DataModel dataModel = new DataModel();
    ResultSet resultSet = null;
    Vector parameters = new Vector();
    TickerTapeBean tickerBean = null;
    String externalUrl = "";
    try {
      parameters.add("220703");
      resultSet = dataModel.getResultSet("WU_UTIL_PKG.GET_TICKER_TAPES", parameters);
      while (resultSet.next()) {
        tickerBean = new TickerTapeBean();
        tickerBean.setTickerName(resultSet.getString("TICKER_NAME"));
        tickerBean.setTickerCollegeId(resultSet.getString("COLLEGE_ID"));
        tickerBean.setTickerDescription(resultSet.getString("DESCRIPTION"));
        externalUrl = resultSet.getString("DESTINATION_URL");
        //Added WHATUNI_SCHEME_NAME by Indumathi Mar-29-16
        if(externalUrl.indexOf("whatuni.com") > -1) {
          externalUrl = (externalUrl.indexOf("https://") >= 0) ? externalUrl.replace("https://", GlobalConstants.WHATUNI_SCHEME_NAME) : (externalUrl.indexOf("http://") >= 0) ? externalUrl.replace("http://", GlobalConstants.WHATUNI_SCHEME_NAME) : GlobalConstants.WHATUNI_SCHEME_NAME + externalUrl;
        } else {          
          externalUrl = (externalUrl.indexOf("http://") >= 0 || externalUrl.indexOf("https://") >= 0) ? externalUrl : "http://" + externalUrl;
        }
        tickerBean.setTickerExternalUrl(externalUrl);
        listOfTickerTapes.add(tickerBean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
      dataModel = null;
      resultSet = null;
      parameters.clear();
      parameters = null;
      tickerBean = null;
    }
    return listOfTickerTapes;
  } //end of getTickerTapes()

  public String replaceSpaceByHypen(String inString) {
    if (inString != null && inString.trim().length() > 0) {
      inString = inString.replaceAll("     ", "-"); //five spaces
      inString = inString.replaceAll("    ", "-"); //four spaces
      inString = inString.replaceAll("   ", "-"); //three spaces
      inString = inString.replaceAll("  ", "-"); //two spaces
      inString = inString.replaceAll(" ", "-"); //one space    
      inString = inString.replaceAll("-----", "-"); //five hyphen
      inString = inString.replaceAll("----", "-"); //four hyphen
      inString = inString.replaceAll("---", "-"); //three hyphen
      inString = inString.replaceAll("--", "-"); //two hyphen      
    }
    return inString;
  }

  public String getThumbnail(String fileName, String thumbnailVersion) {
    String thumbnail = fileName;
    if (thumbnail != null && thumbnail.trim().length() > 0) {
      CommonUtil comUtil = new CommonUtil();
      String fileExtension = comUtil.getFileExtension(thumbnail);
      thumbnail = comUtil.removeFileExtension(thumbnail);
      if (!GenericValidator.isBlankOrNull(fileExtension) && (fileExtension.equals(".flv") || fileExtension.equals(".mp4"))) {
        thumbnail = thumbnail + thumbnailVersion + ".jpg";
      } else {
        thumbnail = thumbnail + thumbnailVersion + fileExtension;
      }
      //nullify unused objects
      comUtil = null;
    }
    return thumbnail;
  }


    /**
     * will return domain and CDN name
     * @author Sekhar Kasala 
     */
     public static String getCSSPath()
     {
       ResourceBundle resourcebundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
       if(dbName!= null && dbName.equalsIgnoreCase("grdom")){
          return GlobalConstants.WHATUNI_SCHEME_NAME+resourcebundle.getString("wuni.cssdomain")+resourcebundle.getString("wuni.con.path");
          //return GlobalConstants.WHATUNI_SCHEME_NAME+resourcebundle.getString("wuni.live.domain")+resourcebundle.getString("wuni.con.path");
        }else if(dbName != null && dbName.equalsIgnoreCase("gs2fo")){
          return GlobalConstants.WHATUNI_SCHEME_NAME+resourcebundle.getString("wuni.cssdomain")+resourcebundle.getString("wuni.con.path");
          //return GlobalConstants.WHATUNI_SCHEME_NAME+resourcebundle.getString("wuni.failover.cssdomain")+resourcebundle.getString("wuni.con.path"); //TODO uncomment this line and comment the below line while for normal domain.
          //return GlobalConstants.WHATUNI_SCHEME_NAME+resourcebundle.getString("wuni.failover.domain")+resourcebundle.getString("wuni.con.path");          
        }else if(dbName != null && dbName.equalsIgnoreCase("domtest")){
          //return GlobalConstants.WHATUNI_SCHEME_NAME+resourcebundle.getString("wuni.cssdomain")+resourcebundle.getString("wuni.con.path"); //TODO uncomment this line and comment the below line while for normal domain.
          return GlobalConstants.WHATUNI_SCHEME_NAME+resourcebundle.getString("wuni.mobiletest.domain")+resourcebundle.getString("wuni.con.path");
        }else{            
          //return GlobalConstants.WHATUNI_SCHEME_NAME+resourcebundle.getString("wuni.cssdomain")+resourcebundle.getString("wuni.con.path"); //TODO uncomment this line and comment the below line while for normal domain.
          return GlobalConstants.WHATUNI_SCHEME_NAME+resourcebundle.getString("wuni.mobiledev.domain")+resourcebundle.getString("wuni.con.path");
        }
       //return "/degrees";            //TODO Comment while deploying in live and uncomment above lines.

     }

/**
 * will return thd domain and CDN name path
 * 
 */
    public static String getJsPath(){
       
       ResourceBundle resourceBundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
        if(dbName != null && dbName.equalsIgnoreCase("grdom")){
             return GlobalConstants.WHATUNI_SCHEME_NAME+resourceBundle.getString("wuni.jsdomain")+resourceBundle.getString("wuni.con.path");//TODO uncomment this line and comment the below line while for normal domain.
            //return GlobalConstants.WHATUNI_SCHEME_NAME+resourceBundle.getString("wuni.live.domain")+resourceBundle.getString("wuni.con.path");
        }else if(dbName != null && dbName.equalsIgnoreCase("gs2fo")){
            return GlobalConstants.WHATUNI_SCHEME_NAME+resourceBundle.getString("wuni.jsdomain")+resourceBundle.getString("wuni.con.path");//TODO uncomment this line and comment the below line while for normal domain.
          //return GlobalConstants.WHATUNI_SCHEME_NAME+resourceBundle.getString("wuni.failover.domain")+resourceBundle.getString("wuni.con.path");
          //return GlobalConstants.WHATUNI_SCHEME_NAME+resourceBundle.getString("wuni.failover.jsdomain")+resourceBundle.getString("wuni.con.path");//TODO uncomment this line and comment the below line while for normal domain.
        }else if(dbName != null && dbName.equalsIgnoreCase("domtest")){
          //return GlobalConstants.WHATUNI_SCHEME_NAME+resourceBundle.getString("wuni.jsdomain")+resourceBundle.getString("wuni.con.path");//TODO uncomment this line and comment the below line while for normal domain.
          return GlobalConstants.WHATUNI_SCHEME_NAME+resourceBundle.getString("wuni.mobiletest.domain")+resourceBundle.getString("wuni.con.path");
        }else{
          //return GlobalConstants.WHATUNI_SCHEME_NAME+resourceBundle.getString("wuni.jsdomain")+resourceBundle.getString("wuni.con.path"); //TODO uncomment this line and comment the below line while for normal domain.
          return GlobalConstants.WHATUNI_SCHEME_NAME+resourceBundle.getString("wuni.mobiledev.domain")+resourceBundle.getString("wuni.con.path");
        }
        
      //return "/degrees";    //TODO Comment while deploying in live and uncomment above lines.
    }
    
    /**
     * Added common method for forming domain path in whatunigo journey
     * @param request
     * @return
     */
      public static String getWugoDomain(HttpServletRequest request){
        String domainPath = GlobalConstants.WU_WEB_SERVICE_SCHEME_NAME + request.getServerName();
        domainPath = domainPath + GlobalConstants.WUGO_CONTEXT_PATH;
        //domainPath = domainPath + ":7001" + GlobalConstants.WUGO_CONTEXT_PATH; //TODO Comment while deploying in live and uncomment above lines.
        //System.out.println("domainPath : "+domainPath);
        return domainPath;
      }
    
     /**
     * Added by Sekhar Kasala for 17-01-2011 release
     * Method will return the image path
     * @param imagesrc, imgNum
     * * @return Strig
     * Change Log : Modified number of domain is 8 by Prabha on 23_Mar_2018
     */  
     public static String getImgPath(String imagesrc,int imgNum)//String imgNum
     {
     int domainPath = 0;
     int dc = 8; //number of domains
     String imgpath = null;
     try{
      if(imgNum > 0)//(imgNum!=null)
      {
      //int i=Integer.parseInt("imgNum");
       int i=imgNum;
      domainPath = (i>dc)?((i%dc==0)?((i%dc)+dc):(i%dc)):i;
      }
      else
      {
        Random rnd = new Random();
        int i = rnd.nextInt(dc) +1 ;
        domainPath = i;//(i>dc)?((i%dc==0)?((i%dc)+dc):(i%dc)):i;
      }
      String objPath=Integer.toString(domainPath); 
      Object[] obj ={objPath};      
      if(dbName != null && dbName.equalsIgnoreCase("grdom")){
          //imgpath = CommonUtil.getResourceMessage("wuni.imagedomain", obj)+imagesrc; 
         //imgpath = CommonUtil.getResourceMessage("wuni.live.domain", obj)+imagesrc;//TODO uncomment this line and comment the below line while for normal domain.
         imgpath = "images1.whatuni.com" + imagesrc;
      }else if(dbName != null && dbName.equalsIgnoreCase("gs2fo")){
          //imgpath = CommonUtil.getResourceMessage("wuni.imagedomain", obj)+imagesrc; 
        //imgpath = CommonUtil.getResourceMessage("wuni.failover.domain", obj)+imagesrc;
        //imgpath = CommonUtil.getResourceMessage("wuni.failover.imagedomain", obj)+imagesrc; //TODO uncomment this line and comment the below line while for normal domain.
         imgpath = "images1.whatuni.com" + imagesrc;
      }else if(dbName != null && dbName.equalsIgnoreCase("domtest")){
        //imgpath = CommonUtil.getResourceMessage("wuni.imagedomain", obj)+imagesrc; //TODO uncomment this line and comment the below line while for normal domain.
        imgpath = CommonUtil.getResourceMessage("wuni.mobiletest.domain", obj)+imagesrc;
      //imgpath = "images1.whatuni.com" + imagesrc;
      }else{
        //imgpath = CommonUtil.getResourceMessage("wuni.imagedomain", obj)+imagesrc; //TODO uncomment this line and comment the below line while for normal domain.
        imgpath = CommonUtil.getResourceMessage("wuni.mobiledev.domain", obj)+imagesrc;
      }
      //imagepath = "/degrees";     //TODO comment while deploying in live.        
      imgpath=GlobalConstants.WHATUNI_SCHEME_NAME+imgpath;
     }catch(Exception e){
       e.printStackTrace();
     }
     return imgpath;
     } 
     
     /**
      * This method will dynamically pass argument to the ApplicationResource Properties file and returns the formatted string.
      * @param key
      * @param arg
      * @return
      */
     public static String getResourceMessage(String key, Object[] arg)                                                                                                       
     {
             String msg = null;
             try
             {
                     ResourceBundle resourceBundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
                     String temp = resourceBundle.getString(key);
                     java.text.MessageFormat formatter = new java.text.MessageFormat("");
                     formatter.applyPattern(temp);
                     msg = formatter.format(arg);
             } catch (Exception e)
             {
                     e.printStackTrace();
             }
             return msg;
     }
    
  /**
   * This method will autologin if __wuuid cookie is available. 
   * Added for wu516_19112013
   */

  public void autoLogin(HttpServletRequest request, HttpServletResponse response){
    String userId = null;
    userId = new CookieManager().getCookieValue(request, "__wuuid");
    if(!GenericValidator.isBlankOrNull(userId)){
     new SessionData().addData(request, response, "y", String.valueOf(userId));
     new MobileUtils().setSessionId(request, response, userId);
     new CommonFunction().getUserName(String.valueOf(userId), request);
     HttpSession session = request.getSession();
      session.removeAttribute("userInfoList");
      session.removeAttribute("latestMemberList");
      new CommonFunction().loadUserLoggedInformation(request, null, response);
    }
  }

  /**
   * This will be used to decrypt the values Added for 21-Jan-2014_REL
   * @param value
   * @return String   
  */
  public static String decryptValues(String value){
    StringBuilder outSb = new StringBuilder(value.length());
    char c;
    for (int i = 0; i < value.length(); i++){
      c = value.charAt(i);
      c = (char)(c ^ 129); /// remember to use the same XORkey value you used in javascript
      outSb.append(c);
    }
    value = outSb.toString();
    return value;
  }
  
  public String formatNumberReview(String value) {
    //
    try {
      DecimalFormat df1 = new DecimalFormat("#.0");
      String formatedValue = df1.format(Double.parseDouble(value));
      return formatedValue;
    } catch (Exception exception) {
      return value;
    }
  }

  public static String getPdfPath(String pdfName)
  {
    String pdfPath = null;
    try{
       ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
       pdfPath = bundle.getString("wuni.pdfdomain")+pdfName; //Removed WU scheme as part of SSL work for 08_MAR_2016, By Thiyagu G.       
    } catch(Exception e){
      e.printStackTrace();
     }
     return pdfPath;
  }
  
  public static String getBlockedFlag(String collegeId){
      String blockedFlag = "";
      DataModel dataModel = new DataModel();
      Vector vector = new Vector();
      vector.add(collegeId);
      try{
          blockedFlag   = dataModel.getString("WU_UTIL_PKG.CHECK_BLOCK_PROVIDER_FN", vector);
      }catch(Exception e){
          e.printStackTrace();
      }
      return blockedFlag;
  }
  
  /**
   * @see To generate a random string and return it.
   * @param length
   * @return String
   * @since WU_537_20150224
   * @author Karthi
   */
  public String randomString(int length){
    Random rnd = new Random();
    String generateString = "";
    for(int loop = 0; loop < length; loop++){ 
      generateString += (char)(rnd.nextInt(25)+(int)'a');
    }
    return generateString;
  }
  
  /**
   * @see To set the response text.
   * @param response
   * @param responseMessage
   * @return void
   * @since Nov-24-15
   * @author Indumathi.S
   */
  public static void setResponseText(HttpServletResponse response, StringBuffer responseMessage) {
    try {
      response.setContentType("TEXT/PLAIN");
      Writer writer = response.getWriter();
      writer.write(responseMessage.toString());
    } catch (IOException exception) {
      exception.printStackTrace();
    }
  }
  
  public static String getRequestedURL(HttpServletRequest request) {
    String requestURL = null;
    String serverName = request.getServerName();
    String requestURI = request.getRequestURI();
    if (serverName != null && serverName.trim().length() > 0) {
      requestURL = serverName;
      if (requestURI != null && requestURI.trim().length() > 0) {
        requestURL += request.getRequestURI();
      }
    }
    return GlobalConstants.WHATUNI_SCHEME_NAME + requestURL;
  }
  
  /**
	 * This function is used to check whether this URL has to be filtered if userID not present
	 * 
	 * @param request
	 * 
	 */
  public boolean doesThisPageNeedAuthentication(HttpServletRequest request) {
    boolean flag = true;
    String requestURI = request.getRequestURI();
    if (GlobalConstants.PAGES_THAT_DOESNT_NEEDS_AUTHENTICATION.indexOf(requestURI) > -1) {
      flag = false;
    }
    return flag;
  }
  
	 /**
	  * This function is used to check whether this URL is ajax url or not
	  * 
	  * @param request
	  * 
	 */
  public boolean isThisAjaxCall(HttpServletRequest request) {
    boolean flag = false;
    String requestURI = request.getRequestURI();    
    for(int i=0; i < GlobalConstants.AJAX_URLS.length; i++) {
      if(requestURI.indexOf(GlobalConstants.AJAX_URLS[i]) > -1) {
        flag = true;
        break;
      }
    }
    return flag;
  }
  
  	 /**
	  * This function is used to check whether this URL is ajax url or not
	  * 
	  * @param request
	  * 
	 */
  public boolean isThisAjaxAndNormalCall(HttpServletRequest request) {
    boolean flag = false;
    String requestURI = request.getRequestURI();    
    for(int i=0; i < GlobalConstants.AJAX_NORMAL_URLS.length; i++) {
      if(requestURI.indexOf(GlobalConstants.AJAX_NORMAL_URLS[i]) > -1) {
        flag = true;
        break;
      }
    }
    return flag;
  }
  public String contentHubDeviceSpecificSectionPath(String path, String deviceWidth){
    String imgPath = "";  
    if(!GenericValidator.isBlankOrNull(path) && path.indexOf(".") != -1){
      String imgFormat = path.substring(path.lastIndexOf("."),path.length());
      imgPath += path.replace(imgFormat, deviceWidth + imgFormat);
    }
    return imgPath;
  }
  /**
   * @see This method to check arraylist is blankOrNull
   * @author Sangeeth.S
   * @param list
   * @return boolean
   */
  public static boolean isBlankOrNull(ArrayList list){
    if(list != null && list.size() > 0){
      return false;
    }
    return true;
  }
  /**
   * @see This method to check Map is blankOrNull
   * @author Sangeeth.S
   * @param map
   * @return boolean
   */
  public static boolean isBlankOrNull(Map map){
    if(map != null && !map.isEmpty()){
      return false;
    }else{
      return true;
    }
  }
  
  public ArrayList gradeListAnalysis(ArrayList gardeFilterList) {
    if(!CommonUtil.isBlankOrNull(gardeFilterList)) {
      GardeFilterVO gardeFilterVO = null;
      for(int i=0; i < gardeFilterList.size(); i++) {
        gardeFilterVO = (GardeFilterVO)gardeFilterList.get(i);
        if(!GenericValidator.isBlankOrNull(gardeFilterVO.getGradeStr())) {
          gardeFilterVO.setGradesList(Arrays.asList(gardeFilterVO.getGradeStr().split(",")));
        } 
      }
    }
    return gardeFilterList;
  }
  
  /**
   * @see This method to get review user name color code
   * @author Sangeeth.S
   * @param userName
   * @param iteratorIndexNum
   * @return
   */
  public String getReviewUserNameColorCode(String userName){
    String userNameColorClassName = "rev_blue";
    String userNameFirstLetter = "";        
    if(!GenericValidator.isBlankOrNull(userName)){
      userNameFirstLetter = String.valueOf(userName.charAt(0)).toUpperCase().trim();            
      userNameColorClassName = ("AFKPUZ".indexOf(userNameFirstLetter) > -1) ? "rev_grn" : ("BGLQV".indexOf(userNameFirstLetter) > -1) ? "rev_blue" : ("CHMRW".indexOf(userNameFirstLetter) > -1) ? "rev_grey" : ("DINSX".indexOf(userNameFirstLetter) > -1) ? "rev_lred" : ("EJOTY".indexOf(userNameFirstLetter) > -1) ? "rev_dblue" : "rev_blue";
    }    
    return userNameColorClassName;
  }
  /**
   * @see This method to empty the page number parameters in url
   * @author Hema.S
   * @param String
   * @return
   */
public String pageNumberEmpty(String Url,String pagenochar){
    Url = Url.replaceAll(pagenochar,"");
    return Url;
}
/**
 * @see Encryting to user id to base64 and adding it in cookie by Sangeeth.S for whatuniGO journey
 * @param userId
 * @param response
 */
public void addEncryptedCookieUserId(HttpServletRequest request, String userId, HttpServletResponse response){    
  CookieManager cookieManager = new CookieManager();       
  if(!GenericValidator.isBlankOrNull(userId) && !"0".equals(userId)) {  
    String enApplyNowUserID = Base64.encodeBytes(userId.getBytes());      
    Cookie enCApplyNowUserId = cookieManager.createCookie(request, GlobalConstants.AN_USER_ID_COOKIE, enApplyNowUserID);
    response.addCookie(enCApplyNowUserId);    
  }
}
/**
 * @see This method to get the decrypt the cookie user id value
 * @author Sangeeth.S
 * @param String
 * @return 
 */
public String getDecrytedCookieUseId(String cookieEncUserId) throws Exception {
  String decodedUserId = "";
  try{
    decodedUserId = URLDecoder.decode(cookieEncUserId, "UTF-8");
    byte[] decodedCUserId = Base64.decode(decodedUserId);
    decodedUserId = new String(decodedCUserId);
    //System.out.println("user decoded user id " + decodedUserId);
  } catch (Exception e) {
    //e.printStackTrace();
    throw new Exception(e);
  }
  return decodedUserId;
}  

public String getSessionRandomNumber(HttpServletRequest request, HttpSession session) {
 String randomNumber = null;
 int digit = 9;
// int newDigit = 0;
 String sessionRandomNumber = (String) session.getAttribute("RANDOM_NUMBER");
 //String sessionRandomNumberDigit = String.valueOf(session.getAttribute("RANDOM_NUMBER_DIGITS"));
 if (sessionRandomNumber != null ) {
   // digit = Integer.valueOf(sessionRandomNumberDigit);
    randomNumber = sessionRandomNumber;
    
    session.setAttribute("RANDOM_NUMBER", randomNumber);
 }
 if (randomNumber == null) {
    digit = getRandomIntegerBetweenRange(9,12);
    // session.setAttribute("RANDOM_NUMBER_DIGITS", digit);
    randomNumber = getRandomInteger(digit);
    session.setAttribute("RANDOM_NUMBER", randomNumber);
 }
 return randomNumber;
  }
  
  
  
public String getRandomInteger(int digit) {
  String randomNumber = String.valueOf((Math.abs(new Random().nextLong())));
  String randomNumberSubString = randomNumber.substring(0, digit);
  return randomNumberSubString;
}
  
public static int getRandomIntegerBetweenRange(int min, int max) {
    Random r = new Random();
    int randomint = r.nextInt((max - min) + 1) + min;
    return randomint;
}
/**
 * @see this method to set the user type to clearing
 * @param request
 * @param response
 */
public void setClearingUserType(HttpServletRequest request, HttpServletResponse response){
  HttpSession session = request.getSession();
  CookieManager cookieManager = new CookieManager();
  String clearingSession = (String)session.getAttribute("USER_TYPE");
  if (GenericValidator.isBlankOrNull(clearingSession)) {      
    session.setAttribute("USER_TYPE", GlobalConstants.CLEARING_USER_TYPE_VAL);
    Cookie cookiee = cookieManager.createCookie(request, GlobalConstants.CLEARING_USER_TYPE_COOKIE, GlobalConstants.CLEARING_USER_TYPE_VAL);
    response.addCookie(cookiee);
  }    
}
/**
   * @author Sangeeth.S
   * @see this method to check the valid date with respect to the date format
   * @param request
   * @param response
   */
  public static boolean isValidDate(String dateStr, String dateFormat) {
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
    simpleDateFormat.setLenient(false);
    try {
      simpleDateFormat.parse(dateStr);
    } catch (Exception e) {
      return false;
    }
    return true;
  } 
  /**
   * @author Jeyalakshmi.D
   * @see this method to get the data from request data
   * @param request
   * @param response
   */
  public String getBody(HttpServletRequest request) {
	  String body = null;
	  StringBuilder stringBuilder = new StringBuilder();
	  BufferedReader bufferedReader = null;
	  try {
	    InputStream inputStream = request.getInputStream();
	    if (inputStream != null) {
	      bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	      char[] charBuffer = new char[128];
	      int bytesRead = -1;
	      while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	        stringBuilder.append(charBuffer, 0, bytesRead);
	      }
	    } else {
	      stringBuilder.append("");
	    }
	  } catch (IOException ex) {
	    ex.printStackTrace();
	    return null;
	  } finally {
	    if (bufferedReader != null) {
	      try {
	        bufferedReader.close();
	      } catch (IOException ex) {
	        ex.printStackTrace();
	        return null;
	      }
	    }
	  }
	  body = stringBuilder.toString();
	  return body;
	}
  
  /**
   * @author Hemalatha.K
   * @see this method to get the device specific image url
   * @param request, imagePath, mobileSize, tabSize, desktopSize
   * @return imageUrl
   */
  public String getImageUrl(HttpServletRequest request, String imagePath, String mobileSize, String tabSize, String desktopSize) {
    String imageUrl = imagePath;
    String size = "";
    UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
    OperatingSystem agent = userAgent.getOperatingSystem();
    String deviceType = agent.getDeviceType().getName();
    if (deviceType.equalsIgnoreCase("Mobile")) {
      size = mobileSize;
    } else if (deviceType.equalsIgnoreCase("Tablet")) {
      size = tabSize;
    } else if (deviceType.equalsIgnoreCase("Computer")) {
      size = desktopSize;
    }
    imageUrl = contentHubDeviceSpecificSectionPath(imagePath, size);
    return imageUrl;
  } 
  
  
  public boolean getMobileFlag(HttpServletRequest request) {
    UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
    OperatingSystem agent = userAgent.getOperatingSystem();
    String deviceType = agent.getDeviceType().getName();
    if (StringUtils.isNotBlank(deviceType) && StringUtils.equalsIgnoreCase(deviceType,"Mobile")) {
      return true;
    }
    return false;
  }
  
  /**
   * @author Sangeeth.S
   * @see this method to get the study level text
   * @param selQual
   * @return study leve text
   */
  public String getStudyLevelText(String selQual) {	  
      String studyLevelTextForEnterGrade = "";
      if ("degree".equals(selQual)) {        
        studyLevelTextForEnterGrade = GlobalConstants.UNDERGRADUATE;
      } else if ("postgraduate".equals(selQual)) {
        // do nothing
      } else if ("foundation-degree".equals(selQual)) {        
        studyLevelTextForEnterGrade = GlobalConstants.FOUNDATION_DEGREE;
      } else if ("access-foundation".equals(selQual)) {        
        studyLevelTextForEnterGrade = GlobalConstants.ACCESS_FOUNDATION;
      } else if ("hnd-hnc".equals(selQual)) {        
        studyLevelTextForEnterGrade = GlobalConstants.HND_HNC;
      } else if ("ucas_code".equals(selQual)) {
        // do nothing
      } else if ("Clearing".equals(selQual)) {        
        studyLevelTextForEnterGrade = GlobalConstants.UNDERGRADUATE;
      } else if ("all".equals(selQual)) {        
        studyLevelTextForEnterGrade = GlobalConstants.ALL;
      }
      return studyLevelTextForEnterGrade;
  }
  /**
   * @author sangeeth.s
   * @see for appending the open day event type ga value for dimendion18 
   * @param openDayEventType
   * @param eventCategoryName
   * @since 24 april 2020
   * @return
   */
  public String setEventTypeValueGA(String openDayEventType, String eventCategoryName) {
  	if(openDayEventType.indexOf(eventCategoryName) <= -1) {
	  openDayEventType =  openDayEventType + eventCategoryName + "|";
	}
  	return openDayEventType;
    }
  /**
   * @author Sangeeth.s
   * @see this method is to convert the event type name to ga specific event name for logging from collection of the event name in the string
   * @since 28 april 2020
   * @return
   */
  public String setOpenDayEventGALabel(String openDayEventType) {
	if(!GenericValidator.isBlankOrNull(openDayEventType)) {
	  openDayEventType = openDayEventType.replace(GlobalConstants.VIRTUAL_EVENT_TYPE,GlobalConstants.VIRTUAL_GA_TYPE);
	  openDayEventType = openDayEventType.replace(GlobalConstants.PHYSICAL_EVENT_TYPE,GlobalConstants.PHYSICAL_GA_TYPE);
	  openDayEventType = openDayEventType.replace(GlobalConstants.ONLINE_EVENT_TYPE,GlobalConstants.ONLINE_GA_TYPE);
	  openDayEventType = openDayEventType.replace("Virtual tour",GlobalConstants.VIRTUAL_GA_TYPE);
	  //openDayEventType = openDayEventType.replace("",GlobalConstants.PHYSICAL_GA_TYPE);
	  openDayEventType = openDayEventType.replace("Online event",GlobalConstants.ONLINE_GA_TYPE);
	}  	
    return openDayEventType;
  }
  /**
   * @author Sangeeth.s
   * @see this method is modifying the dimesnion18 value with the pipe symbol specification
   * @since 30 april 2020
   * @return
   */
  public String setDimValWithEmptyPipe(String openDayEventType) {
	 String [] dim18 = {GlobalConstants.VIRTUAL_GA_TYPE, GlobalConstants.ONLINE_GA_TYPE ,GlobalConstants.PHYSICAL_GA_TYPE};
	 String dime18Val = GlobalConstants.VIRTUAL_GA_TYPE + "|" + GlobalConstants.ONLINE_GA_TYPE + "|" +GlobalConstants.PHYSICAL_GA_TYPE;
	 if(!GenericValidator.isBlankOrNull(openDayEventType)) {
	  for(int i = 0 ; i < dim18.length ; i++) {
		if(openDayEventType.indexOf(dim18[i]) > -1) {			
		}else {
			dime18Val = dime18Val.replace(dim18[i], "");
		} 
	  } 	  
	 }  	
    return dime18Val;
  }
  
  public String generateWebServiceUrl(String mapping) {
    String domainPath = GlobalConstants.WU_WEB_SERVICE_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN;
    //domainPath = domainPath + ":7001";
    domainPath = domainPath + SpringConstants.CLEARING_API_CONTEXT_PATH + mapping;
    return domainPath;
  }
  
  public String generateContentfulServiceUrl(String mapping) {
    String domainPath = GlobalConstants.WU_WEB_SERVICE_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN;
    //domainPath = domainPath + ":7001";
    domainPath = domainPath + SpringConstants.WEBSERVICE_CONTEXT_PATH + mapping;
    return domainPath;
  }
   /**
   * @author Sujitha V
   * @see this method is used to change the input values in title case.
   * @since 17 SEP 2020
   * @return
   */
  public String titleCaseWithSlash(String inputString) {
	 
	String titleCaseResult = "";
	try{
	  if(StringUtils.isNotBlank(inputString)) {
	    for(int i=0; i<inputString.length(); i++) {
	      String nextString = StringUtils.substring(inputString, i, i+1);	   
	      if(StringUtils.contains(inputString, "/")) {
	    	/* String titleCaseForSlashInput = Arrays.stream(inputString.split("/"))
	    		                            .map(t -> t.substring(0, 1).toUpperCase() + t.substring(1))
	    		                            .collect(Collectors.joining("/"));*/
	        titleCaseResult = inputString;	        
	      }else {
	      if(!StringUtils.isNumeric(inputString)) {
	        if(i == 0){
	          titleCaseResult += StringUtils.upperCase(nextString);
	        }else{	     
	          titleCaseResult += StringUtils.lowerCase(nextString);
	        } 
	       }
	      }
	    }
	  }
	}catch(Exception exception) {
	  return inputString;
	}
	return titleCaseResult;	  
  }
  
  /**
   * @author Sri Sankari R
   * @see this method is used to iterate the meta details send from back office.
   * @since 20 OCT 2020
   * @return
   */
  public void iterateMetaDetails(ArrayList<SEOMetaDetailsVO> seoMetaDetailsList, ModelMap model) {
	  if(!CollectionUtils.isEmpty(seoMetaDetailsList)){
			SEOMetaDetailsVO seoMetaDetailsVO = (SEOMetaDetailsVO) seoMetaDetailsList.get(0);
			model.addAttribute(SpringConstants.META_TITLE, seoMetaDetailsVO.getMetaTitle());
			model.addAttribute(SpringConstants.META_DESC, seoMetaDetailsVO.getMetaDesc());
			model.addAttribute(SpringConstants.META_KEYWORD, seoMetaDetailsVO.getMetaKeyword());
			model.addAttribute(SpringConstants.CATEGORY_DESC, seoMetaDetailsVO.getCategoryDesc());
			model.addAttribute(SpringConstants.STUDY_DESC, seoMetaDetailsVO.getStudyDesc());
			model.addAttribute(SpringConstants.META_ROBOTS, seoMetaDetailsVO.getMetaRobots());
		  }
  }
  /**
   * @see This method check the designated cookie is opted in by the user or not
   * @since 23-OCT-2020
   * @author Sangeeth.S
   */
  public static boolean checkCookiePreference(HttpServletRequest request, String cookieName){
	String cookieConsent = new CookieManager().getCookieValue(request, GlobalConstants.COOKIE_CONSENT_COOKIE);
	cookieConsent = cookieConsent != null ? cookieConsent : GlobalConstants.CONSENT_COOKIE_DEFAULT_VAL;
	//String cookiePOCFlag = StringUtils.isBlank((String)request.getSession().getAttribute("cookiePOC")) ? request.getParameter("cookiePOC") : (String)request.getSession().getAttribute("cookiePOC");
	if(StringUtils.isNotBlank(cookieConsent) && (cookieConsent.length() >= 4)) {
	  //ex cookie consent value format - 0111(0-strict,1-functional,1-performance,1-targetting) where 0 -opt in and 1 - opt out
	  String stictlyNeccessaryFlag = cookieConsent.substring(0,1);
	  String functionalCookieFlag = cookieConsent.substring(1,2);
	  String performanceCookieFlag = cookieConsent.substring(2,3);//performance
	  String targetingCookieFlag = cookieConsent.substring(3,4);
	  //String stictlyList = GlobalConstants.STRICTLY_NECESSARY_COOKIES;
	  //String functionalList = GlobalConstants.FUNCTIONAL_COOKIES;
	  //
	  if(StringUtils.isNotBlank(cookieConsent)) {
	    if(GlobalConstants.ZERO.equals(stictlyNeccessaryFlag) && GlobalConstants.STRICTLY_NECESSARY_COOKIES.indexOf(cookieName) != -1) {
		  return true;
	    } else if(GlobalConstants.ZERO.equals(functionalCookieFlag) && GlobalConstants.FUNCTIONAL_COOKIES.indexOf(cookieName) != -1) {
		  return true;
	    }else if(GlobalConstants.ZERO.equals(performanceCookieFlag) && GlobalConstants.PERFORMANCE_COOKIE.indexOf(cookieName) != -1){
	      return true;
	    }else if(GlobalConstants.ZERO.equals(targetingCookieFlag) && GlobalConstants.TARGETING_COOKIE.indexOf(cookieName) != -1){
		  return true;
	    }		    
	  }
	}else{
		return true;
	}
    return false;
  }
  /**
   * @see This method set the cookie consent details in session
   * @since 27-OCT-2020
   * @author Sangeeth.s
   */
  public static void setThirdPartyCookieFlagInsession(HttpServletRequest request, HttpServletResponse response){
	String cookieConsent = new CookieManager().getCookieValue(request, GlobalConstants.COOKIE_CONSENT_COOKIE);
	cookieConsent = cookieConsent != null ? cookieConsent : GlobalConstants.CONSENT_COOKIE_DEFAULT_VAL;	
	if(StringUtils.isNotBlank(cookieConsent) && (cookieConsent.length() >= 4)) {
	  //ex cookie consent value format - 0111(0-strict,1-functional,1-performance,1-targetting) where 0 -opt in and 1 - opt out
	  //String stictlyNeccessaryFlag = cookieConsent.substring(0,1);
	  String functionalCookieFlag = cookieConsent.substring(1,2);
	  String performanceCookieFlag = cookieConsent.substring(2,3);
	  String targetingCookieFlag = cookieConsent.substring(3,4);	  
	  
	    SessionData sessionData = new SessionData();
	    if(GlobalConstants.NUMBER_ONE.equals(performanceCookieFlag)) {
	      sessionData.addData(request, response, "cookiePerformanceDisabled", GlobalConstants.FLAG_Y);	      
	    } else if(GlobalConstants.ZERO.equals(performanceCookieFlag)) {
	      sessionData.addData(request, response, "cookiePerformanceDisabled", GlobalConstants.FLAG_N);	     
	    }
	    if(GlobalConstants.NUMBER_ONE.equals(functionalCookieFlag)) {
	      sessionData.addData(request, response, "cookieFunctionalDisabled", GlobalConstants.FLAG_Y);
	      
	    } else if(GlobalConstants.ZERO.equals(functionalCookieFlag)) {
	      sessionData.addData(request, response, "cookieFunctionalDisabled", GlobalConstants.FLAG_N);	    	
	    }
	    if(GlobalConstants.NUMBER_ONE.equals(targetingCookieFlag)) {
	      sessionData.addData(request, response, "cookieTargetingCookieDisabled", GlobalConstants.FLAG_Y);		  
		} else if(GlobalConstants.ZERO.equals(targetingCookieFlag)) {
		  sessionData.addData(request, response, "cookieTargetingCookieDisabled", GlobalConstants.FLAG_N);			
		}
	 
    }
  }
  /**
   * @see This method set the cookie consent details in session
   * @since 1-NOV-2020
   * @author Sangeeth.s
   */
  public static void updateCookieConsentCookie(HttpServletRequest request, HttpServletResponse response,String cookieName, String cookieValue){
    if(StringUtils.isNotBlank(cookieValue) && (cookieValue.length() >= 4)) {      
      String functionalCookieFlag = cookieValue.substring(1,2);      
      String functionalList = GlobalConstants.FUNCTIONAL_COOKIES;	    
      String cookieConsent = new CookieManager().getCookieValue(request, GlobalConstants.COOKIE_CONSENT_COOKIE); 
      if(StringUtils.isNotBlank(cookieConsent) && (cookieConsent.length() >= 4)) { 
    	//ex cookie consent value format - 0111(0-strict,1-functional,1-performance,1-targeting) where 0 -opt in and 1 - opt out
    	String prefunctionalCookieFlag = cookieConsent.substring(1,2);//getting functional cookie value    	
    	if(GlobalConstants.NUMBER_ONE.equals(functionalCookieFlag) && !prefunctionalCookieFlag.equals(functionalCookieFlag)){
    	  CookieManager cookieManager = new CookieManager();
    	  String[] cookieNames  = functionalList.split(",");
          for(int i = 0 ; i < cookieNames.length ; i++){
            String cookieVal = cookieManager.getCookieValue(request, cookieNames[i]);       
            if(StringUtils.isNotBlank(cookieVal)) {
        	  Cookie cookie = cookieManager.deleteCookie(request, cookieNames[i]);
              response.addCookie(cookie); 		
            }
    	  }
        }      
      } 
	}
  }
}