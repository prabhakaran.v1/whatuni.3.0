package WUI.utilities.form;

import org.apache.struts.action.ActionForm;

public class CpeWorstPerformersBean {
  
  //attributes used to bulid links-to-provider-result-page
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String collegeLocation ="";
  private String ldcs1Code = "";
  private String ldcs1Description = "";
  private String hashRemovedLdcs1Description = "";
  private String qualificationCode = "";
  private String qualificationDescription = "";
  private String ucasPoint = "";
  private String combinedValueDistance = "";
  private String tenencyBoostiingValue = "";  
  private String studyMode = "0";
  private String location = "United+Kingdom";
  
  //attributes uesd to hold enquires details   
  private String orderItemId = "";
  private String subOrderItemId = "";
  private String subOrderWebsite = "";
  private String subOrderEmail = "";  
  private String subOrderEmailWebform = "";
  private String subOrderEmailTargetURL = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";
  private String subOrderProspectusTargetURL = "";
  private String profileType = "";  
  private String myhcProfileId = "";
  private String cpeQualificationNetworkId = "";  
  private String profileAdvertName = "";
  
  //attributes uesd to hold media details
  private String mediaId = "";
  private String mediaType = "";
  private String mediaPath = ""; 
  private String mediaThumbPath = "";
  
  private String openDayCount = "";
  private String collegeLogo = "";
  
  private String uniHomeUrl = null;

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setLdcs1Code(String ldcs1Code) {
    this.ldcs1Code = ldcs1Code;
  }

  public String getLdcs1Code() {
    return ldcs1Code;
  }

  public void setLdcs1Description(String ldcs1Description) {
    this.ldcs1Description = ldcs1Description;
  }

  public String getLdcs1Description() {
    return ldcs1Description;
  }

  public void setHashRemovedLdcs1Description(String hashRemovedLdcs1Description) {
    this.hashRemovedLdcs1Description = hashRemovedLdcs1Description;
  }

  public String getHashRemovedLdcs1Description() {
    return hashRemovedLdcs1Description;
  }

  public void setQualificationCode(String qualificationCode) {
    this.qualificationCode = qualificationCode;
  }

  public String getQualificationCode() {
    return qualificationCode;
  }

  public void setQualificationDescription(String qualificationDescription) {
    this.qualificationDescription = qualificationDescription;
  }

  public String getQualificationDescription() {
    return qualificationDescription;
  }

  public void setUcasPoint(String ucasPoint) {
    this.ucasPoint = ucasPoint;
  }

  public String getUcasPoint() {
    return ucasPoint;
  }

  public void setCombinedValueDistance(String combinedValueDistance) {
    this.combinedValueDistance = combinedValueDistance;
  }

  public String getCombinedValueDistance() {
    return combinedValueDistance;
  }

  public void setTenencyBoostiingValue(String tenencyBoostiingValue) {
    this.tenencyBoostiingValue = tenencyBoostiingValue;
  }

  public String getTenencyBoostiingValue() {
    return tenencyBoostiingValue;
  }

  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }

  public String getStudyMode() {
    return studyMode;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getLocation() {
    return location;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderEmailTargetURL(String subOrderEmailTargetURL) {
    this.subOrderEmailTargetURL = subOrderEmailTargetURL;
  }

  public String getSubOrderEmailTargetURL() {
    return subOrderEmailTargetURL;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setSubOrderProspectusTargetURL(String subOrderProspectusTargetURL) {
    this.subOrderProspectusTargetURL = subOrderProspectusTargetURL;
  }

  public String getSubOrderProspectusTargetURL() {
    return subOrderProspectusTargetURL;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }

  public String getMediaType() {
    return mediaType;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setMediaThumbPath(String mediaThumbPath) {
    this.mediaThumbPath = mediaThumbPath;
  }

  public String getMediaThumbPath() {
    return mediaThumbPath;
  }

  public void setProfileAdvertName(String profileAdvertName) {
    this.profileAdvertName = profileAdvertName;
  }

  public String getProfileAdvertName() {
    return profileAdvertName;
  }

  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }

  public String getCollegeLocation() {
    return collegeLocation;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

    public void setOpenDayCount(String openDayCount) {
        this.openDayCount = openDayCount;
    }

    public String getOpenDayCount() {
        return openDayCount;
    }

    public void setCollegeLogo(String collegeLogo) {
        this.collegeLogo = collegeLogo;
    }

    public String getCollegeLogo() {
        return collegeLogo;
    }
  public void setUniHomeUrl(String uniHomeUrl) {
    this.uniHomeUrl = uniHomeUrl;
  }
  public String getUniHomeUrl() {
    return uniHomeUrl;
  }
}
