package WUI.utilities.form;

import org.apache.struts.action.ActionForm;

public class WhatStudentsAreSayingAboutThisCollegeBean {

  private String collegeId = "";
  private String collegeName = "";
  private String reviewId = "";
  private String reviewTitle = "";
  private String reviewerName = "";
  private String reviewRating = "";

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }

  public String getReviewId() {
    return reviewId;
  }

  public void setReviewTitle(String reviewTitle) {
    this.reviewTitle = reviewTitle;
  }

  public String getReviewTitle() {
    return reviewTitle;
  }

  public void setReviewerName(String reviewerName) {
    this.reviewerName = reviewerName;
  }

  public String getReviewerName() {
    return reviewerName;
  }

  public void setReviewRating(String reviewRating) {
    this.reviewRating = reviewRating;
  }

  public String getReviewRating() {
    return reviewRating;
  }

}
