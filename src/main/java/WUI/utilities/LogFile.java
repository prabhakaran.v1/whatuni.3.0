package WUI.utilities;

import java.io.File;
import java.io.FileOutputStream;

/**
   * @LogFile.java
   * @Version : 2.0
   * @www.whatuni.com
   * @author By : Balraj. Selva Kumar
   * @Purpose  : Program used to log  the application messages
   * *************************************************************************************************************************
   * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
   * *************************************************************************************************************************
   *
   */
public class LogFile {
  java.util.ResourceBundle rbundlle = java.util.ResourceBundle.getBundle("com.resources.ApplicationResources");
  String filepath = rbundlle.getString("log_file_path");
  public void writeLogFile(String message_text) {
    try {
      java.util.Date date = new java.util.Date();
      java.util.Date timeStamp = new java.sql.Timestamp(date.getTime());
      File f = new File(filepath);
      String filename = filepath + "whatunilogfile.txt";
      if (f.isDirectory()) {
        File outputFile = new File(filename);
        FileOutputStream fos = new FileOutputStream(outputFile, true);
        String message = message_text + "  ==> " + timeStamp + "\n";
        if (message_text != null && message_text.indexOf("End") >= 0) {
          message = message + "\n";
        }
        fos.write(message.getBytes());
        fos.close();
      } else {
        //System.out.println("Unable to creat File " + filename);
      }
      //
    } catch (Exception ioexcep) {
      ioexcep.printStackTrace();
    }
  }
}
