package WUI.utilities;

import org.apache.struts.action.ActionForm;

public class TickerTapeBean {

  public TickerTapeBean() {}
  
  private String tickerName = "";
  private String tickerCollegeId = "";
  private String tickerDescription = "";
  private String tickerExternalUrl = "";
  
  public void init(){
    this.tickerName = "";
    this.tickerCollegeId = "";
    this.tickerDescription = "";
    this.tickerExternalUrl = "";
  }

  public void flush(){
    this.tickerName = null;
    this.tickerCollegeId = null;
    this.tickerDescription = null;
    this.tickerExternalUrl = null;
  }

  public void setTickerName(String tickerName) {
    this.tickerName = tickerName;
  }

  public String getTickerName() {
    return tickerName;
  }

  public void setTickerCollegeId(String tickerCollegeId) {
    this.tickerCollegeId = tickerCollegeId;
  }

  public String getTickerCollegeId() {
    return tickerCollegeId;
  }

  public void setTickerDescription(String tickerDescription) {
    this.tickerDescription = tickerDescription;
  }

  public String getTickerDescription() {
    return tickerDescription;
  }

  public void setTickerExternalUrl(String tickerExternalUrl) {
    this.tickerExternalUrl = tickerExternalUrl;
  }

  public String getTickerExternalUrl() {
    return tickerExternalUrl;
  }

}
