package WUI.message.form;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class FriendsBean {
  public FriendsBean() {
  }
  private String friendId = "";
  private String friendName = "";
  private String Town = "";
  private String groupId = "";
  private String groupName = "";
  private String emailId = "";
  private String dataOfBirth = "";
  private String gender = "";
  private String nationality = "";
  private String subInterest1 = "";
  private String subInterest2 = "";
  private String yearOfGraduation = "";
  private String iama = "";
  private String userImage = "";
  private String userLargeImage = "";
  private ArrayList subjectInterested = null;
  private String subjectCount = "0";
  private String homeTown = "";
  public void setFriendId(String friendId) {
    this.friendId = friendId;
  }
  public String getFriendId() {
    return friendId;
  }
  public void setFriendName(String friendName) {
    this.friendName = friendName;
  }
  public String getFriendName() {
    return friendName;
  }
  public void setTown(String town) {
    this.Town = town;
  }
  public String getTown() {
    return Town;
  }
  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }
  public String getGroupId() {
    return groupId;
  }
  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }
  public String getGroupName() {
    return groupName;
  }
  public void setEmailId(String emailId) {
    this.emailId = emailId;
  }
  public String getEmailId() {
    return emailId;
  }
  public void setDataOfBirth(String dataOfBirth) {
    this.dataOfBirth = dataOfBirth;
  }
  public String getDataOfBirth() {
    return dataOfBirth;
  }
  public void setGender(String gender) {
    this.gender = gender;
  }
  public String getGender() {
    return gender;
  }
  public void setNationality(String nationality) {
    this.nationality = nationality;
  }
  public String getNationality() {
    return nationality;
  }
  public void setSubInterest1(String subInterest1) {
    this.subInterest1 = subInterest1;
  }
  public String getSubInterest1() {
    return subInterest1;
  }
  public void setSubInterest2(String subInterest2) {
    this.subInterest2 = subInterest2;
  }
  public String getSubInterest2() {
    return subInterest2;
  }
  public void setYearOfGraduation(String yearOfGraduation) {
    this.yearOfGraduation = yearOfGraduation;
  }
  public String getYearOfGraduation() {
    return yearOfGraduation;
  }
  public void setIama(String iama) {
    this.iama = iama;
  }
  public String getIama() {
    return iama;
  }
  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }
  public String getUserImage() {
    return userImage;
  }
  public void setUserLargeImage(String userLargeImage) {
    this.userLargeImage = userLargeImage;
  }
  public String getUserLargeImage() {
    return userLargeImage;
  }
  public void setSubjectInterested(ArrayList subjectInterested) {
    this.subjectInterested = subjectInterested;
  }
  public ArrayList getSubjectInterested() {
    return subjectInterested;
  }
  public void setSubjectCount(String subjectCount) {
    this.subjectCount = subjectCount;
  }
  public String getSubjectCount() {
    return subjectCount;
  }
  public void setHomeTown(String homeTown) {
    this.homeTown = homeTown;
  }
  public String getHomeTown() {
    return homeTown;
  }
}