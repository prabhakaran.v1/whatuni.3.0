package WUI.message.form;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import WUI.utilities.CommonFunction;

public class MessageBean {
  public MessageBean() {
  }
  private String messageId = "";
  private String senderAddress = "";
  private String senderId = "";
  private String messageDate = "";
  private String recieverAddress = "";
  private String receiverId = "";
  private String groupId = "";
  private String inboxStatus = "";
  private String messageCount = "";
  private String senderImage = "";
  private String senderImageLarge = "";
  private String receiverImage = "";
  private String receiverImageLarge = "";
  private String messageSubject = "";
  private String messageContent = "";
  private String friends[] = null;
  private String messageType = "";
  private List messageList = null;
  public void setSenderAddress(String senderAddress) {
    this.senderAddress = senderAddress;
  }
  public String getSenderAddress() {
    return senderAddress;
  }
  public void setMessageSubject(String messageSubject) {
    this.messageSubject = messageSubject;
  }
  public String getMessageSubject() {
    return messageSubject;
  }
  public void setMessageContent(String messageContent) {
    this.messageContent = messageContent;
  }
  public String getMessageContent() {
    return messageContent;
  }
  public void setFriends(String[] friends) {
    this.friends = friends;
  }
  public String[] getFriends() {
    return friends;
  }
  public void setMessageId(String messageId) {
    this.messageId = messageId;
  }
  public String getMessageId() {
    return messageId;
  }
  public void setSenderId(String senderId) {
    this.senderId = senderId;
  }
  public String getSenderId() {
    return senderId;
  }
  public void setMessageDate(String messageDate) {
    this.messageDate = messageDate;
  }
  public String getMessageDate() {
    return messageDate;
  }
  public void setRecieverAddress(String recieverAddress) {
    this.recieverAddress = recieverAddress;
  }
  public String getRecieverAddress() {
    return recieverAddress;
  }
  public void setReceiverId(String receiverId) {
    this.receiverId = receiverId;
  }
  public String getReceiverId() {
    return receiverId;
  }
  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }
  public String getGroupId() {
    return groupId;
  }
  public void setInboxStatus(String inboxStatus) {
    this.inboxStatus = inboxStatus;
  }
  public String getInboxStatus() {
    return inboxStatus;
  }
  public void setMessageList(List messageList) {
    this.messageList = messageList;
  }
  public List getMessageList() {
    return messageList;
  }
  public void setMessageCount(String messageCount) {
    this.messageCount = messageCount;
  }
  public String getMessageCount() {
    return messageCount;
  }
  public void setMessageType(String messageType) {
    this.messageType = messageType;
  }
  public String getMessageType() {
    return messageType;
  }
  public void setSenderImage(String senderImage) {
    this.senderImage = senderImage;
  }
  public String getSenderImage() {
    return senderImage;
  }
  public void setSenderImageLarge(String senderImageLarge) {
    this.senderImageLarge = senderImageLarge;
  }
  public String getSenderImageLarge() {
    return senderImageLarge;
  }
  public void setReceiverImage(String receiverImage) {
    this.receiverImage = receiverImage;
  }
  public String getReceiverImage() {
    return receiverImage;
  }
  public void setReceiverImageLarge(String receiverImageLarge) {
    this.receiverImageLarge = receiverImageLarge;
  }
  public String getReceiverImageLarge() {
    return receiverImageLarge;
  }
/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response) {
    ActionErrors errors = new ActionErrors();
    String buttontext = request.getParameter("butvalue");
    HttpSession session = request.getSession();
    errors.clear();
    if (buttontext != null && (buttontext.equalsIgnoreCase("Send") || buttontext.equalsIgnoreCase("Save Draft"))) {
      if (new CommonFunction().checkSessionExpired(request, response, session, "REGISTERED_USER")) {
        return errors;
      }
      if (session.getAttribute("replymail") == null) {
        if (friends == null) {
          errors.add("wuni.error.message.compose.empty.sender", new ActionMessage("wuni.error.message.compose.empty.sender"));
        }
      }
      if (senderAddress != null && messageSubject.trim().length() == 0) {
        errors.add("wuni.error.message.compose.empty.subject", new ActionMessage("wuni.error.message.compose.empty.subject"));
      }
      if (messageContent != null && messageContent.trim().length() == 0) {
        errors.add("wuni.error.message.compose.empty.msg.body", new ActionMessage("wuni.error.message.compose.empty.msg.body"));
      }
    }
    return errors;
  }*/
}

//////////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////////////
