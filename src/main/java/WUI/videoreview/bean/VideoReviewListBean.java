package WUI.videoreview.bean;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.apache.struts.action.ActionForm;

public class VideoReviewListBean {

  public VideoReviewListBean() {
  }
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String courseId = "";
  private String courseName = "";
  private String userId = "";
  private String userName = "";
  private String videoReviewId = "";
  private String videoReviewTitle = "";
  private String videoReviewDesc = "";
  private String overallRating = "";
  private String videoUrl = "";
  private String videoLength = "";
  private String videoCategory = "";
  private String noOfHits = "";
  private String thumbnailUrl = "";
  private String responseCode = "";
  private String userNationality = "";
  private String userGender = "";
  private String userAtUni = "";
  private String videoType = "";
  private String profileOverviewFlag = "";
  private String userType = "";
  private String subjectId = "";

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }

  public String getCourseName() {
    return courseName;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserName() {
    return userName;
  }

  public void setVideoReviewId(String videoReviewId) {
    this.videoReviewId = videoReviewId;
  }

  public String getVideoReviewId() {
    return videoReviewId;
  }

  public void setVideoReviewTitle(String videoReviewTitle) {
    this.videoReviewTitle = videoReviewTitle;
  }

  public String getVideoReviewTitle() {
    return videoReviewTitle;
  }

  public void setVideoReviewDesc(String videoReviewDesc) {
    this.videoReviewDesc = videoReviewDesc;
  }

  public String getVideoReviewDesc() {
    return videoReviewDesc;
  }

  public void setOverallRating(String overallRating) {
    this.overallRating = overallRating;
  }

  public String getOverallRating() {
    return overallRating;
  }

  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }

  public String getVideoUrl() {
    return videoUrl;
  }

  public void setVideoLength(String videoLength) {
    this.videoLength = videoLength;
  }

  public String getVideoLength() {
    return videoLength;
  }

  public void setVideoCategory(String videoCategory) {
    this.videoCategory = videoCategory;
  }

  public String getVideoCategory() {
    return videoCategory;
  }

  public void setNoOfHits(String noOfHits) {
    this.noOfHits = noOfHits;
  }

  public String getNoOfHits() {
    return noOfHits;
  }

  public void setThumbnailUrl(String thumbnailUrl) {
    this.thumbnailUrl = thumbnailUrl;
  }

  public String getThumbnailUrl() {
    return thumbnailUrl;
  }

  public void setResponseCode(String responseCode) {
    this.responseCode = responseCode;
  }

  public String getResponseCode() {
    return responseCode;
  }

  public void setUserNationality(String userNationality) {
    this.userNationality = userNationality;
  }

  public String getUserNationality() {
    return userNationality;
  }

  public void setUserGender(String userGender) {
    this.userGender = userGender;
  }

  public String getUserGender() {
    return userGender;
  }

  public void setUserAtUni(String userAtUni) {
    this.userAtUni = userAtUni;
  }

  public String getUserAtUni() {
    return userAtUni;
  }

  public String checkPlayUrl(String playUrl) {
    int response = 0;
    try {
      URL url = new URL(playUrl);
      URLConnection urlConnection = url.openConnection();
      if (urlConnection instanceof HttpURLConnection) {
        HttpURLConnection httpConnection = (HttpURLConnection)urlConnection;
        httpConnection.connect();
        response = httpConnection.getResponseCode();
        url = httpConnection.getURL();
        String responseQueryString = url.getQuery();
        httpConnection.disconnect();
        if (responseQueryString == null || responseQueryString.equals("") || responseQueryString.trim().length() < 8 || !responseQueryString.substring(0, 8).equals("video_id")) {
          response = 600;
        }
      }
    } catch (Exception urlException) {
      urlException.printStackTrace();
    }
    return (response + "");
  }

  public void setVideoType(String videoType) {
    this.videoType = videoType;
  }

  public String getVideoType() {
    return videoType;
  }

  public void setProfileOverviewFlag(String profileOverviewFlag) {
    this.profileOverviewFlag = profileOverviewFlag;
  }

  public String getProfileOverviewFlag() {
    return profileOverviewFlag;
  }

  public void setUserType(String userType) {
    this.userType = userType;
  }

  public String getUserType() {
    return userType;
  }

  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public String getSubjectId() {
    return subjectId;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
//////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////////////    
