package WUI.videoreview.controller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
  * @VideoDetail.java
  * @Version : 2.0
  * @June 30 2007
  * @www.whatuni.com
  * @Created By :  Balraj Selvakumar
  * @Purpose  : This program used to get the list of videos
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 
  */
public class VideoDetail {
  public VideoDetail() {
  }
  boolean errorFlag = false;
  public String getBodyContent(String uploadedUrl, String startAttribute, String endAttribute) {
    String XmlReadingUrl = "http://www.youtube.com/api2_rest?method=youtube.videos.get_details&dev_id=SOXBxcHTUQw&video_id=" + uploadedUrl.substring(uploadedUrl.lastIndexOf("v=") + 2, uploadedUrl.length());
    int response = 0;
    String bodycontent = "";
    try {
      URL url = new URL(XmlReadingUrl);
      URLConnection connection = url.openConnection();
      HttpURLConnection httpConnection = (HttpURLConnection)connection;
      httpConnection.connect();
      response = httpConnection.getResponseCode();
      if (response == 200) {
        bodycontent = readXML(XmlReadingUrl, startAttribute, endAttribute);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
      bodycontent = "";
    }
    return bodycontent;
  }

  /**
    *   This function is used to read the XML data for the given input URL(Used to read the Video related information from Limelight server).
    * @param inputUrl
    * @param begin_tag
    * @param end_tag
    * @return
    * @throws Exception
    */
  private String readXML(String inputUrl, String begin_tag, String end_tag) throws Exception {
    try {
      URL url = new URL(inputUrl);
      URLConnection uconnection = url.openConnection();
      String inputLine = "";
      String inputLine2 = "";
      BufferedReader inp = new BufferedReader(new InputStreamReader(uconnection.getInputStream()));
      while ((inputLine = inp.readLine()) != null) {
        inputLine2 += inputLine;
      }
      inp.close();
      String returnData = inputLine2.substring(inputLine2.indexOf(begin_tag) + begin_tag.length(), inputLine2.indexOf(end_tag));
      return returnData;
    } catch (Exception exception) {
      errorFlag = true;
      exception.printStackTrace();
      return "";
    }
  }

 /**
   *   This function is used to upload a thumbnail for the particular video uploaded.
   * @param fileUrl
   * @param OutFilename
   */
  public void uploadThumbnail(String fileUrl, String OutFilename) {
    OutputStream out = null;
    InputStream in = null;
    try {
      URL url = new URL(fileUrl);
      URLConnection conn = url.openConnection();
      in = conn.getInputStream();
      out = new BufferedOutputStream(new FileOutputStream(OutFilename));
      byte[] buffer = new byte[1024];
      int numRead = 0;
      long numWritten = 0;
      while ((numRead = in.read(buffer)) != -1) {
        out.write(buffer, 0, numRead);
        numWritten += numRead;
      }
    } catch (Exception thumbnailException) {
      thumbnailException.printStackTrace();
      errorFlag = true;
    } finally {
      try {
        if (in != null) {
          in.close();
        }
        if (out != null) {
          out.close();
        }
      } catch (IOException ioe) {
        ioe.printStackTrace();
      }
    }
  }

  /**
    *   This function is uead to chenage the format of the URL that supports Limelight path
    * @param url_in
    * @return
    */
  public String changeVideoUrlFormat(String url_in) {
    String url_out = url_in != null? url_in.replace('?', ' '): url_in;
    url_out = url_out.replaceAll("/watch v=", "/v/");
    return url_out;
  }
}
