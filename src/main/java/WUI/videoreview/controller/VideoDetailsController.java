package WUI.videoreview.controller;

import com.layer.util.SpringConstants;
import com.wuni.util.sql.DataModel;

import WUI.utilities.AdvertUtilities;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;

import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import spring.form.VideoReviewListBean;

/**
 * This class to display the video player in popup window
 * @author Balraj Selvakumar
 * @version 1.0
 * @since 17.06.2009
 * Change Log
 * *****************************************************************************************************
 * Date           Name                      Ver.     Changes desc                               Rel Ver.
 * ******************************************************************************************************
 * 17-Jun-2009    Selvakumar                1.0      Initial draft                                1.0
 * 03-Nov-2015    Indumathi                 1.1      Added additional parameter spMyhcProfileId   wu_546
 * 31-May-2016    Prabhakaran V.            1.2      Button structure changes.                    wu_553
 * 23-Oct-2018    Sabapathi S.              1.3      Added screen width in stats logging          wu_582
 * 15-Mar-2019    Sangeeth.S                1.4      Handled CMMT interaction changes             
 * 09_MAR_2020    Sangeeth.S		        1.5    	 Added lat and long for stats logging         wu_300320
 * 17_sep_2020    Sri Sankari               1.6      Added stats log for tariff logging(UCAS point)    wu_20200917
 * 10_NOV_2020    Sujitha V                 1.7      Added session for YOE to log in d_session_log. wu_20201110
 */
@Controller
public class VideoDetailsController{

  public VideoDetailsController() {
  }
  CommonFunction common = new CommonFunction();
  String userJourneyFlag = "";//Added for cmmt
  String networkId = "";
  @RequestMapping(value = "/vids-video")
  public ModelAndView videoDetails(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    String videoReviewId = request.getParameter("vrid");
    String collegeId = request.getParameter("cid");
    String videoType = request.getParameter("videoType");
    String offsetLeft = request.getParameter("offsetlft");
    String clearingFlag = (request.getParameter("clearingFlag") != null) ? request.getParameter("clearingFlag") : "";
    String suborderitemId = (request.getParameter("subOrderItemId") != null) ? request.getParameter("subOrderItemId") : null;
    networkId = (request.getParameter("networkId") != null) ? request.getParameter("networkId") : null;
    String profileType = "";
  //NEED TO HANDLE CONDITION IN INTERACTION BUTTON LOGIC FOR CLEARING JOURNEY HIDING OF BUTTON FOR OTHER SEC AND SHOWING ONLY FOR CLEARING SEC    
    userJourneyFlag = common.getClearingUserJourneyFlag(request); // Added for cmmt
    String clearingVideo = (request.getParameter("clearingVideo") != null) ? request.getParameter("clearingVideo") : "";
    String clrProfUrlFlag = (request.getParameter("clrProfUrlFlag") != null) ? request.getParameter("clrProfUrlFlag") : "";
    if("clearingvideo".equalsIgnoreCase(clearingVideo)){
      clearingFlag = "CLEARING";
    }
    //
    if (request.getParameter("profileType") != null) { //13_JAN_15 Added by Amir for Video Rich Profile
      profileType = request.getParameter("profileType");
    }
    StringBuffer returnString = new StringBuffer("");
    response.setContentType("text/html");
    response.setHeader("Cache-Control", "no-cache");
    if (videoType != null && videoType.equalsIgnoreCase("SITEVIDEO")) {
      returnString = getAppServerVideoInformation(request, returnString, GlobalConstants.WHATUNI_SITE_VIDEO_FNAME);
      response.getWriter().write(returnString.toString());
    } else {
      returnString = getVideoInformation(request, videoReviewId, videoType,suborderitemId,networkId, returnString, session);
      if(!"contentHub".equalsIgnoreCase(request.getParameter("pageName"))){
         returnString = getInteractiveInformation(request, collegeId, clearingFlag, returnString, profileType, clrProfUrlFlag);
         if(!"cd_page".equalsIgnoreCase(request.getParameter("pageName"))){
           returnString = getVideoTicker(request, returnString, offsetLeft);
         }
        new GlobalFunction().setHitCounter(videoReviewId, "V");
      }
      response.getWriter().write(returnString.toString());
    }
    return null;
  }

  /**
   *   This function is used to load the video information and play the video in popup window.
   * @param request
   * @param videoId
   * @param returnString
   * @return StringBuffer
   */
  private StringBuffer getAppServerVideoInformation(HttpServletRequest request, StringBuffer returnString, String videoFilename) {

    returnString.append(""); //college-id - 0
    returnString.append("|BREAK|");
    returnString.append(""); // collgename - 1
    returnString.append("|BREAK|");
    returnString.append(""); //seo url for unilanding page //2
    returnString.append("|BREAK|");
    returnString.append(""); //review title //3
    returnString.append("|BREAK|");
    returnString.append("CUSTOM"); //9
    returnString.append("|BREAK|");
    returnString.append(videoFilename); //10
    returnString.append("|BREAK|");
    returnString.append(CommonUtil.getJsPath() + "/js/limelightvideoplayer/isight.gif"); //11
    returnString.append("|BREAK|");
    for (int j = 1; j <= 13; j++) {
      returnString.append("");
      returnString.append("|BREAK|");
    }
    String otherinformation = "";
    otherinformation += "<span class=\"cros avmg\"><strong><a href=\"javascript:closeVideo()\"/>Close</a></strong></span><h3>All about Whatuni</h3>";
    returnString.append(otherinformation);
    returnString.append("|BREAK|");
    return returnString;
  }

  /**
   *   This function is used to load the video information and play the video in popup window.
   * @param request
   * @param videoId
   * @param returnString
   * @return StringBuffer
   */
  private StringBuffer getVideoInformation(HttpServletRequest request, String videoId, String mediaType, String suborderitemId,String networkId, StringBuffer returnString, HttpSession session) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    String videoUrl = "";
    String limeLightPath = GlobalConstants.LIMELIGHT_VIDEO_PATH;
    Vector vector = new Vector();
    SessionData sessiondata = new SessionData();
    String clientIp = request.getHeader("CLIENTIP");
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    }
    try {
      vector.clear();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(videoId);
      vector.add("A"); // Status of review "A", "P"  "D"
      vector.add(sessiondata.getData(request, "y"));
      vector.add(sessiondata.getData(request, "x"));
      vector.add(request.getHeader("User-Agent"));
      vector.add(clientIp);
      vector.add(request.getHeader("referer"));
      vector.add(request.getParameter("refererUrl"));
      vector.add(mediaType != null && mediaType.trim().length() > 0 ? mediaType : ""); //mediaType - "B" get data from w_media table else from w_review tabvle 
      vector.add(new SessionData().getData(request, "userTrackId"));
      vector.add(suborderitemId);
      vector.add(networkId);
      // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
      String screenWidth = GenericValidator.isBlankOrNull(request.getParameter("screenwidth")) ? GlobalConstants.DEFAULT_SCREEN_WIDTH : request.getParameter("screenwidth");
      // wu_300320 - Sangeeth.S: Added lat and long for stats logging
      String[] latAndLong = sessiondata.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG).split(GlobalConstants.SPLIT_CONSTANT);
      String latitude = (latAndLong.length > 0 && !GenericValidator.isBlankOrNull(latAndLong[0])) ? latAndLong[0].trim() : "";
  	  String longitude = (latAndLong.length > 1 && !GenericValidator.isBlankOrNull(latAndLong[1])) ? latAndLong[1].trim() : "";
  	  String userUcasScore = StringUtils.isNotBlank((String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE)) ? (String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE) : null; //Added for tariff-logging-ucas-point by Sri Sankari on wu_20200917 release
  	  String yearOfEntryForDSession = StringUtils.isNotBlank((String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING)) ? (String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING) : null; //Added for getting YOE value in session to log in d_session_log stats by Sujitha V on 2020_NOV_10 rel
      vector.add(screenWidth);
      vector.add(latitude); //latitude added by sangeeth.s for March2020 rel
      vector.add(longitude); //longitude
      vector.add(userUcasScore); //P_UCAS_TARIFF
      vector.add(yearOfEntryForDSession); //P_YEAR_OF_ENTRY
      resultset = dataModel.getResultSet("wu_college_info_pkg.play_video_fn", vector);
      if(!"contentHub".equalsIgnoreCase(request.getParameter("pageName"))){
        while (resultset.next()) {
          videoUrl = resultset.getString("video_url");
          returnString.append(resultset.getString("college_id")); //0
          returnString.append("|BREAK|");
          String collegename = resultset.getString("college_name") != null ? resultset.getString("college_name").trim() : resultset.getString("college_name");
          returnString.append(collegename); //1
          returnString.append("|BREAK|");
          //http://www.whatuni.com/degrees/university-uk/Lancaster-University-ranking/3744/page.html
          returnString.append(GlobalConstants.SEO_PATH_NEW_UNILANDING_PAGE + new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(collegename))) + "/" + resultset.getString("college_id") + "/"); //2
          returnString.append("|BREAK|");
          returnString.append(resultset.getString("review_title")); //3
          returnString.append("|BREAK|");
          returnString.append(resultset.getString("video_type")); //9
          returnString.append("|BREAK|");
          String videoType = resultset.getString("video_type");
          if (mediaType != null && mediaType.equalsIgnoreCase("B")) {
            returnString.append(videoUrl); //10
            returnString.append("|BREAK|");
            returnString.append(new GlobalFunction().videoThumbnailFormatChange(videoUrl, "0")); //11
            returnString.append("|BREAK|");
          } else {
            if (videoType != null && videoType.equalsIgnoreCase("V")) {
              returnString.append(videoUrl); //10
              returnString.append("|BREAK|");
              returnString.append(resultset.getString("thumbnail_assoc_text")); //11
              returnString.append("|BREAK|");
            } else {
              returnString.append(videoUrl); //10
              returnString.append("|BREAK|");
              returnString.append(new GlobalFunction().videoThumbnailFormatChange(limeLightPath + videoUrl, "0")); //11
              returnString.append("|BREAK|");
            }
          }
          request.setAttribute("reviewtitle", resultset.getString("review_title"));
        }
      }else{
        returnString.append("SUCCESS");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return returnString;
  }

  /**
    *   This function is uead to load the interactive option for the given collegeid.
    * @param request
    * @param collegeId
    * @param returnString
    * @return StringBuffer
    */
  private StringBuffer getInteractiveInformation(HttpServletRequest request, String collegeId, String clearingFlag, StringBuffer returnString, String profileType, String clrProfUrlFlag) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    Vector vector = new Vector();
    String otherinformation = "";
    String contextPath = request.getContextPath();
    String subOrderEmail = "";
    String subOrderWebsite = "";
    String subOrderProspectusTargetURL = "";
    String prospectusTargetURL = "";
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
    String spMyhcProfileId = GenericValidator.isBlankOrNull(request.getParameter("spMyhcProfileId")) ? null : request.getParameter("spMyhcProfileId"); //Added by Indumathi NOV-03-15 For rich profile
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = "2";
    }
    request.getSession().setAttribute("cpeQualificationNetworkId", cpeQualificationNetworkId);
    try {
      vector.clear();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(collegeId);
      vector.add("0"); //userid        
      vector.add(""); //basketid
      vector.add(cpeQualificationNetworkId);
      vector.add(clearingFlag);
      List institutionProfileList = null;
      List subjectProfileList = null;
      VideoReviewListBean videobean = null;
      new AdvertUtilities().getIpSpProfileDetails(collegeId, request, cpeQualificationNetworkId, profileType, clearingFlag, spMyhcProfileId); //Added by Indumathi NOV-03-15 For rich profile
      institutionProfileList = (List)request.getAttribute("institution-profile-list");
      if (institutionProfileList != null && institutionProfileList.size() > 0) {
        Iterator uniiterator = institutionProfileList.iterator();
        videobean = (VideoReviewListBean)uniiterator.next();
      }
      subjectProfileList = (List)request.getAttribute("subject-profile-list");
      if (subjectProfileList != null && subjectProfileList.size() > 0) {
        Iterator uniiterator = subjectProfileList.iterator();
        videobean = (VideoReviewListBean)uniiterator.next();
      }
      resultset = dataModel.getResultSet("wu_gmap_pkg.get_interactive_pod_info", vector);
      while (resultset.next()) {
        String collegename = resultset.getString("college_name") != null ? resultset.getString("college_name").trim() : resultset.getString("college_name");
        String gaCollegeName = new CommonFunction().replaceSpecialCharacter(collegename);
        String collegenameDisplay = resultset.getString("college_name_display") != null ? resultset.getString("college_name_display").trim() : resultset.getString("college_name_display");
        String clearingStr = "";
        if ((clearingFlag != null && "CLEARING".equals(clearingFlag)) || ("CLEARING_JOURNEY".equalsIgnoreCase(userJourneyFlag) && !"3".equals(networkId) && "CLEARING_PROFILE_URL".equalsIgnoreCase(clrProfUrlFlag))) {
           clearingStr = "?clearing";
        }
        String landingurl = (GlobalConstants.SEO_PATH_NEW_UNILANDING_PAGE + new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(collegename))).toLowerCase() + "/" + resultset.getString("college_id") + "/"+clearingStr);
        otherinformation = "<h3><a href=\"" + landingurl + "\" class=\"lnk\">" + collegenameDisplay + "</a></h3>"; //collegename 
        otherinformation += "<h4>" + request.getAttribute("reviewtitle") + "</h4>"; //video title 
        otherinformation += "<div class=\"rpvd_clse\"><a onclick=\"closeVideo();\"><i class=\"fa fa-times\"></i></a></div>"; //Close button structure added by Prabha on 31_May_2016
        if (clearingFlag != null && !"CLEARING".equals(clearingFlag) && !"CLEARING_PROFILE_URL".equalsIgnoreCase(clrProfUrlFlag)) {
          otherinformation += "<p class=\"video_popup_icon\">";
        }
        if ((clearingFlag != null && "CLEARING".equals(clearingFlag)) || ("CLEARING_JOURNEY".equalsIgnoreCase(userJourneyFlag) && "CLEARING_PROFILE_URL".equalsIgnoreCase(clrProfUrlFlag))) {
          otherinformation += "<p class=\"video_popup_icon clr15\">";
        }
         //To display Subject profile Button
        if (institutionProfileList != null && institutionProfileList.size() > 0 || subjectProfileList != null && subjectProfileList.size() > 0) {
          if (videobean.getSubOrderItemIdForSp() != null && videobean.getSubOrderItemIdForSp().trim().length() > 0 && !videobean.getSubOrderItemIdForSp().equalsIgnoreCase("null")) {
            String subjecturl = contextPath + "/subject-profile/" + new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(collegename)))) + "-overview/" + resultset.getString("college_id") + "/overview/" + videobean.getMyhcProfileIdSp() + "/0/" + cpeQualificationNetworkId + "/subjectprofile.html";

           // otherinformation += "<a class=\"sub_pr\" href=\"" + subjecturl + "\" title=\"" + collegenameDisplay + " profile\">Subject profile <i class=\"fa fa-caret-right\"></i></a>"; Commented by Prabha on 28_Jun_2016
          }
          //To display University profile Button
          if (videobean.getSubOrderItemIdForIp() != null && videobean.getSubOrderItemIdForIp().trim().length() > 0 && !videobean.getSubOrderItemIdForIp().equalsIgnoreCase("null")) {

            //otherinformation += "<a href=\"" + profileurl + "\" title=\"" + collegename + " profile\"><img src=\"" + contextPath + "/img/whatuni/buttons/uni_profile.jpg\" alt=\"\" title=\"\" /></a>";
            //otherinformation += "<a class=\"uni_pr\" href=\"" + landingurl + "\" title=\"" + collegenameDisplay + " profile\"></a>"; //Commented by Amir based on Luke requirement
          }
          //To display prospectus and  website Interaction 
          if (videobean.getSubOrderItemIdForIp() != null && videobean.getSubOrderItemIdForIp().trim().length() > 0 && !videobean.getSubOrderItemIdForIp().equalsIgnoreCase("null")) {
            String websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice(videobean.getSubOrderItemIdForIp(), "website_price");
            String webformPrice = new WUI.utilities.CommonFunction().getGACPEPrice(videobean.getSubOrderItemIdForIp(), "webform_price");
            if ((videobean.getSubOrderProspectusWebformIp() != null && videobean.getSubOrderProspectusWebformIp().trim().length() > 0 && !videobean.getSubOrderProspectusWebformIp().equalsIgnoreCase("null")) || (videobean.getSubOrderProspectusIp() != null && videobean.getSubOrderProspectusIp().trim().length() > 0 && !videobean.getSubOrderProspectusIp().equalsIgnoreCase("null"))) {
              prospectusTargetURL = videobean.getSubOrderProspectusWebformIp() != null && videobean.getSubOrderProspectusWebformIp().trim().length() > 0 ? " rel=\"nofollow\" target=\"_blank\" onclick=\"GAInteractionEventTracking('prospectuswebform', 'interaction', 'Webclick','" + gaCollegeName + "'," + webformPrice + ");cpeProspectusWebformClick(this,'" + collegeId + "','" + videobean.getSubOrderItemIdForIp() + "','" + cpeQualificationNetworkId + "','" + videobean.getSubOrderProspectusWebformIp() + "'); var a='s.tl(';\" href=\"" + videobean.getSubOrderProspectusWebformIp() + "\"" : " onclick=\"GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request','" + gaCollegeName + "'); return prospectusRedirect('" + request.getContextPath() + "','" + collegeId + "','','','','" + videobean.getSubOrderItemIdForIp() + "'); \"";
              subOrderProspectusTargetURL = prospectusTargetURL;
            } else {
              subOrderProspectusTargetURL = "";
            }
            subOrderEmail = videobean.getSubOrderEmailIp();
            subOrderWebsite = videobean.getSubOrderEmailIp();
            String tempClearing = ""; //Labels added by Prabha on 31_May_2016
            if (clearingFlag != null && !"CLEARING".equals(clearingFlag) && !"CLEARING_PROFILE_URL".equalsIgnoreCase(clrProfUrlFlag)) {
              otherinformation += "<a class=\"get-pros\" " + subOrderProspectusTargetURL + "\" title=\"Get " + collegenameDisplay + " Prospectus\">Get prospectus <i class=\"fa fa-caret-right\"></i></a>"; 
              String websiteclk = "GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick','" + gaCollegeName + "'," + websitePrice + "); cpeWebClick(this,'" + resultset.getString("college_id") + "','" + videobean.getSubOrderItemIdForIp() + "','" + cpeQualificationNetworkId + "','" + videobean.getSubOrderWebsiteIp() + "');var a='s.tl(';";
              otherinformation += "<a rel=\"nofollow\" class=\"visit-web\" target=\"_blank\" onclick=\"" + websiteclk + "\" href=\"" + videobean.getSubOrderWebsiteIp() + "\"  title=\"" + collegenameDisplay + "website\">Visit website <i class=\"fa fa-caret-right\"></i></a>";
            }
            if (clearingFlag != null && "CLEARING".equals(clearingFlag) && "CLEARING_JOURNEY".equalsIgnoreCase(userJourneyFlag)) {
              String hotLineNo = videobean.getHotlineIp();
              if(hotLineNo != null && hotLineNo.length() > 0){
                 otherinformation += "<a href=\""+"tel:" + videobean.getHotlineIp() + "\" class=\"cbtn\" title='" + collegenameDisplay + "'" + " onclick=\"GAInteractionEventTracking('Webclick', 'interaction', 'hotline', '" + gaCollegeName + "');" + " cpeHotlineClearing(this,'" + resultset.getString("college_id") + "','" + videobean.getSubOrderItemIdForIp() + "','" + cpeQualificationNetworkId + "','" + videobean.getHotlineIp() + "'); callHotline('"+ videobean.getHotlineIp() +"','video_popup',this);\">\n" + " <i class=\"fa fa-phone pr5\"></i>\n CALL NOW</a>";    
              }              
              tempClearing = "Visit website <i class=\"fa fa-caret-right\"></i>";
              String websiteclk = "GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick','" + gaCollegeName + "'," + websitePrice + "); cpeWebClickClearing(this,'" + resultset.getString("college_id") + "','" + videobean.getSubOrderItemIdForIp() + "','" + cpeQualificationNetworkId + "','" + videobean.getSubOrderWebsiteIp() + "');var a='s.tl(';";
              otherinformation += "<a rel=\"nofollow\" class=\"visit-web\" target=\"_blank\" onclick=\"" + websiteclk + "\" href=\"" + videobean.getSubOrderWebsiteIp() + "\"  title=\"" + collegenameDisplay + "website\">" + tempClearing + "</a>";
            }
          } else if (videobean.getSubOrderItemIdForSp() != null && videobean.getSubOrderItemIdForSp().trim().length() > 0 && !videobean.getSubOrderItemIdForSp().equalsIgnoreCase("null")) {
            String webformPrice = new WUI.utilities.CommonFunction().getGACPEPrice(videobean.getSubOrderItemIdForSp(), "webform_price");
            String websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice(videobean.getSubOrderItemIdForSp(), "website_price");
            if ((videobean.getSubOrderProspectusWebformSp() != null && videobean.getSubOrderProspectusWebformSp().trim().length() > 0 && !videobean.getSubOrderProspectusWebformSp().equalsIgnoreCase("null")) || (videobean.getSubOrderProspectusSp() != null && videobean.getSubOrderProspectusSp().trim().length() > 0 && !videobean.getSubOrderProspectusSp().equalsIgnoreCase("null"))) {
              prospectusTargetURL = videobean.getSubOrderProspectusWebformSp() != null && videobean.getSubOrderProspectusWebformSp().trim().length() > 0 ? " rel=\"nofollow\" target=\"_blank\" onclick=\"GAInteractionEventTracking('prospectuswebform', 'interaction', 'Webclick','" + gaCollegeName + "'," + webformPrice + ");cpeProspectusWebformClick(this,'" + collegeId + "','" + videobean.getSubOrderItemIdForSp() + "','" + cpeQualificationNetworkId + "','" + videobean.getSubOrderProspectusWebformSp() + "'); var a='s.tl(';\" href=\"" + videobean.getSubOrderProspectusWebformSp() + "\"" : " onclick=\"GAInteractionEventTracking('prospectusbutton', 'engagement', 'Prospectus-Request','" + gaCollegeName + "'); return prospectusRedirect('" + request.getContextPath() + "','" + collegeId + "','','','','" + videobean.getSubOrderItemIdForSp() + "');\"";
              subOrderProspectusTargetURL = prospectusTargetURL;
            } else {
              subOrderProspectusTargetURL = "";
            }
            subOrderEmail = videobean.getSubOrderEmailSp();
            subOrderWebsite = videobean.getSubOrderEmailSp();
            String tempClearing = "";
            if (clearingFlag != null && !"CLEARING".equals(clearingFlag) && !"CLEARING_PROFILE_URL".equalsIgnoreCase(clrProfUrlFlag)) {
              otherinformation += "<a class=\"get-pros\" " + subOrderProspectusTargetURL + "\" title=\"Get " + collegenameDisplay + " Prospectus\">Get prospectus <i class=\"fa fa-caret-right\"></i></a>";
              String websiteclk = "GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick','" + gaCollegeName + "'," + websitePrice + "); cpeWebClick(this,'" + resultset.getString("college_id") + "','" + videobean.getSubOrderItemIdForSp() + "','" + cpeQualificationNetworkId + "','" + videobean.getSubOrderWebsiteSp() + "');var a='s.tl(';";
              otherinformation += "<a rel=\"nofollow\" class=\"visit-web\" target=\"_blank\" onclick=\"" + websiteclk + "\" href=\"" + videobean.getSubOrderWebsiteSp() + "\"  title=\"" + collegenameDisplay + "website\">Visit website <i class=\"fa fa-caret-right\"></i></a>";
            }
            if (clearingFlag != null && "CLEARING".equals(clearingFlag) && "CLEARING_JOURNEY".equalsIgnoreCase(userJourneyFlag)) {
              String hotLineNo = videobean.getHotlineSp();
              if(hotLineNo != null && hotLineNo.length() > 0){
                otherinformation += "<a href=\"javascript:void(0)\" class=\"cbtn\" title='" + collegenameDisplay + "'" + " onclick=\"GAInteractionEventTracking('Webclick', 'interaction', 'hotline', '" + gaCollegeName + "');" + " cpeHotlineClearing(this,'" + resultset.getString("college_id") + "','" + videobean.getSubOrderItemIdForSp() + "','" + cpeQualificationNetworkId + "','" + videobean.getHotlineSp() + "'); callHotline('"+ videobean.getHotlineSp() +"','video_popup',this);\">\n" + " <i class=\"fa fa-phone pr5\"></i>\n CALL NOW</a>";  
              }
              tempClearing = "Visit website <i class=\"fa fa-caret-right\"></i>";
              String websiteclk = "GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick','" + gaCollegeName + "'," + websitePrice + "); cpeWebClickClearing(this,'" + resultset.getString("college_id") + "','" + videobean.getSubOrderItemIdForSp() + "','" + cpeQualificationNetworkId + "','" + videobean.getSubOrderWebsiteSp() + "');var a='s.tl(';";
              otherinformation += "<a rel=\"nofollow\" class=\"visit-web\" target=\"_blank\" onclick=\"" + websiteclk + "\" href=\"" + videobean.getSubOrderWebsiteSp() + "\"  title=\"" + collegenameDisplay + "website\">" + tempClearing + "</a>";
            }
          }
        }
        otherinformation += "</p>";
        returnString.append(resultset.getString("college_profile"));
        returnString.append("|BREAK|");
        returnString.append(resultset.getString("subject_profile_count"));
        returnString.append("|BREAK|");
        returnString.append(subOrderEmail);
        returnString.append("|BREAK|");
        if (clearingFlag != null && !"CLEARING".equals(clearingFlag) && !"CLEARING_PROFILE_URL".equalsIgnoreCase(clrProfUrlFlag)) {
          String[] prospectusPurFlag = resultset.getString("prospectus_pur_flag").split(GlobalConstants.PROSPECTUS_SPLIT_CHAR);
          String prospectusurl = "";
          if (prospectusPurFlag != null && prospectusPurFlag.length > 1) {
            prospectusurl = prospectusPurFlag[1];
          }
          prospectusurl = prospectusurl != null && prospectusurl.trim().length() > 0 ? "target=\"_blank\" onclick=\"prospectusClick(this,'" + resultset.getString("college_id") + "'); var a='s.tl(';\" href=\"" + request.getContextPath() + "/prospectuslink.html?cid=" + resultset.getString("college_id") + "&url=" + prospectusPurFlag[1] + "\"" : " href=\"" + request.getContextPath() + "/prospectus/" + new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(collegename)))) + "-prospectus/" + resultset.getString("college_id") + "/0/0/n/order-prospectus.html" + "\"";
          returnString.append(prospectusPurFlag[0]);
        }
        returnString.append("|BREAK|");
        returnString.append(subOrderProspectusTargetURL);
        returnString.append("|BREAK|");
        returnString.append(subOrderWebsite);
        returnString.append("|BREAK|");
        returnString.append(resultset.getString("all_course_count"));
        returnString.append("|BREAK|");
        returnString.append(resultset.getString("pg_course_count"));
        returnString.append("|BREAK|");
        returnString.append(resultset.getString("degree_course_count"));
        returnString.append("|BREAK|");
        returnString.append(resultset.getString("acc_found_course_cnt"));
        returnString.append("|BREAK|");
        returnString.append(resultset.getString("foundation_course_cnt"));
        returnString.append("|BREAK|");
        returnString.append(resultset.getString("hnd_hnc_course_cnt"));
        returnString.append("|BREAK|");
        returnString.append(resultset.getString("college_in_basket"));
        returnString.append("|BREAK|");
        returnString.append(otherinformation);
        returnString.append("|BREAK|");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return returnString;
  }

  private StringBuffer getVideoTicker(HttpServletRequest request, StringBuffer returnString, String offsetLeft) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    try {
      java.util.ArrayList tickerVideoList = new WUI.utilities.GlobalFunction().loadTickerVideo();
      String videoTicketContent = "";
      String userAgent = request.getHeader("User-Agent");
      boolean isFirefox = (userAgent != null && userAgent.indexOf("Firefox/") != -1);
      boolean isMSIE = (userAgent != null && userAgent.indexOf("MSIE") != -1);
      videoTicketContent = videoTicketContent + ("<div id=\"slideshowcontainer\">");
      videoTicketContent = videoTicketContent + ("<div id=\"galleryContainer\">");
      videoTicketContent = videoTicketContent + ("<div id=\"theImages\"  style=\"left:" + offsetLeft + "px;\">");
      for (int j = 0; j < tickerVideoList.size(); j++) {
        spring.form.VideoReviewListBean tickerVidBean = (spring.form.VideoReviewListBean)tickerVideoList.get(j);
        videoTicketContent = videoTicketContent + ("<a onclick=\"return showVideo('" + tickerVidBean.getVideoReviewId() + "','" + tickerVidBean.getCollegeId() + "');\"><img class=\"scrollimg\" src='" + tickerVidBean.getThumbnailUrl() + "' />");
        videoTicketContent = videoTicketContent + ("<p class=\"imageCaption\" title=\"" + tickerVidBean.getCollegeNameDisplay() + "\">" + tickerVidBean.getCollegeNameDisplay() + "</p>");
        videoTicketContent = videoTicketContent + ("</a>");
      }
      videoTicketContent = videoTicketContent + ("<div id=\"slideEnd\"></div>");
      videoTicketContent = videoTicketContent + ("</div>");
      videoTicketContent = videoTicketContent + ("</div>");
      videoTicketContent = videoTicketContent + ("</div>");
      videoTicketContent = videoTicketContent + ("<input type=\"hidden\" name=\"initslideshow\" id=\"initslideshow\" value=\"0\" />");
      returnString.append(videoTicketContent);
      returnString.append("|BREAK|");
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return returnString;
  }

}
