package WUI.search.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class SubjectProfileBean {

  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String subjectId = "";
  private String subjectDesc = "";
  private String sectionId = "";
  private String profileId = "";
  private String imagePath = "";
  private String subjectName = "";
  private String videoUrl = "";
  private String videoThumbnailUrl = "";
  private String jsStatsLogId = "";
  private String subjectVideoUrl = "";
  private String dLogTypeKey = "";
  private String videoId = "";
  private String imageType = "";
  private String imageId = "";
  private String collegeImagePath = "";
  private String selectedSectionName = "";
  private String myhcProfileId = "";
  private List studyLevelList = null;
  private List studyModeList = null;
  private String courseName = "";
  private String studyModeId = "";
  private String studyModeDesc = "";
  private String studyModeValue = "";
  private String optionId = "";
  private String studyLevelId = "";

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setSubjectDesc(String subjectDesc) {
    this.subjectDesc = subjectDesc;
  }

  public String getSubjectDesc() {
    return subjectDesc;
  }

  public void setSectionId(String sectionId) {
    this.sectionId = sectionId;
  }

  public String getSectionId() {
    return sectionId;
  }

  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }

  public String getProfileId() {
    return profileId;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public String getSubjectId() {
    return subjectId;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }

  public String getSubjectName() {
    return subjectName;
  }

  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }

  public String getVideoUrl() {
    return videoUrl;
  }

  public void setVideoThumbnailUrl(String videoThumbnailUrl) {
    this.videoThumbnailUrl = videoThumbnailUrl;
  }

  public String getVideoThumbnailUrl() {
    return videoThumbnailUrl;
  }

  public void setJsStatsLogId(String jsStatsLogId) {
    this.jsStatsLogId = jsStatsLogId;
  }

  public String getJsStatsLogId() {
    return jsStatsLogId;
  }

  public void setSubjectVideoUrl(String subjectVideoUrl) {
    this.subjectVideoUrl = subjectVideoUrl;
  }

  public String getSubjectVideoUrl() {
    return subjectVideoUrl;
  }

  public void setDLogTypeKey(String dLogTypeKey) {
    this.dLogTypeKey = dLogTypeKey;
  }

  public String getDLogTypeKey() {
    return dLogTypeKey;
  }

  public void setVideoId(String videoId) {
    this.videoId = videoId;
  }

  public String getVideoId() {
    return videoId;
  }

  public void setImageType(String imageType) {
    this.imageType = imageType;
  }

  public String getImageType() {
    return imageType;
  }

  public void setImageId(String imageId) {
    this.imageId = imageId;
  }

  public String getImageId() {
    return imageId;
  }

  public void setCollegeImagePath(String collegeImagePath) {
    this.collegeImagePath = collegeImagePath;
  }

  public String getCollegeImagePath() {
    return collegeImagePath;
  }

  public void setSelectedSectionName(String selectedSectionName) {
    this.selectedSectionName = selectedSectionName;
  }

  public String getSelectedSectionName() {
    return selectedSectionName;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setStudyLevelList(List studyLevelList) {
    this.studyLevelList = studyLevelList;
  }

  public List getStudyLevelList() {
    return studyLevelList;
  }

  public void setStudyModeList(List studyModeList) {
    this.studyModeList = studyModeList;
  }

  public List getStudyModeList() {
    return studyModeList;
  }

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }

  public String getCourseName() {
    return courseName;
  }

  public void setStudyModeId(String studyModeId) {
    this.studyModeId = studyModeId;
  }

  public String getStudyModeId() {
    return studyModeId;
  }

  public void setStudyModeDesc(String studyModeDesc) {
    this.studyModeDesc = studyModeDesc;
  }

  public String getStudyModeDesc() {
    return studyModeDesc;
  }

  public void setStudyModeValue(String studyModeValue) {
    this.studyModeValue = studyModeValue;
  }

  public String getStudyModeValue() {
    return studyModeValue;
  }

  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }

  public String getOptionId() {
    return optionId;
  }

  public void setStudyLevelId(String studyLevelId) {
    this.studyLevelId = studyLevelId;
  }

  public String getStudyLevelId() {
    return studyLevelId;
  }

}
