package WUI.search.form;

import org.apache.struts.action.ActionForm;

public class HaveYouThoughtAboutBean {

  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String ldcs1Code = "";
  private String ldcs1Description = "";
  private String hashRemovedLdcs1Description = "";
  private String qualificationCode = "";
  private String qualificationDescription = "";
  private String ucasPoint = "";
  private String combinedValueDistance = "";
  private String tenencyBoostiingValue = "";
  private String studyMode = "0";
  private String location = "United+Kingdom";

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setLdcs1Code(String ldcs1Code) {
    this.ldcs1Code = ldcs1Code;
  }

  public String getLdcs1Code() {
    return ldcs1Code;
  }

  public void setLdcs1Description(String ldcs1Description) {
    this.ldcs1Description = ldcs1Description;
  }

  public String getLdcs1Description() {
    return ldcs1Description;
  }

  public void setQualificationCode(String qualificationCode) {
    this.qualificationCode = qualificationCode;
  }

  public String getQualificationCode() {
    return qualificationCode;
  }

  public void setQualificationDescription(String qualificationDescription) {
    this.qualificationDescription = qualificationDescription;
  }

  public String getQualificationDescription() {
    return qualificationDescription;
  }

  public void setUcasPoint(String ucasPoint) {
    this.ucasPoint = ucasPoint;
  }

  public String getUcasPoint() {
    return ucasPoint;
  }

  public void setCombinedValueDistance(String combinedValueDistance) {
    this.combinedValueDistance = combinedValueDistance;
  }

  public String getCombinedValueDistance() {
    return combinedValueDistance;
  }

  public void setTenencyBoostiingValue(String tenencyBoostiingValue) {
    this.tenencyBoostiingValue = tenencyBoostiingValue;
  }

  public String getTenencyBoostiingValue() {
    return tenencyBoostiingValue;
  }

  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }

  public String getStudyMode() {
    return studyMode;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getLocation() {
    return location;
  }

  public void setHashRemovedLdcs1Description(String hashRemovedLdcs1Description) {
    this.hashRemovedLdcs1Description = hashRemovedLdcs1Description;
  }

  public String getHashRemovedLdcs1Description() {
    return hashRemovedLdcs1Description;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
