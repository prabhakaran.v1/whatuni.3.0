package WUI.search.form;

import java.util.ArrayList;
import java.util.List;

public class CourseDetailsBean  {

  public CourseDetailsBean() {
  }
  private String courseId = "";
  private String courseTitle = "";
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String courseDuration = "";
  private String studyModeDesc = "";
  private String ucasCode = "";
  private String startDate = "";
  private String courseQualification = "";
  private String courseVenue1 = "";
  private String courseVenue2 = "";
  private String awardingBody = "";
  private String courseFees = "";
  private String assessment = "";
  private String entryRequirement = "";
  private String venueDescription = "";
  private String town = "";
  private String county = "";
  private String postCode = "";
  private String telePhone = "";
  private String fax = "";
  private String webSite = "";
  private String contactDetails = "";
  private String contactAddress = "";
  private String noOfReviews = "";
  private String courseSummary = "";
  private String courseInBasket = "";
  private String collegeInBasket = "";
  private String collegeLogo = "";
  private String courseContact = "";
  private String registrarEmail = "";
  private String courseFullDescription = "";
  private String admissionEmail = "";
  private String emailSubject = "";
  private String emailMessage = "";
  private String lastUpdateDate = "";
  private String seoStudyLevelText = "";
  private String advertiserFlag = "";
  private String emailPaidFlag = "";
  private String prospectusPaidFlag = "";
  private String websitePaidFlag = "";
  private String collegeProfileFlag = "";
  private String subjectProfileCount = "";
  private String courseFoundFlag = "";
  private String courseStudyLevel = "";
  private String courseLdcs1Desc = "";
  private String courseSubjectName = "";
  private String courseTariffPoints = "";
  private String courseTariffPointsMore = "";
  private String qualification = "";
  private String collegeLocation = "";
  private String hyperLinkSeoURL = "";
  private String nextOpenDay = "";
  private String prospectusExternalUrl = "";
  //
  private String departmentName = "";
  private String courseType = "";
  private String timeTable = "";
  private String deparmentContact = "";
  private String numberOfStudentsForDepartment = "";
  private String numberOfAcademicsInDepartment = "";
  private String equipmentRequired = "";
  private String addressLine2 = "";
  private String internationalEntryRequirement = "";
  private String courseModules = "";
  //wu310_20110517_Syed: newly added to show ed-media fields
  private String grades = "";
  private String entryRequirementDescriptionUG = "";
  private String entryRequirementDescriptionPG = "";
  private String degreeNeeded = "";
  //
  private ArrayList listOfCourseOpertunities = new ArrayList();
  
  //w319 course search pod propertites
    private String studyModeId = "";
    private String studyModeValue = "";
    private String studyLevelId = "";
    private String courseName = "";
    private String clearingFlag = "";  
    private List studyLevelList = null;
    private List studyModeList = null;
  //wu501_29012012 added by Sekhar K
  private String opportunityId = null;
  private String checkApplyFlag = null;
  
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public String getCourseTitle() {
    return courseTitle;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCourseDuration(String courseDuration) {
    this.courseDuration = courseDuration;
  }

  public String getCourseDuration() {
    return courseDuration;
  }

  public void setStudyModeDesc(String studyModeDesc) {
    this.studyModeDesc = studyModeDesc;
  }

  public String getStudyModeDesc() {
    return studyModeDesc;
  }

  public void setUcasCode(String ucasCode) {
    this.ucasCode = ucasCode;
  }

  public String getUcasCode() {
    return ucasCode;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setCourseQualification(String courseQualification) {
    this.courseQualification = courseQualification;
  }

  public String getCourseQualification() {
    return courseQualification;
  }

  public void setCourseVenue1(String courseVenue1) {
    this.courseVenue1 = courseVenue1;
  }

  public String getCourseVenue1() {
    return courseVenue1;
  }

  public void setCourseVenue2(String courseVenue2) {
    this.courseVenue2 = courseVenue2;
  }

  public String getCourseVenue2() {
    return courseVenue2;
  }

  public void setAwardingBody(String awardingBody) {
    this.awardingBody = awardingBody;
  }

  public String getAwardingBody() {
    return awardingBody;
  }

  public void setCourseFees(String courseFees) {
    this.courseFees = courseFees;
  }

  public String getCourseFees() {
    return courseFees;
  }

  public void setAssessment(String assessment) {
    this.assessment = assessment;
  }

  public String getAssessment() {
    return assessment;
  }

  public void setEntryRequirement(String entryRequirement) {
    this.entryRequirement = entryRequirement;
  }

  public String getEntryRequirement() {
    return entryRequirement;
  }

  public void setVenueDescription(String venueDescription) {
    this.venueDescription = venueDescription;
  }

  public String getVenueDescription() {
    return venueDescription;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getTown() {
    return town;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  public String getCounty() {
    return county;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setTelePhone(String telePhone) {
    this.telePhone = telePhone;
  }

  public String getTelePhone() {
    return telePhone;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getFax() {
    return fax;
  }

  public void setWebSite(String webSite) {
    this.webSite = webSite;
  }

  public String getWebSite() {
    return webSite;
  }

  public void setContactDetails(String contactDetails) {
    this.contactDetails = contactDetails;
  }

  public String getContactDetails() {
    return contactDetails;
  }

  public void setContactAddress(String contactAddress) {
    this.contactAddress = contactAddress;
  }

  public String getContactAddress() {
    return contactAddress;
  }

  public void setNoOfReviews(String noOfReviews) {
    this.noOfReviews = noOfReviews;
  }

  public String getNoOfReviews() {
    return noOfReviews;
  }

  public void setCourseSummary(String courseSummary) {
    this.courseSummary = courseSummary;
  }

  public String getCourseSummary() {
    return courseSummary;
  }

  public void setCourseInBasket(String courseInBasket) {
    this.courseInBasket = courseInBasket;
  }

  public String getCourseInBasket() {
    return courseInBasket;
  }

  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }

  public String getCollegeLogo() {
    return collegeLogo;
  }

  public void setCourseContact(String courseContact) {
    this.courseContact = courseContact;
  }

  public String getCourseContact() {
    return courseContact;
  }

  public void setRegistrarEmail(String registrarEmail) {
    this.registrarEmail = registrarEmail;
  }

  public String getRegistrarEmail() {
    return registrarEmail;
  }

  public void setEmailSubject(String emailSubject) {
    this.emailSubject = emailSubject;
  }

  public String getEmailSubject() {
    return emailSubject;
  }

  public void setEmailMessage(String emailMessage) {
    this.emailMessage = emailMessage;
  }

  public String getEmailMessage() {
    return emailMessage;
  }

  public void setCourseFullDescription(String courseFullDescription) {
    this.courseFullDescription = courseFullDescription;
  }

  public String getCourseFullDescription() {
    return courseFullDescription;
  }

  public void setLastUpdateDate(String lastUpdateDate) {
    this.lastUpdateDate = lastUpdateDate;
  }

  public String getLastUpdateDate() {
    return lastUpdateDate;
  }

  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }

  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }

  public void setCollegeInBasket(String collegeInBasket) {
    this.collegeInBasket = collegeInBasket;
  }

  public String getCollegeInBasket() {
    return collegeInBasket;
  }

  public void setAdmissionEmail(String admissionEmail) {
    this.admissionEmail = admissionEmail;
  }

  public String getAdmissionEmail() {
    return admissionEmail;
  }

  public void setAdvertiserFlag(String advertiserFlag) {
    this.advertiserFlag = advertiserFlag;
  }

  public String getAdvertiserFlag() {
    return advertiserFlag;
  }

  public void setEmailPaidFlag(String emailPaidFlag) {
    this.emailPaidFlag = emailPaidFlag;
  }

  public String getEmailPaidFlag() {
    return emailPaidFlag;
  }

  public void setProspectusPaidFlag(String prospectusPaidFlag) {
    this.prospectusPaidFlag = prospectusPaidFlag;
  }

  public String getProspectusPaidFlag() {
    return prospectusPaidFlag;
  }

  public void setWebsitePaidFlag(String websitePaidFlag) {
    this.websitePaidFlag = websitePaidFlag;
  }

  public String getWebsitePaidFlag() {
    return websitePaidFlag;
  }

  public void setCollegeProfileFlag(String collegeProfileFlag) {
    this.collegeProfileFlag = collegeProfileFlag;
  }

  public String getCollegeProfileFlag() {
    return collegeProfileFlag;
  }

  public void setSubjectProfileCount(String subjectProfileCount) {
    this.subjectProfileCount = subjectProfileCount;
  }

  public String getSubjectProfileCount() {
    return subjectProfileCount;
  }

  public void setCourseFoundFlag(String courseFoundFlag) {
    this.courseFoundFlag = courseFoundFlag;
  }

  public String getCourseFoundFlag() {
    return courseFoundFlag;
  }

  public void setCourseStudyLevel(String courseStudyLevel) {
    this.courseStudyLevel = courseStudyLevel;
  }

  public String getCourseStudyLevel() {
    return courseStudyLevel;
  }

  public void setCourseSubjectName(String courseSubjectName) {
    this.courseSubjectName = courseSubjectName;
  }

  public String getCourseSubjectName() {
    return courseSubjectName;
  }

  public void setCourseTariffPoints(String courseTariffPoints) {
    this.courseTariffPoints = courseTariffPoints;
  }

  public String getCourseTariffPoints() {
    return courseTariffPoints;
  }

  public void setQualification(String qualification) {
    this.qualification = qualification;
  }

  public String getQualification() {
    return qualification;
  }

  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }

  public String getCollegeLocation() {
    return collegeLocation;
  }

  public void setHyperLinkSeoURL(String hyperLinkSeoURL) {
    this.hyperLinkSeoURL = hyperLinkSeoURL;
  }

  public String getHyperLinkSeoURL() {
    return hyperLinkSeoURL;
  }

  public void setNextOpenDay(String nextOpenDay) {
    this.nextOpenDay = nextOpenDay;
  }

  public String getNextOpenDay() {
    return nextOpenDay;
  }

  public void setProspectusExternalUrl(String prospectusExternalUrl) {
    this.prospectusExternalUrl = prospectusExternalUrl;
  }

  public String getProspectusExternalUrl() {
    return prospectusExternalUrl;
  }

  public void setListOfCourseOpertunities(ArrayList listOfCourseOpertunities) {
    this.listOfCourseOpertunities = listOfCourseOpertunities;
  }

  public ArrayList getListOfCourseOpertunities() {
    return listOfCourseOpertunities;
  }

  public void setDepartmentName(String departmentName) {
    this.departmentName = departmentName;
  }

  public String getDepartmentName() {
    return departmentName;
  }

  public void setCourseType(String courseType) {
    this.courseType = courseType;
  }

  public String getCourseType() {
    return courseType;
  }

  public void setTimeTable(String timeTable) {
    this.timeTable = timeTable;
  }

  public String getTimeTable() {
    return timeTable;
  }

  public void setDeparmentContact(String deparmentContact) {
    this.deparmentContact = deparmentContact;
  }

  public String getDeparmentContact() {
    return deparmentContact;
  }

  public void setNumberOfStudentsForDepartment(String numberOfStudentsForDepartment) {
    this.numberOfStudentsForDepartment = numberOfStudentsForDepartment;
  }

  public String getNumberOfStudentsForDepartment() {
    return numberOfStudentsForDepartment;
  }

  public void setNumberOfAcademicsInDepartment(String numberOfAcademicsInDepartment) {
    this.numberOfAcademicsInDepartment = numberOfAcademicsInDepartment;
  }

  public String getNumberOfAcademicsInDepartment() {
    return numberOfAcademicsInDepartment;
  }

  public void setEquipmentRequired(String equipmentRequired) {
    this.equipmentRequired = equipmentRequired;
  }

  public String getEquipmentRequired() {
    return equipmentRequired;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  public String getAddressLine2() {
    return addressLine2;
  }

  public void setInternationalEntryRequirement(String internationalEntryRequirement) {
    this.internationalEntryRequirement = internationalEntryRequirement;
  }

  public String getInternationalEntryRequirement() {
    return internationalEntryRequirement;
  }

  public void setCourseModules(String courseModules) {
    this.courseModules = courseModules;
  }

  public String getCourseModules() {
    return courseModules;
  }

  public void setCourseTariffPointsMore(String courseTariffPointsMore) {
    this.courseTariffPointsMore = courseTariffPointsMore;
  }

  public String getCourseTariffPointsMore() {
    return courseTariffPointsMore;
  }

  public void setGrades(String grades) {
    this.grades = grades;
  }

  public String getGrades() {
    return grades;
  }

  public void setEntryRequirementDescriptionUG(String entryRequirementDescriptionUG) {
    this.entryRequirementDescriptionUG = entryRequirementDescriptionUG;
  }

  public String getEntryRequirementDescriptionUG() {
    return entryRequirementDescriptionUG;
  }

  public void setEntryRequirementDescriptionPG(String entryRequirementDescriptionPG) {
    this.entryRequirementDescriptionPG = entryRequirementDescriptionPG;
  }

  public String getEntryRequirementDescriptionPG() {
    return entryRequirementDescriptionPG;
  }

  public void setDegreeNeeded(String degreeNeeded) {
    this.degreeNeeded = degreeNeeded;
  }

  public String getDegreeNeeded() {
    return degreeNeeded;
  }

  public void setCourseLdcs1Desc(String courseLdcs1Desc) {
    this.courseLdcs1Desc = courseLdcs1Desc;
  }

  public String getCourseLdcs1Desc() {
    return courseLdcs1Desc;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

    public void setStudyModeId(String studyModeId) {
        this.studyModeId = studyModeId;
    }

    public String getStudyModeId() {
        return studyModeId;
    }

    public void setStudyModeValue(String studyModeValue) {
        this.studyModeValue = studyModeValue;
    }

    public String getStudyModeValue() {
        return studyModeValue;
    }

    public void setStudyLevelId(String studyLevelId) {
        this.studyLevelId = studyLevelId;
    }

    public String getStudyLevelId() {
        return studyLevelId;
    }

    public void setStudyLevelList(List studyLevelList) {
        this.studyLevelList = studyLevelList;
    }

    public List getStudyLevelList() {
        return studyLevelList;
    }

    public void setStudyModeList(List studyModeList) {
        this.studyModeList = studyModeList;
    }

    public List getStudyModeList() {
        return studyModeList;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

  public void setOpportunityId(String opportunityId) {
    this.opportunityId = opportunityId;
  }

  public String getOpportunityId() {
    return opportunityId;
  }
  public void setClearingFlag(String clearingFlag) {
    this.clearingFlag = clearingFlag;
  }
  public String getClearingFlag() {
    return clearingFlag;
  }


public String getCheckApplyFlag() {
   return checkApplyFlag;
}


public void setCheckApplyFlag(String checkApplyFlag) {
   this.checkApplyFlag = checkApplyFlag;
}
}
////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////    
