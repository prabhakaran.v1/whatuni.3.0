package WUI.search.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class CollegeProfileBean {

  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = ""; 
  private String collegeImagePath = "";
  private String profileImagePath = "";
  private String profileDescription = "";
  private String showOverViewTab = "";
  private String showAccommodationTab = "";
  private String showEntertainmentTab = "";
  private String showCoursesFacilitiesTab = "";
  private String showWelfareTab = "";
  private String showFeesFundingTab = "";
  private String showJobProspectsTab = "";
  private String showContactTab = "";
  private String videoUrl = "";
  private String videoThumbnailUrl = "";
  private String jsStatsLogId = "";
  private String dLogTypeKey = "";
  private String videoId = "";
  private String selectedSectionName = "";
  private String orderItemId = "";
  private String subOrderItemId = "";
  private String subOrderWebsite = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";
  private List studyLevelList = null;
  private List studyModeList = null;
  private String videoMetaDuration = null;
  private String profileId = null;//24_JUN_2014_REL
  private String myhcProfileId = null;
  //
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeImagePath(String collegeImagePath) {
    this.collegeImagePath = collegeImagePath;
  }

  public String getCollegeImagePath() {
    return collegeImagePath;
  }

  public void setProfileImagePath(String profileImagePath) {
    this.profileImagePath = profileImagePath;
  }

  public String getProfileImagePath() {
    return profileImagePath;
  }

  public void setProfileDescription(String profileDescription) {
    this.profileDescription = profileDescription;
  }

  public String getProfileDescription() {
    return profileDescription;
  }

  public void setShowOverViewTab(String showOverViewTab) {
    this.showOverViewTab = showOverViewTab;
  }

  public String getShowOverViewTab() {
    return showOverViewTab;
  }

  public void setShowAccommodationTab(String showAccommodationTab) {
    this.showAccommodationTab = showAccommodationTab;
  }

  public String getShowAccommodationTab() {
    return showAccommodationTab;
  }

  public void setShowEntertainmentTab(String showEntertainmentTab) {
    this.showEntertainmentTab = showEntertainmentTab;
  }

  public String getShowEntertainmentTab() {
    return showEntertainmentTab;
  }

  public void setShowCoursesFacilitiesTab(String showCoursesFacilitiesTab) {
    this.showCoursesFacilitiesTab = showCoursesFacilitiesTab;
  }

  public String getShowCoursesFacilitiesTab() {
    return showCoursesFacilitiesTab;
  }

  public void setShowWelfareTab(String showWelfareTab) {
    this.showWelfareTab = showWelfareTab;
  }

  public String getShowWelfareTab() {
    return showWelfareTab;
  }

  public void setShowFeesFundingTab(String showFeesFundingTab) {
    this.showFeesFundingTab = showFeesFundingTab;
  }

  public String getShowFeesFundingTab() {
    return showFeesFundingTab;
  }

  public void setShowJobProspectsTab(String showJobProspectsTab) {
    this.showJobProspectsTab = showJobProspectsTab;
  }

  public String getShowJobProspectsTab() {
    return showJobProspectsTab;
  }

  public void setShowContactTab(String showContactTab) {
    this.showContactTab = showContactTab;
  }

  public String getShowContactTab() {
    return showContactTab;
  }

  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }

  public String getVideoUrl() {
    return videoUrl;
  }

  public void setVideoThumbnailUrl(String videoThumbnailUrl) {
    this.videoThumbnailUrl = videoThumbnailUrl;
  }

  public String getVideoThumbnailUrl() {
    return videoThumbnailUrl;
  }

  public void setJsStatsLogId(String jsStatsLogId) {
    this.jsStatsLogId = jsStatsLogId;
  }

  public String getJsStatsLogId() {
    return jsStatsLogId;
  }

  public void setDLogTypeKey(String dLogTypeKey) {
    this.dLogTypeKey = dLogTypeKey;
  }

  public String getDLogTypeKey() {
    return dLogTypeKey;
  }

  public void setVideoId(String videoId) {
    this.videoId = videoId;
  }

  public String getVideoId() {
    return videoId;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setSelectedSectionName(String selectedSectionName) {
    this.selectedSectionName = selectedSectionName;
  }

  public String getSelectedSectionName() {
    return selectedSectionName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setStudyLevelList(List studyLevelList) {
    this.studyLevelList = studyLevelList;
  }
  public List getStudyLevelList() {
    return studyLevelList;
  }
  public void setStudyModeList(List studyModeList) {
    this.studyModeList = studyModeList;
  }
  public List getStudyModeList() {
    return studyModeList;
  }
  public void setVideoMetaDuration(String videoMetaDuration) {
    this.videoMetaDuration = videoMetaDuration;
  }
  public String getVideoMetaDuration() {
    return videoMetaDuration;
  }
  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }
  public String getProfileId() {
    return profileId;
  }
  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }
  public String getMyhcProfileId() {
    return myhcProfileId;
  }
}
/////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////    
