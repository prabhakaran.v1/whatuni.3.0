package WUI.search.form;

import org.apache.struts.action.ActionForm;

public class CourseOpertunityBean {

  private String courseId = "";
  private String collegeId = "";
  private String price = "";
  private String studyMode = "";
  private String startDate = "";
  private String duration = "";
  private String applicationDeadline = "";
  private String venue = "";
  private String isVenueBelongsToLondonBorough = "";
  private String startDateDesc = "";
  private String applyNowUrl = "";

  public void flush() {
    this.courseId = null;
    this.collegeId = null;
    this.price = null;
    this.studyMode = null;
    this.startDate = null;
    this.duration = null;
    this.applicationDeadline = null;
    this.venue = null;
  }

  public void init() {
    this.courseId = "";
    this.collegeId = "";
    this.price = "";
    this.studyMode = "";
    this.startDate = "";
    this.duration = "";
    this.applicationDeadline = "";
    this.venue = "";
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getPrice() {
    return price;
  }

  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }

  public String getStudyMode() {
    return studyMode;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  public String getDuration() {
    return duration;
  }

  public void setApplicationDeadline(String applicationDeadline) {
    this.applicationDeadline = applicationDeadline;
  }

  public String getApplicationDeadline() {
    return applicationDeadline;
  }

  public void setVenue(String venue) {
    this.venue = venue;
  }

  public String getVenue() {
    return venue;
  }

  public void setIsVenueBelongsToLondonBorough(String isVenueBelongsToLondonBorough) {
    this.isVenueBelongsToLondonBorough = isVenueBelongsToLondonBorough;
  }

  public String getIsVenueBelongsToLondonBorough() {
    return isVenueBelongsToLondonBorough;
  }

  public void setStartDateDesc(String startDateDesc) {
    this.startDateDesc = startDateDesc;
  }

  public String getStartDateDesc() {
    return startDateDesc;
  }

  public void setApplyNowUrl(String applyNowUrl) {
    this.applyNowUrl = applyNowUrl;
  }

  public String getApplyNowUrl() {
    return applyNowUrl;
  }

}
