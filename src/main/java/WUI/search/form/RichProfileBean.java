package WUI.search.form;

import lombok.Data;

@Data
public class RichProfileBean {
  private String collegeId = null;
  private String collegeName = null;
  private String collgeDispName = null;
  private String networkId = null;
  private String profileType = null;
  private String myhcProfileId = null;
  private String requestURL = null;
  private String setProfileType = null;
  private String userJourney;
  private String richOrSubPageUrl = null;
  
}
