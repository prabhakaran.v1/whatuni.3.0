package WUI.search.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

public class SearchResultBean {

  public SearchResultBean() {
  }
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String admissionEmail = "";
  private String collegeSite = "";
  private String addLine1 = "";
  private String addLine2 = "";
  private String town = "";
  private String county = "";
  private String postCode = "";
  private String telephone = "";
  private String fax = "";
  private String totalReviews = "";
  private String collegeLogo = "";
  private String overAllRating = "";
  private String recommendedPercent = "";
  private String studyMode = "";
  private String ucasCode = "";
  private String ucasFlag = "";
  private String startDetails = "";
  private String courseDescription = "";
  private String coursetitle = "";
  private String courseDuration = "";
  private String learnDirectQual = "";
  private String departmentOrFaculty = "";
  private String courseCost = "";
  private String venueName = "";
  private String findRegion = "";
  private String cityName = "";
  private String accomodation = "";
  private String studentUnion = "";
  private String profile = "";
  private String Prospectus = "";
  private List courseList = null;
  private List collegeList = null;
  private String noOfCourses = "";
  private String learndirQualsForThisCollege = ""; //wu312_20110614_Syed: all related qualification fetch to form non link text, PANDA fix      
  private String noOfReviews = "";
  private List filterOptionList = null;
  private String optionId = "";
  private String optionDesc = "";
  private String filterId = "";
  private String orderById = "";
  private List orderByList = null;
  private String contactDetail = "";
  private String contactAddress = "";
  private String shortListFlag = "";
  private String courseInBasket = "";
  private String collegeInBasket = "";
  private String webSite = "";
  private String collegeProfileFlag = "";
  private String emailPaidFlag = "";
  private String prospectusPaidFlag = "";
  private String subProfilePurchaseFlag = "";
  private String websitePaidFlag = "";
  private String subjectProfileCount = "";
  private String categoryCode = "";
  private String courseFoundFlag = ""; // to delete in next release after 30.09.2008
  /////////////////FOR Uni search Autocomplete ////
  private String college_Name = "";
  private String searchCollegeId = "";
  private String courseid = "";
  private String courseId = "";
  private String seoStudyLevelText = "";
  private String videoUrl = "";
  private String videoThumbnail = "";
  private String videoDesc = "";
  private String videoReviewId = "";
  private String overviewShortDesc = "";
  private String advertisorFlag = "";
  private String profileCount = "";
  private String courseTitle = "";
  private String allCourseCount = "";
  private String pgCourseCount = "";
  private String degreeCourseCount = "";
  private String accessFoundationCount = "";
  private String foundationCount = "";
  private String hncHndCount = "";
  private String profileVideoUrl = "";
  private String collegeLocation = "";
  private String subjectCategoryCode = "";
  private String studentsSatisfication = "";
  private String studentsJob = "";
  private String timesLeaguePosition = "";
  private String subjectVideoUrl = "";
  private String latestCollegeUserImage = "";
  private String latestCollegeUserThumbImage = "";
  private String intlStudRatio = "";
  private String maleFemaleRatio = "";
  private String dropoutRate = "";
  private String avgUcasPoints = "";
  private String imageName = "";
  private String imageShortName = "";
  private String author = "";
  private String scholarshipCount = "";
  private String showMoreLink = "";
  private String prospectusExternalUrl = "";
  private String profileShortText = "";
  private String subjectProfile = "";
  private String reviewCount = "";
  private String qualificationCode = "";
  private String qualCodeSeo = "";
  private String qualification = "";
  private String categoryDesc = "";
  private String ldcsLevel = "";
  private String logoId = "";
  private String logoType = "";
  private String videoThumbUrl = "";
  private String studentStaffRatio = "";
  private String ImageThumbPath = "";
  private String lattitude = "";
  private String longtitude = "";
  private String accomodationFees = "";
  private String subjectTimesRanking = "";
  private String statDisplayButton = "";
  private String locationMapImagePath = "";
  private String ucasPoint = "";
  private String availableStudyModes = "";
  //CPE related properties
  private String orderItemId = "";
  private String subOrderItemId = "";
  private String myhcProfileId = "";
  private String advertName = "";
  private String mediaPath = "";
  private String mediaTypeId = "";
  private String mediaId = "";
  private String thumbnailMediaPath = "";
  private String thumbnailMediaTypeId = "";
  private String searchThumbnail = "";
  private String interactivityId = "";
  private String whichProfleBoosted = "";
  private String isIpExists = "";
  private String isSpExists = "";
  private String cpeQualificationNetworkId = "";
  private String cpeQualificationNetworkDesc = "";
  private String spOrderItemId = "";
  private String ipOrderItemId = "";
  private String mychPrifileIdForSp = "";
  private String mychPrifileIdForIp = "";
  //Button related properties
  //private String subOrderItemId = "";
  private String subOrderWebsite = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";
  private String advertiserflag = "";
  private String subOrderProspectusTargetURL = "";
  //money page region specific uk map related: wu303_20110222
  private String regionName = "";
  private String regionSpecificUkMapCssClass = "";
  //wu310_20110517_Syed: for PANDA related fix: next-open-day will be shown in money page: 
  private String nextOpenDay = "";
  private ArrayList whatStudentsAreSayingAboutThisCollegeList = null;
  //wu314_20110712_Syed: Stacey-functional-crd#1 - linking to clearing-micro-site
  private ArrayList clearingInfoList = null;
  private String fees = "";
  private String courseDetailUrl = "";
  private String feeDuration = "";
  private String hotLineNo="";
  private String placesAvailable = "";
  private String lastUpdatedDate = "";
  private String webformPrice = null;
  private String websitePrice = null;

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setAdmissionEmail(String admissionEmail) {
    this.admissionEmail = admissionEmail;
  }

  public String getAdmissionEmail() {
    return admissionEmail;
  }

  public void setCollegeSite(String collegeSite) {
    this.collegeSite = collegeSite;
  }

  public String getCollegeSite() {
    return collegeSite;
  }

  public void setAddLine1(String addLine1) {
    this.addLine1 = addLine1;
  }

  public String getAddLine1() {
    return addLine1;
  }

  public void setAddLine2(String addLine2) {
    this.addLine2 = addLine2;
  }

  public String getAddLine2() {
    return addLine2;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getTown() {
    return town;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  public String getCounty() {
    return county;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setFax(String fax) {
    this.fax = fax;
  }

  public String getFax() {
    return fax;
  }

  public void setTotalReviews(String totalReviews) {
    this.totalReviews = totalReviews;
  }

  public String getTotalReviews() {
    return totalReviews;
  }

  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }

  public String getCollegeLogo() {
    return collegeLogo;
  }

  public void setOverAllRating(String overAllRating) {
    this.overAllRating = overAllRating;
  }

  public String getOverAllRating() {
    return overAllRating;
  }

  public void setRecommendedPercent(String recommendedPercent) {
    this.recommendedPercent = recommendedPercent;
  }

  public String getRecommendedPercent() {
    return recommendedPercent;
  }

  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }

  public String getStudyMode() {
    return studyMode;
  }

  public void setUcasCode(String ucasCode) {
    this.ucasCode = ucasCode;
  }

  public String getUcasCode() {
    return ucasCode;
  }

  public void setUcasFlag(String ucasFlag) {
    this.ucasFlag = ucasFlag;
  }

  public String getUcasFlag() {
    return ucasFlag;
  }

  public void setStartDetails(String startDetails) {
    this.startDetails = startDetails;
  }

  public String getStartDetails() {
    return startDetails;
  }

  public void setCourseDescription(String courseDescription) {
    this.courseDescription = courseDescription;
  }

  public String getCourseDescription() {
    return courseDescription;
  }

  public void setCoursetitle(String coursetitle) {
    this.coursetitle = coursetitle;
  }

  public String getCoursetitle() {
    return coursetitle;
  }

  public void setCourseDuration(String courseDuration) {
    this.courseDuration = courseDuration;
  }

  public String getCourseDuration() {
    return courseDuration;
  }

  public void setLearnDirectQual(String learnDirectQual) {
    this.learnDirectQual = learnDirectQual;
  }

  public String getLearnDirectQual() {
    return learnDirectQual;
  }

  public void setCourseCost(String courseCost) {
    this.courseCost = courseCost;
  }

  public String getCourseCost() {
    return courseCost;
  }

  public void setVenueName(String venueName) {
    this.venueName = venueName;
  }

  public String getVenueName() {
    return venueName;
  }

  public void setFindRegion(String findRegion) {
    this.findRegion = findRegion;
  }

  public String getFindRegion() {
    return findRegion;
  }

  public void setCityName(String cityName) {
    this.cityName = cityName;
  }

  public String getCityName() {
    return cityName;
  }

  public void setAccomodation(String accomodation) {
    this.accomodation = accomodation;
  }

  public String getAccomodation() {
    return accomodation;
  }

  public void setStudentUnion(String studentUnion) {
    this.studentUnion = studentUnion;
  }

  public String getStudentUnion() {
    return studentUnion;
  }

  public void setProfile(String profile) {
    this.profile = profile;
  }

  public String getProfile() {
    return profile;
  }

  public void setProspectus(String prospectus) {
    this.Prospectus = prospectus;
  }

  public String getProspectus() {
    return Prospectus;
  }

  public void setCourseList(List courseList) {
    this.courseList = courseList;
  }

  public List getCourseList() {
    return courseList;
  }

  public void setCollegeList(List collegeList) {
    this.collegeList = collegeList;
  }

  public List getCollegeList() {
    return collegeList;
  }

  public void setNoOfCourses(String noOfCourses) {
    this.noOfCourses = noOfCourses;
  }

  public String getNoOfCourses() {
    return noOfCourses;
  }

  public void setNoOfReviews(String noOfReviews) {
    this.noOfReviews = noOfReviews;
  }

  public String getNoOfReviews() {
    return noOfReviews;
  }

  public void setFilterOptionList(List filterOptionList) {
    this.filterOptionList = filterOptionList;
  }

  public List getFilterOptionList() {
    return filterOptionList;
  }

  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }

  public String getOptionId() {
    return optionId;
  }

  public void setOptionDesc(String optionDesc) {
    this.optionDesc = optionDesc;
  }

  public String getOptionDesc() {
    return optionDesc;
  }

  public void setFilterId(String filterId) {
    this.filterId = filterId;
  }

  public String getFilterId() {
    return filterId;
  }

  public void setOrderById(String orderById) {
    this.orderById = orderById;
  }

  public String getOrderById() {
    return orderById;
  }

  public void setOrderByList(List orderByList) {
    this.orderByList = orderByList;
  }

  public List getOrderByList() {
    return orderByList;
  }

  public void setContactDetail(String contactDetail) {
    this.contactDetail = contactDetail;
  }

  public String getContactDetail() {
    return contactDetail;
  }

  public void setContactAddress(String contactAddress) {
    this.contactAddress = contactAddress;
  }

  public String getContactAddress() {
    return contactAddress;
  }

  public void setShortListFlag(String shortListFlag) {
    this.shortListFlag = shortListFlag;
  }

  public String getShortListFlag() {
    return shortListFlag;
  }

  public void setCourseInBasket(String courseInBasket) {
    this.courseInBasket = courseInBasket;
  }

  public String getCourseInBasket() {
    return courseInBasket;
  }

  public void setCollegeInBasket(String collegeInBasket) {
    this.collegeInBasket = collegeInBasket;
  }

  public String getCollegeInBasket() {
    return collegeInBasket;
  }

  public void setWebSite(String webSite) {
    this.webSite = webSite;
  }

  public String getWebSite() {
    return webSite;
  }

  public void setCollegeProfileFlag(String collegeProfileFlag) {
    this.collegeProfileFlag = collegeProfileFlag;
  }

  public String getCollegeProfileFlag() {
    return collegeProfileFlag;
  }

  public void setEmailPaidFlag(String emailPaidFlag) {
    this.emailPaidFlag = emailPaidFlag;
  }

  public String getEmailPaidFlag() {
    return emailPaidFlag;
  }

  public void setProspectusPaidFlag(String prospectusPaidFlag) {
    this.prospectusPaidFlag = prospectusPaidFlag;
  }

  public String getProspectusPaidFlag() {
    return prospectusPaidFlag;
  }

  public void setSubProfilePurchaseFlag(String subProfilePurchaseFlag) {
    this.subProfilePurchaseFlag = subProfilePurchaseFlag;
  }

  public String getSubProfilePurchaseFlag() {
    return subProfilePurchaseFlag;
  }

  public void setWebsitePaidFlag(String websitePaidFlag) {
    this.websitePaidFlag = websitePaidFlag;
  }

  public String getWebsitePaidFlag() {
    return websitePaidFlag;
  }

  public void setSubjectProfileCount(String subjectProfileCount) {
    this.subjectProfileCount = subjectProfileCount;
  }

  public String getSubjectProfileCount() {
    return subjectProfileCount;
  }

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setCourseFoundFlag(String courseFoundFlag) {
    this.courseFoundFlag = courseFoundFlag;
  }

  public String getCourseFoundFlag() {
    return courseFoundFlag;
  }

  public void setCollege_Name(String college_Name) {
    this.college_Name = college_Name;
  }

  public String getCollege_Name() {
    return college_Name;
  }

  public void setSearchCollegeId(String searchCollegeId) {
    this.searchCollegeId = searchCollegeId;
  }

  public String getSearchCollegeId() {
    return searchCollegeId;
  }

  public void setCourseid(String courseid) {
    this.courseid = courseid;
  }

  public String getCourseid() {
    return courseid;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }

  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }

  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }

  public String getVideoUrl() {
    return videoUrl;
  }

  public void setVideoThumbnail(String videoThumbnail) {
    this.videoThumbnail = videoThumbnail;
  }

  public String getVideoThumbnail() {
    return videoThumbnail;
  }

  public void setVideoDesc(String videoDesc) {
    this.videoDesc = videoDesc;
  }

  public String getVideoDesc() {
    return videoDesc;
  }

  public void setVideoReviewId(String videoReviewId) {
    this.videoReviewId = videoReviewId;
  }

  public String getVideoReviewId() {
    return videoReviewId;
  }

  public void setOverviewShortDesc(String overviewShortDesc) {
    this.overviewShortDesc = overviewShortDesc;
  }

  public String getOverviewShortDesc() {
    return overviewShortDesc;
  }

  public void setAdvertisorFlag(String advertisorFlag) {
    this.advertisorFlag = advertisorFlag;
  }

  public String getAdvertisorFlag() {
    return advertisorFlag;
  }

  public void setProfileCount(String profileCount) {
    this.profileCount = profileCount;
  }

  public String getProfileCount() {
    return profileCount;
  }

  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public String getCourseTitle() {
    return courseTitle;
  }

  public void setAllCourseCount(String allCourseCount) {
    this.allCourseCount = allCourseCount;
  }

  public String getAllCourseCount() {
    return allCourseCount;
  }

  public void setPgCourseCount(String pgCourseCount) {
    this.pgCourseCount = pgCourseCount;
  }

  public String getPgCourseCount() {
    return pgCourseCount;
  }

  public void setDegreeCourseCount(String degreeCourseCount) {
    this.degreeCourseCount = degreeCourseCount;
  }

  public String getDegreeCourseCount() {
    return degreeCourseCount;
  }

  public void setAccessFoundationCount(String accessFoundationCount) {
    this.accessFoundationCount = accessFoundationCount;
  }

  public String getAccessFoundationCount() {
    return accessFoundationCount;
  }

  public void setFoundationCount(String foundationCount) {
    this.foundationCount = foundationCount;
  }

  public String getFoundationCount() {
    return foundationCount;
  }

  public void setHncHndCount(String hncHndCount) {
    this.hncHndCount = hncHndCount;
  }

  public String getHncHndCount() {
    return hncHndCount;
  }

  public void setProfileVideoUrl(String profileVideoUrl) {
    this.profileVideoUrl = profileVideoUrl;
  }

  public String getProfileVideoUrl() {
    return profileVideoUrl;
  }

  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }

  public String getCollegeLocation() {
    return collegeLocation;
  }

  public void setSubjectCategoryCode(String subjectCategoryCode) {
    this.subjectCategoryCode = subjectCategoryCode;
  }

  public String getSubjectCategoryCode() {
    return subjectCategoryCode;
  }

  public void setStudentsSatisfication(String studentsSatisfication) {
    this.studentsSatisfication = studentsSatisfication;
  }

  public String getStudentsSatisfication() {
    return studentsSatisfication;
  }

  public void setStudentsJob(String studentsJob) {
    this.studentsJob = studentsJob;
  }

  public String getStudentsJob() {
    return studentsJob;
  }

  public void setTimesLeaguePosition(String timesLeaguePosition) {
    this.timesLeaguePosition = timesLeaguePosition;
  }

  public String getTimesLeaguePosition() {
    return timesLeaguePosition;
  }

  public void setSubjectVideoUrl(String subjectVideoUrl) {
    this.subjectVideoUrl = subjectVideoUrl;
  }

  public String getSubjectVideoUrl() {
    return subjectVideoUrl;
  }

  public void setLatestCollegeUserImage(String latestCollegeUserImage) {
    this.latestCollegeUserImage = latestCollegeUserImage;
  }

  public String getLatestCollegeUserImage() {
    return latestCollegeUserImage;
  }

  public void setLatestCollegeUserThumbImage(String latestCollegeUserThumbImage) {
    this.latestCollegeUserThumbImage = latestCollegeUserThumbImage;
  }

  public String getLatestCollegeUserThumbImage() {
    return latestCollegeUserThumbImage;
  }

  public void setIntlStudRatio(String intlStudRatio) {
    this.intlStudRatio = intlStudRatio;
  }

  public String getIntlStudRatio() {
    return intlStudRatio;
  }

  public void setMaleFemaleRatio(String maleFemaleRatio) {
    this.maleFemaleRatio = maleFemaleRatio;
  }

  public String getMaleFemaleRatio() {
    return maleFemaleRatio;
  }

  public void setDropoutRate(String dropoutRate) {
    this.dropoutRate = dropoutRate;
  }

  public String getDropoutRate() {
    return dropoutRate;
  }

  public void setAvgUcasPoints(String avgUcasPoints) {
    this.avgUcasPoints = avgUcasPoints;
  }

  public String getAvgUcasPoints() {
    return avgUcasPoints;
  }

  public void setImageName(String imageName) {
    this.imageName = imageName;
  }

  public String getImageName() {
    return imageName;
  }

  public void setImageShortName(String imageShortName) {
    this.imageShortName = imageShortName;
  }

  public String getImageShortName() {
    return imageShortName;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getAuthor() {
    return author;
  }

  public void setScholarshipCount(String scholarshipCount) {
    this.scholarshipCount = scholarshipCount;
  }

  public String getScholarshipCount() {
    return scholarshipCount;
  }

  public void setShowMoreLink(String showMoreLink) {
    this.showMoreLink = showMoreLink;
  }

  public String getShowMoreLink() {
    return showMoreLink;
  }

  public void setProspectusExternalUrl(String prospectusExternalUrl) {
    this.prospectusExternalUrl = prospectusExternalUrl;
  }

  public String getProspectusExternalUrl() {
    return prospectusExternalUrl;
  }

  public void setProfileShortText(String profileShortText) {
    this.profileShortText = profileShortText;
  }

  public String getProfileShortText() {
    return profileShortText;
  }

  public void setSubjectProfile(String subjectProfile) {
    this.subjectProfile = subjectProfile;
  }

  public String getSubjectProfile() {
    return subjectProfile;
  }

  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }

  public String getReviewCount() {
    return reviewCount;
  }

  public void setQualificationCode(String qualificationCode) {
    this.qualificationCode = qualificationCode;
  }

  public String getQualificationCode() {
    return qualificationCode;
  }

  public void setQualCodeSeo(String qualCodeSeo) {
    this.qualCodeSeo = qualCodeSeo;
  }

  public String getQualCodeSeo() {
    return qualCodeSeo;
  }

  public void setQualification(String qualification) {
    this.qualification = qualification;
  }

  public String getQualification() {
    return qualification;
  }

  public void setCategoryDesc(String categoryDesc) {
    this.categoryDesc = categoryDesc;
  }

  public String getCategoryDesc() {
    return categoryDesc;
  }

  public void setLdcsLevel(String ldcsLevel) {
    this.ldcsLevel = ldcsLevel;
  }

  public String getLdcsLevel() {
    return ldcsLevel;
  }

  public void setLogoId(String logoId) {
    this.logoId = logoId;
  }

  public String getLogoId() {
    return logoId;
  }

  public void setLogoType(String logoType) {
    this.logoType = logoType;
  }

  public String getLogoType() {
    return logoType;
  }

  public void setVideoThumbUrl(String videoThumbUrl) {
    this.videoThumbUrl = videoThumbUrl;
  }

  public String getVideoThumbUrl() {
    return videoThumbUrl;
  }

  public void setStudentStaffRatio(String studentStaffRatio) {
    this.studentStaffRatio = studentStaffRatio;
  }

  public String getStudentStaffRatio() {
    return studentStaffRatio;
  }

  public void setImageThumbPath(String imageThumbPath) {
    this.ImageThumbPath = imageThumbPath;
  }

  public String getImageThumbPath() {
    return ImageThumbPath;
  }

  public void setLattitude(String lattitude) {
    this.lattitude = lattitude;
  }

  public String getLattitude() {
    return lattitude;
  }

  public void setLongtitude(String longtitude) {
    this.longtitude = longtitude;
  }

  public String getLongtitude() {
    return longtitude;
  }

  public void setAccomodationFees(String accomodationFees) {
    this.accomodationFees = accomodationFees;
  }

  public String getAccomodationFees() {
    return accomodationFees;
  }

  public void setSubjectTimesRanking(String subjectTimesRanking) {
    this.subjectTimesRanking = subjectTimesRanking;
  }

  public String getSubjectTimesRanking() {
    return subjectTimesRanking;
  }

  public void setStatDisplayButton(String statDisplayButton) {
    this.statDisplayButton = statDisplayButton;
  }

  public String getStatDisplayButton() {
    return statDisplayButton;
  }

  public void setLocationMapImagePath(String locationMapImagePath) {
    this.locationMapImagePath = locationMapImagePath;
  }

  public String getLocationMapImagePath() {
    return locationMapImagePath;
  }

  public void setUcasPoint(String ucasPoint) {
    this.ucasPoint = ucasPoint;
  }

  public String getUcasPoint() {
    return ucasPoint;
  }

  public void setAvailableStudyModes(String availableStudyModes) {
    this.availableStudyModes = availableStudyModes;
  }

  public String getAvailableStudyModes() {
    return availableStudyModes;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }

  public String getAdvertName() {
    return advertName;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setMediaTypeId(String mediaTypeId) {
    this.mediaTypeId = mediaTypeId;
  }

  public String getMediaTypeId() {
    return mediaTypeId;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setThumbnailMediaPath(String thumbnailMediaPath) {
    this.thumbnailMediaPath = thumbnailMediaPath;
  }

  public String getThumbnailMediaPath() {
    return thumbnailMediaPath;
  }

  public void setThumbnailMediaTypeId(String thumbnailMediaTypeId) {
    this.thumbnailMediaTypeId = thumbnailMediaTypeId;
  }

  public String getThumbnailMediaTypeId() {
    return thumbnailMediaTypeId;
  }

  public void setSearchThumbnail(String searchThumbnail) {
    this.searchThumbnail = searchThumbnail;
  }

  public String getSearchThumbnail() {
    return searchThumbnail;
  }

  public void setInteractivityId(String interactivityId) {
    this.interactivityId = interactivityId;
  }

  public String getInteractivityId() {
    return interactivityId;
  }

  public void setWhichProfleBoosted(String whichProfleBoosted) {
    this.whichProfleBoosted = whichProfleBoosted;
  }

  public String getWhichProfleBoosted() {
    return whichProfleBoosted;
  }

  public void setIsIpExists(String isIpExists) {
    this.isIpExists = isIpExists;
  }

  public String getIsIpExists() {
    return isIpExists;
  }

  public void setIsSpExists(String isSpExists) {
    this.isSpExists = isSpExists;
  }

  public String getIsSpExists() {
    return isSpExists;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setCpeQualificationNetworkDesc(String cpeQualificationNetworkDesc) {
    this.cpeQualificationNetworkDesc = cpeQualificationNetworkDesc;
  }

  public String getCpeQualificationNetworkDesc() {
    return cpeQualificationNetworkDesc;
  }

  public void setSpOrderItemId(String spOrderItemId) {
    this.spOrderItemId = spOrderItemId;
  }

  public String getSpOrderItemId() {
    return spOrderItemId;
  }

  public void setIpOrderItemId(String ipOrderItemId) {
    this.ipOrderItemId = ipOrderItemId;
  }

  public String getIpOrderItemId() {
    return ipOrderItemId;
  }

  public void setMychPrifileIdForSp(String mychPrifileIdForSp) {
    this.mychPrifileIdForSp = mychPrifileIdForSp;
  }

  public String getMychPrifileIdForSp() {
    return mychPrifileIdForSp;
  }

  public void setMychPrifileIdForIp(String mychPrifileIdForIp) {
    this.mychPrifileIdForIp = mychPrifileIdForIp;
  }

  public String getMychPrifileIdForIp() {
    return mychPrifileIdForIp;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setAdvertiserflag(String advertiserflag) {
    this.advertiserflag = advertiserflag;
  }

  public String getAdvertiserflag() {
    return advertiserflag;
  }

  public void setSubOrderProspectusTargetURL(String subOrderProspectusTargetURL) {
    this.subOrderProspectusTargetURL = subOrderProspectusTargetURL;
  }

  public String getSubOrderProspectusTargetURL() {
    return subOrderProspectusTargetURL;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }

  public String getRegionName() {
    return regionName;
  }

  public void setRegionSpecificUkMapCssClass(String regionSpecificUkMapCssClass) {
    this.regionSpecificUkMapCssClass = regionSpecificUkMapCssClass;
  }

  public String getRegionSpecificUkMapCssClass() {
    return regionSpecificUkMapCssClass;
  }

  public void setNextOpenDay(String nextOpenDay) {
    this.nextOpenDay = nextOpenDay;
  }

  public String getNextOpenDay() {
    return nextOpenDay;
  }

  public void setWhatStudentsAreSayingAboutThisCollegeList(ArrayList whatStudentsAreSayingAboutThisCollegeList) {
    this.whatStudentsAreSayingAboutThisCollegeList = whatStudentsAreSayingAboutThisCollegeList;
  }

  public ArrayList getWhatStudentsAreSayingAboutThisCollegeList() {
    return whatStudentsAreSayingAboutThisCollegeList;
  }

  public void setLearndirQualsForThisCollege(String learndirQualsForThisCollege) {
    this.learndirQualsForThisCollege = learndirQualsForThisCollege;
  }

  public String getLearndirQualsForThisCollege() {
    return learndirQualsForThisCollege;
  }

  public void setDepartmentOrFaculty(String departmentOrFaculty) {
    this.departmentOrFaculty = departmentOrFaculty;
  }

  public String getDepartmentOrFaculty() {
    return departmentOrFaculty;
  }

  public void setClearingInfoList(ArrayList clearingInfoList) {
    this.clearingInfoList = clearingInfoList;
  }

  public ArrayList getClearingInfoList() {
    return clearingInfoList;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setFees(String fees) {
    this.fees = fees;
  }
  public String getFees() {
    return fees;
  }
  public void setCourseDetailUrl(String courseDetailUrl) {
    this.courseDetailUrl = courseDetailUrl;
  }
  public String getCourseDetailUrl() {
    return courseDetailUrl;
  }
    public void setFeeDuration(String feeDuration) {
        this.feeDuration = feeDuration;
    }
    public String getFeeDuration() {
        return feeDuration;
    }

    public void setHotLineNo(String hotLineNo) {
        this.hotLineNo = hotLineNo;
    }

    public String getHotLineNo() {
        return hotLineNo;
    }

  public void setPlacesAvailable(String placesAvailable) {
    this.placesAvailable = placesAvailable;
  }

  public String getPlacesAvailable() {
    return placesAvailable;
  }

  public void setLastUpdatedDate(String lastUpdatedDate) {
    this.lastUpdatedDate = lastUpdatedDate;
  }

  public String getLastUpdatedDate() {
    return lastUpdatedDate;
  }

  public void setWebformPrice(String webformPrice) {
    this.webformPrice = webformPrice;
  }

  public String getWebformPrice() {
    return webformPrice;
  }

  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }

  public String getWebsitePrice() {
    return websitePrice;
  }

}
