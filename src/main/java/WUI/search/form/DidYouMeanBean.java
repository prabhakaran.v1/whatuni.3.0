package WUI.search.form;

import org.apache.struts.action.ActionForm;

public class DidYouMeanBean {

  public DidYouMeanBean() {}
  
  private String didYouMeanKeyword = "";
  private String urlForDidYouMeanKeyword = "";

  public void setDidYouMeanKeyword(String didYouMeanKeyword) {
    this.didYouMeanKeyword = didYouMeanKeyword;
  }

  public String getDidYouMeanKeyword() {
    return didYouMeanKeyword;
  }

  public void setUrlForDidYouMeanKeyword(String urlForDidYouMeanKeyword) {
    this.urlForDidYouMeanKeyword = urlForDidYouMeanKeyword;
  }

  public String getUrlForDidYouMeanKeyword() {
    return urlForDidYouMeanKeyword;
  }

}
