package WUI.search.form;

import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class GooglemapBean {

  private String latitudeValue = "";
  private List latitudeList = null;
  private String longtitudeValue = "";
  private List longititudeList = null;
  private String placeDetail = "";
  private List placeDetailList = null;
  private String searchLocation = "";
  private String latLanValue = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String postCode = "";
  private String collegeId = "";
  private String googleSearchText = "";
  private String googleDisplayText = "";
  private String goolgeSearchPostCode = "";
  private String optionId = "";
  private String optionDescription = "";
  private String countyName = "";
  private String townName = "";
  private String location = "";
  private String mapCenterPoint = "";
  private String mapZoomLevel = "";
  private String searchRange = "25";
  private String reviewCount = "";
  private String totalCount = "";
  private String profileFlag = "";
  private String overallRating = "";
  private String collegeLogo = "";
  private String overviewShortDesc = "";
  private String collegeAdvertisorFlag = "";
  private String collegeInBasket = "";
  private String emailPurFlag = "";
  private String prospectusPurFlag = "";
  private String websitePurFlag = "";
  private String collegeSite = "";
  private String videoDesc = "";
  private String videoUrl = "";
  private String videoReviewId = "";
  private String videoThumbnail = "";
  private String prospectusExternalUrl = "";
  private String subOrderItemId = "";
  private String subOrderWebsite = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";
  private String cpeQualificationNetworkId = "";
  private String profileType = "";
  private String spOrderItemId = "";
  private String myhcProfileId = "";

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setLatitudeValue(String latitudeValue) {
    this.latitudeValue = latitudeValue;
  }

  public String getLatitudeValue() {
    return latitudeValue;
  }

  public void setLongtitudeValue(String longtitudeValue) {
    this.longtitudeValue = longtitudeValue;
  }

  public String getLongtitudeValue() {
    return longtitudeValue;
  }

  public void setSearchLocation(String searchLocation) {
    this.searchLocation = searchLocation;
  }

  public String getSearchLocation() {
    return searchLocation;
  }

  public void setLatitudeList(List latitudeList) {
    this.latitudeList = latitudeList;
  }

  public List getLatitudeList() {
    return latitudeList;
  }

  public void setLongititudeList(List longititudeList) {
    this.longititudeList = longititudeList;
  }

  public List getLongititudeList() {
    return longititudeList;
  }

  public void setPlaceDetailList(List placeDetailList) {
    this.placeDetailList = placeDetailList;
  }

  public List getPlaceDetailList() {
    return placeDetailList;
  }

  public void setPlaceDetail(String placeDetail) {
    this.placeDetail = placeDetail;
  }

  public String getPlaceDetail() {
    return placeDetail;
  }

  public void setLatLanValue(String latLanValue) {
    this.latLanValue = latLanValue;
  }

  public String getLatLanValue() {
    return latLanValue;
  }

  public void setGoogleSearchText(String googleSearchText) {
    this.googleSearchText = googleSearchText;
  }

  public String getGoogleSearchText() {
    return googleSearchText;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }

  public String getOptionId() {
    return optionId;
  }

  public void setOptionDescription(String optionDescription) {
    this.optionDescription = optionDescription;
  }

  public String getOptionDescription() {
    return optionDescription;
  }

  public void setCountyName(String countyName) {
    this.countyName = countyName;
  }

  public String getCountyName() {
    return countyName;
  }

  public void setTownName(String townName) {
    this.townName = townName;
  }

  public String getTownName() {
    return townName;
  }

  public void setGoogleDisplayText(String googleDisplayText) {
    this.googleDisplayText = googleDisplayText;
  }

  public String getGoogleDisplayText() {
    return googleDisplayText;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getLocation() {
    return location;
  }

  public void setMapCenterPoint(String mapCenterPoint) {
    this.mapCenterPoint = mapCenterPoint;
  }

  public String getMapCenterPoint() {
    return mapCenterPoint;
  }

  public void setMapZoomLevel(String mapZoomLevel) {
    this.mapZoomLevel = mapZoomLevel;
  }

  public String getMapZoomLevel() {
    return mapZoomLevel;
  }

  public void setGoolgeSearchPostCode(String goolgeSearchPostCode) {
    this.goolgeSearchPostCode = goolgeSearchPostCode;
  }

  public String getGoolgeSearchPostCode() {
    return goolgeSearchPostCode;
  }

  public void setSearchRange(String searchRange) {
    this.searchRange = searchRange;
  }

  public String getSearchRange() {
    return searchRange;
  }

/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    errors.clear();
    ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    String googleDefaultText = bundle.getString("wui.message.google.search.default.text");
    if (request.getParameter("pid") == null) {
      if (googleSearchText != null && (googleSearchText.trim().equalsIgnoreCase(googleDefaultText) || googleSearchText.trim().length() == 0)) {
        errors.add("wui.error.blank.google.search.text", new ActionMessage("wui.error.blank.google.search.text"));
      }
    }
    return errors;
  }*/

  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }

  public String getReviewCount() {
    return reviewCount;
  }

  public void setTotalCount(String totalCount) {
    this.totalCount = totalCount;
  }

  public String getTotalCount() {
    return totalCount;
  }

  public void setProfileFlag(String profileFlag) {
    this.profileFlag = profileFlag;
  }

  public String getProfileFlag() {
    return profileFlag;
  }

  public void setOverallRating(String overallRating) {
    this.overallRating = overallRating;
  }

  public String getOverallRating() {
    return overallRating;
  }

  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }

  public String getCollegeLogo() {
    return collegeLogo;
  }

  public void setOverviewShortDesc(String overviewShortDesc) {
    this.overviewShortDesc = overviewShortDesc;
  }

  public String getOverviewShortDesc() {
    return overviewShortDesc;
  }

  public void setCollegeAdvertisorFlag(String collegeAdvertisorFlag) {
    this.collegeAdvertisorFlag = collegeAdvertisorFlag;
  }

  public String getCollegeAdvertisorFlag() {
    return collegeAdvertisorFlag;
  }

  public void setCollegeInBasket(String collegeInBasket) {
    this.collegeInBasket = collegeInBasket;
  }

  public String getCollegeInBasket() {
    return collegeInBasket;
  }

  public void setEmailPurFlag(String emailPurFlag) {
    this.emailPurFlag = emailPurFlag;
  }

  public String getEmailPurFlag() {
    return emailPurFlag;
  }

  public void setProspectusPurFlag(String prospectusPurFlag) {
    this.prospectusPurFlag = prospectusPurFlag;
  }

  public String getProspectusPurFlag() {
    return prospectusPurFlag;
  }

  public void setWebsitePurFlag(String websitePurFlag) {
    this.websitePurFlag = websitePurFlag;
  }

  public String getWebsitePurFlag() {
    return websitePurFlag;
  }

  public void setCollegeSite(String collegeSite) {
    this.collegeSite = collegeSite;
  }

  public String getCollegeSite() {
    return collegeSite;
  }

  public void setVideoDesc(String videoDesc) {
    this.videoDesc = videoDesc;
  }

  public String getVideoDesc() {
    return videoDesc;
  }

  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }

  public String getVideoUrl() {
    return videoUrl;
  }

  public void setVideoReviewId(String videoReviewId) {
    this.videoReviewId = videoReviewId;
  }

  public String getVideoReviewId() {
    return videoReviewId;
  }

  public void setVideoThumbnail(String videoThumbnail) {
    this.videoThumbnail = videoThumbnail;
  }

  public String getVideoThumbnail() {
    return videoThumbnail;
  }

  public void setProspectusExternalUrl(String prospectusExternalUrl) {
    this.prospectusExternalUrl = prospectusExternalUrl;
  }

  public String getProspectusExternalUrl() {
    return prospectusExternalUrl;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setSpOrderItemId(String spOrderItemId) {
    this.spOrderItemId = spOrderItemId;
  }

  public String getSpOrderItemId() {
    return spOrderItemId;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
/////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////    
