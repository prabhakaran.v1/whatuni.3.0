package WUI.search.form;

import org.apache.struts.action.ActionForm;

/**
 * to hold "Category/Keyword sponsership" and "Featured course sponsership" related datas
 * 
 * @version initial draft. 
 * @since 2010-Jun-30
 * @author Mohamed Syed
 */
public class KeywordCourseSponsershipBean {

  String keyword = "";
  String categoryCode = "";
  String collegeId = "";
  String collegeName = "";
  String collegeNameDisplay = "";
  String collegeExternalUrl = "";
  String courseTitle = "";
  String courseDescription = "";
  String reviewerName = "";
  String reviewDescription = "";
  //For Clearing
  String catSponsorId = "";
  String text = "";
  String mediaPath = "";

  public void init() {
    this.keyword = "";
    this.categoryCode = "";
    this.collegeId = "";
    this.collegeName = "";
    this.collegeExternalUrl = "";
    this.reviewerName = "";
    this.reviewDescription = "";
  }

  public void flush() {
    this.keyword = null;
    this.categoryCode = null;
    this.collegeId = null;
    this.collegeName = null;
    this.collegeExternalUrl = null;
    this.reviewerName = null;
    this.reviewDescription = null;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeExternalUrl(String collegeExternalUrl) {
    this.collegeExternalUrl = collegeExternalUrl;
  }

  public String getCollegeExternalUrl() {
    return collegeExternalUrl;
  }

  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public String getCourseTitle() {
    return courseTitle;
  }

  public void setCourseDescription(String courseDescription) {
    this.courseDescription = courseDescription;
  }

  public String getCourseDescription() {
    return courseDescription;
  }

  public void setReviewerName(String reviewerName) {
    this.reviewerName = reviewerName;
  }

  public String getReviewerName() {
    return reviewerName;
  }

  public void setReviewDescription(String reviewDescription) {
    this.reviewDescription = reviewDescription;
  }

  public String getReviewDescription() {
    return reviewDescription;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setCatSponsorId(String catSponsorId) {
    this.catSponsorId = catSponsorId;
  }

  public String getCatSponsorId() {
    return catSponsorId;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getText() {
    return text;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

}
