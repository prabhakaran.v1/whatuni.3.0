package WUI.search.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class SubjectBean {
  private String categoryCode = "";
  private String categoryDesc = "";
  private String ldcsLevel = "";
  private List child1List = null;
  private List child2List = null;
  private String seoCategoryDesc = "";
  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }
  public String getCategoryCode() {
    return categoryCode;
  }
  public void setCategoryDesc(String categoryDesc) {
    this.categoryDesc = categoryDesc;
  }
  public String getCategoryDesc() {
    return categoryDesc;
  }
  public void setLdcsLevel(String ldcsLevel) {
    this.ldcsLevel = ldcsLevel;
  }
  public String getLdcsLevel() {
    return ldcsLevel;
  }
  public void setChild1List(List child1List) {
    this.child1List = child1List;
  }
  public List getChild1List() {
    return child1List;
  }
  public void setChild2List(List child2List) {
    this.child2List = child2List;
  }
  public List getChild2List() {
    return child2List;
  }
  public void setSeoCategoryDesc(String seoCategoryDesc) {
    this.seoCategoryDesc = seoCategoryDesc;
  }
  public String getSeoCategoryDesc() {
    return seoCategoryDesc;
  }
}
