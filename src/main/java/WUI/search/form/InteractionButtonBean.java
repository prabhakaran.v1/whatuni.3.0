package WUI.search.form;

public class InteractionButtonBean {
    public InteractionButtonBean() {
    }
    private String orderItemId = "";
    private String subOrderItemId = "";
    private String subOrderWebsite = "";
    private String subOrderEmail = "";
    private String subOrderEmailWebform = "";
    private String subOrderProspectus = "";
    private String subOrderProspectusWebform = "";
   

    public void setSubOrderItemId(String subOrderItemId) {
        this.subOrderItemId = subOrderItemId;
    }

    public String getSubOrderItemId() {
        return subOrderItemId;
    }

    public void setSubOrderWebsite(String subOrderWebsite) {
        this.subOrderWebsite = subOrderWebsite;
    }

    public String getSubOrderWebsite() {
        return subOrderWebsite;
    }

    public void setSubOrderEmail(String subOrderEmail) {
        this.subOrderEmail = subOrderEmail;
    }

    public String getSubOrderEmail() {
        return subOrderEmail;
    }

    public void setSubOrderEmailWebform(String subOrderEmailWebform) {
        this.subOrderEmailWebform = subOrderEmailWebform;
    }

    public String getSubOrderEmailWebform() {
        return subOrderEmailWebform;
    }

    public void setSubOrderProspectus(String subOrderProspectus) {
        this.subOrderProspectus = subOrderProspectus;
    }

    public String getSubOrderProspectus() {
        return subOrderProspectus;
    }

    public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
        this.subOrderProspectusWebform = subOrderProspectusWebform;
    }

    public String getSubOrderProspectusWebform() {
        return subOrderProspectusWebform;
    }


    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getOrderItemId() {
        return orderItemId;
    }
}
