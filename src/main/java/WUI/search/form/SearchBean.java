package WUI.search.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

import WUI.utilities.GlobalConstants;

public class SearchBean {

  private String collegeId = "";
  private String courseCollegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String courseName = "";
  private String optionId = "";
  private String optionText = "";
  private String optionSeqId = "";
  private String studyModeId = "";
  private String studyModeDesc = "";
  private String studyModeValue = "";
  private List regionList = null;
  private List subjectList = null;
  private String studyLevelId = "";
  private List studyLevelList = null;
  private List studyModeList = null;
  private String courseSearchText = "";
  private String postCode = "";
  private String regionId = "";
  private List filterList = null;
  private List orderList = null;
  private String filterId = "";
  private String filterName = "";
  private String orderId = "R";
  private String orderName = "";
  private String countyId = "";
  private String subjectId = "";
  private String subjectNameWithoutHypen = "";
  private String subjectName = "";
  private String footerLink = "";
  private String searchRange = "";
  /////////////////FOR Uni search Autocomplete ////
  private String college_Name = "";
  private String searchCollegeId = "";
  private String coursetitle = "";
  private String courseid = "";
  private String subjectCode = "";
  private String pageName = "";
  private String parentCategoryId = "";
  private String categoryCodeLength = "";
  private String seoStudyLevel = "";
  private String hyperLinkUrl = "";
  private String collegeOrderIcon = GlobalConstants.BLANK_IMAGE;
  private String resultOrderIcon = GlobalConstants.BLANK_IMAGE;
  private String reviewOrderIcon = GlobalConstants.BLANK_IMAGE;
  private String studentSatisfyOrderIcon = GlobalConstants.BLANK_IMAGE;
  private String studentJobOrderIcon = GlobalConstants.BLANK_IMAGE;
  private String timesLeagueOrderIcon = GlobalConstants.BLANK_IMAGE;
  //
  private String timesRankingOrderIcon = GlobalConstants.BLANK_IMAGE;
  private String timesSubjectRankingOrderIcon = GlobalConstants.BLANK_IMAGE;
  //
  private String ucasCode = "";
  private String bespokeHeading = "";
  private String bespokeContent = "";
  private String timesPosition = "";
  private String filterUniType = "";
  private String deemedUniText = "";
  private String deemedUniValue = "";
  private List deemedUniList = null;
  private String subjectCount = "";
  private String refineFlag = "";
  private String omnitureFlag = "";
  //
  private String advertName = "";
  private String myhcProfileId = "";
  private String subOrderItemId = "";
  private String collegeLogo = "";
  private String prospectusQualType = "";
  private String wasThisCollegeShortlisted = null;
  private String totalRecords = null;
  private String uniHomeUrl = null;
  private String overallRating = null;
  private String dpFlag = null;
  private String dpteasertext = null;
  //
  private String userId = null;
  private String attributeId = null;
  private String attributeValue = null;
  private String displaySeq = null;
  private String optionDesc = null;
  private String basketId = null;
  private String actualRating = null;
  private String nextOpenDay = null;
  private String collegeLocation = null;
  private String snippetText = null;//3_JUN_2014
  
  //
 
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }

  public String getOptionId() {
    return optionId;
  }

  public void setOptionText(String optionText) {
    this.optionText = optionText;
  }

  public String getOptionText() {
    return optionText;
  }

  public void setStudyModeId(String studyModeId) {
    this.studyModeId = studyModeId;
  }

  public String getStudyModeId() {
    return studyModeId;
  }

  public void setStudyModeDesc(String studyModeDesc) {
    this.studyModeDesc = studyModeDesc;
  }

  public String getStudyModeDesc() {
    return studyModeDesc;
  }

  public void setStudyModeValue(String studyModeValue) {
    this.studyModeValue = studyModeValue;
  }

  public String getStudyModeValue() {
    return studyModeValue;
  }

  public void setStudyLevelId(String studyLevelId) {
    this.studyLevelId = studyLevelId;
  }

  public String getStudyLevelId() {
    return studyLevelId;
  }

  public void setStudyLevelList(List studyLevelList) {
    this.studyLevelList = studyLevelList;
  }

  public List getStudyLevelList() {
    return studyLevelList;
  }

  public void setStudyModeList(List studyModeList) {
    this.studyModeList = studyModeList;
  }

  public List getStudyModeList() {
    return studyModeList;
  }

  public void setCollege_Name(String college_Name) {
    this.college_Name = college_Name;
  }

  public String getCollege_Name() {
    return college_Name;
  }

  public void setSearchCollegeId(String searchCollegeId) {
    this.searchCollegeId = searchCollegeId;
  }

  public String getSearchCollegeId() {
    return searchCollegeId;
  }

  public void setCoursetitle(String coursetitle) {
    this.coursetitle = coursetitle;
  }

  public String getCoursetitle() {
    return coursetitle;
  }

  public void setCourseid(String courseid) {
    this.courseid = courseid;
  }

  public String getCourseid() {
    return courseid;
  }

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }

  public String getCourseName() {
    return courseName;
  }

  public void setRegionList(List regionList) {
    this.regionList = regionList;
  }

  public List getRegionList() {
    return regionList;
  }

  public void setSubjectList(List subjectList) {
    this.subjectList = subjectList;
  }

  public List getSubjectList() {
    return subjectList;
  }

  public void setCourseSearchText(String courseSearchText) {
    this.courseSearchText = courseSearchText;
  }

  public String getCourseSearchText() {
    return courseSearchText;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setRegionId(String regionId) {
    this.regionId = regionId;
  }

  public String getRegionId() {
    return regionId;
  }

  public void setFilterList(List filterList) {
    this.filterList = filterList;
  }

  public List getFilterList() {
    return filterList;
  }

  public void setOrderList(List orderList) {
    this.orderList = orderList;
  }

  public List getOrderList() {
    return orderList;
  }

  public void setFilterId(String filterId) {
    this.filterId = filterId;
  }

  public String getFilterId() {
    return filterId;
  }

  public void setOrderId(String orderId) {
    this.orderId = orderId;
  }

  public String getOrderId() {
    return orderId;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }

  public String getSubjectName() {
    return subjectName;
  }

  public void setFilterName(String filterName) {
    this.filterName = filterName;
  }

  public String getFilterName() {
    return filterName;
  }

  public void setOrderName(String orderName) {
    this.orderName = orderName;
  }

  public String getOrderName() {
    return orderName;
  }

  public void setCourseCollegeId(String courseCollegeId) {
    this.courseCollegeId = courseCollegeId;
  }

  public String getCourseCollegeId() {
    return courseCollegeId;
  }

  public void setOptionSeqId(String optionSeqId) {
    this.optionSeqId = optionSeqId;
  }

  public String getOptionSeqId() {
    return optionSeqId;
  }

  public void setCountyId(String countyId) {
    this.countyId = countyId;
  }

  public String getCountyId() {
    return countyId;
  }

  public void setFooterLink(String footerLink) {
    this.footerLink = footerLink;
  }

  public String getFooterLink() {
    return footerLink;
  }

  public void setSearchRange(String searchRange) {
    this.searchRange = searchRange;
  }

  public String getSearchRange() {
    return searchRange;
  }

  public void setSubjectCode(String subjectCode) {
    this.subjectCode = subjectCode;
  }

  public String getSubjectCode() {
    return subjectCode;
  }

  public void setPageName(String pageName) {
    this.pageName = pageName;
  }

  public String getPageName() {
    return pageName;
  }

  public void setParentCategoryId(String parentCategoryId) {
    this.parentCategoryId = parentCategoryId;
  }

  public String getParentCategoryId() {
    return parentCategoryId;
  }

  public void setCategoryCodeLength(String categoryCodeLength) {
    this.categoryCodeLength = categoryCodeLength;
  }

  public String getCategoryCodeLength() {
    return categoryCodeLength;
  }

  public void setSeoStudyLevel(String seoStudyLevel) {
    this.seoStudyLevel = seoStudyLevel;
  }

  public String getSeoStudyLevel() {
    return seoStudyLevel;
  }

  public void setHyperLinkUrl(String hyperLinkUrl) {
    this.hyperLinkUrl = hyperLinkUrl;
  }

  public String getHyperLinkUrl() {
    return hyperLinkUrl;
  }

  public void setCollegeOrderIcon(String collegeOrderIcon) {
    this.collegeOrderIcon = collegeOrderIcon;
  }

  public String getCollegeOrderIcon() {
    return collegeOrderIcon;
  }

  public void setResultOrderIcon(String resultOrderIcon) {
    this.resultOrderIcon = resultOrderIcon;
  }

  public String getResultOrderIcon() {
    return resultOrderIcon;
  }

  public void setReviewOrderIcon(String reviewOrderIcon) {
    this.reviewOrderIcon = reviewOrderIcon;
  }

  public String getReviewOrderIcon() {
    return reviewOrderIcon;
  }

  public void setStudentJobOrderIcon(String studentJobOrderIcon) {
    this.studentJobOrderIcon = studentJobOrderIcon;
  }

  public String getStudentJobOrderIcon() {
    return studentJobOrderIcon;
  }

  public void setStudentSatisfyOrderIcon(String studentSatisfyOrderIcon) {
    this.studentSatisfyOrderIcon = studentSatisfyOrderIcon;
  }

  public String getStudentSatisfyOrderIcon() {
    return studentSatisfyOrderIcon;
  }

  public void setTimesLeagueOrderIcon(String timesLeagueOrderIcon) {
    this.timesLeagueOrderIcon = timesLeagueOrderIcon;
  }

  public String getTimesLeagueOrderIcon() {
    return timesLeagueOrderIcon;
  }

  public void setUcasCode(String ucasCode) {
    this.ucasCode = ucasCode;
  }

  public String getUcasCode() {
    return ucasCode;
  }

  public void setBespokeHeading(String bespokeHeading) {
    this.bespokeHeading = bespokeHeading;
  }

  public String getBespokeHeading() {
    return bespokeHeading;
  }

  public void setBespokeContent(String bespokeContent) {
    this.bespokeContent = bespokeContent;
  }

  public String getBespokeContent() {
    return bespokeContent;
  }

  public void setTimesPosition(String timesPosition) {
    this.timesPosition = timesPosition;
  }

  public String getTimesPosition() {
    return timesPosition;
  }

  public void setFilterUniType(String filterUniType) {
    this.filterUniType = filterUniType;
  }

  public String getFilterUniType() {
    return filterUniType;
  }

  public void setDeemedUniText(String deemedUniText) {
    this.deemedUniText = deemedUniText;
  }

  public String getDeemedUniText() {
    return deemedUniText;
  }

  public void setDeemedUniValue(String deemedUniValue) {
    this.deemedUniValue = deemedUniValue;
  }

  public String getDeemedUniValue() {
    return deemedUniValue;
  }

  public void setDeemedUniList(List deemedUniList) {
    this.deemedUniList = deemedUniList;
  }

  public List getDeemedUniList() {
    return deemedUniList;
  }

  public void setSubjectCount(String subjectCount) {
    this.subjectCount = subjectCount;
  }

  public String getSubjectCount() {
    return subjectCount;
  }

  public void setRefineFlag(String refineFlag) {
    this.refineFlag = refineFlag;
  }

  public String getRefineFlag() {
    return refineFlag;
  }

  public void setOmnitureFlag(String omnitureFlag) {
    this.omnitureFlag = omnitureFlag;
  }

  public String getOmnitureFlag() {
    return omnitureFlag;
  }

  public void setSubjectNameWithoutHypen(String subjectNameWithoutHypen) {
    this.subjectNameWithoutHypen = subjectNameWithoutHypen;
  }

  public String getSubjectNameWithoutHypen() {
    return subjectNameWithoutHypen;
  }

  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }

  public String getAdvertName() {
    return advertName;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setTimesRankingOrderIcon(String timesRankingOrderIcon) {
    this.timesRankingOrderIcon = timesRankingOrderIcon;
  }

  public String getTimesRankingOrderIcon() {
    return timesRankingOrderIcon;
  }

  public void setTimesSubjectRankingOrderIcon(String timesSubjectRankingOrderIcon) {
    this.timesSubjectRankingOrderIcon = timesSubjectRankingOrderIcon;
  }

  public String getTimesSubjectRankingOrderIcon() {
    return timesSubjectRankingOrderIcon;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectId() {
        return subjectId;
    }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
      return collegeLogo;
    }
  public void setProspectusQualType(String  prospectusQualType) {
    this.prospectusQualType =  prospectusQualType;
  }
    public String getProspectusQualType() {
     return prospectusQualType;
    }
    
    public void setWasThisCollegeShortlisted(String wasThisCollegeShortlisted) {
        this.wasThisCollegeShortlisted = wasThisCollegeShortlisted;
    }

    public String getWasThisCollegeShortlisted() {
        return wasThisCollegeShortlisted;
    }

    public void setTotalRecords(String totalRecords) {
        this.totalRecords = totalRecords;
    }

    public String getTotalRecords() {
        return totalRecords;
    }

    public void setUniHomeUrl(String uniHomeUrl) {
        this.uniHomeUrl = uniHomeUrl;
    }

    public String getUniHomeUrl() {
        return uniHomeUrl;
    }

    public void setOverallRating(String overallRating) {
        this.overallRating = overallRating;
    }

    public String getOverallRating() {
        return overallRating;
    }

    public void setDpFlag(String dpFlag) {
        this.dpFlag = dpFlag;
    }

    public String getDpFlag() {
        return dpFlag;
    }
    
  public void setDpteasertext(String dpteasertext) {
      this.dpteasertext = dpteasertext;
  }

  public String getDpteasertext() {
      return dpteasertext;
  }
  
  public void setUserId(String userId) {
    this.userId = userId;
  }
  
  public String getUserId() {
    return userId;
  }
  
  public void setAttributeId(String attributeId) {
    this.attributeId = attributeId;
  }
  
  public String getAttributeId() {
    return attributeId;
  }
  
  public void setAttributeValue(String attributeValue) {
    this.attributeValue = attributeValue;
  }
  
  public String getAttributeValue() {
    return attributeValue;
  }
  
  public void setDisplaySeq(String displaySeq) {
    this.displaySeq = displaySeq;
  }
  
  public String getDisplaySeq() {
    return displaySeq;
  }
  
  public void setOptionDesc(String optionDesc) {
    this.optionDesc = optionDesc;
  }
  
  public String getOptionDesc() {
    return optionDesc;
  }
  public void setBasketId(String basketId) {
    this.basketId = basketId;
  }
  public String getBasketId() {
    return basketId;
  }
  public void setActualRating(String actualRating) {
    this.actualRating = actualRating;
  }
  public String getActualRating() {
    return actualRating;
  }

  public void setNextOpenDay(String nextOpenDay)
  {
    this.nextOpenDay = nextOpenDay;
  }

  public String getNextOpenDay()
  {
    return nextOpenDay;
  }

  public void setCollegeLocation(String collegeLocation)
  {
    this.collegeLocation = collegeLocation;
  }

  public String getCollegeLocation()
  {
    return collegeLocation;
  }
  public void setSnippetText(String snippetText) {
    this.snippetText = snippetText;
  }
  public String getSnippetText() {
    return snippetText;
  }
}
/////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////    
