package WUI.search.form;

import org.apache.struts.action.ActionForm;

import WUI.cpe.form.CollegeCpeInteractionDetailsWithMediaBean;

public class SimilarCoursesBean  {
  //
  private String courseId = "";
  private String courseTitle = "";
  private String courseDetailPageUrl = "";
  private String courseQualification = "";
  //
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String collegeLogo = "";
  private String collegeHomePageUrl = "";
  //
  CollegeCpeInteractionDetailsWithMediaBean collegeCpeInteractionDetailsWithMedia = null;

  public void clear() {
    courseId = null;
    courseTitle = null;
    collegeId = null;
    collegeName = null;
    collegeLogo = null;
    collegeCpeInteractionDetailsWithMedia.clear();
    collegeCpeInteractionDetailsWithMedia = null;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public String getCourseTitle() {
    return courseTitle;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }

  public String getCollegeLogo() {
    return collegeLogo;
  }

  public void setCollegeCpeInteractionDetailsWithMedia(CollegeCpeInteractionDetailsWithMediaBean collegeCpeInteractionDetailsWithMedia) {
    this.collegeCpeInteractionDetailsWithMedia = collegeCpeInteractionDetailsWithMedia;
  }

  public CollegeCpeInteractionDetailsWithMediaBean getCollegeCpeInteractionDetailsWithMedia() {
    return collegeCpeInteractionDetailsWithMedia;
  }

  public void setCourseDetailPageUrl(String courseDetailPageUrl) {
    this.courseDetailPageUrl = courseDetailPageUrl;
  }

  public String getCourseDetailPageUrl() {
    return courseDetailPageUrl;
  }

  public void setCollegeHomePageUrl(String collegeHomePageUrl) {
    this.collegeHomePageUrl = collegeHomePageUrl;
  }

  public String getCollegeHomePageUrl() {
    return collegeHomePageUrl;
  }

  public void setCourseQualification(String courseQualification) {
    this.courseQualification = courseQualification;
  }

  public String getCourseQualification() {
    return courseQualification;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
