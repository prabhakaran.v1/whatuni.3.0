package WUI.search.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class RefineByPodBean {

  public RefineByPodBean() {
  }
  //common   
  private String refineOrder = "";
  private String refineCode = "";
  private String refineDesc = "";
  private String categoryId = "";
  private String categoryCode = "";
  private String displaySequence = "";
  //provider type related
  private String providerTypeDesc = "";
  private String providerTypeCode = "";
  //qualification/study-level related
  private String qualificationDesc = "";
  private String qualificationCode = "";
  //study mode related
  private String studyModeDesc = "";
  private String studyModeCode = "";
  //location related
  private String locationDesc = "";
  private String locationCode = "";
  private String parentCountryName = "";
  private String numberOfChildLocations = "";
  private List listOfChildrenLocations = null;
  private String browseMoneyageLocationRefineUrl = "";
  //

  public void flush() {
    this.refineOrder = null;
    this.refineCode = null;
    this.refineDesc = null;
    this.categoryId = null;
    this.categoryCode = null;
    this.displaySequence = null;
    this.providerTypeDesc = null;
    this.providerTypeCode = null;
    this.qualificationDesc = null;
    this.qualificationCode = null;
    this.studyModeDesc = null;
    this.studyModeCode = null;
    this.locationDesc = null;
    this.locationCode = null;
  }

  public void init() {
    this.refineOrder = "";
    this.refineCode = "";
    this.refineDesc = "";
    this.categoryId = "";
    this.categoryCode = "";
    this.displaySequence = "";
    this.providerTypeDesc = "";
    this.providerTypeCode = "";
    this.qualificationDesc = "";
    this.qualificationCode = "";
    this.studyModeDesc = "";
    this.studyModeCode = "";
    this.locationDesc = "";
    this.locationCode = "";
  }

  public void setRefineOrder(String refineOrder) {
    this.refineOrder = refineOrder;
  }

  public String getRefineOrder() {
    return refineOrder;
  }

  public void setRefineCode(String refineCode) {
    this.refineCode = refineCode;
  }

  public String getRefineCode() {
    return refineCode;
  }

  public void setRefineDesc(String refineDesc) {
    this.refineDesc = refineDesc;
  }

  public String getRefineDesc() {
    return refineDesc;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setProviderTypeDesc(String providerTypeDesc) {
    this.providerTypeDesc = providerTypeDesc;
  }

  public String getProviderTypeDesc() {
    return providerTypeDesc;
  }

  public void setProviderTypeCode(String providerTypeCode) {
    this.providerTypeCode = providerTypeCode;
  }

  public String getProviderTypeCode() {
    return providerTypeCode;
  }

  public void setQualificationDesc(String qualificationDesc) {
    this.qualificationDesc = qualificationDesc;
  }

  public String getQualificationDesc() {
    return qualificationDesc;
  }

  public void setQualificationCode(String qualificationCode) {
    this.qualificationCode = qualificationCode;
  }

  public String getQualificationCode() {
    return qualificationCode;
  }

  public void setStudyModeDesc(String studyModeDesc) {
    this.studyModeDesc = studyModeDesc;
  }

  public String getStudyModeDesc() {
    return studyModeDesc;
  }

  public void setStudyModeCode(String studyModeCode) {
    this.studyModeCode = studyModeCode;
  }

  public String getStudyModeCode() {
    return studyModeCode;
  }

  public void setLocationDesc(String locationDesc) {
    this.locationDesc = locationDesc;
  }

  public String getLocationDesc() {
    return locationDesc;
  }

  public void setLocationCode(String locationCode) {
    this.locationCode = locationCode;
  }

  public String getLocationCode() {
    return locationCode;
  }

  public void setParentCountryName(String parentCountryName) {
    this.parentCountryName = parentCountryName;
  }

  public String getParentCountryName() {
    return parentCountryName;
  }

  public void setNumberOfChildLocations(String numberOfChildLocations) {
    this.numberOfChildLocations = numberOfChildLocations;
  }

  public String getNumberOfChildLocations() {
    return numberOfChildLocations;
  }

  public void setDisplaySequence(String displaySequence) {
    this.displaySequence = displaySequence;
  }

  public String getDisplaySequence() {
    return displaySequence;
  }

  public void setListOfChildrenLocations(List listOfChildrenLocations) {
    this.listOfChildrenLocations = listOfChildrenLocations;
  }

  public List getListOfChildrenLocations() {
    return listOfChildrenLocations;
  }

  public void setBrowseMoneyageLocationRefineUrl(String browseMoneyageLocationRefineUrl) {
    this.browseMoneyageLocationRefineUrl = browseMoneyageLocationRefineUrl;
  }

  public String getBrowseMoneyageLocationRefineUrl() {
    return browseMoneyageLocationRefineUrl;
  }

}
