package WUI.chatbot;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

import com.wuni.valueobjects.whatunigo.GradeFilterValidationVO;

public class ChatbotBean {

  private String userId;
  private String appVersion;
  private String accessToken;
  private String affiliateId;
  private String keywordText;
  private String questionId;
  private String questionName;
  private String answer;
  private String actionType;
  private String actionDate;
  private String actionStatus;
  private String actionUrl;
  private String actionKeyId;
  private String actionDetails;
  private String trackSessionId;
  private String categoryCode;
  private String qualificationCode;
  private String previousQual;
  private String previousQualGrades;
  private String jobOrIndustry;
  private String subjectText;
  private String qualTypeId;
  private String jacsCode;
  private String selectedFilterUrl;
  //  
  private String subject1_id;
  private String subject2_id;
  private String subject3_id;
  private String subject4_id;
  private String subject5_id;
  private String subject6_id;
  private String subj1_tariff_points;
  private String subj2_tariff_points;
  private String subj3_tariff_points;
  private String subj4_tariff_points;
  private String subj5_tariff_points;
  private String subj6_tariff_points;
  //   
  private String nextQuestionId;
  private String timeDeplay;
 
  private String clearingUser;
  
  private ArrayList<GradeFilterValidationVO> qualSubList;
  
  public ArrayList<GradeFilterValidationVO> getQualSubList() {
	return qualSubList;
  }

  public void setQualSubList(ArrayList<GradeFilterValidationVO> qualSubList) {
	this.qualSubList = qualSubList;
  }

  private Object[][] qualDetailsArr = null;
  
  public Object[][] getQualDetailsArr() {
	return qualDetailsArr;
  }

  public void setQualDetailsArr(Object[][] qualDetailsArr) {
	this.qualDetailsArr = qualDetailsArr;
  }

  public String getJacsCode() {
    return jacsCode;
  }

  public void setJacsCode(String jacsCode) {
    this.jacsCode = jacsCode;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getAppVersion() {
    return appVersion;
  }

  public void setAppVersion(String appVersion) {
    this.appVersion = appVersion;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getAffiliateId() {
    return affiliateId;
  }

  public void setAffiliateId(String affiliateId) {
    this.affiliateId = affiliateId;
  }

  public String getKeywordText() {
    return keywordText;
  }

  public void setKeywordText(String keywordText) {
    this.keywordText = keywordText;
  }

  public String getQuestionId() {
    return questionId;
  }

  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public String getActionType() {
    return actionType;
  }

  public void setActionType(String actionType) {
    this.actionType = actionType;
  }

  public String getActionDate() {
    return actionDate;
  }

  public void setActionDate(String actionDate) {
    this.actionDate = actionDate;
  }

  public String getActionStatus() {
    return actionStatus;
  }

  public void setActionStatus(String actionStatus) {
    this.actionStatus = actionStatus;
  }

  public String getActionUrl() {
    return actionUrl;
  }

  public void setActionUrl(String actionUrl) {
    this.actionUrl = actionUrl;
  }

  public String getActionKeyId() {
    return actionKeyId;
  }

  public void setActionKeyId(String actionKeyId) {
    this.actionKeyId = actionKeyId;
  }

  public String getActionDetails() {
    return actionDetails;
  }

  public void setActionDetails(String actionDetails) {
    this.actionDetails = actionDetails;
  }

  public String getTrackSessionId() {
    return trackSessionId;
  }

  public void setTrackSessionId(String trackSessionId) {
    this.trackSessionId = trackSessionId;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getQualificationCode() {
    return qualificationCode;
  }

  public void setQualificationCode(String qualificationCode) {
    this.qualificationCode = qualificationCode;
  }

  public String getPreviousQual() {
    return previousQual;
  }

  public void setPreviousQual(String previousQual) {
    this.previousQual = previousQual;
  }

  public String getPreviousQualGrades() {
    return previousQualGrades;
  }

  public void setPreviousQualGrades(String previousQualGrades) {
    this.previousQualGrades = previousQualGrades;
  }

  public String getJobOrIndustry() {
    return jobOrIndustry;
  }

  public void setJobOrIndustry(String jobOrIndustry) {
    this.jobOrIndustry = jobOrIndustry;
  }

  public String getSubjectText() {
    return subjectText;
  }

  public void setSubjectText(String subjectText) {
    this.subjectText = subjectText;
  }

  public String getQualTypeId() {
    return qualTypeId;
  }

  public void setQualTypeId(String qualTypeId) {
    this.qualTypeId = qualTypeId;
  }

  public void setNextQuestionId(String nextQuestionId) {
    this.nextQuestionId = nextQuestionId;
  }

  public String getNextQuestionId() {
    return nextQuestionId;
  }

  public void setTimeDeplay(String timeDeplay) {
    this.timeDeplay = timeDeplay;
  }

  public String getTimeDeplay() {
    return timeDeplay;
  }

  public void setQuestionName(String questionName) {
    this.questionName = questionName;
  }

  public String getQuestionName() {
    return questionName;
  }

  public void setSubject1_id(String subject1_id) {
    this.subject1_id = subject1_id;
  }

  public String getSubject1_id() {
    return subject1_id;
  }

  public void setSubject2_id(String subject2_id) {
    this.subject2_id = subject2_id;
  }

  public String getSubject2_id() {
    return subject2_id;
  }

  public void setSubject3_id(String subject3_id) {
    this.subject3_id = subject3_id;
  }

  public String getSubject3_id() {
    return subject3_id;
  }

  public void setSubject4_id(String subject4_id) {
    this.subject4_id = subject4_id;
  }

  public String getSubject4_id() {
    return subject4_id;
  }

  public void setSubject5_id(String subject5_id) {
    this.subject5_id = subject5_id;
  }

  public String getSubject5_id() {
    return subject5_id;
  }

  public void setSubject6_id(String subject6_id) {
    this.subject6_id = subject6_id;
  }

  public String getSubject6_id() {
    return subject6_id;
  }

  public void setSubj1_tariff_points(String subj1_tariff_points) {
    this.subj1_tariff_points = subj1_tariff_points;
  }

  public String getSubj1_tariff_points() {
    return subj1_tariff_points;
  }

  public void setSubj2_tariff_points(String subj2_tariff_points) {
    this.subj2_tariff_points = subj2_tariff_points;
  }

  public String getSubj2_tariff_points() {
    return subj2_tariff_points;
  }

  public void setSubj3_tariff_points(String subj3_tariff_points) {
    this.subj3_tariff_points = subj3_tariff_points;
  }

  public String getSubj3_tariff_points() {
    return subj3_tariff_points;
  }

  public void setSubj4_tariff_points(String subj4_tariff_points) {
    this.subj4_tariff_points = subj4_tariff_points;
  }

  public String getSubj4_tariff_points() {
    return subj4_tariff_points;
  }

  public void setSubj5_tariff_points(String subj5_tariff_points) {
    this.subj5_tariff_points = subj5_tariff_points;
  }

  public String getSubj5_tariff_points() {
    return subj5_tariff_points;
  }

  public void setSubj6_tariff_points(String subj6_tariff_points) {
    this.subj6_tariff_points = subj6_tariff_points;
  }

  public String getSubj6_tariff_points() {
    return subj6_tariff_points;
  }

  public void setSelectedFilterUrl(String selectedFilterUrl) {
    this.selectedFilterUrl = selectedFilterUrl;
  }

  public String getSelectedFilterUrl() {
    return selectedFilterUrl;
  }


public String getClearingUser() {
   return clearingUser;
}


public void setClearingUser(String clearingUser) {
   this.clearingUser = clearingUser;
}

}
