package WUI.autocomplete.servlet;

import org.apache.commons.lang.builder.ToStringBuilder;

class Item {
  private String name;
  private String value;
  private boolean asData;
  public Item() {
  }
  public Item(String name, String value, boolean asData) {
    this.name = name;
    this.value = value;
    this.asData = asData;
  }
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getValue() {
    return value;
  }
  public void setValue(String value) {
    this.value = value;
  }
  public boolean isAsCData() {
    return asData;
  }
  public void setAsData(boolean asData) {
    this.asData = asData;
  }
  public String toString() {
    return (new ToStringBuilder(this)).append("name", name).append("value", value).append("asData", asData).toString();
  }
}
