package WUI.autocomplete.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class BaseAjaxServlet extends HttpServlet {
  public BaseAjaxServlet() {
  }
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String xml = null;
    try {
      xml = getXmlContent(request, response);
    } catch (Exception ex) {
      response.sendError(500, "Can not create response");
      return;
    }
    response.setContentType("text/xml; charset=UTF-8");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter pw = response.getWriter();
    pw.write(xml);
    pw.close();
  }
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
  }
  public abstract String getXmlContent(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse) throws Exception;
}
