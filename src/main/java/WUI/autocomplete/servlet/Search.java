package WUI.autocomplete.servlet;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Search implements Serializable {
  private String keyword;
  private String courseid;
  public Search() {
    super();
  }
  public Search(String keyword, String courseid) {
    super();
    this.keyword = keyword;
    this.courseid = courseid;
  }
  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }
  public String getKeyword() {
    return keyword;
  }
  public void setCourseid(String courseid) {
    this.courseid = courseid;
  }
  public String getCourseid() {
    return courseid;
  }
  public String toString() {
    return new ToStringBuilder(this).append("keyword", keyword).append("courseid", courseid).toString();
  }
}
