package WUI.autocomplete.servlet;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import oracle.jdbc.driver.OracleTypes;

import com.wuni.util.sql.DataModel;

public class SearchKeyword {

  List key = new ArrayList();
  static final List institutionNameList = new ArrayList();

  public SearchKeyword() {
  }
  static {
    ResultSet resultSet = null;
    Connection conn = null;
    java.sql.CallableStatement cStmt = null;
    try {
      conn = new DataModel().getDataSourceConnection();
      cStmt = conn.prepareCall("{?=call Wu_review_Pkg.get_college_list_fn(?)}");
      cStmt.registerOutParameter(1, OracleTypes.CURSOR);
      cStmt.setString(2, "%%%");
      cStmt.execute();
      resultSet = (ResultSet)cStmt.getObject(1);
      while (resultSet.next()) {
        institutionNameList.add(new Search(resultSet.getString("college_id"), resultSet.getString("college_name")));
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      try {
        if (resultSet != null) {
          resultSet.close();
        }
        if (cStmt != null) {
          cStmt.close();
        }
        if (conn != null) {
          conn.close();
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      }
    }
  }

  /**
   *    Constructor is used for AutoComplete in finderpod in course page
   *    It will generate all the records that matches the typed keyword in finderpod
   *    and store the "Course Title" value in the ArrayList.
   *  @param name as String
   */
  public SearchKeyword(String name, String collegeId, String collegesearch) {
    super();
    Connection conn = null;
    java.sql.CallableStatement cStmt = null;
    ResultSet resultset = null;
    if (collegesearch == null) {
      try {
        Vector vector = new Vector();
        vector.clear();
        vector.add(name);
        conn = new DataModel().getDataSourceConnection();
        if (collegeId == null) {
          key.clear();
          for (Iterator iter = institutionNameList.iterator(); iter.hasNext(); ) {
            Search search = (Search)iter.next();
            if (search.getCourseid().toLowerCase().indexOf(name.toLowerCase()) != -1) {
              key.add(new Search(search.getKeyword(), search.getCourseid()));
            }
          }
        }
        if (collegeId != null) {
          key.clear();
          conn = new DataModel().getDataSourceConnection();
          cStmt = conn.prepareCall("{?=call Wu_review_Pkg.get_course_list_fn(?,?)}");
          cStmt.registerOutParameter(1, OracleTypes.CURSOR);
          cStmt.setString(2, collegeId);
          cStmt.setString(3, name);
          cStmt.execute();
          resultset = (ResultSet)cStmt.getObject(1); ////////////// CODE_REVIEW this local ResultSet was not closed. and fixed on wu304_20110308 by Syed
          int count = 0;
          while (resultset.next()) {
            key.add(new Search(resultset.getString("course_id"), resultset.getString("course_title")));
            count++;
          }
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        try {
          if (resultset != null) {
            resultset.close();
          }
          if (cStmt != null) {
            cStmt.close();
          }
          if (conn != null) {
            conn.close();
          }
        } catch (Exception exception) {
          exception.printStackTrace();
        }
      }
    } else {
      try {
        key.clear();
        for (Iterator iter = institutionNameList.iterator(); iter.hasNext(); ) {
          Search search = (Search)iter.next();
          if (search.getCourseid().toLowerCase().indexOf(name.toLowerCase()) != -1) {
            key.add(new Search(search.getKeyword(), search.getCourseid()));
          }
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        try {
          if (resultset != null) {
            resultset.close();
          }
          if (cStmt != null) {
            cStmt.close();
          }
          if (conn != null) {
            conn.close();
          }
        } catch (Exception exception) {
          exception.printStackTrace();
        }
      }
    }
  }

  public List getCourses(String name) {
    List courseList = new ArrayList();
    for (Iterator iter = key.iterator(); iter.hasNext(); ) {
      Search search = (Search)iter.next();
      courseList.add(search);
    }
    return courseList;
  }

}
