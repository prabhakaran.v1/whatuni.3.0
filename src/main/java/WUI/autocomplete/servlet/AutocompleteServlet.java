package WUI.autocomplete.servlet;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AutocompleteServlet extends BaseAjaxServlet {
  List list;
  public String getXmlContent(HttpServletRequest request, HttpServletResponse response) throws Exception {
    HttpSession session = request.getSession(false);
    String keyword = request.getParameter("collegeName") != null? request.getParameter("collegeName"): request.getParameter("college_Name");
    if (request.getParameter("colName") != null && request.getParameter("colName").trim().length() > 0) {
      keyword = request.getParameter("colName");
    }
    if (request.getParameter("subjectCollegeName") != null && request.getParameter("subjectCollegeName").trim().length() > 0) {
      keyword = request.getParameter("subjectCollegeName");
    }
    if (request.getParameter("basketColegeName") != null && request.getParameter("basketColegeName").trim().length() > 0) {
      keyword = request.getParameter("basketColegeName");
    }
    String colegeid = request.getParameter("selectid");
    String searchcourse = request.getParameter("coursetitle");
    String collegesearch = request.getParameter("collegesearch");
    String autocomplete = request.getParameter("collegesearchautocomplete");
    if (autocomplete != null) {
      if (session.getAttribute("selectid") != null || collegesearch != null) {
        session.removeAttribute("selectid");
      }
    } else {
      if (!(String.valueOf(session.getAttribute("selectid")).equalsIgnoreCase("null"))) {
        colegeid = (String)session.getAttribute("selectid");
      }
    }
    if (searchcourse != null) {
      keyword = searchcourse;
    }
    SearchKeyword key = null;
    if (colegeid == null) {
      key = new SearchKeyword(keyword, colegeid, collegesearch);
      list = key.getCourses(keyword);
      Iterator iterator = list.iterator();
      while (iterator.hasNext()) {
        Search search = (Search)iterator.next();
      }
    } else {
      key = new SearchKeyword(keyword, colegeid, collegesearch);
      list.clear();
      list = key.getCourses(searchcourse);
      java.util.Iterator iterator = list.iterator();
      while (iterator.hasNext()) {
        Search search = (Search)iterator.next();
      }
    }
    return new AjaxXmlBuilder().addItems(list, "courseid", "keyword").toString();
  }
}
