package WUI.autocomplete.servlet;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

public class AjaxXmlBuilder {
  private String encoding;
  private List items;
  public AjaxXmlBuilder() {
    encoding = "UTF-8";
    items = new ArrayList();
  }
  public String getEncoding() {
    return encoding;
  }
  public void setEncoding(String encoding) {
    this.encoding = encoding;
  }
  public AjaxXmlBuilder addItem(String name, String value) {
    items.add(new Item(name, value, false));
    return this;
  }
  public AjaxXmlBuilder addItemAsCData(String name, String value) {
    items.add(new Item(name, value, true));
    return this;
  }
  public AjaxXmlBuilder addItems(Collection collection, String nameProperty, String valueProperty) throws IllegalAccessException, 
                                                                                                          InvocationTargetException, 
                                                                                                          NoSuchMethodException {
    return addItems(collection, nameProperty, valueProperty, false);
  }
  public AjaxXmlBuilder addItems(Collection collection, String nameProperty, String valueProperty, boolean asCData) throws IllegalAccessException, 
                                                                                                                           InvocationTargetException, 
                                                                                                                           NoSuchMethodException {
    for (Iterator iter = collection.iterator(); iter.hasNext(); ) {
      Object element = iter.next();
      String name = BeanUtils.getProperty(element, nameProperty);
      String value = BeanUtils.getProperty(element, valueProperty);
      if (asCData) {
        items.add(new Item(name, value, false));
      } else {
        items.add(new Item(name, value, true));
      }
    }
    return this;
  }
  public AjaxXmlBuilder addItemsAsCData(Collection collection, String nameProperty, String valueProperty) throws IllegalAccessException, 
                                                                                                                 InvocationTargetException, 
                                                                                                                 NoSuchMethodException {
    return addItems(collection, nameProperty, valueProperty, true);
  }
  public String toString() {
    StringBuffer xml = (new StringBuffer()).append("<?xml version=\"1.0\"");
    if (encoding != null) {
      xml.append(" encoding=\"");
      xml.append(encoding);
      xml.append("\"");
    }
    xml.append(" ?>");
    xml.append("<ajax-response>");
    xml.append("<response>");
    for (Iterator iter = items.iterator(); iter.hasNext(); xml.append("</item>")) {
      Item item = (Item)iter.next();
      xml.append("<item>");
      xml.append("<name>");
      if (item.isAsCData()) {
        xml.append("<![CDATA[");
      }
      xml.append(item.getName());
      if (item.isAsCData()) {
        xml.append("]]>");
      }
      xml.append("</name>");
      xml.append("<value>");
      if (item.isAsCData()) {
        xml.append("<![CDATA[");
      }
      xml.append(item.getValue());
      if (item.isAsCData()) {
        xml.append("]]>");
      }
      xml.append("</value>");
    }
    xml.append("</response>");
    xml.append("</ajax-response>");
    return xml.toString();
  }
}
