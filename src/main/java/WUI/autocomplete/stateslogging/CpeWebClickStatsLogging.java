package WUI.autocomplete.stateslogging;

import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.ResultSet;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mobile.util.MobileUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.layer.util.SpringConstants;
import com.wuni.util.sql.DataModel;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : CpeWebClickStatsLogging.java
* Description   :
* @version      : 1.0
* Change Log :
* *****************************************************************************************************************************************
* author	              Ver 	              Modified On     	Modification Details 	    Change
* *****************************************************************************************************************************************
* Sangeeth.S		    wu_310320			  09_MAR_2020	       Modified					Added lat and long for stats logging
* Prabhakaran V         1.2
* Kailash L             1.3                   23_JUN_2020                                   Added stats log for Clearing Sponsored and Featured.
* Kailash L				1.4                   21_JUL_2020                                   Added stats log for In Year Sponsored.
* Keerthana E           Wu_379                17_SEP_2020          Modified                 Added featured college id and order item id for chain logging						
* Sri Sankari		    wu_20200917           17_SEP_2020          1.5                      Added stats log for tariff logging(UCAS point)
* Sujitha V             wu_20201101           10_NOV_2020          Modified                 Added session for YOE to log in d_session_log.
* ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
@Controller
public class CpeWebClickStatsLogging {


	  @RequestMapping(value = "/ajaxlogging/cpe-web-click-db-logging", method = {RequestMethod.GET,RequestMethod.POST})
	  public ModelAndView cpeWebClicksStatsLogging(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		    //
		    SessionData sessionData = new SessionData();
		    String collegeId = request.getParameter("z") != null && request.getParameter("z").trim().length() > 0 ? request.getParameter("z").trim() : "0";
		    String subOrderItemId = request.getParameter("subOrderItemId") != null && request.getParameter("subOrderItemId").trim().length() > 0 ? request.getParameter("subOrderItemId").trim() : "0";
		    String networkId = request.getParameter("networkId") != null && request.getParameter("networkId").trim().length() > 0 ? request.getParameter("networkId").trim() : "2";
		    String externalUrl = request.getParameter("externalUrl") != null && request.getParameter("externalUrl").trim().length() > 0 ? request.getParameter("externalUrl").trim() : "0";
		    String clickType = request.getParameter("clickType") != null && request.getParameter("clickType").trim().length() > 0 ? request.getParameter("clickType").trim() : "0";
		    String sectionName = request.getParameter("sectionName") != null && request.getParameter("sectionName").trim().length() > 0 ? request.getParameter("sectionName").trim() : "Overview";
		    String userId = request.getParameter("userId") != null && request.getParameter("userId").trim().length() > 0 ? request.getParameter("userId").trim() : "0";
		    String autoEmailType = request.getParameter("emailFlag") != null && request.getParameter("emailFlag").trim().length() > 0 ? request.getParameter("emailFlag").trim() : "";
		    String orderItemId = request.getParameter("orderItemId") != null && request.getParameter("orderItemId").trim().length() > 0 ? request.getParameter("orderItemId").trim() : "0";
		    String adRollType = request.getParameter("adRollType") != null && request.getParameter("adRollType").trim().length() > 0 ? request.getParameter("adRollType").trim() : "0";//13-Jan-2015 for adroll remarketing
		    String vwcourseId = (!GenericValidator.isBlankOrNull(request.getParameter("vwcourseId"))) ? request.getParameter("vwcourseId").trim() : "0";//11-AUG-2015 for logging course Id
		    String hcCollegeId = (!GenericValidator.isBlankOrNull(request.getParameter("hcCollegeId"))) ? request.getParameter("hcCollegeId").trim() : ""; //31-May-2016 for logging institution id
		    String studyLevelId = request.getParameter("studyLevelId");
		    studyLevelId = StringUtils.isNoneBlank(studyLevelId) ? studyLevelId : "";
		    String searchCategory = request.getParameter("featuredSubject");
            studyLevelId = StringUtils.isNoneBlank(studyLevelId) ? studyLevelId : "";
		    String categoryCode = request.getParameter("categoryCode");
		    categoryCode = StringUtils.isNotBlank(categoryCode) ? categoryCode : "";
		    String mobileFlag = "";
		    String featuredFlag = request.getParameter("featuredFlag");
		    // wu_300320 - Sangeeth.S: Added lat and long for stats logging
		    String[] latAndLong = sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG).split(GlobalConstants.SPLIT_CONSTANT);
		    String latitude = (latAndLong.length > 0 && !GenericValidator.isBlankOrNull(latAndLong[0])) ? latAndLong[0].trim() : "";
		    String longitude = (latAndLong.length > 1 && !GenericValidator.isBlankOrNull(latAndLong[1])) ? latAndLong[1].trim() : "";
		    String sourceOrderItemId = StringUtils.isNotBlank(request.getParameter("sourceOrderItemId")) ? request.getParameter("sourceOrderItemId") : "0";
		    String sponsoredFlag = StringUtils.isNotBlank(request.getParameter("sponsoredFlag")) ? request.getParameter("sponsoredFlag") : "";
		    String userUcasScore = StringUtils.isNotBlank((String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE)) ? (String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE) : null; //Added for tariff-logging-ucas-point by Sri Sankari on wu_20200915 release
		    boolean autoEmailLink = false;
		    //Wu_379 - Keerthana E : Added featured college and order item id for chain logging
		    //String[] featuredCollegeAndOrderItemId = sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID).split(GlobalConstants.SPLIT_CONSTANT);
		    String[] featuredCollegeAndOrderItemId = StringUtils.isNotBlank(sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID)) ? sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID).split(GlobalConstants.SPLIT_CONSTANT) : null;
		    String featuredCollegeId =""; 
		    String featuredOrderItemId = "0";
		    if(!StringUtils.isAllBlank(featuredCollegeAndOrderItemId) && featuredCollegeAndOrderItemId.length == 2) {
		       featuredCollegeId = StringUtils.isNotBlank(featuredCollegeAndOrderItemId[0])? featuredCollegeAndOrderItemId[0] : "0";
		       featuredOrderItemId = StringUtils.isNotBlank(featuredCollegeAndOrderItemId[1])? featuredCollegeAndOrderItemId[1] : "0"; 
		    }
		    /*if (clickType.equalsIgnoreCase("WEBSITE") || clickType.equalsIgnoreCase("PROSPECTUS_WEBFORM") || clickType.equalsIgnoreCase("EMAIL_WEBFORM") || clickType.equalsIgnoreCase("PROVIDER_OD_RESERVE_PLACE") || clickType.equalsIgnoreCase("WEBSITE_PROFILE_INNERLINK")) {  
		    	  if(StringUtils.equals(collegeId, featuredCollegeId) && StringUtils.isBlank(sponsoredFlag)) {
    	    sourceOrderItemId =  featuredOrderItemId;
	      }
	    }*/
		    if (StringUtils.equalsIgnoreCase(clickType, "WEBSITE") || StringUtils.equalsIgnoreCase(clickType, "PROSPECTUS_WEBFORM") || StringUtils.equalsIgnoreCase(clickType, "EMAIL_WEBFORM") || StringUtils.equalsIgnoreCase(clickType, "PROVIDER_OD_RESERVE_PLACE") || StringUtils.equalsIgnoreCase(clickType, "WEBSITE_PROFILE_INNERLINK")) {  
		      if(StringUtils.equals(collegeId, featuredCollegeId) && StringUtils.isBlank(sponsoredFlag)) {
        sourceOrderItemId =  featuredOrderItemId;
      }
    }
		    String yearOfEntryForDSession = StringUtils.isNotBlank((String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING)) ? (String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING) : null; //Added for getting YOE value in session to log in d_session_log stats by Sujitha V on 2020_NOV_10 rel
		    if(new MobileUtils().mobileUserAgentCheck(request)){
		      mobileFlag = "Y";
		    }
		    //redefine section name
		    if (sectionName.equalsIgnoreCase("Overview")) {
		      sectionName = "Overview";
		    } else if (sectionName.equalsIgnoreCase("Location")) {
		      sectionName = "Accommodation & Location";
		    } else if (sectionName.equalsIgnoreCase("Entertainment")) {
		      sectionName = "Student Union & Entertainment";
		    } else if (sectionName.equalsIgnoreCase("Facilities")) {
		      sectionName = "Courses, Academics & Facilities";
		    } else if (sectionName.equalsIgnoreCase("Welfare")) {
		      sectionName = "Welfare";
		    } else if (sectionName.equalsIgnoreCase("Funding")) {
		      sectionName = "Fees And Funding";
		    } else if (sectionName.equalsIgnoreCase("Prospects")) {
		      sectionName = "Job Prospects";
		    } else if (sectionName.equalsIgnoreCase("Contact")) {
		      sectionName = "Contact Details";
		    }
		    
		    if(!GenericValidator.isBlankOrNull(externalUrl)) {
		      try{
		        externalUrl = URLDecoder.decode(externalUrl, "UTF-8");        
		      }catch(Exception e){
		        e.printStackTrace();
		      }
		    }                 
		    //
		    String logId = "-999";		    
		    String x = sessionData.getData(request, "x");
		    String y = sessionData.getData(request, "y");
		    String requestUrl = new CommonUtil().getRequestedURL(request);
		    String activity = "";
		    //
		    session = request.getSession();
		    //wu413_20120710 loging the widgetId in d_session_log
		    String widgetId = "";
		    if(session.getAttribute("widgetId") != null && session.getAttribute("widgetId").toString().trim().length() > 0){
		        widgetId = (String)session.getAttribute("widgetId");
		    }
		    if (clickType.equalsIgnoreCase("WEBSITE")) {
		      activity = GlobalConstants.WHATUNI_STATS_WEB_CLICK;
		      requestUrl = externalUrl;
		    } else if (clickType.equalsIgnoreCase("WEBSITE_PROFILE_INNERLINK")) {
		      activity = GlobalConstants.WHATUNI_STATS_WEB_CLICK;
		      externalUrl = sectionName + " : " + externalUrl;
		      requestUrl = externalUrl;
		    }else if (clickType.equalsIgnoreCase("CLEARING_WEBSITE_PROFILE_INNERLINK")) {
		      activity = GlobalConstants.CLEARING_STATS_WEB_CLICK;
		      externalUrl = sectionName + " : " + externalUrl;
		      requestUrl = externalUrl;
		    }else if (clickType.equalsIgnoreCase("PROSPECTUS_WEBFORM")) {
		      activity = GlobalConstants.STATS_COLLEGE_PROSPECTUS_WEBFORM;
		      requestUrl = externalUrl;
		    } else if (clickType.equalsIgnoreCase("EMAIL_WEBFORM")) {
		      activity = GlobalConstants.STATS_COLLEGE_EMAIL_WEBFORM;
		      requestUrl = externalUrl;
		    } else if (clickType.equalsIgnoreCase("APPLYNOW")) {
		      activity = GlobalConstants.STATS_COURSE_OPPORTUNITY_APPLY_NOW;
		    } else if(clickType.equalsIgnoreCase("WEBSITE_CLEARING")){
		      activity = GlobalConstants.CLEARING_STATS_WEB_CLICK;
		      requestUrl = externalUrl;
		    }else if(clickType.equalsIgnoreCase("DOWNLOAD_PROSPECTUS_WEBFORM")){
		      activity = GlobalConstants.DOWNLOAD_PROSPECTUS_WEB_CLICK;
		      requestUrl = externalUrl;
		    }else if(clickType.equalsIgnoreCase("LEARNING_TEACHING_LINK")){
		      activity = GlobalConstants.WHATUNI_STATS_WEB_CLICK;
		      externalUrl = "ASSESSMENT(KIS): " + externalUrl;
		      requestUrl = externalUrl;
		    }else if(clickType.equalsIgnoreCase("FINANCIAL_SUPPORT_URL")){
		      activity = GlobalConstants.WHATUNI_STATS_WEB_CLICK;
		      externalUrl = "FUNDING(KIS): " + externalUrl;
		      requestUrl = externalUrl;
		    }else if(clickType.equalsIgnoreCase("HOTLINE")){
		      activity = GlobalConstants.CLEARING_STATS_HOTLINE;
		    }else if(clickType.equalsIgnoreCase("SPOTLIGHT_VIDEO")){//16_SEP_2014
		      activity = GlobalConstants.STATS_SPOTLIGHT_VIDEO;
		    }else if(clickType.equalsIgnoreCase("HERO_IMAGE_VIEW")){//18_Nov_2014 added hero image stats view by Thiyagu G.
		      activity = GlobalConstants.STATS_HERO_IMAGE_VIEW;
		    }else if(clickType.equalsIgnoreCase("HERO_IMAGE_CLICK")){//18_Nov_2014 added hero image stats click by Thiyagu G.
		      activity = GlobalConstants.STATS_HERO_IMAGE_CLICK;
		      requestUrl = externalUrl;
		    }else if(clickType.equalsIgnoreCase("ADVICE_DETAIL_VIEW")){//09_Dec_2014 added advice detail view by Thiyagu G.
		      activity = GlobalConstants.STATS_ADVICE_DETAIL_VIEW;
		    }else if(clickType.equalsIgnoreCase("ARTICLE_SPONSORSHIP_WEBCLICK")){//9_June_2015_rel
		      activity = GlobalConstants.STATS_ARTICLE_SPONSORSHIP_WEBCLICK;    
		      requestUrl = externalUrl;
		    }else if(clickType.equalsIgnoreCase("PROVIDER_OPEN_DAYS_ADD_CALENDAR")){
		      activity  = GlobalConstants.STATS_OPEN_DAYS_AD_CALENDAR;
		      externalUrl = "";
		    }else if(clickType.equalsIgnoreCase("PROVIDER_OD_RESERVE_PLACE")){
		      activity  = GlobalConstants.STATS_PROVIDER_OD_RESERVE_PLACE;
		      requestUrl = externalUrl;
		    }    
		    else if(clickType.equalsIgnoreCase("PROVIDER_RESULTS_LIST") || clickType.equalsIgnoreCase("PROVIDER_REVIEW_LIST") || clickType.equalsIgnoreCase("PROVIDER_REVIEW_VIEW") || 
		      clickType.equalsIgnoreCase("PROVIDER_PROSPECTUS_LIST") || clickType.equalsIgnoreCase("PROVIDER_SCHOLARSHIP_LIST") || clickType.equalsIgnoreCase("PROVIDER_SCHOLARSHIP_VIEW") || 
		      clickType.equalsIgnoreCase("PROVIDER_OPENDAYS_VIEW")){   //09_DEC_2014 added by Amir for Provider Stats Logging                         
		      if(clickType.equalsIgnoreCase("PROVIDER_RESULTS_LIST")){      
		        activity  = GlobalConstants.STATS_PROVIDER_RESULTS_LIST;
		      }else if(clickType.equalsIgnoreCase("PROVIDER_REVIEW_LIST")){
		        activity  = GlobalConstants.STATS_PROVIDER_REVIEW_LIST;
		      }else if(clickType.equalsIgnoreCase("PROVIDER_REVIEW_VIEW")){
		        activity  = GlobalConstants.STATS_PROVIDER_REVIEW_VIEW;
		      }else if(clickType.equalsIgnoreCase("PROVIDER_PROSPECTUS_LIST")){
		        activity  = GlobalConstants.STATS_PROVIDER_PROSPECTUS_LIST;
		      }else if(clickType.equalsIgnoreCase("PROVIDER_SCHOLARSHIP_LIST")){
		        activity  = GlobalConstants.STATS_PROVIDER_SCHOLARSHIP_LIST;
		      }else if(clickType.equalsIgnoreCase("PROVIDER_SCHOLARSHIP_VIEW")){
		        activity  = GlobalConstants.STATS_PROVIDER_SCHOLARSHIP_VIEW;
		      }else if(clickType.equalsIgnoreCase("PROVIDER_OPENDAYS_VIEW")){
		        activity  = GlobalConstants.STATS_PROVIDER_OPENDAYS_VIEW;
		      }
		      if(request.getSession().getAttribute("cpeQualificationNetworkId")!=null && !"".equals(request.getSession().getAttribute("cpeQualificationNetworkId"))) {
		        networkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");  
		      }                  
		      if(externalUrl!=null && "0".equals(externalUrl)) {
		        externalUrl = "";
		      }      
		    }else if(clickType.equalsIgnoreCase("RICH_PROFILE_SECVIDEO")){
		        activity  = GlobalConstants.STATS_PROVIDER_RICHPROFILE_SEC_VIDEOCLICK; //Added by Amir for 13_JAN_15 
		    }else if(clickType.equalsIgnoreCase("NON_ADV_PDF_DOWNLOAD")){ //Added new pdf stats key for no advertiasor pdf download for 24_Nov_2015, By Thiyagu G.
		        activity  = GlobalConstants.STATS_NON_ADVERTAISOR_PDF_DOWNLOAD;
		    }else if(clickType.equalsIgnoreCase("FEATURED_HOTLINE")){
		        activity = GlobalConstants.CLEARING_STATS_FEATURED_HOTLINE;
		    }else if(clickType.equalsIgnoreCase("APPLY_NOW")){ //Added for logging apply now click in CD page by Prabha
			    activity = GlobalConstants.STATS_WUGO_APPLY_NOW;
		    }else if(clickType.equalsIgnoreCase("FeaturedClick")) {//Added for logging featuredclick in SR page- Jeyalakshmi Jan 6th 2020 rel
		       if("INTERNAL".equalsIgnoreCase(featuredFlag)) {
		    	 addSessionData(request,response,collegeId,orderItemId);  
		    	 activity = GlobalConstants.FEATURED_INTERNAL_PROVIDER;
			     String protocolDomain = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();
			     requestUrl = (externalUrl.indexOf(protocolDomain) < 0) ? (protocolDomain + externalUrl) : externalUrl;		         
		       } else if("EXTERNAL".equalsIgnoreCase(featuredFlag)) {
				 activity = GlobalConstants.FEATURED_EXTERNAL_PROVIDER;  	
				 requestUrl = externalUrl;
		      /* } else if("FEATUREDVIDEO".equalsIgnoreCase(featuredFlag)) {
				 activity = GlobalConstants.FEATURED_VIDEO;
				 System.out.println("FEATUREDVIDEO" + activity);*/
			   }else if("FEATURED_PAGE_VIEW".equalsIgnoreCase(featuredFlag)) {
			     activity = GlobalConstants.FEATURED_PAGE_VIEW;
			   }
		    }else if(clickType.equalsIgnoreCase("CLEARING_PROFILE_VIEW")) {
		      activity = GlobalConstants.STATS_CLEARING_PROFILE_VIEW;
		    }else if(clickType.equalsIgnoreCase("SPONSORED_WEBSITE_CLEARING")){
			  activity = SpringConstants.CLEARING_STATS_SPONSORED_WEBSITE_CLICK;
			  requestUrl = externalUrl;
			} else if(clickType.equalsIgnoreCase("SPONSORED_HOTLINE_CLEARING")){
			  activity = SpringConstants.CLEARING_STATS_SPONSORED_HOTLINE_CLICK;
			} else if(clickType.equalsIgnoreCase("CLEARING_SPONSORED_PAGE_VIEW")){
			  activity = SpringConstants.CLEARING_STATS_SPONSORED_BOOSTING;
		    }else if(clickType.equalsIgnoreCase("FEATURED_CLICK_CLEARING")) {
		       if("INTERNAL".equalsIgnoreCase(featuredFlag)) {
			    activity = SpringConstants.CLEARING_STATS_FEATURED_INSTITUTION_INTERNAL_LINK;
			    String protocolDomain = GlobalConstants.WHATUNI_SCHEME_NAME + request.getServerName();
			    requestUrl = (externalUrl.indexOf(protocolDomain) < 0) ? (protocolDomain + externalUrl) : externalUrl;		         
		       } else if("EXTERNAL".equalsIgnoreCase(featuredFlag)) {
				activity = SpringConstants.CLEARING_STATS_FEATURED_INSTITUTION_EXTERNAl_LINK;  	
			     requestUrl = externalUrl;
		       } else if("FEATURED_CLEARING_VIEO_VIEW".equalsIgnoreCase(featuredFlag)) {
				 activity = SpringConstants.CLEARING_STATS_FEATURED_INSTITUTION_VIDEO_VIEW;
			   }else if("FEATURED_CLEARING_BOOSTING".equalsIgnoreCase(featuredFlag)) {
			     activity = SpringConstants.CLEARING_STATS_FEATURED_INSTITUTION_BOOSTING;
			   } else if("CLEARING_FEATURED_HOTLINE".equalsIgnoreCase(featuredFlag)) {
				 activity = SpringConstants.CLEARING_STATS_FEATURED_INSTITUTION_HOTLINE;
			   }else if("CLEARING_FEATURED_WESITE".equalsIgnoreCase(featuredFlag)) {
			     activity = SpringConstants.CLEARING_STATS_FEATURED_INSTITUTION_WESITE;
			     requestUrl = externalUrl;
			   }
			 }else if(clickType.equalsIgnoreCase("SPONSORED_PAGE_VIEW")){
				 activity = SpringConstants.STATS_SPONSORED_BOOSTING;
			}
			else{
		        activity = GlobalConstants.WHATUNI_STATS_WEB_CLICK;
		    }
		    //
		    if(!GenericValidator.isBlankOrNull(hcCollegeId)){
		      String institutionId = new GlobalFunction().getInstitutionId(hcCollegeId);
		      if(!GenericValidator.isBlankOrNull(institutionId)){
		        StringBuffer outPutBuffer = new StringBuffer();
		        outPutBuffer.append(institutionId);    
		        response.setContentType("text/html");
		        response.setHeader("Cache-Control", "no-cache");
		        PrintWriter outPrint = response.getWriter();
		        outPrint.println(outPutBuffer);
		        return null;
		      }
		    }
		    //
		    String clientIp = request.getHeader("CLIENTIP");
		    if (clientIp == null || clientIp.length() == 0) {
		      clientIp = request.getRemoteAddr();
		    }
		    String forwardURL = "";
		    if(!GenericValidator.isBlankOrNull(autoEmailType))
		    {
		      autoEmailLink = true;
		      y = userId;
		      if("VW".equalsIgnoreCase(autoEmailType)){
		        activity = GlobalConstants.WHATUNI_STATS_WEB_CLICK;
		      }else if("RP".equalsIgnoreCase(autoEmailType)){
		        activity = GlobalConstants.STATS_COLLEGE_PROSPECTUS_WEBFORM;
		      }else if("RI".equalsIgnoreCase(autoEmailType)){
		        activity = GlobalConstants.STATS_COLLEGE_EMAIL_WEBFORM;
		      }
		      getAutoEmailDetails(orderItemId, subOrderItemId, autoEmailType, userId, request);
		      networkId = (String)request.getAttribute("networkId");
		      String url = (String)request.getAttribute("url");
		      externalUrl = new CommonFunction().appendingHttptoURL(url);
		      String cappingLog = (String)request.getAttribute("cappingFlag");      
		      
		      if("N".equalsIgnoreCase(cappingLog)){
		        StringBuffer outPutBuffer = new StringBuffer();
		        outPutBuffer.append(externalUrl);
		        outPutBuffer.append("###");
		        outPutBuffer.append("N");
		        response.setContentType("text/html");
		        response.setHeader("Cache-Control", "no-cache");
		        PrintWriter outPrint = response.getWriter();
		        outPrint.println(outPutBuffer);
		        return null;      
		      }else{
		        if(!GenericValidator.isBlankOrNull(externalUrl))
		        {
		          forwardURL = externalUrl;
		        }else
		        {
		          forwardURL = "/degrees/home.html";
		        }
		        widgetId = new CommonFunction().getWUSysVarValue("WU_EMAIL_WIDGETID");
		      }
		    }
		    // wu582_20181023 - Sabapathi: Added screenwidth and studymode for stats logging
		    String studyModeId = GenericValidator.isBlankOrNull(request.getParameter("studymodeid")) ? null : request.getParameter("studymodeid");
		    String screenWidth = GenericValidator.isBlankOrNull(request.getParameter("screenwidth")) ? GlobalConstants.DEFAULT_SCREEN_WIDTH : request.getParameter("screenwidth");
		    //
		    DataModel dataModel = new DataModel();
		    Vector parameters = new Vector();
		    parameters.add(x);
		    parameters.add(y);
		    parameters.add(GlobalConstants.WHATUNI_AFFILATE_ID);
		    parameters.add(activity);
		    parameters.add(collegeId);
		    if(request.getParameter("richProfile")!=null && "true".equals(request.getParameter("richProfile"))) {
		      parameters.add(request.getParameter("profileId")); //Added by Amir for 13_JAN_15
		    }else{
		      parameters.add(""); //profileId
		    }
		    parameters.add(""); //searchHeaderId
		    parameters.add(externalUrl); //extra text
		    parameters.add(clientIp);
		    parameters.add(request.getHeader("user-agent"));
		    parameters.add(requestUrl);
		    parameters.add(request.getHeader("referer"));
		    /*if("FEATUREDVIDEO".equalsIgnoreCase(featuredFlag)) {//Handled by Jeya for featured slot video Jan 6th 2020 rel
		      parameters.add("N"); //JS log flag
		    } else {*/
		      parameters.add("Y");
		    //}
		    parameters.add(subOrderItemId);
		    parameters.add(networkId);
		    parameters.add(widgetId);
		    parameters.add(vwcourseId); //p_course_id//11-AUG-2015 for logging course Id
		    parameters.add(""); //p_course_title
		    parameters.add(""); //p_department_id
		    parameters.add(""); //p_department_name
		    parameters.add(mobileFlag);
		    parameters.add(new SessionData().getData(request, "userTrackId"));
		    parameters.add("");//p_app_flag
		    parameters.add(studyModeId); //p_study_mode
		    parameters.add(screenWidth); //screenwidth
		    parameters.add(latitude); //latitude added by sangeeth.s for March2020 rel
		    parameters.add(longitude); //longitude
		    parameters.add(categoryCode); //p_category_code
		    parameters.add(orderItemId); //p_order_item_id
		    parameters.add(studyLevelId); //p_qualification
		    parameters.add(searchCategory); //p_search_category
		    parameters.add(sourceOrderItemId); //source_order_item_id
		    parameters.add(userUcasScore); // P_UCAS_TARIFF
		    parameters.add(yearOfEntryForDSession); //P_YEAR_OF_ENTRY
		    try {
		    
		      logId = dataModel.getString("WU_STATS_LOG_WRAPPER_PKG.wu_add_log_fn", parameters);
		    } catch (Exception exception) {
		      exception.printStackTrace();
		    } finally {
		      //
		    
		      //nullify unused objects
		      activity = null;
		      collegeId = null;
		      requestUrl = null;
		      clientIp = null;
		      parameters.clear();
		      parameters = null;
		      dataModel = null;
		      x = null;
		      y = null;
		      sessionData = null;
		      networkId = null;
		      subOrderItemId = null;
		      externalUrl = null;
		      sectionName = null;
		    }
		    if(autoEmailLink){      
		      StringBuffer outPutBuffer = new StringBuffer();
		      outPutBuffer.append(forwardURL);
		      if(request.getAttribute("cappingFlag")!=null && "Y".equals(request.getAttribute("cappingFlag"))){ //18_NOV_2014 Added by Amir for Insight
		        outPutBuffer.append("###");
		        outPutBuffer.append("Y");
		      }  
		      response.setContentType("text/html");
		      response.setHeader("Cache-Control", "no-cache");
		      PrintWriter outPrint = response.getWriter();
		      outPrint.println(outPutBuffer);
		      return null;
		    }
		    if(!GenericValidator.isBlankOrNull(adRollType)){////13-Jan-2015 for adroll remarketing
		      String adRollSegmentName = adRollRemarketing(collegeId, adRollType, "");
		      if(!GenericValidator.isBlankOrNull(adRollSegmentName)){
		        StringBuffer outPutBuffer = new StringBuffer();
		        outPutBuffer.append(adRollSegmentName);    
		        response.setContentType("text/html");
		        response.setHeader("Cache-Control", "no-cache");
		        PrintWriter outPrint = response.getWriter();
		        outPrint.println(outPutBuffer);
		        return null;
		      }
		    }
		    
		    return null;  
	  }
	  /**
	   *  function that returns the automate email URL
	   * wu516_19112013
	   * 
	   */
	  private void getAutoEmailDetails(String orderItemId, String subOrderItemId, String autoEmailType, String userId, HttpServletRequest request) {
	    DataModel dataModel = new DataModel();
	    Vector vector = new Vector();
	    ResultSet resultset = null;
	    vector.clear();
	    vector.add(orderItemId);
	    vector.add(subOrderItemId);
	    vector.add(autoEmailType);
	    vector.add(userId);
	    try {
	      resultset = dataModel.getResultSet("wu_automated_mail_pkg.get_button_urls", vector);
	      while (resultset.next()) {
	        request.setAttribute("networkId", resultset.getString("network_id"));
	        request.setAttribute("url", resultset.getString("url"));
	        request.setAttribute("cappingFlag",resultset.getString("loging_status") );
	      }
	    } catch (Exception exception) {
	      exception.printStackTrace();
	    } finally {
	      dataModel.closeCursor(resultset);
	    }
	  }
	  /**
	   * Added to get the segment type for Adroll marketing - for 13-JAN-2015_Release
	   * @param collegeid
	   * @param buttonType
	   * @param pageType
	   * @return segmentType
	   */
	  private String adRollRemarketing(String collegeid, String buttonType, String pageType){
	      DataModel datamodel = new DataModel();
	      ResultSet resultset = null;
	      Vector parameters = new Vector();
	      String segmentName = "";
	      parameters.add(null);
	      parameters.add("PROFILE_PAGE");
	      parameters.add("220703");
	      parameters.add(collegeid);
	      parameters.add(buttonType);
	      try {
	        segmentName =  datamodel.getString("hot_admin.get_pixel_tracking_fn", parameters);         
	        //
	      } catch (Exception e) {
	        e.printStackTrace();
	      } finally {
	         try {
	           datamodel.closeCursor(resultset);
	           if (resultset != null) {
	             resultset.close();
	           }
	           resultset = null;
	         } catch (Exception e) {
	           e.printStackTrace();
	         }
	         //nullify unused objects
	         datamodel = null;
	      }
	      return segmentName;
	  }
	  
	  /**
	   * This method is used to set the Featured orderItem Id in session
	   * @param request
	   * @param response
	   * @param collegeId
	   * @param orderItemId
	   */
   public static void addSessionData(HttpServletRequest request, HttpServletResponse response, String collegeId, String orderItemId) {
    SessionData sessionData = new SessionData();
    String[] sessionCollegeAndOrderId = StringUtils.isNotBlank(sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID)) ? sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID).split(GlobalConstants.SPLIT_CONSTANT) : null;
	    if(StringUtils.isAllBlank(sessionCollegeAndOrderId)) {
	      String collegeIdAndOrderItemId = collegeId + GlobalConstants.SPLIT_CONSTANT + orderItemId;
	      sessionData.addData(request, response, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID, collegeIdAndOrderItemId);    
	    } else {
	      //String[] featuredCollegeAndOrderItemId = sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID).split(GlobalConstants.SPLIT_CONSTANT);
	      String[] featuredCollegeAndOrderItemId = StringUtils.isNotBlank(sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID)) ? sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID).split(GlobalConstants.SPLIT_CONSTANT) : null;
	      String featuredCollegeId =""; 
	      if(!StringUtils.isAllBlank(featuredCollegeAndOrderItemId)) {
	        featuredCollegeId = StringUtils.isNotBlank(featuredCollegeAndOrderItemId[0])? featuredCollegeAndOrderItemId[0] : "";
	        if(!featuredCollegeId.equals(collegeId)) {
	          String collegeIdAndOrderItemId = collegeId + GlobalConstants.SPLIT_CONSTANT + orderItemId;
	          sessionData.addData(request, response, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID, collegeIdAndOrderItemId);    
	        }
	      }
	     }
    }  
}
