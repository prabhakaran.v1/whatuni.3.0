package WUI.autocomplete.stateslogging;

import java.net.URLDecoder;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.layer.util.SpringConstants;
import com.wuni.util.sql.DataModel;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : CpeWebClickStatsLogging.java
* Description   :
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	              Ver 	              Modified On     	Modification Details 	             Change
* **************************************************************************************************************************************
* 
* Sangeeth.S		  wu_310320			  09_MAR_2020	         Modified							 Added lat and long for stats logging
* Kailash L								  23.06.2020	         Modified                            JIRA - 288 - Added stats log for elite solts in view port
* Sri Sankari		  wu_20200917         17_SEP_2020            1.3                                 Added stats log for tariff logging(UCAS point)
* Sujitha V           wu_20201101         10_NOV_2020            Modified                            Added session for YOE to log in d_session_log.
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
@Controller
public class AdvertiserExternalUrlStatsLogging {


	  @RequestMapping(value = "/advertiser-external-url-stats-logging", method = {RequestMethod.GET,RequestMethod.POST})
	  public ModelAndView advertiserExternalUrlStatsLogging(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		    String logId = doStatsLog(request, session);    
		    return null; 
	  }	
	  private String doStatsLog(HttpServletRequest request, HttpSession session){
		    DataModel dataModel = new DataModel();
		    String logId = GlobalConstants.WHATUNI_STATS_WEB_CLICK;
		    SessionData sessionData = new SessionData();    
		    String x = sessionData.getData(request, "x");
		    String y = sessionData.getData(request, "y");
		    String activity = "";    
		    String networkId = "";
		    String collegeWebsite = "";
		    String requestUrl = CommonUtil.getRequestedURL(request); //Change the getRequestURL by Prabha on 28_Jun_2016
		    collegeWebsite = request.getParameter("url") != null ? request.getParameter("url") : "-999" ;   
		    String logType = request.getParameter("logType");
		    if(logType == null){
		      activity = GlobalConstants.WHATUNI_STATS_WEB_CLICK;
		    }else{
		      if("tickertape".equalsIgnoreCase(logType)) {  
		        activity = GlobalConstants.STATS_TICKER_TAPE;    
		      } else if("categorySponsership".equalsIgnoreCase(logType)){
		        activity = GlobalConstants.STATS_CATEGORY_SPONSERSHIP;   
		      }else if("featuredCourse".equalsIgnoreCase(logType)){
		        activity = GlobalConstants.STATS_FEATURED_COURSE;
		      }else if("clearingCategorySponsership".equalsIgnoreCase(logType)){  
		        activity = GlobalConstants.CLEARING_STATS_KEYWORD_SPONSORSHIP;
		        networkId = "2";
		      }else if("clearingFeaturedProviders".equalsIgnoreCase(logType)){
		        activity = GlobalConstants.CLEARING_STATS_FEATURED_PROVIDERS;
		        networkId = "2";
		        requestUrl = collegeWebsite;
		      } else if("clearingFeaturedProvidersBoosting".equalsIgnoreCase(logType)){
			    activity = SpringConstants.CLEARING_STATS_FEATURED_PROVIDERS_BOOSTING;
			    networkId = "2";
			  } else{
		        activity = GlobalConstants.WHATUNI_STATS_WEB_CLICK;
		      }
		    }       
		    String collegeId = request.getParameter("z") != null && request.getParameter("z").trim().length() > 0 ? request.getParameter("z").trim() : "0";
		    if(collegeWebsite!=null && !"".equals(collegeWebsite)) {
		      try{
		        collegeWebsite = URLDecoder.decode(collegeWebsite, "UTF-8");
		      }catch(Exception e){
		        e.printStackTrace();
		      }
		    }
		    String clientIp = request.getHeader("CLIENTIP");    
		    if (clientIp == null || clientIp.length() == 0) {
		      clientIp = request.getRemoteAddr();
		    }
		    // wu582_20181023 - Sabapathi: Added screenwidth and studymode for stats logging
		    String studyModeId = GenericValidator.isBlankOrNull(request.getParameter("studymodeid")) ? null : request.getParameter("studymodeid");
		    String screenWidth = GenericValidator.isBlankOrNull(request.getParameter("screenwidth")) ? GlobalConstants.DEFAULT_SCREEN_WIDTH : request.getParameter("screenwidth");
		    // wu_300320 - Sangeeth.S: Added lat and long for stats logging
		    String[] latAndLong = sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG).split(GlobalConstants.SPLIT_CONSTANT);
			String latitude = (latAndLong.length > 0 && !GenericValidator.isBlankOrNull(latAndLong[0])) ? latAndLong[0].trim() : "";
			String longitude = (latAndLong.length > 1 && !GenericValidator.isBlankOrNull(latAndLong[1])) ? latAndLong[1].trim() : "";
		    String orderItemId = request.getParameter("orderItemId") != null && request.getParameter("orderItemId").trim().length() > 0 ? request.getParameter("orderItemId").trim() : "0";
		    String userUcasScore = StringUtils.isNotBlank((String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE)) ? (String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE) : null; //Added for tariff-logging-ucas-point by Sri Sankari on wu_20200915 release
		    String yearOfEntryForDSession = StringUtils.isNotBlank((String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING)) ? (String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING) : null; //Added for getting YOE value in session to log in d_session_log stats by Sujitha V on 2020_NOV_10 rel
		    //prepare parameters
		    Vector parameters = new Vector();    
		    parameters.add(x);
		    parameters.add(y);
		    parameters.add(GlobalConstants.WHATUNI_AFFILATE_ID);
		    parameters.add(activity);
		    parameters.add(collegeId);
		    parameters.add("");//profileId
		    parameters.add("");//searchHeaderId
		    parameters.add(collegeWebsite); //extra-text
		    parameters.add(clientIp);
		    parameters.add(request.getHeader("user-agent"));      
		    parameters.add(requestUrl);
		    parameters.add(request.getHeader("referer"));
		    parameters.add("Y");
		    parameters.add("");//p_suborder_item_id
		    parameters.add(networkId);//p_network_id
		    parameters.add("");//p_widget_id
		    parameters.add("");//p_course_id
		    parameters.add("");//p_course_title
		    parameters.add("");//p_department_id
		    parameters.add("");//p_department_name
		    parameters.add("");//p_mobile_flag
		    parameters.add(new SessionData().getData(request, "userTrackId"));//p_track_session
		    parameters.add("");//p_app_flag
		    parameters.add(studyModeId); //p_study_mode
		    parameters.add(screenWidth); //screenwidth
		    parameters.add(latitude); //latitude added by sangeeth.s for March2020 rel
		    parameters.add(longitude); //longitude
		    parameters.add(""); //p_category_code
		    parameters.add(orderItemId); //p_order_item_id
		    parameters.add(""); //p_qualification
		    parameters.add(""); //p_search_category
		    parameters.add(""); //source_order_item_id
		    parameters.add(userUcasScore); //P_Ucas_tariff 
		    parameters.add(yearOfEntryForDSession); //P_YEAR_OF_ENTRY
		    try {
		     
		      logId = dataModel.getString("WU_STATS_LOG_WRAPPER_PKG.wu_add_log_fn", parameters);
		    } catch (Exception exception) {
		      exception.printStackTrace();
		    }finally{
		      activity = null;
		      logType = null;
		      collegeId = null;
		      collegeWebsite = null;
		      requestUrl = null;
		      clientIp = null;
		      parameters.clear();
		      parameters = null;
		      dataModel = null;
		      x = null;
		      y = null;
		      sessionData = null;      
		    }
		    return logId;
		  }//end of doStatsLog()
		  
}
