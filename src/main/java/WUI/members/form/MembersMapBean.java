package WUI.members.form;

import org.apache.struts.action.ActionForm;

public class MembersMapBean {
  public MembersMapBean() {
  }
  private String userId = "";
  private String userName = "";
  private String collegeId = "";
  private String gender = "";
  private String userPhoto = "";
  private String nationality = "";
  private String homeTown = "";
  private String yearOfGraduation = "";
  private String iamaQuestion = "";
  private String latitudeValue = "";
  private String longtitudeValue = "";
  private String userThumbnail = "";
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }
  public String getUserName() {
    return userName;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setGender(String gender) {
    this.gender = gender;
  }
  public String getGender() {
    return gender;
  }
  public void setUserPhoto(String userPhoto) {
    this.userPhoto = userPhoto;
  }
  public String getUserPhoto() {
    return userPhoto;
  }
  public void setNationality(String nationality) {
    this.nationality = nationality;
  }
  public String getNationality() {
    return nationality;
  }
  public void setHomeTown(String homeTown) {
    this.homeTown = homeTown;
  }
  public String getHomeTown() {
    return homeTown;
  }
  public void setYearOfGraduation(String yearOfGraduation) {
    this.yearOfGraduation = yearOfGraduation;
  }
  public String getYearOfGraduation() {
    return yearOfGraduation;
  }
  public void setIamaQuestion(String iamaQuestion) {
    this.iamaQuestion = iamaQuestion;
  }
  public String getIamaQuestion() {
    return iamaQuestion;
  }
  public void setLatitudeValue(String latitudeValue) {
    this.latitudeValue = latitudeValue;
  }
  public String getLatitudeValue() {
    return latitudeValue;
  }
  public void setLongtitudeValue(String longtitudeValue) {
    this.longtitudeValue = longtitudeValue;
  }
  public String getLongtitudeValue() {
    return longtitudeValue;
  }
  public void setUserThumbnail(String userThumbnail) {
    this.userThumbnail = userThumbnail;
  }
  public String getUserThumbnail() {
    return userThumbnail;
  }
}
