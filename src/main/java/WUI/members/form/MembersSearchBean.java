package WUI.members.form;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
  *
  * @ViewAllRelatedMembersAction.java
  * @Version : 2.0
  * @July 20 2007
  * @www.whatuni.com
  * @author By : Balraj. Selva Kumar
  * @Purpose  : Program to generated all the related members list for the specified search result.
  */
public class MembersSearchBean {

  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String collegeId = "";
  private String subjectId = "";
  private String userId = "";
  private String userName = "";
  private String foreName = "";
  private String surName = "";
  private String dataOfBirth = "";
  private String email = "";
  private String iama = "";
  private String gender = "";
  private String nationality = "";
  private String subInterest1 = "";
  private String subInterest2 = "";
  private String userImage = "";
  private String userLargeImage = "";
  private String homeTown = "";
  private String yearOfGraduation = "";
  private String createdDate = "";
  private ArrayList subjectInterested = null;
  private String subjectCount = "0";
  private String schoolId = "";
  private String schoolName = "";
  private String optionId = "";
  private String optionText = "";
  private String subjectLinkList = "";

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public String getSubjectId() {
    return subjectId;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserName() {
    return userName;
  }

  public void setIama(String iama) {
    this.iama = iama;
  }

  public String getIama() {
    return iama;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return gender;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getNationality() {
    return nationality;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserId() {
    return userId;
  }

  public void setForeName(String foreName) {
    this.foreName = foreName;
  }

  public String getForeName() {
    return foreName;
  }

  public void setSurName(String surName) {
    this.surName = surName;
  }

  public String getSurName() {
    return surName;
  }

  public void setDataOfBirth(String dataOfBirth) {
    this.dataOfBirth = dataOfBirth;
  }

  public String getDataOfBirth() {
    return dataOfBirth;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public void setSubInterest1(String subInterest1) {
    this.subInterest1 = subInterest1;
  }

  public String getSubInterest1() {
    return subInterest1;
  }

  public void setSubInterest2(String subInterest2) {
    this.subInterest2 = subInterest2;
  }

  public String getSubInterest2() {
    return subInterest2;
  }

  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }

  public String getUserImage() {
    return userImage;
  }

  public void setUserLargeImage(String userLargeImage) {
    this.userLargeImage = userLargeImage;
  }

  public String getUserLargeImage() {
    return userLargeImage;
  }

  public void setHomeTown(String homeTown) {
    this.homeTown = homeTown;
  }

  public String getHomeTown() {
    return homeTown;
  }

  public void setYearOfGraduation(String yearOfGraduation) {
    this.yearOfGraduation = yearOfGraduation;
  }

  public String getYearOfGraduation() {
    return yearOfGraduation;
  }

  public void setCreatedDate(String createdDate) {
    this.createdDate = createdDate;
  }

  public String getCreatedDate() {
    return createdDate;
  }

  public void setSubjectInterested(ArrayList subjectInterested) {
    this.subjectInterested = subjectInterested;
  }

  public ArrayList getSubjectInterested() {
    return subjectInterested;
  }

  public void setSubjectCount(String subjectCount) {
    this.subjectCount = subjectCount;
  }

  public String getSubjectCount() {
    return subjectCount;
  }

  public void setSchoolId(String schoolId) {
    this.schoolId = schoolId;
  }

  public String getSchoolId() {
    return schoolId;
  }

  public void setSchoolName(String schoolName) {
    this.schoolName = schoolName;
  }

  public String getSchoolName() {
    return schoolName;
  }

/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    return errors;
  }*/

  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }

  public String getOptionId() {
    return optionId;
  }

  public void setOptionText(String optionText) {
    this.optionText = optionText;
  }

  public String getOptionText() {
    return optionText;
  }

  public void setSubjectLinkList(String subjectLinkList) {
    this.subjectLinkList = subjectLinkList;
  }

  public String getSubjectLinkList() {
    return subjectLinkList;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
