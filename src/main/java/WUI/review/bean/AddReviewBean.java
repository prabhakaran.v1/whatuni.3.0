package WUI.review.bean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import WUI.utilities.CommonFunction;

public class AddReviewBean {
  public AddReviewBean() {
  }
  private String collegeName = "";
  private String collegeId = "";
  private String currently = "";
  private String currentlyText = "";
  private String coursetitle = "";
  private String courseID = "";
  private String currently_key = "";
  private String currently_value = "";
  private String reviewTitle = "";
  private String sendFriendEmail = "";
  private String termsAndCondition = "";
  private String butvalue = "";
  private String cId = "";
  private String courseId = "";
  private String recommentuni = "";
  private String yearOfPassing = "";
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCurrently(String currently) {
    this.currently = currently;
  }
  public String getCurrently() {
    return currently;
  }
  public void setCourseID(String courseID) {
    this.courseID = courseID;
  }
  public String getCourseID() {
    return courseID;
  }
  public void setCurrently_Key(String currently_key) {
    this.currently_key = currently_key;
  }
  public String getCurrently_Key() {
    return currently_key;
  }
  public void setCurrently_Value(String currently_value) {
    this.currently_value = currently_value;
  }
  public String getCurrently_Value() {
    return currently_value;
  }
  public void setCurrentlyText(String currentlyText) {
    this.currentlyText = currentlyText;
  }
  public String getCurrentlyText() {
    return currentlyText;
  }
  public void setSendFriendEmail(String sendFriendEmail) {
    this.sendFriendEmail = sendFriendEmail;
  }
  public String getSendFriendEmail() {
    return sendFriendEmail;
  }
  public void setReviewTitle(String reviewTitle) {
    this.reviewTitle = reviewTitle;
  }
  public String getReviewTitle() {
    return reviewTitle;
  }
  public void setTermsAndCondition(String termsAndCondition) {
    this.termsAndCondition = termsAndCondition;
  }
  public String getTermsAndCondition() {
    return termsAndCondition;
  }
  public void setButvalue(String butvalue) {
    this.butvalue = butvalue;
  }
  public String getButvalue() {
    return butvalue;
  }
  public void setCId(String cId) {
    this.cId = cId;
  }
  public String getCId() {
    return cId;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setCoursetitle(String coursetitle) {
    this.coursetitle = coursetitle;
  }
  public String getCoursetitle() {
    return coursetitle;
  }
  public void setRecommentUni(String recommentuni) {
    this.recommentuni = recommentuni;
  }
  public String getRecommentUni() {
    return recommentuni;
  }
  public void setYearOfPassing(String yearOfPassing) {
    this.yearOfPassing = yearOfPassing;
  }
  public String getYearOfPassing() {
    return yearOfPassing;
  }
/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    errors.clear();
    HttpSession session = request.getSession(false);
    String errormessages[] = new String[10];
    ResourceBundle resourcebundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    errormessages[0] = resourcebundle.getString("wuni.error.over_all_rating");
    errormessages[1] = resourcebundle.getString("wuni.error.lecturers");
    errormessages[2] = resourcebundle.getString("wuni.error.accommodation");
    errormessages[3] = resourcebundle.getString("wuni.error.citylife");
    errormessages[4] = resourcebundle.getString("wuni.error.uni_facilities");
    errormessages[5] = resourcebundle.getString("wuni.error.clubs_societies");
    errormessages[6] = resourcebundle.getString("wuni.error.student_union");
    errormessages[7] = resourcebundle.getString("wuni.error.eyecandy");
    errormessages[8] = resourcebundle.getString("wuni.error.jobprospects");
    errormessages[9] = resourcebundle.getString("wuni.error.gettingplace");
    String over_rating_comment = resourcebundle.getString("wuni.error.manidatory");
    List Questionlist = (List)session.getAttribute("Questionlist");
    if (Questionlist == null) {
      errors.clear();
      request.setAttribute("sessionclosed", "TRUE");
      return errors;
    }
    if (request.getParameter("selectid") != null) {
      session.setAttribute("selectid", request.getParameter("selectid"));
    }
    if (request.getParameter("collegeName") != null) {
      session.setAttribute("collegeName", request.getParameter("collegeName"));
    }
    if (request.getParameter("coursetitle") != null) {
      session.setAttribute("coursetitle", request.getParameter("coursetitle"));
    }
    if (request.getParameter("courseid") != null) {
      session.setAttribute("courseid", request.getParameter("courseid"));
    }
    if (session.getAttribute("validate") != null) {
      session.removeAttribute("validate");
    } else {
      if (request.getParameter("selectid") == null || request.getParameter("selectid").equals("") || request.getParameter("selectid").trim().length() == 0) {
        errors.add("invalid_college", new ActionMessage("wuni.error.invalid_college"));
      }
      if (request.getParameter("butvalue") == null) {
        if (request.getParameter("courseid") == null || request.getParameter("courseid").equals("") || request.getParameter("courseid").trim().length() == 0) {
          errors.add("reviewqeustion", new ActionMessage("wuni.error.test"));
        }
      }
      if (errors.size() > 0) {
        return errors;
      }
    }
    errors.clear();
    boolean capsError = false;
    boolean swearingsError = false;
    if (request.getParameter("butvalue") != null && request.getParameter("butvalue").equals("Preview your review")) {
      List errorlist = new ArrayList();
      Iterator iterator = Questionlist.iterator();
      int arraycount = 0;
      while (iterator.hasNext()) {
        QuestionBean qbean = (QuestionBean)iterator.next();
        if (qbean.getQuestionGroupId() != null && qbean.getQuestionGroupId().equals("5")) {
          String reviewComment = request.getParameter(qbean.getQuestionID() + "tarea");
          reviewComment = reviewComment != null && reviewComment.trim().length() > 0? reviewComment.trim(): reviewComment;
          if (reviewComment != null && reviewComment.trim().length() != 0) {
            String returnError = new CommonFunction().checkCapsSwearing(request, reviewComment);
            if (returnError != null && returnError.equals("caps")) {
              capsError = true;
            }
            if (returnError != null && returnError.equals("swearing")) {
              swearingsError = true;
            }
          }
          qbean.setReviewComment(reviewComment);
          String optionvalue = request.getParameter(qbean.getQuestionID());
          if (optionvalue != null) {
            qbean.setAnswerSelected(optionvalue);
          } else {
            errors.add("reviewqeustion", new ActionMessage("wuni.error.test"));
            //errorlist.add(errormessages[arraycount]); //commented for 04 November 2009 release 
            errorlist.add(errormessages[arraycount] + " (<a class=\"editblack\" onclick=\"showDivToEdit('" + (arraycount + 1) + "')\">Edit</a>)"); //added for 04 November 2009 release" 
          }
          if (arraycount == 0) {
            if (request.getParameter("Btarea") != null && request.getParameter("Btarea").toString().trim().length() == 0) {
              errors.add("reviewqeustion", new ActionMessage("wuni.error.test"));
              //errorlist.add(over_rating_comment);//commented for 04 November 2009 release 
              errorlist.add(over_rating_comment + " (<a class=\"editblack\" onclick=\"showDivToEdit('1')\">Edit</a>)"); //added for 04 November 2009 release" 
            }
          }
          arraycount++;
        }
        if (qbean.getQuestionGroupId() != null && qbean.getQuestionGroupId().equals("2")) {
          if (recommentuni != null && recommentuni.trim().length() == 0) {
            errors.add("recommuniv", new ActionMessage("wuni.error.recommuniv")); // commented fof 14th July 2009 Release
            errorlist.add(resourcebundle.getString("wuni.error.recommuniv")); // added for 14th July 2009 Release
          } else {
            qbean.setAnswerSelected(recommentuni);
          }
        }
      }
      session.setAttribute("Questionlist", Questionlist);
      termsAndCondition = request.getParameter("termsandcondition");
      request.setAttribute("tcvalue", termsAndCondition);
      if (termsAndCondition == null) {
        errors.add("tcondition", new ActionMessage("wuni.error.tcondition")); // commented fof 14th July 2009 Release
        errorlist.add(resourcebundle.getString("wuni.error.tcondition")); // added for 14th July 2009 Release
      }
      if (errorlist.size() != 0) { // added for 14th July 2009 Release
        session.setAttribute("errorlist", errorlist);
      }
      if (reviewTitle != null && reviewTitle.trim().length() == 0) {
        errors.add("reviewtitle", new ActionMessage("wuni.error.reviewtitle"));
      } else {
        String returnError = new CommonFunction().checkCapsSwearing(request, reviewTitle);
        if (returnError != null && returnError.equals("caps")) {
          capsError = true;
        }
        if (returnError != null && returnError.equals("swearing")) {
          swearingsError = true;
        }
      }
      if (request.getParameter("courseid") == null || request.getParameter("courseid").equals("") || request.getParameter("courseid").trim().length() == 0) {
        errors.add("invalid_course", new ActionMessage("wuni.error.invalid_course"));
      }
    }
    // added for 04th November 2009 Relase 
    if (request.getParameter("butvalue") != null && (request.getParameter("butvalue").equals("Update Preview") || request.getParameter("butvalue").equalsIgnoreCase("Submit your review"))) {
      List errorlist = new ArrayList();
      Iterator iterator = Questionlist.iterator();
      int arraycount = 0;
      while (iterator.hasNext()) {
        QuestionBean qbean = (QuestionBean)iterator.next();
        if (qbean.getQuestionGroupId() != null && qbean.getQuestionGroupId().equals("5")) {
          String reviewComment = request.getParameter(qbean.getQuestionID() + "tarea");
          reviewComment = reviewComment != null && reviewComment.trim().length() > 0? reviewComment.trim(): reviewComment;
          if (reviewComment != null && reviewComment.trim().length() != 0) {
            String returnError = new CommonFunction().checkCapsSwearing(request, reviewComment);
            if (returnError != null && returnError.equals("caps")) {
              capsError = true;
            }
            if (returnError != null && returnError.equals("swearing")) {
              swearingsError = true;
            }
          } 
          qbean.setReviewComment(reviewComment);
          if (arraycount == 0) {
            if (request.getParameter("Btarea") != null && request.getParameter("Btarea").toString().trim().length() == 0) {
              errors.add("reviewqeustion", new ActionMessage("wuni.error.test"));
              errorlist.add(over_rating_comment);
            }
          }
          arraycount++;
        }
      }
      session.setAttribute("Questionlist", Questionlist);
      ServletContext servletContext = request.getServletContext();
      List userList = new CommonFunction().getMainDetails(String.valueOf(session.getAttribute("reviewid")), request, servletContext, "P");
      if (userList != null && userList.size() > 0) {
        request.setAttribute("userList", userList);
      }
      if (errorlist.size() != 0) { // added for 14th July 2009 Release
        session.setAttribute("errorlist", errorlist);
      }
      request.setAttribute("review_title", String.valueOf(session.getAttribute("review_title")));
    }
    if (swearingsError) {
        if (request.getParameter("butvalue") != null && request.getParameter("butvalue").equals("Preview your review")) {
            errors.add("swearingError", new ActionMessage("wuni.error.swearing1"));
        }
            if (request.getParameter("butvalue") != null && (request.getParameter("butvalue").equals("Update Preview") || request.getParameter("butvalue").equalsIgnoreCase("Submit your review"))) {
            errors.add("swearingError", new ActionMessage("wuni.error.swearing"));
        }
        request.setAttribute("presentswearing", "yes");
    }
    if (capsError) {
      errors.add("capsError", new ActionMessage("wuni.error.caps"));
      request.setAttribute("presentcaps", "yes");
    }
    return errors;
  }*/
}
