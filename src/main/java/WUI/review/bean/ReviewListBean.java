package WUI.review.bean;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class ReviewListBean {
  //New
  private String collegeId = "";
  private String collegeName = "";
  private String uniHomeURL = "";
  private String uniReviewsURL = "";
  private String collegeDisplayName = "";
  private String rating = "";
  private String roundRating = "";
  private String reviews = "";
  private String status = "";
  private String logo = "";
  private String sponsorURL = "";
  private String questionTitle = "";
  private String questionId = "";
  private String displaySeq = "";
  private String logoImageName = "";
  private String awardImage = "";
  private String awardImagePath = "";
  private String createdDate = "";
  private List viewMoreRes = null;
  private String overallRatingExact = "";
  private String richProfileImgPath = "";
  private String pullQuotes = "";
  private String studentRanking = "";
  private String totStudentRanking = "";
  private String timesRanking = "";
  private String totalStudents = "";
  private String dispQuestionTitle = "";
  private String imageTitle = "";
  
  //Old
  private String reviewid = "";
  private String reviewtitle = "";
  private String collegeid = "";
  private String collegename = "";
  private String collegeNameDisplay = "";
  private String username = "";
  private String userId = "";
  private String coursename = "";
  private String overallratingcomment = "";
  private String studcurrentstatus = "";
  private String nationality = "";
  private String gender = "";
  private String reviewquestion = "";
  private String overallrating = "";
  private String overAllRating = "";
  private String maxrating = "";
  private String userImage = "";
  private String userImageLarge = "";
  private String userGender = "";
  private String userGraduate = "";
  private String userImageTest = "";
  private String courseId = "";
  private String seoStudyLevelText = "";
  private String courseDeletedFlag = "";
  private String reviewQuestionId = "";
  private String seoQuestionText = "";
  private String subjectId = "";
  private String userType = "";
  private String reviewCount = "";
  private String reviewVotingCount = "";
  private String star = "";
  private String ratingPercent = "";
  private String reviewType = "";
  private String courseTitle = "";
  private String buttonLabel = "";
  private String viewAllCourseUrl = "";
  private String openDaysUrl = "";
  private String reviewDetailUrl = ""; //16_SEP_2014
  private String reviewerName = "";
  private String reviewDesc = "";
  private String collegeLandingUrl= "";
  private String positionId= "";
  private String totalCnt= "";  
  private String overallQuesDesc = "";
  private String sponsorName = "";
    private String reviewText = "";
  private String userNameInitial = "";
  private String reviewDate = "";
  private String userNameIntial = ""; 
  private String userNameIntialColorCode = "";
  private String commonPhrase = "";
  private String totalReviewCountDisplay ="";
  private String userNameInitialColourcode = "";
  private String reviewRatings = null;
  
  private String courseDetailsReviewURL = null;
  public ReviewListBean() {
  }
  public void setReviewId(String reviewid) {
    this.reviewid = reviewid;
  }
  public String getReviewId() {
    return reviewid;
  }
  public void setReviewTitle(String reviewtitle) {
    this.reviewtitle = reviewtitle;
  }
  public String getReviewTitle() {
    return reviewtitle;
  }
  public void setCollegeName(String collegename) {
    this.collegename = collegename;
  }
  public String getCollegeName() {
    return collegename;
  }
  public void setUserName(String username) {
    this.username = username;
  }
  public String getUserName() {
    return username;
  }
  public void setCourseName(String coursename) {
    this.coursename = coursename;
  }
  public String getCourseName() {
    return coursename;
  }
  public void setOverallRatingComment(String overallratingcomment) {
    this.overallratingcomment = overallratingcomment;
  }
  public String getOverallRatingComment() {
    return overallratingcomment;
  }
  public void setStudCurrentStatus(String studcurrentstatus) {
    this.studcurrentstatus = studcurrentstatus;
  }
  public String getStudCurrentStatus() {
    return studcurrentstatus;
  }
  public void setReviewQuestion(String reviewquestion) {
    this.reviewquestion = reviewquestion;
  }
  public String getReviewQuestion() {
    return reviewquestion;
  }
  public void setOverallRating(String overallrating) {
    this.overallrating = overallrating;
  }
  public String getOverallRating() {
    return overallrating;
  }
  public void setMaxRating(String maxrating) {
    this.maxrating = maxrating;
  }
  public String getMaxRating() {
    return maxrating;
  }
  public void setCollegeId(String collegeid) {
    this.collegeid = collegeid;
  }
  public String getCollegeId() {
    return collegeid;
  }
  public void setNationality(String nationality) {
    this.nationality = nationality;
  }
  public String getNationality() {
    return nationality;
  }
  public void setGender(String gender) {
    this.gender = gender;
  }
  public String getGender() {
    return gender;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }
  public String getUserImage() {
    return userImage;
  }
  public void setUserImageLarge(String userImageLarge) {
    this.userImageLarge = userImageLarge;
  }
  public String getUserImageLarge() {
    return userImageLarge;
  }
  public void setUserImageTest(String userImageTest) {
    this.userImageTest = userImageTest;
  }
  public String getUserImageTest() {
    return userImageTest;
  }
  public void setOverAllRating(String overAllRating) {
    this.overAllRating = overAllRating;
  }
  public String getOverAllRating() {
    return overAllRating;
  }
  public void setUserGender(String userGender) {
    this.userGender = userGender;
  }
  public String getUserGender() {
    return userGender;
  }
  public void setUserGraduate(String userGraduate) {
    this.userGraduate = userGraduate;
  }
  public String getUserGraduate() {
    return userGraduate;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }
  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }
  public void setCourseDeletedFlag(String courseDeletedFlag) {
    this.courseDeletedFlag = courseDeletedFlag;
  }
  public String getCourseDeletedFlag() {
    return courseDeletedFlag;
  }
  public void setReviewQuestionId(String reviewQuestionId) {
    this.reviewQuestionId = reviewQuestionId;
  }
  public String getReviewQuestionId() {
    return reviewQuestionId;
  }
  public void setSeoQuestionText(String seoQuestionText) {
    this.seoQuestionText = seoQuestionText;
  }
  public String getSeoQuestionText() {
    return seoQuestionText;
  }
  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }
  public String getSubjectId() {
    return subjectId;
  }
  public void setUserType(String userType) {
    this.userType = userType;
  }
  public String getUserType() {
    return userType;
  }
  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }
  public String getReviewCount() {
    return reviewCount;
  }
  public void setReviewVotingCount(String reviewVotingCount) {
    this.reviewVotingCount = reviewVotingCount;
  }
  public String getReviewVotingCount() {
    return reviewVotingCount;
  }
  public void setStar(String star) {
    this.star = star;
  }
  public String getStar() {
    return star;
  }
  public void setRatingPercent(String ratingPercent) {
    this.ratingPercent = ratingPercent;
  }
  public String getRatingPercent() {
    return ratingPercent;
  }
  public void setReviewType(String reviewType) {
    this.reviewType = reviewType;
  }
  public String getReviewType() {
    return reviewType;
  }
  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }
  public String getCourseTitle() {
    return courseTitle;
  }
  public void setButtonLabel(String buttonLabel) {
    this.buttonLabel = buttonLabel;
  }
  public String getButtonLabel() {
    return buttonLabel;
  }
  public void setReviewid(String reviewid) {
    this.reviewid = reviewid;
  }
  public String getReviewid() {
    return reviewid;
  }
  public void setReviewtitle(String reviewtitle) {
    this.reviewtitle = reviewtitle;
  }
  public String getReviewtitle() {
    return reviewtitle;
  }
  public void setCollegeid(String collegeid) {
    this.collegeid = collegeid;
  }
  public String getCollegeid() {
    return collegeid;
  }
  public void setCollegename(String collegename) {
    this.collegename = collegename;
  }
  public String getCollegename() {
    return collegename;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public String getUsername() {
    return username;
  }
  public void setCoursename(String coursename) {
    this.coursename = coursename;
  }
  public String getCoursename() {
    return coursename;
  }
  public void setOverallratingcomment(String overallratingcomment) {
    this.overallratingcomment = overallratingcomment;
  }
  public String getOverallratingcomment() {
    return overallratingcomment;
  }
  public void setStudcurrentstatus(String studcurrentstatus) {
    this.studcurrentstatus = studcurrentstatus;
  }
  public String getStudcurrentstatus() {
    return studcurrentstatus;
  }
  public void setReviewquestion(String reviewquestion) {
    this.reviewquestion = reviewquestion;
  }
  public String getReviewquestion() {
    return reviewquestion;
  }
  public void setOverallrating(String overallrating) {
    this.overallrating = overallrating;
  }
  public String getOverallrating() {
    return overallrating;
  }
  public void setMaxrating(String maxrating) {
    this.maxrating = maxrating;
  }
  public String getMaxrating() {
    return maxrating;
  }
  public void setViewAllCourseUrl(String viewAllCourseUrl) {
    this.viewAllCourseUrl = viewAllCourseUrl;
  }
  public String getViewAllCourseUrl() {
    return viewAllCourseUrl;
  }
  public void setOpenDaysUrl(String openDaysUrl) {
    this.openDaysUrl = openDaysUrl;
  }
  public String getOpenDaysUrl() {
    return openDaysUrl;
  }
  public void setReviewDetailUrl(String reviewDetailUrl) {
    this.reviewDetailUrl = reviewDetailUrl;
  }
  public String getReviewDetailUrl() {
    return reviewDetailUrl;
  }
  public void setReviewerName(String reviewerName) {
    this.reviewerName = reviewerName;
  }
  public String getReviewerName() {
    return reviewerName;
  }
  public void set_collegeId(String _collegeId) {
    this.collegeId = _collegeId;
  }
  public String get_collegeId() {
    return collegeId;
  }
  public void set_collegeName(String _collegeName) {
    this.collegeName = _collegeName;
  }
  public String get_collegeName() {
    return collegeName;
  }
  public void setCollegeDisplayName(String collegeDisplayName) {
    this.collegeDisplayName = collegeDisplayName;
  }
  public String getCollegeDisplayName() {
    return collegeDisplayName;
  }
  public void setRating(String rating) {
    this.rating = rating;
  }
  public String getRating() {
    return rating;
  }
  public void setReviews(String reviews) {
    this.reviews = reviews;
  }
  public String getReviews() {
    return reviews;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  public String getStatus() {
    return status;
  }
  public void setLogo(String logo) {
    this.logo = logo;
  }
  public String getLogo() {
    return logo;
  }
  public void setSponsorURL(String sponsorURL) {
    this.sponsorURL = sponsorURL;
  }
  public String getSponsorURL() {
    return sponsorURL;
  }
  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }
  public String getQuestionTitle() {
    return questionTitle;
  }
  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }
  public String getQuestionId() {
    return questionId;
  }
  public void setDisplaySeq(String displaySeq) {
    this.displaySeq = displaySeq;
  }
  public String getDisplaySeq() {
    return displaySeq;
  }
  public void setRoundRating(String roundRating) {
    this.roundRating = roundRating;
  }
  public String getRoundRating() {
    return roundRating;
  }
  public void setLogoImageName(String logoImageName) {
    this.logoImageName = logoImageName;
  }
  public String getLogoImageName() {
    return logoImageName;
  }
  public void setAwardImage(String awardImage) {
    this.awardImage = awardImage;
  }
  public String getAwardImage() {
    return awardImage;
  }
  public void setAwardImagePath(String awardImagePath) {
    this.awardImagePath = awardImagePath;
  }
  public String getAwardImagePath() {
    return awardImagePath;
  }
  public void setUniHomeURL(String uniHomeURL) {
    this.uniHomeURL = uniHomeURL;
  }
  public String getUniHomeURL() {
    return uniHomeURL;
  }
  public void setCreatedDate(String createdDate) {
    this.createdDate = createdDate;
  }
  public String getCreatedDate() {
    return createdDate;
  }
  public void setViewMoreRes(List viewMoreRes) {
    this.viewMoreRes = viewMoreRes;
  }
  public List getViewMoreRes() {
    return viewMoreRes;
  }
  public void setOverallRatingExact(String overallRatingExact) {
    this.overallRatingExact = overallRatingExact;
  }
  public String getOverallRatingExact() {
    return overallRatingExact;
  }
  public void setRichProfileImgPath(String richProfileImgPath) {
    this.richProfileImgPath = richProfileImgPath;
  }
  public String getRichProfileImgPath() {
    return richProfileImgPath;
  }
  public void setPullQuotes(String pullQuotes) {
    this.pullQuotes = pullQuotes;
  }
  public String getPullQuotes() {
    return pullQuotes;
  }
  public void setStudentRanking(String studentRanking) {
    this.studentRanking = studentRanking;
  }
  public String getStudentRanking() {
    return studentRanking;
  }
  public void setTotStudentRanking(String totStudentRanking) {
    this.totStudentRanking = totStudentRanking;
  }
  public String getTotStudentRanking() {
    return totStudentRanking;
  }
  public void setTimesRanking(String timesRanking) {
    this.timesRanking = timesRanking;
  }
  public String getTimesRanking() {
    return timesRanking;
  }
  public void setTotalStudents(String totalStudents) {
    this.totalStudents = totalStudents;
  }
  public String getTotalStudents() {
    return totalStudents;
  }
  public void setDispQuestionTitle(String dispQuestionTitle) {
    this.dispQuestionTitle = dispQuestionTitle;
  }
  public String getDispQuestionTitle() {
    return dispQuestionTitle;
  }

    public void setUniReviewsURL(String uniReviewsURL) {
        this.uniReviewsURL = uniReviewsURL;
    }

    public String getUniReviewsURL() {
        return uniReviewsURL;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getImageTitle() {
        return imageTitle;
    }
  public void setReviewDesc(String reviewDesc) {
    this.reviewDesc = reviewDesc;
  }
  public String getReviewDesc() {
    return reviewDesc;
  }
  public void setCollegeLandingUrl(String collegeLandingUrl) {
    this.collegeLandingUrl = collegeLandingUrl;
  }
  public String getCollegeLandingUrl() {
    return collegeLandingUrl;
  }
  public void setPositionId(String positionId) {
    this.positionId = positionId;
  }
  public String getPositionId() {
    return positionId;
  }
  public void setTotalCnt(String totalCnt) {
    this.totalCnt = totalCnt;
  }
  public String getTotalCnt() {
    return totalCnt;
  }

  public void setOverallQuesDesc(String overallQuesDesc) {
    this.overallQuesDesc = overallQuesDesc;
  }

  public String getOverallQuesDesc() {
    return overallQuesDesc;
  }
  public void setSponsorName(String sponsorName) {
    this.sponsorName = sponsorName;
  }
  public String getSponsorName() {
    return sponsorName;
  }


    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setUserNameInitial(String userNameInitial) {
        this.userNameInitial = userNameInitial;
    }

    public String getUserNameInitial() {
        return userNameInitial;
    }

    public void setReviewDate(String reviewDate) {
        this.reviewDate = reviewDate;
    }

    public String getReviewDate() {
        return reviewDate;
    }
  public void setUserNameIntial(String userNameIntial) {
    this.userNameIntial = userNameIntial;
  }

  public String getUserNameIntial() {
    return userNameIntial;
  }

  public void setUserNameIntialColorCode(String userNameIntialColorCode) {
    this.userNameIntialColorCode = userNameIntialColorCode;
  }

  public String getUserNameIntialColorCode() {
    return userNameIntialColorCode;
  }

  public void setCommonPhrase(String commonPhrase) {
    this.commonPhrase = commonPhrase;
  }

  public String getCommonPhrase() {
    return commonPhrase;
  }

  public void setTotalReviewCountDisplay(String totalReviewCountDisplay) {
    this.totalReviewCountDisplay = totalReviewCountDisplay;
  }

  public String getTotalReviewCountDisplay() {
    return totalReviewCountDisplay;
  }

    public void setUserNameInitialColourcode(String userNameInitialColourcode) {
        this.userNameInitialColourcode = userNameInitialColourcode;
    }

    public String getUserNameInitialColourcode() {
        return userNameInitialColourcode;
    }

    public void setReviewRatings(String reviewRatings) {
        this.reviewRatings = reviewRatings;
    }

    public String getReviewRatings() {
        return reviewRatings;
    }

    public void setCourseDetailsReviewURL(String courseDetailsReviewURL) {
        this.courseDetailsReviewURL = courseDetailsReviewURL;
    }

    public String getCourseDetailsReviewURL() {
        return courseDetailsReviewURL;
    }
}
////////////////////////////////////////////////E N D   O F   F I L E/////////////////////////////////////////////////////////////
