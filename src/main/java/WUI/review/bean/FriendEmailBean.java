package WUI.review.bean;

import org.apache.struts.action.ActionForm;

public class FriendEmailBean {
  public FriendEmailBean() {
  }
  private String email1 = "";
  private String email2 = "";
  private String email3 = "";
  private String email4 = "";
  private String email5 = "";
  private String email6 = "";
  private String submitbuttonvalue = "";
  private String senderName = "";
  private String receiverName = "";
  private String typedContent = "";
  private String senderEmail = "";
  private String searchLink = "";
  private String mailmesg = "";
  private String butvalue = "";
  public void setEmail1(String email1) {
    this.email1 = email1;
  }
  public String getEmail1() {
    return email1;
  }
  public void setEmail2(String email2) {
    this.email2 = email2;
  }
  public String getEmail2() {
    return email2;
  }
  public void setEmail3(String email3) {
    this.email3 = email3;
  }
  public String getEmail3() {
    return email3;
  }
  public void setEmail4(String email4) {
    this.email4 = email4;
  }
  public String getEmail4() {
    return email4;
  }
  public void setEmail5(String email5) {
    this.email5 = email5;
  }
  public String getEmail5() {
    return email5;
  }
  public void setEmail6(String email6) {
    this.email6 = email6;
  }
  public String getEmail6() {
    return email6;
  }
  public void setMailmesg(String mailmesg) {
    this.mailmesg = mailmesg;
  }
  public String getMailmesg() {
    return mailmesg;
  }
  public void setButvalue(String butvalue) {
    this.butvalue = butvalue;
  }
  public String getButvalue() {
    return butvalue;
  }
  public void setSubmitButtonValue(String submitbuttonvalue) {
    this.submitbuttonvalue = submitbuttonvalue;
  }
  public String getSubmitButtonValue() {
    return submitbuttonvalue;
  }
  public void setSenderName(String senderName) {
    this.senderName = senderName;
  }
  public String getSenderName() {
    return senderName;
  }
  public void setReceiverName(String receiverName) {
    this.receiverName = receiverName;
  }
  public String getReceiverName() {
    return receiverName;
  }
  public void setTypedContent(String typedContent) {
    this.typedContent = typedContent;
  }
  public String getTypedContent() {
    return typedContent;
  }
  public void setSenderEmail(String senderEmail) {
    this.senderEmail = senderEmail;
  }
  public String getSenderEmail() {
    return senderEmail;
  }
  public void setSearchLink(String searchLink) {
    this.searchLink = searchLink;
  }
  public String getSearchLink() {
    return searchLink;
  }
}
