package WUI.review.bean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class UniBrowseBean  {

  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String collegeId = "";
  private String nameStartswith = "";
  private String reviewCount = "";
  private String seoURLString = "";
  private String collegeLocation = "";
  /* ADVERT REALATED DETAILS*/
  private String subOrderItemId = "";
  private String subOrderWebsite = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";
  private String subOrderProspectusTargetURL = "";
  private String profileType = "";
  private String myhcProfileId = "";
  private String cpeQualificationNetworkId = "";
  private String mediaId = "";
  private String mediaType = "";
  private String mediaPath = "";
  private String mediaThumbPath = "";
  private String searchText = null;
  private String pageNo = null;
  private String orderBy = null;
  private String collegeLogo = null;
  private String startFrom = null;
  private String totalRecordCount = null;
  private String resultOrder = null;
  private String networkId = null;
  private String overallRating = null;
  private String orderItemId = null;
  private String qlFlag = null;
  private String hotline = null;
  private String advertName = null;
  private String mediaTypeId = null;
  private String collegeNameUrl = null;
  private String videoImagePath = null;
  private String imageThumbPath = null;
  //
  private String actualRating = null; //25-March-2014_REL
  //
  private String positionId = null;
  private String awardImage = null;
  private String profileId = null;
  private String seoButtonLabel = null;
  private ArrayList spList = null;
  private ArrayList awardList = null;
  private String viewAllCoursesFlag = null;
  private String questionTitle = null;
  private String collegeOverview = null;
  private String advertShortDesc = null;
  private String imagePath = null;
  private String profileURL = null;
  //Added by Indumathi for redesign - wu_546
  private String regionName = null;
  private String currentRank = null;
  private String totalRank = null;
  private String reviewURL = null;

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setNameStartswith(String nameStartswith) {
    this.nameStartswith = nameStartswith;
  }

  public String getNameStartswith() {
    return nameStartswith;
  }

  public void setReviewCount(String reviewCount) {
    this.reviewCount = reviewCount;
  }

  public String getReviewCount() {
    return reviewCount;
  }

  public void setSeoURLString(String seoURLString) {
    this.seoURLString = seoURLString;
  }

  public String getSeoURLString() {
    return seoURLString;
  }

  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }

  public String getCollegeLocation() {
    return collegeLocation;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setSubOrderProspectusTargetURL(String subOrderProspectusTargetURL) {
    this.subOrderProspectusTargetURL = subOrderProspectusTargetURL;
  }

  public String getSubOrderProspectusTargetURL() {
    return subOrderProspectusTargetURL;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }

  public String getMediaType() {
    return mediaType;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setMediaThumbPath(String mediaThumbPath) {
    this.mediaThumbPath = mediaThumbPath;
  }

  public String getMediaThumbPath() {
    return mediaThumbPath;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setSearchText(String searchText) {
    this.searchText = searchText;
  }

  public String getSearchText() {
    return searchText;
  }

  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }

  public String getPageNo() {
    return pageNo;
  }

  public void setOrderBy(String orderBy) {
    this.orderBy = orderBy;
  }

  public String getOrderBy() {
    return orderBy;
  }

  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }

  public String getCollegeLogo() {
    return collegeLogo;
  }

  public void setStartFrom(String startFrom) {
    this.startFrom = startFrom;
  }

  public String getStartFrom() {
    return startFrom;
  }

  public void setTotalRecordCount(String totalRecordCount) {
    this.totalRecordCount = totalRecordCount;
  }

  public String getTotalRecordCount() {
    return totalRecordCount;
  }

  public void setResultOrder(String resultOrder) {
    this.resultOrder = resultOrder;
  }

  public String getResultOrder() {
    return resultOrder;
  }

  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }

  public String getNetworkId() {
    return networkId;
  }

  public void setOverallRating(String overallRating) {
    this.overallRating = overallRating;
  }

  public String getOverallRating() {
    return overallRating;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setQlFlag(String qlFlag) {
    this.qlFlag = qlFlag;
  }

  public String getQlFlag() {
    return qlFlag;
  }

  public void setHotline(String hotline) {
    this.hotline = hotline;
  }

  public String getHotline() {
    return hotline;
  }

  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }

  public String getAdvertName() {
    return advertName;
  }

  public void setMediaTypeId(String mediaTypeId) {
    this.mediaTypeId = mediaTypeId;
  }

  public String getMediaTypeId() {
    return mediaTypeId;
  }

  public void setCollegeNameUrl(String collegeNameUrl) {
    this.collegeNameUrl = collegeNameUrl;
  }

  public String getCollegeNameUrl() {
    return collegeNameUrl;
  }

  public void setVideoImagePath(String videoImagePath) {
    this.videoImagePath = videoImagePath;
  }

  public String getVideoImagePath() {
    return videoImagePath;
  }

  public void setImageThumbPath(String imageThumbPath) {
    this.imageThumbPath = imageThumbPath;
  }

  public String getImageThumbPath() {
    return imageThumbPath;
  }

  public void setActualRating(String actualRating) {
    this.actualRating = actualRating;
  }

  public String getActualRating() {
    return actualRating;
  }

  public void setPositionId(String positionId) {
    this.positionId = positionId;
  }

  public String getPositionId() {
    return positionId;
  }

  public void setAwardImage(String awardImage) {
    this.awardImage = awardImage;
  }

  public String getAwardImage() {
    return awardImage;
  }

  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }

  public String getProfileId() {
    return profileId;
  }

  public void setSeoButtonLabel(String seoButtonLabel) {
    this.seoButtonLabel = seoButtonLabel;
  }

  public String getSeoButtonLabel() {
    return seoButtonLabel;
  }

  public void setSpList(ArrayList spList) {
    this.spList = spList;
  }

  public ArrayList getSpList() {
    return spList;
  }

  public void setViewAllCoursesFlag(String viewAllCoursesFlag) {
    this.viewAllCoursesFlag = viewAllCoursesFlag;
  }

  public String getViewAllCoursesFlag() {
    return viewAllCoursesFlag;
  }

  public void setAwardList(ArrayList awardList) {
    this.awardList = awardList;
  }

  public ArrayList getAwardList() {
    return awardList;
  }

  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }

  public String getQuestionTitle() {
    return questionTitle;
  }

  public void setCollegeOverview(String collegeOverview) {
    this.collegeOverview = collegeOverview;
  }

  public String getCollegeOverview() {
    return collegeOverview;
  }

  public void setAdvertShortDesc(String advertShortDesc) {
    this.advertShortDesc = advertShortDesc;
  }

  public String getAdvertShortDesc() {
    return advertShortDesc;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setProfileURL(String profileURL) {
    this.profileURL = profileURL;
  }

  public String getProfileURL() {
    return profileURL;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }

  public String getRegionName() {
    return regionName;
  }

  public void setCurrentRank(String currentRank) {
    this.currentRank = currentRank;
  }

  public String getCurrentRank() {
    return currentRank;
  }

  public void setTotalRank(String totalRank) {
    this.totalRank = totalRank;
  }

  public String getTotalRank() {
    return totalRank;
  }

  public void setReviewURL(String reviewURL) {
    this.reviewURL = reviewURL;
  }

  public String getReviewURL() {
    return reviewURL;
  }

}
