package WUI.review.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.review.bean.AddReviewBean;
import WUI.utilities.CommonFunction;

@Controller
public class AddReviewWidgetController {
	 @RequestMapping(value = "/university-course-reviews/add-review"  , method = {RequestMethod.GET , RequestMethod.POST})
	  public ModelAndView addReviewWidgetController(ModelMap model, HttpServletRequest request, HttpServletResponse response, AddReviewBean addReviewBean , HttpSession session) throws Exception {
		    //  
		    String reviewId = null;
		    if (new CommonFunction().checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED //
		    	return new  ModelAndView("forward: /userLogin.html");
		    }
		    if (request.getAttribute("sessionclosed") != null && String.valueOf(request.getAttribute("sessionclosed")).trim().length() > 0) {
		      session.setAttribute("message", "sessionexpiry");
		      return new  ModelAndView("forward: /userLogin.html");
		    }
		    //Insight start
		    request.setAttribute("insightPageFlag", "yes");
		    //Insight end
		    request.setAttribute("curActiveMenu", "reviews");
		    return new  ModelAndView("addwidgetreview");
		  }
		}


