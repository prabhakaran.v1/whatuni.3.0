package WUI.whatunigo.bean;
import java.util.ArrayList;
import com.wuni.valueobjects.whatunigo.GradeFilterValidationVO;

public class GradeFilterBean {
   private ArrayList<GradeFilterValidationVO> qualSubList;
   private String action;
   private String userId;
   private String sessionId;
   private Object[][] qualDetailsArr = null;
   private String subjectName;
   private String keyword;
   private String gradePoints;
   
   private String collegeName;
   
   private String qualificationCode;
   private String subQualId;
   
   private String location;
   
   private String userUcasScore;
   
   private String pageName = null;  
   private String studyMode = null;
   private String postcode = null; 
   private String univLocTypeName = null;
   private String campusTypeName = null;
   private String russelGroupName = null;  
   private String empRate = null;
   private String moduleStr = null;
   private String ucasCode = null;
   private String ucasTariff = null;
   private String jacsCode = null;
   private String assessmentType = null;
   private String reviewSubjects = null;
   private String searchRange = null;
   
   public ArrayList<GradeFilterValidationVO> getQualSubList() {
      return qualSubList;
   }
   
   public void setQualSubList(ArrayList<GradeFilterValidationVO> qualSubList) {
      this.qualSubList = qualSubList;
   }
   
   public String getAction() {
      return action;
   }
   
   public void setAction(String action) {
      this.action = action;
   }
   
   public String getUserId() {
      return userId;
   }
   
   public void setUserId(String userId) {
      this.userId = userId;
   }
   
   public String getSessionId() {
      return sessionId;
   }
   
   public void setSessionId(String sessionId) {
      this.sessionId = sessionId;
   }
   
   public Object[][] getQualDetailsArr() {
      return qualDetailsArr;
   }
   
   public void setQualDetailsArr(Object[][] qualDetailsArr) {
      this.qualDetailsArr = qualDetailsArr;
   }
   
   public String getSubjectName() {
      return subjectName;
   }
   
   public void setSubjectName(String subjectName) {
      this.subjectName = subjectName;
   }
   
   public String getKeyword() {
      return keyword;
   }
   
   public void setKeyword(String keyword) {
      this.keyword = keyword;
   }
   
   public String getGradePoints() {
      return gradePoints;
   }
   
   public void setGradePoints(String gradePoints) {
      this.gradePoints = gradePoints;
   }

   
   public String getCollegeName() {
      return collegeName;
   }

   
   public void setCollegeName(String collegeName) {
      this.collegeName = collegeName;
   }

   
   public String getQualificationCode() {
      return qualificationCode;
   }

   
   public void setQualificationCode(String qualificationCode) {
      this.qualificationCode = qualificationCode;
   }

   
   public String getSubQualId() {
      return subQualId;
   }

   
   public void setSubQualId(String subQualId) {
      this.subQualId = subQualId;
   }

   
   public String getLocation() {
      return location;
   }

   
   public void setLocation(String location) {
      this.location = location;
   }

   
   public String getUserUcasScore() {
      return userUcasScore;
   }

   
   public void setUserUcasScore(String userUcasScore) {
      this.userUcasScore = userUcasScore;
   }

  public String getPageName() {
	return pageName;
  }

  public void setPageName(String pageName) {
	this.pageName = pageName;
  }

  public String getStudyMode() {
	return studyMode;
  }

  public void setStudyMode(String studyMode) {
	this.studyMode = studyMode;
  }

  public String getPostcode() {
	return postcode;
  }

  public void setPostcode(String postcode) {
	this.postcode = postcode;
  }

  public String getUnivLocTypeName() {
	return univLocTypeName;
  }

  public void setUnivLocTypeName(String univLocTypeName) {
	this.univLocTypeName = univLocTypeName;
  }

  public String getCampusTypeName() {
	return campusTypeName;
  }

  public void setCampusTypeName(String campusTypeName) {
	this.campusTypeName = campusTypeName;
  }

  public String getRusselGroupName() {
	return russelGroupName;
  }

  public void setRusselGroupName(String russelGroupName) {
	this.russelGroupName = russelGroupName;
  }

  public String getEmpRate() {
	return empRate;
  }

  public void setEmpRate(String empRate) {
	this.empRate = empRate;
  }

  public String getModuleStr() {
	return moduleStr;
  }

  public void setModuleStr(String moduleStr) {
	this.moduleStr = moduleStr;
  }

  public String getUcasCode() {
	return ucasCode;
  }

  public void setUcasCode(String ucasCode) {
	this.ucasCode = ucasCode;
  }

  public String getUcasTariff() {
	return ucasTariff;
 }

  public void setUcasTariff(String ucasTariff) {
	this.ucasTariff = ucasTariff;
  }

  public String getJacsCode() {
	return jacsCode;
  }

  public void setJacsCode(String jacsCode) {
	this.jacsCode = jacsCode;
  }

  public String getAssessmentType() {
	return assessmentType;
  }

  public void setAssessmentType(String assessmentType) {
	this.assessmentType = assessmentType;
  }

  public String getReviewSubjects() {
	return reviewSubjects;
  }

  public void setReviewSubjects(String reviewSubjects) {
	this.reviewSubjects = reviewSubjects;
  }

  public String getSearchRange() {
	return searchRange;
  }

  public void setSearchRange(String searchRange) {
	this.searchRange = searchRange;
  }
     
}
