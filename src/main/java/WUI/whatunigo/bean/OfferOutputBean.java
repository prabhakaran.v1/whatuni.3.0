package WUI.whatunigo.bean;

import java.util.ArrayList;
import com.wuni.valueobjects.whatunigo.PageInfoVO;
import com.wuni.valueobjects.whatunigo.QualificationsVO;
import com.wuni.valueobjects.whatunigo.QuestionAnswerVO;
import com.wuni.valueobjects.whatunigo.SubjectAjaxListVO;
import com.wuni.valueobjects.whatunigo.UserqualificationsVO;
import com.wuni.valueobjects.whatunigo.course.CourseInfoVO;


public class OfferOutputBean {
   private ArrayList<CourseInfoVO> course_info_list;
   private ArrayList<CourseInfoVO> alternate_offer_info_list;
   private ArrayList<CourseInfoVO> alternate_university_info_list;
   private ArrayList<QuestionAnswerVO> question_answer_list;
private ArrayList<PageInfoVO> page_info_list;
   private ArrayList<PageInfoVO> page_status_list; 
   private String intial_text;
   private String user_ucas_tariff_point;
	private String user_ucas_point_shortage;
	private String user_name;
	private String applicant_count;
   private String user_ucas_point;
   private String apply_now_success_flag;
   private String timer_status;   
   private ArrayList<PageInfoVO> review_section_list;
   private String hotline_number;
   private String static_text;
	private String success_flag;
	private ArrayList<QualificationsVO> qual_list;
   private ArrayList<UserqualificationsVO> user_qual_list;
   private ArrayList<UserqualificationsVO> user_qual_list1;
   private String timer1_status;
 private String timer2_status;
   private String places_available_flag;
   private String timer1_end_time;
 private String timer2_end_time;
   private String  qual_val_flag;
   private ArrayList<SubjectAjaxListVO> qual_subject_list;
   private String UCAS_point;
private String provisional_form_flag; 
   private String offer_form_flag;
   private String error_message;
   private String error_code;
   private String validation_error_code;
private String user_tariff_points;
private String generate_application_flag;
private String section_review_flag;
private String user_status_code;
private String article_content;
private String user_submitted_date;
private String req_sub_count;
private String deletion_message;
private String qual_confirm_flag;

public ArrayList<CourseInfoVO> getCourse_info_list() {
   return course_info_list;
}

public void setCourse_info_list(ArrayList<CourseInfoVO> course_info_list) {
   this.course_info_list = course_info_list;
}

public ArrayList<CourseInfoVO> getAlternate_offer_info_list() {
   return alternate_offer_info_list;
}

public void setAlternate_offer_info_list(ArrayList<CourseInfoVO> alternate_offer_info_list) {
   this.alternate_offer_info_list = alternate_offer_info_list;
}

public ArrayList<CourseInfoVO> getAlternate_university_info_list() {
   return alternate_university_info_list;
}

public void setAlternate_university_info_list(ArrayList<CourseInfoVO> alternate_university_info_list) {
   this.alternate_university_info_list = alternate_university_info_list;
}

public ArrayList<QuestionAnswerVO> getQuestion_answer_list() {
   return question_answer_list;
}

public void setQuestion_answer_list(ArrayList<QuestionAnswerVO> question_answer_list) {
   this.question_answer_list = question_answer_list;
}

public ArrayList<PageInfoVO> getPage_info_list() {
   return page_info_list;
}

public void setPage_info_list(ArrayList<PageInfoVO> page_info_list) {
   this.page_info_list = page_info_list;
}

public ArrayList<PageInfoVO> getPage_status_list() {
   return page_status_list;
}

public void setPage_status_list(ArrayList<PageInfoVO> page_status_list) {
   this.page_status_list = page_status_list;
}

public String getIntial_text() {
   return intial_text;
}

public void setIntial_text(String intial_text) {
   this.intial_text = intial_text;
}

public String getUser_ucas_tariff_point() {
   return user_ucas_tariff_point;
}

public void setUser_ucas_tariff_point(String user_ucas_tariff_point) {
   this.user_ucas_tariff_point = user_ucas_tariff_point;
}

public String getUser_ucas_point_shortage() {
   return user_ucas_point_shortage;
}

public void setUser_ucas_point_shortage(String user_ucas_point_shortage) {
   this.user_ucas_point_shortage = user_ucas_point_shortage;
}

public String getUser_name() {
   return user_name;
}

public void setUser_name(String user_name) {
   this.user_name = user_name;
}

public String getApplicant_count() {
   return applicant_count;
}

public void setApplicant_count(String applicant_count) {
   this.applicant_count = applicant_count;
}

public String getUser_ucas_point() {
   return user_ucas_point;
}

public void setUser_ucas_point(String user_ucas_point) {
   this.user_ucas_point = user_ucas_point;
}

public String getApply_now_success_flag() {
   return apply_now_success_flag;
}

public void setApply_now_success_flag(String apply_now_success_flag) {
   this.apply_now_success_flag = apply_now_success_flag;
}

public String getTimer_status() {
   return timer_status;
}

public void setTimer_status(String timer_status) {
   this.timer_status = timer_status;
}

public ArrayList<PageInfoVO> getReview_section_list() {
   return review_section_list;
}

public void setReview_section_list(ArrayList<PageInfoVO> review_section_list) {
   this.review_section_list = review_section_list;
}

public String getHotline_number() {
   return hotline_number;
}

public void setHotline_number(String hotline_number) {
   this.hotline_number = hotline_number;
}

public String getStatic_text() {
   return static_text;
}

public void setStatic_text(String static_text) {
   this.static_text = static_text;
}

public String getSuccess_flag() {
   return success_flag;
}

public void setSuccess_flag(String success_flag) {
   this.success_flag = success_flag;
}

public ArrayList<QualificationsVO> getQual_list() {
   return qual_list;
}

public void setQual_list(ArrayList<QualificationsVO> qual_list) {
   this.qual_list = qual_list;
}

public ArrayList<UserqualificationsVO> getUser_qual_list() {
   return user_qual_list;
}

public void setUser_qual_list(ArrayList<UserqualificationsVO> user_qual_list) {
   this.user_qual_list = user_qual_list;
}

public ArrayList<UserqualificationsVO> getUser_qual_list1() {
   return user_qual_list1;
}

public void setUser_qual_list1(ArrayList<UserqualificationsVO> user_qual_list1) {
   this.user_qual_list1 = user_qual_list1;
}

public String getTimer1_status() {
   return timer1_status;
}

public void setTimer1_status(String timer1_status) {
   this.timer1_status = timer1_status;
}

public String getTimer2_status() {
   return timer2_status;
}

public void setTimer2_status(String timer2_status) {
   this.timer2_status = timer2_status;
}

public String getPlaces_available_flag() {
   return places_available_flag;
}

public void setPlaces_available_flag(String places_available_flag) {
   this.places_available_flag = places_available_flag;
}

public String getTimer1_end_time() {
   return timer1_end_time;
}

public void setTimer1_end_time(String timer1_end_time) {
   this.timer1_end_time = timer1_end_time;
}

public String getTimer2_end_time() {
   return timer2_end_time;
}

public void setTimer2_end_time(String timer2_end_time) {
   this.timer2_end_time = timer2_end_time;
}

public String getQual_val_flag() {
   return qual_val_flag;
}

public void setQual_val_flag(String qual_val_flag) {
   this.qual_val_flag = qual_val_flag;
}

public ArrayList<SubjectAjaxListVO> getQual_subject_list() {
   return qual_subject_list;
}

public void setQual_subject_list(ArrayList<SubjectAjaxListVO> qual_subject_list) {
   this.qual_subject_list = qual_subject_list;
}

public String getUCAS_point() {
   return UCAS_point;
}

public void setUCAS_point(String uCAS_point) {
   UCAS_point = uCAS_point;
}

public String getProvisional_form_flag() {
   return provisional_form_flag;
}

public void setProvisional_form_flag(String provisional_form_flag) {
   this.provisional_form_flag = provisional_form_flag;
}

public String getOffer_form_flag() {
   return offer_form_flag;
}

public void setOffer_form_flag(String offer_form_flag) {
   this.offer_form_flag = offer_form_flag;
}

public String getError_message() {
   return error_message;
}

public void setError_message(String error_message) {
   this.error_message = error_message;
}

public String getError_code() {
   return error_code;
}

public void setError_code(String error_code) {
   this.error_code = error_code;
}

public String getValidation_error_code() {
   return validation_error_code;
}

public void setValidation_error_code(String validation_error_code) {
   this.validation_error_code = validation_error_code;
}

public String getUser_tariff_points() {
   return user_tariff_points;
}

public void setUser_tariff_points(String user_tariff_points) {
   this.user_tariff_points = user_tariff_points;
}

public String getGenerate_application_flag() {
   return generate_application_flag;
}

public void setGenerate_application_flag(String generate_application_flag) {
   this.generate_application_flag = generate_application_flag;
}

public String getSection_review_flag() {
   return section_review_flag;
}

public void setSection_review_flag(String section_review_flag) {
   this.section_review_flag = section_review_flag;
}

public String getUser_status_code() {
   return user_status_code;
}

public void setUser_status_code(String user_status_code) {
   this.user_status_code = user_status_code;
}

public String getArticle_content() {
   return article_content;
}

public void setArticle_content(String article_content) {
   this.article_content = article_content;
}

public String getUser_submitted_date() {
   return user_submitted_date;
}

public void setUser_submitted_date(String user_submitted_date) {
   this.user_submitted_date = user_submitted_date;
}

public String getReq_sub_count() {
   return req_sub_count;
}

public void setReq_sub_count(String req_sub_count) {
   this.req_sub_count = req_sub_count;
}

public String getDeletion_message() {
   return deletion_message;
}

public void setDeletion_message(String deletion_message) {
   this.deletion_message = deletion_message;
}

public String getQual_confirm_flag() {
   return qual_confirm_flag;
}

public void setQual_confirm_flag(String qual_confirm_flag) {
   this.qual_confirm_flag = qual_confirm_flag;
}
}
