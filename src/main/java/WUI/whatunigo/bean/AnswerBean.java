package WUI.whatunigo.bean;


public class AnswerBean {
   
   private String questionId;
   private String answer;
   private String optionId;
   
   public String getQuestionId() {
      return questionId;
   }
   
   public void setQuestionId(String questionId) {
      this.questionId = questionId;
   }
   
   public String getAnswer() {
      return answer;
   }
   
   public void setAnswer(String answer) {
      this.answer = answer;
   }
   
   public String getOptionId() {
      return optionId;
   }
   
   public void setOptionId(String optionId) {
      this.optionId = optionId;
   }
   
}
