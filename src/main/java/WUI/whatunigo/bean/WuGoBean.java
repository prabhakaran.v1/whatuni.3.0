package WUI.whatunigo.bean;

import java.util.ArrayList;
import com.wuni.valueobjects.whatunigo.stats.StatsLoggingVO;


public class WuGoBean {
   private String userId;
   private String affiliateId;
   private String accessToken;
   private String pageId;
   private String opportunityId;
   private String collegeId;
   private String courseId;
   private String dbName;
   private ArrayList<AnswerBean> inputAnswerList;
   
   private String collegeLogo;
   private String courseTitle;
   private String studyMode;
   private String studyDuration;
   private String tariffPoint;
   private String intitalText;
   private String collegeNameDisp;
   private String userStatus;
   private String timer1EndTime;
   private String timer1Status;
   private String userUcasTariffPoint;  
   private String nextPageId;
   
   private Object[] questionIdArr = null;
   private Object[] answerOptionIdArr = null;
   private Object[] answervalueArr = null;
   
   private String qusArr;
   private String ansArr;
   private String optArr;
   
   private String reviewFlag;
   
   private String htmlId;
   private String action;
   
   private String qualIdArr;
   private String qualLevelArr;
   private String subIdArr;
   private String gradeArr;
   private String gradeptArr;
   private String displaySeqArr;
   

   
   private String qualSub1;
   private String qualSub2;
   private String qualSub3;
   
   //Timer one expiry related fields
   private String timerOneStatus;
   private String timerTwoStatus;
   private String offerTimerOneStatusId;
   private String forceExpiryFlag;
   //
   
   //subject list ajax
   private String freeText;
   //
   private String offerName;
   private Object[][] qualDetailsArr = null;
    
   private String sessionId;
   private String referralUrl;
   
   private ArrayList<StatsLoggingVO> statsLogList;
   private String fromAcceptingPage;
   private String fromProvpage;
   
   private String qualConfirmFlag;

   
   public String getUserId() {
      return userId;
   }

   
   public void setUserId(String userId) {
      this.userId = userId;
   }

   
   public String getAffiliateId() {
      return affiliateId;
   }

   
   public void setAffiliateId(String affiliateId) {
      this.affiliateId = affiliateId;
   }

   
   public String getAccessToken() {
      return accessToken;
   }

   
   public void setAccessToken(String accessToken) {
      this.accessToken = accessToken;
   }

   
   public String getPageId() {
      return pageId;
   }

   
   public void setPageId(String pageId) {
      this.pageId = pageId;
   }

   
   public String getOpportunityId() {
      return opportunityId;
   }

   
   public void setOpportunityId(String opportunityId) {
      this.opportunityId = opportunityId;
   }

   
   public String getCollegeId() {
      return collegeId;
   }

   
   public void setCollegeId(String collegeId) {
      this.collegeId = collegeId;
   }

   
   public String getCourseId() {
      return courseId;
   }

   
   public void setCourseId(String courseId) {
      this.courseId = courseId;
   }

   
   public String getDbName() {
      return dbName;
   }

   
   public void setDbName(String dbName) {
      this.dbName = dbName;
   }

   
   public ArrayList<AnswerBean> getInputAnswerList() {
      return inputAnswerList;
   }

   
   public void setInputAnswerList(ArrayList<AnswerBean> inputAnswerList) {
      this.inputAnswerList = inputAnswerList;
   }

   
   public String getCollegeLogo() {
      return collegeLogo;
   }

   
   public void setCollegeLogo(String collegeLogo) {
      this.collegeLogo = collegeLogo;
   }

   
   public String getCourseTitle() {
      return courseTitle;
   }

   
   public void setCourseTitle(String courseTitle) {
      this.courseTitle = courseTitle;
   }

   
   public String getStudyMode() {
      return studyMode;
   }

   
   public void setStudyMode(String studyMode) {
      this.studyMode = studyMode;
   }

   
   public String getStudyDuration() {
      return studyDuration;
   }

   
   public void setStudyDuration(String studyDuration) {
      this.studyDuration = studyDuration;
   }

   
   public String getTariffPoint() {
      return tariffPoint;
   }

   
   public void setTariffPoint(String tariffPoint) {
      this.tariffPoint = tariffPoint;
   }

   
   public String getIntitalText() {
      return intitalText;
   }

   
   public void setIntitalText(String intitalText) {
      this.intitalText = intitalText;
   }

   
   public String getCollegeNameDisp() {
      return collegeNameDisp;
   }

   
   public void setCollegeNameDisp(String collegeNameDisp) {
      this.collegeNameDisp = collegeNameDisp;
   }

   
   public String getUserStatus() {
      return userStatus;
   }

   
   public void setUserStatus(String userStatus) {
      this.userStatus = userStatus;
   }

   
   public String getTimer1EndTime() {
      return timer1EndTime;
   }

   
   public void setTimer1EndTime(String timer1EndTime) {
      this.timer1EndTime = timer1EndTime;
   }

   
   public String getTimer1Status() {
      return timer1Status;
   }

   
   public void setTimer1Status(String timer1Status) {
      this.timer1Status = timer1Status;
   }

   
   public String getUserUcasTariffPoint() {
      return userUcasTariffPoint;
   }

   
   public void setUserUcasTariffPoint(String userUcasTariffPoint) {
      this.userUcasTariffPoint = userUcasTariffPoint;
   }

   
   public String getNextPageId() {
      return nextPageId;
   }

   
   public void setNextPageId(String nextPageId) {
      this.nextPageId = nextPageId;
   }

   
   public Object[] getQuestionIdArr() {
      return questionIdArr;
   }

   
   public void setQuestionIdArr(Object[] questionIdArr) {
      this.questionIdArr = questionIdArr;
   }

   
   public Object[] getAnswerOptionIdArr() {
      return answerOptionIdArr;
   }

   
   public void setAnswerOptionIdArr(Object[] answerOptionIdArr) {
      this.answerOptionIdArr = answerOptionIdArr;
   }

   
   public Object[] getAnswervalueArr() {
      return answervalueArr;
   }

   
   public void setAnswervalueArr(Object[] answervalueArr) {
      this.answervalueArr = answervalueArr;
   }

   
   public String getQusArr() {
      return qusArr;
   }

   
   public void setQusArr(String qusArr) {
      this.qusArr = qusArr;
   }

   
   public String getAnsArr() {
      return ansArr;
   }

   
   public void setAnsArr(String ansArr) {
      this.ansArr = ansArr;
   }

   
   public String getOptArr() {
      return optArr;
   }

   
   public void setOptArr(String optArr) {
      this.optArr = optArr;
   }

   
   public String getReviewFlag() {
      return reviewFlag;
   }

   
   public void setReviewFlag(String reviewFlag) {
      this.reviewFlag = reviewFlag;
   }

   
   public String getHtmlId() {
      return htmlId;
   }

   
   public void setHtmlId(String htmlId) {
      this.htmlId = htmlId;
   }

   
   public String getAction() {
      return action;
   }

   
   public void setAction(String action) {
      this.action = action;
   }

   
   public String getQualIdArr() {
      return qualIdArr;
   }

   
   public void setQualIdArr(String qualIdArr) {
      this.qualIdArr = qualIdArr;
   }

   
   public String getQualLevelArr() {
      return qualLevelArr;
   }

   
   public void setQualLevelArr(String qualLevelArr) {
      this.qualLevelArr = qualLevelArr;
   }

   
   public String getSubIdArr() {
      return subIdArr;
   }

   
   public void setSubIdArr(String subIdArr) {
      this.subIdArr = subIdArr;
   }

   
   public String getGradeArr() {
      return gradeArr;
   }

   
   public void setGradeArr(String gradeArr) {
      this.gradeArr = gradeArr;
   }

   
   public String getGradeptArr() {
      return gradeptArr;
   }

   
   public void setGradeptArr(String gradeptArr) {
      this.gradeptArr = gradeptArr;
   }

   
   public String getDisplaySeqArr() {
      return displaySeqArr;
   }

   
   public void setDisplaySeqArr(String displaySeqArr) {
      this.displaySeqArr = displaySeqArr;
   }

   
   public String getQualSub1() {
      return qualSub1;
   }

   
   public void setQualSub1(String qualSub1) {
      this.qualSub1 = qualSub1;
   }

   
   public String getQualSub2() {
      return qualSub2;
   }

   
   public void setQualSub2(String qualSub2) {
      this.qualSub2 = qualSub2;
   }

   
   public String getQualSub3() {
      return qualSub3;
   }

   
   public void setQualSub3(String qualSub3) {
      this.qualSub3 = qualSub3;
   }

   
   public String getTimerOneStatus() {
      return timerOneStatus;
   }

   
   public void setTimerOneStatus(String timerOneStatus) {
      this.timerOneStatus = timerOneStatus;
   }

   
   public String getTimerTwoStatus() {
      return timerTwoStatus;
   }

   
   public void setTimerTwoStatus(String timerTwoStatus) {
      this.timerTwoStatus = timerTwoStatus;
   }

   
   public String getOfferTimerOneStatusId() {
      return offerTimerOneStatusId;
   }

   
   public void setOfferTimerOneStatusId(String offerTimerOneStatusId) {
      this.offerTimerOneStatusId = offerTimerOneStatusId;
   }

   
   public String getForceExpiryFlag() {
      return forceExpiryFlag;
   }

   
   public void setForceExpiryFlag(String forceExpiryFlag) {
      this.forceExpiryFlag = forceExpiryFlag;
   }

   
   public String getFreeText() {
      return freeText;
   }

   
   public void setFreeText(String freeText) {
      this.freeText = freeText;
   }

   
   public String getOfferName() {
      return offerName;
   }

   
   public void setOfferName(String offerName) {
      this.offerName = offerName;
   }

   
   public Object[][] getQualDetailsArr() {
      return qualDetailsArr;
   }

   
   public void setQualDetailsArr(Object[][] qualDetailsArr) {
      this.qualDetailsArr = qualDetailsArr;
   }

   
   public String getSessionId() {
      return sessionId;
   }

   
   public void setSessionId(String sessionId) {
      this.sessionId = sessionId;
   }

   
   public String getReferralUrl() {
      return referralUrl;
   }

   
   public void setReferralUrl(String referralUrl) {
      this.referralUrl = referralUrl;
   }

   
   public ArrayList<StatsLoggingVO> getStatsLogList() {
      return statsLogList;
   }

   
   public void setStatsLogList(ArrayList<StatsLoggingVO> statsLogList) {
      this.statsLogList = statsLogList;
   }

   
   public String getFromAcceptingPage() {
      return fromAcceptingPage;
   }

   
   public void setFromAcceptingPage(String fromAcceptingPage) {
      this.fromAcceptingPage = fromAcceptingPage;
   }

   
   public String getFromProvpage() {
      return fromProvpage;
   }

   
   public void setFromProvpage(String fromProvpage) {
      this.fromProvpage = fromProvpage;
   }

   
   public String getQualConfirmFlag() {
      return qualConfirmFlag;
   }

   
   public void setQualConfirmFlag(String qualConfirmFlag) {
      this.qualConfirmFlag = qualConfirmFlag;
   }
}
