package WUI.cpe.form;

public class CollegeCpeInteractionDetailsWithMediaBean {
  //button details - from db
  private String orderItemId = "";
  private String subOrderItemId = "";
  private String myhcProfileId = "";
  private String profileId = "";
  private String profileType = "";
  private String advertName = "";
  private String cpeQualificationNetworkId = "";
  private String subOrderEmail = "";
  private String subOrderProspectus = "";
  private String subOrderWebsite = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectusWebform = "";
  //media details - from db
  private String mediaId = "";
  private String mediaType = "";
  private String mediaPath = "";
  private String mediaPathThumb = "";
  //media details - customized according to db-media values
  private String isMediaAttached = "";
  private String imageId = "";
  private String imagePath = "";
  private String imagePathThumb = "";
  private String videoId = "";
  private String videoPathFlv = "";
  private String videoPath0001 = "";
  private String videoPath0002 = "";
  private String videoPath0003 = "";
  private String videoPath0004 = "";
  //profile count details - from db
  private String ugInstitutionProfileCount = "";
  private String pgInstitutionProfileCount = "";
  private String ugSubjectProfileCount = "";
  private String pgSubjectProfileCount = "";
  private String hotLineNo = "";

  public void clear() {
    orderItemId = null;
    subOrderItemId = null;
    myhcProfileId = null;
    profileId = null;
    profileType = null;
    advertName = null;
    cpeQualificationNetworkId = null;
    subOrderEmail = null;
    subOrderProspectus = null;
    subOrderWebsite = null;
    subOrderEmailWebform = null;
    subOrderProspectusWebform = null;
    // 
    mediaId = null;
    mediaType = null;
    mediaPath = null;
    mediaPathThumb = null;
    //
    isMediaAttached = null;
    imageId = null;
    imagePath = null;
    imagePathThumb = null;
    videoId = null;
    videoPathFlv = null;
    videoPath0001 = null;
    videoPath0002 = null;
    videoPath0003 = null;
    videoPath0004 = null;
    //
    ugInstitutionProfileCount = null;
    pgInstitutionProfileCount = null;
    ugSubjectProfileCount = null;
    pgSubjectProfileCount = null;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }

  public String getAdvertName() {
    return advertName;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }

  public String getMediaId() {
    return mediaId;
  }

  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }

  public String getMediaType() {
    return mediaType;
  }

  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }

  public String getMediaPath() {
    return mediaPath;
  }

  public void setMediaPathThumb(String mediaPathThumb) {
    this.mediaPathThumb = mediaPathThumb;
  }

  public String getMediaPathThumb() {
    return mediaPathThumb;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePathThumb(String imagePathThumb) {
    this.imagePathThumb = imagePathThumb;
  }

  public String getImagePathThumb() {
    return imagePathThumb;
  }

  public void setVideoPathFlv(String videoPathFlv) {
    this.videoPathFlv = videoPathFlv;
  }

  public String getVideoPathFlv() {
    return videoPathFlv;
  }

  public void setVideoPath0001(String videoPath0001) {
    this.videoPath0001 = videoPath0001;
  }

  public String getVideoPath0001() {
    return videoPath0001;
  }

  public void setVideoPath0002(String videoPath0002) {
    this.videoPath0002 = videoPath0002;
  }

  public String getVideoPath0002() {
    return videoPath0002;
  }

  public void setVideoPath0003(String videoPath0003) {
    this.videoPath0003 = videoPath0003;
  }

  public String getVideoPath0003() {
    return videoPath0003;
  }

  public void setVideoPath0004(String videoPath0004) {
    this.videoPath0004 = videoPath0004;
  }

  public String getVideoPath0004() {
    return videoPath0004;
  }

  public void setUgInstitutionProfileCount(String ugInstitutionProfileCount) {
    this.ugInstitutionProfileCount = ugInstitutionProfileCount;
  }

  public String getUgInstitutionProfileCount() {
    return ugInstitutionProfileCount;
  }

  public void setPgInstitutionProfileCount(String pgInstitutionProfileCount) {
    this.pgInstitutionProfileCount = pgInstitutionProfileCount;
  }

  public String getPgInstitutionProfileCount() {
    return pgInstitutionProfileCount;
  }

  public void setUgSubjectProfileCount(String ugSubjectProfileCount) {
    this.ugSubjectProfileCount = ugSubjectProfileCount;
  }

  public String getUgSubjectProfileCount() {
    return ugSubjectProfileCount;
  }

  public void setPgSubjectProfileCount(String pgSubjectProfileCount) {
    this.pgSubjectProfileCount = pgSubjectProfileCount;
  }

  public String getPgSubjectProfileCount() {
    return pgSubjectProfileCount;
  }

  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }

  public String getProfileId() {
    return profileId;
  }

  public void setIsMediaAttached(String isMediaAttached) {
    this.isMediaAttached = isMediaAttached;
  }

  public String getIsMediaAttached() {
    return isMediaAttached;
  }

  public void setImageId(String imageId) {
    this.imageId = imageId;
  }

  public String getImageId() {
    return imageId;
  }

  public void setVideoId(String videoId) {
    this.videoId = videoId;
  }

  public String getVideoId() {
    return videoId;
  }

  public void setHotLineNo(String hotLineNo) {
    this.hotLineNo = hotLineNo;
  }

  public String getHotLineNo() {
    return hotLineNo;
  }

}
