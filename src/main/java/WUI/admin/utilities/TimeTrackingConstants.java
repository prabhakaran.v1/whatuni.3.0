package WUI.admin.utilities;

public interface TimeTrackingConstants {

  //Display flags
  public final boolean TIME_TRACKING_FLAG_ON_OFF = true;                       
    public final boolean TIME_TRACKING_FLAG_TO_PRINT_IN_APP_SERVER_LOG = false; //TODO set to false while live deploy  
    public final boolean TIME_TRACKING_FLAG_TO_PRINT_IN_JSP_PAGE = true;       
    public final boolean TIME_TRACKING_FLAG_TO_PRINT_IN_EXTERNAL_FILE = false;  //TODO set to false while live deploy  
  
  public final boolean DB_CALLS_TIME_TRACKING_FLAG = false;                     
    public final boolean DB_CALLS_TIME_FLAG_TO_PRINT_IN_APP_SERVER_LOG = false; //TODO set to false while live deploy  
    public final boolean DB_CALLS_TIME_FLAG_TO_PRINT_IN_EXTERNAL_FILE = false;  //TODO set to false while live deploy  

  public final boolean DB_CALLS_PARAMETERS_TRACKING_FLAG = false;               //TODO set to false while live deploy      
    public final boolean DB_CALLS_PARAMETERS_FLAG_TO_PRINT_IN_APP_SERVER_LOG = false;  //TODO set to false while live deploy  
    public final boolean DB_CALLS_PARAMETERS_FLAG_TO_PRINT_IN_EXTERNAL_FILE = false;   //TODO set to false while live deploy  
  
  public final boolean URL_TRACKING_FLAG = false;                               //TODO set to false while live deploy
    public final boolean URL_TRACKING_FLAG_TO_PRINT_IN_APP_SERVER_LOG = false;  //TODO set to false while live deploy  
    public final boolean URL_TRACKING_FLAG_TO_PRINT_IN_EXTERNAL_FILE = false;   //TODO set to false while live deploy  
  
  public final boolean SOP_FLAG_FOR_GENERAL_PURPOSE = false;                    //TODO set to false while live deploy

}
