package WUI.admin.utilities;

import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

public class AdminUtilities {
  
  public void storeTimeAndResourceDetailsInRequestScope(HttpServletRequest request, String actionClassName, Long startActionTime){   //
    request.setAttribute("TIME_TRACKING_URL", request.getRequestURI());
    request.setAttribute("TIME_TRACKING_QUERY_STRING", request.getQueryString());
    request.setAttribute("TIME_TRACKING_ACTION_NAME", actionClassName);
    request.setAttribute("TIME_TRACKING_ACTION_START_TIME", startActionTime);
    request.setAttribute("TIME_TRACKING_ACTION_END_TIME", new Long(System.currentTimeMillis()));
        
//    System.out.println("TIME_TRACKING_URL: " + mapping.getPath());
//    System.out.println("TIME_TRACKING_QUERY_STRING: " + request.getQueryString());
//    System.out.println("TIME_TRACKING_ACTION_NAME: " + actionClassName);
//    System.out.println("TIME_TRACKING_ACTION_START_TIME: " + startActionTime);
//    System.out.println("TIME_TRACKING_ACTION_END_TIME: " + new Long(System.currentTimeMillis()));
    
  } //end of storeTimeAndResourceDetailsInRequestScope()
  
  public void logDbCallFullDetails(String dbCallerName, Vector dbCallerParameters, Long dbCallerStartTime, Long dbCallerEndTime){//used in datamodel
    logDbCallTimeDetails(dbCallerName, dbCallerParameters, dbCallerStartTime, dbCallerEndTime);
    logDbCallParameterDetails(dbCallerName, dbCallerParameters);
  }
  
  public void logDbCallTimeDetails(String dbCallerName, Vector dbCallerParameters, Long dbCallerStartTime, Long dbCallerEndTime){//used in datamodel
    if(TimeTrackingConstants.DB_CALLS_TIME_FLAG_TO_PRINT_IN_APP_SERVER_LOG){
      //System.out.println("---> TIME TAKEN FOR --> "+procedureName_in+" ("+endDbtime.longValue()+"-"+startDbtime.longValue()+") = "+(endDbtime.longValue() - startDbtime.longValue())+" ms.");// remove SOP
      //System.out.println("---> TIME TAKEN FOR --> "+procedureName_in+" = "+(endDbtime.longValue() - startDbtime.longValue())+" ms.");// remove SOP
      System.out.println("---> TIME TAKEN FOR --> "+dbCallerName+"("+dbCallerParameters.size()+") = "+(dbCallerEndTime.longValue() - dbCallerStartTime.longValue())+" ms.");
      //System.out.println("---> TIME TAKEN FOR --> "+procedureName_in+" = "+(endDbtime.longValue() - startDbtime.longValue())+" ms. "+values_in);// remove SOP
    }
    if(TimeTrackingConstants.DB_CALLS_TIME_FLAG_TO_PRINT_IN_EXTERNAL_FILE){
      //print the log deails in external file
    }
  }
  
  public void logDbCallParameterDetails(String dbCallerName, Vector dbCallerParameters){//used in datamodel
    if(TimeTrackingConstants.DB_CALLS_PARAMETERS_FLAG_TO_PRINT_IN_APP_SERVER_LOG){
     // System.out.println("---> PARAMETER DETAILS FOR --> "+dbCallerName+"("+dbCallerParameters.size()+") --> " + dbCallerParameters);
    } 
    if(TimeTrackingConstants.DB_CALLS_PARAMETERS_FLAG_TO_PRINT_IN_EXTERNAL_FILE){
      //print the log deails in external file
    }
  }
  
  public void logDbCallTimeDetails(String dbCallerName, Long dbCallerStartTime, Long dbCallerEndTime){//used in spring
    if(TimeTrackingConstants.DB_CALLS_TIME_FLAG_TO_PRINT_IN_APP_SERVER_LOG){
      System.out.println("---> TIME TAKEN FOR --> "+dbCallerName+" = "+(dbCallerEndTime.longValue() - dbCallerStartTime.longValue())+" ms.");
    }
    if(TimeTrackingConstants.DB_CALLS_TIME_FLAG_TO_PRINT_IN_EXTERNAL_FILE){
      //print the log deails in external file
    }
    
  }
  
  public void logDbCallParameterDetails(String dbCallerName, Map dbCallerParameters){//used in spring
    if(TimeTrackingConstants.DB_CALLS_PARAMETERS_FLAG_TO_PRINT_IN_APP_SERVER_LOG){
     // System.out.println("---> PARAMETER DETAILS FOR --> "+dbCallerName+"("+dbCallerParameters.size()+") --> " + dbCallerParameters);
    } 
    if(TimeTrackingConstants.DB_CALLS_PARAMETERS_FLAG_TO_PRINT_IN_EXTERNAL_FILE){
      //print the log deails in external file
    }
  }
  
  public void printUrlDetails(String actionClass, String urlString, String[] urlArray){    
    if(TimeTrackingConstants.URL_TRACKING_FLAG_TO_PRINT_IN_APP_SERVER_LOG){
      //System.out.println("--> "+actionClass+".urlString: "+urlString);// remove SOP
      for(int i=0; i<urlArray.length; i++){
         //System.out.println(actionClass+".urlArray["+i+"]: "+urlArray[i]);
       }
      //System.out.println(actionClass+".urlArray.length: "+urlArray.length);           
    }
    if(TimeTrackingConstants.URL_TRACKING_FLAG_TO_PRINT_IN_EXTERNAL_FILE){
      //print the log deails in external file
    }
  }
  
}//end of class

