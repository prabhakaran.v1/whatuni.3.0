package WUI.admin.controller;

import WUI.admin.bean.UploadVideoBean;
import com.wuni.util.sql.DataModel;
import WUI.utilities.CommonFunction;
import WUI.utilities.SessionData;
import java.sql.ResultSet;
import java.util.Vector;
import org.apache.struts.action.*;
import javax.servlet.http.*;
import org.apache.struts.upload.FormFile;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.UploadThread;

/**
 * @UploadVideoAction.java
 * @Version : 2.0
 * @June 30 2007
 * @www.whatuni.com
 * @Created By : Kalikumar and modified by Balraj. Selva Kumar
 * @Purpose  :
 * Change Log
 * *************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * *************************************************************************************************************************
 **
*/
public class UploadVideoAction {

  /**
    *   Function that get the video details for the particular college
    * @param request, collegeId
    * @return Video list as collection object
    */
  private void getCollegeVideoInformation(UploadVideoBean videoBean, String collegeId, String sectionId, String profileType) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    String limeLightPath = GlobalConstants.LIMELIGHT_VIDEO_PATH;
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID); // affiliate_id
      vector.add(collegeId); // college id
      vector.add(sectionId); // section id
      vector.add(profileType); // UNIPROFILE/SUBJECTPROFILE 
      resultset = dataModel.getResultSet("wu_admin_pkg.get_provider_video_info_fn", vector);
      while (resultset.next()) {
        // http://hotcourses.vo.llnwd.net/o18/wu_910_8910.flv     -- video file format
        videoBean.setCollegeId(collegeId);
        videoBean.setCollegeName(resultset.getString("college_name"));
        videoBean.setVideoId(resultset.getString("video_id"));
        videoBean.setVideoAssocId(resultset.getString("video_assoc_id"));
        videoBean.setVideoTitle(resultset.getString("video_title"));
        videoBean.setVideoDescription(resultset.getString("video_desc"));
        videoBean.setVideoFileName(resultset.getString("video_url"));
        videoBean.setVideoUrl(limeLightPath + resultset.getString("video_url"));
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
  }

  /**
    *   Function that get the video details for the particular college
    * @param request, collegeId
    * @return Video list as collection object
    */
  private String updateVideoInformation(String videoId, String videoTitle, String videoDesc, String videoName, String videoAssocId) {
    DataModel dataModel = new DataModel();
    String reviewId = "";
    Vector vector = new Vector();
    try {
      vector.clear();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID); // Affiliate_id
      vector.add(videoId); // Review id
      vector.add(videoAssocId); // Review id
      vector.add(videoTitle); // Review title 
      vector.add(videoDesc); // Video_Desc 
      vector.add(videoName); // Video_Name 
      reviewId = dataModel.getString("wu_admin_pkg.update_provider_video_info_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return reviewId;
  }

  /**
    *   Method used to get the limelight video  category name
    * @param categoryId
    * @return categoryName as a String
    */
  private String getProfileCategoryName(String categoryId) {
    String categoryName = "";
    Vector vector = new Vector();
    try {
      vector.clear();
      vector.add(categoryId);
      categoryName = new DataModel().executeUpdate("wu_admin_pkg.get_sub_video_folder_name_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return categoryName;
  }

  /**
    *   function to send email to the user after successful adding video in whatuni
    * @param request, videoId
    * return void
    */
  public
  //http://217.33.19.173/degrees/college-videos/oxford-university-videos/submitted-date/3757/1/student-videos.html  
  void sendProviderVideoSubmissionMail(HttpServletRequest request, String videoId, String userId, String collegeName, String collegeId) {
    String value = "";
    try {
      CommonFunction comnFn = new CommonFunction();
      String urlCollegeName = (collegeName != null && collegeName.trim().length() > 0) ? comnFn.replaceHypen(comnFn.replaceSpecialCharacter(comnFn.replaceURL(collegeName))) : "";
      String p_url_view_review = request.getContextPath() + "/college-videos/" + urlCollegeName + "/submitted-date/" + collegeId + "/1/student-videos.html";
      //String p_url_view_review   =   "videoDetail.html?x=&y=&a="+GlobalConstants.WHATUNI_AFFILATE_ID+"&w=&vrid="+videoId;
      Vector inputVector = new Vector();
      inputVector.add(GlobalConstants.WHATUNI_AFFILATE_ID); // ADDLIATE ID 
      inputVector.add(userId); // user_id
      inputVector.add(p_url_view_review); // URL Link
      value = new DataModel().executeUpdate("wu_email_pkg.provider_video_mail_fn", inputVector);
    } catch (Exception videoEmailException) {
      videoEmailException.printStackTrace();
    }
  }

  /**
      *   Function that insert the update video details to data base
      * @param videoReviewId, categoryId, fileType
      * @return string
      */
  private String updateCategoryProfile(String videoReviewId, String categoryId, String fileType) {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    String reviewAssociationId = "";
    try {
      vector.clear();
      vector.addElement(videoReviewId); // p_review_id
      vector.addElement(categoryId); // p_category_id
      vector.addElement(fileType); // p_assoc_type_code
      reviewAssociationId = dataModel.executeUpdate("wu_admin_pkg.upd_category_video_review_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return videoReviewId;
  }

  /**
    *   Method used to get the limelight video  section name
    * @param sectionId
    * @return sectionName as a String
    */
  private String getProfileSectionName(String sectionId) {
    String sectionName = "";
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    try {
      vector.clear();
      vector.add(sectionId);
      sectionName = dataModel.executeUpdate("wu_admin_pkg.get_cat_sec_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return sectionName;
  }

}
