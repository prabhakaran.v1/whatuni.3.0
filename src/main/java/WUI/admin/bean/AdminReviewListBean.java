package WUI.admin.bean;

import org.apache.struts.action.ActionForm;

public class AdminReviewListBean {

  private String reviewid = "";
  private String reviewtitle = "";
  private String collegeid = "";
  private String collegename = "";
  private String collegeNameDisplay = "";
  private String username = "";
  private String coursename = "";
  private String overallratingcomment = "";
  private String studcurrentstatus = "";
  private String nationality = "";
  private String gender = "";
  private String reviewquestion = "";
  private String overallrating = "";
  private String maxrating = "";
  private String swearingId = "";
  private String swearingDescription = "";
  private String degreeCount = "";
  private String hnchndCount = "";
  private String foundationDegreeCount = "";
  private String accessFoundationCount = "";
  private String postgraduateCount = "";
  private String othersCount = "";
  private String userImage = "";
  private String reviewType = "";
  private String emailAddress = "";
  private String submitedDate = "";
  private String reviewCount = "";

  public void setReviewId(String reviewid) {
    this.reviewid = reviewid;
  }

  public String getReviewId() {
    return reviewid;
  }

  public void setReviewTitle(String reviewtitle) {
    this.reviewtitle = reviewtitle;
  }

  public String getReviewTitle() {
    return reviewtitle;
  }

  public void setCollegeName(String collegename) {
    this.collegename = collegename;
  }

  public String getCollegeName() {
    return collegename;
  }

  public void setUserName(String username) {
    this.username = username;
  }

  public String getUserName() {
    return username;
  }

  public void setCourseName(String coursename) {
    this.coursename = coursename;
  }

  public String getCourseName() {
    return coursename;
  }

  public void setOverallRatingComment(String overallratingcomment) {
    this.overallratingcomment = overallratingcomment;
  }

  public String getOverallRatingComment() {
    return overallratingcomment;
  }

  public void setStudCurrentStatus(String studcurrentstatus) {
    this.studcurrentstatus = studcurrentstatus;
  }

  public String getStudCurrentStatus() {
    return studcurrentstatus;
  }

  public void setReviewQuestion(String reviewquestion) {
    this.reviewquestion = reviewquestion;
  }

  public String getReviewQuestion() {
    return reviewquestion;
  }

  public void setOverallRating(String overallrating) {
    this.overallrating = overallrating;
  }

  public String getOverallRating() {
    return overallrating;
  }

  public void setMaxRating(String maxrating) {
    this.maxrating = maxrating;
  }

  public String getMaxRating() {
    return maxrating;
  }

  public void setCollegeId(String collegeid) {
    this.collegeid = collegeid;
  }

  public String getCollegeId() {
    return collegeid;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getNationality() {
    return nationality;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return gender;
  }

  public void setSwearingId(String swearingId) {
    this.swearingId = swearingId;
  }

  public String getSwearingId() {
    return swearingId;
  }

  public void setSwearingDescription(String swearingDescription) {
    this.swearingDescription = swearingDescription;
  }

  public String getSwearingDescription() {
    return swearingDescription;
  }

  public void setDegreeCount(String degreeCount) {
    this.degreeCount = degreeCount;
  }

  public String getDegreeCount() {
    return degreeCount;
  }

  public void setHnchndCount(String hnchndCount) {
    this.hnchndCount = hnchndCount;
  }

  public String getHnchndCount() {
    return hnchndCount;
  }

  public void setFoundationDegreeCount(String foundationDegreeCount) {
    this.foundationDegreeCount = foundationDegreeCount;
  }

  public String getFoundationDegreeCount() {
    return foundationDegreeCount;
  }

  public void setAccessFoundationCount(String accessFoundationCount) {
    this.accessFoundationCount = accessFoundationCount;
  }

  public String getAccessFoundationCount() {
    return accessFoundationCount;
  }

  public void setPostgraduateCount(String postgraduateCount) {
    this.postgraduateCount = postgraduateCount;
  }

  public String getPostgraduateCount() {
    return postgraduateCount;
  }

  public void setOthersCount(String othersCount) {
    this.othersCount = othersCount;
  }

  public String getOthersCount() {
    return othersCount;
  }

  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }

  public String getUserImage() {
    return userImage;
  }

  public void setReviewType(String reviewType) {
    this.reviewType = reviewType;
  }

  public String getReviewType() {
    return reviewType;
  }

  public void setReviewid(String reviewid) {
    this.reviewid = reviewid;
  }

  public String getReviewid() {
    return reviewid;
  }

  public void setReviewtitle(String reviewtitle) {
    this.reviewtitle = reviewtitle;
  }

  public String getReviewtitle() {
    return reviewtitle;
  }

  public void setCollegeid(String collegeid) {
    this.collegeid = collegeid;
  }

  public String getCollegeid() {
    return collegeid;
  }

  public void setCollegename(String collegename) {
    this.collegename = collegename;
  }

  public String getCollegename() {
    return collegename;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getUsername() {
    return username;
  }

  public void setCoursename(String coursename) {
    this.coursename = coursename;
  }

  public String getCoursename() {
    return coursename;
  }

  public void setOverallratingcomment(String overallratingcomment) {
    this.overallratingcomment = overallratingcomment;
  }

  public String getOverallratingcomment() {
    return overallratingcomment;
  }

  public void setStudcurrentstatus(String studcurrentstatus) {
    this.studcurrentstatus = studcurrentstatus;
  }

  public String getStudcurrentstatus() {
    return studcurrentstatus;
  }

  public void setReviewquestion(String reviewquestion) {
    this.reviewquestion = reviewquestion;
  }

  public String getReviewquestion() {
    return reviewquestion;
  }

  public void setOverallrating(String overallrating) {
    this.overallrating = overallrating;
  }

  public String getOverallrating() {
    return overallrating;
  }

  public void setMaxrating(String maxrating) {
    this.maxrating = maxrating;
  }

  public String getMaxrating() {
    return maxrating;
  }
  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }
  public String getEmailAddress() {
    return emailAddress;
  }
  public void setSubmitedDate(String submitedDate) {
    this.submitedDate = submitedDate;
  }
  public String getSubmitedDate() {
    return submitedDate;
  }

    public void setReviewCount(String reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getReviewCount() {
        return reviewCount;
    }
}
