package WUI.admin.bean;

import org.apache.struts.action.ActionForm;

public class ExtractUniMembersBean {
  public ExtractUniMembersBean() {
  }

  String uniId = ""; 
  String uniName = "";
  String totalUserForThisUni = "";
  String userId = "";
  String userName = "";
  String foreName = "";
  String surName = "";
  String userEmail = "";
  String intentedYear = "";

  public void setUniId(String uniId) {
    this.uniId = uniId;
  }

  public String getUniId() {
    return uniId;
  }

  public void setUniName(String uniName) {
    this.uniName = uniName;
  }

  public String getUniName() {
    return uniName;
  }

  public void setTotalUserForThisUni(String totalUserForThisUni) {
    this.totalUserForThisUni = totalUserForThisUni;
  }

  public String getTotalUserForThisUni() {
    return totalUserForThisUni;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserName() {
    return userName;
  }

  public void setForeName(String foreName) {
    this.foreName = foreName;
  }

  public String getForeName() {
    return foreName;
  }

  public void setSurName(String surName) {
    this.surName = surName;
  }

  public String getSurName() {
    return surName;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }

  public String getUserEmail() {
    return userEmail;
  }

  public void setIntentedYear(String intentedYear) {
    this.intentedYear = intentedYear;
  }

  public String getIntentedYear() {
    return intentedYear;
  }

}
