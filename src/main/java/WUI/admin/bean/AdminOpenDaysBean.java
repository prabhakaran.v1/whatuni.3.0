package WUI.admin.bean;

import org.apache.struts.action.ActionForm;

/**
 * @AdminOpenDaysBean.java
 * @Version : 2.0
 * @www.whatuni.com
 * @Created By : Kalikumar and modified by Balraj. Selva Kumar
 * @Purpose  :
 */
public class AdminOpenDaysBean {

  private String openDaysId = "";
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String openDate = "";
  private String shortNotes = "";
  private String websiteAddress = "";
  private String openDayStatus = "";
  private String postCode = "";
  private String latitudeValue = "";
  private String longtitudeValue = "";
  private String searchPrefix = "";
  private String type = "";
  private String path = "";
  private String videoReviewId = "";
  private String videoThumbUrl = "";
  private String whichVideoType = ""; //IP/SP/review video

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setOpenDate(String openDate) {
    this.openDate = openDate;
  }

  public String getOpenDate() {
    return openDate;
  }

  public void setShortNotes(String shortNotes) {
    this.shortNotes = shortNotes;
  }

  public String getShortNotes() {
    return shortNotes;
  }

  public void setOpenDaysId(String openDaysId) {
    this.openDaysId = openDaysId;
  }

  public String getOpenDaysId() {
    return openDaysId;
  }

  public void setWebsiteAddress(String websiteAddress) {
    this.websiteAddress = websiteAddress;
  }

  public String getWebsiteAddress() {
    return websiteAddress;
  }

  public void setOpenDayStatus(String openDayStatus) {
    this.openDayStatus = openDayStatus;
  }

  public String getOpenDayStatus() {
    return openDayStatus;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setLatitudeValue(String latitudeValue) {
    this.latitudeValue = latitudeValue;
  }

  public String getLatitudeValue() {
    return latitudeValue;
  }

  public void setLongtitudeValue(String longtitudeValue) {
    this.longtitudeValue = longtitudeValue;
  }

  public String getLongtitudeValue() {
    return longtitudeValue;
  }

  public void setSearchPrefix(String searchPrefix) {
    this.searchPrefix = searchPrefix;
  }

  public String getSearchPrefix() {
    return searchPrefix;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getPath() {
    return path;
  }

  public void setVideoReviewId(String videoReviewId) {
    this.videoReviewId = videoReviewId;
  }

  public String getVideoReviewId() {
    return videoReviewId;
  }

  public void setVideoThumbUrl(String videoThumbUrl) {
    this.videoThumbUrl = videoThumbUrl;
  }

  public String getVideoThumbUrl() {
    return videoThumbUrl;
  }

  public void setWhichVideoType(String whichVideoType) {
    this.whichVideoType = whichVideoType;
  }

  public String getWhichVideoType() {
    return whichVideoType;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
