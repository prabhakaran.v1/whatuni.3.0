package WUI.admin.bean;

import org.apache.struts.action.ActionForm;

public class BrowseStaticCategoryBean {
  public BrowseStaticCategoryBean() {
  }
  private String browseCategoryId = "";
  private String categoryCode = "";
  private String description = "";
  private String qualification = "";
  private String qualificationTitle = "";
  private String groupData = "";
  private String processStatus = "";
  private String oldBrowseCategoryId = "";
  public void setBrowseCategoryId(String browseCategoryId) {
    this.browseCategoryId = browseCategoryId;
  }
  public String getBrowseCategoryId() {
    return browseCategoryId;
  }
  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }
  public String getCategoryCode() {
    return categoryCode;
  }
  public void setDescription(String description) {
    this.description = description;
  }
  public String getDescription() {
    return description;
  }
  public void setQualification(String qualification) {
    this.qualification = qualification;
  }
  public String getQualification() {
    return qualification;
  }
  public void setQualificationTitle(String qualificationTitle) {
    this.qualificationTitle = qualificationTitle;
  }
  public String getQualificationTitle() {
    return qualificationTitle;
  }
  public void setGroupData(String groupData) {
    this.groupData = groupData;
  }
  public String getGroupData() {
    return groupData;
  }
  public void setProcessStatus(String processStatus) {
    this.processStatus = processStatus;
  }
  public String getProcessStatus() {
    return processStatus;
  }
  public void setOldBrowseCategoryId(String oldBrowseCategoryId) {
    this.oldBrowseCategoryId = oldBrowseCategoryId;
  }
  public String getOldBrowseCategoryId() {
    return oldBrowseCategoryId;
  }
}
