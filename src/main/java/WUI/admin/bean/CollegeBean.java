package WUI.admin.bean;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class CollegeBean {

  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String courseCount = "";
  private String oldCollegeId = "";
  private String selectedCollegeId = "";
  private FormFile fileUpload = null;
  private String shtmlData = "";
  private String lineBreak = "";
  private String shtmlName = "";

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCourseCount(String courseCount) {
    this.courseCount = courseCount;
  }

  public String getCourseCount() {
    return courseCount;
  }

  public void setOldCollegeId(String oldCollegeId) {
    this.oldCollegeId = oldCollegeId;
  }

  public String getOldCollegeId() {
    return oldCollegeId;
  }

  public void setSelectedCollegeId(String selectedCollegeId) {
    this.selectedCollegeId = selectedCollegeId;
  }

  public String getSelectedCollegeId() {
    return selectedCollegeId;
  }

  public void setFileUpload(FormFile fileUpload) {
    this.fileUpload = fileUpload;
  }

  public FormFile getFileUpload() {
    return fileUpload;
  }

  public void setShtmlData(String shtmlData) {
    this.shtmlData = shtmlData;
  }

  public String getShtmlData() {
    return shtmlData;
  }

  public void setLineBreak(String lineBreak) {
    this.lineBreak = lineBreak;
  }

  public String getLineBreak() {
    return lineBreak;
  }

  public void setShtmlName(String shtmlName) {
    this.shtmlName = shtmlName;
  }

  public String getShtmlName() {
    return shtmlName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
