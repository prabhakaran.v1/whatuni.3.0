package WUI.admin.bean;

import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import WUI.utilities.CommonFunction;

public class UploadCollegeImageBean {
  private FormFile logoUpload = null;
  private String collegeId = "";
  private String liveStatus = "";
  private String collegeLogoPath = "";
  private String collegeLogoName = "";
  private String sessionId = "";
  private String userId = "";
  private String videoTitle = "";
  private String videoDesc = "";
  private String videoLogoName = "";
  private String imageLogoName = "";
  private String collegeVideoPath = "";
  private String reviewId = "";
  private String imageTitle = "";
  private String videoFileName = "";
  public void setLogoUpload(FormFile logoUpload) {
    this.logoUpload = logoUpload;
  }
  public FormFile getLogoUpload() {
    return logoUpload;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setLiveStatus(String liveStatus) {
    this.liveStatus = liveStatus;
  }
  public String getLiveStatus() {
    return liveStatus;
  }
  public void setCollegeLogoPath(String collegeLogoPath) {
    this.collegeLogoPath = collegeLogoPath;
  }
  public String getCollegeLogoPath() {
    return collegeLogoPath;
  }
/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    String collegeId = request.getParameter("z") != null? request.getParameter("z"): request.getParameter("collegeId");
    request.setAttribute("collegename", new CommonFunction().getCollegeName(collegeId, request));
    ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    if (request.getParameter("uploadType") != null && request.getParameter("uploadType").equalsIgnoreCase("image")) {
      request.setAttribute("tabname", "image");
      String contentExt = "";
      FormFile myFile = this.getLogoUpload();
      String contentType = myFile.getContentType();
      String fileName = myFile.getFileName();
      int fileSize = myFile.getFileSize();
      fileName = logoUpload.getFileName().toString();
      if (logoUpload == null || (logoUpload != null && fileName.trim().length() <= 0)) {
        errors.add("imagempty", new ActionMessage("user.uni.upload.image.empty"));
      }
      if (imageTitle == null || (imageTitle != null && imageTitle.trim().length() <= 0)) {
        errors.add("videotitleempty", new ActionMessage("admin.imagetitle.upload.error"));
      }
      if (fileName.trim().length() > 0) {
        boolean flag = false;
        if ((contentType != null) && (fileName.trim().length() > 0)) {
          StringTokenizer st = new StringTokenizer(contentType, "/");
          st.nextToken("/");
          contentExt = st.nextElement().toString();
          if (contentExt.equalsIgnoreCase("pjpeg")) {
            flag = true;
          }
          if (contentExt.equalsIgnoreCase("gif")) {
            flag = true;
          }
          if (contentExt.equalsIgnoreCase("x-png")) {
            flag = true;
          }
          if (contentExt.equalsIgnoreCase("jpeg")) {
            flag = true;
          }
          if (!flag) {
            errors.add("phototypeerror", new ActionMessage("admin.fileupload.photo.typeerror"));
          }
        }
        if (fileSize > (1024 * 1024)) {
          errors.add("photosizeerror", new ActionMessage("admin.fileupload.photo.error"));
        }
      }
      if (errors.size() > 0) {
        request.setAttribute("imageerrorpresent", "true");
      }
    }
    return errors;
  }*/
  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }
  public String getSessionId() {
    return sessionId;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setCollegeLogoName(String collegeLogoName) {
    this.collegeLogoName = collegeLogoName;
  }
  public String getCollegeLogoName() {
    return collegeLogoName;
  }
  public void setVideoTitle(String videoTitle) {
    this.videoTitle = videoTitle;
  }
  public String getVideoTitle() {
    return videoTitle;
  }
  public void setVideoDesc(String videoDesc) {
    this.videoDesc = videoDesc;
  }
  public String getVideoDesc() {
    return videoDesc;
  }
  public void setVideoLogoName(String videoLogoName) {
    this.videoLogoName = videoLogoName;
  }
  public String getVideoLogoName() {
    return videoLogoName;
  }
  public void setImageLogoName(String imageLogoName) {
    this.imageLogoName = imageLogoName;
  }
  public String getImageLogoName() {
    return imageLogoName;
  }
  public void setCollegeVideoPath(String collegeVideoPath) {
    this.collegeVideoPath = collegeVideoPath;
  }
  public String getCollegeVideoPath() {
    return collegeVideoPath;
  }
  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }
  public String getReviewId() {
    return reviewId;
  }
  public void setImageTitle(String imageTitle) {
    this.imageTitle = imageTitle;
  }
  public String getImageTitle() {
    return imageTitle;
  }
  public void setVideoFileName(String videoFileName) {
    this.videoFileName = videoFileName;
  }
  public String getVideoFileName() {
    return videoFileName;
  }
}
