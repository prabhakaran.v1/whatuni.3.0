package WUI.admin.bean;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

public class UploadVideoBean {

  private FormFile fileUpload = null;
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String videoId = "";
  private String videoTitle = "";
  private String videoDescription = "";
  private String videoFileName = "";
  private String videoUrl = "";
  private String videoAssocId = "";
  private String salesPersonUserId = "";
  private String categoryId = "";
  private String typeId = "";
  private String categoryName = "";

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setVideoTitle(String videoTitle) {
    this.videoTitle = videoTitle;
  }

  public String getVideoTitle() {
    return videoTitle;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setVideoDescription(String videoDescription) {
    this.videoDescription = videoDescription;
  }

  public String getVideoDescription() {
    return videoDescription;
  }

  public void setFileUpload(FormFile fileUpload) {
    this.fileUpload = fileUpload;
  }

  public FormFile getFileUpload() {
    return fileUpload;
  }

  public void setVideoId(String videoId) {
    this.videoId = videoId;
  }

  public String getVideoId() {
    return videoId;
  }

  public void setVideoUrl(String videoUrl) {
    this.videoUrl = videoUrl;
  }

  public String getVideoUrl() {
    return videoUrl;
  }

  public void setVideoAssocId(String videoAssocId) {
    this.videoAssocId = videoAssocId;
  }

  public String getVideoAssocId() {
    return videoAssocId;
  }

  public void setVideoFileName(String videoFileName) {
    this.videoFileName = videoFileName;
  }

  public String getVideoFileName() {
    return videoFileName;
  }

/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    errors.clear();
    ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    String validVideoContent = bundle.getString("wnui.limelight.validcontenttypes");
    if (request.getParameter("butvalue") != null && request.getParameter("butvalue").equals("Submit review")) {
      if (fileUpload != null && fileUpload.getFileName() != null && fileUpload.getFileName().trim().length() == 0) {
        if (videoFileName != null && videoFileName.trim().length() == 0) {
          errors.add("blank_upload_videourl", new ActionMessage("wnui.error.blank_upload_videourl"));
        }
      }
      if (videoTitle != null && videoTitle.trim().length() == 0) {
        errors.add("invalid_videotitle", new ActionMessage("wnui.error.invalid_videotitle"));
      }
      if (videoDescription != null && videoDescription.trim().length() == 0) {
        errors.add("invalid_videodesc", new ActionMessage("wnui.error.invalid_videodesc"));
      }
      if (errors.size() > 0) {
        return errors;
      }
      if (fileUpload != null && fileUpload.getFileName() != null && fileUpload.getFileName().trim().length() > 0) {
        if (fileUpload != null && fileUpload.getFileSize() == 0) {
          errors.add("blank_upload_videourl", new ActionMessage("wnui.error.blank_upload_videourl"));
          if (errors.size() > 0) {
            return errors;
          }
        }
        if (fileUpload != null && fileUpload.getFileSize() >= 52428800) {
          errors.add("large_file_size", new ActionMessage("wnui.limelight.largefileerror"));
          if (errors.size() > 0) {
            return errors;
          }
        }
        if (fileUpload != null && fileUpload.getContentType() != null && validVideoContent != null && validVideoContent.trim().length() > 0) {
          boolean errorFlag = true;
          String format[] = validVideoContent.split("##");
          if (format != null) {
            for (int i = 0; i < format.length; i++) {
              if (format[i] != null && format[i].trim().equals(fileUpload.getContentType())) {
                errorFlag = false;
              }
            }
            if (errorFlag) {
              errors.add("invalid_file_name", new ActionMessage("wnui.limelight.invalidcontenterror"));
            }
          }
        }
      }
    }
    return errors;
  }*/

  public void setSalesPersonUserId(String salesPersonUserId) {
    this.salesPersonUserId = salesPersonUserId;
  }

  public String getSalesPersonUserId() {
    return salesPersonUserId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setTypeId(String typeId) {
    this.typeId = typeId;
  }

  public String getTypeId() {
    return typeId;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public String getCategoryName() {
    return categoryName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
