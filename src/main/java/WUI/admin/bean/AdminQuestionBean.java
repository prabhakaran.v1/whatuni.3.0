package WUI.admin.bean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class AdminQuestionBean {
  public AdminQuestionBean() {
  }
  private ArrayList optionValue = null;
  private String questionID = "";
  private String questionTitle = "";
  private String questiongroupid = "";
  private String answerSelected = "";
  private String reviewComment = "";
  private String divDisplayMessage = "";
  private String questionNumber = "";
  private String groupOptionId = "";
  private String groupId = "";
  private String option_Value = "";
  private String optionDesc = "";
  private String optionSequence = "";
  public void setQuestionID(String questionID) {
    this.questionID = questionID;
  }
  public String getQuestionID() {
    return questionID;
  }
  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }
  public String getQuestionTitle() {
    return this.questionTitle;
  }
  public ArrayList getOptionValue() {
    return optionValue;
  }
  public void setOptionValue(ArrayList optionValue) {
    this.optionValue = optionValue;
  }
  public void setAnswerSelected(String answerSelected) {
    this.answerSelected = answerSelected;
  }
  public String getAnswerSelected() {
    return answerSelected;
  }
  public void setReviewComment(String reviewComment) {
    this.reviewComment = reviewComment;
  }
  public String getReviewComment() {
    return reviewComment;
  }
  public void setDivDisplayMessage(String divDisplayMessage) {
    this.divDisplayMessage = divDisplayMessage;
  }
  public String getDivDisplayMessage() {
    return divDisplayMessage;
  }
  public void setQuestionNumber(String questionNumber) {
    this.questionNumber = questionNumber;
  }
  public String getQuestionNumber() {
    return questionNumber;
  }
  public void setGroupOptionId(String groupOptionId) {
    this.groupOptionId = groupOptionId;
  }
  public String getGroupOptionId() {
    return groupOptionId;
  }
  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }
  public String getGroupId() {
    return groupId;
  }
  public void setOption_Value(String option_Value) {
    this.option_Value = option_Value;
  }
  public String getOption_Value() {
    return option_Value;
  }
  public void setOptionDesc(String optionDesc) {
    this.optionDesc = optionDesc;
  }
  public String getOptionDesc() {
    return optionDesc;
  }
  public void setOptionSequence(String optionSequence) {
    this.optionSequence = optionSequence;
  }
  public String getOptionSequence() {
    return optionSequence;
  }
  public void setQuestionGroupId(String questiongroupid) {
    this.questiongroupid = questiongroupid;
  }
  public String getQuestionGroupId() {
    return questiongroupid;
  }
}