package WUI.admin.bean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import WUI.utilities.CommonFunction;

public class AdminReviewBean {

  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String collegeId = "";
  private String currently = "";
  private String currentlyText = "";
  private String coursetitle = "";
  private String courseID = "";
  private String currently_key = "";
  private String currently_value = "";
  private String reviewTitle = "";
  private String sendFriendEmail = "";
  private String termsAndCondition = "";
  private String butvalue = "";
  private String cId = "";
  private String courseId = "";
  private String recommentuni = "";
  private String optionId = "";
  private String optionText = "";
  private String optionSeqId = "";
  private String categoryCode = "";
  private String keyword = "";

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCurrently(String currently) {
    this.currently = currently;
  }

  public String getCurrently() {
    return currently;
  }

  public void setCourseID(String courseID) {
    this.courseID = courseID;
  }

  public String getCourseID() {
    return courseID;
  }

  public void setCurrently_Key(String currently_key) {
    this.currently_key = currently_key;
  }

  public String getCurrently_Key() {
    return currently_key;
  }

  public void setCurrently_Value(String currently_value) {
    this.currently_value = currently_value;
  }

  public String getCurrently_Value() {
    return currently_value;
  }

  public void setCurrentlyText(String currentlyText) {
    this.currentlyText = currentlyText;
  }

  public String getCurrentlyText() {
    return currentlyText;
  }

  public void setSendFriendEmail(String sendFriendEmail) {
    this.sendFriendEmail = sendFriendEmail;
  }

  public String getSendFriendEmail() {
    return sendFriendEmail;
  }

  public void setReviewTitle(String reviewTitle) {
    this.reviewTitle = reviewTitle;
  }

  public String getReviewTitle() {
    return reviewTitle;
  }

  public void setTermsAndCondition(String termsAndCondition) {
    this.termsAndCondition = termsAndCondition;
  }

  public String getTermsAndCondition() {
    return termsAndCondition;
  }

  public void setButvalue(String butvalue) {
    this.butvalue = butvalue;
  }

  public String getButvalue() {
    return butvalue;
  }

  public void setCId(String cId) {
    this.cId = cId;
  }

  public String getCId() {
    return cId;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCoursetitle(String coursetitle) {
    this.coursetitle = coursetitle;
  }

  public String getCoursetitle() {
    return coursetitle;
  }

  public void setRecommentUni(String recommentuni) {
    this.recommentuni = recommentuni;
  }

  public String getRecommentUni() {
    return recommentuni;
  }

/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    errors.clear();
    HttpSession session = request.getSession(false);
    String errormessages[] = new String[10];
    ResourceBundle resourcebundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    errormessages[0] = resourcebundle.getString("wuni.error.over_all_rating");
    errormessages[1] = resourcebundle.getString("wuni.error.student_union");
    errormessages[2] = resourcebundle.getString("wuni.error.clubs_societies");
    errormessages[3] = resourcebundle.getString("wuni.error.accommodation");
    errormessages[4] = resourcebundle.getString("wuni.error.uni_facilities");
    errormessages[5] = resourcebundle.getString("wuni.error.lecturers");
    errormessages[6] = resourcebundle.getString("wuni.error.eyecandy");
    errormessages[7] = resourcebundle.getString("wuni.error.citylife");
    errormessages[8] = resourcebundle.getString("wuni.error.jobprospects");
    errormessages[9] = resourcebundle.getString("wuni.error.gettingplace");
    String over_rating_comment = resourcebundle.getString("wuni.error.manidatory");
    if (request.getParameter("selectid") != null) {
      session.setAttribute("selectid", request.getParameter("selectid"));
    }
    if (request.getParameter("collegeName") != null) {
      session.setAttribute("collegeName", request.getParameter("collegeName"));
    }
    if (request.getParameter("coursetitle") != null) {
      session.setAttribute("coursetitle", request.getParameter("coursetitle"));
    }
    if (request.getParameter("courseid") != null) {
      session.setAttribute("courseid", request.getParameter("courseid"));
    }
    boolean capsError = false;
    boolean swearingsError = false;
    if (request.getParameter("butvalue") != null && request.getParameter("butvalue").equals("Preview your review")) {
      List errorList = new ArrayList();
      List questionList = (List)session.getAttribute("Questionlist");
      Iterator questionIterator = questionList.iterator();
      int arrayCount = 0;
      while (questionIterator.hasNext()) {
        AdminQuestionBean qbean = (AdminQuestionBean)questionIterator.next();
        if (qbean.getQuestionGroupId() != null && qbean.getQuestionGroupId().equals("5")) {
          String reviewcomment = request.getParameter(qbean.getQuestionID() + "tarea");
          reviewcomment = reviewcomment != null ? reviewcomment.trim() : reviewcomment;
          // added newly 14.03.2008     
          if (reviewcomment != null && reviewcomment.trim().length() != 0) {
            String returnError = new CommonFunction().checkCapsSwearing(request, reviewcomment);
            if (returnError != null && returnError.equals("caps")) {
              capsError = true;
            }
            if (returnError != null && returnError.equals("swearing")) {
              swearingsError = true;
            }
          }
          // added newly 14.03.2008 
           if(reviewcomment !=null)
                    {
                        reviewcomment = reviewcomment.replaceAll("\n","<br />");
                    }
          qbean.setReviewComment(reviewcomment);
          String optionvalue = request.getParameter(qbean.getQuestionID());
          if (optionvalue != null) {
            qbean.setAnswerSelected(optionvalue);
          } else {
            errors.add("reviewqeustion", new ActionMessage("wuni.error.test"));
            errorList.add(errormessages[arrayCount]);
          }
          if (arrayCount == 0) {
            if (request.getParameter("Btarea") != null && request.getParameter("Btarea").toString().trim().length() == 0) {
              errors.add("reviewqeustion", new ActionMessage("wuni.error.test"));
              errorList.add(over_rating_comment);
            }
          }
          arrayCount++;
        }
        if (qbean.getQuestionGroupId() != null && qbean.getQuestionGroupId().equals("2")) {
          if (recommentuni != null && recommentuni.trim().length() == 0) {
            errors.add("recommuniv", new ActionMessage("wuni.error.recommuniv"));
          } else {
            qbean.setAnswerSelected(recommentuni);
          }
        }
        if (qbean.getQuestionGroupId() != null && qbean.getQuestionGroupId().equals("6")) {
          qbean.setAnswerSelected(currently);
        }
      }
      session.setAttribute("Questionlist", questionList);
      if (errorList.size() != 0) {
        session.setAttribute("errorlist", errorList);
      }
      if (reviewTitle != null && reviewTitle.trim().length() == 0) {
        errors.add("reviewtitle", new ActionMessage("wuni.error.reviewtitle"));
      } else {
        // added newly 14.03.2008      
        String returnError = new CommonFunction().checkCapsSwearing(request, reviewTitle);
        if (returnError != null && returnError.equals("caps")) {
          capsError = true;
        }
        if (returnError != null && returnError.equals("swearing")) {
          swearingsError = true;
        }
        // added newly 14.03.2008
      }
    }
    if (swearingsError) {
      errors.add("swearingError", new ActionMessage("wuni.error.swearing"));
    }
    return errors;
  }*/

  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }

  public String getOptionId() {
    return optionId;
  }

  public void setOptionText(String optionText) {
    this.optionText = optionText;
  }

  public String getOptionText() {
    return optionText;
  }

  public void setOptionSeqId(String optionSeqId) {
    this.optionSeqId = optionSeqId;
  }

  public String getOptionSeqId() {
    return optionSeqId;
  }

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setCurrently_key(String currently_key) {
    this.currently_key = currently_key;
  }

  public String getCurrently_key() {
    return currently_key;
  }

  public void setCurrently_value(String currently_value) {
    this.currently_value = currently_value;
  }

  public String getCurrently_value() {
    return currently_value;
  }

  public void setRecommentuni(String recommentuni) {
    this.recommentuni = recommentuni;
  }

  public String getRecommentuni() {
    return recommentuni;
  }

}
