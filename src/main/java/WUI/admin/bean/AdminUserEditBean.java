package WUI.admin.bean;

import org.apache.struts.action.ActionForm;

public class AdminUserEditBean  {
  public AdminUserEditBean() {
  }
  private String userName = "";
  private String email = "";
  private String password = "";
  private String userId = "";
  private String userImage = "";
  private String userLargeImage = "";
  private String firstName = "";
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  public String getFirstName() {
    return firstName;
  }
  public void setUserName(String firstName) {
    this.userName = firstName;
  }
  public String getUserName() {
    return userName;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public String getEmail() {
    return email;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public String getPassword() {
    return password;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }
  public String getUserImage() {
    return userImage;
  }
  public void setUserLargeImage(String userLargeImage) {
    this.userLargeImage = userLargeImage;
  }
  public String getUserLargeImage() {
    return userLargeImage;
  }
}
