package WUI.admin.bean;

import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

public class UploadCollegeLogoBean {
  private FormFile logoUpload = null;
  private String collegeId = "";
  private String liveStatus = "";
  private String collegeLogoPath = "";
  private String collegeLogoName = "";
  private String sessionId = "";
  private String userId = "";
  public void setLogoUpload(FormFile logoUpload) {
    this.logoUpload = logoUpload;
  }
  public FormFile getLogoUpload() {
    return logoUpload;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setLiveStatus(String liveStatus) {
    this.liveStatus = liveStatus;
  }
  public String getLiveStatus() {
    return liveStatus;
  }
  public void setCollegeLogoPath(String collegeLogoPath) {
    this.collegeLogoPath = collegeLogoPath;
  }
  public String getCollegeLogoPath() {
    return collegeLogoPath;
  }
/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    if (request.getParameter("bottontext") != null) {
      String contentExt = "";
      FormFile myFile = this.getLogoUpload();
      String contentType = myFile.getContentType();
      String fileName = myFile.getFileName();
      int fileSize = myFile.getFileSize();
      if (fileName.trim().length() > 0) {
        boolean flag = false;
        if ((contentType != null) && (fileName.trim().length() > 0)) {
          StringTokenizer st = new StringTokenizer(contentType, "/");
          st.nextToken("/");
          contentExt = st.nextElement().toString();
          if (contentExt.equalsIgnoreCase("pjpeg")) {
            flag = true;
          }
          if (contentExt.equalsIgnoreCase("gif")) {
            flag = true;
          }
          if (contentExt.equalsIgnoreCase("x-png")) {
            flag = true;
          }
          if (contentExt.equalsIgnoreCase("jpeg")) {
            flag = true;
          }
          if (!flag) {
            errors.add("phototypeerror", new ActionMessage("admin.fileupload.photo.typeerror"));
          }
        }
        if (fileSize > 6144) {
          errors.add("photosizeerror", new ActionMessage("admin.fileupload.photo.error"));
        }
      }
    }
    return errors;
  }*/
  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }
  public String getSessionId() {
    return sessionId;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setCollegeLogoName(String collegeLogoName) {
    this.collegeLogoName = collegeLogoName;
  }
  public String getCollegeLogoName() {
    return collegeLogoName;
  }
}
