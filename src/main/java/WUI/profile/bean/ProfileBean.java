package WUI.profile.bean;

import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;

public class ProfileBean {

  public ProfileBean() {
  }
  private String userId = "";
  private String imageId = "";
  private String imageName = "";
  private String imageFlag = "";
  private String groupId = "";
  private String statusFlag = "";
  private List defaultImageList = null;
  private String defaultImageId = "";
  private FormFile imagePath = null;
  private String termsandconditions = "";
  private String userPodImage = "";
  private String staticDateOfBirth = "";
  private String intendedyearOfGraduation = "";
  private String intendedyearOfGraduation_fieldId = "";
  private String yearOfGraduationFlag = "";
  private String userName = "";
  private String userName_fieldId = "";
  private String password = "";
  private String password_fieldId = "";
  private String emailId = "";
  private String emailId_fieldId = "";
  private String educationStatus = "";
  private String educationStatus_fieldId = "";
  private String gender = "";
  private String gender_fieldId = "";
  private String nationality = "";
  private String nationality_fieldId = "";
  private String dateOfBirth = "";
  private String dateOfBirth_fieldId = "";
  private String currentlyLiving = "London";
  private String currentlyLiving_fieldId = "";
  private String aboutMe = "";
  private String aboutMe_fieldId = "";
  private String receiveEmails = "";
  private String receiveEmails_fieldId = "";
  private String courseId = "";
  private String reviewDesc = "";
  private String optionId = "";
  private String optionText = "";
  private String nickName = "";
  private String nickName_fieldId = "";
  private String firstName = "";
  private String firstName_fieldId = "";
  private String surName = "";
  private String surName_fieldId = "";
  private String foreName = "";
  private String confirmEmailId = "";
  private String confirmEmailId_fieldId = "";
  private String turnOn = "";
  private String turnOn_fieldId = "";
  private String turnOff = "";
  private String turnOff_fieldId = "";
  private String myMusic = "";
  private String myMusic_fieldId = "";
  private String myMovies = "";
  private String myMovies_fieldId = "";
  private String myTvShows = "";
  private String myTvShows_fieldId = "";
  private String hero_heroines = "";
  private String hero_heroines_fieldId = "";
  private String staticEmailAddress = "";
  private String unReadMessage = "";
  private String nationalityDescription = "";
  private String sex = "";
  private String schoolName = "";
  private String schoolId = "";
  private String buttonType = "";
  private String collegeName = "";
  private String collegeNameDisplay = ""; 
  private String collegeId = "";
  private String courseName = "";
  private String reviewId = "";
  private String reviewTitle = "";
  private String requestFrom = "";
  private String userPhoto = "";
  private String userImage = "";
  private String userImageLarge = "";
  private String yearOfGraduation = "";
  private String yearOfGraduation_fieldId = "";
  private List nationalityList = null;
  private List genderList = null;
  private List educationList = null;
  private List graduationList = null;
  private List intendedGraduationList = null;
  private String town = "";
  private String schoolType = "";
  private String seoStudyLevelText = "";
  private String courseDeletedFlag = "";
  private String postCode = "";
  private String mobileNumber = "";
  private String mobFieldId = "";
  private String mychNavCnt = "";
  private String isChatbotDisbaled = "";

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserName() {
    return userName;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPassword() {
    return password;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserId() {
    return userId;
  }

  public void setRequestFrom(String requestFrom) {
    this.requestFrom = requestFrom;
  }

  public String getRequestFrom() {
    return requestFrom;
  }

  public void setEmailId(String emailId) {
    this.emailId = emailId;
  }

  public String getEmailId() {
    return emailId;
  }

  public void setUserPhoto(String userPhoto) {
    this.userPhoto = userPhoto;
  }

  public String getUserPhoto() {
    return userPhoto;
  }

  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }

  public String getUserImage() {
    return userImage;
  }

  public void setUserImageLarge(String userImageLarge) {
    this.userImageLarge = userImageLarge;
  }

  public String getUserImageLarge() {
    return userImageLarge;
  }

  public void setEducationStatus(String educationStatus) {
    this.educationStatus = educationStatus;
  }

  public String getEducationStatus() {
    return educationStatus;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return gender;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getNationality() {
    return nationality;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setCurrentlyLiving(String currentlyLiving) {
    this.currentlyLiving = currentlyLiving;
  }

  public String getCurrentlyLiving() {
    return currentlyLiving;
  }

  public void setSchoolName(String schoolName) {
    this.schoolName = schoolName;
  }

  public String getSchoolName() {
    return schoolName;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }

  public String getCourseName() {
    return courseName;
  }

  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }

  public String getReviewId() {
    return reviewId;
  }

  public void setReviewTitle(String reviewTitle) {
    this.reviewTitle = reviewTitle;
  }

  public String getReviewTitle() {
    return reviewTitle;
  }

  public void setYearOfGraduation(String yearOfGraduation) {
    this.yearOfGraduation = yearOfGraduation;
  }

  public String getYearOfGraduation() {
    return yearOfGraduation;
  }

  public void setAboutMe(String aboutMe) {
    this.aboutMe = aboutMe;
  }

  public String getAboutMe() {
    return aboutMe;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setReviewDesc(String reviewDesc) {
    this.reviewDesc = reviewDesc;
  }

  public String getReviewDesc() {
    return reviewDesc;
  }

  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }

  public String getOptionId() {
    return optionId;
  }

  public void setOptionText(String optionText) {
    this.optionText = optionText;
  }

  public String getOptionText() {
    return optionText;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public String getNickName() {
    return nickName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setSurName(String surName) {
    this.surName = surName;
  }

  public String getSurName() {
    return surName;
  }

  public void setConfirmEmailId(String confirmEmailId) {
    this.confirmEmailId = confirmEmailId;
  }

  public String getConfirmEmailId() {
    return confirmEmailId;
  }

  public void setNationalityList(List nationalityList) {
    this.nationalityList = nationalityList;
  }

  public List getNationalityList() {
    return nationalityList;
  }

  public void setGenderList(List genderList) {
    this.genderList = genderList;
  }

  public List getGenderList() {
    return genderList;
  }

  public void setEducationList(List educationList) {
    this.educationList = educationList;
  }

  public List getEducationList() {
    return educationList;
  }

  public void setGraduationList(List graduationList) {
    this.graduationList = graduationList;
  }

  public List getGraduationList() {
    return graduationList;
  }

  public void setUserName_fieldId(String userName_fieldId) {
    this.userName_fieldId = userName_fieldId;
  }

  public String getUserName_fieldId() {
    return userName_fieldId;
  }

  public void setPassword_fieldId(String password_fieldId) {
    this.password_fieldId = password_fieldId;
  }

  public String getPassword_fieldId() {
    return password_fieldId;
  }

  public void setEmailId_fieldId(String emailId_fieldId) {
    this.emailId_fieldId = emailId_fieldId;
  }

  public String getEmailId_fieldId() {
    return emailId_fieldId;
  }

  public void setEducationStatus_fieldId(String educationStatus_fieldId) {
    this.educationStatus_fieldId = educationStatus_fieldId;
  }

  public String getEducationStatus_fieldId() {
    return educationStatus_fieldId;
  }

  public void setGender_fieldId(String gender_fieldId) {
    this.gender_fieldId = gender_fieldId;
  }

  public String getGender_fieldId() {
    return gender_fieldId;
  }

  public void setNationality_fieldId(String nationality_fieldId) {
    this.nationality_fieldId = nationality_fieldId;
  }

  public String getNationality_fieldId() {
    return nationality_fieldId;
  }

  public void setDateOfBirth_fieldId(String dateOfBirth_fieldId) {
    this.dateOfBirth_fieldId = dateOfBirth_fieldId;
  }

  public String getDateOfBirth_fieldId() {
    return dateOfBirth_fieldId;
  }

  public void setCurrentlyLiving_fieldId(String currentlyLiving_fieldId) {
    this.currentlyLiving_fieldId = currentlyLiving_fieldId;
  }

  public String getCurrentlyLiving_fieldId() {
    return currentlyLiving_fieldId;
  }

  public void setYearOfGraduation_fieldId(String yearOfGraduation_fieldId) {
    this.yearOfGraduation_fieldId = yearOfGraduation_fieldId;
  }

  public String getYearOfGraduation_fieldId() {
    return yearOfGraduation_fieldId;
  }

  public void setAboutMe_fieldId(String aboutMe_fieldId) {
    this.aboutMe_fieldId = aboutMe_fieldId;
  }

  public String getAboutMe_fieldId() {
    return aboutMe_fieldId;
  }

  public void setNickName_fieldId(String nickName_fieldId) {
    this.nickName_fieldId = nickName_fieldId;
  }

  public String getNickName_fieldId() {
    return nickName_fieldId;
  }

  public void setFirstName_fieldId(String firstName_fieldId) {
    this.firstName_fieldId = firstName_fieldId;
  }

  public String getFirstName_fieldId() {
    return firstName_fieldId;
  }

  public void setSurName_fieldId(String surName_fieldId) {
    this.surName_fieldId = surName_fieldId;
  }

  public String getSurName_fieldId() {
    return surName_fieldId;
  }

  public void setConfirmEmailId_fieldId(String confirmEmailId_fieldId) {
    this.confirmEmailId_fieldId = confirmEmailId_fieldId;
  }

  public String getConfirmEmailId_fieldId() {
    return confirmEmailId_fieldId;
  }

  public void setTurnOn(String turnOn) {
    this.turnOn = turnOn;
  }

  public String getTurnOn() {
    return turnOn;
  }

  public void setTurnOn_fieldId(String turnOn_fieldId) {
    this.turnOn_fieldId = turnOn_fieldId;
  }

  public String getTurnOn_fieldId() {
    return turnOn_fieldId;
  }

  public void setTurnOff(String turnOff) {
    this.turnOff = turnOff;
  }

  public String getTurnOff() {
    return turnOff;
  }

  public void setTurnOff_fieldId(String turnOff_fieldId) {
    this.turnOff_fieldId = turnOff_fieldId;
  }

  public String getTurnOff_fieldId() {
    return turnOff_fieldId;
  }

  public void setMyMusic(String myMusic) {
    this.myMusic = myMusic;
  }

  public String getMyMusic() {
    return myMusic;
  }

  public void setMyMusic_fieldId(String myMusic_fieldId) {
    this.myMusic_fieldId = myMusic_fieldId;
  }

  public String getMyMusic_fieldId() {
    return myMusic_fieldId;
  }

  public void setMyMovies(String myMovies) {
    this.myMovies = myMovies;
  }

  public String getMyMovies() {
    return myMovies;
  }

  public void setMyMovies_fieldId(String myMovies_fieldId) {
    this.myMovies_fieldId = myMovies_fieldId;
  }

  public String getMyMovies_fieldId() {
    return myMovies_fieldId;
  }

  public void setMyTvShows(String myTvShows) {
    this.myTvShows = myTvShows;
  }

  public String getMyTvShows() {
    return myTvShows;
  }

  public void setMyTvShows_fieldId(String myTvShows_fieldId) {
    this.myTvShows_fieldId = myTvShows_fieldId;
  }

  public String getMyTvShows_fieldId() {
    return myTvShows_fieldId;
  }

  public void setHero_heroines(String hero_heroines) {
    this.hero_heroines = hero_heroines;
  }

  public String getHero_heroines() {
    return hero_heroines;
  }

  public void setHero_heroines_fieldId(String hero_heroines_fieldId) {
    this.hero_heroines_fieldId = hero_heroines_fieldId;
  }

  public String getHero_heroines_fieldId() {
    return hero_heroines_fieldId;
  }

  public void setNationalityDescription(String nationalityDescription) {
    this.nationalityDescription = nationalityDescription;
  }

  public String getNationalityDescription() {
    return nationalityDescription;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public String getSex() {
    return sex;
  }

  public void setDefaultImageList(List defaultImageList) {
    this.defaultImageList = defaultImageList;
  }

  public List getDefaultImageList() {
    return defaultImageList;
  }

  public void setDefaultImageId(String defaultImageId) {
    this.defaultImageId = defaultImageId;
  }

  public String getDefaultImageId() {
    return defaultImageId;
  }

  public void setImagePath(FormFile imagePath) {
    this.imagePath = imagePath;
  }

  public FormFile getImagePath() {
    return imagePath;
  }

  public void setTermsandconditions(String termsandconditions) {
    this.termsandconditions = termsandconditions;
  }

  public String getTermsandconditions() {
    return termsandconditions;
  }

  public void setStaticEmailAddress(String staticEmailAddress) {
    this.staticEmailAddress = staticEmailAddress;
  }

  public String getStaticEmailAddress() {
    return staticEmailAddress;
  }

  public void setUserPodImage(String userPodImage) {
    this.userPodImage = userPodImage;
  }

  public String getUserPodImage() {
    return userPodImage;
  }

  public void setImageId(String imageId) {
    this.imageId = imageId;
  }

  public String getImageId() {
    return imageId;
  }

  public void setImageName(String imageName) {
    this.imageName = imageName;
  }

  public String getImageName() {
    return imageName;
  }

  public void setImageFlag(String imageFlag) {
    this.imageFlag = imageFlag;
  }

  public String getImageFlag() {
    return imageFlag;
  }

  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }

  public String getGroupId() {
    return groupId;
  }

  public void setStaticDateOfBirth(String staticDateOfBirth) {
    this.staticDateOfBirth = staticDateOfBirth;
  }

  public String getStaticDateOfBirth() {
    return staticDateOfBirth;
  }

  public void setUnReadMessage(String unReadMessage) {
    this.unReadMessage = unReadMessage;
  }

  public String getUnReadMessage() {
    return unReadMessage;
  }

  public void setStatusFlag(String statusFlag) {
    this.statusFlag = statusFlag;
  }

  public String getStatusFlag() {
    return statusFlag;
  }

  public void setSchoolId(String schoolId) {
    this.schoolId = schoolId;
  }

  public String getSchoolId() {
    return schoolId;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getTown() {
    return town;
  }

  public void setSchoolType(String schoolType) {
    this.schoolType = schoolType;
  }

  public String getSchoolType() {
    return schoolType;
  }

  public void setButtonType(String buttonType) {
    this.buttonType = buttonType;
  }

  public String getButtonType() {
    return buttonType;
  }

  public void setIntendedyearOfGraduation(String intendedyearOfGraduation) {
    this.intendedyearOfGraduation = intendedyearOfGraduation;
  }

  public String getIntendedyearOfGraduation() {
    return intendedyearOfGraduation;
  }

  public void setIntendedyearOfGraduation_fieldId(String intendedyearOfGraduation_fieldId) {
    this.intendedyearOfGraduation_fieldId = intendedyearOfGraduation_fieldId;
  }

  public String getIntendedyearOfGraduation_fieldId() {
    return intendedyearOfGraduation_fieldId;
  }

  public void setIntendedGraduationList(List intendedGraduationList) {
    this.intendedGraduationList = intendedGraduationList;
  }

  public List getIntendedGraduationList() {
    return intendedGraduationList;
  }

  public void setYearOfGraduationFlag(String yearOfGraduationFlag) {
    this.yearOfGraduationFlag = yearOfGraduationFlag;
  }

  public String getYearOfGraduationFlag() {
    return yearOfGraduationFlag;
  }

  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }

  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }

  public void setReceiveEmails(String receiveEmails) {
    this.receiveEmails = receiveEmails;
  }

  public String getReceiveEmails() {
    return receiveEmails;
  }

  public void setReceiveEmails_fieldId(String receiveEmails_fieldId) {
    this.receiveEmails_fieldId = receiveEmails_fieldId;
  }

  public String getReceiveEmails_fieldId() {
    return receiveEmails_fieldId;
  }

  public void setCourseDeletedFlag(String courseDeletedFlag) {
    this.courseDeletedFlag = courseDeletedFlag;
  }

  public String getCourseDeletedFlag() {
    return courseDeletedFlag;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response) {
    ActionErrors errors = new ActionErrors();
    HttpSession httpSession = request.getSession();
    errors.clear();
    if (request.getParameter("butvalue") != null && request.getParameter("butvalue").equalsIgnoreCase("Save profile")) {
      if (new CommonFunction().checkSessionExpired(request, response, httpSession, "REGISTERED_USER")) {
        return errors;
      }
      firstName = firstName != null ? firstName.trim() : firstName;
      surName = surName != null ? surName.trim() : surName;
      emailId = emailId != null ? emailId.trim() : emailId;
      confirmEmailId = confirmEmailId != null ? confirmEmailId.trim() : confirmEmailId;
      password = password != null ? password.trim() : password;
      if (firstName == null || firstName.equals("") || firstName.trim().length() == 0) {
        errors.add("wuni.error.profile.firstname", new ActionMessage("wuni.error.profile.firstname"));
      }
      if (surName == null || surName.equals("") || surName.trim().length() == 0) {
        errors.add("wuni.error.profile.surname", new ActionMessage("wuni.error.profile.surname"));
      }
      if (educationStatus == null || educationStatus.equals("") || educationStatus.trim().length() == 0) {
        errors.add("wuni.error.profile.educationstatus", new ActionMessage("wuni.error.profile.educationstatus"));
      }
      if (gender == null || gender.equals("") || gender.trim().length() == 0) {
        errors.add("wuni.error.profile.gender", new ActionMessage("wuni.error.profile.gender"));
      }
      if (nationality == null || nationality.equals("") || nationality.trim().length() == 0) {
        errors.add("wuni.error.profile.nationality", new ActionMessage("wuni.error.profile.nationality"));
      }
      if (emailId == null || emailId.equals("") || emailId.trim().length() == 0) {
        errors.add("wuni.error.profile.emailid", new ActionMessage("wuni.error.profile.emailid"));
      }
      if (confirmEmailId == null || confirmEmailId.equals("") || confirmEmailId.trim().length() == 0) {
        errors.add("wuni.error.profile.confirm.email", new ActionMessage("wuni.error.profile.confirm.email"));
      }
      if (password == null || password.equals("") || password.trim().length() == 0) {
        errors.add("wuni.error.profile.password", new ActionMessage("wuni.error.profile.password"));
      }
      if (yearOfGraduation == null || yearOfGraduation.equals("") || yearOfGraduation.trim().length() == 0) {
        errors.add("wuni.error.profile.year_of_graduation", new ActionMessage("wuni.error.profile.year_of_graduation"));
      }
      if ((emailId != null && emailId.trim().length() > 0) && !new CommonUtil().isValidEmail(emailId)) {
        errors.add("wuni.error.profile.invalid.emailid", new ActionMessage("wuni.error.profile.invalid.emailid"));
      }
      if (emailId != null && !emailId.equals(confirmEmailId)) {
        errors.add("wuni.error.profile.email_mismatch", new ActionMessage("wuni.error.profile.email_mismatch"));
      }
      // added on dec 15 2009 release. for DOB checking
      if (dateOfBirth != null && dateOfBirth.equals("")) {
        errors.add("wnui.error.invalid_date", new ActionMessage("wnui.error.invalid_date"));
      } else{
        staticDateOfBirth=dateOfBirth;
      }
      if ((dateOfBirth != null || dateOfBirth.length() > 0) && (staticDateOfBirth != null || staticDateOfBirth.length() > 0) && (!dateOfBirth.equals(staticDateOfBirth))) {
        ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
        String dateFormat = bundle.getString("wuni.format.date");
        int dateFormatLength = dateFormat.length();
        if (dateOfBirth.length() != dateFormatLength) {
          errors.add("wnui.error.invalid_date", new ActionMessage("wnui.error.invalid_date"));
        } else {
          String sysDate = String.valueOf(httpSession.getAttribute("sysDate"));
          if (sysDate != null && sysDate.trim().length() > 0) {
            if (!new CommonUtil().isValidDate(dateOfBirth, sysDate, dateFormat)) {
              errors.add("wnui.error.invalid_date", new ActionMessage("wnui.error.invalid_date"));
            }
          }
        }
      }
    }
    ///////////////////////// Check for Validation in Profile Picture uploading pagee/////////////////////////////////////
    if (request.getParameter("butvalue") != null && request.getParameter("butvalue").equalsIgnoreCase("browseupload")) {
      String imagename = null;
      String contentType = null;
      String contentExt = "";
      boolean flag = false;
      long fileSize = 0;
      if (imagePath != null) {
        imagename = imagePath.getFileName();
        contentType = imagePath.getContentType();
        fileSize = imagePath.getFileSize();
      }
      if (imagename == null || imagename.equals("")) {
        errors.add("fileupload.photo.empty", new ActionMessage("fileupload.photo.empty"));
      } else {
        if ((contentType != null) && (imagename != null && imagename.trim().length() > 0)) {
          StringTokenizer st = new StringTokenizer(contentType, "/");
          if (st != null) {
            st.nextToken("/");
            contentExt = st.nextElement().toString();
            if (contentExt != null && contentExt.equalsIgnoreCase("pjpeg")) {
              flag = true;
            }
            if (contentExt != null && contentExt.equalsIgnoreCase("gif")) {
              flag = true;
            }
            if (contentExt != null && contentExt.equalsIgnoreCase("x-png")) {
              flag = true;
            }
            if (contentExt != null && contentExt.equalsIgnoreCase("jpeg")) {
              flag = true;
            }
            if (!flag) {
              errors.add("phototypeerror", new ActionMessage("fileupload.photo.typeerror"));
            }
          }
        }
        if (fileSize > 1048576) {
          errors.add("photosizeerror", new ActionMessage("fileupload.photo.error"));
        }
      }
    }
    if (request.getParameter("butvalue") != null && request.getParameter("butvalue").equalsIgnoreCase("defaultupload")) {
      if (defaultImageId == null || defaultImageId.equals("")) {
        errors.add("fileupload.photo.empty", new ActionMessage("fileupload.photo.empty"));
      }
    }
    return errors;
  }*/

  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public void setMobFieldId(String mobFieldId) {
    this.mobFieldId = mobFieldId;
  }

  public String getMobFieldId() {
    return mobFieldId;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setMychNavCnt(String mychNavCnt) {
    this.mychNavCnt = mychNavCnt;
  }
  public String getMychNavCnt() {
    return mychNavCnt;
  }

  public void setIsChatbotDisbaled(String isChatbotDisbaled) {
    this.isChatbotDisbaled = isChatbotDisbaled;
  }

  public String getIsChatbotDisbaled() {
    return isChatbotDisbaled;
  }

  public void setForeName(String foreName) {
    this.foreName = foreName;
  }

  public String getForeName() {
    return foreName;
  }

}
