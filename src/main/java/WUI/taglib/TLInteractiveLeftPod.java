package WUI.taglib;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import spring.form.VideoReviewListBean;
import spring.util.GlobalMethods;
import WUI.search.form.SearchBean;
import WUI.search.form.SearchResultBean;
import WUI.utilities.AdvertUtilities;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;

import com.wuni.util.affiliate.AffiliateUtilities;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.ClearingInfoVO;

/**
  * @Version : 1.0
  * @Since 2010-Dec-07
  * @author Mohamed Syed  *
  * Display the course provider related interactive informations.
  * *************************************************************************************************************************
  * Date           Name               Ver.          Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 2010-Dec-07    Syed               wu_2.1.5      Initial draft
  */
  
public class TLInteractiveLeftPod extends BodyTagSupport {
  public String resultType = null;
  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    String collegeId = (String)request.getAttribute("interactive-college-id");
    collegeId = collegeId != null && collegeId.trim().length() > 0? collegeId.trim(): "0";
    String subjectid = (String)request.getAttribute("interactive-sid");
    subjectid = subjectid != null && subjectid.trim().length() > 0? subjectid.trim(): "";
    String showSubjectList = (String)request.getAttribute("interactive-show-subjectlist");
    showSubjectList = showSubjectList != null && showSubjectList.trim().length() > 0 ? showSubjectList: "";
    List InstitutionProfileList = null;
    List subjectProfileList = null;
    VideoReviewListBean videobean = null;
    HttpSession session = request.getSession();
    
    String cpeQualificationNetworkId = (String)session.getAttribute("cpeQualificationNetworkId");     
    if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0 || cpeQualificationNetworkId.equals("0") ){
        cpeQualificationNetworkId = "2";
    }
    session.removeAttribute("cpeQualificationNetworkId");
    session.setAttribute("cpeQualificationNetworkId", cpeQualificationNetworkId);       
    
    String contextPath = request.getContextPath();
    Vector vector = new Vector();
    vector.clear();
    JspWriter out = pageContext.getOut();
    
    try {
      List CollegeInteractivePodList = null;
      CollegeInteractivePodList = (List)request.getAttribute("complete_college_interaction");
      if (CollegeInteractivePodList != null && CollegeInteractivePodList.size() > 0) {
        Iterator uniiterator = CollegeInteractivePodList.iterator();
        SearchResultBean bean = (SearchResultBean)uniiterator.next();
        
        //GETTING IP & SP DETAIS
        new AdvertUtilities().getIpSpProfileDetails(collegeId,request,cpeQualificationNetworkId,null, null, null); //13_JAN_15 Added by Amir for Video Rich Profile
        
        InstitutionProfileList = (List)request.getAttribute("institution-profile-list");
        if (InstitutionProfileList != null && InstitutionProfileList.size() > 0) {
          uniiterator = InstitutionProfileList.iterator();
          videobean = (VideoReviewListBean)uniiterator.next();
        } 
       
        subjectProfileList = (List)request.getAttribute("subject-profile-list");
        if (subjectProfileList != null && subjectProfileList.size() > 0) {
          uniiterator = subjectProfileList.iterator();
          videobean = (VideoReviewListBean)uniiterator.next();
        } 



          
        // To display the college profile and Subject profile if the colleges is an advertiser otherwise it will display the video ticker tape 
        if (resultType != null && resultType.equalsIgnoreCase("university-profile-interaction")) {
          String topimageid = "";
          if(InstitutionProfileList != null && InstitutionProfileList.size() > 0 || subjectProfileList != null && subjectProfileList.size() >0 ) {
          if((videobean.getSubOrderItemIdForIp() != null && videobean.getSubOrderItemIdForIp().trim().length()>0 && !videobean.getSubOrderItemIdForIp().equalsIgnoreCase("NULL")) ||
            (videobean.getSubOrderItemIdForSp() != null && videobean.getSubOrderItemIdForSp().trim().length()>0 && !videobean.getSubOrderItemIdForSp().equalsIgnoreCase("NULL")))
          {
            if(videobean.getProfileTypeIp() !=null  && videobean.getProfileTypeIp().trim().length() > 0 && !videobean.getProfileTypeIp().equalsIgnoreCase("null"))
            {
            //To display the college profile
            
out.println("<h1>");
            if(videobean.getMyhcProfileIdIp() != null && videobean.getMyhcProfileIdIp().trim().length() > 0){            
out.println("<a class=\"bklink\" href=\""+contextPath+GlobalConstants.SEO_PATH_UNI_PROFILE_PAGE +new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(bean.getCollegeName()))))+"-overview/"+bean.getCollegeId()+"/overview/"+videobean.getMyhcProfileIdIp()+"/0/"+cpeQualificationNetworkId+"/universityprofile.html\" title=\"" + bean.getCollegeNameDisplay() + " profile\">" + bean.getCollegeNameDisplay() + "</a>");            
            }else{
out.println("<a class=\"bklink\" href=\"javascript:void(0);\" >" + bean.getCollegeNameDisplay() + "</a>");                          
            }
out.println("</h1>");

out.println("<div>");
             
            //wu314_20110712_Syed: Stacey-functional-crd#1 - linking to clearing-micro-site            
            //getClearingInfo(out, collegeId);//Commented by Sekhar K for wu414_20120731
             
            //
            getNextOpenDays(request,out);
             
            //
            getUniOverAllReviewSummary(request,out,bean);


            if (request.getAttribute("show-google-map") == null) {
              if (bean.getCollegeLogo() != null && bean.getCollegeLogo().trim().length() >= 0) {
                if (bean.getLogoType() != null && bean.getLogoType().trim().equalsIgnoreCase("video")) {
out.println("<a rel=\"nofollow\" class=\"vhold\" onclick=\"return showVideo('" + bean.getLogoId() + "','" + bean.getCollegeId() + "','B')\" title=\"overview\"><img src=\"" + bean.getVideoThumbUrl() + "\" alt=\"" + bean.getCollegeName() + " profile\"" + topimageid + " title=\"" + bean.getCollegeNameDisplay() + " profile\" width=\"205\" height=\"160\" /><span class=\"vplay2\"><img title=\"\" alt=\"\" src=\"" + CommonUtil.getImgPath("",0) + "/wu-cont/images/playvideo.png\"/></span></a>");
                  request.setAttribute("showlightbox", "YES");
                }
                if (bean.getLogoType() != null && bean.getLogoType().trim().equalsIgnoreCase("image")) {
                  if(videobean.getMyhcProfileIdIp() != null && videobean.getMyhcProfileIdIp().trim().length() > 0){
out.println("<a href=\""+contextPath+GlobalConstants.SEO_PATH_UNI_PROFILE_PAGE +new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(bean.getCollegeName()))))+"-overview/"+bean.getCollegeId()+"/overview/"+videobean.getMyhcProfileIdIp()+"/0/"+cpeQualificationNetworkId+"/universityprofile.html\" title=\"overview\"><img src=\"" + CommonUtil.getImgPath(bean.getCollegeLogo(),0)  + "\" alt=\"" + bean.getCollegeName() + " profile\"" + topimageid + " title=\"" + bean.getCollegeNameDisplay() + " profile\" /></a>");
                  }
                }
              }
            } else {
              String latlangvalue = getLatitudeLangtitue(bean.getCollegeId());
              if(latlangvalue !=null && latlangvalue.trim().length()>0){
                  String latLangArr[] = latlangvalue.split(",");
                  if (latLangArr != null && latLangArr.length >= 2) {
                    out.println("<img src=\"http://maps.google.com/maps/api/staticmap?center=54.7211,-3.5943&markers=color:red|label:|"+latLangArr[0]+","+latLangArr[1]+"&zoom=4&size=180x200&sensor=false&language=en-UK&maptype=roadmap&key="+new SessionData().getData(request,"GOOGLE_MAP_KEY")+"\" />");
                    out.println("<p class=\"expandv\"><a rel=\"nofollow\"  href=\"" + request.getContextPath() + "/expand-collegemap.html?z=" + bean.getCollegeId() + "&latval=" + latLangArr[0] + "&langval=" + latLangArr[1] + "\">Expand</a></p>");
                  }
                }    
              }
            if (bean.getOverviewShortDesc() != null && bean.getOverviewShortDesc().trim().length() > 0) {
              if (bean.getOverviewShortDesc().trim().length() > 295) {
out.println("<p class=\"unilogo\">" + bean.getOverviewShortDesc().substring(0, 294) + "....&nbsp;");
                if(videobean.getMyhcProfileIdIp() != null && videobean.getMyhcProfileIdIp().trim().length() > 0){
out.println("<a href=\""+contextPath+GlobalConstants.SEO_PATH_UNI_PROFILE_PAGE +new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(bean.getCollegeName()))))+"-overview/"+bean.getCollegeId()+"/overview/"+videobean.getMyhcProfileIdIp()+"/0/"+cpeQualificationNetworkId+"/universityprofile.html\" title=\"Find out more\">Find out more</a>");
                }
out.println("</p>");
              } else {
out.println("<p class=\"unilogo\">" + bean.getOverviewShortDesc() + "</p>");
              }
            }
out.println("</div>");
           }else if(videobean.getProfileTypeSp() !=null  && videobean.getProfileTypeSp().trim().length() > 0 && !videobean.getProfileTypeSp().equalsIgnoreCase("null"))
             {
              
out.println("<h1>");
              if(videobean.getMyhcProfileIdSp() != null && videobean.getMyhcProfileIdSp().trim().length() > 0){
out.println("<a class=\"bklink\" href=\""+contextPath+GlobalConstants.SEO_PATH_SUBJECT_PROFILE_PAGE +new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(bean.getCollegeName()))))+"-overview/"+bean.getCollegeId()+"/overview/"+videobean.getMyhcProfileIdSp()+"/0/"+cpeQualificationNetworkId+"/subjectprofile.html\" title=\"" + bean.getCollegeNameDisplay() + " profile\">" + bean.getCollegeNameDisplay() + "</a> ");
            }else{
              out.println("<a class=\"bklink\" href=\"javascript:void(0);\" >" + bean.getCollegeNameDisplay() + "</a>");                          
            }
out.println("</h1>");
out.println("<div>");

              //wu314_20110712_Syed: Stacey-functional-crd#1 - linking to clearing-micro-site            
              //getClearingInfo(out, collegeId);//Commented By Sekhar K for wu414_20120731              
              
              //
              getNextOpenDays(request,out);
               
              //
              getUniOverAllReviewSummary(request,out,bean);
              

              if (request.getAttribute("show-google-map") == null) {
                if (bean.getCollegeLogo() != null && bean.getCollegeLogo().trim().length() >= 0) {
                  if (bean.getLogoType() != null && bean.getLogoType().trim().equalsIgnoreCase("video")) {
out.println("<a rel=\"nofollow\" class=\"video\" onclick=\"return showVideo('" + bean.getLogoId() + "','" + bean.getCollegeId() + "','B')\" title=\"overview\"><img src=\"" + bean.getVideoThumbUrl() + "\" alt=\"" + bean.getCollegeName() + " profile\"" + topimageid + " title=\"" + bean.getCollegeNameDisplay() + " profile\" width=\"80\" height=\"80\" /><span><img title=\"\" alt=\"\" class=\"play_icon\" src=\"" + CommonUtil.getImgPath("",0) + "/wu-cont/img/whatuni/video_play_medium.gif\"/></span></a>");
                    request.setAttribute("showlightbox", "YES");
                  }
                  if (bean.getLogoType() != null && bean.getLogoType().trim().equalsIgnoreCase("image")) {
                    if(videobean.getMyhcProfileIdSp() != null && videobean.getMyhcProfileIdSp().trim().length() > 0){
out.println("<a href=\""+contextPath+GlobalConstants.SEO_PATH_SUBJECT_PROFILE_PAGE +new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(bean.getCollegeName()))))+"-overview/"+bean.getCollegeId()+"/overview/"+videobean.getMyhcProfileIdSp()+"/0/"+cpeQualificationNetworkId+"/subjectprofile.html\" title=\"overview\"><img src=\"" + CommonUtil.getImgPath(bean.getCollegeLogo(),0) + "\" alt=\"" + bean.getCollegeName() + " profile\"" + topimageid + " title=\"" + bean.getCollegeNameDisplay() + " profile\" class=\"unilogo\" /></a>");
                    }
                  }
                }
              } else {
                String latlangvalue = getLatitudeLangtitue(bean.getCollegeId());
                if(latlangvalue !=null && latlangvalue.trim().length()>0){
                    String latLangArr[] = latlangvalue.split(",");
                    if (latLangArr != null && latLangArr.length >= 2) {
                      out.println("<img src=\"http://maps.google.com/maps/api/staticmap?center=54.7211,-3.5943&markers=color:red|label:|"+latLangArr[0]+","+latLangArr[1]+"&zoom=4&size=180x200&sensor=false&language=en-UK&maptype=roadmap&key="+new SessionData().getData(request,"GOOGLE_MAP_KEY")+"\" />");
                      out.println("<p class=\"expandv\"><a rel=\"nofollow\"  href=\"" + request.getContextPath() + "/expand-collegemap.html?z=" + bean.getCollegeId() + "&latval=" + latLangArr[0] + "&langval=" + latLangArr[1] + "\">Expand</a></p>");
                    }
                  }                  
               }
              if (bean.getOverviewShortDesc() != null && bean.getOverviewShortDesc().trim().length() > 0) {
                if (bean.getOverviewShortDesc().trim().length() > 295) {
out.println("<p class=\"unilogo\">" + bean.getOverviewShortDesc().substring(0, 294) + "....&nbsp;");
                  if(videobean.getMyhcProfileIdSp() != null && videobean.getMyhcProfileIdSp().trim().length() > 0){
out.println("<a href=\""+contextPath+GlobalConstants.SEO_PATH_SUBJECT_PROFILE_PAGE +new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(bean.getCollegeName()))))+"-overview/"+bean.getCollegeId()+"/overview/"+videobean.getMyhcProfileIdSp()+"/0/"+cpeQualificationNetworkId+"/subjectprofile.html\" title=\"Find out more\">Find out more</a>");
                  }
out.println("</p>");
                   
                } else {
out.println("<p class=\"unilogo\">" + bean.getOverviewShortDesc() + "</p>");
                }
              }
out.println("</div>");              
            }     
          }
        } else { 
            //To Display the video with Ticker Tape in non-advertiser pages
            if (request.getAttribute("uniLanding") != null) {
              ArrayList tickerVideoList = new GlobalFunction().loadTickerVideo();
              request.setAttribute("showlightbox", "YES");
              
              out.println("<h1>");
              out.println("<a class=\"bklink\" href=\"" + new GlobalMethods().changeSeoURLLink("unilanding", bean.getCollegeName() + GlobalConstants.SEO_URL_SPLIT_CHAR + bean.getCollegeId()) + "\" title=\"" + bean.getCollegeNameDisplay() + "\">" + bean.getCollegeNameDisplay() + "</a>");              
              out.println("</h1>");
              //
              getNextOpenDays(request,out);
              //
              getUniOverAllReviewSummary(request,out,bean);                            
              //
            }
          }
        }
    
    //This will display subject profile list     
    if(resultType != null && resultType.equalsIgnoreCase("subject-profile-interaction")) {      
        subjectProfileList =  new AdvertUtilities().getAvailableSubjectProfileList(collegeId,cpeQualificationNetworkId);
        if(subjectProfileList != null && subjectProfileList.size() > 0) {
          request.setAttribute("subjectProfileList", subjectProfileList);
          out.println("<div class=\"unisponsor left_uspons\">");
          out.println("<ul class=\"subprofile\">");
          out.println("<li><a  rel=\"nofollow\" href=\"" + contextPath + "/subprofilelist.html?z=" + bean.getCollegeId() + "\" title=\"" + bean.getCollegeNameDisplay() + " subject profile\">Subject profile</a>");
          out.println("<ul>");
          Iterator subjectiterator = subjectProfileList.iterator();
          while (subjectiterator.hasNext()) {
            SearchBean subjectbean = (SearchBean)subjectiterator.next();
            if(subjectbean.getMyhcProfileId() != null && subjectbean.getMyhcProfileId().trim().length() > 0){
              out.println("<li><a href=\""+ contextPath + GlobalConstants.SEO_PATH_SUBJECT_PROFILE_PAGE +new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(bean.getCollegeName())))) +"-overview/"+ bean.getCollegeId() +"/overview/"+subjectbean.getMyhcProfileId()+"/0/"+cpeQualificationNetworkId+"/subjectprofile.html\" title=\"" + subjectbean.getAdvertName() + "\" >" + subjectbean.getAdvertName()+"</a></li>");
            }
          }
          out.println("</ul>");
          out.println("</li>");
          out.println("</ul>");
          out.println("</div>");
    } } 
    
    //To display the college email option
    if (resultType != null && resultType.equalsIgnoreCase("university-email-interaction")) {
      if(InstitutionProfileList != null && InstitutionProfileList.size() > 0) {
        if(videobean.getSubOrderItemIdForIp() != null && videobean.getSubOrderItemIdForIp().trim().length() > 0 && !videobean.getSubOrderItemIdForIp().equalsIgnoreCase("NULL")){    
          if(videobean.getSubOrderEmailWebformIp() != null && videobean.getSubOrderEmailWebformIp().trim().length() > 0){
            out.println("<div>");
            out.println("<h4 class=\"emailuni\" title=\"Email " + bean.getCollegeNameDisplay() + "\">");
            String webformPrice = new CommonFunction().getGACPEPrice(videobean.getSubOrderItemIdForIp(), "webform_price");
            out.println("<a rel=\"nofollow\" target=\"_blank\" href='"+videobean.getSubOrderEmailWebformIp()+"' onclick=\"GAInteractionEventTracking('emailwebform', 'interaction', 'Webclick','"+new CommonFunction().replaceSpecialCharacter(bean.getCollegeName())+"', "+webformPrice+"); cpeEmailWebformClick(this,'"+bean.getCollegeId()+"','"+videobean.getSubOrderItemIdForIp()+"','"+cpeQualificationNetworkId+"','"+videobean.getSubOrderEmailWebformIp()+"'); var a='s.tl(';\" title=\"Email " + bean.getCollegeNameDisplay() + "\" >Email uni</a>");
            out.println("</h4>");
            out.println("</div>");                
          }else{        
            out.println("<div>");
            out.println("<h4 class=\"emailuni\" title=\"Email " + bean.getCollegeNameDisplay() + "\">");
            //out.println("<a rel=\"nofollow\" href=\"" + contextPath + "/send-college-email.html?z=" + bean.getCollegeId()+ "&amp;subOrderItemId="+ videobean.getSubOrderItemIdForIp() + "\" title=\"Email " + bean.getCollegeNameDisplay() + "\" >Email uni</a>");
            out.println("<a onclick=\"GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '"+new CommonFunction().replaceSpecialCharacter(bean.getCollegeName())+"');\" rel=\"nofollow\" href=\"" + contextPath + "/email/"+new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(bean.getCollegeName())))+"-email/"+bean.getCollegeId()+"/0/0/n-"+videobean.getSubOrderItemIdForIp()+"/send-college-email.html" + "\" title=\"Email " + bean.getCollegeNameDisplay() + "\" >Email uni</a>");
            out.println("</h4>");
            out.println("</div>");   
          }
        }
      }
    }

    //display the uni-competitors
    if (resultType != null && resultType.equalsIgnoreCase("university-interactive-competitors")) {    
      if(request.getAttribute("isAdvertiser") == null || (request.getAttribute("isAdvertiser") != null && request.getAttribute("isAdvertiser").equals("FALSE"))){
        List competitorsList = new CommonFunction().getCompetitorsPod(collegeId, request);
        if (competitorsList != null && competitorsList.size() > 0) {
          Iterator competitorsiterator = competitorsList.iterator();
          out.println("<div id=\"spons\">");
          out.println("<h2>Students interested in this uni are also interested in</h2>");
          while (competitorsiterator.hasNext()) {
            SearchResultBean competitorbean = (SearchResultBean)competitorsiterator.next();
            out.println("<p><a href=\"" + new GlobalMethods().changeSeoURLLink("unilanding", competitorbean.getCollegeName() + GlobalConstants.SEO_URL_SPLIT_CHAR + competitorbean.getCollegeId()) + "\" title=\"" + competitorbean.getCollegeNameDisplay() + "\">" + competitorbean.getCollegeNameDisplay() + "</a><span>&raquo; </span> </p>");
          }
          out.println("<hr />");
          out.println("</div>");
    } } }
        
    //removing "providerResult"
    if(InstitutionProfileList != null && InstitutionProfileList.size() > 0) {
      if(videobean.getSubOrderItemIdForIp() != null && videobean.getSubOrderItemIdForIp().trim().length()>0 && !videobean.getSubOrderItemIdForIp().equalsIgnoreCase("NULL")){
        if(videobean.getProfileTypeIp() !=null  &&  videobean.getProfileTypeIp().trim().length() > 0 && !videobean.getProfileTypeIp().equalsIgnoreCase("null")){
          request.removeAttribute("providerResult");
        }
      } 
    } 
       
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return EVAL_BODY_BUFFERED;
  }
  
  public int doAfterBody() throws JspException {
    try {
      BodyContent bodycontent = getBodyContent();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }
  
  public int doEndTag() {
    return EVAL_PAGE;
  }
  
  private String getLatitudeLangtitue(String collegeId) {
    String latLang = "";
    Vector vector = new Vector();
    try {
      vector.add(collegeId);
      latLang = new DataModel().getString("wu_util_pkg.get_college_lat_long_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
    }
    return latLang;
  }

  private void getUniOverAllReviewSummary(HttpServletRequest request, JspWriter out, SearchResultBean bean) {
    try{
      String contextPath = request.getContextPath();
      if (bean.getReviewCount() != null && !bean.getReviewCount().equals("0") && bean.getReviewCount().trim().length() > 0) {
        out.println("<div id=\"reviewright\">");
        String orating = "rating" + bean.getOverAllRating();
        out.println("Overall rating:");
        //out.println("<a rel=\"nofollow\" href=\"" + contextPath + "/uniwrittenreviews.html?z=" + (bean.getCollegeId() != null? bean.getCollegeId().trim(): bean.getCollegeId()) + "\" title=\"View " + bean.getCollegeNameDisplay() + " review(s)\">");
        out.println("<a href=\"" + contextPath + new CommonFunction().uniLandingReviewSEOUrl(bean.getCollegeId(), bean.getCollegeName())+ "\" title=\"View " + bean.getCollegeNameDisplay() + " review(s)\">");
        out.println("<span class=\"" + orating + "\" title=\"View " + bean.getCollegeNameDisplay() + " review(s)\"></span>");
        out.println("</a>");
        out.println("Based on <a href=\"" + contextPath + new CommonFunction().uniLandingReviewSEOUrl(bean.getCollegeId(), bean.getCollegeName())+ "\" title=\"View " + bean.getCollegeNameDisplay() + " review(s)\">" + bean.getReviewCount() + " review(s)</a>");
        out.println("</div>");
      }  
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  
  private String getNextOpenDays(HttpServletRequest request, JspWriter out) {
    String nextOpenDaysPod = "";
    CommonFunction common = new CommonFunction();
    String opendayCollegeId = (String)request.getAttribute("overallCollegeId");
    String opendayCollegeName = (String)request.getAttribute("collegeName");    
    String opendayCollegeNameDisplay = request.getAttribute("collegeNameDisplay") != null && request.getAttribute("collegeNameDisplay").toString().trim().length() > 0 ? (String) request.getAttribute("collegeNameDisplay") : ""; 
    String seoCollegeName = opendayCollegeName != null && opendayCollegeName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(opendayCollegeName)).toLowerCase() : "";
    String collgeLocation = (String)request.getAttribute("collegeLocation");
    collgeLocation = collgeLocation !=null && collgeLocation.trim().length()>0 ? common.replaceHypen(common.replaceURL(collgeLocation)).toLowerCase() : ""; 
    String openDayUrl = (String)request.getAttribute("OPEN_DAY_URL");     
    String subOrderItemId = (String)request.getAttribute("subOrderItemId");    
    String subOrderProspectusWebform = (String)request.getAttribute("subOrderProspectusWebform"); 
    String subOrderWebsite = (String)request.getAttribute("subOrderWebsite"); 
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");    
    if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0){ 
      cpeQualificationNetworkId = "2";
    }  request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);    
    String nextCollegeOpenday = (String)request.getAttribute("next_col_openday");
    try{
    if(nextCollegeOpenday != null && nextCollegeOpenday.trim().length() > 0){
      nextOpenDaysPod = "<p class=\"nextopen1\">Next open day:<br //>";
      //out.println(nextOpenDaysPod);      

      if(subOrderItemId != null && subOrderItemId.trim().length() > 0){
        String websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice(subOrderItemId, "website_price");
        if(openDayUrl != null && openDayUrl.trim().length() > 0){
          nextOpenDaysPod = nextOpenDaysPod + "<a rel=\"nofollow\" target=\"_blank\" onclick=\"GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick','"+new CommonFunction().replaceSpecialCharacter(opendayCollegeName)+"',"+websitePrice+"); cpeWebClick(this,'"+opendayCollegeId+"','"+subOrderItemId+"','"+cpeQualificationNetworkId+"','"+openDayUrl+"');var a='s.tl(';\" href=\""+openDayUrl+"\" title=\"Important "+opendayCollegeNameDisplay+" open day details\">"+nextCollegeOpenday+"</a>";          
          //out.println(nextOpenDaysPod);
        } else {
          nextOpenDaysPod = nextOpenDaysPod + "<a rel=\"nofollow\" target=\"_blank\" onclick=\"GAInteractionEventTracking('visitwebsite', 'interaction', 'Webclick','"+new CommonFunction().replaceSpecialCharacter(opendayCollegeName)+"'"+websitePrice+"); cpeWebClick(this,'"+opendayCollegeId+"','"+subOrderItemId+"','"+cpeQualificationNetworkId+"','"+subOrderWebsite+"');var a='s.tl(';\" href=\""+subOrderWebsite+"\" title=\""+opendayCollegeNameDisplay+" website\">"+nextCollegeOpenday+"</a>";          
          //out.println(nextOpenDaysPod);          
        }
        
      } else {
        if(collgeLocation != null && collgeLocation.trim().length() > 0){
          nextOpenDaysPod = nextOpenDaysPod + "<a href='"+request.getContextPath()+GlobalConstants.SEO_PATH_OPEN_DAY_PAGE+collgeLocation+"-open-days-"+seoCollegeName+"/"+opendayCollegeId+"/page.html' title=\"Important "+opendayCollegeNameDisplay+" open day details\">"+nextCollegeOpenday+"</a>";      
          //out.println(nextOpenDaysPod);
        } else {
          nextOpenDaysPod = nextOpenDaysPod + nextCollegeOpenday;
          //out.println(nextOpenDaysPod);
        }
      }
      nextOpenDaysPod = nextOpenDaysPod + "</p>";          
      out.println(nextOpenDaysPod);      
    }  
    }catch(Exception e){
      e.printStackTrace();
    }
    return nextOpenDaysPod;
  }
  
  //
  private String getClearingInfo(JspWriter out, String collegeId) { 
    //wu314_20110712_Syed: Stacey-functional-crd#1 - linking to clearing-micro-site    
    ArrayList clearingInfoList = new AffiliateUtilities().getClearingInfoList(collegeId);
    String clearingInfo = "";
    if (clearingInfoList != null && clearingInfoList.size() > 0) {
        ClearingInfoVO clearingInfoVO = null;        
        for(int i=0; i<1; i++ ){          
          clearingInfoVO = (ClearingInfoVO)clearingInfoList.get(i);                
          clearingInfo = clearingInfo + "<a rel=\"nofollow\" target=\"_blank\" href=\"" + clearingInfoVO.getClearingProfileUrl() + "\" title=\"Clearing 2011\" >";
          clearingInfo = clearingInfo + "<img src=\""+CommonUtil.getImgPath("",0)+"/wu-cont/img/whatuni/sprites/clearing/clearinglogo.png\" class=\"lclr\" width=\"180\" height=\"52\" alt=\"Clearing 2011\" /></a>";          
        }
      try{        
        out.println(clearingInfo);
      }catch(Exception e){
        e.printStackTrace();
      }
    }    
    return clearingInfo;
  }
  
  
  public void setResultType(String resultType) {
    this.resultType = resultType;
  }
  
  public String getResultType() {
    return resultType;
  }
}
