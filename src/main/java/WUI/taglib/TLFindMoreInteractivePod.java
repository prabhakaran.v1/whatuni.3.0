package WUI.taglib;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import WUI.search.form.SearchResultBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

/**
  * @TLFindMoreInteractivePod.java
  * @Version : 2.0
  * @September 20 2007
  * @www.whatuni.com
  * @Created By : Balraj. Selva Kumar
  * @Purpose  : Display the course provider related interactive informations.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class TLFindMoreInteractivePod extends BodyTagSupport {
  public TLFindMoreInteractivePod() {
  }
  private String showHide = "";
  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    String collegeId = (String)request.getAttribute("interactive-college-id");
    collegeId = collegeId != null && collegeId.trim().length() > 0? collegeId.trim(): "0";
    String contextPath = request.getContextPath();
    try {
      JspWriter out = pageContext.getOut();
      List CollegeInteractivePodList = (List)request.getAttribute("complete_college_interaction");
      if (CollegeInteractivePodList != null && CollegeInteractivePodList.size() > 0) {
        Iterator uniiterator = CollegeInteractivePodList.iterator();
        SearchResultBean bean = (SearchResultBean)uniiterator.next();
        String tooltipcollegename = bean.getCollegeName();
        out.println(" <div class=\"stud-say-blk\">");
        /////////////// To show the link to view all courses  /////////////////
       // out.println("<ul class=\"allcourses\" title=\"Find out more about " + bean.getCollegeNameDisplay() + "\">");
        out.println("<h2 class=\"hdr find\"><strong><a href=\"javascript:void(0);\" onclick=\"hideExpandDiv('findoutmore')\" >Find out more</a></strong></h2>");
        out.println("<ul id=\"findoutmore\" class=\"sch-list\" style=\"" + showHide + ";\" >");
        String collegetext = new CommonFunction().replaceHypen(new CommonFunction().replaceURL(String.valueOf(bean.getCollegeName()).toLowerCase()));
        if (bean.getAllCourseCount() != null && !bean.getAllCourseCount().equals("0")) {
          //Changed new all graduate courses url pettern, By Thiyagu G for 27_Jan_2016.          
          out.println("<li><a href=\"all-courses/csearch?university=" + collegetext + "\" title=\"View all courses at " + bean.getCollegeNameDisplay() + "\">View all " + bean.getCollegeNameDisplay() + " courses</a></li>");
        }
        // added for 10th March Release
        if (bean.getAccessFoundationCount() != null && !bean.getAccessFoundationCount().equals("0")) {
          out.println("<li><a href=\"" + contextPath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE + "access-foundation-university/all-access-foundation-courses-at-" + collegetext + "/t/" + bean.getCollegeId() + "/csearch.html\" title=\"View all " + bean.getCollegeNameDisplay() + " Access and Foundation courses at " + bean.getCollegeNameDisplay() + "\">View all Access and Foundation courses</a></li>");
        }
        if (bean.getFoundationCount() != null && !bean.getFoundationCount().equals("0")) {
          out.println("<li><a href=\"" + contextPath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE + "foundation-degree-university/all-foundation-degree-courses-at-" + collegetext + "/a/" + bean.getCollegeId() + "/csearch.html\" title=\"View all Foundation degree courses at " + bean.getCollegeNameDisplay() + "\">View all " + bean.getCollegeNameDisplay() + " Foundation degree courses</a></li>");
        }
        if (bean.getHncHndCount() != null && !bean.getHncHndCount().equals("0")) {
          out.println("<li><a href=\"" + contextPath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE + "hnd-hnc-university/all-hnd-hnc-courses-at-" + collegetext + "/n/" + bean.getCollegeId() + "/csearch.html\" title=\"View all HND/HNC courses at " + bean.getCollegeNameDisplay() + "\">View all " + bean.getCollegeNameDisplay() + " HND/HNC courses</a></li>");
        }
        // End of 10th March Release
        if (bean.getDegreeCourseCount() != null && !bean.getDegreeCourseCount().equals("0")) {
          out.println("<li><a href=\"" + contextPath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE + "degree-university/all-degree-courses-at-" + collegetext + "/m/" + bean.getCollegeId() + "/csearch.html\" title=\"View all undergraduate courses at " + bean.getCollegeNameDisplay() + "\">View all " + bean.getCollegeNameDisplay() + " undergraduate courses</a></li>");
        }
        if (bean.getPgCourseCount() != null && !bean.getPgCourseCount().equals("0")) {
          out.println("<li><a href=\"" + contextPath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE + "postgraduate-university/all-postgraduate-courses-at-" + collegetext + "/l/" + bean.getCollegeId() + "/csearch.html\" title=\"View all postgraduate courses at " + bean.getCollegeNameDisplay() + "\">View all " + bean.getCollegeNameDisplay() + " postgraduate courses</a></li>");
        }
        if (bean.getScholarshipCount() != null && !bean.getScholarshipCount().equals("0")) {
          out.println("<li><a href=\"" + contextPath + GlobalConstants.SEO_PATH_SCHOLARSHIP_PAGE + collegetext + "-scholarships-bursaries/m,n,a,t,l/" + bean.getCollegeId() + "/1/bursary-uni.html\" title=\"View all scholarships at " + bean.getCollegeNameDisplay() + "\">View all " + bean.getCollegeNameDisplay() + " scholarships</a></li>");
        }
        //out.println("<li><a  rel=\"nofollow\" href=\"" + contextPath + "/uniwrittenreviews.html?z=" + bean.getCollegeId() + "\" title=\"View all written reviews at " + bean.getCollegeNameDisplay() + "\">View all " + bean.getCollegeNameDisplay() + " written reviews</a></li>");
        out.println("<li><a href=\"" + contextPath + new CommonFunction().uniLandingReviewSEOUrl(bean.getCollegeId(), bean.getCollegeName())+ "\" title=\"View all written reviews at " + bean.getCollegeNameDisplay() + "\">View all " + bean.getCollegeNameDisplay() + " written reviews</a></li>");
        out.println("<li><a href=\"" + contextPath + "/college-videos/" + collegetext + "-videos/highest-rated/" + bean.getCollegeId() + "/1/student-videos.html\"  title=\"View all uni videos at " + bean.getCollegeNameDisplay() + "\">View all " + bean.getCollegeNameDisplay() + " uni videos</a></li>");
        out.println("</ul>");
       // out.println("</li>");
       // out.println("</ul>");
        out.println("</div>");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return EVAL_BODY_BUFFERED;
  }
  public int doAfterBody() throws JspException {
    try {
      BodyContent bodycontent = getBodyContent();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }
  public int doEndTag() {
    return EVAL_PAGE;
  }
  public void setShowHide(String showHide) {
    this.showHide = showHide;
  }
  public String getShowHide() {
    return showHide;
  }
}
