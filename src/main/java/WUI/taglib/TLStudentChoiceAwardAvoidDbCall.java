package WUI.taglib;

import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import WUI.homepage.form.HomePageBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

/**
 * will draw the StudentChoiceAwards, no DB call from taglib. it will fetch the award details from request.
 *
 * @since wu306_20110405
 * @author Mohamed Syed
 *
 */
public class TLStudentChoiceAwardAvoidDbCall extends BodyTagSupport {

  public TLStudentChoiceAwardAvoidDbCall() {
  }
  public ArrayList listOfStudentChoiceAwards = new ArrayList();

  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    String contextPath = request.getContextPath();
    listOfStudentChoiceAwards = (ArrayList)request.getAttribute("listOfStudentChoiceAwards");
    //
    try {
      JspWriter out = pageContext.getOut();
      if (listOfStudentChoiceAwards != null && listOfStudentChoiceAwards.size() > 0) {
        String awardsimage[] = new String[] { "OVERALL_10.png", "STUD-UNION_10.png", "CLUBS-SOCIETIES_10.png", "ACCOMMODATION_10.png", "UNI-FACILITY_10.png", "COURSE-LECTURER_10.png", "EYE-CANDY_10.png", "CITY-LIFE_10.png", "JOB-PROSPECTS_10.png" };
        /*
         *  4  Overall Rating
         *  5  Student Union
         *  6  Clubs and Societies
         *  7  Accommodation
         *  8  Uni Facilities
         *  9  Course and Lecturers
         *  10 Eye Candy
         *  11 City Life
         *  12 Job Prospects
         */
        int questionId = new CommonUtil().generateRandomNumber(4, 12);
        questionId = questionId < 4 || questionId > 12 ? 4 : questionId;
        out.println("<div class=\"award-blk\" id=\"mostReviewUni\"><a href=\""+new CommonFunction().getSchemeName(request)+"www.whatuni.com/degrees/uk-best-university-ranking/rankings-2009/universityrankings.html\" title=\"Student choice awards\" class=\"stud-award\"></a>");//Added getSchemeName by Indumathi Mar-29-16
        //out.println("<h2 class=\"awards\"><a href=\"" + contextPath + "/uk-best-university-ranking/rankings-2009/universityrankings.html\" title=\"Student Choice Awards 2009\">Student Choice Awards</a></h2>");
        out.println("<ul class=\"award-list\" type=\"1\" start=\"0\"><li class=\"award-head\">YOUR TOP 10</li>");
        //out.println("<li class=\"image\"><a href=\"" + contextPath + "/uk-best-university-ranking/rankings-2009/universityrankings.html\"><img class=\"logo\" src=\"" + contextPath + "/img/whatuni/awards/" + awardsimage[questionId - 4] + "\" alt=\"\" title=\"Student Choice Awards 2009\" /></a></li>");
        Iterator uniiterator = listOfStudentChoiceAwards.iterator();
        int count=1;
        while (uniiterator.hasNext()) {
          HomePageBean bean = (HomePageBean)uniiterator.next();
          if (bean.getQuestionId() != null && bean.getQuestionId().trim().equals(String.valueOf(questionId).trim())) {
            String seoCollegeName = new CommonFunction().replaceHypen(new CommonFunction().replaceURL(bean.getCollegeName())).replaceAll(" ", "-") + "-";
          //  out.println("<li><a href=\"" + contextPath + GlobalConstants.SEO_PATH_REVIEW_CATEGORY_PAGE + seoCollegeName + bean.getSeoQuestionText() + "-reviews/" + bean.getCollegeId() + "/" + bean.getQuestionId() + "/1/highest_rated/reviewcategory.html\"" + " title=\"" + bean.getCollegeNameDisplay() + "\">" + bean.getCollegeNameDisplay() + "</a></li>");
           if(count%2==0){
             out.println("<li><span>"+count+"</span>");
           }else{
             out.println("<li class=\"even\"><span>"+count+"</span>");
           }
              out.println("<a href=\"" + contextPath + GlobalConstants.SEO_PATH_REVIEW_CATEGORY_PAGE + seoCollegeName + bean.getSeoQuestionText() + "-reviews/" + bean.getCollegeId() + "/" + bean.getQuestionId() + "/1/highest_rated/reviewcategory.html\"" + " title=\"" + bean.getCollegeNameDisplay() + "\">" + bean.getCollegeNameDisplay() + "</a></li>");
              count++;
          }
        }
        out.println("</ul> ");
        out.println("<div class=\"award-btm\" class=\"center_txt\"><div></div><p>The What Uni Student Awards are based on what students say about their uni.</p><a class=\"sblue\" href=\"" + contextPath + "/uk-best-university-ranking/rankings-2009/universityrankings.html\" title=\"View all award results\">Check out all award results &gt;</a></div>");
        out.println("</div>");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return EVAL_BODY_BUFFERED;
  }

  public int doAfterBody() throws JspException {
    try {
      BodyContent bodycontent = getBodyContent();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }

  public int doEndTag() {
    return EVAL_PAGE;
  }

}
