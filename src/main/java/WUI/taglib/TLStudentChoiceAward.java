package WUI.taglib;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.validator.GenericValidator;

import WUI.homepage.form.HomePageBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.wuni.util.sql.DataModel;

/**
  * @TLMostReviewUni.java
  * @Version : 2.0
  * @September 20 2007
  * @www.whatuni.com
  * @Created By : Balraj. Selva Kumar
  * @Purpose  : Display the random students awards pod.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class TLStudentChoiceAward extends BodyTagSupport {
  public TLStudentChoiceAward() {
  }
  public static final List studentsawardspodlist = new ArrayList();
  static {
    DataModel dataModel = new DataModel();
    HomePageBean bean = new HomePageBean();
    ResultSet resultset = null;
    Vector vector = new Vector();
    CommonFunction common = new CommonFunction();
    String year =    common.getWUSysVarValue("CURRENT_YEAR_AWARDS");
    vector.add(year);
    try {
      resultset = dataModel.getResultSet("wu_reviews_pkg.get_student_awards_fn", vector);
      while (resultset.next()) {
        bean = new HomePageBean();
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setCollegeNameDisplay(resultset.getString("college_name_display"));
        bean.setQuestionTitle(resultset.getString("question_title"));
        bean.setRating(resultset.getString("rating"));
        String questionTitle = resultset.getString("question_title");
        String seoText = new CommonFunction().seoQuestionTitle(questionTitle);
        bean.setSeoQuestionText(seoText);
        bean.setQuestionId(resultset.getString("question_id"));
        studentsawardspodlist.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
  }
  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    String contextPath = request.getContextPath();
    try {
      JspWriter out = pageContext.getOut();
      if (studentsawardspodlist != null && studentsawardspodlist.size() > 0) {
        String awardsimage[] = new String[] { "OVERALL_10.png", "STUD-UNION_10.png", "CLUBS-SOCIETIES_10.png", 
                                              "ACCOMMODATION_10.png", "UNI-FACILITY_10.png", "COURSE-LECTURER_10.png", 
                                              "EYE-CANDY_10.png", "CITY-LIFE_10.png", "JOB-PROSPECTS_10.png" };
        /*4  Overall Rating
                 5  Student Union
                 6  Clubs and Societies
                 7  Accommodation
                 8  Uni Facilities
                 9  Course and Lecturers
                 10 Eye Candy
                 11 City Life
                 12 Job Prospects*/
        int questionId = new CommonUtil().generateRandomNumber(4, 12);
        //questionId = questionId < 4 || questionId > 12? 4: questionId;
         questionId = 4;
        //out.println("<div class=\"mostreviews\"  id=\"mostReviewUni\">");
         out.println("<div class=\"award-blk\"><a href=\""+new CommonFunction().getSchemeName(request)+"www.whatuni.com/student-awards-winners/2014.html\" title=\"Student choice awards\" class=\"stud-award\"><span class=\"staw-hrd\">your top 10 universities</span></a><ul class=\"award-list\">");//Added getSchemeName by Indumathi Mar-29-16
      /*   
        out.println("<h2 class=\"awards\"><a href=\"" + contextPath + "/uk-best-university-ranking/rankings-2009/universityrankings.html\" title=\"Student Choice Awards 2009\">Student Choice Awards</a></h2>");
        out.println("<ol type=\"1\" start=\"0\">");
        out.println("<li class=\"image\"><a href=\"" + contextPath + "/uk-best-university-ranking/rankings-2009/universityrankings.html\"><img class=\"logo\" src=\"" + contextPath + "/img/whatuni/awards/" + awardsimage[questionId - 4] + "\" alt=\"\" title=\"Student Choice Awards 2009\" /></a></li>");
        */
        Iterator uniiterator = studentsawardspodlist.iterator();
        int count=1;
        while (uniiterator.hasNext()) {
          HomePageBean bean = (HomePageBean)uniiterator.next();
          if (bean.getQuestionId() != null && bean.getQuestionId().trim().equals(String.valueOf(questionId).trim())) {
            if(!GenericValidator.isBlankOrNull(bean.getCollegeName())){
            String seoCollegeName = new CommonFunction().replaceHypen(new CommonFunction().replaceURL(bean.getCollegeName())).replaceAll(" ", "-") + "-";
        //    out.println("<li><a href=\"" + contextPath + GlobalConstants.SEO_PATH_REVIEW_CATEGORY_PAGE + seoCollegeName + bean.getSeoQuestionText() + "-reviews/" + bean.getCollegeId() + "/" + bean.getQuestionId() + "/1/highest_rated/reviewcategory.html\"" + " title=\"" + bean.getCollegeNameDisplay() + "\">" + bean.getCollegeNameDisplay() + "</a></li>");
   
         if(count%2==0){
           out.println("<li><span><a href=\"\">"+count+"</a></span>");
         }else{
           out.println("<li class=\"even\"><span><a href=\"\">"+count+"</a></span>");
                }
                //24-JUN-2014
                //out.println("<a href=\"" + contextPath + GlobalConstants.SEO_PATH_REVIEW_CATEGORY_PAGE + seoCollegeName + bean.getSeoQuestionText() + "-reviews/" + bean.getCollegeId() + "/" + bean.getQuestionId() + "/1/highest_rated/reviewcategory.html\"" + " title=\"" + bean.getCollegeNameDisplay() + "\">" + bean.getCollegeNameDisplay() + "</a></li>");
                out.println("<a href=\"" + GlobalConstants.SEO_PATH_NEW_UNILANDING_PAGE + seoCollegeName +"/"+ bean.getCollegeId() + "/\"" + " title=\"" + bean.getCollegeNameDisplay() + "\">" + bean.getCollegeNameDisplay() + "</a></li>");                
            }
            count++;
          }
            
        }
        out.println("</ul>");
      /*  out.println("</ol> ");
        out.println("<p class=\"center_txt\"><a href=\"" + contextPath + "/uk-best-university-ranking/rankings-2009/universityrankings.html\" title=\"View all award results\">View all award results</a></p>");
        out.println("</div>");
         <div class="award-btm">
								<div></div>
								<p>The What Uni Student Awards are based on what students say about their uni.</p>
								<p><a class="sblue" href="">Check out all award results &gt;</a></p>
							</div>
        */
       out.println("<div class=\"award-btm\">");
       out.println("<p>The What Uni Student Awards are based on what students say about their uni.</p>");
       out.println("<p><a class=\"sblue\" href=\"" +  "/student-awards-winners/2014.html\" title=\"View all award results\">Check out all award results &gt;</a></p>");
        out.println("</div></div>"); 
       
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return EVAL_BODY_BUFFERED;
  }
  public int doAfterBody() throws JspException {
    try {
      BodyContent bodycontent = getBodyContent();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }
  public int doEndTag() {
    return EVAL_PAGE;
  }
}
