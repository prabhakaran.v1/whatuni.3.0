package WUI.taglib;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import spring.util.GlobalMethods;
import WUI.homepage.form.HomePageBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

/**
  * @TLMostReviewUni.java
  * @Version : 2.0
  * @September 20 2007
  * @www.whatuni.com
  * @Created By : Balraj. Selva Kumar
  * @Purpose  : Display the common request prospectus for the provider.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class TLOrderProspectus extends BodyTagSupport {
  public TLOrderProspectus() {
  }
  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    String contextPath = request.getContextPath();
    try {
      JspWriter out = pageContext.getOut();
      List orderProspectusUniList = new CommonFunction().getOrderProspectusUniList(request);
      if (orderProspectusUniList != null && orderProspectusUniList.size() > 0) {
        request.setAttribute("orderProspectusUniList", orderProspectusUniList);
      }
      if (orderProspectusUniList != null && orderProspectusUniList.size() > 0) {
        out.println("<div class=\"ord_pros\">");
        out.println("<div class=\"content\">");
        out.println("<center><h4>Popular uni's</h4></center>");
        out.println("<center class=\"order_pros\">Get their prospectuses now!</center>");
        out.println("<hr />");
        Iterator uniiterator = orderProspectusUniList.iterator();
        while (uniiterator.hasNext()) {
          HomePageBean bean = (HomePageBean)uniiterator.next();
          out.println("<p>");
          out.println("<input type=\"checkbox\" class=\"checkbox\" name=\"opcid\" value=\"" + bean.getCollegeId() + "\" checked=\"true\" />");
          out.println("<span>");
          out.println("<a href=\"" + new GlobalMethods().changeSeoURLLink("unilanding", bean.getCollegeName() + GlobalConstants.SEO_URL_SPLIT_CHAR + bean.getCollegeId()) + "\" title=\"" + bean.getCollegeName() + "\">" + bean.getCollegeName() + "</a>");
          out.println("</span>");
          out.println("</p>");
        }
        out.println("</div>");
        out.println("</div>");
        out.println("<center> <input type=\"image\" src=\"" + request.getContextPath() + "/img/whatuni/backgrounds/ord_pros_go.png\" />");
        out.println("<input type=\"hidden\" id=\"reqcontextpath\" name=\"reqcontextpath\" value=\"" + request.getContextPath() + "\"></center>");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return EVAL_BODY_BUFFERED;
  }
  public int doAfterBody() throws JspException {
    try {
      BodyContent bodycontent = getBodyContent();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }
  public int doEndTag() {
    return EVAL_PAGE;
  }
}
