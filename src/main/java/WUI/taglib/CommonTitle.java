package WUI.taglib;

import java.sql.ResultSet;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import WUI.utilities.CommonFunction;

import com.wuni.util.sql.DataModel;

/**
  * @CommonTitle.java
  * @Version : 2.0
  * @www.whatuni.com
  * @Purpose  : Tag library file for the Common HTML Title for all the JSP page(non SEO).
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class CommonTitle extends BodyTagSupport {
  public CommonTitle() {
  }
  private String pagename3 = "";
  private String homepageFlag ;
  private String affliateId = "";
  private String indexFollowFlag = "";
  private String[] indexPages = { "aboutReviewRating.jsp", "uniVideoReview.jsp", "reviewHome.jsp", "searchHome.jsp", 
                                  "googleMap.jsp", "County.jsp", "seoCounty.jsp", "subjectUniList.jsp", 
                                  "subjectProfileList.jsp", "messagePage.jsp", "uniGeneralReview.jsp", "uniVideoReview.jsp", 
                                  "contactUs.jsp", "membersFriend.jsp", "membersVideo.jsp", "membersPhoto.jsp", 
                                  "searchUniCollege.jsp", "comparisonInfo.jsp", "uniStreetView.jsp","uniImageSlideShow.jsp","musictrack.jsp","clearinghome.jsp",
                                  "qlbasicform.jsp", "membersProfile.jsp", "cookies.jsp", "aboutUs.jsp", "schoolVisitsPage.jsp", "orderDownloadPage.jsp", "mywhatuni.jsp", "prospectussentsuccess.jsp", "downloadprospectussentsuccess.jsp", "resetpassword.jsp",
                                  "forgotPassword.jsp", "loginPage.jsp",  "error.jsp", "subjectCourseSearch.jsp", "showReviewDetail.jsp", "subjectProfile.jsp","mobileLayout.jsp", "error404.jsp", "mysettings.jsp", "uploadedimage.jsp", "compareBasket.jsp","newProviderHome.jsp",
                                  "overviewPage.jsp"}; // noIndex, follow modified for 29-OCT-2031_REL
  private String[] noFollowPages = { "searchMembers.jsp", "addReview.jsp", "addLimeLightVideo.jsp", "myUniversityBasket.jsp", 
                                     "viewProfile.jsp", "prospectus.jsp", "userRegistration.jsp", 
                                     "uniProfile.jsp", "genericError.jsp", 
                                     "subjectSearchLanding.jsp", "moreReviews.jsp", "listReview.jsp", 
                                     "listVideoReview.jsp", "myVideoProfile.jsp", "courseReview.jsp", "searchGVReview.jsp", 
                                     "basketHome.jsp", "courseContact.jsp", "searchGeneralReview.jsp", 
                                     "searchVideoReview.jsp",  "searchMemberResult.jsp", 
                                     "relatedMembersList.jsp", "viewReview.jsp", "compose.jsp", "inBox.jsp", "deleteBox.jsp", 
                                     "draftBox.jsp", "friendsBox.jsp", "sentBox.jsp", "viewMessage.jsp", "collegeEmail.jsp", 
                                     "membersMap.jsp", "collegeProspectus.jsp", "collegeEmail.jsp", 
                                     "collegeProspectusSuccess.jsp", "collegeEmailSuccess.jsp", "UniPhotoGallery.jsp", 
                                     "compareCourseBasket.jsp", "myCourseBasket.jsp", "addWidgetReview.jsp" }; // noindex, nofollow modified for 29-OCT-2031_REL

  /**
    *   This method is used to set affiliateid.
    * @param affliateId
    * @return none.
    */
  public void setAffliateId(String affliateId) {
    this.affliateId = affliateId;
  }

  /**
    *   This method is used to get affiliateid.
    * @return affiliateid as String.
    */
  public String getAffliateId() {
    return affliateId;
  }

  public String getHomepageFlag() {
	return homepageFlag;
  }

  public void setHomepageFlag(String homepageFlag) {
	this.homepageFlag = homepageFlag;
  }

/**
    *   This function is used to print the HTML title for the non SEO related JSP pages.
    * @return Print status as int.
    * @throws JspException
    */
  public int doStartTag() throws JspException {
    DataModel dataModel = new DataModel();
    ResultSet resultSet = null;
    CommonFunction commonFunction = new CommonFunction();
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    int stind1 = request.getRequestURI().lastIndexOf("/");
    int len1 = request.getRequestURI().length();
    String page_name = pagename3 != null && pagename3 != "" ? pagename3 :  request.getRequestURI().substring(stind1 + 1, len1);
   
    boolean checkIndex = true;
    StringBuffer seoKeyWord = new StringBuffer("");
    Vector vector = new Vector();
    vector.clear();
    try {
      JspWriter out = pageContext.getOut();
      vector.add(affliateId);
      resultSet = dataModel.getResultSet("Wu_seo_pkg.get_common_title_fn", vector);
      while (resultSet.next()) {
        if("error.jsp".equalsIgnoreCase(page_name)){
          seoKeyWord.append("<title>Page not found</title> \n ");
        }else{
          seoKeyWord.append("<title>" + resultSet.getString("meta_title") + "</title> \n ");
        }
        seoKeyWord.append("<meta name=\"description\" content=\"" + resultSet.getString("meta_desc") + "\" /> \n ");
        if(resultSet.getString("meta_keyword")!=null && !"null".equals(resultSet.getString("meta_keyword")) && !"".equals(resultSet.getString("meta_keyword")))
        {
        seoKeyWord.append("<meta name=\"keywords\" content=\"" + resultSet.getString("meta_keyword") + "\" /> \n");
        }else{
          seoKeyWord.append("<meta name=\"keywords\" content=\""+"\"/>\n");
        }
        for (int i = 0; i < indexPages.length; i++) {
          if (page_name != null && page_name.equalsIgnoreCase(indexPages[i])) {
            if (indexFollowFlag != null && indexFollowFlag.trim().length() > 0) {
              seoKeyWord.append("<meta name=\"ROBOTS\" content=\"" + indexFollowFlag + "\" /> \n");
              checkIndex = false;
              break;
            } else {
              seoKeyWord.append("<meta name=\"ROBOTS\" content=\"noindex, follow\" /> \n");
              checkIndex = false;
              break;
            }
          }
        }
        for (int i = 0; i < noFollowPages.length; i++) {
          if (page_name != null && page_name.equalsIgnoreCase(noFollowPages[i])) {
            seoKeyWord.append("<meta name=\"ROBOTS\" content=\"noindex,nofollow\" /> \n");
            checkIndex = false;
            break;
          }
        }
        if (checkIndex) {
          if(request.getServerName().indexOf("mtest.whatuni.com") > -1 &&  ("home.jsp".equalsIgnoreCase(page_name) || "newUser.jsp".equalsIgnoreCase(page_name) || "retUser.jsp".equalsIgnoreCase(page_name) || "loggedIn.jsp".equalsIgnoreCase(page_name))){
            seoKeyWord.append("<meta name=\"ROBOTS\" content=\"noindex,nofollow\" /> \n");
          }else{
            seoKeyWord.append("<meta name=\"ROBOTS\" content=\"" + resultSet.getString("robots") + "\" /> \n");
          }
        }
        seoKeyWord.append("<meta name=\"content-type\" content=\"" + resultSet.getString("content_type") + "\" /> \n");
        if("Y".equalsIgnoreCase(homepageFlag)){
        
          seoKeyWord.append("<meta property=\"fb:app_id\" content=\"617249984971742\"/>\n");
          seoKeyWord.append("<meta property=\"og:title\" content=\"" + resultSet.getString("meta_title") + "\" /> \n");
          seoKeyWord.append("<meta property=\"og:type\" content=\"website\" /> \n");
          seoKeyWord.append("<meta property=\"og:description\" content=\"" + resultSet.getString("meta_desc") + "\" /> \n");
          seoKeyWord.append("<meta property=\"og:image\" content=\"" + commonFunction.getSchemeName(request) +"www.whatuni.com/wu-cont/images/logo_print.png\" /> \n");//Added getSchemeName by Indumathi Mar-29-16
          seoKeyWord.append("<meta property=\"og:url\" content=\"" + commonFunction.getSchemeName(request) + "www.whatuni.com/\" /> \n");//Added getSchemeName by Indumathi Mar-29-16
          
          seoKeyWord.append("<meta name=\"twitter:card\" value=\"summary\" content=\"summary\" /> \n");
          seoKeyWord.append("<meta name=\"twitter:creator\" value=\"@whatuni\" content=\"@whatuni\" /> \n");
          seoKeyWord.append("<meta name=\"twitter:url\" value=\"" + commonFunction.getSchemeName(request) + "www.whatuni.com/\" content=\"" + commonFunction.getSchemeName(request) +"www.whatuni.com/\" /> \n");//Added getSchemeName by Indumathi Mar-29-16
          seoKeyWord.append("<meta name=\"twitter:title\" value=\"" + resultSet.getString("meta_title") + "\" content=\""+ resultSet.getString("meta_title") +"\" /> \n");
          seoKeyWord.append("<meta name=\"twitter:description\" value=\"" + resultSet.getString("meta_desc") + "\" content=\"" +resultSet.getString("meta_desc")+ "\" /> \n");
          seoKeyWord.append("<meta name=\"twitter:image\" content=\"" + commonFunction.getSchemeName(request) + "www.whatuni.com/wu-cont/images/logo_print.png\" /> \n");//Added getSchemeName by Indumathi Mar-29-16
        }
        request.setAttribute("hitbox_titletag", resultSet.getString("meta_title"));
      }
      dataModel.closeCursor(resultSet);
      out.print(seoKeyWord.toString());
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return EVAL_BODY_BUFFERED;
  }

  /**
    *   This Function is used to run after the body content.
    * @return status as int
    * @throws JspException
    */
  public int doAfterBody() throws JspException {
    try {
      BodyContent bodycontent = getBodyContent();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }

  /**
    *   This function is used for end tag.
    * @return status as int.
    */
  public int doEndTag() {
    return EVAL_PAGE;
  }

  /**
   *   This method is used to reset the affiliateid value.
   * @return none.
   */
  public void release() {
    affliateId = null;
  }

  /**
    *   This function is used ti set the index,follow flag.
    * @param indexFollowFlag
    * @return none.
    */
  public void setIndexFollowFlag(String indexFollowFlag) {
    this.indexFollowFlag = indexFollowFlag;
  }

  /**
    *   This function is used to get the index, follow flag.
    * @return index,follow as String.
    */
  public String getIndexFollowFlag() {
    return indexFollowFlag;
  }

  public String getPagename3() {
	return pagename3;
  }

  public void setPagename3(String pagename3) {
	this.pagename3 = pagename3;
  }
}
