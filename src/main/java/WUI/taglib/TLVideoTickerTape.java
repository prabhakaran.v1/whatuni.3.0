package WUI.taglib;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import spring.form.VideoReviewListBean;
import spring.util.GlobalMethods;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;

public class TLVideoTickerTape extends BodyTagSupport {
  public TLVideoTickerTape() {
  }

  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    String contextPath = request.getContextPath();
      String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");   
      if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0){ 
        cpeQualificationNetworkId = "2";
      }request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);
    Vector vector = new Vector();
    vector.clear();
    try {
      JspWriter out = pageContext.getOut();
      out.println("<div class=\"you-might-blk\">");
      out.println("<h2 class=\"hs-1 fea-uni\"><strong>Featured universities</strong></h2>");      
       List homeVideoList = new GlobalFunction().loadHomeVideo();
       ArrayList tickerVideoList = new GlobalFunction().loadTickerVideo();
       request.setAttribute("showlightbox","YES");
       if(homeVideoList != null && homeVideoList.size() >0){
           for(int i=0;i<homeVideoList.size();i++){
               VideoReviewListBean vidBean = (VideoReviewListBean)homeVideoList.get(i);
               if(i==0){
                 out.println("<h3 class=\"hs-4\"><strong><a href=\"" + new GlobalMethods().changeSeoURLLink("unilanding", vidBean.getCollegeName() + GlobalConstants.SEO_URL_SPLIT_CHAR + vidBean.getCollegeId()) + "\" title=\"" + vidBean.getCollegeNameDisplay() + "\">" + vidBean.getCollegeNameDisplay() + "</a></strong></h3>");
               }
               out.println("<div class=\"ivideo\">");
               out.println("<a rel=\"nofollow\" onclick=\"return showVideo('" + vidBean.getMediaId() + "','" + vidBean.getCollegeId() + "', 'B');\" title='" + vidBean.getVideoReviewTitle() + "' class=\"vhold\" ><span class=\"vplay2\"><img src=\"" +CommonUtil.getImgPath("",0) + "/wu-cont/images/playvideo.png\" /></span><img src=\"" + vidBean.getThumbnailUrl() + "\"  width=\"205\" height=\"160\" class=\"bigimg\" alt=\"\" title=\"\" /></a> ");
               if (tickerVideoList != null && tickerVideoList.size() > 0) {
                  String userAgent = request.getHeader( "User-Agent" );
                  boolean isFirefox = ( userAgent != null && userAgent.indexOf( "Firefox/" ) != -1 );
                  boolean isMSIE = ( userAgent != null && userAgent.indexOf( "MSIE" ) != -1 );
                 out.println("<script language=\"javascript\" type=\"text/javascript\">");
                 out.println("var sectionIndex = parseInt(0);var center = 210;var sectionWidth = 100;var sectionCount = parseInt(" + tickerVideoList.size() + ");");
                 out.println("</script>");
                // out.println("<script type=\"text/javascript\" language=\"javascript\" src=\"" + CommonUtil.getJsPath() + "/js/slideshow/thumb-repeat-scroller.js\"></script>");
                 out.println("</div></div>");
                 out.println("<div class=\"more-video mvd\"><div class=\"fvideo-top\">");
                 /*if(isFirefox){
                     out.println("<div id=\"arrow_left\">" + "<img src=\"" + request.getContextPath() + "/img/whatuni/icons/prev_arrow.gif\" onmouseout=\"zxcCngDirection('scrollmain',-1, -3);setDirection('left');\" onmouseover=\"zxcCngDirection('scrollmain',-1, -6);setDirection('left');\"></div>");
                     out.println("<span class=\"moretitle\">More Uni Videos</span>");
                     out.println("<div id=\"arrow_right\"><img src=\"" + request.getContextPath() + "/img/whatuni/icons/next_arrow.gif\" onmouseout=\"zxcCngDirection('scrollmain',1, 3);setDirection('right');\" onmouseover=\"zxcCngDirection('scrollmain',1, 6);setDirection('right');\"></div>");
                 }else{
                   out.println("<div id=\"arrow_left\">" + "<img src=\"" + request.getContextPath() + "/img/whatuni/icons/prev_arrow.gif\" onmouseout=\"zxcCngDirection('scrollmain',-1, -1);setDirection('left');\" onmouseover=\"zxcCngDirection('scrollmain',-1, -3);setDirection('left');\"></div>");
                   out.println("<span class=\"moretitle\">More Uni Videos</span>");
                   out.println("<div id=\"arrow_right\"><img src=\"" + request.getContextPath() + "/img/whatuni/icons/next_arrow.gif\" onmouseout=\"zxcCngDirection('scrollmain',1, 1);setDirection('right');\" onmouseover=\"zxcCngDirection('scrollmain',1, 3);setDirection('right');\"></div>");
                 }
                  out.println("<span class=\"fright\">");
                   out.println("<a onmouseout=\"zxcCngDirection('scrollmain',-1, -3);setDirection('left');\" onmouseover=\"zxcCngDirection('scrollmain',-1, -6);setDirection('left');\">Next</a>");
                   out.println("&nbsp; | &nbsp;");
                   out.println("<a onmouseout=\"zxcCngDirection('scrollmain',1, 3);setDirection('right');\" onmouseover=\"zxcCngDirection('scrollmain',1, 6);setDirection('right');\">Previous</a>");
                   out.println("</span>");*/
                   out.println("<strong>More Uni Videos</strong>");
                   out.println("</div>");
                   out.println("<div class=\"video-scroller\">");
                 
                 for (int j = 0; j < tickerVideoList.size(); j++) {
                   VideoReviewListBean tickerVidBean = (VideoReviewListBean)tickerVideoList.get(j);
                   if(j == 0){out.println("<div class=\"plzr\">");}
                   else {out.println("<div>");}
                   out.println("<a class=\"vhold\" onclick=\"return showVideo('" + tickerVidBean.getVideoReviewId() + "','" + tickerVidBean.getCollegeId() + "');\"><span class=\"vplay3\"><img src=\"" + CommonUtil.getImgPath("",0) + "/wu-cont/images/playvideo.png\" /></span><img class=\"scrollimg\" width=\"88\" height=\"88\"  src='" + tickerVidBean.getThumbnailUrl() + "' /></a></a></div>");
                 }
                 
                 out.println("</div>");
                 out.println("<input type=\"hidden\" name=\"currentdirection\" id=\"currentdirection\" value=\"left\" />");
                 out.println("<script language=\"javascript\" type=\"text/javascript\">");
                 if(isFirefox){
                     out.println("InitScripts('scrollmain'); justifySelectedSection('scrollmain');zxcCngDirection('scrollmain',1, -3);");
                 }else{
                     out.println("InitScripts('scrollmain'); justifySelectedSection('scrollmain');zxcCngDirection('scrollmain',1, -1);");
                 }
                 out.println("</script>");
               }
               out.println("</div>");
             }
         }
      } catch (Exception exception) {
      exception.printStackTrace();
    }
    return EVAL_BODY_BUFFERED;
  }
  public int doAfterBody() throws JspException {
    try {
      BodyContent bodycontent = getBodyContent();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }
  public int doEndTag() {
    return EVAL_PAGE;
  }
}
