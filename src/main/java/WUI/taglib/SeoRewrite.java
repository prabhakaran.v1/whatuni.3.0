package WUI.taglib;

import java.sql.ResultSet;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import com.wuni.util.sql.DataModel;

/**
  * @SeoRewrite.java
  * @Version : 2.0
  * @www.whatuni.com
  * @Purpose  : Tag library file for the Common  Title for all the SEO related JSP page.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class SeoRewrite extends BodyTagSupport {

  public SeoRewrite() {
  }
  private String collegeId = "";
  private String courseId = "";
  private String paramFlag = "";
  private String sessionId = "";
  private String htmlPageId = "";
  private String pageName = "";
  private String affliateId = "";
  private String htmlTagName = "";
  private String studyLevelId = "";
  private String courseSubjectId = "";
  private String locationCountyId = "";
  private String categoryCode = "";
  private String entityText = "";
  private String searchKeyword = "";
  private String noIndexFollow = "";
  private String scholarshipId = "";
  private String pageNo = "";
  private String metaDesc = "";
  private String articleId = "";    
  private String artParentName = "";
  private String artSubName = "";
  private String awardYear = "";
  private String categoryName = "";
  private String totalProviderCount = "";
  private String totalCourseCount = "";
  
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setHtmlPageId(String htmlPageId) {
    this.htmlPageId = htmlPageId;
  }

  public String getHtmlPageId() {
    return htmlPageId;
  }

  public void setPageName(String pageName) {
    this.pageName = pageName;
  }

  public String getPageName() {
    return pageName;
  }

  public void setAffliateId(String affliateId) {
    this.affliateId = affliateId;
  }

  public String getAffliateId() {
    return affliateId;
  }

  public void setHtmlTagName(String htmlTagName) {
    this.htmlTagName = htmlTagName;
  }

  public String getHtmlTagName() {
    return htmlTagName;
  }

  public String getHtmlTag() {
    return "<" + htmlTagName + ">";
  }

  public String endHtmlTag() {
    return "</" + htmlTagName + ">";
  }  

  public String getTotalProviderCount() {
	return totalProviderCount;
	}
	
	public void setTotalProviderCount(String totalProviderCount) {
		this.totalProviderCount = totalProviderCount;
	}
	
	public String getTotalCourseCount() {
		return totalCourseCount;
	}

	public void setTotalCourseCount(String totalCourseCount) {
		this.totalCourseCount = totalCourseCount;
	}

/**
    *   This function is used to print the SEO title for the SEO related pages.
    * @return status as int.
    * @throws JspException
    */
  public int doStartTag() throws JspException {
    DataModel dataModel = new DataModel();
    ResultSet resultSet = null;
    StringBuffer seoKeyWord = new StringBuffer("");
    ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    Vector vector = new Vector();
    vector.clear();
    try {
      JspWriter out = pageContext.getOut();
      String pageNumber = pageNo != null && !pageNo.equals("1") ? pageNo : "";
      if (request.getAttribute("addPageOneForUcas") != null) {
        pageNumber = pageNo;
      }
      if ((collegeId != null && collegeId.trim().length() > 0) && (affliateId != null && affliateId.trim().length() > 0) && (pageName != null && pageName.trim().length() > 0)) {
        vector.add(collegeId);
        vector.add(courseId);
        vector.add(sessionId);
        vector.add(affliateId);
        vector.add(pageName); ///////////key parameter
        vector.add(studyLevelId);
        vector.add(courseSubjectId);
        vector.add(locationCountyId);
        vector.add(categoryCode);
        vector.add(paramFlag);
        vector.add(entityText);
        vector.add(searchKeyword);
        vector.add(scholarshipId);
        vector.add(pageNumber);
        vector.add(metaDesc);
        vector.add(articleId);
        vector.add(artParentName);
        vector.add(artSubName);
        vector.add(awardYear);
        vector.add(categoryName);
        vector.add(totalProviderCount);
        vector.add(totalCourseCount);
        //
        resultSet = dataModel.getResultSet("wu_seo_pkg.wu_seo_title_fn", vector);
        //
        while (resultSet.next()) {
          seoKeyWord.append("<title>" + resultSet.getString("meta_title") + "</title> \n ");
          seoKeyWord.append("<meta name=\"description\" content=\"" + resultSet.getString("meta_desc") + "\"/> \n ");
          /*if(resultSet.getString("meta_keyword") != null){
              seoKeyWord.append("<meta name=\"keywords\" content=\"" + resultSet.getString("meta_keyword") + "\"/> \n");    
          }else{
              seoKeyWord.append("<meta name=\"keywords\" content=\"\"/> \n");
          }*///Commented based on Neils suggestion to remove meta keyowrd on 5-DEC-2031 Call
          //
          request.setAttribute("currentPageMetaTitle", resultSet.getString("meta_title"));
          
          if (noIndexFollow != null && noIndexFollow.equalsIgnoreCase("index,follow")) {
            seoKeyWord.append(bundle.getString("wuni.seo.metatag.details"));
          } else if (noIndexFollow != null && noIndexFollow.equalsIgnoreCase("noindex,follow")) { // added for 16th December Release 
            seoKeyWord.append(bundle.getString("wuni.seo.metatag.noindex.follow.details"));
          } else if (noIndexFollow != null && noIndexFollow.equalsIgnoreCase("noindex,nofollow")) {
            seoKeyWord.append(bundle.getString("wuni.seo.metatag.noindex.nofollow.details"));
          } else if (noIndexFollow != null && noIndexFollow.equalsIgnoreCase("index,nofollow")) { //added for 29th May 2099 release 
            seoKeyWord.append(bundle.getString("wuni.seo.metatag.index.nofollow.details"));
          } else if (noIndexFollow != null && noIndexFollow.equalsIgnoreCase("NOFORMFOLLOW")) { //added for 12th May 2009 release 
            seoKeyWord.append(bundle.getString("wuni.seo.metatag.noformfollow.details"));
          } else if ((noIndexFollow != null && noIndexFollow.equalsIgnoreCase("true")) && (pageName != null && (pageName.equalsIgnoreCase("COLLEGE RESULTS") || pageName.equalsIgnoreCase("COURSE RESULTS") || pageName.equalsIgnoreCase("COLLEGE PROFILE")))) {
            seoKeyWord.append(bundle.getString("wuni.seo.metatag.noindex.details"));
          } else {
            String metaRobots = resultSet.getString("meta_robo");
            if(metaRobots != null && metaRobots.length() > 0){
              seoKeyWord.append("<meta name=\"ROBOTS\" content=\"" + metaRobots + "\"/> \n <meta name=\"country\" content=\"United Kingdom\"/> \n");    
            }else{
              seoKeyWord.append(bundle.getString("wuni.seo.metatag.details"));
            }
          }
          /* added for Hitbox code 02. March 2009 for 10th March Release */
          request.setAttribute("hitbox_titletag", resultSet.getString("meta_title"));
          if (locationCountyId != null && !locationCountyId.equalsIgnoreCase("null") && locationCountyId.trim().length() > 0) {
            request.setAttribute("hitbox_location", locationCountyId);
          }
          if (resultSet.getString("study_desc") != null && resultSet.getString("study_desc").trim().length() > 0) {
            request.setAttribute("hitbox_qualification", resultSet.getString("study_desc"));
          }
          if (searchKeyword != null && !searchKeyword.equalsIgnoreCase("null") && searchKeyword.trim().length() > 0) {
            request.setAttribute("hitbox_keyword", searchKeyword);
            if (request.getAttribute("keyword_search_prob42") != null) {
              request.setAttribute("sc_prob42_subject", searchKeyword);
            }
          }
          if (resultSet.getString("category_desc") != null && resultSet.getString("category_desc").trim().length() > 0) {
            request.setAttribute("hitbox_keyword", resultSet.getString("category_desc"));
            if (request.getAttribute("keyword_search_prob42") != null) {
              request.setAttribute("sc_prob42_subject", resultSet.getString("category_desc"));
            }
          }
          /* added for Hitbox code */
        }
        dataModel.closeCursor(resultSet);
        out.print(seoKeyWord.toString());
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultSet);
    }
    return EVAL_BODY_BUFFERED;
  }

  public int doAfterBody() throws JspException {
    try {
      BodyContent bodycontent = getBodyContent();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }

  public int doEndTag() {
    return EVAL_PAGE;
  }

  public void release() {
    sessionId = null;
    collegeId = null;
    affliateId = null;
    pageName = null;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setParamFlag(String paramFlag) {
    this.paramFlag = paramFlag;
  }

  public String getParamFlag() {
    return paramFlag;
  }

  public void setStudyLevelId(String studyLevelId) {
    this.studyLevelId = studyLevelId;
  }

  public String getStudyLevelId() {
    return studyLevelId;
  }

  public void setCourseSubjectId(String courseSubjectId) {
    this.courseSubjectId = courseSubjectId;
  }

  public String getCourseSubjectId() {
    return courseSubjectId;
  }

  public void setLocationCountyId(String locationCountyId) {
    this.locationCountyId = locationCountyId;
  }

  public String getLocationCountyId() {
    return locationCountyId;
  }

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setEntityText(String entityText) {
    this.entityText = entityText;
  }

  public String getEntityText() {
    return entityText;
  }

  public void setNoIndexFollow(String noIndexFollow) {
    this.noIndexFollow = noIndexFollow;
  }

  public String getNoIndexFollow() {
    return noIndexFollow;
  }

  public void setSearchKeyword(String searchKeyword) {
    this.searchKeyword = searchKeyword;
  }

  public String getSearchKeyword() {
    return searchKeyword;
  }

  public void setScholarshipId(String scholarshipId) {
    this.scholarshipId = scholarshipId;
  }

  public String getScholarshipId() {
    return scholarshipId;
  }

  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }

  public String getPageNo() {
    return pageNo;
  }
  public void setMetaDesc(String metaDesc) {
    this.metaDesc = metaDesc;
  }
  public String getMetaDesc() {
    return metaDesc;
  }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArtParentName(String artParentName) {
        this.artParentName = artParentName;
    }

    public String getArtParentName() {
        return artParentName;
    }

    public void setArtSubName(String artSubName) {
        this.artSubName = artSubName;
    }

    public String getArtSubName() {
        return artSubName;
    }

  public void setAwardYear(String awardYear) {
    this.awardYear = awardYear;
  }

  public String getAwardYear() {
    return awardYear;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public String getCategoryName() {
    return categoryName;
  }

}
