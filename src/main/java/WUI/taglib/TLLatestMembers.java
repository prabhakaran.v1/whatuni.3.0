package WUI.taglib;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import WUI.members.form.MembersSearchBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;

/**
  * @TLLatestMembers.java
  * @Version : 2.0
  * @September 20 2007
  * @www.whatuni.com
  * @Created By : Balraj. Selva Kumar
  * @Purpose  : Display the latest members for the course provider.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class TLLatestMembers extends BodyTagSupport {
  public TLLatestMembers() {
  }
  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    HttpSession session = request.getSession();
    String contextPath = request.getContextPath();
    ServletContext context = pageContext.getServletContext();
    try {
      JspWriter out = pageContext.getOut();
      List latestMemberList = null;
      if (session.getAttribute("latestMemberList") == null) {
        new CommonFunction().loadLatestMembersPod(request, context);
      }
      latestMemberList = latestMemberList = (ArrayList)session.getAttribute("latestMemberList");
      if (latestMemberList != null && latestMemberList.size() > 0) {
     /*   out.println("<h2>Latest members</h2>");
        out.println("<div id=\"people\">");
        Iterator uniiterator = latestMemberList.iterator();
        while (uniiterator.hasNext()) {
          MembersSearchBean bean = (MembersSearchBean)uniiterator.next();
          out.println("<dl>");
          out.println("<dt><a rel=\"nofollow\" href=\"" + contextPath + "/viewprofile.html?uid=" + bean.getUserId() + "\" title=\"" + bean.getUserName() + "\"> <img src=\"" + bean.getUserImage() + "\" alt=\"\" title=\"" + bean.getUserName() + "\" /> </a></dt>");
          if(bean.getUserName() !=null){
              out.println("<dd><a rel=\"nofollow\" href=\"" + contextPath + "/viewprofile.html?uid=" + bean.getUserId() + "\" title=\"" + bean.getUserName() + "\">" + bean.getUserName() + "</a></dd>");
          } else{
              out.println("<dd>&nbsp;</dd>");
          }    
          String otherinfo = "";
          if (bean.getIama() != null && !bean.getIama().equalsIgnoreCase("null") && bean.getIama().trim().length() > 0) {
            otherinfo = bean.getIama() + ", ";
          }
          if (bean.getYearOfGraduation() != null && !bean.getYearOfGraduation().equalsIgnoreCase("null") && bean.getYearOfGraduation().trim().length() > 0) {
            otherinfo = otherinfo + bean.getYearOfGraduation() + ", ";
          }
          if (bean.getGender() != null && !bean.getGender().equalsIgnoreCase("null") && bean.getGender().trim().length() > 0) {
            otherinfo = otherinfo + bean.getGender() + ", ";
          }
          otherinfo = otherinfo + bean.getNationality();
          out.println("<dd>" + otherinfo + "</dd>");
          out.println("<dd></dd>");
          out.println("</dl>");
        }
        out.println("</div>");
        //out.println("<p class=\"viewall\"><a rel=\"nofollow\" href=\"" + contextPath + "/allmembers.html?e=all\" title=\"View all members\">View all members</a></p>");
     */ 
      out.println("<div class=\"top-crs-blk\"><h2 class=\"hc-1\"><strong>Latest members</strong></h2>");
            out.println("<ul class=\"top-crs-list\">");
            Iterator uniiterator = latestMemberList.iterator();
            while (uniiterator.hasNext()) {
              MembersSearchBean bean = (MembersSearchBean)uniiterator.next();
              out.println("<li>");
              out.println("<span><a rel=\"nofollow\" title=\"" + bean.getUserName() + "\"> <img src=\"" + new CommonUtil().getImgPath(bean.getUserImage(), 0) + "\" alt=\"\" title=\"" + bean.getUserName() + "\" width=\"50\" /> </a></span>");
             // <a href="" >Jesse Mcjessiey</a><br />Future Student, 2012, Male, Nigeria</div>
              if(bean.getUserName() !=null){
                  out.println("<div><a rel=\"nofollow\" class=\"sblue\" title=\"" + bean.getUserName() + "\">" + bean.getUserName() + "</a><br/>");
              } else{
                  out.println("<div>");
              }    
              String otherinfo = "";
              if (bean.getIama() != null && !bean.getIama().equalsIgnoreCase("null") && bean.getIama().trim().length() > 0) {
                otherinfo = bean.getIama() + ", ";
              }
              if (bean.getYearOfGraduation() != null && !bean.getYearOfGraduation().equalsIgnoreCase("null") && bean.getYearOfGraduation().trim().length() > 0) {
                otherinfo = otherinfo + bean.getYearOfGraduation() + ", ";
              }
              if (bean.getGender() != null && !bean.getGender().equalsIgnoreCase("null") && bean.getGender().trim().length() > 0) {
                otherinfo = otherinfo + bean.getGender() + ", ";
              }
              otherinfo = otherinfo + bean.getNationality();
              out.println(otherinfo);
              out.println("");
              out.println("</div>");
            }
            out.println("</ul>");
            out.println("</div>");
     }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return EVAL_BODY_BUFFERED;
  }
  public int doAfterBody() throws JspException {
    try {
      BodyContent bodycontent = getBodyContent();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }
  public int doEndTag() {
    return EVAL_PAGE;
  }
}
