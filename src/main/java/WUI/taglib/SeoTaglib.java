package WUI.taglib;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import org.apache.commons.validator.GenericValidator;

import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

import com.wuni.util.seo.SeoUrls;

/**
  * @SeoTaglib.java
  * @Version : 2.0
  * @September 20 2007
  * @www.whatuni.com
  * @Created By : Balraj. Selva Kumar
  * @modified Mohamed Syed
  * @Purpose  : This program compose the URL for the SEO.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 2010          Mohamed Syed                        IP & SP links redefined during CPE release
  *
  */
public class SeoTaglib extends BodyTagSupport {

  public SeoTaglib() {
  }
  private String pageTitle = "";

  public BodyContent getBodyContent() {
    return super.getBodyContent();
  }

  public void setBodyContent(BodyContent BodyContent) {
    super.setBodyContent(BodyContent);
  }

  public int doStartTag() throws JspException {
    try {
      JspWriter out = pageContext.getOut();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return EVAL_BODY_BUFFERED;
  }

  public int doAfterBody() throws JspException {
    try {
      BodyContent bodyContent = getBodyContent();
      HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
      String contextPath = GlobalConstants.WU_CONTEXT_PATH;
      String requestString = bodyContent.getString();
      String responseString = "";
      CommonFunction common = new CommonFunction();
      if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("list-rating-review")) {
        //  /university-rating-reviews/[order-by]/[college-id]/1/rating-reviews.html
        responseString = contextPath + GlobalConstants.SEO_PATH_RATING_REVIEW;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          int arrLength = urlArray.length;
          for (int i = 0; i < arrLength; i++) {
            if (i == 0) { //orderby
              responseString = responseString + (urlArray[0] != null && urlArray[0].trim().length() > 0 ? urlArray[0].trim() : "highest-rated");
            }
            if (i == 1) { //college-id
              responseString = responseString + "/" + urlArray[1];
            }
            if (i == 2) { //rating-filter-condition
              responseString = responseString + "/" + (urlArray[2] != null && urlArray[2].trim().length() > 0 ? urlArray[2].replaceAll("&", "+") : urlArray[2]);
            }
            if (i == 3) { //pageno
              responseString = responseString + "/" + urlArray[3];
            }
          }
        }
        responseString = responseString + "/rating-reviews.html";
        responseString = responseString != null ? responseString.toLowerCase() : responseString;
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("university-profile")) {
        //new CPE-IP SEO url: /degrees/university-profile/bangor-university-overview/3769/overview/8311/0/2/universityprofile.html      
        //new CPE-IP SEO url pattern: /university-profile/[PROVIDER_NAME]-[SECTION_DESC]/[PROVIDER_ID]/[SECTION_DESC]/[MYHC_PROFILE_ID]/[PROFILE_ID]/[CPE_QUAL_NETWORK_ID]/universityprofile.html
        responseString = "";
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          int arrLength = urlArray.length;
          for (int i = 0; i < arrLength; i++) {
            if (i == 0) { //college-name 
              responseString = "";
              responseString = contextPath + GlobalConstants.SEO_PATH_UNI_PROFILE_PAGE + common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(urlArray[0]))) + "-" + urlArray[2];
            } else {
              responseString = responseString + "/" + urlArray[i];
            }
          }
        }
        responseString = responseString + "/universityprofile.html";
        responseString = responseString.toLowerCase();
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("subject-profile")) {
        //new CPE-IP SEO url: /degrees/subject-profile/bangor-university-overview/3769/overview/8311/0/2/subjectprofile.html      
        //new CPE-IP SEO url pattern: /subject-profile/[PROVIDER_NAME]-[SECTION_DESC]/[PROVIDER_ID]/[SECTION_DESC]/[MYHC_PROFILE_ID]/[PROFILE_ID]/[CPE_QUAL_NETWORK_ID]/subjectprofile.html
        responseString = "";
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          int arrLength = urlArray.length;
          for (int i = 0; i < arrLength; i++) {
            if (i == 0) { //college-name 
              responseString = "";
              responseString = contextPath + GlobalConstants.SEO_PATH_SUBJECT_PROFILE_PAGE + common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(urlArray[0]))) + "-" + common.replaceSpecialCharacter(common.replaceURL(urlArray[2]));
            } else {
              responseString = responseString + "/" + common.replaceSpecialCharacter(common.replaceURL(urlArray[i]));
            }
          }
        }
        responseString = responseString + "/subjectprofile.html";
        responseString = responseString != null ? responseString.toLowerCase() : responseString;
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("listreview")) { // SEO list review page //
        //  /university-course-reviews/*/*/*/*/reviews
        boolean keywordsearch = false; // added for 17th November release 
        String searchKeyword = ""; // added for 17th November;
        responseString = "";
        String reviewCategoryCode = "";
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          int arrLength = urlArray.length;
          for (int i = 0; i < arrLength; i++) {
            if (i == 0) { //keyword search // if block added for 17th November release and the array index has been changesd in the below if block increase by 1
              searchKeyword = urlArray[0] != null && !urlArray[0].equals("0") && urlArray[0].trim().length() > 0 ? urlArray[0].trim() : "";
              if (searchKeyword != null && searchKeyword.trim().length() > 0) {
                responseString = "";
                responseString = contextPath + "/" + (searchKeyword.trim().replaceAll(" ", "-")) + "-university-course-reviews/";
                keywordsearch = true;
              }
            }
            if (i == 1) { //orderby
              String orderby = urlArray[1] != null && urlArray[1].trim().length() > 0 ? urlArray[1].trim() : "";
              responseString = responseString + (orderby != null && orderby.trim().length() > 0 ? urlArray[1].replaceAll("_", "-") : "HIGHEST-RATED");
            }
            if (i == 2) { //filter-by
              responseString = responseString + "/" + urlArray[2];
            }
            if (i == 3) { //subjectid
              responseString = responseString + "/" + urlArray[3];
              reviewCategoryCode = "";
            }
            if (i == 4) { //pageno
              responseString = responseString + "/" + urlArray[4];
            }
          }
        }
        if (keywordsearch) {
          responseString = responseString + "/subject-reviews.html";
        } else {
          String seoSubjectName = (String)request.getAttribute("seoSubjectName");
          seoSubjectName = seoSubjectName != null && seoSubjectName.trim().length() > 0 ? seoSubjectName : "";
          if (reviewCategoryCode != null && reviewCategoryCode.equals("0")) {
            seoSubjectName = "";
          }
          responseString = contextPath + (seoSubjectName != null && seoSubjectName.trim().length() > 0 ? "/" + seoSubjectName + "-course-reviews/" : GlobalConstants.SEO_PATH_LIST_REVIEW) + responseString + (seoSubjectName != null && seoSubjectName.trim().length() > 0 ? "/coursereviews.html" : "/reviews.html");
        }
        responseString = responseString != null ? responseString.toLowerCase() : responseString;
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("collegeprospectus")) {
        //URL_PATTERN: /prospectus/[COLLEGE_NAME]-prospectus/[COLLEGE_ID]/[COURSE_ID]/[KEYWORD]/[OPENDAY]-[SUBOREDR_ITEM_ID]/order-prospectus.html         
        //url: /degrees/prospectus/kensington-college-of-business-prospectus/904/0/business/n-3326/order-prospectus.html    
        //url: /degrees/prospectus/kensington-college-of-business-prospectus/904/0/0/n-3325/order-prospectus.html        
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          String collegeName = urlArray[0];
          String keyword = urlArray[3];
          if (collegeName != null && collegeName.trim().length() > 0) {
            collegeName = collegeName.trim();
            collegeName = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(collegeName)));
          }
          if (keyword != null && keyword.trim().length() > 0) {
            keyword = keyword.trim();
            keyword = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(keyword)));
          }
          responseString = contextPath + "/prospectus/" + collegeName + "-prospectus";
          responseString = responseString + "/" + urlArray[1]; //collegeId
          responseString = responseString + "/" + urlArray[2]; //courseId
          responseString = responseString + "/" + keyword;
          responseString = responseString + "/" + urlArray[4] + "-" + urlArray[5]; //openday-subOrderItemId
          responseString = responseString + "/order-prospectus.html";
          responseString = responseString.toLowerCase();
        }
      }else if(pageTitle != null && pageTitle.trim().equalsIgnoreCase("downloadprospectus")){
          //URL_PATTERN: /prospectus/[COLLEGE_NAME]-prospectus/[COLLEGE_ID]/[COURSE_ID]/[KEYWORD]/[OPENDAY]-[SUBOREDR_ITEM_ID]/download-prospectus.html         
          //url: /degrees/prospectus/kensington-college-of-business-prospectus/904/0/business/n-3326/download-prospectus.html    
          //url: /degrees/prospectus/kensington-college-of-business-prospectus/904/0/0/n-3325/download-prospectus.html        
          if (requestString != null) {
            String urlArray[] = requestString.split("#");
            String collegeName = urlArray[0];
            String keyword = urlArray[3];
            if (collegeName != null && collegeName.trim().length() > 0) {
              collegeName = collegeName.trim();
              collegeName = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(collegeName)));
            }
            if (keyword != null && keyword.trim().length() > 0) {
              keyword = keyword.trim();
              keyword = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(keyword)));
            }
            responseString = contextPath + "/prospectus/" + collegeName + "-prospectus";
            responseString = responseString + "/" + urlArray[1]; //collegeId
            responseString = responseString + "/" + urlArray[2]; //courseId
            responseString = responseString + "/" + keyword;
            responseString = responseString + "/" + urlArray[4] + "-" + urlArray[5]; //openday-subOrderItemId
            responseString = responseString + "/download-prospectus.html";
            responseString = responseString.toLowerCase();
          }
      }else if(pageTitle != null && pageTitle.trim().equalsIgnoreCase("sendcollegemail")){
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          String collegeName = urlArray[0];
          String keyword = urlArray[3];
          if (collegeName != null && collegeName.trim().length() > 0) {
            collegeName = collegeName.trim();
            collegeName = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(collegeName)));
          }
          if (keyword != null && keyword.trim().length() > 0) {
            keyword = keyword.trim();
            keyword = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(keyword)));
          }
          keyword = GenericValidator.isBlankOrNull(keyword)?"0": keyword;
          responseString = contextPath + "/email/" + collegeName + "-email";
          responseString = responseString + "/" + urlArray[1]; //collegeId
          responseString = responseString + "/" + urlArray[2]; //courseId
          responseString = responseString + "/" + keyword;
          responseString = responseString + "/" + urlArray[4] + "-" + urlArray[5]; //openday-subOrderItemId
          responseString = responseString + "/send-college-email.html";
          responseString = responseString.toLowerCase();
        }
      }
      else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("locationunibrowse")) { // SEO Uni browse Location page //
        //http://www.whatuni.com/degrees/university-colleges-uk/university-colleges-bath/4/uni-browse.html
        responseString = contextPath + GlobalConstants.SEO_PATH_UNI_BROWSE_LOCATION_PAGE;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) {
              String locationName = urlArray[0] != null && urlArray[0].trim().length() > 0 ? urlArray[0].trim() : "";
              locationName = locationName != null ? locationName.replaceAll("all-", "") : "";
              responseString = responseString + GlobalConstants.SEO_PATH_UNI_BROWSE_LOCATION_PAGE_URL + (locationName != null ? locationName.toLowerCase() : locationName);
            } else {
              responseString = responseString + "/" + urlArray[i];
            }
          }
          responseString = responseString + "/universities.html";
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("univideoreview")) { // SEO univideos //
        //http://www.whatuni.com/degrees/college-videos/university-of-york-videos/highest-rated/3773/1/student-videos.html
        responseString = contextPath + GlobalConstants.SEO_PATH_UNI_VIDEOS;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) {
              String providerName = urlArray[0];
              responseString = responseString + (providerName != null && providerName.trim().length() > 0 ? common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(providerName.trim().toLowerCase()))) + "-videos" : "");
            } else if (i == 1) {
              responseString = responseString + "/highest-rated/" + (urlArray[1] != null ? urlArray[1].trim() : urlArray[1]);
            }
          }
          responseString = responseString + "/1/student-videos.html";
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("videodetail")) { // SEO video detail  //
        //http://www.whatuni.com/degrees/college-videos/[COURSE PROVIDOR]-video-[VIDEO TITLE]/.... + parameters/vids-video.html
        responseString = contextPath + GlobalConstants.SEO_PATH_UNI_VIDEOS;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) {
              String providerName = urlArray[0];
              responseString = responseString + (providerName != null && providerName.trim().length() > 0 ? common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(providerName.trim().toLowerCase()))) + "-video-" : "");
            } else if (i == 1) {
              String videoTitle = urlArray[1];
              responseString = responseString + (videoTitle != null && videoTitle.trim().length() > 0 ? common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(videoTitle.trim().toLowerCase()))) : "") + "/";
            } else {
              responseString = responseString + urlArray[i] + "/";
            }
          }
          responseString = responseString + "vids-video.html";
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("clearingcourseprovider")) { // SEO Clearing Course provider page //
        //http://www.whatuni.com/degrees/university-colleges-uk/university-colleges-bath/4/uni-browse.html
        responseString = contextPath + "/" + GlobalConstants.SEO_PATH_CLEARING + "/";
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) {
              String providerName = urlArray[0] != null && urlArray[0].trim().length() > 0 ? urlArray[0].trim() : "";
              responseString = responseString + (providerName != null && providerName.trim().length() > 0 ? common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(providerName.toLowerCase()))) + "-" + GlobalConstants.SEO_PATH_CLEARING : "") + "-";
            } else if (i == 1) {
              responseString = responseString + urlArray[i];
            } else {
              responseString = responseString + "/" + urlArray[i];
            }
          }
          responseString = responseString + "/clearing.html";
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("unilanding")) { // SEO URL for unilanding page //
        //http://www.whatuni.com/degrees/universities/guide/Aston-University/3443/page.html
        responseString = GlobalConstants.SEO_PATH_NEW_UNILANDING_PAGE;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) {
              String collegeName = urlArray[0] != null && urlArray[0].trim().length() > 0 ? urlArray[0].trim() : "";
              responseString = responseString + (collegeName != null && collegeName.trim().length() > 0 ? common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(collegeName))) : "");
            }
            if (i == 1) {
              responseString = responseString + "/" + urlArray[1];
            }
          }
          responseString = responseString + "/";
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("subject-search-landing")) { // SEO subject landing page added for 02nd March 2010 Release //
        //http://www.whatuni.com/degrees/what-is//[subject-name]-study-[subject-name]/[subject-code]/study.html
        responseString = contextPath + GlobalConstants.SEO_SUBJECT_LANDING;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) {
              String subjectName = urlArray[0]; //subject-name
              subjectName = (subjectName != null && subjectName.trim().length() > 0 ? common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(subjectName.trim().toLowerCase()))) : "");
              responseString = responseString + subjectName + "-study-" + subjectName;
            } else if (i == 1) { //subject-code
              responseString = responseString + "/" + (urlArray[1] != null ? urlArray[1].trim() : urlArray[1]);
            }
          }
          responseString = (responseString != null ? responseString.toLowerCase() : responseString) + "/study.html";
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("uni-student-reviews")) { //Modified review URL formation by Indumathi.S Mar-03-2016 
        //https://www.whatuni.com/degrees/university-course-reviews/[college-name]/[college-id].html
        responseString = GlobalConstants.SEO_PATH_LIST_REVIEW;
        if (!GenericValidator.isBlankOrNull(requestString)) {
          String urlArray[] = requestString.split("#");
          String providerName = urlArray[0];//college-name
          responseString += (!GenericValidator.isBlankOrNull(providerName) ? common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(providerName.trim().toLowerCase()))) : "");
          //college-id
          responseString += "/" + (!GenericValidator.isBlankOrNull(urlArray[1]) ? urlArray[1].trim() : urlArray[1] + "/"); 
          responseString = !GenericValidator.isBlankOrNull(responseString) ? responseString.toLowerCase() : responseString;
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("unibrowse")) { /// SEO URL for unibrowse page ////
        //http://www.whatuni.com/degrees/universities/browse.html
        responseString = contextPath + GlobalConstants.SEO_PATH_UNIBROWSE_PAGE;
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("coursedetail")) { // SEO URL for course detail page //
        //URL_PATTERN:  www.whatuni.com/degrees/courses//[STUDY LEVEL]-details/[COURSE TITLE]-courses-details/[course_id]/[college_id]/cdetail.html
        //EXAMPLE:      www.whatuni.com/degrees/courses/Postgraduate-details/Accounting-MPhil-course-details/29837179/3769/cdetail.html
        responseString = contextPath + GlobalConstants.SEO_PATH_COURSEDETAIL_PAGE;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) {
              // to replace the / and "" with - for the study level text hnd/hnd etc.. //
              String studyLevel = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].replaceAll("/", "-") : "");
              studyLevel = (studyLevel != null && studyLevel.trim().length() > 0 ? common.replaceHypen(studyLevel) : "");
              responseString = responseString + studyLevel + "/";
            }
            if (i == 1) {
              String courseName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim().replaceAll("\\/", "-") : "";
              responseString = responseString + (courseName != null && courseName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(common.replaceSpecialCharacter(courseName))) + "-" : "");
            }
            if (i == 2) {
              responseString = responseString + "-course-details";
              responseString = responseString != null && responseString.trim().length() > 0 ? common.replaceHypen(responseString) : "";
            }
            if (i == 3) {
              responseString = responseString + "/" + urlArray[i];
            }
            if (i == 4) {
              responseString = responseString + "/" + urlArray[i];
            }
          }
          responseString = responseString + "/cdetail.html";
        }
      }else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("clcoursedetail")) { // SEO URL for course detail page //
        //URL_PATTERN:  www.whatuni.com/degrees/courses//[STUDY LEVEL]-details/[COURSE TITLE]-courses-details/[course_id]/[college_id]/cdetail.html
        //EXAMPLE:      www.whatuni.com/degrees/courses/Postgraduate-details/Accounting-MPhil-course-details/29837179/3769/cdetail.html
        responseString = contextPath + GlobalConstants.SEO_PATH_COURSEDETAIL_PAGE;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) {
              // to replace the / and "" with - for the study level text hnd/hnd etc.. //
              String studyLevel = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].replaceAll("/", "-") : "");
              studyLevel = (studyLevel != null && studyLevel.trim().length() > 0 ? common.replaceHypen(studyLevel) : "");
              responseString = responseString + studyLevel + "/";
            }
            if (i == 1) {
              String courseName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim().replaceAll("\\/", "-") : "";
              responseString = responseString + (courseName != null && courseName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(common.replaceSpecialCharacter(courseName))) + "-" : "");
            }
            if (i == 2) {
              responseString = responseString + "-course-details";
              responseString = responseString != null && responseString.trim().length() > 0 ? common.replaceHypen(responseString) : "";
            }
            if (i == 3) {
              responseString = responseString + "/" + urlArray[i];
            }
            if (i == 4) {
              responseString = responseString + "/" + urlArray[i];
            }
          }
          responseString = responseString + "/clcdetail.html";
        }
      }else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("coursesearch")) {
        responseString = contextPath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          String studyLevel = "";
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) { // study level 
              studyLevel = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? common.replaceHypen(urlArray[i]) : "");
              responseString = responseString + studyLevel + "/";
            }
            if (i == 1) { //subject name
               if(!GenericValidator.isBlankOrNull(urlArray[i])){urlArray[i] = urlArray[i].replaceAll(",","-");}//19_NOV_2013
              String subjectName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? (urlArray[i].trim().indexOf(",") > 0 ? urlArray[i].trim().substring(0, urlArray[i].trim().indexOf(",")) : urlArray[i].trim()) + "-courses-at-" : "all-courses-at-";
              responseString = responseString + (subjectName != null && subjectName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(subjectName)) : "");
            }
            if (i == 2) { //collegename
              String collegeName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
              responseString = responseString + (collegeName != null && collegeName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(collegeName)) : "");
            }
            if (i == 3) { //urlString
              responseString = responseString + (urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim().replaceAll("//", "/-/") : "");
            }
          }
          responseString = responseString != null ? responseString.toLowerCase() + "/csearch.html" : responseString + "/csearch.html";
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("coursesearchwithFilter")) {
        responseString = contextPath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          String studyLevel = "";
          String appendUrl = "";
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) { // study level 
              studyLevel = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? common.replaceHypen(urlArray[i]) : "");
              responseString = responseString + studyLevel + "/";
            }
            if (i == 1) { //subject name
               if(!GenericValidator.isBlankOrNull(urlArray[i])){urlArray[i] = urlArray[i].replaceAll(",","-");}//19_NOV_2013
              String subjectName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? (urlArray[i].trim().indexOf(",") > 0 ? urlArray[i].trim().substring(0, urlArray[i].trim().indexOf(",")) : urlArray[i].trim()) + "-courses-at-" : "all-courses-at-";
              responseString = responseString + (subjectName != null && subjectName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(subjectName)) : "");
            }
            if (i == 2) { //collegename
              String collegeName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
              responseString = responseString + (collegeName != null && collegeName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(collegeName)) : "");
            }
            if (i == 3) { //urlString
              responseString = responseString + (urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim().replaceAll("//", "/-/") : "");
            }
            if (i == 4) { //urlString
              appendUrl =  (urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i] : "" );
            }
          }
          responseString = responseString != null ? responseString.toLowerCase() + "/csearch.html" : responseString + "/csearch.html";
          responseString = responseString + appendUrl;
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("clcoursesearchwithFilter")) {//24_JUN_2014
        responseString = contextPath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          String studyLevel = "";
          String appendUrl = "";
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) { // study level 
              studyLevel = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? common.replaceHypen(urlArray[i]) : "");
              responseString = responseString + studyLevel + "/";
            }
            if (i == 1) { //subject name
               if(!GenericValidator.isBlankOrNull(urlArray[i])){urlArray[i] = urlArray[i].replaceAll(",","-");}//19_NOV_2013
              String subjectName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? (urlArray[i].trim().indexOf(",") > 0 ? urlArray[i].trim().substring(0, urlArray[i].trim().indexOf(",")) : urlArray[i].trim()) + "-courses-at-" : "all-courses-at-";
              responseString = responseString + (subjectName != null && subjectName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(subjectName)) : "");
            }
            if (i == 2) { //collegename
              String collegeName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
              responseString = responseString + (collegeName != null && collegeName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(collegeName)) : "");
            }
            if (i == 3) { //urlString
              responseString = responseString + (urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim().replaceAll("//", "/-/") : "");
            }
            if (i == 4) { //urlString
              appendUrl =  (urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i] : "" );
            }
          }
          responseString = responseString != null ? responseString.toLowerCase() + "/clcsearch.html" : responseString + "/clcsearch.html";
          responseString = responseString + appendUrl;
        }
      } else if(pageTitle != null && pageTitle.trim().equalsIgnoreCase("clcoursesearch")){
        responseString = contextPath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          String studyLevel = "";
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) { // study level 
              studyLevel = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? common.replaceHypen(urlArray[i]) : "");
              responseString = responseString + studyLevel + "/";
            }
            if (i == 1) { //subject name
              String subjectName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? (urlArray[i].trim().indexOf(",") > 0 ? urlArray[i].trim().substring(0, urlArray[i].trim().indexOf(",")) : urlArray[i].trim()) + "-courses-at-" : "all-courses-at-";
              responseString = responseString + (subjectName != null && subjectName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(subjectName)) : "");
            }
            if (i == 2) { //collegename
              String collegeName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
              responseString = responseString + (collegeName != null && collegeName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(collegeName)) : "");
            }
            if (i == 3) { //urlString
              responseString = responseString + (urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim().replaceAll("//", "/-/") : "");
            }
          }
          responseString = responseString != null ? responseString.toLowerCase() + "/csearch.html?nd&clearing" : responseString + "/csearch.html?nd&clearing";
        }
        
      }else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("unisubjectsearch")) {
        responseString = contextPath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          String studyLevel = "";
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) { // study level 
              studyLevel = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? common.replaceHypen(urlArray[i]) : "");
              responseString = responseString + studyLevel + "/";
            }
            if (i == 1) { //subject name
              String subjectName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? (urlArray[i].trim().indexOf(",") > 0 ? urlArray[i].trim().substring(0, urlArray[i].trim().indexOf(",")) : urlArray[i].trim()) + "-courses-at-" : "all-courses-at-";
              responseString = responseString + (subjectName != null && subjectName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(subjectName)) : "");
            }
            if (i == 2) { //collegename
              String collegeName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
              responseString = responseString + (collegeName != null && collegeName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(collegeName)) : "");
            }
            if (i == 3) { //urlString
              responseString = responseString + (urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim().replaceAll("//", "/-/") : "");
            }
          }
          responseString = responseString != null ? responseString.toLowerCase() + "/rsearch.html" : responseString + "/rsearch.html";
          //responseString = responseString +"/csearch.html";
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("journeyhome")) {
        responseString = contextPath + GlobalConstants.SEO_PATH_COURSEJOUNEY_PAGE;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) { // study level desc
              String studyLevel = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? common.replaceHypen(urlArray[i]) : "");
              responseString = responseString + studyLevel + "/";
            }
            if (i == 1) { //study level id
              String subjectName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
              responseString = responseString + (subjectName != null && subjectName.trim().length() > 0 ? "qualification/" + subjectName : "");
            }
          }
          responseString = responseString + "/list.html";
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("subjecthome")) {
        responseString = contextPath + GlobalConstants.SEO_PATH_COURSEJOUNEY_PAGE;
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) { // study level desc
              String studyLevelDesc = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? common.replaceHypen(urlArray[i]) : "");
              responseString = responseString + studyLevelDesc;
            }
            if (i == 1) { //subjectName
              String subjectName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
              responseString = responseString + (subjectName != null && subjectName.trim().length() > 0 ? "/" + common.replaceHypen(common.replaceURL(subjectName)) : "");
            }
            if (i == 2) { //searchUrlText
              String studyLevelText = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
              responseString = responseString + (studyLevelText != null && studyLevelText.trim().length() > 0 ? "-" + common.replaceHypen(studyLevelText) + "-courses-UK" : "-courses-UK");
            }
            if (i == 3) { //StudyLevelId
              String studyLevelId = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
              responseString = responseString + (studyLevelId != null && studyLevelId.trim().length() > 0 ? "/qualification/" + studyLevelId : "");
            }
            if (i == 4) { //SubjectId
              String courseId = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
              responseString = responseString + (courseId != null && courseId.trim().length() > 0 ? "/search_category/" + courseId : "");
            }
          }
          responseString = responseString + "/loc.html";
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("reviewcategory")) {
        //http://192.168.1.17/degrees/reviews/uni/University-Of-Sussex-student-life-reviews/5193/11/1/highest_rated/reviewcategory.html
        responseString = contextPath + GlobalConstants.SEO_PATH_REVIEW_CATEGORY_PAGE;
        String collegeName = "";
        String collegeid = "";
        String questionid = "";
        String questiontitle = "";
        String pageno = "";
        String orderby = "";
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) { //collegename
              collegeName = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? common.replaceHypen(urlArray[i]) : "");
              collegeName = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(collegeName.trim())));
            }
            if (i == 1) { //questiontitle
              questiontitle = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? common.replaceHypen(urlArray[i]) : "");
            }
            if (i == 2) { //collegeid
              collegeid = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
            }
            if (i == 3) { //questionid
              questionid = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
            }
            if (i == 4) { //pageno
              pageno = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
            }
            if (i == 5) { //orderby
              orderby = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
            }
          }
          responseString = responseString + collegeName + "-" + questiontitle + "-reviews/" + collegeid + "/" + questionid + "/" + pageno + "/" + orderby + "/reviewcategory.html";
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("uni-open-days")) {
        //URL_PATTERN:  http://www.whatuni.com/degrees/open-days/[LOCATION]-open-days-[PROVIDER-NAME]/[COLLEGE-ID]/page.html  
        //URL:          http://www.whatuni.com/degrees/open-days/aberdeen-open-days-aberdeen-college/43961/page.html        
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          String collegeName = urlArray[0];
          String collegeId = urlArray[1];
          String collegeLocation = "";
          if(urlArray.length > 2){
            collegeLocation = urlArray[2];
          }
          SeoUrls seoUrls = new SeoUrls();
          //
          //responseString = seoUrls.constructUniSpecificOpenDaysUrl(collegeId, collegeName, collegeLocation);
          responseString = seoUrls.constructOpendaysSeoUrl(collegeName,collegeId);  //17_Mar_ 15 Modified by Amir for new provider open day URL pattern
          //
          //nullify unused objects
          collegeName = null;
          collegeId = null;
          collegeLocation = null;
          seoUrls = null;
          for (int i = 0; i < urlArray.length; i++) {
            urlArray[i] = null;
          }
          urlArray = null;
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("browse-by-subject")) {
        // new SEO page "browse > browse by location > browse by category > money page" 's second page. Since: wu306_20110405. Author: Mohamed Syed--> 
        // URL_PATTERN: www.whatuni.com/degrees/courses/[LOCATION_NAME]-degree/[LOCATION_NAME]/[QUALIFICATION_CODE]/[PAGE_NO]/degrees.html -->
        // Example:     www.whatuni.com/degrees/courses/london-degree/london/m/1/degrees.html -->l  
        if (requestString != null) {
          String location = requestString.trim();
          String locationName1 = common.replaceURL(common.replaceHypen(String.valueOf(location))); //refered from regionHome.jsp
          String locationName2 = location.replaceAll(" & ", " & ").replaceAll(" ", "+"); //refered from regionHome.jsp                             
          responseString = "/degrees/courses/";
          responseString = responseString + locationName1 + "-degree/";
          responseString = responseString + locationName2;
          responseString = responseString + "/m/1/degrees.html";
          responseString = responseString.toLowerCase();
          //nullify unused objects
          location = null;
          locationName1 = null;
          locationName2 = null;
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("uni-specific-scholarship")) {
        // URL_PATTERN: http://www.whatuni.com/degrees/scholarship-uk/[COLEEGE_NAME]-scholarships-bursaries/[STUDY_LEVEL]/[COLLEGE_ID]/[PAGE_NO]/bursary-uni.html
        // Example:     http://www.whatuni.com/degrees/scholarship-uk/university-of-east-anglia-scholarships-bursaries/m,n,a,t,l/5637/2/bursary-uni.html  
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          String collegeName = urlArray[0];
          String collegeId = urlArray[1];
          SeoUrls seoUrls = new SeoUrls();
          //                           
          responseString = seoUrls.constructUniSpecificScholarshipUrl(collegeId, collegeName);
          //nullify unused objects
          collegeName = null;
          collegeId = null;
          seoUrls = null;
          for (int i = 0; i < urlArray.length; i++) {
            urlArray[i] = null;
          }
          urlArray = null;
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("uni-specific-reviews")) {
        // URL_PATTERN: http://www.whatuni.com/degrees/reviews/uni/[COLLEGE_NAME]-reviews/[ORDER_BY]/[FILTER_BY]/[SUBJECT_CODE]/[COLLEGE_ID]/[PAGE_NO]/studentreviews.html
        // Example:     http://www.whatuni.com/degrees/reviews/uni/university-of-worcester-reviews/highest-rated/0/0/5635/1/studentreviews.html  
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          String collegeName = urlArray[0];
          String collegeId = urlArray[1];
          SeoUrls seoUrls = new SeoUrls();
          //                           
          responseString = seoUrls.constructUniSpecificReviewUrl(collegeId, collegeName);
          //nullify unused objects
          collegeName = null;
          collegeId = null;
          seoUrls = null;
          for (int i = 0; i < urlArray.length; i++) {
            urlArray[i] = null;
          }
          urlArray = null;
        }
      } else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("review-details")) {
        // URL_PATTERN: http://www.whatuni.com/degrees/showReviewDetail.html?rid=[REVIEW_ID]&z=[COLLEGE_ID]
        // Example:     http://www.whatuni.com/degrees/showReviewDetail.html?rid=27031&z=3745  
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          String reviewId = urlArray[0];
          String collegeId = urlArray[1];
          SeoUrls seoUrls = new SeoUrls();
          //                           
          responseString = seoUrls.constructReviewDetailsUrl(collegeId, reviewId);
          //nullify unused objects
          reviewId = null;
          collegeId = null;
          seoUrls = null;
          for (int i = 0; i < urlArray.length; i++) {
            urlArray[i] = null;
          }
          urlArray = null;
        }
      }else if(pageTitle != null && pageTitle.trim().equalsIgnoreCase("claring-provider-results")){
          responseString = contextPath + GlobalConstants.SEO_PATH_COURSESEARCH_PAGE;
          if (requestString != null) {
            String urlArray[] = requestString.split("#");
            String studyLevel = "";
            for (int i = 0; i < urlArray.length; i++) {
              if (i == 0) { // study level 
                studyLevel = (urlArray[i] != null && urlArray[i].trim().length() > 0 ? common.replaceHypen(urlArray[i]) : "");
                responseString = responseString + studyLevel + "/";
              }
              if (i == 1) { //subject name
                String subjectName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? (urlArray[i].trim().indexOf(",") > 0 ? urlArray[i].trim().substring(0, urlArray[i].trim().indexOf(",")) : urlArray[i].trim()) + "-courses-at-" : "all-courses-at-";
                responseString = responseString + (subjectName != null && subjectName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(subjectName)) : "");
              }
              if (i == 2) { //collegename
                String collegeName = urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim() : "";
                responseString = responseString + (collegeName != null && collegeName.trim().length() > 0 ? common.replaceHypen(common.replaceURL(collegeName)) : "");
              }
              if (i == 3) { //urlString
                responseString = responseString + (urlArray[i] != null && urlArray[i].trim().length() > 0 ? urlArray[i].trim().replaceAll("//", "/-/") : "");
              }
            }
            responseString = responseString != null ? responseString.toLowerCase() + "/clcsearch.html" : responseString + "/clcsearch.html";
            //responseString = responseString +"/csearch.html";
          }
      }else if(pageTitle != null && pageTitle.trim().equalsIgnoreCase("clearing-university-profile")){
        //url pattern: /university-clearing-2012/[PROVIDER_NAME]-clearing-[SECTION_DESC]/[PROVIDER_ID]/[SECTION_DESC]/[MYHC_PROFILE_ID]/[PROFILE_ID]/[CPE_QUAL_NETWORK_ID]/universityprofile.html
        responseString = "";
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          int arrLength = urlArray.length;
          for (int i = 0; i < arrLength; i++) {
            if (i == 0) { //college-name 
              responseString = "";
              responseString = "/university-clearing-"+GlobalConstants.CLEARING_YEAR+"/"+ common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(urlArray[0]))) + "-clearing-" + urlArray[2];
            } else {
              responseString = responseString + "/" + urlArray[i];
            }
          }
        }
        responseString = responseString + "/universityprofile.html";
        responseString = responseString.toLowerCase();
      }else if(pageTitle != null && pageTitle.trim().equalsIgnoreCase("clearing-unioverivew")){
        //http://www.whatuni.com/degrees/universities/guide/Aston-University/3443/page.html
         responseString = "/university-clearing-"+GlobalConstants.CLEARING_YEAR+"/";
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          for (int i = 0; i < urlArray.length; i++) {
            if (i == 0) {
              String collegeName = urlArray[0] != null && urlArray[0].trim().length() > 0 ? urlArray[0].trim() : "";
              responseString = responseString + (collegeName != null && collegeName.trim().length() > 0 ? common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(collegeName)) + "-clearing-overview") : "");
            }
            if (i == 1) {
              responseString = responseString + "/" + urlArray[1];
            }
          }
          responseString = responseString + "/page.html";
          responseString = responseString.toLowerCase();
        }
      }else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("newReview")) { // SEO list review page //
        //  /university-course-reviews/*/*/*/*/reviews
        String searchKeyword = ""; 
        responseString = "";
        String reviewCategoryCode = "";
        String queryString = "";
        if (requestString != null) {
          String urlArray[] = requestString.split("#");
          int arrLength = urlArray.length;
          responseString = contextPath + "/" +  "university-course-reviews/";          
          for (int i = 0; i < arrLength; i++) {
            if (i == 0) { 
              searchKeyword = urlArray[0] != null && !urlArray[0].equals("0") && urlArray[0].trim().length() > 0 ? urlArray[0].trim() : "";             
            }
            if (i == 1) { //orderby
              String orderby = urlArray[1] != null && urlArray[1].trim().length() > 0 ? urlArray[1].trim() : "";
              responseString = responseString + (orderby != null && orderby.trim().length() > 0 ? urlArray[1].replaceAll("_", "-") : "HIGHEST-RATED");
            }
            if (i == 2) { //filter-by
              responseString = responseString + "/" + urlArray[2];
            }
            if (i == 3) { //subjectid
              responseString = responseString + "/" + urlArray[3];
              reviewCategoryCode = "";
            }
            if (i == 4) { //pageno
              responseString = responseString + "/" + urlArray[4];
            }
          }
        }
        responseString = responseString != null ? responseString.toLowerCase() : responseString;
        responseString = responseString + "/reviews.html";
        if (searchKeyword != null && searchKeyword.trim().length() > 0) {
          queryString = "?key=" + (searchKeyword.trim().replaceAll(" ", "-"));
          responseString = responseString + queryString;
        }
      }else if (pageTitle != null && pageTitle.trim().equalsIgnoreCase("new-uni-student-reviews")) { // SEO uni-landing review added for 02nd March 2010 Release //
             //http://www.whatuni.com/degrees/reviews/uni/[college-name]-reviews/[order-by]/[filter-by]/[subject-code]/[college-id]/[pageno]/studentreviews.html
             responseString = contextPath + GlobalConstants.SEO_PATH_UNI_REVIEWS;
             String searchKeyword = ""; 
             String queryString = "";
             if (requestString != null) {
               String urlArray[] = requestString.split("#");
               for (int i = 0; i < urlArray.length; i++) {
                 if (i == 0) {
                   String providerName = urlArray[0];
                   responseString = responseString + (providerName != null && providerName.trim().length() > 0 ? common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(providerName.trim().toLowerCase()))) + "-reviews" : "");
                 } else if (i == 1) { //order-by
                   responseString = responseString + "/" + (urlArray[1] != null ? urlArray[1].trim() : urlArray[1]);
                 } else if (i == 2) { //filter-by
                   searchKeyword = urlArray[2] != null && !urlArray[2].equals("0") && urlArray[2].trim().length() > 0 ? urlArray[2].trim() : "";  
                   responseString = responseString + "/0";
                 } else if (i == 3) { //subject-code
                   responseString = responseString + "/" + (urlArray[3] != null ? urlArray[3].trim() : urlArray[3]);
                 } else if (i == 4) { //college-id
                   responseString = responseString + "/" + (urlArray[4] != null ? urlArray[4].trim() : urlArray[4]);
                 } else if (i == 5) { //page-no
                   responseString = responseString + "/" + (urlArray[5] != null ? urlArray[5].trim() : urlArray[5]);
                 }
               }
               responseString = (responseString != null ? responseString.toLowerCase() : responseString) + "/studentreviews.html";
               if (searchKeyword != null && searchKeyword.trim().length() > 0) {
                 queryString = "?key=" + (searchKeyword.trim().replaceAll(" ", "-"));
                 responseString = responseString + queryString;
               }
             }
           }else if(pageTitle != null && pageTitle.trim().equalsIgnoreCase("all-provider-results")){
             if (requestString != null) {
               String appendUrl = "";               
               String urlArray[] = requestString.split("#");
               for (int i = 0; i < urlArray.length; i++) {
                 String studyLevelDesc = "";                 
                 //Changed new all courses url pettern, By Thiyagu G for 27_Jan_2016.
                 if (i == 0) { //collegeName                   
                   String collegeName = urlArray[0] != null && urlArray[1].trim().length() > 0 ? urlArray[0].trim() : "";
                   responseString = (collegeName != null && collegeName.trim().length() > 0 ? "/all-courses/csearch?university=" +common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(collegeName))) : "");
                 }else if (i == 1) { //collegId
                   //responseString = responseString + "/" + (urlArray[1] != null ? urlArray[1].trim() : "");
                 }else if (i == 2) { //urlString
                   appendUrl =  (urlArray[2] != null && urlArray[i].trim().length() > 0 ? urlArray[2] : "" );
                 }
               }
               responseString = responseString != null ? responseString.toLowerCase() : responseString;
               //responseString = responseString + appendUrl;
             }         
           
           }
      //
      JspWriter out = bodyContent.getEnclosingWriter();
      out.print(responseString);
      bodyContent.clearBody();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }

  public String getFormatedName(String inString) {
    if (inString != null && inString.trim().length() > 0) {
      inString = inString.trim();
      char strArray[] = inString.toCharArray();
      for (int i = 0; i < strArray.length; i++) {
        int charValue = strArray[i];
        if (!((charValue >= 48 && charValue <= 57) || (charValue >= 65 && charValue <= 90) || (charValue >= 97 && charValue <= 122) || charValue == 45)) {
          strArray[i] = ' ';
        }
      }
      inString = new String(strArray);
      inString = inString.replaceAll(" ", "-");
      inString = inString.replaceAll("--", "-");
      inString = inString.replaceAll("--", "-");
      inString = inString.replaceAll("--", "-");
      inString = inString.replaceAll("(?i)-amp-", "-");
      inString = inString.replaceAll("(?i)-39-", "-");
      inString = inString.replaceAll("(?i)-Pte-", "-");
      inString = inString.replaceAll("(?i)-Ltd-", "-");
      inString = inString.replaceAll("(?i)-of-", "-");
      inString = inString.replaceAll("(?i)-and-", "-");
      inString = inString.replaceAll("(?i)-for-", "-");
      inString = inString.replaceAll("(?i)-in-", "-");
      inString = inString.replaceAll("(?i)-S-", "-");
      inString = inString.replaceAll("(?i)-com-", "-");
      inString = inString.replaceAll("(?i)-for-", "-");
      inString = inString.replaceAll("(?i)-at-", "-");
      inString = inString.replaceAll("(?i)-2003-", "-");
      inString = inString.replaceAll("(?i)-the-", "-");
      inString = inString.replaceAll("(?i)-an-", "-");
      inString = inString.replaceAll("(?i)-Ltd", "");
      inString = inString.replaceAll("(?i)The-", "");
      if (inString.charAt(inString.length() - 1) == '-') {
        inString = inString.substring(0, inString.length() - 1);
      }
    }
    return inString;
  }

  public int doEndTag() throws JspException {
    return EVAL_PAGE;
  }

  public void release() {
    pageTitle = "";
  }

  public void setPageTitle(String pageTitle) {
    this.pageTitle = pageTitle;
  }

  public String getPageTitle() {
    return pageTitle;
  }

}
