package WUI.taglib;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import spring.form.VideoReviewListBean;
import spring.util.GlobalMethods;
import WUI.search.form.SearchBean;
import WUI.search.form.SearchResultBean;
import WUI.utilities.AdvertUtilities;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.wuni.util.affiliate.AffiliateUtilities;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.ClearingInfoVO;

/**
  * @TLInteractivePod.java
  * @Version : 2.0
  * @September 20 2007
  * @www.whatuni.com
  * @Created By : Balraj. Selva Kumar
  * @Purpose  : Display the course provider related interactive informations.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 02.09.2009    Selvakumar          whatuni_1_60    Changed the logic to display the video and image in the interactive pod after migrating the CMI in domestic site
  */
public class TLInteractivePod extends BodyTagSupport {
  public TLInteractivePod() {
  }
  public String resultType = null;
  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    String collegeId = (String)request.getAttribute("interactive-college-id");
    collegeId = collegeId != null && collegeId.trim().length() > 0? collegeId.trim(): "0";
    String subjectid = (String)request.getAttribute("interactive-sid");
    subjectid = subjectid != null && subjectid.trim().length() > 0? subjectid.trim(): "";
    String showSubjectList = (String)request.getAttribute("interactive-show-subjectlist");
    showSubjectList = showSubjectList != null && showSubjectList.trim().length() > 0? showSubjectList: "";
    List InstitutionProfileList = null;
    List subjectProfileList = null;
    VideoReviewListBean videobean = null;
    HttpSession session = request.getSession();
    String cpeQualificationNetworkId = (String)session.getAttribute("cpeQualificationNetworkId");     
    if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0 || cpeQualificationNetworkId.equals("0") ){
        cpeQualificationNetworkId = "2";
    }
    session.removeAttribute("cpeQualificationNetworkId");
    session.setAttribute("cpeQualificationNetworkId", cpeQualificationNetworkId);       
    
    String contextPath = request.getContextPath();
    Vector vector = new Vector();
    vector.clear();
    JspWriter out = pageContext.getOut();
    try {
      List CollegeInteractivePodList = null;
    
      CollegeInteractivePodList = (List)request.getAttribute("complete_college_interaction");
      if (CollegeInteractivePodList != null && CollegeInteractivePodList.size() > 0) {
        Iterator uniiterator = CollegeInteractivePodList.iterator();
        SearchResultBean bean = (SearchResultBean)uniiterator.next();
        
        //GETTING IP & SP DETAIS
        new AdvertUtilities().getIpSpProfileDetails(collegeId,request,cpeQualificationNetworkId,null, null, null);  ////DB_CALL //13_JAN_15 Added by Amir for Video Rich Profile
        
        InstitutionProfileList = (List)request.getAttribute("institution-profile-list");
        if (InstitutionProfileList != null && InstitutionProfileList.size() > 0) {
          uniiterator = InstitutionProfileList.iterator();
          videobean = (VideoReviewListBean)uniiterator.next();
        } 
       
        subjectProfileList = (List)request.getAttribute("subject-profile-list");
        if (subjectProfileList != null && subjectProfileList.size() > 0) {
          uniiterator = subjectProfileList.iterator();
          videobean = (VideoReviewListBean)uniiterator.next();
        } 
          
        // To display the college profile and Subject profile if the colleges is an advertiser otherwise it will display the video ticker tape 
        if (resultType != null && resultType.equalsIgnoreCase("university-profile-interaction")) {
          String topimageid = "";
          if(InstitutionProfileList != null && InstitutionProfileList.size() > 0 || subjectProfileList != null && subjectProfileList.size() >0 ) {
          if((videobean.getSubOrderItemIdForIp() != null && videobean.getSubOrderItemIdForIp().trim().length()>0 && !videobean.getSubOrderItemIdForIp().equalsIgnoreCase("NULL")) ||
            (videobean.getSubOrderItemIdForSp() != null && videobean.getSubOrderItemIdForSp().trim().length()>0 && !videobean.getSubOrderItemIdForSp().equalsIgnoreCase("NULL")))
          {
            if(videobean.getProfileTypeIp() !=null  && videobean.getProfileTypeIp().trim().length() > 0 && !videobean.getProfileTypeIp().equalsIgnoreCase("null"))
            {
          
            //To display the IP details
            //
           // getClearingInfo(out, collegeId);     //wu314_20110712_Syed: Stacey-functional-crd#1 - linking to clearing-micro-site            
            //
            
            out.println("<div class=\"uni-porfile-blk\">");             
            out.println("<h2 class=\"hs-1 uni-pro\"><strong>University <br/><span class=\"green\">Profile</span></strong></h2>");
            //out.println("<h2 class=\"hs-2\">"+bean.getCollegeNameDisplay());
             out.println("<h2 class=\"hs-2\"><a href=\"" + new GlobalMethods().changeSeoURLLink("unilanding", bean.getCollegeName() + GlobalConstants.SEO_URL_SPLIT_CHAR + bean.getCollegeId()) + "\" title=\"" + bean.getCollegeNameDisplay() + "\">" + bean.getCollegeNameDisplay() + "</a>");
            if(bean.getCollegeLocation()!=null&&!bean.getCollegeLocation().equals("")){
                out.println(", "+bean.getCollegeLocation());
            }
            out.println("<br /><span class=\"ash\">United Kingdom</span></h2>");
               
            if (request.getAttribute("show-google-map") == null) {
              if (bean.getCollegeLogo() != null && bean.getCollegeLogo().trim().length() >= 0) {
                if (bean.getLogoType() != null && bean.getLogoType().trim().equalsIgnoreCase("video")) {
                  out.println("<a rel=\"nofollow\" class=\"vhold\" onclick=\"return showVideo('" + bean.getLogoId() + "','" + bean.getCollegeId() + "','B')\" title=\"overview\"><img src=\"" + bean.getVideoThumbUrl() + "\" alt=\"" + bean.getCollegeName() + " profile\"" + topimageid + " title=\"" + bean.getCollegeNameDisplay() + " profile\" width=\"80\" height=\"80\" align=\"left\" /><span class=\"vplay5\"><img title=\"\" alt=\"\" src=\"" + CommonUtil.getImgPath("", 0) + "/wu-cont/images/playvideo.png\"/></span></a>");
                  request.setAttribute("showlightbox", "YES");
                }
                if (bean.getLogoType() != null && bean.getLogoType().trim().equalsIgnoreCase("image")) {
                  if(videobean.getMyhcProfileIdIp() != null && videobean.getMyhcProfileIdIp().trim().length() > 0){
                   out.println("<a href=\"" + new GlobalMethods().changeSeoURLLink("unilanding", bean.getCollegeName() + GlobalConstants.SEO_URL_SPLIT_CHAR + bean.getCollegeId()) + "\" title=\"overview\"><img src=\"" + CommonUtil.getImgPath(bean.getCollegeLogo(),0) + "\" alt=\"" + bean.getCollegeName() + " profile\"" + topimageid + " title=\"" + bean.getCollegeNameDisplay() + " profile\" class=\"unilogo\" width=\"80\" height=\"80\" align=\"left\" /></a>");
                  }
                }
              }
            } else {
              String latlangvalue = getLatitudeLangtitue(bean.getCollegeId());
              if(latlangvalue !=null && latlangvalue.trim().length()>0){
                  String latLangArr[] = latlangvalue.split(",");
                  if (latLangArr != null && latLangArr.length >= 2) {
                    out.println("<img src=\"http://maps.google.com/maps/api/staticmap?center=54.7211,-3.5943&markers=color:red|label:|"+latLangArr[0]+","+latLangArr[1]+"&zoom=4&size=180x200&sensor=false&language=en-UK&maptype=roadmap&key="+new SessionData().getData(request,"GOOGLE_MAP_KEY")+"\" />");
                    out.println("<p class=\"expandv\"><a rel=\"nofollow\"  href=\"" + request.getContextPath() + "/expand-collegemap.html?z=" + bean.getCollegeId() + "&latval=" + latLangArr[0] + "&langval=" + latLangArr[1] + "\">Expand</a></p>");
                  }
                }    
              }
            if (bean.getOverviewShortDesc() != null && bean.getOverviewShortDesc().trim().length() > 0) {
              if (bean.getOverviewShortDesc().trim().length() > 295) {
                 out.println("<p class=\"clear-1\">" + bean.getOverviewShortDesc().substring(0, 294) + "");
                 out.println("</p>");
                if(videobean.getMyhcProfileIdIp() != null && videobean.getMyhcProfileIdIp().trim().length() > 0){
                 out.println("<a class=\"sblue fright\" href=\"" + new GlobalMethods().changeSeoURLLink("unilanding", bean.getCollegeName() + GlobalConstants.SEO_URL_SPLIT_CHAR + bean.getCollegeId()) + "\" title=\"Find out more\">Find out more</a>");
                 
                }
                 
              } else {
                out.println("<p class=\"clear-1\">" + bean.getOverviewShortDesc() + "</p>");
              }
            }
            out.println("</div>");  
           }else if(videobean.getProfileTypeSp() !=null  && videobean.getProfileTypeSp().trim().length() > 0 && !videobean.getProfileTypeSp().equalsIgnoreCase("null"))
             {
            
               //To display the SP details
              //
              //wu314_20110712_Syed: Stacey-functional-crd#1 - linking to clearing-micro-site            
               //getClearingInfo(out, collegeId);     //Commented By Sekhar K for wu414_20120731
              //               
             // out.println("<h2 class=\"intpod\">");
             // if(videobean.getMyhcProfileIdSp() != null && videobean.getMyhcProfileIdSp().trim().length() > 0){
             // out.println("<a href=\""+contextPath+GlobalConstants.SEO_PATH_SUBJECT_PROFILE_PAGE +new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(bean.getCollegeName()))))+"-overview/"+bean.getCollegeId()+"/overview/"+videobean.getMyhcProfileIdSp()+"/0/"+cpeQualificationNetworkId+"/subjectprofile.html\" title=\"" + bean.getCollegeNameDisplay() + " profile\">" + bean.getCollegeNameDisplay() + " Profile </a> ");
            // }
             // out.println("</h2>");
              out.println("<div>");
              if (request.getAttribute("show-google-map") == null) {
                if (bean.getCollegeLogo() != null && bean.getCollegeLogo().trim().length() >= 0) {
                  if (bean.getLogoType() != null && bean.getLogoType().trim().equalsIgnoreCase("video")) {
                    out.println("<a rel=\"nofollow\" class=\"vhold\" onclick=\"return showVideo('" + bean.getLogoId() + "','" + bean.getCollegeId() + "','B')\" title=\"overview\"><img src=\"" + bean.getVideoThumbUrl() + "\" alt=\"" + bean.getCollegeName() + " profile\"" + topimageid + " title=\"" + bean.getCollegeNameDisplay() + " profile\" width=\"80\" height=\"80\" /><span class=\"vplay5\"><img title=\"\" alt=\"\" src=\"" + contextPath + "/images.png\"/></span></a>");
                    request.setAttribute("showlightbox", "YES");
                  }
                  if (bean.getLogoType() != null && bean.getLogoType().trim().equalsIgnoreCase("image")) {
                    if(videobean.getMyhcProfileIdSp() != null && videobean.getMyhcProfileIdSp().trim().length() > 0){
                     out.println("<a href=\""+contextPath+GlobalConstants.SEO_PATH_SUBJECT_PROFILE_PAGE +new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(bean.getCollegeName()))))+"-overview/"+bean.getCollegeId()+"/overview/"+videobean.getMyhcProfileIdSp()+"/0/"+cpeQualificationNetworkId+"/subjectprofile.html\" title=\"overview\"><img width=\"80\" height=\"80\" src=\"" + CommonUtil.getImgPath(bean.getCollegeLogo(),0)  + "\" alt=\"" + bean.getCollegeName() + " profile\"" + topimageid + " title=\"" + bean.getCollegeNameDisplay() + " profile\" /></a>");
                    }
                  }
                }
              } else {
                String latlangvalue = getLatitudeLangtitue(bean.getCollegeId());
                if(latlangvalue !=null && latlangvalue.trim().length()>0){
                    String latLangArr[] = latlangvalue.split(",");
                    if (latLangArr != null && latLangArr.length >= 2) {
                      out.println("<img src=\"http://maps.google.com/maps/api/staticmap?center=54.7211,-3.5943&markers=color:red|label:|"+latLangArr[0]+","+latLangArr[1]+"&zoom=4&size=180x200&sensor=false&language=en-UK&maptype=roadmap&key="+new SessionData().getData(request,"GOOGLE_MAP_KEY")+"\" />");
                      out.println("<p class=\"expandv\"><a rel=\"nofollow\"  href=\"" + request.getContextPath() + "/expand-collegemap.html?z=" + bean.getCollegeId() + "&latval=" + latLangArr[0] + "&langval=" + latLangArr[1] + "\">Expand</a></p>");
                    }
                  }                  
               }
              if (bean.getOverviewShortDesc() != null && bean.getOverviewShortDesc().trim().length() > 0) {
                if (bean.getOverviewShortDesc().trim().length() > 295) {
                   out.println("<p class=\"unilogo\">" + bean.getOverviewShortDesc().substring(0, 294) + "....&nbsp;");
                  if(videobean.getMyhcProfileIdSp() != null && videobean.getMyhcProfileIdSp().trim().length() > 0){
                   out.println("<a href=\""+contextPath+GlobalConstants.SEO_PATH_SUBJECT_PROFILE_PAGE +new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(bean.getCollegeName()))))+"-overview/"+bean.getCollegeId()+"/overview/"+videobean.getMyhcProfileIdSp()+"/0/"+cpeQualificationNetworkId+"/subjectprofile.html\" title=\"Find out more\">Find out more</a>");
                  }
                   out.println("</p>");
                   
                } else {
                  out.println("<p class=\"unilogo\">" + bean.getOverviewShortDesc() + "</p>");
                }
              }
              out.println("</div>");                 
            }     
          }
        } else { //To Display the video with Ticker Tape for non advertisers
            //16-Sep-2014
            /*if (request.getAttribute("uniLanding") != null) {
              List homeVideoList = new GlobalFunction().loadHomeVideo();//DB_CALL to video list
              ArrayList tickerVideoList = new GlobalFunction().loadTickerVideo();
              request.setAttribute("showlightbox", "YES");
              out.println("<div class=\"you-might-blk\"><h2 class=\"hs-1 fea-uni\"><strong>UNIVERSITY OF THE WEEK</strong></h2>");
              if (homeVideoList != null && homeVideoList.size() > 0) {
                for (int i = 0; i < homeVideoList.size(); i++) {
                  VideoReviewListBean vidBean = (VideoReviewListBean)homeVideoList.get(i);
                  if (i == 0) {
                        out.println("<h3 class=\"hs-4\"><strong><a href=\"" + new GlobalMethods().changeSeoURLLink("unilanding", vidBean.getCollegeName() + GlobalConstants.SEO_URL_SPLIT_CHAR + vidBean.getCollegeId()) + "\" title=\"" + bean.getCollegeNameDisplay() + "\">" + vidBean.getCollegeNameDisplay() + "</a></strong></h3>");
                        if(vidBean.getCollegeLogo() != null){
                          out.println("<div class=\"flogo\"><a href=\"" + new GlobalMethods().changeSeoURLLink("unilanding", vidBean.getCollegeName() + GlobalConstants.SEO_URL_SPLIT_CHAR + vidBean.getCollegeId()) + "\" title=\"" + bean.getCollegeNameDisplay() + "\"><img src=\""+CommonUtil.getImgPath(vidBean.getCollegeLogo(),0)+"\" title=\""+vidBean.getCollegeNameDisplay()+"\" width=\"105\"/></a>");
                          out.println("</div>");
                        }
                  }                        
                  out.println("<div class=\"fvideo\">");
                  out.println("<a rel=\"nofollow\" onclick=\"return showVideo('" + vidBean.getMediaId() + "','" + vidBean.getCollegeId() + "', 'B');\" title='" + vidBean.getVideoReviewTitle() + "' class=\"vhold\" ><span class=\"vplay2\"><img src=\"" + CommonUtil.getImgPath("",0) + "/wu-cont/images/playvideo.png\" /></span><img src=\"" + vidBean.getThumbnailUrl() + "\"  width=\"205\" height=\"160\" class=\"bigimg\" alt=\"\" title=\"\" /></a> ");
                  if (tickerVideoList != null && tickerVideoList.size() > 0) {
                     String userAgent = request.getHeader( "User-Agent" );
                     boolean isFirefox = ( userAgent != null && userAgent.indexOf( "Firefox/" ) != -1 );
                     boolean isMSIE = ( userAgent != null && userAgent.indexOf( "MSIE" ) != -1 );
                    out.println("<script language=\"javascript\" type=\"text/javascript\">");
                    out.println("var sectionIndex = parseInt(0);var center = 210;var sectionWidth = 100;var sectionCount = parseInt(" + tickerVideoList.size() + ");");
                    out.println("</script>");
                    out.println("</div></div>");
                    //for more videos
                    out.println("<div class=\"more-video mvd\"><div class=\"fvideo-top\">");
                    out.println("<strong>More Uni Videos</strong>");
                    out.println("</div>");
                    out.println("<div class=\"video-scroller\">");
                    for (int j = 0; j < tickerVideoList.size(); j++) {
                      VideoReviewListBean tickerVidBean = (VideoReviewListBean)tickerVideoList.get(j);
                      if(j==0){
                          out.println("<div class=\"plzr\">");
                      }
                      else { out.println("<div>"); }
                      out.println("<a class=\"vhold\" title=\"" + tickerVidBean.getCollegeNameDisplay() + "\" onclick=\"return showVideo('" + tickerVidBean.getVideoReviewId() + "','" + tickerVidBean.getCollegeId() + "');\"><span class=\"vplay3\"><img src=\"" + CommonUtil.getImgPath("",0) + "/wu-cont/images/playvideo.png\" /></span><img class=\"scrollimg\" width=\"88\" height=\"88\"  src='" + tickerVidBean.getThumbnailUrl() + "' /></a></a></div>");
                    }
                    out.println("</div>");
                    out.println("<input type=\"hidden\" name=\"currentdirection\" id=\"currentdirection\" value=\"left\" />");
                    out.println("<script language=\"javascript\" type=\"text/javascript\">");
                    if(isFirefox){
                        out.println("InitScripts('scrollmain'); justifySelectedSection('scrollmain');zxcCngDirection('scrollmain',1, -3);");
                    }else{
                        out.println("InitScripts('scrollmain'); justifySelectedSection('scrollmain');zxcCngDirection('scrollmain',1, -1);");
                    }
                    out.println("</script>");
                  }
                  out.println("</div>");
                }
              }
            }*/
          }
        }
            
         if(resultType != null && resultType.equalsIgnoreCase("subject-profile-interaction")) {
          subjectProfileList =  new AdvertUtilities().getAvailableSubjectProfileList(collegeId,cpeQualificationNetworkId);
          if(subjectProfileList != null && subjectProfileList.size() > 0){
              request.setAttribute("subjectProfileList", subjectProfileList);
          } 
          if(subjectProfileList != null && subjectProfileList.size() > 0) {
              out.println("<div class=\"times-blk\">");
              out.println("<h2 class=\"hc-1\">");
              //out.println("<a  rel=\"nofollow\" href=\"" + contextPath + "/subprofilelist.html?z=" + bean.getCollegeId() + "\" title=\"" + bean.getCollegeNameDisplay() + " subject profile\">Subject profile</a></h2>");
              out.println("Subject profiles</h2>");
              out.println("<ul class=\"top-crs-list\">");
              Iterator subjectiterator = subjectProfileList.iterator();
              while (subjectiterator.hasNext()) {
                SearchBean subjectbean = (SearchBean)subjectiterator.next();
                if(subjectbean.getMyhcProfileId() != null && subjectbean.getMyhcProfileId().trim().length() > 0){
                out.println("<li> <a href=\""+ contextPath + GlobalConstants.SEO_PATH_SUBJECT_PROFILE_PAGE +new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(new CommonUtil().toLowerCase(bean.getCollegeName())))) +"-overview/"+ bean.getCollegeId() +"/overview/"+subjectbean.getMyhcProfileId()+"/0/"+cpeQualificationNetworkId+"/subjectprofile.html\" title=\"" + subjectbean.getAdvertName() + "\" >" + subjectbean.getAdvertName()+"</a> ");
                out.println("</li>");
                }
              }
              out.println("</ul>");
              out.println("</div>");
          }
        }
    
        if (resultType != null && resultType.equalsIgnoreCase("university-email-interaction")) { //To display the college profile         
          if(InstitutionProfileList != null && InstitutionProfileList.size() > 0) {
          if(videobean.getSubOrderItemIdForIp() != null && videobean.getSubOrderItemIdForIp().trim().length() > 0 && !videobean.getSubOrderItemIdForIp().equalsIgnoreCase("NULL")){
          if(videobean.getSubOrderEmailWebformIp() != null && videobean.getSubOrderEmailWebformIp().trim().length() > 0){
            out.println("<div class=\"uni-porfile-blk\">");
            out.println("<h2 class=\"hdr mail\" title=\"Email " + bean.getCollegeNameDisplay() + "\">");
            String webformPrice = new CommonFunction().getGACPEPrice(videobean.getSubOrderItemIdForIp(), "webform_price");
            out.println("<strong><a rel=\"nofollow\" target=\"_blank\" href='"+videobean.getSubOrderEmailWebformIp()+"' onclick=\"GAInteractionEventTracking('emailwebform', 'interaction', 'Webclick','"+new CommonFunction().replaceSpecialCharacter(bean.getCollegeName())+"',"+webformPrice+"); cpeEmailWebformClick(this,'"+bean.getCollegeId()+"','"+videobean.getSubOrderItemIdForIp()+"','"+cpeQualificationNetworkId+"','"+videobean.getSubOrderEmailWebformIp()+"'); var a='s.tl(';\" title=\"Email " + bean.getCollegeNameDisplay() + "\" >Email uni</a></strong>");
            out.println("</h2>");
            out.println("</div>");                
          }else{
            out.println("<div class=\"uni-porfile-blk\">");
            out.println("<h2 class=\"hdr mail\" title=\"Email " + bean.getCollegeNameDisplay() + "\">");
            //out.println("<strong><a rel=\"nofollow\" href=\"" + contextPath + "/send-college-email.html?z=" + bean.getCollegeId()+ "&amp;subOrderItemId="+ videobean.getSubOrderItemIdForIp() + "\" title=\"Email " + bean.getCollegeNameDisplay() + "\" >Email uni</a></strong>");
            out.println("<a  onclick=\"GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '"+new CommonFunction().replaceSpecialCharacter(bean.getCollegeName())+"');\" rel=\"nofollow\" href=\"" + contextPath + "/email/"+new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(bean.getCollegeName())))+"-email/"+bean.getCollegeId()+"/0/0/n-"+videobean.getSubOrderItemIdForIp()+"/send-college-email.html" + "\" title=\"Email " + bean.getCollegeNameDisplay() + "\" >Email uni</a>");
            out.println("</h2>");
            out.println("</div>"); 
          }
          }
         }
        }
      //
       if (resultType != null && resultType.equalsIgnoreCase("university-sch-email-interaction")) { //Added for 13-JAN-2014 Release     
         if(InstitutionProfileList != null && InstitutionProfileList.size() > 0) {
         if(videobean.getSubOrderItemIdForIp() != null && videobean.getSubOrderItemIdForIp().trim().length() > 0 && !videobean.getSubOrderItemIdForIp().equalsIgnoreCase("NULL")){
         if(videobean.getSubOrderEmailWebformIp() != null && videobean.getSubOrderEmailWebformIp().trim().length() > 0){          
           String webformPrice = new CommonFunction().getGACPEPrice(videobean.getSubOrderItemIdForIp(), "webform_price");
           out.println("<strong><a  class=\"req-inf\"  rel=\"nofollow\" target=\"_blank\" href='"+videobean.getSubOrderEmailWebformIp()+"' onclick=\"GAInteractionEventTracking('emailwebform', 'interaction', 'Webclick','"+new CommonFunction().replaceSpecialCharacter(bean.getCollegeName())+"',"+webformPrice+"); cpeEmailWebformClick(this,'"+bean.getCollegeId()+"','"+videobean.getSubOrderItemIdForIp()+"','"+cpeQualificationNetworkId+"','"+videobean.getSubOrderEmailWebformIp()+"'); var a='s.tl(';\" title=\"Email " + bean.getCollegeNameDisplay() + "\" ></a></strong>");                       
         }else{
           out.println("<a  class=\"req-inf\" onclick=\"GAInteractionEventTracking('emailbutton', 'engagement', 'Email-Request', '"+new CommonFunction().replaceSpecialCharacter(bean.getCollegeName())+"');\" rel=\"nofollow\" href=\"" + contextPath + "/email/"+new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(bean.getCollegeName())))+"-email/"+bean.getCollegeId()+"/0/0/n-"+videobean.getSubOrderItemIdForIp()+"/send-college-email.html" + "\" title=\"Email " + bean.getCollegeNameDisplay() + "\" ></a>");
         }
         }
        }
       }
      //
        
        
        if (resultType != null && resultType.equalsIgnoreCase("university-interactive-competitors")) { //To display the college competitors 
         if(InstitutionProfileList != null && InstitutionProfileList.size() > 0) {
           if(videobean.getSubOrderItemIdForIp() != null && videobean.getSubOrderItemIdForIp().trim().length()>0 && !videobean.getSubOrderItemIdForIp().equalsIgnoreCase("NULL"))
           {
             if(videobean.getProfileTypeIp() !=null  &&  videobean.getProfileTypeIp().trim().length() > 0 && !videobean.getProfileTypeIp().equalsIgnoreCase("null"))
             {
            List competitorsList = new CommonFunction().getCompetitorsPod(collegeId, request);
            if (competitorsList != null && competitorsList.size() > 0) {
              Iterator competitorsiterator = competitorsList.iterator();
              out.println("<div id=\"spons\" class=\"top-crs-blk\">");
              out.println("<h2 class=\"hc-1 mb-10\">Students interested in this uni are also interested in</h2><ul class=\"top-crs-list\">");
              while (competitorsiterator.hasNext()) {
                SearchResultBean competitorbean = (SearchResultBean)competitorsiterator.next();
                out.println("<li><a href=\"" + new GlobalMethods().changeSeoURLLink("unilanding", competitorbean.getCollegeName() + GlobalConstants.SEO_URL_SPLIT_CHAR + competitorbean.getCollegeId()) + "\" title=\"" + competitorbean.getCollegeNameDisplay() + "\">" + competitorbean.getCollegeNameDisplay() + "</a></li>");
              }
              out.println("</ul>");
              out.println("</div>");
            }
           }
          }
         } 
        }
        if (resultType != null && resultType.equalsIgnoreCase("university-interactive-reviews")) { //To display the college competitors 
          if (request.getAttribute("hide_review_rating") == null) {
            //if (bean.getCollegeProfileFlag() != null && !bean.getCollegeProfileFlag().equalsIgnoreCase("TRUE")) {
           if(InstitutionProfileList != null && InstitutionProfileList.size() > 0) {
             if(videobean.getSubOrderItemIdForIp() != null && videobean.getSubOrderItemIdForIp().trim().length()>0 && !videobean.getSubOrderItemIdForIp().equalsIgnoreCase("NULL"))
             {
               if(videobean.getProfileTypeIp() !=null  &&  videobean.getProfileTypeIp().trim().length() > 0 && !videobean.getProfileTypeIp().equalsIgnoreCase("null"))
               {
              //out.println("<h2 class=\"intpod\">");
              //out.println("<a href=\"" + new GlobalMethods().changeSeoURLLink("unilanding", bean.getCollegeName() + GlobalConstants.SEO_URL_SPLIT_CHAR + bean.getCollegeId()) + "\" title=\"" + bean.getCollegeNameDisplay() + "\">" + bean.getCollegeNameDisplay() + "</a>");
              //out.println("</h2>");
              }
            }
          } 
            if (bean.getReviewCount() != null && !bean.getReviewCount().equals("0") && bean.getReviewCount().trim().length() > 0) {
              out.println("<div class=\"you-might-blk\" id=\"reviewright\">");
              String orating = "rate-" + bean.getOverAllRating();
              out.println("Overall rating:");
              //out.println("<a rel=\"nofollow\" href=\"" + contextPath + "/uniwrittenreviews.html?z=" + (bean.getCollegeId() != null? bean.getCollegeId().trim(): bean.getCollegeId()) + "\" title=\"View " + bean.getCollegeNameDisplay() + " review(s)\">");
              out.println("<a href=\"" + contextPath + new CommonFunction().uniLandingReviewSEOUrl(bean.getCollegeId(), bean.getCollegeName())+ "\" title=\"View " + bean.getCollegeNameDisplay() + " review(s)\">");
              out.println("<span class=\"" + orating + "\" title=\"View " + bean.getCollegeNameDisplay() + " review(s)\"></span>");
              out.println("</a>");
              out.println("Based on <a href=\"" + contextPath + new CommonFunction().uniLandingReviewSEOUrl(bean.getCollegeId(), bean.getCollegeName())+ "\" title=\"View " + bean.getCollegeNameDisplay() + " review(s)\">" + bean.getReviewCount() + " review(s)</a>");
              out.println("</div>");
            }
          }
        }
       if(InstitutionProfileList != null && InstitutionProfileList.size() > 0) {
        if(videobean.getSubOrderItemIdForIp() != null && videobean.getSubOrderItemIdForIp().trim().length()>0 && !videobean.getSubOrderItemIdForIp().equalsIgnoreCase("NULL"))
        {
          if(videobean.getProfileTypeIp() !=null  &&  videobean.getProfileTypeIp().trim().length() > 0 && !videobean.getProfileTypeIp().equalsIgnoreCase("null"))
          {
          request.removeAttribute("providerResult");
          }
        } 
       } 
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return EVAL_BODY_BUFFERED;
  }
  public int doAfterBody() throws JspException {
    try {
      BodyContent bodycontent = getBodyContent();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }
  public int doEndTag() {
    return EVAL_PAGE;
  }
  private String getLatitudeLangtitue(String collegeId) {
    String latLang = "";
    Vector vector = new Vector();
    try {
      vector.add(collegeId);
      latLang = new DataModel().getString("wu_util_pkg.get_college_lat_long_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
    }
    return latLang;
  }
  
  //
  private String getClearingInfo(JspWriter out, String collegeId) { 
    //wu314_20110712_Syed: Stacey-functional-crd#1 - linking to clearing-micro-site
    ArrayList clearingInfoList = new AffiliateUtilities().getClearingInfoList(collegeId);
    String clearingInfo = "";
    if (clearingInfoList != null && clearingInfoList.size() > 0) {
        ClearingInfoVO clearingInfoVO = null;
        for(int i=0; i<1; i++ ){
          clearingInfoVO = (ClearingInfoVO)clearingInfoList.get(i);      
          clearingInfo = clearingInfo + "<a rel=\"nofollow\" target=\"_blank\" href=\"" + clearingInfoVO.getClearingProfileUrl() + "\" title=\"Clearing 2011\" >";
          clearingInfo = clearingInfo + "<img src=\""+CommonUtil.getImgPath("",0)+"/img/whatuni/sprites/clearing/clearinglogo.png\" class=\"clrin\" width=\"180\" height=\"52\" alt=\"Clearing 2011\" /></a>";
        }
      try{
        out.println(clearingInfo);
      }catch(Exception e){
        e.printStackTrace();
      }
    }    
    return clearingInfo;
  }
  
  public void setResultType(String resultType) {
    this.resultType = resultType;
  }
  public String getResultType() {
    return resultType;
  }
}
