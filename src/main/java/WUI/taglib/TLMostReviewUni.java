package WUI.taglib;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

import WUI.homepage.form.HomePageBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

/**
  * @TLMostReviewUni.java
  * @Version : 2.0
  * @September 20 2007
  * @www.whatuni.com
  * @Created By : Balraj. Selva Kumar
  * @Purpose  : Display the most reviewed university pod.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class TLMostReviewUni extends BodyTagSupport {
  public TLMostReviewUni() {
  }
  public int doStartTag() throws JspException {
    HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
    String contextPath = request.getContextPath();
    try {
      JspWriter out = pageContext.getOut();
      List mostReviewUniList = null;
      mostReviewUniList = new CommonFunction().getMostReviewedUni(request);
      if (mostReviewUniList != null && mostReviewUniList.size() > 0) {
        String awardsimage[] = new String[] { "OVERALL_10.png", "STUD-UNION_10.png", "CLUBS-SOCIETIES_10.png", 
                                              "ACCOMMODATION_10.png", "UNI-FACILITY_10.png", "COURSE-LECTURER_10.png", 
                                              "EYE-CANDY_10.png", "CITY-LIFE_10.png", "JOB-PROSPECTS_10.png" };
        /*4  Overall Rating
               5  Student Union
               6  Clubs and Societies
               7  Accommodation
               8  Uni Facilities
               9  Course and Lecturers
               10 Eye Candy
               11 City Life
               12 Job Prospects*/
        String awardquestionid = (String)request.getAttribute("awardsquestionid");
        awardquestionid = awardquestionid != null && awardquestionid.trim().length() > 0? awardquestionid: "4";
        int questionId = Integer.parseInt(awardquestionid);
        out.println("<div class=\"mostreviews\"  id=\"mostReviewUni\">");
        out.println("<h2 class=\"awards\"><a href=\"" + contextPath + "/uk-best-university-ranking/2011/ur.html\" title=\"Student Choice Awards 2009\">Student Choice Awards 2009</a></h2>");
        out.println("<ol type=\"1\" start=\"0\">");
        out.println("<li class=\"image\"><a href=\"" + contextPath + "/uk-best-university-ranking/2011/ur.html\"><img class=\"logo\" src=\"" + CommonUtil.getImgPath("",0) + "/wu-cont/img/whatuni/awards/" + awardsimage[questionId - 4] + "\" alt=\"\" title=\"Student Choice Awards 2009\" /></a></li>");
        Iterator uniiterator = mostReviewUniList.iterator();
        while (uniiterator.hasNext()) {
          HomePageBean bean = (HomePageBean)uniiterator.next();
          String seoCollegeName = new CommonFunction().replaceHypen(new CommonFunction().replaceURL(bean.getCollegeName())).replaceAll(" ", "-") + "-";
          out.println("<li>");
          out.println("<a href=\"" + contextPath + GlobalConstants.SEO_PATH_REVIEW_CATEGORY_PAGE + seoCollegeName + bean.getSeoQuestionText() + "-reviews/" + bean.getCollegeId() + "/" + bean.getQuestionId() + "/1/highest_rated/reviewcategory.html\"" + " title=\"" + bean.getCollegeName() + "\">" + bean.getCollegeName() + "</a>");
          out.println("</li>");
        }
        out.println("</ol> ");
        out.println("<p class=\"center_txt\"><a href=\"" + contextPath + "/uk-best-university-ranking/2011/ur.html\" title=\"View all award results\">View all award results</a></p>");
        out.println("</div>");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return EVAL_BODY_BUFFERED;
  }
  public int doAfterBody() throws JspException {
    try {
      BodyContent bodycontent = getBodyContent();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return SKIP_BODY;
  }
  public int doEndTag() {
    return EVAL_PAGE;
  }
}
