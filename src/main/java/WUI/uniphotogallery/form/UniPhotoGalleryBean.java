package WUI.uniphotogallery.form;

import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;

public class UniPhotoGalleryBean {

  private FormFile uniphotoUpload = null;
  private String author = "";
  private String imagename = "";
  private String tags = "";
  private String imagePath = "";
  private String imageId = "";
  private String keyword = "";
  private String shortTag = "";
  private String imageThumbPath = "";
  private int imageHeight;
  private int imageWidth;
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String dateCreated = "";
  private String shortImageName = "";

  public void setUniphotoUpload(FormFile uniphotoUpload) {
    this.uniphotoUpload = uniphotoUpload;
  }

  public FormFile getUniphotoUpload() {
    return uniphotoUpload;
  }

/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    if (request.getParameter("uploadimage") != null) {
      String contentExt = "";
      FormFile myFile = this.getUniphotoUpload();
      String contentType = myFile.getContentType();
      String fileName = myFile.getFileName();
      int fileSize = myFile.getFileSize();
      if (fileName == null || (fileName != null && fileName.trim().length() == 0)) {
        errors.add("user.uni.upload.image.empty", new ActionMessage("user.uni.upload.image.empty"));
      }
      if (author == null || (author != null && author.trim().length() == 0)) {
        errors.add("user.uni.upload.author.empty", new ActionMessage("user.uni.upload.author.empty"));
      }
      if (imagename == null || (imagename != null && imagename.trim().length() == 0)) {
        errors.add("user.uni.upload.imagename.empty", new ActionMessage("user.uni.upload.imagename.empty"));
      }
      if (tags == null || (tags != null && tags.trim().length() == 0)) {
        errors.add("user.uni.upload.tags.empty", new ActionMessage("user.uni.upload.tags.empty"));
      }
      if (fileName.trim().length() > 0) {
        boolean flag = false;
        if ((contentType != null) && (fileName.trim().length() > 0)) {
          StringTokenizer st = new StringTokenizer(contentType, "/");
          st.nextToken("/");
          contentExt = st.nextElement().toString();
          if (contentExt.equalsIgnoreCase("pjpeg")) {
            flag = true;
          }
          if (contentExt.equalsIgnoreCase("gif")) {
            flag = true;
          }
          if (contentExt.equalsIgnoreCase("x-png")) {
            flag = true;
          }
          if (contentExt.equalsIgnoreCase("jpeg")) {
            flag = true;
          }
          if (!flag) {
            errors.add("user.uni.upload.image.type.error", new ActionMessage("user.uni.upload.image.type.error"));
          }
        }
        if (fileSize > (1024 * 1024)) {
          errors.add("user.uni.upload.image.size.error", new ActionMessage("user.uni.upload.image.size.error"));
        }
      }
    }
    if (errors != null && errors.size() > 0) {
      String collegeId = request.getParameter("z") != null ? request.getParameter("z").toString() : "";
      String collegeName = new CommonFunction().getCollegeName(collegeId, request);
      request.setAttribute("collegeName", collegeName);
      request.setAttribute("interactive-college-id", collegeId);
      request.setAttribute("collegeId", collegeId);
      new GlobalFunction().getInteractivePodInfo(collegeId, request);
    }
    return errors;
  }*/

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getAuthor() {
    return author;
  }

  public void setImagename(String imagename) {
    this.imagename = imagename;
  }

  public String getImagename() {
    return imagename;
  }

  public void setTags(String tags) {
    this.tags = tags;
  }

  public String getTags() {
    return tags;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImageId(String imageId) {
    this.imageId = imageId;
  }

  public String getImageId() {
    return imageId;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setShortTag(String shortTag) {
    this.shortTag = shortTag;
  }

  public String getShortTag() {
    return shortTag;
  }

  public void setImageThumbPath(String imageThumbPath) {
    this.imageThumbPath = imageThumbPath;
  }

  public String getImageThumbPath() {
    return imageThumbPath;
  }

  public void setImageHeight(int imageHeight) {
    this.imageHeight = imageHeight;
  }

  public int getImageHeight() {
    return imageHeight;
  }

  public void setImageWidth(int imageWidth) {
    this.imageWidth = imageWidth;
  }

  public int getImageWidth() {
    return imageWidth;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setDateCreated(String dateCreated) {
    this.dateCreated = dateCreated;
  }

  public String getDateCreated() {
    return dateCreated;
  }

  public void setShortImageName(String shortImageName) {
    this.shortImageName = shortImageName;
  }

  public String getShortImageName() {
    return shortImageName;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

}
