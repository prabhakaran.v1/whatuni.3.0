package WUI.registration.validator;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import WUI.registration.bean.UserLoginBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.wuni.util.sql.DataModel;

public class UserLoginValidator  implements Validator{

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
	
		UserLoginBean bean = (UserLoginBean) target;
		HttpServletRequest request = bean.getRequest();
		
	    if (request.getParameter("buttext") != null && request.getParameter("buttext").equals("Sign in")) {
	        //Modified username validation by Indumathi.S Jan-27-16 Rel
	        if (GenericValidator.isBlankOrNull(bean.getUserName())) {
	        	//request.setAttribute("invaliduser", ResourceBundle.getBundle("").getString("wuni.error.blankusername"));	
	          errors.rejectValue("userName", "wuni.error.blankusername");
	        } else if(!GenericValidator.isBlankOrNull(bean.getUserName()) && !CommonUtil.isValidEmail(bean.getUserName())) {
	        	// request.setAttribute("invaliduser", ResourceBundle.getBundle("").getString("wuni.error.notvalidemail"));
	          errors.rejectValue("userName", "wuni.error.notvalidemail");
	        }
	        if (bean.getPassword() == null || bean.getPassword().equals("") || bean.getPassword().trim().length() == 0) {
	        	//request.setAttribute("invaliduser", ResourceBundle.getBundle("").getString("wuni.error..blankpassword"));
	          errors.rejectValue("password", "wuni.error..blankpassword");
	        }
	      }
	      if (request.getParameter("buttext") != null && request.getParameter("buttext").equals("Send me my password")) {
	        if (bean.getEmailId() == null || bean.getEmailId().equals("") || bean.getEmailId().trim().length() == 0) {
	        	// request.setAttribute("invalidloginemail", ResourceBundle.getBundle("").getString("wuni.login.message.invalidemail"));
	          errors.rejectValue("invalidloginemail", "wuni.login.message.invalidemail");
	        }
	      }
	      /* Start of code added for wu_537 24-FEB-2015 by Karthi for forgot password validation */ 
	      if (request.getParameter("buttext") != null && request.getParameter("buttext").equals("Send password")) {
	        if (bean.getEmailId() == null || bean.getEmailId().equals("") || bean.getEmailId().trim().length() == 0) {
	         errors.rejectValue("invalidEmail", "wuni.error.blank.email");
	        }else if(!new CommonUtil().isValidEmail(bean.getEmailId().trim())){ // emailId trim() is added by Prabha on 03-Nov-2015
	          errors.rejectValue("invalidEmail", "wuni.error.invalid.email");
	        }
	      }
	    
		
	}

}
