package WUI.registration.bean;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

/**
* @RegistrationBean.java
* @Version : 2.0
* @May 30, 2007
* @www.whatuni.com
* @Created By : Kalikumar Modified by Balraj. Selva Kumar
* @Purpose    : Program used to store/retrive the information in the set/get method. used to handle the data related to registration.
*
* *****************************************************************************************************
* Date           Name                      Ver.     Changes desc                               Rel Ver.
* ******************************************************************************************************
* 23-Oct-2018    Sabapathi S.              1.3      Added screen width in stats logging        wu_582
*/
public class RegistrationBean {
  public RegistrationBean() {
  }
  private String filedId = null;
  private String fieldType = null;
  private String questionLabel = null;
  private String displaySeqId = null;
  private String fieldRequired = null;
  private String perRowCount = null;
  private String defaultValue = null;
  private String templateIdHint = null;
  private String rowStatus = null;
  private String maxSize = null;
  private String displaySize = null;
  private String optionId = null;
  private String optionText = null;
  private List optionValues = null;
  private String editUserImage = null;
  private String staticEmailAddress = null;
  private String selectedEmailAddress = null;
  private String selectedValues = null;
  private FormFile fileUpload = null;
  private String selectFileUpload = "browse";
  private String defaultImageUpload = null;
  private List yearOfUniEntryList = null;
  private String yearOfUniEntry = null;
  private String yearOfPassing = null;
  private boolean termsAndCondition=false;
  private String userName = null;
  private String password = null;
  private String emailAddress = null;
  private boolean sendMeUpdates=false;
 // private boolean newsLetter = false;
  private String newsLetter = null;
  public String getNewsLetter() {
	return newsLetter;
  }
  public void setNewsLetter(String newsLetter) {
	this.newsLetter = newsLetter;
  }
private String subOrderItemId = null;
  private String clientIp = null;
  private String clientBrowser = null;
  private String refferalURL = null;
  private String requestURL = null;
  //
  private String firstName = null;
  private String lastName = null;
  private String surName = null;
  private String sessionId = null;
  private String userId = null;
  private String submitType = null;
  private String confirmPassword = null;
  private String newsLetterFlag = null;
  private String trackSessionId = null;
  private String yeartoJoinCourse  = null;//Added by Priyaa for 8_OCT_2014_REL for year of entry addition
  private String fbUserId = null; //Added FB user id to store and generate FB image path for 19_May_2015, by Thiyagu G.
  
   //Added for 03_Nov_2015, By Thiyagu G.
   private String addressLine1 = null;
   private String addressLine2 = null;
   private String town = null;
   private String postCode = null;
   private String phoneNumber = null;
   private String nationality = null;
   private String countryOfResidence = null;
   private String dateOfBirth = null;
  private List nationalityList = null;
  private List countryOfResList = null;
  private String date = null;
  private String month = null;
  private String year = null;
  private List dateList = null;
  private List monthList = null;
  private List yearList = null;
  
  private String gradeTypeAttrId = null;
  private String gradeTypeAttrValue = null;
  private List gradeTypeList = null;
  private String gradesAttrId = null;
  private String gradesAttrValues = null;
	
 	private String userCMMType = null;
  private String widgetId = null;
  private String pageId = null;
  private String collegeId = null;
  private String affilateId = null;
  private String reviewId = null;
  private String categoryCode = null;
  private String basketId = null;
  
  private String marketingEmailFlag = null;
  private String solusEmailFlag = null;
  private String surveyEmailFlag = null;
  private String screenWidth = null;
  private String subjectSessionId = null;
  private String latitude = null;
  private String longitude = null;
  private String userUcasScore = null;
  
  public String getUserUcasScore() {
	return userUcasScore;
  }
  public void setUserUcasScore(String userUcasScore) {
	this.userUcasScore = userUcasScore;
  }
public String getLatitude() {
	return latitude;
}
public void setLatitude(String latitude) {
	this.latitude = latitude;
}
public String getLongitude() {
	return longitude;
}
public void setLongitude(String longitude) {
	this.longitude = longitude;
}
public void reset(ActionMapping mapping, HttpServletRequest request) {
    this.defaultImageUpload = "";
  }
  public void setSelectedValue(String selectedValues) {
    this.selectedValues = selectedValues;
  }
  public String getSelectedValue() {
    return selectedValues;
  }
  public void setTheFileUpload(FormFile fileUpload) {
    this.fileUpload = fileUpload;
  }
  public FormFile getTheFileUpload() {
    return fileUpload;
  }
  public void setSelectFileUpload(String selectFileUpload) {
    this.selectFileUpload = selectFileUpload;
  }
  public String getSelectFileUpload() {
    return selectFileUpload;
  }
  public void setDefaultImageUpload(String defaultImageUpload) {
    this.defaultImageUpload = defaultImageUpload;
  }
  public String getDefaultImageUpload() {
    return defaultImageUpload;
  }
  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }
  public String getOptionId() {
    return optionId;
  }
  public void setOptionText(String optionText) {
    this.optionText = optionText;
  }
  public String getOptionText() {
    return optionText;
  }
  public void setOptionValues(List optionValues) {
    this.optionValues = optionValues;
  }
  public List getOptionValues() {
    return optionValues;
  }
  public void setFiledId(String filedId) {
    this.filedId = filedId;
  }
  public String getFiledId() {
    return filedId;
  }
  public void setFieldType(String fieldType) {
    this.fieldType = fieldType;
  }
  public String getFieldType() {
    return fieldType;
  }
  public void setQuestionLabel(String questionLabel) {
    this.questionLabel = questionLabel;
  }
  public String getQuestionLabel() {
    return questionLabel;
  }
  public void setDisplaySeqId(String displaySeqId) {
    this.displaySeqId = displaySeqId;
  }
  public String getDisplaySeqId() {
    return displaySeqId;
  }
  public void setFieldRequired(String fieldRequired) {
    this.fieldRequired = fieldRequired;
  }
  public String getFieldRequired() {
    return fieldRequired;
  }
  public void setPerRowCount(String perRowCount) {
    this.perRowCount = perRowCount;
  }
  public String getPerRowCount() {
    return perRowCount;
  }
  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }
  public String getDefaultValue() {
    return defaultValue;
  }
  public void setTemplateIdHint(String templateIdHint) {
    this.templateIdHint = templateIdHint;
  }
  public String getTemplateIdHint() {
    return templateIdHint;
  }
  public void setRowStatus(String rowStatus) {
    this.rowStatus = rowStatus;
  }
  public String getRowStatus() {
    return rowStatus;
  }
  public void setMaxSize(String maxSize) {
    this.maxSize = maxSize;
  }
  public String getMaxSize() {
    return maxSize;
  }
  public void setDisplaySize(String displaySize) {
    this.displaySize = displaySize;
  }
  public String getDisplaySize() {
    return displaySize;
  }
  public void setEditUserImage(String editUserImage) {
    this.editUserImage = editUserImage;
  }
  public String getEditUserImage() {
    return editUserImage;
  }
  public void setStaticEmailAddress(String staticEmailAddress) {
    this.staticEmailAddress = staticEmailAddress;
  }
  public String getStaticEmailAddress() {
    return staticEmailAddress;
  }
  public void setSelectedEmailAddress(String selectedEmailAddress) {
    this.selectedEmailAddress = selectedEmailAddress;
  }
  public String getSelectedEmailAddress() {
    return selectedEmailAddress;
  }
/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    
    return errors;
  } */ 
  
  public boolean isValidPassword(String password) {
    boolean errorFlag = false;
    errorFlag = password != null && password.trim().length() > 0? true: false;
    return errorFlag;
  }
  public void setYearOfUniEntryList(List yearOfUniEntryList) {
    this.yearOfUniEntryList = yearOfUniEntryList;
  }
  public List getYearOfUniEntryList() {
    return yearOfUniEntryList;
  }
  public void setSelectedValues(String selectedValues) {
    this.selectedValues = selectedValues;
  }
  public String getSelectedValues() {
    return selectedValues;
  }
  public void setFileUpload(FormFile fileUpload) {
    this.fileUpload = fileUpload;
  }
  public FormFile getFileUpload() {
    return fileUpload;
  }
  
  public void setYearOfPassing(String yearOfPassing) {
    this.yearOfPassing = yearOfPassing;
  }
  public String getYearOfPassing() {
    return yearOfPassing;
  }
  public void setTermsAndCondition(boolean termsAndCondition) {
    this.termsAndCondition = termsAndCondition;
  }
  public boolean isTermsAndCondition() {
    return termsAndCondition;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }
  public String getUserName() {
    return userName;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public String getPassword() {
    return password;
  }
  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }
  public String getEmailAddress() {
    return emailAddress;
  }
  public void setYearOfUniEntry(String yearOfUniEntry) {
    this.yearOfUniEntry = yearOfUniEntry;
  }
  public String getYearOfUniEntry() {
    return yearOfUniEntry;
  }
  
  public void setSendMeUpdates(boolean sendMeUpdates) {
      this.sendMeUpdates = sendMeUpdates;
    }

    public boolean isSendMeUpdates() {
        return sendMeUpdates;
    }

   /** public void setNewsLetter(boolean newsLetter) {
        this.newsLetter = newsLetter;
    }

    public boolean isNewsLetter() {
        return newsLetter;
    }
*/
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  
  public void setFirstName(String firstName){
    this.firstName = firstName;
  }
  
  public String getFirstName(){
    return firstName;
  }
  
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  
  public String getLastName() {
    return lastName;
  }
  
  public void setSurName(String surName) {
    this.surName = surName;
  }
  
  public String getSurName() {
    return surName;
  }
  
  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }
  
  public String getSessionId() {
    return sessionId;
  }
  
  public void setUserId(String userId) {
    this.userId = userId;
  }
  
  public String getUserId() {
    return userId;
  }
  
  public void setSubmitType(String submitType) {
    this.submitType = submitType;
  }
  
  public String getSubmitType() {
    return submitType;
  }
  
  public void setConfirmPassword(String confirmPassword) {
    this.confirmPassword = confirmPassword;
  }
  
  public String getConfirmPassword() {
    return confirmPassword;
  }
  
  public void setNewsLetterFlag(String newsLetterFlag) {
    this.newsLetterFlag = newsLetterFlag;
  }
  
  public String getNewsLetterFlag() {
    return newsLetterFlag;
  }
  public void setClientIp(String clientIp) {
    this.clientIp = clientIp;
  }
  public String getClientIp() {
    return clientIp;
  }
  public void setClientBrowser(String clientBrowser) {
    this.clientBrowser = clientBrowser;
  }
  public String getClientBrowser() {
    return clientBrowser;
  }
  public void setRefferalURL(String refferalURL) {
    this.refferalURL = refferalURL;
  }
  public String getRefferalURL() {
    return refferalURL;
  }
  public void setRequestURL(String requestURL) {
    this.requestURL = requestURL;
  }
  public String getRequestURL() {
    return requestURL;
  }
  public void setTrackSessionId(String trackSessionId) {
    this.trackSessionId = trackSessionId;
  }
  public String getTrackSessionId() {
    return trackSessionId;
  }
  public void setYeartoJoinCourse(String yeartoJoinCourse) {
    this.yeartoJoinCourse = yeartoJoinCourse;
  }
  public String getYeartoJoinCourse() {
    return yeartoJoinCourse;
  }
  public void setFbUserId(String fbUserId) {
    this.fbUserId = fbUserId;
  }
  public String getFbUserId() {
    return fbUserId;
  }
  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }
  public String getAddressLine1() {
    return addressLine1;
  }
  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }
  public String getAddressLine2() {
    return addressLine2;
  }
  public void setTown(String town) {
    this.town = town;
  }
  public String getTown() {
    return town;
  }
  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }
  public String getPostCode() {
    return postCode;
  }
  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }
  public String getPhoneNumber() {
    return phoneNumber;
  }
  public void setNationality(String nationality) {
    this.nationality = nationality;
  }
  public String getNationality() {
    return nationality;
  }
  public void setCountryOfResidence(String countryOfResidence) {
    this.countryOfResidence = countryOfResidence;
  }
  public String getCountryOfResidence() {
    return countryOfResidence;
  }
  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }
  public String getDateOfBirth() {
    return dateOfBirth;
  }
  public void setNationalityList(List nationalityList) {
    this.nationalityList = nationalityList;
  }
  public List getNationalityList() {
    return nationalityList;
  }  
  public void setGradeTypeAttrId(String gradeTypeAttrId) {
    this.gradeTypeAttrId = gradeTypeAttrId;
  }
  public String getGradeTypeAttrId() {
    return gradeTypeAttrId;
  }
  public void setGradeTypeAttrValue(String gradeTypeAttrValue) {
    this.gradeTypeAttrValue = gradeTypeAttrValue;
  }
  public String getGradeTypeAttrValue() {
    return gradeTypeAttrValue;
  }
  public void setGradeTypeList(List gradeTypeList) {
    this.gradeTypeList = gradeTypeList;
  }
  public List getGradeTypeList() {
    return gradeTypeList;
  }
  public void setGradesAttrId(String gradesAttrId) {
    this.gradesAttrId = gradesAttrId;
  }
  public String getGradesAttrId() {
    return gradesAttrId;
  }
  public void setGradesAttrValues(String gradesAttrValues) {
    this.gradesAttrValues = gradesAttrValues;
  }
  public String getGradesAttrValues() {
    return gradesAttrValues;
  }
  public void setDate(String date) {
    this.date = date;
  }
  public String getDate() {
    return date;
  }
  public void setMonth(String month) {
    this.month = month;
  }
  public String getMonth() {
    return month;
  }
  public void setYear(String year) {
    this.year = year;
  }
  public String getYear() {
    return year;
  }
  public void setDateList(List dateList) {
    this.dateList = dateList;
  }
  public List getDateList() {
    return dateList;
  }
  public void setMonthList(List monthList) {
    this.monthList = monthList;
  }
  public List getMonthList() {
    return monthList;
  }
  public void setYearList(List yearList) {
    this.yearList = yearList;
  }
  public List getYearList() {
    return yearList;
  }
  public void setCountryOfResList(List countryOfResList) {
    this.countryOfResList = countryOfResList;
  }
  public List getCountryOfResList() {
    return countryOfResList;
  }

	public void setUserCMMType(String userCMMType) {
		this.userCMMType = userCMMType;
	}

	public String getUserCMMType() {
		return userCMMType;
	}
  public void setWidgetId(String widgetId) {
    this.widgetId = widgetId;
  }
  public String getWidgetId() {
    return widgetId;
  }
  public void setPageId(String pageId) {
    this.pageId = pageId;
  }
  public String getPageId() {
    return pageId;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setAffilateId(String affilateId) {
    this.affilateId = affilateId;
  }
  public String getAffilateId() {
    return affilateId;
  }
  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }
  public String getReviewId() {
    return reviewId;
  }
  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }
  public String getCategoryCode() {
    return categoryCode;
  }
  public void setBasketId(String basketId) {
    this.basketId = basketId;
  }
  public String getBasketId() {
    return basketId;
  }

  public void setMarketingEmailFlag(String marketingEmailFlag) {
    this.marketingEmailFlag = marketingEmailFlag;
  }

  public String getMarketingEmailFlag() {
    return marketingEmailFlag;
  }

  public void setSolusEmailFlag(String solusEmailFlag) {
    this.solusEmailFlag = solusEmailFlag;
  }

  public String getSolusEmailFlag() {
    return solusEmailFlag;
  }

  public void setSurveyEmailFlag(String surveyEmailFlag) {
    this.surveyEmailFlag = surveyEmailFlag;
  }

  public String getSurveyEmailFlag() {
    return surveyEmailFlag;
  }

  public void setScreenWidth(String screenWidth) {
    this.screenWidth = screenWidth;
  }

  public String getScreenWidth() {
    return screenWidth;
  }

public String getSubjectSessionId() {
   return subjectSessionId;
}

public void setSubjectSessionId(String subjectSessionId) {
   this.subjectSessionId = subjectSessionId;
}

}
