package WUI.registration.bean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import WUI.utilities.CommonUtil;

/**
* @ProfileBean.java
* @Version : 2.0
* @May 2 2007
* @www.whatuni.com
* @Created By : Balraj. Selva Kumar
* @Purpose    : Program used to store/retrive the information in the set/get method. used to handle the data related to login page.
* *************************************************************************************************************************************
* Author              Release                   Modification Details                         Change
* *************************************************************************************************************************************
* Karthi              WU_537_20150224           Validation for forgot Password  
* Prabhakaran V.      WU_546_20151103           Validation for forgot Password               Removed capcha validation codes.
* Indumathi.S         WU_547_20151124           Additioanl parameters                        Added additional parameters for social box.
*/
public class UserLoginBean {
  public UserLoginBean() {
  }
  private String userName = "";
  private String password = "";
  private String userId = "";
  private String requestFrom = "";
  private String emailId = "";
  private String userMessage = "";
  private String referralURL = "";
  private String searchKeyword = "";
  private HttpServletRequest request = null;
  
  public HttpServletRequest getRequest() {
	return request;
  }
  public void setRequest(HttpServletRequest request) {
	this.request = request;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }
  public String getUserName() {
    return userName;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public String getPassword() {
    return password;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setRequestFrom(String requestFrom) {
    this.requestFrom = requestFrom;
  }
  public String getRequestFrom() {
    return requestFrom;
  }
  public void setEmailId(String emailId) {
    this.emailId = emailId;
  }
  public String getEmailId() {
    return emailId;
  }
  public void setUserMessage(String userMessage) {
    this.userMessage = userMessage;
  }

  public String getUserMessage() {
    return userMessage;
  }

/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    errors.clear();
    if (request.getParameter("buttext") != null && request.getParameter("buttext").equals("Sign in")) {
      //Modified username validation by Indumathi.S Jan-27-16 Rel
      if (GenericValidator.isBlankOrNull(userName)) {
        errors.add("invaliduser", new ActionMessage("wuni.error.blankusername"));
      } else if(!GenericValidator.isBlankOrNull(userName) && !CommonUtil.isValidEmail(userName)) {
        errors.add("invaliduser", new ActionMessage("wuni.error.notvalidemail"));
      }
      if (password == null || password.equals("") || password.trim().length() == 0) {
        errors.add("invalidpassword", new ActionMessage("wuni.error..blankpassword"));
      }
    }
    if (request.getParameter("buttext") != null && request.getParameter("buttext").equals("Send me my password")) {
      if (emailId == null || emailId.equals("") || emailId.trim().length() == 0) {
        errors.add("invalidloginemail", new ActionMessage("wuni.login.message.invalidemail"));
      }
    }
     Start of code added for wu_537 24-FEB-2015 by Karthi for forgot password validation  
    if (request.getParameter("buttext") != null && request.getParameter("buttext").equals("Send password")) {
      if (emailId == null || emailId.equals("") || emailId.trim().length() == 0) {
       errors.add("invalidEmail", new ActionMessage("wuni.error.blank.email"));
      }else if(!new CommonUtil().isValidEmail(emailId.trim())){ // emailId trim() is added by Prabha on 03-Nov-2015
        errors.add("invalidEmail", new ActionMessage("wuni.error.invalid.email"));
      }
    }
     End of code added for wu_537 24-FEB-2015 by Karthi  
    return errors;
  }*/

  public void setReferralURL(String referralURL) {
    this.referralURL = referralURL;
  }

  public String getReferralURL() {
    return referralURL;
  }

  public void setSearchKeyword(String searchKeyword) {
    this.searchKeyword = searchKeyword;
  }

  public String getSearchKeyword() {
    return searchKeyword;
  }

}
