package WUI.registration.controller;

import WUI.basket.form.BasketBean;
import WUI.registration.bean.RegistrationBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;
import com.wuni.advertiser.prospectus.controller.ProspectusBasketLinkController;
import com.wuni.ajax.controller.StatsLoggingPdfDownloadController;
import com.wuni.util.RequestResponseUtils;
import com.wuni.util.sql.DataModel;

import java.io.IOException;
import java.io.Writer;
import java.net.URLDecoder;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.util.LabelValueBean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : UserRegisterationAction.java
* Description   :
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	               Change
* *************************************************************************************************************************************
* Anbarasan.R             1.0             09.11.2012           First draft   		
* Karthi              WU_537_20150224                         To check user account locked or not
* Thiyagu G           wu_546              03_Nov_2015         To save the grades and contact data.
* Prabhakaran V.      WU_552              19_Apr_2016         Added submit type for scout page login
* Prabhakaran V.      WU_566              11_Jul_2017         Commented FB Removed checking YOE for FB user by PRabha on 11_Jul_17
* Sabapathi S         wu_582              10_OCt_2018         Added screen width for stats logging
* Sujitha V           wu_588              23_APR_2019         Clearing landing page changes
* Sangeeth.S          wu_586              27.02.2019          Handled conditions for the Whatuni Go sign in and sign up and added encrypted userid in cookie 
* Sangeeth.S		  wu_310320			  09_MAR_2020	   	  Added lat and long for stats logging
* Sujitha V           wu_06052020         06_MAY-2020         Added condition for Corana virus hub registration pod.
* Sujitha V           wu_20200602         02_JUNE_2020        Added YOE for pre clearing landing page facebook registration
* Sri Sankari		  wu_20200917         17_SEP_2020          Added stats log for tariff logging(UCAS point)
* Minu S              wu_20200930         30_SEP_2020         Added condition for post clearing user source type
* Sujitha V           wu_20201110         10_NOV_2020         Added session for YOE to log in d_session_log.
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
@Controller
public class UserRegisterationController{  
  
  @RequestMapping(value = "/newuserregistration", method = RequestMethod.POST)
  public ModelAndView newUserRegistration(@RequestParam(value = "yoe", required = false) String yoeSession, ModelMap model,RegistrationBean registerBean ,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    //
	
    ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    StringBuffer myShortList = new StringBuffer();
    StringBuffer buffer = new StringBuffer();
    CommonUtil commonUtil = new CommonUtil();
    SessionData sessionData = new SessionData();
    String userId = null;
    //
    String method = request.getParameter("submittype");
    String enquiryLogin = request.getParameter("method");
    String tabFocus = request.getParameter("tabfocus");
    request.setAttribute("tabFocus", tabFocus);
    registerBean.setClientIp(new RequestResponseUtils().getRemoteIp(request));
    registerBean.setClientBrowser(request.getHeader("user-agent"));
    registerBean.setRequestURL(CommonUtil.getRequestedURL(request)); //Change the getRequestURL by Prabha on 28_Jun_2016
    registerBean.setRefferalURL(request.getHeader("referer"));
    registerBean.setYearOfUniEntry(request.getParameter("yoe"));//Added by Priyaa for 8_OCT_2014_REL for year of entry addition
    registerBean.setTrackSessionId(new SessionData().getData(request, "userTrackId"));
    //Added for the whatuni go     
    String subjectSessionId = (String)session.getAttribute("subjectSessionId");
    registerBean.setSubjectSessionId(!GenericValidator.isBlankOrNull(subjectSessionId) ? subjectSessionId : null);
    //
    String userType = request.getParameter("pSubmitType");
    String pdfId = request.getParameter("pdfId");
    StatsLoggingPdfDownloadController statsLogging = new StatsLoggingPdfDownloadController();
    getRequestParam(request, registerBean);
    request.setAttribute("referrerURL_GA", registerBean.getRefferalURL());//5_AUG_2014
    String gaLoggingType = request.getParameter("gaLoggingType");
    String nonloggedTm = request.getParameter("nonloggedTm");
    request.setAttribute("nonloggedTm", nonloggedTm);    
    model.addAttribute("timeline_login", commonUtil.getImgPath("/wu-cont/images/timline_login.jpg", 0));
    String vwCollegeId  = request.getParameter("vwcid") != null ? request.getParameter("vwcid") : "";
    String userCMMType = request.getParameter("userCMMType");//Added by Priyaa for 3_NOV_2015_REL    
    String vwcname  = request.getParameter(SpringConstants.USER_REGISTERATION_VWCNAME) != null ? request.getParameter(SpringConstants.USER_REGISTERATION_VWCNAME) : "";
    String vwprice  = request.getParameter(SpringConstants.USER_REGISTERATION_VWPRICE) != null ? request.getParameter(SpringConstants.USER_REGISTERATION_VWPRICE) : "";
    String vwpdfcid  = request.getParameter(SpringConstants.USER_REGISTERATION_VWPDFCID) != null ? request.getParameter(SpringConstants.USER_REGISTERATION_VWPDFCID) : "";    
    String vwsid = request.getParameter(SpringConstants.USER_REGISTERATION_VWSID) != null ? request.getParameter(SpringConstants.USER_REGISTERATION_VWSID) : "";
    String vwnid = request.getParameter(SpringConstants.USER_REGISTERATION_VWNID) != null ? request.getParameter(SpringConstants.USER_REGISTERATION_VWNID) : "";
    String vwsewf = request.getParameter(SpringConstants.USER_REGISTERATION_VWSEWF) != null ? request.getParameter(SpringConstants.USER_REGISTERATION_VWSEWF) : "";    
    String vwcourseId = request.getParameter(SpringConstants.USER_REGISTERATION_VWCOURSEID) != null ? request.getParameter(SpringConstants.USER_REGISTERATION_VWCOURSEID) : "";    
    String vwurl = request.getParameter(SpringConstants.USER_REGISTERATION_VWURL) != null ? request.getParameter(SpringConstants.USER_REGISTERATION_VWURL) : "";
    // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
    String screenWidth = GenericValidator.isBlankOrNull(request.getParameter("screenwidth")) ? GlobalConstants.DEFAULT_SCREEN_WIDTH : request.getParameter("screenwidth");
    // wu_300320 - Sangeeth.S: Added lat and long for stats logging
    String[] latAndLong = sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG).split(GlobalConstants.SPLIT_CONSTANT);
    String latitude = (latAndLong.length > 0 && !GenericValidator.isBlankOrNull(latAndLong[0])) ? latAndLong[0].trim() : "";
	String longitude = (latAndLong.length > 1 && !GenericValidator.isBlankOrNull(latAndLong[1])) ? latAndLong[1].trim() : "";
    String userUcasScore = StringUtils.isNotBlank((String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE)) ? (String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE) : null; //Added for tariff-logging-ucas-point by Sri Sankari on wu_20200917 release
	String pageNameParam = StringUtils.isNotBlank(request.getParameter(SpringConstants.USER_REGISTERATION_POST_CLEARING)) ? request.getParameter(SpringConstants.USER_REGISTERATION_POST_CLEARING) : null;  //Added in post clearing for setting Whatuni_clearing_user src same as pre-clearing register pod by Minu 
	String postClearingParam = request.getParameter("postClearingParam");
    postClearingParam = StringUtils.isNotBlank(postClearingParam) ? postClearingParam : null;
    // post clearing register pod fb sign up check, 30-09-2020 Minu
    if (StringUtils.equalsIgnoreCase("postClearingFbSignup", postClearingParam)) {
        pageNameParam = "clearingRegisterPod";
    }
    String downloadUrl = StringUtils.isNotBlank(request.getParameter("downloadurl")) ? request.getParameter("downloadurl") : "";
    String pdfName = StringUtils.isNotBlank(request.getParameter("pdfName")) ? request.getParameter("pdfName") : "";
    String nonmywu = StringUtils.isNotBlank(request.getParameter("nonmywu"))  ? request.getParameter("nonmywu") : "";
    String pnotes = StringUtils.isNotBlank(request.getParameter("pnotes")) ? request.getParameter("pnotes") : "";
    String registrationLBNew  = StringUtils.isNotBlank(request.getParameter("newLightBox")) ? request.getParameter("newLightBox") : "";
    
    model.addAttribute(SpringConstants.USER_REGISTERATION_POST_CLEARING, pageNameParam);
    registerBean.setScreenWidth(screenWidth);
    registerBean.setLatitude(latitude);
    registerBean.setLongitude(longitude);
    registerBean.setUserUcasScore(userUcasScore);
    String openDayResUniIds = "";
    CommonFunction common = new CommonFunction();
    
    //Added YOE value in session to log in d_session by Sujitha V on 2020_NOV_10 rel
    if(StringUtils.isNotBlank(method) && !StringUtils.equalsIgnoreCase("spamreg", method)){
      if(StringUtils.isNotBlank(yoeSession)){   
        CommonFunction.getDSessionLogForYOE(request, yoeSession);
      }
    }
    
    if(!GenericValidator.isBlankOrNull(request.getParameter("resUniIds"))){
      openDayResUniIds = request.getParameter("resUniIds");
    }
	  registerBean.setUserCMMType("add-open-days".equalsIgnoreCase(userCMMType) ? "open_days_reg" : "");//Added for openday registration by Prabha on 27_JAN_2016_REL
	   if(!GenericValidator.isBlankOrNull(registerBean.getEmailAddress())){
	     registerBean.setEmailAddress(URLDecoder.decode(registerBean.getEmailAddress(), SpringConstants.USER_REGISTERATION_UTF8));
    }
    
    if(!GenericValidator.isBlankOrNull(registerBean.getPassword())){
	     registerBean.setPassword(URLDecoder.decode(registerBean.getPassword(), SpringConstants.USER_REGISTERATION_UTF8));
    }
    
    String blockStatus = common.checkEmailBlocked(registerBean.getEmailAddress(), null);  //Added for wu_537 24-FEB-2015 by Karthi to check user blocked or not 
    if(gaLoggingType!=null && "splash-page".equals(gaLoggingType)){ //9_DEC_14 Added by Amir to handle Splash popup
      request.setAttribute("splash-page","true");
    }
    String[] yoeArr = common.getYearOfEntryArr();
    if(GenericValidator.isBlankOrNull(registerBean.getYearOfUniEntry())){    	
       registerBean.setYeartoJoinCourse(yoeArr[0]);  
    }    
    if(!GenericValidator.isBlankOrNull(method) && ("login".equalsIgnoreCase(method) || "applynowlogin".equalsIgnoreCase(method))){
      
      if (common.checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED //
        return new ModelAndView("forward: /userLogin.html");
      }
      /* Start of code Added for wu_537 24-FEB-2015 by Karthi to check user blocked or not */
      if(blockStatus.equals("1")){
        buffer.append("Blocked##SPLIT##1");
        setResponseText(response, buffer);
        return null;
      }
      /* End of code Added for wu_537 24-FEB-2015 by Karthi*/
      
      /* Start of code Added by Prabha for scout page login on wu_552 19-APR-2016 */
      String scoutSubmitType = request.getParameter("scoutSubmit");
      registerBean.setSubmitType(method); 
      if("scoutLogin".equalsIgnoreCase(scoutSubmitType)){
        registerBean.setSubmitType("scoutLogin"); 
      }
      /* End of scout page login */
      registerBean.setSessionId(GenericValidator.isBlankOrNull(new SessionData().getData(request, "x")) ? "531968744310" : new SessionData().getData(request, "x"));   
      userId = new GlobalFunction().checkEmailExist(registerBean.getEmailAddress(), request);
      String surveyUrlUserId = request.getParameter("surveyUrlUserId");
     if(!GenericValidator.isBlankOrNull(surveyUrlUserId)){
       String matchFlag =  checkSurveyUserId(surveyUrlUserId,userId);
       if(("N").equals(matchFlag)){
         myShortList.append(bundle.getString("wuni.error.survey.userid.mismatch"));
         setResponseText(response, buffer.append(myShortList));   
         return null;
       }
      }
      if(!GenericValidator.isBlankOrNull(userId) && !"0".equals(userId) && !"null".equalsIgnoreCase(userId)){ 
          userId = userRegisteration(request, response, registerBean);
          try{
          String cookieProsBasketId = common.checkProspectusCookieStatus(request);//28_OCT_2014 added by Priyaa For prospectus changes
          BasketBean basketBean = new BasketBean();
          if(!GenericValidator.isBlankOrNull(cookieProsBasketId)){
            basketBean.setBasketType("RP");
            basketBean.setBasketId(cookieProsBasketId);
            Object[][] prosBasketValues = new Object[1][4];
            new ProspectusBasketLinkController().addBasketContent(basketBean,userId,response,request,prosBasketValues);
          }
          }catch (Exception ex){
            ex.printStackTrace();
          }    
          if(GenericValidator.isBlankOrNull(userId) || "0".equals(userId) || "null".equalsIgnoreCase(userId)){
            myShortList.append(bundle.getString("wuni.error.login.pod.invalidpass"));
            setResponseText(response, myShortList);
            return null;
          }  
          userId = new SessionData().getData(request, "y");
          if(!GenericValidator.isBlankOrNull(userType) && ("pdfuser").equals(userType)){
           statsLogging.logStatsForPDFDownload(pdfId, userId);
          }
          String showUserName = common.getUserName(userId, request);     
          String userYOE  = common.getUserYearOfEntry(userId);
          //Added YOE value(for logged in user) in session to log in d_session by Sujitha V on 2020_NOV_10 rel
          if(StringUtils.isNotBlank(userYOE)) {
            CommonFunction.getDSessionLogForYOE(request, userYOE);
          }
         
          buffer.append("YES");
          buffer.append(SpringConstants.USER_REGISTERATION_BUFFER_APPEND);
          buffer.append(GenericValidator.isBlankOrNull(showUserName) ? "" : "null".equalsIgnoreCase(showUserName) ? "" : showUserName);
          buffer.append(SpringConstants.USER_REGISTERATION_BUFFER_APPEND);
          buffer.append(userId);
          buffer.append(SpringConstants.USER_REGISTERATION_BUFFER_APPEND);
          buffer.append(GenericValidator.isBlankOrNull(userYOE) ? "" : "null".equalsIgnoreCase(userYOE) ? "" : userYOE);
          String remember = request.getParameter("rememberMe");
          if ("Y".equalsIgnoreCase(remember)) {
            Cookie cookie = CookieManager.createCookie(request,SpringConstants.USER_REGISTERATION_USERID, userId);
            cookie.setMaxAge(2*30*24*60*60);
            response.addCookie(cookie);
          }          
          Cookie wcache = CookieManager.createCookie(request,SpringConstants.USER_REGISTERATION_WCACHE, "loggedin"); //08_OCT_2014 Added by amir for adding wcache
          response.addCookie(wcache);
          //Encryting to user id to base64 and adding it in cookie by Sangeeth.S for whatuniGO journey
          commonUtil.addEncryptedCookieUserId(request, userId, response);          
          // 
      }else {
        myShortList.append(bundle.getString("wuni.error.login.pod.invalidusername"));
      }
      setResponseText(response, buffer.append(myShortList));   
      if(!GenericValidator.isBlankOrNull(openDayResUniIds)){
        new CommonFunction().storeReserveOpendayList(request,response,openDayResUniIds);
      }  
      return null;
    }else if(!GenericValidator.isBlankOrNull(method) && ("registration".equalsIgnoreCase(method) || "applynowreg".equalsIgnoreCase(method))) {
      registerBean.setSubmitType(method);
      if(!GenericValidator.isBlankOrNull(userType) && ("pdfuser").equals(userType)){
        registerBean.setSubmitType("pdfreg");
      }else if(!GenericValidator.isBlankOrNull(userType) && ("whatcanido".equals(userType) || "iwanttobe".equals(userType)))
      {
        registerBean.setSubmitType(userType);
      }
      if(!GenericValidator.isBlankOrNull(userCMMType) && ("Webclick-comparison").equals(userCMMType)){////Added by Priyaa for 3_NOV_2015_REL for logging 
        registerBean.setSubmitType("webclick_comparison");
      }
      if(!GenericValidator.isBlankOrNull(userCMMType) && ("non-request-info").equals(userCMMType)){////Added by Priyaa for 3_NOV_2015_REL for logging 
        registerBean.setSubmitType("non_advertiser_pdf");
      }
      // wu588_20190423 - Sujitha V: Added p_submit as clearing_landing to log user source in DB
      if(!GenericValidator.isBlankOrNull(userCMMType) && ("lead-capture").equals(userCMMType)){
        registerBean.setSubmitType("clearing_landing");
      }
      if(!GenericValidator.isBlankOrNull(vwCollegeId)){
        registerBean.setSubmitType("regwc");
      }
      userId = new GlobalFunction().checkEmailExist(registerBean.getEmailAddress(), request);
      if(GenericValidator.isBlankOrNull(userId) || "0".equals(userId)){
        userId = userRegisteration(request, response, registerBean);
        try{            
        String cookieProsBasketId = new CommonFunction().checkProspectusCookieStatus(request);//28_OCT_2014 added by Priyaa For prospectus changes
        BasketBean basketBean = new BasketBean();
        if(!GenericValidator.isBlankOrNull(cookieProsBasketId)){
          basketBean.setBasketType("RP");
          basketBean.setBasketId(cookieProsBasketId);
          Object[][] prosBasketValues = new Object[1][4];
          new ProspectusBasketLinkController().addBasketContent(basketBean,userId,response,request,prosBasketValues);
        }
        }catch (Exception ex){
          ex.printStackTrace();
        } 
        if(!GenericValidator.isBlankOrNull(userType) && ("pdfuser").equals(userType)){
          statsLogging.logStatsForPDFDownload(pdfId, userId);
        }  
        String showUserName = new CommonFunction().getUserName(userId, request);  
        myShortList.append("YES"); 
        myShortList.append(SpringConstants.USER_REGISTERATION_BUFFER_APPEND);
        myShortList.append(GenericValidator.isBlankOrNull(showUserName) ? "" : "null".equalsIgnoreCase(showUserName) ? "" : showUserName);
        myShortList.append(SpringConstants.USER_REGISTERATION_BUFFER_APPEND);
        myShortList.append(userId);
        myShortList.append(SpringConstants.USER_REGISTERATION_BUFFER_APPEND);//5_AUG_2014
        myShortList.append(GenericValidator.isBlankOrNull(gaLoggingType) ? "" : "null".equalsIgnoreCase(gaLoggingType) ? "" : gaLoggingType);
        myShortList.append(SpringConstants.USER_REGISTERATION_BUFFER_APPEND);
        myShortList.append(GenericValidator.isBlankOrNull(registerBean.getYearOfUniEntry()) ? "" : "null".equalsIgnoreCase(registerBean.getYearOfUniEntry()) ? "" : registerBean.getYearOfUniEntry());                        
        String remember = request.getParameter("rememberMe");//Added for wu516_19112013 release.
        if ("Y".equalsIgnoreCase(remember)) {
          if (!GenericValidator.isBlankOrNull(userId)) {
            Cookie cookie = CookieManager.createCookie(request,SpringConstants.USER_REGISTERATION_USERID, userId);
            cookie.setMaxAge(2*30*24*60*60);
            response.addCookie(cookie);
          }
        }
        //Encryting to user id to base64 and adding it in cookie by Sangeeth.S for whatuniGO journey
        commonUtil.addEncryptedCookieUserId(request, userId, response);          
        //
      }else {
    	//Handled the condition for Pre Clearing Landing Page email error message by Sujitha V on 02-JUNE-2020
        String emailErrorText = "";
        String emailError = "";
        MessageFormat formatter = new MessageFormat("");
        if(StringUtils.isNotBlank(userCMMType) && StringUtils.equals(userCMMType, "lead-capture")) {
          emailError = bundle.getString("wuni.error.preclearing.existing.emailid");
          formatter.applyPattern(emailError);
          Object[] preClearingEmailArgument = {registerBean.getEmailAddress()};  
          emailErrorText = formatter.format(preClearingEmailArgument);
        }else {
          emailError = bundle.getString("wuni.error.profile.existing.emailid");
          formatter.applyPattern(emailError);
          Object[] normalEmailArgument = {registerBean.getEmailAddress()};  
          emailErrorText = formatter.format(normalEmailArgument);
        }
        myShortList.append(emailErrorText);
      }
      setResponseText(response, myShortList);
      Cookie wcache = CookieManager.createCookie(request,SpringConstants.USER_REGISTERATION_WCACHE, "loggedin"); //08_OCT_2014 Added by amir for adding wcache
      response.addCookie(wcache);
      if(!GenericValidator.isBlankOrNull(openDayResUniIds)){
        new CommonFunction().storeReserveOpendayList(request,response,openDayResUniIds);
      }  
      return null;
    } else if(!GenericValidator.isBlankOrNull(method) && "fbreg".equalsIgnoreCase(method)) {//13_JAN_2015 Modified foe userregistration       
      String lightboxSubmit = request.getParameter("lightboxSubmit");
      /* Start of code Added for wu_537 24-FEB-2015 by Karthi to check user blocked or not */
      if(blockStatus.equals("1")){
        buffer.append("Blocked##SPLIT##1");
        setResponseText(response, buffer);
        return null;
      }
      /* End of code Added for wu_537 24-FEB-2015 by Karthi*/
      //17_MAR_15 Added Gplus login by Amir      
      userId = new GlobalFunction().checkEmailExist(registerBean.getEmailAddress(), request);             
      String socialType = request.getParameter("socialType");            
      if("fbsignup".equalsIgnoreCase(socialType) && (GenericValidator.isBlankOrNull(userId) || "0".equals(userId))){
        registerBean.setSubmitType("fbreg");
      }else if("fblogin".equalsIgnoreCase(socialType)){
        registerBean.setSubmitType("fblogin");
      }else if("gpluslogin".equalsIgnoreCase(socialType)){
        registerBean.setSubmitType("gpluslogin");  //Added by Amir 17_MAR_15 for GPLUS login      
      } else if("fbsignup".equalsIgnoreCase(socialType) && !GenericValidator.isBlankOrNull(userId) && !"0".equals(userId)){
        registerBean.setSubmitType("userexists");
      } 
      if(!GenericValidator.isBlankOrNull(userType) && ("pdfuser").equals(userType)){
        registerBean.setSubmitType("fbloginpdf");
      }else if(!GenericValidator.isBlankOrNull(userType) && ("whatcanido").equals(userType)){
        registerBean.setSubmitType("fbwhatcanido");
      }else if(!GenericValidator.isBlankOrNull(userType) && ("iwanttobe").equals(userType)){
        registerBean.setSubmitType("fbiwanttobe");
      }
      if(!GenericValidator.isBlankOrNull(vwCollegeId)){
        registerBean.setSubmitType("fbloginwc");
      }      
      if("userexists".equalsIgnoreCase(registerBean.getSubmitType())) {        
        myShortList.append("sorry, facebook email address already exists");
      }else{        
        String fbYoeFlag = "";
        String fbUserExistsFlag = new GlobalFunction().checkFBUserExists(registerBean.getEmailAddress());//5_AUG_2014        
         if( !GenericValidator.isBlankOrNull(userId) && !"0".equals(userId) && !"null".equalsIgnoreCase(userId) ){
            fbYoeFlag = new GlobalFunction().checkifYearOfEntryExists(registerBean.getEmailAddress());//5_AUG_2014            
         }
         if( ((GenericValidator.isBlankOrNull(userId) || "0".equals(userId) || "null".equalsIgnoreCase(userId))) && GenericValidator.isBlankOrNull(lightboxSubmit) && !"whatuni-go".equalsIgnoreCase(userCMMType)){                    
          request.setAttribute("firstNameprep",  request.getParameter("firstName"));
          request.setAttribute("surNameprep",  request.getParameter("surName"));
          request.setAttribute("userNameprep",  request.getParameter("userName"));
          request.setAttribute("emailAddressprep",  request.getParameter("emailAddress"));          
          request.setAttribute("fbUserIdprep",  request.getParameter("fbUserId")); //Added FB user id          
          request.setAttribute("lbregistration", "lbregistration");
          request.setAttribute("tabFocus", "prep");
          request.setAttribute("hidenewuserform", "N");
          //Setting YOE for pre clearing landing page facebook prepopulation by Sujitha V on 02-JUNE-2020 rel
          if(!GenericValidator.isBlankOrNull(userCMMType) && ("lead-capture").equals(userCMMType)){
            request.setAttribute("yearofEntry", yoeArr[0]);
          }
          if(!GenericValidator.isBlankOrNull(socialType)){
           request.setAttribute("socialRegType",socialType);
          } 
          loadDobMethod(registerBean);
          new SpringContextInjector();
		  ISearchBusiness searchBusiness = (ISearchBusiness)SpringContextInjector.getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
          Map resultMap = searchBusiness.userRegistrationLightbox();    
          if(resultMap != null && !resultMap.isEmpty()) {
            List countryOfList = (ArrayList)resultMap.get("pc_country_list");
            if (countryOfList != null && countryOfList.size() > 0) {
              List countryList = (ArrayList)countryOfList.get(0);
              registerBean.setCountryOfResList(countryList);
            }
          }
          // wu588_20190423 - Sujitha V: Forward to clearing registration JSP if it is from clearing landing page
          if(request.getParameter("gaLoggingType").equals("lead-capture")) {
            return new ModelAndView("clearingebookregistration", "registerBean", registerBean);
          }else if(StringUtils.equalsIgnoreCase(registrationLBNew, "registration-lb-new")){
        	return new ModelAndView("newRegistrationLightBox", "registerBean", registerBean); 
          }else {
            return new ModelAndView("userregistration", "registerBean", registerBean);
          }
          //
        }else{
          //Added FB user id 
          if("whatuni-go".equalsIgnoreCase(userCMMType)){
            String fbUserid = request.getParameter("fbUserId");
            request.setAttribute("fbUserIdprep", fbUserid); 
            registerBean.setFbUserId(fbUserid);
          }
          //
          String fbuserId = userRegisteration(request, response, registerBean);
          if(GenericValidator.isBlankOrNull(fbuserId) || "0".equals(fbuserId)){
            myShortList.append(bundle.getString("wuni.error.login.pod.invalidusername")); 
          } else {
           String remember = request.getParameter("rememberMe");
           if ("Y".equalsIgnoreCase(remember)) {
             Cookie cookie = CookieManager.createCookie(request,SpringConstants.USER_REGISTERATION_USERID, fbuserId);
             cookie.setMaxAge(2*30*24*60*60);
             response.addCookie(cookie);
           } 
          myShortList.append("YES");
          myShortList.append(SpringConstants.USER_REGISTERATION_BUFFER_APPEND);
          myShortList.append(fbuserId);
          try{            
          String cookieProsBasketId = new CommonFunction().checkProspectusCookieStatus(request);//28_OCT_2014 added by Priyaa For prospectus changes
          BasketBean basketBean = new BasketBean();
          if(!GenericValidator.isBlankOrNull(cookieProsBasketId)){
            basketBean.setBasketType("RP");
            basketBean.setBasketId(cookieProsBasketId);
            Object[][] prosBasketValues = new Object[1][4];
            new ProspectusBasketLinkController().addBasketContent(basketBean,fbuserId,response,request,prosBasketValues);
          }
          }catch (Exception ex){
            ex.printStackTrace();
          } 
          if(!GenericValidator.isBlankOrNull(fbUserExistsFlag) && ("Y").equals(fbUserExistsFlag)){//5_AUG_2014
            myShortList.append(SpringConstants.USER_REGISTERATION_BUFFER_APPEND);
            myShortList.append(fbUserExistsFlag);
            myShortList.append(SpringConstants.USER_REGISTERATION_BUFFER_APPEND);
            myShortList.append(fbuserId);
          }
          if(!GenericValidator.isBlankOrNull(userType) && ("pdfuser").equals(userType)){
           statsLogging.logStatsForPDFDownload(pdfId, fbuserId);
          }
          //to add the decrypt cookie user id value
          commonUtil.addEncryptedCookieUserId(request, userId, response);
          //
         }
        }
      } 
      setResponseText(response, myShortList);
      Cookie wcache = CookieManager.createCookie(request,SpringConstants.USER_REGISTERATION_WCACHE, "loggedin"); //08_OCT_2014 Added by amir for adding wcache
      response.addCookie(wcache);
      //17_MAR_2014 Added by Amir for Storing reserved opendays
      if(!GenericValidator.isBlankOrNull(openDayResUniIds)){
        new CommonFunction().storeReserveOpendayList(request,response,openDayResUniIds);
      }  
      return null;
    }else if(!GenericValidator.isBlankOrNull(method) && "spamreg".equalsIgnoreCase(method) || "covidreg".equalsIgnoreCase(method)){
        registerBean.setSubmitType(method);
        String msg;
        registerBean.setMarketingEmailFlag("Y");
        registerBean.setSolusEmailFlag("N");
        registerBean.setSurveyEmailFlag("N");
        msg = userRegisteration(request, response, registerBean);
        myShortList.append(msg); 
        setResponseText(response, myShortList);
        //to add the decrypt cookie user id value
        commonUtil.addEncryptedCookieUserId(request, userId, response);
        //
        return null;      
    }else if(!GenericValidator.isBlankOrNull(enquiryLogin) && "nosplash".equalsIgnoreCase(enquiryLogin)) {
      session.setAttribute("noSplashWin","true");
    }    
    if(!GenericValidator.isBlankOrNull(enquiryLogin) && ("enquiryForm".equalsIgnoreCase(enquiryLogin) || "surveyForm".equalsIgnoreCase(enquiryLogin))){//Modified by Priyaa For 21_JUL_2015_REL 
     request.setAttribute("hidenewuserform", "Y"); 
     if("surveyForm".equalsIgnoreCase(enquiryLogin)){request.setAttribute("surveyForm", "Y"); }
     String message = request.getParameter("unmessage");
     request.getSession().setAttribute("usermessage", message);
    }else{
      request.setAttribute("hidenewuserform", "N"); 
    }
    loadDobMethod(registerBean);
    new SpringContextInjector();
	ISearchBusiness searchBusiness = (ISearchBusiness)SpringContextInjector.getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
    Map resultMap = searchBusiness.userRegistrationLightbox();    
    if(resultMap != null && !resultMap.isEmpty()) {
      List countryOfList = (ArrayList)resultMap.get("pc_country_list");
      if (countryOfList != null && countryOfList.size() > 0) {
        List countryList = (ArrayList)countryOfList.get(0);
        registerBean.setCountryOfResList(countryList);
      }
    }
    request.setAttribute(SpringConstants.USER_REGISTERATION_VWCNAME, vwcname);
    request.setAttribute(SpringConstants.USER_REGISTERATION_VWPRICE, vwprice);        
    request.setAttribute("vwpdfcid", vwpdfcid);
    request.setAttribute(SpringConstants.USER_REGISTERATION_VWSID, vwsid);
    request.setAttribute(SpringConstants.USER_REGISTERATION_VWNID, vwnid);
    request.setAttribute(SpringConstants.USER_REGISTERATION_VWSEWF, vwsewf);
    request.setAttribute(SpringConstants.USER_REGISTERATION_VWCOURSEID, vwcourseId);
    request.setAttribute(SpringConstants.USER_REGISTERATION_VWURL, vwurl);
    model.addAttribute("vwcid", vwCollegeId);
    model.addAttribute("YOE_0_newForm", yoeArr[0]);
    model.addAttribute("YOE_1_newForm", yoeArr[1]);
    model.addAttribute("YOE_2_newForm", yoeArr[2]);
    model.addAttribute("YOE_3_newForm", yoeArr[3]);
    model.addAttribute("downloadurl", downloadUrl);
    model.addAttribute("submitType", method);
    model.addAttribute("pdfId", pdfId);
    model.addAttribute("pdfName", pdfName);
    model.addAttribute("regLoggingType", gaLoggingType);
    model.addAttribute("nonmywu", nonmywu);
    model.addAttribute("pnotes", pnotes);
    model.addAttribute("indicatorgif", CommonUtil.getImgPath("/wu-cont/img/whatuni/indicator1.gif", 0));
    model.addAttribute("emailDomainJs", CommonUtil.getJsPath()+ "/js/emaildomain/" + CommonUtil.getProperties().getString("wuni.email.domain.js"));
    model.addAttribute("emailDomainLoginJs", CommonUtil.getJsPath()+ "/js/emaildomain/" + CommonUtil.getProperties().getString("wuni.email.domain.login.js"));
    model.addAttribute("emailDomainList", CommonUtil.getProperties().getString("wuni.form.email.domain.list")); 
    if(StringUtils.equalsIgnoreCase(registrationLBNew, "registration-lb-new")) {
      return new ModelAndView("newRegistrationLightBox", "registerBean", registerBean); 
    }else {
      return new ModelAndView("userregistration", "registerBean", registerBean);	
    }
    
  }
  // 
  public void getRequestParam(HttpServletRequest request, RegistrationBean registerBean) throws Exception{
    String firstName = request.getParameter("firstName");
    String surName = request.getParameter("surName");
    String userName = request.getParameter("userName");
    String emailAddress = request.getParameter("emailAddress");
    if(!GenericValidator.isBlankOrNull(emailAddress)){
      emailAddress = URLDecoder.decode(emailAddress, SpringConstants.USER_REGISTERATION_UTF8);
    }
    
    String fbUserId = request.getParameter("fbUserId"); //Added FB user id
    String password = request.getParameter("password");
    
    if(!GenericValidator.isBlankOrNull(password)){
      password = URLDecoder.decode(password, SpringConstants.USER_REGISTERATION_UTF8);
    }
    
    String confirmPassword = request.getParameter("confirmPassword");
    String newsLetter = request.getParameter("newsLetter");
    String yoe = request.getParameter("yoe");//8_OCT_2014_REL added by Priyaa for year of entry
    //Added user contact details for 03_Nov_2015, by Thiyagu G.
    String dob = request.getParameter("dob");    
    String tCounOfRes = request.getParameter("tCounOfRes");
    String addr1 = request.getParameter("addr1");
    String addr2 = request.getParameter("addr2");
    String town = request.getParameter("town");
    String postcode = request.getParameter("postcode");    
    //
    if(!GenericValidator.isBlankOrNull(firstName)) {
      registerBean.setFirstName(firstName);
    }
    if(!GenericValidator.isBlankOrNull(surName)) {
      registerBean.setSurName(surName);
    }
    if(!GenericValidator.isBlankOrNull(userName)) {
      registerBean.setUserName(userName);
    }
    
    if(!GenericValidator.isBlankOrNull(emailAddress)) {
      registerBean.setEmailAddress(emailAddress);
    }
    if(!GenericValidator.isBlankOrNull(password)) {
      registerBean.setPassword(password);
    }
    if(!GenericValidator.isBlankOrNull(confirmPassword)) {
      registerBean.setConfirmPassword(confirmPassword);
    }
    if(!GenericValidator.isBlankOrNull(yoe)) {//8_OCT_2014_REL added by Priyaa for year of entry
      registerBean.setYearOfUniEntry(yoe);
    }
    if(!GenericValidator.isBlankOrNull(fbUserId)) {//Added FB user id to store and generate FB image path for 19_May_2015, by Thiyagu G.
      registerBean.setFbUserId(fbUserId);
    }    
    if(!GenericValidator.isBlankOrNull(newsLetter)) {
      registerBean.setNewsLetterFlag(newsLetter);
    } else {
      registerBean.setNewsLetterFlag("N");
    }
    if(!GenericValidator.isBlankOrNull(dob)) {
      registerBean.setDateOfBirth(dob);
    }
    if(!GenericValidator.isBlankOrNull(tCounOfRes)) {
      registerBean.setCountryOfResidence(tCounOfRes);
    }
    if(!GenericValidator.isBlankOrNull(addr1)) {
      registerBean.setAddressLine1(addr1.trim());
    }
    if(!GenericValidator.isBlankOrNull(addr2)) {
      registerBean.setAddressLine2(addr2.trim());
    }
    if(!GenericValidator.isBlankOrNull(town)) {
      registerBean.setTown(town.trim());
    }
    if(!GenericValidator.isBlankOrNull(postcode)) {
      registerBean.setPostCode(postcode.trim());
    }
  }
  //
  public String userRegisteration(HttpServletRequest request, HttpServletResponse response, RegistrationBean registerBean) throws Exception {
    //
     CommonFunction common = new CommonFunction();
    new SpringContextInjector();
	ISearchBusiness searchBusiness = (ISearchBusiness)SpringContextInjector.getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
    //    
    HttpSession session = request.getSession();
    ServletContext context = request.getServletContext();
    new SessionData().setParameterValuestoSessionData(request, response);
    //Added user contact details and grades for 03_Nov_2015, by Thiyagu G.     
    String[] attributeIds = GlobalConstants.ATTRIBUTE_IDS;
    String qual = request.getParameter("qual");
    String grades = request.getParameter("grades");
    String dgrade = request.getParameter("dgrade");
    String userCmmType = request.getParameter("userCMMType");
    Object[][] attrValues = new Object[attributeIds.length][6];
    String attributeValueId = null;
    String orderAttributeId = null;    
    String attributeValue = null;    
    String optionId = null;
    String attributeValueSeq = null;   
    String postClearingParam = request.getParameter("postClearingParam");  
    postClearingParam = StringUtils.isNotBlank(postClearingParam) ? postClearingParam : null;	
    for(int i = 0; i < attributeIds.length; i++){
      if(!"Y".equals(dgrade)){
        if(i==0){
          if(!GenericValidator.isBlankOrNull(qual)){
           optionId = qual;
          }
        }
        if(i==1){
          if(!GenericValidator.isBlankOrNull(grades)){
           attributeValue = grades;
          }
        }
      }else{
        if(i==2){
          if(!GenericValidator.isBlankOrNull(dgrade)){
           attributeValue = dgrade;
          }
        }
      }
      // wu588_20190423 - Sabapathi: Send option id and attribute value to DB if it is from clearing landing
	  // Send option id and attribute value to DB if it is from post clearing register pod, by Minu 28-Aug-2020
      if(("lead-capture".equals(userCmmType) || StringUtils.equalsIgnoreCase("postClearing-lead-capture", postClearingParam)) && i == 3) {
        optionId = "579";
        attributeValue = "WHATUNI_CLEARING_USER";
      }
      //
      attrValues[i][0] = attributeValueId;
      attrValues[i][1] = orderAttributeId;
      attrValues[i][2] = attributeIds[i];
      attrValues[i][3] = attributeValue;
      attrValues[i][4] = optionId;
      attrValues[i][5] = attributeValueSeq;
      optionId = null;
      attributeValue = null;
    }    
    Map resultMap = searchBusiness.newUserRegistration(registerBean, attrValues);
    Object userId = null;
    Object sessionId = null;
    Object spamMsg = null;
    String returnMsg = null;
    String userUcasScore = null;
   
    if(resultMap != null) {
      userId = resultMap.get("p_user_id");
      sessionId = resultMap.get("p_session_id");
     // wu_20200917 - Sri Sankari: Added for user UCAS tariff point
      userUcasScore = StringUtils.isNotBlank((String)resultMap.get("P_USER_UCAS_POINTS")) ? (String)resultMap.get("P_USER_UCAS_POINTS") : "";
      if(StringUtils.isNotBlank(userUcasScore)) {
    	  session.setAttribute("USER_UCAS_SCORE", userUcasScore); 
      }
      if("spamreg".equalsIgnoreCase(registerBean.getSubmitType()) || "covidreg".equalsIgnoreCase(registerBean.getSubmitType())){
       spamMsg = resultMap.get("o_spam_msg");
       if(spamMsg != null){
         returnMsg = String.valueOf(spamMsg);
       }
      }else{
      request.setAttribute("userId", resultMap.get("p_user_id"));
      new SessionData().addData(request, response,"y", String.valueOf(userId));
      new SessionData().addData(request, response,"x", String.valueOf(sessionId));
      common.getUserName(String.valueOf(userId), request);
       String voucherCode = null;
      if (voucherCode != null && voucherCode.trim().length()>0) {
        request.setAttribute("voucherCode", voucherCode);
      }      
      session.removeAttribute("userInfoList");
      session.removeAttribute("latestMemberList");
      common.loadUserLoggedInformation(request, context, response);
      
      String userFrmReg = new SessionData().getData(request, "userIdAfterSubmit");
      if (userFrmReg != null && userFrmReg.equals("true")) {
        session.removeAttribute("collegeSelected");
        new GlobalFunction().preLoginUpdateStatusCode(request);
      }
      returnMsg = String.valueOf(userId);
     } 
    }
    return returnMsg;
  }
  
  //
  private void setResponseText(HttpServletResponse response, StringBuffer responseMessage) {
     try {
       response.setContentType("TEXT/PLAIN");
       Writer writer = response.getWriter();
       writer.write(responseMessage.toString());
     } catch (IOException exception) {
       exception.printStackTrace();
     }
   }  
   
  private String checkSurveyUserId(String surveyUserId, String userId){
    String flag = "";
    try {
      if (!GenericValidator.isBlankOrNull(userId)) {
        DataModel dataModel = new DataModel();
        Vector vector = new Vector();
        vector.clear();        
        vector.add(userId);
        vector.add(surveyUserId);
        flag = dataModel.getString(GlobalConstants.CHECK_DECRYPT_VALUE_FN, vector);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return flag;
  
  }
  private void loadDobMethod(RegistrationBean registerBean) {
    LabelValueBean dobBean = null;
    ArrayList dateList = new ArrayList();
    for (int i = 1; i <= 31; i++) {
      dobBean = new LabelValueBean();
      if (i < 10) {
        dobBean.setLabel("0" + String.valueOf(i));
        dobBean.setValue("0" + String.valueOf(i));
      } else {
        dobBean.setLabel(String.valueOf(i));
        dobBean.setValue(String.valueOf(i));
      }
      dateList.add(dobBean);
    }
    ArrayList monthList = new ArrayList();
    String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    for (int i = 1; i <= 12; i++) {
      dobBean = new LabelValueBean();
      if (i < 10) {
        dobBean.setLabel(months[i - 1]);
        dobBean.setValue("0" + String.valueOf(i));
      } else {
        dobBean.setLabel(months[i - 1]);
        dobBean.setValue(String.valueOf(i));
      }
      monthList.add(dobBean);
    }
    ArrayList yearList = new ArrayList();
    Calendar ca = new GregorianCalendar();
    int currentYear = (ca.get(Calendar.YEAR)-13);
    for (int i = 1900; i <= currentYear; i++) { //Changed years from 1970 to 1900 for 19_Apr_2016.
      dobBean = new LabelValueBean();
      dobBean.setLabel(String.valueOf(i));
      dobBean.setValue(String.valueOf(i));
      yearList.add(dobBean);
    }  
    registerBean.setDateList(dateList);
    registerBean.setMonthList(monthList);
    registerBean.setYearList(yearList);    
  }
  
}
  