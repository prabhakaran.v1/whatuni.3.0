package WUI.registration.controller;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import oracle.sql.ARRAY;
import com.wuni.util.sql.DataModel;
import javax.servlet.http.*;
import WUI.utilities.SessionData;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.*;
import oracle.sql.ArrayDescriptor;
import org.apache.struts.upload.FormFile;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import oracle.jdbc.OracleCallableStatement;
import WUI.registration.bean.RegistrationBean;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;

/**
  * @RegistrationAction.java
  * @Version : 2.0
  * @June 8 2007
  * @www.whatuni.com
  * @Created By : Kalikumar. Modified by Balraj. Selva Kumar
  * @Purpose  : This Program is used to register the user.
  * ************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * ************************************************************************************************************************
  */

@Controller
public class RegistrationController{
	
  public ModelAndView registration(ActionMapping mapping, RegistrationBean regBean, HttpServletRequest request, HttpServletResponse response) throws ServletException, 
                                                                                                                                        IOException {
    String action = request.getParameter("action");
    HttpSession session = request.getSession();
    ServletContext context = request.getServletContext();
    if (new CommonFunction().checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    
    String urlString = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
    String queryString = request.getQueryString();
    
    // STORE'S PARAM VALUE OF X, Y, A TO HASMAP UNDER THE SESSION VARIABLE SESSIONDATA //
    new SessionData().setParameterValuestoSessionData(request, response);
    try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
        if (action != null) {
          if (session.getAttribute("DynamicQuestionList") == null) {
            return new ModelAndView("loginPage");
          }
        }
        String fromUrl = "/home.html";
        session.setAttribute("fromUrl", fromUrl);
        String additionalFromUrl = "";
        
        if (request.getParameter("z") != null && request.getParameter("z").trim().length() > 0) {
          additionalFromUrl = "&amp;z=" + request.getParameter("z");
        }
        if (request.getParameter("w") != null && request.getParameter("w").trim().length() > 0) {
          additionalFromUrl = "&amp;w=" + request.getParameter("w");
        }
        if (additionalFromUrl != null && additionalFromUrl.trim().length() > 0) {
          session.setAttribute("additionalFromUrl", additionalFromUrl);
        }
        return new ModelAndView("registrationpage");
      }
    } catch (Exception securityException) {
      securityException.printStackTrace();
    }
    if (action == null) {
      if (session.getAttribute("DynamicQuestionList") != null) {
        session.removeAttribute("DynamicQuestionList");
      }
      regBean.setSelectedEmailAddress("");
    // need to check by me  regBean.setNewsLetter(false);
      regBean.setStaticEmailAddress("");
      regBean.setSelectFileUpload("browse");
      regBean.setEditUserImage("");
      regBean.setTermsAndCondition(false);
        
      List questionList = generateRegistrationQuestion(request);
      if (questionList != null && questionList.size() > 0) {
        session.setAttribute("DynamicQuestionList", questionList);
        List defaultImageList = new CommonFunction().defaultImageList(request);
        request.getSession().setAttribute("defaultImageList", defaultImageList);
        if (new SessionData().getData(request, "y") != null && !(new SessionData().getData(request, "y").equalsIgnoreCase("null")) && !(new SessionData().getData(request, "y").equals("0")) && new SessionData().getData(request, "y").trim().length() > 0) {
          loadRegistrationDetails(regBean, request);
        }
      }
      new CommonFunction().loadUserLoggedInformation(request, context, response);
      return new ModelAndView("showregistrationpage");
    } else {
      
      String checkEmail = "0";
      boolean checkEmailFlag = true;
      if (new SessionData().getData(request, "y") != null && new SessionData().getData(request, "y").trim().length() > 0) {
        String selectedEmailAddress = regBean.getSelectedEmailAddress() != null? regBean.getSelectedEmailAddress().trim(): "";
        String staticEmailAddress = regBean.getStaticEmailAddress() != null? regBean.getStaticEmailAddress().trim(): "";
        if (selectedEmailAddress != null && selectedEmailAddress.equalsIgnoreCase(staticEmailAddress)) {
          checkEmailFlag = false;
        }
      }
      if (checkEmailFlag) {
        checkEmail = checkEmailExist(request);
      }
     String newsLetterFlag = "N";
    		 // need to check by me regBean.isNewsLetter() ? "Y" : "N";
     if (checkEmail != null && checkEmail.equals("0")) {
        String userId = updateRegistrationDetails(request, response, newsLetterFlag);
        ////////////////////// added for 23.03.2009 release /////////////////
        //new GlobalFunction().updateBasketContent(request, userId);//Commented for 19th May release as part of W_USER_SEARCHES
        ////////////////////// END added for 23.03.2009 release //////////////
        String showUserName = new CommonFunction().getUserName(userId, request);
        String voucherCode = new CommonUtil().getMusicVocher(userId, request); /* email send to the user with voucherid for downloading the music */
        if (voucherCode != null && voucherCode.trim().length()>0) {
          request.setAttribute("voucherCode", voucherCode);
        }
        new CommonFunction().sendRegistrtionMail(userId, voucherCode, request); /* email send to the user after registration */
        session.removeAttribute("userInfoList");
        session.removeAttribute("latestMemberList");
        new CommonFunction().loadUserLoggedInformation(request, context, response);
        ////////////////////////////Added for 16/06/2009 release. Anon user review Status Changed to Active state////////////////////////////
        String userFrmReg = new SessionData().getData(request, "userIdAfterSubmit");
        if (userFrmReg != null && userFrmReg.equals("true")) {
          session.removeAttribute("collegeSelected");
          new GlobalFunction().preLoginUpdateStatusCode(request);
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      } else { // Email Already Exists
       /* ActionErrors errors = new ActionErrors();
        errors.add("emailexist", new ActionMessage("wnui.error.email_already_exists"));*/
      //  saveErrors(request, errors);
        return new ModelAndView();
      }
    }
    if (session.getAttribute("fromUrl") != null && String.valueOf(session.getAttribute("fromUrl")).equalsIgnoreCase("/addreview.html")) {
      String additionalUrl = "";
      String strCollegeId = request.getParameter("z");
      String strCourseId = request.getParameter("w");
      if (strCollegeId != null && strCollegeId.trim().length() > 0) {
        additionalUrl = additionalUrl + "&z=" + strCollegeId;
      }
      if (strCourseId != null && strCourseId.trim().length() > 0) {
        additionalUrl = additionalUrl + "&w=" + strCourseId;
      }
      if (additionalUrl != null && additionalUrl.trim().length() > 0) {
        session.setAttribute("additionalFromUrl", additionalUrl);
      }
      return new ModelAndView("redirectPage");
    }
    return new ModelAndView("successpage");
  }

  /**
    *   This function is used to generate the dynamic registration question.
    * @param request
    * @return Dynamic registartion qustionas Collection object.
    */
  public List generateRegistrationQuestion(HttpServletRequest request) {
    ResultSet resultset = null;
    Vector vector = new Vector();
    ArrayList list = new ArrayList();
    RegistrationBean bean = null;
    DataModel datamodel = new DataModel();
    try {
      vector.add("USER REGISTRATION WU");
      resultset = datamodel.getResultSet("wu_review_pkg.registration_page_fn", vector);
      while (resultset.next()) {
        bean = new RegistrationBean();
        bean.setFiledId(resultset.getString("FIELD_ID"));
        bean.setFieldType(resultset.getString("FIELD_TYPE"));
        bean.setQuestionLabel(resultset.getString("DESCRIPTION"));
        bean.setDisplaySeqId(resultset.getString("DISPLAY_SEQ"));
        bean.setFieldRequired(resultset.getString("FIELD_REQ"));
        bean.setPerRowCount(resultset.getString("PER_ROW_CNT"));
        bean.setDefaultValue(resultset.getString("DEFAULT_VALUE"));
        bean.setTemplateIdHint(resultset.getString("TEMPLATE_ID"));
        bean.setRowStatus(resultset.getString("ROW_STATUS"));
        bean.setMaxSize(resultset.getString("MAX_SIZE"));
        bean.setDisplaySize(resultset.getString("DISPLAY_SIZE"));
        bean.setSelectedValue("");
        if (bean.getQuestionLabel() != null && (bean.getQuestionLabel().equalsIgnoreCase("confirm email address") || bean.getQuestionLabel().equalsIgnoreCase("password") || bean.getQuestionLabel().equalsIgnoreCase("nickname"))) {
          // list.add(bean);
          continue;
        }
        if (resultset.getString("FIELD_TYPE") != null && (resultset.getString("FIELD_TYPE").equalsIgnoreCase("dropdown"))) {
          ResultSet dropdownresultset = null;
          try {
            vector.clear();
            vector.add(resultset.getString("FIELD_ID"));
            vector.add("YES");
            dropdownresultset = datamodel.getResultSet("wu_review_pkg.get_drop_down_list_fn", vector);
            List optionvalue = new ArrayList();
            RegistrationBean optionvaluebean = null;
            while (dropdownresultset.next()) {
              optionvaluebean = new RegistrationBean();
              optionvaluebean.setOptionId(dropdownresultset.getString("Value"));
              optionvaluebean.setOptionText(dropdownresultset.getString("description"));
              optionvalue.add(optionvaluebean);
            }
            bean.setOptionValues(optionvalue);
          } catch (Exception exception) {
            exception.printStackTrace();
          } finally {
            datamodel.closeCursor(dropdownresultset);
          }
        }
        if (resultset.getString("FIELD_TYPE") != null && (resultset.getString("FIELD_TYPE").equalsIgnoreCase("checkbox"))) {
          List optionvalue = new ArrayList();
          RegistrationBean optionvaluebean = new RegistrationBean();
          ;
          optionvaluebean.setOptionId("M");
          optionvaluebean.setOptionText("Male");
          optionvalue.add(optionvaluebean);
          optionvaluebean = new RegistrationBean();
          optionvaluebean.setOptionId("F");
          optionvaluebean.setOptionText("Female");
          optionvalue.add(optionvaluebean);
          bean.setOptionValues(optionvalue);
        }
        String EXTRA_ATTRIB = resultset.getString("EXTRA_ATTRIB");
        if (EXTRA_ATTRIB != null && EXTRA_ATTRIB.trim().length() > 0) {
          ResultSet dropdownresultset = null;
          try {
            Vector yearOfPassing = new Vector();
            yearOfPassing.add(resultset.getString("FIELD_ID"));
            dropdownresultset = datamodel.getResultSet(EXTRA_ATTRIB, yearOfPassing);
            List optionvalue = new ArrayList();
            RegistrationBean optionvaluebean = null;
            while (dropdownresultset.next()) {
              optionvaluebean = new RegistrationBean();
              optionvaluebean.setOptionId(dropdownresultset.getString("year_of_passing"));
              optionvaluebean.setOptionText(dropdownresultset.getString("year_of_passing"));
              optionvalue.add(optionvaluebean);
            }
            bean.setOptionValues(optionvalue);
          } catch (Exception exception) {
            exception.printStackTrace();
          } finally {
            datamodel.closeCursor(dropdownresultset);
          }
        }
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      datamodel.closeCursor(resultset);
    }
    return list;
  }

  /**
    *   This function is used to update the user registration related information.
    * @param request
    * @return userid as String.
    * @throws ServletException
    * @throws IOException
    */
  public String updateRegistrationDetails(HttpServletRequest request, HttpServletResponse response, String newsLetterFlag) throws ServletException, IOException {
    String userId = "";
    RegistrationBean bean = null;
    CommonUtil commonUtil = new CommonUtil();
    CookieManager cookieManager = new CookieManager();
    int rowNum = 4;
    HttpSession session = request.getSession();
    ResultSet resultset = null;
    ArrayList questionlist = (ArrayList)session.getAttribute("DynamicQuestionList");
    String answerId[] = new String[questionlist.size() + 8];
    String answerText[] = new String[questionlist.size() + 8];
    answerId[0] = "x";
    answerText[0] = new SessionData().getData(request, "x");
    answerId[1] = "y";
    if (new SessionData().getData(request, "y") != null && (!(new SessionData().getData(request, "y").equals("0")) || new SessionData().getData(request, "y").trim().length() > 0)) {
      answerText[1] = new SessionData().getData(request, "y"); //user Id
    } else {
      answerText[1] = ""; //user Id 
    }
    answerId[2] = "a";
    answerText[2] = GlobalConstants.WHATUNI_AFFILATE_ID;
    answerId[3] = "190";
    answerText[3] = "USER REGISTRATION WU"; //form ID
    if (questionlist != null) {
      Iterator questioniterator = questionlist.iterator();
      while (questioniterator.hasNext()) {
        bean = new RegistrationBean();
        bean = (RegistrationBean)questioniterator.next();
        String questionanswerId = bean.getFiledId();
        String questionanswerText = bean.getSelectedValue() != null? bean.getSelectedValue().trim(): bean.getSelectedValue();
        answerId[rowNum] = "pm_" + questionanswerId;
        answerText[rowNum] = questionanswerText;
        rowNum++;
      }
      // Random password generator 
      String password = new CommonFunction().getRandomPassword();
      answerId[rowNum] = "pm_2";
      answerText[rowNum] = password;
      rowNum++;
      answerId[rowNum] = "pm_61";
      answerText[rowNum] = "";
      rowNum++;
      answerId[rowNum] = "pm_346";
      answerText[rowNum] = "";
        rowNum++;
        answerId[rowNum] = "newsletter";
        answerText[rowNum] = newsLetterFlag;
        
      if (answerId != null && answerText != null) {
        Connection connection = null;
        OracleCallableStatement oraclestmt = null;
        try {
          DataSource datasource = null;
          try {
            InitialContext ic = new InitialContext();
            datasource = (DataSource)ic.lookup("whatuni");
          } catch (Exception exception) {
            exception.printStackTrace();
          }
          connection = datasource.getConnection();
          String query = "{ ?= call hot_admin.hc_maintain_user3.create_user_reg_fn(?,?) }";
          oraclestmt = (OracleCallableStatement)connection.prepareCall(query);
          oraclestmt.registerOutParameter(1, OracleTypes.CURSOR);
          ArrayDescriptor descriptor = ArrayDescriptor.createDescriptor("T_USERREGTABTYPE", connection);
          ARRAY array_1 = new ARRAY(descriptor, connection, answerId);
          ARRAY array_2 = new ARRAY(descriptor, connection, answerText);
          oraclestmt.setARRAY(2, array_1);
          oraclestmt.setARRAY(3, array_2);
          oraclestmt.execute();
          resultset = (ResultSet)oraclestmt.getObject(1);
          while (resultset.next()) {
            userId = resultset.getString("user_id");
            new SessionData().addData(request, response, "y", resultset.getString("user_id"));
            new SessionData().addData(request, response, "x", resultset.getString("session_id"));
          }
          if(!GenericValidator.isBlankOrNull(userId) && !"0".equalsIgnoreCase(userId)){
             //Encryting to user id to base64 and adding it in cookie by Sangeeth.S for whatuniGO journey            
             commonUtil.addEncryptedCookieUserId(request, userId, response);          
           }
        } catch (Exception exception) {
          exception.printStackTrace();
        } finally {
          try {
            if (oraclestmt != null) {
              oraclestmt.close();
            }
            if (resultset != null) {
              resultset.close();
            }
            if (connection != null) {
              connection.close();
            }
          } catch (Exception closeException) {
            closeException.printStackTrace();
          }
        }
      }
    }
    return userId;
  }

  /**
    *   This function is used to create a directory to the application server.
    * @param srcDir
    * @return none.
    * @throws IOException
    */
  private void createDirectory(File srcDir) throws IOException {
    if (!srcDir.isDirectory()) {
      srcDir.mkdir();
    }
  }

  /**
    *   This function is used to check the given Email address is already present.
    * @param request
    * @return userid as String.
    */
  private String checkEmailExist(HttpServletRequest request) {
    String checkEmailFlag = null;
    RegistrationBean bean = null;
    HttpSession session = request.getSession();
    DataModel datamodel = new DataModel();
    Vector vector = new Vector();
    ArrayList questionlist = (ArrayList)session.getAttribute("DynamicQuestionList");
    if (questionlist != null) {
      try {
        Iterator questioniterator = questionlist.iterator();
        while (questioniterator.hasNext()) {
          bean = new RegistrationBean();
          bean = (RegistrationBean)questioniterator.next();
          if (bean.getQuestionLabel() != null && bean.getQuestionLabel().equalsIgnoreCase("Email address")) {
            vector.add(bean.getSelectedValue());
            vector.add("Y");
            checkEmailFlag = datamodel.getString("email_exists", vector);
          }
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      }
    }
    return checkEmailFlag;
  }

  /**
    *   This function is used to load the registration related information.
    * @param form
    * @param request
    * @return none.
    */
  private void loadRegistrationDetails(RegistrationBean rBean, HttpServletRequest request) {
    ResultSet resultset = null;
    Vector vector = new Vector();
    DataModel datamodel = new DataModel();
    HttpSession session = request.getSession();
    try {
      vector.clear();
      vector.add(new SessionData().getData(request, "y")); //userid
      resultset = datamodel.getResultSet("Wu_review_pkg.get_user_info_fn", vector);
      ArrayList questionList = (ArrayList)session.getAttribute("DynamicQuestionList");
      String userPhotoUrl = "";
      String emailAddress = "";
      while (resultset.next()) {
        if (resultset.getString("field_id") != null && resultset.getString("field_id").equalsIgnoreCase("image_path")) {
          userPhotoUrl = resultset.getString("detail");
        } else {
          Iterator questionIterator = questionList.iterator();
          while (questionIterator.hasNext()) {
            RegistrationBean questionBean = (RegistrationBean)questionIterator.next();
            String answerFieldId = resultset.getString("field_id") != null? resultset.getString("field_id").trim(): resultset.getString("field_id");
            if (questionBean.getFiledId() != null && questionBean.getFiledId().trim().equalsIgnoreCase(answerFieldId)) {
              if (questionBean.getQuestionLabel() != null && questionBean.getQuestionLabel().equalsIgnoreCase("Email address")) {
                emailAddress = resultset.getString("detail");
              }
              questionBean.setSelectedValue(resultset.getString("detail"));
            }
            if (questionBean.getQuestionLabel() != null && questionBean.getQuestionLabel().equalsIgnoreCase("Confirm Email address")) {
              questionBean.setSelectedValue(emailAddress);
              rBean.setStaticEmailAddress(emailAddress);
            }
          }
        }
      }
      session.setAttribute("DynamicQuestionList", questionList);
      rBean.setEditUserImage(userPhotoUrl);
    } catch (Exception exception) {
      //exception.printStackTrace();
    } finally {
      datamodel.closeCursor(resultset);
    }
  }  
}
