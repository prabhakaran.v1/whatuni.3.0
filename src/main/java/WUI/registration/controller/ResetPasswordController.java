package WUI.registration.controller;

import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Class         : ResetPasswordAction.java
 * Description   : This is a action class used to do reset password operations.
 * @version      : 1.0
 * Author        : Karthi
 * Since         : WU_537_20150224
 * Change Log    :
 * *************************************************************************************************************************************
 * Author              Release                   Modification Details                         Change
 * *************************************************************************************************************************************
 * Karthi              WU_537_20150224           First draft
 * Prabhakaran V.      WU_546_20151103           Second draft                                  Remove mobile redirection related codes
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */

@Controller
public class ResetPasswordController { //Change the extends class MobileResetPasswordAction into Action by Prabha on 03_NOV_15
  
  @RequestMapping(value="/resetPassword", method={RequestMethod.GET, RequestMethod.POST})
  public ModelAndView resetPassword(HttpServletRequest request, HttpServletResponse response) throws Exception{
    String submitType =  request.getParameter("method");
    HttpSession session = request.getSession();
    session.setAttribute("noSplashpopup","true"); //Added for wu_537 24-FEB-2015 by Karthi to remove login lightbox
    String sessionUserId = new SessionData().getData(request, "y");
    sessionUserId = GenericValidator.isBlankOrNull(sessionUserId) ? "0" : sessionUserId;
    String userId = request.getParameter("token");
    if(!GenericValidator.isBlankOrNull(submitType) && ("resetPassword").equals(submitType)){ 
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);      
      String caTrackFlag = request.getParameter("ca-track-flag") != null ? request.getParameter("ca-track-flag") : ""; //Added by Thiyagu G to add a ca track flag on CATOOL.
      Map userExistsMap = commonBusiness.resetPasswordPrc(userId, null, sessionUserId, null);
      String userExistStatus = (String)userExistsMap.get("p_error_status");
      if(!GenericValidator.isBlankOrNull(userExistStatus) && ("0").equals(userExistStatus)){
        request.setAttribute("userId", userId);
        request.setAttribute("caTrackFlag", caTrackFlag);
        return new ModelAndView("resetPassword");
      }
    }
    if(!GenericValidator.isBlankOrNull(submitType) && ("resetSubmit").equals(submitType)){
      String password = request.getParameter("password");
      String caTrackFlag = request.getParameter("caTrackFlag") != null ? request.getParameter("caTrackFlag") : null; //Added by Thiyagu G to add a ca track flag on CATOOL.
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      Map statusMap = commonBusiness.resetPasswordPrc(userId, password, sessionUserId, caTrackFlag);
      String status = (String)statusMap.get("p_error_status");
      if(!GenericValidator.isBlankOrNull(status) && ("0").equals(status)){
        request.setAttribute("messageFlag", "found"); // Added by Prabha on 03-NOV-15 
        request.setAttribute("userId", userId); // Added by Prabha on 03-NOV-15 
        return new ModelAndView("resetPassword"); // Added by Prabha on 03-NOV-15
      }
    }
    response.sendError(404, "404 error message");
    return null;
  }
}
