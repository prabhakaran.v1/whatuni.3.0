package WUI.registration.controller;

import java.util.*;

import javax.servlet.http.*;

import WUI.registration.bean.*;
import WUI.registration.validator.UserLoginValidator;
import WUI.utilities.SessionData;

import com.layer.util.SpringConstants;
import com.wuni.util.sql.DataModel;

import WUI.utilities.CommonFunction;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;

import java.sql.ResultSet;

import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
  * @UserLoginAction.java
  * @Version : 2.0
  * @May 2 2007
  * @www.whatuni.com
  * @Created By : Balraj. Selva Kumar
  * @Purpose  : This program is used for the user login.
  * ************************************************************************************************************************
  * Date           Name                      Ver.        Changes desc                                              Rel Ver.
  * ************************************************************************************************************************
  * Nov 03 2015    Prabhakaran V.     WU_546_20151103    Forgot password redesign.                                  546
  * 24_Jan_2017    Thiyagu G          WU_561_20170124    Removed user track session id                              561
  * 19_03_2019     Sangeeth.S                            deleting the apply now cookie user id after logout 
  * 10_NOV_2020    Sujitha V                             Removing the YOE value when user logout
  */

@Controller
public class UserLoginController{
	
  @RequestMapping(value={"/userLogin","/sendPassword"}, method={RequestMethod.POST, RequestMethod.GET})	
  public ModelAndView userLogin(@ModelAttribute UserLoginBean userBean, BindingResult results ,HttpServletRequest request, HttpServletResponse response) {
    String forward = "showlogin";
    userBean.setRequest(request);
    UserLoginValidator validator = new UserLoginValidator();
    validator.validate(userBean, results);
    if(!results.hasErrors()) {
    HttpSession session = request.getSession();
    ServletContext context = request.getServletContext();
    ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    String blockStatus = new CommonFunction().checkEmailBlocked(userBean.getUserName(), null);  //Added for wu_537 24-FEB-2015 by Karthi to check user blocked or not 
    CookieManager cookieManager = new CookieManager();
    
    if (new CommonFunction().checkSessionExpired(request, response, session, "ANONYMOUS")) { // TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    if (request.getParameter("e") != null && request.getParameter("e").equalsIgnoreCase("Logout")) { // Logout from whatuni //
      if (session.getAttribute("showUserName") != null) {
        session.removeAttribute("showUserName");
      }
      if (session.getAttribute("userInfoList") != null) {
        session.removeAttribute("userInfoList");
      }
      if (session.getAttribute("userBasketPodList") != null) {
        session.removeAttribute("userBasketPodList");
      }
      if (session.getAttribute("basketpodcollegecount") != null) {
        session.removeAttribute("basketpodcollegecount");
      }
      if (session.getAttribute("basketpodcourseCount") != null) {
        session.removeAttribute("basketpodcourseCount");
      }
      if (session.getAttribute("subjectSessionId") != null) {
         session.removeAttribute("subjectSessionId");
       }
      if (session.getAttribute("USER_UCAS_SCORE") != null) {
        session.removeAttribute("USER_UCAS_SCORE");
      }
      if(StringUtils.isNotBlank((String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING))) {
        session.removeAttribute(SpringConstants.SESSION_YOE_LOGGING);
      }
      Cookie cookie = CookieManager.createCookie(request,"basketId", "");
      response.addCookie(cookie);
      /*Cookie cookies[] = request.getCookies();
      String cookieName = "";
      if (cookies != null) {
        for (int i = 0; i < cookies.length; i++) {
          cookieName = cookies[i].getName();
          if (cookieName != null && cookieName.trim().equalsIgnoreCase("basketId")) {
            cookies[i].setValue("");
            response.addCookie(cookies[i]);
            break;
          }
        }
      }*/
      // For mobile to log out from mobile when u log out from deskyop version
       if (session.getAttribute("loggedIn") != null) {
          session.removeAttribute("loggedIn");
       }
       if (session.getAttribute("mobileReferralURL") != null) {
          session.removeAttribute("mobileReferralURL");
       }       
       String wcacheDel = cookieManager.getCookieValue(request, "wcache");//08_OCT_2014 Added by amir for removing wcache       
       if(wcacheDel!=null && !"".equals(wcacheDel)) {
        Cookie wcacheDelete = cookieManager.deleteCookie(request, "wcache");
        response.addCookie(wcacheDelete);
       }
       //deleting the apply now user id cookie after logout by Sangeeth.S
       String applyNowCuserId = cookieManager.getCookieValue(request, GlobalConstants.AN_USER_ID_COOKIE);
       //System.out.println(" logout action cookie val " + applyNowCuserId);        
       if(applyNowCuserId!=null && !"".equals(applyNowCuserId)) {
        Cookie applyNowCUserIdDel = cookieManager.deleteCookie(request, GlobalConstants.AN_USER_ID_COOKIE);
        response.addCookie(applyNowCUserIdDel);
       }
      //
       Cookie cookieUser = CookieManager.createCookie(request,"__wuuid", "");
       cookieUser.setMaxAge(0);
       cookieUser.setPath("/");
       cookieUser.setComment("EXPIRING COOKIE at " + System.currentTimeMillis());
       response.addCookie(cookieUser);

       Cookie prosBasketIdCookie = CookieManager.createCookie(request,"prospectusBasketId", "");
       prosBasketIdCookie.setMaxAge(0);
       prosBasketIdCookie.setPath("/");
       prosBasketIdCookie.setComment("EXPIRING COOKIE at " + System.currentTimeMillis());
       response.addCookie(prosBasketIdCookie);
       //
       Cookie gradesPrepopulateCookie = cookieManager.createCookie("grades_prepopulate", "");
       gradesPrepopulateCookie.setMaxAge(0);
       gradesPrepopulateCookie.setPath("/");       
       gradesPrepopulateCookie.setComment("EXPIRING COOKIE at " + System.currentTimeMillis());
       response.addCookie(gradesPrepopulateCookie);
       //
        //
        Cookie socialPopupCookie = CookieManager.createCookie(request,"social_popup", "");
        socialPopupCookie.setMaxAge(0);
        socialPopupCookie.setPath("/");       
        socialPopupCookie.setComment("EXPIRING COOKIE at " + System.currentTimeMillis());
        response.addCookie(socialPopupCookie);
        
      session.removeAttribute("unibasketalert");  // Added for 15 dec release popup login functionality
      new SessionData().addData(request, response, "y", "");
      
      //Added by Thiyagu G for removing userTrackId on 24_Jan_2017      
      new SessionData().addData(request, response, "userTrackId", "");  
      session.removeAttribute("sessionUserTrackId");
      new SessionData().addData(request, response, "sessionUserTrackId", "");      
      forward = "homepage";      
    }
    boolean returnFlag = false;
    if ( "Sign in".equalsIgnoreCase(request.getParameter("buttext"))) { //Changed the buttext value to sign in Jan-27-16 Rel Indumathi
      if(blockStatus.equals("1")){
        /*ActionErrors error = new ActionErrors();
        error.add("invaliduser", new ActionMessage("wuni.error.login.pod.blockeduser"));*/
       // saveErrors(request, error);
        forward = "showlogin";
      }else{
        returnFlag = validateLogin(userBean, request, context, response);
        if (!returnFlag) {
          forward = "showlogin";
        } else {
          session.setAttribute("unibasketalert","yes"); // Added for 15 dec release popup login functionality
          forward = "redirectPage";
        }
      }
    }
    if (request.getParameter("buttext") != null && request.getParameter("buttext").equalsIgnoreCase("Log me in")) /// LOGIN FROM LOGGED POD //
    {
      returnFlag = validateLogin(userBean, request, context, response);
      if (!returnFlag) {
        String invalidUser = bundle.getString("wuni.error.login.pod.invalidusername");
        UserLoginBean userLoginBean = new UserLoginBean();
        BeanUtils.copyProperties(userBean, userLoginBean);
        session.setAttribute("LoginErrors", invalidUser);
        session.setAttribute("UserEmail", userLoginBean.getUserName());
        session.setAttribute("UserPassword", userLoginBean.getPassword());
        String logbackurl = (String)(session.getAttribute("logbackurl") != null && !String.valueOf(session.getAttribute("logbackurl")).equalsIgnoreCase("null") && String.valueOf(session.getAttribute("logbackurl")).trim().length() > 0? String.valueOf(session.getAttribute("logbackurl")).replaceAll("e=logout", ""): "/home.html");
        logbackurl = logbackurl != null && logbackurl.indexOf("e=logout") >= 0? "/home.html": logbackurl;
        session.setAttribute("LoginUrl", logbackurl);
      } else // newly added
      {
        String logbackurl = (String)(session.getAttribute("logbackurl") != null && !String.valueOf(session.getAttribute("logbackurl")).equalsIgnoreCase("null") && String.valueOf(session.getAttribute("logbackurl")).trim().length() > 0? String.valueOf(session.getAttribute("logbackurl")).replaceAll("e=logout", ""): "/home.html");
        logbackurl = logbackurl != null && logbackurl.indexOf("e=logout") >= 0? "/home.html": logbackurl;
        session.setAttribute("LoginUrl", logbackurl);
        session.setAttribute("unibasketalert","yes"); // Added for 15 dec release popup login functionality
      }
      forward = "LoginRedirectPage";
    }
    if (request.getParameter("buttext") != null && ("Send password").equalsIgnoreCase(request.getParameter("buttext"))) // SEND THE PASSWORD TO USER //
    {
      session.setAttribute("noSplashpopup","true"); // Added for wu_537 24-FEB-2015 by Karthi to remove login lightbox
      // Added by Sabapathi for GDPR changes to show generic message on forgot password form
      boolean fpFlag = true;
      fpFlag = sendPassword(userBean, request);
      /*
      if (!fpFlag) {
        request.setAttribute("messageflag", "notfound");
      } else {
        request.setAttribute("messageflag", "found");
      }
      */
      request.setAttribute("messageflag", "found");
      forward = "forgotPassword"; // Forward to forgot password page code added by Prabha on 03-NOV-15
    }
    //////////////////////////////////////// Load the User Basket courses & colleges from DB and store to cookies -- 3rd Feb release
    String userId = new SessionData().getData(request, "y");
    // added for 24th March Release
    if (userId != null && !userId.equals("0") && !userId.equalsIgnoreCase("null") && userId.trim().length() > 0) {
      session.removeAttribute("userBasketPodList");
      if (session.getAttribute("mySearches") != null) {
        session.removeAttribute("mySearches");
      }
      //new GlobalFunction().updateBasketContentLogin(request, userId, response);
      new CommonFunction().loadUserLoggedInformation(request, context, response);
    }
    //end of code for 24th Release 
    
    ////////////////////////////Added for 16/06/2009 release. Anon user review Status Changed to Active state////////////////////////////
    String userFrmReg = new SessionData().getData(request, "userIdAfterSubmit");
    if (userFrmReg != null && userFrmReg.equals("true") && returnFlag) {
      session.removeAttribute("collegeSelected");
      new GlobalFunction().preLoginUpdateStatusCode(request);
      new SessionData().removeData(request, response, "userIdAfterSubmit");
      return new ModelAndView("reviewlistpage");
    }
    if(!GenericValidator.isBlankOrNull(forward) && "homepage".equalsIgnoreCase(forward)){
      return new ModelAndView("redirect:/home.html");
    }
    }
    return new ModelAndView(forward, "userLoginBean", userBean);
  }

  /**
    *   This function is used to validate the user login.
    * @param form
    * @param request
    * @param context
    * @param response
    * @return login status as boolean.
    */
  private boolean validateLogin(UserLoginBean bean, HttpServletRequest request, ServletContext context, HttpServletResponse response) {
	    HttpSession session = request.getSession();
	    DataModel dataModel = new DataModel();
	    Vector vector = new Vector();
	    ;
	    String userName = bean.getUserName();
	    String password = bean.getPassword();
	    boolean flag = true;
	    String userId = "";
	    String sessionId = new SessionData().getData(request, "x");
	    ResultSet resultset = null;
	    try {
	      vector.clear();
	      vector.addElement(sessionId);
	      vector.addElement(GlobalConstants.WHATUNI_AFFILATE_ID);
	      vector.addElement(userName);
	      vector.addElement(password);
	      resultset = dataModel.getResultSet("Wu_Review_Pkg.check_username_password_fn", vector);
	      while (resultset.next()) {
	        userId = resultset.getString("user_id");
	        if (userId != null && !userId.trim().equals("0")) {
	          new SessionData().addData(request, response, "y", userId);
	          new SessionData().addData(request, response, "x", resultset.getString("session_id"));
	          String showUserName = new CommonFunction().getUserName(userId, request);
	          if (session.getAttribute("userInfoList") != null) {
	            session.removeAttribute("userInfoList");
	          }
	          ArrayList userInfoList = new CommonFunction().getUserInformation(userId, request, context);
	          session.setAttribute("emailalertmessage", "true");
	          if (userInfoList != null && userInfoList.size() > 0) {
	            session.setAttribute("userInfoList", userInfoList);
	            //new CommonFunction().loadBasketPod(request, response);
	            if (session.getAttribute("backPageURL") != null) {
	              String backPageURL = String.valueOf(session.getAttribute("backPageURL"));
	              if (backPageURL != null && backPageURL.trim().length() > 0) {
	                if (backPageURL.indexOf("y=0") > 0) {
	                  backPageURL = backPageURL.replaceAll("y=0", "y=" + userId);
	                  backPageURL = backPageURL.replaceAll("y=0", "y=" + userId);
	                  session.setAttribute("backPageURL", backPageURL);
	                  String xValue = backPageURL.substring(backPageURL.indexOf("x=") + 2, backPageURL.indexOf("y=") - 5);
	                  backPageURL = backPageURL.replaceAll(xValue, new SessionData().getData(request, "x"));
	                  session.setAttribute("backPageURL", backPageURL);
	                } else {
	                  if (backPageURL.indexOf("y=") > 0) {
	                    backPageURL = backPageURL.replaceAll("y=", "y=" + userId);
	                    session.setAttribute("backPageURL", backPageURL);
	                  }
	                }
	              }
	            }
	          }
	        } else if (userId != null && userId.equals("0")) {
	          request.setAttribute("invaliduser", ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.error.login.pod.invalidusername"));	
	          //error.add("invaliduser", new ActionMessage("wuni.error.login.pod.invalidusername"));
	         // saveErrors(request, error);
	          //request.setAttribute("showforgetpassword", "TRUE"); Commented for responsive redesign Dev-15-15 Indumathi.S
	          flag = false;
	        } else {
	          flag = false;
	        }
	      }
	      if (flag) {
	        session.removeAttribute("userBasketPodList");
	      }
	    } catch (Exception exception) {
	      exception.printStackTrace();
	    } finally {
	      dataModel.closeCursor(resultset);
	    }
	    return flag;
  }

  /**
    *   This function is used to resend the password to the given email address.
    * @param form
    * @param request
    * @return Password send status as boolean.
    */
  public boolean sendPassword(UserLoginBean bean, HttpServletRequest request) {
    DataModel dataModel = new DataModel();
    String emailId = bean.getEmailId();
           emailId = !GenericValidator.isBlankOrNull(emailId) ? emailId.trim() : emailId; // trim() is added by Prabha on Nov-03-2015
    String sessionId = new SessionData().getData(request, "x");
    String affId = GlobalConstants.WHATUNI_AFFILATE_ID;
    String status = "";
    Vector vector = new Vector();
    vector.clear();
    boolean returnFlag = true;
    try {
      vector.addElement(sessionId);
      vector.addElement(affId);
      vector.addElement(emailId);
      vector.addElement("WU_FORGOTTEN_PASSWORD");
      status = dataModel.executeUpdate("wu_email_pkg.forget_password_fn", vector);
      if (status != null && status.equals("1")) {
        returnFlag = false;
        /*ActionErrors errors = new ActionErrors();
        errors.clear();
        errors.add("invalidemailid", new ActionMessage("wuni.error.invalidemail"));*/
       // saveErrors(request, errors);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return returnFlag;
  }
}
