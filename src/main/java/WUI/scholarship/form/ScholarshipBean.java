package WUI.scholarship.form;

import org.apache.struts.action.ActionForm;

public class ScholarshipBean  {

  public ScholarshipBean() {
  }
  private String scholarshipId = "";
  private String scholarshipTitle = "";
  private String scholarshipValue = "";
  private String duration = "";
  private String contactSite = "";
  private String qualDetails = "";
  private String startDate = "";
  private String endDate = "";
  private String awardBody = "";
  private String awardId = "";
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String lastUpdatedDate = "";
  private String qualificationTitle = "";
  private String scholarshipInfoURL = "";
  private String searchCount = "";
  private String scholarshipCount = "";
  private String collegeInBasketFlag = "";
  private String prospectusPaidFlag = "";
  private String subjectProfileCount = "";
  private String webSitePaidFlag = "";
  private String collegeSite = "";
  private String categoryCode = "";
  private String qualification = "";
  private String searchText = "";
  private String totalSearchCount = "";
  private String scholarshipType = "";
  private String prospectusExternalUrl = "";
  private String subOrderItemId = "";
  private String subOrderWebsite = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";
  private String subOrderProspectusTargetURL = "";
  private String awardCriteria = "";
  private String awardSubject = "";
  private String studyLevel = "";
  private String noOfAwards = "";
  private String gender = "";
  private String department = "";
  //Award Value
  private String bursaryValue = "";
  private String awardDesc = "";
  private String feesCoverage = "";
  //

  public void setScholarshipId(String scholarshipId) {
    this.scholarshipId = scholarshipId;
  }

  public String getScholarshipId() {
    return scholarshipId;
  }

  public void setScholarshipTitle(String scholarshipTitle) {
    this.scholarshipTitle = scholarshipTitle;
  }

  public String getScholarshipTitle() {
    return scholarshipTitle;
  }

  public void setScholarshipValue(String scholarshipValue) {
    this.scholarshipValue = scholarshipValue;
  }

  public String getScholarshipValue() {
    return scholarshipValue;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  public String getDuration() {
    return duration;
  }

  public void setContactSite(String contactSite) {
    this.contactSite = contactSite;
  }

  public String getContactSite() {
    return contactSite;
  }

  public void setQualDetails(String qualDetails) {
    this.qualDetails = qualDetails;
  }

  public String getQualDetails() {
    return qualDetails;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setAwardBody(String awardBody) {
    this.awardBody = awardBody;
  }

  public String getAwardBody() {
    return awardBody;
  }

  public void setAwardId(String awardId) {
    this.awardId = awardId;
  }

  public String getAwardId() {
    return awardId;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setLastUpdatedDate(String lastUpdatedDate) {
    this.lastUpdatedDate = lastUpdatedDate;
  }

  public String getLastUpdatedDate() {
    return lastUpdatedDate;
  }

  public void setQualificationTitle(String qualificationTitle) {
    this.qualificationTitle = qualificationTitle;
  }

  public String getQualificationTitle() {
    return qualificationTitle;
  }

  public void setScholarshipInfoURL(String scholarshipInfoURL) {
    this.scholarshipInfoURL = scholarshipInfoURL;
  }

  public String getScholarshipInfoURL() {
    return scholarshipInfoURL;
  }

  public void setSearchCount(String searchCount) {
    this.searchCount = searchCount;
  }

  public String getSearchCount() {
    return searchCount;
  }

  public void setScholarshipCount(String scholarshipCount) {
    this.scholarshipCount = scholarshipCount;
  }

  public String getScholarshipCount() {
    return scholarshipCount;
  }

  public void setCollegeInBasketFlag(String collegeInBasketFlag) {
    this.collegeInBasketFlag = collegeInBasketFlag;
  }

  public String getCollegeInBasketFlag() {
    return collegeInBasketFlag;
  }

  public void setProspectusPaidFlag(String prospectusPaidFlag) {
    this.prospectusPaidFlag = prospectusPaidFlag;
  }

  public String getProspectusPaidFlag() {
    return prospectusPaidFlag;
  }

  public void setSubjectProfileCount(String subjectProfileCount) {
    this.subjectProfileCount = subjectProfileCount;
  }

  public String getSubjectProfileCount() {
    return subjectProfileCount;
  }

  public void setWebSitePaidFlag(String webSitePaidFlag) {
    this.webSitePaidFlag = webSitePaidFlag;
  }

  public String getWebSitePaidFlag() {
    return webSitePaidFlag;
  }

  public void setCollegeSite(String collegeSite) {
    this.collegeSite = collegeSite;
  }

  public String getCollegeSite() {
    return collegeSite;
  }

  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }

  public String getCategoryCode() {
    return categoryCode;
  }

  public void setQualification(String qualification) {
    this.qualification = qualification;
  }

  public String getQualification() {
    return qualification;
  }

  public void setSearchText(String searchText) {
    this.searchText = searchText;
  }

  public String getSearchText() {
    return searchText;
  }

  public void setTotalSearchCount(String totalSearchCount) {
    this.totalSearchCount = totalSearchCount;
  }

  public String getTotalSearchCount() {
    return totalSearchCount;
  }

  public void setScholarshipType(String scholarshipType) {
    this.scholarshipType = scholarshipType;
  }

  public String getScholarshipType() {
    return scholarshipType;
  }

  public void setProspectusExternalUrl(String prospectusExternalUrl) {
    this.prospectusExternalUrl = prospectusExternalUrl;
  }

  public String getProspectusExternalUrl() {
    return prospectusExternalUrl;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setSubOrderProspectusTargetURL(String subOrderProspectusTargetURL) {
    this.subOrderProspectusTargetURL = subOrderProspectusTargetURL;
  }

  public String getSubOrderProspectusTargetURL() {
    return subOrderProspectusTargetURL;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setAwardCriteria(String awardCriteria) {
    this.awardCriteria = awardCriteria;
  }

  public String getAwardCriteria() {
    return awardCriteria;
  }

  public void setAwardSubject(String awardSubject) {
    this.awardSubject = awardSubject;
  }

  public String getAwardSubject() {
    return awardSubject;
  }

  public void setStudyLevel(String studyLevel) {
    this.studyLevel = studyLevel;
  }

  public String getStudyLevel() {
    return studyLevel;
  }

  public void setNoOfAwards(String noOfAwards) {
    this.noOfAwards = noOfAwards;
  }

  public String getNoOfAwards() {
    return noOfAwards;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return gender;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  public String getDepartment() {
    return department;
  }

  public void setBursaryValue(String bursaryValue) {
    this.bursaryValue = bursaryValue;
  }

  public String getBursaryValue() {
    return bursaryValue;
  }

  public void setFeesCoverage(String feesCoverage) {
    this.feesCoverage = feesCoverage;
  }

  public String getFeesCoverage() {
    return feesCoverage;
  }

  public void setAwardDesc(String awardDesc) {
    this.awardDesc = awardDesc;
  }

  public String getAwardDesc() {
    return awardDesc;
  }

}
////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////    
