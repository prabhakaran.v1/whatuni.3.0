package WUI.itext;

import WUI.search.form.SearchBean;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;

import WUI.utilities.GlobalConstants;

import com.config.SpringContextInjector;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;
import java.io.*;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.sql.Clob;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.validator.GenericValidator;

/**
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Class         : InternalPdfServlet.java
 * Description   : Class file to loads the PDF with help of XML and XLS internal files using ITEXT
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * Author	                     Ver 	      Modified On     	Modification Details
 * *************************************************************************************************************************************
 * Prabhakaran V.              wu547                         Generate Pdf for Non advertiser details..
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
public class InternalPdfServlet extends HttpServlet {
  public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      String PDF_FILE = null;
      String XLS_FILE = null;
      String from = request.getParameter("from");
      String collegeId = request.getParameter("collegeId");
      String courseId = request.getParameter("courseId");
      String TODAY = getTodayDate();
      //
			 //Check the user login or not
			 try {
				 if (!(new SecurityEvaluator().checkSecurity("REGISTERED_USER", request, response))) {
					 response.sendRedirect(GlobalConstants.WU_CONTEXT_PATH + "/mywhatuni.html");
				 }
			 }
			 catch (Exception secutityException) {
				 secutityException.printStackTrace();
			 }
			 //End of login check
      CommonFunction common = new WUI.utilities.CommonFunction();
      if ("COURSE".equalsIgnoreCase(from)) {
        PDF_FILE = common.getCourseName(courseId, ""); //Changed new function on 16_May_2017, By Thiyagu G.
        //XLS_FILE = getServletContext().getRealPath("/itext/courseTemplate.xsl"); //TODO Comment while deployee in server and uncomment below line.
        XLS_FILE = common.getWUSysVarValue("WU_PDF_COURSE_TEMPLATE_PATH");
      } else if ("PROVIDER".equalsIgnoreCase(from)) {
        PDF_FILE = common.getCollegeDisplayName(collegeId);
        //XLS_FILE = getServletContext().getRealPath("/itext/prividerTemplate.xsl"); //TODO Comment while deployee in server and uncomment below line.
        XLS_FILE = common.getWUSysVarValue("WU_PDF_PROVIDER_TEMPLATE_PATH");
      }
      //
      //Below code is to get the XML content to generate the PDF   
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
			SearchBean searchbean = new SearchBean();
			searchbean.setCollegeId(collegeId);
			searchbean.setCourseid(courseId);
			Map getXMLMap = commonBusiness.getPDFXMLData(searchbean);
      Clob clob = (Clob)getXMLMap.get("P_RET_PDF");
      String XML_CONTENT = new String();
      XML_CONTENT = CommonFunction.getClobValue(clob);
      //System.out.println(XML_CONTENT);
      //End of XML data
      //
      //Converting the XML and XLS to HTML content and store to a variable xmlOutWriter
      StreamSource xmlInSource = new StreamSource(new StringReader(XML_CONTENT));
      TransformerFactory tFactory = TransformerFactory.newInstance();
      Transformer xslSource = tFactory.newTransformer(new StreamSource(new File(XLS_FILE)));
      StringWriter xmlOutWriter = new StringWriter();
      xslSource.transform(xmlInSource, new StreamResult(xmlOutWriter));
      String htmlContent = new String(xmlOutWriter.toString().getBytes(), "UTF-8");
      //
      Document document = new Document(PageSize.LETTER);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      PdfWriter pdfWriter = PdfWriter.getInstance(document, baos);
      HeaderFooter event = new HeaderFooter();
      pdfWriter.setBoxSize("art", new Rectangle(36, 54, 559, 788));
      pdfWriter.setPageEvent(event);
      response.setContentType("application/pdf");
      document.open();
      //System.out.println("htmlContent generated and doc opened successfully...");
      //
      document.addAuthor("whatuni");
      document.addCreator("whatuni");
      document.addSubject("Thanks for your support");
      document.addCreationDate();
      document.addTitle("www.whatuni.com");
      //     
      document.setMargins(0, 0, 0, 0);
      document.newPage();
      document.setMargins(0, 0, 20, 20);
      XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
      worker.parseXHtml(pdfWriter, document, new StringReader(htmlContent));
      //System.out.println("parseXHtml successfully generated...");
      document.close();
      //
      response.setHeader("Expires", "0");
      response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
      response.setHeader("Content-Disposition", "attachment;filename=\"" + PDF_FILE + (!GenericValidator.isBlankOrNull(PDF_FILE)? "-" + TODAY.replaceAll("/", "-"): "") + ".pdf\"");
      response.setContentType("application/pdf");
      response.setContentLength(baos.size());
      OutputStream os = response.getOutputStream();
      baos.writeTo(os);
      //System.out.println("PDF successfully generated...");
      os.flush();
      os.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * Method to get the Todays date to display in the PDF page
   * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public String getTodayDate() {
    DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
    String TODAY = formatter.format(new Date());
    String[] days = TODAY.split("-");
    String time = "";
    int day = Integer.parseInt(days[0]);
    if ((day >= 4 && day <= 20) || (day >= 24 && day <= 30)) {
      time = day + "th";
    } else if (day == 3 || day == 23) {
      time = day + "rd";
    } else if (day == 2 || day == 22) {
      time = day + "nd";
    } else if (day == 1 || day == 21 || day == 31) {
      time = day + "st";
    }
    String formatedDate = time + " " + days[1] + " " + days[2];
    return formatedDate;
  }
}
/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * @see inner class to draw the pagenation and footer link in the PDF
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
class HeaderFooter extends PdfPageEventHelper {
  /**
   * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see onEndPage Method to adds a header to every page
   * @param writer
   * @param document
   * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void onEndPage(PdfWriter writer, Document document) {
    PdfContentByte canvas = getCanvas(writer, 0, 0, 0, 0);
    try {
      if (writer.getPageNumber() > 1) {
        Chunk wuLink = new Chunk("www.whatuni.com", FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.NORMAL));
        wuLink.setAnchor("www.whatuni.com");
        addTextToCanvas(canvas, "" + (wuLink), 10, 500, 10, "#0080ff"); // whatuni link
        addTextToCanvas(canvas, "" + (writer.getPageNumber() - 1), 10, 590, 10, "#707070"); // right side pagination
      }
    } catch (MalformedURLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (DocumentException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see addTextToCanvas Method to add a footer content
   * @param canvas
   * @param text
   * @param size
   * @param text_x
   * @param text_y
   * @param canvasColor
   * @throws DocumentException
   * @throws IOException
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void addTextToCanvas(PdfContentByte canvas, String text, int size, float text_x, float text_y, String canvasColor) throws DocumentException, 
                                                                                                                                   IOException {
    BaseFont base_normal = null;
    try {
      base_normal = BaseFont.createFont("http://www.whatuni.com/wu-cont/cssstyles/fonts/Lato-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);//Added WHATUNI_SCHEME_NAME by Indumathi Mar-29-16
    } catch (com.itextpdf.text.DocumentException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    canvas.beginText();
    canvas.setFontAndSize(base_normal, size);
    canvas.moveText(text_x, text_y);
    canvas.setColorFill(getBaseColor(canvasColor));
    canvas.showText(text);
    canvas.endText();
  }

  /**
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see getBaseColor Method to get the base color of pagination and footer link
   * @param colorStr as color code
   * @return BaseColor
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public BaseColor getBaseColor(String colorStr) {
    return new BaseColor(Integer.parseInt(colorStr.substring(1, 3), 16), Integer.parseInt(colorStr.substring(3, 5), 16), Integer.parseInt(colorStr.substring(5, 7), 16));
  }

  /**
   * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see getCanvas Method to get canvas for writting footer
   * @param writer
   * @param x as x-coordinate of the top left corner
   * @param y as y-coordinate of the top left corner
   * @param w as width of the rectangle
   * @param h as  height of the rectangle
   * @return canvas
   * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public PdfContentByte getCanvas(PdfWriter writer, int x, int y, int w, int h) {
    PdfContentByte canvas = writer.getDirectContent();
    canvas.rectangle(x, y, w, h);
    canvas.setColorFill(getBaseColor("#FFFFFF"));
    canvas.fill();
    return canvas;
  }
}
