package WUI.basket.form;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class BasketBean {

  private String studentsStatisfied = "";
  private String studentJob = "";
  private String studentsStatisfieddisplay = "";
  private String studentJobdisplay = "";
  private String collegeId = "";
  private String subjectId = "";
  private String subjectCode = "";
  private String subjectName = "";
  private String subjectCount = "";
  private String collegeName = "";
  private String collegeNameDisplay = ""; 
  private String selectId = "";
  private String coursetitle = "";
  private String courseid = "";
  private String courseCount = "";
  private int countList = 0;
  private String[] empDataArray;
  private String duration = "";
  private String startDate = "";
  private String collegeWebSite = "";
  private String overAllRating = "";
  private String shortListFlag = "";
  private String associationId = "";
  private String associationType = "";
  private String basketId = "";
  private String associationName = "";
  private String associationNameDisplay = "";
  private String contactDetail = "";
  private String contactAddress = "";
  private String telephone = "";
  private String admissionEmail = "";
  private String address1 = "";
  private String address2 = "";
  private String town = "";
  private String county = "";
  private String questionTitle = "";
  private String questionId = "";
  private String seoStudyLevelText = "";
  private String emailPaidFlag = "";
  private String prospectusPaidFlag = "";
  private String websitePaidFlag = "";
  private String courseDeleted = "";
  private String removedSpecialCharCoursetitle = "";
  private String uniProfileFlag = "";
  private String collegeId_1 = "";
  private String collegeId_2 = "";
  private String collegeId_3 = "";
  private String collegeId_4 = "";
  private String courseId_1 = "";
  private String courseId_2 = "";
  private String courseId_3 = "";
  private String courseId_4 = "";
  private String prospectusPurchaseFlag_1 = "";
  private String prospectusPurchaseFlag_2 = "";
  private String prospectusPurchaseFlag_3 = "";
  private String prospectusPurchaseFlag_4 = "";
  private String prospectusExternalUrl_1 = "";
  private String prospectusExternalUrl_2 = "";
  private String prospectusExternalUrl_3 = "";
  private String prospectusExternalUrl_4 = "";
  private String profilePurchaseFlag_1 = "";
  private String profilePurchaseFlag_2 = "";
  private String profilePurchaseFlag_3 = "";
  private String profilePurchaseFlag_4 = "";
  private String websitePurchaseFlag_1 = "";
  private String websitePurchaseFlag_2 = "";
  private String websitePurchaseFlag_3 = "";
  private String websitePurchaseFlag_4 = "";
  private String subjectProfileCount_1 = "";
  private String subjectProfileCount_2 = "";
  private String subjectProfileCount_3 = "";
  private String subjectProfileCount_4 = "";
  private String collegeLogo_1 = "";
  private String collegeLogo_2 = "";
  private String collegeLogo_3 = "";
  private String collegeLogo_4 = "";
  private String collegeName_1 = "";
  private String collegeName_2 = "";
  private String collegeName_3 = "";
  private String collegeName_4 = "";
  private String collegeNameDisplay_1 = "";
  private String collegeNameDisplay_2 = "";
  private String collegeNameDisplay_3 = "";
  private String collegeNameDisplay_4 = "";
  private String courseName_1 = "";
  private String courseName_2 = "";
  private String courseName_3 = "";
  private String courseName_4 = "";
  private String seoStudyLevelText_1 = "";
  private String seoStudyLevelText_2 = "";
  private String seoStudyLevelText_3 = "";
  private String seoStudyLevelText_4 = "";
  private String searchName = "";
  private String pageUrl = "";
  private String webSiteAddress = "";
  private String courseCollegeId = "";
  private String courseCollegeName = "";
  private String courseStudyLevel = "";
  private String videoReviewId_1 = "";
  private String videoUrl_1 = "";
  private String videoThumbnail_1 = "";
  private String videoReviewId_2 = "";
  private String videoUrl_2 = "";
  private String videoThumbnail_2 = "";
  private String videoReviewId_3 = "";
  private String videoUrl_3 = "";
  private String videoThumbnail_3 = "";
  private String videoReviewId_4 = "";
  private String videoUrl_4 = "";
  private String videoThumbnail_4 = "";
  private String subOrderWebsite = "";
  private String subOrderItemId = "";
  private String subOrderProspectusTargetURL_1 = "";
  private String subOrderProspectusTargetURL_2 = "";
  private String subOrderProspectusTargetURL_3 = "";
  private String subOrderProspectusTargetURL_4 = "";
  private String subOrderWebsite_1 = "";
  private String subOrderWebsite_2 = "";
  private String subOrderWebsite_3 = "";
  private String subOrderWebsite_4 = "";
  private String subOrderItemId_1 = "";
  private String subOrderItemId_2 = "";
  private String subOrderItemId_3 = "";
  private String subOrderItemId_4 = "";
  private String cpeQualificationNetworkId = "";
  private String myhcProfileId_1 = "";
  private String myhcProfileId_2 = "";
  private String myhcProfileId_3 = "";
  private String myhcProfileId_4 = "";
  private String profileType_1 = "";
  private String profileType_2 = "";
  private String profileType_3 = "";
  private String profileType_4 = "";
  //Added By Sekhar K for wu420_31012012
  private String timesRanking = "";
  private String studentStaffRatio = "";  
  private String scholarshipsCount = "";
  private String accomodationFee = "";
  private String genderRatio = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";
  private String courseFee = "";
  private String entryPoints = "";
  
  private String basketName = null;
  private String basketCount = null;
  private String collegeUCASCode = null;
  private String courseUCASCode = null;
  private String providerDeleteFlag = null;
  private String courseDeleteFlag = null;
  private String basketType = null;
  private String existBasketId = null;
  private String personalNotes = null;
  private String userId = null;
  private String cookieBasketId = null;
  private String basketCompareCount = null;
  private String uniHomeURL = null;
  private String courseURL = null;
  private String showContentFlag = "TRUE";
  private String comparePage = null;
  private String clearingONOFF = null;
  private String hotLine = null;
  private String[][] prosBasketArray;
  private String prospectusBasketStatus = null;
  private String trackSessionId = null;
  private ArrayList basketList = null;//Added by Priyaa for 11-AUG-2015 RELease
  private String alreadyAddedFlag = null;
  
  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public String getSubjectId() {
    return subjectId;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }

  public String getSubjectName() {
    return subjectName;
  }

  public void setSubjectCount(String subjectCount) {
    this.subjectCount = subjectCount;
  }

  public String getSubjectCount() {
    return subjectCount;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCoursetitle(String coursetitle) {
    this.coursetitle = coursetitle;
  }

  public String getCoursetitle() {
    return coursetitle;
  }

  public void setCourseid(String courseid) {
    this.courseid = courseid;
  }

  public String getCourseid() {
    return courseid;
  }

  public void setSelectId(String selectId) {
    this.selectId = selectId;
  }

  public String getSelectId() {
    return selectId;
  }

  public void setCourseCount(String courseCount) {
    this.courseCount = courseCount;
  }

  public String getCourseCount() {
    return courseCount;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCountList(int countList) {
    this.countList = countList;
  }

  public int getCountList() {
    return countList;
  }

  public void setCollegeId_1(String collegeId_1) {
    this.collegeId_1 = collegeId_1;
  }

  public String getCollegeId_1() {
    return collegeId_1;
  }

  public void setCollegeId_2(String collegeId_2) {
    this.collegeId_2 = collegeId_2;
  }

  public String getCollegeId_2() {
    return collegeId_2;
  }

  public void setCollegeId_3(String collegeId_3) {
    this.collegeId_3 = collegeId_3;
  }

  public String getCollegeId_3() {
    return collegeId_3;
  }

  public void setEmpDataArray(String[] empDataArray) {
    this.empDataArray = empDataArray;
  }

  public String[] getEmpDataArray() {
    return empDataArray;
  }

  public void setDuration(String duration) {
    this.duration = duration;
  }

  public String getDuration() {
    return duration;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setCollegeWebSite(String collegeWebSite) {
    this.collegeWebSite = collegeWebSite;
  }

  public String getCollegeWebSite() {
    return collegeWebSite;
  }

  public void setOverAllRating(String overAllRating) {
    this.overAllRating = overAllRating;
  }

  public String getOverAllRating() {
    return overAllRating;
  }

  public void setShortListFlag(String shortListFlag) {
    this.shortListFlag = shortListFlag;
  }

  public String getShortListFlag() {
    return shortListFlag;
  }

  public void setAssociationType(String associationType) {
    this.associationType = associationType;
  }

  public String getAssociationType() {
    return associationType;
  }

  public void setBasketId(String basketId) {
    this.basketId = basketId;
  }

  public String getBasketId() {
    return basketId;
  }

  public void setAssociationName(String associationName) {
    this.associationName = associationName;
  }

  public String getAssociationName() {
    return associationName;
  }

  public void setAssociationId(String associationId) {
    this.associationId = associationId;
  }

  public String getAssociationId() {
    return associationId;
  }

  public void setSubjectCode(String subjectCode) {
    this.subjectCode = subjectCode;
  }

  public String getSubjectCode() {
    return subjectCode;
  }

  public void setContactDetail(String contactDetail) {
    this.contactDetail = contactDetail;
  }

  public String getContactDetail() {
    return contactDetail;
  }

  public void setContactAddress(String contactAddress) {
    this.contactAddress = contactAddress;
  }

  public String getContactAddress() {
    return contactAddress;
  }

  public void setTelephone(String telephone) {
    this.telephone = telephone;
  }

  public String getTelephone() {
    return telephone;
  }

  public void setAdmissionEmail(String admissionEmail) {
    this.admissionEmail = admissionEmail;
  }

  public String getAdmissionEmail() {
    return admissionEmail;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getAddress2() {
    return address2;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getTown() {
    return town;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  public String getCounty() {
    return county;
  }

  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }

  public String getQuestionTitle() {
    return questionTitle;
  }

  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }

  public String getQuestionId() {
    return questionId;
  }

  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }

  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }

  public void setEmailPaidFlag(String emailPaidFlag) {
    this.emailPaidFlag = emailPaidFlag;
  }

  public String getEmailPaidFlag() {
    return emailPaidFlag;
  }

  public void setProspectusPaidFlag(String prospectusPaidFlag) {
    this.prospectusPaidFlag = prospectusPaidFlag;
  }

  public String getProspectusPaidFlag() {
    return prospectusPaidFlag;
  }

  public void setWebsitePaidFlag(String websitePaidFlag) {
    this.websitePaidFlag = websitePaidFlag;
  }

  public String getWebsitePaidFlag() {
    return websitePaidFlag;
  }

  public void setRemovedSpecialCharCoursetitle(String removedSpecialCharCoursetitle) {
    this.removedSpecialCharCoursetitle = removedSpecialCharCoursetitle;
  }

  public String getRemovedSpecialCharCoursetitle() {
    return removedSpecialCharCoursetitle;
  }

  public void setCourseDeleted(String courseDeleted) {
    this.courseDeleted = courseDeleted;
  }

  public String getCourseDeleted() {
    return courseDeleted;
  }

  public void setStudentsStatisfieddisplay(String studentsStatisfieddisplay) {
    this.studentsStatisfieddisplay = studentsStatisfieddisplay;
  }

  public String getStudentsStatisfieddisplay() {
    return studentsStatisfieddisplay;
  }

  public void setStudentJobdisplay(String studentJobdisplay) {
    this.studentJobdisplay = studentJobdisplay;
  }

  public String getStudentJobdisplay() {
    return studentJobdisplay;
  }

  public void setStudentsStatisfied(String studentsStatisfied) {
    this.studentsStatisfied = studentsStatisfied;
  }

  public String getStudentsStatisfied() {
    return studentsStatisfied;
  }

  public void setStudentJob(String studentJob) {
    this.studentJob = studentJob;
  }

  public String getStudentJob() {
    return studentJob;
  }

  public void setCollegeId_4(String collegeId_4) {
    this.collegeId_4 = collegeId_4;
  }

  public String getCollegeId_4() {
    return collegeId_4;
  }

  public void setUniProfileFlag(String uniProfileFlag) {
    this.uniProfileFlag = uniProfileFlag;
  }

  public String getUniProfileFlag() {
    return uniProfileFlag;
  }

  public void setProspectusPurchaseFlag_1(String prospectusPurchaseFlag_1) {
    this.prospectusPurchaseFlag_1 = prospectusPurchaseFlag_1;
  }

  public String getProspectusPurchaseFlag_1() {
    return prospectusPurchaseFlag_1;
  }

  public void setProspectusPurchaseFlag_2(String prospectusPurchaseFlag_2) {
    this.prospectusPurchaseFlag_2 = prospectusPurchaseFlag_2;
  }

  public String getProspectusPurchaseFlag_2() {
    return prospectusPurchaseFlag_2;
  }

  public void setProspectusPurchaseFlag_3(String prospectusPurchaseFlag_3) {
    this.prospectusPurchaseFlag_3 = prospectusPurchaseFlag_3;
  }

  public String getProspectusPurchaseFlag_3() {
    return prospectusPurchaseFlag_3;
  }

  public void setProspectusPurchaseFlag_4(String prospectusPurchaseFlag_4) {
    this.prospectusPurchaseFlag_4 = prospectusPurchaseFlag_4;
  }

  public String getProspectusPurchaseFlag_4() {
    return prospectusPurchaseFlag_4;
  }

  public void setProfilePurchaseFlag_1(String profilePurchaseFlag_1) {
    this.profilePurchaseFlag_1 = profilePurchaseFlag_1;
  }

  public String getProfilePurchaseFlag_1() {
    return profilePurchaseFlag_1;
  }

  public void setProfilePurchaseFlag_2(String profilePurchaseFlag_2) {
    this.profilePurchaseFlag_2 = profilePurchaseFlag_2;
  }

  public String getProfilePurchaseFlag_2() {
    return profilePurchaseFlag_2;
  }

  public void setProfilePurchaseFlag_3(String profilePurchaseFlag_3) {
    this.profilePurchaseFlag_3 = profilePurchaseFlag_3;
  }

  public String getProfilePurchaseFlag_3() {
    return profilePurchaseFlag_3;
  }

  public void setProfilePurchaseFlag_4(String profilePurchaseFlag_4) {
    this.profilePurchaseFlag_4 = profilePurchaseFlag_4;
  }

  public String getProfilePurchaseFlag_4() {
    return profilePurchaseFlag_4;
  }

  public void setWebsitePurchaseFlag_1(String websitePurchaseFlag_1) {
    this.websitePurchaseFlag_1 = websitePurchaseFlag_1;
  }

  public String getWebsitePurchaseFlag_1() {
    return websitePurchaseFlag_1;
  }

  public void setWebsitePurchaseFlag_2(String websitePurchaseFlag_2) {
    this.websitePurchaseFlag_2 = websitePurchaseFlag_2;
  }

  public String getWebsitePurchaseFlag_2() {
    return websitePurchaseFlag_2;
  }

  public void setWebsitePurchaseFlag_3(String websitePurchaseFlag_3) {
    this.websitePurchaseFlag_3 = websitePurchaseFlag_3;
  }

  public String getWebsitePurchaseFlag_3() {
    return websitePurchaseFlag_3;
  }

  public void setWebsitePurchaseFlag_4(String websitePurchaseFlag_4) {
    this.websitePurchaseFlag_4 = websitePurchaseFlag_4;
  }

  public String getWebsitePurchaseFlag_4() {
    return websitePurchaseFlag_4;
  }

  public void setCollegeName_1(String collegeName_1) {
    this.collegeName_1 = collegeName_1;
  }

  public String getCollegeName_1() {
    return collegeName_1;
  }

  public void setCollegeName_2(String collegeName_2) {
    this.collegeName_2 = collegeName_2;
  }

  public String getCollegeName_2() {
    return collegeName_2;
  }

  public void setCollegeName_3(String collegeName_3) {
    this.collegeName_3 = collegeName_3;
  }

  public String getCollegeName_3() {
    return collegeName_3;
  }

  public void setCollegeName_4(String collegeName_4) {
    this.collegeName_4 = collegeName_4;
  }

  public String getCollegeName_4() {
    return collegeName_4;
  }

  public void setCollegeLogo_1(String collegeLogo_1) {
    this.collegeLogo_1 = collegeLogo_1;
  }

  public String getCollegeLogo_1() {
    return collegeLogo_1;
  }

  public void setCollegeLogo_2(String collegeLogo_2) {
    this.collegeLogo_2 = collegeLogo_2;
  }

  public String getCollegeLogo_2() {
    return collegeLogo_2;
  }

  public void setCollegeLogo_3(String collegeLogo_3) {
    this.collegeLogo_3 = collegeLogo_3;
  }

  public String getCollegeLogo_3() {
    return collegeLogo_3;
  }

  public void setCollegeLogo_4(String collegeLogo_4) {
    this.collegeLogo_4 = collegeLogo_4;
  }

  public String getCollegeLogo_4() {
    return collegeLogo_4;
  }

  public void setSubjectProfileCount_1(String subjectProfileCount_1) {
    this.subjectProfileCount_1 = subjectProfileCount_1;
  }

  public String getSubjectProfileCount_1() {
    return subjectProfileCount_1;
  }

  public void setSubjectProfileCount_2(String subjectProfileCount_2) {
    this.subjectProfileCount_2 = subjectProfileCount_2;
  }

  public String getSubjectProfileCount_2() {
    return subjectProfileCount_2;
  }

  public void setSubjectProfileCount_3(String subjectProfileCount_3) {
    this.subjectProfileCount_3 = subjectProfileCount_3;
  }

  public String getSubjectProfileCount_3() {
    return subjectProfileCount_3;
  }

  public void setSubjectProfileCount_4(String subjectProfileCount_4) {
    this.subjectProfileCount_4 = subjectProfileCount_4;
  }

  public String getSubjectProfileCount_4() {
    return subjectProfileCount_4;
  }

  public void setCourseId_1(String courseId_1) {
    this.courseId_1 = courseId_1;
  }

  public String getCourseId_1() {
    return courseId_1;
  }

  public void setCourseId_2(String courseId_2) {
    this.courseId_2 = courseId_2;
  }

  public String getCourseId_2() {
    return courseId_2;
  }

  public void setCourseId_3(String courseId_3) {
    this.courseId_3 = courseId_3;
  }

  public String getCourseId_3() {
    return courseId_3;
  }

  public void setCourseId_4(String courseId_4) {
    this.courseId_4 = courseId_4;
  }

  public String getCourseId_4() {
    return courseId_4;
  }

  public void setCourseName_1(String courseName_1) {
    this.courseName_1 = courseName_1;
  }

  public String getCourseName_1() {
    return courseName_1;
  }

  public void setCourseName_2(String courseName_2) {
    this.courseName_2 = courseName_2;
  }

  public String getCourseName_2() {
    return courseName_2;
  }

  public void setCourseName_3(String courseName_3) {
    this.courseName_3 = courseName_3;
  }

  public String getCourseName_3() {
    return courseName_3;
  }

  public void setCourseName_4(String courseName_4) {
    this.courseName_4 = courseName_4;
  }

  public String getCourseName_4() {
    return courseName_4;
  }

  public void setSeoStudyLevelText_1(String seoStudyLevelText_1) {
    this.seoStudyLevelText_1 = seoStudyLevelText_1;
  }

  public String getSeoStudyLevelText_1() {
    return seoStudyLevelText_1;
  }

  public void setSeoStudyLevelText_2(String seoStudyLevelText_2) {
    this.seoStudyLevelText_2 = seoStudyLevelText_2;
  }

  public String getSeoStudyLevelText_2() {
    return seoStudyLevelText_2;
  }

  public void setSeoStudyLevelText_3(String seoStudyLevelText_3) {
    this.seoStudyLevelText_3 = seoStudyLevelText_3;
  }

  public String getSeoStudyLevelText_3() {
    return seoStudyLevelText_3;
  }

  public void setSeoStudyLevelText_4(String seoStudyLevelText_4) {
    this.seoStudyLevelText_4 = seoStudyLevelText_4;
  }

  public String getSeoStudyLevelText_4() {
    return seoStudyLevelText_4;
  }

  public void setSearchName(String searchName) {
    this.searchName = searchName;
  }

  public String getSearchName() {
    return searchName;
  }

  public void setPageUrl(String pageUrl) {
    this.pageUrl = pageUrl;
  }

  public String getPageUrl() {
    return pageUrl;
  }

  public void setProspectusExternalUrl_1(String prospectusExternalUrl_1) {
    this.prospectusExternalUrl_1 = prospectusExternalUrl_1;
  }

  public String getProspectusExternalUrl_1() {
    return prospectusExternalUrl_1;
  }

  public void setProspectusExternalUrl_2(String prospectusExternalUrl_2) {
    this.prospectusExternalUrl_2 = prospectusExternalUrl_2;
  }

  public String getProspectusExternalUrl_2() {
    return prospectusExternalUrl_2;
  }

  public void setProspectusExternalUrl_3(String prospectusExternalUrl_3) {
    this.prospectusExternalUrl_3 = prospectusExternalUrl_3;
  }

  public String getProspectusExternalUrl_3() {
    return prospectusExternalUrl_3;
  }

  public void setProspectusExternalUrl_4(String prospectusExternalUrl_4) {
    this.prospectusExternalUrl_4 = prospectusExternalUrl_4;
  }

  public String getProspectusExternalUrl_4() {
    return prospectusExternalUrl_4;
  }

  public void setWebSiteAddress(String webSiteAddress) {
    this.webSiteAddress = webSiteAddress;
  }

  public String getWebSiteAddress() {
    return webSiteAddress;
  }

  public void setCourseCollegeId(String courseCollegeId) {
    this.courseCollegeId = courseCollegeId;
  }

  public String getCourseCollegeId() {
    return courseCollegeId;
  }

  public void setCourseStudyLevel(String courseStudyLevel) {
    this.courseStudyLevel = courseStudyLevel;
  }

  public String getCourseStudyLevel() {
    return courseStudyLevel;
  }

  public void setCourseCollegeName(String courseCollegeName) {
    this.courseCollegeName = courseCollegeName;
  }

  public String getCourseCollegeName() {
    return courseCollegeName;
  }

  public void setVideoReviewId_1(String videoReviewId_1) {
    this.videoReviewId_1 = videoReviewId_1;
  }

  public String getVideoReviewId_1() {
    return videoReviewId_1;
  }

  public void setVideoUrl_1(String videoUrl_1) {
    this.videoUrl_1 = videoUrl_1;
  }

  public String getVideoUrl_1() {
    return videoUrl_1;
  }

  public void setVideoThumbnail_1(String videoThumbnail_1) {
    this.videoThumbnail_1 = videoThumbnail_1;
  }

  public String getVideoThumbnail_1() {
    return videoThumbnail_1;
  }

  public void setVideoReviewId_2(String videoReviewId_2) {
    this.videoReviewId_2 = videoReviewId_2;
  }

  public String getVideoReviewId_2() {
    return videoReviewId_2;
  }

  public void setVideoUrl_2(String videoUrl_2) {
    this.videoUrl_2 = videoUrl_2;
  }

  public String getVideoUrl_2() {
    return videoUrl_2;
  }

  public void setVideoThumbnail_2(String videoThumbnail_2) {
    this.videoThumbnail_2 = videoThumbnail_2;
  }

  public String getVideoThumbnail_2() {
    return videoThumbnail_2;
  }

  public void setVideoReviewId_3(String videoReviewId_3) {
    this.videoReviewId_3 = videoReviewId_3;
  }

  public String getVideoReviewId_3() {
    return videoReviewId_3;
  }

  public void setVideoUrl_3(String videoUrl_3) {
    this.videoUrl_3 = videoUrl_3;
  }

  public String getVideoUrl_3() {
    return videoUrl_3;
  }

  public void setVideoThumbnail_3(String videoThumbnail_3) {
    this.videoThumbnail_3 = videoThumbnail_3;
  }

  public String getVideoThumbnail_3() {
    return videoThumbnail_3;
  }

  public void setVideoReviewId_4(String videoReviewId_4) {
    this.videoReviewId_4 = videoReviewId_4;
  }

  public String getVideoReviewId_4() {
    return videoReviewId_4;
  }

  public void setVideoUrl_4(String videoUrl_4) {
    this.videoUrl_4 = videoUrl_4;
  }

  public String getVideoUrl_4() {
    return videoUrl_4;
  }

  public void setVideoThumbnail_4(String videoThumbnail_4) {
    this.videoThumbnail_4 = videoThumbnail_4;
  }

  public String getVideoThumbnail_4() {
    return videoThumbnail_4;
  }

  public void setSubOrderProspectusTargetURL_1(String subOrderProspectusTargetURL_1) {
    this.subOrderProspectusTargetURL_1 = subOrderProspectusTargetURL_1;
  }

  public String getSubOrderProspectusTargetURL_1() {
    return subOrderProspectusTargetURL_1;
  }

  public void setSubOrderProspectusTargetURL_2(String subOrderProspectusTargetURL_2) {
    this.subOrderProspectusTargetURL_2 = subOrderProspectusTargetURL_2;
  }

  public String getSubOrderProspectusTargetURL_2() {
    return subOrderProspectusTargetURL_2;
  }

  public void setSubOrderProspectusTargetURL_3(String subOrderProspectusTargetURL_3) {
    this.subOrderProspectusTargetURL_3 = subOrderProspectusTargetURL_3;
  }

  public String getSubOrderProspectusTargetURL_3() {
    return subOrderProspectusTargetURL_3;
  }

  public void setSubOrderProspectusTargetURL_4(String subOrderProspectusTargetURL_4) {
    this.subOrderProspectusTargetURL_4 = subOrderProspectusTargetURL_4;
  }

  public String getSubOrderProspectusTargetURL_4() {
    return subOrderProspectusTargetURL_4;
  }

  public void setSubOrderWebsite_1(String subOrderWebsite_1) {
    this.subOrderWebsite_1 = subOrderWebsite_1;
  }

  public String getSubOrderWebsite_1() {
    return subOrderWebsite_1;
  }

  public void setSubOrderWebsite_2(String subOrderWebsite_2) {
    this.subOrderWebsite_2 = subOrderWebsite_2;
  }

  public String getSubOrderWebsite_2() {
    return subOrderWebsite_2;
  }

  public void setSubOrderWebsite_3(String subOrderWebsite_3) {
    this.subOrderWebsite_3 = subOrderWebsite_3;
  }

  public String getSubOrderWebsite_3() {
    return subOrderWebsite_3;
  }

  public void setSubOrderWebsite_4(String subOrderWebsite_4) {
    this.subOrderWebsite_4 = subOrderWebsite_4;
  }

  public String getSubOrderWebsite_4() {
    return subOrderWebsite_4;
  }

  public void setSubOrderItemId_1(String subOrderItemId_1) {
    this.subOrderItemId_1 = subOrderItemId_1;
  }

  public String getSubOrderItemId_1() {
    return subOrderItemId_1;
  }

  public void setSubOrderItemId_2(String subOrderItemId_2) {
    this.subOrderItemId_2 = subOrderItemId_2;
  }

  public String getSubOrderItemId_2() {
    return subOrderItemId_2;
  }

  public void setSubOrderItemId_3(String subOrderItemId_3) {
    this.subOrderItemId_3 = subOrderItemId_3;
  }

  public String getSubOrderItemId_3() {
    return subOrderItemId_3;
  }

  public void setSubOrderItemId_4(String subOrderItemId_4) {
    this.subOrderItemId_4 = subOrderItemId_4;
  }

  public String getSubOrderItemId_4() {
    return subOrderItemId_4;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setMyhcProfileId_1(String myhcProfileId_1) {
    this.myhcProfileId_1 = myhcProfileId_1;
  }

  public String getMyhcProfileId_1() {
    return myhcProfileId_1;
  }

  public void setMyhcProfileId_2(String myhcProfileId_2) {
    this.myhcProfileId_2 = myhcProfileId_2;
  }

  public String getMyhcProfileId_2() {
    return myhcProfileId_2;
  }

  public void setMyhcProfileId_3(String myhcProfileId_3) {
    this.myhcProfileId_3 = myhcProfileId_3;
  }

  public String getMyhcProfileId_3() {
    return myhcProfileId_3;
  }

  public void setMyhcProfileId_4(String myhcProfileId_4) {
    this.myhcProfileId_4 = myhcProfileId_4;
  }

  public String getMyhcProfileId_4() {
    return myhcProfileId_4;
  }

  public void setProfileType_1(String profileType_1) {
    this.profileType_1 = profileType_1;
  }

  public String getProfileType_1() {
    return profileType_1;
  }

  public void setProfileType_2(String profileType_2) {
    this.profileType_2 = profileType_2;
  }

  public String getProfileType_2() {
    return profileType_2;
  }

  public void setProfileType_3(String profileType_3) {
    this.profileType_3 = profileType_3;
  }

  public String getProfileType_3() {
    return profileType_3;
  }

  public void setProfileType_4(String profileType_4) {
    this.profileType_4 = profileType_4;
  }

  public String getProfileType_4() {
    return profileType_4;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setAssociationNameDisplay(String associationNameDisplay) {
    this.associationNameDisplay = associationNameDisplay;
  }

  public String getAssociationNameDisplay() {
    return associationNameDisplay;
  }

  public void setCollegeNameDisplay_1(String collegeNameDisplay_1) {
    this.collegeNameDisplay_1 = collegeNameDisplay_1;
  }

  public String getCollegeNameDisplay_1() {
    return collegeNameDisplay_1;
  }

  public void setCollegeNameDisplay_2(String collegeNameDisplay_2) {
    this.collegeNameDisplay_2 = collegeNameDisplay_2;
  }

  public String getCollegeNameDisplay_2() {
    return collegeNameDisplay_2;
  }

  public void setCollegeNameDisplay_3(String collegeNameDisplay_3) {
    this.collegeNameDisplay_3 = collegeNameDisplay_3;
  }

  public String getCollegeNameDisplay_3() {
    return collegeNameDisplay_3;
  }

  public void setCollegeNameDisplay_4(String collegeNameDisplay_4) {
    this.collegeNameDisplay_4 = collegeNameDisplay_4;
  }

  public String getCollegeNameDisplay_4() {
    return collegeNameDisplay_4;
  }
    public void setTimesRanking(String timesRanking) {
        this.timesRanking = timesRanking;
    }

    public String getTimesRanking() {
        return timesRanking;
    }

    public void setStudentStaffRatio(String studentStaffRatio) {
        this.studentStaffRatio = studentStaffRatio;
    }

    public String getStudentStaffRatio() {
        return studentStaffRatio;
    }

    public void setScholarshipsCount(String scholarshipsCount) {
        this.scholarshipsCount = scholarshipsCount;
    }

    public String getScholarshipsCount() {
        return scholarshipsCount;
    }
    public void setAccomodationFee(String accomodationFee) {
        this.accomodationFee = accomodationFee;
    }

    public String getAccomodationFee() {
        return accomodationFee;
    }

    public void setGenderRatio(String genderRatio) {
        this.genderRatio = genderRatio;
    }

    public String getGenderRatio() {
        return genderRatio;
    }

    public void setSubOrderEmail(String subOrderEmail) {
        this.subOrderEmail = subOrderEmail;
    }

    public String getSubOrderEmail() {
        return subOrderEmail;
    }

    public void setSubOrderEmailWebform(String subOrderEmailWebform) {
        this.subOrderEmailWebform = subOrderEmailWebform;
    }

    public String getSubOrderEmailWebform() {
        return subOrderEmailWebform;
    }

    public void setSubOrderProspectus(String subOrderProspectus) {
        this.subOrderProspectus = subOrderProspectus;
    }

    public String getSubOrderProspectus() {
        return subOrderProspectus;
    }

    public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
        this.subOrderProspectusWebform = subOrderProspectusWebform;
    }

    public String getSubOrderProspectusWebform() {
        return subOrderProspectusWebform;
    }

    public void setCourseFee(String courseFee) {
        this.courseFee = courseFee;
    }

    public String getCourseFee() {
        return courseFee;
    }

    public void setEntryPoints(String entryPoints) {
        this.entryPoints = entryPoints;
    }

    public String getEntryPoints() {
        return entryPoints;
    }

  public void setBasketName(String basketName)
  {
    this.basketName = basketName;
  }

  public String getBasketName()
  {
    return basketName;
  }

  public void setBasketCount(String basketCount)
  {
    this.basketCount = basketCount;
  }

  public String getBasketCount()
  {
    return basketCount;
  }

  public void setCollegeUCASCode(String collegeUCASCode)
  {
    this.collegeUCASCode = collegeUCASCode;
  }

  public String getCollegeUCASCode()
  {
    return collegeUCASCode;
  }

  public void setCourseUCASCode(String courseUCASCode)
  {
    this.courseUCASCode = courseUCASCode;
  }

  public String getCourseUCASCode()
  {
    return courseUCASCode;
  }

  public void setProviderDeleteFlag(String providerDeleteFlag)
  {
    this.providerDeleteFlag = providerDeleteFlag;
  }

  public String getProviderDeleteFlag()
  {
    return providerDeleteFlag;
  }

  public void setCourseDeleteFlag(String courseDeleteFlag)
  {
    this.courseDeleteFlag = courseDeleteFlag;
  }

  public String getCourseDeleteFlag()
  {
    return courseDeleteFlag;
  }

  public void setBasketType(String basketType)
  {
    this.basketType = basketType;
  }

  public String getBasketType()
  {
    return basketType;
  }

  public void setExistBasketId(String existBasketId)
  {
    this.existBasketId = existBasketId;
  }

  public String getExistBasketId()
  {
    return existBasketId;
  }

  public void setPersonalNotes(String personalNotes)
  {
    this.personalNotes = personalNotes;
  }

  public String getPersonalNotes()
  {
    return personalNotes;
  }

  public void setUserId(String userId)
  {
    this.userId = userId;
  }

  public String getUserId()
  {
    return userId;
  }

  public void setCookieBasketId(String cookieBasketId)
  {
    this.cookieBasketId = cookieBasketId;
  }

  public String getCookieBasketId()
  {
    return cookieBasketId;
  }

  public void setBasketCompareCount(String basketCompareCount)
  {
    this.basketCompareCount = basketCompareCount;
  }

  public String getBasketCompareCount()
  {
    return basketCompareCount;
  }

  public void setUniHomeURL(String uniHomeURL)
  {
    this.uniHomeURL = uniHomeURL;
  }

  public String getUniHomeURL()
  {
    return uniHomeURL;
  }

  public void setCourseURL(String courseURL)
  {
    this.courseURL = courseURL;
  }

  public String getCourseURL()
  {
    return courseURL;
  }

  public void setShowContentFlag(String showContentFlag)
  {
    this.showContentFlag = showContentFlag;
  }

  public String getShowContentFlag()
  {
    return showContentFlag;
  }

  public void setComparePage(String comparePage)
  {
    this.comparePage = comparePage;
  }

  public String getComparePage()
  {
    return comparePage;
  }

  public void setClearingONOFF(String clearingONOFF)
  {
    this.clearingONOFF = clearingONOFF;
  }

  public String getClearingONOFF()
  {
    return clearingONOFF;
  }

  public void setHotLine(String hotLine)
  {
    this.hotLine = hotLine;
  }

  public String getHotLine()
  {
    return hotLine;
  }
  public void setProsBasketArray(String[][] prosBasketArray) {
    this.prosBasketArray = prosBasketArray;
  }
  public String[][] getProsBasketArray() {
    return prosBasketArray;
  }
  public void setProspectusBasketStatus(String prospectusBasketStatus) {
    this.prospectusBasketStatus = prospectusBasketStatus;
  }
  public String getProspectusBasketStatus() {
    return prospectusBasketStatus;
  }
  public void setTrackSessionId(String trackSessionId) {
    this.trackSessionId = trackSessionId;
  }
  public String getTrackSessionId() {
    return trackSessionId;
  }
  public void setBasketList(ArrayList basketList) {
    this.basketList = basketList;
  }
  public ArrayList getBasketList() {
    return basketList;
  }
  public void setAlreadyAddedFlag(String alreadyAddedFlag) {
    this.alreadyAddedFlag = alreadyAddedFlag;
  }
  public String getAlreadyAddedFlag() {
    return alreadyAddedFlag;
  }
}
//////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////////////    
