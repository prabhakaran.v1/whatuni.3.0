package WUI.basket.controller;

import WUI.basket.form.BasketBean;

import com.layer.util.SpringConstants;
import com.wuni.util.sql.DataModel;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.servlet.http.*;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
  * @Version :  .0
  * @June 30 2007   
  * @www.whatuni.com
  * @Created By : Balraj. Selva Kumar
  * @Purpose  : This program is used to add selected college/course to user basket using AJAX.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 16.05.2017     Prabhakaran V.             1.1      
  * 23.10.2018     Sabapathi S.               1.2     Added screen width for stats logging                         wu_582
  * 09_MAR_2020    Sangeeth.S		   		  1.3     Added lat and long for stats logging                         wu_300320
  * 17_SEP_2020    Sri Sankari                1.4     Added stats log for tariff logging(UCAS point)               wu_20200917
  * 10_NOV_2020    Sujitha V                  1.5     Added session for YOE to log in d_session_log.               wu_20201110
  */
@Controller
public class BasketLinkController {

  @RequestMapping(value = "/addbasket", method = {RequestMethod.POST, RequestMethod.GET})
  public ModelAndView addBasket(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
    session.removeAttribute("userBasketPodList");
    CommonFunction common = new CommonFunction();
    SessionData sessiondata = new SessionData();
    BasketBean bean = new BasketBean();
    BasketBean inputBean = new BasketBean();

    if (common.checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED //
      response.sendRedirect(GlobalConstants.WU_CONTEXT_PATH + "/mywhatuni.html");
      return null;
    }
    
    sessiondata.setParameterValuestoSessionData(request, response);
    String assocId = "";
    String assocType = "";
    assocId =   GenericValidator.isBlankOrNull(request.getParameter("assocId")) ? "" : request.getParameter("assocId");
    assocType = GenericValidator.isBlankOrNull(request.getParameter("assocType")) ? "" : request.getParameter("assocType");
    String comparepage = GenericValidator.isBlankOrNull(request.getParameter("comppage")) ? "NORMAL" : request.getParameter("comppage");
    String tabSelected = GenericValidator.isBlankOrNull(request.getParameter("tabSelected")) ? "" : request.getParameter("tabSelected");
    String fromClearing = GenericValidator.isBlankOrNull(request.getParameter("fromClearing")) ? "" : request.getParameter("fromClearing");
    bean.setAssociationId(assocId);
    String cookieBasketId = common.checkCookieStatus(request);
    String returnBasketId = "";
    String compareallIds = request.getParameter("compareallIds");
    String compareallType = request.getParameter("compareallType");
    String[] compareallIdsArray = null;
    inputBean.setComparePage(comparepage);
    if(request.getParameter("compareallIds") != null){
      inputBean.setAssociationId(compareallIds);
      inputBean.setBasketId(cookieBasketId);
      if (request.getParameter("dowhat") != null && request.getParameter("dowhat").equalsIgnoreCase("add")){
      compareallIdsArray = compareallIds.split(","); 
      if(cookieBasketId == null || cookieBasketId.equals("") || cookieBasketId.trim().length() == 0){
        if("OTHER_COURSES".equalsIgnoreCase(compareallType)){
          inputBean.setAssociationType("O");
          bean = addBasketContent(inputBean, request, session);
        }else{
          inputBean.setAssociationType("C");
          bean = addBasketContent(inputBean, request, session);
        }
        returnBasketId = bean.getBasketId();
        //updateBasketContent(request, new SessionData().getData(request, "y")); // added for 02 Feb 2010 Release
        if (returnBasketId != null && !returnBasketId.equals("")) {
          Cookie cookie = CookieManager.createCookie(request,"basketId", returnBasketId);
          response.addCookie(cookie);
          String basketCount1 = common.getBasketCount(returnBasketId, request);
          StringBuffer returnString = null;
          returnString = new StringBuffer("");
          response.setContentType("text/html");
          response.setHeader("Cache-Control", "no-cache");
          response.getWriter().write(countOfBasket(Integer.parseInt(basketCount1), bean, request));
        }        
      }else{
        String basketCount = common.getBasketCount(cookieBasketId, request);
        if((Integer.parseInt(basketCount) + compareallIdsArray.length) <12){
          if("OTHER_COURSES".equalsIgnoreCase(compareallType)){
            inputBean.setAssociationType("O");
            bean  = addBasketContent(inputBean, request, session);
          }else{
            inputBean.setAssociationType("C");
            bean = addBasketContent(inputBean, request, session);
          }
          returnBasketId = bean.getBasketId();
          //updateBasketContent(request, new SessionData().getData(request, "y")); // added for 02 Feb 2010 Release
          if (returnBasketId != null && !returnBasketId.equals("")) {
            Cookie cookie = CookieManager.createCookie(request,"basketId", returnBasketId);
            String basketCount1 = common.getBasketCount(cookieBasketId, request);
            response.addCookie(cookie);
            StringBuffer returnString = null;
            returnString = new StringBuffer("");
            response.setContentType("text/html");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().write(countOfBasket(Integer.parseInt(basketCount1), bean, request));
          }          
        }else{
          String returnString = "";
          response.setContentType("text/html");
          response.setHeader("Cache-Control", "no-cache");
          returnString = returnString + "Oh dear, your comparison basket is full! If you want to make new comparisons, you'll have to delete a few of the old ones first...";
          returnString = returnString + "BREAKBREAK";
          response.getWriter().write(returnString);
        }
      }
    }else if(request.getParameter("dowhat") != null && request.getParameter("dowhat").equalsIgnoreCase("remove")){
      assocId = request.getParameter("compareallIds");
      if("OTHER_COURSES".equalsIgnoreCase(compareallType)){
        assocType = "O";
      }else{
        assocType = "C";
      }
      
      String basketId = request.getParameter("basketId");
      if(GenericValidator.isBlankOrNull(basketId))
      {
        basketId = cookieBasketId;
      }
      String status = common.removeBasketDetails(assocId, assocType, basketId, comparepage, request); // remove the Course from basket 
      String basketCount1 = common.getBasketCount(cookieBasketId, request);
      StringBuffer returnString = null;
      returnString = new StringBuffer("");
      response.setContentType("text/html");
      response.setHeader("Cache-Control", "no-cache");
      response.getWriter().write(countOfBasket(Integer.parseInt(basketCount1), bean, request));        
    }
    }
    
    if (request.getParameter("fromAjax") != null) {
      if (request.getParameter("dowhat") != null && request.getParameter("dowhat").equalsIgnoreCase("add")) {
        //Added By Sekhar for wu402_31012012
        inputBean.setAssociationId(assocId);
        inputBean.setAssociationType(assocType);
        if(cookieBasketId == null || cookieBasketId.equals("") || cookieBasketId.trim().length() == 0){
            inputBean.setBasketId(returnBasketId);
            bean = addBasketContent(inputBean, request,session);
            returnBasketId = bean.getBasketId();
            if (returnBasketId != null && !returnBasketId.equals("")) {
              Cookie cookie = CookieManager.createCookie(request,"basketId", returnBasketId);
              response.addCookie(cookie);
              String basketCount1 = common.getBasketCount(returnBasketId, request);
              StringBuffer returnString = null;
              returnString = new StringBuffer("");
              response.setContentType("text/html");
              response.setHeader("Cache-Control", "no-cache");
              response.getWriter().write(countOfBasket(Integer.parseInt(basketCount1), bean, request));
            }
        }else{
            String basketCount = common.getBasketCount(cookieBasketId, request);
            if(Integer.parseInt(basketCount) <12){
                inputBean.setBasketId(cookieBasketId);
                bean = addBasketContent(inputBean, request, session);
                returnBasketId = bean.getBasketId();
                //updateBasketContent(request, new SessionData().getData(request, "y")); // added for 02 Feb 2010 Release
                if (returnBasketId != null && !returnBasketId.equals("")) {
                  Cookie cookie = CookieManager.createCookie(request,"basketId", returnBasketId);
                  String basketCount1 = common.getBasketCount(cookieBasketId, request);
                  response.addCookie(cookie);
                  StringBuffer returnString = null;
                  returnString = new StringBuffer("");
                  response.setContentType("text/html");
                  response.setHeader("Cache-Control", "no-cache");
                  response.getWriter().write(countOfBasket(Integer.parseInt(basketCount1), bean, request));
                }
            }else{
                  String returnString = "";
                  response.setContentType("text/html");
                  response.setHeader("Cache-Control", "no-cache");
                  returnString = returnString + "No Options to Select more than 10 comparisions";
                  returnString = returnString + "BREAKBREAK";
                  response.getWriter().write(returnString);
            }
        }
      }
      if (request.getParameter("dowhat") != null && request.getParameter("dowhat").equalsIgnoreCase("remove")) {
        assocId = request.getParameter("assocId");
        assocType = request.getParameter("assocType");
        String basketId = request.getParameter("basketId");
        if(GenericValidator.isBlankOrNull(basketId))
        {
          basketId = cookieBasketId;
        }
        request.setAttribute("deletefrommywhatunipage", "yes");
        String status = "";
        if(!GenericValidator.isBlankOrNull(fromClearing)){
           status = common.removeClearingBasketDetails(assocId, assocType, basketId, fromClearing, request); // remove the Course from basket 
        }else{
           status = common.removeBasketDetails(assocId, assocType, basketId, comparepage, request); // remove the Course from basket 
        }
        if(GenericValidator.isBlankOrNull(request.getParameter("basketId"))){
          String basketCount1 = common.getBasketCount(cookieBasketId, request);
          StringBuffer returnString = null;
          returnString = new StringBuffer("");
          response.setContentType("text/html");
          response.setHeader("Cache-Control", "no-cache");
          response.getWriter().write(countOfBasket(Integer.parseInt(basketCount1), bean, request));        
        }else{
          String logUserId = new SessionData().getData(request, "y");
          common.getBasketDetails(request, basketId,assocType,logUserId, tabSelected);//Added for 19_MAY_2015 Release
          String basketCount = common.getBasketCount(basketId, request);
          request.setAttribute("selectedBasketId", basketId);
          request.setAttribute("sessionBasketId", cookieBasketId);
          common.getBasketPodContent(request, response);
          request.setAttribute("basketpodcollegecount", basketCount);
          return new ModelAndView("viewcompare");
        }
      }
    }
    common.getBasketPodContent(request, response);
    return null;
  }

private String countOfBasket(int basketCount, BasketBean bean, HttpServletRequest request){
  
  String returnString = "";
  new CommonFunction().getBasketPodContent(request, null);
  if(request.getSession().getAttribute("userInfoList") != null)
  {
    returnString = returnString + "<a class=\"view_more\" href=\"/degrees/comparison\">View comparison</a>";
  }else
  {
    returnString = returnString + "<a class=\"view_more\"  onclick=\"javascript:showLightBoxLoginForm('popup-newlogin', 650, 500, 'view-comparison', '', 'view-comparison');\">View comparison</a>";
  }
  returnString = returnString + "BREAKBREAK";
  returnString = returnString + "downString";
  returnString = returnString + "BREAKBREAK";
  returnString = returnString +  bean.getAlreadyAddedFlag();//ADDED FOR 11-AUG-2015_REL FOR not showing lightbox to course/uni already added to basket
  returnString = returnString + "BREAKBREAK";
  returnString = returnString + String.valueOf(basketCount);
  returnString = returnString + "BREAKBREAK";
  returnString = returnString + String.valueOf(basketCount);
  return returnString;
}

  /**
    *   This function is used to add the college/course to user basket.
    * @param association_id
    * @param association_code
    * @param cookieBasketId
    * @param request
    * @return Basketid as String.
    * @throws SQLException
    */
  private BasketBean addBasketContent(BasketBean inputBean, HttpServletRequest request, HttpSession session) throws SQLException {
    String basketId = "";
    SessionData sessionData = new SessionData();
    String sessionId = sessionData.getData(request, "x");
    String logUserId = sessionData.getData(request, "y");
    String userTrackSessionId = new SessionData().getData(request, "userTrackId");    
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    BasketBean addBean = null;
    String clientIp = request.getHeader("CLIENTIP");
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    }
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = "2";
    }
    String clientBrowser = request.getHeader("user-agent");
    // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
    String screenWidth = GenericValidator.isBlankOrNull(request.getParameter("screenwidth")) ? GlobalConstants.DEFAULT_SCREEN_WIDTH : request.getParameter("screenwidth");
    String[] latAndLong = sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG).split(GlobalConstants.SPLIT_CONSTANT);
    String latitude = (latAndLong.length > 0 && !GenericValidator.isBlankOrNull(latAndLong[0])) ? latAndLong[0].trim() : "";
    String longitude = (latAndLong.length > 1 && !GenericValidator.isBlankOrNull(latAndLong[1])) ? latAndLong[1].trim() : "";
    String userUcasScore = StringUtils.isNotBlank((String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE)) ? (String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE) : null; //Added for tariff-logging-ucas-point by Sri Sankari on wu_20200915 release
    String yearOfEntryForDSession = StringUtils.isNotBlank((String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING)) ? (String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING) : null; //Added for getting YOE value in session to log in d_session_log stats by Sujitha V on 2020_NOV_10 rel
    try {
      Vector vector = new Vector();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(logUserId);
      vector.add(inputBean.getAssociationId());
      vector.add(inputBean.getAssociationType()); //"O", "C" 
      vector.add((!GenericValidator.isBlankOrNull(inputBean.getBasketId()) && !"0".equalsIgnoreCase(inputBean.getBasketId())) ? inputBean.getBasketId() : null);
      vector.add(sessionId);
      vector.add(clientIp);
      vector.add(clientBrowser);
      vector.add(logUserId);
      String requestUrl = CommonUtil.getRequestedURL(request); //Change the getRequestURL by Prabha on 28_Jun_2016   
      vector.add(requestUrl);
      vector.add(request.getHeader("referer"));
      vector.add(cpeQualificationNetworkId);
      vector.add(inputBean.getComparePage());
      vector.add(userTrackSessionId); 
      vector.add(screenWidth); // screenwidth
      vector.add(latitude); //latitude added by sangeeth.s for March2020 rel
      vector.add(longitude); //longitude
      vector.add(userUcasScore); //P_UCAS_TARIFF
      vector.add(yearOfEntryForDSession); //P_YEAR_OF_ENTRY
      resultset = dataModel.getResultSet("WU_BASKET_PKG.pre_login_basket_fn", vector);
      while(resultset.next())
      {
        addBean = new BasketBean();
        addBean.setCollegeId(resultset.getString("college_id"));
        addBean.setCollegeName(resultset.getString("college_name"));
        addBean.setCollegeNameDisplay(resultset.getString("college_name_display"));
        addBean.setCollegeUCASCode(resultset.getString("college_ucas_code"));
        addBean.setCourseid(resultset.getString("course_id"));
        addBean.setCoursetitle(resultset.getString("course_title"));
        addBean.setCourseUCASCode(resultset.getString("course_ucas_code"));
        addBean.setBasketId(resultset.getString("basket_id"));
        addBean.setAlreadyAddedFlag(resultset.getString("status"));//ADDED FOR 11-AUG-2015_REL FOR not showing lightbox to course/uni already added to basket
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }finally
    {
      dataModel.closeCursor(resultset);
    }
    addBean.setAssociationType(inputBean.getAssociationType());
    addBean.setAssociationId(inputBean.getAssociationId());
    return addBean;
  }

}
