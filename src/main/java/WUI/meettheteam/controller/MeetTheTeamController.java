package WUI.meettheteam.controller;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;
import spring.controller.contenful.GetContentfuldata;
import java.util.ArrayList;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
  * @MeetTheTeamAction.java
  * @Version : 2.0
  * @Purpose   : This program is used to load the Meet the team page
  * *********************************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc       change                                                       Rel Ver.
  * *********************************************************************************************************************************************
  * 10-Nov-2020    vedha R                   2.1       Modified          Three new members were added and two members removed         WUNI-943
  * 08-Dec-2020    Hemalatha K               2.2       Modified          Changed middle content as cmsable
  */

@Controller
public class MeetTheTeamController{
  
  @Autowired
  GetContentfuldata getContentfuldata = null;

  @RequestMapping(value = {"/team","/team/*"}, method = RequestMethod.GET)
  public ModelAndView meetTheTeam(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
    ServletContext context = request.getServletContext();
    CommonFunction common = new CommonFunction();
    GlobalFunction global = new GlobalFunction();
    String urlString = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
    String urlArray[] = urlString.split("/");
    String teamMember = "";
    String forwardString = "meettheteam";
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { //  TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
        String fromUrl = "/friendsprofile.html";
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    common.loadUserLoggedInformation(request, context, response); // TO LOAD THE USERINFORMATION POD (LOGGED POD) //
    global.removeSessionData(session, request, response);
    if (urlArray.length > 2) {
      if (urlArray[2] != null) {
        teamMember = urlArray[2];
        ArrayList<String> memberList = getContentfuldata.getContentfulData("person", null, null);
        if (!CollectionUtils.isEmpty(memberList) && memberList.contains(teamMember)) {
          if (!"".equals(teamMember)) {
            request.setAttribute("dispTeamMember", teamMember);
            String getMetaTitle = "MEET THE TEAM - " + (teamMember.replace("-", " ")).toUpperCase();
            request.setAttribute("setParamFlag", getMetaTitle);
            forwardString = "teamMember";
          }
        } else {
          response.setStatus(301);
          response.setHeader("Location", "/team");
        }  
      }
    }
    request.setAttribute("setAboutusType", "Our people");
    return new ModelAndView(forwardString);
  }
}
