package WUI.facebook.bean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.SessionData;

/**
 * @RegistrationBean.java
 * @Version : 2.0
 * @May 30, 2007
 * @www.whatuni.com
 * @Created By : Kalikumar Modified by Balraj. Selva Kumar
 * @Purpose    : Program used to store/retrive the information in the set/get method. used to handle the data related to registration.
 * *************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * *************************************************************************************************************************
 **
*/
public class FacebookRegistrationBean {
  public FacebookRegistrationBean() {
  }
  private String filedId = "";
  private String fieldType = "";
  private String questionLabel = "";
  private String displaySeqId = "";
  private String fieldRequired = "";
  private String perRowCount = "";
  private String defaultValue = "";
  private String templateIdHint = "";
  private String rowStatus = "";
  private String maxSize = "";
  private String displaySize = "";
  private String optionId = null;
  private String optionText = null;
  private List optionValues = null;
  private String editUserImage = "";
  private String staticEmailAddress = "";
  private String selectedEmailAddress = "";
  private String selectedValues = "";
  private FormFile fileUpload = null;
  private String selectFileUpload = "browse";
  private String defaultImageUpload = "";
  private String faceBookUserId = "";
  private String forwardPage = "";
  public void reset(ActionMapping mapping, HttpServletRequest request) {
    this.defaultImageUpload = "";
  }
  public void setSelectedValue(String selectedValues) {
    this.selectedValues = selectedValues;
  }
  public String getSelectedValue() {
    return selectedValues;
  }
  public void setTheFileUpload(FormFile fileUpload) {
    this.fileUpload = fileUpload;
  }
  public FormFile getTheFileUpload() {
    return fileUpload;
  }
  public void setSelectFileUpload(String selectFileUpload) {
    this.selectFileUpload = selectFileUpload;
  }
  public String getSelectFileUpload() {
    return selectFileUpload;
  }
  public void setDefaultImageUpload(String defaultImageUpload) {
    this.defaultImageUpload = defaultImageUpload;
  }
  public String getDefaultImageUpload() {
    return defaultImageUpload;
  }
  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }
  public String getOptionId() {
    return optionId;
  }
  public void setOptionText(String optionText) {
    this.optionText = optionText;
  }
  public String getOptionText() {
    return optionText;
  }
  public void setOptionValues(List optionValues) {
    this.optionValues = optionValues;
  }
  public List getOptionValues() {
    return optionValues;
  }
  public void setFiledId(String filedId) {
    this.filedId = filedId;
  }
  public String getFiledId() {
    return filedId;
  }
  public void setFieldType(String fieldType) {
    this.fieldType = fieldType;
  }
  public String getFieldType() {
    return fieldType;
  }
  public void setQuestionLabel(String questionLabel) {
    this.questionLabel = questionLabel;
  }
  public String getQuestionLabel() {
    return questionLabel;
  }
  public void setDisplaySeqId(String displaySeqId) {
    this.displaySeqId = displaySeqId;
  }
  public String getDisplaySeqId() {
    return displaySeqId;
  }
  public void setFieldRequired(String fieldRequired) {
    this.fieldRequired = fieldRequired;
  }
  public String getFieldRequired() {
    return fieldRequired;
  }
  public void setPerRowCount(String perRowCount) {
    this.perRowCount = perRowCount;
  }
  public String getPerRowCount() {
    return perRowCount;
  }
  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }
  public String getDefaultValue() {
    return defaultValue;
  }
  public void setTemplateIdHint(String templateIdHint) {
    this.templateIdHint = templateIdHint;
  }
  public String getTemplateIdHint() {
    return templateIdHint;
  }
  public void setRowStatus(String rowStatus) {
    this.rowStatus = rowStatus;
  }
  public String getRowStatus() {
    return rowStatus;
  }
  public void setMaxSize(String maxSize) {
    this.maxSize = maxSize;
  }
  public String getMaxSize() {
    return maxSize;
  }
  public void setDisplaySize(String displaySize) {
    this.displaySize = displaySize;
  }
  public String getDisplaySize() {
    return displaySize;
  }
/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response) {
    ActionErrors errors = new ActionErrors();
    if (request.getParameter("action") != null) {
      HttpSession httpSession = request.getSession();
      if (new CommonFunction().checkSessionExpired(request, response, httpSession, "REGISTERED_USER")) {
        return errors;
      }
      List errorlist = new ArrayList();
      boolean errorFound = false;
      boolean flagpwd = false;
      boolean flagemail = false;
      FacebookRegistrationBean questionBean = null;
      HttpSession session = request.getSession(false);
      ArrayList answerlist = new ArrayList();
      String pwd = "";
      String ConfirmPwd = "";
      String email = "";
      String confirmemail = "";
      String I_am_question = "";
      boolean uploadCheckFlag = true;
      if (new SessionData().getData(request, "y") != null && new SessionData().getData(request, "y").trim().length() > 0 && !new SessionData().getData(request, "y").equals("0")) {
        if (this.getTheFileUpload() != null && String.valueOf(this.getTheFileUpload()).trim().length() > 0 || (this.getDefaultImageUpload() != null && this.getDefaultImageUpload().trim().length() > 0))
          uploadCheckFlag = true;
        else
          uploadCheckFlag = false;
      } else {
        if (fileUpload != null && !selectFileUpload.equals("") && selectFileUpload.equals("default") && defaultImageUpload.equals("")) {
          errors.add("photoempty", new ActionMessage("fileupload.photo.empty"));
        }
      }
      if (uploadCheckFlag) {
        if (fileUpload != null && !selectFileUpload.equals("") && selectFileUpload.equals("browse")) {
          String contentExt = "";
          FormFile myFile = this.getTheFileUpload();
          String contentType = myFile.getContentType();
          String fileName = myFile.getFileName();
          int fileSize = myFile.getFileSize();
          if (fileName.trim().length() == 0) {
            errors.add("photoempty", new ActionMessage("fileupload.photo.empty"));
          }
          boolean flag = false;
          if ((contentType != null) && (fileName.trim().length() > 0)) {
            StringTokenizer st = new StringTokenizer(contentType, "/");
            st.nextToken("/");
            contentExt = st.nextElement().toString();
            if (contentExt.equalsIgnoreCase("pjpeg"))
              flag = true;
            if (contentExt.equalsIgnoreCase("gif"))
              flag = true;
            if (contentExt.equalsIgnoreCase("x-png"))
              flag = true;
            if (contentExt.equalsIgnoreCase("jpeg"))
              flag = true;
            if (!flag) {
              errors.add("phototypeerror", new ActionMessage("fileupload.photo.typeerror"));
            }
          }
          if (fileSize > 1048576) {
            errors.add("photosizeerror", new ActionMessage("fileupload.photo.error"));
          }
        }
      }
       CHECK FOR ERROR IN THE REGISTRATION QUESTION 
      ArrayList questionlist = (ArrayList)session.getAttribute("DynamicQuestionList");
      try {
        if (questionlist != null) {
          Iterator questioniterator = questionlist.iterator();
          while (questioniterator.hasNext()) {
            questionBean = new FacebookRegistrationBean();
            questionBean = (FacebookRegistrationBean)questioniterator.next();
            String questionanswer = request.getParameter("reg_" + questionBean.getDisplaySeqId());
            questionanswer = questionanswer != null? questionanswer.trim(): questionanswer;
            if (questionBean.getFieldRequired() != null && questionBean.getFieldRequired().equalsIgnoreCase("Y")) {
              if (questionanswer == null || questionanswer.trim().equals("") || questionanswer.trim().equals("0")) {
                errorFound = true;
                if (questionBean.fieldType != null && (questionBean.fieldType.equalsIgnoreCase("dropdown") || questionBean.fieldType.equalsIgnoreCase("checkbox") || questionBean.fieldType.equalsIgnoreCase("radio"))) {
                  if (questionBean.filedId != null && questionBean.filedId.equalsIgnoreCase("50")) {
                    errorlist.add("Don't be shy - are you male or female?");
                  } else if (questionBean.filedId != null && questionBean.filedId.equalsIgnoreCase("345")) {
                    errorlist.add("Tell us about yourself - are you looking to go uni, already there, or as old as the hills?");
                  } else {
                    errorlist.add("Oops! Please select " + (questionBean.getQuestionLabel() != null? questionBean.getQuestionLabel().toLowerCase(): questionBean.getQuestionLabel()));
                  }
                } else {
                  //pwd
                  if (questionBean.filedId != null && questionBean.filedId.equalsIgnoreCase("2")) {
                    errorlist.add("Oops! Don't forget you'll need a password to log back in");
                  }
                  //sur or first name
                  else if (questionBean.filedId != null && (questionBean.filedId.equalsIgnoreCase("4") || questionBean.filedId.equalsIgnoreCase("5"))) {
                    errorlist.add("Sorry, we need to know your name so that uni's know who to address it to if you ask them for a prospectus");
                  }
                  // postcode
                  else if (questionBean.filedId != null && questionBean.filedId.equalsIgnoreCase("11")) {
                    errorlist.add("Please enter your home postcode so that we can help you find people from your area interested in the same unis");
                  } else {
                    errorlist.add("Oops! Please enter " + (questionBean.getQuestionLabel() != null? questionBean.getQuestionLabel().toLowerCase(): questionBean.getQuestionLabel()));
                  }
                }
              }
            }
            if (questionBean.getFiledId() != null && questionBean.getFiledId().equals("345")) {
              I_am_question = questionanswer;
            }
            if (I_am_question != null && I_am_question.equalsIgnoreCase("Future student")) {
              if (questionBean.getFiledId() != null && questionBean.getFiledId().equals("349") && (questionanswer != null && questionanswer.equals("0"))) {
                errorFound = true;
                errorlist.add("Can you tell us in which year you hope to go to uni? ");
              }
            } else {
              if (questionBean.getFiledId() != null && questionBean.getFiledId().equals("350") && (questionanswer != null && questionanswer.equals("0"))) {
                errorFound = true;
                errorlist.add("Please tell us when you will / did graduate, people need to know how recent your review experience is");
              }
            }
             Check Password MisMatch Field 
            if (!flagpwd) {
              if (questionanswer != null && !questionanswer.equals("")) {
                if (questionBean.getQuestionLabel() != null && questionBean.getQuestionLabel().equalsIgnoreCase("Password")) {
                  pwd = questionanswer;
                }
                if (questionBean.getQuestionLabel() != null && questionBean.getQuestionLabel().equalsIgnoreCase("Confirm password")) {
                  ConfirmPwd = questionanswer;
                }
                if (!pwd.equals(ConfirmPwd) && !pwd.equals("") && !ConfirmPwd.equals("")) {
                  errorlist.add("Oops! Password doesn't match confirm password");
                  flagpwd = true;
                  errorFound = true;
                }
              }
            }
             Check Email Field 
            if (questionBean.getFiledId() != null && (questionBean.getFiledId().equalsIgnoreCase("1") || questionBean.getFiledId().equalsIgnoreCase("61"))) {
              if (questionanswer != null && !questionanswer.equals("")) {
                if (!new CommonUtil().isValidEmail(questionanswer)) {
                  errorlist.add("Oops! Please enter valid " + (questionBean.getQuestionLabel() != null? questionBean.getQuestionLabel().toLowerCase(): questionBean.getQuestionLabel()));
                  errorFound = true;
                }
              }
            }
             CHECK FOR DATA OF BERTH AND ITS FORMAT 
            if (questionBean.getQuestionLabel() != null && questionBean.getQuestionLabel().equalsIgnoreCase("D.O.B")) {
              if (questionanswer != null && !questionanswer.equals("")) {
                ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
                String dateFormat = bundle.getString("wuni.format.date");
                int dateFormatLength = dateFormat.length();
                if (questionanswer.length() != dateFormatLength) {
                  errors.add("invalid_date", new ActionMessage("wnui.error.invalid_date"));
                }
              }
            }
             CHECK FOR EMAIL MISMATCH FIELD 
            if (!flagemail) {
              if (questionanswer != null && !questionanswer.equals("")) {
                if (questionBean.getFiledId() != null && questionBean.getFiledId().equalsIgnoreCase("1")) {
                  email = questionanswer;
                  selectedEmailAddress = email;
                }
                if (questionBean.getFiledId() != null && questionBean.getFiledId().equalsIgnoreCase("61")) {
                  confirmemail = questionanswer;
                }
                if (!email.equals(confirmemail) && !email.equals("") && !confirmemail.equals("")) {
                  errorlist.add("Bit of a typing problem - it seems the 2 emails you entered are different");
                  flagemail = true;
                  errorFound = true;
                }
              }
            }
            questionBean.setSelectedValue(questionanswer);
            answerlist.add(questionBean);
          }
          session.setAttribute("DynamicQuestionList", answerlist);
          if (errorFound) {
            errors.add("dymmy", new ActionMessage("wuni.error.test"));
          }
        }
      } catch (Exception registrationException) {
        registrationException.printStackTrace();
        ;
      }
      if (errorlist.size() > 0) {
        request.setAttribute("errorlist", errorlist);
      }
    }
    return errors;
  }*/
  public boolean isValidPassword(String password) {
    boolean errorFlag = false;
    errorFlag = password != null && password.trim().length() > 0? true: false;
    return errorFlag;
  }
  public void setEditUserImage(String editUserImage) {
    this.editUserImage = editUserImage;
  }
  public String getEditUserImage() {
    return editUserImage;
  }
  public void setStaticEmailAddress(String staticEmailAddress) {
    this.staticEmailAddress = staticEmailAddress;
  }
  public String getStaticEmailAddress() {
    return staticEmailAddress;
  }
  public void setSelectedEmailAddress(String selectedEmailAddress) {
    this.selectedEmailAddress = selectedEmailAddress;
  }
  public String getSelectedEmailAddress() {
    return selectedEmailAddress;
  }
  public void setFaceBookUserId(String faceBookUserId) {
    this.faceBookUserId = faceBookUserId;
  }
  public String getFaceBookUserId() {
    return faceBookUserId;
  }
  public void setForwardPage(String forwardPage) {
    this.forwardPage = forwardPage;
  }
  public String getForwardPage() {
    return forwardPage;
  }
}


//////////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////////////