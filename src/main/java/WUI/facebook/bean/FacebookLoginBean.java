package WUI.facebook.bean;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * @FaceBookLoginBean.java
 * @Version : 2.0
 * @June 30, 2008
 * @www.whatuni.com
 * @Created By : Kalikumar
 * @Purpose    : Program used to store/retrive the information in the set/get method. used to handle the data related to registration.
 * *************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * *************************************************************************************************************************
 **
*/
public class FacebookLoginBean {
  public FacebookLoginBean() {
  }
  private String facebookUserId = "";
  private String forwardPage = "";
  private String userEmail = "";
  private String userPwd = "";
  public void setFacebookUserId(String facebookUserId) {
    this.facebookUserId = facebookUserId;
  }
  public String getFacebookUserId() {
    return facebookUserId;
  }
  public void setForwardPage(String forwardPage) {
    this.forwardPage = forwardPage;
  }
  public String getForwardPage() {
    return forwardPage;
  }
  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }
  public String getUserEmail() {
    return userEmail;
  }
  public void setUserPwd(String userPwd) {
    this.userPwd = userPwd;
  }
  public String getUserPwd() {
    return userPwd;
  }
 /* public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    errors.clear();
    if (request.getParameter("buttext") != null && request.getParameter("buttext").equals("Login")) {
      if (userEmail == null || userEmail.equals("") || userEmail.trim().length() == 0) {
        errors.add("invaliduser", new ActionMessage("wuni.error.blankusername"));
      }
      if (userPwd == null || userPwd.equals("") || userPwd.trim().length() == 0) {
        errors.add("invalidpassword", new ActionMessage("wuni.error..blankpassword"));
      }
    }
    return errors;
  }*/
}
