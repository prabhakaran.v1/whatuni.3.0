package WUI.security;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;
import com.wuni.util.sql.DataModel;

/**
  * @since Feburary 2006
  * @author Balraj Selvakumar
  *    This file calls the security PL/SQL block and check for the valid Session ID,
  *       if so then it will return the Session-ID, User ID and flag (true/false).
  *       if not so then it will create a new SessionID and return the Session-ID,
  *       User ID and flag (true/false).
  *       The flag (true/false) is used to decide whether the conctol can redirect to request page or not.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class SecurityEvaluator {

  /**
    *   This function is used to check the user stats (ANONYMOUS/REGISTRED_USER).
    * @param pageId
    * @param request
    * @return Status of user status.
    * @throws SQLException
    */
  public boolean checkSecurity(String pageId, HttpServletRequest request, HttpServletResponse response) throws SQLException {
    HttpSession session = request.getSession();
    if (pageId != null && pageId.equals("ANONYMOUS") && new CommonFunction().checkSessionExpiry(request, session)) {
      new SessionData().addData(request, response, "y", "0");
    }
    int pageno = 0;
    boolean status = false;
    ResultSet resultset = null;
    DataModel dataModel = new DataModel();
    try {
      String collegeId = new SessionData().getData(request, "w");
      collegeId = collegeId != null ? collegeId.trim() : collegeId;
      String sessionId = new SessionData().getData(request, "x");
      String userId = new SessionData().getData(request, "y");
      if (collegeId == null || collegeId.equals("null") || collegeId.equals("")) {
        collegeId = "";
      }
      if (sessionId == null || sessionId.equals("null") || sessionId.equals("")) {
        sessionId = "";
      }
      if (userId == null || userId.equals("null") || userId.equals("")) {
        userId = "";
      }
      /*Vector checkValues = new Vector();
      checkValues.addElement(pageId); // page id
      checkValues.addElement(collegeId); // college id
      checkValues.addElement(sessionId); // session id
      checkValues.addElement(userId); // user  user-id
      checkValues.addElement(GlobalConstants.WHATUNI_AFFILATE_ID); // affliate id
      resultset = dataModel.getResultSet("authenticate_fn", checkValues);
      while (resultset.next()) {
        pageno = resultset.getInt(1);
        sessionId = resultset.getString(2);
        userId = resultset.getString(3);
      }*/
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);    
      RegistrationBean registrationBean = new RegistrationBean();       
      registrationBean.setPageId(pageId);
      registrationBean.setCollegeId(collegeId);
      registrationBean.setSessionId(sessionId);
      registrationBean.setUserId(userId);
      registrationBean.setAffilateId(GlobalConstants.WHATUNI_AFFILATE_ID);
      Map checkUserAuthMap = commonBusiness.checkUserAuthentication(registrationBean);
      if (checkUserAuthMap != null && !checkUserAuthMap.isEmpty()) {
        ArrayList userAuthList = (ArrayList)checkUserAuthMap.get("User_Auth");
        if(userAuthList != null && userAuthList.size() > 0){
          Iterator userAuthItr = userAuthList.iterator();
          while (userAuthItr.hasNext()){
            RegistrationBean registrationVO = (RegistrationBean)userAuthItr.next();
            if(!GenericValidator.isBlankOrNull(registrationVO.getPageId())){              
              pageno = Integer.parseInt(registrationVO.getPageId());
            }            
            sessionId = registrationVO.getSessionId();
            userId = registrationVO.getUserId();
          }
        }
      }
      new SessionData().addData(request, response, "x", sessionId);
      if (userId == null || userId.equals("null")) {
        new SessionData().addData(request, response, "y", "");
      } else {
        new SessionData().addData(request, response, "y", userId);
      }
      if (pageno == 0) {
        status = true;
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return status;
  }

  public void overrideJSessionId(HttpServletRequest request, HttpServletResponse response) {
    // Below code added 30th November 2010 release 
    Cookie cook[] = ((HttpServletRequest)request).getCookies();
    String dname = request.getServerName();
    String dname_prefix = dname != null? dname.substring(dname.indexOf("."), dname.length()): "";
    if (cook != null) {
      for (int i = 0; i < cook.length; i++) {
        if (cook[i].getName().equals("JSESSIONID")) {
          Cookie scookieDummy = new Cookie("JSESSIONID", cook[i].getValue());
          scookieDummy.setDomain(dname_prefix); //Specifies the domain within which this cookie should be presented.        
          scookieDummy.setPath("/");
          scookieDummy.setSecure(true);
          ((HttpServletResponse)response).addCookie(scookieDummy);
        }
      }
    }
    // end of code added 30th November 2010 release 
  }

}
