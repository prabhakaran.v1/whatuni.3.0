package WUI.openday.form;

import org.apache.struts.action.ActionForm;

public class OpenDaysCalendarBean {
  //
  private String openDate = "";
  private String openDate_day = "";
  private String openDate_month = "";
  private String openDate_year = "";
  //
  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = ""; 
  private String collegeLocation = "";
  private String collegeDepartmentDescription = "";
  private String collegeOpendayExternalUrl = "";
  //wu_320 changes done by Sekhar K
  private String openDay_day = "";
  private String openDay_date = "";
  private String openDay_month_year = "";
    private String subOrderItemId = "";
    private String subOrderWebsite = "";
    private String subOrderEmail = "";
    private String subOrderEmailWebform = "";
    private String subOrderProspectus = "";
    private String subOrderProspectusWebform = "";
    private String subOrderProspectusTargetURL = "";
    private String profileType = "";
    private String myhcProfileId = "";
    private String cpeQualificationNetworkId = "";
    private String mediaId = "";
    private String mediaType = "";
    private String mediaPath = "";
    private String mediaThumbPath = "";
    private String totalRecordsCount = ""; 
    private String dateExistFlg = null;
    private String personalNotes = null;
    private String pastFutureDate = null;
    private String emailURLString = null;
    private String eventId = null;

  public void setOpenDate(String openDate) {
    this.openDate = openDate;
  }

  public String getOpenDate() {
    return openDate;
  }

  public void setOpenDate_day(String openDate_day) {
    this.openDate_day = openDate_day;
  }

  public String getOpenDate_day() {
    return openDate_day;
  }

  public void setOpenDate_month(String openDate_month) {
    this.openDate_month = openDate_month;
  }

  public String getOpenDate_month() {
    return openDate_month;
  }

  public void setOpenDate_year(String openDate_year) {
    this.openDate_year = openDate_year;
  }

  public String getOpenDate_year() {
    return openDate_year;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setCollegeLocation(String collegeLocation) {
    this.collegeLocation = collegeLocation;
  }

  public String getCollegeLocation() {
    return collegeLocation;
  }

  public void setCollegeDepartmentDescription(String collegeDepartmentDescription) {
    this.collegeDepartmentDescription = collegeDepartmentDescription;
  }

  public String getCollegeDepartmentDescription() {
    return collegeDepartmentDescription;
  }

  public void setCollegeOpendayExternalUrl(String collegeOpendayExternalUrl) {
    this.collegeOpendayExternalUrl = collegeOpendayExternalUrl;
  }

  public String getCollegeOpendayExternalUrl() {
    return collegeOpendayExternalUrl;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }


    public void setOpenDay_day(String openDay_day) {
        this.openDay_day = openDay_day;
}

    public String getOpenDay_day() {
        return openDay_day;
    }

    public void setOpenDay_date(String openDay_date) {
        this.openDay_date = openDay_date;
    }

    public String getOpenDay_date() {
        return openDay_date;
    }

    public void setOpenDay_month_year(String openDay_month_year) {
        this.openDay_month_year = openDay_month_year;
    }

    public String getOpenDay_month_year() {
        return openDay_month_year;
    }

    public void setSubOrderItemId(String subOrderItemId) {
        this.subOrderItemId = subOrderItemId;
    }

    public String getSubOrderItemId() {
        return subOrderItemId;
    }

    public void setSubOrderWebsite(String subOrderWebsite) {
        this.subOrderWebsite = subOrderWebsite;
    }

    public String getSubOrderWebsite() {
        return subOrderWebsite;
    }

    public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
        this.cpeQualificationNetworkId = cpeQualificationNetworkId;
    }

    public String getCpeQualificationNetworkId() {
        return cpeQualificationNetworkId;
    }

    public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
        this.subOrderProspectusWebform = subOrderProspectusWebform;
    }

    public String getSubOrderProspectusWebform() {
        return subOrderProspectusWebform;
    }

    public void setSubOrderEmail(String subOrderEmail) {
        this.subOrderEmail = subOrderEmail;
    }

    public String getSubOrderEmail() {
        return subOrderEmail;
    }

    public void setSubOrderEmailWebform(String subOrderEmailWebform) {
        this.subOrderEmailWebform = subOrderEmailWebform;
    }

    public String getSubOrderEmailWebform() {
        return subOrderEmailWebform;
    }

    public void setSubOrderProspectus(String subOrderProspectus) {
        this.subOrderProspectus = subOrderProspectus;
    }

    public String getSubOrderProspectus() {
        return subOrderProspectus;
    }

    public void setSubOrderProspectusTargetURL(String subOrderProspectusTargetURL) {
        this.subOrderProspectusTargetURL = subOrderProspectusTargetURL;
    }

    public String getSubOrderProspectusTargetURL() {
        return subOrderProspectusTargetURL;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setMyhcProfileId(String myhcProfileId) {
        this.myhcProfileId = myhcProfileId;
    }

    public String getMyhcProfileId() {
        return myhcProfileId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setMediaThumbPath(String mediaThumbPath) {
        this.mediaThumbPath = mediaThumbPath;
    }

    public String getMediaThumbPath() {
        return mediaThumbPath;
    }

    public void setTotalRecordsCount(String totalRecordsCount) {
        this.totalRecordsCount = totalRecordsCount;
    }

    public String getTotalRecordsCount() {
        return totalRecordsCount;
    }
    
  public void setDateExistFlg(String dateExistFlg) {
    this.dateExistFlg = dateExistFlg;
  }
  
  public String getDateExistFlg() {
    return dateExistFlg;
  }

  public void setPersonalNotes(String personalNotes)
  {
    this.personalNotes = personalNotes;
  }

  public String getPersonalNotes()
  {
    return personalNotes;
  }

  public void setPastFutureDate(String pastFutureDate)
  {
    this.pastFutureDate = pastFutureDate;
  }

  public String getPastFutureDate()
  {
    return pastFutureDate;
  }

  public void setEmailURLString(String emailURLString)
  {
    this.emailURLString = emailURLString;
  }

  public String getEmailURLString()
  {
    return emailURLString;
  }


  public void setEventId(String eventId)
  {
    this.eventId = eventId;
  }

  public String getEventId()
  {
    return eventId;
  }

}
