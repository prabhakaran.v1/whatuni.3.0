package WUI.ucas.bean;

import org.apache.struts.action.ActionForm;

public class UcasCourseBean {
  public UcasCourseBean() {
  }
  
  String collegeId = "";
  String courseId = "";
  String ucasCode = "";
  String qualificationCode = "";
  String qualificationDesc = "";
  String studyMode = "";
  String ucasCodeLink = "";
  String totalUcasCodes = "";  

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setUcasCode(String ucasCode) {
    this.ucasCode = ucasCode;
  }

  public String getUcasCode() {
    return ucasCode;
  }

  public void setQualificationCode(String qualificationCode) {
    this.qualificationCode = qualificationCode;
  }

  public String getQualificationCode() {
    return qualificationCode;
  }

  public void setQualificationDesc(String qualificationDesc) {
    this.qualificationDesc = qualificationDesc;
  }

  public String getQualificationDesc() {
    return qualificationDesc;
  }

  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }

  public String getStudyMode() {
    return studyMode;
  }

  public void setTotalUcasCodes(String totalUcasCodes) {
    this.totalUcasCodes = totalUcasCodes;
  }

  public String getTotalUcasCodes() {
    return totalUcasCodes;
  }

  public void setUcasCodeLink(String ucasCodeLink) {
    this.ucasCodeLink = ucasCodeLink;
  }

  public String getUcasCodeLink() {
    return ucasCodeLink;
  }

}
