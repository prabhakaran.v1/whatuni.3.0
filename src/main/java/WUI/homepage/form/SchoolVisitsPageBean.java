package WUI.homepage.form;

import org.apache.struts.action.ActionForm;

public class SchoolVisitsPageBean {

    public SchoolVisitsPageBean() {
    }
    private String firstName = "";
    private String lastName = "";
    private String emailAddress = "";
    private String Phone = "";
    private String jobTitle = "";
    private String schoolName = ""; 
    private String town = "";
    private String date = "";
    private String time = "";
    private String aboutus = "";
    private String otherTxt = "";
    private String message = "";
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setPhone(String phone) {
        this.Phone = phone;
    }

    public String getPhone() {
        return Phone;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getTown() {
        return town;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setAboutus(String aboutus) {
        this.aboutus = aboutus;
    }

    public String getAboutus() {
        return aboutus;
    }

    public void setOtherTxt(String otherTxt) {
        this.otherTxt = otherTxt;
    }

    public String getOtherTxt() {
        return otherTxt;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
