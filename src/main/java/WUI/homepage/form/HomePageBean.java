package WUI.homepage.form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class HomePageBean {

  public HomePageBean() {
  }
  private String rowNumner = "";
  private String collegeId = "";
  private String courseId = "";
  private String subjectId = "";
  private String collegeName = "";
  private String collegeNameDisplay = ""; 
  private String shortCollegeName = "";
  private String reviewId = "";
  private String reviewTitle = "";
  private String courseTitle = "";
  private String userId = "";
  private String userName = "";
  private String userGender = "";
  private String gender = "";
  private String userGraduate = "";
  private String courseName = "";
  private String nationality = "";
  private String userImage = "";
  private String userImageLarge = "";
  private String overAllRating = "";
  private String overallRating = "";
  private String studCurrentStatus = "";
  private String overallRatingComment = "";
  private String seoStudyLevelText = "";
  private String courseDeletedFlag = "";
  private String questionTitle = "";
  private String rating = "";
  private String seoQuestionText = "";
  private String questionId = "";
  private String seoQuesitonText = "";
  private String displayTilte = "";
  private String sponsorBy = "";
  private String displaySeq = "";
  private String sponsorURL = "";
  private String collegeLogo = "";//26_AUG_2014
  private String positionId = "";
  private String basketID = "";//16_SEP_2014
  private String userType = "";
  private String friendsId = "";  
  private String newUniReviewURL = "";  
  private String trackSessionId = "";
  //private String clearingUserType = "";

  public String getTrackSessionId() {
    return trackSessionId;
  }

  public void setTrackSessionId(String trackSessionId) {
    this.trackSessionId = trackSessionId;
  }

  public void setRowNumner(String rowNumner) {
    this.rowNumner = rowNumner;
  }

  public String getRowNumner() {
    return rowNumner;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setShortCollegeName(String shortCollegeName) {
    this.shortCollegeName = shortCollegeName;
  }

  public String getShortCollegeName() {
    return shortCollegeName;
  }

  public void setReviewId(String reviewId) {
    this.reviewId = reviewId;
  }

  public String getReviewId() {
    return reviewId;
  }

  public void setReviewTitle(String reviewTitle) {
    this.reviewTitle = reviewTitle;
  }

  public String getReviewTitle() {
    return reviewTitle;
  }

  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }

  public String getCourseTitle() {
    return courseTitle;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserGender(String userGender) {
    this.userGender = userGender;
  }

  public String getUserGender() {
    return userGender;
  }

  public void setUserGraduate(String userGraduate) {
    this.userGraduate = userGraduate;
  }

  public String getUserGraduate() {
    return userGraduate;
  }

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }

  public String getCourseName() {
    return courseName;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getNationality() {
    return nationality;
  }

  public void setUserImage(String userImage) {
    this.userImage = userImage;
  }

  public String getUserImage() {
    return userImage;
  }

  public void setOverAllRating(String overAllRating) {
    this.overAllRating = overAllRating;
  }

  public String getOverAllRating() {
    return overAllRating;
  }

  public void setUserImageLarge(String userImageLarge) {
    this.userImageLarge = userImageLarge;
  }

  public String getUserImageLarge() {
    return userImageLarge;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return gender;
  }

  public void setStudCurrentStatus(String studCurrentStatus) {
    this.studCurrentStatus = studCurrentStatus;
  }

  public String getStudCurrentStatus() {
    return studCurrentStatus;
  }

  public void setOverallRatingComment(String overallRatingComment) {
    this.overallRatingComment = overallRatingComment;
  }

  public String getOverallRatingComment() {
    return overallRatingComment;
  }

  public void setOverallRating(String overallRating) {
    this.overallRating = overallRating;
  }

  public String getOverallRating() {
    return overallRating;
  }

  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public String getSubjectId() {
    return subjectId;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

/*  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    return errors;
  }*/

  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }

  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }

  public void setCourseDeletedFlag(String courseDeletedFlag) {
    this.courseDeletedFlag = courseDeletedFlag;
  }

  public String getCourseDeletedFlag() {
    return courseDeletedFlag;
  }

  public void setQuestionTitle(String questionTitle) {
    this.questionTitle = questionTitle;
  }

  public String getQuestionTitle() {
    return questionTitle;
  }

  public void setRating(String rating) {
    this.rating = rating;
  }

  public String getRating() {
    return rating;
  }

  public void setSeoQuestionText(String seoQuestionText) {
    this.seoQuestionText = seoQuestionText;
  }

  public String getSeoQuestionText() {
    return seoQuestionText;
  }

  public void setQuestionId(String questionId) {
    this.questionId = questionId;
  }

  public String getQuestionId() {
    return questionId;
  }

  public void setSeoQuesitonText(String seoQuesitonText) {
    this.seoQuesitonText = seoQuesitonText;
  }

  public String getSeoQuesitonText() {
    return seoQuesitonText;
  }

  public void setDisplayTilte(String displayTilte) {
    this.displayTilte = displayTilte;
  }

  public String getDisplayTilte() {
    return displayTilte;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setSponsorBy(String sponsorBy)
  {
    this.sponsorBy = sponsorBy;
  }

  public String getSponsorBy()
  {
    return sponsorBy;
  }

  public void setDisplaySeq(String displaySeq)
  {
    this.displaySeq = displaySeq;
  }

  public String getDisplaySeq()
  {
    return displaySeq;
  }
  public void setSponsorURL(String sponsorURL) {
    this.sponsorURL = sponsorURL;
  }
  public String getSponsorURL() {
    return sponsorURL;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
    return collegeLogo;
  }
  public void setPositionId(String positionId) {
    this.positionId = positionId;
  }
  public String getPositionId() {
    return positionId;
  }
  public void setBasketID(String basketID) {
    this.basketID = basketID;
  }
  public String getBasketID() {
    return basketID;
  }
  public void setUserType(String userType) {
    this.userType = userType;
  }
  public String getUserType() {
    return userType;
  }
  public void setFriendsId(String friendsId) {
    this.friendsId = friendsId;
  }
  public String getFriendsId() {
    return friendsId;
  }
  public void setNewUniReviewURL(String newUniReviewURL) {
    this.newUniReviewURL = newUniReviewURL;
  }
  public String getNewUniReviewURL() {
    return newUniReviewURL;
  }

 /* public String getClearingUserType() {
	return clearingUserType;
  }
  public void setClearingUserType(String clearingUserType) {
	this.clearingUserType = clearingUserType;
  }*/
  
}
