package WUI.homepage.controller;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import java.sql.SQLException;
import javax.servlet.http.*;
import javax.servlet.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @AboutusAction.java
 * @Version : 2.0
 * @Feburary 03 2007
 * @www.whatuni.com
 * @Created By : Balraj. Selva Kumar
 * @Purpose  :This program is used to display the informaiton of aboutus content.
 * *************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * *************************************************************************************************************************
 * 08-Dec-2020    Hemalatha.K               2.1      Changed middle content as CMS-able
 */

@Controller
public class AboutusController {
	
  @RequestMapping(value = "/about-us", method = RequestMethod.GET)
  public ModelAndView aboutUs(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws ServletException,java.io.IOException,SQLException {
    ServletContext context = request.getServletContext();
    CommonFunction common = new CommonFunction();
    SessionData sessiondata = new SessionData();
    String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
    String canonicalUrl = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + URL_STRING + GlobalConstants.SLASH;  //Added by Sangeeth.S for 25_Sep_18 rel
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { //  TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    sessiondata.setParameterValuestoSessionData(request, response);
    new GlobalFunction().removeSessionData(session, request, response);
    try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
        String fromUrl = "/home.html";
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    common.loadUserLoggedInformation(request, context, response);
    request.setAttribute("setAboutusType","Overview");
    request.setAttribute("canonicalUrl", canonicalUrl);
    return new ModelAndView("overviewpage");
  }
}
