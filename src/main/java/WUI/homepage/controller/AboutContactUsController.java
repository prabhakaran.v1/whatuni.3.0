package WUI.homepage.controller;

import WUI.homepage.form.AboutUsPagesBean;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import WUI.utilities.GlobalConstants;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.valueobjects.AboutUsPagesVO;

import java.util.Map;

import javax.servlet.http.*;
import javax.servlet.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @AboutContactUsAction.java
 * @Version : 1.0
 * @Oct 08 2014
 * @www.whatuni.com
 * @Created By : Thiyagu G.
 * @Purpose  :This program is used to display the informaiton of aboutus - order and download content.
 * *************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * *************************************************************************************************************************
 * 08-Oct-2014    Thiyagu G                 1.0      Initial draft                                                 1.0
 * 23-Jan-2019    Sangeeth.S                1.1      passed the lat,long values for the map                        585
 */
@Controller
public class AboutContactUsController {
	
  @RequestMapping(value = "/about-us/contact-us", method = {RequestMethod.GET,RequestMethod.POST})
  public ModelAndView aboutContactUs(@ModelAttribute("aboutusBean")AboutUsPagesBean aboutusBean, HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
    ServletContext context = request.getServletContext();
    CommonFunction common = new CommonFunction();
    SessionData sessiondata = new SessionData();
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { //  TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    sessiondata.setParameterValuestoSessionData(request, response);
    new GlobalFunction().removeSessionData(session, request, response);
    try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
        String fromUrl = "/home.html";
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    common.loadUserLoggedInformation(request, context, response);
    AboutUsPagesVO aboutUsPagesVO = new AboutUsPagesVO();
    String submitType = request.getParameter("submitType");
    String forwardString = null;
    if (submitType != null && submitType.equalsIgnoreCase("sendcontactinfo")) {
      aboutUsPagesVO.setFirstName(aboutusBean.getFirstName());
      aboutUsPagesVO.setLastName(aboutusBean.getLastName());
      aboutUsPagesVO.setEmailAddress(aboutusBean.getEmailAddress());
      aboutUsPagesVO.setMessage(aboutusBean.getMessage());
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      Map schoolvistsmap = commonBusiness.sendAboutUsEmails(aboutUsPagesVO, "contactus");
      forwardString = "contactussuccess";
    } else {
 //Passed lat and long values for Whatuni location in map by Sangeeth.S for Feb_12_19 rel
        request.setAttribute("latitudeStr", GlobalConstants.WU_LAT);
        request.setAttribute("longitudeStr", GlobalConstants.WU_LONG);
        //
      forwardString = "contactuspage";
    }
    request.setAttribute("setAboutusType","Contact us");
    return new ModelAndView(forwardString);
  }
}
