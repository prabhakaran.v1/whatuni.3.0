package WUI.homepage.controller;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import javax.servlet.ServletContext;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.*;

/**
 * @CookiesAction.java
 * @Version : 1.0
 * @May 22 2012
 * @www.whatuni.com
 * @Created By : Sekhar Kasala
 * @Purpose  :This program is used to display the informaiton of cookies.
 * *************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * *************************************************************************************************************************
 */
@Controller
public class CookiesController{
	
  @RequestMapping(value = "/cookies", method = RequestMethod.GET)
  public ModelAndView cookies(HttpServletRequest request, HttpServletResponse response) throws Exception {
    HttpSession session = request.getSession();
    ServletContext context = request.getServletContext();
    CommonFunction common = new CommonFunction();
    SessionData sessiondata = new SessionData();
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { //  TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    sessiondata.setParameterValuestoSessionData(request, response);
    new GlobalFunction().removeSessionData(session, request, response);
    try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
        String fromUrl = "/home.html";
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    common.loadUserLoggedInformation(request, context, response);
    String htmlContent = new CommonFunction().getSHTMLContent("WU_COOKIE_POLICY_220703");
    if(!GenericValidator.isBlankOrNull(htmlContent)){
      request.setAttribute("cookieText", htmlContent);
    }
    request.setAttribute("insightPageFlag", "yes"); //28_OCT_2014 Added by Amir for insight
    return new ModelAndView("cookiespage");
  }
}
