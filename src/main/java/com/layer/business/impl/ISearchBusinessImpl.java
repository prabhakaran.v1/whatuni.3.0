package com.layer.business.impl;

import java.util.List;
import java.util.Map;

import mobile.form.MobileUniBrowseBean;
import mobile.valueobject.SearchVO;
import spring.form.OpenDaysBean;
import spring.valueobject.seo.BreadcrumbVO;
import WUI.registration.bean.RegistrationBean;
import WUI.review.bean.UniBrowseBean;
import WUI.search.form.SearchBean;

import com.layer.business.ISearchBusiness;
import com.layer.dao.ISearchDAO;
import com.wuni.util.valueobject.interstitialsearch.InterstitialCourseCountVO;
import com.wuni.util.valueobject.interstitialsearch.InterstitialSearchVO;
import com.wuni.valueobjects.ModuleVO;
import com.wuni.valueobjects.topnavsearch.TopNavSearchVO;

/**
 * contains business logic for SR, PR, CD
 *
 * @since        wu318_20111020 - redesign
 * @author       Mohamed Syed
 *
 */
public class ISearchBusinessImpl implements ISearchBusiness {

  private ISearchDAO searchDAO = null;

  public void setSearchDAO(ISearchDAO searchDAO) {
    this.searchDAO = searchDAO;
  }

  public ISearchDAO getSearchDAO() {
    return this.searchDAO;
  }

  /**
   * will return course-details page info
   * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-details/[COURSE_TITLE]-courses-details/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
   *
   *
   * @since        wu318_20111020 - redesign
   * @author       Mohamed Syed
   *
   * @param inputList
   * @return
   *
   */
  public Map getCourseDetailsInfo(List inputList) {
    Map resultmap = searchDAO.getCourseDetailsInfo(inputList);
    return resultmap;
  }

  /**
   * will return course-specific-money-page details
   * URL_PATTERN: /courses/[STUDY_LEVEL_DESC]-courses/[KEYWORD]-[STUDY_LEVEL_DESC]-courses-[LOCATION1]/[KEYWORD]/[STUDY_LEVEL_ID]/[LOCATION2]/[LOCATION2]/[SEARCH_RANGE]/[COUNTY_ID]/[STUDY_MODE_ID]/[CATEGORY_CODE]/[ORDER_BY]/[COLLEGE_ID]/[PAGE_NO]/[EXTRA_TEXT]/[UC/U/C]/page.html
   *
   * @since        wu318_20111020 - redesign
   * @author       Mohamed Syed
   *
   * @param inputList
   * @return
   *
   */
  public Map getCourseSpecificMoneyPage(List inputList) {
    Map resultmap = searchDAO.getCourseSpecificMoneyPage(inputList);
    return resultmap;
  }

  /**
   * will return uni-specific-money-page details
   * URL_PATTERN: /courses/[STUDY_LEVEL_DESC]-courses/[KEYWORD]-[STUDY_LEVEL_DESC]-courses-[LOCATION1]/[KEYWORD]/[STUDY_LEVEL_ID]/[LOCATION2]/[LOCATION2]/[SEARCH_RANGE]/[COUNTY_ID]/[STUDY_MODE_ID]/[CATEGORY_CODE]/[ORDER_BY]/[COLLEGE_ID]/[PAGE_NO]/[EXTRA_TEXT]/[UC/U/C]/uniview.html
   *
   * @since        wu318_20111020 - redesign
   * @author       Mohamed Syed
   *
   * @param inputList
   * @return
   *
   */
  public Map getUniSpecificMoneyPage(List inputList) {
    Map resultmap = searchDAO.getUniSpecificMoneyPage(inputList);
    return resultmap;
  }
  //
  public Map getUniversityBrowseList(UniBrowseBean uniBrowseBean) {
    Map resultmap = searchDAO.getUniversityBrowseList(uniBrowseBean);
    return resultmap;
  }
  
  public Map getMobileUniversityBrowseList(MobileUniBrowseBean uniBrowseBean) {
    Map resultmap = searchDAO.getMobileUniversityBrowseList(uniBrowseBean);
    return resultmap;
  }  
  
  //
  public Map newUserRegistration(RegistrationBean registrationBean, Object[][] attrValues) {
    Map resultmap = searchDAO.newUserRegistration(registrationBean, attrValues);
    return resultmap;
  }
  
  public Map getPredictedGrades(SearchBean searchBean) {
    Map resultmap = searchDAO.getPredictedGrades(searchBean);
    return resultmap;
  }
  
  public Map getCourseDetailsKISAjax(List inputList){
    Map resultmap = searchDAO.getCourseDetailsKISAjax(inputList);
    return resultmap;    
  }
  
  public Map addMyOpendays(OpenDaysBean openDaysBean) {
    Map resultmap = searchDAO.addMyOpendays(openDaysBean);
    return resultmap;
  }
  
  public Map addOpenDayForReservePlacesAction(OpenDaysBean openDaysBean) {
    Map resultmap = searchDAO.addOpenDayForReservePlacesAction(openDaysBean);
    return resultmap;
  }
 
  public Map getMobileCourseSpecificMoneyPage(SearchVO searchVO){//Mobile_Release
    Map resultmap = searchDAO.getMobileCourseSpecificMoneyPage(searchVO);
    return resultmap;
   }
  //
  public Map getMobileRefineResults(SearchVO searchVO){//Mobile_Release
    Map resultmap = searchDAO.getMobileRefineResults(searchVO);
    return resultmap;
  }
  //
  public Map getMobileProviderResultPageContent(SearchVO searchVO){
    Map resultmap = searchDAO.getMobileProviderResultPageContent(searchVO);
    return resultmap;
  }
 //
  public Map getModuleStageDetails(ModuleVO moduleVO){//11_Feb_2014_REL
    Map resultmap = searchDAO.getModuleStageDetails(moduleVO);
    return resultmap;
  }
  //
  public Map getModuleStageDetailDesc(ModuleVO moduleVO){//11_Feb_2014_REL
    Map  resultMap = searchDAO.getModuleStageDetailDesc(moduleVO);
    return resultMap;
  }
  //
  public Map getSearchResults(SearchVO searchVO){//Search_Redesign
    Map resultmap = searchDAO.getSearchResults(searchVO);
    return resultmap;
  }
  //
  public Map getModuleDetailsSearch(String moduleGroupId){//Search_Redesign
     Map resultmap = searchDAO.getModuleDetailsSearch(moduleGroupId);
     return resultmap;
  }
  //
  public Map getArticleData(String postId){//Search_Redesign
   Map resultmap = searchDAO.getArticleData(postId);
   return resultmap;
  }  
  /**
    * will return user details for survey
    * @param registrationBean
    * @return Map
    * //added by Priyaa For 21_JUL_2015_REL 
    */
  public Map surveyUserAutoLogin(RegistrationBean registrationBean) {//Modified by Priyaa For 21_JUL_2015_REL 
    Map resultmap = searchDAO.surveyUserAutoLogin(registrationBean);
    return resultmap;
  }
  
  public Map userRegistrationLightbox() {
    Map resultmap = searchDAO.userRegistrationLightbox();
    return resultmap;
  }
  public Map getBreadcrumbData(BreadcrumbVO breadcrumbVO){//Get breadcrumb details by Prabha For 21_MAR_2017_REL 
    Map resultmap = searchDAO.getBreadcrumbData(breadcrumbVO);
    return resultmap;
  }
  //
  public Map getAdvanceSearchResults(SearchVO searchVO){ //Added for Advance search page by Prabha on 29_Dec_2017_REL
    Map resultmap = searchDAO.getAdvanceSearchResults(searchVO);
    return resultmap;
  }
  //

  public Map getInterstitialSearchFilters(InterstitialSearchVO interstitialSearchVO) { //Added for loading Advance search page by Sabapathi on 28_Aug_2018_REL
    Map resultmap = searchDAO.getInterstitialSearchFilters(interstitialSearchVO);
    return resultmap;
  }
  //
  
   public Map getInterstitialCourseCount(InterstitialCourseCountVO interstitialCourseCountVO) { //Added for Getting course count for Advance search page by Sabapathi on 28_Aug_2018_REL
     Map resultmap = searchDAO.getInterstitialCourseCount(interstitialCourseCountVO);
     return resultmap;
   }
   //
   
   public Map getTopNavSearchFilters(TopNavSearchVO topNavSearchVO){
     Map resultmap = searchDAO.getTopNavSearchFilters(topNavSearchVO);
     return resultmap;
   }
}
