package com.layer.business.impl;

import java.util.Map;

import WUI.homepage.form.HomePageBean;

import com.layer.business.IHomeBusiness;
import com.layer.dao.IHomeDAO;

/**
 * IHomeBusinessImpl.
 *
 * @author:     Mohamed Syed
 * @version:    1.0
 * @since:      wu314_20110712
 *
 **/
public class IHomeBusinessImpl implements IHomeBusiness {
  private IHomeDAO homeDAO = null;
  public void setHomeDAO(IHomeDAO homeDAO) {
    this.homeDAO = homeDAO;
  }
  public IHomeDAO getHomeDAO() {
    return homeDAO;
  }
  public Map getHomePageData() {
    Map resultmap = homeDAO.getHomePageData();
    return resultmap;
  }
  //
  public Map getNewHomePageData(HomePageBean homepagebean) { //16_SEP_2014
    Map resultmap = homeDAO.getNewHomePageData(homepagebean);
    return resultmap;
  }
  public Map getUserTimeLinePod(String userId) { //16_SEP_2014
    Map userTimeLinePodMap = homeDAO.getUserTimeLinePod(userId);
    return userTimeLinePodMap;
  }
}
