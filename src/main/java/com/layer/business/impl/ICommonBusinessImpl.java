package com.layer.business.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mobile.valueobject.ProfileVO;
import mobile.valueobject.SearchVO;
import spring.dao.util.valueobject.clearing.ClearingProfileVO;
import spring.gradefilter.bean.SRGradeFilterBean;
import spring.valueobject.articles.RssBlogArticleVO;
import WUI.basket.form.BasketBean;
import WUI.registration.bean.RegistrationBean;
import WUI.registration.bean.UserLoginBean;
import WUI.review.bean.ReviewListBean;
import WUI.search.form.CourseDetailsBean;
import WUI.search.form.SearchBean;
import WUI.whatunigo.bean.GradeFilterBean;

import com.layer.business.ICommonBusiness;
import com.layer.dao.ICommonDAO;
import com.wuni.advertiser.ql.form.QLFormBean;
import com.wuni.myfinalchoice.form.MyFinalChoiceBean;
import com.wuni.mywhatuni.MywhatuniInputBean;
import com.wuni.mywhatuni.form.MyCompareBean;
import com.wuni.scholarship.form.ScholarshipFormBean;
import com.wuni.ultimatesearch.SaveULInputbean;
import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;
import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;
import com.wuni.util.form.opendays.OpenDaysBean;
import com.wuni.util.form.studentawards.StudentAwardsBean;
import com.wuni.util.valueobject.AutoCompleteVO;
import com.wuni.util.valueobject.VersionChangesVO;
import com.wuni.util.valueobject.advert.NewUniLandingVO;
import com.wuni.util.valueobject.advert.ProspectusVO;
import com.wuni.util.valueobject.advert.RichProfileVO;
import com.wuni.util.valueobject.contenthub.ContentHubVO;
import com.wuni.util.valueobject.contenthub.OpendayVO;
import com.wuni.util.valueobject.coursesearch.CourseSearchVO;
import com.wuni.util.valueobject.interstitialsearch.SubjectAjaxVO;
import com.wuni.util.valueobject.mywhatuni.MyWhatuniInputVO;
import com.wuni.util.valueobject.openday.OpendaySearchVO;
import com.wuni.util.valueobject.openday.OpendaysVO;
import com.wuni.util.valueobject.review.ReviewListVO;
import com.wuni.util.valueobject.review.UserReviewsVO;
import com.wuni.util.valueobject.search.CourseDetailsVO;
import com.wuni.valueobjects.AboutUsPagesVO;
import com.wuni.valueobjects.FinalFiveChoiceVO;
import com.wuni.valueobjects.InsightsVO;
import com.wuni.valueobjects.itext.UserDownloadPdfBean;
import com.wuni.valueobjects.whatunigo.ApplyNowVO;

public class ICommonBusinessImpl implements ICommonBusiness {

  private ICommonDAO commonDAO = null;

  public void setCommonDAO(ICommonDAO commonDAO) {
    this.commonDAO = commonDAO;
  }

  public ICommonDAO getCommonDAO() {
    return commonDAO;
  }
  
  // Added by Saba for Content hub profile 23_JAN_18
  public Map getContentHubDetails(ContentHubVO contentHubVO) {    
    return commonDAO.getContentHubDetails(contentHubVO); 
  } 
  
  // Added by Amir for Rich profile Landing page 13_JAN_15
  public Map getRichProfileUniLandingData(RichProfileVO richProfileVO) {    
    return commonDAO.getRichProfileUniLandingData(richProfileVO); 
  }   
  public Map versionChanges(VersionChangesVO versionChangesVO) {
   return commonDAO.versionChanges(versionChangesVO);        
  }
  // Added by Amir for all profile Landing page 03_NOV_15
  public Map getNewUniLandingData(NewUniLandingVO newUniLandingVO){
    return commonDAO.getNewUniLandingData(newUniLandingVO);
  }
  
  //Added by Indumathi May_31_2016 For Tab changes
  public Map checkCourseExists(NewUniLandingVO newUniLandingVO){
    return commonDAO.checkCourseExists(newUniLandingVO);
  }
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map getMyFinalChoiceData(MyFinalChoiceBean myFinalChoiceBean){
    return commonDAO.getMyFinalChoiceData(myFinalChoiceBean);
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map getFinalChoiceSuggestionList(MyFinalChoiceBean myFinalChoiceBean){
    return commonDAO.getFinalChoiceSuggestionList(myFinalChoiceBean);
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15  
  public Map saveFinalChoiceData(MyFinalChoiceBean myFinalChoiceBean){
    return commonDAO.saveFinalChoiceData(myFinalChoiceBean);
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map getFinalChUsrActionPod(MyFinalChoiceBean myFinalChoiceBean){
    return commonDAO.getFinalChUsrActionPod(myFinalChoiceBean);
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map confirmUsrActionPod(MyFinalChoiceBean myFinalChoiceBean){
    return commonDAO.confirmUsrActionPod(myFinalChoiceBean);
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map swapFinalChoicePod(MyFinalChoiceBean myFinalChoiceBean){
    return commonDAO.swapFinalChoicePod(myFinalChoiceBean);
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map editMyFinalChoicePod(MyFinalChoiceBean myFinalChoiceBean){
    return commonDAO.editMyFinalChoicePod(myFinalChoiceBean);
  }
 
  //To get the list of institution to display in open days browse
  public Map getOpenDaysBrowse(List inputList) {
    Map resultmap = commonDAO.getOpenDaysBrowse(inputList);
    return resultmap;
  }
  //To get the information about the openday for the college  

  public Map getOpenDaysInfo(List inputList) {
    Map resultmap = commonDAO.getOpenDaysInfo(inputList);
    return resultmap;
  }
  
  // To store Reserve openday unis list 17_MAR_2015 By Amir
  public Map saveReserveOpendayUniList(OpenDaysBean openDaysBean){
    return commonDAO.saveReserveOpendayUniList(openDaysBean);
  }

  //To check the provider is deleted or not flag 16_FEB_2016 By Prabha 
   public Map getProviderDeletedFlag(ReviewListBean reviewBean){
     return commonDAO.getProviderDeletedFlag(reviewBean);
   }
   //To check the course is degree or not flag 16_FEB_2016_REL By Prabha
   public Map getDegreeCourseFlag(CourseDetailsBean courseDetailBean){
     return commonDAO.getDegreeCourseFlag(courseDetailBean);
   }
  //To get the college video review list to display in unilanmding Tab

  public Map getUniVideoReviews(List inputList) {
    Map resultmap = commonDAO.getUniVideoReviews(inputList);
    return resultmap;
  }
  
  public Map getYearOfEntry(IWanttobeBean inputBean){
     return commonDAO.getYearOfEntry(inputBean); //Added by Indumathi.S Feb-16-2016 IWantToBe redesign
  } 
  
  //Display all video review - view all video reviews
  public Map getMoreVideoList(List inputList) {
    Map resultmap = commonDAO.getMoreVideoList(inputList);
    return resultmap;
  }
  
  public Map getReviewResultList(ReviewListVO reviewListVO){     
    return commonDAO.getReviewResultList(reviewListVO);
  }
  
  //To Get the information for the college profile 

  public Map getCollegeProfileInfo(List inputList) {
    Map resultmap = commonDAO.getCollegeProfileInfo(inputList);
    return resultmap;
  }
  //To get the list of subject that has been purchased by the college 

  public Map getSubjectProfileList(List inputList) {
    Map resultmap = commonDAO.getSubjectProfileList(inputList);
    return resultmap;
  }
  
  //To get the instition list which has bought the same subject profile 
  public Map getSubjectUniList(List inputList) {
    Map resultmap = commonDAO.getSubjectUniList(inputList);
    return resultmap;
  }
  //To get the all the members list when user click the view all members link 

  public Map getViewAllMembersList(List inputList) {
    Map resultmap = commonDAO.getViewAllMembersList(inputList);
    return resultmap;
  }
  ////To get the data that are related to review home page

  public Map getReviewHomePageData(List inputList) {
    Map resultmap = commonDAO.getReviewHomePageData(inputList);
    return resultmap;
  }
  // To get the data that are related to location to display in the following page 
  // http://www.whatuni.com/degrees/university-colleges-uk/uni-browse.html

  public Map getLocationBrowseList(List inputList) {
    Map resultmap = commonDAO.getLocationBrowseList(inputList);
    return resultmap;
  }
  //To get the data that are related university list to display in the following page
  //http://www.whatuni.com/degrees/university-colleges-uk/university-colleges-england/3/1/universities.html

  public Map getLocationUniversityList(List inputList) {
    Map resultmap = commonDAO.getLocationUniversityList(inputList);
    return resultmap;
  }
  //To get the data tyhat display in the browse journey home page 
  //http://www.whatuni.com/degrees/courses/browse.html

  public Map getJourneyHomeDataList(List inputList) {
    Map resultmap = commonDAO.getJourneyHomeDataList(inputList);
    return resultmap;
  }
  //To get the data that has to display in the browse journey category home page 
  //http://www.whatuni.com/degrees/courses/Degree-UK/qualification/M/list.html

  public Map getJourneyCategoryDataList(List inputList) {
    Map resultmap = commonDAO.getJourneyCategoryDataList(inputList);
    return resultmap;
  }
  //To get the data that has to display in the browse journey region home page 
  //http://www.whatuni.com/degrees/courses/Degree-list/Accounting-Degree-courses-UK/qualification/M/search_category/5934/loc.html

  public Map getJourneyLocationDataList(List inputList) {
    Map resultmap = commonDAO.getJourneyLocationDataList(inputList);
    return resultmap;
  }
  //To get the data that will display categroy names only produces the search result in the unilanding page tab

  public Map getCollegeBrowseSubjectList(List inputList) {
    Map resultmap = commonDAO.getCollegeBrowseSubjectList(inputList);
    return resultmap;
  }
  //This will return money-page's main details(headerId, midcontent, refineByContent, numberOfTotalRecordsForThatSearchSynario)

  public Map getMoneyPageMainContent(List inputList) {
    Map resultmap = commonDAO.getMoneyPageMainContent(inputList);
    return resultmap;
  }

  public Map getBulkProspectusColleges(List inputList) {
    Map resultmap = commonDAO.getBulkProspectusColleges(inputList);
    return resultmap;
  }
  //This will return provider-result-page's main details(headerId, midcontent, numberOfTotalRecordsForThatSearchSynario)

  public Map getProviderResultPageMainContent(List inputList) {
    Map resultmap = commonDAO.getProviderResultPageMainContent(inputList);
    return resultmap;
  }
  //This will return blog's home page details

  public Map getBlogHomeInfo(List inputList) {
    Map resultmap = commonDAO.getBlogHomeInfo(inputList);
    return resultmap;
  }
  //This will return post's home page details

  public Map getPostHomeInfo(List inputList) {
    Map resultmap = commonDAO.getPostHomeInfo(inputList);
    return resultmap;
  }
  //This will return BrowseUgCoursesByLocation page's all top to bottom details

  public Map getBrowseUgCoursesByLocation(List inputList) {
    Map resultmap = commonDAO.getBrowseUgCoursesByLocation(inputList);
    return resultmap;
  }
  //This will return BrowseUgCoursesBySubject page's all top to bottom details

  public Map getBrowseUgCoursesBySubject(List inputList) {
    Map resultmap = commonDAO.getBrowseUgCoursesBySubject(inputList);
    return resultmap;
  }
  //This will return ArticleGroupsHome all top to bottom details

  public Map getClearingSubjectLandingPage(List inputList) {
    return commonDAO.getClearingSubjectLandingPage(inputList);
  }      
  
  public Map basicFormSubmissionDBCall(QLFormBean qlFormBean) {
    return commonDAO.basicFormSubmissionDBCall(qlFormBean);
  }
  
  public Map getQlBasicFormInfo(QLFormBean qlFormBean){
    return commonDAO.getQlBasicFormInfo(qlFormBean);
  }
  
  public Map qlAdvanceFormSubmission(List inputList, Object[][] attrValues){
    return commonDAO.qlAdvanceFormSubmission(inputList, attrValues);
  }
  
  public Map getMyWhatuniData(MywhatuniInputBean inputBean){
    return commonDAO.getMyWhatuniData(inputBean);
  }
  
  public Map myWhatuniUserAttrValues(MyWhatuniInputVO inputBean, Object[][] attrValues){
    return commonDAO.myWhatuniUserAttrValues(inputBean, attrValues);//Modified list to bean Indumathi Jan-27-16
  }

  public Map PostenquirySubmission(List inputList, Object[][] postValues){
    return commonDAO.PostenquirySubmission(inputList, postValues);
  }
  
  public Map mywhatuniPostenquiry(List inputList, Object[][] mywhatuniPostValues){
    return commonDAO.mywhatuniPostenquiry(inputList, mywhatuniPostValues);
  }
  
  public RssBlogArticleVO rssArticleBlogXml(RssBlogArticleVO rssBlogArticleVO) {
    return commonDAO.rssArticleBlogXml(rssBlogArticleVO);
  }
  public Map getProviderScholarshipList(ScholarshipFormBean bean){
    return commonDAO.getProviderScholarshipList(bean);
  }
  
  public Map getMegamenuValue(){
    return commonDAO.getMegamenuValue();
  }
    
    public Map getMegaMenuDegreesList(String newstudylevelid){
      return commonDAO.getMegaMenuDegreesList(newstudylevelid);
    }

  public Map getMobileCoursesDetailsInfo(Map inputMap){
    return commonDAO.getMobileCoursesDetailsInfo(inputMap);
  }   
    
  public Map getWhyStudyGuideData(SearchBean searchbean){
    return commonDAO.getWhyStudyGuideData(searchbean);
  }//SEO_RELEASE_29_OCT_2013_REL
  
  public Map getSHTMLData(String shtmlName){
    return commonDAO.getSHTMLData(shtmlName);
  } //SEO_RELEASE_29_OCT_2013_REL
  
  //Method to get the XML content for generating PDF added by Prabha on 24_NOV_2015_REL
   public Map getPDFXMLData(SearchBean searchbean){
     return commonDAO.getPDFXMLData(searchbean);
   } 
    
   public Map getCategorydetails(String categoryCode){//10_DEC_2013_REL
    return commonDAO.getCategorydetails(categoryCode);
   }
   //
   public Map oneclickEnquirySubmit(QLFormBean qlformbean)
   {
    return commonDAO.oneclickEnquirySubmit(qlformbean);
   }
  //
  public Map updatePassword(String userId, String password, String oldPassword){//21-Jan-2014
    return commonDAO.updatePassword(userId, password, oldPassword);
  }
  
  public Map getMyWUOpendayAjax(List inputList)
  {
    return commonDAO.getMyWUOpendayAjax(inputList);
  }
  
  public Map sendAboutUsEmails(AboutUsPagesVO aboutUsPagesVO, String flag){ //26_Aug_2014
    return commonDAO.sendAboutUsEmails(aboutUsPagesVO, flag);
  }
  
  public Map getProviderResultsDetails(SearchVO inputBean)
  {
    return commonDAO.getProviderResultsDetails(inputBean);
  }
  
  public Map getUserBasketDetails(String userId)
  {
    return commonDAO.getUserBasketDetails(userId);
  }
  
  public Map saveBasket(BasketBean bean)
  {
    return commonDAO.saveBasket(bean);
  }
  
  public Map getBasketDetails(BasketBean bean)
  {
    return commonDAO.getBasketDetails(bean);
  }
  
  public Map getKeywordBrowseNodes(List inputList, String dbObject)
  {
    return commonDAO.getKeywordBrowseNodes(inputList, dbObject);
  }
  public Map getKeywordBrowseNodesDesktop(List inputList)
  {
    return commonDAO.getKeywordBrowseNodesDesktop(inputList);
  }
  public Map checkIfL2Node(String categroyId, String studyLevelId)//13_May_2014_REL
  {
    return commonDAO.checkIfL2Node(categroyId, studyLevelId);
  }
  public Map saveUltimateSearchDetails(SaveULInputbean inputbean)
  {
    return commonDAO.saveUltimateSearchDetails(inputbean);
  }
  
  public Map getWhatcanidoSearchResults(WhatCanIDoBean whatcanidoBean)
  {
    return commonDAO.getWhatcanidoSearchResults(whatcanidoBean);
  }
  //Added by Indumathi.S 08-03-16 wcid redesign
  public Map getWhatcanidoQualificationResults(WhatCanIDoBean whatcanidoBean)
  {
    return commonDAO.getWhatcanidoQualificationResults(whatcanidoBean);
  }
  
  public Map getWhatucanidoEmploymentInfo(WhatCanIDoBean inputBean)
  {
    return commonDAO.getWhatucanidoEmploymentInfo(inputBean);
  }
    
  public Map getWhatucanidoEmploymentInfoAjax(WhatCanIDoBean inputBean)
  {
    return commonDAO.getWhatucanidoEmploymentInfoAjax(inputBean);
  }
  
  public Map loadUniEmploymentInfo(WhatCanIDoBean inputBean)
  {
    return commonDAO.loadUniEmploymentInfo(inputBean);
  }
  
  public Map getEmpRateSortPagination(WhatCanIDoBean inputBean)
  {
    return commonDAO.getEmpRateSortPagination(inputBean);
  }
  
  public Map getIWanttobeSearchResults(IWanttobeBean inputBean)
  {
    return commonDAO.getIWanttobeSearchResults(inputBean);
  }

  public Map getIWanttobeEmpInfo(IWanttobeBean inputBean)
  {
    return commonDAO.getIWanttobeEmpInfo(inputBean);
  }
  
    public Map getIWanttobeEmpInfoAjax(IWanttobeBean inputBean)
    {
      return commonDAO.getIWanttobeEmpInfoAjax(inputBean);
    }

  public Map getIWanttobEmpSortPagincation(IWanttobeBean inputBean)
  {
    return commonDAO.getIWanttobEmpSortPagincation(inputBean);
  }

  public Map getIWanttobeLoadUniEmpInfo(IWanttobeBean inputBean)
  {
    return commonDAO.getIWanttobeLoadUniEmpInfo(inputBean);
  } 
  
  public Map getUltimateSearchInputDetails(SaveULInputbean inpuBean)
  {
    return commonDAO.getUltimateSearchInputDetails(inpuBean);
  }
  
  //
  public Map logProfileTabStats(ProfileVO profileVO){//3_JUN_2014
   return commonDAO.logProfileTabStats(profileVO);
  }

  public Map updateUserTimeLinePod(String userId,String timeLineId){ //16_SEP_2014 created by Amir
    Map updateUserTimeLineMap = commonDAO.updateUserTimeLinePod(userId,timeLineId);
    return updateUserTimeLineMap;
  }

  public Map getKeyStatisticsCount(){ //08_Oct_2014 added by Thiyagu G to get statistics count.
    Map keyStatisticsCountMap = commonDAO.getKeyStatisticsCount();
    return keyStatisticsCountMap;
  } 
  //
  public Map getProspectusesResults(ProspectusVO prospectusVO){ //28_Oct_2014 added by Priyaa for Prospectus chnages
    Map getProspectusesResultsMap = commonDAO.getProspectusesResults(prospectusVO);
    return getProspectusesResultsMap;    
  }
  //
  public Map getAutoCompleteSubUniResults(List inputList){
     return commonDAO.getAutoCompleteSubUniResults(inputList);
  }
  //
  public Map addProspectusBasketContent(BasketBean basketBean,  Object[][] attrValues){//28_OCT_2014 Added by Priyaa  
     Map addProspectusBasketContent = commonDAO.addProspectusBasketContent(basketBean, attrValues);
     return addProspectusBasketContent;  
  }
  //
  public Map enqBasketSubmitPrc(QLFormBean qlFormBean){//28_OCT_2014 Added by Priyaa {
     Map enqBasketSubmitPrc = commonDAO.enqBasketSubmitPrc(qlFormBean);
     return enqBasketSubmitPrc;   
  }
  // 
  public Map addRollMarketingPrc(ArrayList list){//13_JAN_2015 Added by Priyaa 
   Map adRollMarketingPrcMap = commonDAO.addRollMarketingPrc(list);
   return adRollMarketingPrcMap;   
  }
  //
  public Map prosPostEnquirySuggestion(List inputList){//3_FEB2015 For Dld prospectus post enquiry suggestion
    Map peSuggestionMap = commonDAO.prosPostEnquirySuggestion(inputList);
    return peSuggestionMap;
  }


  public Map resetPasswordPrc(String userId, String password, String sessionUserId, String caTrackFlag){//wu_537 24-FEB-2015 Added by Karthi
    Map resetPasswordPrcMap = commonDAO.resetPasswordPrc(userId, password, sessionUserId, caTrackFlag);
    return resetPasswordPrcMap;   
  }

  //
  public Map getOpenDaysLandingLocation(OpendaysVO opendaysVO) { //To get the information about the openday location landing, 17_Mar_2015 By Thiyagu G
    Map resultmap = commonDAO.getOpenDaysLandingLocation(opendaysVO);
    return resultmap;
  }
  //
  public Map getOpenDaysLandingDate(List inputList) { //To get the information about the openday date landing, 17_Mar_2015 By Thiyagu G
    Map resultmap = commonDAO.getOpenDaysLandingDate(inputList);
    return resultmap;
  }
  //
  public Map getOpenDaysWeekDateInfo(List inputList) { //To get the information about the openday date landing, 17_Mar_2015 By Thiyagu G
    Map resultmap = commonDAO.getOpenDaysWeekDateInfo(inputList);
    return resultmap;
  }  
  //
  public Map getOpendaysBrowseNodesDesktop(List inputList) { //To get the information about the openday ajax search, 17_Mar_2015 By Thiyagu G
    return commonDAO.getOpendaysBrowseNodesDesktop(inputList);
  }
  //
  public Map fetchOpenDaySearchResults(OpendaySearchVO opendaysvo){ //17_MAR_2015 Opn days search results
    Map openDaysSRMap = commonDAO.fetchOpenDaySearchResults(opendaysvo);
    return openDaysSRMap;  
  }
  
  public Map getWuscaRankListResult(StudentAwardsBean studAwdBean) {  //Added by Amir 22_Apr_15 release for New Student Choice Awards Result
    return commonDAO.getWuscaRankListResult(studAwdBean);
  }
  //
  public Map getAwardsYearList(StudentAwardsBean studAwdBean) { //Added by Indumathi.S Mar-08-16
   return commonDAO.getAwardsYearList(studAwdBean);
 }
 //
  public Map updateUserImageDetails(String userId, String imagePath){ //17_MAR_2015 Opn days search results
    Map userImgMap = commonDAO.updateUserImageDetails(userId, imagePath);
    return userImgMap;  
}
  //
  public Map getCompareBasketDetails(MyCompareBean mycompareBean){//19_MAY_2015 OpenDays MywhatuniComparison
    Map compareBasketMap = commonDAO.getCompareBasketDetails(mycompareBean);
    return compareBasketMap;
  }
  //
  public Map removeMyOpendays(OpendaysVO opendaysVO){//19_MAY_2015 OpenDays MywhatuniComparison 
   Map removeMyOpendaysMap = commonDAO.removeMyOpendays(opendaysVO);
   return removeMyOpendaysMap;
  }  
  //
  public Map getRecentCourseResults(List inputList) { //To get the information about the openday date landing, 17_Mar_2015 By Thiyagu G
    Map resultmap = commonDAO.getRecentCourseResults(inputList);
    return resultmap;
  }
  public Map sendSocialInfoEmail(UserLoginBean userLoginBean) { //Added by Indumathi.S for social box Nov-24 Rel
    Map resultmap = commonDAO.sendSocialInfoEmail(userLoginBean);
    return resultmap;
 }
  public Map getPGUniversityNewUrl(String postgraduateUrl) { //To get the PG university new subject to form the new URL pettern for 27_Jan_2016, By Thiyagu G.
    Map resultmap = commonDAO.getPGUniversityNewUrl(postgraduateUrl);
    return resultmap;
  }
  //
  public Map getYearOfEntryDetails(List inputList) { //To get year of entry 16_Feb_2015 By Thiyagu G
    Map resultmap = commonDAO.getYearOfEntryDetails(inputList);
    return resultmap;
  }
  //
  public Map getUserScoreDetails(List inputList) { //To get ucas points and score based on the subject, 16_Feb_2015 By Thiyagu G
    Map resultmap = commonDAO.getUserScoreDetails(inputList);
    return resultmap;
  }
  //
  public Map getTariffDetails(List inputList) { //To get tariff tables, 16_Feb_2015 By Thiyagu G
    Map resultmap = commonDAO.getTariffDetails(inputList);
    return resultmap;
  }  
  //
  public Map submitUcasSubjectDetails(List inputList) { //Submit subject details, 16_Feb_2015 By Thiyagu G
    Map resultmap = commonDAO.submitUcasSubjectDetails(inputList);
    return resultmap;
  }
  //
  public Map getUcasKeywordBrowseNodes(List inputList){ //To get the browse nodes 16_Feb_2015 By Thiyagu G
    return commonDAO.getUcasKeywordBrowseNodes(inputList);
  }
  //
  public Map getYearOfEntryList() { //To get tariff tables, 16_Feb_2015 By Thiyagu G
    Map resultmap = commonDAO.getYearOfEntryList();
    return resultmap;
  }
  //
   public Map getUniSortInfo(WhatCanIDoBean inputBean){ //Added by Prabha for what course i do widget 08-03-16
     return commonDAO.getUniSortInfo(inputBean);
   }
   //
   public Map getInsightData(InsightsVO insightsVO){ //Added by Prabha for Insight data 29-03-16
     return commonDAO.getInsightData(insightsVO);
  }
  //
  public Map getInstitutionId(InsightsVO insightsVO){ //Added to get Institution id 31_May_2016, by Thiayagu G
    return commonDAO.getInstitutionId(insightsVO);
  }
  //Added for login method changed from datamodal to spring for 28_June_2016, By Thiyagu G. start   
  public Map checkEmailBlocked(RegistrationBean registrationBean){
    return commonDAO.checkEmailBlocked(registrationBean);
  }
  //
  public Map checkUserAuthentication(RegistrationBean registrationBean){ 
    return commonDAO.checkUserAuthentication(registrationBean);
  }
  //   
  public Map checkEmailExist(RegistrationBean registrationBean){
    return commonDAO.checkEmailExist(registrationBean);
  }
  //   
  public Map getUserYearOfEntry(RegistrationBean registrationBean){
    return commonDAO.getUserYearOfEntry(registrationBean);
  }
  //Added for login method changed from datamodal to spring for 28_June_2016, By Thiyagu G. end
  //To get the user name by Prabha on 28_Jun_2016   
  public Map getUserName(RegistrationBean registrationBean){
    return commonDAO.getUserName(registrationBean);
  }
  //To get the User login info by Prabha on 28_Jun_2016   
  public Map getUserInformationLogin(RegistrationBean registrationBean){
    return commonDAO.getUserInformationLogin(registrationBean);
  }
  //To check the user login status byPrabha on 28_jun_2016  
  public Map preLoginUpdateStatusCode(RegistrationBean registrationBean){
    return commonDAO.preLoginUpdateStatusCode(registrationBean);
  }
  //To get the sys var value by Prabha on 28_jun_2016
  public Map getWUSysVarValue(RegistrationBean registrationBean){
    return commonDAO.getWUSysVarValue(registrationBean);
  }
  //To get the sys var value by Prabha on 28_jun_2016
  public Map getCourseSubject(RegistrationBean registrationBean){
    return commonDAO.getCourseSubject(registrationBean);
  }
  //   
  public Map getBasketCount(RegistrationBean registrationBean){
    return commonDAO.getBasketCount(registrationBean);
  }
  //   
  public Map updateCaTrackFlag(MyWhatuniInputVO myWhatuniInputVO){
    return commonDAO.updateCaTrackFlag(myWhatuniInputVO);
  }
  
  //To get the new college details By Thiyagu G on 24_jan_2017
  public Map getNewCollegeDetails(SearchVO inputVO)
  {
    return commonDAO.getNewCollegeDetails(inputVO);
  }
  //
  public Map getCourseScemaInfo(CourseDetailsVO courseDetailsVO){
    return commonDAO.getCourseScemaInfo(courseDetailsVO);
  }
  //
  public Map getPopularSubjects(CourseSearchVO csVO) { //To get the popular subjects list, 21_Mar_2016 By Thiyagu G
    Map resultmap = commonDAO.getPopularSubjects(csVO);
    return resultmap;
  }
  //
  public Map swapFinalFiveChoice(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception{ //Swap the final five choice in a basket by Prabha on 16_May_2017 
    Map resultmap = commonDAO.swapFinalFiveChoice(finalFiveChoiceVO);
    return resultmap;
  }
  //
  public Map editConfirmFinalFive(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception{ //Edit confirm final five choice in a basket by Prabha on 16_May_2017
    Map resultmap = commonDAO.editConfirmFinalFive(finalFiveChoiceVO);
    return resultmap;
  }
  //
  public Map getSuggestedComparisonPod(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception{ //Get the suggested pod content by Prabha on 16_May_2017
    Map resultmap = commonDAO.getSuggestedComparisonPod(finalFiveChoiceVO);
    return resultmap;
  }
  //
  public Map saveUpdateFinalChoice(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception{ //save or update the final choice content by Prabha on 16_May_2017
    Map resultmap = commonDAO.saveUpdateFinalChoice(finalFiveChoiceVO);
    return resultmap;
  }
  //
   public Map updateBasketCourse(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception{ //update course content by Prabha on 16_May_2017
     Map resultmap = commonDAO.updateBasketCourse(finalFiveChoiceVO);
     return resultmap;
   }
   //To get the course name By Thiyagu G on 16_May_2017
   public Map getCourseName(CourseDetailsBean courseDetailsBean){
     return commonDAO.getCourseName(courseDetailsBean);
   }
   //Signup in ALP by Prabha on 08_Aug_17
    public Map appLandingPageSignUp(RegistrationBean registrationBean){
      return commonDAO.appLandingPageSignUp(registrationBean);
    }
    
  public Map getAdvanceProviderResultsInfo(SearchVO inputBean)
  {
    return commonDAO.getAdvanceProviderResultsInfo(inputBean);
  }
  
  public Map getOpendayPopupAjax(OpendayVO opendayVO)
  {
    return commonDAO.getOpendayPopupAjax(opendayVO);
  }
  
  public Map getRecentSearchResultList(AutoCompleteVO autoCompleteVO)
  {
    return commonDAO.getRecentSearchResultList(autoCompleteVO);
  }
  
  public Map getInterstitialSubjectList(SubjectAjaxVO subjectAjaxVO) { //Added for Advance search page Subject ajax by Sabapathi on 28_AUG_2018_REL
    Map resultmap = commonDAO.getInterstitialSubjectList(subjectAjaxVO);
    return resultmap;
  }
  
  public Map getUserProfilePdf(UserDownloadPdfBean userDownloadPdfBean) { //Added for User profile download by Hema on 25_sep_2018_REL
    Map resultmap = commonDAO.getUserProfilePdf(userDownloadPdfBean);
    return resultmap;
  }
  
    public Map getReviewBreakDownList(AutoCompleteVO autoCompleteVO) { 
      Map resultmap = commonDAO.getReviewBreakDownList(autoCompleteVO);
      return resultmap;
    }
    
  // To update if user minimized the chatbot, 25_Sep_2018 By Sabapathi
  public void updateChatBotDisable(String userId, Object[][] attrValues) { 
    commonDAO.updateChatBotDisable(userId, attrValues);
  }
  
  // To get the user review for displaying the light box, 19_Dec_2018 By Jeyalakshmi
  public Map getReviewLightbox(UserReviewsVO userReviewsVO) {
    Map resultmap = commonDAO.getReviewLightbox(userReviewsVO);
    return resultmap;
  }
  
  //
   public Map getSubjectReviews(AutoCompleteVO autoCompleteVO){
     Map resultMap = commonDAO.getSubjectReviews(autoCompleteVO);
     return resultMap;
   }

  public Map getReviewSubjectList(AutoCompleteVO autoCompleteVO) {
    Map resultMap = commonDAO.getReviewSubjectList(autoCompleteVO);
    return resultMap;
  }
//To get course details for apply now journey 
 public Map applyNowCourseDetails(ApplyNowVO applyNowVO){    
   Map resultMap = commonDAO.applyNowCourseDetails(applyNowVO);
   return resultMap;
 }
  public Map getCourseList(AutoCompleteVO autoCompleteVO){
      Map resultMap = commonDAO.getCourseList(autoCompleteVO);
      return resultMap;   
  }
//To get the details of grade filter page in wugo journey Jeya 28-3-2019 
 public Map getGradeFilterPage(GradeFilterBean gradeFilterBean) {
   Map resultMap = commonDAO.getGradeFilterPage(gradeFilterBean);
   return resultMap;
 }
 //To get Qual Subject Ajax in grade filter page in wugo journey Jeya 28-3-2019 
 public Map getSubjectlist(String freeText,GradeFilterBean gradeFilterBean) {
   Map resultMap = commonDAO.getSubjectlist(freeText, gradeFilterBean);
   return resultMap;
 }
 //To get ucas points Ajax in grade filter page in wugo journey Jeya 28-3-2019 
 public Map getSubjectUcasPoints(GradeFilterBean gradeFilterBean) {
   Map resultMap = commonDAO.getSubjectUcasPoints(gradeFilterBean);
   return resultMap;
 }
 //for grade filter page validation in wugo journey Jeya 28-3-2019 
 public Map validateQualGrade(GradeFilterBean gradeFilterBean) {
   Map resultMap = commonDAO.validateQualGrade(gradeFilterBean);
   return resultMap;
 }
 // for saving the qualification in grade filter page  in wugo journey Jeya 28-3-2019 
 public Map saveQualGradeFilter(GradeFilterBean gradeFilterBean) {
   Map resultMap = commonDAO.saveQualGradeFilter(gradeFilterBean);
   return resultMap;
 }
 // for getting the matching course count in grade filter page in wugo journey Jeya 28-3-2019 
 public Map getMatchingCourse(GradeFilterBean gradeFilterBean) {
   Map resultMap = commonDAO.getMatchingCourse(gradeFilterBean);
   return resultMap;
 }
 
//To get the details of new SR grade filter page Jeya 22-10-2019 
public Map getSRGradeFilterPage(GradeFilterBean gradeFilterBean) {
 Map resultMap = commonDAO.getSRGradeFilterPage(gradeFilterBean);
 return resultMap;
}
//To get Qual Subject Ajax of new SR grade filter page Jeya 22-10-2019
public Map getSRSubjectlist(String freeText,GradeFilterBean gradeFilterBean) {
 Map resultMap = commonDAO.getSRSubjectlist(freeText, gradeFilterBean);
 return resultMap;
}
//To get ucas points Ajax in grade filter page in Jeya 22-10-2019
public Map getSRSubjectUcasPoints(GradeFilterBean gradeFilterBean) {
 Map resultMap = commonDAO.getSRSubjectUcasPoints(gradeFilterBean);
 return resultMap;
}
//for grade filter page validation in wugo journey Jeya 28-3-2019
//public Map validateQualGrade(GradeFilterBean gradeFilterBean) {
// Map resultMap = commonDAO.validateQualGrade(gradeFilterBean);
// return resultMap;
//}
// for saving the qualification in grade filter page in wugo journey Jeya 28-3-2019
public Map saveSRQualGradeFilter(GradeFilterBean gradeFilterBean) {
 Map resultMap = commonDAO.saveSRQualGradeFilter(gradeFilterBean);
 return resultMap;
}
// for getting the matching course count in grade filter page in wugo journey Jeya 28-3-2019
public Map getSRMatchingCourse(GradeFilterBean gradeFilterBean) {
 Map resultMap = commonDAO.getSRMatchingCourse(gradeFilterBean);
 return resultMap;
}
 //
 
 public Map getDBName(){
   Map resultMap = commonDAO.getDBName();
   return resultMap;
 }
 //
  public Map getUserNameDetail(RegistrationBean registrationBean){
    return commonDAO.getUserNameDetail(registrationBean);
  }
  //
  
  public Map getSubjectLandingRegionList() throws Exception{
   Map resultMap = commonDAO.getSubjectLandingRegionList();
   return resultMap;
 }
  //
  public Map getYearOfEntryDetails() throws Exception{
    Map resultMap = commonDAO.getYearOfEntryDetails();
    return resultMap;
  }
  //
  public Map getCookieLatestUpdatedDate() throws Exception{
    Map resultMap = commonDAO.getCookieLatestUpdatedDate();
    return resultMap;
  }  
  
//for deleting the user record if already saved in db
 public Map deleteUserRecord(GradeFilterBean gradeFilterBean) {
   Map resultMap = commonDAO.deleteUserRecord(gradeFilterBean);
   return resultMap;
 }  
 //
  public Map getTopNavSubjectList(AutoCompleteVO autoCompleteVO) {
    Map resultMap = commonDAO.getTopNavSubjectList(autoCompleteVO);
    return resultMap;
  }
  
  public Map<String, Object> getCovid19Data() {
    return commonDAO.getCovid19Data();
  }
  //For getting clearing profile data
  public Map getClearingProfileDetails(ClearingProfileVO clearingProfileVO) {
    Map resultMap = commonDAO.getClearingProfileDetails(clearingProfileVO);
    return resultMap;
  }
  
  //For getting the navigation url from DB for CTA buttons by Sujitha V on 2020_DEC_08
  public Map getNavigationUrl(String subOrderItemId, String CourseId, String ctaButtonName) throws Exception{
	Map resultMap = commonDAO.getNavigationUrl(subOrderItemId, CourseId, ctaButtonName);
	return resultMap;  
  }
  //Added by Sri Sankari for Sysvar calls on 19_Jan_2021 rel
  public Map<String, Object> getSysvarValues() throws Exception {
    return commonDAO.getSysvarValues();
  }
  
  public Map getQualificationList (SRGradeFilterBean srGradeFilterBean) {
	Map resultMap = commonDAO.getQualificationList(srGradeFilterBean);
	return resultMap;
  }

  public Map saveSRGradeFilterInfo(SRGradeFilterBean srGradeFilterBean) {
	Map resultMap = commonDAO.saveSRGradeFilterInfo(srGradeFilterBean);
    return resultMap;
  }

  public Map deleteSRGradeFilterInfo(SRGradeFilterBean srGradeFilterBean) {
	Map resultMap = commonDAO.deleteSRGradeFilterInfo(srGradeFilterBean);
	return resultMap;
  }
}
