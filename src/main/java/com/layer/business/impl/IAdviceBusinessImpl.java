package com.layer.business.impl;

import java.util.List;
import java.util.Map;
import spring.valueobject.advice.AdviceDetailInfoVO;
import spring.valueobject.advice.AdviceHomeInfoVO;
import com.layer.business.IAdviceBusiness;
import com.layer.dao.IAdviceDAO;

/**
 * contains business logic for Articles
 *
 * @since        wu333_20141209
 * @author       Thiyagu G
 *
 */
public class IAdviceBusinessImpl implements IAdviceBusiness {

    private IAdviceDAO adviceDAO = null;

    public void setAdviceDAO(IAdviceDAO adviceDAO) {
        this.adviceDAO = adviceDAO;
    }

    public IAdviceDAO getAdviceDAO() {
        return adviceDAO;
    }

   /**
    * will return article-details page info
    * URL_PATTERN: www.whatuni.com/degrees/advice.html
    *
    *
    * @since        wu333_20141209 - redesign
    * @author       Thiyagu G
    * @param inputList
    * @return
    *
    */  
   public Map getAdviceHomeInfo(List inputList) {
     Map resultmap = adviceDAO.getAdviceHomeInfo(inputList);
     return resultmap;
   }

    /**
     * will return article-details page info
     * URL_PATTERN: www.whatuni.com/degrees/advice.html
     *
     *
     * @since        wu333_20141209 - redesign
     * @author       Thiyagu G
     * @param inputList
     * @return
     *
     */  
    public Map getPrimaryCategoryLandingInfo(List inputList) {
      Map resultmap = adviceDAO.getPrimaryCategoryLandingInfo(inputList);
      return resultmap;
    }
        
    public Map getSecondaryCategoryLandingInfo(List inputList) {
      Map resultmap = adviceDAO.getSecondaryCategoryLandingInfo(inputList);
      return resultmap;
    }
     
    public Map getAdviceSearchResults(List inputList) {
      Map resultmap = adviceDAO.getAdviceSearchResults(inputList);
      return resultmap;
    }
    
    public Map getAdviceDetailInfo(List inputList) {
      Map resultmap = adviceDAO.getAdviceDetailInfo(inputList);
      return resultmap;
    }
    /**
     * will return trending/most popular advice info
     * 
     * @since wu546_20151103
     * @author Prabhakaran V.
     * @param inputList
     * @return @author Prabhakaran V.
     */
    public Map getAdviceAjaxDetail(List inputList) {
      Map resultmap = adviceDAO.getAdviceAjaxDetail(inputList);
      return resultmap;
    }
    
  public Map checkArticleAndPrimaryCategoryAvilable(AdviceDetailInfoVO adviceDetailInfoVO) {
    Map resultmap = adviceDAO.checkArticleAndPrimaryCategoryAvilable(adviceDetailInfoVO);
    return resultmap;
  }
  //
  public Map getArticlesPod(AdviceHomeInfoVO adviceHomeInfoVO) throws Exception{ //Get the articles pod on 16_May_2017, By Thiyagu G.
    Map resultmap = adviceDAO.getArticlesPod(adviceHomeInfoVO);
    return resultmap;
  }
  
  public Map getArticleList(List inputList) {
    Map resultmap = adviceDAO.getArticleList(inputList);
	return resultmap;
  }

@Override
  public Map getCovidSnippetPod(AdviceHomeInfoVO adviceHomeInfoVO) throws Exception {
	Map resultmap = adviceDAO.getCovidSnippetPod(adviceHomeInfoVO);
	return resultmap;
  }
}
