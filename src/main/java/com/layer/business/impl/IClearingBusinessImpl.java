package com.layer.business.impl;

import com.layer.business.IClearingBusiness;
import com.layer.dao.IClearingDAO;

public class IClearingBusinessImpl implements IClearingBusiness {

  private IClearingDAO clearingDAO = null;

  public void setClearingDAO(IClearingDAO clearingDAO) {
    this.clearingDAO = clearingDAO;
  }

  public IClearingDAO getClearingDAO() {
    return clearingDAO;
  }

}
