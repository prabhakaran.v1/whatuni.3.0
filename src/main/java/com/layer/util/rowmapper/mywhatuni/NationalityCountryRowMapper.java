package com.layer.util.rowmapper.mywhatuni;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.STRUCT;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.mywhatuni.MyWhatuniGetdataVO;

 /**
   * @NationalityCountryRowMapper
   * @author Thiyagu G
   * @version 1.0
   * @since 03.11.2015
   * @purpose This program is used get and split the dataset and set to VO..
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
   * *************************************************************************************************************************
   * 03_NOV_2015   Thiyagu G                  1.0      To get data and set the VO object.                            wu_545
   */

public class NationalityCountryRowMapper implements RowMapper{
   public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
    java.sql.Array optionsRS = (java.sql.Array)rs.getObject("options");
    Object datanum[] = (Object[])optionsRS.getArray();  // added for weblogic migration 28-JUN_2016_REL
    ArrayList optionList = new ArrayList();
    for (int i = 0; i < datanum.length; i++) {
      STRUCT struct = (STRUCT)datanum[i];
      Object obj[] = struct.getAttributes();
      MyWhatuniGetdataVO optionsVO = new MyWhatuniGetdataVO();
      optionsVO.setOptionId(String.valueOf(obj[0]));
      optionsVO.setOptionDesc((String)obj[1]);
      optionsVO.setOptionValue((String)obj[2]);
      optionList.add(optionsVO);
    }
    return optionList;
  }
 }