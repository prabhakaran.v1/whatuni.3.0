/**
 * Created for 21 Jan 2014 
 * It is used to get the CPE pod details of myopendays page.
 */

package com.layer.util.rowmapper.mywhatuni;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.opendays.OpenDaysCalendarVO;
import WUI.utilities.GlobalFunction;

import com.wuni.util.seo.SeoUrls;

public class MywuOpendayCPEPod implements RowMapper
{

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
    OpenDaysCalendarVO bean = new OpenDaysCalendarVO();
    bean.setCollegeId(rs.getString("college_id"));
    bean.setCollegeName(rs.getString("college_name"));
    bean.setCollegeNameDisplay(rs.getString("college_name_display"));
    bean.setCollegeLocation(rs.getString("college_location"));
    bean.setCpepodopendayscount(rs.getString("cnt"));
    bean.setUniHomeURL(new SeoUrls().construnctUniHomeURL(bean.getCollegeId(), bean.getCollegeName()));
    ResultSet profileDetails = (ResultSet)rs.getObject("profile_image"); 
    try
    {
      if(profileDetails != null)
      {
        while(profileDetails.next())
        {
          bean.setMediaType(profileDetails.getString("media_type"));
          if (bean.getMediaType() != null && bean.getMediaType().equalsIgnoreCase("VIDEO")) {
            bean.setMediaPath(profileDetails.getString("media_path"));
            if (bean.getMediaPath() != null && bean.getMediaPath().trim().length() > 0) {
              bean.setMediaPath(new GlobalFunction().videoThumbnailFormatChange(bean.getMediaPath(), "1"));
            }
          } else if (bean.getMediaType() != null && bean.getMediaType().equalsIgnoreCase("PICTURE")) {
            bean.setMediaThumbPath(profileDetails.getString("thumb_media_path"));
          }
        }
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }finally
    {
      try
      {
        if(profileDetails != null)
        {
          profileDetails.close();
        }
      }
      catch(Exception e)
      {
        throw new SQLException(e.getMessage());
      }
    }
    
    ResultSet nextOpendayRS = (ResultSet)rs.getObject("next_open_day");
    try
    {
      if(nextOpendayRS != null)
      {
        while(nextOpendayRS.next())
        {
          bean.setOpenDay_date(nextOpendayRS.getString("open_day_date"));
          bean.setOpenDay_month_year(nextOpendayRS.getString("open_month"));
          bean.setDateExistFlg(nextOpendayRS.getString("date_exist"));
        }
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      try
      {
        if(nextOpendayRS != null)
        {
          nextOpendayRS.close();
        }
      }
      catch(Exception e)
      {
        throw new SQLException(e.getMessage());
      }
    }
    return bean;
    
  }

}
