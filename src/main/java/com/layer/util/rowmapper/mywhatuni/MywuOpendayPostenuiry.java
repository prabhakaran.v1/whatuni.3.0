/**
 * Created for 21 Jan 2014 release
 * It is used to get the myopendays postenquiry details.
 */

package com.layer.util.rowmapper.mywhatuni;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.opendays.OpenDaysCalendarVO;

import com.wuni.util.seo.SeoUrls;

public class MywuOpendayPostenuiry implements RowMapper
{
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
    OpenDaysCalendarVO openDaysPEVO = new OpenDaysCalendarVO();
    SeoUrls seoUrl = new SeoUrls();
    openDaysPEVO.setCollegeId(rs.getString("college_id"));
    openDaysPEVO.setCollegeName(rs.getString("college_name"));
    openDaysPEVO.setCollegeNameDisplay(rs.getString("college_name_display"));
    openDaysPEVO.setCollegeLocation(rs.getString("college_location"));
    openDaysPEVO.setUniHomeURL(seoUrl.construnctUniHomeURL(openDaysPEVO.getCollegeId(), openDaysPEVO.getCollegeName()));
    
    ResultSet nextOpendayRS = (ResultSet)rs.getObject("next_open_day");
    try
    {
      if(nextOpendayRS != null)
      {
        while(nextOpendayRS.next())
        {
          openDaysPEVO.setOpenDay_date(nextOpendayRS.getString("open_day_date"));
          openDaysPEVO.setOpenDay_month_year(nextOpendayRS.getString("open_month"));
          openDaysPEVO.setDateExistFlg(nextOpendayRS.getString("date_exist"));
          openDaysPEVO.setEventId(nextOpendayRS.getString("event_id"));
        }
      }
    }
    catch(Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      try
      {
        if(nextOpendayRS != null)
        {
          nextOpendayRS.close();
        }
      }
      catch(Exception e)
      {
        throw new SQLException(e.getMessage());
      }
    }    
    
    return openDaysPEVO;
  }
}




