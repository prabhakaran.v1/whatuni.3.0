package com.layer.util.rowmapper.advert;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.advert.SpListVO;

public class SpListRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    SpListVO spListVO = new SpListVO();
    SeoUrls seoUrl = new SeoUrls();
    //
    spListVO.setCollegeId(resultset.getString("college_id"));
    spListVO.setCollegeName(resultset.getString("college_name"));
    spListVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    spListVO.setAdvertName(resultset.getString("advert_name"));
    spListVO.setAdvertDescShort(resultset.getString("advert_short_desc"));
    spListVO.setMyhcProfileId(resultset.getString("myhc_profile_id"));
    spListVO.setCpeQualificationNetworkId(resultset.getString("network_id"));
    spListVO.setProfileId("0"); //"resultset.getString("profile_id"));
    spListVO.setSectionDesc("overview"); //resultset.getString("section_desc"));
    //
    spListVO.setSpListHomeURL(seoUrl.construnctSpListHomeURL(spListVO.getCollegeId()));
    spListVO.setSpURL(seoUrl.construnctSpURL(spListVO.getCollegeId(), spListVO.getCollegeName(), spListVO.getMyhcProfileId(), spListVO.getProfileId(), spListVO.getCpeQualificationNetworkId(), spListVO.getSectionDesc()));
    //
    return spListVO;
  }

}
