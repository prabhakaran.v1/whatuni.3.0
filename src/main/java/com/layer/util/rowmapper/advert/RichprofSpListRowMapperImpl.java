package com.layer.util.rowmapper.advert;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonUtil;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.advert.SpListVO;

public class RichprofSpListRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    SpListVO spListVO = new SpListVO();
    SeoUrls seoUrl = new SeoUrls();
    spListVO.setCollegeId(resultset.getString("college_id"));
    spListVO.setCollegeName(resultset.getString("college_name"));
    spListVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    spListVO.setAdvertName(resultset.getString("advert_name"));
    spListVO.setAdvertDescShort(resultset.getString("advert_short_desc"));
    spListVO.setMyhcProfileId(resultset.getString("myhc_profile_id"));
    spListVO.setCpeQualificationNetworkId(resultset.getString("network_id"));
    spListVO.setProfileId("0");
    spListVO.setSectionDesc("overview");
    spListVO.setImagePath(resultset.getString("image_path"));
    if (!GenericValidator.isBlankOrNull(spListVO.getImagePath())) {
      String mediaTypeId = resultset.getString("media_type_id");
      String splitMediaPath = "";
      if ("1".equals(mediaTypeId) || "17".equals(mediaTypeId)) {
        splitMediaPath = spListVO.getImagePath().replace(".", "_116px.");
        spListVO.setImagePath(new CommonUtil().getImgPath((splitMediaPath), rowNum));
      } else if ("2".equals(mediaTypeId) || "16".equals(mediaTypeId)) {
        splitMediaPath = spListVO.getImagePath().replace(".", "_140px.");
        spListVO.setImagePath(new CommonUtil().getImgPath((splitMediaPath), rowNum));
      }
    }
    spListVO.setSpURL(seoUrl.construnctSpURL(spListVO.getCollegeId(), spListVO.getCollegeName(), spListVO.getMyhcProfileId(), spListVO.getProfileId(), spListVO.getCpeQualificationNetworkId(), spListVO.getSectionDesc()));
    return spListVO;
  }
}
