package com.layer.util.rowmapper.advert;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.profile.ProfileTabsVO;

public class CollegeProfileTabsRowMapper implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ProfileTabsVO profileTabsVO = new ProfileTabsVO();
    profileTabsVO.setMyhcProfileId(rs.getString("MYHC_PROFILE_ID"));
    profileTabsVO.setProfileId(rs.getString("PROFILE_ID"));
    profileTabsVO.setButtonLabel(rs.getString("BUTTON_LABEL"));
    profileTabsVO.setCollegeDisplayName(rs.getString("college_name"));
    return profileTabsVO;
  }
}


     