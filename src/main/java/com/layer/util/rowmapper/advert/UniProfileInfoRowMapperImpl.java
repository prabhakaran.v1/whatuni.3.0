package com.layer.util.rowmapper.advert;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;

import com.wuni.util.valueobject.CollegeCpeInteractionDetailsWithMediaVO;

public class UniProfileInfoRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    CollegeCpeInteractionDetailsWithMediaVO enquiryInfo = new CollegeCpeInteractionDetailsWithMediaVO();
    //
    enquiryInfo.setCollegeId(resultset.getString("college_id"));
    enquiryInfo.setCollegeName(resultset.getString("college_name"));
    enquiryInfo.setCollegeNameDisplay(resultset.getString("college_name_display"));
    enquiryInfo.setCollegeLocation(resultset.getString("college_location"));
    enquiryInfo.setProfileShortDesc(resultset.getString("profile_short_desc"));
    enquiryInfo.setProfileDesc(resultset.getString("profile_desc"));    
    //
    // media details from db
    enquiryInfo.setMediaId(resultset.getString("MEDIA_ID"));
    enquiryInfo.setMediaType(resultset.getString("MEDIA_TYPE"));
    enquiryInfo.setMediaPath(resultset.getString("MEDIA_PATH"));
    enquiryInfo.setMediaPathThumb(resultset.getString("THUMB_MEDIA_PATH"));
    //customized media details
    //reassign image/video related variables according to mediaType.
    if (enquiryInfo.getMediaType() != null && enquiryInfo.getMediaType().equalsIgnoreCase("VIDEO")) {
      enquiryInfo.setIsMediaAttached("TRUE");
      enquiryInfo.setVideoId(enquiryInfo.getMediaId());
      enquiryInfo.setVideoPathFlv(enquiryInfo.getMediaPath());
      if (enquiryInfo.getVideoPathFlv() != null && enquiryInfo.getVideoPathFlv().trim().length() > 0) {
        //enquiryInfo.setVideoPath0001(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "1"));
        //enquiryInfo.setVideoPath0002(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "2"));
        //enquiryInfo.setVideoPath0003(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "3"));
        //enquiryInfo.setVideoPath0004(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "4"));
         enquiryInfo.setVideoPath0001(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(enquiryInfo.getMediaId(), "1"), rowNum)); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path.
         enquiryInfo.setVideoPath0002(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(enquiryInfo.getMediaId(), "2"), rowNum));
         enquiryInfo.setVideoPath0003(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(enquiryInfo.getMediaId(), "3"), rowNum));
         enquiryInfo.setVideoPath0004(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(enquiryInfo.getMediaId(), "4"), rowNum));
      }
      enquiryInfo.setImageId("FALSE");
      enquiryInfo.setImagePath("FALSE");
      enquiryInfo.setImagePathThumb("FALSE");
    } else if (enquiryInfo.getMediaType() != null && enquiryInfo.getMediaType().equalsIgnoreCase("PICTURE")) {
      enquiryInfo.setIsMediaAttached("TRUE");
      enquiryInfo.setImageId(enquiryInfo.getMediaId());
      enquiryInfo.setImagePath(enquiryInfo.getMediaPath());
      enquiryInfo.setImagePathThumb(enquiryInfo.getMediaPathThumb());
      if (enquiryInfo.getImagePathThumb() == null) {
        if(resultset.getString("media_type_id")!=null && "16".equals(resultset.getString("media_type_id"))){
          String splitMediaPath = enquiryInfo.getImagePath().replace(".", "_140px.");
          enquiryInfo.setImagePathThumb(splitMediaPath);
        }else{
          enquiryInfo.setImagePathThumb(enquiryInfo.getImagePath());
        }  
      }
      enquiryInfo.setVideoId("FALSE");
      enquiryInfo.setVideoPathFlv("FALSE");
      enquiryInfo.setVideoPath0001("FALSE");
      enquiryInfo.setVideoPath0002("FALSE");
      enquiryInfo.setVideoPath0003("FALSE");
      enquiryInfo.setVideoPath0004("FALSE");
    }
    //
    return enquiryInfo;
  }

}
