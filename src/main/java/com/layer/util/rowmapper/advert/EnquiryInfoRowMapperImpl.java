package com.layer.util.rowmapper.advert;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;

import com.wuni.util.valueobject.CollegeCpeInteractionDetailsWithMediaVO;

public class EnquiryInfoRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    CollegeCpeInteractionDetailsWithMediaVO enquiryInfo = new CollegeCpeInteractionDetailsWithMediaVO();
    String tmpUrl = null;
    //
    enquiryInfo.setOrderItemId(resultset.getString("ORDER_ITEM_ID"));
    enquiryInfo.setSubOrderItemId(resultset.getString("SUBORDER_ITEM_ID"));
    enquiryInfo.setMyhcProfileId(resultset.getString("MYHC_PROFILE_ID"));
    enquiryInfo.setProfileType(resultset.getString("PROFILE_TYPE"));
    enquiryInfo.setAdvertName(resultset.getString("ADVERT_NAME"));
    enquiryInfo.setCpeQualificationNetworkId(resultset.getString("NETWORK_ID"));
    //
    enquiryInfo.setSubOrderEmail(resultset.getString("EMAIL"));
    enquiryInfo.setSubOrderProspectus(resultset.getString("PROSPECTUS"));
    enquiryInfo.setHotLineNo(resultset.getString("hotline"));
    //
    tmpUrl = resultset.getString("WEBSITE");
    if (tmpUrl != null && tmpUrl.trim().length() > 0) {
        tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
    } else {
      tmpUrl = "";
    }
    enquiryInfo.setSubOrderWebsite(tmpUrl);
    //
    tmpUrl = resultset.getString("EMAIL_WEBFORM");
    if (tmpUrl != null && tmpUrl.trim().length() > 0) {
        tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
    } else {
      tmpUrl = "";
    }
    enquiryInfo.setSubOrderEmailWebform(tmpUrl);
    //
    tmpUrl = resultset.getString("PROSPECTUS_WEBFORM");
    if (tmpUrl != null && tmpUrl.trim().length() > 0) {
        tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
    } else {
      tmpUrl = "";
    }
    enquiryInfo.setSubOrderProspectusWebform(tmpUrl);
    // media details from db
    enquiryInfo.setMediaId(resultset.getString("MEDIA_ID"));
    enquiryInfo.setMediaType(resultset.getString("MEDIA_TYPE"));
    enquiryInfo.setMediaPath(resultset.getString("MEDIA_PATH"));
    enquiryInfo.setMediaPathThumb(resultset.getString("THUMB_MEDIA_PATH"));
    //customized media details
    //reassign image/video related variables according to mediaType.
    if (enquiryInfo.getMediaType() != null && enquiryInfo.getMediaType().equalsIgnoreCase("VIDEO")) {
      enquiryInfo.setIsMediaAttached("TRUE");
      enquiryInfo.setVideoId(enquiryInfo.getMediaId());
      enquiryInfo.setVideoPathFlv(enquiryInfo.getMediaPath());
      if (enquiryInfo.getVideoPathFlv() != null && enquiryInfo.getVideoPathFlv().trim().length() > 0) {
        enquiryInfo.setVideoPath0001(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "1"));
        enquiryInfo.setVideoPath0002(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "2"));
        enquiryInfo.setVideoPath0003(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "3"));
        enquiryInfo.setVideoPath0004(new GlobalFunction().videoThumbnailFormatChange(enquiryInfo.getVideoPathFlv(), "4"));
      }
      enquiryInfo.setImageId("FALSE");
      enquiryInfo.setImagePath("FALSE");
      enquiryInfo.setImagePathThumb("FALSE");
    } else if (enquiryInfo.getMediaType() != null && enquiryInfo.getMediaType().equalsIgnoreCase("PICTURE")) {
      enquiryInfo.setIsMediaAttached("TRUE");
      enquiryInfo.setImageId(enquiryInfo.getMediaId());
      enquiryInfo.setImagePath(enquiryInfo.getMediaPath());
      enquiryInfo.setImagePathThumb(enquiryInfo.getMediaPathThumb());
      if (enquiryInfo.getImagePathThumb() == null || enquiryInfo.getVideoPathFlv().trim().length() == 0) {
        enquiryInfo.setImagePathThumb(enquiryInfo.getImagePath());
      }
      enquiryInfo.setVideoId("FALSE");
      enquiryInfo.setVideoPathFlv("FALSE");
      enquiryInfo.setVideoPath0001("FALSE");
      enquiryInfo.setVideoPath0002("FALSE");
      enquiryInfo.setVideoPath0003("FALSE");
      enquiryInfo.setVideoPath0004("FALSE");
    }
    //
    return enquiryInfo;
  }

}
