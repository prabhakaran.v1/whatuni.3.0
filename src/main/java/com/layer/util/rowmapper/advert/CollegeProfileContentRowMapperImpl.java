package com.layer.util.rowmapper.advert;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;

import com.wuni.util.valueobject.advert.CollegeProfileVO;

public class CollegeProfileContentRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    CollegeProfileVO collegeProfileVO = new CollegeProfileVO();
    collegeProfileVO.setCollegeId(rs.getString("college_id"));
    collegeProfileVO.setCollegeName(rs.getString("college_name"));
    collegeProfileVO.setCollegeNameDisplay(rs.getString("college_name_display"));
    collegeProfileVO.setProfileImagePath(rs.getString("image_path"));
    collegeProfileVO.setProfileDescription(rs.getString("description"));
    collegeProfileVO.setSelectedSectionName(rs.getString("button_lable"));
    collegeProfileVO.setProfileId(rs.getString("profile_id"));
    collegeProfileVO.setMyhcProfileId(rs.getString("myhc_profile_id"));
    collegeProfileVO.setCollegeImagePath("");
    String dataArray[] = null;
    String durationArray[] = null;
    if (rs.getString("image_path") != null && rs.getString("image_path").trim().length() > 0) {
      dataArray = rs.getString("image_path").split("###", 3);
    }
    String imagePath = "";
    int lenth = dataArray != null? dataArray.length: 0;
    if (lenth == 1) {
      imagePath = dataArray[0];
      collegeProfileVO.setCollegeImagePath(imagePath);
    } else if (lenth > 1) {
      collegeProfileVO.setVideoId(dataArray[0]);
      collegeProfileVO.setVideoUrl(dataArray[1]);
      if(collegeProfileVO.getVideoUrl().indexOf(".flv") > -1){
        String videoDuration = dataArray[2]; //wu505_23042013 release
         if(!GenericValidator.isBlankOrNull(videoDuration)){
           durationArray = videoDuration.split(":", 2);
           int length1 = durationArray != null? durationArray.length: 0;
           if(length1 > 1){
             String minutes = durationArray[0];
             String seconds = durationArray[1];
             collegeProfileVO.setVideoMetaDuration("T"+minutes+"M"+seconds+"S");
           }
         }
      }
      //collegeProfileVO.setVideoThumbnailUrl(new GlobalFunction().videoThumbnailFormatChange(dataArray[1], "4"));
      String thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(collegeProfileVO.getVideoId(), "4"), rowNum); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path.
      collegeProfileVO.setVideoThumbnailUrl(thumbnailPath);
      
    }
    if (rs.getString("js_stats_log_id") != null && rs.getString("js_stats_log_id").trim().length() > 0) {
      collegeProfileVO.setJsStatsLogId(rs.getString("js_stats_log_id"));
    }
    collegeProfileVO.setDLogTypeKey(rs.getString("d_log_type_key"));
    return collegeProfileVO;
  }
}
