package com.layer.util.rowmapper.scholarship;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;

import com.wuni.util.valueobject.ScholarshipVO;

public class RichProvScholarshipRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ScholarshipVO scholarshipVO = new ScholarshipVO();
    scholarshipVO.setScholarshipId(rs.getString("scholarship_id"));
    scholarshipVO.setScholarshipTitle(rs.getString("scholarship_title"));
    scholarshipVO.setCollegeId(rs.getString("college_id"));
    scholarshipVO.setCollegeName(rs.getString("college_name"));
    scholarshipVO.setScholarshipDesc(rs.getString("scholarship_desc"));
    scholarshipVO.setScholarshipCnt(rs.getString("total_scholarship"));
    String scholarshipInfoURL = "/degrees/scholarship-uk/" + (rs.getString("scholarship_title") != null? new CommonFunction().replaceHypen(new CommonFunction().replaceURL(new CommonFunction().replaceSpecialCharacter(rs.getString("scholarship_title")))): "");
    scholarshipInfoURL = scholarshipInfoURL + "/" + rs.getString("scholarship_id") + "/bursary-info.html";
    scholarshipVO.setScholarshipInfoURL(scholarshipInfoURL.toLowerCase());
    return scholarshipVO;
  }
}
