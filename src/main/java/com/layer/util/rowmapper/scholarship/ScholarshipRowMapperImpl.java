package com.layer.util.rowmapper.scholarship;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.ScholarshipVO;

public class ScholarshipRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ScholarshipVO scholarshipVO = new ScholarshipVO();
    scholarshipVO.setScholarshipId(rs.getString("scholarship_id"));
    scholarshipVO.setScholarshipTitle(rs.getString("scholarship_title"));
    scholarshipVO.setCollegeId(rs.getString("college_id"));
    scholarshipVO.setCollegeName(rs.getString("college_name"));
  return scholarshipVO;
  }
}
