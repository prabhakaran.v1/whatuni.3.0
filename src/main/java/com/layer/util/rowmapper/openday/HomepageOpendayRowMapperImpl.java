package com.layer.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.openday.OpendaysVO;

public class HomepageOpendayRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    OpendaysVO opendaysVO = new OpendaysVO();
    //
    opendaysVO.setCollegeId(resultset.getString("college_id"));
    opendaysVO.setCollegeName(resultset.getString("college_name"));
    opendaysVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    opendaysVO.setNextOpenday(resultset.getString("next_open_day"));
    opendaysVO.setOpendayType(resultset.getString("open_day_type"));
    opendaysVO.setOpendayDate(resultset.getString("open_date"));
    opendaysVO.setCollegeLocation(resultset.getString("college_location"));
    //
    return opendaysVO;
  }

}