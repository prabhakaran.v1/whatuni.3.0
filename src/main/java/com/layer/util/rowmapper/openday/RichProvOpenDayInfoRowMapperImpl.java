package com.layer.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.openday.OpendaysVO;

public class RichProvOpenDayInfoRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    OpendaysVO opendaysVO = new OpendaysVO();
    //
    opendaysVO.setCollegeId(resultset.getString("college_id"));
    opendaysVO.setCollegeName(resultset.getString("college_name"));
    opendaysVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    opendaysVO.setOpendayDate(resultset.getString("open_day"));
    //opendaysVO.setStartDate(resultset.getString("open_date")); //Added open date column to show the schema tagging for openday section on 16_May_2017, By Thiyagu G.
    opendaysVO.setHeadline(resultset.getString("headlines"));
    opendaysVO.setOpendayVenue(resultset.getString("event_venue"));
    opendaysVO.setOpendayDesc(resultset.getString("openday_desc"));
    opendaysVO.setCollegeLocation(resultset.getString("college_location"));
    opendaysVO.setOpendayExternalUrl(resultset.getString("openday_url"));
    //opendaysVO.setEndDate(resultset.getString("end_date")); //Added end date column to show the schema tagging for openday section on 16_May_2017, By Thiyagu G.
    //Added open day start and end date in schema tag for time stamp format for Nov_20 th rel by Sangeeth.S
    opendaysVO.setStOpenDayStartDate(resultset.getString("schematag_od_start_date"));
    opendaysVO.setStOpenDayEndDate(resultset.getString("schematag_od_end_date"));
    opendaysVO.setEventCategoryId(resultset.getString("EVENT_CATEGORY_ID"));
    opendaysVO.setEventCategoryName(resultset.getString("EVENT_CATEGORY_NAME"));
    //
    opendaysVO.setOpendaysProviderURL(new SeoUrls().constructOpendaysSeoUrl(opendaysVO.getCollegeName(), opendaysVO.getCollegeId()));    
    //
    return opendaysVO;
  }
}
