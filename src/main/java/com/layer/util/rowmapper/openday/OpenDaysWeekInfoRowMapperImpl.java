package com.layer.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonUtil;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.openday.OpendaysVO;

/**
  * @OpenDaysWeekInfoRowMapperImpl
  * @author Thiyagu G
  * @version 1.0
  * @since 17.03.2015
  * @purpose  This program is used to assign all objects from cursor and to build the open days date page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.       Changes desc                                      Rel Ver.
  * *************************************************************************************************************************
  * 17-March-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
  */
public class OpenDaysWeekInfoRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    OpendaysVO openDaysVO = new OpendaysVO();
    openDaysVO.setDays(rs.getString("DAYS"));
    openDaysVO.setStartDate(rs.getString("START_DATE"));
    ResultSet openDetailsRS = (ResultSet)rs.getObject("OPENDAY_COLLEGES");
    try {
      if (openDetailsRS != null) {
        ArrayList openDaysWeekInfoList = new ArrayList();
        OpendaysVO opendaysInnerVO = null;
        while (openDetailsRS.next()) {
          opendaysInnerVO = new OpendaysVO();
          opendaysInnerVO.setCollegeId(openDetailsRS.getString("COLLEGE_ID"));
          opendaysInnerVO.setCollegeName(openDetailsRS.getString("COLLEGE_NAME"));
          opendaysInnerVO.setCollegeNameDisplay(openDetailsRS.getString("COLLEGE_NAME_DISPLAY"));
          opendaysInnerVO.setOpendaysProviderURL(new SeoUrls().constructOpendaysSeoUrl(opendaysInnerVO.getCollegeName(), opendaysInnerVO.getCollegeId()));
          opendaysInnerVO.setCollegeLogo(openDetailsRS.getString("LOGO_PATH"));
          if (!GenericValidator.isBlankOrNull(opendaysInnerVO.getCollegeLogo())) {
            opendaysInnerVO.setOpdProviderImage(new CommonUtil().getImgPath((opendaysInnerVO.getCollegeLogo()), rowNum));
          }
          openDaysWeekInfoList.add(opendaysInnerVO);
        }
        openDaysVO.setOpenDaysWeekInfoList(openDaysWeekInfoList);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        openDetailsRS.close();
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    openDaysVO.setDaysPosition(rs.getString("DAYTH"));
    return openDaysVO;
  }
}
