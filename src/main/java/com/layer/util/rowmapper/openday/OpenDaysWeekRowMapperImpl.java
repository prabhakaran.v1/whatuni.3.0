package com.layer.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.openday.OpendaysVO;

/**
  * @OpenDaysWeekRowMapperImpl
  * @author Thiyagu G
  * @version 1.0
  * @since 17.03.2015
  * @purpose  This program is used to assign all objects from cursor and to build the open days date page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.       Changes desc                                      Rel Ver.
  * *************************************************************************************************************************
  * 17-March-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
  */
public class OpenDaysWeekRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    OpendaysVO openDaysVO = new OpendaysVO();
    openDaysVO.setWeeks(rs.getString("WEEKS"));
    openDaysVO.setStartDate(rs.getString("START_DATE"));
    openDaysVO.setEndDate(rs.getString("END_DATE"));
    openDaysVO.setCurrentMonth(rs.getString("CURRENT_MONTH"));
    openDaysVO.setLinkFlag(rs.getString("LINK_FLAG"));
    return openDaysVO;
  }
}
