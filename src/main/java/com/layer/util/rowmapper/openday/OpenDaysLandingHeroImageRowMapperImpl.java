package com.layer.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.openday.OpendaysVO;

/**
  * @OpenDaysLandingHeroImageRowMapperImpl
  * @author Thiyagu G
  * @version 1.0
  * @since 17.03.2015
  * @purpose  This program is used to assign all objects from cursor and to build the open days location page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.       Changes desc                                      Rel Ver.
  * *************************************************************************************************************************
  * 17-March-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
  */
public class OpenDaysLandingHeroImageRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    OpendaysVO openDaysVO = new OpendaysVO();
    openDaysVO.setHomeHeroImageId(rs.getString("HOME_HEROIMAGE_ID"));
    openDaysVO.setOrderItemId(rs.getString("ORDER_ITEM_ID"));
    openDaysVO.setCollegeId(rs.getString("COLLEGE_ID"));
    openDaysVO.setCollegeName(rs.getString("COLLEGE_NAME"));
    openDaysVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
    openDaysVO.setMediaId(rs.getString("MEDIA_ID"));
    openDaysVO.setMediaTypeId(rs.getString("MEDIA_TYPE_ID"));
    openDaysVO.setMediaPath(rs.getString("MEDIA_PATH"));
    openDaysVO.setTitle(rs.getString("TITLE"));
    openDaysVO.setSubTitle(rs.getString("SUB_TITLE"));
    openDaysVO.setImageUrl(rs.getString("IMAGE_URL"));
    openDaysVO.setStartDate(rs.getString("START_DATE"));
    openDaysVO.setExpiryDate(rs.getString("EXPIRY_DATE"));
    openDaysVO.setStatus(rs.getString("STATUS"));
    openDaysVO.setCreatedDate(rs.getString("CREATED_DATE"));
    openDaysVO.setUpdatedDate(rs.getString("UPDATED_DATE"));
    return openDaysVO;
  }
}
