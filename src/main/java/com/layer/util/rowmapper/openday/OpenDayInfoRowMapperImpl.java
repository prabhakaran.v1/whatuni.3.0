package com.layer.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.openday.OpendaysVO;

public class OpenDayInfoRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    OpendaysVO opendaysVO = new OpendaysVO();
    //
    opendaysVO.setCollegeId(resultset.getString("college_id"));
    opendaysVO.setCollegeName(resultset.getString("college_name"));
    opendaysVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    opendaysVO.setCollegeLocation(resultset.getString("college_location"));
    opendaysVO.setOpendayCount(resultset.getString("openday_count"));
    opendaysVO.setNextOpenday(resultset.getString("next_col_openday"));
    opendaysVO.setOpendayExternalUrl(resultset.getString("open_day_url"));
    opendaysVO.setOpendayText(resultset.getString("open_day_text"));
    //
    return opendaysVO;
  }

}
