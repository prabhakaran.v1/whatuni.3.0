package com.layer.util.rowmapper.openday;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.openday.OpendaySearchVO;

/**
 * @OpenDaysSrRowMapperImpl - Row mapper used for getting openday search result page data
 * @author prabhakaran.v
 * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
 * @comments - Added EVENT_CATEGORY_ID, EVENT_CATEGORY_NAME and EVENT_DATE column
 *
 */
public class OpenDaysSrRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    OpendaySearchVO openDaysVo = new OpendaySearchVO();
    openDaysVo.setOpenDate(resultSet.getString("START_DATE"));
    ResultSet openDaysRs = (ResultSet)resultSet.getObject("LC_COLLEGE_OPEN_DAY_RESULTS");
    try {
      if (openDaysRs != null) {
        ArrayList openDaysSearchList = new ArrayList();
        OpendaySearchVO openDaySearchVo = null;
        while (openDaysRs.next()) {
          openDaySearchVo = new OpendaySearchVO();
          openDaySearchVo.setOpenDate(openDaysRs.getString("START_DATE"));
          openDaySearchVo.setCollegeId(openDaysRs.getString("COLLEGE_ID"));
          openDaySearchVo.setTown(openDaysRs.getString("TOWN"));
          openDaySearchVo.setVenue(openDaysRs.getString("VENUE"));
          openDaySearchVo.setHeadlines(openDaysRs.getString("HEADLINES"));
          openDaySearchVo.setBookingUrl(openDaysRs.getString("BOOKING_URL"));
          openDaySearchVo.setOpenDate(openDaysRs.getString("OPEN_DATE"));
          openDaySearchVo.setOpenMonthYear(openDaysRs.getString("OPEN_MONTH_YEAR"));
          openDaySearchVo.setOpenDayEventId(openDaysRs.getString("EVENT_CALENDAR_ITEM_ID"));
          openDaySearchVo.setOpenDayExistsFlag(openDaysRs.getString("OPEN_DAY_EXISTS_FLAG"));
          openDaySearchVo.setSubOrderItemId(openDaysRs.getString("SUBORDER_ITEM_ID"));
          openDaySearchVo.setNetworkQualificationId(openDaysRs.getString("NETWORK_ID"));
          openDaySearchVo.setWebsitePrice(openDaysRs.getString("WEBSITE_PRICE"));
          openDaySearchVo.setQualification(openDaysRs.getString("OPEN_DAY_QUALIFICATION"));
          openDaySearchVo.setCollegeLogo(openDaysRs.getString("COLLEGE_LOGO"));
          openDaySearchVo.setCollegeName(openDaysRs.getString("COLLEGE_NAME"));
          openDaySearchVo.setCollegeDisplayName(openDaysRs.getString("COLLEGE_NAME_DISPLAY"));
          openDaySearchVo.setPastDateFlag(openDaysRs.getString("PAST_OPEN_DAYS_FLAG"));
          openDaySearchVo.setStartDateDD(openDaysRs.getString("START_DATE_DD"));
          openDaySearchVo.setStartDateDay(openDaysRs.getString("START_DATE_DAY"));
          openDaySearchVo.setStartDateMonth(openDaysRs.getString("START_DATE_MONTH"));
          openDaySearchVo.setEventCategoryId(openDaysRs.getString("EVENT_CATEGORY_ID"));
          openDaySearchVo.setEventCategoryName(openDaysRs.getString("EVENT_CATEGORY_NAME"));
          openDaySearchVo.setEventDate(openDaysRs.getString("EVENT_DATE"));
          if (!GenericValidator.isBlankOrNull(openDaySearchVo.getCollegeName())) {
            openDaySearchVo.setProviderUrl(new SeoUrls().constructOpendaysSeoUrl(openDaySearchVo.getCollegeName().trim(), openDaySearchVo.getCollegeId()));
          } 
          openDaysSearchList.add(openDaySearchVo);
        }
        openDaysVo.setOpenDaysSRList(openDaysSearchList);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (openDaysRs != null) {
          openDaysRs.close();
          openDaysRs = null;
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    return openDaysVo;
  }
}
