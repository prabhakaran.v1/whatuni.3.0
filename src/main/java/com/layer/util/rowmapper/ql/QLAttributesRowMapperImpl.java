package com.layer.util.rowmapper.ql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.STRUCT;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import com.wuni.advertiser.ql.form.QuestionAnswerBean;

public class QLAttributesRowMapperImpl implements RowMapper{
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
    QuestionAnswerBean qlquestionaansbean = new QuestionAnswerBean();
    qlquestionaansbean.setOrderAttributeId(rs.getString("ORDER_ATTRIBUTE_ID"));
    qlquestionaansbean.setTypeAttributeId(rs.getString("TYPE_ATTRIBUTE_ID"));
    qlquestionaansbean.setAttributeId(rs.getString("ATTRIBUTE_ID"));
    qlquestionaansbean.setAttrValueSeq(rs.getString("ATTR_VAL_DISPLAY_SEQ"));
    qlquestionaansbean.setAttributeName(rs.getString("ATTRIBUTE_NAME"));
    qlquestionaansbean.setDisplaySeq(rs.getString("DISPLAY_SEQ"));
    qlquestionaansbean.setMandatory(rs.getString("MANDATORY"));
    qlquestionaansbean.setDefaultValue(GenericValidator.isBlankOrNull(rs.getString("DEFAULT_VALUE")) ? "" : rs.getString("DEFAULT_VALUE"));
    qlquestionaansbean.setAttributeText(rs.getString("ATTRIBUTE_TEXT"));
    qlquestionaansbean.setOptionFlag(rs.getString("OPTION_FLAG"));
    //
    //if ("Y".equals(qlquestionaansbean.getOptionFlag())) {
    java.sql.Array optionsRS = (java.sql.Array)rs.getObject("OPTIONS");// added for weblogic migration 28-JUN_2016_REL
    Object datanum[] = (Object[])optionsRS.getArray();
     ArrayList optionList = new ArrayList();
     for (int i = 0; i < datanum.length; i++) {
       STRUCT struct = (STRUCT)datanum[i];
       Object obj[] = struct.getAttributes();
       QuestionAnswerBean optionsVO = new QuestionAnswerBean();
       optionsVO.setOptionId(String.valueOf(obj[0]));
       optionsVO.setOptionDesc((String)obj[1]);
       optionsVO.setOptionValue((String)obj[2]);
       optionList.add(optionsVO);
     }
    qlquestionaansbean.setOptions(optionList);
    //}
    //
    qlquestionaansbean.setMaxLength(rs.getString("MAX_LENGTH"));
    qlquestionaansbean.setFormatDesc(rs.getString("FORMAT_DESC"));
    qlquestionaansbean.setDataTypeDesc(rs.getString("DATA_TYPE_DESC"));
    qlquestionaansbean.setGroupId(rs.getString("GROUP_ID"));
    qlquestionaansbean.setHelpFlag(rs.getString("HELP_FLAG"));
    qlquestionaansbean.setHelpText(rs.getString("HELP_TEXT"));
    qlquestionaansbean.setOptionId(rs.getString("OPTION_ID"));
    if("Y".equalsIgnoreCase(rs.getString("OPTION_FLAG"))){
      qlquestionaansbean.setAttributeValue(qlquestionaansbean.getOptionId());      
    }else{
      qlquestionaansbean.setAttributeValue(rs.getString("ATTRIBUTE_VALUE"));
    }
    qlquestionaansbean.setAttributeValueId(rs.getString("ENQ_ATTR_VAL_ID"));
    qlquestionaansbean.setClassName(rs.getString("css_class_name"));
    qlquestionaansbean.setErrorDesc(rs.getString("mandatory_err_msg"));
    //
    return qlquestionaansbean;

  }
}
