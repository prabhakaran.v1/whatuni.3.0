package com.layer.util.rowmapper.ql;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.ql.PostEnquiryVO;

/**
* @PostEnquriyRowMapperImpls
* @author Thiyagu G
* @version 1.0
* @since 03.11.2015
* @purpose This program is used call the database procedure to post enquiry data.
* Change Log
* *************************************************************************************************************************
* Date           Name                      Ver.     Changes desc                                                 Rel Ver.
* *************************************************************************************************************************
* 03_NOV_2015   Thiyagu G                  1.0      get the post enquiry data.                                   wu_546
*/

public class PostEnquriyRowMapperImpls implements RowMapper{
  public Object mapRow(ResultSet rs, int i) throws SQLException {
    PostEnquiryVO postVO = new PostEnquiryVO();
    postVO.setCollegeId(rs.getString("college_id"));
    postVO.setCollegeName(rs.getString("college_name"));
    postVO.setCollegeDisplayName(rs.getString("college_name_display"));      
    postVO.setEnquiryType(rs.getString("enquiry_type"));
    postVO.setInteractionType(rs.getString("interaction_type"));
    postVO.setCollegeLogo(rs.getString("college_logo"));
    postVO.setDpFlag(rs.getString("download_prospectus"));
    postVO.setCourseTitle(rs.getString("course_title"));
    postVO.setCourseId(rs.getString("course_id"));
    SeoUrls seoUrl = new SeoUrls();
    seoUrl.construnctCourseDetailsPageURL(rs.getString("study_level_detail"),postVO.getCourseId(), postVO.getCourseTitle(), postVO.getCollegeId(), "NORMAL_COURSE");
    postVO.setEmailPrice(rs.getString("email_price"));
    if((!GenericValidator.isBlankOrNull(postVO.getCourseId()) || !"0".equalsIgnoreCase(postVO.getCourseId()))){
      postVO.setCourseDetailsURL(new SeoUrls().construnctCourseDetailsPageURL(postVO.getCourseSeoStudyLevelText(), postVO.getCourseTitle(), postVO.getCourseId(), postVO.getCollegeId(), "COURSE_SPECIFIC"));
    }
    ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("enquiry_details");
    try{
      if(enquiryDetailsRS != null){
        while(enquiryDetailsRS.next()){
          String tmpUrl = null;
          postVO.setOrderItemId(enquiryDetailsRS.getString("order_item_id"));
          postVO.setSubOrderEmail(enquiryDetailsRS.getString("email"));            
          tmpUrl = enquiryDetailsRS.getString("email_webform");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          postVO.setSubOrderEmailWebform(tmpUrl);
          postVO.setSubOrderProspectus(enquiryDetailsRS.getString("prospectus"));
          tmpUrl = enquiryDetailsRS.getString("prospectus_webform");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          postVO.setSubOrderProspectusWebform(tmpUrl);
          tmpUrl = enquiryDetailsRS.getString("website");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          postVO.setSubOrderWebsite(tmpUrl);
          postVO.setSubOrderItemId(enquiryDetailsRS.getString("suborder_item_id"));
          postVO.setMyhcProfileId(enquiryDetailsRS.getString("myhc_profile_id"));            
          postVO.setProfileType(enquiryDetailsRS.getString("profile_type"));
          postVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("network_id"));            
          postVO.setAdvertName(enquiryDetailsRS.getString("advert_name"));            
        }
      }
    }catch(Exception e){
      e.printStackTrace();
    }finally{
      try{
        enquiryDetailsRS.close();
      }catch(Exception e){
        throw new SQLException(e.getMessage());
      }
    }
    
    ResultSet dpDetailsRs = (ResultSet)rs.getObject("DP_DETAILS");
    try {
      if (dpDetailsRs != null) {
        while (dpDetailsRs.next()) {
          postVO.setDpURL(dpDetailsRs.getString("dpurl"));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (dpDetailsRs != null) {
          dpDetailsRs.close();
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    postVO.setUniHomeUrl(seoUrl.construnctUniHomeURL(rs.getString("college_id"), rs.getString("college_name")).toLowerCase());
    if("IP".equalsIgnoreCase(postVO.getProfileType())){
      postVO.setWhatUniURL(seoUrl.construnctUniHomeURL(rs.getString("college_id"), rs.getString("college_name")).toLowerCase());
    }else if("SP".equalsIgnoreCase(postVO.getProfileType())){
      postVO.setWhatUniURL(seoUrl.construnctSpURL(postVO.getCollegeId(), postVO.getCollegeName(), postVO.getMyhcProfileId(), "0", postVO.getCpeQualificationNetworkId(), "overview"));
    }
    return postVO;    
  }
}