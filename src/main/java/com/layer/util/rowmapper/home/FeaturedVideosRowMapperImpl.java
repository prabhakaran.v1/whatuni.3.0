package com.layer.util.rowmapper.home;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.form.VideoReviewListBean;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;

/**
 * This calss is used to load the best video on homepage.
 */
public class FeaturedVideosRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    VideoReviewListBean bean = new VideoReviewListBean();
    bean.setCollegeId(rs.getString("college_id"));
    bean.setCollegeName(rs.getString("college_name"));
    bean.setCollegeNameDisplay(rs.getString("college_name_display"));
    //bean.setCollegeProfileFlag(rs.getString("college_profile"));
    //bean.setVideoReviewId(rs.getString("review_id"));
    //bean.setVideoReviewTitle(rs.getString("review_title"));
    //bean.setMyhcProfileId(rs.getString("myhc_profile_id"));
    //bean.setProfileType(rs.getString("myhc_profile_type"));
    bean.setVideoType(rs.getString("video_type"));
    bean.setMediaId(rs.getString("media_id"));
    /*String videoType = rs.getString("video_type");
    String videoThumbNumber = rs.getString("video_thumb_nos"); //adeed for 28th July 2009 Release
    if (videoType != null && videoType.equalsIgnoreCase("V")) {
      bean.setThumbnailUrl(rs.getString("thumbnail_assoc_text"));
    } else {
      bean.setVideoUrl(rs.getString("video_assoc_text"));*/
      //bean.setThumbnailUrl(new GlobalFunction().videoThumbnailFormatChange(new CommonUtil().getImgPath("/commimg/whatuni/", rowNum) + rs.getString("video_assoc_text"), "2"));
      bean.setThumbnailUrl(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(bean.getMediaId(), "3"), rowNum));
   // }
    bean.setCollegeLogo(rs.getString("college_logo"));
    return bean;
  }

}
