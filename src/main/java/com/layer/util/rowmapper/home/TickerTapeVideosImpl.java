package com.layer.util.rowmapper.home;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.form.VideoReviewListBean;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;

/**
   * This class is used to load the video ticker tape list.
   */
public class TickerTapeVideosImpl implements RowMapper {

  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    String limeLightPath = GlobalConstants.LIMELIGHT_VIDEO_PATH;
    VideoReviewListBean bean = new VideoReviewListBean();
    bean.setVideoReviewId(resultSet.getString("review_id"));
    bean.setCollegeId(resultSet.getString("college_id"));
    bean.setCollegeName(resultSet.getString("college_name"));
    bean.setCollegeNameDisplay(resultSet.getString("college_name_display"));
    bean.setVideoType(resultSet.getString("video_type"));
    if (bean.getVideoType() != null && bean.getVideoType().equalsIgnoreCase("V")) {
      bean.setThumbnailUrl(resultSet.getString("thumbnail_assoc_text"));
    } else {
      bean.setVideoUrl(resultSet.getString("video_url"));
      bean.setThumbnailUrl(resultSet.getString("thumbnail_path"));
      //bean.setThumbnailUrl(new GlobalFunction().videoThumbnailFormatChange(new CommonUtil().getImgPath("/commimg/whatuni/", rowNum) + resultSet.getString("video_assoc_text"), "3"));
      bean.setThumbnailUrl(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnailReviewAppserver(bean, "3"), rowNum));
    }
    return bean;
  }

}
