package com.layer.util.rowmapper.home;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.form.CourseJourneyBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

public class CategorySubjectPodImpl implements RowMapper {

  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    CourseJourneyBean bean = new CourseJourneyBean();
    bean.setSubjectId(resultSet.getString("subject_id"));
    bean.setSubjectName(resultSet.getString("subject_name"));
    bean.setStudyLevelId(resultSet.getString("qualification_code"));
    String seoStudyLevelDesc = resultSet.getString("seo_studylevel_text");
    bean.setStudyLevelDesc(seoStudyLevelDesc);
    String subjectName = resultSet.getString("subject_name");
    bean.setSubjectName(subjectName);
    subjectName = (subjectName != null && String.valueOf(subjectName).trim().length() > 0 && String.valueOf(subjectName).indexOf("#") >= 0) ? String.valueOf(subjectName).replaceAll("#", "") : subjectName;
    subjectName = subjectName != null && !subjectName.equalsIgnoreCase("null") && subjectName.trim().length() > 0 ? new CommonFunction().replaceHypen(new CommonFunction().replaceURL(subjectName)) : "";
    //http://192.168.1.159:8988/degrees/courses/degree-courses/accounting-degree-courses-united-kingdom/m/united+kingdom/r/5934/page.html
    String randomizeURL = GlobalConstants.SEO_PATH_COURSEJOUNEY_PAGE + seoStudyLevelDesc + "/" + subjectName + "-" + seoStudyLevelDesc + "-united-kingdom/m/united+kingdom/r/" + bean.getSubjectId() + "/page.html";
    bean.setHyperLinkUrl(randomizeURL.toLowerCase());
    return bean;
  }

}
