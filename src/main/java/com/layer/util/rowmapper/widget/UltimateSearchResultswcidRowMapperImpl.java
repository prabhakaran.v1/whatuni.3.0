package com.layer.util.rowmapper.widget;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;

 /**
    * @UltimateSearchResultswcidRowMapperImpl  
    * @version 1.0
    * @author Thiyagu G
    * @since 21-July-2015
    * @purpose  This rowmapper is used to read the results for given input of Whatcanido and Coursesearch.
    * Change Log
    * ***************************************************************************************************************************************************
    * Date           Name          Ver.       Changes desc                                                                  Rel Ver.
    * ***************************************************************************************************************************************************
    * 
    */

public class UltimateSearchResultswcidRowMapperImpl implements RowMapper{
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException
  {
   WhatCanIDoBean whatcanidoVO = new WhatCanIDoBean();
   whatcanidoVO.setJacsCode(rs.getString("jacs_code"));
   whatcanidoVO.setJacsSubject(rs.getString("jacs_subject"));
   whatcanidoVO.setJacsQualification(rs.getString("jacs_qualifications"));        
   whatcanidoVO.setJacsPercentage(rs.getString("jacs_percentage"));
   whatcanidoVO.setSubjectGuideURL(rs.getString("subject_guide_url"));
   whatcanidoVO.setSearchGrades(rs.getString("grade_url_string"));
   whatcanidoVO.setEquivalentTarriffPoints(rs.getString("equivalent_tariff_point"));
   whatcanidoVO.setJacsDefinition(rs.getString("jacs_description"));
   ResultSet qualSubjectDetails = (ResultSet)rs.getObject("c_previous_study");
   try
   {
     if(qualSubjectDetails != null)
     {
       ArrayList qualSubjectArrayList = new ArrayList();
        WhatCanIDoBean qualSubjectVO = null;
       while(qualSubjectDetails.next())
       {
         qualSubjectVO = new WhatCanIDoBean();
         qualSubjectVO.setQualSubjectId(qualSubjectDetails.getString("subject_id"));
         qualSubjectVO.setQualSubjectName(qualSubjectDetails.getString("subject_desc"));
         qualSubjectVO.setQualSubPercentage(qualSubjectDetails.getString("subject_percentage"));
         qualSubjectArrayList.add(qualSubjectVO);
       }
       whatcanidoVO.setQualSubjectList(qualSubjectArrayList);
     }
   }catch(Exception e)
   {
     e.printStackTrace();
   }finally
   {
     try
     {
       qualSubjectDetails.close();
     }catch(Exception e)
     {
       throw new SQLException(e.getMessage());
     }
   }
   return whatcanidoVO;
  }
}
