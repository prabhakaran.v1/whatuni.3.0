package com.layer.util.rowmapper.widget;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonUtil;

import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;

 /**
    * @UltimateSearchResultsRowMapperImpl  
    * @version 1.0
    * @author Thiyagu G
    * @since 21-July-2015
    * @purpose  This rowmapper is used to read the results for given input of Whatcanido and Coursesearch.
    * Change Log
    * ***************************************************************************************************************************************************
    * Date           Name          Ver.       Changes desc                                                                  Rel Ver.
    * ***************************************************************************************************************************************************
    * 
    */

public class UltimateSearchResultsRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
   IWanttobeBean iwanttobeResultsVO = new IWanttobeBean();
   iwanttobeResultsVO.setJacsCode(rs.getString("jacs_code"));
   iwanttobeResultsVO.setJacsSubject(rs.getString("jacs_subject"));
   iwanttobeResultsVO.setJacsQualification(rs.getString("jacs_qualifications"));        
   iwanttobeResultsVO.setJacsPercentage(rs.getString("jacs_percentage"));
   iwanttobeResultsVO.setEmpPercentage(rs.getString("percentage_employed"));
   iwanttobeResultsVO.setSalaryRange(rs.getString("salary_range"));
   iwanttobeResultsVO.setSubjectGuideURL(rs.getString("subject_guide_url"));
   iwanttobeResultsVO.setIwantSearchCode(rs.getString("job_or_industry_id"));
   iwanttobeResultsVO.setIwantSearchName(rs.getString("job_or_industry_name"));
   iwanttobeResultsVO.setJobIndustryDefinition(rs.getString("job_OR_industry_definition"));
   iwanttobeResultsVO.setEntryRequirements(rs.getString("entry_requirements"));
   iwanttobeResultsVO.setJacsDefinition(rs.getString("jacs_description"));
   ResultSet qualSubjectDetails = (ResultSet)rs.getObject("c_previous_study");
    try
    {
      if(qualSubjectDetails != null)
      {
        ArrayList qualSubjectArrayList = new ArrayList();
        IWanttobeBean qualSubjectVO = null;
        while(qualSubjectDetails.next())
        {
          qualSubjectVO = new IWanttobeBean();//3_FEB_2015 Modified to change the parameters
          qualSubjectVO.setQualSubjectId(qualSubjectDetails.getString("subject_id"));
          qualSubjectVO.setQualSubjectName(qualSubjectDetails.getString("subject_desc"));
          qualSubjectVO.setQualSubPercentage(qualSubjectDetails.getString("subject_percentage"));
          qualSubjectArrayList.add(qualSubjectVO);
        }
        iwanttobeResultsVO.setQualSubjectList(qualSubjectArrayList);
      }
      iwanttobeResultsVO.setImagePath(rs.getString("image_path"));
      if (!GenericValidator.isBlankOrNull(iwanttobeResultsVO.getImagePath())) {
        iwanttobeResultsVO.setImageName(new CommonUtil().getImgPath((iwanttobeResultsVO.getImagePath()), rowNum));
      }
      iwanttobeResultsVO.setCreatedDate(rs.getString("created_date"));
    }catch(Exception e)
    {
      e.printStackTrace();
    }finally
    {
      try
      {
        qualSubjectDetails.close();
      }catch(Exception e)
      {
        throw new SQLException(e.getMessage());
      }
    }     
   return iwanttobeResultsVO;
  }
}