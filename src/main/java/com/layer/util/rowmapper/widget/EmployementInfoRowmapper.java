package com.layer.util.rowmapper.widget;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;

/**
  * @EmployementInfoRowmapper
  * @version 1.0
  * @author Prabhakaran V.
  * @since 16-Feb-2016
  * @purpose  This rowmapper is used to read the results for given input of I want to be widget..
  * Change Log
  * ***************************************************************************************************************************************************
  * Date           Name          Ver.       Changes desc                                                                  Rel Ver.
  * ***************************************************************************************************************************************************
  *
  */
public class EmployementInfoRowmapper implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    IWanttobeBean employementInfoVO = new IWanttobeBean();
    employementInfoVO.setStickManFig(rs.getString("stickman_figure"));
    employementInfoVO.setSalaryRange(rs.getString("salary_range"));
    employementInfoVO.setEntryRequirements(rs.getString("entry_requirements"));
    employementInfoVO.setJacsDefinition(rs.getString("jacs_description"));
    employementInfoVO.setStickManColor(rs.getString("stickman_figure_colour"));
    employementInfoVO.setSalaryRangeColor(rs.getString("salary_range_colour"));
    employementInfoVO.setEmpTooltipTxt(rs.getString("job_ind_emp_rate_text"));
    employementInfoVO.setSalaryTooltipTxt(rs.getString("job_ind_salary_text"));
    employementInfoVO.setSalaryDisplayColor(rs.getString("salary_display_percentages"));
    ResultSet qualSubjectDetails = (ResultSet)rs.getObject("c_previous_study");
    try {
      if (qualSubjectDetails != null) {
        ArrayList qualSubjectArrayList = new ArrayList();
        IWanttobeBean qualSubjectVO = null;
        while (qualSubjectDetails.next()) {
          qualSubjectVO = new IWanttobeBean();
          qualSubjectVO.setQualSubjectId(qualSubjectDetails.getString("subject_id"));
          qualSubjectVO.setQualSubjectName(qualSubjectDetails.getString("subject_desc"));
          qualSubjectVO.setQualSubPercentage(qualSubjectDetails.getString("subject_percentage"));
          String donutColor = qualSubjectDetails.getString("colour");
          if (!GenericValidator.isBlankOrNull(donutColor)) { //Hex color for donut
            qualSubjectVO.setImagePath("grn".equalsIgnoreCase(donutColor)? "#00b260": "red".equalsIgnoreCase(donutColor)? "#FC7268": "org".equalsIgnoreCase(donutColor)? "#FF8B00": "");
          }
          qualSubjectArrayList.add(qualSubjectVO);
        }
        employementInfoVO.setQualSubjectList(qualSubjectArrayList);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (qualSubjectDetails != null) {
          qualSubjectDetails.close();
          qualSubjectDetails = null;
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    return employementInfoVO;
  }
}
