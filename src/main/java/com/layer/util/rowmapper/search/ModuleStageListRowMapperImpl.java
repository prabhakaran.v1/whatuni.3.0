package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.valueobjects.ModuleVO;

public class ModuleStageListRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    ModuleVO moduleVO = new ModuleVO();
    moduleVO.setModuleGroupId(resultSet.getString("module_group_id"));
    moduleVO.setModuleId(resultSet.getString("module_id"));
    moduleVO.setModuleTitle(resultSet.getString("module_title"));
    moduleVO.setCredits(resultSet.getString("credits"));
    moduleVO.setModuleTypeName(resultSet.getString("module_type_name"));
    moduleVO.setModuleGroupDesc(resultSet.getString("module_group_description"));
    moduleVO.setModuleDescriptionFlag(resultSet.getString("module_description_flag"));
    return moduleVO;
  }
}
