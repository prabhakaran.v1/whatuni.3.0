package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.search.ProviderResultPageVO;

import com.wuni.util.seo.SeoUrls;

public class ProviderResultsRowMapper implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    ProviderResultPageVO providerResultPageVO = new ProviderResultPageVO();    
    SeoUrls seoUrl = new SeoUrls();
    providerResultPageVO.setCourseId(resultset.getString("COURSE_ID"));
    providerResultPageVO.setCourseDescription(resultset.getString("COURSE_DESC"));
    providerResultPageVO.setCourseTitle(resultset.getString("COURSE_TITLE"));
    providerResultPageVO.setCourseDuration(resultset.getString("DURATION"));
    providerResultPageVO.setLearndDirQual(resultset.getString("LEARNDIR_QUAL"));
//  providerResultPageVO.setCostDescription(resultset.getString("COST_DESC"));
    providerResultPageVO.setCollegeId(resultset.getString("COLLEGE_ID"));
    providerResultPageVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
    providerResultPageVO.setWebsite(resultset.getString("WEBSITE"));
    providerResultPageVO.setProfile(resultset.getString("PROFILE"));
    providerResultPageVO.setVenueName(resultset.getString("VENUE_NAME"));
    providerResultPageVO.setPostcode(resultset.getString("POSTCODE"));
    providerResultPageVO.setUcasCode(resultset.getString("UCAS_CODE"));
    providerResultPageVO.setUcasFlag(resultset.getString("UCAS_FLAG"));
    providerResultPageVO.setUcasPoint(resultset.getString("UCAS_TARIFF")); 
    providerResultPageVO.setStudyMode(resultset.getString("STUDY_MODE"));
    providerResultPageVO.setWasThisCourseShortListed(resultset.getString("COURSE_IN_BASKET"));
    providerResultPageVO.setScholarshipCount(resultset.getString("SCHOLORSHIP_COUNT"));
    providerResultPageVO.setSeoStudyLevelText(resultset.getString("SEO_STUDY_LEVEL_TEXT"));
    providerResultPageVO.setWebsitePurchaseFlag(resultset.getString("WEBSITE_PUR_FLAG"));
    providerResultPageVO.setProviderUrl(resultset.getString("PROVIDER_URL"));
    providerResultPageVO.setDepartmentOrFaculty(resultset.getString("DEPARTMENT_OR_FACULTY"));
    providerResultPageVO.setAvailableStudyModes(resultset.getString("AVAILABLE_STUDY_MODES"));
    providerResultPageVO.setFees(resultset.getString("search_course_fee"));  
    providerResultPageVO.setFeeDuration(resultset.getString("fee_duration"));
    providerResultPageVO.setPlacesAvailable(resultset.getString("places_available"));
    providerResultPageVO.setLastUpdatedDate(resultset.getString("last_updated_date"));
    
    //providerResultPageVO.setCourseDetailUrl(seoUrl.construnctCourseDetailsPageURL(providerResultPageVO.getSeoStudyLevelText(), providerResultPageVO.getCourseTitle(), providerResultPageVO.getCourseId(), providerResultPageVO.getCollegeId(), "COURSE_SPECIFIC"));
    //
    //Button Details
    ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("ENQUIRY_DETAILS");
    try{
      if(enquiryDetailsRS != null){
        while(enquiryDetailsRS.next()){
          providerResultPageVO.setOrderItemId(enquiryDetailsRS.getString("ORDER_ITEM_ID"));                
          providerResultPageVO.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
          providerResultPageVO.setSubOrderWebsite(enquiryDetailsRS.getString("WEBSITE"));                
          providerResultPageVO.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
          providerResultPageVO.setSubOrderEmailWebform(enquiryDetailsRS.getString("EMAIL_WEBFORM"));
          providerResultPageVO.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
          providerResultPageVO.setSubOrderProspectusWebform(enquiryDetailsRS.getString("PROSPECTUS_WEBFORM"));       
          providerResultPageVO.setHotLineNo(enquiryDetailsRS.getString("hotline"));
          providerResultPageVO.setNetworkId(enquiryDetailsRS.getString("network_id"));
          providerResultPageVO.setWebformPrice(enquiryDetailsRS.getString("webform_price"));
          providerResultPageVO.setWebsitePrice(enquiryDetailsRS.getString("website_price"));
        }
      }        
    } catch(Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if(enquiryDetailsRS != null) {
          enquiryDetailsRS.close();
        }
      } catch(Exception e) {
        throw new SQLException(e.getMessage());
      }  
    }
    return providerResultPageVO; 
  }
}
