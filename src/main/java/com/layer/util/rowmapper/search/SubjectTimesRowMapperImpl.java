package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.search.TimesRankingVO;
import WUI.utilities.CommonFunction;

public class SubjectTimesRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNmum) throws SQLException {
    TimesRankingVO timesVO = new TimesRankingVO();
    timesVO.setSubjectName(rs.getString("tr_subject_name"));
    timesVO.setCurrentRanking(rs.getString("current_ranking"));  
    timesVO.setRankOrdinal(new CommonFunction().getOrdinalFor(Integer.parseInt(timesVO.getCurrentRanking())));
    timesVO.setTotalRanking(rs.getString("total_ranking"));
   return timesVO;
  }   
}

