package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.search.OmnitureLoggingVO;

/**
 * this row mapper used to map refine by pod's content detalis
 */
public class OmnitureLoggingRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    OmnitureLoggingVO omnitureLoggingVO = new OmnitureLoggingVO();
    omnitureLoggingVO.setCollegeIdsForThisSearch(resultset.getString("college_ids"));
    return omnitureLoggingVO;
  }

}
