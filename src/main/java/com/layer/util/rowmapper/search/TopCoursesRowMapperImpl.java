package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.search.TopCoursesVO;

public class TopCoursesRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    TopCoursesVO topCoursesVO = new TopCoursesVO();
    SeoUrls seoUrl = new SeoUrls();
    topCoursesVO.setCollegeId(resultset.getString("college_id"));
    topCoursesVO.setCollegeName(resultset.getString("college_name"));
    topCoursesVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    topCoursesVO.setCourseId(resultset.getString("course_id"));
    topCoursesVO.setCourseTitle(resultset.getString("course_title"));
    topCoursesVO.setStudyLevelDesc(resultset.getString("study_level_desc"));
    topCoursesVO.setCourseDetailsPageURL(seoUrl.construnctCourseDetailsPageURL(topCoursesVO.getStudyLevelDesc(), topCoursesVO.getCourseTitle(), topCoursesVO.getCourseId(), topCoursesVO.getCollegeId(), resultset.getString("p_page_name")));
    topCoursesVO.setWhatUniSays(resultset.getString("what_the_uni_say"));
    topCoursesVO.setWebsitePrice(resultset.getString("website_price"));
    topCoursesVO.setWebformPrice(resultset.getString("webform_price"));
    topCoursesVO.setShortListFlag(resultset.getString("course_in_basket"));
    topCoursesVO.setGaCollegeName(new CommonFunction().replaceSpecialCharacter(topCoursesVO.getCollegeName()));
    ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("enquiry_details");
    try{
      if(enquiryDetailsRS != null){
        while(enquiryDetailsRS.next()){
          String tmpUrl = null;
          topCoursesVO.setOrderItemId(enquiryDetailsRS.getString("order_item_id"));
          topCoursesVO.setMyhcProfileId(enquiryDetailsRS.getString("myhc_profile_id"));
          topCoursesVO.setSubOrderItemId(enquiryDetailsRS.getString("suborder_item_id"));
          topCoursesVO.setProfileType(enquiryDetailsRS.getString("profile_type"));
          topCoursesVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("network_id"));
          topCoursesVO.setAdvertName(enquiryDetailsRS.getString("advert_name"));
          topCoursesVO.setSubOrderEmail(enquiryDetailsRS.getString("email"));
          topCoursesVO.setSubOrderProspectus(enquiryDetailsRS.getString("prospectus"));
          tmpUrl = enquiryDetailsRS.getString("website");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          topCoursesVO.setSubOrderWebsite(tmpUrl);
          tmpUrl = enquiryDetailsRS.getString("email_webform");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          topCoursesVO.setSubOrderEmailWebform(tmpUrl);
          tmpUrl = enquiryDetailsRS.getString("prospectus_webform");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          topCoursesVO.setSubOrderProspectusWebform(tmpUrl);
          topCoursesVO.setMediaPath(enquiryDetailsRS.getString("media_path"));
          topCoursesVO.setMediaThumbPath(enquiryDetailsRS.getString("thumb_media_path"));
          topCoursesVO.setMediaType(enquiryDetailsRS.getString("media_type"));
          topCoursesVO.setMediaId(enquiryDetailsRS.getString("media_id"));
          if("VIDEO".equalsIgnoreCase(topCoursesVO.getMediaType())){
            //topCoursesVO.setVideoThumbPath(new GlobalFunction().videoThumbnailFormatChange(topCoursesVO.getMediaPath(), "2"));
            String thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(topCoursesVO.getMediaId(), "2"), rowNum); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path.
            topCoursesVO.setVideoThumbPath(thumbnailPath);
            
          }
          topCoursesVO.setHotLineNo(enquiryDetailsRS.getString("hotline"));
        }
      }
    }catch(Exception e){
      e.printStackTrace();
    }finally{
      try{
        enquiryDetailsRS.close();
      }catch(Exception e){
        throw new SQLException(e.getMessage());
      }
    }
    if("CLEARING_COURSE".equalsIgnoreCase(resultset.getString("p_page_name"))  || "CLEARING".equalsIgnoreCase(resultset.getString("p_page_name") )){
      topCoursesVO.setUniHomeURL((seoUrl.construnctUniHomeURL(topCoursesVO.getCollegeId(), topCoursesVO.getCollegeName()).toLowerCase())+ "?clearing" );
    }else{
      topCoursesVO.setUniHomeURL(seoUrl.construnctUniHomeURL(topCoursesVO.getCollegeId(), topCoursesVO.getCollegeName()).toLowerCase());
    }

    return topCoursesVO;
  }

}
