package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.search.CourseDetailsVO;

public class CourseDetailsDsktpRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    CourseDetailsVO courseDetailsVO = new CourseDetailsVO();
    SeoUrls seoUrl = new SeoUrls();
    //
    courseDetailsVO.setCollegeId(resultset.getString("college_id"));
    courseDetailsVO.setCourseSummaryFull(resultset.getString("course_summary"));
    courseDetailsVO.setOppotunityId(resultset.getString("opportunity_id"));
    courseDetailsVO.setStudyModeId(resultset.getString("study_mode_id"));
    courseDetailsVO.setStudyModes(resultset.getString("description"));
    courseDetailsVO.setDurations(resultset.getString("duration_desc"));
    courseDetailsVO.setCourseTitle(resultset.getString("course_title"));
    courseDetailsVO.setStudyLevelCode(resultset.getString("study_level_code"));
    courseDetailsVO.setUcasCode(resultset.getString("ucas_code"));
    courseDetailsVO.setCourseId(resultset.getString("course_id"));
    courseDetailsVO.setCollegeName(resultset.getString("college_name"));
    courseDetailsVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    courseDetailsVO.setCollegeLocation(resultset.getString("college_location"));
    courseDetailsVO.setQualification(resultset.getString("learn_dir_qual"));
    courseDetailsVO.setUkFees(resultset.getString("dom_price"));
    courseDetailsVO.setStartDate(resultset.getString("start_date"));
    courseDetailsVO.setWasThisCourseShortlisted(resultset.getString("course_in_basket"));
    courseDetailsVO.setUniHomeUrl(seoUrl.construnctUniHomeURL(courseDetailsVO.getCollegeId(), courseDetailsVO.getCollegeName()));
    courseDetailsVO.setAccreditedBy(resultset.getString("accreditation"));
    courseDetailsVO.setAppyNowURL(resultset.getString("apply_now_url"));
    courseDetailsVO.setApplyNowURLCount(resultset.getString("apply_now_url_count"));
    courseDetailsVO.setScholarshipCount(resultset.getString("scholarship_count"));
    courseDetailsVO.setClearingPlacesAvail(resultset.getString("places_available"));
    courseDetailsVO.setCityGuideDispalyFlag(resultset.getString("city_guide_display_flag"));
    courseDetailsVO.setHeadline(resultset.getString("headline"));
    courseDetailsVO.setCityGuideLocation(resultset.getString("location"));    
    return courseDetailsVO;
  }
}
