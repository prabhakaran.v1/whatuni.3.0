package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.search.UniStatsInfoVO;

public class UniStatsInfoRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    UniStatsInfoVO uniStatsInfoVO = new UniStatsInfoVO();
    //
    SeoUrls seoUrl = new SeoUrls();
    uniStatsInfoVO.setCollegeId(resultset.getString("college_id"));
    uniStatsInfoVO.setCollegeName(resultset.getString("college_name"));
    uniStatsInfoVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    uniStatsInfoVO.setUniHomeURL(seoUrl.construnctUniHomeURL(uniStatsInfoVO.getCollegeId(), uniStatsInfoVO.getCollegeName()));
    uniStatsInfoVO.setTimesRank(resultset.getString("times_ranking"));
    if(!GenericValidator.isBlankOrNull(uniStatsInfoVO.getTimesRank())){
      uniStatsInfoVO.setRankOrdinal(new CommonFunction().getOrdinalFor(Integer.parseInt(uniStatsInfoVO.getTimesRank())));
    }
    uniStatsInfoVO.setFullPartTime(resultset.getString("full_part_time"));
    uniStatsInfoVO.setUkInternation(resultset.getString("home_international_students"));
    uniStatsInfoVO.setSchoolMature(resultset.getString("mature_non_mature_students"));
    uniStatsInfoVO.setNoOfStudents(resultset.getString("no_of_students"));
    uniStatsInfoVO.setJobOrWork(resultset.getString("work_or_study"));
    uniStatsInfoVO.setUgpg(resultset.getString("under_post_graduate"));
    return uniStatsInfoVO;
  }

}
