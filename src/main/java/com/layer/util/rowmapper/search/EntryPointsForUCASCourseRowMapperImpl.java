package com.layer.util.rowmapper.search;
//Changed column names for new entry requirments structure by Hema.S on 06-june-2018
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.search.EntryPointsUCASCourseVO;

public class EntryPointsForUCASCourseRowMapperImpl implements RowMapper{
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
    EntryPointsUCASCourseVO entryPointsUCASCourseVO = new EntryPointsUCASCourseVO();
    entryPointsUCASCourseVO.setOpportunityId(rs.getString("opportunity_id"));
    entryPointsUCASCourseVO.setEntryDesc(rs.getString("entry_desc"));
    entryPointsUCASCourseVO.setEntryPoints(rs.getString("entry_points"));
    entryPointsUCASCourseVO.setAdditionalInfo(rs.getString("additional_info"));
    return entryPointsUCASCourseVO;
  }
}