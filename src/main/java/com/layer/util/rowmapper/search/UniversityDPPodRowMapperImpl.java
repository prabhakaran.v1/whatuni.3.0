package com.layer.util.rowmapper.search;
//wu411_20120612
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.UniversityDPPodVo;

public class UniversityDPPodRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNUm) throws SQLException{
        UniversityDPPodVo universityDP = new UniversityDPPodVo();
        universityDP.setSubOrderItemId(rs.getString("suborder_item_id"));
        universityDP.setMediaPath(rs.getString("media_path"));
        universityDP.setDpFlag(rs.getString("dp_flag"));
        universityDP.setDpteasertext(rs.getString("prospectus_teaser_text"));
        universityDP.setProspectusType(rs.getString("prospectus_type"));
        return universityDP;
    }
}
