package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;

import com.wuni.util.CommonValidator;
import com.wuni.util.valueobject.search.CourseOpertunityVO;

public class OppertunityInfoRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    CourseOpertunityVO courseOpertunityVO = new CourseOpertunityVO();
    CommonValidator validate = new CommonValidator();
    String contactProvider = "Contact provider";
    //
    courseOpertunityVO.setCollegeId(resultset.getString("college_id"));
    courseOpertunityVO.setCourseId(resultset.getString("course_id"));
    courseOpertunityVO.setDomesticPrice(resultset.getString("dom_price") != null ? resultset.getString("dom_price") : contactProvider);
    courseOpertunityVO.setDomesticPriceDesc(resultset.getString("dom_price_desc") != null ? resultset.getString("dom_price_desc") : "");
    courseOpertunityVO.setStudyMode(resultset.getString("study_mode") != null ? resultset.getString("study_mode") : contactProvider);
    courseOpertunityVO.setDuration(resultset.getString("duration") != null ? resultset.getString("duration") : contactProvider);
    courseOpertunityVO.setDurationDesc(resultset.getString("duration_desc") != null ? resultset.getString("duration_desc") : "");
    courseOpertunityVO.setStartDate(resultset.getString("start_date") != null ? resultset.getString("start_date") : contactProvider);
    courseOpertunityVO.setStartDateDesc(resultset.getString("start_date_desc") != null ? resultset.getString("start_date_desc") : "");
    courseOpertunityVO.setIsVenueBelongsToLondonBorough(resultset.getString("is_venue_in_london_borough"));
    courseOpertunityVO.setApplyNowUrl(resultset.getString("apply_now_url"));
    String tmpUrl;
    if (validate.isNotBlankAndNotNull(courseOpertunityVO.getApplyNowUrl())) {
      tmpUrl = courseOpertunityVO.getApplyNowUrl();
        tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
      courseOpertunityVO.setApplyNowUrl(tmpUrl);
    }
    courseOpertunityVO.setApplyNowUrlCount(resultset.getString("apply_now_url_count"));
    courseOpertunityVO.setClearingPlacesAvaiable(resultset.getString("places_available"));
    courseOpertunityVO.setLastUpdatedDate(resultset.getString("last_updated_date"));
    //
    return courseOpertunityVO;
  }

}
