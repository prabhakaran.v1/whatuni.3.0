package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.search.CpeWorstPerformersVO;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;

public class CpeWorstPerformersRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet cpeWorstPerformersRS, int rowNum) throws SQLException {
  
    CpeWorstPerformersVO cpeWorstPerformersVO = new CpeWorstPerformersVO();
    cpeWorstPerformersVO.setCollegeId(cpeWorstPerformersRS.getString("COLLEGE_ID"));
    cpeWorstPerformersVO.setCollegeName(cpeWorstPerformersRS.getString("COLLEGE_NAME"));
    cpeWorstPerformersVO.setCollegeNameDisplay(cpeWorstPerformersRS.getString("COLLEGE_NAME_DISPLAY"));
    cpeWorstPerformersVO.setLdcs1Code(cpeWorstPerformersRS.getString("LDCS1_CODE"));
    cpeWorstPerformersVO.setLdcs1Description(cpeWorstPerformersRS.getString("LDCS1_DESCRIPTION"));     
    if(cpeWorstPerformersVO.getLdcs1Description() != null){
      cpeWorstPerformersVO.setHashRemovedLdcs1Description(cpeWorstPerformersVO.getLdcs1Description().replaceAll("#","-"));
    }
    cpeWorstPerformersVO.setQualificationCode(cpeWorstPerformersRS.getString("QUALIFICATION_CODE"));
    cpeWorstPerformersVO.setQualificationDescription(cpeWorstPerformersRS.getString("QUALIFICATION_DESCRIPTION"));
    
    ResultSet cpeWorstPerformersEnquiryDetailsRS = (ResultSet)cpeWorstPerformersRS.getObject("ENQUIRY_DETAILS");
    try{
      if(cpeWorstPerformersEnquiryDetailsRS != null){
        while(cpeWorstPerformersEnquiryDetailsRS.next()){
          cpeWorstPerformersVO.setOrderItemId(cpeWorstPerformersEnquiryDetailsRS.getString("ORDER_ITEM_ID"));
          cpeWorstPerformersVO.setSubOrderItemId(cpeWorstPerformersEnquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
          cpeWorstPerformersVO.setMyhcProfileId(cpeWorstPerformersEnquiryDetailsRS.getString("MYHC_PROFILE_ID"));
          cpeWorstPerformersVO.setProfileType(cpeWorstPerformersEnquiryDetailsRS.getString("PROFILE_TYPE"));        
          cpeWorstPerformersVO.setSubOrderEmail(cpeWorstPerformersEnquiryDetailsRS.getString("EMAIL"));
          cpeWorstPerformersVO.setSubOrderProspectus(cpeWorstPerformersEnquiryDetailsRS.getString("PROSPECTUS"));
          cpeWorstPerformersVO.setCpeQualificationNetworkId(cpeWorstPerformersEnquiryDetailsRS.getString("NETWORK_ID"));
          
          String tmpUrl = cpeWorstPerformersEnquiryDetailsRS.getString("WEBSITE");
          if(tmpUrl != null && tmpUrl.trim().length() > 0){
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else { tmpUrl = ""; }
          cpeWorstPerformersVO.setSubOrderWebsite(tmpUrl); 
          
          tmpUrl =  cpeWorstPerformersEnquiryDetailsRS.getString("EMAIL_WEBFORM");
          if(tmpUrl != null && tmpUrl.trim().length() > 0){
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else { tmpUrl = ""; }
          cpeWorstPerformersVO.setSubOrderEmailWebform(tmpUrl); 
          
          tmpUrl = cpeWorstPerformersEnquiryDetailsRS.getString("PROSPECTUS_WEBFORM");  
          if(tmpUrl != null && tmpUrl.trim().length() > 0){
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else { tmpUrl = ""; }
          cpeWorstPerformersVO.setSubOrderProspectusWebform(tmpUrl); 
          
          //collecting media informations
          cpeWorstPerformersVO.setMediaId(cpeWorstPerformersEnquiryDetailsRS.getString("MEDIA_ID"));
          cpeWorstPerformersVO.setMediaPath(cpeWorstPerformersEnquiryDetailsRS.getString("MEDIA_PATH"));
          cpeWorstPerformersVO.setMediaType(cpeWorstPerformersEnquiryDetailsRS.getString("MEDIA_TYPE"));
          cpeWorstPerformersVO.setMediaThumbPath(cpeWorstPerformersEnquiryDetailsRS.getString("THUMB_MEDIA_PATH"));
          if(cpeWorstPerformersVO.getMediaId() != null && cpeWorstPerformersVO.getMediaPath() != null && cpeWorstPerformersVO.getMediaType() != null ){              
            if (cpeWorstPerformersVO.getMediaType().equalsIgnoreCase("video")) {
              //cpeWorstPerformersVO.setMediaThumbPath(new GlobalFunction().videoThumbnailFormatChange(cpeWorstPerformersVO.getMediaPath(),"3"));           
              String thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(cpeWorstPerformersVO.getMediaId(), "4"), rowNum); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path.
              cpeWorstPerformersVO.setMediaThumbPath(thumbnailPath);
            } else if (cpeWorstPerformersVO.getMediaType().equalsIgnoreCase("picture")) {       
    //                if(cpeWorstPerformersVO.getMediaPath().indexOf("http://") > -1 ){
    //                  cpeWorstPerformersVO.setMediaPath(cpeWorstPerformersVO.getMediaPath());
    //                } else {
    //                  cpeWorstPerformersVO.setMediaPath(new CommonFunction().getWUSysVarValue("IMAGE_DISPLAY_PATH") + "college/image/" + cpeWorstPerformersVO.getCollegeId() + "/" + cpeWorstPerformersVO.getMediaPath());
    //                }
           } else {
             cpeWorstPerformersVO.setMediaPath("/img/whatuni/unihome_default.png");
           }
          }else{
            cpeWorstPerformersVO.setMediaId("-999");
            cpeWorstPerformersVO.setMediaPath("/img/whatuni/unihome_default.png");
            cpeWorstPerformersVO.setMediaType("PICTURE");
          }
        }
      }        
    } catch(Exception e) {
        e.printStackTrace();
    } finally {
      try {
        if(cpeWorstPerformersEnquiryDetailsRS != null){
          cpeWorstPerformersEnquiryDetailsRS.close();
        }
      } catch(Exception e) {
        e.printStackTrace();
      }  
    }      
    return cpeWorstPerformersVO;
  
  }
}
