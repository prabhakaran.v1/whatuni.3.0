package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.valueobjects.ModuleVO;

public class ModuleListRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        ModuleVO moduleVO = new ModuleVO();
        moduleVO.setModuleGroupId(resultSet.getString("module_group_id"));
        moduleVO.setStageLabelId(resultSet.getString("stage_label_id"));
        moduleVO.setCourseId(resultSet.getString("course_id"));
        moduleVO.setStageLabelName(resultSet.getString("stage_label_name"));
        moduleVO.setModules(resultSet.getString("modules"));
        moduleVO.setModuleGroup(resultSet.getString("module_group"));
        return moduleVO;
      }  
}
