package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.search.RefineByOptionsVO;

/**
 * this row mapper used to map refine by pod's content detalis
 */
public class RefineByOptionsRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    RefineByOptionsVO refineByOptionsVO = new RefineByOptionsVO();
    refineByOptionsVO.setRefineOrder(resultset.getString("REFINE_ORDER"));
    refineByOptionsVO.setRefineCode(resultset.getString("REFINE_CODE"));
    refineByOptionsVO.setRefineDesc(resultset.getString("REFINE_DESC"));
    refineByOptionsVO.setCategoryId(resultset.getString("CATEGORY_ID"));
    refineByOptionsVO.setCategoryCode(resultset.getString("CATEGORY_CODE"));
    refineByOptionsVO.setDisplaySequence(resultset.getString("DISP_SEQ"));
    refineByOptionsVO.setParentCountryName(resultset.getString("PARENT_COUNTRY_NAME"));
    refineByOptionsVO.setNumberOfChildLocations(resultset.getString("CHILD_LOCATION_COUNT"));
    return refineByOptionsVO;
  }

}
