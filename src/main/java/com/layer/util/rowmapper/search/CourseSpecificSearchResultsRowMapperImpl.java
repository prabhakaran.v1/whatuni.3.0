package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.seo.SeoUrlsForAffiliateSiteLinks;
import com.wuni.util.valueobject.ClearingInfoVO;
import com.wuni.util.valueobject.search.BestMatchCoursesVO;
import com.wuni.util.valueobject.search.CourseSpecificSearchResultsVO;

public class CourseSpecificSearchResultsRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    CourseSpecificSearchResultsVO courseSpecificSearchResultsVO = new CourseSpecificSearchResultsVO();
    SeoUrls seoUrl = new SeoUrls();
    courseSpecificSearchResultsVO.setCollegeId(resultset.getString("COLLEGE_ID"));
    courseSpecificSearchResultsVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
    courseSpecificSearchResultsVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
    courseSpecificSearchResultsVO.setUniHomeUrl(seoUrl.construnctUniHomeURL(courseSpecificSearchResultsVO.getCollegeId(), courseSpecificSearchResultsVO.getCollegeName()));
   
    //courseSpecificSearchResultsVO.setProviderResultPageUrl(seoUrl.construnctUniHomeURL(courseSpecificSearchResultsVO.getCollegeId(), courseSpecificSearchResultsVO.getCollegeName()));
    courseSpecificSearchResultsVO.setNumberOfCoursesForThisCollege(resultset.getString("HITS"));
   // courseSpecificSearchResultsVO.setLearndirQualsForThisCollege(resultset.getString("LEARNDIR_QUALS")); //Commented based on Rams request for improving performance for 21-Jan-2014
    courseSpecificSearchResultsVO.setWasThisCollegeShortlisted((resultset.getString("COLLEGE_IN_BASKET")));
    courseSpecificSearchResultsVO.setCollegeLogoPath(resultset.getString("COLLEGE_LOGO"));
    courseSpecificSearchResultsVO.setScholarShipCnt(resultset.getString("SCH_CNT"));
    courseSpecificSearchResultsVO.setScholarShipCount(resultset.getString("scholarship_count"));
    courseSpecificSearchResultsVO.setOverallRating(resultset.getString("overall_rating"));
    //wu318_20111020_Syed: fetching best match courses for PANADA effect
    ResultSet bestMatchCoursesRS = (ResultSet)resultset.getObject("BEST_MATCH_COURSES");
    try {
      if (bestMatchCoursesRS != null) {
        ArrayList bestMatchCoursesList = new ArrayList();
        BestMatchCoursesVO bestMatchCoursesVO = null;
        String tmpUrl = null;
        while (bestMatchCoursesRS.next()) {
          bestMatchCoursesVO = new BestMatchCoursesVO();
          bestMatchCoursesVO.setCourseId(bestMatchCoursesRS.getString("COURSE_ID"));
          bestMatchCoursesVO.setCourseTitle(bestMatchCoursesRS.getString("COURSE_TITLE"));
          bestMatchCoursesVO.setCourseUcasTariff(bestMatchCoursesRS.getString("UCAS_TARIFF"));
          bestMatchCoursesVO.setCourseDomesticFees(bestMatchCoursesRS.getString("COURSE_DOM_FEES"));
          bestMatchCoursesVO.setWasThisCourseShortlisted(bestMatchCoursesRS.getString("COURSE_IN_BASKET"));
          bestMatchCoursesVO.setStudyLevelDesc(bestMatchCoursesRS.getString("SEO_STUDY_LEVEL_TEXT"));  
          bestMatchCoursesVO.setStudyLevelDescDet(bestMatchCoursesRS.getString("STUDY_LEVEL_DESC"));
          bestMatchCoursesVO.setUcasCode(bestMatchCoursesRS.getString("UCAS_CODE"));
          bestMatchCoursesVO.setSubOrderItemId(bestMatchCoursesRS.getString("SUBORDER_ITEM_ID"));
          bestMatchCoursesVO.setOrderItemId(bestMatchCoursesRS.getString("ORDER_ITEM_ID"));
          bestMatchCoursesVO.setSubOrderEmail(bestMatchCoursesRS.getString("EMAIL"));
          bestMatchCoursesVO.setSubOrderProspectus(bestMatchCoursesRS.getString("PROSPECTUS"));
          bestMatchCoursesVO.setFeeDuration(bestMatchCoursesRS.getString("FEE_DURATION"));
          //
          tmpUrl = bestMatchCoursesRS.getString("WEBSITE");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          bestMatchCoursesVO.setSubOrderWebsite(tmpUrl);
          //
          tmpUrl = bestMatchCoursesRS.getString("EMAIL_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          bestMatchCoursesVO.setSubOrderEmailWebform(tmpUrl);
          //
          tmpUrl = bestMatchCoursesRS.getString("PROSPECTUS_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          bestMatchCoursesVO.setSubOrderProspectusWebform(tmpUrl);
          //
          bestMatchCoursesVO.setMyhcProfileId(bestMatchCoursesRS.getString("MYHC_PROFILE_ID"));
          bestMatchCoursesVO.setProfileType(bestMatchCoursesRS.getString("PROFILE_TYPE"));
          bestMatchCoursesVO.setApplyNowFlag(bestMatchCoursesRS.getString("APPLY_NOW_FLAG"));
          bestMatchCoursesVO.setMediaId(bestMatchCoursesRS.getString("MEDIA_ID"));
          bestMatchCoursesVO.setMediaPath(bestMatchCoursesRS.getString("MEDIA_PATH"));
          bestMatchCoursesVO.setMediaType(bestMatchCoursesRS.getString("MEDIA_TYPE"));
          bestMatchCoursesVO.setCpeQualificationNetworkId(bestMatchCoursesRS.getString("NETWORK_ID"));
          bestMatchCoursesVO.setPageName(bestMatchCoursesRS.getString("page_name"));
          bestMatchCoursesVO.setCourseDetailsPageURL(seoUrl.construnctCourseDetailsPageURL(bestMatchCoursesVO.getStudyLevelDesc(), bestMatchCoursesVO.getCourseTitle(), bestMatchCoursesVO.getCourseId(), courseSpecificSearchResultsVO.getCollegeId(), bestMatchCoursesVO.getPageName()));
          bestMatchCoursesVO.setHotLineNo(bestMatchCoursesRS.getString("hotline"));
          bestMatchCoursesVO.setCourseClearingPlaces(bestMatchCoursesRS.getString("places_available"));
          bestMatchCoursesVO.setInstitutionClearingPlaces(bestMatchCoursesRS.getString("tot_places_available"));
          bestMatchCoursesVO.setLastUpdated(bestMatchCoursesRS.getString("last_updated_date"));
          bestMatchCoursesVO.setWebformPrice(bestMatchCoursesRS.getString("webform_price"));
          bestMatchCoursesVO.setWebsitePrice(bestMatchCoursesRS.getString("website_price"));
          //          
          bestMatchCoursesList.add(bestMatchCoursesVO);
        }
        courseSpecificSearchResultsVO.setBestMatchCoursesList(bestMatchCoursesList);
        

      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (bestMatchCoursesRS != null) {
          bestMatchCoursesRS.close();
          bestMatchCoursesRS = null;
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    //wu314_20110712_Syed: Stacey-functional-crd#1 - linking to clearing-micro-site
    ResultSet clearingRS = (ResultSet)resultset.getObject("clearing_info");
    try {
      if (clearingRS != null) {
        ArrayList clearingInfoList = new ArrayList();
        ClearingInfoVO clearingInfoVO = null;
        SeoUrlsForAffiliateSiteLinks affiliateSiteLinks = new SeoUrlsForAffiliateSiteLinks();
        while (clearingRS.next()) {
          clearingInfoVO = new ClearingInfoVO();
          clearingInfoVO.setClearingCollegeId(clearingRS.getString("college_id"));
          clearingInfoVO.setClearingCollegeName(clearingRS.getString("college_name"));
          clearingInfoVO.setClearingProfileCount(clearingRS.getString("clearing_profile_count"));
          if (clearingInfoVO.getClearingProfileCount() != null && (Integer.parseInt(String.valueOf(clearingInfoVO.getClearingProfileCount())) == 1)) {
            clearingInfoVO.setClearingProfileUrl(affiliateSiteLinks.hcPoviderClearingURL(clearingInfoVO.getClearingCollegeId(), clearingInfoVO.getClearingCollegeName()));
          } else {
            clearingInfoVO.setClearingProfileUrl(affiliateSiteLinks.hcClearingHomeURL());
          }
          clearingInfoList.add(clearingInfoVO);
        }
        courseSpecificSearchResultsVO.setClearingInfoList(clearingInfoList);
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (clearingRS != null) {
          clearingRS.close();
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    return courseSpecificSearchResultsVO;
  }

}
