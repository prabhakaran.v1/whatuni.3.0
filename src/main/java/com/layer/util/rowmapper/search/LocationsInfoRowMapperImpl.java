package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.search.LocationsInfoVO;

public class LocationsInfoRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    LocationsInfoVO locationsInfoVO = new LocationsInfoVO();
    //
    locationsInfoVO.setLatitude(resultset.getString("latitude"));
    locationsInfoVO.setLongitude(resultset.getString("longitude"));
    locationsInfoVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    locationsInfoVO.setTown(resultset.getString("town"));
    locationsInfoVO.setPostcode(resultset.getString("postcode"));
    locationsInfoVO.setNearestStationName(resultset.getString("nearest_train_stn"));
    locationsInfoVO.setDistanceFromNearestStation(resultset.getString("distance_from_train_stn"));
    locationsInfoVO.setAddLine1(resultset.getString("add_line_1"));
    locationsInfoVO.setAddLine2(resultset.getString("add_line_2"));
    locationsInfoVO.setCountryState(resultset.getString("country_state"));
    locationsInfoVO.setIsInLondon(resultset.getString("is_in_london"));
    locationsInfoVO.setNearestTubeStation(resultset.getString("nearest_tube_stn"));
    locationsInfoVO.setDistanceFromTubeStation(resultset.getString("distance_from_tube_stn"));
    //
    return locationsInfoVO;
  }

}
