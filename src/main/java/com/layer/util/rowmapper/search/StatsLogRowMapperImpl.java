package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.search.RefineByOptionsVO;

/**
 * this row mapper used to map refine by pod's content detalis
 */
 public class StatsLogRowMapperImpl implements RowMapper {
   public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
     RefineByOptionsVO refineVO = new RefineByOptionsVO();
     refineVO.setBrowseCatFlag(resultSet.getString("browse_cat_flag"));
     refineVO.setBrowseCatId(resultSet.getString("browse_cat_id"));
     refineVO.setBrowseCatDesc(resultSet.getString("browse_cat_desc"));
     refineVO.setBrowseCatCode(resultSet.getString("browse_cat_code"));
     refineVO.setStudyModeDesc(resultSet.getString("study_mode_desc"));
     refineVO.setCollegeId(resultSet.getString("college_id"));     
     refineVO.setCollegeName(resultSet.getString("college_name"));
     refineVO.setCollegeNameDisplay(resultSet.getString("college_name_display"));
     refineVO.setCollegeLocation(resultSet.getString("location"));     
     return refineVO;
   }
 }
