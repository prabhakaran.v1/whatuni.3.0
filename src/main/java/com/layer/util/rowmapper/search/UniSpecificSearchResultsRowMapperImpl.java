package com.layer.util.rowmapper.search;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;

import com.wuni.util.valueobject.search.UniSpecificSearchResultsVO;

public class UniSpecificSearchResultsRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    UniSpecificSearchResultsVO uniSpecificSearchResultsVO = new UniSpecificSearchResultsVO();
    uniSpecificSearchResultsVO.setCollegeId(rs.getString("COLLEGE_ID"));
    uniSpecificSearchResultsVO.setCollegeName(rs.getString("college_name"));
    uniSpecificSearchResultsVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
    uniSpecificSearchResultsVO.setNumberOfCoursesForThisCollege(rs.getString("HITS"));
    uniSpecificSearchResultsVO.setOverallRating(rs.getString("OVERALL_RATING"));
    uniSpecificSearchResultsVO.setReviewCount(rs.getString("REVIEW_COUNT"));    
    uniSpecificSearchResultsVO.setNextOpenDay(rs.getString("NEXT_COL_OPENDAY"));
    uniSpecificSearchResultsVO.setRegionName(rs.getString("REGION_NAME"));
    uniSpecificSearchResultsVO.setCollegeLogo(rs.getString("COLLEGE_LOGO"));
    uniSpecificSearchResultsVO.setThumbNo(rs.getString("thumb_no"));
    uniSpecificSearchResultsVO.setCollegeLocation(rs.getString("college_location"));
    uniSpecificSearchResultsVO.setWasThisCollegeShortlisted(rs.getString("college_in_basket"));    
    
    String regionName = uniSpecificSearchResultsVO.getRegionName();
    if (regionName != null && regionName.trim().length() > 0) {
      if (regionName.equalsIgnoreCase("NORTHERN ENGLAND")) {
        uniSpecificSearchResultsVO.setRegionSpecificUkMapCssClass("NEmap");
      } else if (regionName.equalsIgnoreCase("SOUTHERN ENGLAND")) {
        uniSpecificSearchResultsVO.setRegionSpecificUkMapCssClass("SEmap");
      } else if (regionName.equalsIgnoreCase("CENTRAL ENGLAND")) {
        uniSpecificSearchResultsVO.setRegionSpecificUkMapCssClass("CEmap");
      } else if (regionName.equalsIgnoreCase("GREATER LONDON")) {
        uniSpecificSearchResultsVO.setRegionSpecificUkMapCssClass("GLmap");
      } else if (regionName.equalsIgnoreCase("NORTHERN IRELAND")) {
        uniSpecificSearchResultsVO.setRegionSpecificUkMapCssClass("NImap");
      } else if (regionName.equalsIgnoreCase("SCOTLAND")) {
        uniSpecificSearchResultsVO.setRegionSpecificUkMapCssClass("SCmap");
      } else if (regionName.equalsIgnoreCase("WALES")) {
        uniSpecificSearchResultsVO.setRegionSpecificUkMapCssClass("Wmap");
      } else {
        uniSpecificSearchResultsVO.setRegionSpecificUkMapCssClass("Umap"); //for default UK map
      }
    } else {
      uniSpecificSearchResultsVO.setRegionSpecificUkMapCssClass("Umap");
    }
    uniSpecificSearchResultsVO.setTimesRanking(rs.getString("TIMES_RANKING"));
    uniSpecificSearchResultsVO.setTimesRankingSub(rs.getString("TIMES_RANKING_SUBJECT"));
    uniSpecificSearchResultsVO.setCollegeLatitudeAndLongitude(rs.getString("COL_LAT_LONG"));
    String collegeLatLong = uniSpecificSearchResultsVO.getCollegeLatitudeAndLongitude();
    if (collegeLatLong != null && collegeLatLong.trim().length() > 0 && collegeLatLong.indexOf(",") >= 0) {
      String collegeLatLongUrl[] = collegeLatLong.split(",");
      uniSpecificSearchResultsVO.setLattitude(collegeLatLongUrl[0]);
      uniSpecificSearchResultsVO.setLongtitude(collegeLatLongUrl[1]);
    }
    uniSpecificSearchResultsVO.setUniHallFees(rs.getString("UNI_HALL_FEES"));
    uniSpecificSearchResultsVO.setGenderRatio(rs.getString("GENDER_RATIO"));    
    uniSpecificSearchResultsVO.setStudentsJob(rs.getString("WORK_OR_STUDY"));
    //uniSpecificSearchResultsVO.setWhatUniSays(rs.getString("what_the_uni_say"));
    //profileInfo related properties
    ResultSet profileInfoRS = (ResultSet)rs.getObject("PROFILE_INFO");
    try {
      if (profileInfoRS != null) {
        while (profileInfoRS.next()) {
          uniSpecificSearchResultsVO.setOrderItemId(profileInfoRS.getString("ORDER_ITEM_ID"));
          uniSpecificSearchResultsVO.setSubOrderItemId(profileInfoRS.getString("SUB_ORDER_ITEM_ID"));
          uniSpecificSearchResultsVO.setMyhcProfileId(profileInfoRS.getString("MYHC_PROFILE_ID"));
          uniSpecificSearchResultsVO.setAdvertName(profileInfoRS.getString("ADVERT_NAME"));
          uniSpecificSearchResultsVO.setMediaPath(profileInfoRS.getString("MEDIA_PATH"));
          uniSpecificSearchResultsVO.setMediaTypeId(profileInfoRS.getString("MEDIA_TYPE_ID"));
          uniSpecificSearchResultsVO.setMediaId(profileInfoRS.getString("MEDIA_ID"));
          uniSpecificSearchResultsVO.setThumbnailMediaPath(profileInfoRS.getString("THUMBNAIL_MEDIA_PATH"));
          CommonFunction common = new CommonFunction();
          if (uniSpecificSearchResultsVO.getMediaTypeId() != null && uniSpecificSearchResultsVO.getMediaTypeId().equalsIgnoreCase("2")) {
            uniSpecificSearchResultsVO.setSearchThumbnail(common.getSearchVideoThumbPath(uniSpecificSearchResultsVO.getMediaPath(), uniSpecificSearchResultsVO.getThumbNo()));
          } else {
            uniSpecificSearchResultsVO.setSearchThumbnail(uniSpecificSearchResultsVO.getThumbnailMediaPath());
          }
          uniSpecificSearchResultsVO.setInteractivityId(profileInfoRS.getString("INTERACTIVITY_ID"));
          uniSpecificSearchResultsVO.setWhichProfleBoosted(profileInfoRS.getString("BOOSTED_PROFILE_FLAG"));
          uniSpecificSearchResultsVO.setIsIpExists(profileInfoRS.getString("IP_EXISTS_FLAG"));
          uniSpecificSearchResultsVO.setIsSpExists(profileInfoRS.getString("SP_EXISTS_FLAG"));
          uniSpecificSearchResultsVO.setSpOrderItemId(profileInfoRS.getString("SP_ORDER_ITEM_ID"));
          uniSpecificSearchResultsVO.setIpOrderItemId(profileInfoRS.getString("IP_ORDER_ITEM_ID"));
          uniSpecificSearchResultsVO.setMychPrifileIdForIp(profileInfoRS.getString("IP_MYHC_PROFILE_ID"));
          uniSpecificSearchResultsVO.setMychPrifileIdForSp(profileInfoRS.getString("SP_MYHC_PROFILE_ID"));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (profileInfoRS != null) {
          profileInfoRS.close();
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    //enquiryDetails related properties
    ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("ENQUIRY_DETAILS");
    try {
      if(enquiryDetailsRS != null) {
        String tmpUrl = null;
        while (enquiryDetailsRS.next()) {
          tmpUrl = enquiryDetailsRS.getString("WEBSITE");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          uniSpecificSearchResultsVO.setSubOrderWebsite(tmpUrl);
          uniSpecificSearchResultsVO.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
          tmpUrl = enquiryDetailsRS.getString("EMAIL_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          uniSpecificSearchResultsVO.setSubOrderEmailWebform(tmpUrl);
          uniSpecificSearchResultsVO.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
          tmpUrl = enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          uniSpecificSearchResultsVO.setSubOrderProspectusWebform(tmpUrl);
          uniSpecificSearchResultsVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("NETWORK_ID"));
          uniSpecificSearchResultsVO.setHotLineNo(enquiryDetailsRS.getString("hotline"));
          uniSpecificSearchResultsVO.setWebformPrice(enquiryDetailsRS.getString("webform_price"));
          uniSpecificSearchResultsVO.setWebsitePrice(enquiryDetailsRS.getString("website_price"));
          
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if(enquiryDetailsRS != null) {
          enquiryDetailsRS.close();
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    ResultSet latestReviewRs = (ResultSet)rs.getObject("latest_review");
    try {
      if (latestReviewRs != null) {
        while (latestReviewRs.next()) {
          uniSpecificSearchResultsVO.setCollegeId(latestReviewRs.getString("college_id"));
          uniSpecificSearchResultsVO.setReviewId(latestReviewRs.getString("review_id"));
          uniSpecificSearchResultsVO.setReviewText(latestReviewRs.getString("review_text"));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (latestReviewRs != null) {
          latestReviewRs.close();
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    return uniSpecificSearchResultsVO;
  }
}
