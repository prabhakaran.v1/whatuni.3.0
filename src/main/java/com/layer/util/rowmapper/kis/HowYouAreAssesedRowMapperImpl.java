package com.layer.util.rowmapper.kis;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;

import com.wuni.util.valueobject.search.CourseKISDataVO;

  public class HowYouAreAssesedRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
      CourseKISDataVO kisDataVO = new CourseKISDataVO();
      String tmpUrl = null;
      kisDataVO.setCourseWork(rs.getString("avg_pct_coursework"));
      kisDataVO.setPracticalWork(rs.getString("avg_pct_practical_work"));
      kisDataVO.setExam(rs.getString("exam"));
      tmpUrl = rs.getString("learning_and_teaching_url");
      kisDataVO.setLearTeachingURL(new CommonFunction().appendingHttptoURL(tmpUrl));
      tmpUrl = rs.getString("support_url");
      kisDataVO.setFinanceSupportURL(new CommonFunction().appendingHttptoURL(tmpUrl));
      kisDataVO.setAccomodationLowerPrice(rs.getString("institution_accom_lower"));
      kisDataVO.setAccomodationHigherPrice(rs.getString("institution_accom_upper"));
      kisDataVO.setUkFees(rs.getString("uk_fees"));
      kisDataVO.setIntlFees(rs.getString("intl_fees"));
      kisDataVO.setFeeWaiver(rs.getString("fee_waiver"));
      kisDataVO.setMeansSupport(rs.getString("means_support"));
      kisDataVO.setUkFeesDesc(rs.getString("uk_fee_desc"));
      kisDataVO.setIntFeesDesc(rs.getString("intl_fee_desc"));
      return kisDataVO;
    }
  }


