package com.layer.util.rowmapper.kis;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.search.CourseKISDataVO;
 
public class OpportunitiesListRowMapper implements RowMapper {

  public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
    CourseKISDataVO oppListVO = new CourseKISDataVO();
    oppListVO.setOpportunityId(rs.getString("opportunity_id"));
    oppListVO.setOpportunityDesc(rs.getString("opportunity"));
    oppListVO.setStudyModeId(rs.getString("search_study_mode_id"));
    oppListVO.setStudyModeDesc(rs.getString("stdymde_description"));
    return oppListVO;
  }
}

