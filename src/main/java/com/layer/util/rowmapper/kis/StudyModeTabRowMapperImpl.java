package com.layer.util.rowmapper.kis;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.search.CourseKISDataVO;
 
 
  public class StudyModeTabRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
      CourseKISDataVO studyModeTabVO = new CourseKISDataVO();
      studyModeTabVO.setStudyModeId(rs.getString("search_study_mode_id"));
      studyModeTabVO.setStudyModeDesc(rs.getString("stdymde_description"));
      return studyModeTabVO;
    }
  }


