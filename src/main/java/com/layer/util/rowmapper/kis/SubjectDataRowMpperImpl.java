package com.layer.util.rowmapper.kis;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import oracle.sql.STRUCT;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.search.CourseKISDataVO;

  public class SubjectDataRowMpperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
      CourseKISDataVO subjectData = new CourseKISDataVO();
      subjectData.setSubjectName(rs.getString("subject_name"));
      subjectData.setGraduates(rs.getString("graduates"));
      subjectData.setGraduatesHelp(rs.getString("graduates_link"));
      subjectData.setEnrolledStudents(rs.getString("enrolled_students"));
      subjectData.setEnrolledHelp(rs.getString("enrolled_students_link"));
      subjectData.setFurtherStudy(rs.getString("further_study"));
      subjectData.setFurtherStudyHelp(rs.getString("further_study_link"));
      subjectData.setAvgSalaryAfterSixMonths(rs.getString("salary_6_months"));
      subjectData.setAvgSalaryAfterFourtyMonths(rs.getString("salary_40_months"));
      subjectData.setAvgSalAfterSixMonthsAtuni(rs.getString("median_inst_salary_6_months"));
      subjectData.setSalaryHelp(rs.getString("salary_subject_name_link"));
      subjectData.setGraduateHelpFlag(rs.getString("graduates_flag"));
      subjectData.setEnrollHelpFlag(rs.getString("enrolled_flag"));
      subjectData.setFurtherStudyHelpFlag(rs.getString("further_study_flag"));
      subjectData.setSalaryHelpFlag(rs.getString("salary_flag"));
      java.sql.Array optionsRS = (java.sql.Array)rs.getObject("subjects");// added for weblogic migration 28-JUN_2016_REL
      Object datanum[] = (Object[])optionsRS.getArray();
      ArrayList kissubjectList  = new ArrayList();
      for (int i = 0; i < datanum.length; i++) {
        STRUCT struct = (STRUCT)datanum[i];
        Object obj[] = struct.getAttributes();
        CourseKISDataVO kissubjectdate = new CourseKISDataVO();
        kissubjectdate.setKisSubject(String.valueOf(obj[3]));
        kissubjectdate.setHeldByPercentage(String.valueOf(obj[1]));
        kissubjectList.add(kissubjectdate);
      }
      subjectData.setSubjectList(kissubjectList);
      
      return subjectData;
    }
  }


