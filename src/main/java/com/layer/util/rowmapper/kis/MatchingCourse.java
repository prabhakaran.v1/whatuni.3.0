package com.layer.util.rowmapper.kis;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.search.CourseKISDataVO;
public class MatchingCourse implements RowMapper{
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
    CourseKISDataVO matchingCourse = new CourseKISDataVO();
    String pageName = "NORMAL_COURSE";
    if("NORMAL_COURSE".equalsIgnoreCase(rs.getString("page_name"))){
      pageName = "CLEARING_COURSE";
    }
    matchingCourse.setMatchingCourseURL(new SeoUrls().construnctCourseDetailsPageURL(rs.getString("study_level_desc"), rs.getString("course_title"), rs.getString("course_id"), rs.getString("college_id"), pageName));
    return matchingCourse;
  }
}
