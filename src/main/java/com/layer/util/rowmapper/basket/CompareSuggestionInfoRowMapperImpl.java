package com.layer.util.rowmapper.basket;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;

import com.wuni.mywhatuni.form.MyCompareBean;
import com.wuni.util.seo.SeoUrls;

public class CompareSuggestionInfoRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    MyCompareBean bean = new MyCompareBean();
    bean.setCollegeId(resultset.getString("college_id"));
    bean.setCollegeDisplayName(resultset.getString("college_name_display"));
    bean.setCollegeName(resultset.getString("college_name"));
    SeoUrls seoUrls = new SeoUrls();
    bean.setStudyLevel(resultset.getString("study_level_desc"));
    bean.setCourseTitle(resultset.getString("course_title"));
    bean.setClearingFlag(resultset.getString("clearing_flag"));
    bean.setCourseId(resultset.getString("course_id"));
    if ("CLEARING".equalsIgnoreCase(bean.getClearingFlag())) { //30_JUL_2015_REL
      bean.setUniHomeURL(seoUrls.construnctUniHomeURL(bean.getCollegeId(), bean.getCollegeName()) + "?clearing");
      bean.setCourseUrl(seoUrls.construnctCourseDetailsPageURL(bean.getStudyLevel(), bean.getCourseTitle(), bean.getCourseId(), bean.getCollegeId(), "CLEARING_COURSE"));
    } else {
      bean.setUniHomeURL(seoUrls.construnctUniHomeURL(bean.getCollegeId(), bean.getCollegeName()));
      bean.setCourseUrl(seoUrls.construnctCourseDetailsPageURL(bean.getStudyLevel(), bean.getCourseTitle(), bean.getCourseId(), bean.getCollegeId(), "COURSE_SPECIFIC"));
    }
    ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("enquiry_details");
    String tmpUrl = "";
    String splitMediaThumbPath = "";
    try {
      if (enquiryDetailsRS != null) {
        while (enquiryDetailsRS.next()) {
          bean.setOrderItemId(enquiryDetailsRS.getString("ORDER_ITEM_ID"));
          bean.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
          bean.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
          bean.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
          bean.setMyhcProfileId(enquiryDetailsRS.getString("MYHC_PROFILE_ID"));
          bean.setProfileType(enquiryDetailsRS.getString("PROFILE_TYPE"));
          bean.setCpeQualificationNetworkId(enquiryDetailsRS.getString("NETWORK_ID"));
          bean.setWebformPrice(enquiryDetailsRS.getString("WEBFORM_PRICE"));
          bean.setWebsitePrice(enquiryDetailsRS.getString("WEBSITE_PRICE"));
          tmpUrl = enquiryDetailsRS.getString("WEBSITE");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          bean.setSubOrderWebsite(tmpUrl);
          tmpUrl = enquiryDetailsRS.getString("EMAIL_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          bean.setSubOrderEmailWebform(tmpUrl);
          tmpUrl = enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          bean.setSubOrderProspectusWebform(tmpUrl);
          bean.setMediaPath(enquiryDetailsRS.getString("media_path"));
          bean.setMediaThumbPath(enquiryDetailsRS.getString("thumb_media_path"));
          if (!GenericValidator.isBlankOrNull(bean.getMediaThumbPath())) {
            splitMediaThumbPath = bean.getMediaThumbPath().substring(0, bean.getMediaThumbPath().lastIndexOf("."));
            bean.setMediaThumbPath(new CommonUtil().getImgPath((splitMediaThumbPath + ".jpg"), rowNum));
          } else {
            if (!GenericValidator.isBlankOrNull(bean.getMediaPath())) {
              if (enquiryDetailsRS.getString("media_type_id") != null && "16".equals(enquiryDetailsRS.getString("media_type_id"))) {
                bean.setMediaThumbPath(new CommonUtil().getImgPath((bean.getMediaPath().replace(".", "_140px.")), rowNum));
              } else {
                bean.setMediaThumbPath(new CommonUtil().getImgPath((bean.getMediaPath().replace(".", "_116px.")), rowNum));
              }
            }
          }
          bean.setMediaType(enquiryDetailsRS.getString("media_type"));
          bean.setMediaId(enquiryDetailsRS.getString("media_id"));
          if ("VIDEO".equalsIgnoreCase(bean.getMediaType())) {
            //topCoursesVO.setVideoThumbPath(new GlobalFunction().videoThumbnailFormatChange(topCoursesVO.getMediaPath(), "2"));
            String thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(bean.getMediaId(), "2"), rowNum); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path.
            bean.setVideoThumbPath(thumbnailPath);
          }
          bean.setHotlineNo(enquiryDetailsRS.getString("hotline")); //30_JUL_2015_REL
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (enquiryDetailsRS != null) {
          enquiryDetailsRS.close();
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    return bean;
  }
}
