package com.layer.util.rowmapper.basket;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.wuni.mywhatuni.form.MyCompareBean;
import com.wuni.util.seo.SeoUrls;

/**
  * @Version :  .0
  * @May 17 2015
  * @www.whatuni.com
  * @Created By : Priyaa Parthasarathy
  * @Purpose  : This program is used to fetch the comparison results cursor.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 17-May-2015  Priyaa Parthasarathy         1.0
  * 16-May-2016  Prabhakaran V.               1.2      Merging final five and comparison page                       wu_564 
  */
public class CompareBasketRowMapperImpl implements RowMapper {
  int index = 0;
  String visualType = "";
  String visualTypeArr[] = null;
  String imgPath = new CommonUtil().getImgPath("", 0);
  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    SeoUrls seourls = new SeoUrls();
    MyCompareBean bean = new MyCompareBean();
    bean.setCourseId(resultset.getString("course_id"));
    bean.setCollegeId(resultset.getString("college_id"));
    bean.setAssocId(resultset.getString("assoc_id"));
    bean.setAssociateTypeCode(resultset.getString("assoc_type_code"));
    bean.setCourseTitle(resultset.getString("course_title"));
    bean.setBasketId(resultset.getString("basket_Id"));
    bean.setClearingFlag(resultset.getString("page_type"));
    bean.setCollegeDisplayName(resultset.getString("college_display_name"));
    bean.setCollegeName(resultset.getString("college_name"));
    bean.setGaCollegeName(new CommonFunction().replaceSpecialCharacter(bean.getCollegeName()));
    bean.setDataString(resultset.getString("column1"));
    bean.setCollegeLog(resultset.getString("college_logo"));
    if(!GenericValidator.isBlankOrNull(bean.getCollegeLog())){
      bean.setCollegeLog(new CommonUtil().getImgPath(bean.getCollegeLog(), rowNum));
    }else{
      bean.setCollegeLog(imgPath + GlobalConstants.DEFAULT_UNI_IMG_PATH);
    }
    bean.setBasketContentId(resultset.getString("basket_content_id"));
    bean.setFinalChoiceId(resultset.getString("final_choice_id"));
    bean.setFinalFiveFlag(resultset.getString("final_choice_count_flag"));
   
    bean.setVisualType(resultset.getString("visual_type"));
   
    bean.setFactors(resultset.getString("factor_name"));
 
    bean.setStudyLevel(resultset.getString("study_level"));
    bean.setCourseDeletedFlag(resultset.getString("course_deleted_flag"));  
    if("C".equals(bean.getClearingFlag())){//30-JUN-2015_REL
       if(!"Y".equals(bean.getCourseDeletedFlag())){
        bean.setCourseUrl(seourls.construnctCourseDetailsPageURL(bean.getStudyLevel(), bean.getCourseTitle(), bean.getCourseId(), bean.getCollegeId(), "CLEARING_COURSE"));
       }
        bean.setUniHomeURL(seourls.construnctUniHomeURL(bean.getCollegeId(), bean.getCollegeName())+"?clearing");
     }else{
        if(!"Y".equals(bean.getCourseDeletedFlag())){
         bean.setCourseUrl(seourls.construnctCourseDetailsPageURL(bean.getStudyLevel(), bean.getCourseTitle(), bean.getCourseId(), bean.getCollegeId(), "COURSE_SPECIFIC"));
        }
        bean.setUniHomeURL(seourls.construnctUniHomeURL(bean.getCollegeId(), bean.getCollegeName()));
     }
    ArrayList compareTablevaluesList = new ArrayList();
    if (!GenericValidator.isBlankOrNull(bean.getDataString())) {
      int arrsize = bean.getDataString().split(GlobalConstants.SPLIT_CONSTANT).length;
      String dataArr[] = new String[arrsize];
      String visualTypeArr[] = new String[arrsize];
      String factorNameArr[] = new String[arrsize];     
      dataArr = bean.getDataString().split(GlobalConstants.SPLIT_CONSTANT);
      visualTypeArr = bean.getVisualType().split(GlobalConstants.SPLIT_CONSTANT);
      factorNameArr = bean.getFactors().split(GlobalConstants.SPLIT_CONSTANT);
      for (int i = 0; i < dataArr.length; i++) {
        MyCompareBean mycmpBean = new MyCompareBean();
        mycmpBean.setVisualTypeValue(visualTypeArr[i]);
        mycmpBean.setColumnvalue(dataArr[i]);
        mycmpBean.setFactorName(factorNameArr[i]);
        compareTablevaluesList.add(mycmpBean);
        if("bar".equalsIgnoreCase(mycmpBean.getVisualTypeValue())){
        if (!GenericValidator.isBlankOrNull(dataArr[i])) {
          String innerSplitArr[] = new String[dataArr[i].split(GlobalConstants.INNER_SPLIT_CONSTANT).length];
          innerSplitArr =  dataArr[i].split(GlobalConstants.INNER_SPLIT_CONSTANT);
          mycmpBean.setChartprctValue(innerSplitArr[1]);
          mycmpBean.setColumnvalue(innerSplitArr[0]);
          }
        }
      }
    }
    bean.setCompareTableDataList(compareTablevaluesList);
    bean.setChoicePosition(resultset.getString("FINAL_CHOICE_POSITION"));
    if(!GenericValidator.isBlankOrNull(bean.getChoicePosition())){
      int pos = Integer.parseInt(bean.getChoicePosition());
      String posText = "";
      if(pos == 1){
        posText = "st";
      }else if(pos == 2){
        posText = "nd";
      }else if(pos == 3){
        posText = "rd";
      }else if(pos > 3){
        posText = "th";
      }
      bean.setPosText(pos + posText);
    }
    if (!GenericValidator.isBlankOrNull(bean.getDataString())) {
      ArrayList tableValueList = new ArrayList(Arrays.asList(bean.getDataString().split(GlobalConstants.SPLIT_CONSTANT)));
      bean.setComparetableValuesList(tableValueList);
    }
    bean.setAddtoFinal5(resultset.getString("final_choice_exists_flag"));
    bean.setFinalChoiceCount(resultset.getString("final_choice_count"));
    return bean;
  }
}
