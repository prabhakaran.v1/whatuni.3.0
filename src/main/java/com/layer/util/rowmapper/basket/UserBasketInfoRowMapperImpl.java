package com.layer.util.rowmapper.basket;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.basket.form.BasketBean;

public class UserBasketInfoRowMapperImpl implements RowMapper{
  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException{
    BasketBean bean = new BasketBean();
    bean.setBasketId(resultset.getString("basket_id"));
    bean.setBasketCompareCount(resultset.getString("basket_content_cnt"));
    bean.setBasketCount(resultset.getString("user_basket_cnt"));
    bean.setBasketName(resultset.getString("basket_name"));
    bean.setPersonalNotes(resultset.getString("personal_notes"));
    return bean;
  }
}
