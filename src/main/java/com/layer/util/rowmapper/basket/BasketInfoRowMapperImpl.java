package com.layer.util.rowmapper.basket;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import WUI.basket.form.BasketBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

import com.wuni.util.seo.SeoUrls;


public class BasketInfoRowMapperImpl implements RowMapper{
  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException{
    BasketBean bean = new BasketBean();
    SeoUrls seoUrl = new SeoUrls();
    CommonFunction common = new CommonFunction();
    bean.setBasketId(resultset.getString("basket_id"));
    bean.setCollegeId(resultset.getString("college_id"));
    bean.setBasketCompareCount(resultset.getString("cnt"));
    bean.setCourseid(resultset.getString("course_id"));
    bean.setAssociationId(resultset.getString("assoc_id"));
    bean.setAssociationType(resultset.getString("assoc_type_code"));
    bean.setCollegeName(resultset.getString("college_name"));
    bean.setCollegeNameDisplay(resultset.getString("college_display_name"));
    bean.setCoursetitle(resultset.getString("course_title"));
    bean.setProviderDeleteFlag(resultset.getString("provider_del_flag"));
    bean.setCourseDeleteFlag(resultset.getString("course_del_flag"));
    bean.setSeoStudyLevelText(resultset.getString("seo_study_level"));
    bean.setComparePage(resultset.getString("page_type"));
    bean.setClearingONOFF(resultset.getString("clearing_on_off"));

    bean.setEntryPoints(resultset.getString("entry_points")); 
    bean.setTimesRanking(resultset.getString("times_league_position"));
    bean.setStudentJob(resultset.getString("job_study"));
    bean.setStudentStaffRatio(resultset.getString("student_staff_ratio"));
    bean.setScholarshipsCount(resultset.getString("scholarships"));
    bean.setAccomodationFee(resultset.getString("accomodation_fee"));
    bean.setGenderRatio(resultset.getString("male_female_ratio"));
    ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("enquiry_details"); 
    try {
      if (enquiryDetailsRS != null) {
        while (enquiryDetailsRS.next()) {
          bean.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
          String tmpUrl = enquiryDetailsRS.getString("WEBSITE");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          bean.setSubOrderWebsite(tmpUrl);
          
          bean.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
          tmpUrl = enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          bean.setSubOrderProspectusWebform(tmpUrl);
          bean.setCpeQualificationNetworkId(enquiryDetailsRS.getString("NETWORK_ID"));
          bean.setHotLine(enquiryDetailsRS.getString("hotline"));
        }
      }
    }catch(Exception e){
        e.printStackTrace();
      }finally{
        try{
          enquiryDetailsRS.close();
        }catch(Exception e){
          throw new SQLException(e.getMessage());
        }
      }
    if("N".equalsIgnoreCase(bean.getProviderDeleteFlag())){
      if("NORMAL".equalsIgnoreCase(bean.getComparePage())){
        bean.setUniHomeURL(seoUrl.construnctUniHomeURL(bean.getCollegeId(), bean.getCollegeName()));
      }else{
        if("ON".equalsIgnoreCase(bean.getClearingONOFF())){
          String clearingUniURL;
          if (!GenericValidator.isBlankOrNull(bean.getSubOrderItemId()) && !"0".equalsIgnoreCase(bean.getSubOrderItemId())) {
            clearingUniURL = "/university-clearing-"+GlobalConstants.CLEARING_YEAR+"/"+common.replaceHypen(common.replaceSpecialCharacter(bean.getCollegeName())).toLowerCase()+"-clearing-overview/"+bean.getCollegeId()+"/"+"page.html";
            bean.setUniHomeURL(clearingUniURL);
          }else{
           clearingUniURL =  "/university-clearing-"+GlobalConstants.CLEARING_YEAR+"/"+common.replaceHypen(common.replaceSpecialCharacter(bean.getCollegeName())).toLowerCase()+"/"+bean.getCollegeId()+".html";
            bean.setUniHomeURL(clearingUniURL);
          }
        }
      }
      if("N".equalsIgnoreCase(bean.getCourseDeleteFlag())){
        String pageName = "COURSE_SPECIFIC";
        if("NORMAL".equalsIgnoreCase(bean.getComparePage())){
          bean.setCourseURL(seoUrl.construnctCourseDetailsPageURL(bean.getSeoStudyLevelText(), bean.getCoursetitle(), bean.getCourseid(), bean.getCollegeId() , pageName));
        }else if("CLEARING".equalsIgnoreCase(bean.getComparePage()) && "ON".equalsIgnoreCase(bean.getClearingONOFF())){
          pageName = "CLEARING_COURSE";
          bean.setCourseURL(seoUrl.construnctCourseDetailsPageURL(bean.getSeoStudyLevelText(), bean.getCoursetitle(), bean.getCourseid(), bean.getCollegeId() , pageName));
        }
      }
    }
    if("NORMAL".equalsIgnoreCase(bean.getComparePage())){
      if("Y".equalsIgnoreCase(bean.getProviderDeleteFlag()) || "Y".equalsIgnoreCase(bean.getCourseDeleteFlag())){
        bean.setShowContentFlag("FALSE");
      }
    }else{
      if("ON".equalsIgnoreCase(bean.getClearingONOFF())){
        if("Y".equalsIgnoreCase(bean.getProviderDeleteFlag()) || "Y".equalsIgnoreCase(bean.getCourseDeleteFlag())){
          bean.setShowContentFlag("FALSE");
        }
      }else{
        bean.setShowContentFlag("FALSE");
      }
    }      
    return bean;
  }
}
