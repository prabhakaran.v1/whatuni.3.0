package com.layer.util.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.LocationVO;

public class VenueRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    LocationVO locationVO = new LocationVO();
    locationVO.setLatitude(rs.getString("latitude"));
    locationVO.setLongitude(rs.getString("longitude"));
    locationVO.setCollege_name_display(rs.getString("college_name_display"));
    locationVO.setAddress(rs.getString("address"));
    return locationVO;
  }
}
