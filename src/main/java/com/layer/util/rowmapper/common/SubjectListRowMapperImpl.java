package com.layer.util.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.CommonVO;

public class SubjectListRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CommonVO commonVO = new CommonVO();
      commonVO.setCategoryCode(rs.getString("category_code"));
      commonVO.setSubjectName(rs.getString("subject"));
      return commonVO;
    }
}
