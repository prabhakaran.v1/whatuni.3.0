package com.layer.util.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.CommonVO;

public class StudyLevelListRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    CommonVO commonVO = new CommonVO();
    commonVO.setOptionId(rs.getString("qual_opt_id"));
    commonVO.setOptionText(rs.getString("display_name"));
    return commonVO;
  }
}
