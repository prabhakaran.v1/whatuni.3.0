package com.layer.util.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.CommonVO;

public class StudyModeListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CommonVO commonVO = new CommonVO();
      commonVO.setStudyModeId(rs.getString("stdymde_opt_id"));
      commonVO.setStudyModeDesc(rs.getString("stdymde_webdesc"));
      commonVO.setStudyModeValue(rs.getString("stdymde_value"));
      return commonVO;
    }
  }