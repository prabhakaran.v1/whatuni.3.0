package com.layer.util.rowmapper.common;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.CollegeNamesVO;

public class CollegeNamesRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    CollegeNamesVO collegeNamesVO = new CollegeNamesVO();
    SeoUrls seoUrl = new SeoUrls();
    //
    collegeNamesVO.setCollegeId(resultset.getString("college_id"));
    collegeNamesVO.setCollegeName(resultset.getString("college_name"));
    collegeNamesVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    collegeNamesVO.setCollegeNameAlias(resultset.getString("college_name_alias"));
    collegeNamesVO.setUniHomeURL(seoUrl.construnctUniHomeURL(collegeNamesVO.getCollegeId(), collegeNamesVO.getCollegeName()));
    //
    return collegeNamesVO;
  }

}
