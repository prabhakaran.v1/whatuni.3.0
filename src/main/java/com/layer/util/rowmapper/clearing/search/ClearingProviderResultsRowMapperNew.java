package com.layer.util.rowmapper.clearing.search;

import WUI.utilities.CommonFunction;

import com.wuni.util.seo.SeoUrls;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.validator.GenericValidator;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.search.ProviderResultPageVO;

public class ClearingProviderResultsRowMapperNew implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    ProviderResultPageVO providerResultPageVO = new ProviderResultPageVO();    
    SeoUrls seoUrl = new SeoUrls();
    providerResultPageVO.setCourseId(resultset.getString("COURSE_ID"));
    //providerResultPageVO.setCourseDescription(resultset.getString("COURSE_DESC"));
    providerResultPageVO.setTotalCount(resultset.getString("total_cnt"));
    //providerResultPageVO.setAvailableStudyModes(resultset.getString("AVAILABLE_STUDY_MODES"));
    providerResultPageVO.setDepartmentOrFaculty(resultset.getString("DEPARTMENT_OR_FACULTY"));
    providerResultPageVO.setCourseTitle(resultset.getString("COURSE_TITLE"));
    providerResultPageVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
    providerResultPageVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    providerResultPageVO.setCollegeId(resultset.getString("COLLEGE_ID"));
    //providerResultPageVO.setUcasCode(resultset.getString("UCAS_CODE"));
    providerResultPageVO.setStudyMode(resultset.getString("study_level"));
    //providerResultPageVO.setWasThisCourseShortListed(resultset.getString("COURSE_IN_BASKET"));
    providerResultPageVO.setSeoStudyLevelText(resultset.getString("SEO_STUDY_LEVEL_TEXT"));
    //providerResultPageVO.setEmploymentRate(resultset.getString("employment_rate"));
    //providerResultPageVO.setCourseRank(resultset.getString("course_rank"));
    providerResultPageVO.setEntryRquirements(resultset.getString("entry_requirements"));
    //providerResultPageVO.setCourseUcasTariff("60-80 points");
    providerResultPageVO.setShowApplyNowButton(resultset.getString("SHOW_APPLY_NOW_BUTTON"));
    providerResultPageVO.setShowTooltipFlag(resultset.getString("SHOW_TOOLTIP_FLAG"));
    //providerResultPageVO.setModuleGroupDetails(resultset.getString("module_group_details"));
    //providerResultPageVO.setJacsSubject(resultset.getString("JACS_SUBJECT"));
    //providerResultPageVO.setPlacesAvailable(resultset.getString("places_available"));//24_JUN_2014
    //providerResultPageVO.setLastUpdatedDate(resultset.getString("last_updated_date"));//24_JUN_2014
     // Code added by Priyaa for Jan 2016 release
    // String moduletext = resultset.getString("module_group_info_rec");
//     if(!GenericValidator.isBlankOrNull(moduletext)){
//       String modArr[] = moduletext.split("##SP##");
//       ArrayList moduledetailList = new ArrayList(Arrays.asList( modArr));      
//       providerResultPageVO.setModuleDetailList(moduledetailList);
//     }            
     //End of code
//    if(!GenericValidator.isBlankOrNull(providerResultPageVO.getModuleGroupDetails())){
//      String[] modArr= providerResultPageVO.getModuleGroupDetails().split("##");
//      if(!GenericValidator.isBlankOrNull(modArr[1])){
//        providerResultPageVO.setModuleGroupId(modArr[1]);
//        if(!GenericValidator.isBlankOrNull(modArr[0])){
//          providerResultPageVO.setModuleTitle(modArr[0]);
//        }
//      }
//    }
    providerResultPageVO.setGaCollegeName(new WUI.utilities.CommonFunction().replaceSpecialCharacter(providerResultPageVO.getCollegeName()));
//    if(!GenericValidator.isBlankOrNull(providerResultPageVO.getCourseRank())){
//      providerResultPageVO.setRankOrdinal(new CommonFunction().getOrdinalFor(Integer.parseInt(providerResultPageVO.getCourseRank())));
//    }    
//    if(!GenericValidator.isBlankOrNull(providerResultPageVO.getModuleGroupDetails())){
//      String[] modArr= providerResultPageVO.getModuleGroupDetails().split("##");
//      if(modArr != null && modArr.length > 1 ){
//        providerResultPageVO.setModuleGroupId(modArr[1]);
//        providerResultPageVO.setModuleTitle(modArr[0]);
//      }
//    }  
      providerResultPageVO.setPageName(resultset.getString("page_name"));
       String courseDetailURL = seoUrl.construnctCourseDetailsPageURL(providerResultPageVO.getSeoStudyLevelText(), providerResultPageVO.getCourseTitle(), providerResultPageVO.getCourseId(), providerResultPageVO.getCollegeId(), providerResultPageVO.getPageName(), resultset.getString("opportunity_id"));
    providerResultPageVO.setCourseDetailUrl(courseDetailURL);
    providerResultPageVO.setApplyUrl(seoUrl.applyNowWugoURL(providerResultPageVO.getCourseId(), resultset.getString("opportunity_id")));
    providerResultPageVO.setOpportunityId(resultset.getString("opportunity_id"));
    //
//    providerResultPageVO.setFeeDuration(resultset.getString("FEE_DURATION"));
//    providerResultPageVO.setFees(resultset.getString("COURSE_DOM_FEES"));
//    providerResultPageVO.setAddedToFinalChoice(resultset.getString("final_choices_exist_flag"));
    //
    ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("ENQUIRY_DETAILS");
    try{
      if(enquiryDetailsRS != null){
        while(enquiryDetailsRS.next()){
          providerResultPageVO.setOrderItemId(enquiryDetailsRS.getString("ORDER_ITEM_ID"));                
          providerResultPageVO.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
          providerResultPageVO.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
          providerResultPageVO.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
          
          String tmpUrl = null;
          tmpUrl = enquiryDetailsRS.getString("WEBSITE");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          providerResultPageVO.setSubOrderWebsite(tmpUrl);
          //
          tmpUrl = enquiryDetailsRS.getString("EMAIL_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          providerResultPageVO.setSubOrderEmailWebform(tmpUrl);
          //
          tmpUrl = enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          providerResultPageVO.setSubOrderProspectusWebform(tmpUrl);
          
          providerResultPageVO.setHotLineNo(enquiryDetailsRS.getString("hotline"));
          providerResultPageVO.setNetworkId(enquiryDetailsRS.getString("network_id"));
          providerResultPageVO.setWebformPrice(enquiryDetailsRS.getString("webform_price"));
          providerResultPageVO.setWebsitePrice(enquiryDetailsRS.getString("website_price"));
          providerResultPageVO.setProfileType(enquiryDetailsRS.getString("profile_type"));
          providerResultPageVO.setMyhcProfileId(enquiryDetailsRS.getString("myhc_profile_id"));
          providerResultPageVO.setProfileId("0");
        }
      }        
    } catch(Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if(enquiryDetailsRS != null) {
          enquiryDetailsRS.close();
        }
      } catch(Exception e) {
        throw new SQLException(e.getMessage());
      }  
    }
    if("SP".equalsIgnoreCase(providerResultPageVO.getProfileType()))
    {
      providerResultPageVO.setSpURL(seoUrl.construnctSpURL(providerResultPageVO.getCollegeId(), providerResultPageVO.getCollegeName(), providerResultPageVO.getMyhcProfileId(), providerResultPageVO.getProfileId(), providerResultPageVO.getNetworkId(), "overview"));
    }
    return providerResultPageVO; 
  }
}
