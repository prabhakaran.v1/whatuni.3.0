package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.review.ReviewBreakDownVO;


public class ReviewBreakDownSP implements RowMapper{
      public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
        ReviewBreakDownVO reviewBreakDownVO = new ReviewBreakDownVO();
        reviewBreakDownVO.setQuestionTitle(resultset.getString("QUESTION_TITLE"));
        reviewBreakDownVO.setFiveStarPercent(resultset.getString("FIVE_STAR_PERCENT"));
        reviewBreakDownVO.setFourStarPercent(resultset.getString("FOUR_STAR_PERCENT"));
        reviewBreakDownVO.setThreeStarPercent(resultset.getString("THREE_STAR_PERCENT"));
        reviewBreakDownVO.setTwoStarPercent(resultset.getString("TWO_STAR_PERCENT"));
        reviewBreakDownVO.setOneStarPercent(resultset.getString("ONE_STAR_PERCENT"));
        reviewBreakDownVO.setRating(resultset.getString("RATING"));
        reviewBreakDownVO.setReviewExact(resultset.getString("RATING_EXACT"));
        reviewBreakDownVO.setReviewCount( resultset.getString("REVIEW_CNT"));
        return reviewBreakDownVO;
      }
    }
   

