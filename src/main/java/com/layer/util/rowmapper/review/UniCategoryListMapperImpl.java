package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import WUI.review.bean.ReviewListBean;
import WUI.utilities.CommonUtil;

import com.wuni.util.seo.SeoUrls;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : UniCategoryListMapperImpl.java
 * Description   : This class used to get the uni categories in reviews..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * ?
 * Prabhakaran V.          1.1             21.12.2015           Second draft   		               Remove review count
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
public class UniCategoryListMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ReviewListBean bean = new ReviewListBean();
    SeoUrls seoUrl = new SeoUrls();
    try {
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeName(rs.getString("college_name"));
      bean.setUniHomeURL(seoUrl.construnctUniHomeURL(bean.getCollegeId(), bean.getCollegeName()));
			bean.setUniReviewsURL(seoUrl.constructReviewPageSeoUrl(bean.getCollegeName(), bean.getCollegeId()));
      bean.setCollegeDisplayName(rs.getString("college_display_name"));
      bean.setQuestionTitle(rs.getString("question_title"));
      bean.setRating(rs.getString("rating"));
      bean.setRoundRating(rs.getString("round_rating"));
      bean.setQuestionId(rs.getString("question_id"));
      bean.setLogo(rs.getString("logo"));
      if (!GenericValidator.isBlankOrNull(bean.getLogo())) {
        String splitMediaThumbPath = bean.getLogo().substring(0, bean.getLogo().lastIndexOf("."));
        //bean.setLogoImageName(new CommonUtil().getImgPath((splitMediaThumbPath + ".jpg"), rowNum));
         bean.setLogoImageName(new CommonUtil().getImgPath((bean.getLogo()), rowNum));          
      }
      bean.setAwardImage(rs.getString("award_image"));
      if (!GenericValidator.isBlankOrNull(bean.getAwardImage())) {
        bean.setAwardImagePath(new CommonUtil().getImgPath(("/wu-cont/images/awards/" + bean.getAwardImage()), rowNum));
      }
      String imgTitle = "";
      if(bean.getAwardImage().contains("gold")){imgTitle = "Student Gold Award "+bean.getQuestionTitle();}
      if(bean.getAwardImage().contains("silver")){imgTitle = "Student Silver Award "+bean.getQuestionTitle();}
      if(bean.getAwardImage().contains("bronze")){imgTitle = "Student Bronze Award "+bean.getQuestionTitle();}
      bean.setImageTitle(imgTitle);
      bean.setDisplaySeq(rs.getString("display_seq"));
      bean.setReviews(rs.getString("reviews"));
      bean.setStatus(rs.getString("status"));
      bean.setSponsorName(rs.getString("sponsor_name"));
      bean.setSponsorURL(rs.getString("sponsor_url"));
    } catch (Exception e) {
      e.printStackTrace();
    }
    return bean;
  }
}
