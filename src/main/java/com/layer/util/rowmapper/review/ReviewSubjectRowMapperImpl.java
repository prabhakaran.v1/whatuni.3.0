package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.review.ReviewSubjectVO;

public class ReviewSubjectRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      ReviewSubjectVO reviewSubjectVO = new ReviewSubjectVO();
      reviewSubjectVO.setSubjectId(resultset.getString("SUBJECT_ID"));
      reviewSubjectVO.setCategoryCode(resultset.getString("CATEGORY_CODE"));
      reviewSubjectVO.setSubjectName(resultset.getString("SUBJECT_NAME"));
      return reviewSubjectVO;
    }
}
