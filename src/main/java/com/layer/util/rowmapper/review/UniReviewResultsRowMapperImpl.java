package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.jdbc.core.RowMapper;

import WUI.review.bean.ReviewListBean;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.review.ReviewListVO;

/**
  * @UniReviewResultsRowMapperImpl
  * @author Thiyagu G
  * @version 1.0
  * @since 01.08.2015
  * @purpose This program is used call the database procedure to build the review landing page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 01_SEP_2015   Thiyagu G                  1.0      To get the user's reviews.                                      wu_545
  * 18_DEC_2018   Sangeeth S                 1.1      Added user name initail for DEC_18 rel by Sangeeth.S            wu_584
  */
public class UniReviewResultsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewListBean reviewResBean = new ReviewListBean();
      reviewResBean.setCollegeId(rs.getString("college_id"));
      reviewResBean.setReviewId(rs.getString("review_id"));
      reviewResBean.setUserId(rs.getString("user_id"));
      reviewResBean.setReviewTitle(rs.getString("review_title"));
      reviewResBean.setOverallQuesDesc(rs.getString("question_desc"));//Added Question description by Indumathi.S 26_07_2016
      reviewResBean.setCollegeName(rs.getString("college_name"));
      reviewResBean.setCollegeDisplayName(rs.getString("college_name_display"));
      reviewResBean.setUserName(rs.getString("user_name"));
      reviewResBean.setCourseName(rs.getString("course_name"));
      reviewResBean.setCourseDeletedFlag(rs.getString("course_deleted_flag"));
      reviewResBean.setCourseId(rs.getString("course_id"));
      reviewResBean.setOverAllRating(rs.getString("overall_rating"));
      reviewResBean.setOverallRatingComment(rs.getString("overall_rating_comments"));
      reviewResBean.setReviewCount(rs.getString("review_cnt"));      
      reviewResBean.setCreatedDate(rs.getString("created_date"));
      reviewResBean.setTotalReviewCountDisplay(rs.getString("tot_review"));
      reviewResBean.setSeoStudyLevelText(rs.getString("seo_study_level_text"));
      reviewResBean.setUniHomeURL(new SeoUrls().construnctUniHomeURL(reviewResBean.getCollegeId(), reviewResBean.getCollegeName()));
      ResultSet viewMoreReviewRS = (ResultSet)rs.getObject("view_more");
      if (viewMoreReviewRS != null) {
        ArrayList viewMoreList = new ArrayList();
        try {
          ReviewListVO viewMoreInfo = null;
          while (viewMoreReviewRS.next()) {
            viewMoreInfo = new ReviewListVO();
            viewMoreInfo.setReviewQuestionId(viewMoreReviewRS.getString("review_question_id"));
            viewMoreInfo.setReviewQuestion(viewMoreReviewRS.getString("question_title"));
            viewMoreInfo.setReviewAnswer(viewMoreReviewRS.getString("answer"));
            viewMoreInfo.setOverallRating(viewMoreReviewRS.getString("rating"));
            viewMoreInfo.setQuestionDesc(viewMoreReviewRS.getString("question_desc"));//Added Question description by Indumathi.S 26_07_2016
            viewMoreList.add(viewMoreInfo);
          }
          reviewResBean.setViewMoreRes(viewMoreList);
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          try {
            if (viewMoreReviewRS != null) {
              viewMoreReviewRS.close();
            }
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
      reviewResBean.setUserNameIntial(rs.getString("user_name_int"));//Added user name initail for DEC_18 rel by Sangeeth.S
      return reviewResBean;
    }
  }