package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.review.UniOverallReviewsVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : WuscaRatingRowMapperImpl.java
 * Description   : This class used to get the WUSCA rating details in key stats pod..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * Prabhakaran V.          1.0             21.12.2015           First draft   		
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
 
public class WuscaRatingRowMapperImpl implements RowMapper {
	public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
		UniOverallReviewsVO uniOverallReviewsVO = new UniOverallReviewsVO();
		uniOverallReviewsVO.setReviewCount(resultset.getString("overall_wusca_rating_exact"));
		uniOverallReviewsVO.setOverallRating(resultset.getString("overall_wusca_rating"));
		return uniOverallReviewsVO;
	}
}
