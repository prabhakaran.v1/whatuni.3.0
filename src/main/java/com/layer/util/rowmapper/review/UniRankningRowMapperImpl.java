package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import spring.valueobject.search.TimesRankingVO;


public class UniRankningRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      TimesRankingVO timesRankingVO = new TimesRankingVO();
      timesRankingVO.setUniTimesRanking(resultset.getString("times_ranking"));
      timesRankingVO.setStudentRanking(resultset.getString("student_ranking"));
      timesRankingVO.setTotalStudentRanking(resultset.getString("total_student_ranking"));
      return timesRankingVO;
    }
  }