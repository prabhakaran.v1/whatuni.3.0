package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.review.UserReviewsVO;

public class UserReviewsRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    UserReviewsVO userReviewsVO = new UserReviewsVO();
    SeoUrls seoUrls = new SeoUrls();
    //
    userReviewsVO.setCollegeId(resultset.getString("college_id"));
    userReviewsVO.setCollegeName(resultset.getString("college_name"));
    userReviewsVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    userReviewsVO.setReviewId(resultset.getString("review_id"));
    userReviewsVO.setReviewTitle(resultset.getString("review_title"));
    userReviewsVO.setReviewerName(resultset.getString("reviewer_name"));
    userReviewsVO.setReviewDesc(resultset.getString("review_desc"));
    userReviewsVO.setReviewRatings(resultset.getString("review_ratings"));
    userReviewsVO.setReviewDetailsURL(seoUrls.constructReviewDetailsUrl(userReviewsVO.getCollegeId(), userReviewsVO.getReviewId()));
    //
    seoUrls = null;
    //
    return userReviewsVO;
  }

}