package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.review.UserReviewsVO;

public class PRUserReviewsRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    UserReviewsVO userReviewsVO = new UserReviewsVO();
    SeoUrls seoUrls = new SeoUrls();    
    userReviewsVO.setCollegeId(resultset.getString("college_id"));
    userReviewsVO.setReviewId(resultset.getString("review_id"));
    userReviewsVO.setUserId(resultset.getString("user_id"));
    userReviewsVO.setReviewTitle(resultset.getString("review_title"));
    userReviewsVO.setOverallQuesDesc(resultset.getString("question_desc"));//Added Question description by Indumathi.S 26_07_2016
    userReviewsVO.setCollegeName(resultset.getString("college_name"));
    userReviewsVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    userReviewsVO.setUserName(resultset.getString("user_name"));
    userReviewsVO.setCourseName(resultset.getString("course_name"));    
    userReviewsVO.setCourseDeletedFlag(resultset.getString("course_deleted_flag"));
    userReviewsVO.setCourseId(resultset.getString("course_id"));
    userReviewsVO.setOverallRating(resultset.getString("overall_rating"));
    userReviewsVO.setOverallRatingComments(resultset.getString("overall_rating_comments"));       
    userReviewsVO.setCreatedDate(resultset.getString("created_date"));
    userReviewsVO.setStudyLevelText(resultset.getString("study_level_text"));
    ResultSet userReviewsRS = (ResultSet) resultset.getObject("view_more");
    userReviewsVO.setUserNameInitial(resultset.getString("user_name_int"));
      try{
         if(userReviewsRS != null){
             ArrayList userMoreReviewaList = new ArrayList();
             UserReviewsVO userInnerReviewsVO = null;
             while(userReviewsRS.next()){
                 userInnerReviewsVO = new UserReviewsVO();
                 userInnerReviewsVO.setReviewQuestionId(userReviewsRS.getString("REVIEW_QUESTION_ID"));
                 userInnerReviewsVO.setQuestionTitle(userReviewsRS.getString("QUESTION_TITLE"));
                 userInnerReviewsVO.setAnswer(userReviewsRS.getString("ANSWER"));                 
                 userInnerReviewsVO.setRating(userReviewsRS.getString("RATING"));
                 userInnerReviewsVO.setQuestionDesc(userReviewsRS.getString("question_desc"));//Added Question description by Indumathi.S 26_07_2016
                 userMoreReviewaList.add(userInnerReviewsVO);
             }
             userReviewsVO.setUserMoreReviewaList(userMoreReviewaList);
         }
      }catch (Exception e) {
         e.printStackTrace();
       } finally {
         try {
           userReviewsRS.close();
         } catch (Exception e) {
           throw new SQLException(e.getMessage());
         }
       }
    userReviewsVO.setUniHomeURL(seoUrls.construnctUniHomeURL(userReviewsVO.getCollegeId(),userReviewsVO.getCollegeName()));
    userReviewsVO.setCourseDetailsURL(seoUrls.construnctCourseDetailsPageURL(userReviewsVO.getStudyLevelText(),userReviewsVO.getCourseName(),userReviewsVO.getCourseId(),userReviewsVO.getCollegeId(),"NORMAL_COURSE"));
    userReviewsVO.setReviewDetailsURL(seoUrls.constructReviewDetailsUrl(userReviewsVO.getCollegeId(), userReviewsVO.getReviewId()));
    //
    seoUrls = null;
    //
    return userReviewsVO;
  }

}