package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;

import com.wuni.util.valueobject.review.ReviewListVO;

public class CollegeOverallRatingMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ReviewListVO reviewListVO = new ReviewListVO();
    reviewListVO.setReviewQuestion(rs.getString("question_title"));
    reviewListVO.setOverallRating(rs.getString("rating"));
    reviewListVO.setReviewQuestionId(rs.getString("question_id"));
    reviewListVO.setCollegeId(rs.getString("college_id"));
    reviewListVO.setMaxRating("10");
    String questionTitle = rs.getString("question_title");
    String seoText = new CommonFunction().seoQuestionTitle(questionTitle);
    reviewListVO.setSeoQuestionText(seoText);
    return reviewListVO;
  }
}
