package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.review.UniReviewVO;

public class UniReviewRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
  
    UniReviewVO uniReviewVO = new UniReviewVO();
    try{
    uniReviewVO.setStar(resultSet.getString("star"));
    uniReviewVO.setRatingPercent(resultSet.getString("rating_percent"));
    uniReviewVO.setReviewCount(resultSet.getString("total_count"));
    }catch(Exception e){
      e.printStackTrace();
    }
    return uniReviewVO;
  }
}
