package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.review.UniOverallReviewsVO;

public class UniOverallReviewRowMapperImpl implements RowMapper {

  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    UniOverallReviewsVO uniOverallReviewsVO = new UniOverallReviewsVO();
    //
    uniOverallReviewsVO.setCollegeId(resultset.getString("college_id"));
    uniOverallReviewsVO.setCollegeName(resultset.getString("college_name"));
    uniOverallReviewsVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    uniOverallReviewsVO.setReviewCount(resultset.getString("review_count"));
    uniOverallReviewsVO.setOverallRating(resultset.getString("overall_rating"));
    //
    return uniOverallReviewsVO;
  }

}
