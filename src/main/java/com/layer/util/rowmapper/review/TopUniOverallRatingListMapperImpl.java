package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.review.bean.ReviewListBean;

import com.wuni.util.seo.SeoUrls;

public class TopUniOverallRatingListMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ReviewListBean reviewBean = new ReviewListBean();
    reviewBean.setCollegeId(rs.getString("college_id"));
    reviewBean.setCollegeName(rs.getString("college_name"));
    reviewBean.setCollegeDisplayName(rs.getString("college_name_display"));
    reviewBean.setSponsorURL(new SeoUrls().construnctUniHomeURL(reviewBean.getCollegeId(), reviewBean.getCollegeName()));
    return reviewBean;
  }
}
