package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.review.UserReviewsVO;

public class RichProfileReviewRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    UserReviewsVO userReviewsVO = new UserReviewsVO();
    SeoUrls seoUrls = new SeoUrls();
    //
    userReviewsVO.setCollegeId(resultset.getString("college_id"));
    userReviewsVO.setReviewId(resultset.getString("review_id"));
    userReviewsVO.setUserId(resultset.getString("user_id"));
    userReviewsVO.setReviewTitle(resultset.getString("review_title"));
    userReviewsVO.setCollegeName(resultset.getString("college_name"));
    userReviewsVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
    userReviewsVO.setReviewerName(resultset.getString("user_name"));
    userReviewsVO.setReviewRatings(resultset.getString("overall_rating"));
    userReviewsVO.setCourseName(resultset.getString("course_title"));
    userReviewsVO.setCourseId(resultset.getString("course_id"));
userReviewsVO.setCourseDeletedFlag(resultset.getString("course_deleted_flag"));
    userReviewsVO.setCollegeLandingUrl(seoUrls.construnctUniHomeURL(userReviewsVO.getCollegeId(), userReviewsVO.getCollegeName()));
    userReviewsVO.setReviewDetailsURL(seoUrls.constructReviewDetailsUrl(userReviewsVO.getCollegeId(), userReviewsVO.getReviewId()));
      if((!GenericValidator.isBlankOrNull(userReviewsVO.getCourseId()) || !"0".equals(userReviewsVO.getCourseId()))){
        userReviewsVO.setCourseDetailsReviewURL(new SeoUrls().courseDetailsPageURL(userReviewsVO.getCourseName(), userReviewsVO.getCourseId(), userReviewsVO.getCollegeId()));
      }
    userReviewsVO.setReviewDesc(resultset.getString("review_text"));
    userReviewsVO.setUserNameInitial(resultset.getString("user_name_int"));
    userReviewsVO.setReviewDate(resultset.getString("review_date"));
    return userReviewsVO;
  }
}
