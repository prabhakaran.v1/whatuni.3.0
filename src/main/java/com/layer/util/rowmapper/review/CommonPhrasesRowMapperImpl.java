package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.wuni.util.valueobject.review.CommonPhrasesVO;

public class CommonPhrasesRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
    CommonPhrasesVO commonPhrasesVO = new CommonPhrasesVO();
    commonPhrasesVO.setCommonPhrases(resultset.getString("COMMON_PHRASES"));
    return commonPhrasesVO;
  }
}
