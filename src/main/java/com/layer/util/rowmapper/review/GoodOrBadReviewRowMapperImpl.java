package com.layer.util.rowmapper.review;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.review.bean.ReviewListBean;

public class GoodOrBadReviewRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    ReviewListBean bean = new ReviewListBean();
    bean.setReviewId(rs.getString("review_id"));
    bean.setReviewTitle(rs.getString("review_title"));
    bean.setUserId(rs.getString("user_id"));
    bean.setUserName(rs.getString("user_name"));
    bean.setCollegeId(rs.getString("college_id"));
    bean.setCollegeName(rs.getString("college_name"));
    bean.setCollegeNameDisplay(rs.getString("college_name_display"));
    bean.setCourseName(rs.getString("course_name"));
    bean.setCourseId(rs.getString("course_id"));
    bean.setOverallRatingComment(rs.getString("overall_rating_comments"));
    bean.setStudCurrentStatus(rs.getString("at_uni"));
    bean.setUserGraduate(rs.getString("at_uni"));
    bean.setOverallRating(rs.getString("overall_rating"));
    bean.setNationality(rs.getString("nationality"));
    bean.setGender(rs.getString("gender"));
    //bean.setReviewCount(rs.getString("review_cnt"));
    bean.setReviewType(rs.getString("review_status")); // good or bad - based on the review 
     //NEED TODO - need to remove for Ram's reference
    /*String userImageName = rs.getString("user_photo");
    String userThumbName = (userImageName != null? userImageName.substring(0, userImageName.lastIndexOf(".")) + "T" + userImageName.substring(userImageName.lastIndexOf("."), userImageName.length()): userImageName);
    bean.setUserImage(userThumbName);
    bean.setUserImageLarge(userImageName);*/
    bean.setReviewVotingCount(rs.getString("review_voting"));
    return bean;
  }
}
