package com.layer.util;

import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

/**
 * contains spring specific utilities
 * moved from "src.spring.util" to this package - as a part of STRUTS-SPRING migration, and all unused function were removed.
 *
 * @author     Mohamed Syed
 * @since      wu315_20110726
 *
 */
public class SpringUtilities {

  /**
   * function is used to load the spring config files to context.
   * @param beanId
   * @return bean as Object.
   */
  public static Object getBeanFromSpringContext(String beanId) {
    BeanFactoryLocator bfLocator = ContextSingletonBeanFactoryLocator.getInstance(SpringConstants.BEANREFCONTEXT_XML); //getting factories lis
    BeanFactoryReference factory = bfLocator.useBeanFactory(SpringConstants.SERVICELAYER_CONTEXT); //getting particular factory i.e servicelayer-context
    Object beanObj = factory.getFactory().getBean(beanId); //     
    return beanObj;
  }

}
