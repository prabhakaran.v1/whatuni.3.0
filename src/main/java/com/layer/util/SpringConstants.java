package com.layer.util;

/**
 * contains spring specific constants
 * moved from "src.spring.util" to this package - as a part of STRUTS-SPRING migration
 *
 * @author     Mohamed Syed
 * @since      wu315_20110726
 *
 */
public interface SpringConstants {

  public static final String BEANREFCONTEXT_XML = "classpath:/com/conf/beanRefContext.xml";
  public static final String SERVICELAYER_CONTEXT = "servicelayer-context";
  public static final String DEVELOPER_GROUP_ID = "1000";
  public static final String DATA_GROUP_ID = "1001";
  public static final String BEAN_ID_COMMON_BUSINESS = "ICommonBusiness";
  public static final String BEAN_ID_SEARCH_BUSINESS = "ISearchBusiness";
  public static final String BEAN_ID_HOME_BUSINESS = "IHomeBusiness";
  public static final String BEAN_ID_ADVICE_BUSINESS = "IAdviceBusiness"; //09_Dec_2014 add constants for Advice by Thiyagu G 
  //THIS CODE IS RELATED TO DB PROC NAME CONSTANTS ADDED BY ANBU.R   
  public static final String GET_ALPHABET_COLLEGE_NAME_FN = "WU_COLLEGE_INFO_PKG.FIND_COLLEGES_PRC"; //Modified by Indumathi For 03_NOV_2015_REL
  public static final String GET_MBL_COLLEGE_FINDER_FN = "hot_wuni.WU_MOBILE_SEARCH_PKG.GET_COLLEGE_NAME_LIST_PRC";
  public static final String USER_REGISTERATION_LBX_PRC = "WU_USER_PROFILE_PKG.WU_USER_REGISTERATION_LBX_PRC";
  public static final String CHECK_USERNAME_PASSWORD_FN = "WU_REVIEW_PKG.CHECK_USERNAME_PASSWORD_FN";
  public static final String GET_USER_PREDICTED_GRADES_VAL_FN = "WHATUNI_SEARCH3.GET_USER_ATTR_VAL_FN";
  public static final String ADD_MY_OPENDAYS_FN = "hot_wuni.wuni_opendays_pkg.add_opendays_user_fn";
  public static final String STUDENT_CHOICE_AWARDS_LIST_PRC = "WU_REVIEWS_PKG.WU_UNI_RANK_LIST_PRC";
  public static final String GET_AWARD_YEAR_FN = "HOT_WUNI.WU_REVIEWS_PKG.GET_AWARD_YEAR_FN";
  public static final String CLEARING_PROFILE_PRC = "HOT_WUNI.WU_CLEARING_PKG.GET_CLEARING_PROFILE_PRC";
  public static final String NEW_UNI_LANDING_PRC = "HOT_WUNI.WUNI_SPRING_PKG.UNI_PROFILE_PAGE_PRC";
  public static final String FINAL_CHOICES_HOME_FN = "HOT_WUNI.WU_FINAL_CHOICES_PKG.GET_FINAL_CHOICES_FN";
  public static final String FINAL_CHOICES_EDIT_FN = "HOT_WUNI.WU_FINAL_CHOICES_PKG.EDIT_FINAL_CHOICES_FN";
  public static final String GET_PROFILE_TAB_DETAILS = "HOT_WUNI.WUNI_SPRING_PKG.GET_PROFILE_TAB_DETAILS_PRC";//Added by Indumathi May_31_2016 For Tab changes
  public static final String CONFIRM_FINAL_CHOICES_FN = "HOT_WUNI.WU_FINAL_CHOICES_PKG.CONFIRM_FINAL_CHOICE_FN";
  public static final String FINAL_CHOICES_SUGGESTION_FN = "HOT_WUNI.WU_FINAL_CHOICES_PKG.GET_FINAL_SUGGESTION_FN";
  public static final String SAVE_FINAL_CHOICES_DATA_PRC = "HOT_WUNI.WU_FINAL_CHOICES_PKG.SAVE_FINAL_CHOICES_PRC";
  public static final String FINAL_CHUSER_ACTION_PRC = "HOT_WUNI.WU_FINAL_CHOICES_PKG.GET_FINAL_CHOICE_ACTION_PRC";
  public static final String CHANGE_FINAL_CHOICE_DATA_PRC = "HOT_WUNI.WU_FINAL_CHOICES_PKG.CHANGE_FINAL_CHOICE_PRC";
  public static final String SAVE_USER_ACTION_VAL_PRC = "SAVE_USER_ACTION_VAL_PRC"; // 19.05.2015This class used to inserting opendays for user against reserve place.
  public static final String CLEARING_HOME_PAGE_PRC = "WU_CLEARING_PKG.CLEARING_HOME_PAGE_PRC"; //Added steing constants for hompe page procedure reference for 09_Jun_2015 release, By Thiyagu G.
  public static final String SURVEY_USER_LOGIN_PRC = "HOT_WUNI.WU_SURVEY_USER_LOGIN_PRC"; //Modified by Priyaa For 21_JUL_2015_REL 
  public static final String GET_USER_REGISTRATION_LIGHTBOX_PRC = "WU_USER_PROFILE_PKG.COUNTRY_LIST_PRC"; //Added to get user registration lightbox data for 03_Nov_2015, By Thiyagu G.
  public static final String BULK_PROSPECTUS_SUBMIT_PRC = "WHATUNI_INTERACTIVE_PKG.BULK_PROSPECTUS_SUBMIT_PRC"; //Added bulk prospectus save proc for 03_Nov_2015, By Thiyagu G.
  public static final String WU_ENQUIRY_FORM_SUBMISSION_PRC = "WHATUNI_INTERACTIVE_PKG.WU_ENQUIRY_FORM_SUBMISSION_PRC"; //Added enquiry submission proc for 03_Nov_2015, By Thiyagu G.
  public static final String SCOUT_USER_REGISTRATION_PRC = "HOT_WUNI.scout_registration_prc"; //Scout registration and login prc added by Prabha on 19_Apr_2016_REL
  public static final String RICH_PROFILE_PRC = "HOT_WUNI.WUNI_PROFILE_PKG.GET_RICH_PROFILE_PRC"; //Rich profile procedure added by Prabha on 31_May_16_REL
  public static final String REORDER_FINAL_CHOICE_FN = "HOT_WUNI.WU_FINAL_CHOICES_PKG.REORDERING_FINAL_CHOICE_FN"; //Reordering final five choice fn added by Prabha on 16_May_17
  public static final String EDIT_CONFIRM_CHOICES_PRC = "HOT_WUNI.WU_FINAL_CHOICES_PKG.CONFIRM_FINAL_CHOICE_PRC"; //Edit/confirm final five choice fn added by Prabha on 16_May_17
  public static final String SUGGESTED_COMPARISON_PRC = "HOT_WUNI.WU_FINAL_CHOICES_PKG.GET_FINAL_SUGGESTION_FN";
  public static final String UPDATE_BASKET_COURSE_PRC = "HOT_WUNI.MYWU_COMPARE_PKG.UPDATE_COURSE_IN_BASKET_PRC";
  public static final String GET_ARTICLE_POD_PRC = "HOT_WUNI.WUNI_ARTICLES_PKG.GET_ARTICLE_POD_PRC"; //Get the article pod through ajax call, added on 13_Jun_2017, By Thiyagu G.
  public static final String CONTENT_HUB_PROFILE_PRC = "WUNI_CONTENTHUB_PKG.GET_CONTENTHUB_PROFILE_PRC";  
  public static final String GET_OPEN_DAY_SEARCH_PRC = "WU_OPENDAYS_SEARCH_PKG.GET_OPENDAYS_SRCH_RESULTS_PRC";
  public static final String SAVE_USER_DETAILS_PRC = "WU_USER_PROFILE_PKG.SAVE_USER_DETAILS_PRC";
  public static final String GET_SUBJECT_REVIEWS_AJAX = "WU_REVIEW_PKG.GET_LATEST_SUB_UNIREVIEW_FN";
  public static final String GET_CD_PAGE_SUBJECT_REVIEWS = "HOT_WUNI.WU_COURSE_DETAILS_PKG.GET_CD_REVIEW_POD_PRC";
  public static final String GET_REVIEW_RESULT_PRC = "HOT_WUNI.WU_REVIEW_PKG.GET_REVIEW_RESULT_PRC";
  public static final String GET_REVIEW_SUBJECT_FN = "HOT_WUNI.WU_UTIL_PKG.GET_REVIEW_SUBJECT_FN";
  public static final String GET_APPLY_NOW_COURSE_DETAIL_PRC = "WU_UTIL_PKG.GET_APPLY_COURSE_DETAILS_PRC";
  public static final String GET_CLEARING_COURSE_LIST = "HOT_WUNI.WUGO_CLEARING_SEARCH_PKG.GET_AJAX_CL_SUBJECT_FN";
  //grade filter page
  public static final String WUGO_GET_QUALIFICATION_LIST_PRC = "WUGO_CLEARING_SEARCH_PKG.GET_QUALIFICATION_LIST_PRC";
  public static final String GET_QUAL_SUBJECTS_FN = "HOT_WUNI.WUGO_CLEARING_SEARCH_PKG.GET_QUAL_SUBJECTS_FN";//subject ajax
  public static final String CALCULATE_UCAS_POINTS_FN = "HOT_WUNI.WUGO_CLEARING_SEARCH_PKG.CALCULATE_UCAS_POINTS_FN";//subject ucas calculation 
  public static final String SAVE_QUAL_DATA_PRC = "HOT_WUNI.WUGO_CLEARING_SEARCH_PKG.SAVE_QUAL_DATA_PRC";//saving the date in grade filter page 
  
  public static final String GET_WUGO_LOCTAION_LIST_PRC = "HOT_WUNI.WUGO_CLEARING_SEARCH_PKG.GET_WUGO_LOCTAION_LIST_PRC";
  
  public static final String GET_COOKIE_LATEST_UPDATED_DATE_PRC  = "HOT_WUNI.UTILITY_PKG.GET_LATEST_DATE_PRC";

  //SR new grade filter page - Jeya 22nd Oct 2019 rel
  public static final String SR_GET_QUALIFICATION_LIST_PRC = "HOT_WUNI.WU_QUALIFICATIONS_PKG.GET_QUALIFICATION_LIST_PRC";
  public static final String SR_GET_QUAL_SUBJECTS_FN = "HOT_WUNI.WU_QUALIFICATIONS_PKG.GET_QUAL_SUBJECTS_FN";//subject ajax
  public static final String SR_CALCULATE_UCAS_POINTS_FN = "HOT_WUNI.WU_QUALIFICATIONS_PKG.CALCULATE_UCAS_POINTS_FN";//subject ucas calculation 
  public static final String SR_SAVE_QUAL_DATA_PRC = "HOT_WUNI.WU_QUALIFICATIONS_PKG.SAVE_QUAL_DATA_PRC";//saving the date in grade filter page 
  public static final String SR_DELETE_USER_SUBJ_GRADE_FN = "HOT_WUNI.WU_QUALIFICATIONS_PKG.DELETE_USER_SUBJ_GRADE_FN";
  public static final String SR_GET_COURSE_COUNT_FN = "HOT_WUNI.WU_QUALIFICATIONS_PKG.GET_COURSE_COUNT_FN";
  public static final String TOP_NAV_COURSE_SEARCH_PRC = "HOT_WUNI.WHATUNI_SEARCH_PKG.GET_AJAX_SUBJECT_LIST_PRC";
  public static final String TOP_NAV_SEARCH_PRC = "HOT_WUNI.WHATUNI_SEARCH_PKG.GET_SEARCH_DETAILS_PRC";
  public static final String BEAN_ID_CPC_LANDING_BUSINESS = "ICPCLandingBusiness";
  public static final String OPENDAY_MOB_IMGSIZE = "_336px";
  public static final String OPENDAY_TAB_IMGSIZE = "_728px";
  public static final String OPENDAY_DESKTOP_IMGSIZE = "_979px";
  public static final String APP_FLAG = "N";
  
  public static final String GET_OPENDAY_HOME_PRC = "WUNI_OPENDAYS_PKG.GET_OPENDAY_HOMEPAGE_PRC";
  public static final String REMOVE_OPENDAY_PRC = "WUNI_OPENDAYS_PKG.REMOVE_OPENDAYS_PRC";
  public static final String GET_CLEARING_PROFILE_PRC = "HOT_WUNI.WUNI_CLEARING_PROFILE_PKG.GET_CLEARING_PROFILE_PRC";
  //Pre clearing page URLS added by Sujitha V on 02_JUNE_2020 rel
  public final String PRE_CLEARING_LANDING_URL = "/clearing-guide";
  public final String PRE_CLEARING_SUCCESS_URL = "/success/clearing-guide";
  public final String DOT_HTML                 = ".html";
  
  //File path for advice page 301 redirection added by Sujitha V on 02_JUNE_2020 rel
  public final String FILE_PATH_301_REDIRECTS = "/wu-cont/itext/whatuni_article_urls_20201208.xlsx";
  
  public final String CLEAR_ACCESS_TOKEN_KEY = "accessToken";
  public final String WU_AFFILIATE_ID = "affiliateId";
  public final String CLEARING_API_CONTEXT_PATH = "/clearing-api";
  //
  public final String CLEARING_STATS_FEATURED_PROVIDERS_BOOSTING = "HOTCOURSES: CLEARING: FEATURED PROVIDER: BOOSTING";// Added by Kailash L on 23_Jun_2020_rel JIRA 288
  
  public final String MAPPING_FOR_SEARCH_RESULTS = "/search-results";
  public final String MAPPING_FOR_PROVIDER_RESULTS = "/provider-results";
  public final String MAPPING_FOR_UNI_AJAX = "/search-results/university-ajax";
  public final String MAPPING_FOR_SUBJECT_AJAX = "/provider-results/subject-ajax";
  public final String MAPPING_FOR_GET_GRADE_FILTER_INFO_AJAX = "/get-grade-filter-info-ajax";
  public final String MAPPING_FOR_SAVE_GRADE_FILTER_INFO_AJAX = "/save-grade-filter-info-ajax";
  public final String MAPPING_FOR_DELETE_GRADE_FILTER_INFO_AJAX = "/delete-grade-filter-info-ajax";
  public final String WU_CONT_IMAGE = "/wu-cont/images/";
  
  public final String CLEARING_STATS_SPONSORED_WEBSITE_CLICK = "HOTCOURSES: CLEARING: SPONSORED LIST: WEBSITE CLICK";
  public final String CLEARING_STATS_SPONSORED_HOTLINE_CLICK = "HOTCOURSES: CLEARING: SPONSORED LIST: HOTLINE";
  public final String CLEARING_STATS_SPONSORED_BOOSTING = "HOTCOURSES: CLEARING: SPONSORED LIST: BOOSTING";
  public final String WEBSITE_APP_CODE="WU";
  
  public final String CLEARING_STATS_FEATURED_INSTITUTION_INTERNAL_LINK = "HOTCOURSES: CLEARING: FEATURED INSTITUTION: INTERNAL LINK";
  public final String CLEARING_STATS_FEATURED_INSTITUTION_EXTERNAl_LINK = "HOTCOURSES: CLEARING: FEATURED INSTITUTION: EXTERNAL LINK";
  public final String CLEARING_STATS_FEATURED_INSTITUTION_BOOSTING = "HOTCOURSES: CLEARING: FEATURED INSTITUTION: BOOSTING";
  public final String CLEARING_STATS_FEATURED_INSTITUTION_VIDEO_VIEW = "HOTCOURSES: CLEARING: FEATURED INSTITUTION: VIDEO VIEW";
  public final String CLEARING_STATS_FEATURED_INSTITUTION_HOTLINE = "HOTCOURSES: CLEARING: FEATURED INSTITUTION: HOTLINE";
  public final String CLEARING_STATS_FEATURED_INSTITUTION_WESITE = "HOTCOURSES: CLEARING: FEATURED INSTITUTION: WEBSITE CLICK";
  
  public final String STATS_SPONSORED_BOOSTING = "HOTCOURSES: SPONSORED LIST: BOOSTING";
  public final String COVID_SNIPPET_AJAX = "WUNI_ARTICLES_PKG.GET_COVID_SNIPPET_PRC";
  
  public final String SESSION_FEATURED_COLLEGEID_ORDERITEMID = "FEATURED_COLLEGEID_ORDERITEMID";
  
  public final String ALPHA_NUMERIC_WITH_SPACE = "^(?=.*[a-zA-Z])(?=.*[0-9])[A-Za-z0-9 ]+$";

  public static final String USER_REGISTERATION_BUFFER_APPEND = "##SPLIT##";
  public static final String USER_REGISTERATION_POST_CLEARING = "pageNameParam";
  public static final String USER_REGISTERATION_UTF8 = "UTF-8";
  public static final String USER_REGISTERATION_USERID = "__wuuid";
  public static final String USER_REGISTERATION_VWCNAME = "vwcname";
  public static final String USER_REGISTERATION_VWCOURSEID = "vwcourseId";
  public static final String USER_REGISTERATION_VWNID = "vwnid";
  public static final String USER_REGISTERATION_VWPDFCID = "vwpdfcid";
  public static final String USER_REGISTERATION_VWPRICE = "vwprice";
  public static final String USER_REGISTERATION_VWSEWF = "vwsewf";
  public static final String USER_REGISTERATION_VWSID = "vwsid";
  public static final String USER_REGISTERATION_VWURL = "vwurl";
  public static final String USER_REGISTERATION_WCACHE = "wcache";
  public final int FIFTY = 50;
  public static final String META_TITLE = "META_TITLE";
  public static final String META_DESC = "META_DESC";
  public static final String META_KEYWORD = "META_KEYWORD";
  public static final String CATEGORY_DESC = "CATEGORY_DESC";
  public static final String STUDY_DESC = "STUDY_DESC";
  public static final String META_ROBOTS = "META_ROBOTS";
  public static final String USER_JOURNEY_FLAG = "USER_JOURNEY_FLAG";
  public static final String CANONICAL_URL = "canonicalUrl";
  public static final String CLEARING_HEADER = "clearingHeader";
  public static final String HIGH_LIGHT_TAB = "highlightTab";
  public static final String INTERACTION_EXIST = "interactionExist";
  public static final String NEW_PROVIDER_TYPE = "newProviderType";
  public static final String REFERER = "referer";
  public static final String SET_PROFILE_TYPE = "setProfileType";
  public static final String USER_AGENT = "user-agent";
  public static final String USER_TRACK_ID = "userTrackId";
  public static final String COLLEGE_ID = "collegeId";
  public static final String COLLEGE_NAME = "collegeName";
  public static final String CPE_QUAL_NEWTWORK_ID = "cpeQualificationNetworkId";
  public static final String DISPLAY_OPENDAY_COUNT = "displayOpenDayCount";

  public final String SESSION_YOE_LOGGING = "D_SESSION_YOE_LOGGING";
  public static final String REGION = "region";
  public static final String SEARCH_TXT = "searchTxt";
  public static final String GET_UNSUBSCRIBE_PAGE_PRC = "HOT_WUNI.WU_USER_PROFILE_PKG.get_user_preference_prc";
  public static final String SAVE_USR_PREFERENCE_PRC = "HOT_WUNI.WU_USER_PROFILE_PKG.do_update_usr_preference_prc";
  public static final String GET_COLLEGE_URL = "HOT_WUNI.GET_COLLEGE_URL";
  public final String WEBSERVICE_CONTEXT_PATH = "/review-service";
  public final String MAPPING_FOR_CONTENTFUL_WEBSERVICE = "/getContentfulData";
  public static final String SYSVAR_IN_SESSION = "SYSVAR_IN_SESSION";
  public static final String YES = "YES";
  
  public static final String META_ROBOTS_NOINDEX_FOLLOW = "META_ROBOTS_NOINDEX_FOLLOW";
  public static final String NO_INDEX_FOLLOW = "noindex,follow";
  
  public static final String ERR_LOCATION = "err_location";
  public static final String ERR_STUDY_LEVEL_ID = "err_study_level_id";
  public static final String ERR_STUDY_LEVEL_NAME = "err_study_level_name";
  public static final String ERR_SUBJECT_NAME = "err_subject_name";

}
