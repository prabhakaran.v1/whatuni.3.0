package com.layer.dao.types;

import java.sql.Connection;
import java.sql.SQLException;

import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

/**
 * Class to contruct a Oracle Sql Array Type.
 *
 * @author:     Mohamed Syed
 * @version:    1.0
 * @since:      wu314_20110712
 *
 **/
public class OracleSqlArray {

  /**
   * method to convert the given String array to an Oracle sql array
   *
   * @param connection
   * @param typeName to represent the Database Type name.
   * @param inputArray as String array.
   * @return array as oracle.sql.ARRAY
   *
   */
  public ARRAY getSqlArray(Connection connection, String typeName, Object[][] inputArray) {
    ARRAY array = null;
    try {
      ArrayDescriptor descriptor = ArrayDescriptor.createDescriptor(typeName, connection);
      array = new ARRAY(descriptor, connection, inputArray);
    } catch (SQLException sQLException) {
      sQLException.printStackTrace();
    }
    return array;
  }

  /**
   * This method is to convert the given String array to an Oracle sql array
   *
   * @param connection
   * @param typeName to represent the Database Type name.
   * @param inputArray as String array.
   * @return array as oracle.sql.ARRAY
   *
   */
  public ARRAY getSqlArray(Connection connection, String typeName, Object[] inputArray) {
    ARRAY array = null;
    try {
      ArrayDescriptor descriptor = ArrayDescriptor.createDescriptor(typeName, connection);
      array = new ARRAY(descriptor, connection, inputArray);
    } catch (SQLException sQLException) {
      sQLException.printStackTrace();
    }
    return array;
  }

}
