package com.layer.dao.impl;

import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import spring.dao.sp.advice.CovidSnippetPodSP;
import spring.sp.advice.AdviceDetailInfoSP;
import spring.sp.advice.AdviceHomeInfoSP;
import spring.sp.advice.AdviceInfoAjaxSP;
import spring.sp.advice.AdviceSearchResultsSP;
import spring.sp.advice.ArticleListSP;
import spring.sp.advice.ArticlesPodSP;
import spring.sp.advice.CategoriesLandingInfoSP;
import spring.sp.advice.CheckArticleAndPrimaryCategorySP;
import spring.valueobject.advice.AdviceDetailInfoVO;
import spring.valueobject.advice.AdviceHomeInfoVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import com.layer.dao.IAdviceDAO;

/**
 * contains business logic for Articles
 *
 * @since        wu333_20141209
 * @author       Thiyagu G
 *
 */

public class IAdviceDAOImpl implements IAdviceDAO {

  private DataSource dataSource = null;

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  public DataSource getDataSource() {
    return this.dataSource;
  }

   /**
    * will return article-details page info
    * URL_PATTERN: www.whatuni.com/degrees/advice.html
    *
    *
    * @since        wu333_20141209 - redesign
    * @author       Thiyagu G
    * @param inputList
    * @return
    *
    */
   public Map getAdviceHomeInfo(List inputList) {
     Long startDbtime = new Long(System.currentTimeMillis());
     AdviceHomeInfoSP adviceHomeInfoSP = new AdviceHomeInfoSP(dataSource);
     Map resultmap = adviceHomeInfoSP.execute(inputList);
     Long endDbtime = new Long(System.currentTimeMillis());
     if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
       new AdminUtilities().logDbCallTimeDetails("wuni_articles_pkg.advice_land_page_prc", startDbtime, endDbtime);
     }
     return resultmap;
   }

    public Map getPrimaryCategoryLandingInfo(List inputList) {
      Long startDbtime = new Long(System.currentTimeMillis());
      CategoriesLandingInfoSP categoriesLandingInfoSP = new CategoriesLandingInfoSP(dataSource);
      String categoryFlag = "primary";
      Map resultmap = categoriesLandingInfoSP.execute(inputList, categoryFlag);
      Long endDbtime = new Long(System.currentTimeMillis());
      if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
        new AdminUtilities().logDbCallTimeDetails("wuni_articles_pkg.category_land_page_prc", startDbtime, endDbtime);
      }
      return resultmap;
    }
    
    public Map getSecondaryCategoryLandingInfo(List inputList) {
      Long startDbtime = new Long(System.currentTimeMillis());
      CategoriesLandingInfoSP categoriesLandingInfoSP = new CategoriesLandingInfoSP(dataSource);
      String categoryFlag = "secondary";
      Map resultmap = categoriesLandingInfoSP.execute(inputList, categoryFlag);
      Long endDbtime = new Long(System.currentTimeMillis());
      if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
        new AdminUtilities().logDbCallTimeDetails("wuni_articles_pkg.category_land_page_prc", startDbtime, endDbtime);
      }
      return resultmap;
    }
    
    public Map getAdviceSearchResults(List inputList) {
      Long startDbtime = new Long(System.currentTimeMillis());
      AdviceSearchResultsSP adviceSearchResultsSP = new AdviceSearchResultsSP(dataSource);
      String categoryFlag = "secondary";
      Map resultmap = adviceSearchResultsSP.execute(inputList, categoryFlag);
      Long endDbtime = new Long(System.currentTimeMillis());
      if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
        new AdminUtilities().logDbCallTimeDetails("wuni_articles_pkg.get_search_results_prc", startDbtime, endDbtime);
      }
      return resultmap;
    }
    
    public Map getAdviceDetailInfo(List inputList) {
      Long startDbtime = new Long(System.currentTimeMillis());
      AdviceDetailInfoSP adviceDetailInfoSP = new AdviceDetailInfoSP(dataSource);      
      Map resultmap = adviceDetailInfoSP.execute(inputList);
      Long endDbtime = new Long(System.currentTimeMillis());
      if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
        new AdminUtilities().logDbCallTimeDetails("wuni_articles_pkg.get_article_details_info_prc", startDbtime, endDbtime);
      }
      return resultmap;
    }
  
  /**
   * will return trending/most popular advice info
   * 
   * @since wu546_20151103
   * @author Prabhakaran V.
   * @param inputList
   * @return Map as advice info
   */
  public Map getAdviceAjaxDetail(List inputList) {
    AdviceInfoAjaxSP adviceInfoAjaxSP = new AdviceInfoAjaxSP(dataSource);      
    Map resultmap = adviceInfoAjaxSP.execute(inputList);
    return resultmap;
  }
  
  public Map checkArticleAndPrimaryCategoryAvilable(AdviceDetailInfoVO adviceDetailInfoVO) {
    CheckArticleAndPrimaryCategorySP checkArticleAndPrimaryCategorySP = new CheckArticleAndPrimaryCategorySP(dataSource);      
    Map resultmap = checkArticleAndPrimaryCategorySP.execute(adviceDetailInfoVO);
    return resultmap;
  }
  //
  public Map getArticlesPod(AdviceHomeInfoVO adviceHomeInfoVO) throws Exception { //Get the articles pod on 16_May_2017, By Thiyagu G.
    ArticlesPodSP articlesPodSP = new ArticlesPodSP(dataSource);
    Map resultmap = articlesPodSP.execute(adviceHomeInfoVO);      
    return resultmap;
  }
  
  /**
   * will return article-details for clearing landing page
   *
   *
   * @since        wu588_20190423
   * @author       Sabapathi S
   * @param inputList
   * @return
   *
   */
  public Map getArticleList(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    ArticleListSP articleListSP = new ArticleListSP(dataSource);
    Map resultmap = articleListSP.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("wuni_articles_pkg.get_page_featured_post_fn", startDbtime, endDbtime);
    }
    return resultmap;
  }
  
  public Map getCovidSnippetPod(AdviceHomeInfoVO adviceHomeInfoVO) throws Exception {
	CovidSnippetPodSP coviSnippetPodSP = new CovidSnippetPodSP(dataSource);
    Map resultmap = coviSnippetPodSP.execute(adviceHomeInfoVO);      
    return resultmap;
  }
}