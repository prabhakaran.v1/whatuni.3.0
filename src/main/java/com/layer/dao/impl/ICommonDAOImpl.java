package com.layer.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import mobile.dao.sp.MobileCourseDetailsSP;
import mobile.valueobject.ProfileVO;
import mobile.valueobject.SearchVO;
import spring.dao.sp.clearing.ClearingProfileSP;
import spring.dao.sp.common.GetCovid19DataSP;
import spring.dao.sp.common.GetSysvarsCallSP;
import spring.dao.util.valueobject.clearing.ClearingProfileVO;
import spring.gradefilter.bean.SRGradeFilterBean;
import spring.sp.CollegeBrowseSubjectListSP;
import spring.sp.CollegeProfileSP;
import spring.sp.MoreVideoListSP;
import spring.sp.OpenDaysBrowseSP;
import spring.sp.OpenDaysInfoSP;
import spring.sp.ReviewSearchResultsSP;
import spring.sp.SubjectProfileListSP;
import spring.sp.SubjectUniListSP;
import spring.sp.UniVideoReviewSP;
import spring.sp.blogs.BlogHomeInfoSP;
import spring.sp.blogs.PostHomeInfoSP;
import spring.sp.blogs.RssBlogArticleSP;
import spring.sp.browsebylocation.BrowseUgCoursesByLocationSP;
import spring.sp.browsebylocation.BrowseUgCoursesBySubjectSP;
import spring.sp.clearing.ClearingSubjectLandingSP;
import spring.sp.coursejourney.JourneyCategorySP;
import spring.sp.coursejourney.JourneyHomeSP;
import spring.sp.coursejourney.LocationBrowseSP;
import spring.sp.coursejourney.LocationUniResultSP;
import spring.sp.coursejourney.RegionListSP;
import spring.sp.coursejourney.WhyStudyGuideSP;
import spring.sp.gradefilter.DeleteSRGradeFilterSP;
import spring.sp.gradefilter.SRGradeFilterSP;
import spring.sp.gradefilter.SaveSRGradeFilterSP;
import spring.sp.home.ReviewHomeSP;
import spring.sp.members.ViewAllMembersSP;
import spring.sp.prospectus.BulkProspectusCollegesSP;
import spring.sp.search.MoneyPageMainContentSP;
import spring.valueobject.articles.RssBlogArticleVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.basket.form.BasketBean;
import WUI.registration.bean.RegistrationBean;
import WUI.registration.bean.UserLoginBean;
import WUI.review.bean.ReviewListBean;
import WUI.search.form.CourseDetailsBean;
import WUI.search.form.SearchBean;
import WUI.utilities.GlobalConstants;
import WUI.whatunigo.bean.GradeFilterBean;

import com.layer.dao.ICommonDAO;
import com.layer.dao.sp.AddProspectusBasketSP;
import com.layer.dao.sp.CheckDegreeCourseSP;
import com.layer.dao.sp.CheckDeletedProviderFlagSP;
import com.layer.dao.sp.CheckIfL2NodeSP;
import com.layer.dao.sp.GetCategoryDetailsSP;
import com.layer.dao.sp.GetInsightDataSP;
import com.layer.dao.sp.GetInstitutionIdSP;
import com.layer.dao.sp.GetPGUniversityNewUrlSP;
import com.layer.dao.sp.GetPdfDataSP;
import com.layer.dao.sp.GetShtmlDataSP;
import com.layer.dao.sp.MegaMenuDegreesSP;
import com.layer.dao.sp.MegaMenuSP;
import com.layer.dao.sp.NavigationUrlSP;
import com.layer.dao.sp.ProsPostEnquirySuggestionSP;
import com.layer.dao.sp.ProspectusResultsSP;
import com.layer.dao.sp.SocialMessageSP;
import com.layer.dao.sp.UpdateChatBotDisbaleSP;
import com.layer.dao.sp.VersionChangesSP;
import com.layer.dao.sp.common.GetCookieLatestUpdatedDateSP;
import com.layer.dao.sp.common.GetYearOfEntrySP;
import com.layer.dao.sp.contenthub.GetContentHubDetailsSP;
import com.layer.dao.sp.contenthub.LoadOpendayPopupSP;
import com.layer.dao.sp.coursesearch.CourseSearchSP;
import com.layer.dao.sp.coursesearch.GetPopularSubjectsSP;
import com.layer.dao.sp.downloadpdf.GetUserProfilePdfSP;
import com.layer.dao.sp.home.AboutUsPagesSP;
import com.layer.dao.sp.home.KeyStatisticsCountSP;
import com.layer.dao.sp.home.UpdateUserTimeLinePodSP;
import com.layer.dao.sp.interstitialsearch.SubjectListSP;
import com.layer.dao.sp.login.CheckEmailBlockedSP;
import com.layer.dao.sp.login.CheckEmailExistSP;
import com.layer.dao.sp.login.CheckUserAuthenticationSP;
import com.layer.dao.sp.login.GetBasketCountSP;
import com.layer.dao.sp.login.GetCourseSubjectSP;
import com.layer.dao.sp.login.GetUserInformationLoginSP;
import com.layer.dao.sp.login.GetUserNameSP;
import com.layer.dao.sp.login.GetUserNamesSP;
import com.layer.dao.sp.login.GetUserYearOfEntrySP;
import com.layer.dao.sp.login.GetWUSysVarValueSP;
import com.layer.dao.sp.login.PreLoginUpdateStatusCodeSP;
import com.layer.dao.sp.myfinalchoice.ConfirmFinalChoicePodSP;
import com.layer.dao.sp.myfinalchoice.EditMyFinalChoiceSP;
import com.layer.dao.sp.myfinalchoice.FinalChUsrActionPodSP;
import com.layer.dao.sp.myfinalchoice.FinalChoiceSuggestionListSP;
import com.layer.dao.sp.myfinalchoice.MyFinalChoiceHomeSP;
import com.layer.dao.sp.myfinalchoice.SaveFinalChoiceDataSP;
import com.layer.dao.sp.myfinalchoice.SaveUpdateChoiceSP;
import com.layer.dao.sp.myfinalchoice.SwapFinalChoicePodSP;
import com.layer.dao.sp.myfinalchoice.UpdateBasketCourseSP;
import com.layer.dao.sp.mywhatuni.EditConfirmChoiceSP;
import com.layer.dao.sp.mywhatuni.MyComparisonBasketSP;
import com.layer.dao.sp.mywhatuni.MyWUOpendayAjaxSP;
import com.layer.dao.sp.mywhatuni.MyWhatuniGetdataSP;
import com.layer.dao.sp.mywhatuni.MyWhatuniImageUpdateSP;
import com.layer.dao.sp.mywhatuni.MyWhatuniUserAttrSP;
import com.layer.dao.sp.mywhatuni.ReorderChoiceSP;
import com.layer.dao.sp.mywhatuni.SuggestedComparisonPodSP;
import com.layer.dao.sp.mywhatuni.UpdateCaTrackFlagSP;
import com.layer.dao.sp.opendays.OpenDaySearchResultsSP;
import com.layer.dao.sp.opendays.OpenDaysLandingDateSP;
import com.layer.dao.sp.opendays.OpenDaysLandingLocationSP;
import com.layer.dao.sp.opendays.OpenDaysWeekDateInfoSP;
import com.layer.dao.sp.opendays.RemoveMyOpenDaysSP;
import com.layer.dao.sp.opendays.StoreReserveOpenDaysUniSP;
import com.layer.dao.sp.ql.BasicFormSubmissionSP;
import com.layer.dao.sp.ql.EnqBasketSubmitSP;
import com.layer.dao.sp.ql.OneclickEnquirySubmitSP;
import com.layer.dao.sp.ql.PostenquirySubmissionSP;
import com.layer.dao.sp.ql.QLAdvanceFormSubmissionSP;
import com.layer.dao.sp.ql.QLBasicformSP;
import com.layer.dao.sp.registration.ResetPasswordSP;
import com.layer.dao.sp.registration.UpdatePasswordSP;
import com.layer.dao.sp.review.GetReviewBreakDownAjaxSP;
import com.layer.dao.sp.review.GetSubjectReviewsAjaxSP;
import com.layer.dao.sp.review.ReviewSubjectListSP;
import com.layer.dao.sp.search.GetCourseNameSP;
import com.layer.dao.sp.search.NewCollegeDetailsSP;
import com.layer.dao.sp.search.ProviderResultMainContentSP;
import com.layer.dao.sp.search.ProviderResultsSP;
import com.layer.dao.sp.search.advancesearch.AdvanceProviderResultsSP;
import com.layer.dao.sp.search.srgradefilter.DeleteUserRecordSP;
import com.layer.dao.sp.search.srgradefilter.SRGetMatchingCourseSP;
import com.layer.dao.sp.search.srgradefilter.SRGradeFilterPageSP;
import com.layer.dao.sp.search.srgradefilter.SRQualSubjectAjaxSP;
import com.layer.dao.sp.search.srgradefilter.SRSaveGradeFilterPageSP;
import com.layer.dao.sp.search.srgradefilter.SRUcasPointsAjaxSP;
import com.layer.dao.sp.studentawards.AwardsYearListSP;
import com.layer.dao.sp.studentawards.StudentChoiceAwardsSP;
import com.layer.dao.sp.topnavsearch.TopNavCourseSearchSP;
import com.layer.dao.sp.ucascalculator.GetYearOfEntryListSP;
import com.layer.dao.sp.ucascalculator.UCASCalcTariffTableAjaxSP;
import com.layer.dao.sp.ucascalculator.UCASCalculatorPageSP;
import com.layer.dao.sp.ucascalculator.UCASCalculatorScoreAjaxSP;
import com.layer.dao.sp.ucascalculator.UcasSubjectSubmitSP;
import com.layer.dao.sp.uni.CheckCourseExistsSP;
import com.layer.dao.sp.uni.LogProfileTabStatsSP;
import com.layer.dao.sp.uni.NewUniLandingSP;
import com.layer.dao.sp.uni.RichProfileUniLandingSP;
import com.layer.dao.sp.whatunigo.GetMatchingCourseSP;
import com.layer.dao.sp.whatunigo.GradeFilterPageSP;
import com.layer.dao.sp.whatunigo.GradeFilterValidationSP;
import com.layer.dao.sp.whatunigo.QualSubjectAjaxSP;
import com.layer.dao.sp.whatunigo.SaveGradeFilterPageSP;
import com.layer.dao.sp.whatunigo.UcasPointsAjaxSP;
import com.layer.dao.sp.whatunigo.WhatuniGoCourseInfoSP;
import com.layer.dao.sp.whatunigo.clearing.CourseListSP;
import com.layer.dao.sp.whatunigo.clearing.GetSubjectLandingRegionListSP;
import com.wuni.advertiser.ql.form.QLFormBean;
import com.wuni.ajax.AdRollMarketingSP;
import com.wuni.ajax.BrowseUniAutoCompleteSP;
import com.wuni.ajax.KeywordBrowseNodesDesktopSP;
import com.wuni.ajax.KeywordBrowseNodesSP;
import com.wuni.ajax.OpendayKeywordBrowseNodesDesktopSP;
import com.wuni.ajax.RecentSearchAjaxSP;
import com.wuni.ajax.UcasKeywordBrowseNodesSP;
import com.wuni.app.sp.AppLandingPageSignupSP;
import com.wuni.basket.GetBasketDetailsSP;
import com.wuni.basket.GetUserBasketDetailsSP;
import com.wuni.basket.SaveBasketSP;
import com.wuni.myfinalchoice.form.MyFinalChoiceBean;
import com.wuni.mywhatuni.MywhatuniInputBean;
import com.wuni.mywhatuni.form.MyCompareBean;
import com.wuni.review.sp.GetReviewLightBoxSP;
import com.wuni.scholarship.form.ScholarshipFormBean;
import com.wuni.scholarship.sp.ScholarshipSP;
import com.wuni.search.sp.GetCourseSchemaSP;
import com.wuni.ultimatesearch.GetUltimateSearchInputdetailsSP;
import com.wuni.ultimatesearch.SaveULInputbean;
import com.wuni.ultimatesearch.SaveUltimateSearchDetailsSP;
import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;
import com.wuni.ultimatesearch.iwanttobe.sp.IWanttobeEmpSortPaginationSP;
import com.wuni.ultimatesearch.iwanttobe.sp.IWanttobeEmployementInfoAjaxSP;
import com.wuni.ultimatesearch.iwanttobe.sp.IWanttobeEmployementInfoSP;
import com.wuni.ultimatesearch.iwanttobe.sp.IWanttobeLoadUniEmpInfoSP;
import com.wuni.ultimatesearch.iwanttobe.sp.IWanttobeSearchResultsSP;
import com.wuni.ultimatesearch.iwanttobe.sp.IWanttobeYearOfEntrySP;
import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;
import com.wuni.ultimatesearch.whatcanido.sp.EmpRateSortPaginationSP;
import com.wuni.ultimatesearch.whatcanido.sp.LoadUniEmploymentInfoSP;
import com.wuni.ultimatesearch.whatcanido.sp.WhatcanidoEmpInfoAjaxSP;
import com.wuni.ultimatesearch.whatcanido.sp.WhatcanidoEmpInfoSP;
import com.wuni.ultimatesearch.whatcanido.sp.WhatcanidoResultsSP;
import com.wuni.util.form.opendays.OpenDaysBean;
import com.wuni.util.form.studentawards.StudentAwardsBean;
import com.wuni.util.valueobject.AutoCompleteVO;
import com.wuni.util.valueobject.VersionChangesVO;
import com.wuni.util.valueobject.advert.NewUniLandingVO;
import com.wuni.util.valueobject.advert.ProspectusVO;
import com.wuni.util.valueobject.advert.RichProfileVO;
import com.wuni.util.valueobject.contenthub.ContentHubVO;
import com.wuni.util.valueobject.contenthub.OpendayVO;
import com.wuni.util.valueobject.coursesearch.CourseSearchVO;
import com.wuni.util.valueobject.interstitialsearch.SubjectAjaxVO;
import com.wuni.util.valueobject.mywhatuni.MyWhatuniInputVO;
import com.wuni.util.valueobject.openday.OpendaySearchVO;
import com.wuni.util.valueobject.openday.OpendaysVO;
import com.wuni.util.valueobject.review.ReviewListVO;
import com.wuni.util.valueobject.review.UserReviewsVO;
import com.wuni.util.valueobject.search.CourseDetailsVO;
import com.wuni.valueobjects.AboutUsPagesVO;
import com.wuni.valueobjects.FinalFiveChoiceVO;
import com.wuni.valueobjects.InsightsVO;
import com.wuni.valueobjects.itext.UserDownloadPdfBean;
import com.wuni.valueobjects.whatunigo.ApplyNowVO;
import com.wuni.whatcanido.sp.GetDBNameSP;
import com.wuni.whatcanido.sp.WhatCanIDoUniInfoSP;
import com.wuni.whatcanido.sp.WhatcanidoQualificationSP;

public class ICommonDAOImpl implements ICommonDAO {

  private DataSource dataSource = null;

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  public DataSource getDataSource() {
    return dataSource;
  }
  
  // Added by Saba for Content hub profile 23_JAN_18
  public Map getContentHubDetails(ContentHubVO contentHubVO) {
    GetContentHubDetailsSP contentHubDetailsSP = new GetContentHubDetailsSP(dataSource);    
    Map contentHubMap = contentHubDetailsSP.execute(contentHubVO);
    return contentHubMap;
  } 
  
  // Added by Amir for Rich profile Landing page 13_JAN_15
  public Map getRichProfileUniLandingData(RichProfileVO richProfileVO) {
    RichProfileUniLandingSP richProfileSP = new RichProfileUniLandingSP(dataSource);    
    Map richProfileMap = richProfileSP.execute(richProfileVO);
    return richProfileMap;
  }  
  public Map versionChanges(VersionChangesVO versionChangesVO) {
    VersionChangesSP versionChangesSP = new VersionChangesSP(dataSource);
    Map versionMap = versionChangesSP.execute(versionChangesVO);
    return versionMap;
  }
  // Added by Amir for all profile Landing page 03_NOV_15
  public Map getNewUniLandingData(NewUniLandingVO newUniLandingVO){
    NewUniLandingSP newUniLandingSP = new NewUniLandingSP(dataSource);
    Map newUniLandingMap = newUniLandingSP.execute(newUniLandingVO);
    return newUniLandingMap;            
  }    
  
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map getMyFinalChoiceData(MyFinalChoiceBean myFinalChoiceBean){
    MyFinalChoiceHomeSP myChoiceHomeSP = new MyFinalChoiceHomeSP(dataSource);
    Map myChoiceHomeMap = myChoiceHomeSP.execute(myFinalChoiceBean);
    return myChoiceHomeMap;
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map getFinalChoiceSuggestionList(MyFinalChoiceBean myFinalChoiceBean){
    FinalChoiceSuggestionListSP finalChoiceSuggestionSP = new FinalChoiceSuggestionListSP(dataSource);
    Map finalChoiceSuggestionMap = finalChoiceSuggestionSP.execute(myFinalChoiceBean);
    return finalChoiceSuggestionMap;
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15  
  public Map saveFinalChoiceData(MyFinalChoiceBean myFinalChoiceBean){
    SaveFinalChoiceDataSP saveChoiceDataSP = new SaveFinalChoiceDataSP(dataSource);
    Map saveChoiceDataMap = saveChoiceDataSP.execute(myFinalChoiceBean);
    return saveChoiceDataMap;
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map confirmUsrActionPod(MyFinalChoiceBean myFinalChoiceBean){
    ConfirmFinalChoicePodSP confirmChoicePodSP = new ConfirmFinalChoicePodSP(dataSource);
    Map confirmChoiceMap = confirmChoicePodSP.execute(myFinalChoiceBean);
    return confirmChoiceMap;
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map getFinalChUsrActionPod(MyFinalChoiceBean myFinalChoiceBean){
    FinalChUsrActionPodSP fnChUsrActionSP = new FinalChUsrActionPodSP(dataSource);
    Map fnChUsrActionMap = fnChUsrActionSP.execute(myFinalChoiceBean);
    return fnChUsrActionMap;
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map swapFinalChoicePod(MyFinalChoiceBean myFinalChoiceBean){
    SwapFinalChoicePodSP swapChoicePodSP = new SwapFinalChoicePodSP(dataSource);
    Map swapChoicePodMap = swapChoicePodSP.execute(myFinalChoiceBean);
    return swapChoicePodMap;
  }
  
  // Added by Amir for MyFinalChoice 21_JUL_15
  public Map editMyFinalChoicePod(MyFinalChoiceBean myFinalChoiceBean){
    EditMyFinalChoiceSP editMyFinalSP = new EditMyFinalChoiceSP(dataSource);
    Map editMyFinalMap = editMyFinalSP.execute(myFinalChoiceBean);
    return editMyFinalMap;
  }
  
   //Added by Indumathi May_31_2016 For Tab changes
  public Map checkCourseExists(NewUniLandingVO newUniLandingVO){
    CheckCourseExistsSP checkCourseExistsSP = new CheckCourseExistsSP(dataSource);
    Map courseExistsMap = checkCourseExistsSP.execute(newUniLandingVO);
    return courseExistsMap;
  }
 
  //To get the list of institution to display in open days browse
  public Map getOpenDaysBrowse(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    OpenDaysBrowseSP opendaysbrowse = new OpenDaysBrowseSP(dataSource);
    Map resultmap = opendaysbrowse.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_OPENDAYS_BROWSE_DATA_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //To get the information about the openday for the college  

  public Map getOpenDaysInfo(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    OpenDaysInfoSP opendaysinfo = new OpenDaysInfoSP(dataSource);
    Map resultmap = opendaysinfo.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_OPENDAYS_INFO_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  
  // To store Reserve openday unis list 17_MAR_2015 By Amir
  public Map saveReserveOpendayUniList(OpenDaysBean openDaysBean){
    StoreReserveOpenDaysUniSP storeReserveOpenDaysUniSP = new StoreReserveOpenDaysUniSP(dataSource);
    Map storeReserveOpenDaysUniMap = storeReserveOpenDaysUniSP.execute(openDaysBean);
    return storeReserveOpenDaysUniMap;
  }
  //To check the provider is deleted or not flag 16_FEB_2016_REL By Prabha
  public Map getProviderDeletedFlag(ReviewListBean reviewBean){
    CheckDeletedProviderFlagSP checkDeletedProviderFlagSP = new CheckDeletedProviderFlagSP(dataSource);
    Map deletedProviderFlag = checkDeletedProviderFlagSP.execute(reviewBean);
    return deletedProviderFlag;
  }
  //To check the course is degree or not flag 16_FEB_2016_REL By Prabha
  public Map getDegreeCourseFlag(CourseDetailsBean courseDetailBean){
    CheckDegreeCourseSP checkDegreeCourseSP = new CheckDegreeCourseSP(dataSource);
    Map deletedProviderFlag = checkDegreeCourseSP.execute(courseDetailBean);
    return deletedProviderFlag;
  }
  //To get the college written review list to display in unilanmding Tab

  public Map getUniVideoReviews(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    UniVideoReviewSP univideoreviews = new UniVideoReviewSP(dataSource);
    Map resultmap = univideoreviews.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_UNI_VIDEO_DATA_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
    
  //To get review search Results, 03_Feb_2015 by Amir
  public Map getReviewResultList(ReviewListVO reviewListVO){
    ReviewSearchResultsSP reviewSrchResultsSP = new ReviewSearchResultsSP(dataSource);
    Map reviewSrchResultsMap = reviewSrchResultsSP.execute(reviewListVO);
    return reviewSrchResultsMap;
  }
    
  //Display all video review - view all video reviews

  public Map getMoreVideoList(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    MoreVideoListSP morevideolist = new MoreVideoListSP(dataSource);
    Map resultmap = morevideolist.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_MORE_VIDEO_DATA_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //To Get the information for the college profile 

  public Map getCollegeProfileInfo(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    CollegeProfileSP collegeprofileinfo = new CollegeProfileSP(dataSource);
    Map resultmap = collegeprofileinfo.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_COLLEGE_PROFILE_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //To get the list of subject that has been purchased by the college

  public Map getSubjectProfileList(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    SubjectProfileListSP subjectprofilelist = new SubjectProfileListSP(dataSource);
    Map resultmap = subjectprofilelist.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_SUBJECT_PROFILE_LIST_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
 
  //To get the instition list which has bought the same subject profile 
  public Map getSubjectUniList(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    SubjectUniListSP subjectunilist = new SubjectUniListSP(dataSource);
    Map resultmap = subjectunilist.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_SUBJECT_UNI_LIST_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //To get the all the members list when user click the view all members link 

  public Map getViewAllMembersList(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    ViewAllMembersSP viewallmembers = new ViewAllMembersSP(dataSource);
    Map resultmap = viewallmembers.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_VIEW_ALL_MEMBERS_LIST_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  ////To get the data that are related to review home page

  public Map getReviewHomePageData(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    ReviewHomeSP reviewhomedata = new ReviewHomeSP(dataSource);
    Map resultmap = reviewhomedata.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_REVIEW_HOME_DATA_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  // To get the data that are related to location to display in the following page 
  // http://www.whatuni.com/degrees/university-colleges-uk/uni-browse.html

  public Map getLocationBrowseList(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    LocationBrowseSP locationdata = new LocationBrowseSP(dataSource);
    Map resultmap = locationdata.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_UNI_LOCATION_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //To get the data that are related university list to display in the following page
  //http://www.whatuni.com/degrees/university-colleges-uk/university-colleges-england/3/1/universities.html

  public Map getLocationUniversityList(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    LocationUniResultSP universitydata = new LocationUniResultSP(dataSource);
    Map resultmap = universitydata.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_BROWSE_LOCATION_UNI_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //To get the data that are related to the study level list to display in journey home
  //http://www.whatuni.com/degrees/courses/browse.html

  public Map getJourneyHomeDataList(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    JourneyHomeSP journeyhomedata = new JourneyHomeSP(dataSource);
    Map resultmap = journeyhomedata.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_COURSE_STUDY_LEVEL_FN", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //To get the data that are related to the category list to display in journey category home
  //http://www.whatuni.com/degrees/courses/Degree-UK/qualification/M/list.html

  public Map getJourneyCategoryDataList(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    JourneyCategorySP journeycategorydata = new JourneyCategorySP(dataSource);
    Map resultmap = journeycategorydata.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_BROWSE_CATEGORY_LIST_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //To get the data that has to display in the browse journey region home page 
  //http://www.whatuni.com/degrees/courses/Degree-list/Accounting-Degree-courses-UK/qualification/M/search_category/5934/loc.html

  public Map getJourneyLocationDataList(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    RegionListSP journeylocationdata = new RegionListSP(dataSource);
    Map resultmap = journeylocationdata.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_BROWSE_PAGE_DATA_PRC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //To get the data that will display categroy names only produces the search result in the unilanding page tab

  public Map getCollegeBrowseSubjectList(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    CollegeBrowseSubjectListSP collegesubjectlist = new CollegeBrowseSubjectListSP(dataSource);
    Map resultmap = collegesubjectlist.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_COLLEGE_SUBJECT_LIST_PRC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //This will return money-page's main details(headerId, midcontent, refineByContent, numberOfTotalRecordsForThatSearchSynario)

  public Map getMoneyPageMainContent(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    MoneyPageMainContentSP moneyPageMainContent = new MoneyPageMainContentSP(dataSource);
    Map resultmap = moneyPageMainContent.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WHATUNI_SEARCH3.GET_MONEY_PAGE_MAIN_CONTENT", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //this will return list of college details to bulk prospectus page.

  public Map getBulkProspectusColleges(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    BulkProspectusCollegesSP bulkProspectusColleges = new BulkProspectusCollegesSP(dataSource);
    Map resultmap = bulkProspectusColleges.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WHATUNI_SEARCH3.GET_BULK_PROSPECTUS_COLLEGES", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //this will return details to provider result page

  public Map getProviderResultPageMainContent(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    ProviderResultMainContentSP providerResultPageMainContentSP = new ProviderResultMainContentSP(dataSource);
    Map resultmap = providerResultPageMainContentSP.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WHATUNI_SEARCH3.GET_PROVIDER_RESULT_PAGE", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //This will return blog's home page details

  public Map getBlogHomeInfo(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    BlogHomeInfoSP blogHomeInfo = new BlogHomeInfoSP(dataSource);
    Map resultmap = blogHomeInfo.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WU_BLOGS_PKG.GET_BLOG_HOME_INFO", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //This will return post's home page details

  public Map getPostHomeInfo(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    PostHomeInfoSP postHomeInfoSP = new PostHomeInfoSP(dataSource);
    Map resultmap = postHomeInfoSP.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WU_BLOGS_PKG.GET_POST_HOME_INFO", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //This will return BrowseUgCoursesByLocation page's all top to bottom details

  public Map getBrowseUgCoursesByLocation(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    BrowseUgCoursesByLocationSP browseUgCoursesByLocationSP = new BrowseUgCoursesByLocationSP(dataSource);
    Map resultmap = browseUgCoursesByLocationSP.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WU_BROWSE_BY_LOCATION_PKG.GET_BROWSE_BY_LOCATION_PRC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //This will return BrowseUgCoursesBySubject page's all top to bottom details

  public Map getBrowseUgCoursesBySubject(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    BrowseUgCoursesBySubjectSP browseUgCoursesBySubjectSP = new BrowseUgCoursesBySubjectSP(dataSource);
    Map resultmap = browseUgCoursesBySubjectSP.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WU_BROWSE_BY_LOCATION_PKG.GET_BROWSE_BY_SUBJECT_PRC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //This will return ArticleGroupsHome all top to bottom details

  public Map getClearingSubjectLandingPage(List inputList){//## Start of WU11042012_RELEASE_SEKHAR K
    Long startDbtime = new Long(System.currentTimeMillis());
    ClearingSubjectLandingSP clearingSubjectLandingSP = new ClearingSubjectLandingSP(dataSource);
    Map resultmap = clearingSubjectLandingSP.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if(TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG){
      new AdminUtilities().logDbCallTimeDetails("WU_CLEARING_PKG.clearing_subject_page_prc", startDbtime, endDbtime);
    }
    return resultmap;
  }     
    
  public Map getQlBasicFormInfo(QLFormBean qlFormBean){
    Long startDbtime = new Long(System.currentTimeMillis());
    QLBasicformSP qlBasicformSP = new QLBasicformSP(dataSource);
    Map resultmap = qlBasicformSP.execute(qlFormBean);
    Long endDbtime = new Long(System.currentTimeMillis());
    if(TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG){
      new AdminUtilities().logDbCallTimeDetails("HOT_WUNI.GET_ENQUIRY_DETAILS_PRC", startDbtime, endDbtime);
    }
    return resultmap;
  }
    
  public Map basicFormSubmissionDBCall(QLFormBean qlFormBean) {
    Long startDbtime = new Long(System.currentTimeMillis());
    BasicFormSubmissionSP basicFormSP = new BasicFormSubmissionSP(dataSource);
    Map resultmap = basicFormSP.execute(qlFormBean);
    Long endDbtime = new Long(System.currentTimeMillis());
    if(TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG){
      new AdminUtilities().logDbCallTimeDetails("WU_INTERACTION_PKG.WU_BASIC_FORM_SUBMISSION_PRC", startDbtime, endDbtime);
    }
    return resultmap;
  } 
  
  public Map qlAdvanceFormSubmission(List inputList, Object[][] attrValues){
    Long startDbtime = new Long(System.currentTimeMillis());
    QLAdvanceFormSubmissionSP qlAdvanceFormSubSP = new QLAdvanceFormSubmissionSP(dataSource);
    Map resultmap = qlAdvanceFormSubSP.execute(inputList, attrValues);
    Long endDbtime = new Long(System.currentTimeMillis());
    if(TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG){
      new AdminUtilities().logDbCallTimeDetails("WU_INTERACTION_PKG.WU_BASIC_FORM_SUBMISSION_PRC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  public Map getMyWhatuniData(MywhatuniInputBean inputBean){
    Long startDbtime = new Long(System.currentTimeMillis());
    MyWhatuniGetdataSP mywhatuniGetData = new MyWhatuniGetdataSP(dataSource);
    Map resultmap = mywhatuniGetData.execute(inputBean);
    Long endDbtime = new Long(System.currentTimeMillis());
    if(TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG){
      new AdminUtilities().logDbCallTimeDetails("", startDbtime, endDbtime);
    }
    return resultmap;  
  }
  
  public Map myWhatuniUserAttrValues(MyWhatuniInputVO inputBean, Object[][] attrValues){//Modified list to bean Indumathi Jan-27-16
    Long startDbtime = new Long(System.currentTimeMillis());
    MyWhatuniUserAttrSP mywhatuniAttrSP = new MyWhatuniUserAttrSP(dataSource);
    Map resultmap = mywhatuniAttrSP.execute(inputBean, attrValues);
    Long endDbtime = new Long(System.currentTimeMillis());
    if(TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG){
      new AdminUtilities().logDbCallTimeDetails("wu_user_profile_pkg.store_user_prof_attr_val_prc", startDbtime, endDbtime);
    }
    return resultmap;
  }
  
 //not used
  public Map PostenquirySubmission(List inputList, Object[][] postValues){
    Long startDbtime = new Long(System.currentTimeMillis());
    PostenquirySubmissionSP postenquirySP = new PostenquirySubmissionSP(dataSource);
    Map resultmap = postenquirySP.execute(inputList, postValues);
    Long endDbtime = new Long(System.currentTimeMillis());
    if(TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG){
      new AdminUtilities().logDbCallTimeDetails("wu_interaction_pkg.wu_post_enq_submit_prc", startDbtime, endDbtime);
    }
    return resultmap;
  }
  public Map mywhatuniPostenquiry(List inputList, Object[][] mywhatuniPostValues){
    Long startDbtime = new Long(System.currentTimeMillis());
    PostenquirySubmissionSP postenquirySP = new PostenquirySubmissionSP(dataSource);
    Map resultmap = postenquirySP.execute(inputList, mywhatuniPostValues);
    Long endDbtime = new Long(System.currentTimeMillis());
    if(TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG){
      new AdminUtilities().logDbCallTimeDetails("wu_user_profile_pkg.wu_post_enq_pros_submit_prc", startDbtime, endDbtime);
    }
    return resultmap;
  }
  
  public RssBlogArticleVO rssArticleBlogXml(RssBlogArticleVO rssBlogArticleVO) {
    RssBlogArticleSP rssBlogArticleSP = new RssBlogArticleSP(dataSource);
    Map rssBlogArticleMap = rssBlogArticleSP.execute();
    List rssXMLLst = (List)rssBlogArticleMap.get("O_GET_RSS_XML");
    if(rssXMLLst!=null) {
      rssBlogArticleVO.setRssXMLList(rssXMLLst);
    }    
    return rssBlogArticleVO;
  }
  public Map getProviderScholarshipList(ScholarshipFormBean bean){
    ScholarshipSP scholarshipsp = new ScholarshipSP(dataSource);
    Map resultmap = scholarshipsp.execut(bean);
    return resultmap;
  }
  
  public Map getMegamenuValue(){
    MegaMenuSP megamenu = new MegaMenuSP(dataSource);
    Map resultmap = megamenu.execut();
    return resultmap;
  }
  
    public Map getMegaMenuDegreesList(String newstudylevelid){
      MegaMenuDegreesSP megamenu = new MegaMenuDegreesSP(dataSource);
      Map resultmap = megamenu.execut(newstudylevelid);
      return resultmap;
    }
  
  public Map getMobileCoursesDetailsInfo(Map inputMap){
    MobileCourseDetailsSP mobilecoursesp = new MobileCourseDetailsSP(dataSource);
    Map resultmap = mobilecoursesp.execute(inputMap);
    return resultmap;
  }    
    
  public Map getWhyStudyGuideData(SearchBean searchbean) { //SEO_RELEASE_29_OCT_2013_REL
    Long startDbtime = new Long(System.currentTimeMillis());
    WhyStudyGuideSP whyStudyGuideSP = new WhyStudyGuideSP(dataSource);
    Map resultmap = whyStudyGuideSP.execute(searchbean);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_BROWSE_CATEGORY_LIST_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  }
  
  public Map getSHTMLData(String shtmlName){
    Long startDbtime = new Long(System.currentTimeMillis());
    GetShtmlDataSP getShtmlDataSP = new GetShtmlDataSP(dataSource);
    Map resultmap = getShtmlDataSP.execute(shtmlName);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_BROWSE_CATEGORY_LIST_PROC", startDbtime, endDbtime);
    }
    return resultmap;
  
  }
   //To get the XML content for generate PDF, 24_NOV_2015_REL by Prabha
   public Map getPDFXMLData(SearchBean searchbean) {
     Long startDbtime = new Long(System.currentTimeMillis());
     GetPdfDataSP getPdfDataSP = new GetPdfDataSP(dataSource);
     Map resultmap = getPdfDataSP.execute(searchbean);
     Long endDbtime = new Long(System.currentTimeMillis());
     if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
       new AdminUtilities().logDbCallTimeDetails(GlobalConstants.GET_PDF_XML_CONTENT_PRC, startDbtime, endDbtime);
     }
     return resultmap;
   }
	 //    
  public Map getCategorydetails(String categorycode){ 
      GetCategoryDetailsSP getCategorydetails = new GetCategoryDetailsSP(dataSource);
      Map resultmap = getCategorydetails.execute(categorycode);  
      return resultmap;
   }  
  //
  public Map oneclickEnquirySubmit(QLFormBean qlformbean)
  {
    OneclickEnquirySubmitSP oneclickSubmit = new OneclickEnquirySubmitSP(dataSource);
    Map resultmap = oneclickSubmit.execute(qlformbean);
    return resultmap;
  }  
  //
  public Map updatePassword(String userId, String newpassword, String oldPassword){ //21_JAN_2014_REL    
     UpdatePasswordSP updatePassword = new UpdatePasswordSP(dataSource);
     Map resultmap = updatePassword.execute(userId, newpassword, oldPassword);  
     return resultmap;
  }
  
  public Map getMyWUOpendayAjax(List inputList)
  {
    MyWUOpendayAjaxSP getMyWUOpenday = new MyWUOpendayAjaxSP(dataSource);
    Map resultMap = getMyWUOpenday.execute(inputList);
    return resultMap;
  }  
  
  public Map sendAboutUsEmails(AboutUsPagesVO aboutUsPagesVO, String flag){ //26_Aug_2014
    AboutUsPagesSP aboutUsPagesSP = new AboutUsPagesSP(dataSource);
    Map resulMap = aboutUsPagesSP.execute(aboutUsPagesVO, flag);
    return resulMap;
  }
  
  public Map getProviderResultsDetails(SearchVO inputBean)
  {
    ProviderResultsSP providerSP = new ProviderResultsSP(dataSource);
    Map resultMap = providerSP.execute(inputBean);
    return resultMap;
  }  
  
  public Map getUserBasketDetails(String userId)
  {
    GetUserBasketDetailsSP getuserBasketDetails = new GetUserBasketDetailsSP(dataSource);
    Map resultMap = getuserBasketDetails.execute(userId);
    return resultMap;
  }

  public Map saveBasket(BasketBean bean)
  {
    SaveBasketSP saveBasketSP = new SaveBasketSP(dataSource);
    Map resultMap = saveBasketSP.execute(bean);
    return resultMap;
  }

  public Map getBasketDetails(BasketBean bean)
  {
    GetBasketDetailsSP basketDet = new GetBasketDetailsSP(dataSource);
    Map resultsMap  = basketDet.execute(bean);
    return resultsMap;
  }
  
  public Map getKeywordBrowseNodes(List inputList, String dbObject)
  {
    KeywordBrowseNodesSP keywordbrowsesp = new KeywordBrowseNodesSP(dataSource, dbObject);
    Map resultMap = keywordbrowsesp.execute(inputList);
    return resultMap;
  }

  public Map getKeywordBrowseNodesDesktop(List inputList)
  {
    KeywordBrowseNodesDesktopSP keywordBrowseDesktop = new KeywordBrowseNodesDesktopSP(dataSource);
    Map resultMap = keywordBrowseDesktop.execute(inputList);
    return resultMap;
  }
  
  public Map checkIfL2Node(String categroyId, String studyLevelId)//13_May_2014_REL
  {
    CheckIfL2NodeSP checkIfL2Node = new CheckIfL2NodeSP(dataSource);
    Map resultMap = checkIfL2Node.execute(categroyId, studyLevelId);
    return resultMap;
  }

  public Map saveUltimateSearchDetails(SaveULInputbean inputbean)
  {
    SaveUltimateSearchDetailsSP savesearchSP = new SaveUltimateSearchDetailsSP(dataSource);
    Map resultMap = savesearchSP.execute(inputbean);
    return resultMap;
  }
  public Map getWhatcanidoSearchResults(WhatCanIDoBean inputBean)
  {
    WhatcanidoResultsSP whatcanidoResultsSP = new WhatcanidoResultsSP(dataSource);
    Map resultMap = whatcanidoResultsSP.execute(inputBean);
    return resultMap;
  }
  //Added by Indumathi.S 08-03-16 Rel whatcanido redesign
  public Map getWhatcanidoQualificationResults(WhatCanIDoBean inputBean)
  {
    WhatcanidoQualificationSP whatcanidoQualificationSP = new WhatcanidoQualificationSP(dataSource);
    Map resultMap = whatcanidoQualificationSP.execute(inputBean);
    return resultMap;
  }
  //not used any where
  public Map getWhatucanidoEmploymentInfo(WhatCanIDoBean inputBean)
  {
    WhatcanidoEmpInfoSP employmentRateInfo = new WhatcanidoEmpInfoSP(dataSource);
    Map resultMap = employmentRateInfo.execute(inputBean);
    return resultMap;
  }
  //
  // not used any where in controller
    public Map getWhatucanidoEmploymentInfoAjax(WhatCanIDoBean inputBean)
    {
      WhatcanidoEmpInfoAjaxSP employmentRateInfoAjax = new WhatcanidoEmpInfoAjaxSP(dataSource);
      Map resultMap = employmentRateInfoAjax.execute(inputBean);
      return resultMap;
    }
  //not used any where in controller
  public Map loadUniEmploymentInfo(WhatCanIDoBean inputBean)
  {
    LoadUniEmploymentInfoSP uniEmpInfoSP = new LoadUniEmploymentInfoSP(dataSource);
    Map resultMap = uniEmpInfoSP.execute(inputBean);
    return resultMap;
  }
  
  public Map getEmpRateSortPagination(WhatCanIDoBean inputBean)
  {
    EmpRateSortPaginationSP sortPaginationSP = new EmpRateSortPaginationSP(dataSource);
    Map resultMap = sortPaginationSP.execute(inputBean);
    return resultMap;
  }

  public Map getIWanttobeSearchResults(IWanttobeBean inputBean)
  {
    IWanttobeSearchResultsSP searchResultsSP = new IWanttobeSearchResultsSP(dataSource);
    Map resultMap = searchResultsSP.execute(inputBean);
    return resultMap;
  }
  
  public Map getIWanttobeEmpInfo(IWanttobeBean inputBean)
  {
    IWanttobeEmployementInfoSP employmentInfoSP = new IWanttobeEmployementInfoSP(dataSource);
    Map resultMap = employmentInfoSP.execute(inputBean);
    return resultMap;
  }
  
    public Map getIWanttobeEmpInfoAjax(IWanttobeBean inputBean)
    {
      IWanttobeEmployementInfoAjaxSP employmentInfoSP = new IWanttobeEmployementInfoAjaxSP(dataSource);
      Map resultMap = employmentInfoSP.execute(inputBean);
      return resultMap;
    }
  
  public Map getIWanttobEmpSortPagincation(IWanttobeBean inputBean)
  {
    IWanttobeEmpSortPaginationSP iwanttobeEmpSortSP = new IWanttobeEmpSortPaginationSP(dataSource);
    Map resultMap = iwanttobeEmpSortSP.execute(inputBean);
    return resultMap;
  }
  // not used any where in controller
  public Map getIWanttobeLoadUniEmpInfo(IWanttobeBean inputBean)
  {
    IWanttobeLoadUniEmpInfoSP loadUniEmpInfoSP = new IWanttobeLoadUniEmpInfoSP(dataSource);
    Map resultMap = loadUniEmpInfoSP.execute(inputBean);
    return resultMap;
  }
  //
  public Map getUltimateSearchInputDetails(SaveULInputbean inpuBean){
    GetUltimateSearchInputdetailsSP inputDetailsSP = new GetUltimateSearchInputdetailsSP(dataSource);
    Map resultMap = inputDetailsSP.execute(inpuBean);
    return resultMap;
  }
  //
   public Map getYearOfEntry(IWanttobeBean inputBean){ //Added by Indumathi.S Feb-16-2016 IWantToBe redesign
    IWanttobeYearOfEntrySP loadYearInfoSP = new IWanttobeYearOfEntrySP(dataSource);
    Map resultMap = loadYearInfoSP.execute(inputBean);
    return resultMap;
  }

   public Map logProfileTabStats(ProfileVO profileVO){//3_JUN_2014
     LogProfileTabStatsSP logProfileTabStatsSP = new LogProfileTabStatsSP(dataSource);
     Map resultMap = logProfileTabStatsSP.execute(profileVO);
     return resultMap;
   }
   
  public Map updateUserTimeLinePod(String userId,String timeLineId){ //16_SEP_2014 created by Amir
    UpdateUserTimeLinePodSP updateUserTimeLineSP = new UpdateUserTimeLinePodSP(dataSource);
    Map updateUserTimeLineMap = updateUserTimeLineSP.execute(userId,timeLineId);
    return updateUserTimeLineMap;
  }
  
    public Map getKeyStatisticsCount(){ //08_Oct_2014 added by Thiyagu G to get statistics count.
      KeyStatisticsCountSP keyStatisticsCountSP = new KeyStatisticsCountSP(dataSource);
      Map keyStatisticsCountMap = keyStatisticsCountSP.execute();
      return keyStatisticsCountMap;
    }
  //
  public Map getProspectusesResults(ProspectusVO prospectusVO){//28-OCT-2014_REL added by Priyaa for prospectus chANGES
    ProspectusResultsSP prospectusResultsSP = new ProspectusResultsSP(dataSource);
    Map getProspectusesResultsMap = prospectusResultsSP.execute(prospectusVO);
    return getProspectusesResultsMap;
  }
  //
  public Map getAutoCompleteSubUniResults(List inputList){//28-OCT-2014_REL
     BrowseUniAutoCompleteSP browseUniAutoCompleteSP = new BrowseUniAutoCompleteSP(dataSource);
     Map getAutoCompleteSubUniResults = browseUniAutoCompleteSP.execute(inputList);
     return getAutoCompleteSubUniResults;
  }
  //
  public Map addProspectusBasketContent(BasketBean basketBean,  Object[][] attrValues){//28-OCT-2014_REL
      AddProspectusBasketSP addProspectusBasketContentSP = new AddProspectusBasketSP(dataSource);
      Map addProspectusBasketContent = addProspectusBasketContentSP.execute(basketBean, attrValues);
      return addProspectusBasketContent;
  }
  //
   public Map enqBasketSubmitPrc(QLFormBean qlFormBean){//28_OCT_2014 Added by Priyaa
    EnqBasketSubmitSP enqBasketSubmitSP = new EnqBasketSubmitSP(dataSource);
    Map enqBasketSubmitprc = enqBasketSubmitSP.execute(qlFormBean);
    return enqBasketSubmitprc;   
   }
  //
   public Map addRollMarketingPrc(ArrayList list){//13_JAN_2015 Added by Priyaa
    AdRollMarketingSP addRollMarketingPrcSP= new AdRollMarketingSP(dataSource);
    Map addRollMarketingMap = addRollMarketingPrcSP.execute(list);
    return addRollMarketingMap;   
   }
  //
   public Map prosPostEnquirySuggestion(List list){//3_FEB2015 For Dld prospectus post enquiry suggestion
    ProsPostEnquirySuggestionSP prosPostEnquirySuggestionSP= new ProsPostEnquirySuggestionSP(dataSource);
    Map prosPostEnquirySuggestionMap = prosPostEnquirySuggestionSP.execute(list);
    return prosPostEnquirySuggestionMap;   
   }

  //
  public Map resetPasswordPrc(String userId, String password, String sessionUserId, String caTrackFlag){//wu_537 24-FEB-2015 Added by Karthi
    ResetPasswordSP resetPassword = new ResetPasswordSP(dataSource);
    Map resultmap = resetPassword.execute(userId, password, sessionUserId, caTrackFlag);  
    return resultmap;
  }

   //
   public Map getOpenDaysLandingLocation(OpendaysVO opendaysVO) { //To get the information about the openday location landing, 17_Mar_2015 By Thiyagu G
     OpenDaysLandingLocationSP openDaysLandingLocationSP = new OpenDaysLandingLocationSP(dataSource);
     Map resultmap = openDaysLandingLocationSP.execute(opendaysVO);      
     return resultmap;
   }
   //
   public Map getOpenDaysLandingDate(List inputList) { //To get the information about the openday location landing, 17_Mar_2015 By Thiyagu G
     OpenDaysLandingDateSP openDaysLandingDateSP = new OpenDaysLandingDateSP(dataSource);
     Map resultmap = openDaysLandingDateSP.execute(inputList);      
     return resultmap;
   }
   //
   public Map getOpenDaysWeekDateInfo(List inputList) { //To get the information about the openday location landing, 17_Mar_2015 By Thiyagu G
     OpenDaysWeekDateInfoSP openDaysWeekDateInfoSP = new OpenDaysWeekDateInfoSP(dataSource);
     Map resultmap = openDaysWeekDateInfoSP.execute(inputList);      
     return resultmap;
   }   
   //
   public Map getOpendaysBrowseNodesDesktop(List inputList){ //To get the information about the openday ajax search, 17_Mar_2015 By Thiyagu G
     OpendayKeywordBrowseNodesDesktopSP OpendayKeywordBrowseDesktop = new OpendayKeywordBrowseNodesDesktopSP(dataSource);
     Map resultMap = OpendayKeywordBrowseDesktop.execute(inputList);
     return resultMap;
   }
   //
   public Map fetchOpenDaySearchResults(OpendaySearchVO opendaysvo){ //17_MAR_2015 Opn days search results
    OpenDaySearchResultsSP openDaySearchResultsSP = new OpenDaySearchResultsSP(dataSource);
    Map openDaySearchResultsMap = openDaySearchResultsSP.execute(opendaysvo);
    return openDaySearchResultsMap;
   }   
   public Map getWuscaRankListResult(StudentAwardsBean studAwdBean){    //Added by Amir 22_Apr_15 release for New Student Choice Awards Result
     StudentChoiceAwardsSP studentChoiceAwardsSP = new StudentChoiceAwardsSP(dataSource);
     Map studentChoiceAwardsMap = studentChoiceAwardsSP.execute(studAwdBean);
     return studentChoiceAwardsMap;
   }
  //
   public Map getAwardsYearList(StudentAwardsBean studAwdBean){  //Added by Indumathi.S Mar-08-16
     AwardsYearListSP studentChoiceAwardsSP = new AwardsYearListSP(dataSource);
     Map studentChoiceAwardsMap = studentChoiceAwardsSP.execute(studAwdBean);
     return studentChoiceAwardsMap;
   }
   //
  public Map updateUserImageDetails(String userId, String imagePath){ 
   MyWhatuniImageUpdateSP myWhatuniImageUpdateSP = new MyWhatuniImageUpdateSP(dataSource);
   Map userImgMap = myWhatuniImageUpdateSP.execute(userId, imagePath);
   return userImgMap;
}
  //
  public Map getCompareBasketDetails(MyCompareBean mycompareBean){//19_MAY_2015 OpenDays MywhatuniComparison
   MyComparisonBasketSP myComparisonBasketSP = new MyComparisonBasketSP(dataSource);
    Map compareBasketMap = myComparisonBasketSP.execute(mycompareBean);
    return compareBasketMap;
  }
  //
  public Map removeMyOpendays(OpendaysVO opendaysVO){
    RemoveMyOpenDaysSP removeMyOpenDaysSP = new RemoveMyOpenDaysSP(dataSource);
    Map removeMyOpenDaysMap = removeMyOpenDaysSP.execute(opendaysVO);
    return removeMyOpenDaysMap;
  }
  //
  public Map getRecentCourseResults(List inputList) { //To get the information about the openday location landing, 17_Mar_2015 By Thiyagu G
    CourseSearchSP courseSearchSP = new CourseSearchSP(dataSource);
    Map resultmap = courseSearchSP.execute(inputList);      
    return resultmap;
  }
  public Map sendSocialInfoEmail(UserLoginBean userLoginBean) {  //Added by Indumathi.S for social box Nov-24 Rel
    SocialMessageSP socialMessageSP = new SocialMessageSP(dataSource);
    Map resultmap = socialMessageSP.execute(userLoginBean);      
    return resultmap;
  }
  public Map getPGUniversityNewUrl(String postgraduateUrl) {  //To get the PG university new subject to form the new URL pettern for 27_Jan_2016, By Thiyagu G.
    GetPGUniversityNewUrlSP pgUniversityNewUrlSP = new GetPGUniversityNewUrlSP(dataSource);
    Map resultmap = pgUniversityNewUrlSP.execute(postgraduateUrl);      
    return resultmap;
  }
  //
  public Map getYearOfEntryDetails(List inputList) { //To get year of entry 16_Feb_2015 By Thiyagu G
    UCASCalculatorPageSP ucasCalcPage = new UCASCalculatorPageSP(dataSource);
    Map resultmap = ucasCalcPage.execute(inputList);      
    return resultmap;
  }
  //
  public Map getUserScoreDetails(List inputList) { //To get ucas points and score based on the subject, 16_Feb_2015 By Thiyagu G
    UCASCalculatorScoreAjaxSP ucasCalcAjax = new UCASCalculatorScoreAjaxSP(dataSource);
    Map resultmap = ucasCalcAjax.execute(inputList);      
    return resultmap;
  }
  //
  public Map getTariffDetails(List inputList) { //To get tariff tables, 16_Feb_2015 By Thiyagu G
    UCASCalcTariffTableAjaxSP tariffTableAjax = new UCASCalcTariffTableAjaxSP(dataSource);
    Map resultmap = tariffTableAjax.execute(inputList);      
    return resultmap;
  }
  //
  public Map submitUcasSubjectDetails(List inputList) { //Submit subject details, 16_Feb_2015 By Thiyagu G
    UcasSubjectSubmitSP tariffTableAjax = new UcasSubjectSubmitSP(dataSource);
    Map resultmap = tariffTableAjax.execute(inputList);      
    return resultmap;
  }
  
  public Map getUcasKeywordBrowseNodes(List inputList){ //To get the browse nodes 16_Feb_2015 By Thiyagu G
    UcasKeywordBrowseNodesSP ucaskeywordbrowsesp = new UcasKeywordBrowseNodesSP(dataSource);
    Map resultMap = ucaskeywordbrowsesp.execute(inputList);
    return resultMap;
  }  //
  public Map getYearOfEntryList() { //To get tariff tables, 16_Feb_2015 By Thiyagu G
    GetYearOfEntryListSP yoeListSP = new GetYearOfEntryListSP(dataSource);
    Map resultmap = yoeListSP.execute();      
    return resultmap;
  }
  //To get the what course i do widget result by Prabha on 08-03-16
  public Map getUniSortInfo(WhatCanIDoBean inputBean){
    WhatCanIDoUniInfoSP whatCanIDoUniInfoSP = new WhatCanIDoUniInfoSP(dataSource);
    Map resultMap = whatCanIDoUniInfoSP.execute(inputBean);
    return resultMap;
  }
  //To get the Insight data by Prabha on 29-03-16
  public Map getInsightData(InsightsVO insightsVO){
    GetInsightDataSP insightDataSP = new GetInsightDataSP(dataSource);
    Map resultMap = insightDataSP.execute(insightsVO);
    return resultMap;
  }
  
  //Added to get Institution id 31_May_2016, by Thiayagu G
  public Map getInstitutionId(InsightsVO insightsVO){
    GetInstitutionIdSP institutionIdSP = new GetInstitutionIdSP(dataSource);
    Map resultMap = institutionIdSP.execute(insightsVO);
    return resultMap;
  }
   //Added for login method changed from datamodal to spring for 28_June_2016, By Thiyagu G. start
  public Map checkEmailBlocked(RegistrationBean registrationBean){
    CheckEmailBlockedSP checkEmailBlockedSP = new CheckEmailBlockedSP(dataSource);
    Map deletedProviderFlag = checkEmailBlockedSP.execute(registrationBean);
    return deletedProviderFlag;
  }
  //
  public Map checkUserAuthentication(RegistrationBean registrationBean){
    CheckUserAuthenticationSP checkUserAuthenticationSP = new CheckUserAuthenticationSP(dataSource);
    Map resultMap = checkUserAuthenticationSP.execute(registrationBean);
    return resultMap;
  }
  //
  public Map checkEmailExist(RegistrationBean registrationBean){
    CheckEmailExistSP checkEmailExistSP = new CheckEmailExistSP(dataSource);
    Map deletedProviderFlag = checkEmailExistSP.execute(registrationBean);
    return deletedProviderFlag;
  }
  //
  public Map getUserYearOfEntry(RegistrationBean registrationBean){
    GetUserYearOfEntrySP getUserYearOfEntrySP = new GetUserYearOfEntrySP(dataSource);
    Map deletedProviderFlag = getUserYearOfEntrySP.execute(registrationBean);
    return deletedProviderFlag;
  }
  //Added for login method changed from datamodal to spring for 28_June_2016, By Thiyagu G. end
  //To get the user name by Prabha on 28_Jun_2016
  public Map getUserName(RegistrationBean registrationBean){
    GetUserNameSP getUserNameSP = new GetUserNameSP(dataSource);
    Map resultMap = getUserNameSP.execute(registrationBean);
    return resultMap;
  }
  //To get the User login info by Prabha on 28_Jun_2016
  public Map getUserInformationLogin(RegistrationBean registrationBean){
    GetUserInformationLoginSP getUserInformationLoginSP = new GetUserInformationLoginSP(dataSource);
    Map resultMap = getUserInformationLoginSP.execute(registrationBean);
    return resultMap;
  }
  //To check the user login status byPrabha on 28_jun_2016
  public Map preLoginUpdateStatusCode(RegistrationBean registrationBean){
    PreLoginUpdateStatusCodeSP preLoginUpdateStatusCodeSP = new PreLoginUpdateStatusCodeSP(dataSource);
    Map resultMap = preLoginUpdateStatusCodeSP.execute(registrationBean);
    return resultMap;
  }
   //To get the sys var value by Prabha on 28_jun_2016
  public Map getWUSysVarValue(RegistrationBean registrationBean){
    GetWUSysVarValueSP getWUSysVarValueSP = new GetWUSysVarValueSP(dataSource);
    Map resultMap = getWUSysVarValueSP.execute(registrationBean);
    return resultMap;
  }
  //To get the sys var value by Prabha on 28_jun_2016
  public Map getCourseSubject(RegistrationBean registrationBean){
   GetCourseSubjectSP getCourseSubjectSP = new GetCourseSubjectSP(dataSource);
   Map resultMap = getCourseSubjectSP.execute(registrationBean);
   return resultMap;
  }
  //
  public Map getBasketCount(RegistrationBean registrationBean){
    GetBasketCountSP getBasketCountSP = new GetBasketCountSP(dataSource);
    Map basketCount = getBasketCountSP.execute(registrationBean);
    return basketCount;
  }
  //
  public Map updateCaTrackFlag(MyWhatuniInputVO myWhatuniInputVO){
    UpdateCaTrackFlagSP updateCaTrackFlagSP = new UpdateCaTrackFlagSP(dataSource);
    Map caTrackFlagMap = updateCaTrackFlagSP.execute(myWhatuniInputVO);
    return caTrackFlagMap;
  }
  //To get the new college details By Thiyagu G on 24_jan_2017
  public Map getNewCollegeDetails(SearchVO inputVO)
  {
    NewCollegeDetailsSP newCollegeNameSP = new NewCollegeDetailsSP(dataSource);
    Map resultMap = newCollegeNameSP.execute(inputVO);
    return resultMap;
  }
  //
  public Map getCourseScemaInfo(CourseDetailsVO courseDetailsVO){
    GetCourseSchemaSP getCourseSchemaSP = new GetCourseSchemaSP(dataSource);
    Map corseScemaMap = getCourseSchemaSP.execute(courseDetailsVO);
    return corseScemaMap;
  }
  //
  public Map getPopularSubjects(CourseSearchVO csVO) { //To get the popular subjects list, 21_Mar_2016 By Thiyagu G
    GetPopularSubjectsSP getPopularSubjectsSP = new GetPopularSubjectsSP(dataSource);
    Map resultmap = getPopularSubjectsSP.execute(csVO);      
    return resultmap;
  }
  //
  public Map swapFinalFiveChoice(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception {
    ReorderChoiceSP reorderChoiceSP = new ReorderChoiceSP(dataSource);
    Map resultmap = reorderChoiceSP.execute(finalFiveChoiceVO);      
    return resultmap;
  }
  //
  public Map editConfirmFinalFive(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception {
    EditConfirmChoiceSP editConfirmChoiceSP = new EditConfirmChoiceSP(dataSource);
    Map resultmap = editConfirmChoiceSP.execute(finalFiveChoiceVO);      
    return resultmap;
  }
  //
  public Map getSuggestedComparisonPod(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception {
    SuggestedComparisonPodSP suggestedComparisonPodSP = new SuggestedComparisonPodSP(dataSource);
    Map resultmap = suggestedComparisonPodSP.execute(finalFiveChoiceVO);      
    return resultmap;
  }
  //
  public Map saveUpdateFinalChoice(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception {
    SaveUpdateChoiceSP saveUpdateChoiceSP = new SaveUpdateChoiceSP(dataSource);
    Map resultmap = saveUpdateChoiceSP.execute(finalFiveChoiceVO);      
    return resultmap;
  }
  //
  public Map updateBasketCourse(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception {
    UpdateBasketCourseSP updateBasketCourseSP = new UpdateBasketCourseSP(dataSource);
    Map resultmap = updateBasketCourseSP.execute(finalFiveChoiceVO);      
    return resultmap;
  }
  //To get the course name By Thiyagu G on 16_May_2017
  public Map getCourseName(CourseDetailsBean courseDetailsBean){
    GetCourseNameSP getCourseNameSP = new GetCourseNameSP(dataSource);
    Map resultMap = getCourseNameSP.execute(courseDetailsBean);
    return resultMap;
  }
  //Signup the ALP page by Prabha on 08_Aug_17
  public Map appLandingPageSignUp(RegistrationBean registrationBean){
    AppLandingPageSignupSP appLandingPageSignupSP = new AppLandingPageSignupSP(dataSource);
    Map resultMap = appLandingPageSignupSP.execute(registrationBean);
    return resultMap;
  }
  
  public Map getAdvanceProviderResultsInfo(SearchVO inputBean)
  {
    AdvanceProviderResultsSP providerSP = new AdvanceProviderResultsSP(dataSource);
    Map resultMap = providerSP.execute(inputBean);
    return resultMap;
  }
  
  public Map getOpendayPopupAjax(OpendayVO opendayVO)
  {
    LoadOpendayPopupSP providerSP = new LoadOpendayPopupSP(dataSource);
    Map resultMap = providerSP.execute(opendayVO);
    return resultMap;
  }
  
  public Map getInterstitialSubjectList(SubjectAjaxVO subjectAjaxVO) {
    SubjectListSP subjectListSP = new SubjectListSP(dataSource);
    Map resultmap = subjectListSP.execute(subjectAjaxVO);
    return resultmap;
  }
  
  public Map getRecentSearchResultList(AutoCompleteVO autoCompleteVO)
  {
    RecentSearchAjaxSP recentSearchAjaxSP = new RecentSearchAjaxSP(dataSource);
    Map resultMap = recentSearchAjaxSP.execute(autoCompleteVO);
    return resultMap;
  }
  
  //Added for User Profile pdf download by Hema.S on 25_SEP_2018_REL
  public Map getUserProfilePdf(UserDownloadPdfBean userDownloadPdfBean){
    GetUserProfilePdfSP userProfilePdfSP = new GetUserProfilePdfSP(dataSource);
    Map resultMap = userProfilePdfSP.execute(userDownloadPdfBean);
    return resultMap;
  }  
  
  // To update if user minimized the chatbot, 25_Sep_2018 By Sabapathi
  public void updateChatBotDisable(String userId, Object[][] attrValues){
    UpdateChatBotDisbaleSP updateChatBotDisableSP = new UpdateChatBotDisbaleSP(dataSource);
    updateChatBotDisableSP.execute(userId, attrValues);
  }  
  
    public Map getReviewBreakDownList(AutoCompleteVO autoCompleteVO){
      GetReviewBreakDownAjaxSP getReviewBreakDownAjaxSP = new GetReviewBreakDownAjaxSP(dataSource);
     Map resultMap =  getReviewBreakDownAjaxSP.execute(autoCompleteVO);
     return resultMap;
    
    }
    // To get the user review for displaying the light box, 18_Dec_2018 By Jeyalakshmi
      public Map getReviewLightbox(UserReviewsVO userReviewsVO) {
        GetReviewLightBoxSP reviewLightBoxSP = new GetReviewLightBoxSP(dataSource);
        Map resultMap = reviewLightBoxSP.execute(userReviewsVO);
        return resultMap;
       }
       
    public Map getSubjectReviews(AutoCompleteVO autoCompleteVO){
      GetSubjectReviewsAjaxSP getSubjectReviewsAjaxSP = new GetSubjectReviewsAjaxSP(dataSource);
     Map resultMap =  getSubjectReviewsAjaxSP.execute(autoCompleteVO);
     return resultMap;
    
    }

  public Map getReviewSubjectList(AutoCompleteVO autoCompleteVO) {
    ReviewSubjectListSP reviewSubjectListSP = new ReviewSubjectListSP(dataSource);
    Map resultMap = reviewSubjectListSP.execute(autoCompleteVO);
    return resultMap;
  }
//To get course details for apply now journey 
 public Map applyNowCourseDetails(ApplyNowVO applyNowVO){ 
   WhatuniGoCourseInfoSP applyNowCourseDtlsSP = new WhatuniGoCourseInfoSP(dataSource);
   Map resultMap = applyNowCourseDtlsSP.execute(applyNowVO);
   return resultMap;
 }
   public Map getCourseList(AutoCompleteVO autoCompleteVO){
   CourseListSP courseListSP = new CourseListSP(dataSource);
       Map resultMap =  courseListSP.execute(autoCompleteVO);
       return resultMap;
   }
 //To get the details of grade filter page in wugo journey Jeya 28-3-2019   
 public Map getGradeFilterPage(GradeFilterBean gradeFilterBean) {
  GradeFilterPageSP gradeFilterPageSP = new GradeFilterPageSP(dataSource);
  Map resultMap = gradeFilterPageSP.execute(gradeFilterBean);
  return resultMap;
 }
 
//To get Qual Subject Ajax in grade filter page in wugo journey Jeya 28-3-2019 
 public Map getSubjectlist(String freeText, GradeFilterBean gradeFilterBean) { 
   QualSubjectAjaxSP qualSubjectAjaxSP = new QualSubjectAjaxSP(dataSource);
   Map resultMap = qualSubjectAjaxSP.execute(freeText, gradeFilterBean);
   return resultMap;
 }
 //To get ucas points Ajax in grade filter page in wugo journey Jeya 28-3-2019 
  public Map getSubjectUcasPoints(GradeFilterBean gradeFilterBean) {
    UcasPointsAjaxSP ucasPointsAjaxSP = new UcasPointsAjaxSP(dataSource);
    Map reultMap = ucasPointsAjaxSP.execute(gradeFilterBean);
    return reultMap;
  }
//for grade filter page validation in wugo journey Jeya 28-3-2019 
 public Map validateQualGrade(GradeFilterBean gradeFilterBean) {
   GradeFilterValidationSP gradeFilterValidationSP = new GradeFilterValidationSP(dataSource);
   Map resultMap = gradeFilterValidationSP.execute(gradeFilterBean);
   return resultMap;
 }
 // for saving the qualification in grade filter page 
 public Map saveQualGradeFilter(GradeFilterBean gradeFilterBean) {
   SaveGradeFilterPageSP saveGradeFilterPageSP = new SaveGradeFilterPageSP(dataSource);
   Map resultMap = saveGradeFilterPageSP.execute(gradeFilterBean);
   return resultMap;
 }
 
//for getting the matching course count in grade filter page in wugo journey Jeya 28-3-2019 
 public Map getMatchingCourse(GradeFilterBean gradeFilterBean) {
   GetMatchingCourseSP getMatchingCourseSP = new GetMatchingCourseSP(dataSource);
   Map resultMap = getMatchingCourseSP.execute(gradeFilterBean);
   return resultMap;
 }
 //
 public Map getDBName(){
   GetDBNameSP getDBNameSP = new GetDBNameSP(dataSource);
   Map resultMap = getDBNameSP.execute();
   return resultMap;
 }
 //
  public Map getUserNameDetail(RegistrationBean registrationBean){
    GetUserNamesSP getUserNamesSP = new GetUserNamesSP(dataSource);
    Map resultMap = getUserNamesSP.execute(registrationBean);
    return resultMap;
  }
  //
  
  public Map getSubjectLandingRegionList() throws Exception{
   GetSubjectLandingRegionListSP getSubjectLandingRegionListSP = new GetSubjectLandingRegionListSP(dataSource);
   Map resultMap = getSubjectLandingRegionListSP.execute();
   return resultMap;
 }
  
  //
  public Map getYearOfEntryDetails() throws Exception{
    GetYearOfEntrySP getYearOfEntrySP = new GetYearOfEntrySP(dataSource);
    Map resultMap = getYearOfEntrySP.execute();
    return resultMap;
  }
  //
  
//SR new grade filter page Jeya 22-Oct-2019  
  public Map getSRGradeFilterPage(GradeFilterBean gradeFilterBean) {
   SRGradeFilterPageSP srgradeFilterPageSP = new SRGradeFilterPageSP(dataSource);
   Map resultMap = srgradeFilterPageSP.execute(gradeFilterBean);
   return resultMap;
  }
  
  public Map getSRSubjectlist(String freeText, GradeFilterBean gradeFilterBean) { 
    SRQualSubjectAjaxSP srqualSubjectAjaxSP = new SRQualSubjectAjaxSP(dataSource);
    Map resultMap = srqualSubjectAjaxSP.execute(freeText, gradeFilterBean);
    return resultMap;
  }
  public Map getSRSubjectUcasPoints(GradeFilterBean gradeFilterBean) {
    SRUcasPointsAjaxSP srucasPointsAjaxSP = new SRUcasPointsAjaxSP(dataSource);
    Map reultMap = srucasPointsAjaxSP.execute(gradeFilterBean);
    return reultMap;
  }
  //  public Map validateQualGrade(GradeFilterBean gradeFilterBean) {
  //    GradeFilterValidationSP gradeFilterValidationSP = new GradeFilterValidationSP(dataSource);
  //    Map resultMap = gradeFilterValidationSP.execute(gradeFilterBean);
  //    return resultMap;
  //  }
  public Map saveSRQualGradeFilter(GradeFilterBean gradeFilterBean) {
    SRSaveGradeFilterPageSP savesrGradeFilterPageSP = new SRSaveGradeFilterPageSP(dataSource);
    Map resultMap = savesrGradeFilterPageSP.execute(gradeFilterBean);
    return resultMap;
  }
  
  public Map getSRMatchingCourse(GradeFilterBean gradeFilterBean) {
    SRGetMatchingCourseSP getsrMatchingCourseSP = new SRGetMatchingCourseSP(dataSource);
    Map resultMap = getsrMatchingCourseSP.execute(gradeFilterBean);
    return resultMap;
  }  
 // for deleting the user record if already saved in db
  public Map deleteUserRecord(GradeFilterBean gradeFilterBean) {
    DeleteUserRecordSP deleteUserRecordSP = new DeleteUserRecordSP(dataSource);
    Map resultMap = deleteUserRecordSP.execute(gradeFilterBean);
    return resultMap;
  }  
  
  public Map getCookieLatestUpdatedDate() throws Exception{
    GetCookieLatestUpdatedDateSP getCookieLatestUpdatedDateSP = new GetCookieLatestUpdatedDateSP(dataSource);
    Map resultMap = getCookieLatestUpdatedDateSP.execute();
    return resultMap;
  }  
  //
  public Map getTopNavSubjectList(AutoCompleteVO autoCompleteVO) {
    TopNavCourseSearchSP topNavCourseSearchSP = new TopNavCourseSearchSP(dataSource);
    Map resultMap = topNavCourseSearchSP.execute(autoCompleteVO);
    return resultMap;
  }
  
  public Map<String, Object> getCovid19Data() {
    GetCovid19DataSP sp = new GetCovid19DataSP(dataSource);
	return sp.executeProcedure();
  }
//Added by Sangeeth for Clearing profile 12_MAY_20
 public Map getClearingProfileDetails(ClearingProfileVO clearingProfileVO) {
   ClearingProfileSP clearingProfileSP = new ClearingProfileSP(dataSource);    
   Map clearingProfileMap = clearingProfileSP.execute(clearingProfileVO);
   return clearingProfileMap;
 } 
 
  //For getting the navigation url from DB for CTA buttons by Sujitha V on 2020_DEC_08
  public Map getNavigationUrl(String subOrderItemId, String CourseId, String ctaButtonName) throws Exception{
	NavigationUrlSP navigationUrlSP = new NavigationUrlSP(dataSource);    
    Map navigationUrlMap = navigationUrlSP.execute(subOrderItemId, CourseId, ctaButtonName);
    return navigationUrlMap;
  }
 //Added by Sri Sankari for Sysvar calls on 19_Jan_2021 rel
 public Map<String, Object> getSysvarValues() throws Exception {
	GetSysvarsCallSP getSysvarsCallSP = new GetSysvarsCallSP(dataSource);
	return getSysvarsCallSP.execute();
 } 
  public Map getQualificationList(SRGradeFilterBean srgradeFilterBean) {
	SRGradeFilterSP srGradeFilterSP = new SRGradeFilterSP(dataSource);
	Map resultMap = srGradeFilterSP.execute(srgradeFilterBean);
	return resultMap;
  }

  public Map saveSRGradeFilterInfo(SRGradeFilterBean srgradeFilterBean) {
	SaveSRGradeFilterSP saveSRGradeFilterSP = new SaveSRGradeFilterSP(dataSource);
	Map resultMap = saveSRGradeFilterSP.execute(srgradeFilterBean);
	return resultMap;
  }

  public Map deleteSRGradeFilterInfo(SRGradeFilterBean srgradeFilterBean) {
	DeleteSRGradeFilterSP deleteSRGradeFilterSP = new DeleteSRGradeFilterSP(dataSource);
	Map resultMap = deleteSRGradeFilterSP.execute(srgradeFilterBean);
	return resultMap;
  }
}
