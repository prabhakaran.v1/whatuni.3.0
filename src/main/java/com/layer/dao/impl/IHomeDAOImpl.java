package com.layer.dao.impl;

import java.util.Map;

import javax.sql.DataSource;

import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.homepage.form.HomePageBean;

import com.layer.dao.IHomeDAO;
import com.layer.dao.sp.home.UserTimeLinePodSP;
import com.layer.dao.sp.home.WhatuiHomeSP;
import com.layer.dao.sp.home.WhatuniNewHomeSP;

/**
 * IHomeDAOImpl.
 *
 * @author:     Mohamed Syed
 * @version:    1.0
 * @since:      wu314_20110712
 *
 **/
public class IHomeDAOImpl implements IHomeDAO {

  private DataSource dataSource;

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  public DataSource getDataSource() {
    return dataSource;
  }

  public Map getHomePageData() {
    Long startDbtime = new Long(System.currentTimeMillis());
    WhatuiHomeSP homesp = new WhatuiHomeSP(dataSource);
    Map resultmap = homesp.execute();
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_HOMEPAGE_DATA_PROC(0)", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //
  public Map getNewHomePageData(HomePageBean homepagebean) {//16_SEP_2014
    Long startDbtime = new Long(System.currentTimeMillis());
    WhatuniNewHomeSP homesp = new WhatuniNewHomeSP(dataSource);
    Map resultmap = homesp.execute(homepagebean);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
     new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.GET_HOMEPAGE_DATA_PROC(0)", startDbtime, endDbtime);
    }
    return resultmap;
  }
  
  public Map getUserTimeLinePod(String userId) { //16_SEP_2014
    UserTimeLinePodSP userTimeLineSP = new UserTimeLinePodSP(dataSource);
    Map userTimeLineMap = userTimeLineSP.execute(userId);
    return userTimeLineMap;
  }
}
