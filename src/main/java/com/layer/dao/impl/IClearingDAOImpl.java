package com.layer.dao.impl;

import com.layer.dao.IClearingDAO;
import javax.sql.DataSource;

public class IClearingDAOImpl implements IClearingDAO {

  private DataSource dataSource;

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  public DataSource getDataSource() {
    return dataSource;
  }

}
