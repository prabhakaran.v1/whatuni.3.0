package com.layer.dao.impl;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import mobile.dao.sp.GetMobileInstitutionsListSP;
import mobile.dao.sp.search.MobileCourseSpecificMoneyPageSP;
import mobile.dao.sp.search.MobileProviderResultMainContentSP;
import mobile.dao.sp.search.MobileRefineResultsSP;
import mobile.form.MobileUniBrowseBean;
import mobile.valueobject.SearchVO;
import spring.form.OpenDaysBean;
import spring.sp.BreadcrumbSP;
import spring.valueobject.seo.BreadcrumbVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.registration.bean.RegistrationBean;
import WUI.review.bean.UniBrowseBean;
import WUI.search.form.SearchBean;

import com.layer.dao.ISearchDAO;
import com.layer.dao.sp.interstitialsearch.InterstitialCourseCountSP;
import com.layer.dao.sp.interstitialsearch.InterstitialSearchSP;
import com.layer.dao.sp.opendays.AddMyOpendaysSP;
import com.layer.dao.sp.opendays.AddOpenDayForReservePlacesSP;
import com.layer.dao.sp.registration.SurveyAutoLoginSP;
import com.layer.dao.sp.registration.UserRegistrationLighboxSP;
import com.layer.dao.sp.registration.UserRegistrationSP;
import com.layer.dao.sp.search.ArticleDetailSP;
import com.layer.dao.sp.search.CourseDetailsInfoSP;
import com.layer.dao.sp.search.CourseDetailsKISAjaxSP;
import com.layer.dao.sp.search.CourseSpecificMoneyPageSP;
import com.layer.dao.sp.search.GetInstitutionsListSP;
import com.layer.dao.sp.search.GetModuleStageDetailDescSP;
import com.layer.dao.sp.search.GetModuleStageDetailsSP;
import com.layer.dao.sp.search.GetPredictedGradesSP;
import com.layer.dao.sp.search.ModuleDetailsSearchSP;
import com.layer.dao.sp.search.SearchResultsSP;
import com.layer.dao.sp.search.UniSpecificMoneyPageSP;
import com.layer.dao.sp.search.advancesearch.AdvanceSearchResultsSP;
import com.layer.dao.sp.topnavsearch.TopNavSearchSP;
import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.interstitialsearch.InterstitialCourseCountVO;
import com.wuni.util.valueobject.interstitialsearch.InterstitialSearchVO;
import com.wuni.valueobjects.ModuleVO;
import com.wuni.valueobjects.topnavsearch.TopNavSearchVO;

public class ISearchDAOImpl implements ISearchDAO {

  private DataSource dataSource = null;

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  public DataSource getDataSource() {
    return this.dataSource;
  }

  /**
   * will return course-details page info
   * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-details/[COURSE_TITLE]-courses-details/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
   *
   *
   * @since        wu318_20111020 - redesign
   * @author       Mohamed Syed
   *
   * @param inputList
   * @return
   *    
   */
  public Map getCourseDetailsInfo(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    CourseDetailsInfoSP courseDetailsInfoSP = new CourseDetailsInfoSP(dataSource);
    Map resultmap = courseDetailsInfoSP.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WUNI_SPRING_PKG.COURSE_DETAILS_INFO_PRC", startDbtime, endDbtime);
    }
    return resultmap;
  }

  /**
   * will return course-specific-money-page details
   * URL_PATTERN: /courses/[STUDY_LEVEL_DESC]-courses/[KEYWORD]-[STUDY_LEVEL_DESC]-courses-[LOCATION1]/[KEYWORD]/[STUDY_LEVEL_ID]/[LOCATION2]/[LOCATION2]/[SEARCH_RANGE]/[COUNTY_ID]/[STUDY_MODE_ID]/[CATEGORY_CODE]/[ORDER_BY]/[COLLEGE_ID]/[PAGE_NO]/[EXTRA_TEXT]/[UC/U/C]/page.html
   *
   * @since        wu318_20111020 - redesign
   * @author       Mohamed Syed
   *
   * @param inputList
   * @return
   *
   */
  public Map getCourseSpecificMoneyPage(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    CourseSpecificMoneyPageSP courseSpecificMoneyPage = new CourseSpecificMoneyPageSP(dataSource);
    Map resultmap = courseSpecificMoneyPage.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WHATUNI_SEARCH3.GET_MONEY_PAGE_MAIN_CONTENT --> COURSE SPECIFIC CALL", startDbtime, endDbtime);
    }
    //TODO comment below two line while deploying in live server.
      Long dbTime = new Long(endDbtime.longValue() - startDbtime.longValue());
      resultmap.put("dbExecutionTime",dbTime);
    return resultmap;
  }

  /**
   * will return uni-specific-money-page details
   * URL_PATTERN: /courses/[STUDY_LEVEL_DESC]-courses/[KEYWORD]-[STUDY_LEVEL_DESC]-courses-[LOCATION1]/[KEYWORD]/[STUDY_LEVEL_ID]/[LOCATION2]/[LOCATION2]/[SEARCH_RANGE]/[COUNTY_ID]/[STUDY_MODE_ID]/[CATEGORY_CODE]/[ORDER_BY]/[COLLEGE_ID]/[PAGE_NO]/[EXTRA_TEXT]/[UC/U/C]/uniview.html
   *
   * @since        wu318_20111020 - redesign
   * @author       Mohamed Syed
   *
   * @param inputList
   * @return
   *
   */
  public Map getUniSpecificMoneyPage(List inputList) {
    Long startDbtime = new Long(System.currentTimeMillis());
    UniSpecificMoneyPageSP uniSpecificMoneyPage = new UniSpecificMoneyPageSP(dataSource);
    Map resultmap = uniSpecificMoneyPage.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WHATUNI_SEARCH3.GET_MONEY_PAGE_MAIN_CONTENT --> UNI SPECIFIC CALL", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //
  public Map getUniversityBrowseList(UniBrowseBean uniBrowseBean) {
    Long startDbtime = new Long(System.currentTimeMillis());
    GetInstitutionsListSP institutionsListSP = new GetInstitutionsListSP(dataSource);
    Map resultmap = institutionsListSP.execute(uniBrowseBean);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails(SpringConstants.GET_ALPHABET_COLLEGE_NAME_FN +" --> UNI SPECIFIC CALL", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //
   public Map getMobileUniversityBrowseList(MobileUniBrowseBean uniBrowseBean) {     
     GetMobileInstitutionsListSP institutionsListSP = new GetMobileInstitutionsListSP(dataSource);
     Map resultmap = institutionsListSP.execute(uniBrowseBean);          
     return resultmap;
   }  
  //
  public Map newUserRegistration(RegistrationBean registrationBean, Object[][] attrValues) {
    Long startDbtime = new Long(System.currentTimeMillis());
    UserRegistrationSP userRegistrationSP = new UserRegistrationSP(dataSource);
    Map resultmap = userRegistrationSP.execute(registrationBean, attrValues);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails(SpringConstants.USER_REGISTERATION_LBX_PRC +" --> UNI SPECIFIC CALL", startDbtime, endDbtime);
    }
    return resultmap;
  } 
  
  public Map getPredictedGrades(SearchBean searchBean) {
    Long startDbtime = new Long(System.currentTimeMillis());
    GetPredictedGradesSP predictedGradesSP = new GetPredictedGradesSP(dataSource);
    Map resultmap = predictedGradesSP.execute(searchBean);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails(SpringConstants.GET_USER_PREDICTED_GRADES_VAL_FN +" --> UNI SPECIFIC CALL", startDbtime, endDbtime);
    }
    return resultmap;
  }
  
  public Map getCourseDetailsKISAjax(List inputList){
    Long startDbtime = new Long(System.currentTimeMillis());
    CourseDetailsKISAjaxSP courseKISAjax = new CourseDetailsKISAjaxSP(dataSource);
    Map resultmap = courseKISAjax.execute(inputList);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("wuni_spring_pkg.course_page_ajax_prc", startDbtime, endDbtime);
    }
    return resultmap;
  }
  
  public Map addMyOpendays(OpenDaysBean openDaysBean) {
    Long startDbtime = new Long(System.currentTimeMillis());
    AddMyOpendaysSP addMyOpendaysSP = new AddMyOpendaysSP(dataSource);
    Map resultmap = addMyOpendaysSP.execute(openDaysBean);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails(SpringConstants.ADD_MY_OPENDAYS_FN, startDbtime, endDbtime);
    }
    return resultmap;
  }
  
  public Map addOpenDayForReservePlacesAction(OpenDaysBean openDaysBean) {
    Long startDbtime = new Long(System.currentTimeMillis());
    AddOpenDayForReservePlacesSP addOpenDayForReservePlacesSP = new AddOpenDayForReservePlacesSP(dataSource);
    Map resultmap = addOpenDayForReservePlacesSP.execute(openDaysBean);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails(SpringConstants.SAVE_USER_ACTION_VAL_PRC, startDbtime, endDbtime);
    }
    return resultmap;
  }
  
  /**
   * will return mobile-course-specific-money-page details
   * @param inputList
   * @return
   */
  public Map getMobileCourseSpecificMoneyPage(SearchVO searchVO) {
    Long startDbtime = new Long(System.currentTimeMillis());
    MobileCourseSpecificMoneyPageSP courseSpecificMoneyPage = new MobileCourseSpecificMoneyPageSP(dataSource);
    Map resultmap = courseSpecificMoneyPage.execute(searchVO);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WHATUNI_SEARCH3.GET_MONEY_PAGE_MAIN_CONTENT --> COURSE SPECIFIC CALL", startDbtime, endDbtime);
    }
    //TODO comment below two line while deploying in live server.
      Long dbTime = new Long(endDbtime.longValue() - startDbtime.longValue());
      resultmap.put("dbExecutionTime",dbTime);
    return resultmap;
  }
  
  /**
   * will return load refine page details
   * @param inputList
   * @return
   */
  public Map getMobileRefineResults(SearchVO searchVO) {
    Long startDbtime = new Long(System.currentTimeMillis());
    MobileRefineResultsSP getMobileRefineResults = new MobileRefineResultsSP(dataSource);
    Map resultmap = getMobileRefineResults.execute(searchVO);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WHATUNI_SEARCH3.GET_MONEY_PAGE_MAIN_CONTENT --> COURSE SPECIFIC CALL", startDbtime, endDbtime);
    }
    //TODO comment below two line while deploying in live server.
      Long dbTime = new Long(endDbtime.longValue() - startDbtime.longValue());
      resultmap.put("dbExecutionTime",dbTime);
    return resultmap;
  }


  public Map getMobileProviderResultPageContent(SearchVO searchVO) {
    Long startDbtime = new Long(System.currentTimeMillis());
    MobileProviderResultMainContentSP providerResultPageMainContentSP = new MobileProviderResultMainContentSP(dataSource);
    Map resultmap = providerResultPageMainContentSP.execute(searchVO);
    Long endDbtime = new Long(System.currentTimeMillis());
    if (TimeTrackingConstants.DB_CALLS_TIME_TRACKING_FLAG) {
      new AdminUtilities().logDbCallTimeDetails("WHATUNI_SEARCH3.GET_PROVIDER_RESULT_PAGE", startDbtime, endDbtime);
    }
    return resultmap;
  }
  //
  public Map getModuleStageDetails(ModuleVO moduleVO) {//11_Feb_2014_REL
    GetModuleStageDetailsSP getModuleStageDetails = new GetModuleStageDetailsSP(dataSource);
    Map resultmap = getModuleStageDetails.execute(moduleVO);
    return resultmap;
  }
  //
   public Map getModuleStageDetailDesc(ModuleVO moduleVO) {//11_Feb_2014_REL
     GetModuleStageDetailDescSP getModuleStageDetailDesc = new GetModuleStageDetailDescSP(dataSource);
     Map resultmap = getModuleStageDetailDesc.execute(moduleVO);
     return resultmap;
   }
  //
   //Added for search redesign
   public Map getSearchResults(SearchVO searchVO){//Search_Redesign
     SearchResultsSP searchResultsSP = new SearchResultsSP(dataSource);
     Map resultmap = searchResultsSP.execute(searchVO);
     return resultmap;
   }
   //
   public Map getModuleDetailsSearch(String moduleGroupId){//Search_Redesign
    ModuleDetailsSearchSP searchResultsSP = new ModuleDetailsSearchSP(dataSource);
    Map resultmap = searchResultsSP.execute(moduleGroupId);
    return resultmap;
   }
   //
   public Map getArticleData(String postId){//Search_Redesign
     ArticleDetailSP getArticleDataSP = new ArticleDetailSP(dataSource);
     Map resultmap = getArticleDataSP.execute(postId);
     return resultmap;
   }
  /**
   * will return user details for auto login
   * @param registrationBean
   * @return map
   * added by Priyaa For 21_JUL_2015_REL  
   */
   public Map surveyUserAutoLogin(RegistrationBean registrationBean) {
    SurveyAutoLoginSP surveyAutoLoginSP = new SurveyAutoLoginSP(dataSource);
    Map resultmap = surveyAutoLoginSP.execute(registrationBean);
    return resultmap;
  } 
  
  public Map userRegistrationLightbox() {
    UserRegistrationLighboxSP userRegistrationLighboxSP = new UserRegistrationLighboxSP(dataSource);
    Map resultmap = userRegistrationLighboxSP.execute();    
    return resultmap;
  } 
  public Map getBreadcrumbData(BreadcrumbVO breadcrumbVO){//Get breadcrumb details by Prabha For 21_MAR_2017_REL 
    BreadcrumbSP breadcrumbSP = new BreadcrumbSP(dataSource);
    Map resultmap = breadcrumbSP.execute(breadcrumbVO);
    return resultmap;
  }
  //
  public Map getAdvanceSearchResults(SearchVO searchVO){
    AdvanceSearchResultsSP advanceSearchResultsSP = new AdvanceSearchResultsSP(dataSource);
    Map resultmap = advanceSearchResultsSP.execute(searchVO);
    return resultmap;
  }
  //

  public Map getInterstitialSearchFilters(InterstitialSearchVO interstitialSearchVO) {
    InterstitialSearchSP interstitialSearchSP = new InterstitialSearchSP(dataSource);
    Map resultmap = interstitialSearchSP.execute(interstitialSearchVO);
    return resultmap;
  }
  //
  
   public Map getInterstitialCourseCount(InterstitialCourseCountVO interstitialCourseCountVO) {
     InterstitialCourseCountSP interstitialCourseCountSP = new InterstitialCourseCountSP(dataSource);
     Map resultmap = interstitialCourseCountSP.execute(interstitialCourseCountVO);
     return resultmap;
   }
   //
   public Map getTopNavSearchFilters(TopNavSearchVO topNavSearchVO) {
     TopNavSearchSP topNavSearchSP = new TopNavSearchSP(dataSource);
     Map resultmap = topNavSearchSP.execute(topNavSearchVO);
     return resultmap;
   }  
}