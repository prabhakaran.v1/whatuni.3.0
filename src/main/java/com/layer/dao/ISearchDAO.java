package com.layer.dao;

import java.util.List;
import java.util.Map;

import mobile.form.MobileUniBrowseBean;
import mobile.valueobject.SearchVO;
import spring.form.OpenDaysBean;
import spring.valueobject.seo.BreadcrumbVO;
import WUI.registration.bean.RegistrationBean;
import WUI.review.bean.UniBrowseBean;
import WUI.search.form.SearchBean;

import com.wuni.util.valueobject.interstitialsearch.InterstitialCourseCountVO;
import com.wuni.util.valueobject.interstitialsearch.InterstitialSearchVO;
import com.wuni.valueobjects.ModuleVO;
import com.wuni.valueobjects.topnavsearch.TopNavSearchVO;

/**
 * contains business logic for SR, PR, CD
 *
 * @since        wu318_20111020 - redesign
 * @author       Mohamed Syed
 *
 */
public interface ISearchDAO {

  /**
   * will return course-details page info
   * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-details/[COURSE_TITLE]-courses-details/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
   *
   *
   * @since        wu318_20111020 - redesign
   * @author       Mohamed Syed
   *
   * @param inputList
   * @return
   *
   */
  public Map getCourseDetailsInfo(List inputList);

  /**
   * will return course-specific-money-page details
   * URL_PATTERN: /courses/[STUDY_LEVEL_DESC]-courses/[KEYWORD]-[STUDY_LEVEL_DESC]-courses-[LOCATION1]/[KEYWORD]/[STUDY_LEVEL_ID]/[LOCATION2]/[LOCATION2]/[SEARCH_RANGE]/[COUNTY_ID]/[STUDY_MODE_ID]/[CATEGORY_CODE]/[ORDER_BY]/[COLLEGE_ID]/[PAGE_NO]/[EXTRA_TEXT]/[UC/U/C]/page.html
   *
   * @since        wu318_20111020 - redesign
   * @author       Mohamed Syed
   *
   * @param inputList
   * @return
   *
   */
  public Map getCourseSpecificMoneyPage(List inputList);

  /**
   * will return uni-specific-money-page details
   * URL_PATTERN: /courses/[STUDY_LEVEL_DESC]-courses/[KEYWORD]-[STUDY_LEVEL_DESC]-courses-[LOCATION1]/[KEYWORD]/[STUDY_LEVEL_ID]/[LOCATION2]/[LOCATION2]/[SEARCH_RANGE]/[COUNTY_ID]/[STUDY_MODE_ID]/[CATEGORY_CODE]/[ORDER_BY]/[COLLEGE_ID]/[PAGE_NO]/[EXTRA_TEXT]/[UC/U/C]/uniview.html
   *
   * @since        wu318_20111020 - redesign
   * @author       Mohamed Syed
   *
   * @param inputList
   * @return
   *
   */
  public Map getUniSpecificMoneyPage(List inputList);
  //
  public Map getUniversityBrowseList(UniBrowseBean uniBrowseBean);  
  public Map getMobileUniversityBrowseList(MobileUniBrowseBean uniBrowseBean);
  public Map newUserRegistration(RegistrationBean registrationBean, Object[][] attrValues);
  public Map getPredictedGrades(SearchBean searchBean);
  
  public Map getCourseDetailsKISAjax(List inputList);
  public Map addMyOpendays(OpenDaysBean openDaysBean);
  public Map addOpenDayForReservePlacesAction(OpenDaysBean openDaysBean);
  public Map getMobileCourseSpecificMoneyPage(SearchVO searchVO);//Mobile_Release
  public Map getMobileRefineResults(SearchVO searchVO);//Mobile_Release   
  public Map getMobileProviderResultPageContent(SearchVO searchVO); 
  public Map getModuleStageDetails(ModuleVO moduleVO);//11_Feb_2014_REL
  public Map getModuleStageDetailDesc(ModuleVO moduleVO);//11_Feb_2014_REL
  public Map getSearchResults(SearchVO searchVO);//Search_Redesign
  public Map getModuleDetailsSearch(String moduleGroupId);//Search_Redesign
  public Map getArticleData(String moduleGroupId);//Search_Redesign
  public Map surveyUserAutoLogin(RegistrationBean registrationBean);//Added by Priyaa For 21_JUL_2015_REL   
  
  public Map userRegistrationLightbox();

  public Map getBreadcrumbData(BreadcrumbVO breadcrumbVO); //Get breadcrumb details by Prabha For 21_MAR_2017_REL

  public Map getAdvanceSearchResults(SearchVO searchVO); //Added for Advance search page by Prabha on 29_Dec_2017_REL

  public Map getInterstitialSearchFilters(InterstitialSearchVO interstitialSearchVO); //Added for Interstitial Search page by Saba for wu580_28082018
  
  public Map getInterstitialCourseCount(InterstitialCourseCountVO interstitialCourseCountVO); //Added for Interstitial Search page by Saba for wu580_28082018

  public Map getTopNavSearchFilters(TopNavSearchVO topNavSearchVO);
}

