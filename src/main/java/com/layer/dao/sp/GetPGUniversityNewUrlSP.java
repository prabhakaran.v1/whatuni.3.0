package com.layer.dao.sp;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

/**
   * @since        27_Jan_2016 - PR page OLD URL redirection
   * @author       Thiyagu G.
   *
   * URL_PATTERN: www.whatuni.com/[STUDY_LEVEL_DESC]-courses/csearch
   * URL_EXAMPLE: /degrees/courses/csearch?      
   *
   * Modification history:
   * **************************************************************************************************************
   * Author           Relase Date              Modification Details
   * **************************************************************************************************************   
   *
   */ 
public class GetPGUniversityNewUrlSP extends StoredProcedure {
  public GetPGUniversityNewUrlSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.PG_UNIVERSITY_NEW_URL_PRC);
    declareParameter(new SqlOutParameter("pg_new_url_details", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_url", OracleTypes.VARCHAR));
    compile();
  }
  //  
  public Map execute(String postgraduateUrl) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_url", postgraduateUrl);
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
  
}
