package com.layer.dao.sp.whatunigo;

import WUI.whatunigo.bean.GradeFilterBean;
import com.layer.util.SpringConstants;
import com.wuni.valueobjects.whatunigo.GradeFilterVO;
import com.wuni.valueobjects.whatunigo.GradeSubjectVO;
import com.wuni.valueobjects.whatunigo.UserQualDetailsVO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @see This SP class is used to call the database procedure to get the user grades and qualifications before search result page.
  * @since  28.03.2019- intital draft
  * @version 1.0
  *
  * Modification history:
  * *****************************************************************************************************************
  * Author          Relase tag       Modification Details                                                  Rel Ver.
  * *****************************************************************************************************************
  * Jeyalakshmi.D       1.0             initial draft                                                           wu_586
  *
  */

public class GradeFilterPageSP extends StoredProcedure {
  public GradeFilterPageSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql(SpringConstants.WUGO_GET_QUALIFICATION_LIST_PRC);
    declareParameter(new SqlInOutParameter("P_SESSION_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_QUALIFICATION_LIST", OracleTypes.CURSOR, new QualInfoRowmapperImpl()));
    declareParameter(new SqlOutParameter("PC_USER_QUALIFICATIONS", OracleTypes.CURSOR, new UserQualInfoRowmapperImpl()));
    declareParameter(new SqlOutParameter("P_UCAS_POINTS", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_SUBJECT_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_KEYWORD", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_QUALIFICATION_CODE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_MAX_UCAS_POINTS", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_LOCATION", OracleTypes.VARCHAR));
    compile();
  }
  
  public Map execute(GradeFilterBean gradeFilterBean){
    Map outMap = null;
    try{
      Map inputMap = new HashMap();
      inputMap.put("P_SESSION_ID", gradeFilterBean.getSessionId());
      inputMap.put("P_USER_ID", gradeFilterBean.getUserId());
      inputMap.put("P_SUBJECT_NAME", gradeFilterBean.getSubjectName());
      inputMap.put("P_KEYWORD", gradeFilterBean.getKeyword());
      inputMap.put("P_COLLEGE_NAME", gradeFilterBean.getCollegeName());
      inputMap.put("P_QUALIFICATION_CODE", gradeFilterBean.getQualificationCode());
      inputMap.put("P_LOCATION", gradeFilterBean.getLocation());
      outMap = execute(inputMap);
      
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  
  private class QualInfoRowmapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int num) throws SQLException {
      GradeFilterVO gradeFilterVO = new GradeFilterVO();
      gradeFilterVO.setEntry_qual_id(rs.getString("qual_id"));
      gradeFilterVO.setEntry_qualification(rs.getString("qualification"));
      gradeFilterVO.setParent_qualification(rs.getString("parent_qualification")); 
      gradeFilterVO.setEntry_grade(rs.getString("grade_str"));
      gradeFilterVO.setEntry_old_grade(rs.getString("old_grade_str"));
      gradeFilterVO.setEntry_subject(rs.getString("no_of_subjects"));
      gradeFilterVO.setQual_level(rs.getString("qual_level"));
      return gradeFilterVO;
   }
 }   
  
  private class UserQualInfoRowmapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int num) {
      UserQualDetailsVO userQualDetailsVO = new UserQualDetailsVO();
      try {
        userQualDetailsVO.setEntry_qual_level(rs.getString("qual_level"));
        userQualDetailsVO.setEntry_qual_id(rs.getString("qual_id"));
        userQualDetailsVO.setEntry_qual_desc(rs.getString("qualification"));
        userQualDetailsVO.setGcse_grade_flag(rs.getString("grade_type"));
        ResultSet userqualRS = (ResultSet)rs.getObject("subject_list");
        try {
          if(userqualRS != null){
            GradeSubjectVO gradeSubjectVO = new GradeSubjectVO();
            ArrayList qualOptionsList = new ArrayList();
            gradeSubjectVO = null;
            while (userqualRS.next()) {
              gradeSubjectVO = new GradeSubjectVO();
              gradeSubjectVO.setEntry_subject_id(userqualRS.getString("SUBJECT_ID"));
              gradeSubjectVO.setEntry_subject(userqualRS.getString("SUBJECT_DESC"));
              gradeSubjectVO.setEntry_grade(userqualRS.getString("GRADE"));
              qualOptionsList.add(gradeSubjectVO);
            }
            userQualDetailsVO.setQual_subject_list(qualOptionsList);
          }
        }catch (Exception e) {
          e.printStackTrace();
        }finally {
          try {
            if (userqualRS != null) {
              userqualRS.close();
              userqualRS = null;
            }
          } catch (Exception e) {
            throw new SQLException(e.getMessage());
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
      return userQualDetailsVO;
    }
  }
}
