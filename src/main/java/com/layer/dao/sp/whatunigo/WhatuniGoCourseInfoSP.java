package com.layer.dao.sp.whatunigo;

import com.layer.util.SpringConstants;
import com.wuni.valueobjects.whatunigo.ApplyNowVO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @see This SP class is used to call the database procedure to get the course details of the apply now journey page
  * @author Sangeeth.S
  * @since  19.02.2019- intital draft
  * @version 1.0
  *
  * Modification history:
  * *****************************************************************************************************************
  * Author          Relase tag       Modification Details                                                  Rel Ver.
  * *****************************************************************************************************************  
  * Sangeeth.S       1.0             initial draft                                                           wu_586
  *
*/
public class WhatuniGoCourseInfoSP extends StoredProcedure{

  public WhatuniGoCourseInfoSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql(SpringConstants.GET_APPLY_NOW_COURSE_DETAIL_PRC);    
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_COURSE_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_OPPORTUNITY_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_APPLY_COURSE_DET", OracleTypes.CURSOR, new CourseDetailRowMapperImpl()));
    compile();
  }
  
  public Map execute(ApplyNowVO applyNowVO){
    Map outMap = null;
    try{
      Map inputMap = new HashMap();
      inputMap.put("P_USER_ID", applyNowVO.getUserId());
      inputMap.put("P_COURSE_ID", applyNowVO.getCourseId());
      inputMap.put("P_OPPORTUNITY_ID", applyNowVO.getOpportunityId());
      //System.out.println("inputMap "+ inputMap);
      outMap = execute(inputMap);
      //System.out.println("outMap " + outMap);
    }catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class CourseDetailRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
      ApplyNowVO applyNowVO = new ApplyNowVO();            
      applyNowVO.setCollegeId(rs.getString("COLLEGE_ID"));
      applyNowVO.setCollegeName(rs.getString("COLLEGE_NAME"));
      applyNowVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));     
      applyNowVO.setCourseId(rs.getString("COURSE_ID"));
      applyNowVO.setCourseTitle(rs.getString("COURSE_TITLE"));
      applyNowVO.setCollegeLogoPath(rs.getString("COLLEGE_LOGO"));
      applyNowVO.setStudyMode(rs.getString("STUDY_MODE"));
      applyNowVO.setStudyDuration(rs.getString("STUDY_DURATION"));
      applyNowVO.setUcasTariffPoints(rs.getString("UCAS_TARIFF_POINTS"));      
      return applyNowVO;
    }
  }

}
