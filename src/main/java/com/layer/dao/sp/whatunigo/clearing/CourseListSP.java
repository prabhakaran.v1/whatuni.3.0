package com.layer.dao.sp.whatunigo.clearing;

import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.AutoCompleteVO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class CourseListSP extends StoredProcedure {
 
    public CourseListSP(DataSource dataSource) {
      setDataSource(dataSource);
      setFunction(true);
      setSql(SpringConstants.GET_CLEARING_COURSE_LIST);
      declareParameter(new SqlOutParameter("GET_AJAX_COURSE_LIST", OracleTypes.CURSOR, new getCourseListRowMapperImpl()));
      declareParameter(new SqlParameter("P_KEYWORD_TEXT", OracleTypes.VARCHAR));
      compile();
    }
    public Map execute(AutoCompleteVO autoCompleteVO){
      Map outMap = null;
      try{
        Map inputMap = new HashMap();  
        inputMap.put("P_KEYWORD_TEXT",autoCompleteVO.getKeywordText());
        //System.out.println(inputMap);
        outMap = execute(inputMap);
      }catch (Exception e) {
        e.printStackTrace();
      }
      return outMap;
    }
    private class getCourseListRowMapperImpl implements RowMapper{
      public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
        AutoCompleteVO autoCompleteVO = new AutoCompleteVO();            
        autoCompleteVO.setCourseTitle(rs.getString("DESC1"));
        autoCompleteVO.setDescription(rs.getString("DESCRIPTION"));
        autoCompleteVO.setCoursePageURL(rs.getString("URL"));
        autoCompleteVO.setCategoryCode(rs.getString("CATEGORY_CODE")); 
        //System.out.println("--"+autoCompleteVO.getCategoryCode());
        autoCompleteVO.setBrowseCatId(rs.getString("BROWSE_CAT_ID"));
        //System.out.println(autoCompleteVO.getBrowseCatId());
        autoCompleteVO.setCourseMessage(rs.getString("SD_MSG"));
        return autoCompleteVO;
      }
    }  
}
