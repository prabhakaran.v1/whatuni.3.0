package com.layer.dao.sp.whatunigo;

import WUI.whatunigo.bean.GradeFilterBean;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @see This SP class is used to call the database procedure to get the count of matching course based on subject and ucas points.
  * @since  28.03.2019- intital draft
  * @version 1.0
  *
  * Modification history:
  * *****************************************************************************************************************
  * Author          Relase tag       Modification Details                                                  Rel Ver.
  * *****************************************************************************************************************
  * Jeyalakshmi.D       1.0             initial draft                                                           wu_586
  *
  */

public class GetMatchingCourseSP extends StoredProcedure {
  DataSource dataSource;
  public GetMatchingCourseSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql("HOT_WUNI.WUGO_CLEARING_SEARCH_PKG.GET_MATCHING_COURSES_COUNT_PRC");
    declareParameter(new SqlParameter("P_SUBJECT_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_KEYWORD" , OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_GRADE_POINTS", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_COURSE_COUNT", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_QUALIFICATION_CODE", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_LOCATION", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_USER_UCAS_SCORE", OracleTypes.NUMBER));
    compile();
  }
  public Map execute(GradeFilterBean gradeFilterBean) {
    Map outMap = null;
    HashMap inMap = new HashMap();
    //
    try{
      inMap.put("P_SUBJECT_NAME", gradeFilterBean.getSubjectName());
      inMap.put("P_KEYWORD", gradeFilterBean.getKeyword());
      inMap.put("P_GRADE_POINTS", gradeFilterBean.getGradePoints());
      inMap.put("P_COLLEGE_NAME", gradeFilterBean.getCollegeName());
      inMap.put("P_QUALIFICATION_CODE", gradeFilterBean.getQualificationCode());
      inMap.put("P_LOCATION", gradeFilterBean.getLocation());
      inMap.put("P_USER_UCAS_SCORE", gradeFilterBean.getUserUcasScore());
      //System.out.println("inmap===" + inMap);
      outMap = execute(inMap);
      //System.out.println("outmap---" + outMap);
    } catch(Exception e){
      e.printStackTrace();
    }
    return outMap;
  }
  
}
