package com.layer.dao.sp.whatunigo.clearing;

import com.layer.util.SpringConstants;
import com.wuni.valueobjects.whatunigo.RegionListVO;
import com.wuni.valueobjects.whatunigo.SubRegionListVO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

 /**
   * @see This SP class is used to call the database procedure to get the region_list and sub_region_list.
   * @since  01.08.2019- intital draft
   * @version 1.0
   *
   * Modification history:
   * *****************************************************************************************************************
   * Author          Relase tag       Modification Details                                                  Rel Ver.
   * *****************************************************************************************************************
   * Sujitha V       1.0              initial draft                                                           
   *
   */
 
public class GetSubjectLandingRegionListSP extends StoredProcedure{

  public GetSubjectLandingRegionListSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql(SpringConstants.GET_WUGO_LOCTAION_LIST_PRC);
    declareParameter(new SqlOutParameter("PC_REGION_LIST", OracleTypes.CURSOR, new getRegionListRowMapperImpl()));
    compile();
  }
  public Map execute() throws Exception{
    Map outMap = null;
    Map inMap = new HashMap();
    outMap = execute(inMap);
    return outMap;
  }
  
  private class getRegionListRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
      RegionListVO regionListVO = new RegionListVO();
      regionListVO.setRegionId(rs.getString("REGION_ID"));
      regionListVO.setRegionName(rs.getString("REGION_NAME"));
      regionListVO.setUrlText(rs.getString("URL_TEXT"));
      ResultSet subRegionListRS = (ResultSet) rs.getObject("SUB_REGION_LIST");
      try{
        if(subRegionListRS != null){
          ArrayList subRegionList = new ArrayList();
          SubRegionListVO subRegionListVO = null;
          while(subRegionListRS.next()) {
            subRegionListVO = new SubRegionListVO();
            subRegionListVO.setRegionId(subRegionListRS.getString("REGION_ID"));
            subRegionListVO.setRegionName(subRegionListRS.getString("REGION_NAME"));
            subRegionListVO.setUrlText(subRegionListRS.getString("URL_TEXT"));
            if(!GenericValidator.isBlankOrNull(subRegionListRS.getString("REGION_ID"))) {
              subRegionList.add(subRegionListVO);
            }
          }
          regionListVO.setSubRegionList(subRegionList);
        }  
      }catch(Exception e){
        e.printStackTrace();
      }finally{
         try{
          if(subRegionListRS != null){
            subRegionListRS.close();
          }
         }catch(Exception e){
           throw new SQLException(e.getMessage());
         }
      }
      return regionListVO;
    } 
  }
}
