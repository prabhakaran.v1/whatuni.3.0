package com.layer.dao.sp.whatunigo;

import WUI.whatunigo.bean.GradeFilterBean;
import WUI.whatunigo.bean.WuGoBean;
import com.layer.util.SpringConstants;
import com.wuni.util.sql.OracleArray;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @see This SP class is used to call the database procedure to calculate UCAS points based on subjects entered.
  * @since  28.03.2019- intital draft
  * @version 1.0
  *
  * Modification history:
  * *****************************************************************************************************************
  * Author          Relase tag       Modification Details                                                  Rel Ver.
  * *****************************************************************************************************************
  * Jeyalakshmi.D       1.0             initial draft                                                           wu_586
  *
  */
  
public class UcasPointsAjaxSP extends StoredProcedure {
  DataSource datasource = null;
  public UcasPointsAjaxSP(DataSource datasource) {
    setDataSource(datasource);
    this.datasource = datasource;
    setSql(SpringConstants.CALCULATE_UCAS_POINTS_FN);
    setFunction(true);
    declareParameter(new SqlOutParameter("UCAS_POINTS", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("PT_QUAL_DETAIL_ARR", OracleTypes.ARRAY, "TB_QUALIFICATION_DETAIL"));
    compile();	 
  }

  public Map execute(GradeFilterBean gradeFilterBean) {
    //
    Connection connection = null;
    Map outMap = null;
    Map inMap = new HashMap();
    //
    try{
      connection = datasource.getConnection();
      ARRAY qualDetailsArr = OracleArray.getOracleArray(connection, "TB_QUALIFICATION_DETAIL", gradeFilterBean.getQualDetailsArr());
      inMap.put("PT_QUAL_DETAIL_ARR", qualDetailsArr);
      //System.out.println("UcasPointsAjaxSP----->"+inMap);
      outMap = execute(inMap);
      //System.out.println("outMap" + outMap);
    } catch(Exception e){
       e.printStackTrace();
    }
    finally {
      try {
	if (connection != null) {
	   connection.close();
	}
      } catch (SQLException closeException) {
	closeException.printStackTrace();
      }
   }
   return outMap;
  }
}
