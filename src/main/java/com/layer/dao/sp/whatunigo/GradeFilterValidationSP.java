package com.layer.dao.sp.whatunigo;

import WUI.whatunigo.bean.GradeFilterBean;
import com.wuni.util.sql.OracleArray;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;

import oracle.sql.ARRAY;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @see This SP class is used to call the database procedure to validate the user entry requirements.
  * @since  28.03.2019- intital draft
  * @version 1.0
  *
  * Modification history:
  * *****************************************************************************************************************
  * Author          Relase tag       Modification Details                                                  Rel Ver.
  * *****************************************************************************************************************
  * Jeyalakshmi.D       1.0             initial draft                                                           wu_586
  *
  */

public class GradeFilterValidationSP extends StoredProcedure {
  DataSource dataSource = null;
  public GradeFilterValidationSP(DataSource dataSource) {
    setDataSource(dataSource);
    this.dataSource = dataSource;
    setSql("HOT_WUNI.WUGO_CLEARING_SEARCH_PKG.QUAL_VALIDATION_PRC");
    declareParameter(new SqlParameter("PT_QUAL_DETAIL_ARR", OracleTypes.ARRAY, "TB_QUALIFICATION_DETAIL"));
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_SESSION_ID", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("P_VALIDATION_ERROR_CODE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_QUALIFICATION", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_QUALIFICATION1", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_QUALIFICATION2", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_QUALIFICATION3", OracleTypes.VARCHAR));
    compile();
  }
  public Map execute(GradeFilterBean gradeFilterBean){
    Connection connection = null;
    Map<String, Object> outMap = null;
    HashMap inMap = new HashMap();
    //
    try{
      connection = dataSource.getConnection();
      ARRAY qualDetailsArr = OracleArray.getOracleArray(connection, "TB_QUALIFICATION_DETAIL", gradeFilterBean.getQualDetailsArr());
      inMap.put("PT_QUAL_DETAIL_ARR", qualDetailsArr);
      inMap.put("P_USER_ID",gradeFilterBean.getUserId());
      inMap.put("P_SESSION_ID",gradeFilterBean.getSessionId());
      outMap = execute(inMap);
    } catch(Exception e){
       e.printStackTrace();
    }
    finally {
      try {
        if (connection != null) {
           connection.close();
        }
      } catch (SQLException closeException) {
        closeException.printStackTrace();
      }
    }
    return outMap;
    }
}
