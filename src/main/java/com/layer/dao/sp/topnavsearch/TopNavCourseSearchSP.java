package com.layer.dao.sp.topnavsearch;

import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.AutoCompleteVO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @ReviewSubjectListSP
  * @author Sangeeth.S
  * @version 1.0
  * @since 27.09.2019
  * @purpose This Class is used to get TOP NAV subject list for the search popup.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 27.09.2019     Sangeeth.S               wu_595     initial draft                                              18_NOV_2019
  *
  */

public class TopNavCourseSearchSP extends StoredProcedure{

  public TopNavCourseSearchSP(DataSource datasource) {
    setDataSource(datasource);    
    setSql(SpringConstants.TOP_NAV_COURSE_SEARCH_PRC);    
    declareParameter(new SqlParameter("P_KEYWORD_TEXT", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_QUALIFICATION_CODE", Types.VARCHAR));        
    declareParameter(new SqlOutParameter("P_BROWSE_CATEGORY_CODE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_BROWSE_CAT_ID", OracleTypes.NUMBER));
    //declareParameter(new SqlOutParameter("P_BROWSE_TEXT_KEY", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_GET_SUBJECT_LIST", OracleTypes.CURSOR, new SubjectListRowMapperImpl()));
  }
  
  public Map execute(AutoCompleteVO autoCompleteVO){
    Map outMap = null;
    Map inMap = new HashMap();
    try{
      inMap.put("P_KEYWORD_TEXT", autoCompleteVO.getKeywordText());
      inMap.put("P_QUALIFICATION_CODE", autoCompleteVO.getQualificationCode());      
      outMap = execute(inMap);      
    }catch(Exception e){
      e.printStackTrace();
    }
    return outMap;
  }
  
  private class SubjectListRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException{
      AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
      autoCompleteVO.setDescription(resultset.getString("DESCRIPTION"));
      autoCompleteVO.setUrl(resultset.getString("URL"));
      autoCompleteVO.setCategoryCode(resultset.getString("CATEGORY_CODE"));
      autoCompleteVO.setBrowseCatId(resultset.getString("BROWSE_CAT_ID"));     
      return autoCompleteVO;
    }
  }

}