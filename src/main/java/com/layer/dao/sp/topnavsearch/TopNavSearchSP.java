package com.layer.dao.sp.topnavsearch;

import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.AutoCompleteVO;
import com.wuni.util.valueobject.home.HomePageSearchFilterVO;
import com.wuni.valueobjects.topnavsearch.TopNavSearchVO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @TopNavSearchSP
  * @author Sangeeth.S
  * @version 1.0
  * @since 01.10.2019
  * @purpose This program is used to get TOP NAV search filters related data in the popup.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 01.10.2019     Sangeeth.S               wu_595     initial draft                                              18_NOV_2019
  *
  */
public class TopNavSearchSP extends StoredProcedure{

  public TopNavSearchSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql(SpringConstants.TOP_NAV_SEARCH_PRC);
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.VARCHAR));    
    declareParameter(new SqlParameter("P_TRACK_SESSION_ID", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("PC_RECENT_SUBJECT_SEARCH", OracleTypes.CURSOR, new RecentSearchRowmapperImpl()));    
    declareParameter(new SqlOutParameter("PC_LOCATION_LIST", OracleTypes.CURSOR, new LocationListRowmapperImpl()));
    declareParameter(new SqlOutParameter("PC_KEY_TOPICS_LIST", OracleTypes.CURSOR, new keyTopicsListRowmapperImpl()));
    declareParameter(new SqlOutParameter("PC_QUALIFICATION_LIST", OracleTypes.CURSOR, new QualificationListRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_CL_RECENT_SUBJECT_SEARCH", OracleTypes.CURSOR, new ClearingRecentSearchRowmapperImpl()));      
    //declareParameter(new SqlParameter("P_CLEARING_USER_TYPE", OracleTypes.VARCHAR));
  }
  
  public Map execute(TopNavSearchVO topNavSearchVO)
  {
    Map outMap = null;
    try{
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", topNavSearchVO.getUserId());
      inMap.put("P_TRACK_SESSION_ID", topNavSearchVO.getTrackSessionId());      
      //inMap.put("P_CLEARING_USER_TYPE", topNavSearchVO.getClearingUserType());
      outMap = execute(inMap);      
    }catch(Exception ex){
      ex.printStackTrace();
    }
    return outMap;
  }
  private class RecentSearchRowmapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      TopNavSearchVO topNavSearchVO = new TopNavSearchVO();      
      topNavSearchVO.setSubjectName(rs.getString("DESCRIPTION"));      
      topNavSearchVO.setSubjectUrl(rs.getString("URL")); 
      topNavSearchVO.setRegionName(rs.getString("LOCATION"));
      topNavSearchVO.setQualDesc(rs.getString("QUAL_LEVEL"));
      
      return topNavSearchVO;
    }
  }
  
  private class ClearingRecentSearchRowmapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      TopNavSearchVO topNavSearchVO = new TopNavSearchVO();      
      topNavSearchVO.setSubjectName(rs.getString("DESCRIPTION"));      
      topNavSearchVO.setSubjectUrl(rs.getString("URL")); 
      topNavSearchVO.setRegionName(rs.getString("LOCATION"));
      topNavSearchVO.setQualDesc(rs.getString("QUAL_LEVEL"));
      
      return topNavSearchVO;
    }
  }
  
  private class LocationListRowmapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      TopNavSearchVO topNavSearchVO = new TopNavSearchVO();      
      topNavSearchVO.setRegionName(rs.getString("REGION_NAME"));      
      topNavSearchVO.setRegionUrlText(rs.getString("URL_TEXT"));
      topNavSearchVO.setRegionId(rs.getString("REGION_ID"));      
      return topNavSearchVO;
    }
  }
  private class keyTopicsListRowmapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      TopNavSearchVO topNavSearchVO = new TopNavSearchVO();      
      topNavSearchVO.setKeytopics(rs.getString("KEY_TOPICS"));      
      topNavSearchVO.setKeyTopicUrl(rs.getString("URL"));      
      return topNavSearchVO;
    }
  }
  private class QualificationListRowMapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      TopNavSearchVO topNavSearchVO = new TopNavSearchVO();      
      topNavSearchVO.setQualDesc(rs.getString("QUAL_DESC"));      
      topNavSearchVO.setQualCode(rs.getString("QUAL_CODE"));
      topNavSearchVO.setUrlText(rs.getString("URL_TEXT"));      
      return topNavSearchVO;
    }
  }
}
