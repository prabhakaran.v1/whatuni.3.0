package com.layer.dao.sp;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.valueobjects.InsightsVO;

/**
  * This SP is used to get the institution id.
  * @author Thiyagu G.
  * @version 1.0
  * @since 31_May_2016 Release.
  * Change Log
  * *******************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
  * *******************************************************************************************************************************
  *
  *
  */
public class GetInstitutionIdSP extends StoredProcedure {
  public GetInstitutionIdSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_INSTITUTION_ID_FN);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.NUMERIC));    
    declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMERIC));
  }
  public Map execute(InsightsVO insightsVO) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();      
      inMap.put("p_college_id", insightsVO.getCollegeId());      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  
}
