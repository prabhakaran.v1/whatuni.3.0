package com.layer.dao.sp.contenthub;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import spring.rowmapper.common.SeoMetaDetailsRowMapperImpl;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.review.CommonPhrasesRowMapperImpl;
import com.layer.util.rowmapper.review.ReviewBreakDownRowMapperImpl;
import com.layer.util.rowmapper.review.ReviewSubjectRowMapperImpl;
import com.layer.util.rowmapper.search.UniRankingRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.contenthub.CampusVO;
import com.wuni.util.valueobject.contenthub.CollegeDetailsVO;
import com.wuni.util.valueobject.contenthub.ContentHubVO;
import com.wuni.util.valueobject.contenthub.CostOfLivingVO;
import com.wuni.util.valueobject.contenthub.EnquiryVO;
import com.wuni.util.valueobject.contenthub.GalleryVO;
import com.wuni.util.valueobject.contenthub.OpendayVO;
import com.wuni.util.valueobject.contenthub.PopularSubjectsVO;
import com.wuni.util.valueobject.contenthub.ReviewVO;
import com.wuni.util.valueobject.contenthub.SectionsVO;
import com.wuni.util.valueobject.contenthub.SocialVO;
import com.wuni.util.valueobject.contenthub.StudentStatsVO;
import com.wuni.util.valueobject.contenthub.UserDetailsVO;
import com.wuni.util.valueobject.ql.BasicFormVO;
import com.wuni.util.valueobject.ql.ClientLeadConsentVO;
import com.wuni.util.valueobject.search.LocationsInfoVO;

/**
  * @see This SP class is used to call the database procedure to get the details of the content hub page
  * @author Sabapathi
  * @since  23.01.2018- intital draft
  * @version 1.0
  *
  * Modification history:
  * *****************************************************************************************************************
  * Author          Relase tag       Modification Details                                                  Rel Ver.
  * *****************************************************************************************************************
  * Prabhakaran V.   1.1             Added out param for showing/hiding the opendays button
  * Sangeeth.S       1.2             Added open day start and end date in schema tag for time stamp format   wu_583
  * Sangeeth.S       1.3             Added in and out param to handle cmmt
  * Hema.S           1.4             Added client details cursor                                             wu_171219
  * Sangeeth.S       1.5             Added out column for event type
  * Sri Sankari R    1.1             Added new cursor and in param for dynamic meta details from Back office  wu_20201020
*/
public class GetContentHubDetailsSP extends StoredProcedure {

  public GetContentHubDetailsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.CONTENT_HUB_PROFILE_PRC);
    declareParameter(new SqlParameter("P_SESSION_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_BASKET_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_NETWORK_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_REQUEST_DESCRIPTION", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_CLIENT_IP", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_CLIENT_BROWSER", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_SECTION", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_REQUEST_URL", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_REFEREL_URL", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_META_PAGE_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_META_PAGE_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_TRACK_SESSION", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_USER_JOURNEY", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("OC_UNI_STATS_INFO", OracleTypes.CURSOR, new CollegeUnistatsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_OPEN_DAY_INFO", OracleTypes.CURSOR, new OpendayInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_LATEST_UNI_REVIEWS", OracleTypes.CURSOR, new LatestUniReviewsRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_STUDENT_UNI_REVIEWS", OracleTypes.CURSOR, new StudentUniReviewsRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_ENQUIRY_INFO", OracleTypes.CURSOR, new EnquiryInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_PROFILE_SECTIONS", OracleTypes.CURSOR, new ProfileSectionsRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_CAMPUSES_VENUE", OracleTypes.CURSOR, new CampusVenueRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_COLLEGE_DETAILS", OracleTypes.CURSOR, new CollegeDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_UNI_RAKING", OracleTypes.CURSOR, new UniRankingRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_SOCIAL_NETWORK_URLS", OracleTypes.CURSOR, new SocialNetworkUrlRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_COST_OF_LIVING", OracleTypes.CURSOR, new CostOfLivingRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_HERO_GALLERY", OracleTypes.CURSOR, new HeroGalleryRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_POPULAR_SUBJECTS", OracleTypes.CURSOR, new PopularSubjectsRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_WUSCA_CATEGORIES_RATINGS", OracleTypes.CURSOR, new WuscaCategoriesRatingRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_USER_DETAILS", OracleTypes.CURSOR, new UserDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_USER_SUBSCRIPTION_FLAGS", OracleTypes.CURSOR, new UserSubscriptionFlagRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_GET_ADMIN_VENUE", OracleTypes.CURSOR, new LocationsInfoRowMapperImpl())); 
    declareParameter(new SqlOutParameter("PC_REVIEW_SUBJECT", OracleTypes.CURSOR,new ReviewSubjectRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_REVIEW_BREAKDOWN", OracleTypes.CURSOR,new ReviewBreakDownRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_COMMON_PHRASES", OracleTypes.CURSOR, new CommonPhrasesRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_AVG_RATING", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("O_LOG_STATUS_ID", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("O_COLLEGE_IN_BASKET", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_PIXEL_TRACKING_CODE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_COLLEGE_UCAS_CODE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_TYPE_KEY", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_JOB_PERCENT", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_COURSE_EXIST_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_STUDY_ABROAD_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_FINAL_CHOICES_EXIST_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_HIDE_LOCAION_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_ENQUIRY_EXIST_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_hide_od_button_flag", OracleTypes.VARCHAR)); //Version 1.1
    declareParameter(new SqlOutParameter("P_CL_COURSE_EXIST_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_CLIENT_CONSENT_DETAILS", OracleTypes.CURSOR, new ClientConsentDetailsRowMapperImpl()));//Added this for showing consent flag details By Hema.S on 17_12_2019_REL
    //datasource.declareParameter(new SqlOutParameter("P_CLEARING_SWITCH_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_URL", OracleTypes.VARCHAR)); // Added in param for dynamic meta details from Back office, 20201020 rel by Sri Sankari
    declareParameter(new SqlOutParameter("PC_META_DETAILS", OracleTypes.CURSOR, new SeoMetaDetailsRowMapperImpl())); // Added new cursor for dynamic meta details from Back office, 20201020 rel by Sri Sankari
    compile();
  }

  public Map execute(ContentHubVO contentHubVO) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("P_SESSION_ID", contentHubVO.getSessionId());
      inputMap.put("P_USER_ID", contentHubVO.getUserId());
      inputMap.put("P_BASKET_ID", contentHubVO.getBasketId());
      inputMap.put("P_COLLEGE_ID", contentHubVO.getCollegeId());
      inputMap.put("P_NETWORK_ID", contentHubVO.getNetworkId());
      inputMap.put("P_REQUEST_DESCRIPTION", contentHubVO.getRequestDesc());
      inputMap.put("P_CLIENT_IP", contentHubVO.getClientIp());
      inputMap.put("P_CLIENT_BROWSER", contentHubVO.getClientBrowser());
      inputMap.put("P_COLLEGE_SECTION", contentHubVO.getCollegeSection());
      inputMap.put("P_REQUEST_URL", contentHubVO.getRequestUrl());
      inputMap.put("P_REFEREL_URL", contentHubVO.getReferelUrl());
      inputMap.put("P_META_PAGE_NAME", contentHubVO.getMetaPageName());
      inputMap.put("P_META_PAGE_FLAG", contentHubVO.getMetaPageFlag());
      inputMap.put("P_TRACK_SESSION", contentHubVO.getTrackSession());
      inputMap.put("P_USER_JOURNEY", contentHubVO.getUserJourney());
      inputMap.put("P_URL", contentHubVO.getContentHubPageUrl());
      outMap = execute(inputMap); 
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }

  public class UserDetailsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      UserDetailsVO userDetailsVO = new UserDetailsVO();
      userDetailsVO.setEmailAddress(GenericValidator.isBlankOrNull(rs.getString("USER_EMAIL")) ? "" : rs.getString("USER_EMAIL"));
      userDetailsVO.setFirstName(GenericValidator.isBlankOrNull(rs.getString("FORENAME")) ? "" : rs.getString("FORENAME"));
      userDetailsVO.setLastName(GenericValidator.isBlankOrNull(rs.getString("SURNAME")) ? "" : rs.getString("SURNAME"));
      userDetailsVO.setPostCode(GenericValidator.isBlankOrNull(rs.getString("POSTCODE")) ? "" : rs.getString("POSTCODE"));
      userDetailsVO.setDefaultYoe(rs.getString("CURRENT_YEAR_OF_ENTRY"));
      userDetailsVO.setYeartoJoinCourse(GenericValidator.isBlankOrNull(rs.getString("YEAR_OF_COURSE")) ? userDetailsVO.getDefaultYoe() : rs.getString("YEAR_OF_COURSE"));
      userDetailsVO.setDateOfBirth(GenericValidator.isBlankOrNull(rs.getString("DOB")) ? null : rs.getString("DOB"));
      return userDetailsVO;
    }

  }

  public class LatestUniReviewsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      SeoUrls seoUrl = new SeoUrls();
      ReviewVO reviewsVO = new ReviewVO();
      reviewsVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      reviewsVO.setReviewId(resultset.getString("REVIEW_ID"));
      reviewsVO.setUserId(resultset.getString("USER_ID"));
      reviewsVO.setReviewTitle(resultset.getString("REVIEW_TITLE"));
      reviewsVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
      reviewsVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
      reviewsVO.setUserName(resultset.getString("USER_NAME"));
      reviewsVO.setOverAllRating(resultset.getString("OVERALL_RATING"));
      reviewsVO.setAtUni(resultset.getString("AT_UNI"));
      reviewsVO.setGender(resultset.getString("GENDER"));
      reviewsVO.setCourseTitle(resultset.getString("COURSE_TITLE"));
      reviewsVO.setCourseId(resultset.getString("COURSE_ID"));
      reviewsVO.setCourseDeletedFlag(resultset.getString("COURSE_DELETED_FLAG"));
      reviewsVO.setSeoStudyLevelText(resultset.getString("SEO_STUDY_LEVEL_TEXT"));
      reviewsVO.setReviewText(resultset.getString("REVIEW_TEXT"));
      reviewsVO.setReviewedDate(resultset.getString("REVIEWED_DATE"));
      reviewsVO.setUserNameInitial(resultset.getString("USER_NAME_INT"));
      reviewsVO.setCourseDetailsPageURL(seoUrl.construnctCourseDetailsPageURL(reviewsVO.getSeoStudyLevelText(), reviewsVO.getCourseTitle(), reviewsVO.getCourseId(), reviewsVO.getCollegeId(), "COURSE_SPECIFIC"));
      return reviewsVO;
    }

  }

  private class EnquiryInfoRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      EnquiryVO enquiryInfoVO = new EnquiryVO();
      enquiryInfoVO.setOrderItemId(resultset.getString("ORDER_ITEM_ID"));
      enquiryInfoVO.setEmail(resultset.getString("EMAIL"));
      enquiryInfoVO.setEmailWebform(resultset.getString("EMAIL_WEBFORM"));
      String emailFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getEmail()) || !GenericValidator.isBlankOrNull(enquiryInfoVO.getEmailWebform())) ? "Y" : "N";
      String emailWebFormFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getEmailWebform())) ? "Y" : "N";
      enquiryInfoVO.setEmailWebFormFlag(emailWebFormFlag);
      enquiryInfoVO.setEmailFlag(emailFlag);
      enquiryInfoVO.setProspectus(resultset.getString("PROSPECTUS"));
      enquiryInfoVO.setProspectusWebform(resultset.getString("PROSPECTUS_WEBFORM"));
      String prospectusFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getProspectus()) || !GenericValidator.isBlankOrNull(enquiryInfoVO.getProspectusWebform())) ? "Y" : "N";
      enquiryInfoVO.setProspectusFlag(prospectusFlag);
      enquiryInfoVO.setWebsite(resultset.getString("WEBSITE"));
      String websiteFlag = (!GenericValidator.isBlankOrNull(enquiryInfoVO.getWebsite())) ? "Y" : "N";
      enquiryInfoVO.setWebsiteFlag(websiteFlag);
      enquiryInfoVO.setSubOrderItemId(resultset.getString("SUBORDER_ITEM_ID"));
      enquiryInfoVO.setMyhcProfileId(resultset.getString("MYHC_PROFILE_ID"));
      enquiryInfoVO.setProfileType(resultset.getString("PROFILE_TYPE"));
      enquiryInfoVO.setNetworkId(resultset.getString("NETWORK_ID"));
      enquiryInfoVO.setHotline(resultset.getString("HOTLINE"));
      enquiryInfoVO.setAdvertName(resultset.getString("ADVERT_NAME"));
      enquiryInfoVO.setWebsitePrice(resultset.getString("WEBSITE_PRICE"));
      enquiryInfoVO.setWebformPrice(resultset.getString("WEBFORM_PRICE"));
      return enquiryInfoVO;
    }

  }

  public class ProfileSectionsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SectionsVO sectionsVO = new SectionsVO();
      sectionsVO.setCollegeId(rs.getString("COLLEGE_ID"));
      sectionsVO.setDescription(rs.getString("DESCRIPTION"));
      sectionsVO.setMediaTypeId(rs.getString("MEDIA_TYPE_ID"));
      sectionsVO.setMediaId(rs.getString("MEDIA_ID"));
      String mediaType = null;
      if ("27".equals(sectionsVO.getMediaTypeId())) {
        mediaType = "IMAGE";
      }
      if ("29".equals(sectionsVO.getMediaTypeId())) {
        mediaType = "VIDEO";
      }
      sectionsVO.setMediaType(mediaType);
      sectionsVO.setImagePath(rs.getString("IMAGE_PATH"));
      sectionsVO.setSectionNameDisplay(rs.getString("SECTION_NAME_DISPLAY"));
      sectionsVO.setSectionName(rs.getString("SECTION_NAME"));
      sectionsVO.setProfileId(rs.getString("PROFILE_ID"));
      sectionsVO.setMyhcProfileId(rs.getString("MYHC_PROFILE_ID"));
      sectionsVO.setMediaName(rs.getString("MEDIA_NAME"));
      sectionsVO.setThumbnailPath(rs.getString("THUMBNAIL_PATH"));
      sectionsVO.setWuscaRank(rs.getString("WUSCA_RANK"));
      sectionsVO.setWuscaOverall(rs.getString("TOTAL_RANK_IN_CATEGORY"));
      sectionsVO.setAwardImage(rs.getString("AWARD_IMAGE")); 
      sectionsVO.setSubOrderItemId(rs.getString("SUBORDER_ITEM_ID"));
      return sectionsVO;
    }

  }

  public class SocialNetworkUrlRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SocialVO socialVO = new SocialVO();
      socialVO.setSocialNetworkName(rs.getString("SOCIAL_NETWORK_NAME"));
      socialVO.setSocialNetworkUrl(rs.getString("SOCIAL_NETWORK_URL"));
      return socialVO;
    }

  }

  public class CollegeUnistatsInfoRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      StudentStatsVO studentStatsVO = new StudentStatsVO();
      studentStatsVO.setStatsDescription(rs.getString("STATS_DESC"));
      studentStatsVO.setStatsValue1(rs.getString("STAT_VALUE1"));
      studentStatsVO.setStatsValue2(rs.getString("STAT_VALUE2"));
      studentStatsVO.setCustomizedFlag(rs.getString("CUSTOMIZED_FLAG"));
      return studentStatsVO;
    }

  }

  public class OpendayInfoRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      OpendayVO opendayVO = new OpendayVO();
      opendayVO.setEventId(rs.getString("EVENT_ID"));
      opendayVO.setOpenDate(rs.getString("OPEN_DATE"));
      opendayVO.setOpenDayDesc(rs.getString("openday_desc"));
      opendayVO.setOpenDayFlag(rs.getString("OD_FLAG"));
      opendayVO.setSuborderItemId(rs.getString("SUB_ORDER_ITEM_ID"));
      opendayVO.setTiming(rs.getString("TIMING"));
      opendayVO.setBookingUrl(rs.getString("BOOKING_URL"));//booking_url
      opendayVO.setQualType(rs.getString("QUAL_TYPE"));
      opendayVO.setCollegeId(rs.getString("COLLEGE_ID"));
      opendayVO.setCollegeDispName(rs.getString("COLLEGE_NAME_DISPLAY"));
      //opendayVO.setStartDate(rs.getString("START_DATE"));
      opendayVO.setBookingUrl(rs.getString("BOOKING_URL"));
      opendayVO.setQualId(rs.getString("QUAL_ID"));
      opendayVO.setQualDesc(rs.getString("QUAL_DESC"));
      opendayVO.setNetworkId(rs.getString("NETWORK_ID"));
      opendayVO.setMonthYear(rs.getString("OPEN_MONTH_YEAR"));
      opendayVO.setWebsitePrice(rs.getString("WEBSITE_PRICE"));
      opendayVO.setOpenday(rs.getString("OPENDATE"));
      opendayVO.setCollegeName(rs.getString("COLLEGE_NAME"));
      opendayVO.setHeadline(rs.getString("headlines"));
      opendayVO.setOpendayDesc(rs.getString("openday_desc"));
      opendayVO.setOpendayVenue(rs.getString("event_venue"));
      //opendayVO.setEndDate(rs.getString("end_date"));
      opendayVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
      //Added open day start and end date in schema tag for time stamp format for Nov_20 th rel by Sangeeth.S
      opendayVO.setStOpenDayStartDate(rs.getString("schematag_od_start_date"));
      opendayVO.setStOpenDayEndDate(rs.getString("schematag_od_end_date"));
      opendayVO.setEventCategoryId(rs.getString("EVENT_CATEGORY_ID"));
      opendayVO.setEventCategoryName(rs.getString("EVENT_CATEGORY_NAME"));
      //
      opendayVO.setOpendaysProviderURL(new SeoUrls().constructOpendaysSeoUrl(opendayVO.getCollegeName(), opendayVO.getCollegeId()));
      return opendayVO;
    }

  }

  public class CampusVenueRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CampusVO campusVO = new CampusVO();
      campusVO.setVenueId(rs.getString("VENUE_ID"));
      campusVO.setVenueName(rs.getString("VENUE_NAME"));
      campusVO.setAddressOne(rs.getString("ADD_LINE_1"));
      campusVO.setAddressTwo(rs.getString("ADD_LINE_2"));
      campusVO.setPostcode(rs.getString("POSTCODE"));
      campusVO.setCountryState(rs.getString("COUNTRY_STATE"));
      campusVO.setLongitude(rs.getString("LONGITUDE"));
      campusVO.setLatitude(rs.getString("LATITUDE"));
      campusVO.setMediaId(rs.getString("MEDIA_ID"));
      campusVO.setMediaPath(rs.getString("MEDIA_PATH"));
      campusVO.setMediaTypeId(rs.getString("MEDIA_TYPE_ID"));
      campusVO.setMediaName(rs.getString("MEDIA_NAME"));
      campusVO.setThumbNailPath(rs.getString("THUMBNAIL_PATH"));
      campusVO.setOnlineFlag(rs.getString("ONLINE_FLAG"));
      campusVO.setPromotionalText(rs.getString("PROMOTIONAL_TEXT"));
      return campusVO;
    }

  }

  public class CollegeDetailsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CollegeDetailsVO collegeDetailsVO = new CollegeDetailsVO();
      collegeDetailsVO.setCollegeId(rs.getString("COLLEGE_ID"));
      collegeDetailsVO.setCollegeName(rs.getString("COLLEGE_NAME"));
      collegeDetailsVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
      collegeDetailsVO.setReviewCount(rs.getString("REVIEW_COUNT"));
      collegeDetailsVO.setOverAllRating(rs.getString("OVERALL_RATING"));
      collegeDetailsVO.setAbsoluteRating(rs.getString("OVERALL_RATING_EXACT"));
      collegeDetailsVO.setCollegeLogo(rs.getString("COLLEGE_LOGO"));
      return collegeDetailsVO;
    }

  }

  public class CostOfLivingRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CostOfLivingVO costOfLivingVO = new CostOfLivingVO();
      costOfLivingVO.setAccomodationLower(rs.getString("ACCOMMODATION_LOWER"));
      costOfLivingVO.setAccomodationUpper(rs.getString("ACCOMMODATION_UPPER"));
      costOfLivingVO.setAccomLowerBarLimit(rs.getString("ACCOM_LOWER_BAR_LIMIT"));
      costOfLivingVO.setAccomUpperBarLimit(rs.getString("ACCOM_UPPER_BAR_LIMIT"));
      costOfLivingVO.setLivingCost(rs.getString("LIVING_COST"));
      costOfLivingVO.setLivingBarLimit(rs.getString("LIVING_BAR_LIMIT"));
      costOfLivingVO.setCostPint(rs.getString("WHATUNI_COST_PINT"));
      costOfLivingVO.setAccomToolTipFlag(rs.getString("MYHC_ACCOM_TOOL_TIP_FLAG"));
      return costOfLivingVO;
    }

  }

  public class WuscaCategoriesRatingRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewVO reviewVO = new ReviewVO();
      reviewVO.setQuestionTitle(rs.getString("QUESTION_TITLE"));
      reviewVO.setRating(rs.getString("RATING"));
      reviewVO.setRoundRating(rs.getString("ROUND_RATING"));
      return reviewVO;
    }

  }

  public class StudentUniReviewsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewVO reviewsVO = new ReviewVO();
      reviewsVO.setMediaId(rs.getString("MEDIA_ID"));
      reviewsVO.setMediaTypeId(rs.getString("MEDIA_TYPE_ID"));
      reviewsVO.setMediaPath(rs.getString("MEDIA_PATH"));
      reviewsVO.setStudentName(rs.getString("STUDENT_NAME"));
      reviewsVO.setStudentShortDesc(rs.getString("STUDENT_SHORT_DESC"));
      reviewsVO.setMediaName(rs.getString("MEDIA_NAME"));
      reviewsVO.setStudentView(rs.getString("STUDENT_VIEW"));
      reviewsVO.setSubjectOfStudy(rs.getString("SUBJECT_OF_STUDY"));
      reviewsVO.setThumbNailPath(rs.getString("THUMBNAIL_PATH"));
      return reviewsVO;
    }

  }

  public class HeroGalleryRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      GalleryVO galleryVO = new GalleryVO();
      galleryVO.setMediaId(rs.getString("MEDIA_ID"));
      galleryVO.setMediaPath(rs.getString("MEDIA_PATH"));
      galleryVO.setMediaTypeId(rs.getString("MEDIA_TYPE_ID"));
      galleryVO.setMediaName(rs.getString("MEDIA_NAME"));
      galleryVO.setRichFlag(rs.getString("RICH_FLAG"));
      galleryVO.setThumbNailPath(rs.getString("THUMBNAIL_PATH"));
      galleryVO.setPromotionalText(rs.getString("PROMOTIONAL_TEXT"));
      return galleryVO;
    }

  }

  public class PopularSubjectsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      PopularSubjectsVO popularSubjectsVO = new PopularSubjectsVO();
      popularSubjectsVO.setSubjectId(rs.getString("SUBJECT_ID"));
      popularSubjectsVO.setSubjectName(rs.getString("SUBJECT_NAME"));
      popularSubjectsVO.setBrowseTextKey(rs.getString("BROWSE_TEXT_KEY"));
      popularSubjectsVO.setImageName(rs.getString("IMAGE_NAME"));
      popularSubjectsVO.setSubjectCat(rs.getString("SUBJECT_CATEGORY"));
      ResultSet spDetailsRS = null;
      spDetailsRS = (ResultSet)rs.getObject("SP_DETAILS");
      try {
        if (spDetailsRS != null) {
          while (spDetailsRS.next()) {
            popularSubjectsVO.setCollegeId(spDetailsRS.getString("COLLEGE_ID"));
            popularSubjectsVO.setCollegeName(spDetailsRS.getString("COLLEGE_NAME"));
            popularSubjectsVO.setSubjectOrderItemId(spDetailsRS.getString("ORDER_ITEM_ID"));
            popularSubjectsVO.setSubjectMyhcProfileId(spDetailsRS.getString("MYHC_PROFILE_ID"));
            popularSubjectsVO.setAdvertName(spDetailsRS.getString("ADVERT_NAME"));
          }
        }
      } finally {
        try {
          if (spDetailsRS != null) {
            spDetailsRS.close();
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
      return popularSubjectsVO;
    }

  }
  
  private class UserSubscriptionFlagRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{       
      BasicFormVO basicFormVO = new BasicFormVO();      
      basicFormVO.setMarketingEmail(rs.getString("marketing_email_flag"));
      basicFormVO.setSolusEmail(rs.getString("solus_email_flag"));
      basicFormVO.setSurveyEmail(rs.getString("survey_flag"));
      return basicFormVO;
    }
  }
  
  public class LocationsInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      LocationsInfoVO locationsInfoVO = new LocationsInfoVO();
      //
      locationsInfoVO.setTown(resultset.getString("town"));
      locationsInfoVO.setPostcode(resultset.getString("postcode"));
      locationsInfoVO.setAddLine1(resultset.getString("add_line_1"));
      locationsInfoVO.setAddLine2(resultset.getString("add_line_2"));
      locationsInfoVO.setCountryState(resultset.getString("country_state"));
      //
      return locationsInfoVO;
    }
  }
  //Added this for showing consent flag details By Hema.S on 17_12_2019_REL
  private class ClientConsentDetailsRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{ 
	  ClientLeadConsentVO clientLeadConsentVO	 = new ClientLeadConsentVO();
      clientLeadConsentVO.setConsentText(rs.getString("consent_text"));
      clientLeadConsentVO.setClientMarketingConsent(rs.getString("client_marketing_consent"));
	  return clientLeadConsentVO;
	}
  }
}
