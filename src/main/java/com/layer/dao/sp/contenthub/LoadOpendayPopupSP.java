package com.layer.dao.sp.contenthub;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.util.valueobject.contenthub.OpendayVO;


public class LoadOpendayPopupSP extends StoredProcedure {
  public LoadOpendayPopupSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_OPENDAYS_DETAILS_PRC);
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_EVENT_ID", Types.NUMERIC));
    declareParameter(new SqlOutParameter("PC_OPENDAYS_INFO", OracleTypes.CURSOR, new OpendayInfoRowMapperNew()));
    declareParameter(new SqlOutParameter("PC_OD_DET_LIST", OracleTypes.CURSOR, new OpendayDetailsRowMapperImpl()));
    declareParameter(new SqlParameter("P_QUAL_CODE", Types.VARCHAR));
    compile();
  }
  public Map execute(OpendayVO inputBean) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("P_COLLEGE_ID", inputBean.getCollegeId());
      inMap.put("P_EVENT_ID", inputBean.getEventId());
      inMap.put("P_QUAL_CODE", inputBean.getQualCode());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }    
    return outMap;
  }
  private class OpendayInfoRowMapperNew implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      OpendayVO opendayVO = new OpendayVO();    
      opendayVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      opendayVO.setCollegeLogo(resultset.getString("COLLEGE_LOGO"));
     // opendayVO.setCollegeId(resultset.getString("refine_desc"));
      opendayVO.setCollegeDispName(resultset.getString("COLLEGE_NAME_DISPLAY"));
      opendayVO.setOpenDate(resultset.getString("OPEN_DATE"));
      opendayVO.setStartDate(resultset.getString("START_DATE"));
      opendayVO.setEventCalItemId(resultset.getString("EVENT_CALENDAR_ITEM_ID"));
      opendayVO.setEventDesp(resultset.getString("EVENT_DESCRIPTION"));
      opendayVO.setStartTime(resultset.getString("START_TIME"));
      opendayVO.setEndTime(resultset.getString("END_TIME"));
      opendayVO.setBookingUrl(resultset.getString("BOOKING_URL"));
      opendayVO.setVenue(resultset.getString("VENUE"));
      opendayVO.setQualId(resultset.getString("QUAL_ID"));
      opendayVO.setQualDesc(resultset.getString("QUAL_DESC"));
      opendayVO.setSuborderItemId(resultset.getString("SUBORDER_ITEM_ID"));
      opendayVO.setNetworkId(resultset.getString("NETWORK_ID"));
      opendayVO.setMonthYear(resultset.getString("OPEN_MONTH_YEAR"));
      opendayVO.setWebsitePrice(resultset.getString("WEBSITE_PRICE"));
      opendayVO.setOpenday(resultset.getString("OPENDATE"));
      return opendayVO; 
    }
  }
  private class OpendayDetailsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      OpendayVO opendayVO = new OpendayVO();
      opendayVO.setOpendayDetail(resultSet.getString("OPEN_DAYS_DET"));
      opendayVO.setEventId(resultSet.getString("EVENT_ID"));
      opendayVO.setSelectedTxt(resultSet.getString("SELECTED_TEXT"));
      return opendayVO;
    }
  }
}
