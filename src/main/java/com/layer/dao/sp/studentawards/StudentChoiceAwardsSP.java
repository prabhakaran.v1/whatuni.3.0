package com.layer.dao.sp.studentawards;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;

import com.layer.util.SpringConstants;
import com.wuni.util.form.studentawards.StudentAwardsBean;
import com.wuni.util.seo.SeoUrls;

/**
 * StudentChoiceAwardsSP is used to fetch results for WUSCA page
 * @author Amirtharaj
 * @since 13_JAN_15
 *
 */
public class StudentChoiceAwardsSP extends StoredProcedure {
  public StudentChoiceAwardsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.STUDENT_CHOICE_AWARDS_LIST_PRC);
    declareParameter(new SqlParameter("P_NETWORK_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_PAGE_NO", Types.VARCHAR));
    declareParameter(new SqlParameter("P_AWARD_CURR_YEAR", Types.VARCHAR));
    declareParameter(new SqlParameter("P_AWARD_LAST_YEAR", Types.VARCHAR));
    declareParameter(new SqlParameter("P_QUESTION_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
    declareParameter(new SqlOutParameter("pc_uni_rank_list", OracleTypes.CURSOR, new UniRankListRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_category_list", OracleTypes.CURSOR, new CategoryListRowMapperImpl()));
    declareParameter(new SqlOutParameter("p_sponser_by", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_sponser_url", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_uni_cnt", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_bread_crumb", Types.VARCHAR));
  }
  public Map execute(StudentAwardsBean studAwdBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_NETWORK_ID", studAwdBean.getNetworkId());
      inMap.put("P_PAGE_NO", studAwdBean.getPageNo());
      inMap.put("P_AWARD_CURR_YEAR", studAwdBean.getCurYear());
      inMap.put("P_AWARD_LAST_YEAR", studAwdBean.getLastYear());
      inMap.put("P_QUESTION_ID", studAwdBean.getQuestionId());
      inMap.put("P_COLLEGE_ID", studAwdBean.getCollegeId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class UniRankListRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      StudentAwardsBean studAwdBean = new StudentAwardsBean();
      studAwdBean.setCurYear(rs.getString("current_year_rank"));
      studAwdBean.setCurYearSubScript(String.valueOf(new CommonFunction().getOrdinalFor(Integer.parseInt(rs.getString("current_year_rank")))));
      studAwdBean.setLastYear(rs.getString("previous_year_rank"));
      if (!GenericValidator.isBlankOrNull(studAwdBean.getLastYear())) {
        studAwdBean.setLastYearSubScript(String.valueOf(new CommonFunction().getOrdinalFor(Integer.parseInt(rs.getString("previous_year_rank")))));
      }
      studAwdBean.setRankStatus(rs.getString("rank_status"));
      studAwdBean.setCollegeId(rs.getString("college_id"));
      studAwdBean.setCollegeName(rs.getString("college_name"));
      studAwdBean.setCollegeDisplayName(rs.getString("college_name_display"));
      studAwdBean.setRating(rs.getString("rating"));
      if (!GenericValidator.isBlankOrNull(studAwdBean.getRating())) {
        studAwdBean.setRatingPercent(String.valueOf(Double.parseDouble(studAwdBean.getRating()) * 20) + "%");
      }
      studAwdBean.setCollegeLogo(rs.getString("college_logo"));
      if (!GenericValidator.isBlankOrNull(studAwdBean.getCollegeLogo())) {
        studAwdBean.setCollegeLogo(new CommonUtil().getImgPath((studAwdBean.getCollegeLogo()), rowNum));
      }
      studAwdBean.setUniHomeURL(new SeoUrls().construnctUniHomeURL(studAwdBean.getCollegeId(), studAwdBean.getCollegeName()));
      ResultSet interactionDetails = (ResultSet)rs.getObject("ENQUIRY_DETAILS");
      try {
        if (interactionDetails != null) {
          while (interactionDetails.next()) {
            String tmpUrl = null;
            studAwdBean.setOrderItemId(interactionDetails.getString("order_item_id"));
            studAwdBean.setSubOrderEmail(interactionDetails.getString("email"));
            studAwdBean.setSubOrderEmailWebform(interactionDetails.getString("email_webform"));
            studAwdBean.setSubOrderProspectus(interactionDetails.getString("prospectus"));
            studAwdBean.setSubOrderProspectusWebform(interactionDetails.getString("prospectus_webform"));
            studAwdBean.setSubOrderItemId(interactionDetails.getString("suborder_item_id"));
            studAwdBean.setMyhcProfileId(interactionDetails.getString("myhc_profile_id"));
            studAwdBean.setProfileType(interactionDetails.getString("profile_type"));
            studAwdBean.setNetworkId(interactionDetails.getString("network_id"));
            studAwdBean.setHotline(interactionDetails.getString("hotline"));
            studAwdBean.setMediaId(interactionDetails.getString("media_id"));
            studAwdBean.setMediaPath(interactionDetails.getString("media_path"));
            studAwdBean.setMediaThumbPath(interactionDetails.getString("thumb_media_path"));
            studAwdBean.setMediaType(interactionDetails.getString("media_type"));
            studAwdBean.setAdvertName(interactionDetails.getString("advert_name"));
            studAwdBean.setQlFlag(interactionDetails.getString("ql_flag"));
            tmpUrl = interactionDetails.getString("website");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            studAwdBean.setSubOrderWebsite(tmpUrl);
            tmpUrl = interactionDetails.getString("email_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            studAwdBean.setSubOrderEmailWebform(tmpUrl);
            tmpUrl = interactionDetails.getString("prospectus_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            studAwdBean.setSubOrderProspectusWebform(tmpUrl);
          }
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      } finally {
        interactionDetails.close();
      }
      return studAwdBean;
    }
  }
  private class CategoryListRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      StudentAwardsBean studAwdBean = new StudentAwardsBean();
      studAwdBean.setQuestionId(rs.getString("question_id"));
      studAwdBean.setQuestionTitle(rs.getString("question_title"));
      return studAwdBean;
    }
  }
}
