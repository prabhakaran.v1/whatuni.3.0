package com.layer.dao.sp.studentawards;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.wuni.util.form.studentawards.StudentAwardsBean;

/**
 * AwardsYearListSP is used to fetch results for WUSCA page
 * @author Indumathi.S
 * @since Mar-08-16
 *
 */
public class AwardsYearListSP extends StoredProcedure {

  public AwardsYearListSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(SpringConstants.GET_AWARD_YEAR_FN);
    declareParameter(new SqlOutParameter("pc_award_year_list", OracleTypes.CURSOR, new AwardsYearListRowMapperImpl()));
  }

  public Map execute(StudentAwardsBean studAwdBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }

  private class AwardsYearListRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      StudentAwardsBean studAwdBean = new StudentAwardsBean();
      studAwdBean.setCurrentYear(rs.getString("current_year"));
      return studAwdBean;
    }
  }
}
