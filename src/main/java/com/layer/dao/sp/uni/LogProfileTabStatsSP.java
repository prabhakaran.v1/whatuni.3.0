package com.layer.dao.sp.uni;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import mobile.valueobject.ProfileVO;
import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
/**
  * @LogProfileTabStatsSP
  * @author Pryaa Parthasarathy
  * @version 1.0
  * @since 3-Jun-2014
  * @purpose This program is used to get stats key
  * Change Log
  * ******************************************************************************************
  * name                ver                  date            Changes desc
  * ******************************************************************************************
  * Sabapathi S.        wu_582              23-Oct-2018      Added scren width for stats logging
  * Sangeeth.S			wu_31032020	        09_MAR_2020		 Added lat and long input param
  * Sri Sankari		    wu_20200917         17_SEP_2020      Added stats log for tariff logging(UCAS point)
  * Sujitha V           wu_20201110         10_NOV_2020      Added YOE value in d_session_log stats
  */
public class LogProfileTabStatsSP extends StoredProcedure {
  public LogProfileTabStatsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("wu_college_info_pkg.college_profile_log_prc");
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("p_profile_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_myhc_profile_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_required_desc", Types.VARCHAR));
    declareParameter(new SqlParameter("p_section_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_ip", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
    declareParameter(new SqlParameter("p_request_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_referer_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_network_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_name", Types.VARCHAR));//24_JUN_2014
    declareParameter(new SqlOutParameter("p_log_status_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_type_key", Types.VARCHAR));
    declareParameter(new SqlParameter("p_track_session", Types.VARCHAR));
    declareParameter(new SqlParameter("p_mobile_flag", Types.VARCHAR));//15_JUL_2014
    declareParameter(new SqlParameter("p_profile_type", Types.VARCHAR)); //13_Jan_15 
    declareParameter(new SqlParameter("p_screen_width", Types.VARCHAR)); //23_Oct_18
    declareParameter(new SqlParameter("P_LATITUDE", Types.VARCHAR));// March2020 rel - Sangeeth.S: Added latitude and longitde for stats logging
    declareParameter(new SqlParameter("P_LONGITUDE", Types.VARCHAR));// March2020 rel - Sangeeth.S: Added latitude and longitde for stats logging
    declareParameter(new SqlParameter("P_UCAS_TARIFF", OracleTypes.NUMBER));// wu_20200917 - Sri Sankari: Added UCAS tariff point
    declareParameter(new SqlParameter("P_YEAR_OF_ENTRY", OracleTypes.NUMBER));//Added this for YOE logging in d_session by Sujitha V on 2020_NOV_10
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(ProfileVO profileVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    try{
    inMap.put("P_COLLEGE_ID", profileVO.getCollegeId());
    inMap.put("p_profile_id", profileVO.getProfileId()); 
    inMap.put("p_myhc_profile_id", profileVO.getMyhcProfileId());    
    inMap.put("p_session_id", profileVO.getX()); 
    inMap.put("p_user_id", profileVO.getY());    
    inMap.put("p_required_desc", profileVO.getRequestDesc());
    inMap.put("p_section_name", profileVO.getCollegeSection());    
    inMap.put("p_user_ip", profileVO.getClientIp());
    inMap.put("p_user_agent", profileVO.getUserAgent());
    inMap.put("p_request_url", profileVO.getRequestURL());
    inMap.put("p_referer_url", profileVO.getReferralURL());
    inMap.put("p_network_id", profileVO.getNetwordId());
    inMap.put("p_page_name", profileVO.getJourneyType());//24_JUN_2014
    inMap.put("p_track_session", profileVO.getTrackSessionId());
    inMap.put("p_mobile_flag", profileVO.getMobileFlag());//15_JUL_2014
    inMap.put("p_profile_type", profileVO.getRichProfileType());// 13_JAN_15 Added by Amir    
    inMap.put("p_screen_width", profileVO.getScreenWidth()); // 23_Oct_18 
    inMap.put("P_LATITUDE", profileVO.getLatitude());
    inMap.put("P_LONGITUDE", profileVO.getLongitude());
    inMap.put("P_UCAS_TARIFF", profileVO.getUserUcasScore());
    inMap.put("P_YEAR_OF_ENTRY", profileVO.getYearOfEntry());
    outMap = execute(inMap);
    }catch(Exception e){
     e.printStackTrace();
    }
    return outMap;
  }
}//class

