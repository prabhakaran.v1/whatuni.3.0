package com.layer.dao.sp.uni;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.CommonMapper;
import spring.rowmapper.common.SeoMetaDetailsRowMapperImpl;
import spring.valueobject.interaction.CpeInteractionVO;
import WUI.review.bean.ReviewListBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.advert.RichprofSpListRowMapperImpl;
import com.layer.util.rowmapper.review.CommonPhrasesRowMapperImpl;
import com.layer.util.rowmapper.review.ReviewBreakDownRowMapperImpl;
import com.layer.util.rowmapper.review.ReviewSubjectRowMapperImpl;
import com.layer.util.rowmapper.review.UniRankningRowMapperImpl;
import com.layer.util.rowmapper.scholarship.RichProvScholarshipRowMapperImpl;
import com.layer.util.rowmapper.search.UniStatsInfoRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.advert.CollegeProfileVO;
import com.wuni.util.valueobject.advert.NewUniLandingVO;
import com.wuni.util.valueobject.advert.RichProfileVO;
import com.wuni.util.valueobject.search.LocationsInfoVO;
import com.wuni.util.valueobject.search.TopCoursesVO;

/**
  * NewUniLandingSP its going to be common SP for all profile actions
  * @author Amirtharaj
  * @since 3_NOV_15 * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * Nov-24-15      Indumathi.S               1.1      Cost of Pint details                                           547
  * 19_Apr_16      Indumathi.S               1.2      Hero image on clearing page                                    551             
  * May_31_16      Indumathi.S               1.3      Added WUSCA details cursor                                     553
  * 20_10_2020    Sri Sankari R              1.4      Added new cursor and in param for dynamic meta details from BO  wu_20201020
  */
public class NewUniLandingSP extends StoredProcedure {
  public NewUniLandingSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.NEW_UNI_LANDING_PRC);
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SESSION_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_PAGE_NAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_NETWORK_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REQUEST_DESCRIPTION", Types.VARCHAR));
    declareParameter(new SqlParameter("P_CLIENT_IP", Types.VARCHAR));
    declareParameter(new SqlParameter("P_CLIENT_BROWSER", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_SECTION", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REQUEST_URL", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REFEREL_URL", Types.VARCHAR));
    declareParameter(new SqlParameter("P_BASKET_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_TRACK_SESSION", Types.VARCHAR));
    declareParameter(new SqlParameter("P_MYHC_PROFILE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_JOURNEY", Types.VARCHAR));
    declareParameter(new SqlOutParameter("pc_latest_uni_reviews", OracleTypes.CURSOR, new CommonMapper.collegeLatestReviewsMapperImpl()));
    declareParameter(new SqlOutParameter("pc_uni_profile_content", OracleTypes.CURSOR, new collegeProfileContentMapperImpl()));
    declareParameter(new SqlOutParameter("pc_profile_interaction_details", OracleTypes.CURSOR, new ProfileInteractionDetailsRowMapper()));
    declareParameter(new SqlOutParameter("pc_college_unistats_info", OracleTypes.CURSOR, new UniStatsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_scholarship", OracleTypes.CURSOR, new RichProvScholarshipRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_admin_venue", OracleTypes.CURSOR, new LocationsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_uni_info", OracleTypes.CURSOR, new UniInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_uni_raking", OracleTypes.CURSOR, new UniRankningRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_uni_sp_list", OracleTypes.CURSOR, new RichprofSpListRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_unis_u_may_like", OracleTypes.CURSOR, new TopCoursesRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_COST_OF_LIVING", OracleTypes.CURSOR, new CostOfLivingRowMapperImpl()));//Added by Indumathi.S For Cost of Pint Nov-24-15 Rel
    declareParameter(new SqlOutParameter("pc_get_hero_image", OracleTypes.CURSOR, new HeroImageListMapperImpl()));//Added by Indumathi.S For Hero image in clearing page Apr_19_2016 Rel
    declareParameter(new SqlOutParameter("p_log_status_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_type_key", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_course_exist_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_pixel_tracking_code", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_college_in_basket", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_open_days_exist_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_study_abroad_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_uni_town", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_final_choices_exist_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_dept_name", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_adv_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_college_logo", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_ug_content_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_REVIEW_SUBJECT", OracleTypes.CURSOR,new ReviewSubjectRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_REVIEW_BREAKDOWN", OracleTypes.CURSOR,new ReviewBreakDownRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_COMMON_PHRASES", OracleTypes.CURSOR, new CommonPhrasesRowMapperImpl()));
    declareParameter(new SqlOutParameter("P_CL_COURSE_EXIST_FLAG", OracleTypes.VARCHAR));
   // declareParameter(new SqlOutParameter("P_CLEARING_SWITCH_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_URL", OracleTypes.VARCHAR)); // Added in param for dynamic meta details from Back office, 20201020 rel by Sri Sankari
    declareParameter(new SqlOutParameter("PC_META_DETAILS", OracleTypes.CURSOR, new SeoMetaDetailsRowMapperImpl())); // Added new cursor for dynamic meta details from Back office, 20201020 rel by Sri Sankari
    compile();
  }
  public Map execute(NewUniLandingVO newUniLandingVO) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", newUniLandingVO.getUserId());
      inMap.put("P_SESSION_ID", newUniLandingVO.getSessionId());
      inMap.put("P_COLLEGE_ID", newUniLandingVO.getCollegeId());
      inMap.put("P_PAGE_NAME", newUniLandingVO.getPageName());
      inMap.put("P_NETWORK_ID", newUniLandingVO.getNetworkId());
      inMap.put("P_REQUEST_DESCRIPTION", newUniLandingVO.getRequestDesc());
      inMap.put("P_CLIENT_IP", newUniLandingVO.getClientIp());
      inMap.put("P_CLIENT_BROWSER", newUniLandingVO.getUserAgent());
      inMap.put("P_COLLEGE_SECTION", newUniLandingVO.getSectionName());
      inMap.put("P_REQUEST_URL", newUniLandingVO.getRequestURL());
      inMap.put("P_REFEREL_URL", newUniLandingVO.getReferelURL());
      inMap.put("P_BASKET_ID", newUniLandingVO.getBasketId());
      inMap.put("P_TRACK_SESSION", newUniLandingVO.getTrackSessionId());
      inMap.put("P_MYHC_PROFILE_ID", newUniLandingVO.getMyhcProfileId());
      inMap.put("P_USER_JOURNEY", newUniLandingVO.getUserJourney());
      inMap.put("P_URL", newUniLandingVO.getNonAdvPageUrl());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  //
  private class collegeProfileContentMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CollegeProfileVO collegeProfileVO = new CollegeProfileVO();
      collegeProfileVO.setCollegeId(rs.getString("college_id"));
      collegeProfileVO.setCollegeName(rs.getString("college_name"));
      collegeProfileVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      if (rs.getString("image_path") != null && !"".equals(rs.getString("image_path"))) {
        String imagePath[] = rs.getString("image_path").split("###");
        if (imagePath.length == 1) {
          collegeProfileVO.setMediaType("Image");
          collegeProfileVO.setProfileImagePath(new CommonUtil().getImgPath(imagePath[0], rowNum));
        } else if (imagePath.length > 1) {
          collegeProfileVO.setMediaType("Video");
          if (imagePath[0] != null) {
            if ("Y".equalsIgnoreCase(rs.getString("rich_content"))) {
              collegeProfileVO.setProfileImagePath(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(imagePath[0], "7"), rowNum));
            } else {
            collegeProfileVO.setProfileImagePath(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(imagePath[0], "4"), rowNum));
            }
            collegeProfileVO.setVideoId(imagePath[0]);
          }
          if (imagePath[1] != null) {
            collegeProfileVO.setVideoUrl(imagePath[1]);
          }
          if (imagePath[2] != null) {
            String duration[] = imagePath[2].toString().split(":");
            collegeProfileVO.setVideoMetaDuration("T" + duration[0] + "M" + duration[1] + "S");
          }
        }
      }
      collegeProfileVO.setProfileDescription(rs.getString("description"));
      collegeProfileVO.setSelectedSectionName(rs.getString("button_lable"));
      if (!GenericValidator.isBlankOrNull(collegeProfileVO.getSelectedSectionName())) {
        CommonUtil commonUtil = new CommonUtil();
        collegeProfileVO.setSectionName(commonUtil.removeSpecialCharAndConstructSeoText(collegeProfileVO.getSelectedSectionName()));
      }
      collegeProfileVO.setProfileId(rs.getString("profile_id"));
      collegeProfileVO.setMyhcProfileId(rs.getString("myhc_profile_id"));
      //Added by Indumathi.S For showing WUSCA details May_31_2016
      ResultSet profileDetailsRS = (ResultSet)rs.getObject("wusca_details");
      try {
        if (profileDetailsRS != null && profileDetailsRS.next()) {
          collegeProfileVO.setWuscaRank(profileDetailsRS.getString("wusca_rank"));
          collegeProfileVO.setProfileRating(profileDetailsRS.getString("rating"));
          collegeProfileVO.setProfileRoundRating(profileDetailsRS.getString("round_rating"));
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (profileDetailsRS != null) {
            profileDetailsRS.close();
            profileDetailsRS = null;
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      //End May_31_2016
      collegeProfileVO.setSubOrderItemId(rs.getString("suborder_item_id"));
      return collegeProfileVO;
    }
  }
  //
  private class ProfileInteractionDetailsRowMapper implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CpeInteractionVO cpeInteractionVO = new CpeInteractionVO();
      cpeInteractionVO.setOrderItemId(resultset.getString("ORDER_ITEM_ID"));
      cpeInteractionVO.setSubOrderItemId(resultset.getString("SUBORDER_ITEM_ID"));
      cpeInteractionVO.setSubOrderWebsite(resultset.getString("WEBSITE"));
      cpeInteractionVO.setSubOrderEmail(resultset.getString("EMAIL"));
      cpeInteractionVO.setSubOrderEmailWebform(resultset.getString("EMAIL_WEBFORM"));
      cpeInteractionVO.setSubOrderProspectus(resultset.getString("PROSPECTUS"));
      cpeInteractionVO.setSubOrderProspectusWebform(resultset.getString("PROSPECTUS_WEBFORM"));
      cpeInteractionVO.setMyhcProfileId(resultset.getString("MYHC_PROFILE_ID"));
      cpeInteractionVO.setProfileType(resultset.getString("PROFILE_TYPE"));
      cpeInteractionVO.setHotLineNo(resultset.getString("hotline"));
      cpeInteractionVO.setCpeQualificationNetworkId(resultset.getString("network_id"));
      return cpeInteractionVO;
    }
  }
  //
  public class LocationsInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      LocationsInfoVO locationsInfoVO = new LocationsInfoVO();
      //
      locationsInfoVO.setLatitude(resultset.getString("latitude"));
      locationsInfoVO.setLongitude(resultset.getString("longitude"));
      locationsInfoVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      locationsInfoVO.setTown(resultset.getString("town"));
      locationsInfoVO.setPostcode(resultset.getString("postcode"));
      locationsInfoVO.setNearestStationName(resultset.getString("nearest_train_stn"));
      locationsInfoVO.setNearestTubeStation(resultset.getString("nearest_tube_stn"));
      locationsInfoVO.setDistanceFromNearestStation(resultset.getString("distance_from_train_stn"));
      locationsInfoVO.setDistanceFromTubeStation(resultset.getString("distance_from_tube_stn"));
      locationsInfoVO.setAddLine1(resultset.getString("add_line_1"));
      locationsInfoVO.setAddLine2(resultset.getString("add_line_2"));
      locationsInfoVO.setCountryState(resultset.getString("country_state"));
      if(!GenericValidator.isBlankOrNull(resultset.getString("is_in_london"))){
      locationsInfoVO.setIsInLondon(resultset.getString("is_in_london").toUpperCase());
      }
      locationsInfoVO.setCityGuideDisplayFlag(resultset.getString("CITY_GUIDE_DISPLAY_FLAG"));
      locationsInfoVO.setLocation(resultset.getString("location"));
      locationsInfoVO.setCityGuideUrl(new SeoUrls().constructAdviceSeoUrl(resultset.getString("article_category"), resultset.getString("post_url"), resultset.getString("article_id")));
      //
      return locationsInfoVO;
    }
  }
  //
  private class UniInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewListBean reviewBean = new ReviewListBean();
      reviewBean.setCollegeId(rs.getString("college_id"));
      reviewBean.setCollegeName(rs.getString("college_name"));
      reviewBean.setCollegeDisplayName(rs.getString("college_name_display"));
      reviewBean.setReviewCount(rs.getString("review_count"));
      reviewBean.setOverAllRating(rs.getString("overall_rating"));
      reviewBean.setOverallRatingExact(rs.getString("overall_rating_exact"));
      reviewBean.setRichProfileImgPath(rs.getString("rich_hero_image"));
      reviewBean.setPullQuotes(rs.getString("pull_quotes"));
      reviewBean.setAwardImage(rs.getString("award_image_path"));
      reviewBean.setSponsorURL(new SeoUrls().construnctUniHomeURL(reviewBean.getCollegeId(), reviewBean.getCollegeName()));
      reviewBean.setUniReviewsURL(new SeoUrls().constructReviewPageSeoUrl(reviewBean.getCollegeName(), reviewBean.getCollegeId()));
      return reviewBean;
    }
  }
  //
  public class TopCoursesRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      TopCoursesVO topCoursesVO = new TopCoursesVO();
      SeoUrls seoUrl = new SeoUrls();
      topCoursesVO.setCollegeId(resultset.getString("college_id"));
      topCoursesVO.setCollegeName(resultset.getString("college_name"));
      topCoursesVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      topCoursesVO.setCourseId(resultset.getString("course_id"));
      topCoursesVO.setCourseTitle(resultset.getString("course_title"));
      topCoursesVO.setStudyLevelDesc(resultset.getString("study_level_desc"));
      topCoursesVO.setCourseDetailsPageURL(seoUrl.construnctCourseDetailsPageURL(topCoursesVO.getStudyLevelDesc(), topCoursesVO.getCourseTitle(), topCoursesVO.getCourseId(), topCoursesVO.getCollegeId(), resultset.getString("p_page_name")));
      topCoursesVO.setCollegeLandingUrl(new SeoUrls().construnctUniHomeURL(topCoursesVO.getCollegeId(), topCoursesVO.getCollegeName()));
      topCoursesVO.setCount(resultset.getString("cnt"));
      topCoursesVO.setWebsitePrice(resultset.getString("website_price"));
      topCoursesVO.setWebformPrice(resultset.getString("webform_price"));
      topCoursesVO.setShortListFlag(resultset.getString("course_in_basket"));
      topCoursesVO.setGaCollegeName(new CommonFunction().replaceSpecialCharacter(topCoursesVO.getCollegeName()));
      ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("enquiry_details");
      try {
        if (enquiryDetailsRS != null) {
          while (enquiryDetailsRS.next()) {
            String tmpUrl = null;
            topCoursesVO.setOrderItemId(enquiryDetailsRS.getString("order_item_id"));
            topCoursesVO.setMyhcProfileId(enquiryDetailsRS.getString("myhc_profile_id"));
            topCoursesVO.setSubOrderItemId(enquiryDetailsRS.getString("suborder_item_id"));
            topCoursesVO.setProfileType(enquiryDetailsRS.getString("profile_type"));
            topCoursesVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("network_id"));
            topCoursesVO.setAdvertName(enquiryDetailsRS.getString("advert_name"));
            topCoursesVO.setSubOrderEmail(enquiryDetailsRS.getString("email"));
            topCoursesVO.setSubOrderProspectus(enquiryDetailsRS.getString("prospectus"));
            tmpUrl = enquiryDetailsRS.getString("website");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            topCoursesVO.setSubOrderWebsite(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("email_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            topCoursesVO.setSubOrderEmailWebform(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("prospectus_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            topCoursesVO.setSubOrderProspectusWebform(tmpUrl);
            topCoursesVO.setMediaPath(enquiryDetailsRS.getString("media_path"));
            String splitMediaThumbPath = "";
            topCoursesVO.setMediaThumbPath(enquiryDetailsRS.getString("thumb_media_path"));
            if (!GenericValidator.isBlankOrNull(topCoursesVO.getMediaThumbPath())) {
              splitMediaThumbPath = topCoursesVO.getMediaThumbPath().substring(0, topCoursesVO.getMediaThumbPath().lastIndexOf("."));
              topCoursesVO.setMediaThumbPath(new CommonUtil().getImgPath((splitMediaThumbPath + ".jpg"), rowNum));
            } else {
              if (!GenericValidator.isBlankOrNull(topCoursesVO.getMediaPath())) {
                if (enquiryDetailsRS.getString("media_type_id") != null && "16".equals(enquiryDetailsRS.getString("media_type_id"))) {
                  topCoursesVO.setMediaThumbPath(new CommonUtil().getImgPath((topCoursesVO.getMediaPath().replace(".", "_140px.")), rowNum));
                } else {
                  topCoursesVO.setMediaThumbPath(new CommonUtil().getImgPath((topCoursesVO.getMediaPath().replace(".", "_116px.")), rowNum));
                }
              }
            }
            topCoursesVO.setMediaType(enquiryDetailsRS.getString("media_type"));
            topCoursesVO.setMediaId(enquiryDetailsRS.getString("media_id"));
            if ("VIDEO".equalsIgnoreCase(topCoursesVO.getMediaType())) {
              String thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(topCoursesVO.getMediaId(), "2"), rowNum);
              topCoursesVO.setVideoThumbPath(thumbnailPath);
            }
            topCoursesVO.setHotLineNo(enquiryDetailsRS.getString("hotline"));
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          enquiryDetailsRS.close();
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      if ("CLEARING_COURSE".equalsIgnoreCase(resultset.getString("p_page_name")) || "CLEARING".equalsIgnoreCase(resultset.getString("p_page_name"))) {
        topCoursesVO.setUniHomeURL((seoUrl.construnctUniHomeURL(topCoursesVO.getCollegeId(), topCoursesVO.getCollegeName()).toLowerCase()) + "?clearing");
      } else {
        topCoursesVO.setUniHomeURL(seoUrl.construnctUniHomeURL(topCoursesVO.getCollegeId(), topCoursesVO.getCollegeName()).toLowerCase());
      }
      return topCoursesVO;
    }
  }
  
  public class CostOfLivingRowMapperImpl implements RowMapper{
      public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
        RichProfileVO richProfileVO = new RichProfileVO();
        richProfileVO.setCollegeId(resultset.getString("college_id"));
        richProfileVO.setAccommodationLower(resultset.getString("accommodation_lower"));
        richProfileVO.setAccommodationUpper(resultset.getString("accommodation_upper"));
        richProfileVO.setLivingCost(resultset.getString("living_cost"));        
        richProfileVO.setLivingBarLimit(resultset.getString("living_bar_limit"));
        richProfileVO.setAccommodationLowerBarLimit(resultset.getString("accom_lower_bar_limit"));
        richProfileVO.setAccommodationUpperBarLimit(resultset.getString("accom_upper_bar_limit"));
        richProfileVO.setWhatuniCostPint(resultset.getString("whatuni_cost_pint"));
        richProfileVO.setMyhcAccommodationToolTipFlag(resultset.getString("myhc_accom_tool_tip_flag")); //added AccommodationToolTip for 24_Jana_2017 by Thiyagu G.
        return richProfileVO;
      }
    }
    
  public class HeroImageListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      RichProfileVO richProfileVO = new RichProfileVO();//
      richProfileVO.setMediaPath(rs.getString("image_path"));
      richProfileVO.setMediaId(rs.getString("media_id"));
      richProfileVO.setMediaType(rs.getString("media_type_id"));
      return richProfileVO;
    }
  }
  //
}
