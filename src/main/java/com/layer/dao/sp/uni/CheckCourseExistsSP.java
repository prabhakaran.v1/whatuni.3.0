package com.layer.dao.sp.uni;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.advert.NewUniLandingVO;

import oracle.jdbc.OracleTypes;

 /**
  * To get the profile TAB related details
  * @author Indumathi.S
  * @since 31_May_2016
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * May_31_2016    Indumathi.S               1.0       First Draft                                                 553
  */
public class CheckCourseExistsSP extends StoredProcedure {

  public CheckCourseExistsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.GET_PROFILE_TAB_DETAILS);
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.NUMERIC));
    declareParameter(new SqlOutParameter("pc_profile_network_type", OracleTypes.CURSOR, new StudyModeDetailsRowMapperImpl()));
  }

  public Map execute(NewUniLandingVO newUniLandingVO) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_COLLEGE_ID", newUniLandingVO.getCollegeId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
public class StudyModeDetailsRowMapperImpl implements RowMapper {

	    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
	    	NewUniLandingVO newUniLandingVO = new NewUniLandingVO();
     	    newUniLandingVO.setStudyMode(resultset.getString("profile_network_type"));
			return newUniLandingVO;    
	    }
	  }
}
