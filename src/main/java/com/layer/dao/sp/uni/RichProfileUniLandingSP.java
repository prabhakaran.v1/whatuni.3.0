package com.layer.dao.sp.uni;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.form.VideoReviewListBean;
import spring.rowmapper.common.SeoMetaDetailsRowMapperImpl;
import spring.rowmapper.seo.PageMetaTagsRowMapperImpl;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalFunction;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.advert.CollegeProfileTabsRowMapper;
import com.layer.util.rowmapper.advert.EnquiryInfoRowMapperImpl;
import com.layer.util.rowmapper.advert.RichprofSpListRowMapperImpl;
import com.layer.util.rowmapper.openday.RichProvOpenDayInfoRowMapperImpl;
import com.layer.util.rowmapper.review.CommonPhrasesRowMapperImpl;
import com.layer.util.rowmapper.review.ReviewBreakDownRowMapperImpl;
import com.layer.util.rowmapper.review.ReviewSubjectRowMapperImpl;
import com.layer.util.rowmapper.review.RichProfileReviewRowMapperImpl;
import com.layer.util.rowmapper.review.UniOverallReviewRowMapperImpl;
import com.layer.util.rowmapper.review.UniRankningRowMapperImpl;
import com.layer.util.rowmapper.review.WuscaRatingRowMapperImpl;
import com.layer.util.rowmapper.search.UniStatsInfoRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.advert.CollegeProfileVO;
import com.wuni.util.valueobject.advert.RichProfileVO;
import com.wuni.util.valueobject.search.LocationsInfoVO;

/**
  * Rich profile Landing page Data for both IP and SP
  * @author Amirtharaj
  * @since 13_JAN_15
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * Nov-24-15      Indumathi.S               1.1      Cost of Pint details                                           547
 	* Jan-27-16      Prabhakaran V.            1.2      Added WUSCA Rating cursor                                      548
  * May_31_16      Indumathi.S               1.3      Added WUSCA details inner cursor                               553
  * May-31-16      Prabhakaran V.            1.4      Added hide location param..                                    553
  * Jul-11-17      Prabhakaran V.            1.5      Remove unused cursors..                                        567    
  * Nov-20-18      Sangeeth.S                1.6      Added open day start and end date in schema tag for
  *                                                         time stamp format in  RichProvOpenDayInfoRowMapperImpl   583
  * Dec-18_18      Hema.S                    1.7      Added Review subject and breakdown cursor                      584
  * Feb-28-19      Sangeeth.S                1.8      Added in and out param to handle cmmt
  * Apr-22-20	   Sangeeth.S       		 1.9      Added out column for event type
  * 20_10_2020    Sri Sankari R              1.10     Added new cursor and in param for dynamic meta details from BO  wu_20201020
  */
public class RichProfileUniLandingSP extends StoredProcedure {
  public RichProfileUniLandingSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.RICH_PROFILE_PRC);
    declareParameter(new SqlParameter("P_SESSION_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_BASKET_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_NETWORK_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REQUEST_DESCRIPTION", Types.VARCHAR));
    declareParameter(new SqlParameter("P_CLIENT_IP", Types.VARCHAR));
    declareParameter(new SqlParameter("P_CLIENT_BROWSER", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_SECTION", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REQUEST_URL", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REFEREL_URL", Types.VARCHAR));
    declareParameter(new SqlParameter("P_META_PAGE_NAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_META_PAGE_FLAG", Types.VARCHAR));
    declareParameter(new SqlParameter("P_TRACK_SESSION", Types.VARCHAR));
    declareParameter(new SqlParameter("P_PROFILE_TYPE", Types.VARCHAR));
    declareParameter(new SqlParameter("P_MYHC_PROFILE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_JOURNEY", Types.VARCHAR));
    declareParameter(new SqlOutParameter("oc_college_unistats_info", OracleTypes.CURSOR, new UniStatsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_uni_sp_list", OracleTypes.CURSOR, new RichprofSpListRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_open_day_info", OracleTypes.CURSOR, new RichProvOpenDayInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_latest_uni_reviews", OracleTypes.CURSOR, new RichProfileReviewRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_enquiry_info", OracleTypes.CURSOR, new EnquiryInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_uni_profile_content", OracleTypes.CURSOR, new CollegeProfileContentRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_uni_profile_tabs", OracleTypes.CURSOR, new CollegeProfileTabsRowMapper()));
    //declareParameter(new SqlOutParameter("oc_scholarship", OracleTypes.CURSOR, new RichProvScholarshipRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_admin_venue", OracleTypes.CURSOR, new LocationsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_page_meta_tags", OracleTypes.CURSOR, new PageMetaTagsRowMapperImpl()));
    //declareParameter(new SqlOutParameter("oc_college_names", OracleTypes.CURSOR, new CollegeNamesRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_overall_review", OracleTypes.CURSOR, new UniOverallReviewRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_wusca_win_nom_logos", OracleTypes.CURSOR, new WuscaWinnersLogoListMapperImpl()));
    declareParameter(new SqlOutParameter("oc_rich_profile_media", OracleTypes.CURSOR, new RichProfileMediaListMapperImpl()));
    declareParameter(new SqlOutParameter("oc_uni_raking", OracleTypes.CURSOR, new UniRankningRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_social_network_urls", OracleTypes.CURSOR, new SocialNetworkRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_COST_OF_LIVING", OracleTypes.CURSOR, new CostOfLivingRowMapperImpl()));//Added by Indumathi.S For Cost of Pint Nov-24-15 Rel
    declareParameter(new SqlOutParameter("oc_WUSCA_ratings", OracleTypes.CURSOR, new WuscaRatingRowMapperImpl())); //Added by Prabha for WUSCA Rating 27_JAN_2016_REL
    declareParameter(new SqlOutParameter("o_avg_rating", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_log_status_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_college_in_basket", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_college_ucas_code", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_type_key", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_profile_tab_list", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_course_exist_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_study_abroad_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_final_choices_exist_flag", OracleTypes.VARCHAR)); //Added fby Priyaa for final 5 - 21-Jul-2015
    declareParameter(new SqlOutParameter("p_dept_name", OracleTypes.VARCHAR)); //Added by Amir for department name 30_SEP_15
    declareParameter(new SqlOutParameter("p_hide_locaion_flag", OracleTypes.VARCHAR)); //Added by Prabha for hide location 31_MAY_16
    declareParameter(new SqlOutParameter("PC_REVIEW_SUBJECT", OracleTypes.CURSOR, new ReviewSubjectRowMapperImpl()));//Added this for review redesign by Hema.S on 18_DEC_2018_REL
    declareParameter(new SqlOutParameter("PC_REVIEW_BREAKDOWN", OracleTypes.CURSOR, new ReviewBreakDownRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_COMMON_PHRASES", OracleTypes.CURSOR, new CommonPhrasesRowMapperImpl()));
    declareParameter(new SqlOutParameter("P_CL_COURSE_EXIST_FLAG", OracleTypes.VARCHAR));
    //declareParameter(new SqlOutParameter("P_CLEARING_SWITCH_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_URL", OracleTypes.VARCHAR)); // Added in param for dynamic meta details from Back office, 20201020 rel by Sri Sankari
    declareParameter(new SqlOutParameter("PC_META_DETAILS", OracleTypes.CURSOR, new SeoMetaDetailsRowMapperImpl())); // Added new cursor for dynamic meta details from Back office, 20201020 rel by Sri Sankari
    compile();
  }
  public Map execute(RichProfileVO richProfileVO) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_SESSION_ID", richProfileVO.getSessionId());
      inMap.put("P_USER_ID", richProfileVO.getUserId());
      inMap.put("P_BASKET_ID", richProfileVO.getBasketId());
      inMap.put("P_COLLEGE_ID", richProfileVO.getCollegeId());
      inMap.put("P_NETWORK_ID", richProfileVO.getNetworkId());
      inMap.put("P_REQUEST_DESCRIPTION", richProfileVO.getRequestDesc());
      inMap.put("P_CLIENT_IP", richProfileVO.getClientIp());
      inMap.put("P_CLIENT_BROWSER", richProfileVO.getUserAgent());
      inMap.put("P_COLLEGE_SECTION", richProfileVO.getSectionName());
      inMap.put("P_REQUEST_URL", richProfileVO.getRequestURL());
      inMap.put("P_REFEREL_URL", richProfileVO.getReferelURL());
      inMap.put("P_META_PAGE_NAME", richProfileVO.getMetaPageName());
      inMap.put("P_META_PAGE_FLAG", richProfileVO.getMetaPageFlag());
      inMap.put("P_TRACK_SESSION", richProfileVO.getTrackSessionId());
      inMap.put("P_PROFILE_TYPE", richProfileVO.getProfileType());
      inMap.put("P_MYHC_PROFILE_ID", richProfileVO.getMyhcProfileId());
      inMap.put("P_USER_JOURNEY", richProfileVO.getUserJourney());
      inMap.put("P_URL", richProfileVO.getRichOrSubPageUrl());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  public class CollegeProfileContentRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CollegeProfileVO collegeProfileVO = new CollegeProfileVO();
      collegeProfileVO.setCollegeId(rs.getString("college_id"));
      collegeProfileVO.setCollegeName(rs.getString("college_name"));
      collegeProfileVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      if (rs.getString("image_path") != null && !"".equals(rs.getString("image_path"))) {
        String imagePath[] = rs.getString("image_path").split("###");
        if (imagePath.length == 1) {
          collegeProfileVO.setMediaType("Image");
          collegeProfileVO.setProfileImagePath(new CommonUtil().getImgPath(imagePath[0], rowNum));
        } else if (imagePath.length > 1) {
          collegeProfileVO.setMediaType("Video");
          if (imagePath[0] != null) {
            collegeProfileVO.setProfileImagePath(new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(imagePath[0], "7"), rowNum));
            collegeProfileVO.setVideoId(imagePath[0]);
          }
          if (imagePath[1] != null) {
            collegeProfileVO.setVideoUrl(imagePath[1]);
          }
          if (imagePath[2] != null) {
            String duration[] = imagePath[2].toString().split(":");
            collegeProfileVO.setVideoMetaDuration("T" + duration[0] + "M" + duration[1] + "S");
          }
        }
      }
      collegeProfileVO.setProfileDescription(rs.getString("description"));
      collegeProfileVO.setSelectedSectionName(rs.getString("button_lable"));
      collegeProfileVO.setOriginalSectionName(rs.getString("original_section_label"));
      if (!GenericValidator.isBlankOrNull(collegeProfileVO.getSelectedSectionName())) {
        CommonUtil commonUtil = new CommonUtil();
        collegeProfileVO.setSectionName(commonUtil.removeSpecialCharAndConstructSeoText(collegeProfileVO.getSelectedSectionName()));
      }
      collegeProfileVO.setProfileId(rs.getString("profile_id"));
      collegeProfileVO.setMyhcProfileId(rs.getString("myhc_profile_id"));
      //Added by Indumathi.S For showing wusca rank and ratings May_31_2016
      ResultSet profileDetailsRS = (ResultSet)rs.getObject("wusca_details");
      try {
        if (profileDetailsRS != null && profileDetailsRS.next()) {
          collegeProfileVO.setWuscaRank(profileDetailsRS.getString("wusca_rank"));
          collegeProfileVO.setProfileRating(profileDetailsRS.getString("rating"));
          collegeProfileVO.setProfileRoundRating(profileDetailsRS.getString("round_rating"));
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (profileDetailsRS != null) {
            profileDetailsRS.close();
            profileDetailsRS = null;
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      //End
      collegeProfileVO.setSubOrderItemId(rs.getString("suborder_item_id"));
      return collegeProfileVO;
    }
  }
  //
  public class LocationsInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      LocationsInfoVO locationsInfoVO = new LocationsInfoVO();
      //
      locationsInfoVO.setLatitude(resultset.getString("latitude"));
      locationsInfoVO.setLongitude(resultset.getString("longitude"));
      locationsInfoVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      locationsInfoVO.setTown(resultset.getString("town"));
      locationsInfoVO.setPostcode(resultset.getString("postcode"));
      locationsInfoVO.setNearestStationName(resultset.getString("nearest_train_stn"));
      locationsInfoVO.setNearestTubeStation(resultset.getString("nearest_tube_stn"));
      locationsInfoVO.setDistanceFromNearestStation(resultset.getString("distance_from_train_stn"));
      locationsInfoVO.setDistanceFromTubeStation(resultset.getString("distance_from_tube_stn"));
      locationsInfoVO.setAddLine1(resultset.getString("add_line_1"));
      locationsInfoVO.setAddLine2(resultset.getString("add_line_2"));
      locationsInfoVO.setCountryState(resultset.getString("country_state"));
      if(!GenericValidator.isBlankOrNull(resultset.getString("is_in_london"))){
      locationsInfoVO.setIsInLondon(resultset.getString("is_in_london").toUpperCase());
      }
      locationsInfoVO.setCityGuideDisplayFlag(resultset.getString("CITY_GUIDE_DISPLAY_FLAG"));
      locationsInfoVO.setLocation(resultset.getString("location"));
      locationsInfoVO.setCityGuideUrl(new SeoUrls().constructAdviceSeoUrl(resultset.getString("article_category"), resultset.getString("post_url"), resultset.getString("article_id")));
      //
      return locationsInfoVO;
    }
  }
  //
  public class WuscaWinnersLogoListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      VideoReviewListBean bean = new VideoReviewListBean();
      bean.setPosition(rs.getString("POSITION"));
      bean.setPositionId(rs.getString("POSITION_ID"));
      bean.setReviewQuestionId(rs.getString("REVIEW_QUESTION_ID"));
      if (rs.getString("AWARD_IMAGE") != null && !"".equals(rs.getString("AWARD_IMAGE"))) {
        bean.setAwardImage(new CommonUtil().getImgPath(rs.getString("AWARD_IMAGE"), rowNum));
      }
      return bean;
    }
  }
  public class RichProfileMediaListMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      RichProfileVO richProfileVO = new RichProfileVO();
      richProfileVO.setMediaType(rs.getString("MEDIA_TYPE"));
      richProfileVO.setMediaPath(rs.getString("MEDIA_PATH"));
      return richProfileVO;
    }
  }
  //
  //
  public class SocialNetworkRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      RichProfileVO richProfileVO = new RichProfileVO();
      richProfileVO.setSocialNetworkType(resultset.getString("social_network_name"));
      richProfileVO.setSoicalNetworkURL(resultset.getString("social_network_url"));
      return richProfileVO;
    }
  }
  //
  public class CostOfLivingRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      RichProfileVO richProfileVO = new RichProfileVO();
      richProfileVO.setCollegeId(resultset.getString("college_id"));
      richProfileVO.setAccommodationLower(resultset.getString("accommodation_lower"));
      richProfileVO.setAccommodationUpper(resultset.getString("accommodation_upper"));
      richProfileVO.setLivingCost(resultset.getString("living_cost"));      
      richProfileVO.setLivingBarLimit(resultset.getString("living_bar_limit"));
      richProfileVO.setAccommodationLowerBarLimit(resultset.getString("accom_lower_bar_limit"));
      richProfileVO.setAccommodationUpperBarLimit(resultset.getString("accom_upper_bar_limit"));
      richProfileVO.setWhatuniCostPint(resultset.getString("whatuni_cost_pint"));
      richProfileVO.setWhatuniCostPint(resultset.getString("whatuni_cost_pint"));
      richProfileVO.setMyhcAccommodationToolTipFlag(resultset.getString("myhc_accom_tool_tip_flag")); //added AccommodationToolTip for 24_Jana_2017 by Thiyagu G.
      return richProfileVO;
    }
  }
}
