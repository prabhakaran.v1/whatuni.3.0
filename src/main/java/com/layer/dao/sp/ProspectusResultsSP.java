package com.layer.dao.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalFunction;

import com.wuni.util.valueobject.advert.ProspectusVO;

public class ProspectusResultsSP extends StoredProcedure {
  public ProspectusResultsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("wuni_prospectus_pkg.get_prospectus_prc");
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_basket_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_enq_basket_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_qual_code", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_cat_code", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_region_name", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_carousel_type", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_sort_by", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_page_name", OracleTypes.VARCHAR));    
    declareParameter(new SqlParameter("p_page_no", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("oc_prospectus_sr_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_subject_ranking_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_recommend_uni_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_other_students_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_uni_near_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_times_ranking_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_student_awards_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_popular_regions_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_campus_uni_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_non_campus_uni_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_city_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_countryside_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_seaside_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_town_pros_list", OracleTypes.CURSOR, new prospectusSliderRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_enq_basket_count", OracleTypes.CURSOR, new enqBasketRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_user_prospectus", OracleTypes.CURSOR, new userProspectusRowMapperImpl()));
    compile();
  }
  //  
  public Map execute(ProspectusVO prospectusVO) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_user_id", prospectusVO.getUserId() );
      inputMap.put("p_basket_id", prospectusVO.getBasketId());
      inputMap.put("p_enq_basket_id", prospectusVO.getEnqBasketId());
      inputMap.put("p_qual_code", prospectusVO.getQualification() );
      inputMap.put("p_cat_code", prospectusVO.getCategoryCode());
      inputMap.put("p_college_id", prospectusVO.getCollegeId());
      inputMap.put("p_region_name", prospectusVO.getRegionName() );
      inputMap.put("p_carousel_type", prospectusVO.getType());
      inputMap.put("p_sort_by", prospectusVO.getSortBy());
      inputMap.put("p_page_name", prospectusVO.getPageName());
      inputMap.put("p_page_no", prospectusVO.getPageNo());      
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
  private class prospectusSliderRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      ProspectusVO prospectusVO = new ProspectusVO();
      prospectusVO.setCollegeId(resultSet.getString("college_id"));
      prospectusVO.setCollegeName(resultSet.getString("college_name"));
      prospectusVO.setCollegeDisplayName(resultSet.getString("college_name_display"));
      prospectusVO.setGaCollegeName(new GlobalFunction().getReplacedString(resultSet.getString("college_name_display")));
      prospectusVO.setSubOrderItemId(resultSet.getString("suborder_item_id"));
      prospectusVO.setOrderItemId(resultSet.getString("order_item_id"));
      prospectusVO.setOverallRating(resultSet.getString("overall_rating_exact"));
      prospectusVO.setOverAllRatingRounded(resultSet.getString("overall_rating"));
      prospectusVO.setTimesRanking(resultSet.getString("times_ranking"));
      prospectusVO.setCollegeLogoPath(resultSet.getString("college_logo_path"));
      prospectusVO.setSubjectTimesRanking(resultSet.getString("subject_times_ranking"));
      prospectusVO.setEmailPrice(resultSet.getString("email_price"));
      prospectusVO.setRegionName(resultSet.getString("region_name"));
      prospectusVO.setCarouselType(resultSet.getString("carousel_type"));   
      prospectusVO.setCollegeInBasket(resultSet.getString("college_in_enq_basket"));
      prospectusVO.setDisplayName(resultSet.getString("display_name"));
      prospectusVO.setOpenDays(resultSet.getString("open_days"));
      prospectusVO.setOpenDaysUrl(resultSet.getString("open_day_url"));
      prospectusVO.setTotalCount(resultSet.getString("total_count"));      
      return prospectusVO;
    }
  }
  
  private class enqBasketRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      ProspectusVO prospectusVO = new ProspectusVO();
      prospectusVO.setEnqBasketId(resultSet.getString("enq_basket_id"));
      prospectusVO.setTotalCount(resultSet.getString("total_count"));
      return prospectusVO;
    }
  }
  
  public class userProspectusRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {      
      ProspectusVO prospectusVO = new ProspectusVO();
      prospectusVO.setCollegeName(resultset.getString("college_name"));
      prospectusVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      prospectusVO.setFinalChoiceExistsFlag(resultset.getString("final_choice_exists_flag"));      
      prospectusVO.setCollegeId(resultset.getString("college_id"));
      prospectusVO.setCourseId(resultset.getString("course_id"));
      prospectusVO.setDateSent(resultset.getString("date_sent"));      
      prospectusVO.setCourseTitle(resultset.getString("course_title"));
      prospectusVO.setTimesRanking(resultset.getString("course_deleted_flag"));
      prospectusVO.setStudyLevelDesc(resultset.getString("study_level_desc"));
      prospectusVO.setCollegeLogoPath(resultset.getString("college_logo_path"));
      prospectusVO.setOverAllRatingRounded(resultset.getString("overall_rating"));
      prospectusVO.setOverallRating(resultset.getString("overall_rating_exact"));
      prospectusVO.setOpenDays(resultset.getString("open_days"));
      prospectusVO.setOpenDaysUrl(resultset.getString("open_day_url"));
      prospectusVO.setTotalCount(resultset.getString("total_cnt"));
      prospectusVO.setDetailUserName(resultset.getString("user_name"));
      return prospectusVO;
    }
  }
}
