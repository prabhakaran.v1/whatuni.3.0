package com.layer.dao.sp.ucascalculator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

/**
   * @UCASCalculatorScoreAjaxSP
   * @author Thiyagu G
   * @version 1.0
   * @since 16-Feb-2015
   * @purpose  This program is used call the database procedure to calculate the UCAS points.
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.       Changes desc                                      Rel Ver.
   * *************************************************************************************************************************
   * 16-Feb-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
   */
public class UCASCalculatorScoreAjaxSP extends StoredProcedure {
  public UCASCalculatorScoreAjaxSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.CALCULATE_UCAS_POINTS_FN);
    declareParameter(new SqlOutParameter("p_ucas_points", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_year_of_entry", OracleTypes.VARCHAR));    
    declareParameter(new SqlParameter("p_qual_grade_string1", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_qual_grade_string2", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_qual_grade_string3", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_max_ucas_points", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_ucas_percentage", OracleTypes.VARCHAR));
    compile();
    
  }

  /**
    * This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List parameters) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_year_of_entry", parameters.get(0));
      inMap.put("p_qual_grade_string1", parameters.get(1));
      inMap.put("p_qual_grade_string2", parameters.get(2));
      inMap.put("p_qual_grade_string3", parameters.get(3));
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  
}