package com.layer.dao.sp.ucascalculator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.ucascalculator.bean.UcasCalcBean;

/**
   * @UCASCalcTariffTableAjaxSP
   * @author Thiyagu G
   * @version 1.0
   * @since 16-Feb-2015
   * @purpose  This program is used call the database procedure to build the UCAS tariff table.
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.       Changes desc                                      Rel Ver.
   * *************************************************************************************************************************
   * 16-Feb-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
   */
public class UCASCalcTariffTableAjaxSP extends StoredProcedure {
  public UCASCalcTariffTableAjaxSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.CALCULATE_TARIFF_POINTS_TABLE_FN);
    declareParameter(new SqlOutParameter("pc_get_tariff_table", OracleTypes.CURSOR, new TariffTableRowMapperImpl()));
    declareParameter(new SqlParameter("p_year_of_entry", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_qual_id", OracleTypes.NUMERIC));
    compile();
    
  }

  /**
    * This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List parameters) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_year_of_entry", parameters.get(0));
      inMap.put("p_qual_id", parameters.get(1));      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  
  private class TariffTableRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      UcasCalcBean ucasBean = new UcasCalcBean();
      ucasBean.setGrade(rs.getString("grade"));
      ucasBean.setTariffPoints(rs.getString("tariff_points"));
      return ucasBean;
    }
  }
  
}