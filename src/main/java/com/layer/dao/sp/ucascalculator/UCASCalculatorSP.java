package com.layer.dao.sp.ucascalculator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.layer.util.rowmapper.widget.UltimateSearchResultsRowMapperImpl;
import com.layer.util.rowmapper.widget.UltimateSearchResultswcidRowMapperImpl;
import com.wuni.ultimatesearch.UltimateSearchInputDetailsRowmapper;
import com.wuni.util.valueobject.coursesearch.CourseSearchVO;

/**
   * @UCASCalculatorSP
   * @author Thiyagu G
   * @version 1.0
   * @since 21.07.2015
   * @purpose  This program is used call the database procedure to build the UCAS calculator details page.
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.       Changes desc                                      Rel Ver.
   * *************************************************************************************************************************
   * 16-Feb-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
   */
public class UCASCalculatorSP extends StoredProcedure {
  public UCASCalculatorSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.MY_COURSE_SEARCH_PRC);
    declareParameter(new SqlParameter("p_track_session_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMERIC));
    declareParameter(new SqlParameter("p_ajax_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_qual", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_clearing", OracleTypes.VARCHAR));
    
    declareParameter(new SqlOutParameter("p_breadcrumb", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("p_iwtb_return_code", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("p_iwtb_remaining_percent", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("p_wcid_return_code", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("p_wcid_remaining_percent", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("p_wcid_message_code", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("p_wcid_results_grades", OracleTypes.VARCHAR));
    
    declareParameter(new SqlOutParameter("pc_url_params", OracleTypes.CURSOR, new CourseSearchURLParamsRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_iwtb_results", OracleTypes.CURSOR, new UltimateSearchResultsRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_wcid_input_details", OracleTypes.CURSOR, new UltimateSearchInputDetailsRowmapper()));
    declareParameter(new SqlOutParameter("pc_wcid_results", OracleTypes.CURSOR, new UltimateSearchResultswcidRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_get_subject_guides", OracleTypes.CURSOR, new CourseSearchSubjectGuidesRowMapperImpl()));  
    
    declareParameter(new SqlOutParameter("popular_subjects_cnt", OracleTypes.VARCHAR));//Added by Indumathi.S Nov-03-15 Rel
  }

  /**
    * This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List parameters) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_track_session_id", parameters.get(0));
      inMap.put("p_user_id", parameters.get(1));
      inMap.put("p_ajax_flag", parameters.get(2));
      inMap.put("p_qual", parameters.get(3));
      inMap.put("p_clearing", parameters.get(4));      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  
  public class CourseSearchURLParamsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {      
      CourseSearchVO courseSearchVO = new CourseSearchVO();
      try{
        courseSearchVO.setTrackUserActionId(rs.getString("track_user_action_id"));
        courseSearchVO.setActionDate(rs.getString("action_date"));
        courseSearchVO.setActionDetails(rs.getString("action_details"));
        courseSearchVO.setMainSearch(rs.getString("main_search"));
        courseSearchVO.setLocation(rs.getString("location"));
        courseSearchVO.setChildCatId(rs.getString("child_cat_id"));
        courseSearchVO.setChildDesc(rs.getString("child_desc"));
        courseSearchVO.setChildCatCode(rs.getString("child_cat_code"));      
        courseSearchVO.setParentCatId(rs.getString("parent_cat_id"));
        courseSearchVO.setParentDesc(rs.getString("parent_desc"));
        courseSearchVO.setParentCatCode(rs.getString("parent_cat_code"));
        courseSearchVO.setKeyword(rs.getString("keyword"));
        courseSearchVO.setPageNo(rs.getString("page_no"));
        courseSearchVO.setCollegeId(rs.getString("college_id"));
        courseSearchVO.setCollegeName(rs.getString("college_name"));
        courseSearchVO.setCollegeDisplayName(rs.getString("college_display_name"));
        courseSearchVO.setGrade(rs.getString("grade"));
        courseSearchVO.setGradeValue(rs.getString("grade_value"));
        courseSearchVO.setFilter(rs.getString("filter"));
        courseSearchVO.setStudyMode(rs.getString("study_mode"));
        courseSearchVO.setPostcode(rs.getString("postcode"));
        courseSearchVO.setDistance(rs.getString("distance"));
        courseSearchVO.setModuleSearch(rs.getString("module_search"));
        courseSearchVO.setClearing(rs.getString("clearing"));
        courseSearchVO.setCampusType(rs.getString("campus_type"));
        courseSearchVO.setLocationTypeStr(rs.getString("location_type_str"));
        courseSearchVO.setUcasCodeSrch(rs.getString("ucas_code_srch"));
        courseSearchVO.setRussellGroup(rs.getString("russell_group"));
        courseSearchVO.setEmploymentRate(rs.getString("employment_rate"));
        courseSearchVO.setTariffRate(rs.getString("tariff_rate"));
        courseSearchVO.setQualLevel(rs.getString("qual_level"));
        courseSearchVO.setExtraUrl(rs.getString("extra_url"));
        courseSearchVO.setUrl(rs.getString("url"));
        courseSearchVO.setImagePath(rs.getString("image_path"));
        if (!GenericValidator.isBlankOrNull(courseSearchVO.getImagePath())) {
          courseSearchVO.setImageName(new CommonUtil().getImgPath((courseSearchVO.getImagePath()), rowNum));
        }
        courseSearchVO.setTotalCount(rs.getString("total_count"));
      } catch (Exception e) {
        e.printStackTrace();
      }
      return courseSearchVO;
    }
  }
  
  public class CourseSearchSubjectGuidesRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {      
      CourseSearchVO courseSearchVO = new CourseSearchVO();
      try{
        courseSearchVO.setDescription(rs.getString("description"));
        courseSearchVO.setSubjectGuideUrl(rs.getString("url"));
        courseSearchVO.setReadCount(rs.getString("read_count"));
        courseSearchVO.setReadCountPercent(rs.getString("read_count_percent"));
        courseSearchVO.setTotalCount(rs.getString("total_count"));
      } catch (Exception e) {
        e.printStackTrace();
      }
      return courseSearchVO;
    }
  }
  
}