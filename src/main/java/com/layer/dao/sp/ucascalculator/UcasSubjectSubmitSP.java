package com.layer.dao.sp.ucascalculator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.wuni.ucascalculator.bean.UcasCalcBean;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.ucascalculator.UcasBestMatchCoursesVO;

/**
   * @UcasSubjectSubmitSP
   * @author Thiyagu G
   * @version 1.0
   * @since 16-Feb-2015
   * @purpose  This program is used call the database procedure to submit the UCAS subjects.
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.       Changes desc                                      Rel Ver.
   * *************************************************************************************************************************
   * 16-Feb-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
   */
public class UcasSubjectSubmitSP extends StoredProcedure {
  public UcasSubjectSubmitSP(DataSource datasource) {
    setDataSource(datasource);    
    setSql(GlobalConstants.GET_SUBJECT_LIST_PRC);    
    declareParameter(new SqlParameter("P_SUBJECT_ID_STR", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_UCAS_POINTS", OracleTypes.NUMERIC));
    declareParameter(new SqlParameter("P_SUBMIT_FLAG", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("PC_SEARCH_RESULTS", OracleTypes.CURSOR, new SearchResultsRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_GET_SUBJECT_LIST", OracleTypes.CURSOR, new SubjectListRowMapperImpl()));    
  }

  /**
    * This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List parameters) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_SUBJECT_ID_STR", parameters.get(1));
      inMap.put("P_UCAS_POINTS", parameters.get(0));
      inMap.put("P_SUBMIT_FLAG", parameters.get(2));
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  
  private class SearchResultsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      UcasCalcBean ucasBeanVO = new UcasCalcBean();
      //college_id, college_name_display, college_name, college_logo, best_match_course      
      ucasBeanVO.setCollegeId(rs.getString("college_id"));
      ucasBeanVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      ucasBeanVO.setCollegeName(rs.getString("college_name"));
      ucasBeanVO.setCollegeLogo(rs.getString("college_logo"));
      if (!GenericValidator.isBlankOrNull(ucasBeanVO.getCollegeLogo())) {
        ucasBeanVO.setProviderImage(new CommonUtil().getImgPath((ucasBeanVO.getCollegeLogo()), rowNum));
      }
      ucasBeanVO.setCollegeNameUrl(new SeoUrls().construnctUniHomeURL(ucasBeanVO.getCollegeId(), ucasBeanVO.getCollegeName()));
       String tmpUrl = null;
      ResultSet bestMatchCoursesRS = (ResultSet)rs.getObject("best_match_course");      
      try {
        if (bestMatchCoursesRS != null) {
          ArrayList bestMatchCoursesList = new ArrayList();
          UcasBestMatchCoursesVO bestMatchCoursesVO = null;
          while (bestMatchCoursesRS.next()) {
            bestMatchCoursesVO = new UcasBestMatchCoursesVO();
            bestMatchCoursesVO.setCollegeId(bestMatchCoursesRS.getString("college_id"));
            bestMatchCoursesVO.setCourseTitle(bestMatchCoursesRS.getString("course_title"));
            bestMatchCoursesVO.setCourseId(bestMatchCoursesRS.getString("course_id"));
            bestMatchCoursesVO.setSubOrderItemId(bestMatchCoursesRS.getString("suborder_item_id"));
            bestMatchCoursesVO.setWebsitePrice(bestMatchCoursesRS.getString("website_price"));
            bestMatchCoursesVO.setWebformPrice(bestMatchCoursesRS.getString("webform_price"));            
            tmpUrl = bestMatchCoursesRS.getString("email_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            bestMatchCoursesVO.setSubOrderEmailWebform(tmpUrl);            
            bestMatchCoursesVO.setSubOrderEmail(bestMatchCoursesRS.getString("email"));
            bestMatchCoursesVO.setALevel(bestMatchCoursesRS.getString("a_level"));
            bestMatchCoursesVO.setSqaHigher(bestMatchCoursesRS.getString("sqa_higher"));
            bestMatchCoursesVO.setSqaAdv(bestMatchCoursesRS.getString("sqa_adv"));
            bestMatchCoursesVO.setUcasPoints(bestMatchCoursesRS.getString("ucas_points"));
            bestMatchCoursesVO.setALevelMax(bestMatchCoursesRS.getString("a_level_max"));
            bestMatchCoursesVO.setSqaHigherMax(bestMatchCoursesRS.getString("sqa_higher_max"));
            bestMatchCoursesVO.setSqaAdvMax(bestMatchCoursesRS.getString("sqa_adv_max"));
            bestMatchCoursesVO.setUcasPointsMax(bestMatchCoursesRS.getString("ucas_points_max"));
            bestMatchCoursesVO.setSeoStudyLevelText(bestMatchCoursesRS.getString("study_level"));
            bestMatchCoursesVO.setCpeQualificationNetworkId(bestMatchCoursesRS.getString("network_id"));
            bestMatchCoursesVO.setCourseUcasTariff(bestMatchCoursesRS.getString("ucas_tariff"));            
            bestMatchCoursesVO.setCourseDetailUrl(new SeoUrls().construnctCourseDetailsPageURL(bestMatchCoursesVO.getSeoStudyLevelText(), bestMatchCoursesVO.getCourseTitle(), bestMatchCoursesVO.getCourseId(), bestMatchCoursesVO.getCollegeId(), "COURSE_SPECIFIC"));
            bestMatchCoursesList.add(bestMatchCoursesVO);
          }
          ucasBeanVO.setBestMatchCoursesList(bestMatchCoursesList);
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (bestMatchCoursesRS != null) {
            bestMatchCoursesRS.close();
            bestMatchCoursesRS = null;
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      
      return ucasBeanVO;
    }
  }
  
  private class SubjectListRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      UcasCalcBean ucasBean = new UcasCalcBean();
      ucasBean.setSubjectId(rs.getString("subject_id"));
      ucasBean.setSubjectDesc(rs.getString("subject_desc"));
      return ucasBean;
    }
  }  
  
}