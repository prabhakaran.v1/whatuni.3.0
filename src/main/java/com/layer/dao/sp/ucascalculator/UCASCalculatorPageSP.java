package com.layer.dao.sp.ucascalculator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.ucascalculator.bean.UcasCalcBean;

/**
   * @UCASCalculatorSP
   * @author Thiyagu G
   * @version 1.0
   * @since 16-Feb-2015
   * @purpose  This program is used call the database procedure to build the UCAS calculator page.
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.       Changes desc                                      Rel Ver.
   * *************************************************************************************************************************
   * 16-Feb-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
   */
public class UCASCalculatorPageSP extends StoredProcedure {
  public UCASCalculatorPageSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_UCAS_YEAR_OF_ENTRY_PRC);
    declareParameter(new SqlParameter("P_YEAR_OF_ENTRY", OracleTypes.NUMERIC));    
    declareParameter(new SqlOutParameter("PC_QUALIFICATION_LIST", OracleTypes.CURSOR, new YearOfEntryRowMapperImpl()));    
    declareParameter(new SqlOutParameter("P_YEAR_EXIST_FLAG", OracleTypes.VARCHAR));
  }

  /**
    * This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List parameters) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_YEAR_OF_ENTRY", parameters.get(0));
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  
  public class YearOfEntryRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {      
      UcasCalcBean ucasCalcVO = new UcasCalcBean();
      try{
        ucasCalcVO.setQualId(rs.getString("qual_id"));
        ucasCalcVO.setQualification(rs.getString("qualification"));
        ucasCalcVO.setParentQualification(rs.getString("parent_qualification"));
        ucasCalcVO.setGradeStr(rs.getString("grade_str"));
        ucasCalcVO.setNoOfSubjects(rs.getString("no_of_subjects"));        
      } catch (Exception e) {
        e.printStackTrace();
      }
      return ucasCalcVO;
    }
  } 
  
}