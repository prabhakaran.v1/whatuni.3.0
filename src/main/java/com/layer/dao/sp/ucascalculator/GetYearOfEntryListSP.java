package com.layer.dao.sp.ucascalculator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.ucascalculator.bean.UcasCalcBean;

/**
   * @GetYearOfEntryListSP
   * @author Thiyagu G
   * @version 1.0
   * @since 16-Feb-2015
   * @purpose  This program is used call the database procedure to build the UCAS tariff table.
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.       Changes desc                                      Rel Ver.
   * *************************************************************************************************************************
   * 16-Feb-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
   */
public class GetYearOfEntryListSP extends StoredProcedure {
  public GetYearOfEntryListSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.UCAS_YEAR_OF_ENTRY_FN);
    declareParameter(new SqlOutParameter("pc_yoe_list", OracleTypes.CURSOR, new TariffTableRowMapperImpl()));    
    compile();
    
  }

  /**
    * This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute() {
    Map outMap = null;
    try {
      Map inMap = new HashMap();      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  
  private class TariffTableRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      UcasCalcBean ucasBean = new UcasCalcBean();      
      ucasBean.setYearOfEntry(rs.getString("year_of_entry"));
      return ucasBean;
    }
  }
  
}