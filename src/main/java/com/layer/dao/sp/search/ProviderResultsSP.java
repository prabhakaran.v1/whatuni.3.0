package com.layer.dao.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import mobile.valueobject.SearchVO;
import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.common.SeoMetaDetailsRowMapperImpl;
import spring.valueobject.search.ProviderResultPageVO;
import spring.valueobject.search.RefineByOptionsVO;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.layer.util.rowmapper.review.PRUserReviewsRowMapperImpl;
import com.layer.util.rowmapper.review.UniOverallReviewRowMapperImpl;
import com.layer.util.rowmapper.review.UniRankningRowMapperImpl;
import com.layer.util.rowmapper.review.WuscaRatingRowMapperImpl;
import com.layer.util.rowmapper.search.ProviderResultsRowMapperNew;
import com.layer.util.rowmapper.search.StatsLogRowMapperImpl;
import com.layer.util.rowmapper.search.UniStatsInfoRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.interstitialsearch.AssessmentTypeVO;
import com.wuni.util.valueobject.search.SimilarStudyLevelCoursesVO;

/**
 * This class to display the provider results
 * @author
 * @version 1.0
 * @since 17.06.2009
 * Change Log
 * *******************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
 * *******************************************************************************************************************************
 * 03-Nov-2015    Indumathi                 1.1      Added additional parameter for showing banners in advertiser page.    wu_546
 * 27-Jan-2016    Prabhakaran V.            1.2      Added WUSCA rating parameter for key stats pod.                       wu_548
 * 27_Jan_2016    Thiyagu G                 1.3      Removed p_college_id, p_location_id, p_search_cat_id                  wu_548
 * 28_Aug_2018    Sabapathi S.              1.5      Added new filters as column (Assessment)                              wu_580
 * 23_Oct_2018    Sabapathi S.              1.5      Received studymode from DB side                                       wu_582
 * 20_Nov_2018    Sangeeth.S                1.6      Received open day info from DB side in CollegeDetailsRowMapperImpl to wu_583
 *                                                    fix the broken schema in tructured data for next open day pod in PR page.
 * 28_Apr_2020    Sangeeth.s                1.7      Added out column event category name for od logging events			   wu_603   
 * 20_Oct_2020    Sri Sankari R             1.8     Added new cursor for dynamic meta details from Back office             wu_20201020
 *                                                       
 */
public class ProviderResultsSP extends StoredProcedure {
  SeoUrls seoUrls = new SeoUrls();
  public ProviderResultsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_PROVIDER_RESULT_PAGE_PRC);
    declareParameter(new SqlParameter("x", Types.VARCHAR));
    declareParameter(new SqlParameter("y", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_what", Types.VARCHAR));
    declareParameter(new SqlParameter("p_phrase_search", Types.VARCHAR));
    declareParameter(new SqlParameter("p_college_name", Types.VARCHAR));    
    declareParameter(new SqlParameter("p_qualification", Types.VARCHAR));
    declareParameter(new SqlParameter("p_town_city", Types.VARCHAR));
    declareParameter(new SqlParameter("p_postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_range", Types.VARCHAR));    
    declareParameter(new SqlParameter("p_study_mode", Types.VARCHAR));
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_how", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_category", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entity_text", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR)); //no
    declareParameter(new SqlParameter("p_basket_id", Types.VARCHAR)); //no    
    declareParameter(new SqlParameter("p_page_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_level", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_points", Types.VARCHAR));    
    declareParameter(new SqlParameter("p_univ_loc_type_name_str", Types.VARCHAR));
    declareParameter(new SqlParameter("p_campus_based_univ_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_russell_group_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_employment_rate", Types.VARCHAR));
    declareParameter(new SqlParameter("p_module_search_phrase", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_tariff_range", Types.VARCHAR));
    declareParameter(new SqlParameter("p_network_id", Types.VARCHAR)); //no
    declareParameter(new SqlParameter("p_jacs_code", Types.VARCHAR)); //3_JUN_2014   
    declareParameter(new SqlParameter("p_search_url", Types.VARCHAR)); //21_JUL_2015_REL Added by Priyaa for Course search logging
    declareParameter(new SqlParameter("p_track_session_id", Types.NUMERIC));
    declareParameter(new SqlOutParameter("pc_search_results", OracleTypes.CURSOR, new ProviderResultsRowMapperNew()));
    declareParameter(new SqlOutParameter("pc_study_mode_refine", OracleTypes.CURSOR, new StudymodeRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_qualification_refine", OracleTypes.CURSOR, new QualificationRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_subject_refine", OracleTypes.CURSOR, new SubjectRefineRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_college_details", OracleTypes.CURSOR, new CollegeDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_college_unistats_info", OracleTypes.CURSOR, new UniStatsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_user_reviews", OracleTypes.CURSOR, new PRUserReviewsRowMapperImpl())); //Changed existing cursor for key stats pod, 24_Feb_2015 By Thiyagu G
    declareParameter(new SqlOutParameter("pc_uni_raking", OracleTypes.CURSOR, new UniRankningRowMapperImpl())); //Added new cursor for key stats pod, 24_Feb_2015 By Thiyagu G
    declareParameter(new SqlOutParameter("pc_overall_review", OracleTypes.CURSOR, new UniOverallReviewRowMapperImpl())); //Added new cursor for user reviews section, 24_Feb_2015 By Thiyagu G
  		declareParameter(new SqlOutParameter("pc_WUSCA_ratings", OracleTypes.CURSOR, new WuscaRatingRowMapperImpl())); //Added new cursor for WUSCA Rating in key stats pod, 27_JAN_2016 By Prabha
    declareParameter(new SqlOutParameter("pc_stats_log", OracleTypes.CURSOR, new StatsLogRowMapperImpl())); //Added new cursor for stats logging, 27_JAN_2016 By Thiyagu G
    declareParameter(new SqlOutParameter("p_suggested_module_text", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_reg_clear_result_exist_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", OracleTypes.VARCHAR)); //9_DEC_2014 Added by Priyaa for pixel tracking
    declareParameter(new SqlOutParameter("o_avg_rating", OracleTypes.VARCHAR)); //Added new object for key stats pod, 24_Feb_2015 By Thiyagu G
    declareParameter(new SqlOutParameter("o_adv_flag", OracleTypes.VARCHAR)); //Added by Indumathi.S for showing banners in advertiser page. Nov-03-15 Rel  
    //Added by Indumathi.S For URL redirection Jan-27-16 Rel
    declareParameter(new SqlOutParameter("p_parent_cat_id", Types.NUMERIC));
    declareParameter(new SqlOutParameter("p_parent_cat_text_key", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_parent_cat_desc", Types.VARCHAR));
    //End
    //Added by Thiyagu G for rollup search 04_Oct_2016
    declareParameter(new SqlOutParameter("p_invalid_category_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_rollup_to_category", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_rollup_type", Types.VARCHAR));
    //Added below for assessment filter 23_Aug_2018, By Sabapathi.S
    declareParameter(new SqlOutParameter("pc_assessment_type_filter", OracleTypes.CURSOR, new assessmentTypeFilterRowMapperImpl()));
    declareParameter(new SqlParameter("p_assessment_type", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_insights_study_mode_id", Types.VARCHAR)); // wu582_20181023 - Sabapathi: Added studymode for stats logging
    declareParameter(new SqlOutParameter("PC_META_DETAILS", OracleTypes.CURSOR, new SeoMetaDetailsRowMapperImpl())); // Added new cursor for dynamic meta details from Back office, 20201020 rel by Sri Sankari
    //End
  }
    
  public Map execute(SearchVO inputBean) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("x", inputBean.getX());
      inMap.put("y", inputBean.getY());
      inMap.put("p_search_what", inputBean.getSearchWhat());
      inMap.put("p_phrase_search", inputBean.getPhraseSearch());
      inMap.put("p_college_name", inputBean.getCollegeName());      
      inMap.put("p_qualification", inputBean.getQualification());
      inMap.put("p_town_city", inputBean.getTownCity());
      inMap.put("p_postcode", inputBean.getPostcode());
      inMap.put("p_search_range", inputBean.getSearchRange());      
      inMap.put("p_study_mode", inputBean.getStudyMode());
      inMap.put("a", inputBean.getAffiliateId());
      inMap.put("p_search_how", inputBean.getSearchHOW());
      inMap.put("p_search_category", inputBean.getSearchCategory());
      inMap.put("p_entity_text", inputBean.getEntityText());
      inMap.put("p_user_agent", inputBean.getUserAgent());
      inMap.put("p_page_no", inputBean.getPageNo());
      inMap.put("p_basket_id", inputBean.getBasketId());      
      inMap.put("p_page_name", inputBean.getPageName());
      inMap.put("p_entry_level", inputBean.getEntryLevel());
      inMap.put("p_entry_points", inputBean.getEntryPoints());
      inMap.put("p_univ_loc_type_name_str", inputBean.getUnivLocTypeName());
      inMap.put("p_campus_based_univ_name", inputBean.getCampusTypeName());
      inMap.put("p_russell_group_name", inputBean.getRusselGroupName());
      inMap.put("p_employment_rate", inputBean.getEmpRate());
      inMap.put("p_module_search_phrase", inputBean.getModuleStr());
      inMap.put("p_ucas_code", inputBean.getUcasCode());
      inMap.put("p_ucas_tariff_range", inputBean.getUcasTariff());
      inMap.put("p_network_id", inputBean.getNetworkId());
      inMap.put("p_jacs_code", inputBean.getJacsCode()); //3_JUN_2014    
      inMap.put("p_search_url", inputBean.getRequestURL()); //21_JUL_2015_REL Added by Priyaa for Course search logging
      inMap.put("p_track_session_id", inputBean.getUserTrackSessionId());
      inMap.put("p_assessment_type", inputBean.getAssessmentType()); //Added below for assessment filter 23_Aug_2018, By Sabapathi.S
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }    
    return outMap;
  }

  private class StudymodeRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      RefineByOptionsVO locationRefineVO = new RefineByOptionsVO();
      locationRefineVO.setRefineDesc(resultSet.getString("refine_desc"));
      locationRefineVO.setRefineCode(resultSet.getString("refine_code"));
      locationRefineVO.setRefineTextKey(resultSet.getString("refine_text_key"));      
      return locationRefineVO;
    }
  }

  private class SubjectRefineRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {      
      SimilarStudyLevelCoursesVO subjectRefineVO = new SimilarStudyLevelCoursesVO();
      subjectRefineVO.setCategoryCode(resultSet.getString("category_code"));
      subjectRefineVO.setCategoryDesc(resultSet.getString("subject"));
      subjectRefineVO.setSubjectTextKey(resultSet.getString("subject_text_key"));      
      subjectRefineVO.setCollegeCount(resultSet.getString("result_count"));
      subjectRefineVO.setCategoryId(resultSet.getString("browse_cat_id"));
      subjectRefineVO.setQualificationCode(resultSet.getString("qualification")); //get from ram sire
      subjectRefineVO.setBrowseMoneyPageUrl(seoUrls.construnctNewBrowseMoneyPageURL(subjectRefineVO.getQualificationCode(), subjectRefineVO.getCategoryId(), subjectRefineVO.getCategoryDesc(), "PAGE"));
      return subjectRefineVO;
    }
  }

  private class QualificationRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      RefineByOptionsVO locationRefineVO = new RefineByOptionsVO();
      locationRefineVO.setRefineDesc(resultSet.getString("refine_desc"));
      locationRefineVO.setRefineCode(resultSet.getString("refine_code"));
      locationRefineVO.setRefineTextKey(resultSet.getString("refine_text_key"));      
      locationRefineVO.setCategoryId(resultSet.getString("browse_cat_id"));
      locationRefineVO.setCategoryName(resultSet.getString("browse_cat_desc"));
      locationRefineVO.setBrowseCatTextKey(resultSet.getString("browse_cat_text_key"));
      locationRefineVO.setRefineDisplayDesc(resultSet.getString("refine_display_desc"));
      return locationRefineVO;
    }
  }

  private class CollegeDetailsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ProviderResultPageVO collegeDetails = new ProviderResultPageVO();
      CommonUtil commonutil = new CommonUtil();
      collegeDetails.setScholarshipCount(rs.getString("sch_cnt"));
      collegeDetails.setCollegeOverview(rs.getString("overview"));
      collegeDetails.setCollegeName(rs.getString("college_name"));
      collegeDetails.setCollegeNameDisplay(rs.getString("college_name_display"));
      //
      if (!GenericValidator.isBlankOrNull(rs.getString("overall_rating"))) {
        collegeDetails.setActualRating(commonutil.formatNumberReview(rs.getString("overall_rating")));
      }
      if (!GenericValidator.isBlankOrNull(collegeDetails.getActualRating())) { //13_MAY_2014_REL
        int roundOfRating = (int)Math.round((Double.parseDouble(rs.getString("overall_rating"))));
        collegeDetails.setCollegeStudentRating(String.valueOf(roundOfRating));
      }
      //
      collegeDetails.setNextOpenDay(rs.getString("next_open_date"));
      String nextOpenDay = collegeDetails.getNextOpenDay();
      if (nextOpenDay != null && nextOpenDay.trim().length() > 0) {
        String splitedDate[] = nextOpenDay.split("##");
        if (splitedDate != null && splitedDate.length > 3) {
          collegeDetails.setOpendateDay(splitedDate[0]);
          collegeDetails.setOpendateDate(splitedDate[1]);
          collegeDetails.setOpendateMonth(splitedDate[2]);
          collegeDetails.setOpenDayType(splitedDate[3]); //Added OpenDayType for 13_Dec_2016, by Thiyagu G.
        }
      }
      collegeDetails.setCollegeLogo(rs.getString("college_logo"));
      collegeDetails.setCollegeId(rs.getString("hc_college_id"));
      collegeDetails.setCollegeLoaction(rs.getString("college_location"));
      collegeDetails.setReviewCount(rs.getString("review_count"));
      collegeDetails.setUniReviewsURL(seoUrls.constructReviewPageSeoUrl(collegeDetails.getCollegeName(), collegeDetails.getCollegeId()));
      collegeDetails.setUniHomeURL(seoUrls.construnctUniHomeURL(collegeDetails.getCollegeId(), collegeDetails.getCollegeName()));
      //Added opendays button related column by Prabha on 8_May_2018
      collegeDetails.setNetworkId(rs.getString("network_id"));
      collegeDetails.setOpendaySuborderItemId(rs.getString("open_day_suborder_item_id"));
      collegeDetails.setOpenDate(rs.getString("open_day"));
      collegeDetails.setOpenMonthYear(rs.getString("open_month_year"));
      collegeDetails.setBookingUrl(rs.getString("booking_url"));
      collegeDetails.setTotalOpendays(rs.getString("total_open_days"));
      //Added schema tag info for NOV_20th rel by Sangeeth.S
      collegeDetails.setHeadline(rs.getString("od_headlines"));
      collegeDetails.setOpendayDesc(rs.getString("od_description"));      
      collegeDetails.setStOpenDayStartDate(rs.getString("od_start_date"));
      collegeDetails.setStOpenDayEndDate(rs.getString("od_end_date"));
      collegeDetails.setOpenDayUrl(rs.getString("od_url"));
      collegeDetails.setOpendaysProviderURL(seoUrls.constructOpendaysSeoUrl(collegeDetails.getCollegeName(), collegeDetails.getCollegeId()));    
      collegeDetails.setOpendayVenue(rs.getString("od_event_venue"));
      collegeDetails.setEventCategoryId(rs.getString("EVENT_CATEGORY_ID"));
      collegeDetails.setEventCategoryName(rs.getString("EVENT_CATEGORY_NAME"));
      //
      //End of openday code
      return collegeDetails;
    }
  }
  
  private class assessmentTypeFilterRowMapperImpl implements RowMapper {
     public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
       AssessmentTypeVO assessmentTypeVO = new AssessmentTypeVO();
       assessmentTypeVO.setAssessmentTypeId(resultSet.getString("assessment_type_id"));
       assessmentTypeVO.setAssessmentDisplayName(resultSet.getString("assessment_type_name"));
       assessmentTypeVO.setUrlText(resultSet.getString("url_text"));
       return assessmentTypeVO;
     }
   }
}