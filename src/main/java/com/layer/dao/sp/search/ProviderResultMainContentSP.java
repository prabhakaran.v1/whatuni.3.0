package com.layer.dao.sp.search;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.common.TickerTapeRowMapperImpl;
import spring.rowmapper.common.UserCompareBasketRowMapperImpl;
import spring.rowmapper.common.UserInfoRowMapperImpl;
import spring.rowmapper.seo.PageMetaTagsRowMapperImpl;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;

import com.layer.util.rowmapper.advert.SpListRowMapperImpl;
import com.layer.util.rowmapper.advert.UniProfileInfoRowMapperImpl;
import com.layer.util.rowmapper.common.CollegeNamesRowMapperImpl;
import com.layer.util.rowmapper.common.StudyLevelListRowMapperImpl;
import com.layer.util.rowmapper.common.StudyModeListMapperImpl;
import com.layer.util.rowmapper.common.SubjectListRowMapperImpl;
import com.layer.util.rowmapper.openday.OpenDayInfoRowMapperImpl;
import com.layer.util.rowmapper.review.UserReviewsRowMapperImpl;
import com.layer.util.rowmapper.scholarship.PoviderScholarshipRowMapperImpl;
import com.layer.util.rowmapper.search.CpeWorstPerformersRowMapperImpl;
import com.layer.util.rowmapper.search.ProviderResultsRowMapper;
import com.layer.util.rowmapper.search.UniStatsInfoRowMapperImpl;
import com.layer.util.rowmapper.search.UniversityDPPodRowMapperImpl;

public class ProviderResultMainContentSP extends StoredProcedure {
  public ProviderResultMainContentSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("hot_wuni.wuni_spring_pkg.get_provider_result_data_proc");    
    declareParameter(new SqlParameter("p_session_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
    declareParameter(new SqlParameter("search_what", Types.VARCHAR));
    declareParameter(new SqlParameter("phrase_search", Types.VARCHAR));
    declareParameter(new SqlParameter("college_name", Types.VARCHAR));    
    declareParameter(new SqlParameter("qualification", Types.VARCHAR));
    declareParameter(new SqlParameter("country", Types.VARCHAR));
    declareParameter(new SqlParameter("town_city", Types.VARCHAR));
    declareParameter(new SqlParameter("postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("search_range", Types.VARCHAR));
    declareParameter(new SqlParameter("postflag", Types.VARCHAR));
    declareParameter(new SqlParameter("location_id", Types.VARCHAR));
    declareParameter(new SqlParameter("study_mode", Types.VARCHAR));
    declareParameter(new SqlParameter("course_duration", Types.VARCHAR));
    declareParameter(new SqlParameter("submit", Types.VARCHAR));
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("aui", Types.VARCHAR));
    declareParameter(new SqlParameter("search_how", Types.VARCHAR));
    declareParameter(new SqlParameter("z", Types.VARCHAR));
    declareParameter(new SqlParameter("s_type", Types.VARCHAR));
    declareParameter(new SqlParameter("search_category", Types.VARCHAR));
    declareParameter(new SqlParameter("search_career", Types.VARCHAR));
    declareParameter(new SqlParameter("career_id", Types.VARCHAR));
    declareParameter(new SqlParameter("nrp", Types.VARCHAR));
    declareParameter(new SqlParameter("area", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pg_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_col", Types.VARCHAR));
    declareParameter(new SqlParameter("p_county_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pheader_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entity_text", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_where", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_browse_link", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_basket_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));    
    declareParameter(new SqlParameter("p_page_name", Types.VARCHAR));
    //Added By Sekhar for wu28022012 for new search filter entry points
    declareParameter(new SqlParameter("p_entry_level", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_points", Types.VARCHAR));

    declareParameter(new SqlParameter("p_meta_page_name", Types.VARCHAR));  
    declareParameter(new SqlParameter("p_meta_page_flag", Types.VARCHAR));  
    declareParameter(new SqlOutParameter("o_search_results", OracleTypes.CURSOR, new ProviderResultsRowMapper()));
    declareParameter(new SqlOutParameter("o_cpe_worst_performers", OracleTypes.CURSOR, new CpeWorstPerformersRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_filter_by_subjects", OracleTypes.CURSOR, new SubjectListRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_SCHOLARSHIP", OracleTypes.CURSOR, new PoviderScholarshipRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_college_unistats_info", OracleTypes.CURSOR, new UniStatsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_uni_sp_list", OracleTypes.CURSOR, new SpListRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_user_reviews", OracleTypes.CURSOR, new UserReviewsRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_uniprofile_info", OracleTypes.CURSOR, new UniProfileInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_ticker_tape", OracleTypes.CURSOR, new TickerTapeRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_user_compare_basket", OracleTypes.CURSOR, new UserCompareBasketRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_user_info", OracleTypes.CURSOR, new UserInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_page_meta_tags", OracleTypes.CURSOR, new PageMetaTagsRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_college_names", OracleTypes.CURSOR, new CollegeNamesRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_header_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_record_count", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_open_day_info", OracleTypes.CURSOR, new OpenDayInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_study_mode_list", OracleTypes.CURSOR, new StudyModeListMapperImpl()));
    declareParameter(new SqlOutParameter("o_study_level_list", OracleTypes.CURSOR, new StudyLevelListRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_university_dppod", OracleTypes.CURSOR, new UniversityDPPodRowMapperImpl()));//wu414_20120731
  }
  
  public Map execute(List inputList) {     
    Map outMap = null;
    Map inMap = new HashMap(); 
    try{           
      inMap.put("p_session_id", inputList.get(0));
      inMap.put("p_user_id", inputList.get(1));
      inMap.put("search_what", inputList.get(2));
      inMap.put("phrase_search", inputList.get(3));
      inMap.put("college_name", inputList.get(4));    
      inMap.put("qualification", inputList.get(5));
      inMap.put("country", inputList.get(6));
      inMap.put("town_city", inputList.get(7));
      inMap.put("postcode", inputList.get(8));
      inMap.put("search_range", inputList.get(9));
      inMap.put("postflag", inputList.get(10));
      inMap.put("location_id", inputList.get(11));
      inMap.put("study_mode", inputList.get(12));
      inMap.put("course_duration", inputList.get(13));
      inMap.put("submit", inputList.get(14));
      inMap.put("a", inputList.get(15));
      inMap.put("aui", inputList.get(16));
      inMap.put("search_how", inputList.get(17));
      inMap.put("z", inputList.get(18));
      inMap.put("s_type", inputList.get(19));
      inMap.put("search_category", inputList.get(20));
      inMap.put("search_career", inputList.get(21));
      inMap.put("career_id", inputList.get(22));
      inMap.put("nrp", inputList.get(23));
      inMap.put("area", inputList.get(24));
      inMap.put("p_pg_type", inputList.get(25));
      inMap.put("p_search_col", inputList.get(26));
      inMap.put("p_county_id", inputList.get(27));
      inMap.put("p_pheader_id", inputList.get(28));
      inMap.put("p_entity_text", inputList.get(29));
      inMap.put("p_search_where", inputList.get(30));
      inMap.put("p_user_agent", inputList.get(31));
      inMap.put("p_search_browse_link", inputList.get(32));
      inMap.put("p_ucas_code", inputList.get(33));
      inMap.put("p_basket_id", inputList.get(34));
      inMap.put("p_page_no", inputList.get(35));  
      inMap.put("p_page_name", inputList.get(36));    
      //Added By Sekhar for wu28022012 for new search filter entry points
      inMap.put("p_entry_level", inputList.get(37));
      inMap.put("p_entry_points", inputList.get(38));

      inMap.put("p_meta_page_name", inputList.get(38));    
      inMap.put("p_meta_page_flag", inputList.get(39));    
      outMap = execute(inMap);
    }catch(Exception e){
      e.printStackTrace();    
    }
    if(TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG){
      new AdminUtilities().logDbCallParameterDetails("WHATUNI_SEARCH3.GET_PROVIDER_RESULT_PAGE", inMap);  
    }
    return outMap;
  }

}//class
