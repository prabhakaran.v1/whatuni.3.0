package com.layer.dao.sp.search;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.rowmapper.search.ModuleStageListRowMapperImpl;
import com.wuni.valueobjects.ModuleVO;

/**
  * will return course-details page info
  * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-details/[COURSE_TITLE]-courses-details/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
  *
  *
  * @since        wu318_20111020 - redesign
  * @author       Mohamed Syed
  *
  * @param inputList
  * @return
  *
  */
public class GetModuleStageDetailsSP extends StoredProcedure {

  public GetModuleStageDetailsSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql("WU_COURSE_DETAILS_PKG.get_course_module_grp_fn");
    //
    declareParameter(new SqlOutParameter("lc_mod_details", OracleTypes.CURSOR, new ModuleStageListRowMapperImpl()));
    declareParameter(new SqlParameter("P_COURSE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_MODULE_GROUP_ID", Types.VARCHAR));   
  }

  public Map execute(ModuleVO moduleVO) {
    Map outMap = null;
    try{
      Map inMap = new HashMap();
      inMap.put("P_COURSE_ID", moduleVO.getCourseId());
      inMap.put("P_MODULE_GROUP_ID", moduleVO.getModuleGroupId());
      outMap = execute(inMap);
    }catch(Exception e){
      e.printStackTrace();
    }
    return outMap;
  }

}
