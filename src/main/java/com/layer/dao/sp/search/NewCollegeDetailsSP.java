package com.layer.dao.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import mobile.valueobject.SearchVO;
import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

/**
 * This class to display the provider results
 * @author
 * @version 1.0
 * @since 24_Jan_2017
 * Change Log
 * *******************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
 * *******************************************************************************************************************************
 * 27_Jan_2016    Thiyagu G                 1.0      Intial draft                                                         wu_561
 *
 */
public class NewCollegeDetailsSP extends StoredProcedure {
  public NewCollegeDetailsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_NEW_COLLEGE_NAME_PRC);
    declareParameter(new SqlParameter("p_old_college_name", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_college_details", OracleTypes.CURSOR, new NewCollegeDetailsRowMapperImpl()));
  } 
  public Map execute(SearchVO inputVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("p_old_college_name", inputVO.getUrlProviderName());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class NewCollegeDetailsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      SearchVO searchVO = new SearchVO();
      searchVO.setCollegeId(resultSet.getString("COLLEGE_ID"));
      searchVO.setNewCollegeName(resultSet.getString("NEW_COLLEGE_NAME"));      
      return searchVO;
    }
  }
}
