package com.layer.dao.sp.search.advancesearch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import mobile.valueobject.SearchVO;
import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.search.AssessedFilterVO;
import spring.valueobject.search.ProviderResultPageVO;
import spring.valueobject.search.RefineByOptionsVO;
import spring.valueobject.search.ReviewCatFilterVO;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.layer.util.rowmapper.review.PRUserReviewsRowMapperImpl;
import com.layer.util.rowmapper.review.UniOverallReviewRowMapperImpl;
import com.layer.util.rowmapper.review.UniRankningRowMapperImpl;
import com.layer.util.rowmapper.review.WuscaRatingRowMapperImpl;
import com.layer.util.rowmapper.search.StatsLogRowMapperImpl;
import com.layer.util.rowmapper.search.UniStatsInfoRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.search.SimilarStudyLevelCoursesVO;

/**
 * PAGE_NAME:   Advance provider results page
 * URL_PATTERN: www.whatuni.com/[STUDY_LEVEL_DESC]-courses/herb/search?university=[UNIVERSITY-NAME]&subject=[KEYWORD]&study-mode=[STUDY_LEVEL_DESC]&location=[LOCATION1]&entry-level=[ENTRY-LEVEL]&entry-points=[ENTRY-POINTS]&sort=[ORDER_BY]&pageno=[PAGE_NO]
 * URL_EXAMPLE: /degree-courses/herb/search?university=bangor-university&subject=business
 *
 * @author  Prabhakaran V.
 * @since   wu571_20171129 - redesign
 * Change Log
 * *******************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
 * *******************************************************************************************************************************
 * 08_May_2018    Prabhakaran V.             1.1     Added opendays button related column                                   wu_576
 *
 */
public class AdvanceProviderResultsSP extends StoredProcedure {

  public AdvanceProviderResultsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_ADVANCE_PROVIDER_RESULTS_PRC);
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SESSION_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_NAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_NETWORK_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SEARCH_CATEGORY", Types.VARCHAR));
    declareParameter(new SqlParameter("P_KEYWORD_SEARCH", Types.VARCHAR));
    declareParameter(new SqlParameter("P_QUALIFICATION", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
    declareParameter(new SqlParameter("P_PAGE_NO", Types.VARCHAR)); //
    declareParameter(new SqlParameter("P_PAGE_NAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_BASKET_ID", Types.VARCHAR)); //
    declareParameter(new SqlParameter("P_ORDER_BY", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REGION", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REGION_FLAG", Types.VARCHAR)); //
    declareParameter(new SqlParameter("P_LOCATION_TYPE", Types.VARCHAR));
    declareParameter(new SqlParameter("P_STUDY_MODE", Types.VARCHAR));
    declareParameter(new SqlParameter("P_ENTRY_LEVEL", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_points", Types.VARCHAR));
    declareParameter(new SqlParameter("P_ASSESSMENT_TYPE", Types.VARCHAR)); //
    declareParameter(new SqlParameter("P_REVIEW_SUBJECT_STR", Types.VARCHAR)); //
    declareParameter(new SqlParameter("p_search_url", Types.VARCHAR)); //
    declareParameter(new SqlParameter("p_track_session_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_jacs_code", Types.VARCHAR)); // 
    declareParameter(new SqlOutParameter("PC_SEARCH_RESULT", OracleTypes.CURSOR, new ProviderResultsRowMapperNew()));
    declareParameter(new SqlOutParameter("PC_COLLEGE_DETAILS", OracleTypes.CURSOR, new CollegeDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_COLLEGE_UNISTATS_INFO", OracleTypes.CURSOR, new UniStatsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_QUAL_FILTERS", OracleTypes.CURSOR, new QualificationRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_SUBJECT_FILTERS", OracleTypes.CURSOR, new SubjectRefineRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_STUDY_MODE_FILTER", OracleTypes.CURSOR, new StudymodeRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_ASSESSMENT_TYPE_FILTER", OracleTypes.CURSOR, new assessmentFilterRowMapperImpl())); 
    declareParameter(new SqlOutParameter("PC_REVIEW_CATEGORY_FILTER", OracleTypes.CURSOR, new reviewCatFilterRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_user_reviews", OracleTypes.CURSOR, new PRUserReviewsRowMapperImpl())); 
    declareParameter(new SqlOutParameter("pc_uni_raking", OracleTypes.CURSOR, new UniRankningRowMapperImpl())); 
    declareParameter(new SqlOutParameter("pc_overall_review", OracleTypes.CURSOR, new UniOverallReviewRowMapperImpl())); 
  		declareParameter(new SqlOutParameter("pc_WUSCA_ratings", OracleTypes.CURSOR, new WuscaRatingRowMapperImpl())); 
    declareParameter(new SqlOutParameter("PC_STATS_LOG", OracleTypes.CURSOR, new StatsLogRowMapperImpl()));
    declareParameter(new SqlOutParameter("p_include_best_match", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_pixel_tracking_code", OracleTypes.VARCHAR)); //
    declareParameter(new SqlOutParameter("p_adv_flag", OracleTypes.VARCHAR)); //
     declareParameter(new SqlOutParameter("p_avg_rating", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_invalid_category_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_parent_cat_id", Types.NUMERIC));
    declareParameter(new SqlOutParameter("p_parent_cat_text_key", Types.VARCHAR)); 
    declareParameter(new SqlOutParameter("p_parent_cat_desc", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_rollup_to_category", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_rollup_type", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_show_count", Types.VARCHAR));
    compile();
  }
    
  public Map execute(SearchVO inputBean) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("P_USER_ID", inputBean.getY());
      inMap.put("P_SESSION_ID", inputBean.getX());
      inMap.put("P_COLLEGE_NAME", inputBean.getCollegeName());      
      inMap.put("P_NETWORK_ID", inputBean.getNetworkId());
      inMap.put("P_SEARCH_CATEGORY", inputBean.getSearchCategory());
      inMap.put("P_KEYWORD_SEARCH", inputBean.getPhraseSearch()); //p_phrase_search
      inMap.put("P_QUALIFICATION", inputBean.getQualification());
      inMap.put("P_USER_AGENT", inputBean.getUserAgent());
      inMap.put("P_PAGE_NO", inputBean.getPageNo());
      inMap.put("P_PAGE_NAME", inputBean.getPageName());
      inMap.put("P_BASKET_ID", inputBean.getBasketId());
      inMap.put("P_ORDER_BY", inputBean.getSearchHOW()); //p_search_how
      inMap.put("P_REGION", inputBean.getTownCity()); //p_town_city
      inMap.put("P_REGION_FLAG", inputBean.getRegionFlag()); 
      inMap.put("P_LOCATION_TYPE", inputBean.getUnivLocTypeName()); //p_univ_loc_type_name_str - P_LOCATION_TYPE 
      inMap.put("P_STUDY_MODE", inputBean.getStudyMode());
      inMap.put("P_ENTRY_LEVEL", inputBean.getEntryLevel()); //p_entry_level - P_PREV_QUAL 
      inMap.put("p_entry_points", inputBean.getEntryPoints());
      inMap.put("P_ASSESSMENT_TYPE", inputBean.getAssessmentType()); // 
      inMap.put("P_REVIEW_SUBJECT_STR", inputBean.getReviewSubjects()); // 
      inMap.put("p_search_url", inputBean.getRequestURL()); //
      inMap.put("p_track_session_id", inputBean.getUserTrackSessionId());
      inMap.put("p_jacs_code", inputBean.getJacsCode()); //
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }    
    return outMap;
  }
  private class ProviderResultsRowMapperNew implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      ProviderResultPageVO providerResultPageVO = new ProviderResultPageVO();    
      SeoUrls seoUrl = new SeoUrls();
      providerResultPageVO.setCourseId(resultset.getString("COURSE_ID"));
      providerResultPageVO.setCourseDescription(resultset.getString("COURSE_DESC"));
      providerResultPageVO.setTotalCount(resultset.getString("total_cnt"));
      providerResultPageVO.setAvailableStudyModes(resultset.getString("AVAILABLE_STUDY_MODES"));
      providerResultPageVO.setDepartmentOrFaculty(resultset.getString("DEPARTMENT_OR_FACULTY"));
      providerResultPageVO.setCourseTitle(resultset.getString("COURSE_TITLE"));
      providerResultPageVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
      providerResultPageVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      providerResultPageVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      providerResultPageVO.setUcasCode(resultset.getString("UCAS_CODE"));
      providerResultPageVO.setStudyMode(resultset.getString("study_level"));
      providerResultPageVO.setWasThisCourseShortListed(resultset.getString("COURSE_IN_BASKET"));
      providerResultPageVO.setSeoStudyLevelText(resultset.getString("SEO_STUDY_LEVEL_TEXT"));
      providerResultPageVO.setEmploymentRate(resultset.getString("employment_rate"));
      providerResultPageVO.setCourseRank(resultset.getString("course_rank"));
      providerResultPageVO.setEntryRquirements(resultset.getString("entry_requirements"));
      providerResultPageVO.setModuleGroupDetails(resultset.getString("module_group_details"));
      providerResultPageVO.setJacsSubject(resultset.getString("JACS_SUBJECT"));
      String moduletext = resultset.getString("module_group_info_rec");
       if(!GenericValidator.isBlankOrNull(moduletext)){
         String modArr[] = moduletext.split(GlobalConstants.ADVICE_SP_SPLIT); //##SP##
         ArrayList moduledetailList = new ArrayList(Arrays.asList( modArr));      
         providerResultPageVO.setModuleDetailList(moduledetailList);
       }            
       //End of code
      if(!GenericValidator.isBlankOrNull(providerResultPageVO.getModuleGroupDetails())){
        String[] modArr= providerResultPageVO.getModuleGroupDetails().split(GlobalConstants.PROSPECTUS_SPLIT_CHAR); //##
        if(!GenericValidator.isBlankOrNull(modArr[1])){
          providerResultPageVO.setModuleGroupId(modArr[1]);
          if(!GenericValidator.isBlankOrNull(modArr[0])){
            providerResultPageVO.setModuleTitle(modArr[0]);
          }
        }
      }
      providerResultPageVO.setGaCollegeName(new WUI.utilities.CommonFunction().replaceSpecialCharacter(providerResultPageVO.getCollegeName()));
      if(!GenericValidator.isBlankOrNull(providerResultPageVO.getCourseRank())){
        providerResultPageVO.setRankOrdinal(new CommonFunction().getOrdinalFor(Integer.parseInt(providerResultPageVO.getCourseRank())));
      }    
      if(!GenericValidator.isBlankOrNull(providerResultPageVO.getModuleGroupDetails())){
        String[] modArr= providerResultPageVO.getModuleGroupDetails().split(GlobalConstants.PROSPECTUS_SPLIT_CHAR); //##
        if(modArr != null && modArr.length > 1 ){
          providerResultPageVO.setModuleGroupId(modArr[1]);
          providerResultPageVO.setModuleTitle(modArr[0]);
        }
      }  
      providerResultPageVO.setPageName(resultset.getString("page_name"));
      providerResultPageVO.setCourseDetailUrl(seoUrl.construnctCourseDetailsPageURL(providerResultPageVO.getSeoStudyLevelText(), providerResultPageVO.getCourseTitle(), providerResultPageVO.getCourseId(), providerResultPageVO.getCollegeId(), providerResultPageVO.getPageName()));//24_JUN_2014
      //
      providerResultPageVO.setFeeDuration(resultset.getString("FEE_DURATION"));
      providerResultPageVO.setFees(resultset.getString("COURSE_DOM_FEES"));
      //
      ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("ENQUIRY_DETAILS");
      try{
        if(enquiryDetailsRS != null){
          while(enquiryDetailsRS.next()){
            providerResultPageVO.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
            providerResultPageVO.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
            providerResultPageVO.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
            
            String tmpUrl = null;
            tmpUrl = enquiryDetailsRS.getString("WEBSITE");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            providerResultPageVO.setSubOrderWebsite(tmpUrl);
            //
            tmpUrl = enquiryDetailsRS.getString("EMAIL_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            providerResultPageVO.setSubOrderEmailWebform(tmpUrl);
            //
            tmpUrl = enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            providerResultPageVO.setSubOrderProspectusWebform(tmpUrl);
            providerResultPageVO.setNetworkId(enquiryDetailsRS.getString("network_id"));
            providerResultPageVO.setWebformPrice(enquiryDetailsRS.getString("webform_price"));
            providerResultPageVO.setWebsitePrice(enquiryDetailsRS.getString("website_price"));
            providerResultPageVO.setProfileType(enquiryDetailsRS.getString("profile_type"));
            providerResultPageVO.setMyhcProfileId(enquiryDetailsRS.getString("myhc_profile_id"));
            providerResultPageVO.setProfileId("0");
          }
          providerResultPageVO.setLocationMatchFlag(resultset.getString("location_match"));
          providerResultPageVO.setLocationTypeMatchFlag(resultset.getString("location_type_match"));
          providerResultPageVO.setStudyModeMatchFlag(resultset.getString("study_mode_match"));
          providerResultPageVO.setEntryLevelMatchFlag(resultset.getString("prev_qual_match"));
          providerResultPageVO.setAssessedMatchFlag(resultset.getString("assessment_type_match"));
          providerResultPageVO.setReviewSub1MatchFlag(resultset.getString("review_subject_one_match"));
          providerResultPageVO.setReviewSub2MatchFlag(resultset.getString("review_subject_two_match"));
          providerResultPageVO.setReviewSub3MatchFlag(resultset.getString("review_subject_three_match"));
          providerResultPageVO.setMatchPercentage(resultset.getString("matching_percentage"));
          providerResultPageVO.setQualificationMatchFlag(resultset.getString("qualification_match"));
          providerResultPageVO.setSubjectMatchFlag(resultset.getString("subject_match"));
        }        
      } catch(Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if(enquiryDetailsRS != null) {
            enquiryDetailsRS.close();
          }
        } catch(Exception e) {
          throw new SQLException(e.getMessage());
        }  
      }
      if("SP".equalsIgnoreCase(providerResultPageVO.getProfileType()))
      {
        providerResultPageVO.setSpURL(seoUrl.construnctSpURL(providerResultPageVO.getCollegeId(), providerResultPageVO.getCollegeName(), providerResultPageVO.getMyhcProfileId(), providerResultPageVO.getProfileId(), providerResultPageVO.getNetworkId(), "overview"));
      }
      return providerResultPageVO; 
    }
  }
  private class StudymodeRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      RefineByOptionsVO locationRefineVO = new RefineByOptionsVO();
      locationRefineVO.setRefineDesc(resultSet.getString("refine_desc"));
      // locationRefineVO.setRefineCode(resultSet.getString("refine_code"));
      locationRefineVO.setRefineTextKey(resultSet.getString("refine_text_key"));      
      locationRefineVO.setSelectedStudyMode(resultSet.getString("study_mode_selected"));
      return locationRefineVO;
    }
  }
  private class assessmentFilterRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      AssessedFilterVO assessedFilterVO = new AssessedFilterVO();
      assessedFilterVO.setAssessmentTypeId(resultSet.getString("assessment_type_id"));
      assessedFilterVO.setAssessmentTypeName(resultSet.getString("assessment_type_display_name")); 
      assessedFilterVO.setSelectedAssessment(resultSet.getString("selected_text"));       
      return assessedFilterVO;
    }
  }
  
  private class reviewCatFilterRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      ReviewCatFilterVO reviewCatFilterVO = new ReviewCatFilterVO();
      reviewCatFilterVO.setReviewCatId(resultSet.getString("review_category_id"));
      reviewCatFilterVO.setReviewCatName(resultSet.getString("review_category_display_name")); 
      reviewCatFilterVO.setReviewCatTextKey(resultSet.getString("text_key"));
      reviewCatFilterVO.setReviewSelectText(resultSet.getString("selected_text"));       
      return reviewCatFilterVO;
    }
  }
  
  private class SubjectRefineRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      SeoUrls seoUrls = new SeoUrls();
      SimilarStudyLevelCoursesVO subjectRefineVO = new SimilarStudyLevelCoursesVO();
      subjectRefineVO.setCategoryCode(resultSet.getString("category_code"));
      subjectRefineVO.setCategoryDesc(resultSet.getString("subject"));
      subjectRefineVO.setSubjectTextKey(resultSet.getString("subject_text_key"));      
      subjectRefineVO.setCollegeCount(resultSet.getString("result_count"));
      subjectRefineVO.setCategoryId(resultSet.getString("browse_cat_id"));
      subjectRefineVO.setQualificationCode(resultSet.getString("qualification")); //get from ram sire
      subjectRefineVO.setBrowseMoneyPageUrl(seoUrls.construnctNewBrowseMoneyPageURL(subjectRefineVO.getQualificationCode(), subjectRefineVO.getCategoryId(), subjectRefineVO.getCategoryDesc(), "PAGE"));
      return subjectRefineVO;
    }
  }

  private class QualificationRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      RefineByOptionsVO qualRefineVO = new RefineByOptionsVO();
      qualRefineVO.setRefineDesc(resultSet.getString("refine_desc"));
      qualRefineVO.setRefineCode(resultSet.getString("refine_code"));
      qualRefineVO.setRefineTextKey(resultSet.getString("refine_text_key"));       
      // qualRefineVO.setCategoryId(resultSet.getString("browse_cat_id"));
      qualRefineVO.setCategoryName(resultSet.getString("browse_cat_desc"));
      qualRefineVO.setBrowseCatTextKey(resultSet.getString("browse_cat_text_key"));       
      //qualRefineVO.setCategoryCode(resultSet.getString("browse_category_code"));
      return qualRefineVO;
    }
  }

  private class CollegeDetailsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ProviderResultPageVO collegeDetails = new ProviderResultPageVO();
      CommonUtil commonutil = new CommonUtil();
     // collegeDetails.setScholarshipCount(rs.getString("sch_cnt"));
      collegeDetails.setCollegeOverview(rs.getString("overview"));
      collegeDetails.setCollegeName(rs.getString("college_name"));
      collegeDetails.setCollegeNameDisplay(rs.getString("college_name_display"));
      //
      if (!GenericValidator.isBlankOrNull(rs.getString("overall_rating"))) {
        collegeDetails.setActualRating(commonutil.formatNumberReview(rs.getString("overall_rating")));
      }
      if (!GenericValidator.isBlankOrNull(collegeDetails.getActualRating())) { //13_MAY_2014_REL
        int roundOfRating = (int)Math.round((Double.parseDouble(rs.getString("overall_rating"))));
        collegeDetails.setCollegeStudentRating(String.valueOf(roundOfRating));
      }
      //
      collegeDetails.setNextOpenDay(rs.getString("next_open_date"));
      String nextOpenDay = collegeDetails.getNextOpenDay();
      if (nextOpenDay != null && nextOpenDay.trim().length() > 0) {
        String splitedDate[] = nextOpenDay.split("##");
        if (splitedDate != null && splitedDate.length > 3) {
          collegeDetails.setOpendateDay(splitedDate[0]);
          collegeDetails.setOpendateDate(splitedDate[1]);
          collegeDetails.setOpendateMonth(splitedDate[2]);
          collegeDetails.setOpenDayType(splitedDate[3]); //Added OpenDayType for 13_Dec_2016, by Thiyagu G.
        }
      }
      collegeDetails.setCollegeLogo(rs.getString("college_logo"));
      collegeDetails.setCollegeId(rs.getString("hc_college_id"));
      collegeDetails.setReviewCount(rs.getString("review_count"));
      collegeDetails.setUniReviewsURL(new SeoUrls().constructReviewPageSeoUrl(collegeDetails.getCollegeName(), collegeDetails.getCollegeId()));
      collegeDetails.setUniHomeURL(new SeoUrls().construnctUniHomeURL(collegeDetails.getCollegeId(), collegeDetails.getCollegeName()));
     //Added opendays button related column by Prabha on 08_May_2018
      collegeDetails.setNetworkId(rs.getString("network_id"));
      collegeDetails.setOpendaySuborderItemId(rs.getString("open_day_suborder_item_id"));
      collegeDetails.setOpenDate(rs.getString("open_day"));
      collegeDetails.setOpenMonthYear(rs.getString("open_month_year"));
      collegeDetails.setBookingUrl(rs.getString("booking_url"));
      collegeDetails.setTotalOpendays(rs.getString("total_open_days"));
      //End of openday code
      return collegeDetails;
    }
  }
  
}
