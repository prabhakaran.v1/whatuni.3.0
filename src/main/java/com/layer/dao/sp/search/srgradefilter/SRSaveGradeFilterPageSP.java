package com.layer.dao.sp.search.srgradefilter;

import WUI.whatunigo.bean.GradeFilterBean;
import com.layer.util.SpringConstants;
import com.wuni.util.sql.OracleArray;
import java.sql.Connection;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @see This SP class is used to call the database procedure to save the user entry requirements before search result page.
  * @since  22.11.2019- intital draft
  * @version 1.0
  *
  * Modification history:
  * *****************************************************************************************************************
  * Author          Relase tag       Modification Details                                                  Rel Ver.
  * *****************************************************************************************************************
  * Jeyalakshmi.D       1.0             initial draft                                                       wu_596
  *
  */

public class SRSaveGradeFilterPageSP extends StoredProcedure {
  DataSource dataSource;
  public SRSaveGradeFilterPageSP(DataSource dataSource) {
    setDataSource(dataSource);
    this.dataSource = dataSource;
    setSql(SpringConstants.SR_SAVE_QUAL_DATA_PRC);
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_SESSION_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("PT_QUAL_DETAIL_ARR", Types.ARRAY, "TB_USER_SUBJ_GRADE_DETAIL"));
    declareParameter(new SqlOutParameter("P_USER_UCAS_POINTS", OracleTypes.VARCHAR));    
    compile();
  }
  public Map execute(GradeFilterBean gradeFilterBean) {
    //
    Connection connection = null;
    Map<String, Object> outMap = null;
    HashMap inMap = new HashMap();
    //
    try{
      connection = dataSource.getConnection();
      ARRAY qualDetailsArr = OracleArray.getOracleArray(connection, "TB_USER_SUBJ_GRADE_DETAIL", gradeFilterBean.getQualDetailsArr());
      inMap.put("P_USER_ID",gradeFilterBean.getUserId());
      inMap.put("P_SESSION_ID",gradeFilterBean.getSessionId());
      inMap.put("PT_QUAL_DETAIL_ARR", qualDetailsArr);
      outMap = execute(inMap);
    } catch(Exception e){
       e.printStackTrace();
    }
    finally {
      try {
	if (connection != null) {
	   connection.close();
	}
      } catch (Exception closeException) {
	    closeException.printStackTrace();
      }
   }
   return outMap;
  }
}
