package com.layer.dao.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.search.form.SearchBean;

import com.layer.util.SpringConstants;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : GetPredictedGradesSP.java
* Description   : Get the userd predicted grades values
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	               Change
* *************************************************************************************************************************************
* Anbarasan.R             1.0             26.11.2012           First draft   		
*
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class GetPredictedGradesSP extends StoredProcedure{
  public GetPredictedGradesSP(DataSource datasource) {
    setDataSource(datasource);    
    setFunction(true);
    setSql(SpringConstants.GET_USER_PREDICTED_GRADES_VAL_FN);
    declareParameter(new SqlOutParameter("lc_get_user_attr_val", OracleTypes.CURSOR, new GetPredictedGradesRowMapperImpl()));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER)); 
    compile();
  }
  //  
  public Map execute(SearchBean searchBean) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_user_id", searchBean.getUserId());
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
  
  private class GetPredictedGradesRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      SearchBean searchBean = new SearchBean();
      searchBean.setAttributeId(resultSet.getString("attribute_id"));      
      searchBean.setAttributeValue(resultSet.getString("attribute_value"));
      searchBean.setDisplaySeq(resultSet.getString("display_seq"));
      searchBean.setOptionId(resultSet.getString("option_id"));
      if("74".equals(searchBean.getOptionId())){
        searchBean.setOptionDesc("A_LEVEL");
      } else if("75".equals(searchBean.getOptionId())){
        searchBean.setOptionDesc("SQA_HIGHER");
      } else if("76".equals(searchBean.getOptionId())){
        searchBean.setOptionDesc("SQA_ADV");
      } else if("555".equals(searchBean.getOptionId())){
        searchBean.setOptionDesc("BTEC");
      } else {
        searchBean.setOptionDesc(resultSet.getString("option_desc"));
      }
      return searchBean;
    }
  }
}