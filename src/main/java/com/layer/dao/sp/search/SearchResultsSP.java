package com.layer.dao.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import mobile.valueobject.SearchVO;
import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.search.FeaturedBrandVO;
import spring.valueobject.search.OmnitureLoggingVO;
import spring.valueobject.search.RefineByOptionsVO;
import spring.valueobject.seo.SEOMetaDetailsVO;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.layer.util.rowmapper.search.OmnitureLoggingRowMapperImpl;
import com.layer.util.rowmapper.search.StatsLogRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.interstitialsearch.AssessmentTypeVO;
import com.wuni.util.valueobject.interstitialsearch.ReviewCategoryVO;
import com.wuni.util.valueobject.search.BestMatchCoursesVO;
import com.wuni.util.valueobject.search.CourseSpecificSearchResultsVO;
import com.wuni.util.valueobject.search.LocationRefineVO;
import com.wuni.util.valueobject.search.SimilarStudyLevelCoursesVO;

/**
 * PAGE_NAME:   Course specific Money page
 * URL_PATTERN: www.whatuni.com/[STUDY_LEVEL_DESC]-courses/search?q=[KEYWORD]&study-mode=[STUDY_LEVEL_DESC]&location=[LOCATION1]&entry-level=[ENTRY-LEVEL]&entry-points=[ENTRY-POINTS]&sort=[ORDER_BY]&pageno=[PAGE_NO]
 * URL_EXAMPLE: /degree-courses/search?subject=business
 *
 * @author  Mohamed Syed
 * @since   wu318_20111020 - redesign
 * Change Log
 * *******************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
 * *******************************************************************************************************************************
 * 27_Jan_2016    Thiyagu G                 1.3      Removed p_college_id, p_location_id, p_search_cat_id                  wu_548
 * 08_May_2018    Prabhakaran V.            1.4      Added opendays button related column                                  wu_576
 * 28_Aug_2018    Sabapathi S.              1.5      Added new filters as column (Assessment and pref)                     wu_580
 * 24_Feb_2020 	  Sangeeth.S			             Added new column random number generation							   wu_600 
 * 13_Apr_2020    Sangeeth.S				1.6      Added in-param for the user ip in geo loc track					   wu_602
 * 28_Apr_2020    Sangeeth.s                1.7      Added out column event category name for od logging events			   wu_603   
 * 21_JUL_2020    Kailash L                 1.8      Added out parameter for sponsored orderItemId			                  
 */
public class SearchResultsSP extends StoredProcedure {

  public SearchResultsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_SEARCH_RESULT_PAGE_PRC);//3_JUN_2014
    //
    declareParameter(new SqlParameter("x", Types.VARCHAR));
    declareParameter(new SqlParameter("y", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_what", Types.VARCHAR));
    declareParameter(new SqlParameter("p_phrase_search", Types.VARCHAR));
    declareParameter(new SqlParameter("p_college_name", Types.VARCHAR));    
    declareParameter(new SqlParameter("p_qualification", Types.VARCHAR));
    declareParameter(new SqlParameter("p_town_city", Types.VARCHAR));
    declareParameter(new SqlParameter("p_postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_range", Types.VARCHAR));    
    declareParameter(new SqlParameter("p_study_mode", Types.VARCHAR));
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_how", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_category", Types.VARCHAR));   
    declareParameter(new SqlParameter("p_entity_text", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.NUMERIC));
    declareParameter(new SqlParameter("p_basket_id", Types.NUMERIC));    
    declareParameter(new SqlParameter("p_page_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_level", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_points", Types.VARCHAR));
    declareParameter(new SqlParameter("p_univ_loc_type_name_str", Types.VARCHAR));
    declareParameter(new SqlParameter("p_campus_based_univ_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_russell_group_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_employment_rate", Types.VARCHAR));
    declareParameter(new SqlParameter("p_module_search_phrase", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_tariff_range", Types.VARCHAR));
    declareParameter(new SqlParameter("p_last_applied_filter_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_jacs_code", Types.VARCHAR));//3_JUN_2014    
    declareParameter(new SqlParameter("p_search_url", Types.VARCHAR));//8_OCT_2014 Added by Priyaa for What have you done pod 
    declareParameter(new SqlParameter("p_track_session_id", Types.NUMERIC));
    declareParameter(new SqlOutParameter("pc_search_results", OracleTypes.CURSOR, new CourseSpecificSearchResultRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_study_mode_refine", OracleTypes.CURSOR, new RefinRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_qualification_refine", OracleTypes.CURSOR, new QualificationRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_subject_refine", OracleTypes.CURSOR, new SubjectRefineRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_refines", OracleTypes.CURSOR, new RefineRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_location_refine", OracleTypes.CURSOR,  new LocationRefineRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_stats_log", OracleTypes.CURSOR, new StatsLogRowMapperImpl())); //Added new cursor for stats logging, 27_JAN_2016 By Thiyagu G
    declareParameter(new SqlOutParameter("p_record_count", Types.NUMERIC));
    declareParameter(new SqlOutParameter("p_total_course_count", Types.NUMERIC));   
    declareParameter(new SqlOutParameter("pc_search_college_ids", OracleTypes.CURSOR, new OmnitureLoggingRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_search_institution_ids", OracleTypes.CURSOR, new InstitutionIdsLoggingRowMapperImpl()));    
    declareParameter(new SqlOutParameter("p_result_exists", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_reg_clear_result_exist_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_parent_cat_id", Types.NUMERIC));
    declareParameter(new SqlOutParameter("p_parent_cat_text_key", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_parent_cat_desc", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_suggested_module_text", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_course_rank_found_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_invalid_category_flag", Types.VARCHAR)); //Added invalid category flag for roll up search for wu557_29092016, By Thiyagu G.
     //Added below for assessment and pref filter 23_Aug_2018, By Sabapathi.S
    declareParameter(new SqlOutParameter("pc_assessment_type_filter", OracleTypes.CURSOR, new assessmentTypeFilterRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_review_category_filter", OracleTypes.CURSOR, new reviewCategoryFilterRowMapperImpl()));
    declareParameter(new SqlParameter("p_assessment_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_review_subject_str", Types.VARCHAR));    
    declareParameter(new SqlInOutParameter("p_subj_grades_session_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_dynamic_random_number", Types.NUMERIC));
    declareParameter(new SqlOutParameter("p_user_qual_exist_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("pc_featured_brand", OracleTypes.CURSOR, new featuredBrandRowMapperImpl()));//Added by Jeyalakshmi D for Jan 6th 2020 rel
    declareParameter(new SqlParameter("p_user_ip", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_spons_order_item_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("pc_meta_details", OracleTypes.CURSOR, new SEODetails()));
    compile();
  }
  public Map execute(SearchVO searchVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("x", searchVO.getX());
      inMap.put("y", searchVO.getY());
      inMap.put("p_search_what", searchVO.getSearchWhat());
      inMap.put("p_phrase_search", searchVO.getPhraseSearch());
      inMap.put("p_college_name", searchVO.getCollegeName());      
      inMap.put("p_qualification", searchVO.getQualification());
      inMap.put("p_town_city", searchVO.getTownCity());
      inMap.put("p_postcode", searchVO.getPostcode());
      inMap.put("p_search_range", searchVO.getSearchRange());      
      inMap.put("p_study_mode", searchVO.getStudyMode());
      inMap.put("a", searchVO.getAffiliateId());
      inMap.put("p_search_how", searchVO.getSearchHOW());
      inMap.put("p_search_category", searchVO.getSearchCategory());
      inMap.put("p_entity_text", searchVO.getEntityText());
      inMap.put("p_user_agent", searchVO.getUserAgent());    
      inMap.put("p_page_no", searchVO.getPageNo());
      inMap.put("p_basket_id", searchVO.getBasketId());      
      inMap.put("p_page_name", searchVO.getPageName());
      inMap.put("p_entry_level", searchVO.getEntryLevel());
      inMap.put("p_entry_points", searchVO.getEntryPoints());
      inMap.put("p_univ_loc_type_name_str", searchVO.getUnivLocTypeName());
      inMap.put("p_campus_based_univ_name", searchVO.getCampusTypeName());
      inMap.put("p_russell_group_name", searchVO.getRusselGroupName());
      inMap.put("p_employment_rate", searchVO.getEmpRate());
      inMap.put("p_module_search_phrase", searchVO.getModuleStr());
      inMap.put("p_ucas_code", searchVO.getUcasCode());
      inMap.put("p_ucas_tariff_range", searchVO.getUcasTariff());
      inMap.put("p_last_applied_filter_flag", searchVO.getRecentFilterApplied());
      inMap.put("p_jacs_code", searchVO.getJacsCode());
      inMap.put("p_search_url", searchVO.getRequestURL());//8_OCT_2014 Added by Priyaa for What have you done pod
      inMap.put("p_track_session_id", searchVO.getUserTrackSessionId());//19_MAY_2015 - Friends activity     
       //Added below for assessment and pref filter 23_Aug_2018, By Sabapathi.S
      inMap.put("p_assessment_type", searchVO.getAssessmentType());
      inMap.put("p_review_subject_str", searchVO.getReviewSubjects());      
      inMap.put("p_subj_grades_session_id", searchVO.getSubjectSessionId());
      inMap.put("p_dynamic_random_number", searchVO.getRandomNumber());
      inMap.put("p_user_ip", searchVO.getClientIp());
      outMap = execute(inMap);      
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class RefinRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      RefineByOptionsVO locationRefineVO = new RefineByOptionsVO();
      locationRefineVO.setRefineDesc(resultSet.getString("refine_desc"));
      locationRefineVO.setRefineCode(resultSet.getString("refine_code")); 
      locationRefineVO.setRefineTextKey(resultSet.getString("refine_text_key"));       
      return locationRefineVO;
    }
  }
  
  private class SubjectRefineRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      SeoUrls seoUrls = new SeoUrls();
      SimilarStudyLevelCoursesVO subjectRefineVO = new SimilarStudyLevelCoursesVO();
      subjectRefineVO.setCategoryCode(resultSet.getString("category_code"));
      subjectRefineVO.setCategoryDesc(resultSet.getString("subject"));
      subjectRefineVO.setSubjectTextKey(resultSet.getString("subject_text_key"));      
      subjectRefineVO.setCollegeCount(resultSet.getString("result_count"));
      subjectRefineVO.setCategoryId(resultSet.getString("browse_cat_id"));
      subjectRefineVO.setQualificationCode(resultSet.getString("qualification"));//get from ram sire
      subjectRefineVO.setBrowseMoneyPageUrl(seoUrls.construnctNewBrowseMoneyPageURL(subjectRefineVO.getQualificationCode(), subjectRefineVO.getCategoryId(), subjectRefineVO.getCategoryDesc(), "PAGE"));
      return subjectRefineVO;
    }
  }

  private class LocationRefineRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      LocationRefineVO locationRefineVO = new LocationRefineVO();
      locationRefineVO.setRegionName(resultSet.getString("region_name"));
      locationRefineVO.setLocationTextKey(resultSet.getString("location_text_key"));      
      locationRefineVO.setTopRegionName(resultSet.getString("top_region_name")); 
      locationRefineVO.setRegionTextKey(resultSet.getString("region_text_key"));
      return locationRefineVO;
    }
  }
  //
  private class QualificationRowMapperImpl implements RowMapper {
     public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
       RefineByOptionsVO locationRefineVO = new RefineByOptionsVO();
       locationRefineVO.setRefineDesc(resultSet.getString("refine_desc"));
       locationRefineVO.setRefineCode(resultSet.getString("refine_code"));
       locationRefineVO.setRefineTextKey(resultSet.getString("refine_text_key"));       
       locationRefineVO.setCategoryId(resultSet.getString("browse_cat_id"));
       locationRefineVO.setCategoryName(resultSet.getString("browse_cat_desc"));
       locationRefineVO.setBrowseCatTextKey(resultSet.getString("browse_cat_text_key"));       
       locationRefineVO.setCategoryCode(resultSet.getString("browse_category_code"));
       locationRefineVO.setRefineDisplayDesc(resultSet.getString("refine_display_desc"));
       return locationRefineVO;
     }
   }
  //
  private class RefineRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      RefineByOptionsVO reineByOptionsVO = new RefineByOptionsVO();
      reineByOptionsVO.setRefineCode(resultSet.getString("option_id"));      
      reineByOptionsVO.setRefineDesc(resultSet.getString("option_desc"));
      reineByOptionsVO.setOptionTextKey(resultSet.getString("option_text_key"));      
      reineByOptionsVO.setRefineOrder(resultSet.getString("option_type"));
      return reineByOptionsVO;
    }  
  }
  //
  private class CourseSpecificSearchResultRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseSpecificSearchResultsVO courseSpecificSearchResultsVO = new CourseSpecificSearchResultsVO();
      SeoUrls seoUrl = new SeoUrls();
      CommonUtil commonutil = new CommonUtil();
      courseSpecificSearchResultsVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      courseSpecificSearchResultsVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
      courseSpecificSearchResultsVO.setCollegeTextKey(resultset.getString("COLLEGE_TEXT_KEY"));
      courseSpecificSearchResultsVO.setCollegePRUrl(resultset.getString("COLLEGE_PR_URL"));
      courseSpecificSearchResultsVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
      courseSpecificSearchResultsVO.setUniHomeUrl(seoUrl.construnctUniHomeURL(courseSpecificSearchResultsVO.getCollegeId(), courseSpecificSearchResultsVO.getCollegeName()));
      courseSpecificSearchResultsVO.setOverallRating(resultset.getString("rating"));
      if(!GenericValidator.isBlankOrNull(resultset.getString("exact_rating"))){
       courseSpecificSearchResultsVO.setExactRating(commonutil.formatNumberReview(resultset.getString("exact_rating")));//25-Mar-2014_REL
      }
      courseSpecificSearchResultsVO.setNumberOfCoursesForThisCollege(resultset.getString("HITS"));
      int count = Integer.parseInt(courseSpecificSearchResultsVO.getNumberOfCoursesForThisCollege()) - 1;
      courseSpecificSearchResultsVO.setAdditionalCourses(String.valueOf(count));
      courseSpecificSearchResultsVO.setWasThisCollegeShortlisted((resultset.getString("COLLEGE_IN_BASKET")));
      courseSpecificSearchResultsVO.setCollegeLogoPath(resultset.getString("COLLEGE_LOGO"));
      courseSpecificSearchResultsVO.setScholarShipCnt(resultset.getString("SCH_CNT"));
     // courseSpecificSearchResultsVO.setOverview(resultset.getString("overview"));
      courseSpecificSearchResultsVO.setRusselGroup(resultset.getString("russell_group_flag"));
      courseSpecificSearchResultsVO.setJacsSubject(resultset.getString("JACS_SUBJECT"));
      courseSpecificSearchResultsVO.setCurrentRank(resultset.getString("CURRENT_RANK_IN_CATEGORY"));
      courseSpecificSearchResultsVO.setTotalRank(resultset.getString("TOTAL_RANK_IN_CATEGORY"));
      courseSpecificSearchResultsVO.setIsSponsoredCollege(resultset.getString("CATEGORY_SPONSORED_FLAG"));
      //Added out column for openday button by Prabha on 08_May_2018
      courseSpecificSearchResultsVO.setNetworkId(resultset.getString("network_id"));
      courseSpecificSearchResultsVO.setOpendaySuborderItemId(resultset.getString("open_day_suborder_item_id"));
      courseSpecificSearchResultsVO.setOpenDate(resultset.getString("open_day"));
      courseSpecificSearchResultsVO.setOpenMonthYear(resultset.getString("open_month_year"));
      courseSpecificSearchResultsVO.setBookingUrl(resultset.getString("booking_url"));
      courseSpecificSearchResultsVO.setTotalOpendays(resultset.getString("total_open_days"));
      //End of opendays button code
      courseSpecificSearchResultsVO.setProviderId(resultset.getString("provider_id"));//Added by Sangeeth.S for the smart pixel logging college id      
      courseSpecificSearchResultsVO.setSponsoredListFlag(resultset.getString("std_advert_type"));
      courseSpecificSearchResultsVO.setEventCategoryId(resultset.getString("EVENT_CATEGORY_ID"));
      courseSpecificSearchResultsVO.setEventCategoryName(resultset.getString("EVENT_CATEGORY_NAME"));     
      ResultSet bestMatchCoursesRS = (ResultSet)resultset.getObject("BEST_MATCH_COURSES");
      try {
        if (bestMatchCoursesRS != null) {
          ArrayList bestMatchCoursesList = new ArrayList();
          BestMatchCoursesVO bestMatchCoursesVO = null;
          String tmpUrl = null;
          String courseUcasTariff = null;
          while (bestMatchCoursesRS.next()) {
            bestMatchCoursesVO = new BestMatchCoursesVO();
            bestMatchCoursesVO.setCourseId(bestMatchCoursesRS.getString("COURSE_ID"));
            bestMatchCoursesVO.setCourseTitle(bestMatchCoursesRS.getString("COURSE_TITLE"));
            bestMatchCoursesVO.setCourseUcasTariff(bestMatchCoursesRS.getString("UCAS_TARIFF"));
            courseUcasTariff = GenericValidator.isBlankOrNull(bestMatchCoursesVO.getCourseUcasTariff())? "": bestMatchCoursesVO.getCourseUcasTariff();
            bestMatchCoursesVO.setCourseUcasTariff(courseUcasTariff);
            bestMatchCoursesVO.setCourseDomesticFees(bestMatchCoursesRS.getString("COURSE_DOM_FEES"));
            bestMatchCoursesVO.setWasThisCourseShortlisted(bestMatchCoursesRS.getString("COURSE_IN_BASKET"));
            bestMatchCoursesVO.setStudyLevelDesc(bestMatchCoursesRS.getString("SEO_STUDY_LEVEL_TEXT"));
            bestMatchCoursesVO.setStudyLevelDescDet(bestMatchCoursesRS.getString("STUDY_LEVEL_DESC"));
            bestMatchCoursesVO.setUcasCode(bestMatchCoursesRS.getString("UCAS_CODE"));
            bestMatchCoursesVO.setSubOrderItemId(bestMatchCoursesRS.getString("SUBORDER_ITEM_ID"));
            bestMatchCoursesVO.setOrderItemId(bestMatchCoursesRS.getString("ORDER_ITEM_ID"));
            bestMatchCoursesVO.setSubOrderEmail(bestMatchCoursesRS.getString("EMAIL"));
            bestMatchCoursesVO.setSubOrderProspectus(bestMatchCoursesRS.getString("PROSPECTUS"));
            bestMatchCoursesVO.setFeeDuration(bestMatchCoursesRS.getString("FEE_DURATION"));
            // Code added by Priyaa for Jan 2016 release
            String moduletext = bestMatchCoursesRS.getString("module_group_info_rec");
            if(!GenericValidator.isBlankOrNull(moduletext)){
              String modArr[] = moduletext.split("##SP##");
              ArrayList moduledetailList = new ArrayList(Arrays.asList( modArr));      
              bestMatchCoursesVO.setModuleDetailList(moduledetailList);
            }            
            //End of code
            tmpUrl = bestMatchCoursesRS.getString("WEBSITE");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            bestMatchCoursesVO.setSubOrderWebsite(tmpUrl);
            //
            tmpUrl = bestMatchCoursesRS.getString("EMAIL_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            bestMatchCoursesVO.setSubOrderEmailWebform(tmpUrl);
            //
            tmpUrl = bestMatchCoursesRS.getString("PROSPECTUS_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            bestMatchCoursesVO.setSubOrderProspectusWebform(tmpUrl);
            //
            bestMatchCoursesVO.setMyhcProfileId(bestMatchCoursesRS.getString("MYHC_PROFILE_ID"));
            bestMatchCoursesVO.setProfileType(bestMatchCoursesRS.getString("PROFILE_TYPE"));
            bestMatchCoursesVO.setApplyNowFlag(bestMatchCoursesRS.getString("APPLY_NOW_FLAG"));
            bestMatchCoursesVO.setMediaId(bestMatchCoursesRS.getString("MEDIA_ID"));
            bestMatchCoursesVO.setMediaPath(bestMatchCoursesRS.getString("MEDIA_PATH"));
            bestMatchCoursesVO.setMediaType(bestMatchCoursesRS.getString("MEDIA_TYPE"));
            bestMatchCoursesVO.setCpeQualificationNetworkId(bestMatchCoursesRS.getString("NETWORK_ID"));
            bestMatchCoursesVO.setPageName(bestMatchCoursesRS.getString("page_name"));
            bestMatchCoursesVO.setCourseDetailsPageURL(seoUrl.construnctCourseDetailsPageURL(bestMatchCoursesVO.getStudyLevelDesc(), bestMatchCoursesVO.getCourseTitle(), bestMatchCoursesVO.getCourseId(), courseSpecificSearchResultsVO.getCollegeId(), bestMatchCoursesVO.getPageName()));
            bestMatchCoursesVO.setHotLineNo(bestMatchCoursesRS.getString("hotline"));
            bestMatchCoursesVO.setCourseClearingPlaces(bestMatchCoursesRS.getString("places_available"));
            bestMatchCoursesVO.setInstitutionClearingPlaces(bestMatchCoursesRS.getString("tot_places_available"));
            bestMatchCoursesVO.setLastUpdated(bestMatchCoursesRS.getString("last_updated_date"));
            bestMatchCoursesVO.setWebformPrice(bestMatchCoursesRS.getString("webform_price"));
            bestMatchCoursesVO.setWebsitePrice(bestMatchCoursesRS.getString("website_price"));
            bestMatchCoursesVO.setModuleGroupDetails(bestMatchCoursesRS.getString("module_group_details"));
            if(!GenericValidator.isBlankOrNull(bestMatchCoursesVO.getModuleGroupDetails())){
              String[] modArr= bestMatchCoursesVO.getModuleGroupDetails().split("##");
              if(!GenericValidator.isBlankOrNull(modArr[1])){
                bestMatchCoursesVO.setModuleGroupId(modArr[1]);
                if(!GenericValidator.isBlankOrNull(modArr[0])){
                  bestMatchCoursesVO.setModuleTitle(modArr[0]);
                }
              }
            }
            bestMatchCoursesVO.setEmploymentRate(bestMatchCoursesRS.getString("employment_rate"));
            bestMatchCoursesVO.setCourseRank(bestMatchCoursesRS.getString("course_rank"));
            if(!GenericValidator.isBlankOrNull(bestMatchCoursesVO.getCourseRank())){
             bestMatchCoursesVO.setOrdinalRank(new CommonFunction().getOrdinalFor(Integer.parseInt(bestMatchCoursesVO.getCourseRank())));
            }
            bestMatchCoursesVO.setAddedToFinalChoice(bestMatchCoursesRS.getString("final_choices_exist_flag"));  
            bestMatchCoursesVO.setDepartmentOrFaculty(bestMatchCoursesRS.getString("DEPARTMENT_OR_FACULTY"));//Added by Indumathi.S 29-03-16     
            if("SP".equalsIgnoreCase(bestMatchCoursesVO.getProfileType())){
               bestMatchCoursesVO.setSpUrl(seoUrl.construnctSpURL(courseSpecificSearchResultsVO.getCollegeId(), courseSpecificSearchResultsVO.getCollegeName(), bestMatchCoursesVO.getMyhcProfileId(), "", bestMatchCoursesVO.getCpeQualificationNetworkId(), "overview"));
            }
            //
            bestMatchCoursesList.add(bestMatchCoursesVO);               
          }          
          courseSpecificSearchResultsVO.setBestMatchCoursesList(bestMatchCoursesList);
        }
        courseSpecificSearchResultsVO.setReviewCount(resultset.getString("review_count"));
        courseSpecificSearchResultsVO.setUniReviewsURL(new SeoUrls().constructReviewPageSeoUrl(courseSpecificSearchResultsVO.getCollegeName(), courseSpecificSearchResultsVO.getCollegeId()));
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (bestMatchCoursesRS != null) {
            bestMatchCoursesRS.close();
            bestMatchCoursesRS = null;
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      return courseSpecificSearchResultsVO;
    }
  }
  //
  private class InstitutionIdsLoggingRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      OmnitureLoggingVO omnitureLoggingVO = new OmnitureLoggingVO();
      omnitureLoggingVO.setInstitutionIdsForThisSearch(resultSet.getString("institution_ids"));
      return omnitureLoggingVO;
    }  
  }
  //
  private class assessmentTypeFilterRowMapperImpl implements RowMapper {
     public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
       AssessmentTypeVO assessmentTypeVO = new AssessmentTypeVO();
       assessmentTypeVO.setAssessmentTypeId(resultSet.getString("assessment_type_id"));
       assessmentTypeVO.setAssessmentDisplayName(resultSet.getString("assessment_type_name"));
       assessmentTypeVO.setUrlText(resultSet.getString("url_text"));
       return assessmentTypeVO;
     }
   }
   //
   private class reviewCategoryFilterRowMapperImpl implements RowMapper {
      public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        ReviewCategoryVO reviewCategoryVO = new ReviewCategoryVO();
        reviewCategoryVO.setReviewCategoryId(resultSet.getString("review_category_id"));
        reviewCategoryVO.setReviewCategoryDisplayName(resultSet.getString("review_category_name"));
        reviewCategoryVO.setUrlText(resultSet.getString("url_text"));
        return reviewCategoryVO;
      }
   }
   //
   private class featuredBrandRowMapperImpl implements RowMapper {
	 public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
	   FeaturedBrandVO featuredBrandVO = new FeaturedBrandVO();
	   featuredBrandVO.setCollegeId(resultSet.getString("college_id"));
	   featuredBrandVO.setCollegeName(resultSet.getString("college_name"));
	   featuredBrandVO.setMediaId(resultSet.getString("media_id")); 
	   featuredBrandVO.setMediaPath(resultSet.getString("media_path"));
	   featuredBrandVO.setThumbnailPath(resultSet.getString("thumbnail_path"));
	   featuredBrandVO.setMediaType(resultSet.getString("media_type"));
	   featuredBrandVO.setHeadline(resultSet.getString("headline"));
	   featuredBrandVO.setProfileHeadlineUrl(resultSet.getString("profile_headline_url"));
	   featuredBrandVO.setTagline(resultSet.getString("tagline"));
	   featuredBrandVO.setReviewDisplayflag(resultSet.getString("review_display_flag"));
	   featuredBrandVO.setReviewCount(resultSet.getString("review_count"));
	   featuredBrandVO.setOverallRating(resultSet.getString("overall_rating"));
	   featuredBrandVO.setOverallRatingExact(resultSet.getString("overall_rating_exact"));
       featuredBrandVO.setNavigationText(resultSet.getString("navigation_text"));
	   featuredBrandVO.setNavigationUrl(resultSet.getString("navigation_url"));
	   featuredBrandVO.setOrderItemId(resultSet.getString("order_item_id"));
	   featuredBrandVO.setUniReviewUrl(new SeoUrls().constructReviewPageSeoUrl(featuredBrandVO.getCollegeName(), featuredBrandVO.getCollegeId()));
	   return featuredBrandVO;
	 }
   }
   
   private class SEODetails implements RowMapper {
     public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
       SEOMetaDetailsVO	seoMetaDetailsVO = new SEOMetaDetailsVO();
       seoMetaDetailsVO.setMetaTitle(resultSet.getString("META_TITLE")); 
       seoMetaDetailsVO.setMetaDesc(resultSet.getString("META_DESC"));
       seoMetaDetailsVO.setMetaKeyword(resultSet.getString("META_KEYWORD"));
       seoMetaDetailsVO.setMetaRobots(resultSet.getString("META_ROBO"));
       seoMetaDetailsVO.setCategoryDesc(resultSet.getString("CATEGORY_DESC"));
       seoMetaDetailsVO.setStudyDesc(resultSet.getString("STUDY_DESC"));
       return seoMetaDetailsVO;
     }
  }
}
