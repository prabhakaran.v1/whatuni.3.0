package com.layer.dao.sp.search;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;

import com.layer.util.rowmapper.advert.EnquiryInfoRowMapperImpl;
import com.layer.util.rowmapper.common.CollegeNamesRowMapperImpl;
import com.layer.util.rowmapper.kis.HowYouAreAssesedRowMapperImpl;
import com.layer.util.rowmapper.kis.MatchingCourse;
import com.layer.util.rowmapper.kis.OpportunitiesListRowMapper;
import com.layer.util.rowmapper.kis.StudyModeTabRowMapperImpl;
import com.layer.util.rowmapper.kis.SubjectDataRowMpperImpl;
import com.layer.util.rowmapper.search.CourseDetailsDsktpRowMapperImpl;
import com.layer.util.rowmapper.search.EntryPointsForUCASCourseRowMapperImpl;
import com.layer.util.rowmapper.search.LocationsInfoRowMapperImpl;
import com.layer.util.rowmapper.search.ModuleListRowMapperImpl;
import com.layer.util.rowmapper.search.SubjectTimesRowMapperImpl;

public class CourseDetailsKISAjaxSP extends StoredProcedure{

  public CourseDetailsKISAjaxSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("wuni_spring_pkg.course_page_ajax_prc");
    //
    declareParameter(new SqlParameter("p_course_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_opportunity_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_study_level", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pagename", Types.VARCHAR));
    //
    declareParameter(new SqlOutParameter("o_course_details", OracleTypes.CURSOR, new CourseDetailsDsktpRowMapperImpl()));//19_NOV_2013_REL
    declareParameter(new SqlOutParameter("o_assessed_course", OracleTypes.CURSOR, new HowYouAreAssesedRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_opportunity_venue", OracleTypes.CURSOR, new OpportunitiesListRowMapper()));
    declareParameter(new SqlOutParameter("o_location_info", OracleTypes.CURSOR, new LocationsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_subject_data", OracleTypes.CURSOR, new SubjectDataRowMpperImpl()));
    declareParameter(new SqlOutParameter("o_enquiry_info", OracleTypes.CURSOR, new EnquiryInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_college_names", OracleTypes.CURSOR, new CollegeNamesRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_ucas_course_details", OracleTypes.CURSOR, new EntryPointsForUCASCourseRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_study_mode", OracleTypes.CURSOR, new StudyModeTabRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_match_course_link", OracleTypes.CURSOR, new MatchingCourse()));
    declareParameter(new SqlOutParameter("o_pg_entry_req", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_module_info", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("oc_subject_times_ranking", OracleTypes.CURSOR, new SubjectTimesRowMapperImpl())); //11_FEB_2014_REL  
    declareParameter(new SqlOutParameter("oc_module_details", OracleTypes.CURSOR, new ModuleListRowMapperImpl()));      
    
  }

  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_course_id", inputList.get(0));
    inMap.put("p_college_id", inputList.get(1));
    inMap.put("p_opportunity_id", inputList.get(2));
    inMap.put("p_study_level", inputList.get(3));
    inMap.put("p_pagename", inputList.get(4));
    outMap = execute(inMap);
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("wuni_spring_pkg.course_page_ajax_prc", inMap);
    }
    return outMap;
  }
}
