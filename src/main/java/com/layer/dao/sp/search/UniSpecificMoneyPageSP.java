package com.layer.dao.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.search.DBExecutionTimeVO;
import spring.valueobject.search.OmnitureLoggingVO;
import spring.valueobject.search.RefineByOptionsVO;
import WUI.admin.utilities.AdminUtilities;
import WUI.admin.utilities.TimeTrackingConstants;

import com.layer.util.rowmapper.search.UniSpecificSearchResultsRowMapperImpl;
import com.wuni.util.valueobject.search.CourseSpecificSearchResultsVO;

/**
 * PAGE_NAME:   Course specific Money page
 * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-courses/[KEYWORD]-[STUDY_LEVEL_DESC]-courses-[LOCATION1]/[KEYWORD]/[STUDY_LEVEL_ID]/[LOCATION2]/[LOCATION2]/[SEARCH_RANGE]/[COUNTY_ID]/[STUDY_MODE_ID]/[CATEGORY_CODE]/[ORDER_BY]/[COLLEGE_ID]/[PAGE_NO]/[EXTRA_TEXT]/[UC/U/C]/[ENTRY-LEVEL]/[ENTRY-POINTS]/page.html
 * URL_EXAMPLE: /degrees/courses/degree-courses/business-degree-courses-united-kingdom/business/m/united+kingdom/united+kingdom/25/0/a1/0/r/0/1/0/uc/a-level/3a*-1a-0b-0c-0d-0e/page.html
 *
 * @author  Mohamed Syed
 * @since   wu318_20111020 - redesign
 *
 */
public class UniSpecificMoneyPageSP extends StoredProcedure {

  public UniSpecificMoneyPageSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WHATUNI_SEARCH3.GET_MONEY_PAGE_MAIN_CONTENT");
    //
    declareParameter(new SqlParameter("x", Types.VARCHAR));
    declareParameter(new SqlParameter("y", Types.VARCHAR));
    declareParameter(new SqlParameter("search_what", Types.VARCHAR));
    declareParameter(new SqlParameter("phrase_search", Types.VARCHAR));
    declareParameter(new SqlParameter("college_name", Types.VARCHAR));
    declareParameter(new SqlParameter("qualification", Types.VARCHAR));
    declareParameter(new SqlParameter("country", Types.VARCHAR));
    declareParameter(new SqlParameter("town_city", Types.VARCHAR));
    declareParameter(new SqlParameter("postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("search_range", Types.VARCHAR));
    declareParameter(new SqlParameter("postflag", Types.VARCHAR));
    declareParameter(new SqlParameter("location_id", Types.VARCHAR));
    declareParameter(new SqlParameter("study_mode", Types.VARCHAR));
    declareParameter(new SqlParameter("course_duration", Types.VARCHAR));
    declareParameter(new SqlParameter("lucky", Types.VARCHAR));
    declareParameter(new SqlParameter("crs_search", Types.VARCHAR));
    declareParameter(new SqlParameter("submit", Types.VARCHAR));
    declareParameter(new SqlParameter("a", Types.VARCHAR));
    declareParameter(new SqlParameter("search_how", Types.VARCHAR));
    declareParameter(new SqlParameter("s_type", Types.VARCHAR));
    declareParameter(new SqlParameter("search_category", Types.VARCHAR));
    declareParameter(new SqlParameter("search_career", Types.VARCHAR));
    declareParameter(new SqlParameter("career_id", Types.VARCHAR));
    declareParameter(new SqlParameter("ref_id", Types.VARCHAR));
    declareParameter(new SqlParameter("nrp", Types.VARCHAR));
    declareParameter(new SqlParameter("area", Types.VARCHAR));
    declareParameter(new SqlParameter("p_pg_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_col", Types.VARCHAR));
    declareParameter(new SqlParameter("p_county_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entity_text", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_where", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_agent", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_browse_link", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_deemed_uni", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_action", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", Types.VARCHAR));
    declareParameter(new SqlParameter("p_basket_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_cat_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_page_name", Types.VARCHAR)); 
    declareParameter(new SqlParameter("p_entry_level", Types.VARCHAR));
    declareParameter(new SqlParameter("p_entry_points", Types.VARCHAR));
 
    //
    declareParameter(new SqlOutParameter("o_search_results", OracleTypes.CURSOR, new UniSpecificSearchResultsRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_refine_by_options", OracleTypes.CURSOR, new RefineByOptionsRowMapper()));
    declareParameter(new SqlOutParameter("o_header_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_record_count", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_search_college_ids", OracleTypes.CURSOR, new OmnitureLoggingRowMapper()));
    declareParameter(new SqlOutParameter("o_pod_time", OracleTypes.CURSOR, new DBExecutionTimeRowImpl()));
    declareParameter(new SqlOutParameter("o_snippet", OracleTypes.CURSOR, new SnipetTxtRowImpl()));
    declareParameter(new SqlOutParameter("o_college_count", Types.NUMERIC));
    declareParameter(new SqlOutParameter("o_clearing_crs_exist", Types.VARCHAR));
  }

  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("x", inputList.get(0));
      inMap.put("y", inputList.get(1));
      inMap.put("search_what", inputList.get(2));
      inMap.put("phrase_search", inputList.get(3));
      inMap.put("college_name", inputList.get(4));
      inMap.put("qualification", inputList.get(5));
      inMap.put("country", inputList.get(6));
      inMap.put("town_city", inputList.get(7));
      inMap.put("postcode", inputList.get(8));
      inMap.put("search_range", inputList.get(9));
      inMap.put("postflag", inputList.get(10));
      inMap.put("location_id", inputList.get(11));
      inMap.put("study_mode", inputList.get(12));
      inMap.put("course_duration", inputList.get(13));
      inMap.put("lucky", inputList.get(14));
      inMap.put("crs_search", inputList.get(15));
      inMap.put("submit", inputList.get(16));
      inMap.put("a", inputList.get(17));
      inMap.put("search_how", inputList.get(18));
      inMap.put("s_type", inputList.get(19));
      inMap.put("search_category", inputList.get(20));
      inMap.put("search_career", inputList.get(21));
      inMap.put("career_id", inputList.get(22));
      inMap.put("ref_id", inputList.get(23));
      inMap.put("nrp", inputList.get(24));
      inMap.put("area", inputList.get(25));
      inMap.put("p_pg_type", inputList.get(26));
      inMap.put("p_search_col", inputList.get(27));
      inMap.put("p_county_id", inputList.get(28));
      inMap.put("p_entity_text", inputList.get(29));
      inMap.put("p_search_where", inputList.get(30));
      inMap.put("p_user_agent", inputList.get(31));
      inMap.put("p_search_browse_link", inputList.get(32));
      inMap.put("p_ucas_code", inputList.get(33));
      inMap.put("p_deemed_uni", inputList.get(34));
      inMap.put("p_search_action", inputList.get(35));
      inMap.put("p_page_no", inputList.get(36));
      inMap.put("p_basket_id", inputList.get(37));
      inMap.put("p_search_cat_id", inputList.get(38));
      inMap.put("p_page_name", inputList.get(39));
      inMap.put("p_entry_level", inputList.get(40));
      inMap.put("p_entry_points", inputList.get(41));
      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    if (TimeTrackingConstants.DB_CALLS_PARAMETERS_TRACKING_FLAG) {
      new AdminUtilities().logDbCallParameterDetails("WHATUNI_SEARCH3.GET_MONEY_PAGE_MAIN_CONTENT", inMap);
    }
    return outMap;
  }

  /**
   * this row mapper used to map refine by pod's content detalis
   */
  private class OmnitureLoggingRowMapper implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      OmnitureLoggingVO omnitureLoggingVO = new OmnitureLoggingVO();
      omnitureLoggingVO.setCollegeIdsForThisSearch(resultset.getString("college_ids"));
      return omnitureLoggingVO;
    }

  }

  /**
   * this row mapper used to map refine by pod's content detalis
   */
  private class RefineByOptionsRowMapper implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      RefineByOptionsVO refineByOptionsVO = new RefineByOptionsVO();
      refineByOptionsVO.setRefineOrder(resultset.getString("REFINE_ORDER"));
      refineByOptionsVO.setRefineCode(resultset.getString("REFINE_CODE"));
      refineByOptionsVO.setRefineDesc(resultset.getString("REFINE_DESC"));
      refineByOptionsVO.setCategoryId(resultset.getString("CATEGORY_ID"));
      refineByOptionsVO.setCategoryCode(resultset.getString("CATEGORY_CODE"));
      refineByOptionsVO.setDisplaySequence(resultset.getString("DISP_SEQ"));
      refineByOptionsVO.setParentCountryName(resultset.getString("PARENT_COUNTRY_NAME"));
      refineByOptionsVO.setNumberOfChildLocations(resultset.getString("CHILD_LOCATION_COUNT"));
      return refineByOptionsVO;
    }

  }

    private class DBExecutionTimeRowImpl implements RowMapper {

      public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
        DBExecutionTimeVO dBExecutionTimeVO = new DBExecutionTimeVO();
        dBExecutionTimeVO.setFucntionName(resultset.getString("name"));
        dBExecutionTimeVO.setTimeTaken(resultset.getString("time_taken"));
        return dBExecutionTimeVO;
      }
    }
    private class SnipetTxtRowImpl implements RowMapper {

      public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
        CourseSpecificSearchResultsVO courseSpecificSearchResultsVO = new CourseSpecificSearchResultsVO();
        courseSpecificSearchResultsVO.setSnipetText(resultset.getString("snippet_text"));
        return courseSpecificSearchResultsVO;
      }
    }
  

}
