package com.layer.dao.sp.search;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.seo.PageMetaTagsRowMapperImpl;
import spring.valueobject.search.TimesRankingVO;
import WUI.review.bean.ReviewListBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;

import com.layer.util.rowmapper.advert.EnquiryInfoRowMapperImpl;
import com.layer.util.rowmapper.review.CommonPhrasesRowMapperImpl;
import com.layer.util.rowmapper.review.ReviewBreakDownRowMapperImpl;
import com.layer.util.rowmapper.search.EntryPointsForUCASCourseRowMapperImpl;
import com.layer.util.rowmapper.search.ModuleListRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.advert.SpListVO;
import com.wuni.util.valueobject.review.ReviewSubjectVO;
import com.wuni.util.valueobject.search.CourseDetailsVO;
import com.wuni.util.valueobject.search.CourseKISDataVO;
import com.wuni.util.valueobject.search.LocationsInfoVO;
import com.wuni.util.valueobject.search.TopCoursesVO;
import com.wuni.util.valueobject.search.UniSpecificSearchResultsVO;

/**
  * will return course-details page info
  * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-details/[COURSE_TITLE]-courses-details/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
  *
  *
  * @since        wu318_20111020 - redesign
  * @author       Mohamed Syed
  *
  * @param inputList
  * @return
	* ********************************************************************************************************************************
	* Author	                 Ver 	           Modified On     	Modification Details 	               Change
	* *******************************************************************************************************************************
	* Prabhakaran V.           wu_548          16.12.2015       16.12.2015_REL                        Change the read more url change
	* Indumathi.S              wu_558          15.11.2016       15.11.2016_REL                        Added a check for mediaType 17 and 16 in SpList
    * Prabhakaran V.           wu_566          03.07.2017       11.07.2017_REL                        Added p_part_time_msg_flag for showing message on UCAS part time course
    * Hema.S                   wu_573          02.02.2018       02.02.2018_REL                        Modified fees columns for new UCAS Fees Structure
    * Hema.S                   wu_577          06.06.2018       06.06.2018_REL                        Modified entry requirments columns for new structure 
    * Hema.S                   wu_584          18.12.2018       18.12.2018_REL                        Added review subject and breakdown cursor
	* Sangeeth.S							   15.06.2020		23.02.2020_REL						  Added switch related columns
 */
public class CourseDetailsInfoSP extends StoredProcedure {
  public CourseDetailsInfoSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_COURSE_DETAILS_PRC);
    //
    declareParameter(new SqlParameter("P_COURSE_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_opportunity_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_SESSION_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_BASKET_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_pagename", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_META_PAGE_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_META_PAGE_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_USER_AGENT", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_TRACK_SESSION_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("oc_course_info", OracleTypes.CURSOR, new CourseDetailsDsktpRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_enquiry_info", OracleTypes.CURSOR, new EnquiryInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_module_info", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("oc_module_details", OracleTypes.CURSOR, new ModuleListRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_ucas_course_details", OracleTypes.CURSOR, new EntryPointsForUCASCourseRowMapperImpl()));//Modified by Hema.S on 06-june-2018 for entry requirments structure change
    declareParameter(new SqlOutParameter("oc_uni_sp_list", OracleTypes.CURSOR, new SpListRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_UNI_INFO_FN", OracleTypes.CURSOR, new UniProfileInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_REVIEW_LIST_FN", OracleTypes.CURSOR, new ReviewLstRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_OPPORTUNITY_DETAILS", OracleTypes.CURSOR, new OppDropDownRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_KEY_STATS_INFO", OracleTypes.CURSOR, new KeyStatsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_UNI_RANKING_FN", OracleTypes.CURSOR, new UniRankningRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_GET_COURSE_DURATIONS", OracleTypes.CURSOR, new GetCourseDurationRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_GET_ASSESSED_COURSE", OracleTypes.CURSOR, new GetAssessesCourseRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_VENUE_DETAILS", OracleTypes.CURSOR, new LocationsInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_PREVIOUS_STUDY_SUBJS", OracleTypes.CURSOR, new PrvisstdySubjsRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_OTHER_COURSES_POD", OracleTypes.CURSOR, new TopCoursesRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_APPLICANTS_DATA", OracleTypes.CURSOR, new ApplicantsDataRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_FEES_DATA", OracleTypes.CURSOR, new FeesDataRowMapperImpl()));//Modified by Hema.S on 20-feb-2018 for New UCAS fees Structure 
    declareParameter(new SqlOutParameter("O_ADVERTISER_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_OPP_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_OPP_DET", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("OC_PAGE_META_TAGS", OracleTypes.CURSOR, new PageMetaTagsRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_college_logo", OracleTypes.VARCHAR));  //Added non advisor provider's logo to show in registeration lightbox for 24_Nov_2015, by Thiyagu G. 
    declareParameter(new SqlOutParameter("p_degree_course_flag", OracleTypes.VARCHAR));//Added by Indumathi.S Feb-16-16
    //Added course tyoe flag and update date by Thiyagu G.S for 08_MAR_2016 Start
    declareParameter(new SqlOutParameter("p_course_type", OracleTypes.VARCHAR)); 
    declareParameter(new SqlOutParameter("p_course_last_updated_date", OracleTypes.VARCHAR));   
    declareParameter(new SqlOutParameter("p_opp_last_updated_date", OracleTypes.VARCHAR));    
    //Added course tyoe flag and update date by Thiyagu G.S for 08_MAR_2016 End
    declareParameter(new SqlOutParameter("p_part_time_msg_flag", OracleTypes.VARCHAR)); //Added flag for showing message on part time course by Prabha on 11_Jul_17
     declareParameter(new SqlOutParameter("p_ldcs_descriptions", OracleTypes.VARCHAR)); //Added subject by Prabha on 05_Sep_17
     declareParameter(new SqlOutParameter("PC_REVIEW_BREAKDOWN",OracleTypes.CURSOR, new ReviewBreakDownRowMapperImpl()));
     declareParameter(new SqlOutParameter("PC_SUB_REVIEW_BREAKDOWN", OracleTypes.CURSOR, new ReviewBreakDownRowMapperImpl()));
      declareParameter(new SqlOutParameter("PC_COMMON_PHRASES", OracleTypes.CURSOR, new CommonPhrasesRowMapperImpl()));
      declareParameter(new SqlOutParameter("PC_CRS_REVIEW_SUB", OracleTypes.CURSOR, new CourseReviewSubjectRowMapperImpl()));
      declareParameter(new SqlOutParameter("p_show_apply_now_button", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_user_subjgrades_session_id", OracleTypes.VARCHAR));
      declareParameter(new SqlOutParameter("P_QUALIFICATION_CODE", OracleTypes.VARCHAR));
      declareParameter(new SqlOutParameter("P_SUBJECT_NAME", OracleTypes.VARCHAR));
      declareParameter(new SqlOutParameter("P_CLEARING_SWITCH_FLAG", OracleTypes.VARCHAR));  
  compile();
  }
  public Map execute(List inputList) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("P_COURSE_ID", inputList.get(0));
      inMap.put("P_COLLEGE_ID", inputList.get(1));
      inMap.put("p_opportunity_id", inputList.get(2));
      inMap.put("P_USER_ID", inputList.get(4));
      inMap.put("P_SESSION_ID", inputList.get(3));
      inMap.put("P_BASKET_ID", inputList.get(5));
      inMap.put("p_pagename", inputList.get(6));
      inMap.put("P_META_PAGE_NAME", inputList.get(7));
      inMap.put("P_META_PAGE_FLAG", inputList.get(8));
      inMap.put("P_USER_AGENT", inputList.get(9));
      inMap.put("P_TRACK_SESSION_ID", inputList.get(10));   
      inMap.put("p_user_subjgrades_session_id", inputList.get(11));
      //
      outMap = execute(inMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  
    return outMap;
  }
  public class LocationsInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      LocationsInfoVO locationsInfoVO = new LocationsInfoVO();
      //
      locationsInfoVO.setLatitude(resultset.getString("latitude"));
      locationsInfoVO.setLongitude(resultset.getString("longitude"));
      locationsInfoVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      locationsInfoVO.setTown(resultset.getString("town"));
      locationsInfoVO.setPostcode(resultset.getString("postcode"));
      locationsInfoVO.setNearestStationName(resultset.getString("nearest_train_stn"));
      locationsInfoVO.setNearestTubeStation(resultset.getString("nearest_tube_stn"));
      locationsInfoVO.setDistanceFromNearestStation(resultset.getString("distance_from_train_stn"));
      locationsInfoVO.setDistanceFromTubeStation(resultset.getString("distance_from_tube_stn"));
      locationsInfoVO.setAddLine1(resultset.getString("add_line_1"));
      locationsInfoVO.setAddLine2(resultset.getString("add_line_2"));
      locationsInfoVO.setCountryState(resultset.getString("country_state"));
      //Changing is in london to uppercase by Prabha on 17_05_2018
      if(!GenericValidator.isBlankOrNull(resultset.getString("is_in_london"))){
        locationsInfoVO.setIsInLondon(resultset.getString("is_in_london").toUpperCase());
      }
      //End of code
      locationsInfoVO.setCityGuideDisplayFlag(resultset.getString("CITY_GUIDE_DISPLAY_FLAG"));
      locationsInfoVO.setLocation(resultset.getString("location"));
      locationsInfoVO.setCityGuideUrl(new SeoUrls().constructAdviceSeoUrl(resultset.getString("article_category"), resultset.getString("post_url"), resultset.getString("article_id")));
      //
      return locationsInfoVO;
    }
  }
  //
  public class GetAssessesCourseRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseKISDataVO kisDataVO = new CourseKISDataVO();
      kisDataVO.setExam(resultset.getString("exam"));
      kisDataVO.setCourseWork(resultset.getString("course_work"));
      kisDataVO.setPracticalWork(resultset.getString("practical_work"));
      return kisDataVO;
    }
  }
  //
  public class GetCourseDurationRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseDetailsVO courseDetailsVO = new CourseDetailsVO();
      courseDetailsVO.setStudyModes(resultset.getString("description"));
      courseDetailsVO.setDurations(resultset.getString("duration_desc"));
      courseDetailsVO.setStartDate(resultset.getString("start_date"));
      courseDetailsVO.setShortStartDate(resultset.getString("short_text"));
      return courseDetailsVO;
    }
  }
  //
  public class UniRankningRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      TimesRankingVO timesRankingVO = new TimesRankingVO();
      timesRankingVO.setTimesRanking(resultset.getString("times_ranking"));
      String uniRanking = "";
      uniRanking = !GenericValidator.isBlankOrNull(timesRankingVO.getTimesRanking()) ? timesRankingVO.getTimesRanking().substring(0,timesRankingVO.getTimesRanking().length()-2) : "";
      String subScript = "";
      subScript = !GenericValidator.isBlankOrNull(timesRankingVO.getTimesRanking()) ? timesRankingVO.getTimesRanking().substring(timesRankingVO.getTimesRanking().length()-2) : "";
      timesRankingVO.setUniTimesRanking(uniRanking);
      timesRankingVO.setThAndRdValue(subScript);
      timesRankingVO.setStudentRanking(resultset.getString("student_ranking"));
      timesRankingVO.setTotalStudentRanking(resultset.getString("total_student_ranking"));
      ResultSet timesRankingRs = (ResultSet)resultset.getObject("times_subject_ranking");
      ArrayList timeRankingList = null;
      try {
        if (timesRankingRs != null) {
          timeRankingList = new ArrayList();
          while (timesRankingRs.next()) {
            TimesRankingVO timeRankingVO = new TimesRankingVO();
            timeRankingVO.setSubjectName(timesRankingRs.getString("TR_SUBJECT_NAME"));
            timeRankingVO.setCugSubjectRank(timesRankingRs.getString("CURRENT_RANKING"));
            String cugRanking = "";
            cugRanking = !GenericValidator.isBlankOrNull(timeRankingVO.getCugSubjectRank()) ? timeRankingVO.getCugSubjectRank().substring(0,timeRankingVO.getCugSubjectRank().length()-2) : "";
            String superScript = "";
            superScript = !GenericValidator.isBlankOrNull(timeRankingVO.getCugSubjectRank()) ? timeRankingVO.getCugSubjectRank().substring(timeRankingVO.getCugSubjectRank().length()-2) : "";
            timeRankingVO.setCurrentRanking(cugRanking);
            timeRankingVO.setThAndRdValue(superScript);
            timeRankingList.add(timeRankingVO);
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (timesRankingRs != null) {
            timesRankingRs.close();
            timesRankingRs = null;
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      timesRankingVO.setTimesSubRankingList(timeRankingList);
      return timesRankingVO;
    }
  }
  public class KeyStatsInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseKISDataVO kisDataVO = new CourseKISDataVO();
      kisDataVO.setSubjectName(resultset.getString("subject_name"));
      kisDataVO.setTwoistooneRatio(resultset.getString("ratio"));
      kisDataVO.setDropOutRate(resultset.getString("drop_out_rate"));
      kisDataVO.setGraduates(resultset.getString("graduates_percent"));
      kisDataVO.setAvgSalaryAfterSixMonths(resultset.getString("salary_6_months"));
      kisDataVO.setAvgSalaryAfterFourtyMonths(resultset.getString("salary_40_months"));
      kisDataVO.setAvgSalAfterSixMonthsAtuni(resultset.getString("salary_6_months_at_uni"));
      kisDataVO.setPercentageSixMonths(resultset.getString("salary_6_months_per"));
      kisDataVO.setPercentage40Months(resultset.getString("salary_40_months_per"));
      kisDataVO.setPercentageSixMonthsAtUni(resultset.getString("salary_6_months_at_uni_per"));
      //Added three columns to show the drop-out rate calculation in tooltip By Thiyagu G on 24_jan_2017
      kisDataVO.setContinuingCourse(resultset.getString("continuing_course")); 
      kisDataVO.setCompletedCourse(resultset.getString("completed_course"));
      kisDataVO.setCourseContinuation(resultset.getString("course_continuation"));
      return kisDataVO;
    }
  }
  public class OppDropDownRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      CourseKISDataVO oppListVO = new CourseKISDataVO();
      oppListVO.setOpportunityId(rs.getString("opportunity_id"));
      oppListVO.setOpportunityDesc(rs.getString("opportunity"));
      return oppListVO;
    }
  }
  public class ReviewLstRowMapperImpl implements RowMapper {//Modified column names for review redesign by Hema.S on 18_DEC_2018
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewListBean reviewBean = new ReviewListBean();
      reviewBean.setReviewId(rs.getString("review_id"));
      reviewBean.setReviewerName(rs.getString("USER_NAME"));
      reviewBean.setCollegeId(rs.getString("college_id"));
      reviewBean.setCourseId(rs.getString("course_id"));
      reviewBean.setOverAllRating(rs.getString("overall_rating"));
      reviewBean.setOverallRatingComment(rs.getString("REVIEW_TEXT"));
      reviewBean.setCollegeName(rs.getString("college_name"));  
      String reviewUrl = new SeoUrls().constructReviewPageSeoUrl(reviewBean.getCollegeName(), reviewBean.getCollegeId());
      reviewUrl = reviewUrl+ "?reviewId="+reviewBean.getReviewId();
      reviewBean.setReviewDetailUrl(reviewUrl);
      reviewBean.setCourseTitle(rs.getString("COURSE_TITLE"));
      reviewBean.setReviewDate(rs.getString("review_date"));  
      reviewBean.setUserNameInitial(rs.getString("user_name_int"));  
      reviewBean.setCollegeName(rs.getString("college_name"));
      if((!GenericValidator.isBlankOrNull(reviewBean.getCourseId()) || !"0".equals(reviewBean.getCourseId()))){
        reviewBean.setCourseDetailsReviewURL(new SeoUrls().courseDetailsPageURL(reviewBean.getCourseTitle(), reviewBean.getCourseId(), reviewBean.getCollegeId()));
      }
      reviewBean.setCourseDeletedFlag(rs.getString("course_deleted_flag"));
      return reviewBean;
    }
  }
  public class CourseDetailsDsktpRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseDetailsVO courseDetailsVO = new CourseDetailsVO();
      SeoUrls seoUrl = new SeoUrls();
      //
      courseDetailsVO.setCollegeId(resultset.getString("college_id"));
      courseDetailsVO.setCourseSummaryFull(resultset.getString("course_summary"));
      courseDetailsVO.setCourseSummary(resultset.getString("cut_off_course_summary"));
      courseDetailsVO.setCourseTitle(resultset.getString("course_title"));
      courseDetailsVO.setUcasCode(resultset.getString("ucas_code"));
      courseDetailsVO.setCourseId(resultset.getString("course_id"));
      courseDetailsVO.setCollegeName(resultset.getString("college_name"));
      courseDetailsVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      courseDetailsVO.setDepartmentOrFaculty(resultset.getString("dept_name"));
      courseDetailsVO.setWasThisCourseShortlisted(resultset.getString("course_in_basket"));
      courseDetailsVO.setUniHomeUrl(seoUrl.construnctUniHomeURL(courseDetailsVO.getCollegeId(), courseDetailsVO.getCollegeName()));
      courseDetailsVO.setMyhcProfileId(resultset.getString("myhc_profile_id"));
      courseDetailsVO.setOverAllRatingExact(resultset.getString("overall_rating_exact"));
      courseDetailsVO.setOverAllRating(resultset.getString("overall_rating"));
      courseDetailsVO.setReviewCount(resultset.getString("review_count"));
      courseDetailsVO.setNetworkId(resultset.getString("network_id"));
      courseDetailsVO.setCollegeLocation(resultset.getString("college_location"));
      courseDetailsVO.setProfileURL(seoUrl.construnctSpURL(courseDetailsVO.getCollegeId(), courseDetailsVO.getCollegeName(), courseDetailsVO.getMyhcProfileId(), "", courseDetailsVO.getNetworkId(), ""));
      courseDetailsVO.setReviewURL("/degrees/university-course-reviews/highest-rated/" + courseDetailsVO.getCollegeId() + "/0/1/reviews.html");
      courseDetailsVO.setStudyModes(resultset.getString("study_mode"));
      courseDetailsVO.setStudyLevelCode(resultset.getString("qual_code"));
      courseDetailsVO.setAddedToFinalChoice(resultset.getString("final_choices_exist_flag")); 
      return courseDetailsVO;
    }
  }
  public class UniProfileInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
	 SeoUrls seoUrl = new SeoUrls();
      UniSpecificSearchResultsVO uniSpecificSearchResultsVO = new UniSpecificSearchResultsVO();
      uniSpecificSearchResultsVO.setCollegeId(resultset.getString("college_id"));
      uniSpecificSearchResultsVO.setCollegeName(resultset.getString("college_name").toLowerCase());
      uniSpecificSearchResultsVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      uniSpecificSearchResultsVO.setOverallRating(resultset.getString("overall_rating"));
      uniSpecificSearchResultsVO.setOverallRatingExact(resultset.getString("overall_rating_exact"));
      uniSpecificSearchResultsVO.setTimesRanking(resultset.getString("times_ranking"));
      uniSpecificSearchResultsVO.setNextOpenDay(resultset.getString("next_open_date"));
      uniSpecificSearchResultsVO.setOpenDayUrl(resultset.getString("open_day_url"));
      uniSpecificSearchResultsVO.setAwardImagePath(resultset.getString("award_image_det"));
      uniSpecificSearchResultsVO.setUniHomeUrl(seoUrl.construnctUniHomeURL(uniSpecificSearchResultsVO.getCollegeId(), uniSpecificSearchResultsVO.getCollegeName()));
      uniSpecificSearchResultsVO.setClearingProfileURL(seoUrl.construnctProfileURL(uniSpecificSearchResultsVO.getCollegeId(),uniSpecificSearchResultsVO.getCollegeName(),GlobalConstants.CLEARING_PAGE_FLAG));
      //System.out.println(uniSpecificSearchResultsVO.getUniHomeUrl() + "url=======================");
      
      ResultSet interactionDetails = (ResultSet)resultset.getObject("enquiry_info");
      try {
        while (interactionDetails.next()) {
          String tmpUrl = null;
          uniSpecificSearchResultsVO.setOrderItemId(interactionDetails.getString("order_item_id"));
          uniSpecificSearchResultsVO.setSubOrderEmail(interactionDetails.getString("email"));
          uniSpecificSearchResultsVO.setSubOrderEmailWebform(interactionDetails.getString("email_webform"));
          uniSpecificSearchResultsVO.setSubOrderProspectus(interactionDetails.getString("prospectus"));
          uniSpecificSearchResultsVO.setSubOrderProspectusWebform(interactionDetails.getString("prospectus_webform"));
          uniSpecificSearchResultsVO.setSubOrderItemId(interactionDetails.getString("suborder_item_id"));
          uniSpecificSearchResultsVO.setMyhcProfileId(interactionDetails.getString("myhc_profile_id"));
          uniSpecificSearchResultsVO.setCpeQualificationNetworkId(interactionDetails.getString("network_id"));
          uniSpecificSearchResultsVO.setMediaId(interactionDetails.getString("media_id"));
          uniSpecificSearchResultsVO.setMediaPath(interactionDetails.getString("media_path"));
          //
          uniSpecificSearchResultsVO.setImagePath("");
          String dataArray[] = null;
          String durationArray[] = null;
          if (resultset.getString("media_path") != null && resultset.getString("media_path").trim().length() > 0) {
            dataArray = resultset.getString("media_path").split("###", 3);
          }
          String imagePath = "", thumbnailPath = "";
          int lenth = dataArray != null? dataArray.length: 0;
          if (lenth == 1) {
            imagePath = dataArray[0];
            uniSpecificSearchResultsVO.setImagePath(imagePath);
          } else if (lenth > 1) {
            uniSpecificSearchResultsVO.setVideoId(dataArray[0]);
            uniSpecificSearchResultsVO.setVideoUrl(dataArray[1]);
            if ( (uniSpecificSearchResultsVO.getVideoUrl().indexOf(".flv")  > -1) ||  (uniSpecificSearchResultsVO.getVideoUrl().indexOf(".mp4")>-1)   ) {
              String videoDuration = dataArray[2];
              if (!GenericValidator.isBlankOrNull(videoDuration)) {
                durationArray = videoDuration.split(":", 2);
                int length1 = durationArray != null? durationArray.length: 0;
                if (length1 > 1) {
                  String minutes = durationArray[0];
                  String seconds = durationArray[1];
                  uniSpecificSearchResultsVO.setVideoMetaDuration("T" + minutes + "M" + seconds + "S");
                }
              }
            }
            if (resultset.getString("media_type_id") != null && "19".equals(resultset.getString("media_type_id"))) {
              thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(dataArray[0], "7"), rowNum);
            } else {
              thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(dataArray[0], "4"), rowNum); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path. 
            }
            uniSpecificSearchResultsVO.setVideoThumbnailUrl(thumbnailPath);
          }
          uniSpecificSearchResultsVO.setAdvertName(interactionDetails.getString("advert_name"));
          tmpUrl = interactionDetails.getString("website");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          uniSpecificSearchResultsVO.setSubOrderWebsite(tmpUrl);
          tmpUrl = interactionDetails.getString("email_webform");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          uniSpecificSearchResultsVO.setSubOrderEmailWebform(tmpUrl);
          tmpUrl = interactionDetails.getString("prospectus_webform");
          if (tmpUrl != null && tmpUrl.trim().length() > 0) {
            tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
          } else {
            tmpUrl = "";
          }
          uniSpecificSearchResultsVO.setSubOrderProspectusWebform(tmpUrl);
        }
        uniSpecificSearchResultsVO.setCollegeLocation(resultset.getString("college_location"));
      } catch (Exception ex) {
        ex.printStackTrace();
      } finally {
        interactionDetails.close();
      }
      uniSpecificSearchResultsVO.setQuestionID(resultset.getString("question_title"));
      uniSpecificSearchResultsVO.setProfileDesc(resultset.getString("profile_text"));
      uniSpecificSearchResultsVO.setMediaId(resultset.getString("media_id"));
      uniSpecificSearchResultsVO.setMediaTypeId(resultset.getString("media_type_id"));
      uniSpecificSearchResultsVO.setMediaPath(resultset.getString("media_path"));
      return uniSpecificSearchResultsVO;
    }
  }
  public class PrvisstdySubjsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseKISDataVO kissubjectdate = new CourseKISDataVO();
      kissubjectdate.setKisSubject(resultset.getString("kis_subject_name"));
      ResultSet kisSubjectrs = (ResultSet)resultset.getObject("kis_subjects");
      List subjectList = new ArrayList();
      try {
        if (kisSubjectrs != null) {
          while (kisSubjectrs.next()) {
            CourseKISDataVO kissubjectvo = new CourseKISDataVO();
            kissubjectvo.setKisSubject(kisSubjectrs.getString("held_qualification_subject"));
            kissubjectvo.setHeldByPercentage(kisSubjectrs.getString("derived_held_percentage"));
            subjectList.add(kissubjectvo);
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          kisSubjectrs.close();
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      kissubjectdate.setSubjectList(subjectList);
      return kissubjectdate;
    }
  }
  public class SpListRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      SpListVO spListVO = new SpListVO();
      SeoUrls seoUrl = new SeoUrls();
      //
      spListVO.setCollegeId(resultset.getString("college_id"));
      spListVO.setCollegeName(resultset.getString("college_name"));
      spListVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      spListVO.setAdvertName(resultset.getString("advert_name"));
      spListVO.setAdvertDescShort(resultset.getString("advert_short_desc"));
      spListVO.setMyhcProfileId(resultset.getString("myhc_profile_id"));
      spListVO.setCpeQualificationNetworkId(resultset.getString("network_id"));
      spListVO.setProfileId("0"); //"resultset.getString("profile_id"));
      spListVO.setSectionDesc("overview"); //resultset.getString("section_desc"))
      //
      spListVO.setImagePath(resultset.getString("image_path"));
      if (!GenericValidator.isBlankOrNull(spListVO.getImagePath())) {
        String splitMediaPath = spListVO.getImagePath().substring(0, spListVO.getImagePath().lastIndexOf("."));
        String mediaTypeId = resultset.getString("media_type_id");
        if ("1".equals(mediaTypeId) || "17".equals(mediaTypeId)) {//Added a check for mediaType 17 and 16 by Indumathi.S Nov_15_2016
          spListVO.setImagePath(new CommonUtil().getImgPath((splitMediaPath + "_116px.jpg"), rowNum));
        } else if ("2".equals(mediaTypeId) || "16".equals(mediaTypeId)) {
          spListVO.setImagePath(new CommonUtil().getImgPath((splitMediaPath + "_140px.jpg"), rowNum));
        } else if ("18".equals(mediaTypeId) || "19".equals(mediaTypeId)) {
          spListVO.setImagePath("");
        }
        
      }
      spListVO.setSpURL(seoUrl.construnctSpURL(spListVO.getCollegeId(), spListVO.getCollegeName(), spListVO.getMyhcProfileId(), spListVO.getProfileId(), spListVO.getCpeQualificationNetworkId(), spListVO.getSectionDesc()));
      //
      return spListVO;
    }
  }
  public class ApplicantsDataRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseKISDataVO courseDetailKisVO = new CourseKISDataVO();
      courseDetailKisVO.setTotalApplicants(resultset.getString("total_applicants"));
      courseDetailKisVO.setTotalApplicantsStickMen(resultset.getString("applicants_val"));
      courseDetailKisVO.setSuccessfulApplicants(resultset.getString("successful_applicants"));
      courseDetailKisVO.setSuccessfulApplicantsStickMen(resultset.getString("offers_val"));
      courseDetailKisVO.setApplicantsRate(resultset.getString("applicant_successrate"));
      return courseDetailKisVO;
    }
  }
  public class FeesDataRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseKISDataVO courseDetailKisVO = new CourseKISDataVO();
      courseDetailKisVO.setFeesType(resultset.getString("fee_type"));
      courseDetailKisVO.setFeesTypeTooltip("Rest of World".equalsIgnoreCase(resultset.getString("fee_type")) ? "International" : resultset.getString("fee_type"));
      courseDetailKisVO.setFees(resultset.getString("fee"));
      courseDetailKisVO.setDuration(resultset.getString("duration"));
      courseDetailKisVO.setCurrency(resultset.getString("currency"));
      courseDetailKisVO.setFeesDesc(resultset.getString("fee_desc"));
      courseDetailKisVO.setState("free".equalsIgnoreCase(resultset.getString("state")) ? resultset.getString("state").toUpperCase() : resultset.getString("state"));
      courseDetailKisVO.setUrl(resultset.getString("url"));
      courseDetailKisVO.setSubOrderItemId(resultset.getString("suborder_item_id"));
      courseDetailKisVO.setNetworkId(resultset.getString("network_id"));
      courseDetailKisVO.setSequenceNo(resultset.getString("seq_no"));
      return courseDetailKisVO;
    }
  }
  //
  public class TopCoursesRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      TopCoursesVO topCoursesVO = new TopCoursesVO();
      SeoUrls seoUrl = new SeoUrls();
      topCoursesVO.setCollegeId(resultset.getString("college_id"));
      topCoursesVO.setCollegeName(resultset.getString("college_name"));
      topCoursesVO.setCollegeNameDisplay(resultset.getString("college_name_display"));
      topCoursesVO.setCourseId(resultset.getString("course_id"));
      topCoursesVO.setCourseTitle(resultset.getString("course_title"));
      topCoursesVO.setStudyLevelDesc(resultset.getString("study_level_desc"));
      topCoursesVO.setCourseDetailsPageURL(seoUrl.construnctCourseDetailsPageURL(topCoursesVO.getStudyLevelDesc(), topCoursesVO.getCourseTitle(), topCoursesVO.getCourseId(), topCoursesVO.getCollegeId(), resultset.getString("p_page_name")));
      topCoursesVO.setCount(resultset.getString("cnt"));
      topCoursesVO.setWebsitePrice(resultset.getString("website_price"));
      topCoursesVO.setWebformPrice(resultset.getString("webform_price"));
      topCoursesVO.setShortListFlag(resultset.getString("course_in_basket"));
      topCoursesVO.setGaCollegeName(new CommonFunction().replaceSpecialCharacter(topCoursesVO.getCollegeName()));
      ResultSet enquiryDetailsRS = (ResultSet)resultset.getObject("enquiry_details");
      try {
        if (enquiryDetailsRS != null) {
          while (enquiryDetailsRS.next()) {
            String tmpUrl = null;
            topCoursesVO.setOrderItemId(enquiryDetailsRS.getString("order_item_id"));
            topCoursesVO.setMyhcProfileId(enquiryDetailsRS.getString("myhc_profile_id"));
            topCoursesVO.setSubOrderItemId(enquiryDetailsRS.getString("suborder_item_id"));
            topCoursesVO.setProfileType(enquiryDetailsRS.getString("profile_type"));
            topCoursesVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("network_id"));
            topCoursesVO.setAdvertName(enquiryDetailsRS.getString("advert_name"));
            topCoursesVO.setSubOrderEmail(enquiryDetailsRS.getString("email"));
            topCoursesVO.setSubOrderProspectus(enquiryDetailsRS.getString("prospectus"));
            tmpUrl = enquiryDetailsRS.getString("website");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            topCoursesVO.setSubOrderWebsite(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("email_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            topCoursesVO.setSubOrderEmailWebform(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("prospectus_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            topCoursesVO.setSubOrderProspectusWebform(tmpUrl);
            topCoursesVO.setMediaPath(enquiryDetailsRS.getString("media_path"));
            String splitMediaThumbPath = "";
            topCoursesVO.setMediaThumbPath(enquiryDetailsRS.getString("thumb_media_path")); 
            if (!GenericValidator.isBlankOrNull(topCoursesVO.getMediaThumbPath())) {
              splitMediaThumbPath = topCoursesVO.getMediaThumbPath().substring(0, topCoursesVO.getMediaThumbPath().lastIndexOf("."));
              topCoursesVO.setMediaThumbPath(new CommonUtil().getImgPath((splitMediaThumbPath + ".jpg"), rowNum));
            }else{
              if(!GenericValidator.isBlankOrNull(topCoursesVO.getMediaPath())){
                if (enquiryDetailsRS.getString("media_type_id") != null && "16".equals(enquiryDetailsRS.getString("media_type_id"))) {                
                  topCoursesVO.setMediaThumbPath(new CommonUtil().getImgPath((topCoursesVO.getMediaPath().replace(".", "_140px.")), rowNum));
                } else {                
                  topCoursesVO.setMediaThumbPath(new CommonUtil().getImgPath((topCoursesVO.getMediaPath().replace(".", "_116px.")), rowNum));
                } 
              }
            }
            topCoursesVO.setMediaType(enquiryDetailsRS.getString("media_type"));
            topCoursesVO.setMediaId(enquiryDetailsRS.getString("media_id"));
            if ("VIDEO".equalsIgnoreCase(topCoursesVO.getMediaType())) {
              //topCoursesVO.setVideoThumbPath(new GlobalFunction().videoThumbnailFormatChange(topCoursesVO.getMediaPath(), "2"));
              String thumbnailPath = new CommonUtil().getImgPath(new GlobalFunction().videoThumbnail(topCoursesVO.getMediaId(), "2"), rowNum); //03_JUNE_2013 changing the limepath to appserver path for reffering vidothumbnail path.
              topCoursesVO.setVideoThumbPath(thumbnailPath);
            }
            topCoursesVO.setHotLineNo(enquiryDetailsRS.getString("hotline"));
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          enquiryDetailsRS.close();
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      if ("CLEARING_COURSE".equalsIgnoreCase(resultset.getString("p_page_name")) || "CLEARING".equalsIgnoreCase(resultset.getString("p_page_name"))) {
        topCoursesVO.setUniHomeURL( (seoUrl.construnctUniHomeURL(topCoursesVO.getCollegeId(), topCoursesVO.getCollegeName()).toLowerCase())+"?clearing"  );
      } else {
        topCoursesVO.setUniHomeURL(seoUrl.construnctUniHomeURL(topCoursesVO.getCollegeId(), topCoursesVO.getCollegeName()).toLowerCase());
      }
      return topCoursesVO;
    }
  }
  
    //Added this rowmapper for review Redesign by Hema.S on 18_DEC_2018_REL
    public class CourseReviewSubjectRowMapperImpl implements RowMapper {
      public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
        ReviewSubjectVO reviewSubjectVO = new ReviewSubjectVO();
        reviewSubjectVO.setCategoryCode(resultset.getString("CATEGORY_CODE"));
        reviewSubjectVO.setSubjectName(resultset.getString("SUBJECT_NAME"));
        return reviewSubjectVO;
      }
    }
}
