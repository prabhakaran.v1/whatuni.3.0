package com.layer.dao.sp.search.srgradefilter;

import WUI.whatunigo.bean.GradeFilterBean;
import com.layer.util.SpringConstants;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @see This SP class is used to call the database procedure to get the count of matching course based on subject and ucas points.
  * @since  22.11.2019- intital draft
  * @version 1.0
  *
  * Modification history:
  * *****************************************************************************************************************
  * Author          Relase tag       Modification Details                                                  Rel Ver.
  * *****************************************************************************************************************
  * Jeyalakshmi.D       1.0             initial draft                                                       wu_596
  *
  */

public class SRGetMatchingCourseSP extends StoredProcedure {
  DataSource dataSource;
  public SRGetMatchingCourseSP(DataSource dataSource) {
    setDataSource(dataSource);
    this.dataSource = dataSource;
    setSql(SpringConstants.SR_GET_COURSE_COUNT_FN);
    setFunction(true);
    declareParameter(new SqlOutParameter("P_COURSE_COUNT", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_SUBJECT_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_KEYWORD" , OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_QUALIFICATION_CODE", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_page_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_study_mode", Types.VARCHAR));
    declareParameter(new SqlParameter("P_LOCATION", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("p_search_range", Types.VARCHAR));
    declareParameter(new SqlParameter("p_univ_loc_type_name_str", Types.VARCHAR));
    declareParameter(new SqlParameter("p_campus_based_univ_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_russell_group_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_employment_rate", Types.VARCHAR));
    declareParameter(new SqlParameter("p_module_search_phrase", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_ucas_tariff_range", Types.VARCHAR));
    declareParameter(new SqlParameter("p_jacs_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_assessment_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_review_subject_str", Types.VARCHAR));   
    declareParameter(new SqlParameter("P_UCAS_POINTS", OracleTypes.NUMBER));
    compile();
  }
  public Map execute(GradeFilterBean gradeFilterBean) {
    Map outMap = null;
    HashMap inMap = new HashMap();
    //
    try{
      inMap.put("P_SUBJECT_NAME", gradeFilterBean.getSubjectName());
      inMap.put("P_KEYWORD", gradeFilterBean.getKeyword());
      inMap.put("P_COLLEGE_NAME", gradeFilterBean.getCollegeName());
      inMap.put("P_QUALIFICATION_CODE", gradeFilterBean.getQualificationCode());
      inMap.put("p_page_name", gradeFilterBean.getPageName());
      inMap.put("p_study_mode", gradeFilterBean.getStudyMode());
      inMap.put("P_LOCATION", gradeFilterBean.getLocation());
      inMap.put("p_postcode", gradeFilterBean.getPostcode());
      inMap.put("p_search_range", gradeFilterBean.getSearchRange());
      inMap.put("p_univ_loc_type_name_str", gradeFilterBean.getUnivLocTypeName());
      inMap.put("p_campus_based_univ_name", gradeFilterBean.getCampusTypeName());
      inMap.put("p_russell_group_name", gradeFilterBean.getRusselGroupName());
      inMap.put("p_employment_rate", gradeFilterBean.getEmpRate());
      inMap.put("p_module_search_phrase", gradeFilterBean.getModuleStr());
      inMap.put("p_ucas_code", gradeFilterBean.getUcasCode());
      inMap.put("p_ucas_tariff_range", gradeFilterBean.getUcasTariff());
      inMap.put("p_jacs_code", gradeFilterBean.getJacsCode());
      inMap.put("p_assessment_type", gradeFilterBean.getAssessmentType());
      inMap.put("p_review_subject_str", gradeFilterBean.getReviewSubjects());
      inMap.put("P_UCAS_POINTS", gradeFilterBean.getUserUcasScore());
      outMap = execute(inMap);
    } catch(Exception e){
      e.printStackTrace();
    }
    return outMap;
  }
  
}
