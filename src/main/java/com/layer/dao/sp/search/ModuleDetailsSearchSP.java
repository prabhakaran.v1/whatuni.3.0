package com.layer.dao.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.valueobjects.ModuleVO;

/**
  * will return course-details page info
  * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-details/[COURSE_TITLE]-courses-details/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
  *
  *
  *
  * @param inputList
  * @return
  *
  */
public class ModuleDetailsSearchSP extends StoredProcedure {

  public ModuleDetailsSearchSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql("WHATUNI_SEARCH_PKG.GET_MODULE_FN");
    //
    declareParameter(new SqlOutParameter("lc_module", OracleTypes.CURSOR, new ModuleDetailsSearchRowMapperImpl()));
    declareParameter(new SqlParameter("P_MODULE_GROUP_ID", Types.NUMERIC));   
  }

  public Map execute(String moduleGroupId) {
    Map outMap = null;
    try{
      Map inMap = new HashMap();
      inMap.put("P_MODULE_GROUP_ID", moduleGroupId);
      outMap = execute(inMap);
    }catch(Exception e){
      e.printStackTrace();
    }
    return outMap;
  }
  
  private class ModuleDetailsSearchRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ModuleVO moduleVO = new ModuleVO();
      moduleVO.setModuleId(rs.getString("module_id"));
      moduleVO.setModuleTitle(rs.getString("module_title"));
      return moduleVO;
    }
  }

}
