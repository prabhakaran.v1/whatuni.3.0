package com.layer.dao.sp.search;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * will return course-details page info
  * URL_PATTERN: www.whatuni.com/degrees/courses/[STUDY_LEVEL_DESC]-details/[COURSE_TITLE]-courses-details/[COURSE_ID]/[COLLEGE_ID]/cdetail.html
  *
  *
  *
  * @param inputList
  * @return
  *
  */
public class ArticleDetailSP extends StoredProcedure {

  public ArticleDetailSP(DataSource datasource){
    setDataSource(datasource);
    setSql("WU_ARTICLES_PKG.get_post_text_prc");   
    declareParameter(new SqlParameter("p_post_id", Types.NUMERIC));   
    declareParameter(new SqlOutParameter("p_headline", OracleTypes.VARCHAR));  
    declareParameter(new SqlOutParameter("p_article_full_desc", OracleTypes.CLOB));  
  }

  public Map execute(String postId) {
    Map outMap = null;
    try{
      Map inMap = new HashMap();
      inMap.put("p_post_id", postId);
      outMap = execute(inMap);
    }catch(Exception e){
      e.printStackTrace();
    }
    return outMap;
  }
}
