package com.layer.dao.sp.search;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.search.form.CourseDetailsBean;
import WUI.utilities.GlobalConstants;
/**
  * @GetCourseNameSP
  * @author Thiyagu G.
  * @version 1.0
  * @since 16.04.2017
  * @purpose This program is used get the course name by passing course id
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
  
public class GetCourseNameSP extends StoredProcedure {
  public GetCourseNameSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_COURSE_TITLE_FN);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_course_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_clearing_flag", Types.VARCHAR));
  }
  public Map execute(CourseDetailsBean courseDetailsBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_course_id", courseDetailsBean.getCourseId());
      inMap.put("p_clearing_flag", courseDetailsBean.getClearingFlag());      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}

