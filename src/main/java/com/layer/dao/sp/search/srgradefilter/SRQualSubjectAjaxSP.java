package com.layer.dao.sp.search.srgradefilter;

import WUI.whatunigo.bean.GradeFilterBean;
import com.layer.util.SpringConstants;
import com.wuni.util.sql.OracleArray;
import com.wuni.valueobjects.whatunigo.GradeFilterVO;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @see This SP class is used to call the database procedure to get the subject Ajax in grade filter page.
  * @since  22.11.2019- intital draft
  * @version 1.0
  *
  * Modification history:
  * *****************************************************************************************************************
  * Author          Relase tag       Modification Details                                                  Rel Ver.
  * *****************************************************************************************************************
  * Jeyalakshmi.D       1.0             initial draft                                                      wu_596
  *
  */

public class SRQualSubjectAjaxSP extends StoredProcedure {
  DataSource datasource = null;
  public SRQualSubjectAjaxSP(DataSource datasource) {
    setDataSource(datasource);
    this.datasource = datasource;
    setSql(SpringConstants.SR_GET_QUAL_SUBJECTS_FN);    
    setFunction(true);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.CURSOR, new GetQualInfoRowmapperImpl()));
    declareParameter(new SqlParameter("P_SUBJECT_TEXT", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_QUAL_ID", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("PT_QUAL_DETAIL_ARR", Types.ARRAY, "TB_USER_SUBJ_GRADE_DETAIL"));
    compile();
  }
  public Map execute(String freetext,GradeFilterBean gradeFilterBean){
    Map outMap = null;
    HashMap inMap = new HashMap();
    Connection connection = null;
    //
    try{
      connection = datasource.getConnection();
      ARRAY qualDetailsArr = OracleArray.getOracleArray(connection, "TB_USER_SUBJ_GRADE_DETAIL", gradeFilterBean.getQualDetailsArr());
      inMap.put("P_SUBJECT_TEXT", freetext);
      inMap.put("P_QUAL_ID", gradeFilterBean.getSubQualId());
      inMap.put("PT_QUAL_DETAIL_ARR", qualDetailsArr);
      outMap = execute(inMap);
    } catch(Exception e){
       e.printStackTrace();
    }
    finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException closeException) {
        closeException.printStackTrace();
      }
    }
    return outMap;
  }
  //This rowmapper gives the detail for subject ajax in grade filter page
  private class GetQualInfoRowmapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int num) {
      GradeFilterVO gradeFilterVO = new GradeFilterVO();
      try {
        gradeFilterVO.setSubject_id(rs.getString("QUAL_SUBJECT_ID")); 
        gradeFilterVO.setSubject_desc(rs.getString("QUAL_SUBJECT_DESC"));
      }
      catch (Exception e) {
	    e.printStackTrace();
      }
      return gradeFilterVO;
    }
  }  
}
	   