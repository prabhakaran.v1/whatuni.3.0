package com.layer.dao.sp.search.advancesearch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import mobile.valueobject.SearchVO;
import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.search.AssessedFilterVO;
import spring.valueobject.search.OmnitureLoggingVO;
import spring.valueobject.search.RefineByOptionsVO;
import spring.valueobject.search.ReviewCatFilterVO;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.layer.util.rowmapper.search.OmnitureLoggingRowMapperImpl;
import com.layer.util.rowmapper.search.StatsLogRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.search.BestMatchCoursesVO;
import com.wuni.util.valueobject.search.CourseSpecificSearchResultsVO;
import com.wuni.util.valueobject.search.LocationRefineVO;
import com.wuni.util.valueobject.search.SimilarStudyLevelCoursesVO;

/**
 * PAGE_NAME:   Advance search results page
 * URL_PATTERN: www.whatuni.com/[STUDY_LEVEL_DESC]-courses/herb/search?subject=[KEYWORD]&study-mode=[STUDY_LEVEL_DESC]&location=[LOCATION1]&entry-level=[ENTRY-LEVEL]&entry-points=[ENTRY-POINTS]&sort=[ORDER_BY]&pageno=[PAGE_NO]
 * URL_EXAMPLE: /degree-courses/herb/search?subject=business
 *
 * @author  Prabhakaran V.
 * @since   wu571_20171129 - redesign
 * Change Log
 * *******************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
 * *******************************************************************************************************************************
 * 08_May_2018    Prabhakaran V.             1.1      Added opendays button related column                                  wu_576
 *
 */
public class AdvanceSearchResultsSP extends StoredProcedure {

  public AdvanceSearchResultsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_ADVANCE_SEARCH_RESULTS_PRC);
    //
     declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SESSION_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_PAGE_NAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SEARCH_CATEGORY", Types.VARCHAR)); //p_search_category 
    declareParameter(new SqlOutParameter("P_SEARCH_CAT_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_KEYWORD_SEARCH", Types.VARCHAR)); //p_phrase_search
    declareParameter(new SqlParameter("P_QUALIFICATION", Types.VARCHAR));
    declareParameter(new SqlParameter("P_PAGE_NO", Types.NUMERIC));
    declareParameter(new SqlParameter("P_SEARCH_URL", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_AGENT", Types.VARCHAR));
    declareParameter(new SqlParameter("P_BASKET_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_ORDER_BY", Types.VARCHAR)); //p_search_how
     declareParameter(new SqlParameter("P_LOCATION_TYPE", Types.VARCHAR)); //p_univ_loc_type_name_str - P_LOCATION_TYPE
    declareParameter(new SqlParameter("P_REGION", Types.VARCHAR)); //p_town_city
     declareParameter(new SqlParameter("P_REGION_FLAG", Types.VARCHAR)); //
     declareParameter(new SqlParameter("P_STUDY_MODE", Types.VARCHAR));
     declareParameter(new SqlParameter("p_entry_level", Types.VARCHAR)); //p_entry_level
    declareParameter(new SqlParameter("p_entry_points", Types.VARCHAR)); //p_entry_points
    declareParameter(new SqlParameter("P_ASSESSMENT_TYPE", Types.VARCHAR)); //
     declareParameter(new SqlParameter("P_REVIEW_SUBJECT_STR", Types.VARCHAR)); //

     declareParameter(new SqlParameter("P_JACS_CODE", Types.VARCHAR));
     declareParameter(new SqlParameter("P_TRACK_SESSION_ID", Types.NUMERIC));
    declareParameter(new SqlOutParameter("PC_SEARCH_RESULT", OracleTypes.CURSOR, new CourseSpecificSearchResultRowMapperImpl())); //pc_search_results - PC_SEARCH_RESULT
    declareParameter(new SqlOutParameter("PC_QUAL_FILTERS", OracleTypes.CURSOR, new QualificationRowMapperImpl())); //pc_qualification_refine - PC_QUAL_FILTERS 
    declareParameter(new SqlOutParameter("PC_SUBJECT_FILTERS", OracleTypes.CURSOR, new SubjectRefineRowMapperImpl())); //pc_subject_refine - PC_SUBJECT_FILTERS 
    declareParameter(new SqlOutParameter("PC_LOCATION_FILTERS", OracleTypes.CURSOR,  new LocationRefineRowMapperImpl())); //pc_location_refine - PC_LOCATION_FILTERS 
    declareParameter(new SqlOutParameter("PC_LOCATION_TYPES_FILTER", OracleTypes.CURSOR, new LocationTypeRowMapperImpl()));   
    declareParameter(new SqlOutParameter("PC_STUDY_MODE_FILTER", OracleTypes.CURSOR, new RefinRowMapperImpl())); //pc_study_mode_refine - PC_STUDY_MODE_FILTER 
    declareParameter(new SqlOutParameter("PC_ASSESSMENT_TYPE_FILTER", OracleTypes.CURSOR, new assessmentFilterRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_REVIEW_CATEGORY_FILTER", OracleTypes.CURSOR, new reviewCatFilterRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_SEARCH_COLLEGE_IDS", OracleTypes.CURSOR, new OmnitureLoggingRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_SEARCH_INSTITUTION_IDS", OracleTypes.CURSOR, new InstitutionIdsLoggingRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_STATS_LOG", OracleTypes.CURSOR, new StatsLogRowMapperImpl()));
    declareParameter(new SqlOutParameter("P_TOTAL_COLLEGE_COUNT", Types.NUMERIC)); //p_record_count - P_TOTAL_COLLEGE_COUNT 
    declareParameter(new SqlOutParameter("P_TOTAL_COURSE_COUNT", Types.NUMERIC));
    declareParameter(new SqlOutParameter("p_show_count", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_include_best_match", Types.VARCHAR));
    declareParameter(new SqlOutParameter("P_PARENT_CAT_ID", Types.NUMERIC));
    declareParameter(new SqlOutParameter("P_PARENT_CAT_TEXT_KEY", Types.VARCHAR));
    declareParameter(new SqlOutParameter("P_PARENT_CAT_DESC", Types.VARCHAR));
    declareParameter(new SqlOutParameter("P_COURSE_RANK_FOUND_FLAG", Types.VARCHAR));
    declareParameter(new SqlOutParameter("P_INVALID_CATEGORY_FLAG", Types.VARCHAR));
    compile();
  }
  public Map execute(SearchVO searchVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("P_USER_ID", searchVO.getY());
      inMap.put("P_SESSION_ID", searchVO.getX());
      inMap.put("P_PAGE_NAME", searchVO.getPageName());
      inMap.put("P_SEARCH_CATEGORY", searchVO.getSearchCategory());
      inMap.put("P_SEARCH_CAT_ID", searchVO.getSearchCatId());
      inMap.put("P_KEYWORD_SEARCH", searchVO.getPhraseSearch()); //p_phrase_search - P_KEYWORD_SEARCH 
      inMap.put("P_QUALIFICATION", searchVO.getQualification());
      inMap.put("P_PAGE_NO", searchVO.getPageNo());
      inMap.put("P_SEARCH_URL", searchVO.getRequestURL());
      inMap.put("P_USER_AGENT", searchVO.getUserAgent());  
      inMap.put("P_BASKET_ID", searchVO.getBasketId()); 
      inMap.put("P_ORDER_BY", searchVO.getSearchHOW()); //p_search_how - P_ORDER_BY
      inMap.put("P_LOCATION_TYPE", searchVO.getUnivLocTypeName()); //p_univ_loc_type_name_str - P_LOCATION_TYPE 
      inMap.put("P_REGION", searchVO.getTownCity()); //p_town_city - P_REGION 
      inMap.put("P_REGION_FLAG", searchVO.getRegionFlag()); //
      inMap.put("P_STUDY_MODE", searchVO.getStudyMode());
      inMap.put("p_entry_level", searchVO.getEntryLevel()); //p_entry_level - P_PREV_QUAL 
      inMap.put("p_entry_points", searchVO.getEntryPoints()); //p_entry_points - P_PREV_QUAL_GRADE 
      inMap.put("P_ASSESSMENT_TYPE", searchVO.getAssessmentType()); // 
      inMap.put("P_REVIEW_SUBJECT_STR", searchVO.getReviewSubjects()); // 
      inMap.put("P_JACS_CODE", searchVO.getJacsCode());
      inMap.put("P_TRACK_SESSION_ID", searchVO.getUserTrackSessionId());//19_MAY_2015 - Friends activity

      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class RefinRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      RefineByOptionsVO locationRefineVO = new RefineByOptionsVO();
      locationRefineVO.setRefineDesc(resultSet.getString("refine_desc"));
      locationRefineVO.setRefineTextKey(resultSet.getString("refine_text_key"));      
      locationRefineVO.setSelectedStudyMode(resultSet.getString("study_mode_selected"));
      return locationRefineVO;
    }
  }
  
  private class assessmentFilterRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      AssessedFilterVO assessedFilterVO = new AssessedFilterVO();
      assessedFilterVO.setAssessmentTypeId(resultSet.getString("assessment_type_id"));
      assessedFilterVO.setAssessmentTypeName(resultSet.getString("assessment_type_display_name")); 
      assessedFilterVO.setSelectedAssessment(resultSet.getString("selected_text"));       
      return assessedFilterVO;
    }
  }
  
  private class reviewCatFilterRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      ReviewCatFilterVO reviewCatFilterVO = new ReviewCatFilterVO();
      reviewCatFilterVO.setReviewCatId(resultSet.getString("review_category_id"));
      reviewCatFilterVO.setReviewCatName(resultSet.getString("review_category_display_name")); 
      reviewCatFilterVO.setReviewCatTextKey(resultSet.getString("text_key"));
      reviewCatFilterVO.setReviewSelectText(resultSet.getString("selected_text"));       
      return reviewCatFilterVO;
    }
  }
  
  private class SubjectRefineRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      SeoUrls seoUrls = new SeoUrls();
      SimilarStudyLevelCoursesVO subjectRefineVO = new SimilarStudyLevelCoursesVO();
      subjectRefineVO.setCategoryCode(resultSet.getString("category_code"));
      subjectRefineVO.setCategoryDesc(resultSet.getString("subject"));
      subjectRefineVO.setSubjectTextKey(resultSet.getString("subject_text_key"));
      subjectRefineVO.setCollegeCount(resultSet.getString("result_count"));
      subjectRefineVO.setCategoryId(resultSet.getString("browse_cat_id"));
      subjectRefineVO.setQualificationCode(resultSet.getString("qualification"));//get from ram sire
      subjectRefineVO.setBrowseMoneyPageUrl(seoUrls.construnctNewBrowseMoneyPageURL(subjectRefineVO.getQualificationCode(), subjectRefineVO.getCategoryId(), subjectRefineVO.getCategoryDesc(), "PAGE"));
      return subjectRefineVO;
    }
  }

  private class LocationRefineRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      LocationRefineVO locationRefineVO = new LocationRefineVO();
      locationRefineVO.setRegionName(resultSet.getString("region_name"));
      locationRefineVO.setLocationTextKey(resultSet.getString("location_text_key"));      
      locationRefineVO.setTopRegionName(resultSet.getString("top_region_name")); 
      locationRefineVO.setRegionTextKey(resultSet.getString("region_text_key"));
      locationRefineVO.setRegionSelected(resultSet.getString("region_selected"));
      return locationRefineVO;
    }
  }
  //
  private class QualificationRowMapperImpl implements RowMapper {
     public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
       RefineByOptionsVO qualRefineVO = new RefineByOptionsVO();
       qualRefineVO.setRefineDesc(resultSet.getString("refine_desc"));
       qualRefineVO.setRefineCode(resultSet.getString("refine_code"));
       qualRefineVO.setRefineTextKey(resultSet.getString("refine_text_key"));       
       qualRefineVO.setCategoryName(resultSet.getString("browse_cat_desc"));
       qualRefineVO.setBrowseCatTextKey(resultSet.getString("browse_cat_text_key"));       
       return qualRefineVO;
     }
   }
  //
  private class LocationTypeRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      RefineByOptionsVO reineByOptionsVO = new RefineByOptionsVO();
      reineByOptionsVO.setRefineCode(resultSet.getString("option_id"));      
      reineByOptionsVO.setRefineDesc(resultSet.getString("option_desc"));
      reineByOptionsVO.setOptionTextKey(resultSet.getString("option_text_key"));      
      reineByOptionsVO.setSelectedLocationType(resultSet.getString("location_type_selected"));
      reineByOptionsVO.setRefineOrder("UNIV_LOC_TYPE");
      return reineByOptionsVO;
    }  
  }
  //
  private class CourseSpecificSearchResultRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      CourseSpecificSearchResultsVO courseSpecificSearchResultsVO = new CourseSpecificSearchResultsVO();
      SeoUrls seoUrl = new SeoUrls();
      CommonUtil commonutil = new CommonUtil();
      courseSpecificSearchResultsVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      courseSpecificSearchResultsVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
      courseSpecificSearchResultsVO.setCollegePRUrl(resultset.getString("COLLEGE_PR_URL"));
      courseSpecificSearchResultsVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
      courseSpecificSearchResultsVO.setUniHomeUrl(seoUrl.construnctUniHomeURL(courseSpecificSearchResultsVO.getCollegeId(), courseSpecificSearchResultsVO.getCollegeName()));
      courseSpecificSearchResultsVO.setOverallRating(resultset.getString("rating"));
      if(!GenericValidator.isBlankOrNull(resultset.getString("exact_rating"))){
       courseSpecificSearchResultsVO.setExactRating(commonutil.formatNumberReview(resultset.getString("exact_rating")));//25-Mar-2014_REL
      }
      courseSpecificSearchResultsVO.setNumberOfCoursesForThisCollege(resultset.getString("HITS"));
      int count = 0;
      if(!GenericValidator.isBlankOrNull(courseSpecificSearchResultsVO.getNumberOfCoursesForThisCollege())){
        count = Integer.parseInt(courseSpecificSearchResultsVO.getNumberOfCoursesForThisCollege()) - 1;
      }
      courseSpecificSearchResultsVO.setAdditionalCourses(String.valueOf(count));
      courseSpecificSearchResultsVO.setWasThisCollegeShortlisted((resultset.getString("COLLEGE_IN_BASKET")));
      courseSpecificSearchResultsVO.setCollegeLogoPath(resultset.getString("COLLEGE_LOGO"));
      courseSpecificSearchResultsVO.setJacsSubject(resultset.getString("JACS_SUBJECT"));
      courseSpecificSearchResultsVO.setIsSponsoredCollege(resultset.getString("CATEGORY_SPONSORED_FLAG"));
      //Added opendays button related column by Prabha on 8_May_2018
      courseSpecificSearchResultsVO.setNetworkId(resultset.getString("network_id"));
      courseSpecificSearchResultsVO.setOpendaySuborderItemId(resultset.getString("open_day_suborder_item_id"));
      courseSpecificSearchResultsVO.setOpenDate(resultset.getString("open_day"));
      courseSpecificSearchResultsVO.setOpenMonthYear(resultset.getString("open_month_year"));
      courseSpecificSearchResultsVO.setBookingUrl(resultset.getString("booking_url"));
      courseSpecificSearchResultsVO.setTotalOpendays(resultset.getString("total_open_days"));
      //End of code
      courseSpecificSearchResultsVO.setProviderId(resultset.getString("provider_id"));//Added by Sangeeth.S for the smart pixel logging college id      
      ResultSet bestMatchCoursesRS = (ResultSet)resultset.getObject("BEST_MATCH_COURSES");
      try {
        if (bestMatchCoursesRS != null) {
          ArrayList bestMatchCoursesList = new ArrayList();
          BestMatchCoursesVO bestMatchCoursesVO = null;
          String tmpUrl = null;
          String courseUcasTariff = null;
          while (bestMatchCoursesRS.next()) {
            bestMatchCoursesVO = new BestMatchCoursesVO();
            bestMatchCoursesVO.setCourseId(bestMatchCoursesRS.getString("COURSE_ID"));
            bestMatchCoursesVO.setCourseTitle(bestMatchCoursesRS.getString("COURSE_TITLE"));
            bestMatchCoursesVO.setCourseUcasTariff(bestMatchCoursesRS.getString("UCAS_TARIFF"));
            courseUcasTariff = GenericValidator.isBlankOrNull(bestMatchCoursesVO.getCourseUcasTariff())? "": bestMatchCoursesVO.getCourseUcasTariff();
            bestMatchCoursesVO.setCourseUcasTariff(courseUcasTariff);
            bestMatchCoursesVO.setCourseDomesticFees(bestMatchCoursesRS.getString("COURSE_DOM_FEES"));
            bestMatchCoursesVO.setWasThisCourseShortlisted(bestMatchCoursesRS.getString("COURSE_IN_BASKET"));
            bestMatchCoursesVO.setStudyLevelDesc(bestMatchCoursesRS.getString("SEO_STUDY_LEVEL_TEXT"));
            bestMatchCoursesVO.setUcasCode(bestMatchCoursesRS.getString("UCAS_CODE"));
            bestMatchCoursesVO.setSubOrderItemId(bestMatchCoursesRS.getString("SUBORDER_ITEM_ID"));
            bestMatchCoursesVO.setSubOrderEmail(bestMatchCoursesRS.getString("EMAIL"));
            bestMatchCoursesVO.setSubOrderProspectus(bestMatchCoursesRS.getString("PROSPECTUS"));
            bestMatchCoursesVO.setFeeDuration(bestMatchCoursesRS.getString("FEE_DURATION"));
            String moduletext = bestMatchCoursesRS.getString("module_group_info_rec");
            if(!GenericValidator.isBlankOrNull(moduletext)){
              String modArr[] = moduletext.split("##SP##");
              ArrayList moduledetailList = new ArrayList(Arrays.asList( modArr));      
              bestMatchCoursesVO.setModuleDetailList(moduledetailList);
            }            
           tmpUrl = bestMatchCoursesRS.getString("WEBSITE");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            bestMatchCoursesVO.setSubOrderWebsite(tmpUrl);
            //
            tmpUrl = bestMatchCoursesRS.getString("EMAIL_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            bestMatchCoursesVO.setSubOrderEmailWebform(tmpUrl);
            //
            tmpUrl = bestMatchCoursesRS.getString("PROSPECTUS_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            bestMatchCoursesVO.setSubOrderProspectusWebform(tmpUrl);
            //
            bestMatchCoursesVO.setMyhcProfileId(bestMatchCoursesRS.getString("MYHC_PROFILE_ID"));
            bestMatchCoursesVO.setProfileType(bestMatchCoursesRS.getString("PROFILE_TYPE"));
            bestMatchCoursesVO.setApplyNowFlag(bestMatchCoursesRS.getString("APPLY_NOW_FLAG"));
            bestMatchCoursesVO.setCpeQualificationNetworkId(bestMatchCoursesRS.getString("NETWORK_ID"));
            bestMatchCoursesVO.setPageName(bestMatchCoursesRS.getString("page_name"));
            bestMatchCoursesVO.setCourseDetailsPageURL(seoUrl.construnctCourseDetailsPageURL(bestMatchCoursesVO.getStudyLevelDesc(), bestMatchCoursesVO.getCourseTitle(), bestMatchCoursesVO.getCourseId(), courseSpecificSearchResultsVO.getCollegeId(), bestMatchCoursesVO.getPageName()));
            bestMatchCoursesVO.setWebformPrice(bestMatchCoursesRS.getString("webform_price"));
            bestMatchCoursesVO.setWebsitePrice(bestMatchCoursesRS.getString("website_price"));
            bestMatchCoursesVO.setModuleGroupDetails(bestMatchCoursesRS.getString("module_group_details"));
            if(!GenericValidator.isBlankOrNull(bestMatchCoursesVO.getModuleGroupDetails())){
              String[] modArr= bestMatchCoursesVO.getModuleGroupDetails().split("##");
              if(!GenericValidator.isBlankOrNull(modArr[1])){
                bestMatchCoursesVO.setModuleGroupId(modArr[1]);
                if(!GenericValidator.isBlankOrNull(modArr[0])){
                  bestMatchCoursesVO.setModuleTitle(modArr[0]);
                }
              }
            }
            bestMatchCoursesVO.setEmploymentRate(bestMatchCoursesRS.getString("employment_rate"));
            bestMatchCoursesVO.setCourseRank(bestMatchCoursesRS.getString("course_rank"));
            if(!GenericValidator.isBlankOrNull(bestMatchCoursesVO.getCourseRank())){
             bestMatchCoursesVO.setOrdinalRank(new CommonFunction().getOrdinalFor(Integer.parseInt(bestMatchCoursesVO.getCourseRank())));
            }
            bestMatchCoursesVO.setDepartmentOrFaculty(bestMatchCoursesRS.getString("DEPARTMENT_OR_FACULTY"));//Added by Indumathi.S 29-03-16     
            if("SP".equalsIgnoreCase(bestMatchCoursesVO.getProfileType())){
               bestMatchCoursesVO.setSpUrl(seoUrl.construnctSpURL(courseSpecificSearchResultsVO.getCollegeId(), courseSpecificSearchResultsVO.getCollegeName(), bestMatchCoursesVO.getMyhcProfileId(), "", bestMatchCoursesVO.getCpeQualificationNetworkId(), "overview"));
            }
            bestMatchCoursesVO.setMatchPercentage(bestMatchCoursesRS.getString("best_match_percent"));
            bestMatchCoursesVO.setLocationMatchFlag(bestMatchCoursesRS.getString("location_match"));
            bestMatchCoursesVO.setLocationTypeMatchFlag(bestMatchCoursesRS.getString("loc_type_match"));
            bestMatchCoursesVO.setStudyModeMatchFlag(bestMatchCoursesRS.getString("study_mode_match"));
            bestMatchCoursesVO.setEntryLevelMatchFlag(bestMatchCoursesRS.getString("entry_level_match"));
            bestMatchCoursesVO.setAssessedMatchFlag(bestMatchCoursesRS.getString("assessment_type_match"));
            bestMatchCoursesVO.setReviewSub1MatchFlag(bestMatchCoursesRS.getString("review_sub_one_match"));
            bestMatchCoursesVO.setReviewSub2MatchFlag(bestMatchCoursesRS.getString("review_sub_two_match"));
            bestMatchCoursesVO.setReviewSub3MatchFlag(bestMatchCoursesRS.getString("review_sub_three_match"));
            bestMatchCoursesVO.setQualificationMatchFlag(bestMatchCoursesRS.getString("qualification_match"));
            bestMatchCoursesVO.setSubjectMatchFlag(bestMatchCoursesRS.getString("subject_match"));
            //
            bestMatchCoursesList.add(bestMatchCoursesVO);               
          }          
          courseSpecificSearchResultsVO.setBestMatchCoursesList(bestMatchCoursesList);
        }
        courseSpecificSearchResultsVO.setReviewCount(resultset.getString("review_count"));
        courseSpecificSearchResultsVO.setUniReviewsURL(new SeoUrls().constructReviewPageSeoUrl(courseSpecificSearchResultsVO.getCollegeName(), courseSpecificSearchResultsVO.getCollegeId()));
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (bestMatchCoursesRS != null) {
            bestMatchCoursesRS.close();
            bestMatchCoursesRS = null;
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      return courseSpecificSearchResultsVO;
    }
  }
  //
  private class InstitutionIdsLoggingRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      OmnitureLoggingVO omnitureLoggingVO = new OmnitureLoggingVO();
      omnitureLoggingVO.setInstitutionIdsForThisSearch(resultSet.getString("institution_ids"));
      return omnitureLoggingVO;
    }  
  }
}

