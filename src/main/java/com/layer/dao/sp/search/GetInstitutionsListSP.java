package com.layer.dao.sp.search;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.review.bean.UniBrowseBean;

import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUrls;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : GetInstitutionsListSP.java
 * Description   : Store procedure class to get the Institution details.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * Anbarasan.R                    1.0               20.11.2012           First draft
 * Indumathi Selvam               1.1               03.09.2015           Page redesign  		
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
public class GetInstitutionsListSP extends StoredProcedure {

  public GetInstitutionsListSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.GET_ALPHABET_COLLEGE_NAME_FN);
    declareParameter(new SqlParameter("p_alpha_char", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_region_name", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("pc_college_list", OracleTypes.CURSOR, new BrowseUniversityRowMapperImpl()));
    declareParameter(new SqlOutParameter("p_breadcrumb", OracleTypes.VARCHAR));
    compile();
  }

  /**
   * @Method to set the in parameters
   * @param uniBrowseBean
   * @return Map
   */
  public Map execute(UniBrowseBean uniBrowseBean) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_alpha_char", uniBrowseBean.getSearchText());
      inputMap.put("p_region_name", uniBrowseBean.getRegionName());
      inputMap.put("p_page_no", uniBrowseBean.getPageNo());
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }

  /**
   * To get the university details from the cursor and store it in bean
   */
  private class BrowseUniversityRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      UniBrowseBean uniBrowseBean = new UniBrowseBean();
      uniBrowseBean.setCollegeId(resultSet.getString("college_id"));
      uniBrowseBean.setCollegeName(resultSet.getString("college_name"));
      uniBrowseBean.setCollegeNameUrl(new SeoUrls().construnctUniHomeURL(uniBrowseBean.getCollegeId(), uniBrowseBean.getCollegeName()));
      uniBrowseBean.setCollegeNameDisplay(resultSet.getString("college_name_display"));
      uniBrowseBean.setCollegeLogo(resultSet.getString("college_logo"));
      if (!GenericValidator.isBlankOrNull(resultSet.getString("review_count"))) {
        uniBrowseBean.setReviewURL(new SeoUrls().constructUniSpecificReviewUrl(uniBrowseBean.getCollegeId(), uniBrowseBean.getCollegeName()));
        uniBrowseBean.setReviewCount(resultSet.getString("review_count"));
      }
      if (!GenericValidator.isBlankOrNull(resultSet.getString("overall_rating_round"))) {
        int roundOfRating = Integer.parseInt(resultSet.getString("overall_rating_round"));
        if (roundOfRating > 0) {
          uniBrowseBean.setOverallRating(String.valueOf(roundOfRating));
        }
      }
      uniBrowseBean.setActualRating(resultSet.getString("overall_rating_exact"));
      uniBrowseBean.setTotalRecordCount(resultSet.getString("total_records"));
      uniBrowseBean.setCurrentRank(resultSet.getString("current_rank"));
      uniBrowseBean.setTotalRank(resultSet.getString("total_rank"));
      return uniBrowseBean;
    }

  }

}
