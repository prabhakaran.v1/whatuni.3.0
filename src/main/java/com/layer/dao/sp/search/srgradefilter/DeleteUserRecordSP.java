package com.layer.dao.sp.search.srgradefilter;

import WUI.whatunigo.bean.GradeFilterBean;
import com.layer.util.SpringConstants;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @see This SP class is used to call the database procedure to delete the user records or subjects if exist.
  * @since  22.11.2019- intital draft
  * @version 1.0
  *
  * Modification history:
  * *****************************************************************************************************************
  * Author          Relase tag       Modification Details                                                  Rel Ver.
  * *****************************************************************************************************************
  * Jeyalakshmi.D       1.0             initial draft                                                       wu_596
  *
  */
  
public class DeleteUserRecordSP extends StoredProcedure {
  DataSource datasource = null;
  public DeleteUserRecordSP(DataSource datasource) {
    setDataSource(datasource);
    this.datasource = datasource;
    setSql(SpringConstants.SR_DELETE_USER_SUBJ_GRADE_FN);
    setFunction(true);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_SESSION_ID", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.NUMBER));
    compile();	 
  }

  public Map execute(GradeFilterBean gradeFilterBean) {
    Map outMap = null;
    HashMap inMap = new HashMap();
    //
    try{
      inMap.put("P_SESSION_ID", gradeFilterBean.getSessionId());
      inMap.put("P_USER_ID", gradeFilterBean.getUserId());
      outMap = execute(inMap);
    } catch(Exception e){
      e.printStackTrace();
    }
    return outMap;
  }
}
