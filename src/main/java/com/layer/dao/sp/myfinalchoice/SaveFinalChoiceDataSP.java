package com.layer.dao.sp.myfinalchoice;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.wuni.myfinalchoice.form.MyFinalChoiceBean;

public class SaveFinalChoiceDataSP extends StoredProcedure {
  public SaveFinalChoiceDataSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.SAVE_FINAL_CHOICES_DATA_PRC);
    declareParameter(new SqlParameter("P_FINAL_CHOICE_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_COURSE_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_CHOICE_POSITION", Types.NUMERIC));
    declareParameter(new SqlParameter("P_STATUS", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_new_final_choice_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_college_logo", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_return_code", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_incomplete_action_cnt", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_final_choices_cnt", Types.VARCHAR));
  }
  public Map execute(MyFinalChoiceBean myChoiceBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_FINAL_CHOICE_ID", myChoiceBean.getFinalChoiceId());
      inMap.put("P_USER_ID", myChoiceBean.getUserId());
      inMap.put("P_COLLEGE_ID", myChoiceBean.getCollegeId());
      inMap.put("P_COURSE_ID", myChoiceBean.getCourseId());
      inMap.put("P_CHOICE_POSITION", myChoiceBean.getSlotPositionId());
      inMap.put("P_STATUS", myChoiceBean.getSlotPositionStatus());

      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
