package com.layer.dao.sp.myfinalchoice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.wuni.myfinalchoice.form.MyFinalChoiceBean;

public class FinalChUsrActionPodSP extends StoredProcedure {
  public FinalChUsrActionPodSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.FINAL_CHUSER_ACTION_PRC);
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_COURSE_ID", Types.NUMERIC));
    declareParameter(new SqlOutParameter("pc_final_choice_list", OracleTypes.CURSOR, new FinalChUserActionListRowMapperImpl()));
  }
  public Map execute(MyFinalChoiceBean myChoiceBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", myChoiceBean.getUserId());
      inMap.put("P_COLLEGE_ID", myChoiceBean.getCollegeId());
      inMap.put("P_COURSE_ID", myChoiceBean.getCourseId());
     
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class FinalChUserActionListRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      MyFinalChoiceBean choiceBean = new MyFinalChoiceBean();
      choiceBean.setActionMsg(resultSet.getString("action_msg"));
      choiceBean.setActionStatus(resultSet.getString("which_status"));
      choiceBean.setActionURL(resultSet.getString("incomplete_action_url"));
      return choiceBean;
    }
  }
}
