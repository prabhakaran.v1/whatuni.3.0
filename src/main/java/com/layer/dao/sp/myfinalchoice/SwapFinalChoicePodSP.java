package com.layer.dao.sp.myfinalchoice;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.wuni.myfinalchoice.form.MyFinalChoiceBean;

public class SwapFinalChoicePodSP extends StoredProcedure {
  public SwapFinalChoicePodSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.CHANGE_FINAL_CHOICE_DATA_PRC);
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_FINAL_CHOICE_ID_1", Types.NUMERIC));
    declareParameter(new SqlParameter("P_CHOICE_POSITION_1", Types.NUMERIC));
    declareParameter(new SqlParameter("P_FINAL_CHOICE_ID_2", Types.NUMERIC));
    declareParameter(new SqlParameter("P_CHOICE_POSITION_2", Types.NUMERIC));
    declareParameter(new SqlOutParameter("p_status_code", Types.VARCHAR));
  }
  public Map execute(MyFinalChoiceBean myChoiceBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", myChoiceBean.getUserId());
      inMap.put("P_FINAL_CHOICE_ID_1", myChoiceBean.getSwapFianlChoiceId1());
      inMap.put("P_CHOICE_POSITION_1", myChoiceBean.getSwapChoicePosition1());
      inMap.put("P_FINAL_CHOICE_ID_2", myChoiceBean.getSwapFianlChoiceId2());
      inMap.put("P_CHOICE_POSITION_2", myChoiceBean.getSwapChoicePosition2());

      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
