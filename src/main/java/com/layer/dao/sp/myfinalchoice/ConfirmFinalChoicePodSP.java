package com.layer.dao.sp.myfinalchoice;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.wuni.myfinalchoice.form.MyFinalChoiceBean;

public class ConfirmFinalChoicePodSP extends StoredProcedure {
  public ConfirmFinalChoicePodSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(SpringConstants.CONFIRM_FINAL_CHOICES_FN);
    declareParameter(new SqlOutParameter("p_confirm_status", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
  }
  public Map execute(MyFinalChoiceBean myChoiceBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", myChoiceBean.getUserId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
