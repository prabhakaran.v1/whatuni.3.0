package com.layer.dao.sp.myfinalchoice;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.CommonUtil;

import com.layer.util.SpringConstants;
import com.wuni.myfinalchoice.form.MyFinalChoiceBean;

public class FinalChoiceSuggestionListSP extends StoredProcedure {
  public FinalChoiceSuggestionListSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(SpringConstants.FINAL_CHOICES_SUGGESTION_FN);
    declareParameter(new SqlOutParameter("pc_final_choice_Suggestion", OracleTypes.CURSOR, new FinalChSuggestionRowMapperImpl()));
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
  }
  public Map execute(MyFinalChoiceBean myChoiceBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", myChoiceBean.getUserId());
    
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class FinalChSuggestionRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      MyFinalChoiceBean choiceBean = new MyFinalChoiceBean();
      choiceBean.setCollegeId(resultSet.getString("college_id"));
      choiceBean.setCollegeDispName(resultSet.getString("college_name_display"));
      choiceBean.setCollegeLogo(resultSet.getString("college_logo"));
      if (!GenericValidator.isBlankOrNull(choiceBean.getCollegeLogo())) {
        choiceBean.setCollegeLogo(new CommonUtil().getImgPath((choiceBean.getCollegeLogo()), rowNum));
      }
      choiceBean.setCourseId(resultSet.getString("course_id"));
      choiceBean.setCourseTitle(resultSet.getString("course_title"));
      return choiceBean;
    }
  }
}
