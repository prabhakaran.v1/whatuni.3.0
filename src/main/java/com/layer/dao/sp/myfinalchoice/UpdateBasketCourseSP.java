package com.layer.dao.sp.myfinalchoice;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.basket.CompareBasketRowMapperImpl;
import com.wuni.valueobjects.FinalFiveChoiceVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : UpdateBasketCourseSP.java
 * Description   : This class is used to add a course or remove course in a basket..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Prabhakaran V.           1.0             16.05.2017      First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

public class UpdateBasketCourseSP extends StoredProcedure {
  public UpdateBasketCourseSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.UPDATE_BASKET_COURSE_PRC);
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_BASKET_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_BASKET_CONTENT_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_FINAL_CHOICE_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_COURSE_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_ASSOC_TEXT", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_TYPE_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_ret_Code", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_COMPARE", OracleTypes.CURSOR, new CompareBasketRowMapperImpl()));
    compile();
  }
  public Map execute(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", finalFiveChoiceVO.getUserId());
      inMap.put("P_BASKET_ID", finalFiveChoiceVO.getBasketId());
      inMap.put("P_BASKET_CONTENT_ID", finalFiveChoiceVO.getBasketContentId());
      inMap.put("P_FINAL_CHOICE_ID", finalFiveChoiceVO.getFinalChoiceId());
      inMap.put("P_COURSE_ID", finalFiveChoiceVO.getCourseId());
      inMap.put("P_COLLEGE_ID", finalFiveChoiceVO.getCollegeId());
      inMap.put("P_ASSOC_TEXT", finalFiveChoiceVO.getAssociateText());
      inMap.put("P_TYPE_FLAG", finalFiveChoiceVO.getStatus());
 
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
