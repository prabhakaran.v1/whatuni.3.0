package com.layer.dao.sp.myfinalchoice;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.basket.CompareBasketRowMapperImpl;
import com.wuni.valueobjects.FinalFiveChoiceVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : SaveUpdateChoiceSP.java
 * Description   : This class is used to add to final five and remove final five choice..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Prabhakaran V.           1.0             16.05.2017      First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
public class SaveUpdateChoiceSP extends StoredProcedure {
  public SaveUpdateChoiceSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.SAVE_FINAL_CHOICES_DATA_PRC);
    declareParameter(new SqlParameter("P_FINAL_CHOICE_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_COURSE_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_CHOICE_POSITION", Types.NUMERIC));
    declareParameter(new SqlParameter("P_STATUS", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_new_final_choice_id", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_college_logo", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_return_code", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_incomplete_action_cnt", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_final_choices_cnt", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_BASKET_ID", Types.NUMERIC));
    declareParameter(new SqlOutParameter("PC_COMPARE", OracleTypes.CURSOR, new CompareBasketRowMapperImpl()));
    compile();
  }
  public Map execute(FinalFiveChoiceVO finalFiveChoiceVO) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_FINAL_CHOICE_ID", finalFiveChoiceVO.getFinalChoiceId());
      inMap.put("P_USER_ID", finalFiveChoiceVO.getUserId());
      inMap.put("P_COLLEGE_ID", finalFiveChoiceVO.getCollegeId());
      inMap.put("P_COURSE_ID", finalFiveChoiceVO.getCourseId());
      inMap.put("P_CHOICE_POSITION", finalFiveChoiceVO.getChoicePos());
      inMap.put("P_STATUS", finalFiveChoiceVO.getStatus());
      inMap.put("P_BASKET_ID", finalFiveChoiceVO.getBasketId());

      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
