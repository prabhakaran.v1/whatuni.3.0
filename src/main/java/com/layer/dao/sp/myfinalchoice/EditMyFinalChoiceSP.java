package com.layer.dao.sp.myfinalchoice;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.wuni.myfinalchoice.form.MyFinalChoiceBean;

public class EditMyFinalChoiceSP extends StoredProcedure {
  public EditMyFinalChoiceSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(SpringConstants.FINAL_CHOICES_EDIT_FN);
    declareParameter(new SqlOutParameter("p_return_code", Types.VARCHAR));
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
  }
  public Map execute(MyFinalChoiceBean myChoiceBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", myChoiceBean.getUserId());

      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
