package com.layer.dao.sp;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.valueobject.VersionChangesVO;

public class VersionChangesSP extends StoredProcedure {
      public VersionChangesSP(DataSource datasource) {
        setDataSource(datasource);
        setSql("HOT_WUNI.GET_PRIVACY_AND_TC_URL_PRC");
        
        declareParameter(new SqlParameter("P_AFFILIATE_ID", OracleTypes.NUMERIC));
        declareParameter(new SqlParameter("P_PAGE_TYPE", OracleTypes.VARCHAR));
        declareParameter(new SqlOutParameter("P_PAGE_URL", OracleTypes.VARCHAR));
        compile();
      }
      public Map execute(VersionChangesVO versionChangesVO) {
        Map outMap = null;
        try {
          Map inMap = new HashMap();
         inMap.put("P_AFFILIATE_ID", versionChangesVO.getAffliateId());
          inMap.put("P_PAGE_TYPE", versionChangesVO.getPageType());
          outMap = execute(inMap);
        } catch (Exception e) {
          e.printStackTrace();
        }
        return outMap;
      }
}
