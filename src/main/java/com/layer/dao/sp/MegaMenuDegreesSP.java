package com.layer.dao.sp;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class MegaMenuDegreesSP extends StoredProcedure{
  public MegaMenuDegreesSP(DataSource datasource){
    setDataSource(datasource);
    setFunction(true);
    setSql("wu_html_editor_pkg.page_not_found_degrees_list_fn");
    declareParameter(new SqlOutParameter("o_megamenu", OracleTypes.CLOB));
    declareParameter(new SqlParameter(" p_qualification_code", OracleTypes.VARCHAR)); 
  }
  
  public Map execut(String newstudylevelid){
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put(" p_qualification_code", newstudylevelid);
    outMap = execute(inMap);
    return outMap;
  }
}
