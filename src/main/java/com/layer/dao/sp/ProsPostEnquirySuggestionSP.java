package com.layer.dao.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.CommonFunction;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.ql.PostEnquiryVO;

public class ProsPostEnquirySuggestionSP extends StoredProcedure {

  public ProsPostEnquirySuggestionSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql("wuni_enquiry_baskets_pkg.get_cpe_down_pros_fn");   
    declareParameter(new SqlOutParameter("oc_dp_suggestion_list", OracleTypes.CURSOR, new PostenquiryRowMapperImpl()));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_enq_basket_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_enquiry_type", Types.VARCHAR));
    compile();
    }
    public Map execute(List inputList){
      Map outMap = null;
      try{
          Map inMap = new HashMap();        
          inMap.put("p_user_id", inputList.get(1));
          inMap.put("p_enq_basket_id", inputList.get(2));          
          inMap.put("p_enquiry_type", inputList.get(3));          
          outMap = execute(inMap);
        }catch(Exception e){
          e.printStackTrace();
        }
    return outMap;    
    }
    
  private class  PostenquiryRowMapperImpl implements RowMapper {
     public Object mapRow(ResultSet rs, int i) throws SQLException {
       PostEnquiryVO postVO = new PostEnquiryVO();
       postVO.setCollegeId(rs.getString("college_id"));
       postVO.setCollegeName(rs.getString("college_name"));
       postVO.setCollegeDisplayName(rs.getString("college_name_display"));
       postVO.setCollegeLogo(rs.getString("college_logo"));
       SeoUrls seoUrl = new SeoUrls();
       postVO.setEmailPrice(rs.getString("email_price"));
       if((!GenericValidator.isBlankOrNull(postVO.getCourseId()) || !"0".equalsIgnoreCase(postVO.getCourseId()))){
         postVO.setCourseDetailsURL(new SeoUrls().construnctCourseDetailsPageURL(postVO.getCourseSeoStudyLevelText(), postVO.getCourseTitle(), postVO.getCourseId(), postVO.getCollegeId(), "COURSE_SPECIFIC"));
       }
       ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("enquiry_details");
       try{
         if(enquiryDetailsRS != null){
           while(enquiryDetailsRS.next()){
             String tmpUrl = null;
             postVO.setOrderItemId(enquiryDetailsRS.getString("order_item_id"));
             postVO.setMyhcProfileId(enquiryDetailsRS.getString("myhc_profile_id"));
             postVO.setSubOrderItemId(enquiryDetailsRS.getString("suborder_item_id"));
             postVO.setProfileType(enquiryDetailsRS.getString("profile_type"));
             postVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("network_id"));
             postVO.setAdvertName(enquiryDetailsRS.getString("advert_name"));
             postVO.setSubOrderEmail(enquiryDetailsRS.getString("email"));
             postVO.setSubOrderProspectus(enquiryDetailsRS.getString("prospectus"));
             tmpUrl = enquiryDetailsRS.getString("website");
             if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                 tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
             } else {
               tmpUrl = "";
             }
             postVO.setSubOrderWebsite(tmpUrl);
             tmpUrl = enquiryDetailsRS.getString("email_webform");
             if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                 tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
             } else {
               tmpUrl = "";
             }
             postVO.setSubOrderEmailWebform(tmpUrl);
             tmpUrl = enquiryDetailsRS.getString("prospectus_webform");
             if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                 tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
             } else {
               tmpUrl = "";
             }
             postVO.setSubOrderProspectusWebform(tmpUrl);
           }
         }
       }catch(Exception e){
         e.printStackTrace();
       }finally{
         try{
           enquiryDetailsRS.close();
         }catch(Exception e){
           throw new SQLException(e.getMessage());
         }
       }
       
       ResultSet dpDetailsRs = (ResultSet)rs.getObject("DP_DETAILS");
       try {
         if (dpDetailsRs != null) {
           while (dpDetailsRs.next()) {
             postVO.setDpURL(dpDetailsRs.getString("dpurl"));
           }
         }
       } catch (Exception e) {
         e.printStackTrace();
       } finally {
         try {
           if (dpDetailsRs != null) {
             dpDetailsRs.close();
           }
         } catch (Exception e) {
           throw new SQLException(e.getMessage());
         }
       }
       postVO.setUniHomeUrl(seoUrl.construnctUniHomeURL(rs.getString("college_id"), rs.getString("college_name")).toLowerCase());
       if("IP".equalsIgnoreCase(postVO.getProfileType())){
         postVO.setWhatUniURL(seoUrl.construnctUniHomeURL(rs.getString("college_id"), rs.getString("college_name")).toLowerCase());
       }else if("SP".equalsIgnoreCase(postVO.getProfileType())){
         postVO.setWhatUniURL(seoUrl.construnctSpURL(postVO.getCollegeId(), postVO.getCollegeName(), postVO.getMyhcProfileId(), "0", postVO.getCpeQualificationNetworkId(), "overview"));
       }
       return postVO;
   
     }
   }
}
