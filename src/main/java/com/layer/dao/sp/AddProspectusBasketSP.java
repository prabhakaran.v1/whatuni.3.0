package com.layer.dao.sp;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.basket.form.BasketBean;

import com.wuni.util.sql.OracleArray;

public class AddProspectusBasketSP extends StoredProcedure {
  DataSource ds = null;
  public AddProspectusBasketSP(DataSource datasource) {
    setDataSource(datasource);   
    this.ds = datasource;
    setSql("WUNI_ENQUIRY_BASKETS_PKG.GET_ENQ_BASKET_ADD_PRC");
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));    
    declareParameter(new SqlParameter("p_enq_basket_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_enquiry_type", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_attr_val_arr", OracleTypes.ARRAY, "TB_ENQ_BASKET_ADD"));
    declareParameter(new SqlParameter("p_track_session", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_enq_baskets", OracleTypes.CURSOR, new enqBasketSliderRowMapperImpl()));
    compile();
  }
  //  
  public Map execute(BasketBean basketBean, Object[][] attrValues) {
    Map outMap = null;
    Connection connection = null;
    try {
      connection = ds.getConnection();
      Map inputMap = new HashMap();
      ARRAY values = OracleArray.getOracleArray(connection, "TB_ENQ_BASKET_ADD", attrValues);
          inputMap.put("p_user_id", basketBean.getUserId());
          inputMap.put("p_enq_basket_id", basketBean.getBasketId());
          inputMap.put("p_enquiry_type", basketBean.getBasketType());
          inputMap.put("p_attr_val_arr",  values);
          inputMap.put("p_track_session", basketBean.getTrackSessionId());
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }finally{
      try{
        connection.close();
      }catch(Exception d){
        d.printStackTrace();
      }
    }
    return outMap;
  }
  private class enqBasketSliderRowMapperImpl implements RowMapper {    
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      BasketBean addBean = new BasketBean();
      addBean.setBasketId(resultset.getString("enq_basket_id"));
      addBean.setSubOrderItemId(resultset.getString("suborder_item_id"));
      addBean.setCollegeId(resultset.getString("assoc_id"));
      addBean.setAssociationType(resultset.getString("assoc_type_code"));
      addBean.setProspectusBasketStatus(resultset.getString("status"));
      return addBean;
    }
  }
}
