package com.layer.dao.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.valueobject.CommonVO;

public class CheckIfL2NodeSP extends StoredProcedure {
  public CheckIfL2NodeSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql("wu_util_pkg.to_check_browse_level2_fn");
    declareParameter(new SqlOutParameter("p_status", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_br_browse_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_qualification", OracleTypes.VARCHAR));
    compile();
  }
  //  
  public Map execute(String categoryId, String studyLevelId) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_br_browse_id", categoryId);
      inputMap.put("p_qualification", studyLevelId);
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
  private class CategoryCodeRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      CommonVO commonVO = new CommonVO();
      commonVO.setCategoryCode(resultSet.getString("browse_cat_id"));
      commonVO.setCategoryName(resultSet.getString("description"));
      return commonVO;
    }
  }
}
