package com.layer.dao.sp;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.wuni.util.sql.OracleArray;

/**
  * @UpdateChatBotDisbaleSP.java
  * @Version : initial draft
  * @since : 25_Sep_2018
  * @author : Sabapathi.S
  * @Purpose  : SP for updating in USER attributes if user minimized chatbot
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 
  */
public class UpdateChatBotDisbaleSP extends StoredProcedure {

  DataSource ds = null;

  public UpdateChatBotDisbaleSP(DataSource datasource) {
    setDataSource(datasource);
    this.ds = datasource;
    setSql(SpringConstants.SAVE_USER_DETAILS_PRC);
    declareParameter(new SqlParameter("p_user_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_attr_val_arr", OracleTypes.ARRAY, "HOT_ADMIN.TB_QL_MAS_ATTR_VALUE"));
    //
    compile();
  }

  public void execute(String userId, Object[][] attrValues) {
    Connection connection = null;
    try {
      connection = ds.getConnection();
      ARRAY values = OracleArray.getOracleArray(connection, "HOT_ADMIN.TB_QL_MAS_ATTR_VALUE", attrValues);
      Map inputMap = new HashMap();
      inputMap.put("p_user_id", userId);
      inputMap.put("p_attr_val_arr", values);
      execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (Exception closeException) {
        closeException.printStackTrace();
      }
    }
  }

}
