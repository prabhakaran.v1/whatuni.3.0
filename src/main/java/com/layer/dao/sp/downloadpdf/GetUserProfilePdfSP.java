package com.layer.dao.sp.downloadpdf;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.valueobjects.itext.UserDownloadPdfBean;

/**
  * @see This SP class is used to call the database procedure to get the user details for download pdf
  * @author Hema.S
  * @since  25.09.2018- intital draft
  * @version 1.0
  * *************************************************************************************************************************************
  * Author	               Ver 	        Modified On          	Modification Details
  * *************************************************************************************************************************************
  * Hema S.               wu581                              Generate Pdf for logged in user details..  
  * Sangeeth.S            wu585        12.01.2019            Hanlded pdf download error message
  * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  */
  
public class GetUserProfilePdfSP extends StoredProcedure {
  public GetUserProfilePdfSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_USER_DOWNLOAD_PDF);
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("P_USER_PDF", OracleTypes.CLOB));
    declareParameter(new SqlOutParameter("P_ERROR", OracleTypes.VARCHAR));//Added out parameter for the error handling by Sangeeth.S for 12_FEB_2019_REL
    compile();
  }
  public Map execute(UserDownloadPdfBean userDownloadPdfBean){
    Map outMap = null;
    try{
      Map inputMap = new HashMap();
      inputMap.put("P_USER_ID", userDownloadPdfBean.getUserId());
      outMap = execute(inputMap); 
    }
    catch(Exception ex){
      ex.printStackTrace();
    }
    return outMap;
  }
}
