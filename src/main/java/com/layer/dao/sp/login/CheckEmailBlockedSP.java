package com.layer.dao.sp.login;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;

/**
  * @CheckEmailBlockedSP
  * @author Thiyagu G.
  * @version 1.0
  * @since 28.06.2016
  * @purpose This program is used check the provider is deleted or not and return YES/NO flag
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class CheckEmailBlockedSP extends StoredProcedure {
  public CheckEmailBlockedSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.CHECK_USER_STATUS_FN);
    declareParameter(new SqlOutParameter("p_user_status", OracleTypes.NUMERIC));
    declareParameter(new SqlParameter("p_email_address", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
  }
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_email_address", registrationBean.getEmailAddress());
      inMap.put("p_user_id", registrationBean.getUserId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
