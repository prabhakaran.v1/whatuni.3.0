package com.layer.dao.sp.login;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;

/**
  * @CheckEmailExistSP
  * @author Thiyagu G.
  * @version 1.0
  * @since 28.06.2016
  * @purpose This program is used check the email address is existing or not.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class CheckEmailExistSP extends StoredProcedure {
  public CheckEmailExistSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.CHECK_EMAIL_EXISTS_FN);
    declareParameter(new SqlOutParameter("p_user_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("a", Types.NUMERIC));
    declareParameter(new SqlParameter("p_email_id", Types.VARCHAR));
  }
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("a", registrationBean.getAffilateId());
      inMap.put("p_email_id", registrationBean.getEmailAddress());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
