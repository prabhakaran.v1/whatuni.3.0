package com.layer.dao.sp.login;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;

/**
  * This SP is used to check the user authentication.
  * @author Thiyagu G.
  * @version 1.0
  * @since 28_June_2016 Release.
  * Change Log
  * *******************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
  * *******************************************************************************************************************************
  *
  *
  */
public class CheckUserAuthenticationSP extends StoredProcedure {
  public CheckUserAuthenticationSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.CHECK_USER_AUTHENTICATE_FN);
    declareParameter(new SqlOutParameter("User_Auth", OracleTypes.CURSOR, new UserAuthenticationRowMapperImpl()));
    declareParameter(new SqlParameter("p_page", Types.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_x", Types.VARCHAR));
    declareParameter(new SqlParameter("p_y", Types.VARCHAR));
    declareParameter(new SqlParameter("p_a", Types.VARCHAR));    
  }
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_page", registrationBean.getPageId());
      inMap.put("p_college_id", registrationBean.getCollegeId());
      inMap.put("p_x", registrationBean.getSessionId());
      inMap.put("p_y", registrationBean.getUserId());
      inMap.put("p_a", registrationBean.getAffilateId());      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class UserAuthenticationRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      RegistrationBean registrationVO = new RegistrationBean();
      registrationVO.setPageId(rs.getString(1));
      registrationVO.setSessionId(rs.getString(2));
      registrationVO.setUserId(rs.getString(3));
      return registrationVO;
    }
  }
}
