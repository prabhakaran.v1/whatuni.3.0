package com.layer.dao.sp.login;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.profile.bean.ProfileBean;
import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;

/**
  * @GetUserInformationLoginSP
  * @author Prabhakaran V.
  * @version 1.0
  * @since 28.06.2016
  * @purpose This program is used to get the user information
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 25_Sep_2018    Sabapathi          Received chatbot flag from user attributes
  */
public class GetUserInformationLoginSP extends StoredProcedure {
  public GetUserInformationLoginSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_USER_INFO_FN);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.CURSOR, new InsightInfoRowMapperImpl()));
    declareParameter(new SqlParameter("p_user_id", Types.NUMERIC));
  }
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_user_id", registrationBean.getUserId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class InsightInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ProfileBean profileVO = new ProfileBean();
      profileVO.setUserName(rs.getString("user_name"));
      profileVO.setForeName(rs.getString("forename"));
      profileVO.setConfirmEmailId(rs.getString("user_email"));
      profileVO.setPassword(rs.getString("password"));
      profileVO.setStatusFlag(rs.getString("mandatory_flag"));
      profileVO.setImageName(rs.getString("user_image_details"));
      profileVO.setImageFlag(rs.getString("user_image_details_type"));
      profileVO.setMychNavCnt(rs.getString("final_choices_cnt"));
      profileVO.setIsChatbotDisbaled(rs.getString("hide_chatbot"));
      return profileVO;
    }
  }
}
