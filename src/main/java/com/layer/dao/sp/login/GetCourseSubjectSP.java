package com.layer.dao.sp.login;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;

/**
  * @GetCourseSubjectSP
  * @author Prabhakaran V.
  * @version 1.0
  * @since 28.06.2016
  * @purpose This program is used get the WU sysvar value
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class GetCourseSubjectSP extends StoredProcedure {
  public GetCourseSubjectSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.WU_SUBJECT_LIST_FN);
    declareParameter(new SqlOutParameter("Ret_Sub", OracleTypes.CURSOR, new CourseSubjectRowMapperImpl()));    
  }
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class CourseSubjectRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      RegistrationBean registrationVO = new RegistrationBean();      
      registrationVO.setOptionId(rs.getString("subject_id"));
      registrationVO.setCategoryCode(rs.getString("category_code"));
      registrationVO.setOptionText(rs.getString("subject_name"));
      return registrationVO;
    }
  }
}
