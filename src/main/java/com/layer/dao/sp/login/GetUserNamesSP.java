package com.layer.dao.sp.login;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
  * @GetUserNamesSP
  * @author Prabhakaran V.
  * @version 1.0
  * @since 21.06.2019
  * @purpose This program is used get the users first name and last name
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */

public class GetUserNamesSP extends StoredProcedure {

  public GetUserNamesSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_FIRST_LAST_NAME_FN);
    declareParameter(new SqlOutParameter("PC_USER_NAME", OracleTypes.CURSOR, new UserNamesRowMapperImpl()));
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    compile();
  }

  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", registrationBean.getUserId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }

  public class UserNamesRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      RegistrationBean registrationVO = new RegistrationBean();
      registrationVO.setFirstName(resultset.getString("FIRSTNAME"));
      registrationVO.setLastName(resultset.getString("LASTNAME"));
      registrationVO.setUserName(resultset.getString("FULLNAME"));
      return registrationVO;
    }

  }

}
