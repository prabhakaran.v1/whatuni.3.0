package com.layer.dao.sp.login;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;

/**
  * @GetWUSysVarValueSP
  * @author Prabhakaran V.
  * @version 1.0
  * @since 28.06.2016
  * @purpose This program is used get the WU sysvar value
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class GetWUSysVarValueSP extends StoredProcedure {
  public GetWUSysVarValueSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.WU_SYSVAR_VALUE_FN);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_name", Types.VARCHAR));
  }
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_name", registrationBean.getDefaultValue());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
