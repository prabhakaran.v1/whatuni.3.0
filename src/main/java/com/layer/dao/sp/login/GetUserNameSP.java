package com.layer.dao.sp.login;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;
/**
  * @GetUserNameSP
  * @author Prabhakaran V.
  * @version 1.0
  * @since 28.01.2016
  * @purpose This program is used check the provider is deleted or not and return YES/NO flag
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class GetUserNameSP extends StoredProcedure {
  public GetUserNameSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_USER_NAME_FN);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.NUMERIC));
  }
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_user_id", registrationBean.getUserId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}

