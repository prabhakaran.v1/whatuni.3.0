package com.layer.dao.sp.login;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;

/**
  * @GetUserInformationLoginSP
  * @author Prabhakaran V.
  * @version 1.0
  * @since 28.06.2016
  * @purpose This program is used check the user login status
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class PreLoginUpdateStatusCodeSP extends StoredProcedure {
  public PreLoginUpdateStatusCodeSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.PRE_LOGIN_STATUS_CODE_FN);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("a", Types.NUMERIC));
    declareParameter(new SqlParameter("p_review_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
  }
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("a", registrationBean.getAffilateId());
      inMap.put("p_review_id", registrationBean.getReviewId());
      inMap.put("p_user_id", registrationBean.getUserId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
