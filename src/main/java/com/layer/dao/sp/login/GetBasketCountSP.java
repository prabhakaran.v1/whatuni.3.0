package com.layer.dao.sp.login;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;

/**
  * @GetBasketCountSP
  * @author Thiyagu G.
  * @version 1.0
  * @since 28.06.2016
  * @purpose This program is used check the email address is existing or not.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class GetBasketCountSP extends StoredProcedure {
  public GetBasketCountSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_BASKET_COUNT_FN);
    declareParameter(new SqlOutParameter("Ret_Cnt", OracleTypes.NUMERIC));
    declareParameter(new SqlParameter("p_basket_id", Types.NUMERIC));
    
  }
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_basket_id", registrationBean.getBasketId());      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
