package com.layer.dao.sp.login;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;

/**
  * @GetUserYearOfEntrySP
  * @author Thiyagu G.
  * @version 1.0
  * @since 28.06.2016
  * @purpose This program is used get the user's year of entry.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class GetUserYearOfEntrySP extends StoredProcedure {
  public GetUserYearOfEntrySP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.USER_YEAR_OF_ENTRY_FN);
    declareParameter(new SqlOutParameter("p_user_year", OracleTypes.VARCHAR));    
    declareParameter(new SqlParameter("p_user_id", Types.NUMERIC));
  }
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();      
      inMap.put("p_user_id", registrationBean.getUserId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
