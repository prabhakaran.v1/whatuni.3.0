package com.layer.dao.sp;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.search.form.SearchBean;
import WUI.utilities.GlobalConstants;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : GetPdfDataSP.java
 * Description   : This class used to get the String of XML content to generate PDF file
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * Prabhakaran V.          1.0             06.11.2015           First draft   		
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
public class GetPdfDataSP extends StoredProcedure {
  public GetPdfDataSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_PDF_XML_CONTENT_PRC);
    declareParameter(new SqlParameter("P_COURSE_ID", OracleTypes.NUMERIC));
    declareParameter(new SqlParameter("P_COLLEGE_ID", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("P_RET_PDF", OracleTypes.CLOB));
    compile();
  }
  /**
   * /////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see Method to get the xml content
   * @param collegeId
   * @param courseId
   * @return Map
   * /////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public Map execute(SearchBean searchbean) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("P_COURSE_ID", searchbean.getCourseid());
      inputMap.put("P_COLLEGE_ID", searchbean.getCollegeId());
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
}
