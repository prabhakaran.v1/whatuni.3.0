package com.layer.dao.sp.coursesearch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.util.valueobject.coursesearch.CourseSearchVO;

/**
   * @CourseSearchSP
   * @author Thiyagu G
   * @version 1.0
   * @since 21.03.2017
   * @purpose  This program is used call the database procedure to get popular subjects list.
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.       Changes desc                                      Rel Ver.
   * *************************************************************************************************************************
   * 21-Mar-2017    Thiyagu G                 1.0        Initial draft to spring                           1.0
   *
   */
public class GetPopularSubjectsSP extends StoredProcedure {
  public GetPopularSubjectsSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_SUBJECT_POD_FN);
    declareParameter(new SqlOutParameter("pc_get_subject_guides", OracleTypes.CURSOR, new CourseSearchSubjectGuidesRowMapperImpl()));    
    declareParameter(new SqlParameter("p_track_session_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.NUMERIC));    
    declareParameter(new SqlParameter("p_qual", Types.VARCHAR));
    declareParameter(new SqlParameter("p_clearing", Types.VARCHAR));
  }

  /**
    * This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(CourseSearchVO csVO) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_track_session_id", csVO.getTrackSessionId());
      inMap.put("p_user_id", csVO.getUserId());
      inMap.put("p_qual", csVO.getStudyLevelId());
      inMap.put("p_clearing", csVO.getClearingFlag());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  
  public class CourseSearchSubjectGuidesRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {      
      CourseSearchVO courseSearchVO = new CourseSearchVO();
      try{
        courseSearchVO.setDescription(rs.getString("description"));
        courseSearchVO.setSubjectGuideUrl(rs.getString("url"));
        courseSearchVO.setReadCount(rs.getString("read_count"));
        courseSearchVO.setReadCountPercent(rs.getString("read_count_percent"));
        courseSearchVO.setTotalCount(rs.getString("total_count"));
      } catch (Exception e) {
        e.printStackTrace();
      }
      return courseSearchVO;
    }
  }
}