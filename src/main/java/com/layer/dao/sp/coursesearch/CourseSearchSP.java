package com.layer.dao.sp.coursesearch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import WUI.utilities.GlobalConstants;
import com.layer.util.rowmapper.widget.UltimateSearchResultswcidRowMapperImpl;
import com.wuni.ultimatesearch.UltimateSearchInputDetailsRowmapper;
import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;
import spring.valueobject.findacourse.ArticlePodVO;
import com.wuni.util.valueobject.coursesearch.CourseSearchVO;
import spring.valueobject.findacourse.PopularSubjectVO;

/**
   * @CourseSearchSP
   * @author Thiyagu G
   * @version 1.0
   * @since 21.07.2015
   * @purpose  This program is used call the database procedure to build the course search page.
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.       Changes desc                                      Rel Ver.
   * *************************************************************************************************************************
   * 21-July-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
   * 16-FEB-2016   Prabhakaran V.            1.1        Remove old common row mapper and added new row mapper for ultimate search
   * 08-DEC-2020   Keerthana E               WUNI_923   Added popular subject and article cursor
   */
public class CourseSearchSP extends StoredProcedure {
  public CourseSearchSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.MY_COURSE_SEARCH_PRC);
    declareParameter(new SqlParameter("p_track_session_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMERIC));    
    declareParameter(new SqlParameter("p_qual", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_clearing", OracleTypes.VARCHAR));
  
    declareParameter(new SqlOutParameter("P_BREADCRUMB", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("P_IWTB_RETURN_CODE", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("P_IWTB_REMAINING_PERCENT", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("P_WCID_RETURN_CODE", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("P_WCID_REMAINING_PERCENT", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("P_WCID_MESSAGE_CODE", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("P_WCID_RESULTS_GRADES", OracleTypes.VARCHAR));
    
        
    declareParameter(new SqlOutParameter("PC_IWTB_RESULTS", OracleTypes.CURSOR, new CourseUltimateSearchRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_WCID_INPUT_DETAILS", OracleTypes.CURSOR, new UltimateSearchInputDetailsRowmapper()));
    declareParameter(new SqlOutParameter("PC_WCID_RESULTS", OracleTypes.CURSOR, new UltimateSearchResultswcidRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_POPULAR_SUBJECT", OracleTypes.CURSOR, new PopularSubjectsRowMapperImpl())); //Added for popular subjects in find a course by keerthana E for dec 08 2020 rel
    declareParameter(new SqlOutParameter("PC_ARTICLE_LIST", OracleTypes.CURSOR, new ArticlePodRowMapperImpl())); //Added for article list in find a course by keerthana E for dec 08 2020 rel
    declareParameter(new SqlOutParameter("P_SNIPPET_TEXT", OracleTypes.VARCHAR)); //Added for snippet section in find a course by keerthana E for dec 08 2020 rel
  }

  /**
    * This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List parameters) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_track_session_id", parameters.get(0));
      inMap.put("p_user_id", parameters.get(1));      
      inMap.put("p_qual", parameters.get(2));
      inMap.put("p_clearing", parameters.get(3));      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    return outMap;
  }
  
  public class CourseSearchSubjectGuidesRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {      
      CourseSearchVO courseSearchVO = new CourseSearchVO();
      try{
        courseSearchVO.setDescription(rs.getString("description"));
        courseSearchVO.setSubjectGuideUrl(rs.getString("url"));
        courseSearchVO.setReadCount(rs.getString("read_count"));
        courseSearchVO.setReadCountPercent(rs.getString("read_count_percent"));
        courseSearchVO.setTotalCount(rs.getString("total_count"));
      } catch (Exception e) {
        e.printStackTrace();
      }
      return courseSearchVO;
    }
  }
  /**
   * Private class for ultimate search Added by Prabha on 16-02-2016
   */
  public class CourseUltimateSearchRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {      
      IWanttobeBean iwanttobeResultsVO = new IWanttobeBean();
      iwanttobeResultsVO.setJacsCode(rs.getString("jacs_code"));
      iwanttobeResultsVO.setJacsSubject(rs.getString("jacs_subject"));
      iwanttobeResultsVO.setIwantSearchCode(rs.getString("job_or_industry_id"));
      iwanttobeResultsVO.setIwantSearchName(rs.getString("job_or_industry_name"));
      iwanttobeResultsVO.setImagePath(rs.getString("image_path"));
      iwanttobeResultsVO.setCreatedDate(rs.getString("created_date"));
      return iwanttobeResultsVO;
    }
  }
  
  /**
   * Private class for popular subjects Added by keerthana on 08-12-2020
   */
  
  public class PopularSubjectsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      PopularSubjectVO popularSubjectVO = new PopularSubjectVO();
      popularSubjectVO.setSubjectName(rs.getString("subject_name"));
      popularSubjectVO.setSubjectUrl(rs.getString("url"));
      return popularSubjectVO;
    }
  }
  
  /**
   * Private class for article pod Added by keerthana on 08-12-2020
   */
  
  public class ArticlePodRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ArticlePodVO articlePodVO = new ArticlePodVO();
      articlePodVO.setArticleTitle(rs.getString("article_title"));
      articlePodVO.setMediaPath(rs.getString("media_path"));
      articlePodVO.setPersonalizedText(rs.getString("personalized_text"));
      articlePodVO.setUrl(rs.getString("url"));
      articlePodVO.setDisplayOrder(rs.getString("display_order"));
      return articlePodVO;
    }
  }
}