package com.layer.dao.sp.mywhatuni;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.valueobject.opendays.OpenDaysCalendarVO;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;

import com.layer.util.rowmapper.mywhatuni.MywuOpendayCPEPod;
import com.layer.util.rowmapper.mywhatuni.MywuOpendayPostenuiry;

public class MyWUOpendayAjaxSP extends StoredProcedure
{

  public MyWUOpendayAjaxSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql("wu_user_profile_pkg.get_user_opendays_ajax_prc");
    declareParameter(new SqlParameter("p_month", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_year", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_networ_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_date", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("pc_avail_opendays", OracleTypes.CURSOR, new UserAvailableOpenDays()));
    declareParameter(new SqlOutParameter("pc_opendays_calendar", OracleTypes.CURSOR, new userOpendayRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_pe_my_opendays", OracleTypes.CURSOR, new MywuOpendayPostenuiry()));
    declareParameter(new SqlOutParameter("pc_cpe_pod_opendays", OracleTypes.CURSOR, new MywuOpendayCPEPod()));
    declareParameter(new SqlOutParameter("p_next_open_month", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_past_od_flag", OracleTypes.VARCHAR));
  }
  
  public Map execute(List inputList){
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_month", inputList.get(0));
    inMap.put("p_year", inputList.get(1));
    inMap.put("p_networ_id", inputList.get(2));
    inMap.put("p_date", inputList.get(3));
    inMap.put("p_page_no", inputList.get(4));
    inMap.put("p_user_id", inputList.get(5));
    inMap.put("p_flag", inputList.get(6));
    outMap = execute(inMap);
    return outMap;
  }  
  private class UserAvailableOpenDays implements RowMapper{
      public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
          OpenDaysCalendarVO openDaysCalendarVO = new OpenDaysCalendarVO();
          openDaysCalendarVO.setOpenDate_day(rs.getString("open_date"));
          openDaysCalendarVO.setPastFutureDate(rs.getString("past_future_date"));
          return openDaysCalendarVO;
      }
  }   

  private class userOpendayRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      OpenDaysCalendarVO openDaysCalendarVO = new OpenDaysCalendarVO();
      openDaysCalendarVO.setCollegeId(rs.getString("college_id"));
      openDaysCalendarVO.setCollegeName(rs.getString("college_name"));
      openDaysCalendarVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      openDaysCalendarVO.setCollegeLocation(rs.getString("college_location"));
      openDaysCalendarVO.setCollegeDepartmentDescription(rs.getString("open_day_type"));
      openDaysCalendarVO.setCollegeOpendayExternalUrl(rs.getString("openday_external_url"));
      openDaysCalendarVO.setOpenDate(rs.getString("open_date"));
      //Added By Sekhar for wu_320
      openDaysCalendarVO.setOpenDay_day(rs.getString("open_day"));
      openDaysCalendarVO.setOpenDay_date(rs.getString("open_day_date"));
      openDaysCalendarVO.setOpenDay_month_year(rs.getString("open_month"));
      openDaysCalendarVO.setTotalRecordsCount(rs.getString("total_cnt"));
      openDaysCalendarVO.setTotalRecordsCount(rs.getString("total_cnt"));
      openDaysCalendarVO.setPersonalNotes(rs.getString("personal_notes"));
      openDaysCalendarVO.setEventId(rs.getString("event_id"));
      openDaysCalendarVO.setPastFutureDate(rs.getString("past_future_date"));
        String tmpUrl = "";
        ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("ENQUIRY_DETAILS");
        try {
          if (enquiryDetailsRS != null) {
            while (enquiryDetailsRS.next()) {
              openDaysCalendarVO.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
              openDaysCalendarVO.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
              openDaysCalendarVO.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
              openDaysCalendarVO.setMyhcProfileId(enquiryDetailsRS.getString("MYHC_PROFILE_ID"));
              openDaysCalendarVO.setProfileType(enquiryDetailsRS.getString("PROFILE_TYPE"));
              openDaysCalendarVO.setCpeQualificationNetworkId(enquiryDetailsRS.getString("NETWORK_ID"));
              tmpUrl = enquiryDetailsRS.getString("WEBSITE");
              if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else {
                tmpUrl = "";
              }
              openDaysCalendarVO.setSubOrderWebsite(tmpUrl);
              tmpUrl = enquiryDetailsRS.getString("EMAIL_WEBFORM");
              if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else {
                tmpUrl = "";
              }
              openDaysCalendarVO.setSubOrderEmailWebform(tmpUrl);
              tmpUrl = enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
              if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                  tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
              } else {
                tmpUrl = "";
              }
              openDaysCalendarVO.setSubOrderProspectusWebform(tmpUrl);
              openDaysCalendarVO.setMediaId("-999");
              openDaysCalendarVO.setMediaType("picture");
              openDaysCalendarVO.setMediaPath("/img/whatuni/unihome_default.png");
              if (enquiryDetailsRS.getString("MEDIA_ID") != null && enquiryDetailsRS.getString("MEDIA_TYPE") != null && enquiryDetailsRS.getString("MEDIA_PATH") != null) {
                openDaysCalendarVO.setMediaId(enquiryDetailsRS.getString("MEDIA_ID"));
                openDaysCalendarVO.setMediaPath(enquiryDetailsRS.getString("MEDIA_PATH"));
                openDaysCalendarVO.setMediaThumbPath(enquiryDetailsRS.getString("THUMB_MEDIA_PATH"));
                openDaysCalendarVO.setMediaType(enquiryDetailsRS.getString("MEDIA_TYPE").toLowerCase());
                if (openDaysCalendarVO.getMediaType().equals("video")) {
                  openDaysCalendarVO.setMediaThumbPath(new GlobalFunction().videoThumbnailFormatChange(openDaysCalendarVO.getMediaPath(), "2"));
                }
              }
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          try {
            if (enquiryDetailsRS != null) {
              enquiryDetailsRS.close();
            }
          } catch (Exception e) {
            throw new SQLException(e.getMessage());
          }
        }
      String emailurlString = null;
      String collegeName = openDaysCalendarVO.getCollegeName();
      CommonFunction common = new CommonFunction();
      if (collegeName != null && collegeName.trim().length() > 0) {
        collegeName = collegeName.trim();
        collegeName = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(collegeName)));
      }
      emailurlString = "/degrees/email/" + collegeName + "-email";
      emailurlString = emailurlString + "/" + openDaysCalendarVO.getCollegeId(); //collegeId
      emailurlString = emailurlString + "/0"; //courseId
      emailurlString = emailurlString + "/0";
      emailurlString = emailurlString + "/" +  "n-" + openDaysCalendarVO.getSubOrderItemId(); //openday-subOrderItemId
      if("Y".equalsIgnoreCase(openDaysCalendarVO.getPastFutureDate()))
      {
        emailurlString = emailurlString + "/send-college-email.html?p_open_day="+openDaysCalendarVO.getOpenDay_date()+" "+openDaysCalendarVO.getOpenDay_month_year();
      }else
      {
        emailurlString = emailurlString + "/send-college-email.html";
      }
      emailurlString = emailurlString.toLowerCase();      
      openDaysCalendarVO.setEmailURLString(emailurlString);
      return openDaysCalendarVO;
    }
  }
  
}
