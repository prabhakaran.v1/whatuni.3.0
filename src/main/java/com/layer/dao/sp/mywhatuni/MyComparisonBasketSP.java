package com.layer.dao.sp.mywhatuni;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.layer.util.rowmapper.basket.CompareBasketRowMapperImpl;
import com.wuni.mywhatuni.form.MyCompareBean;

/**
  * @Version :  .0
  * @May 17 2015
  * @www.whatuni.com
  * @Created By : Priyaa Parthasarathy
  * @Purpose  : This program is used to draw the mycomparison page.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 17-May-2015  Priyaa Parthasarathy         1.0
  * 16-May-2017  Prabhakaran V.               1.1     Merging comparison and final five                               wu_564
  */
public class MyComparisonBasketSP extends StoredProcedure {
  public MyComparisonBasketSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.COMPARE_COURSE_UNI_PRC);
    declareParameter(new SqlParameter("p_basket_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_assoc_type_code", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_session_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_factors", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_sess_basket_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_type", OracleTypes.VARCHAR)); //30_JUN_2015_REL
    declareParameter(new SqlOutParameter("PC_COMPARE", OracleTypes.CURSOR, new CompareBasketRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_ADD_FACTORS", OracleTypes.CURSOR, new BasketFactorsMapperImpl()));
    declareParameter(new SqlOutParameter("P_DEF_FACTORS", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_VISUAL_TYPE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_BREAD_CRUMB", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_CURR_BASKET_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_HELP_TEXT", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_choice_status", OracleTypes.VARCHAR));
    compile();
  }
  //
  public Map execute(MyCompareBean inputBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_basket_id", inputBean.getBasketId());
      inMap.put("p_assoc_type_code", "");
      inMap.put("p_user_id", inputBean.getUserId());
      inMap.put("p_session_id", inputBean.getUserSessionId());
      inMap.put("p_factors", inputBean.getFactors());
      inMap.put("p_sess_basket_id", inputBean.getSessionBasketId());
      inMap.put("p_user_type", inputBean.getUserType());
      outMap = execute(inMap);

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
  //
  public class BasketFactorsMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      MyCompareBean bean = new MyCompareBean();
      bean.setFactorId(resultset.getString("factor_id"));
      bean.setFactorName(resultset.getString("factor_name"));
      bean.setFactorStatus(resultset.getString("status"));
      return bean;
    }
  }
}
