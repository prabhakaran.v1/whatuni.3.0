package com.layer.dao.sp.mywhatuni;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;

import com.layer.util.SpringConstants;
import com.wuni.mywhatuni.form.MyCompareBean;
import com.wuni.util.seo.SeoUrls;
import com.wuni.valueobjects.FinalFiveChoiceVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : SuggestedComparisonPodSP.java
 * Description   : This class is used to get the suggested uni/course for comparison..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Prabhakaran V.           1.0             16.05.2017      First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
public class SuggestedComparisonPodSP extends StoredProcedure {
  String imgPath = new CommonUtil().getImgPath("", 0);
  public SuggestedComparisonPodSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(SpringConstants.SUGGESTED_COMPARISON_PRC);
    declareParameter(new SqlOutParameter("PC_COMPARE_SUG", OracleTypes.CURSOR, new CompareSuggestionInfoRowMapperImpl()));
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_SUGG_TYPE", OracleTypes.VARCHAR));
    compile();
  }
  public Map execute(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("P_USER_ID", finalFiveChoiceVO.getUserId());
    inMap.put("P_SUGG_TYPE", finalFiveChoiceVO.getAssociateTypeCode());

    outMap = execute(inMap);
    return outMap;
  }
  private class CompareSuggestionInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      MyCompareBean bean = new MyCompareBean();
      bean.setCollegeId(rs.getString("college_id"));
      bean.setCollegeDisplayName(rs.getString("college_name_display"));
      bean.setCollegeLog(rs.getString("college_logo"));
      bean.setCourseId(rs.getString("course_id"));
      bean.setCourseTitle(rs.getString("course_title"));
      bean.setStudyLevel(rs.getString("study_level_desc"));
      if(!GenericValidator.isBlankOrNull(bean.getCollegeLog())){
        bean.setCollegeLog(new CommonUtil().getImgPath(bean.getCollegeLog(), rowNum));
      }else{
        bean.setCollegeLog(imgPath + GlobalConstants.DEFAULT_UNI_IMG_PATH);
      }
      SeoUrls seoUrls = new SeoUrls();
      if(!GenericValidator.isBlankOrNull(bean.getCollegeId())){
        bean.setUniHomeURL(seoUrls.construnctUniHomeURL(bean.getCollegeId(), bean.getCollegeDisplayName()));
      }
      if(!GenericValidator.isBlankOrNull(bean.getCourseId())){
        bean.setCourseUrl(seoUrls.construnctCourseDetailsPageURL(bean.getStudyLevel(), bean.getCourseTitle(), bean.getCourseId(), bean.getCollegeId(), "COURSE_SPECIFIC"));
      }  
      return bean;
    }
  }
}
