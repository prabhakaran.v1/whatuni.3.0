package com.layer.dao.sp.mywhatuni;

import java.sql.Connection;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.sql.OracleArray;

public class MywhatuniPostenquirySP extends StoredProcedure {
  DataSource ds = null;

  public MywhatuniPostenquirySP(DataSource datasource) {
    setDataSource(datasource);
    this.ds = datasource;
    setSql("wu_user_profile_pkg.wu_post_enq_pros_submit_prc");
    declareParameter(new SqlParameter("p_affiliate_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_session_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_user_agent", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_ip", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_request_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_referer_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_mywhatuni_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_mobile_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_attr_val_arr", OracleTypes.ARRAY, "TB_PE_PROSPECTUS_SUB"));
    declareParameter(new SqlParameter("p_track_session", Types.VARCHAR));
    compile();
    }
    public Map execute(List inputList, Object[][] postValues){
      Map outMap = null;
      Connection connection = null;
      /*for(int i=0; i<postValues.length;i++)
          {
            for(int j=0; j<postValues[i].length;j++) 
            {
              System.out.println("attrvalues : "+i+" "+j+" "+  postValues[i][j]);
            }
            
          }*/      
      try{
          Map inMap = new HashMap();
          connection = ds.getConnection();
          ARRAY values = OracleArray.getOracleArray(connection, "TB_PE_PROSPECTUS_SUB", postValues);
          inMap.put("p_affiliate_id", inputList.get(0));
          inMap.put("p_user_id", inputList.get(1));
          inMap.put("p_session_id", inputList.get(2));
          inMap.put("p_user_agent", inputList.get(3));
          inMap.put("p_user_ip", inputList.get(4));
          inMap.put("p_request_url", inputList.get(5));
          inMap.put("p_referer_url", inputList.get(6));
          inMap.put("p_mywhatuni_flag", inputList.get(7));
          inMap.put("p_mobile_flag", inputList.get(8));
          inMap.put("p_attr_val_arr", values);
          inMap.put("p_track_session", inputList.get(9));          
          outMap = execute(inMap);
        }catch(Exception e){
          e.printStackTrace();
        }finally{
          try{
            connection.close();
          }catch(Exception d){
            d.printStackTrace();
          }
        }
    return outMap;    
    }
}
