package com.layer.dao.sp.mywhatuni;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.basket.CompareBasketRowMapperImpl;
import com.wuni.valueobjects.FinalFiveChoiceVO;

/**
  * @EditConfirmChoiceSP
  * @author Prabhakaran V.
  * @version 1.0
  * @since 04.04.2017
  * @purpose This program is used edit/confirm the final five choice in a basket..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class EditConfirmChoiceSP extends StoredProcedure {
  public EditConfirmChoiceSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.EDIT_CONFIRM_CHOICES_PRC);
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_BASKET_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_INPUT_TYPE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_COMPARE", OracleTypes.CURSOR, new CompareBasketRowMapperImpl()));
    declareParameter(new SqlOutParameter("P_RETURN_CODE", OracleTypes.VARCHAR));
    compile();
  }
  public Map execute(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("P_USER_ID", finalFiveChoiceVO.getUserId());
    inMap.put("P_BASKET_ID", finalFiveChoiceVO.getBasketId());
    inMap.put("P_INPUT_TYPE", finalFiveChoiceVO.getInputType());

    outMap = execute(inMap);
    return outMap;
  }
}
