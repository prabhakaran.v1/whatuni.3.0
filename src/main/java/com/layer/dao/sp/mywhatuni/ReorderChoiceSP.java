package com.layer.dao.sp.mywhatuni;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.basket.CompareBasketRowMapperImpl;
import com.wuni.valueobjects.FinalFiveChoiceVO;

/**
  * @ReorderChoiceSP
  * @author Prabhakaran V.
  * @version 1.0
  * @since 04.04.2017
  * @purpose This program is used swap the final five choice in a basket..
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class ReorderChoiceSP extends StoredProcedure {
  public ReorderChoiceSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(SpringConstants.REORDER_FINAL_CHOICE_FN);
    declareParameter(new SqlOutParameter("PC_COMPARE", OracleTypes.CURSOR, new CompareBasketRowMapperImpl()));
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_BASKET_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_POSITION_ID", Types.NUMERIC));
    declareParameter(new SqlParameter("P_DIRECTION", OracleTypes.VARCHAR));
    compile();
  }
  public Map execute(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("P_USER_ID", finalFiveChoiceVO.getUserId());
    inMap.put("P_BASKET_ID", finalFiveChoiceVO.getBasketId());
    inMap.put("P_POSITION_ID", finalFiveChoiceVO.getSwapPosition());
    inMap.put("P_DIRECTION", finalFiveChoiceVO.getSwapDirection());
  
    outMap = execute(inMap);
    return outMap;
  }
}
