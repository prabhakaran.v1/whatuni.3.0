package com.layer.dao.sp.mywhatuni;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.sql.OracleArray;
import com.wuni.util.valueobject.mywhatuni.MyWhatuniInputVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Class         : MyWhatuniUserAttrSP.java
 * Description   : Class to save the user details in my settings page.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               
 * *************************************************************************************************************************************
 * Indumathi.S             1.1             27.01.2016       Added parameter for saving mailing preferences detials
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
public class MyWhatuniUserAttrSP extends StoredProcedure {

  DataSource ds = null;

  public MyWhatuniUserAttrSP(DataSource datasource) {
    setDataSource(datasource);
    this.ds = datasource;
    setSql("wu_user_profile_pkg.store_user_prof_attr_val_prc");
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_user_study_level", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_attr_val_arr", OracleTypes.ARRAY, "HOT_ADMIN.TB_QL_MAS_ATTR_VALUE"));
    //Added by Indumathi for mailing preferences Jan-27-16
    declareParameter(new SqlParameter("P_RECEIVE_NO_EMAIL_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_PERMIT_EMAIL", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_SOLUS_EMAIL", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_MARKETING_EMAIL", OracleTypes.VARCHAR));
    //End    
    declareParameter(new SqlParameter("P_SURVEY_EMAIL", OracleTypes.VARCHAR)); //Added survey mail flag in mailing preferences for 31_May_2016, By Thiyagu G.
    declareParameter(new SqlParameter("P_PRIVACY", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_CA_TRACK_FLAG", OracleTypes.VARCHAR)); //Added parameter to update ca track flag for 04-09-2016, By Thiyagu G.
    declareParameter(new SqlOutParameter("p_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_REVIEW_SURVEY_FLAG", OracleTypes.VARCHAR));
  }

  public Map execute(MyWhatuniInputVO inputBean, Object[][] attrValues) {
    Map outMap = null;
    Connection connection = null;
    /*for(int i=0; i<attrValues.length;i++){
        for(int j=0; j<attrValues[i].length;j++){
          System.out.println("attrvalues"+i+" "+j+" "+  attrValues[i][j]);
        }
      }*/
    try {
      connection = ds.getConnection();
      ARRAY values = OracleArray.getOracleArray(connection, "HOT_ADMIN.TB_QL_MAS_ATTR_VALUE", attrValues);
      Map inMap = new HashMap();
      inMap.put("p_user_id", inputBean.getUserId());
      inMap.put("p_user_study_level", inputBean.getUserAttributeType());
      inMap.put("p_attr_val_arr", values);
      inMap.put("P_RECEIVE_NO_EMAIL_FLAG", inputBean.getReceiveNoEmailFlag());
      inMap.put("P_PERMIT_EMAIL", inputBean.getPermitEmail());
      inMap.put("P_SOLUS_EMAIL", inputBean.getSolusEmail());
      inMap.put("P_MARKETING_EMAIL", inputBean.getMarkettingEmail());
      inMap.put("P_SURVEY_EMAIL", inputBean.getSurveyMail()); //Added survey mail flag in mailing preferences for 31_May_2016, By Thiyagu G.
      inMap.put("P_PRIVACY", inputBean.getPrivacy());
      inMap.put("P_CA_TRACK_FLAG", inputBean.getCaTrackFlag());
      inMap.put("P_REVIEW_SURVEY_FLAG", inputBean.getReviewSurveyFlag());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        connection.close();
      } catch (Exception d) {
        d.printStackTrace();
      }
    }
    return outMap;
  }

}
