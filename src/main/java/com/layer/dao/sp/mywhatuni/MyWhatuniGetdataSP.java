package com.layer.dao.sp.mywhatuni;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;
import oracle.sql.STRUCT;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.rowmapper.mywhatuni.NationalityCountryRowMapper;
import com.wuni.advertiser.ql.form.QuestionAnswerBean;
import com.wuni.mywhatuni.MywhatuniInputBean;
import com.wuni.util.valueobject.mywhatuni.MyWhatuniGetdataVO;
import com.wuni.util.valueobject.mywhatuni.UserClientConsentVO;
import com.wuni.util.valueobject.mywhatuni.UserConfirmedOfferVO;
import com.wuni.valueobjects.whatunigo.GradeSubjectVO;
import com.wuni.valueobjects.whatunigo.UserQualDetailsVO;

/**
  *
  * MyWhatuniGetdataSP.java
  * @Version : 2.0
  * @www.whatuni.com
  * @Purpose  : This program is used to get the data of the user my settings page.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *  ?												 Initial draft
  *  23.07.20	  Sangeeth.S						 Added hide download flag column  
  */
public class MyWhatuniGetdataSP extends StoredProcedure {

  public MyWhatuniGetdataSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("wu_user_profile_pkg.get_mywhatuni_profile_fields");
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_network_id", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("pc_user_details", OracleTypes.CURSOR, new UserDetailsRowMapper()));
    declareParameter(new SqlOutParameter("pc_user_address", OracleTypes.CURSOR, new UserAddressDetailsRowMapper()));
    declareParameter(new SqlOutParameter("pc_ug_user_attr_fields", OracleTypes.CURSOR, new UserUGAttributeFieldsRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_pg_user_attr_fields", OracleTypes.CURSOR, new UserPGAttributeFieldsRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_nationality", OracleTypes.CURSOR, new NationalityCountryRowMapper()));
    declareParameter(new SqlOutParameter("pc_country", OracleTypes.CURSOR, new NationalityCountryRowMapper()));
    declareParameter(new SqlOutParameter("pc_user_subscription_flags", OracleTypes.CURSOR, new MailingPreferenceRowMapper())); //Added by Indumathi.S Jan-27-16 Rel For Mailing preference
    declareParameter(new SqlOutParameter("p_user_image_details", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_user_image_details_type", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_friends_privacy_flag", OracleTypes.VARCHAR)); //Added flag to get facebook friends activity privacy setting status, 21_July_2015 By Thiyagu G.
    declareParameter(new SqlOutParameter("p_ca_track_flag", OracleTypes.VARCHAR)); //Added flag to get catool track flag for privacy setting status, 04_Oct_2016 By Thiyagu G.
    declareParameter(new SqlOutParameter("PC_USER_CONFIRMED_OFFER", OracleTypes.CURSOR, new userConfirmedOfferDetails()));
    declareParameter(new SqlOutParameter("PC_USER_QUALIFICATIONS", OracleTypes.CURSOR, new UserQualInfoRowmapperImpl()));
    declareParameter(new SqlOutParameter("PC_USER_CONSENT_DETAILS",OracleTypes.CURSOR, new UserConsentDetailsRowMapperImpl()));//Added this for showing consent flag details By Hema.S on 17_12_2019_REL
    declareParameter(new SqlOutParameter("P_HIDE_DOWNLOAD_BUTTON", OracleTypes.VARCHAR));    
  }

  public Map execute(MywhatuniInputBean inputBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_user_id", inputBean.getUserId());
      inMap.put("p_tab", inputBean.getTabName());
      inMap.put("p_network_id", inputBean.getNetworkId());
      inMap.put("p_page_no", inputBean.getPageNo());
      inMap.put("P_ORDER_BY", inputBean.getOrderBy());
      inMap.put("p_cookie_basket_id", inputBean.getCookieBasketId());   
      outMap = execute(inMap);
      System.out.println("pr inmap.." + inMap);
      System.out.println("pr outmap.." + outMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }

  private class UserDetailsRowMapper implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      MyWhatuniGetdataVO userdetailsVO = new MyWhatuniGetdataVO();
      userdetailsVO.setFirstName(rs.getString("forename"));
      userdetailsVO.setLastName(rs.getString("surname"));
      userdetailsVO.setUserName(rs.getString("username"));
      userdetailsVO.setNationality(rs.getString("nationality"));
      userdetailsVO.setDateOfBirth(rs.getString("dob"));
      userdetailsVO.setEmail(rs.getString("user_email"));
      userdetailsVO.setPassword(rs.getString("password"));
      userdetailsVO.setPhoneNumber(rs.getString("user_telephone"));      
      userdetailsVO.setYeartoJoinCourse(rs.getString("year_of_course"));
      userdetailsVO.setDefaultYoe(rs.getString("CURRENT_YEAR_OF_ENTRY"));
      return userdetailsVO;
    }

  }

  private class UserAddressDetailsRowMapper implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      MyWhatuniGetdataVO userAddressVO = new MyWhatuniGetdataVO();
      userAddressVO.setAddressLine1(rs.getString("address_line_1"));
      userAddressVO.setAddressLine2(rs.getString("address_line_2"));
      userAddressVO.setTown(rs.getString("city"));
      userAddressVO.setCountryOfResidence(rs.getString("country_id"));
      userAddressVO.setPostCode(rs.getString("postcode_zip_prefix"));
      return userAddressVO;
    }

  }

  private class UserUGAttributeFieldsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      QuestionAnswerBean qlquestionaansbean = new QuestionAnswerBean();
      qlquestionaansbean.setAttributeId(rs.getString("ATTRIBUTE_ID"));
      qlquestionaansbean.setAttributeName(rs.getString("ATTRIBUTE_NAME"));
      qlquestionaansbean.setDefaultValue(GenericValidator.isBlankOrNull(rs.getString("DEFAULT_VALUE")) ? "" : rs.getString("DEFAULT_VALUE"));
      qlquestionaansbean.setAttributeText(rs.getString("ATTRIBUTE_TEXT"));
      qlquestionaansbean.setOptionFlag(rs.getString("OPTION_FLAG"));
      //      
      java.sql.Array optionsRS = (java.sql.Array)rs.getObject("OPTIONS");// added for weblogic migration 28-JUN_2016_REL
      Object datanum[] = (Object[])optionsRS.getArray();
      ArrayList optionList = new ArrayList();
      for (int i = 0; i < datanum.length; i++) {
        STRUCT struct = (STRUCT)datanum[i];
        Object obj[] = struct.getAttributes();
        QuestionAnswerBean optionsVO = new QuestionAnswerBean();
        optionsVO.setOptionId(String.valueOf(obj[0]));
        optionsVO.setOptionDesc((String)obj[1]);
        optionsVO.setOptionValue((String)obj[2]);
        optionList.add(optionsVO);
      }
      qlquestionaansbean.setOptions(optionList);
      qlquestionaansbean.setOptionId(rs.getString("OPTION_ID"));
      if ("Y".equalsIgnoreCase(rs.getString("OPTION_FLAG"))) {
        qlquestionaansbean.setAttributeValue(qlquestionaansbean.getOptionId());
      } else {
        qlquestionaansbean.setAttributeValue(rs.getString("ATTRIBUTE_VALUE"));
      }
      //
      return qlquestionaansbean;     
    }

  }

  private class UserPGAttributeFieldsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      QuestionAnswerBean qlquestionaansbean = new QuestionAnswerBean();
      qlquestionaansbean.setAttributeId(rs.getString("ATTRIBUTE_ID"));
      qlquestionaansbean.setAttributeName(rs.getString("ATTRIBUTE_NAME"));
      qlquestionaansbean.setDefaultValue(GenericValidator.isBlankOrNull(rs.getString("DEFAULT_VALUE")) ? "" : rs.getString("DEFAULT_VALUE"));
      qlquestionaansbean.setAttributeText(rs.getString("ATTRIBUTE_TEXT"));
      qlquestionaansbean.setOptionFlag(rs.getString("OPTION_FLAG"));
      qlquestionaansbean.setFormatDesc(rs.getString("FORMAT_DESC"));
      qlquestionaansbean.setGroupId(rs.getString("GROUP_ID"));
      qlquestionaansbean.setDisplayFlag(rs.getString("display_flag"));
      
      java.sql.Array optionsRS = (java.sql.Array)rs.getObject("OPTIONS");// added for weblogic migration 28-JUN_2016_REL
      Object datanum[] = (Object[])optionsRS.getArray();
      ArrayList optionList = new ArrayList();
      for (int i = 0; i < datanum.length; i++) {
        STRUCT struct = (STRUCT)datanum[i];
        Object obj[] = struct.getAttributes();
        QuestionAnswerBean optionsVO = new QuestionAnswerBean();
        optionsVO.setOptionId(String.valueOf(obj[0]));
        optionsVO.setOptionDesc((String)obj[1]);
        optionsVO.setOptionValue((String)obj[2]);
        optionList.add(optionsVO);
      }
      qlquestionaansbean.setOptions(optionList);
      qlquestionaansbean.setOptionId(rs.getString("OPTION_ID"));
      if ("Y".equalsIgnoreCase(rs.getString("OPTION_FLAG")) && qlquestionaansbean.getOptionId() != null) {
        if ("CHECKBOX".equalsIgnoreCase(qlquestionaansbean.getFormatDesc())) {
          qlquestionaansbean.setAttributeValue("<" + qlquestionaansbean.getOptionId().replaceAll(",", ">,<") + ">");
        } else {
          qlquestionaansbean.setAttributeValue(qlquestionaansbean.getOptionId());
        }
      } else {
        qlquestionaansbean.setAttributeValue(rs.getString("ATTRIBUTE_VALUE"));
      }
      //
      return qlquestionaansbean;      
    }

  }

  private class MailingPreferenceRowMapper implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      MyWhatuniGetdataVO emailSettingsVO = new MyWhatuniGetdataVO();
      emailSettingsVO.setReceiveNoEmailFlag(rs.getString("receive_no_email_flag"));
      emailSettingsVO.setPermitEmail(rs.getString("permit_email_flag"));
      emailSettingsVO.setSolusEmail(rs.getString("solus_email_flag"));
      emailSettingsVO.setMarkettingEmail(rs.getString("marketing_email_flag"));
      emailSettingsVO.setSurveyMail(rs.getString("survey_flag")); //Added survey mail flag in mailing preferences for 31_May_2016, By Thiyagu G.
      emailSettingsVO.setReviewSurveyFlag(rs.getString("REVIEW_SURVEY_FLAG"));
      return emailSettingsVO;
    }

  }
  private class userConfirmedOfferDetails implements RowMapper {

     public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
       UserConfirmedOfferVO userConfirmedOfferVO = new UserConfirmedOfferVO();
       userConfirmedOfferVO.setCollegeDisplayName(rs.getString("college_name_display"));
       userConfirmedOfferVO.setCollegeLogo(rs.getString("college_logo"));
       userConfirmedOfferVO.setCourseTitle(rs.getString("course_title"));
       userConfirmedOfferVO.setUserSubmittedDate(rs.getString("submitted_date")); 
       return userConfirmedOfferVO;
     }

   }
  private class UserQualInfoRowmapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int num) {
	  UserQualDetailsVO userQualDetailsVO = new UserQualDetailsVO();
	  try {
	    userQualDetailsVO.setEntry_qual_level(rs.getString("qual_level"));
	    userQualDetailsVO.setEntry_qual_id(rs.getString("qual_id"));
	    userQualDetailsVO.setEntry_qual_desc(rs.getString("qualification"));
	    userQualDetailsVO.setGcse_grade_flag(rs.getString("grade_type"));
	    ResultSet userqualRS = (ResultSet)rs.getObject("subject_list");
	    try {
	      if(userqualRS != null){
	        GradeSubjectVO gradeSubjectVO = new GradeSubjectVO();
	        ArrayList qualOptionsList = new ArrayList();
	        gradeSubjectVO = null;
	        while (userqualRS.next()) {
	          gradeSubjectVO = new GradeSubjectVO();
	          gradeSubjectVO.setEntry_subject_id(userqualRS.getString("SUBJECT_ID"));
	          gradeSubjectVO.setEntry_subject(userqualRS.getString("SUBJECT_DESC"));
	          gradeSubjectVO.setEntry_grade(userqualRS.getString("GRADE"));
	          qualOptionsList.add(gradeSubjectVO);
	          System.out.println("SUBJECT_ID..." + userqualRS.getString("SUBJECT_ID"));
	          System.out.println("SUBJECT_DESC..." + userqualRS.getString("SUBJECT_DESC"));
	          System.out.println("GRADE..." + userqualRS.getString("GRADE"));
	        }
	        userQualDetailsVO.setQual_subject_list(qualOptionsList);
	      }
	    }catch (Exception e) {
	      e.printStackTrace();
	    }finally {
	      try {
	        if (userqualRS != null) {
	         userqualRS.close();
	         userqualRS = null;
	        }
	      } catch (Exception e) {
	         throw new SQLException(e.getMessage());
	      }
	    }
	  } catch (Exception e) {
	     e.printStackTrace();
	  }
	  return userQualDetailsVO;
	}
  }
  private class UserConsentDetailsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    	UserClientConsentVO userClientConsentVO = new UserClientConsentVO();
    	userClientConsentVO.setCollegeName(rs.getString("college_name"));
    	userClientConsentVO.setCollegeURL(rs.getString("college_url"));
    	userClientConsentVO.setUserOptDate(rs.getString("user_opt_date"));

    	return userClientConsentVO;   	 
    }
  }
}
