package com.layer.dao.sp.mywhatuni;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : MyWhatuniImageUpdateSP.java
* Description   : This class used to fetch openday search results
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	               Change
* *************************************************************************************************************************************
* Thiyagu G               1.0             19_May_2015           First draft   		
*
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class MyWhatuniImageUpdateSP extends StoredProcedure {
  public MyWhatuniImageUpdateSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql("WU_USER_PROFILE_PKG.SET_USER_IMAGE_PRC");
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_IMAGE_PATH", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_FLAG", OracleTypes.VARCHAR));
    
    compile();
  }
  public Map execute(String userId, String imagePath) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("P_USER_ID", userId);
      inputMap.put("P_IMAGE_PATH", imagePath);      
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }  
}
