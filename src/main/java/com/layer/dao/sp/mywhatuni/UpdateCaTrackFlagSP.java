package com.layer.dao.sp.mywhatuni;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.util.valueobject.mywhatuni.MyWhatuniInputVO;

/**
  * @UpdateCaTrackFlagSP
  * @author Thiyagu G.
  * @version 1.0
  * @since 04.10.2016
  * @purpose This class is used to update the ca track flag.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class UpdateCaTrackFlagSP extends StoredProcedure {
  public UpdateCaTrackFlagSP(DataSource datasource) {
    setDataSource(datasource);    
    setSql(GlobalConstants.UPDATE_CA_TRACK_FLAG_PRC);    
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_CA_TRACK_FLAG", Types.VARCHAR));    
    declareParameter(new SqlOutParameter("P_RET_CODE", OracleTypes.NUMERIC));
    declareParameter(new SqlOutParameter("P_ERR_MSG", OracleTypes.VARCHAR));
  }
  public Map execute(MyWhatuniInputVO myWhatuniInputVO) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", myWhatuniInputVO.getUserId());
      inMap.put("P_CA_TRACK_FLAG", myWhatuniInputVO.getCaTrackFlag());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
