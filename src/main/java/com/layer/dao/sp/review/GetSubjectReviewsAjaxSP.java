package com.layer.dao.sp.review;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.review.RichProfileReviewRowMapperImpl;
import com.wuni.util.valueobject.AutoCompleteVO;

public class GetSubjectReviewsAjaxSP extends StoredProcedure {
    public GetSubjectReviewsAjaxSP(DataSource datasource) {
      setDataSource(datasource);
      setFunction(true);
      setSql(SpringConstants.GET_SUBJECT_REVIEWS_AJAX);
        declareParameter(new SqlOutParameter("SUBJECT_REVIEW_LIST", OracleTypes.CURSOR,new RichProfileReviewRowMapperImpl()));
        declareParameter(new SqlParameter("p_college_id", OracleTypes.VARCHAR));
        declareParameter(new SqlParameter("p_subject_id", OracleTypes.VARCHAR)); 
        compile();
    }
      public Map execute(AutoCompleteVO autoCompleteVO) {
      Map outMap = null;
      Map inMap = new HashMap();
      try { 
        inMap.put("p_college_id", autoCompleteVO.getCollegeId());
        inMap.put("p_subject_id", autoCompleteVO.getSubjectId());
        outMap = execute(inMap);
      } catch (Exception e) {
        e.printStackTrace();
      }    
      return outMap;
    }
}
