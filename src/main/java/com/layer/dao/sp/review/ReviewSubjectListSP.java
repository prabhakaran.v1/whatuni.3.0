package com.layer.dao.sp.review;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.AutoCompleteVO;

/**
  * @ReviewSubjectListSP
  * @author Sangeeth.S
  * @version 1.0
  * @since 11.12.2018
  * @purpose This program is used to get review subject list for the review page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
 	* 11.12.2018     Sangeeth.S               wu_584     initial draft                                              18_DEC_2018
  * 
  */

public class ReviewSubjectListSP extends StoredProcedure{

  public ReviewSubjectListSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(SpringConstants.GET_REVIEW_SUBJECT_FN);
    declareParameter(new SqlOutParameter("PC_REVIEW_SUBJECT", OracleTypes.CURSOR, new ReviewSubjectListRowMapperImpl()));
    declareParameter(new SqlParameter("P_KEYWORD_TEXT", Types.VARCHAR));
    compile();
  }
  
  public Map execute(AutoCompleteVO autoCompleteVO){
    Map outMap = null;
    Map inMap = new HashMap();
    try{
      inMap.put("P_KEYWORD_TEXT", autoCompleteVO.getKeywordText());
      outMap = execute(inMap);
    }catch(Exception e){
      e.printStackTrace();
    }
    return outMap;
  }
  
  private class ReviewSubjectListRowMapperImpl implements RowMapper{
   public Object mapRow(ResultSet resultset, int rowNum) throws SQLException{
     AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
     autoCompleteVO.setDescription(resultset.getString("DESCRIPTION"));     
     autoCompleteVO.setCategoryCode(resultset.getString("CATEGORY_CODE"));      
     autoCompleteVO.setReviewMessage(resultset.getString("REVIEW_COUNT_MSG"));
     return autoCompleteVO;
   }
  }

}
