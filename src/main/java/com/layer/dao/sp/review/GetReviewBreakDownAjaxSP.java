package com.layer.dao.sp.review;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.review.bean.ReviewListBean;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.review.ReviewBreakDownRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.AutoCompleteVO;
import com.wuni.util.valueobject.review.ReviewBreakDownVO;


/**
  * @GetReviewBreakDownAjaxSP
  * @author Hema.S
  * @version 1.0
  * @since 18.12.2018
  * @purpose  This program is used to get reviews details from selected subject in drop down.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.       Changes desc                                      Rel Ver.
  * *************************************************************************************************************************
  * 18-Dec-2018   Hema.S                     1.0        Initial draft to spring                           584
  */
  
public class GetReviewBreakDownAjaxSP extends StoredProcedure{
    public GetReviewBreakDownAjaxSP(DataSource datasource) {
      setDataSource(datasource);
      setSql(SpringConstants.GET_CD_PAGE_SUBJECT_REVIEWS);
      declareParameter(new SqlParameter("p_college_id", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_subject_id", OracleTypes.VARCHAR));
      declareParameter(new SqlOutParameter("PC_REVIEW_LIST",OracleTypes.CURSOR,new ReviewLstRowMapperImpl()));
      declareParameter(new SqlOutParameter("PC_REVIEW_BREAKDOWN",OracleTypes.CURSOR, new ReviewBreakDownRowMapperImpl())); 
      declareParameter(new SqlOutParameter("PC_REVIEW_SUB_BREAKDOWN",OracleTypes.CURSOR, new ReviewBreakDownRowMapperImpl()));      
      //
      compile();
    }
    public Map execute(AutoCompleteVO autoCompleteVO) {
      Map outMap = null;
      Map inMap = new HashMap();
      try {
        inMap.put("p_college_id", autoCompleteVO.getCollegeId());
        inMap.put("p_subject_id", autoCompleteVO.getSubjectName());
        outMap = execute(inMap);
      } catch (Exception e) {
        e.printStackTrace();
      }
      return outMap;
    }

    public class ReviewLstRowMapperImpl implements RowMapper {
      public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        ReviewListBean reviewBean = new ReviewListBean();
        reviewBean.setReviewId(rs.getString("review_id"));
        reviewBean.setReviewerName(rs.getString("USER_NAME"));
        reviewBean.setCollegeId(rs.getString("college_id"));
        reviewBean.setCourseId(rs.getString("course_id"));
        reviewBean.setOverAllRating(rs.getString("overall_rating"));
        reviewBean.setOverallRatingComment(rs.getString("REVIEW_TEXT"));
        reviewBean.setCollegeName(rs.getString("college_name"));    
        String reviewUrl = new SeoUrls().constructReviewPageSeoUrl(reviewBean.getCollegeName(), reviewBean.getCollegeId());
        reviewUrl = reviewUrl+ "?reviewId="+reviewBean.getReviewId();
        reviewBean.setReviewDetailUrl(reviewUrl);
        reviewBean.setCourseName(rs.getString("COURSE_TITLE"));
        reviewBean.setReviewDate(rs.getString("review_date"));  
       reviewBean.setCourseTitle(rs.getString("COURSE_TITLE"));
        reviewBean.setUserNameInitial(rs.getString("user_name_int"));  
        reviewBean.setCollegeName(rs.getString("college_name"));
        if((!GenericValidator.isBlankOrNull(reviewBean.getCourseId()) || !"0".equals(reviewBean.getCourseId()))){
            reviewBean.setCourseDetailsReviewURL(new SeoUrls().courseDetailsPageURL(reviewBean.getCourseTitle(), reviewBean.getCourseId(), reviewBean.getCollegeId()));
          }
   reviewBean.setCourseDeletedFlag(rs.getString("course_deleted_flag"));  
        return reviewBean;
      }
    }
}
