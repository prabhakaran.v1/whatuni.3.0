package com.layer.dao.sp;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.search.form.CourseDetailsBean;
import WUI.utilities.GlobalConstants;

/**
  * @CheckDegreeCourseSP
  * @author Prabhakaran V.
  * @version 1.0
  * @since 28.01.2016
  * @purpose This program is used check the couses is degree or not and return Y/N flag
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class CheckDegreeCourseSP extends StoredProcedure {
  public CheckDegreeCourseSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.CHECK_DEGREE_COURSE_FN);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", Types.NUMERIC));
  }
  public Map execute(CourseDetailsBean courseDetailBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_college_id", courseDetailBean.getCollegeId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
