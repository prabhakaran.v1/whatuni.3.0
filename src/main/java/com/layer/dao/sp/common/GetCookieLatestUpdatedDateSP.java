package com.layer.dao.sp.common;

import com.layer.util.SpringConstants;
import com.wuni.ajax.RecentSearchAjaxSP;
import com.wuni.util.valueobject.AutoCompleteVO;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
/**
  * @see This SP class is to get the latest updated date from the cookie or privacy or terms&condition policy.
  * @author Sangeeth.S
  * @since  04.10.2019- intital draft
  * @version 1.0
  *
  * Modification history:
  * **************************************************************************************************************
  * Author          Relase tag              Modification Details
  * **************************************************************************************************************
  * Sangeeth.S     wu_595_22102019            initial draft
  *
*/
public class GetCookieLatestUpdatedDateSP extends StoredProcedure{

  public GetCookieLatestUpdatedDateSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql(SpringConstants.GET_COOKIE_LATEST_UPDATED_DATE_PRC);    
    declareParameter(new SqlOutParameter("P_LATEST_DATE", OracleTypes.VARCHAR));
    
  }
  public Map execute()
  {
    Map outMap = null;
    try{
      Map inMap = new HashMap();               
      outMap = execute(inMap);      
    }catch(Exception ex){
      ex.printStackTrace();
    }
    return outMap;
  }
}
