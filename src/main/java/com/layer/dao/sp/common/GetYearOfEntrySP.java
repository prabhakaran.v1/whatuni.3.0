package com.layer.dao.sp.common;

import WUI.utilities.GlobalConstants;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;
import spring.valueobject.common.YearOfEntryVO;

public class GetYearOfEntrySP extends StoredProcedure {
  public GetYearOfEntrySP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_YEAR_OF_ENTRY_FN);
    declareParameter(new SqlOutParameter("YEAR_LIST", OracleTypes.CURSOR, new YearOfEntryRowMapperImpl()));
    compile();
  }
  public Map execute() {
    Map outMap = null;
    try {
      Map inMap = new HashMap();

      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class YearOfEntryRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      YearOfEntryVO yearOfEntryVO = new YearOfEntryVO();
      yearOfEntryVO.setWhatYear(rs.getString("in_what_year"));
      yearOfEntryVO.setYearofEntry(rs.getString("year_of_entry"));
      return yearOfEntryVO;
    }
  }
}
