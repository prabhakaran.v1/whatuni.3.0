package com.layer.dao.sp.home;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.homepage.form.HomePageBean;
import WUI.review.bean.ReviewListBean;
import WUI.utilities.CommonUtil;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.HomePageImageDetailsVO;
import com.wuni.util.valueobject.home.HeroImageDeatailsVO;
import com.wuni.util.valueobject.home.HomePageSearchFilterVO;
import com.wuni.util.valueobject.home.PopularUniveristyVO;
import com.wuni.util.valueobject.home.RecentSearchVO;
import com.wuni.util.valueobject.home.SpotLightDetailsVO;
import com.wuni.util.valueobject.home.WhatHaveYouDoneVO;
import com.wuni.valueobjects.topnavsearch.TopNavSearchVO;

/**
 * used call the database procedure to build the homepage
 *
 * @author  Priyaa Parthasarathy
 * @since   wu528
 *
 * Modification history:
 * **************************************************************************************************************
 * Author     Relase Date              Modification Details
 * **************************************************************************************************************
 * Yogeswari  21:07:2015               changes for drawing friends activity pod.
 * Prabhakaran V. 27:01:2016           Change the read more url change
 * Sangeeth.S     21:11:2019           Added columns and cursors for Home page UX changes(Hero image,search Filtes,recent search and popular uni) 
 */
public class WhatuniNewHomeSP extends StoredProcedure {

  public WhatuniNewHomeSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("wuni_homepage_pkg.wu_homepage_prc");
    declareParameter(new SqlParameter("p_user_id", Types.VARCHAR)); //16_SEP_2014
    declareParameter(new SqlParameter("p_basket_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_type", Types.VARCHAR));    
    //
    declareParameter(new SqlOutParameter("oc_wu_counts", OracleTypes.CURSOR, new HomePageCountRowMapperImpl())); //wu312_20110614_Syed: including editor rambling article in homp page article teaser pod
    declareParameter(new SqlOutParameter("oc_wu_awards", OracleTypes.CURSOR, new HomePageAwardsRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_article_slots", OracleTypes.CURSOR, new HomePageArticleListRowMapper()));
    declareParameter(new SqlOutParameter("oc_spotlight", OracleTypes.CURSOR, new UniOfWeekRowMapperImpl())); //16_SEP_2014
    declareParameter(new SqlOutParameter("oc_review_list", OracleTypes.CURSOR, new ReviewListRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_user_done", OracleTypes.CURSOR, new WhatHaveYouDoneRowMapperImpl())); // 08_AUG_2014 Added by Priyaa for What have you done pod
    declareParameter(new SqlOutParameter("oc_next_done", OracleTypes.CURSOR, new WhatHaveYouDoneRowMapperImpl()));        
    //
    declareParameter(new SqlOutParameter("p_awards_video", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_display_final_choice_pod", OracleTypes.VARCHAR));//Added fby Priyaa for final 5 - 21-Jul-2015    
    //
    declareParameter(new SqlParameter("p_track_session_id", Types.VARCHAR));  
    declareParameter(new SqlOutParameter("pc_popular_recent_details", OracleTypes.CURSOR, new PopularRecentDetailsRowMapperImpl()));  
    declareParameter(new SqlOutParameter("pc_popular_univ_list", OracleTypes.CURSOR, new PopularUniversityListRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_text_image_details", OracleTypes.CURSOR, new HeroImageTextDetailsRowMapperImpl()));    
    declareParameter(new SqlOutParameter("pc_qualification_list", OracleTypes.CURSOR, new QualificationListRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_location_list", OracleTypes.CURSOR, new LocationListRowmapperImpl()));
    declareParameter(new SqlOutParameter("pc_key_topics_list", OracleTypes.CURSOR, new keyTopicsListRowmapperImpl()));
    declareParameter(new SqlOutParameter("pc_recent_search_details", OracleTypes.CURSOR, new RecentSearchListRowMapperImpl()));
    declareParameter(new SqlOutParameter("p_filter_url", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("p_recent_search_date", OracleTypes.VARCHAR));
    //declareParameter(new SqlParameter("p_clearing_user_type", Types.VARCHAR));
    //
  }

  public Map execute(HomePageBean homepagebean) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("p_user_id", homepagebean.getUserId()); //16_SEP_2014
      inMap.put("p_basket_id", homepagebean.getBasketID());
      inMap.put("p_user_type", homepagebean.getUserType());
      inMap.put("p_track_session_id", homepagebean.getTrackSessionId()); 
      //inMap.put("p_clearing_user_type", homepagebean.getClearingUserType());
      //System.out.println("inMap home page-->"+ inMap);
      outMap = execute(inMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }

  /**
 * This Class will return the Home Page Hero Image section Count Details;
 */
  private class HomePageCountRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      HomePageImageDetailsVO homePageImageDetailsVO = new HomePageImageDetailsVO();
      homePageImageDetailsVO.setUserCount(rs.getString("wu_user_count"));
      homePageImageDetailsVO.setCourseCount(rs.getString("wu_course_count"));
      homePageImageDetailsVO.setReviewCount(rs.getString("wu_review_count"));
      homePageImageDetailsVO.setVideoPath(rs.getString("video_path"));
      homePageImageDetailsVO.setFutureOpendayCount(rs.getString("wu_future_openday_count")); //Added opendays count for mar_2017
      return homePageImageDetailsVO;
    }

  }

  /**
   * This Class will return the Home Page award section Details;
   */
  private class HomePageAwardsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      HomePageBean homePageBean = new HomePageBean();
      homePageBean.setCollegeId(rs.getString("college_id"));
      homePageBean.setCollegeName(rs.getString("college_name"));
      homePageBean.setCollegeNameDisplay(rs.getString("college_name_display"));
      homePageBean.setQuestionTitle(rs.getString("question_title"));
      homePageBean.setCollegeLogo(rs.getString("college_logo"));
      homePageBean.setPositionId(rs.getString("position_id"));
      homePageBean.setDisplaySeq(rs.getString("display_seq"));
      homePageBean.setSponsorURL(new SeoUrls().construnctUniHomeURL(homePageBean.getCollegeId(), homePageBean.getCollegeName()));
      return homePageBean;
    }

  }

  /**
   * This Class will return the Home Page Article section Details;
   */
  private class HomePageArticleListRowMapper implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      HomePageImageDetailsVO homePageImageDetailsVO = new HomePageImageDetailsVO();
      homePageImageDetailsVO.setSlots(rs.getString("slots"));
      homePageImageDetailsVO.setDisplayOrder(rs.getString("display_order"));
      //homePageImageDetailsVO.setImageId(rs.getString("media_id"));
      homePageImageDetailsVO.setMediaPath(rs.getString("media_path"));
      homePageImageDetailsVO.setMediaType(rs.getString("media_type")); 
      homePageImageDetailsVO.setLongText(rs.getString("article_title"));
      homePageImageDetailsVO.setSmallText(rs.getString("article_short_desc"));
      homePageImageDetailsVO.setUrl(rs.getString("url"));
      homePageImageDetailsVO.setArticleType(rs.getString("article_type"));
      String splitMediaThumbPath = "";
      if (!GenericValidator.isBlankOrNull(homePageImageDetailsVO.getMediaType()) && !GenericValidator.isBlankOrNull(homePageImageDetailsVO.getMediaPath())) {
        if (("ARTICLE_MEDIA").equals(homePageImageDetailsVO.getMediaType())) {
          if (("FEATURED").equals(homePageImageDetailsVO.getArticleType()) || ("TIMELINE").equals(homePageImageDetailsVO.getArticleType())) {
            splitMediaThumbPath = homePageImageDetailsVO.getMediaPath().substring(0, homePageImageDetailsVO.getMediaPath().lastIndexOf("."));
            homePageImageDetailsVO.setImageName(new CommonUtil().getImgPath((homePageImageDetailsVO.getMediaPath().replace(".", "_211px.")), rowNum));
          }else{
            homePageImageDetailsVO.setImageName(new CommonUtil().getImgPath((homePageImageDetailsVO.getMediaPath()), rowNum));  
          }          
        }
      }
      return homePageImageDetailsVO;
    }

  }

  /**
    * This Class will return the Home Page spotlight Details;//16_SEP_2014
    */
  private class UniOfWeekRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SpotLightDetailsVO spotLightVO = new SpotLightDetailsVO();
      spotLightVO.setOrderItemId(rs.getString("order_item_id"));
      spotLightVO.setCollegeId(rs.getString("college_id"));
      spotLightVO.setCollegeName(rs.getString("college_name"));
      spotLightVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      spotLightVO.setTitle(rs.getString("title"));
      spotLightVO.setTitleUrl(rs.getString("title_url"));
      spotLightVO.setSubTitle(rs.getString("sub_title"));
      spotLightVO.setSubTitleUrl(rs.getString("sub_title_url"));
      spotLightVO.setMediaId(rs.getString("media_id"));
      spotLightVO.setMediaType(rs.getString("media_type_desc"));
      spotLightVO.setImagePath(rs.getString("thumbnail_path"));
      spotLightVO.setVideoUrl(rs.getString("video_url"));
      spotLightVO.setDescription(rs.getString("description"));
      spotLightVO.setCallToActionText(rs.getString("call_to_action_text"));
      spotLightVO.setCallToActionUrl(rs.getString("call_to_action_url"));
      spotLightVO.setPosition(rs.getString("position"));
      return spotLightVO;
    }

  }

  /**
    * This Class will return the Home Page Review of the Week Details;
    */
  private class ReviewListRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewListBean reviewListBean = new ReviewListBean();
      reviewListBean.setReviewId(rs.getString("review_id"));
      reviewListBean.setReviewTitle(rs.getString("review_title"));
      reviewListBean.setCollegeId(rs.getString("college_id"));
      reviewListBean.setCollegeName(rs.getString("college_name"));
      reviewListBean.setCourseId(rs.getString("course_id"));
      reviewListBean.setCourseTitle(rs.getString("course_title"));
      reviewListBean.setOverAllRating(rs.getString("overall_rating"));
      reviewListBean.setOverallRatingComment(rs.getString("overall_rating_comments"));
      //Change the read more reviews url by Prabha on  27-JAN-2016
		    String reviewUrl = new SeoUrls().constructReviewPageSeoUrl(reviewListBean.getCollegeName(), reviewListBean.getCollegeId());
      if(!GenericValidator.isBlankOrNull(reviewListBean.getReviewId())){
			     reviewUrl = reviewUrl+ "?reviewId="+reviewListBean.getReviewId();
      }
			   reviewListBean.setReviewDetailUrl(reviewUrl);
			   //End of review url
      return reviewListBean;
    }

  }

  /**
   * This Class will return the Home Page What have you Done pod Details;
   *  08_AUG_2014 Added by Priyaa for What have you done pod
   */
  private class WhatHaveYouDoneRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      WhatHaveYouDoneVO whatHaveYouDoneVO = new WhatHaveYouDoneVO();
      whatHaveYouDoneVO.setActionName(rs.getString("action_name"));
      whatHaveYouDoneVO.setActionUrl(rs.getString("url"));
      whatHaveYouDoneVO.setMainText(rs.getString("main_text"));
      whatHaveYouDoneVO.setShortText(rs.getString("short_text"));
      whatHaveYouDoneVO.setTimeLineId(rs.getString("timeline_item_id"));
      return whatHaveYouDoneVO;
    }

  }
  //Home page UX changes
  
  private class RecentSearchListRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      RecentSearchVO recentSearchVO = new RecentSearchVO();
      recentSearchVO.setFilterName(rs.getString("filter_list"));
      recentSearchVO.setDescription(rs.getString("description"));      
      return recentSearchVO;
    }
  }  
  private class PopularRecentDetailsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      RecentSearchVO recentSearchVO = new RecentSearchVO();
      recentSearchVO.setSubjectName(rs.getString("subject_name"));
      recentSearchVO.setActionDate(rs.getString("action_date"));      
      return recentSearchVO;
    }
  }  
  private class PopularUniversityListRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      PopularUniveristyVO popularUniveristyVO = new PopularUniveristyVO();
      SeoUrls seoUrl = new SeoUrls();      
      popularUniveristyVO.setCollegeId(rs.getString("college_id"));
      popularUniveristyVO.setCollegeName(rs.getString("college_name"));
      popularUniveristyVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      popularUniveristyVO.setLogoPath(rs.getString("logo_path"));
      popularUniveristyVO.setExactRating(rs.getString("exact_rating"));
      popularUniveristyVO.setProviderResultsUrl(rs.getString("view_courses_url"));
      //popularUniveristyVO.setUniHomeUrl(seoUrl.construnctUniHomeURL(popularUniveristyVO.getCollegeId(), popularUniveristyVO.getCollegeName()));
      popularUniveristyVO.setUniHomeUrl(rs.getString("college_url"));
      return popularUniveristyVO;
    }    
  }
  private class HeroImageTextDetailsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      HeroImageDeatailsVO heroImageDeatailsVO = new HeroImageDeatailsVO();
      heroImageDeatailsVO.setH1Text(rs.getString("h1_text"));
      heroImageDeatailsVO.setH2Text(rs.getString("h2_text"));
      heroImageDeatailsVO.setHeroImagePath(rs.getString("hero_image_path"));      
      return heroImageDeatailsVO;
    }
  }  
  private class QualificationListRowMapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      HomePageSearchFilterVO homePageSearchFilterVO = new HomePageSearchFilterVO();      
      homePageSearchFilterVO.setQualDesc(rs.getString("qual_desc"));      
      homePageSearchFilterVO.setQualCode(rs.getString("qual_code"));
      homePageSearchFilterVO.setUrlText(rs.getString("url_text"));      
      return homePageSearchFilterVO;
    }
  }
  private class LocationListRowmapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      HomePageSearchFilterVO homePageSearchFilterVO = new HomePageSearchFilterVO();      
      homePageSearchFilterVO.setRegionName(rs.getString("region_name"));      
      homePageSearchFilterVO.setRegionUrlText(rs.getString("url_text"));
      homePageSearchFilterVO.setRegionId(rs.getString("region_id"));      
      return homePageSearchFilterVO;
    }
  }
  private class keyTopicsListRowmapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      HomePageSearchFilterVO homePageSearchFilterVO = new HomePageSearchFilterVO();      
      homePageSearchFilterVO.setKeytopics(rs.getString("key_topics"));      
      homePageSearchFilterVO.setKeyTopicUrl(rs.getString("url"));      
      return homePageSearchFilterVO;
    }
  }
  //
}
