package com.layer.dao.sp.home;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.rowmapper.CommonMapper;
import spring.rowmapper.articles.PremiumArticleListRowMapper;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

import com.layer.util.rowmapper.home.CategorySubjectPodImpl;
import com.layer.util.rowmapper.home.FeaturedVideosRowMapperImpl;
import com.layer.util.rowmapper.home.TickerTapeVideosImpl;
import com.layer.util.rowmapper.openday.HomepageOpendayRowMapperImpl;
import com.wuni.util.valueobject.HomePageImageDetailsVO;

/**
 * used call the database procedure to build the homepage
 *
 * @author  Mohamed Syed
 * @since   wu315_20110726
 *
 */
public class WhatuiHomeSP extends StoredProcedure {

  public WhatuiHomeSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WUNI_SPRING_PKG.GET_HOMEPAGE_DATA_PROC");
    declareParameter(new SqlParameter("p_affiliate_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_best_video", OracleTypes.CURSOR, new FeaturedVideosRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_latest_reviews", OracleTypes.CURSOR, new CommonMapper.latestReviewMapperImpl()));
    declareParameter(new SqlOutParameter("o_total_reviews_count", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_get_video_ticker", OracleTypes.CURSOR, new TickerTapeVideosImpl()));
    declareParameter(new SqlOutParameter("o_home_rand_subject", OracleTypes.CURSOR, new CategorySubjectPodImpl()));
    declareParameter(new SqlOutParameter("O_HOME_PAGE_ARTICLE_TEASER", OracleTypes.CURSOR, new PremiumArticleListRowMapper())); //wu312_20110614_Syed: including editor rambling article in homp page article teaser pod
    declareParameter(new SqlOutParameter("o_open_day_pod", OracleTypes.CURSOR, new HomepageOpendayRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_IMAGE_DETAILS", OracleTypes.CURSOR, new HomePageImageDetails()));
     
  }

  public Map execute() {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_affiliate_id", GlobalConstants.WHATUNI_AFFILATE_ID);
    outMap = execute(inMap);
    return outMap;
  }

/**
 * This Class will return the Home Page Image Gallery Details;
 */
  private class HomePageImageDetails implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
      HomePageImageDetailsVO  homePageImageDetailsVO = new HomePageImageDetailsVO();
      homePageImageDetailsVO.setImageId(rs.getString("image_id"));
      homePageImageDetailsVO.setImageName(rs.getString("image_name"));
      homePageImageDetailsVO.setLongText(rs.getString("long_text"));
      homePageImageDetailsVO.setSmallText(rs.getString("small_text"));
      homePageImageDetailsVO.setUrl(new CommonFunction().appendingHttptoURL(rs.getString("url")));
      return homePageImageDetailsVO;
    }
  }  
}
