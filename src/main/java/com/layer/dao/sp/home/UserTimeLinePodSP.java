package com.layer.dao.sp.home;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.CommonUtil;

import com.wuni.util.valueobject.UserTimeLineVO;

/**
 * used call the database procedure to build User Timeline pod in homepage
 * @author Amirtharaj
 * @since  16_SEP_2014
 *
 */
public class UserTimeLinePodSP extends StoredProcedure {
  public UserTimeLinePodSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WU_TIMELINE_PKG.GET_TIMELINE_WITH_PROGRESS_PRC");
    declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
    declareParameter(new SqlOutParameter("PC_TIMELINE_ITEMS", OracleTypes.CURSOR, new UserTimeLineRowMapperImpl()));
  }
  public Map execute(String userId) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("p_user_id", userId); //16_SEP_2014      
      outMap = execute(inMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
  private class UserTimeLineRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      UserTimeLineVO userTimeLineVO = new UserTimeLineVO();
      userTimeLineVO.setTimeLineId(resultset.getString("TIMELINE_ITEM_ID"));
      userTimeLineVO.setParentTimeLineId(resultset.getString("PARENT_TIMELINE_ITEM_ID"));
      userTimeLineVO.setTlTitle(resultset.getString("TITLE"));
      userTimeLineVO.setTlDescription(resultset.getString("DESCRIPTION"));
      userTimeLineVO.setTlURL(resultset.getString("URL"));
      userTimeLineVO.setTlImageURL(new CommonUtil().getImgPath(resultset.getString("IMAGE_URL"), rowNum));
      userTimeLineVO.setTlMandFlag(resultset.getString("MANDATORY_FLAG"));
      userTimeLineVO.setTlItemType(resultset.getString("TIMELINE_ITEM_TYPE"));
      userTimeLineVO.setTlItemActionType(resultset.getString("TIMELINE_ITEM_ACTION_TYPE"));
      userTimeLineVO.setTlPostId(resultset.getString("POST_ID"));
      userTimeLineVO.setTlItemStatus(resultset.getString("ITEM_ACTION_STATUS"));
      userTimeLineVO.setTlDeadLineFlag(resultset.getString("DEADLINE_ITEM_FLAG"));
      userTimeLineVO.setTlYouAreHereId(resultset.getString("YOUAREHERE_ITEM_ID"));
      userTimeLineVO.setTlYouShouldBeHereId(resultset.getString("YOUSHOULDBEHERE_ITEM_ID"));
      userTimeLineVO.setTlDispSeq(resultset.getString("DISPLAY_SEQ"));
      return userTimeLineVO;
    }
  }
}
