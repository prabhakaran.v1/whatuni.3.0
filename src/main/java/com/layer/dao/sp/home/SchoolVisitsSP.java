package com.layer.dao.sp.home;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.valueobjects.AboutUsPagesVO;

/**
 * used call the database procedure to build the homepage
 *
 * @author  Thiyagu G
 * @since   wu528
 *
 */
 public class SchoolVisitsSP extends StoredProcedure {

   public SchoolVisitsSP(DataSource datasource) {
     setDataSource(datasource);
     setFunction(true);
     setSql("hot_wuni.wu_email_pkg.ABOUTUS_MAIL_FN");
     //
     declareParameter(new SqlOutParameter("RetVal", OracleTypes.NUMBER));
     declareParameter(new SqlParameter("P_FIRSTNAME", Types.VARCHAR));
     declareParameter(new SqlParameter("P_SURNAME", Types.VARCHAR));   
     declareParameter(new SqlParameter("P_JOBTITLE", Types.VARCHAR));
     declareParameter(new SqlParameter("P_SCHOOLNAME", Types.VARCHAR));
     declareParameter(new SqlParameter("P_TOWNNAME", Types.VARCHAR));
     declareParameter(new SqlParameter("P_ABOUTUS_TYPE", Types.VARCHAR));
    
    declareParameter(new SqlParameter("P_ADDRESSLINE1", Types.VARCHAR));
    declareParameter(new SqlParameter("P_ADDRESSLINE2", Types.VARCHAR));   
    declareParameter(new SqlParameter("P_EMAIL", Types.VARCHAR));
    declareParameter(new SqlParameter("P_PHONENO", Types.VARCHAR));
    declareParameter(new SqlParameter("P_POSTCODE", Types.VARCHAR));
    declareParameter(new SqlParameter("P_DATE", Types.VARCHAR));
    
    declareParameter(new SqlParameter("P_TIME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_KNOWABOUTUS", Types.VARCHAR));
    declareParameter(new SqlParameter("P_MESSAGE", Types.VARCHAR));
   }

   public Map execute(AboutUsPagesVO aboutUsPagesVO, String flag) {
     Map outMap = null;
     Map inMap = new HashMap();
     
       if("schoolvisits".equals(flag)){
           inMap.put("P_FIRSTNAME", aboutUsPagesVO.getFirstName());
           inMap.put("P_SURNAME", aboutUsPagesVO.getLastName());
           inMap.put("P_JOBTITLE", aboutUsPagesVO.getJobTitle());
           inMap.put("P_SCHOOLNAME", aboutUsPagesVO.getSchoolName());
           inMap.put("P_TOWNNAME", aboutUsPagesVO.getTown());
           inMap.put("P_ABOUTUS_TYPE", "SCHOOL_VISIT");    
           inMap.put("P_ADDRESSLINE1", null);
           inMap.put("P_ADDRESSLINE2", null);
           inMap.put("P_EMAIL", aboutUsPagesVO.getEmailAddress());
           inMap.put("P_PHONENO", aboutUsPagesVO.getPhone());
           inMap.put("P_POSTCODE", null);
           inMap.put("P_DATE", aboutUsPagesVO.getDate());
           inMap.put("P_TIME", aboutUsPagesVO.getTime());
           inMap.put("P_KNOWABOUTUS", aboutUsPagesVO.getAboutus());
           inMap.put("P_MESSAGE", aboutUsPagesVO.getMessage());
       }else if("orderdownload".equals(flag)){
           inMap.put("P_FIRSTNAME", aboutUsPagesVO.getFirstName());
           inMap.put("P_SURNAME", aboutUsPagesVO.getLastName());
           inMap.put("P_JOBTITLE", aboutUsPagesVO.getJobTitle());
           inMap.put("P_SCHOOLNAME", aboutUsPagesVO.getSchoolName());
           inMap.put("P_TOWNNAME", aboutUsPagesVO.getTown());
           inMap.put("P_ABOUTUS_TYPE", "ORDER_GIFT");    
           inMap.put("P_ADDRESSLINE1", aboutUsPagesVO.getAddressLine1());
           inMap.put("P_ADDRESSLINE2", aboutUsPagesVO.getAddressLine2());
           inMap.put("P_EMAIL", null);
           inMap.put("P_PHONENO", null);
           inMap.put("P_POSTCODE", aboutUsPagesVO.getPostCode());
           inMap.put("P_DATE", null);
           inMap.put("P_TIME", null);
           inMap.put("P_KNOWABOUTUS", null);
           inMap.put("P_MESSAGE", null);
       }    
     outMap = execute(inMap);
     return outMap;
   }

 }
