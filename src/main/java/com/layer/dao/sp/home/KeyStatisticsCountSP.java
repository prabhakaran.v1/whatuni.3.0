package com.layer.dao.sp.home;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.valueobject.HomePageImageDetailsVO;

/**
 * used call the database procedure to build the homepage
 *
 * @author  Thiyagu G
 * @since   wu528
 *
 */
 public class KeyStatisticsCountSP extends StoredProcedure {

   public KeyStatisticsCountSP(DataSource datasource) {
     setDataSource(datasource);
     setFunction(true);
     setSql("WUNI_HOMEPAGE_PKG.GET_HOMEPAGE_COUNTS_FN");
     //     
     declareParameter(new SqlOutParameter("key_statistics_count_details", OracleTypes.CURSOR, new KeyStatisticsCountListRowMapperImpl()));
   }

   public Map execute() {
     Map outMap = null;
     Map inMap = new HashMap();
     outMap = execute(inMap);
     return outMap;
   }
    
     public class KeyStatisticsCountListRowMapperImpl implements RowMapper {
       public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
           HomePageImageDetailsVO  homePageImageDetailsVO = new HomePageImageDetailsVO();
           homePageImageDetailsVO.setUserCount(resultSet.getString("wu_user_count"));
           homePageImageDetailsVO.setCourseCount(resultSet.getString("wu_course_count"));
           homePageImageDetailsVO.setReviewCount(resultSet.getString("wu_review_count"));
           homePageImageDetailsVO.setVideoPath(resultSet.getString("video_path"));
           homePageImageDetailsVO.setFutureOpendayCount(resultSet.getString("wu_future_openday_count")); //Added opendays count for mar_2017
           return homePageImageDetailsVO;
       }
     }

 }
