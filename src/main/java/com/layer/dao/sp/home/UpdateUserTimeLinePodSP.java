package com.layer.dao.sp.home;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * used call the database procedure to update User Timeline pod in homepage
 * @author Amirtharaj
 * @since  16_SEP_2014
 *
 */
public class UpdateUserTimeLinePodSP extends StoredProcedure {
  public UpdateUserTimeLinePodSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("WU_TIMELINE_PKG.SAVE_USER_TIMELINEITEM_VAL_PRC");
    declareParameter(new SqlParameter("p_user_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_timeline_item_id", Types.VARCHAR));
  }
  public Map execute(String userId, String timeLineId) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("p_user_id", userId);
      inMap.put("p_timeline_item_id", timeLineId);
      outMap = execute(inMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
}
