package com.layer.dao.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.valueobjects.InsightsVO;

/**
  * This SP is used to get the Insights info..
  * @author Prabhakaran V.
  * @version 1.0
  * @since 29_Mar_2016 Release.
  * Change Log
  * *******************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
  * *******************************************************************************************************************************
  * 08_Aug_2017    Prabhakaran V.             568      Added schoolUrn, source type                                          wu_568
  *
  */
public class GetInsightDataSP extends StoredProcedure {
  public GetInsightDataSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_INSIGHT_PAGE_NAME_FN);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.CURSOR, new InsightInfoRowMapperImpl()));
    declareParameter(new SqlParameter("p_ga_page_name", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("p_course_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_qualification", Types.VARCHAR));
    declareParameter(new SqlParameter("p_category_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_location", Types.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", Types.VARCHAR));
  }
  public Map execute(InsightsVO insightsVO) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_ga_page_name", insightsVO.getPageName());
      inMap.put("p_user_id", insightsVO.getUserId());
      inMap.put("p_postcode", insightsVO.getPostCode());
      inMap.put("p_course_id", insightsVO.getCourseId());
      inMap.put("p_qualification", insightsVO.getQualification());
      inMap.put("p_category_code", insightsVO.getCategoryCode());
      inMap.put("p_location", insightsVO.getLocation());
      inMap.put("p_college_id", insightsVO.getCollegeId());      
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class InsightInfoRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      InsightsVO insightsVO = new InsightsVO();
      insightsVO.setPageName(rs.getString("insight_page_name"));
      insightsVO.setYearOfEntry(rs.getString("year_of_entry"));
      insightsVO.setGradeValue(rs.getString("grade_value"));//This will return tariff points
      insightsVO.setSchoolName(rs.getString("school_name"));
      insightsVO.setRegionName(rs.getString("region_name"));
      insightsVO.setCategoryDesc(rs.getString("category_desc"));
      insightsVO.setCpeSubjectName(rs.getString("cpe_subject_name"));
      insightsVO.setJacs(rs.getString("jacs"));
      insightsVO.setHecos(rs.getString("hecos"));
      insightsVO.setAdvertiser(rs.getString("advertiser"));
      insightsVO.setGranularQual(rs.getString("granular_qual"));
      insightsVO.setCountry(rs.getString("country"));
      insightsVO.setQuintile(rs.getString("quintile"));
      insightsVO.setCourseTitle(rs.getString("course_title"));
      insightsVO.setStudyLevel(rs.getString("study_level"));
      insightsVO.setStudyMode(rs.getString("study_mode"));
      insightsVO.setCollegeId(rs.getString("college_id"));
      insightsVO.setInstitutionId(rs.getString("institution_id"));
      insightsVO.setSchoolUrn(rs.getString("school_urn"));
      insightsVO.setSourceType(rs.getString("source_type"));
      insightsVO.setCpeSubjectNames(rs.getString("quoted_cpe_subject_name"));
      insightsVO.setCpeSubjectCount(rs.getString("cpe_subject_count"));
      return insightsVO;
    }
  }
}
