package com.layer.dao.sp.registration;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : CheckCurrentPasswordSP.java
* Description   : Check Current Password
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	               Change
* *************************************************************************************************************************************
* Anbarasan.R             1.0             09.11.2012           First draft   		
*
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class CheckCurrentPasswordSP extends StoredProcedure {
  public CheckCurrentPasswordSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql("hot_wuni.wu_user_profile_pkg.chk_user_password_fn");
    declareParameter(new SqlOutParameter("l_password_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_password", OracleTypes.VARCHAR));    
    compile();
  }
  //
  public Map execute(String userId, String password) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_user_id", userId);
      inputMap.put("p_password", password);     
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
}
