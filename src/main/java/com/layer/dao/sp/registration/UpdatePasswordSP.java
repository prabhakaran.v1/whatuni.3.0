package com.layer.dao.sp.registration;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : UpdatePasswordSP.java
* Description   : Update user password
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	               Change
* *************************************************************************************************************************************
* Priyaa Parthasarathy    1.0             03.1.2014           First draft   		
*
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class UpdatePasswordSP extends StoredProcedure{
  public UpdatePasswordSP(DataSource datasource) {
    setDataSource(datasource);  
    setSql("HOT_WUNI.WU_USER_PROFILE_PKG.CHANGE_PASSWORD_PRC");    
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_old_password", OracleTypes.VARCHAR));  
    declareParameter(new SqlParameter("p_new_password", OracleTypes.VARCHAR)); 
    declareParameter(new SqlOutParameter("p_return_code", OracleTypes.VARCHAR));
    compile();
  }
  //  
  public Map execute(String userId, String password, String oldPassword){
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_user_id", userId);
      inputMap.put("p_old_password", oldPassword);
      inputMap.put("p_new_password", password);
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
}