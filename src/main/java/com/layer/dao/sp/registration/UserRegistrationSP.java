package com.layer.dao.sp.registration;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import WUI.registration.bean.RegistrationBean;
import com.layer.util.SpringConstants;
import com.wuni.util.sql.OracleArray;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : UserRegistrationSP.java
* Description   : New user registration
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	   Change
* *************************************************************************************************************************************
* Anbarasan.R             1.0             09.11.2012           First draft   		
* Thiyagu G               wu_546          03_Nov_2015          Modified             Save the grades and user contact details
* Prabhakaran V.          wu_564          16_May_2017          Modified             Added basket id in out param
* Prabhakaran V.          wu_570          01_Nov_2017          Modified             Added p_app_permit_flag in param
* Sabapathi S.            wu_582          10_Oct_2018          Modified             Added Screen width for stats logging
* Sangeeth.S                              16_Apr_2019          Modified             Added p_subj_grades_session_id in param
* Sangeeth.S			  wu_31032020	  09_MAR_2020		   Modified				Added lat and long input param
* Sri Sankari		      wu_20200917     17_SEP_2020            1.8                Added stats log for tariff logging(UCAS point)
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class UserRegistrationSP extends StoredProcedure{
  DataSource ds = null;
  public UserRegistrationSP(DataSource datasource) {
    setDataSource(datasource);
    this.ds = datasource;
    setSql(SpringConstants.USER_REGISTERATION_LBX_PRC);
    declareParameter(new SqlParameter("p_firstname", OracleTypes.VARCHAR));    
    declareParameter(new SqlParameter("p_surname", OracleTypes.VARCHAR));    
    declareParameter(new SqlParameter("p_email_address", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_password", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_submit", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_permit_flag", OracleTypes.VARCHAR)); 
    declareParameter(new SqlParameter("p_user_ip", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_agent", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_request_url", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_referrer_url", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlInOutParameter("p_session_id", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_spam_msg", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_track_session", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_year_of_entry", OracleTypes.VARCHAR));//Added by Priyaa for 8_OCT_2014_REL for year of entry addition
    declareParameter(new SqlParameter("p_fb_user_id", OracleTypes.VARCHAR));//Added FB user id to store and generate FB image path for 19_May_2015, by Thiyagu G.    
     //Added user contact details for 03_Nov_2015, by Thiyagu G.
    declareParameter(new SqlParameter("p_date_of_birth", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_attr_val_arr", OracleTypes.ARRAY, "HOT_ADMIN.TB_QL_MAS_ATTR_VALUE"));
    declareParameter(new SqlParameter("p_address_line_one", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_address_line_two", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_country_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_city", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_postcode", OracleTypes.VARCHAR));    
	declareParameter(new SqlParameter("p_open_days_submit", OracleTypes.VARCHAR)); //Added for openday registration by Prabha on 27_JAN_2016_REL
	declareParameter(new SqlOutParameter("p_basket_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_app_permit_flag", OracleTypes.VARCHAR)); //Added by Prabha for app permit flag on 1_Nov_2017
    declareParameter(new SqlParameter("p_marketing_email_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_solus_email_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_survey_email_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_screen_width", OracleTypes.VARCHAR)); // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
    declareParameter(new SqlParameter("p_subj_grades_session_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_LATITUDE", OracleTypes.VARCHAR));// March2020 rel - Sangeeth.S: Added latitude and longitde for stats logging
    declareParameter(new SqlParameter("P_LONGITUDE", OracleTypes.VARCHAR));// March2020 rel - Sangeeth.S: Added latitude and longitde for stats logging
    declareParameter(new SqlParameter("P_UCAS_TARIFF", OracleTypes.NUMBER));// wu_20200917 - Sri Sankari: Added UCAS tariff point
    declareParameter(new SqlOutParameter("P_USER_UCAS_POINTS", OracleTypes.VARCHAR));// wu_20200917 - Sri Sankari: Added for user UCAS tariff point
    //
    compile();
  }
  //  
  public Map execute(RegistrationBean registrationBean, Object[][] attrValues) {
    Map outMap = null;
    Connection connection = null;
    try {
      connection = ds.getConnection();
      ARRAY values = OracleArray.getOracleArray(connection, "HOT_ADMIN.TB_QL_MAS_ATTR_VALUE", attrValues);
      Map inputMap = new HashMap();
      inputMap.put("p_firstname", registrationBean.getFirstName());      
      inputMap.put("p_surname", registrationBean.getSurName());      
      inputMap.put("p_email_address", registrationBean.getEmailAddress());
      inputMap.put("p_password", registrationBean.getPassword());
      inputMap.put("p_submit", registrationBean.getSubmitType());
      inputMap.put("p_permit_flag", registrationBean.getNewsLetterFlag());
      inputMap.put("p_user_ip", registrationBean.getClientIp());
      inputMap.put("p_user_agent", registrationBean.getClientBrowser());
      inputMap.put("p_request_url", registrationBean.getRequestURL());
      inputMap.put("p_referrer_url", registrationBean.getRefferalURL());
      inputMap.put("p_session_id", registrationBean.getSessionId());
      inputMap.put("p_track_session", registrationBean.getTrackSessionId());
      inputMap.put("p_year_of_entry", registrationBean.getYearOfUniEntry());//Added by Priyaa for 8_OCT_2014_REL for year of entry addition      
      inputMap.put("p_fb_user_id", registrationBean.getFbUserId());//Added FB user id to store and generate FB image path for 19_May_2015, by Thiyagu G.       
      //Added user contact details for 03_Nov_2015, by Thiyagu G.
      inputMap.put("p_date_of_birth", registrationBean.getDateOfBirth());
      inputMap.put("p_attr_val_arr", values);      
      inputMap.put("p_address_line_one", registrationBean.getAddressLine1());      
      inputMap.put("p_address_line_two", registrationBean.getAddressLine2());      
      inputMap.put("p_country_id", registrationBean.getCountryOfResidence());      
      inputMap.put("p_city", registrationBean.getTown());      
      inputMap.put("p_postcode", registrationBean.getPostCode());
	  inputMap.put("p_open_days_submit", registrationBean.getUserCMMType());
      inputMap.put("p_app_permit_flag", null);
      inputMap.put("p_marketing_email_flag", registrationBean.getMarketingEmailFlag());
      inputMap.put("p_solus_email_flag", registrationBean.getSolusEmailFlag());
      inputMap.put("p_survey_email_flag", registrationBean.getSurveyEmailFlag());      
      inputMap.put("p_screen_width", registrationBean.getScreenWidth()); // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
      inputMap.put("p_subj_grades_session_id", registrationBean.getSubjectSessionId());//Added for the whatunigo by sangeeth.S    
      inputMap.put("P_LATITUDE", registrationBean.getLatitude());
      inputMap.put("P_LONGITUDE", registrationBean.getLongitude());
      inputMap.put("P_UCAS_TARIFF", registrationBean.getUserUcasScore());
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }finally {
      try {
        if (connection != null) {
          connection.close();          
        }          
      } catch (Exception closeException) {
        closeException.printStackTrace();
      }
    }
    return outMap;
  }
}