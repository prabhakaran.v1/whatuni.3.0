package com.layer.dao.sp.registration;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.RegistrationBean;

import com.layer.util.SpringConstants;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : SurveyAutoLoginSP.java
* Description   : Automatically login the user based on userid for Survey
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	               Change
* *************************************************************************************************************************************
* Priyaa Parthasarathy    1.0             09.11.2012           First draft   		
*
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class SurveyAutoLoginSP extends StoredProcedure{
  public SurveyAutoLoginSP(DataSource datasource) {
    setDataSource(datasource);    
    setSql(SpringConstants.SURVEY_USER_LOGIN_PRC);
    declareParameter(new SqlParameter("p_token", OracleTypes.VARCHAR));      
    declareParameter(new SqlInOutParameter("p_session_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_track_session", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_user_id", OracleTypes.VARCHAR));
    compile();
  }
  //  
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();    
      inputMap.put("p_token", registrationBean.getUserId());
      inputMap.put("p_session_id", registrationBean.getSessionId());
      inputMap.put("p_track_session", registrationBean.getTrackSessionId());
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
}