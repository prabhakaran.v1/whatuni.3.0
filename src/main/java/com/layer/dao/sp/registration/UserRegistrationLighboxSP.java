package com.layer.dao.sp.registration;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.mywhatuni.NationalityCountryRowMapper;

/**
   * @UserRegistrationLighboxSP
   * @author Thiyagu G
   * @version 1.0
   * @since 03.11.2015
   * @purpose This program is used call the database procedure to build the registration lightbox data load.
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
   * *************************************************************************************************************************
   * 03_NOV_2015   Thiyagu G                  1.0      To get data for user registration lightbox.                  wu_546
   */
public class UserRegistrationLighboxSP extends StoredProcedure {

  public UserRegistrationLighboxSP(DataSource datasource) {
    //
    setDataSource(datasource);
    setSql(SpringConstants.GET_USER_REGISTRATION_LIGHTBOX_PRC);    
    declareParameter(new SqlOutParameter("pc_country_list", OracleTypes.CURSOR, new NationalityCountryRowMapper()));    
  }

  public Map execute() {
    Map outMap = null;    
    Map inMap = new HashMap();
    try {
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  } 
   
}
