package com.layer.dao.sp.registration;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

/**
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Class         : ResetPasswordSP.java
 * Description   : Reset user password.
 * @version      : 1.0
 * Author        : Karthi
 * Since         : wu_537 24-FEB-2015
 * Change Log    :
 * *************************************************************************************************************************************
 * Author              Release     Modified On      Modification Details                         Change
 * *************************************************************************************************************************************
 * Karthi              wu_537      24-FEB-2015      First draft   
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
public class ResetPasswordSP extends StoredProcedure{
  public ResetPasswordSP(DataSource datasource){
    setDataSource(datasource);  
    setSql(GlobalConstants.RESET_PASSWORD_PRC);    
    declareParameter(new SqlParameter("p_token", OracleTypes.VARCHAR)); 
    declareParameter(new SqlParameter("p_new_password", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_ca_track_flag", OracleTypes.VARCHAR)); //Added by Thiyagu G to add a ca track flag on CATOOL.
    declareParameter(new SqlOutParameter("p_error_status", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_error_message", OracleTypes.VARCHAR));
    compile();
  }
  //
  public Map execute(String userId, String password, String sessionUserId, String caTrackFlag){
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_token", userId);
      inputMap.put("p_new_password", password);
      inputMap.put("p_user_id", sessionUserId);
      inputMap.put("p_ca_track_flag", caTrackFlag); //Added by Thiyagu G to add a ca track flag on CATOOL.
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
}
