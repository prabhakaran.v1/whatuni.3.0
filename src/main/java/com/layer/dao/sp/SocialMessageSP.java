package com.layer.dao.sp;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.UserLoginBean;
import WUI.utilities.GlobalConstants;

/**
 * @SocialMessageSP
 * @author Indumathi.S
 * @version 1.0
 * @since 24.11.2015
 * @purpose  This program is used set the user email details for sending email to specific email id.
 * Change Log
 * *************************************************************************************************************************
 * Date           Name                      Ver.       Changes desc                                      Rel Ver.
 * *************************************************************************************************************************
 * Nov-24-15      Indumathi.S               1.0         Cost of Pint                                      547
 */
public class SocialMessageSP extends StoredProcedure {

  public SocialMessageSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.SEND_US_MSG_PRC);
    declareParameter(new SqlParameter("P_USER_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_EMAIL", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_MESSAGE", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_URL", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_keyword", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_STATUS", OracleTypes.VARCHAR));
  }

  public Map execute(UserLoginBean userLoginBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_USER_NAME", userLoginBean.getUserName() );
      inMap.put("P_EMAIL", userLoginBean.getEmailId());
      inMap.put("P_MESSAGE", userLoginBean.getUserMessage());
      inMap.put("P_URL", userLoginBean.getReferralURL());
      inMap.put("p_keyword", userLoginBean.getSearchKeyword());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }

}
