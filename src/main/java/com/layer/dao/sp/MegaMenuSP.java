package com.layer.dao.sp;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class MegaMenuSP extends StoredProcedure{
  public MegaMenuSP(DataSource datasource){
    setDataSource(datasource);
    setFunction(true);
    setSql("WU_UTIL_PKG.mega_menu_degrees_list");
    declareParameter(new SqlOutParameter("o_megamenu", OracleTypes.CLOB));
  }
  
  public Map execut(){
    Map outMap = null;
    Map inMap = new HashMap();
    outMap = execute(inMap);
    return outMap;
  }
}
