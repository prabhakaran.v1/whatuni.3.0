package com.layer.dao.sp;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.SpringConstants;
import oracle.jdbc.OracleTypes;

/**
 * @NavigationUrlSP
 * @author   Sujitha V
 * @version  1.0
 * @since    2020_DEC_08
 * @purpose  This SP used to return the navigation URL for CTA button.
 * Change Log
 * *************************************************************************************************************************
 * author	              Ver 	              Modified On     	Modification Details 	             Change
 * *************************************************************************************************************************
 *     
*/

public class NavigationUrlSP extends StoredProcedure{
	
  public NavigationUrlSP(DataSource dataSource) {
	setDataSource(dataSource);
	setFunction(true);
	setSql(SpringConstants.GET_COLLEGE_URL);
	declareParameter(new SqlOutParameter("RetVal", OracleTypes.VARCHAR));
	declareParameter(new SqlParameter("P_SUBORDER_ITEMID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_COURSE_ID", Types.VARCHAR));
	declareParameter(new SqlParameter("P_BUTTON_LABEL", Types.VARCHAR));
    compile();
  }
  public Map execute(String subOrderItemId, String CourseId, String ctaButtonName) throws Exception{
	Map<String, Object> outMap = null;
	try {
	  Map<String, String> inMap = new HashMap<>();
	  inMap.put("P_SUBORDER_ITEMID", subOrderItemId);
	  inMap.put("P_COURSE_ID", CourseId);
	  inMap.put("P_BUTTON_LABEL", ctaButtonName);
	  outMap = execute(inMap);
	}catch (Exception e) {
	  e.printStackTrace();
	}
	return outMap;
  }  

}
