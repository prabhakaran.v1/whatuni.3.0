package com.layer.dao.sp.opendays;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.form.OpenDaysBean;

import com.layer.util.SpringConstants;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : AddMyOpendaysSP.java
* Description   : This class used to inserting opendays for user against
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	               Change
* *************************************************************************************************************************************
* Anbarasan.R             1.0             10.01.2013           First draft   		
* Prabhakaran V.          1.1             27.01.2016           Second draft   		                Added new param for openday registration
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class AddMyOpendaysSP extends StoredProcedure {

  public AddMyOpendaysSP(DataSource dataSource) {
    setDataSource(dataSource);
    setFunction(true);
    setSql(SpringConstants.ADD_MY_OPENDAYS_FN);
    declareParameter(new SqlOutParameter("l_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_open_date", OracleTypes.VARCHAR)); 
    declareParameter(new SqlParameter("p_event_id", OracleTypes.VARCHAR));
		  declareParameter(new SqlParameter("p_new_user", OracleTypes.VARCHAR)); //Added for open day registration by Prabha on 27_JAN_2016_REL
    compile();
  }
  
  public Map execute(OpenDaysBean openDaysBean) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_user_id", openDaysBean.getUserId());
      inputMap.put("p_college_id", openDaysBean.getCollegeId());
      inputMap.put("p_open_date", openDaysBean.getOpenDate());
      inputMap.put("p_event_id", openDaysBean.getEventId());
			   inputMap.put("p_new_user", openDaysBean.getDateExistFlg()); //Added for open day registration by Prabha on 27_JAN_2016_REL
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
}