package com.layer.dao.sp.opendays;

import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.rowmapper.openday.OpenDaysWeekInfoRowMapperImpl;
import com.layer.util.rowmapper.openday.OpenDaysWeekRowMapperImpl;


/**
  * @OpenDaysWeekDateInfoSP
  * @author Thiyagu G
  * @version 1.0
  * @since 17.03.2015
  * @purpose This program is used call the database procedure to build the open days browse page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class OpenDaysWeekDateInfoSP extends StoredProcedure {
  public OpenDaysWeekDateInfoSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("wuni_opendays_pkg.get_ajax_od_datewise_prc");
    declareParameter(new SqlParameter("p_mmyyyy", Types.VARCHAR));
    declareParameter(new SqlParameter("p_start_date", Types.VARCHAR));
    declareParameter(new SqlParameter("p_end_date", Types.VARCHAR));    
    declareParameter(new SqlOutParameter("oc_od_week", OracleTypes.CURSOR, new OpenDaysWeekRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_od_week_info", OracleTypes.CURSOR, new OpenDaysWeekInfoRowMapperImpl()));        
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List parameters) {  
    Map outMap = null;
    try {
        Map inMap = new HashMap();
        inMap.put("p_mmyyyy", parameters.get(0));
        inMap.put("p_start_date", parameters.get(1));
        inMap.put("p_end_date", parameters.get(2));
      
        outMap = execute(inMap);
    } catch (Exception e) {
        e.printStackTrace();
    }
    return outMap;
  } 
    
}
