package com.layer.dao.sp.opendays;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.CommonUtil;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.openday.OpendaysVO;


/**
  * @OpenDaysDateInfoSP
  * @author Thiyagu G
  * @version 1.0
  * @since 17.03.2015
  * @purpose This program is used call the database procedure to build the open days browse page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class OpenDaysDateInfoSP extends StoredProcedure {
  public OpenDaysDateInfoSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql("wuni_opendays_pkg.get_opendays_weekwise_fn");
    declareParameter(new SqlOutParameter("oc_od_week_info", OracleTypes.CURSOR, new OpenDaysWeekInfoRowMapperImpl()));
    declareParameter(new SqlParameter("p_mmyyyy", Types.VARCHAR));
    declareParameter(new SqlParameter("p_start_date", Types.VARCHAR));
    declareParameter(new SqlParameter("p_end_date", Types.VARCHAR));
    compile();
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List parameters) {  
    Map outMap = null;
    try {
        Map inMap = new HashMap();
        inMap.put("p_mmyyyy", parameters.get(0));
        inMap.put("p_start_date", parameters.get(1));
        inMap.put("p_end_date", parameters.get(2));
      
        outMap = execute(inMap);
    } catch (Exception e) {
        e.printStackTrace();
    }
    return outMap;
  } 
 
   private class OpenDaysWeekInfoRowMapperImpl implements RowMapper{
     public Object mapRow(ResultSet rs, int rowNum) throws SQLException{         
         String[] posExt = {"st", "nd", "rd", "th", "th", "th", "th"};
         OpendaysVO openDaysVO = new OpendaysVO();
         openDaysVO.setDays(rs.getString("DAYS"));
         int positionInt = rowNum + 1;
         String positionString = positionInt+posExt[rowNum];
         openDaysVO.setDaysPosition(positionString);
         openDaysVO.setStartDate(rs.getString("START_DATE"));
         ResultSet openDetailsRS = (ResultSet)rs.getObject("OPENDAY_COLLEGES");
         try{
            if(openDetailsRS != null){
                ArrayList openDaysWeekInfoList = new ArrayList();
                OpendaysVO opendaysInnerVO = null;
                while(openDetailsRS.next()){
                    opendaysInnerVO = new OpendaysVO();
                    opendaysInnerVO.setCollegeId(openDetailsRS.getString("COLLEGE_ID"));
                    opendaysInnerVO.setCollegeName(openDetailsRS.getString("COLLEGE_NAME"));
                    opendaysInnerVO.setCollegeNameDisplay(openDetailsRS.getString("COLLEGE_NAME_DISPLAY"));
                    opendaysInnerVO.setOpendaysProviderURL(new SeoUrls().constructOpendaysSeoUrl(openDaysVO.getCollegeName(), openDaysVO.getCollegeId()));                    
                    opendaysInnerVO.setCollegeLogo(openDetailsRS.getString("LOGO_PATH"));
                    if(!GenericValidator.isBlankOrNull(opendaysInnerVO.getCollegeLogo())){                        
                         opendaysInnerVO.setOpdProviderImage(new CommonUtil().getImgPath((opendaysInnerVO.getCollegeLogo()), rowNum));
                    }                    
                    openDaysWeekInfoList.add(opendaysInnerVO);
                }
                openDaysVO.setOpenDaysWeekInfoList(openDaysWeekInfoList);
            }
         }catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          openDetailsRS.close();
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
       openDaysVO.setCurrentMonth(rs.getString("CURRENT_MONTH"));
       return openDaysVO;
     }
 }
}
