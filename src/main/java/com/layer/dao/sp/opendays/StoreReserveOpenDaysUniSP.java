package com.layer.dao.sp.opendays;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.form.opendays.OpenDaysBean;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : StoreReserveOpenDaysUniSP.java
* Description   : This class used to Reserve openday for a providers
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* Author	                 Ver 	           Modified On     	Modification Details 	               Change
* *************************************************************************************************************************************
* Amirtharaj              1.0             17.03.2015           First draft   		
*
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class StoreReserveOpenDaysUniSP extends StoredProcedure {
  public StoreReserveOpenDaysUniSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql("HOT_WUNI.WUNI_OPENDAYS_PKG.SUBMIT_RES_PLACE_OD_USER_FN");
    declareParameter(new SqlOutParameter("p_status", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
  }
  public Map execute(OpenDaysBean openDaysBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", openDaysBean.getUserId());
      inMap.put("P_COLLEGE_ID", openDaysBean.getCollegeId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
