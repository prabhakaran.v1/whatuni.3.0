package com.layer.dao.sp.opendays;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.openday.OpendaySearchVO;

/**
 * @MyOpenDaysRowMapperImpl - RowMapper used for multiple user related cursors
 * @author prabhakaran.v
 * @since 06_May_2020_REL
 * @version - 1.0
 * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
 *
 */
public class MyOpenDaysRowMapperImpl implements RowMapper {
  public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
    OpendaySearchVO openDaysVo = new OpendaySearchVO();
    openDaysVo.setOpenDate(resultSet.getString("OPEN_DATE"));
    openDaysVo.setOpenDay(resultSet.getString("OPEN_DAY"));
    openDaysVo.setOpenDat(resultSet.getString("OPENDATE"));
    openDaysVo.setOpenMonth(resultSet.getString("OPEN_MONTH"));
    openDaysVo.setEventDate(resultSet.getString("EVENT_DATE"));
    openDaysVo.setOpenMonthYear(resultSet.getString("OPEN_MONTH_YEAR"));
    openDaysVo.setCollegeId(resultSet.getString("COLLEGE_ID"));
    openDaysVo.setCollegeName(resultSet.getString("COLLEGE_NAME"));
    openDaysVo.setCollegeDisplayName(resultSet.getString("COLLEGE_NAME_DISPLAY"));
    openDaysVo.setCollegeLogo(resultSet.getString("COLLEGE_LOGO"));
    openDaysVo.setEventDate(resultSet.getString("EVENT_DATE"));
    if (!GenericValidator.isBlankOrNull(openDaysVo.getCollegeName())) {
      openDaysVo.setProviderUrl(new SeoUrls().constructOpendaysSeoUrl(openDaysVo.getCollegeName().trim(), openDaysVo.getCollegeId()));
    }
    openDaysVo.setCollegeLandingUrl(new SeoUrls().construnctUniHomeURL(openDaysVo.getCollegeId(), openDaysVo.getCollegeName()));
    openDaysVo.setOpenDaysCount(resultSet.getString("OPENDAYS_CNT"));
    openDaysVo.setBasketFlag(resultSet.getString("COLLEGE_IN_BASKET"));//Added by Priyaa for Jul-21-2015 Release For Final 5
    ResultSet openDaysRs = (ResultSet)resultSet.getObject("LC_UNI_OPENDAY_DTLS");
    try {
      if (openDaysRs != null) {
        ArrayList openDaysSearchList = new ArrayList();
        OpendaySearchVO openDaySearchVo = null;
        while (openDaysRs.next()){
          openDaySearchVo = new OpendaySearchVO();
          openDaySearchVo.setHeadlines(openDaysRs.getString("HEADLINES"));
          openDaySearchVo.setBookingUrl(openDaysRs.getString("OPENDAY_EXTERNAL_URL"));
          openDaySearchVo.setTown(openDaysRs.getString("COLLEGE_LOCATION"));
          openDaySearchVo.setOpenDayEventId(openDaysRs.getString("EVENT_ID"));
            openDaySearchVo.setQualification(openDaysRs.getString("QUAL_DESC"));
            openDaySearchVo.setEventCategoryId(openDaysRs.getString("EVENT_CATEGORY_ID"));
            ResultSet orderItemIdRS = (ResultSet)openDaysRs.getObject("SUBORDER_ITEM_DETAILS");
            try {
                if(orderItemIdRS != null){
                 ArrayList subOrderItemList = new ArrayList();
                    while (orderItemIdRS.next()) {
                    openDaySearchVo.setSubOrderItemId(orderItemIdRS.getString("SUB_ORDER_ITEM_ID"));
                 
                    openDaySearchVo.setNetworkQualificationId(orderItemIdRS.getString("NETWORK_ID")); 
                    
                    subOrderItemList.add(openDaySearchVo); 
                    }
                    openDaySearchVo.setSubOrderItemIdList(subOrderItemList);  
                }
            } catch (Exception e) {
              e.printStackTrace();
            }finally{
                try {
                  if (orderItemIdRS != null) {
                    orderItemIdRS.close();
                    orderItemIdRS = null;
                  }
                } catch (Exception e) {
                  throw new SQLException(e.getMessage());
                }
            }
            openDaySearchVo.setWebsitePrice(openDaysRs.getString("website_price"));
            openDaySearchVo.setStartTime(openDaysRs.getString("START_TIME")); 
            openDaySearchVo.setEndTime(openDaysRs.getString("END_TIME")); 
          openDaysSearchList.add(openDaySearchVo);
        }
        openDaysVo.setOpenDaysSRList(openDaysSearchList);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      try {
        if (openDaysRs != null) {
          openDaysRs.close();
          openDaysRs = null;
        }
      } catch (Exception e) {
        throw new SQLException(e.getMessage());
      }
    }
    return openDaysVo;
  }
}
