package com.layer.dao.sp.opendays;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import spring.form.OpenDaysBean;

import com.layer.util.SpringConstants;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : AddOpenDayForReservePlacesSP.java
 * Description   : This class used to inserting opendays for user against reserve place.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                   Ver 	            Modified On     	        Modification Details 	               Change
 * *************************************************************************************************************************************
 * Yogeswari.S               1.0              19.05.2015                First draft   		
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
public class AddOpenDayForReservePlacesSP extends StoredProcedure {

  public AddOpenDayForReservePlacesSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql(SpringConstants.SAVE_USER_ACTION_VAL_PRC);
    declareParameter(new SqlParameter("p_user_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_action_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_action_status", Types.VARCHAR));
    declareParameter(new SqlParameter("p_action_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_action_key_id", Types.NUMERIC));
    declareParameter(new SqlParameter("p_action_details", Types.VARCHAR));
    declareParameter(new SqlParameter("p_track_session_id", Types.NUMERIC));
    compile();
  }

  public Map execute(OpenDaysBean openDaysBean) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_user_id", (openDaysBean.getUserId() == null || openDaysBean.getUserId().trim().length() == 0) ? "0" : openDaysBean.getUserId());
      inputMap.put("p_action_type", "OD");
      inputMap.put("p_action_status", "C");
      inputMap.put("p_action_url", null);
      inputMap.put("p_action_key_id", openDaysBean.getCollegeId());
      inputMap.put("p_action_details", openDaysBean.getOpenDate());
      inputMap.put("p_track_session_id", openDaysBean.getUserTrackSessionId());
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }

}
