package com.layer.dao.sp.opendays;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.openday.OpenDaysLandingHeroImageRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.openday.OpendaysVO;

/**
  * @OpenDaysLandingLocationSP
  * @author Thiyagu G
  * @version 1.0
  * @LastModifiedBranch : FEATURE_VIRTUAL_OPENDAY branch commit
  * @since 17.03.2015
  * @purpose  This program is used call the database procedure to build the open days browse page.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.       Changes desc                                      Rel Ver.
  * *************************************************************************************************************************
  * 17-March-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
  * 06.05.2020     Prabhakaran V.            1.1        Added in param - P_DYNAMIC_RANDOM_NUMBER          1.1
  */
public class OpenDaysLandingLocationSP extends StoredProcedure {
  public OpenDaysLandingLocationSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.GET_OPENDAY_HOME_PRC);    
    declareParameter(new SqlParameter("P_USER_ID", Types.NUMERIC));
    declareParameter(new SqlOutParameter("oc_heroimage", OracleTypes.CURSOR, new OpenDaysLandingHeroImageRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_future_opendays", OracleTypes.CURSOR, new OpenDaysLandingUpCommingRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_bread_crumb", Types.VARCHAR));
    declareParameter(new SqlOutParameter("PC_USER_PAST_OPENDAYS", OracleTypes.CURSOR, new MyOpenDaysRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_USER_FUTURE_OPENDAYS", OracleTypes.CURSOR, new MyOpenDaysRowMapperImpl()));
    declareParameter(new SqlParameter("P_BASKET_ID", Types.VARCHAR)); 
    declareParameter(new SqlParameter("P_DYNAMIC_RANDOM_NUMBER", Types.VARCHAR));
    compile();
  }

  /**
    * This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(OpendaysVO opendaysVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("P_USER_ID", opendaysVO.getUserId());
    inMap.put("P_BASKET_ID", opendaysVO.getBasketId());
    inMap.put("P_DYNAMIC_RANDOM_NUMBER", opendaysVO.getRandomNumber());
    outMap = execute(inMap);
   
    return outMap;
  }
  
  /**
   * @OpenDaysLandingUpCommingRowMapperImpl - Feature open day pod in open day home page
   * @author prabhakaran.v
   * @since 06.05.2020_REL
   * @LastModifiedBranch : FEATURE_VIRTUAL_OPENDAY branch commit
   * @comments - Added EVENT_CATEGORY_ID, EVENT_DATE and EVENT_CATEGORY_NAME columns
   *
   */
  private class OpenDaysLandingUpCommingRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      OpendaysVO openDaysVO = new OpendaysVO();      
      openDaysVO.setCollegeId(rs.getString("COLLEGE_ID"));
      openDaysVO.setCollegeName(rs.getString("COLLEGE_NAME"));
      openDaysVO.setCollegeNameDisplay(rs.getString("COLLEGE_NAME_DISPLAY"));
      openDaysVO.setCollegeLogo(rs.getString("COLLEGE_LOGO"));
      openDaysVO.setCollegeLocation(rs.getString("COLLEGE_LOCATION"));
      openDaysVO.setOpenDate(rs.getString("OPEN_DATE"));
      openDaysVO.setOpenDay(rs.getString("OPEN_DAY"));
      openDaysVO.setOpenMonth(rs.getString("OPEN_MONTH"));
      openDaysVO.setOpenMonthYear(rs.getString("OPEN_MONTH_YEAR"));
      openDaysVO.setStartDate(rs.getString("START_DATE"));
      openDaysVO.setEndDate(rs.getString("END_DATE"));
      openDaysVO.setStartTime(rs.getString("START_TIME"));
      openDaysVO.setEndTime(rs.getString("END_TIME"));
      openDaysVO.setWeekendFlag(rs.getString("WEEKEND_FLAG"));
      openDaysVO.setStartDateTime(rs.getString("STARTDATETIME"));
      openDaysVO.setEndDateTime(rs.getString("ENDDATETIME"));
      openDaysVO.setEventId(rs.getString("EVENT_CALENDAR_ITEM_ID"));
      openDaysVO.setEventDesc(rs.getString("EVENT_DESCRIPTION"));
      openDaysVO.setHeadline(rs.getString("HEADLINES"));
      openDaysVO.setUrl(rs.getString("URL"));
      openDaysVO.setOpendayVenue(rs.getString("VENUE"));
      openDaysVO.setOrganizationName(rs.getString("ORGANIZATION_NAME"));
      openDaysVO.setQualId(rs.getString("QUAL_ID"));
      openDaysVO.setQualType(rs.getString("QUAL_TYPE"));
      openDaysVO.setBookingUrl(rs.getString("BOOKING_URL"));
      openDaysVO.setOpendayExist(rs.getString("OD_EXIST"));
      openDaysVO.setOrderItemId(rs.getString("SUB_ORDER_ITEM_ID"));
      openDaysVO.setNetworkId(rs.getString("NETWORK_ID")); 
      openDaysVO.setWebsitePrice(rs.getString("WEBSITE_PRICE"));
      openDaysVO.setOpendaysProviderURL(new SeoUrls().constructOpendaysSeoUrl(openDaysVO.getCollegeName(), openDaysVO.getCollegeId()));
      openDaysVO.setEventCategoryName(rs.getString("EVENT_CATEGORY_NAME"));
      openDaysVO.setEventCategoryId(rs.getString("EVENT_CATEGORY_ID"));
      openDaysVO.setEventDate(rs.getString("EVENT_DATE"));
      return openDaysVO;
    }
  }
}

