package com.layer.dao.sp.opendays;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.rowmapper.openday.OpenDaysLandingHeroImageRowMapperImpl;
import com.layer.util.rowmapper.openday.OpenDaysWeekInfoRowMapperImpl;
import com.layer.util.rowmapper.openday.OpenDaysWeekRowMapperImpl;
import com.wuni.util.valueobject.openday.OpendaysVO;

/**
   * @OpenDaysLandingDateSP
   * @author Thiyagu G
   * @version 1.0
   * @since 17.03.2015
   * @purpose  This program is used call the database procedure to build the open days browse page.
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.       Changes desc                                      Rel Ver.
   * *************************************************************************************************************************
   * 17-March-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
   */
public class OpenDaysLandingDateSP extends StoredProcedure {
  public OpenDaysLandingDateSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("wuni_opendays_pkg.get_opendays_datewise_prc");
    declareParameter(new SqlParameter("p_mmyyyy", Types.VARCHAR));
    declareParameter(new SqlParameter("p_start_date", Types.VARCHAR));
    declareParameter(new SqlParameter("p_end_date", Types.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", Types.NUMERIC));
    declareParameter(new SqlOutParameter("oc_heroimage", OracleTypes.CURSOR, new OpenDaysLandingHeroImageRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_od_months", OracleTypes.CURSOR, new OpenDaysMonthsRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_od_week", OracleTypes.CURSOR, new OpenDaysWeekRowMapperImpl()));
    declareParameter(new SqlOutParameter("oc_od_week_info", OracleTypes.CURSOR, new OpenDaysWeekInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_bread_crumb", Types.VARCHAR));
    declareParameter(new SqlOutParameter("PC_USER_PAST_OPENDAYS", OracleTypes.CURSOR, new MyOpenDaysRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_USER_FUTURE_OPENDAYS", OracleTypes.CURSOR, new MyOpenDaysRowMapperImpl()));
  }

  /**
    *   This function is used to map input parameters for the procedure call.
    * @param inputList
    * @return parameter list as Collection object.
    */
  public Map execute(List parameters) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_mmyyyy", parameters.get(0));
      inMap.put("p_start_date", parameters.get(1));
      inMap.put("p_end_date", parameters.get(2));
      inMap.put("p_user_id", parameters.get(3));
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  private class OpenDaysMonthsRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      OpendaysVO openDaysVO = new OpendaysVO();
      openDaysVO.setMonthYear(rs.getString("MONYY"));
      openDaysVO.setMonthFullYear(rs.getString("MMYYYY"));
      openDaysVO.setOdStatus(rs.getString("SELECTED_FLAG"));
      return openDaysVO;
    }
  }
  //
}
