package com.layer.dao.sp.opendays;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import spring.valueobject.search.RefineByOptionsVO;
import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.openday.OpenDaysSrRowMapperImpl;
import com.wuni.util.valueobject.openday.OpendaySearchVO;
import com.wuni.util.valueobject.openday.OpendaysVO;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : OpenDaySearchResultsSP.java
* Description   : This class used to fetch openday search results
* @version      : 1.0
* @LastModifiedBranch : FEATURE_VIRTUAL_OPENDAY branch commit
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	               Change                 
* *************************************************************************************************************************************
* Priyaa Parthasarathy           1.0                10.01.2013           First draft   	
* Prabhakaran V.                 1.1                03.07.2018           Opendays search resull page has modified
* Prabhakaran V.                 1.2                06.05.2020           Added in param - P_DYNAMIC_RANDOM_NUMBER
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class OpenDaySearchResultsSP extends StoredProcedure {
  public OpenDaySearchResultsSP(DataSource dataSource) {
    setDataSource(dataSource);
    setSql(SpringConstants.GET_OPEN_DAY_SEARCH_PRC);
    declareParameter(new SqlParameter("P_SEARCH_KEYWORD", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_MONTHYEAR", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_QUALIFICATION", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_SEARCH_LOCATION", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_PAGE_NO", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_DYNAMIC_RANDOM_NUMBER", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("PC_SEARCH_RESULTS", OracleTypes.CURSOR, new OpenDaysSrRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_QUALIFICATION_REFINE", OracleTypes.CURSOR, new QualificationRefineRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_OPEN_DAYS_MONTHS", OracleTypes.CURSOR, new UniOpenDayMonthRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_DEFAULT_OPENDAYS", OracleTypes.CURSOR, new OpenDaysSrRowMapperImpl()));
    declareParameter(new SqlOutParameter("p_highlight_monthyear", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_TOTAL_OPEN_DAYS_COUNT", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("p_bread_crumbs", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_advertiser_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_future_openday_exists_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_highlight_month", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_LOCATIONS", OracleTypes.CURSOR, new RefineRowMapperImpl()));
    compile();
  }
  public Map execute(OpendaySearchVO openDaysVO) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("P_SEARCH_KEYWORD", openDaysVO.getSearchKeyword());
      inputMap.put("p_college_id", openDaysVO.getCollegeId());
      inputMap.put("P_MONTHYEAR", openDaysVO.getMonthYear());
      inputMap.put("P_QUALIFICATION", openDaysVO.getQualification());
      inputMap.put("P_SEARCH_LOCATION", openDaysVO.getSearchLocation());
      inputMap.put("P_PAGE_NO", openDaysVO.getPageNo());
      inputMap.put("P_USER_ID", openDaysVO.getUserId());
      inputMap.put("P_DYNAMIC_RANDOM_NUMBER", openDaysVO.getRandomNumber());
      
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
  private class QualificationRefineRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      RefineByOptionsVO refineOptionsVo = new RefineByOptionsVO();
      refineOptionsVo.setRefineCode(resultSet.getString("refine_code"));
      refineOptionsVo.setRefineDesc(resultSet.getString("refine_desc"));
      refineOptionsVo.setSelectedStudyMode(resultSet.getString("selected_flag"));
      return refineOptionsVo;
    }
  }
  private class RefineRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      RefineByOptionsVO refineOptionsVo = new RefineByOptionsVO();
      refineOptionsVo.setRefineDesc(resultSet.getString("location"));
      refineOptionsVo.setRefineCode(resultSet.getString("location_value"));
      refineOptionsVo.setLocationSelectedFlag(resultSet.getString("selected_flag"));
      return refineOptionsVo;
    }
  }
  private class UniOpenDayMonthRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      OpendaysVO opendaysVO = new OpendaysVO();
      opendaysVO.setOpenDayMonthYear(rs.getString("monyy"));
      opendaysVO.setOpenDayMonthYearDesc(rs.getString("mmyyyy"));
      opendaysVO.setOpenDayStatus(rs.getString("selected_flag"));
      return opendaysVO;
    }
  }
}
