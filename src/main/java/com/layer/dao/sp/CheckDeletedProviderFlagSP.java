package com.layer.dao.sp;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.review.bean.ReviewListBean;
import WUI.utilities.GlobalConstants;

/**
  * @CheckDeletedProviderFlagSP
  * @author Prabhakaran V.
  * @version 1.0
  * @since 28.01.2016
  * @purpose This program is used check the provider is deleted or not and return YES/NO flag
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
public class CheckDeletedProviderFlagSP extends StoredProcedure {
  public CheckDeletedProviderFlagSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.CHECK_COLLEGE_EXIST_FN);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", Types.NUMERIC));
  }
  public Map execute(ReviewListBean reviewBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_college_id", reviewBean.getCollegeId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
