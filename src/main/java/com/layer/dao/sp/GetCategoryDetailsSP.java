package com.layer.dao.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.valueobject.CommonVO;

public class GetCategoryDetailsSP extends StoredProcedure {
  public GetCategoryDetailsSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql("WU_SEO_PKG.get_browse_category_details_fn");
    declareParameter(new SqlOutParameter("lc_browse_details", OracleTypes.CURSOR, new CategoryCodeRowMapperImpl()));
    declareParameter(new SqlParameter("p_subject_cat_code", OracleTypes.VARCHAR));
    compile();
  }
  //  
  public Map execute(String categoryName) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("p_subject_cat_code", categoryName);
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
  private class CategoryCodeRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      CommonVO commonVO = new CommonVO();
      commonVO.setCategoryCode(resultSet.getString("browse_cat_id"));
      commonVO.setCategoryName(resultSet.getString("description"));
      return commonVO;
    }
  }
}
