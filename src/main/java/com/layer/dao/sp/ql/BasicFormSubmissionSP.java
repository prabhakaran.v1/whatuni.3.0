package com.layer.dao.sp.ql;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.ql.PostEnquriyRowMapperImpls;
import com.wuni.advertiser.ql.form.QLFormBean;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : BasicFormSubmissionSP.java
* Description   : Store procedure class.
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	      Change
* *************************************************************************************************************************************
* Anbarasan.r             1.0             12.10.2012           First draft   		
* Thiyagu G               wu_546          03_NOV_2015          Modified                Save the bulk prospectus data.
* Sabapathi S             wu_582          23_OCT_2018          Modified                Added screenwidth for stats logging.
* Hema.S                  wu_171219       17_DEC_2019          Modified                Added consent flag input parameter
* Sangeeth.S			  wu_31032020	  09_MAR_2020		   Modified				   Added lat and long input param
* Kailash L                               21_JUL_2020          Modified				   Added source order itemId for stats logging
* Sri Sankari		      wu_20200917     17_SEP_2020            1.7                   Added stats log for tariff logging(UCAS point)
* Sujitha V               wu_20201110     10_NOV_2020          Modified                Added YOE value in d_session_log stats
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class BasicFormSubmissionSP extends StoredProcedure {

  public BasicFormSubmissionSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(SpringConstants.WU_ENQUIRY_FORM_SUBMISSION_PRC);
    declareParameter(new SqlParameter("p_affiliate_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_course_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_message", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_email", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_forename", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_surname", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_html_id", OracleTypes.VARCHAR));
    declareParameter(new SqlInOutParameter("p_session_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_user_agent", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_ip", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_request_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_referer_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_suborder_item_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_network_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_type_of_prospectus", Types.VARCHAR));
    declareParameter(new SqlParameter("p_widget_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_level_of_study", Types.VARCHAR));
    declareParameter(new SqlParameter("p_addressline_one", Types.VARCHAR));
    declareParameter(new SqlParameter("p_addressline_two", Types.VARCHAR));
    declareParameter(new SqlParameter("p_town", Types.VARCHAR));
    declareParameter(new SqlParameter("p_postcode", Types.VARCHAR));
    declareParameter(new SqlParameter("p_country_residence", Types.VARCHAR));
    declareParameter(new SqlParameter("p_date", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_month", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_year", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_course_start_year", Types.VARCHAR));
    declareParameter(new SqlParameter("p_post_enquiry_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_mobile_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_grade_type", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_grades", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_no_grades_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_enquiry_id", OracleTypes.NUMBER));
    declareParameter(new SqlInOutParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("o_post_enquiry", OracleTypes.CURSOR, new PostEnquriyRowMapperImpls()));
    declareParameter(new SqlOutParameter("o_user_sent", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_college_names", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_track_session", Types.VARCHAR));
    declareParameter(new SqlParameter("p_bulk_process", Types.VARCHAR));
    declareParameter(new SqlParameter("p_one_click_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_new_user_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_password", Types.VARCHAR));
    //
    declareParameter(new SqlParameter("p_marketing_email_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_solus_email_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_survey_email_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_screen_width", Types.VARCHAR)); // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
    declareParameter(new SqlParameter("P_CLIENT_CONSENT_FLAG", Types.VARCHAR));//Added this for passing consent flag details By Hema.S on 17_12_2019_REL
    declareParameter(new SqlParameter("P_LATITUDE", Types.VARCHAR));// March2020 rel - Sangeeth.S: Added latitude and longitde for stats logging
    declareParameter(new SqlParameter("P_LONGITUDE", Types.VARCHAR));// March2020 rel - Sangeeth.S: Added latitude and longitde for stats logging
    declareParameter(new SqlParameter("P_SOURCE_ORDER_ITEM_ID", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_UCAS_TARIFF", OracleTypes.NUMBER));// wu_20200917 - Sri Sankari: Added UCAS tariff point
    declareParameter(new SqlParameter("P_YEAR_OF_ENTRY", OracleTypes.NUMBER));//Added this for YOE logging in d_session by Sujitha V on 2020_NOV_10
    //
    compile();
  }

  public Map execute(QLFormBean qlFormBean) {
    Map outMap = null;
    Map inMap = new HashMap();
    String emailAddress = "";
    String nullFlag = null;
    try {
      inMap.put("p_affiliate_id", qlFormBean.getAffliatedId());
      inMap.put("p_college_id", qlFormBean.getCollegeId());
      inMap.put("p_course_id", qlFormBean.getCourseId());
      inMap.put("p_message", qlFormBean.getMessage());
      if (!GenericValidator.isBlankOrNull(qlFormBean.getEmailAddress())) {
        emailAddress = qlFormBean.getEmailAddress().trim();
      }
      inMap.put("p_user_email", emailAddress);
      inMap.put("p_forename", qlFormBean.getFirstName());
      inMap.put("p_surname", qlFormBean.getLastName());
      inMap.put("p_html_id", qlFormBean.getHtmlId()); //blank
      inMap.put("p_session_id", qlFormBean.getSessionId());
      inMap.put("p_user_agent", qlFormBean.getClientBrowser());
      inMap.put("p_user_ip", qlFormBean.getClientIp());
      inMap.put("p_request_url", qlFormBean.getRequestUrl());
      inMap.put("p_referer_url", qlFormBean.getRefferalUrl());
      inMap.put("p_suborder_item_id", qlFormBean.getSubOrderItemId());
      inMap.put("p_network_id", qlFormBean.getCpeQualificationNetworkId());
      inMap.put("p_type_of_prospectus", qlFormBean.getTypeOfProspectus()); //
      inMap.put("p_widget_id", qlFormBean.getWidgetId());
      inMap.put("p_level_of_study", qlFormBean.getStudyLevelId());
      inMap.put("p_addressline_one", qlFormBean.getAddress1());
      inMap.put("p_addressline_two", qlFormBean.getAddress2());
      inMap.put("p_town", qlFormBean.getTown());
      inMap.put("p_postcode", qlFormBean.getPostCode());
      inMap.put("p_country_residence", qlFormBean.getCountryOfResidence());
      inMap.put("p_date", qlFormBean.getDate());
      inMap.put("p_month", qlFormBean.getMonth());
      inMap.put("p_year", qlFormBean.getYear());
      inMap.put("p_course_start_year", qlFormBean.getYeartoJoinCourse());
      inMap.put("p_post_enquiry_flag", qlFormBean.getPostButtonFlag());
      inMap.put("p_mobile_flag", qlFormBean.getMobileFlag());
      inMap.put("p_grade_type", qlFormBean.getProsGradePointSelected());
      inMap.put("p_grades", qlFormBean.getProsGradeValueSelected());
      inMap.put("p_no_grades_flag", qlFormBean.getProsDoNotGradesChkBox());
      inMap.put("p_user_id", qlFormBean.getUserId());
      inMap.put("p_track_session", qlFormBean.getTrackSessionId());
      inMap.put("p_bulk_process", nullFlag);
      inMap.put("p_one_click_flag", nullFlag);
      inMap.put("p_password", qlFormBean.getPassword());
      inMap.put("p_marketing_email_flag", qlFormBean.getMarketingEmail());
      inMap.put("p_solus_email_flag", qlFormBean.getSolusEmail());
      inMap.put("p_survey_email_flag", qlFormBean.getSurveyEmail());
      inMap.put("p_screen_width", qlFormBean.getScreenWidth()); // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
      inMap.put("P_CLIENT_CONSENT_FLAG", qlFormBean.getConsentFlag());//Added this for passing consent flag details By Hema.S on 17_12_2019_REL 
      inMap.put("P_LATITUDE", qlFormBean.getLatitude());
      inMap.put("P_LONGITUDE", qlFormBean.getLongitude());   
      inMap.put("P_SOURCE_ORDER_ITEM_ID", qlFormBean.getSponsoredOrderItemId());
      inMap.put("P_UCAS_TARIFF", qlFormBean.getUserUcasScore());
      inMap.put("P_YEAR_OF_ENTRY", qlFormBean.getYearOfEntry());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }

}
