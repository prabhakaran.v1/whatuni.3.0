/*
  * Added By Sekhar for wu417_20101030 for QL.
  */
package com.layer.dao.sp.ql;

import java.sql.Connection;
import java.sql.Types;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;

import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.SpringConstants;
import com.layer.util.rowmapper.ql.PostEnquriyRowMapperImpl;
import com.wuni.advertiser.ql.form.QLFormBean;
import com.wuni.util.sql.OracleArray;

/**
* @EnqBasketSubmitSP
* @author Thiyagu G
* @version 1.0
* @since 03.11.2015
* @purpose This program is used call the database procedure to build the registration lightbox data load.
* Change Log
* *************************************************************************************************************************
* Date           Name                      Ver.     Changes desc                                                 Rel Ver.
* *************************************************************************************************************************
* 03_NOV_2015   Thiyagu G                  1.0      Save the bulk prospectus data.                                wu_546
* 23_OCT_2018   Sabapathi S                1.1      Added screen width for stats logging.                         wu_582
* 17_DEC_2019   Hema.S                     1.2      Added consent flag details                                    wu_597
* 09_MAR_2020   Sangeeth.S			  				Added lat and long input param								wu_10032020
* 17_SEP_2020   Sri Sankari                1.4      Added stats log for tariff logging(UCAS point)                wu_20200917
* 10_NOV_2020   Sujitha V                  1.5      Added YOE value in d_session_log stats                        wu_20201110
*/
public class EnqBasketSubmitSP extends StoredProcedure {
	DataSource ds = null;
  public EnqBasketSubmitSP(DataSource datasource) {
    setDataSource(datasource);
    this.ds = datasource;
    setSql(SpringConstants.BULK_PROSPECTUS_SUBMIT_PRC);
    declareParameter(new SqlParameter("p_affiliate_id", OracleTypes.NUMBER));
    declareParameter(new SqlInOutParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_enq_basket_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_enquiry_type", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_email", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_forename", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_surname", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_html_id", OracleTypes.VARCHAR));
    declareParameter(new SqlInOutParameter("p_session_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_user_agent", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_ip", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_subject", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_request_url", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_referer_url", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_network_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_type_of_prospectus", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_level_of_study", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_course_start_year", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_addressline_one", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_addressline_two", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_town", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_postcode", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_country_residence", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_date", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_month", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_year", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_mobile_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_grade_type", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_grades", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_no_grades_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_track_session", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_college_names", Types.VARCHAR));
    declareParameter(new SqlOutParameter("p_colleges_details", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_new_user_flag", Types.VARCHAR));
    declareParameter(new SqlOutParameter("oc_dp_suggestion_list", OracleTypes.CURSOR, new PostEnquriyRowMapperImpl()));
    declareParameter(new SqlParameter("p_password", Types.VARCHAR));
    declareParameter(new SqlParameter("p_marketing_email_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_solus_email_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_survey_email_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_screen_width", Types.VARCHAR));// wu582_20181023 - Sabapathi: Added screenwidth for stats logging
    declareParameter(new SqlParameter("pt_client_consent_flag_arr",OracleTypes.ARRAY,"HOT_WUNI.TB_CLIENT_CONSENT_DETAIL"));//Added this for client consent details by Hema.S on 17_12_19_REL
    declareParameter(new SqlParameter("P_LATITUDE", Types.VARCHAR));// March2020 rel - Sangeeth.S: Added latitude and longitde for stats logging
    declareParameter(new SqlParameter("P_LONGITUDE", Types.VARCHAR));// March2020 rel - Sangeeth.S: Added latitude and longitde for stats logging
    declareParameter(new SqlParameter("P_UCAS_TARIFF", OracleTypes.NUMBER));// wu_20200917 - Sri Sankari: Added UCAS tariff point
	declareParameter(new SqlParameter("P_SOURCE_ORDER_ITEM_ID", OracleTypes.NUMBER));// wu_20200917 - Keerthana: Added source order item id for FI chain logging
	declareParameter(new SqlParameter("P_YEAR_OF_ENTRY", OracleTypes.NUMBER));//Added this for YOE logging in d_session by Sujitha V on 2020_NOV_10
	compile();
  }

  public Map execute(QLFormBean qlFormBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      Connection connection = null;
      inMap.put("p_affiliate_id", qlFormBean.getAffliatedId());
      inMap.put("p_user_id", qlFormBean.getUserId());
      inMap.put("p_enq_basket_id", qlFormBean.getEnquiryBasketId());
      inMap.put("p_enquiry_type", qlFormBean.getEnquiryType());
      inMap.put("p_user_email", qlFormBean.getEmailAddress());
      inMap.put("p_forename", qlFormBean.getFirstName());
      inMap.put("p_surname", qlFormBean.getLastName());
      inMap.put("p_html_id", qlFormBean.getHtmlId());
      inMap.put("p_session_id", qlFormBean.getSessionId());
      inMap.put("p_user_agent", qlFormBean.getClientBrowser());
      inMap.put("p_user_ip", qlFormBean.getClientIp());
      inMap.put("p_subject", qlFormBean.getSubject());
      inMap.put("p_request_url", qlFormBean.getRequestUrl());
      inMap.put("p_referer_url", qlFormBean.getRefferalUrl());
      inMap.put("p_network_id", qlFormBean.getNetworkId());
      inMap.put("p_type_of_prospectus", qlFormBean.getTypeOfProspectus());
      inMap.put("p_level_of_study", qlFormBean.getStudyLevelId());
      inMap.put("p_course_start_year", qlFormBean.getYeartoJoinCourse());
      inMap.put("p_addressline_one", qlFormBean.getAddress1());
      inMap.put("p_addressline_two", qlFormBean.getAddress2());
      inMap.put("p_town", qlFormBean.getTown());
      inMap.put("p_postcode", qlFormBean.getPostCode());
      inMap.put("p_country_residence", qlFormBean.getCountryOfResidence());
      inMap.put("p_date", qlFormBean.getDate());
      inMap.put("p_month", qlFormBean.getMonth());
      inMap.put("p_year", qlFormBean.getYear());
      inMap.put("p_mobile_flag", qlFormBean.getMobileFlag());
      inMap.put("p_grade_type", qlFormBean.getProsGradePointSelected());
      inMap.put("p_grades", qlFormBean.getProsGradeValueSelected());
      inMap.put("p_no_grades_flag", qlFormBean.getProsDoNotGradesChkBox());
      inMap.put("p_track_session", qlFormBean.getTrackSessionId());
      inMap.put("p_password", qlFormBean.getPassword());
      inMap.put("p_marketing_email_flag", qlFormBean.getMarketingEmail());
      inMap.put("p_solus_email_flag", qlFormBean.getSolusEmail());
      inMap.put("p_survey_email_flag", qlFormBean.getSurveyEmail());
      inMap.put("p_screen_width", qlFormBean.getScreenWidth()); // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
    //Added this for passing consent flag details By Hema.S on 17_12_2019_REL
      connection = ds.getConnection();
      try{  
        //Arrays.stream(qlFormBean.getCollegeConsentArray()).flatMap(x -> Arrays.stream(x)).forEach(System.out::println);
        ARRAY collegeConsentFlagArray = OracleArray.getOracleArray(connection, "HOT_WUNI.TB_CLIENT_CONSENT_DETAIL", qlFormBean.getCollegeConsentArray());
        inMap.put("pt_client_consent_flag_arr", collegeConsentFlagArray);
        inMap.put("P_LATITUDE", qlFormBean.getLatitude());
        inMap.put("P_LONGITUDE", qlFormBean.getLongitude());
        inMap.put("P_UCAS_TARIFF", qlFormBean.getUserUcasScore());
		inMap.put("P_SOURCE_ORDER_ITEM_ID", qlFormBean.getSponsoredOrderItemId());
		inMap.put("P_YEAR_OF_ENTRY", qlFormBean.getYearOfEntry());
        outMap = execute(inMap);
        //System.out.println("outMap" + outMap);
      } catch(Exception e){
         e.printStackTrace();
      }
      finally {
        try {
  	if (connection != null) {
  	   connection.close();
  	}
        } catch (Exception closeException) {
  	     closeException.printStackTrace();
        }
     }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }

}
