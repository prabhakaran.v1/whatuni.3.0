/*
 * Added By Sekhar for wu417_20101030 for QL.
 */
package com.layer.dao.sp.ql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;
import oracle.sql.STRUCT;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.advertiser.ql.form.QLQuestionAnswerBean;

public class QLAdvanceFormInfoSP extends StoredProcedure{

  public QLAdvanceFormInfoSP() {
  }
  public QLAdvanceFormInfoSP(DataSource datasource){
    setDataSource(datasource);
    setSql("HOT_ADMIN.WU_QL_ENQ_PKG.GET_QL_ATTRIBUTES_PRC");
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SUBORDER_ITEM_ID", Types.VARCHAR));
    declareParameter(new SqlOutParameter("PC_ATTR_VALUES", OracleTypes.CURSOR, new QLAttributesRowMapperImpl()));
    declareParameter(new SqlOutParameter("P_STUDY_LEVEL_TYPE", OracleTypes.VARCHAR));
  }
  
  public Map execute(List inputList){
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("P_USER_ID", inputList.get(0));
    inMap.put("P_SUBORDER_ITEM_ID", inputList.get(1));
    outMap = execute(inMap);
    return outMap;
    
  }

private class QLAttributesRowMapperImpl implements RowMapper{
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
    QLQuestionAnswerBean qlquestionaansbean = new QLQuestionAnswerBean();
    qlquestionaansbean.setOrderAttributeId(rs.getString("ORDER_ATTRIBUTE_ID"));
    qlquestionaansbean.setTypeAttributeId(rs.getString("TYPE_ATTRIBUTE_ID"));
    qlquestionaansbean.setAttributeId(rs.getString("ATTRIBUTE_ID"));
    qlquestionaansbean.setAttrValueSeq(rs.getString("ATTR_VAL_DISPLAY_SEQ"));
    qlquestionaansbean.setAttributeName(rs.getString("ATTRIBUTE_NAME"));
    qlquestionaansbean.setDisplaySeq(rs.getString("DISPLAY_SEQ"));
    qlquestionaansbean.setMandatory(rs.getString("MANDATORY"));
    qlquestionaansbean.setDefaultValue(GenericValidator.isBlankOrNull(rs.getString("DEFAULT_VALUE")) ? "" : rs.getString("DEFAULT_VALUE"));
    qlquestionaansbean.setAttributeText(rs.getString("ATTRIBUTE_TEXT"));
    qlquestionaansbean.setOptionFlag(rs.getString("OPTION_FLAG"));
    //
    //if ("Y".equals(qlquestionaansbean.getOptionFlag())) {
    java.sql.Array optionsRS = (java.sql.Array)rs.getObject("OPTIONS");// added for weblogic migration 28-JUN_2016_REL
    Object datanum[] = (Object[])optionsRS.getArray();
    ArrayList optionList = new ArrayList();
    for (int i = 0; i < datanum.length; i++) {
      STRUCT struct = (STRUCT)datanum[i];
      Object obj[] = struct.getAttributes();
      QLQuestionAnswerBean optionsVO = new QLQuestionAnswerBean();
      optionsVO.setOptionId(String.valueOf(obj[0]));
      optionsVO.setOptionDesc((String)obj[1]);
      optionsVO.setOptionValue((String)obj[2]);
      optionList.add(optionsVO);
    }
    qlquestionaansbean.setOptions(optionList);
    //}
    //
    qlquestionaansbean.setMaxLength(rs.getString("MAX_LENGTH"));
    qlquestionaansbean.setFormatDesc(rs.getString("FORMAT_DESC"));
    qlquestionaansbean.setDataTypeDesc(rs.getString("DATA_TYPE_DESC"));
    qlquestionaansbean.setGroupId(rs.getString("GROUP_ID"));
    qlquestionaansbean.setHelpFlag(rs.getString("HELP_FLAG"));
    qlquestionaansbean.setHelpText(rs.getString("HELP_TEXT"));
    qlquestionaansbean.setOptionId(rs.getString("OPTION_ID"));
    qlquestionaansbean.setAttributeValue(rs.getString("ATTRIBUTE_VALUE"));
    qlquestionaansbean.setAttributeValueId(rs.getString("ENQ_ATTR_VAL_ID"));
    qlquestionaansbean.setClassName(rs.getString("css_class_name"));
    //
    return qlquestionaansbean;

  }
}



}
