package com.layer.dao.sp.ql;

import java.sql.Connection;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;

import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.rowmapper.ql.PostEnquriyRowMapperImpls;
import com.wuni.util.sql.OracleArray;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : PostenquirySubmissionSP.java
* Description   : Post enqiry submit SP
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	   Change
* *************************************************************************************************************************************
* Sabapathi S.            wu_582          10_Oct_2018          Modified             Added Screen width for stats logging
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class PostenquirySubmissionSP extends StoredProcedure{

  DataSource ds = null;
  public PostenquirySubmissionSP(DataSource datasource) {
    setDataSource(datasource);
    this.ds = datasource;
    setSql("whatuni_interactive_pkg.post_enquiry_submit_prc");
    declareParameter(new SqlInOutParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_affiliate_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_html_id", OracleTypes.VARCHAR));
    declareParameter(new SqlInOutParameter("p_session_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_user_agent", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_ip", OracleTypes.VARCHAR));    
    declareParameter(new SqlParameter("p_request_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_referer_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_network_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_type", Types.VARCHAR));
    declareParameter(new SqlParameter("p_mobile_flag", Types.VARCHAR));
    declareParameter(new SqlParameter("p_attr_val_arr", OracleTypes.ARRAY, "TB_POST_ENQUIRY_SUB"));
    declareParameter(new SqlParameter("p_track_session", Types.VARCHAR));
    declareParameter(new SqlParameter("p_bulk_process", Types.VARCHAR));
	declareParameter(new SqlParameter("p_enq_basket_id", OracleTypes.NUMBER));    
	declareParameter(new SqlParameter("p_bulk_pros_new_user", Types.VARCHAR));//Added for new registration by Prabha on 27_JAN_2016_REL
	declareParameter(new SqlOutParameter("p_college_names", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_post_enquiry", OracleTypes.CURSOR, new PostEnquriyRowMapperImpls()));
    declareParameter(new SqlParameter("p_screen_width", Types.VARCHAR)); // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
    compile();
  }
  public Map execute(List inputList, Object[][] postValues){
      Map outMap = null;
      Connection connection = null;
      /*for(int i=0; i<postValues.length;i++)
          {
            for(int j=0; j<postValues[i].length;j++) 
            {
              System.out.println("attrvalues : "+i+" "+j+" "+  postValues[i][j]);
            }
            
          }*/
      try{
          Map inMap = new HashMap();
          connection = ds.getConnection();
          ARRAY values = OracleArray.getOracleArray(connection, "TB_POST_ENQUIRY_SUB", postValues);
          inMap.put("p_user_id", inputList.get(0));
          inMap.put("p_affiliate_id", inputList.get(1));          
          inMap.put("p_html_id", inputList.get(2));
          inMap.put("p_session_id", inputList.get(3));
          inMap.put("p_user_agent", inputList.get(4));
          inMap.put("p_user_ip", inputList.get(5));          
          inMap.put("p_request_url", inputList.get(6));
          inMap.put("p_referer_url", inputList.get(7));
          inMap.put("p_network_id", inputList.get(8));
          inMap.put("p_type", inputList.get(9));
          inMap.put("p_mobile_flag", inputList.get(10));
          inMap.put("p_attr_val_arr", values);
          inMap.put("p_track_session", inputList.get(11));          
          inMap.put("p_bulk_process", inputList.get(12));
          inMap.put("p_enq_basket_id", inputList.get(13));
					inMap.put("p_bulk_pros_new_user", inputList.get(14));//Added for new registration by Prabha on 27_JAN_2016_REL 
					inMap.put("p_screen_width", inputList.get(15)); // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
          outMap = execute(inMap);
        }catch(Exception e){
          e.printStackTrace();
        }finally{
          try{
            connection.close();
          }catch(Exception d){
            d.printStackTrace();
          }
        }
    return outMap;    
  }
}
