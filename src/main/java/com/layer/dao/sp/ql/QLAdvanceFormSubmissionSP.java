/*
 * Added By Sekhar for wu417_20101030 for QL.
 */
package com.layer.dao.sp.ql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.CommonFunction;

import com.layer.util.rowmapper.ql.QLAttributesRowMapperImpl;
import com.wuni.util.seo.SeoUrls;
import com.wuni.util.sql.OracleArray;
import com.wuni.util.valueobject.mywhatuni.MyprospectusPEVO;

public class QLAdvanceFormSubmissionSP extends StoredProcedure{
    
  DataSource ds = null;
  public QLAdvanceFormSubmissionSP(DataSource datasource){
    setDataSource(datasource);
    this.ds = datasource;
    setSql("HOT_WUNI.WU_INTERACTION_PKG.WU_QL_FORM_SUBMISSION_PRC");
    declareParameter(new SqlParameter("p_session_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_attr_val_arr", OracleTypes.ARRAY, "HOT_ADMIN.TB_QL_MAS_ATTR_VALUE"));
    declareParameter(new SqlInOutParameter("p_message", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_level_of_study", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_mobile_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_error_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("pc_attr_values", OracleTypes.CURSOR, new QLAttributesRowMapperImpl()));
    declareParameter(new SqlInOutParameter("p_enquiry_id", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_mywhat_pros_postenq", OracleTypes.CURSOR, new MyprospectusPostenquiryRowmapper()));
    declareParameter(new SqlParameter("p_track_session", Types.VARCHAR));
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", Types.VARCHAR)); //18_NOV_2014 Added by Amir for pixel tracking in email success page
  }
  
  public Map execute(List inputList, Object[][] attrValues){
    Map outMap = null;
    Connection connection = null;
    try{
      connection = ds.getConnection();
      ARRAY values = OracleArray.getOracleArray(connection, "HOT_ADMIN.TB_QL_MAS_ATTR_VALUE", attrValues);
      Map inMap = new HashMap();
      inMap.put("p_session_id", inputList.get(0));
      inMap.put("p_user_id", inputList.get(1));
      inMap.put("p_attr_val_arr", values);
      inMap.put("p_message", inputList.get(3));
      inMap.put("p_level_of_study", inputList.get(4));
      inMap.put("p_mobile_flag", inputList.get(5));
      inMap.put("p_enquiry_id", inputList.get(2));  
      inMap.put("p_track_session", inputList.get(6));  
      outMap = execute(inMap);
    }catch(Exception e){
      e.printStackTrace();
    }finally{
      try{
        connection.close();
      }catch(Exception d){
        d.printStackTrace();
      }
    }
    return outMap;
  }
  private class MyprospectusPostenquiryRowmapper implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
      MyprospectusPEVO prospevo = new MyprospectusPEVO();
      prospevo.setCollegeName(rs.getString("college_name"));
      prospevo.setCollegeDisplayName(rs.getString("college_name_display"));
      prospevo.setCollegeId(rs.getString("college_id"));
      prospevo.setEnquiryType(rs.getString("enquiry_type"));
      prospevo.setInteractionType(rs.getString("interaction_type"));
      prospevo.setCollegeLogo(rs.getString("college_logo"));
      prospevo.setDpFlag(rs.getString("download_prospectus"));
      prospevo.setEmailPrice(rs.getString("email_price"));
      prospevo.setUniHomeUrl(new SeoUrls().construnctUniHomeURL(prospevo.getCollegeId(), prospevo.getCollegeName()).toLowerCase());
      String tmpUrl = "";
      ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("ENQUIRY_DETAILS");
      try {
        if (enquiryDetailsRS != null) {
          while (enquiryDetailsRS.next()) {
            prospevo.setSubOrderItemId(enquiryDetailsRS.getString("SUBORDER_ITEM_ID"));
            prospevo.setSubOrderEmail(enquiryDetailsRS.getString("EMAIL"));
            prospevo.setSubOrderProspectus(enquiryDetailsRS.getString("PROSPECTUS"));
            prospevo.setMyhcProfileId(enquiryDetailsRS.getString("MYHC_PROFILE_ID"));
            prospevo.setProfileType(enquiryDetailsRS.getString("PROFILE_TYPE"));
            prospevo.setCpeQualificationNetworkId(enquiryDetailsRS.getString("NETWORK_ID"));
            tmpUrl = enquiryDetailsRS.getString("WEBSITE");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            prospevo.setSubOrderWebsite(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("EMAIL_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            prospevo.setSubOrderEmailWebform(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("PROSPECTUS_WEBFORM");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
                tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            prospevo.setSubOrderProspectusWebform(tmpUrl);
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (enquiryDetailsRS != null) {
            enquiryDetailsRS.close();
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      ResultSet dpDetailsRs = (ResultSet)rs.getObject("DP_DETAILS");
      try {
        if (dpDetailsRs != null) {
          while (dpDetailsRs.next()) {
            prospevo.setDpURL(dpDetailsRs.getString("dpurl"));
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (dpDetailsRs != null) {
            dpDetailsRs.close();
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      return prospevo;
    }
  }

}
