/*
 * Added By Sekhar for wu417_20101030 for QL.
 */
package com.layer.dao.sp.ql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import oracle.sql.STRUCT;
import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import WUI.utilities.CommonUtil;
import com.layer.util.rowmapper.advert.UniProfileInfoRowMapperImpl;
import com.layer.util.rowmapper.openday.OpenDayInfoRowMapperImpl;
import com.layer.util.rowmapper.review.UniOverallReviewRowMapperImpl;
import com.wuni.advertiser.ql.form.QLFormBean;
import com.wuni.advertiser.ql.form.QuestionAnswerBean;
import com.wuni.util.valueobject.ql.BasicFormVO;
import com.wuni.util.valueobject.ql.ClientLeadConsentVO;

public class QLBasicformSP extends StoredProcedure{
  
  public QLBasicformSP(DataSource datasource){
    setDataSource(datasource);
    setSql("HOT_WUNI.GET_ENQUIRY_DETAILS_PRC");
    declareParameter(new SqlParameter("P_USER_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COLLEGE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SUBORDER_ITEM_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_COURSE_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_HTML_ID", Types.VARCHAR));
    declareParameter(new SqlOutParameter("PC_USER_DETAILS", OracleTypes.CURSOR, new userInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_USER_ADDRESS", OracleTypes.CURSOR, new prospectusAddressRowMapper()));
    declareParameter(new SqlOutParameter("PC_COURSE_DETAILS", OracleTypes.CURSOR, new courseInfoPodRowMapperImpl()));
    declareParameter(new SqlOutParameter("P_QL_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_study_level_type", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_PROFILE_TYPE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("pc_nationality", OracleTypes.CURSOR, new nationalilyddRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_country_of_res", OracleTypes.CURSOR, new countryOfResidenceRowImpls()));
    declareParameter(new SqlOutParameter("o_uniprofile_info", OracleTypes.CURSOR, new UniProfileInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_open_day_info", OracleTypes.CURSOR, new OpenDayInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("o_overall_review", OracleTypes.CURSOR, new UniOverallReviewRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_ONE_CLIK_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_ONE_CHECK_BOX_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_PREDICTED_GRADES", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("pc_grade_details", OracleTypes.CURSOR, new GradeDetailsRowMapperImpl()));
    declareParameter(new SqlOutParameter("p_country_of_residence", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("oc_country_of_res", OracleTypes.CURSOR, new nationalilyddRowMapperImpl()));
    declareParameter(new SqlOutParameter("OC_GRADE_OPTION", OracleTypes.CURSOR, new GradeOptionRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_COURSE_INFO", OracleTypes.CURSOR, new CourseInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_COLLEGE_INFO", OracleTypes.CURSOR, new CollegeInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("O_NETWORK_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("pc_user_subscription_flags", OracleTypes.CURSOR, new UserSubscriptionFlagRowMapperImpl()));
    declareParameter(new SqlOutParameter("P_ENQUIRY_EXIST_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_CLIENT_CONSENT_DETAILS", OracleTypes.CURSOR, new ClientConsentDetailsRowMapperImpl()));//Added this for showing consent flag details By Hema.S on 17_12_2019_REL
    declareParameter(new SqlParameter("P_ENQ_BASKET_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("O_CLIENT_CONSENT_ARR", OracleTypes.CURSOR, new BulkProspectusClientConsentDetailsRowMapperImpl()));
    compile();
  }
  
  public Map execute(QLFormBean qlFormBean){
    Map outMap = null;
    try{
    Map inMap = new HashMap();
    inMap.put("P_USER_ID", qlFormBean.getUserId());
    inMap.put("P_COLLEGE_ID", qlFormBean.getCollegeId());
    inMap.put("P_SUBORDER_ITEM_ID", qlFormBean.getSubOrderItemId());
    inMap.put("P_COURSE_ID", qlFormBean.getCourseId());
    inMap.put("P_HTML_ID", qlFormBean.getHtmlId());
    inMap.put("P_ENQ_BASKET_ID",qlFormBean.getEnquiryBasketId());
    outMap = execute(inMap);
    }catch(Exception e)
    {
      e.printStackTrace();
    }
    return outMap;    
  }
  
  private class userInfoRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
      BasicFormVO basicFormVO = new BasicFormVO();
      basicFormVO.setEmailAddress(rs.getString("user_email"));
      basicFormVO.setFirstName(rs.getString("forename"));
      basicFormVO.setLastName(rs.getString("surname"));
      basicFormVO.setPostCode(rs.getString("postcode"));
      if (rs.getString("nationality") != null && rs.getString("postcode") == null || (rs.getString("postcode") != null && rs.getString("postcode").equals(""))) {
        basicFormVO.setNonukresident(true);
      }
      basicFormVO.setNationality(rs.getString("nationality"));
      basicFormVO.setNewsLetter(rs.getString("permit_email"));
      basicFormVO.setYeartoJoinCourse(rs.getString("year_of_course"));
      basicFormVO.setTown(rs.getString("city"));
      basicFormVO.setDateOfBirth(rs.getString("DOB"));
      basicFormVO.setMarketingEmail("");
      basicFormVO.setSolusEmail("");
      basicFormVO.setSurveyEmail("");
      basicFormVO.setDefaultYoe(rs.getString("CURRENT_YEAR_OF_ENTRY"));
      return basicFormVO;
    }
  }
  
  private class prospectusAddressRowMapper implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
      BasicFormVO basicFormVO1 = new BasicFormVO();
      basicFormVO1.setAddress1(rs.getString("address_line_1"));
      basicFormVO1.setAddress2(rs.getString("address_line_2")); 
      basicFormVO1.setTown(rs.getString("city"));
      basicFormVO1.setCountryOfResId(rs.getString("country_id"));
      return basicFormVO1;
    }
  }
  
  private class courseInfoPodRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
      BasicFormVO basicFormVO = new BasicFormVO();
      basicFormVO.setCourseId(rs.getString("course_id"));
      basicFormVO.setCourseName(rs.getString("course_title"));
      basicFormVO.setCourseDesc(rs.getString("course_summary"));
      basicFormVO.setQualification(rs.getString("learn_dir_qual"));
      basicFormVO.setUcasTariff(rs.getString("ucas_tariff"));      
      basicFormVO.setStudyMode(rs.getString("study_mode"));      
      basicFormVO.setTutionFees(rs.getString("dom_price"));
      basicFormVO.setTutionFeesDesc(rs.getString("dom_price_desc"));      
      basicFormVO.setStartDate(rs.getString("start_date"));      
      basicFormVO.setEntryRequirements(rs.getString("entry_requirements"));      
      basicFormVO.setDuration(rs.getString("duration"));
      basicFormVO.setDurationDesc(rs.getString("duration_desc"));
      basicFormVO.setStudyLevelCode(rs.getString("study_level_code"));
      return basicFormVO;
    }
  }
  
  private class countryOfResidenceRowImpls implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
      BasicFormVO basicForm = new BasicFormVO();
      basicForm.setNationalityValue(rs.getString("value"));
      basicForm.setNationalityDesc(rs.getString("description"));
      return basicForm;
    }
  }

  private class nationalilyddRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{
     java.sql.Array optionsRS = (java.sql.Array)rs.getObject("OPTIONS");// added for weblogic migration 28-JUN_2016_REL
     Object datanum[] = (Object[])optionsRS.getArray();
     ArrayList optionList = new ArrayList();
     for (int i = 0; i < datanum.length; i++) {
       STRUCT struct = (STRUCT)datanum[i];
       Object obj[] = struct.getAttributes();
       BasicFormVO basicForm = new BasicFormVO();
       //optionsVO.setOptionId(String.valueOf(obj[0]));
       basicForm.setNationalityDesc((String)obj[1]);
       basicForm.setNationalityValue(String.valueOf(obj[0]));
       optionList.add(basicForm);
     }
     return optionList;
   }
  }  
  
  private class GradeDetailsRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{   
      QuestionAnswerBean questionAnswerBean = new QuestionAnswerBean();
      questionAnswerBean.setAttributeId(rs.getString("attribute_id"));
      questionAnswerBean.setAttributeValue(rs.getString("attribute_value"));
      questionAnswerBean.setOptionId(rs.getString("option_id"));
      questionAnswerBean.setOptionDesc(rs.getString("option_desc"));
      return questionAnswerBean;
    }  
  } 
  private class GradeOptionRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{   
      QuestionAnswerBean questionAnswerBean = new QuestionAnswerBean();
      questionAnswerBean.setOptionId(rs.getString("option_id"));
      questionAnswerBean.setOptionDesc(rs.getString("option_desc"));
      return questionAnswerBean;
    }  
  }  
  
  private class CourseInfoRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{      
      QuestionAnswerBean questionAnswerBean = new QuestionAnswerBean();
      questionAnswerBean.setCourseTitle(rs.getString("course_title"));
      questionAnswerBean.setCollegeNameDisplay(rs.getString("college_name_display"));
      questionAnswerBean.setCollegeLogo(rs.getString("college_logo"));
      if (!GenericValidator.isBlankOrNull(questionAnswerBean.getCollegeLogo())) {
        questionAnswerBean.setCollegeLogPath(new CommonUtil().getImgPath((questionAnswerBean.getCollegeLogo()), rowNum));
      }
      questionAnswerBean.setEntryPoints(rs.getString("entry_points"));
      questionAnswerBean.setDescription(rs.getString("description"));
      questionAnswerBean.setDurationDesc(rs.getString("duration_desc"));
      questionAnswerBean.setShortText(rs.getString("short_text"));
      questionAnswerBean.setStartDate(rs.getString("start_date"));
      questionAnswerBean.setDomesticFees(rs.getString("domestic_fee"));
      questionAnswerBean.setOtherUKFees(rs.getString("other_uk_fee"));
      questionAnswerBean.setIntlFees(rs.getString("intl_fee"));
      questionAnswerBean.setEuFees(rs.getString("eu_fee"));
      questionAnswerBean.setEngFees(rs.getString("eng_fee"));
      questionAnswerBean.setScotFees(rs.getString("scot_fee")); 
      questionAnswerBean.setWalesFees(rs.getString("wales_fee"));
      questionAnswerBean.setNorthIreFees(rs.getString("north_ire_fee"));
//      if((!GenericValidator.isBlankOrNull(questionAnswerBean.getUkFees())) ||  (!GenericValidator.isBlankOrNull(questionAnswerBean.getIntlFees())) || (!GenericValidator.isBlankOrNull(questionAnswerBean.getEuFees())) || (!GenericValidator.isBlankOrNull(questionAnswerBean.getRegionalFees()))){
//        questionAnswerBean.setFeesExistsFlag("YES");
//      }      
      return questionAnswerBean;
    }  
  }
  private class CollegeInfoRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{       
      QuestionAnswerBean questionAnswerBean = new QuestionAnswerBean();      
      questionAnswerBean.setCollegeNameDisplay(rs.getString("college_name_display"));
      questionAnswerBean.setCollegeLogo(rs.getString("college_logo"));
      if (!GenericValidator.isBlankOrNull(questionAnswerBean.getCollegeLogo())) {
        questionAnswerBean.setCollegeLogPath(new CommonUtil().getImgPath((questionAnswerBean.getCollegeLogo()), rowNum));
      }
      questionAnswerBean.setOverallRatingExact(rs.getString("overall_rating_exact"));
      questionAnswerBean.setOverallRating(rs.getString("overall_rating"));
      questionAnswerBean.setTimesRanking(rs.getString("times_ranking"));
      questionAnswerBean.setPositionId(rs.getString("position_id"));
      questionAnswerBean.setTotalCnt(rs.getString("total_cnt"));      
      return questionAnswerBean;
    }  
  }
  
  private class UserSubscriptionFlagRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{       
      BasicFormVO basicFormVO = new BasicFormVO();      
      basicFormVO.setMarketingEmail(rs.getString("marketing_email_flag"));
      basicFormVO.setSolusEmail(rs.getString("solus_email_flag"));
      basicFormVO.setSurveyEmail(rs.getString("survey_flag"));
      return basicFormVO;
    }  
  }
  //Added this for showing consent flag details By Hema.S on 17_12_2019_REL
  private class ClientConsentDetailsRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{ 
    	ClientLeadConsentVO clientLeadConsentVO	 = new ClientLeadConsentVO();
    	clientLeadConsentVO.setConsentText(rs.getString("consent_text"));
    	clientLeadConsentVO.setClientMarketingConsent(rs.getString("client_marketing_consent"));
    	return clientLeadConsentVO;
	}
  }
  //Added this for showing consent flag details By Hema.S on 17_12_2019_REL
  private class BulkProspectusClientConsentDetailsRowMapperImpl implements RowMapper{
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException{ 
	  ClientLeadConsentVO clientLeadConsentVO	 = new ClientLeadConsentVO();
	  clientLeadConsentVO.setBasketId(rs.getString("basket_id"));
	  clientLeadConsentVO.setCollegeId(rs.getString("college_id"));
	  clientLeadConsentVO.setCollegeDisplayName(rs.getString("college_display_name"));
	  clientLeadConsentVO.setCollegeConsentText(rs.getString("college_consent_text"));
	  return clientLeadConsentVO;
    }
  }
  
}