package com.layer.dao.sp.ql;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.layer.util.rowmapper.ql.PostEnquriyRowMapperImpls;
import com.wuni.advertiser.ql.form.QLFormBean;


/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : OneclickEnquirySubmitSP.java
* Description   : One click enqiry submit SP
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	                 Ver 	           Modified On     	Modification Details 	   Change
* *************************************************************************************************************************************
* Sabapathi S.            wu_582          10_Oct_2018          Modified             Added Screen width for stats logging
* Sangeeth.S			  wu_31032020	  09_MAR_2020		   Modified				Added lat and long input param
* Kailash L                               21_JUL_2020          Modified				Added source order itemId for stats logging
* Sri Sankari		      wu_20200917     17_SEP_2020            1.4                Added stats log for tariff logging(UCAS point)
* Sujitha V               wu_20201110     10_NOV_2020          Modified             Added YOE value in d_session_log stats
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
public class OneclickEnquirySubmitSP extends StoredProcedure {
  public OneclickEnquirySubmitSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("whatuni_interactive_pkg.one_click_submit_prc");
    declareParameter(new SqlInOutParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_course_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_message", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_suborder_item_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_affiliate_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_html_id", OracleTypes.VARCHAR));
    declareParameter(new SqlInOutParameter("p_session_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_user_agent", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_ip", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_request_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_referer_url", Types.VARCHAR));
    declareParameter(new SqlParameter("p_network_id", Types.VARCHAR));
    declareParameter(new SqlParameter("p_type_of_prospectus", Types.VARCHAR));
    declareParameter(new SqlParameter("p_mobile_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_track_session", Types.VARCHAR));
    declareParameter(new SqlParameter("p_bulk_process", Types.VARCHAR));
    declareParameter(new SqlParameter("p_one_click_flag", OracleTypes.VARCHAR));  
    declareParameter(new SqlOutParameter("p_college_names", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_enquiry_id", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_post_enquiry", OracleTypes.CURSOR, new PostEnquriyRowMapperImpls()));
    declareParameter(new SqlOutParameter("o_user_sent", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", Types.VARCHAR));
    declareParameter(new SqlParameter("p_screen_width", OracleTypes.VARCHAR)); // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
    declareParameter(new SqlParameter("p_client_consent_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_widget_id", Types.VARCHAR));
    declareParameter(new SqlParameter("P_LATITUDE", Types.VARCHAR));// March2020 rel - Sangeeth.S: Added latitude and longitde for stats logging
    declareParameter(new SqlParameter("P_LONGITUDE", Types.VARCHAR));// March2020 rel - Sangeeth.S: Added latitude and longitde for stats logging
    declareParameter(new SqlParameter("P_SOURCE_ORDER_ITEM_ID", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_UCAS_TARIFF", OracleTypes.NUMBER));// wu_20200917 - Sri Sankari: Added UCAS tariff point
    declareParameter(new SqlParameter("P_YEAR_OF_ENTRY", OracleTypes.NUMBER));//Added this for YOE logging in d_session by Sujitha V on 2020_NOV_10
    compile();
  }
  public Map execute(QLFormBean qlFormBean) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_user_id", qlFormBean.getUserId());
    inMap.put("p_college_id", qlFormBean.getCollegeId());
    inMap.put("p_course_id", qlFormBean.getCourseId());
    inMap.put("p_message", qlFormBean.getMessage());
    inMap.put("p_suborder_item_id", qlFormBean.getSubOrderItemId());
    inMap.put("p_affiliate_id", qlFormBean.getAffliatedId());
    inMap.put("p_html_id", qlFormBean.getHtmlId());
    inMap.put("p_session_id", qlFormBean.getSessionId());
    inMap.put("p_user_agent", qlFormBean.getClientBrowser());
    inMap.put("p_user_ip", qlFormBean.getClientIp());
    inMap.put("p_request_url", qlFormBean.getRequestUrl());
    inMap.put("p_referer_url", qlFormBean.getRefferalUrl());    
    inMap.put("p_network_id", qlFormBean.getCpeQualificationNetworkId());
    inMap.put("p_type_of_prospectus", qlFormBean.getTypeOfProspectus());
    inMap.put("p_mobile_flag", qlFormBean.getMobileFlag());
    inMap.put("p_track_session", qlFormBean.getTrackSessionId());
    inMap.put("p_bulk_process", null);
    inMap.put("p_one_click_flag", qlFormBean.getOneClickCheckBox());
    inMap.put("p_screen_width", qlFormBean.getScreenWidth()); // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
    inMap.put("p_client_consent_flag", qlFormBean.getConsentFlag()); 
    inMap.put("p_widget_id", qlFormBean.getWidgetId());
    inMap.put("P_LATITUDE", qlFormBean.getLatitude());
    inMap.put("P_LONGITUDE", qlFormBean.getLongitude());
    inMap.put("P_SOURCE_ORDER_ITEM_ID", qlFormBean.getSponsoredOrderItemId());
    inMap.put("P_UCAS_TARIFF", qlFormBean.getUserUcasScore());
    inMap.put("P_YEAR_OF_ENTRY", qlFormBean.getYearOfEntry());
    outMap = execute(inMap);
    return outMap;
  }  
}
