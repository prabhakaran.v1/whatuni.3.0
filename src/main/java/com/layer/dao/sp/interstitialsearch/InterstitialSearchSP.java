package com.layer.dao.sp.interstitialsearch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.util.valueobject.interstitialsearch.AssessmentTypeVO;
import com.wuni.util.valueobject.interstitialsearch.GardeFilterVO;
import com.wuni.util.valueobject.interstitialsearch.InterstitialSearchVO;
import com.wuni.util.valueobject.interstitialsearch.LocationTypeVO;
import com.wuni.util.valueobject.interstitialsearch.QualificationVO;
import com.wuni.util.valueobject.interstitialsearch.RegionVO;
import com.wuni.util.valueobject.interstitialsearch.ReviewCategoryVO;
import com.wuni.util.valueobject.interstitialsearch.StudymodeVO;
import com.wuni.valueobjects.whatunigo.GradeSubjectVO;
import com.wuni.valueobjects.whatunigo.UserQualDetailsVO;


/**
  * @InterstitialSearchSP.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : SP for loading interstitial page
  */
public class InterstitialSearchSP extends StoredProcedure {

  public InterstitialSearchSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_INTERSTITIAL_SEARCH_PRC);
    //
    declareParameter(new SqlParameter("P_PAGE_NAME", Types.VARCHAR));
    declareParameter(new SqlInOutParameter("P_DEFAULT_QUAL_SELECTED", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_CLEARING_ON_OFF_FLAG", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_QUALIFICATION_LIST", OracleTypes.CURSOR, new QualificationListRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_REGION_LIST", OracleTypes.CURSOR, new regionListRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_REGION_UNDER_ENGLAND", OracleTypes.CURSOR, new regionListRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_LOCATION_TYPE_LIST", OracleTypes.CURSOR, new locationTypeListRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_STUDY_MODE_LIST", OracleTypes.CURSOR, new studymodeListRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_GRADE_FILTER", OracleTypes.CURSOR, new gradeFilterListRowMapperImpl()));
    //declareParameter(new SqlOutParameter("PC_CLEARING_GRADE_FILTER", OracleTypes.CURSOR, new gradeFilterListRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_ASSESSMENT_TYPE_LIST", OracleTypes.CURSOR, new assessmentTypeRowMapperImpl()));
    declareParameter(new SqlOutParameter("PC_REVIEW_CAT_LIST", OracleTypes.CURSOR, new reviewCatListRowMapperImpl()));
    //declareParameter(new SqlOutParameter("PC_USER_QUALIFICATIONS", OracleTypes.CURSOR, new UserQualInfoRowmapperImpl()));
    //declareParameter(new SqlParameter("P_USER_ID", OracleTypes.VARCHAR));
    //declareParameter(new SqlInOutParameter("P_USER_SUBJGRADES_SESSION_ID", OracleTypes.VARCHAR));
    compile();
  }

  public Map execute(InterstitialSearchVO interstitialSearchVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("P_PAGE_NAME", interstitialSearchVO.getPageName());
      inMap.put("P_DEFAULT_QUAL_SELECTED", interstitialSearchVO.getDefaultQualSelected());
      //inMap.put("P_USER_ID", interstitialSearchVO.getUserId());
      //inMap.put("P_USER_SUBJGRADES_SESSION_ID", interstitialSearchVO.getSubjectSessionId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }

  private class QualificationListRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      QualificationVO qualificationVO = new QualificationVO();
      qualificationVO.setQualDesc(rs.getString("QUAL_DESC"));
      qualificationVO.setQualCode(rs.getString("QUAL_CODE"));
      qualificationVO.setUrlText(rs.getString("URL_TEXT"));
      return qualificationVO;
    }

  }

  private class regionListRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      RegionVO regionVO = new RegionVO();
      regionVO.setRegionName(rs.getString("REGION_NAME"));
      regionVO.setUrlText(rs.getString("URL_TEXT"));
      return regionVO;
    }

  }

  private class locationTypeListRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      LocationTypeVO locationTypeVO = new LocationTypeVO();
      locationTypeVO.setOptionId(rs.getString("OPTION_ID"));
      locationTypeVO.setOptionDesc(rs.getString("OPTION_DESC"));
      locationTypeVO.setUrlText(rs.getString("URL_TEXT"));
      return locationTypeVO;
    }

  }

  private class studymodeListRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      StudymodeVO studymodeVO = new StudymodeVO();
      studymodeVO.setStudymodeDesc(rs.getString("STUDY_MODE_DESC"));
      studymodeVO.setUrlText(rs.getString("URL_TEXT"));
      return studymodeVO;
    }

  }

  private class gradeFilterListRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      GardeFilterVO gradeFilterVO = new GardeFilterVO();
      gradeFilterVO.setEntry_qual_id(rs.getString("qual_id"));
      gradeFilterVO.setEntry_qualification(rs.getString("qualification"));
      gradeFilterVO.setParent_qualification(rs.getString("parent_qualification")); 
      gradeFilterVO.setEntry_grade(rs.getString("grade_str"));
      gradeFilterVO.setEntry_old_grade(rs.getString("old_grade_str"));
      gradeFilterVO.setEntry_subject(rs.getString("no_of_subjects"));
      gradeFilterVO.setQual_level(rs.getString("qual_level"));
      return gradeFilterVO;
    }

  }

  private class assessmentTypeRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      AssessmentTypeVO assessmentTypeVO = new AssessmentTypeVO();
      assessmentTypeVO.setAssessmentTypeId(rs.getString("ASSESSMENT_TYPE_ID"));
      assessmentTypeVO.setAssessmentDisplayName(rs.getString("ASSESSMENT_TYPE_DISPLAY_NAME"));
      assessmentTypeVO.setUrlText(rs.getString("URL_TEXT"));
      return assessmentTypeVO;
    }

  }

  private class reviewCatListRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ReviewCategoryVO reviewCategoryVO = new ReviewCategoryVO();
      reviewCategoryVO.setReviewCategoryId(rs.getString("REVIEW_CATEGORY_ID"));
      reviewCategoryVO.setReviewCategoryDisplayName(rs.getString("REVIEW_CATEGORY_DISPLAY_NAME"));
      reviewCategoryVO.setUrlText(rs.getString("URL_TEXT"));
      return reviewCategoryVO;
    }

  }
  
  private class UserQualInfoRowmapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int num) {
	  UserQualDetailsVO userQualDetailsVO = new UserQualDetailsVO();
	  try {
	    userQualDetailsVO.setEntry_qual_level(rs.getString("qual_level"));
	    userQualDetailsVO.setEntry_qual_id(rs.getString("qual_id"));
	    userQualDetailsVO.setEntry_qual_desc(rs.getString("qualification"));
	    userQualDetailsVO.setGcse_grade_flag(rs.getString("grade_type"));
	    ResultSet userqualRS = (ResultSet)rs.getObject("subject_list");
	    try {
	      if(userqualRS != null){
	        GradeSubjectVO gradeSubjectVO = new GradeSubjectVO();
	        ArrayList qualOptionsList = new ArrayList();
	        gradeSubjectVO = null;
	        while (userqualRS.next()) {
	          gradeSubjectVO = new GradeSubjectVO();
	          gradeSubjectVO.setEntry_subject_id(userqualRS.getString("SUBJECT_ID"));
	          gradeSubjectVO.setEntry_subject(userqualRS.getString("SUBJECT_DESC"));
	          gradeSubjectVO.setEntry_grade(userqualRS.getString("GRADE"));
	          qualOptionsList.add(gradeSubjectVO);
	         }
	         userQualDetailsVO.setQual_subject_list(qualOptionsList);
	       }
	     }catch (Exception e) {
	       e.printStackTrace();
	     }finally {
	       try {
	         if (userqualRS != null) {
	           userqualRS.close();
	           userqualRS = null;
	         }
	       } catch (Exception e) {
	         throw new SQLException(e.getMessage());
	       }
	     }
	   } catch (Exception e) {
	     e.printStackTrace();
	   }
	   return userQualDetailsVO;
	}
  }
}
