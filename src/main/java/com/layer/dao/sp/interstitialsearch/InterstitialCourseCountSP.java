package com.layer.dao.sp.interstitialsearch;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;
import com.wuni.util.sql.OracleArray;
import java.sql.Connection;
import java.sql.SQLException;
import oracle.sql.ARRAY;
import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.util.valueobject.interstitialsearch.InterstitialCourseCountVO;

/**
  * @InterstitialCourseCountSP.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : SP for getting count for interstitial page
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 25_Sep_2018    Sabapathi          Added keyword as a input parameter
  * 20_Nov_2018    Sabapathi          Added additional parameters to avoid mobile app conflicts
  */
public class InterstitialCourseCountSP extends StoredProcedure {

  DataSource datasource = null;
  public InterstitialCourseCountSP(DataSource datasource) {
    setDataSource(datasource);
    this.datasource = datasource;
    setSql(GlobalConstants.GET_INTERSTITIAL_COURSE_COUNT_PRC);
    //
    declareParameter(new SqlParameter("P_PAGE_NAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_QUALIFICATION", Types.VARCHAR));
    declareParameter(new SqlParameter("P_SEARCH_KEYWORD", Types.VARCHAR));    
    declareParameter(new SqlParameter("P_SEARCH_CATEGORY", Types.VARCHAR));
    declareParameter(new SqlParameter("P_TOWN_CITY", Types.VARCHAR));
    declareParameter(new SqlParameter("P_UNIV_LOC_TYPE_NAME_STR", Types.VARCHAR));
    declareParameter(new SqlParameter("P_STUDY_MODE", Types.VARCHAR));
    declareParameter(new SqlParameter("P_ENTRY_LEVEL", Types.VARCHAR));
    declareParameter(new SqlParameter("P_ENTRY_POINTS", Types.VARCHAR));
    declareParameter(new SqlParameter("P_ASSESSMENT_TYPE", Types.VARCHAR));
    declareParameter(new SqlParameter("P_REVIEW_SUBJECT_STR", Types.VARCHAR));
    declareParameter(new SqlOutParameter("P_COURSE_COUNT", OracleTypes.VARCHAR));
    // Added CURSOR amd Input parameters as a output and input parameter to avoid conflicts
    declareParameter(new SqlOutParameter("P_FILTER_CRS_CNT", OracleTypes.ARRAY, "TB_CHATBOT_FILTERS_CNT"));
    declareParameter(new SqlParameter("P_JACS_CODE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_UCAS_EQUIV_TARIFF_POINTS", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("PT_QUAL_DETAIL_ARR", OracleTypes.ARRAY, "TB_USER_SUBJ_GRADE_DETAIL"));
    compile();
  }

  public Map execute(InterstitialCourseCountVO interstitialCourseCountVO) {
    Connection connection = null;
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      connection = datasource.getConnection();
      ARRAY qualDetailsArr = OracleArray.getOracleArray(connection, "TB_USER_SUBJ_GRADE_DETAIL", interstitialCourseCountVO.getQualDetailsArr());
      inMap.put("P_PAGE_NAME", interstitialCourseCountVO.getPageName());
      inMap.put("P_QUALIFICATION", interstitialCourseCountVO.getQualification());
      inMap.put("P_SEARCH_KEYWORD", interstitialCourseCountVO.getSearchKeyword());
      inMap.put("P_SEARCH_CATEGORY", interstitialCourseCountVO.getSearchCategory());
      inMap.put("P_TOWN_CITY", interstitialCourseCountVO.getTownCity());
      inMap.put("P_UNIV_LOC_TYPE_NAME_STR", interstitialCourseCountVO.getLocationTypeString());
      inMap.put("P_STUDY_MODE", interstitialCourseCountVO.getStudymode());
      inMap.put("P_ENTRY_LEVEL", interstitialCourseCountVO.getEntryLevel());
      inMap.put("P_ENTRY_POINTS", interstitialCourseCountVO.getEntryPoints());
      inMap.put("P_ASSESSMENT_TYPE", interstitialCourseCountVO.getAssessmentType());
      inMap.put("P_REVIEW_SUBJECT_STR", interstitialCourseCountVO.getReviewCategory());
      inMap.put("P_JACS_CODE", interstitialCourseCountVO.getJacsCode());
      inMap.put("PT_QUAL_DETAIL_ARR", qualDetailsArr);
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        if (connection != null) {
         connection.close();
        }
      } catch (SQLException closeException) {
        closeException.printStackTrace();
      }
    }
    return outMap;
  }

}
