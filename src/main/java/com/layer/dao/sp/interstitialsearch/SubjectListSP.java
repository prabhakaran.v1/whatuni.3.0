package com.layer.dao.sp.interstitialsearch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.util.valueobject.interstitialsearch.SubjectAjaxVO;
import com.wuni.util.valueobject.interstitialsearch.SubjectListVO;

/**
  * @SubjectListSP.java
  * @Version : initial draft
  * @since : 28-Aug-2018
  * @author : Sabapathi.S
  * @Purpose  : SP for subject ajax in interstitial page
  */
public class SubjectListSP extends StoredProcedure {

  public SubjectListSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_SUBJECT_AJAX_FN);
    //
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.CURSOR, new SubjectListRowMapperImpl()));
    declareParameter(new SqlParameter("P_PAGE_NAME", Types.VARCHAR));
    declareParameter(new SqlParameter("P_KEYWORD_TEXT", Types.VARCHAR));
    declareParameter(new SqlParameter("P_QUALIFICATION", Types.VARCHAR));   
    declareParameter(new SqlParameter("P_BROWSE_CAT_ID", Types.VARCHAR));
    declareParameter(new SqlParameter("P_L1_FLAG", Types.VARCHAR));    
    compile();
  }

  public Map execute(SubjectAjaxVO subjectAjaxVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("P_PAGE_NAME", subjectAjaxVO.getPageName());
      inMap.put("P_KEYWORD_TEXT", subjectAjaxVO.getKeywordText());
      inMap.put("P_QUALIFICATION", subjectAjaxVO.getQualification());
      inMap.put("P_BROWSE_CAT_ID", subjectAjaxVO.getCategoryId());
      inMap.put("P_L1_FLAG", subjectAjaxVO.getL1Flag());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }

  private class SubjectListRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SubjectListVO subjectListVO = new SubjectListVO();
      subjectListVO.setBrowseCategoryId(rs.getString("BROWSE_CAT_ID"));
      subjectListVO.setBrowseDescription(rs.getString("BROWSE_DESCRIPTION"));
      subjectListVO.setCategoryCode(rs.getString("CATEGORY_CODE"));
      subjectListVO.setOrderSequence(rs.getString("ORDER_SEQ"));
      subjectListVO.setUrlText(rs.getString("URL_TEXT"));
      subjectListVO.setL2Flag(rs.getString("L2_FLAG"));
      return subjectListVO;
    }

  }

}
