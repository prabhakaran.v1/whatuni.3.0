package com.layer.dao.sp;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class GetShtmlDataSP extends StoredProcedure{
  public GetShtmlDataSP(DataSource datasource) {
    setDataSource(datasource);    
    setSql("HOT_WUNI.WU_UTIL_PKG.GET_HTML_CONTENT_PRC");
    declareParameter(new SqlParameter("P_HTML_ID", OracleTypes.VARCHAR)); 
    declareParameter(new SqlOutParameter("P_HTML_CONTENT", OracleTypes.CLOB));
    compile();
  }
  //  
  public Map execute(String shtmlName) {
    Map outMap = null;
    try {
      Map inputMap = new HashMap();
      inputMap.put("P_HTML_ID", shtmlName);
      outMap = execute(inputMap);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return outMap;
  }
}