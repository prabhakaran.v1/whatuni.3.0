package com.layer.dao;

import java.util.Map;

import WUI.homepage.form.HomePageBean;

/**
 * IHomeDAO.
 *
 * @author:     Mohamed Syed
 * @version:    1.0
 * @since:      wu314_20110712
 *
 **/
public interface IHomeDAO {
  public Map getHomePageData();
  //
  public Map getNewHomePageData(HomePageBean homepagebean); //16_SEP_2014
  public Map getUserTimeLinePod(String userId); //16_SEP_2014
}
