package com.layer.dao;

import java.util.List;
import java.util.Map;

import spring.valueobject.advice.AdviceDetailInfoVO;
import spring.valueobject.advice.AdviceHomeInfoVO;

/**
 * contains business logic for Articles
 *
 * @since        wu333_20141209
 * @author       Thiyagu G
 *
 */
public interface IAdviceDAO {

   /**
    * will return article-details page info
    * URL_PATTERN: www.whatuni.com/degrees/advice.html
    *
    *
    * @since        wu333_20141209 - redesign
    * @author       Thiyagu G
    * @param inputList
    * @return
    *
    */
   public Map getAdviceHomeInfo(List inputList); //This will return ArticleGroupsHome all top to bottom details
  
    /**
     * will return article-details page info
     * URL_PATTERN: www.whatuni.com/degrees/advice.html
     *
     *
     * @since        wu333_20141209 - redesign
     * @author       Thiyagu G
     * @param inputList
     * @return
     *
     */
    public Map getPrimaryCategoryLandingInfo(List inputList); //This will return ArticleGroupsHome all top to bottom details
    
    public Map getSecondaryCategoryLandingInfo(List inputList); //This will return ArticleGroupsHome all top to bottom details
    
    public Map getAdviceSearchResults(List inputList);
    
    public Map getAdviceDetailInfo(List inputList);
    /**
     * will return trending/most popular advice info
     * 
     * @since wu546_20151103
     * @author Prabhakaran V.
     * @param inputList
     * @return Map as advice info
     */
    public Map getAdviceAjaxDetail(List inputList);
    
    public Map checkArticleAndPrimaryCategoryAvilable(AdviceDetailInfoVO adviceDetailInfoVO);
    //
    public Map getArticlesPod(AdviceHomeInfoVO adviceHomeInfoVO) throws Exception; //Get the articles pod on 16_May_2017, By Thiyagu G.

    public Map getArticleList(List inputList);
    
    public Map getCovidSnippetPod(AdviceHomeInfoVO adviceHomeInfoVO) throws Exception;
}
