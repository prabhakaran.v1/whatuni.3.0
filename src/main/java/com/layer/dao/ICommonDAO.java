package com.layer.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mobile.valueobject.ProfileVO;
import mobile.valueobject.SearchVO;
import spring.dao.util.valueobject.clearing.ClearingProfileVO;
import spring.gradefilter.bean.SRGradeFilterBean;
import spring.valueobject.articles.RssBlogArticleVO;
import WUI.basket.form.BasketBean;
import WUI.registration.bean.RegistrationBean;
import WUI.registration.bean.UserLoginBean;
import WUI.review.bean.ReviewListBean;
import WUI.search.form.CourseDetailsBean;
import WUI.search.form.SearchBean;
import WUI.whatunigo.bean.GradeFilterBean;

import com.wuni.advertiser.ql.form.QLFormBean;
import com.wuni.myfinalchoice.form.MyFinalChoiceBean;
import com.wuni.mywhatuni.MywhatuniInputBean;
import com.wuni.mywhatuni.form.MyCompareBean;
import com.wuni.scholarship.form.ScholarshipFormBean;
import com.wuni.ultimatesearch.SaveULInputbean;
import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;
import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;
import com.wuni.util.form.opendays.OpenDaysBean;
import com.wuni.util.form.studentawards.StudentAwardsBean;
import com.wuni.util.valueobject.AutoCompleteVO;
import com.wuni.util.valueobject.VersionChangesVO;
import com.wuni.util.valueobject.advert.NewUniLandingVO;
import com.wuni.util.valueobject.advert.ProspectusVO;
import com.wuni.util.valueobject.advert.RichProfileVO;
import com.wuni.util.valueobject.contenthub.ContentHubVO;
import com.wuni.util.valueobject.contenthub.OpendayVO;
import com.wuni.util.valueobject.coursesearch.CourseSearchVO;
import com.wuni.util.valueobject.interstitialsearch.SubjectAjaxVO;
import com.wuni.util.valueobject.mywhatuni.MyWhatuniInputVO;
import com.wuni.util.valueobject.openday.OpendaySearchVO;
import com.wuni.util.valueobject.openday.OpendaysVO;
import com.wuni.util.valueobject.review.ReviewListVO;
import com.wuni.util.valueobject.review.UserReviewsVO;
import com.wuni.util.valueobject.search.CourseDetailsVO;
import com.wuni.valueobjects.AboutUsPagesVO;
import com.wuni.valueobjects.FinalFiveChoiceVO;
import com.wuni.valueobjects.InsightsVO;
import com.wuni.valueobjects.itext.UserDownloadPdfBean;
import com.wuni.valueobjects.whatunigo.ApplyNowVO;

public interface ICommonDAO {

  public Map getContentHubDetails(ContentHubVO contentHubVO); // Added by Saba for Content hub profile 23_JAN_18

  public Map getRichProfileUniLandingData(RichProfileVO richProfileVO); // Added by Amir for Rich profile Landing page 13_JAN_15    
  
  public Map getNewUniLandingData(NewUniLandingVO newUniLandingVO); // Added by Amir for all profile Landing page 03_NOV_15
  
  public Map checkCourseExists(NewUniLandingVO newUniLandingVO);//Added by Indumathi May_31_2016 For Tab changes
  
  public Map getMyFinalChoiceData(MyFinalChoiceBean myFinalChoiceBean); // Added by Amir for MyFinalChoice 21_JUL_15
  
  public Map getFinalChoiceSuggestionList(MyFinalChoiceBean myFinalChoiceBean); // Added by Amir for MyFinalChoice 21_JUL_15
  
  public Map saveFinalChoiceData(MyFinalChoiceBean myFinalChoiceBean); // Added by Amir for MyFinalChoice 21_JUL_15     
   
  public Map getFinalChUsrActionPod(MyFinalChoiceBean myFinalChoiceBean); // Added by Amir for MyFinalChoice 21_JUL_15
  
  public Map confirmUsrActionPod(MyFinalChoiceBean myFinalChoiceBean); // Added by Amir for MyFinalChoice 21_JUL_15
  
   public Map swapFinalChoicePod(MyFinalChoiceBean myFinalChoiceBean); // Added by Amir for MyFinalChoice 21_JUL_15
  
  public Map editMyFinalChoicePod(MyFinalChoiceBean myFinalChoiceBean); // Added by Amir for MyFinalChoice 21_JUL_15       

  public Map getOpenDaysBrowse(List inputList); //To get the list of institution to display in the open days browse

  public Map getOpenDaysInfo(List inputList); //To get the information about the openday for the college  
     
  public Map saveReserveOpendayUniList(OpenDaysBean openDaysBean); // To store Reserve openday unis list 17_MAR_2015 By Amir

  public Map getProviderDeletedFlag(ReviewListBean reviewBean); //To check the provider is deleted or nor flag 16_FEB_2016_REL By Prabha
  
  public Map getDegreeCourseFlag(CourseDetailsBean courseDetailBean); //To check provider is deleted or not 16_FEB_2016_REL By Prabha

  public Map getUniVideoReviews(List inputList); //To get the college video review list to display in unilanmding Tab
    
  public Map getReviewResultList(ReviewListVO reviewListVO);

  public Map getMoreVideoList(List inputList); //Display all video review - view all video reviews

  public Map getCollegeProfileInfo(List inputList); //To Get the information for the college profile 

  public Map getSubjectProfileList(List inputList); //To get the list of subject that has been purchased by the college  

  public Map getSubjectUniList(List inputList); //To get the instition list which has bought the same subject profile 

  public Map getViewAllMembersList(List inputList); //To get the all the members list when user click the view all members link 

  public Map getReviewHomePageData(List inputList); //To get the data that are related to review home page  

  public Map getLocationBrowseList(List inputList); //To get the list of location grouped by root category to display in "Universities and colleges by location" page 

  public Map getLocationUniversityList(List inputList); //To get the list of university that matches the location

  public Map getJourneyHomeDataList(List inputList); //To get the qualificationlist that display in the journey home 

  public Map getJourneyCategoryDataList(List inputList); //To get the category list to display in the browse journey category home 

  public Map getJourneyLocationDataList(List inputList); //To get the location list to display in the browse journey location home 

  public Map getCollegeBrowseSubjectList(List inputList); //To get the all category name that prioduce the result for the college (SEO requirement)

  public Map getMoneyPageMainContent(List inputList); //This will return money-page's main details(headerId, midcontent, refineByContent, numberOfTotalRecordsForThatSearchSynario)

  public Map getBulkProspectusColleges(List inputList); //This will return colleges for selecting bulk Prospectus

  public Map getProviderResultPageMainContent(List inputList); //This will return provider-result-page's main details(headerId, midcontent, numberOfTotalRecordsForThatSearchSynario)

  public Map getBlogHomeInfo(List inputList); //This will return blog's home page details

  public Map getPostHomeInfo(List inputList); //This will return post's home page details 

  public Map getBrowseUgCoursesByLocation(List inputList); //This will return BrowseUgCoursesByLocation page's all top to bottom details  

  public Map getBrowseUgCoursesBySubject(List inputList); //This will return BrowseUgCoursesBySubject page's all top to bottom details
   
  public Map getClearingSubjectLandingPage(List inputList);//This will return data for clearing subject landing page .      

  public Map getQlBasicFormInfo(QLFormBean qlFormBean); //This will return data for ql basic form.
     
  public Map basicFormSubmissionDBCall(QLFormBean qlFormBean);

  public Map qlAdvanceFormSubmission(List inputList, Object[][] attrValues);

  public Map getMyWhatuniData(MywhatuniInputBean inputBean);

  public Map myWhatuniUserAttrValues(MyWhatuniInputVO inputBean, Object[][] attrValues);//Modified list to bean Indumathi Jan-27-1

  public Map PostenquirySubmission(List inputList, Object[][] postValues);

  public Map mywhatuniPostenquiry(List inputList, Object[][] mywhatuniPostValues);
  
  public RssBlogArticleVO rssArticleBlogXml(RssBlogArticleVO rssBlogArticleVO);
  
  public Map getProviderScholarshipList(ScholarshipFormBean bean);

  public Map getMegamenuValue();
  
  public Map getMegaMenuDegreesList(String newstudylevelid);

  public Map getMobileCoursesDetailsInfo(Map inputMap);    
  
  public Map getWhyStudyGuideData(SearchBean searchbean);//SEO_RELEASE_29_OCT_2013_REL
  
  public Map getSHTMLData(String shtmlName);//SEO_RELEASE_29_OCT_2013_REL
  
   public Map getPDFXMLData(SearchBean searchbean); //To get the XML content for generate PDF, 24_NOV_2015_REL by Prabha
  
	 public Map oneclickEnquirySubmit(QLFormBean qlformbean);
  
  public Map getCategorydetails(String categoryCode); //SEO_RELEASE_10_Dec_2013_REL

  public Map updatePassword(String userId, String password, String oldPassword);//21-Jan-2014_REL
  
   public Map getMyWUOpendayAjax(List inputList);  
   
   public Map getYearOfEntry(IWanttobeBean inputBean);//Added by Indumathi.S Feb-16-2016 IWantToBe redesign
   
   public Map sendAboutUsEmails(AboutUsPagesVO aboutUsPagesVO, String flag); //26_Aug_2014
  
   public Map getProviderResultsDetails(SearchVO inputBean);
   
   public Map getUserBasketDetails(String userId); 

   public Map saveBasket(BasketBean bean);
   
   public Map getBasketDetails(BasketBean bean);

   public Map getKeywordBrowseNodes(List inputList, String dbObject);

   public Map getKeywordBrowseNodesDesktop(List inputList);
   
   public Map checkIfL2Node(String categroyId, String studyLevelId);//13_May_2014_REL
   
    public Map saveUltimateSearchDetails(SaveULInputbean inputbean);

    public Map getWhatcanidoSearchResults(WhatCanIDoBean whatcanidoBean);
    
    public Map getWhatcanidoQualificationResults(WhatCanIDoBean whatcanidoBean);//Added by Indumathi.S 08-03-16 wcid redesign

    public Map getWhatucanidoEmploymentInfo(WhatCanIDoBean inputBean);
    
    public Map getWhatucanidoEmploymentInfoAjax(WhatCanIDoBean inputBean);

    public Map loadUniEmploymentInfo(WhatCanIDoBean inputBean);
    
    public Map getEmpRateSortPagination(WhatCanIDoBean inputBean);
    
    public Map getIWanttobeSearchResults(IWanttobeBean inputBean); 

    public Map getIWanttobeEmpInfo(IWanttobeBean inputBean);
    
    public Map getIWanttobeEmpInfoAjax(IWanttobeBean inputBean);

    public Map getIWanttobEmpSortPagincation(IWanttobeBean inputBean);

    public Map getIWanttobeLoadUniEmpInfo(IWanttobeBean inputBean);   
    public Map getUltimateSearchInputDetails(SaveULInputbean inpuBean);
    
    public Map logProfileTabStats(ProfileVO profileVO);//3_JUN_2014
    
     public Map updateUserTimeLinePod(String userId,String timeLineId); //16_SEP_2014 created by Amir
    
    public Map getKeyStatisticsCount(); //08_Oct_2014 added by Thiyagu G to get statistics count.
    //
    public Map getProspectusesResults(ProspectusVO prospectusVO);//28-OCT-2014_REL Added by Priyaa
    //
    public Map getAutoCompleteSubUniResults(List inputList);//28-OCT-2014_REL
    //
    public Map addProspectusBasketContent(BasketBean basketBean,  Object[][] attrValues);//28_OCT_2014 Added by Priyaa
    //
    public Map enqBasketSubmitPrc(QLFormBean qlFormBean);//28_OCT_2014 Added by Priyaa
    //
    public Map addRollMarketingPrc(ArrayList list);//13_JAN_2015 Added by Priyaa 
    //
    public Map prosPostEnquirySuggestion(List inputList);//3_FEB2015 For Dld prospectus post enquiry suggestion
    //
    public Map resetPasswordPrc(String userId, String password, String sessionUserId, String caTrackFlag);//wu_537 24-FEB-2015 Added by Karthi
    //
    public Map getOpenDaysLandingLocation(OpendaysVO opendaysVO); //To get the information about the openday location landing, 17_Mar_2015 By Thiyagu G
    //
    public Map getOpenDaysLandingDate(List inputList); //To get the information about the openday date landing, 17_Mar_2015 By Thiyagu G
    //
    public Map getOpenDaysWeekDateInfo(List inputList); //To get the information about the openday date landing, 17_Mar_2015 By Thiyagu G
    //
    public Map getOpendaysBrowseNodesDesktop(List inputList); //To get the information about the openday search, 17_Mar_2015 By Thiyagu G
    //
    public Map fetchOpenDaySearchResults(OpendaySearchVO opendaysvo); //17_MAR_2015 Opn days search results
    //
    public Map getWuscaRankListResult(StudentAwardsBean studAwdBean); //Added by Amir 22_Apr_15 release for New Student Choice Awards Result
    //
    public Map getAwardsYearList(StudentAwardsBean studAwdBean); //Added by Indumathi.S Mar-08-16
    //
    public Map updateUserImageDetails(String userId, String imagePath); //, 19_May_2015 By Thiyagu G
    //
    public Map getCompareBasketDetails(MyCompareBean mycompareBean);//19_MAY_2015 OpenDays MywhatuniComparison
    //
    public Map removeMyOpendays(OpendaysVO opendaysVO);//19_MAY_2015 OpenDays MywhatuniComparison
    //
    public Map getRecentCourseResults(List inputList); //To get the information about the openday date landing, 17_Mar_2015 By Thiyagu G
    //
    public Map sendSocialInfoEmail(UserLoginBean userLoginBean);//Added by Indumathi.S for social box Nov-24 Rel
    //
    public Map getPGUniversityNewUrl(String postgraduateUrl); //To get the PG university new subject to form the new URL pettern for 27_Jan_2016, By Thiyagu G.
    //
    public Map getYearOfEntryDetails(List inputList); //To get year of entry 16_Feb_2015 By Thiyagu G
    //
    public Map getUserScoreDetails(List inputList); //To get ucas points and score based on the subject, 16_Feb_2015 By Thiyagu G
    //
    public Map getTariffDetails(List inputList); //To get tariff tables, 16_Feb_2015 By Thiyagu G
    //
    public Map submitUcasSubjectDetails(List inputList); //Submit subject details, 16_Feb_2015 By Thiyagu G
    //
    public Map getUcasKeywordBrowseNodes(List inputList); //To get the browse nodes 16_Feb_2015 By Thiyagu G
    //
    public Map getYearOfEntryList(); //To get tariff tables, 16_Feb_2015 By Thiyagu G
    //
    public Map getUniSortInfo(WhatCanIDoBean inputBean); //Added by Prabha for what course i do widget 08-03-16
    //
    public Map getInsightData(InsightsVO insightsVO); //Added by Prabha for Insight data 29-03-16
    //
    public Map getInstitutionId(InsightsVO insightsVO); //Added to get Institution id 31_May_2016, by Thiayagu G    
    
    //Added for login method changed from datamodal to spring for 28_June_2016, By Thiyagu G. start
    public Map checkEmailBlocked(RegistrationBean registrationBean);
    //
    public Map checkUserAuthentication(RegistrationBean registrationBean);
    //
    public Map checkEmailExist(RegistrationBean registrationBean);
    //
    public Map getUserYearOfEntry(RegistrationBean registrationBean);
    //Added for login method changed from datamodal to spring for 28_June_2016, By Thiyagu G. end
	   public Map getUserName(RegistrationBean registrationBean); //To get the user name by Prabha on 28_Jun_2016
    //
    public Map getUserInformationLogin(RegistrationBean registrationBean); //To get the User login info by Prabha on 28_Jun_2016
    //
    public Map preLoginUpdateStatusCode(RegistrationBean registrationBean); //To check the user login status byPrabha on 28_jun_2016
    //
    public Map getWUSysVarValue(RegistrationBean registrationBean); //To get the sys var value by Prabha on 28_jun_2016
    //
    public Map getCourseSubject(RegistrationBean registrationBean); //To get the sys var value by Prabha on 28_jun_2016
    //
    public Map getBasketCount(RegistrationBean registrationBean);
    //
    public Map updateCaTrackFlag(MyWhatuniInputVO myWhatuniInputVO);
    //
    public Map getNewCollegeDetails(SearchVO inputVO); //To get the new college details By Thiyagu G on 24_jan_2017
    //
    public Map getCourseScemaInfo(CourseDetailsVO courseDetailsVO); //To get the course specific schema detail by Prabha on 24_jan_2017}
    //
    public Map getPopularSubjects(CourseSearchVO csVO); //To get the popular subjects list, 21_Mar_2016 By Thiyagu G
    //
    public Map swapFinalFiveChoice(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception; //Swap the final five choice in a basket by Prabha on 16_May_2017
    //
    public Map editConfirmFinalFive(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception; //Edit confirm final five choice in a basket by Prabha on 16_May_2017
    //
    public Map getSuggestedComparisonPod(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception; //Get the suggested pod content by Prabha on 16_May_2017
    //
    public Map saveUpdateFinalChoice(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception; //save or update the final choice content by Prabha on 16_May_2017
    //
    public Map updateBasketCourse(FinalFiveChoiceVO finalFiveChoiceVO) throws Exception; //update course content by Prabha on 16_May_2017
    //
    public Map getCourseName(CourseDetailsBean courseDetailsBean); //To get the course name By Thiyagu G on 16_May_2017
    //
    public Map appLandingPageSignUp(RegistrationBean registrationBean); //Signup for ALP by Prabha on 08_Aug_17
    public Map versionChanges(VersionChangesVO versionChangesVO);
    public Map getAdvanceProviderResultsInfo(SearchVO inputBean);
    public Map getOpendayPopupAjax(OpendayVO opendayVO);
    
    public Map getRecentSearchResultList(AutoCompleteVO autoCompleteVO);
	
    public Map getInterstitialSubjectList(SubjectAjaxVO subjectAjaxVO); //Added for Interstitial Search page by Saba for wu580_28082018

    public Map getUserProfilePdf(UserDownloadPdfBean userDownloadPdfBean);//Added for User Profile pdf download by Hema.S on 25_SEP_2018_REL
    
    public void updateChatBotDisable(String userId, Object[][] attrValues); // To update if user minimized the chatbot, 25_Sep_2018 By Sabapathi
    
	public Map getReviewBreakDownList(AutoCompleteVO autoCompleteVO);
    public Map getReviewLightbox(UserReviewsVO userReviewsVO); // To get the user review for displaying the light box, 18_Dec_2018 By Jeyalakshmi  
     public Map getSubjectReviews(AutoCompleteVO autoCompleteVO);
    
    public Map getReviewSubjectList(AutoCompleteVO autoCompleteVO);//To get the review page subject list
    public Map applyNowCourseDetails(ApplyNowVO applyNowVO); // To get course details for apply now journey 
    public Map getCourseList(AutoCompleteVO autoCompleteVO);
	public Map getGradeFilterPage(GradeFilterBean gradeFilterBean); // To get the details of grade filter page
    
    public Map getSubjectlist(String freeText, GradeFilterBean gradeFilterBean);  //To get Qual Subject Ajax in grade filter page
    
    public Map getSubjectUcasPoints(GradeFilterBean gradeFilterBean); //To get ucas points Ajax in grade filter page
    
    public Map validateQualGrade(GradeFilterBean gradeFilterBean); //for grade filter page validation
     
    public Map saveQualGradeFilter(GradeFilterBean gradeFilterBean); // for saving the qualification in grade filter page   
    
     public Map getMatchingCourse(GradeFilterBean gradeFilterBean); // for getting the matching course count in grade filter page
     
     public Map getDBName();
     
    public Map getUserNameDetail(RegistrationBean registrationBean);
	
	public Map getSubjectLandingRegionList() throws Exception;
	
	public Map getYearOfEntryDetails() throws Exception;
	
	public Map getCookieLatestUpdatedDate() throws Exception;  
	
	// SR new grade filter page Jeya 22-Oct-2019
	  public Map getSRGradeFilterPage(GradeFilterBean gradeFilterBean); //To get the details of the SR grade filter page
	  
	  public Map getSRSubjectlist(String freeText, GradeFilterBean gradeFilterBean);  //To get Qual Subject Ajax in SR grade filter page
	  
	  public Map getSRSubjectUcasPoints(GradeFilterBean gradeFilterBean);//To get ucas points ajax in SR grade filter page
	  
	  //public Map validateQualGrade(GradeFilterBean gradeFilterBean); //for SR grade filter page validation
	  
	  public Map saveSRQualGradeFilter(GradeFilterBean gradeFilterBean); // for saving the qualification in SR grade filter page
	  
	  public Map getSRMatchingCourse(GradeFilterBean gradeFilterBean); // for getting the matching course count in SR grade filter page
	 
	  public Map deleteUserRecord(GradeFilterBean gradeFilterBean); // for deleting the user record if already saved in db
	
	public Map getTopNavSubjectList(AutoCompleteVO autoCompleteVO);
	
	public Map<String, Object> getCovid19Data();
	
	public Map getClearingProfileDetails(ClearingProfileVO clearingProfileVO); // for clearing profile page
	
	public Map getNavigationUrl(String subOrderItemId, String CourseId, String ctaButtonName) throws Exception; //For getting the navigation url from DB for CTA buttons by Sujitha V on 2020_DEC_08

	public Map<String, Object> getSysvarValues() throws Exception; //Added by Sri Sankari for Sysvar calls on 19_Jan_2021 rel
	
	  public Map getQualificationList(SRGradeFilterBean srGradeFilterBean); // To get qual list for sr gradefilter
	  
	  public Map saveSRGradeFilterInfo(SRGradeFilterBean srGradeFilterBean); // To save sr gradefilter info
	  
	  public Map deleteSRGradeFilterInfo(SRGradeFilterBean srGradeFilterBean); // To save sr gradefilter info
}