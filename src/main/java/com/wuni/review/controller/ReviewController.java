package com.wuni.review.controller;

import WUI.review.bean.ReviewListBean;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUtilities;
import com.wuni.util.uni.CollegeUtilities;
import com.wuni.util.valueobject.CollegeNamesVO;
import com.wuni.util.valueobject.review.CommonPhrasesVO;
import com.wuni.util.valueobject.review.ReviewListVO;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import spring.util.GlobalMethods;

/**
  * @ReviewAction
  * @author Amirtharaj/Thiyagu G
  * @version 1.0
  * @since 3-FEB-2015
  * @purpose This program is used to display the review related information.
  * **************************************************************************************************************************************
  * author	                 Ver 	           Modified On     	Modification Details 	               Change
  * **************************************************************************************************************************************
  * Prabhakaran V.          1.1             28.01.2016       16_FEB_2016_REL                       To check the provider is deleted or not
  * Prabhakaran V.          1.2             15.02.2017       21_FEB_2017_REL                       Cursor name changes based on back end
  * Prabhakaran V.          1.3             20.02.2018       20_FEB_2018_REL                       Added collegeId/collegeName for smart pixel tagging
  * Sangeeth.S              1.4             29.11.2018       18_DEC_2018_REL                       Added common phrase list and removed unused code for review redesign page.
  * Sangeeth.S              1.5             23.01.2019       12_FEB_2019_REL  
  */
@Controller
public class ReviewController {
	
  @RequestMapping(value = {"/university-course-reviews","/university-course-reviews/search","/university-course-reviews/*/*", "/university-course-reviews/*/*/*/*/reviews"})
  public ModelAndView getUniversityCourseReviews(HttpServletRequest request, HttpServletResponse response) throws Exception
                                                                                                                                     {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    ServletContext context = request.getServletContext();
    CollegeNamesVO uniNames = null;
    ReviewListVO reviewListVO = new ReviewListVO();
    CommonFunction comFn = new CommonFunction();
    CommonUtil commonUtil = new CommonUtil();
    String collegeId = "0";    
    String subjectId = "";
    String subjectName = "";
    String keyword = "";
    String pageNo = "1";
    String recordCount = "10";
    String orderBy = "DATE_DESC"; //Changed order by default value from highest rated to date desc for 08_MAR_2016, By Thiyagu G.
    String forwardString = "reviewPageResults";
    String dbcollegeName = "";
    String dbcollegeNameDisplay = "";
    String reviewId = "";
    String reviewCount = "";
    String tempOrderBy = "";
    String totalReviewCountDisplay = "";
    String overAllRating = "";
    String collegeIdParam = request.getParameter("college-id");
    String studyLevel = "";
    //    
    
    HttpSession session = request.getSession();
    if (comFn.checkSessionExpired(request, response, session, "ANONYMOUS")) { // TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    try {
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
        String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    //
    new GlobalMethods().setCommonPodInformation(context, request, response);
    String userId = new SessionData().getData(request, "y");
    
    String basketId = "";    
    if ((userId != null && !userId.equals("0") && userId.trim().length() == 0) && (basketId != null && basketId.trim().length() == 0)) {
      userId = "0";
      basketId = "";
    }
    
    basketId = comFn.checkCookieStatus(request);
    if(GenericValidator.isBlankOrNull(basketId) && "0".equals(basketId)){
      basketId = null;
    }
    
    String urlArray[] = request.getRequestURI().split("/");
    String urlProviderName = "";
    String providerId = "";
    String providerName = "";
    
    for (int arrayIndex = 3; arrayIndex < urlArray.length; arrayIndex++) {
      if(arrayIndex == 3){
        urlProviderName = urlArray[3];
      }
      if (arrayIndex == 4) {
        collegeId = urlArray[4].replace(".html", "");
      }
    }
    
  //lower case of url for 12_FEB_19 rel by Sangeeth.S
    if (!GenericValidator.isBlankOrNull(collegeIdParam)) {
      collegeId = collegeIdParam;
    }
    //Added to restrict 301 redirection by Prabha
    if (!GenericValidator.isBlankOrNull(request.getParameter("study-level"))) {
    	studyLevel = request.getParameter("study-level");
    	request.setAttribute("reviewSearchStudyLevel", commonUtil.titleCase(studyLevel));
      }
    if (!GenericValidator.isBlankOrNull(request.getParameter("university-name"))) {
      urlProviderName = request.getParameter("university-name");
    }
    //
    if (!GenericValidator.isBlankOrNull(request.getParameter("subject"))) {
      subjectId = request.getParameter("subject");
      request.setAttribute("refSubjectId", subjectId);
    }
    if (!GenericValidator.isBlankOrNull(request.getParameter("keyword"))) {
      keyword = request.getParameter("keyword");
      keyword = commonUtil.titleCase(keyword);//Added by Sangeeth.S for titlecase of the keyword text for 12_FEB_2019_REL
      if(keyword.indexOf(GlobalConstants.HYPHEN) > -1){        
        keyword = keyword.replaceAll(GlobalConstants.HYPHEN, " ");  
      }
      request.setAttribute("reviewSearchKeyword", keyword.trim());
    }
    if (request.getParameter("reviewId") != null && !"".equals(request.getParameter("reviewId"))) {
      reviewId = GenericValidator.isBlankOrNull(request.getParameter("reviewId")) ? "" : request.getParameter("reviewId");
    }else{ 
		  reviewId = GenericValidator.isBlankOrNull(request.getParameter("reviewid")) ? "" : request.getParameter("reviewid"); //Added by Prabha on 27_JAN_2016
		}
		  // Send to 404 page if the provider has been deleted from the DB  - added for 
    if(!"0".equals(collegeId)){        
		    String providerDeletedStatus = comFn.checkProviderDeleted(collegeId);
		    if(providerDeletedStatus != null && !"YES".equalsIgnoreCase(providerDeletedStatus)){
		      request.setAttribute("reason_for_404", "--(1)-->  This provider has been deleted (" + String.valueOf(collegeId) + ")");
		      response.sendError(404, "404 error message");
		      return null;
		    }
    }
    //End of checking deleted provider code
    //Removed highest rated if condition and added date desc else if condition for 08_MAR_2016, By Thiyagu G.
    if (!GenericValidator.isBlankOrNull(request.getParameter("orderby"))) {
      tempOrderBy = request.getParameter("orderby");
      if ("high".equalsIgnoreCase(tempOrderBy)) {
        orderBy = "HIGHEST_RATED";
      }else if ("low".equalsIgnoreCase(tempOrderBy)) {
        orderBy = "LOWEST_RATED";
      } else if ("Dtelow".equalsIgnoreCase(tempOrderBy)) {
        orderBy = "DATE_ASC";
      } 
      request.setAttribute("orderBy", orderBy);
      request.setAttribute("tmpOrderBy", tempOrderBy);
    }
    if (request.getParameter("pageno") != null && !"".equals(request.getParameter("orderby"))) {
      pageNo = request.getParameter("pageno");
      request.setAttribute("page", pageNo);
    }
    if (!GenericValidator.isBlankOrNull(reviewId)) {
      request.setAttribute("reviewId", reviewId);
      orderBy = "";
    }
	
    String mypage = (!GenericValidator.isBlankOrNull(request.getParameter("mypage"))) ? request.getParameter("mypage") : "";    
    
    if (collegeId != null && !"0".equals(collegeId)) {
      uniNames = new CollegeUtilities().getCollegeNames(collegeId, request);
      if (uniNames != null) {
        dbcollegeName = uniNames.getCollegeName();
        dbcollegeNameDisplay = uniNames.getCollegeNameDisplay();
        request.setAttribute("collegeId", collegeId);
        request.setAttribute("providerUrl", comFn.replaceHypen(comFn.replaceSpecialCharacter(comFn.replaceURL(dbcollegeName))));
        request.setAttribute("collegeDispName", dbcollegeNameDisplay);
        /* Added college display name for smart pixel by Prabha on 20_02_2018 */
        if(!GenericValidator.isBlankOrNull(dbcollegeName)){
          request.setAttribute("cDimCollegeDisName", ("\""+dbcollegeName+"\""));
        }
        /* End od smart pixel code */
        providerId = uniNames.getCollegeId();
        providerName = commonUtil.replaceSpaceByHypen(comFn.replaceSpecialCharacter(dbcollegeName).trim().toLowerCase());
      }
      //Added 301 redirection for provider name change for 21-02-2017, By Thiyagu G.
      if(!GenericValidator.isBlankOrNull(urlProviderName) && !GenericValidator.isBlankOrNull(providerName) && !urlProviderName.equals(providerName)){
        comFn.providerURLredirection(providerName, providerId, "reviewlist", response);
        return null;
      }
    }
    reviewListVO.setCollegeId(collegeId);
    reviewListVO.setPageNo(pageNo);
    reviewListVO.setShowRecordCount(recordCount);
    reviewListVO.setOrderBy(orderBy);
    reviewListVO.setSubjectCode(subjectId);
    reviewListVO.setSearchText(keyword);
    reviewListVO.setUserId(userId);
    reviewListVO.setBasketId(basketId);
    reviewListVO.setReviewId(reviewId);
    reviewListVO.setStudyLevel(studyLevel);
    Map reviewSearchMap = commonBusiness.getReviewResultList(reviewListVO);
    if (reviewSearchMap != null) {
      List reviewSearchList = (List)reviewSearchMap.get("oc_uni_reviews");
      if (reviewSearchList != null && reviewSearchList.size() > 0) {
        request.setAttribute("reviewSearchList", reviewSearchList);
        ReviewListBean reviewResBean = (ReviewListBean)reviewSearchList.get(0);        
        reviewCount = reviewResBean.getReviewCount();
        totalReviewCountDisplay = reviewResBean.getTotalReviewCountDisplay();          
        request.setAttribute("totalRecordCount", reviewCount);
        request.setAttribute("totalReviewCountDisplay", totalReviewCountDisplay);
      }       
      
      //START :: common phrases section
      ArrayList commonPhrasesList = (ArrayList)reviewSearchMap.get("PC_COMMMON_PHRASES");
      if (!CommonUtil.isBlankOrNull(commonPhrasesList)) { 
        CommonPhrasesVO  commonPhrasesVO = (CommonPhrasesVO)commonPhrasesList.get(0);          
        if(!GenericValidator.isBlankOrNull(commonPhrasesVO.getCommonPhrases())){
          request.setAttribute("commonPhrasesList", commonPhrasesList);             
        }                
      }
      //END :: common phrases section      
      String readResultBreadCrumb = (String)reviewSearchMap.get("o_bread_crumb");        
      if (!GenericValidator.isBlankOrNull(readResultBreadCrumb)) {
        request.setAttribute("breadCrumb", readResultBreadCrumb);
      }
      subjectName = (String)reviewSearchMap.get("o_subject_name");
      if (!GenericValidator.isBlankOrNull(subjectName)) {
        request.setAttribute("subjectDisplayName", subjectName);
      }
      String pixelTracking = (String)reviewSearchMap.get("o_pixel_tracking_code");
      if (!GenericValidator.isBlankOrNull(pixelTracking)) {
        request.setAttribute("pixelTracking", pixelTracking);
      }
      /************Loads Provider Result page/Search Result content*********************/
      if (!"0".equals(collegeId) && "".equals(subjectId) && "".equals(keyword) && "".equals(studyLevel)) {
        String reviewCnt = "0";
        String reviewRating = "0";
        List uniInfoList = (List)reviewSearchMap.get("oc_uni_info");
        if (uniInfoList != null && uniInfoList.size() > 0) {
          request.setAttribute("uniInfoList", uniInfoList);
          Iterator uniInfoItr = uniInfoList.iterator();
          while (uniInfoItr.hasNext()) {
            ReviewListBean reviewResBean = (ReviewListBean)uniInfoItr.next();
            reviewCnt = reviewResBean.getReviewCount();
            reviewRating = reviewResBean.getOverallRatingExact();
            overAllRating = reviewResBean.getOverAllRating();
            break;
          }
          request.setAttribute("overAllRating", overAllRating);
          request.setAttribute("reviewRating", reviewRating);
          request.setAttribute("reviewCnt", reviewCnt);
          
        }          
                 
        List uniCategoryList = (List)reviewSearchMap.get("oc_rated_uni_category");
        if (uniCategoryList != null && uniCategoryList.size() > 0) {
          request.setAttribute("uniCategoryList", uniCategoryList);
        }        
        request.setAttribute("cid", collegeId);
        request.setAttribute("interactionCollegeId", collegeId);
        request.setAttribute("interactionCollegeName", dbcollegeName);
        request.setAttribute("interactionCollegeNameDisplay", dbcollegeNameDisplay);
        request.setAttribute("hitbox_college_name", dbcollegeName);
        request.setAttribute("cDimCollegeId", collegeId);
        if(!GenericValidator.isBlankOrNull(collegeId)){
          request.setAttribute("cDimCollegeIds", ("'" + collegeId.trim() + "'")); //Added collegeId for smart pixel tagging by Prabha on 20.02.2018
        }
        request.setAttribute("COLLEGEID_IN_INTERACTION", collegeId);
        request.setAttribute("provSrchesult", "true");        
      }else if("0".equals(collegeId) && "".equals(subjectId) && "".equals(keyword) && "".equals(studyLevel)){
        request.setAttribute("reviewHomeResults", "true");        
      }else {
        request.setAttribute("searchResult", "true");        
      }
      if (!"".equals(keyword)) {
        request.setAttribute("reviewSrchType", "keyword");
      }
      if ((!"".equals(subjectId) && !"".equals(keyword)) || !"".equals(subjectId)) {
        request.setAttribute("reviewSrchType", "subject");
      }
      if ((!"0".equals(collegeId) || (!"0".equals(collegeId) && !"".equals(keyword)))) {
        request.setAttribute("reviewSrchType", "provider");
        request.setAttribute("reviewInsType", "provider");
        request.setAttribute("cDimCollegeId", collegeId);
        request.setAttribute("cDimCollegeIds", ("'" + collegeId.trim() + "'")); //Added collegeId for smart pixel tagging by Prabha on 20.02.2018
      }
      if (!"0".equals(collegeId) && !"".equals(subjectId)) {
        request.setAttribute("reviewSrchType", "keyword");
        request.setAttribute("reviewInsType", "provider");
      }
      if (!"".equals(subjectId) && !"".equals(keyword) && !"0".equals(collegeId)) {
        request.setAttribute("reviewSrchType", "keyword");
      }
      request.setAttribute("dimCategoryCode", GenericValidator.isBlankOrNull(subjectId)? "": subjectId.toUpperCase());
      request.setAttribute("qualification", "M");
    }else{
      request.setAttribute("reason_for_404", "--(5)--> Redirect to 404 page for empty results");
      response.sendError(404, "404 error message");
      return null;
    }
    String getCurrentURL = getFullURL(request);     
    if (!GenericValidator.isBlankOrNull(getCurrentURL)) {
      if (getCurrentURL.indexOf("&pageno") != -1) {
        getCurrentURL = getCurrentURL.replace("&pageno=" + pageNo, "");
      }
      if (getCurrentURL.indexOf("?pageno") != -1) {
        getCurrentURL = getCurrentURL.replace("?pageno=" + pageNo, "");
      }
      if(GenericValidator.isBlankOrNull(mypage)){
        if (getCurrentURL.indexOf("&orderby") != -1) {
          getCurrentURL = getCurrentURL.replace("&orderby=" + tempOrderBy, "");
        }
        if (getCurrentURL.indexOf("?orderby") != -1) {
          getCurrentURL = getCurrentURL.replace("?orderby=" + tempOrderBy, "");
        }
        if (getCurrentURL.indexOf("?reviewId=") != -1) {
          getCurrentURL = getCurrentURL.replace("?reviewId=" + reviewId, "");
        }
      }else if(!GenericValidator.isBlankOrNull(mypage) && "my_reviews".equals(mypage)){
        if (getCurrentURL.indexOf("&reviewid=") != -1) {
          getCurrentURL = getCurrentURL.replace("&reviewid=" + reviewId, "");
        }
      }
      if (getCurrentURL.indexOf("/degrees") != -1) {
        getCurrentURL = getCurrentURL.replace("/degrees", "");
      }        
      if (getCurrentURL.indexOf(".html") != -1) {
        getCurrentURL = getCurrentURL.replace(".html", "/");
      }
      request.setAttribute("reviewCurUrl", getCurrentURL);
      
      String parentPath = "/university-course-reviews/";
      URL currentUrl = new URL(getCurrentURL);
      URL url;
      if(!"0".equals(collegeId)) {
        parentPath += urlProviderName + "/" + collegeId;
        url = new URL( currentUrl.getProtocol(), currentUrl.getHost(), currentUrl.getPort(), parentPath);
      }else {
        url = new URL( currentUrl.getProtocol(), currentUrl.getHost(), currentUrl.getPort(), parentPath);
      }
      String canonicalUrl = url.toString();
      request.setAttribute("reviewCanonicalUrl", canonicalUrl);
      //Added breadCrumb Indumathi.S Feb-16-2016
      String  breadCrumb = new SeoUtilities().getBreadCrumb("UNI_REVIEWS_PAGE", "", "", "", "", collegeId);
      if (breadCrumb != null && breadCrumb.trim().length() > 0) {
        request.setAttribute("breadCrumb", breadCrumb);
      }
      //
    }
    request.setAttribute("studyLevelDesc", StringUtils.isNotBlank(studyLevel) ? studyLevel : "all");
    request.setAttribute("curActiveMenu", "reviews");
    return new ModelAndView(forwardString);
  }
  private String getFullURL(HttpServletRequest request) {
    String url = CommonUtil.getRequestedURL(request); //Change the getRequestURL by Prabha on 28_Jun_2016
    if (request.getQueryString() != null) {
      url += '?';
      url += request.getQueryString();
    }
    return url.toString();
  }
}
