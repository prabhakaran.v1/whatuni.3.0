package com.wuni.review.controller;

import WUI.utilities.CommonUtil;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;
import com.wuni.util.valueobject.review.UserReviewsVO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
   * PAGE NAME:  Profile page light box using ajax
   *
   * @since        19-DEC-2018 - review redesign
   * @author       Jeyalakhmi D
   *
   * URL_PATTERN: www.whatuni.com/[STUDY_LEVEL_DESC]-courses/csearch
   * URL_EXAMPLE: /ajax/reviewlightbox
   *
   * Modification history:
   * **************************************************************************************************************
   * Author           Relase Date              Modification Details                                        Rel Ver.
   * **************************************************************************************************************
   * Jeyalakshmi      19.12.2018               Added this class for review light box                       wu_584
   *                                              
   */ 
@Controller
public class ReviewAjaxController {
	
  @RequestMapping(value = "/ajax/reviewlightbox", method = RequestMethod.POST)
  public ModelAndView reviewAjax(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ServletException, IOException {
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    UserReviewsVO userReviewsVO = new UserReviewsVO();
    CommonUtil comUtil = new CommonUtil();
    String reviewId = request.getParameter("reviewId");
    userReviewsVO.setReviewId(reviewId);
    Map resultMap = commonBusiness.getReviewLightbox(userReviewsVO);
    if(resultMap != null && !resultMap.isEmpty()) {
      ArrayList listOfUserReviews = (ArrayList)resultMap.get("REVIEW_LIGHT_BOX");//db call for light box
      if(listOfUserReviews != null && listOfUserReviews.size() > 0) {
         request.setAttribute("listOfUserReviews", listOfUserReviews);
         String userNameColorClassName = "rev_grey";          
         for (int i = 0; i < listOfUserReviews.size(); i++) {
           userReviewsVO = (UserReviewsVO)listOfUserReviews.get(i);
           //getting the user name color class name 
           if(!GenericValidator.isBlankOrNull(userReviewsVO.getUserNameInitial()) ||!GenericValidator.isBlankOrNull(userReviewsVO.getUserNameInitialColourcode()) ){   
             userNameColorClassName = comUtil.getReviewUserNameColorCode(userReviewsVO.getUserNameInitial());
           }
           
           userReviewsVO.setUserNameInitialColourcode(userNameColorClassName);
         }
       }
    }
    return new ModelAndView("reviewlightbox");
  }
}
