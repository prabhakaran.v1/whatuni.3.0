package com.wuni.review.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.util.seo.SeoUrls;
import com.wuni.util.valueobject.review.UserReviewsVO;

public class GetReviewLightBoxSP extends StoredProcedure {
  public GetReviewLightBoxSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_VIEW_REVIEW_FN);
    declareParameter(new SqlOutParameter("REVIEW_LIGHT_BOX" , OracleTypes.CURSOR, new ProfileUserReviewsRowMapperImpl()));
    declareParameter(new SqlParameter("P_REVIEW_ID", Types.VARCHAR));    
  }
  public Map execute(UserReviewsVO userReviewsVO) {
    Map outMap = null;
    Map inMap = new HashMap();
    try { 
      inMap.put("P_REVIEW_ID", userReviewsVO.getReviewId());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }    
    return outMap;
  }

  private class ProfileUserReviewsRowMapperImpl implements RowMapper {
  SeoUrls seoUrl = new SeoUrls();
    public Object mapRow(ResultSet resultset, int rowNum) throws SQLException {
      UserReviewsVO userReviewsVO = new UserReviewsVO();
      userReviewsVO.setCollegeId(resultset.getString("COLLEGE_ID"));
      userReviewsVO.setReviewId(resultset.getString("REVIEW_ID"));
      userReviewsVO.setUserId(resultset.getString("USER_ID"));
      userReviewsVO.setCollegeName(resultset.getString("COLLEGE_NAME"));
      userReviewsVO.setCollegeNameDisplay(resultset.getString("COLLEGE_NAME_DISPLAY"));
      userReviewsVO.setCourseName(resultset.getString("COURSE_NAME"));    
      userReviewsVO.setCourseDeletedFlag(resultset.getString("COURSE_DELETED_FLAG"));
      userReviewsVO.setCourseId(resultset.getString("COURSE_ID"));
      userReviewsVO.setUserName(resultset.getString("USER_NAME"));
      userReviewsVO.setUserNameInitial(resultset.getString("USER_NAME_INT"));
      userReviewsVO.setCreatedDate(resultset.getString("REVIEW_DATE"));
      if(!GenericValidator.isBlankOrNull(userReviewsVO.getCollegeId())) {
        userReviewsVO.setUniHomeURL(seoUrl.construnctUniHomeURL(userReviewsVO.getCollegeId(), userReviewsVO.getCollegeName()));
      }
      if((!GenericValidator.isBlankOrNull(userReviewsVO.getCourseId()) || !"0".equalsIgnoreCase(userReviewsVO.getCourseId()))){
        userReviewsVO.setCourseDetailsURL(seoUrl.courseDetailsPageURL(userReviewsVO.getCourseName(), userReviewsVO.getCourseId(), userReviewsVO.getCollegeId()));
      }
      ResultSet userReviewsRS = (ResultSet) resultset.getObject("VIEW_MORE");
      try{
        if(userReviewsRS != null){
          ArrayList userMoreReviewaList = new ArrayList();
          UserReviewsVO userInnerReviewsVO = null;
          while(userReviewsRS.next()){
            userInnerReviewsVO = new UserReviewsVO();
            userInnerReviewsVO.setReviewQuestionId(userReviewsRS.getString("REVIEW_QUESTION_ID"));
            userInnerReviewsVO.setQuestionTitle(userReviewsRS.getString("QUESTION_TITLE"));
            userInnerReviewsVO.setQuestionDesc(userReviewsRS.getString("QUESTION_DESC"));
            userInnerReviewsVO.setAnswer(userReviewsRS.getString("ANSWER"));                 
            userInnerReviewsVO.setRating(userReviewsRS.getString("RATING"));
            userMoreReviewaList.add(userInnerReviewsVO);
          }
          userReviewsVO.setUserMoreReviewaList(userMoreReviewaList);
        }
      }catch (Exception e) {
        e.printStackTrace();
      }finally {
        try {
          userReviewsRS.close();
        }catch (Exception e) {
           throw new SQLException(e.getMessage());
        }
      }
      return userReviewsVO;
    }
  }
}

    