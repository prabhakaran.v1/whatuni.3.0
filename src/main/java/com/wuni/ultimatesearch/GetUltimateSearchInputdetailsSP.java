package com.wuni.ultimatesearch;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This SP is used to get the input details and user details given by User for both Ultimate Searches(Whatcanido & IWanttobe).
 */

public class GetUltimateSearchInputdetailsSP extends StoredProcedure

{

  public GetUltimateSearchInputdetailsSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql("wu_ultimate_srch_entry_det_pkg.get_input_details");
    declareParameter(new SqlParameter("p_ultimate_search_log_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_ultimate_search_type", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("pc_input_details", OracleTypes.CURSOR, new UltimateSearchInputDetailsRowmapper()));
    declareParameter(new SqlOutParameter("pc_user_details", OracleTypes.CURSOR, new UserDetailsRowmapper()));
    declareParameter(new SqlOutParameter("pc_whatcanido_popular_ex", OracleTypes.CURSOR, new PopularExamplesWhatcanidoRowmapper()));
    declareParameter(new SqlOutParameter("pc_iwanttobe_popular_ex", OracleTypes.CURSOR, new PopularExamplesIWanttobeRowmapper()));
  }
  
  public Map execute(SaveULInputbean inputbean)
  {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_ultimate_search_log_id", inputbean.getSaveLogId());
    inMap.put("p_user_id", inputbean.getUserId());
    inMap.put("p_ultimate_search_type", inputbean.getUlSearchType());
    outMap = execute(inMap);
    return outMap;
  }
  
  private class UserDetailsRowmapper implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SaveULInputbean userDetailsVO = new SaveULInputbean();
      userDetailsVO.setIntendedYear(rs.getString("year_of_entry"));
      userDetailsVO.setSchoolId(rs.getString("school_id"));
      userDetailsVO.setSchoolName(rs.getString("school_name"));
      userDetailsVO.setSchoolCheckFlag(rs.getString("non_uk_flag"));
      return userDetailsVO;
    } 
  }
  
  private class PopularExamplesWhatcanidoRowmapper implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      SaveULInputbean whatcanidoPopularExamplesVO = new SaveULInputbean();
      whatcanidoPopularExamplesVO.setQualSub1(rs.getString("qual_subject1_id"));
      whatcanidoPopularExamplesVO.setQualSubDesc1(rs.getString("qual_subject1_name"));
      whatcanidoPopularExamplesVO.setQualSub2(rs.getString("qual_subject2_id"));
      whatcanidoPopularExamplesVO.setQualSubDesc2(rs.getString("qual_subject2_name"));
      whatcanidoPopularExamplesVO.setQualSub3(rs.getString("qual_subject3_id"));
      whatcanidoPopularExamplesVO.setQualSubDesc3(rs.getString("qual_subject3_name"));
      whatcanidoPopularExamplesVO.setJacsSubjectCode1(rs.getString("jacs_subject1_code"));
      whatcanidoPopularExamplesVO.setJacsSubjectName1(rs.getString("jacs_subject1_name"));
      whatcanidoPopularExamplesVO.setJacsSubjectCode2(rs.getString("jacs_subject2_code"));
      whatcanidoPopularExamplesVO.setJacsSubjectName2(rs.getString("jacs_subject2_name"));
      whatcanidoPopularExamplesVO.setJacsSubjectCode3(rs.getString("jacs_subject3_code"));
      whatcanidoPopularExamplesVO.setJacsSubjectName3(rs.getString("jacs_subject3_name"));
      return whatcanidoPopularExamplesVO;
    }
  }
  
  private class PopularExamplesIWanttobeRowmapper implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      SaveULInputbean iwanttobePopularExamplesVO = new SaveULInputbean();
      iwanttobePopularExamplesVO.setJobCode(rs.getString("job_code"));
      iwanttobePopularExamplesVO.setJobName(rs.getString("job_name"));
      iwanttobePopularExamplesVO.setJacsSubjectCode1(rs.getString("jacs_subject1_code"));
      iwanttobePopularExamplesVO.setJacsSubjectName1(rs.getString("jacs_subject1_name"));
      iwanttobePopularExamplesVO.setJacsSubjectPercent1(rs.getString("jacs_subject1_percent"));
      iwanttobePopularExamplesVO.setJacsSubjectCode2(rs.getString("jacs_subject2_code"));
      iwanttobePopularExamplesVO.setJacsSubjectName2(rs.getString("jacs_subject2_name"));
      iwanttobePopularExamplesVO.setJacsSubjectPercent2(rs.getString("jacs_subject2_percent"));
      iwanttobePopularExamplesVO.setJacsSubjectCode3(rs.getString("jacs_subject3_code"));
      iwanttobePopularExamplesVO.setJacsSubjectName3(rs.getString("jacs_subject3_name"));
      iwanttobePopularExamplesVO.setJacsSubjectPercent3(rs.getString("jacs_subject3_percent"));
      return iwanttobePopularExamplesVO;
    }
  }
}
