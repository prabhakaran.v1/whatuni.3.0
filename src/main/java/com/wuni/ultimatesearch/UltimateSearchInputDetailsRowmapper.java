package com.wuni.ultimatesearch;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

 /**
    * @UltimateSearchInputDetailsRowmapper  
    * @version 1.0
    * @author Thiyagu G
    * @since 21-July-2015
    * @purpose  This rowmapper is used to read the results for given input of Whatcanido and Coursesearch.
    * Change Log
    * ***************************************************************************************************************************************************
    * Date           Name          Ver.       Changes desc                                                                  Rel Ver.
    * ***************************************************************************************************************************************************
    * 
    */

public class UltimateSearchInputDetailsRowmapper implements RowMapper {
  public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
    SaveULInputbean inputDetailsVO = new SaveULInputbean();
    inputDetailsVO.setQualification(rs.getString("qual_type_id"));
    inputDetailsVO.setQualSub1(rs.getString("subject1_id"));
    inputDetailsVO.setQualSub2(rs.getString("subject2_id"));
    inputDetailsVO.setQualSub3(rs.getString("subject3_id"));
    inputDetailsVO.setQualSub4(rs.getString("subject4_id"));
    inputDetailsVO.setQualSub5(rs.getString("subject5_id"));
    inputDetailsVO.setQualSub6(rs.getString("subject6_id"));
    inputDetailsVO.setQualSubDesc1(rs.getString("subject1_desc"));
    inputDetailsVO.setQualSubDesc2(rs.getString("subject2_desc"));
    inputDetailsVO.setQualSubDesc3(rs.getString("subject3_desc"));
    inputDetailsVO.setQualSubDesc4(rs.getString("subject4_desc"));
    inputDetailsVO.setQualSubDesc5(rs.getString("subject5_desc"));
    inputDetailsVO.setQualSubDesc6(rs.getString("subject6_desc"));
    inputDetailsVO.setQualGrades1(rs.getString("subj1_grade_or_points"));
    inputDetailsVO.setQualGrades2(rs.getString("subj2_grade_or_points"));
    inputDetailsVO.setQualGrades3(rs.getString("subj3_grade_or_points"));
    inputDetailsVO.setQualGrades4(rs.getString("subj4_grade_or_points"));
    inputDetailsVO.setQualGrades5(rs.getString("subj5_grade_or_points"));
    inputDetailsVO.setQualGrades6(rs.getString("subj6_grade_or_points"));
    inputDetailsVO.setUserId(rs.getString("user_id"));
    inputDetailsVO.setJobSocCode(rs.getString("job_soc_id"));
    inputDetailsVO.setJobDesc(rs.getString("job_desc"));
    inputDetailsVO.setIndustrySicCode(rs.getString("industry_sic_id"));
    inputDetailsVO.setIndustryDesc(rs.getString("industry_desc"));
    inputDetailsVO.setSchoolId(rs.getString("school_id"));
    inputDetailsVO.setSchoolName(rs.getString("school_name"));
    inputDetailsVO.setIntendedYear(rs.getString("year_of_entry"));
    inputDetailsVO.setCreatedDate(rs.getString("created_date"));
    return inputDetailsVO;
  }
}