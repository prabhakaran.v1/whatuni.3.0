package com.wuni.ultimatesearch.iwanttobe.forms;

import java.util.ArrayList;
import java.util.List;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This is used class used as input bean as well as Valued Objects.
 */

public class IWanttobeBean
{

  private String logId = null;
  private String pageNo = null;
  
  //IWanttobe SearchResults page properties
  private String jacsCode = null;
  private String jacsSubject = null;
  private String jacsPercentage = null;
  private String empPercentage = null;
  private String salaryRange = null;
  private String subjectGuideURL = null;
  private String iwantSearchType = null;
  private String iwantSearchCode = null;
  private String iwantSearchName = null;
  private String entryRequirements = null;
  private String jacsDefinition = null;
  private List qualSubjectList = new ArrayList();
  private String qualSubjectId = null;
  private String qualSubjectName = null;
  private String qualSubPercentage = null;
  private String jobIndustryDefinition = null;
  
  
  //IWanttobe EmploymentInfo page properties
  private String allGradEmpPercentage = null;
  private String UKPRN = null;
  private String collegeId = null;
  private String collegeNameDisplay = null;
  private String gradEmployeedPercent = null;
  private String totalCount = null;
  private String sortByWhich = null;
  private String sortByHow = null;  
  private String providerViewDegreeFlag = null;
  private String providerViewDegreeURL = null;
  private String jacsQualification = null;
  private String displayRecCnt = null;
  private String imagePath = null;
  private String imageName = null;
  private String createdDate = null;
  //Added by Indumathi.s for IWantToBe redesign Feb-16-2016
  private String schoolType = null;
  private String inWhatYear = null;
  private String yearOfEntry = null;
  private String color = null;
  private String jobOrIndustryText = null;
  private String jacsJobOrIndTooltipText = null;
  //Added by Prabha for IWantToBe redesign Feb-16-2016
  private String stickManFig = null;
  private String stickManColor = null;
  private String jobIndustryId = null;
  private String salaryRangeColor = null;
  private String empTooltipTxt = null;
  private String salaryDisplayColor = null;
  private String salaryTooltipTxt = null;
  
  public void setLogId(String logId)
  {
    this.logId = logId;
  }

  public String getLogId()
  {
    return logId;
  }

  public void setPageNo(String pageNo)
  {
    this.pageNo = pageNo;
  }

  public String getPageNo()
  {
    return pageNo;
  }

  public void setJacsCode(String jacsCode)
  {
    this.jacsCode = jacsCode;
  }

  public String getJacsCode()
  {
    return jacsCode;
  }

  public void setJacsSubject(String jacsSubject)
  {
    this.jacsSubject = jacsSubject;
  }

  public String getJacsSubject()
  {
    return jacsSubject;
  }

  public void setJacsPercentage(String jacsPercentage)
  {
    this.jacsPercentage = jacsPercentage;
  }

  public String getJacsPercentage()
  {
    return jacsPercentage;
  }

  public void setEmpPercentage(String empPercentage)
  {
    this.empPercentage = empPercentage;
  }

  public String getEmpPercentage()
  {
    return empPercentage;
  }

  public void setSalaryRange(String salaryRange)
  {
    this.salaryRange = salaryRange;
  }

  public String getSalaryRange()
  {
    return salaryRange;
  }

  public void setSubjectGuideURL(String subjectGuideURL)
  {
    this.subjectGuideURL = subjectGuideURL;
  }

  public String getSubjectGuideURL()
  {
    return subjectGuideURL;
  }

  public void setIwantSearchType(String iwantSearchType)
  {
    this.iwantSearchType = iwantSearchType;
  }

  public String getIwantSearchType()
  {
    return iwantSearchType;
  }

  public void setIwantSearchCode(String iwantSearchCode)
  {
    this.iwantSearchCode = iwantSearchCode;
  }

  public String getIwantSearchCode()
  {
    return iwantSearchCode;
  }

  public void setIwantSearchName(String iwantSearchName)
  {
    this.iwantSearchName = iwantSearchName;
  }

  public String getIwantSearchName()
  {
    return iwantSearchName;
  }

  public void setAllGradEmpPercentage(String allGradEmpPercentage)
  {
    this.allGradEmpPercentage = allGradEmpPercentage;
  }

  public String getAllGradEmpPercentage()
  {
    return allGradEmpPercentage;
  }

  public void setUKPRN(String uKPRN)
  {
    this.UKPRN = uKPRN;
  }

  public String getUKPRN()
  {
    return UKPRN;
  }

  public void setCollegeId(String collegeId)
  {
    this.collegeId = collegeId;
  }

  public String getCollegeId()
  {
    return collegeId;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay)
  {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay()
  {
    return collegeNameDisplay;
  }

  public void setGradEmployeedPercent(String gradEmployeedPercent)
  {
    this.gradEmployeedPercent = gradEmployeedPercent;
  }

  public String getGradEmployeedPercent()
  {
    return gradEmployeedPercent;
  }

  public void setTotalCount(String totalCount)
  {
    this.totalCount = totalCount;
  }

  public String getTotalCount()
  {
    return totalCount;
  }

  public void setSortByWhich(String sortByWhich)
  {
    this.sortByWhich = sortByWhich;
  }

  public String getSortByWhich()
  {
    return sortByWhich;
  }

  public void setSortByHow(String sortByHow)
  {
    this.sortByHow = sortByHow;
  }

  public String getSortByHow()
  {
    return sortByHow;
  }

  public void setProviderViewDegreeFlag(String providerViewDegreeFlag)
  {
    this.providerViewDegreeFlag = providerViewDegreeFlag;
  }

  public String getProviderViewDegreeFlag()
  {
    return providerViewDegreeFlag;
  }

  public void setProviderViewDegreeURL(String providerViewDegreeURL)
  {
    this.providerViewDegreeURL = providerViewDegreeURL;
  }

  public String getProviderViewDegreeURL()
  {
    return providerViewDegreeURL;
  }

  public void setEntryRequirements(String entryRequirements)
  {
    this.entryRequirements = entryRequirements;
  }

  public String getEntryRequirements()
  {
    return entryRequirements;
  }

  public void setJacsDefinition(String jacsDefinition)
  {
    this.jacsDefinition = jacsDefinition;
  }

  public String getJacsDefinition()
  {
    return jacsDefinition;
  }

  public void setQualSubjectList(List qualSubjectList)
  {
    this.qualSubjectList = qualSubjectList;
  }

  public List getQualSubjectList()
  {
    return qualSubjectList;
  }

  public void setQualSubjectId(String qualSubjectId)
  {
    this.qualSubjectId = qualSubjectId;
  }

  public String getQualSubjectId()
  {
    return qualSubjectId;
  }

  public void setQualSubjectName(String qualSubjectName)
  {
    this.qualSubjectName = qualSubjectName;
  }

  public String getQualSubjectName()
  {
    return qualSubjectName;
  }

  public void setQualSubPercentage(String qualSubPercentage)
  {
    this.qualSubPercentage = qualSubPercentage;
  }

  public String getQualSubPercentage()
  {
    return qualSubPercentage;
  }

  public void setJobIndustryDefinition(String jobIndustryDefinition)
  {
    this.jobIndustryDefinition = jobIndustryDefinition;
  }

  public String getJobIndustryDefinition()
  {
    return jobIndustryDefinition;
  }

    public void setJacsQualification(String jacsQualification) {
        this.jacsQualification = jacsQualification;
    }

    public String getJacsQualification() {
        return jacsQualification;
    }
  public void setDisplayRecCnt(String displayRecCnt) {
    this.displayRecCnt = displayRecCnt;
  }
  public String getDisplayRecCnt() {
    return displayRecCnt;
  }
  public void setCreatedDate(String createdDate) {
    this.createdDate = createdDate;
  }
  public String getCreatedDate() {
    return createdDate;
  }
  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }
  public String getImagePath() {
    return imagePath;
  }
  public void setImageName(String imageName) {
    this.imageName = imageName;
  }
  public String getImageName() {
    return imageName;
  }

  public void setSchoolType(String schoolType) {
    this.schoolType = schoolType;
  }

  public String getSchoolType() {
    return schoolType;
  }

  public void setInWhatYear(String inWhatYear) {
    this.inWhatYear = inWhatYear;
  }

  public String getInWhatYear() {
    return inWhatYear;
  }

  public void setYearOfEntry(String yearOfEntry) {
    this.yearOfEntry = yearOfEntry;
  }

  public String getYearOfEntry() {
    return yearOfEntry;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getColor() {
    return color;
  }
  public void setStickManFig(String stickManFig) {
    this.stickManFig = stickManFig;
  }
  public String getStickManFig() {
    return stickManFig;
  }
  public void setStickManColor(String stickManColor) {
    this.stickManColor = stickManColor;
  }
  public String getStickManColor() {
    return stickManColor;
  }
  public void setJobIndustryId(String jobIndustryId) {
    this.jobIndustryId = jobIndustryId;
  }
  public String getJobIndustryId() {
    return jobIndustryId;
  }
  public void setSalaryRangeColor(String salaryRangeColor) {
    this.salaryRangeColor = salaryRangeColor;
  }
  public String getSalaryRangeColor() {
    return salaryRangeColor;
  }
  public void setEmpTooltipTxt(String empTooltipTxt) {
    this.empTooltipTxt = empTooltipTxt;
  }
  public String getEmpTooltipTxt() {
    return empTooltipTxt;
  }
  public void setSalaryDisplayColor(String salaryDisplayColor) {
    this.salaryDisplayColor = salaryDisplayColor;
  }
  public String getSalaryDisplayColor() {
    return salaryDisplayColor;
  }
  public void setSalaryTooltipTxt(String salaryTooltipTxt) {
    this.salaryTooltipTxt = salaryTooltipTxt;
  }
  public String getSalaryTooltipTxt() {
    return salaryTooltipTxt;
  }
  public void setJobOrIndustryText(String jobOrIndustryText) {
    this.jobOrIndustryText = jobOrIndustryText;
  }

  public String getJobOrIndustryText() {
    return jobOrIndustryText;
  }

  public void setJacsJobOrIndTooltipText(String jacsJobOrIndTooltipText) {
    this.jacsJobOrIndTooltipText = jacsJobOrIndTooltipText;
  }

  public String getJacsJobOrIndTooltipText() {
    return jacsJobOrIndTooltipText;
  }
}
