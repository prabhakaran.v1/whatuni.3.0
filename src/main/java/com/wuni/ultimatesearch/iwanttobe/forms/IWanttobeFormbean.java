package com.wuni.ultimatesearch.iwanttobe.forms;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class IWanttobeFormbean
{
  private String jobId = null;
  private String jobName = null;
  private String industryId = null;
  private String industryName = null;
  private String schoolId = null;
  private String schoolName = null;
  private String schoolNameFreeText = null;
  private String intendedYear = null;
  private String schoolCheckFlag = null;
  private ArrayList searchResultsList = new ArrayList();
  private String iwantSearchType = null;
  private String iwantSearchCode = null;
  private String iwantSearchName = null;
  private String logId = null;
  private String allGradEmpPercenrage = null;
  private String salaryRange = null;
  private String pageNo = null;
  private String totalCount = null;
  private String sortByWhich = null;
  private String sortByHow = null; 
  private String intendedYearText = null; 

  public void setJobId(String jobId)
  {
    this.jobId = jobId;
  }

  public String getJobId()
  {
    return jobId;
  }

  public void setJobName(String jobName)
  {
    this.jobName = jobName;
  }

  public String getJobName()
  {
    return jobName;
  }

  public void setIndustryId(String industryId)
  {
    this.industryId = industryId;
  }

  public String getIndustryId()
  {
    return industryId;
  }

  public void setIndustryName(String industryName)
  {
    this.industryName = industryName;
  }

  public String getIndustryName()
  {
    return industryName;
  }

  public void setSchoolId(String schoolId)
  {
    this.schoolId = schoolId;
  }

  public String getSchoolId()
  {
    return schoolId;
  }

  public void setSchoolName(String schoolName)
  {
    this.schoolName = schoolName;
  }

  public String getSchoolName()
  {
    return schoolName;
  }

  public void setSchoolNameFreeText(String schoolNameFreeText)
  {
    this.schoolNameFreeText = schoolNameFreeText;
  }

  public String getSchoolNameFreeText()
  {
    return schoolNameFreeText;
  }

  public void setIntendedYear(String intendedYear)
  {
    this.intendedYear = intendedYear;
  }

  public String getIntendedYear()
  {
    return intendedYear;
  }

  public void setSchoolCheckFlag(String schoolCheckFlag)
  {
    this.schoolCheckFlag = schoolCheckFlag;
  }

  public String getSchoolCheckFlag()
  {
    return schoolCheckFlag;
  }

  public void setSearchResultsList(ArrayList searchResultsList)
  {
    this.searchResultsList = searchResultsList;
  }

  public ArrayList getSearchResultsList()
  {
    return searchResultsList;
  }

  public void setIwantSearchType(String iwantSearchType)
  {
    this.iwantSearchType = iwantSearchType;
  }

  public String getIwantSearchType()
  {
    return iwantSearchType;
  }

  public void setIwantSearchCode(String iwantSearchCode)
  {
    this.iwantSearchCode = iwantSearchCode;
  }

  public String getIwantSearchCode()
  {
    return iwantSearchCode;
  }

  public void setIwantSearchName(String iwantSearchName)
  {
    this.iwantSearchName = iwantSearchName;
  }

  public String getIwantSearchName()
  {
    return iwantSearchName;
  }

  public void setLogId(String logId)
  {
    this.logId = logId;
  }

  public String getLogId()
  {
    return logId;
  }

  public void setAllGradEmpPercenrage(String allGradEmpPercenrage)
  {
    this.allGradEmpPercenrage = allGradEmpPercenrage;
  }

  public String getAllGradEmpPercenrage()
  {
    return allGradEmpPercenrage;
  }

  public void setSalaryRange(String salaryRange)
  {
    this.salaryRange = salaryRange;
  }

  public String getSalaryRange()
  {
    return salaryRange;
  }

  public void setPageNo(String pageNo)
  {
    this.pageNo = pageNo;
  }

  public String getPageNo()
  {
    return pageNo;
  }

  public void setTotalCount(String totalCount)
  {
    this.totalCount = totalCount;
  }

  public String getTotalCount()
  {
    return totalCount;
  }

  public void setSortByWhich(String sortByWhich)
  {
    this.sortByWhich = sortByWhich;
  }

  public String getSortByWhich()
  {
    return sortByWhich;
  }

  public void setSortByHow(String sortByHow)
  {
    this.sortByHow = sortByHow;
  }

  public String getSortByHow()
  {
    return sortByHow;
  }

    public void setIntendedYearText(String intendedYearText) {
        this.intendedYearText = intendedYearText;
    }

    public String getIntendedYearText() {
        return intendedYearText;
    }
}
