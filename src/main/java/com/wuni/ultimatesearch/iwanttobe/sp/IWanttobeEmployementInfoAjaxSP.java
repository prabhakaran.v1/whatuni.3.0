package com.wuni.ultimatesearch.iwanttobe.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.layer.util.rowmapper.widget.EmployementInfoRowmapper;
import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This SP class is used to get the results(Page2) of IWanttobe Ultimate Search
 * **************************************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * **************************************************************************************************************************************************
 * 03.02.2016     Prabhakaran V.             1.1     Redisign the i want to be page added new params               wu_548
 */

public class IWanttobeEmployementInfoAjaxSP extends StoredProcedure{
  public IWanttobeEmployementInfoAjaxSP(DataSource datasource){
    setDataSource(datasource);
    setSql(GlobalConstants.SORT_IWANTTOBE_INFO_PRC);
    declareParameter(new SqlParameter("p_jacs_code", OracleTypes.VARCHAR));             
    declareParameter(new SqlParameter("p_ukprn", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_ultimate_srch_log_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_sortby_which_col_str", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_sortby_how_str", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("p_job_ind_emp_rate_text", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("p_job_ind_salary_text", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("pc_univ_emp_info", OracleTypes.CURSOR, new UniEmoloymentInfoRowmapper()));
    declareParameter(new SqlOutParameter("pc_emp_info", OracleTypes.CURSOR, new EmployementInfoRowmapper()));
  }

  public Map execute(IWanttobeBean inputBean){
    Map outMap = null;
    try{    
        Map inMap = new HashMap();
        inMap.put("p_jacs_code", inputBean.getJacsCode());
        inMap.put("p_ukprn", inputBean.getUKPRN());
        inMap.put("p_college_id", inputBean.getCollegeId());
        inMap.put("p_ultimate_srch_log_id", inputBean.getLogId());
        inMap.put("p_sortby_which_col_str", inputBean.getSortByWhich());
        inMap.put("p_sortby_how_str", inputBean.getSortByHow());
        inMap.put("p_page_no", inputBean.getPageNo());        
        outMap = execute(inMap);
      }catch(Exception ex){
          ex.printStackTrace();
      }
    return outMap;
  }  
  
  private class UniEmoloymentInfoRowmapper implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      IWanttobeBean uniEmpInfoVO = new IWanttobeBean();
      uniEmpInfoVO.setUKPRN(rs.getString("ukprn"));
      uniEmpInfoVO.setCollegeId(rs.getString("college_id"));
      uniEmpInfoVO.setImagePath(rs.getString("college_logo"));
      uniEmpInfoVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      uniEmpInfoVO.setProviderViewDegreeURL(rs.getString("college_url"));
      uniEmpInfoVO.setProviderViewDegreeFlag(rs.getString("view_degree_flag"));
      uniEmpInfoVO.setSubjectGuideURL(rs.getString("college_name_for_url"));
      uniEmpInfoVO.setStickManFig(rs.getString("stickman_figure"));
      uniEmpInfoVO.setStickManColor(rs.getString("stickman_figure_colour"));
      uniEmpInfoVO.setSalaryRange(rs.getString("salary_range"));
      uniEmpInfoVO.setSalaryRangeColor(rs.getString("salary_range_colour"));
      uniEmpInfoVO.setTotalCount(rs.getString("total_cnt"));
      uniEmpInfoVO.setSalaryDisplayColor(rs.getString("salary_display_percentages"));
      return uniEmpInfoVO;
    }
  }
}
