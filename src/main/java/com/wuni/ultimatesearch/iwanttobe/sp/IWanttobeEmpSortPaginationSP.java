package com.wuni.ultimatesearch.iwanttobe.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This SP class is used for getting results of Sorting and Pagination of IWanttobe page3.
 */

public class IWanttobeEmpSortPaginationSP extends StoredProcedure
{

    public IWanttobeEmpSortPaginationSP(DataSource datasource)
    {
      setDataSource(datasource);
      setSql("wu_ultimate_search_pkg.sort_iwanttobe_employ_info_prc");
        declareParameter(new SqlParameter("p_ukprn", OracleTypes.VARCHAR));
        declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
      declareParameter(new SqlParameter("p_job_or_industry_code", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_which_search", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_sortby_which_col_str", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_sortby_how_str", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_page_no", OracleTypes.NUMBER));
      declareParameter(new SqlOutParameter("p_job_OR_industry_name", OracleTypes.VARCHAR));
      declareParameter(new SqlOutParameter("pc_univ_emp_info", OracleTypes.CURSOR, new UniEmoloymentInfoRowmapper()));
      
    }
    public Map execute(IWanttobeBean inputBean)
    {
      Map outMap = null;
      Map inMap = new HashMap();
        inMap.put("p_ukprn", inputBean.getUKPRN());
        inMap.put("p_college_id", inputBean.getCollegeId());
      inMap.put("p_job_or_industry_code", inputBean.getIwantSearchCode());
      inMap.put("p_which_search", inputBean.getIwantSearchType());
      inMap.put("p_sortby_which_col_str", inputBean.getSortByWhich());
      inMap.put("p_sortby_how_str", inputBean.getSortByHow());
      inMap.put("p_page_no", inputBean.getPageNo());
      outMap = execute(inMap);
      return outMap;
    }  

  private class UniEmoloymentInfoRowmapper implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
     IWanttobeBean uniEmpInfoVO = new IWanttobeBean();
     uniEmpInfoVO.setUKPRN(rs.getString("ukprn"));
     uniEmpInfoVO.setCollegeId(rs.getString("college_id"));
     uniEmpInfoVO.setCollegeNameDisplay(rs.getString("college_name_display"));
     uniEmpInfoVO.setGradEmployeedPercent(rs.getString("grad_employed_percent"));
     uniEmpInfoVO.setSalaryRange(rs.getString("salary_range"));
     uniEmpInfoVO.setTotalCount(rs.getString("total_cnt"));
     return uniEmpInfoVO;
    }
  }


}
