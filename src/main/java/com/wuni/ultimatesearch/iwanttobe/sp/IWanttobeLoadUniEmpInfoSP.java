package com.wuni.ultimatesearch.iwanttobe.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This SP class is used get the results of University Employment Info(Page3).
 */

public class IWanttobeLoadUniEmpInfoSP extends StoredProcedure
  {

    public IWanttobeLoadUniEmpInfoSP(DataSource datasource)
    {
      setDataSource(datasource);
      setSql("wu_ultimate_search_pkg.get_common_degrees_prc");
      declareParameter(new SqlParameter("p_ukprn", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_college_id", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_job_or_industry_code", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_which_search", OracleTypes.VARCHAR));
      declareParameter(new SqlOutParameter("pc_common_degrees", OracleTypes.CURSOR, new UniEmploymentInfoRowMapper()));
    }

    public Map execute(IWanttobeBean inputBean)
    {
      Map outMap = null;
      Map inMap = new HashMap();
      inMap.put("p_ukprn", inputBean.getUKPRN());
      inMap.put("p_college_id", inputBean.getCollegeId());
      inMap.put("p_job_or_industry_code", inputBean.getIwantSearchCode());
      inMap.put("p_which_search", inputBean.getIwantSearchType());
      outMap = execute(inMap);
      return outMap;
    }  

    private class UniEmploymentInfoRowMapper implements RowMapper
    {
      public Object mapRow(ResultSet rs, int rowNum) throws SQLException
      {
       IWanttobeBean uniEmploymentInfoVO = new IWanttobeBean();
       uniEmploymentInfoVO.setJacsCode(rs.getString("JACS_CODE"));
       uniEmploymentInfoVO.setJacsSubject(rs.getString("jacs_subject"));
       uniEmploymentInfoVO.setJacsPercentage(rs.getString("jacs_percentage"));
       uniEmploymentInfoVO.setProviderViewDegreeFlag(rs.getString("view_degree_flag")); 
       return uniEmploymentInfoVO;
      }
    }
    
}
