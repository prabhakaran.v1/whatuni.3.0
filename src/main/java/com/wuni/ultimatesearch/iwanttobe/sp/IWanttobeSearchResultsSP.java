package com.wuni.ultimatesearch.iwanttobe.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;

/**
  * @IWANTTOBE Ultimate Search
  * @version 1.0
  * @since 3-JUNE-2014 Release
  * @purpose  This SP is used to get the results for given input of IWanttobe Ultimate Search(Page2).
  * Change Log
  * ***************************************************************************************************************************************************
  * Date           Name          Ver.       Changes desc                                                                  Rel Ver.
  * ***************************************************************************************************************************************************
  * 21-July-2015  Thiyagu G     1.1        Moved rowmapper class to common place for reuseing course search page            1.1
  */
public class IWanttobeSearchResultsSP extends StoredProcedure {

  public IWanttobeSearchResultsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("wu_ultimate_search_pkg.get_iwanttobe_results_prc");
    declareParameter(new SqlParameter("p_ultimate_srch_log_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_page_no", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_display_rec_cnt", OracleTypes.NUMBER)); //Added display record count for 21_July_2015, by Thiyagu G.
    declareParameter(new SqlOutParameter("pc_results", OracleTypes.CURSOR, new IWantToBeSearchResultsRowMapperImpl())); //Modified by Indumathi.S Feb-16-2016 Rel
    declareParameter(new SqlOutParameter("p_return_code", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("p_remaining_percent", OracleTypes.NUMBER));
  }

  public Map execute(IWanttobeBean inputBean) {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      inMap.put("p_ultimate_srch_log_id", inputBean.getLogId());
      inMap.put("p_page_no", inputBean.getPageNo());
      inMap.put("p_display_rec_cnt", inputBean.getDisplayRecCnt()); //Added display record count for 21_July_2015, by Thiyagu G.   
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }

  public class IWantToBeSearchResultsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      IWanttobeBean iwanttobeResultsVO = new IWanttobeBean();
      iwanttobeResultsVO.setJacsCode(rs.getString("jacs_code"));
      iwanttobeResultsVO.setJacsSubject(rs.getString("jacs_subject"));
      iwanttobeResultsVO.setJacsPercentage(rs.getString("jacs_percentage"));
      iwanttobeResultsVO.setColor(rs.getString("colour"));
      iwanttobeResultsVO.setIwantSearchCode(rs.getString("job_or_industry_id"));
      iwanttobeResultsVO.setIwantSearchName(rs.getString("job_or_industry_name"));
      iwanttobeResultsVO.setCreatedDate(rs.getString("created_date"));
      iwanttobeResultsVO.setJobOrIndustryText(rs.getString("job_OR_industry_text"));
      iwanttobeResultsVO.setJacsJobOrIndTooltipText(rs.getString("jacs_job_OR_ind_tooltip_text"));
      return iwanttobeResultsVO;
    }
  }
}
