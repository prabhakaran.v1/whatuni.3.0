package com.wuni.ultimatesearch.iwanttobe.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;

/**
 * @author Indumathi.S
 * @IWANTTOBE Ultimate Search
 * FEB-16-2016 Release.
 * @Purpose: This SP class is used to get the year of entry for IWanttobe Ultimate Search
 */
public class IWanttobeYearOfEntrySP extends StoredProcedure {

  public IWanttobeYearOfEntrySP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql("WU_ULTIMATE_SEARCH_PKG.get_in_what_year_fn");
    declareParameter(new SqlOutParameter("pc_year_of_entry", OracleTypes.CURSOR, new YearOfEntryListRowMapperImpl()));
    declareParameter(new SqlParameter("p_school_type", OracleTypes.VARCHAR));
  }

  public Map execute(IWanttobeBean inputBean) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_school_type", inputBean.getSchoolType());
    outMap = execute(inMap);
    return outMap;
  }

  private class YearOfEntryListRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet resultSet, int rowNum) throws SQLException {
      IWanttobeBean resultBean = new IWanttobeBean();
      resultBean.setInWhatYear(resultSet.getString("in_what_year"));
      resultBean.setYearOfEntry(resultSet.getString("year_of_entry"));
      return resultBean;
    }
  }
}
