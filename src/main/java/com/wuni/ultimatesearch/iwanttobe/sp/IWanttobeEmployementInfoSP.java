package com.wuni.ultimatesearch.iwanttobe.sp;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.layer.util.rowmapper.widget.EmployementInfoRowmapper;
import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;

/**
 * @IWANTTOBE Ultimate Search
 * 16-Feb-2016 Release.
 * @Purpose: This SP class is used to get the results(Page2) of IWanttobe Ultimate Search
 * ******************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * ******************************************************************************************************************************
 * 03.02.2016     Prabhakaran V.             1.1     Redesign the i want to be page added new params               wu_548
 */

public class IWanttobeEmployementInfoSP extends StoredProcedure{
  public IWanttobeEmployementInfoSP(DataSource datasource){
    setDataSource(datasource);
    setSql(GlobalConstants.GET_IWANTTOBE_STATS_INFO_PRC);
    declareParameter(new SqlParameter("p_JACS_code", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_ultimate_srch_log_id", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("pc_emp_info", OracleTypes.CURSOR, new EmployementInfoRowmapper()));
  }

  public Map execute(IWanttobeBean inputBean){
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_JACS_code", inputBean.getJacsCode());
    inMap.put("p_ultimate_srch_log_id", inputBean.getLogId());
    outMap = execute(inMap);
    return outMap;
  }  
}
    
  
  
  
