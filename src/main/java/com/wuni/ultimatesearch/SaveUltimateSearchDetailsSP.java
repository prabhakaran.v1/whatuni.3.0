package com.wuni.ultimatesearch;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This SP is used to Srote the User Inputs of both Ultimate Searches(Whatcanido and Ultimate Search).
 */

public class SaveUltimateSearchDetailsSP extends StoredProcedure

{

  public SaveUltimateSearchDetailsSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql("wu_ultimate_srch_entry_det_pkg.save_srch_input_details_prc");
    declareParameter(new SqlParameter("p_ultimate_search_type", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_qual_type_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_subj1_id", OracleTypes.NUMBER));//3_FEB_2015 Modified to change the parameters
    declareParameter(new SqlParameter("p_subj2_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_subj3_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_subj4_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_subj5_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_subj6_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_subj1_grade_OR_points", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_subj2_grade_OR_points", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_subj3_grade_OR_points", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_subj4_grade_OR_points", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_subj5_grade_OR_points", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_subj6_grade_OR_points", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_school_name", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_school_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_year_of_entry", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_job_soc_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_industry_sic_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_track_session_id", Types.NUMERIC)); //Added tracking_session_id for 21_July_2015, by Thiyagu G.
    declareParameter(new SqlOutParameter("p_ultimate_search_new_log_id", OracleTypes.VARCHAR));
  }
  
  public Map execute(SaveULInputbean inputbean)
  {
    Map outMap = null;
    Map inMap = new HashMap();
    try{
    inMap.put("p_ultimate_search_type", inputbean.getUlSearchType());
    inMap.put("p_qual_type_id", inputbean.getQualification());
    inMap.put("p_subj1_id", inputbean.getQualSub1());
    inMap.put("p_subj2_id", inputbean.getQualSub2());
    inMap.put("p_subj3_id", inputbean.getQualSub3());
    inMap.put("p_subj4_id", inputbean.getQualSub4());
    inMap.put("p_subj5_id", inputbean.getQualSub5());
    inMap.put("p_subj6_id", inputbean.getQualSub6());
    inMap.put("p_subj1_grade_OR_points", inputbean.getQualGrades1());
    inMap.put("p_subj2_grade_OR_points", inputbean.getQualGrades2());
    inMap.put("p_subj3_grade_OR_points", inputbean.getQualGrades3());
    inMap.put("p_subj4_grade_OR_points", inputbean.getQualGrades4());
    inMap.put("p_subj5_grade_OR_points", inputbean.getQualGrades5());
    inMap.put("p_subj6_grade_OR_points", inputbean.getQualGrades6());
    inMap.put("p_school_name", inputbean.getSchoolName());
    inMap.put("p_school_id", inputbean.getSchoolId());
    inMap.put("p_year_of_entry", inputbean.getIntendedYear());
    inMap.put("p_user_id", inputbean.getUserId());
    inMap.put("p_job_soc_id", inputbean.getJobSocCode());
    inMap.put("p_industry_sic_id", inputbean.getIndustrySicCode());
    inMap.put("p_track_session_id", inputbean.getTrackSessionId()); //Added tracking_session_id for 21_July_2015, by Thiyagu G.
    outMap = execute(inMap);
    }catch(Exception e){
      e.printStackTrace();
    }
    return outMap;
  }
}
