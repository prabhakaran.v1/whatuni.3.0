package com.wuni.ultimatesearch.whatcanido.forms;

import java.util.ArrayList;
import java.util.List;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This Bean class is used for input bean and Value Object bean.
 */
public class WhatCanIDoBean
{

  private String saveLogId = null;
  private String pageNo = null;
  private String subjectGuideURL = null;
  private String searchGrades = null;
  private String searchQual = null;
  private String providerSearcURL = null;
  
  private String jacsCode = null;
  private String jacsSubject = null;
  private String jacsPercentage = null;
  private String qualSubjectId = null;
  private String qualSubjectName = null;
  private String qualSubPercentage = null;
  private List qualSubjectList = new ArrayList();
  private String equivalentTarriffPoints = null;
  
  //Employment Info
  private String JacsSubGradEmpRate = null;
  private String allGradEmpRate = null;
  private String subjectLowerSalary = null;
  private String subjectHigherSalary = null;
  private String allLowerSalary = null;
  private String allHigherSalary = null;
  
  private String industryCode = null;
  private String industryDescription = null;
  private String industryWisePerncentage = null;
  
  private String jobCode = null;
  private String jobDescription = null;
  private String jobWisePercentage = null;
  
  private String collegeId = null;
  private String collegeNameDisplay = null;
  private String collegeName = null;
  private String subjectWiseEmpRate = null;
  private String lowerPerncentage = null;
  private String higherPercentage = null;
  private String salaryRange = null;
  private String ukprn = null;
  private String totalCount = null;
  private String sortByWhich = null;
  private String sortByHow = null;  
  private String jacsDefinition = null;
  private String jacsQualification = null;
  private String displayRecCnt = null;  
  
  //Added by Indumathi.S For What can i do redesign 08-03-16
  private String qualificationId = null;
  private String qualification = null;
  private String qualificationGrades = null;
  private String qualificationTypeId = null;
  private String defaultNoOfSubjects = null;
  private String wcidColor = null;
  private String defaultGrades = null;
  //Added by Prabha for What can i do widget redesign 08-03-16
  private String qualificationType = null;
  private String tariffPoint = null;
  private String sortingOrder = null;
  private String sortingType = null;
  private String employeeInfoFlag = null;
  private String empRate = null;
  private String empRateColor = null;
  private String salaryRangeColor = null;
  private String salaryBarWidth = null;
  private String imagePath = null;
  private String providerViewDegreeFlag = null;
  private String industryColour = null;
  private String jobColour = null;
  private String jobIndustryFlag = null;
  private String collegeNameForUrl = null;
  
  public void setJacsCode(String jacsCode)
  {
    this.jacsCode = jacsCode;
  }

  public String getJacsCode()
  {
    return jacsCode;
  }

  public void setJacsSubject(String jacsSubject)
  {
    this.jacsSubject = jacsSubject;
  }

  public String getJacsSubject()
  {
    return jacsSubject;
  }

  public void setJacsPercentage(String jacsPercentage)
  {
    this.jacsPercentage = jacsPercentage;
  }

  public String getJacsPercentage()
  {
    return jacsPercentage;
  }

  public void setQualSubjectId(String qualSubjectId)
  {
    this.qualSubjectId = qualSubjectId;
  }

  public String getQualSubjectId()
  {
    return qualSubjectId;
  }

  public void setQualSubjectName(String qualSubjectName)
  {
    this.qualSubjectName = qualSubjectName;
  }

  public String getQualSubjectName()
  {
    return qualSubjectName;
  }

  public void setQualSubPercentage(String qualSubPercentage)
  {
    this.qualSubPercentage = qualSubPercentage;
  }

  public String getQualSubPercentage()
  {
    return qualSubPercentage;
  }

  public void setQualSubjectList(List qualSubjectList)
  {
    this.qualSubjectList = qualSubjectList;
  }

  public List getQualSubjectList()
  {
    return qualSubjectList;
  }

  public void setPageNo(String pageNo)
  {
    this.pageNo = pageNo;
  }

  public String getPageNo()
  {
    return pageNo;
  }

  public void setAllGradEmpRate(String allGradEmpRate)
  {
    this.allGradEmpRate = allGradEmpRate;
  }

  public String getAllGradEmpRate()
  {
    return allGradEmpRate;
  }

  public void setSubjectLowerSalary(String subjectLowerSalary)
  {
    this.subjectLowerSalary = subjectLowerSalary;
  }

  public String getSubjectLowerSalary()
  {
    return subjectLowerSalary;
  }

  public void setSubjectHigherSalary(String subjectHigherSalary)
  {
    this.subjectHigherSalary = subjectHigherSalary;
  }

  public String getSubjectHigherSalary()
  {
    return subjectHigherSalary;
  }

  public void setAllLowerSalary(String allLowerSalary)
  {
    this.allLowerSalary = allLowerSalary;
  }

  public String getAllLowerSalary()
  {
    return allLowerSalary;
  }

  public void setAllHigherSalary(String allHigherSalary)
  {
    this.allHigherSalary = allHigherSalary;
  }

  public String getAllHigherSalary()
  {
    return allHigherSalary;
  }

  public void setJacsSubGradEmpRate(String jacsSubGradEmpRate)
  {
    this.JacsSubGradEmpRate = jacsSubGradEmpRate;
  }

  public String getJacsSubGradEmpRate()
  {
    return JacsSubGradEmpRate;
  }

  public void setIndustryCode(String industryCode)
  {
    this.industryCode = industryCode;
  }

  public String getIndustryCode()
  {
    return industryCode;
  }

  public void setIndustryDescription(String industryDescription)
  {
    this.industryDescription = industryDescription;
  }

  public String getIndustryDescription()
  {
    return industryDescription;
  }

  public void setIndustryWisePerncentage(String industryWisePerncentage)
  {
    this.industryWisePerncentage = industryWisePerncentage;
  }

  public String getIndustryWisePerncentage()
  {
    return industryWisePerncentage;
  }

  public void setJobCode(String jobCode)
  {
    this.jobCode = jobCode;
  }

  public String getJobCode()
  {
    return jobCode;
  }

  public void setJobDescription(String jobDescription)
  {
    this.jobDescription = jobDescription;
  }

  public String getJobDescription()
  {
    return jobDescription;
  }

  public void setJobWisePercentage(String jobWisePercentage)
  {
    this.jobWisePercentage = jobWisePercentage;
  }

  public String getJobWisePercentage()
  {
    return jobWisePercentage;
  }

  public void setCollegeId(String collegeId)
  {
    this.collegeId = collegeId;
  }

  public String getCollegeId()
  {
    return collegeId;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay)
  {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay()
  {
    return collegeNameDisplay;
  }

  public void setSubjectWiseEmpRate(String subjectWiseEmpRate)
  {
    this.subjectWiseEmpRate = subjectWiseEmpRate;
  }

  public String getSubjectWiseEmpRate()
  {
    return subjectWiseEmpRate;
  }

  public void setLowerPerncentage(String lowerPerncentage)
  {
    this.lowerPerncentage = lowerPerncentage;
  }

  public String getLowerPerncentage()
  {
    return lowerPerncentage;
  }

  public void setHigherPercentage(String higherPercentage)
  {
    this.higherPercentage = higherPercentage;
  }

  public String getHigherPercentage()
  {
    return higherPercentage;
  }

  public void setSubjectGuideURL(String subjectGuideURL)
  {
    this.subjectGuideURL = subjectGuideURL;
  }

  public String getSubjectGuideURL()
  {
    return subjectGuideURL;
  }

  public void setUkprn(String ukprn)
  {
    this.ukprn = ukprn;
  }

  public String getUkprn()
  {
    return ukprn;
  }

  public void setTotalCount(String totalCount)
  {
    this.totalCount = totalCount;
  }

  public String getTotalCount()
  {
    return totalCount;
  }

  public void setSortByWhich(String sortByWhich)
  {
    this.sortByWhich = sortByWhich;
  }

  public String getSortByWhich()
  {
    return sortByWhich;
  }

  public void setSortByHow(String sortByHow)
  {
    this.sortByHow = sortByHow;
  }

  public String getSortByHow()
  {
    return sortByHow;
  }

  public void setSearchGrades(String searchGrades)
  {
    this.searchGrades = searchGrades;
  }

  public String getSearchGrades()
  {
    return searchGrades;
  }

  public void setSearchQual(String searchQual)
  {
    this.searchQual = searchQual;
  }

  public String getSearchQual()
  {
    return searchQual;
  }

 
  public void setCollegeName(String collegeName)
  {
    this.collegeName = collegeName;
  }

  public String getCollegeName()
  {
    return collegeName;
  }

  public void setProviderSearcURL(String providerSearcURL)
  {
    this.providerSearcURL = providerSearcURL;
  }

  public String getProviderSearcURL()
  {
    return providerSearcURL;
  }

  public void setSaveLogId(String saveLogId)
  {
    this.saveLogId = saveLogId;
  }

  public String getSaveLogId()
  {
    return saveLogId;
  }

  public void setSalaryRange(String salaryRange)
  {
    this.salaryRange = salaryRange;
  }

  public String getSalaryRange()
  {
    return salaryRange;
  }

  public void setEquivalentTarriffPoints(String equivalentTarriffPoints)
  {
    this.equivalentTarriffPoints = equivalentTarriffPoints;
  }

  public String getEquivalentTarriffPoints()
  {
    return equivalentTarriffPoints;
  }

  public void setJacsDefinition(String jacsDefinition)
  {
    this.jacsDefinition = jacsDefinition;
  }

  public String getJacsDefinition()
  {
    return jacsDefinition;
  }

    public void setJacsQualification(String jacsQualification) {
        this.jacsQualification = jacsQualification;
    }

    public String getJacsQualification() {
        return jacsQualification;
    }
  public void setDisplayRecCnt(String displayRecCnt) {
    this.displayRecCnt = displayRecCnt;
  }
  public String getDisplayRecCnt() {
    return displayRecCnt;
  }

  public void setQualificationId(String qualificationId) {
    this.qualificationId = qualificationId;
  }

  public String getQualificationId() {
    return qualificationId;
  }

  public void setQualification(String qualification) {
    this.qualification = qualification;
  }

  public String getQualification() {
    return qualification;
  }

  public void setQualificationGrades(String qualificationGrades) {
    this.qualificationGrades = qualificationGrades;
  }

  public String getQualificationGrades() {
    return qualificationGrades;
  }

  public void setQualificationTypeId(String qualificationTypeId) {
    this.qualificationTypeId = qualificationTypeId;
  }

  public String getQualificationTypeId() {
    return qualificationTypeId;
  }

  public void setWcidColor(String wcidColor) {
    this.wcidColor = wcidColor;
  }

  public String getWcidColor() {
    return wcidColor;
  }

  public void setDefaultNoOfSubjects(String defaultNoOfSubjects) {
    this.defaultNoOfSubjects = defaultNoOfSubjects;
  }

  public String getDefaultNoOfSubjects() {
    return defaultNoOfSubjects;
  }
  public void setQualificationType(String qualificationType) {
    this.qualificationType = qualificationType;
  }
  public String getQualificationType() {
    return qualificationType;
  }
  public void setTariffPoint(String tariffPoint) {
    this.tariffPoint = tariffPoint;
  }
  public String getTariffPoint() {
    return tariffPoint;
  }
  public void setSortingOrder(String sortingOrder) {
    this.sortingOrder = sortingOrder;
  }
  public String getSortingOrder() {
    return sortingOrder;
  }
  public void setSortingType(String sortingType) {
    this.sortingType = sortingType;
  }
  public String getSortingType() {
    return sortingType;
  }
  public void setEmployeeInfoFlag(String employeeInfoFlag) {
    this.employeeInfoFlag = employeeInfoFlag;
  }
  public String getEmployeeInfoFlag() {
    return employeeInfoFlag;
  }
  public void setEmpRate(String empRate) {
    this.empRate = empRate;
  }
  public String getEmpRate() {
    return empRate;
  }
  public void setEmpRateColor(String empRateColor) {
    this.empRateColor = empRateColor;
  }
  public String getEmpRateColor() {
    return empRateColor;
  }
  public void setSalaryRangeColor(String salaryRangeColor) {
    this.salaryRangeColor = salaryRangeColor;
  }
  public String getSalaryRangeColor() {
    return salaryRangeColor;
  }
  public void setSalaryBarWidth(String salaryBarWidth) {
    this.salaryBarWidth = salaryBarWidth;
  }
  public String getSalaryBarWidth() {
    return salaryBarWidth;
  }
  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }
  public String getImagePath() {
    return imagePath;
  }
  public void setProviderViewDegreeFlag(String providerViewDegreeFlag) {
    this.providerViewDegreeFlag = providerViewDegreeFlag;
  }
  public String getProviderViewDegreeFlag() {
    return providerViewDegreeFlag;
  }
  public void setIndustryColour(String industryColour) {
    this.industryColour = industryColour;
  }
  public String getIndustryColour() {
    return industryColour;
  }
  public void setJobColour(String jobColour) {
    this.jobColour = jobColour;
  }
  public String getJobColour() {
    return jobColour;
  }
  public void setJobIndustryFlag(String jobIndustryFlag) {
    this.jobIndustryFlag = jobIndustryFlag;
  }
  public String getJobIndustryFlag() {
    return jobIndustryFlag;
  }
  public void setCollegeNameForUrl(String collegeNameForUrl) {
    this.collegeNameForUrl = collegeNameForUrl;
  }
  public String getCollegeNameForUrl() {
    return collegeNameForUrl;
  }

  public void setDefaultGrades(String defaultGrades) {
    this.defaultGrades = defaultGrades;
  }

  public String getDefaultGrades() {
    return defaultGrades;
  }

}
