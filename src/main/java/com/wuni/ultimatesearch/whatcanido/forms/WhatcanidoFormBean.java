package com.wuni.ultimatesearch.whatcanido.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

public class WhatcanidoFormBean 
{
  private String qualification = null;
  private String qualValue = null;
  private String qualDesc = null;
  private List qualList = new ArrayList();
  private String qualSub1 = null;
  private String qualSub2 = null;
  private String qualSub3 = null;
  private String qualSub4 = null;
  private String qualSub5 = null;
  private String qualSub6 = null;
  private String qualSubjName1 = null;
  private String qualSubjName2 = null;
  private String qualSubjName3 = null;
  private String qualSubjName4 = null;
  private String qualSubjName5 = null;
  private String qualSubjName6 = null;
  private String resultsHeading = null;
  private String returnCodeTwoHeading = null;
  private String seoSubjectsText = null;

  private String qualGrades1 = null;
  private String qualGrades2 = null;
  private String qualGrades3 = null;
  private String qualGrades4 = null;
  private String qualGrades5 = null;
  private String qualGrades6 = null;
  private String qualUCASPoints1 = null;
  private String qualUCASPoints2 = null;
  private String qualUCASPoints3 = null;

  private List qualGradeList = new ArrayList();
  private String schoolId = null;
  private String schoolName = null;
  private String schoolNameWCSID = null;
  private String schoolNameFreeText = null;
  private String intendedYear = null;
  private String schoolCheckFlag = null;
  private String logId = null;
  private String pageNo = null;
  private String jacsCode = null;
  private String searchQualification = null;
  private String searchGrades = null;
  private String equivalentTariffPoints = null;
  private String yearOfEntry = null;
  private String uniId = null;
  private String uniUkprn = null;  
  private String intendedYearText = null;  
  private List jacsSubjectList = new ArrayList();
  
  
  //Employment Info
  private String JacsSubGradEmpRate = null;
  private String allGradEmpRate = null;
  private String subjectLowerSalary = null;
  private String subjectHigherSalary = null;
  private String allLowerSalary = null;
  private String allHigherSalary = null;
  private String empJacsSubjectName = null;
  
  private String industryCode = null;
  private String industryDescription = null;
  private String industryWisePerncentage = null;
  
  private String jobCode = null;
  private String jobDescription = null;
  private String jobWisePercentage = null;
  
  private String collegeId = null;
  private String collegeNameDisplay = null;
  private String subjectWiseEmpRate = null;
  private String lowerPerncentage = null;
  private String higherPercentage = null;
  private List industryInfoList = new ArrayList();
  private List jobInfoList = new ArrayList();
  private List universitiesEmpRateList = new ArrayList();
  private String totalCount = null;
  private String sortByWhich = null;
  private String sortByHow = null;

  public void setQualification(String qualification)
  {
    this.qualification = qualification;
  }

  public String getQualification()
  {
    return qualification;
  }

  public void setQualValue(String qualValue)
  {
    this.qualValue = qualValue;
  }

  public String getQualValue()
  {
    return qualValue;
  }

  public void setQualDesc(String qualDesc)
  {
    this.qualDesc = qualDesc;
  }

  public String getQualDesc()
  {
    return qualDesc;
  }

  public void setQualList(List qualList)
  {
    this.qualList = qualList;
  }

  public List getQualList()
  {
    return qualList;
  }

  public void setQualSub1(String qualSub1)
  {
    this.qualSub1 = qualSub1;
  }

  public String getQualSub1()
  {
    return qualSub1;
  }

  public void setQualSub2(String qualSub2)
  {
    this.qualSub2 = qualSub2;
  }

  public String getQualSub2()
  {
    return qualSub2;
  }

  public void setQualSub3(String qualSub3)
  {
    this.qualSub3 = qualSub3;
  }

  public String getQualSub3()
  {
    return qualSub3;
  }

  public void setQualGrades1(String qualGrades1)
  {
    this.qualGrades1 = qualGrades1;
  }

  public String getQualGrades1()
  {
    return qualGrades1;
  }

  public void setQualGrades2(String qualGrades2)
  {
    this.qualGrades2 = qualGrades2;
  }

  public String getQualGrades2()
  {
    return qualGrades2;
  }

  public void setQualGradeList(List qualGradeList)
  {
    this.qualGradeList = qualGradeList;
  }

  public List getQualGradeList()
  {
    return qualGradeList;
  }

  public void setSchoolId(String schoolId)
  {
    this.schoolId = schoolId;
  }

  public String getSchoolId()
  {
    return schoolId;
  }

  public void setSchoolName(String schoolName)
  {
    this.schoolName = schoolName;
  }

  public String getSchoolName()
  {
    return schoolName;
  }

  public void setSchoolNameFreeText(String schoolNameFreeText)
  {
    this.schoolNameFreeText = schoolNameFreeText;
  }

  public String getSchoolNameFreeText()
  {
    return schoolNameFreeText;
  }

  public void setIntendedYear(String intendedYear)
  {
    this.intendedYear = intendedYear;
  }

  public String getIntendedYear()
  {
    return intendedYear;
  }

  public void setSchoolCheckFlag(String schoolCheckFlag)
  {
    this.schoolCheckFlag = schoolCheckFlag;
  }

  public String getSchoolCheckFlag()
  {
    return schoolCheckFlag;
  }

  public void setQualSub4(String qualSub4)
  {
    this.qualSub4 = qualSub4;
  }

  public String getQualSub4()
  {
    return qualSub4;
  }

  public void setQualSub5(String qualSub5)
  {
    this.qualSub5 = qualSub5;
  }

  public String getQualSub5()
  {
    return qualSub5;
  }

  public void setQualGrades3(String qualGrades3)
  {
    this.qualGrades3 = qualGrades3;
  }

  public String getQualGrades3()
  {
    return qualGrades3;
  }

  public void setQualGrades4(String qualGrades4)
  {
    this.qualGrades4 = qualGrades4;
  }

  public String getQualGrades4()
  {
    return qualGrades4;
  }

  public void setQualGrades5(String qualGrades5)
  {
    this.qualGrades5 = qualGrades5;
  }

  public String getQualGrades5()
  {
    return qualGrades5;
  }

  public void setLogId(String logId)
  {
    this.logId = logId;
  }

  public String getLogId()
  {
    return logId;
  }

  public void setJacsSubjectList(List jacsSubjectList)
  {
    this.jacsSubjectList = jacsSubjectList;
  }

  public List getJacsSubjectList()
  {
    return jacsSubjectList;
  }

  public void setPageNo(String pageNo)
  {
    this.pageNo = pageNo;
  }

  public String getPageNo()
  {
    return pageNo;
  }

  public void setJacsCode(String jacsCode)
  {
    this.jacsCode = jacsCode;
  }

  public String getJacsCode()
  {
    return jacsCode;
  }

  public void setJacsSubGradEmpRate(String jacsSubGradEmpRate)
  {
    this.JacsSubGradEmpRate = jacsSubGradEmpRate;
  }

  public String getJacsSubGradEmpRate()
  {
    return JacsSubGradEmpRate;
  }

  public void setAllGradEmpRate(String allGradEmpRate)
  {
    this.allGradEmpRate = allGradEmpRate;
  }

  public String getAllGradEmpRate()
  {
    return allGradEmpRate;
  }

  public void setSubjectLowerSalary(String subjectLowerSalary)
  {
    this.subjectLowerSalary = subjectLowerSalary;
  }

  public String getSubjectLowerSalary()
  {
    return subjectLowerSalary;
  }

  public void setSubjectHigherSalary(String subjectHigherSalary)
  {
    this.subjectHigherSalary = subjectHigherSalary;
  }

  public String getSubjectHigherSalary()
  {
    return subjectHigherSalary;
  }

  public void setAllLowerSalary(String allLowerSalary)
  {
    this.allLowerSalary = allLowerSalary;
  }

  public String getAllLowerSalary()
  {
    return allLowerSalary;
  }

  public void setAllHigherSalary(String allHigherSalary)
  {
    this.allHigherSalary = allHigherSalary;
  }

  public String getAllHigherSalary()
  {
    return allHigherSalary;
  }

  public void setEmpJacsSubjectName(String empJacsSubjectName)
  {
    this.empJacsSubjectName = empJacsSubjectName;
  }

  public String getEmpJacsSubjectName()
  {
    return empJacsSubjectName;
  }

  public void setIndustryCode(String industryCode)
  {
    this.industryCode = industryCode;
  }

  public String getIndustryCode()
  {
    return industryCode;
  }

  public void setIndustryDescription(String industryDescription)
  {
    this.industryDescription = industryDescription;
  }

  public String getIndustryDescription()
  {
    return industryDescription;
  }

  public void setIndustryWisePerncentage(String industryWisePerncentage)
  {
    this.industryWisePerncentage = industryWisePerncentage;
  }

  public String getIndustryWisePerncentage()
  {
    return industryWisePerncentage;
  }

  public void setJobCode(String jobCode)
  {
    this.jobCode = jobCode;
  }

  public String getJobCode()
  {
    return jobCode;
  }

  public void setJobDescription(String jobDescription)
  {
    this.jobDescription = jobDescription;
  }

  public String getJobDescription()
  {
    return jobDescription;
  }

  public void setJobWisePercentage(String jobWisePercentage)
  {
    this.jobWisePercentage = jobWisePercentage;
  }

  public String getJobWisePercentage()
  {
    return jobWisePercentage;
  }

  public void setCollegeId(String collegeId)
  {
    this.collegeId = collegeId;
  }

  public String getCollegeId()
  {
    return collegeId;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay)
  {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay()
  {
    return collegeNameDisplay;
  }

  public void setSubjectWiseEmpRate(String subjectWiseEmpRate)
  {
    this.subjectWiseEmpRate = subjectWiseEmpRate;
  }

  public String getSubjectWiseEmpRate()
  {
    return subjectWiseEmpRate;
  }

  public void setLowerPerncentage(String lowerPerncentage)
  {
    this.lowerPerncentage = lowerPerncentage;
  }

  public String getLowerPerncentage()
  {
    return lowerPerncentage;
  }

  public void setHigherPercentage(String higherPercentage)
  {
    this.higherPercentage = higherPercentage;
  }

  public String getHigherPercentage()
  {
    return higherPercentage;
  }

  public void setIndustryInfoList(List industryInfoList)
  {
    this.industryInfoList = industryInfoList;
  }

  public List getIndustryInfoList()
  {
    return industryInfoList;
  }

  public void setJobInfoList(List jobInfoList)
  {
    this.jobInfoList = jobInfoList;
  }

  public List getJobInfoList()
  {
    return jobInfoList;
  }

  public void setUniversitiesEmpRateList(List universitiesEmpRateList)
  {
    this.universitiesEmpRateList = universitiesEmpRateList;
  }

  public List getUniversitiesEmpRateList()
  {
    return universitiesEmpRateList;
  }

  public void setTotalCount(String totalCount)
  {
    this.totalCount = totalCount;
  }

  public String getTotalCount()
  {
    return totalCount;
  }

  public void setSortByWhich(String sortByWhich)
  {
    this.sortByWhich = sortByWhich;
  }

  public String getSortByWhich()
  {
    return sortByWhich;
  }

  public void setSortByHow(String sortByHow)
  {
    this.sortByHow = sortByHow;
  }

  public String getSortByHow()
  {
    return sortByHow;
  }

  public void setSearchQualification(String searchQualification)
  {
    this.searchQualification = searchQualification;
  }

  public String getSearchQualification()
  {
    return searchQualification;
  }

  public void setSearchGrades(String searchGrades)
  {
    this.searchGrades = searchGrades;
  }

  public String getSearchGrades()
  {
    return searchGrades;
  }

  public void setQualSub6(String qualSub6)
  {
    this.qualSub6 = qualSub6;
  }

  public String getQualSub6()
  {
    return qualSub6;
  }

  public void setQualGrades6(String qualGrades6)
  {
    this.qualGrades6 = qualGrades6;
  }

  public String getQualGrades6()
  {
    return qualGrades6;
  }

  public void setQualUCASPoints1(String qualUCASPoints1)
  {
    this.qualUCASPoints1 = qualUCASPoints1;
  }

  public String getQualUCASPoints1()
  {
    return qualUCASPoints1;
  }

  public void setQualUCASPoints2(String qualUCASPoints2)
  {
    this.qualUCASPoints2 = qualUCASPoints2;
  }

  public String getQualUCASPoints2()
  {
    return qualUCASPoints2;
  }

  public void setQualUCASPoints3(String qualUCASPoints3)
  {
    this.qualUCASPoints3 = qualUCASPoints3;
  }

  public String getQualUCASPoints3()
  {
    return qualUCASPoints3;
  }

  public void setQualSubjName1(String qualSubjName1)
  {
    this.qualSubjName1 = qualSubjName1;
  }

  public String getQualSubjName1()
  {
    return qualSubjName1;
  }

  public void setQualSubjName2(String qualSubjName2)
  {
    this.qualSubjName2 = qualSubjName2;
  }

  public String getQualSubjName2()
  {
    return qualSubjName2;
  }

  public void setQualSubjName3(String qualSubjName3)
  {
    this.qualSubjName3 = qualSubjName3;
  }

  public String getQualSubjName3()
  {
    return qualSubjName3;
  }

  public void setQualSubjName4(String qualSubjName4)
  {
    this.qualSubjName4 = qualSubjName4;
  }

  public String getQualSubjName4()
  {
    return qualSubjName4;
  }

  public void setQualSubjName5(String qualSubjName5)
  {
    this.qualSubjName5 = qualSubjName5;
  }

  public String getQualSubjName5()
  {
    return qualSubjName5;
  }

  public void setQualSubjName6(String qualSubjName6)
  {
    this.qualSubjName6 = qualSubjName6;
  }

  public String getQualSubjName6()
  {
    return qualSubjName6;
  }

  public void setEquivalentTariffPoints(String equivalentTariffPoints)
  {
    this.equivalentTariffPoints = equivalentTariffPoints;
  }

  public String getEquivalentTariffPoints()
  {
    return equivalentTariffPoints;
  }

  public void setYearOfEntry(String yearOfEntry)
  {
    this.yearOfEntry = yearOfEntry;
  }

  public String getYearOfEntry()
  {
    return yearOfEntry;
  }

  public void setResultsHeading(String resultsHeading)
  {
    this.resultsHeading = resultsHeading;
  }

  public String getResultsHeading()
  {
    return resultsHeading;
  }

  public void setReturnCodeTwoHeading(String returnCodeTwoHeading)
  {
    this.returnCodeTwoHeading = returnCodeTwoHeading;
  }

  public String getReturnCodeTwoHeading()
  {
    return returnCodeTwoHeading;
  }

  public void setSeoSubjectsText(String seoSubjectsText)
  {
    this.seoSubjectsText = seoSubjectsText;
  }

  public String getSeoSubjectsText()
  {
    return seoSubjectsText;
  }

    public void setUniId(String uniId) {
        this.uniId = uniId;
    }

    public String getUniId() {
        return uniId;
    }

    public void setUniUkprn(String uniUkprn) {
        this.uniUkprn = uniUkprn;
    }

    public String getUniUkprn() {
        return uniUkprn;
    }

    public void setIntendedYearText(String intendedYearText) {
        this.intendedYearText = intendedYearText;
    }

    public String getIntendedYearText() {
        return intendedYearText;
    }
  public void setSchoolNameWCSID(String schoolNameWCSID) {
    this.schoolNameWCSID = schoolNameWCSID;
  }
  public String getSchoolNameWCSID() {
    return schoolNameWCSID;
  }
}
