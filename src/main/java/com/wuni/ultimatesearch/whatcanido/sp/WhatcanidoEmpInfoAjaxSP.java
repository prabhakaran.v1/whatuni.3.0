package com.wuni.ultimatesearch.whatcanido.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This is SP is used to get the employment info of Whatcanido Ultimate Search.
 */

public class WhatcanidoEmpInfoAjaxSP extends StoredProcedure
{
  public WhatcanidoEmpInfoAjaxSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql("WU_ULTIMATE_SEARCH_PKG.sort_whatcanIdo_empl_info_prc");
    declareParameter(new SqlParameter("p_ukprn", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_JACS_code", OracleTypes.VARCHAR));    
    declareParameter(new SqlParameter("P_QUAL_TYPE_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_TARIFF_POINT", OracleTypes.NUMBER));
      declareParameter(new SqlParameter("p_ultimate_srch_log_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_SORTBY_WHICH_COL_STR", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_SORTBY_HOW_STR", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_PAGE_NO", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("P_JACS_SUBJECT_NAME", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("pc_univ_emp_info", OracleTypes.CURSOR, new UniverstiesEmpRowMapperImpl()));
  }
  public Map execute(WhatCanIDoBean inputBean)
  {
    Map outMap = null;
    try{
    Map inMap = new HashMap();
    inMap.put("p_ukprn", inputBean.getUkprn());
    inMap.put("p_college_id", inputBean.getCollegeId());
    inMap.put("p_JACS_code", inputBean.getJacsCode());
    inMap.put("p_ultimate_srch_log_id", inputBean.getSaveLogId());    
    inMap.put("P_QUAL_TYPE_ID", inputBean.getSearchQual());
    inMap.put("P_TARIFF_POINT", null);
    inMap.put("P_SORTBY_WHICH_COL_STR", inputBean.getSortByWhich());
    inMap.put("P_SORTBY_HOW_STR", inputBean.getSortByHow());
    inMap.put("P_PAGE_NO", inputBean.getPageNo());
    inMap.put("P_JACS_SUBJECT_NAME", inputBean.getJacsSubject());    
    outMap = execute(inMap);
    }catch(Exception ex){
      ex.printStackTrace();
    }
    return outMap;
  }  
  
  private class UniverstiesEmpRowMapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      WhatCanIDoBean universitiesEmpVO = new WhatCanIDoBean();
      universitiesEmpVO.setCollegeId(rs.getString("college_id"));
      universitiesEmpVO.setUkprn(rs.getString("ukprn"));
      universitiesEmpVO.setCollegeName(rs.getString("college_name"));
      universitiesEmpVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      universitiesEmpVO.setTotalCount(rs.getString("total_cnt"));
      universitiesEmpVO.setSubjectWiseEmpRate(rs.getString("subject_wise_emp_rate"));
      universitiesEmpVO.setSalaryRange(rs.getString("salary_range"));
      return universitiesEmpVO;
    }
  }  
}