package com.wuni.ultimatesearch.whatcanido.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This SP is used to get the Sorting and Pagination of Employment info in Whancanido Ultimate Search.
 */

public class EmpRateSortPaginationSP extends StoredProcedure
{

    public EmpRateSortPaginationSP(DataSource datasource)
    {
      setDataSource(datasource);
      setSql("wu_ultimate_search_pkg.sort_whatcanido_empl_info_prc");
        declareParameter(new SqlParameter("p_ukprn", OracleTypes.VARCHAR));
        declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
      declareParameter(new SqlParameter("p_jacs_code", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_qual_type_id", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_tariff_point", OracleTypes.NUMBER));
      declareParameter(new SqlParameter("p_ultimate_srch_log_id", OracleTypes.NUMBER));
      declareParameter(new SqlParameter("p_sortby_which_col_str", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_sortby_how_str", OracleTypes.VARCHAR));
      declareParameter(new SqlParameter("p_page_no", OracleTypes.NUMBER));
      declareParameter(new SqlOutParameter("p_JACS_subject_name", OracleTypes.VARCHAR));
      declareParameter(new SqlOutParameter("pc_univ_emp_info", OracleTypes.CURSOR, new UniverstiesEmpRowMapperImpl()));
      
    }
    public Map execute(WhatCanIDoBean inputBean)
    {
      Map outMap = null;
      Map inMap = new HashMap();
        inMap.put("p_ukprn", inputBean.getUkprn());
        inMap.put("p_college_id", inputBean.getCollegeId());
      inMap.put("p_jacs_code", inputBean.getJacsCode());
      inMap.put("p_qual_type_id", null);
      inMap.put("p_tariff_point", null);
      inMap.put("p_ultimate_srch_log_id", inputBean.getSaveLogId());
      inMap.put("p_sortby_which_col_str", inputBean.getSortByWhich());
      inMap.put("p_sortby_how_str", inputBean.getSortByHow());
      inMap.put("p_page_no", inputBean.getPageNo());
      outMap = execute(inMap);
      return outMap;
    }  

    private class UniverstiesEmpRowMapperImpl implements RowMapper
    {
      public Object mapRow(ResultSet rs, int rowNum) throws SQLException
      {
      WhatCanIDoBean universitiesEmpVO = new WhatCanIDoBean();
        universitiesEmpVO.setCollegeId(rs.getString("college_id"));
        universitiesEmpVO.setCollegeNameDisplay(rs.getString("college_name_display"));
        universitiesEmpVO.setSubjectWiseEmpRate(rs.getString("subject_wise_emp_rate"));
        universitiesEmpVO.setSalaryRange(rs.getString("salary_range"));
        universitiesEmpVO.setUkprn(rs.getString("ukprn"));
        universitiesEmpVO.setTotalCount(rs.getString("total_cnt"));
        return universitiesEmpVO;
      }
    }


  }
