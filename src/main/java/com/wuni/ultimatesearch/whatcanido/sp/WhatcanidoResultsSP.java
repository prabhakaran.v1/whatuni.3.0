package com.wuni.ultimatesearch.whatcanido.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.ultimatesearch.SaveULInputbean;
import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;

/**
    * @IWANTTOBE Ultimate Search
    * @version 1.0
    * @since 3-JUNE-2014 Release
    * @purpose  This SP is used to get the results for given input of IWanttobe Ultimate Search(Page2).
    * Change Log
    * ***************************************************************************************************************************************************
    * Date           Name          Ver.       Changes desc                                                                  Rel Ver.
    * ***************************************************************************************************************************************************
    * 21-July-2015  Thiyagu G     1.1        Moved rowmapper class to common place for reuseing course search page            1.1
    * 08-mar-16     Indumathi.S   1.2        Places common row mapper specific to WCID
    */
public class WhatcanidoResultsSP extends StoredProcedure {

  public WhatcanidoResultsSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("wu_ultimate_search_pkg.get_whatcanido_results_prc");
    declareParameter(new SqlParameter("p_ultimate_srch_log_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_page_no", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_display_rec_cnt", OracleTypes.NUMBER)); //Added display record count for 21_July_2015, by Thiyagu G.
    declareParameter(new SqlOutParameter("pc_results", OracleTypes.CURSOR, new WcidWidgetSearchResultsRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_input_details", OracleTypes.CURSOR, new WcidWidgetInputDetailsRowmapper()));
    declareParameter(new SqlOutParameter("p_return_code", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("p_message_code", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("p_remaining_percent", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("p_results_grades", OracleTypes.VARCHAR));
  }

  public Map execute(WhatCanIDoBean inputBean) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("p_ultimate_srch_log_id", inputBean.getSaveLogId());
      inMap.put("p_page_no", inputBean.getPageNo());
      inMap.put("p_display_rec_cnt", inputBean.getDisplayRecCnt()); //Added display record count for 21_July_2015, by Thiyagu G.  
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }

  public class WcidWidgetSearchResultsRowMapperImpl implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      WhatCanIDoBean whatcanidoVO = new WhatCanIDoBean();
      whatcanidoVO.setJacsCode(rs.getString("jacs_code"));
      whatcanidoVO.setJacsSubject(rs.getString("jacs_subject"));
      whatcanidoVO.setJacsQualification(rs.getString("jacs_qualifications"));
      whatcanidoVO.setJacsPercentage(rs.getString("jacs_percentage"));
      whatcanidoVO.setWcidColor(rs.getString("colour"));
      whatcanidoVO.setSearchGrades(rs.getString("grade_url_string"));
      whatcanidoVO.setEquivalentTarriffPoints(rs.getString("equivalent_tariff_point"));
      return whatcanidoVO;
    }
  }
  
  public class WcidWidgetInputDetailsRowmapper implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      SaveULInputbean inputDetailsVO = new SaveULInputbean();
      inputDetailsVO.setQualification(rs.getString("qual_type_id"));
      inputDetailsVO.setQualSub1(rs.getString("subject1_id"));
      inputDetailsVO.setQualSub2(rs.getString("subject2_id"));
      inputDetailsVO.setQualSub3(rs.getString("subject3_id"));
      inputDetailsVO.setQualSub4(rs.getString("subject4_id"));
      inputDetailsVO.setQualSub5(rs.getString("subject5_id"));
      inputDetailsVO.setQualSub6(rs.getString("subject6_id"));
      inputDetailsVO.setQualSubDesc1(rs.getString("subject1_desc"));
      inputDetailsVO.setQualSubDesc2(rs.getString("subject2_desc"));
      inputDetailsVO.setQualSubDesc3(rs.getString("subject3_desc"));
      inputDetailsVO.setQualSubDesc4(rs.getString("subject4_desc"));
      inputDetailsVO.setQualSubDesc5(rs.getString("subject5_desc"));
      inputDetailsVO.setQualSubDesc6(rs.getString("subject6_desc"));
      inputDetailsVO.setQualGrades1(rs.getString("subj1_grade_or_points"));
      inputDetailsVO.setQualGrades2(rs.getString("subj2_grade_or_points"));
      inputDetailsVO.setQualGrades3(rs.getString("subj3_grade_or_points"));
      inputDetailsVO.setQualGrades4(rs.getString("subj4_grade_or_points"));
      inputDetailsVO.setQualGrades5(rs.getString("subj5_grade_or_points"));
      inputDetailsVO.setQualGrades6(rs.getString("subj6_grade_or_points"));
      inputDetailsVO.setUserId(rs.getString("user_id"));
      inputDetailsVO.setJobSocCode(rs.getString("job_soc_id"));
      inputDetailsVO.setJobDesc(rs.getString("job_desc"));
      inputDetailsVO.setIndustrySicCode(rs.getString("industry_sic_id"));
      inputDetailsVO.setIndustryDesc(rs.getString("industry_desc"));
      inputDetailsVO.setSchoolId(rs.getString("school_id"));
      inputDetailsVO.setSchoolName(rs.getString("school_name"));
      inputDetailsVO.setIntendedYear(rs.getString("year_of_entry"));
      inputDetailsVO.setCreatedDate(rs.getString("created_date"));
      inputDetailsVO.setWcidResultsFlag(rs.getString("result_flag"));
      return inputDetailsVO;
    }
  }

}
