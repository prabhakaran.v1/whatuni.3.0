package com.wuni.ultimatesearch.whatcanido.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This is SP is used to get the employment info of Whatcanido Ultimate Search.
 */

public class WhatcanidoEmpInfoSP extends StoredProcedure
{

  public WhatcanidoEmpInfoSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql("wu_ultimate_search_pkg.get_whatcanido_employ_info_prc");
    declareParameter(new SqlParameter("p_JACS_code", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_ultimate_srch_log_id", OracleTypes.NUMBER));
    declareParameter(new SqlOutParameter("pc_emp_info", OracleTypes.CURSOR, new EmploymentInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_industry_info", OracleTypes.CURSOR, new IndustryInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_job_info", OracleTypes.CURSOR, new JobInfoRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_univ_emp_info", OracleTypes.CURSOR, new UniverstiesEmpRowMapperImpl()));
    declareParameter(new SqlOutParameter("pc_ukprn_extra_details", OracleTypes.CURSOR, new UKPRNExtrainoRowmapperImpls()));
    

  }
  public Map execute(WhatCanIDoBean inputBean)
  {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_JACS_code", inputBean.getJacsCode());
    inMap.put("p_ultimate_srch_log_id", inputBean.getSaveLogId());
    outMap = execute(inMap);
    return outMap;
  }  
  
  private class EmploymentInfoRowMapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      WhatCanIDoBean employmentInfoVO = new WhatCanIDoBean();
      employmentInfoVO.setJacsSubGradEmpRate(rs.getString("jacs_subject_grad_emp_rate"));
      employmentInfoVO.setAllGradEmpRate(rs.getString("all_grad_emp_rate"));
      employmentInfoVO.setAllLowerSalary(rs.getString("sal_lower_percentile_univ"));
      employmentInfoVO.setAllHigherSalary(rs.getString("sal_higher_percentile_univ"));
      employmentInfoVO.setSubjectLowerSalary(rs.getString("sal_lower_percentile_subj"));
      employmentInfoVO.setSubjectHigherSalary(rs.getString("sal_higher_percentile_subj"));
      employmentInfoVO.setJacsSubject(rs.getString("jacs_subject"));
      return employmentInfoVO;
    }
  }

  private class IndustryInfoRowMapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      WhatCanIDoBean industryInfoVO = new WhatCanIDoBean();
      industryInfoVO.setIndustryCode(rs.getString("industry_code"));
      industryInfoVO.setIndustryDescription(rs.getString("industry_desc"));
      industryInfoVO.setIndustryWisePerncentage(rs.getString("industry_wise_percentage"));
      return industryInfoVO;
    }
  }

  private class JobInfoRowMapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      WhatCanIDoBean jobIndfoVO = new WhatCanIDoBean();
      jobIndfoVO.setJacsCode(rs.getString("job_code"));
      jobIndfoVO.setJobDescription(rs.getString("job_desc"));
      jobIndfoVO.setJobWisePercentage(rs.getString("job_wise_percentage"));
      return jobIndfoVO;
    }
  }


  private class UniverstiesEmpRowMapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      WhatCanIDoBean universitiesEmpVO = new WhatCanIDoBean();
      universitiesEmpVO.setCollegeId(rs.getString("college_id"));
      universitiesEmpVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      universitiesEmpVO.setSubjectWiseEmpRate(rs.getString("subject_wise_emp_rate"));
      universitiesEmpVO.setSalaryRange(rs.getString("salary_range"));
      universitiesEmpVO.setUkprn(rs.getString("ukprn"));
      universitiesEmpVO.setTotalCount(rs.getString("total_cnt"));
      return universitiesEmpVO;
    }
  }
  
  private class UKPRNExtrainoRowmapperImpls implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      WhatCanIDoBean industryInfoVO = new WhatCanIDoBean();
      return industryInfoVO;
    }
  }
  
}
