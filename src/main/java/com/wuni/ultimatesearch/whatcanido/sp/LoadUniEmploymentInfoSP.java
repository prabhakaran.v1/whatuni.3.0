package com.wuni.ultimatesearch.whatcanido.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.apache.commons.validator.GenericValidator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;

 /**
   * This SP is used to get the University employment info of What course i do(Page3).
   * @author
   * @version 1.0
   * @since 3-JUNE-2014 Release.
   * Change Log
   * *******************************************************************************************************************************
   * Date           Name                      Ver.     Changes desc                                                         Rel Ver.
   * *******************************************************************************************************************************
   * 08-Mar-2016    Prabhakaran V.            1.1      Page redesign                                                        wu_550
   *
   */
  
 public class LoadUniEmploymentInfoSP extends StoredProcedure {
   public LoadUniEmploymentInfoSP(DataSource datasource) {
     setDataSource(datasource);
     setSql(GlobalConstants.GET_JOB_INDUSTRY_INFO_PRC);
     declareParameter(new SqlParameter("p_ukprn", OracleTypes.VARCHAR));
     declareParameter(new SqlParameter("p_college_id", OracleTypes.VARCHAR));
     declareParameter(new SqlParameter("p_jacs_code", OracleTypes.VARCHAR));
     declareParameter(new SqlParameter("p_qual_type_id", OracleTypes.VARCHAR));
     declareParameter(new SqlParameter("p_tariff_point", OracleTypes.NUMBER));
     declareParameter(new SqlParameter("p_ultimate_srch_log_id", OracleTypes.NUMBER));
     declareParameter(new SqlOutParameter("p_view_degree_flag", OracleTypes.VARCHAR));
     declareParameter(new SqlOutParameter("pc_industry_info", OracleTypes.CURSOR, new IndustryInfoRowMapperImpl()));
     declareParameter(new SqlOutParameter("pc_job_info", OracleTypes.CURSOR, new JobInfoRowMapperImpl()));
   }
   public Map execute(WhatCanIDoBean inputBean) {
     Map outMap = null;
     Map inMap = new HashMap();
     inMap.put("p_ukprn", inputBean.getUkprn());
     inMap.put("p_college_id", inputBean.getCollegeId());
     inMap.put("p_jacs_code", inputBean.getJacsCode());
     inMap.put("p_qual_type_id", inputBean.getQualificationType());
     inMap.put("p_tariff_point", inputBean.getTariffPoint());
     inMap.put("p_ultimate_srch_log_id", inputBean.getSaveLogId());
     outMap = execute(inMap);
     return outMap;
   }
   private class IndustryInfoRowMapperImpl implements RowMapper {
     public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
       WhatCanIDoBean industryInfoVO = new WhatCanIDoBean();
       industryInfoVO.setIndustryCode(rs.getString("industry_code"));
       industryInfoVO.setIndustryDescription(rs.getString("industry_desc"));
       industryInfoVO.setIndustryWisePerncentage(rs.getString("industry_wise_percentage"));
       String donutColor = rs.getString("colour");
       if (!GenericValidator.isBlankOrNull(donutColor)) { //Hex color for donut
         industryInfoVO.setIndustryColour("grn".equalsIgnoreCase(donutColor)? "#00b260": "red".equalsIgnoreCase(donutColor)? "#FC7268": "org".equalsIgnoreCase(donutColor)? "#FF8B00": "");
       }
       return industryInfoVO;
     }
   }
   private class JobInfoRowMapperImpl implements RowMapper {
     public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
       WhatCanIDoBean jobIndfoVO = new WhatCanIDoBean();
       jobIndfoVO.setJobCode(rs.getString("job_code"));
       jobIndfoVO.setJobDescription(rs.getString("job_desc"));
       jobIndfoVO.setJobWisePercentage(rs.getString("job_wise_percentage"));
       String donutColor = rs.getString("colour");
       if (!GenericValidator.isBlankOrNull(donutColor)) { //Hex color for donut
         jobIndfoVO.setJobColour("grn".equalsIgnoreCase(donutColor)? "#00b260": "red".equalsIgnoreCase(donutColor)? "#FC7268": "org".equalsIgnoreCase(donutColor)? "#FF8B00": "");
       }
       return jobIndfoVO;
     }
   }
 }
