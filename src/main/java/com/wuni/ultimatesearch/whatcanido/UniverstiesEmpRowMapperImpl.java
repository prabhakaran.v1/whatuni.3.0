package com.wuni.ultimatesearch.whatcanido;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import WUI.utilities.CommonFunction;

import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;

public class UniverstiesEmpRowMapperImpl implements RowMapper
  {
    CommonFunction common = new CommonFunction();
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      WhatCanIDoBean universitiesEmpVO = new WhatCanIDoBean();
      universitiesEmpVO.setCollegeId(rs.getString("college_id"));
      String searchQual = null;
      universitiesEmpVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      universitiesEmpVO.setCollegeName(rs.getString("college_name"));
      universitiesEmpVO.setSubjectWiseEmpRate(rs.getString("subject_wise_emp_rate"));
      universitiesEmpVO.setLowerPerncentage(rs.getString("lower_percentile"));
      universitiesEmpVO.setHigherPercentage(rs.getString("upper_percentile"));
      universitiesEmpVO.setUkprn(rs.getString("ukprn"));
      universitiesEmpVO.setTotalCount(rs.getString("total_cnt"));
      universitiesEmpVO.setSearchGrades(rs.getString("grade_url_string"));
      universitiesEmpVO.setSearchQual(rs.getString("qual_type_id"));
      if("AL".equalsIgnoreCase(universitiesEmpVO.getSearchQual())){
        searchQual = "a_level";
      }else if("H".equalsIgnoreCase(universitiesEmpVO.getSearchQual())){
        searchQual = "sqa_higher";
      }else if("AH".equalsIgnoreCase(universitiesEmpVO.getSearchQual())){
        searchQual = "sqa_adv";
      }else if("BTEC".equalsIgnoreCase(universitiesEmpVO.getSearchQual())){
        searchQual = "btec";
      }else if("TP".equalsIgnoreCase(universitiesEmpVO.getSearchQual())){
        searchQual = "ucas_points";
      }      
      universitiesEmpVO.setJacsCode(rs.getString("jacs_code"));
      universitiesEmpVO.setJacsSubject(rs.getString("jacs_subject_name"));
      StringBuffer providerURL = new StringBuffer();
      providerURL.append("/degrees/courses/degrees/courses/degree-university/");
      providerURL.append(common.replaceHypen(common.replaceURL(universitiesEmpVO.getJacsSubject())));
      providerURL.append("-courses-at-");
      providerURL.append(common.replaceHypen(common.replaceURL(universitiesEmpVO.getCollegeName())));
      providerURL.append("/0/m/0/united+kingdom/");
      providerURL.append(searchQual);
      providerURL.append("/");
      providerURL.append(universitiesEmpVO.getSearchGrades());
      providerURL.append("/");
      providerURL.append(universitiesEmpVO.getCollegeId());
      providerURL.append("/csearch.html");
      universitiesEmpVO.setProviderSearcURL(providerURL.toString().toLowerCase());
      return universitiesEmpVO;
    }
  }