package com.wuni.ultimatesearch;

import java.util.ArrayList;
import java.util.List;

/**
 * @IWANTTOBE Ultimate Search
 * 3-JUNE-2014 Release.
 * @Purpose: This is used as input bean of Save the Ultimate Search inputs given by User. And also used Value Object of to get the user and input details.
 */

public class SaveULInputbean
{
  private String ulSearchType = null;
  private String qualification = null;
  private String qualValue = null;
  private String qualDesc = null;
  private List qualList = new ArrayList();
  private String qualSub1 = null;
  private String qualSub2 = null;
  private String qualSub3 = null;
  private String qualSub4 = null;
  private String qualSub5 = null;
  private String qualSub6 = null;
  private String qualSubDesc1 = null;
  private String qualSubDesc2 = null;
  private String qualSubDesc3 = null;
  private String qualSubDesc4 = null;
  private String qualSubDesc5 = null;
  private String qualSubDesc6 = null;

  private String qualGrades1 = null;
  private String qualGrades2 = null;
  private String qualGrades3 = null;
  private String qualGrades4 = null;
  private String qualGrades5 = null;
  private String qualGrades6 = null;

  private List qualGradeList = new ArrayList();
  private String schoolId = null;
  private String schoolName = null;
  private String schoolNameFreeText = null;
  private String schoolNameWCSID = null;
  private String schoolNameFreeTextWCSID = null;  
  private String intendedYear = null;
  private String intendedYearWCSID = null;
  private String schoolCheckFlag = null;
  private String userId = null;
  private String saveLogId = null;
  private String jobSocCode = null;
  private String industrySicCode = null;
  private String pageNo = null;
  private String jobDesc = null;
  private String industryDesc = null;
  
  //Popular Examples Whatcanido and IWanttobe
  private String jobCode = null;
  private String jobName = null;
  private String jacsSubjectCode1 = null;
  private String jacsSubjectName1 = null;
  private String jacsSubjectPercent1 = null;
  private String jacsSubjectCode2 = null;
  private String jacsSubjectName2 = null;
  private String jacsSubjectPercent2 = null;
  private String jacsSubjectCode3 = null;
  private String jacsSubjectName3 = null;
  private String jacsSubjectPercent3 = null;
  private String trackSessionId = null;  //Added tracking_session_id for 21_July_2015, by Thiyagu G.
  private String createdDate = null;
  
  private String wcidResultsFlag = null;

  public void setQualification(String qualification)
  {
    this.qualification = qualification;
  }

  public String getQualification()
  {
    return qualification;
  }

  public void setQualValue(String qualValue)
  {
    this.qualValue = qualValue;
  }

  public String getQualValue()
  {
    return qualValue;
  }

  public void setQualDesc(String qualDesc)
  {
    this.qualDesc = qualDesc;
  }

  public String getQualDesc()
  {
    return qualDesc;
  }

  public void setQualList(List qualList)
  {
    this.qualList = qualList;
  }

  public List getQualList()
  {
    return qualList;
  }

  public void setQualSub1(String qualSub1)
  {
    this.qualSub1 = qualSub1;
  }

  public String getQualSub1()
  {
    return qualSub1;
  }

  public void setQualSub2(String qualSub2)
  {
    this.qualSub2 = qualSub2;
  }

  public String getQualSub2()
  {
    return qualSub2;
  }

  public void setQualSub3(String qualSub3)
  {
    this.qualSub3 = qualSub3;
  }

  public String getQualSub3()
  {
    return qualSub3;
  }

  public void setQualSub4(String qualSub4)
  {
    this.qualSub4 = qualSub4;
  }

  public String getQualSub4()
  {
    return qualSub4;
  }

  public void setQualSub5(String qualSub5)
  {
    this.qualSub5 = qualSub5;
  }

  public String getQualSub5()
  {
    return qualSub5;
  }

  public void setQualSub6(String qualSub6)
  {
    this.qualSub6 = qualSub6;
  }

  public String getQualSub6()
  {
    return qualSub6;
  }

  public void setQualGrades1(String qualGrades1)
  {
    this.qualGrades1 = qualGrades1;
  }

  public String getQualGrades1()
  {
    return qualGrades1;
  }

  public void setQualGrades2(String qualGrades2)
  {
    this.qualGrades2 = qualGrades2;
  }

  public String getQualGrades2()
  {
    return qualGrades2;
  }

  public void setQualGrades3(String qualGrades3)
  {
    this.qualGrades3 = qualGrades3;
  }

  public String getQualGrades3()
  {
    return qualGrades3;
  }

  public void setQualGrades4(String qualGrades4)
  {
    this.qualGrades4 = qualGrades4;
  }

  public String getQualGrades4()
  {
    return qualGrades4;
  }

  public void setQualGrades5(String qualGrades5)
  {
    this.qualGrades5 = qualGrades5;
  }

  public String getQualGrades5()
  {
    return qualGrades5;
  }

  public void setQualGrades6(String qualGrades6)
  {
    this.qualGrades6 = qualGrades6;
  }

  public String getQualGrades6()
  {
    return qualGrades6;
  }

  public void setQualGradeList(List qualGradeList)
  {
    this.qualGradeList = qualGradeList;
  }

  public List getQualGradeList()
  {
    return qualGradeList;
  }

  public void setSchoolId(String schoolId)
  {
    this.schoolId = schoolId;
  }

  public String getSchoolId()
  {
    return schoolId;
  }

  public void setSchoolName(String schoolName)
  {
    this.schoolName = schoolName;
  }

  public String getSchoolName()
  {
    return schoolName;
  }

  public void setSchoolNameFreeText(String schoolNameFreeText)
  {
    this.schoolNameFreeText = schoolNameFreeText;
  }

  public String getSchoolNameFreeText()
  {
    return schoolNameFreeText;
  }

  public void setIntendedYear(String intendedYear)
  {
    this.intendedYear = intendedYear;
  }

  public String getIntendedYear()
  {
    return intendedYear;
  }

  public void setSchoolCheckFlag(String schoolCheckFlag)
  {
    this.schoolCheckFlag = schoolCheckFlag;
  }

  public String getSchoolCheckFlag()
  {
    return schoolCheckFlag;
  }

  public void setUserId(String userId)
  {
    this.userId = userId;
  }

  public String getUserId()
  {
    return userId;
  }

  public void setSaveLogId(String saveLogId)
  {
    this.saveLogId = saveLogId;
  }

  public String getSaveLogId()
  {
    return saveLogId;
  }

  public void setJobSocCode(String jobSocCode)
  {
    this.jobSocCode = jobSocCode;
  }

  public String getJobSocCode()
  {
    return jobSocCode;
  }

  public void setIndustrySicCode(String industrySicCode)
  {
    this.industrySicCode = industrySicCode;
  }

  public String getIndustrySicCode()
  {
    return industrySicCode;
  }

  public void setPageNo(String pageNo)
  {
    this.pageNo = pageNo;
  }

  public String getPageNo()
  {
    return pageNo;
  }

  public void setUlSearchType(String ulSearchType)
  {
    this.ulSearchType = ulSearchType;
  }

  public String getUlSearchType()
  {
    return ulSearchType;
  }

  public void setQualSubDesc1(String qualSubDesc1)
  {
    this.qualSubDesc1 = qualSubDesc1;
  }

  public String getQualSubDesc1()
  {
    return qualSubDesc1;
  }

  public void setQualSubDesc2(String qualSubDesc2)
  {
    this.qualSubDesc2 = qualSubDesc2;
  }

  public String getQualSubDesc2()
  {
    return qualSubDesc2;
  }

  public void setQualSubDesc3(String qualSubDesc3)
  {
    this.qualSubDesc3 = qualSubDesc3;
  }

  public String getQualSubDesc3()
  {
    return qualSubDesc3;
  }

  public void setQualSubDesc4(String qualSubDesc4)
  {
    this.qualSubDesc4 = qualSubDesc4;
  }

  public String getQualSubDesc4()
  {
    return qualSubDesc4;
  }

  public void setQualSubDesc5(String qualSubDesc5)
  {
    this.qualSubDesc5 = qualSubDesc5;
  }

  public String getQualSubDesc5()
  {
    return qualSubDesc5;
  }

  public void setQualSubDesc6(String qualSubDesc6)
  {
    this.qualSubDesc6 = qualSubDesc6;
  }

  public String getQualSubDesc6()
  {
    return qualSubDesc6;
  }

  public void setJobDesc(String jobDesc)
  {
    this.jobDesc = jobDesc;
  }

  public String getJobDesc()
  {
    return jobDesc;
  }

  public void setIndustryDesc(String industryDesc)
  {
    this.industryDesc = industryDesc;
  }

  public String getIndustryDesc()
  {
    return industryDesc;
  }

  public void setJacsSubjectCode1(String jacsSubjectCode1)
  {
    this.jacsSubjectCode1 = jacsSubjectCode1;
  }

  public String getJacsSubjectCode1()
  {
    return jacsSubjectCode1;
  }

  public void setJacsSubjectName1(String jacsSubjectName1)
  {
    this.jacsSubjectName1 = jacsSubjectName1;
  }

  public String getJacsSubjectName1()
  {
    return jacsSubjectName1;
  }

  public void setJacsSubjectCode2(String jacsSubjectCode2)
  {
    this.jacsSubjectCode2 = jacsSubjectCode2;
  }

  public String getJacsSubjectCode2()
  {
    return jacsSubjectCode2;
  }

  public void setJacsSubjectName2(String jacsSubjectName2)
  {
    this.jacsSubjectName2 = jacsSubjectName2;
  }

  public String getJacsSubjectName2()
  {
    return jacsSubjectName2;
  }

  public void setJacsSubjectCode3(String jacsSubjectCode3)
  {
    this.jacsSubjectCode3 = jacsSubjectCode3;
  }

  public String getJacsSubjectCode3()
  {
    return jacsSubjectCode3;
  }

  public void setJacsSubjectName3(String jacsSubjectName3)
  {
    this.jacsSubjectName3 = jacsSubjectName3;
  }

  public String getJacsSubjectName3()
  {
    return jacsSubjectName3;
  }

  public void setJacsSubjectPercent1(String jacsSubjectPercent1)
  {
    this.jacsSubjectPercent1 = jacsSubjectPercent1;
  }

  public String getJacsSubjectPercent1()
  {
    return jacsSubjectPercent1;
  }

  public void setJacsSubjectPercent2(String jacsSubjectPercent2)
  {
    this.jacsSubjectPercent2 = jacsSubjectPercent2;
  }

  public String getJacsSubjectPercent2()
  {
    return jacsSubjectPercent2;
  }

  public void setJacsSubjectPercent3(String jacsSubjectPercent3)
  {
    this.jacsSubjectPercent3 = jacsSubjectPercent3;
  }

  public String getJacsSubjectPercent3()
  {
    return jacsSubjectPercent3;
  }

  public void setJobCode(String jobCode)
  {
    this.jobCode = jobCode;
  }

  public String getJobCode()
  {
    return jobCode;
  }

  public void setJobName(String jobName)
  {
    this.jobName = jobName;
  }

  public String getJobName()
  {
    return jobName;
  }
  public void setSchoolNameWCSID(String schoolNameWCSID) {
    this.schoolNameWCSID = schoolNameWCSID;
  }
  public String getSchoolNameWCSID() {
    return schoolNameWCSID;
  }
  public void setSchoolNameFreeTextWCSID(String schoolNameFreeTextWCSID) {
    this.schoolNameFreeTextWCSID = schoolNameFreeTextWCSID;
  }
  public String getSchoolNameFreeTextWCSID() {
    return schoolNameFreeTextWCSID;
  }
  public void setIntendedYearWCSID(String intendedYearWCSID) {
    this.intendedYearWCSID = intendedYearWCSID;
  }
  public String getIntendedYearWCSID() {
    return intendedYearWCSID;
  }
  public void setTrackSessionId(String trackSessionId) {
    this.trackSessionId = trackSessionId;
  }
  public String getTrackSessionId() {
    return trackSessionId;
  }
  public void setCreatedDate(String createdDate) {
    this.createdDate = createdDate;
  }
  public String getCreatedDate() {
    return createdDate;
  }

  public void setWcidResultsFlag(String wcidResultsFlag) {
    this.wcidResultsFlag = wcidResultsFlag;
  }

  public String getWcidResultsFlag() {
    return wcidResultsFlag;
  }

}
