package com.wuni.rss;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import spring.valueobject.articles.RssBlogArticleVO;
import com.layer.util.SpringConstants;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Class         : RssBlogArticleAction.java
 *
 * Description   : Action class used to generate rss file for all article content channel
 *
 * @version      : 1.0
 *
 * Change Log :
 * **************************************************************************************************************
 * author	 Ver 	 Modified On 	Modification Details 	Change
 * **************************************************************************************************************
 * Amirtharaj.A  1.0       14-12-2010       First draft   		
 */

@Controller
public class RssBlogArticleController {
  ICommonBusiness commonBusiness = null;
  @RequestMapping(value = {"/domrssblogarticle"})
  public ModelAndView getClearingHome(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception{
    commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    String forwardString = "success";
    RssBlogArticleVO rssBlogArticleVO = new RssBlogArticleVO();
    try {
      rssBlogArticleVO = commonBusiness.rssArticleBlogXml(rssBlogArticleVO);
      if (rssBlogArticleVO.getRssXMLList() != null) {
        Iterator rssIterator = rssBlogArticleVO.getRssXMLList().iterator();
        //Delete Dir and all the file contained in tht path
        //String dirPath = "D:/java write";
        String dirPath = "/wu-cont/domrss";
        boolean dirDelete = deleteDir(new File(dirPath));
        //Create Dir if doesn't exist
        boolean dirCreation = (new File(dirPath)).mkdir();
        if (dirCreation) {
          while (rssIterator.hasNext()) {
            RssBlogArticleVO rssIter = (RssBlogArticleVO)rssIterator.next();
            File file = new File(dirPath + "/" + rssIter.getChannel().toLowerCase().replaceAll(" ","-") + ".xml");
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF8"));
            out.write(rssIter.getRssXMLData());
            out.close();
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new ModelAndView(forwardString);
  }
  private boolean deleteDir(File dir) {
    if (dir.isDirectory()) {
      String[] children = dir.list();
      for (int i = 0; i < children.length; i++) {
        boolean success = deleteDir(new File(dir, children[i]));
        if (!success) {
          return false;
        }
      }
    }
    return dir.delete();
  }
  public void setCommonBusiness(ICommonBusiness commonBusiness) {
    this.commonBusiness = commonBusiness;
  }
  public ICommonBusiness getCommonBusiness() {
    return commonBusiness;
  }
}
