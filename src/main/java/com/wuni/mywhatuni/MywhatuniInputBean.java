package com.wuni.mywhatuni;

public class MywhatuniInputBean
{
  private String userId = null;
  private String tabName = null;
  private String networkId = null;
  private String pageNo = null;
  private String orderBy = null;
  private String cookieBasketId = null;

  public void setUserId(String userId)
  {
    this.userId = userId;
  }

  public String getUserId()
  {
    return userId;
  }

  public void setTabName(String tabName)
  {
    this.tabName = tabName;
  }

  public String getTabName()
  {
    return tabName;
  }

  public void setNetworkId(String networkId)
  {
    this.networkId = networkId;
  }

  public String getNetworkId()
  {
    return networkId;
  }

  public void setPageNo(String pageNo)
  {
    this.pageNo = pageNo;
  }

  public String getPageNo()
  {
    return pageNo;
  }

  public void setOrderBy(String orderBy)
  {
    this.orderBy = orderBy;
  }

  public String getOrderBy()
  {
    return orderBy;
  }

  public void setCookieBasketId(String cookieBasketId)
  {
    this.cookieBasketId = cookieBasketId;
  }

  public String getCookieBasketId()
  {
    return cookieBasketId;
  }

}
