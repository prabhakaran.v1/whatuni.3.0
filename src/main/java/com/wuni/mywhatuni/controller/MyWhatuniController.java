package com.wuni.mywhatuni.controller;

import WUI.registration.bean.UserLoginBean;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.SessionData;
import spring.valueobject.gradefilter.SRGradeFilterVO;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.advertiser.ql.form.QuestionAnswerBean;
import com.wuni.mywhatuni.MywhatuniCommon;
import com.wuni.mywhatuni.MywhatuniInputBean;
import com.wuni.mywhatuni.form.MyWhatuniBean;
import com.wuni.mywhatuni.validator.ValidateFormElementsValidator;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.mywhatuni.MyWhatuniGetdataVO;
import com.wuni.util.valueobject.mywhatuni.MyWhatuniInputVO;
import com.wuni.valueobjects.whatunigo.UserQualDetailsVO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.util.LabelValueBean;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * contains class for MyWhatuni page
 *
 * Modification history:
 * **************************************************************************************************************
 * Author           Relase Date              Modification Details
 * **************************************************************************************************************
 * Thiyagu G        19_05_2015               Retained existing attribute models and removed existing unused cursors and then added required cursors alone.
 * Indumathi.S      Jan-27-16                Modified code for saving mailing preferences
 * Hema.S           Dec-17-19                Added consent flag cursor
 * Sangeeth.s		July-23-20				 Added download now button hide during failover
 * Sujitha V        10_NOV_2020              Added session for YOE to log in d_session_log.
 */

@Controller
public class MyWhatuniController{
  
  @RequestMapping(value = "/mywhatuni", method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView myWhatUni(ModelMap model,@ModelAttribute("mywhatunibean") MyWhatuniBean mybean, BindingResult result ,HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    ServletContext context = request.getServletContext();
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    if (new CommonFunction().checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    try {
      if (!(new SecurityEvaluator().checkSecurity("REGISTERED_USER", request, response))) {
        String fromUrl = "/mywhatuni.html";
        if (session.getAttribute("fromUrl") == null) {
          session.setAttribute("fromUrl", fromUrl);
        }
        //Added below code to update ca track flag when the user come to time to login page with ca track flag for 04-09-2016, By Thiyagu G.
        if(!GenericValidator.isBlankOrNull(request.getParameter("token")) && !GenericValidator.isBlankOrNull(request.getParameter("fromca"))){          
          MyWhatuniInputVO inputBean = new MyWhatuniInputVO();
          inputBean.setUserId(request.getParameter("token"));
          inputBean.setCaTrackFlag(request.getParameter("fromca"));
          Map caTrackMap = commonBusiness.updateCaTrackFlag(inputBean);        
          int retCode = Integer.parseInt(caTrackMap.get("P_RET_CODE").toString());
          String errMsg = (String)caTrackMap.get("P_ERR_MSG");
          if (retCode == 1) {
            response.sendError(404, "404 error message");            
            return null;
          }
        }
        return new ModelAndView("loginPage", "userLoginBean", new UserLoginBean());
      }
    } catch (Exception secutityException) {
      secutityException.printStackTrace();
    }
    String userId = new SessionData().getData(request, "y");
    String imageName = "";    
    new CommonFunction().loadUserLoggedInformation(request, context, response);    
    if (request.getParameter("butvalue") != null && request.getParameter("butvalue").equalsIgnoreCase("Save changes")) {
	 ValidateFormElementsValidator validator =  new ValidateFormElementsValidator();
	 validator.validate(mybean, result);
      if (!result.hasErrors()) {
        if(!GenericValidator.isBlankOrNull(mybean.getDate()) && !GenericValidator.isBlankOrNull(mybean.getMonth()) && !GenericValidator.isBlankOrNull(mybean.getYear())){
        mybean.setDateOfBirth(mybean.getDate() + "/" + mybean.getMonth() + "/" + mybean.getYear());
        }
        new MywhatuniCommon().userDataUpdataion(userId, mybean, request, response);
        ArrayList userAttributeList = null;
        String userStudyLevel = mybean.getUserStudyLevel();
        String userAttributeType = "";
        if (!GenericValidator.isBlankOrNull(userStudyLevel)) {
          if ("453".equalsIgnoreCase(userStudyLevel)) {
            userAttributeList = (ArrayList)mybean.getPgQuestionAnswers();
            userAttributeType = "PG";
          } else {
            userAttributeList = (ArrayList)mybean.getUgQuestionAnswers();
            userAttributeType = "UG";
          }
        }
        //
        String attributeId = "";
        String attributeValue = "";
        String orderAttributeId = "";
        String optionId = "";
        String attributeValueSeq = "";
        String attributeValueId = "";
        int listSize = userAttributeList.size();
        int extraRecords = 0;
        for (int i = 0; i < listSize; i++) {
          QuestionAnswerBean bean = (QuestionAnswerBean)userAttributeList.get(i);
          if (!GenericValidator.isBlankOrNull(bean.getAttributeId())) {
            if (!"HTML".equalsIgnoreCase(bean.getFormatDesc())) {
              extraRecords++;
              if ("CHECKBOX".equalsIgnoreCase(bean.getFormatDesc())) {
                if ("Y".equalsIgnoreCase(bean.getOptionFlag())) {
                  String checkedValues[] = request.getParameterValues("CHKB_" + bean.getAttributeName());
                  if (checkedValues != null) {
                    int checedValuesCount = checkedValues.length;
                    checedValuesCount = checedValuesCount - 1;
                    extraRecords += checedValuesCount;
                  }
                }
              }
            }
          }
        }
        Object[][] attrValues = new Object[extraRecords][6];
        for (int i = 0, x = 0; i < listSize; i++) {
          QuestionAnswerBean bean = (QuestionAnswerBean)userAttributeList.get(i);
          attributeValueId = GenericValidator.isBlankOrNull(bean.getAttributeValueId())? null: bean.getAttributeValueId();
          orderAttributeId = GenericValidator.isBlankOrNull(bean.getOrderAttributeId())? null: bean.getOrderAttributeId();
          attributeValueSeq = GenericValidator.isBlankOrNull(bean.getAttrValueSeq())? null: bean.getAttrValueSeq();
          attributeId = bean.getAttributeId();
          optionId = null;
          attributeValue = null;
          if (!GenericValidator.isBlankOrNull(bean.getAttributeId())) {
            if (!"HTML".equalsIgnoreCase(bean.getFormatDesc())) {
              if ("CHECKBOX".equalsIgnoreCase(bean.getFormatDesc())) {
                if ("Y".equalsIgnoreCase(bean.getOptionFlag())) {
                  String checkedValues[] = request.getParameterValues("CHKB_" + bean.getAttributeName());
                  if (checkedValues != null) {
                    for (int k = 0; k < checkedValues.length; k++, x++) {
                      attrValues[x][0] = attributeValueId;
                      attrValues[x][1] = orderAttributeId;
                      attrValues[x][2] = attributeId;
                      attrValues[x][3] = attributeValue;
                      attrValues[x][4] = checkedValues[k];
                      attrValues[x][5] = String.valueOf(k + 1);
                    }
                  } else {
                    attrValues[x][0] = attributeValueId;
                    attrValues[x][1] = orderAttributeId;
                    attrValues[x][2] = attributeId;
                    attrValues[x][3] = attributeValue;
                    attrValues[x][4] = optionId;
                    attrValues[x][5] = attributeValueSeq;
                    x++;
                  }
                } else {
                  if (!GenericValidator.isBlankOrNull(bean.getAttributeValue())) {
                    attributeValue = bean.getAttributeValue();
                  }
                  attrValues[x][0] = attributeValueId;
                  attrValues[x][1] = orderAttributeId;
                  attrValues[x][2] = attributeId;
                  attrValues[x][3] = attributeValue;
                  attrValues[x][4] = optionId;
                  attrValues[x][5] = attributeValueSeq;
                  x++;
                }
              } else {
                if (!GenericValidator.isBlankOrNull(bean.getAttributeValue()) && !bean.getAttributeValue().equals(bean.getDefaultValue())) {
                  if ("Y".equals(bean.getOptionFlag())) {
                    optionId = bean.getAttributeValue();
                  } else {
                    attributeValue = bean.getAttributeValue();
                  }
                } else {
                  attributeValue = null;
                }
                attrValues[x][0] = attributeValueId;
                attrValues[x][1] = orderAttributeId;
                attrValues[x][2] = attributeId;
                attrValues[x][3] = attributeValue;
                attrValues[x][4] = optionId;
                attrValues[x][5] = attributeValueSeq;
                x++;
              }
            }
          }
        }
        //Added by Indumathi.S Jan-27-16 for mailing preference
        MyWhatuniInputVO inputBean = new MyWhatuniInputVO();
        inputBean.setUserId(userId);
        inputBean.setUserAttributeType(userAttributeType);
        inputBean.setReceiveNoEmailFlag(request.getParameter("receiveNoEmailFlag"));
        inputBean.setSolusEmail(request.getParameter("solusEmail"));
        inputBean.setPermitEmail(request.getParameter("permitEmail"));
        inputBean.setMarkettingEmail(request.getParameter("markettingEmail"));
        inputBean.setSurveyMail(request.getParameter("surveyMail")); //Added survey mail flag in mailing preferences for 31_May_2016, By Thiyagu G.
        inputBean.setPrivacy(request.getParameter("privacy"));
        inputBean.setCaTrackFlag(request.getParameter("catPrivacy"));        
        inputBean.setReviewSurveyFlag(request.getParameter("reviewSurvey"));       
        Map mywhatuniAttrMap = commonBusiness.myWhatuniUserAttrValues(inputBean, attrValues);
        String successFlag = (String)mywhatuniAttrMap.get("p_flag");
        if ("SUCCESS".equalsIgnoreCase(successFlag)) {
          request.setAttribute("updatesuccess", "updatesuccess");
        }//End
        ArrayList userInfoList = new CommonFunction().getUserInformation(new SessionData().getData(request, "y"), request, context);
        if (userInfoList != null && userInfoList.size() > 0) {
          session.setAttribute("userInfoList", userInfoList);
        }
      } else {
        MyWhatuniBean formBean = new MyWhatuniBean();
        BeanUtils.copyProperties(mybean, formBean);
        mywhatuniData(request, response, formBean);
        dobMethod(mybean);
        mybean.setNationalityList(formBean.getNationalityList());
        mybean.setCounttyOfResList(formBean.getCounttyOfResList());
        mybean.setGradeTypeList(formBean.getGradeTypeList());
        mybean.setUgQuestionAnswers(formBean.getUgQuestionAnswers());
        mybean.setPgQuestionAnswers(formBean.getPgQuestionAnswers());
        mybean.setClassificcationList(formBean.getClassificcationList());
        mybean.setEnquiryCount(formBean.getEnquiryCount());
        mybean.setProspectusCount(formBean.getProspectusCount());
        mybean.setOpenDayCount(formBean.getOpenDayCount());
        mybean.setBasketContentCount(formBean.getBasketContentCount());
        mybean.setProfileComplete(formBean.getProfileComplete());
        request.setAttribute("expanderror", "expanderror");
        request.setAttribute("mywhatunipage", "mywhatunipage");
        return null;
      }
    }
    if (request.getParameter("butValue") != null && request.getParameter("butValue").equalsIgnoreCase("imageupload")) {
      request.setAttribute("imagePath", request.getParameter("data"));
      return new ModelAndView("userimageupload");
    }
    if (request.getParameter("butValue") != null && request.getParameter("butValue").equalsIgnoreCase("AjaxUpload") && request.getParameter("imageCropFlag") != null) {
      imageName = mybean.getUserPhotoUpload().getOriginalFilename();
      if (!GenericValidator.isBlankOrNull(imageName)) {
        if (mybean.getUserPhotoUpload().getSize() > 2097152) { //2.0 MB         
          request.setAttribute("imageuploaderror", "imageuploaderror");        
        } else {
          String imagePath = new MywhatuniCommon().uploadBrowseImageToWebserver(mybean, userId, request);
          if (request.getParameter("imageCropFlag").equalsIgnoreCase("Yes")) {
            if (!GenericValidator.isBlankOrNull(imagePath)) {
              setResponseText(response, imagePath);
            }
          } else {
            Map userImgMap = null;
            if (imagePath != null) {
              imagePath = new CommonFunction().getWUSysVarValue("USER_IMAGE_UPLOAD_PATH") + imagePath;
              userImgMap = commonBusiness.updateUserImageDetails(userId, imagePath);
              String pFLAG = (String)userImgMap.get("P_FLAG");
              if (!GenericValidator.isBlankOrNull(pFLAG)) {
                setResponseText(response, imagePath);
                ArrayList userInfoList = new CommonFunction().getUserInformation(new SessionData().getData(request, "y"), request, context);
                if (userInfoList != null && userInfoList.size() > 0) {
                  session.setAttribute("userInfoList", userInfoList);
                }
              }
            }
          }
        }
      }
      return null;
    }
    if (request.getParameter("butValue") != null && request.getParameter("butValue").equalsIgnoreCase("cropprdImage")) {
      String croppedImage = "";
      if (request.getParameter("x") != null && request.getParameter("y") != null && request.getParameter("w") != null && request.getParameter("h") != null && request.getParameter("orgImagePath") != null) {
        int x = Integer.parseInt(request.getParameter("x"));
        int y = Integer.parseInt(request.getParameter("y"));
        int w = Integer.parseInt(request.getParameter("w"));
        int h = Integer.parseInt(request.getParameter("h"));
        String orgImagePath = request.getParameter("orgImagePath");
        croppedImage = cropImage(x, y, w, h, orgImagePath);
      }
      Map userImgMap = null;
      if (croppedImage != null) {
        userImgMap = commonBusiness.updateUserImageDetails(userId, croppedImage);
        String pFLAG = (String)userImgMap.get("P_FLAG");
        if (!GenericValidator.isBlankOrNull(pFLAG)) {
          request.setAttribute("croppedImage", croppedImage);
          ArrayList userInfoList = new CommonFunction().getUserInformation(new SessionData().getData(request, "y"), request, context);
          if (userInfoList != null && userInfoList.size() > 0) {
            session.setAttribute("userInfoList", userInfoList);
          }
        }
      }
      mywhatuniData(request, response, mybean);
      request.setAttribute("mywhatunipage", "mywhatunipage");
      request.setAttribute("parentwindowclose", "YES");

      request.setAttribute("curActiveMenu", "mywhatuni");
      request.setAttribute("insightPageFlag", "yes");
      request.setAttribute("showResponsiveTimeLine","true");
      return new ModelAndView("mywhatuni", "mywhatunibean", mybean);
    }
    if (request.getParameter("butvalue") != null && request.getParameter("butvalue").equalsIgnoreCase("Save profile")) {
      new CommonFunction().getUserTimeLineTrackPod(request); //08_OCT_2014 Added by Amir for updating userTimeline
    }
    mywhatuniData(request, response, mybean);
    request.setAttribute("mywhatunipage", "mywhatunipage");    
    request.setAttribute("curActiveMenu", "mywhatuni");
    request.setAttribute("insightPageFlag", "yes"); //28_OCT_2014 Added by Amir for insight       
    request.setAttribute("showResponsiveTimeLine","true");
    request.setAttribute("pageName", "mywhatuni");
    //Added by Indumathi.S Jan-27-16 to save details and redirect to another page
    String redirectingURL = request.getParameter("mail_a_href");
    if(!GenericValidator.isBlankOrNull(redirectingURL)) {
      response.sendRedirect(redirectingURL);
    }//End
    return new ModelAndView("mywhatuni", "mywhatunibean", mybean);
  }
  private void mywhatuniData(HttpServletRequest request, HttpServletResponse response, MyWhatuniBean mybean) {
	  String userId = new SessionData().getData(request, "y");
    String tabName = request.getParameter("tabname");
    String mainTab = null;
    if (tabName == null || "".equalsIgnoreCase(tabName)) {
      tabName = "MD";
    }
    if ("MD".equalsIgnoreCase(tabName)) {
      mainTab = "MMD"; // My Details Tab.      
    } else if ("EN".equalsIgnoreCase(tabName) || "PR".equalsIgnoreCase(tabName) || "OD".equalsIgnoreCase(tabName) || "BA".equalsIgnoreCase(tabName)) {
      mainTab = "MCST"; //My Course Search Tab
      if ("EN".equalsIgnoreCase(tabName)) { //28_OCT_2014 Added by Amir for insight
        request.setAttribute("getInsightName", "myenquiry");
      } else if ("PR".equalsIgnoreCase(tabName)) {
        request.setAttribute("getInsightName", "myprospectus");
      } else if ("OD".equalsIgnoreCase(tabName)) {
        request.setAttribute("getInsightName", "myopendays");
      }
    } else {
      mainTab = "MCC"; //My Compare Customisize Tab
    }
    request.setAttribute("maintab", mainTab);
    String pageNo = "1";
    String page = request.getParameter("pageno");
    pageNo = page != null && !"".equalsIgnoreCase(page)? page: pageNo;
    request.setAttribute("pageno", pageNo);
    request.setAttribute("tabname", tabName);
    if ("BA".equalsIgnoreCase(tabName)) {
      if (!GenericValidator.isBlankOrNull(userId) && !"0".equalsIgnoreCase(userId)) {
        request.setAttribute("comparegapagename", "Final5");
        request.setAttribute("getInsightName", "Final5"); //28_OCT_2014 Added by Amir for insight
      } else {
        request.setAttribute("comparegapagename", "Final5");
      }
    }
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = "2";
    }
    String enquiryOrderBy = request.getParameter("enqOrder");
    if (enquiryOrderBy == null || "".equalsIgnoreCase(enquiryOrderBy)) {
      enquiryOrderBy = "DATE_ORDER";
    }
    request.setAttribute("enqOrder", enquiryOrderBy);
    request.getSession().setAttribute("cpeQualificationNetworkId", cpeQualificationNetworkId);
    MywhatuniInputBean inputBean = new MywhatuniInputBean();
    String cookieBasketId = new CommonFunction().checkCookieStatus(request);
    inputBean.setUserId(userId);
    inputBean.setTabName(tabName);
    inputBean.setNetworkId(cpeQualificationNetworkId);
    inputBean.setPageNo(pageNo);
    inputBean.setOrderBy(enquiryOrderBy);
    inputBean.setCookieBasketId(cookieBasketId);
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    Map mywhatuniDataMap = commonBusiness.getMyWhatuniData(inputBean);
    List userDetailsList = new ArrayList();
    List userAddressDetailsList = new ArrayList();
    List userUGAttrDetailsList = new ArrayList();
    List userPGAttrDetailsList = new ArrayList();
    userDetailsList = (ArrayList)mywhatuniDataMap.get("pc_user_details");
    userAddressDetailsList = (ArrayList)mywhatuniDataMap.get("pc_user_address");
    userUGAttrDetailsList = (ArrayList)mywhatuniDataMap.get("pc_ug_user_attr_fields");
    userPGAttrDetailsList = (ArrayList)mywhatuniDataMap.get("pc_pg_user_attr_fields");
    ArrayList userConfirmedList = (ArrayList)mywhatuniDataMap.get("PC_USER_CONFIRMED_OFFER");
    ArrayList<UserQualDetailsVO> userQualList = (ArrayList<UserQualDetailsVO>)mywhatuniDataMap.get("PC_USER_QUALIFICATIONS");
    Iterator it = userQualList.iterator();
    UserQualDetailsVO userQualDetailsVO = new UserQualDetailsVO();
    while(it.hasNext()) {
    	userQualDetailsVO = (UserQualDetailsVO) it.next();	
    }
    if (userConfirmedList != null && userConfirmedList.size() > 0) {
      request.setAttribute("userConfirmedList",userConfirmedList);
    }
    if (userQualList != null && userQualList.size() > 0) {
      request.setAttribute("userQualList", userQualList);
      request.setAttribute("userQualSize", String.valueOf(userQualList.size()));
    }
    if (userUGAttrDetailsList != null && userUGAttrDetailsList.size() > 0) {
      mybean.setUgQuestionAnswers(userUGAttrDetailsList);
      for (int i = 0; i < userUGAttrDetailsList.size(); i++) {
        QuestionAnswerBean qlquestionaansbean = new QuestionAnswerBean();
        qlquestionaansbean = (QuestionAnswerBean)userUGAttrDetailsList.get(i);
        if ("MWU_STUDY_LEVEL".equalsIgnoreCase(qlquestionaansbean.getAttributeName())) {
          if (GenericValidator.isBlankOrNull(qlquestionaansbean.getAttributeValue())) {
            mybean.setUserStudyLevel("452");
          } else {
            mybean.setUserStudyLevel(qlquestionaansbean.getAttributeValue());
          }
          break;
        }
      }
    }
    if (userPGAttrDetailsList != null && userPGAttrDetailsList.size() > 0) {
      mybean.setPgQuestionAnswers(userPGAttrDetailsList);
    }
    if (userDetailsList != null && userDetailsList.size() > 0) {
      MyWhatuniGetdataVO mywhatuniVO = (MyWhatuniGetdataVO)userDetailsList.get(0);
      mybean.setFirstName(mywhatuniVO.getFirstName());
      mybean.setLastName(mywhatuniVO.getLastName());
      mybean.setUserName(mywhatuniVO.getUserName());
      mybean.setNationality(mywhatuniVO.getNationality());
      mybean.setDateOfBirth(mywhatuniVO.getDateOfBirth());
      mybean.setYeartoJoinCourse(mywhatuniVO.getYeartoJoinCourse());
      if (GenericValidator.isBlankOrNull(mybean.getYeartoJoinCourse())) {
        mybean.setYeartoJoinCourse(mybean.getDefaultYoe()); //Added for Year of entry change 08_OCT_2014
      }
      //Added YOE value in session to log in d_session by Sujitha V on 2020_NOV_10 rel
      if(StringUtils.isNotBlank(mybean.getYeartoJoinCourse())) {
		CommonFunction.getDSessionLogForYOE(request, mybean.getYeartoJoinCourse()); 
      }
      
      
      if (!GenericValidator.isBlankOrNull(mybean.getDateOfBirth())) {
        String[] dob = mybean.getDateOfBirth().split("/");
        mybean.setDate(dob[0]);
        mybean.setMonth(dob[1]);
        mybean.setYear(dob[2]);
      }
      mybean.setEmail(mywhatuniVO.getEmail());
      mybean.setPassword(mywhatuniVO.getPassword());
      mybean.setPhoneNumber(mywhatuniVO.getPhoneNumber());
      String userPhoto = mywhatuniVO.getUserPhoto();
      String userThumbPhoto = (userPhoto != null? userPhoto.substring(0, userPhoto.lastIndexOf(".")) + "T" + userPhoto.substring(userPhoto.lastIndexOf("."), userPhoto.length()): userPhoto);
      mybean.setUserThumbPhoto(userThumbPhoto);
    }
    dobMethod(mybean);
    if (userAddressDetailsList != null && userAddressDetailsList.size() > 0) {
      MyWhatuniGetdataVO mywhatuniAddVO = (MyWhatuniGetdataVO)userAddressDetailsList.get(0);
      mybean.setAddressLine1(mywhatuniAddVO.getAddressLine1());
      mybean.setAddressLine2(mywhatuniAddVO.getAddressLine2());
      mybean.setTown(mywhatuniAddVO.getTown());
      mybean.setCountryOfResidence(mywhatuniAddVO.getCountryOfResidence());
      mybean.setPostCode(mywhatuniAddVO.getPostCode());
      request.setAttribute("cDimPostcode", mywhatuniAddVO.getPostCode());
    }
    List nationalityList = (ArrayList)mywhatuniDataMap.get("pc_nationality");
    if (nationalityList != null && nationalityList.size() > 0) {
      List nationalList = (ArrayList)nationalityList.get(0);
      mybean.setNationalityList(nationalList);
    }
    List countryOfList = (ArrayList)mywhatuniDataMap.get("pc_country");
    if (countryOfList != null && countryOfList.size() > 0) {
      List countryList = (ArrayList)countryOfList.get(0);
      mybean.setCounttyOfResList(countryList);
    }
    //Added two new objects to get user image and type details, 19_May_2015 By Thiyagu G.
    String userImageDetails = (String)mywhatuniDataMap.get("p_user_image_details");
    String userImageDetailsType = (String)mywhatuniDataMap.get("p_user_image_details_type");
    if (!GenericValidator.isBlankOrNull(userImageDetailsType)) {
      if (userImageDetailsType.equals("USER_IMAGE")) {
        if (!GenericValidator.isBlankOrNull(userImageDetails)) {
          String envronmentName = new CommonFunction().getWUSysVarValue("WU_ENV_NAME");                      
          String profileImagePath = "";
          if(("LIVE").equals(envronmentName)){
            profileImagePath = new CommonUtil().getImgPath(userImageDetails, 0);
          }else{
            profileImagePath = userImageDetails;
          } 
          request.setAttribute("croppedImage", profileImagePath);
        }
      } else if (userImageDetailsType.equals("FB_USER_ID")) {
        String fbImagePath = "http://graph.facebook.com/" + userImageDetails + "/picture?type=normal";
        request.setAttribute("croppedImage", fbImagePath);
      }
    }
    String friendsPrivacyFlag = (String)mywhatuniDataMap.get("p_friends_privacy_flag");
    if (!GenericValidator.isBlankOrNull(friendsPrivacyFlag)) {
      request.setAttribute("friendsPrivacyFlag", friendsPrivacyFlag);
    }
    
    String catPrivacyFlag = (String)mywhatuniDataMap.get("p_ca_track_flag");
    if (!GenericValidator.isBlankOrNull(catPrivacyFlag)) {
      request.setAttribute("catPrivacyFlag", catPrivacyFlag);
    }
    String hideDownloadBtnFlag = (String)mywhatuniDataMap.get("P_HIDE_DOWNLOAD_BUTTON");
    if (!GenericValidator.isBlankOrNull(hideDownloadBtnFlag)) {
      request.setAttribute("hideDownloadBtnFlag", hideDownloadBtnFlag);
    }
	//Added this for showing consent details by Hema.S on 17_12_19_REL
    ArrayList userClientDetails = (ArrayList)mywhatuniDataMap.get("PC_USER_CONSENT_DETAILS");
    if (userClientDetails != null && userClientDetails.size() > 0) {
      request.setAttribute("userClientDetails", userClientDetails);
    }
    //Added by Indumathi.S Jan-27-16 Rel For displaying mail preference settings
    ArrayList userSubscriptionFlags = (ArrayList)mywhatuniDataMap.get("pc_user_subscription_flags");
    if (userSubscriptionFlags != null && userSubscriptionFlags.size() > 0) {
      MyWhatuniGetdataVO mywhatuniFlagVO = (MyWhatuniGetdataVO)userSubscriptionFlags.get(0);
      request.setAttribute("receiveNoEmail",mywhatuniFlagVO.getReceiveNoEmailFlag());
      request.setAttribute("permitEmail",mywhatuniFlagVO.getPermitEmail());
      request.setAttribute("solusEmail",mywhatuniFlagVO.getSolusEmail());
      request.setAttribute("markettingEmail",mywhatuniFlagVO.getMarkettingEmail());
      request.setAttribute("surveyMail",mywhatuniFlagVO.getSurveyMail()); //Added survey mail flag in mailing preferences for 31_May_2016, By Thiyagu G.
      request.setAttribute("reviewSurveyFlag",mywhatuniFlagVO.getReviewSurveyFlag());
    }
  }

  private void dobMethod(MyWhatuniBean mybean) {
    LabelValueBean dobBean = null;
    ArrayList dateList = new ArrayList();
    for (int i = 1; i <= 31; i++) {
      dobBean = new LabelValueBean();
      if (i < 10) {
        dobBean.setLabel("0" + String.valueOf(i));
        dobBean.setValue("0" + String.valueOf(i));
      } else {
        dobBean.setLabel(String.valueOf(i));
        dobBean.setValue(String.valueOf(i));
      }
      dateList.add(dobBean);
    }
    ArrayList monthList = new ArrayList();
    String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    for (int i = 1; i <= 12; i++) {
      dobBean = new LabelValueBean();
      if (i < 10) {
        dobBean.setLabel(months[i - 1]);
        dobBean.setValue("0" + String.valueOf(i));
      } else {
        dobBean.setLabel(months[i - 1]);
        dobBean.setValue(String.valueOf(i));
      }
      monthList.add(dobBean);
    }
    ArrayList yearList = new ArrayList();
    Calendar ca = new GregorianCalendar();
    int currentYear = (ca.get(Calendar.YEAR)-13);
    for (int i = 1900; i <= currentYear; i++) { //Changed years from 1970 to 1900 for 19_Apr_2016.
      dobBean = new LabelValueBean();
      dobBean.setLabel(String.valueOf(i));
      dobBean.setValue(String.valueOf(i));
      yearList.add(dobBean);
    }
    mybean.setDateList(dateList);
    mybean.setMonthList(monthList);
    mybean.setYearList(yearList);
  }
  public String getUKSchoolName(String ukSchoolId) {
    String ukSchoolName = null;
    try {
      DataModel dataModel = new DataModel();
      Vector vector = new Vector();
      vector.clear();
      vector.add(ukSchoolId);
      ukSchoolName = dataModel.getString("wu_user_profile_pkg.get_school_name_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return ukSchoolName;
  }
  //Added setResponseText method to post ajax response, 19_May_2015 By Thiyagu G.
  private void setResponseText(HttpServletResponse response, String responseMessage) {
    try {
      response.setContentType("TEXT/PLAIN");
      Writer writer = response.getWriter();
      writer.write(responseMessage.toString());
    } catch (IOException exception) {
      exception.printStackTrace();
    }
  }
  //Added cropImage method to get the cropped image , 19_May_2015 By Thiyagu G.
  public String cropImage(int x, int y, int w, int h, String orgImagePath) {
    try {      
      BufferedImage originalImgage = ImageIO.read(new File(orgImagePath));
      BufferedImage SubImgage = originalImgage.getSubimage(x, y, w, h);
      String destFileName = orgImagePath.substring(0, orgImagePath.lastIndexOf("."));
      String destFileExt = orgImagePath.substring(orgImagePath.lastIndexOf("."), orgImagePath.length());
      String destPath = destFileName + "_C" + destFileExt;
      File outputfile = new File(destPath);
      ImageIO.write(SubImgage, "jpg", outputfile);
      return destFileName + "_C" + destFileExt;
    } catch (IOException e) {
      e.printStackTrace();
      return "";
    }
  }
}
