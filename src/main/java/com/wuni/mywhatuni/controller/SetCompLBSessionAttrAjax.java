package com.wuni.mywhatuni.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @Version : 1.0
 * @since July 28 2015
 * @www.whatuni.com
 * @Created By : Priyaa Parthasarathy
 * @Purpose  : This program is used to set the session attribute for not showing the comparison lightbox again
 * *************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * *************************************************************************************************************************
 * 11-AUG-2015  Priyaa Parthasarathy        1.0
 * 17-DEC-2019  Hemalatha.K                 1.1       Spring migration done                                        wu_597        
 */

@Controller
public class SetCompLBSessionAttrAjax {
  
  @RequestMapping(value = "/setcomplbsessionattrajax", method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView execute(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {    
    String tempVal = (String)session.getAttribute("tempnoSplash");
    if(!GenericValidator.isBlankOrNull(tempVal)  && "NOSPLASH".equals(tempVal)) { 
      session.setAttribute("tempnoSplash",  null);}
      session.setAttribute("noSplashWin", session.getAttribute("tempnoSplash"));
      //session.setAttribute("dontShowCompSplash","TRUE");
      session.removeAttribute("tempnoSplash");
      return null;
  }
}