package com.wuni.mywhatuni.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.mywhatuni.form.MyCompareBean;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

@Controller
public class MyWhatuniComparisonController {
	
  @RequestMapping(value = "/myfinalchoice", method = {RequestMethod.GET, RequestMethod.POST}) 
  public ModelAndView getMyFinalChoice(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	
	  ServletContext context = request.getServletContext();
	    CommonFunction common = new CommonFunction();
	    String basketId = new CookieManager().getCookieValue(request, "basketId");
	    if (common.checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED 
	      response.sendRedirect(GlobalConstants.WU_CONTEXT_PATH + "/mywhatuni.html");
	      return null;
	    }
	    try {
	      if (!(new SecurityEvaluator().checkSecurity("REGISTERED_USER", request, response))) {
	        response.sendRedirect(GlobalConstants.WU_CONTEXT_PATH + "/mywhatuni.html");
	        return null;
	      }
	    } catch (Exception secutityException) {
	      secutityException.printStackTrace();
	    }
	    String actionFlag = request.getParameter("actionFlag");
	    String factors = request.getParameter("factors");
	    String assocTypeCode = request.getParameter("assocTypeCode");
	    String selectedBasketId = basketId; // request.getParameter("basketId");
	    String ajaxFlag = request.getParameter("ajaxFlag");
	    String viewsaved = request.getParameter("viewsaved");
	    String backFlag = request.getParameter("backFlag");
	    String frompod = request.getParameter("frompod");
	    String clearingOnOff = common.getWUSysVarValue("CLEARING_ON_OFF");
	    String clUserType = "";
	    if (("ON").equals(clearingOnOff)) {
	      clUserType = (String)session.getAttribute("USER_TYPE");
	      if ("CLEARING".equals(clUserType)) {
	        clUserType = "CLEARING_USER";
	      }
	    }
	    if (!GenericValidator.isBlankOrNull(viewsaved) && ("Y").equals(viewsaved)) {
	      request.setAttribute("viewsaved", "Y");
	    }
	    try {
	      if (!(new SecurityEvaluator().checkSecurity("REGISTERED_USER", request, response))) {
	        String fromUrl = "/mywhatuni.html";
	        if (session.getAttribute("fromUrl") == null) {
	          session.setAttribute("fromUrl", fromUrl);
	        }
	        return new ModelAndView("loginPage");
	      }
	    } catch (Exception secutityException) {
	      secutityException.printStackTrace();
	    }
	    String userId = new SessionData().getData(request, "y");
	    MyCompareBean myCompareBean = new MyCompareBean();
	    myCompareBean.setAssociateTypeCode("C");
	    //
	    common.loadUserLoggedInformation(request, context, response);
	    String cookieBasketId = common.checkCookieStatus(request);
	    myCompareBean.setSessionBasketId(cookieBasketId);
	    //
	    myCompareBean.setUserId(userId);
	    myCompareBean.setBasketId(cookieBasketId);
	    if (("Y").equals(ajaxFlag)) {
	      myCompareBean.setBasketId(selectedBasketId); // check for the basket id and send
	      if (GenericValidator.isBlankOrNull(selectedBasketId)) {
	        myCompareBean.setBasketId(cookieBasketId);
	      }
	      if (("Y").equals(frompod)) {
	        myCompareBean.setBasketId(cookieBasketId);
	        if (!GenericValidator.isBlankOrNull(selectedBasketId)) {
	          myCompareBean.setBasketId(selectedBasketId);
	        }
	      }
	      if (!GenericValidator.isBlankOrNull(assocTypeCode)) {
	        myCompareBean.setAssociateTypeCode(assocTypeCode);
	      }
	    }
	    request.setAttribute("selectedBasketId", myCompareBean.getBasketId());
	    request.setAttribute("sessionBasketId", myCompareBean.getSessionBasketId());
	    //   
	    request.setAttribute("assocTypeCode", myCompareBean.getAssociateTypeCode());
	    //
	    myCompareBean.setFactors(factors);
	    myCompareBean.setUserType(clUserType);
	    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	    Map resultMap = commonBusiness.getCompareBasketDetails(myCompareBean);
	    request.setAttribute("mywhatunipage", "mywhatunipage");
	    //saveToken(request);
	    request.setAttribute("curActiveMenu", "comparison");
	    request.setAttribute("insightPageFlag", "yes");
	    if (request.getParameter("butvalue") != null && request.getParameter("butvalue").equalsIgnoreCase("Save profile")) {
	      common.getUserTimeLineTrackPod(request);
	    }
	    //
	    int size = 0;
	    if (resultMap != null) {
	      String choiceStatus = (String)resultMap.get("p_choice_status");
	      if (!GenericValidator.isBlankOrNull(choiceStatus)) {
	        request.setAttribute("actionFlag", choiceStatus);
	      }
	      //This is to set the comparison table values - Right hand side  
	      ArrayList comparisonList = (ArrayList)resultMap.get("PC_COMPARE");
	      if (comparisonList != null && comparisonList.size() > 0) {
	        request.setAttribute("comparisonList", comparisonList);
	        size = comparisonList.size();
	      }
	      //This is to set the Additional factors - Right hand side  
	      ArrayList addFactorsList = (ArrayList)resultMap.get("PC_ADD_FACTORS");
	      if (addFactorsList != null && addFactorsList.size() > 0) {
	        request.setAttribute("addFactorsList", addFactorsList);
	      }
	      String breadCrumb = (String)resultMap.get("P_BREAD_CRUMB");
	      if (breadCrumb != null && breadCrumb.trim().length() > 0) {
	        request.setAttribute("breadCrumb", breadCrumb);
	      }
	      String defaultFactors = (String)resultMap.get("P_DEF_FACTORS");
	      if (!GenericValidator.isBlankOrNull(defaultFactors)) {
	        request.setAttribute("defaultFactorsSize", (String.valueOf(defaultFactors.split(GlobalConstants.SPLIT_CONSTANT).length)));
	      }
	      String helpText = (String)resultMap.get("P_HELP_TEXT");
	      String visualtype = (String)resultMap.get("P_VISUAL_TYPE");
	      if (!GenericValidator.isBlankOrNull(defaultFactors)) {
	        ArrayList defaultFactorsList = new ArrayList();
	        ArrayList tableValueList = new ArrayList(Arrays.asList(defaultFactors.split(GlobalConstants.SPLIT_CONSTANT)));
	        ArrayList visualTypeList = new ArrayList(Arrays.asList(visualtype.split(GlobalConstants.SPLIT_CONSTANT)));
	        ArrayList helpTextList = new ArrayList(Arrays.asList(helpText.split(GlobalConstants.SPLIT_CONSTANT)));
	        for (int i = 0; i < tableValueList.size(); i++) {
	          MyCompareBean myCompBean = new MyCompareBean();
	          myCompBean.setVisualType((String)visualTypeList.get(i));
	          myCompBean.setFactorName((String)tableValueList.get(i));
	          myCompBean.setHelpText(((String)helpTextList.get(i)).trim());
	          defaultFactorsList.add(myCompBean);
	        }
	        request.setAttribute("defaultFactorsList", defaultFactorsList);
	      }
	      //
	      String cookieBasketCount = common.getBasketCount(cookieBasketId, request);
	      cookieBasketCount = GenericValidator.isBlankOrNull(cookieBasketCount)? "0": cookieBasketCount;
	      if (GenericValidator.isBlankOrNull(cookieBasketId) && GenericValidator.isBlankOrNull(selectedBasketId)) {
	        cookieBasketCount = String.valueOf(size);
	        String currBasketId = (String)resultMap.get("P_CURR_BASKET_ID");
	      
	        if (!GenericValidator.isBlankOrNull(defaultFactors)) {
	          request.setAttribute("defaultFactorsSize", (String.valueOf(defaultFactors.split(GlobalConstants.SPLIT_CONSTANT).length)));
	        }
	        currBasketId = currBasketId != null? currBasketId: "";
	        Cookie cookie = CookieManager.createCookie(request,"basketId", currBasketId);
	        response.addCookie(cookie);
	      }
	      request.getSession().setAttribute("basketpodcollegecount", cookieBasketCount);
	      request.setAttribute("basketpodcollegecount", cookieBasketCount);
	      request.setAttribute("comparegapagename", "Final5");
	      request.setAttribute("getInsightName", "Final5");
	      request.setAttribute("curActiveMenu", "comparison");
	      //  
	    }
	    if (("Y").equals(frompod)) {
	      return new ModelAndView("mycomparisontableDetailResp");
	    }
	    if (("Y").equals(ajaxFlag) || ("Y").equals(backFlag)) {
	      return new ModelAndView("mycomparisontableresp");
	    }
	    request.setAttribute("showResponsiveTimeLine", "true");
	    return new ModelAndView("mycomparisonpage"); 
  }
}
