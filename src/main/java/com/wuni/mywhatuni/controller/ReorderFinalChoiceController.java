package com.wuni.mywhatuni.controller;

import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.valueobjects.FinalFiveChoiceVO;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

@Controller
public class ReorderFinalChoiceController {
	
  @RequestMapping(value = "/reorder-final-choice", method = {RequestMethod.GET, RequestMethod.POST}) 
  public ModelAndView getReorderFinalChoice(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	    CommonFunction common = new CommonFunction();
	    String basketId = new CookieManager().getCookieValue(request, "basketId");
	    if (common.checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED //
	      response.sendRedirect(GlobalConstants.WU_CONTEXT_PATH + GlobalConstants.timeToLoginUrl);
	      return null;
	    }
	    try {
	      if (!(new SecurityEvaluator().checkSecurity("REGISTERED_USER", request, response))) {
	        response.sendRedirect(GlobalConstants.WU_CONTEXT_PATH + GlobalConstants.timeToLoginUrl);
	        return null;
	      }
	    } catch (Exception secutityException) {
	      secutityException.printStackTrace();
	    }
	    String userId = new SessionData().getData(request, "y");
	    String actionFlag = request.getParameter("actionFlag"); //E - edit mode / C - confirm mode
	    String direction = request.getParameter("direction");
	    String position = request.getParameter("position");
	    String defaultFactorsSize = request.getParameter("defaultFactorsSize");
	    request.setAttribute("defaultFactorsSize", defaultFactorsSize);
	    CommonUtil util = new CommonUtil();
	    FinalFiveChoiceVO finalFiveChoiceVO = new FinalFiveChoiceVO();
	    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	    if ("E".equalsIgnoreCase(actionFlag) || "C".equalsIgnoreCase(actionFlag)) { //Edit and confirm final choices
	      finalFiveChoiceVO.setUserId(userId);
	      finalFiveChoiceVO.setBasketId(basketId);
	      finalFiveChoiceVO.setInputType(actionFlag);
	      Map resultMap = commonBusiness.editConfirmFinalFive(finalFiveChoiceVO);
	      ArrayList comparisonList = (ArrayList)resultMap.get("PC_COMPARE");
	      if (comparisonList != null && comparisonList.size() > 0) {
	        request.setAttribute("comparisonList", comparisonList);
	      }
	    } else if ("REORDER".equalsIgnoreCase(actionFlag)) { //Swap the choices
	      finalFiveChoiceVO.setUserId(userId);
	      finalFiveChoiceVO.setSwapDirection(direction);
	      finalFiveChoiceVO.setSwapPosition(position);
	      finalFiveChoiceVO.setBasketId(basketId);
	      Map resultMap = commonBusiness.swapFinalFiveChoice(finalFiveChoiceVO);
	      ArrayList comparisonList = (ArrayList)resultMap.get("PC_COMPARE");
	      if (comparisonList != null && comparisonList.size() > 0) {
	        request.setAttribute("comparisonList", comparisonList);
	      }
	      actionFlag = "E";
	    } else if ("SAVE".equalsIgnoreCase(actionFlag) || "REMOVE".equalsIgnoreCase(actionFlag)) { //Add to final five / remove from final five
	      String finalChoiceId = (!GenericValidator.isBlankOrNull(request.getParameter("finalChoiceId")) && util.isNumber(request.getParameter("finalChoiceId")) && !"0".equalsIgnoreCase(request.getParameter("finalChoiceId")))? request.getParameter("finalChoiceId"): null;
	      String collegeId = (!GenericValidator.isBlankOrNull(request.getParameter("collegeId")) && util.isNumber(request.getParameter("collegeId")) && !"0".equalsIgnoreCase(request.getParameter("collegeId")))? request.getParameter("collegeId"): null;
	      String courseId = (!GenericValidator.isBlankOrNull(request.getParameter("courseId")) && util.isNumber(request.getParameter("courseId")) && !"0".equalsIgnoreCase(request.getParameter("courseId")))? request.getParameter("courseId"): null;
	      String status = request.getParameter("status");
	      String positionId = (!GenericValidator.isBlankOrNull(request.getParameter("positionId")) && util.isNumber(request.getParameter("positionId")) && !"0".equalsIgnoreCase(request.getParameter("positionId")))? request.getParameter("positionId"): null;
	      finalFiveChoiceVO.setFinalChoiceId(finalChoiceId);
	      finalFiveChoiceVO.setCollegeId(collegeId);
	      finalFiveChoiceVO.setCourseId(courseId);
	      finalFiveChoiceVO.setStatus(status);
	      finalFiveChoiceVO.setChoicePos(positionId);
	      finalFiveChoiceVO.setUserId(userId);
	      finalFiveChoiceVO.setBasketId(basketId);
	      Map resultMap = commonBusiness.saveUpdateFinalChoice(finalFiveChoiceVO);
	      if (resultMap != null && !resultMap.isEmpty()) {
	        String retCode = (String)resultMap.get("p_return_code");
	        if ("SUCCESS".equalsIgnoreCase(retCode)) {
	          ArrayList comparisonList = (ArrayList)resultMap.get("PC_COMPARE");
	          if (comparisonList != null && comparisonList.size() > 0) {
	            request.setAttribute("comparisonList", comparisonList);
	          }
	        } else {
	          response.getWriter().write(retCode + GlobalConstants.SPLIT_CONSTANT + "FINAL_5");
	          return null;
	        }
	      }
	    } else if ("ADD_COURSE".equalsIgnoreCase(actionFlag) || "REMOVE_COURSE".equalsIgnoreCase(actionFlag)) { //Add and remove the course for basket/final choice
	      String finalChoiceId = (!GenericValidator.isBlankOrNull(request.getParameter("finalChoiceId")) && util.isNumber(request.getParameter("finalChoiceId")) && !"0".equalsIgnoreCase(request.getParameter("finalChoiceId")))? request.getParameter("finalChoiceId"): null;
	      String collegeId = (!GenericValidator.isBlankOrNull(request.getParameter("collegeId")) && util.isNumber(request.getParameter("collegeId")) && !"0".equalsIgnoreCase(request.getParameter("collegeId")))? request.getParameter("collegeId"): null;
	      String courseId = (!GenericValidator.isBlankOrNull(request.getParameter("courseId")) && util.isNumber(request.getParameter("courseId")) && !"0".equalsIgnoreCase(request.getParameter("courseId")))? request.getParameter("courseId"): null;
	      String status = request.getParameter("status");
	      String assocText = request.getParameter("assocText");
	      String basketContentId = (!GenericValidator.isBlankOrNull(request.getParameter("basketContentId")) && !"0".equalsIgnoreCase(request.getParameter("basketContentId")))? request.getParameter("basketContentId"): null;
	      finalFiveChoiceVO.setFinalChoiceId(finalChoiceId);
	      finalFiveChoiceVO.setCollegeId(collegeId);
	      finalFiveChoiceVO.setCourseId(courseId);
	      finalFiveChoiceVO.setStatus(status);
	      finalFiveChoiceVO.setAssociateText(assocText);
	      finalFiveChoiceVO.setUserId(userId);
	      finalFiveChoiceVO.setBasketId(basketId);
	      finalFiveChoiceVO.setBasketContentId(basketContentId);
	      Map resultMap = commonBusiness.updateBasketCourse(finalFiveChoiceVO);
	      if (resultMap != null && !resultMap.isEmpty()) {
	        String retCode = (String)resultMap.get("p_ret_Code");
	        if ("SUCCESS".equalsIgnoreCase(retCode)) {
	          ArrayList comparisonList = (ArrayList)resultMap.get("PC_COMPARE");
	          if (comparisonList != null && comparisonList.size() > 0) {
	            request.setAttribute("comparisonList", comparisonList);
	          }
	        } else {
	          response.getWriter().write(retCode + GlobalConstants.SPLIT_CONSTANT + "COURSE");
	          return null;
	        }
	      }
	    }
	    request.setAttribute("actionFlag", actionFlag);
	    return new ModelAndView("mycomparison-table-content");
  }
}
