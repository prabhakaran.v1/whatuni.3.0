package com.wuni.mywhatuni.controller;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.wuni.util.sql.DataModel;
import WUI.basket.form.BasketBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.SessionData;

@Controller
public class ShowBasketContentAjax {
	
  @RequestMapping(value = "/showbasketcontentajax", method = {RequestMethod.GET, RequestMethod.POST}) 
  public ModelAndView getShowBasketContentAjax(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    String logUserId = new SessionData().getData(request, "y");
    String userTrackSessionId = new SessionData().getData(request, "userTrackId");
    CommonFunction common = new CommonFunction();
    DataModel dataModel = new DataModel();
    String tempVal = (String)session.getAttribute("noSplashWin");
    String visitWebLBNew = StringUtils.isNotBlank(request.getParameter("visitWebLBNew")) ? request.getParameter("visitWebLBNew") : "";
    if(GenericValidator.isBlankOrNull(tempVal)){tempVal = "NOSPLASH";}
      if(session.getAttribute("tempnoSplash")== null){
        session.setAttribute("tempnoSplash",  tempVal);
      }
      session.setAttribute("noSplashWin","true");
      ResultSet resultset = null;
      String cookie_basket_id = common.checkCookieStatus(request);
      ArrayList basketList = new ArrayList();
      try {
        Vector vector = new Vector();
        vector.add(logUserId);
        vector.add(cookie_basket_id);
        vector.add(userTrackSessionId);
        vector.add(request.getParameter("assocId"));
        vector.add(request.getParameter("assocType"));
        vector.add(request.getParameter("compareType"));
        resultset = dataModel.getResultSet("MYWU_COMPARE_PKG.get_webclick_wishlist_fn", vector);     
        while (resultset.next()) {
          BasketBean compBean = new BasketBean();
          compBean.setCollegeId(resultset.getString("college_id"));
          compBean.setCoursetitle(resultset.getString("assoc_text"));
          compBean.setCollegeNameDisplay(resultset.getString("college_name_display"));
          compBean.setCollegeName(resultset.getString("college_name"));
          compBean.setCourseid(resultset.getString("course_id"));
          compBean.setShortListFlag(resultset.getString("shortist_flag"));
          compBean.setCollegeLogo_1(resultset.getString("college_logo"));
          compBean.setAssociationType(request.getParameter("assocType"));
          basketList.add(compBean);
        } 
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        dataModel.closeCursor(resultset);
      }
      if (basketList != null && basketList.size() > 0) {
        request.setAttribute("basketList", basketList);
      }
    if(StringUtils.equalsIgnoreCase(visitWebLBNew, "visit_web_lb_new")) {
      return new ModelAndView("showComparisonLBNew");
    }else {
      return new ModelAndView("showComparisonLB");
    }
    
  }
}
