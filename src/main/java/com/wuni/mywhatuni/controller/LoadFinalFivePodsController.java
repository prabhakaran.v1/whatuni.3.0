package com.wuni.mywhatuni.controller;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.wuni.myfinalchoice.controller.MyFinalChoiceController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.valueobjects.FinalFiveChoiceVO;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

@Controller
public class LoadFinalFivePodsController {
	
  @RequestMapping(value = "/get-suggested-pod", method = {RequestMethod.GET, RequestMethod.POST}) 
   public ModelAndView getSuggestedPod(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
	    CommonFunction common = new CommonFunction();
	    if (common.checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED //
	      response.sendRedirect(GlobalConstants.WU_CONTEXT_PATH + GlobalConstants.timeToLoginUrl);
	      return null;
	    }
	    try {
	      if (!(new SecurityEvaluator().checkSecurity("REGISTERED_USER", request, response))) {
	        response.sendRedirect(GlobalConstants.WU_CONTEXT_PATH + GlobalConstants.timeToLoginUrl);
	        return null;
	      }
	    } catch (Exception secutityException) {
	      secutityException.printStackTrace();
	    }
	    String forwardPath = "friends-activity-pod";
	    String userId = new SessionData().getData(request, "y");
	    String podName = request.getParameter("podName");
	    if ("COURSE_POD".equalsIgnoreCase(podName) || "UNI_POD".equalsIgnoreCase(podName)) {
	      String clearingOnOff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");
	      String clUserType = "";
	      if (("ON").equals(clearingOnOff)) {
	        clUserType = (String)session.getAttribute("USER_TYPE");
	        if ("CLEARING".equals(clUserType)) {
	          clUserType = "CLEARING_USER";
	        }
	      }
	      String sliderId = "1"; //1 for other uni pod slider div id and 2 for other course pod slider div id
	      String associateCode = "COLLEGE";
	      FinalFiveChoiceVO finalFiveChoiceVO = new FinalFiveChoiceVO();
	      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	      if ("COURSE_POD".equalsIgnoreCase(podName)) {
	        sliderId = "2";
	        associateCode = "COURSE";
	      }
	      finalFiveChoiceVO.setUserId(userId);
	      finalFiveChoiceVO.setAssociateTypeCode(associateCode);
	      Map resultMap = commonBusiness.getSuggestedComparisonPod(finalFiveChoiceVO);
	      if (resultMap != null) {
	        ArrayList compareSuggestionsList = (ArrayList)resultMap.get("PC_COMPARE_SUG");
	        if (compareSuggestionsList != null && compareSuggestionsList.size() > 0) {
	          request.setAttribute("suggestedComparisonList", compareSuggestionsList);
	          request.setAttribute("compareSuggestionsListSize", String.valueOf(compareSuggestionsList.size()));
	        }
	      }
	      request.setAttribute("associateType", associateCode);
	      request.setAttribute("sliderId", sliderId);
	      forwardPath = "suggested-comparison-pod";
	    } else {
	      new MyFinalChoiceController().getfriendslist(userId, request);
	      //forwardPath = "friends-activity-pod";
	    }
	    return new ModelAndView(forwardPath);
  }
}
