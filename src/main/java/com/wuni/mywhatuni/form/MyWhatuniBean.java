package com.wuni.mywhatuni.form;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;
import com.wuni.advertiser.ql.form.QuestionAnswerBean;

public class MyWhatuniBean{

  //User Basic Data
  private String firstName = null;
  private String lastName = null;
  private String userName = null;
  private String addressLine1 = null;
  private String addressLine2 = null;
  private String town = null;
  private String postCode = null;
  private String phoneNumber = null;
  private String nationality = null;
  private String countryOfResidence = null;
  private String dateOfBirth = null;
  private String email = null;
  private String password = null;
  private List nationalityList = null;
  private List counttyOfResList = null;
  private String userThumbPhoto = null;
  private MultipartFile imagePath = null;
  
  private String gradeTypeAttrId = null;
  private String gradeTypeAttrValue = null;
  private List gradeTypeList = null;
  private String gradesAttrId = null;
  private String gradesAttrValues = null;
  private String firstDegreedAttrId = null;
  private String firstDegreeAttrValue = null;
  private String classificationAttrId = null;
  private String classificationAttrValue = null;
  private List classificcationList = null;
  private String researchIntAttrId = null;
  private String researchIntAttrValue = null;
  private String ukSchoolIdAttrId = null;
  private String ukSchoolIdAttrValue = null;
  private String ukSchoolName = null;
  private String nonUKSchoolAttrId = null;
  private String nonUKSchoolAttrValue = null;
  private String nonUKSchoolFlagAttrId = null;
  private String nonUKSchoolFlagAttrValue = null;
  private String actualGradesAttrId = null;
  private String actualGradesAttrValue = null;
  
  
  private int attrListSize;
  private String validateFlag = null;
  private String date = null;
  private String month = null;
  private String year = null;
  private List dateList = null;
  private List monthList = null;
  private List yearList = null;
  private String yeartoJoinCourse = null;
  private List<QuestionAnswerBean> ugQuestionAnswers=new ArrayList<QuestionAnswerBean>();
  
 
  private List<QuestionAnswerBean> pgQuestionAnswers=new ArrayList<QuestionAnswerBean>();
  
  private String userStudyLevel = null;
  
  //Profile complete fields
   private String openDayCount = null;
   private String prospectusCount = null;
   private String enquiryCount = null;
   private String basketContentCount = null;
   private String profileComplete = null;
   
  private MultipartFile userPhotoUpload = null;
  
  private String defaultYoe = null;

  public QuestionAnswerBean getUGquestionAnswerBean(int index)
  {
    
    int size = ugQuestionAnswers.size();
    //Check if object exists at specified index
    if(index+1>size)
    {
      //adding objects
      for (int j = size; j < index + 1; j++)
      {
        QuestionAnswerBean bean = new QuestionAnswerBean();
        ugQuestionAnswers.add(bean);
      }
    }
    return (QuestionAnswerBean)ugQuestionAnswers.get(index);
  }

  public QuestionAnswerBean getPGquestionAnswerBean(int index)
  {
    
    int size = pgQuestionAnswers.size();
    //Check if object exists at specified index
    if(index+1>size)
    {
      //adding objects
      for (int j = size; j < index + 1; j++)
      {
        QuestionAnswerBean bean = new QuestionAnswerBean();
        pgQuestionAnswers.add(bean);
      }
    }
    return (QuestionAnswerBean)pgQuestionAnswers.get(index);
  }


  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUserName() {
    return userName;
  }

  public void setAddressLine1(String addressLine1) {
    this.addressLine1 = addressLine1;
  }

  public String getAddressLine1() {
    return addressLine1;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  public String getAddressLine2() {
    return addressLine2;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getTown() {
    return town;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getNationality() {
    return nationality;
  }

  public void setCountryOfResidence(String countryOfResidence) {
    this.countryOfResidence = countryOfResidence;
  }

  public String getCountryOfResidence() {
    return countryOfResidence;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPassword() {
    return password;
  }

  public void setGradeTypeAttrId(String gradeTypeAttrId) {
    this.gradeTypeAttrId = gradeTypeAttrId;
  }

  public String getGradeTypeAttrId() {
    return gradeTypeAttrId;
  }

  public void setGradeTypeAttrValue(String gradeTypeAttrValue) {
    this.gradeTypeAttrValue = gradeTypeAttrValue;
  }

  public String getGradeTypeAttrValue() {
    return gradeTypeAttrValue;
  }

  public void setGradesAttrId(String gradesAttrId) {
    this.gradesAttrId = gradesAttrId;
  }

  public String getGradesAttrId() {
    return gradesAttrId;
  }

  public void setGradesAttrValues(String gradesAttrValues) {
    this.gradesAttrValues = gradesAttrValues;
  }

  public String getGradesAttrValues() {
    return gradesAttrValues;
  }

  public void setFirstDegreedAttrId(String firstDegreedAttrId) {
    this.firstDegreedAttrId = firstDegreedAttrId;
  }

  public String getFirstDegreedAttrId() {
    return firstDegreedAttrId;
  }

  public void setFirstDegreeAttrValue(String firstDegreeAttrValue) {
    this.firstDegreeAttrValue = firstDegreeAttrValue;
  }

  public String getFirstDegreeAttrValue() {
    return firstDegreeAttrValue;
  }

  public void setClassificationAttrId(String classificationAttrId) {
    this.classificationAttrId = classificationAttrId;
  }

  public String getClassificationAttrId() {
    return classificationAttrId;
  }

  public void setClassificationAttrValue(String classificationAttrValue) {
    this.classificationAttrValue = classificationAttrValue;
  }

  public String getClassificationAttrValue() {
    return classificationAttrValue;
  }


  public void setResearchIntAttrId(String researchIntAttrId) {
    this.researchIntAttrId = researchIntAttrId;
  }

  public String getResearchIntAttrId() {
    return researchIntAttrId;
  }

  public void setResearchIntAttrValue(String researchIntAttrValue) {
    this.researchIntAttrValue = researchIntAttrValue;
  }

  public String getResearchIntAttrValue() {
    return researchIntAttrValue;
  }

  public void setGradeTypeList(List gradeTypeList) {
    this.gradeTypeList = gradeTypeList;
  }

  public List getGradeTypeList() {
    return gradeTypeList;
  }

  public void setClassificcationList(List classificcationList) {
    this.classificcationList = classificcationList;
  }

  public List getClassificcationList() {
    return classificcationList;
  }

  public void setNationalityList(List nationalityList) {
    this.nationalityList = nationalityList;
  }

  public List getNationalityList() {
    return nationalityList;
  }

  public void setCounttyOfResList(List counttyOfResList) {
    this.counttyOfResList = counttyOfResList;
  }

  public List getCounttyOfResList() {
    return counttyOfResList;
  }

  public void setUserThumbPhoto(String userThumbPhoto) {
    this.userThumbPhoto = userThumbPhoto;
  }

  public String getUserThumbPhoto() {
    return userThumbPhoto;
  }

  public void setAttrListSize(int attrListSize) {
    this.attrListSize = attrListSize;
  }

  public int getAttrListSize() {
    return attrListSize;
  }

  public void setValidateFlag(String validateFlag) {
    this.validateFlag = validateFlag;
  }

  public String getValidateFlag() {
    return validateFlag;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getDate() {
    return date;
  }

  public void setMonth(String month) {
    this.month = month;
  }

  public String getMonth() {
    return month;
  }

  public void setYear(String year) {
    this.year = year;
  }

  public String getYear() {
    return year;
  }

  public void setDateList(List dateList) {
    this.dateList = dateList;
  }

  public List getDateList() {
    return dateList;
  }

  public void setMonthList(List monthList) {
    this.monthList = monthList;
  }

  public List getMonthList() {
    return monthList;
  }

  public void setYearList(List yearList) {
    this.yearList = yearList;
  }

  public List getYearList() {
    return yearList;
  }
  
  public void setYeartoJoinCourse(String yeartoJoinCourse) {
    this.yeartoJoinCourse = yeartoJoinCourse;
  }
  
  public String getYeartoJoinCourse() {
    return yeartoJoinCourse;
  }
  public void setUkSchoolIdAttrId(String ukSchoolIdAttrId) {
    this.ukSchoolIdAttrId = ukSchoolIdAttrId;
  }
  public String getUkSchoolIdAttrId() {
    return ukSchoolIdAttrId;
  }
  public void setUkSchoolIdAttrValue(String ukSchoolIdAttrValue) {
    this.ukSchoolIdAttrValue = ukSchoolIdAttrValue;
  }
  public String getUkSchoolIdAttrValue() {
    return ukSchoolIdAttrValue;
  }
  public void setNonUKSchoolAttrId(String nonUKSchoolAttrId) {
    this.nonUKSchoolAttrId = nonUKSchoolAttrId;
  }
  public String getNonUKSchoolAttrId() {
    return nonUKSchoolAttrId;
  }
  public void setNonUKSchoolAttrValue(String nonUKSchoolAttrValue) {
    this.nonUKSchoolAttrValue = nonUKSchoolAttrValue;
  }
  public String getNonUKSchoolAttrValue() {
    return nonUKSchoolAttrValue;
  }
  public void setNonUKSchoolFlagAttrId(String nonUKSchoolFlagAttrId) {
    this.nonUKSchoolFlagAttrId = nonUKSchoolFlagAttrId;
  }
  public String getNonUKSchoolFlagAttrId() {
    return nonUKSchoolFlagAttrId;
  }
  public void setNonUKSchoolFlagAttrValue(String nonUKSchoolFlagAttrValue) {
    this.nonUKSchoolFlagAttrValue = nonUKSchoolFlagAttrValue;
  }
  public String getNonUKSchoolFlagAttrValue() {
    return nonUKSchoolFlagAttrValue;
  }
  public void setActualGradesAttrId(String actualGradesAttrId) {
    this.actualGradesAttrId = actualGradesAttrId;
  }
  public String getActualGradesAttrId() {
    return actualGradesAttrId;
  }
  public void setActualGradesAttrValue(String actualGradesAttrValue) {
    this.actualGradesAttrValue = actualGradesAttrValue;
  }
  public String getActualGradesAttrValue() {
    return actualGradesAttrValue;
  }
  public void setUkSchoolName(String ukSchoolName) {
    this.ukSchoolName = ukSchoolName;
  }
  public String getUkSchoolName() {
    return ukSchoolName;
  }

  public void setUserStudyLevel(String userStudyLevel)
  {
    this.userStudyLevel = userStudyLevel;
  }

  public String getUserStudyLevel()
  {
    return userStudyLevel;
  }

  public void setOpenDayCount(String openDayCount)
  {
    this.openDayCount = openDayCount;
  }

  public String getOpenDayCount()
  {
    return openDayCount;
  }

  public void setProspectusCount(String prospectusCount)
  {
    this.prospectusCount = prospectusCount;
  }

  public String getProspectusCount()
  {
    return prospectusCount;
  }

  public void setEnquiryCount(String enquiryCount)
  {
    this.enquiryCount = enquiryCount;
  }

  public String getEnquiryCount()
  {
    return enquiryCount;
  }

  public void setBasketContentCount(String basketContentCount)
  {
    this.basketContentCount = basketContentCount;
  }

  public String getBasketContentCount()
  {
    return basketContentCount;
  }

  public void setProfileComplete(String profileComplete)
  {
    this.profileComplete = profileComplete;
  }

  public String getProfileComplete()
  {
    return profileComplete;
  }

  public List<QuestionAnswerBean> getUgQuestionAnswers() {
	return ugQuestionAnswers;
  }

  public void setUgQuestionAnswers(List<QuestionAnswerBean> ugQuestionAnswers) {
	this.ugQuestionAnswers = ugQuestionAnswers;
  }

  public List<QuestionAnswerBean> getPgQuestionAnswers() {
	return pgQuestionAnswers;
  }

  public void setPgQuestionAnswers(List<QuestionAnswerBean> pgQuestionAnswers) {
	this.pgQuestionAnswers = pgQuestionAnswers;
  }

  public MultipartFile getImagePath() {
	return imagePath;
  }

  public void setImagePath(MultipartFile imagePath) {
	this.imagePath = imagePath;
  }

  public MultipartFile getUserPhotoUpload() {
	return userPhotoUpload;
  }

  public void setUserPhotoUpload(MultipartFile userPhotoUpload) {
	this.userPhotoUpload = userPhotoUpload;
  }

public String getDefaultYoe() {
	return defaultYoe;
}

public void setDefaultYoe(String defaultYoe) {
	this.defaultYoe = defaultYoe;
}
  
  
}
