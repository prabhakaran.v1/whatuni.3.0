package com.wuni.mywhatuni.form;

import org.apache.struts.action.ActionForm;

public class StudentSurveyForm{
  
  String userId = null;
  String collegeId = null;
  String courseId = null;
  String courseYear = null;
  String userName = null;
  String collegeName = null;
  String courseTitle = null;
  
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setCourseYear(String courseYear) {
    this.courseYear = courseYear;
  }
  public String getCourseYear() {
    return courseYear;
  }
  public void setUserName(String userName) {
    this.userName = userName;
  }
  public String getUserName() {
    return userName;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }
  public String getCourseTitle() {
    return courseTitle;
  }
}
