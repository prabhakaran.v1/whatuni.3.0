package com.wuni.mywhatuni.form;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;
/**
  * @Version :  1.0
  * @May 17 2015  
  * @www.whatuni.com
  * @Created By : Priyaa Parthasarathy
  * @Purpose  : This bean is used in mycompariosn page
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 17-May-2015  Priyaa Parthasarathy         1.0
  */
public class MyCompareBean{
  //
  private String basketId = null;
  private String sessionBasketId = null;
  private String associateTypeCode = null;
  private String userId = null;
  private String userSessionId = null;
  private String courseId = null;
  private String collegeId = null;
  private String assocId = null;
  private String courseTitle = null;
  private String collegeName = null;
  private String gaCollegeName = null;
  private String collegeDisplayName = null;
  private String dataString = null;
  private String factors = null;
  private String factorId = null;
  private String factorName = null;  
  private String comparisonBasketName = null; 
  private String pageType = null;
  private String factorStatus = null;
  private ArrayList comparetableValuesList = null;
  
  //
   private String enquiryType = null;
   private String orderItemId = null;   
   private String subOrderItemId = null;
   private String subOrderProspectus = null;
   private String subOrderEmailWebform = null;
   private String subOrderEmail = null;
   private String subOrderWebsite = null;
   
   private String myhcProfileId = null;
   private String profileType = null;  
   private String cpeQualificationNetworkId = null;

   private String subOrderProspectusWebform = null;
   private String websitePrice = null;
   private String webformPrice = null;
   private String mediaPath = null;
   private String mediaThumbPath = null;
   private String mediaType = null;
   private String mediaId = null;
   private String videoThumbPath = null;
   private String uniHomeURL = null;
   private String visualType =null;
   private String columnvalue = null;
   private String visualTypeValue = null;
   private ArrayList compareTableDataList = null;
   private String studyLevel = null;
   private String courseUrl = null;
   private String chartprctValue = null;
   private String helpText = null;
   private String deleteFlag = null;
   private String userType = null;//30_JUL_2015_REL
   private String clearingFlag  = null;
   private String hotlineNo  = null;
   private String courseDeletedFlag = null;
   private String addtoFinal5  = null;//Added fby Priyaa for final 5 - 21-Jul-2015       
   private String choicePosition = null;
   private String posText = null;
   private String collegeLog = null;
   private String finalFiveFlag = null;
   private String finalChoiceId = null;
   private String basketContentId = null;
   private String finalChoiceCount = null;
   
  //
  public void setBasketId(String basketId) {
    this.basketId = basketId;
  }
  public String getBasketId() {
    return basketId;
  }
  public void setAssociateTypeCode(String associateTypeCode) {
    this.associateTypeCode = associateTypeCode;
  }
  public String getAssociateTypeCode() {
    return associateTypeCode;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setUserSessionId(String userSessionId) {
    this.userSessionId = userSessionId;
  }
  public String getUserSessionId() {
    return userSessionId;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setAssocId(String assocId) {
    this.assocId = assocId;
  }
  public String getAssocId() {
    return assocId;
  }
  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }
  public String getCourseTitle() {
    return courseTitle;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeDisplayName(String collegeDisplayName) {
    this.collegeDisplayName = collegeDisplayName;
  }
  public String getCollegeDisplayName() {
    return collegeDisplayName;
  }
  public void setDataString(String dataString) {
    this.dataString = dataString;
  }
  public String getDataString() {
    return dataString;
  }
  public void setFactors(String factors) {
    this.factors = factors;
  }
  public String getFactors() {
    return factors;
  }
  public void setFactorId(String factorId) {
    this.factorId = factorId;
  }
  public String getFactorId() {
    return factorId;
  }
  public void setFactorName(String factorName) {
    this.factorName = factorName;
  }
  public String getFactorName() {
    return factorName;
  }
  public void setComparisonBasketName(String comparisonBasketName) {
    this.comparisonBasketName = comparisonBasketName;
  }
  public String getComparisonBasketName() {
    return comparisonBasketName;
  }
  public void setPageType(String pageType) {
    this.pageType = pageType;
  }
  public String getPageType() {
    return pageType;
  }
  public void setComparetableValuesList(ArrayList comparetableValuesList) {
    this.comparetableValuesList = comparetableValuesList;
  }
  public ArrayList getComparetableValuesList() {
    return comparetableValuesList;
  }
  public void setFactorStatus(String factorStatus) {
    this.factorStatus = factorStatus;
  }
  public String getFactorStatus() {
    return factorStatus;
  }
  public void setEnquiryType(String enquiryType) {
    this.enquiryType = enquiryType;
  }
  public String getEnquiryType() {
    return enquiryType;
  }
  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }
  public String getOrderItemId() {
    return orderItemId;
  }
  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }
  public String getSubOrderItemId() {
    return subOrderItemId;
  }
  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }
  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }
  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }
  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }
  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }
  public String getSubOrderEmail() {
    return subOrderEmail;
  }
  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }
  public String getMyhcProfileId() {
    return myhcProfileId;
  }
  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }
  public String getProfileType() {
    return profileType;
  }
  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }
  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }
  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }
  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }
  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }
  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }
  public void setGaCollegeName(String gaCollegeName) {
    this.gaCollegeName = gaCollegeName;
  }
  public String getGaCollegeName() {
    return gaCollegeName;
  }
  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }
  public String getWebsitePrice() {
    return websitePrice;
  }
  public void setWebformPrice(String webformPrice) {
    this.webformPrice = webformPrice;
  }
  public String getWebformPrice() {
    return webformPrice;
  }
  public void setMediaThumbPath(String mediaThumbPath) {
    this.mediaThumbPath = mediaThumbPath;
  }
  public String getMediaThumbPath() {
    return mediaThumbPath;
  }
  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }
  public String getMediaType() {
    return mediaType;
  }
  public void setMediaId(String mediaId) {
    this.mediaId = mediaId;
  }
  public String getMediaId() {
    return mediaId;
  }
  public void setVideoThumbPath(String videoThumbPath) {
    this.videoThumbPath = videoThumbPath;
  }
  public String getVideoThumbPath() {
    return videoThumbPath;
  }
  public void setMediaPath(String mediaPath) {
    this.mediaPath = mediaPath;
  }
  public String getMediaPath() {
    return mediaPath;
  }
  public void setSessionBasketId(String sessionBasketId) {
    this.sessionBasketId = sessionBasketId;
  }
  public String getSessionBasketId() {
    return sessionBasketId;
  }
  public void setUniHomeURL(String uniHomeURL) {
    this.uniHomeURL = uniHomeURL;
  }
  public String getUniHomeURL() {
    return uniHomeURL;
  }
  public void setVisualType(String visualType) {
    this.visualType = visualType;
  }
  public String getVisualType() {
    return visualType;
  }
  public void setColumnvalue(String columnvalue) {
    this.columnvalue = columnvalue;
  }
  public String getColumnvalue() {
    return columnvalue;
  }
  public void setVisualTypeValue(String visualTypeValue) {
    this.visualTypeValue = visualTypeValue;
  }
  public String getVisualTypeValue() {
    return visualTypeValue;
  }
  public void setCompareTableDataList(ArrayList compareTableDataList) {
    this.compareTableDataList = compareTableDataList;
  }
  public ArrayList getCompareTableDataList() {
    return compareTableDataList;
  }
  public void setStudyLevel(String studyLevel) {
    this.studyLevel = studyLevel;
  }
  public String getStudyLevel() {
    return studyLevel;
  }
  public void setCourseUrl(String courseUrl) {
    this.courseUrl = courseUrl;
  }
  public String getCourseUrl() {
    return courseUrl;
  }
  public void setChartprctValue(String chartprctValue) {
    this.chartprctValue = chartprctValue;
  }
  public String getChartprctValue() {
    return chartprctValue;
  }
  public void setHelpText(String helpText) {
    this.helpText = helpText;
  }
  public String getHelpText() {
    return helpText;
  }
  public void setDeleteFlag(String deleteFlag) {
    this.deleteFlag = deleteFlag;
  }
  public String getDeleteFlag() {
    return deleteFlag;
  }
  public void setUserType(String userType) {
    this.userType = userType;
  }
  public String getUserType() {
    return userType;
  }
  public void setClearingFlag(String clearingFlag) {
    this.clearingFlag = clearingFlag;
  }
  public String getClearingFlag() {
    return clearingFlag;
  }
  public void setHotlineNo(String hotlineNo) {
    this.hotlineNo = hotlineNo;
  }
  public String getHotlineNo() {
    return hotlineNo;
  }
  public void setCourseDeletedFlag(String courseDeletedFlag) {
    this.courseDeletedFlag = courseDeletedFlag;
  }
  public String getCourseDeletedFlag() {
    return courseDeletedFlag;
  }
  public void setAddtoFinal5(String addtoFinal5) {
    this.addtoFinal5 = addtoFinal5;
  }
  public String getAddtoFinal5() {
    return addtoFinal5;
  }
  public void setChoicePosition(String choicePosition) {
    this.choicePosition = choicePosition;
  }
  public String getChoicePosition() {
    return choicePosition;
  }
  public void setPosText(String posText) {
    this.posText = posText;
  }
  public String getPosText() {
    return posText;
  }
  public void setCollegeLog(String collegeLog) {
    this.collegeLog = collegeLog;
  }
  public String getCollegeLog() {
    return collegeLog;
  }
  public void setFinalFiveFlag(String finalFiveFlag) {
    this.finalFiveFlag = finalFiveFlag;
  }
  public String getFinalFiveFlag() {
    return finalFiveFlag;
  }
  public void setFinalChoiceId(String finalChoiceId) {
    this.finalChoiceId = finalChoiceId;
  }
  public String getFinalChoiceId() {
    return finalChoiceId;
  }
  public void setBasketContentId(String basketContentId) {
    this.basketContentId = basketContentId;
  }
  public String getBasketContentId() {
    return basketContentId;
  }
  public void setFinalChoiceCount(String finalChoiceCount) {
    this.finalChoiceCount = finalChoiceCount;
  }
  public String getFinalChoiceCount() {
    return finalChoiceCount;
  }
}
