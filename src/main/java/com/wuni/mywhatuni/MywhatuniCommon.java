package com.wuni.mywhatuni;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import org.apache.commons.validator.GenericValidator;
import org.apache.struts.upload.FormFile;
import org.springframework.web.multipart.MultipartFile;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.wuni.mywhatuni.form.MyWhatuniBean;
import com.wuni.util.RequestResponseUtils;
import com.wuni.util.sql.DataModel;

public class MywhatuniCommon {

  public void userDataUpdataion(String userId,MyWhatuniBean mybean, HttpServletRequest request, HttpServletResponse response){
    try{
      List questionList = basicFormRegQuestion();
      ArrayList questionAnsList = new ArrayList();
      for (int i = 0; i < questionList.size(); i++) {
        RegistrationBean rbean = (RegistrationBean)questionList.get(i);
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("1") || "Email address".equalsIgnoreCase(rbean.getQuestionLabel()))) {
          rbean.setSelectedValue(mybean.getEmail());
        }
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("4") || "First name".equalsIgnoreCase(rbean.getQuestionLabel()))) {
          rbean.setSelectedValue(mybean.getFirstName());
        }
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("5") || "Surname".equalsIgnoreCase(rbean.getQuestionLabel()))) {
          rbean.setSelectedValue(mybean.getLastName());
        }          
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("7") || "Address Line One".equalsIgnoreCase(rbean.getQuestionLabel()))) {
          if("Address line 1".equalsIgnoreCase(mybean.getAddressLine1()))
          {
           rbean.setSelectedValue(""); 
          }else
          {
            rbean.setSelectedValue(mybean.getAddressLine1());
          }
        }          
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("8") || "Address Line Two".equalsIgnoreCase(rbean.getQuestionLabel().trim()))) {
          if("Address line 2".equalsIgnoreCase(mybean.getAddressLine2()))
          {
           rbean.setSelectedValue(""); 
          }else
          {
            rbean.setSelectedValue(mybean.getAddressLine2());
          }
        }          
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("9") || "Town / City".equalsIgnoreCase(rbean.getQuestionLabel()))) {
          if("Please enter town/city".equalsIgnoreCase(mybean.getTown()))
          {
            rbean.setSelectedValue("");
          }else
          {
            rbean.setSelectedValue(mybean.getTown());
          }
        }          
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("11") || "Postcode".equalsIgnoreCase(rbean.getQuestionLabel()))) {
          if("Please enter".equalsIgnoreCase(mybean.getPostCode()))
          {
           rbean.setSelectedValue(""); 
          }else
          {
            rbean.setSelectedValue(mybean.getPostCode());
          }
        }          
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("12") || "Mobile phone number".equalsIgnoreCase(rbean.getQuestionLabel()))) {
          rbean.setSelectedValue(mybean.getPhoneNumber());
        }          
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("17") || "Nationality".equalsIgnoreCase(rbean.getQuestionLabel()))) {
          rbean.setSelectedValue(mybean.getNationality());
        }          
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("52") || "Date of birth".equalsIgnoreCase(rbean.getQuestionLabel()))) {
          rbean.setSelectedValue(mybean.getDateOfBirth());
        }          
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("62") || "Country Of Residency".equalsIgnoreCase(rbean.getQuestionLabel()))) {
          rbean.setSelectedValue(mybean.getCountryOfResidence());
        }
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && (rbean.getFiledId().equalsIgnoreCase("1467") || "Year of course".equalsIgnoreCase(rbean.getQuestionLabel()))) {
          rbean.setSelectedValue(mybean.getYeartoJoinCourse());
        }   
        questionAnsList.add(rbean);
      }
      userId = updateUserRelatedData(userId, request, response, questionAnsList);
    }catch(Exception e){
      e.printStackTrace();
    }
  }

  /**
    *   This function is used to update the user registration related information.
    * @param request
    * @return userid as String.
    * @throws ServletException
    * @throws IOException
    */
  public String updateUserRelatedData(String userId, HttpServletRequest request, HttpServletResponse response, ArrayList questionlist) throws ServletException, IOException {
    RegistrationBean bean = null;
    int rowNum = 4;
    HttpSession session = request.getSession();
    ResultSet resultset = null;
    String answerId[] = new String[questionlist.size() + 8];
    String answerText[] = new String[questionlist.size() + 8];
    answerId[0] = "x";
    answerText[0] = new SessionData().getData(request, "x");
    answerId[1] = "y";
    answerText[1] = userId;  
    answerId[2] = "a";
    answerText[2] = GlobalConstants.WHATUNI_AFFILATE_ID;
    String formName = "";
    String formId = "";
    answerId[3] = "206";
    answerText[3] = "MYWHATUNI_USER_FORM"; //form ID
    if (questionlist != null) {
      Iterator questioniterator = questionlist.iterator();
      while (questioniterator.hasNext()) {
        bean = new RegistrationBean();
        bean = (RegistrationBean)questioniterator.next();
        String questionanswerId = bean.getFiledId();
        String questionanswerText = bean.getSelectedValue() != null? bean.getSelectedValue().trim(): bean.getSelectedValue();
        answerId[rowNum] = "pm_" + questionanswerId;
        answerText[rowNum] = questionanswerText;
        rowNum++;
      }
      if (answerId != null && answerText != null) {
        Connection connection = null;
        OracleCallableStatement oraclestmt = null;
        try {
          DataSource datasource = null;
          try {
            InitialContext ic = new InitialContext();
            datasource = (DataSource)ic.lookup("whatuni");
          } catch (Exception exception) {
            exception.printStackTrace();
          }
          connection = datasource.getConnection();
          String query = "{ ?= call hot_admin.hc_maintain_user3.create_user_reg_fn(?,?,?,?,?,?,?) }";
          oraclestmt = (OracleCallableStatement)connection.prepareCall(query);
          oraclestmt.registerOutParameter(1, OracleTypes.CURSOR);
          ArrayDescriptor descriptor = ArrayDescriptor.createDescriptor("T_USERREGTABTYPE", connection);
          ARRAY array_1 = new ARRAY(descriptor, connection, answerId);
          ARRAY array_2 = new ARRAY(descriptor, connection, answerText);
          oraclestmt.setARRAY(2, array_1);
          oraclestmt.setARRAY(3, array_2);
          oraclestmt.setString(4, new RequestResponseUtils().getRemoteIp(request));
          oraclestmt.setString(5, request.getHeader("user-agent"));
          oraclestmt.setString(6, CommonUtil.getRequestedURL(request));//Change the getRequestURL by Prabha on 28_Jun_2016
          oraclestmt.setString(7, request.getHeader("referer"));
          oraclestmt.setString(8, new SessionData().getData(request, "userTrackId"));
          oraclestmt.execute();
          resultset = (ResultSet)oraclestmt.getObject(1);
          while (resultset.next()) {
            userId = resultset.getString("user_id");
            new SessionData().addData(request, response, "y", resultset.getString("user_id"));
            new SessionData().addData(request, response, "x", resultset.getString("session_id"));
          }
        } catch (Exception exception) {
          exception.printStackTrace();
        } finally {
          try {
            if (oraclestmt != null) {
              oraclestmt.close();
            }
            if (resultset != null) {
              resultset.close();
            }
            if (connection != null) {
              connection.close();
            }
          } catch (Exception closeException) {
            closeException.printStackTrace();
          }
        }
      }
    }
    return userId;
  }
 
  /**
    *   This function is used to generate the dynamic question to update user info.
    * @param request
    * @return Dynamic registartion qustionas Collection object.
    */
  public List basicFormRegQuestion() {
    ResultSet resultset = null;
    Vector vector = new Vector();
    ArrayList list = new ArrayList();
    RegistrationBean bean = null;
    DataModel datamodel = new DataModel();
    try {
      vector.add("MYWHATUNI_USER_FORM");
      resultset = datamodel.getResultSet("wu_review_pkg.registration_page_fn", vector);
      while (resultset.next()) {
        bean = new RegistrationBean();
        bean.setFiledId(resultset.getString("FIELD_ID"));
        bean.setFieldType(resultset.getString("FIELD_TYPE"));
        bean.setQuestionLabel(resultset.getString("DESCRIPTION"));
        bean.setDisplaySeqId(resultset.getString("DISPLAY_SEQ"));
        bean.setFieldRequired(resultset.getString("FIELD_REQ"));
        bean.setPerRowCount(resultset.getString("PER_ROW_CNT"));
        bean.setDefaultValue(resultset.getString("DEFAULT_VALUE"));
        bean.setTemplateIdHint(resultset.getString("TEMPLATE_ID"));
        bean.setRowStatus(resultset.getString("ROW_STATUS"));
        bean.setMaxSize(resultset.getString("MAX_SIZE"));
        bean.setDisplaySize(resultset.getString("DISPLAY_SIZE"));
        bean.setSelectedValue("");
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      datamodel.closeCursor(resultset);
    }
    return list;
  }
  
  /**
      *   This funciton is used to upload the user image to application server.
      * @param myForm
      * @param userId
      * @param imageId
      * @return upload status as boolean.
      */
    public boolean loadBrowseImage(MyWhatuniBean mybean, String imageId, HttpServletRequest request) {
      boolean uploadStatus = true;
      String desDir = "";
      HttpSession session = request.getSession();
      String sessionId = session.getId();
      try {
    	MultipartFile formFile = mybean.getUserPhotoUpload();
        InputStream inputStream = formFile.getInputStream();
        String fileName = formFile.getOriginalFilename();
        desDir = new CommonFunction().getWUSysVarValue("USER_IMAGE_UPLOAD_PATH");
        desDir = "d:"+desDir;
        createDirectory(new File(desDir));
        String fileExtension = fileName.substring(fileName.indexOf("."), fileName.length());
        FileOutputStream fout = new FileOutputStream(desDir + sessionId + imageId + fileExtension); //open the ouput stream
        boolean eof = false;
        int readByte;
        while (!eof) {
          readByte = inputStream.read();
          if (readByte == -1)
            eof = true;
          fout.write(readByte);
        }
        inputStream.close();
        fout.close(); //Close the output stream
        String sourceFile = desDir + sessionId + imageId + fileExtension;
        String thumbNailFile = desDir + sessionId + imageId + "T" + fileExtension;
        int thumbNailSize = 100;      
        new CommonFunction().createThumbnail(sourceFile, thumbNailFile, thumbNailSize);
        //Check for the upload files.. if the file not found then it will update the default image path to the DB 
        /*String checkImagePath = new CommonFunction().getWUSysVarValue("USER_IMAGE_FILECHECK_PATH") + "users/photos/" + sessionId + imageId + fileExtension;
        boolean checkFoundFlag = new CommonFunction().checkImagePresent(checkImagePath);
        if (!checkFoundFlag) {
          new GlobalFunction().updateExistingImage(imageId);
        }*/
        // end 
      } catch (Exception exception) {
        uploadStatus = false;
        exception.printStackTrace();
      }
      return uploadStatus;
    }
    
  /**
    *   This funciton is used to upload the user image to application server.
    * @param myForm
    * @param userId
    * @param imageId
    * @return upload status as boolean.
    */
  public String loadBrowseImageNew(MyWhatuniBean mybean, String userId, HttpServletRequest request) {
    boolean uploadStatus = true;
    String desDir = "";
    String thumbNailFile = "";
    HttpSession session = request.getSession();
    String sessionId = session.getId();
    try {
      MultipartFile formFile = mybean.getUserPhotoUpload();
      InputStream inputStream = formFile.getInputStream();
      String fileName = formFile.getOriginalFilename();
      desDir = new CommonFunction().getWUSysVarValue("USER_IMAGE_UPLOAD_PATH");
      //desDir = "d:"+desDir;
      createDirectory(new File(desDir));
      String fileExtension = fileName.substring(fileName.indexOf("."), fileName.length());
      //generate random UUIDs
      String randomNo = UUID.randomUUID().toString();
      FileOutputStream fout = new FileOutputStream(desDir + randomNo + userId + fileExtension); //open the ouput stream
      boolean eof = false;
      int readByte;
      while (!eof) {
        readByte = inputStream.read();
        if (readByte == -1)
          eof = true;
        fout.write(readByte);
      }
      inputStream.close();
      fout.close(); //Close the output stream
      String sourceFile = desDir + randomNo + userId + fileExtension;
      thumbNailFile = desDir + randomNo + userId + "T" + fileExtension;
      int thumbNailSize = 100;      
      new CommonFunction().createThumbnail(sourceFile, thumbNailFile, thumbNailSize);      
      // end 
    } catch (Exception exception) {
      uploadStatus = false;
      exception.printStackTrace();
    }    
    return thumbNailFile;    
  }
  public String uploadBrowseImageToWebserver(MyWhatuniBean mybean, String userId, HttpServletRequest request) {
    boolean uploadStatus = true;
    String desDir = "";    
    String sourceFile = "";
    String thumbNailFile = "";
    try {
      MultipartFile formFile = mybean.getUserPhotoUpload();
      InputStream inputStream = formFile.getInputStream();
      String fileName = formFile.getOriginalFilename();
      BufferedInputStream is = new BufferedInputStream (formFile.getInputStream());
      BufferedImage image = ImageIO.read(is);
      desDir = new CommonFunction().getWUSysVarValue("USER_IMAGE_UPLOAD_PATH");
      //desDir = "D:/cvsroot/latest/New/hcwhatuni/ViewController/public_html"+desDir;
      createDirectory(new File(desDir));      
      String fileExtension = fileName.substring(fileName.indexOf("."), fileName.length());
      //generate random UUIDs
      String randomNo = UUID.randomUUID().toString();
      FileOutputStream fout = new FileOutputStream(desDir + randomNo.replaceAll("-", "") + userId + fileExtension); //open the ouput stream
      boolean eof = false;
      int readByte;
      while (!eof) {
        readByte = inputStream.read();
        if (readByte == -1)
          eof = true;
        fout.write(readByte);
      }
      inputStream.close();
      fout.close(); //Close the output stream      
      if(image.getWidth(null) > 980){
        sourceFile = desDir + randomNo.replaceAll("-", "") + userId + fileExtension;
        thumbNailFile = desDir + randomNo.replaceAll("-", "") + userId + "T" + fileExtension;
        int thumbNailSize = 950;      
        new CommonFunction().createThumbnail(sourceFile, thumbNailFile, thumbNailSize);
        sourceFile = randomNo.replaceAll("-", "") + userId + "T" + fileExtension;
      }else{
        sourceFile = randomNo.replaceAll("-", "") + userId + fileExtension;
      }
      // end 
    } catch (Exception exception) {
      uploadStatus = false;
      exception.printStackTrace();
    }    
    return sourceFile;    
  }

  /**
    *   This function is used to create a directory in application server.
    * @param srcDir
    * @return none.
    * @throws IOException
    */
  public void createDirectory(File srcDir) throws IOException {
    if (!srcDir.isDirectory()) {
      srcDir.mkdir();
    }
  }
}
