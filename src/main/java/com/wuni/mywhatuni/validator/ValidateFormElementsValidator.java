package com.wuni.mywhatuni.validator;

import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMessage;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wuni.mywhatuni.form.MyWhatuniBean;

import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import javax.servlet.ServletRequest;

public class ValidateFormElementsValidator implements Validator{

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
//		boolean status = false;
		//HttpServletRequest request = (HttpServletRequest) req;
		MyWhatuniBean mybean = (MyWhatuniBean) (target);
	    if (GenericValidator.isBlankOrNull(mybean.getFirstName()) || "First Name".equalsIgnoreCase(mybean.getFirstName())) {
	      errors.rejectValue("firstName", "wuni.error.mywhatuni.firstname");
	    }
	    if (GenericValidator.isBlankOrNull(mybean.getLastName()) || "Last Name".equalsIgnoreCase(mybean.getLastName())) {
	      errors.rejectValue("lastName", "wuni.error.mywhatuni.lastname");
	    }
	    if(!GenericValidator.isBlankOrNull(mybean.getDate()) && !GenericValidator.isBlankOrNull(mybean.getMonth()) && !GenericValidator.isBlankOrNull(mybean.getYear())){
	    mybean.setDateOfBirth(mybean.getDate() + "/" + mybean.getMonth() + "/" + mybean.getYear());
	    String dateOfBirth = mybean.getDateOfBirth();
	    if (GenericValidator.isBlankOrNull(mybean.getDateOfBirth())) {
	      errors.rejectValue("wuni.error.mywhatuni.dob", "wuni.error.mywhatuni.dob");
	    } else {
	      ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
	      String dateFormat = bundle.getString("wuni.format.date");
	      int dateFormatLength = dateFormat.length();
	      if (dateOfBirth.length() != dateFormatLength) {
	        errors.rejectValue("wuni.error.mywhatuni.dob", "wnui.error.invalid_date");
	      } else {
	        String sysDate = new CommonFunction().getSysDate();
	        if (sysDate != null && sysDate.trim().length() > 0) {
	          if (!new CommonUtil().isValidDate(dateOfBirth, sysDate, dateFormat)) {
	            errors.rejectValue("wuni.error.mywhatuni.dob", "wnui.error.invalid_date_exceed_cur_date");
	          }
	        }
	      }
	    }
	    }
	    if (GenericValidator.isBlankOrNull(mybean.getYeartoJoinCourse())) { //Added for Year of entry change 08_OCT_2014
	      errors.rejectValue("wuni.error.mywhatuni.yoe", "wuni.error.mywhatuni.yearOfEntry");
	    }
	   // saveErrors(request, errors);
//	    if (errors.size() > 0) {
//	      status = true;
//	    }
		
	}

}
