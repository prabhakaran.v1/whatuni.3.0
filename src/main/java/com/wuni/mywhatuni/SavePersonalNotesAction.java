package com.wuni.mywhatuni;

import java.io.PrintWriter;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.web.servlet.ModelAndView;

import com.wuni.util.sql.DataModel;


public class SavePersonalNotesAction
{

  public ModelAndView savePersonalNotes(HttpServletRequest request, HttpServletResponse response) throws Exception
  {
      String collegeId = request.getParameter("collegeId");
    String courseId = request.getParameter("courseId") != null ? request.getParameter("courseId") : "";
    String userId = request.getParameter("userId");
    String enquiryType = request.getParameter("etype");
    String personalNotes = request.getParameter("pernote") != null ? request.getParameter("pernote") : "";
    String message = null;
    try {
      DataModel dataModel = new DataModel();
      Vector vector = new Vector();
      vector.clear();
      vector.add(userId);
      vector.add(collegeId);
      vector.add(courseId);
      vector.add(enquiryType);
      vector.add(personalNotes);
      message = dataModel.getString("WU_USER_PROFILE_PKG.STORE_PERSONAL_NOTES_FN", vector);

    } catch (Exception exception) {
      exception.printStackTrace();
    }
    response.setContentType("text/html");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter outPrint = response.getWriter();
    outPrint.println(message);
    return null;
  }

}
