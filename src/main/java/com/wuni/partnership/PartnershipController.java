package com.wuni.partnership;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * @PartnershipController.java
 * @Version : 1.0
 * @www.whatuni.com
 * @Purpose  :This controller used to display the informaiton of partner content.
 * *************************************************************************************************************************
 * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
 * *************************************************************************************************************************
 * 08-Dec-2020    Hemalatha.K               1.1      Changed middle content as CMS-able
 */

@Controller
public class PartnershipController {
	
  @RequestMapping(value = "/about-us/partners", method = RequestMethod.GET)
  public ModelAndView aboutUsPartners(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
    ServletContext context = request.getServletContext();
    CommonFunction common = new CommonFunction();
    GlobalFunction global = new GlobalFunction();
    String URL_STRING = request.getRequestURI().substring(request.getContextPath().length());
    String canonicalUrl = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + URL_STRING + GlobalConstants.SLASH;  //Added by Sangeeth.S for 25_Sep_18 rel
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { //  TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }    
    try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
        String fromUrl = "/friendsprofile.html";
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    String forwardString = "partnershippage";
    common.loadUserLoggedInformation(request, context, response); // TO LOAD THE USERINFORMATION POD (LOGGED POD) //
    global.removeSessionData(session, request, response);
    request.setAttribute("setAboutusType","Partners");
    request.setAttribute("canonicalUrl", canonicalUrl);
    return new ModelAndView(forwardString);
  }
  
}
