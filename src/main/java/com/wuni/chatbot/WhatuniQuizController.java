package com.wuni.chatbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wuni.util.valueobject.interstitialsearch.InterstitialCourseCountVO;
import com.wuni.valueobjects.chatbot.AnswerOptionVO;
import com.wuni.valueobjects.chatbot.CourseAjaxVO;
import com.wuni.valueobjects.chatbot.ExceptionJSON;
import com.wuni.valueobjects.chatbot.JobIndustryAjaxVO;
import com.wuni.valueobjects.chatbot.PreviousQualVO;
import com.wuni.valueobjects.chatbot.QualSubjectAjaxVO;
import com.wuni.valueobjects.chatbot.QuestionDetailsVO;
import com.wuni.valueobjects.chatbot.QuizDetailsVO;
import com.wuni.valueobjects.whatunigo.GradeFilterValidationVO;
import WUI.chatbot.ChatbotBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;
/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Class         : WhatuniQuizAction.java
 * Description   : Action to get the response for chatbot pod from Whatuni-api webservice.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * Indumathi.S             1.0             13.12.2017       New
 * Sabapathi.S             1.0             28.08.2018       Added additional param for coursecount
 * Sabapathi.S             1.0             20.11.2018       Added additional param for qual and in/out param for selected URL
 * Jeyalakshmi.D           1.1             21.11.2019       Added qualifications pod same as SR grade filter pop up
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */

@Controller
public class WhatuniQuizController {

	
	 @RequestMapping(value = "/chatbot-widget", method = {RequestMethod.GET,RequestMethod.POST})
	  public ModelAndView getChatbotWidget(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		
		 CommonUtil commonUtil = new CommonUtil();
		 String pageName = request.getParameter("pageName");
		    CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		    HttpResponse responsePost;
		    ChatbotBean chatbotBean = new ChatbotBean();
		    ChatbotBean quizPayloadFromClient = new ChatbotBean();
		    Gson gson = null;
		    String responseAsString = null;
		    //
		    //Added qualifications pod same as SR grade filter pop up - Jeya 21 Nov 2019
		    responseAsString = commonUtil.getBody(request);
	        gson = new GsonBuilder().create();
	        if(!GenericValidator.isBlankOrNull(responseAsString)) {
	          quizPayloadFromClient = gson.fromJson(responseAsString, ChatbotBean.class);
	          Object[][] qualDetailsArr;
		      ArrayList<GradeFilterValidationVO> qualSubList = quizPayloadFromClient.getQualSubList();
		      if (qualSubList != null && qualSubList.size() > 0) {
		        qualDetailsArr = new String[qualSubList.size()][5];
		        for (int lp = 0; lp < qualSubList.size(); lp++) {
		          GradeFilterValidationVO gradeFilterValidationVO = qualSubList.get(lp);
		          qualDetailsArr[lp][0] = gradeFilterValidationVO.getQualId();
		          qualDetailsArr[lp][1] = gradeFilterValidationVO.getQualSeqId();
		          qualDetailsArr[lp][2] = gradeFilterValidationVO.getSubId();
		          qualDetailsArr[lp][3] = gradeFilterValidationVO.getGrade();
		          qualDetailsArr[lp][4] = gradeFilterValidationVO.getSubSeqId();
		        }
		        quizPayloadFromClient.setQualDetailsArr(qualDetailsArr);
		      }
	        }
		    quizPayloadFromClient.setUserId(new SessionData().getData(request, "y"));
		    quizPayloadFromClient.setAccessToken(GlobalConstants.CHATBOT_ACCESS_TOKEN);
		    quizPayloadFromClient.setAffiliateId(GlobalConstants.WHATUNI_AFFILATE_ID);
		    // String clearingOnOff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");
		    String clearingFlag = "N";
		    if ("CLEARING_JOURNEY".equalsIgnoreCase(new CommonFunction().getClearingUserJourneyFlag(request))) {
		      clearingFlag = "Y";
		    }
		    quizPayloadFromClient.setClearingUser(clearingFlag);
		    //
		    String domainPath = GlobalConstants.WU_WEB_SERVICE_SCHEME_NAME + request.getServerName();
		
		    //String domainPath = "http://mdev.whatuni.com"; //TODO uncomment this line when we deploy the WAR in LIVE.
		    String httpPostURL = domainPath + "/wuappws/quiz/quiz-details/";   
		    SessionData sessionData = new SessionData();               
		    //
		    if ("set-dimension-ajax".equalsIgnoreCase(pageName)) {
		      String dimension10 = !GenericValidator.isBlankOrNull(request.getParameter("labelText")) ? request.getParameter("labelText") : "";      
		      if(!GenericValidator.isBlankOrNull(dimension10)){
		        sessionData.addData(request, response, "quizDimension10", dimension10);
		      }
		      return null;
		    } else {
		      if ("qual-subject-ajax".equalsIgnoreCase(pageName)) {
		        String subjectText = !GenericValidator.isBlankOrNull(request.getParameter("subjectText")) ? request.getParameter("subjectText") : "";
		        String qualTypeId = !GenericValidator.isBlankOrNull(request.getParameter("qualTypeId")) ? request.getParameter("qualTypeId") : "";
		        quizPayloadFromClient.setSubjectText(subjectText);
		        quizPayloadFromClient.setQualTypeId(qualTypeId);
		        //
		        httpPostURL = domainPath + "/wuappws/quiz/qual-subject-ajax/";
		        //
		      } else if ("course-list-ajax".equalsIgnoreCase(pageName)) {
		        String keywordText = !GenericValidator.isBlankOrNull(request.getParameter("keywordText")) ? request.getParameter("keywordText") : "";
		        String qualCode = !GenericValidator.isBlankOrNull(request.getParameter("qualCode")) ? request.getParameter("qualCode") : "";
		        quizPayloadFromClient.setKeywordText(keywordText);
		        quizPayloadFromClient.setQualificationCode(qualCode); // Added additional input param qualification for keyword ajax 20_Nov_2018
		        //
		        httpPostURL = domainPath + "/wuappws/quiz/course-list-ajax/";
		        //
		      } else if ("job-industry-list-ajax".equalsIgnoreCase(pageName)) {
		        String jobOrIndustry = !GenericValidator.isBlankOrNull(request.getParameter("jobOrIndustry")) ? request.getParameter("jobOrIndustry") : "";
		        quizPayloadFromClient.setJobOrIndustry(jobOrIndustry);
		        //
		        httpPostURL = domainPath + "/wuappws/quiz/job-industry-list-ajax/";
		        //
		      } else {
		        String questionId = !GenericValidator.isBlankOrNull(request.getParameter("questionId")) ? request.getParameter("questionId") : "1";
		        String answer = !GenericValidator.isBlankOrNull(request.getParameter("answer")) ? request.getParameter("answer") : "";
		        String qualificationCode = !GenericValidator.isBlankOrNull(request.getParameter("qualificationCode")) ? request.getParameter("qualificationCode") : "";
		        String categoryCode = !GenericValidator.isBlankOrNull(request.getParameter("categoryCode")) ? request.getParameter("categoryCode") : "";
		        String previousQualGrades = !GenericValidator.isBlankOrNull(request.getParameter("previousQualGrades")) ? request.getParameter("previousQualGrades") : "";
		        String previousQual = !GenericValidator.isBlankOrNull(request.getParameter("previousQual")) ? request.getParameter("previousQual") : "";
		        String qualTypeId = !GenericValidator.isBlankOrNull(request.getParameter("qualTypeId")) ? request.getParameter("qualTypeId") : "";
		        String subject1_id = !GenericValidator.isBlankOrNull(request.getParameter("subject1_id")) ? request.getParameter("subject1_id") : "";
		        String subject2_id = !GenericValidator.isBlankOrNull(request.getParameter("subject2_id")) ? request.getParameter("subject2_id") : "";
		        String subject3_id = !GenericValidator.isBlankOrNull(request.getParameter("subject3_id")) ? request.getParameter("subject3_id") : "";
		        String subject4_id = !GenericValidator.isBlankOrNull(request.getParameter("subject4_id")) ? request.getParameter("subject4_id") : "";
		        String subject5_id = !GenericValidator.isBlankOrNull(request.getParameter("subject5_id")) ? request.getParameter("subject5_id") : "";
		        String subject6_id = !GenericValidator.isBlankOrNull(request.getParameter("subject6_id")) ? request.getParameter("subject6_id") : "";
		        String subj1_tariff_points = !GenericValidator.isBlankOrNull(request.getParameter("subj1_tariff_points")) ? request.getParameter("subj1_tariff_points") : "";
		        String subj2_tariff_points = !GenericValidator.isBlankOrNull(request.getParameter("subj2_tariff_points")) ? request.getParameter("subj2_tariff_points") : "";
		        String subj3_tariff_points = !GenericValidator.isBlankOrNull(request.getParameter("subj3_tariff_points")) ? request.getParameter("subj3_tariff_points") : "";
		        String subj4_tariff_points = !GenericValidator.isBlankOrNull(request.getParameter("subj4_tariff_points")) ? request.getParameter("subj4_tariff_points") : "";
		        String subj5_tariff_points = !GenericValidator.isBlankOrNull(request.getParameter("subj5_tariff_points")) ? request.getParameter("subj5_tariff_points") : "";
		        String subj6_tariff_points = !GenericValidator.isBlankOrNull(request.getParameter("subj6_tariff_points")) ? request.getParameter("subj6_tariff_points") : "";
		        String jacsCode = !GenericValidator.isBlankOrNull(request.getParameter("jacsCode")) ? request.getParameter("jacsCode") : "";
		        //Got the selected filter and decoded it for sending it to Webservice 23_Aug_2018, By Sabapathi.S
		        String selectedFilterUrl = !GenericValidator.isBlankOrNull(request.getParameter("selectedFilterUrl")) ? request.getParameter("selectedFilterUrl") : "";
		        selectedFilterUrl = URLDecoder.decode(selectedFilterUrl, "UTF-8");
		        String dimension11 = !GenericValidator.isBlankOrNull(request.getParameter("dimension11")) ? request.getParameter("dimension11") : "";
		        if(!GenericValidator.isBlankOrNull(dimension11)){
		          sessionData.addData(request, response, "quizDimension11", dimension11);
		        }  
		        request.setAttribute("keywordSearchSubject", "");        
		        String keywordSearchSubject = !GenericValidator.isBlankOrNull(request.getParameter("keywordSearchSubject")) ? request.getParameter("keywordSearchSubject") : "";
		        if(!GenericValidator.isBlankOrNull(keywordSearchSubject)){
		          CommonFunction common = new CommonFunction();
		          request.setAttribute("keywordSearchSubject", common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(keywordSearchSubject))));
		        }          
		        //  
		        quizPayloadFromClient.setQuestionId(questionId);
		        quizPayloadFromClient.setAnswer(answer);
		        quizPayloadFromClient.setQualificationCode(qualificationCode);
		        quizPayloadFromClient.setCategoryCode(categoryCode);
		        quizPayloadFromClient.setPreviousQual(previousQual);
		        quizPayloadFromClient.setPreviousQualGrades(previousQualGrades);
		        quizPayloadFromClient.setQualTypeId(qualTypeId);
		        quizPayloadFromClient.setJacsCode(jacsCode);
		        quizPayloadFromClient.setSubject1_id(subject1_id);
		        quizPayloadFromClient.setSubject2_id(subject2_id);
		        quizPayloadFromClient.setSubject3_id(subject3_id);
		        quizPayloadFromClient.setSubject4_id(subject4_id);
		        quizPayloadFromClient.setSubject5_id(subject5_id);
		        quizPayloadFromClient.setSubject6_id(subject6_id);
		        quizPayloadFromClient.setSubj1_tariff_points(subj1_tariff_points);
		        quizPayloadFromClient.setSubj2_tariff_points(subj2_tariff_points);
		        quizPayloadFromClient.setSubj3_tariff_points(subj3_tariff_points);
		        quizPayloadFromClient.setSubj4_tariff_points(subj4_tariff_points);
		        quizPayloadFromClient.setSubj5_tariff_points(subj5_tariff_points);
		        quizPayloadFromClient.setSubj6_tariff_points(subj6_tariff_points);
		        //Sending it to Webservice 23_Aug_2018, By Sabapathi.S
		        quizPayloadFromClient.setSelectedFilterUrl(selectedFilterUrl);
		        //
		       
		      }
		      //String responseAsString = null;     
		      try {
		        //       
		        gson = new GsonBuilder().create();
		        String json = gson.toJson(quizPayloadFromClient,ChatbotBean.class);     
		        //      
		        HttpPost requestPost = new HttpPost(httpPostURL);
		        StringEntity params = new StringEntity(json); 
		        requestPost.addHeader("content-type", "application/json");
		        requestPost.setEntity(params);             
		        responsePost = httpClient.execute(requestPost);
		        //      
		        String responseStatusCode = responsePost.getStatusLine().toString();
		        responseAsString = EntityUtils.toString(responsePost.getEntity());            
		        //
		        if ("HTTP/1.1 200 OK".equalsIgnoreCase(responseStatusCode)) {       
		          QuizDetailsVO quizDetailsVO = new QuizDetailsVO();
		          quizDetailsVO = new Gson().fromJson(responseAsString, QuizDetailsVO.class);
		          //
		          if ("qual-subject-ajax".equalsIgnoreCase(pageName) && quizDetailsVO.getQualSubjectList() != null) {
		            ArrayList<QualSubjectAjaxVO> qualSubjectAndCourse = quizDetailsVO.getQualSubjectList();
		            if (qualSubjectAndCourse != null && qualSubjectAndCourse.size() > 0) {
		              request.setAttribute("qualSubjectAndCourse", qualSubjectAndCourse);
		            }
		            return new ModelAndView("qualSubjectAndCourseAjax");
		            //
		          } else if ("course-list-ajax".equalsIgnoreCase(pageName) && quizDetailsVO.getCourseList() != null) {
		            ArrayList<CourseAjaxVO> courseAjaxList = quizDetailsVO.getCourseList();
		            if (courseAjaxList != null && courseAjaxList.size() > 0) {
		              request.setAttribute("qualSubjectAndCourse", courseAjaxList);
		            }
		            return new ModelAndView("qualSubjectAndCourseAjax");
		            //
		          } else if ("job-industry-list-ajax".equalsIgnoreCase(pageName) && quizDetailsVO.getJobIndustryList() != null) {
		            ArrayList<JobIndustryAjaxVO> jobIndustryAjaxList = quizDetailsVO.getJobIndustryList();
		            JobIndustryAjaxVO jobIndustryAjaxVO = new JobIndustryAjaxVO();
		            jobIndustryAjaxVO.setJobOrIndustryFlag("0");
		            jobIndustryAjaxVO.setJobOrIndustryId("0");
		            jobIndustryAjaxVO.setJobOrIndustryName("Career not listed");
		            jobIndustryAjaxList.add(jobIndustryAjaxVO);
		            //
		            request.setAttribute("jobIndustryAjaxList", jobIndustryAjaxList);
		            return new ModelAndView("jobIndustryAjax");
		          } else if (quizDetailsVO.getQuestionDetailsList() != null) {
		            ArrayList<QuestionDetailsVO> questionDetailsList = quizDetailsVO.getQuestionDetailsList();
		            if (questionDetailsList != null && questionDetailsList.size() == 1) {
		              request.setAttribute("questionDetailsList", questionDetailsList);
		              QuestionDetailsVO questionDetailsVO = questionDetailsList.get(0);
		              chatbotBean.setQuestionId(questionDetailsVO.getQuestionId());
		              chatbotBean.setQuestionName(questionDetailsVO.getQuestionName());
		            
		              chatbotBean.setTimeDeplay(questionDetailsVO.getTimeDeplay());
		              request.setAttribute("skipToNext", questionDetailsVO.getSkipToNext());
		              request.setAttribute("skipExit", questionDetailsVO.getSkipExit());
		            }
		            //
		            String tempQualGrades = "";
		            ArrayList<PreviousQualVO> previousQualList = quizDetailsVO.getPreviousQualList();
		            if (previousQualList != null && previousQualList.size() == 1) {
		              PreviousQualVO previousQualVO = previousQualList.get(0);
		              request.setAttribute("previousQualification", previousQualVO.getQualification());
		              request.setAttribute("gradeStr", previousQualVO.getGradeStr());
		              request.setAttribute("gradeLevel", previousQualVO.getGradeLevel());
		            } else if (previousQualList != null && previousQualList.size() > 1) {
		              request.setAttribute("previousQualList", previousQualList);
		              if (questionDetailsList != null && questionDetailsList.size() == 1) {
		                QuestionDetailsVO questionDetailsVO = questionDetailsList.get(0);
		                
		                String filterName = questionDetailsVO.getFilterName();
		               
		                if ("Grade1".equalsIgnoreCase(filterName)) {
		                  for (int loop = 0; loop < previousQualList.size(); loop++) {
		                    PreviousQualVO previousQualVO = previousQualList.get(loop);
		                    tempQualGrades += "0" + previousQualVO.getGradeStr() + "-";
		                  }
		                  tempQualGrades = tempQualGrades.substring(0, tempQualGrades.length() - 1);
		                  request.setAttribute("tempQualGrades", tempQualGrades);
		                }
		              }
		            }
		                        
		            //
		            ArrayList<CourseAjaxVO> courseList = quizDetailsVO.getCourseList();
		            if (courseList != null && courseList.size() > 0) {
		              request.setAttribute("courseList", courseList);
		            }
		            //
		            ArrayList<JobIndustryAjaxVO> jobIndustryList = quizDetailsVO.getJobIndustryList();
		            if (jobIndustryList != null && jobIndustryList.size() > 0) {
		              request.setAttribute("jobIndustryList", jobIndustryList);
		            }
		            //
		            ArrayList<QualSubjectAjaxVO> qualSubjectList = quizDetailsVO.getQualSubjectList();
		            if (qualSubjectList != null && qualSubjectList.size() > 0) {
		              request.setAttribute("qualSubjectList", qualSubjectList);
		            }
		            //
		            String courseCount = !GenericValidator.isBlankOrNull(quizDetailsVO.getCourseCount()) ? quizDetailsVO.getCourseCount() : "";
		            request.setAttribute("courseCount", courseCount);
		            // Added additional in/out param selected URL for chatbot 20_Nov_2018
		            String selectedFilterUrl = !GenericValidator.isBlankOrNull(quizDetailsVO.getSelectedFilterUrl()) ? quizDetailsVO.getSelectedFilterUrl() : "";
		            request.setAttribute("selectedFilterUrl", selectedFilterUrl);
		            //
		            ArrayList<AnswerOptionVO> answerOptionsList = questionDetailsList.get(0).getAnswerOptionsList();
		            request.setAttribute("nextQuestionId", "0");
		            if (answerOptionsList != null && answerOptionsList.size() > 0) {
		              AnswerOptionVO answerOptionVO = answerOptionsList.get(0);
		              request.setAttribute("optionText", answerOptionVO.getOptionText());
		              request.setAttribute("answerOptionsList", answerOptionsList);
		              request.setAttribute("nextQuestionId", answerOptionVO.getNextQuestionId());
		              if (answerOptionsList.size() > 1) {
		                AnswerOptionVO answerOptVO = answerOptionsList.get(1);
		                request.setAttribute("multipleNextQuestionId", answerOptVO.getNextQuestionId());
		              }
		            }
		            Iterator itr = answerOptionsList.iterator();
		            while (itr.hasNext()) {
		              AnswerOptionVO answerOptionVO = (AnswerOptionVO)itr.next();
		              String image = !GenericValidator.isBlankOrNull(answerOptionVO.getOptionValue()) ? answerOptionVO.getOptionValue().toLowerCase().replaceAll(" ", "_") : "";
		              answerOptionVO.setImageName(image);
		              String optionText = !GenericValidator.isBlankOrNull(answerOptionVO.getOptionText()) ? answerOptionVO.getOptionText().replaceAll("'", "&#39;") : "";
		              answerOptionVO.setOptionText(optionText);
		            }
		            // 
		            ArrayList<QuestionDetailsVO> subQuestionDetails = questionDetailsList.get(0).getQuestionList();
		            if (subQuestionDetails != null && subQuestionDetails.size() > 0) {
		              request.setAttribute("subQuestionDetails", subQuestionDetails);
		            }
		          }
		          //           
		          if ("quizResults".equalsIgnoreCase(pageName)) {
		            return new ModelAndView("quizDetailsPage");
		          }
		        } else {        
		          ExceptionJSON exceptionJSON = new ExceptionJSON();
		          exceptionJSON = new Gson().fromJson(responseAsString, ExceptionJSON.class);
		          int statusCode = !GenericValidator.isBlankOrNull(exceptionJSON.getStatus()) ? Integer.parseInt(exceptionJSON.getStatus()) : 0;
		          response.sendError(statusCode, exceptionJSON.getException());
		        }
		      } catch (Exception securityException) {      
		        String exceptionTraces = ExceptionUtils.getStackTrace(securityException);
		           
		      } finally {
		        httpClient.close();
		      }    
		      return new ModelAndView("quizMainPage","chatbotBean",chatbotBean);
		    }
  	 }
}

