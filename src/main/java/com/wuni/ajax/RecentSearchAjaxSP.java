package com.wuni.ajax;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.valueobject.AutoCompleteVO;
/**
  * @see This SP class is used to call the database procedure to get the recent search ajax list
  * @author Sangeeth.S
  * @since  28.08.2018- intital draft
  * @version 1.0
  *
  * Modification history:
  * **************************************************************************************************************
  * Author          Relase tag              Modification Details
  * **************************************************************************************************************
  * Sangeeth.S     wu_2018_08_28            initial draft
  *
*/
public class RecentSearchAjaxSP extends StoredProcedure
{

  public RecentSearchAjaxSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql("HOT_WUNI.WU_UTIL_PKG.GET_USER_RECENT_SEARCH_PRC");
    declareParameter(new SqlParameter("P_USER_ID", OracleTypes.VARCHAR));    
    declareParameter(new SqlParameter("P_USER_SESSION_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_QUAL_CODE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_RECENT_SEARCH", OracleTypes.CURSOR, new RecentSearchRowmapperImpl()));    
  }
  public Map execute(AutoCompleteVO autoCompleteVO)
  {
    Map outMap = null;
    try{
      Map inMap = new HashMap();
      inMap.put("P_USER_ID", autoCompleteVO.getUserId());
      inMap.put("P_USER_SESSION_ID", autoCompleteVO.getTrackSessionId());
      inMap.put("P_QUAL_CODE", autoCompleteVO.getNavQual());            
      outMap = execute(inMap);      
    }catch(Exception ex){
      ex.printStackTrace();
    }
    return outMap;
  }
  
  private class RecentSearchRowmapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      AutoCompleteVO autoCompleteVO = new AutoCompleteVO();      
      autoCompleteVO.setDescription(rs.getString("DESCRIPTION"));      
      autoCompleteVO.setUrl(rs.getString("URL"));
      autoCompleteVO.setBrowseCatId(rs.getString("BROWSE_CAT_ID"));
      autoCompleteVO.setOrd(rs.getString("ORD"));
      return autoCompleteVO;
    }
  }
}



