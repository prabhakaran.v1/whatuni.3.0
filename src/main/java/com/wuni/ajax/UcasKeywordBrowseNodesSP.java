package com.wuni.ajax;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.util.valueobject.AutoCompleteVO;

/**
   * @UcasKeywordBrowseNodesSP
   * @author Thiyagu G
   * @version 1.0
   * @since 16-Feb-2015
   * @purpose  This program is used call the database procedure to submit the UCAS subjects.
   * Change Log
   * *************************************************************************************************************************
   * Date           Name                      Ver.       Changes desc                                      Rel Ver.
   * *************************************************************************************************************************
   * 16-Feb-2015  Thiyagu G                 1.0        Initial draft to spring                           1.0
   */
   
public class UcasKeywordBrowseNodesSP extends StoredProcedure
{
  public UcasKeywordBrowseNodesSP(DataSource datasource){
    setDataSource(datasource);
    setSql(GlobalConstants.GET_SEARCH_SUBJECT_LIST_PRC);
    declareParameter(new SqlParameter("P_KEYWORD", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("P_QUALIFICATION", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("PC_SUBJECT_LIST", OracleTypes.CURSOR, new subjectListRowmapperImpl()));
    declareParameter(new SqlOutParameter("P_BROWSE_CATEGORY_CODE", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_BROWSE_CAT_ID", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("P_BROWSE_TEXT_KEY", OracleTypes.VARCHAR));    
  }
  public Map execute(List inputList){
    Map outMap = null;
    try{
      Map inMap = new HashMap();
      inMap.put("P_KEYWORD", inputList.get(0));
      inMap.put("P_QUALIFICATION", inputList.get(1));      
      outMap = execute(inMap);      
    }catch(Exception e){
      e.printStackTrace();
    }    
    return outMap;
  }
  
  private class subjectListRowmapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      AutoCompleteVO autoCompleteVO = new AutoCompleteVO();      
      autoCompleteVO.setSubject(rs.getString("description"));
      autoCompleteVO.setUrl(rs.getString("url"));
      autoCompleteVO.setCatCode(rs.getString("category_code"));
      autoCompleteVO.setBrowseCatId(rs.getString("browse_cat_id"));      
      return autoCompleteVO;
    }
  }
}
