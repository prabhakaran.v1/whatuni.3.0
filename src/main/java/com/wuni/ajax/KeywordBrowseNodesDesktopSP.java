package com.wuni.ajax;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.valueobject.AutoCompleteVO;

public class KeywordBrowseNodesDesktopSP extends StoredProcedure
{

  public KeywordBrowseNodesDesktopSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql("WHATUNI_SEARCH_PKG.get_subject_list_prc");
    declareParameter(new SqlParameter("p_keyword", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_qualification", OracleTypes.VARCHAR));  
    declareParameter(new SqlOutParameter("pc_subject_list", OracleTypes.CURSOR, new subjectListRowmapperImpl()));
    declareParameter(new SqlOutParameter("p_browse_category_code", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_browse_cat_id", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_browse_text_key", OracleTypes.VARCHAR));// Added by widgetteam  for weblogic ssl migration
    declareParameter(new SqlParameter("p_survey_flag", OracleTypes.VARCHAR));

  }
  public Map execute(List inputList)
  {
    Map outMap = null;
    try{
    Map inMap = new HashMap();
    inMap.put("p_keyword", inputList.get(0));
    inMap.put("p_qualification", inputList.get(1));
    inMap.put("p_survey_flag", inputList.get(2));
    outMap = execute(inMap);
    }catch(Exception ex){
     ex.printStackTrace();
    }
    return outMap;
  }
  
  private class subjectListRowmapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
      autoCompleteVO.setCatCode(rs.getString("category_code"));
      autoCompleteVO.setSubject(rs.getString("description"));
      autoCompleteVO.setBrowseCatId(rs.getString("browse_cat_id"));
      autoCompleteVO.setUrl(rs.getString("url"));
      return autoCompleteVO;
    }
  }
}


