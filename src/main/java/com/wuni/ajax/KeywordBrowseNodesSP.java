package com.wuni.ajax;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.valueobject.AutoCompleteVO;

public class KeywordBrowseNodesSP extends StoredProcedure
{

  public KeywordBrowseNodesSP(DataSource datasource, String dbObject)
  {
    setDataSource(datasource);
    setSql(dbObject);
    declareParameter(new SqlParameter("p_keyword", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_qualification", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("pc_subject_list", OracleTypes.CURSOR, new subjectListRowmapperImpl()));
    declareParameter(new SqlOutParameter("p_browse_category_code", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_browse_cat_id", OracleTypes.VARCHAR));
  }
  public Map execute(List inputList)
  {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_keyword", inputList.get(0));
    inMap.put("p_qualification", inputList.get(1));
    outMap = execute(inMap);
    return outMap;
  }
  
  private class subjectListRowmapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
      autoCompleteVO.setCatCode(rs.getString("category_code"));
      autoCompleteVO.setSubject(rs.getString("subject"));
      autoCompleteVO.setBrowseCatId(rs.getString("browse_cat_id"));
      return autoCompleteVO;
    }
  }
}
