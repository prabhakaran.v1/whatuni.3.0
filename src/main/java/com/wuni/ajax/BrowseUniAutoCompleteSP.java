package com.wuni.ajax;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.valueobject.AutoCompleteVO;

public class BrowseUniAutoCompleteSP extends StoredProcedure
{

  public BrowseUniAutoCompleteSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql("wuni_prospectus_pkg.get_ajax_brwse_pros_fn");
    setFunction(true);
    declareParameter(new SqlOutParameter("pc_subject_list", OracleTypes.CURSOR, new subjectListRowmapperImpl()));
    declareParameter(new SqlParameter("p_keyword_text", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_qualification_code", OracleTypes.VARCHAR));

  }
  public Map execute(List inputList)
  {
    Map outMap = null;
    Map inMap = new HashMap();
    try{ 
    inMap.put("p_keyword_text", inputList.get(0));
    inMap.put("p_qualification_code", inputList.get(1));
    outMap = execute(inMap);
    }catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
  
  private class subjectListRowmapperImpl implements RowMapper
  {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
      AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
      autoCompleteVO.setCatCode(rs.getString("category_code"));
      autoCompleteVO.setSubject(rs.getString("description"));
      autoCompleteVO.setBrowseCatId(rs.getString("browse_cat_id"));
      autoCompleteVO.setUrl(rs.getString("url"));
      return autoCompleteVO;
    }
  }
}
