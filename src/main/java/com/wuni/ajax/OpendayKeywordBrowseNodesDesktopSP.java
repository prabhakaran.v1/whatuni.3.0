package com.wuni.ajax;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import com.wuni.util.valueobject.AutoCompleteVO;

public class OpendayKeywordBrowseNodesDesktopSP extends StoredProcedure
{

  public OpendayKeywordBrowseNodesDesktopSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql("wuni_opendays_pkg.get_opendays_list_prc");
    declareParameter(new SqlParameter("p_keyword", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("oc_opendays_list", OracleTypes.CURSOR, new opendaysListRowmapperImpl()));
    declareParameter(new SqlOutParameter("o_opendays_url", OracleTypes.VARCHAR));    
    declareParameter(new SqlOutParameter("o_college_name", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_college_id", OracleTypes.VARCHAR));

  }
  public Map execute(List inputList)
  {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_keyword", inputList.get(0));    
    outMap = execute(inMap);
    return outMap;
  }
  
  private class opendaysListRowmapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException { //college_name_display, opendays_url, college_id
      AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
      autoCompleteVO.setCollegeNameDisplay(rs.getString("college_name_display"));
      autoCompleteVO.setOpendaysUrl(rs.getString("opendays_url"));
      autoCompleteVO.setCollegeId(rs.getString("college_id"));     
      autoCompleteVO.setOpenDayMessage(rs.getString("od_msg"));
      return autoCompleteVO;
    }
  }
}


