package com.wuni.ajax.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import WUI.utilities.CookieManager;


@Controller
public class CreateGradePopupCookie {
	@RequestMapping(value="/create-grade-popup-cookie", method=RequestMethod.POST)	
	  public ModelAndView createGradePopup(HttpServletRequest request, HttpServletResponse response) throws Exception {
	    String cookieProcessType = request.getParameter("processType");
		String cookieName = request.getParameter("cookieName");		
	    String cookieValue = request.getParameter("cookieValue");
	    if("get".equalsIgnoreCase(cookieProcessType)){
	      response.setContentType("text/html");
	      response.setHeader("Cache-Control", "no-cache");         
	      response.getWriter().write("YES");   
	    }else{
	      if(!GenericValidator.isBlankOrNull(cookieName)){           
	        Cookie cookie = CookieManager.createCookie(request,cookieName, cookieValue);
	        response.addCookie(cookie);
	        response.setContentType("text/html");
	        response.setHeader("Cache-Control", "no-cache");   
	        response.getWriter().write("SUCEESS");
	      }
	    }
	    return null;
	  }
}
