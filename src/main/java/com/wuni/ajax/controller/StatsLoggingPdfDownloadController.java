package com.wuni.ajax.controller;

import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.AutoCompleteVO;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 */
@Controller
public class StatsLoggingPdfDownloadController{
  
  @RequestMapping(value = "/statsajaxpdfdownload", method = RequestMethod.POST)
  public ModelAndView execute(HttpServletRequest request, HttpServletResponse response) {
    //http://www.whatuni.com/degrees/ajax/autocomplete.html?t=[Z/W]&l=[FREE_TEXT]&z=[COLLEGE_ID]&r=[RAND_NO]
    String pdfId = request.getParameter("pdfid");
    String userid = request.getParameter("userid");
    logStatsForPDFDownload(pdfId, userid);
    return null;
  }
  
  public void logStatsForPDFDownload(String pdfId, String userid){  
    DataModel datamodel = new DataModel();
    ResultSet resultset = null;
    Vector parameters = new Vector();
    parameters.add(pdfId);
    parameters.add(userid);
    try {
      datamodel.getString("hot_wuni.wuni_articles_pkg.add_pdf_logs_fn", parameters);         
      //
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      //close opened IO-objcects, resultsets, and nullify unused objects.
       try {
         datamodel.closeCursor(resultset);
         if (resultset != null) {
           resultset.close();
         }
         resultset = null;
       } catch (Exception e) {
         e.printStackTrace();
       }
       //nullify unused objects
       datamodel = null;
    }
  }
}
