package com.wuni.ajax.controller;

import WUI.utilities.CommonFunction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mobile.util.MobileUtils;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
  * @PageImpressionJSLog
  * @author Pryaa Parthasarathy
  * @version 1.0
  * @since 3-Jun-2014
  * @purpose This program is used to log some page impressions stats log
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  * 23-Oct-2018    Sabapathi S.              1.3      Added extra conditions for clearing home page stats          wu_582
  */
@Controller
public class PageImpressionJSLog {

  @RequestMapping(value = "/pageimpression-jslog")
  public ModelAndView pageImpressionJsLog(HttpServletRequest request, HttpServletResponse response, HttpSession session)
  {
    String id = request.getParameter("id") != null && request.getParameter("id").trim().length() > 0 ? request.getParameter("id").trim() : "0";
    String pName = request.getParameter("pagename") != null && request.getParameter("pagename").trim().length() > 0 ? request.getParameter("pagename").trim() : "";
    String pageName = null;
    if("CD".equalsIgnoreCase(pName)){
      pageName = "COURSE_DETAIL";
    }else if("CLCD".equalsIgnoreCase(pName)){
      pageName = "CLEARING_COURSE_DETAIL";
    }else if("CLHM".equalsIgnoreCase(pName)){ // wu582_20181023 - Sabapathi: Added extra conditions for clearing home page stats
      pageName = "CLEARING_HOME_PAGE";
    }
    String mobileFlag = "";
    boolean autoEmailLink = false;
    if(new MobileUtils().mobileUserAgentCheck(request)){
      mobileFlag = "Y";
    }
    String statLogId = "";
    statLogId = new CommonFunction().collegeProfileTracker(id, pageName, request, "", session); //DB_CALL
    return null;
  }
}
