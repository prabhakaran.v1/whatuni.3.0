package com.wuni.ajax.controller;

import WUI.review.bean.ReviewListBean;
import WUI.utilities.CommonUtil;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;
import com.wuni.util.valueobject.AutoCompleteVO;
import com.wuni.util.valueobject.review.UserReviewsVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GetSubjectReviewsController {
	
	@RequestMapping(value = "/get-subject-review", method = RequestMethod.POST)
    public ModelAndView getSubjectReview(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
      String collegeId = request.getParameter("collegeId");
      String subjectId = request.getParameter("subjectId");
      CommonUtil util = new CommonUtil();
      AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
      autoCompleteVO.setCollegeId(collegeId);
      autoCompleteVO.setSubjectId(subjectId);
      Map subjectReviewsMap = new HashMap();
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      subjectReviewsMap = commonBusiness.getSubjectReviews(autoCompleteVO);
      ArrayList latestSubjectAjaxReview = (ArrayList)subjectReviewsMap.get("SUBJECT_REVIEW_LIST");
      if (latestSubjectAjaxReview != null && latestSubjectAjaxReview.size() > 0) {
        request.setAttribute("latestSubjectAjaxReview", latestSubjectAjaxReview);
      }
      return new ModelAndView("profilePage");
    }
}
