package com.wuni.ajax.controller;

import com.wuni.util.CommonValidator;
import com.wuni.util.autocomplete.AutoCompleteUtilities;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * AJAX Auto-complete for college name and course title.
 * Previouly these possible college/course names were populated by STATIC block
 * now these possible college/course names will be populated by dynamic DB call
 *
 * @since         wu316_20110823
 * @author        Mohamed Syed
 *
 * URL_PATTERN:     http://www.whatuni.com/degrees/ajax/autocomplete.html?t=[Z/W]&l=[FREE_TEXT]&z=[COLLEGE_ID]&r=[RAND_NO]
 * URL_EXAMPLE:     http://www.whatuni.com/degrees/ajax/autocomplete.html?t=w&l=bus&z=3469&r=1311851420467
 * QUERY_STRING1:   t = type (which means college_name/course_title autocomplete; possible values = z/w; z=college, w=course)
 * QUERY_STRING2:   l = letters (which means the free text which was typed in the input box, it may be college/course name)
 * QUERY_STRING3:   z = collegeId (this is notnull for courseTile autocomplete)
 * QUERY_STRING4:   r = randomly generated number.
 *
 * Modification history:
 * **************************************************************************************************************
 * Author     Relase Date              Modification Details
 * **************************************************************************************************************
 * Yogeswari  19.05.2015               Changed for adding ajax keyword search in compare page
 * Sabapathi  28.08.2018               Added interstitaial subject ajax
 * Sangeeth   28.11.2018               To get review subject ajax search in read reiews page 
 */

@Controller
public class AutoCompleteController{
  
  @RequestMapping(value="/ajax/autocomplete", method=RequestMethod.POST)	
  public String ajaxAutocomplete(Model model, HttpServletRequest request, HttpServletResponse response) {
    //http://www.whatuni.com/degrees/ajax/autocomplete.html?t=[Z/W]&l=[FREE_TEXT]&z=[COLLEGE_ID]&r=[RAND_NO]
    String autocompleteType = request.getParameter("t");
    String freeText = request.getParameter("l");
    String collegeId = request.getParameter("z");
    String randomSequence = request.getParameter("r");
    String subOrderItemId = request.getParameter("subOrderItemId");
    String qualification = request.getParameter("qual");
    String siteFlag = "desktop";
    siteFlag = request.getParameter("siteflag");
    String searchType = request.getParameter("type"); //3_JUN_2014
    String userId = request.getParameter("userId");
    String navQual = request.getParameter("navQual");
    //
    AutoCompleteUtilities autoCompleteUtil = new AutoCompleteUtilities();
    CommonValidator validate = new CommonValidator();
    //    
    if (validate.isNotBlankAndNotNull(autocompleteType) && (autocompleteType.equals("z") || autocompleteType.equalsIgnoreCase("reviewUniSearch") || autocompleteType.equalsIgnoreCase("topNavUniSearch"))) {
      if(autocompleteType.equalsIgnoreCase("topNavUniSearch")){
        try {         
          freeText = URLDecoder.decode(freeText, "UTF-8");         
          autoCompleteUtil.populateCollegeNameAutoComplete(freeText, searchType, response);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }else{
        autoCompleteUtil.populateCollegeNameAutoComplete(freeText, searchType, response); //3_JUN_2014
      }
    } else if (validate.isNotBlankAndNotNull(autocompleteType) && autocompleteType.equals("w")) {
      autoCompleteUtil.populateCourseTitleAutoComplete(freeText, collegeId, response);
    }else if (validate.isNotBlankAndNotNull(autocompleteType) && autocompleteType.equals("coursesearchsurvey")) {
      autoCompleteUtil.populateSurveyCourseTitleAutoComplete(freeText, collegeId, response);
    }else if (validate.isNotBlankAndNotNull(autocompleteType) && autocompleteType.equals("std")) {
      autoCompleteUtil.populateCourseTitleAutoCompleteInMobile(freeText, collegeId, response);
    } else if (!GenericValidator.isBlankOrNull(autocompleteType) && "courselist".equals(autocompleteType)) {
      autoCompleteUtil.courseOfInterestAutoComplete(freeText, subOrderItemId, collegeId, response);
    } else if (!GenericValidator.isBlankOrNull(autocompleteType) && "ukschoollist".equals(autocompleteType)) {
      autoCompleteUtil.ukSchoolListAutoComplete(freeText, response);
    } else if (!GenericValidator.isBlankOrNull(autocompleteType) && "firstcourse".equalsIgnoreCase(autocompleteType)) {
      autoCompleteUtil.firstUniversityCourses(freeText, collegeId, response);
    } else if (!GenericValidator.isBlankOrNull(autocompleteType) && "keywordbrowse".equalsIgnoreCase(autocompleteType) && "mobile".equalsIgnoreCase(siteFlag)) {
      autoCompleteUtil.keywordBrowseNodesMobile(freeText, response, qualification, siteFlag);
    } else if (!GenericValidator.isBlankOrNull(autocompleteType) && ("keywordbrowse".equalsIgnoreCase(autocompleteType) || "mobkeywordbrowse".equalsIgnoreCase(autocompleteType)) && ("desktop".equalsIgnoreCase(siteFlag) ||"desktopsurvey".equalsIgnoreCase(siteFlag) ) ) {//Added by Priyaa for 21_JULY_2015_REL For Student Survey 
      autoCompleteUtil.keywordBrowseNodesDesktop(freeText, response, qualification, siteFlag); //Added condition for 'mobkeywordbrowse' mobile search by Thiyagu G for 21_Mar_2017
    } else if (!GenericValidator.isBlankOrNull(autocompleteType) && "qualsubects".equals(autocompleteType)) {
      String ultimateQual = request.getParameter("ulqual");
      autoCompleteUtil.ultimateQualSubject(freeText, response, ultimateQual);
    } else if (!GenericValidator.isBlankOrNull(autocompleteType) && "uninamelist".equals(autocompleteType)) {
      String ultimateQual = request.getParameter("jacsCode");
      autoCompleteUtil.universitySearchList(freeText, response, ultimateQual);
    } else if (!GenericValidator.isBlankOrNull(autocompleteType) && "qualuniofinterstsubects".equals(autocompleteType)) {
      autoCompleteUtil.ultimateUniOfInterestQualSubject(freeText, response);
    } else if (!GenericValidator.isBlankOrNull(autocompleteType) && "jobOrIndustry".equals(autocompleteType)) {
      autoCompleteUtil.ultimateChooseJobOrIndustry(freeText, response);//Added job or industry ajax call Indumathi.S Feb-16-2016
    } else if (validate.isNotBlankAndNotNull(autocompleteType) && autocompleteType.equals("uniAjax")) {
      autoCompleteUtil.popualteCollegeNamesMobile(freeText, response); //15_JUL_2014
    } else if (validate.isNotBlankAndNotNull(autocompleteType) && autocompleteType.equals("prospectusbrowse")) {
      autoCompleteUtil.prospectusBrowseNodesDesktop(freeText, response, qualification); //28_OCT_2014
    } else if (validate.isNotBlankAndNotNull(autocompleteType) && "opdkeywordbrowse".equalsIgnoreCase(autocompleteType) && "opddesktop".equalsIgnoreCase(siteFlag)) {
      autoCompleteUtil.opendaysBrowseNodesDesktop(freeText, response, qualification); //To get the information about the openday ajax search, 17_Mar_2015 By Thiyagu G
    }else if (!GenericValidator.isBlankOrNull(autocompleteType) && "clkeywordbrowse".equalsIgnoreCase(autocompleteType) && "desktop".equalsIgnoreCase(siteFlag)) {
      autoCompleteUtil.keywordBrowseNodesDesktop(freeText, response, qualification, siteFlag);
    }
    /* ::START:: Yogeswari :: 19.05.2015 :: Changed for adding ajax keyword search in compare page */
    else if (!GenericValidator.isBlankOrNull(autocompleteType) && "compKeywordbrowse".equalsIgnoreCase(autocompleteType) && "mobile".equalsIgnoreCase(siteFlag)) {
      autoCompleteUtil.keywordBrowseNodesMobile(freeText, response, qualification, siteFlag);
    } else if (!GenericValidator.isBlankOrNull(autocompleteType) && "compKeywordbrowse".equalsIgnoreCase(autocompleteType) && "desktop".equalsIgnoreCase(siteFlag)) {
      autoCompleteUtil.keywordBrowseNodesDesktop(freeText, response, qualification, siteFlag);
    }
    /* ::END:: Yogeswari :: 19.05.2015 :: Changed for adding ajax keyword search in compare page */
    else if (!GenericValidator.isBlankOrNull(autocompleteType) && "csKeywordbrowse".equalsIgnoreCase(autocompleteType) && "desktop".equalsIgnoreCase(siteFlag)) {
      autoCompleteUtil.keywordBrowseNodesDesktop(freeText, response, qualification, siteFlag);
    } else if (!GenericValidator.isBlankOrNull(autocompleteType) && "ucasqualsubects".equals(autocompleteType)) { //To get ucas qual subject list for 16_Feb_2016, By Thiyagu G.      
      autoCompleteUtil.ucasQualSubject(freeText, response);
    }else if (!GenericValidator.isBlankOrNull(autocompleteType) && "ucaskeywordbrowse".equalsIgnoreCase(autocompleteType)) {
      autoCompleteUtil.ucaskeywordBrowseNodes(freeText, response, "M", siteFlag);
    }else if(validate.isNotBlankAndNotNull(autocompleteType) && "opendaysearch".equalsIgnoreCase(autocompleteType)){
       autoCompleteUtil.populateOpendaysCollegeNameAutoComplete(freeText, "OD", response);
    }else if(!GenericValidator.isBlankOrNull(autocompleteType) && "recentAndAdvSrch".equalsIgnoreCase(autocompleteType)){ //To get the recent and advanced search link when user click the empty search nav for 31_July_rel,By Samgeeth.S       
       autoCompleteUtil.getRecentSearchResultList(navQual, userId, request, response);
    }else if(validate.isNotBlankAndNotNull(autocompleteType) && "interstitialsearch".equalsIgnoreCase(autocompleteType)) { //Added this for interstitial search page 23_Aug_2018, By Sabapathi.S
     try {
        String qualCode = request.getParameter("qual");
        String l1Flag = request.getParameter("l1flag");
        String pageName = request.getParameter("pagename");
        String parentCategoryId = request.getParameter("parentcatid");
        //decoded freetext user may give special symbols, 25_Sep_2018 By Sabapathi
        freeText = URLDecoder.decode(freeText, "UTF-8");
        autoCompleteUtil.populateInterstitialSubjectListAutoComplete(freeText, qualCode, l1Flag, parentCategoryId, pageName, response);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }else if(!GenericValidator.isBlankOrNull(autocompleteType) && "reviewSubjectSearch".equalsIgnoreCase(autocompleteType)){ // To get review subject ajax search in read reiews page for DEC_18 rel added by Sangeeth.S
       autoCompleteUtil.populateReviewSubjectNameAutoComplete(freeText,"", response);
    }else if(!GenericValidator.isBlankOrNull(autocompleteType) && "coursekeywordbrowse".equalsIgnoreCase(autocompleteType) && "coursedesktop".equalsIgnoreCase(siteFlag)){ // To get review subject ajax search in read reiews page for DEC_18 rel added by Sangeeth.S
       autoCompleteUtil.populateCourseNameAutoComplete(freeText, response);
    }else if(!GenericValidator.isBlankOrNull(autocompleteType) && "subjectSearchTopNav".equalsIgnoreCase(autocompleteType)){
       try {         
         freeText = URLDecoder.decode(freeText, "UTF-8");         
         autoCompleteUtil.subjectSearchTopNavAutoComplete(freeText, response, qualification, siteFlag);
       } catch (Exception e) {
         e.printStackTrace();
       }
    }
    // 
    //nullify unused objects
    autocompleteType = null;
    freeText = null;
    collegeId = null;
    randomSequence = null;
    autoCompleteUtil = null;
    validate = null;
    //
    return null;
  }

}
