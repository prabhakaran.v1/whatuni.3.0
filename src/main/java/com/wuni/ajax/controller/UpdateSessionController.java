package com.wuni.ajax.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.utilities.CommonFunction;
import WUI.utilities.SessionData;


@Controller
public class UpdateSessionController {

  @RequestMapping(value = "/session/chatbot-disabled", method = {RequestMethod.GET,RequestMethod.POST})
  public ModelAndView updateChatbotSession(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {

	session = request.getSession();
	session.setAttribute("chatbot_promo_disabled", "Y");
	//
	if (session.getAttribute("userInfoList") != null) {
	  String userId = new SessionData().getData(request, "y");
	  new CommonFunction().updateChatbotDisabled(userId);
	}
	//
	response.setContentType("text/html");
	response.setHeader("Cache-Control", "no-cache");
	response.getWriter().write("SUCEESS");
	//
	return null;
  }
}
