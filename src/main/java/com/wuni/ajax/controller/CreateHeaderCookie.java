package com.wuni.ajax.controller;

import WUI.profile.bean.ProfileBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.layer.util.SpringConstants;

/**
  * @CreateHeaderCookie
  * @author Hema.S
  * @version 1.0
  * @since 09.07.2018
  * @purpose  This program is used to manage cookie in java layer through ajax call from js end.
  * Change Log
  * *************************************************************************************************************************
  * Date                 Name                      Ver.       Changes desc                                          Rel Ver.
  * *************************************************************************************************************************
  * 31-July-2018        Hema.S                     1.0        Added Create cookies method                              1.0
  * 18-September-2019   Sangeeth.S                 1.1        Added get cookie value in java layer                
  *                                                           through ajax call from js end
  * 22-October-2019     Sangeeth.S                 1.2        Added showing cookie popup logic dynamically whenever
  *                                                            cookie or privacy or terms & conditions is updated by
  * 21-September-2020   Sangeeth.s                 1.3        Added getval if block for fetching the data through 
  * 															ajax response from angular layer for review form                                          
  * 10_NOV_2020         Sujitha V                  1.4        Added session for YOE to log in d_session_log.	                     
  *                                                            
  * 10_NOV_2020         Sangeeth.S				   1.5		  Added cookie consent flag in angular hit response                                               
  */
  
@Controller
public class CreateHeaderCookie{
	
  CookieManager cookieManager = new CookieManager();
  CommonFunction commonFunction = new CommonFunction();
  SimpleDateFormat simpleDateFormat= new SimpleDateFormat(GlobalConstants.COOKIE_DATE_FORMAT);//dd-MM-yyyy HH:mm:ss - 24 hourse format 
  
  @RequestMapping(value="/cookies/create-cookie-popup", method=RequestMethod.POST) 
  public ModelAndView createCookiePopup(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
        
    Date date = new Date();      
    String cookieProcessType = request.getParameter("processType");
	  String cookieName = request.getParameter("cookieName");
    String cookieValue = request.getParameter("cookieValue");
    String cookieAcceptedDate =  null;
    String cookieLatestUpdatedDate = null;
    boolean showCookieFlag = false;
    if("get".equalsIgnoreCase(cookieProcessType)){
      showCookieFlag = checkCookieBanner(cookieAcceptedDate, cookieLatestUpdatedDate, cookieName, request);
      response.setContentType("text/html");
      response.setHeader("Cache-Control", "no-cache");         
      response.getWriter().write(showCookieFlag ? "YES" : "NO");      //YES - show cookie popup, NO - hide cookie popup
    }else if("getVal".equalsIgnoreCase(cookieProcessType)){
     SessionData sessionData = new SessionData();
     String sessionUserId = sessionData.getData(request, "y");
     String userTrackId = sessionData.getData(request, "userTrackId");     
     String userId = "";
     String userName = "";
     String ucasTariff = (String)session.getAttribute("USER_UCAS_SCORE");   
     String yearOfEntryForDSession = StringUtils.isNotBlank((String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING)) ? (String)session.getAttribute(SpringConstants.SESSION_YOE_LOGGING) : null; //Added for getting YOE value in session to log in d_session_log stats by Sujitha V on 2020_NOV_10 rel
     String[] latAndLong = sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG).split(GlobalConstants.SPLIT_CONSTANT);
	 String latitude = (latAndLong.length > 0 && !GenericValidator.isBlankOrNull(latAndLong[0])) ? latAndLong[0].trim() : "";
	 String longitude = (latAndLong.length > 1 && !GenericValidator.isBlankOrNull(latAndLong[1])) ? latAndLong[1].trim() : "";
	 String splashCookieName = GlobalConstants.COOKIE_SPLASH_FLAG_COOKIE;
	 String cookieConsent = new CookieManager().getCookieValue(request, GlobalConstants.COOKIE_CONSENT_COOKIE);
	 cookieConsent = cookieConsent != null ? cookieConsent : GlobalConstants.CONSENT_COOKIE_DEFAULT_VAL;	
	 showCookieFlag = checkCookieBanner(cookieAcceptedDate, cookieLatestUpdatedDate, splashCookieName, request);
     String responseText = "";
     response.setContentType("text/plain");     
     response.setHeader("Cache-Control", "no-cache"); 
    	 if (!GenericValidator.isBlankOrNull(sessionUserId) && !"0".equals(sessionUserId)) {
    		 userId = sessionUserId;     
   	     }
    	 ArrayList userInfoList = session.getAttribute("userInfoList") != null ?(ArrayList)session.getAttribute("userInfoList") : null;
    	 if(!CollectionUtils.isEmpty(userInfoList)){
    		 ProfileBean profileBean =(ProfileBean)userInfoList.get(0);
    		 userName = profileBean.getUserName();
    	 }    	 
    	 JSONObject jsonObject = new JSONObject();
    	 jsonObject.put("userId",userId);
    	 jsonObject.put("userTrackId",userTrackId);
    	 jsonObject.put("ucasTariff",ucasTariff);
    	 jsonObject.put("latitude",latitude);
    	 jsonObject.put("longitude",longitude);
    	 jsonObject.put("userName",userName);
    	 jsonObject.put("yearOfEntry", yearOfEntryForDSession);
    	 jsonObject.put("showCookieFlag",showCookieFlag ? "YES" : "NO");
	 jsonObject.put("cookieConsent",cookieConsent);
	 responseText = "{\"userValues\":" + jsonObject + "}";    
 	 response.getWriter().write(!GenericValidator.isBlankOrNull(responseText)?responseText :"");   
     return null;
    }else{
      if(!GenericValidator.isBlankOrNull(cookieName)){           
        cookieAcceptedDate = simpleDateFormat.format(date);                
        cookieValue = cookieAcceptedDate;
        Cookie cookie = CookieManager.createCookie(request, cookieName, cookieValue);
        response.addCookie(cookie);
        if("cookie_splash_flag".equalsIgnoreCase(cookieName)){
          String cookieConsentVal = "0000"; //0-Strictly necessary, 0 - Functional, 0 - performance , 0- target
	      Cookie cookieConsent = CookieManager.createCookie(request,GlobalConstants.COOKIE_CONSENT_COOKIE, cookieConsentVal);	    
	      response.addCookie(cookieConsent);
        } 
        response.setContentType("text/html");
        response.setHeader("Cache-Control", "no-cache");   
        response.getWriter().write("SUCEESS");
      }
    }
    return null;
  }
  
  public boolean checkCookieBanner(String cookieAcceptedDate, String cookieLatestUpdatedDate, String cookieName,HttpServletRequest request) throws Exception{
	  boolean showCookieFlag = false;
	  cookieAcceptedDate = cookieManager.getCookieValue(request, cookieName);
      cookieLatestUpdatedDate = commonFunction.getCookieLatestUpdatedDate();// example -- "04-10-2019 13:33:51";
      if(!GenericValidator.isBlankOrNull(cookieAcceptedDate) && CommonUtil.isValidDate(cookieAcceptedDate, GlobalConstants.COOKIE_DATE_FORMAT)&& !GenericValidator.isBlankOrNull(cookieLatestUpdatedDate) && CommonUtil.isValidDate(cookieLatestUpdatedDate, GlobalConstants.COOKIE_DATE_FORMAT)){        
        //comparing cookie popup accepted date and latest cookie,privacy and terms&condition policy        
        Date cookieAcceptedDt = simpleDateFormat.parse(cookieAcceptedDate);
        Date cookieLatestUpdatedDt = simpleDateFormat.parse(cookieLatestUpdatedDate);        
        //Using compareTo method comparing cookieAcceptedDt and cookieLatestUpdatedDt
        if (cookieAcceptedDt.compareTo(cookieLatestUpdatedDt) <= 0) { //It returns a value less than 0 if this cookieAcceptedDt is before the cookieLatestUpdatedDt.
          showCookieFlag = true;    //if cookieAcceptedDate is before cookieLatestUpdatedDate          
        }        
        //
      }else{
        showCookieFlag = true; //if cookie value is not setted (ie, cookie popup is not accepted)
      }
	  return showCookieFlag;
  }
}
