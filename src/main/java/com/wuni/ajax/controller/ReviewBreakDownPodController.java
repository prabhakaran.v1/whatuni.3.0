package com.wuni.ajax.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.review.bean.ReviewListBean;

import com.layer.business.ICommonBusiness;
import com.wuni.util.valueobject.AutoCompleteVO;
import com.wuni.util.valueobject.review.ReviewBreakDownVO;


/**
  * @GetReviewBreakDownAjaxSP
  * @author Hema.S
  * @version 1.0
  * @since 18.12.2018
  * @purpose  This program is used to get reviews details from selected subject in drop down.
  * Change Log
  * *************************************************************************************************************************
  * Date           Name                      Ver.       Changes desc                                      Rel Ver.
  * *************************************************************************************************************************
  * 18-Dec-2018   Hema.S                     1.0        Initial draft to spring                           584
  */
@Controller
public class ReviewBreakDownPodController {
	
	@Autowired
	  private ICommonBusiness commonBusiness;
	
	@RequestMapping(value = "/get-review-break-down-list", method=RequestMethod.POST)	
	  public ModelAndView getReviewBreakDownPod(HttpServletRequest request, HttpServletResponse response)  throws Exception {
		    String collegeId = request.getParameter("collegeId");
		    String subjectId = request.getParameter("subjectId");
		    String subjectName = request.getParameter("subjectName");
		    AutoCompleteVO autoCompleteVO = new AutoCompleteVO();
		    autoCompleteVO.setCollegeId(collegeId);
		    autoCompleteVO.setSubjectName(subjectId);
		    Map reviewBreakDownMap = new HashMap();
		    reviewBreakDownMap = commonBusiness.getReviewBreakDownList(autoCompleteVO);
		    ArrayList getReviewList = (ArrayList)reviewBreakDownMap.get("PC_REVIEW_LIST");   
		    if (getReviewList != null && getReviewList.size() > 0) {
		      request.setAttribute("getReviewList", getReviewList);
		        ReviewListBean reviewListBean = (ReviewListBean)getReviewList.get(0);
		        request.setAttribute("collegeId", reviewListBean.getCollegeId());
		        request.setAttribute("collegeName", reviewListBean.getCollegeName());
		      
		    }
		    ArrayList reviewBreakDownList = (ArrayList)reviewBreakDownMap.get("PC_REVIEW_BREAKDOWN");
		    if (reviewBreakDownList != null && reviewBreakDownList.size() > 0) {
		      request.setAttribute("reviewBreakDownList", reviewBreakDownList);
		      ReviewBreakDownVO reviewBreakDownVO = (ReviewBreakDownVO)reviewBreakDownList.get(0);
		      request.setAttribute("rating",reviewBreakDownVO.getRating());
		      request.setAttribute("reviewCount",reviewBreakDownVO.getReviewCount());
		      request.setAttribute("reviewExact",reviewBreakDownVO.getReviewExact());
		    }
		    ArrayList getReviewBreakdownSubjectList = (ArrayList)reviewBreakDownMap.get("PC_REVIEW_SUB_BREAKDOWN");
		    if(getReviewBreakdownSubjectList != null && getReviewBreakdownSubjectList.size() > 0) {
		      request.setAttribute("getReviewBreakdownSubjectList", getReviewBreakdownSubjectList);
		      ReviewBreakDownVO reviewSubjectBreakDownVO = (ReviewBreakDownVO)getReviewBreakdownSubjectList.get(0);
		      request.setAttribute("subjectDropDownRating",reviewSubjectBreakDownVO.getRating());
		      request.setAttribute("subjectDropDownReviewCount",reviewSubjectBreakDownVO.getReviewCount());
		        request.setAttribute("subjectReviewExact",reviewSubjectBreakDownVO.getReviewExact());
		    }
		    if(subjectName != null){
		      request.setAttribute("subjectName",subjectName);
		    }
		    return new ModelAndView("courseDetailPageSubjectReviewbreakdown");  
		  }
}
