package com.wuni.ajax.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;

/**
*
* @CookieManager.java
* @Version : 2.0
* @www.whatuni.com
* @Purpose  : This program is used to create and get cookie.
* *************************************************************************************************************************
* Date           Name                      Ver.     Changes desc                                                 Rel Ver.
* *************************************************************************************************************************
*  ?
* 
* 10_NOV_2020    Sangeeth.S	  		     1.3     Added getting cookie value condition block
*/
@Controller
public class CreateUserTypeCookie {
	@RequestMapping(value="/create-cookies", method=RequestMethod.POST)	
	  public ModelAndView createUserType(HttpServletRequest request, HttpServletResponse response) throws Exception {
	    String cookieProcessType = request.getParameter("processType");
		String cookieName = request.getParameter("cookieName");		
	    String cookieValue = request.getParameter("cookieValue");
	    if("get".equalsIgnoreCase(cookieProcessType)){
	     String userType = new CookieManager().getCookieValue(request, cookieName);
	      response.setContentType("text/html");
	      response.setHeader("Cache-Control", "no-cache");	      
	      if(userType != null){
	      response.getWriter().write("YES");   
	      }
	      else{
	    	  response.getWriter().write("NO");   
	      }
	    }else if("getCookieValue".equalsIgnoreCase(cookieProcessType)){
		     String cookieVal = new CookieManager().getCookieValue(request, cookieName);
		      response.setContentType("text/html");
		      response.setHeader("Cache-Control", "no-cache");	      
		      if(cookieVal != null){
		      response.getWriter().write(cookieVal);   
		      }
		      else{
		    	  response.getWriter().write("");   
		      }
		}else{
	      if(!GenericValidator.isBlankOrNull(cookieName)){
	    	if(GlobalConstants.COOKIE_CONSENT_COOKIE.equalsIgnoreCase(cookieName)){
	    		CommonUtil.updateCookieConsentCookie(request,response,cookieName,cookieValue);
	    		Date date = new Date(); 
	    		SimpleDateFormat simpleDateFormat= new SimpleDateFormat(GlobalConstants.COOKIE_DATE_FORMAT);//dd-MM-yyyy HH:mm:ss - 24 hourse format 
	    		String cookieAcceptedDate = simpleDateFormat.format(date);                
	            String cookieSplashValue = cookieAcceptedDate;
	            Cookie cookieSplash = CookieManager.createCookie(request, "cookie_splash_flag", cookieSplashValue);
	            response.addCookie(cookieSplash);
	    	}
	        Cookie cookie = CookieManager.createCookie(request,cookieName, cookieValue);
	        response.addCookie(cookie);
	        response.setContentType("text/html");
	        response.setHeader("Cache-Control", "no-cache");   
	        response.getWriter().write("SUCEESS");
	      }
	    }
	    return null;
	  }
}
