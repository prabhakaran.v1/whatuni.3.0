package com.wuni.ajax;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 *Added for adroll remarketing for 13-Jan-2015
 */
public class AdRollMarketingSP extends StoredProcedure
{

  public AdRollMarketingSP(DataSource datasource)
  {
    setDataSource(datasource);
    setSql("hot_admin.get_pixel_tracking_fn");
    setFunction(true);
    declareParameter(new SqlOutParameter("RetVal", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_order_item_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_page_type", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_affiliate_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_int_button", OracleTypes.VARCHAR));

  }
  public Map execute(List inputList)
  {
    Map outMap = null;
    Map inMap = new HashMap();
    try{ 
    inMap.put("p_order_item_id", inputList.get(0));
    inMap.put("p_page_type", inputList.get(1));
    inMap.put("p_affiliate_id", inputList.get(2));
    inMap.put("p_college_id", inputList.get(3));
    inMap.put("p_int_button", inputList.get(4));
    outMap = execute(inMap);
    }catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
