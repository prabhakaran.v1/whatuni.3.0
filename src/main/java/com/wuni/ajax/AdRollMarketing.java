package com.wuni.ajax;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *Added for adroll remarketing for 13-Jan-2015
 */

@Controller
public class AdRollMarketing{

  @RequestMapping(value="/adrollmarketingajax", method=RequestMethod.POST)	
  public ModelAndView adRollMarketingAjax(HttpServletRequest request, HttpServletResponse response) {
    String collegeid = request.getParameter("collegeid");
    String buttonType = request.getParameter("buttonType");
    String pageType = request.getParameter("pageType");
    String segmentName = getAdRollMarketingScript(collegeid, buttonType, pageType);
    StringBuffer outPutBuffer = new StringBuffer();
    response.setContentType("text/html");
    response.setHeader("Cache-Control", "no-cache");
    PrintWriter outPrint = null;
    try {
      outPrint = response.getWriter();
      outPutBuffer.append(segmentName);
    } catch (IOException e) {
      // TODO
    }
    outPrint.println(outPutBuffer);
    return null;
  }
  
  public String getAdRollMarketingScript(String collegeid, String buttonType, String pageType){  
   ArrayList inputList = new ArrayList();
    inputList.add("0");
    inputList.add("PROFILE_PAGE");
    inputList.add("220703");
    inputList.add(collegeid);
    inputList.add(buttonType);
    String segmentName = "";
    try {
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      Map resultMap = commonBusiness.addRollMarketingPrc(inputList);
      segmentName = (String)resultMap.get("RetVal");
      //
    } catch (Exception e) {
      e.printStackTrace();
    } 
    return segmentName;
  }
  
}
