package com.wuni.ucascalculator.bean;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class UcasCalcBean {
  
  private String grade = "";
  private String tariffPoints = "";
  private String subjectDesc = "";
  private String subjectId = "";
  private String collegeId = "";
  private String collegeNameDisplay = "";
  private String collegeName = "";
  private String collegeLogo = "";
  private String collegeNameUrl = "";
  private String providerImage = "";
  private String qualId = "";
  private String qualification = ""; 
  private String parentQualification = ""; 
  private String gradeStr = "";
  private String yearOfEntry = "";
  private String noOfSubjects = "";  
  
  private ArrayList bestMatchCoursesList = null;
  
  public UcasCalcBean() {}
  
  public void setGrade(String grade) {
    this.grade = grade;
  }
  public String getGrade() {
    return grade;
  }
  public void setTariffPoints(String tariffPoints) {
    this.tariffPoints = tariffPoints;
  }
  public String getTariffPoints() {
    return tariffPoints;
  }
  public void setSubjectDesc(String subjectDesc) {
    this.subjectDesc = subjectDesc;
  }
  public String getSubjectDesc() {
    return subjectDesc;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }
  public String getCollegeName() {
    return collegeName;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
    return collegeLogo;
  }
  public void setBestMatchCoursesList(ArrayList bestMatchCoursesList) {
    this.bestMatchCoursesList = bestMatchCoursesList;
  }
  public ArrayList getBestMatchCoursesList() {
    return bestMatchCoursesList;
  }
  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }
  public String getSubjectId() {
    return subjectId;
  }
  public void setCollegeNameUrl(String collegeNameUrl) {
    this.collegeNameUrl = collegeNameUrl;
  }
  public String getCollegeNameUrl() {
    return collegeNameUrl;
  }
  public void setProviderImage(String providerImage) {
    this.providerImage = providerImage;
  }
  public String getProviderImage() {
    return providerImage;
  }
  public void setQualId(String qualId) {
    this.qualId = qualId;
  }
  public String getQualId() {
    return qualId;
  }
  public void setQualification(String qualification) {
    this.qualification = qualification;
  }
  public String getQualification() {
    return qualification;
  }
  public void setParentQualification(String parentQualification) {
    this.parentQualification = parentQualification;
  }
  public String getParentQualification() {
    return parentQualification;
  }
  public void setGradeStr(String gradeStr) {
    this.gradeStr = gradeStr;
  }
  public String getGradeStr() {
    return gradeStr;
  }
  public void setYearOfEntry(String yearOfEntry) {
    this.yearOfEntry = yearOfEntry;
  }
  public String getYearOfEntry() {
    return yearOfEntry;
  }
  public void setNoOfSubjects(String noOfSubjects) {
    this.noOfSubjects = noOfSubjects;
  }
  public String getNoOfSubjects() {
    return noOfSubjects;
  }
}
