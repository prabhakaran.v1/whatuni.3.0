package com.wuni.ucascalculator.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;

@Controller
public class UCASCalculatorController {
	@RequestMapping(value = { "/ucas-calculator" , "/ucas/*" } , method = {RequestMethod.POST , RequestMethod.GET })
	  public ModelAndView getStudentAwardsDetails(@ModelAttribute("studentAwardsBean")   ModelMap model, HttpServletRequest request,  HttpServletResponse response , HttpSession session) throws Exception {
	    if (new CommonFunction().checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED //
	    	return new ModelAndView("forward: /userLogin.html");
	    }
	    if (request.getAttribute("sessionclosed") != null && String.valueOf(request.getAttribute("sessionclosed")).trim().length() > 0) {
	      session.setAttribute("message", "sessionexpiry");
	      return new ModelAndView("forward: /userLogin.html");
	    }
	    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);   
	    List scoreInputList = null;
	    List tariffInputList = null;
	    List subInputList = null;
	    String ajaxFlag = request.getParameter("ajaxFlag");  
	    if(GenericValidator.isBlankOrNull(ajaxFlag)){     
	      tariffInputList = new ArrayList();
	      String yoe = GlobalConstants.UCAS_LOAD_YEAR_VALUE;//Added by Indumathi.S JUN_28_16 REL
	      tariffInputList.add(yoe);
	      Map yoeMap = commonBusiness.getYearOfEntryDetails(tariffInputList);
	      List qualList = (ArrayList)yoeMap.get("PC_QUALIFICATION_LIST");
	      if (qualList != null && qualList.size() > 0) {
	        request.setAttribute("qualificationList", qualList);        
	      }
	      String yoeFlag = (String)yoeMap.get("P_YEAR_EXIST_FLAG"); 
	      request.setAttribute("yoeFlag", yoeFlag);
	    }else if("score".equals(ajaxFlag)){
	      scoreInputList = new ArrayList();
	      String yoe = request.getParameter("yoe");
	      String grdParam1 = !GenericValidator.isBlankOrNull(request.getParameter("grdParam1")) ? request.getParameter("grdParam1") : "";
	      String grdParam2 = !GenericValidator.isBlankOrNull(request.getParameter("grdParam2")) ? request.getParameter("grdParam2") : "";
	      String grdParam3 = !GenericValidator.isBlankOrNull(request.getParameter("grdParam3")) ? request.getParameter("grdParam3") : "";
	      scoreInputList.add(yoe);
	      scoreInputList.add(grdParam1);
	      scoreInputList.add(grdParam2);
	      scoreInputList.add(grdParam3);
	      Map recentCourseMap = commonBusiness.getUserScoreDetails(scoreInputList);      
	      String ucasPoints = (String)recentCourseMap.get("p_ucas_points");    
	      if (ucasPoints != null && ucasPoints.length() > 0) {
	        request.setAttribute("ucasPoints", ucasPoints);
	      }      
	      String ucasMaxScore = (String)recentCourseMap.get("p_max_ucas_points");      
	      if((ucasMaxScore != null && ucasMaxScore.length() > 0) && (ucasPoints != null && ucasPoints.length() > 0)) {
	        int ucasScorePercnt1 = 0;
	        int ucasScorePercnt2 = 0;
	        if(Integer.parseInt(ucasPoints)<720){
	           ucasScorePercnt1 = (int)(Math.round((Float.parseFloat(ucasPoints)/Float.parseFloat(ucasMaxScore))*100));           
	           request.setAttribute("ucasScorePercnt1", String.valueOf(ucasScorePercnt1));
	           ucasScorePercnt2 = (100 - ucasScorePercnt1);
	           request.setAttribute("ucasScorePercnt2", String.valueOf(ucasScorePercnt2));
	         }else{
	           request.setAttribute("ucasScorePercnt1", "99");
	           request.setAttribute("ucasScorePercnt2", "1");
	         }
	      }
	      String ucasScore = (String)recentCourseMap.get("p_ucas_percentage");    
	      if (ucasScore != null && ucasScore.length() > 0) {
	        request.setAttribute("ucasScore", ucasScore);
	      }
	    return new ModelAndView("scoredataajax");
	    }else if("tariff".equals(ajaxFlag)){
	      tariffInputList = new ArrayList();
	      String yoe = request.getParameter("yoe");
	      String tfqual = request.getParameter("tfqual");  
	      String tfqualVal = request.getParameter("tfqualVal");
	      tariffInputList.add(yoe);
	      tariffInputList.add(tfqual);
	      Map recentCourseMap = commonBusiness.getTariffDetails(tariffInputList);      
	      List traiffPointsList = new ArrayList();
	      traiffPointsList = (ArrayList)recentCourseMap.get("pc_get_tariff_table");      
	      if(traiffPointsList != null && traiffPointsList.size() > 0){
	        request.setAttribute("traiffPointsList", traiffPointsList);
	        request.setAttribute("tfqualVal", tfqualVal);//Added by Indumathi.S FEB-16-16
	      }
	      request.setAttribute("tfqualifications", tfqual);
	      return new ModelAndView("tariffdataajax");
	    }else if("submit".equals(ajaxFlag) || "resultsDp".equals(ajaxFlag)){
	      String pageForward = "youcouldstudysubmitajax";
	      String submitFlag = "N";
	      String subjectFlag = "N";
	      subInputList = new ArrayList();
	      String ucasPoints = "";
	      String ucasScorePrcnt = "";
	      String totSubParam = request.getParameter("totSubParam");      
	      if(!GenericValidator.isBlankOrNull(totSubParam)){
	        String[] params = totSubParam.split("-");
	        ucasPoints = params[0];
	        ucasScorePrcnt = params[1];
	        String subjectList="";
	        for(int i=2; i<params.length;i++){
	          subjectList += '-'+params[i];
	          subjectFlag = "Y";
	        }
	        if("Y".equals(subjectFlag)){
	          subjectList = subjectList.substring(1);  
	        }        
	        subInputList.add(ucasPoints);
	        subInputList.add(subjectList);
	        if("submit".equals(ajaxFlag)){          
	          pageForward = "submituserscoreajax";
	          submitFlag = "Y";
	        }
	        subInputList.add(submitFlag);
	      }      
	      Map ucasSubListMap = commonBusiness.submitUcasSubjectDetails(subInputList);
	      List searchResultsList = (ArrayList)ucasSubListMap.get("PC_SEARCH_RESULTS");      
	      if(searchResultsList != null && searchResultsList.size() > 0){
	        request.setAttribute("searchResultsList", searchResultsList);
	      }
	      List subjectList = (ArrayList)ucasSubListMap.get("PC_GET_SUBJECT_LIST");      
	      if(subjectList != null && subjectList.size() > 0){
	        request.setAttribute("subjectList", subjectList);
	      }      
	      request.setAttribute("ucasPoints", ucasPoints);
	      if(StringUtils.isNotBlank(ucasPoints)) {
	  	    session.setAttribute("USER_UCAS_SCORE", ucasPoints); //Added for from footer UCAS tariff calculator by Sri Sankari for 20200917 release
	  	  }
	      request.setAttribute("ucasScorePrcnt", ucasScorePrcnt);
	      return new ModelAndView(pageForward);
	    }
	    //Insight start
	    request.setAttribute("insightPageFlag", "yes");
	    //Insight end
	    session.setAttribute("noSplashpopup","true");    
	    request.setAttribute("getInsightName", "ucascalculator.jsp");
	    return new ModelAndView("ucascalculator");
	  }
	  

}

	
