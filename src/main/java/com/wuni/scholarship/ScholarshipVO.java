package com.wuni.scholarship;

public class ScholarshipVO {

  private String scholarshipId = null;
  private String scholarshipTitle = null;
  private String collegeId = null;
  private String awardValue = null;
  private String awardCriteria = null;
  private String scholarshipDesc = null;
  private String studyLevel = null;
  private String collegeNameDisplay = null;
  private String collegeLogo = null;
  private String awardInfo = null;
  private String totalCount = null;
  private String scholarshipInfoURL = null;
  private String awardSubjects = null;
  private String uniHomeURL = null;
  private String collegeName = null;
  private String gaCollegeName = null;
  private String emailPrice = null;
  private String websitePrice = null;
  private String webformPrice = null;
  private String shortListFlag = null;
  private String providerSchoalrshipURL = null;
  private String orderItemId = null;
  private String subOrderItemId = null;
  private String myhcProfileId = null;
  private String profileId = null;
  private String profileType = null;
  private String advertName = null;
  private String cpeQualificationNetworkId = null;
  private String subOrderEmail = null;
  private String subOrderProspectus = null;
  private String subOrderWebsite = null;
  private String subOrderEmailWebform = null;
  private String subOrderProspectusWebform = null;
  //StudyLevel
  private String studyLevelId = null;
  private String studyLevelDesc = null;
  //Subject Dropdown
  private String subjectId = null;
  private String subjectDesc = null;
  //Nationality Dropdown
  private String nationalityId = null;
  private String nationalityDesc = null;
  //Award Coverage Dropdown
  private String awardCoverageId = null;
  private String awardCoverageDesc = null;
  //Award Type Dropdown
  private String awardTypeId = null;
  private String awardTypeDesc = null;
  //Award Value
  private String bursaryValue = null;
  private String awardDesc = null;
  private String feesCoverage = null;
  private String addedToFinalChoice = null;

  public void setScholarshipId(String scholarshipId) {
    this.scholarshipId = scholarshipId;
  }

  public String getScholarshipId() {
    return scholarshipId;
  }

  public void setScholarshipTitle(String scholarshipTitle) {
    this.scholarshipTitle = scholarshipTitle;
  }

  public String getScholarshipTitle() {
    return scholarshipTitle;
  }

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setAwardValue(String awardValue) {
    this.awardValue = awardValue;
  }

  public String getAwardValue() {
    return awardValue;
  }

  public void setAwardCriteria(String awardCriteria) {
    this.awardCriteria = awardCriteria;
  }

  public String getAwardCriteria() {
    return awardCriteria;
  }

  public void setScholarshipDesc(String scholarshipDesc) {
    this.scholarshipDesc = scholarshipDesc;
  }

  public String getScholarshipDesc() {
    return scholarshipDesc;
  }

  public void setStudyLevel(String studyLevel) {
    this.studyLevel = studyLevel;
  }

  public String getStudyLevel() {
    return studyLevel;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }

  public String getCollegeLogo() {
    return collegeLogo;
  }

  public void setAwardInfo(String awardInfo) {
    this.awardInfo = awardInfo;
  }

  public String getAwardInfo() {
    return awardInfo;
  }

  public void setTotalCount(String totalCount) {
    this.totalCount = totalCount;
  }

  public String getTotalCount() {
    return totalCount;
  }

  public void setStudyLevelId(String studyLevelId) {
    this.studyLevelId = studyLevelId;
  }

  public String getStudyLevelId() {
    return studyLevelId;
  }

  public void setStudyLevelDesc(String studyLevelDesc) {
    this.studyLevelDesc = studyLevelDesc;
  }

  public String getStudyLevelDesc() {
    return studyLevelDesc;
  }

  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public String getSubjectId() {
    return subjectId;
  }

  public void setSubjectDesc(String subjectDesc) {
    this.subjectDesc = subjectDesc;
  }

  public String getSubjectDesc() {
    return subjectDesc;
  }

  public void setNationalityId(String nationalityId) {
    this.nationalityId = nationalityId;
  }

  public String getNationalityId() {
    return nationalityId;
  }

  public void setNationalityDesc(String nationalityDesc) {
    this.nationalityDesc = nationalityDesc;
  }

  public String getNationalityDesc() {
    return nationalityDesc;
  }

  public void setAwardCoverageId(String awardCoverageId) {
    this.awardCoverageId = awardCoverageId;
  }

  public String getAwardCoverageId() {
    return awardCoverageId;
  }

  public void setAwardCoverageDesc(String awardCoverageDesc) {
    this.awardCoverageDesc = awardCoverageDesc;
  }

  public String getAwardCoverageDesc() {
    return awardCoverageDesc;
  }

  public void setAwardTypeId(String awardTypeId) {
    this.awardTypeId = awardTypeId;
  }

  public String getAwardTypeId() {
    return awardTypeId;
  }

  public void setAwardTypeDesc(String awardTypeDesc) {
    this.awardTypeDesc = awardTypeDesc;
  }

  public String getAwardTypeDesc() {
    return awardTypeDesc;
  }

  public void setScholarshipInfoURL(String scholarshipInfoURL) {
    this.scholarshipInfoURL = scholarshipInfoURL;
  }

  public String getScholarshipInfoURL() {
    return scholarshipInfoURL;
  }

  public void setAwardSubjects(String awardSubjects) {
    this.awardSubjects = awardSubjects;
  }

  public String getAwardSubjects() {
    return awardSubjects;
  }

  public void setUniHomeURL(String uniHomeURL) {
    this.uniHomeURL = uniHomeURL;
  }

  public String getUniHomeURL() {
    return uniHomeURL;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }

  public String getProfileId() {
    return profileId;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setAdvertName(String advertName) {
    this.advertName = advertName;
  }

  public String getAdvertName() {
    return advertName;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setGaCollegeName(String gaCollegeName) {
    this.gaCollegeName = gaCollegeName;
  }

  public String getGaCollegeName() {
    return gaCollegeName;
  }

  public void setEmailPrice(String emailPrice) {
    this.emailPrice = emailPrice;
  }

  public String getEmailPrice() {
    return emailPrice;
  }

  public void setWebsitePrice(String websitePrice) {
    this.websitePrice = websitePrice;
  }

  public String getWebsitePrice() {
    return websitePrice;
  }

  public void setWebformPrice(String webformPrice) {
    this.webformPrice = webformPrice;
  }

  public String getWebformPrice() {
    return webformPrice;
  }

  public void setShortListFlag(String shortListFlag) {
    this.shortListFlag = shortListFlag;
  }

  public String getShortListFlag() {
    return shortListFlag;
  }

  public void setProviderSchoalrshipURL(String providerSchoalrshipURL) {
    this.providerSchoalrshipURL = providerSchoalrshipURL;
  }

  public String getProviderSchoalrshipURL() {
    return providerSchoalrshipURL;
  }

  public void setBursaryValue(String bursaryValue) {
    this.bursaryValue = bursaryValue;
  }

  public String getBursaryValue() {
    return bursaryValue;
  }

  public void setFeesCoverage(String feesCoverage) {
    this.feesCoverage = feesCoverage;
  }

  public String getFeesCoverage() {
    return feesCoverage;
  }

  public void setAwardDesc(String awardDesc) {
    this.awardDesc = awardDesc;
  }

  public String getAwardDesc() {
    return awardDesc;
  }
  public void setAddedToFinalChoice(String addedToFinalChoice) {
    this.addedToFinalChoice = addedToFinalChoice;
  }
  public String getAddedToFinalChoice() {
    return addedToFinalChoice;
  }
}
