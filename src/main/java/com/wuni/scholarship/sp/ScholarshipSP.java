package com.wuni.scholarship.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

import com.wuni.scholarship.ScholarshipVO;
import com.wuni.scholarship.form.ScholarshipFormBean;
import com.wuni.util.seo.SeoUrls;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : ScholarshipSP.java
 * Description   : This class is for displaying all scholarships.
 * @since
 * @author       :sekar.k
 *
 * Modification history:
 * **************************************************************************************************************
 * Author     Relase Date              Modification Details
 * **************************************************************************************************************
 * Yogeswari  19.05.2015               Changed for Scholarship RANGE VALUE RQ
 */
public class ScholarshipSP extends StoredProcedure {

  public ScholarshipSP(DataSource datasource) {
    setDataSource(datasource);
    setSql("wu_sch_search_pkg.get_sch_filter_srch_result_prc");
    declareParameter(new SqlParameter("p_college_id", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_level_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_subject_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_country_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_award_cov_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_award_typ_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_page_no", OracleTypes.NUMBER));
    declareParameter(new SqlParameter("p_flag", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_user_id", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_basket_id", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("oc_study_level", OracleTypes.CURSOR, new StudyLevelRowmapper()));
    declareParameter(new SqlOutParameter("oc_subject", OracleTypes.CURSOR, new SubjectDropDownRowmapper()));
    declareParameter(new SqlOutParameter("oc_nationality", OracleTypes.CURSOR, new NatinalityDropDownRowmapper()));
    declareParameter(new SqlOutParameter("oc_award_cov", OracleTypes.CURSOR, new AwardCoveraveDropDown()));
    declareParameter(new SqlOutParameter("oc_award_type", OracleTypes.CURSOR, new AwardTypeDropDown()));
    declareParameter(new SqlOutParameter("oc_uni_sch_page", OracleTypes.CURSOR, new ScholarshipListRowmapper()));
    declareParameter(new SqlOutParameter("oc_country_id", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("o_pixel_tracking_code", OracleTypes.VARCHAR)); //9_DEC_2014 Added by Priyaa for pixel tracking    
  }

  public Map execut(ScholarshipFormBean bean) {
    Map outMap = null;
    Map inMap = new HashMap();
    inMap.put("p_college_id", bean.getCollegeId());
    inMap.put("p_level_id", bean.getStudyLevelId());
    inMap.put("p_subject_id", bean.getSubjectId());
    inMap.put("p_country_id", bean.getNationalityId());
    inMap.put("p_award_cov_id", bean.getAwardCoverageId());
    inMap.put("p_award_typ_id", bean.getAwardTypeId());
    inMap.put("p_page_no", bean.getPageNo());
    inMap.put("p_flag", bean.getPageName());
    inMap.put("p_user_id", bean.getUserId());
    inMap.put("p_basket_id", bean.getBasketId());
    outMap = execute(inMap);
    return outMap;
  }

  private class ScholarshipListRowmapper implements RowMapper {

    CommonFunction common = new CommonFunction();

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ScholarshipVO scholarshipvo = new ScholarshipVO();
      scholarshipvo.setScholarshipId(rs.getString("scholarship_id"));
      scholarshipvo.setScholarshipTitle(rs.getString("scholarship_title"));
      scholarshipvo.setCollegeId(rs.getString("college_id"));
      scholarshipvo.setCollegeName(rs.getString("college_name"));
      /* ::START:: Yogeswari :: 19.05.2015 :: Changed for Scholarship RANGE VALUE RQ */
      scholarshipvo.setBursaryValue(rs.getString("bursary_value"));
      scholarshipvo.setFeesCoverage(rs.getString("fees_coverage"));
      scholarshipvo.setAwardDesc(rs.getString("award_desc"));
      /* ::END:: Yogeswari :: 19.05.2015 :: Changed for  RANGE VALUE RQ */
      scholarshipvo.setAwardCriteria(rs.getString("award_criteria"));
      scholarshipvo.setStudyLevel(rs.getString("study_level"));
      scholarshipvo.setCollegeNameDisplay(rs.getString("college_name_display"));
      scholarshipvo.setCollegeLogo(rs.getString("college_logo"));
      scholarshipvo.setAwardInfo(rs.getString("award_info"));
      scholarshipvo.setAwardSubjects(rs.getString("award_subjects"));
      scholarshipvo.setTotalCount(rs.getString("total_no_of_sch"));
      String providerScholarshipURL = "/degrees" + GlobalConstants.SEO_PATH_SCHOLARSHIP_PAGE + new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(rs.getString("college_name")))) + "-scholarships-bursaries" + "/m,n,a,t,l" + "/" + rs.getString("college_id") + "/1" + "/bursary-uni.html";
      scholarshipvo.setProviderSchoalrshipURL(providerScholarshipURL.toLowerCase());
      String scholarshipInfoURL = "/degrees/scholarship-uk/" + (rs.getString("scholarship_title") != null ? common.replaceHypen(common.replaceURL(new CommonFunction().replaceSpecialCharacter(rs.getString("scholarship_title")))) : "");
      scholarshipInfoURL = scholarshipInfoURL + "/" + rs.getString("scholarship_id") + "/bursary-info.html";
      scholarshipvo.setScholarshipInfoURL(scholarshipInfoURL.toLowerCase());
      scholarshipvo.setUniHomeURL(new SeoUrls().construnctUniHomeURL(scholarshipvo.getCollegeId(), scholarshipvo.getCollegeName()));
      scholarshipvo.setGaCollegeName(new CommonFunction().replaceSpecialCharacter(scholarshipvo.getCollegeName()));
      scholarshipvo.setWebsitePrice(rs.getString("website_price"));
      scholarshipvo.setWebformPrice(rs.getString("webform_price"));
      scholarshipvo.setShortListFlag(rs.getString("college_in_basket"));
      scholarshipvo.setAddedToFinalChoice(rs.getString("final_choices_exist_flag"));
      ResultSet enquiryDetailsRS = (ResultSet)rs.getObject("enquiry_details");
      try {
        if (enquiryDetailsRS != null) {
          while (enquiryDetailsRS.next()) {
            String tmpUrl = null;
            scholarshipvo.setOrderItemId(enquiryDetailsRS.getString("order_item_id"));
            scholarshipvo.setSubOrderItemId(enquiryDetailsRS.getString("suborder_item_id"));
            scholarshipvo.setMyhcProfileId(enquiryDetailsRS.getString("myhc_profile_id"));
            scholarshipvo.setProfileType(enquiryDetailsRS.getString("profile_type"));
            scholarshipvo.setCpeQualificationNetworkId(enquiryDetailsRS.getString("network_id"));
            scholarshipvo.setAdvertName(enquiryDetailsRS.getString("advert_name"));
            scholarshipvo.setSubOrderEmail(enquiryDetailsRS.getString("email"));
            scholarshipvo.setSubOrderProspectus(enquiryDetailsRS.getString("prospectus"));
            tmpUrl = enquiryDetailsRS.getString("website");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            scholarshipvo.setSubOrderWebsite(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("email_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            scholarshipvo.setSubOrderEmailWebform(tmpUrl);
            tmpUrl = enquiryDetailsRS.getString("prospectus_webform");
            if (tmpUrl != null && tmpUrl.trim().length() > 0) {
              tmpUrl = new CommonFunction().appendingHttptoURL(tmpUrl);
            } else {
              tmpUrl = "";
            }
            scholarshipvo.setSubOrderProspectusWebform(tmpUrl);
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
      } finally {
        try {
          if (enquiryDetailsRS != null) {
            enquiryDetailsRS.close();
          }
        } catch (Exception e) {
          throw new SQLException(e.getMessage());
        }
      }
      return scholarshipvo;
    }

  }

  private class StudyLevelRowmapper implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ScholarshipVO studylevelvo = new ScholarshipVO();
      studylevelvo.setStudyLevelId(rs.getString("level_id"));
      studylevelvo.setStudyLevelDesc(rs.getString("level_desc"));
      return studylevelvo;
    }

  }

  private class SubjectDropDownRowmapper implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ScholarshipVO subjectvo = new ScholarshipVO();
      subjectvo.setSubjectId(rs.getString("subject_id"));
      subjectvo.setSubjectDesc(rs.getString("subject_area"));
      return subjectvo;
    }

  }

  private class NatinalityDropDownRowmapper implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ScholarshipVO nationalityvo = new ScholarshipVO();
      nationalityvo.setNationalityId(rs.getString("myhc_option_id"));
      nationalityvo.setNationalityDesc(rs.getString("nationality_display_text"));
      return nationalityvo;
    }

  }

  private class AwardCoveraveDropDown implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ScholarshipVO awardcoveragevo = new ScholarshipVO();
      awardcoveragevo.setAwardCoverageId(rs.getString("option_id"));
      awardcoveragevo.setAwardCoverageDesc(rs.getString("option_desc"));
      return awardcoveragevo;
    }

  }

  private class AwardTypeDropDown implements RowMapper {

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      ScholarshipVO awardtypevo = new ScholarshipVO();
      awardtypevo.setAwardTypeId(rs.getString("option_id"));
      awardtypevo.setAwardTypeDesc(rs.getString("option_desc"));
      return awardtypevo;
    }

  }

}
