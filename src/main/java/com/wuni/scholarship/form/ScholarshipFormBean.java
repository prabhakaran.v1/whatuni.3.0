package com.wuni.scholarship.form;

import org.apache.struts.action.ActionForm;

public class ScholarshipFormBean{

  private String collegeId = null;
  private String studyLevelId = null;
  private String subjectId = null;
  private String nationalityId = null;
  private String awardCoverageId = null;
  private String awardTypeId = null;
  private String pageNo = null;
  private String userId = null;
  private String basketId = null;
  
  private String pageName = null;
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setStudyLevelId(String studyLevelId) {
    this.studyLevelId = studyLevelId;
  }
  public String getStudyLevelId() {
    return studyLevelId;
  }
  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }
  public String getSubjectId() {
    return subjectId;
  }
  public void setNationalityId(String nationalityId) {
    this.nationalityId = nationalityId;
  }
  public String getNationalityId() {
    return nationalityId;
  }
  public void setAwardCoverageId(String awardCoverageId) {
    this.awardCoverageId = awardCoverageId;
  }
  public String getAwardCoverageId() {
    return awardCoverageId;
  }
  public void setAwardTypeId(String awardTypeId) {
    this.awardTypeId = awardTypeId;
  }
  public String getAwardTypeId() {
    return awardTypeId;
  }
  public void setPageNo(String pageNo) {
    this.pageNo = pageNo;
  }
  public String getPageNo() {
    return pageNo;
  }
  public void setPageName(String pageName) {
    this.pageName = pageName;
  }
  public String getPageName() {
    return pageName;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setBasketId(String basketId) {
    this.basketId = basketId;
  }
  public String getBasketId() {
    return basketId;
  }
}
