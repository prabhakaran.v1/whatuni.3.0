package com.wuni.opendays.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.openday.OpendaysVO;

import WUI.admin.utilities.TimeTrackingConstants;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;

/**
 * @OpenDaysLandingController - Used for getting open days home page data
 * @author ?
 * @version 1.0
 * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
 * 
 * Modification history:
 * **************************************************************************************************************
 * Author            Relase Date              Modification Details                           version
 * **************************************************************************************************************
 * Prabhakaran V.    06.05.2020               Added in param -random number                      1.1
 *                                            Forming eventCategory code                         1.1
 */
@Controller
public class OpenDaysLandingController {
	
  @SuppressWarnings("unchecked")
  @RequestMapping(value = "/open-days", method = RequestMethod.GET)
  public ModelAndView getOpendaysLandingPage(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    Long startActionTime = new Long(System.currentTimeMillis());
	CommonFunction common = new CommonFunction();
	CommonUtil utilities = new CommonUtil();
    session = request.getSession();
    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { //  TO CHECK FOR SESSION EXPIRED //
	  return new ModelAndView("forward: /userLogin.html");
    }
	try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
	  if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
	    String fromUrl = "/home.html";
		session.setAttribute("fromUrl", fromUrl);
		return new ModelAndView("loginPage");
	  }
    } catch (Exception exception) {
	    exception.printStackTrace();
	  }
    new GlobalFunction().removeSessionData(session, request, response);
	String urlString = request.getRequestURI().substring(request.getContextPath().length());
	session.removeAttribute("prospectus_redirect_url");
    session.setAttribute("prospectus_redirect_url", urlString);
	String searchCharacter = null;
	List locInputList = null;
    List dateInputList = null;
    List weekDayInputList = null;
	int found = -1;
	if (urlString != null) {
	  found = urlString.indexOf(GlobalConstants.SEO_PATH_OPEN_DAY_PAGE);
	}
    String datePage = request.getParameter("date") != null? request.getParameter("date"): "";
	String month = request.getParameter("month") != null? request.getParameter("month"): "";
	String startDate = request.getParameter("startDate") != null? request.getParameter("startDate"): null;
	String endDate = request.getParameter("endDate") != null? request.getParameter("endDate"): null;
	if (found == -1 && !"".equals(datePage) && "y".equals(datePage)) {
	  searchCharacter = datePage;
	}
	request.setAttribute("urlmapping", urlString);
	request.setAttribute("searchCharacter", searchCharacter);
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
	if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = "2";
	}
	request.getSession().setAttribute("cpeQualificationNetworkId", cpeQualificationNetworkId);
	String userId = new SessionData().getData(request, "y");
	ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	Map openDaysLandingDateMap = null;
	String forwardString = "";
	String page = "";
	String curMonth = "0";
	String odStatusPos = "0";
	if (datePage != null && "y".equals(datePage)) {
	  forwardString = "opendaylandingdate";
	  page = "D";
	  int odStatusPosition = 1;
	  dateInputList = new ArrayList();
	  dateInputList.add(month);
	  dateInputList.add(startDate);
	  dateInputList.add(endDate);
	  dateInputList.add(userId);
	  openDaysLandingDateMap = commonBusiness.getOpenDaysLandingDate(dateInputList);
	  ArrayList openDaysHeroImageList = (ArrayList)openDaysLandingDateMap.get("oc_heroimage");
	  if (openDaysHeroImageList != null && openDaysHeroImageList.size() > 0) {
	    request.setAttribute("openDaysHeroImageList", openDaysHeroImageList);
	  }
	  ArrayList openDaysMonthsList = (ArrayList)openDaysLandingDateMap.get("oc_od_months");
	  if (openDaysMonthsList != null && openDaysMonthsList.size() > 0) {
	    request.setAttribute("openDaysMonthsList", openDaysMonthsList);
		Iterator iterate = openDaysMonthsList.iterator();
		while (iterate.hasNext()) {
		  OpendaysVO opendaysVO = (OpendaysVO)iterate.next();
		  if ("Y".equals(opendaysVO.getOdStatus())) {
		    odStatusPos = String.valueOf(odStatusPosition);
		    break;
		  }
		  odStatusPosition++;
		}
	  }
	  ArrayList openDaysWeeksList = (ArrayList)openDaysLandingDateMap.get("oc_od_week");
	  if (openDaysWeeksList != null && openDaysWeeksList.size() > 0) {
	    request.setAttribute("openDaysWeeksList", openDaysWeeksList);
		OpendaysVO opendaysVO = (OpendaysVO)openDaysWeeksList.get(0);
		curMonth = opendaysVO.getCurrentMonth();
	  }
	  ArrayList openDayWeekInfoList = (ArrayList)openDaysLandingDateMap.get("oc_od_week_info");
	  if (openDayWeekInfoList != null && openDayWeekInfoList.size() > 0) {
	    request.setAttribute("openDayWeekInfoList", openDayWeekInfoList);
	  }
	  String opdLocBreadCrumb = (String)openDaysLandingDateMap.get("o_bread_crumb");
	  if (!GenericValidator.isBlankOrNull(opdLocBreadCrumb)) {
	    request.setAttribute("breadCrumb", opdLocBreadCrumb);
	  }
	  String remainingPos = String.valueOf(openDaysMonthsList.size() - odStatusPosition);
	  request.setAttribute("currentMonth", curMonth);
	  request.setAttribute("totalPos", String.valueOf(openDaysMonthsList.size()));
	  request.setAttribute("odStatusPos", odStatusPos);
	  request.setAttribute("remainingPos", remainingPos);
	  } else if (month != null && !"".equals(month)) {
		  forwardString = "opdWeekAndDateLandingAjax";
		  page = "D";
		  weekDayInputList = new ArrayList();
		  weekDayInputList.add(month);
		  weekDayInputList.add(startDate);
		  weekDayInputList.add(endDate);
		  openDaysLandingDateMap = commonBusiness.getOpenDaysWeekDateInfo(weekDayInputList);
		  ArrayList openDaysWeeksAjaxList = (ArrayList)openDaysLandingDateMap.get("oc_od_week");
		  if (openDaysWeeksAjaxList != null && openDaysWeeksAjaxList.size() > 0) {
		    request.setAttribute("openDaysWeeksList", openDaysWeeksAjaxList);
		    OpendaysVO opendaysVO = (OpendaysVO)openDaysWeeksAjaxList.get(0);
		    curMonth = opendaysVO.getCurrentMonth();
		  }
		  ArrayList openDayWeekInfoAjaxList = (ArrayList)openDaysLandingDateMap.get("oc_od_week_info");
		  if (openDayWeekInfoAjaxList != null && openDayWeekInfoAjaxList.size() > 0) {
		    request.setAttribute("openDayWeekInfoList", openDayWeekInfoAjaxList);
		  }
		} else {
		    forwardString = "opendaylanding";
		    page = "L";
		    locInputList = new ArrayList();
		    OpendaysVO opendaysVO = new OpendaysVO();
		    if(GenericValidator.isBlankOrNull(userId)) {
		      userId = "0";
		    }
		    //Added to get basket id by Sangeeth.S
		    String cookie_basket_id = common.checkCookieStatus(request);
		    opendaysVO.setUserId(userId);
		    opendaysVO.setBasketId(cookie_basket_id);
		    opendaysVO.setRandomNumber(utilities.getSessionRandomNumber(request, session)); //1.1
		    Map openDaysLandingMap = commonBusiness.getOpenDaysLandingLocation(opendaysVO);
		    ArrayList openDaysHeroImageList = (ArrayList)openDaysLandingMap.get("oc_heroimage");
		    if (openDaysHeroImageList != null && openDaysHeroImageList.size() > 0) {
		      request.setAttribute("openDaysHeroImageList", openDaysHeroImageList);
		    }
		    ArrayList openDaysUpCommingList = (ArrayList)openDaysLandingMap.get("oc_future_opendays");
		    if (openDaysUpCommingList != null && openDaysUpCommingList.size() > 0) {
		      Iterator uniUpcomingOpenDays = openDaysUpCommingList.iterator();
		      while (uniUpcomingOpenDays.hasNext()) {
		        OpendaysVO openDaysVOIter = (OpendaysVO)uniUpcomingOpenDays.next();
		        request.setAttribute("bookingUrl",openDaysVOIter.getBookingUrl());     
		        request.setAttribute("orderItemId",openDaysVOIter.getOrderItemId());
		        request.setAttribute("collegeName",openDaysVOIter.getCollegeName());
		        request.setAttribute("websitePrice",openDaysVOIter.getWebsitePrice());
		        request.setAttribute("openDate",openDaysVOIter.getOpenDate());
		        request.setAttribute("openMonthYear",openDaysVOIter.getOpenMonthYear());
		        request.setAttribute("collegeId",openDaysVOIter.getCollegeId());
		        request.setAttribute("networkId",openDaysVOIter.getNetworkId());
		        break;
		      }
		      
		      //Added event category name for GA - 1.1
 			    ArrayList<OpendaysVO> opendayList = openDaysUpCommingList;	      
		      Collection<OpendaysVO> nonDuplicatedOpenday = opendayList.stream()
		    		   .<Map<String, OpendaysVO>> collect(HashMap::new,(m,e)->m.put(e.getEventCategoryName(), e), Map::putAll).values();
		      String eventCategoryNames = nonDuplicatedOpenday.stream().distinct().map(ins -> ins.getEventCategoryName()).collect(Collectors.joining("|"));
		      model.addAttribute("openDayEventType", eventCategoryNames);
          //End of the code
		      
		      request.setAttribute("openDaysUpCommingList", openDaysUpCommingList);
		    }
		    ArrayList myOpenDaysFutureList = (ArrayList)openDaysLandingMap.get("PC_USER_FUTURE_OPENDAYS");
		    if (myOpenDaysFutureList != null && myOpenDaysFutureList.size() > 0) {
		      request.setAttribute("myOpenDaysFutureList", myOpenDaysFutureList);
		    }
		    ArrayList myOpenDaysPastList = (ArrayList)openDaysLandingMap.get("PC_USER_PAST_OPENDAYS");
		    if (myOpenDaysPastList != null && myOpenDaysPastList.size() > 0) {
		      request.setAttribute("myOpenDaysPastList", myOpenDaysPastList);
		    }
		    String opdLocBreadCrumb = (String)openDaysLandingMap.get("o_bread_crumb");
		    if (!GenericValidator.isBlankOrNull(opdLocBreadCrumb)) {
		      request.setAttribute("breadCrumb", opdLocBreadCrumb);
		    }    
		  }
		  request.setAttribute("page", page);
		  request.setAttribute("datePage", datePage);
		  /*  DONT ALTER THIS ---> mainly used to track time taken details  */
		  if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
		    new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
		  } /*  DONT ALTER THIS ---> mainly used to track time taken details  */
		  request.setAttribute("curActiveMenu", "opendays");
		  //Insight start
		  request.setAttribute("insightPageFlag", "yes");
		  request.setAttribute("showResponsiveTimeLine","true");
		  //Insight end.
		  return new ModelAndView(forwardString);
		}
}
