package com.wuni.opendays.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import spring.valueobject.search.RefineByOptionsVO;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.SessionData;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.openday.OpendaySearchVO;
import com.wuni.util.valueobject.openday.OpendaysVO;

/**
 * @OpenDaySearchController - Used for getting open days search results page data
 * @author ?
 * @version 1.0
 * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
 * 
 * Modification history:
 * **************************************************************************************************************
 * Author            Relase Date              Modification Details                           version
 * **************************************************************************************************************
 * Prabhakaran V.    06.05.2020               Added in param -random number                      1.1
 *
 */

@Controller
public class OpenDaySearchController {
	
  @RequestMapping(value = "/open-days/search", method = {RequestMethod.GET,RequestMethod.POST})
  public ModelAndView getOpendaysSearchPage(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    OpendaySearchVO openDaysVO = new OpendaySearchVO();
    CommonUtil utilities = new CommonUtil();
	//
    String URL_STRING = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
	URL_STRING = URL_STRING + "/";
	String queryString = request.getQueryString();
	String fullOpenDayUrlwithparam = URL_STRING + (GenericValidator.isBlankOrNull(queryString)? "": ("?" + queryString));
		    //
	String y = new SessionData().getData(request, "y");
	y = y != null && !y.equals("0") && y.trim().length() >= 0 && !y.equals("")? y: "0";
	String pageURL = URL_STRING;
	String location = request.getParameter("location");
	String keyword = request.getParameter("keyword");
	keyword = (GenericValidator.isBlankOrNull(keyword) || keyword.equals("Please enter subject or uni"))? "": keyword;
	String collegeid = request.getParameter("collegeid");
	String pageNo = request.getParameter("pageno");
	pageNo = GenericValidator.isBlankOrNull(pageNo)? "1": pageNo;
	String monthYear = request.getParameter("date");
	String qualification = !GenericValidator.isBlankOrNull(request.getParameter("level")) ? request.getParameter("level").toUpperCase() : request.getParameter("level");
	String collegeName = "";
	boolean mblLoginReg = false;
	session = request.getSession();
	if (!GenericValidator.isBlankOrNull(collegeid)) {
	  collegeName = new CommonFunction().getCollegeNameDisplay(collegeid);
	  request.setAttribute("odcollegeName", collegeName);
	}
	if (request.getParameter("odEventId") != null && !"".equals(request.getParameter("odEventId"))) { //Added by Amir for 07_APR_15 release towards responsive open day save to calendar
	  request.setAttribute("odEventId", request.getParameter("odEventId"));
      mblLoginReg = true;
    }
	if (request.getParameter("resUniIds") != null && !"".equals(request.getParameter("resUniIds"))) { //Added by Amir for 07_APR_15 release towards responsive open day save to calendar
	  request.setAttribute("odResUniIds", request.getParameter("resUniIds"));
	  mblLoginReg = true;
	}
	if (mblLoginReg) {
	  String odColId = request.getParameter("odColId");
	  String odColName = "";
	  if (!GenericValidator.isBlankOrNull(odColId)) {
	    odColName = new CommonFunction().getCollegeNameDisplay(odColId);
		request.setAttribute("odColName", odColName);
	  }
	  if (request.getParameter("newReg") != null && !"".equals(request.getParameter("newReg"))) {
	    request.setAttribute("newOdReg", request.getParameter("newReg"));
	  }
	  if (fullOpenDayUrlwithparam.indexOf("&odEventId") != -1) {
	    fullOpenDayUrlwithparam = fullOpenDayUrlwithparam.replace("&odEventId=" + request.getParameter("odEventId"), "");
	  }
	  if (fullOpenDayUrlwithparam.indexOf("?odEventId") != -1) {
	    fullOpenDayUrlwithparam = fullOpenDayUrlwithparam.replace("?odEventId=" + request.getParameter("odEventId"), "");
	  }
	  if (fullOpenDayUrlwithparam.indexOf("&odColId") != -1) {
	    fullOpenDayUrlwithparam = fullOpenDayUrlwithparam.replace("&odColId=" + odColId, "");
	  }
	  if (fullOpenDayUrlwithparam.indexOf("&newReg") != -1) {
	    fullOpenDayUrlwithparam = fullOpenDayUrlwithparam.replace("&newReg=Y", "");
	  }
	  if (fullOpenDayUrlwithparam.indexOf("&resUniIds") != -1) {
	    fullOpenDayUrlwithparam = fullOpenDayUrlwithparam.replace("&resUniIds=" + request.getParameter("resUniIds"), "");
	  }
	}
    request.setAttribute("fullOpenDayUrlwithparam", fullOpenDayUrlwithparam);
    //
	ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	openDaysVO.setUserId(y);
	openDaysVO.setPageNo(pageNo);
	openDaysVO.setSearchLocation(location);
    String skwd = new CommonFunction().replacePlus(keyword);
	request.setAttribute("search_keyword", skwd);
	openDaysVO.setSearchKeyword(skwd);
	openDaysVO.setCollegeId(collegeid);
	openDaysVO.setMonthYear(monthYear);
	openDaysVO.setQualification(qualification);
	openDaysVO.setRandomNumber(utilities.getSessionRandomNumber(request, session)); //1.1
	//
	Map searchResultMap = commonBusiness.fetchOpenDaySearchResults(openDaysVO);
	//
	String currentMonth = (String)searchResultMap.get("p_highlight_monthyear");
	if (!GenericValidator.isBlankOrNull(currentMonth)) {
	  request.setAttribute("curMonth", currentMonth);
	}
    ArrayList searchResultsList = (ArrayList)searchResultMap.get("PC_SEARCH_RESULTS");
	if (searchResultsList != null && searchResultsList.size() > 0) {
	  request.setAttribute("openDaySearchResultsList", searchResultsList);
	  int searchCount1 = Integer.parseInt(searchResultMap.get("P_TOTAL_OPEN_DAYS_COUNT").toString());
	  String totalRecordCount = new Integer(searchCount1).toString();
	  request.setAttribute("totalRecordCount", totalRecordCount);
	  //
	  int totalOpenDaysCount = Integer.parseInt(searchResultMap.get("P_TOTAL_OPEN_DAYS_COUNT").toString());
	  String totalOpenDaysCnt = new Integer(totalOpenDaysCount).toString();
	  request.setAttribute("totalOpenDaysCount", totalOpenDaysCnt);
	  } else {
		  request.setAttribute("noResultsFlag", "true");
		  ArrayList noResultsList = (ArrayList)searchResultMap.get("PC_DEFAULT_OPENDAYS");
		  if (noResultsList != null && noResultsList.size() > 0) {
		    request.setAttribute("openDaySearchResultsList", noResultsList);
		  }
		  String opendayAdvertiser = (String)searchResultMap.get("p_advertiser_flag");
		  if (!GenericValidator.isBlankOrNull(opendayAdvertiser) && ("Y").equals(opendayAdvertiser)) {
		    request.setAttribute("opendayAdvertiser", opendayAdvertiser);
		  }
		  String futureOpenDayExists = (String)searchResultMap.get("p_future_openday_exists_flag");
		  if (!GenericValidator.isBlankOrNull(futureOpenDayExists) && ("Y").equals(futureOpenDayExists)) {
		    request.setAttribute("futureOpenDayExists", futureOpenDayExists);
		  }
		  String highlightMonth = (String)searchResultMap.get("p_highlight_month");
		  if (!GenericValidator.isBlankOrNull(highlightMonth)) {
		    request.setAttribute("highlightMonth", highlightMonth);
		  }
		}
		String breadCrumb = (String)searchResultMap.get("p_bread_crumbs");
		if (breadCrumb != null && breadCrumb.trim().length() > 0) {
		  request.setAttribute("breadCrumb", breadCrumb);
		}
		ArrayList studyLevelList = (ArrayList)searchResultMap.get("PC_QUALIFICATION_REFINE");
		if (studyLevelList != null && studyLevelList.size() > 0) {
		  Iterator studyLevelItr = studyLevelList.iterator();
		  ArrayList qualificationList = new ArrayList();
		  while (studyLevelItr.hasNext()) {
		    RefineByOptionsVO refineVO = (RefineByOptionsVO)studyLevelItr.next();
		    qualificationList.add(formRefineURL(refineVO, pageURL, "studyLevel", "level", request));
		    if("Y".equalsIgnoreCase(refineVO.getSelectedStudyMode()) && !GenericValidator.isBlankOrNull(refineVO.getRefineCode())){
		      request.setAttribute("selectedStudyLevel", refineVO.getRefineDesc());
		    }
		  }
		  request.setAttribute("qualificationList", qualificationList);
		}
		ArrayList locationList = (ArrayList)searchResultMap.get("PC_LOCATIONS");
		if (locationList != null && locationList.size() > 0) {
		  Iterator locationItr = locationList.iterator();
		  ArrayList locationRefineList = new ArrayList();
		  while (locationItr.hasNext()) {
		    RefineByOptionsVO refineVO = (RefineByOptionsVO)locationItr.next();
		    locationRefineList.add(formRefineURL(refineVO, pageURL, "location", "location", request));
		    if("Y".equalsIgnoreCase(refineVO.getLocationSelectedFlag())){
		      request.setAttribute("selectedLocation", refineVO.getRefineDesc());
		    }
		  }
		  request.setAttribute("locationList", locationRefineList);
		}
		//
		String openDayCurrentPos = "0";
		ArrayList uniOpenDaysMnthList = (ArrayList)searchResultMap.get("PC_OPEN_DAYS_MONTHS");
		if (uniOpenDaysMnthList != null && uniOpenDaysMnthList.size() > 0) {
		  request.setAttribute("uniOpenDaysMnthList", uniOpenDaysMnthList);
		  Iterator iterate = uniOpenDaysMnthList.iterator();
		  int openDayStatus = 1;
		  while (iterate.hasNext()) {
		    OpendaysVO opendaysVO = (OpendaysVO)iterate.next();
		    if ("Y".equals(opendaysVO.getOpenDayStatus())) {
		      openDayCurrentPos = String.valueOf(openDayStatus);
		      break;
		    }
		    openDayStatus++;
		  }
		}
		request.setAttribute("openDayCurrentPos", openDayCurrentPos);
		if(!GenericValidator.isBlankOrNull(location)){
		  request.setAttribute("location", !GenericValidator.isBlankOrNull(location) ? new CommonFunction().replaceHypenWithSpace(location).toUpperCase() : "");
		}
		if (!GenericValidator.isBlankOrNull(collegeid)) {
		  request.setAttribute("cDimCollegeId", collegeid);
		}
		//
		String requestUrl = URL_STRING + appendFilterValues("date", "", request);
		request.setAttribute("opendaySearchUrl", requestUrl);
		//
		String paginationURL = pageURL + appendFilterValues("page", "", request);
		request.setAttribute("openDayUrl", pageURL);
		request.setAttribute("openDayPaginationURL", paginationURL);
		request.setAttribute("curActiveMenu", "opendays");
		session.setAttribute("noSplashpopup", "true");
		request.setAttribute("showResponsiveTimeLine","true");
		return new ModelAndView("opendaysearchresults");
		    //
      }
		      //
      public void optimiseRefineList(ArrayList refineList, String pageURL, HttpServletRequest request) {
	    Iterator itr = refineList.iterator();
		ArrayList campustypeList = new ArrayList();
		ArrayList russelList = new ArrayList();
		ArrayList locationList = new ArrayList();
		Map locationMap = new HashMap();
		RefineByOptionsVO campusVO = new RefineByOptionsVO();
		RefineByOptionsVO russellVO = new RefineByOptionsVO();
		campusVO.setRefineDesc("All campus type");
		campusVO.setOpenDayURL(pageURL + appendFilterValues("campus", "", request));
		campustypeList.add(campusVO);
		russellVO.setRefineDesc("All universities");
		russellVO.setOpenDayURL(pageURL + appendFilterValues("russell", "", request));
		russelList.add(russellVO);
		while (itr.hasNext()) {
		  RefineByOptionsVO refineVO = (RefineByOptionsVO)itr.next();
		  //
		  if (("UNIV_LOC_TYPE").equals(refineVO.getRefineOrder())) {
		    RefineByOptionsVO resultVO = formRefineURL(refineVO, pageURL, "location", "lc-tpe", request);
		    locationList.add(resultVO);
		  } else if (("CAMPUS_BASED_UNIV").equals(refineVO.getRefineOrder())) {
		      RefineByOptionsVO resultVO = formRefineURL(refineVO, pageURL, "campus", "cmps", request);
		      campustypeList.add(resultVO);
		    } else if (("RUSSELL_GROUP").equals(refineVO.getRefineOrder())) {
		        RefineByOptionsVO resultVO = formRefineURL(refineVO, pageURL, "russell", "rsl", request);
		        russelList.add(resultVO);
		      }
		}
		locationMap.put("676", "Big City");
		locationMap.put("677", "Seaside");
		locationMap.put("675", "Town");
		locationMap.put("674", "Countryside");
		request.setAttribute("locationMap", locationMap);
		request.setAttribute("campustypeList", campustypeList);
		request.setAttribute("russelList", russelList);
		request.setAttribute("locationList", locationList);
      }
	  //
	  public RefineByOptionsVO formRefineURL(RefineByOptionsVO refineVO, String pageURL, String refineparametername, String parameter, HttpServletRequest request) {
	    String filterURL = "";
		String url = "";
		String filterparam = request.getQueryString();
		RefineByOptionsVO locationVO = new RefineByOptionsVO();
		locationVO.setRefineCode(refineVO.getRefineCode());
		locationVO.setRefineDesc(refineVO.getRefineDesc());
		if (!GenericValidator.isBlankOrNull(filterparam)) {
		  filterURL = appendFilterValues(refineparametername, locationVO.getRefineCode(), request);
		} else {
		    filterURL = "?" + parameter + "=" + locationVO.getRefineCode();
		  }
		  url = pageURL + filterURL;
		  locationVO.setOpenDayURL(url.toLowerCase());
		  return locationVO;
      }
      //
	  public String appendFilterValues(String filterType, String filterValue, HttpServletRequest request) {
	    String filter = "";
		String russellGroup = request.getParameter("rsl");
		russellGroup = !GenericValidator.isBlankOrNull(russellGroup)? russellGroup: "";
		String empRateMax = request.getParameter("e-rt-max");
		empRateMax = !GenericValidator.isBlankOrNull(empRateMax)? empRateMax: "";
		String empRateMin = request.getParameter("e-rt-min");
		empRateMin = !GenericValidator.isBlankOrNull(empRateMin)? empRateMin: "";
		String campusType = request.getParameter("cmps");
		campusType = !GenericValidator.isBlankOrNull(campusType)? campusType: "";
		String locType = request.getParameter("lc-tpe");
		locType = !GenericValidator.isBlankOrNull(locType)? locType: "";
		String studyLevel = request.getParameter("level");
		studyLevel = !GenericValidator.isBlankOrNull(studyLevel)? studyLevel: "";
		String collegeid = request.getParameter("collegeid");
		collegeid = !GenericValidator.isBlankOrNull(collegeid)? collegeid: "";
		String location = request.getParameter("location");
		location = !GenericValidator.isBlankOrNull(location)? location: "";
		String keyword = request.getParameter("keyword");
		keyword = (!GenericValidator.isBlankOrNull(keyword) && !("Please enter subject or uni").equals(keyword))? keyword: "";
		String pageNo = request.getParameter("pageno");
		pageNo = !GenericValidator.isBlankOrNull(pageNo)? pageNo: "";
		String date = (String)request.getAttribute("curMonth");
		date = !GenericValidator.isBlankOrNull(date)? date: "";
		String monthSearchFlag = (String)request.getParameter("flag");
		monthSearchFlag = !GenericValidator.isBlankOrNull(monthSearchFlag)? monthSearchFlag: "";
		//
		if ("studyLevel".equals(filterType)) {
		  if (!GenericValidator.isBlankOrNull(filterValue)) {
		    filter += ((filter.indexOf("?") > -1? "&": "?") + "level=" + filterValue);
		  }
		  } else {
		      if (!GenericValidator.isBlankOrNull(studyLevel)) {
		        filter += ((filter.indexOf("?") > -1? "&": "?") + "level=" + studyLevel);
		      }
		    }
		    if ("location".equals(filterType)) {
		      if (!GenericValidator.isBlankOrNull(filterValue)) {
		        filter += ((filter.indexOf("?") > -1? "&": "?") + "location=" + filterValue);
		      }
		    } else {
		      if (!GenericValidator.isBlankOrNull(location)) {
		        filter += ((filter.indexOf("?") > -1? "&": "?") + "location=" + location);
		      }
		    }
		    //
		    if ("campus".equals(filterType)) {
		      if (!GenericValidator.isBlankOrNull(filterValue)) {
		        filter += ((filter.indexOf("?") > -1? "&": "?") + "cmps=" + filterValue);
		      }
		    } else {
		      if (!GenericValidator.isBlankOrNull(campusType)) {
		        filter += ((filter.indexOf("?") > -1? "&": "?") + "cmps=" + campusType);
		      }
		    }
		    //
		    if ("russell".equals(filterType)) {
		      if (!GenericValidator.isBlankOrNull(filterValue)) {
		        filter += ((filter.indexOf("?") > -1? "&": "?") + "rsl=" + filterValue);
		      }
		    } else {
		      if (!GenericValidator.isBlankOrNull(russellGroup)) {
		        filter += ((filter.indexOf("?") > -1? "&": "?") + "rsl=" + russellGroup);
		      }
		    }
		    //
		    if (!GenericValidator.isBlankOrNull(empRateMax)) {
		      filter += ((filter.indexOf("?") > -1? "&": "?") + "e-rt-max=" + empRateMax);
		    }
		    if (!GenericValidator.isBlankOrNull(empRateMin)) {
		      filter += ((filter.indexOf("?") > -1? "&": "?") + "e-rt-min=" + empRateMin);
		    }
		    //
		    if (!GenericValidator.isBlankOrNull(collegeid)) {
		      filter += ((filter.indexOf("?") > -1? "&": "?") + "collegeid=" + collegeid);
		    }
		    /*
		    if (!GenericValidator.isBlankOrNull(location)) {
		      filter += ((filter.indexOf("?") > -1? "&": "?") + "location=" + location);
		    }
		    */    
		    if (!GenericValidator.isBlankOrNull(keyword)) {
		      filter += ((filter.indexOf("?") > -1? "&": "?") + "keyword=" + keyword);
		    }
		    //    
		    if ("date".equals(filterType)) {
		      if (!GenericValidator.isBlankOrNull(filterValue)) {
		        filter += ((filter.indexOf("?") > -1? "&": "?") + "date=" + filterValue);
		      }
		    } else {
		      if (!GenericValidator.isBlankOrNull(date)) {
		        filter += ((filter.indexOf("?") > -1? "&": "?") + "date=" + date);
		      }
		    }
		    //    
		    if (!GenericValidator.isBlankOrNull(monthSearchFlag)) {
		      filter += ((filter.indexOf("?") > -1? "&": "?") + "flag=" + monthSearchFlag);
		    }
		    //  
       return filter; 	 	 
	 }	
}
