package com.wuni.opendays.controller;

import java.io.IOException;
import java.io.Writer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import WUI.utilities.CommonFunction;

@Controller
public class OpenDaysReserveController {

	  @RequestMapping(value = "/open-days/reserve-place", method = {RequestMethod.GET,RequestMethod.POST})
	  public ModelAndView getOpendaysReservePlace(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		  
		    session = request.getSession();
		    StringBuffer opendaysExit = new StringBuffer();
		    String action = "";
		    String forwardString = "reserveOpenDay";
		    if (request.getParameter("reqAction") != null && !"".equals(request.getParameter("reqAction"))) {
		      action = request.getParameter("reqAction");
		    }
		    if ("".equals(action)) {
		      forwardString = "reserveform";
		    } else if ("storedata".equalsIgnoreCase(action)) {
		      if (!GenericValidator.isBlankOrNull(request.getParameter("resUniIds"))) {
		        new CommonFunction().storeReserveOpendayList(request, response, request.getParameter("resUniIds"));
		      }
		    } else if ("exitOpenDaysRes".equals(action)) {
		      session.setAttribute("exitOpRes", "true");
		      opendaysExit.append("true");
		      setResponseText(response, opendaysExit);
		      return null;
		    }
		    return new ModelAndView(forwardString);  
	  }
	  private void setResponseText(HttpServletResponse response, StringBuffer responseMessage) {
		    try {
		      response.setContentType("TEXT/PLAIN");
		      Writer writer = response.getWriter();
		      writer.write(responseMessage.toString());
		    } catch (IOException exception) {
		      exception.printStackTrace();
		    }
		  }
}
