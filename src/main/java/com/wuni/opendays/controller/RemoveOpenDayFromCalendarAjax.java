package com.wuni.opendays.controller;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.openday.OpendaysVO;

import WUI.utilities.CommonUtil;
import WUI.utilities.SessionData;

/**
 * RemoveOpenDayFromCalendarAjax - Used for remove open day functionality
 * @author ?
 * @version 1.0
 * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
 * 
 * Modification history:
 * **************************************************************************************************************
 * Author            Relase Date              Modification Details                           version
 * **************************************************************************************************************
 * Prabhakaran V.    06.05.2020               Added in param -random number                      1.1
 *
 */

@Controller
public class RemoveOpenDayFromCalendarAjax {

	 @RequestMapping(value = "/removeOpendDay", method = {RequestMethod.GET,RequestMethod.POST})
	  public ModelAndView removeOpenDay(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		 
		 session = request.getSession();
		    String eventId = request.getParameter("eventId");
		    String userId = new SessionData().getData(request, "y");
		    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
		    OpendaysVO opendaysvo = new OpendaysVO();
		    CommonUtil utilities = new CommonUtil();
		    
		    opendaysvo.setEventId(eventId);
		    opendaysvo.setUserId(userId);
		    opendaysvo.setRandomNumber(utilities.getSessionRandomNumber(request, session)); //1.1
		    Map resultMap = commonBusiness.removeMyOpendays(opendaysvo);
		    ArrayList opendayslist = (ArrayList)resultMap.get("PC_FUTURE_OPENDAYS");
		    String status = (String)resultMap.get("P_STATUS");
		    if (!GenericValidator.isBlankOrNull(status) && ("SUCCESS").equals(status)) {
		      if (opendayslist != null && opendayslist.size() > 0) {
		        request.setAttribute("myOpenDaysFutureList", opendayslist);
		      }
		    }
		      ArrayList myOpenDaysFutureList = (ArrayList)resultMap.get("PC_FUTURE_OPENDAYS");
		      if (myOpenDaysFutureList != null && myOpenDaysFutureList.size() > 0) {
		        request.setAttribute("myOpenDaysFutureList", myOpenDaysFutureList);
		      }
		      ArrayList myOpenDaysPastList = (ArrayList)resultMap.get("PC_PAST_OPENDAYS");
		      if (myOpenDaysPastList != null && myOpenDaysPastList.size() > 0) {
		        request.setAttribute("myOpenDaysPastList", myOpenDaysPastList);
		      }
		      ArrayList openDaysUpCommingList = (ArrayList)resultMap.get("oc_future_opendays");
		      if (openDaysUpCommingList != null && openDaysUpCommingList.size() > 0) {
		          request.setAttribute("openDaysUpCommingList", openDaysUpCommingList);
		      }
		    return new ModelAndView("removesuccessresponse");
	 }
	 
	 }
