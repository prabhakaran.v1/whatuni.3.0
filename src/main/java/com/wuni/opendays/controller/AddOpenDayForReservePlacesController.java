package com.wuni.opendays.controller;

import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.config.SpringContextInjector;
import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;

import spring.form.OpenDaysBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.SessionData;

/**
 * Class for Article detail SP
 *
 * @since        wu333_20141209
 * @author       Thiyagu G
 * @LastModifiedBranch - FEATURE_VIRTUAL_OPENDAY branch commit
 *
 * Modification history:
 * **************************************************************************************************************
 * Author     Relase Date              Modification Details                                              version
 * **************************************************************************************************************
 * Yogeswari  19.05.2015               Added  tracking_session_id
 * Prabhakaran V. 03.11.2015           Commented the most popular and trending article out cursor
 * Prabhakaran V. 21.02.2017           Added p_amp_flag param for AMP page
 * Hema.S         20.11.2018           Added spons_tracking_code for sponsor article tracking
 * Prabhakaran V. 06.05.2020           Added to restriction for not forming dateString value if null       1.1
 */

@Controller
public class AddOpenDayForReservePlacesController {
  String finalDayString = null;

  @RequestMapping(value = "/add-opendays-for-reserveplace", method = { RequestMethod.GET, RequestMethod.POST })
  public ModelAndView getOpendaysReservePlace(ModelMap model, HttpServletRequest request, HttpServletResponse response,
      HttpSession session) throws Exception {
    String collegeId = GenericValidator.isBlankOrNull(request.getParameter("collegeId")) ? null
        : request.getParameter("collegeId");
    String openDate = GenericValidator.isBlankOrNull(request.getParameter("opendate")) ? null
        : request.getParameter("opendate");
    String[] openYear = GenericValidator.isBlankOrNull(request.getParameter("openyear")) ? null
        : request.getParameter("openyear").split(" ");
    String userID = new SessionData().getData(request, "y");
    String dateString = null;
    if (openYear != null && openYear.length > 1) {
      dateString = openYear[0] + "-" + openDate + "-" + openYear[1];
    }
    String dayName = null;
    if (StringUtils.isNotBlank(dateString)) { // 1.1
      try {
        SimpleDateFormat format1 = new SimpleDateFormat("MMM-dd-yyyy");
        Date dt1 = format1.parse(dateString.trim());
        DateFormat format2 = new SimpleDateFormat("EEEE");
        dayName = format2.format(dt1);
      } catch (Exception e) {
        e.printStackTrace();
      }
      finalDayString = dayName + " " + openDate + new CommonFunction().getOrdinalFor(Integer.parseInt(openDate)) + " "
          + openYear[0] + " " + openYear[1];
    }
    StringBuffer responseMsg = new StringBuffer("");
    OpenDaysBean openDaysBean = new OpenDaysBean();
    openDaysBean.setCollegeId(collegeId);
    openDaysBean.setOpenDate(dateString);
    openDaysBean.setUserId(userID);
    openDaysBean.setUserTrackSessionId(new SessionData().getData(request, "userTrackId"));
    openDaysBean.setActionURL(request.getHeader("REFERER"));
    responseMsg.append(AddOpenDayForReservePlaces(openDaysBean));
    setResponseText(response, responseMsg);
    return null;
  }

  public String AddOpenDayForReservePlaces(OpenDaysBean openDaysBean) {
    ISearchBusiness searchBusiness = (ISearchBusiness) new SpringContextInjector()
        .getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
    searchBusiness.addOpenDayForReservePlacesAction(openDaysBean);
    String returnMsg = "";
    return returnMsg;
  }

  private void setResponseText(HttpServletResponse response, StringBuffer responseMessage) {
    try {
      response.setContentType("TEXT/PLAIN");
      Writer writer = response.getWriter();
      writer.write(responseMessage.toString());
    } catch (IOException exception) {
      exception.printStackTrace();
    }
  }
}
