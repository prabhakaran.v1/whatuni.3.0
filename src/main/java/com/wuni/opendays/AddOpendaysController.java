package com.wuni.opendays;

import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import spring.form.OpenDaysBean;

import com.config.SpringContextInjector;
import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;

import WUI.utilities.CommonFunction;
import WUI.utilities.SessionData;

@Controller
public class AddOpendaysController {
	
	 String finalDayString = null;
	 @RequestMapping(value = "/addmyopendays", method = {RequestMethod.GET,RequestMethod.POST})
	  public ModelAndView addMyOpenDays(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
		 
			String collegeId = GenericValidator.isBlankOrNull(request.getParameter("collegeId")) ? null : request.getParameter("collegeId");
		    String openDate = GenericValidator.isBlankOrNull(request.getParameter("opendate")) ? null : request.getParameter("opendate");
		    String eventId = GenericValidator.isBlankOrNull(request.getParameter("eventid")) ? null : request.getParameter("eventid");
		    String[] openYear = GenericValidator.isBlankOrNull(request.getParameter("openyear")) ? null : request.getParameter("openyear").split(" ");
				  String regParam = GenericValidator.isBlankOrNull(request.getParameter("param")) ? "" : request.getParameter("param"); //Added for openday registration by Prabha on 27_JAN_2016_REL
		    String userID = new SessionData().getData(request, "y");    
		    String dateString = null;
		    if(openYear != null && openYear.length > 1) {
		      dateString = openYear[0] + "-" + openDate + "-" + openYear[1];
		    }
		    String dayName = null;
		    try
		    {
		      SimpleDateFormat format1=new SimpleDateFormat("MMM-dd-yyyy");
		      Date dt1=format1.parse(dateString.trim());
		      DateFormat format2=new SimpleDateFormat("EEEE"); 
		      dayName = format2.format(dt1); 
		    }catch(Exception e)
		    {
		      e.printStackTrace();
		    }
		    finalDayString = dayName + " " + openDate + new CommonFunction().getOrdinalFor(Integer.parseInt(openDate)) + " " +openYear[0] + " " + openYear[1];
		    StringBuffer responseMsg = new StringBuffer("");
		    OpenDaysBean openDaysBean = new OpenDaysBean();
		    openDaysBean.setCollegeId(collegeId);
		    openDaysBean.setOpenDate(dateString);
		    openDaysBean.setUserId(userID);
		    openDaysBean.setEventId(eventId);
				  openDaysBean.setDateExistFlg(regParam); //Added by Prabha on 27_JAN_2016_REL
		    responseMsg.append(AddOpendays(openDaysBean));
		    setResponseText(response, responseMsg);
		    return null;
	 }
	  public String AddOpendays(OpenDaysBean openDaysBean) {    
		    ISearchBusiness searchBusiness = (ISearchBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
		    Map resultMap = searchBusiness.addMyOpendays(openDaysBean);
		    String returnMsg = null;
		    if(resultMap != null) {
		      returnMsg = (String) resultMap.get("l_flag");
		      if(!GenericValidator.isBlankOrNull(returnMsg)) {        
		        String collegeName = new CommonFunction().getCollegeNameDisplay(openDaysBean.getCollegeId()); //Changed the college display name fn by Prabha on 31_May_2016
		        returnMsg = returnMsg + "##SPLIT##" + collegeName + "##SPLIT##" + finalDayString;
		      } 
		    }
		    return returnMsg;
		  }
		  
		  private void setResponseText(HttpServletResponse response, StringBuffer responseMessage) {
		    try {
		      response.setContentType("TEXT/PLAIN");
		      Writer writer = response.getWriter();
		      writer.write(responseMessage.toString());
		    } catch (IOException exception) {
		      exception.printStackTrace();
		    }
		  }

}
