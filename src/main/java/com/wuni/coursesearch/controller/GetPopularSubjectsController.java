package com.wuni.coursesearch.controller;

import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.valueobject.coursesearch.CourseSearchVO;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.SessionData;
import spring.form.CourseJourneyBean;

@Controller
public class GetPopularSubjectsController {
	
	@RequestMapping(value = "/courses/getPopularSubjectsAjax"  , method = {RequestMethod.POST , RequestMethod.GET })
	  public ModelAndView mobileSearchNav(ModelMap model, HttpServletRequest request, HttpServletResponse response, CourseJourneyBean coursejourneybean , HttpSession session) throws Exception {
	    CommonFunction common = new CommonFunction();
	    
	    request.setAttribute("currentPageUrl", (common.getSchemeName(request) + "www.whatuni.com/degrees" +request.getRequestURI() + ".html")); //Added getSchemeName by Indumathi Mar-29-16
	    if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { //TO CHECK FOR SESSION EXPIRED //
	    	return new  ModelAndView("forward: /userLogin.html");
	    }
	    try {
	      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
	        String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
	        session.setAttribute("fromUrl", fromUrl);
	        return new  ModelAndView("loginPage");
	      }
	    } catch (Exception exception) {
	      exception.printStackTrace();
	    }    
	    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);        
	    String userId = new SessionData().getData(request, "y");
	    userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0? userId: "0";
	    String studyLevelId = !GenericValidator.isBlankOrNull(new SessionData().getData(request, "studyLevelId")) ? new SessionData().getData(request, "studyLevelId") : "M";
	    String clearingFlag = "";
	    if("ON".equalsIgnoreCase(common.getWUSysVarValue("CLEARING_ON_OFF"))){      
	      clearingFlag = "CLEARING";
	    }    
	    CourseSearchVO csVO = new CourseSearchVO();    
	    csVO.setTrackSessionId(new SessionData().getData(request, "userTrackId"));
	    csVO.setUserId(userId);
	    csVO.setStudyLevelId(studyLevelId);
	    csVO.setClearingFlag(clearingFlag);
	    Map recentCourseMap = commonBusiness.getPopularSubjects(csVO);      
	    ArrayList subjectGuidesList = (ArrayList)recentCourseMap.get("pc_get_subject_guides");
	    if (subjectGuidesList != null && subjectGuidesList.size() > 0) {
	      CourseSearchVO courseSearchVO = (CourseSearchVO)subjectGuidesList.get(0);
	      request.setAttribute("subGuidTotalCount", courseSearchVO.getTotalCount());
	      request.setAttribute("subjectGuidesList", subjectGuidesList);
	    }    
	    return new  ModelAndView("popularsubjectsajax");
	  }
	}  


