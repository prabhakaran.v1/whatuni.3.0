package com.wuni.coursesearch.controller;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import WUI.utilities.GlobalConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.util.LabelValueBean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import java.util.Map;
import spring.form.CourseJourneyBean;
import spring.valueobject.findacourse.ArticlePodVO;
import spring.valueobject.findacourse.PopularSubjectVO;
import WUI.admin.utilities.TimeTrackingConstants;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;

/**
 * @CourseSearchController
 * @version  1.0
 * @purpose  This controller is used to search courses in find a course page
 * Change Log
 * *************************************************************************************************************************
 * author              Ver               Modified On     Modification Details                    Change
 * *************************************************************************************************************************
 * Keerthana E        WUNI_923           08-12-2020      Added popular subjects and articles     Added popular subjects list and article list for find a course page update
*/

@Controller
public class CourseSearchController {
		  
  @RequestMapping(value = "/courses/*/qualification/*/list"  , method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView getCourseSearchDetails(ModelMap model, HttpServletRequest request, HttpServletResponse response, CourseJourneyBean coursejourneybean , HttpSession session) throws Exception {
    Long startActionTime = new Long(System.currentTimeMillis());  
    session = request.getSession();
	CommonFunction common = new CommonFunction();
	request.setAttribute("currentPageUrl", (common.getSchemeName(request) + "www.whatuni.com/degrees" + request.getRequestURI() + ".html")); //Added getSchemeName by Indumathi Mar-29-16
	if (common.checkSessionExpired(request, response, session, "ANONYMOUS")) { //TO CHECK FOR SESSION EXPIRED //
      return new  ModelAndView("forward: /userLogin.html");
	}
	try {
	  if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
	    String fromUrl = "/home.html"; //ANONYMOUS REGISTERED_USER
		session.setAttribute("fromUrl", fromUrl);
		return new  ModelAndView("loginPage");
	  }
    } catch (Exception exception) {
	  exception.printStackTrace();
	  }    
	//Added canonical-url by Sujitha V on 02-JUNE-2020 rel
	String canonicalUrl = common.getFullURL(request, false);
	model.addAttribute("coureseSearchCanonicalUrl", canonicalUrl.toLowerCase());
    String userId = new SessionData().getData(request, "y");
    userId = (userId != null && !userId.equals("0") && userId.trim().length() >= 0) ? userId : "0";
	String studyLevelId = "";
    String mappingForward = "";
	String clearingFlag = "";
	String urlString = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
	int found = -1;
    if (urlString != null) {
	  found = urlString.indexOf(GlobalConstants.SEO_PATH_COURSEJOUNEY_PAGE);
	}    
    if (found != -1) {
	  if (urlString != null && urlString.indexOf("/qualification/") != -1) {
	    String[] urlArray = urlString.split("/");
		if (urlArray.length > 3) {
		  studyLevelId = urlArray[4];
		  mappingForward = "coursesearch";
		  if("qualification".equals(studyLevelId)) {
		    String clearingOnOff = common.getWUSysVarValue("CLEARING_ON_OFF");
		    if("ON".equalsIgnoreCase(clearingOnOff)) {
		      if(!GenericValidator.isBlankOrNull(urlString) && (urlString).contains("clearing")) {
		        studyLevelId = "M";
		        clearingFlag = "CLEARING";
		        mappingForward = "clearingcoursesearch";
		      }
		    }
		  }
		}
	  request.setAttribute("studyLevelId", studyLevelId);
      new SessionData().addData(request, response, "studyLevelId", studyLevelId); //Added Thiyagu G to get popular subjects in mobile search popup for 21_Mar_2017.
	  request.setAttribute("clearingFlag", clearingFlag);
	  }
    }
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);        
	List inputList = null;
    //Removed "Recent and latest search" code on 13_Jun_2017, By Thiyagu G.
	inputList = new ArrayList();
	inputList.add(new SessionData().getData(request, "userTrackId"));
	inputList.add(userId);      
	inputList.add(studyLevelId);
	inputList.add(clearingFlag);
	Map recentCourseMap = commonBusiness.getRecentCourseResults(inputList);
	String csBreadCrumb = (String)recentCourseMap.get("P_BREADCRUMB");
	if (!GenericValidator.isBlankOrNull(csBreadCrumb)) {
	  request.setAttribute("csBreadCrumb", csBreadCrumb);        
	}
    ArrayList iwtbResultsList = (ArrayList)recentCourseMap.get("PC_IWTB_RESULTS");    
    if(!CollectionUtils.isEmpty(iwtbResultsList)) {
	    request.setAttribute("iwtbResultsList", iwtbResultsList);
		    request.setAttribute("iwtbEntryPod", "notEmpty");
	  }
    ArrayList wcidInputDetailsList = (ArrayList)recentCourseMap.get("PC_WCID_INPUT_DETAILS");
    if(!CollectionUtils.isEmpty(wcidInputDetailsList)) {
	    request.setAttribute("wcidInputDetailsList", wcidInputDetailsList);        
	  }
	ArrayList wcidResultsList = (ArrayList)recentCourseMap.get("PC_WCID_RESULTS");
	    if(!CollectionUtils.isEmpty(wcidResultsList)) {
	    request.setAttribute("wcidResultsList", wcidResultsList);
		    request.setAttribute("wcidEntryPod", "notEmpty");
	  }
	  
	  // Added for snippet section in find a course- WUNI_923 on dec 08 2020 rel by Keerthana E
	  String snippetSection = (String) recentCourseMap.get("P_SNIPPET_TEXT");
  if (StringUtils.isNotBlank(snippetSection)) {
    model.addAttribute("snippetSection", snippetSection);
  }
  
  //Added for popular subjects in find a course- WUNI_923 on dec 08 2020 rel by Keerthana E
	  ArrayList<PopularSubjectVO> popularSubjectList = (ArrayList)recentCourseMap.get("PC_POPULAR_SUBJECT");
	  if(!CollectionUtils.isEmpty(popularSubjectList)) {
	    model.addAttribute("popularSubjectList", popularSubjectList);
	  }
	  
  //Added for articles in find a course- WUNI_923 on dec 08 2020 rel by Keerthana E
	  ArrayList<ArticlePodVO> articlePodList = (ArrayList)recentCourseMap.get("PC_ARTICLE_LIST");
	  if(!CollectionUtils.isEmpty(articlePodList)) {
	    model.addAttribute("articlePodList" , articlePodList);
	  }
	  
	  
	  //I Want To Be
	coursejourneybean.setIndustryName("Enter industry, eg Engineering");
    coursejourneybean.setJobName("Enter job name, eg Doctor");
	coursejourneybean.setSchoolNameFreeText("Enter school name");
	coursejourneybean.setSchoolName("Enter school name (optional)");    
	coursejourneybean.setIntendedYear(GlobalConstants.YEAR_OF_ENTRY_1);
		      
	//What Can I Do
    LabelValueBean gradesValBean = null;
	String[] gradesArray = {"A*", "A", "B", "C", "D", "E"};
	ArrayList gradesList = new ArrayList();
	for(int i = 0; i < gradesArray.length; i++) {
	  gradesValBean = new LabelValueBean();
	  gradesValBean.setLabel(gradesArray[i]);
	  gradesValBean.setValue(gradesArray[i]);
	  gradesList.add(gradesValBean);
	}
	LabelValueBean qualValBean = null;
	String[] qualVal = {"AL", "H", "AH", "TP"};
	String[] qualLabel = {"A-levels", "SQA Highers", "SQA Advanced Highers", "Tariff Points"};
	ArrayList qualList = new ArrayList();
	for(int i = 0; i < qualVal.length; i++) {
	  qualValBean = new LabelValueBean();
	  qualValBean.setLabel(qualLabel[i]);
	  qualValBean.setValue(qualVal[i]);
	  qualList.add(qualValBean);
	}  
    coursejourneybean.setQualGradeList(gradesList);
    coursejourneybean.setQualList(qualList);       
	coursejourneybean.setQualSubjName1(GlobalConstants.SUBJECT);
	coursejourneybean.setQualSubjName2(GlobalConstants.SUBJECT);
	coursejourneybean.setQualSubjName3(GlobalConstants.SUBJECT);
	coursejourneybean.setQualSubjName4(GlobalConstants.SUBJECT);
	coursejourneybean.setQualSubjName5(GlobalConstants.SUBJECT);
	coursejourneybean.setQualSubjName6(GlobalConstants.SUBJECT);
	coursejourneybean.setSchoolNameFreeTextWCSID("Enter school name");
	coursejourneybean.setSchoolNameWCSID("Enter school name (optional)");
	coursejourneybean.setIntendedYearWCSID(GlobalConstants.YEAR_OF_ENTRY_1);
		      
	//Insights
    if(!GenericValidator.isBlankOrNull(studyLevelId)) {
      new GlobalFunction().getStudyLevelDesc(studyLevelId, request); 
	}
    request.setAttribute("insightPageFlag", "yes");
	//Highlit menu's
	if("M".equals(studyLevelId)) {
	  request.setAttribute("curActiveMenu","findcrs");
	}
	request.setAttribute("showResponsiveTimeLine","true");
	/*DONT ALTER THIS ---> mainly used to track time taken details  */
    if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
	  new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
	}  /* DONT ALTER THIS ---> mainly used to track time taken details  */
	return new  ModelAndView(mappingForward, "coursejourneybean" , coursejourneybean);    
	}  
    private void setResponseText(HttpServletResponse response, String responseMessage) {
	  try {
	    response.setContentType("TEXT/PLAIN");
		Writer writer = response.getWriter();
		writer.write(responseMessage.toString());
	  } catch (IOException exception) {
	    exception.printStackTrace();
	    }
    } 
}
