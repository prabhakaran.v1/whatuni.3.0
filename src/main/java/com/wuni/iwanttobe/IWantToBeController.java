package com.wuni.iwanttobe;

import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.security.SecurityEvaluator;
import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;
import com.wuni.ultimatesearch.SaveULInputbean;
import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;

import spring.form.CourseJourneyBean;

@Controller
public class IWantToBeController {

	 @RequestMapping(value = "/i-want-to-be-widget"  , method = {RequestMethod.GET , RequestMethod.POST})
	  public ModelAndView iWantToBeController(ModelMap model, HttpServletRequest request, HttpServletResponse response, IWanttobeBean iWantToBeBean , HttpSession session) throws Exception {
		 try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
		      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
		        String fromUrl = "/home.html";
		        session.setAttribute("fromUrl", fromUrl);
		        return new  ModelAndView("loginPage");
		      }
		    } catch (Exception securityException) {
		      securityException.printStackTrace();
		    }
		    String userID = new SessionData().getData(request, "y");
		    String pageName = request.getParameter("pageName");
		    String schoolType = request.getParameter("schoolType");
		    Map IWanttobeMap = null;
		    String jacsCode = !GenericValidator.isBlankOrNull(request.getParameter("jacsCode")) ? request.getParameter("jacsCode") : "";
		    String ultimateSearchId = !GenericValidator.isBlankOrNull(request.getParameter("ultimateSearchId")) ? request.getParameter("ultimateSearchId") : "";
		    if (!GenericValidator.isBlankOrNull(ultimateSearchId)) {
		      request.setAttribute("ultimateSearchId", ultimateSearchId);
		    }
		    if (!GenericValidator.isBlankOrNull(jacsCode)) {
		      request.setAttribute("jacsCode", jacsCode);
		    }
		    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS); 
		    if ("yearOfEntry".equalsIgnoreCase(pageName) && !GenericValidator.isBlankOrNull(schoolType)) {
		    
		      ArrayList yearOfEntryList = new ArrayList();
		      request.setAttribute("checkSchoolType", schoolType);
		      if ("S".equalsIgnoreCase(schoolType)) {
		        schoolType = "SECONDARY SCHOOL";
		      } else if ("C".equalsIgnoreCase(schoolType)) {
		        schoolType = "SIXTH FORM/COLLEGE";
		      }
		      iWantToBeBean.setSchoolType(schoolType);
		      IWanttobeMap = commonBusiness.getYearOfEntry(iWantToBeBean);
		      yearOfEntryList = (ArrayList)IWanttobeMap.get("pc_year_of_entry");
		      if (yearOfEntryList != null && yearOfEntryList.size() > 0) {
		        request.setAttribute("yearOfEntryList", yearOfEntryList);
		      }
		      request.setAttribute("schoolType", schoolType);
		      return new  ModelAndView("yearOfEntry");
		    }
		    if ("results".equalsIgnoreCase(pageName)) {
		      SaveULInputbean inputBean = new SaveULInputbean();
		      String jobOrIndustryId = request.getParameter("jobOrIndustryId");
		      String jobOrIndustryFlag = request.getParameter("jobOrIndustryFlag");
		      String jobOrIndustryName = request.getParameter("jobOrIndustryName");
		      String yearOfEntryVal = request.getParameter("yearOfEntryVal");
		      if ("J".equalsIgnoreCase(jobOrIndustryFlag)) { //J-Job
		        inputBean.setJobSocCode(jobOrIndustryId);
		      }
		      if ("I".equalsIgnoreCase(jobOrIndustryFlag)) { //I-Industry
		        inputBean.setIndustrySicCode(jobOrIndustryId);
		      }
		      inputBean.setUlSearchType("IWTB");
		      inputBean.setUserId(userID);
		      inputBean.setTrackSessionId(new SessionData().getData(request, "userTrackId"));
		      inputBean.setIntendedYear(yearOfEntryVal);
		      IWanttobeMap = commonBusiness.saveUltimateSearchDetails(inputBean);
		      String logId = (String)IWanttobeMap.get("p_ultimate_search_new_log_id");
		      if (!GenericValidator.isBlankOrNull(logId)) {
		        getIWanttobeResults(logId, request, commonBusiness);
		      }
		      request.setAttribute("jobOrIndustryName", jobOrIndustryName);
		      request.setAttribute("jobOrIndustryFlag", jobOrIndustryFlag);
		      return new  ModelAndView("iwanttoberesults");
		    }
		    //IWant to be subject stats page and subject result added by Prabha
		    if ("SUBJECT-STATS".equalsIgnoreCase(pageName)) {
		      String jacsSubject = request.getParameter("jacsSubject");
		      iWantToBeBean.setJacsCode(jacsCode);
		      iWantToBeBean.setLogId(ultimateSearchId);
		      Map subjectStatsMap = commonBusiness.getIWanttobeEmpInfo(iWantToBeBean);
		      if (subjectStatsMap != null) {
		        ArrayList subjectStatsList = (ArrayList)subjectStatsMap.get("pc_emp_info");
		        if (subjectStatsList != null && subjectStatsList.size() > 0) {
		          request.setAttribute("subjectStatsList", subjectStatsList);
		        }
		      }
		      request.setAttribute("jacsSubject", jacsSubject);
		      return new  ModelAndView("iwanttobesubjectstats");
		    } else if ("SUBJECT-RESULT".equalsIgnoreCase(pageName)) {
		      String jacsSubject = request.getParameter("jacsSubject");
		      String pageNo = request.getParameter("pageNo");
		      String collegeId = request.getParameter("collegeId");
		      String sortBy = request.getParameter("sortBy");
		      String sortByHow = null;
		      String salSortByHow = request.getParameter("salSortByHow");
		      String empSortByHow = request.getParameter("empSortByHow");
		      if ("SALARY".equalsIgnoreCase(sortBy)) {
		        sortBy = "SALARY";
		        sortByHow = salSortByHow;
		      } else {
		        sortBy = "GRADUATE_EMPLOYED_PERCENT";
		        sortByHow = empSortByHow;
		      }
		      iWantToBeBean.setJacsCode(jacsCode);
		      iWantToBeBean.setPageNo(pageNo);
		      iWantToBeBean.setCollegeId(collegeId);
		      iWantToBeBean.setSortByWhich(sortBy);
		      iWantToBeBean.setSortByHow(sortByHow);
		      iWantToBeBean.setLogId(ultimateSearchId);
		      Map subjectResultMap = commonBusiness.getIWanttobeEmpInfoAjax(iWantToBeBean);
		      if (subjectResultMap != null) {
		        ArrayList subjectResultList = (ArrayList)subjectResultMap.get("pc_univ_emp_info");
		        if (subjectResultList != null && subjectResultList.size() > 0) {
		          IWanttobeBean uniEmpVO = new IWanttobeBean();
		          uniEmpVO = (IWanttobeBean)subjectResultList.get(0);
		          request.setAttribute("totalCount", uniEmpVO.getTotalCount());
		          request.setAttribute("subjectResultsList", subjectResultList);
		        }
		        ArrayList subjectStatsList = (ArrayList)subjectResultMap.get("pc_emp_info");
		        if (subjectStatsList != null && subjectStatsList.size() > 0) {
		          request.setAttribute("subjectStatsList", subjectStatsList);
		        }
		        String salaryTooltip = (String)subjectResultMap.get("p_job_ind_salary_text");
		        if (!GenericValidator.isBlankOrNull(salaryTooltip)) {
		          request.setAttribute("salaryTooltip", salaryTooltip);
		        }
		        String empTooltip = (String)subjectResultMap.get("p_job_ind_emp_rate_text");
		        if (!GenericValidator.isBlankOrNull(empTooltip)) {
		          request.setAttribute("empTooltip", empTooltip);
		        }
		        if (!"YES".equalsIgnoreCase(request.getParameter("paginationFlag"))) {
		          if ("SALARY".equalsIgnoreCase(sortBy)) { //Change the salary sort by value
		            if ("A".equalsIgnoreCase(salSortByHow)) {
		              salSortByHow = "D";
		            } else {
		              salSortByHow = "A";
		            }
		          } else if ("GRADUATE_EMPLOYED_PERCENT".equalsIgnoreCase(sortBy)) { //Change the emp rate sort by value
		            if ("A".equalsIgnoreCase(empSortByHow)) {
		              empSortByHow = "D";
		            } else {
		              empSortByHow = "A";
		            }
		          }
		        }
		        request.setAttribute("jacsSubject", jacsSubject);
		        request.setAttribute("salSortByHow", salSortByHow);
		        request.setAttribute("empSortByHow", empSortByHow);
		        if (!GenericValidator.isBlankOrNull(ultimateSearchId)) {
		          request.setAttribute("ultimateSearchId", ultimateSearchId);
		        }
		        request.setAttribute("sortByHow", sortByHow);
		        request.setAttribute("sortByWhich", sortBy); //request for selecting salary or emp rate in pagination
		        request.setAttribute("pageNo", pageNo);
		        return new  ModelAndView("iwanttobesearchresult");
		      }
		    }
		    request.setAttribute("GAPageName", "I-want-to-be/common-degrees");
		    request.setAttribute("getInsightName", "I-want-to-be/common-degrees");
		    return new  ModelAndView("iwanttobehome");
		  }

		  /**
		   * getIWanttobeResults method to get the results for page 2
		   * @param logId
		   * @param request
		   * @param commonBusiness
		   */
		  private void getIWanttobeResults(String logId, HttpServletRequest request, ICommonBusiness commonBusiness) {
		    Map iwanttobeSearchRestulsMap = null;
		    IWanttobeBean getResultsInputBean = new IWanttobeBean();
		    getResultsInputBean.setLogId(logId);
		    iwanttobeSearchRestulsMap = commonBusiness.getIWanttobeSearchResults(getResultsInputBean);
		    ArrayList searchResultsList = (ArrayList)iwanttobeSearchRestulsMap.get("pc_results");
		    if (iwanttobeSearchRestulsMap.get("p_remaining_percent") != null) {
		      double remainingPercent = Double.parseDouble(iwanttobeSearchRestulsMap.get("p_remaining_percent").toString());
		      if (iwanttobeSearchRestulsMap.get("p_return_code") != null) {
		        int returnCode = Integer.parseInt(iwanttobeSearchRestulsMap.get("p_return_code").toString());
		        if (!GenericValidator.isBlankOrNull(String.valueOf(remainingPercent))) {
		          request.setAttribute("returnCode", String.valueOf(returnCode));
		          request.setAttribute("remainingPercent", String.valueOf(remainingPercent));
		      }
		    }
		    }
		    if (searchResultsList != null && searchResultsList.size() > 0) {
		      IWanttobeBean resultsVO = (IWanttobeBean)searchResultsList.get(0);
		      request.setAttribute("joborindustryname", resultsVO.getIwantSearchName());
		      request.setAttribute("joborindustryText",resultsVO.getJobOrIndustryText());
		      request.setAttribute("searchResultsList", searchResultsList);
		      request.setAttribute("ultimateSearchId", logId);
		    }
		  }

}
