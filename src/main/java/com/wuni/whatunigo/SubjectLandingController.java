package com.wuni.whatunigo;

import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : SubjectLandingAction.java
 * Description   : This class is used to display regions in subject landing page.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Sujitha V              1.0             01.08.2019     First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

@Controller
public class SubjectLandingController {
   @RequestMapping(value={"/wugo/subject/search"}, method = {RequestMethod.GET,RequestMethod.POST})
   public ModelAndView getSubjectLandingPage(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception { 
	ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	Map resultMap = commonBusiness.getSubjectLandingRegionList();
	if(resultMap != null && resultMap.size() > 0) {
	  ArrayList regionList = (ArrayList)resultMap.get("PC_REGION_LIST");
	  //System.out.println("REGION_LIST " + regionList);
	  if(regionList != null && regionList.size() > 0){
	    request.setAttribute("REGION_LIST", regionList);
	  }
	}
	return new ModelAndView("subjectLandingPage");
   }
}
