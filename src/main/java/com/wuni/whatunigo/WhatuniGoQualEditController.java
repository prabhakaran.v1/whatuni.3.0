package com.wuni.whatunigo;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wuni.util.seo.SeoUrls;
import com.wuni.valueobjects.chatbot.ExceptionJSON;
import com.wuni.valueobjects.whatunigo.ApplyNowVO;
import com.wuni.valueobjects.whatunigo.PageInfoVO;
import com.wuni.valueobjects.whatunigo.QualificationsVO;
import com.wuni.valueobjects.whatunigo.QuestionAnswerVO;
import com.wuni.valueobjects.whatunigo.SubjectAjaxListVO;
import com.wuni.valueobjects.whatunigo.UserqualificationsVO;
import com.wuni.valueobjects.whatunigo.course.CourseInfoVO;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.whatunigo.bean.OfferOutputBean;
import WUI.whatunigo.bean.WuGoBean;


@Controller
public class WhatuniGoQualEditController {
   @RequestMapping(value={"/clearing-match-maker-tool-edit"}, method = {RequestMethod.GET,RequestMethod.POST})
   public ModelAndView editQualDetails(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
	    CloseableHttpClient httpClient = HttpClientBuilder.create().build();
	    HttpResponse responsePost;
	    CommonFunction commonFunction = new CommonFunction();
	    CommonUtil commonUtil = new CommonUtil();
	    SeoUrls seoUrls = new SeoUrls();
	    ApplyNowVO courseInfoBean = new ApplyNowVO();
	    WuGoBean wuGoPayloadFromClient = new WuGoBean();
	    String responseAsString = null;
	    String referralUrl = "";
	    String forwardPage = "acceptingOfferPage";
	    String formName = request.getParameter("formName");
	    //System.out.println("formName-->" + formName);
	    request.setAttribute("HIDE_LOGIN", "Y");
	    request.setAttribute("ebookDim14", "WUGO");
	    //request.setAttribute("showBanner", "no");
	    String applyNowCourseId = !GenericValidator.isBlankOrNull(request.getParameter("courseId")) ? request.getParameter("courseId") : null;
	    String applyNowOpportunityId = !GenericValidator.isBlankOrNull(request.getParameter("opportunityId")) ? request.getParameter("opportunityId") : null;
	    String applicationCount = !GenericValidator.isBlankOrNull(request.getParameter("applicationCount")) ? request.getParameter("applicationCount") : "";
	    String oldQualDesc = request.getParameter("oldQualDesc");
	    String newQualDesc = request.getParameter("newQualDesc");
	    String oldGrade = request.getParameter("oldGrade");
	    String newGrade = request.getParameter("newGrade");
	    String freeText = request.getParameter("free-text");
	    String markAsFlag = request.getParameter("markAsFlag");
	    String qualConfirmFlag = request.getParameter("qualConfirmFlag");    
	    String courseId = !GenericValidator.isBlankOrNull((String)courseInfoBean.getCourseId()) ? courseInfoBean.getCourseId() : request.getParameter("courseId");
	    String opportunityId = !GenericValidator.isBlankOrNull((String)courseInfoBean.getOpportunityId()) ? courseInfoBean.getOpportunityId() : request.getParameter("opportunityId");
	    String timerOneStatus = request.getParameter("timerOneStatus");
	    String timerTwoStatus = request.getParameter("timerTwoStatus");
	    String offerName = request.getParameter("offerName");
	    String userId = "0";
	    //getting the decrypted cookie user id for logged in user
	    String cApplyNowUserId = new CookieManager().getCookieValue(request, GlobalConstants.AN_USER_ID_COOKIE);
	    if (!GenericValidator.isBlankOrNull(cApplyNowUserId) && !"0".equals(cApplyNowUserId)) {
	      userId = commonUtil.getDecrytedCookieUseId(cApplyNowUserId);
	      if("0".equals(userId)){
	        response.sendRedirect(seoUrls.applyNowWugoURL(courseId, opportunityId));
	      }
	      String userDispName = commonFunction.getUserNameDetais(userId, request);
	      if(!GenericValidator.isBlankOrNull(userDispName)){
	        request.setAttribute("userDispName", userDispName.toUpperCase());
	      }
	      //System.out.println("whatuni Go user id after decrypt " + userId);
	    } else {
	      seoUrls.applyNowWugoURL(courseId, opportunityId);
	      //System.out.println("User id is null");
	    }
	    //
	     //To get the apply now course details info for the top pod
	     ArrayList courseInfoDetList = commonFunction.getApplyNowCourseDtlsList(applyNowCourseId, applyNowOpportunityId, userId);
	     if (!CommonUtil.isBlankOrNull(courseInfoDetList)) {
	       courseInfoBean = (ApplyNowVO)courseInfoDetList.get(0);
	       request.setAttribute("COLLEGE_ID", courseInfoBean.getCollegeId());
	       referralUrl = seoUrls.construnctCourseDetailsPageURL(GlobalConstants.DEGREE_STUDY_LEVEL, courseInfoBean.getCourseTitle(), courseInfoBean.getCourseId(), courseInfoBean.getCollegeId(), GlobalConstants.CLEARING_PAGE_FLAG, applyNowOpportunityId);
	       request.setAttribute("REFERER_URL", referralUrl);
	       request.setAttribute("hitbox_college_name", courseInfoBean.getCollegeName());
	       
	     } else {
	       //response.sendRedirect(seoUrls.getWhatuniHomeURL());
	       //System.out.println("courseInfoDetList is emppyt so redirecting");
	     }
	     //
	    String clearingOnOff = commonFunction.getWUSysVarValue("CLEARING_ON_OFF");
	    String clearingFlag = "";
	    if (("ON").equals(clearingOnOff)) {
	      clearingFlag = "Y";
	      request.setAttribute("studyLevelDesc", "Clearing");
	      commonUtil.setClearingUserType(request, response);//setting the user type to clearing
	    }
	    //
	    String pageId = request.getParameter("page-id");
	    String nextPageId = request.getParameter("next-page-id");
	    String status = request.getParameter("status");
	    String reviewFlag = request.getParameter("review-flag");
	    String action = request.getParameter("action");
	    //To check the user journey for the apply now button click
	    if (!GenericValidator.isBlankOrNull((String)request.getAttribute("checkUserJourneyFlag"))) {
	      action = "check-user-journey";
	    }
	    //
	    String htmlId = request.getParameter("htmlId");
	    //To get wugo domain path
	    String httpPostURL = CommonUtil.getWugoDomain(request);    
	    //    
	    if ("EDIT".equalsIgnoreCase(action)) {
	      httpPostURL += "/update-qualification-details/";
	    } else if ("PROV_QUAL_UPDATE".equalsIgnoreCase(action)) {
	      httpPostURL += "/update-provisional-qualification-details/";
	    } else if ("USER_REVIEW_QUAL_UPDATE".equalsIgnoreCase(action)) {
	      httpPostURL += "/update-qualification-details/";
	    } else if ("CALCULATE_UCAS".equalsIgnoreCase(action)) {
	      httpPostURL += "/calcualte-ucas-points/";
	    } else if ("QUAL_SUB_AJAX".equalsIgnoreCase(action)) {
	      httpPostURL += "/ajax/subject-list/";    
	    } else if ("ALTERNATIVE_COURSE".equalsIgnoreCase(action)) {
	      //httpPostURL += "/get-qualification-details/";
	      httpPostURL += "/update-qualification-details/";
	    }
	    reviewFlag = !GenericValidator.isBlankOrNull(reviewFlag) ? reviewFlag : "N";
	    //
	    wuGoPayloadFromClient.setCourseId(courseId);
	    wuGoPayloadFromClient.setOpportunityId(opportunityId);
	    wuGoPayloadFromClient.setNextPageId(nextPageId);
	    wuGoPayloadFromClient.setPageId(pageId);
	    wuGoPayloadFromClient.setUserStatus(status);
	    wuGoPayloadFromClient.setHtmlId(htmlId);
	    wuGoPayloadFromClient.setReviewFlag("5".equals(nextPageId) ? "Y" : reviewFlag);
	    wuGoPayloadFromClient.setOfferName(offerName);
	    wuGoPayloadFromClient.setUserId(userId);
	    wuGoPayloadFromClient.setQualConfirmFlag(qualConfirmFlag);
	    wuGoPayloadFromClient.setAccessToken(GlobalConstants.WU_GO_ACCESS_TOKEN);
	    wuGoPayloadFromClient.setAffiliateId(GlobalConstants.WHATUNI_AFFILATE_ID);
	    wuGoPayloadFromClient.setFreeText(freeText);
	    //
	    //System.out.println("httpPostURL-->" + httpPostURL);
	    try {
	      Gson gson = new GsonBuilder().create();
	      String json = gson.toJson(wuGoPayloadFromClient, WuGoBean.class);
	      String jsonBodyContent = getBody(request);
	      WuGoBean JsonBean = gson.fromJson(jsonBodyContent, WuGoBean.class);
	      jsonBodyContent = jsonBodyContent.substring(0, jsonBodyContent.lastIndexOf("}")) + "," + json.substring(1);
	      //System.out.println("jsonBodyContent--->" + jsonBodyContent);
	      HttpPost requestPost = new HttpPost(httpPostURL);
	      StringEntity params = new StringEntity(jsonBodyContent);
	      requestPost.addHeader("content-type", "application/json");
	      requestPost.setEntity(params);
	      responsePost = httpClient.execute(requestPost);
	      //      
	      String responseStatusCode = responsePost.getStatusLine().toString();
	      responseAsString = EntityUtils.toString(responsePost.getEntity());
	      if ("HTTP/1.1 200 OK".equalsIgnoreCase(responseStatusCode)) {
	        //System.out.println("responseStatusCode-->" + responseStatusCode);
	        OfferOutputBean offerOutPayLoad = new OfferOutputBean();
	        offerOutPayLoad = new Gson().fromJson(responseAsString, OfferOutputBean.class);
	        ArrayList<CourseInfoVO> courseInfoList = offerOutPayLoad.getCourse_info_list();
	        ArrayList<CourseInfoVO> alternateOfferInfoList = offerOutPayLoad.getAlternate_offer_info_list();
	        ArrayList<CourseInfoVO> alternateUniversityInfoList = offerOutPayLoad.getAlternate_university_info_list();
	        String userName = offerOutPayLoad.getUser_name();
	        String shortageOfUserUcasPoint = offerOutPayLoad.getUser_ucas_point_shortage();
	        applicationCount = offerOutPayLoad.getApplicant_count();
	        ArrayList<QuestionAnswerVO> questionAnswerList = offerOutPayLoad.getQuestion_answer_list();
	        String staticContent = offerOutPayLoad.getStatic_text();
	        String ucasPoint = offerOutPayLoad.getUCAS_point();
	        ArrayList<PageInfoVO> pageStatusList = offerOutPayLoad.getPage_status_list();
	        ArrayList<PageInfoVO> sectionInfoList = offerOutPayLoad.getReview_section_list();
	        String qualValFlag = offerOutPayLoad.getQual_val_flag();
	        String qualErrorCode = offerOutPayLoad.getValidation_error_code();
	        String userTariffPoints = offerOutPayLoad.getUser_tariff_points();
	        String applyNowSuccessFlag = offerOutPayLoad.getApply_now_success_flag();
	        String timer1Status = offerOutPayLoad.getTimer1_status();
	        String timer2Status = offerOutPayLoad.getTimer2_status();
	        String userUcasTraiffPoint = offerOutPayLoad.getUser_ucas_point();
	        String sectionReviewedFlag = offerOutPayLoad.getSection_review_flag();
	        String reqSubCount = offerOutPayLoad.getReq_sub_count();
	        String qualificationConfirmFlag = offerOutPayLoad.getQual_confirm_flag();
	        String cTimerEndTime = "0"; //offerOutPayLoad.getTimer1_end_time();       1 min it is mentioned in millliseconds        
	        if (!GenericValidator.isBlankOrNull(offerOutPayLoad.getTimer1_end_time())) {
	          cTimerEndTime = offerOutPayLoad.getTimer1_end_time();
	          //System.out.println("timer 1:");
	        } else if (!GenericValidator.isBlankOrNull(offerOutPayLoad.getTimer2_end_time())) {
	          cTimerEndTime = offerOutPayLoad.getTimer2_end_time();
	          //System.out.println("timer 2:");
	        }
	        //System.out.println("timer :" + cTimerEndTime);
	        ArrayList<QualificationsVO> qualList = offerOutPayLoad.getQual_list();
	        ArrayList<UserqualificationsVO> userQualList = offerOutPayLoad.getUser_qual_list();
	        ArrayList<SubjectAjaxListVO> ajaxSubjectList = offerOutPayLoad.getQual_subject_list();
	        if (courseInfoList != null && courseInfoList.size() > 0) {
	          CourseInfoVO coureInfovo = courseInfoList.get(0);
	          String strCourseName = "";
	          strCourseName = commonFunction.getCourseName(coureInfovo.getCourse_id(), clearingFlag);
	          strCourseName = strCourseName != null && strCourseName.trim().length() > 0 ? strCourseName.trim().replaceAll("\\/", "-") : "";
	          strCourseName = (strCourseName != null && strCourseName.trim().length() > 0 ? commonFunction.replaceHypen(commonFunction.replaceURL(commonFunction.replaceSpecialCharacter(strCourseName))) : "");
	          request.setAttribute("hitbox_course_title", strCourseName);
	          request.setAttribute("hitbox_college_name", coureInfovo.getCollege_name());
	          request.setAttribute("hitbox_college_id", coureInfovo.getCollege_id());
	          request.setAttribute("COLLEGE_DISPLAY_NAME", coureInfovo.getCollege_display_name());
	          request.setAttribute("COURSE_REQ_TARIFF_POINT", coureInfovo.getTariff_points());
	          request.setAttribute("courseInfoList", courseInfoList);
	        }
	        if (pageStatusList != null && pageStatusList.size() > 0) {
	          Iterator pageItr = pageStatusList.iterator();
	          while (pageItr.hasNext()) {
	            PageInfoVO pageVO = (PageInfoVO)pageItr.next();
	            if ("Y".equalsIgnoreCase(pageVO.getCurrent_page_flag())) {
	              request.setAttribute("pageName", pageVO.getPage_name());
	              request.setAttribute("pageNo", pageVO.getPage_id());
	              request.setAttribute("pageStatus", pageVO.getStatus());
	              break;
	            }
	          }
	          request.setAttribute("pageInfoList", pageStatusList);
	        }
	        //Ajax call for subject list in qualification edit page
	        if (ajaxSubjectList != null && ajaxSubjectList.size() > 0) {
	          request.setAttribute("ajaxSubjectList", ajaxSubjectList);          
	        }
	        if (!GenericValidator.isBlankOrNull(sectionReviewedFlag)) {
	          request.setAttribute("SECTION_REVIEWED_FLAG", sectionReviewedFlag);
	        }
	        if (sectionInfoList != null && sectionInfoList.size() > 0) {
	          request.setAttribute("sectionInfoList", sectionInfoList);
	        }
	        if (!GenericValidator.isBlankOrNull(staticContent)) {
	          request.setAttribute("staticContent", staticContent);
	        }
	        if (questionAnswerList != null && questionAnswerList.size() > 0) {
	          Iterator qusAnsItr = questionAnswerList.iterator();
	          while (qusAnsItr.hasNext()) {
	            QuestionAnswerVO qusAnsVO = (QuestionAnswerVO)qusAnsItr.next();
	            if ("24".equalsIgnoreCase(qusAnsVO.getPage_question_id())) {
	              String qualTCCheckbox = qusAnsVO.getAnswer_value();
	              if(GenericValidator.isBlankOrNull(qualTCCheckbox)){
	                qualTCCheckbox = "N";
	              }
	              request.setAttribute("qualTCCheckbox", qualTCCheckbox);
	              break;
	            }
	          }
	          request.setAttribute("questionAnswerList", questionAnswerList);
	        }
	        if(!GenericValidator.isBlankOrNull(qualificationConfirmFlag)){
	          request.setAttribute("qualTCCheckbox", qualificationConfirmFlag);
	        }
	        if (qualList != null && qualList.size() > 0) {
	          request.setAttribute("qualificationList", qualList);
	          //System.out.println(qualList.size() + "qualList size");
	        }
	        if (userQualList != null && userQualList.size() > 0) {
	          request.setAttribute("userQualList", userQualList);
	          request.setAttribute("userQualSize", userQualList.size());
	          request.setAttribute("className", "qual_cnt");
	        }
	        if (!GenericValidator.isBlankOrNull(qualValFlag)) {
	          request.setAttribute("qualError", qualValFlag);
	        }
	        if (!GenericValidator.isBlankOrNull(qualErrorCode)) {
	          request.setAttribute("qualErrorCode", qualErrorCode);
	          //System.out.println("qualErrorCode" + qualErrorCode);
	        }
	        if (sectionInfoList != null && sectionInfoList.size() > 0) {
	          request.setAttribute("sectionInfoList", sectionInfoList);
	        }
	        if (!GenericValidator.isBlankOrNull(userTariffPoints)) {
	          request.setAttribute("userTariff", userTariffPoints);
	        }
	        if (!commonUtil.isBlankOrNull(alternateOfferInfoList)) {
	          request.setAttribute("ALTERNATE_OFFER_INFO_LIST", alternateOfferInfoList);
	        }
	        if (!commonUtil.isBlankOrNull(alternateOfferInfoList)) {
	          request.setAttribute("ALTERNATE_UNIVERSITY_INFO_LIST", alternateUniversityInfoList);
	        }
	        if (!GenericValidator.isBlankOrNull(userName)) {
	          request.setAttribute("USER_NAME", userName);
	        }
	        if (!GenericValidator.isBlankOrNull(shortageOfUserUcasPoint)) {
	          request.setAttribute("SHORTAGE_USER_UCAS_TARIFF_POINT", shortageOfUserUcasPoint);
	        }
	        if (!GenericValidator.isBlankOrNull(applicationCount)) {
	          request.setAttribute("APPLICANTION_COUNT", applicationCount);
	        }
	        if (!GenericValidator.isBlankOrNull(userUcasTraiffPoint)) {
	          request.setAttribute("USER_UCAS_TARIFF_POINT", userUcasTraiffPoint);
	        }
	        if (!GenericValidator.isBlankOrNull(applyNowSuccessFlag)) {
	          request.setAttribute("APPLY_NOW_SUCCESS_FLAG", applyNowSuccessFlag);
	        }
	        //Request for success page
	        if (!commonUtil.isBlankOrNull(courseInfoDetList)) {
	          request.setAttribute("APPLY_NOW_COURSE_LIST", courseInfoDetList);
	        }
	        if (!GenericValidator.isBlankOrNull(applyNowCourseId)) {
	          request.setAttribute("APPLY_NOW_COURSE_ID", applyNowCourseId);
	        }
	        if (!GenericValidator.isBlankOrNull(applyNowOpportunityId)) {
	          request.setAttribute("APPLY_NOW_OPPORTUNITY_ID", applyNowOpportunityId);
	        }
	        //
	        if ("FAILED".equalsIgnoreCase(qualValFlag)) {
	          String returnString = "";
	          StringBuffer buffer = new StringBuffer();
	          response.setContentType("text/html");
	          response.setHeader("Cache-Control", "no-cache");
	          returnString = "QUAL_ERROR"; //QUAL_ERROR##SPLIT##FAILED##SPLIT##1##SPLIT##undefined##SPLIT##undefined##SPLIT##56##SPLIT##undefined##SPLIT##null
	                                       //QUAL_ERROR##SPLIT##FAILED##SPLIT##1##SPLIT##A Level##SPLIT##A Level##SPLIT##56##SPLIT##56##SPLIT##null
	          returnString += GlobalConstants.SPLIT_CONSTANT + qualValFlag + GlobalConstants.SPLIT_CONSTANT + qualErrorCode;
	          returnString += GlobalConstants.SPLIT_CONSTANT + oldQualDesc;
	          returnString += GlobalConstants.SPLIT_CONSTANT + newQualDesc;
	          returnString += GlobalConstants.SPLIT_CONSTANT + oldGrade;
	          returnString += GlobalConstants.SPLIT_CONSTANT + newGrade;
	          returnString += GlobalConstants.SPLIT_CONSTANT + reqSubCount;
	          buffer.append(returnString);
	          setResponseText(response, buffer);
	          return null;
	        } else if ("CALCULATE_UCAS".equalsIgnoreCase(action)) {
	          String returnString = "";
	          StringBuffer buffer = new StringBuffer();
	          response.setContentType("text/html");
	          response.setHeader("Cache-Control", "no-cache");
	          returnString = "UCAS_POINT";
	          returnString += GlobalConstants.SPLIT_CONSTANT + ucasPoint;
	          buffer.append(returnString);
	          setResponseText(response, buffer);
	          return null;
	        } else if ("STATIC_CONTENT".equalsIgnoreCase(action)) {
	          if ("ACCEPTING_OFFER_TERMS_AND_CONDITION".equalsIgnoreCase(htmlId)) {
	            forwardPage = "termsLightBox";
	          } else if ("FINAL_REVIEW".equalsIgnoreCase(htmlId)) {
	            forwardPage = "finalReviewPage";
	          } else if ("ACCEPTING_OFFER_SUCCESS".equalsIgnoreCase(htmlId)) {
	            forwardPage = "offerSuccessPage";
	          }
	        } else if ("EXPIRED".equalsIgnoreCase(timer1Status) || "EXPIRED".equalsIgnoreCase(timer2Status)) {
	          if ("EXPIRED".equalsIgnoreCase(timerOneStatus) || "EXPIRED".equalsIgnoreCase(timerTwoStatus)) {
	            String returnString = "";
	            StringBuffer buffer = new StringBuffer();
	            response.setContentType("text/html");
	            response.setHeader("Cache-Control", "no-cache");
	            returnString = "TIMER_EXPIRY";
	            if (!GenericValidator.isBlankOrNull(timer1Status)) {
	              returnString += GlobalConstants.SPLIT_CONSTANT + timer1Status;
	            }
	            if (!GenericValidator.isBlankOrNull(timer2Status)) {
	              returnString += GlobalConstants.SPLIT_CONSTANT + timer2Status;
	            }
	            buffer.append(returnString);
	            setResponseText(response, buffer);
	            return null;
	          } else {
	            request.setAttribute("timerExpCall", "Y");
	          }
	        } else if ("QUAL_SUB_AJAX".equalsIgnoreCase(action)) {
	          forwardPage = "ajaxSubjectPage";
	        } else if ("PROV_QUAL_UPDATE".equalsIgnoreCase(action)) {
	          forwardPage = "provisionalOfferTermsConditionPage";
	        } else if ("QUAL_EDIT".equalsIgnoreCase(action)) {
	          forwardPage = "editReviewQualPage";
	        } else if ("ALTERNATIVE_COURSE".equals(action)) {
	          forwardPage = "alternateCoursePage";
	        } else if ("USER_REVIEW_QUAL_UPDATE".equalsIgnoreCase(action)) {
	          forwardPage = "getUserQualReviewPage";
	        } else if ("4".equals(wuGoPayloadFromClient.getNextPageId())) {
	          if("Y".equalsIgnoreCase(markAsFlag)){
	            forwardPage = "entryReqPage";
	          }else if ("EDIT".equalsIgnoreCase(action)) {
	            forwardPage = "editEntryReqPage";
	          }
	        } else if ("5".equals(wuGoPayloadFromClient.getNextPageId())) {
	          forwardPage = "termsLightBox";
	          //forwardPage = "user-review-page";
	        }
	      } else {
	        ExceptionJSON exceptionJSON = new ExceptionJSON();
	        exceptionJSON = new Gson().fromJson(responseAsString, ExceptionJSON.class);
	        int statusCode = !GenericValidator.isBlankOrNull(exceptionJSON.getStatus()) ? Integer.parseInt(exceptionJSON.getStatus()) : 0;
	        response.sendError(statusCode, exceptionJSON.getException());
	      }
	    } catch (Exception securityException) {
	      String exceptionTraces = ExceptionUtils.getStackTrace(securityException);
	      System.out.println("WhatuniGo exception ----->" + exceptionTraces);
	    } finally {
	      httpClient.close();
	    }
     return new ModelAndView(forwardPage);
   }
   public String getBody(HttpServletRequest request) {
	    String body = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    BufferedReader bufferedReader = null;
	    try {
	      InputStream inputStream = request.getInputStream();
	      if (inputStream != null) {
	        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	        char[] charBuffer = new char[128];
	        int bytesRead = -1;
	        while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	          //System.out.println("charBuffer-->" + charBuffer);
	          ////System.out.println("bytesRead-->" + bytesRead);
	          stringBuilder.append(charBuffer, 0, bytesRead);
	        }
	      } else {
	        stringBuilder.append("");
	      }
	    } catch (IOException ex) {
	      ex.printStackTrace();
	      return null;
	    } finally {
	      if (bufferedReader != null) {
	        try {
	          bufferedReader.close();
	        } catch (IOException ex) {
	          ex.printStackTrace();
	          return null;
	        }
	      }
	    }
	    body = stringBuilder.toString();
	    return body;
	  }

	  public String getBody(String request) {
	    String body = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    BufferedReader bufferedReader = null;
	    try {
	      InputStream inputStream = new ByteArrayInputStream(request.getBytes());
	      if (inputStream != null) {
	        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	        char[] charBuffer = new char[128];
	        int bytesRead = -1;
	        while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	          //System.out.println("charBuffer-->" + charBuffer);
	          //System.out.println("bytesRead-->" + bytesRead);
	          stringBuilder.append(charBuffer, 0, bytesRead);
	        }
	      } else {
	        stringBuilder.append("");
	      }
	    } catch (IOException ex) {
	      ex.printStackTrace();
	      return null;
	    } finally {
	      if (bufferedReader != null) {
	        try {
	          bufferedReader.close();
	        } catch (IOException ex) {
	          ex.printStackTrace();
	          return null;
	        }
	      }
	    }
	    body = stringBuilder.toString();
	    return body;
	  }

	  private void setResponseText(HttpServletResponse response, StringBuffer responseMessage) {
	    try {
	      response.setContentType("TEXT/PLAIN");
	      Writer writer = response.getWriter();
	      writer.write(responseMessage.toString());
	    } catch (IOException exception) {
	      exception.printStackTrace();
	    }
	  }
}
