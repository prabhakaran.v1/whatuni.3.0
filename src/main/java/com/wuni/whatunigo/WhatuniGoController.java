package com.wuni.whatunigo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mobile.util.MobileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.struts.util.LabelValueBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUrls;
import com.wuni.valueobjects.chatbot.ExceptionJSON;
import com.wuni.valueobjects.whatunigo.ApplyNowVO;
import com.wuni.valueobjects.whatunigo.PageInfoVO;
import com.wuni.valueobjects.whatunigo.QualificationsVO;
import com.wuni.valueobjects.whatunigo.QuestionAnswerVO;
import com.wuni.valueobjects.whatunigo.SubjectAjaxListVO;
import com.wuni.valueobjects.whatunigo.UserqualificationsVO;
import com.wuni.valueobjects.whatunigo.course.CourseInfoVO;
import com.wuni.valueobjects.whatunigo.stats.StatsLoggingVO;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.whatunigo.bean.OfferOutputBean;
import WUI.whatunigo.bean.StatsOutputBean;
import WUI.whatunigo.bean.WuGoBean;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Class         : WhatuniGoAction.java
 * Description   : Action to get the response for Offer journey from WhatuniGo webservice.
 * @version      : 1.0
 * @author       : Prabhakaran V.
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * Prabhakaran V.          1.0             07.02.2019        New
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */

@Controller
public class WhatuniGoController {
   @RequestMapping(value={"/wugo/apply-offer/success", "/wugo/provisional-offer/success", "/clearing-match-maker-tool", "/get-application-page"}, method = {RequestMethod.GET,RequestMethod.POST})
   public ModelAndView getWugoDetails(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
	    CloseableHttpClient httpClient = HttpClientBuilder.create().build();
	    HttpResponse responsePost;
	    CommonFunction commonFunction = new CommonFunction();
	    CommonUtil commonUtil = new CommonUtil();
	    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	    SeoUrls seoUrls = new SeoUrls();
	    ApplyNowVO courseInfoBean = new ApplyNowVO();
	    WuGoBean wuGoPayloadFromClient = new WuGoBean();
	    
	    String responseAsString = null;
	    String userId = "0";
	    String referralUrl = "";
	    String forwardPage = "acceptingOfferPage";
	    String journeyName = "PROVISIONAL_FORM";
	    
	    request.setAttribute("HIDE_LOGIN", "Y");
	    request.setAttribute("ebookDim14", "WUGO");
	    //request.setAttribute("showBanner", "no");
	    
	    //To get wugo domain path
	    String httpPostURL = CommonUtil.getWugoDomain(request);    
	    //
	     String fromPage = request.getParameter("action-param");
	    String applyNowCourseId = !GenericValidator.isBlankOrNull(request.getParameter("courseId")) ? request.getParameter("courseId") : null;
	    String applyNowOpportunityId = !GenericValidator.isBlankOrNull(request.getParameter("opportunityId")) ? request.getParameter("opportunityId") : null;
	   
	    //To get the apply now course details info for the top pod
	    String dbName = "";
	    Map getDBNameMap = commonBusiness.getDBName();
	    if(getDBNameMap != null && getDBNameMap.size() > 0){
	      dbName = (String)getDBNameMap.get("DB_NAME");
	    }
	    //    
	    String courseId = !GenericValidator.isBlankOrNull((String)courseInfoBean.getCourseId()) ? courseInfoBean.getCourseId() : request.getParameter("courseId");
	    String opportunityId = !GenericValidator.isBlankOrNull((String)courseInfoBean.getOpportunityId()) ? courseInfoBean.getOpportunityId() : request.getParameter("opportunityId");    
	    String applicationCount = !GenericValidator.isBlankOrNull(request.getParameter("applicationCount")) ? request.getParameter("applicationCount") : "";    
	    
	    
	    //getting the decrypted cookie user id for logged in user
	    String cApplyNowUserId = new CookieManager().getCookieValue(request, GlobalConstants.AN_USER_ID_COOKIE);
	    if (!GenericValidator.isBlankOrNull(cApplyNowUserId)) {
	      userId = commonUtil.getDecrytedCookieUseId(cApplyNowUserId);
	      if("0".equals(userId)){
	        
	        response.sendRedirect(seoUrls.applyNowWugoURL(courseId, opportunityId));
	      }
	      //System.out.println("whatuni Go user id after decrypt " + userId);
	      request.setAttribute("cDimUID", userId);
	      String userDispName = commonFunction.getUserNameDetais(userId, request);
	      if(!GenericValidator.isBlankOrNull(userDispName)){
	        request.setAttribute("userDispName", userDispName.toUpperCase());
	      }
	    } else {
	       response.sendRedirect(seoUrls.applyNowWugoURL(courseId, opportunityId));
	      //System.out.println("User id is null");
	    }
	     //
	     ArrayList courseInfoDetList = commonFunction.getApplyNowCourseDtlsList(applyNowCourseId, applyNowOpportunityId, userId);
	     if (!CommonUtil.isBlankOrNull(courseInfoDetList)) {
	       courseInfoBean = (ApplyNowVO)courseInfoDetList.get(0);
	       referralUrl = seoUrls.construnctCourseDetailsPageURL(GlobalConstants.DEGREE_STUDY_LEVEL, courseInfoBean.getCourseTitle(), courseInfoBean.getCourseId(), courseInfoBean.getCollegeId(), GlobalConstants.CLEARING_PAGE_FLAG, applyNowOpportunityId);
	       request.setAttribute("hitbox_college_name", courseInfoBean.getCollegeName());
	       request.setAttribute("COLLEGE_ID", courseInfoBean.getCollegeId());
		   request.setAttribute("REFERER_URL", referralUrl);
	     }
	    //    
	     String clearingOnOff = new CommonFunction().getWUSysVarValue("CLEARING_ON_OFF");
	     String clearingFlag = "";
	     if (("ON").equals(clearingOnOff)) {
	       clearingFlag = "Y";
	       request.setAttribute("studyLevelDesc","Clearing");
	       commonUtil.setClearingUserType(request, response);//setting the user type to clearing
	     }
	    //
	    String pageId = request.getParameter("page-id");
	    String nextPageId = request.getParameter("next-page-id");
	    String status = request.getParameter("status");
	    String reviewFlag = request.getParameter("review-flag");
	    String resultQus = request.getParameter("resultQus");
	    String resultAnsArr = request.getParameter("resultAnsArr");
	    String resultOptArr = request.getParameter("resultOptArr");
	    String freeText = request.getParameter("free-text");
	    String action = request.getParameter("action");
	    String statsParam = request.getParameter("stats-param");
	    String fromUserPage = request.getParameter("fromUserPage");
	    String qualConfirmFlag = request.getParameter("qualConfirmFlag");
	    //To check the user journey for the apply now button click
	    if (!GenericValidator.isBlankOrNull((String)request.getAttribute("checkUserJourneyFlag"))) {
	      action = "check-user-journey";
	    }
	    //    
	    String htmlId = request.getParameter("htmlId");
	    //Timer one expiry call related data    
	    String timer1Data = request.getParameter("timer1Data");
	    //
	    if("STATS_LOG".equalsIgnoreCase(statsParam)){
	      String logId = getStatsLogData(request, response);
	      String returnString = "";
	      StringBuffer buffer = new StringBuffer();
	      response.setContentType("text/html");
	      response.setHeader("Cache-Control", "no-cache");
	      returnString = "STATS";
	      returnString += GlobalConstants.SPLIT_CONSTANT + logId;
	      buffer.append(returnString);
	      setResponseText(response, buffer);
	      return null;
	    }
	    if ("GET".equalsIgnoreCase(action) || "STATIC_CONTENT".equalsIgnoreCase(action)) {
	      httpPostURL += "/get-user-details/";
	    } else if ("UPDATE".equalsIgnoreCase(action)) {
	      httpPostURL += "/update-user-details/";
	    } else if ("EDIT".equalsIgnoreCase(action) || "QUAL_GET".equalsIgnoreCase(action) || "QUAL_EDIT".equalsIgnoreCase(action) || "DELETE_USER_DATA".equalsIgnoreCase(action)) {
	      httpPostURL += "/get-qualification-details/";
	    } else if ("USER_REVIEW_QUAL_UPDATE".equalsIgnoreCase(action)) {
	      httpPostURL += "/update-qualification-details/";
	    } else if ("QUAL_UPDATE".equalsIgnoreCase(action)) {
	    } else if ("PROV_QUAL_UPDATE".equalsIgnoreCase(action)) {
	      httpPostURL += "/update-provisional-qualification-details/";
	    } else if ("PROV_QUAL".equalsIgnoreCase(action) || "PROV_QUAL_EDIT".equalsIgnoreCase(action)) {
	      if("PROV_QUAL".equalsIgnoreCase(action)){
	        //referralUrl = seoUrls.construnctCourseDetailsPageURL(GlobalConstants.DEGREE_STUDY_LEVEL, courseTitle, courseId, collegeId, GlobalConstants.CLEARING_PAGE_FLAG);
	        //request.setAttribute("REFERER_URL", referralUrl);                
	        wuGoPayloadFromClient.setReferralUrl(referralUrl);
	      }
	      httpPostURL += "/get-provisional-qualification-details/";
	    } else if ("check-user-journey".equalsIgnoreCase(action) || ("5a").equalsIgnoreCase(action) || ("provisional-offer-tc").equalsIgnoreCase(action) || ("provisional-offer-loading").equalsIgnoreCase(action) || ("provisonal-offer-congrats").equalsIgnoreCase(action) || ("timer-1-expiry").equalsIgnoreCase(action)) {
	      httpPostURL += "/get-provisional-offer-details/";
	    } else if ("QUAL_SUB_AJAX".equalsIgnoreCase(action)) {
	      httpPostURL += "/ajax/subject-list/";
	    }else if("ALTERNATIVE_COURSE".equalsIgnoreCase(action)){
	      //httpPostURL += "/get-qualification-details/";
	       httpPostURL += "/update-qualification-details/";
	    }  else if ("ACCEPTING_OFFER_SUCCESS".equalsIgnoreCase(action)) {
	      httpPostURL += "/apply-offer/success/";
	    } else if ("APPLICATION-PAGE".equalsIgnoreCase(action) || "CANCEL_OFFER_TIMER1".equalsIgnoreCase(action) || "CANCEL_OFFER_TIMER2".equalsIgnoreCase(action)) {
	           httpPostURL += "/get-application-page/";
	         } 
	     else {
	      if(!("SECTION_REVIEW".equalsIgnoreCase(action) || "GENERATE_APPLICATION".equalsIgnoreCase(action) || "timer-2-expiry".equalsIgnoreCase(action) || "INTL".equalsIgnoreCase(action) || (!GenericValidator.isBlankOrNull(pageId) && "12345".indexOf(pageId) > -1))){
	        //referralUrl = seoUrls.construnctCourseDetailsPageURL(GlobalConstants.DEGREE_STUDY_LEVEL, courseTitle, courseId, collegeId, GlobalConstants.CLEARING_PAGE_FLAG);
	        request.setAttribute("REFERER_URL", referralUrl);        
	        wuGoPayloadFromClient.setReferralUrl(referralUrl);
	      }     
	      httpPostURL += "/get-offer-details/";
	    }
	    //
	        
	    String timerOneStatus = request.getParameter("timerOneStatus");
	    String timerTwoStatus = request.getParameter("timerTwoStatus");
	    String offerName = request.getParameter("offerName");
	    if("ACCEPTING_OFFER".equalsIgnoreCase(offerName)){
	      wuGoPayloadFromClient.setFromAcceptingPage("D");
	    }    
	    //    
	    String qualIdArr = request.getParameter("qualIdArr");
	    String qualLevelArr = request.getParameter("qualLevelArr");
	    String subArr = request.getParameter("subIdArr");
	    String gradeArr = request.getParameter("gradeArr");
	    String gradeptArr = request.getParameter("gradeptArr");
	    String qualSub1 = request.getParameter("qualSub1");
	    String qualSub2 = request.getParameter("qualSub2");
	    String qualSub3 = request.getParameter("qualSub3");
	    pageId = !GenericValidator.isBlankOrNull(pageId) ? pageId : "0";
	    nextPageId = !GenericValidator.isBlankOrNull(nextPageId) ? nextPageId : "1";
	    status = !GenericValidator.isBlankOrNull(status) ? status : "";
	    reviewFlag = !GenericValidator.isBlankOrNull(reviewFlag) ? reviewFlag : "N";
	    if("SECTION_REVIEW".equalsIgnoreCase(action)){reviewFlag = "Y";}
	    //
	    wuGoPayloadFromClient.setAccessToken(GlobalConstants.WU_GO_ACCESS_TOKEN);
	    wuGoPayloadFromClient.setDbName(dbName);
	    wuGoPayloadFromClient.setAffiliateId(GlobalConstants.WHATUNI_AFFILATE_ID);
	    wuGoPayloadFromClient.setUserId(userId);
	    wuGoPayloadFromClient.setReviewFlag("5".equals(nextPageId) ? "Y" : reviewFlag);
	    wuGoPayloadFromClient.setAction(action);
	    wuGoPayloadFromClient.setHtmlId(htmlId);
	    wuGoPayloadFromClient.setOfferName(offerName);
	    wuGoPayloadFromClient.setOpportunityId(opportunityId);
	    wuGoPayloadFromClient.setCourseId(courseId);
	    //
	    wuGoPayloadFromClient.setPageId(pageId);
	    wuGoPayloadFromClient.setUserStatus(status);
	    wuGoPayloadFromClient.setNextPageId(nextPageId);
	    wuGoPayloadFromClient.setQusArr(resultQus);
	    wuGoPayloadFromClient.setAnsArr(resultAnsArr);
	    wuGoPayloadFromClient.setOptArr(resultOptArr);
	    wuGoPayloadFromClient.setQualIdArr(qualIdArr);
	    wuGoPayloadFromClient.setQualLevelArr(qualLevelArr);
	    wuGoPayloadFromClient.setSubIdArr(subArr);
	    wuGoPayloadFromClient.setGradeArr(gradeArr);
	    wuGoPayloadFromClient.setGradeptArr(gradeptArr);
	    wuGoPayloadFromClient.setQualSub1(qualSub1);
	    wuGoPayloadFromClient.setQualSub2(qualSub2);
	    wuGoPayloadFromClient.setQualSub3(qualSub3);
	    wuGoPayloadFromClient.setQualIdArr(qualIdArr);
	    wuGoPayloadFromClient.setTimerOneStatus(timerOneStatus);
	    wuGoPayloadFromClient.setTimerTwoStatus(timerTwoStatus);
	    wuGoPayloadFromClient.setQualConfirmFlag(qualConfirmFlag);
	    wuGoPayloadFromClient.setFreeText(freeText);
	    //Request for applying course id and opportunity id data
	    if (!GenericValidator.isBlankOrNull(applyNowCourseId)) {
	      request.setAttribute("APPLY_NOW_COURSE_ID", applyNowCourseId);
	    }
	    if (!GenericValidator.isBlankOrNull(applyNowOpportunityId)) {
	      request.setAttribute("APPLY_NOW_OPPORTUNITY_ID", applyNowOpportunityId);
	    }
	    //
	    if(!GenericValidator.isBlankOrNull(fromPage)){
	      journeyName = new WhatuniGoRegistrationController().getUrlRedirect(wuGoPayloadFromClient, request, response);
	      if(!journeyName.equalsIgnoreCase(fromPage)){
	        if("PROVISIONAL_FORM".equalsIgnoreCase(journeyName)){
	          response.sendRedirect(seoUrls.provisionalJourneyURL(courseId, opportunityId, "provisional_form"));
	        }else if("OFFER_FORM".equalsIgnoreCase(journeyName)){
	          response.sendRedirect(seoUrls.offerJourneyURL(courseId, opportunityId, "offer_form"));
	        }else if("ERROR_MESSAGE".equalsIgnoreCase(journeyName)){
	          request.setAttribute("REFERER_URL", referralUrl);
	          return new ModelAndView("whatunigoErrorPage");
	        }
	        return null;
	      }
	    }
	    
	    //System.out.println("httpPostURL-->" + httpPostURL);
	    //
	    try {
	      Gson gson = new GsonBuilder().create();
	      String json = gson.toJson(wuGoPayloadFromClient, WuGoBean.class);
	      //System.out.println("cmmtPayloadFromClient-->" + json);
	      HttpPost requestPost = new HttpPost(httpPostURL);
	      StringEntity params = new StringEntity(json);
	      requestPost.addHeader("content-type", "application/json");
	      requestPost.setEntity(params);
	      responsePost = httpClient.execute(requestPost);
	      //      
	      String responseStatusCode = responsePost.getStatusLine().toString();
	      responseAsString = EntityUtils.toString(responsePost.getEntity());
	      if ("HTTP/1.1 200 OK".equalsIgnoreCase(responseStatusCode)) {
	        //System.out.println("responseStatusCode-->" + responseStatusCode);
	        OfferOutputBean offerOutPayLoad = new OfferOutputBean();
	        offerOutPayLoad = new Gson().fromJson(responseAsString, OfferOutputBean.class);
	        ArrayList<CourseInfoVO> courseInfoList = offerOutPayLoad.getCourse_info_list();
	        ArrayList<QuestionAnswerVO> questionAnswerList = offerOutPayLoad.getQuestion_answer_list();
	        String offerText = offerOutPayLoad.getIntial_text();
	        String hotLineNumber = offerOutPayLoad.getHotline_number();
	        String staticContent = offerOutPayLoad.getStatic_text();
	        ArrayList<PageInfoVO> pageStatusList = offerOutPayLoad.getPage_status_list();
	        ArrayList<PageInfoVO> sectionInfoList = offerOutPayLoad.getReview_section_list();
	        String qualValFlag = offerOutPayLoad.getQual_val_flag();
	        String qualErrorCode = offerOutPayLoad.getValidation_error_code();
	        String articleContent = offerOutPayLoad.getArticle_content();
	        String userSubmittedDate = offerOutPayLoad.getUser_submitted_date();
	        //Data for the alternate course page
	        ArrayList<CourseInfoVO> alternateOfferInfoList = offerOutPayLoad.getAlternate_offer_info_list();
	        ArrayList<CourseInfoVO> alternateUniversityInfoList = offerOutPayLoad.getAlternate_university_info_list();
	        String userName = offerOutPayLoad.getUser_name();
	        String shortageOfUserUcasPoint = offerOutPayLoad.getUser_ucas_point_shortage();
	        applicationCount = offerOutPayLoad.getApplicant_count();
	        String placeAvailableFlag = offerOutPayLoad.getPlaces_available_flag();
	        String userStatusCode = offerOutPayLoad.getUser_status_code();
	        String userUcasTraiffPoint = offerOutPayLoad.getUser_ucas_point();
	        String applyNowSuccessFlag = offerOutPayLoad.getApply_now_success_flag();
	        String provisionalFormFlag = offerOutPayLoad.getProvisional_form_flag();
	        String offerFormFlag = offerOutPayLoad.getOffer_form_flag();
	        String errorMessage = offerOutPayLoad.getError_message();
	        String userTariffPoints = offerOutPayLoad.getUser_tariff_points();
	        String qualificationConfirmFlag = offerOutPayLoad.getQual_confirm_flag();
	        String timer1EndTime = offerOutPayLoad.getTimer1_end_time();
	        //System.out.println("timer1EndTime--->>>"+timer1EndTime);
	        String timer2EndTime = offerOutPayLoad.getTimer2_end_time();
	          //System.out.println("timer2EndTime--->>>"+timer2EndTime);
	        String cTimerEndTime = "0";//offerOutPayLoad.getTimer1_end_time();       1 min it is mentioned in millliseconds        
	        String sectionReviewedFlag = offerOutPayLoad.getSection_review_flag();
	        if(!GenericValidator.isBlankOrNull(offerOutPayLoad.getTimer1_end_time())){
	         cTimerEndTime = offerOutPayLoad.getTimer1_end_time();
	          //System.out.println("timer 1:");
	        }
	        else if(!GenericValidator.isBlankOrNull(offerOutPayLoad.getTimer2_end_time())){
	          cTimerEndTime = offerOutPayLoad.getTimer2_end_time();
	          //System.out.println("timer 2:");
	        }
	        //System.out.println("timer :" + cTimerEndTime);
	        //
	        //
	        ArrayList<QualificationsVO> qualList = offerOutPayLoad.getQual_list();
	        ArrayList<UserqualificationsVO> userQualList = offerOutPayLoad.getUser_qual_list();
	        ArrayList<SubjectAjaxListVO> ajaxSubjectList = offerOutPayLoad.getQual_subject_list();
	        String timer1Status = offerOutPayLoad.getTimer1_status();
	        //System.out.println("timer1Status---"+timer1Status);
	        String timer2Status = offerOutPayLoad.getTimer2_status();
	        if (!GenericValidator.isBlankOrNull(offerText)) {
	          request.setAttribute("offerText", offerText);
	        }
	        if (courseInfoList != null && courseInfoList.size() > 0) {
	          CourseInfoVO coureInfovo = courseInfoList.get(0);
	          String strCourseName = "";
	          strCourseName = commonFunction.getCourseName(coureInfovo.getCourse_id(), clearingFlag);
	          strCourseName = strCourseName != null && strCourseName.trim().length() > 0 ? strCourseName.trim().replaceAll("\\/", "-") : "";
	          strCourseName = (strCourseName != null && strCourseName.trim().length() > 0 ? commonFunction.replaceHypen(commonFunction.replaceURL(commonFunction.replaceSpecialCharacter(strCourseName))) : "");
	          request.setAttribute("hitbox_course_title", strCourseName);
	           request.setAttribute("hitbox_college_name", coureInfovo.getCollege_name());
	          request.setAttribute("hitbox_college_id", coureInfovo.getCollege_id());
	          
	          request.setAttribute("COLLEGE_DISPLAY_NAME", coureInfovo.getCollege_display_name());
	          request.setAttribute("COURSE_REQ_TARIFF_POINT", coureInfovo.getTariff_points());
	          request.setAttribute("COURSE_NAME", coureInfovo.getCourse_name());
	          if(!GenericValidator.isBlankOrNull(coureInfovo.getCourse_id())){
	            request.setAttribute("APPLY_NOW_COURSE_ID", coureInfovo.getCourse_id());
	          }
	          if(!GenericValidator.isBlankOrNull(coureInfovo.getOpportunity_id())){
	            request.setAttribute("APPLY_NOW_OPPORTUNITY_ID", coureInfovo.getOpportunity_id());
	          }
	            if(!GenericValidator.isBlankOrNull(coureInfovo.getCourse_deleted_flag())){
	            request.setAttribute("DELETED_COURSE_FLAG", coureInfovo.getCourse_deleted_flag());
	            }
	            if("APPLICATION-PAGE".equalsIgnoreCase(action)){
	              coureInfovo.setCdPageUrl(seoUrls.construnctCourseDetailsPageURL(GlobalConstants.DEGREE_STUDY_LEVEL, coureInfovo.getCourse_Title(),  coureInfovo.getCourse_id(), coureInfovo.getCollege_id(), GlobalConstants.CLEARING_PAGE_FLAG, applyNowOpportunityId));
	              coureInfovo.setProfilePageUrl(seoUrls.construnctProfileURL(coureInfovo.getCollege_id(),coureInfovo.getCollege_name(),GlobalConstants.CLEARING_PAGE_FLAG));
	              request.setAttribute("REFERER_URL",coureInfovo.getCdPageUrl());
	            }
	          request.setAttribute("courseInfoList", courseInfoList);
	        }
	        if (pageStatusList != null && pageStatusList.size() > 0) {
	          Iterator pageItr = pageStatusList.iterator();
	          while (pageItr.hasNext()) {
	            PageInfoVO pageVO = (PageInfoVO)pageItr.next();
	            if ("Y".equalsIgnoreCase(pageVO.getCurrent_page_flag())) {
	              request.setAttribute("pageName", pageVO.getPage_name());
	              request.setAttribute("pageNo", pageVO.getPage_id());
	              request.setAttribute("pageStatus", pageVO.getStatus());
	              break;
	            }
	          }
	          request.setAttribute("pageInfoList", pageStatusList);
	        }
	        if(!GenericValidator.isBlankOrNull(sectionReviewedFlag)){
	          request.setAttribute("SECTION_REVIEWED_FLAG", sectionReviewedFlag);
	        }
	        if (sectionInfoList != null && sectionInfoList.size() > 0) {
	          request.setAttribute("sectionInfoList", sectionInfoList);
	        }
	        if (!GenericValidator.isBlankOrNull(hotLineNumber)) {
	          request.setAttribute("hotLine", hotLineNumber);
	        }
	          if (!GenericValidator.isBlankOrNull(timer1EndTime)) {
	            request.setAttribute("timer1EndTime", timer1EndTime);
	          }
	          if (!GenericValidator.isBlankOrNull(timer2EndTime)) {
	            request.setAttribute("timer2EndTime", timer2EndTime);
	          }
	        if (!GenericValidator.isBlankOrNull(staticContent)) {
	          request.setAttribute("staticContent", staticContent);
	        }
	        if (questionAnswerList != null && questionAnswerList.size() > 0) {
	          Iterator qusAnsItr = questionAnswerList.iterator();
	          while (qusAnsItr.hasNext()) {
	            QuestionAnswerVO qusAnsVO = (QuestionAnswerVO)qusAnsItr.next();
	            if ("24".equalsIgnoreCase(qusAnsVO.getPage_question_id())) {
	              String qualTCCheckbox = qusAnsVO.getAnswer_value();
	              if(GenericValidator.isBlankOrNull(qualTCCheckbox)){
	                qualTCCheckbox = "N";
	              }
	              request.setAttribute("qualTCCheckbox", qualTCCheckbox);
	              break;
	            }
	          }
	          request.setAttribute("questionAnswerList", questionAnswerList);
	        }
	        if(!GenericValidator.isBlankOrNull(qualificationConfirmFlag)){
	          request.setAttribute("qualTCCheckbox", qualificationConfirmFlag);
	        }
	        if (qualList != null && qualList.size() > 0) {
	          request.setAttribute("qualificationList", qualList);
	          //System.out.println(qualList.size() + "qualList size");
	        }
	        if (userQualList != null && userQualList.size() > 0) {
	          request.setAttribute("userQualList", userQualList);
	          request.setAttribute("userQualSize", String.valueOf(userQualList.size()));
	          request.setAttribute("className", "qual_cnt");
	        }
	        if (!GenericValidator.isBlankOrNull(qualValFlag)) {
	          request.setAttribute("qualError", qualValFlag);
	        }
	        if(!GenericValidator.isBlankOrNull(qualErrorCode)){
	          request.setAttribute("qualErrorCode", qualErrorCode);
	          //System.out.println("qualErrorCode"+ qualErrorCode);
	        }
	        //Ajax call for subject list in qualification edit page
	        if (ajaxSubjectList != null && ajaxSubjectList.size() > 0) {
	          request.setAttribute("ajaxSubjectList", ajaxSubjectList);
	          //System.out.println(ajaxSubjectList.size() + "ajaxSubjectList");
	        }
	        //Request for the alternate course page
	        if (!commonUtil.isBlankOrNull(alternateOfferInfoList)) {
	          request.setAttribute("ALTERNATE_OFFER_INFO_LIST", alternateOfferInfoList);
	        }
	        if (!commonUtil.isBlankOrNull(alternateOfferInfoList)) {
	          request.setAttribute("ALTERNATE_UNIVERSITY_INFO_LIST", alternateUniversityInfoList);
	        }
	        if (!GenericValidator.isBlankOrNull(userName)) {
	          request.setAttribute("USER_NAME", userName);
	        }
	        if (!GenericValidator.isBlankOrNull(userUcasTraiffPoint)) {
	          request.setAttribute("USER_UCAS_TARIFF_POINT", userUcasTraiffPoint);
	        }
	        if (!GenericValidator.isBlankOrNull(shortageOfUserUcasPoint)) {
	          request.setAttribute("SHORTAGE_USER_UCAS_TARIFF_POINT", shortageOfUserUcasPoint);
	        }
	        if (!GenericValidator.isBlankOrNull(applicationCount)) {
	          request.setAttribute("APPLICANTION_COUNT", applicationCount);
	        }
	        if (!GenericValidator.isBlankOrNull(applyNowSuccessFlag)) {
	          request.setAttribute("APPLY_NOW_SUCCESS_FLAG", applyNowSuccessFlag);
	        }
	        //Request for success page
	        if (!GenericValidator.isBlankOrNull(cTimerEndTime)) {
	          request.setAttribute("C_END_TIME_T1", cTimerEndTime);
	        }
	        
	        //
	        
			if(!GenericValidator.isBlankOrNull(userTariffPoints)) {
	          request.setAttribute("userTariff" , userTariffPoints);
	        }
	        //
	         if(!GenericValidator.isBlankOrNull(userStatusCode)) {
	         request.setAttribute("userStatusCode" , userStatusCode);
	         }
	          //
	           if(!GenericValidator.isBlankOrNull(articleContent)) {
	           request.setAttribute("articleContent" , articleContent);
	           }
	          //
	           if(!GenericValidator.isBlankOrNull(userSubmittedDate)) {
	           request.setAttribute("userSubmittedDate" , userSubmittedDate);
	           }
	          //
	           if(!GenericValidator.isBlankOrNull(timer1Status)) {
	           request.setAttribute("timer1Status" , timer1Status);
	           }
	          if(!GenericValidator.isBlankOrNull(timer2Status)) {
	          request.setAttribute("timer2Status" , timer2Status);
	          }
	        //     
	        if (!GenericValidator.isBlankOrNull(provisionalFormFlag) && !GenericValidator.isBlankOrNull(offerFormFlag)) {
	          //String journeyName = "PROVISIONAL_FORM";
	          if ("Y".equalsIgnoreCase(provisionalFormFlag)) {
	            journeyName = "PROVISIONAL_FORM";
	            //response.sendRedirect("/degrees/clearing-match-maker-tool.html?action=prov_qual&courseId=" + courseId + "&opportunityId=" + opportunityId);
	            return null;
	          } else if ("Y".equalsIgnoreCase(offerFormFlag)) {
	            journeyName = "OFFER_FORM";
	            //response.sendRedirect("/degrees/clearing-match-maker-tool.html?courseId=" + courseId + "&opportunityId=" + opportunityId);
	            return null;
	          } else if ("N".equalsIgnoreCase(provisionalFormFlag) && "N".equalsIgnoreCase(offerFormFlag)) {
	            journeyName = "SHOW_ERROR_MESSAGE";
	            
	            String returnString = "";
	            StringBuffer buffer = new StringBuffer();
	            response.setContentType("text/html");
	            response.setHeader("Cache-Control", "no-cache");
	            returnString = "ERROR";
	            returnString += GlobalConstants.SPLIT_CONSTANT + "ERROR" +  GlobalConstants.SPLIT_CONSTANT + errorMessage;
	            buffer.append(returnString);
	            setResponseText(response, buffer);
	            
	            
	            request.setAttribute("ERROR_MESSAGE", errorMessage);
	            request.setAttribute("ERROR_FLAG", "Y");
	            //forwardPage = "accepting-offer-page";
	             return null;
	          }
	          //System.out.println("journeyName " + journeyName);
	          request.setAttribute("JOURNEY_NAME", journeyName);
	         
	        }else if("N".equalsIgnoreCase(placeAvailableFlag)){
	          request.setAttribute("PLCE_AVAIL_FLAG", placeAvailableFlag);
	          request.setAttribute("REFERER_URL", referralUrl);
	          forwardPage = "whatunigoErrorPage";
	        }else if ("FAILED".equalsIgnoreCase(qualValFlag)) {
	          //System.out.println("qualValFlag--->" + qualValFlag);
	          String returnString = "";
	          StringBuffer buffer = new StringBuffer();
	          response.setContentType("text/html");
	          response.setHeader("Cache-Control", "no-cache");
	          returnString = "QUAL_ERROR";
	          returnString += GlobalConstants.SPLIT_CONSTANT + qualValFlag +  GlobalConstants.SPLIT_CONSTANT + qualErrorCode;
	          buffer.append(returnString);
	          setResponseText(response, buffer);
	          return null;
	        } else if ("STATIC_CONTENT".equalsIgnoreCase(action)) {
	          if ("ACCEPTING_OFFER_TERMS_AND_CONDITION".equalsIgnoreCase(htmlId)) {
	            forwardPage = "termsLightBox";
	          } else if ("FINAL_REVIEW".equalsIgnoreCase(htmlId)) {
	            forwardPage = "finalReviewPage";
	          } else if ("ACCEPTING_OFFER_SUCCESS".equalsIgnoreCase(htmlId)) {
	            forwardPage = "offerSuccessPage";
	          }
	        } else if ("EXPIRED".equalsIgnoreCase(timer1Status) || "EXPIRED".equalsIgnoreCase(timer2Status)) {
	          
	          if("EXPIRED".equalsIgnoreCase(timerOneStatus) || "EXPIRED".equalsIgnoreCase(timerTwoStatus)){
	            String returnString = "";
	            StringBuffer buffer = new StringBuffer();
	            response.setContentType("text/html");
	            response.setHeader("Cache-Control", "no-cache");
	            returnString = "TIMER_EXPIRY";
	            if (!GenericValidator.isBlankOrNull(timer1Status)) {
	              returnString += GlobalConstants.SPLIT_CONSTANT + timer1Status;
	            }
	            if (!GenericValidator.isBlankOrNull(timer2Status)) {
	              returnString += GlobalConstants.SPLIT_CONSTANT + timer2Status;
	            }
	            buffer.append(returnString);
	            setResponseText(response, buffer);
	            return null;
	          }else{
	            String whichExp = "";
	            if(!GenericValidator.isBlankOrNull(timer1Status)){
	              whichExp = "TIMER_1";
	            }else if(!GenericValidator.isBlankOrNull(timer2Status)){
	              whichExp = "TIMER_2";
	            }
	            if(!GenericValidator.isBlankOrNull(whichExp)){
	              request.setAttribute("TIMER", whichExp);
	            }
	            request.setAttribute("timerExpCall", "Y");
	            request.setAttribute("REFERER_URL", referralUrl);
	            forwardPage = "whatunigoErrorPage";
	          }
	        }
	        else if ("USER_PROFILE_AJAX".equalsIgnoreCase(fromUserPage) && !GenericValidator.isBlankOrNull(userStatusCode)){
	            String returnString = "";
	            StringBuffer buffer = new StringBuffer();
	            response.setContentType("text/html");
	            response.setHeader("Cache-Control", "no-cache");
	            returnString = userStatusCode;
	            buffer.append(returnString);
	            setResponseText(response, buffer);
	            return null;
	        }
	        else if ("APPLICATION-PAGE".equalsIgnoreCase(action)) {      
	            forwardPage = "applicationPage";
	        } 
	        else if("SECTION_REVIEW".equalsIgnoreCase(action) || "GENERATE_APPLICATION".equalsIgnoreCase(action) || "DELETE_USER_DATA".equalsIgnoreCase(action)){
	          String returnString = "";          
	          String reviewSuccessFlag = "";
	          String generateApplicationFlag = "";
	          if("SECTION_REVIEW".equalsIgnoreCase(action)){
	            reviewSuccessFlag = offerOutPayLoad.getSuccess_flag();
	            returnString = "SECTION_REVIEW_FLAG";
	          }else if("GENERATE_APPLICATION".equalsIgnoreCase(action)){
	            generateApplicationFlag = offerOutPayLoad.getGenerate_application_flag();
	            returnString = "GENERATE_APPLICATION" + "_" +generateApplicationFlag;
	          }else if("DELETE_USER_DATA".equalsIgnoreCase(action)){
	            returnString = "DELETION_USER_MESSAGE_"+offerOutPayLoad.getDeletion_message();
	          }
	          StringBuffer buffer = new StringBuffer();
	          response.setContentType("text/html");
	          response.setHeader("Cache-Control", "no-cache");          
	          buffer.append(returnString);
	          setResponseText(response, buffer);
	          return null;
	        }else if("INTL".equalsIgnoreCase(action)){
	          //request.setAttribute("pageNo", wuGoPayloadFromClient.getNextPageId());
	          forwardPage = "intlUserPage";
	        }
	        else if ("PROV_QUAL".equalsIgnoreCase(action)) {
	          request.setAttribute("REFERER_URL", referralUrl);          
	          if(userQualList != null && userQualList.size() == 0) {
	            forwardPage = "newUserEntryPage";
	            request.setAttribute("actionName", "PROV_QUAL_UPDATE");
	          } else {
	             forwardPage = "provisionalEntryReqPage";
	          }
	        } else if ("PROV_QUAL_EDIT".equalsIgnoreCase(action)) {
	          request.setAttribute("actionName", "PROV_QUAL_UPDATE");
	          forwardPage = "editEntryReqPage"; //"provisional_edit_entry_req_pag";
	        } else if ("PROV_QUAL_UPDATE".equalsIgnoreCase(action)) {
	          forwardPage = "provisionalOfferTermsConditionPage";
	        } else if ("provisional-offer-loading".equalsIgnoreCase(action)) {
	          forwardPage = "provisionalOfferLoadingPage";
	        }else if ("ALTERNATIVE_COURSE".equals(action)) {
	          forwardPage = "alternateCoursepage";
	        }else if ("provisonal-offer-congrats".equalsIgnoreCase(action)) {
	          request.setAttribute("actionName", action);
	          forwardPage = "provisionalOfferCongratsPage";
	        } else if ("timer-1-expiry".equalsIgnoreCase(action)) {
	          forwardPage = "timerExpiredPage";
	        } else if ("GET".equalsIgnoreCase(action) || "UPDATE".equalsIgnoreCase(action) || "QUAL_GET".equalsIgnoreCase(action) || "QUAL_EDIT".equalsIgnoreCase(action)) {
	          if ("2".equals(wuGoPayloadFromClient.getPageId())) {
	            dobMethod(request);
	          }
	          if ("QUAL_GET".equalsIgnoreCase(action)) {
	            forwardPage = "getUserQualReviewPage";
	          } else if ("QUAL_EDIT".equalsIgnoreCase(action)) {
	            forwardPage = "editReviewQualPage";
	          } else {
	            forwardPage = "getUserQuestionPage";
	          }
	        } else if ("QUAL_SUB_AJAX".equalsIgnoreCase(action)) {
	          forwardPage = "ajaxSubjectPage";
	        } else if ("0".equals(wuGoPayloadFromClient.getPageId())) {
	          request.setAttribute("REFERER_URL", referralUrl);          
	          forwardPage = "acceptingOfferPage";
	        } else if ("1".equals(wuGoPayloadFromClient.getNextPageId())) {
	          forwardPage = "basicInfoPage";
	        } else if ("2".equals(wuGoPayloadFromClient.getNextPageId())) {
	          if (questionAnswerList != null && questionAnswerList.size() > 0) {
	            dobMethod(request);
	            forwardPage = "personalInfoPage";
	          } else {
	            //request.setAttribute("pageNo", wuGoPayloadFromClient.getNextPageId());
	            forwardPage = "intlUserPage";
	          }
	        } else if ("3".equals(wuGoPayloadFromClient.getNextPageId())) {
	          forwardPage = "contactInfoPage";
	        } else if ("4".equals(wuGoPayloadFromClient.getNextPageId())) {
	          if ("EDIT".equalsIgnoreCase(action)) {
	            forwardPage = "editEntryReqPage";
	          } else {
	            forwardPage = "entryReqPage";
	          }
	        } else if ("5".equals(wuGoPayloadFromClient.getNextPageId())) {          
	          forwardPage = "termsLightBox";
	        } else if ("6".equals(wuGoPayloadFromClient.getNextPageId())) {
	          forwardPage = "termsLightBox";
	        } else if ("7".equalsIgnoreCase(wuGoPayloadFromClient.getNextPageId())) {
	          forwardPage = "finalReviewPage";
	        } else if ("8".equalsIgnoreCase(wuGoPayloadFromClient.getNextPageId())) {
	          forwardPage = "offerSuccessPage";
	        } else if ("5a".equals(wuGoPayloadFromClient.getPageId())) {
	          forwardPage = "alternateCoursePage";
	        } else if ("provisional-offer-tc".equalsIgnoreCase(wuGoPayloadFromClient.getPageId())) {
	          forwardPage = "provisionalOfferTermsConditionPage";
	        } else if ("provisional-offer-loading".equalsIgnoreCase(wuGoPayloadFromClient.getPageId())) {
	          forwardPage = "provisionalOfferLoadingPage";
	        } else if ("provisonal-offer-congrats".equalsIgnoreCase(wuGoPayloadFromClient.getPageId())) {
	          forwardPage = "provisionalOfferCongratsPage";
	        } else if ("timer-1-expiry".equalsIgnoreCase(wuGoPayloadFromClient.getPageId())) {
	          forwardPage = "timerExpiredPage";
	        }
	      } else {
	        ExceptionJSON exceptionJSON = new ExceptionJSON();
	        exceptionJSON = new Gson().fromJson(responseAsString, ExceptionJSON.class);
	        int statusCode = !GenericValidator.isBlankOrNull(exceptionJSON.getStatus()) ? Integer.parseInt(exceptionJSON.getStatus()) : 0;
	        response.sendError(statusCode, exceptionJSON.getException());
	      }
	    } catch (Exception securityException) {
	      String exceptionTraces = ExceptionUtils.getStackTrace(securityException);
	      //System.out.println("WhatuniGo exception ----->" + exceptionTraces);
	    } finally {
	      httpClient.close();
	    }
	return new ModelAndView(forwardPage);
   }
   
   private void dobMethod(HttpServletRequest request) {
	    LabelValueBean dobBean = null;
	    ArrayList dateList = new ArrayList();
	    for (int i = 1; i <= 31; i++) {
	      dobBean = new LabelValueBean();
	      if (i < 10) {
	        dobBean.setLabel("0" + String.valueOf(i));
	        dobBean.setValue("0" + String.valueOf(i));
	      } else {
	        dobBean.setLabel(String.valueOf(i));
	        dobBean.setValue(String.valueOf(i));
	      }
	      dateList.add(dobBean);
	    }
	    ArrayList monthList = new ArrayList();
	    String months[] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	    for (int i = 1; i <= 12; i++) {
	      dobBean = new LabelValueBean();
	      if (i < 10) {
	        dobBean.setLabel(months[i - 1]);
	        dobBean.setValue("0" + String.valueOf(i));
	      } else {
	        dobBean.setLabel(months[i - 1]);
	        dobBean.setValue(String.valueOf(i));
	      }
	      monthList.add(dobBean);
	    }
	    ArrayList yearList = new ArrayList();
	    Calendar ca = new GregorianCalendar();
	    int currentYear = (ca.get(Calendar.YEAR) - 13);
	    for (int i = currentYear; i >= 1900; i--) {
	      dobBean = new LabelValueBean();
	      dobBean.setLabel(String.valueOf(i));
	      dobBean.setValue(String.valueOf(i));
	      yearList.add(dobBean);
	    }
	    request.setAttribute("dateList", dateList);
	    request.setAttribute("monthList", monthList);
	    request.setAttribute("yearList", yearList);
	  }

	  private void setResponseText(HttpServletResponse response, StringBuffer responseMessage) {
	    try {
	      response.setContentType("TEXT/PLAIN");
	      Writer writer = response.getWriter();
	      writer.write(responseMessage.toString());
	    } catch (IOException exception) {
	      exception.printStackTrace();
	    }
	  }

	  public String getBody(HttpServletRequest request) {
	    String body = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    BufferedReader bufferedReader = null;
	    try {
	      InputStream inputStream = request.getInputStream();
	      if (inputStream != null) {
	        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	        char[] charBuffer = new char[128];
	        int bytesRead = -1;
	        while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	          stringBuilder.append(charBuffer, 0, bytesRead);
	        }
	      } else {
	        stringBuilder.append("");
	      }
	    } catch (IOException ex) {
	      ex.printStackTrace();
	      return null;
	    } finally {
	      if (bufferedReader != null) {
	        try {
	          bufferedReader.close();
	        } catch (IOException ex) {
	          ex.printStackTrace();
	          return null;
	        }
	      }
	    }
	    body = stringBuilder.toString();
	    return body;
	  }
	  public String getStatsLogData(HttpServletRequest request, HttpServletResponse response) throws Exception {
	    CloseableHttpClient httpClient = HttpClientBuilder.create().build();
	    HttpResponse responsePost;
	    String logId = "-1";
	    String responseAsString = null;
	    //To get wugo domain path
	    String httpPostURL = CommonUtil.getWugoDomain(request);    
	    //
	    httpPostURL += "/wugo-stats-logging/";
	    String mobileFlag = "";
	    CommonUtil commonUtil = new CommonUtil();
	    String userId = "0";
	    String action = request.getParameter("action");
	    String pageId  = request.getParameter("page-id");
	    String requestUrl = new CommonUtil().getRequestedURL(request);
	    String activity = GenericValidator.isBlankOrNull(request.getParameter("activity")) ? null : request.getParameter("activity");
	    String collegeId = GenericValidator.isBlankOrNull(request.getParameter("collegeId")) ? null : request.getParameter("collegeId");
	    String networkId = request.getParameter("networkId") != null && request.getParameter("networkId").trim().length() > 0 ? request.getParameter("networkId").trim() : "2";
	    String screenWidth = GenericValidator.isBlankOrNull(request.getParameter("screenwidth")) ? GlobalConstants.DEFAULT_SCREEN_WIDTH : request.getParameter("screenwidth");
	    String studyModeId = GenericValidator.isBlankOrNull(request.getParameter("studymodeid")) ? null : request.getParameter("studymodeid");
	    String applyNowCourseId = !GenericValidator.isBlankOrNull(request.getParameter("courseId")) ? request.getParameter("courseId") : null;
	    String applyNowOpportunityId = !GenericValidator.isBlankOrNull(request.getParameter("opportunityId")) ? request.getParameter("opportunityId") : null;
	    String jsLog = "Y";
	    if("0".equals(pageId)){
	      action = "OFFER_FORM";
	    }
	    if("provisional-offer-loading".equalsIgnoreCase(action)){
	      activity = GlobalConstants.STATS_WUGO_COMPLETE_APPLICATION;
	      jsLog = "N";
	    }else if("GENERATE_APPLICATION".equalsIgnoreCase(action)){
	      activity = GlobalConstants.STATS_WUGO_CONFIRM_OFFER;
	      jsLog = "N";
	    }else if("timer-1-expiry".equalsIgnoreCase(action)){
	      activity = GlobalConstants.STATS_WUGO_TIMER1_EXP;
	    }else if("timer-2-expiry".equalsIgnoreCase(action)){
	      activity = GlobalConstants.STATS_WUGO_TIMER2_EXP;
	    }else if("OFFER_FORM".equalsIgnoreCase(action)){
	      activity = GlobalConstants.STATS_WUGO_ACCEPT_OFFER;
	    }else if("INTL".equalsIgnoreCase(action)){
	      activity = GlobalConstants.STATS_WUGO_TIMER2_NOT_ELIGIBLE;
	    }else if("CANCEL_OFFER".equalsIgnoreCase(action)){
	      activity = GlobalConstants.STATS_WUGO_CANCEL_OFFER;
	    }else if("CANCAL_OFFER_LIGHTBOX".equalsIgnoreCase(action)){
	       activity = GlobalConstants.CLEARING_STATS_HOTLINE;
	    }
	    
	    if(new MobileUtils().mobileUserAgentCheck(request)){
	      mobileFlag = "Y";
	    }
	    if(request.getSession().getAttribute("cpeQualificationNetworkId")!=null && !"".equals(request.getSession().getAttribute("cpeQualificationNetworkId"))) {
	      networkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");  
	    } 
	    String widgetId = "";
	    /*if(session.getAttribute("widgetId") != null && session.getAttribute("widgetId").toString().trim().length() > 0){
	        widgetId = (String)session.getAttribute("widgetId");
	    }*/
	     String clientIp = request.getHeader("CLIENTIP");
	     if (clientIp == null || clientIp.length() == 0) {
	       clientIp = request.getRemoteAddr();
	     }
	    String cApplyNowUserId = new CookieManager().getCookieValue(request, GlobalConstants.AN_USER_ID_COOKIE);
	    if (!GenericValidator.isBlankOrNull(cApplyNowUserId) && !"0".equals(cApplyNowUserId)) {
	      userId = commonUtil.getDecrytedCookieUseId(cApplyNowUserId);
	      //System.out.println("whatuni Go user id after decrypt " + userId);
	    }
	    
	    StatsLoggingVO statsPayloadFromClient = new StatsLoggingVO();
	    statsPayloadFromClient.setAccessToken(GlobalConstants.WU_GO_ACCESS_TOKEN);
	    statsPayloadFromClient.setAffilateId(GlobalConstants.WHATUNI_AFFILATE_ID);
	    statsPayloadFromClient.setUserId(userId);
	    statsPayloadFromClient.setActivity(activity);
	    statsPayloadFromClient.setStudyMode(studyModeId);
	    statsPayloadFromClient.setCourseId(applyNowCourseId);
	    statsPayloadFromClient.setExtraText(applyNowOpportunityId);
	    statsPayloadFromClient.setScreenWidth(screenWidth);
	    statsPayloadFromClient.setMobileFlag(mobileFlag);
	    statsPayloadFromClient.setUserAgent(request.getHeader("user-agent"));
	    statsPayloadFromClient.setRefererUrl(request.getHeader("referer"));
	    statsPayloadFromClient.setRequestUrl(requestUrl);
	    statsPayloadFromClient.setJsLog(jsLog);
	    statsPayloadFromClient.setNetworkId(networkId);
	    statsPayloadFromClient.setIpUrl(clientIp);
	    statsPayloadFromClient.setCollegeId(collegeId);
	    
	    try {
	    Gson gson = new GsonBuilder().create();
	    String json = gson.toJson(statsPayloadFromClient, StatsLoggingVO.class);
	      //System.out.println("stats-111111111->" + json);
	      HttpPost requestPost = new HttpPost(httpPostURL);
	      StringEntity params = new StringEntity(json);
	      requestPost.addHeader("content-type", "application/json");
	      requestPost.setEntity(params);
	      responsePost = httpClient.execute(requestPost);
	      
	      String responseStatusCode = responsePost.getStatusLine().toString();
	      responseAsString = EntityUtils.toString(responsePost.getEntity());
	      //System.out.println("responseAsString--->"+responseAsString);
	      if ("HTTP/1.1 200 OK".equalsIgnoreCase(responseStatusCode)) {
	        StatsOutputBean statsOutPayLoad = new StatsOutputBean();
	        statsOutPayLoad = new Gson().fromJson(responseAsString, StatsOutputBean.class);
	        logId = statsOutPayLoad.getLog_id();

	        
	       //FWD_PAGE
	        
	     }else{
	        ExceptionJSON exceptionJSON = new ExceptionJSON();
	        exceptionJSON = new Gson().fromJson(responseAsString, ExceptionJSON.class);
	        int statusCode = !GenericValidator.isBlankOrNull(exceptionJSON.getStatus()) ? Integer.parseInt(exceptionJSON.getStatus()) : 0;
	        //logId = String.valueOf(statusCode);
	        response.sendError(statusCode, exceptionJSON.getException());
	      }
	    } catch (Exception securityException) {
	      String exceptionTraces = ExceptionUtils.getStackTrace(securityException);
	      System.out.println("WhatuniGo exception ----->" + exceptionTraces);
	    } finally {
	      httpClient.close();
	    }
	    return logId;
	  }

}
