package com.wuni.whatunigo;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.whatunigo.bean.GradeFilterBean;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : ViewCoursesCountAction.java
 * Description   : This class is used to get the course count.
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Sujitha V              1.0             01.08.2019      First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

@Controller
public class ViewCoursesCountController {
   @RequestMapping(value={"/get-subject-landing-courses-count"}, method = {RequestMethod.GET,RequestMethod.POST})
   public ModelAndView getCourseCout(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
	    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	    String courseName = !GenericValidator.isBlankOrNull(request.getParameter("subject")) ? request.getParameter("subject") : null;
	    String location   = !GenericValidator.isBlankOrNull(request.getParameter("location")) ? request.getParameter("location") : null;
	    String courseCount = "";
	    //System.out.println("location " + location);
	    GradeFilterBean gradeFilterBean = new GradeFilterBean();
	    gradeFilterBean.setSubjectName(courseName);
	    gradeFilterBean.setLocation(location);
	    gradeFilterBean.setKeyword((String)request.getParameter("q"));
	    Map resultMap = commonBusiness.getMatchingCourse(gradeFilterBean);
	    if(resultMap != null && resultMap.size() > 0){
	      courseCount = (String)resultMap.get("P_COURSE_COUNT");
	      //System.out.println("courseCount " + courseCount);
	    }
	    String returnString = "";
	    StringBuffer buffer = new StringBuffer();
	    response.setContentType("text/html");
	    response.setHeader("Cache-Control", "no-cache");
	    returnString = "COURSE_COUNT";
	    returnString += GlobalConstants.SPLIT_CONSTANT + courseCount;
	    buffer.append(returnString);
	    CommonUtil.setResponseText(response, buffer);
	    return null;
   }
		
}
