package com.wuni.whatunigo;

import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.config.SpringContextInjector;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUrls;
import com.wuni.valueobjects.chatbot.ExceptionJSON;
import com.wuni.valueobjects.whatunigo.ApplyNowVO;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.whatunigo.bean.OfferOutputBean;
import WUI.whatunigo.bean.WuGoBean;

/**
 * @see This class is used to get the course details of the apply now journey page
 * @author Sangeeth.S
 * @since  19.02.2019- intital draft
 * @version 1.0
 *
 * Modification history:
 * *****************************************************************************************************************
 * Author          Relase tag       Modification Details                                                  Rel Ver.
 * *****************************************************************************************************************  
 * Sangeeth.S       1.0             initial draft                                                           wu_586
 *
*/

@Controller
public class WhatuniGoRegistrationController {
   @RequestMapping(value={"/wugo/apply-now"}, method = {RequestMethod.GET,RequestMethod.POST})
   public ModelAndView getRegistrationDetails(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
	    //---(A)--- DECLARE local objects & variables        
	    WuGoBean wuGoBean = new WuGoBean();
	    SeoUrls seoUrls = new SeoUrls();
	    ApplyNowVO applyNowVO = new ApplyNowVO();
	    CommonUtil commonUtil = new CommonUtil();
	    CommonFunction commonFunc = new CommonFunction();
	    CookieManager cookieManager = new CookieManager();
	    String forwardString = "cmmtSignUp";
	    String pageType = request.getParameter("pagename");    
	    Map cmmtApplyNowMap = null;
	    String referralUrl = "";
	    ArrayList applyNowCourseDtlList = null;
	    String courseId = !GenericValidator.isBlankOrNull(request.getParameter("courseId")) ? request.getParameter("courseId") : "";
	    String opportunityId = !GenericValidator.isBlankOrNull(request.getParameter("opportunityId")) ? request.getParameter("opportunityId") : "";
	    if(GenericValidator.isBlankOrNull(courseId) || GenericValidator.isBlankOrNull(opportunityId)){
	      response.sendRedirect( seoUrls.getWhatuniHomeURL());
	    }
	    String userId = "0";
	    //getting the decrypted cookie user id for logged in user 
	    String cApplyNowUserId = cookieManager.getCookieValue(request, GlobalConstants.AN_USER_ID_COOKIE);
	    if(!GenericValidator.isBlankOrNull(cApplyNowUserId)) {
	      userId = commonUtil.getDecrytedCookieUseId(cApplyNowUserId);
	      request.setAttribute("cDimUID", userId);      
	    }
	    String clearingOnOff = commonFunc.getWUSysVarValue("CLEARING_ON_OFF");
	    String clearingFlag = "";
	    if (("ON").equals(clearingOnOff)) {
	      clearingFlag = "Y";
	      request.setAttribute("studyLevelDesc","Clearing");
	      commonUtil.setClearingUserType(request, response);//setting the user type to clearing
	    }
	    //    
	    //redirecting logged user directly to the offer journey 
	    if(!GenericValidator.isBlankOrNull(userId) && !"0".equals(userId)){      
	      String applyNowUserJourneyFlag = commonFunc.checkApplyNowUserJourney(request, response, session); 
	      
	      if("PROVISIONAL_FORM".equalsIgnoreCase(applyNowUserJourneyFlag)){
	         response.sendRedirect(seoUrls.provisionalJourneyURL(courseId, opportunityId, "provisional_form"));
	      }else if("OFFER_FORM".equalsIgnoreCase(applyNowUserJourneyFlag)){
	         response.sendRedirect(seoUrls.offerJourneyURL(courseId, opportunityId, "offer_form"));
	      }else if("SHOW_ERROR_MESSAGE".equalsIgnoreCase(applyNowUserJourneyFlag)){        
	        forwardString = "accepting-offer-page";
	      }else{
	        response.sendRedirect( seoUrls.getWhatuniHomeURL());
	      }
	    }
	    //   
	    //
	    //---(B) Business logic follow
	    //
	    try{
	      //
	       applyNowVO.setUserId(userId);
	       applyNowVO.setCourseId(courseId);
	       applyNowVO.setOpportunityId(opportunityId);
	      //DB call
	      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	      //
	      //Getting details from DB
	      cmmtApplyNowMap = commonBusiness.applyNowCourseDetails(applyNowVO);
	      //
	      //Setting Request atributes for using in JSP
	       ApplyNowVO coureInfovo = null;
	       if (!CommonUtil.isBlankOrNull(cmmtApplyNowMap)) {                  
	         applyNowCourseDtlList = (ArrayList)cmmtApplyNowMap.get("PC_APPLY_COURSE_DET");
	         if (!CommonUtil.isBlankOrNull(applyNowCourseDtlList)) {
	           coureInfovo = (ApplyNowVO)applyNowCourseDtlList.get(0);
	           String strCourseName = "";
	           strCourseName = new CommonFunction().getCourseName(coureInfovo.getCourseId(), clearingFlag);
	           strCourseName = strCourseName != null && strCourseName.trim().length() > 0 ? strCourseName.trim().replaceAll("\\/", "-") : "";
	           strCourseName = (strCourseName != null && strCourseName.trim().length() > 0 ? commonFunc.replaceHypen(commonFunc.replaceURL(commonFunc.replaceSpecialCharacter(strCourseName))) : "");
	           request.setAttribute("hitbox_course_title", strCourseName);
	           request.setAttribute("hitbox_college_name", coureInfovo.getCollegeName());
	           request.setAttribute("hitbox_college_id", coureInfovo.getCollegeId());
	         
	           request.setAttribute("APPLY_NOW_COURSE_LIST", applyNowCourseDtlList);            
	         }
	       }       
	      //
	       request.setAttribute("COLLEGE_ID", coureInfovo.getCollegeId());
	      request.setAttribute("COURSE_ID",courseId);
	      request.setAttribute("OPPORTUNITY_ID",opportunityId);
	      request.setAttribute("REFERER_URL", seoUrls.construnctCourseDetailsPageURL(GlobalConstants.DEGREE_STUDY_LEVEL, coureInfovo.getCourseTitle(), courseId, coureInfovo.getCollegeId(), GlobalConstants.CLEARING_PAGE_FLAG, opportunityId));
	      //
	    }catch(Exception ex){
	      ex.printStackTrace();
	    }
	    //
	    if("signin".equalsIgnoreCase(pageType)){
	      forwardString = "cmmtSignIn";
	    }    
	    request.setAttribute("HIDE_LOGIN", "Y");
	    request.setAttribute("ebookDim14", "WUGO");
	    //request.setAttribute("showBanner", "no");

	return new ModelAndView(forwardString);
   }
   public String getUrlRedirect(WuGoBean wuGoBean, HttpServletRequest request, HttpServletResponse response) throws Exception {
      CloseableHttpClient httpClient = HttpClientBuilder.create().build();
      String action = "check-user-journey";
      HttpResponse responsePost;
      String responseAsString = null;
      String journeyName = "PROVISIONAL_FORM";
      WuGoBean wuGoPayloadFromClient = new WuGoBean();
      //To get wugo domain path
      String httpPostURL = CommonUtil.getWugoDomain(request);    
      //
      httpPostURL += "/get-provisional-offer-details/";
      
      
      wuGoPayloadFromClient.setAction(action);
      wuGoPayloadFromClient.setAffiliateId(wuGoBean.getAffiliateId());
      wuGoPayloadFromClient.setCourseId(wuGoBean.getCourseId());
      wuGoPayloadFromClient.setOpportunityId(wuGoBean.getOpportunityId());
      wuGoPayloadFromClient.setAccessToken(wuGoBean.getAccessToken());
      wuGoPayloadFromClient.setUserId(wuGoBean.getUserId());
      
      //System.out.println("httpPostURL-->" + httpPostURL);
      try {
      Gson gson = new GsonBuilder().create();
      String json = gson.toJson(wuGoPayloadFromClient, WuGoBean.class);
        //System.out.println("cmmtPayloadFromClient-111111111->" + json);
        HttpPost requestPost = new HttpPost(httpPostURL);
        StringEntity params = new StringEntity(json);
        requestPost.addHeader("content-type", "application/json");
        requestPost.setEntity(params);
        responsePost = httpClient.execute(requestPost);
        
        String responseStatusCode = responsePost.getStatusLine().toString();
        responseAsString = EntityUtils.toString(responsePost.getEntity());
        if ("HTTP/1.1 200 OK".equalsIgnoreCase(responseStatusCode)) {
          OfferOutputBean offerOutPayLoad = new OfferOutputBean();
          offerOutPayLoad = new Gson().fromJson(responseAsString, OfferOutputBean.class);
          String provisionalFormFlag = offerOutPayLoad.getProvisional_form_flag();
          String offerFormFlag = offerOutPayLoad.getOffer_form_flag();
          String errorCode = offerOutPayLoad.getError_code();
          if("Y".equalsIgnoreCase(provisionalFormFlag)){
            journeyName = "PROVISIONAL_FORM";
          }
          if("Y".equalsIgnoreCase(offerFormFlag)){
            journeyName = "OFFER_FORM";
          }
          if(!GenericValidator.isBlankOrNull(errorCode)){
            journeyName = "ERROR_MESSAGE";
            request.setAttribute("ERROR_CODE", errorCode);
          }
          
         //FWD_PAGE
          
       }else{
          ExceptionJSON exceptionJSON = new ExceptionJSON();
          exceptionJSON = new Gson().fromJson(responseAsString, ExceptionJSON.class);
          int statusCode = !GenericValidator.isBlankOrNull(exceptionJSON.getStatus()) ? Integer.parseInt(exceptionJSON.getStatus()) : 0;
          response.sendError(statusCode, exceptionJSON.getException());
        }
      } catch (Exception securityException) {
        String exceptionTraces = ExceptionUtils.getStackTrace(securityException);
        //System.out.println("WhatuniGo exception ----->" + exceptionTraces);
      } finally {
        httpClient.close();
      }
      //request.setAttribute("showBanner", "no");

      return journeyName;
    }
}
