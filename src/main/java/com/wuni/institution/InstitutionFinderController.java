package com.wuni.institution;

import WUI.review.bean.UniBrowseBean;
import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import com.config.SpringContextInjector;
import com.layer.business.ISearchBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUtilities;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.util.LabelValueBean;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import spring.util.GlobalMethods;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : InstitutionFinderAction.java
 * Description   : This class used to get the UG & PG institutions list
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * Anbarasan.R              1.0               20.11.2012           First draft   		
 * Indumathi.S              1.1               01.09.2015           Page redesign
 * Prabhakaran V.           1.2               29.03.2016           Set location request for insights..
 * Sri Sankari R            1.3               10/11/2020           Changed default view of find uni page to list view from map view
 * Sri Sankari R            1.4               08/12/2020           Redirect old list view URL to /find-university.
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
@Controller
public class InstitutionFinderController {

  @RequestMapping(value = {"/find-university","/findUniversityInfoAjax"}, method = {RequestMethod.GET,RequestMethod.POST})
  public ModelAndView institutionFinder(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
    //
    ServletContext context = request.getServletContext();
    GlobalFunction globalFn = new GlobalFunction();
    CommonFunction common = new CommonFunction();
    String ajaxFlag = GenericValidator.isBlankOrNull(request.getParameter("ajaxFlag")) ? "N" : request.getParameter("ajaxFlag");
    if (new CommonFunction().checkSessionExpired(request, response, session, "ANONYMOUS")) {
      return new ModelAndView("forward: /userLogin.html");
    }
    try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
        String fromUrl = "/home.html";
        session.setAttribute("fromUrl", fromUrl);
        if ("Y".equalsIgnoreCase(ajaxFlag)) {
          return null;
        } else {
        	return new ModelAndView("loginPage");
        }
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    //
    new GlobalMethods().setCommonPodInformation(context, request, response);
    globalFn.removeSessionData(session, request, response);
    request.setAttribute("alphabetList", getAlphabetList());
    String forward = null;
    String navigation = StringUtils.isNotBlank(request.getParameter("view")) ? request.getParameter("view").toLowerCase() : "";
    String searchTxt = request.getParameter(SpringConstants.SEARCH_TXT);
    String regionName = request.getParameter(SpringConstants.REGION);
	/* Start of old list view redirection */
    if(StringUtils.equals(navigation, "az")) {
    	response.setStatus(response.SC_MOVED_PERMANENTLY);
    	response.setHeader("Location", GlobalConstants.FIND_A_UNI_URL);
    }
    /* End of old list view redirection */
    //For navigation
    if (!GenericValidator.isBlankOrNull(searchTxt) || ((!GenericValidator.isBlankOrNull(regionName)) && (!GenericValidator.isBlankOrNull(request.getParameter("flag"))))) {
      forward = "findUniversityInfoLandingAjax";
      getUniBrowseList(request);
    } else if(StringUtils.equals(navigation, "m")) {
      String breadCrumb = new SeoUtilities().getBreadCrumb("FIND_A_UNI_PAGE", "", "", "", "", "");
      if (!GenericValidator.isBlankOrNull(breadCrumb)) {
        request.setAttribute("breadCrumb", breadCrumb);
      }
      forward = "showUniBrowser";
    } else {
      forward = "showUniFinderAZ";
      getUniBrowseList(request);
    }
    String cpeQualNetworkId = (String)request.getSession().getAttribute(SpringConstants.CPE_QUAL_NEWTWORK_ID);
    //Insight start
    request.setAttribute("insightPageFlag", "yes");
    if (GenericValidator.isBlankOrNull(cpeQualNetworkId) || GlobalConstants.UG_NETWORK_ID.equals(cpeQualNetworkId)) {
      globalFn.getStudyLevelDesc("M", request);
    } else {
      globalFn.getStudyLevelDesc("L", request);
    }
    if(!GenericValidator.isBlankOrNull(regionName)){ //Added for insight dimension 12 and 13 by Prabha on 29_Mar_2016
      request.setAttribute("location", globalFn.getReplacedString(common.replaceHypenWithSpace(regionName).toUpperCase()));
    }
    //Insight end
    request.setAttribute("curActiveMenu", "finduni");
    return new ModelAndView(forward);
  }
  //

  /**
   * This function is used to get the unibrowse list.
   * @return Unibrowse list as Collection object.
   */
  public void getUniBrowseList(HttpServletRequest request) {
    // 
    String networkId = request.getParameter("nid");
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute(SpringConstants.CPE_QUAL_NEWTWORK_ID);
    if (GenericValidator.isBlankOrNull(cpeQualificationNetworkId)) {
      cpeQualificationNetworkId = GlobalConstants.UG_NETWORK_ID;
    }
    networkId = GenericValidator.isBlankOrNull(networkId) ? cpeQualificationNetworkId : networkId;
    String regionName = request.getParameter(SpringConstants.REGION);
    regionName = GenericValidator.isBlankOrNull(regionName) ? null : regionName;
    //
    String searchText = request.getParameter(SpringConstants.SEARCH_TXT);
    searchText = GenericValidator.isBlankOrNull(searchText) ? null : searchText;
    searchText = (GenericValidator.isBlankOrNull(searchText) && GenericValidator.isBlankOrNull(regionName)) ? "A" : searchText;
    //
    String pageNo = request.getParameter("pageNo");
    pageNo = GenericValidator.isBlankOrNull(pageNo) ? "1" : pageNo;
    //
    UniBrowseBean uniBrowse = new UniBrowseBean();
    //
    uniBrowse.setNetworkId(networkId);
    uniBrowse.setRegionName(regionName);
    uniBrowse.setSearchText(searchText);
    uniBrowse.setPageNo(pageNo);
    ISearchBusiness searchBusiness = (ISearchBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_SEARCH_BUSINESS);
    Map resultMap = searchBusiness.getUniversityBrowseList(uniBrowse);
    if (resultMap != null) {
      ArrayList uniBrowseList = (ArrayList)resultMap.get("pc_college_list");
      if (!CollectionUtils.isEmpty(uniBrowseList)) {
        request.setAttribute("uniBroswerList", uniBrowseList);
        UniBrowseBean uniBean = (UniBrowseBean)uniBrowseList.get(0);
        request.setAttribute("totalRecordCount", uniBean.getTotalRecordCount());
      }
    }
    request.setAttribute(SpringConstants.SEARCH_TXT, uniBrowse.getSearchText());
    String breadCrumb = (String)resultMap.get("p_breadcrumb");
    if (!GenericValidator.isBlankOrNull(breadCrumb)) {
      request.setAttribute("breadCrumb", breadCrumb);
    }
    request.setAttribute(SpringConstants.REGION, uniBrowse.getRegionName());
    if (!GenericValidator.isBlankOrNull(regionName)) {
      String displayRegionName = new CommonUtil().toTitleCase(regionName.replaceAll("-", " "));
      request.setAttribute("displayRegionName", displayRegionName);
    }
    request.setAttribute("networkId", uniBrowse.getNetworkId());
    request.setAttribute("pageNo", uniBrowse.getPageNo());
    request.getSession().setAttribute(SpringConstants.CPE_QUAL_NEWTWORK_ID, uniBrowse.getNetworkId());
    
  }
  //

  /**
   * Method to get the alphabet list to display in the alphabet menu
   * @return ArrayList
   */
  public ArrayList getAlphabetList() {
    String[] alphabetArr = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };
    LabelValueBean labelBean = null;
    ArrayList alphabetList = new ArrayList();
    for (int i = 0; i < alphabetArr.length; i++) {
      labelBean = new LabelValueBean();
      labelBean.setLabel(alphabetArr[i]);
      labelBean.setValue(alphabetArr[i].toUpperCase());
      alphabetList.add(labelBean);
    }
    return alphabetList;
  }

}
