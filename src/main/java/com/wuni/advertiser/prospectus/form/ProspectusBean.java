package com.wuni.advertiser.prospectus.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class ProspectusBean {

  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String courseId = "";
  private String courseName = "";
  private String subjectId = "";
  private String subjectName = "";
  private String studyLevel = "M";
  private String selectedCollege[] = null;
  private String selectedSubject[] = null;
  private String firstName = "";
  private String lastName = "";
  private String address1 = "";
  private String address2 = "";
  private String town = "";
  private String postCode = "";
  boolean collegeChecked = false;
  boolean subjectChecked = false;
  boolean courseChecked = false;
  private String shortListFlag = "";
  private String emailAddress = null;
  boolean nonukresident = false;
  private String yearofPassing = null;
  private String aboutMe = null;
  private String gender = null;
  private String optionValue = null;
  private String optionDescription = null;
  private String nationality = null;
  private String groupAboutMe[] = { "", "" };
  private boolean termsAndCondition = false;
  private String openDays = "";
  private List yearOfUniEntryList = null;
  private List yearOfUniFutureStudList = null;
  private String yearOfUniEntry = null;
  private String yearOfUniFutureStud = null;
  private String websitePaidFlag = "";
  private String courseDeleted = "";
  private String seoStudyLevelText = "";
  private String pageFrom = "";
  private String headerId = "";
  private boolean checkUncheckAll = true;
  private String nationalityValue = "";
  private String nationalityDesc = "";
  //private String age = "";
  private String dateOfBirth = "";
  private String subject = "";
  private String prospectusPurchaseFlag = "";
  private String customUrl = "";
  private boolean receiveInfo = false;
  private String mobileNumber = "";
  private boolean newsLetter = true;
  private String orderItemId = "";
  private String subOrderItemId = "";
  private String myhcProfileId = "";
  private String profileType = "";
  private String subOrderWebsite = "";
  private String subOrderProspectus = "";
  private String subOrderProspectusWebform = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private String cpeQualificationNetworkId = "";
  private String dpRedirectionURL = "";
  private String getProspectusFlag = "";
  private List studyLevelList = null;
  private String studyLevelId = "";

  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public String getSubjectId() {
    return subjectId;
  }

  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }

  public String getSubjectName() {
    return subjectName;
  }

  public void setStudyLevel(String studyLevel) {
    this.studyLevel = studyLevel;
  }

  public String getStudyLevel() {
    return studyLevel;
  }

  public void setSelectedCollege(String[] selectedCollege) {
    this.selectedCollege = selectedCollege;
  }

  public String[] getSelectedCollege() {
    return selectedCollege;
  }

  public void setSelectedSubject(String[] selectedSubject) {
    this.selectedSubject = selectedSubject;
  }

  public String[] getSelectedSubject() {
    return selectedSubject;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getAddress2() {
    return address2;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getTown() {
    return town;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setCollegeChecked(boolean collegeChecked) {
    this.collegeChecked = collegeChecked;
  }

  public boolean isCollegeChecked() {
    return collegeChecked;
  }

  public void setSubjectChecked(boolean subjectChecked) {
    this.subjectChecked = subjectChecked;
  }

  public boolean isSubjectChecked() {
    return subjectChecked;
  }

  public void setShortListFlag(String shortListFlag) {
    this.shortListFlag = shortListFlag;
  }

  public String getShortListFlag() {
    return shortListFlag;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setNonukresident(boolean nonukresident) {
    this.nonukresident = nonukresident;
  }

  public boolean isNonukresident() {
    return nonukresident;
  }

  public void setYearofPassing(String yearofPassing) {
    this.yearofPassing = yearofPassing;
  }

  public String getYearofPassing() {
    return yearofPassing;
  }

  public void setAboutMe(String aboutMe) {
    this.aboutMe = aboutMe;
  }

  public String getAboutMe() {
    return aboutMe;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return gender;
  }

  public void setOptionValue(String optionValue) {
    this.optionValue = optionValue;
  }

  public String getOptionValue() {
    return optionValue;
  }

  public void setOptionDescription(String optionDescription) {
    this.optionDescription = optionDescription;
  }

  public String getOptionDescription() {
    return optionDescription;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getNationality() {
    return nationality;
  }

  public void setGroupAboutMe(String[] groupAboutMe) {
    this.groupAboutMe = groupAboutMe;
  }

  public String[] getGroupAboutMe() {
    return groupAboutMe;
  }

  public void setTermsAndCondition(boolean termsAndCondition) {
    this.termsAndCondition = termsAndCondition;
  }

  public boolean isTermsAndCondition() {
    return termsAndCondition;
  }

  public void setOpenDays(String openDays) {
    this.openDays = openDays;
  }

  public String getOpenDays() {
    return openDays;
  }

  public void setYearOfUniEntryList(List yearOfUniEntryList) {
    this.yearOfUniEntryList = yearOfUniEntryList;
  }

  public List getYearOfUniEntryList() {
    return yearOfUniEntryList;
  }

  public void setYearOfUniFutureStudList(List yearOfUniFutureStudList) {
    this.yearOfUniFutureStudList = yearOfUniFutureStudList;
  }

  public List getYearOfUniFutureStudList() {
    return yearOfUniFutureStudList;
  }

  public void setYearOfUniEntry(String yearOfUniEntry) {
    this.yearOfUniEntry = yearOfUniEntry;
  }

  public String getYearOfUniEntry() {
    return yearOfUniEntry;
  }

  public void setYearOfUniFutureStud(String yearOfUniFutureStud) {
    this.yearOfUniFutureStud = yearOfUniFutureStud;
  }

  public String getYearOfUniFutureStud() {
    return yearOfUniFutureStud;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }

  public String getCourseName() {
    return courseName;
  }

  public void setWebsitePaidFlag(String websitePaidFlag) {
    this.websitePaidFlag = websitePaidFlag;
  }

  public String getWebsitePaidFlag() {
    return websitePaidFlag;
  }

  public void setCourseDeleted(String courseDeleted) {
    this.courseDeleted = courseDeleted;
  }

  public String getCourseDeleted() {
    return courseDeleted;
  }

  public void setSeoStudyLevelText(String seoStudyLevelText) {
    this.seoStudyLevelText = seoStudyLevelText;
  }

  public String getSeoStudyLevelText() {
    return seoStudyLevelText;
  }

  public void setCourseChecked(boolean courseChecked) {
    this.courseChecked = courseChecked;
  }

  public boolean isCourseChecked() {
    return courseChecked;
  }

  public void setPageFrom(String pageFrom) {
    this.pageFrom = pageFrom;
  }

  public String getPageFrom() {
    return pageFrom;
  }

  public void setHeaderId(String headerId) {
    this.headerId = headerId;
  }

  public String getHeaderId() {
    return headerId;
  }

  public void setCheckUncheckAll(boolean checkUncheckAll) {
    this.checkUncheckAll = checkUncheckAll;
  }

  public boolean isCheckUncheckAll() {
    return checkUncheckAll;
  }

  public void setNationalityValue(String nationalityValue) {
    this.nationalityValue = nationalityValue;
  }

  public String getNationalityValue() {
    return nationalityValue;
  }

  public void setNationalityDesc(String nationalityDesc) {
    this.nationalityDesc = nationalityDesc;
  }

  public String getNationalityDesc() {
    return nationalityDesc;
  }
  /* public void setAge(String age) {
    this.age = age;
  }
  public String getAge() {
    return age;
  }*/

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getSubject() {
    return subject;
  }

  public void setProspectusPurchaseFlag(String prospectusPurchaseFlag) {
    this.prospectusPurchaseFlag = prospectusPurchaseFlag;
  }

  public String getProspectusPurchaseFlag() {
    return prospectusPurchaseFlag;
  }

  public void setCustomUrl(String customUrl) {
    this.customUrl = customUrl;
  }

  public String getCustomUrl() {
    return customUrl;
  }

  public void setReceiveInfo(boolean receiveInfo) {
    this.receiveInfo = receiveInfo;
  }

  public boolean isReceiveInfo() {
    return receiveInfo;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public void setNewsLetter(boolean newsLetter) {
    this.newsLetter = newsLetter;
  }

  public boolean isNewsLetter() {
    return newsLetter;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setOrderItemId(String orderItemId) {
    this.orderItemId = orderItemId;
  }

  public String getOrderItemId() {
    return orderItemId;
  }

  public void setMyhcProfileId(String myhcProfileId) {
    this.myhcProfileId = myhcProfileId;
  }

  public String getMyhcProfileId() {
    return myhcProfileId;
  }

  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }

  public String getProfileType() {
    return profileType;
  }

  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

    public void setDpRedirectionURL(String dpRedirectionURL) {
        this.dpRedirectionURL = dpRedirectionURL;
    }

    public String getDpRedirectionURL() {
        return dpRedirectionURL;
    }

    public void setGetProspectusFlag(String getProspectusFlag) {
        this.getProspectusFlag = getProspectusFlag;
    }

    public String getGetProspectusFlag() {
        return getProspectusFlag;
    }


  public void setStudyLevelId(String studyLevelId) {
    this.studyLevelId = studyLevelId;
  }

  public String getStudyLevelId() {
    return studyLevelId;
  }

  public void setStudyLevelList(List studyLevelList) {
    this.studyLevelList = studyLevelList;
  }

  public List getStudyLevelList() {
    return studyLevelList;
  }

}
/////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////    
