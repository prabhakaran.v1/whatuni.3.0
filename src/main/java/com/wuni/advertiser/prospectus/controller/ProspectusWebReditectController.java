package com.wuni.advertiser.prospectus.controller;

import com.wuni.util.sql.DataModel;

import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

import java.util.Vector;

import javax.servlet.http.*;

import org.apache.struts.action.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
   * @ProspectusWebReditectAction.java
   * @Version : 1.0
   * @April 16 2009
   * @www.whatuni.com
   * @Created By : Balraj Selvakumar.M
   * @Purpose  : This program used to redirect the college wwebsite when prospectus purchace flag is true.
   * *************************************************************************************************************************
   * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
   * *************************************************************************************************************************
   */


@Controller
public class ProspectusWebReditectController{

  @RequestMapping(value="/prospectuswebredirect", method=RequestMethod.POST)	
  public String prospectusWebRedirect(HttpServletRequest request, HttpServletResponse response) throws Exception {
    String collegeId = request.getParameter("z") != null ? request.getParameter("z") : "";
    CommonFunction common = new CommonFunction();
    String collegeName = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(common.getCollegeName(collegeId, request))));
    response.getWriter().write(collegeName);
    //prospectusURLRedirection(collegeId, response,request);
    return null;
  }

  /**
    *   This function is used to redirect to the external website for specific providers.
    * @param collegeId
    * @param response
    * @param request
    */
  private void prospectusURLRedirection(String collegeId, HttpServletResponse response, HttpServletRequest request) {
    try {
      Vector vector = new Vector();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(collegeId);
      String webRedirect = new DataModel().getString("wu_email_pkg.get_backoff_web_email_fn", vector);
      response.setContentType("text/html");
      response.setHeader("Cache-Control", "no-cache");
      if (webRedirect != null && !webRedirect.equalsIgnoreCase("EMAIL")) {
        String respString = "webLink>>" + webRedirect;
        response.getWriter().write(respString);
      } else {
        CommonFunction common = new CommonFunction();
        String collegeName = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(new CommonFunction().getCollegeName(collegeId, request))));
        response.getWriter().write(collegeName);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
  }

}
