package com.wuni.advertiser.prospectus.controller;

import WUI.utilities.SessionData;

import com.wuni.util.sql.DataModel;

import java.io.IOException;
import java.io.Writer;
import java.sql.ResultSet;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
  * @Version :  1.0
  * @www.whatuni.com
  * @Created By : Priyaa Parthasarathy
  * @Purpose  : This program is used to ger if the BasicFormDetails flag
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */

@Controller
public class BasicFormDetailsExistsController {
  @RequestMapping(value="/basicformdetailsexists", method=RequestMethod.POST)	
  public ModelAndView basicFormDetailsExists(HttpServletRequest request, HttpServletResponse response) throws Exception {
    String logUserId = new SessionData().getData(request, "y");
    HttpSession session = request.getSession();
    String flag = basicFormDetailsExists(logUserId);
    String resText = flag;
    if(flag.equals("Y")){
      //saveToken(request);
      String token = ""+session.getAttribute("_synchronizerToken"); 
      resText = resText + "##SPLIT##" + token;
    }  
    setResponseText(response, resText);
   return null;
}

  public String basicFormDetailsExists(String logUserId){
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    String flag = null;
    try {
      Vector vector = new Vector();
      vector.add(logUserId);
      vector.add("0");
      vector.add("WU_COLLEGE_BULKPROSPECTUS_EMAIL");
      vector.add("");
      flag = dataModel.getString("whatuni_interactive_pkg.get_one_click_flag_fn", vector);
     
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return flag; 
  }
  
  private void setResponseText(HttpServletResponse response, String responseMessage) {
     try {
       response.setContentType("TEXT/PLAIN");
       Writer writer = response.getWriter();
       writer.write(responseMessage);
     } catch (IOException exception) {
       exception.printStackTrace();
     }
   }
}


