package com.wuni.advertiser.prospectus.controller;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.utilities.CommonFunction;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.advert.ProspectusVO;

/**
  * @Version :  1.0
  * @www.whatuni.com
  * @Created By : Priyaa Parthasarathy
  * @Purpose  : This program is used to display college prospectus to user basket using AJAX.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
@Controller
public class ProspectusPopupAjaxController{
	
	@RequestMapping(value = "/openProspectusPopUp", method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView getProspectusPopupAjax(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    String logUserId = new SessionData().getData(request, "y");
    logUserId = GenericValidator.isBlankOrNull(logUserId) || "0".equals(logUserId)? null: logUserId;
    CommonFunction commonFn = new CommonFunction();
    String cookie_pros_basket_id = commonFn.checkProspectusCookieStatus(request);
    getProspectusPopupContent(logUserId, cookie_pros_basket_id, request, response);
    return new ModelAndView("popupProspectusOrder");
  }
  public void getProspectusPopupContent(String logUserId, String cookie_pros_basket_id, HttpServletRequest request, HttpServletResponse response) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    try {
      Vector vector = new Vector();
      vector.add(logUserId);
      vector.add(cookie_pros_basket_id);
      vector.add("RP");
      resultset = dataModel.getResultSet("wuni_enquiry_baskets_pkg.get_enq_basket_list_fn", vector);
      ArrayList list = new ArrayList();
      while (resultset.next()) {
        ProspectusVO prospectusVo = new ProspectusVO();
        prospectusVo.setBasketId(resultset.getString("enq_basket_id"));
        prospectusVo.setCollegeId(resultset.getString("college_id"));
        prospectusVo.setCollegeName(resultset.getString("college_name"));
        prospectusVo.setCollegeNameDisplay(resultset.getString("college_name_display"));
        prospectusVo.setSubOrderItemId(resultset.getString("suborder_item_id"));
        prospectusVo.setAssocTypeCode(resultset.getString("assoc_type_code"));
        prospectusVo.setStatus(resultset.getString("status"));
        prospectusVo.setTotalCount(resultset.getString("total_count"));                
        list.add(prospectusVo);
      }
      if (list != null && list.size() > 0) {
        request.setAttribute("porspectusList", list);
        request.setAttribute("porspectusListSize", String.valueOf(list.size()));
        ProspectusVO prVO = (ProspectusVO)list.get(0);
        String prospectusBasketId = prVO.getBasketId();
        String totalCount = prVO.getTotalCount();
        if (prospectusBasketId != null && !prospectusBasketId.equals("")) {
          Cookie cookie = CookieManager.createCookie(request,"prospectusBasketId", prospectusBasketId);
          cookie.setMaxAge(GlobalConstants.PROS_COOKIE_MAX_AGE);
          response.addCookie(cookie);
          request.setAttribute("prosBasketId", prospectusBasketId);
          request.setAttribute("prospectusBasketCount", totalCount);
        }
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
  }
}
