package com.wuni.advertiser.prospectus.controller;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import com.layer.business.ICommonBusiness;
import com.wuni.util.seo.SeoUtilities;
import com.wuni.util.valueobject.advert.ProspectusVO;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
  * @BulkProspectusAction.java
  * @Version : 1.0
  * @www.whatuni.com
  * @Created By : Priyaa Parthasarathy
  * @Purpose  : This program used to get the free prospectus for loaction based course provider.
  * ***************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                   Rel Ver.
  * ***************************************************************************************************************************
  * 19_05_2015     Thiyagu G                 1.1      Added 'oc_user_prospectus' cursor for user my porspectus pod    wu541
  * 19.05.2015     Yogeswari                          Changed for adding sort type when pagination.
  */
@Controller
public class BulkProspectusController{

  public BulkProspectusController() {
  }
  
	@Autowired
	  private ICommonBusiness commonBusiness;
	
	@RequestMapping(value = {"/prospectus", "/prospectus/university-prospectus-college-prospectus/order-prospectus"}, method = {RequestMethod.GET,RequestMethod.POST})
	public ModelAndView getBulkProspectus(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
        String fromUrl = "/degrees/home.html";
        session.setAttribute("fromUrl", fromUrl);
        return new ModelAndView("loginPage");
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    CommonFunction common = new CommonFunction();
    request.setAttribute("queryStr", request.getQueryString());
    //
    request.setAttribute("currentPageUrl", (common.getSchemeName(request) + "www.whatuni.com/degrees" + request.getRequestURI().substring(request.getContextPath().length())));//Added getSchemeName by Indumathi Mar-29-16
    String urlString = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
    CommonUtil commonutil = new CommonUtil();
    request.setAttribute("urlString", urlString);
    new CommonFunction().loadUserLoggedInformation(request, request.getServletContext(), response);
    String loginUrl = "/prospectus/university-prospectus-college-prospectus/order-prospectus.html";
    request.setAttribute("loginUrl", loginUrl);
    createQueryString(request);
    createQueryStringWOSubject(request);
    //
    String studyLevel = request.getParameter("level");
    studyLevel = GenericValidator.isBlankOrNull(studyLevel) ? "M" : studyLevel;
    String collegeid = request.getParameter("collegeid");
    collegeid = GenericValidator.isBlankOrNull(collegeid) ? "" : collegeid;
    String catCode = request.getParameter("subject");
    catCode = GenericValidator.isBlankOrNull(catCode) ? "" : catCode;
    String type = request.getParameter("type");
    type = GenericValidator.isBlankOrNull(type) ? "" : type;
    String sort = request.getParameter("sort");
    /* ::START:: Yogeswari :: 19.05.2015 :: for adding sort type when pagination. */
    if (sort != null && sort.trim().length() > 0) {
      request.setAttribute("SORT", sort);
    }
    /* ::END:: Yogeswari :: 19.05.2015 :: for adding sort type when pagination. */
    sort = GenericValidator.isBlankOrNull(sort) ? "" : sort.toUpperCase();
    String region = request.getParameter("region");
    region = GenericValidator.isBlankOrNull(region) ? "" : region;
    String basketID = GenericValidator.isBlankOrNull(common.checkCookieStatus(request)) ? null : common.checkCookieStatus(request);
    String enqBasketID = GenericValidator.isBlankOrNull(common.checkProspectusCookieStatus(request)) ? null : common.checkProspectusCookieStatus(request);
    String userId = new SessionData().getData(request, "y");
    userId = userId != null && !userId.equals("0") && userId.trim().length() >= 0 ? userId : null;
    String mypage = request.getParameter("mypage");
    mypage = GenericValidator.isBlankOrNull(mypage) ? "" : mypage;
    String mypageno = request.getParameter("pageno");
    mypageno = GenericValidator.isBlankOrNull(mypageno) ? "1" : mypageno;
    String mappingFindForward = "";
    if (mypage != null && !"".equals(mypage)) {
      mappingFindForward = "my-prospectus-location";
      if (userId == null) {
        response.sendRedirect("/degrees/mywhatuni.html");
        return null;
      }
      request.setAttribute("pageno", mypageno);
    } else {
      mappingFindForward = "prospectus-location";
    }
    request.setAttribute("studyLevel", studyLevel);
    ProspectusVO prospectusVO = new ProspectusVO();
    prospectusVO.setQualification(studyLevel.toUpperCase());
    prospectusVO.setCollegeId(collegeid);
    prospectusVO.setCategoryCode(catCode);
    prospectusVO.setSortBy(sort);
    prospectusVO.setType(type);
    prospectusVO.setEnqBasketId(enqBasketID);
    prospectusVO.setBasketId(basketID);
    prospectusVO.setUserId(userId);
    prospectusVO.setRegionName(region);
    prospectusVO.setPageName(mypage);
    prospectusVO.setPageNo(mypageno);
    request.setAttribute("studyLevel", studyLevel.toUpperCase());
    //Get the college display name by using 'getCollegeNameDisplay' to show the searched college name.
    request.setAttribute("collegeDispName", new CommonFunction().getCollegeNameDisplay(collegeid));        
    Map prospectusResultsMap = commonBusiness.getProspectusesResults(prospectusVO);
    //
    if ((prospectusResultsMap == null) || (prospectusResultsMap != null && prospectusResultsMap.size() == 0)) {
      String institutionId = (String)request.getAttribute("institutionId");
      String blockedFlag = commonutil.getBlockedFlag(institutionId);      
      if (!GenericValidator.isBlankOrNull(blockedFlag) && ("Y").equals(blockedFlag)) {
        request.setAttribute("reason_for_404", "No Prospectus found");
        response.sendError(404, "404 error message");
        return null;
      }
      return new ModelAndView("noresultspage");
    }
    if (prospectusResultsMap != null && prospectusResultsMap.size() > 0) {
      request.setAttribute("prospectusResultsMap", prospectusResultsMap);
      ArrayList propsectusSrList = (ArrayList)prospectusResultsMap.get("oc_prospectus_sr_list");
      if (propsectusSrList != null && propsectusSrList.size() > 0) {
        request.setAttribute("propsectusSrList", propsectusSrList);
        ProspectusVO prospectussVO = (ProspectusVO)propsectusSrList.get(0);
        request.setAttribute("prospectusSearchType", (prospectussVO.getDisplayName()));
        request.setAttribute("searchTypeCount", (prospectussVO.getTotalCount()));
      } else {
        if (!GenericValidator.isBlankOrNull(collegeid) || !GenericValidator.isBlankOrNull(catCode)) {
          request.setAttribute("prosSearchNoResults", "prosSearchNoResults");
        }
      }
      ArrayList subjectRankingList = (ArrayList)prospectusResultsMap.get("oc_subject_ranking_pros_list");
      if (subjectRankingList != null && subjectRankingList.size() > 0) {
        request.setAttribute("subjectRankingList", subjectRankingList);
      }
      int prosBasketCount = 0;
      ArrayList enqBasketCount = (ArrayList)prospectusResultsMap.get("oc_enq_basket_count");
      if (enqBasketCount != null && enqBasketCount.size() > 0) {
        ProspectusVO prospectusenqVO = (ProspectusVO)enqBasketCount.get(0);
        Cookie cookie = CookieManager.createCookie(request,"prospectusBasketId", prospectusenqVO.getEnqBasketId());
        cookie.setMaxAge(GlobalConstants.PROS_COOKIE_MAX_AGE);
        response.addCookie(cookie);
        if (!GenericValidator.isBlankOrNull(prospectusenqVO.getTotalCount())) {
          try {
            prosBasketCount = Integer.parseInt(prospectusenqVO.getTotalCount());
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
      request.setAttribute("prospectusBasketCount", String.valueOf(prosBasketCount));
      ArrayList recommendUniList = (ArrayList)prospectusResultsMap.get("oc_recommend_uni_pros_list");
      if (recommendUniList != null && recommendUniList.size() > 0) {
        request.setAttribute("recommendUniList", recommendUniList);
      }
      ArrayList otherStudentsProsList = (ArrayList)prospectusResultsMap.get("oc_other_students_pros_list");
      if (otherStudentsProsList != null && otherStudentsProsList.size() > 0) {
        request.setAttribute("otherStudentsProsList", otherStudentsProsList);
      }
      ArrayList uniNearProsList = (ArrayList)prospectusResultsMap.get("oc_uni_near_pros_list");
      if (uniNearProsList != null && uniNearProsList.size() > 0) {
        request.setAttribute("uniNearProsList", uniNearProsList);
      }
      ArrayList timesRankingList = (ArrayList)prospectusResultsMap.get("oc_times_ranking_pros_list");
      if (timesRankingList != null && timesRankingList.size() > 0) {
        request.setAttribute("timesRankingList", timesRankingList);
      }
      ArrayList studentAwardsList = (ArrayList)prospectusResultsMap.get("oc_student_awards_pros_list");
      if (studentAwardsList != null && studentAwardsList.size() > 0) {
        request.setAttribute("studentAwardsList", studentAwardsList);
      }
      ArrayList popularRegionList = (ArrayList)prospectusResultsMap.get("oc_popular_regions_pros_list");
      if (popularRegionList != null && popularRegionList.size() > 0) {
        request.setAttribute("popularRegionList", popularRegionList);
      }
      ArrayList campusUniProsList = (ArrayList)prospectusResultsMap.get("oc_campus_uni_pros_list");
      if (campusUniProsList != null && campusUniProsList.size() > 0) {
        request.setAttribute("campusUniProsList", campusUniProsList);
      }
      ArrayList nonCampusUniProsList = (ArrayList)prospectusResultsMap.get("oc_non_campus_uni_pros_list");
      if (nonCampusUniProsList != null && nonCampusUniProsList.size() > 0) {
        request.setAttribute("nonCampusUniProsList", nonCampusUniProsList);
      }
      ArrayList cityProsList = (ArrayList)prospectusResultsMap.get("oc_city_pros_list");
      if (cityProsList != null && cityProsList.size() > 0) {
        request.setAttribute("cityProsList", cityProsList);
      }
      ArrayList countrySideProsList = (ArrayList)prospectusResultsMap.get("oc_countryside_pros_list");
      if (countrySideProsList != null && countrySideProsList.size() > 0) {
        request.setAttribute("countrySideProsList", countrySideProsList);
      }
      ArrayList seaSideProsList = (ArrayList)prospectusResultsMap.get("oc_seaside_pros_list");
      if (seaSideProsList != null && seaSideProsList.size() > 0) {
        request.setAttribute("seaSideProsList", seaSideProsList);
      }
      ArrayList townProsList = (ArrayList)prospectusResultsMap.get("oc_town_pros_list");
      if (townProsList != null && townProsList.size() > 0) {
        request.setAttribute("townProsList", townProsList);
      }
      //Added 'oc_user_prospectus' cursor for user my porspectus, 19_May_2015 By Thiyagu G.      
        ArrayList userProsList = (ArrayList)prospectusResultsMap.get("oc_user_prospectus");
        if (userProsList != null && userProsList.size() > 0) {
          request.setAttribute("userProsList", userProsList);
          ProspectusVO prVO = (ProspectusVO)userProsList.get(0);
          request.setAttribute("allProsCount", prVO.getTotalCount());
          request.setAttribute("userNameDetail", prVO.getDetailUserName());
        }      
    }
    String networkId1 = (String)request.getAttribute("netWorkId"); //16-Apr-2014
    String netWorkId2 = "";
    if ("2".equals(networkId1) || (studyLevel != null && "M".equalsIgnoreCase(studyLevel))) { //9_DEC_14 Modified by Amir by adding Study Level restriction as well 
      netWorkId2 = "2";
    } else { //L or postgraduate-courses
      netWorkId2 = "3";
    }
    //Added insights flag and qualifications for 08_Mar_2016, By Thiyagu G.
    //Insights
    if(!GenericValidator.isBlankOrNull(studyLevel)){
      new GlobalFunction().getStudyLevelDesc(studyLevel, request); 
    }
    request.setAttribute("insightPageFlag", "yes");
    
    session.removeAttribute("cpeQualificationNetworkId");
    session.setAttribute("cpeQualificationNetworkId", netWorkId2);
    request.setAttribute("curActiveMenu", "prospectus");
    String breadCrumb = new SeoUtilities().getBreadCrumb("PROSPECTUS_SEARCH_PAGES", "", "", "", "", collegeid);
    if (breadCrumb != null && breadCrumb.trim().length() > 0) {
      request.setAttribute("breadCrumb", breadCrumb);
    }
    request.setAttribute("showResponsiveTimeLine", "true");
    request.setAttribute("includeFlexScript","no");    
    // end 
    return new ModelAndView(mappingFindForward);
  }

  public void createQueryString(HttpServletRequest request) {
    String collegeid = request.getParameter("collegeid");
    collegeid = GenericValidator.isBlankOrNull(collegeid) ? "" : collegeid;
    String catCode = request.getParameter("subject");
    catCode = GenericValidator.isBlankOrNull(catCode) ? "" : catCode;
    String type = request.getParameter("type");
    type = GenericValidator.isBlankOrNull(type) ? "" : type;
    String region = request.getParameter("region");
    region = GenericValidator.isBlankOrNull(region) ? "" : region;
    String queryStr = "";
    if (collegeid != "") {
      queryStr = "?collegeid=" + collegeid;
    }
    if (catCode != "") {
      queryStr = queryStr != "" ? (queryStr + "&subject=" + catCode) : ("?subject=" + catCode);
    }
    if (type != "") {
      queryStr = queryStr != "" ? (queryStr + "&type=" + type) : ("?type=" + type);
    }
    if (region != "") {
      queryStr = queryStr != "" ? (queryStr + "&region=" + region) : ("?region=" + region);
    }
    request.setAttribute("modqueryStr", queryStr);
  }

  public void createQueryStringWOSubject(HttpServletRequest request) {
    String collegeid = request.getParameter("collegeid");
    collegeid = GenericValidator.isBlankOrNull(collegeid) ? "" : collegeid;
    String type = request.getParameter("type");
    type = GenericValidator.isBlankOrNull(type) ? "" : type;
    String region = request.getParameter("region");
    region = GenericValidator.isBlankOrNull(region) ? "" : region;
    String queryStr = "";
    if (collegeid != "") {
      queryStr = "?collegeid=" + collegeid;
    }
    if (type != "") {
      queryStr = queryStr != "" ? (queryStr + "&type=" + type) : ("?type=" + type);
    }
    if (region != "") {
      queryStr = queryStr != "" ? (queryStr + "&region=" + region) : ("?region=" + region);
    }
    request.setAttribute("modqueryStrWO", queryStr);
  }

}
