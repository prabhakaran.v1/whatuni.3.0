package com.wuni.advertiser.prospectus.controller;

import com.wuni.advertiser.prospectus.form.ProspectusBean;
import WUI.registration.controller.RegistrationController;
import WUI.registration.bean.RegistrationBean;
import WUI.search.form.SearchBean;
import com.wuni.util.sql.DataModel;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import com.wuni.util.valueobject.CommonVO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpSession;

/**
  * @ProspectusCommonFunction.java
  * @Version : 2.0
  * @December 16 2008
  * @www.whatuni.com
  * @Created By : Balraj Selvakumar
  * @Modifeid : Velayutharaj.M
  * @Purpose  : This program is used to group prospected related funcitons.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  *
  */
public class ProspectusCommonFunction {
  public ProspectusCommonFunction() {
  }

  /**
    *   This function is used to update the prospectus details and also log the stats related information.
    * @param userId
    * @param collegeIds
    * @param subjectIds
    * @param firstName
    * @param lastName
    * @param address1
    * @param address2
    * @param town
    * @param postCode
    * @param studyLevel
    * @param request
    * @param openDayFlag
    * @param age
    * @param courseId
    * @param subject
    * @param country
    * @param yearOfEntry
    * @param gender
    * @return addressid as String.
    */
 //public String updateProspectusDetails(String userId, String[] collegeIds, String[] subjectIds, String firstName, String lastName, String address1, String address2, String town, String postCode, String studyLevel, HttpServletRequest request, String openDayFlag, String age, String courseId, String subject, String country, String yearOfEntry, String gender) {  // commented on 01St december 2009
  public String updateProspectusDetails(String userId, String[] collegeIds, String[] subjectIds, String firstName, String lastName, String address1, String address2, String town, String postCode, String studyLevel, HttpServletRequest request, String openDayFlag, String dateOfBirth, String courseId, String subject, String country, String yearOfEntry, String gender, String[] subOrderItemIDs, String typeOfProspectus) {  // changed on 01St december 2009
    DataModel dataModel = new DataModel();
    String addressId = "";
    String selectCollegeList = "";
    String selectSubjectList = "";
    String selectSubOrderItemIdList = "";
    SessionData sessiondata = new SessionData();
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");    
    if(cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0){ 
      cpeQualificationNetworkId = "2";
    }request.getSession().setAttribute("cpeQualificationNetworkId",cpeQualificationNetworkId);
    String widgetId = "";
    if(request.getSession().getAttribute("widgetId") != null && request.getSession().getAttribute("widgetId").toString().trim().length() > 0){
        widgetId = (String)request.getSession().getAttribute("widgetId");
    }
    String clientIp = request.getHeader("CLIENTIP");
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    }
    for (int i = 0; i < collegeIds.length; i++) {
      selectCollegeList = selectCollegeList + collegeIds[i] + ",";
    }
    if (subOrderItemIDs != null && subOrderItemIDs.length > 0) {
      for (int i = 0; i < subOrderItemIDs.length; i++) {
        selectSubOrderItemIdList = selectSubOrderItemIdList + subOrderItemIDs[i] + ",";
      }
    }
    if (subjectIds != null && subjectIds.length > 0) {
      for (int i = 0; i < subjectIds.length; i++) {
        selectSubjectList = selectSubjectList + subjectIds[i] + ",";
      }
    }
    try {
      String p_url = "a=" + GlobalConstants.WHATUNI_AFFILATE_ID;
      Vector parameters = new Vector();      
      parameters.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      parameters.add(userId);
      parameters.add(firstName);
      parameters.add(lastName);
      parameters.add(address1);
      parameters.add(address2);
      parameters.add(town);
      parameters.add(postCode);
      parameters.add(selectCollegeList != null && selectCollegeList.trim().length() > 0? selectCollegeList.substring(0, selectCollegeList.length() - 1): "");
      parameters.add(selectSubjectList != null && selectSubjectList.trim().length() > 0? selectSubjectList.substring(0, selectSubjectList.length() - 1): "");
      parameters.add(studyLevel);
      parameters.add(p_url);
      parameters.add(sessiondata.getData(request, "x"));
      parameters.add(request.getHeader("User-Agent"));
      parameters.add(clientIp);
      parameters.add(openDayFlag);
      parameters.add("");   // changed for 01st December 2009 Release 
      courseId = courseId != null && courseId.equals("0")? "": courseId;
      parameters.add(courseId);
      subject = subject != null && subject.equals("0")? "": subject;
      parameters.add(subject);
      parameters.add(country);
      parameters.add(yearOfEntry);
      parameters.add(gender);
      String requestUrl = CommonUtil.getRequestedURL(request); //Change the getRequestURL by Prabha on 28_Jun_2016   
      parameters.add(requestUrl);
      parameters.add(request.getHeader("referer"));
      parameters.add(dateOfBirth);  
      parameters.add(selectSubOrderItemIdList != null && selectSubOrderItemIdList.trim().length() > 0? selectSubOrderItemIdList.substring(0, selectSubOrderItemIdList.length() - 1): "");       
      parameters.add(cpeQualificationNetworkId); 
      parameters.add(typeOfProspectus);
      parameters.add(widgetId);
      addressId = dataModel.getString("wu_course_search_pkg.update_college_prospectus_fn", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
    }
    return addressId;
  }

  /**
    *   This function is used to set the user related information such as basket content.
    * @param form
    * @param request
    * @return none.
    */
  public void setUserInformation(ProspectusBean form, HttpServletRequest request) {
    ResultSet resultset = null;
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    vector.clear();
    vector.add(new SessionData().getData(request, "y"));
    try {
      resultset = dataModel.getResultSet("wu_review_pkg.Get_User_Info_Fn", vector);
      while (resultset.next()) {
        form.setEmailAddress(resultset.getString("user_email"));
        form.setFirstName(resultset.getString("forename"));
        form.setLastName(resultset.getString("surname"));
        form.setTown(resultset.getString("currently_living"));
        form.setPostCode(resultset.getString("postcode"));
        form.setGender(resultset.getString("gender"));
        form.setStudyLevel(resultset.getString("at_uni"));
        if (form.getStudyLevel().equalsIgnoreCase("postgraduate")) {
          form.setYearOfUniEntry(resultset.getString("year_of_graduation"));
        } else {
          form.setYearOfUniFutureStud(resultset.getString("year_of_graduation"));
        }
        form.setDateOfBirth(resultset.getString("dob"));  // added for 01st december 2009 Release
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
  }

  /**
    *   This function is used to load the studyleves.
    * @return Studylevels as Collection object.
    */
  public List getStudyLevelList() {
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    ResultSet resultset = null;
    ArrayList list = new ArrayList();
    SearchBean bean = null;
    try {
      resultset = dataModel.getResultSet("Wu_Course_Search_Pkg.get_prospectus_level_fn", vector);
      while (resultset.next()) {
        bean = new SearchBean();
        bean.setOptionId(resultset.getString("display_id"));
        bean.setOptionText(resultset.getString("display_name"));
        list.add(bean);
      }
    } catch (Exception studyLevelException) {
      studyLevelException.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }

  /**
    *   This function is used to set the address fields for the prospectus.
    * @param form
    * @param request
    * @return none.
    */
  public void setProspectusAddress(ProspectusBean form, HttpServletRequest request) {
    ResultSet resultset = null;
    DataModel dataModel = new DataModel();
    Vector vector = new Vector();
    SessionData sessiondata = new SessionData();
    try {
      vector.add(sessiondata.getData(request, "y"));
      resultset = dataModel.getResultSet("wu_course_search_pkg.get_user_prospectus_add_fn", vector);
      while (resultset.next()) {
        form.setAddress1(resultset.getString("address_line_1"));
        form.setAddress2(resultset.getString("address_line_2"));
        form.setTown(resultset.getString("city"));
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
  }

  /**
    *   This function is used to load the user basket uni from cookies.
    * @param request
    * @param collegeIds
    * @return uni basket as Collection object.
    */
  public ArrayList getCookiesUniContent(HttpServletRequest request, String collegeIds) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    ArrayList list = new ArrayList();
    ProspectusBean bean = null;
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(collegeIds);
      resultset = dataModel.getResultSet("wu_prospectus_pkg.get_cookies_uni_shortlist_fn", vector);
      while (resultset.next()) {
        bean = new ProspectusBean();
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setWebsitePaidFlag(resultset.getString("website_pur_flag"));
        bean.setCollegeChecked(true);
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }

  /**
   *   This function is used to load the user basket uni from cookies.
   * @param request
   * @param courseIds
   * @return Courselist as Collection object.
   */
  public ArrayList getCookiesCourseContent(HttpServletRequest request, String courseIds) {
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    ArrayList list = new ArrayList();
    ProspectusBean bean = null;
    try {
      Vector vector = new Vector();
      vector.clear();
      vector.add(GlobalConstants.WHATUNI_AFFILATE_ID);
      vector.add(courseIds);
      resultset = dataModel.getResultSet("wu_prospectus_pkg.get_cookies_crs_shortlist_fn", vector);
      while (resultset.next()) {
        bean = new ProspectusBean();
        bean.setCourseId(resultset.getString("course_id"));
        bean.setCourseName(resultset.getString("course_title"));
        bean.setCollegeId(resultset.getString("college_id"));
        bean.setCollegeName(resultset.getString("college_name"));
        bean.setWebsitePaidFlag(resultset.getString("website_pur_flag"));
        bean.setCourseChecked(true);
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      dataModel.closeCursor(resultset);
    }
    return list;
  }

  /**
    *   This function is used to load the course information for the given courseid.
    * @param request
    * @param courseIds
    * @return none.
    */
  public void loadCourseInfo(HttpServletRequest request, String courseIds) {
    if (courseIds != null && courseIds.trim().length() > 0) {
      ProspectusCommonFunction prospectuscommon = new ProspectusCommonFunction();
      List courseBasketList = prospectuscommon.getCookiesCourseContent(request, courseIds);
      if (courseBasketList != null && courseBasketList.size() > 0) {
        request.setAttribute("courseBasketList", courseBasketList);
      }
    }
  }

  /**
    *   This function is used to load the college information for the given collegeid
    * @param request
    * @param collegeIds
    * @return none.
    */
  public void loadCollegeInfo(HttpServletRequest request, String collegeIds) {
    if (collegeIds != null && collegeIds.trim().length() > 0) {
      ProspectusCommonFunction prospectuscommon = new ProspectusCommonFunction();
      List uniBasketList = prospectuscommon.getCookiesUniContent(request, collegeIds);
      if (uniBasketList != null && uniBasketList.size() > 0) {
        request.setAttribute("unibasketList", uniBasketList);
      }
    }
  }

  /**
    *   This function is used to update the prospectus related informations that includes user registarion & sending Email to course provider & user.
    * @param form
    * @param request
    * @param collegeIds
    * @param context
    * @param courseId
    * @param subject
    * @param response
    * @return userid as String.
    */
  protected String updateCollegeProspectus(ProspectusBean bean, HttpServletRequest request, String[] collegeIds, ServletContext context, String courseId, String subject, HttpServletResponse response,String newsLetterFlag, String typeOfProspects) {
    String userId = "";
    CommonFunction common = new CommonFunction();
    GlobalFunction global = new GlobalFunction();
    RegistrationController registration = new RegistrationController();
    try {
      HttpSession session = request.getSession();
      userId = global.checkEmailExist(bean.getEmailAddress(), request);
      String password = "";
      String subOrderItemId = request.getParameter("subOrderItemId");  
      if(subOrderItemId == null || subOrderItemId.trim().length() == 0){
        subOrderItemId = bean.getSubOrderItemId();    
      }        
      String[] subOrderItemIDs = new String[1];
      subOrderItemIDs[0] = subOrderItemId;
      
      if (userId == null) {
        session.removeAttribute("userInfoList");
        String fileName = getRandomImage();
        List questionList = registration.generateRegistrationQuestion(request);
        ArrayList questionAnsList = new ArrayList();
        for (int i = 0; i < questionList.size(); i++) {
          RegistrationBean rbean = (RegistrationBean)questionList.get(i);
          if (rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("Email address") || rbean.getQuestionLabel().equalsIgnoreCase("Confirm email address")) {
            rbean.setSelectedValue(bean.getEmailAddress());
          }
          if (rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("Password")) {
            password = common.getRandomPassword();
            rbean.setSelectedValue(password);
          }
          if (rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("First name")) {
            rbean.setSelectedValue(bean.getFirstName());
          }
          if (rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("Surname")) {
            rbean.setSelectedValue(bean.getLastName());
          }
          if (rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("Nickname")) {
            if (bean.getFirstName() != null && bean.getFirstName().trim().length() > 19) {
              rbean.setSelectedValue(bean.getFirstName().substring(0, 19));
            } else {
              rbean.setSelectedValue(bean.getFirstName());
            }
          }
          if (rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("Nationality")) {
            String nationality = "210";
            if (bean.isNonukresident()) {
              nationality = bean.getNationality();
            }
            rbean.setSelectedValue(nationality);
          }
          if (rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("Postcode")) {
            if (!bean.isNonukresident()) {
              rbean.setSelectedValue(bean.getPostCode());
            } else {
              rbean.setSelectedValue("");
            }
          }
          if (rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("Gender")) {
            rbean.setSelectedValue(bean.getGender());
          }
          if (rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("I am a")) {
            String aboutMeData[] = bean.getGroupAboutMe();
            if ((aboutMeData[0] != null && aboutMeData[0].trim().length() > 0) && (aboutMeData[1] != null && aboutMeData[1].trim().length() > 0)) {
              rbean.setSelectedValue("Future student");
            } else if ((aboutMeData[0] != null && aboutMeData[0].trim().length() > 0) && (aboutMeData[1] != null && aboutMeData[1].trim().length() == 0)) {
              rbean.setSelectedValue("Future student");
            } else if ((aboutMeData[0] != null && aboutMeData[0].trim().length() == 0) && (aboutMeData[1] != null && aboutMeData[1].trim().length() > 0)) {
              rbean.setSelectedValue("Undergraduate");
            }
          }
          if (rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("Intended year of entry to uni")) {
            if (bean.getGroupAboutMe() != null) {
              String aboutMeData[] = bean.getGroupAboutMe();
              if ((aboutMeData[0] != null && aboutMeData[0].trim().length() > 0) && (aboutMeData[1] != null && aboutMeData[1].trim().length() > 0)) {
                rbean.setSelectedValue(bean.getYearOfUniFutureStud());
              } else if ((aboutMeData[0] != null && aboutMeData[0].trim().length() > 0) && (aboutMeData[1] != null && aboutMeData[1].trim().length() == 0)) {
                rbean.setSelectedValue(bean.getYearOfUniFutureStud());
              } else if ((aboutMeData[0] != null && aboutMeData[0].trim().length() == 0) && (aboutMeData[1] != null && aboutMeData[1].trim().length() > 0)) {
                rbean.setSelectedValue(bean.getYearOfUniEntry());
              }
            }
          }
          if(rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("Date of birth")){
            rbean.setSelectedValue(bean.getDateOfBirth());
          }
           if(rbean.getQuestionLabel() != null && rbean.getQuestionLabel().equalsIgnoreCase("Mobile phone number")){
             rbean.setSelectedValue(bean.getMobileNumber());
           } 
          questionAnsList.add(rbean);
        }
        session.setAttribute("DynamicQuestionList", questionAnsList);
        RegistrationBean regBean = new RegistrationBean();
        
        userId = updateRegistrationDetails(request, response, newsLetterFlag);
        //new GlobalFunction().updateBasketContent(request, userId);//Commented for 19th May release as part of W_USER_SEARCHES
        
        regBean.setSelectFileUpload("");
        regBean.setDefaultImageUpload(fileName);
        String voucherCode = new CommonUtil().getMusicVocher(userId, request); /* email send to the user with voucherid for downloading the music */
        if (voucherCode != null && voucherCode.trim().length()>0) {
          request.setAttribute("voucherCode", voucherCode);
        }
        common.sendRegistrtionMail(userId, voucherCode, request); /* email send to the user after registration */
        new CommonFunction().loadUserLoggedInformation(request, context, response);
      }
      if (userId != null && !userId.equals("0") && !userId.equalsIgnoreCase("null") && userId.trim().length() > 0) {
        //if(bean.isReceiveInfo()){
          String cids = "";
          for (int i = 0; i < collegeIds.length; i++) {
            cids = cids + collegeIds[i] + ",";
          }
          cids = cids != null? cids.substring(0, cids.length() - 1): "";
          addBulkBasket(userId, cids, "C", request);
       // }
      }
      String selectedStudyLevel[] = bean.getGroupAboutMe();
      String prospectusType = selectedStudyLevel != null && selectedStudyLevel[0] != null && selectedStudyLevel[0].trim().length() > 0? selectedStudyLevel[0] + (selectedStudyLevel != null && selectedStudyLevel[1] != null && selectedStudyLevel[1].trim().length() > 0? ", " + selectedStudyLevel[1]: ""): (selectedStudyLevel != null && selectedStudyLevel[1] != null && selectedStudyLevel[1].trim().length() > 0? selectedStudyLevel[1]: "");
      String opendays = bean.getOpenDays() != null && !bean.getOpenDays().equalsIgnoreCase("null") && bean.getOpenDays().trim().length() > 0? bean.getOpenDays(): "";
      
     //added for 28th April 2009 Release by selva
      String country = "";
      if (bean.isNonukresident()) {
        country = bean.getNationality();
      }
      //added for 28th April 2009 Release by selva
      subject = subject != null && subject.trim().length() > 0? new CommonFunction().replacePlus(subject): subject;
      /* To check maximum number prospectus requested by the user in the current day - June 16th 2009 release */
      int existingProspectusCount = Integer.parseInt(new ProspectusCommonFunction().getUserProspectusCount(userId));
      int currentProspectusCount = collegeIds != null? collegeIds.length: 0;
      int maxprospectus = Integer.parseInt(new CommonFunction().getWUSysVarValue("WU_MAX_UNI_PROSPECTUS"));
      int remainingprospectus = maxprospectus - existingProspectusCount;
      boolean updataprospectusflag = true;
      if (existingProspectusCount >= maxprospectus) {
        updataprospectusflag = false;
        request.setAttribute("userprospectuscount", "MAX");
        request.setAttribute("maxprospectus", String.valueOf(maxprospectus));
      } else if ((existingProspectusCount + currentProspectusCount) > maxprospectus) {
        updataprospectusflag = false;
        request.setAttribute("userprospectuscount", String.valueOf(existingProspectusCount));
        request.setAttribute("remainingprospectus", String.valueOf(remainingprospectus));
        request.setAttribute("maxprospectus", String.valueOf(maxprospectus));
        request.setAttribute("currentProspectusCount", String.valueOf(currentProspectusCount));
      }
      /* ////////////////////////////////////////////// */
      if (updataprospectusflag) {
        String yearOfEntry = "";
        String aboutMeData[] = bean.getGroupAboutMe();
        if ((aboutMeData[0] != null && aboutMeData[0].trim().length() > 0) && (aboutMeData[1] != null && aboutMeData[1].trim().length() > 0)) {
          yearOfEntry = bean.getYearOfUniFutureStud();
        } else if ((aboutMeData[0] != null && aboutMeData[0].trim().length() > 0) && (aboutMeData[1] != null && aboutMeData[1].trim().length() == 0)) {
          yearOfEntry = bean.getYearOfUniFutureStud();
        } else if ((aboutMeData[0] != null && aboutMeData[0].trim().length() == 0) && (aboutMeData[1] != null && aboutMeData[1].trim().length() > 0)) {
          yearOfEntry = bean.getYearOfUniEntry();
        }
        //String statLogId = updateProspectusDetails(userId, collegeIds, null, bean.getFirstName(), bean.getLastName(), bean.getAddress1(), bean.getAddress2(), bean.getTown(), bean.getPostCode(), prospectusType, request, opendays, bean.getAge(), courseId, subject, country, yearOfEntry, bean.getGender());  // commented for 01st december 2009 Release
         String statLogId = updateProspectusDetails(userId, collegeIds, null, bean.getFirstName(), bean.getLastName(), bean.getAddress1(), bean.getAddress2(), bean.getTown(), bean.getPostCode(), prospectusType, request, opendays, bean.getDateOfBirth(), courseId, subject, country, yearOfEntry, bean.getGender(),subOrderItemIDs, typeOfProspects); // cnanged for 01st December 2009 Release 
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return userId;
  }

  /**
    *   This function is used to load the random image list.
    * @return Random image path as String.
    */
  public String getRandomImage() {
    int randomNoLimit = GlobalConstants.DEFAULT_IMAGE_LENGTH;
    List imageList = getImageList();
    Random r = new Random();
    int randint = r.nextInt(randomNoLimit);
    return (String)imageList.get(randint);
  }

  /**
    *   This function is used to load the image list from ApplicationResources.properties.
    * @return Image list as Collection object.
    */
  private List getImageList() {
    List list = new ArrayList();
    ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    for (int cou = 1; cou <= 50; cou++) {
      String propertyName = "wuni.default.images_id_" + String.valueOf(cou);
      list.add(bundle.getString(propertyName));
    }
    return list;
  }
  
  /**
    *   This function is used to load the Gender list.
    * @param request
    * @return none.
    */
  protected void loadGenderList(HttpServletRequest request) {
    ProspectusBean bean = null;
    ResultSet resultSet = null;
    Vector vector = new Vector();
    DataModel datamodel = new DataModel();
    ArrayList genderList = new ArrayList();
    try {
      vector.add("50");
      vector.add("YES");
      resultSet = datamodel.getResultSet("wu_review_pkg.get_drop_down_list_fn", vector);
      while (resultSet.next()) {
        bean = new ProspectusBean();
        bean.setOptionValue(resultSet.getString("value"));
        bean.setOptionDescription(resultSet.getString("description"));
        genderList.add(bean);
      }
      request.setAttribute("genderList", genderList);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      datamodel.closeCursor(resultSet);
    }
  }

  /**
    *   This function is used to add all the selected course providers to user basket.
    * @param userId
    * @param assocIds
    * @param assocType
    * @param request
    * @return return status as String.
    */
  public String addBulkBasket(String userId, String assocIds, String assocType, HttpServletRequest request) {
    SessionData sessiondata = new SessionData();
    String value = null;
    try {
      String sessionId = sessiondata.getData(request, "x");
      String affliateId = GlobalConstants.WHATUNI_AFFILATE_ID;
      String clientIp = request.getHeader("CLIENTIP");
      if (clientIp == null || clientIp.length() == 0) {
        clientIp = request.getRemoteAddr();
      }
      String clientBrowser = request.getHeader("user-agent");
      Vector vector = new Vector();
      vector.add(affliateId);
      vector.add(userId != null? userId.trim(): userId); //10.03.2009 release
      vector.add(assocIds != null? assocIds.trim(): assocIds); //10.03.2009 release
      vector.add(assocType);
      vector.add(sessionId);
      vector.add(clientIp);
      vector.add(clientBrowser);
      String requestUrl = CommonUtil.getRequestedURL(request); //Change the getRequestURL by Prabha on 28_Jun_2016   
      vector.add(requestUrl);
      vector.add(request.getHeader("referer"));
      vector.add(new SessionData().getData(request, "userTrackId"));    
      value = new DataModel().getString("wu_prospectus_pkg.upd_shortlist_college_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return value;
  }

  /**
    *   This function is used for the user registration.
    * @param request
    * @return userid as String.
    * @throws ServletException
    * @throws IOException
    */
  public String updateRegistrationDetails(HttpServletRequest request, HttpServletResponse response,String newsLetterFlag) throws ServletException, IOException {
    String userId = "";
    RegistrationBean bean = null;
    CommonUtil commonUtil = new CommonUtil();
    CookieManager cookieManager = new CookieManager();
    int rowNum = 4;
    HttpSession session = request.getSession();
    ResultSet resultset = null;
    ArrayList questionlist = (ArrayList)session.getAttribute("DynamicQuestionList");
    String answerId[] = new String[questionlist.size() + 8];
    String answerText[] = new String[questionlist.size() + 8];
    answerId[0] = "x";
    answerText[0] = new SessionData().getData(request, "x");
    answerId[1] = "y";
    answerText[1] = ""; //user Id 
    answerId[2] = "a";
    answerText[2] = GlobalConstants.WHATUNI_AFFILATE_ID;
    answerId[3] = "190";
    answerText[3] = "USER REGISTRATION WU"; //form ID
    if (questionlist != null) {
      Iterator questioniterator = questionlist.iterator();
      while (questioniterator.hasNext()) {
        bean = new RegistrationBean();
        bean = (RegistrationBean)questioniterator.next();
        String questionanswerId = bean.getFiledId();
        String questionanswerText = bean.getSelectedValue() != null? bean.getSelectedValue().trim(): bean.getSelectedValue();
        answerId[rowNum] = "pm_" + questionanswerId;
        answerText[rowNum] = questionanswerText;
        rowNum++;
      }
      String password = new CommonFunction().getRandomPassword();
      answerId[rowNum] = "pm_2";
      answerText[rowNum] = password;
      rowNum++;
      answerId[rowNum] = "pm_61";
      answerText[rowNum] = "";
      rowNum++;
      answerId[rowNum] = "pm_346";
      answerText[rowNum] = "";
       rowNum++;
        answerId[rowNum] = "newsletter";
        answerText[rowNum] = newsLetterFlag;
      if (answerId != null && answerText != null) {
        Connection connection = null;
        OracleCallableStatement oraclestmt = null;
        try {
          DataSource datasource = null;
          try {
            InitialContext ic = new InitialContext();
            datasource = (DataSource)ic.lookup("whatuni");
          } catch (Exception exception) {
            exception.printStackTrace();
          }
          connection = datasource.getConnection();
          String query = "{ ?= call hot_admin.hc_maintain_user3.create_user_reg_fn(?,?) }";
          oraclestmt = (OracleCallableStatement)connection.prepareCall(query);
          oraclestmt.registerOutParameter(1, OracleTypes.CURSOR);
          ArrayDescriptor descriptor = ArrayDescriptor.createDescriptor("T_USERREGTABTYPE", connection);
          ARRAY array_1 = new ARRAY(descriptor, connection, answerId);
          ARRAY array_2 = new ARRAY(descriptor, connection, answerText);
          oraclestmt.setARRAY(2, array_1);
          oraclestmt.setARRAY(3, array_2);
          oraclestmt.execute();
          resultset = (ResultSet)oraclestmt.getObject(1);
          while (resultset.next()) {
            userId = resultset.getString("user_id");
            new SessionData().addData(request, response, "y", resultset.getString("user_id"));
            new SessionData().addData(request, response, "x", resultset.getString("session_id"));
          }
          if(!GenericValidator.isBlankOrNull(userId) && !"0".equals(userId)){
             //Encryting to user id to base64 and adding it in cookie by Sangeeth.S for whatuniGO journey            
             commonUtil.addEncryptedCookieUserId(request, userId, response);           
           }
           // 
        } catch (Exception exception) {
          exception.printStackTrace();
        } finally {
          try {
            if (oraclestmt != null) {
              oraclestmt.close();
            }
            if (resultset != null) {
              resultset.close();
            }
            if (connection != null) {
              connection.close();
            }
          } catch (Exception closeException) {
            closeException.printStackTrace();
          }
        }
      }
    }
    return userId;
  }

  /**
    *   This function is used to log the prospectus details.
    * @param collegeIds
    * @param userId
    * @param emailAddress
    * @param gender
    * @param firstName
    * @param lastName
    * @param address1
    * @param address2
    * @param town
    * @param postCode
    * @param collegeCount
    * @param userAgent
    * @param clientIp
    * @return log status as String.
    */
    //Need to remove below method and refernce for 03-Nov-2015, By Thiyagu G.
  public String updateProspectusLog(String collegeIds, String userId, String emailAddress, String gender, String firstName, String lastName, String address1, String address2, String town, String postCode, int collegeCount, String userAgent, String clientIp) {
    Vector vector = new Vector();
    vector.add(collegeIds);
    vector.add(userId);
    vector.add(emailAddress);
    vector.add(gender);
    vector.add(firstName);
    vector.add(lastName);
    vector.add(address1);
    vector.add(address2);
    vector.add(town);
    vector.add(postCode);
    vector.add(String.valueOf(collegeCount));
    vector.add(userAgent);
    vector.add(clientIp);
    vector.add("");
    String logStatus = "";
    try {
      logStatus = new DataModel().getString("upd_sent_user_pros_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return logStatus;
  }

  /**
    *   This function is used to get the prospectus count for the given userid.
    * @param userId
    * @return prospetuscount as String.
    */
  public String getUserProspectusCount(String userId) {
    Vector vector = new Vector();
    vector.add(userId);
    String prospectusCount = "";
    try {
      prospectusCount = new DataModel().getString("wu_prospectus_pkg.get_user_prosp_cnt_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return prospectusCount;
  }

  /**
    *   This function is used to find the blocked user list for the given email address.
    * @param emilAddress
    * @return blocked status as String.
    */
  public String checkBlockUserStatus(String emilAddress) {
    Vector vector = new Vector();
    String status = "";
    try {
      vector.add(emilAddress);
      status = new DataModel().getString("WU_PROSPECTUS_PKG.chk_email_block_fn", vector);
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
    }
    return status;
  }
}
