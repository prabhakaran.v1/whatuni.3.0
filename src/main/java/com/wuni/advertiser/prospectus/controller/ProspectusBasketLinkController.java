package com.wuni.advertiser.prospectus.controller;

import java.io.IOException;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.basket.form.BasketBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.layer.business.ICommonBusiness;
import com.wuni.util.sql.DataModel;
import com.wuni.util.valueobject.advert.ProspectusVO;

/**
  * @Version :  1.0
  * @www.whatuni.com
  * @Created By : Priyaa Parthasarathy
  * @Purpose  : This program is used to add selected college/course prospectus to user basket using AJAX.
  * *************************************************************************************************************************
  * Date           Name                      Ver.     Changes desc                                                 Rel Ver.
  * *************************************************************************************************************************
  */
@Controller
public class ProspectusBasketLinkController {
	
	@Autowired
	  private ICommonBusiness commonBusiness;
	
  @RequestMapping(value = "/addprosbasket", method = {RequestMethod.GET,RequestMethod.POST})
  public ModelAndView getProspectusBasketLink(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    CommonFunction common = new CommonFunction();
    SessionData sessiondata = new SessionData();
    BasketBean bean = new BasketBean();
    BasketBean inputBean = new BasketBean();
    String logUserId = new SessionData().getData(request, "y");
    logUserId = GenericValidator.isBlankOrNull(logUserId) || ("0").equals(logUserId)? null: logUserId;
    if (common.checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED //
      return new ModelAndView("forward: /userLogin.html");
    }
    sessiondata.setParameterValuestoSessionData(request, response);
    String assocId = "";
    String assocType = "";
    assocId = GenericValidator.isBlankOrNull(request.getParameter("assocId"))? "": request.getParameter("assocId");
    assocType = GenericValidator.isBlankOrNull(request.getParameter("assocType"))? "": request.getParameter("assocType");
    bean.setAssociationId(assocId);
    String cookie_pros_basket_id = common.checkProspectusCookieStatus(request);
    String return_basket_id = "";
    String selectallids = request.getParameter("selectallids");
    //
    if (request.getParameter("selectallids") != null) {
      String enqType = GenericValidator.isBlankOrNull(request.getParameter("enqType"))? "": request.getParameter("enqType");
      inputBean.setBasketType(enqType);
      String status = "D";
      if (request.getParameter("dowhat") != null && request.getParameter("dowhat").equalsIgnoreCase("add")){
        status = "A";
      }
      String[] selectallidsArr = selectallids.split("GP");
      Object[][] prosBasketValues = new Object[selectallidsArr.length][4];
      for (int i = 0; i < selectallidsArr.length; i++) {
        String column = selectallidsArr[i];
        String[] columnArr = column.split("SP");
        for (int j = 0; j < columnArr.length - 1; j++) {
          prosBasketValues[i][0] = columnArr[1];
          prosBasketValues[i][1] = assocType;
          prosBasketValues[i][2] = columnArr[0];
          prosBasketValues[i][3] = status;
        }
      }
      if (request.getParameter("dowhat") != null && request.getParameter("dowhat").equalsIgnoreCase("add")) {
        if (cookie_pros_basket_id == null || cookie_pros_basket_id.equals("") || cookie_pros_basket_id.trim().length() == 0) {
          addBasketContent(inputBean, logUserId, response, request, prosBasketValues);
          return_basket_id = (String)request.getAttribute("reqRrospectusBasketId");
          String count = getProspectusBasketCount(logUserId, return_basket_id, request);
          setResponseText(response, count);
          return null;
        } else {
          inputBean.setBasketId(cookie_pros_basket_id);
          addBasketContent(inputBean,logUserId, response, request, prosBasketValues);
          return_basket_id = (String)request.getAttribute("reqRrospectusBasketId");
          String count = getProspectusBasketCount(logUserId, return_basket_id, request);
          setResponseText(response, count);
          return null;
        }
      } else if (request.getParameter("dowhat") != null && request.getParameter("dowhat").equalsIgnoreCase("remove")) {
        inputBean.setBasketId(cookie_pros_basket_id);
        addBasketContent(inputBean,logUserId, response, request, prosBasketValues);
        return_basket_id = (String)request.getAttribute("reqRrospectusBasketId");
        String count = getProspectusBasketCount(logUserId, return_basket_id, request);
        setResponseText(response, count);
        return null;
      }
    }
    if (request.getParameter("fromAjax") != null) {
      String subOrderItemId = GenericValidator.isBlankOrNull(request.getParameter("subOrderItemId"))? "": request.getParameter("subOrderItemId");
      String enqType = GenericValidator.isBlankOrNull(request.getParameter("enqType"))? "": request.getParameter("enqType");
      inputBean.setBasketType(enqType);
      if (request.getParameter("dowhat") != null && request.getParameter("dowhat").equalsIgnoreCase("add")) {
        //       
          Object[][] prosBasketValues = new Object[1][4];
          prosBasketValues[0][0] = assocId;
          prosBasketValues[0][1] = assocType;
          prosBasketValues[0][2] = subOrderItemId;
          prosBasketValues[0][3] = "A";
        //
        if (cookie_pros_basket_id == null || cookie_pros_basket_id.equals("") || cookie_pros_basket_id.trim().length() == 0) {
          addBasketContent(inputBean,logUserId,  response, request, prosBasketValues);
          return_basket_id = (String)request.getAttribute("reqRrospectusBasketId");
          String count = getProspectusBasketCount(logUserId, return_basket_id, request);
          setResponseText(response, count);
          return null;
        } else {
          inputBean.setBasketId(cookie_pros_basket_id);
          addBasketContent(inputBean,logUserId, response, request, prosBasketValues);  
          return_basket_id = (String)request.getAttribute("reqRrospectusBasketId");
          String count = getProspectusBasketCount(logUserId, return_basket_id, request);
          setResponseText(response, count);
          return null;
        }
      }
      if (request.getParameter("dowhat") != null && request.getParameter("dowhat").equalsIgnoreCase("remove")) {
          Object[][] prosBasketValues = new Object[1][4];
          prosBasketValues[0][0] = assocId;
          prosBasketValues[0][1] = assocType;
          prosBasketValues[0][2] = subOrderItemId;
          prosBasketValues[0][3] = "D";
        if (cookie_pros_basket_id == null || cookie_pros_basket_id.equals("") || cookie_pros_basket_id.trim().length() == 0) {
          addBasketContent(inputBean,logUserId, response, request, prosBasketValues);
          return_basket_id = (String)request.getAttribute("reqRrospectusBasketId");
          String count = getProspectusBasketCount(logUserId, return_basket_id, request);
          setResponseText(response, count);
          return null;
        } else {
          inputBean.setBasketId(cookie_pros_basket_id);
          addBasketContent(inputBean,logUserId, response, request, prosBasketValues);
          return_basket_id = (String)request.getAttribute("reqRrospectusBasketId");
          String count = getProspectusBasketCount(logUserId, return_basket_id, request);
          setResponseText(response, count);
          return null;
        }
      }
    }

    if (request.getParameter("fromPopup") != null) {
    String forward = "popupProspectusOrder";
     if(request.getParameter("frominteraction") != null ){
       forward = "interactionproscpectuspod";
     }
      String subOrderItemId = GenericValidator.isBlankOrNull(request.getParameter("subOrderItemId"))? "": request.getParameter("subOrderItemId");
      String enqType = GenericValidator.isBlankOrNull(request.getParameter("enqType"))? "": request.getParameter("enqType");
      inputBean.setBasketType(enqType);
      if (request.getParameter("dowhat") != null && request.getParameter("dowhat").equalsIgnoreCase("add")) {
        //       
        Object[][] prosBasketValues = new Object[1][4];
        prosBasketValues[0][0] = assocId;
        prosBasketValues[0][1] = assocType;
        prosBasketValues[0][2] = subOrderItemId;
        prosBasketValues[0][3] = "A";
        //
        if (cookie_pros_basket_id == null || cookie_pros_basket_id.equals("") || cookie_pros_basket_id.trim().length() == 0) {
          bean = addBasketContent(inputBean,logUserId, response, request, prosBasketValues);
          return_basket_id = (String)request.getAttribute("reqRrospectusBasketId");
          new ProspectusPopupAjaxController().getProspectusPopupContent(logUserId, return_basket_id, request, response);          
          setResponseText(response, "##SPLIT##"+ request.getAttribute("prospectusBasketCount"));
          return new ModelAndView(forward);
        } else {
          inputBean.setBasketId(cookie_pros_basket_id);
          bean = addBasketContent(inputBean,logUserId, response, request, prosBasketValues);
          return_basket_id = (String)request.getAttribute("reqRrospectusBasketId");
          new ProspectusPopupAjaxController().getProspectusPopupContent(logUserId, return_basket_id, request, response);      
          setResponseText(response, "##SPLIT##"+ request.getAttribute("prospectusBasketCount"));
          return new ModelAndView(forward);
        }
      }
      if (request.getParameter("dowhat") != null && request.getParameter("dowhat").equalsIgnoreCase("remove")) {
        Object[][] prosBasketValues = new Object[1][4];
        prosBasketValues[0][0] = assocId;
        prosBasketValues[0][1] = assocType;
        prosBasketValues[0][2] = subOrderItemId;
        prosBasketValues[0][3] = "D";
        if (cookie_pros_basket_id == null || cookie_pros_basket_id.equals("") || cookie_pros_basket_id.trim().length() == 0) {
          bean = addBasketContent(inputBean,logUserId, response, request, prosBasketValues);
          return_basket_id = (String)request.getAttribute("reqRrospectusBasketId");
          new ProspectusPopupAjaxController().getProspectusPopupContent(logUserId, return_basket_id, request, response);       
          setResponseText(response, "##SPLIT##"+ request.getAttribute("prospectusBasketCount"));
          return new ModelAndView(forward);
        } else {
          inputBean.setBasketId(cookie_pros_basket_id);
          bean = addBasketContent(inputBean,logUserId, response, request, prosBasketValues);
          return_basket_id = (String)request.getAttribute("reqRrospectusBasketId");
          new ProspectusPopupAjaxController().getProspectusPopupContent(logUserId, return_basket_id, request, response); 
          setResponseText(response, "##SPLIT##"+ request.getAttribute("prospectusBasketCount"));
          return new ModelAndView(forward);
        }
      }
    }
    return null;
  }

  /**
    *   This function is used to add the college/course to user basket.
    * @param request
    * @return prospectusBasketId as String.
    * @throws SQLException
    */
  public BasketBean addBasketContent(BasketBean addBean,String logUserId, HttpServletResponse response, HttpServletRequest request, Object[][] attrValues) throws SQLException {
    
    String userTrackSessionId = new SessionData().getData(request, "userTrackId");
    addBean.setUserId(logUserId);
    String clientIp = request.getHeader("CLIENTIP");
    if (clientIp == null || clientIp.length() == 0) {
      clientIp = request.getRemoteAddr();
    }
    String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
    if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
      cpeQualificationNetworkId = "2";
    }
    try {
      addBean.setTrackSessionId(userTrackSessionId);
      
      Map prosBasketMap = commonBusiness.addProspectusBasketContent(addBean, attrValues);
      if (prosBasketMap != null) {
        ArrayList prospectusBasketList = (ArrayList)prosBasketMap.get("p_enq_baskets");
        if (prospectusBasketList != null && prospectusBasketList.size() > 0) {
          request.setAttribute("prospectusBasketList", prospectusBasketList);
          BasketBean prospectusBean = (BasketBean)prospectusBasketList.get(0);
          String prospectusBasketId = prospectusBean.getBasketId();
          if (prospectusBasketId != null && !prospectusBasketId.equals("")) {
            request.setAttribute("reqRrospectusBasketId", prospectusBasketId);
            Cookie cookie = CookieManager.createCookie(request,"prospectusBasketId", prospectusBasketId);
            cookie.setMaxAge(GlobalConstants.PROS_COOKIE_MAX_AGE);
            response.addCookie(cookie);
          }
        }
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return addBean;
  }
  
  private void setResponseText(HttpServletResponse response, String responseMessage) {
     try {
       response.setContentType("TEXT/PLAIN");
       Writer writer = response.getWriter();
       writer.write(responseMessage);
     } catch (IOException exception) {
       exception.printStackTrace();
     }
   }
   
  public String getProspectusBasketCount(String logUserId, String cookie_pros_basket_id,HttpServletRequest request){
    DataModel dataModel = new DataModel();
    ResultSet resultset = null;
    int prosBasketCount = 0;
    try {
      Vector vector = new Vector();
      vector.add(logUserId);
      vector.add(cookie_pros_basket_id);
      vector.add("RP");
      resultset = dataModel.getResultSet("wuni_enquiry_baskets_pkg.get_enq_basket_count_fn", vector);     
      while (resultset.next()) {
        ProspectusVO prospectusVo = new ProspectusVO();
        prospectusVo.setBasketId(resultset.getString("enq_basket_id"));
        prospectusVo.setTotalCount(resultset.getString("total_count")); 
        
        if(!GenericValidator.isBlankOrNull(prospectusVo.getTotalCount())){
          try{
          prosBasketCount = Integer.parseInt(prospectusVo.getTotalCount());          
          }catch(Exception e){
            e.printStackTrace();
          }         
        } 
        break;
      }
      request.setAttribute("prospectusBasketCount", String.valueOf(prosBasketCount));
    } catch (Exception exception) {
      exception.printStackTrace();
    }finally {
      dataModel.closeCursor(resultset);
    }  
    return String.valueOf(prosBasketCount);
  }
}
