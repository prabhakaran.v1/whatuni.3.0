package com.wuni.advertiser;

import com.layer.business.ICommonBusiness;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : VisitWebRedirectAction.java
* Description   : This class used to get open day search results
* @version      : 1.0
* Change Log :
* *********************************************************************************************************************************************
* author	                 Ver 	           Created On     	Modification Details 	               Change
* *********************************************************************************************************************************************
* Priyaa Parthasarathy       1.0               17-Mar-2015         First draft   		
* Sujitha V                  1.1               08-DEC-2020         Modified                            Added one url format for cta redirects,
*                                                                                                      added sending cta flag to DB and changed
*                                                                                                      old data model to spring.
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
*/
@Controller
public class VisitWebRedirectController {
  String finalDayString = null;
  
  @Autowired
  private ICommonBusiness commonBusiness;
  @RequestMapping(value = {"/visitwebredirect", "/navigation-url"}, method = RequestMethod.GET)
  public ModelAndView visitWebsite(@RequestParam Map<String, String> parameters, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    //
	String suborderItemId = StringUtils.isNotBlank(parameters.get("id")) ? parameters.get("id") : "";
	String ctaButtonName  = StringUtils.isNotBlank(parameters.get("cta-button-name")) ? parameters.get("cta-button-name") : "" ;
    String courseId =  StringUtils.isBlank(parameters.get("courseid")) || Integer.parseInt(parameters.get("courseid")) == 0 ? null : parameters.get("courseid");

    Map<String, Object> websiteUrl = commonBusiness.getNavigationUrl(suborderItemId, courseId, ctaButtonName);
    request.setAttribute("url", websiteUrl.get("RetVal").toString());
    //
    return new ModelAndView("visitwebredirect");
    //
  }

}
