package com.wuni.advertiser.ql.validator;

import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import com.layer.util.SpringConstants;
import com.wuni.advertiser.ql.form.QLFormBean;

public class QLFormValidator implements Validator{

  @Override
  public boolean supports(Class<?> arg0) {
    // TODO Auto-generated method stub
	return false;
  }

  HttpServletRequest request = null;
  HttpServletResponse response = null;
	   
  @Override
  public void validate(Object target, Errors errors) {
	// TODO Auto-generated method stub
		
	QLFormBean qlformBean = (QLFormBean) target;
	request = qlformBean.getRequest();
		
		
	String userID = new SessionData().getData(request, "y");
    if(!GenericValidator.isBlankOrNull(qlformBean.getEmailAddress())){
      String existUserId = new GlobalFunction().checkEmailExist(qlformBean.getEmailAddress(), request);		      
	  if(("0".equalsIgnoreCase(userID) || userID == null) && (existUserId != null && !"".equalsIgnoreCase(existUserId) && !"0".equalsIgnoreCase(existUserId))){
	    errors.rejectValue("emailAddress", "wuni.error.profile.existing.emailid", new String[] {qlformBean.getEmailAddress()}, "");
        request.setAttribute("errEmailExists", "Y");
	  }
	}		    
	if (!GenericValidator.isBlankOrNull(qlformBean.getMessage())) {
	  String[] vulnerableKeywords = ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.vulnerable.keywords").split(GlobalConstants.SPLIT_CONSTANT);
	  if(qlformBean.getMessage().trim().length() < SpringConstants.FIFTY) {
		errors.rejectValue("message", "wuni.error.ql.text.max.length.error");
		request.setAttribute("errTextArea", "Y"); 
	  }else {
		for (int i = 0; i < vulnerableKeywords.length; i++) {
		  if ((qlformBean.getMessage()).indexOf(vulnerableKeywords[i]) != -1) {
		    errors.rejectValue("message", "wuni.error.ql.text.area.error");
		    request.setAttribute("errTextArea", "Y");
		    break;
		  }
		}	
      }
	}
		

	if(!GenericValidator.isBlankOrNull(qlformBean.getPostCode()) && StringUtils.equalsIgnoreCase(qlformBean.getTypeOfEnquiry(), "send-college-email")) {
	  final Pattern alphaNumericPattern = Pattern.compile(SpringConstants.ALPHA_NUMERIC_WITH_SPACE);
	  Matcher alphaNumberiValidation = alphaNumericPattern.matcher(qlformBean.getPostCode());
	  if(!alphaNumberiValidation.matches()) {
	    errors.rejectValue("postCodeMessage", "wuni.error.ql.postcode.error");
	    request.setAttribute("errPostCodeArea", "Y"); 
	  }
	}
		    
  }

}
