package com.wuni.advertiser.ql.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class QLQuestionAnswerBean{

  private String orderAttributeId = null;
  private String attributeId = null;
  private String typeAttributeId = null;
  private String attributeName = null;
  private String displaySeq = null;
  private String mandatory = null;
  private String defaultValue = null;
  private String attributeText = null;
  private String optionFlag = null;
  private List options = null;
  private String optionId = null;
  private String optionDesc = null;
  private String optionValue = null;
  private String maxLength = null;
  private String formatDesc = null;
  private String dataTypeDesc = null;
  private String groupId = null;
  private String helpFlag = null;
  private String helpText = null;
  private String attributeValueId = null;
  private String attributeValue = null;
  private String stepNum = null;
  private String existEnqFlag = null;
  private String existUserFlag = null;
  private String errorDesc = null;
  private String attrValueSeq = null;
  private String contextPath = null;
  private String className = null;
  private String deptAttrList = null;

  public void setOrderAttributeId(String orderAttributeId) {
    this.orderAttributeId = orderAttributeId;
  }

  public String getOrderAttributeId() {
    return orderAttributeId;
  }

  public void setAttributeId(String attributeId) {
    this.attributeId = attributeId;
  }

  public String getAttributeId() {
    return attributeId;
  }

  public void setAttributeName(String attributeName) {
    this.attributeName = attributeName;
  }

  public String getAttributeName() {
    return attributeName;
  }

  public void setDisplaySeq(String displaySeq) {
    this.displaySeq = displaySeq;
  }

  public String getDisplaySeq() {
    return displaySeq;
  }

  public void setMandatory(String mandatory) {
    this.mandatory = mandatory;
  }

  public String getMandatory() {
    return mandatory;
  }

  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public void setAttributeText(String attributeText) {
    this.attributeText = attributeText;
  }

  public String getAttributeText() {
    return attributeText;
  }

  public void setOptionFlag(String optionFlag) {
    this.optionFlag = optionFlag;
  }

  public String getOptionFlag() {
    return optionFlag;
  }

  public void setOptions(List options) {
    this.options = options;
  }

  public List getOptions() {
    return options;
  }

  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }

  public String getOptionId() {
    return optionId;
  }

  public void setOptionDesc(String optionDesc) {
    this.optionDesc = optionDesc;
  }

  public String getOptionDesc() {
    return optionDesc;
  }

  public void setOptionValue(String optionValue) {
    this.optionValue = optionValue;
  }

  public String getOptionValue() {
    return optionValue;
  }

  public void setMaxLength(String maxLength) {
    this.maxLength = maxLength;
  }

  public String getMaxLength() {
    return maxLength;
  }

  public void setFormatDesc(String formatDesc) {
    this.formatDesc = formatDesc;
  }

  public String getFormatDesc() {
    return formatDesc;
  }

  public void setDataTypeDesc(String dataTypeDesc) {
    this.dataTypeDesc = dataTypeDesc;
  }

  public String getDataTypeDesc() {
    return dataTypeDesc;
  }

  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }

  public String getGroupId() {
    return groupId;
  }

  public void setHelpFlag(String helpFlag) {
    this.helpFlag = helpFlag;
  }

  public String getHelpFlag() {
    return helpFlag;
  }

  public void setHelpText(String helpText) {
    this.helpText = helpText;
  }

  public String getHelpText() {
    return helpText;
  }

  public void setAttributeValueId(String attributeValueId) {
    this.attributeValueId = attributeValueId;
  }

  public String getAttributeValueId() {
    return attributeValueId;
  }

  public void setAttributeValue(String attributeValue) {
    this.attributeValue = attributeValue;
  }

  public String getAttributeValue() {
    return attributeValue;
  }

  public void setStepNum(String stepNum) {
    this.stepNum = stepNum;
  }

  public String getStepNum() {
    return stepNum;
  }

  public void setExistEnqFlag(String existEnqFlag) {
    this.existEnqFlag = existEnqFlag;
  }

  public String getExistEnqFlag() {
    return existEnqFlag;
  }

  public void setExistUserFlag(String existUserFlag) {
    this.existUserFlag = existUserFlag;
  }

  public String getExistUserFlag() {
    return existUserFlag;
  }

  public void setErrorDesc(String errorDesc) {
    this.errorDesc = errorDesc;
  }

  public String getErrorDesc() {
    return errorDesc;
  }

  public void setAttrValueSeq(String attrValueSeq) {
    this.attrValueSeq = attrValueSeq;
  }

  public String getAttrValueSeq() {
    return attrValueSeq;
  }

  public void setContextPath(String contextPath) {
    this.contextPath = contextPath;
  }

  public String getContextPath() {
    return contextPath;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public String getClassName() {
    return className;
  }

  public void setDeptAttrList(String deptAttrList) {
    this.deptAttrList = deptAttrList;
  }

  public String getDeptAttrList() {
    return deptAttrList;
  }

  public void setTypeAttributeId(String typeAttributeId) {
    this.typeAttributeId = typeAttributeId;
  }

  public String getTypeAttributeId() {
    return typeAttributeId;
  }

}
