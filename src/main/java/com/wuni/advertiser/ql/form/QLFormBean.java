package com.wuni.advertiser.ql.form;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class QLFormBean {

  private String collegeId = "";
  private String collegeName = "";
  private String collegeNameDisplay = "";
  private String courseId = "";
  private String courseName = "";
  private String subjectId = "";
  private String firstName = "";
  private String lastName = "";
  private String address1 = "";
  private String address2 = "";
  private String town = "";
  private String postCode = "";
  private String emailAddress = null;
  boolean nonukresident = false;
  private String aboutMe = null;
  private String gender = null;
  private String nationality = null;
  private boolean tac = false;
  private String nationalityValue = "";
  private String nationalityDesc = "";
  private String dateOfBirth = "";
  private String subject = "";
  private String newsLetter = null;
  private String subOrderItemId = "";
  private String cpeQualificationNetworkId = "";
  private List studyLevelList = null;
  private String studyLevelId = "M";
  private String typeOfEnquiry = "";
  private String emailContent = "";
  private List nationalityList = null; 
  private String yeartoJoinCourse = null;
  private String qlFlag = null;
  private String mobileNumber = "";
  private List questionAnswers=new ArrayList();
  private List countryOfRes = null;
  private String countryOfResidence = null;
  private String countryOfResDesc = null;
  
  private String userId = null;
  private String htmlId = null;
  private String sessionId = null;
  private String clientBrowser = null;
  private String clientIp = null;
  private String requestUrl = null;
  private String refferalUrl = null;
  private String widgetId = null;
  private String affliatedId = null;
  private String typeOfProspectus= null;
  private String userIP = null;
  private String userAgent = null;
  private String message = null;
  private String networkId = null;
  private String enquiryId = null;
  private String profileType = null;  
  private String loggedUserId = null;
  private String studyLevelDesc = null;
  private String mobileFlag = null;
  
  private List postEnquiryList = null;
  private String postButtonFlag = null;
  
  private List dateList = null;
  private List monthList = null;
  private List yearList = null;
  
  private String date = null;
  private String month = null;
  private String year = null;
  private String oneClickCheckBox = null;
  private String oneClickMandatory = null;
  private String predictedGrades = null;
  private String autoEmailFlag = null;
  private String postEnquiry = null;
  private String trackSessionId = null;
  private String bulkProspectusType = null;
  private String enquiryBasketId = null;//28_OCT_2014 added for Bulk prospectus change
  private String enquiryType = null;
  private String countryOfResi = null;
  private String gradeType = null;
  private String gradeValue = null;
  private String prosnonukresident = null;
  private List prospectusCountryOfRes = null;
  private String prosCountryOfResDesc = null;
  private String prosCountryOfRes = null;
  private List prosPredictedGrades = null;
  private List prosGradesList = null;
  private String prosGradePointSelected = null;
  private String prosGradeValueSelected = null;
  private String prosDoNotGradesChkBox = null;
  private String marketingEmail = null;
  private String solusEmail = null;
  private String surveyEmail = null; 
  private String password = null;
  private String screenWidth = null;
  private HttpServletRequest request = null;
  
  private String defaultYoe = null;
  private String consentFlag = null;
  private String consentDate = null;
  private String consentFlagCheck = null;
  private String consentFlagValues = null;
  private String collegeIdLists = null;
  private Object[][] collegeConsentArray = null;
  private String removedUni = null;
  private String latitude = null;
  private String longitude = null;
  private String sponsoredOrderItemId = null;
  private String userUcasScore = null;
  
  private String postCodeMessage = null;
  private String yearOfEntry = null;
  

public String getUserUcasScore() {
	return userUcasScore;
  }

  public void setUserUcasScore(String userUcasScore) {
	this.userUcasScore = userUcasScore;
  }

public String getSponsoredOrderItemId() {
	return sponsoredOrderItemId;
}


public void setSponsoredOrderItemId(String sponsoredOrderItemId) {
	this.sponsoredOrderItemId = sponsoredOrderItemId;
}


public String getLatitude() {
	return latitude;
}


public void setLatitude(String latitude) {
	this.latitude = latitude;
}


public String getLongitude() {
	return longitude;
}


public void setLongitude(String longitude) {
	this.longitude = longitude;
}


public String getRemovedUni() {
	return removedUni;
}


public void setRemovedUni(String removedUni) {
	this.removedUni = removedUni;
}


public Object[][] getCollegeConsentArray() {
	return collegeConsentArray;
}


public void setCollegeConsentArray(Object[][] collegeConsentArray) {
	this.collegeConsentArray = collegeConsentArray;
}


public String getCollegeIdLists() {
	return collegeIdLists;
}


public void setCollegeIdLists(String collegeIdLists) {
	this.collegeIdLists = collegeIdLists;
}


public String getConsentFlagValues() {
	return consentFlagValues;
}


public void setConsentFlagValues(String consentFlagValues) {
	this.consentFlagValues = consentFlagValues;
}


public String getConsentFlagCheck() {
	return consentFlagCheck;
}


public void setConsentFlagCheck(String consentFlagCheck) {
	this.consentFlagCheck = consentFlagCheck;
}


public String getConsentFlag() {
	return consentFlag;
}


public String getConsentDate() {
	return consentDate;
}


public void setConsentDate(String consentDate) {
	this.consentDate = consentDate;
}


public void setConsentFlag(String consentFlag) {
	this.consentFlag = consentFlag;
}


public QuestionAnswerBean getQuestionAnswerBean(int index)
  {
    
    int size = questionAnswers.size();
    //Check if object exists at specified index
    if(index+1>size)
    {
      //adding objects
      for (int j = size; j < index + 1; j++)
      {
        QuestionAnswerBean bean = new QuestionAnswerBean();
        questionAnswers.add(bean);
      }
    }
    return (QuestionAnswerBean)questionAnswers.get(index);
  }
  
  
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setNonukresident(boolean nonukresident) {
    this.nonukresident = nonukresident;
  }

  public boolean isNonukresident() {
    return nonukresident;
  }


  public void setAboutMe(String aboutMe) {
    this.aboutMe = aboutMe;
  }

  public String getAboutMe() {
    return aboutMe;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return gender;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getNationality() {
    return nationality;
  }


  public void setNationalityDesc(String nationalityDesc) {
    this.nationalityDesc = nationalityDesc;
  }

  public String getNationalityDesc() {
    return nationalityDesc;
  }

  public void setNationalityValue(String nationalityValue) {
    this.nationalityValue = nationalityValue;
  }

  public String getNationalityValue() {
    return nationalityValue;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }


  public void setNewsLetter(String newsLetter) {
    this.newsLetter = newsLetter;
  }

  public String getNewsLetter() {
    return newsLetter;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }


  public void setStudyLevelList(List studyLevelList) {
    this.studyLevelList = studyLevelList;
  }

  public List getStudyLevelList() {
    return studyLevelList;
  }

  public void setStudyLevelId(String studyLevelId) {
    this.studyLevelId = studyLevelId;
  }

  public String getStudyLevelId() {
    return studyLevelId;
  }

  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }

  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }

  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }

  public String getCourseId() {
    return courseId;
  }

  public void setCourseName(String courseName) {
    this.courseName = courseName;
  }

  public String getCourseName() {
    return courseName;
  }

  public void setSubjectId(String subjectId) {
    this.subjectId = subjectId;
  }

  public String getSubjectId() {
    return subjectId;
  }


  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public String getAddress1() {
    return address1;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public String getAddress2() {
    return address2;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public String getTown() {
    return town;
  }


  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getSubject() {
    return subject;
  }


  public void setCpeQualificationNetworkId(String cpeQualificationNetworkId) {
    this.cpeQualificationNetworkId = cpeQualificationNetworkId;
  }

  public String getCpeQualificationNetworkId() {
    return cpeQualificationNetworkId;
  }

  public void setTypeOfEnquiry(String typeOfEnquiry) {
    this.typeOfEnquiry = typeOfEnquiry;
  }

  public String getTypeOfEnquiry() {
    return typeOfEnquiry;
  }

  public void setEmailContent(String emailContent) {
    this.emailContent = emailContent;
  }

  public String getEmailContent() {
    return emailContent;
  }

  public void setNationalityList(List nationalityList) {
    this.nationalityList = nationalityList;
  }

  public List getNationalityList() {
    return nationalityList;
  }

  public void setYeartoJoinCourse(String yeartoJoinCourse) {
    this.yeartoJoinCourse = yeartoJoinCourse;
  }

  public String getYeartoJoinCourse() {
    return yeartoJoinCourse;
  }

  public void setQlFlag(String qlFlag) {
    this.qlFlag = qlFlag;
  }

  public String getQlFlag() {
    return qlFlag;
  }

  public void setTac(boolean tac) {
    this.tac = tac;
  }

  public boolean isTac() {
    return tac;
  }

  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public void setQuestionAnswers(List questionAnswers) {
    this.questionAnswers = questionAnswers;
  }

  public List getQuestionAnswers() {
    return questionAnswers;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserId() {
    return userId;
  }

  public void setHtmlId(String htmlId) {
    this.htmlId = htmlId;
  }

  public String getHtmlId() {
    return htmlId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setClientBrowser(String clientBrowser) {
    this.clientBrowser = clientBrowser;
  }

  public String getClientBrowser() {
    return clientBrowser;
  }

  public void setClientIp(String clientIp) {
    this.clientIp = clientIp;
  }

  public String getClientIp() {
    return clientIp;
  }

  public void setRequestUrl(String requestUrl) {
    this.requestUrl = requestUrl;
  }

  public String getRequestUrl() {
    return requestUrl;
  }

  public void setRefferalUrl(String refferalUrl) {
    this.refferalUrl = refferalUrl;
  }

  public String getRefferalUrl() {
    return refferalUrl;
  }

  public void setWidgetId(String widgetId) {
    this.widgetId = widgetId;
  }

  public String getWidgetId() {
    return widgetId;
  }

  public void setAffliatedId(String affliatedId) {
    this.affliatedId = affliatedId;
  }

  public String getAffliatedId() {
    return affliatedId;
  }

  public void setTypeOfProspectus(String typeOfProspectus) {
    this.typeOfProspectus = typeOfProspectus;
  }

  public String getTypeOfProspectus() {
    return typeOfProspectus;
  }
  
  public void setUserIP(String userIP) {
    this.userIP = userIP;
  }
  
  public String getUserIP() {
    return userIP;
  }
  
  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }
  
  public String getUserAgent() {
    return userAgent;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }
  
  public String getMessage() {
    return message;
  }
  
  public void setNetworkId(String networkId) {
    this.networkId = networkId;
  }
  
  public String getNetworkId() {
    return networkId;
  }
  
  public void setEnquiryId(String enquiryId) {
    this.enquiryId = enquiryId;
  }
  
  public String getEnquiryId() {
    return enquiryId;
  }

  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request)
  {  
    //Setting the menu related entries in the request scoope
  /*  request.setAttribute(CMIConstants.NAVIGATION_NAME,AccessConstants.MAIN_MENU_COURSES_PROGRAMS);
    
    if("Add".equals(courseAction))
    {
        request.setAttribute(CMIConstants.SUB_NAVIGATION_NAME,AccessConstants.ADD_A_COURSE);     
    }
    else
    {   
        request.setAttribute(CMIConstants.SUB_NAVIGATION_NAME,AccessConstants.VIEW_ALL_COURSES);     
    }
    request.setAttribute(CMIConstants.NO_LEFT_COLUMN, "true");
    request.setAttribute(CMIConstants.NO_CONTENT_COLUMN, "true");
    request.setAttribute(CMIConstants.NO_RIGHT_COLUMN, "true");*/
    return null;
  }
  public void setProfileType(String profileType) {
    this.profileType = profileType;
  }
  public String getProfileType() {
    return profileType;
  }

  public void setLoggedUserId(String loggedUserId) {
    this.loggedUserId = loggedUserId;
  }

  public String getLoggedUserId() {
    return loggedUserId;
  }
  public void setStudyLevelDesc(String studyLevelDesc)
  {
    this.studyLevelDesc = studyLevelDesc;
  }
  public String getStudyLevelDesc()
  {
    return studyLevelDesc;
  }

  public void setPostEnquiryList(List postEnquiryList) {
    this.postEnquiryList = postEnquiryList;
  }

  public List getPostEnquiryList() {
    return postEnquiryList;
  }
  public void setPostButtonFlag(String postButtonFlag) {
    this.postButtonFlag = postButtonFlag;
  }
  public String getPostButtonFlag() {
    return postButtonFlag;
  }
  public void setDateList(List dateList) {
    this.dateList = dateList;
  }
  public List getDateList() {
    return dateList;
  }
  public void setMonthList(List monthList) {
    this.monthList = monthList;
  }
  public List getMonthList() {
    return monthList;
  }
  public void setYearList(List yearList) {
    this.yearList = yearList;
  }
  public List getYearList() {
    return yearList;
  }
  public void setDate(String date) {
    this.date = date;
  }
  public String getDate() {
    return date;
  }
  public void setMonth(String month) {
    this.month = month;
  }
  public String getMonth() {
    return month;
  }
  public void setYear(String year) {
    this.year = year;
  }
  public String getYear() {
    return year;
  }
  public void setMobileFlag(String mobileFlag) {
    this.mobileFlag = mobileFlag;
  }
  public String getMobileFlag() {
    return mobileFlag;
  }
  public void setCountryOfRes(List countryOfRes) {
    this.countryOfRes = countryOfRes;
  }
  public List getCountryOfRes() {
    return countryOfRes;
  }
  public void setCountryOfResidence(String countryOfResidence) {
    this.countryOfResidence = countryOfResidence;
  }
  public String getCountryOfResidence() {
    return countryOfResidence;
  }

  public void setOneClickCheckBox(String oneClickCheckBox)
  {
    this.oneClickCheckBox = oneClickCheckBox;
  }

  public String getOneClickCheckBox()
  {
    return oneClickCheckBox;
  }

  public void setOneClickMandatory(String oneClickMandatory)
  {
    this.oneClickMandatory = oneClickMandatory;
  }

  public String getOneClickMandatory()
  {
    return oneClickMandatory;
  }

  public void setPredictedGrades(String predictedGrades)
  {
    this.predictedGrades = predictedGrades;
  }

  public String getPredictedGrades()
  {
    return predictedGrades;
  }

  public void setCountryOfResDesc(String countryOfResDesc)
  {
    this.countryOfResDesc = countryOfResDesc;
  }

  public String getCountryOfResDesc()
  {
    return countryOfResDesc;
  }

  public void setAutoEmailFlag(String autoEmailFlag)
  {
    this.autoEmailFlag = autoEmailFlag;
  }

  public String getAutoEmailFlag()
  {
    return autoEmailFlag;
  }

  public void setPostEnquiry(String postEnquiry)
  {
    this.postEnquiry = postEnquiry;
  }

  public String getPostEnquiry()
  {
    return postEnquiry;
  }
  public void setTrackSessionId(String trackSessionId) {
    this.trackSessionId = trackSessionId;
  }
  public String getTrackSessionId() {
    return trackSessionId;
  }
  public void setBulkProspectusType(String bulkProspectusType) {
    this.bulkProspectusType = bulkProspectusType;
  }
  public String getBulkProspectusType() {
    return bulkProspectusType;
  }

  public void setEnquiryBasketId(String enquiryBasketId) {
    this.enquiryBasketId = enquiryBasketId;
  }
  public String getEnquiryBasketId() {
    return enquiryBasketId;
  }
  public void setEnquiryType(String enquiryType) {
    this.enquiryType = enquiryType;
  }
  public String getEnquiryType() {
    return enquiryType;
  }
  public void setCountryOfResi(String countryOfResi) {
    this.countryOfResi = countryOfResi;
  }
  public String getCountryOfResi() {
    return countryOfResi;
  }
  public void setGradeType(String gradeType) {
    this.gradeType = gradeType;
  }
  public String getGradeType() {
    return gradeType;
  }
  public void setGradeValue(String gradeValue) {
    this.gradeValue = gradeValue;
  }
  public String getGradeValue() {
    return gradeValue;
  }
  public void setProsnonukresident(String prosnonukresident) {
    this.prosnonukresident = prosnonukresident;
  }
  public String getProsnonukresident() {
    return prosnonukresident;
  }
  public void setProspectusCountryOfRes(List prospectusCountryOfRes) {
    this.prospectusCountryOfRes = prospectusCountryOfRes;
  }
  public List getProspectusCountryOfRes() {
    return prospectusCountryOfRes;
  }
 
  public void setProsCountryOfRes(String prosCountryOfRes) {
    this.prosCountryOfRes = prosCountryOfRes;
  }
  public String getProsCountryOfRes() {
    return prosCountryOfRes;
  }
  public void setProsCountryOfResDesc(String prosCountryOfResDesc) {
    this.prosCountryOfResDesc = prosCountryOfResDesc;
  }
  public String getProsCountryOfResDesc() {
    return prosCountryOfResDesc;
  }
  public void setProsPredictedGrades(List prosPredictedGrades) {
    this.prosPredictedGrades = prosPredictedGrades;
  }
  public List getProsPredictedGrades() {
    return prosPredictedGrades;
  }
  public void setProsGradesList(List prosGradesList) {
    this.prosGradesList = prosGradesList;
  }
  public List getProsGradesList() {
    return prosGradesList;
  }
  public void setProsGradePointSelected(String prosGradePointSelected) {
    this.prosGradePointSelected = prosGradePointSelected;
  }
  public String getProsGradePointSelected() {
    return prosGradePointSelected;
  }
  public void setProsGradeValueSelected(String prosGradeValueSelected) {
    this.prosGradeValueSelected = prosGradeValueSelected;
  }
  public String getProsGradeValueSelected() {
    return prosGradeValueSelected;
  }
  public void setProsDoNotGradesChkBox(String prosDoNotGradesChkBox) {
    this.prosDoNotGradesChkBox = prosDoNotGradesChkBox;
  }
  public String getProsDoNotGradesChkBox() {
    return prosDoNotGradesChkBox;
  }

  public void setMarketingEmail(String marketingEmail) {
    this.marketingEmail = marketingEmail;
  }

  public String getMarketingEmail() {
    return marketingEmail;
  }

  public void setSolusEmail(String solusEmail) {
    this.solusEmail = solusEmail;
  }

  public String getSolusEmail() {
    return solusEmail;
  }

  public void setSurveyEmail(String surveyEmail) {
    this.surveyEmail = surveyEmail;
  }

  public String getSurveyEmail() {
    return surveyEmail;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPassword() {
    return password;
  }

  public void setScreenWidth(String screenWidth) {
    this.screenWidth = screenWidth;
  }

  public String getScreenWidth() {
    return screenWidth;
  }


  public HttpServletRequest getRequest() {
	return request;
  }


  public void setRequest(HttpServletRequest request) {
	this.request = request;
  }


  public String getDefaultYoe() {
	return defaultYoe;
  }


  public void setDefaultYoe(String defaultYoe) {
	this.defaultYoe = defaultYoe;
  }


  public String getPostCodeMessage() {
	return postCodeMessage;
  }


  public void setPostCodeMessage(String postCodeMessage) {
	this.postCodeMessage = postCodeMessage;
  }

  public String getYearOfEntry() {
	return yearOfEntry;
  }

  public void setYearOfEntry(String yearOfEntry) {
	this.yearOfEntry = yearOfEntry;
  }

}
