package com.wuni.advertiser.ql.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class QuestionAnswerBean{

  private String orderAttributeId = null;
  private String attributeId = null;
  private String typeAttributeId = null;
  private String attributeName = null;
  private String displaySeq = null;
  private String mandatory = null;
  private String defaultValue = null;
  private String attributeText = null;
  private String optionFlag = null;
  private List options = null;
  private String optionId = null;
  private String optionDesc = null;
  private String optionValue = null;
  private String maxLength = null;
  private String formatDesc = null;
  private String dataTypeDesc = null;
  private String groupId = null;
  private String helpFlag = null;
  private String helpText = null;
  private String attributeValueId = null;
  private String attributeValue = null;
  private String stepNum = null;
  private String existEnqFlag = null;
  private String existUserFlag = null;
  private String errorDesc = null;
  private String attrValueSeq = null;
  private String contextPath = null;
  private String className = null;
  private String deptAttrList = null;
  private String displayFlag = null;  
  private String courseTitle = null;
  private String collegeNameDisplay = null;
  private String collegeLogo = null;
  private String entryPoints = null;
  private String description = null;
  private String durationDesc = null;
  private String shortText = null;
  private String startDate = null;
  private String ukFees = null;
  private String ukFeesDesc = null;
  private String intlFees = null;
  private String intlFeesDesc = null;
  private String euFees = null;
  private String euFeesDesc = null;
  private String regionalFees = null;
  private String regionalFeesDesc = null;  
  private String overallRatingExact = null;
  private String overallRating = null;
  private String timesRanking = null;
  private String positionId = null;
  private String totalCnt = null;
  private String collegeLogPath = null; 
  private String feesExistsFlag = null;
  private String engFees = null;
  private String scotFees = null;
  private String walesFees = null;
  private String northIreFees = null;
  private String domesticFees = null;
  private String otherUKFees = null;
  
  public void setOrderAttributeId(String orderAttributeId) {
    this.orderAttributeId = orderAttributeId;
  }

  public String getOrderAttributeId() {
    return orderAttributeId;
  }

  public void setAttributeId(String attributeId) {
    this.attributeId = attributeId;
  }

  public String getAttributeId() {
    return attributeId;
  }

  public void setAttributeName(String attributeName) {
    this.attributeName = attributeName;
  }

  public String getAttributeName() {
    return attributeName;
  }

  public void setDisplaySeq(String displaySeq) {
    this.displaySeq = displaySeq;
  }

  public String getDisplaySeq() {
    return displaySeq;
  }

  public void setMandatory(String mandatory) {
    this.mandatory = mandatory;
  }

  public String getMandatory() {
    return mandatory;
  }

  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }

  public String getDefaultValue() {
    return defaultValue;
  }

  public void setAttributeText(String attributeText) {
    this.attributeText = attributeText;
  }

  public String getAttributeText() {
    return attributeText;
  }

  public void setOptionFlag(String optionFlag) {
    this.optionFlag = optionFlag;
  }

  public String getOptionFlag() {
    return optionFlag;
  }

  public void setOptions(List options) {
    this.options = options;
  }

  public List getOptions() {
    return options;
  }

  public void setOptionId(String optionId) {
    this.optionId = optionId;
  }

  public String getOptionId() {
    return optionId;
  }

  public void setOptionDesc(String optionDesc) {
    this.optionDesc = optionDesc;
  }

  public String getOptionDesc() {
    return optionDesc;
  }

  public void setOptionValue(String optionValue) {
    this.optionValue = optionValue;
  }

  public String getOptionValue() {
    return optionValue;
  }

  public void setMaxLength(String maxLength) {
    this.maxLength = maxLength;
  }

  public String getMaxLength() {
    return maxLength;
  }

  public void setFormatDesc(String formatDesc) {
    this.formatDesc = formatDesc;
  }

  public String getFormatDesc() {
    return formatDesc;
  }

  public void setDataTypeDesc(String dataTypeDesc) {
    this.dataTypeDesc = dataTypeDesc;
  }

  public String getDataTypeDesc() {
    return dataTypeDesc;
  }

  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }

  public String getGroupId() {
    return groupId;
  }

  public void setHelpFlag(String helpFlag) {
    this.helpFlag = helpFlag;
  }

  public String getHelpFlag() {
    return helpFlag;
  }

  public void setHelpText(String helpText) {
    this.helpText = helpText;
  }

  public String getHelpText() {
    return helpText;
  }

  public void setAttributeValueId(String attributeValueId) {
    this.attributeValueId = attributeValueId;
  }

  public String getAttributeValueId() {
    return attributeValueId;
  }

  public void setAttributeValue(String attributeValue) {
    this.attributeValue = attributeValue;
  }

  public String getAttributeValue() {
    return attributeValue;
  }

  public void setStepNum(String stepNum) {
    this.stepNum = stepNum;
  }

  public String getStepNum() {
    return stepNum;
  }

  public void setExistEnqFlag(String existEnqFlag) {
    this.existEnqFlag = existEnqFlag;
  }

  public String getExistEnqFlag() {
    return existEnqFlag;
  }

  public void setExistUserFlag(String existUserFlag) {
    this.existUserFlag = existUserFlag;
  }

  public String getExistUserFlag() {
    return existUserFlag;
  }

  public void setErrorDesc(String errorDesc) {
    this.errorDesc = errorDesc;
  }

  public String getErrorDesc() {
    return errorDesc;
  }

  public void setAttrValueSeq(String attrValueSeq) {
    this.attrValueSeq = attrValueSeq;
  }

  public String getAttrValueSeq() {
    return attrValueSeq;
  }

  public void setContextPath(String contextPath) {
    this.contextPath = contextPath;
  }

  public String getContextPath() {
    return contextPath;
  }

  public void setClassName(String className) {
    this.className = className;
  }

  public String getClassName() {
    return className;
  }

  public void setDeptAttrList(String deptAttrList) {
    this.deptAttrList = deptAttrList;
  }

  public String getDeptAttrList() {
    return deptAttrList;
  }

  public void setTypeAttributeId(String typeAttributeId) {
    this.typeAttributeId = typeAttributeId;
  }

  public String getTypeAttributeId() {
    return typeAttributeId;
  }

  public void setDisplayFlag(String displayFlag)
  {
    this.displayFlag = displayFlag;
  }

  public String getDisplayFlag()
  {
    return displayFlag;
  }
  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }
  public String getCourseTitle() {
    return courseTitle;
  }
  public void setCollegeNameDisplay(String collegeNameDisplay) {
    this.collegeNameDisplay = collegeNameDisplay;
  }
  public String getCollegeNameDisplay() {
    return collegeNameDisplay;
  }
  public void setCollegeLogo(String collegeLogo) {
    this.collegeLogo = collegeLogo;
  }
  public String getCollegeLogo() {
    return collegeLogo;
  }
  public void setEntryPoints(String entryPoints) {
    this.entryPoints = entryPoints;
  }
  public String getEntryPoints() {
    return entryPoints;
  }
  public void setDescription(String description) {
    this.description = description;
  }
  public String getDescription() {
    return description;
  }
  public void setDurationDesc(String durationDesc) {
    this.durationDesc = durationDesc;
  }
  public String getDurationDesc() {
    return durationDesc;
  }
  public void setShortText(String shortText) {
    this.shortText = shortText;
  }
  public String getShortText() {
    return shortText;
  }
  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }
  public String getStartDate() {
    return startDate;
  }
  public void setUkFees(String ukFees) {
    this.ukFees = ukFees;
  }
  public String getUkFees() {
    return ukFees;
  }
  public void setUkFeesDesc(String ukFeesDesc) {
    this.ukFeesDesc = ukFeesDesc;
  }
  public String getUkFeesDesc() {
    return ukFeesDesc;
  }
  public void setIntlFees(String intlFees) {
    this.intlFees = intlFees;
  }
  public String getIntlFees() {
    return intlFees;
  }
  public void setIntlFeesDesc(String intlFeesDesc) {
    this.intlFeesDesc = intlFeesDesc;
  }
  public String getIntlFeesDesc() {
    return intlFeesDesc;
  }
  public void setEuFees(String euFees) {
    this.euFees = euFees;
  }
  public String getEuFees() {
    return euFees;
  }
  public void setEuFeesDesc(String euFeesDesc) {
    this.euFeesDesc = euFeesDesc;
  }
  public String getEuFeesDesc() {
    return euFeesDesc;
  }
  public void setRegionalFees(String regionalFees) {
    this.regionalFees = regionalFees;
  }
  public String getRegionalFees() {
    return regionalFees;
  }
  public void setRegionalFeesDesc(String regionalFeesDesc) {
    this.regionalFeesDesc = regionalFeesDesc;
  }
  public String getRegionalFeesDesc() {
    return regionalFeesDesc;
  }
  public void setOverallRatingExact(String overallRatingExact) {
    this.overallRatingExact = overallRatingExact;
  }
  public String getOverallRatingExact() {
    return overallRatingExact;
  }
  public void setOverallRating(String overallRating) {
    this.overallRating = overallRating;
  }
  public String getOverallRating() {
    return overallRating;
  }
  public void setTimesRanking(String timesRanking) {
    this.timesRanking = timesRanking;
  }
  public String getTimesRanking() {
    return timesRanking;
  }
  public void setPositionId(String positionId) {
    this.positionId = positionId;
  }
  public String getPositionId() {
    return positionId;
  }
  public void setTotalCnt(String totalCnt) {
    this.totalCnt = totalCnt;
  }
  public String getTotalCnt() {
    return totalCnt;
  }
  public void setCollegeLogPath(String collegeLogPath) {
    this.collegeLogPath = collegeLogPath;
  }
  public String getCollegeLogPath() {
    return collegeLogPath;
  }
  public void setFeesExistsFlag(String feesExistsFlag) {
    this.feesExistsFlag = feesExistsFlag;
  }
  public String getFeesExistsFlag() {
    return feesExistsFlag;
  }

    public void setEngFees(String engFees) {
        this.engFees = engFees;
    }

    public String getEngFees() {
        return engFees;
    }

    public void setScotFees(String scotFees) {
        this.scotFees = scotFees;
    }

    public String getScotFees() {
        return scotFees;
    }

    public void setWalesFees(String walesFees) {
        this.walesFees = walesFees;
    }

    public String getWalesFees() {
        return walesFees;
    }

    public void setNorthIreFees(String northIreFees) {
        this.northIreFees = northIreFees;
    }

    public String getNorthIreFees() {
        return northIreFees;
    }

    public void setDomesticFees(String domesticFees) {
        this.domesticFees = domesticFees;
    }

    public String getDomesticFees() {
        return domesticFees;
    }

    public void setOtherUKFees(String otherUKFees) {
        this.otherUKFees = otherUKFees;
    }

    public String getOtherUKFees() {
        return otherUKFees;
    }
}
