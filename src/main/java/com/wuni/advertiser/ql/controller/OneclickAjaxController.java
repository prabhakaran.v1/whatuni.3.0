/*
 * Added By Sekhar K for wu417_20121030 for WU QL
 */
package com.wuni.advertiser.ql.controller;

import WUI.search.form.SearchBean;
import WUI.utilities.AdvertUtilities;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.advertiser.prospectus.form.ProspectusBean;
import com.wuni.advertiser.ql.QLCommon;
import com.wuni.advertiser.ql.form.QLFormBean;
import com.wuni.token.TokenFormController;
import com.wuni.util.uni.CollegeUtilities;
import com.wuni.util.valueobject.CollegeNamesVO;
import com.wuni.util.valueobject.ql.BasicFormVO;
import com.wuni.util.valueobject.ql.ClientLeadConsentVO;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.util.LabelValueBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class OneclickAjaxController{

  @RequestMapping(value="/oneclickajaxURL", method=RequestMethod.POST)	
  public ModelAndView oneClickAjaxURL(QLFormBean qlFormBean, HttpServletRequest request, HttpServletResponse response) throws Exception {

    String userID = new SessionData().getData(request, "y");
    HttpSession session = request.getSession();
    ServletContext context = request.getServletContext();
    String collegeId = request.getParameter("p_college_id");
    String courseId = request.getParameter("p_course_id");
    String subOrderItemId = request.getParameter("p_suborder_item");
    String subject = request.getParameter("p_subject");
    String typeOfenquiry = request.getParameter("type");
    String consentFlagCheck = request.getParameter("consentFlagCheck");
    String consentFlagOneClick = request.getParameter("dataShareFlag");
    qlFormBean.setPostEnquiry(GenericValidator.isBlankOrNull(request.getParameter("postEnquiry")) ? "" : "P");
    String cpeValue = new WUI.utilities.CommonFunction().getGACPEPrice(subOrderItemId, "email");
    //new GlobalFunction().getInteractivePodInfo(collegeId, request);
    CommonFunction common = new CommonFunction();
    SessionData sessiondata = new SessionData();
    String collegeName = common.getCollegeName(collegeId, request).toLowerCase();
    String gaCollegeName = common.replaceSpecialCharacter(collegeName);
    String prospectusLBNew = StringUtils.isNotBlank(request.getParameter("prospectusLBNew")) ? request.getParameter("prospectusLBNew") : "";
    qlFormBean.setCollegeId(collegeId);
    qlFormBean.setCourseId(courseId);
    qlFormBean.setSubject(subject);
    qlFormBean.setSubOrderItemId(subOrderItemId);
    if("EMAIL".equalsIgnoreCase(typeOfenquiry)){
      qlFormBean.setTypeOfEnquiry("send-college-email");
    }else if("RP".equalsIgnoreCase(typeOfenquiry)){
      qlFormBean.setTypeOfEnquiry("order-prospectus");
    }else if("DP".equalsIgnoreCase(typeOfenquiry)){
      qlFormBean.setTypeOfEnquiry("download-prospectus");
    }
    List inputList = new ArrayList();
    inputList.add(userID);
    inputList.add(qlFormBean.getCollegeId());
    inputList.add(qlFormBean.getSubOrderItemId());
    inputList.add(qlFormBean.getCourseId());  
    getBasicFormData(qlFormBean, sessiondata, inputList, request);
    String consentFlag = (String)request.getAttribute("consentFlag");//Added this for consent flag changes by Hema.S on 17_12_19_REL
    request.setAttribute("consentFlagCheck", consentFlagCheck);
    request.setAttribute("consentFlagOneClick", consentFlagOneClick);
     if(!GenericValidator.isBlankOrNull(userID) && !"0".equalsIgnoreCase(userID)){
       if(!"SP".equalsIgnoreCase(qlFormBean.getProfileType()) && "Y".equalsIgnoreCase(qlFormBean.getOneClickMandatory())){
         if("Y".equalsIgnoreCase(qlFormBean.getOneClickCheckBox())){
        	new TokenFormController().saveToken(request);
            String token = ""+session.getAttribute("_synchronizerToken");  
            String newsLetterFlag = "Y".equalsIgnoreCase(qlFormBean.getNewsLetter()) ? "Y" : "N";
            String enquiryExistFlag = !GenericValidator.isBlankOrNull((String)request.getAttribute("enquiryExistFlag")) ? (String)request.getAttribute("enquiryExistFlag") : "N";
            setResponseText(response, "NOBOX" + "##SPLIT##" + newsLetterFlag + "##SPLIT##" + qlFormBean.getTypeOfEnquiry() + "##SPLIT##" + gaCollegeName +"##SPLIT##" +cpeValue + "##SPLIT##" + token + "##SPLIT##" + enquiryExistFlag+"##SPLIT##"+consentFlag);
            return null;
         }else{
          new TokenFormController().saveToken(request);
          qlFormBean.setOneClickCheckBox("Y");
          if("RP".equalsIgnoreCase(typeOfenquiry)){
        	  setResponseText(response, "##SPLIT##"+"BOX"+"##SPLIT##"+consentFlag);
          }else{
        	  setResponseText(response, "##SPLIT##"+"BOX");
          }
          
          if(StringUtils.equalsIgnoreCase(prospectusLBNew, "prospectus_lb_new")) {
        	return new ModelAndView("oneClickEnquiryLBNew", "qlformbean", qlFormBean); 
          }else {
        	return new ModelAndView("oneclickbox", "qlformbean", qlFormBean);   
          }
        }
       }else
       {
         if("send-college-email".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
           setResponseText(response, "EMAIL" + "##SPLIT##");
         }else{
           setResponseText(response, "EXIST" + "##SPLIT##");
         }
         return null;         
       }

     }else
     {
       if("send-college-email".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
         setResponseText(response, "EMAIL" + "##SPLIT##");
       }else{
         setResponseText(response, "EXIST" + "##SPLIT##");
       }
       return null;
     }
  }
 
  /**
    * This function is used to get the basic form related data
    * @param qlFormBean
    * @param sessiondata
    * @param inputList
    * @param request    
    */
  private void getBasicFormData(QLFormBean qlFormBean, SessionData sessiondata, List inputList, HttpServletRequest request) {
    String session_user_id = sessiondata.getData(request, "y");
    if(session_user_id == null || "0".equalsIgnoreCase(session_user_id)){
      qlFormBean.setUserId(qlFormBean.getUserId());
    }else{
      qlFormBean.setUserId(session_user_id);
    }
    String collegeId = qlFormBean.getCollegeId();
    String courseId = qlFormBean.getCourseId();
    String subOrderItemId = qlFormBean.getSubOrderItemId();
    String subject = qlFormBean.getSubject();
    String typeOfEnquiry = qlFormBean.getTypeOfEnquiry();
    String htmlId = "";
    if("send-college-email".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
      htmlId = "WU_COLLEGE_EMAIL";
    }else if("order-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
      htmlId = "WU_COLLEGE_PROSPECTUS_EMAIL";
    }else if("download-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
      htmlId = "WU_COLLEGE_DOWNLOADPROSPECTUS_EMAIL";
    }    
    qlFormBean.setHtmlId(htmlId);
    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
    
    Map BasicFormInfoMap = commonBusiness.getQlBasicFormInfo(qlFormBean);
    if (session_user_id != null && !session_user_id.equalsIgnoreCase("null") && !session_user_id.equals("0") && session_user_id.trim().length() > 0) {
      ArrayList userInfoList = (ArrayList)BasicFormInfoMap.get("PC_USER_DETAILS");
      if(userInfoList != null && userInfoList.size() > 0){
        BasicFormVO basicformvo = new BasicFormVO();
        basicformvo = (BasicFormVO)userInfoList.get(0);
        qlFormBean.setEmailAddress(basicformvo.getEmailAddress());
        qlFormBean.setFirstName(basicformvo.getFirstName());
        qlFormBean.setLastName(basicformvo.getLastName());
        qlFormBean.setPostCode(basicformvo.getPostCode());
        qlFormBean.setYeartoJoinCourse(basicformvo.getYeartoJoinCourse());
        qlFormBean.setNationality(basicformvo.getNationality());
        qlFormBean.setDateOfBirth(basicformvo.getDateOfBirth());
        qlFormBean.setNewsLetter(basicformvo.getNewsLetter());
        qlFormBean.setDefaultYoe(basicformvo.getDefaultYoe());
        if(!GenericValidator.isBlankOrNull(basicformvo.getNationality())) {
          qlFormBean.setNonukresident(true);
        }        
        if(!GenericValidator.isBlankOrNull(session_user_id) && !GenericValidator.isBlankOrNull(qlFormBean.getEmailAddress())){
          request.setAttribute("isExistsBasicFormEmail", "YES");
        }        
      }
      ArrayList userAddressInfo = (ArrayList)BasicFormInfoMap.get("PC_USER_ADDRESS");
      if(userAddressInfo != null && userAddressInfo.size() > 0){
        BasicFormVO basicformvo = new BasicFormVO();
        basicformvo = (BasicFormVO)userAddressInfo.get(0);
        qlFormBean.setAddress1(basicformvo.getAddress1());
        qlFormBean.setAddress2(basicformvo.getAddress2()); 
        qlFormBean.setTown(basicformvo.getTown());
        qlFormBean.setCountryOfResidence(basicformvo.getCountryOfResId());
      }
    }
    //
    String enquiryExistFlag = (String)BasicFormInfoMap.get("P_ENQUIRY_EXIST_FLAG");
    if(!GenericValidator.isBlankOrNull(enquiryExistFlag)){
      request.setAttribute("enquiryExistFlag", enquiryExistFlag);
    }
    //
    String qlFlag = String.valueOf(BasicFormInfoMap.get("P_QL_FLAG"));
    String studyLevelType = String.valueOf(BasicFormInfoMap.get("p_study_level_type"));    
    if(!GenericValidator.isBlankOrNull(courseId) && !"0".equalsIgnoreCase(courseId)){
      ArrayList coursePodList = (ArrayList)BasicFormInfoMap.get("PC_COURSE_DETAILS");
      if(coursePodList != null && coursePodList.size() > 0){
        BasicFormVO basicFormVO= (BasicFormVO)coursePodList.get(0);        
        qlFormBean.setStudyLevelId(basicFormVO.getStudyLevelCode());
        basicFormVO.setCourseDesc(!GenericValidator.isBlankOrNull(basicFormVO.getCourseDesc())? basicFormVO.getCourseDesc(): "");
        basicFormVO.setTutionFees(GenericValidator.isBlankOrNull(basicFormVO.getTutionFees()) ? GenericValidator.isBlankOrNull(basicFormVO.getTutionFeesDesc())?  "" : basicFormVO.getTutionFeesDesc() : basicFormVO.getTutionFees());
        basicFormVO.setUcasTariff(GenericValidator.isBlankOrNull(basicFormVO.getUcasTariff()) ? GenericValidator.isBlankOrNull(basicFormVO.getEntryRequirements())?  "" : basicFormVO.getEntryRequirements() : basicFormVO.getUcasTariff());
        basicFormVO.setDuration(GenericValidator.isBlankOrNull(basicFormVO.getDuration()) ? GenericValidator.isBlankOrNull(basicFormVO.getDurationDesc())?  "" : basicFormVO.getDurationDesc() : basicFormVO.getDuration());
        coursePodList.set(0,basicFormVO);
        request.setAttribute("coursePodInfo", coursePodList);
      }
    }else{
      if("UG".equalsIgnoreCase(studyLevelType)){
        qlFormBean.setStudyLevelId("M");
        
      }else if("PG".equalsIgnoreCase(studyLevelType)){
        qlFormBean.setStudyLevelId("L");
      }
    }
    ArrayList nationalityList = (ArrayList)BasicFormInfoMap.get("pc_nationality");
    if(nationalityList != null && nationalityList.size() > 0){
      List nationality = (ArrayList)nationalityList.get(0);
      qlFormBean.setNationalityList(nationality);
      if(!GenericValidator.isBlankOrNull(qlFormBean.getNationality())) //wu516_19112013 oneclick enquiry changes
      {
        Iterator natList = nationality.iterator();
        BasicFormVO basicformvo1 = new BasicFormVO();
        while(natList.hasNext())
        {
          basicformvo1 = (BasicFormVO) natList.next();
          if(qlFormBean.getNationality().equalsIgnoreCase(basicformvo1.getNationalityValue()))
          {
            qlFormBean.setNationalityDesc(basicformvo1.getNationalityDesc());
            break;
          }
        }
      }
    }
    ArrayList courntyOfRes = (ArrayList)BasicFormInfoMap.get("pc_country_of_res");
    if(courntyOfRes != null && courntyOfRes.size() > 0){
      if(!GenericValidator.isBlankOrNull(qlFormBean.getCountryOfResidence())) //wu516_19112013 oneclick enquiry changes
      {
        Iterator countryList = courntyOfRes.iterator();
        BasicFormVO basicformvo2 = new BasicFormVO();
        while(countryList.hasNext())
        {
          basicformvo2 = (BasicFormVO) countryList.next();
          if(qlFormBean.getCountryOfResidence().equalsIgnoreCase(basicformvo2.getNationalityValue()))
          {
            qlFormBean.setCountryOfResDesc(basicformvo2.getNationalityDesc());
            break;
          }
        }
      }      
    }     
    
    List studyLevelList = new CommonFunction().getStudyLevel();
    if(studyLevelList != null && studyLevelList.size()>0) { 
      Iterator listItr = studyLevelList.iterator();      
      SearchBean searchBean = null;
      while(listItr.hasNext()) {
        searchBean = (SearchBean) listItr.next();
        if(!GenericValidator.isBlankOrNull(qlFormBean.getStudyLevelId()) && qlFormBean.getStudyLevelId().equalsIgnoreCase(searchBean.getOptionId())) {
          qlFormBean.setStudyLevelDesc(searchBean.getOptionText());
          break;
        }
      }      
    }
    qlFormBean.setStudyLevelList(studyLevelList);
    qlFormBean.setTypeOfEnquiry(typeOfEnquiry);
    qlFormBean.setQlFlag(qlFlag);
    
    ArrayList listOfCollegeInteractionDetailsWithMedia = new AdvertUtilities().getInteractionDetailsById(qlFormBean.getSubOrderItemId(), "sub_order_item_id", request);
    if (listOfCollegeInteractionDetailsWithMedia != null && listOfCollegeInteractionDetailsWithMedia.size() > 0) {
      request.setAttribute("listOfCollegeInteractionDetailsWithMedia", listOfCollegeInteractionDetailsWithMedia);
      String websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice(qlFormBean.getSubOrderItemId(), "website_price");
      request.setAttribute("websitePrice", websitePrice);
    }
    String collegename = "";
    String collegeNameDisplay = "";
    CollegeUtilities uniUtils = new CollegeUtilities();
    CollegeNamesVO uniNames = null;
    uniNames = uniUtils.getCollegeNames(collegeId, request);
    if (uniNames != null) {
      collegename = uniNames.getCollegeName();
      collegeNameDisplay = uniNames.getCollegeNameDisplay();
    }
    String courseName = new CommonFunction().getCourseName(courseId, ""); //Changed new function on 16_May_2017, By Thiyagu G.
    qlFormBean.setCourseName(courseName);
    if(GenericValidator.isBlankOrNull(qlFormBean.getYeartoJoinCourse())) {
      qlFormBean.setYeartoJoinCourse(qlFormBean.getDefaultYoe());//Added for Year of entry change 08_OCT_2014
    }
    request.setAttribute("collegeId", collegeId);
    request.setAttribute("courseId", courseId);
    request.setAttribute("subOrderItemId", subOrderItemId);
    request.setAttribute("collegeName", collegename);
    request.setAttribute("collegeNameDisplay", collegeNameDisplay);
    request.setAttribute("courseName", courseName);
    //
    String loginurl = "/prospectus/" + new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(collegename))) + "-prospectus/" + collegeId + "/" + (courseId != null && courseId.trim().length() > 0 ? courseId : "0") + "/" + (subject != null && subject.trim().length() > 0 ? subject : "0") + "/n-" + qlFormBean.getSubOrderItemId()  + "/order-prospectus.html";
    loginurl = loginurl != null ? loginurl.trim().toLowerCase() : loginurl;
    request.setAttribute("loginurl", loginurl);
    //
    /* Setting robotValues */
    if (subject != null && subject.trim().length() > 0 && !subject.equals("0")) {
      request.setAttribute("ROBOT_VALUE", "noindex,nofollow");
    }
    /*showing ip pod*/
    if(GenericValidator.isBlankOrNull(courseId) || "0".equals(courseId)){
      ArrayList listOfUniProfileInfo= (ArrayList) BasicFormInfoMap.get("o_uniprofile_info"); 
      if (listOfUniProfileInfo != null && listOfUniProfileInfo.size() > 0) {
        request.setAttribute("listOfUniProfileInfo", listOfUniProfileInfo);
      }
      ArrayList listOfUniOpenDays = (ArrayList) BasicFormInfoMap.get("o_open_day_info");
      if (listOfUniProfileInfo != null && listOfUniProfileInfo.size() > 0) {
        request.setAttribute("listOfOpendayInfo", listOfUniOpenDays);
      }
      ArrayList listOfUniOverallReviewsInfo = (ArrayList) BasicFormInfoMap.get("o_overall_review");
      if (listOfUniOverallReviewsInfo != null && listOfUniOverallReviewsInfo.size() > 0) {
        request.setAttribute("listOfUniOverallReviewsInfo", listOfUniOverallReviewsInfo);
      }    
    }
    //
    String profileType = (String) BasicFormInfoMap.get("P_PROFILE_TYPE");
    if(!GenericValidator.isBlankOrNull(profileType) && "IP".equals(profileType) && !GenericValidator.isBlankOrNull(courseId) && "0".equals(courseId)) {
      request.setAttribute("profileType", profileType);
    }
    qlFormBean.setProfileType(GenericValidator.isBlankOrNull(profileType)? "" : profileType);
    if(GenericValidator.isBlankOrNull(qlFormBean.getNewsLetter())){
      qlFormBean.setNewsLetter("Y");
    }
    if(!GenericValidator.isBlankOrNull(qlFormBean.getPostCode())){
      qlFormBean.setNonukresident(false);
    }
    String oneClickMandatoryFlag = (String) BasicFormInfoMap.get("O_ONE_CLIK_FLAG");
    if(!GenericValidator.isBlankOrNull(oneClickMandatoryFlag)) {
      qlFormBean.setOneClickMandatory(oneClickMandatoryFlag);
    }

    String oneClickCheckBox = (String) BasicFormInfoMap.get("O_ONE_CHECK_BOX_FLAG");
    if(!GenericValidator.isBlankOrNull(oneClickCheckBox) && "Y".equalsIgnoreCase(oneClickCheckBox)) {
      qlFormBean.setOneClickCheckBox(oneClickCheckBox);
    }

    String userGrades = (String)BasicFormInfoMap.get("O_PREDICTED_GRADES");
    if(!GenericValidator.isBlankOrNull(userGrades)) {
      qlFormBean.setPredictedGrades(userGrades);
    }
    //Added this for showing consent flag details By Hema.S on 17_12_2019_REL
    ArrayList clientLeadConsentList = (ArrayList) BasicFormInfoMap.get("O_CLIENT_CONSENT_DETAILS");
    if (clientLeadConsentList != null && clientLeadConsentList.size() > 0) {
  	ClientLeadConsentVO clientLeadConsentVO = (ClientLeadConsentVO)clientLeadConsentList.get(0);
  	request.setAttribute("consentFlag", clientLeadConsentVO.getClientMarketingConsent());
    request.setAttribute("clientLeadConsentList", clientLeadConsentList);
    }
    request.setAttribute("refferalUrl", qlFormBean.getRefferalUrl());
    if("download-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
        List dpList = new ArrayList();
        dpList = new QLCommon().getDpDetails(request,qlFormBean.getSubOrderItemId());
        ProspectusBean dpBean = (ProspectusBean)dpList.get(0);
        String downloadRequetedUrl = dpBean.getDpRedirectionURL();
        String getProspectusFlag = dpBean.getGetProspectusFlag();
        request.setAttribute("downloadRequetedUrl", downloadRequetedUrl);
        request.setAttribute("getProspectusFlag",getProspectusFlag);
    }
    new GlobalFunction().getInteractivePodInfo(collegeId, request);
  }

  private void dobMethod(QLFormBean qlformBean){
    LabelValueBean dobBean = null;
    ArrayList dateList = new ArrayList();
    for(int i = 1; i<=31; i++){
      dobBean = new LabelValueBean();
      if(i < 10){
        dobBean.setLabel("0"+String.valueOf(i));
        dobBean.setValue("0"+String.valueOf(i));
      }else{
        dobBean.setLabel(String.valueOf(i));
        dobBean.setValue(String.valueOf(i));
      }
      dateList.add(dobBean);
    }
    ArrayList monthList = new ArrayList();
    String months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    for(int i = 1; i<=12; i++){
      dobBean = new LabelValueBean();
      if(i < 10){
        dobBean.setLabel(months[i-1]);
        dobBean.setValue("0"+String.valueOf(i));
      }else{
        dobBean.setLabel(months[i-1]);
        dobBean.setValue(String.valueOf(i));
      }
      monthList.add(dobBean);
    }
    ArrayList yearList = new ArrayList();
    Calendar ca = new GregorianCalendar();
    int currentYear = (ca.get(Calendar.YEAR)-13);
    for(int i = 1900; i<=currentYear; i++){ //Changed years from 1970 to 1900 for 19_Apr_2016.
        dobBean = new LabelValueBean();
        dobBean.setLabel(String.valueOf(i));
        dobBean.setValue(String.valueOf(i));
        yearList.add(dobBean);
    }
    qlformBean.setDateList(dateList);
    qlformBean.setMonthList(monthList);
    qlformBean.setYearList(yearList);
  }
  
  private void setResponseText(HttpServletResponse response, String responseMessage) {
     try {
       response.setContentType("TEXT/PLAIN");
       Writer writer = response.getWriter();
       writer.write(responseMessage);
     } catch (IOException exception) {
       exception.printStackTrace();
     }
   }
  
}
