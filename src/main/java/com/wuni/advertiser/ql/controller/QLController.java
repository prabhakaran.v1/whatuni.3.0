 /*
   * Added By Sekhar K for wu417_20121030 for WU QL
   */
 package com.wuni.advertiser.ql.controller;

 import WUI.admin.utilities.TimeTrackingConstants;
import WUI.search.form.SearchBean;
import WUI.security.SecurityEvaluator;
import WUI.utilities.AdvertUtilities;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.advertiser.prospectus.controller.ProspectusCommonFunction;
import com.wuni.advertiser.prospectus.controller.ProspectusPopupAjaxController;
import com.wuni.advertiser.prospectus.form.ProspectusBean;
import com.wuni.advertiser.ql.QLCommon;
import com.wuni.advertiser.ql.form.QLFormBean;
import com.wuni.advertiser.ql.form.QuestionAnswerBean;
import com.wuni.advertiser.ql.validator.QLFormValidator;
import com.wuni.token.TokenFormController;
import com.wuni.util.CommonValidator;
import com.wuni.util.RequestResponseUtils;
import com.wuni.util.interaction.InteractionUtilities;
import com.wuni.util.uni.CollegeUtilities;
import com.wuni.util.user.UserUtilities;
import com.wuni.util.valueobject.CollegeNamesVO;
import com.wuni.util.valueobject.advert.ProspectusVO;
import com.wuni.util.valueobject.ql.BasicFormVO;
import com.wuni.util.valueobject.ql.ClientLeadConsentVO;

import java.io.PrintWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.struts.Globals;
import org.apache.struts.util.LabelValueBean;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*
* Class         : QLAction.java
* Description   :
* @version      : 1.0
* Change Log :
* *************************************************************************************************************************************
* author	              Ver 	              Modified On     	Modification Details 	             Change
* *************************************************************************************************************************************
* Thiyagu G           wu_546              03_Nov_2015            Modified                 To save the enquired data.
* Prabhakaran V.      wu_565              13_Jun_2017            Modified                 Server side validation for text area.
* Hema.S              wu_573              20_feb_2018            Modified                 UCAS new Fees Structure 
* Prabhakaran V.      wu_573              20_Feb_2018            Modified                 Added smart pixel collegeId and collge name
* Prabhakaran V.      wu_574              23_Mar_2018            Modified                 Added email exists check in validate method
* Sabapathi S.        wu_582              23-Oct-2018            Modified                 Added scren width for stats logging 
* Sangeeth.S          wu_583              20-Nov-2018            Modified                 Assigned user id registering through enquiry form
* Hema.S              wu_171219           17_DEC_2019            Modified                 Added consent flag details
* Sangeeth.S		  wu_310320			  09_MAR_2020	         Modified				  Added lat and long for stats logging
* Kailash L                               21_JUL_2020            Modified                 Added source order itemId for stats logging
* Keerthana E         Wu_379              17_SEP_2020            Modified                 Added featured college id and order item id for chain logging             				
* Sri Sankari		  wu_20200917         17_SEP_2020            1.11                     Added stats log for tariff logging(UCAS point)	
* Sujitha V           wu_20201101         10_NOV_2020            Modified                 Added session for YOE to log in d_session_log.	
* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/

@Controller
public class QLController{

	TokenFormController tokenController = new TokenFormController();
	SessionData sessionData = new SessionData();
    @RequestMapping(value={"/email/*-email/*/*/*/*/send-college-email", "/send-college-email"
    		               , "/orderprospectus/send-college-email", "/orderprospectus"
    		               , "/prospectus/*-prospectus/*/*/*/*/order-prospectus"
    		               , "/prospectus/*-prospectus/*/*/*/*/download-prospectus"}, 
    		method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView ql(@ModelAttribute("qlformbean") QLFormBean qlFormBean, BindingResult result , HttpServletRequest request, HttpServletResponse response, HttpSession httpSession) throws Exception {
      String mappingUrl = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
      String returnType = "qlbasicform";
      if(mappingUrl.lastIndexOf("/orderprospectus") != -1 || mappingUrl.lastIndexOf("/orderprospectus/send-college-email") != -1){
        returnType = "prospectus-basic-form"; 
      }
      qlFormBean.setRequest(request);      
      Long startActionTime = new Long(System.currentTimeMillis());
      CommonUtil commonutil = new CommonUtil();
      String clientIp = new RequestResponseUtils().getRemoteIp(request);
      String emailID = qlFormBean.getEmailAddress();
      String userID = new SessionData().getData(request, "y");
      if(!GenericValidator.isBlankOrNull(emailID)){
        emailID = URLDecoder.decode(emailID, "UTF-8");
        String existUserId = new GlobalFunction().checkEmailExist(emailID, request);
        if(("0".equalsIgnoreCase(userID) || userID == null) && (existUserId != null && !"".equalsIgnoreCase(existUserId) && !"0".equalsIgnoreCase(existUserId)))
        {
          //errors.add("wuni.error.ql.email.exists.error", new ActionMessage("wuni.error.profile.existing.emailid", emailID));
          request.setAttribute("errEmailExists", "Y");
        }
      }
      
      String userTrackSessionId = new SessionData().getData(request, "userTrackId");
      InteractionUtilities interactionUtil = new InteractionUtilities();
      String postButtonFlag = request.getParameter("postEnquiry");
      String openday = request.getParameter("p_open_day");
      String mappingPath = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "");
      if(request.getParameter("p_open_day") != null){
        request.setAttribute("opendate", openday);
      }
      if(!GenericValidator.isBlankOrNull(postButtonFlag)){
        request.setAttribute("postEnquiryRequest", "PE");
      }
      String autoEmailUserId = request.getParameter("p_autoemail_user");
      String autoEmailFlag = request.getParameter("p_autoemail_flag");
      if(GenericValidator.isBlankOrNull(qlFormBean.getAutoEmailFlag())){
        qlFormBean.setAutoEmailFlag(GenericValidator.isBlankOrNull(autoEmailFlag) ? "" : "Y");
      }
      qlFormBean.setUserId(autoEmailUserId);
      request.setAttribute("enquiryURL", request.getRequestURI().substring(request.getContextPath().length()).replace(".html", ""));
      CommonValidator validate = new CommonValidator();
      UserUtilities userUtil = new UserUtilities();
      SessionData sessiondata = new SessionData();
      if (validate.isNotBlankAndNotNullAndNotZero(userID)) {
        emailID = userUtil.getEmailIdByUserId(userID);
      }
      HashMap userBlockedStatus = interactionUtil.getUserBlockedStatus(clientIp, emailID, userID);
      boolean isIpBlocked = validate.getBoolean((String)userBlockedStatus.get("IP_BLOCKED")); 
      boolean isEmailIdBlocked = validate.getBoolean((String)userBlockedStatus.get("EMAIL_ID_BLOCKED")); 
      boolean isUserIdBlocked = validate.getBoolean((String)userBlockedStatus.get("USER_ID_BLOCKED")); 
     request.setAttribute("desktopURL", request.getRequestURI()); //9_DEC_14 Added by Amir for switch to mobile site
      if (isIpBlocked || isEmailIdBlocked || isUserIdBlocked) {
        new CommonFunction().loadUserLoggedInformation(request, request.getServletContext(), response);
        if (isIpBlocked) {
          request.setAttribute("BLOCK_TYPE", "IP");
        } else if (isEmailIdBlocked) {
          request.setAttribute("BLOCK_TYPE", "EMAIL_ID");
        } else if (isUserIdBlocked) {
          request.setAttribute("BLOCK_TYPE", "USER_ID");
        }
        //request.setAttribute("OTHER_DETAILS", ("IP=" + clientIp + "; EmailId=" + emailID + "; userID=" + userID));
        //return mapping.findForward("blockeduser");
        request.setAttribute("reason_for_404", ("IP=" + clientIp + "; EmailId=" + emailID + "; userID=" + userID));
        response.sendError(404, "404 error message");
        return null;
      }
      HttpSession session = request.getSession();
      ServletContext context = request.getServletContext();
      //new GlobalFunction().getInteractivePodInfo(collegeId, request);
      CommonFunction common = new CommonFunction();
      try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
        if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
          String fromUrl = "/home.html";
          session.setAttribute("fromUrl", fromUrl);
          return new ModelAndView("loginPage");
        }
      } catch (Exception securityException) {
        securityException.printStackTrace();
      }
      if (common.checkSessionExpired(request, response, session, "REGISTERED_USER")) { //  TO CHECK FOR SESSION EXPIRED //
        return new ModelAndView("forward: /userLogin.html");
      }
      sessiondata.setParameterValuestoSessionData(request, response);
      sessiondata.addData(request, response, "a", GlobalConstants.WHATUNI_AFFILATE_ID);
      common.loadUserLoggedInformation(request, context, response);
      
      AdvertUtilities advertUtility = new AdvertUtilities();
      String collegeId = request.getParameter("collegeId")!= null ? request.getParameter("collegeId") : "";
      String courseId = request.getParameter("courseId")!= null ? request.getParameter("courseId") : "";
      String subOrderItemId = request.getParameter("subOrderItemId")!= null ? request.getParameter("subOrderItemId") : "";
      String typeOfEnquiry = qlFormBean.getTypeOfEnquiry();
      String subject = qlFormBean.getSubject();
      String[] urlArray = request.getRequestURI().substring(request.getContextPath().length()).replace(".html", "").split("/");
      
      if (urlArray.length >= 7) {
        collegeId = urlArray[3];
        courseId = urlArray[4];
        subject = urlArray[5];
        typeOfEnquiry = urlArray[7];
        request.setAttribute("getInsightName",typeOfEnquiry); //28_OCT_14 Added by Amir

        String[] openDaysSubOrderId = urlArray[6].split("-");
        if (openDaysSubOrderId.length > 1) {
          subOrderItemId = openDaysSubOrderId[1];
          qlFormBean.setSubOrderItemId(subOrderItemId);
        } else { //no subOrderItemId found mean 404
          request.setAttribute("reason_for_404", "--(1)-- college Prospectus URL dont have subOrderItemId");
          response.sendError(404, "404 error message");
          return null;
        }
        if (advertUtility.isLiveSuborderItem(subOrderItemId).equals("FALSE")) {
          request.setAttribute("reason_for_404", "--(2)-- given suborderitem is expired.");
          response.sendError(404, "404 error message");
          return null;
        }
        String blockedFlag = commonutil.getBlockedFlag(collegeId);
        if(("Y").equals(blockedFlag)){
          request.setAttribute("reason_for_404", "--(3)-- Provider is blocked.");
          response.sendError(404, "404 error message");
          return null;
        }
      } 
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      qlFormBean.setCollegeId(collegeId);
      qlFormBean.setCourseId(courseId);
      qlFormBean.setSubject(subject);
      qlFormBean.setSubOrderItemId(subOrderItemId);
      qlFormBean.setTypeOfEnquiry(typeOfEnquiry);
      qlFormBean.setRequestUrl(CommonUtil.getRequestedURL(request));//Change the getRequestURL by Prabha on 28_Jun_2016
      qlFormBean.setRefferalUrl(request.getHeader("referer"));
      if(GenericValidator.isBlankOrNull(qlFormBean.getPostButtonFlag())){
        qlFormBean.setPostButtonFlag(GenericValidator.isBlankOrNull(postButtonFlag) ? "N" : postButtonFlag);
      }
      //Added trim function By Indumathi.S For Nov-03-15 Rel
      if(!GenericValidator.isBlankOrNull(qlFormBean.getEmailAddress())){
        qlFormBean.setEmailAddress(URLDecoder.decode(qlFormBean.getEmailAddress().trim(), "UTF-8"));
      }
      if(!GenericValidator.isBlankOrNull(qlFormBean.getAddress1())){
        qlFormBean.setAddress1(qlFormBean.getAddress1().trim());
      }
      if(!GenericValidator.isBlankOrNull(qlFormBean.getAddress2())){
        qlFormBean.setAddress2(qlFormBean.getAddress2().trim());
      }
      if(!GenericValidator.isBlankOrNull(qlFormBean.getTown())){
        qlFormBean.setTown(qlFormBean.getTown().trim());
      }
      if(!GenericValidator.isBlankOrNull(qlFormBean.getPostCode())){
        qlFormBean.setPostCode(qlFormBean.getPostCode().trim());
      }

      //End
      // to set the userinfomation to the bean if the used logged in to the system
      String sessionUserId = sessiondata.getData(request, "y");
      List inputList = new ArrayList();
      inputList.add(sessionUserId);
      inputList.add(qlFormBean.getCollegeId());
      inputList.add(qlFormBean.getSubOrderItemId());
      inputList.add(qlFormBean.getCourseId());  
      if(request.getParameter("btSubmit")== null || !  (request.getParameter("btSubmit").equalsIgnoreCase("Proceed") || request.getParameter("btSubmit").equalsIgnoreCase("ProsProceed"))){    
        getBasicFormData(qlFormBean, sessiondata, inputList, request);
        dobMethod(qlFormBean);       
        if(!GenericValidator.isBlankOrNull(mappingPath) && ("/orderprospectus").equals(mappingPath)){//28_OCT_2014 added for Bulk prospectus change
          typeOfEnquiry = "order-prospectus";
          qlFormBean.setTypeOfEnquiry(typeOfEnquiry);
          qlFormBean.setBulkProspectusType("bulkProspectus");
          request.setAttribute("getInsightName",typeOfEnquiry); //28_OCT_14 Added by Amir
          String cookieProsBasketId = new CommonFunction().checkProspectusCookieStatus(request);
          new ProspectusPopupAjaxController().getProspectusPopupContent(userID, cookieProsBasketId, request, response);
        }else if(!GenericValidator.isBlankOrNull(mappingPath) && ("/send-college-email").equalsIgnoreCase(mappingPath)){
          typeOfEnquiry = "send-college-email";
          qlFormBean.setTypeOfEnquiry(typeOfEnquiry);
        }
        request.setAttribute("typeOfEnquiry", typeOfEnquiry);
       }
      
      if(request.getParameter("oneClick")!= null && request.getParameter("oneClick").equalsIgnoreCase("Proceed")){
        qlFormBean.setUserId(userID);
        qlFormBean.setOneClickCheckBox(GenericValidator.isBlankOrNull(request.getParameter("checboxflag")) ? "" : "Y");
        qlFormBean.setNewsLetter(GenericValidator.isBlankOrNull(request.getParameter("newsflag")) ? "" : "Y");
        qlFormBean.setMessage(GenericValidator.isBlankOrNull(request.getParameter("umessage")) ? "" : request.getParameter("umessage"));
        if(tokenController.isTokenValid(request)){
          return oneclickSubmitMethod(qlFormBean, request, commonBusiness, httpSession);
        }else{
          response.sendRedirect("/degrees/home.html");
          return null;          
        }
      }
      //
      // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
      String screenWidth = GenericValidator.isBlankOrNull(request.getParameter("screenwidth")) ? GlobalConstants.DEFAULT_SCREEN_WIDTH : request.getParameter("screenwidth");
      // wu_300320 - Sangeeth.S: Added lat and long for stats logging
      String[] latAndLong = sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG).split(GlobalConstants.SPLIT_CONSTANT);
      String latitude = (latAndLong.length > 0 && !GenericValidator.isBlankOrNull(latAndLong[0])) ? latAndLong[0].trim() : "";
	      String longitude = (latAndLong.length > 1 && !GenericValidator.isBlankOrNull(latAndLong[1])) ? latAndLong[1].trim() : "";
      String sponsoredOrderItemId = StringUtils.isNotBlank(request.getParameter("sponsoredOrderItemId")) && !StringUtils.equals(request.getParameter("sponsoredOrderItemId"), "0") ? request.getParameter("sponsoredOrderItemId") : "0";
      String userUcasScore = StringUtils.isNotBlank((String)httpSession.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE)) ? (String)httpSession.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE) : null; //Added for tariff-logging-ucas-point by Sri Sankari on wu_20200917 release
	  //String yearOfEntryForDSession = StringUtils.isNotBlank((String)httpSession.getAttribute(SpringConstants.SESSION_YOE_LOGGING)) ? (String)httpSession.getAttribute(SpringConstants.SESSION_YOE_LOGGING) : null; //Added for getting YOE value in session to log in d_session_log stats by Sujitha V on 2020_NOV_10 rel
	  String yearOfEntryForDSession = StringUtils.isNotBlank((String)qlFormBean.getYeartoJoinCourse()) ? qlFormBean.getYeartoJoinCourse() : null;
      //Wu_379 - Keerthana E : Added featured college and order item id for chain logging
      //String[] featuredCollegeAndOrderItemId = sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID).split(GlobalConstants.SPLIT_CONSTANT);
      String[] featuredCollegeAndOrderItemId = StringUtils.isNotBlank(sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID)) ? sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID).split(GlobalConstants.SPLIT_CONSTANT) : null;
      String featuredCollegeId =""; 
      String featuredOrderItemId = "0";
      if(!StringUtils.isAllBlank(featuredCollegeAndOrderItemId) && featuredCollegeAndOrderItemId.length == 2) {
         featuredCollegeId = StringUtils.isNotBlank(featuredCollegeAndOrderItemId[0])? featuredCollegeAndOrderItemId[0] : "0";
         featuredOrderItemId = StringUtils.isNotBlank(featuredCollegeAndOrderItemId[1])? featuredCollegeAndOrderItemId[1] : "0"; 
      }
      if(StringUtils.equals(collegeId, featuredCollegeId) && StringUtils.equals("0", sponsoredOrderItemId)) {
        sponsoredOrderItemId =  featuredOrderItemId;
      }
      if(request.getParameter("btSubmit")!= null && (request.getParameter("btSubmit").equalsIgnoreCase("Proceed"))){
    	if(tokenController.isTokenValid(request)){
          request.getSession().removeAttribute("postEnquiryList");
          request.getSession().removeAttribute("prospectusPEList");
          request.removeAttribute("getInsightName"); //28_OCT_14 Added by Amir      
          String userId = "";         
          String clientBrowser = request.getHeader("user-agent");
          String cpeQualificationNetworkId = (String)request.getSession().getAttribute("cpeQualificationNetworkId");
          if (cpeQualificationNetworkId == null || cpeQualificationNetworkId.trim().length() == 0) {
            cpeQualificationNetworkId = "2";
          }
          String widgetId = "";
          if("Y".equalsIgnoreCase(qlFormBean.getAutoEmailFlag())){
           widgetId = new CommonFunction().getWUSysVarValue("WU_EMAIL_WIDGETID");    
          }else{
            if(request.getSession().getAttribute("widgetId") != null && request.getSession().getAttribute("widgetId").toString().trim().length() > 0){
                widgetId = (String)request.getSession().getAttribute("widgetId");
            }
          }
          request.getSession().setAttribute("cpeQualificationNetworkId", cpeQualificationNetworkId);
          String htmlId = "";
          String typeOfProspectus = "";
          request.setAttribute("typeOfEnquiry", typeOfEnquiry);
          
          if("send-college-email".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
            htmlId = "WU_COLLEGE_EMAIL";
            typeOfProspectus = "COLLEGE EMAIL";
          }else if("order-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
            htmlId = "WU_COLLEGE_PROSPECTUS_EMAIL";
            typeOfProspectus = "COLLEGE PROSPECTUS";
          }else if("download-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
            htmlId = "WU_COLLEGE_DOWNLOADPROSPECTUS_EMAIL";
            typeOfProspectus = "DOWNLOAD PROSPECTUS";
          }
          qlFormBean.setAffliatedId("220703");
          userId="0";
          //Added this for passing consent flag values by Hema.S on 17_12_2019_REL
          String consentFlag = qlFormBean.getConsentFlag();
          String consentFlagCheck = qlFormBean.getConsentFlagCheck()!= null ? qlFormBean.getConsentFlagCheck() : request.getParameter("consentFlagCheck");
          if("YES".equalsIgnoreCase(consentFlagCheck)){
            if("ON".equalsIgnoreCase(consentFlag)){ 
        	  consentFlag = "Y";
        	}else{
        	  consentFlag = "N"; 
        	}
          }else{
        	  consentFlag = "A"; 
          }
          qlFormBean.setConsentFlag(consentFlag);
          qlFormBean.setUserId(userId);  
          qlFormBean.setHtmlId(htmlId);
          qlFormBean.setSessionId(new SessionData().getData(request, "x"));
          qlFormBean.setClientBrowser(clientBrowser);
          qlFormBean.setClientIp(clientIp);
          qlFormBean.setRequestUrl(qlFormBean.getRequestUrl());
          qlFormBean.setRefferalUrl(qlFormBean.getRefferalUrl());
          qlFormBean.setCpeQualificationNetworkId(cpeQualificationNetworkId);
          qlFormBean.setTypeOfProspectus(typeOfProspectus);
          qlFormBean.setWidgetId(widgetId);      
          qlFormBean.setTrackSessionId(userTrackSessionId);
          qlFormBean.setScreenWidth(screenWidth); //screenwidth
          qlFormBean.setLatitude(latitude); //latitude
          qlFormBean.setLongitude(longitude); //longitude
          qlFormBean.setSponsoredOrderItemId(sponsoredOrderItemId); // Added source order item id for featured uni chain logging
          qlFormBean.setUserUcasScore(userUcasScore); //P_UCAS_TARIFF
          qlFormBean.setYearOfEntry(yearOfEntryForDSession); //P_YEAR_OF_ENTRY
          String donNotGradeValue = null;
          if(!GenericValidator.isBlankOrNull(qlFormBean.getProsDoNotGradesChkBox()) && ("on").equals(qlFormBean.getProsDoNotGradesChkBox())){
            donNotGradeValue = "Y";        
          }
          qlFormBean.setProsDoNotGradesChkBox(donNotGradeValue);
          if(GenericValidator.isBlankOrNull(qlFormBean.getProsGradePointSelected()) || ("on").equals(qlFormBean.getProsDoNotGradesChkBox())){
            qlFormBean.setProsGradePointSelected(null);
          }
          if(GenericValidator.isBlankOrNull(qlFormBean.getProsGradeValueSelected()) || ("on").equals(qlFormBean.getProsDoNotGradesChkBox())){
            qlFormBean.setProsGradeValueSelected(null);
          }
          qlFormBean.setMarketingEmail("on".equalsIgnoreCase(qlFormBean.getMarketingEmail()) || "Y".equalsIgnoreCase(qlFormBean.getMarketingEmail()) ? "Y" : "N");
          qlFormBean.setSolusEmail("on".equalsIgnoreCase(qlFormBean.getSolusEmail()) || "Y".equalsIgnoreCase(qlFormBean.getSolusEmail()) ? "Y" : "N");
          qlFormBean.setSurveyEmail("on".equalsIgnoreCase(qlFormBean.getSurveyEmail()) || "Y".equalsIgnoreCase(qlFormBean.getSurveyEmail()) ? "Y" : "N");
          
          /*Added server side text area validation by Prabha on 13_Jun_2017*/
          if(!"content-hub".equals(request.getParameter("fromPage"))) {
        	QLFormValidator validator = new QLFormValidator();
        	validator.validate(qlFormBean, result);
            if(result.hasErrors()){
              if("DOWNLOAD PROSPECTUS".equalsIgnoreCase(typeOfProspectus) || "COLLEGE PROSPECTUS".equalsIgnoreCase(typeOfProspectus)){
                getBasicFormData(qlFormBean, sessiondata, inputList, request);
              }else{
                QLFormBean formBean = new QLFormBean();
                BeanUtils.copyProperties(qlFormBean, formBean);
                getBasicFormData(formBean, sessiondata, inputList, request);
              }
              request.setAttribute("getInsightName",typeOfEnquiry);
              return new ModelAndView(returnType, "qlformbean", qlFormBean);
            }
          }
          /*End of server side validation*/
          Map basicFormSubmission = commonBusiness.basicFormSubmissionDBCall(qlFormBean);
          if(tokenController.isTokenValid(request)){
        	  tokenController.resetToken(request);
          }
          String enquiryId = String.valueOf(basicFormSubmission.get("p_enquiry_id"));
          String enquriyCollegeLimit = String.valueOf(basicFormSubmission.get("o_user_sent"));         
          String pixelTracking = (String)basicFormSubmission.get("o_pixel_tracking_code"); //18_NOV_2014 Added by Amir for pixel tracking in email success page        
          String newUserFlag = (String)basicFormSubmission.get("o_new_user_flag");
          if(!GenericValidator.isBlankOrNull(newUserFlag)){
            request.setAttribute("newUserFlag", newUserFlag);
          } 
          //
          try{
            int userid1 = Integer.parseInt(basicFormSubmission.get("p_user_id").toString());
            String userid = new Integer(userid1).toString();
              if(!("content-hub".equals(request.getParameter("fromPage"))) && StringUtils.isNotBlank(userid)) {
             new SessionData().addData(request, response, "y", userid);
             new SessionData().addData(request, response, "x", basicFormSubmission.get("p_session_id").toString());
             new CommonFunction().getUserTrackSessionID(request, response);
             request.setAttribute("GA_newUserRegister", userid );
             userID = new SessionData().getData(request, "y"); //Added by Sangeeth.S for 20_Nov_18 rel 
             //Encryting to user id to base64 and adding it in cookie by Sangeeth.S for whatuniGO journey
             commonutil.addEncryptedCookieUserId(request, userid, response);            
             // 
              }
          }catch(Exception e){
           e.printStackTrace();
          }
          //
          if(!GenericValidator.isBlankOrNull(pixelTracking)){
            request.setAttribute("pixelTracking", pixelTracking);  //Modified for wu_536 3-FEB-2015_REL by Karthi
          }        
          List postEnquiryList = new ArrayList();         
          postEnquiryList = (ArrayList)basicFormSubmission.get("o_post_enquiry");
          if(postEnquiryList != null && postEnquiryList.size() > 0){           
            request.setAttribute("postEnquiryList", postEnquiryList);
          }                  
          qlFormBean.setEnquiryId(enquiryId);
          //
          request.getSession().removeAttribute("usermessage");
          if(!"content-hub".equals(request.getParameter("fromPage"))) {
          getBasicFormData(qlFormBean, sessiondata, inputList, request);
          }
          if("Y".equalsIgnoreCase(qlFormBean.getAutoEmailFlag())){
            request.removeAttribute("postEnquiryList");           
          }
          String collegeNames = (String)basicFormSubmission.get("p_college_names");
          if(!GenericValidator.isBlankOrNull(collegeNames)){
            String[] arrayCollege = (collegeNames.split("##SPLIT##"));
            ArrayList institutionArrayList = new ArrayList();
            for(int i=0;i<arrayCollege.length;i++){
              ProspectusVO prospectusVO = new ProspectusVO();
              prospectusVO.setCollegeDisplayName(arrayCollege[i]);
              institutionArrayList.add(prospectusVO);
            }
            request.setAttribute("prospectusSentCollegeNames", institutionArrayList);
          }
          if("content-hub".equals(request.getParameter("fromPage"))) {
            response.setContentType("text/plain; charset=UTF-8");
            response.setHeader("Cache-Control", "no-cache");
            PrintWriter outPrint = response.getWriter();
            if("Y".equalsIgnoreCase((String)request.getAttribute("errEmailExists"))){              
              //Added for the GDPR email exist check 
              String token = (String)session.getAttribute(TokenFormController.getTokenKey()) ;
              if (token != null) { 
                session.setAttribute(Globals.TRANSACTION_TOKEN_KEY, token); 
              }
              outPrint.println("emailExists" + GlobalConstants.SPLIT_CONSTANT + token);
              //
            }else if("Y".equalsIgnoreCase(enquriyCollegeLimit)){
              outPrint.println("duplicate");  
            }else {
              outPrint.println("success");
            }
            return null;
          } else {
            if("Y".equalsIgnoreCase(enquriyCollegeLimit)){
            request.setAttribute("enquriyCollegeLimit", enquriyCollegeLimit);           
            return new ModelAndView("enquirylimitmsg");           
          }
          //
          if("download-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
        	  tokenController.saveToken(request);
            return new ModelAndView("downloadProspectusSuccess");
          }else{           
            session.removeAttribute("userBasketPodList");
            session.removeAttribute("latestMemberList");
            common.loadUserLoggedInformation(request, context, response);
            if("send-college-email".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
              String collegeWebsite = new GlobalFunction().getCollegeWebsite(collegeId);
              if (collegeWebsite != null && collegeWebsite.trim().length() > 0) {
                request.setAttribute("universityWebSite", collegeWebsite);
              }
            }else if("order-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){               
              ProspectusCommonFunction prosCommFn = new ProspectusCommonFunction();               
              String status = prosCommFn.checkBlockUserStatus(qlFormBean.getEmailAddress());
              if (status != null && !status.equals("0")) {
                request.setAttribute("blockStatus", status);
              }
            }
            request.setAttribute("postsubject", qlFormBean.getSubject());
            request.setAttribute("poststudyLevelId", qlFormBean.getStudyLevelId());
            request.setAttribute("postyearofcourse", qlFormBean.getYeartoJoinCourse());
            /*  DONT ALTER THIS ---> mainly used to track time taken details  */
            if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
              new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
            } /*  DONT ALTER THIS ---> mainly used to track time taken details  */
            if("send-college-email".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
            	tokenController.saveToken(request);
              return new ModelAndView("collegeemailSuccess");
            }else if("order-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
            	tokenController.saveToken(request);
              return new ModelAndView("collegeProspectusSuccess");
            }             
          }
          }
        }else{
          if("content-hub".equals(request.getParameter("fromPage"))) {
            response.setContentType("text/plain; charset=UTF-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println("failed");
            return null; 
          } else {
          response.sendRedirect("/degrees/home.html");
          return null;  
        }
      }
      }
      if(request.getParameter("btSubmit")!= null && (request.getParameter("btSubmit").equalsIgnoreCase("ProsProceed"))){////28_OCT_2014 added for Bulk prospectus change
        if(tokenController.isTokenValid(request)){
          String cookieProsBasketId = common.checkProspectusCookieStatus(request);
          new ProspectusPopupAjaxController().getProspectusPopupContent(userID, cookieProsBasketId, request, response);
          String userId = null;
        //Added this for passing consent flag values by Hema.S on 17_12_2019_REL
          String consentFlagCheckinBk = qlFormBean.getConsentFlagCheck();
		  sponsoredOrderItemId = featuredOrderItemId;   // Added for featured university chain logging       
          if("YES".equalsIgnoreCase(consentFlagCheckinBk)){
          int loopCount = 0;
          String collegeIdLists = qlFormBean.getCollegeIdLists();
          String[] allCollegeIdList = collegeIdLists.split("###");
          ArrayList<String> collegeIDList = new ArrayList(Arrays.asList(allCollegeIdList));
          String[] consentFlagValues = request.getParameterValues("consentFlag");
          String[] removedUni = request.getParameterValues("removedUni");
          ArrayList<String> removedUniList = new ArrayList(Arrays.asList(removedUni));
          ArrayList<String> checkCollegeList = null;
          if(consentFlagValues!=null){
            checkCollegeList = new ArrayList(Arrays.asList(consentFlagValues));
          }
          String flag = null;
          ArrayList<String> bulkList = new ArrayList();
          for (String i: collegeIDList) {
        	  if(!removedUniList.contains(i)){
        		  bulkList.add(i);
        	  }
          }
          Object[][] collegeConsentArray  = new String[bulkList.size()][2];    
          for (String i: bulkList) {
        	flag = "N"; 
        	if(checkCollegeList!=null &&checkCollegeList.size()>0){
            if (checkCollegeList.contains(i)) {
              flag = "Y";
            } 
        	}
            if(i!=null){
            collegeConsentArray[loopCount][0] = i;
            collegeConsentArray[loopCount][1] = flag;
            }
            loopCount ++; 
        	}	  
           qlFormBean.setCollegeConsentArray(collegeConsentArray);
          }
          qlFormBean.setCountryOfResidence(qlFormBean.getProsCountryOfRes());//Prospectus redesign 9_DEC_2014
          request.setAttribute("fromblkprospectus","prospectus");
          String clientBrowser = request.getHeader("user-agent");
          String htmlId = "";
          String typeOfProspectus = "";
          request.setAttribute("typeOfEnquiry", typeOfEnquiry);
          htmlId = "WU_COLLEGE_PROSPECTUS_EMAIL";
          typeOfProspectus = "COLLEGE PROSPECTUS";
          
          if(!"content-hub".equals(request.getParameter("fromPage"))) {
        	QLFormValidator validator = new QLFormValidator();
        	validator.validate(qlFormBean, result);
            if(result.hasErrors()){
              if("DOWNLOAD PROSPECTUS".equalsIgnoreCase(typeOfProspectus) || "COLLEGE PROSPECTUS".equalsIgnoreCase(typeOfProspectus)){
                getBasicFormData(qlFormBean, sessiondata, inputList, request);
              }
              request.setAttribute("getInsightName",typeOfEnquiry);
              return new ModelAndView(returnType, "qlformbean", qlFormBean);
            }
          }
          qlFormBean.setAffliatedId("220703");         
          qlFormBean.setLoggedUserId(userID);
          qlFormBean.setEnquiryBasketId(cookieProsBasketId);
          qlFormBean.setEnquiryType("RP");
          qlFormBean.setHtmlId(htmlId);
          qlFormBean.setSessionId(new SessionData().getData(request, "x"));
          qlFormBean.setClientBrowser(clientBrowser);
          qlFormBean.setClientIp(clientIp);
          qlFormBean.setRequestUrl(qlFormBean.getRequestUrl());
          qlFormBean.setRefferalUrl(qlFormBean.getRefferalUrl());         
          qlFormBean.setCpeQualificationNetworkId("");
          qlFormBean.setTypeOfProspectus(typeOfProspectus);         
          qlFormBean.setMobileFlag("N");         
          qlFormBean.setTrackSessionId(userTrackSessionId);
          qlFormBean.setScreenWidth(screenWidth); //screenwidth
          qlFormBean.setLatitude(latitude); //latitude
          qlFormBean.setLongitude(longitude); //longitude
          qlFormBean.setSponsoredOrderItemId(sponsoredOrderItemId); // Added source order item id for featured uni chain logging
          qlFormBean.setUserUcasScore(userUcasScore); // P_UCAS_TARIFF
          qlFormBean.setYearOfEntry(yearOfEntryForDSession); //P_YEAR_OF_ENTRY
          //Added YOE value in session to log in d_session for bulk prospectus by Sujitha V on 2020_NOV_10 rel
          if(StringUtils.isNotBlank(qlFormBean.getYeartoJoinCourse())) {
  		    CommonFunction.getDSessionLogForYOE(request, qlFormBean.getYeartoJoinCourse()); 
          }
          String donNotGradeValue = null;
          if(!GenericValidator.isBlankOrNull(qlFormBean.getProsDoNotGradesChkBox()) && ("on").equals(qlFormBean.getProsDoNotGradesChkBox())){
            donNotGradeValue = "Y";        
          }
          qlFormBean.setProsDoNotGradesChkBox(donNotGradeValue);
          if(GenericValidator.isBlankOrNull(qlFormBean.getProsGradePointSelected()) || ("on").equals(qlFormBean.getProsDoNotGradesChkBox())){
            qlFormBean.setProsGradePointSelected(null);
          }
          if(GenericValidator.isBlankOrNull(qlFormBean.getProsGradeValueSelected()) || ("on").equals(qlFormBean.getProsDoNotGradesChkBox())){
            qlFormBean.setProsGradeValueSelected(null);
          }
          
          qlFormBean.setMarketingEmail("on".equalsIgnoreCase(qlFormBean.getMarketingEmail()) || "Y".equalsIgnoreCase(qlFormBean.getMarketingEmail()) ? "Y" : "N");
          qlFormBean.setSolusEmail("on".equalsIgnoreCase(qlFormBean.getSolusEmail()) || "Y".equalsIgnoreCase(qlFormBean.getSolusEmail()) ? "Y" : "N");
          qlFormBean.setSurveyEmail("on".equalsIgnoreCase(qlFormBean.getSurveyEmail()) || "Y".equalsIgnoreCase(qlFormBean.getSurveyEmail()) ? "Y" : "N");
          
          Map basicFormSubmission = commonBusiness.enqBasketSubmitPrc(qlFormBean);         
          if(basicFormSubmission!=null){//Prospectus redesign 9_DEC_2014
           //Changed to new procedure with new out and list for bulk prospectus for 03_Nov_2015, By Thiyagu G.
           try{
             int userid1 = Integer.parseInt(basicFormSubmission.get("p_user_id").toString());
             String userid = new Integer(userid1).toString();
             if(!GenericValidator.isBlankOrNull(userid)){
               new SessionData().addData(request, response, "y", userid);
               new SessionData().addData(request, response, "x", basicFormSubmission.get("p_session_id").toString());
               new CommonFunction().getUserTrackSessionID(request, response);
               request.setAttribute("GA_newUserRegister", userid );
               userID = new SessionData().getData(request, "y");  //Added by Sangeeth.S for 20_Nov_18 rel
               //Encryting to user id to base64 and adding it in cookie by Sangeeth.S for whatuniGO journey
               commonutil.addEncryptedCookieUserId(request, userid, response);              
               // 
             }
           }catch(Exception e){
             e.printStackTrace();
           }
           String collegeNames = (String)basicFormSubmission.get("p_college_names");
           String prosProvidersTracking = (String)basicFormSubmission.get("p_colleges_details");
           String newUserFlag = (String)basicFormSubmission.get("o_new_user_flag");
           if(!GenericValidator.isBlankOrNull(newUserFlag)){
             request.setAttribute("newUserFlag", newUserFlag);
           }
           ArrayList prospectusPEList = (ArrayList)basicFormSubmission.get("oc_dp_suggestion_list");   
           if(!GenericValidator.isBlankOrNull(collegeNames)){
             String[] arrayCollege = (collegeNames.split("##SPLIT##"));
             ArrayList institutionArrayList = new ArrayList();
             for(int i=0;i<arrayCollege.length;i++){
               ProspectusVO prospectusVO = new ProspectusVO();
               prospectusVO.setCollegeDisplayName(arrayCollege[i]);
               institutionArrayList.add(prospectusVO);
             }
             request.setAttribute("prospectusSentCollegeNames", institutionArrayList);
           }
           if(!GenericValidator.isBlankOrNull(prosProvidersTracking)){
             request.setAttribute("bulkProsIntProviders", prosProvidersTracking);
             request.setAttribute("bulkProsUserId",userID);
           }  
           if(prospectusPEList!= null && prospectusPEList.size()>0){
             request.setAttribute("prospectusPEList",prospectusPEList);
           } 
          }        
          if(tokenController.isTokenValid(request)){
        	  tokenController.resetToken(request);
          }        
          session.removeAttribute("userBasketPodList");
          session.removeAttribute("latestMemberList");
          common.loadUserLoggedInformation(request, context, response);
         if("order-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){ 
            ProspectusCommonFunction prosCommFn = new ProspectusCommonFunction();
           //Need to remove below refernce for 03-Nov-2015, By Thiyagu G.
            String status = prosCommFn.checkBlockUserStatus(qlFormBean.getEmailAddress());
            if (status != null && !status.equals("0")) {
              request.setAttribute("blockStatus", status);
            }
          }
          /*  DONT ALTER THIS ---> mainly used to track time taken details  */
          if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
            new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
          } /*  DONT ALTER THIS ---> mainly used to track time taken details  */
        
          tokenController.saveToken(request);
          return new ModelAndView("prospectusSentSuccess");      
        }else{
          response.sendRedirect("/degrees/home.html");
          return null;  
        }
      }
      
      if(request.getParameter("sendCollegeEmail")!= null && request.getParameter("sendCollegeEmail").equalsIgnoreCase("yes")){
        if(tokenController.isTokenValid(request)){
        String cookieProsBasketId = common.checkProspectusCookieStatus(request);
        String userId = "";
        request.setAttribute("fromblkprospectus","prospectus");//Prospectus redesign 9_DEC_2014
        userId = new QLCommon().getUserId(qlFormBean, request, response, context);
        String clientBrowser = request.getHeader("user-agent");
        String htmlId = "";
        String typeOfProspectus = "";
        request.setAttribute("typeOfEnquiry", typeOfEnquiry);
        htmlId = "WU_COLLEGE_PROSPECTUS_EMAIL";
        typeOfProspectus = "COLLEGE PROSPECTUS";
        qlFormBean.setAffliatedId("220703");
        qlFormBean.setUserId(userId);
        qlFormBean.setLoggedUserId(userID);
        qlFormBean.setEnquiryBasketId(cookieProsBasketId);
        qlFormBean.setEnquiryType("RP");        
        qlFormBean.setHtmlId(htmlId);
        qlFormBean.setSessionId(new SessionData().getData(request, "x"));
        qlFormBean.setClientBrowser(clientBrowser);
        qlFormBean.setClientIp(clientIp);
        qlFormBean.setRequestUrl(qlFormBean.getRequestUrl());
        qlFormBean.setRefferalUrl(qlFormBean.getRefferalUrl());
        qlFormBean.setCpeQualificationNetworkId("");
        qlFormBean.setTypeOfProspectus(typeOfProspectus);
        qlFormBean.setMobileFlag("N");
        qlFormBean.setTrackSessionId(userTrackSessionId);
        qlFormBean.setScreenWidth(screenWidth); // screenwidth
        qlFormBean.setLatitude(latitude); //latitude
        qlFormBean.setLongitude(longitude); //longitude
        qlFormBean.setSponsoredOrderItemId(sponsoredOrderItemId); // Added source order item id for featured uni chain logging
        qlFormBean.setUserUcasScore(userUcasScore); // P_UCAS_TARIFF
        qlFormBean.setYearOfEntry(yearOfEntryForDSession);  //P_YEAR_OF_ENTRY
        Map basicFormSubmission = commonBusiness.enqBasketSubmitPrc(qlFormBean);
        if(basicFormSubmission!=null){
          //Changed to new procedure with new out and list for bulk prospectus for 03_Nov_2015, By Thiyagu G.
          try{
            int userid1 = Integer.parseInt(basicFormSubmission.get("p_user_id").toString());
            String userid = new Integer(userid1).toString();
            if(!GenericValidator.isBlankOrNull(userid)){
             new SessionData().addData(request, response, "y", userid);
             new SessionData().addData(request, response, "x", basicFormSubmission.get("p_session_id").toString());
             new CommonFunction().getUserTrackSessionID(request, response);
             request.setAttribute("GA_newUserRegister", userid );
             userID = new SessionData().getData(request, "y"); //Added by Sangeeth.S for 20_Nov_18 rel
             //Encryting to user id to base64 and adding it in cookie by Sangeeth.S for whatuniGO journey
             commonutil.addEncryptedCookieUserId(request, userid, response);    
             //
            }
          }catch(Exception e){
           e.printStackTrace();
          }
        String collegeNames = (String)basicFormSubmission.get("p_college_names");
        String prosProvidersTracking = (String)basicFormSubmission.get("p_colleges_details");      
        if(!GenericValidator.isBlankOrNull(collegeNames)){
          String[] arrayCollege = (collegeNames.split("##SPLIT##"));
          ArrayList institutionArrayList = new ArrayList();
          for(int i=0;i<arrayCollege.length;i++){
            ProspectusVO prospectusVO = new ProspectusVO();
            prospectusVO.setCollegeDisplayName(arrayCollege[i]);
            institutionArrayList.add(prospectusVO);   
        }     
          request.setAttribute("prospectusSentCollegeNames", institutionArrayList);
        }     
        if(!GenericValidator.isBlankOrNull(prosProvidersTracking)){
          request.setAttribute("bulkProsIntProviders", prosProvidersTracking);
          request.setAttribute("bulkProsUserId",userID);
        }
        String newUserFlag = (String)basicFormSubmission.get("o_new_user_flag");
        if(!GenericValidator.isBlankOrNull(newUserFlag)){
          request.setAttribute("newUserFlag", newUserFlag);
        }
        ArrayList prospectusPEList = (ArrayList)basicFormSubmission.get("oc_dp_suggestion_list"); 
          if(prospectusPEList!= null && prospectusPEList.size()>0){
            request.setAttribute("prospectusPEList",prospectusPEList);
          }
        }
        tokenController.saveToken(request);      
        session.removeAttribute("userBasketPodList");
        session.removeAttribute("latestMemberList");
        common.loadUserLoggedInformation(request, context, response);
        if("order-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){ 
          ProspectusCommonFunction prosCommFn = new ProspectusCommonFunction();         
          String status = prosCommFn.checkBlockUserStatus(qlFormBean.getEmailAddress());
          if (status != null && !status.equals("0")) {
            request.setAttribute("blockStatus", status);
          }
        }
            if(!("content-hub".equals(request.getParameter("fromPage")))) {
        new CommonFunction().getUserTimeLineTrackPod(request); //28_OCT_2014 Added by Amir for updating userTimeline
            }
        /*  DONT ALTER THIS ---> mainly used to track time taken details  */
        if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
          new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
        } /*  DONT ALTER THIS ---> mainly used to track time taken details  */
        
        
          return new ModelAndView("prospectusSentSuccess");
        }else
          {
            response.sendRedirect("/degrees/home.html");
            return null;  
          }
      }     
       request.setAttribute("refferalUrl", qlFormBean.getRefferalUrl());
      /*  DONT ALTER THIS ---> mainly used to track time taken details  */
      if (TimeTrackingConstants.TIME_TRACKING_FLAG_ON_OFF) {
        new WUI.admin.utilities.AdminUtilities().storeTimeAndResourceDetailsInRequestScope(request, this.getClass().toString(), startActionTime);
      } /*  DONT ALTER THIS ---> mainly used to track time taken details  */
      //    
      session.setAttribute("noSplashpopup","true");
      request.removeAttribute("userTimeLinePodList"); //08_OCT_2014 Added by Amir to not show Timeline pod
      //
      tokenController.saveToken(request);
      return new ModelAndView(returnType, "qlformbean", qlFormBean);
    }
    /**
      * This function is used to submit the oneclick enquiry
      * @param qlFormBean
      * @param sessiondata
      * @param request    
      * Date : wu516_19112013
      */
    private ModelAndView oneclickSubmitMethod(QLFormBean qlformbean, HttpServletRequest request, ICommonBusiness commonBusiness, HttpSession session) {
      request.getSession().removeAttribute("postEnquiryList");
      request.getSession().removeAttribute("prospectusPEList");
      String widgetId = "";
      String dataShareFlag = request.getParameter("consentFlagOneClick");
      String consentFlagChecks = request.getParameter("consentFlagCheckOneClick");
      if("YES".equalsIgnoreCase(consentFlagChecks)){
        if("Y".equalsIgnoreCase(dataShareFlag)){ 
        	dataShareFlag = "Y";
    	}else{
    		dataShareFlag = "N"; 
    	}
      }else{
    	  dataShareFlag = "A"; 
      }
      qlformbean.setConsentFlag(dataShareFlag);
      // wu582_20181023 - Sabapathi: Added screenwidth for stats logging
      String screenWidth = GenericValidator.isBlankOrNull(request.getParameter("screenwidth")) ? GlobalConstants.DEFAULT_SCREEN_WIDTH : request.getParameter("screenwidth");
      // wu_300320 - Sangeeth.S: Added lat and long for stats logging
      String collegeId = request.getParameter("collegeId")!= null ? request.getParameter("collegeId") : "";
	  if(StringUtils.isBlank(collegeId) && StringUtils.isNotBlank(qlformbean.getCollegeId())) {
        collegeId = qlformbean.getCollegeId();
      }
      String[] latAndLong = sessionData.getData(request, GlobalConstants.SESSION_KEY_LAT_LONG).split(GlobalConstants.SPLIT_CONSTANT);
	      String latitude = (latAndLong.length > 0 && !GenericValidator.isBlankOrNull(latAndLong[0])) ? latAndLong[0].trim() : "";
	      String longitude = (latAndLong.length > 1 && !GenericValidator.isBlankOrNull(latAndLong[1])) ? latAndLong[1].trim() : "";
      String sponsoredOrderItemId = StringUtils.isNotBlank(request.getParameter("sponsoredOrderItemId")) && !StringUtils.equals(request.getParameter("sponsoredOrderItemId"), "0") ? request.getParameter("sponsoredOrderItemId") : "0";
      String userUcasScore = StringUtils.isNotBlank((String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE)) ? (String)session.getAttribute(GlobalConstants.SESSION_KEY_USER_UCAS_SCORE) : null; //Added for tariff-logging-ucas-point by Sri Sankari on wu_20200915 release
      String yearOfEntryForDSession = StringUtils.isNotBlank(qlformbean.getYeartoJoinCourse()) ? qlformbean.getYeartoJoinCourse() : null;
      //Wu_379 - Keerthana E : Added featured college and order item id for chain logging
      //String[] featuredCollegeAndOrderItemId = sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID).split(GlobalConstants.SPLIT_CONSTANT);
      String[] featuredCollegeAndOrderItemId = StringUtils.isNotBlank(sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID)) ? sessionData.getData(request, SpringConstants.SESSION_FEATURED_COLLEGEID_ORDERITEMID).split(GlobalConstants.SPLIT_CONSTANT) : null;
      String featuredCollegeId =""; 
      String featuredOrderItemId = "0";
      //if(StringUtils.isNoneBlank(featuredCollegeAndOrderItemId)) {
      if(!StringUtils.isAllBlank(featuredCollegeAndOrderItemId) && featuredCollegeAndOrderItemId.length == 2) {
         featuredCollegeId = StringUtils.isNotBlank(featuredCollegeAndOrderItemId[0])? featuredCollegeAndOrderItemId[0] : "0";
         featuredOrderItemId = StringUtils.isNotBlank(featuredCollegeAndOrderItemId[1])? featuredCollegeAndOrderItemId[1] : "0"; 
      }
      if(StringUtils.equals(collegeId, featuredCollegeId) && StringUtils.equals("0", sponsoredOrderItemId)) {
        sponsoredOrderItemId =  featuredOrderItemId;
      }
      if(request.getSession().getAttribute("widgetId") != null && request.getSession().getAttribute("widgetId").toString().trim().length() > 0){
          widgetId = (String)request.getSession().getAttribute("widgetId");
      }
      String htmlId = "";
      String typeOfProspectus = "";
      if("send-college-email".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){
        htmlId = "WU_COLLEGE_EMAIL";
        typeOfProspectus = "COLLEGE EMAIL";
      }else if("order-prospectus".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){
        htmlId = "WU_COLLEGE_PROSPECTUS_EMAIL";
        typeOfProspectus = "COLLEGE PROSPECTUS";
      }else if("download-prospectus".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){
        htmlId = "WU_COLLEGE_DOWNLOADPROSPECTUS_EMAIL";
        typeOfProspectus = "DOWNLOAD PROSPECTUS";
      }
      request.removeAttribute("getInsightName"); //28_OCT_14 Added by Amir
      String clientIp = new RequestResponseUtils().getRemoteIp(request);
      String clientBrowser = request.getHeader("user-agent");
      qlformbean.setHtmlId(htmlId);
      qlformbean.setSessionId(new SessionData().getData(request, "x"));
      qlformbean.setClientBrowser(clientBrowser);
      qlformbean.setClientIp(clientIp);
      qlformbean.setLoggedUserId(new SessionData().getData(request, "y"));
      qlformbean.setTrackSessionId(new SessionData().getData(request,"userTrackId"));
      qlformbean.setScreenWidth(screenWidth); //screenwidth
      qlformbean.setLatitude(latitude); //latitude
      qlformbean.setLongitude(longitude); //longitude
      qlformbean.setSponsoredOrderItemId(sponsoredOrderItemId); // Added source order item id for featured uni chain logging
      qlformbean.setUserUcasScore(userUcasScore); //P_UCAS_TARIFF
      qlformbean.setYearOfEntry(yearOfEntryForDSession);  //P_YEAR_OF_ENTRY
      if(qlformbean.getRequestUrl() == null){
        qlformbean.setRequestUrl(CommonUtil.getRequestedURL(request));//Change the getRequestURL by Prabha on 28_Jun_2016
      }
      if(qlformbean.getRefferalUrl() == null){
        qlformbean.setRefferalUrl(request.getHeader("referer"));
      }
      qlformbean.setTypeOfProspectus(typeOfProspectus);
      qlformbean.setWidgetId(widgetId);
      qlformbean.setAffliatedId("220703");
      String emailDefaultText = "";
      if(qlformbean.getMessage() == null || "".equalsIgnoreCase(qlformbean.getMessage())){
        if(GenericValidator.isBlankOrNull(qlformbean.getCourseId()) || "0".equalsIgnoreCase(qlformbean.getCourseId())){
          emailDefaultText = "Hello, \n\nI read about " + qlformbean.getCollegeNameDisplay() + " on Whatuni.com, and would like to request more information about the institution."; 
        }else{
          emailDefaultText = "Hello, \n\nI read about the " + qlformbean.getCourseName() + " offered by "+qlformbean.getCollegeNameDisplay()+" on Whatuni.com and would like to request more information about the course."; 
        }    
        qlformbean.setMessage(emailDefaultText);
      }
      //Changed to new procedure with new out and list for bulk prospectus for 03_Nov_2015, By Thiyagu G.
      Map oneclickEnquirysubmit = commonBusiness.oneclickEnquirySubmit(qlformbean);
      String collegeNames = (String)oneclickEnquirysubmit.get("p_college_names");     
      if(!GenericValidator.isBlankOrNull(collegeNames)){
        String[] arrayCollege = (collegeNames.split("##SPLIT##"));
        ArrayList institutionArrayList = new ArrayList();
        for(int i=0;i<arrayCollege.length;i++){
          ProspectusVO prospectusVO = new ProspectusVO();
          prospectusVO.setCollegeDisplayName(arrayCollege[i]);
          institutionArrayList.add(prospectusVO);   
        }     
        request.setAttribute("prospectusSentCollegeNames", institutionArrayList);
      }   
      String enquiryId = String.valueOf(oneclickEnquirysubmit.get("p_enquiry_id"));
      String enquriyCollegeLimit = String.valueOf(oneclickEnquirysubmit.get("o_user_sent"));     
      String pixelTracking = (String)oneclickEnquirysubmit.get("o_pixel_tracking_code");   //18_NOV_2014 Added by Amir for pixel tracking in email success page          
      
      if(!GenericValidator.isBlankOrNull(pixelTracking)){
        request.setAttribute("pixelTracking", pixelTracking);  //Modified for wu_536 3-FEB-2015_REL by Karthi
      }
      List postEnquiryList = new ArrayList();     
      qlformbean.setEnquiryId(enquiryId);
      if(tokenController.isTokenValid(request)){
    	  tokenController.resetToken(request);
      }    
      if("Y".equalsIgnoreCase(enquriyCollegeLimit)){
        request.setAttribute("enquriyCollegeLimit", enquriyCollegeLimit);       
        return new ModelAndView("enquirylimitmsg");
      }    
      postEnquiryList = (ArrayList)oneclickEnquirysubmit.get("o_post_enquiry");     
      if(postEnquiryList != null && postEnquiryList.size() > 0){
        request.setAttribute("postEnquiryList", postEnquiryList);
      }
      request.setAttribute("postsubject", qlformbean.getSubject());
      request.setAttribute("poststudyLevelId", qlformbean.getStudyLevelId());
      request.setAttribute("postyearofcourse", qlformbean.getYeartoJoinCourse());
      
      //Added YOE value in session to log in d_session by Sujitha V on 2020_NOV_10 rel
      if(StringUtils.isNotBlank(qlformbean.getYeartoJoinCourse())) {
		CommonFunction.getDSessionLogForYOE(request, qlformbean.getYeartoJoinCourse()); 
      }
      
      tokenController.resetToken(request);
      if("send-college-email".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){
    	  tokenController.saveToken(request);
        return new ModelAndView("collegeemailSuccess");
      }else if("order-prospectus".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){       
    	  tokenController.saveToken(request);
        return new ModelAndView("collegeProspectusSuccess");
      }else if("download-prospectus".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){
    	  tokenController.saveToken(request);
        return new ModelAndView("downloadProspectusSuccess");
      }
       return null;
    }
    //
    /**
      * This function is used to get the basic form related data
      * @param qlFormBean
      * @param sessiondata
      * @param inputList
      * @param request    
      */
    private void getBasicFormData(QLFormBean qlFormBean, SessionData sessiondata, List inputList, HttpServletRequest request) throws Exception {
      String sessionUserId = sessiondata.getData(request, "y");
      if(sessionUserId == null || "0".equalsIgnoreCase(sessionUserId)){
        qlFormBean.setUserId(qlFormBean.getUserId());
      }else{
        qlFormBean.setUserId(sessionUserId);
      }
      String collegeId = qlFormBean.getCollegeId();
      String courseId = qlFormBean.getCourseId();
      String subOrderItemId = qlFormBean.getSubOrderItemId();
      String subject = qlFormBean.getSubject();
      String typeOfEnquiry = qlFormBean.getTypeOfEnquiry();
      String enqId = new CommonFunction().checkProspectusCookieStatus(request);
      qlFormBean.setEnquiryBasketId(enqId);
      String htmlId = "";
      if("send-college-email".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
        htmlId = "WU_COLLEGE_EMAIL";
      }else if("order-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
        htmlId = "WU_COLLEGE_PROSPECTUS_EMAIL";
      }else if("download-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
        htmlId = "WU_COLLEGE_DOWNLOADPROSPECTUS_EMAIL";
      }            
      qlFormBean.setHtmlId(htmlId);
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);     
      Map basicFormInfoMap = commonBusiness.getQlBasicFormInfo(qlFormBean);
      ArrayList userInfoList = (ArrayList)basicFormInfoMap.get("PC_USER_DETAILS");
      if(userInfoList != null && userInfoList.size() > 0  && !"Y".equalsIgnoreCase((String)request.getAttribute("errEmailExists"))){
        BasicFormVO basicformvo = new BasicFormVO();
        basicformvo = (BasicFormVO)userInfoList.get(0);
        qlFormBean.setEmailAddress(basicformvo.getEmailAddress());
        qlFormBean.setFirstName(basicformvo.getFirstName());
        qlFormBean.setLastName(basicformvo.getLastName());
        qlFormBean.setPostCode(basicformvo.getPostCode());
        qlFormBean.setYeartoJoinCourse(basicformvo.getYeartoJoinCourse());
        qlFormBean.setNationality(basicformvo.getNationality());
        qlFormBean.setDateOfBirth(basicformvo.getDateOfBirth());
        qlFormBean.setMarketingEmail(basicformvo.getMarketingEmail());
        qlFormBean.setSolusEmail(basicformvo.getSolusEmail());
        qlFormBean.setSurveyEmail(basicformvo.getSurveyEmail());
        qlFormBean.setDefaultYoe(basicformvo.getDefaultYoe());
        if(!GenericValidator.isBlankOrNull(basicformvo.getDateOfBirth())){
        String[] dob = qlFormBean.getDateOfBirth().split("/");
        qlFormBean.setDate(dob[0]);
        qlFormBean.setMonth(dob[1]);
        qlFormBean.setYear(dob[2]);
        }
        if(GenericValidator.isBlankOrNull(basicformvo.getNewsLetter())) {
          qlFormBean.setNewsLetter("Y");
        }else {
          qlFormBean.setNewsLetter(basicformvo.getNewsLetter());
        }
        if(!GenericValidator.isBlankOrNull(basicformvo.getNationality())) {
           qlFormBean.setNonukresident(true);   
        }        
        if(!"Y".equalsIgnoreCase((String)request.getAttribute("errEmailExists")) && !GenericValidator.isBlankOrNull(sessionUserId) && !GenericValidator.isBlankOrNull(qlFormBean.getEmailAddress())){
          request.setAttribute("isExistsBasicFormEmail", "YES");
        }
        
        //Added YOE value in session to log in d_session by Sujitha V on 2020_NOV_10 rel
        if(StringUtils.isNotBlank(qlFormBean.getYeartoJoinCourse())) {
		  CommonFunction.getDSessionLogForYOE(request, qlFormBean.getYeartoJoinCourse()); 
        }
        
      }
      ArrayList flagInfoList = (ArrayList)basicFormInfoMap.get("pc_user_subscription_flags");
      if(flagInfoList != null && flagInfoList.size() > 0 && !"Y".equalsIgnoreCase((String)request.getAttribute("errEmailExists"))){
        BasicFormVO basicformvo = new BasicFormVO();
        basicformvo = (BasicFormVO)flagInfoList.get(0);
        qlFormBean.setMarketingEmail(!GenericValidator.isBlankOrNull(basicformvo.getMarketingEmail()) ? basicformvo.getMarketingEmail() : "N");
        qlFormBean.setSolusEmail(!GenericValidator.isBlankOrNull(basicformvo.getSolusEmail()) ? basicformvo.getSolusEmail() : "N");
        qlFormBean.setSurveyEmail(!GenericValidator.isBlankOrNull(basicformvo.getSurveyEmail()) ? basicformvo.getSurveyEmail() : "N");
    }
    String enquiryExistFlag = (String)basicFormInfoMap.get("P_ENQUIRY_EXIST_FLAG");
    if(!GenericValidator.isBlankOrNull(enquiryExistFlag)){
      request.setAttribute("enquiryExistFlag", enquiryExistFlag);
    }
      ArrayList userAddressInfo = (ArrayList)basicFormInfoMap.get("PC_USER_ADDRESS");
      if(userAddressInfo != null && userAddressInfo.size() > 0){
        BasicFormVO basicformvo = new BasicFormVO();
        basicformvo = (BasicFormVO)userAddressInfo.get(0);
        qlFormBean.setAddress1(basicformvo.getAddress1());
        qlFormBean.setAddress2(basicformvo.getAddress2()); 
        qlFormBean.setTown(basicformvo.getTown());
        qlFormBean.setCountryOfResidence(basicformvo.getCountryOfResId());
        if(!GenericValidator.isBlankOrNull(basicformvo.getCountryOfResId())){//Prospectus redesign 9_DEC_2014
          qlFormBean.setProsnonukresident("No");
        }
      }
      String qlFlag = String.valueOf(basicFormInfoMap.get("P_QL_FLAG"));
      String studyLevelType = String.valueOf(basicFormInfoMap.get("p_study_level_type"));    
      if(!GenericValidator.isBlankOrNull(courseId) && !"0".equalsIgnoreCase(courseId)){
        ArrayList coursePodList = (ArrayList)basicFormInfoMap.get("PC_COURSE_DETAILS");
        if(coursePodList != null && coursePodList.size() > 0){
          BasicFormVO basicFormVO= (BasicFormVO)coursePodList.get(0);        
          qlFormBean.setStudyLevelId(basicFormVO.getStudyLevelCode());
          basicFormVO.setCourseDesc(!GenericValidator.isBlankOrNull(basicFormVO.getCourseDesc())? basicFormVO.getCourseDesc(): "");
          basicFormVO.setTutionFees(GenericValidator.isBlankOrNull(basicFormVO.getTutionFees()) ? GenericValidator.isBlankOrNull(basicFormVO.getTutionFeesDesc())?  "" : basicFormVO.getTutionFeesDesc() : basicFormVO.getTutionFees());
          basicFormVO.setUcasTariff(GenericValidator.isBlankOrNull(basicFormVO.getUcasTariff()) ? GenericValidator.isBlankOrNull(basicFormVO.getEntryRequirements())?  "" : basicFormVO.getEntryRequirements() : basicFormVO.getUcasTariff());
          basicFormVO.setDuration(GenericValidator.isBlankOrNull(basicFormVO.getDuration()) ? GenericValidator.isBlankOrNull(basicFormVO.getDurationDesc())?  "" : basicFormVO.getDurationDesc() : basicFormVO.getDuration());
          if(basicFormVO.getStudyMode()!=null && !"".equals(basicFormVO.getStudyMode())) {
            request.setAttribute("cDimStudyMode",basicFormVO.getStudyMode().toLowerCase());  
          }        
          coursePodList.set(0,basicFormVO);
          request.setAttribute("coursePodInfo", coursePodList);
        }
      }else{
        if("UG".equalsIgnoreCase(studyLevelType)){
          qlFormBean.setStudyLevelId("M");
          
        }else if("PG".equalsIgnoreCase(studyLevelType)){
          qlFormBean.setStudyLevelId("L");
        }
      }
      if(qlFormBean.getStudyLevelId()!=null && !"".equals(qlFormBean.getStudyLevelId())) {
        new GlobalFunction().getStudyLevelDesc(qlFormBean.getStudyLevelId(), request);  
      }    
      request.setAttribute("cDimUID",qlFormBean.getUserId());    
      ArrayList nationalityList = (ArrayList)basicFormInfoMap.get("pc_nationality");
      if(nationalityList != null && nationalityList.size() > 0){
        List nationality = (ArrayList)nationalityList.get(0);
        qlFormBean.setNationalityList(nationality);
        if(!GenericValidator.isBlankOrNull(qlFormBean.getNationality())) //wu516_19112013 oneclick enquiry changes
        {
          Iterator natList = nationality.iterator();
          BasicFormVO basicformvo1 = new BasicFormVO();
          while(natList.hasNext())
          {
            basicformvo1 = (BasicFormVO) natList.next();
            if(qlFormBean.getNationality().equalsIgnoreCase(basicformvo1.getNationalityValue()))
            {
              qlFormBean.setNationalityDesc(basicformvo1.getNationalityDesc());
              break;
            }
          }
        }
      }
      ArrayList courntyOfRes = (ArrayList)basicFormInfoMap.get("pc_country_of_res");
      if(courntyOfRes != null && courntyOfRes.size() > 0){
        qlFormBean.setProspectusCountryOfRes(courntyOfRes);//Prospectus redesign 9_DEC_2014
        if(!GenericValidator.isBlankOrNull(qlFormBean.getCountryOfResidence())) //wu516_19112013 oneclick enquiry changes
        {
          Iterator countryList = courntyOfRes.iterator();
          BasicFormVO basicformvo2 = new BasicFormVO();
          while(countryList.hasNext())
          {
            basicformvo2 = (BasicFormVO) countryList.next();
            if(qlFormBean.getCountryOfResidence().equalsIgnoreCase(basicformvo2.getNationalityValue()))
            {
              qlFormBean.setCountryOfResDesc(basicformvo2.getNationalityDesc());
              break;
            }
          }
        }
      } 
      List studyLevelList = new CommonFunction().getStudyLevel();
      if(studyLevelList != null && studyLevelList.size()>0) { 
        Iterator listItr = studyLevelList.iterator();      
        SearchBean searchBean = null;
        while(listItr.hasNext()) {
          searchBean = (SearchBean) listItr.next();
          if(!GenericValidator.isBlankOrNull(qlFormBean.getStudyLevelId()) && qlFormBean.getStudyLevelId().equalsIgnoreCase(searchBean.getOptionId())) {
            qlFormBean.setStudyLevelDesc(searchBean.getOptionText());
            break;
          }
        }      
      }
      qlFormBean.setStudyLevelList(studyLevelList);
      qlFormBean.setTypeOfEnquiry(typeOfEnquiry);
      qlFormBean.setQlFlag(qlFlag);
      
      ArrayList listOfCollegeInteractionDetailsWithMedia = new AdvertUtilities().getInteractionDetailsById(qlFormBean.getSubOrderItemId(), "sub_order_item_id", request);
      if (listOfCollegeInteractionDetailsWithMedia != null && listOfCollegeInteractionDetailsWithMedia.size() > 0) {
        request.setAttribute("listOfCollegeInteractionDetailsWithMedia", listOfCollegeInteractionDetailsWithMedia);
        String websitePrice = new WUI.utilities.CommonFunction().getGACPEPrice(qlFormBean.getSubOrderItemId(), "website_price");
        request.setAttribute("websitePrice", websitePrice);
      }
      String collegename = "";
      String collegeNameDisplay = "";
      CollegeUtilities uniUtils = new CollegeUtilities();
      CollegeNamesVO uniNames = null;
      uniNames = uniUtils.getCollegeNames(collegeId, request);
      if (uniNames != null) {
        collegename = uniNames.getCollegeName();
        collegeNameDisplay = uniNames.getCollegeNameDisplay();
      }
      String courseName = new CommonFunction().getCourseName(courseId, ""); //Changed new function on 16_May_2017, By Thiyagu G.
      qlFormBean.setCourseName(courseName);
      if(courseName!=null && !"".equals(courseName)) {
       request.setAttribute("cDimCourseTitle",new GlobalFunction().getReplacedString(courseName));        
      }
      if(GenericValidator.isBlankOrNull(qlFormBean.getYeartoJoinCourse())) {
    	  String[] yoeArr = new CommonFunction().getYearOfEntryArr();
          qlFormBean.setYeartoJoinCourse(yoeArr[0]);
      }
      request.setAttribute("collegeId", collegeId);
      request.setAttribute("courseId", courseId);
      request.setAttribute("subOrderItemId", subOrderItemId);
      request.setAttribute("collegeName", collegename);
      request.setAttribute("collegeNameDisplay", collegeNameDisplay);
      /* Added college display name for smart pixel by Prabha on 20_02_2018 */
      if(!GenericValidator.isBlankOrNull(collegename)){
        request.setAttribute("cDimCollegeDisName", ("\""+collegename+"\""));
      }
      /* End od smart pixel code */
      
      request.setAttribute("courseName", courseName);
      
      request.setAttribute("cDimCollegeId", collegeId);
      if(!GenericValidator.isBlankOrNull(collegeId)){
        request.setAttribute("cDimCollegeIds", ("'" + collegeId + "'")); //Added for smart pixel changes by Prabha on 20.02.2018
      }
      
      //
      String loginurl = "/prospectus/" + new CommonFunction().replaceHypen(new CommonFunction().replaceSpecialCharacter(new CommonFunction().replaceURL(collegename))) + "-prospectus/" + collegeId + "/" + (courseId != null && courseId.trim().length() > 0 ? courseId : "0") + "/" + (subject != null && subject.trim().length() > 0 ? subject : "0") + "/n-" + qlFormBean.getSubOrderItemId()  + "/order-prospectus.html";
      loginurl = loginurl != null ? loginurl.trim().toLowerCase() : loginurl;
      request.setAttribute("loginurl", loginurl);
      //
      /* Setting robotValues */
      if (subject != null && subject.trim().length() > 0 && !subject.equals("0")) {
        request.setAttribute("ROBOT_VALUE", "noindex,nofollow");
      }
      /*showing ip pod*/
      if(GenericValidator.isBlankOrNull(courseId) || "0".equals(courseId)){
        ArrayList listOfUniProfileInfo= (ArrayList) basicFormInfoMap.get("o_uniprofile_info"); 
        if (listOfUniProfileInfo != null && listOfUniProfileInfo.size() > 0) {
          request.setAttribute("listOfUniProfileInfo", listOfUniProfileInfo);
        }
        ArrayList listOfUniOpenDays = (ArrayList) basicFormInfoMap.get("o_open_day_info");
        if (listOfUniProfileInfo != null && listOfUniProfileInfo.size() > 0) {
          request.setAttribute("listOfOpendayInfo", listOfUniOpenDays);
        }
        ArrayList listOfUniOverallReviewsInfo = (ArrayList) basicFormInfoMap.get("o_overall_review");
        if (listOfUniOverallReviewsInfo != null && listOfUniOverallReviewsInfo.size() > 0) {
          request.setAttribute("listOfUniOverallReviewsInfo", listOfUniOverallReviewsInfo);
        }    
      }
      //
      String profileType = (String) basicFormInfoMap.get("P_PROFILE_TYPE");
      if(!GenericValidator.isBlankOrNull(profileType) && "IP".equals(profileType) && !GenericValidator.isBlankOrNull(courseId) && "0".equals(courseId)) {
        request.setAttribute("profileType", profileType);
      }
      qlFormBean.setProfileType(GenericValidator.isBlankOrNull(profileType)? "" : profileType);
      if(GenericValidator.isBlankOrNull(qlFormBean.getNewsLetter())){
        qlFormBean.setNewsLetter("Y");
      }
      if(!GenericValidator.isBlankOrNull(qlFormBean.getPostCode())){
        qlFormBean.setNonukresident(false);
        qlFormBean.setProsnonukresident("Yes");//Prospectus redesign 9_DEC_2014
      }
      String oneClickMandatoryFlag = (String) basicFormInfoMap.get("O_ONE_CLIK_FLAG");
      if(!GenericValidator.isBlankOrNull(oneClickMandatoryFlag)) {
        qlFormBean.setOneClickMandatory(oneClickMandatoryFlag);
      }

      String oneClickCheckBox = (String) basicFormInfoMap.get("O_ONE_CHECK_BOX_FLAG");
      if(!GenericValidator.isBlankOrNull(oneClickCheckBox) && "Y".equalsIgnoreCase(oneClickCheckBox)) {
        qlFormBean.setOneClickCheckBox(oneClickCheckBox);
      }

      String userGrades = (String)basicFormInfoMap.get("O_PREDICTED_GRADES");
      if(!GenericValidator.isBlankOrNull(userGrades)) {
        qlFormBean.setPredictedGrades(userGrades);
      }
      ////Prospectus redesign 9_DEC_2014
      ArrayList countryResList = (ArrayList)basicFormInfoMap.get("oc_country_of_res");
       if(countryResList != null && countryResList.size() > 0){
         List countryResidenceList = (ArrayList)countryResList.get(0);
         qlFormBean.setProspectusCountryOfRes(countryResidenceList);
         if(!GenericValidator.isBlankOrNull(qlFormBean.getCountryOfResidence())) //wu516_19112013 oneclick enquiry changes
         {
           qlFormBean.setProsCountryOfRes(qlFormBean.getCountryOfResidence());
           Iterator countryList = countryResidenceList.iterator();
           BasicFormVO basicformvo1 = new BasicFormVO();
           while(countryList.hasNext())
           {
             basicformvo1 = (BasicFormVO) countryList.next();
             if(qlFormBean.getCountryOfResidence().equalsIgnoreCase(basicformvo1.getNationalityValue()))
             {
               qlFormBean.setProsCountryOfResDesc(basicformvo1.getNationalityDesc());
               break;
             }
           }
         }
       }
      //
      ArrayList gradeDetails = (ArrayList)basicFormInfoMap.get("OC_GRADE_OPTION");
      if (gradeDetails != null && gradeDetails.size() > 0) {
         qlFormBean.setProsGradesList(gradeDetails);
       }
      //Modified By Hema.S on 20-Feb-2018 for UCAS new Fees Structure
      ArrayList courseInfoList = (ArrayList) basicFormInfoMap.get("O_COURSE_INFO");
      if (courseInfoList != null && courseInfoList.size() > 0) {
        QuestionAnswerBean questionAnswerBean = (QuestionAnswerBean)courseInfoList.get(0);
        if(GenericValidator.isBlankOrNull(questionAnswerBean.getEngFees())){
        request.setAttribute("englandFeesEmp","YES");
        }
        if(GenericValidator.isBlankOrNull(questionAnswerBean.getNorthIreFees())){
        request.setAttribute("northIreFeesEmp","YES");
        }
        if(GenericValidator.isBlankOrNull(questionAnswerBean.getScotFees())){
        request.setAttribute("scotFeesEmp","YES");
        }
        if(GenericValidator.isBlankOrNull(questionAnswerBean.getWalesFees())){
        request.setAttribute("walesFeesEmp","YES");
        }
        if(GenericValidator.isBlankOrNull(questionAnswerBean.getEuFees())){
        request.setAttribute("euFeesEmp","YES");
        }
        if(GenericValidator.isBlankOrNull(questionAnswerBean.getIntlFees())){
          request.setAttribute("intelFeesEmp","YES");
        }
        if(GenericValidator.isBlankOrNull(questionAnswerBean.getDomesticFees())){
          request.setAttribute("domesticFeesEmp","YES");
        }
        if(GenericValidator.isBlankOrNull(questionAnswerBean.getOtherUKFees())){
          request.setAttribute("otherUKFeesEmp","YES");
        }
        if(GenericValidator.isBlankOrNull(questionAnswerBean.getEngFees())&& GenericValidator.isBlankOrNull(questionAnswerBean.getNorthIreFees()) && GenericValidator.isBlankOrNull(questionAnswerBean.getScotFees()) && GenericValidator.isBlankOrNull(questionAnswerBean.getWalesFees()) && GenericValidator.isBlankOrNull(questionAnswerBean.getEuFees()) && GenericValidator.isBlankOrNull(questionAnswerBean.getIntlFees()) && GenericValidator.isBlankOrNull(questionAnswerBean.getOtherUKFees()) && GenericValidator.isBlankOrNull(questionAnswerBean.getDomesticFees())) {
         request.setAttribute("emptyFees","yes");
        }
        request.setAttribute("courseInfoList", courseInfoList);
      } 
      // New Fees Structure end
      ArrayList collegeInfoList = (ArrayList) basicFormInfoMap.get("O_COLLEGE_INFO");
      if (collegeInfoList != null && collegeInfoList.size() > 0) {
        request.setAttribute("collegeInfoList", collegeInfoList);
      }
      //Added network id to restrict a grades for 27_dec_2015, By Thiyagu G. start
      String networkId = (String)basicFormInfoMap.get("O_NETWORK_ID");
      if(!GenericValidator.isBlankOrNull(networkId)) {
        qlFormBean.setNetworkId(networkId);
      }
      //Added network id to restrict a grades for 27_dec_2015, By Thiyagu G. end
      //
      ArrayList gradeOptions = (ArrayList)basicFormInfoMap.get("pc_grade_details");
      if (gradeOptions != null && gradeOptions.size() > 0) {
        qlFormBean.setProsPredictedGrades(gradeOptions);
        Iterator listItr = gradeOptions.iterator();      
        QuestionAnswerBean qaBean = null;
        while(listItr.hasNext()) {
          qaBean = (QuestionAnswerBean) listItr.next();
          if(!GenericValidator.isBlankOrNull(qaBean.getAttributeId()) && ("197").equals(qaBean.getAttributeId())){
          qlFormBean.setProsGradePointSelected(qaBean.getOptionId());
          }
          if(!GenericValidator.isBlankOrNull(qaBean.getAttributeId()) && ("210").equals(qaBean.getAttributeId())){
          qlFormBean.setProsGradeValueSelected(qaBean.getAttributeValue());
          }
          if(!GenericValidator.isBlankOrNull(qaBean.getAttributeId()) && ("268").equals(qaBean.getAttributeId())){
            if(("Y").equals(qaBean.getAttributeValue())){
              qlFormBean.setProsDoNotGradesChkBox("on");
            }
          }
        }
      }
      //Added this for showing consent flag details By Hema.S on 17_12_2019_REL
      ArrayList clientLeadConsentList = (ArrayList) basicFormInfoMap.get("O_CLIENT_CONSENT_DETAILS");
      if (clientLeadConsentList != null && clientLeadConsentList.size() > 0) {
    	ClientLeadConsentVO clientLeadConsentVO = (ClientLeadConsentVO)clientLeadConsentList.get(0);
    	request.setAttribute("consentFlag", clientLeadConsentVO.getClientMarketingConsent());
    	request.setAttribute("consentText", clientLeadConsentVO.getConsentText());
        request.setAttribute("clientLeadConsentList", clientLeadConsentList);
      }
      //
      ArrayList bulkProspectusClientleadConsentList = (ArrayList) basicFormInfoMap.get("O_CLIENT_CONSENT_ARR");
      ArrayList<ClientLeadConsentVO> consentList = bulkProspectusClientleadConsentList;
      if (bulkProspectusClientleadConsentList != null && bulkProspectusClientleadConsentList.size() > 0) {
        String collegeIdValues = consentList.stream().map(ins -> ins.getCollegeId()).collect(Collectors.joining("###"));
        request.setAttribute("collegeIdValues", collegeIdValues);
        request.setAttribute("bulkProspectusClientleadConsentList", bulkProspectusClientleadConsentList);
      }
      //
      request.setAttribute("refferalUrl", qlFormBean.getRefferalUrl());
      if("download-prospectus".equalsIgnoreCase(qlFormBean.getTypeOfEnquiry())){
          List dpList = new ArrayList();
          dpList = new QLCommon().getDpDetails(request,qlFormBean.getSubOrderItemId());
          ProspectusBean dpBean = (ProspectusBean)dpList.get(0);
          String downloadRequetedUrl = dpBean.getDpRedirectionURL();
          String getProspectusFlag = dpBean.getGetProspectusFlag();
          request.setAttribute("downloadRequetedUrl", downloadRequetedUrl);
          request.setAttribute("getProspectusFlag",getProspectusFlag);
      }
      new GlobalFunction().getInteractivePodInfo(collegeId, request);
    }
    
    private void dobMethod(QLFormBean qlformBean){
      LabelValueBean dobBean = null;
      ArrayList dateList = new ArrayList();
      for(int i = 1; i<=31; i++){
        dobBean = new LabelValueBean();
        if(i < 10){
          dobBean.setLabel("0"+String.valueOf(i));
          dobBean.setValue("0"+String.valueOf(i));
        }else{
          dobBean.setLabel(String.valueOf(i));
          dobBean.setValue(String.valueOf(i));
        }
        dateList.add(dobBean);
      }
      ArrayList monthList = new ArrayList();
      String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
      for(int i = 1; i<=12; i++){
        dobBean = new LabelValueBean();
        if(i < 10){
          dobBean.setLabel(months[i-1]);
          dobBean.setValue("0"+String.valueOf(i));
        }else{
          dobBean.setLabel(months[i-1]);
          dobBean.setValue(String.valueOf(i));
        }
        monthList.add(dobBean);
      }
      ArrayList yearList = new ArrayList();
      Calendar ca = new GregorianCalendar();
      int currentYear = (ca.get(Calendar.YEAR)-13);
      for(int i = 1900; i<=currentYear; i++){ //Changed years from 1970 to 1900 for 19_Apr_2016.
          dobBean = new LabelValueBean();
          dobBean.setLabel(String.valueOf(i));
          dobBean.setValue(String.valueOf(i));
          yearList.add(dobBean);
      }
      qlformBean.setDateList(dateList);
      qlformBean.setMonthList(monthList);
      qlformBean.setYearList(yearList);
    }
    
    public Object[][] setGradeAttributeValues(QLFormBean qlformBean){
    String donNotGradeValue = null;
     if(!GenericValidator.isBlankOrNull(qlformBean.getProsDoNotGradesChkBox()) && ("on").equals(qlformBean.getProsDoNotGradesChkBox())){
        donNotGradeValue = "Y";        
      }
      qlformBean.setProsDoNotGradesChkBox(donNotGradeValue);
      if(GenericValidator.isBlankOrNull(qlformBean.getProsGradePointSelected())){
        qlformBean.setProsGradePointSelected(null);
      }
      if(GenericValidator.isBlankOrNull(qlformBean.getProsGradeValueSelected())){
        qlformBean.setProsGradeValueSelected(null);
      }
      Object[][] attrValues = new Object[3][6];
      attrValues[0][0] = null;
      attrValues[0][1] = null;
      attrValues[0][2] = "197";
      attrValues[0][3] = null;
      attrValues[0][4] = qlformBean.getProsGradePointSelected();
      attrValues[0][5] = null;
      attrValues[1][0] = null;
      attrValues[1][1] = null;
      attrValues[1][2] = "210";
      attrValues[1][3] = qlformBean.getProsGradeValueSelected();
      attrValues[1][4] = null;
      attrValues[1][5] = null;
      attrValues[2][0] = null;
      attrValues[2][1] = null;
      attrValues[2][2] = "268";
      attrValues[2][3] = donNotGradeValue;
      attrValues[2][4] = null;
      attrValues[2][5] = null;
      return attrValues;
    }
    
 /**
  * @validateFormElements method to validate the form text area...
  * @param qlformBean
  * @param request
  * @return status
  * @author Prabhakaran V.
  */
/*  private boolean validateFormElements(QLFormBean qlformBean, HttpServletRequest request) {
    boolean status = false;
    ActionErrors errors = new ActionErrors();
    errors.clear();
    String userID = new SessionData().getData(request, "y");
    if(!GenericValidator.isBlankOrNull(qlformBean.getEmailAddress())){
      String existUserId = new GlobalFunction().checkEmailExist(qlformBean.getEmailAddress(), request);
      System.out.println("existUserId >> "+ existUserId + ",,, userID " + userID);
      if(("0".equalsIgnoreCase(userID) || userID == null) && (existUserId != null && !"".equalsIgnoreCase(existUserId) && !"0".equalsIgnoreCase(existUserId)))
      {
        errors.add("wuni.error.ql.email.exists.error", new ActionMessage("wuni.error.profile.existing.emailid", qlformBean.getEmailAddress()));
        request.setAttribute("errEmailExists", "Y");
      }
    }
    System.out.println("message >> in the action erros" + qlformBean.getMessage());
    if (!GenericValidator.isBlankOrNull(qlformBean.getMessage())) {
      String[] vulnerableKeywords = ResourceBundle.getBundle("com.resources.ApplicationResources").getString("wuni.vulnerable.keywords").split(GlobalConstants.SPLIT_CONSTANT);
      for (int i = 0; i < vulnerableKeywords.length; i++) {
        if ((qlformBean.getMessage()).indexOf(vulnerableKeywords[i]) != -1) {
          errors.add("wuni.error.ql.text.area.error", new ActionMessage("wuni.error.ql.text.area.error"));
          request.setAttribute("errTextArea", "Y");
          break;
        }
      }
    }
   // new GlobalFunction().getInteractivePodInfo(qlformBean.getCollegeId(), request);
    //saveErrors(request, errors);
    if (errors.size() > 0) {
      status = true;
    }
    System.out.println("status >> " + status);
    return status;
  }*/
}