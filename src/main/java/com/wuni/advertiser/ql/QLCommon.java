package com.wuni.advertiser.ql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.naming.InitialContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import org.apache.commons.validator.GenericValidator;
import WUI.registration.bean.RegistrationBean;
import WUI.utilities.CommonFunction;
import WUI.utilities.CommonUtil;
import WUI.utilities.CookieManager;
import WUI.utilities.GlobalConstants;
import WUI.utilities.GlobalFunction;
import WUI.utilities.SessionData;
import com.wuni.advertiser.prospectus.form.ProspectusBean;
import com.wuni.advertiser.ql.form.QLFormBean;
import com.wuni.util.RequestResponseUtils;
import com.wuni.util.sql.DataModel;

public class QLCommon {

 
  public String getUserId(QLFormBean qlFormBean, HttpServletRequest request, HttpServletResponse response, ServletContext context){
    String userId = "";
    CommonFunction common = new CommonFunction();
    GlobalFunction global = new GlobalFunction();
    SessionData sessionData = new SessionData();
    String prospectusFlag = (String)request.getAttribute("fromblkprospectus");//9_DEC_2014 Redesign
    
    try{
      HttpSession session = request.getSession();
      userId = global.checkEmailExist(qlFormBean.getEmailAddress(), request);
      String newsLetterFlag = !GenericValidator.isBlankOrNull(qlFormBean.getNewsLetter()) ? "Y" : "N";
      if(("prospectus").equals(prospectusFlag)){//Added for bulk prospectus permit email flag fix
        newsLetterFlag = GenericValidator.isBlankOrNull(qlFormBean.getNewsLetter()) ? "Y" : qlFormBean.getNewsLetter();      
      }      
      List questionList = basicFormRegQuestion(qlFormBean);
      ArrayList questionAnsList = new ArrayList();
      for (int i = 0; i < questionList.size(); i++) {
        RegistrationBean rbean = (RegistrationBean)questionList.get(i);
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && rbean.getFiledId().equalsIgnoreCase("1")) {
          rbean.setSelectedValue(qlFormBean.getEmailAddress());
        }
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && rbean.getFiledId().equalsIgnoreCase("4")) {
          rbean.setSelectedValue(qlFormBean.getFirstName());
        }
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && rbean.getFiledId().equalsIgnoreCase("5")) {
          rbean.setSelectedValue(qlFormBean.getLastName());
        }          
        if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && rbean.getFiledId().equalsIgnoreCase("1467")) {   
          rbean.setSelectedValue(qlFormBean.getYeartoJoinCourse());
        }
          if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && rbean.getFiledId().equalsIgnoreCase("17")) {            
            rbean.setSelectedValue(qlFormBean.getNationality());
          }
          if("Y".equalsIgnoreCase(qlFormBean.getMobileFlag()) || ("prospectus").equals(prospectusFlag)){//9_DEC_2014 Redesign
            if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && rbean.getFiledId().equalsIgnoreCase("62")) {            
              rbean.setSelectedValue(qlFormBean.getCountryOfResidence());
            }
          }
          if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && rbean.getFiledId().equalsIgnoreCase("11")) {            
            if (!qlFormBean.isNonukresident()) {
              rbean.setSelectedValue(qlFormBean.getPostCode());
            } else {
              rbean.setSelectedValue("");
            }
          }         
          if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && rbean.getFiledId().equalsIgnoreCase("9")) {   
            rbean.setSelectedValue(qlFormBean.getTown());
          }
          if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && rbean.getFiledId().equalsIgnoreCase("7")) {   
            rbean.setSelectedValue(qlFormBean.getAddress1());
          }
          if (!GenericValidator.isBlankOrNull(rbean.getFiledId()) && rbean.getFiledId().equalsIgnoreCase("8")) {   
            rbean.setSelectedValue("Address line 2".equalsIgnoreCase(qlFormBean.getAddress2()) ? "" : qlFormBean.getAddress2());
        }
        questionAnsList.add(rbean);
      }
      session.setAttribute("DynamicQuestionList", questionAnsList);
      if(userId == null || "".equalsIgnoreCase(userId) || "0".equalsIgnoreCase(userId)){
        sessionData.removeData(request, response, "y");
        session.removeAttribute("userInfoList");
        String fileName = getRandomImage();
        RegistrationBean regBean = new RegistrationBean();
        
        userId = updateBasicFormRegDetails(userId, request, response, newsLetterFlag, "new", qlFormBean);
        if(!GenericValidator.isBlankOrNull(userId)){//5_AUG_2014
          request.setAttribute("GA_newUserRegister", userId );
        }
        //new GlobalFunction().updateBasketContent(request, userId);
        regBean.setSelectFileUpload("");
        regBean.setDefaultImageUpload(fileName);
        String voucherCode = new CommonUtil().getMusicVocher(userId, request); 
        if (voucherCode != null && voucherCode.trim().length() > 0) {
          request.setAttribute("voucherCode", voucherCode);
        }
        common.sendRegistrtionMail(userId, voucherCode, request); /* email send to the user after registration */
        //new CommonFunction().loadUserLoggedInformation(request, context, response);
      }else
      //if (userId != null && !userId.equals("0") && !userId.equalsIgnoreCase("null") && userId.trim().length() > 0)
      {
        userId = updateBasicFormRegDetails(userId, request, response, newsLetterFlag, "exist", qlFormBean);
      }
    }catch(Exception e){
      e.printStackTrace();
    }
    return userId;
  }
 
  /**
    *   This function is used to generate the dynamic basic form registration question.
    * @param request
    * @return Dynamic registartion qustionas Collection object.
    */
  public List basicFormRegQuestion(QLFormBean qlformbean) {
    ResultSet resultset = null;
    Vector vector = new Vector();
    ArrayList list = new ArrayList();
    RegistrationBean bean = null;
    DataModel datamodel = new DataModel();
    try {
      String formName = "";
      if("send-college-email".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){
        formName = "BASIC EMAIL WU";
      }else if("order-prospectus".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){
        if("Y".equalsIgnoreCase(qlformbean.getMobileFlag()))        {
          formName = "MOBILE BASIC RP WU";
        }else{
          formName = "BASIC RP WU";  
        }
      }else if("download-prospectus".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){
        formName = "BASIC DP WU";
      }
      vector.add(formName);
      resultset = datamodel.getResultSet("wu_review_pkg.registration_page_fn", vector);
     while (resultset.next()) {
        bean = new RegistrationBean();
        bean.setFiledId(resultset.getString("FIELD_ID"));
        bean.setFieldType(resultset.getString("FIELD_TYPE"));
        bean.setQuestionLabel(resultset.getString("DESCRIPTION"));
        bean.setDisplaySeqId(resultset.getString("DISPLAY_SEQ"));
        bean.setFieldRequired(resultset.getString("FIELD_REQ"));
        bean.setPerRowCount(resultset.getString("PER_ROW_CNT"));
        bean.setDefaultValue(resultset.getString("DEFAULT_VALUE"));
        bean.setTemplateIdHint(resultset.getString("TEMPLATE_ID"));
        bean.setRowStatus(resultset.getString("ROW_STATUS"));
        bean.setMaxSize(resultset.getString("MAX_SIZE"));
        bean.setDisplaySize(resultset.getString("DISPLAY_SIZE"));
        bean.setSelectedValue("");
        if (bean.getQuestionLabel() != null && (bean.getQuestionLabel().equalsIgnoreCase("confirm email address") || bean.getQuestionLabel().equalsIgnoreCase("password") || bean.getQuestionLabel().equalsIgnoreCase("nickname"))) {
          // list.add(bean);
          continue;
        }
        if (resultset.getString("FIELD_TYPE") != null && (resultset.getString("FIELD_TYPE").equalsIgnoreCase("dropdown"))) {
          ResultSet dropdownresultset = null;
          try {
            vector.clear();
            vector.add(resultset.getString("FIELD_ID"));
            vector.add("YES");
            dropdownresultset = datamodel.getResultSet("wu_review_pkg.get_drop_down_list_fn", vector);
            List optionvalue = new ArrayList();
            RegistrationBean optionvaluebean = null;
            while (dropdownresultset.next()) {
              optionvaluebean = new RegistrationBean();
              optionvaluebean.setOptionId(dropdownresultset.getString("Value"));
              optionvaluebean.setOptionText(dropdownresultset.getString("description"));
              optionvalue.add(optionvaluebean);
            }
            bean.setOptionValues(optionvalue);
          } catch (Exception exception) {
            exception.printStackTrace();
          } finally {
            datamodel.closeCursor(dropdownresultset);
          }
        }
        if (resultset.getString("FIELD_TYPE") != null && (resultset.getString("FIELD_TYPE").equalsIgnoreCase("checkbox"))) {
          List optionvalue = new ArrayList();
          RegistrationBean optionvaluebean = new RegistrationBean();
          ;
          optionvaluebean.setOptionId("M");
          optionvaluebean.setOptionText("Male");
          optionvalue.add(optionvaluebean);
          optionvaluebean = new RegistrationBean();
          optionvaluebean.setOptionId("F");
          optionvaluebean.setOptionText("Female");
          optionvalue.add(optionvaluebean);
          bean.setOptionValues(optionvalue);
        }
        String EXTRA_ATTRIB = resultset.getString("EXTRA_ATTRIB");
        if (EXTRA_ATTRIB != null && EXTRA_ATTRIB.trim().length() > 0) {
          ResultSet dropdownresultset = null;
          try {
            Vector yearOfPassing = new Vector();
            yearOfPassing.add(resultset.getString("FIELD_ID"));
            dropdownresultset = datamodel.getResultSet(EXTRA_ATTRIB, yearOfPassing);
            List optionvalue = new ArrayList();
            RegistrationBean optionvaluebean = null;
            while (dropdownresultset.next()) {
              optionvaluebean = new RegistrationBean();
              optionvaluebean.setOptionId(dropdownresultset.getString("year_of_passing"));
              optionvaluebean.setOptionText(dropdownresultset.getString("year_of_passing"));
              optionvalue.add(optionvaluebean);
            }
            bean.setOptionValues(optionvalue);
          } catch (Exception exception) {
            exception.printStackTrace();
          } finally {
            datamodel.closeCursor(dropdownresultset);
          }
        }
        list.add(bean);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    } finally {
      datamodel.closeCursor(resultset);
    }
    return list;
  }
  
  /**
    *   This function is used to update the user registration related information from ql basic form.
    * @param request
    * @return userid as String.
    * @throws ServletException
    * @throws IOException
    */
  public String updateBasicFormRegDetails(String userId1, HttpServletRequest request, HttpServletResponse response, String newsLetterFlag, String flag, QLFormBean qlformbean) throws ServletException, IOException {
    String userId = "";
    RegistrationBean bean = null;
    CommonUtil commonUtil = new CommonUtil();
    CookieManager cookieManager = new CookieManager();
    int rowNum = 4;
    HttpSession session = request.getSession();
    ResultSet resultset = null;
    ArrayList questionlist = (ArrayList)session.getAttribute("DynamicQuestionList");
    String answerId[] = new String[questionlist.size() + 8];
    String answerText[] = new String[questionlist.size() + 8];
    answerId[0] = "x";
    answerText[0] = new SessionData().getData(request, "x");
    answerId[1] = "y";
    if("new".equalsIgnoreCase(flag))
    {
      if (new SessionData().getData(request, "y") != null && (!(new SessionData().getData(request, "y").equals("0")) || new SessionData().getData(request, "y").trim().length() > 0)) {
        answerText[1] = new SessionData().getData(request, "y"); //user Id
      } else {
        answerText[1] = ""; //user Id 
      }
    }else
    {
      answerText[1] = userId1;  
    }
    answerId[2] = "a";
    answerText[2] = GlobalConstants.WHATUNI_AFFILATE_ID;
    String formName = "";
    String formId = "";
    if("send-college-email".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){
      formName = "BASIC EMAIL WU";
      formId = "204";
    }else if("order-prospectus".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){
      formName = "BASIC RP WU";
      formId = "203";
    }else if("download-prospectus".equalsIgnoreCase(qlformbean.getTypeOfEnquiry())){
      formName = "BASIC DP WU";
      formId = "205";
    }
    answerId[3] = formId;
    answerText[3] = formName; //form ID
    if (questionlist != null) {
      Iterator questioniterator = questionlist.iterator();
      while (questioniterator.hasNext()) {
        bean = new RegistrationBean();
        bean = (RegistrationBean)questioniterator.next();
        String questionanswerId = bean.getFiledId();
        String questionanswerText = bean.getSelectedValue() != null? bean.getSelectedValue().trim(): bean.getSelectedValue();
        answerId[rowNum] = "pm_" + questionanswerId;
        answerText[rowNum] = questionanswerText;
        rowNum++;
      }
      // Random password generator 
      if("new".equalsIgnoreCase(flag)){
        String password = new CommonFunction().getRandomPassword();
        answerId[rowNum] = "pm_2";
        answerText[rowNum] = password;
      }
      rowNum++;
      answerId[rowNum] = "pm_61";
      answerText[rowNum] = "";
      rowNum++;
      answerId[rowNum] = "pm_346";
      answerText[rowNum] = "";
        rowNum++;
        answerId[rowNum] = "newsletter";
        answerText[rowNum] = newsLetterFlag;
        
      if (answerId != null && answerText != null) {
        Connection connection = null;
        OracleCallableStatement oraclestmt = null;
        try {
          DataSource datasource = null;
          try {
            InitialContext ic = new InitialContext();
            datasource = (DataSource)ic.lookup("whatuni");
          } catch (Exception exception) {
            exception.printStackTrace();
          }
          connection = datasource.getConnection();
          String query = "{ ?= call hot_admin.hc_maintain_user3.create_user_reg_fn(?,?,?,?,?,?,?) }";
          oraclestmt = (OracleCallableStatement)connection.prepareCall(query);
          oraclestmt.registerOutParameter(1, OracleTypes.CURSOR);
          ArrayDescriptor descriptor = ArrayDescriptor.createDescriptor("T_USERREGTABTYPE", connection);
          ARRAY array_1 = new ARRAY(descriptor, connection, answerId);
          ARRAY array_2 = new ARRAY(descriptor, connection, answerText);
          oraclestmt.setARRAY(2, array_1);
          oraclestmt.setARRAY(3, array_2);
          oraclestmt.setString(4, new RequestResponseUtils().getRemoteIp(request));
          oraclestmt.setString(5, request.getHeader("user-agent"));
          oraclestmt.setString(6, CommonUtil.getRequestedURL(request)); //Change the getRequestURL by Prabha on 28_Jun_2016
          oraclestmt.setString(7, request.getHeader("referer"));
          oraclestmt.setString(8, new SessionData().getData(request, "userTrackId"));          
          oraclestmt.execute();
          resultset = (ResultSet)oraclestmt.getObject(1);
          while (resultset.next()) {
            userId = resultset.getString("user_id");
            new SessionData().addData(request, response, "y", resultset.getString("user_id"));
            new SessionData().addData(request, response, "x", resultset.getString("session_id"));
          }
          if(!GenericValidator.isBlankOrNull(userId) && !"0".equals(userId)){
             //Encryting to user id to base64 and adding it in cookie by Sangeeth.S for whatuniGO journey
             commonUtil.addEncryptedCookieUserId(request, userId, response);          
           }
           // 
        } catch (Exception exception) {
          exception.printStackTrace();
        } finally {
          try {
            if (oraclestmt != null) {
              oraclestmt.close();
            }
            if (resultset != null) {
              resultset.close();
            }
            if (connection != null) {
              connection.close();
            }
          } catch (Exception closeException) {
            closeException.printStackTrace();
          }
        }
      }
    }
    return userId;
  }

  private String getRandomImage() {
    int randomNoLimit = GlobalConstants.DEFAULT_IMAGE_LENGTH;
    List imageList = getImageList();
    Random r = new Random();
    int randint = r.nextInt(randomNoLimit);
    return (String)imageList.get(randint);
  }

  private List getImageList() {
    List list = new ArrayList();
    ResourceBundle bundle = ResourceBundle.getBundle("com.resources.ApplicationResources");
    for (int cou = 1; cou <= 50; cou++) {
      String propertyName = "wuni.default.images_id_" + String.valueOf(cou);
      list.add(bundle.getString(propertyName));
    }
    return list;
  }
  
  public List getDpDetails(HttpServletRequest request, String suborderItemId){
      ResultSet resultSet = null;
      DataModel dataModel = new DataModel();
      Vector vector = new Vector();
      ProspectusBean prosBean = null;
      ArrayList nationalityList = new ArrayList();
      try {
        vector.add(suborderItemId);
        resultSet = dataModel.getResultSet("wu_cpe_advert_pkg.get_dp_details_fn", vector);
        while (resultSet.next()) {
          prosBean = new ProspectusBean();
          prosBean.setDpRedirectionURL(resultSet.getString("dpurl"));
          prosBean.setGetProspectusFlag(resultSet.getString("get_prospectus_flag"));
          nationalityList.add(prosBean);
        }
      } catch (Exception exception) {
        exception.printStackTrace();
      } finally {
        dataModel.closeCursor(resultSet);
      }
      return nationalityList;
  }
  
  public String getStudyLevelFlagFromProspectus(HttpServletRequest request){//9_DEC_2014 Redesign
    DataModel dataModel = new DataModel();
    CommonFunction common = new CommonFunction();
    String prosBasketId = common.checkProspectusCookieStatus(request);
    Vector parameters = new Vector();
    parameters.add(prosBasketId);
    String flag = "N";
    try {
      flag = dataModel.getString("WU_UTIL_PKG.GET_UGPG_PROSBASKET_FLAG_FN", parameters);
    } catch (Exception exception) {
      exception.printStackTrace();
    }
    return flag;
  }
  
}
