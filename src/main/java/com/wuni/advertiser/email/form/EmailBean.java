package com.wuni.advertiser.email.form;

import java.util.List;

import org.apache.struts.action.ActionForm;

public class EmailBean{

  private String collegeId = "";
  private String collegeName = "";
  private String studyLevel = "M";
  private String firstName = "";
  private String lastName = "";
  private String postCode = "";
  private String emailAddress = null;
  boolean nonukresident = false;
  private String yearofPassing = null;
  private String aboutMe = null;
  private String gender = null;
  private String nationality = null;
  private String openDays = "";
  private String emailSubject = "";
  private String emailContent = "";
  private boolean termsAndCondition = false;
  private boolean receiveInfo = false;
  private List yearOfUniEntryList = null;
  private List yearOfUniFutureStudList = null;
  private String yearOfUniEntry = null;
  private String yearOfUniFutureStud = null;
  private String nationalityDesc = "";
  private String nationalityValue = "";
  private String dateOfBirth = "";
  private String mobileNumber = "";
  private boolean newsLetter = false;
  private String subOrderItemId = "";
  private String subOrderProspectusWebform = "";
  private String subOrderProspectus = "";
  private String subOrderWebsite = "";
  private String subOrderEmail = "";
  private String subOrderEmailWebform = "";
  private List studyLevelList = null;
  private String studyLevelId = "";
  private String yeartoJoinCourse = null;
  
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }

  public String getCollegeId() {
    return collegeId;
  }

  public void setCollegeName(String collegeName) {
    this.collegeName = collegeName;
  }

  public String getCollegeName() {
    return collegeName;
  }

  public void setStudyLevel(String studyLevel) {
    this.studyLevel = studyLevel;
  }

  public String getStudyLevel() {
    return studyLevel;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public String getPostCode() {
    return postCode;
  }

  public void setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public void setNonukresident(boolean nonukresident) {
    this.nonukresident = nonukresident;
  }

  public boolean isNonukresident() {
    return nonukresident;
  }

  public void setYearofPassing(String yearofPassing) {
    this.yearofPassing = yearofPassing;
  }

  public String getYearofPassing() {
    return yearofPassing;
  }

  public void setAboutMe(String aboutMe) {
    this.aboutMe = aboutMe;
  }

  public String getAboutMe() {
    return aboutMe;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return gender;
  }

  public void setNationality(String nationality) {
    this.nationality = nationality;
  }

  public String getNationality() {
    return nationality;
  }

  public void setOpenDays(String openDays) {
    this.openDays = openDays;
  }

  public String getOpenDays() {
    return openDays;
  }

  public void setEmailSubject(String emailSubject) {
    this.emailSubject = emailSubject;
  }

  public String getEmailSubject() {
    return emailSubject;
  }

  public void setEmailContent(String emailContent) {
    this.emailContent = emailContent;
  }

  public String getEmailContent() {
    return emailContent;
  }

  public void setTermsAndCondition(boolean termsAndCondition) {
    this.termsAndCondition = termsAndCondition;
  }

  public boolean isTermsAndCondition() {
    return termsAndCondition;
  }

  public void setYearOfUniEntryList(List yearOfUniEntryList) {
    this.yearOfUniEntryList = yearOfUniEntryList;
  }

  public List getYearOfUniEntryList() {
    return yearOfUniEntryList;
  }

  public void setYearOfUniFutureStudList(List yearOfUniFutureStudList) {
    this.yearOfUniFutureStudList = yearOfUniFutureStudList;
  }

  public List getYearOfUniFutureStudList() {
    return yearOfUniFutureStudList;
  }

  public void setYearOfUniEntry(String yearOfUniEntry) {
    this.yearOfUniEntry = yearOfUniEntry;
  }

  public String getYearOfUniEntry() {
    return yearOfUniEntry;
  }

  public void setYearOfUniFutureStud(String yearOfUniFutureStud) {
    this.yearOfUniFutureStud = yearOfUniFutureStud;
  }

  public String getYearOfUniFutureStud() {
    return yearOfUniFutureStud;
  }

  public void setNationalityDesc(String nationalityDesc) {
    this.nationalityDesc = nationalityDesc;
  }

  public String getNationalityDesc() {
    return nationalityDesc;
  }

  public void setNationalityValue(String nationalityValue) {
    this.nationalityValue = nationalityValue;
  }

  public String getNationalityValue() {
    return nationalityValue;
  }

  public void setReceiveInfo(boolean receiveInfo) {
    this.receiveInfo = receiveInfo;
  }

  public boolean isReceiveInfo() {
    return receiveInfo;
  }

  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  public void setMobileNumber(String mobileNumber) {
    this.mobileNumber = mobileNumber;
  }

  public String getMobileNumber() {
    return mobileNumber;
  }

  public void setNewsLetter(boolean newsLetter) {
    this.newsLetter = newsLetter;
  }

  public boolean isNewsLetter() {
    return newsLetter;
  }

  public void setSubOrderItemId(String subOrderItemId) {
    this.subOrderItemId = subOrderItemId;
  }

  public String getSubOrderItemId() {
    return subOrderItemId;
  }

  public void setSubOrderProspectusWebform(String subOrderProspectusWebform) {
    this.subOrderProspectusWebform = subOrderProspectusWebform;
  }

  public String getSubOrderProspectusWebform() {
    return subOrderProspectusWebform;
  }

  public void setSubOrderProspectus(String subOrderProspectus) {
    this.subOrderProspectus = subOrderProspectus;
  }

  public String getSubOrderProspectus() {
    return subOrderProspectus;
  }

  public void setSubOrderWebsite(String subOrderWebsite) {
    this.subOrderWebsite = subOrderWebsite;
  }

  public String getSubOrderWebsite() {
    return subOrderWebsite;
  }

  public void setSubOrderEmail(String subOrderEmail) {
    this.subOrderEmail = subOrderEmail;
  }

  public String getSubOrderEmail() {
    return subOrderEmail;
  }

  public void setSubOrderEmailWebform(String subOrderEmailWebform) {
    this.subOrderEmailWebform = subOrderEmailWebform;
  }

  public String getSubOrderEmailWebform() {
    return subOrderEmailWebform;
  }

  public void setStudyLevelList(List studyLevelList) {
    this.studyLevelList = studyLevelList;
  }

  public List getStudyLevelList() {
    return studyLevelList;
  }

  public void setStudyLevelId(String studyLevelId) {
    this.studyLevelId = studyLevelId;
  }

  public String getStudyLevelId() {
    return studyLevelId;
  }

  public void setYeartoJoinCourse(String yeartoJoinCourse) {
    this.yeartoJoinCourse = yeartoJoinCourse;
  }

  public String getYeartoJoinCourse() {
    return yeartoJoinCourse;
  }

}
/////////////////////////////////////////////   E  N  D   O  F   F  I   L   E  /////////////////////////////////////////    
