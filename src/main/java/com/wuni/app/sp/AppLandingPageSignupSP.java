package com.wuni.app.sp;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : AppLandingPageSignupSP.java
 * Description   : ALP page signup from this SP..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Prabhakaran V.           1.0             08.08.2017      First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */
public class AppLandingPageSignupSP extends StoredProcedure {
  public AppLandingPageSignupSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_APP_LANDING_PAGE_PRC);
    declareParameter(new SqlParameter("p_email", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_first_name", OracleTypes.VARCHAR));
    declareParameter(new SqlParameter("p_last_name", OracleTypes.VARCHAR));
    declareParameter(new SqlOutParameter("p_return_msg", OracleTypes.VARCHAR));
    compile();
  }
  public Map execute(RegistrationBean registrationBean) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      inMap.put("p_email", registrationBean.getEmailAddress());
      inMap.put("p_first_name", registrationBean.getFirstName());
      inMap.put("p_last_name", registrationBean.getLastName());
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
