package com.wuni.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : AppFaqPageController.java
 * Description   : Controller class for ALP FAQ page..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Thiyagu G.              1.0             05_Sep_2017      First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

@Controller
public class AppFaqController {
	
  @RequestMapping(value = "/whatuni-mobile-app/faq", method = {RequestMethod.GET}) 
  public ModelAndView getWhatuniMobileAppFaq(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    return new ModelAndView("appFaqPage");
  }
}
