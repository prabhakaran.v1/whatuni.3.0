package com.wuni.app.controller;

import java.io.IOException;
import java.io.Writer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import java.util.*;
import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.valueobjects.AboutUsPagesVO;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : AppContactUsController.java
 * Description   : Controller class for ALP FAQ page..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Thiyagu G.              1.0             05_Sep_2017      First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

@Controller
public class AppContactUsController {
	
  @RequestMapping(value = "/whatuni-mobile-app/contact-us", method = {RequestMethod.GET, RequestMethod.POST}) 
  public ModelAndView getWhatuniMobileAppContactUs(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    String forwardString = null;
    AboutUsPagesVO aboutUsPagesVO = new AboutUsPagesVO();
    String actionFlag = request.getParameter("actionFlag");
    if (actionFlag != null && "SENDMAIL".equalsIgnoreCase(actionFlag)) {
      aboutUsPagesVO.setFirstName(request.getParameter("firstName"));
      aboutUsPagesVO.setLastName(request.getParameter("lastName"));
      aboutUsPagesVO.setEmailAddress(request.getParameter("emailAddress"));
      aboutUsPagesVO.setMessage(request.getParameter("message"));
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      Map schoolvistsmap = (Map) commonBusiness.sendAboutUsEmails(aboutUsPagesVO, "contactus");
      String suMsg = "Thanks,\n" + 
                     "Your message has been received, we'll be in touch with you shortly";
      setResponseText(response, suMsg);
      return null;      
    } else {
      forwardString = "appContactUsPage";
    }
    return new ModelAndView(forwardString);
  }
  
  /**
   * @setResponseText this method to get the response text
   * @param response
   * @param responseMessage
   */
  
  private void setResponseText(HttpServletResponse response, String responseMessage) {
    try {
      response.setContentType("TEXT/PLAIN");
      Writer writer = response.getWriter();
      writer.write(responseMessage.toString());
    } catch (IOException exception) {
      exception.printStackTrace();
    }
  }
}
