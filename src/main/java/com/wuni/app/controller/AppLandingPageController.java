package com.wuni.app.controller;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.registration.bean.RegistrationBean;
import WUI.utilities.GlobalConstants;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.layer.util.SpringUtilities;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 * Class         : AppLandingPageController.java
 * Description   : Controller class for ALP page..
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	   Change
 * *************************************************************************************************************************************
 * Prabhakaran V.           1.0             08.08.2017      First draft
 *
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 *
 */

@Controller
public class AppLandingPageController {
  
  @RequestMapping(value = {"/whatuni-mobile-app", "/whatuni-mobile-app-ajax"}, method = {RequestMethod.GET}) 
  public ModelAndView getWhatuniMobileApp(ModelMap model, HttpServletRequest request, HttpServletResponse response, HttpSession session) throws Exception {
    String actionFlag = request.getParameter("actionFlag");
    if ("SIGNUP".equalsIgnoreCase(actionFlag)) {
      ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
      RegistrationBean registerBean = new RegistrationBean();
      String firstName = !GenericValidator.isBlankOrNull(request.getParameter("firstName"))? request.getParameter("firstName"): "";
      String lastName = !GenericValidator.isBlankOrNull(request.getParameter("lastName"))? request.getParameter("lastName"): "";
      String email = !GenericValidator.isBlankOrNull(request.getParameter("emailAddress"))? request.getParameter("emailAddress"): "";
      registerBean.setFirstName(firstName);
      registerBean.setLastName(lastName);
      registerBean.setEmailAddress(email);
      String returnMsg = appUserSignup(registerBean, commonBusiness);
      if (!GenericValidator.isBlankOrNull(returnMsg)) {
        setResponseText(response, returnMsg);
        return null;
      } else {
        return new ModelAndView("appSuccessPage");
      }
    } else {
      return new ModelAndView("appSuccessPage");  
    }  
  }
  
  /**
   * @setResponseText this method to get the response text
   * @param response
   * @param responseMessage
   */
  
  private void setResponseText(HttpServletResponse response, String responseMessage) {
    try {
      response.setContentType("TEXT/PLAIN");
      Writer writer = response.getWriter();
      writer.write(responseMessage.toString());
    } catch (IOException exception) {
      exception.printStackTrace();
    }
  }
  
  /**
   * @appUserSignup this method to get the ALP signup return message
   * @param registerBean
   * @param commonBusiness
   * @return buffer as return message
   * @throws Exception
   */
  
  public String appUserSignup(RegistrationBean registerBean, ICommonBusiness commonBusiness) throws Exception {
    String buffer = "";
    Map resultMap = commonBusiness.appLandingPageSignUp(registerBean);
    if (resultMap != null) {
      String returnMessage = (String)resultMap.get("p_return_msg");
      if (!GenericValidator.isBlankOrNull(returnMessage)) {
        buffer += returnMessage;
        buffer += GlobalConstants.SPLIT_CONSTANT; //##SPLIT##
      }
    }
    return buffer;
  }
}


