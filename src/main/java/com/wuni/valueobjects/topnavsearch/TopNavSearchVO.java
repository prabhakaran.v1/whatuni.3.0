package com.wuni.valueobjects.topnavsearch;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TopNavSearchVO {
  private String userId = null;
  private String trackSessionId = null;   
  private String subjectName = null;
  private String subjectUrl = null;
  private String regionName = null;
  private String regionUrlText = null;
  private String regionId = null;
  private String keytopics = null;
  private String keyTopicUrl = null;
  private String qualDesc = null;
  private String qualCode = null;
  private String urlText = null;  
  //private String clearingUserType = null;
}
