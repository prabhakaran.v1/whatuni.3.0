package com.wuni.valueobjects;

public class DownloadPdfVO
{
  private String pdfId = null;
  private String pdfName = null;
  private String pdfUrl = null;
  private String guideType = null;

  public void setPdfId(String pdfId)
  {
    this.pdfId = pdfId;
  }

  public String getPdfId()
  {
    return pdfId;
  }

  public void setPdfName(String pdfName)
  {
    this.pdfName = pdfName;
  }

  public String getPdfName()
  {
    return pdfName;
  }

  public void setPdfUrl(String pdfUrl)
  {
    this.pdfUrl = pdfUrl;
  }

  public String getPdfUrl()
  {
    return pdfUrl;
  }

  public void setGuideType(String guideType)
  {
    this.guideType = guideType;
  }

  public String getGuideType()
  {
    return guideType;
  }

}
