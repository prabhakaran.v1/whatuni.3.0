package com.wuni.valueobjects;

public class ModuleVO {
  //
private String moduleGroupId = null;
private String stageLabelId = null;
private String stageLabelName = null;
private String modules = null;
private String sourceCourseId = null;
private String moduleGroupDesc= null;
private String courseId = null;
private String moduleTypeName = null;
private String moduleId = null;
private String moduleTitle = null;
private String credits = null;
private String moduleDescription = null;
private String moduleURL = null;
private String moduleDescriptionFlag = null;
private String moduleGroup = null;
private String shortModuleDesc = null;

  public void setModuleGroupId(String moduleGroupId) {
    this.moduleGroupId = moduleGroupId;
  }
  public String getModuleGroupId() {
    return moduleGroupId;
  }
  public void setStageLabelId(String stageLabelId) {
    this.stageLabelId = stageLabelId;
  }
  public String getStageLabelId() {
    return stageLabelId;
  }
  public void setStageLabelName(String stageLabelName) {
    this.stageLabelName = stageLabelName;
  }
  public String getStageLabelName() {
    return stageLabelName;
  }
  public void setModules(String modules) {
    this.modules = modules;
  }
  public String getModules() {
    return modules;
  }
  public void setSourceCourseId(String sourceCourseId) {
    this.sourceCourseId = sourceCourseId;
  }
  public String getSourceCourseId() {
    return sourceCourseId;
  }
  public void setModuleGroupDesc(String moduleGroupDesc) {
    this.moduleGroupDesc = moduleGroupDesc;
  }
  public String getModuleGroupDesc() {
    return moduleGroupDesc;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setModuleTypeName(String moduleTypeName) {
    this.moduleTypeName = moduleTypeName;
  }
  public String getModuleTypeName() {
    return moduleTypeName;
  }
  public void setModuleId(String moduleId) {
    this.moduleId = moduleId;
  }
  public String getModuleId() {
    return moduleId;
  }
  public void setModuleTitle(String moduleTitle) {
    this.moduleTitle = moduleTitle;
  }
  public String getModuleTitle() {
    return moduleTitle;
  }
  public void setCredits(String credits) {
    this.credits = credits;
  }
  public String getCredits() {
    return credits;
  }
  public void setModuleDescription(String moduleDescription) {
    this.moduleDescription = moduleDescription;
  }
  public String getModuleDescription() {
    return moduleDescription;
  }
  public void setModuleURL(String moduleURL) {
    this.moduleURL = moduleURL;
  }
  public String getModuleURL() {
    return moduleURL;
  }
  public void setModuleDescriptionFlag(String moduleDescriptionFlag) {
    this.moduleDescriptionFlag = moduleDescriptionFlag;
  }
  public String getModuleDescriptionFlag() {
    return moduleDescriptionFlag;
  }

  public void setModuleGroup(String moduleGroup)
  {
    this.moduleGroup = moduleGroup;
  }

  public String getModuleGroup()
  {
    return moduleGroup;
  }
  public void setShortModuleDesc(String shortModuleDesc) {
    this.shortModuleDesc = shortModuleDesc;
  }
  public String getShortModuleDesc() {
    return shortModuleDesc;
  }
}
