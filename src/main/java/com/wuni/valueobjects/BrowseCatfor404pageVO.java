package com.wuni.valueobjects;

public class BrowseCatfor404pageVO {
  private String parentDesc = null;
  private String parentCatCode = null;
  private String childURl = null;
  public void setParentDesc(String parentDesc) {
    this.parentDesc = parentDesc;
  }
  public String getParentDesc() {
    return parentDesc;
  }
  public void setParentCatCode(String parentCatCode) {
    this.parentCatCode = parentCatCode;
  }
  public String getParentCatCode() {
    return parentCatCode;
  }
  public void setChildURl(String childURl) {
    this.childURl = childURl;
  }
  public String getChildURl() {
    return childURl;
  }
}
