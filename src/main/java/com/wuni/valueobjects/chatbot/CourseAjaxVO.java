package com.wuni.valueobjects.chatbot;


public class CourseAjaxVO {
   private String description;      
   private String searchId;
   private String searchCode;
   
   public String getDescription() {
      return description;
   }
   
   public void setDescription(String description) {
      this.description = description;
   }
   
   public String getSearchId() {
      return searchId;
   }
   
   public void setSearchId(String searchId) {
      this.searchId = searchId;
   }
   
   public String getSearchCode() {
      return searchCode;
   }
   
   public void setSearchCode(String searchCode) {
      this.searchCode = searchCode;
   }
   
}
