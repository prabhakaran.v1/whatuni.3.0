package com.wuni.valueobjects.chatbot;;

public class QualSubjectAjaxVO {
   
   private String searchId;
   private String description;
   private String searchCode;
   
   public String getSearchId() {
      return searchId;
   }
   
   public void setSearchId(String searchId) {
      this.searchId = searchId;
   }
   
   public String getDescription() {
      return description;
   }
   
   public void setDescription(String description) {
      this.description = description;
   }

  public String getSearchCode() {
	return searchCode;
  }

  public void setSearchCode(String searchCode) {
	this.searchCode = searchCode;
  }
   
}
