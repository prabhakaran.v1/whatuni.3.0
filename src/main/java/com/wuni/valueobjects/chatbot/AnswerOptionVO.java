package com.wuni.valueobjects.chatbot;


public class AnswerOptionVO {
   private String optionText;
   private String optionValue;
   private String nextQuestionId;
   private String commentAgainstAnswer;
   private String imageName;
   
   public String getOptionText() {
      return optionText;
   }
   
   public void setOptionText(String optionText) {
      this.optionText = optionText;
   }
   
   public String getOptionValue() {
      return optionValue;
   }
   
   public void setOptionValue(String optionValue) {
      this.optionValue = optionValue;
   }
   
   public String getNextQuestionId() {
      return nextQuestionId;
   }
   
   public void setNextQuestionId(String nextQuestionId) {
      this.nextQuestionId = nextQuestionId;
   }
   
   public String getCommentAgainstAnswer() {
      return commentAgainstAnswer;
   }
   
   public void setCommentAgainstAnswer(String commentAgainstAnswer) {
      this.commentAgainstAnswer = commentAgainstAnswer;
   }

  public void setImageName(String imageName) {
    this.imageName = imageName;
  }

  public String getImageName() {
    return imageName;
  }

}
