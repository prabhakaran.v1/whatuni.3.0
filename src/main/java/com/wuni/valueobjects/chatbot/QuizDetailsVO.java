package com.wuni.valueobjects.chatbot;

import java.util.ArrayList;

public class QuizDetailsVO {
  private ArrayList<QuestionDetailsVO> questionDetailsList = null;  
  private ArrayList<PreviousQualVO> previousQualList = null;
  private ArrayList<CourseAjaxVO> courseList = null;
  private ArrayList<AppVersionVO> versionDetails = null;
  private ArrayList<JobIndustryAjaxVO> jobIndustryList = null;
  private ArrayList<QualSubjectAjaxVO> qualSubjectList = null;
  private String courseCount = null;
  private String selectedFilterUrl = null;

  public void setQuestionDetailsList(ArrayList<QuestionDetailsVO> questionDetailsList) {
    this.questionDetailsList = questionDetailsList;
  }

  public ArrayList<QuestionDetailsVO> getQuestionDetailsList() {
    return questionDetailsList;
  }

  public void setPreviousQualList(ArrayList<PreviousQualVO> previousQualList) {
    this.previousQualList = previousQualList;
  }

  public ArrayList<PreviousQualVO> getPreviousQualList() {
    return previousQualList;
  }

  public void setCourseList(ArrayList<CourseAjaxVO> courseList) {
    this.courseList = courseList;
  }

  public ArrayList<CourseAjaxVO> getCourseList() {
    return courseList;
  }

  public void setVersionDetails(ArrayList<AppVersionVO> versionDetails) {
    this.versionDetails = versionDetails;
  }

  public ArrayList<AppVersionVO> getVersionDetails() {
    return versionDetails;
  }

  public void setJobIndustryList(ArrayList<JobIndustryAjaxVO> jobIndustryList) {
    this.jobIndustryList = jobIndustryList;
  }

  public ArrayList<JobIndustryAjaxVO> getJobIndustryList() {
    return jobIndustryList;
  }

  public void setQualSubjectList(ArrayList<QualSubjectAjaxVO> qualSubjectList) {
    this.qualSubjectList = qualSubjectList;
  }

  public ArrayList<QualSubjectAjaxVO> getQualSubjectList() {
    return qualSubjectList;
  }

  public void setCourseCount(String courseCount) {
    this.courseCount = courseCount;
  }

  public String getCourseCount() {
    return courseCount;
  }

  public void setSelectedFilterUrl(String selectedFilterUrl) {
    this.selectedFilterUrl = selectedFilterUrl;
  }

  public String getSelectedFilterUrl() {
    return selectedFilterUrl;
  }

}
