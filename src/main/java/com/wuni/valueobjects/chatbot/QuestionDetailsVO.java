package com.wuni.valueobjects.chatbot;

import java.util.ArrayList;


public class QuestionDetailsVO {   
   private String questionId;
   private String questionText;
   private String questionName;
   private String answerType;
   private String answerDisplayStyle;
   private String skipToNext;
   private String skipExit;
   private String progressPercent;
   private String ctaButtonText;
   private String filterName;
   private String timeDeplay;
   private ArrayList<AnswerOptionVO> answerOptionsList;
   private ArrayList<QuestionDetailsVO> questionList;
         
   public String getTimeDeplay() {
      return timeDeplay;
   }
   
   public void setTimeDeplay(String timeDeplay) {
      this.timeDeplay = timeDeplay;
   }

   public ArrayList<QuestionDetailsVO> getQuestionList() {
      return questionList;
   }
   
   public void setQuestionList(ArrayList<QuestionDetailsVO> questionList) {
      this.questionList = questionList;
   }

   public String getQuestionId() {
      return questionId;
   }
   
   public void setQuestionId(String questionId) {
      this.questionId = questionId;
   }
   
   public String getQuestionText() {
      return questionText;
   }
   
   public void setQuestionText(String questionText) {
      this.questionText = questionText;
   }   
   
   public String getQuestionName() {
      return questionName;
   }

   public void setQuestionName(String questionName) {
      this.questionName = questionName;
   }

   public String getAnswerType() {
      return answerType;
   }
   
   public void setAnswerType(String answerType) {
      this.answerType = answerType;
   }
   
   public String getAnswerDisplayStyle() {
      return answerDisplayStyle;
   }
   
   public void setAnswerDisplayStyle(String answerDisplayStyle) {
      this.answerDisplayStyle = answerDisplayStyle;
   }
   
   public String getSkipToNext() {
      return skipToNext;
   }
   
   public void setSkipToNext(String skipToNext) {
      this.skipToNext = skipToNext;
   }
   
   public String getSkipExit() {
      return skipExit;
   }
   
   public void setSkipExit(String skipExit) {
      this.skipExit = skipExit;
   }
   
   public String getProgressPercent() {
      return progressPercent;
   }
   
   public void setProgressPercent(String progressPercent) {
      this.progressPercent = progressPercent;
   }
   
   public String getCtaButtonText() {
      return ctaButtonText;
   }
   
   public void setCtaButtonText(String ctaButtonText) {
      this.ctaButtonText = ctaButtonText;
   }
   
   public String getFilterName() {
      return filterName;
   }
   
   public void setFilterName(String filterName) {
      this.filterName = filterName;
   }
   
   public ArrayList<AnswerOptionVO> getAnswerOptionsList() {
      return answerOptionsList;
   }
   
   public void setAnswerOptionsList(ArrayList<AnswerOptionVO> answerOptionsList) {
      this.answerOptionsList = answerOptionsList;
   }
  
}
