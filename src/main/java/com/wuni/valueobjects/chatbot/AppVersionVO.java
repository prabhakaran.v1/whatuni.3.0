package com.wuni.valueobjects.chatbot;

public class AppVersionVO {

   private String versionName;
   private String status;
   private String message;

   public String getVersionName() {
	return versionName;
   }

   public void setVersionName(String versionName) {
	this.versionName = versionName;
   }

   public String getStatus() {
	return status;
   }

   public void setStatus(String status) {
	this.status = status;
   }

   public String getMessage() {
	return message;
   }

   public void setMessage(String message) {
	this.message = message;
   }
}
