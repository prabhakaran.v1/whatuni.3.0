package com.wuni.valueobjects.chatbot;

public class JobIndustryAjaxVO {

   private String jobOrIndustryId;
   private String jobOrIndustryFlag;
   private String jobOrIndustryName;
   
   public String getJobOrIndustryId() {
      return jobOrIndustryId;
   }
   
   public void setJobOrIndustryId(String jobOrIndustryId) {
      this.jobOrIndustryId = jobOrIndustryId;
   }
   
   public String getJobOrIndustryFlag() {
      return jobOrIndustryFlag;
   }
   
   public void setJobOrIndustryFlag(String jobOrIndustryFlag) {
      this.jobOrIndustryFlag = jobOrIndustryFlag;
   }
   
   public String getJobOrIndustryName() {
      return jobOrIndustryName;
   }
   
   public void setJobOrIndustryName(String jobOrIndustryName) {
      this.jobOrIndustryName = jobOrIndustryName;
   }        
  
}
