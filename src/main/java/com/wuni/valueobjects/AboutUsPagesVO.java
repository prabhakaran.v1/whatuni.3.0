package com.wuni.valueobjects;

public class AboutUsPagesVO {
  public AboutUsPagesVO() {
  }
  
    private String firstName = null;
    private String lastName = null;
    private String emailAddress = null;
    private String Phone = null;
    private String jobTitle = null;
    private String schoolName = null; 
    private String town = null;
    private String date = null;
    private String time = null;
    private String aboutus = null;
    private String otherTxt = null;
    private String message = null;
    private String addressLine1 = null;
    private String addressLine2 = null;
    private String postCode = null;
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setPhone(String phone) {
        this.Phone = phone;
    }

    public String getPhone() {
        return Phone;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getTown() {
        return town;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setAboutus(String aboutus) {
        this.aboutus = aboutus;
    }

    public String getAboutus() {
        return aboutus;
    }

    public void setOtherTxt(String otherTxt) {
        this.otherTxt = otherTxt;
    }

    public String getOtherTxt() {
        return otherTxt;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPostCode() {
        return postCode;
    }
}
