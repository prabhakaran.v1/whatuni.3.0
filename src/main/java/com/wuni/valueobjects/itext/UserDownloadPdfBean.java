package com.wuni.valueobjects.itext;

public class UserDownloadPdfBean {
  private String userId = null;
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
}
