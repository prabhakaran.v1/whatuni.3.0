package com.wuni.valueobjects;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

public class InsightsVO {
  private String collegeId = null;
  private String pageName = null;
  private String userId = null;
  private String courseId = null;
  private String qualification = null;
  private String categoryCode = null;
  private String location = null;
  private String yearOfEntry = null;
  private String gradeValue = null;
  private String schoolName = null;
  private String regionName = null;
  private String categoryDesc = null;
  private String cpeSubjectName = null;
  private String cpeSubjectName2 = null;
  private String jacs = null;
  private String dateSearched = null;
  private String hecos = null;
  private String advertiser = null;
  private String granularQual = null;
  private String country = null;
  private String postCode = null;
  private String courseTitle = null;
  private String studyLevel = null;
  private String studyMode = null;
  private String searchCollegeId = null;
  private List basketInsights = new ArrayList();  
  private String institutionId = null;
  private String quintile = null;
  
  private String schoolUrn = null;
  private String sourceType = null;
  
  private String cpeSubjectNames = null;
  private String cpeSubjectCount = null;
   
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setPageName(String pageName) {
    this.pageName = pageName;
  }
  public String getPageName() {
    return pageName;
  }
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setQualification(String qualification) {
    this.qualification = qualification;
  }
  public String getQualification() {
    return qualification;
  }
  public void setCategoryCode(String categoryCode) {
    this.categoryCode = categoryCode;
  }
  public String getCategoryCode() {
    return categoryCode;
  }
  public void setLocation(String location) {
    this.location = location;
  }
  public String getLocation() {
    return location;
  }
  public void setYearOfEntry(String yearOfEntry) {
    this.yearOfEntry = yearOfEntry;
  }
  public String getYearOfEntry() {
    return yearOfEntry;
  }
  public void setGradeValue(String gradeValue) {
    this.gradeValue = gradeValue;
  }
  public String getGradeValue() {
    return gradeValue;
  }
  public void setSchoolName(String schoolName) {
    this.schoolName = schoolName;
  }
  public String getSchoolName() {
    return schoolName;
  }
  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }
  public String getRegionName() {
    return regionName;
  }
  public void setCategoryDesc(String categoryDesc) {
    this.categoryDesc = categoryDesc;
  }
  public String getCategoryDesc() {
    return categoryDesc;
  }
  public void setCpeSubjectName(String cpeSubjectName) {
    this.cpeSubjectName = cpeSubjectName;
  }
  public String getCpeSubjectName() {
    return cpeSubjectName;
  }
  public void setCpeSubjectName2(String cpeSubjectName2) {
    this.cpeSubjectName2 = cpeSubjectName2;
  }
  public String getCpeSubjectName2() {
    return cpeSubjectName2;
  }
  public void setJacs(String jacs) {
    this.jacs = jacs;
  }
  public String getJacs() {
    return jacs;
  }
  public void setDateSearched(String dateSearched) {
    this.dateSearched = dateSearched;
  }
  public String getDateSearched() {
    return dateSearched;
  }
  public void setHecos(String hecos) {
    this.hecos = hecos;
  }
  public String getHecos() {
    return hecos;
  }
  public void setAdvertiser(String advertiser) {
    this.advertiser = advertiser;
  }
  public String getAdvertiser() {
    return advertiser;
  }
  public void setGranularQual(String granularQual) {
    this.granularQual = granularQual;
  }
  public String getGranularQual() {
    return granularQual;
  }
  public void setCountry(String country) {
    this.country = country;
  }
  public String getCountry() {
    return country;
  }
  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }
  public String getPostCode() {
    return postCode;
  }
  public void setCourseTitle(String courseTitle) {
    this.courseTitle = courseTitle;
  }
  public String getCourseTitle() {
    return courseTitle;
  }
  public void setStudyLevel(String studyLevel) {
    this.studyLevel = studyLevel;
  }
  public String getStudyLevel() {
    return studyLevel;
  }
  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }
  public String getStudyMode() {
    return studyMode;
  }
  public void setSearchCollegeId(String searchCollegeId) {
    this.searchCollegeId = searchCollegeId;
  }
  public String getSearchCollegeId() {
    return searchCollegeId;
  }
  public void setBasketInsights(List basketInsights) {
    this.basketInsights = basketInsights;
  }
  public List getBasketInsights() {
    return basketInsights;
  }
  public void setInstitutionId(String institutionId) {
    this.institutionId = institutionId;
  }
  public String getInstitutionId() {
    return institutionId;
  }
  public void setSchoolUrn(String schoolUrn) {
    this.schoolUrn = schoolUrn;
  }
  public String getSchoolUrn() {
    return schoolUrn;
  }
  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }
  public String getSourceType() {
    return sourceType;
  }
  public void setCpeSubjectNames(String cpeSubjectNames) {
    this.cpeSubjectNames = cpeSubjectNames;
  }
  public String getCpeSubjectNames() {
    return cpeSubjectNames;
  }
  public void setCpeSubjectCount(String cpeSubjectCount) {
    this.cpeSubjectCount = cpeSubjectCount;
  }
  public String getCpeSubjectCount() {
    return cpeSubjectCount;
  }

    public void setQuintile(String quintile) {
        this.quintile = quintile;
    }

    public String getQuintile() {
        return quintile;
    }
}
