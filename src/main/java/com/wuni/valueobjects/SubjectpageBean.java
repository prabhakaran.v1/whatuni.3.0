package com.wuni.valueobjects;

public class SubjectpageBean {
  public SubjectpageBean() {
  }
  
  private String subjectName = null;
  private String category_id = null;
  private String qualification = null;
  public void setSubjectName(String subjectName) {
    this.subjectName = subjectName;
  }
  public String getSubjectName() {
    return subjectName;
  }
  public void setCategory_id(String category_id) {
    this.category_id = category_id;
  }
  public String getCategory_id() {
    return category_id;
  }
  public void setQualification(String qualification) {
    this.qualification = qualification;
  }
  public String getQualification() {
    return qualification;
  }
}
