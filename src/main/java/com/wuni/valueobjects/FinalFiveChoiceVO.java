package com.wuni.valueobjects;

public class FinalFiveChoiceVO {
  String userId = null;
  String swapPosition = null;
  String swapDirection = null;
  String basketId = null;
  String inputType = null;
  String associateTypeCode = null;
  String userType = null;
  String finalChoiceId = null;
  String collegeId = null;
  String courseId = null;
  String choicePos = null;
  String status = null;
  String associateText = null;
  String basketContentId = null;
  
  public void setUserId(String userId) {
    this.userId = userId;
  }
  public String getUserId() {
    return userId;
  }
  public void setSwapPosition(String swapPosition) {
    this.swapPosition = swapPosition;
  }
  public String getSwapPosition() {
    return swapPosition;
  }
  public void setSwapDirection(String swapDirection) {
    this.swapDirection = swapDirection;
  }
  public String getSwapDirection() {
    return swapDirection;
  }
  public void setBasketId(String basketId) {
    this.basketId = basketId;
  }
  public String getBasketId() {
    return basketId;
  }
  public void setInputType(String inputType) {
    this.inputType = inputType;
  }
  public String getInputType() {
    return inputType;
  }
  public void setAssociateTypeCode(String associateTypeCode) {
    this.associateTypeCode = associateTypeCode;
  }
  public String getAssociateTypeCode() {
    return associateTypeCode;
  }
  public void setUserType(String userType) {
    this.userType = userType;
  }
  public String getUserType() {
    return userType;
  }
  public void setFinalChoiceId(String finalChoiceId) {
    this.finalChoiceId = finalChoiceId;
  }
  public String getFinalChoiceId() {
    return finalChoiceId;
  }
  public void setCollegeId(String collegeId) {
    this.collegeId = collegeId;
  }
  public String getCollegeId() {
    return collegeId;
  }
  public void setCourseId(String courseId) {
    this.courseId = courseId;
  }
  public String getCourseId() {
    return courseId;
  }
  public void setChoicePos(String choicePos) {
    this.choicePos = choicePos;
  }
  public String getChoicePos() {
    return choicePos;
  }
  public void setStatus(String status) {
    this.status = status;
  }
  public String getStatus() {
    return status;
  }
  public void setAssociateText(String associateText) {
    this.associateText = associateText;
  }
  public String getAssociateText() {
    return associateText;
  }
  public void setBasketContentId(String basketContentId) {
    this.basketContentId = basketContentId;
  }
  public String getBasketContentId() {
    return basketContentId;
  }
}
