package com.wuni.valueobjects.whatunigo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;
import WUI.whatunigo.bean.GradeFilterBean;
import com.config.SpringContextInjector;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;

/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Class         : GradeFilterPageAction.java
 * Description   : Action to get the response for Offer journey from WhatuniGo webservice.
 * @version      : 1.0
 * @author       : Jeyalakshmi.D
 * Change Log :
 * *************************************************************************************************************************************
 * author	                 Ver 	           Modified On     	Modification Details 	               Change
 * *************************************************************************************************************************************
 * Jeyalakshmi.D                  1.0              28.03.2019            Initial Draft
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */

@Controller
public class GradeFilterPageController {
   @RequestMapping(value={"/wugo/qualifications/search"}, method = {RequestMethod.GET,RequestMethod.POST})
   public ModelAndView getGradeFilterPageContent(HttpServletRequest request, HttpServletResponse response,HttpSession session) throws Exception {
	
	    String forwardPage = "gradeFilterPage";
	    String action = request.getParameter("action");
	    GradeFilterBean gradeFilterBean = new GradeFilterBean();
	    if(!GenericValidator.isBlankOrNull(request.getParameter("subject"))) {
	      String subject = request.getParameter("subject");
	      request.setAttribute("subject",subject);
	    }
	    if(!GenericValidator.isBlankOrNull(request.getParameter("q"))) {
	      String q = request.getParameter("q");
	      request.setAttribute("q",q);
	    }
	   
	    if(!GenericValidator.isBlankOrNull(request.getParameter("university"))) {
	      String university = request.getParameter("university");
	      request.setAttribute("university",university);
	    } 
	    
	    if(!GenericValidator.isBlankOrNull(request.getParameter("location"))) {
	      String location = request.getParameter("location");
	      request.setAttribute("location", location);
	    } 
	    
	   // request.setAttribute("HIDE_LOGIN", "Y");
	    //request.setAttribute("ebookDim14", "WUGO");
	    //request.setAttribute("showBanner", "no");
	    String referrer = request.getHeader("referer");
	      if((referrer != null && referrer.contains("clearing")) || (request.getQueryString() != null && request.getQueryString().contains("clearing"))) {
	        String refererURL = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + "/"; 
	        String selQual = request.getParameter("qualification");
	        String scoreVal = request.getParameter("score");
	        String qualCode = "M";
	        String urlQual = "degree";
	        if ("degree".equalsIgnoreCase(selQual)) {
	          qualCode = "M";
	          urlQual = "degree";
	        } else if ("postgraduate".equalsIgnoreCase(selQual)) {
	          qualCode = "L";
	          urlQual = "postgraduate";
	        } else if ("foundation-degree".equalsIgnoreCase(selQual)) {
	          qualCode = "A";
	          urlQual = "foundation-degree";
	        } else if ("access-foundation".equalsIgnoreCase(selQual)) {
	          qualCode = "T";
	          urlQual = "access-foundation";
	        } else if ("hnd-hnc".equalsIgnoreCase(selQual)) {
	          qualCode = "N";
	          urlQual = "hnd-hnc";
	        } else if ("ucas_code".equalsIgnoreCase(selQual)) {
	          qualCode = "UCAS_CODE";
	          urlQual = "ucas_code";
	        } else if ("Clearing".equalsIgnoreCase(selQual)) {
	          qualCode = "M";
	          urlQual = "Clearing";
	        }
	        request.setAttribute("qualCode", qualCode);
	        request.setAttribute("score", scoreVal);
	        if(!GenericValidator.isBlankOrNull(scoreVal)){
	          session.setAttribute("USER_UCAS_SCORE", scoreVal);
	        }
	        refererURL +=  urlQual + "-courses/"; 
	        if(!GenericValidator.isBlankOrNull(request.getParameter("university"))){
	          refererURL += "csearch?clearing";
	          if(!GenericValidator.isBlankOrNull(request.getParameter("subject"))){
	            refererURL += "&subject=" + request.getParameter("subject");
	          }
	          if(!GenericValidator.isBlankOrNull(request.getParameter("q"))){
	            refererURL += "&q=" + request.getParameter("q");
	          }
	          refererURL += "&university=" + request.getParameter("university");
	        }else {
	          refererURL += "search?clearing"; 
	          if(!GenericValidator.isBlankOrNull(request.getParameter("subject"))){
	            refererURL += "&subject=" + request.getParameter("subject");
	          }
	          if(!GenericValidator.isBlankOrNull(request.getParameter("q"))){
	            refererURL += "&q=" + request.getParameter("q");
	          }
	        }
	      request.setAttribute("referralUrl", refererURL);
	    }  
	    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
	    Map<String, Object> resultMap = null;
	    String subjectSessionId = !GenericValidator.isBlankOrNull((String)session.getAttribute("subjectSessionId")) ? (String)session.getAttribute("subjectSessionId") : null;
	    String userId = !GenericValidator.isBlankOrNull(new SessionData().getData(request, "y")) ? new SessionData().getData(request, "y") : "0";
	    if("QUAL_SUB_AJAX".equalsIgnoreCase(action)) {
	      String freeText = request.getParameter("free-text");
	      Gson gson = new GsonBuilder().create();
	      String responseAsString = getBody(request);
	      gradeFilterBean = gson.fromJson(responseAsString, GradeFilterBean.class);
	      Object[][] qualDetailsArr;
	      ArrayList<GradeFilterValidationVO> qualSubList = gradeFilterBean.getQualSubList();
	      if (qualSubList != null && qualSubList.size() > 0) {
	        qualDetailsArr = new String[qualSubList.size()][5];
	        for (int lp = 0; lp < qualSubList.size(); lp++) {
	          GradeFilterValidationVO gradeFilterValidationVO = qualSubList.get(lp);
	          qualDetailsArr[lp][0] = gradeFilterValidationVO.getQualId();
	          qualDetailsArr[lp][1] = gradeFilterValidationVO.getQualSeqId();
	          qualDetailsArr[lp][2] = gradeFilterValidationVO.getSubId();
	          qualDetailsArr[lp][3] = gradeFilterValidationVO.getGrade();
	          qualDetailsArr[lp][4] = gradeFilterValidationVO.getSubSeqId();
	        }
	       gradeFilterBean.setQualDetailsArr(qualDetailsArr);
	      }else{
	        gradeFilterBean.setQualDetailsArr(null);
	      }
	      resultMap = commonBusiness.getSubjectlist(freeText, gradeFilterBean);
	      ArrayList ajaxSubjectList = (ArrayList)resultMap.get("RetVal");
	      if (ajaxSubjectList != null && ajaxSubjectList.size() > 0) {
	        request.setAttribute("ajaxSubjectList", ajaxSubjectList);
	      }
	      forwardPage = "ajaxSubjectPage";
	    } 
	    else if("CALCULATE_UCAS_POINTS".equalsIgnoreCase(action)) {
	      Gson gson = new GsonBuilder().create();
	      String responseAsString = getBody(request);
	      gradeFilterBean = gson.fromJson(responseAsString, GradeFilterBean.class);
	      Object[][] qualDetailsArr;
	      ArrayList<GradeFilterValidationVO> qualSubList = gradeFilterBean.getQualSubList();
	      if (qualSubList != null && qualSubList.size() > 0) {
	        qualDetailsArr = new String[qualSubList.size()][5];
	        for (int lp = 0; lp < qualSubList.size(); lp++) {
	          GradeFilterValidationVO gradeFilterValidationVO = qualSubList.get(lp);
	          qualDetailsArr[lp][0] = gradeFilterValidationVO.getQualId();
	          qualDetailsArr[lp][1] = gradeFilterValidationVO.getQualSeqId();
	          qualDetailsArr[lp][2] = gradeFilterValidationVO.getSubId();
	          qualDetailsArr[lp][3] = gradeFilterValidationVO.getGrade();
	          qualDetailsArr[lp][4] = gradeFilterValidationVO.getSubSeqId();
	        }
	       gradeFilterBean.setQualDetailsArr(qualDetailsArr);
	     } 
	     resultMap = commonBusiness.getSubjectUcasPoints(gradeFilterBean);
	     String ucasPoints = (String)resultMap.get("UCAS_POINTS");
	     if(ucasPoints != null) {
	       request.setAttribute("ucasPoints", ucasPoints);
	     }
	     String returnString = "";
	     StringBuffer buffer = new StringBuffer();
	     response.setContentType("text/html");
	     response.setHeader("Cache-Control", "no-cache");
	     returnString = "UCAS_POINT";
	     returnString += "##SPLIT##" + ucasPoints;
	     buffer.append(returnString);
	     setResponseText(response, buffer);
	     return null;  
	   }
	   else if("SAVE_VALIDATE".equalsIgnoreCase(action)) {
	     Gson gson = new GsonBuilder().create();
	     String responseAsString = getBody(request);
	     gradeFilterBean = gson.fromJson(responseAsString, GradeFilterBean.class);
	     Object[][] qualDetailsArr;
	     ArrayList<GradeFilterValidationVO> qualSubList = gradeFilterBean.getQualSubList();
	     if (qualSubList != null && qualSubList.size() > 0) {
	       qualDetailsArr = new String[qualSubList.size()][5];
	       for (int lp = 0; lp < qualSubList.size(); lp++) {
	         GradeFilterValidationVO gradeFilterValidationVO = qualSubList.get(lp);
	         qualDetailsArr[lp][0] = gradeFilterValidationVO.getQualId();
	         qualDetailsArr[lp][1] = gradeFilterValidationVO.getQualSeqId();
	         qualDetailsArr[lp][2] = gradeFilterValidationVO.getSubId();
	         qualDetailsArr[lp][3] = gradeFilterValidationVO.getGrade();
	         qualDetailsArr[lp][4] = gradeFilterValidationVO.getSubSeqId();
	      }
	      gradeFilterBean.setQualDetailsArr(qualDetailsArr);
	   } 
	   gradeFilterBean.setUserId(userId);
	   gradeFilterBean.setSessionId(subjectSessionId);
	   resultMap = commonBusiness.validateQualGrade(gradeFilterBean);
	   String valErrorCode = (String)resultMap.get("P_VALIDATION_ERROR_CODE");
	   String qualification = (String)resultMap.get("P_QUALIFICATION");
	   String qualification1 = (String)resultMap.get("P_QUALIFICATION1");
	   String qualification2 = !GenericValidator.isBlankOrNull((String)resultMap.get("P_QUALIFICATION2")) ? ", " + (String)resultMap.get("P_QUALIFICATION2") : "";
	   String qualification3 = !GenericValidator.isBlankOrNull((String)resultMap.get("P_QUALIFICATION3")) ? "and " + (String)resultMap.get("P_QUALIFICATION3") : "";
	   if("0".equalsIgnoreCase(valErrorCode) || "2".equalsIgnoreCase(valErrorCode) || "1".equalsIgnoreCase(valErrorCode)) {
	     String returnString = "";
	     StringBuffer buffer = new StringBuffer();
	     response.setContentType("text/html");
	     response.setHeader("Cache-Control", "no-cache");
	     returnString = "VAL_ERROR";
	     returnString += "##SPLIT##" + valErrorCode;
	     returnString += "##SPLIT##" + qualification;
	     returnString += "##SPLIT##" +qualification1;  
	     returnString += "##SPLIT##" + qualification2;
	     returnString += "##SPLIT##" + qualification3;
	     buffer.append(returnString);
	     setResponseText(response, buffer);
	     return null;
	   }  
	 }
	 else if("QUAL_MATCHED_COURSE".equalsIgnoreCase(action)) {
	   String ucasPoints = request.getParameter("ucasPoints");
	   gradeFilterBean.setSubjectName((String)request.getParameter("subject"));
	   gradeFilterBean.setKeyword((String)request.getParameter("q"));
	   gradeFilterBean.setGradePoints(ucasPoints);
	   gradeFilterBean.setCollegeName((String)request.getParameter("university"));
	   gradeFilterBean.setQualificationCode(!GenericValidator.isBlankOrNull((String)request.getParameter("qualCode")) ? request.getParameter("qualCode") : (String)request.getAttribute("qualCode"));
	   gradeFilterBean.setLocation(request.getParameter("location"));
	   gradeFilterBean.setUserUcasScore(request.getParameter("userUcasScore"));
	   
	   resultMap = commonBusiness.getMatchingCourse(gradeFilterBean);
	   String courseCount = (String)resultMap.get("P_COURSE_COUNT");
	   // if (!"0".equalsIgnoreCase(courseCount)) {
	     String returnString = "";
	     StringBuffer buffer = new StringBuffer();
	     response.setContentType("text/html");
	     response.setHeader("Cache-Control", "no-cache");
	     returnString = "COURSE_COUNT";
	     returnString += "##SPLIT##" + courseCount;
	     buffer.append(returnString);
	     setResponseText(response, buffer);
	     return null;
	  // }
	  }
	  else if("SAVE_REDIRECT".equalsIgnoreCase(action)) {
	    Gson gson = new GsonBuilder().create();
	    String responseAsString = getBody(request);
	    gradeFilterBean = gson.fromJson(responseAsString, GradeFilterBean.class);
	    Object[][] qualDetailsArr;
	    ArrayList<GradeFilterValidationVO> qualSubList = gradeFilterBean.getQualSubList();
	    if (qualSubList != null && qualSubList.size() > 0) {
	      qualDetailsArr = new String[qualSubList.size()][5];
	      for (int lp = 0; lp < qualSubList.size(); lp++) {
	        GradeFilterValidationVO gradeFilterValidationVO = qualSubList.get(lp);
	        qualDetailsArr[lp][0] = gradeFilterValidationVO.getQualId();
	        qualDetailsArr[lp][1] = gradeFilterValidationVO.getQualSeqId();
	        qualDetailsArr[lp][2] = gradeFilterValidationVO.getSubId();
	        qualDetailsArr[lp][3] = gradeFilterValidationVO.getGrade();
	        qualDetailsArr[lp][4] = gradeFilterValidationVO.getSubSeqId();
	      }
	     gradeFilterBean.setQualDetailsArr(qualDetailsArr);
	    }
	    gradeFilterBean.setUserId(userId);
	    gradeFilterBean.setSessionId(subjectSessionId);
	    resultMap = commonBusiness.saveQualGradeFilter(gradeFilterBean);
	    return null;
	  }
	  else {
	    gradeFilterBean.setUserId(userId);
	    gradeFilterBean.setSessionId(subjectSessionId);
	    gradeFilterBean.setSubjectName((String)request.getParameter("subject"));
	    gradeFilterBean.setKeyword((String)request.getParameter("q"));
	    gradeFilterBean.setCollegeName((String)request.getParameter("university"));
	    gradeFilterBean.setQualificationCode(!GenericValidator.isBlankOrNull((String)request.getParameter("qualCode")) ? request.getParameter("qualCode") : (String)request.getAttribute("qualCode"));
	    gradeFilterBean.setLocation(!GenericValidator.isBlankOrNull((String)request.getParameter("location")) ? request.getParameter("location") : (String)request.getAttribute("location"));
	    
	    resultMap = commonBusiness.getGradeFilterPage(gradeFilterBean);
	    ArrayList<GradeFilterVO> qualList = (ArrayList<GradeFilterVO>)resultMap.get("PC_QUALIFICATION_LIST");
	    ArrayList<UserQualDetailsVO> userQualList = (ArrayList<UserQualDetailsVO>)resultMap.get("PC_USER_QUALIFICATIONS");
	    String ucasPoint = (String)resultMap.get("P_UCAS_POINTS");
	    subjectSessionId = (String)resultMap.get("P_SESSION_ID");
	    String ucasMaxPoints = (String)resultMap.get("P_MAX_UCAS_POINTS");
	    session.setAttribute("subjectSessionId" , subjectSessionId);
	    if (qualList != null && qualList.size() > 0) {
	      request.setAttribute("qualificationList", qualList);
	      //System.out.println(qualList.size() + "qualList size");
	    }
	    if (userQualList != null && userQualList.size() > 0) {
	      request.setAttribute("userQualList", userQualList);
	      request.setAttribute("userQualSize", String.valueOf(userQualList.size()));
	      request.setAttribute("className", "qual_cnt");
	    }
	    if(!GenericValidator.isBlankOrNull(ucasPoint)) {
	      request.setAttribute("ucasPoint", ucasPoint);
	    }
	    if(!GenericValidator.isBlankOrNull(ucasMaxPoints)) {
	      request.setAttribute("ucasMaxPoints", ucasMaxPoints);
	    }
	    if(userQualList != null && userQualList.size() == 0) {
	      forwardPage = "newUserGradePage";
	    }
	    else if(userQualList != null && userQualList.size() > 0) {
	      forwardPage = "gradeFilterPage";
	    }
	  } 
	
	
	return new ModelAndView(forwardPage);
   }



private void setResponseText(HttpServletResponse response, StringBuffer responseMessage) {
   try {
     response.setContentType("TEXT/PLAIN");
     Writer writer = response.getWriter();
     writer.write(responseMessage.toString());
   } catch (IOException exception) {
     exception.printStackTrace();
   }
 }
public String getBody(HttpServletRequest request) {
  String body = null;
  StringBuilder stringBuilder = new StringBuilder();
  BufferedReader bufferedReader = null;
  try {
    InputStream inputStream = request.getInputStream();
    if (inputStream != null) {
      bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
      char[] charBuffer = new char[128];
      int bytesRead = -1;
      while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
        stringBuilder.append(charBuffer, 0, bytesRead);
      }
    } else {
      stringBuilder.append("");
    }
  } catch (IOException ex) {
    ex.printStackTrace();
    return null;
  } finally {
    if (bufferedReader != null) {
      try {
        bufferedReader.close();
      } catch (IOException ex) {
        ex.printStackTrace();
        return null;
      }
    }
  }
  body = stringBuilder.toString();
  return body;
}
}