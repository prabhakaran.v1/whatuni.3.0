package com.wuni.valueobjects.whatunigo;


public class PageInfoVO {
   private String page_id;
   private String page_name;
   private String review_page_flag;
   private String status;
   private String current_page_flag;
   private String reviewed_flag;
   
   public String getPage_id() {
      return page_id;
   }
   
   public void setPage_id(String page_id) {
      this.page_id = page_id;
   }
   
   public String getPage_name() {
      return page_name;
   }
   
   public void setPage_name(String page_name) {
      this.page_name = page_name;
   }
   
   public String getReview_page_flag() {
      return review_page_flag;
   }
   
   public void setReview_page_flag(String review_page_flag) {
      this.review_page_flag = review_page_flag;
   }
   
   public String getStatus() {
      return status;
   }
   
   public void setStatus(String status) {
      this.status = status;
   }
   
   public String getCurrent_page_flag() {
      return current_page_flag;
   }
   
   public void setCurrent_page_flag(String current_page_flag) {
      this.current_page_flag = current_page_flag;
   }
   
   public String getReviewed_flag() {
      return reviewed_flag;
   }
   
   public void setReviewed_flag(String reviewed_flag) {
      this.reviewed_flag = reviewed_flag;
   }
}
