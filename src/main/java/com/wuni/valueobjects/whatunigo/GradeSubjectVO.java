package com.wuni.valueobjects.whatunigo;


public class GradeSubjectVO {
   private String entry_subject_id;
   private String entry_subject;
   private String entry_grade;
   
   public String getEntry_subject_id() {
      return entry_subject_id;
   }
   
   public void setEntry_subject_id(String entry_subject_id) {
      this.entry_subject_id = entry_subject_id;
   }
   
   public String getEntry_subject() {
      return entry_subject;
   }
   
   public void setEntry_subject(String entry_subject) {
      this.entry_subject = entry_subject;
   }
   
   public String getEntry_grade() {
      return entry_grade;
   }
   
   public void setEntry_grade(String entry_grade) {
      this.entry_grade = entry_grade;
   }
}
