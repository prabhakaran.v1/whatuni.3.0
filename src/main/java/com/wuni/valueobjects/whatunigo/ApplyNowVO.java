package com.wuni.valueobjects.whatunigo;


public class ApplyNowVO {
   String courseId = "";
   String collegeId = "";
   String collegeName = "";  
   String collegeNameDisplay = "";
   String courseTitle = "";
   String opportunityId = "";
   String courseNameDisplay = "";
   String collegeLogoPath = "";
   String studyMode = "";
   String studyDuration = "";
   String ucasTariffPoints = "";
   String userId = "";
   
   public String getCourseId() {
      return courseId;
   }
   
   public void setCourseId(String courseId) {
      this.courseId = courseId;
   }
   
   public String getCollegeId() {
      return collegeId;
   }
   
   public void setCollegeId(String collegeId) {
      this.collegeId = collegeId;
   }
   
   public String getCollegeName() {
      return collegeName;
   }
   
   public void setCollegeName(String collegeName) {
      this.collegeName = collegeName;
   }
   
   public String getCollegeNameDisplay() {
      return collegeNameDisplay;
   }
   
   public void setCollegeNameDisplay(String collegeNameDisplay) {
      this.collegeNameDisplay = collegeNameDisplay;
   }
   
   public String getCourseTitle() {
      return courseTitle;
   }
   
   public void setCourseTitle(String courseTitle) {
      this.courseTitle = courseTitle;
   }
   
   public String getOpportunityId() {
      return opportunityId;
   }
   
   public void setOpportunityId(String opportunityId) {
      this.opportunityId = opportunityId;
   }
   
   public String getCourseNameDisplay() {
      return courseNameDisplay;
   }
   
   public void setCourseNameDisplay(String courseNameDisplay) {
      this.courseNameDisplay = courseNameDisplay;
   }
   
   public String getCollegeLogoPath() {
      return collegeLogoPath;
   }
   
   public void setCollegeLogoPath(String collegeLogoPath) {
      this.collegeLogoPath = collegeLogoPath;
   }
   
   public String getStudyMode() {
      return studyMode;
   }
   
   public void setStudyMode(String studyMode) {
      this.studyMode = studyMode;
   }
   
   public String getStudyDuration() {
      return studyDuration;
   }
   
   public void setStudyDuration(String studyDuration) {
      this.studyDuration = studyDuration;
   }
   
   public String getUcasTariffPoints() {
      return ucasTariffPoints;
   }
   
   public void setUcasTariffPoints(String ucasTariffPoints) {
      this.ucasTariffPoints = ucasTariffPoints;
   }
   
   public String getUserId() {
      return userId;
   }
   
   public void setUserId(String userId) {
      this.userId = userId;
   }
}
