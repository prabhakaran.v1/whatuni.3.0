package com.wuni.valueobjects.whatunigo;

import java.util.ArrayList;
import com.offer.service.dao.util.valueobject.offer.qualifications.UserSubjectVO;


public class UserqualificationsVO {
   private String entry_qual_id;
   private String entry_qual_desc;
   private String gcse_grade_flag;
   private ArrayList<UserSubjectVO> qual_subject_list;
   private String entry_qual_level;
   
   public String getEntry_qual_id() {
      return entry_qual_id;
   }
   
   public void setEntry_qual_id(String entry_qual_id) {
      this.entry_qual_id = entry_qual_id;
   }
   
   public String getEntry_qual_desc() {
      return entry_qual_desc;
   }
   
   public void setEntry_qual_desc(String entry_qual_desc) {
      this.entry_qual_desc = entry_qual_desc;
   }
   
   public String getGcse_grade_flag() {
      return gcse_grade_flag;
   }
   
   public void setGcse_grade_flag(String gcse_grade_flag) {
      this.gcse_grade_flag = gcse_grade_flag;
   }
   
   public ArrayList<UserSubjectVO> getQual_subject_list() {
      return qual_subject_list;
   }
   
   public void setQual_subject_list(ArrayList<UserSubjectVO> qual_subject_list) {
      this.qual_subject_list = qual_subject_list;
   }
   
   public String getEntry_qual_level() {
      return entry_qual_level;
   }
   
   public void setEntry_qual_level(String entry_qual_level) {
      this.entry_qual_level = entry_qual_level;
   }
}
