package com.wuni.valueobjects.whatunigo;

import java.util.ArrayList;


public class RegionListVO {
   private String regionName = null;
   private String urlText = null;
   private String regionId = null;
   private ArrayList subRegionList = null;
   
   public String getRegionName() {
      return regionName;
   }
   
   public void setRegionName(String regionName) {
      this.regionName = regionName;
   }
   
   public String getUrlText() {
      return urlText;
   }
   
   public void setUrlText(String urlText) {
      this.urlText = urlText;
   }
   
   public String getRegionId() {
      return regionId;
   }
   
   public void setRegionId(String regionId) {
      this.regionId = regionId;
   }
   
   public ArrayList getSubRegionList() {
      return subRegionList;
   }
   
   public void setSubRegionList(ArrayList subRegionList) {
      this.subRegionList = subRegionList;
   }
}
