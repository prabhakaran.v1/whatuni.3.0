package com.wuni.valueobjects.whatunigo;


public class SubRegionListVO {
   private String regionName = null;
   private String urlText = null;
   private String regionId = null;
   
   public String getRegionName() {
      return regionName;
   }
   
   public void setRegionName(String regionName) {
      this.regionName = regionName;
   }
   
   public String getUrlText() {
      return urlText;
   }
   
   public void setUrlText(String urlText) {
      this.urlText = urlText;
   }
   
   public String getRegionId() {
      return regionId;
   }
   
   public void setRegionId(String regionId) {
      this.regionId = regionId;
   }
}
