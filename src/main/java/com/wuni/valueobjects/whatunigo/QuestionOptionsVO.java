package com.wuni.valueobjects.whatunigo;


public class QuestionOptionsVO {
   private String option_id;
   private String option_desc;
   
   public String getOption_id() {
      return option_id;
   }
   
   public void setOption_id(String option_id) {
      this.option_id = option_id;
   }
   
   public String getOption_desc() {
      return option_desc;
   }
   
   public void setOption_desc(String option_desc) {
      this.option_desc = option_desc;
   }
}
