package com.wuni.valueobjects.whatunigo;

import java.util.ArrayList;
import com.wuni.valueobjects.whatunigo.course.CourseInfoVO;


public class AcceptOfferVO {
   private ArrayList<CourseInfoVO> course_info_list;
   private ArrayList<QuestionAnswerVO> question_answer_list;
   private String offer_text;
   
   public ArrayList<CourseInfoVO> getCourse_info_list() {
      return course_info_list;
   }
   
   public void setCourse_info_list(ArrayList<CourseInfoVO> course_info_list) {
      this.course_info_list = course_info_list;
   }
   
   public ArrayList<QuestionAnswerVO> getQuestion_answer_list() {
      return question_answer_list;
   }
   
   public void setQuestion_answer_list(ArrayList<QuestionAnswerVO> question_answer_list) {
      this.question_answer_list = question_answer_list;
   }
   
   public String getOffer_text() {
      return offer_text;
   }
   
   public void setOffer_text(String offer_text) {
      this.offer_text = offer_text;
   }
}
