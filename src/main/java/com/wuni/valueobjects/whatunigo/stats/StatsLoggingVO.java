package com.wuni.valueobjects.whatunigo.stats;


public class StatsLoggingVO {
   private String userId;
   private String sessionId;
   private String affilateId;
   private String activity;
   private String collegeId;
   private String profileId;
   private String searchHeader;
   private String extraText;
   private String ipUrl;
   private String userAgent;
   private String requestUrl;
   private String refererUrl;
   private String jsLog;
   private String subOrderItemId;
   private String networkId;
   private String widgetId;
   private String courseId;
   private String courseTitle;
   private String departmentId;
   private String departmentName;
   private String mobileFlag;
   private String trackSessionId;
   private String appFlag;  
   private String LDCSOne;
   private String LDCSTwo;
   private String studyLevel;
   private String studyMode;
   private String HECOSOne;
   private String HECOSTwo;
   private String screenWidth;
   private String accessToken;
   
   public String getUserId() {
      return userId;
   }
   
   public void setUserId(String userId) {
      this.userId = userId;
   }
   
   public String getSessionId() {
      return sessionId;
   }
   
   public void setSessionId(String sessionId) {
      this.sessionId = sessionId;
   }
   
   public String getAffilateId() {
      return affilateId;
   }
   
   public void setAffilateId(String affilateId) {
      this.affilateId = affilateId;
   }
   
   public String getActivity() {
      return activity;
   }
   
   public void setActivity(String activity) {
      this.activity = activity;
   }
   
   public String getCollegeId() {
      return collegeId;
   }
   
   public void setCollegeId(String collegeId) {
      this.collegeId = collegeId;
   }
   
   public String getProfileId() {
      return profileId;
   }
   
   public void setProfileId(String profileId) {
      this.profileId = profileId;
   }
   
   public String getSearchHeader() {
      return searchHeader;
   }
   
   public void setSearchHeader(String searchHeader) {
      this.searchHeader = searchHeader;
   }
   
   public String getExtraText() {
      return extraText;
   }
   
   public void setExtraText(String extraText) {
      this.extraText = extraText;
   }
   
   public String getIpUrl() {
      return ipUrl;
   }
   
   public void setIpUrl(String ipUrl) {
      this.ipUrl = ipUrl;
   }
   
   public String getUserAgent() {
      return userAgent;
   }
   
   public void setUserAgent(String userAgent) {
      this.userAgent = userAgent;
   }
   
   public String getRequestUrl() {
      return requestUrl;
   }
   
   public void setRequestUrl(String requestUrl) {
      this.requestUrl = requestUrl;
   }
   
   public String getRefererUrl() {
      return refererUrl;
   }
   
   public void setRefererUrl(String refererUrl) {
      this.refererUrl = refererUrl;
   }
   
   public String getJsLog() {
      return jsLog;
   }
   
   public void setJsLog(String jsLog) {
      this.jsLog = jsLog;
   }
   
   public String getSubOrderItemId() {
      return subOrderItemId;
   }
   
   public void setSubOrderItemId(String subOrderItemId) {
      this.subOrderItemId = subOrderItemId;
   }
   
   public String getNetworkId() {
      return networkId;
   }
   
   public void setNetworkId(String networkId) {
      this.networkId = networkId;
   }
   
   public String getWidgetId() {
      return widgetId;
   }
   
   public void setWidgetId(String widgetId) {
      this.widgetId = widgetId;
   }
   
   public String getCourseId() {
      return courseId;
   }
   
   public void setCourseId(String courseId) {
      this.courseId = courseId;
   }
   
   public String getCourseTitle() {
      return courseTitle;
   }
   
   public void setCourseTitle(String courseTitle) {
      this.courseTitle = courseTitle;
   }
   
   public String getDepartmentId() {
      return departmentId;
   }
   
   public void setDepartmentId(String departmentId) {
      this.departmentId = departmentId;
   }
   
   public String getDepartmentName() {
      return departmentName;
   }
   
   public void setDepartmentName(String departmentName) {
      this.departmentName = departmentName;
   }
   
   public String getMobileFlag() {
      return mobileFlag;
   }
   
   public void setMobileFlag(String mobileFlag) {
      this.mobileFlag = mobileFlag;
   }
   
   public String getTrackSessionId() {
      return trackSessionId;
   }
   
   public void setTrackSessionId(String trackSessionId) {
      this.trackSessionId = trackSessionId;
   }
   
   public String getAppFlag() {
      return appFlag;
   }
   
   public void setAppFlag(String appFlag) {
      this.appFlag = appFlag;
   }
   
   public String getLDCSOne() {
      return LDCSOne;
   }
   
   public void setLDCSOne(String lDCSOne) {
      LDCSOne = lDCSOne;
   }
   
   public String getLDCSTwo() {
      return LDCSTwo;
   }
   
   public void setLDCSTwo(String lDCSTwo) {
      LDCSTwo = lDCSTwo;
   }
   
   public String getStudyLevel() {
      return studyLevel;
   }
   
   public void setStudyLevel(String studyLevel) {
      this.studyLevel = studyLevel;
   }
   
   public String getStudyMode() {
      return studyMode;
   }
   
   public void setStudyMode(String studyMode) {
      this.studyMode = studyMode;
   }
   
   public String getHECOSOne() {
      return HECOSOne;
   }
   
   public void setHECOSOne(String hECOSOne) {
      HECOSOne = hECOSOne;
   }
   
   public String getHECOSTwo() {
      return HECOSTwo;
   }
   
   public void setHECOSTwo(String hECOSTwo) {
      HECOSTwo = hECOSTwo;
   }
   
   public String getScreenWidth() {
      return screenWidth;
   }
   
   public void setScreenWidth(String screenWidth) {
      this.screenWidth = screenWidth;
   }
   
   public String getAccessToken() {
      return accessToken;
   }
   
   public void setAccessToken(String accessToken) {
      this.accessToken = accessToken;
   }
}
