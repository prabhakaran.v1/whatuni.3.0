package com.wuni.valueobjects.whatunigo;

import java.util.ArrayList;


public class QuestionAnswerVO {
   private String page_id;
   private String page_name;
   private String review_page_flag;
   private String question_id;
   private String page_question_id;
   private String question_title;
   private ArrayList<QuestionOptionsVO> question_options;
   private String answer_value;
   private String answer_option_id;
   private String max_length;
   private String mandatory_flag;
   private String help_text_desc;
   private String help_text_title;
   private String format_desc;
   private String default_show_flag;
   private String section_review_flag;
   
   public String getPage_id() {
      return page_id;
   }
   
   public void setPage_id(String page_id) {
      this.page_id = page_id;
   }
   
   public String getPage_name() {
      return page_name;
   }
   
   public void setPage_name(String page_name) {
      this.page_name = page_name;
   }
   
   public String getReview_page_flag() {
      return review_page_flag;
   }
   
   public void setReview_page_flag(String review_page_flag) {
      this.review_page_flag = review_page_flag;
   }
   
   public String getQuestion_id() {
      return question_id;
   }
   
   public void setQuestion_id(String question_id) {
      this.question_id = question_id;
   }
   
   public String getPage_question_id() {
      return page_question_id;
   }
   
   public void setPage_question_id(String page_question_id) {
      this.page_question_id = page_question_id;
   }
   
   public String getQuestion_title() {
      return question_title;
   }
   
   public void setQuestion_title(String question_title) {
      this.question_title = question_title;
   }
   
   public ArrayList<QuestionOptionsVO> getQuestion_options() {
      return question_options;
   }
   
   public void setQuestion_options(ArrayList<QuestionOptionsVO> question_options) {
      this.question_options = question_options;
   }
   
   public String getAnswer_value() {
      return answer_value;
   }
   
   public void setAnswer_value(String answer_value) {
      this.answer_value = answer_value;
   }
   
   public String getAnswer_option_id() {
      return answer_option_id;
   }
   
   public void setAnswer_option_id(String answer_option_id) {
      this.answer_option_id = answer_option_id;
   }
   
   public String getMax_length() {
      return max_length;
   }
   
   public void setMax_length(String max_length) {
      this.max_length = max_length;
   }
   
   public String getMandatory_flag() {
      return mandatory_flag;
   }
   
   public void setMandatory_flag(String mandatory_flag) {
      this.mandatory_flag = mandatory_flag;
   }
   
   public String getHelp_text_desc() {
      return help_text_desc;
   }
   
   public void setHelp_text_desc(String help_text_desc) {
      this.help_text_desc = help_text_desc;
   }
   
   public String getHelp_text_title() {
      return help_text_title;
   }
   
   public void setHelp_text_title(String help_text_title) {
      this.help_text_title = help_text_title;
   }
   
   public String getFormat_desc() {
      return format_desc;
   }
   
   public void setFormat_desc(String format_desc) {
      this.format_desc = format_desc;
   }
   
   public String getDefault_show_flag() {
      return default_show_flag;
   }
   
   public void setDefault_show_flag(String default_show_flag) {
      this.default_show_flag = default_show_flag;
   }
   
   public String getSection_review_flag() {
      return section_review_flag;
   }
   
   public void setSection_review_flag(String section_review_flag) {
      this.section_review_flag = section_review_flag;
   }
}
