package com.wuni.valueobjects.whatunigo.course;


public class CourseInfoVO {
   private String course_id;
   private String course_name;
   private String college_id;
   private String college_name;
   private String college_display_name;
   private String study_mode;
   private String study_duration;
   private String tariff_points;
   private String college_logo;
   private String course_Title;
   private String course_exist_flag;
   private String seo_study_level_text;
   private String opportunity_id;
   private String course_deleted_flag;
   private String cdPageUrl;
   private String profilePageUrl;
   
   public String getCourse_id() {
      return course_id;
   }
   
   public void setCourse_id(String course_id) {
      this.course_id = course_id;
   }
   
   public String getCourse_name() {
      return course_name;
   }
   
   public void setCourse_name(String course_name) {
      this.course_name = course_name;
   }
   
   public String getCollege_id() {
      return college_id;
   }
   
   public void setCollege_id(String college_id) {
      this.college_id = college_id;
   }
   
   public String getCollege_name() {
      return college_name;
   }
   
   public void setCollege_name(String college_name) {
      this.college_name = college_name;
   }
   
   public String getCollege_display_name() {
      return college_display_name;
   }
   
   public void setCollege_display_name(String college_display_name) {
      this.college_display_name = college_display_name;
   }
   
   public String getStudy_mode() {
      return study_mode;
   }
   
   public void setStudy_mode(String study_mode) {
      this.study_mode = study_mode;
   }
   
   public String getStudy_duration() {
      return study_duration;
   }
   
   public void setStudy_duration(String study_duration) {
      this.study_duration = study_duration;
   }
   
   public String getTariff_points() {
      return tariff_points;
   }
   
   public void setTariff_points(String tariff_points) {
      this.tariff_points = tariff_points;
   }
   
   public String getCollege_logo() {
      return college_logo;
   }
   
   public void setCollege_logo(String college_logo) {
      this.college_logo = college_logo;
   }
   
   public String getCourse_Title() {
      return course_Title;
   }
   
   public void setCourse_Title(String course_Title) {
      this.course_Title = course_Title;
   }
   
   public String getCourse_exist_flag() {
      return course_exist_flag;
   }
   
   public void setCourse_exist_flag(String course_exist_flag) {
      this.course_exist_flag = course_exist_flag;
   }
   
   public String getSeo_study_level_text() {
      return seo_study_level_text;
   }
   
   public void setSeo_study_level_text(String seo_study_level_text) {
      this.seo_study_level_text = seo_study_level_text;
   }
   
   public String getOpportunity_id() {
      return opportunity_id;
   }
   
   public void setOpportunity_id(String opportunity_id) {
      this.opportunity_id = opportunity_id;
   }
   
   public String getCourse_deleted_flag() {
      return course_deleted_flag;
   }
   
   public void setCourse_deleted_flag(String course_deleted_flag) {
      this.course_deleted_flag = course_deleted_flag;
   }
   
   public String getCdPageUrl() {
      return cdPageUrl;
   }
   
   public void setCdPageUrl(String cdPageUrl) {
      this.cdPageUrl = cdPageUrl;
   }
   
   public String getProfilePageUrl() {
      return profilePageUrl;
   }
   
   public void setProfilePageUrl(String profilePageUrl) {
      this.profilePageUrl = profilePageUrl;
   }
}
