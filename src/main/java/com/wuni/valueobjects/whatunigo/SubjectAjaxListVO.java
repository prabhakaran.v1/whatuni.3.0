package com.wuni.valueobjects.whatunigo;


public class SubjectAjaxListVO {
   private String subject_id = null;
   private String subject_desc = null;
   private String subject_text = null;
   
   public String getSubject_id() {
      return subject_id;
   }
   
   public void setSubject_id(String subject_id) {
      this.subject_id = subject_id;
   }
   
   public String getSubject_desc() {
      return subject_desc;
   }
   
   public void setSubject_desc(String subject_desc) {
      this.subject_desc = subject_desc;
   }
   
   public String getSubject_text() {
      return subject_text;
   }
   
   public void setSubject_text(String subject_text) {
      this.subject_text = subject_text;
   }
}
