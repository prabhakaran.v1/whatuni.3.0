package com.wuni.valueobjects.whatunigo;


public class GradeFilterValidationVO {
   private String qualId;
   private String subId;
   private String grade;
   private String qualSeqId;
   private String subSeqId;
   
   public String getQualId() {
      return qualId;
   }
   
   public void setQualId(String qualId) {
      this.qualId = qualId;
   }
   
   public String getSubId() {
      return subId;
   }
   
   public void setSubId(String subId) {
      this.subId = subId;
   }
   
   public String getGrade() {
      return grade;
   }
   
   public void setGrade(String grade) {
      this.grade = grade;
   }
   
   public String getQualSeqId() {
      return qualSeqId;
   }
   
   public void setQualSeqId(String qualSeqId) {
      this.qualSeqId = qualSeqId;
   }
   
   public String getSubSeqId() {
      return subSeqId;
   }
   
   public void setSubSeqId(String subSeqId) {
      this.subSeqId = subSeqId;
   }
}
