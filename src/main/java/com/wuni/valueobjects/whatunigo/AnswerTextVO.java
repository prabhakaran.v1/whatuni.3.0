package com.wuni.valueobjects.whatunigo;


public class AnswerTextVO {
   private String answerText = null;

   
   public String getAnswerText() {
      return answerText;
   }

   
   public void setAnswerText(String answerText) {
      this.answerText = answerText;
   }
}
