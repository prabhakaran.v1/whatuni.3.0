package com.wuni.downloadpdf.servlet;

import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;

import com.layer.util.SpringConstants;

@Controller
@WebServlet(urlPatterns = "/get-clearing-ebook-pdf")
public class ClearingEBookPdfServlet extends HttpServlet{
	
  public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    CommonFunction common = new CommonFunction();
    String redirectURL = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + GlobalConstants.WU_CONTEXT_PATH + SpringConstants.PRE_CLEARING_LANDING_URL + SpringConstants.DOT_HTML;
    //String myWhatuniUrl = GlobalConstants.WU_CONTEXT_PATH + "/mywhatuni.html"; //TODO Comment while deployee in server and uncomment above line.
    try {
      String URL = common.getWUSysVarValue("WU_PDF_CLEARING_TEMPLATE_PATH");
      String split[] = URL.split("/");
      String pdfName = split[split.length - 1];
      URL url = new URL(URL);
      response.setContentType("application/pdf");
      HttpURLConnection connection = (HttpURLConnection)url.openConnection();
      if (connection.getResponseCode() == 200) {
        int contentLength = connection.getContentLength();
        response.setContentLength(contentLength);
      }
      response.setHeader("Expires", "0");
      response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
      response.setHeader("Content-Disposition", "filename="+pdfName); 
      InputStream pdfSource = connection.getInputStream();
      OutputStream pdfTarget = response.getOutputStream();
      int FILE_CHUNK_SIZE = 1024 * 4;
      byte[] chunk = new byte[FILE_CHUNK_SIZE];
      int n = 0;
      while ((n = pdfSource.read(chunk)) != -1) {
        pdfTarget.write(chunk, 0, n);
      }
      pdfTarget.flush();
      pdfTarget.close();
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("pdfErrorMsg", "error");
      response.sendRedirect(redirectURL);
    }
  }
}
