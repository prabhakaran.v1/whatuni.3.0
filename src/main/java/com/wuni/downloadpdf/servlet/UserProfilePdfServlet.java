package com.wuni.downloadpdf.servlet;

import WUI.security.SecurityEvaluator;
import WUI.utilities.CommonFunction;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.util.seo.SeoUrls;
import com.wuni.valueobjects.itext.UserDownloadPdfBean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.sql.Clob;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;

/**
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * Class         : UserProfilePdfServlet.java
 * Description   : Class file to loads the PDF with help of XML and XLS internal files using ITEXT
 * @version      : 1.0
 * Change Log :
 * *************************************************************************************************************************************
 * Author	               Ver 	        Modified On          	Modification Details
 * *************************************************************************************************************************************
 * Hema S.               wu581                              Generate Pdf for logged in user details..
 * Prabhakaran V.        wu582        05.10.2018            Loading fonts in PDF.
 * Sangeeth.S            wu585        12.01.2019            Hanlded pdf download error message
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */

@Controller
public class UserProfilePdfServlet extends HttpServlet {

  public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    SeoUrls seoUrls = new SeoUrls();
    String myWhatuniUrl = GlobalConstants.WHATUNI_SCHEME_NAME + GlobalConstants.WHATUNI_DOMAIN + seoUrls.getMyWhatuniURL();
    //String myWhatuniUrl = GlobalConstants.WU_CONTEXT_PATH + "/mywhatuni.html"; //TODO Comment while deployee in server and uncomment above line.
    try {
      String PDF_FILE = "userprofile";
      String XLS_FILE = null;
      InputStream htmlStream = null;
      String TODAY = getTodayDate();
      String userId = new SessionData().getData(request, "y");
      CommonFunction common = new CommonFunction();
      UserDownloadPdfBean userDownloadPdfBean = new UserDownloadPdfBean();
      PDF_FILE = common.getUserName(new SessionData().getData(request, "y"), request);
      PDF_FILE = common.replaceHypen(common.replaceSpecialCharacter(common.replaceURL(PDF_FILE)));
      PDF_FILE += "-" + common.replaceHypen(TODAY);
      //
      // ---(A)--- Checking uer has login or not
      try {
        if (!(new SecurityEvaluator().checkSecurity("REGISTERED_USER", request, response))) {
          response.sendRedirect(myWhatuniUrl);
        }
      } catch (Exception secutityException) {
        secutityException.printStackTrace();
      }
      //XLS_FILE = getServletContext().getRealPath("/itext/userTemplate.xsl"); //TODO Comment while deployee in server and uncomment below line.
      XLS_FILE = common.getWUSysVarValue("WU_PDF_USER_TEMPLATE_PATH"); //File path in weblogic server - /wu-cont/itext/userTemplate.xsl
      if (!GenericValidator.isBlankOrNull(userId)) {
        //
        // ---(B)--- Getting XML content from DB call
        //
        ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
        userDownloadPdfBean.setUserId(userId);
        Map getXMLMap = commonBusiness.getUserProfilePdf(userDownloadPdfBean);
        Clob clob = (Clob)getXMLMap.get("P_USER_PDF");
        String errorCode = (String)getXMLMap.get("P_ERROR");        
        //This block to return error message and redirection to display the error for 12_FEB_2019_REL added by Sangeeth.S
        if(GlobalConstants.ERROR.equalsIgnoreCase(errorCode)){
          session.setAttribute("pdfErrorMsg","error");
          response.sendRedirect(myWhatuniUrl);          
        }else{
          String pdfErrorMsg = (String)session.getAttribute("pdfErrorMsg");          
          if (GlobalConstants.ERROR.equalsIgnoreCase(pdfErrorMsg)) {
            session.removeAttribute("pdfErrorMsg");            
          }        
          //
          String XML_CONTENT = new String();
          XML_CONTENT = common.getClobValue(clob);
          //
          // ---(C)--- Iterate XML tag into XSL file and convert into Byte stream 
          //
          StreamSource xmlInSource = new StreamSource(new StringReader(XML_CONTENT));
          TransformerFactory tFactory = TransformerFactory.newInstance();
          Transformer xslSource = tFactory.newTransformer(new StreamSource(new File(XLS_FILE)));
          StringWriter xmlOutWriter = new StringWriter();
          xslSource.transform(xmlInSource, new StreamResult(xmlOutWriter));
          htmlStream = new ByteArrayInputStream(xmlOutWriter.toString().getBytes("UTF-8"));
          //
          Document document = new Document(PageSize.LETTER);
          ByteArrayOutputStream baos = new ByteArrayOutputStream();
          PdfWriter pdfWriter = PdfWriter.getInstance(document, baos);
          HeaderFooter event = new HeaderFooter();
          pdfWriter.setBoxSize("art", new Rectangle(36, 54, 559, 788));
          pdfWriter.setPageEvent(event);
          response.setContentType("application/pdf");
          document.open();
          //
          // ---(D)--- Loading fonts
          //
          XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
           //URL font_path = getServletContext().getResource("/itext/Lato-Regular.ttf"); 
          //fontProvider.register(font_path.toString()); //TODO Comment while deployee in server and uncomment below line.
          fontProvider.register("/wu-cont/itext/Lato-Regular.ttf");
          //font_path = getServletContext().getResource("/itext/Lato-Bold.ttf");
          //fontProvider.register(font_path.toString());//TODO Comment while deployee in server and uncomment below line.
          fontProvider.register("/wu-cont/itext/Lato-Bold.ttf");
          FontFactory.setFontImp(fontProvider);
          //
          // ---(E)--- Adding document properties
          //
          document.addCreationDate();
          document.addTitle(GlobalConstants.WHATUNI_DOMAIN);
          document.setMargins(20, 20, 20, 20);
          document.newPage();
          document.setMargins(20, 20, 20, 20);
          XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
          worker.parseXHtml(pdfWriter, document, htmlStream, null, null, fontProvider); //worker.parseXHtml(pdfWriter, document, htmlContent, charSet, cssContent, fontProvider)
          document.close();
          //
          response.setHeader("Expires", "0");
          response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
          response.setHeader("Content-Disposition", "attachment;filename=\"" + PDF_FILE + ".pdf\"");
          response.setContentType("application/pdf");
          response.setContentLength(baos.size());
          OutputStream os = response.getOutputStream();
          baos.writeTo(os);
          os.flush();
          os.close();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      session.setAttribute("pdfErrorMsg","error");
      response.sendRedirect(myWhatuniUrl);
    }
  }

  /**
   * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * Method to get the Todays date to display in the PDF page
   * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public String getTodayDate() {
    DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
    String TODAY = formatter.format(new Date());
    String[] days = TODAY.split("-");
    int day = Integer.parseInt(days[0]);
    String formatedDate = day + " " + days[1] + " " + days[2];
    return formatedDate;
  }

}
/**
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * @see inner class to draw the pagenation and footer link in the PDF
 * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
class HeaderFooter extends PdfPageEventHelper {

  /**
   * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see onEndPage Method to adds a header to every page
   * @param writer
   * @param document
   * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void onEndPage(PdfWriter writer, Document document) {
    PdfContentByte canvas = getCanvas(writer, 0, 0, 0, 0);
    try {
      Chunk wuLink = new Chunk(GlobalConstants.WHATUNI_DOMAIN, FontFactory.getFont(FontFactory.TIMES_ROMAN, 10, Font.NORMAL));
      wuLink.setAnchor(GlobalConstants.WHATUNI_DOMAIN);
      addTextToCanvas(canvas, "" + (wuLink), 10, 500, 10, "#0080ff"); // whatuni link
      addTextToCanvas(canvas, "" + (writer.getPageNumber()), 10, 590, 10, "#707070"); // right side pagination
    } catch (MalformedURLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (DocumentException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  /**
   * /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see addTextToCanvas Method to add a footer content
   * @param canvas
   * @param text
   * @param size
   * @param text_x
   * @param text_y
   * @param canvasColor
   * @throws DocumentException
   * @throws IOException
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public void addTextToCanvas(PdfContentByte canvas, String text, int size, float text_x, float text_y, String canvasColor) throws DocumentException, IOException {
    BaseFont base_normal = null;
    try {
      base_normal = BaseFont.createFont("http://" + GlobalConstants.WHATUNI_DOMAIN + "/wu-cont/cssstyles/fonts/Lato-Regular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED); //Added WHATUNI_SCHEME_NAME by Indumathi Mar-29-16
    } catch (com.itextpdf.text.DocumentException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    canvas.beginText();
    canvas.setFontAndSize(base_normal, size);
    canvas.moveText(text_x, text_y);
    canvas.setColorFill(getBaseColor(canvasColor));
    canvas.showText(text);
    canvas.endText();
  }

  /**
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see getBaseColor Method to get the base color of pagination and footer link
   * @param colorStr as color code
   * @return BaseColor
   * ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public BaseColor getBaseColor(String colorStr) {
    return new BaseColor(Integer.parseInt(colorStr.substring(1, 3), 16), Integer.parseInt(colorStr.substring(3, 5), 16), Integer.parseInt(colorStr.substring(5, 7), 16));
  }

  /**
   * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   * @see getCanvas Method to get canvas for writting footer
   * @param writer
   * @param x as x-coordinate of the top left corner
   * @param y as y-coordinate of the top left corner
   * @param w as width of the rectangle
   * @param h as  height of the rectangle
   * @return canvas
   * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   */
  public PdfContentByte getCanvas(PdfWriter writer, int x, int y, int w, int h) {
    PdfContentByte canvas = writer.getDirectContent();
    canvas.rectangle(x, y, w, h);
    canvas.setColorFill(getBaseColor("#FFFFFF"));
    canvas.fill();
    return canvas;
  }

}