package com.wuni.whatcanido.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import WUI.security.SecurityEvaluator;
import WUI.utilities.GlobalConstants;
import WUI.utilities.SessionData;

import com.config.SpringContextInjector;
import com.layer.business.ICommonBusiness;
import com.layer.util.SpringConstants;
import com.wuni.ultimatesearch.SaveULInputbean;
import com.wuni.ultimatesearch.iwanttobe.forms.IWanttobeBean;
import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;

@Controller 
public class WhatCanIDoWidgetController {
	 @RequestMapping(value = "/what-can-i-do-widget"  , method = {RequestMethod.GET , RequestMethod.POST})
	  public ModelAndView whatCanIDoWidget(ModelMap model, HttpServletRequest request, HttpServletResponse response, WhatCanIDoBean whatCanIDoBean , HttpSession session) throws Exception {
		    try { // CHECK FOR SECURITY - VERIFY THE SESSION-ID USER-ID VALID //
		      if (!(new SecurityEvaluator().checkSecurity("ANONYMOUS", request, response))) {
		        String fromUrl = "/home.html";
		        session.setAttribute("fromUrl", fromUrl);
		        return new  ModelAndView("loginPage");
		      }
		    } catch (Exception securityException) {
		      securityException.printStackTrace();
		    }
		    String userID = new SessionData().getData(request, "y");
		    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
		    SaveULInputbean inputBean = new SaveULInputbean();
		    String pageName = request.getParameter("pageName");
		    if ("wcidResults".equalsIgnoreCase(pageName)) {
		      Map saveUltimateSearchMap = null;
		      String qualification = request.getParameter("qualification");
		      String qualDispalyName = request.getParameter("qualDispalyName");
		      inputBean.setQualification(checkBlankOrNull(request.getParameter("qualification"), "AL"));
		      inputBean.setQualSub1(checkBlankOrNull(request.getParameter("qualSub1"), "0"));
		      inputBean.setQualSub2(checkBlankOrNull(request.getParameter("qualSub2"), "0"));
		      inputBean.setQualSub3(checkBlankOrNull(request.getParameter("qualSub3"), "0"));
		      inputBean.setQualSub4(checkBlankOrNull(request.getParameter("qualSub4"), "0"));
		      inputBean.setQualSub5(checkBlankOrNull(request.getParameter("qualSub5"), "0"));
		      inputBean.setQualSub6(checkBlankOrNull(request.getParameter("qualSub6"), "0"));
		      inputBean.setQualGrades1(checkBlankOrNull(request.getParameter("qualGrades1"), null));
		      inputBean.setQualGrades2(checkBlankOrNull(request.getParameter("qualGrades2"), null));
		      inputBean.setQualGrades3(checkBlankOrNull(request.getParameter("qualGrades3"), null));
		      inputBean.setQualGrades4(checkBlankOrNull(request.getParameter("qualGrades4"), null));
		      inputBean.setQualGrades5(checkBlankOrNull(request.getParameter("qualGrades5"), null));
		      inputBean.setQualGrades6(checkBlankOrNull(request.getParameter("qualGrades6"), null));
		      inputBean.setUserId(userID);
		      inputBean.setUlSearchType("WCID");
		      inputBean.setTrackSessionId(new SessionData().getData(request, "userTrackId"));
		      saveUltimateSearchMap = commonBusiness.saveUltimateSearchDetails(inputBean);
		      String logId = (String)saveUltimateSearchMap.get("p_ultimate_search_new_log_id");
		      request.setAttribute("logId", logId);
		      getWhatcanidoResults(logId, request,qualification,qualDispalyName);
		      request.setAttribute("qualification", qualification);
		      String entryLevel = getEntryLevel(qualification);
		      request.setAttribute("entryLevel",entryLevel);
		      return new  ModelAndView("whatcanidoresults");
		    }
		    String jacsCode =  checkBlankOrNull(request.getParameter("jacsCode"), "");
		    String entryLevel = checkBlankOrNull(request.getParameter("entryLevel"), "");
		    String entryPoints = checkBlankOrNull(request.getParameter("entryPoints"), "");
		    String tariffPoint = checkBlankOrNull(request.getParameter("tariffPoint"), null);
		    String UKPRN = checkBlankOrNull(request.getParameter("ukprn"), "");
		    String collegeId = checkBlankOrNull(request.getParameter("collegeId"), null);
		    String logId = checkBlankOrNull(request.getParameter("logId"), "");
		    if ("UNI-RESULTS".equalsIgnoreCase(pageName)) {/* This block is used to get the university result while pagination and sorting */
		      String sortingOrder = checkBlankOrNull(request.getParameter("sortingOrder"), "D");
		      String sortingType = checkBlankOrNull(request.getParameter("sortingType"), "EMP_RATE");
		      String pageNo = checkBlankOrNull(request.getParameter("pageNo"), "1");
		      String employeeInfoFlag = checkBlankOrNull(request.getParameter("employeeInfoFlag"), "Y");
		      
		      whatCanIDoBean.setJacsCode(jacsCode);
		      whatCanIDoBean.setUkprn(UKPRN);
		      whatCanIDoBean.setCollegeId(collegeId);
		      whatCanIDoBean.setSortingOrder(sortingOrder);
		      whatCanIDoBean.setSortingType(sortingType);
		      whatCanIDoBean.setSaveLogId(logId);
		      whatCanIDoBean.setPageNo(pageNo);
		      whatCanIDoBean.setQualificationType(entryLevel);
		      whatCanIDoBean.setTariffPoint(tariffPoint);
		      whatCanIDoBean.setEmployeeInfoFlag(employeeInfoFlag); //Employee flag is 'Y' means got average stats info lists
		      Map uniInfoMap = commonBusiness.getUniSortInfo(whatCanIDoBean);
		      if (!uniInfoMap.isEmpty()) {
		        ArrayList uniInfoList = (ArrayList)uniInfoMap.get("pc_univ_emp_info"); //Uni results
		        if (uniInfoList != null && uniInfoList.size() > 0) {
		          WhatCanIDoBean whatcanidoVO = new WhatCanIDoBean();
		          whatcanidoVO = (WhatCanIDoBean)uniInfoList.get(0);
		          request.setAttribute("totalCount", whatcanidoVO.getTotalCount());
		          request.setAttribute("uniInfoList", uniInfoList);
		        }
		        ArrayList averageStatsList = (ArrayList)uniInfoMap.get("pc_emp_info"); //Average stats pod employee info list
		        if (averageStatsList != null && averageStatsList.size() > 0) {
		          request.setAttribute("averageStatsList", averageStatsList);
		        }
		        ArrayList industryInfoList = (ArrayList)uniInfoMap.get("pc_industry_info"); //Average stats industry info list
		        if (industryInfoList != null && industryInfoList.size() > 0) {
		          request.setAttribute("industryInfoList", industryInfoList);
		        }
		        ArrayList jobInfoList = (ArrayList)uniInfoMap.get("pc_job_info"); //Average stats job info list
		        if (jobInfoList != null && jobInfoList.size() > 0) {
		          request.setAttribute("jobInfoList", jobInfoList);
		        }
		        String jacsSubject = (String)uniInfoMap.get("p_JACS_subject_name");
		        if (!GenericValidator.isBlankOrNull(jacsSubject)) {
		          request.setAttribute("jacsSubject", jacsSubject);
		        }
		        request.setAttribute("sortingType", sortingType);
		        request.setAttribute("sortingOrder", sortingOrder);
		        request.setAttribute("jacsCode", jacsCode);
		        request.setAttribute("collegeId", collegeId);
		        request.setAttribute("ukprn", UKPRN);
		        request.setAttribute("logId", logId);
		        request.setAttribute("pageNo", pageNo);
		        request.setAttribute("entryLevel", entryLevel);
		        request.setAttribute("entryPoints", entryPoints);
		        request.setAttribute("tariffPoint", tariffPoint);
		        request.setAttribute("employeeInfoFlag", employeeInfoFlag);
		      }
		      if ("Y".equalsIgnoreCase(employeeInfoFlag)) {
		    	  return new  ModelAndView("unicourseresults");
		      } else {
		    	  return new  ModelAndView("unisortresults");
		      }
		    } else if ("JOB-INDUSTRY".equalsIgnoreCase(pageName)) { /* This block is used to get the job and industry info */
		   
		      whatCanIDoBean.setJacsCode(jacsCode);
		      whatCanIDoBean.setUkprn(UKPRN);
		      whatCanIDoBean.setCollegeId(collegeId);
		      whatCanIDoBean.setSaveLogId(logId);
		      whatCanIDoBean.setTariffPoint(tariffPoint);
		      whatCanIDoBean.setQualificationType(entryLevel);
		      Map jobIndustryInfo = commonBusiness.loadUniEmploymentInfo(whatCanIDoBean);
		      if (!jobIndustryInfo.isEmpty()) {
		        ArrayList industryInfoList = (ArrayList)jobIndustryInfo.get("pc_industry_info"); //Uni industry info list
		        if (industryInfoList != null && industryInfoList.size() > 0) {
		          request.setAttribute("industryStatsList", industryInfoList);
		        }
		        ArrayList jobInfoList = (ArrayList)jobIndustryInfo.get("pc_job_info"); //Uni job info list
		        if (jobInfoList != null && jobInfoList.size() > 0) {
		          request.setAttribute("jobStatsList", jobInfoList);
		        }
		      }
		      request.setAttribute("collegeId", collegeId);
		      return new  ModelAndView("jobindustryresults");
		    }
		    WhatCanIDoBean whatcanidoVO = new WhatCanIDoBean();
		    Map qualificationMap = commonBusiness.getWhatcanidoQualificationResults(whatcanidoVO);
		    List qualList = (ArrayList)qualificationMap.get("pc_qualification_list");
		    if (qualList != null && qualList.size() > 0) {
		      WhatCanIDoBean inputDtailsVO = (WhatCanIDoBean)qualList.get(0);
		      request.setAttribute("qualificationCode",inputDtailsVO.getQualification());
		      String qualificationGrades = inputDtailsVO.getQualificationGrades();
		      String[] qualGrade = qualificationGrades.split(",");
		      ArrayList qualGradeArr = new ArrayList();
		      WhatCanIDoBean whatcanidoQualVO = null;
		      for(int i=0; i<qualGrade.length; i++){
		        whatcanidoQualVO = new WhatCanIDoBean();
		        whatcanidoQualVO.setDefaultGrades(qualGrade[i]);
		        qualGradeArr.add(whatcanidoQualVO);
		      }
		      request.setAttribute("qualGradeArr",qualGradeArr);
		      request.setAttribute("qualificationTypeId",inputDtailsVO.getQualificationTypeId());
		      request.setAttribute("qualificationList", qualList);
		    }
		    request.setAttribute("GAPageName", GlobalConstants.WHAT_CAN_I_DO_GA_PAGE_NAME);
		    request.setAttribute("getInsightName", GlobalConstants.WHAT_CAN_DO_INSIGHT_NAME);
		    return new  ModelAndView("whatcanidohome");
		  }

		  /**
		   * checkBlankOrNull method to check blank or null and assign a default value
		   * @param inValue
		   * @param defaultVal
		   * @return String
		   */
		  private String checkBlankOrNull(String inValue, String defaultVal) {
		    return GenericValidator.isBlankOrNull(inValue) ? defaultVal : inValue;
		  }
		  
		  /**
		   * getEntryLevel method to get entry level based on qualification
		   * @param entryLevel
		   * @return String
		   */
		  private String getEntryLevel(String entryLevel){
		    String entryLevelStr = "ucas_points";
		    if("AL".equalsIgnoreCase(entryLevel)){
		      entryLevelStr = "a_level";
		    }else if("H".equalsIgnoreCase(entryLevel)){
		      entryLevelStr = "sqa_higher";
		    }else if("AH".equalsIgnoreCase(entryLevel)){
		      entryLevelStr = "sqa_adv";
		    }else if("BTEC".equalsIgnoreCase(entryLevel)){
		      entryLevelStr = "btec";
		    }
		    return entryLevelStr;
		  }
		  
		  /**
		   * getWhatcanidoResults method to get the results for page2 in WhatCanIDo widget 
		   * @param logId
		   * @param request
		   * @param qualification
		   * @param qualDispalyName
		   */
		  public void getWhatcanidoResults(String logId, HttpServletRequest request, String qualification, String qualDispalyName) {
		    Map whatcanidoSearchRestuls = null;
		    ICommonBusiness commonBusiness = (ICommonBusiness)new SpringContextInjector().getBeanFromSpringContext(SpringConstants.BEAN_ID_COMMON_BUSINESS);
		    WhatCanIDoBean getInputBean = new WhatCanIDoBean();
		    getInputBean.setSaveLogId(logId);
		    getInputBean.setPageNo("1");
		    whatcanidoSearchRestuls = commonBusiness.getWhatcanidoSearchResults(getInputBean);
		    int returnCode = Integer.parseInt(whatcanidoSearchRestuls.get("p_return_code").toString());
		    if (whatcanidoSearchRestuls.get("p_remaining_percent") != null && whatcanidoSearchRestuls.get("p_message_code") != null) {
		      int messageCode = Integer.parseInt(whatcanidoSearchRestuls.get("p_message_code").toString());
		      double messagePercent = Double.parseDouble(whatcanidoSearchRestuls.get("p_remaining_percent").toString());
		      request.setAttribute("messageCode", String.valueOf(messageCode));
		      request.setAttribute("messagePercent", String.valueOf(messagePercent));
		    }
		    String resultsGrades = "";
		    if(whatcanidoSearchRestuls.get("p_results_grades") != null){
		      resultsGrades = whatcanidoSearchRestuls.get("p_results_grades").toString();
		    }
		    ArrayList inputDetailsList = (ArrayList)whatcanidoSearchRestuls.get("pc_input_details");
		    ArrayList subjectsNameList = new ArrayList();
		    String resultsFlag = "";
		    ArrayList gradesList = new ArrayList();
		    if (inputDetailsList != null && inputDetailsList.size() > 0) {
		      SaveULInputbean inputDtailsVO = (SaveULInputbean)inputDetailsList.get(0);
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualSubDesc1())) { subjectsNameList.add(inputDtailsVO.getQualSubDesc1()); }
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualSubDesc2())) { subjectsNameList.add(inputDtailsVO.getQualSubDesc2()); }
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualSubDesc3())) { subjectsNameList.add(inputDtailsVO.getQualSubDesc3()); }
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualSubDesc4())) { subjectsNameList.add(inputDtailsVO.getQualSubDesc4()); }
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualSubDesc5())) { subjectsNameList.add(inputDtailsVO.getQualSubDesc5()); }
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualSubDesc6())) { subjectsNameList.add(inputDtailsVO.getQualSubDesc6()); }
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualGrades1())) { gradesList.add(inputDtailsVO.getQualGrades1()); }
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualGrades2())) { gradesList.add(inputDtailsVO.getQualGrades2()); }
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualGrades3())) { gradesList.add(inputDtailsVO.getQualGrades3()); }
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualGrades4())) { gradesList.add(inputDtailsVO.getQualGrades4()); }
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualGrades5())) { gradesList.add(inputDtailsVO.getQualGrades5()); }
		      if (!GenericValidator.isBlankOrNull(inputDtailsVO.getQualGrades6())) { gradesList.add(inputDtailsVO.getQualGrades6()); }
		      resultsFlag = inputDtailsVO.getWcidResultsFlag();
		    }
		    String subjectsNameListStr = "";
		    String gradesSubjectList = "";
		    int size = subjectsNameList.size();
		    if(returnCode == 2){
		      size = size - 1;
		    }
		    //To get the list of subjects and grades text
		    int returnCodeTwoSize = subjectsNameList.size();
		    String returnCodeTwoGradesText = "";
		    String returnCodeTwoSubjectDisplayText = "";
		    int returnCodeSumofTariffPoint = 0;
		    for (int i = 0; i < returnCodeTwoSize; i++) {
		      String subNameList = subjectsNameList.get(i).toString();
		      String grdNameList = gradesList.get(i).toString();
		      subjectsNameListStr += (i != returnCodeTwoSize-1) ? subNameList + ", " : subNameList;
		      if ("TP".equalsIgnoreCase(qualification)) {
		        returnCodeSumofTariffPoint += returnCodeSumofTariffPoint + (GenericValidator.isBlankOrNull(grdNameList) ? 0 : Integer.parseInt(grdNameList));
		      } else {
		        returnCodeTwoGradesText += (i != returnCodeTwoSize-1) ? grdNameList + ", " : grdNameList;
		      }
		      gradesSubjectList += (subNameList + "(" + grdNameList + ")");
		      gradesSubjectList += (i != returnCodeTwoSize-1) ? "/ " : "";
		    }
		    if(subjectsNameListStr.indexOf(",") > -1) {
		      subjectsNameListStr = subjectsNameListStr.substring(0,subjectsNameListStr.lastIndexOf(",")) + " and " + subjectsNameListStr.substring(subjectsNameListStr.lastIndexOf(",")+1);
		    }
		    returnCodeTwoSubjectDisplayText += subjectsNameListStr + " with ";
		    returnCodeTwoSubjectDisplayText += "TP".equalsIgnoreCase(qualification) ? ("tariff points "+String.valueOf(returnCodeSumofTariffPoint)) : ("grades " + returnCodeTwoGradesText);
		    request.setAttribute("returnCodeTwoHeadingText", returnCodeTwoSubjectDisplayText);
		    //To get the closest match subject text
		    String closeMatchText = "";
		    for(int i = 0; i < size; i++){
		      closeMatchText += (i != size-1) ? subjectsNameList.get(i) + ", " : subjectsNameList.get(i);
		    }
		    if(closeMatchText.indexOf(",") > -1) {
		      closeMatchText = closeMatchText.substring(0,closeMatchText.lastIndexOf(",")) + " and " + closeMatchText.substring(closeMatchText.lastIndexOf(",")+1);
		    }
		    subjectsNameListStr = closeMatchText;
		    closeMatchText += " with ";
		    closeMatchText += "TP".equalsIgnoreCase(qualification) ? "tariff points " : "grades ";
		    request.setAttribute("returnCodeTwoCloseMatch",(closeMatchText + resultsGrades));
		    //
		    request.setAttribute("subjectsNameList", subjectsNameListStr);
		    request.setAttribute("resultsFlag",resultsFlag);
		    request.setAttribute("gradesSubjectList",gradesSubjectList);
		    request.setAttribute("qualDispalyName", qualDispalyName);
		    ArrayList jacsSubjectsList = (ArrayList)whatcanidoSearchRestuls.get("pc_results");
		    if (jacsSubjectsList != null && jacsSubjectsList.size() > 0) {
		      request.setAttribute("jacsSubjectsList", jacsSubjectsList);
		    }
		  }

}
