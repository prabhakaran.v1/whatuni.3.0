package com.wuni.whatcanido.sp;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

import WUI.utilities.GlobalConstants;

import com.wuni.ultimatesearch.whatcanido.forms.WhatCanIDoBean;

/**
 * @WHATCANIDO 
 * @version 1.0
 * @since 08-03-16 Release
 * @purpose  This SP is used to get the qualification list.
 * Change Log
 * ***************************************************************************************************************************************************
 * Date           Name          Ver.       Changes desc           Rel Ver.
 * ***************************************************************************************************************************************************
 * 08-03-16       S.Indumathi   1.1        Page Redesign            wu_550
 */
public class WhatcanidoQualificationSP extends StoredProcedure {

  public WhatcanidoQualificationSP(DataSource datasource) {
    setDataSource(datasource);
    setSql(GlobalConstants.GET_QUALIFICATION_LIST_PRC);
    declareParameter(new SqlOutParameter("pc_qualification_list", OracleTypes.CURSOR, new WhatCanIDoQualificationRowMapperImpl()));
  }

  public Map execute(WhatCanIDoBean inputBean) {
    Map outMap = null;
    Map inMap = new HashMap();
    try {
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }

  public class WhatCanIDoQualificationRowMapperImpl implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
      WhatCanIDoBean whatcanidoVO = new WhatCanIDoBean();
      whatcanidoVO.setQualificationId(rs.getString("qual_id"));
      whatcanidoVO.setQualification(rs.getString("qualification"));
      whatcanidoVO.setQualificationGrades(rs.getString("grade_str"));
      whatcanidoVO.setQualificationTypeId(rs.getString("qual_type_id"));
      whatcanidoVO.setDefaultNoOfSubjects(rs.getString("default_no_of_subjects"));
      return whatcanidoVO;
    }
  }
}
