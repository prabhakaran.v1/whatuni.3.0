package com.wuni.whatcanido.sp;

import WUI.utilities.GlobalConstants;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import oracle.jdbc.OracleTypes;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.object.StoredProcedure;

public class GetDBNameSP extends StoredProcedure {

  public GetDBNameSP(DataSource datasource) {
    setDataSource(datasource);
    setFunction(true);
    setSql(GlobalConstants.GET_DB_NAME_FN);
    declareParameter(new SqlOutParameter("DB_NAME", OracleTypes.VARCHAR));
    compile();
  }
  public Map execute() {
    Map outMap = null;
    try {
      Map inMap = new HashMap();
      outMap = execute(inMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return outMap;
  }
}
